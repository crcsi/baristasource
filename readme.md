**Barista**
===========

An easy-to-use photogrammetric software system for the generation of spatial information products from satellite imagery, Barista supports multi- and stereo-image networks and has been especially tailored for 3D geopositioning and feature extraction from single images via monoplotting.

Main Features
=============

* High-accuracy geopositioning from high-resolution satellite imagery (HRSI) via bias-corrected RPCs (suited to IKONOS and QuickBird), a rigorous physical model (eg for SPOT5 and ALOS) and the affine projection model
* Ortho-image generation
* Pansharpening via IHS transformation and PCA
* Monoplotting of points, lines and buildings to create 3D models from single images (requires an underlying DEM); affords building height determination

Other capabilities
==================

* Import, display, processing of satellite imagery in TIFF, GeoTIFF, JPEG, GIF, ECW, JPEG2000 and CEOS formats
* RPC and Affine bundle adjustment for bias correction and high-accuracy geopositioning
* Rigorous sensor orientation model for Quickbird, SPOT5 and ALOS PRISM
* Sub-pixel image mensuration
* Various image processing and enhancement features, including automatic feature extraction of edge and point features and texture classification
* Handling of multiple 3D coordinate systems with transformations between various projection and reference systems
* DEM import to enable monoplotting
* Exporting of bias-corrected RPCs for subsequent high-accuracy HRSI applications
* Export of 3D building model data in DXF format
* Processing of airborne laser scanner data
* Handling of 1-, 3- and 4-channel 8-, 11- and 16-bit imagery with multiple channel display and resampling options

Background
==========

Barista was created from a CRCSI research project at The University of Melbourne. Barista’s strength is that it offers easy-to-use, commonly needed spatial information extraction tools which are currently available only in high-end specialist digital photogrammetric workstations. This makes it an ideal tool for practitioners and non-specialists seeking to extract spatial information from HRSI, especially from single images from the Ikonos, Quickbird, SPOT5 and ALOS satellites.

Development setup
=================

* Download and install qt-opensource-windows-x86-msvc2013-5.5.1.exe from https://download.qt.io/archive/qt/5.5/5.5.1/
* Clone BaristaSource and open Barista.sln in Visual Studio 2013
+ Set QTDIR environment variable in visual studio property manager to your QT install location
	* Open Property manager in visual studio from View -> Other Windows -> Property Manager
	* Open Environment_variables_win32 property page under Barista -> Debug | Win32, Under Common Properties -> User Macros set QTDIR variable value to the directory containing bin, lib and include folders. E.g. C:\Qt\Qt5.5.1\5.5\msvc2013
* You can now build the solution and start contributing
	
Installation
============

Download the latest installer for Barista available [here](https://bitbucket.org/crcsi/baristasource/downloads/BaristaSetup3.0.1.exe)

Getting Help
============

The complete user manual for Barista is available in wiki [here](https://bitbucket.org/crcsi/baristasource/wiki/Home)
If you come across any bugs please log an issue, or feel free to fork the project, fix the bug and send a pull request to merge it to the main branch.
You can also join [Barista Users group](https://groups.google.com/forum/#!forum/barista-users) and create new topic for more detailed discussion.

License
=======

Distributed under the BSD license. See LICENSE for more information.
Barista is developed with the help of other opensource projects listed below. Please refer to files named Readme, license or copyright for license terms for these projects. Only the binaries of these projects are included in Barista source code distribution for convenience.

* ANN 1.1.2 - https://www.cs.umd.edu/~mount/ANN/
* Expat 2.0.1 - http://www.libexpat.org/
* GDAL 2.0.2 - http://www.gdal.org/
* jpeg6b - http://www.ijg.org/
* tiff 4.0.6 - http://www.remotesensing.org/libtiff/
* newmat - http://www.robertnz.net/nm10.htm
* OpenCV 2.1.0 - http://opencv.org/
* Qt 5.5.1 - https://www.qt.io/
* VRaniML - https://www.greathill.com/vraniml.php
* xerces 3.1.2 - https://xerces.apache.org/xerces-c/

Barista also uses ERDAS ECW/JP2 Read-only redistributable SDK v5.2.1 software licensed from Intergraph Corporation (“Intergraph”). You must agree to below end user license agreement before you can use Barista, where you refers to you as an individual or organization and your end-user.

* You have acquired a product ("Product") that includes software licensed from Intergraph Corporation (“Intergraph”).  Those installed software products of Intergraph origin, as well as any associated media, printed materials, and "online" or electronic documentation ("Software) are protected by copyright laws and international copyright treaties. The Software is licensed, not sold. 
* If You do not agree to this End User License Agreement ("EULA"), do not use [or download] the Product. If you have paid consideration in return for authorization to use the Product, promptly contact the person from whom You received this Product for instructions on return of the unused Product(s) for a refund. Any use of the Software, including but not limited to use of the Product, will constitute Your agreement to this EULA (or ratification of any previous consent).
* NO WARRANTIES FOR THE SOFTWARE. THE SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS. THE ENTIRE RISK AS TO SATISFACTORY QUALITY, PERFORMANCE AND ACCURACY IS WITH YOU. ALSO, THERE IS NO WARRANTY AGAINST INTERFERENCE WITH YOUR ENJOYMENT OF THE SOFTWARE OR AGAINST INFRINGEMENT. IF YOU HAVE RECEIVED ANY WARRANTIES REGARDING THE PRODUCT OR THE SOFTWARE, THOSE WARRANTIES DO NOT ORIGINATE FROM, AND ARE NOT BINDING ON, INTERGRAPH. 
* NO LIABILITY FOR CERTAIN DAMAGES. EXCEPT AS PROHIBITED BY LAW, INTERGRAPH SHALL HAVE NO LIABILITY FOR ANY INDIRECT, SPECIAL, CONSEQUENTIAL OR INCIDENTAL DAMAGES ARISING FROM OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE SOFTWARE. THIS LIMITATION SHALL APPLY EVEN IF ANY REMEDY FAILS IN ITS ESSENTIAL PURPOSE. 
* Prohibition of Reverse Engineering, Decompilation and Disassembly. You may not reverse engineer, decompile, or disassemble the Software or modifying the Enhanced Compressed Wavelet  (“ECW”)file format in any manner.
* Export Restrictions. You acknowledge that the Software, or any part thereof, or any process or service that is the direct product of the Software is of U.S. origin. You agree to comply with all applicable international and national laws that apply to these products, including the U.S. Export Administration Regulations, as well as end-user, end-use and destination restrictions and embargoes issued by U.S. and other governments having jurisdiction.


Contributing
============

We love contributions, so please feel free to fix bugs, improve things, provide documentation. You SHOULD follow this steps:

* Fork the project
* Make your feature addition or bug fix.
* Send pull request to the specific version branch.