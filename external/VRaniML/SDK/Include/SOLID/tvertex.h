#ifndef __TVERTEX_H3D
#define __TVERTEX_H3D
//-------------------------------------------------------------------------
// Copyright (c) 1997-1998 Great Hill Corporation
// All Rights Reserved.
//----------------------------------------------------------------------
#include "Decls.h"
#include "HalfEdge.h"

/*----------------------------------------------------------------------
CLASS
	vrTVertex

	A point in 2-space pointed at by a vrHalfEdge.

DESCRIPTION
	<ul>
	The vrTVertex class stores the texture vertex information about a vrFace.
	</ul>

NOTES
	<ul>
	<li>You will probably not use this class directly.</li>
	</ul>

EXAMPLE CODE
	<pre>
	// None.
	</pre>

MEMBERS
----------------------------------------------------------------------*/
class LIBInterface vrTVertex : 
	public vrIntrusiveListNode < vrTVertex * >, public SFVec2f 
{
private:
  vrHalfEdge *he;
  Uint32     index;
  SFFloat    scratch;
  Uint32     m_Mark;
  Uint32     Id;

public:
//	ColorData *data;

	//<doc>------------------------------------------------------------
	// <dd>Default Constructor.
	//
	vrTVertex      (void);

	//<doc>------------------------------------------------------------
	// <dd>Copy Constructor.
	//
	vrTVertex      (const vrTVertex& v);

	//<doc>------------------------------------------------------------
	// <dd>Constructor.
	//
	// [in] v: The coordinates of the Tvertex.
	//
	vrTVertex      (const SFVec2f& v);

	//<doc>------------------------------------------------------------
	// <dd>Constructor.
	//
	// [in] s: The solid to which this Tvertex belongs.
	// [in] x,y: The coordinates of the Tvertex.
	//
	vrTVertex      (vrSolid *s, SFFloat x, SFFloat y);

	//<doc>------------------------------------------------------------
	// <dd>Destructor.
	//
	~vrTVertex      (void);

	//<doc>------------------------------------------------------------
	// <dd>Equals operator.
	//
	vrTVertex&  operator=   (const vrTVertex& v);

	//<nodoc>------------------------------------------------------------
	// <dd>Used for copy construction and equals.
	//
	void      RemapPointers(CMapPtrToPtr *map);

	//<doc>------------------------------------------------------------
	// <dd>Calculate the normal surrounding this Tvertex.
	//
//	void        CalcNormal     (void);

	//<doc>------------------------------------------------------------
	// <dd>Set the texture coordinate for this Tvertex.
	//
	// [in] coord: The texture coordinate.
	//
//	void        SetTexCoord    (const SFVec2f& coord);

	//<doc>------------------------------------------------------------
	// <dd>Set the color for this Tvertex.
	//
	// [in] color: The color.
	//
//	void        SetColor       (const SFColor& color);

	//<doc>------------------------------------------------------------
	// <dd>Set the normal for this Tvertex.
	//
	// [in] normal: The normal.
	//
//	void        SetNormal      (const SFVec3f& normal);

	//<doc>------------------------------------------------------------
	// <dd>Set the HE for this Tvertex.
	//
	// [in] he: The halfedge.
	//
  void        SetHe          (vrHalfEdge *he);

	//<doc>------------------------------------------------------------
	// <dd>Set the index for this Tvertex.
	//
	// [in] i: The index.
	//
  void        SetIndex       (Uint32 i);

	//<doc>------------------------------------------------------------
	// <dd>Set the ID for this Tvertex.
	//
	// [in] i: The id.
	//
  void        SetId          (Uint32 i);

	//<doc>------------------------------------------------------------
	// <dd>Set the scratch value for this Tvertex.
	//
	// [in] f: The value.
	//
//  void        SetScratch     (SFFloat f);

	//<doc>------------------------------------------------------------
	// <dd>Set the mark for this Tvertex.
	//
	// [in] mark: The mark.
	//
  void        SetMark        (Uint32 mark);

	//<doc>------------------------------------------------------------
	// <dd>Set the coordinates for this Tvertex.
	//
	// [in] pt: The coordinate.
	//
  void        SetLocation    (const SFVec2f& pt);

	//<doc>------------------------------------------------------------
	// <dd>Return the texture coordinates for this Tvertex.
	//
	// [in] def: Default value if this Tvertex has no texture coord.
	//
//	SFVec2f     GetTextureCoord(const SFVec2f& def) const;

	//<doc>------------------------------------------------------------
	// <dd>Return the color for this Tvertex.
	//
	// [in] def: Default value if this Tvertex has no color.
	//
//	SFColor     GetColor       (const SFColor& def) const;

	//<doc>------------------------------------------------------------
	// <dd>Return the normal for this Tvertex.
	//
	// [in] def: Default value if this Tvertex has no normal.
	//
//	SFVec3f     GetNormal      (const SFVec3f& def) const;

	//<doc>------------------------------------------------------------
	// <dd>Return the half edge for this Tvertex.
	//
  vrHalfEdge *GetHe          (void)        const;

	//<doc>------------------------------------------------------------
	// <dd>Return the index for this Tvertex.
	//
  Uint32      GetIndex       (void)        const;

	//<doc>------------------------------------------------------------
	// <dd>Return the Id for this Tvertex.
	//
  Uint32      GetId          (void)        const;

	//<doc>------------------------------------------------------------
	// <dd>Return the scratch value for this Tvertex.
	//
//  SFFloat     GetScratch     (void)        const;

	//<doc>------------------------------------------------------------
	// <dd>Return the mark value for this Tvertex.
	//
  Uint32      GetMark        (void)        const;

	//<doc>------------------------------------------------------------
	// <dd>Returns TRUE if this Tvertex is marked with 'mark'.
	//
	// [in] mark: The mark to check.
	//
  SFBool      IsMarked       (Uint32 mark) const;

	//<doc>------------------------------------------------------------
	// <dd>Return the vrEdge that this Tvertex is a part of (he->edge).
	//
  vrEdge     *GetEdge        (void)        const;
  
#ifdef _DEBUG
	//<doc>------------------------------------------------------------
	// <dd>Debugging support.  Display the contents of the vrEdge to the dc.
	//
	// [in] dc: The vrDumpContext to which to dump this edge.
	//
  void      Show           (vrDumpContext& dc)            const;

	//<doc>------------------------------------------------------------
	// <dd>Debugging support.  Verify the edge.
	//
  void      Verify         (void)            const;

	//<doc>------------------------------------------------------------
	// <dd>Debugging support.  Write the neighborhood of this Tvertex.
	//
	void        WriteNeighborhood(void);
#endif
};
typedef vrIntrusiveList<vrTVertex *> vrTVertexList;
typedef vrArrayBase<vrTVertex*>      vrTVertexArray;
  
//----------------------------------------------------------------------
inline vrHalfEdge *vrTVertex::GetHe(void) const
{
  return he;
}

inline Uint32 vrTVertex::GetIndex(void) const
{
  return index;
}

inline Uint32 vrTVertex::GetId(void) const
{
  return Id;
}
/*
inline SFFloat vrTVertex::GetScratch(void) const
{
  return scratch;
}
*/
inline Uint32 vrTVertex::GetMark(void) const
{
  return m_Mark;
}

inline SFBool vrTVertex::IsMarked(Uint32 m) const
{
  return (m_Mark == m);
}

inline vrEdge *vrTVertex::GetEdge(void) const
{
  ASSERT(he);
  return he->GetEdge();
}

//----------------------------------------------------------------------
inline void vrTVertex::SetHe(vrHalfEdge *h)
{
  /* may be NULL for example see lkev */
  he = h;
}

inline void vrTVertex::SetIndex(Uint32 i)
{
  index = i;
}

inline void vrTVertex::SetId(Uint32 i)
{
  Id = i;
}
/*
inline void vrTVertex::SetScratch(SFFloat f)
{
  scratch = f;
}
*/
inline void vrTVertex::SetMark(Uint32 m)
{
  ASSERT(m == DELETED || 
					m == BRANDNEW || 
					m == JUSTON || 
					m == VISITED || 
					m == NOTKNOWN);
  m_Mark = m;
}

inline void vrTVertex::SetLocation(const SFVec2f& pt)
{
  x = pt.x; 
	y = pt.y; 
}

#endif
