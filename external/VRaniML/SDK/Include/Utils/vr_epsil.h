#ifndef VR_EPSIL_HPP
#define VR_EPSIL_HPP

#include <math.h>

 class UL_Interface VR_epsilon
 {
//=============================================================================
//================================== data =====================================

   protected:

    static double d_epsilonLinear_;

    static double d_epsilonArea_;

    static double d_epsilonVolume_;


//=============================================================================

   public:

//=============================================================================
//============================== Constructor ==================================

    VR_epsilon() {};


//=============================================================================
//=============================== Destructor ==================================

    ~VR_epsilon() {};


//=============================================================================
//=============================== query data ==================================

    static double d_epsilonLinear() { return d_epsilonLinear_; };

    static double d_epsilonArea() { return d_epsilonArea_; };

    static double d_epsilonVolume() { return d_epsilonVolume_; };


//=============================================================================
//=============================== assignment ==================================

    static void v_epsilonLinear(double Id_epsLin)
    { d_epsilonLinear_ = fabs(Id_epsLin); };

    static void v_epsilonArea(double Id_epsArea)
    { d_epsilonArea_ = fabs(Id_epsArea); };

    static void v_epsilonAreaByAngle(double Id_epsAngle);

    static void v_epsilonVolume(double Id_epsVol)
    { d_epsilonVolume_ = fabs(Id_epsVol); };


//=============================================================================
 };


#endif
