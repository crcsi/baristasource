#ifndef VIMAGEDLG_H
#define VIMAGEDLG_H

#include <QDialog>
#include "ui_VImageDlg.h"
#include "AdjustmentDlg.h"

class CVImage;

class Bui_VImageDlg : public CAdjustmentDlg, public Ui_VImageDlgClass
{
	Q_OBJECT

public:
	Bui_VImageDlg(CVImage* image,QWidget *parent = 0);
	~Bui_VImageDlg();


	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString() {return "Image dialog";};

	virtual void elementAdded(CLabel element=""){};
	virtual void elementDeleted(CLabel element);
	void setVImage(CVImage* newVImage);
	virtual void cleanUp();
signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

public slots:
	void sensorModelStatusChanged(bool b);
	void pointObsDlgStatusChanged(bool b);
	void pointResDlgStatusChanged(bool b);
	void tWCurrentChanged( int index );

private slots:
	virtual void elementsChangedSlot(CLabel element);

protected:

	void initSensorModelDlg();
	void init2DObsPointDlg();
	void init2DResPointDlg();


	CVImage* image;
	CAdjustmentDlg* sensorModelDlg;
	CAdjustmentDlg* pointObsDlg;
	CAdjustmentDlg* pointResDlg;

	int oldTab;
};

#endif // VIMAGEDLG_H
