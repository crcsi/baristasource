#ifndef __CPolyLineDataModel__
#define __CPolyLineDataModel__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "ObjectDataModel.h"
#include "GenericPolyLineArray.h"


class CPolyLineDataModel : public CObjectDataModel
{
	Q_OBJECT
public:
	CPolyLineDataModel(CGenericPolyLineArray* polyLineArray,ViewParameter viewParameter);
	~CPolyLineDataModel(void);


	virtual int getNameColumn() const {return  0;};
	virtual QStringList guessHeader();
	virtual QStringList guessDecimals();	

	virtual GenericPolyLine* getPolyLine(int index);
	virtual void requestWindowTitle();
	virtual int getDimension() const;


	virtual void elementsChanged(CLabel element="");

	virtual int rowCount(const QModelIndex& parent) const;
	virtual int columnCount(const QModelIndex& parent) const;
	virtual QVariant data(const QModelIndex &index, int role) const;
	virtual QVariant headerData(int section,Qt::Orientation orientation, int role) const;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const;
	virtual bool setData(const QModelIndex &index, const QVariant &value,int role = Qt::EditRole);
	virtual bool removeRows (int row,int count,const QModelIndex & parent = QModelIndex());

	virtual void rightMouseButtonReleased(const QModelIndexList& list,vector<QAction*>& actions);
	virtual CReferenceSystem* getReferenceSystem() {return this->polyLineArray->getReferenceSystem();};

public slots:
	void removefromTable();
	void XYZPolyLinesExtractFeatures();
	void XYZPolyLinesSnakeAction();

protected:
	CGenericPolyLineArray* polyLineArray;
	

};
#endif