#ifndef __CPushBroomScannerTable__
#define __CPushBroomScannerTable__

#include "BaristaTable.h"

class CPushBroomScanner;

class CPushBroomScannerTable :	public CBaristaTable
{
    Q_OBJECT // needed to connect signals / slots
public:
	CPushBroomScannerTable(CPushBroomScanner* pushBroomScanner, QWidget* parent);
	~CPushBroomScannerTable(void);

    void setPushBroomScanner(CPushBroomScanner* pushBroomScanner);
    CPushBroomScanner* getPushBroomScanner() {return this->pbs;	}
    
	virtual void initTable();
	virtual void elementAdded(CLabel element){};
	virtual void elementDeleted(CLabel element){};

    void closeEvent ( QCloseEvent * e );

protected:
    CPushBroomScanner* pbs;
};
#endif