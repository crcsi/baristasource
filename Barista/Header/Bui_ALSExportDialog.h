#ifndef BUI_ALSEXPORTDIALOG_H
#define BUI_ALSEXPORTDIALOG_H

#include <qdialog.h>
#include "ui_ALSExportdialog.h"
#include "Filename.h"

class CVALS;

class bui_ALSExportDialog :	public QDialog, public Ui_ExportALSDialog
{Q_OBJECT

  protected:

	  CVALS *ALS;

	  CFileName filename;

	  bool success;

	  bool exportTime;

	  bool exportIntensity;

	  int pulse;

  public:

	  bui_ALSExportDialog(CVALS *als);

	  ~bui_ALSExportDialog();

	  CFileName getFileName() const { return this->filename; }

	  bool getSuccess() const { return this->success; };

	  int getPulse() const { return this->pulse; };

  public slots:

	  void PulseSelected();

	  void FormatSelected();
	
	  void OutputFileSelected();

	  void OK();

	  void Cancel();
	  
	  void Help();

  protected:

	  void setFormat();
	  void setPulseMode();

  private: 

	  void init();

	  void destroy();

};

#endif // BUI_ALSEXPORTDIALOG_H