#ifndef BUI_BUILDINGDXF_H
#define BUI_BUILDINGDXF_H

#include <QDialog>
#include "ui_Bui_BuildingDXF.h"
#include "Filename.h"

class Bui_BuildingDXF : public QDialog, public Ui_Bui_BuildingDXFClass
{
	Q_OBJECT

public:
	Bui_BuildingDXF(const CCharString &filenameBase = "Buildings", QWidget *parent = 0);
	~Bui_BuildingDXF();

	bool getSuccess() const {return this->success;};
	CFileName getFileName() const { return this->iFileName; };
	bool getRelativeHeights() const { return this->relativeHeights; };

public slots:
	void CancelReleased();
	void OkReleased();
	void HelpReleased();
	void BrowseReleased();
	void ModeButtonChanged();

protected:

	void applySettings();

	void checkAvailabilityOfSettings();



private:
	CFileName iFileName;
	CCharString FilenameBase;
	bool relativeHeights;
	bool success;
};

#endif // BUI_NEWPROJECT_H
