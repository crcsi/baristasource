#ifndef __COpenGLObject__
#define __COpenGLObject__


#include <QGLWidget>
#include "3DPoint.h"
#include "2DPoint.h"
#include "LookupTable.h"

#include <QtWidgets\QTextEdit>
#include <QTime>

class CObsPointArray;
class CVDEM;
class CVALS;
class CALSPoint;
class CXYZPolyLineArray;
class CXYZPointArray;

#ifdef WIN32
class CXYZPointArray;
class CBuildingArray;
#endif

typedef struct {
	int level;
	int lowLevel;
	C3DPoint ll;
	C3DPoint lr;
	C3DPoint ul;
	C3DPoint ur;
	C3DPoint pmid;
	GLuint glList;
	int elementcount;
	bool hasList;
}TileData;

typedef struct {
	vector<TileData> tiles;
	int tilesacross;
	int tilesdown;
	C2DPoint pointdist;
}TiledElement;

class COpenGLObject : public QGLWidget
{
    Q_OBJECT
public:
	COpenGLObject(QWidget * parent = 0);
	~COpenGLObject(void);


	void wheelEvent ( QWheelEvent * e );
	void keyReleaseEvent (QKeyEvent *e);

	void resetView();
	void refreshView();


protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

    void setXRotation(double angle);
    void setYRotation(double angle);
    void setZRotation(double angle);
	void normalizeAngle(double *angle);

	void makePoints(CXYZPointArray* pointArray, QColor qcolor);
	void makePointLabels(CXYZPointArray* pointArray, QColor qcolor);
	void makeDEM(CVDEM* vdem, QColor qcolor);
	void makeALS(CVALS* vals, QColor qcolor, int level = 0); 
	int refreshALSTile(unsigned int valsindex, int row, int col, GLuint list, int level);
	int refreshDEMTile(unsigned int demindex, int row, int col, GLuint list, int level); 

	void makeLines(CXYZPolyLineArray* lines, QColor qcolor); 

#ifdef WIN32	
	void makeBuildings(CBuildingArray* buildings, QColor qcolor); 
#endif

	void makeObjects();

	void makeAxes();

	void setupViewingTransform();

	bool drawLabels();

	bool getExtends();

	bool checkTileLevel(C3DPoint ul, C3DPoint ll, C3DPoint lr, C3DPoint ur, C3DPoint tmiddle, C2DPoint dxy, int &level, int lowlevel, double min);

	bool toModel(const CPointBase& point, GLdouble &xmodel, GLdouble &ymodel, GLdouble &zmodel);
	bool toScreen(const CPointBase& point, GLdouble &xscreen, GLdouble &yscreen, GLdouble &zscreen);

	bool areaOnScreen(C3DPoint ul, C3DPoint ll, C3DPoint lr, C3DPoint ur);

	QDialog* dlg;
	QTextEdit* textBrowser;

protected:
	GLuint geoList;
	GLuint monoList;
	GLuint axesList;
	GLuint lineList;
	GLuint buildingList;

	vector<GLuint> xyzLists;
	vector<TiledElement> alsList;
	vector<TiledElement> demList;
	GLuint xyzListcount;

	// view direction
	C3DPoint rot;
	C3DPoint pos;

	// extends
	C3DPoint min;
	C3DPoint max;
	C3DPoint dim;

	C3DPoint center;

	int nElements;
	int nTiles;
	int nPaints;


	int sum0;
	int sum1;
	int sum2;
	int sum3;
	int sum4;
	int sum5;

	double testvalue1;
	double testvalue2;
	double testvalue3;
	double testvalue4;

	CLookupTable lut;

	GLfloat			aspect;
	GLdouble		FOV;
	GLdouble		zNearClip;
	GLdouble		zFarClip;
	GLdouble		zEye;
	GLfloat X_SCREEN[3], Y_SCREEN[3], Z_SCREEN[3];
    GLfloat RotMat[16];
	bool bRotationChanged;
	GLfloat scale;

    QPoint lastPos;
	bool bDrawLabels;

	GLdouble pointsize;

	bool onButton;

	vector<C3DPoint> points;
	CALSPoint** alspoints;
	int capacity;

};
#endif


