/**
 * Copyright:    Copyright (c) Petra Helmholz<p>
 * Company:      University of Melbourne<p>
 * @author PetraH
 * @version 1.0
 */

#ifndef __CMonoplottingSnakeHandler__
#define __CMonoplottingSnakeHandler__
#include "MonoplottingLineHandler.h"

class CVBase;

/*!
 * \brief
 * Handler for the monoplotting of snakes, derivative from CMonoplottingLineHandler.
 *
 * Compared to the CMonoplottingLineHandler just the function keyReleaseEvent were changed.
 * After a line (initialization points for the snake calculation) were monoplotted, the roi
 * around these points is calculated, and it is checked if the roi is too large or ok.\n\n
 * By pressing of the key C to finish the digitalization process the snake will be
 * close, by pressing of F open. This information is given to the Snake Dialog.
 * The Snake Dialog starts the calculation. If the snake calculation were done the snake
 * points will be plotted.
 */

class CMonoplottingSnakeHandler : public CMonoplottingLineHandler
{
public:
	CMonoplottingSnakeHandler(void);
	~CMonoplottingSnakeHandler(void);

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);
	
protected:

};


#endif