#include <qobject.h>
#include "ui_ChangeXYZSigmasDlg.h"

class bui_ChangeXYZSigmasDlg : 	public QDialog, public Ui_ChangeXYZSigmasDlg
{Q_OBJECT
public:
	bui_ChangeXYZSigmasDlg(void);
	double sx;
	double sy;
	double sz;
	bool makechanges;
public:
	~bui_ChangeXYZSigmasDlg(void);
	public slots:
	void setSX( double sx );
    void setSY( double sy );
    void setSZ( double sz );
    double getSX();
    double getSY();
    double getSZ();
    void Cancel_released();
    void OKButton_released();
    bool getmakeChanges();
};
