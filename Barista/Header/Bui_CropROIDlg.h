#ifndef BUI_CROPROIDLG_H
#define BUI_CROPROIDLG_H

#include <QDialog>
#include "ui_CropROIDlg.h"
#include "GDALFile.h"

#include "Filename.h"

class CBaristaView;
class Bui_GDALExportDlg;
class CVImage;
class CVDEM;

class Bui_CropROIDlg : public QDialog, public Ui_CropROIDlg
{
	Q_OBJECT

public:
	Bui_CropROIDlg(CBaristaView *view, const char *title, const char* helpfile,
							   bool hasRPCs,bool hasPushBroom,bool hasTFW,bool hasPoints,bool hasAffine);
	~Bui_CropROIDlg();
	
	void initDlg();

	void setSpinBoxes();
	void setPixelSpinBoxes();
	void setMetricSpinBoxes();
	void updateROI();
	void checkSetup();

	void setMetricValues();
	void setPixelValues();

	void getROIfromMetrictoPixel();

	void blockSpinBoxSignals(bool block);
	void setSpinBoxDecimals(int decimals);
	void setSpinBoxStep(double step);


public slots:
	
	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();

	void rbPixelReleased();
	void rbMetricReleased();

	void spinBoxULXChanged( double newulx);
	void spinBoxULYChanged( double newuly);
	void spinBoxLRXChanged( double newlrx);
	void spinBoxLRYChanged( double newlry);
	void spinBoxHeightChanged( double newheight);
	void spinBoxWidthChanged( double newwidth);

	void OpenOutImage();
	
	bool getSuccsess()const {return this->success;};
protected:
	CBaristaView* view;

	CVImage* oldVImage;
	CVImage* newVImage;
	
	CVDEM* oldVDem;
	CVDEM* newVDem;

	Bui_GDALExportDlg* dlg;

	bool success;
	double ulx;
	double uly;
	double lrx;
	double lry;
	double maxx;
	double maxy;
	double width;
	double height;
	double maxWidth;
	double maxHeight;

	double metriculx;
	double metriculy;
	double metriclrx;
	double metriclry;
	double metricminx;
	double metricminy;
	double metricwidth;
	double metricheight;
	double metricmaxx;
	double metricmaxy;
	double metricmaxwidth;
	double metricmaxheight;

	int ndigits;

	bool hasRPCs;
	bool hasPushBroom;
	bool hasTFW;
	bool hasPoints;
	bool hasAffine;
	const char *title;
	const char* helpfile;

	CFileName cropName;


};

#endif // BUI_CROPROIDLG_H
