#ifndef BUI_CHANNELSWITCHBOX_H
#define BUI_CHANNELSWITCHBOX_H
#include <qdialog.h>
#include "ui_ChannelSwitchBox.h"
#include "HugeImage.h"


#include <QVector>
#include <QGridLayout>


class CHugeImage;
class CBaristaView;
class CTransferFunction;
class bui_ChannelSwitchBox;
class Bui_WallisFilterDlg;


#include <QMap>
#include <QPixmap>
#include <QVector>
#include <QWidget>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QTableView>
#include "histogram.h"
class QToolButton;
class PlotSettings;

class CHistogramPlot : public QWidget
{
    Q_OBJECT

public:
    CHistogramPlot(QWidget *parent = 0);
	~CHistogramPlot();

    void setPlotSettings(const PlotSettings &settings);
    void setCurveData(int id, QColor color,const CHistogram* histogram);
	void setTransferFunction(CTransferFunction* transferFunction,bool resetView,const CHistogram* histogram);
	void updateColor(int id,QColor color);
    void clearCurve(int id);
    QSize minimumSizeHint() const;
    QSize sizeHint() const;

	enum { Red = 0, Green = 1, Blue = 2, Grey = 3, Alpha = 4, Intensity = 5};


public slots:
    void zoomIn();
    void zoomOut();
	void sB_SmoothChanged(int value);

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    void updateRubberBandRegion();
    void refreshPixmap();
    void drawGrid(QPainter *painter);
    void drawCurves(QPainter *painter);

	//void createRGBCurves(QPolygonF& polylineRed,QPolygonF& polylineGreen,QPolygonF& polylineBlue,QRect& rect);
	void createCurve(int id,QPolygonF& polyline,QRect& rect);
	void createDirectTransferCurve(const vector<double> &data,QPolygonF& polyline,QRect& rect);

	void clearView();
	
	void computeCurves();

	void computeCurrentRGBData();
	void computeCurve(int id_curveData);

	QRect computeRect();

    enum { MarginLeft = 50,MarginRight = 20,MarginTop = 10,MarginBottom = 20 };
	

    QToolButton *zoomInButton;
    QToolButton *zoomOutButton;
    
	int rgbIndex[3];
	QMap<int, vector<double> > curveData;
	QMap<int, vector<double> > currentData;
    QMap<int, QColor> curveColor;
	double originalMin;
	double originalMax;

	QVector<PlotSettings> zoomStack;


    int curZoom;
    bool rubberBandIsShown;
    QRect rubberBandRect;
    QPixmap pixmap;

	CTransferFunction* transferFunction;
	bool mouseDrag;
	QPoint lastPos;

	int smooth;

};



class PlotSettings
{
public:
    PlotSettings();

    void scroll(double dx, double dy);
    void adjust();
    double spanX() const { return maxX - minX; }
    double spanY() const { return maxY - minY; }

    double minX;
    double maxX;
    int numXTicks;
    double minY;
    double maxY;
    int numYTicks;

private:
    static void adjustAxis(double &min, double &max, int &numTicks);
};




class CLUTDataModel : public  QAbstractTableModel
{
public:
	CLUTDataModel(CLookupTable* lut);


	virtual int rowCount(const QModelIndex& parent) const;
	virtual int columnCount(const QModelIndex& parent) const;
	virtual QVariant data(const QModelIndex &index, int role) const;
	virtual QVariant headerData(int section,Qt::Orientation orientation, int role) const;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const;
	virtual bool setData(const QModelIndex &index, const QVariant &value,int role = Qt::EditRole);
	virtual bool removeRows (int row,int count,const QModelIndex & parent = QModelIndex());

	void setLUT(CLookupTable* lut);

protected:

	vector<int> lutData;
	CLookupTable* lut;

};



class CLUTTableView : public QTableView
{	
	Q_OBJECT
public:
	CLUTTableView(QWidget* parent,int bpp);

protected:

	virtual void mouseDoubleClickEvent ( QMouseEvent * event ); 
	virtual void mouseReleaseEvent ( QMouseEvent * mouseEvent );

};




class bui_ChannelSwitchBox :	public QDialog , public Ui_ChannelSwitchBox 
{
Q_OBJECT

protected:
	int red;
	int green;
	int blue;
	int Nchannels;
	eResamplingType resamplingType;

	int old_red;
	int old_green;
	int old_blue;
	eResamplingType old_resamplingType;


    CHugeImage* himage;
	CBaristaView *bview;
	QMap<CTransferFunction::Type,CTransferFunction*> transferFunctions;
	CTransferFunction::Type selectedTransferFunction;

	CTransferFunction* transferFunction_save;
	vector<CHistogram*> localHistograms;
	CHistogram localIntensityHistogram;
	int Brightness_save;

	CHistogramPlot* plot;
	CLUTDataModel* lutModel;
	QSortFilterProxyModel* proxyModel;
	CLUTTableView* tV_LUT;

	Bui_WallisFilterDlg* wallisDlg;

	float minValue;
	float maxValue;
	float centreValue;
	float sigmaValue;

	bool success;

public:
	bui_ChannelSwitchBox(CBaristaView* view);
	~bui_ChannelSwitchBox();


public slots:
	void SetImageChannels_released();
	void NearestNeighbourChanged();
	void BilinearChanged();
	void BicubicChanged();
	void Cancel_released();
	void HelpReleased();
	void lW_RedRowChanged(int currentRow);
	void lW_GreenRowChanged(int currentRow); 
	void lW_BlueRowChanged(int currentRow); 

	void rB_GreyValueReleased();
	void rB_LUTReleased();
	void setMax(double max);
	void setMin(double min);
	void cB_TransferCurrentIndexChanged(int);

	void sBMinValueChanged(double value);
	void sBMaxValueChanged(double value);

	void sBCentreValueChanged(double value);
	void sBSigmaValueChanged(double value);
	
	void lutDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight); 
	void loadLUTReleased();

	void pBDefaultLUTReleased();

protected:

	void init(CHugeImage* himage);

	void initHistogramView();
	void initLUTView();
	void initWallisFilterView();



	void initTransferBox();
	void initChannelBoxes();
	void initResampleButtons();

	void updateView();
	void updateTransferFunctionParameter();

	void selectTransferFunction(CTransferFunction::Type type);
	int getIndexOfTransferFunction(CTransferFunction::Type type,int bands);

	virtual void closeEvent(QCloseEvent* event);

};





#endif //CHANNELSWITCHBOX
