#ifndef BUI_IMPORTALS_H
#define BUI_IMPORTALS_H

#include <qdialog.h>
#include "ui_ImportALS.h"
#include "ALSData.h"
#include "Filename.h"
#include "ReferenceSystem.h"


class bui_ImportALSDialog : 	public QDialog , public Ui_ImportALSDialog
{Q_OBJECT

  protected:

	  bool filenameSelected;

	  bool hasStrips;

	  bool success;

	  vector<CFileName> inputFilenames;

	  eALSFormatType eFormat;

	  eALSPulseModeType ePulseMode;

	  CReferenceSystem refsys;

	  C2DPoint offset;

	  C2DPoint delta;

  public:
	
	  bui_ImportALSDialog ();

	  ~bui_ImportALSDialog();

	  bool hasFilename() const { return this->filenameSelected; };

	  bool getHasStrips() const { return this->hasStrips; };
	
	  bool getSuccess() const { return this->success; };

	  vector<CFileName> getInputFilenames() const { return this->inputFilenames; };

	  eALSFormatType getFormat() const { return this->eFormat; };

	  eALSPulseModeType getPulseMode() const { return this->ePulseMode; };

	  CReferenceSystem getRefsys() const { return this->refsys; };

	  C2DPoint getOffset() const { return this->offset; };

	  C2DPoint getDelta() const { return this->delta; };

  public slots:

	  void onFileSelected();

	  void onFormatChanged();

	  void onPulseModeChanged();

	  void onOffsetChanged();

	  void onImportModeChanged();

	  void onDeltaChanged();

	  void OK();

	  void Cancel();

	  void Help();

  protected: 

	  void init();

	  void setDataToGUI();

	  void destroy();

};

#endif // BUI_IMPORTALS_H