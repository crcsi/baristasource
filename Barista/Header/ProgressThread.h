#ifndef __CProgressThread__
#define __CProgressThread__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

/*
Base class for all Threads
*/


#include "qthread.h"
#include "Filename.h"
#include "2DPoint.h"
#include "m_FeatExtract.h"
#include "SnakeExtraction.h"
#include "ALSData.h"
#include "GenericPointCloud.h"

#include <vector>

class CHugeImage;
class CXYPolyLine;
class CImageFile;
class COrthoImageGenerator;
class bui_ImageRegistrationDialog;
class bui_PanDialog;
class CGDALFile;
class CSatelliteDataImporter;
class CSatelliteDataReader;
class CALSDataSet;
class CVDEM;
class CDEM;
class CVImage;
class CVALS;
class CBaristaProject;
class CBaristaBundleHelper;
class CASCIIDEMFile;
class CGCPDataBasePoint;
class CMatchingParameters;
class bui_ALSMergeDialog;

#ifdef WIN32
class bui_BuildingDialog;
class bui_BuildingChangeDialog;
#endif

class bui_ALSFilterDialog;
class CXYZPointArray;
class Bui_DEMMatchingDlg;
class CImageMerger;
class bui_GCPMatchingDialog;
class CImageImportData;
class CImageImporter;
class CBandMerger;
class CImageExporter;
class CBuilding;
class CObservationHandler;
class CBuildingReconstructor;
class bui_BuildingsDetector;
class CGenerateEpipolarProject;

class CProgressThread : public QThread
{
public:
	CProgressThread(void);
	virtual ~CProgressThread(void);

	virtual void run() = 0;
	virtual bool getSuccess() { return this->success;};

protected:

	bool success;

};

/* #####################################################################*/

class COrthoProcessingThread : public CProgressThread
{
  public:
  
	  COrthoProcessingThread(COrthoImageGenerator* I_orthogenerator);

	  virtual ~COrthoProcessingThread();

	  virtual void run();

  protected:

	  COrthoImageGenerator* orthogenerator;

};

/* #####################################################################*/

class CPansharpenProcessingThread : public CProgressThread
{
  public:
  
	  CPansharpenProcessingThread(bui_PanDialog* panDLG);

	  virtual ~CPansharpenProcessingThread();

	  virtual void run();

  protected:

	  bui_PanDialog* panDialog;

};

/* #####################################################################*/

class CImgRegistrationProcessingThread : public CProgressThread
{
  public:
  
	  CImgRegistrationProcessingThread(bui_ImageRegistrationDialog* registrationmatcher);

	  virtual ~CImgRegistrationProcessingThread();

	  virtual void run();

  protected:

	  bui_ImageRegistrationDialog* registrationMatcher;

};


/* #####################################################################*/

class CDEMProcessingThread : public CProgressThread
{
  public:
  
	  CDEMProcessingThread(Bui_DEMMatchingDlg* demmatcher);

	  virtual ~CDEMProcessingThread();

	  virtual void run();

  protected:

	  Bui_DEMMatchingDlg* demMatcher;

};


/* #####################################################################*/

class CGCPMatchingThread : public CProgressThread
{
  public:
  
	  CGCPMatchingThread(bui_GCPMatchingDialog* gcpmatcher) : gcpMatcher(gcpmatcher) {};

	  virtual ~CGCPMatchingThread() { };

	  virtual void run();

  protected:

	  bui_GCPMatchingDialog* gcpMatcher;

};

/* #####################################################################*/

class CGDALExportThread  : public CProgressThread
{
public:
	CGDALExportThread(CImageExporter* imageExporter);
	~CGDALExportThread(void);
	virtual void run();


protected:
	CImageExporter* imageExporter;
};



/* #####################################################################*/

class CSatelliteDataReaderThread : public CProgressThread
{
public:
    CSatelliteDataReaderThread(CSatelliteDataImporter* importer,CFileName filename);
	~CSatelliteDataReaderThread(void){};
	virtual void run(); 

protected:
	CSatelliteDataImporter* importer;
	CFileName filename;
};


/* #####################################################################*/

class CALSDataReaderThread : public CProgressThread
{
public:
    CALSDataReaderThread(CALSDataSet *ALS, const char *Filename, eALSFormatType eformat, 
						 eALSPulseModeType epulseType, const C2DPoint &Offset);

	~CALSDataReaderThread(){};
	virtual void run(); 

protected:

	CALSDataSet *als;

	CFileName filename;

	eALSFormatType eFormat; 
	eALSPulseModeType ePulseType;
	C2DPoint offset;

};


/* #####################################################################*/

class COrbitAdjustmentThread : public CProgressThread
{
public:
    COrbitAdjustmentThread(CSatelliteDataImporter* importer,bool adjustPath,bool adjustAttitudes);

	~COrbitAdjustmentThread(){};
	virtual void run(); 

protected:

	CSatelliteDataImporter* importer;
	bool adjustPath;
	bool adjustAttitudes;

};




/* #####################################################################*/

class CALSDataInterpolatorThread : public CProgressThread
{
public:
    CALSDataInterpolatorThread(CXYZPointArray *Points, const char *fileName, CVDEM *DEM, 
							   int Neighbourhood, eInterpolationMethod Method,
							   const C2DPoint &Pmin, const C2DPoint &Pmax, const C2DPoint &grid,
							   double maxdist);

    CALSDataInterpolatorThread(CALSDataSet *ALS, const char *fileName, CVDEM *DEM, int Pulse,
							   int Level, int Neighbourhood, eInterpolationMethod Method,
							   const C2DPoint &Pmin, const C2DPoint &Pmax, const C2DPoint &grid,
							   double maxdist);

	CALSDataInterpolatorThread(CVALS* VALS, const char *fileName, CVImage *IMG, int Pulse,
							   int Level, int Neighbourhood, eInterpolationMethod Method,
							   const C2DPoint &Pmin, const C2DPoint &Pmax, const C2DPoint &grid,
							   double maxdist);

	~CALSDataInterpolatorThread(){};
	virtual void run(); 

protected:

	CALSDataSet *als;
	CFileName filename;	
	CVDEM *vdem;
	CVImage *vimage;
	CVALS* vals;
	CXYZPointArray *points;

	int pulse;	
	int level;
	int neighbourhood;
	double maxDist;
	eInterpolationMethod method;
	C2DPoint pmin; 
	C2DPoint pmax; 
	C2DPoint gridS; 

};


/* #####################################################################*/

class CALSDataMergeThread : public CProgressThread
{
public:
    CALSDataMergeThread(bui_ALSMergeDialog *ALSMrgDlg);

	~CALSDataMergeThread(){};
	virtual void run(); 

protected:

	bui_ALSMergeDialog  *ALSMergeDlg;
};


/* #####################################################################*/

class CBaristaProjectReaderThread : public CProgressThread
{
public:
    CBaristaProjectReaderThread(CBaristaProject *project, const char *Filename);

	~CBaristaProjectReaderThread(){};
	virtual void run(); 

protected:

	CBaristaProject *project;

	CFileName filename;

};



/* #####################################################################*/

class CRunBundleThread : public CProgressThread
{
public:
    CRunBundleThread(CBaristaBundleHelper *bundleHelper,bool forwardOnly, bool Robust, bool OmitMarked);

	~CRunBundleThread(){};
	virtual void run(); 

protected:

	CBaristaBundleHelper *bundleHelper;

	bool robust;

	bool omitMarked;

	bool forwardOnly;
};


/* #####################################################################*/

class CASCIIConvertFromXYZThread : public CProgressThread
{
public:
    CASCIIConvertFromXYZThread(CASCIIDEMFile *asciiDEM,const char *Filename);

	~CASCIIConvertFromXYZThread(){};
	virtual void run(); 

protected:
	CASCIIDEMFile *asciiDEM;
	CFileName filename;	
};



/* #####################################################################*/

class CFeatureExtractionThread : public CProgressThread, public CProgressHandler
{
public:
    CFeatureExtractionThread(CImageBuffer *imgbuf, CXYPolyLine *boundary, CXYPointArray *xypts, 
		                     CXYContourArray *xycont, CXYPolyLineArray *xyedges, 
							 CLabelImage *Labels, FeatureExtractionParameters Parameter);

	~CFeatureExtractionThread(){};
	virtual void run(); 

protected:
	CImageBuffer *img;

	CXYPolyLine *boundaryPoly;

	CXYPointArray *points;
	CXYContourArray *contours;
	CXYPolyLineArray *edges;
	CLabelImage *labels;

	FeatureExtractionParameters parameter;
};

/* #####################################################################*/

class CSnakeExtractionThread : public CProgressThread, public CProgressHandler
{
public:
    CSnakeExtractionThread(CImageBuffer *imgbuf, CXYPointArray *Start, SnakeExtractionParameters Parameter);
	~CSnakeExtractionThread(){};
	virtual void run(); 

protected:
	CImageBuffer *image;
	CXYPointArray *startPoints;
		
	SnakeExtractionParameters parameter;
};

#ifdef WIN32

/* #####################################################################*/

class CBuildingDetectionThread : public CProgressThread
{
  public:
    CBuildingDetectionThread(bui_BuildingDialog *DLG);

	~CBuildingDetectionThread(){};
	virtual void run(); 

  protected:
	bui_BuildingDialog *dlg;
};

class CBuildingChangeThread : public CProgressThread
{
  public:
    CBuildingChangeThread(bui_BuildingChangeDialog *DLG);

	~CBuildingChangeThread(){};
	virtual void run(); 

  protected:
	bui_BuildingChangeDialog *dlg;
};

#endif
/* #####################################################################*/

class CALSFilterThread : public CProgressThread
{
  public:
    CALSFilterThread(bui_ALSFilterDialog *DLG);

	~CALSFilterThread(){};
	virtual void run(); 

  protected:
	bui_ALSFilterDialog *dlg;
};

/* #####################################################################*/

class CBuildingsDetectThread : public CProgressThread
{
  public:
    CBuildingsDetectThread(bui_BuildingsDetector *DLG);

	~CBuildingsDetectThread(){};
	virtual void run(); 

  protected:
	bui_BuildingsDetector *dlg;
};

/* #####################################################################*/

class CBuildingReconstructionThread : public CProgressThread
{
  public:
    CBuildingReconstructionThread(CBuilding *Building, 
		                          vector<CObservationHandler *> *imgVec, 
							      vector<CHugeImage *> *hugImgVec, 
							      CALSDataSet *ALS, const CCharString &path,
							      double PixImg, double PixDSM,
								  CBuildingReconstructor *reconstructor);

	~CBuildingReconstructionThread(){};
	virtual void run(); 

  protected:
	
	  CBuilding *pBuilding;
	  vector<CObservationHandler *> *pImgVec;							
	  vector<CHugeImage *> *pHugeImgVec;
	  CALSDataSet *pALS;
	  CCharString outputPath;
	  double pixImg;
	  double pixDSM;

	  CBuildingReconstructor *pReconstructor;
};


/* #####################################################################*/

class CDEMMergeThread : public CProgressThread
{
  public:
    CDEMMergeThread(CDEM* newDEM,vector<CDEM*> mergeDEMList);

	~CDEMMergeThread(){};
	virtual void run(); 

  protected:
	CDEM* newDEM;
	vector<CDEM*> mergeDEMList;
};




/* #####################################################################*/

class CDEMDifferenceThread : public CProgressThread
{
  public:
    CDEMDifferenceThread(CDEM* newDEM,CDEM* masterDEM, CDEM* compareDEM);

	~CDEMDifferenceThread(){};
	virtual void run(); 

  protected:
	CDEM* newDEM;
	CDEM* masterDEM;
	CDEM* compareDEM;
};


/* #####################################################################*/

class CHugeImageFlipThread : public CProgressThread
{
  public:
    CHugeImageFlipThread(CHugeImage* himage, bool vertical = true, bool horizontal = false);

	~CHugeImageFlipThread(){};
	virtual void run(); 

  protected:
	CHugeImage* himage;
	bool vertical;
	bool horizontal;
};

/* #####################################################################*/

/* #####################################################################*/

class CHugeImageEntropyThread : public CProgressThread
{
  public:
    CHugeImageEntropyThread(CHugeImage* himage, CHugeImage* entropy);

	~CHugeImageEntropyThread(){};
	virtual void run(); 

  protected:
	CHugeImage* himage;
	CHugeImage* entropy;
};

/* #####################################################################*/

/* #####################################################################*/

class CHugeImageEdgeOrientationThread : public CProgressThread
{
  public:
    CHugeImageEdgeOrientationThread(CHugeImage* himage, CHugeImage* edgeOrientation, CHugeImage* edgeImg, float ResImage);

	~CHugeImageEdgeOrientationThread(){};
	virtual void run(); 

  protected:
	CHugeImage* himage;
	CHugeImage* edgeOrientation;
	CHugeImage* edgeImg;
	float resImage;
};

/* #####################################################################*/

class CDEMTransformThread : public CProgressThread
{
  public:
    CDEMTransformThread(CDEM* trafoDEM,CDEM* srcDEM);

	~CDEMTransformThread(){};
	virtual void run(); 

  protected:
	CDEM* srcDEM;
	CDEM* trafoDEM;
};


/* #####################################################################*/

class CDEMOperationsThread : public CProgressThread
{
public:
	CDEMOperationsThread(double nullval, double constVall, string operation, CDEM* trafoDEM,CDEM* srcDEM);

	~CDEMOperationsThread(){};
	virtual void run(); 

protected:
	CDEM* srcDEM;
	CDEM* trafoDEM;
	double constvalue;
	string operation;
	double nullval;
};

/* #####################################################################*/

class CMeanAndStdDevThread : public CProgressThread
{
  public:
    CMeanAndStdDevThread(CHugeImage* himage,
						  doubles& mean,
						  doubles& stdDev,
						  int& nParametersX,
						  int& nParametersY,
						  int windowSizeX,
						  int windowSizeY,
						  int rectTop,
						  int rectHeight,
						  int rectLeft,
						  int rectWidth);

	~CMeanAndStdDevThread(){};
	virtual void run(); 

  protected:
	CHugeImage* himage;
	doubles& mean;
	doubles& stdDev;
	int& nParametersX;
	int& nParametersY;
	int windowSizeX;
	int windowSizeY;
	int rectTop;
	int rectHeight;
	int rectLeft;
	int rectWidth;
};



/* #####################################################################*/

class CImageMergeThread : public CProgressThread
{
  public:
    CImageMergeThread(CImageMerger* merger);

	~CImageMergeThread(){};
	virtual void run(); 

  protected:
	CImageMerger* merger;
	
};

/* #####################################################################*/

class CApplyTransferFunctionThread : public CProgressThread
{
  public:
    CApplyTransferFunctionThread(CHugeImage* oldHugeImage,CHugeImage* newHugeImage,bool applyTransferFunction);

	~CApplyTransferFunctionThread(){};
	virtual void run(); 

  protected:
	CHugeImage* oldHugeImage;
	CHugeImage* newHugeImage;
	bool applyTransferFunction;
	
};


/* #####################################################################*/

class CComputeHistogramThread : public CProgressThread
{
  public:
    CComputeHistogramThread(CHugeImage* himage,int rectTop,int rectHeight,int rectLeft,int rectWidth,vector<CHistogram*>& histograms,CHistogram& intensityHistogram);

	~CComputeHistogramThread(){};
	virtual void run(); 

  protected:
	CHugeImage* himage;
	int rectTop;
	int rectLeft;
	int width;
	int height;
	vector<CHistogram*>& histograms;
	CHistogram& intensityHistogram;
	
};


/* #####################################################################*/

class CImageLoadThread : public CProgressThread
{
  public:
	  CImageLoadThread(CImageImporter& imageImporter,CImageImportData& data,bool isDEM) :
			imageImporter(imageImporter),importData(data),isDEM(isDEM),callInit(false)
	{

	}
	
	CImageLoadThread(CImageImporter& imageImporter,CImageImportData& importData,bool isDEM,CFileName filename,CFileName hugeFileNameDir):
		imageImporter(imageImporter),importData(importData),isDEM(isDEM),filename(filename),hugeFileNameDir(hugeFileNameDir),callInit(true)
	{

	}

	~CImageLoadThread(){};
	virtual void run(); 

  protected:
	CFileName filename;
	CFileName hugeFileNameDir;
	CImageImporter& imageImporter;
	CImageImportData& importData;
	bool isDEM;
	bool callInit;
};

/* #####################################################################*/

class CBandMergeThread : public CProgressThread
{
  public:
	  CBandMergeThread(CBandMerger& bandMerger);

	~CBandMergeThread(){};
	virtual void run(); 

  protected:
	CBandMerger& bandMerger;
};

/* #####################################################################*/

class CGCPPointMatchingThread : public CProgressThread
{
  public:
	  CGCPPointMatchingThread(CBaristaProject& proj, const vector<CGCPDataBasePoint>& db, const CMatchingParameters &par,CReferenceSystem refSys);

	~CGCPPointMatchingThread(){};
	virtual void run(); 

  protected:
	CBaristaProject& project;
	const vector<CGCPDataBasePoint>& dataBase;
	const CMatchingParameters &pars;
	CReferenceSystem referenceSystem;
};

/* #####################################################################*/

class GenerateEpipolarThread : public CProgressThread
{
  public:	
	  GenerateEpipolarThread(CGenerateEpipolarProject *pEPI);
	  ~GenerateEpipolarThread(){};

	  virtual void run(); 

  protected:
	
	  CGenerateEpipolarProject *pEpi; 
};

/* #####################################################################*/

#endif