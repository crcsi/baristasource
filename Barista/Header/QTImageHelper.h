#ifndef __QTImageHelper__
#define __QTImageHelper__

#include "Filename.h"

class QImage;
class QPixmap;
class CImageFile;
class CHugeImage;
class CVImage;
class ZObject;
class QIcon;
class CTransferFunction;
class CImageBuffer;

class QTImageHelper
{
public:

	static QImage* createTileQImage(CHugeImage* image,int tileR, int tileC, float zoomFactor);
	static QImage* createQImage( CHugeImage* image,int rectTop, int rectHeight, int rectLeft, int rectWidth, int destheight, int destWidth);	
	static bool createQIcon(QIcon* qicon, CHugeImage* image, int rectTop, int rectHeight, int rectLeft, int rectWidth, int destHeight, int destWidth);

	static QImage makeIcon(const ZObject* object);

	static bool writeImageChips(CVImage* vimage, CFileName dir, int width, int height, bool withHTML, bool withControl);

	static QPixmap createNoFileFoundPixmap();
private:
	
	static void computeHistograms(CHugeImage* image);
	static bool fillQImage(QImage* qimage,const CImageBuffer& imgBuf, CHugeImage* image);
	static QImage* createCommonQImage(const CImageBuffer& imgBuf,CHugeImage* image);



protected:

	static int MinGrey;

	static int MaxGrey;

	static int MinNIR;

	static int MaxNIR;

	static int MinRed;

	static int MaxRed;

	static int MinGreen;

	static int MaxGreen;

	static int MinBlue;

	static int MaxBlue;

	static int MinColour;

	static int MaxColour;

	static float colourFactor;

	static float greyFactor;

	static unsigned char scaleGrey(float grey)
	{
		float scaledVal = (grey - float(MinGrey)) * greyFactor;
		if (scaledVal > 255.0) scaledVal = 255;
		else if (scaledVal < 0.0) scaledVal= 0;
                
        return (unsigned char)(scaledVal);
	}

	static unsigned char scaleGrey(unsigned short grey)
	{
		float scaledVal = (float(grey) - float(MinGrey)) * greyFactor;
		if (scaledVal > 255.0) 
			scaledVal = 255;
		else if (scaledVal < 0.0) 
			scaledVal= 0;
                
        return (unsigned char)(scaledVal);
	}

	static unsigned char scaleColour(unsigned short colour)
	{
		float scaledVal = (float(colour) - float(MinColour)) * colourFactor;
		if (scaledVal > 255.0) scaledVal = 255;
		else if (scaledVal < 0.0) scaledVal= 0;
                
        return (unsigned char)(scaledVal);
	}

	static unsigned char brighten(int brightness, unsigned char colour)
	{
		brightness += colour;
		if (brightness > 255.0) brightness = 255;
		else if (brightness < 0.0) brightness= 0;
                
        return (unsigned char)(brightness);
	}

};
#endif

