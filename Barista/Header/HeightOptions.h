#ifndef HEIGHTOPTIONS_H
#define HEIGHTOPTIONS_H
#include <QDialog>
#include "ui_heightOptions.h"


class HeightOptions :	public QDialog, public Ui_Monoplot
{
Q_OBJECT

  protected:
	  bool success;
	
  public:

	  HeightOptions(QWidget *parent=0);
	  ~HeightOptions();

public slots:
    void DefaultSet();
	void DEMButtonclicked();
    void SinglePointButtonclicked();
    void ProjectHeightButtonclicked();
	void Okclicked();
	void Cancelclicked();
	bool getsuccess()const {return this->success;};
	    
};
#endif // HEIGHTOPTIONS