/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/



int ObjectChooserDlg::getSelectedObject()
{
     return (this->ListBox->index(this->ListBox->selectedItem()) );
}


void ObjectChooserDlg::initListBox()
{

    

}


void ObjectChooserDlg::setTitleText( const char * title )
{
    this->setCaption(title);
}


void ObjectChooserDlg::setButtonText( const char * buttontext )
{
    this->Ok->setText(buttontext);
}



void ObjectChooserDlg::ItemtoListbox( const char * name, int i )
{
    this->ListBox->insertItem(name, i);
    this->ListBox->setSelectionMode(QListBox::Single );

}




void ObjectChooserDlg::Ok_clicked()
{
    this->close();
}


void ObjectChooserDlg::Cancel_clicked()
{
    this->ListBox->clearSelection();
    this->close();
}
