#pragma once
#include <qtreewidget.h>



class CBaristaTreeWidget :	public QTreeWidget
{
	Q_OBJECT
public:
	CBaristaTreeWidget(QWidget* parent);
	CBaristaTreeWidget();

	void contextMenuEvent(QContextMenuEvent * e);

public slots:
	void itemClickedSlot(QTreeWidgetItem * item,int column);
	void itemDoubleClickedSlot(QTreeWidgetItem * item,int column);

public:
	~CBaristaTreeWidget(void);
};
