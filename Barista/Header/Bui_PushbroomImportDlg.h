#ifndef BUI_PUSHBROOMIMPORTDLG_H
#define BUI_PUSHBROOMIMPORTDLG_H

#include <QDialog>
#include "ui_PushbroomImportDlg.h"
#include "UpdateHandler.h"
#include "CameraArray.h"
#include "Serializable.h"
#include "ReferenceSystem.h"



// test
#include <QAbstractItemModel>
#include <QLinkedList>

class ZObject;


// end test

class CVImage;
class CameraInfos;

class Bui_PushbroomImportDlg : public QDialog, public Ui_Bui_PushbroomImportDlgClass, public CUpdateHandler
{
	Q_OBJECT

public:
	Bui_PushbroomImportDlg(CVImage* vimage,QWidget *parent = 0);
	~Bui_PushbroomImportDlg();

	bool getSuccess() {return this->success;};

	CCamera* getCamera();
	double getSatelliteHeight() const;
	double getGroundHeight() const { return this->projectH;};
	double getGroundTemperature() const { return this->groundTemp;};
	double getInTrackViewAngle() const { return this->inTrackViewAngle;};
	double getCrossTrackViewAngle() const { return this->crossTrackViewAngle;};

	C3DPoint getLL() const;
	C3DPoint getUL() const;
	C3DPoint getLR() const;
	C3DPoint getUR() const;

	void setViewingAngles(double inTrack,double crossTrack);

	double getOffNadirAngle() const {return this->offNadirAngle;};
	C3DPoint getIRP();

public slots:
	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBSaveCameraReleased();
	void pBSaveSceneInfoReleased();
	void pBLoadSceneInfoReleased();
	void pBMeasureReleased();

	void rBGeocentricReleased();
	void rBGeographicReleased();
	void rBGridReleased();

	void rBDirectReleased();
	void rBMeasureReleased();

	void cBEditCameraChanged(int state);

	void leTextChanged(const QString & text);
	void currentCameraChanged( int index );

	void editingFinishedXc(); 
	void editingFinishedYc(); 
	void editingFinishedF(); 
	void editingFinishedSatH(); 
	void editingFinishedGroundTemp(); 
	void editingFinishedGroundHeight(); 
	void editingFinishedInTrackViewAngle(); 
	void editingFinishedCrossTrackViewAngle(); 
	void editingFinishedOffNadirAngle();
	void editingFinishedCameraName(); 

	void scenePointChanged(QTableWidgetItem * item);	

	void closeEvent(QCloseEvent* e);

private:
	void checkSettings();
	bool checkTable();
	bool readCameras();
	void initSceneTable(int nCols);
	void fillSceneTable(int nCols);
	void transformPoint(C3DPoint& point)const;

private:
	bool success;
	CCameraArray cameras;
	doubles satH;
	
	
	int selectedCamera;
	eCoordinateType selectedCoordinateType;

	double groundTemp;
	double projectH;
	double inTrackViewAngle;
	double crossTrackViewAngle;
	double offNadirAngle;

	bool hasMeasuredSuccessfully;
	bool offNadirEntered;

	CReferenceSystem refSys;

	C3DPoint LL;
	C3DPoint LR;
	C3DPoint UL;
	C3DPoint UR;

	CVImage* image;

	bool hasEnteredRefInfo;
};


class CameraInfos : public  CSerializable
{
public:
	CameraInfos(CCameraArray& cameras,doubles& satH,bool saveNewCamera);

	virtual bool isa(string& className) const
	{
		if (className == "CameraInfos")
			return true;

		return false;
	};

	virtual string getClassName() const {return string("CameraInfos");};

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified() { return true;};
	virtual void reconnectPointers(){};
	virtual void resetPointers(){};

protected:
	CCameraArray& cameras;
	doubles& satH;
	bool saveNewCamera;
};





class SceneInfo : public  CSerializable
{
public:
	SceneInfo(C3DPoint &LL,	C3DPoint &LR,C3DPoint &UL,C3DPoint &UR,
				CReferenceSystem &refSys,double &groundTemp,
				double &projectH,double &inTrackViewAngle,
				double &crossTrackViewAngle);

	virtual bool isa(string& className) const
	{
		if (className == "SceneInfo")
			return true;

		return false;
	};

	virtual string getClassName() const {return string("SceneInfo");};

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified() { return true;};
	virtual void reconnectPointers(){};
	virtual void resetPointers(){};

protected:
	double &groundTemp;
	double &projectH;
	double &inTrackViewAngle;
	double &crossTrackViewAngle;

	CReferenceSystem &refSys;

	C3DPoint &LL;
	C3DPoint &LR;
	C3DPoint &UL;
	C3DPoint &UR;
};




class CBaristaThumbnailNode 
{
public:
	CBaristaThumbnailNode(ZObject* object);
	~CBaristaThumbnailNode();


	
	ZObject* object;
	QIcon icon;
	QString text;

	CBaristaThumbnailNode* parent;
	QLinkedList<CBaristaThumbnailNode*> children;

};






class CBaristaThumbnailModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	CBaristaThumbnailModel(QObject* parent=0);
	~CBaristaThumbnailModel(void);

	virtual QModelIndex index(int,int,const QModelIndex &parent) const;
	virtual QModelIndex parent(const QModelIndex &child) const;
	virtual int rowCount(const QModelIndex &parent) const;
	virtual int columnCount(const QModelIndex &parent) const;
	virtual QVariant data(const QModelIndex &index,int) const;

	virtual QVariant headerData(int section,Qt::Orientation orientation,int role) const;


};








#endif // BUI_PUSHBROOMIMPORTDLG_H
