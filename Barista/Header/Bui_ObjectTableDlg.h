#ifndef BUI_OBJECTTABLEDLG_H
#define BUI_OBJECTTABLEDLG_H

#include <QDialog>
#include "ui_ObjectTableDlg.h"

class CObjectSelectorTable;

class Bui_ObjectTableDlg :  public QDialog,public Ui_Bui_ObjectTableDlgClass
{
	Q_OBJECT

public:
	Bui_ObjectTableDlg(CObjectSelectorTable* table,QWidget *parent = 0);
	~Bui_ObjectTableDlg();

public slots:
		void nameChanged(QString name);

protected: 
	CObjectSelectorTable* table;

};

#endif // BUI_OBJECTTABLEDLG_H
