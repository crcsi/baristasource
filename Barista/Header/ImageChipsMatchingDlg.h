#ifndef IMAGECHIPSMATCHINGDLG_H
#define IMAGECHIPSMATCHINGDLG_H

#include <QDialog>
#include <QDate>
#include "ui_ImageChipsMatchingDlg.h"
//#include <vector.h>
//class CVImage;
//class CVDEM;
class CMatchingParameters;
using namespace std;

class ImageChipsMatchingDlg : public QDialog, public Ui_ImageChipsMatchingDlgClass
{
	Q_OBJECT

public:
	ImageChipsMatchingDlg(QWidget *parent = 0);
	~ImageChipsMatchingDlg();
	bool getSuccess() {return this->success;};
public slots:
	//void pBCancelReleased();
	//void pBOkReleased();
	//void pBHelpReleased();
	void pBFileNameReleased();
	void ckBSensorChanged();
	void ckBResolutionChanged();
	void ckBDateChanged();
	//void ImageSelected(int row);
	void ImageClicked(QListWidgetItem *item);
	void ckBnGCPsChanged();
	void minResChanged();
	void maxResChanged();
	void pBapplySettingsReleased();
	void WallisFilterClicked();
	void PatchSizeChanged();
	void setMatchParGUI(const CMatchingParameters &pars);
	void RhoMinChanged();
	void SearchAreaChanged();
	void MatchParsSelected();
	void pBOKclicked();
	void pBCancelclicked();
	void nGCPsChanged();

protected:
	double minRes;
	double maxRes;

	QDate dateFrom;
	QDate dateTo;

	int nImages;
	//CVDEM *pDEM;
	vector <vector<bool>> originalChipSelectionArray;
	vector <vector<bool>> currentChipSelectionArray;
	vector <bool> isImageSelected;
	bool ImgSelected[1000];
	//vector <CVImage *> possibleOriginalImages;
	unsigned int nSelection;
	unsigned int nGCPs;

	bool useWallis;
	bool success;
	//bool dummy;

protected:
	void intiImages();
	void initDEMs();
	void initChipSelection();
	void initChips();
	void initSelectionCriteria();
	void initMatchingParameters();
	bool closeChipDbase();
	void chipDbFileSave();
	void setProjectChipSelectionArray();
//private:
//	Ui::ImageChipsMatchingDlgClass ui;
};

#endif // IMAGECHIPSMATCHINGDLG_H
