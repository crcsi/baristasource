#ifndef __COpenBaristaViewList__
#define __COpenBaristaViewList__

#include "BaristaView.h"
#include "UpdateHandler.h"

class COpenBaristaViewList : public CUpdateHandler
{
  protected:

	  vector<CBaristaView *> list;

  public:

	  COpenBaristaViewList() {};
	  ~COpenBaristaViewList() {};

	  int getNumberOfViews() const { return list.size(); };

	  CBaristaView *getView(int index) const { return list[index]; };

	  void addView(CBaristaView *view, bool inGeoLinkMode);

	  void deleteView(CBaristaView *view);

	  void activateGeoLinkMode();

	  void deactivateGeoLinkMode();

  protected:

	  int getIndexOfView(CBaristaView *view);

	  void deleteView(int index);

};

#endif

