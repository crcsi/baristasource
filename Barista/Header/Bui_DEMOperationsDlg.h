#ifndef BUI_DEMOPERATIONSDLG_H
#define BUI_DEMOPERATIONSDLG_H

#include <QDialog>
#include "Ui_DEMOperations.h"
#include "Filename.h"
#include "3DPoint.h"
#include "GDALFile.h"


class CVDEM;

class Bui_GDALExportDlg;

class Bui_DEMOperationsDlg : public QDialog, public Ui_DEMOperations
{
	Q_OBJECT
		public slots:

	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBSelectOutputReleased();

	void operationEditingFinished();
	void constantEditingFinished();
	void nullValEditingFinished();
	
public:
	Bui_DEMOperationsDlg(CVDEM* vdem);
	~Bui_DEMOperationsDlg();

	void setSuccess(bool b) { this->success = b;};
	bool getSuccess() {return this->success;};
	void checkSetup();

private:

 	CFileName trafoname;
	int masterIndex;
	int compareIndex;


	bool success;
	
	string condition;

	double nullVALL;
	double consVall;

protected:
	CVDEM* srcdem;
	CVDEM* trafodem;
	Bui_GDALExportDlg* dlg;
};

#endif // BUI_DEMOPERATIONSDLG_H
