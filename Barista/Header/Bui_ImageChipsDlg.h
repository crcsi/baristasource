#ifndef BUI_IMAGECHIPSDLG_H
#define BUI_IMAGECHIPSDLG_H

#include <QDialog>
#include "ui_ImageChipsDlg.h"

class Bui_ImageChipsDlg : public QDialog, public Ui_Bui_ImageChipsDlgClass
{
	Q_OBJECT

public:
	Bui_ImageChipsDlg(QWidget *parent = 0);
	~Bui_ImageChipsDlg();
	void initDialog();

public slots:
	void OK_clicked();
	void Cancel_clicked();

//private:
	//Ui::Bui_ImageChipsDlgClass ui;
};

#endif // BUI_IMAGECHIPSDLG_H
