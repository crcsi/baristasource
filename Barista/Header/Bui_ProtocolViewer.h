#ifndef BUI_PROTOCOLVIEWER_H
#define BUI_PROTOCOLVIEWER_H

#include <QDialog>
#include "ui_ProtocolViewer.h"
#include "UpdateHandler.h"
#include "Filename.h"

class Bui_ProtocolViewer : public QDialog, public Ui_ProtocolViewerClass, public CUpdateHandler
{
	Q_OBJECT

public:
	Bui_ProtocolViewer(CFileName protocolFile,QWidget *parent = 0);
	~Bui_ProtocolViewer();

	void reloadProtocolFile();


	CFileName getFileName() const {return this->filename;};

public slots:
	void pBSaveAsreleased();
protected:

	void closeEvent(QCloseEvent *event);

private:
	CFileName filename;


};

#endif // PROTOCOLVIEWER_H
