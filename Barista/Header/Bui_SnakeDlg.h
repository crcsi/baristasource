/**
 * Copyright:    Copyright (c) Petra Helmholz<p>
 * Company:      University of Melbourne<p>
 * @author PetraH
 * @version 1.0
 */


#ifndef BUI_SNAKEDLG_H
#define BUI_SNAKEDLG_H

#include <qdialog.h>
#include "ui_Bui_SnakeDlg.h"
#include "Filename.h"
#include "SnakeExtraction.h"
//#include "ProgressHandler.h"

using namespace Ui;

class CVImage;
class CXYPointArray;
class CLabelImage;

/*!
 * \brief
 * Dialog for the calculation of snakes to enter the parameters.
 *
 * The max. and min. values are defined here, too. If you want to make
 * GVF usable delete the line "this->GVF->setEnabled(false);" in the
 * function "initSnakeDlg()".
 */

class Bui_SnakeDlg : public QDialog, public Bui_SnakeDlgClass
{
	Q_OBJECT

protected:
	SnakeExtractionParameters parameters;
	
	CVImage *image;
	//CLabelImage *labelimage;
	CFileName labelFileName;
	CFileName iniFileName;
	CXYPointArray *Points;
	
	bool parsAccepted;
	bool enableOpenSnake;	
	bool OpenSnake;
	bool *CaclulationOK;


public:
	Bui_SnakeDlg(QWidget* parent, CFileName initialise, CVImage *I_image,bool open, CXYPointArray *startPoints, bool *ok);
	~Bui_SnakeDlg();

	CFileName getLabelFileName() const { return this->labelFileName; };
	SnakeExtractionParameters getParameters() const { return parameters; };

public slots:
	void initImages();
	void initSnakeDlg();

	void OK();
	void Cancel();
	void Help();

	//void checkSnakeTypes();				//open, close
	void checkSnakeMode();					//traditional, GVF, balloon
	void checkValuesGeneralSettings();		//parameter general settings
	void checkValuesInternalEnergy();		//parameter internal energy
	void checkValuesExternalEnergy();		//parameter external energy
	
private:
	void setup();
	void setParameters();
	
};

#endif
