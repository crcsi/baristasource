#ifndef BUI_OUTPUTMODEHANDLER_H
#define BUI_OUTPUTMODEHANDLER_H

#include "ProtocolHandler.h"

class QRadioButton;


class bui_OutputModeHandler 
{

  protected:

	  QRadioButton *pOutputLevelMinRadioButton;

	  QRadioButton *pOutputLevelMedRadioButton;

	  QRadioButton *pOutputLevelMaxRadioButton;

	  QRadioButton *pOutputLevelDebugRadioButton;

  public:

	  bui_OutputModeHandler(QRadioButton *OutputLevelMinRadioButton,
		                    QRadioButton *OutputLevelMedRadioButton,
							QRadioButton *OutputLevelMaxRadioButton,
							QRadioButton *OutputLevelDebugRadioButton);

	  ~bui_OutputModeHandler();


	  void reactOnButtonSelected();

	  void init();

	  void init(eOUTPUTMODE mode);

};

#endif // BUI_OUTPUTMODEHANDLER_H