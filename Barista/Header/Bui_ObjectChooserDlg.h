
#include <qdialog.h>
#include "ui_ObjectChooserDlg.h"
#include "ZObject.h"

class bui_ObjectChooserDlg : 	public QDialog, public Ui_ObjectChooserDlg
{Q_OBJECT
public:
	bui_ObjectChooserDlg(void);

	void adjustLineCount(int nlines);
	void setMultiSelection(bool b);
public:
	~bui_ObjectChooserDlg(void);
public slots:
	vector<ZObject*> getSelectedObjects();
    void setTitleText( const char * title );
    void setButtonText( const char * buttontext );
    void ItemtoListbox( const char * name,ZObject* object );
    void Ok_clicked();
    void Ok_clicked(QListWidgetItem* item);
    void Cancel_clicked();


	
};
