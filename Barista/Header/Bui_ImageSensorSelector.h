#ifndef BUI_IMAGESENSORSELECTOR_H
#define BUI_IMAGESENSORSELECTOR_H

class QComboBox;
class CVImage;
class CSensorModel;

enum eImageSelectorBandConfig 
{ 
	eALLBANDS,
	ePANONLY,
	eMULTIBANDONLY
};

enum eImageSelectorSensorConfig 
{ 
	eALLSENSORS,
	eTFWONLY,
	eTFWORNONE,
	eFRAMECAMONLY,
	eCURRENTNOTFW,
	eCURRENTALL
};

class bui_ImageSensorSelector 
{
  protected:

	  QComboBox    *imageSelector;

	  QComboBox    *sensorModelSelector;

	  CVImage      *selectedImage;

	  CSensorModel *selectedSensorModel;
 
	  eImageSelectorBandConfig bandConfig;

	  eImageSelectorSensorConfig sensorConfig;

	  int RPCIndex;

	  int affineIndex;

	  int pushbroomIndex;

	  int frameCameraIndex;

	  int deltaNone;

  public:

	  bui_ImageSensorSelector(QComboBox *I_imageSelector, QComboBox *I_sensorModelSelector, 
							  eImageSelectorBandConfig bandconfig,
							  eImageSelectorSensorConfig sensorconfig,
							  CVImage *I_selectedImage = 0, CSensorModel *I_selectedSensorModel = 0);

	  bui_ImageSensorSelector(QComboBox *I_imageSelector, QComboBox *I_sensorModelSelector, 
							  eImageSelectorBandConfig bandconfig, 
							  eImageSelectorSensorConfig sensorconfig,
							  bool addNone,
							  CVImage *I_selectedImage = 0, CSensorModel *I_selectedSensorModel = 0);

	  ~bui_ImageSensorSelector();


	  void init(CVImage *I_selectedImage = 0, CSensorModel *I_selectedSensorModel = 0, bool addNone = false);

	  CVImage      *getSelectedImage() const { return selectedImage; };

	  CSensorModel *getSelectedSensorModel() const { return selectedSensorModel; };

	  bool reactOnImageSelect();

	  bool reactOnSensorModelSelect();

  protected:

	  bool initSensorModel(CSensorModel *I_selectedSensorModel = 0);

};

#endif // BUI_ORTHODIALOG_H