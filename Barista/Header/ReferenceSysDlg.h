#ifndef REFERENCESYSDLG_H
#define REFERENCESYSDLG_H

#include <QWidget>
#include "ui_ReferenceSysDlg.h"

class ReferenceSysDlg : public QWidget
{
    Q_OBJECT

public:
    ReferenceSysDlg(QWidget *parent = 0);
    ~ReferenceSysDlg();

private:
    Ui::ReferenceSysDlgClass ui;
};

#endif // REFERENCESYSDLG_H
