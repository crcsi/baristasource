#ifndef BUI_XYZPOINTDLG_H
#define BUI_XYZPOINTDLG_H

#include "AdjustmentDlg.h"
#include "ui_XYZPointDlg.h"

class CXYZPointArray;


class Bui_XYZPointDlg : public CAdjustmentDlg, public Ui_XYZPointDlgClass
{
	Q_OBJECT

public:
	Bui_XYZPointDlg(CXYZPointArray* points,CXYZPointArray* residuals,QWidget *parent = 0);
	~Bui_XYZPointDlg();


	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString() {return "Points dialog";};

	virtual void elementAdded(CLabel element=""){};
	virtual void elementDeleted(CLabel element);

	void setPoints(CXYZPointArray* points,CXYZPointArray* residuals);
	virtual void cleanUp();
signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

public slots:
	void pointObsDlgStatusChanged(bool b);
	void pointResDlgStatusChanged(bool b);
	void tWCurrentChanged( int index );

private slots:
	virtual void elementsChangedSlot(CLabel element);

protected:


	void init3DObsPointDlg();
	void init3DResPointDlg();


	CAdjustmentDlg* pointObsDlg;
	CAdjustmentDlg* pointResDlg;



protected:
	CXYZPointArray* points;
	CXYZPointArray* residuals;

	int oldTab;
};

#endif // BUI_XYZPOINTDLG_H
