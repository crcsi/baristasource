#ifndef __CMonoplottingPointHandler__
#define __CMonoplottingPointHandler__
#include "MonoplottingHandler.h"
#include "Bui_MonoPlotDlg.h"


class CMonoplottingPointHandler : public CMonoplottingHandler
{
public:
	CMonoplottingPointHandler(void);
	~CMonoplottingPointHandler(void){};

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) { return false; };
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)  {return true;} ;
	virtual bool redraw(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setCrossCursor();};

	virtual void open();

	virtual void close();

	virtual void elementDeleted(CLabel element="");

protected:

	void addMonoPlottedPointDrawings(CBaristaView * v,CXYZPoint * pNewPoint);
	void initPoint();

protected:
	bui_MonoPlotDlg monoplotdlg;
	CLabel currentPointLabel;
};

#endif