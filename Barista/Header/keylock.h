#ifndef KEYLOCK_H
#define KEYLOCK_H

#include <QtWidgets\QDialog>
#include "ui_keylock.h"

class KeyLock : public QDialog, public Ui_KeyLockClass
{
	Q_OBJECT

public:
	KeyLock(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~KeyLock();

	public slots:
		void pBReadReleased();
		void pBWriteReleased();
		void pBCloseReleased();


};

#endif // KEYLOCK_H
