#ifndef BUI_SPLINEMODELDLG_H
#define BUI_SPLINEMODELDLG_H

#include <QDialog>
#include <QStandardItemModel>

#include "ui_SplineModelDlg.h"
#include "AdjustmentDlg.h"

#include "SplineModelFactory.h"
#include "SplineModel.h"

class CSplineDataModel;

class Bui_SplineModelDlg : public CAdjustmentDlg, public Ui_SplineModelDlg
{
	Q_OBJECT

public:
	Bui_SplineModelDlg(CSplineModel* splineModel,SplineModelType splineType,QWidget *parent = 0);
	~Bui_SplineModelDlg();

	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString();
	virtual void cleanUp();

signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

private slots:
	virtual void elementsChangedSlot(CLabel element);

protected:

	// general
	SplineModelType splineType;

	CSplineModel* spline;
	
	CSplineModel *existingSpline;

	CSplineDataModel* dataModel;


};



// ###############################################################################################

class CSplineDataModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	CSplineDataModel(CSplineModel* splineModel,SplineModelType splineType,QObject* parent =0);
	int rowCount(const QModelIndex& parent) const;
	int columnCount(const QModelIndex& parent) const;
	QVariant headerData(int section,Qt::Orientation orientation, int role) const;
	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	bool setData(const QModelIndex &index, const QVariant &value,int role = Qt::EditRole);
	void update();

public slots:
	void itemClicked( const QModelIndex& index);


private:
	CSplineModel* splineModel;
	SplineModelType splineType;
};



#endif // BUI_SPLINEMODELDLG_H
