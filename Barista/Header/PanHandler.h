#ifndef __CPanHandler__
#define __CPanHandler__
#include "BaristaViewEventHandler.h"


class CPanHandler : public CBaristaViewEventHandler
{
public:
	CPanHandler(void);

	~CPanHandler(void);

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyPressEvent (QKeyEvent *e, CBaristaView *v)
	{
		return CBaristaViewEventHandler::keyPressEvent(e, v);
	};

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	bool handleMiddleMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) { return false; };

	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);
	bool handleMiddleMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)  {return true;} ;

	virtual bool redraw(CBaristaView *v);
	virtual void initialize(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setPanCursor();};

	virtual void open();

	virtual void close();



};

#endif