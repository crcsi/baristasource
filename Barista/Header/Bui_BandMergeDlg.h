#ifndef BUI_BANDMERGEDLG_H
#define BUI_BANDMERGEDLG_H

#include <QDialog>
#include "ui_BandMergeDlg.h"
#include "Filename.h"
#include "Bui_GDALExportDlg.h"
#include "IniFile.h"
#include "BaristaProject.h"


class CVImage;
class Bui_GDALExportDlg;
class bui_ImageSensorSelector;

class Bui_BandMergeDlg : public QDialog, public Ui_BandMergeDlg
{
	Q_OBJECT

public:
	Bui_BandMergeDlg(void);
	~Bui_BandMergeDlg();

	void setSuccess(bool b) { this->success = b;};
	bool getSuccess() {return this->success;};
	
	void initDlg();
	void initImageSelectors();

	void updateTFWOptions();

	void checkSetup();

	bool checkBandCount();
	bool checkHeights();
	bool checkWidths();
	bool checkSingleBand();
	bool checkBpp();
	bool checkTFW();

	bool mergeTFW();

	int getSelectedImageCount();

	bool createMergedImage();

protected:
	
	bui_ImageSensorSelector* imageSelectorRED;
	bui_ImageSensorSelector* imageSelectorGREEN;
	bui_ImageSensorSelector* imageSelectorBLUE;
	bui_ImageSensorSelector* imageSelectorNIR;

	bool addTFW;
	bool success;

	CVImage* imageRED;
	CVImage* imageGREEN;
	CVImage* imageBLUE;
	CVImage* imageNIR;
	CVImage* mergedImage;

	CTFW tfw;

	int bands;
	int bpp;
	int width;
	int height;

	int wRED;
	int hRED;
	int wGREEN;
	int hGREEN;
	int wBLUE;
	int hBLUE;
	int wNIR;
	int hNIR;

	int bandsRED;
	int bppRED;
	int bandsGREEN;
	int bppGREEN;
	int bandsBLUE;
	int bppBLUE;
	int bandsNIR;
	int bppNIR;


	bool tfwRED;
	bool tfwGREEN;
	bool tfwBLUE;
	bool tfwNIR;

	CFileName mergedname;

	Bui_GDALExportDlg* dlg;

public slots:
	void applySettings();

	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBSelectOutputReleased();

	void cbTFWReleased();

	void rbMergeTFWReleased();
	void rbUseFromImageTFWReleased();

	void REDImageChanged();
	void GREENImageChanged();
	void BLUEImageChanged();
	void NIRImageChanged();
};

#endif // BUI_BANDMERGEDLG_H
