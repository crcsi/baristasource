#ifndef RPCDLG_H
#define RPCDLG_H

#include "AdjustmentDlg.h"
#include "ui_RpcDlg.h"
#include "RPC.h"


class Bui_RpcDlg :  public CAdjustmentDlg ,public Ui_RpcDlgClass
{
	Q_OBJECT

public:
	Bui_RpcDlg(CRPC* existingRPC,CRPC* originalRPC,QWidget *parent = 0);
	~Bui_RpcDlg();

	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString() {return "RPC parameter";};

	virtual void elementAdded(CLabel element="") {};
	virtual void cleanUp();
signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

public slots:
	void pBResetReleased();
	void pBResetAllReleased();
	void pBApplyReleased();
	void rBNoneReleased();
	void rBShiftReleased();
	void rBShiftDriftReleased();
	void rBAffineReleased();


private slots:
	virtual void elementsChangedSlot(CLabel element);

protected:
	void applySettings();


	CRPC *existingRPC;
	CRPC *originalRPC;

	CRPC localRPC;
};

#endif // RPCDLG_H
