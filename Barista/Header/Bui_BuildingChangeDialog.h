#ifndef BUI_BUILDINGCHANGEDIALOG_H
#define BUI_BUILDINGCHANGEDIALOG_H

#include <QDialog>
#include "ui_BuildingChangeDetector.h"
#include <QTimer>
#include "CharString.h"
#include "TFW.h"

class CProgressListener;
class CProgressThread;
class bui_ImageSensorSelector;
class bui_OutputModeHandler;

class CVImage;
class CBuildingChangeEvaluator;
class CLabelImage;
class CVDEM;

class bui_BuildingChangeDialog :	public QDialog, public Ui_BuildingChangeDialog
{Q_OBJECT

  protected:

	  CVImage *pExistingLabels;

	  CVImage *pNewLabels;

	  CVDEM *pDSM;

	  CVDEM *pDTM;

	  bui_OutputModeHandler *pOutPutHandler;

	  float MinBuildingArea;

	  float noneVsWeak;
	  float weakVsPartial;
	  float partialVsStrong;

	  float toleranceSplit;

	  float toleranceChanges;

	  bool computationOK;

	  float cellsizeHeight, cellsizeArea, maxsizeHeight, maxsizeArea;

	  CTFW trafo;

	  CCharString currentDir;

	  CCharString outputDir;

	  int width, height;

	  int borderSize;

	  bool importExisting;

	  CFileName existingDXF;

	  bool changeDetector; 

  public:

	  bui_BuildingChangeDialog(CVImage *pExisting, bool isChangeDetector);

	  ~bui_BuildingChangeDialog();


	  bool getComputationOK() const { return computationOK; }

	  void setCurrentDir(const CCharString &cdir);

	  void checkSetup();

	  bool detectChanges();

  public slots:
	
	  void ExistingChanged();

	  void NewChanged();

	  void DSMChanged();

	  void DTMChanged();

	  void NoneVsWeakChanged();

	  void WeakVsPartialChanged();

	  void PartialVsStrongChanged();

	  void MinAreaChanged();

	  void BordersizeChanged();

	  void ToleranceSplitChanged();
	  void ToleranceChangesChanged();

	  void CellSizeAreaChanged();
	  void CellSizeHeightChanged();
	  void MaxSizeAreaChanged();
	  void MaxSizeHeightChanged();

	  void OutputLevelChanged();

	  void dxfImport();

	  void explorePathSelected();

	  void OK();

	  void Cancel();
	  
	  void Help();

  protected:

	  CBuildingChangeEvaluator *detector;

	  void setupImageSelector(QComboBox *box, CVImage *selectedImage);

	  void setupDSMSelector(QComboBox *box, CVDEM *selectedDSM);

	  bool setupExtents();

	  bool readLabelImages(CLabelImage &imgOld, CLabelImage &imgNew);

	  void initLabelFromDXF();
  private: 

	  void init();

};

#endif // BUI_BUILDINGCHANGEDIALOG_H