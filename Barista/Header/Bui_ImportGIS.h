#ifndef BUI_IMPORTGIS_H
#define BUI_IMPORTGIS_H

#include <qdialog.h>
#include "ui_ImportGIS.h"
#include "Filename.h"
#include "ReferenceSystem.h"


class bui_ImportGISDialog : 	public QDialog , public Ui_ImportGISDialog
{
	Q_OBJECT

protected:
	bool filenameSelected;
	bool success;
	CFileName inputFilename;
	CReferenceSystem refsys;

public:
	bui_ImportGISDialog ();
	~bui_ImportGISDialog();

	bool hasFilename() const { return this->filenameSelected; };

	bool getSuccess() const { return this->success; };

	const char *getInputFilename() const { return this->inputFilename.GetChar(); };

	CReferenceSystem getRefsys() const { return this->refsys; };

public slots:
	void onFileSelected();
	void onFileChanged();
	void OK();
	void Cancel();
	void Help();

protected:
	void init();
	void destroy();
};

#endif // BUI_IMPORTGIS_H