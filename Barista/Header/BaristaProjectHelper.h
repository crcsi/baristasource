#ifndef __CBaristaProjectHelper__
#define __CBaristaProjectHelper__

#include "StatusBarHandler.h"

class CVImage;

class CBaristaProjectHelper : public CStatusBarHandler
{
public:
	CBaristaProjectHelper(void);
public:
	~CBaristaProjectHelper(void);

	void setIsBusy(bool b); 

    void restoreTree();
    void refreshTree();

	void closeGUI();

	void openBaristaView(CVImage* image);

 
	virtual void setText(const char *text) const; 
};


extern CBaristaProjectHelper baristaProjectHelper;


#endif