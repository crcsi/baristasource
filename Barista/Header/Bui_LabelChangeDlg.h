#ifndef BUI_LABELCHANGEDLG_H
#define BUI_LABELCHANGEDLG_H
#include <qdialog.h>
#include "ui_LabelChangeDlg.h"
#include "Label.h"
#include "CharString.h"

class bui_LabelChangeDlg : 	public QDialog, public Ui_LabelChangeDlg
{Q_OBJECT
public:
	bui_LabelChangeDlg(void);
	bui_LabelChangeDlg(const char * text, const char* title = 0, bool extanded = 0);
	const char* dlglabel;
	CCharString getLabelText();

	void setSuccess(bool b) { this->success = b;};
	bool getSuccess() {return this->success;};

	void forceLabelforAllViews();

public:
	~bui_LabelChangeDlg(void);
public slots:
	//const char *getLabelText();

    void Ok_released();
    void setLabelText( const char * text );
private:
	bool success;
};

#endif // BUI_LABELCHANGEDLG_H
