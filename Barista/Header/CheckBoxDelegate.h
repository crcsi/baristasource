#ifndef CHECKBOXDELEGATE_H
#define CHECKBOXDELEGATE_H

#include <QItemDelegate>

class CCheckBoxDelegate : public QItemDelegate
{
	Q_OBJECT

public:
	CCheckBoxDelegate(QObject* parent=0);
	~CCheckBoxDelegate(){};

	void paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const;
private:
	
};

#endif // CHECKBOXDELEGATE_H
