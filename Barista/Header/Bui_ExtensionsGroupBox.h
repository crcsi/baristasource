#ifndef BUI_EXTENSIONSGROUPBOX_H
#define BUI_EXTENSIONSGROUPBOX_H

#include <QGroupBox>
#include "ui_ExtensionsGroupBox.h"
#include "TFW.h"

class Bui_ExtensionsGroupBox : public QGroupBox, public Ui_ExtensionsGroupBoxClass
{
	Q_OBJECT

public:
	Bui_ExtensionsGroupBox(QWidget *parent = 0, bool mode = false);
	~Bui_ExtensionsGroupBox();

	void setSpinBoxes();
	void setPixelSpinBoxes();
	void setMetricSpinBoxes();

	void setMetricValues();
	void setPixelValues();

	void blockSpinBoxSignals(bool block);
	void setSpinBoxDecimals(int decimals);
	void setSpinBoxStep(double step);

	void resetSpinBoxes();

	void setValues(const C2DPoint &shift, const C2DPoint &gridSize, const int cols, const int rows, const CReferenceSystem &refsys);

	void getROIfromMetrictoPixel();
	
	CTFW getTFW() const { return this->tfw; };


public slots:
	void spinBoxULXChanged( double newulx);
	void spinBoxULYChanged( double newuly);
	void spinBoxLRXChanged( double newlrx);
	void spinBoxLRYChanged( double newlry);
	void spinBoxHeightChanged( double newheight);
	void spinBoxWidthChanged( double newwidth);

	void spinBoxGridChanged(double newgrid);
	void spinBoxColsChanged(double newcols);
	void spinBoxRowChanged(double newrows);


	void rbPixelReleased();
	void rbMetricReleased();

protected:
	double ulx;
	double uly;
	double lrx;
	double lry;
	double maxx;
	double maxy;
	double ncols;
	double nrows;
	double maxcols;
	double maxrows;

	double metriculx;
	double metriculy;
	double metriclrx;
	double metriclry;
	double metricminx;
	double metricminy;
	double metricwidth;
	double metricheight;
	double metricmaxx;
	double metricmaxy;
	double metricmaxwidth;
	double metricmaxheight;
	double gridsize;

	int ndigits;

	bool SwitchMode;

	CTFW tfw;

};

#endif // BUI_EXTENSIONSGROUPBOX_H
