#ifndef BUI_INTERPOLATEDEMHEIGHTDLG_H
#define BUI_INTERPOLATEDEMHEIGHTDLG_H
#include <qdialog.h>
#include "ui_InterpolateDEMHeightDlg.h"

class CVDEM;

class bui_InterpolateDEMHeightDlg : 	public QDialog, public Ui_InterpolateDEMHeightDlg
{
Q_OBJECT
public:
	bui_InterpolateDEMHeightDlg(void);
	~bui_InterpolateDEMHeightDlg(void);

	double x;
	double y;
	double z;

	CVDEM* dem;

public slots:

	void setX( double x );
    void setY( double y );
    void setZ( double z );
    void setDEM( CVDEM* dem );

    double getX();
    double getY();
    double getZ();

    void getHeight_released();
    void Close_released();

};

#endif // BUI_InterpolateDEMHeightDlg_H

