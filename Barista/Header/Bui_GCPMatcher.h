#ifndef BUI_GCPMatcher_H
#define BUI_GCPMatcher_H

#include <qdialog.h>
#include "ui_GCPMatching.h"
#include "2DPoint.h"
#include "ReferenceSystem.h"

class CVImage;
class CXYZPointArray;
class CGCPMatcher;
class CMatchingParameters;
class FeatureExtractionParameters;
class CVDEM;

class bui_GCPMatchingDialog : 	public QDialog , public Ui_GCPMatchingDialog
{Q_OBJECT

  protected:

	  bool success;

	  bool extractFeatures;

	  double sceneSizePercentage;
	  int ScenesBridged;
	  int nPoints;

	  vector<CVImage *> possibleOriginalImages;
	  vector<CVImage *> possibleOrthoImages;

	  CVDEM *pDEM;

	  bool useWallis;

	  eCoordinateType GCPType;

	  bool setGCP;

	  CGCPMatcher *pGCPmatcher;

  public:

	  bui_GCPMatchingDialog ();

	  bui_GCPMatchingDialog (CXYZPointArray *GCPs);

	  ~bui_GCPMatchingDialog();

	  int MatchPoints();

	  bool isInitialised() const;

	  bool getSuccess() const { return this->success; };

  public slots:
	
	  void ImageSelected(int row);
	  void ImageClicked(QListWidgetItem *item);

	  void OrthoImageSelected(int row);
	  void OrthoImageClicked(QListWidgetItem *item);

	  void PatchSizeChanged();
	  void RhoMinChanged();
	  void SearchAreaChanged();
	  void MatchParsSelected();


	  void SceneSizePercentageChanged();
	  void NScenesChanged();
	  void NPointsChanged();
	  void FEXParsSelected();
	  void DEMChanged();
	  void CoordSysChanged();
	  void GCPchanged();

	  void WallisFilterClicked();

	  void OK();

	  void Cancel();

	  void Help();


  protected: 

	  void init();

	  void init(CXYZPointArray *GCPs);

	  void setCurrentOrtho(int index);
	  void initPossibleOrthos();

	  void checkSetup();

	  void setMatchParGUI(const CMatchingParameters &pars);

	  void setFEXParGUI();

	  void setCoordinateTypeGUI();

	  void initDEMs();
};

#endif // BUI_GCPMatcher_H