#ifndef BUI_PANSHARPEN_H
#define BUI_PANSHARPEN_H
#include <qdialog.h>
#include "ui_Pansharpen.h"
//#include "BaristaProject.h"
#include "ImgBuf.h"

class CVImage;
class CVDEM;
class FileName;
class CProgressListener;
class CProgressThread;
class bui_ImageSensorSelector;
class Bui_GDALExportDlg;
class Bui_GDALExportDlg;
class CTrans2D;
class CTrans3D;
class CImageFile;

enum ePanSharpenRegisterMode { eASPAN, eOWN, eAUTOMATIC };


class bui_PanDialog : 	public QDialog , public Ui_PanDialog
{Q_OBJECT

  protected:

	  bool isOrtho;

	  eResamplingType resamplingType;

	  bui_ImageSensorSelector *panSelector;

	  CVImage* panImage;

	  CVDEM *DEM;

	  bui_ImageSensorSelector *multiSelector;

	  CVImage* multiImage;

	  bool PCA;

	  CFileName pansharpenname;

	  CVImage* pansharpenedimage;

	  bool Pansharpencreated;

	  float infraredCorrection;

	  QTimer* timer;

	  CProgressThread* thread;

	  CProgressListener* listener;

	  bool finished;

	  Bui_GDALExportDlg *exportDialog;

	  ePanSharpenRegisterMode registerMode;

	  CTrans2D *trafo;

	  bool hasPansharpen;

	  C2DPoint llc, ruc, grid;

  public:
	  

	  bui_PanDialog (bool orthoMode);

	  ~bui_PanDialog();

	  void setPANsharpenedImage(CVImage *panImg);
	
	  const char *getPansharpFileName() const;

	  bool panSharpIsCreated() const { return Pansharpencreated; };

	  void exportImage();

	  bool panSharpen();
	
  public slots:

	  void onPanImageChanged();

	  void onMultiImageChanged();

	  void DEMSelected();

	  void onIRbuttonClicked();

	  void onIRparChanged();

	  void openOutputImage();
 
	  void algorithmChanged();

	  void interpolationMethodChanged();

	  void asPanSelected();
	  void ownSelected();
	  void automaticSelected();

	  void OrthoLlcChanged();
	  void OrthoRucChanged();
	  void OrthoResChanged();

	  void initDEMBorders();

	  bool automaticRegistration();

	  void Cancel();

	  void Help();
	
	  void slotTimeout();

	  void ComputePansharpenImage();
	 


  protected: 

	  void checkSetup(bool setRegistrationDefaults);

	  void init();

	  void setOrthoLimits(bool checkDEM);

	  bool LoopTiles(CTrans3D *transPan, CTrans3D *transMulti, float panFactor, double *rotPCA, float *meanValPCA,
		             int panLevel, int multiLevel, int panFact, int multiFact);

	  bool computeImgCoordsForTile(C2DPoint &firstDEMPoint, double *xPanArray, double *yPanArray, 
								   double *xMSArray, double *yMSArray, int PanFact, int MsFact,	
								   CTrans3D *Pantrafo, CTrans3D *MStrafo,
								   double &xminPan, double &xmaxPan,double &yminPan, double &ymaxPan,								
								   double &xminMS, double &xmaxMS,double &yminMS, double &ymaxMS);

	  void Ortho8bitTileBilin(unsigned char *orthoBuf, double *xArray, double *yArray, 
							  const CImageBuffer &imgBuf);

	  void Ortho16bitTileBilin(unsigned short *orthoBuf, double *xArray, double *yArray, 
							   const CImageBuffer &imgBuf);

	  void Ortho8bitTileBicub(unsigned char *orthoBuf, double *xArray, double *yArray, 
							  const CImageBuffer &imgBuf);

	  void Ortho16bitTileBicub(unsigned short *orthoBuf, double *xArray, double *yArray, 
							   const CImageBuffer &imgBuf);

	  void destroy();

};

#endif // BUI_PANSHARPEN_H