#ifndef __CZoomInHandler__
#define __CZoomInHandler__
#include "BaristaViewEventHandler.h"

class CZoomInHandler : public CBaristaViewEventHandler
{
public:
	CZoomInHandler(void);

	~CZoomInHandler(void);

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyPressEvent (QKeyEvent *e, CBaristaView *v)
	{
		return CBaristaViewEventHandler::keyPressEvent(e, v);
	};

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
	{
		return CBaristaViewEventHandler::keyReleaseEvent(e, v);
	};

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) { return false; };
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool redraw(CBaristaView *v);
	virtual void initialize(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setZoomInCursor();};

	virtual void open();

	virtual void close();
};


#endif