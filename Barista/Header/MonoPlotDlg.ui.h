/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/
#include "ReferenceSystem.h"
#include "XYZPoint.h"
#include "UTMToGeographic.h"
#include "UTMToGeocentric.h"
#include "WGS84GeocentricToWGS84Geographic.h"
#include "WGS84GeographicToWGS84Geocentric.h"
#include "WGS84Spheroid.h"
#include "GRS1980Spheroid.h"
#include "GeographicToUTM.h"
#include "GeographicToTM.h"
#include "GeocentricToUTM.h"
#include "GeocentricToTM.h"

void MonoPlotDlg::utmsetchecked()
{
    this->coordsys = UTM;
    this->UTMButton->setChecked(true);
}

void MonoPlotDlg::localxyzsetchecked()
{
    this->coordsys = LOCAL_XYZ;
    this->localxyzButton->setChecked(true);   
}

void MonoPlotDlg::geographicsetchecked()
{
    this->coordsys = WGS84_GEOGRAPHIC;
    this->geographicButton->setChecked(true);
}

void MonoPlotDlg::geocentricsetchecked()
{
    this->coordsys = WGS84_GEOCENTRIC;
    this->geocentricButton->setChecked(true);
}


void MonoPlotDlg::LabelValueSetText(const char* label)
{
    this->Labelvalue->setText( label);
}

void MonoPlotDlg::setXValue( double X)
{
    this->Xvalue->setText(QString( "%1" ).arg(X, 0, 'F', 6 ));
}

void MonoPlotDlg::setYValue(double Y)
{
    this->Yvalue->setText(QString( "%1" ).arg(Y, 0, 'F', 6 ));
}

void MonoPlotDlg::setZValue( double Z )
{
    this->Zvalue->setText(QString( "%1" ).arg(Z, 0, 'F', 6 ));
}

void MonoPlotDlg::setDlgValues( double X, double Y, double Z, const char * label, int coordsys )
{
    this->X = X;
    this->Y = Y;
    this->Z = Z;
    this->plabel = label;
    this->coordsys = coordsys;
      
    //  get coordinate system information
    if(this->coordsys == WGS84_GEOCENTRIC)
    {
        this->Xvalue->setText(QString( "%1" ).arg(this->X, 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(this->Y, 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(this->Z, 0, 'F', 4 ));
        this->Labelvalue->setText( this->plabel);

        this->geocentricsetchecked();
    }
    if(this->coordsys == WGS84_GEOGRAPHIC)
    {
        this->Xvalue->setText(QString( "%1" ).arg(this->X, 0, 'F', 8 ));
        this->Yvalue->setText(QString( "%1" ).arg(this->Y, 0, 'F', 8 ));
        this->Zvalue->setText(QString( "%1" ).arg(this->Z, 0, 'F', 4 ));
        this->Labelvalue->setText( this->plabel);

        this->geographicsetchecked();
    }
    if(this->coordsys == UTM)
    {
        this->Xvalue->setText(QString( "%1" ).arg(this->X, 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(this->Y, 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(this->Z, 0, 'F', 4 ));
        this->Labelvalue->setText( this->plabel);
  
        this->utmsetchecked();
    }
    if(this->coordsys == TM)
    {
        this->Xvalue->setText(QString( "%1" ).arg(this->X, 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(this->Y, 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(this->Z, 0, 'F', 4 ));
        this->Labelvalue->setText( this->plabel);
  
        this->tmsetchecked();
    }
    if(this->coordsys == LOCAL_XYZ)
    {
        this->Xvalue->setText(QString( "%1" ).arg(this->X, 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(this->Y, 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(this->Z, 0, 'F', 4 ));
        this->Labelvalue->setText( this->plabel);

        this->localxyzsetchecked();
    }
    this->repaint();
}


void MonoPlotDlg::UTMButtonclicked()
{
    CXYZPoint inpoint(this->X, this->Y, this->Z);
    CXYZPoint outpoint(this->X, this->Y, this->Z);

    CTrans3D* trans3d = NULL;
    CWGS84Spheroid spheroid;

    // from WGS84_GEOCENTRIC to UTM
    if(this->coordsys == WGS84_GEOCENTRIC)
    {
        trans3d = new CGeocentricToUTM();
        trans3d->transform(&outpoint, &inpoint);

        this->Xvalue->setText(QString( "%1" ).arg(outpoint.getX(), 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(outpoint.getY(), 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(outpoint.getZ(), 0, 'F', 4 ));
        this->repaint();
    }
    // from WGS84_GEOGRAPHIC to UTM
    if(this->coordsys == WGS84_GEOGRAPHIC)
    {
        trans3d = new CGeographicToUTM(&spheroid);
        trans3d->transform(&outpoint, &inpoint);

        this->Xvalue->setText(QString( "%1" ).arg(outpoint.getX(), 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(outpoint.getY(), 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(outpoint.getZ(), 0, 'F', 4 ));
        this->repaint();
    }
    if(this->coordsys == UTM)
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel, this->coordsys);
    }
    if(this->coordsys == LOCAL_XYZ)
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel, this->coordsys);
    }
}


void MonoPlotDlg::localxyzButtonclicked()
{
    // do nothing to point, but reset original radiobutton
    if(this->coordsys == WGS84_GEOCENTRIC)
    {
        this->geocentricsetchecked();
    }
    if(this->coordsys == WGS84_GEOGRAPHIC)
    {
        this->geographicsetchecked();
    }
    if(this->coordsys == UTM)
    {
        this->utmsetchecked();
    }

    this->setDlgValues(this->X, this->Y,this->Z, this->plabel, this->coordsys);

    this->repaint();
}


void MonoPlotDlg::geocentricButtonclicked()
{
    CXYZPoint inpoint(this->X, this->Y, this->Z);
    CXYZPoint outpoint;

    CTrans3D* trans3d = NULL;
    CWGS84Spheroid spheroid;

    if(this->coordsys == WGS84_GEOCENTRIC)
    {       
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel, this->coordsys);       
    }

    // from WGS84_GEOGRAPHIC to WGS84_GEOCENTRIC
    else if(this->coordsys == WGS84_GEOGRAPHIC)
    {      
        trans3d = new CWGS84GeographicToWGS84Geocentric();
        trans3d->transform(&outpoint, &inpoint);

        this->Xvalue->setText(QString( "%1" ).arg(outpoint.getX(), 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(outpoint.getY(), 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(outpoint.getZ(), 0, 'F', 4 ));
        this->repaint();
    }

    // from UTM to WGS84_GEOCENTRIC
    else if(this->coordsys == UTM)
    {
        trans3d = new CUTMToGeocentric(&spheroid, this->zone, this->hemisphere);
        trans3d->transform(&outpoint, &inpoint);

        this->Xvalue->setText(QString( "%1" ).arg(outpoint.getX(), 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(outpoint.getY(), 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(outpoint.getZ(), 0, 'F', 4 ));
        this->repaint();
    }
    else if(this->coordsys == LOCAL_XYZ)
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel, this->coordsys);       
    }
}

void MonoPlotDlg::geographicButtonclicked()
{
    CXYZPoint inpoint(this->X, this->Y, this->Z);
    CXYZPoint outpoint;

    CTrans3D* trans3d = NULL;
    CWGS84Spheroid spheroid;

    // from WGS84_GEOCENTRIC to WGS84_GEOGRAPHIC
    if(this->coordsys == WGS84_GEOCENTRIC)
    {
        trans3d = new CWGS84GeocentricToWGS84Geographic();
        trans3d->transform(&outpoint, &inpoint);

        this->Xvalue->setText(QString( "%1" ).arg(outpoint.getX(), 0, 'F', 8 ));
        this->Yvalue->setText(QString( "%1" ).arg(outpoint.getY(), 0, 'F', 8 ));
        this->Zvalue->setText(QString( "%1" ).arg(outpoint.getZ(), 0, 'F', 4 ));
        this->repaint();   
        
    }
    if(this->coordsys == WGS84_GEOGRAPHIC)
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel, this->coordsys);        
    }
    // from UTM to WGS84_GEOGRAPHIC
    if(this->coordsys == UTM)
    {       
        trans3d = new CUTMToGeographic(&spheroid, this->zone, this->hemisphere );
        trans3d->transform(&outpoint, &inpoint);

        this->Xvalue->setText(QString( "%1" ).arg(outpoint.getX(), 0, 'F', 8 ));
        this->Yvalue->setText(QString( "%1" ).arg(outpoint.getY(), 0, 'F', 8 ));
        this->Zvalue->setText(QString( "%1" ).arg(outpoint.getZ(), 0, 'F', 4 ));
        this->repaint();       
    }
    if(this->coordsys == LOCAL_XYZ)
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel, this->coordsys);         
    }
}


void MonoPlotDlg::setZone( int zone )
{ 
    this->zone = zone;
}

void MonoPlotDlg::setHemisphere( int hemisphere )
{
    this->hemisphere = hemisphere;
}

const char* MonoPlotDlg::changeLabel_clicked()
{
    return this->Labelvalue->text();
}

double MonoPlotDlg::changeHeight_clicked()
{
    return this->Zvalue->text().toDouble();
}



void MonoPlotDlg::TMButtonclicked()
{
    CXYZPoint inpoint(this->X, this->Y, this->Z);
    CXYZPoint outpoint(this->X, this->Y, this->Z);

    CTrans3D* trans3d = NULL;
    CGRS1980Spheroid spheroid;

    // from WGS84_GEOCENTRIC to TM
    if(this->coordsys == WGS84_GEOCENTRIC)
    {
        trans3d = new CGeocentricToTM(&spheroid, this->centralMeridian, this->latitudeOrigin);
        trans3d->transform(&outpoint, &inpoint);

        this->Xvalue->setText(QString( "%1" ).arg(outpoint.getX(), 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(outpoint.getY(), 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(outpoint.getZ(), 0, 'F', 4 ));
        this->repaint();
    }
    // from WGS84_GEOGRAPHIC to TM
    if(this->coordsys == WGS84_GEOGRAPHIC)
    {
  trans3d = new CGeographicToTM(&spheroid, this->centralMeridian, this->latitudeOrigin);
        trans3d->transform(&outpoint, &inpoint);

        this->Xvalue->setText(QString( "%1" ).arg(outpoint.getX(), 0, 'F', 4 ));
        this->Yvalue->setText(QString( "%1" ).arg(outpoint.getY(), 0, 'F', 4 ));
        this->Zvalue->setText(QString( "%1" ).arg(outpoint.getZ(), 0, 'F', 4 ));
        this->repaint();
    }
    if(this->coordsys == TM)
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel, this->coordsys);
    }
    if(this->coordsys == LOCAL_XYZ)
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel, this->coordsys);
    }
}


void MonoPlotDlg::setCentralMeridian( double centralMeridian )
{
    this->centralMeridian =  centralMeridian;
}


void MonoPlotDlg::setLatitudeOrigin( double latitudeOrigin )
{
    this->latitudeOrigin = latitudeOrigin ;
}


void MonoPlotDlg::tmsetchecked()
{
   this->coordsys = TM;
    this->TMButton->setChecked(true);
}
