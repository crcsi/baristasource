#ifndef __CXYEllipseTable__
#define __CXYEllipseTable__


#include "BaristaTable.h"


class CXYEllipseArray;

class CXYEllipseTable : public CBaristaTable
{
    Q_OBJECT // needed to connect signals / slots
public:
	CXYEllipseTable(CXYEllipseArray* ellipses,QWidget* parent = 0);
	~CXYEllipseTable(void);

    void setEllipses(CXYEllipseArray* ellipses);
    CXYEllipseArray* getEllipses(){	return this->ellipses;}

	virtual void initTable();
	virtual void elementAdded(CLabel element);
    virtual void elementDeleted(CLabel element);

    void closeEvent ( QCloseEvent * e );
protected:
    CXYEllipseArray* ellipses;

 
public slots:
	void changedValue( int row, int col );
	void contextMenuEvent(QContextMenuEvent * e);
	void removefromTable ();
};

#endif