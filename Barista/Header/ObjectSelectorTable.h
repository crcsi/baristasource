#ifndef _CObjectSelectorTable_
#define _CObjectSelectorTable_

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "ObjectTable.h"
#include "ObjectSortFilterModel.h"
#include <vector>
#include "UpdateListener.h"
#include <QModelIndexList>

class CObsPointArray;
class CBaristaView;
class CTrans3D;
class CSensorModel;

typedef struct {
	CBaristaView* view;
	CTrans3D* trafo;
	const CSensorModel* sensorModel;
	vector<CObjectSortFilterModel*> arrays;
} ObjectData;


class CObjectSelectorTable : public CObjectTable, public CUpdateListener
{
	Q_OBJECT
public:
	CObjectSelectorTable(CObjectDataModel* dataModel,bool isObjectSelectorOnly,QWidget* parent);
	~CObjectSelectorTable(void);

	virtual void elementAdded(CLabel element="");
    virtual void elementDeleted(CLabel element="");
	virtual void elementsChanged(CLabel element=""){};

public slots:
	void selectAllObjects();
	void selectObjects(bool zoom,bool renameObject);
	void dataModelChanged(DataModelChanges changeType,vector<QString> parameter);
	void zoomToObject();
	void setCurrentImageLabel();

protected:

	virtual void mouseDoubleClickEvent ( QMouseEvent * event ); 
	void zoomToPoint(CBaristaView* view, CObsPoint& point);
	void deleteDataSet(int index);
	virtual void mouseReleaseEvent ( QMouseEvent * mouseEvent );

	void initView(CBaristaView* view);


	// obspoint functions
	void initObsPointDataSet(ObjectData& dataSet, CBaristaView* view);
	void initPolyLineDataSet(ObjectData& dataSet,CBaristaView* view);
	
	bool isObjectSelectorOnly;
	vector<ObjectData> views;


};
#endif