#ifndef __CImageTable__
#define __CImageTable__


#include "BaristaTable.h"

#include "VImage.h"

class CImageTable : public CBaristaTable
{
    Q_OBJECT // needed to connect signals / slots
public:
	CImageTable(CVImage* image,QWidget* parent = 0);
	~CImageTable(void);


    void setImage(CVImage* image);
    CVImage* getImage()	{return this->vimage;}
    
	virtual void initTable();
	virtual void elementAdded(CLabel element);
    virtual void elementDeleted(CLabel element);

    void closeEvent ( QCloseEvent * e );

protected:
    CVImage* vimage;

	int imageTableRowCount;

	QString getSensorModelName();
};

#endif