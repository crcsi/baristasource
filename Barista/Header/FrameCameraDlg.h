#ifndef FRAMECAMERADLG_H
#define FRAMECAMERADLG_H

#include <QDialog>
#include "ui_FrameCameraDlg.h"

class FrameCameraDlg : public QDialog
{
	Q_OBJECT

public:
	FrameCameraDlg(QWidget *parent = 0);
	~FrameCameraDlg();

private:
	Ui::FrameCameraDlgClass ui;
};

#endif // FRAMECAMERADLG_H
