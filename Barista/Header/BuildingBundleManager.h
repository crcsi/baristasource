#ifndef __CBuildingBundleManager__
#define __CBuildingBundleManager__

/**
 * Title:        Barista
 * Description:
 * Copyright:    Copyright (c) Franz Rottensteiner, Harry Hanley, Paul Dare, Simon Cronk
 * Company:      University of Melbourne
 * @author Franz Rottensteiner, Harry Hanley
 * @version 1.0
 */

#pragma once
#include <vector>

#include "BundleManager.h"
#include "XYZPoint.h"
#include "OmegaPhiKappaRotation.h"

class CBuilding;

class CBuildingBundleManager : public CBundleManager
{
  protected:

	  CBuilding *pBuilding; 

	  vector<CObservationHandler *> obsVec;

	  CXYZPoint ERP;

	  COmegaPhiKappaRotation ROT;


  public:

	  CBuildingBundleManager(CBuilding *building, float I_convergenceLimit = 0.0001, 
		                     float MaxRobustPercentage = 0.03, float MinRobFac = 3.0, 
							 float havg = 0.0, const CCharString &protfile = "");

	  virtual ~CBuildingBundleManager();

	  virtual void bundleInit(bool forwardOnly);

	  virtual bool solve(bool forwardOnly, bool robust, bool omitMarked);

  protected: 
	   
	  virtual void initObservations();

	  virtual void initParameters(bool forwardOnly);

};

#endif