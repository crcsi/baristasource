/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/



void MeasureHeightDlg::setHeightValue( double height )
{
    this->HeightValue->setText(QString( "%1" ).arg(height, 0, 'F', 4 ));

}


void MeasureHeightDlg::Ok_clicked()
{
    this->close();
}
