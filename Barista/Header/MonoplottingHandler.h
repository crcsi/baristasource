#ifndef __CMonoplottingHandler__
#define __CMonoplottingHandler__
#include "BaristaViewEventHandler.h"
#include "MonoPlotter.h"

class CVImage;
class CVDEM;
class CSensorModel;

class CMonoplottingHandler : public CBaristaViewEventHandler
{
protected:
	CMonoplottingHandler(void){};
public:

	virtual ~CMonoplottingHandler(void){};

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v) = 0;
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) = 0;
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v) = 0;

	virtual void open();
	virtual void close();

	virtual bool redraw(CBaristaView *v);
	virtual void initialize(CBaristaView *v);

protected:
	void removeDrawingDataSets();
	void removeDrawingDataSet(const CLabel& dataSetName);
	void removePointFromDataSet(const CLabel& dataSetName,const CLabel& point);

	bool backprojectPoint(CBaristaView *v,CXYPoint& p2DPoint,const CXYZPoint& p3DPoint);

	void drawDEMBorder(CBaristaView * v);

	void changeColor(const CLabel& dataSetName,QColor& color);

	CMonoPlotter monoPlotter;
};

#endif
