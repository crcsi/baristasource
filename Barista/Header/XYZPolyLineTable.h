#ifndef __CXYZPolyLineTable__
#define __CXYZPolyLineTable__

#include "BaristaTable.h"

class CXYZPolyLineArray;

class CXYZPolyLineTable : public CBaristaTable
{
    Q_OBJECT // needed to connect signals / slots
public:
	CXYZPolyLineTable(CXYZPolyLineArray* lines,QWidget* parent = 0);
	~CXYZPolyLineTable(void);

    void setLines(CXYZPolyLineArray* lines);
    CXYZPolyLineArray* getLines(){return this->lines;}

	virtual void initTable();
	virtual void elementAdded(CLabel element);
    virtual void elementDeleted(CLabel element);

    void closeEvent ( QCloseEvent * e );
protected:
    CXYZPolyLineArray* lines;
 
	int xdezimals;
	int ydezimals;

	int getNumberOfSelectedRows();

public slots:
	void changedValue( int row, int col );
	void contextMenuEvent(QContextMenuEvent * e);
	void removefromTable ();
	void itemSelectionChanged ();
	void XYZPolyLinesExtractFeatures();


};

#endif