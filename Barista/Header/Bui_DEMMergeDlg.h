#ifndef BUI_DEMMERGEDLG_H
#define BUI_DEMMERGEDLG_H

#include <QDialog>
#include "ui_DEMMergeDlg.h"
#include "Filename.h"
#include <vector>
#include "TFW.h"

class CFileTable;
class CVDEM;
class Bui_ExtensionsGroupBox;
class Bui_GDALExportDlg;

class Bui_DEMMergeDlg : public QDialog, public Ui_DEMMergeDlgClass
{
	Q_OBJECT

public:
	Bui_DEMMergeDlg();
	~Bui_DEMMergeDlg();

	void setSuccess(bool b) { this->success = b;};
	bool getSuccess() {return this->success;};

	bool createMergedDEM();
	bool merge();
	void checkSetup();
	void updateMasterOptions();
	void updateDEMSelection();
	void updateExtensionValues();

	bool checkReferenceSystems();
	bool getTFWInfo();

public slots:
	
	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBSelectOutputReleased();

	void DEMSelectionChanged();
	void DEMSelectionChanged(int i, int j);
	void MasterSelectionChanged(int i);
	void getMasterSelection();


private:
	CFileTable* projectdems;
	Bui_ExtensionsGroupBox* gbextensions;
	CFileName mergedname;

	int masterIndex;
	int firstIndex;
	bool success;
	QVBoxLayout *DEMListLayout;
	QVBoxLayout *ExtensionLayout;

	vector<bool> demselection;
	vector<CVDEM*> mergeDEMList;

	double gridx;
	double gridy;
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	double width;
	double height;
	int cols;
	int rows;

	double xminB;
	double xmaxB;
	double yminB;
	double ymaxB;


protected:
	Bui_GDALExportDlg* dlg;
	CVDEM* newdem;
};

#endif // BUI_DEMMERGEDLG_H
