#ifndef BUI_DIFFERENCEUNITDLG_H
#define BUI_DIFFERENCEUNITDLG_H
#include <qdialog.h>
#include "ui_DifferenceUnitDlg.h"

class bui_DifferenceUnitDlg :	public QDialog, public Ui_DifferenceUnitDlg
{Q_OBJECT
public:
	bui_DifferenceUnitDlg(void);
	void setDegreeChecked();
	void setMeterChecked();

public:
	~bui_DifferenceUnitDlg(void);
public slots:
	void OK_released();
    bool meterChecked();
    bool degreeChecked();

};

#endif //BUI_DIFFERENCEUNITDLG_H