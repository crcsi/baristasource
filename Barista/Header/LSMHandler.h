#ifndef __CLSMHandler__
#define __CLSMHandler__
 
#include "DigitizingHandler.h"
#include "BaristaLSM.h"
#include "Bui_MonoPlotDlg.h"

class CLSMHandler : public CDigitizingHandler
{
public:
	CLSMHandler();
	
	virtual ~CLSMHandler();

	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	virtual void handleLeaveEvent ( QEvent * event, CBaristaView *v );

	virtual void handleEnterEvent ( QEvent * event, CBaristaView *v );

	virtual void setCurrentCursor(CBaristaView *v) { v->setCrossCursor();};

	virtual void open();
	virtual void close();

	void openMatchingParameterDlg();

protected:
	bui_MonoPlotDlg matchdlg;
	CBaristaLSM lsm;
};

#endif