#ifndef BUI_NEWPROJECT_H
#define BUI_NEWPROJECT_H

#include <QDialog>
#include "ui_Bui_NewProject.h"
#include "Filename.h"

class Bui_NewProject : public QDialog, public Ui_Bui_NewProjectClass
{
	Q_OBJECT

public:
	Bui_NewProject(QWidget *parent = 0, bool isnewChipDb = false);
	~Bui_NewProject();

	bool getSuccess() {return this->success;};
	CFileName getFileName() 
	{
		return this->iFileName;
	};

	void setspinboxMeanHeight(double height);

public slots:
	void pBCancelReleased();
	void pBOkReleased();
	void pBHelpReleased();
	void pBFileNameReleased();
	void leTextChanged( const QString & text );
	int exec();
protected:
	void applySettings();

	void checkAvailabilityOfSettings();



private:
	CFileName iFileName;
	bool success;
	//addition on 15 Feb 2010
	bool isnewChipDB;
};

#endif // BUI_NEWPROJECT_H
