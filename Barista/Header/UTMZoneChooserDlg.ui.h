/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

void UTMZoneChooserDlg::initZoneListBox()
{
    int i = 0;
    while( ++i <= 60 )
    {
        this->ZoneListBox->insertItem( QString::fromLatin1( "Zone " ) + QString::number( i ), i );      
    }
    
    if (this->previouszone != -1)
    {
		this->ZoneListBox->setSelected( this->previouszone , true);
	}
    this->ZoneListBox->setSelectionMode(QListBox::Single );
}

int UTMZoneChooserDlg::getSelectedZone()
{
    // plus 1, due index is zero based
    return (this->ZoneListBox->index(this->ZoneListBox->selectedItem()) + 1);
}

void UTMZoneChooserDlg::Ok_clicked()
{
    this->close();
}


void UTMZoneChooserDlg::setpreviousZone( int zone )
{
    this->previouszone = zone-1;
}
