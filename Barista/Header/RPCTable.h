#ifndef __CRPCTable__
#define __CRPCTable__


#include "BaristaTable.h"

class CRPC;

class CRPCTable : public CBaristaTable
{
private:
    Q_OBJECT // needed to connect signals / slots
public:
	CRPCTable(CRPC* rpc,QWidget* parent = 0);
	~CRPCTable(void);

    void setRPC(CRPC* rpc);
    CRPC* getRPC(){	return this->rpc;}
    	
	virtual void initTable();
	virtual void elementAdded(CLabel element){};
	virtual void elementDeleted(CLabel element){};

    void closeEvent ( QCloseEvent * e );
protected:
    CRPC* rpc;

};

#endif