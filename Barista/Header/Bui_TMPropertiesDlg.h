#ifndef BUI_TMPROPERTIESDLG_H
#define BUI_TMPROPERTIESDLG_H
#include <qdialog.h>
#include "ui_TMPropertiesDlg.h"

class CSpheroid;
class CReferenceSystem;

class Bui_TMPropertiesDlg :	public QDialog, public Ui_TMPropertiesDlgClass
{
Q_OBJECT
public:
	Bui_TMPropertiesDlg(CReferenceSystem* refSystem, const char *title, const char* helpfile);
	~Bui_TMPropertiesDlg(void);

	bool getSuccess() {return this->success;};
	void setSuccess(bool b) {this->success = b;};

	void initDlg();

public slots:
	void checkValues();
	void checkValues(const QString& text);

	void pBApplySettings();
	void pBCancelReleased();

	void callHelp();
private:
	CReferenceSystem* refSys;
	const char* helpFile;
	bool success;
};

#endif