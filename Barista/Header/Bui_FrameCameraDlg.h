#ifndef __Bui_FrameCameraDlg__
#define __Bui_FrameCameraDlg__

#include <qdialog.h>
#include "ui_FrameCameraDlg.h"
#include <vector>
#include <QItemDelegate>
#include <QStandardItemModel>
#include "ObsPoint.h"
#include "FrameCameraAnalogue.h"
#include "AdjustmentDlg.h"


class CFiducialDataModel;

class Bui_FrameCameraDlg : public CAdjustmentDlg,public Ui_FrameCameraDlgClass
{ 
	Q_OBJECT
public:
	Bui_FrameCameraDlg(CFrameCamera* existingCamera = NULL,QWidget *parent = 0);

	~Bui_FrameCameraDlg(void);

	bool getSuccess() {return this->success;};

	CFrameCamera& getCamera() {return *this->currentCamera;};


	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString() {return "";};
	virtual void cleanUp();
signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

public slots:
	void pbAddDistortionReleased();
	void pbDeleteDistortionReleased();
	void pbAddFiducialReleased();
	void pbDeleteFiducialReleased();

	void leTextChanged(const QString & text);
	
	void deDateDateChanged ( const QDate & date );

	void rbDigitalReleased();
	void rbAnalogueReleased();

	void modelSelectionChanged();
	void tVDistortionHeaderClicked(int logicalIndex  ); 
	void tVDistortionHeaderClicked(const QModelIndex& index ); 

	void tVFiducialMarksHeaderClicked(int logicalIndex  ); 
	void tVFiducialMarksClicked(const QModelIndex& index ); 

	void leXAutoEditingFinished();
	void leYAutoEditingFinished();
	void leXSymmetryEditingFinished();
	void leYSymmetryEditingFinished();
	void leFocalLengthEditingFinished();
	void leNameEditingFinished();
	void leIDEditingFinished();
	void deDateEditingFinished();
	void lePixelSizeEditingFinished();

private slots:
	virtual void elementsChangedSlot(CLabel element);

private:
	
	void setNoDistortionModel();
	void setRadialDistortionModel();

	void applySettings();
	void checkAvailabilityOfSettings();
	
	void initFiducialMarks(const CXYPointArray& fiducialMarks);

	bool success;
	int selectedDistortionModel;

	QStandardItemModel *radialModel;
	CFiducialDataModel* fiducialModel;

	CFrameCamera* currentCamera;
	CFrameCamera digitalCamera;
	CFrameCameraAnalogue analogueCamera;

	CFrameCamera* existingCamera;
};



// ###############################################################################################

class CFiducialDataModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	CFiducialDataModel(QObject* parent =0) : QAbstractTableModel(parent), camera(0) {};
	void setCamera(CFrameCameraAnalogue* camera);
	int rowCount(const QModelIndex& parent) const;
	int columnCount(const QModelIndex& parent) const;
	QVariant headerData(int section,Qt::Orientation orientation, int role) const;
	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	bool setData(const QModelIndex &index, const QVariant &value,int role = Qt::EditRole);
	bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
	bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());


private:
	CFrameCameraAnalogue* camera;
	
};


#endif
