/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

#include <qfiledialog.h> 
#include "orthodialog.h"
#include <qmessagebox.h> 
#include "HugeImage.h"
#include "stdlib.h"
#include "BaristaProject.h"

#define  TIFFbyte   1
#define  TIFFascii   2
#define  TIFFshort   3
#define  TIFFlong   4
#define  TIFFrational  5

/* Tiff tag names */
#define  NewSubFile    254
#define  SubfileType   255
#define  ImageWidth    256
#define  ImageLength    257
#define  RowsPerStrip   278
#define  StripOffsets   273
#define  StripByteCounts  279
#define  SamplesPerPixel  277
#define  BitsPerSample   258
#define  Compression   259
#define  PlanarConfiguration  284
#define  Group3Options   292
#define  Group4Options   293
#define  FillOrder    266
#define  Thresholding   263
#define  CellWidth    264
#define  CellLength   265
#define  MinSampleValue  280
#define  MaxSampleValue      281
#define  PhotometricInterp    262
#define  GrayResponseUnit     290
#define  GrayResponseCurve    291
#define  ColorResponseUnit    300
#define  ColorResponseCurves  301
#define  XResolution   282
#define  YResolution   283
#define  ResolutionUnit  296
#define  Orientation   274
#define  DocumentName   269
#define  PageName    285
#define  XPosition    286
#define  YPosition    287
#define  PageNumber   297
#define  ImageDescription  270
#define  Make      271
#define  Model     272
#define  FreeOffsets   288
#define  FreeByteCounts  289
#define  ColorMap    320
#define  Artist    315
#define  DataTime    306
#define  HostComputer   316
#define  Software    305
#define  SampleFormat     339
#define  SMinSampleValue  340
#define  SMaxSampleValue  341

#define TIFFdouble   12
#define GeoKeyDirectoryTag 34735
#define ModelTiepointTag 33922
#define ModelPixelScaleTag 33550
#define GeoDoubleParamsTag 34736
#define GeoAsciiParamsTag 34737

extern CBaristaProject project;

void OrthoDialog::OpenPancromaticImageFile()
{
    QString filename = QFileDialog::getOpenFileName(
            "/home",
            "Images (*.hug *.tif *.jpg)",
            this,
            "open file dialog",
            "Select a image file" );
    
   
    // make sure the file exists
    QFile test(filename);
    if (!test.exists()) return;
    test.close();

    strcpy(PanFileName, filename);
    orthoInputPanFile->setText(filename);
}

void OrthoDialog::OpenDEMFile()
{
    QString filename = QFileDialog::getOpenFileName(
            "/home",
            "DEM files (*.dtm *.g3d)",
            this,
            "open file dialog",
            "Select a DEM file" );
    
    // make sure the file exists
    QFile test(filename);
    if (!test.exists())return;
    test.close();

    strcpy(DEMFileName, filename);
    orthoInputDEMFile->setText(filename);
}

void OrthoDialog::ComputeOrthoimage()
{
    m_llx=orthoLLX->text().toDouble();
    m_lly=orthoLLY->text().toDouble();
    m_trx=orthoURX->text().toDouble();
    m_try=orthoURY->text().toDouble();

    m_gsd=orthoGResolution->text().toDouble();
    m_UTMZone=UTMZone->text().toInt();

    OrthoImgStruct os;

    os.colour       = 1;
    os.interMethod  = 1;
    os.BL_x         = m_llx;
    os.BL_y      = m_lly;
    os.TR_x      = m_trx;
    os.TR_y      = m_try;
    os.pixelsizeX   = m_gsd;
    os.pixelsizeY   = m_gsd;

    QFile ttp(PanFileName);
    if (!ttp.exists()) 
    {
        QMessageBox::warning( this, "Orthoimage Dialog",
        "cannot open Pan file\n\n",
        "Retry",
        "Quit", 0, 0, 1 );
        return;
    }
    ttp.close();

    QFile ttr(RPCFileName);
    if (!ttr.exists()) return;
    ttr.close();

    QFile ttd(DEMFileName);
    if (!ttd.exists()) return;
    ttd.close();

    fprintf(stderr, "\n%s\n", PanFileName);
    fprintf(stderr, "%s\n", OutFileName);
    fprintf(stderr, "%s\n", DEMFileName);
    fprintf(stderr, "%s\n\n", RPCFileName);

    sprintf(os.imageName,"%s", PanFileName);
    sprintf(os.outputName,"%s",OutFileName);
    sprintf(os.demName,"%s",DEMFileName);
    os.outputFormat=0;

    char filename[2048];
    sprintf(filename,"%s",DEMFileName);
    FILE *fp=fopen(filename,"r");

    dem.open(filename);

    CRPC sRPC;
    sprintf(filename, "%s", RPCFileName);

	ifstream fs(filename, ios::in | ios::binary);
	fs.seekg(0, ios::end);
	int fileLength = fs.tellg();
	if (fileLength < 0)	return;

	fs.seekg(0, ios::beg);
	char* data = new char[fileLength+1];
	::memset(data, 0, fileLength+1);

	// read entire file
	fs.read(data, fileLength);

    //switch between Ikonos and Quickbird readFormat
    // Ikonos starts with LINE_OFF: ....
    // Quickbird starts with satId ...
    if (data[0] == 'L')
    {
        sRPC.readIkonosFormat(data);
    }
    if (data[0] == 's')
    {
        sRPC.readQuickBirdFormat(data);
    }

    delete data;

    int tmpRow, tmpCol;
    CreateOrthoImageTMP( sRPC, &os, &tmpRow, &tmpCol);

    sprintf(filename,"%s.tmp",os.outputName);
    OrthoImageExtents(filename, tmpRow, tmpCol, &os);
 
    sprintf(filename,"%s.info",os.outputName);
    Raw2TIFF(filename, os.row, os.col, os.colour, os.outputName);
 
    char tfwname[512], tfwname1[512];

    for(int i=0; i<512; i++) {tfwname[i]='\0'; tfwname1[i]='\0';}
    int ftwlength=strlen(os.outputName);

    strncpy(tfwname1, os.outputName, ftwlength-4);

    sprintf(tfwname, "%s.tfw", tfwname1);

    WriteTfwFile(tfwname,os);
}

void OrthoDialog::OpenRPCFile()
{
    QString filename = QFileDialog::getOpenFileName(
            "/home",
            "RPC files (*.txt *.rpc)",
            this,
            "open file dialog",
            "Select the image RPC file" );
    
    // make sure the file exists
    QFile test(filename);
    if (!test.exists())return;
    test.close();

    strcpy(RPCFileName, filename);
    orthoInputRPCFile->setText(filename);
}

void OrthoDialog::OpenOutImage()
{
   QString filename = QFileDialog::getSaveFileName(
                    "/home",
                    "Images (*.tif *.jpg)",
                    this,
                    "save file dialog",
                    "Select a filename to save" );


    orthoOutputFile->setText(filename);

    strcpy(OutFileName, filename);
}

void OrthoDialog::init()
{
    m_llx=m_lly=m_trx=m_try=-99999.0;
    m_gsd=1.0; m_UTMZone=55;

    if(project.selectedCVImage==NULL)
    {
        sprintf(PanFileName, "%s", "");
        sprintf(DEMFileName, "%s", "");
        sprintf(RPCFileName, "%s", "");
        sprintf(OutFileName, "%s", "");
    }else
    {
        sprintf(PanFileName, "%s.hug", project.selectedCVImage->getFileName()->GetChar());
        QString filename(PanFileName);
        orthoInputPanFile->setText(filename);

        sprintf(RPCFileName, "%s", project.selectedCVImage->getRPC()->getFileName()->GetChar());
        QString panname(RPCFileName);
        orthoInputRPCFile->setText(panname);
    }

    QString ds=QString("%1").arg( m_llx, 0, 'f', 3);
    orthoLLX->setText(ds);
    orthoURX->setText(ds);
    orthoLLY->setText(ds);
    orthoURY->setText(ds);

    QString dt=QString("%1").arg(m_gsd, 0, 'f', 3);
    orthoGResolution->setText(dt);

    QString dz=QString("%1").arg(m_UTMZone);
    UTMZone->setText(dz);
}

void OrthoDialog::destroy()
{

}

void OrthoDialog::fPutWord( FILE * fp, int i )
{
   int c, c1;

   c = ((unsigned int ) i) & 0xff;  
 c1 = (((unsigned int) i)>>8) & 0xff;
   putc(c, fp);   
 putc(c1,fp);
}

void OrthoDialog::fPutLong( FILE * fp, int i )
{
    int c, c1, c2, c3;
    c  = ((unsigned int ) i)      & 0xff;  
    c1 = (((unsigned int) i)>>8)  & 0xff;
    c2 = (((unsigned int) i)>>16) & 0xff;
    c3 = (((unsigned int) i)>>24) & 0xff;

    putc(c, fp);   
    putc(c1,fp);  
    putc(c2,fp);  
    putc(c3,fp);
}

void OrthoDialog::fPutDouble( FILE * fp, double val )
{
    char *buf=(char *)&val,dest[8]; 

    dest[0] = buf[0]; 
    dest[1] = buf[1]; 
    dest[2] = buf[2]; 
    dest[3] = buf[3]; 
    dest[4] = buf[4]; 
    dest[5] = buf[5]; 
    dest[6] = buf[6]; 
    dest[7] = buf[7]; 

    fwrite(dest,8,1,fp);
}

void OrthoDialog::ProduceOrthoImageTMP( CRPC rpc, OrthoImgStruct * os, int * tr, int * tc )
{

}

int OrthoDialog::WriteOrthoImageInfo( char * filename, OrthoImgStruct os )
{
    FILE *fp;

    fp=fopen(filename,"w");
    fprintf(fp,"%lf\n",os.pixelsizeX);
    double tt=0.0;
    fprintf(fp,"%lf\n",tt);
    fprintf(fp,"%lf\n",tt);
    tt=-os.pixelsizeX;
    fprintf(fp,"%lf\n",tt);
    fprintf(fp,"%lf\n",os.BL_x);
    fprintf(fp,"%lf\n",os.TR_y);

    fclose(fp);
    return(0);
}

int OrthoDialog::ImageAlignment( char * input, int row, int col, OrthoImgStruct * os )
{
    double  input_x0,input_y0,input_x1,input_y1,x,y;
    double  output_x0,output_y0,output_x1,output_y1;
    int   i,j,bytes,k,output_row,output_col;
    char  filename[256];
    FILE  *fpi,*fpo;
    unsigned char *columno,*columni;
     
    bytes = os->colour;

    input_x0 = os->original_x;
    input_y0 = os->original_y;
    input_x1 = os->original_x+(col-1.0)*os->pixelsizeX;
    input_y1 = os->original_y+(row-1.0)*os->pixelsizeY;

    if(os->BL_x == 99999.0 || os->BL_y == 99999.0 ||
        os->TR_x == -99999.0 || os->TR_y == -99999.0) 
    {
        output_x0 = input_x0; os->BL_x = output_x0;
        output_y0 = input_y0; os->BL_y = output_y0;
        output_x1 = input_x1; os->TR_x = output_x1;
        output_y1 = input_y1; os->TR_y = output_y1;
    }else 
    {
        output_x0 = os->BL_x;
        output_y0 = os->BL_y;
        output_x1 = os->TR_x;
        output_y1 = os->TR_y;
    }

    output_row = (int)((output_y1-output_y0)/os->pixelsizeY)+1;
    output_col = (int)((output_x1-output_x0)/os->pixelsizeX)+1;
    os->row = output_row;
    os->col = output_col;
     
    output_x0 = ((int)(output_x0/os->pixelsizeX+0.5)*os->pixelsizeX);
    output_y0 = ((int)(output_y0/os->pixelsizeY+0.5)*os->pixelsizeY);
    input_x0 = ((int)(input_x0/os->pixelsizeX+0.5)*os->pixelsizeX);
    input_y0 = ((int)(input_y0/os->pixelsizeY+0.5)*os->pixelsizeY);

    columno = (unsigned char *)calloc((output_col+2)*bytes, sizeof(char));
    columni = (unsigned char *)calloc((col+2)*bytes, sizeof(char));
     
    sprintf(filename,"%s.info",os->outputName);
     
    fpo = fopen(filename,"wb");
    fpi = fopen(input,"rb");
    for(i=0; i<output_row; i++) 
    {
    for (j=0; j<output_col; j++) columno[j] = 0;
      
    y = output_y0+i*os->pixelsizeY;
    if (y >= input_y0 && y <= input_y1) 
    {
        fseek(fpi,((int)((y-input_y0)/os->pixelsizeY+0.00001))*col*bytes,SEEK_SET);
        fread(columni,col*bytes,1,fpi);
        for (j=0; j<output_col; j++) 
        {
            x = output_x0+j*os->pixelsizeX;
            if (x >= input_x0 && x <= input_x1) 
            {
                for (k=0; k<bytes; k++) 
                {
                    columno[j*bytes+k] = columni[((int)((x-input_x0)/os->pixelsizeX+0.00001))*bytes+k];
                }
            }
        }
    }
      
    fwrite(columno,output_col*bytes,1,fpo);
    }
    fclose(fpo); fclose(fpi);
    free(columno); free(columni);

    remove(input);

    return(0);
}

int OrthoDialog::WriteTIFFTag( FILE * fp, int tag, int type, long length, long offset )
{
    fPutWord(fp, tag);
    fPutWord(fp, type);
    fPutLong(fp, length);
    fPutLong(fp, offset);
      
    return (ferror(fp));
}

int OrthoDialog::Convert2GeoTIFF( char * RAWfile, int row, int col, char * TIFfile, OrthoImgStruct os )
{
   return 1;
}

int OrthoDialog::Convert2TIFF( char * RAWfile, int row, int col, int bytes, char * TIFfile )
{
    FILE            *fp, *tp;
    int             i,dpi;
    long            pos;
    unsigned char   *fpic;

    int    head=0;
       
    dpi = 300;

    fp = fopen(RAWfile,"rb");
    if (!fp) 
    {
        return -1;
    }
     
    tp = fopen(TIFfile,"wb");
    if (!tp) 
    {
        return -1;
    }
       
    fPutWord(tp, 'II');
    fPutWord(tp, 42);
    fPutLong(tp, 0L);

    if (bytes == 1) 
    {
        for (i=0; i<256; ++i) fPutWord(tp, i<<8);
        for (i=0; i<256; ++i) fPutWord(tp, i<<8);
        for (i=0; i<256; ++i) fPutWord(tp, i<<8);
    }
    else 
    {
        fPutWord(tp,8);
        fPutWord(tp,8);
        fPutWord(tp,8);
    }

    /* fill the bitmap image data */
    fpic = (unsigned char *)calloc(col*3, sizeof(unsigned char));
    if (!fpic) 
    {
        return 0;
    }
     
    for(i=0; i<row; i++) 
    {
        fseek(fp,head+(row-1-i)*col*bytes,0);
        fread (fpic, col*bytes, 1, fp);
        fwrite(fpic, col*bytes, 1, tp);
    }
     
    pos = ftell(tp);

    fPutWord(tp,11);
    WriteTIFFTag(tp, SubfileType,         TIFFshort,    1L,   1L);
    WriteTIFFTag(tp, ImageWidth,          TIFFlong ,    1L,   (long)col);
    WriteTIFFTag(tp, ImageLength,         TIFFlong ,    1L,   (long)row);
    WriteTIFFTag(tp, BitsPerSample,       TIFFshort,    1L,   8L);
    WriteTIFFTag(tp, SamplesPerPixel,     TIFFshort,    1L,   (long)bytes);
    WriteTIFFTag(tp, Compression,         TIFFshort,    1L,   1L);
    if (bytes == 1) 
    {
        WriteTIFFTag(tp, PhotometricInterp,   TIFFshort,    1L,   1L);
        WriteTIFFTag(tp, StripOffsets,        TIFFlong,     1L,   1544L);
    }
    else 
    {
        WriteTIFFTag(tp, PhotometricInterp,   TIFFshort,    1L,   2L);
        WriteTIFFTag(tp, StripOffsets,        TIFFlong,     1L,   14L);
    }
    WriteTIFFTag(tp, RowsPerStrip,        TIFFlong,     1L,   (long)row);
    WriteTIFFTag(tp, StripByteCounts,     TIFFlong,     1L,   (long)row*col*bytes);
    WriteTIFFTag(tp, SampleFormat,       TIFFshort,    1L,   1L);
    fPutLong(tp, 0L);  /* the Terminal Flag of the Tag Diectory */
    fPutLong(tp, 0L);

    fseek(tp, 4L, 0);
    fPutLong(tp, pos);
      
    free(fpic);
     
    fclose(fp); fclose(tp);
    remove(RAWfile);
    return 1;
}

void OrthoDialog::OrthoImageExtents( char * input, int row, int col, OrthoImgStruct * os )
{
    double  input_x0,input_y0,input_x1,input_y1,x,y;
    double  output_x0,output_y0,output_x1,output_y1;
    int   i,j,bytes,k,output_row,output_col;
    char  filename[256];
    FILE  *fpi,*fpo;
    unsigned char *columno,*columni;
     
    bytes = os->colour;

    input_x0 = os->original_x;
    input_y0 = os->original_y;
    input_x1 = os->original_x+(col-1.0)*os->pixelsizeX;
    input_y1 = os->original_y+(row-1.0)*os->pixelsizeY;

    if(os->BL_x == 99999.0 || os->BL_y == 99999.0 ||
        os->TR_x == -99999.0 || os->TR_y == -99999.0) 
    {
        output_x0 = input_x0; os->BL_x = output_x0;
        output_y0 = input_y0; os->BL_y = output_y0;
        output_x1 = input_x1; os->TR_x = output_x1;
        output_y1 = input_y1; os->TR_y = output_y1;
    }else 
    {
        output_x0 = os->BL_x;
        output_y0 = os->BL_y;
        output_x1 = os->TR_x;
        output_y1 = os->TR_y;
    }

    output_row = (int)((output_y1-output_y0)/os->pixelsizeY)+1;
    output_col = (int)((output_x1-output_x0)/os->pixelsizeX)+1;
    os->row = output_row;
    os->col = output_col;
     
    output_x0 = ((int)(output_x0/os->pixelsizeX+0.5)*os->pixelsizeX);
    output_y0 = ((int)(output_y0/os->pixelsizeY+0.5)*os->pixelsizeY);
    input_x0 = ((int)(input_x0/os->pixelsizeX+0.5)*os->pixelsizeX);
    input_y0 = ((int)(input_y0/os->pixelsizeY+0.5)*os->pixelsizeY);

    columno = (unsigned char *)calloc((output_col+2)*bytes, sizeof(char));
    columni = (unsigned char *)calloc((col+2)*bytes, sizeof(char));
     
    sprintf(filename,"%s.info",os->outputName);
     
    fpo = fopen(filename,"wb");
    fpi = fopen(input,"rb");

    orthoProgressBar->reset();
    orthoProgressBar->setTotalSteps(100);
    orthoProgressBar->setProgress(0);
    orthoProgressBar->show();

    for(i=0; i<output_row; i++) 
    {
        int p = (int)(100.0*(double)i/(double)output_row);
        orthoProgressBar->setProgress(p);

        for (j=0; j<output_col; j++) columno[j] = 0;
      
        y = output_y0+i*os->pixelsizeY;
        if (y >= input_y0 && y <= input_y1) 
        {
            fseek(fpi,((int)((y-input_y0)/os->pixelsizeY+0.00001))*col*bytes,SEEK_SET);
            fread(columni,col*bytes,1,fpi);
            for (j=0; j<output_col; j++) 
            {
                x = output_x0+j*os->pixelsizeX;
                if (x >= input_x0 && x <= input_x1) 
                {
                    for (k=0; k<bytes; k++) 
                    {
                        columno[j*bytes+k] = columni[((int)((x-input_x0)/os->pixelsizeX+0.00001))*bytes+k];
                    }
                }
            }
        }
      
        fwrite(columno,output_col*bytes,1,fpo);
    }

    fclose(fpo); fclose(fpi);
    free(columno); free(columni);

    remove(input);

    return;
}

void OrthoDialog::WriteTfwFile( char * filename, OrthoImgStruct os )
{
    FILE *fp;


    fp=fopen(filename,"w");
    fprintf(fp,"%lf\n",os.pixelsizeX);
    double tt=0.0;
    fprintf(fp,"%lf\n",tt);
    fprintf(fp,"%lf\n",tt);
    tt=-os.pixelsizeX;
    fprintf(fp,"%lf\n",tt);
    fprintf(fp,"%lf\n",os.BL_x);
    fprintf(fp,"%lf\n",os.TR_y);

    fclose(fp);
    return;
}

void OrthoDialog::Raw2GeoTIFF( char * RAWfile, int row, int col, char * TIFfile, OrthoImgStruct os )
{

}

void OrthoDialog::Raw2TIFF( char * RAWfile, int row, int col, int bytes, char * TIFfile )
{
    FILE            *fp, *tp;
    int             i,dpi;
    long            pos;
    unsigned char   *fpic;

    int    head=0;
       
    dpi = 300;

    fp = fopen(RAWfile,"rb");
    if (!fp) 
    {
        return;
    }
     
    tp = fopen(TIFfile,"wb");
    if (!tp) 
    {
        return;
    }
       
    fPutWord(tp, 'II');
    fPutWord(tp, 42);
    fPutLong(tp, 0L);

    if (bytes == 1) 
    {
        for (i=0; i<256; ++i) fPutWord(tp, i<<8);
        for (i=0; i<256; ++i) fPutWord(tp, i<<8);
        for (i=0; i<256; ++i) fPutWord(tp, i<<8);
    }
    else {
        fPutWord(tp,8);
        fPutWord(tp,8);
        fPutWord(tp,8);
    }

    /* fill the bitmap image data */
    fpic = (unsigned char *)calloc(col*3, sizeof(unsigned char));
    if (!fpic) 
    {
        return;
    }

    orthoProgressBar->reset();
    orthoProgressBar->setTotalSteps(100);
    orthoProgressBar->setProgress(0);
    //orthoProgressBar->setCaption("Save to TIFF file ...");
    //orthoProgressBar->setCenterIndicator(1);
    orthoProgressBar->show();


    for(i=0; i<row; i++) 
    {
        int p = (int)(100.0*(double)i/(double)(row-1));
        orthoProgressBar->setProgress(p);

        fseek(fp,head+(row-1-i)*col*bytes,0);
        fread (fpic, col*bytes, 1, fp);
        fwrite(fpic, col*bytes, 1, tp);
    }
     
    pos = ftell(tp);

    fPutWord(tp,11);
    WriteTIFFTag(tp, SubfileType,         TIFFshort,    1L,   1L);
    WriteTIFFTag(tp, ImageWidth,          TIFFlong ,    1L,   (long)col);
    WriteTIFFTag(tp, ImageLength,         TIFFlong ,    1L,   (long)row);
    WriteTIFFTag(tp, BitsPerSample,       TIFFshort,    1L,   8L);
    WriteTIFFTag(tp, SamplesPerPixel,     TIFFshort,    1L,   (long)bytes);
    WriteTIFFTag(tp, Compression,         TIFFshort,    1L,   1L);
    if (bytes == 1) 
    {
        WriteTIFFTag(tp, PhotometricInterp,   TIFFshort,    1L,   1L);
        WriteTIFFTag(tp, StripOffsets,        TIFFlong,     1L,   1544L);
    }
    else 
    {
        WriteTIFFTag(tp, PhotometricInterp,   TIFFshort,    1L,   2L);
        WriteTIFFTag(tp, StripOffsets,        TIFFlong,     1L,   14L);
    }
    WriteTIFFTag(tp, RowsPerStrip,        TIFFlong,     1L,   (long)row);
    WriteTIFFTag(tp, StripByteCounts,     TIFFlong,     1L,   (long)row*col*bytes);
    WriteTIFFTag(tp, SampleFormat,       TIFFshort,    1L,   1L);
    fPutLong(tp, 0L);  /* the Terminal Flag of the Tag Diectory */
    fPutLong(tp, 0L);

    fseek(tp, 4L, 0);
    fPutLong(tp, pos);
      
    free(fpic);
     
    fclose(fp); fclose(tp);
    remove(RAWfile);
    return ;
}

int OrthoDialog::CreateOrthoImageTMP( CRPC rpc, OrthoImgStruct * os, int * tr, int * tc )
{
    int    i,j,k,kkkk,*blkrow,*blkcol,orthoRow,orthoCol,gridRow,gridCol,BCC,BRR;
    int    row,col,bytes,bx,by,m,n,ix,iy; //k1,k2;
    FILE   *fp;
    char   filename[256];
    double   gridPixR,gridPixC,Lon,Lat;
    double   cornerX,cornerY,cornerZ,minpx,minpy,maxpx,maxpy;
    double   px,py,pxy[4][2];
    double   dx,dy,x,y,pixel1,pixel2,pixel3,pixel4; //pixel16[16];
    double   *pixelX,*pixelY;
    double   IP;

    CHugeImage image;

    unsigned char *gridData,*stripData,*imgPatch;

    bytes=os->colour;
     
    //pixelX=(double *)calloc(ds.col*ds.row, sizeof(double));
    //pixelY=(double *)calloc(ds.col*ds.row, sizeof(double));

    pixelX=(double *)calloc(dem.nx*dem.ny, sizeof(double));
    pixelY=(double *)calloc(dem.nx*dem.ny, sizeof(double));

    os->original_x=dem.xor;
    os->original_y=dem.yor;
     
    orthoRow=(int)(((dem.ny-1)*dem.rasu)/os->pixelsizeY+1);
    orthoCol=(int)(((dem.nx-1)*dem.rasu)/os->pixelsizeX+1);
    *tr=orthoRow; *tc=orthoCol;
     
    gridPixR=dem.rasu/os->pixelsizeY;
    gridPixC=dem.rasu/os->pixelsizeX;
     
    blkrow=(int *)calloc(dem.ny, sizeof(int));
    blkcol=(int *)calloc(dem.nx, sizeof(int));

    for(i=0; i<dem.ny-1; i++) blkrow[i]=(int)((double)(i+1.0)*gridPixR)-(int)((double)i*gridPixR);
    for(i=0; i<dem.nx-1; i++) blkcol[i]=(int)((double)(i+1.0)*gridPixC)-(int)((double)i*gridPixC);

    CSpheroid* spheroid = new CSpheroid(6378137, 298.257222101);
    CXYZPoint inPoint, outPoint;
    CXYPoint  ixy;
    CUTMToGeographic *transU2G = new CUTMToGeographic(spheroid, m_UTMZone, SOUTHERN);

    for(i=0; i<dem.ny; i++) 
    {
        for(j=0; j<dem.nx; j++) 
        {
            cornerX = dem.xor+j*dem.rasu;
            cornerY = dem.yor+i*dem.rasu;
            cornerZ = *(dem.z_data+i*dem.nx+j);
                
            if(cornerZ >= 10000.0 || cornerZ <= -500.0) 
            {
                pixelX[i*dem.nx+j]=-99999.0;
                pixelY[i*dem.nx+j]=-99999.0;
            }else 
            {
                inPoint.setX(cornerX);
                inPoint.setY(cornerY);
                inPoint.setZ(cornerZ);

                outPoint.setX(cornerX);
                outPoint.setY(cornerY);
                outPoint.setZ(cornerZ);

                transU2G->transform(&inPoint, &outPoint);
                rpc.transform(&ixy, &outPoint);

                pixelX[i*dem.nx+j]=ixy.getX();
                pixelY[i*dem.nx+j]=ixy.getY();
            }
        }
    }

    delete spheroid;
    delete transU2G;

    sprintf(filename,"%s.tmp",os->outputName);
    fp=fopen(filename,"wb");
     
    image.open(PanFileName);
    os->o_rows=image.getHeight(); os->o_cols=image.getWidth();

    for(i=0; i<dem.ny-1; i++) 
    {
        gridRow=blkrow[i];
        stripData=(unsigned char *)calloc(gridRow*orthoCol*3, sizeof(char));
        gridData =(unsigned char *)calloc(gridRow*blkcol[0]*3*os->colour, sizeof(char));
          
        for(j=0; j<gridRow*orthoCol*os->colour; j++) stripData[j]=0;
          
        for(j=0; j<dem.nx-1; j++) 
        {
            gridCol=blkcol[j];
               
            BRR=BCC=0;
            for(k=1; k<=j; k++) BCC += blkcol[k-1];

            pxy[0][0] = pixelX[i*dem.nx+j];
            pxy[0][1] = pixelY[i*dem.nx+j];
            pxy[1][0] = pixelX[i*dem.nx+j+1];
            pxy[1][1] = pixelY[i*dem.nx+j+1];
            pxy[2][0] = pixelX[(i+1)*dem.nx+j+1];
            pxy[2][1] = pixelY[(i+1)*dem.nx+j+1];
            pxy[3][0] = pixelX[(i+1)*dem.nx+j];
            pxy[3][1] = pixelY[(i+1)*dem.nx+j];
       
            if (pxy[0][0] == -99999.0 || pxy[0][1] == -99999.0 || 
                pxy[1][0] == -99999.0 || pxy[1][1] == -99999.0 || 
                pxy[2][0] == -99999.0 || pxy[2][1] == -99999.0 || 
                pxy[3][0] == -99999.0 || pxy[3][1] == -99999.0) 
            {
                for (m=0; m<gridRow; m++) 
                {
                    for (n=0; n<gridCol; n++) 
                    {
                        for(kkkk=0; kkkk<bytes; kkkk++) *(stripData+(m*orthoCol+BCC+n)*bytes+kkkk) = 0;
                    }
                }
            }else 
            {
                minpx=minpy=999999.0;
                maxpx=maxpy=-999999.0;
                for(k=0; k<4; k++) 
                {
                    if(minpx>pxy[k][0]) minpx=pxy[k][0];
                    if(minpy>pxy[k][1]) minpy=pxy[k][1];
                    if(maxpx<pxy[k][0]) maxpx=pxy[k][0];
                    if(maxpy<pxy[k][1]) maxpy=pxy[k][1];
                }
               
                if(minpx-4<0) minpx=4;
                if(minpy-4<0) minpy=4;
                bx=(int)(minpx-3.0);
                by=(int)(minpy-3.0);
                if(bx<0) bx=0;
                if(by<0) by=0;
                if(maxpy+4>os->o_rows-1) maxpy=os->o_rows-5;
                if(maxpx+4>os->o_cols-1) maxpx=os->o_cols-5;
                if(minpy+4>os->o_rows-1) minpy=os->o_rows-5;
                if(minpx+4>os->o_cols-1) minpx=os->o_cols-5;
                row=(int)(maxpy-minpy+11.0);
                col=(int)(maxpx-minpx+11.0);
               
                if(row > 0 && col > 0) 
                {
                    imgPatch=(unsigned char *)calloc(row*col*bytes, sizeof(char));
                    image.getRect(imgPatch, by, row, bx, col);

                    for(m=0; m<gridRow; m++) 
                    {
                        for(n=0; n<gridCol; n++) 
                        {
                            dx=(double)n/(double)(gridCol);
                            dy=(double)m/(double)(gridRow);
           
                            x = pxy[0][0]*(1.0-dx)*(1.0-dy)+pxy[1][0]*dx*(1.0-dy)+
                                pxy[2][0]*dx*dy+pxy[3][0]*dy*(1.0-dx)-bx;
                            y = pxy[0][1]*(1.0-dx)*(1.0-dy)+pxy[1][1]*dx*(1.0-dy)+
                                pxy[2][1]*dx*dy+pxy[3][1]*dy*(1.0-dx)-by;
           
                            ix=(int)x;
                            iy=(int)y;
                           
                            dx=x-ix;
                            dy=y-iy;
         
                            if(ix >= 0 && iy >= 0 && ix < col-1 && iy < row-1) 
                            {
                                for(kkkk=0; kkkk<bytes; kkkk++) 
                                {
                                    if(os->interMethod == 1) 
                                    {  
                                        pixel1 = (double)imgPatch[(  iy  *col+ ix )*bytes+kkkk];
                                        pixel2 = (double)imgPatch[(  iy  *col+ix+1)*bytes+kkkk];
                                        pixel3 = (double)imgPatch[((iy+1)*col+ix+1)*bytes+kkkk];
                                        pixel4 = (double)imgPatch[((iy+1)*col+ ix )*bytes+kkkk];
                                          
                                        IP = pixel1*(1.0-dx)*(1.0-dy)+
                                            pixel2*dx*(1.0-dy)+
                                            pixel3*dx*dy+
                                            pixel4*(1.0-dx)*dy;
                                        if (IP > 255.0) IP = 255.0;
                                        if (IP < 0.0) IP = 0.0;
                                        *(gridData+(m*gridCol+n)*bytes+kkkk) = (unsigned char)(IP+0.5);
                                    }
                                }
                            }
                        }
                    }
        
                    for(m=0; m<gridRow; m++) 
                    {
                        for(n=0; n<gridCol; n++) 
                        {
                            for(kkkk=0; kkkk<bytes; kkkk++) 
                            {
                                *(stripData+(m*orthoCol+BCC+n)*bytes+kkkk) = *(gridData+(m*gridCol+n)*bytes+kkkk);
                            }
                        }
                    }
          
                    free(imgPatch);
                }else 
                {
                    for(m=0; m<gridRow; m++) 
                    {
                        for(n=0; n<gridCol; n++) 
                        {
                            for(kkkk=0; kkkk<bytes; kkkk++) 
                            {
                                *(stripData+(m*orthoCol+BCC+n)*bytes+kkkk)=0;
                            }
                        }
                    }
                }
            }
        }

        fwrite(stripData,gridRow*orthoCol*os->colour,1,fp);
      
        free(stripData); free(gridData);
    }


    //free(dem.z_data); //let Qt do 
    free(pixelX); free(pixelY);
    free(blkrow); free(blkcol); fclose(fp);


    return(0);
}
