#ifndef __CZoomOutHandler__
#define __CZoomOutHandler__
#include "BaristaViewEventHandler.h"

class CZoomOutHandler : public CBaristaViewEventHandler
{
public:
	CZoomOutHandler(void);

	~CZoomOutHandler(void);

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyPressEvent (QKeyEvent *e, CBaristaView *v)
	{
		return CBaristaViewEventHandler::keyPressEvent(e, v);
	};

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
	{
		return CBaristaViewEventHandler::keyReleaseEvent(e, v);
	};

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) { return false; };
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool redraw(CBaristaView *v);
	virtual void initialize(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setZoomOutCursor();};

	virtual void open();

	virtual void close();
};




#endif