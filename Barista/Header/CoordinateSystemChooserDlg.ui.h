/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

bool CoordinateSystemChooserDlg::localxyzButtonChecked()
{
    return this->localxyzButton->isChecked();
}

bool CoordinateSystemChooserDlg::utmButtonChecked()
{
    return this->UTMButton->isChecked();
}

bool CoordinateSystemChooserDlg::geographicButtonChecked()
{
    return this->geographicButton->isChecked();
}

bool CoordinateSystemChooserDlg::geocentricButtonChecked()
{
    return this->geocentricButton->isChecked();
}

void CoordinateSystemChooserDlg::Ok_clicked()
{
    this->close();
}


bool CoordinateSystemChooserDlg::tmButtonChecked()
{
    return this->TMButton->isChecked();

}
