#ifndef __CTFWTable__
#define __CTFWTable__

#include "BaristaTable.h"


class CTFW;

class CTFWTable : public CBaristaTable
{
    Q_OBJECT // needed to connect signals / slots
public:
	CTFWTable(CTFW* tfw,QWidget* parent = 0);
	~CTFWTable(void);

	virtual void initTable();
	virtual void elementAdded(CLabel element){};
	virtual void elementDeleted(CLabel element){};

     
    void closeEvent ( QCloseEvent * e );
	CTFW *getTFW() const { return tfw; };

protected:

    CTFW* tfw;
	 
	int offsetdezimals;
	int scaledezimals;
	int rotationdezimals;

};

#endif