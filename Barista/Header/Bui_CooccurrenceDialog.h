#ifndef BUI_COOCCURRENCEDIALOG_H
#define BUI_COOCCURRENCEDIALOG_H

#include <qdialog.h>
#include "ui_Cooccurrence.h"
#include <QTimer>
#include "CharString.h"

class CVImage;
class bui_ImageSensorSelector;

class bui_CooccurrenceDialog :	public QDialog, public Ui_CooccurrenceMatrixDialog
{Q_OBJECT

  protected:

	  CVImage *pAutomaticClassification;

	  CVImage *pReferenceClassification;

	  CCharString currentDir;

	  CCharString outputDir;

	  bool computationOK;

  public:

	  bui_CooccurrenceDialog(CVImage *pReference);

	  ~bui_CooccurrenceDialog();


	  bool getComputationOK() const { return computationOK; }

	  void setCurrentDir(const CCharString &cdir);

	  void checkSetup();

	  bool detectChanges();

  public slots:
	
	  void AutomaticChanged();

	  void ReferenceChanged();

	  void OutputLevelChanged();

	  void explorePathSelected();

	  void OK();

	  void Cancel();
	  
	  void Help();

  protected:

	  void setupImageSelector(QComboBox *box, CVImage *selectedImage);

  private: 

	  void init();

};

#endif // BUI_COOCCURRENCEDIALOG_H