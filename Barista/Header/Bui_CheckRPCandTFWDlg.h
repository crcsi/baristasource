#ifndef BUI_CheckRPCandTFWDlg_H
#define BUI_CheckRPCandTFWDlg_H
#include <qdialog.h>
#include "ui_CheckRPCandTFWDlg.h"

class bui_CheckRPCandTFWDlg : 	public QDialog, public Ui_CheckRPCandTFWDlg
{Q_OBJECT
public:
	bui_CheckRPCandTFWDlg(void);
public:
	~bui_CheckRPCandTFWDlg(void);
public slots:
	void Ok();
	void Cancel();

};

#endif // BUI_CheckRPCandTFWDlg_H
