#ifdef WIN32

#ifndef BUI_BuildingReconstructionDialog_H
#define BUI_BuildingReconstructionDialog_H

#include <qdialog.h>
#include "ui_BuildingReconstruction.h"
#include "m_FeatExtract.h"

class CVImage;
class CVALS;

#ifdef WIN32
class CBuildingReconstructor;
#endif

class CBuilding;
class bui_OutputModeHandler;


class bui_BuildingReconstructionDialog : public QDialog , public Ui_BuildingReconstructionDialog
{Q_OBJECT

  protected:

	  bui_OutputModeHandler *pOutPutHandler;

	  vector<CVImage *> AvailableImageList;
	  vector<CVImage *> SelectedImageList;
	  vector<CBuilding *> BuildingList;

	  CVALS *ALSdata;

	  double ImageRes;

	  double ALSRes;

	  double SearchSize;

	  double sigmaZ;
	  double alpha;
	  int neighbours;
	  double obliqueness;
	  double minBuildingHeight;

	  CCharString outputPath;

	  FeatureExtractionParameters featureParameters;

	  CBuildingReconstructor *reconstructor;

	  bool success;

  public:

	  bui_BuildingReconstructionDialog(const vector<CBuilding *> &buildingList);

	  ~bui_BuildingReconstructionDialog();

	  bool getSuccess() const { return this->success; }

  public slots:

	  void onImageListChanged();

	  void onALSChanged();

	  void onImageResChanged();

	  void onALSResChanged();

	  void onBufferSizeChanged();

	  void onNeighboursChanged();

	  void onObliquenessChanged();

	  void onAlphaChanged();

	  void onSigmaALSChanged();

	  void onBuildingHeightChanged();

	  void OutputLevelChanged();

	  void onFeatureExtractionParsSelected();
	
	  void onBrowseSelected();

	  void OK();

	  void Cancel();

	  void Help();

  protected: 

	  void setupALS();

	  void setupImageSelector();

	  void checkSetup();

	  void init();

	  void saveToFile(const CCharString &filename);

	  void initFromFile(const CCharString &filename);

	  void destroy();

};

#endif // BUI_BuildingReconstructionDialog_H

#endif // WIN32
