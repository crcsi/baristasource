#ifndef __CImageView__
#define __CImageView__

#include "BaristaView.h"
#include "VImage.h"

class QPainter;
class QTimer;
class CXYPolyLine;
class bui_FeatureExtractionDialog;

class CImageView : public CBaristaView
{
Q_OBJECT


public:
	CImageView(CVImage* img, QWidget * parent = 0);
    ~CImageView(void);
    
    virtual CVImage* getVImage() const { return this->image; };

	virtual CHugeImage *getHugeImage() const;

    virtual void mouseMoveEvent ( QMouseEvent * e );

    virtual void keyReleaseEvent ( QKeyEvent * e );

	virtual void createPopUpMenu();
	
    virtual void closeEvent ( QCloseEvent * e );

    virtual bool isA(const char* className) const
    {
        if (::strcmp("CImageView", className) == 0)
            return true;
        else
            return false;
    };

	virtual void drawContents ( QPainter &p);

	virtual void elementsChanged(CLabel element="");

	virtual const CSensorModel* getCurrentSensorModel();

	virtual void getPixelInfoHugeImage(QString& pixelInfo,
										 QString& geoInfo,
										 QString& colorInfo,
										 QString& zoomInfo,
										 QString& residualInfo,
										 QString& brightnessInfo,
										 CHugeImage* himage);

	virtual const CTFW* getTFW() const;

	virtual bool getTFWMonoplotting()const 
	{
		return this->TFWMonoplotting;
		/*
		if (this->image->getModel() == this->getTFW())
			return this->TFWMonoplotting;
		else
			return true;
			*/
	};

	virtual CVBase* getVBase() const { return this->image;};	

	void drawScaleBar(QPainter &p, bool mode, double gsd);
	
protected:
	void regionGrowing(int xSeed, int ySeed);
	void drawZoomer(QPainter &p, CObsPoint point);
	void changeResidualFactor(double factor);
    
    CVImage* image;

	

public slots:

	void OpenFeaturesDialog();
//	void enhanceAreaROI();
	void deletePointsROI();
	void fitEllipsePointsROI();
	
	//void openSnakeDialog();
	
	void OrthofromROI();
	virtual void CropROI();
};

#endif

