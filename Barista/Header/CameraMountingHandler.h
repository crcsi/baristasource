#ifndef __CCameraMountingHandler__
#define __CCameraMountingHandler__
 
#include "BaristaViewEventHandler.h"
#include <QWidget>

class Bui_PushbroomImportDlg;
class CVImage;

class CCameraMountingHandler : public CBaristaViewEventHandler
{
  protected:

	Bui_PushbroomImportDlg* importDlg;
	CVImage* image;

	C2DPoint floorPoint;
	C2DPoint roofPoint;

	void cleanUp();

	void computeViewingAngle();
	bool evalF(double i,double c,double a,double b,double& value,double& dfdi,double& dfdc) const;
	bool evalG(double i,double c,double& value,double& dgdi,double& dgdc) const;
  public:

	  CCameraMountingHandler();

	  virtual ~CCameraMountingHandler();

	  virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	  virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v){return true;};

	  virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);
	  virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v){return true;};

  	  virtual bool redraw(CBaristaView *v){return true;};
	  virtual void initialize(CBaristaView *v){return;};

	  virtual void setCurrentCursor(CBaristaView *v){ v->setCrossCursor();};

	  virtual void open();

	  virtual void close();

	  bool setVImage(CVImage* vimage);

	

	  virtual void elementDeleted(CLabel element="");

};


#endif