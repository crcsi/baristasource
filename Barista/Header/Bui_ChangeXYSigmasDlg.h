#ifndef BUI_CHANGEXYSIGMASDLG_H
#define BUI_CHANGEXYSIGMASDLG_H
#include <qdialog.h>
#include "ui_ChangeXYSigmasDlg.h"

class bui_ChangeXYSigmasDlg :	public QDialog , public Ui_ChangeXYSigmasDlg
{
Q_OBJECT

public:
	bui_ChangeXYSigmasDlg(void);
	double sx;
	double sy;
	bool makechanges;
public:
	~bui_ChangeXYSigmasDlg(void);

	public slots:
	void setSX( double sx );
    void setSY( double sy );

    double getSX();
    double getSY();

    void Cancel_released();
    void OKButton_released();
    bool getmakeChanges();
};

#endif //CHANGEXYSIGMASDLG