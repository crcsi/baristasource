#ifndef BUI_FEATUREEXTRACTIONDIALOG_H
#define BUI_FEATUREEXTRACTIONDIALOG_H
#include <qdialog.h>
#include "Filename.h"
#include "ui_FeatureExtractionDialog.h"
#include "m_FeatExtract.h"

class CVImage;
class CLabelImage;
class CXYZPolyLine;

class bui_FeatureExtractionDialog :	public QDialog, public Ui_FeatureExtractionDialog
{Q_OBJECT

  protected:

	CVImage *image;

	CLabelImage *labelimage;

	CFileName labelFileName;

	FeatureExtractionParameters parameters;

	bool enablePoints;

	bool enableLines;

	bool enableRegions;

	bool parsAccepted;

	CFileName iniFileName;

	CXYPolyLine *boundaryPoly;

	CXYZPolyLine *boundaryPolyXYZ;

  public:

	bui_FeatureExtractionDialog(QWidget* parent, CVImage *I_image, CFileName initialise);

	bui_FeatureExtractionDialog(QWidget* parent, CXYZPolyLine *poly, CFileName initialise);

	bui_FeatureExtractionDialog(QWidget* parent, FeatureExtractionParameters &pars, bool points, bool lines, bool regions, CFileName initialise);

	~bui_FeatureExtractionDialog();

	FeatureExtractionParameters getParameters() const { return parameters; };

	bool parametersAccepted() const { return parsAccepted; };

	CFileName getLabelFileName() const { return this->labelFileName; };

  public slots:

	void Cancel();
	void OK();

	void Help();

	void checkValuesGeneral();

	void checkValuesRegion();

	void checkValuesFeatureTypes();

	void checkValuesDerivative();

	void checkValuesMode();

	void checkValuesFoerstner();

	void checkValuesFoerstnerKernel();

	void labelBrowsePushed();

	void ImageChanged();

	void kWatershedChanged();

	void setBoundaryPoly(CXYZPolyLine *poly);

  protected: 

	void init();

	void setup();

	void setParameters();

	void enableFoerstnerPars();

	void disableFoerstnerPars();

	int getMaxNonMax();

	void initBoundaryPoly();

	void initImages();
};

#endif // BUI_FEATUREEXTRACTIONDIALOG_H
