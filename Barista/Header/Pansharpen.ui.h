/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

#include <qfiledialog.h> 
#include "orthodialog.h"
#include <qmessagebox.h> 
#include "HugeImage.h"
#include "stdlib.h"
#include "stdio.h"
#include "BaristaProject.h"

void PanDialog::openPanImage()
{
     QString filename = QFileDialog::getOpenFileName(
            "/home",
            "Images (*.hug)",
            this,
            "open file dialog",
            "Select a image file" );
    
    QFile test(filename);
    if (!test.exists()) return;
    test.close();

    strcpy(PanFileName, filename);
    Panimagename->setText(filename);    
}


void PanDialog::openRGBImage()
{
    QString filename = QFileDialog::getOpenFileName(
            "/home",
            "Images (*.hug)",
            this,
            "open file dialog",
            "Select a image file" );
    
    QFile test(filename);
    if (!test.exists()) return;
    test.close();

    strcpy(RGBFileName, filename);
    RGBimagename->setText(filename);    
}


void PanDialog::openOutputImage()
{
   QString filename = QFileDialog::getSaveFileName(
                    "/home",
                    "Images (*.hug)",
                    this,
                    "save file dialog",
                    "Select a filename to save" );

    strcpy(OutFileName, filename);
    outputname->setText(filename);    
}


void PanDialog::ComputePansharpenImage()
{
    CVImage rgb;
    CVImage pan;
    
    //rgb.open("C:\\Documents and Settings\\hanley\\My Documents\\images\\pan sharpening demo\\PO_107367_RGB_0000000_LARGE.TIF.hug");
    //pan.open("C:\\Documents and Settings\\hanley\\My Documents\\images\\pan sharpening demo\\PO_107367_PAN_0000000.TIF.hug");
    //rgb.open("E:\\Bhutan\\000000184263_01_P001_MUL\\P001RGB.tif.hug");
    //pan.open("E:\\Bhutan\000000184263_01_P001_PAN\\04DEC19044749-P1BS-000000184263_01_P001.TIF.hug");

    rgb.getHugeImage()->open(RGBFileName);
    pan.getHugeImage()->open(PanFileName);

    CHugeImage tmp;
    remove("c:\\tmp.hug");

    tmp.open("c:\\tmp.hug", pan.getHugeImage()->getWidth(), pan.getHugeImage()->getHeight(), 32, 3);
    tmp.panSharpen(rgb.getHugeImage(), pan.getHugeImage(), 7);

    CHugeImage result;
    result.open(OutFileName, pan.getHugeImage()->getWidth(), pan.getHugeImage()->getHeight(), 8, 3);
    result.floatToRGB(&tmp);
}
