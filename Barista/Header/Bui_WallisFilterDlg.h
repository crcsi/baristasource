#ifndef BUI_WALLISFILTERDLG_H
#define BUI_WALLISFILTERDLG_H

#include <qdialog.h>
#include "ui_WallisFilterDlg.h"


class CBaristaView;
class CTransferFunction;
class CHugeImage;
class CWallisFilter;

class Bui_WallisFilterDlg :	public QDialog , public Ui_WallisFilterDlg 
{
Q_OBJECT

protected:

public:
	Bui_WallisFilterDlg(CBaristaView* view,bool showButtons=true,QWidget* parent = NULL);
	~Bui_WallisFilterDlg();

	bool getSuccess() const { return this->success;};
	void okPressed();

public slots:
	void pBOKReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBUpdateReleased();

	void sBMeanValueChanged(int value);
	void sBStdDevValueChanged(int value);
	void sBContrastValueChanged(int value);
	void sBBrightnessValueChanged(int value);

	void sBWindowSizeXValueChanged(int value);
	void sBWindowSizeYValueChanged(int value);

	void cBPreviewReleased();
private:
	
	void checkSetup();
	void updateWallisFilter();
	
	double contrast;
	double brightness;
	double mean;
	double stdDev;

	int windowSizeX;
	int windowSizeY;
	
	int nParametersX;
	int nParametersY;

	CTransferFunction* backUpFunction;
	CWallisFilter* wallisFilter;
	CHugeImage* himage;

	CBaristaView* view;

	bool meanAndStdDevInitialized;
	bool success;
	bool instantPreView;

	int offsetX;
	int offsetY;
	int width;
	int height;
};





#endif //BUI_WALLISFILTERDLG_H
