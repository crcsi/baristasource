#ifndef EDITORDELEGATE_H
#define EDITORDELEGATE_H

#include <QItemDelegate>

class QObject;


class CEditorDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	enum ValidatorType{eNoValidator,eIntValidator,eDoubleValidator};
	
	CEditorDelegate(QObject* parent,CEditorDelegate::ValidatorType type,double minValue, double maxValue);
	CEditorDelegate(QObject* parent,CEditorDelegate::ValidatorType type);


	void paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const;
	QWidget *createEditor(QWidget *parent,const QStyleOptionViewItem & option, const QModelIndex &index) const;
	void setEditorData(QWidget * editor, const QModelIndex & index )const;

	void setModelData ( QWidget * editor, QAbstractItemModel * model, const QModelIndex & index ) const;

protected:
	ValidatorType type;
	bool setMinMax;
	double min;
	double max;

};

#endif // EDITORDELEGATE_H
