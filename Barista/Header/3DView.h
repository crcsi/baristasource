#ifndef __C3DView__
#define __C3DView__

#include "BaristaView.h"

class COpenGLObject;
class QPushButton;

class C3DView : public CBaristaView
{
    Q_OBJECT

public:
	C3DView(QWidget * parent = 0);
	~C3DView();

	virtual void keyReleaseEvent ( QKeyEvent * e );
	//virtual void createPopUpMenu();

	QPushButton *pbContextMenuButton;


protected:
	virtual void paintEvent(QPaintEvent* e);

public slots:
	void loadXYZPoints();
	void loadDEMs();
	void loadALS();

	void setShowGeoPoints();
	void setShowMonoplottedPoints();
	void setShowMonoplottedLines();
	void setShowMonoplottedBuildings();
	void resetView();

	virtual void createPopUpMenu();

protected:
	COpenGLObject* openGL;


};
#endif

