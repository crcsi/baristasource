#ifndef BUI_ALSFILTERDIALOG_H
#define BUI_ALSFILTERDIALOG_H

#include <qdialog.h>
#include "ui_ALSFilter.h"
#include <QTimer>
#include "2DPoint.h"
#include "CharString.h"

class bui_OutputModeHandler;

class CVALS;
class CALSFilter;

class bui_ALSFilterDialog :	public QDialog, public Ui_ALSFilterDialog
{Q_OBJECT

  protected:

	  CVALS *pALS;

	  bui_OutputModeHandler *pOutPutHandler;

	  float MinHeight;
	  float MinimumBuildingHeight;
	  float structureForOpen;
	  double sigmaALS;
	  double alpha;
	  double maxObliqueness;
	  float heightThresholdFilter;
	  int numberIterations;
	  float structureForOpenDetail;
	  int numberPoints;

	  C2DPoint llPoint;
	  C2DPoint urPoint;
	  C2DPoint resolution;

	  CCharString currentDir;
	  CCharString outputDir;
	  CCharString outputFileName;

	  bool extentsInit;

	  bool computationOK;

	  int getStruct(float size);

  public:

	  bui_ALSFilterDialog(CVALS *ALS);

	  ~bui_ALSFilterDialog();


	  bool getComputationOK() const { return computationOK; }

	  void setCurrentDir(const CCharString &cdir);

	  void checkSetup();

	  bool checkLimits();

	  bool filter();

  public slots:
	
	  void ALSSelected();

	  void LLChanged();
	  
	  void RUChanged();

	  void resolutionChanged();

	  void maxExtentsSelected();

	  void minHeightChanged();

	  void structureForOpenChanged();
	  void NumberIterationsChanged();
	  void SigmaALSChanged();
	  void MaxObliquenessChanged();
	  void AlphaChanged();
	  void HeightThresholdFilterChanged();
	  void FilterDetailChanged();
	  void NumberPointsChanged();
	  void MinBuildHeightChanged();

	  void OutputLevelChanged();

	  void explorePathSelected();

	  void OK();

	  void Cancel();
	  
	  void Help();

  protected:


	  CALSFilter *ALSfilter;

	  void setupALSSelector(CVALS *ALS);

	  void setCurrentExtentsAndResolution();
	  
	  void writeParameters(const CCharString &filename) const;

	  void initParameters(const CCharString &filename);

  private: 

	  void init();

};

#endif // BUI_ALSFILTERDIALOG_H