#ifndef BUI_ORBITSETTINGS_H
#define BUI_ORBITSETTINGS_H

#include <QDialog>
#include "ui_OrbitSettings.h"
#include "OrbitPathModel.h"
#include "OrbitAttitudeModel.h"
#include "XYZPoint.h"
#include "AdjustmentDlg.h"

#include "OrbitObservations.h"

class Bui_OrbitSettings : public CAdjustmentDlg, public Ui_Orbit_Settings
{
	Q_OBJECT

public:
	Bui_OrbitSettings(CSensorModel* sensorModel,SplineModelType splineType,QWidget *parent = 0);
	~Bui_OrbitSettings();

	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString();
	virtual void cleanUp();

signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

protected:
	void initPath();
	void initAttitudes();

	void copyDataInLocalPathVariables();
	void copyDataInLocalAttitudeVariables();


	void applySettings();
	void applySettingsPath();
	void applySettingsAttitudes();

	void checkAvailabilityOfSettingsPath();
	void checkAvailabilityOfSettingsAttitudes();

	void setDoubleText(QLineEdit* le,double value, int decimals);
	QString setDoubleText(double value, int decimals);

	void setParameter(const CSensorModel* sensorModel,bool parameter);
	void setAllParameter(bool parameter);

	void transformToOrbitalSystem(bool inverse = false);
	void transform(CXYZPoint& parameter,bool inverse = false);

public slots:

	// path
	void cBPathShiftStateChanged(int state);
	void cBPathRotationStateChanged(int state);

	void lEPathXShiftAccuracyTextChanged(const QString & text);
	void lEPathYShiftAccuracyTextChanged(const QString & text);
	void lEPathZShiftAccuracyTextChanged(const QString & text);

	void lEPathRollAccuracyTextChanged(const QString & text);
	void lEPathPitchAccuracyTextChanged(const QString & text);
	void lEPathYawAccuracyTextChanged(const QString & text);

	void pBPathShiftResetReleased();
	void pBPathShiftAllReleased();
	void pBPathRotationResetReleased();
	void pBPathRotationAllReleased();

	// attitudes
	void cBAttitudeShiftStateChanged(int state);
	void cBAttitudeRotationStateChanged(int state);

	void lEAttitudeXShiftAccuracyTextChanged(const QString & text);
	void lEAttitudeYShiftAccuracyTextChanged(const QString & text);
	void lEAttitudeZShiftAccuracyTextChanged(const QString & text);

	void lEAttitudeTimeRollAccuracyTextChanged(const QString & text);
	void lEAttitudeTimePitchAccuracyTextChanged(const QString & text);
	void lEAttitudeTimeYawAccuracyTextChanged(const QString & text);

	void pBAttitudeShiftResetReleased();
	void pBAttitudeShiftAllReleased();
	void pBAttitudeTimeRotationResetReleased();
	void pBAttitudeTimeRotationAllReleased();

	void rBGlobalReleased();
	void rBOrbitReleased();
	void pBAllReleased();

	bool getUseOrbitObservationHandler() const { return this->useOrbitObservations;};

private slots:
	virtual void elementsChangedSlot(CLabel element);

private:
	// general
	SplineModelType splineType;

	COrbitPathModel path;
	COrbitAttitudeModel attitudes;

	COrbitPathModel *existingPath;
	COrbitAttitudeModel *existingAttitudes;

	// path
	CXYZPoint pathShiftParameter;
	CXYZPoint pathRotationParameter;
	CXYZPoint pathShiftObservation;
	CXYZPoint pathRotationObservation;
	bool bPathShiftSelected;
	bool bPathRotationSelected;

	// attitudes
	CXYZPoint attitudeRotationParameter;
	CXYZPoint attitudeTimeRotationParameter;
	CXYZPoint attitudeRotationObservation;
	CXYZPoint attitudeTimeRotationObservation;
	bool bAttitudeRotationSelected;
	bool bAttitudeTimeRotationSelected;

	Matrix fixedMatrix1;
	CRotationMatrix fixedMatrix2;
	bool showInOrbitalSystem;

	bool useOrbitObservations;
};

#endif // BUI_ORBITSETTINGS_H
