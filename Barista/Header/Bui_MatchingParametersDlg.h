#ifndef BUI_MATCHINGPARAMETERSDLG_H
#define BUI_MATCHINGPARAMETERSDLG_H

#include <QDialog>
#include "ui_MatchingParametersDlg.h"
#include "BaristaHtmlHelp.h"

#include "MatchingParameters.h"

class Bui_MatchingParametersDlg : public QDialog, public Ui_MatchingParametersDlgClass
{
	Q_OBJECT

public:
	Bui_MatchingParametersDlg(QWidget *parent = 0, CMatchingParameters *pars = 0);
	~Bui_MatchingParametersDlg();

	void initDlg();

	void resetToDefault();

	bool getParsChanged() const { return this->parsChanged; };

public slots:
	
	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBDefaultReleased();

	void rholSliderChanged(int rhoint );
	void patchsizeSliderChanged(int size );
	void iterationSliderChanged(int steps );

	void leSigma8BitEditingFinished();
	void leSigma16BitEditingFinished();
	void leSigmaGeomEditingFinished();

	void leMaxZEditingFinished();
	void leMinZEditingFinished();

	// check box connections
	void cBMatchWindowReleased();
	
	void bXYZPointsReleased();
	void leBXYZEditingFinished();

	void rBScaleModeChanged();

protected:
	CMatchingParameters matchingParameter;
	int decimals;


	bool parsChanged;

};

#endif // BUI_MATCHINGPARAMETERSDLG_H
