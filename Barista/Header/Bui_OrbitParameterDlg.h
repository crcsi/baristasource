#ifndef BUI_ORBITPARAMETERDLG_H
#define BUI_ORBITPARAMETERDLG_H

#include <QDialog>
#include "ui_OrbitParameterDlg.h"

#include "SplineModelFactory.h"
#include "AdjustmentDlg.h"

class CSplineModel;

class Bui_OrbitParameterDlg : public CAdjustmentDlg, public Ui_Orbit_ParameterDlg
{
	Q_OBJECT

public:
	Bui_OrbitParameterDlg(CSplineModel* splineModel,SplineModelType splineType,QWidget *parent = 0);
	~Bui_OrbitParameterDlg();

	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString() {return "Orbit parameter dialog";};

	virtual void elementAdded(CLabel element=""){};
	virtual void cleanUp();

signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

public slots:
	void splineModelDlgStatusChanged(bool b);
	void additionalParameterDlgStatusChanged(bool b);
	void tWCurrentChanged( int index );

private slots:
	virtual void elementsChangedSlot(CLabel element);

protected:

	void initSplineModelDlg(CSplineModel* sensorModel,SplineModelType splineType);
	void initAdditionalParameterDlg(CSplineModel* sensorModel,SplineModelType splineType);

	CAdjustmentDlg* additionalParameterDlg;
	CAdjustmentDlg* splineParameterDlg;

	int oldTab;

	CSplineModel* splineModel;
};
#endif // BUI_ORBITPARAMETERDLG_H
