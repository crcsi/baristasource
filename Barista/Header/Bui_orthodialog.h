#ifndef BUI_ORTHODIALOG_H
#define BUI_ORTHODIALOG_H
#include <qdialog.h>
#include "ui_orthodialog.h"
#include <QTimer>
#include "2DPoint.h"
#include "VImage.h"
#include "OrthoPhotoVector.h"

class COrthoImageGenerator;
class CProgressListener;
class CProgressThread;
class CVImage;
class CSensorModel;
class CVDEM;
class QListWidgetItem;


class bui_OrthoDialog :	public QDialog, public Ui_OrthoDialog
{Q_OBJECT

  protected:

	  COrthoPhotoVector imageVector;

	  int selectedImage;

	  bool Orthocreated;

	  const char* pCurrentDir;

  public:

	  bui_OrthoDialog(CVImage *image, C2DPoint *lrc, C2DPoint *ulc, const char *currentDir);

	  ~bui_OrthoDialog();

	  bool getOrthoCreated() const { return this->Orthocreated; };

  public slots:
	
	  void ImageSelected(int row);

	  void ImageClicked(QListWidgetItem *item);

	  void DEMSelected();

	  void InterpolationMethodSelected();

	  void AnchorSwitchChanged();

	  void AnchorWidthChanged();

	  void OrthoLLCChanged();
	  void OrthoRUCChanged();

	  void OrthoGSDChanged();

	  void applyFormatToAllSelected();

	  void setMaxExtentsForAllSelected();

	  void applyGSDToAllSelected();

	  void applyExtentsToAllSelected();

	  void initDEMBorders();

	  void ComputeOrthoimage();

	  void OpenOutImage();

	  void Cancel();
	  
	  void Help();

  protected:

	  void init(CVImage *image, C2DPoint *lrc, C2DPoint *ulc);

	  void initImg(CVImage *image);
	  
	  void initImgList();

	  void setCurrentImage(int index);

	  void initAreafromROI();

	  void setInterpolationGUI();

	  void setAnchorGUI();

	  void checkSetup();



};

#endif // BUI_ORTHODIALOG_H