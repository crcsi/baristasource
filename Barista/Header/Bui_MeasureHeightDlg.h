#ifndef BUI_MEASUREHEIGHTDLG_H
#define BUI_MEASUREHEIGHTDLG_H
#include <qdialog.h>
#include "ui_MeasureHeightDlg.h"

class bui_MeasureHeightDlg :	public QDialog, public Ui_MeasureHeightDlg
{Q_OBJECT
public:
	bui_MeasureHeightDlg(void);
public:
	~bui_MeasureHeightDlg(void);
	public slots:
	    void setHeightValue( double height );
		void Ok_clicked();
};

#endif // MEASUREHEIGHTDLG