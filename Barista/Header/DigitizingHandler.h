#ifndef __CDigitizingHandler__
#define __CDigitizingHandler__
#include "BaristaViewEventHandler.h"

class CDigitizingHandler : public CBaristaViewEventHandler
{
public:
	CDigitizingHandler(void);

	~CDigitizingHandler(void);

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyPressEvent (QKeyEvent *e, CBaristaView *v)
	{
		return CBaristaViewEventHandler::keyPressEvent(e, v);
	};

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) { return false; };
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)  {return true;} ;
	  
	virtual bool redraw(CBaristaView *v);
	virtual void initialize(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setCrossCursor();};

	virtual void open();

	virtual void close();

	virtual void setCurrentImageLabel(const CLabel& l);

	virtual bool allowSetCurrentImageLabel() const { return true;};

protected:

	CXYZPoint* geoXYZ;
	CXYPoint* imgPoint;
};


#endif