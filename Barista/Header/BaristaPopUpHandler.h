#pragma once
#include <QMenu>
#include <QMdiArea>

#include "BaristaProject.h"
#include "IniFile.h"

class ZObject;
class QTreeWidgetItem;
class QMenu;


extern QMdiArea* workspace;

class CBaristaPopUpHandler : public QMenu
{
private:
    Q_OBJECT // needed to connect signals / slots

public:
	CBaristaPopUpHandler(QTreeWidgetItem* item);
	~CBaristaPopUpHandler(void);

	void createImagePopUpMenu();
/////////
	void createXYPointArrayPopUpMenu(xypointtype type);
	void createRPCPopUpMenu();
	void createAffinePopUpMenu();    
	void createTFWPopUpMenu();    
	void createVDEMPopUpMenu();	    
	void createVALSPopUpMenu();
	void createXYZPolyLineArrayPopUpMenu();    
	void createBuildingArrayPopUpMenu();    
	void createXYContourArrayPopUpMenu();	
	void createXYPolyLineArrayPopUpMenu();	
	void createXYZPointArrayPopUpMenu();    
	void createXYEllipseArrayPopUpMenu();
	void createPushBroomScannerPopUpMenu(CPushBroomScanner* scanner);
	void createFrameCameraPopUpMenu(CFrameCameraModel* frameCamera);
	void createOrbitPathPopUpMenu(COrbitObservations* orbitPath);
	void createOrbitAttitudePopUpMenu(COrbitObservations* orbitAttitudes);
//////////

	void nullActions();

	//void generateEdgeOrientation(CHugeImage *Grey);
	//void generateEntropy(CHugeImage *Grey);

public slots:

	// CVImage slots
	void import2DPoints();
	void importRPC();
	void importAffine();
	void importTFW();
	void importMetadata();
	void importPushbroomData();
	void importCamera();
	void ForwardAffineClicked();
	void SingleImageRPCBiasCorrection();
	void createTFW();
	void create2DConformal();
	void OrthoImageGeneration();
	void DisplayImageProperties();
	void refreshImageHistograms();
	void applyLookupTable();
	void exportTiledTIFF();
	void generateNDVI();
	void generateLabelImage();
	void changeDetection();
	void evaluateDetection();
	void cooccurrence();
	void computeInnerOrientation();
	void setPyramidFile();
	void generateEpipolarImages();
	//addition >>
	void writeRPCtoImageFile();
	char *findRPCstring(CRPC *rpc);
	void generateTextureInfo();
	void generateEdgeOrientation();
	void generateEntropy();
	//<< addition

	// CXYPointArray slots
	void makeImagePointsActive();
	void ChangeSigmaXY();
	void DifferencebyLabelXY();
	void DifferencebyIndexXY();
	void monoplotxyTFW();
	void monoplotxyRPC();
	void monoplotxyAffine();
	void monoplotxyPushBroom();
	void monoplotxyCurrentSensorModel();
	void export2DSpacedTextFile();
	void export2DCSVTextFile();
	void georeferencingTFW(); 
	void georeferencing2DTrafo();
	void writeImagePointChips();
	void pointArraySelector();

	// CRPC slots
	void SetRPCCurrentModel();
	void SingleRPCBiasCorrectionNone();
	void SingleRPCBiasCorrectionShift();
	void SingleRPCBiasCorrectionShiftDrift();
	void SingleRPCBiasCorrectionAffine();
	void SingleRPCRegenerateBiasCorrected();
	void exportIkonosRPCs();
	void exportQuickBirdRPCs();
	void exportSocetRPCs();
	void exportCSVRPCs();
	
	// CAffine slots
	void SetAffineCurrentModel();
	void exportAffineTextFile();

	// CPushBroomScanner slots
	void SetPushBroomScannerCurrentModel();
	void ExportPushBroomScannerRPC();
	void mergeOrbits();

	//CFrameCameraModel
	void SetFrameCameraCurrentModel();
	void ExportFrameCameraProjectionMatrix();
	void ExportFrameCamera();
	void ImportFrameCameraOrientation1();
	void ImportFrameCameraOrientation2();
	void FrameCameraSetRefsys();

	//CTFW slots
	void exportTFWTextFile();

	//CVDEM slots
	void interpolateHeightfromDEM();
	void interpolateHeightsfromDEM();
	void DisplayDEMProperties();
	void DisplayDEMStatistcs();
	void DeleteDEMStatistcs();
	void exportASCIIDEM();
	void exportImageDEM();
	void exportASCIIDEMVerticalFlip();
	void detectBuildings();
	void setDEMActive();
	void unsetDEMActive();
	void transformDEM();
	void demOperation();

	//CVALS slots
	void interpolateDEMfromALS();
	void exportALS();
	void mergeALS();
	void interpolateIntensityFromALS();
	void filterALS();
	void BuildingsDetection();

	// CXYZPolyLineArray  slots
	void exportXYZPolyLinesTextFile();
	void exportXYZPolyLinesVRML();
	void exportXYZPolyLinesDXF();
	void TransformXYZPolyLines3D();
	//addition >>
	void exportXYZPolyLinesShapefile();
	//<< addition

	// CBuildingArray  slots
	void exportBuildingsTextFile();
	void exportBuildingsVRML();
	void exportBuildingsDXF();
	
	// CXYContourArray slots
	void thinoutXYContours();

	// CXYPolyLineArray
	bool monoplotxyPolyLines(CVImage *image);
	void monoplotxyPolyLinesRPC();
	void monoplotxyPolyLinesAffine();
	void geoTFWPolyLines();
	void geo2DConformalPolyLines();

	// CXYZPointArray slots
	void ChangeSigmaXYZ();
	void DifferencebyLabelXYZ();
	void DifferencebyIndexXYZ();
	void export3DSpacedTextFile();
	void export3DCSVTextFile();
	void exportDDMMSSsssFile();
	void xyz_to_xyRPC();
	void xyz_to_xyAffine();
	void xyz_to_xyTFW();
	void xyz_to_xyPushbroom();
	void xyz_to_xyFrameCamera();
	void setControl();
	void unsetControl();
	void XYZtoGeographic();
	void XYZtoGeocentric();
	void XYZtoUTM();
	void XYZtoTM();
	void InterpolateDEM();
	void MatchGCPs();
	//addition>>
	void Get3DResidual();
	void export3DShapefile();
	//<<addition
	void CopyToOtherList();

    // CXYEllipseArray slots
	void getEllipseCenters();
	void exportEllipsestoFile();
	void exportEllipseCenterstoFile();

	void computeOrbitPathPoints();
	void importPreciseALOSOrbitData();



	// General slots
	void DeleteObject();
	void RenameObject();
	void OnOff();
	void setReferenceInformation();


	void createDEMLegend(QWidget *Parent, CVDEM* vdem);

protected:
	const ZObject* object;

	QTreeWidgetItem* treewidgetitem;

// all QActions

	// CVImage QActions
	QAction* import2DPointsAction;
	QAction* importRPCAction;
	QAction* importAffineAction;
	QAction* importTFWAction;
	QAction* importMetadataAction;
	QAction* importPushbroomAction;
	QAction* ForwardAffineClickedAction;
	QAction* SingleImageRPCBiasCorrectionAction;
	QAction* createTFWAction;
	QAction* create2DConformalAction;
	QAction* OrthoImageGenerationAction;
	QAction* DisplayImagePropertiesAction;
	QAction* refreshImageHistogramsAction;
	QAction* applyLookupTableAction;
	QAction* exportTiledTIFFAction;
	QAction* generateNDVIAction;
	QAction* generateLabelImageAction;
	QAction* changeDetectionAction;
	QAction* evaluateDetectionAction;
	QAction* cooccurrenceAction;
	QAction* importCameraAction;
	QAction* innerOrientationAction;
	QAction* orbitPathAction;
	QAction* importPreciseALOSData;
	QAction* MergeOrbitsAction;	
	QAction* setPyramidFileAction;
    QAction* BuildingsDetectionAction;
    QAction* generateEpipolarImagesAction;
	//addition >>
	QAction* writeRPCsToFileAction;
	QAction* generateTextureInfoAction;
	QAction* generateEntropyAction;
	QAction* generateEdgeOrientationAction;
	//<< addition

	// CXYPointArray QActions
	QAction* makeImagePointsActiveAction;
	QAction* ChangeSigmaXYAction;
	QAction* DifferencebyLabelXYAction;
	QAction* DifferencebyIndexXYAction;
	QAction* monoplotxyRPCAction;
	QAction* monoplotxyTFWAction;
	QAction* monoplotxyPushBroomAction;
	QAction* monoplotxyCurrentSensorModelAction;
	QAction* monoplotxyAffineAction;
	QAction* export2DSpacedTextFileAction;
	QAction* export2DCSVTextFileAction;
	QAction* georeferencingTFWAction; 
	QAction* georeferencing2DTrafoAction;
	QAction* writeImagePointChipsAction;
	QAction* xyPointArraySelectorAction;



	//CRPC QActions
	QAction* SetRPCCurrentModelAction;
	QAction* SingleRPCBiasCorrectionNoneAction;
	QAction* SingleRPCBiasCorrectionShiftAction;
	QAction* SingleRPCBiasCorrectionShiftDriftAction;
	QAction* SingleRPCBiasCorrectionAffineAction;
	QAction* SingleRPCRegenerateBiasCorrectedAction;
	QAction* exportIkonosRPCsAction;
	QAction* exportQuickBirdRPCsAction;
	QAction* exportSocetRPCsAction;
	QAction* exportCSVRPCsAction;
	

	// CAffine QAction
	QAction* exportAffineTextFileAction;
	QAction* SetAffineCurrentModelAction;

	// CPushBroomScanner QAction
	QAction* SetPushBroomScannerCurrentModelAction;
	QAction* ExportPushBroomScannerRPCAction;

	// CFramecameraModel QAction
	QAction* SetFrameCameraCurrentModelAction;
	QAction* ExportFrameCameraProjectionMatrixAction;
	QAction* ExportFrameCameraAction;
	QAction* ImportFrameCameraOrientationAction1;
	QAction* ImportFrameCameraOrientationAction2;
	QAction* FrameCameraSetRefsysAction;

	// CTFW QAction
	QAction* exportTFWTextFileAction;

	// CVDEM Qaction
	QAction* interpolateHeightfromDEMAction;
	QAction* interpolateHeightsfromDEMAction;
	QAction* displayDEMPropertiesAction;
	QAction* displayDEMStatisticsAction;
	QAction* deleteDEMStatisticsAction;
	QAction* exportImageDEMAction;
	QAction* exportASCIIDEMAction;
	QAction* exportASCIIDEMVerticalFlipAction;
	QAction* detectBuildingsAction;
	QAction* setDEMActiveAction;
	QAction* unsetDEMActiveAction;
	QAction* transformDEMAction;
	QAction* DEMoperationAction;


	// CVALS Qaction
	QAction* mergeALSAction;
	QAction* exportALSAction;
	QAction* interpolateDEMfromALSAction;
	QAction* interpolateIntensityFromALSAction;
	QAction* filterALSAction;

	// CXYZPolyLineArray  Qaction
	QAction* exportXYZPolyLinesTextFileAction;
	QAction* exportXYZPolyLinesVRMLAction;
	QAction* exportXYZPolyLinesDXFAction;
	QAction* TransformXYZPolyLines3DAction;
	//addition >>
	QAction* exportXYZPolyLinesShapefileAction;
	//<< addition

	// CBuildingArray  Qaction
	QAction* exportBuildingsTextFileAction;
	QAction* exportBuildingsVRMLAction;
	QAction* exportBuildingsDXFAction;
	

	// CXYContourArray Qaction
	QAction* thinoutXYContoursAction;

	// CXYPolyLineArray Qaction
	QAction* monoplotxyPolyLinesRPCAction;
	QAction* monoplotxyPolyLinesAffineAction;
	QAction* geoTFWPolyLinesAction;
	QAction* geo2DConformalPolyLinesAction;

	// CXYZPointArray Qaction
	QAction* ChangeSigmaXYZAction;
	QAction* DifferencebyLabelXYZAction;
	QAction* DifferencebyIndexXYZAction;
	QAction* export3DSpacedTextFileAction;
	QAction* export3DCSVTextFileAction;
	QAction* exportDDMMSSsssFileAction;
	QAction* xyz_to_xyRPCAction;
	QAction* xyz_to_xyAffineAction;
	QAction* xyz_to_xyTFWAction;
	QAction* xyz_to_xyPushbroomAction;
	QAction* xyz_to_xyFrameCameraAction;
	QAction* setControlAction;
	QAction* unsetControlAction;
	QAction* XYZtoGeographicAction;
	QAction* XYZtoGeocentricAction;
	QAction* XYZtoUTMAction;
	QAction* XYZtoTMAction;
	QAction* InterpolateDEMAction;
	QAction* MatchGCPsAction;
	//addition>>
	QAction* Get3DResidualAction;	
	QAction* export3DShapefileAction;
	//<<addition
	QAction* CopyToOtherListAction;

    // CXYEllipseArray  QAction
	QAction* getEllipseCentersAction;
	QAction* exportEllipsestoFileAction;
	QAction* exportEllipseCenterstoFileAction;

	// general QAction
	QAction* DeleteObjectAction;
	QAction* RenameObjectAction;
	QAction* setReferenceInformationAction;
	QAction* OnOffAction;

	QMenu *submenuOne;
	QMenu *submenuTwo;
	QMenu *submenuThree;
};
