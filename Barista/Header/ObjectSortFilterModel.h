#ifndef _CObjectSortFilterModel_
#define _CObjectSortFilterModel_

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */
#include <QSortFilterProxyModel>
#include "ObjectDataModel.h"

class QAction;

class CObjectSortFilterModel: public QSortFilterProxyModel
{
	Q_OBJECT
public:
	CObjectSortFilterModel(CObjectDataModel* dataModel);
	~CObjectSortFilterModel(void);

	void setShowSelector(bool show) {this->dataModel->setShowSelector(show);};
	void setShowSigma(bool show) {this->dataModel->setShowSigma(show);};
	void setShowCoordinates(bool show) {this->dataModel->setShowCoordinates(show);};
	void setAllowSigmaChange(bool allow) {this->dataModel->setAllowSigmaChange(allow);};
	void setHeader(const QStringList& headerList) {this->dataModel->setHeader(headerList);};
	void setFormatString(const QStringList& format) {this->dataModel->setFormatString(format);};
	void setAllowDeletePoints(bool allow) {this->dataModel->setAllowDeletePoints(allow);};
	void setAllowChangeLabels(bool allow) {this->dataModel->setAllowChangeLabels(allow);};
	void setAllowBestFitEllipse(bool allow) {this->dataModel->setAllowBestFitEllipse(allow);};
	void setAllowXYZAveragePoints(bool allow) {this->dataModel->setAllowXYZAveragePoints(allow);};

	int getNameColumn() const { return this->dataModel->getNameColumn();};
	vector<int> getSigmaColumns() const { return this->dataModel->getSigmaColumns();};
	void requestWindowTitle() const {this->dataModel->requestWindowTitle();};

	void rightMouseButtonReleased(const QModelIndexList &list,vector<QAction*>& actions);
	DataModelType getDataModelType() const {return this->dataModel->getDataModelType();};

	CObsPoint* getObsPoint(const QModelIndex& modelIndex);
	GenericPolyLine* getPolyLine(const QModelIndex& modelIndex);

	CReferenceSystem* getReferenceSystem() {return this->dataModel->getReferenceSystem();};

protected:
	CObjectDataModel* dataModel;
};
#endif