#ifndef CObstPointDataModel
#define CObstPointDataModel

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "ObjectDataModel.h"



class CObsPointArray;
class CObsPoint;
class CCharString;
class CFileName;
class CVImage;

class CObsPointDataModel : public CObjectDataModel // CObjectDataModel is a class derived using the QAbstractTableModel
{
	Q_OBJECT
public:
	CObsPointDataModel(CObsPointArray* pointArray,ViewParameter viewParameter,CVImage* vimage = NULL);
	~CObsPointDataModel();

	virtual void elementsChanged(CLabel element="");

	virtual int rowCount(const QModelIndex& parent) const;
	virtual int columnCount(const QModelIndex& parent) const;
	virtual QVariant data(const QModelIndex &index, int role) const;
	virtual QVariant headerData(int section,Qt::Orientation orientation, int role) const;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const;
	virtual bool setData(const QModelIndex &index, const QVariant &value,int role = Qt::EditRole);
	virtual bool removeRows (int row,int count,const QModelIndex & parent = QModelIndex());
	

	virtual vector<int> getSigmaColumns() const;
	virtual int getDimension() const;
	virtual void rightMouseButtonReleased(const QModelIndexList &mappedList,vector<QAction*>& actions);
	virtual void requestWindowTitle();


	virtual CObsPoint* getObsPoint(int index);
	virtual CReferenceSystem* getReferenceSystem();

public slots:
	void invertSelection();
	void changeSigmas();
	void removefromTable();
	void bestFit2DEllipse();
	void bestFit3DEllipse();
	void averagePoints();

signals:
	virtual void dataModelChanged(DataModelChanges changeType,vector<QString> parameter);

protected:

	int getFirstSigmaColumn()const;
	int getFirstCoordinateColumn() const;

	virtual QString guessTitlePrefix();
	virtual QStringList guessHeader();
	virtual QStringList guessDecimals();	

	CObsPointArray* points;
	vector<double> facSigma;
	bool isXYZMonoplotted;
	CVImage* image;
};

#endif
