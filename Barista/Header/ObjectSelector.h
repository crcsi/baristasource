#pragma once
#include "Bui_ObjectChooserDlg.h"

#include "BaristaProject.h"
class ZObject;

#define DEMSELECTOR	0
#define AFFINESELECTOR      1 
#define IMAGESELECTOR       2
#define RPCSELECTOR         3
#define XYPOINTSELECTOR     4
#define XYZPOINTSELECTOR    5
#define ALSSELECTOR			6
#define ORBITMERGESELECTOR  7

extern CBaristaProject project;

class CObjectSelector
{
public:
    CObjectSelector(void);
    ~CObjectSelector(void);

    void setSelector(int selector);
    int getSelector();
	vector<ZObject*> getMultiSelection();

	void setMultiSelection(bool b);
	int getItemCount();

    void makeDEMSelector();
    void makeImageSelector(eImageSensorModel sensorType = eUNDEFINEDSENSOR);
    void makeXYPointSelector();
    void makeXYZPointSelector();
    void makeRPCSelector();
    void makeAffineSelector();
    void makeALSSelector();
	void makeOrbitMergeSelector(CPushBroomScanner* selectedScanner);

    ZObject* getSelection();

  
    void setTitle(const char* title);
    void setButtonText(const char* title);
    void setModal(bool modal);
    void setExec();
    void show();
    

protected:
    bui_ObjectChooserDlg dlg;
    int selector;
    void insertItem(const char* name, ZObject* object);
	vector<CXYPointArray*> xyPointArray;

	int itemcount;

protected:
	static QSize dlgSize;

};
