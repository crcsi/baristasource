#ifndef BUI_MONOPLOTDLG_H
#define BUI_MONOPLOTDLG_H
#include <QDialog>
#include "ui_MonoPlotDlg.h"
#include "Label.h"
#include "ReferenceSystem.h"


class bui_MonoPlotDlg :	public QDialog, public Ui_MonoPlotDlg
{
Q_OBJECT

  protected:
	
	  CReferenceSystem refsys;

	  const char* plabel;
	  const char* chipID;
	  double X;

	  double Y;

	  int zone;

	  bool gotTMvalues;

	  double centralMeridian, latitudeOrigin;
	  
	  double singleHeight;

  public:

	  double Z;

	  bool labeledited;

	  bool heightedited;

	  bui_MonoPlotDlg();

	  ~bui_MonoPlotDlg();

	  CLabel getLabelValueAsLabel();

	  void setRefSys(const CReferenceSystem &I_refsys);

	  void setDlgValues( double X, double Y, double Z, const char * label, bool setchecked = true);

	  void setChipDlgValues( double Xc, double Yc, double Zc, const char * label, const char *chipID, bool setchecked = true);

  	  double getsingleHeight();
	  
	  bool getSuccess() {return this->success;};


public slots:

	void GridButtonclicked();
    void localxyzButtonclicked();
    void geocentricButtonclicked();
    void geographicButtonclicked();
    void changeLabel_clicked();
    void changeHeight_clicked();
	void Ok_clicked();
	void Cancel_clicked();
	void ChangeChipInfo_clicked();
  protected:
	  
	  void initTMparameters();

	  void LabelValueSetText( const char * label );

	  void setXValue(double X, int digits);

	  void setYValue(double Y, int digits);

	  void setZValue(double Z, int digits);

	  void gridsetchecked();

	  void localxyzsetchecked();

	  void geographicsetchecked();

	  void geocentricsetchecked();
	bool success;
};
#endif // BUI_MONOPLOTDLG_H