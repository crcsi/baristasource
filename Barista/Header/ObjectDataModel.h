#ifndef _CObjectDataMOdel_
#define _CObjectDataMOdel_

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include <QStandardItemModel>
#include "UpdateListener.h"
#include <vector>

#define SortingRole 34

typedef struct {
	bool showSelector;
	bool showSigma;
	bool showCoordinates;
	bool allowSigmaChange;
	bool allowDeletePoint;
	bool allowLabelChange;
	bool allowBestFitEllipse;
	bool allowXYZAveragePoints;
}ViewParameter;


typedef enum {eObjectName,eObjectAdded,eObjectDeleted} DataModelChanges;
typedef enum {eModelNotInitialized,eObsPointModel,eBuildingModel,ePolyLineModel} DataModelType;

class QAction;
class CObsPoint;
class CReferenceSystem;
class GenericPolyLine;

class CObjectDataModel :  public QAbstractTableModel, public CUpdateListener
{
	Q_OBJECT
public:
	CObjectDataModel(ViewParameter viewParameter);
	~CObjectDataModel(void);

	virtual void elementAdded(CLabel element="");
	virtual void elementDeleted(CLabel element="");
	virtual void elementsChanged(CLabel element="");


	virtual void setShowSelector(bool show);
	virtual void setShowSigma(bool show);
	virtual void setShowCoordinates(bool show);
	virtual void setAllowSigmaChange(bool allow);
	virtual void setHeader(const QStringList& headerList);
	virtual void setFormatString(const QStringList& format);
	virtual void setAllowDeletePoints(bool allow);
	virtual void setAllowChangeLabels(bool allow);
	virtual void setAllowBestFitEllipse(bool allow);
	virtual void setAllowXYZAveragePoints(bool allow);

	virtual int getNameColumn() const {return (this->vp.showSelector ? 1 : 0);};
	virtual vector<int> getSigmaColumns() const { vector<int> empty; return empty;};
	virtual void requestWindowTitle(){};

	const ViewParameter& getViewParameter() const { return this->vp;};

	virtual void rightMouseButtonReleased(const QModelIndexList &mappedList,vector<QAction*>& actions);

	DataModelType getDataModelType() const {return this->dataModelType;};

	virtual CObsPoint* getObsPoint(int index) {return NULL;};

	virtual GenericPolyLine* getPolyLine(int index) { return NULL;};

	virtual int getDimension() const {return 0;};
	virtual CReferenceSystem* getReferenceSystem() {return NULL;};
signals:
	virtual void dataModelChanged(DataModelChanges changeType,vector<QString> parameter);

protected:

	virtual QString guessTitlePrefix(){ return QString();};
	virtual QStringList guessHeader(){return QStringList();};
	virtual QStringList guessDecimals(){return QStringList();};	

	ViewParameter vp;
	QModelIndexList list;
	bool listenToSignal;

	QStringList headerList; // list of column headers
	QStringList formatList; // data fortmats, for example, number of digits after the decimal point etc.
	QString titlePrefix; // table title

	DataModelType dataModelType;

};
#endif