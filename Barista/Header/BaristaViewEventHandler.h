#ifndef __CBaristaViewEventHandler__
#define __CBaristaViewEventHandler__
 
#include <QMouseEvent>
#include <QKeyEvent>
#include <QWidget>

#include "OpenBaristaViewList.h"
#include "UpdateListener.h"


class CBaristaView;
class bui_MainWindow;

class CBaristaViewEventHandler : public CUpdateListener
{
  protected:

	static COpenBaristaViewList baristaViews;

	QWidget* parent;

	CCharString statusBarText;

	static bool inGeoLinkMode;

	bool shiftdown;
	bool ctrldown;

	void resetFirstSecondClickForAllViews();

  public:

	CBaristaViewEventHandler() : parent(0) ,statusBarText(""),shiftdown(false),ctrldown(false) { };

	virtual ~CBaristaViewEventHandler() { };

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyPressEvent (QKeyEvent *e, CBaristaView *v);
	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	void setParent(QWidget* p) {this->parent = p;};
	QWidget* getParent() { return this->parent; };

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v) = 0;
	bool handleMiddleMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v) = 0;
	bool handleMiddleMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v) = 0;

	virtual void handleLeaveEvent ( QEvent * event, CBaristaView *v );

	virtual void handleEnterEvent ( QEvent * event, CBaristaView *v ){};
	  
	bool handleMouseButtonEvent(QMouseEvent *e, CBaristaView *v) // QMouseEvent class contains parameters that describe a mouse event
	{
	if (e->button() == Qt::LeftButton) 
	{
		if (e->type() == QEvent::MouseButtonPress)  return handleLeftMousePressEvent(e, v);
		if (e->type() == QEvent::MouseButtonRelease) return handleLeftMouseReleaseEvent(e, v);
	}
	else if (e->button() == Qt::MidButton) 
	{
		if (e->type() == QEvent::MouseButtonPress) this->handleMiddleMousePressEvent(e,v);
		if (e->type() == QEvent::MouseButtonRelease) this->handleMiddleMouseReleaseEvent(e,v);
	}
	else if (e->button() == Qt::RightButton) 
	{
		if (e->type() == QEvent::MouseButtonPress)  
			return handleRightMousePressEvent(e, v);

		if (e->type() == QEvent::MouseButtonRelease) 
		{
			v->createPopUpMenu();			
			if( handleRightMouseReleaseEvent(e, v))
			v->showPopUpMenu();
			v->deletePopUpMenu();
		}
	}

	return false;
	}

	virtual bool redraw(CBaristaView *v) = 0;
	virtual void initialize(CBaristaView *v) = 0;

	virtual void setCurrentCursor(CBaristaView *v) = 0;

	static int getNumberOfViews() { return baristaViews.getNumberOfViews(); };

	static CBaristaView *getView(int index) { return baristaViews.getView(index); };

	static void addBaristaView(CBaristaView *view) { baristaViews.addView(view,CBaristaViewEventHandler::inGeoLinkMode); };

	static void deleteBaristaView(CBaristaView *view) { baristaViews.deleteView(view); };

	virtual void open();

	virtual void close();

	virtual void elementAdded(CLabel element=""){};
	virtual void elementDeleted(CLabel element=""){};
	virtual void elementsChanged(CLabel element=""){};

	void setStatusBarText(QString additionalText = "");

	bool getCTRLisDown() { return this->ctrldown; };
	bool getShiftisDown() { return this->shiftdown; };

	void setCTRLPressed(bool b) {this->ctrldown = b;};

	void addListenerToOpenViewList(CUpdateListener* listener);
	void removeListenerFromOpenViewList(CUpdateListener* listener);


	void activateGeoLinkMode();
	void deactivateGeoLinkMode();
	bool inGeoLinMode()const {return this->inGeoLinkMode;};

	void doGeoLink(CBaristaView* view,bool changeResampling = false);

	virtual void drawingSettingsChanged(QColor& newColor,unsigned int newLineWidth){};

	bui_MainWindow* getBaristaMainWindow();

	bool isOpen(CVImage* image)const;

	virtual void setCurrentImageLabel(const CLabel& l) {};

	virtual bool allowSetCurrentImageLabel() const { return false;};

};





extern CBaristaViewEventHandler *currentBaristaViewHandler;

#endif