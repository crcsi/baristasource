#pragma once
#include <QTableWidget>
#include <QString>
#include "Label.h"
#include "UpdateListener.h"

class CBaristaTable : 	public QTableWidget, public CUpdateListener
{
private:
    Q_OBJECT // needed to connect signals / slots

protected:
	CBaristaTable(QWidget* parent);

public:
	~CBaristaTable(void);

	QTableWidgetItem* createItem(QString value,Qt::ItemFlags flags,Qt::Alignment alignment, QColor textColor);

	CLabel getItemAsLabel(int row, int col);
	void setLabelAsItemText(CLabel label, int row, int col);

	QModelIndex getIndexOfItem(QTableWidgetItem* item);

	virtual void initTable()=0;
    virtual void elementsChanged(CLabel element);

    void updateContents();
 
	

protected:
	bool firstDraw;

};
