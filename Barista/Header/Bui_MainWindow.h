#ifndef BUI_MAINWINDOW_H
#define BUI_MAINWINDOW_H

#include "ui_MainWindow.h"
#include <QMdiArea>
#include "BaristaProject.h"
#include "IniFile.h"
#include <QVariant>
#include <QSplitter>
#include <QLayout>
#include <QToolTip>
#include <QWhatsThis>
#include <QAction>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QToolButton>
#include <QImage>
#include <QPixmap>

#include "BaristaTreeWidget.h"


#include "ViewingHandler.h"
#include "DigitizingHandler.h"
#include "ZoomInHandler.h"
#include "ZoomOutHandler.h"
#include "PanHandler.h"
#include "SeedHandler.h"
#include "CameraMountingHandler.h"
#include "MonoplottingPointHandler.h"
#include "MonoplottingLineHandler.h"
#include "MonoplottingSnakeHandler.h"
#include "MonoplottingBuildingHandler.h"
#include "MonoplottingHeightHandler.h"
#include "LSMHandler.h"
#include "MeasureHandler.h"
#include "GCPDataBasePoint.h"
#include "GCPchipHandler.h"

extern CBaristaTreeWidget* thumbnailView;
extern QMdiArea* workspace;



class bui_MainWindow : public QMainWindow, public Ui_MainWindow
{
  protected:

	bool saved;
	bool xPressed;
	
	CViewingHandler viewingHandler;
	CDigitizingHandler digitizingHandler;
	CZoomInHandler zoomInHandler;
	CZoomOutHandler zoomOutHandler;
	CPanHandler panHandler;
	CSeedHandler seedHandler;
	CMonoplottingPointHandler monoplottingPointHandler;
	CMonoplottingLineHandler monoplottingLineHandler;
	CMonoplottingSnakeHandler monoplottingSnakeHandler;

#ifdef WIN32	
	CMonoplottingBuildingHandler monoplottingBuildingHandler;
#endif	
	CMonoplottingHeightHandler monoplottingHeightHandler;
	CLSMHandler lsmHandler;
	CMeasureHandler measureHandler;
	CCameraMountingHandler cameraMountingHandler;
	CGCPchipHandler gcpchiphandler;

	void PanSharpening(bool orthorectified);
	

Q_OBJECT
public:
    bui_MainWindow(QWidget *parent = 0,const char* name=0, Qt::WindowFlags flags = 0);
    ~bui_MainWindow();

//signals
	void closeEvent(QCloseEvent* e);
	bool closeProject();

	//addition>> // GCP chips
	bool closeChipDbase();
	
	//<<addition

	void initBaristaEmpty();
	void initBarista();

	void initTimer();

public slots:

	void thumbnailDblClick( QTreeWidgetItem * item, int i );

	//file									FILE
	void fileNew();							//new
	void fileOpen();						//open
	void fileSave();						//save
	void fileSaveAs();						//save as
	void fileClose();						//close
											//---------------			
	void importImages();					//import image(s)			
	void importImage();						//import image(s)
	void import3DPoints();					//import 3D ponts
	void importDEMs();						//import DEM(s)
	void importDEM();						//import DEM(s)
	void importALSData();					//import ALS data
	void importGISData();					//import GIS data
											//----------------
	void fileExit();						//exit


	//adjustment							ADJUSTMENT
	void AdjustmentBundleClicked();			//start adjusment
	void RPCRegenerateBiasCorrected();		//regenerate bias-corrected rpcs
	void exportAllupdatedIkonosRPCs();		//export rpc -> ikonos format
	void exportAllupdatedQuickBirdRPCs();	//export rpc -> quick bird format


	//image processing						IMAGE PROCESSING
	void OrthoImageGeneration();			//orthoimage
	void PanSharpenOrtho();					//pansharpening -> orthorectified images
	void PanSharpenOriginal();				//pansharpening -> original images
											//--------------------------------------
	void ImageRegistration();				//register images for pansharpening
											//register images using orthophotos
					//register images using image chips
											//--------------------------------------
	void MergeImageBands();					//merge image bands
											//alos merging tools
											//--------------------------------------
											//generate dem by image matching
											//compute staggered ads40 image


	//DEM processing						DEM PROCESSING
	void MergeDEM();						//merge dems
	void DifferenceDEM();					//difference dems


	//monoplotting							MONOPLOTTING
											//monoplot points
											//monoplot lines
											//monoplot buildings
											//monoplot heights

	
	//feature extraction					FEATURE EXTRACTION
											//region growing
	void BuildingsDetection();

	//window								WINDOW
											//image show
											//-----------
	void closeAllWindows();					//close all images


	//settings								SETTINGS
	void setProjectSettings();				//project settings
	void setMatchingParameters();			//matching parameters


	//help									HELP
	void acHelpGetStarted();				//get started
	void acHelpContens();					//contents
	void acHelpSearch();					//help
											//-----------
	void acHelpAbout();						//about
	

	
	void digitizeButtonToggle( bool b );
	
	
	void testline();
	void resetRPCtoOriginal();
	void RPCBundleClicked();
	void RPCBiasCorrectionNone();
	void RPCBiasCorrectionShift();
	void RPCBiasCorrectionShiftDrift();
	void RPCBiasCorrectionAffine();
	
	void validateBarista();
	
	bool setActiveDEM();

	void AffineBundleClicked();
	void ForwardIntersection();
	
	void PushbroomBundleClicked();
	void FrameCameraBundleClicked();
	

	

	// BaristaEventHandler switches
	void MonoplotPoints(bool b);

	void MonoplottingLines();
	void MonoplottingLines(bool b);
	void MonoplottingSnakeLines();
	void MonoplottingSnakeLines(bool b);

	void MonoplotHeights(bool b);

	void MonoplotBuildings(bool b);

	void setPanMove(bool b);
	void setZoomIn(bool b);
	void setZoomOut(bool b);
	void regionGrowing(bool b);
	void MatchDEM();
	void RegisterImagesByOrthophotos();

	void measureButtonToggle(bool b);
	void lsmButtonToggle(bool b);
	void scpCollectionButtonToggle(bool b);
	void scpCollectionButtonToggle();
	void geoLinkModeToggle(bool b);
	void barista3DViewToggle(bool b);

	void shortCutActivated();
	void mergeALOSImages();
	void mergeALOSBands();
	void computeADS40StaggeredImage();


	void startCameraMountingHandler(CVImage* vimage);
	void endCameraMountingHandler();

	void loadGCPdatabase();
	
	//void selectGCPPoints(vector <CGCPDataBasePoint> gcpDB, ANNidxArray &nnIdx);
	//bool selectGCPPoints(CXYZPointArray* ctrlPoints, ANNidxArray &nnIdx);
	

	void imageShow();

	//addition>> GCP chips
	void chipDbaseNew();					//new	
	void chipDbFileSave();					//save chip database
	void chipDbFileOpen();					//open chip database
	void chipDbClose();
	//<<addition

public:
	CPanHandler& getPanHandler() {return this->panHandler;};

	bool openFile(CFileName projectName);
	void setTitle(CFileName title);

	bool AskForHeightMonoplotting();

	//digitize menu
	QAction* DigitizeAction;
	QAction* MeasureAction;
	QAction* LSMAction;
	QAction* SCPcollectionAction;

	//monoplotting lines menu
	//QAction* MonoplotLinesAction;
	QAction* MonoplottingLinesAction;
	QAction* MonoplottingSnakeLinesAction;

	void setCollectGCPActionActive(bool flag) {this->isCollectGCPActionActive = flag;};
	bool getCollectGCPActionActive() {return this->isCollectGCPActionActive;};

protected:
	QTimer* dongleTimer;

	//digitize menu
	QToolButton* DigitizeButtons;
	QMenu* digitizeMenu;

	//monoplotting lines menu
	QToolButton* MonoplotLinesButton;
	QMenu* monoplotLinesMenu;

	bool isCollectGCPActionActive;
};

#endif // BUI_MAINWINDOW_H