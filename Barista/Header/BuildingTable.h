#ifdef WIN32

#ifndef __CBuildingTable__
#define __CBuildingTable__

#include "BaristaTable.h"

class CBuildingArray;

class CBuildingTable : public CBaristaTable
{

    Q_OBJECT // needed to connect signals / slots
public:
	CBuildingTable(CBuildingArray* buildings,QWidget* parent = 0);
	~CBuildingTable(void);

    virtual void initTable();
	
	virtual void elementAdded(CLabel element);
    virtual void elementDeleted(CLabel element);
	virtual void elementSelected(bool selected,CLabel element="",int elementIndex=-1);

    void setBuildings(CBuildingArray* buildings);
    CBuildingArray* getBuildings() 	{return this->buildings;}

    void closeEvent ( QCloseEvent * e );

protected:



    CBuildingArray* buildings;

	int xdezimals;
	int ydezimals;
 
public slots:
	void changedValue( int row, int col );
	void contextMenuEvent(QContextMenuEvent * e);
	void reconstructBuildings();
	void removefromTable ();
	void itemSelectionChanged ();
};

#endif

#endif // WIN32