#ifndef BUI_BUILDINGSDETECTOR_H
#define BUI_BUILDINGSDETECTOR_H
#include <QDialog>
#include "ui_BuildingsDetector.h"
#include "Label.h"
#include "2DPoint.h"
#include "XYPolyLine.h"
class CVImage;
class CVALS;
class BuildingsDetector;
class CTFW;
class CVDEM;


class bui_BuildingsDetector :	public QDialog, public Ui_BuildingsDetector
{
Q_OBJECT

  protected:
	
	  CVALS *ALSfile;

  	  CVImage *colorImage;
	  CVImage *pSigmaNDVI;
	  CVDEM *pDEM;

	  CVImage *orientationImage;
	  CVImage *edgeImage;
	  CVImage *entropyImage;

	  BuildingsDetector *Building;
	  const CTFW *imgTFW;
	  const CTFW *demTFW;
	  //CTFW *maskTFW;

  	  C2DPoint llPoint;
	  C2DPoint urPoint;
	  C2DPoint MAXllPoint;
	  C2DPoint MAXurPoint;
	  C2DPoint resolution;
	  C2DPoint demResolution;

  	  bool extentsInit;
      bool success;
	  bool maskMode; // true for DEM mode, false for Histogram mode
	  bool detectionMode; // true for new building detection mode, false for old building refinement
	  bool useNDVIinExtension; // use NDVI during initial building extension
	  bool useEntropy; //use entropy before removing lines as tree edge
	  bool useHistogram; //use orientation histogram to remove false candidates
	  bool useAdjustment;
	  CXYPolyLine *boundaryPoly;

	  void setupExtents(int code); // 1 for checking all, 2 for ALS, 3 for colour image, 4 for ndvi image, 5 for DEM
	  void setCurrentExtentsAndResolution();
	  
	  static const char *noSigmaNDVITxt;


  public:

	 double Z;
	 bui_BuildingsDetector(CVALS * als=0);
	~bui_BuildingsDetector();
  public slots:

	void Cancel();
	void Ok();
	void Help();
	void ALSSelected();
	void ImageSelected();
	void maxExtentsSelected();
	void LLChanged();
	void RUChanged();
	void explorePathSelected();
	void checkSetup();
	bool getSuccess();
	bool detectBuildings();
	void NDVISigmaSelected();
	void DEMSelected();
	void DEMmodeSelected();
	void HistogramModeSelected();
	void useEntropyClicked();
	void useHistogramClicked();
	void OrientationSelected();
	void EdgeImageSelected();
	void EntropySelected();
	void newDetectionModeSelected();
	void oldRefineModeSelected();
  protected:
  
  CCharString currentDir;
  CCharString outputDir;
  static const char *noImageTxt;

  void setupImageSelector (QComboBox *box, CVImage *selectedImage, const char *descriptor);
  void setupALSSelector (QComboBox *box, CVALS *selectedALS, const char *descriptor);
  void setupDEMSelector (QComboBox *box, CVDEM *selectedDEM, const char *descriptor);
 private:
	
   void init();


};
#endif // BUI_MONOPLOTDLG_H