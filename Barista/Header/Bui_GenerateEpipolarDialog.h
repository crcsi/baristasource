#ifndef BUI_GenerateEpipolarDialog_H
#define BUI_GenerateEpipolarDialog_H

#include <qdialog.h>
#include "CharString.h"
#include "ui_GenerateEpipolar.h"

class CVImage;
class bui_ImageSensorSelector;

class bui_GenerateEpipolarDialog : public QDialog , public Ui_GenerateEpipolarDialog
{Q_OBJECT

  protected:

	  bui_ImageSensorSelector *leftImageSelector;
	  bui_ImageSensorSelector *rightImageSelector;

	  CVImage *leftImage;
	  CVImage *rightImage;

	  bool success; 

	  CCharString outputPath;

  public:

	  bui_GenerateEpipolarDialog (CVImage *left);

	  ~bui_GenerateEpipolarDialog();

	  bool getSuccess() const { return this->success; }


  public slots:

	  void onLeftImageChanged();

	  void onRightImageChanged();

	  void onOutputPathPushed();

	  void OK();

	  void Cancel();

	  void Help();

  protected: 

	  void checkSetup();

	  void init();

	  void destroy();

};

#endif // BUI_GenerateEpipolarDialog_H