/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

bool HemisphereChooserDlg::NorthernButtonChecked()
{
    return this->NorthernButton->isChecked();
}

bool HemisphereChooserDlg::SouthernButtonChecked()
{
    return this->SouthernButton->isChecked();
}

void HemisphereChooserDlg::Ok_clicked()
{
   this->close();
}

void HemisphereChooserDlg::setNorthernButtonChecked()
{
    this->NorthernButton->setChecked(true);
}

void HemisphereChooserDlg::setSouthernButtonChecked()
{
    this->SouthernButton->setChecked(true);
}
