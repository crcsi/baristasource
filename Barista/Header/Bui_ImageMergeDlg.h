#ifndef BUI_IMAGEMERGEDLG_H
#define BUI_IMAGEMERGEDLG_H

#include <QDialog>
#include "ui_ImageMergeDlg.h"
#include "ImageMerger.h"

class Bui_GDALExportDlg;
class CVImage;
class CGDALFile;
class CGroupData;

class Bui_ImageMergeDlg : public QDialog, public Ui_ImageMergeDlg
{
	Q_OBJECT

public:
	Bui_ImageMergeDlg(QWidget *parent = 0);
	~Bui_ImageMergeDlg();


	bool getSuccess() const { return this->success;};
public slots:

	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBFileReleased();
	void cBAutoAdjustStateChanged(int state);
	void leTextChanged(const QString &text);
	void editingFinishedFirstCol();
	void editingFinishedLastCol();
	void rBNearestNeighbourReleased();
	void rBBiLinearReleased();
	void rBBiCubicReleased();
	void tWFilesItemSelectionChanged();
	void tWFilesItemChanged(QTreeWidgetItem* item,int col);

protected:

	void checkSetup();
	void loadImages();

	void selectImageGroup(unsigned int group);
	bool checkCameraName(CCharString& newName,const CCharString& oldName);

	bool success;
	int selectedCamera;

	vector<CGroupData> data;
};


class CGroupData
{
public:
	CGroupData(void) :  readyToImport(false),
						import(true),
						autoAdjustBoundary(true),
						method(CImageMerger::eBiLinear),
						firstCol(-1),
						lastCol(-1),
						exportDialog(0),
						mergedImage(0),
						item(0)
						{};

	~CGroupData();


public:
	bool readyToImport;
	bool import;
	bool autoAdjustBoundary;
	CImageMerger::InterpolationMethod method;
	CFileName filename;
	int firstCol;
	int lastCol;
	Bui_GDALExportDlg *exportDialog;
	CVImage* mergedImage;
	vector<CVImage*> inputImages;
	CCharString cameraName;
	QTreeWidgetItem* item;
};

#endif // BUI_IMAGEMERGEDLG_H
