#ifndef __CBaristaView__
#define __CBaristaView__

#include <QTreeWidgetItem>
#include <QMutex>
#include "Pixmap.h"
#include "2DPoint.h"
#include "XYPoint.h"
#include "UpdateListener.h"
#include "EntityDrawing.h"
#include "XYZPolyLine.h"
#include "VBase.h"
#include "MonoPlotter.h"
#include "GeoLinkThread.h"
#include "ImgBuf.h"
#include "TransferFunction.h"
#include "VImage.h"
#include "BaristaProject.h"

class CXYPolyLine;
class CXYPolyLineArray;
class CXYPointArray;
class CXYContour;
class CXYContourArray;
class CXYEllipseArray;
class CCharString;
class CVImage;
class CHugeImage;
class QFocusEvent;
class CBaristaViewEventHandler;
class CZoomInHandler;
class CZoomOutHandler;
class CDigitizingHandler;
class CViewingHandler;
class CSensorModel;
class QMenu;
class CVDEM;
class CTFW;
class bui_ChannelSwitchBox;

class CBaristaWindowStyle;

class CBaristaView : public QWidget, public CUpdateListener
{
	friend class CZoomOutHandler;
	friend class CZoomInHandler;
	friend class CDigitizingHandler;
	friend class CViewingHandler;
	friend class CGeoLinkThread;

Q_OBJECT
  protected:

	  int virtualWidth;
	  int virtualHeight;

	  C2DPoint pmin, pmax;

	  // transformation screen -> geometric entity
	  C2DPoint offset; 
	  double zoomFactor;
	  int scrollX;
	  int scrollY;
	  int scrollbarSize;

	  bool firstPaint;

	  bool lButtonDown;
	  bool rButtonDown;
	  bool mButtonDown;
     
	  bool vThumbPressed;
	  bool hThumbPressed;

	  bool rightArrowPressed;
	  bool leftArrowPressed;
	  bool upArrowPressed;    
	  bool downArrowPressed;

	  int xPressed; // x position of mouse button being pressed
      int yPressed; // y position of mouse button being pressed
      int xPosPressed;
      int yPosPressed;
	
	  C2DPoint firstClick;
      C2DPoint secondClick;
      CXYPoint cpos;

	  bool haslargeROI;
	  
	  bool showROI;
	  bool showMatchWindow;
	  bool showRubberLine;

	  bool hasROI;
      
	  bool CtrlDown;

	  bool TFWMonoplotting;
	  bool HeightMonoplotting;
	  
	  C2DPoint roiUL;
	  C2DPoint roiLR;
	  
	  double residualFactor;
  
	 // double Res3DXYFactor;
	 //double Res3DHtFactor;
	
	QObjectList qimages; 
    QObjectList offsets;

	  // these pixmaps make up the scroll bar stuff
      QImage hChannel;
	  QImage  vChannel;
      CPixmap  upArrow;
      CPixmap  downArrow;
      CPixmap  leftArrow;
      CPixmap  rightArrow;
      QImage  hThumbNib;
      QImage  vThumbNib;
      CPixmap  leftThumb;
      CPixmap  rightThumb;
      CPixmap  topThumb;
      CPixmap  bottomThumb;
      CPixmap  corner;

	  //these pixmaps are for zoomin/zoomout/pan
      CPixmap zoomin;
	  CPixmap zoomout;
	  CPixmap pan;

      QCursor *zoomin_cur;
      QCursor *zoomout_cur;
      QCursor *pan_cur;
      QCursor *cross_cur;
      QCursor *crossS_cur;
      QCursor *crossC_cur;


	float blue;
	float red;
	float green;
	float nir;

  // the popup menu of the view
	QMenu* contextMenu;
	  
	CEntityDrawing drawings;

	bool inGeoLinkMode;

	CMonoPlotter monoPlotter;

    CGeoLinkThread geoLinkThread;
	QMutex mutex;

	eResamplingType currentResamplingType;

	bool resamplingReset;

	float dataResolution;

  public:

	  CBaristaView(const CCharString &title,const CTransferFunction& transferFunction, 
  		           double xmin, double xmax, double ymin, double ymax, 
				   double xoffset, double yoffset, 
				   QWidget * parent = 0);
	
	  virtual ~CBaristaView();

	  int getViewWidth()  const { return QWidget::width() - this->scrollbarSize; };
      int getViewHeight() const { return QWidget::height() - this->scrollbarSize; };

	  int height() const {return  getViewHeight();};
	  int width() const {return  getViewWidth();};

	  virtual int getDataWidth(bool considerZoom = true) const;
	  virtual int getDataHeight(bool considerZoom = true) const;

	  void unselectROI();

	  void activateGeoLinkMode();

	  void deactivateGeoLinkMode();


	  void setShowROI(bool b){this->showROI = b;};
	  void setShowRubberLine(bool b) {this->showRubberLine = b;};
	  void setShowMatchWindow(bool b) {this->showMatchWindow = b;};

	  void setHasROI(bool b) {this->hasROI = b;};
	  bool getHasROI()const {return this->hasROI;};

	  void setHasLargeROI(bool b){this->haslargeROI = b;};
	  bool getHasLargeROI()const {return this->haslargeROI; };

	  void setTFWMonoplotting(bool b) {this->TFWMonoplotting = b;};
	  virtual bool getTFWMonoplotting()const {return false;};

	  void setHeightMonoplotting(bool b) {this->HeightMonoplotting = b;};
	  bool getHeightMonoplotting()const {return this->HeightMonoplotting;};

	  C2DPoint getROIUL() const { return this->roiUL; };
	  C2DPoint getROILR() const { return this->roiLR; };

	  void setROIUL(C2DPoint p) {this->roiUL = p; };
	  void setROILR(C2DPoint p) {this->roiLR = p; };

	  

	  C2DPoint getPmin() const { return this->pmin; };
	  C2DPoint getPmax() const { return this->pmax; };

	  C2DPoint getOffset() const { return this->offset; };
	  int  getScrollX() const { return this->scrollX; };
	  int  getScrollY() const { return this->scrollY; };
	  double getZoomFactor() const { return this->zoomFactor; };
	  int getScrollbarSize() const { return this->scrollbarSize; };

	  int  getXPressed() const { return this->xPressed; };
	  int  getYPressed() const { return this->yPressed; }; 
  	  int  getXPosPressed() const { return this->xPosPressed; };
	  int  getYPosPressed() const { return this->yPosPressed; }; 
	  bool getLeftButtonDown() const { return this->lButtonDown; };
	  bool getMiddleButtonDown() const { return this->mButtonDown; };
	  bool getRightButtonDown() const { return this->rButtonDown; };

	  void setScrollX(int x);
	  void setScrollY(int y);

	  void setCTRLPressed(bool b) {this->CtrlDown = b;};

	  void setFirstClick(C2DPoint first) { this->firstClick = first;};
	  C2DPoint getFirstClick() const { return this->firstClick;};

	  void setSecondClick(C2DPoint second) { this->secondClick = second;};
	  C2DPoint getSecondClick() const { return this->secondClick;};

	  void setZoomInCursor() { this->setCursor(*zoomin_cur);}
	  void setZoomOutCursor() { this->setCursor(*zoomout_cur);}
	  void setPanCursor() { this->setCursor(*pan_cur);}
	  void setCrossCursor() { this->setCursor(*cross_cur);}
	  void setCrossSCursor() { this->setCursor(*crossS_cur);}
	  void setCrossCCursor() { this->setCursor(*crossC_cur);}
	  void setArrowCursor() { this->setCursor(Qt::ArrowCursor);}
	  void setBusyCursor() { this->setCursor(Qt::BusyCursor);}

    
	  bool getMouseEventCoords(QMouseEvent * e, double &x, double &y);

	  bool fromScreen(double xscreen, double yscreen, double &x, double &y);

	  bool toScreen(double x, double y, double &xscreen, double &yscreen);




	  void drawCross( QPainter &p, const CObsPoint &point, 
		              QColor color = Qt::green, int width = 1, int size = 4);
	  
	  void drawCircle(QPainter &p, const CObsPoint &point, 
		                      QColor color, int width, int size);
	  void drawScaleLabel(QPainter &p, const CObsPoint &point, QColor color, int width, QString lbl);
	 
	  void drawLine( QPainter &p, const CObsPoint &start, const CObsPoint &end, 
		             QColor color = Qt::green, int width = 1);

	  void drawLine( QPainter &p, const C2DPoint &start, const C2DPoint &end, 
		             QColor color = Qt::green, int width = 1);

	  void drawLabel( QPainter &p, const CObsPoint &point, 
		              QColor color = Qt::green, int width = 1);

	  void drawEdge(QPainter &p,  const CXYContour &contour, 
		            QColor color = Qt::green, 
					QColor endcolor = Qt::yellow, int width = 1);

	  void drawThinnedEdge(QPainter &p,  const CXYPolyLine &poly, 
		                   QColor color = Qt::red, int width = 2);

	  virtual void drawPoint(QPainter &p, const CObsPoint &point, 
		              QColor color = Qt::green, int width = 1, int size = 4);

	  virtual CVImage *getVImage() const { return 0; };

	  virtual CVDEM* getVDEM() const {return 0;};

	  virtual CVBase* getVBase() const { return 0; };

	  virtual CHugeImage *getHugeImage() const { return 0; };

	  virtual const CTFW* getTFW() const {return 0;};

    // for monoplotting purposes;
      virtual bool isA(const char* className) const
      {
          if (::strcmp("CBaristaView", className) == 0) return true;
		  return false;
      };

   // signals

	  virtual void mousePressEvent ( QMouseEvent * e );

	  virtual void mouseReleaseEvent ( QMouseEvent * e );

	  virtual void mouseMoveEvent ( QMouseEvent * e );

	  virtual void wheelEvent ( QWheelEvent * e );

	  virtual void keyReleaseEvent ( QKeyEvent * e );

	  virtual void keyPressEvent (QKeyEvent * e );

	  virtual void closeEvent ( QCloseEvent * e );

	  virtual void drawContents (QPainter &p);

	  void drawContentsPre(QPainter &p);

	  void drawContentsPost(QPainter &p);

	  virtual void paintEvent(QPaintEvent* e);

	  virtual void drawXYPointArray(const CXYPointArray* xyPointArray,
									QPainter &p, shwLabel LablelType = lOn, shwPoint PointType = pCross,
									QColor LabelColor = Qt:: yellow, QColor PointColor = Qt:: yellow,
									int Width = 1, 
									int Size = 4,								
									int LabelWidth = 1);

	  virtual void drawXYContours(	CXYContourArray* contours,
									QPainter &p,
									QColor color = Qt::green, 
									QColor endcolor = Qt::yellow, 
									int width = 1);

	  virtual void drawXYPolyLines(CXYPolyLineArray* polyLines,
								   QPainter &p,
								   QColor color = Qt::black, 
								   int width = 2);

	  virtual void drawEllipses(CXYEllipseArray* ellipses,
								QPainter &p,
								QColor color = Qt::green, 
								int width = 1);



	  virtual CLabel getNextLabel() { return CLabel("POINT"); };


	  virtual void enterEvent ( QEvent * event );

	  virtual void leaveEvent ( QEvent * event );
	

	// functions to add and remove a single point to a dataset
	bool addPointDrawing(const CLabel& dataSetName,CObsPoint* p);
	bool removePointDrawing(const CLabel& dataSetName,const CLabel& p);

	// functions to query a dataset
	bool queryPointDrawing(const CLabel& dataSetName,const CLabel & pointLabel);

	// functions to add and remove polylines to a dataset
	bool addPolyLineDrawing(const CLabel& dataSetName,CXYPolyLine* p,const QColor& color,unsigned int lineWidth);
	bool removePolyLineDrawing(const CLabel& dataSetName,CXYZPolyLine* p);

	// functions to add and remove datasets
	bool addDrawingDataSet(CDrawingDataSet& dataSet);
	bool addDrawingDataSet(const CLabel& dataSetName,int dim,bool showPoints = true,bool showPointLabels = true, QColor pointColor = Qt::green);
	bool removeDrawingDataSet(const CLabel& dataSetName);
	void removeAllDrawingDataSets();

	// functions to add and remove topologies
	bool addTopology(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2,QColor& color,unsigned int lineWidth);
	bool removeTopology(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2);

	// querry functions for drawing
	bool getShowPoints(const CLabel& dataSetName);
	void setShowPoints(const CLabel& dataSetName,bool b);

	bool getShowPointLabels(const CLabel& dataSetName);
	void setShowPointLabels(const CLabel& dataSetName,bool b);

	QColor getPointColor(const CLabel& dataSetName);
	void setPointColor(const CLabel& dataSetName,QColor& color);

	// change color
	void changeTopologyColor(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2,QColor& color);
	void changeAllTopologyColors(const CLabel& dataSetName,QColor& color);

	// change line width
	void changeTopologyLineWidth(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2,unsigned int lineWidth);
	void changeAllTopologyLineWidth(const CLabel& dataSetName,unsigned int lineWidth);

	// rename dataSet
	bool renameDataSet(const CLabel& oldName,const CLabel& newName);

	virtual const CSensorModel* getCurrentSensorModel() {return 0;};


	 virtual void createPopUpMenu();

	 void deletePopUpMenu();
	
	 void addPopUpAction(QAction* action, bool addSeparator= false);
	
	 void showPopUpMenu();
	
	 void addContextMenuSeparator();

	virtual void elementAdded(CLabel element="");
    
	virtual void elementDeleted(CLabel element="");
	
	virtual void elementsChanged(CLabel element=""){};

	virtual void selectROI();

	void retrieveGreyValues(int x,int y,CHugeImage* himage);

	virtual void getPixelInfoHugeImage(QString& pixelInfo,
										 QString& geoInfo,
										 QString& colorInfo,
										 QString& zoomInfo,
										 QString& residualInfo,
										 QString& brightnessInfo,
										 CHugeImage* himage);
	void updateStatusBarInfo();

	void zoomToPoint(const C2DPoint& point,double zoomFactor,bool updateView);

	bool computeViewCentre( C2DPoint& centre);

	CMonoPlotter& getMonoPlotter();

	void doGeoLink(const CXYZPoint& point3D,const CReferenceSystem &refSysOf3DPoint, double zoomFactor,float pixelResolution,bool changeResampling = false);
  
	void changeResamplingType(eResamplingType newType);

	void resetResamplingType();

	  void clearImages();

	  float getDataResolution();

	  	  void resetFirstSecondClick();
protected:

	  virtual void zoomToFit();
	  virtual void zoomUp();
	  virtual void zoomDown();
	
	  void setZoomFactor(double zoomFactor);
	  void setVirtualWidth();
	  void setVirtualHeight();

	  virtual void refreshHugeImages(CHugeImage* himage);

	  void refreshImages();



    bool fetchTile(int r, int c,CHugeImage* himage);
    
    bool isImageThere(int r, int c);


	  void clearImage(int index);

	  virtual void doFirstDraw(int virtualHeight, int virtualWidth);

	  void paintBorders(QPainter &dc);
      void paintHBorder(QPainter &dc);
      void paintVBorder(QPainter &dc);
      void paintLeftArrow(QPainter &dc);
      void paintRightArrow(QPainter &dc);
      void paintUpArrow(QPainter &dc);
      void paintDownArrow(QPainter &dc);
      void paintCorner(QPainter &dc);
      void paintHThumb(QPainter &dc);
      void paintVThumb(QPainter &dc);

      bool inVBar(const C2DPoint &point);
      bool inHBar(const C2DPoint &point);

	  bool onVThumb(const C2DPoint &point);
      bool onHThumb(const C2DPoint &point);
      bool onDownArrow(const C2DPoint &point);
      bool onUpArrow(const C2DPoint &point);
      bool onLeftArrow(const C2DPoint &point);
      bool onRightArrow(const C2DPoint &point);
	  bool onVBarArea(const C2DPoint &point);
	  bool onHBarArea(const C2DPoint &point);

      bool onScrollBarArea(const C2DPoint &point);
    
	  
	
	  void changeBrightness(double factor);

	  void initEventHandler();

public slots:
	void setChannelDisplay();
	virtual void CropROI();
	virtual void ResetRoi();
	void callWallisFilterDlg();

	void updateFromThread();

	
	void setPRDisplay(); //settings for point and residual display for images
	
};


#endif

