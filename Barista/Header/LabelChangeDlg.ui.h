/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

const char* LabelChangeDlg::getLabelText()
{
    QString qstr = this->LabelInput->text();

    return  qstr.ascii();
}

void LabelChangeDlg::Ok_released()
{
    if ( !this->LabelInput->text().isEmpty())
        this->close();
}

void LabelChangeDlg::setLabelText(const char* text)
{
    this->LabelInput->setText(text);
}
