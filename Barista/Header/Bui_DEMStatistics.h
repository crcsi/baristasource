#ifndef BUI_DEMSTATISTICS_H
#define BUI_DEMSTATISTICS_H

#include <QDialog>
#include "ui_DEMStatistics.h"

class CVDEM;

class Bui_DEMStatistics : public QDialog, public Ui_Bui_DEMStatisticsClass
{
	Q_OBJECT

public:
	Bui_DEMStatistics(CVDEM* vdem,QWidget *parent = 0);
	~Bui_DEMStatistics();

	bool getSuccess() const { return this->success;};

	int getNStatisticRows() const { return this->nStatisticRows;};
	int getNStatisticCols() const { return this->nStatisticCols;};
	bool getShowAvg() const {return this->showAvg;};
	bool getShowDevAvg() const {return this->showDevAvg;};
	bool getShowMin() const {return this->showMin;};
	bool getShowMax() const {return this->showMax;};
	bool getShowDev() const {return this->showDev;};
	QColor getStatTextColor() const {return this->statTextColor;};
	QColor getStatLineColor() const {return this->statLineColor;};
	int getStatFontSize() const { return this->statFontSize;};
	int getNLegendeClasses() const { return this->nLegendeClasses;};
	bool getComputeHist() const { return this->computeLegende;};


public slots:
	void stateChangedAvg(int state);
	void stateChangedDevAvg(int state);
	void stateChangedMin(int state);
	void stateChangedMax(int state);
	void stateChangedDev(int state);
	void valueChangedRows(int nRows);
	void valueChangedCols(int nCols);
	void valueChangedFontSize(int fontSize);
	void pBOk();
	void pBCancel();
	void pBHelp();
	void pBTextColor();
	void pBLineColor();
	void valueChangedLegende(int value);
	void stateChangedLegende(int state);


private:
	bool success;
	int nStatisticRows;
	int nStatisticCols;
	bool showAvg;
	bool showDevAvg;
	bool showMin;
	bool showMax;
	bool showDev;
	QColor statTextColor;
	QColor statLineColor;
	CVDEM * vdem;
	int statFontSize;
	int nLegendeClasses;
	bool computeLegende;


};

#endif // BUI_DEMSTATISTICS_H
