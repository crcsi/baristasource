#ifndef BUI_ADJUSTDIALOG_H
#define BUI_ADJUSTDIALOG_H

#include <QDialog>
#include "ui_AdjustDialog.h"
#include <QItemDelegate>
#include "Filename.h"

#define N_SENSORMODELS 4
#define N_EXTRA_ROWS 1
#define IMAGE_SPAN (N_SENSORMODELS + N_EXTRA_ROWS)
#define ROW_HEIGHT 15
#define MAX_IMAGES 100

#define CHECKBOX_WIDTH
#define ICON_WIDTH 50
#define FilterRole 35

class CAdjustmentDlg;

class CImageDataModel;
class CObsPointArrayDataModel;
class Bui_VImageDlg;
class CVImage;

class Bui_XYZPointDlg;
class QListWidget;
class QListWidgetItem;
class CObservationHandlerPtrArray;
class CObsPointArray;
class QSortFilterProxyModel;
class CPointTableView;
class CXYZPointArray;
class C3DPoint;
class CReferenceSystem;
class CBaristaBundleHelper;
class CVControl;
class CCharStringArray;

class Bui_AdjustDialog : public QDialog,public Ui_AdjustDialogClass
{
	Q_OBJECT

public:
	Bui_AdjustDialog(QWidget *parent = 0);
	~Bui_AdjustDialog();

	void deleteMemory();

public slots:
	void pbOkReleased();
	void pbApplyReleased();
	void pbCancelReleased();
	void pbHelpReleased();

	void okButtonStatusChanged(bool b);
	void initVImageDlg(CVImage* image);
	void init3DPointDlg(CObsPointArray* pointArray,CObsPointArray* residualArray);
	void filteredImageClicked(const QModelIndex& index);
	void filteredPointArrayClicked(const QModelIndex& index);
	void filteredPointArrayRightClicked(const QModelIndex& index);
	void forceCurrentDlgToSave();

	void pBImageFilterReleased();
	void pBPointFilterReleased();
	void imageFilterItemClicked( QListWidgetItem * item );
	void pointFilterItemClicked( QListWidgetItem * item );	

	void viewProtocol();
	void view3DPoints();
	void runAdjustment();
	void checkAdjustmentSettings();
	void cBRobustStateChanged(int state); 
	void textChanged(const QString &text );
	void editingFinishedMaxRobust();
	void editingFinishedMinRobFac();
	void tWCurrentChanged( int index );
	void resetRobust();
	void cBForwardStateChanged(int state); 
	void cBExcludeTieStateChanged(int state); 

protected:
	void keyPressEvent( QKeyEvent * keyEvent);
	void saveProject(CFileName& filename);
	void loadProject(CFileName& filename);
	void deleteBackup();
	void writeAdjustmentResults(QString s0,QString nObs,QString nPar,QString redundancy,QString grossErrors);
	bool doQualityAssessment(CBaristaBundleHelper &helper, CObservationHandlerPtrArray *images);
	void getQualityValues(CObsPointArray* diffArray,int& nPoints,C3DPoint& rmsValues,C3DPoint& maxResValues);
	void applySettings();

	void exportAdjustmentInfo(CVControl* Control, CFileName infoFileName);
	void guessHeaderFormat(const CXYZPointArray* points, CCharStringArray& colHeading, int &precision1, int &precision2, double &ratio);
	
	void initView();	
	

	CFileName originalFileName;
	CFileName backupFileName;

	Bui_VImageDlg* vimageDlg;
	Bui_XYZPointDlg* obsPointDlg;

	CImageDataModel* imageDataModel;
	QSortFilterProxyModel* imageFilterModel;

	CObsPointArrayDataModel* obsPointDataModel;
	QSortFilterProxyModel* obsPointFilterModel;

	CPointTableView* pointView;

	QListWidget* lW_ImageFilter;
	QListWidget* lW_PointFilter;
	bool showImageFilter;
	bool showPointFilter;

	bool robust;
	bool forward;
	bool excludeTie;

	CFileName infoFileName;
	bool infoExportStatus;
	
	/// Maximum number of observations to be eliminated by robust estimation
	double maxRobustPercentage;

	/// Minimum normalised discrepancy for finishing robust estimation
	double minRobFac;
	
	// fliter images
	Qt::CheckState showPushBroom;
	Qt::CheckState showRPC;
	Qt::CheckState showAffine;
	Qt::CheckState showFrame;

	// filter points
	Qt::CheckState showGeographic;
	Qt::CheckState showGeocentric;
	Qt::CheckState showGrid;


	CFileName protocolFile;
	CFileName iterationFile;
	CFileName* currentProtocolFile;

	int oldTab;

	CXYZPointArray* bundleResult;

};


// ###############################################################################################
// ###############################################################################################
// ###############################################################################################


typedef struct {
	QString name;
	QPixmap thumbnail;
	bool show;
	bool isSelected;
	bool hasPushBroom;
	bool hasRPC;
	bool hasAffine;
	bool hasFrame;
	bool isCurrent;
	int activeModelType;
	CVImage* image;
}ImageDataElements;

class CImageDataModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	CImageDataModel(QObject* parent =0);
	int rowCount(const QModelIndex& parent) const;
	int columnCount(const QModelIndex& parent) const;
	QVariant headerData(int section,Qt::Orientation orientation, int role) const;
	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;

	void selectCurrentImage();
	void applyImageFilter(bool showPushBroom,bool showRpc,bool showAffine,bool showFrame);
	int activeImages() const;
	void getActiveObservationHandler(CObservationHandlerPtrArray *images,CObservationHandlerPtrArray *orbits);
	void itemClicked( const QModelIndex& index);

signals:
	void forceToSave();
	void selectedVImageChanged(CVImage* image);
	void selectionChaged();
private:

	void applyImageFilter();

	vector<ImageDataElements> localData;
	int currentImage;
	
};


// ###############################################################################################
// ###############################################################################################
// ###############################################################################################

typedef struct {
	QString name;
	bool show;
	bool isControl;
	bool isCheckPoint;
	CObsPointArray* pointArray;
	CObsPointArray* residualArray;
	int type;
}ObsPointDataElements;

class CObsPointArrayDataModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	CObsPointArrayDataModel(QObject* parent =0);
	int rowCount(const QModelIndex& parent) const;
	int columnCount(const QModelIndex& parent) const;
	QVariant headerData(int section,Qt::Orientation orientation, int role) const;
	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	bool setData(const QModelIndex &index, const QVariant &value,int role = Qt::EditRole);

	void itemClicked( const QModelIndex& index);
	void itemRightClicked(const QModelIndex& index);

	void selectCurrentPointArray();
	void initModel();
	void applyPointFilter(bool showGeographic,bool showGeocentric,bool showGrid);
	int activePointArrays() const;

public slots:
	void unsetControl();
	void setControl();

	void unsetCheckPoint();
	void setCheckPoint();

signals:
	void clicked( const QModelIndex& index);
	void selectedObsPointArrayChanged(CObsPointArray* pointArray,CObsPointArray* residualArray);
private:

	vector<ObsPointDataElements> localData;
	int currentArray;
	int currentControlArray;
	int currentCheckPointArray;
	QModelIndex selectedIndex;
	
};

// ###############################################################################################
// ###############################################################################################
// ###############################################################################################

class CImageTableDelegate : public QItemDelegate
{	Q_OBJECT
public:
	void paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const;
};


// ###############################################################################################
// ###############################################################################################
// ###############################################################################################

class CCheckBoxWithTextDelegate : public QItemDelegate
{
	Q_OBJECT

public:
	CCheckBoxWithTextDelegate(QObject* parent=0);
	~CCheckBoxWithTextDelegate(){};

	void paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const;
private:
	
};


// ###############################################################################################
// ###############################################################################################
// ###############################################################################################
class CPointTableView : public QTableView
{
	Q_OBJECT
public:

	void contextMenuEvent(QContextMenuEvent * e);

signals:
	void contextMenuCalled(const QModelIndex& index);
};

#endif // ADJUSTDIALOG_H
