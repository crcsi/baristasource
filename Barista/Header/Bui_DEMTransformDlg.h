#ifndef BUI_DEMTRANSFORMDLG_H
#define BUI_DEMTRANSFORMDLG_H

#include <QDialog>
#include "ui_DEMTransformDlg.h"
#include "Filename.h"
#include "3DPoint.h"
#include "GDALFile.h"

class CVDEM;

class Bui_GDALExportDlg;

class Bui_DEMTransformDlg : public QDialog, public Ui_DEMTransformDlgClass
{
	Q_OBJECT

public:
	Bui_DEMTransformDlg(CVDEM* vdem);
	~Bui_DEMTransformDlg();

	void setSuccess(bool b) { this->success = b;};
	bool getSuccess() {return this->success;};

public slots:
	
	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBSelectOutputReleased();

	void rbUTMButtonclicked();
    void rbGeographicButtonclicked();

	// check Boxes
	void cb1arcReleased();
	void cb3arcReleased();
	void cb30arcReleased();

	// spinboxes
	void spinBoxColsChanged(int newcols);
	void spinBoxRowsChanged(int newrows);

	// lineEdits connections
	void leLLXEditingFinished();
	void leLLYEditingFinished();
	void leGridXEditingFinished();
	void leGridYEditingFinished();

private:

	
	void init();
	void reset();

	bool transform();
	void checkSetup();
	bool createTransformedDEM();

	void getExtensions();

	void getColsRows();
	void getGridxGridy();

	bool success;
	CFileName trafoname;

	double gridx;
	double gridy;
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	int cols;
	int rows;

	int decimals;

	bool utmselected;
	bool geographicselected;

	C3DPoint ULin;
	C3DPoint URin;
	C3DPoint LLin;
	C3DPoint LRin;

	C3DPoint ULout;
	C3DPoint URout;
	C3DPoint LLout;
	C3DPoint LRout;


protected:
	CVDEM* srcdem;
	CVDEM* trafodem;
	Bui_GDALExportDlg* dlg;

};

#endif // BUI_DEMTRANSFORMDLG_H
