#ifndef __CBaristaIconViewItem__
#define __CBaristaIconViewItem__


#include <QTreeWidgetItem>
#include <QTreeWidget>

typedef enum  {eImages,eImage,eImageCaption,eXYPoints,eResiduals,eXYMonoPlottedPoints,
				eProjectedPoints,eDifferencedPoints,eRPCs,ePushBroom,eAffine,eTFWs,
				eXYContours,eEllipses,eEdges,eXYZArrays,eXYZPoints,ePolyLines,eBuildings,
				eDEMs,eDEM,eALS,eALSDataSets,eOrbitPaths,eOrbitAttitudes,ePath,eAttitudes,
				eCameras,eCCDCamera,eXYFiducialMarkObservations,eFrameCamera,ObjResiduals} IconViewType; //added by M. Awrangjeb on 20 Mar 2009

class ZObject;
class CVImage;
class CXYPointArray;
class CSensorModel;
class CXYZPointArray;

class CBaristaIconViewItem : public QTreeWidgetItem
{
public:
	CBaristaIconViewItem(QTreeWidgetItem* parent,IconViewType type,CVImage* image);

	CBaristaIconViewItem(QTreeWidget* parent,IconViewType type,ZObject* object,QString caption);
	CBaristaIconViewItem(QTreeWidgetItem* parent,IconViewType type,const ZObject* object,QString caption);

    ~CBaristaIconViewItem(void);

    void setObject(ZObject* object);
    const ZObject* getObject();

protected:
    const ZObject* object;
};

#endif
