#ifndef BUI_DIALOGCONTAINER_H
#define BUI_DIALOGCONTAINER_H

#include <QDialog>
#include "ui_DialogContainer.h"

class CAdjustmentDlg;


class Bui_DialogContainer : public QDialog,public Ui_DialogContainerClass
{
	Q_OBJECT

public:
	Bui_DialogContainer(CAdjustmentDlg	*dialog,QWidget *parent = 0,int width=337,int height=459);
	~Bui_DialogContainer();


public slots:
	void pbOkReleased();
	void pbCancelReleased();
	void pbHelpReleased();
	void okButtonStatusChanged(bool b);
	void closeDlg(bool b);

protected:
	CAdjustmentDlg	*dialog;

};

#endif // DIALOGCONTAINER_H
