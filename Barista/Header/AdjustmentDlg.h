#ifndef __CAdjustmentDlg__
#define __CAdjustmentDlg__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include <qdialog.h>
#include "CharString.h"
#include "UpdateListener.h"

class CAdjustmentDlg : public QDialog, public CUpdateListener
{
	Q_OBJECT
public:
	CAdjustmentDlg(QWidget *parent = 0);
	virtual ~CAdjustmentDlg(void);

	virtual bool okButtonReleased()=0;
	virtual bool cancelButtonReleased()=0;
	virtual CCharString helpButtonReleased()=0;
	virtual CCharString getTitleString()=0;

	bool getSuccess() {return this->success;};

	virtual void elementAdded(CLabel element="");
	virtual void elementDeleted(CLabel element="");
	virtual void elementsChanged(CLabel element="");
	virtual void ckeckDlgContent();

	virtual void cleanUp()=0;

	virtual int getCurrentTab() const { return this->lastTab;};
	virtual void setDefaultTab(int defaultTab) {this->defaultTab = defaultTab;};

	void setListenToSignal(bool b) { this->listenToSignal = b;};

	bool getListenToSignal() const { return this->listenToSignal;};

private slots:
	// see below!!
	virtual void elementsChangedSlot(CLabel element) = 0;

signals:
	virtual void enableOkButton(bool b)=0;
	virtual void closeDlg(bool b)=0;
	
	// when running the adjustment in a different thread then the gui thread,
	// updates will come back to a dialog and force an update of the gui
	// that is not possible and will cause errors e.g. program crashes!!
	// to avoid that, we emit this signal from the elementsChanged function of the dialog
	// and connect it with the elementsChangedSlot
	virtual void elementsChangedSignal(CLabel element)=0;

protected:

	virtual void keyPressEvent ( QKeyEvent * keyEvent);
	bool success;
	bool listenToSignal;
	
	int lastTab;
	int defaultTab;
};

#endif