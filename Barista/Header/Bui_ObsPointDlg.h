#ifndef BUI_OBSPOINTDLG_H
#define BUI_OBSPOINTDLG_H

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "AdjustmentDlg.h"
#include "ui_ObsPointDlg.h"

class CObsPointArray;
class CObjectTable;



class Bui_ObsPointDlg : public CAdjustmentDlg ,public Ui_ObsPointDlgClass
{
	Q_OBJECT

public:
	Bui_ObsPointDlg(CObsPointArray *pointArray,
					bool showSelector = true,
					bool showSigma = true,
					bool allowSigmaChange = true,
					bool allowDeletePoint = false,
					bool allowLabelChange = false,
					QWidget *parent = 0);

	~Bui_ObsPointDlg();

	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString() {return "Points";};

	virtual void elementAdded(CLabel element="") {};

	virtual void elementDeleted(CLabel element);
	virtual void cleanUp();
private slots:
	virtual void elementsChangedSlot(CLabel element);

signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

protected:
	CObjectTable* tableView;
};


#endif // OBSPOINTDLG_H
