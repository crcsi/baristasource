#ifndef BUI_PROGRESSDLG_H
#define BUI_PROGRESSDLG_H

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include <qdialog.h>
#include "ui_ProgressDlg.h"
#include <QTimer>


class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QProgressBar;

class CProgressListener;
class CProgressThread;
class CProgressHandler;


class bui_ProgressDlg :	public QDialog, public Ui_CProgressDlg
{Q_OBJECT
public:
	bui_ProgressDlg(CProgressThread* thread, CProgressHandler* handler,const char* title = "Progress");

	~bui_ProgressDlg(void);

	void setTitleString(const char* title);
	bool getSuccess() {return this->success;};

public slots:
		void paintEvent( QPaintEvent * e );
		void slotTimeout();

private: 
	QTimer* timer;
    CProgressListener* listener;
    CProgressThread* thread;
	CProgressHandler* handler;

private: 

	bool firstPaint;
	bool success;
	bool finished;

};
#endif // BUI_PROGRESSDLG_H