#ifndef BUI_ALSMergeDialog_H
#define BUI_ALSMergeDialog_H

#include <qdialog.h>
#include <vector>
#include "ui_ALSMerge.h"
#include "VALS.h"


class bui_ALSMergeDialog : public QDialog , public Ui_ALSMergeDialog
{Q_OBJECT

  protected:

	  vector<CVALS *> AvailableALSList;

	  vector<CVALS *> SelectedALSList;

	  CFileName outputFile;

	  CVALS *als;

	  CALSDataSetWithTiles *alsTiles;

	  bool success;

  public:

	  bui_ALSMergeDialog();

	  ~bui_ALSMergeDialog();

	  bool getSuccess() const { return this->success; }

	  CVALS *getALS() const { return this->als; };

	  CALSDataSetWithTiles *getALSTiles() const { return this->alsTiles; };

	  bool compute();

  public slots:

	  void onALSListChanged();

	  void onBrowseSelected();

	  void OK();

	  void Cancel();

	  void Help();

  protected: 

	  void setupALSSelector();

	  void checkSetup();

	  void init();

	  void destroy();

};

#endif // BUI_ALSMergeDialog_H