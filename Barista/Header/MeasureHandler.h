#ifndef __CMeasureHandler__
#define __CMeasureHandler__
#include "DigitizingHandler.h"

#include "Filename.h"

class Bui_ProtocolViewer;

class CMeasureHandler : public CDigitizingHandler
{
public:
	CMeasureHandler(void);

	~CMeasureHandler(void);

	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setCrossCursor();};

	virtual void open();

	virtual void close();

	void viewProtocol();
	void initProtocol(const char *filename);
	void closeProtocol();
	void printResults();

	void elementDeleted(CLabel element="");
	void elementsChanged(CLabel element="");

protected:

	CXYZPoint* firstXYZ;
	CXYZPoint* secondXYZ;

	Bui_ProtocolViewer* protocolViewer;
	CFileName protocolFile;

	/// protocol file
	ofstream *protocolstream;
};


#endif