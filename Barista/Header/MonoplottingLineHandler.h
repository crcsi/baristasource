#ifndef __CMonoplottingLineHandler__
#define __CMonoplottingLineHandler__
#include "MonoplottingHandler.h"
#include "XYZPolyLine.h"

class CVBase;


class CMonoplottingLineHandler : public CMonoplottingHandler
{
public:
	CMonoplottingLineHandler(void);
	~CMonoplottingLineHandler(void);

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) { return false; };
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)  {return true;} ;

	virtual bool redraw(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setCrossCursor();};

	virtual void open();

	virtual void close();


	virtual void elementAdded(CLabel element="");
	virtual void elementDeleted(CLabel element="");
	virtual void elementsChanged(CLabel element="");
	virtual void elementSelected(bool selected,CLabel element="",int elementIndex=-1);
	
	virtual void drawingSettingsChanged(QColor& newColor,unsigned int newLineWidth);
protected:

	void addDrawingDataSets(CLabel& label);
	
	void addToDrawings(CLabel& dataSetName,CXYZPoint& p);
	void closeDrawings(CLabel& dataSetName);
	
	void drawMonoplottedLine(CBaristaView * v,CXYZPolyLine * pNewPolyline);

	void initPolyline();

	CXYZPolyLine line;
	CXYPointArray current2DPoints;
	CLabel currentLineLabel;
	CLabel currentPointLabel;
	CLabel changedLineLabel;

	CVBase* currentVBase;

};


#endif