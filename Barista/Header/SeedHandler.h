#ifndef __CSeedHandler__
#define __CSeedHandler__
#include "BaristaViewEventHandler.h"

class CSeedHandler : public CBaristaViewEventHandler
{
public:
	CSeedHandler(void);

	~CSeedHandler(void);

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);


	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) { return false; };
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)  {return true;} ;

	virtual bool redraw(CBaristaView *v);
	virtual void initialize(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setCrossCursor();};

	virtual void open();

	virtual void close();
};

#endif