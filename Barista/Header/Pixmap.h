#pragma once
#include "qpixmap.h"

class CPixmap : public QPixmap
{
public:
    CPixmap(void);
    ~CPixmap(void);

    void draw(QPainter* dc);
    void draw(QPainter* dc, int x, int y);

    int x;
    int y;
};
