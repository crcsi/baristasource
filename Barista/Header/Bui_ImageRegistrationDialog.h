#ifndef BUI_ImageRegistrationDialog_H
#define BUI_ImageRegistrationDialog_H

#include <qdialog.h>
#include "ui_ImageRegistration.h"
#include "m_FeatExtract.h"
#include "MatchRecordVector.h"

class CVImage;
class bui_ImageSensorSelector;
class CTrans2D;
class CRegistrationMatcher;

class bui_ImageRegistrationDialog : public QDialog , public Ui_ImageRegistrationDialog
{Q_OBJECT

  protected:

	  bui_ImageSensorSelector *referenceImageSelector;

	  CVImage *referenceImage;

	  bool hasRefImage;

	  bui_ImageSensorSelector *searchImageSelector;

	  CVImage *searchImage;

	  bool hasSearchImage;

	  bool finished;

	  bool keepPoints;

	  bool transferGCPs;

	  float minCorrCoeff;

	  double RefImageRes;

	  double SearchImageRes;

	  double maxparallax;
	  
	  float maxsigma0;

	  eRegistrationSensorModel model;

	  FeatureExtractionParameters featureParameters;

	  CRegistrationMatcher *matcher;

	  CTrans2D *trafo;

	  bool resampleImage;

	  CFileName resampleFileName;

  public:

	  bui_ImageRegistrationDialog (bool showResample, CVImage *search, CVImage *temp);

	  ~bui_ImageRegistrationDialog();

	  bool isFinished()const { return finished; }

	  CTrans2D *getTrafoClone() const;

	  bool compute();

  public slots:

	  void onRefImageChanged();

	  void onSearchImageChanged();

	  void onModelChanged();

	  void onCorrCoeffChanged();

	  void onRefResChanged();

	  void onSearchResChanged();

	  void onFeatureExtractionParsChanged();
	  
	  void onParallaxChanged();
	
	  void keepPointsChanged();

  	  void onTransferGCPsChanged();

	  void TransformImageChanged();

	  void maxSigma0Changed();
	  void FileNamePushed();

	  void OK();

	  void Cancel();

	  void Help();

  protected: 

	  void initDefaultFilename(const char *hugname);
	  void checkSetup();

	  void init();

	  void destroy();

};

#endif // BUI_ImageRegistrationDialog_H