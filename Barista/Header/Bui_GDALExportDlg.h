#ifndef BUI_GDALEXPORTDLG_H
#define BUI_GDALEXPORTDLG_H
#include <qdialog.h>
#include "ui_GDALExportDlg.h"
#include "ImageExporter.h"


class Bui_GDALExportDlg : public QDialog , public Ui_GDALExportDlgClass
{
	Q_OBJECT

public:
	Bui_GDALExportDlg(	CFileName exportFileName,
						CVBase* srcImage,
						bool isDEM,
						const char* title,
						bool MakeBandsAccessible = true, 
						bool geoCodedDefault = false, 
						bool makeFileAccessibleByDefaultFlag = false);

	Bui_GDALExportDlg(	CImageExporter& externalImageExporter,
						const char* title,
						bool MakeBandsAccessible = true, 
						bool geoCodedDefault = false, 
						bool makeFileAccessibleByDefaultFlag = false);


	~Bui_GDALExportDlg(void);

	void setSuccess(bool b) { this->success = b;};
	bool getSuccess() {return this->success;};

	CFileName getFilename() const { return this->currentImageExporter.getFilename(); };

	void setExportGeo(bool b) { return this->currentImageExporter.setExportGeo(b);}
	bool getExportGeo() const {return this->currentImageExporter.getExportGeo();};

	bool writeImage(const char* title = "Progress");
	bool writeSettings();

public slots:
	void pBSelectFileReleased();
	void pBExportReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBExportExternReleased();

	void rBJpegToggled(bool b);
	void rBLZWReleased();
	void rBNoneReleased();

	void rB8BitReleased();
	void rB16BitReleased();
	void rB32BitReleased();

	void rB1ChannelReleased();
	void rB3ChannelsReleased();
	void rB4ChannelsReleased();

	void rBRedReleased();
	void rBGreenReleased();
	void rBBlueReleased();
	void rBAlphaReleased();
	void rBIntensityReleased();
	void rBRGBReleased();
	void rBCIRReleased();

	void cBFileTypeCurrentIndexChanged(int index);
	void hSJpegSliderChanged(int index);

	void cBExportGeoReleased();
	void cBExportExternReleased();

	void cbTiledTiffReleased();
	void rBPixelReleased();
	void rBBandReleased();

private: 

	void initDlg(const char* title,bool MakeBandsAccessible,bool geoCodedDefault,bool makeFileAccessibleByDefaultFlag);

	void applySettings();

	void checkAvailabilityOfSettings();
	
	void availabilityCompressionSettings();
	void availabilityDepthSettings();
	void availabilityChannelSettings();
	void availabilityGeoSettings();
	void availabilityTIFFSettings();


private: 
	CImageExporter internalImageExporter;
	CImageExporter& currentImageExporter;


	bool success;
	bool exportToDirectory;
};





#endif // BUI_GDALEXPORTDLG_H