#ifndef __CViewingHandler__
#define __CViewingHandler__
#include "BaristaViewEventHandler.h"


class CViewingHandler :	public CBaristaViewEventHandler
{
public:
	CViewingHandler();
	~CViewingHandler();

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyPressEvent (QKeyEvent *e, CBaristaView *v)
	{
		return CBaristaViewEventHandler::keyPressEvent(e, v);
	};

	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
	{
		return CBaristaViewEventHandler::keyReleaseEvent(e, v);
	};

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v){ return false;};
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool redraw(CBaristaView *v);
	virtual void initialize(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setArrowCursor();};

	virtual void close();


};

#endif
