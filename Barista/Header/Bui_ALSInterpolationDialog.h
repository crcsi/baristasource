#ifndef BUI_ALSINTERPOLATIONDIALOG_H
#define BUI_ALSINTERPOLATIONDIALOG_H

#include <qdialog.h>
#include "ui_ALSinterpolatedialog.h"
#include "2DPoint.h"
#include "CharString.h"
#include "Filename.h"
#include "GenericPointCloud.h"
class CProgressListener;
class CProgressThread;
class CVImage;
class CVALS;
class CXYZPointArray;


class bui_ALSInterpolationDialog :	public QDialog, public Ui_InterpolationDialog
{Q_OBJECT

  protected:

	  bool DEMflag;

	  bool success;

	  CFileName filename;

	  CVALS *ALS;

	  CXYZPointArray *points;

	  C2DPoint pmin;

	  C2DPoint pmax;

	  C2DPoint resolution;

	  int pulse;

	  int neighbourhood;

	  int level;

	  double maxDist;

	  eInterpolationMethod method;

	  CCharString helpText;

  public:

	  bui_ALSInterpolationDialog(CVALS *als, const char *title, const char *helptext, bool DEM);
	  bui_ALSInterpolationDialog(CXYZPointArray *Points, const char *title, const char *helptext, bool DEM);

	  ~bui_ALSInterpolationDialog();

	  CFileName getFileName() const { return this->filename; }

	  bool getSuccess() const { return this->success; };

	  bool checkLimits();

	  C2DPoint getMin() const { return this->pmin; };

	  C2DPoint getMax() const { return this->pmax; };
	  
	  C2DPoint getRes() const { return this->resolution; };

	  double getMaxDist() const { return this->maxDist; };

	  int getPulse() const { return this->pulse; };

	  int getNeighbourhood() const { return this->neighbourhood; };

	  int getLevel() const { return this->level; };

	  eInterpolationMethod getMethod() const { return this->method; };


  public slots:
	
	  void SetToMax();

	  void LimitsMinSelected();
	  void LimitsMaxSelected();

	  void GridSelected();

	  void InterpolationMethodSelected();

	  void PulseSelected();

	  void NeighbourhoodSelected();

	  void LevelSelected();

	  void MaxDistChanged();

	  void OutSelected();

	  void OK();

	  void Cancel();
	  
	  void Help();

  protected:

  private: 

	  void init();

	  void destroy();

	  void setInterpolationTypeGUI();

};

#endif // BUI_ALSINTERPOLATIONDIALOG_H