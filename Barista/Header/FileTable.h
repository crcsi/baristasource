#ifndef __CFileTable__
#define __CFileTable__
#include "BaristaTable.h"
#include <QVBoxLayout>

class CFileTable :	public CBaristaTable
{
private:
    Q_OBJECT // needed to connect signals / slots
public:
	CFileTable(QWidget* parent);
	CFileTable(QWidget* parent, int top, int left, int width, int height, int ncols, int colwidth);
	virtual ~CFileTable(void);

	virtual void initTable();
	virtual void elementAdded(CLabel element){};
	virtual void elementDeleted(CLabel element){};

private:
	QVBoxLayout *Layout;


};
#endif