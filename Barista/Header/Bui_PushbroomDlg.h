#ifndef BUI_PUSHBROOMDLG_H
#define BUI_PUSHBROOMDLG_H

#include <QDialog>
#include "ui_PushbroomDlg.h"
#include "AdjustmentDlg.h"

#include "PushbroomScanner.h"
#include "OrbitPathModel.h"
#include "OrbitAttitudeModel.h"

class Bui_CameraMountingDlg;
class Bui_OrbitParameterDlg;
class Bui_CameraDlg;


class Bui_PushbroomDlg : public CAdjustmentDlg, public Ui_PushbroomDlgClass
{
	Q_OBJECT

public:
	Bui_PushbroomDlg(CPushBroomScanner* existingScanner,QWidget *parent = 0);
	~Bui_PushbroomDlg();

	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString() {return "PushBroom scanner";};

	virtual void elementAdded(CLabel element=""){};

	virtual void cleanUp();

	virtual void setDefaultTab(int defaultTab);

signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

public slots:
	void cameraMountingStatusChanged(bool b);
	void cameraStatusChanged(bool b);
	void pathStatusChanged(bool b);
	void attitudeStatusChanged(bool b);
	void tWCurrentChanged( int index );

private slots:
	virtual void elementsChangedSlot(CLabel element);

protected:
	CPushBroomScanner pbs;
	CPushBroomScanner *existingScanner;
	COrbitPathModel *existingPath;
	COrbitAttitudeModel *exstingAttitudes;
	CCameraMounting *existingCameraMounting;
	CCCDLine *existingCamera;

	COrbitPathModel path;
	COrbitAttitudeModel attitudes;
	CCameraMounting cameraMounting;
	CCCDLine camera;

	Bui_CameraMountingDlg* cameraMountingDlg;
	Bui_OrbitParameterDlg* orbitPathDlg;
	Bui_OrbitParameterDlg* orbitAttitudeDlg;
	Bui_CameraDlg* cameraDlg;
};

#endif // PUSHBROOMDLG_H
