#ifndef __CALSTable__
#define __CALSTable__

#include "BaristaTable.h"

class CVALS;

class CALSTable : public CBaristaTable
{
private:
    Q_OBJECT // needed to connect signals / slots

public:
	CALSTable(CVALS* ALS, QWidget* parent = 0);
	~CALSTable();

	virtual void elementAdded(CLabel element){};
	virtual void elementDeleted(CLabel element){};

	virtual void initTable();

 
    void setALS(CVALS* ALS);
    CVALS* getALS(){return this->als;	}
    
    void closeEvent ( QCloseEvent * e );

protected:
    CVALS* als;

};

#endif