#ifndef BUI_PROJECTSETTINGSDLG_H
#define BUI_PROJECTSETTINGSDLG_H

#include <QDialog>
#include "ui_ProjectSettingsDlg.h"
#include "BaristaHtmlHelp.h"

class Bui_ProjectSettingsDlg : public QDialog, public Ui_ProjectSettingsDlgClass
{
	Q_OBJECT

public:
	Bui_ProjectSettingsDlg(QWidget *parent = 0);
	~Bui_ProjectSettingsDlg();

	void initDlg();

	void setspinboxMeanHeight(double height);


public slots:
	
	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBMonoplottedLineColor();
	void setMonoplottedLineWidth(int);
	void pBBuildingLineColor();
	void setBuildingLineWidth(int);

private:
	QColor statMonoplottedLineColor;
	QColor oldMonoplottedLineColor;
	unsigned int oldMonoplottedLineWidth;

	QColor statBuildingLineColor;
	QColor oldBuildingLineColor;
	unsigned int oldBuildingLineWidth;

};

#endif // BUI_PROJECTSETTINGSDLG_H
