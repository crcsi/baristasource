#ifdef WIN32
#ifndef __CMonoplottingBuildingHandler__
#define __CMonoplottingBuildingHandler__
#include "MonoplottingHandler.h"
//#include "Building.h"
#include "XYZPolyLine.h"
#include "XYZPoint.h"
#include "XYPoint.h"

class CBuilding;


class CMonoplottingBuildingHandler : public CMonoplottingHandler
{
public:
	CMonoplottingBuildingHandler(void);

	~CMonoplottingBuildingHandler(void);

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool keyPressEvent (QKeyEvent *e, CBaristaView *v);
	virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) { return false; };
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v) {return true;} ;

	virtual bool redraw(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v)
	{ 
		if ( this->courtYardMode ) v->setCrossCCursor();
		else if ( this->superStructureMode ) v->setCrossSCursor();
		else v->setCrossCursor();
	};


	virtual void open();

	virtual void close();
	
	virtual void elementAdded(CLabel element="");
	virtual void elementDeleted(CLabel element="");
	virtual void elementsChanged(CLabel element="");
	virtual void elementSelected(bool selected,CLabel element="",int elementIndex=-1);

	virtual void drawingSettingsChanged(QColor& newColor,unsigned int newLineWidth);

protected:

	void addDrawingDataSets();
	void addFirstCourtYardDrawing(CXYZPoint& pNewPoint);
	void addFirstSuperStructureDrawing(CXYZPoint& pNewPoint);
	bool createBuildingDataSet(CDrawingDataSet& dataSet,CBaristaView* v,CBuilding* pBuilding);

	void setBuildingLabels(CBuilding* pBuilding,CLabel label);

	void setCourtYardMode();
	void setSuperStructureMode();
	void setBaseMode();
	// drawing
	void addFirstRoofPoint();
	void addNextRoofPoint();

	bool superStructureIsInterior()const;

	// measuring
	bool firstPointClicked(CXYPoint& xy,CBaristaView* v);
	bool secondPointClicked(CXYPoint& xy);
	bool nextPointClicked(CXYPoint& xy);

	// select building
	bool selectBuilding(CXYPoint& xy,CBaristaView* v);

	bool intersectWithBuilding(const CXYPoint& xy,C3DPoint& intersection,const int& buildingIndex);

	void removePoint(CLabel& label);
	void initBuilding();

	CLabel newBuildingLabel;
	CLabel currentFloorPointLabel;
	CLabel currentRoofPointLabel;

	CVImage *currentVImage;

	CXYZPolyLine roof;
	CXYZPolyLine floor;

	CXYPointArray current2DRoof;

	bool courtYardMode;
	bool superStructureMode;

	CXYPoint xyfloor;
	
	CLabel TEMP_BUILDING_LABEL;
};

#endif

#endif // WIN32
