#ifndef __CDEMTable__
#define __CDEMTable__

#include "BaristaTable.h"

class CVDEM;

class CDEMTable : public CBaristaTable
{
    Q_OBJECT // needed to connect signals / slots
public:
	CDEMTable(CVDEM* dem,QWidget* parent = 0);
	~CDEMTable(void);

	virtual void initTable();
	virtual void elementAdded(CLabel element){};
	virtual void elementDeleted(CLabel element){};
	virtual void elementsChanged(CLabel element);

    void setDEM(CVDEM* dem);
    CVDEM* getDEM()	{return this->vdem;}
    

    void closeEvent ( QCloseEvent * e );
protected:
    CVDEM* vdem;

	int decimals;
};

#endif