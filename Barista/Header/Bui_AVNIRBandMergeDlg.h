#ifndef BUI_AVNIRBANDMERGEDLG_H
#define BUI_AVNIRBANDMERGEDLG_H

#include <QDialog>
#include "ui_AVNIRBandMergeDlg.h"
#include "BandMerger.h"
#include <QItemDelegate>


class Bui_GDALExportDlg;
class CVImage;
class CGDALFile;
class CGroupDataBandMerger;

class Bui_AVNIRBandMergeDlg : public QDialog, public Ui_AVNIRBandMergeDlg
{
	Q_OBJECT

public:
	Bui_AVNIRBandMergeDlg(QWidget *parent = 0);
	~Bui_AVNIRBandMergeDlg();


	bool getSuccess() const { return this->success;};
public slots:

	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBFileReleased();
	void tWFilesItemSelectionChanged();
	void tWFilesItemChanged(QTreeWidgetItem* item,int col);
	void tWFilesItemClicked(QTreeWidgetItem * item,int column); 

protected:

	void checkSetup();
	void loadImages();

	bool checkBandSelection(QTreeWidgetItem* parent);
	void changeBandOrder(CGroupDataBandMerger& d);

	void selectImageGroup(unsigned int group);
	bool checkCameraName(CCharString& newName,const CCharString& oldName);

	bool success;
	int selectedCamera;

	vector<CGroupDataBandMerger> data;
};

// ###############################################################################################
// ###############################################################################################

class CGroupDataBandMerger
{
public:
	CGroupDataBandMerger(void) :  readyToImport(false),
						import(true),
						method(CBandMerger::eBiLinear),
						exportDialog(0),
						mergedImage(0)
						{};

	~CGroupDataBandMerger();


public:
	bool readyToImport;
	bool import;
	CBandMerger::InterpolationMethod method;
	CFileName filename;
	Bui_GDALExportDlg *exportDialog;
	CVImage* mergedImage;
	vector<CVImage*> inputImages;
	CCharString cameraName;
	QTreeWidgetItem* item;
};

// ###############################################################################################
// ###############################################################################################

class CCustomDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	
	CCustomDelegate(QObject* parent,const QStringList& items,const QList<QColor>& colors);
	
	QWidget *createEditor(QWidget *parent,const QStyleOptionViewItem & option, const QModelIndex &index) const;
	void setEditorData(QWidget * editor, const QModelIndex & index )const;
	void setModelData ( QWidget * editor, QAbstractItemModel * model, const QModelIndex & index ) const;

public slots:
	void CBBoxCurrentIndexChanged(int index);

protected:
	QStringList items;
	QList<QColor> colors;
};


#endif // BUI_AVNIRBANDMERGEDLG_H
