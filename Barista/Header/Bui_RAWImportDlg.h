#ifndef BUI_RAWIMPORTDLG_H
#define BUI_RAWIMPORTDLG_H

#include <QDialog>
#include "ui_RAWImportDlg.h"
#include "2DPoint.h"
#include "Filename.h"
#include "VDEM.h"
#include "ImageImporter.h"

class Bui_RAWImportDlg : public QDialog ,public Ui_RAWImportDlgClass
{
	Q_OBJECT

public:
	Bui_RAWImportDlg(bool isDEM = false);
	~Bui_RAWImportDlg();

	void setSuccess(bool b) { this->success = b;};
	bool getSuccess() {return this->success;};

public slots:
	
	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();

	void pBFileSelectReleased();

	// lineEdits
	void leLLXChanged(const QString & text);
	void leLLYChanged(const QString & text);
	void leGridXChanged(const QString & text);
	void leGridYChanged(const QString & text);
	void NoDataChanged(const QString & text);
	void leZ0Changed(const QString & text);
	void leScaleChanged(const QString & text);
	void leSigmaZChanged(const QString & text);
	void leHeaderSizeChanged(const QString & text);

	// spinboxes
	void spinBoxChannelsChanged( int bands);
	void spinBoxColsChanged(int newcols);
	void spinBoxRowsChanged(int newrows);


	// radio buttons
	void rb8bitReleased();
	void rb16bitReleased();
	void rb32bitReleased();
	void rb64bitReleased();

	void rbSignedReleased();
	void rbUnsignedReleased();
	void rbBigEndianReleased();
	void rbLittleEndianReleased();

	void rbTopReleased();
	void rbBottomReleased();

	void rbASCIIReleased();
	void rbBinaryReleased();

	void rbInterleavedReleased();
	void rbNonInterleavedReleased();


	// check Boxes
	void cbSquareReleased();

	void cb1arcReleased();
	void cb3arcReleased();
	void cb30arcReleased();

	void cbGeoreferencingReleased();

private:

	void init();
	void reset();
	bool checkSize();
	void checkSetup();

	void guessDimensions();
	bool applyGeoReferencing();

	bool isASCII;


	CImageImportData data;

	int decimals;
	int FileSize;
	int SizefromSettings;

	bool firstpaint;
	bool success;
	bool isDEM;
	bool sizeMustFit;

	QString FileFilter;

};

#endif // BUI_RAWIMPORTDLG_H
