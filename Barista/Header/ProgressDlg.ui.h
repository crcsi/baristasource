/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

/**
 * @class CProgressDlg
 *
 * Shows the progress of the creation of a CHugeImage
 * The image is loaded in a thread
 *
 */
#include "HugeImage.h"
#include "HugeImageLoadThread.h"
#include <QTimer>

/**
 * Called by the constructor
 *
 */
void CProgressDlg::init()
{
    this->firstPaint = true;
    this->image = NULL;
    this->imageFile = NULL;
    this->thread = NULL;
    this->timer = new QTimer(this);
    this->progressBar->setTotalSteps(100);
    this->progressBar->setProgress(0);
    this->listener = new CImagingProgressListener();
    this->finished = false;

    connect( this->timer, SIGNAL(timeout()), this, SLOT(slotTimeout()) );
}

/** 
 * Called by destructor
 *
 */
void CProgressDlg::destroy()
{
    if (this->thread)
        delete this->thread;
    
    this->thread = NULL;

    this->image->removeProgressListener();
    delete this->listener;
}

void CProgressDlg::setImageFile( CImageFile * imageFile )
{
    this->imageFile = imageFile;
}


void CProgressDlg::setImage( CHugeImage * image )
{
    this->image = image;
}

void CProgressDlg::paintEvent ( QPaintEvent * e) 
{
    if (this->firstPaint)
    {
        this->firstPaint = false;

        if ( (this->image != NULL) && (this->imageFile != NULL) )
        {
            this->image->setProgressListener(this->listener);
            this->thread = new CHugeImageLoadThread(this->image, this->imageFile);
            thread->start();
            this->timer->start(500);
        }
    }
    
    QDialog::paintEvent (e);
}


void CProgressDlg::slotTimeout()
{
    if (this->finished)
    {
        this->timer->stop();
        this->done(0);
        return;
    }

    double val = this->listener->getProgress();
    int progress = (int)(val + 0.5);
    this->progressBar->setProgress(progress);

    if (this->thread != NULL && this->thread->finished())
    {
        this->finished = true;
    }
}
