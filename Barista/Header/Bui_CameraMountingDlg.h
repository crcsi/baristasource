#ifndef __Bui_CameraMountingDlg__
#define __Bui_CameraMountingDlg__


#include "ui_CameraMountingDlg.h"
#include "CameraMounting.h"
#include "OmegaPhiKappaRotation.h"

#include "AdjustmentDlg.h"

class Bui_CameraMountingDlg : public CAdjustmentDlg ,public Ui_CameraMountingDlgClass
{
	Q_OBJECT
public:
	Bui_CameraMountingDlg(CCameraMounting* existingCameraMounting = NULL,bool allowEdit = false,QWidget *parent=0);
	~Bui_CameraMountingDlg(void);

	const CCameraMounting& getCameraMounting() { return this->cameraMounting;};

	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString() {return "CameraMounting";};

	virtual void elementAdded(CLabel element="") {};
	virtual void cleanUp();

signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

public slots:


	void leTextChanged(const QString & text);
	void rbRollPitchYawReleased();
	void rbOmegaPhiKappaReleased();	

	void leXShiftEditingFinished();
	void leYShiftEditingFinished();
	void leZShiftEditingFinished();
	void leAngle1EditingFinished();
	void leAngle2EditingFinished();
	void leAngle3EditingFinished();
	void leNameEditingFinished();

private slots:
	virtual void elementsChangedSlot(CLabel element);

private:
	
	void applySettings();
	void checkAvailabilityOfSettings();

	
	CCameraMounting* existingCameraMounting;

	CCameraMounting cameraMounting;
	COmegaPhiKappaRotation omegaPhiKappa;
};
#endif
