#ifndef __CMonoplottingHeightHandler__
#define __CMonoplottingHeightHandler__
#include "MonoplottingHandler.h"

class CMonoplottingHeightHandler : public CMonoplottingHandler
{
public:
	CMonoplottingHeightHandler(void);

	~CMonoplottingHeightHandler(void);

	virtual bool handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v);


	virtual bool handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v);
	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	virtual bool handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v) { return false; };
	virtual bool handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)  {return true;} ;

	virtual bool redraw(CBaristaView *v);

	virtual void setCurrentCursor(CBaristaView *v) { v->setCrossCursor();};

	virtual void open();

	virtual void close();

	//addition >>
	void initPolyline();
	void drawHeightLine(CBaristaView * v,CXYZPolyLine * pNewPolyline);
protected:
	
	CLabel currentLineLabel;	
	//<< addition
};

#endif