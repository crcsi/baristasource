#ifndef __CDEMView__
#define __CDEMView__

#include "BaristaView.h"
#include "VDEM.h"


class CDEMView : public CBaristaView
{

Q_OBJECT

protected:

	CVDEM* vdem;
	QStringList avgText;
	QStringList rmsText;
	QStringList minText;
	QStringList maxText;
	QStringList devText;

	CObsPointArray startRow;
	CObsPointArray endRow;
	CObsPointArray startCol;
	CObsPointArray endCol;
	CObsPointArray centre;

public:
	CDEMView(CVDEM* vdem, QWidget * parent = 0);
	~CDEMView(void);

	virtual CVDEM* getVDEM() const;

	virtual CHugeImage *getHugeImage() const;

	virtual void createPopUpMenu();

	virtual const CSensorModel* getCurrentSensorModel();

    virtual void mouseMoveEvent ( QMouseEvent * e );

	virtual void keyReleaseEvent ( QKeyEvent * e );

	virtual void drawContents ( QPainter &p);

    virtual void closeEvent ( QCloseEvent * e );

	virtual void elementsChanged(CLabel element="");

	virtual void elementDeleted(CLabel element="");

    virtual bool isA(const char* className)
    {
        if (::strcmp("CDEMView", className) == 0)
            return true;
        else
            return false;
    };

	virtual void getPixelInfoHugeImage(QString& pixelInfo,
										 QString& geoInfo,
										 QString& colorInfo,
										 QString& zoomInfo,
										 QString& residualInfo,
										 QString& brightnessInfo,
										 CHugeImage* himage);

	virtual const CTFW* getTFW() const;

	virtual CVBase* getVBase() const { return this->vdem; };

	bool createDEMStatistics(const CObsPointArray& demStatistics,int nRows,int nCols);
	bool deleteDEMStatistics();

public slots:
	virtual void CropROI();

	virtual void ResetRoi();


};

#endif
