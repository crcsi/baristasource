#ifdef WIN32
#ifndef BUI_BUILDINGDIALOG_H
#define BUI_BUILDINGDIALOG_H

#include <QDialog>
#include "ui_BuildingDetector.h"
#include <QTimer>
#include "2DPoint.h"
#include "CharString.h"

class CProgressListener;
class CProgressThread;
class bui_ImageSensorSelector;

class CDEM;
class CVDEM;
class CVImage;


class CBuildingDetector;
class bui_OutputModeHandler;



class bui_BuildingDialog :	public QDialog, public Ui_BuildingDialog
{Q_OBJECT

  protected:

	  CVDEM *pDSMLast;

	  CVDEM *pDSMFirst;

	  CVDEM *pDTM;

	  CVImage *pNDVI;

	  CVImage *pSigmaNDVI;

	  CVImage *pBuildingLabels;

	  bui_OutputModeHandler *pOutPutHandler;

	  float MinBuildingHeight;

	  float MinBuildingHeightDTM;

	  float MaxBuildingSize;
	  float MinBuildingSize;
	  float MinBuildingArea;
	  float NDVIThresholdBuilding;
	  float NDVIThresholdBuildingDTM;
	  bool  useFirstpulseForRoughness;
	  bool  useRoughnessStrengthBuilding;
	  bool  useRoughnessIsotropyBuilding;
	  float percentageTreesBuilding;
	  float percentagePoints0Building;
	  float percentageHomog0Building;
	  float Qmin;
	  float structureForOpen;
	  float structureForClose;
	  int   kernelsizeRoughness;
	  float thinning;
	  float FirstLastThresholdValue;
	  bool  assignInverseFLToTheta;
	  float PointPercentageThresholdValue;

	  int maxPostClassIterations;

	  float buildingPercentage;

	  bool useWaterModel;

	  bool useRank;

	  float rankPercentage;

	  C2DPoint llPoint;
	  C2DPoint urPoint;
	  C2DPoint resolution;

	  CCharString currentDir;
	  CCharString outputDir;

	  bool extentsInit;

	  bool computationOK;


	  void setupExtents(CDEM *dem);
	  void setupExtents(CVImage *img);

	  bool writeDSFile(const CCharString &filename) const;

	  int getStruct(float size);

  public:

	  bui_BuildingDialog(CVDEM *pDSM);

	  ~bui_BuildingDialog();


	  bool getComputationOK() const { return computationOK; }

	  void setCurrentDir(const CCharString &cdir);

	  void initAreafromROI();

	  void setROI(const C2DPoint &lr, const C2DPoint &ul);

	  void checkSetup();

	  bool checkLimits();

	  int progressvalue;

	  int getSensorOption() const;

	  bool extract();

  public slots:
	
	  void DSMLastSelected();

	  void DSMFirstSelected();

	  void DTMSelected();

	  void NDVISelected();

	  void NDVISigmaSelected();

	  void ExistingDatabaseSelected();

	  void maxExtentsSelected();

	  void LLChanged();
	  
	  void RUChanged();

	  void resolutionChanged();

	  void minHeightChanged();
	  void minHeightDTMChanged();

	  void maxExtentChanged();
	  void minExtentChanged();
	  void minAreaChanged();
	  void NDVIThresholdChanged();
	  void NDVIThresholdDTMChanged();
	  void useRoughnessStrengthChanged();
	  void useRoughnessIsoChanged();
	  void roughnessModeChanged();
	  void roughnessParametersChanged();
	  void structureForOpenChanged();
	  void structureForCloseChanged();
	  void ThinningChanged();
	  void BuildingPercentageChanged();
	  void MaxIterationChanged();
	  void UseWaterModelChanged();

	  void FLThetaModeChanged();

	  void FirstLastThresholdChanged();

	  void PointPercentageChanged();

	  void DTMModeChanged();

	  void DTMRankChanged();

	  void DTMParametersChanged();

	  void OutputLevelChanged();

	  void explorePathSelected();

	  void OK();

	  void Cancel();
	  
	  void Help();

  protected:


	  CBuildingDetector *detector;

	  static const char *noDSMLastTxt;
	  static const char *noDSMFirstTxt;
	  static const char *noDTMTxt;
	  static const char *noNDVITxt;
	  static const char *noSigmaNDVITxt;
	  static const char *ExistingDatabaseTxt;

	  void setupDEMSelector(QComboBox *box, CVDEM *selectedDEM, const char *descriptor);

	  void setupImageSelector(QComboBox *box, CVImage *selectedImage, const char *descriptor);

	  void setCurrentExtentsAndResolution();

	  void writeParameters(const CCharString &filename) const;

	  void initParameters(const CCharString &filename);

	  static void writeDSRecord (ofstream &DSFile, const char *key, 
		                         float Pl, float Pu, float xl, float xu, int DSWidth);


	  void setDTMmorphMode();

  private: 

	  void init();

};

#endif // BUI_BUILDINGDIALOG_H
#endif // WIN32
