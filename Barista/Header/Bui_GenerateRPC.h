#ifndef BUI_GENERATERPC_H
#define BUI_GENERATERPC_H

#include <QDialog>
#include "ui_Bui_GenerateRPC.h"

class Bui_GenerateRPC : public QDialog, public Ui_GenerateRPCDialog
{
	Q_OBJECT
  
  protected:

	  double zMin, zMax;
	  bool success;

  public:
	
	  Bui_GenerateRPC(QWidget *parent = 0);
	
	  ~Bui_GenerateRPC();

	
	  bool getSuccess() const { return this->success; };
	
	  double getZMin() const { return this->zMin; };
	  double getZMax() const { return this->zMax; };

	  void setDefaults();

  public slots:

	  void pBCancelReleased();

	  void pBOkReleased();

	  void pBHelpReleased();

	  void pBZMinChanged();
	
	  void pBZMaxChanged();


  protected:

	  void applySettings();
};

#endif // BUI_GENERATERPC_H
