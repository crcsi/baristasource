#ifndef BUI_IMGSHOWDLG_H
#define BUI_IMGSHOWDLG_H

#include <QDialog>
#include "ui_Bui_ImgShowDlg.h"
#include "BaristaProject.h"

using namespace Ui;

class Bui_ImgShowDlg : public QDialog, public Bui_ImgShowDlgClass
{
	Q_OBJECT

public:
	Bui_ImgShowDlg(QWidget *parent = 0);
	~Bui_ImgShowDlg();

	//old values
	shwLabel flag_label; //0 for off, 1 for on
	shwPoint flag_point; // 0 (cross), 1 (dot), 2 (none)
	shwResidual flag_res; // 0-4
	QColor color_label, color_point, color_res;
	int dot_width, line_width;
	bool showProjPoint;
	bool showScale;
	double scaleValue;
	double gsdValue;

	// default values
	shwLabel flag_label_def; //0 for off, 1 for on
	shwPoint flag_point_def; // 0 (cross), 1 (dot), 2 (none)
	shwResidual flag_res_def; // 0-4
	QColor color_label_def, color_point_def, color_res_def;
	int dot_width_def, line_width_def;
	bool showProjPoint_def;
	bool showScale_def;
	double scaleValue_def;
	double gsdValue_def;

public slots:
	// form
	void defineDefaultData();
	void pBCancelReleased();
	void initImgShowDlg(bool defaultSetup);
	void pBApplyReleased();
	void pBOKReleased();
	void saveOldData();
	void retrieveOldData();
	void getNewData();
	void getDefaultData();
	void rbProjectedPointOnReleased();
	void rbProjectedPointOffReleased();
	void rBCustomReleased();
	void rBDefaultReleased();
	void pBCloseReleased();

	//Label
	void pBLabelColorReleased();
	void rBLablelOffReleased();
	void rBLablelOnReleased();

	//Point
	void rBCrossReleased();
	void rBDotReleased();
	void rBNoPntReleased();
	void pBPointColorReleased();

	//Residual
	void rBImagexyReleased();
	void rBImageDiffReleased();
	void rBObjXYReleased();
	void rBObjHeightReleased();
	void rBNoPlotReleased();
	void pBResColorReleased();
	void cBScaleReleased();
};

#endif // BUI_IMGSHOWDLG_H
