#ifndef ___CHtmlHelp_H___
#define ___CHtmlHelp_H___

#include "CharString.h"
#include "Filename.h"


class CHtmlHelp
{
public:
	CHtmlHelp(void);
	~CHtmlHelp(void);

	static CFileName fileNameSearch;
	static CFileName fileNameContens;
	static void callHelp(const char* page);
	static void callSearch();
};

#endif
