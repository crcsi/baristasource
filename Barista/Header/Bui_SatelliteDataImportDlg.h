#ifndef BUI_SATELLITEDATAIMPORTDLG_H
#define BUI_SATELLITEDATAIMPORTDLG_H
#include <qdialog.h>
#include "ui_SatelliteDataImportDlg.h"
//#include "XYZOrbitPointArray.h"
#include "doubles.h"
#include "SatelliteDataImporter.h"

class CFileName;
class COrbitObservations;
class CSensorModel;
class CTFW;
class CVImage;

class Bui_SatelliteDataImportDlg :	public QDialog, public Ui_SatelliteDataImportDlgClass
{Q_OBJECT
public:
	Bui_SatelliteDataImportDlg(CFileName& filename, const char* helpfile);
	~Bui_SatelliteDataImportDlg(void);

	bool getSuccess() {return this->success;};

	CCamera* getCamera();
	CCameraMounting* getCameraMounting();
	CSensorModel* getOrbitPathModel(); 
	CSensorModel* getOrbitAttitudeModel(); 

	double getSatelliteHeight()const;
	double getGroundHeight()const;
	double getGroundTemperature()const;

	double getFirstLineTime()const;
	double getTimePerLine()const;

	int getActiveAdditionalParameter()const;
	int getActiveInteriorParameter() const;
	int getActiveMountingRotationParameter()const;
	int getActiveMountingShiftParameter()const;
	
	bool importMetadata(CFileName filename,SatelliteType sat);

	void cancelImport();
	void finishImport();

/*	bool hasTFW() const;
		
	CTFW *getTFW();
*/
public slots:
	void pBFileReleased();
	void pBImportReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void rB_Spot5Released();
	void rB_QuickbirdReleased();
	void rB_AlosReleased();
	void rB_Formosat2Released();
	void rB_TheosReleased();
	void rB_GeoEye1Released();
	void pBAccuracyReleased();
	void currentIndexChangedPath( int index ); 
	void currentIndexChangedAttitudes( int index ); 
	void currentIndexChangedMounting( int index ); 
	void currentIndexChangedCamera( int index ); 

protected slots:
	void leSatelliteHeightTextChanged(const QString & text);
	void leGroundHeightTextChanged(const QString & text);
	void leGroundTemperatureTextChanged(const QString & text);
	void sBAttitudesValueChanged (double value );
	void sBPathValueChanged (double value );

private:

	void setSuccess(bool b) {this->success = b;};
	void applySettings();
	void checkAvailabilityOfSettings();

	SatelliteType satellite;
	vector<CSatelliteDataImporter*> dataImporter;

	CFileName& filename;

	CCharString filterSpot5;
	CCharString filterQuickbird;
	CCharString filterAlos;
	CCharString filterFormosat2;
	CCharString filterTheos;
	CCharString filterGeoEye1;


	CCharString helpFile;

	bool success;
	bool importError;
	bool adjustPath;
	bool adjustAttitudes;
	bool accuracyChanged;

};

#endif