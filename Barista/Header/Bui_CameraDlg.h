#ifndef __Bui_CameraDlg__
#define __Bui_CameraDlg__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "AdjustmentDlg.h"
#include "ui_CameraDlg.h"
#include <QStandardItemModel>


class CCamera;
class CPushBroomScanner;
class CDistortion;
class CDistortionDataModel;


class Bui_CameraDlg : public CAdjustmentDlg ,public Ui_CameraDlgClass
{
	Q_OBJECT

public:
	Bui_CameraDlg(CCamera* existingCamera,CPushBroomScanner* scanner=NULL,QWidget *parent = 0);
	~Bui_CameraDlg();

	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString() {return "Camera parameter";};

	virtual void elementAdded(CLabel element="") {};

	virtual void cleanUp();

signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);


public slots:
	void leTextChanged(const QString & text);
	void leNameEditingFinished();
	void cBAlosParameterStateChanged(int state);
	void pBResetReleased();
	void pBResetAllReleased();
	void pBApplyReleased();

private slots:
	virtual void elementsChangedSlot(CLabel element);

protected:

	void applySettings();
	void checkAvailabilityOfSettings();

	void initActiveParameter();
	void saveActiveParameter(CPushBroomScanner* sanner);

	CCamera* existingCamera;
	CCamera* localCopyOfCamera;
	CPushBroomScanner* existingScanner;

	CDistortion* distortion;
	CDistortionDataModel * dataModel;

	bool useAlosParameter;
	vector<bool> activeParameter;
};


// ###############################################################################################

class CDistortionDataModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	CDistortionDataModel(CDistortion* distortion,vector<bool> &activeParameter,QObject* parent =0);
	int rowCount(const QModelIndex& parent) const;
	int columnCount(const QModelIndex& parent) const;
	QVariant headerData(int section,Qt::Orientation orientation, int role) const;
	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	bool setData(const QModelIndex &index, const QVariant &value,int role = Qt::EditRole);
	void update();

public slots:
	void itemClicked( const QModelIndex& index);


private:
	vector<bool> &activeParameter;
	CDistortion* distortion;
	
};

#endif // CAMERADLG_H
