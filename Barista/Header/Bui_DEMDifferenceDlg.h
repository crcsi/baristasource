#ifndef BUI_DEMDIFFERENCEDLG_H
#define BUI_DEMDIFFERENCEDLG_H

#include <QDialog>
#include "ui_DEMDifferenceDlg.h"

#include "Filename.h"
#include <vector>
#include "GDALFile.h"
#include "TFW.h"


class CVDEM;
class Bui_GDALExportDlg;

class Bui_DEMDifferenceDlg : public QDialog, public Ui_DEMDifferenceDlgClass
{
	Q_OBJECT

public slots:
	
	void pBOkReleased();
	void pBCancelReleased();
	void pBHelpReleased();
	void pBSelectOutputReleased();
	void MasterSelectionChanged(int i);
	void DiffSelectionChanged(int i);
	void cBCreateColor(int state);
	void textChanged( const QString & text);
	void lEZero();


public:
	Bui_DEMDifferenceDlg(QWidget* parent = NULL);
	~Bui_DEMDifferenceDlg();

	void setSuccess(bool b) { this->success = b;};
	bool getSuccess() {return this->success;};
	bool createDifferenceDEM();
	bool difference();
	void checkSetup();
	void fillComboMaster();
	void fillComboDiff();
	bool checkReferenceSystems();
	bool getTFWInfo();
	void getMasterSelection();
	void getCompareSelection();

private:

	CFileName outname;

	int masterIndex;
	int compareIndex;
	bool success;

	double gridx;
	double gridy;
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	double width;
	double height;
	int cols;
	int rows;

	bool useColor;
	float zero;

protected:
	Bui_GDALExportDlg* dlg;
	CVDEM* newdem;
};

#endif // BUI_DEMDIFFERENCEDLG_H
