#ifndef BUI_GDALIMPORTDLG_H
#define BUI_GDALIMPORTDLG_H
#include <qdialog.h>
#include "ui_GDALImportDlg.h"
#include "Filename.h"
#include <vector>
#include "SatelliteDataReaderFactory.h"
#include "ReferenceSystem.h"
#include "BaristaProject.h"
#include "ImageImporter.h"

class CBaristaTable;
class CFileTable;
class CPredefinedBandOrders;
class CVImage;
class CImageFile;


typedef struct {
	QStringList extRPC;
	QStringList extRPCDescription;
	QStringList extTFW;
	QStringList extTFWDescription;
	QStringList pushBroomFiles;
	QStringList pushBroomDescription;
	vector<SatelliteType> pushBroomType; 

	int bandorder;
	int selectedExtRPC;
	int selectedExtTFW;
	int selectedPushBroom;
	bool success;

	CImageImportData d;
}ImportFile;


class Bui_GDALImportDlg : public QDialog ,public Ui_GDALImportDlgClass
{
    Q_OBJECT
public:
	Bui_GDALImportDlg(bool isDEM = false);
	~Bui_GDALImportDlg(void);


	bool getSuccess() {return this->success;};


public slots:
	void pbFilesReleased();
	void pbImportReleased();
	void pbCancelReleased();
	void pbHelpReleased();
	void cbHugeFileReleased();
	void cbRPCReleased();
	void cbTFWReleased();
	void cBPushBroomReleased();
	void rbImageFileReleased();
	void rbExternalFileReleased();
	void comboRPCCurrentIndexChanged(int index);
	void comboTFWCurrentIndexChanged(int index);
	void comboBandOrderCurrentIndexChanged(int index);
	void comboPushBroomCurrentIndexChanged(int index);

	void lwRedChannelCurrentRowChanged ( int index); 
	void lwGreenChannelCurrentRowChanged ( int index); 
	void lwBlueChannelCurrentRowChanged ( int index); 
	void lwAlphaChannelCurrentRowChanged ( int index); 

	void twFilesItemSelectionChanged();

	// lineEdits
	void NoDataChanged(const QString & text);
	void leZ0Changed(const QString & text);
	void leScaleChanged(const QString & text);
	void leSigmaZChanged(const QString & text);

	int exec();
	
private:
	

	void applySettings();
	void clearDialog();

	void getFileInformation(ImportFile& element);

	void clearImportFiles();

	void determineRPCFiles(ImportFile& element);
	void determineTFWFiles(ImportFile& element);
	void determinePushBroomFiles(ImportFile& element);

	bool loadSatelliteData(CVImage* image, CFileName& filenameVImage,CFileName& filenameMetadata,SatelliteType type);
	void checkAvailabilityOfSettings();

	vector<ImportFile> importFiles;
	vector<int> selectedFiles;
	
	bool success;

	CFileTable* tw_Files;

	CReferenceSystem globalRefSys;
	bool useGlobalRefSys;
	bool wasAskeduseGlobalRefSys;

	bool isDEM;
};










class  CPredefinedBandOrders
{
public:

	CPredefinedBandOrders(CCharString name, int bands,int channel1, int channel2, int channel3, int channel4 = 0);
	CPredefinedBandOrders(const CPredefinedBandOrders& combination);

	CPredefinedBandOrders&	operator=(const CPredefinedBandOrders& combination);
	

	CCharString name;
	
	int bands;
	
	int channel1;
	int channel2;
	int channel3;
	int channel4;

};

#endif