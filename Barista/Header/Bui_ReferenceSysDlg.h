#ifndef BUI_REFERENCESYSDLG_H
#define BUI_REFERENCESYSDLG_H
#include <qdialog.h>
#include "ui_ReferenceSysDlg.h"

class CSpheroid;
class CReferenceSystem;
class CFileName;

class Bui_ReferenceSysDlg :	public QDialog, public Ui_ReferenceSysDlgClass
{
Q_OBJECT
public:
	Bui_ReferenceSysDlg(CReferenceSystem* refSystem, CFileName* filename, const char *title, 
		                const char* helpfile, bool getFileName=false, const char* filter= NULL,bool setRefValues= false,
						bool fileHasToExist = true);
	~Bui_ReferenceSysDlg(void);

	void setGridSettingsEnabled(bool b);
	void setUTMSettingsEnabled(bool b);
	void setTMSettingsEnabled(bool b);

	bool getSuccess() {return this->success;};
	void setSuccess(bool b) {this->success = b;};

	void initDlg();

public slots:
	void rbGeocentricReleased();
	void rbGeographicReleased();
	void rbGridReleased();
	void rbOtherCSReleased();
	void rbUTMReleased();
	void rbTMReleased();
	void rbWGS84Released();
	void rbOtherRefSysReleased();
	void checkValues();
	void checkValues(const QString& text);

	void pBApplySettings();
	void pBCancelReleased();
	void pBFileSelectReleased();

	void callHelp();

	void zoneEdited(const QString & text);
	void zonehighlighted(int index); 

	int exec();
private:
	CReferenceSystem* refSys;
	CFileName* filename;
	const char* filter;
	const char* helpFile;
	bool success;
	bool getFileName;
	bool setRefValues;

	bool FileHasToExist;

	bool firstpaint;

	int zonetyped;
};

#endif