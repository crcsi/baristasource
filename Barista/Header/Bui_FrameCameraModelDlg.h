#ifndef __Bui_FrameCameraModelDlg__
#define __Bui_FrameCameraModelDlg__


#include <qdialog.h>
#include "ui_FrameCameraModelDlg.h"
#include "FrameCameraModel.h"
#include "OmegaPhiKappaRotation.h"
#include "AdjustmentDlg.h"


class Bui_CameraMountingDlg;
class Bui_FrameCameraDlg;


class Bui_FrameCameraModelDlg :	public CAdjustmentDlg, public Ui_FrameCameraModelDlgClass
{
	Q_OBJECT

public:
	Bui_FrameCameraModelDlg(CFrameCameraModel* frameCameraModel,bool viewOnly,QWidget* parent=0);
	~Bui_FrameCameraModelDlg(void);

	virtual bool okButtonReleased();
	virtual bool cancelButtonReleased();
	virtual CCharString helpButtonReleased();
	virtual CCharString getTitleString();

	virtual void elementAdded(CLabel element=""){};
	virtual void ckeckDlgContent();
	virtual void cleanUp();

signals:
	virtual void enableOkButton(bool b);
	virtual void closeDlg(bool b);
	virtual void elementsChangedSignal(CLabel element);

public slots:
	void cameraMountingStatusChanged(bool b);
	void cameraStatusChanged(bool b);
	void tWCurrentChanged( int index );

	void pbCameraReleased();
	void pbCamaraMountingReleased();
	void rbRollPitchYawReleased();
	void rbOmegaPhiKappaReleased();	
	void leTextChanged(const QString & text);
	void cBCameraCurrentIndexChanged(int index);
	void cBCameraMountingCurrentIndexChanged(int index);
	void leXEditingFinished();
	void leYEditingFinished();
	void leZEditingFinished();
	void leAngle1EditingFinished();
	void leAngle2EditingFinished();
	void leAngle3EditingFinished();

private slots:
	virtual void elementsChangedSlot(CLabel element);

private:
	void applySettings();
	void checkAvailabilityOfSettings();

	void initCameraMountingDlg(CCameraMounting* cameraMounting);
	void initCameraDlg(CFrameCamera* camera);

	bool selectCamera(int cbCameraIndex);
	bool selectCameraMounting(int cbCameraMountingIndex);

	void updateCameras();
	void updateMountings();

	bool viewOnly;
	
	// local copies 
	vector<CCamera*> cameras;
	vector<CCameraMounting> cameraMountings;
	CXYZPoint cameraPos;
	COmegaPhiKappaRotation omegaPhiKappa;
	CRollPitchYawRotation rollPitchYaw;
	CFrameCameraModel frameCameraModel;


	// references to existing elements
	vector<CCamera*> existingCameras;
	vector<CCameraMounting*> existingCameraMountings;
	CFrameCameraModel* existingFrameModel;

	int oldTab;
	int selectedCameraMountingIndex;
	int selectedCameraIndex;

	Bui_CameraMountingDlg* cameraMountingDlg;
	Bui_FrameCameraDlg* cameraDlg;
};

#endif