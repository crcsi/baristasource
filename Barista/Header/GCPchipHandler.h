#ifndef __CGCPchipHandler__
#define __CGCPchipHandler__
 
#include "DigitizingHandler.h"
//#include "GDALFile.h"
//#include "ImageExporter.h"
//#include "BaristaLSM.h"
//#include "Bui_MonoPlotDlg.h"

class Bui_GDALExportDlg;

class CGCPchipHandler : public CDigitizingHandler
{
public:
	CGCPchipHandler();
	
	virtual ~CGCPchipHandler();

	virtual bool handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v);

	//virtual bool keyReleaseEvent (QKeyEvent *e, CBaristaView *v);

	virtual void handleLeaveEvent ( QEvent * event, CBaristaView *v );

	virtual void handleEnterEvent ( QEvent * event, CBaristaView *v );

	//virtual void setCurrentCursor(CBaristaView *v) { v->setCrossCursor();};

	virtual void open();
	virtual void close();

	//void openMatchingParameterDlg();
	void remove_imagePoint(CBaristaView *v, CLabel l);
//private: 
//	CImageExporter internalImageExporter;
//	CImageExporter& currentImageExporter;

protected:
	//bui_MonoPlotDlg matchdlg;
	//CBaristaLSM lsm;
	CVImage* oldVImage;
	CVImage* newVImage;

	double ulx;
	double uly;
	//double lrx;
	//double lry;

	int chipWidth;
	int chipHeight;

	unsigned long chipID;
	CCharString chipName;
	//CCharString chipID;
	Bui_GDALExportDlg* dlg;
}; 

#endif