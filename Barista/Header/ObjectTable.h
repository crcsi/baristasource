#ifndef _CObjectTable_
#define _CObjectTable_

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */
#include <QTableView>

#include <QStringList>
#include "ObjectSortFilterModel.h"


class CObsPointArray;
class CObsPoint;
class CObsPointDataModel;
class QSortFilterProxyModel;
class CVImage;

#include "Filename.h"


#define ROW_HEIGHT_TABLE 20


class CObjectTable :  public QTableView
{
	Q_OBJECT
public:

	CObjectTable(CObjectDataModel* dataModel,QWidget* parent);
	~CObjectTable();


	void setFormatString(const QStringList& format);
	void setShowSelector(bool show);
	void setShowSigma(bool show);
	void setShowCoordinates(bool show);
	void setAllowSigmaChange(bool allow);
	void setHeader(const QStringList& headerList);
	void setAllowDeletePoints(bool allow);
	void setAllowChageLabels(bool allow);

	void setFilterRegExp(QString regExp);

	CObsPoint*  getObsPoint(const QModelIndex& index);
	
signals:
	virtual void nameChanged(QString name);

public slots:
	virtual void dataModelChanged(DataModelChanges changeType,vector<QString> parameter);

protected:

	virtual void mouseDoubleClickEvent ( QMouseEvent * event ); 

	virtual void mouseReleaseEvent ( QMouseEvent * mouseEvent );

	void closeEvent(QCloseEvent *event) ;

	void deleteMemory();

	CObjectSortFilterModel* proxyModel;

	vector<QAction*> actionList;

	int objectDimension;

};
#endif
