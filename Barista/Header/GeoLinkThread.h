#ifndef __CGeoLinkThread__
#define __CGeoLinkThread__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include <QThread>
#include <QMutex>
#include "3DPoint.h"
#include "ReferenceSystem.h"

class CBaristaView;
class CTrans3D;


class CGeoLinkThread : public QThread
{
     Q_OBJECT
public:
	CGeoLinkThread(CBaristaView* view);
	~CGeoLinkThread(void);

	void doGeoLink(const C3DPoint& centrePoint,const CReferenceSystem &refSysOf3DPoint, double zoomFactor,float resolutionSource,bool changeResampling = false);

 protected:
     void run();

	 QMutex mutex;
	 CBaristaView* view;
	 C3DPoint centrePoint;
	 CReferenceSystem refSysOf3DPoint;
	 CTrans3D* trafo;   // the trafo from center point to reference  system of view (CVImage)
	 double zoomFactor;
	 bool changeResampling;
	 float resolutionSource;
	
};
#endif