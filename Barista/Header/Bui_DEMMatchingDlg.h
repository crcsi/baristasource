#ifndef BUI_DEMMATCHINGDLG_H
#define BUI_DEMMATCHINGDLG_H

#include <QDialog>
#include "ui_DEMMatching.h"
#include "BaristaHtmlHelp.h"

#include "Filename.h"
#include "DenseMatcher.h"
#include "m_FeatExtract.h"
#include "ReferenceSystem.h"
#include "GenericPointCloud.h"

class CVImage;
class CSensorModel;
class CVDEM;
class CDEMMatcher;
class CXYZPolyLine;

class Bui_DEMMatchingDlg : public QDialog, public Ui_DEMMatchingDialog
{
  Q_OBJECT
  
  protected:
	
	  vector<CVImage *> AvailableImageList;

	  vector<CVImage *> SelectedImageList;

	  CVDEM *DEM;

	  CXYZPolyLine *pOuterPolyLine;
	  vector<CXYZPolyLine *> InnerPolylineList;
	  
	  CSGMParameters SGMpars;

	  C2DPoint llPoint;
	  
	  C2DPoint urPoint;

	  C2DPoint gridSize;

	  CFileName DEMFileName;

	  CDEMMatcher *matcher;
	  int decimals;

	  CReferenceSystem *pRefSys;

	  bool success;

	  eInterpolationMethod InterpolationMethod;

	  double maxDistInterpol;
	  
	  int neighbourhoodSize;

	  double  minZ, maxZ;

  public:

	  Bui_DEMMatchingDlg(QWidget *parent = 0);

	  ~Bui_DEMMatchingDlg();

	  bool getSuccess() const { return this->success; };

	  bool compute();

  public slots:
	

	  void pBOkReleased();
	  void pBCancelReleased();
	  void pBHelpReleased();

	  void preFilterCapSliderChanged(int preFilterCap );
	  void speckleWindowSizeChanged(int size );
	  void speckleRangeChanged(int range );

	  void P1EditingFinished();
	  void P2EditingFinished();
	  void disp12MaxDiffEditingFinished();
	  void onUnitquenessEditingFinished();
	  void onSADWindowEditingFinished();

	  void onFullDPChanged();
	  

	  void leMaxZEditingFinished();
	  void leMinZEditingFinished();

	  void onImageListChanged();

	  void onLLChanged();
	  void onURChanged();
	  void onGridChanged();

	  void onSetToMax();

	  void onInterpolationMethodChanged();

	  void onMaxDistInterpolChanged();
	  
	  void onNeighbourhoodChanged();

	  void onOuterBorderlineChanged();

	  void onInnerBorderlineChanged();

	  void onOutputFileChanged();

	
  protected:

	  void setParsToDlg();
	  
	  void setupImageSelector();
	
	  void checkSetup();

	  void setDEMExtents();

	  void getPminPmax(C2DPoint &pmin, C2DPoint &pmax, CVImage *img);
	  
	  void saveToFile(const CCharString &fn) const;

	  bool initFromFile(const CCharString &fn);

};

#endif // BUI_DEMMATCHINGDLG_H
