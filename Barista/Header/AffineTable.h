#ifndef __CAffineTable__
#define __CAffineTable__

#include "BaristaTable.h"


class CAffine;

class CAffineTable : public CBaristaTable
{
    public:
    CAffineTable(CAffine* affine,QWidget* parent = 0);
    ~CAffineTable(void);
 
    void setAffine(CAffine* affine);
    CAffine *getAffine() const { return affine; };

    void closeEvent ( QCloseEvent * e );

	virtual void elementAdded(CLabel element){};
	virtual void elementDeleted(CLabel element){};

	virtual void initTable();

protected:
    CAffine* affine;
 };

#endif