#ifndef __CTokenPtrArray__
#define __CTokenPtrArray__


#include "MUObjectPtrArray.h"
#include "Token.h"

class CTokenPtrArray : public CMUObjectPtrArray<CToken>
{

public:

	CTokenPtrArray(int nGrowBy = 0);
	~CTokenPtrArray(void);
};

#endif
