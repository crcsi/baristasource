#ifndef __CPolynomial__
#define __CPolynomial__

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Jochen Willneff
 * @version 1.0
 */
#include "doubles.h"

#include "Serializable.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
class Matrix;

class CPolynomial : public CSerializable
{
private:

  doubles coefficients;
   Matrix *covariance;
//=============================================================================

public:

  CPolynomial(int degree);
  CPolynomial(const CPolynomial &poly);
  CPolynomial &operator = (const CPolynomial & poly);

  ~CPolynomial();

  double &coefficient(int degree) { return coefficients[degree]; };
  const double &coefficient(int degree)const  { return coefficients[degree]; };
  void setCoefficient(int degree,double value) {this->coefficients[degree] = value;};

  void operator *=(double factor);

  void setCovariance(const Matrix& covar);
	
  void setCovarElement(unsigned int row, unsigned int col, double value);

  const Matrix& getCovariance()const;

  int getDegree() const { return coefficients.size() - 1; };

  double functionValueAt(double argument) const;
  double firstDerivativeAt(double argument) const;

  CPolynomial getFirstDerivative() const;

  bool zeroCrossing(double approx, double &zero) const; //Newton-method

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();
	
	virtual bool isa(string& className) const
	{
	  if (className == "CPolynomial")
		  return true;
	 return false;
	 };

	  virtual string getClassName() const {return string("CPolynomial"); };  
//=============================================================================

};

#endif
