#ifndef __Statistics__
#define __Statistics__

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#define MAXDOF  10                      // Max. degrees of freedom for 
                                        // chi square test

class Matrix;
class ColumnVector;

 class statistics
 {
  public:

//=============================================================================
//======================== Gamma function and factorial =======================

    static double factorial(long n);

    static double gammaNby2(long n);

    static double betaNMby2(double x, long n1, long n2);


//=============================================================================
//=============================== Distributions ===============================

    static double normalDistribution(double x);

    static double chiSquareDistribution(double x, long n);

    static double fisherDistribution(double x, long n1, long n2);

//=============================================================================
//================================ Densities  =================================

    static double normalDensity(double x);

    static double chiSquareDensity(double x, long n);

    static double fisherDensity(double x, long n1, long n2);

//=============================================================================
//================================== Fractils =================================

   static double normalFractil(double alpha);

   static double chiSquareFractil(double alpha, long n);

   static double fisherFractil(double alpha, long n1, long n2); 
   // not yet correct

   static double fisherFractil95(long n1, long n2);


//=============================================================================


//=============================================================================
//============================= Auxiliary Methods =============================

    static double chiSquareDistribution2(double x);
    static double chiSquareDistribution4(double x);
    static double chiSquareDistribution6(double x);


    static double chiSquareDensity2(double x);
    static double chiSquareDensity4(double x);
    static double chiSquareDensity6(double x);


	/// Auxiliary method for the reduction for vectors according to Heuel (2004).
	static void getReducedVector(const Matrix &U1,       const Matrix &U2,
                                 const ColumnVector &X1, const ColumnVector &X2, 
                                 const Matrix &covar1,   const Matrix &covar2,
                                 int rank,               
								 ColumnVector &dist,     Matrix &covarDist);

// X1, X2:         homogeneous vectors of the two geometric entities that shall
//                 be tested for some geometric relation
// covar1, covar2: the covariance matrices of X1 and X2, respectively
// U1, U2:         two construction matrices, appropriate for the test
// rank:           degrees of freedom of the test
// 
// If the distance vector d = U1 * X2 = U2 * X1
// has a dimensionality larger than rank, U1 and U2 have to 
// be reduced consistently to Ii_rank lines. 
//
// dist:      the reduced distance vector d' with rank lines
// covarDist: the rank * rank covariance matrix Qdd' of dist
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

   static double testMetric(const Matrix &U1,       const Matrix &U2,
                            const ColumnVector &X1, const ColumnVector &X2, 
                            const Matrix &covar1,   const Matrix &covar2,
                            int rank);

// X1, X2:         homogeneous vectors of the two geometric entities that shall
//                 be tested for some geometric relation
// covar1, covar2: the covariance matrices of X1 and X2, respectively
// U1, U2:         two construction matrices, appropriate for the test
// rank:           degrees of freedom of the test
// 
// The method computes the reduced distance vector d' and its covariance
// matrix Qdd' (cf getReducedVector(..))
// The method returns the test metric t = d'^t * Qdd'^-1 * d'
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//============================================================================
//========================== Query / set quantiles ===========================

   static float getNormalQuantil() 
   { return normalQuantil; };

   static float chiSquareQuantil (int degreesOfFreedom) 
   { return chiSquareQuantils[degreesOfFreedom]; };

   static void setNormalQuantil (float S);

   static void setChiSquareQuantils(float S);

// S is the security percentage, i.e. S = 1 - alpha; by default, S = 95%
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

 protected:

   static float normalQuantil;      // current quantil of the normal distribution 

   static float chiSquareQuantils[MAXDOF];
                                  // current quantiles of the Chi square distribution;
                                  // the index to the array is the degrees of freedom

 };

#endif

