// FileArchive.h: interface for the CFileArchive class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CFileArchive__
#define __CFileArchive__

#include <fstream>
using namespace std;

#include "CharString.h"

class CLabel;

class CFileArchive : public fstream
{
private:
	int m_nMode;

public:
	static int READ;
	static int WRITE;
public:
	CFileArchive( const char* szName, int nMode);
	CFileArchive();

	virtual ~CFileArchive();

	void Open(const char* szName, int nMode);

	// storage
	CFileArchive& operator<<(double d);
	CFileArchive& operator>>(double& d);

	CFileArchive& operator<<(long l);
	CFileArchive& operator>>(long& l);

	CFileArchive& operator<<(unsigned long l);
	CFileArchive& operator>>(unsigned long& l);

	CFileArchive& operator<<(short s);
	CFileArchive& operator>>(short& s);

	CFileArchive& operator<<(unsigned short s);
	CFileArchive& operator>>(unsigned short& s);

	CFileArchive& operator<<(const char* pChar);
	CFileArchive& operator>>(char*& pChar);

	CFileArchive& operator<<(char c);
	CFileArchive& operator>>(char& c);

	CFileArchive& operator<<(unsigned char c);
	CFileArchive& operator>>(unsigned char& c);

	CFileArchive& operator<<(CCharString& String);
	CFileArchive& operator>>(CCharString& String);

	CFileArchive& operator<<(bool b);
	CFileArchive& operator>>(bool& b);

	CFileArchive& operator<<(CLabel& Label);
	CFileArchive& operator>>(CLabel& Label);

	bool IsStoring();
};

#endif // __CFileArchive__

