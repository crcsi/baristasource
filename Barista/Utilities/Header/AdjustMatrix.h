#ifndef __CADJUSTMATRIX_H___
#define __CADJUSTMATRIX_H___

class Matrix;
class CPointBase;

class CAdjustMatrix
{
  protected:

	  double **data;

	  int rows, cols;

  public:
	  
	  CAdjustMatrix(int r, int c);	
	  CAdjustMatrix(int r, int c, double val);	
	  CAdjustMatrix(const CAdjustMatrix &mat);
	
	  ~CAdjustMatrix();

	  double &operator() (int i, int j) { return data[i][j]; };

	  const double &operator() (int i, int j) const { return data[i][j]; };

	  int getRows() const {return this->rows;};
	  int getCols() const {return this->cols;};
	  CAdjustMatrix &operator = (const CAdjustMatrix &mat);

	  friend CAdjustMatrix operator *(const CAdjustMatrix &A, const CAdjustMatrix &B); 
	  friend CAdjustMatrix operator *(const CAdjustMatrix &A, const double &value); 
	  friend CAdjustMatrix operator *(const CAdjustMatrix &A, const CPointBase &p); 

	  void operator *= (double factor)
	  {
		  for (int r = 0; r < rows; ++r)
		  {			  		 
			  for (int c = 0; c < cols; ++c)
			  {
				  data[r][c] *= factor;
			  }
		  }
	  };

	  void operator /= (double factor)
	  {
		  *this *= (1.0 / factor);
	  }

	  void transpose();
	  void invert();

      void addToMatrix(int startRow, int startCol, Matrix &dest,bool addTransposed = false)const;

	  void reSize(int r,int c);
	  void reSize(int r,int c,double val);


};

#endif
