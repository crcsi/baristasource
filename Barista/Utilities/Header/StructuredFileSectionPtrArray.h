#ifndef __CStructuredFileSectionPtrArray__
#define __CStructuredFileSectionPtrArray__

#include "MUObjectPtrArray.h"
#include "StructuredFileSection.h"

class CStructuredFileSectionPtrArray : public CMUObjectPtrArray<CStructuredFileSection>
{

public:

	CStructuredFileSectionPtrArray(int nGrowBy = 0);
	~CStructuredFileSectionPtrArray(void);
};
#endif
