#ifndef __CIntegerArrayPtrArray__
#define __CIntegerArrayPtrArray__

#include "IntegerArrayPtrArray.h"
#include "MUIntegerArray.h"

class CIntegerArrayPtrArray : public CMUObjectPtrArray<CMUIntegerArray>
{
public:
	CIntegerArrayPtrArray(int nGrowBy = 0);
	~CIntegerArrayPtrArray(void);
};

#endif

