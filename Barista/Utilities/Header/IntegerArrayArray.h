#ifndef __CIntegerArrayArray__
#define __CIntegerArrayArray__

#include "MUIntegerArray.h"
#include "IntegerArrayPtrArray.h"
#include "MUObjectArray.h"

class CIntegerArrayArray : public CMUObjectArray<CMUIntegerArray, CIntegerArrayPtrArray>
{
public:
    CIntegerArrayArray(int nGrowBy = 0);
    ~CIntegerArrayArray(void);
};

#endif
