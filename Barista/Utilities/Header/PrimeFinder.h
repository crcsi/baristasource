#ifndef __CPrimeFinder__
#define __CPrimeFinder__

#include "MUIntegerArray.h"
class CPrimeFinder
{
public:
	CPrimeFinder(void);
	~CPrimeFinder(void);

	CMUIntegerArray* getPrimes(int limit);

private:
	CMUIntegerArray primes;
};
#endif
