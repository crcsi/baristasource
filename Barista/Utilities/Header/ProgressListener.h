#ifndef __CProgressListener__
#define __CProgressListener__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "CharString.h"

class CProgressListener
{
  protected:

    /// the percent complete
    double progressValue;
	CCharString titleString;

  public:

	CProgressListener();
	virtual ~CProgressListener();

    void setProgressValue(double percent);
    double getProgress() const;

	void setTitleString(const char* title) { this->titleString = title;};
	const char* getTitleString() const {return this->titleString.GetChar(); };


};
#endif