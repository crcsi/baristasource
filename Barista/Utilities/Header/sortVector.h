#ifndef ____CSortVector_H____
#define ____CSortVector_H____


#include <vector>
using namespace std;

template<class T> class CSortVector : public vector<T>
{
public:
	CSortVector(void);
	CSortVector(const CSortVector<T> &vec) : vector<T>(vec) { };
	CSortVector(int n, T initVal);

	int size() const
	{
		return (int)vector<T>::size();
	};

	void resize(int n)
	{
		vector<T>::resize(n);
	}

	void resize(int n, T initVal)
	{
		if (n > 0)
		{
			int oldSize = this->size();

			vector<T>::resize(n);

			for (int i = oldSize; i < n; i++)
				this->at(i) = initVal;
		}
	};

	virtual ~CSortVector();

	virtual void sortAscending();

	virtual void sortDescending();

	void swapElements(long i1, long i2);

	long getMaxIndex() const;

  protected:
	 
    void PartialSortAscending(long low, long high);
    void PartialSortDescending(long low, long high);
};

#include "sortVector.cpp"

#endif