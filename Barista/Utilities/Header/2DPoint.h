#ifndef __C2DPoint__
#define __C2DPoint__

#include "PointBase.h"
#include "m_match_def.h"

#include <math.h>


#include "Serializable.h"

class C2DPoint : public virtual CSerializable, public virtual CPointBase
{
  public:
	  double &x;
	  double &y;

  public:

	  /// Constructors
	  C2DPoint() : CPointBase(2), x(coordinates[0]), y(coordinates[1]) { };
	  C2DPoint(const C2DPoint &p) : CPointBase(p), x(coordinates[0]), y(coordinates[1]) { };
	  C2DPoint(double I_x, double I_y) : CPointBase(2), x(coordinates[0]), y(coordinates[1]) 
	  { x = I_x; y= I_y; };
	  C2DPoint(int I_id, double I_x, double I_y) :  CPointBase(2), x(coordinates[0]), y(coordinates[1])
	  { id = I_id; x = I_x; y = I_y; };

	  /// Destructor
	  virtual ~C2DPoint() { };

  	  /// assignment operator 
	  C2DPoint &operator = (const C2DPoint & rhs) 
	  { id = rhs.id; x = rhs.x; y = rhs.y; return *this; }  

	  /// unary minus operator
	  C2DPoint operator - () const { return C2DPoint ( -x, -y ); };

	  /// addition and subtraction of points 
	  void operator += ( const C2DPoint &p ) 
	  {	x += p.x; y += p.y; }

	  void operator -= ( const C2DPoint &p )
	  {	x -= p.x; y -= p.y; };

	  friend C2DPoint operator + ( const C2DPoint &p1, const C2DPoint &p2 )
	  { return C2DPoint(p1.x + p2.x, p1.y + p2.y); };

	  friend C2DPoint operator - ( const C2DPoint &p1, const C2DPoint &p2 )
	  { return C2DPoint(p1.x - p2.x, p1.y - p2.y); };


	  /// multiplication and division of points by scalars	
	  void operator *= ( double a )
	  {	x *= a; y *= a; };

	  C2DPoint operator * ( double a ) const
	  {	return C2DPoint(id, x * a, y * a); };
    
	  friend C2DPoint operator * ( double a, const C2DPoint &p )
	  { return  p * a; };
	
	  void operator /= ( double a )
  	  {	x /= a; y /= a; };
	
	  C2DPoint operator / ( double a ) const
	  { return C2DPoint(id, x / a, y / a); };

	  /// geometrical operations / queries

	  double innerProduct(const C2DPoint& p) const
	  { return x * p.x + y * p.y; };

	  double getSquareNorm() const { return (x * x + y * y); };

	  double getNorm() const { return sqrt(getSquareNorm()); };

	  double getSquareDistance(C2DPoint p) const
	  {	 p -= *this; return p.getSquareNorm(); };

	  double getDistance(const C2DPoint &p) const	  
	  { return sqrt(getSquareDistance(p)); };

	  double getDirection(const C2DPoint &p) const	  
	  { return atan2((p.x-x),(p.y-y)); };

	  void normalise();

	  void swap();

	  // serialize functions
	  virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	  virtual bool isModified();
	  virtual void reconnectPointers();
	  virtual void resetPointers();
	
	  virtual bool isa(string& className) const
	  {
		  if (className == "C2DPoint")
			  return true;
		  return false;
	  };

	  virtual string getClassName() const {return string("C2DPoint"); };  

	  bool writeDXF( ostream& dxf, int *lineCodes, int decimals,
		             double xfactor, double yfactor) const;

};
#endif
