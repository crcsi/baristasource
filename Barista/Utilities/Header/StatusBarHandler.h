#ifndef __CStatusBarHandler__
#define __CStatusBarHandler__


class CStatusBarHandler
{
  public:

	  /// Constructors
	  CStatusBarHandler()  { };
	  
	  ~CStatusBarHandler() { };

	  virtual void setText(const char *text) const { };  
};

#endif
