// abstract base class for CTestable objects
#ifndef __CTestable__
#define __CTestable__
#include "CharStringArray.h"

class CTestable
{
public:
	CTestable();
	virtual ~CTestable();

	// Virtual functions
	virtual bool runTest(CCharStringArray& report) = 0;
};
#endif
