#ifndef ____CNormalMatrix_HPP____
#define ____CNormalMatrix_HPP____

#include "doubles.h"
#include "AdjustMatrix.h"

//****************************************************************************

class CAdjustMatrix;

//****************************************************************************

class CNormalEquationMatrix : protected doubles
{
  //****************************************************************************
  //
  //  Class       : CNormalEquationMatrix
  //
  //  Description : Normal Equation Matrix. The matrix of size
  //                nUnknowns * nUnknowns, but symmetry is taken into
  //                account: the elements are stored in a one-dimensional
  //                array; the matrix looks as follows:
  //                (0) (1) (3) ....
  //                    (2) (4)    .
  //                        (5)    .
  //                               (n * (n+1)) / 2
  //
  //****************************************************************************

  protected:
 
	  unsigned long nUnknowns;             // number of unknowns

	  double epsilon;

//============================================================================

  public:

 
//============================================================================
//================================ Constructor ===============================
  
	  CNormalEquationMatrix(unsigned long unknowns);


//============================================================================
//================================ Destructor ================================

	  virtual ~CNormalEquationMatrix();


//============================================================================
//=============================== access data ================================

	  unsigned long getNUnknowns() const { return nUnknowns; };

	  double getEpsilon() const { return epsilon; };

	  static unsigned long numElements(unsigned long unknowns)
	  { return (unknowns * (unknowns + 1)) / 2; };

	  unsigned long numElements() const
	  { return numElements(nUnknowns); };

	  static unsigned long getIndex(unsigned long row, unsigned long col);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  double operator () (unsigned long element) const
	  { return doubles::operator[](element); };

	  double operator () (unsigned long row, unsigned long col) const
	  { return doubles::operator[](getIndex(row, col)); };

	  double &operator[] (unsigned long index) 
	  { return doubles::operator[](index); };

	  const double &operator[] (unsigned long index) const
	  { return doubles::operator[](index); };



//============================================================================
//=============================== change data ================================

	  void setEpsilon(double Epsilon);

	  void setElement(unsigned long index, double value) 
	  { doubles::operator[] (index) = value; };

	  virtual void initialize (double value = 0);

	  void initialize (unsigned long unknowns, double value = 0);


//============================================================================

	  virtual void addSubMatrix(int row, int col, CAdjustMatrix &Nsub);

//============================================================================

	  bool solveCholesky(CNormalEquationMatrix &N, int &nErrors); 
	  // R <= R^*N   {i.e. Cholesky decomposition of N: R'*R =N}

	  bool invertR(const CNormalEquationMatrix &R);

	  bool solveQxx(const CNormalEquationMatrix &RinvT);

	  bool invert(CNormalEquationMatrix &N);

	  virtual bool solveCholesky(int &nErrors)
	  { return solveCholesky(*this, nErrors); };
	  // R <= R^*N   {i.e. Cholesky decomposition of N: R'*R =N}

	  virtual bool invertR() { return invertR(*this); };

	  virtual bool solveQxx() { return solveQxx(*this); };

	  virtual bool invert() { return invert(*this); };

  //============================================================================

  protected:

	  virtual int choleskyError(int rowi, double &Nii, double &Rii) const;

};

//****************************************************************************
//
//   E n d   o f   c l a s s   C N o r m a l E q u a t i o n M a t r i x
//
//****************************************************************************

//****************************************************************************
//
//  Class       : CtVector
//
//  Description :
//
//****************************************************************************

class CtVector : public doubles
{
  public:

//============================================================================
//=============================== Constructors: ==============================
 
	  CtVector(unsigned long unknowns);


//============================================================================
//================================ Destructor: ===============================

	  virtual ~CtVector() { };
  
//============================================================================
//=============================== access data ================================

	  unsigned long getNUnknowns() const { return this->size(); };

	  double operator () (unsigned long element) const
	  { return doubles::operator [](element); };

	  const double &operator[] (unsigned long index) const
	  { return doubles::operator [](index); };

	  double &operator[] (unsigned long index) 
	  { return doubles::operator [](index); };

//============================================================================
//=============================== change data ================================

	  void initialize (double value = 0);

	  virtual void initialize (unsigned long unknowns, double value = 0);

//============================================================================

	  void addVector(int row, CAdjustMatrix &Tsub);

	  bool reduceFromR(const CNormalEquationMatrix &R,
                       const CtVector &Atl);

	  bool reduceFromRinv(const CNormalEquationMatrix &RinvT,
                          const CtVector &Atl);

	  bool solveFromR(const CNormalEquationMatrix &R,
                      const CtVector &g);

	  bool solveFromRinv(const CNormalEquationMatrix &RinvT,
                         const CtVector &g);

	  bool solve(CNormalEquationMatrix &N, const CtVector &Atl);

	  bool solve(CNormalEquationMatrix &N, CNormalEquationMatrix &Qxx, const CtVector &Atl);

};

//****************************************************************************
//
//   E n d   o f   c l a s s   C t V e c t o r
//
//****************************************************************************


#endif

