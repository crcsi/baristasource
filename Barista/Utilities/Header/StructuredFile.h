#ifndef __CStructuredFile__
#define __CStructuredFile__

#include "StructuredFileSection.h"


#define NEW_CHILD 0
#define END_OF_SECTION 1
#define MORE_TOKENS 2

class CTextFile;

class CStructuredFile
{
public:
	CStructuredFile(void);
	~CStructuredFile(void);

	// read functions
	bool readFile(const char* pFileName);
	//void parseSection(CStructuredFileSection* pSection);
	//int nextToken(CStructuredFileSection* pSection);
	void parse(CStructuredFileSection* pSection);

	// write functions
	bool writeFile(const char* pFileName);
	void writeSection(CStructuredFileSection* pSection);

	CStructuredFileSection* getMainSection();
	static int sectionCount;
	static int byteCount;
	static int currentByte;

	double timeLength1;


private:
	void verifyTokenStringLength(int length);
	void trimTokenLength();

	char* pData;
	CTextFile* pOutputFile;
	int index;
	int lastMark;
	int fileLength;
	bool endOfFile;
	CStructuredFileSection mainSection;
	int currentTabLevel;
	bool firstSection;

	char* tokenString;
	int tokenStringLength;

};

#endif
