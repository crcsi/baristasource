#ifndef __CProgressHandler__
#define __CProgressHandler__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "ProgressListener.h"

class CProgressHandler
{
  public:

	  CProgressHandler() : listener(0), listenerMaxValue(100){}; // listener: the actual listener

	  virtual ~CProgressHandler() {};

	  double getMaxListenerValue() const {return this->listenerMaxValue;};

	
	  CProgressListener *getListener() const { return this->listener; }
	

	  void setMaxListenerValue(double value) { this->listenerMaxValue = value;};

	  void setProgressListener(CProgressListener* l);
	
	  void removeProgressListener();

  protected:

	  CProgressListener* listener;	// the actual listener
	  double listenerMaxValue;		
};
#endif