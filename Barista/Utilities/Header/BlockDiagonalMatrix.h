#ifndef __CBlockDiagonalMatrix__
#define __CBlockDiagonalMatrix__

#include <vector>
using namespace std;

class Matrix;

class CBlockDiagonalMatrix
{
  public:
	CBlockDiagonalMatrix(void);
	CBlockDiagonalMatrix(const CBlockDiagonalMatrix &src);
	CBlockDiagonalMatrix(const vector<Matrix *> &Covariance);
	
	CBlockDiagonalMatrix(int sr, int sc, int n);
	~CBlockDiagonalMatrix(void);

  private:

    vector<Matrix *> subMatrices; // These are the blocks

public:

	unsigned int getRowDimension() const;

	unsigned int getColumnDimension() const;

	unsigned int getSubMatrixCount() const;

	Matrix *getSubMatrix(int index) const;

	CBlockDiagonalMatrix &operator = (const CBlockDiagonalMatrix &src);

	void addSubMatrix(Matrix &M);
	void resize(int sr, int sc, int n);

	void inverse(CBlockDiagonalMatrix &I) const;
	void transpose(CBlockDiagonalMatrix &T) const;
    
    void times(Matrix &result, const Matrix &B) const;
	void times(Matrix *result, const Matrix *B) const { return times(*result, *B); };

	void deleteAllSubmatrices();
};
#endif
