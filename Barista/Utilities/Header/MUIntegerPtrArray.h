// MUIntegerPtrArray.h: interface for the CMUIntegerPtrArray class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CMUIntegerPtrArray__
#define __CMUIntegerPtrArray__

#include "MUObjectArray.h"

class CMUIntegerPtrArray : public CMUObjectPtrArray<int> 
{
public:
	CMUIntegerPtrArray(int nGrowBy = 0);
	virtual ~CMUIntegerPtrArray();

};

#endif // __CMUIntegerPtrArray__
