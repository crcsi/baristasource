#ifndef __CSystemUtilities__
#define __CSystemUtilities__

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#ifdef WIN32
#include <windows.h>
#endif


#include "CharString.h"
#include "Filename.h"

class CCharStringArray;
struct _stat;

class CSystemUtilities
{
  public:

//===========================================================================

	  static const char dirDelim;
	  static const CCharString dirDelimStr;

//===========================================================================
//	  call a system command
  
	  static int systemCommand( const CCharString &sysCommand );
  
//===========================================================================
//	  get the space available on the hard disk
//		return value :	 0: successfully
//						-1: error
  
	  static int diskFree( unsigned int drive, double &totalSpace, double &freeSpace );

//===========================================================================
//	get a list of all filenames described by Filter in directory Path 
  
	  static int dir ( CCharStringArray &FileList, 
		               CCharString Path = CSystemUtilities::dirDelimStr,
		               CCharString Filter = "*.*" );

//===========================================================================
//	get a list of all filenames described by Filter in directory Path, including
//	subdirectories

	  static int dirS ( CCharStringArray &FileList, 
		                CCharString Path = CSystemUtilities::dirDelimStr,
		                CCharString Filter = "*.*");

//===========================================================================
//	delete a file or a list of files discribed by string "filename" 
  
	  static int del ( const CCharString & filename );

//===========================================================================
//	delete Path and all files and subdirectories

	  static int delS( CCharString Path = CSystemUtilities::dirDelimStr, 
		               CCharString Filter = "*.*" );
 
//===========================================================================
//	get relative path

	  static CFileName RelPath(const CFileName &filename, 
		                                         const CFileName &relTo);

	  static CFileName AbsPath(const CFileName &filename);

//===========================================================================
//	get current working directory 

	  static CCharString getCwd( int bufsize = 256 );

//===========================================================================
//	returns a string, where the environment variables are evaluated and
//	replaced (if they are set)
 
	  static CCharString evaluateEnvironmentVariables( const CCharString& Path );

//===========================================================================
//	returns the value of the environment variable env

	  static CCharString getEnvVar( const CCharString& env );

  
//===========================================================================
//	change directory; environment variables in newDirectory are 
//	evaluated and replaced
//	return value:	0: successfully changed the directory
//				   -1: path not found

	  static int chDir( CCharString &newDirectory );

//===========================================================================
//	change directory; environment variables in newDirectory are 
//	evaluated and replaced, but newDirectory remains constant.
//	return value:	0: successfully changed the directory
//				   -1: path not found

	  static int chDir( const CCharString& newDirectory );

//===========================================================================
//	rename file
//	return value :		0 : file "oldFileName" was renamed to newFileName
//             otherwise:   file "oldFileName" doesn't exist or
//						   (overWrite is not set && file "newFileName" exists)

	  static int move ( const CCharString& oldFileName, 
		                const CCharString& newFileName, bool overWrite = false );

//===========================================================================
//	copy a file
//	return value :       0 : file "oldFileName" was copied to newFileName
//                otherwise: file "oldFileName" doesn't exist or
//		                     newFileName could not be created.

	  static int copy ( const CCharString& oldFileName, 
		                const CCharString& newFileName );


//===========================================================================
//	create a directory
//	The directory dirPath will be created.
//	Only one directory can be created at a time (see createDirectoryTree(...))
//	return value : 0 : everything is ok.
//                -1 : operation did not succeed (e.g. directory already
//                     exists).

	  static int mkdir( const CCharString& dirPath );

//===========================================================================
//	create directory tree (recursive create directories)
//	return value : 0 : everything is ok.
//                -1 : operation did not succeed.
 
	  static int createDirectoryTree( const CCharString& directory );

//===========================================================================
//	check whether directory dirPath exists or not

	  static bool dirExists( const CCharString& dirPath ) ;

//===========================================================================
//	check whether directory dirPath is valid or not
//	Definition of valid dir:
//	DriveName ':' dirDelim { ValidFileName [ dirDelim ] }
//	DriveName := 'a' .. 'z' || 'A' .. 'Z'
//	ValidFileName := string with length > 0 without
//                  { '/', '\\', ':', '*', '"', '?', '<', '>', '|' }

	  static bool validDir( const CCharString& dirPath );
 
//===========================================================================
//	delete a directory
//	The directory dirPath will be removed, but only if it is empty.
//	Only one directory can be created at a time (see createDirectoryTree(...))
//	return value : 0 : everything is ok.
//                -1 : operation did not succeed
 
	  static int rmDir( const CCharString& dirPath );
 
//===========================================================================
//	does file exist or not?

	  static bool fileExists ( const CCharString& fileName );

//===========================================================================
//	Check if file mode is the same as "mode".
//	Values for mode: 00 Existence only
//                   02 Write permission
//                   04 Read permission
//                   06 Read and write permission
//	Return -1 if the named file does not exist or is not accessible in the
//	given mode; in this case, errno is set as follows:
//	EACCES: Access denied: file's permission setting does not allow
//          specified access.
//	ENOENT: Filename or path not found.

	  static int fileAccess( const CCharString& fileName,  int mode );

//===========================================================================
//	Return file statistics eg: Time of last acess, size ... for file named
//	fileName in stats.
//	Return values:
//		    -1: File open error; check errno for further diagnosis.
//		EMFILE: No more file handles available (too many open files)
//		ENOENT: File or path not found
//			 0: OK

	  static int fileStats ( const CCharString& fileName,
                             struct _stat& stats );
 
//===========================================================================
//	Return modification date of file named fileName
//	Return values:
//			-1: File open error; check errno for further diagnosis.
//		EMFILE: No more file handles available (too many open files)
//		ENOENT: File or path not found
//			 0: OK

	  static int fileModDate( const CCharString& fileName, time_t& modDate );


//===========================================================================

	  static CCharString timeString();

	  static CCharString dateString();


#ifdef WIN32
//===========================================================================
//	set/query/delete registry variable.

	  static int setRegVar(const CCharString& subKey, const CCharString& variable,
						   const CCharString& value,  const DWORD      &W_type,
						   HKEY hKey);

	  static int queryRegVar(const CCharString& subKey, 
		                     const CCharString& variable, 
							 CCharString& valueS,  
							 unsigned long &valueU, DWORD &W_type,
							 HKEY hKey);

	  static int deleteRegVar(const CCharString& subKey, 
		                      const CCharString& variable, 
							  HKEY hKey);
#endif


};



#endif

