#ifndef __C2DPointArray__
#define __C2DPointArray__
#include "2DPoint.h"
#include "2DPointPtrArray.h"
#include "MUObjectArray.h"
#include "Serializable.h"

class StructuredFileSection;

class C2DPointArray : public CMUObjectArray<C2DPoint, C2DPointPtrArray>, public CSerializable
{
public:
	C2DPointArray(int nGrowBy = 0);
	C2DPointArray(const C2DPointArray &ptarray);
	~C2DPointArray(void);

	// serialize functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	bool isModified();
	void reconnectPointers();
	void resetPointers();

	C2DPointArray &operator = (const C2DPointArray &ptarray);

	C2DPoint* Add();
	C2DPoint* Add(const C2DPoint& point);
	
	bool isa(string& className) const
	{
		if (className == "C2DPointArray")
			return true;

		return false;
	};
	string getClassName() const {return string("C2DPointArray");};

};
#endif
