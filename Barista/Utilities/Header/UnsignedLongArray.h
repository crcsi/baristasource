#ifndef __CUnsignedLongArray__
#define __CUnsignedLongArray__

#include "UnsignedLongPtrArray.h"
#include "UnsignedLong.h"
#include "MUObjectArray.h"

class CUnsignedLongArray : public CMUObjectArray<CUnsignedLong, CUnsignedLongPtrArray>
{
public:
	CUnsignedLongArray(int nGrowBy = 0);
	~CUnsignedLongArray(void);
};

#endif

