#ifndef __CTokenArray__
#define __CTokenArray__

#include "Token.h"
#include "TokenPtrArray.h"
#include "MUObjectArray.h"

class CTokenArray : public CMUObjectArray<CToken, CTokenPtrArray>
{
public:
	CTokenArray(int nGrowBy = 0);
	~CTokenArray(void);
};

#endif

