#ifndef __CPointerPtrArray__
#define __CPointerPtrArray__

#include "Pointer.h"
#include "MUObjectPtrArray.h"

class CPointerPtrArray : public CMUObjectPtrArray<CPointer>
{

public:

	CPointerPtrArray(int nGrowBy = 0);
	~CPointerPtrArray(void);
};
#endif
