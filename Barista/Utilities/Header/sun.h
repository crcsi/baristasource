#ifndef ___SUN_H___
#define ___SUN_H___

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Institute of Photogrammetry and GeoInformation, Leibniz Universitaet Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <time.h>
#include "2DPoint.h"
#include "3DPoint.h"

//============================================================================

class sun
{
  
//============================================================================

  public: 

//============================================================================

	  static double getTimeSeconds(struct tm *timeStruct);

	  static double getTimeHours(struct tm *timeStruct);
 

//============================================================================

	  static void computeEphemeries(struct tm *timeStruct, 
	                                double &rightAscension, double &declination, 
                                    double &gha, double &semidiam);

// All times are UTC!
//============================================================================
   
	  static C2DPoint getHorizonCoordinates(struct tm *timeStruct, 
											double longitude, double latitude,
											double meridianConvergence = 0.0);

// The resulting point's x co-ordinate will be the azimuth,
// the y co-ordinate will be the zenith angle, both in radiants
// longitude, latitude, meridianConvergence have to be given in [degrees]
//============================================================================

  
	  static C3DPoint getDirectionVector(struct tm *timeStruct, 
	                                     double longitude, double latitude,
                                         double meridianConvergence = 0.0);

// The unit vector pointing from the sun.
// longitude, latitude, meridianConvergence have to be given in [degrees]
//============================================================================

	  static double getRefraction(double elevation, double temperature, double pressure);

// Refraction correction in seconds from elevation, temperature &pressure
// elevation is given in degrees
//============================================================================

};

#endif