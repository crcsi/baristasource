//////////////////////////////////////////////////////////////////////////////
// CMUObjectPtrArray class
//
#ifndef __CMUObjectPtrArray__
#define __CMUObjectPtrArray__

#include <assert.h>
#include <memory.h>
#include <stdlib.h>
#include <string.h>

#ifndef NULL
#define NULL 0
#endif

#ifndef Max 
#define Max(a,b) (((a) > (b) ) ? (a) : (b))
#endif

#ifndef Min 
#define Min(a,b) (((a) < (b) ) ? (a) : (b))
#endif

template<class TYPE>
class CMUObjectPtrArray
{
// Data
protected:
 TYPE** m_pData;  // the actual array of data
 int m_nSize;  // # of elements (upperBound - 1)
 int m_nMaxSize;  // max allocated
 int m_nGrowBy;  // grow amount

public:
// Construction / Destruction
 CMUObjectPtrArray(int nGrowBy = 0);
 CMUObjectPtrArray(const CMUObjectPtrArray<TYPE>& src);
 virtual ~CMUObjectPtrArray();

// Attributes
 virtual int GetSize() const { return m_nSize; }
 virtual int GetUpperBound() const { return m_nSize-1; }
 virtual int GetGrowBy() const { return m_nGrowBy; }
 virtual void SetSize(int nNewSize, int nGrowBy = -1);
 virtual void SetGrowBy(int nGrowBy) { m_nGrowBy = nGrowBy; }

// Operations
 // Clean up
 virtual void FreeExtra();
	virtual void RemoveAll();

	// Accessing elements
	virtual TYPE* GetAt(int nIndex) const;
	virtual void SetAt(int nIndex, TYPE* newElement);
	virtual TYPE*& ElementAt(int nIndex);
	virtual int indexOf(const TYPE* pElement) const;

	// Direct Access to the element data (may return NULL)
	virtual const TYPE** GetData() const { return (const TYPE**)m_pData; }
	virtual TYPE** GetData() { return (TYPE**)m_pData; }

	// Potentially growing the array
	virtual void SetAtGrow(int nIndex, TYPE* newElement);
	virtual int Add(TYPE* newElement);
	virtual int Append(const CMUObjectPtrArray<TYPE>& src);
	virtual void Copy(const CMUObjectPtrArray<TYPE>& src);

	// overloaded operator helpers
	virtual TYPE* operator[](int nIndex) const;
	virtual TYPE*& operator[](int nIndex);
	virtual const CMUObjectPtrArray<TYPE>& operator=(const CMUObjectPtrArray<TYPE>& src);

	// Operations that move elements around
	virtual void InsertAt(int nIndex, TYPE* newElement, int nCount = 1);
	virtual void RemoveAt(int nIndex, int nCount = 1);
	virtual void InsertAt(int nStartIndex, CMUObjectPtrArray<TYPE>* pNewArray);
	virtual void Swap(int FirstIndex, int SecondIndex);
	virtual void Move(int ToIndex, int FromIndex);
};

template<class TYPE>
inline int CMUObjectPtrArray<TYPE>::indexOf(const TYPE* pElement) const
{
	if( m_pData == NULL )
		return -1;

	for (int i = 0; i < this->GetSize(); i++)
	{
		if (pElement == this->GetAt(i))
			return i;
	}

	return -1;
}

	
template<class TYPE>
inline void CMUObjectPtrArray<TYPE>::RemoveAll()
{
	if( m_pData == NULL )
		return;

	// shrink to nothing
	delete[] (unsigned char*)m_pData;
	m_pData = NULL;
	m_nSize = m_nMaxSize = 0;
}

template<class TYPE>
inline TYPE* CMUObjectPtrArray<TYPE>::GetAt(int nIndex) const
{
	assert(nIndex >= 0 && nIndex < m_nSize);

	return m_pData[nIndex];
}

template<class TYPE>
inline void CMUObjectPtrArray<TYPE>::SetAt(int nIndex, TYPE* newElement)
{
	assert(nIndex >= 0 && nIndex < m_nSize);

	m_pData[nIndex] = newElement;
}

template<class TYPE>
inline TYPE*& CMUObjectPtrArray<TYPE>::ElementAt(int nIndex)
{
	assert(nIndex >= 0 && nIndex < m_nSize);

	return m_pData[nIndex];
}

template<class TYPE>
inline TYPE* CMUObjectPtrArray<TYPE>::operator[](int nIndex) const
{ return GetAt(nIndex); }

template<class TYPE>
inline const CMUObjectPtrArray<TYPE>& CMUObjectPtrArray<TYPE>::operator=(const CMUObjectPtrArray<TYPE>& src)
{ 
	Copy(src);
	return *this; 
}

template<class TYPE>
inline TYPE*& CMUObjectPtrArray<TYPE>::operator[](int nIndex)
{ return ElementAt(nIndex); }

template<class TYPE>
inline int CMUObjectPtrArray<TYPE>::Add(TYPE* newElement)
{
	int nIndex = m_nSize;
	SetAtGrow(nIndex, newElement);
	return nIndex;
}

//////////////////////////////////////////////////////////////////////////////
// Constructor / Destructor
//
template<class TYPE>
CMUObjectPtrArray<TYPE>::CMUObjectPtrArray(int nGrowBy)
{
	m_pData = NULL;
	m_nSize = m_nMaxSize = 0;
	m_nGrowBy = nGrowBy;
}

template<class TYPE>
CMUObjectPtrArray<TYPE>::CMUObjectPtrArray(const CMUObjectPtrArray<TYPE>& src)
{
	m_pData = NULL;
	m_nSize = src.m_nSize;
	m_nMaxSize = src.m_nMaxSize;
	m_nGrowBy = src.m_nGrowBy;

	Copy(src);
}

template<class TYPE>
CMUObjectPtrArray<TYPE>::~CMUObjectPtrArray()
{
	RemoveAll();
}

//////////////////////////////////////////////////////////////////////////////
// CMUObjectPtrArray functions
//
template<class TYPE>
void CMUObjectPtrArray<TYPE>::SetSize(int nNewSize, int nGrowBy)
{
	assert(nNewSize >= 0);

	if( nGrowBy >= 0 )
		m_nGrowBy = nGrowBy;  // set new grow size

	if( nNewSize == 0 )
	{
		RemoveAll();
	}
	else if( m_pData == NULL )
	{
		// set max to grow value
		m_nMaxSize = m_nGrowBy;
		if( nNewSize > m_nMaxSize )
			m_nMaxSize = nNewSize;

		// create pointer array
		m_pData = (TYPE**) new unsigned char[m_nMaxSize * sizeof(TYPE*)];
		assert(m_pData);

		memset(m_pData, 0, m_nMaxSize * sizeof(TYPE*));  // zero fill

  m_nSize = nNewSize;
 }
 else if (nNewSize <= m_nMaxSize)
 {
  // it fits
  if (nNewSize > m_nSize)
  {
   // initialize the new elements
   memset(&m_pData[m_nSize], 0, (nNewSize-m_nSize) * sizeof(TYPE*));
  }

  m_nSize = nNewSize;
 }
 else
 {
  // otherwise, grow array

  // heuristically determine growth
  // (this avoids heap fragmentation in many situations)
  int nGrowBy = Min(1024, Max(4, m_nSize / 8));

  // use the maximum of heuristic and user set growth
  if( m_nGrowBy > nGrowBy )
   nGrowBy = m_nGrowBy;

  int nNewMax = m_nMaxSize + nGrowBy;
  if( nNewMax < nNewSize )
   nNewMax = nNewSize;  // no slush

  assert(nNewMax >= m_nMaxSize);  // no wrap around

		TYPE** pNewData = (TYPE**) new unsigned char[nNewMax * sizeof(TYPE*)];
		assert(pNewData);

		// copy new data from old
		memcpy(pNewData, m_pData, m_nSize * sizeof(TYPE*));

		// zero remaining elements
		assert(nNewSize > m_nSize);

		memset(&pNewData[m_nSize], 0, (nNewSize-m_nSize) * sizeof(TYPE*));

		// get rid of old stuff
		delete[] (unsigned char*)m_pData;
		m_pData = pNewData;
		m_nSize = nNewSize;
		m_nMaxSize = nNewMax;
	}
}

template<class TYPE>
int CMUObjectPtrArray<TYPE>::Append(const CMUObjectPtrArray<TYPE>& src)
{
	assert(this != &src);   // cannot append to itself

	int nOldSize = m_nSize;
	SetSize(m_nSize + src.m_nSize);

	memcpy(m_pData + nOldSize, src.m_pData, src.m_nSize * sizeof(TYPE*));

	return nOldSize;
}

template<class TYPE>
void CMUObjectPtrArray<TYPE>::Copy(const CMUObjectPtrArray<TYPE>& src)
{
	assert(this != &src);   // cannot append to itself

	// are there are no elements in the source?
	if( src.m_nSize <= 0 )
	{
		RemoveAll();
		return;
	}

	// set this array to the size of the other, and copy
	// the data
	SetSize(src.m_nSize);
	memcpy(m_pData, src.m_pData, src.m_nSize * sizeof(TYPE*));
}

template<class TYPE>
void CMUObjectPtrArray<TYPE>::FreeExtra()
{
	if( m_nSize != m_nMaxSize )
	{
		// shrink to desired size
		TYPE** pNewData = NULL;
		if( m_nSize != 0 )
		{
			// allocate new pointer array
			pNewData = (TYPE**) new unsigned char[m_nSize * sizeof(TYPE*)];
			assert(pNewData);

			// copy pointers
			memcpy(pNewData, m_pData, m_nSize * sizeof(TYPE*));
		}

		// get rid of old stuff
		delete[] (unsigned char*)m_pData;
		m_pData = pNewData;
		m_nMaxSize = m_nSize;
	}
}

template<class TYPE>
void CMUObjectPtrArray<TYPE>::SetAtGrow(int nIndex, TYPE* newElement)
{
	assert(nIndex >= 0);

	if (nIndex >= m_nSize)
		SetSize(nIndex+1);
	m_pData[nIndex] = newElement;
}

template<class TYPE>
void CMUObjectPtrArray<TYPE>::InsertAt(int nIndex, TYPE* newElement, int nCount)
{
	assert(nIndex >= 0);    // will expand to meet need
	assert(nCount > 0);     // zero or negative size not allowed

	if (nIndex >= m_nSize)
	{
		// adding after the end of the array
		SetSize(nIndex + nCount);  // grow so nIndex is valid
	}
	else
	{
		// inserting in the middle of the array
		int nOldSize = m_nSize;
		SetSize(m_nSize + nCount);  // grow it to new size

		// shift old data up to fill gap
		memmove(&m_pData[nIndex+nCount], &m_pData[nIndex],
			(nOldSize-nIndex) * sizeof(TYPE*));

		// re-init slots we copied from
		memset(&m_pData[nIndex], 0, nCount * sizeof(TYPE*));

	}

	// insert new value in the gap
	assert(nIndex + nCount <= m_nSize);
	while (nCount--)
		m_pData[nIndex++] = newElement;
}

template<class TYPE>
void CMUObjectPtrArray<TYPE>::RemoveAt(int nIndex, int nCount)
{
	assert(nIndex >= 0);
	assert(nCount >= 0);
	assert(nIndex + nCount <= m_nSize);

	// do nothing?
	if( nCount <= 0 )
		return;

	// just remove a range
	int nMoveCount = m_nSize - (nIndex + nCount);

	if( nMoveCount )
		memcpy(&m_pData[nIndex], &m_pData[nIndex + nCount],
			nMoveCount * sizeof(TYPE*));

	m_nSize -= nCount;
}

template<class TYPE>
void CMUObjectPtrArray<TYPE>::InsertAt(int nStartIndex, CMUObjectPtrArray<TYPE>* pNewArray)
{
	assert(pNewArray != NULL);
	assert(nStartIndex >= 0);

	if (pNewArray->GetSize() > 0)
	{
		InsertAt(nStartIndex, pNewArray->GetAt(0), pNewArray->GetSize());
		for (int i = 0; i < pNewArray->GetSize(); i++)
			SetAt(nStartIndex + i, pNewArray->GetAt(i));
	}
}

template<class TYPE>
inline void CMUObjectPtrArray<TYPE>::Swap(int FirstIndex, int SecondIndex)
{
	assert(FirstIndex >= 0 && FirstIndex < m_nSize);
	assert(SecondIndex >= 0 && SecondIndex < m_nSize);
	if(FirstIndex == SecondIndex)
		return;

	TYPE* pTemp;
	pTemp = m_pData[FirstIndex];
	m_pData[FirstIndex] = m_pData[SecondIndex];
	m_pData[SecondIndex] = pTemp;
}

template<class TYPE>
inline void CMUObjectPtrArray<TYPE>::Move(int ToIndex, int FromIndex)
{
	assert(FromIndex >= 0 && FromIndex < m_nSize);
	assert(ToIndex >= 0 && ToIndex < m_nSize);

	// already there?
	if( FromIndex == ToIndex )
		return;

	// save from pointer
	TYPE* pTemp = m_pData[FromIndex];

	// move pointers
	if( FromIndex < ToIndex )
		memmove(&m_pData[FromIndex], &m_pData[FromIndex+1], 
				(ToIndex-FromIndex) * sizeof(TYPE*));
	else
		memmove(&m_pData[ToIndex+1], &m_pData[ToIndex], 
				(FromIndex-ToIndex) * sizeof(TYPE*));

	// set the new position
	m_pData[ToIndex] = pTemp;
}

#endif //__CMUObjectPtrArray__
