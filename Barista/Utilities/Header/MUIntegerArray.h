// MUIntegerArray.h: interface for the CMUIntegerArray class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CMUIntegerArray__
#define __CMUIntegerArray__

#include "MUIntegerPtrArray.h"

class CMUIntegerArray : public CMUObjectArray<int, CMUIntegerPtrArray>   
{
public:
	CMUIntegerArray(int nGrowBy = 0);
	virtual ~CMUIntegerArray();

	bool AddOnce(int value);
};

#endif // __CMUIntegerArray__
