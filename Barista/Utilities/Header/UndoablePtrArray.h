#ifndef __CUndoablePtrArray__
#define __CUndoablePtrArray__


#include "MUObjectPtrArray.h"

class CUndoable;

class CUndoablePtrArray : public CMUObjectPtrArray<CUndoable>
{

public:

	CUndoablePtrArray(int nGrowBy = 0);
	~CUndoablePtrArray(void);

	void undo();
};
#endif
