// CharString.h: interface for the CCharString class.
//
// Copyright 2000 Harry Handley
//
//////////////////////////////////////////////////////////////////////

#ifndef __CCharString__
#define __CCharString__

#include <stdarg.h>
#include <assert.h>
#include <fstream>
using namespace std;

#define FORMAT_MAX_FIELD_LENGTH 256

class CCharString  
{
  protected:

	  // data members
	  char* m_pChar;
	  long  m_BufferSize;

  public:					

	CCharString();
	CCharString(long number, int digits, bool leadingZeroes);
	//CCharString(unsigned long number, int digits, bool leadingZeroes);
	CCharString(double number, int digits, int precision);
	//addition >>
	CCharString(long number, int digits, bool leadingZeroes, bool sign);	
	CCharString(double number, int digits, int precision, bool leadingZeroes, bool sign);
	void setScientificNumber(double number, int precision, bool leadingZeroesInPower = false, int MaxPowerDigits = 1);
	//<< addition
	CCharString(const char* pChar);
	CCharString(const CCharString &String);
	CCharString(const string &String);

	virtual ~CCharString();

	
	bool			IsEmpty() const;
	int				GetLength() const;
	void			Empty();
	bool			MakeLower();
	bool			MakeUpper();
	bool			CompareNoCase(const char* pChar) const;
	bool			CompareNoCase(const CCharString& pString) const;

	char &operator[] (int index) { assert(index >= 0 && index < this->m_BufferSize); return this->m_pChar[index]; };
	const char &operator[] (int index) const { assert(index >= 0 && index < this->m_BufferSize); return this->m_pChar[index]; };

	void			Format(const char* pFmtString, ...);

	bool			operator==(const CCharString& String);
	bool			operator==(const char* String);
	bool			operator!=(const CCharString& String);
					operator const char*() const;
	CCharString&	operator=(const char* pChar);
	CCharString&	operator+=(const char* pChar);
	CCharString&	operator=(const CCharString& String);
	CCharString&	operator+=(const CCharString& String);
	CCharString&	operator+=(int c);

	friend CCharString	operator+(const CCharString &String1, const CCharString &String2);
	friend CCharString	operator+(const CCharString &String1, const char *pChar2);
	friend CCharString	operator+(const char *pChar1, const CCharString &String2);

	const char* GetChar() const;
	int Find(char c) const;
	//addition >>
	int FindNextNumberLength();
	//<< addition
	int Find(char* pChar) const;
	int Find(const CCharString& String) const;
	int ReverseFind(char c) const;
	CCharString Left(int n) const;
	CCharString Right(int n) const;
	CCharString Mid(int l, int r) const;
	void CentreJustify(int cols);
	void trim();
	void trimCharacters();

	bool replace(const CCharString &findStr, const CCharString &replaceByStr);

	void addInteger(int i, int digits = 0);

	static CCharString getTimeString();

	static CCharString getDateString();

	bool isLessThan(CCharString& str);
	bool isGreaterThan(CCharString& str);

	bool isNumber();

  protected:

	  bool set(const char *pChar);
	  bool set(const CCharString& String);


  private:				
					void _NewBuffer(int length);
					void _Init();
	void			_FormatCore(const char* pFmtString, va_list& marker);
	void			_CoreAddChars(const char* s, int howmany);
	bool			_FormatOneValue (const char*& FmtString, va_list& marker);
	void			_AddChar(char ch);

};

ostream& operator<<(ostream& s, const CCharString& rString);

#endif // __CCharString__
