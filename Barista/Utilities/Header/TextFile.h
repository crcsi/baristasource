// TextFile.h: interface for the CTextFile class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CTextFile__
#define __CTextFile__

#include <fstream>
using namespace std;

class CCharString;
class Matrix;
class CTextFile : public fstream
{

private:
	int m_nMode;

public:
	static int READ;
	static int WRITE;
	static int APPEND;

public:
	CTextFile( const char* szName, int nMode);
	virtual ~CTextFile();

	bool WriteLine(CCharString& Record);
	bool WriteLine(const char* pRecord);
	bool WriteLine(Matrix& m);
	bool ReadLine(CCharString& Record);
};

#endif // __CTextFile__
