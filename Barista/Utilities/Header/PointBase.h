#ifndef __CPOINTBASE_H__
#define __CPOINTBASE_H__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include <assert.h>

class CCharString;

//*****************************************************************************
/*!
 * \brief
 * Generic base class for points as an n-tuple of coordinates.
 * Only derived classes should be instantiated. 
 * Thus, the constructors are protected or even private.
 */
//*****************************************************************************

class CPointBase
{
private:
	//! The default constructor is private because CPointBase requires the 
	//! dimensionality of the point as an input - it must not be used by
	//! any client or derived class.
	CPointBase();

public:
	//! integer point label, can be used for various things by clients
	int id;

	//! a double field to store additional information
	double dot;

protected:

	//! dimension of the point; number of coordinates
	int dim;

	//! the point coordinates, an array with dim entries
	double *coordinates;	

	//! Constructor: dimension coordinates are initialised by 0 
	CPointBase(int dimension);
	//! Copy constructor
	CPointBase(const CPointBase& point);
	//! Copy constructor (pointer)
	CPointBase(const CPointBase* point);

public:


	//! destructor
	virtual ~CPointBase(void);

	// get functions
	virtual int getDimension() const { return this->dim;};
	virtual int getID() const {return this->id;};
	virtual double getDot() const {return this->dot;};
	virtual double getCoordinateAt(int index) const {assert(index >= 0 && index < this->dim); return this->coordinates[index];};

	// set functions
	//! set id field
	virtual void setID(int id) {this->id = id;};
	//! set dot field
	virtual void setDot(double dot) {this->dot = dot;};

	
	//! operators
	virtual CPointBase &operator = (const CPointBase & point);
	double &operator[] (int index) { assert(index >= 0 && index < this->dim); return this->coordinates[index]; };
	const double &operator[] (int index) const { assert(index >= 0 && index < this->dim); return this->coordinates[index]; };

	//! create string where each number has width characters and the precision is prec.
	CCharString getString(CCharString delim, int width, int prec) const;

};

#endif


