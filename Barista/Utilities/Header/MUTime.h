// MUTime.h: interface for the CMUTime class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CMUTime__
#define __CMUTime__

#include <time.h>
#include "CharString.h"

#define MAX_MUTIME_STRING_LENGTH 256
class CMUTime  
{

private: 
	
	time_t m_Time;

public:
	bool SetFromString(const char* fmt, const char *datestr);
	CMUTime();
	CMUTime(CMUTime& src);
	CCharString Format(const char* fmt);
	virtual ~CMUTime();

	static CMUTime GetCurrentTime();
};

#endif // __CMUTime__
