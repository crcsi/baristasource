#ifndef __CStack__
#define __CStack__
#include "StructuredFileSectionPtrArray.h"

#define MAX_STACK 5

class CStack : public CStructuredFileSectionPtrArray
{

public:
    CStack();
    ~CStack();
    
    void clearStack();

    void push(CStructuredFileSection* pAddSection);
    CStructuredFileSection* pop();
};

#endif
