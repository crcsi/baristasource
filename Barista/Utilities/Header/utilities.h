#ifndef __Utilities__
#define __Utilities__

#ifndef NULL
	#define NULL 0
#endif

#ifndef M_PI
	#define M_PI    3.1415926535897932384626433832795  // PI
#endif 

#ifndef R2D
	#define R2D 57.295779513082320876798154814105   // Radiants -> Degrees
#endif

#ifndef D2R
	#define D2R 0.017453292519943295769236907684886 // Degrees -> Radiants
#endif

#ifndef Round
	#define Round(number, digits) floor(number * pow( 10.f, digits) + 0.5) * pow(10.f, -digits)
#endif

#endif // __Utilities__

