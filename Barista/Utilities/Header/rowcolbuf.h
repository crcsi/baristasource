#ifndef __CRowColBuf__
#define __CRowColBuf__

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//****************************************************************************
//****************************************************************************

template < class T> class CRowColBuf
{

  protected:

	  T ***buffer;

	  unsigned long width, height, nElements;

  public:

//============================================================================

	  CRowColBuf() : buffer(0), width(0), height(0), nElements(0) {  };

//============================================================================

	  CRowColBuf(unsigned long h, unsigned long w, unsigned long n) : buffer(0) 
	  { this->resize(h, w, n); };


//============================================================================
 
	  CRowColBuf(unsigned long h, unsigned long w, unsigned long n, const T &value): buffer(0) 
	  { this->resize(h, w, n, value); };

//============================================================================
 	
	  CRowColBuf( const CRowColBuf< T> &source ): width(source.width), height(source.height),
		                                          nElements(source.nElements), buffer(0)
	  {
		  this->buffer = new T**[this->height];
		  if (this->buffer)
		  {
			  memset(buffer, 0, this->height * sizeof(T));
			  int l = this->width * sizeof(T);

			  bool OK = true;
			  for (unsigned long r = 0; OK && r < this->height; ++r)
			  {
				  this->buffer[r] = new T*[this->width];
				  if (!this->buffer[r]) OK = false;
				  else
				  {
					  memset(buffer[r], 0, l);
					  for (unsigned long c = 0; OK && c < this->width; ++c)				
					  {						
						  this->buffer[r][c] = new T[this->nElements];
						  if (!this->buffer[r][c]) OK = false;
						  else for (unsigned long e = 0; e < this->nElements; ++e)				
						  {
							  this->buffer[r][c][e] = source.buffer[r][c][e];					
						  }
					  }
				  }
			  }
			  if (!OK) this->clear();
		  }
	  };


//============================================================================

	  virtual ~CRowColBuf() { this->clear(); };  


//============================================================================

	  unsigned long getWidth() const     { return this->width; };

	  unsigned long getHeight() const    { return this->height; };

	  unsigned long getNElements() const { return this->nElements;};


//============================================================================
//======================= check if memory is allocated =======================
//      return value : 0 : memory is allocated (length > 0)
//                     1 : no memory is allocated (length = 0)

	  int operator ! () const { return buffer == 0; };


//============================================================================

	  CRowColBuf < T> &operator = ( const CRowColBuf< T > &source)
	  {
		  if (this != &source)
		  {
			  this->clear();
			  this->buffer = new T**[this->height];
			  if (this->buffer)
			  {
				  memset(buffer, 0, this->height * sizeof(T));
				  int l = this->width * sizeof(T);

				  bool OK = true;
				  for (unsigned long r = 0; OK && r < this->height; ++r)
				  {
					  this->buffer[r] = new T*[this->width];
					  if (!this->buffer[r]) OK = false;
					  else
					  {
						  memset(buffer[r], 0, l);
						  for (unsigned long c = 0; OK && c < this->width; ++c)				
						  {						
							  this->buffer[r][c] = new T[this->nElements];
							  if (!this->buffer[r][c]) OK = false;
							  else for (unsigned long e = 0; e < this->nElements; ++e)				
							  {
								  this->buffer[r][c][e] = source.buffer[r][c][e];					
							  }
						  }
					  }
				  }
				  if (!OK) this->clear();
			  }
		  }

		  return *this;
	  };

//============================================================================

	  void setValues ( const CRowColBuf< T > &source)
	  {
		 for (unsigned long r = 0; r < this->height; ++r)
		 {
			 for (unsigned long c = 0; c < this->width; ++c)				
			{						
				for (unsigned long e = 0; e < this->nElements; ++e)				
				{
					this->buffer[r][c][e] = source.buffer[r][c][e];									
				}
			 }
		 }
	  };

//============================================================================
 
	  T *&operator() ( unsigned long row, unsigned long col) const 
	  { return this->buffer[row][col]; };

	  T &operator() ( unsigned long row, unsigned long col, unsigned long el ) const
	  { return this->buffer[row][col][el]; };

//============================================================================

	  T getInterpolatedValue(float row, float col, unsigned long el) const
	  {
		  int r = int(floor(row));
		  int c = int(floor(col));
		  row -= r; col -= c;
		  float v00 = this->buffer[r    ][c    ][el];
		  float v01 = this->buffer[r    ][c + 1][el];
		  float v10 = this->buffer[r + 1][c    ][el];
		  float v11 = this->buffer[r + 1][c + 1][el];
		  v01 -= v00; v10 -= v00; v11 -= v00;

		  T v = T(v00 + v10 * row + v01 * col + (v11 - v01 - v10) * row * col);

		  return v;
	  };

//============================================================================

	  bool copyRow(CRowColBuf<T> &target, unsigned long row) const
	  {
		  bool res = target.resize(1, this->width, this->nElements);
		  if (res)
		  {
			  for (unsigned long col = 0; col < this->width; ++col)
			  {
				  for (unsigned long el = 0; el < this->nElements; ++el)
				  {
					  target.buffer[0][col][el] = this->buffer[row][col][el];
				  }
			  }
		  }

		  return res;
	  };

//============================================================================
 
	  bool copyCol(CRowColBuf< T > &target, unsigned long col) const
	  {
		  bool res = target.resize(this->height, 1, this->nElements);
		  if (res)
		  {
			  for (unsigned long row = 0; row < this->height; ++row)
			  {
				  for (unsigned long el = 0; el < this->nElements; ++el)
				  {
					  target.buffer[row][0][el] = this->buffer[row][col][el];
				  }
			  }
		  }

		  return res;
	  };

//============================================================================
 
	  void addRow(CRowColBuf< T > &source, unsigned long row) const
	  {
		 for (unsigned long col = 0; col < this->width; ++col)
		 {
			  for (unsigned long el = 0; el < this->nElements; ++el)
			  {
				  this->buffer[row][col][el] += source.buffer[0][col][el];
			  }
		  }
	  };

//============================================================================
 
	  void addCol(CRowColBuf< T > &source, unsigned long col) const
	  {
		  for (unsigned long row = 0; row < this->height; ++row)
		  {
			  for (unsigned long el = 0; el < this->nElements; ++el)
			  {
				  this->buffer[row][col][el] += source.buffer[row][0][el];
			  }
		  }
	  };


//============================================================================

	  void initialize( const T& value ) const
	  {
		  for (unsigned long row = 0; row < this->height; ++row)
		  {
			  for (unsigned long col = 0; col < this->width; ++col)
			  {
				  for (unsigned long el = 0; el < this->nElements; ++el)
				  {
					  this->buffer[row][col][el] = value;
				  }
			  }
		  }
	  };

//============================================================================

	  bool resize( unsigned long h, unsigned long w, unsigned long n)
	  {		
		  bool OK = true;
		  this->clear();
		  this->height = h;
		  this->width = w;
		  this->nElements = n;

		  this->buffer = new T**[this->height];
			 
		  if (!this->buffer) OK = false;
		  else
		  {
			  memset(buffer, 0, this->height * sizeof(T));
			  int l = this->width * sizeof(T);
		 
			  for (unsigned long r = 0; OK && r < this->height; ++r)
			  {
				  this->buffer[r] = new T*[this->width];
				  if (!this->buffer[r]) OK = false;
				  else
				  {
					  memset(buffer[r], 0, l);
					  for (unsigned long c = 0; OK && c < this->width; ++c)				
					  {						
						  this->buffer[r][c] = new T[this->nElements];
						  if (!this->buffer[r][c]) OK = false;
					  }
				  }
			  }
		  }

		  if (!OK) this->clear();

		  return OK;
	  }

//============================================================================

	  bool resize( unsigned long h, unsigned long w, unsigned long n, const T &value)
	  {		
		  bool OK = true;
		  this->clear();
		  this->height = h;
		  this->width = w;
		  this->nElements = n;

		  this->buffer = new T**[this->height];

		  if (!this->buffer) OK = false;
		  else
		  {
			  memset(buffer, 0, this->height * sizeof(T));
			  int l = this->width * sizeof(T);
			  for (unsigned long r = 0; OK && r < this->height; ++r)
			  {
				  this->buffer[r] = new T*[this->width];
				  if (!this->buffer[r]) OK = false;
				  else
				  {
					  memset(buffer[r], 0, l);
					  for (unsigned long c = 0; OK && c < this->width; ++c)				
					  {						
						  this->buffer[r][c] = new T[this->nElements];
						  if (!this->buffer[r][c]) OK = false;
						  else for (unsigned long e = 0; e < this->nElements; ++e)				
						  {
							  this->buffer[r][c][e] = value;					
						  }
					  }
				  }
			  }
		  }

		  if (!OK) this->clear();

		  return OK;
	  };

//============================================================================

  protected:

	void clear()
	{
		if (this->buffer)
		{
			for (unsigned long r = 0; r < this->height; ++r)  
			{
				if (this->buffer[r])
				{
					for (unsigned long c = 0; c < this->width; ++c)							
					{
						if (this->buffer[r][c]) delete [] this->buffer[r][c];
						this->buffer[r][c] = 0;
					 }  

					delete [] this->buffer[r];
					this->buffer[r] = 0;
				}
			}
			
			delete [] this->buffer;

			this->height = 0;
			this->width = 0;
			this->nElements = 0;
			this->buffer = 0;
		}
	};

};

//****************************************************************************

#endif
