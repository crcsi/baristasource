#ifndef __doubles__
#define __doubles__

#include <vector>
using namespace std;

class doubles : public vector<double>
{
public:
	doubles(void);
	doubles(int n);
	doubles(int n, double val);

	unsigned int size() const
	{
		return vector<double>::size();
	};

	void resize(int n)
	{
		if (n <= 0)
			return;

		vector<double>::resize(n);

		for (int i = 0; i < n; i++)
			this->at(i) = 0.0;

	};

	~doubles(void);

	void sort();

	void sort(doubles &indices);

	void swapElements(long i1, long i2);

	double round(double num);

  protected:	
    void PartialSort(long low, long high);
	void PartialSort(long low, long high, doubles &indices);
};

#endif
