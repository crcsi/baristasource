#ifndef __C2DPointHelper__
#define __C2DPointHelper__

class C2DPoint;

class C2DPointHelper
{
public:
	C2DPointHelper(void);
	~C2DPointHelper(void);

	double distance(const C2DPoint &pt1, const C2DPoint &pt2);
};
#endif
