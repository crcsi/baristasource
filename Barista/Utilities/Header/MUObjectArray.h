//////////////////////////////////////////////////////////////////////////////
// CMUObjectArray class
//
#ifndef __CMUObjectArray__
#define __CMUObjectArray__

#include "MUObjectPtrArray.h"

template<class TYPE, class PTRARRAY>
class CMUObjectArray
{
// Implementation
protected:
	PTRARRAY	m_PointerArray;

	virtual void SetPtrArraySize(int nNewSize, int nGrowBy = -1);

public:
// Construction / Destruction
	CMUObjectArray(int nGrowBy = 0);
	CMUObjectArray(int dim,int nGrowBy);
	CMUObjectArray(const CMUObjectArray<TYPE, PTRARRAY>& Source);
	CMUObjectArray(const PTRARRAY& Source);
	virtual ~CMUObjectArray();

// Attributes
	virtual int GetSize() const { return m_PointerArray.GetSize(); }
	virtual int GetUpperBound() const { return m_PointerArray.GetUpperBound(); }
	virtual int	GetGrowBy() const { return m_PointerArray.GetGrowBy(); }
	virtual void SetGrowBy(int nGrowBy) { m_PointerArray.SetGrowBy(nGrowBy); }

// Operations
	// Clean up
	virtual void FreeExtra() { m_PointerArray.FreeExtra(); }
	virtual void RemoveAll();

	// Accessing elements
	virtual TYPE* GetAt(int nIndex) const { return m_PointerArray.GetAt(nIndex); }
	virtual void SetAt(int nIndex, TYPE& newElement);
	virtual TYPE& ElementAt(int nIndex) const { return *m_PointerArray.GetAt(nIndex); }
	virtual int indexOf(const TYPE* pElement) const { return m_PointerArray.indexOf(pElement); }

	// Direct Access to the element data (may return NULL)
	virtual const TYPE** GetData() const { return m_PointerArray.GetData(); }
	virtual TYPE** GetData() { return m_PointerArray.GetData(); }

	// acccess to the protected pointer array
	virtual operator const PTRARRAY&() const { return m_PointerArray; }
	virtual operator const PTRARRAY*() const { return &m_PointerArray; }

	virtual const PTRARRAY& GetPointerArray() const { return m_PointerArray; }

	// Potentially growing the array
	virtual void SetSize(int nNewSize, int nGrowBy = -1);
	virtual TYPE* SetAtGrow(int nIndex, TYPE& newElement);
	virtual TYPE* Add();
	virtual TYPE* Add(const TYPE& newElement);
	virtual TYPE* CreateType() const;
	virtual TYPE* CreateType(const TYPE& newElement) const;
	// overloaded operator helpers
	virtual const TYPE& operator[](int nIndex) const;
	virtual TYPE& operator[](int nIndex);

	virtual const CMUObjectArray<TYPE, PTRARRAY>& Copy(const CMUObjectArray<TYPE, PTRARRAY>& Source);
	virtual const CMUObjectArray<TYPE, PTRARRAY>& operator=(const CMUObjectArray<TYPE, PTRARRAY>& Source)
			{ return Copy(Source); }

	virtual const CMUObjectArray<TYPE, PTRARRAY>& Copy(const PTRARRAY& Source);
	virtual const CMUObjectArray<TYPE, PTRARRAY>& operator=(const PTRARRAY& Source)
			{ return Copy(Source); }

	// Operations that move elements around
	virtual void InsertAt(int nIndex, const TYPE& newElement, int nCount = 1);
	virtual void RemoveAt(int nIndex, int nCount = 1);
	virtual void Remove(TYPE* pElement);
	virtual void Swap(int FirstIndex, int SecondIndex) { m_PointerArray.Swap(FirstIndex, SecondIndex); }
	virtual void Move(int ToIndex, int FromIndex) { m_PointerArray.Move(ToIndex, FromIndex); }
};

template<class TYPE, class PTRARRAY>
inline const TYPE& CMUObjectArray<TYPE, PTRARRAY>::operator[](int nIndex) const
{
	return *m_PointerArray.GetAt(nIndex);
}

template<class TYPE, class PTRARRAY>
 inline TYPE* CMUObjectArray<TYPE, PTRARRAY>::CreateType () const
{
	return new TYPE;
}

template<class TYPE, class PTRARRAY>
 inline TYPE* CMUObjectArray<TYPE, PTRARRAY>::CreateType (const TYPE& newElement) const
{
	return new TYPE(newElement);
}

template<class TYPE, class PTRARRAY>
inline TYPE& CMUObjectArray<TYPE, PTRARRAY>::operator[](int nIndex)
{
	return *m_PointerArray.GetAt(nIndex);
}

template<class TYPE, class PTRARRAY>
inline void CMUObjectArray<TYPE, PTRARRAY>::SetAt(int nIndex, TYPE& newElement)
{ 
	*m_PointerArray.GetAt(nIndex) = newElement;
}

template<class TYPE, class PTRARRAY>
inline TYPE* CMUObjectArray<TYPE, PTRARRAY>::Add(const TYPE& newElement)
{
	TYPE* pNewElement = this->CreateType(newElement);
	assert(pNewElement);

	m_PointerArray.Add(pNewElement);

	return pNewElement;
}

template<class TYPE, class PTRARRAY>
inline TYPE* CMUObjectArray<TYPE, PTRARRAY>::Add()
{
	TYPE* pNewElement = CreateType();
	assert(pNewElement);

	m_PointerArray.Add(pNewElement);

	return pNewElement;
}

//////////////////////////////////////////////////////////////////////////////
// Constructor / Destructor
//
template<class TYPE, class PTRARRAY>
CMUObjectArray<TYPE, PTRARRAY>::CMUObjectArray(int nGrowBy)
	: m_PointerArray(nGrowBy)
{
}

template<class TYPE, class PTRARRAY>
CMUObjectArray<TYPE, PTRARRAY>::CMUObjectArray(int dim,int nGrowBy)
	: m_PointerArray(dim,nGrowBy)
{
}

template<class TYPE, class PTRARRAY>
CMUObjectArray<TYPE, PTRARRAY>::CMUObjectArray(const CMUObjectArray<TYPE, PTRARRAY>& Source)
	: m_PointerArray(Source.GetGrowBy())
{
	Copy(Source);
}

template<class TYPE, class PTRARRAY>
CMUObjectArray<TYPE, PTRARRAY>::CMUObjectArray(const PTRARRAY& Source)
	: m_PointerArray(Source.GetGrowBy())
{
	Copy(Source);
}

template<class TYPE, class PTRARRAY>
CMUObjectArray<TYPE, PTRARRAY>::~CMUObjectArray()
{
	RemoveAll();
}

//////////////////////////////////////////////////////////////////////////////
// CMUObjectArray functions
//
template<class TYPE, class PTRARRAY>
void CMUObjectArray<TYPE, PTRARRAY>::RemoveAll()
{
	// sanity
	if( m_PointerArray.GetData() == NULL )
		return;

	// delete all the objects
	for( int i=0; i < m_PointerArray.GetSize(); i++ )
	{
		assert(m_PointerArray.GetAt(i));
		delete m_PointerArray.GetAt(i);
	}

	// delete the memory for the pointer array
	m_PointerArray.RemoveAll();
}

template<class TYPE, class PTRARRAY>
const CMUObjectArray<TYPE, PTRARRAY>& CMUObjectArray<TYPE, PTRARRAY>::Copy(const CMUObjectArray<TYPE, PTRARRAY>& Source)
{
	// clear out this array
	RemoveAll();

	// set this array's size to the sources size
	int ssize = Source.GetSize();
	SetPtrArraySize(ssize);

	// copy the sources data members
	for( register int i=0; i < ssize; i++ )
	{
		const TYPE* pType = Source.GetAt(i);
		assert(pType);

		TYPE* pCopy = new TYPE(*pType);
		assert(pCopy);

		m_PointerArray.SetAt(i, pCopy);
		assert(m_PointerArray.GetAt(i) != NULL);
	}

	return *this;
}

template<class TYPE, class PTRARRAY>
const CMUObjectArray<TYPE, PTRARRAY>& CMUObjectArray<TYPE, PTRARRAY>::Copy(const PTRARRAY& Source)
{
	// clear out this array
	RemoveAll();

	// set this array's size to the sources size
	int ssize = Source.GetSize();
	SetPtrArraySize(ssize);

	// copy the sources data members
	for( register int i=0; i < ssize; i++ )
	{
		const TYPE* pType = Source.GetAt(i);
		assert(pType);

		TYPE* pCopy = new TYPE(*pType);
		assert(pCopy);

		m_PointerArray.SetAt(i, pCopy);
		assert(m_PointerArray.GetAt(i) != NULL);
	}

	return *this;
}

template<class TYPE, class PTRARRAY>
void CMUObjectArray<TYPE, PTRARRAY>::SetPtrArraySize(int nNewSize, int nGrowBy)
{
	assert(nNewSize >= 0);

	// new size less than the current size?
	if( nNewSize < m_PointerArray.GetSize() )
	{
		// delete the objects which will be cut off
		for( int i=nNewSize; i < m_PointerArray.GetSize(); i++ )
		{
			assert(m_PointerArray.GetAt(i));
			delete m_PointerArray.GetAt(i);
		}
	}

	// call pointer array set size
	m_PointerArray.SetSize(nNewSize, nGrowBy);
}

template<class TYPE, class PTRARRAY>
TYPE* CMUObjectArray<TYPE, PTRARRAY>::SetAtGrow(int nIndex, TYPE& newElement)
{
	assert(nIndex >= 0);

	// grow array if necessary
	if( nIndex >= m_PointerArray.GetSize() )
	{
		int oldsize = m_PointerArray.GetSize();

		// set new pointer array size
		SetPtrArraySize(nIndex+1);

		// allocate an object for each new element
		for( int i=oldsize; i < m_PointerArray.GetSize(); i++ )
		{
			TYPE* pNewElement = new TYPE(newElement);
			assert(pNewElement);

			m_PointerArray.SetAt(i, pNewElement);
			assert(m_PointerArray.GetAt(i) != NULL);
		}
	}
	else // just set the object
		*m_PointerArray.GetAt(nIndex) = newElement;

	return m_PointerArray.GetAt(nIndex);
}

template<class TYPE, class PTRARRAY>
void CMUObjectArray<TYPE, PTRARRAY>::SetSize(int nNewSize, int nGrowBy)
{
	assert(nNewSize >= 0);

	// already desired size?
	if( nNewSize == m_PointerArray.GetSize() )
		return;

	// grow array if necessary
	if( nNewSize > m_PointerArray.GetSize() )
	{
		int oldsize = m_PointerArray.GetSize();

		// set new pointer array size
		SetPtrArraySize(nNewSize, nGrowBy);

		// allocate an object for each new element
		for( int i=oldsize; i < m_PointerArray.GetSize(); i++ )
		{
			TYPE* pNewElement =  this->CreateType();
			assert(pNewElement);

			m_PointerArray.SetAt(i, pNewElement);
			assert(m_PointerArray.GetAt(i) != NULL);
		}
	}
	else // just set the pointer array (will delete extra objects)
		SetPtrArraySize(nNewSize, nGrowBy);
}

template<class TYPE, class PTRARRAY>
void CMUObjectArray<TYPE, PTRARRAY>::InsertAt(int nIndex, const TYPE& newElement, int nCount)
{
	// insert the pointer(s)
	m_PointerArray.InsertAt(nIndex, NULL, nCount);

	// allocate the element(s)
	while( nCount-- )
	{
		TYPE* pNewElement = new TYPE(newElement);
		assert(pNewElement);

		m_PointerArray.SetAt(nIndex, pNewElement);
		assert(m_PointerArray.GetAt(nIndex) != NULL);

		nIndex++;
	}
}

template<class TYPE, class PTRARRAY>
void CMUObjectArray<TYPE, PTRARRAY>::RemoveAt(int nIndex, int nCount)
{
	assert(nIndex >= 0);
	assert(nCount >= 0);
	assert(nIndex + nCount <= m_PointerArray.GetSize());

	// do nothing?
	if( nCount <= 0 )
		return;

	// delete the object(s)
	for( int i=nIndex; i < nIndex + nCount; i++ )
		delete m_PointerArray.GetAt(i);

	// remove the pointer(s) 
	m_PointerArray.RemoveAt(nIndex, nCount);
}

template<class TYPE, class PTRARRAY>
void CMUObjectArray<TYPE, PTRARRAY>::Remove(TYPE* pElement)
{
	for(int i = 0; m_PointerArray.GetSize(); i++)
	{
		if(pElement == m_PointerArray.GetAt(i))
		{
			this->RemoveAt(i);
			return;
		}
	}
}

#endif // __CMUObjectArray__
