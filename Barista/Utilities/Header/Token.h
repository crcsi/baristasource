#ifndef __CToken__
#define __CToken__

#include "CharString.h"
#include "doubles.h"
#include "MemoryAllocator.h"

#ifdef BARISTA
#define ALL_PRECISION "%.18e"
#else
#define ALL_PRECISION "%.13e"
#endif 

class CSerializable;

class CToken
{
public:
 CToken(void);
 ~CToken(void);
 CToken(const char* str);
 void setToken(const char* str);
 
 const char* getKey();
 
 const char* getValue();
 double getDoubleValue();
 void getDoubleValues(double* pDoubleArray, int n);
 void getDoublesClass(doubles& pDoublesClass);
 void getVectorClass(vector<int>& intArray);
 int getIntValue();
 unsigned long getUnsignedLongValue();
 CSerializable* getPointerValue();
//	void setKey(CCharString& key);
//	void setKey(const char* pKey);

	void setValue(const char* pKey, const char* pValue);
	void setValue(const char* pKey, double value);
	void setValue(const char* pKey, int value);
	void setValue(const char* pKey, bool value);
	void setPointerValue(const char* pKey, CSerializable* pSerializable);

	bool getBoolValue();

    bool isKey(const char* key);

private:
//	CCharString key;
//	CCharString value;
    char* token;
    char* value;
    char* key;

	void parseToken();
};

#endif

