#ifndef __C2DPointPtrArray__
#define __C2DPointPtrArray__


#include "MUObjectPtrArray.h"
#include "2DPoint.h"

class C2DPointPtrArray : public CMUObjectPtrArray<C2DPoint>
{
public:
	C2DPointPtrArray(int nGrowBy = 0);
	~C2DPointPtrArray(void);
};

#endif


