#ifndef __ZObject__
#define __ZObject__
#include <string>
using namespace std;

class ZObject
{
public:
	ZObject(void){};
	virtual ~ZObject(void){}; // declaring virtual: functionality of a virtual function can be over-ridden in its derived classes. 
	virtual bool isa(std::string& className) const = 0;
	bool isa(const char* className) const // 'const' at the end bans isa from being anything
	// which can attempt to alter any member variables in the class.
	{
		string s(className);
		return this->isa(s);
	};
	
	virtual string getClassName() const = 0;
	

};
#endif
