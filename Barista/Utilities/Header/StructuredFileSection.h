#ifndef __CStructuredFileSection__
#define __CStructuredFileSection__

#include "TokenArray.h"
#include "Filename.h"

class CStructuredFileSectionArray;


class CStructuredFileSection
{
  protected: 

	  CStructuredFileSection* pParent;

	  CStructuredFileSectionArray* pChildren;

	  CTokenArray tokens;

	  CCharString name;

	  CFileName ProjectPath;

  public:

	  CStructuredFileSection();

	  CStructuredFileSection(const CFileName &projectPath);

	  ~CStructuredFileSection();

	  CStructuredFileSection* getParent();

	  void setParent(CStructuredFileSection* pParent);

	  CStructuredFileSectionArray* getChildren();

	  CStructuredFileSection* addChild();

	  CToken* addToken();

	  CTokenArray* getTokens();

	  CCharString getName() const { return this->name; };

	  CFileName getProjectPath() const { return this->ProjectPath; };

	  void setName(const CCharString &Name);

	  void setProjectPath(const CFileName &projectPath);

	  void setName(const char* pName);

	  void setName(const string& Name);

  public:

	  int beg;

	  int end;
};

#endif

