#ifndef __CCharStringPtrArray__
#define __CCharStringPtrArray__

#include "MUObjectPtrArray.h"
#include "CharString.h"

class CCharStringPtrArray : public CMUObjectPtrArray<CCharString>
{

public:

	CCharStringPtrArray(int nGrowBy = 0);
	~CCharStringPtrArray(void);
};
#endif
