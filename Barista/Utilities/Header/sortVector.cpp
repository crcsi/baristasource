#ifndef ____CSortVector_CPP____
#define ____CSortVector_CPP____

#include "sortVector.h"

template<class T> CSortVector<T>::CSortVector()
{
}

template<class T> CSortVector<T>::CSortVector(int n, T initVal)
{
	if (n > 0)
		vector<T>::resize(n);

	for (int i = 0; i < n; i++)
		(*this)[i] = initVal;

}

template<class T> CSortVector<T>::~CSortVector()
{
}

template<class T> void CSortVector<T>::swapElements(long i1, long i2)
{
	T element = (*this)[i2];
	(*this)[i2] = (*this)[i1];
	(*this)[i1] = element;
};

template<class T> void CSortVector<T>::PartialSortAscending(long low, long high)
{
	if ( low < high )   // stopping condition for recursion!
	{
		long lo = low;
		long hi = high + 1;
		if( high-low > 3)
		{ // put median of three at this->pt_data_[low]
			long m=low+(high-low)/2;
			if ((*this)[m] > (*this)[high]) 
				this->swapElements( high, m );
			if ((*this)[m] > (*this)[low]) 
				this->swapElements( low, m );
			else if ((*this)[high] < (*this)[low])
			  this->swapElements( high, low );
		}

		for ( ; ; )
		{
			while ( (*this)[ ++lo ] < (*this)[ low ] );
			while ( (*this)[ --hi ] > (*this)[ low ] );
			if ( lo < hi ) this->swapElements( lo, hi );
			else break;
		}
    
		this->swapElements( low, hi );
		PartialSortAscending( low, hi - 1 );
		PartialSortAscending( hi + 1, high );
	}
};
//***************************************************************************
template<class T> void CSortVector<T>::sortAscending()
{
	// put the greatest value to the last position in the vector:
	long maxI = size() - 1;
	if ( maxI > 0 )
	{
		T maxD = (*this)[ maxI ];
		for ( long i = maxI - 1; i >= 0; --i )
			if ( (*this)[ i ] > maxD ) 
			{ 
				maxI = i; 
				maxD = (*this)[ maxI ]; 
			}
			this->swapElements( maxI, this->size()  - 1);
			PartialSortAscending( 0, size() - 2); 
	}
}
//**************************************************************************
template<class T> void CSortVector<T>::PartialSortDescending(long low, long high)
{
	if ( low < high )   // stopping condition for recursion!
	{
		long lo = low;
		long hi = high + 1;
		T element = (*this)[low];

		for ( ; ; )
		{
			while ((*this)[ ++lo ] > element );
			while ((*this)[ --hi ] < element );
			if ( lo < hi ) this->swapElements( lo, hi );
			else break;
		}
		this->swapElements( low, hi );
		PartialSortDescending( low, hi - 1 );
		PartialSortDescending( hi + 1, high );
	}
};

template<class T> void CSortVector<T>::sortDescending()
{
	// put the greatest value to the last position in the vector:
	long minI = size() - 1;
	if ( minI > 0 )
	{
		T minT = (*this)[ minI ];
		for ( long i = minI - 1; i >= 0; --i )
			if ( (*this)[ i ] < minT ) 
			{ 
				minI = i; 
				minT = (*this)[ minI ]; 
			}
			this->swapElements( minI, this->size()  - 1);
			PartialSortDescending( 0, size() - 2); 
	}
}

template<class T> long CSortVector<T>::getMaxIndex() const
{
	long maxIdx = 0;
  
	T max = (*this)[maxIdx];

  
	for (unsigned long i = 1; i < vector<T>::size(); i++)
    if ((*this)[i] > max) 
	{
		max = (*this)[i];
		maxIdx = i;
	}
  
  return maxIdx;
}

#endif