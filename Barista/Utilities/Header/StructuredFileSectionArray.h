#ifndef __CStructuredFileSectionArray__
#define __CStructuredFileSectionArray__
#include "StructuredFileSectionPtrArray.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
#include "MUObjectArray.h"
#include "MUObjectPtrArray.h"

class CStructuredFileSectionArray : public CMUObjectArray<CStructuredFileSection, CStructuredFileSectionPtrArray>
{
public:
	CStructuredFileSectionArray(int nGrowBy = 0);
	~CStructuredFileSectionArray(void);
};

#endif

