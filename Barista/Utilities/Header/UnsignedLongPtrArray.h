#ifndef __CUnsignedLongPtrArray__
#define __CUnsignedLongPtrArray__

#include "UnsignedLong.h"
#include "MUObjectPtrArray.h"

class CUnsignedLongPtrArray : public CMUObjectPtrArray<CUnsignedLong>
{

public:

	CUnsignedLongPtrArray(int nGrowBy = 0);
	~CUnsignedLongPtrArray(void);
};
#endif
