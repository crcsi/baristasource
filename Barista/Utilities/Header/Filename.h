//////////////////////////////////////////////////////////////////////////////
// FileName.h - CFileName class header
//
#ifndef __CFileName__
#define __CFileName__

#include "CharString.h"

#define FILENAME_MAX_PATH 256

class CFileName : public CCharString
{
  public:
	  CFileName();
	  CFileName(const CCharString& String);
	  CFileName(const CFileName& FileName);
	  CFileName(const char* psz);
	  ~CFileName();

	  CCharString GetPath() const;			// Returns the full path of the file.
	  int  GetDrive() const;
	  CCharString GetFileName() const;		// Returns the filename of the file.
	  CCharString GetFileExt() const;		// Returns the file extension of the file. 
	  CCharString GetFullFileName() const;	// Returns the file name plus extension.
	  CCharString GetPathName() const { return GetPath(); }

	
	  bool SetPath(const CCharString &Path);
	  bool SetFileName(const CCharString & FileName);
	  bool SetFileExt(const CCharString & Ext);
	  bool SetFullFileName(const CCharString & FileName);
	  bool SetPathCurrent();
	  bool IncrementFileName();

	  CFileName &operator=(const CCharString& String);
	  CFileName &operator=(const CFileName& FileName);
	  CFileName &operator=(const char* pChar);

  protected:

	  void makeFilenameCompatbleWithOS();

};

#endif //__CFileName__

