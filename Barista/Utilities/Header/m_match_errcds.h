#ifndef	M_MATCH_ERRCDS
#define	M_MATCH_ERRCDS

#define M_OK             1
#define M_ERROR         -1


#define M_MATCH_OK             1
#define M_MATCH_ERROR         -1

#define M_MATCH_ALLOC_PTS     -2    /* memory allocation in point structure failed */
#define M_MATCH_ALLOC_MTRX    -3   /* memory allocation in matrces failed */
#define M_MATCH_ALLOC_IMG	    -4   /* memory allocation in image structure failed */
#define M_MATCH_OPEN_IMG	    -5   /* memory allocation in image structure failed */
#define M_MATCH_ALLOC_LSMC	-6  /* memory allocation in lsm structure for control failed */
#define M_MATCH_ALLOC_OFFSET  -7  /* memory allocation for offsets failed */
#define M_MATCH_ALLOC_PARAM   -8  /* memory allocation for parameters structure */
#define M_MATCH_NULLDATA		-9 /* null data where should not be     */

#define M_MATCH_COEFF_UNDERTHR	   -10  /* coefficient less than threshold */
#define M_MATCH_ERROR_INTERPOL       -11  /* interpolation failed */
#define M_MATCH_NEGLEVEL			   -12  /* level to process is negative */
#define M_MATCH_ERROR_SENSORMODEL	   -13 /*error in Sensormodel */
#define M_MATCH_ERROR_FILE		   -14  /*error in file*/
#define M_MATCH_ERROR_INTERSECT	   -15  /*error in intersection*/


#define M_PATCH_ERROR			 -20 /*error in patch*/
#define M_RANGE_ERROR			 -21 /*error in range*/

#define M_PROJECT_ERROR        -30 /*error reading project file */
#define M_PROJECT_OK			   1 /*success reading project file */

/* errors related to faulty structure setups */

#define	M_NOIMAGES		-40	/* no image structures		     */
#define	M_NOROIS			-41	/* no roi structures		     */


/* errors relating to lack of data */

#define	M_INSUFFIMG		-50	/* insufficient number of images     */
#define	M_NOPIXELS		-51	/* no pixel data		     */
#define	M_NOCOORDS		-52	/* no coordinate data		     */
#define	M_NOFILFLAG		-54	/* no filter flag for roi	     */

/* errors relating to invalid/inconsistent data */

#define M_INVCRDTYP		-60	/* invalid coordinate type	     */

/* errors relating to function module failures */

#define	M_NOFILTER		-70	/* filtering failed		     */
#define	M_SUBIMAGE		-71	/* true roi passed to filter or radj */
#define	M_NOWINDOW		-78	/* window creation failed	     */

#endif
