#ifndef ___CProtocolHandler___
#define ___CProtocolHandler___

#include <strstream>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "CharString.h"

class CCharStringArray;

 enum eOUTPUTMODE { eMINIMUM = 0, eMEDIUM, eMAXIMUM, eDEBUG };

 extern eOUTPUTMODE outputMode;

class CProtocolHandler
{
  protected:

	  ostream  *scrStream;
	  ofstream *errFile;
	  ofstream *protFile;

 	
	  static CCharString defaultDirectory;

  public:

	  static const int scr;

	  static const int err;

	  static const int prt;

// --- default filenames

	  static const CCharString defaultError; 

	  static const CCharString defaultProt; 

 
  public:

	  CProtocolHandler();

	  ~CProtocolHandler();

	  bool isOpen(int maskOpen) const;

	  bool open(int maskOpen, CCharString name, bool append = true);

  // ========== close the specified output channels ==========
 
	  void close(int maskClose);

  // ========== write on output channels ==========
  // --- default is to write on default channels if they are open.
  // --- use channel mask to force output only on them.

	  void print(const CCharString &text, int mask= 0);

	  void print(ostrstream &textstream, int mask = 0 );

	  CCharString getDefaultDirectory() const { return this->defaultDirectory; };

	  void setDefaultDirectory(const CCharString &dir); 

  protected:

	  bool actError(int mask) const { return ((mask & this->err) && this->errFile);   };
	  bool actProt(int mask)  const { return ((mask & this->prt) && this->protFile);  };
	  bool actScr(int mask)   const { return ((mask & this->scr) && this->scrStream); };
};

 extern CProtocolHandler protHandler;

#endif
