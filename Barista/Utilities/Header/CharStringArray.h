#ifndef _CCharStringArray_
#define _CCharStringArray_

#include "CharStringPtrArray.h"
#include "CharString.h"

#include "MUObjectArray.h"


class CCharStringArray : public CMUObjectArray<CCharString, CCharStringPtrArray>
{
public:
	CCharStringArray(int nGrowBy = 0);
	~CCharStringArray(void);

};
#endif



