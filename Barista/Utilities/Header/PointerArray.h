#ifndef __CPointerArray__
#define __CPointerArray__

#include "PointerPtrArray.h"
#include "Pointer.h"
#include "MUObjectArray.h"


class CPointerArray : public CMUObjectArray<CPointer, CPointerPtrArray>
{
public:
	CPointerArray(int nGrowBy = 0);
	~CPointerArray(void);
};

#endif

