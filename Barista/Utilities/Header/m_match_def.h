/* method for the extraction of edges */
#ifndef ABS
#define ABS(x)         ((x) < 0 ? -(x) : (x))
#endif

#ifndef ODD
#define ODD(x)  (((x) % 2 == 0) ? (0) : (1))
#endif

#ifndef EVEN
#define EVEN(x)  (((x) % 2 != 0) ? (0) : (1))
#endif

#ifndef True
#define True 1
#endif

#ifndef False
#define False 0
#endif

#ifndef MAX
#define MAX(a, b)  (((a) > (b)) ? (a) : (b)) 
#endif

#ifndef MIN
#define MIN(a, b)  (((a) < (b)) ? (a) : (b)) 
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846264
#endif

#if !defined (M_PI_2)
#   define M_PI_2      1.57079632679489661923
#endif

#if !defined (M_PI_4)
#   define M_PI_4      0.78539816339744830962
#endif

#define RADIANS_PER_SEC  (2.0*M_PI/360.0/3600.0)
                         
#ifndef DEG_TO_RAD 
#   define DEG_TO_RAD      (M_PI/180.0)
#   define RAD_TO_DEG      (180.0/M_PI)
#endif

#define FT_PER_METER     3.28083989501312301

#define fsqrt(x) ((float) sqrt ((double) x))
#define fcos(x) ((float) cos ((double) x))
#define fpow(x,y) ((float) pow ((double) (x), (double) (y)))
#define flog(x) ((float) log10 ((double)(x)))

#define SQR(x) ((x)*(x))

#define NO_EDGE   0
#define EDGE  254
#define POSSIBLE_EDGE   128
#define TEXTUR			192
#define CORNER			224
#define CIRCLE			255

#define IROUND(x) (((x) > 0) ? (int)((x) + 0.5) : (int)((x) - 0.5))

#ifndef SGN
#define SGN(a)   (((a)<0) ? -1 : 1)
#endif

#define M_SQRT2_2  0.70710678
#define LN(x)   (log(x)/log(2.718282))
#define fexp(x) ((float) exp ((double) x))


/* extractor to be used*/
#define CORR_CANNY 0
#define CORR_THRESH 1
#define CORR_FOERSTNER 2



/* image for the matching*/
#define CORR_GVALUES 0
#define CORR_BINARY 1
#define CORR_EDGE_ABS_SIGN 2
#define CORR_EDGE_SIGN 3



/* method for the matching grid or edges*/
#define CORR_EDGE_PIX 0
#define CORR_REG_GRID 1

#define M_NO_EDGES  0
#define M_EDGES     1
#define M_EDGES_LSM 2
#define M_NO_PNTS   0
#define M_PNTS      1
#define M_NO_GRID   0
#define M_GRID      1


/* similarity measures */
#define CORR_NORM_CROSS 0
#define CORR_SIM_SQRT 1
#define CORR_SIM_ABS 2


#define UNCONSTRAINED 0
#define CONSTRAINED 1

#define RAY_MATCH	   0
#define RAY_NO_MATCH  -1
#define MULT_SOLUTION  1
#define OCCLUSION      2

#define RAY_SUCCESS    3
#define RAY_REJECTED   4

#define MATCH_SUCCESS  5
#define MATCH_REJECTED 6
#define MATCH_EXCLUDED 7

#define CC_SUCCESS     0

/* flag whether image with directions is stored or not*/
#define DIREC_OFF 0
#define DIREC_ON 1

/*flag whether doublet on or off*/
#define DOUBLET_OFF 0
#define DOUBLET_ON 1

/*flag whether buffer mirrored or not*/
#define MIRROR_OFF 0
#define MIRROR_ON 1

#define ASC 0  // ascii mode
#define BIN 1  // bin mode

#define STRATEGY_SINGLE    0
#define STRATEGY_MULT	   1
#define STRATEGY_ADAPTIVE  2


#define TEMPLATE         0
#define SEARCH           1
#define TEMPL_SRCH       2

#define LSM_OFF          0
#define LSM_ON           1

/* filter flags and sizes */
#ifndef FIL_NONE
#define	FIL_NONE	0		/* no filter applied		     */
#define	FIL_DONE	1		/* filter has been applied	     */

#define	FIL_ZERO	0		/* no filter to be applied	     */
#define	FIL_THREE	1		/* 3 x 3 filter			     */
#define	FIL_FIVE	2		/* 5 x 5 filter			     */
#define	FIL_SEVEN	3		/* 7 x 7 filter			     */

#define FIL_LOWPASS 0		/* low pass filter			*/
#endif


/* order of image to use in matching */

#define	IMGS_ORIG	0		/* original grey value image	     */
#define	IMGS_DERIV	1		/* gradient magnitude image	     */


/* radiometric adjustment: what and where to apply */

#define	RAD_MEAN	0		/* adjust mean value only	     */
#define	RAD_SDEV	1		/* adjust mean and standard devn     */

#define	RAD_NONE	0		/* adjustment never applied	     */
#define	RAD_PRE		1		/* apply as one pre-iteration adj't  */
#define	RAD_ITER	2		/* apply within each iteration	     */

/* roi pixel data sources */

#define	ROI_OWNMEM	0		/* roi pixel data has its own memory */
#define	ROI_SUBIMG	1		/* pixel data is subimage of parent  */

