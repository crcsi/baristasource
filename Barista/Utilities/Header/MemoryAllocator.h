#ifndef __CMemoryAllocator__
#define __CMemoryAllocator__

class CMemoryAllocator
{
public:
	CMemoryAllocator(size_t objectSize, int nObjects = 1000 );
	~CMemoryAllocator(void);

	CMemoryAllocator* pNext;

	unsigned char* newObject();
	void deleteObject(unsigned char* pObject);

	
//	void *operator new( size_t size ); 
//	void operator delete ( void *);


protected:
	unsigned char* pMemBlock;
	size_t objectSize;
	int nObjects;
	int next;
	int* allocList;
	bool full;
};
#endif
