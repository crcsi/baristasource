#ifndef __C3DPointPtrArray__
#define __C3DPointPtrArray__

#pragma once

#include "MUObjectPtrArray.h"
#include "3DPoint.h"

class C3DPointPtrArray : public CMUObjectPtrArray<C3DPoint>
{
public:

	C3DPointPtrArray(int nGrowBy = 0);
	~C3DPointPtrArray(void);
};

#endif
