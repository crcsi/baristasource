#ifndef __C3DPoint__
#define __C3DPoint__

#include "PointBase.h"

#include <math.h>
#include "Serializable.h"


class Matrix;

class C3DPoint : public virtual CSerializable, public virtual CPointBase
{
  public:
	  double &x;
	  double &y;
	  double &z;

  protected:
	
	  bool bSelected;

  public:

	  C3DPoint() : CPointBase(3),
		bSelected (false), x(coordinates[0]), y(coordinates[1]), z(coordinates[2]){};
	  C3DPoint(double I_x, double I_y, double I_z) : CPointBase(3),
		bSelected (false), x(coordinates[0]), y(coordinates[1]), z(coordinates[2]) 
		{ x = I_x; y = I_y; z = I_z;};
	  C3DPoint(double I_x, double I_y, double I_z, double dot) : CPointBase(3),
		bSelected (false), x(coordinates[0]), y(coordinates[1]), z(coordinates[2]) 
		{ x = I_x; y = I_y; z = I_z; this->dot = dot;};
	  C3DPoint(int I_id, double I_x, double I_y, double I_z) : CPointBase(3),
		bSelected (false), x(coordinates[0]), y(coordinates[1]), z(coordinates[2]) 
		{x = I_x; y = I_y; z = I_z; id = I_id;};
	  C3DPoint(int I_id, double I_x, double I_y, double I_z, double dot) : CPointBase(3),
		bSelected (false), x(coordinates[0]), y(coordinates[1]), z(coordinates[2]) 
		{x = I_x; y = I_y; z = I_z; id = I_id; this->dot = dot;};
	  C3DPoint(const C3DPoint &p) : CPointBase(p),
		bSelected (p.bSelected), x(coordinates[0]), y(coordinates[1]), z(coordinates[2]) {};
	  C3DPoint(const CPointBase &p);



	  virtual ~C3DPoint() { };

	  C3DPoint &operator = (const C3DPoint & rhs)
	  {
		  x = rhs.x; y = rhs.y; z = rhs.z; 
		  dot = rhs.dot; id =  rhs.id; bSelected = rhs.bSelected; 
		  return *this;
	  }

  /// unary minus operator
	  C3DPoint operator - () const { return C3DPoint (id, -x, -y, -z ); };

	  /// addition and subtraction of points 
	  void operator += ( const C3DPoint &p ) 
	  {	x += p.x; y += p.y;  z += p.z; }

	  void operator -= ( const C3DPoint &p )
	  {	x -= p.x; y -= p.y; z -= p.z;  };

	  friend C3DPoint operator + ( const C3DPoint &p1, const C3DPoint &p2 )
	  { return C3DPoint(p1.x + p2.x, p1.y + p2.y, p1.z + p2.z); };

	  friend C3DPoint operator - ( const C3DPoint &p1, const C3DPoint &p2 )
	  { return C3DPoint(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z); };


	  /// multiplication and division of points by scalars	
	  void operator *= ( double a )
	  {	x *= a; y *= a; z *= a; };

	  C3DPoint operator * ( double a ) const
	  {	return C3DPoint(id, x * a, y * a, z * a); };
    
	  friend C3DPoint operator * ( double a, const C3DPoint &p )
	  { return  p * a; };
	
	  void operator /= ( double a )
  	  {	x /= a; y /= a; z /= a; };
	
	  C3DPoint operator / ( double a ) const
	  { return C3DPoint(id, x / a, y / a, z / a); };

	  /// geometrical operations / queries

	  C3DPoint outerProduct(const C3DPoint& p) const
	  { return C3DPoint(z * p.y - y * p.z, 
						z * p.x - x * p.z, 
						x * p.y - y * p.z); };

	  C3DPoint crossProduct(const C3DPoint& p) const
	  { return C3DPoint(y * p.z - z * p.y,
						z * p.x - x * p.z,
						x * p.y - y * p.x);};

	  double innerProduct(const C3DPoint& p) const
	  { return x * p.x + y * p.y+ z * p.z; };

	  double getSquareNorm() const { return (x * x + y * y + z * z); };

	  double getNorm() const { return sqrt(getSquareNorm()); };

	  double getSquareNorm2D() const { return (x * x + y * y); };

	  double getNorm2D() const { return sqrt(getSquareNorm2D()); };

	  // serialize functions

	  virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	  virtual bool isModified();
	  virtual void reconnectPointers();
	  virtual void resetPointers();
	
	  virtual  bool isa(string& className) const
	  {
		if (className == "C3DPoint")
			return true;

		return false;
	  };
	
	  virtual string getClassName() const {return string("C3DPoint");};

	  void setSelected(bool status);	
	  bool isSelected() const;


	  void transform(Matrix* pTransMat);

  	  bool writeDXF(ostream& dxf, int *lineCodes, int decimals,
		            double xfactor, double yfactor, double zfactor) const;

};
#endif
