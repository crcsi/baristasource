#ifndef __CHandleManager__
#define __CHandleManager__
#include "PointerArray.h"
#include "UnsignedLongArray.h"
#include "CharStringArray.h"

class CSerializable;

class CHandleManager
{
public:
	CHandleManager(void);
	~CHandleManager(void);

	bool registerPointers(CSerializable* pOldPointer, CSerializable* pNewPointer);
	CSerializable* getNewPointer(CSerializable* pOldPointer);//, const char* className);
	//CSerializable* getNewPointer2(CSerializable* pOldPointer);
	void reset();

private:
	CPointerArray newPointers;
	CPointerArray oldPointers;
	CCharStringArray classNames;

	void sort();
	void quickSort(int L, int R);

	bool sorted;
};
#endif
