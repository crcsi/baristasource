// Rectangle.h: interface for the CRectangle class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CRectangle__
#define __CRectangle__

class C2DPoint;

class CRectangle
{
public:
	CRectangle(void);
	CRectangle(double x_left,double y_top,double x_right,double y_bottom);
	CRectangle(const C2DPoint& left_top,double height, double width);
	CRectangle(const C2DPoint& left_top, const C2DPoint& right_bottom);
	virtual ~CRectangle(void);

	bool isInitialized();
	void Clear();
	void fill(double x_left,double y_top,double x_right,double y_bottom);
	void fill(const C2DPoint& left_top,double height, double width);
	void fill(const C2DPoint& left_top, const C2DPoint& right_bottom);
	bool contains(const C2DPoint& p);
	bool contains(const CRectangle& r);

	double x;
	double y;
	double width;
	double height;

	
protected:
	bool init;

};

#endif // __CRectangle__