#ifndef __CGenericStack_CPP__
#define __CGenericStack_CPP__


#include "GenericStack.h"
/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

template <class T> bool CGenericStackAscending<T>::checkInsertRecord(const T &rec)
{
	bool ret = false;

	if (this->currMax == 0) 
	{ // insert first element
		this->stack[this->currMax] = rec;
		++this->currMax;
		ret = true;
	}
	else if (this->stack[this->currMax - 1] < rec)
	{ // rec is smaller than the last element; only insert it if the stack is not yet full
		if (this->currMax < this->MaxSize())
		{ // the stack is not yet full => add rec at the end
			this->stack[this->currMax] = rec;
			++this->currMax;
			ret = true;
		}
	}
	else 
	{ // rec has to be inserted somewhere in the stack
		int index = -1;
		if (this->stack[0] > rec) 
		{
			index = 0;
			ret = true;
		}
		else
		{
			int imin = 0, imax = this->currMax - 1;

			while (imax - imin > 1)
			{
				int i = (imin + imax) / 2;

				if (this->stack[i] <= rec) imin = i;
				else imax = i;
			}

			index = imin + 1;
		}

		if (this->currMax < this->MaxSize()) ++this->currMax;

		for (int i = this->currMax - 1; i > index; --i)
		{
			this->stack[i] = this->stack[i - 1];
		}

		if (index < this->MaxSize()) 
		{
			this->stack[index] = rec;
			ret = true;
		}
	};

	return ret;
}

template <class T> bool CGenericStackDescending<T>::checkInsertRecord(const T &rec)
{
	bool ret = false;

	if (this->currMax == 0) 
	{ // insert first element
		this->stack[this->currMax] = rec;
		++this->currMax;
		ret = true;
	}
	else if (this->stack[this->currMax - 1] >= rec)
	{ // rec is larger than the last element; only insert it if the stack is not yet full
		if (this->currMax < this->MaxSize())
		{ // the stack is not yet full => add rec at the end
			this->stack[this->currMax] = rec;
			++this->currMax;
			ret = true;
		}
	}
	else 
	{ // rec has to be inserted somewhere in the stack
		int index = -1;
		if (this->stack[0] <= rec) 
		{
			index = 0;
			ret = true;
		}
		else
		{
			int imin = 0, imax = this->currMax - 1;

			while (imax - imin > 1)
			{
				int i = (imin + imax) / 2;

				if (this->stack[i] > rec) imin = i;
				else imax = i;
			}

			index = imin + 1;
		}

		if (this->currMax < this->MaxSize()) ++this->currMax;

		for (int i = this->currMax - 1; i > index; --i)
		{
			this->stack[i] = this->stack[i - 1];
		}

		if (index < this->MaxSize()) 
		{
			this->stack[index] = rec;
			ret = true;
		}
	};

	return ret;
}

#endif