#ifndef ____CDempShSensorRecord____
#define ____CDempShSensorRecord____

#include <vector>
using namespace std;

#include "CharString.h"


class CDempShSensorRecord 
{
//****************************************************************************
//
// Class:  CDempShSensorRecord
// ======
//
// Description: A record describing the functional model for the probability
// ============ masses for one class in the classification scheme for building
//              detection described in the following publication: 
//
//              Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik:
//              Using the Dempster-Shafer Method for the Fusion of LIDAR Data 
//              and Multi-spectral Images for Building Detection. 
//              Information Fusion 6(4), pp. 283-300.
//
//              There are tow options possibilities for describing such a 
//              functional model:
//
//                                    { prbMin for x <= Xmin
//              1.) Cubic:     m(x) = { cubic polynomial for Xmin < x < Xmax
//                                    { prbMax for x >= Xmax
//
//              2.) Empirical: An ordered(!) vector of tuples {x,m(x)} can  
//                             be read from an ASCII file; m(x) is then 
//                             interpolated in this vector
//
//****************************************************************************

//=============================================================================
 
  protected:

//=============================================================================
//=================================== Data  ===================================

	  int index;                // Index of the sensor (cf. DempsterShafer.h)

	  float Xmin;               // lower boundary for the cubic model:
                                // In these cases, m(x) will be equal to prbMin
                                // if x <= Xmin

    
	  float Xmax;               // upper boundary for the cubic model:
                                // In these cases, m(x) will be equal to prbMax
                                // if x >= Xmax

    
	  float prbMin;             // probability mass for all values x <= Xmin
                                // for the linear and cubic models


	  float prbMax;             // probability mass for all values x >= Xmax
                                // for the linear and cubic models

    
	  float diffProbMaxMin;     // prbMax - prbMin


	  float diffXMaxMin;        // Xmax - Xmin
	  float OneByDiffXMaxMin;   // 1 / (Xmax - Xmin)


	  vector<float> values;     // An ordered vector of values x for the 
	                            // empirical model


	  vector<float> probMasses; // The corresponding probability masses for the 
	                            // empirical model

//=============================================================================

  public:

//=============================================================================
//============================ Default Constructor ============================

	  CDempShSensorRecord() : index(-1), prbMin(0.05f), prbMax(0.95f), Xmin(0.0f), 
                              Xmax(1.0f), diffXMaxMin (1.0f), diffProbMaxMin(0.9f), 
                              values(0), probMasses(0), OneByDiffXMaxMin(1.0) { };


//=============================================================================
//================================ Destructor  ================================

	  ~CDempShSensorRecord() { };

//=============================================================================
//================================ Query data  ================================


	  int getIndex() const { return this->index; };


	  float getPrbMin() const { return this->prbMin; };


	  float getPrbMax() const { return this->prbMax; };


	  float getXmin() const { return this->Xmin; };


	  float getXmax() const { return this->Xmax; };


	  int getNumberOfEmpiricalRecords() const { return this->values.size(); };


//=============================================================================
//============================= Set sensor index  =============================


	  void setIndex(int Index){ this->index = Index; };


//=============================================================================
//============== Set parameters for the linear and cubic models  ==============
   
	  void setProbMinMax(float pMin, float pMax)
	  { 
		  this->prbMin = pMin; this->prbMax = pMax; 
		  this->diffProbMaxMin = this->prbMax - this->prbMin; 
       };

	  void setMinMax(float Min, float Max)
      { 
		  this->Xmin = Min; this->Xmax = Max; 
		  this->diffXMaxMin = this->Xmax - this->Xmin; 
		  this->OneByDiffXMaxMin = float(1.0) / this->diffXMaxMin;
	  };


//=============================================================================
//================= Write parameters of cubic model to string =================

	  CCharString getParameterString() const;

// Format of the string: "prbMin prbMax Xmin Xmax"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============= Initialise parameters of cubic models from string =============

	  void initParametersFromString(const CCharString &str);

// Format of the str: "prbMin prbMax Xmin Xmax"
// (As returned by getParameterString())
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//=============== Initialise parameters for the empirical model ===============

	  void initEmpiricalModel(const vector<float> &Values, 
                              const vector<float> &ProbabilityMasses);

// The empirical model is initialised from Values and ProbabilityMasses.
// Each tuple (Values[i], ProbabilityMasses[i]) gives one point in that 
// model. Values has to be ordered. For values smaller than Values[0],
// the probability mass is assumed to be ProbabilityMasses[0];
// For values greater than Values[Values.size() - 1], the probability 
// mass is assumed to be ProbabilityMasses[Values.size() - 1];
//
// Preconditions (not checked):
// 1. Values.size() == ProbabilityMasses.size()
// 2. Values is ordered
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    bool initEmpiricalModel(const CCharString &fileName);

// The empirical model is read from the ASCII file IrC_fileName.
// Format: Each line contains one tuple (x, m(x)); these tuples are ordered 
//         ascending with x (not checked), thus:
// x0  m(x0)
// x1  m(x1)
//    .
//    .
//    .
// xn  m(xn)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//========================= Compute probability mass ==========================

    float getProbCubic(float X) const;


    float getProbEmpirical(float X) const;

    float getProb(float X) const 
	{
		return (this->getNumberOfEmpiricalRecords() > 0) ? 
			getProbEmpirical(X): getProbCubic(X) ; 
	};

// return value: probability mass for X according to the model selected
//               getProbEmpirical(X) will return getProbCubic(X) if the
//               empirical model has not been initialised 
//               (getNumberOfEmpiricalRecords() == 0)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//================== Auxiliary methods for writing protocols ==================

    CCharString getDescriptor(const CCharString &sensorName, int precision) const;

// Returns the parameters of the linear / cubic models; Ii_precision: number 
// of digits after the comma; IrC_parName: Name of the Sensor
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    CCharString getProtocolStringCubic(const CCharString &headerString) const
	{ return this->getDescriptor(headerString, 3); };

    CCharString getProtocolStringEmpirical(const CCharString &headerString) const;
    CCharString getProtocolString(const CCharString &headerString) const
	{
		return (this->getNumberOfEmpiricalRecords()) ? 
			getProtocolStringEmpirical(headerString): getProtocolStringCubic(headerString);
	};

//=============================================================================

  protected:

//=============================================================================
//========== Auxiliary methods for computing the probability masses  ==========

    
	  float convert01(float X) const       
	  { return ((X - Xmin) * this->OneByDiffXMaxMin); };

// scale the input value If_val into the interval [0,1]
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 



//=============================================================================

};

#endif
