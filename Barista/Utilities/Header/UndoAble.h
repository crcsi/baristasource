#ifndef __CUndoable__
#define __CUndoable__

#include "ZObject.h"
class CCharString;

class CUndoable : public ZObject
{
public:
	CUndoable(void);
	~CUndoable(void);

	virtual void undo() = 0;

	virtual void getUndoName(CCharString& str) = 0;
	bool isa(string& className) const;
	string getClassName() const;
};
#endif
