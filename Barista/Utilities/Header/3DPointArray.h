#ifndef __C3DPointArray__
#define __C3DPointArray__
#include "3DPoint.h"
#include "3DPointPtrArray.h"
#include "MUObjectArray.h"
#include "Serializable.h"



class C3DPointArray : public CMUObjectArray<C3DPoint, C3DPointPtrArray>, public CSerializable
{
public:
	C3DPointArray(int nGrowBy = 0);
	~C3DPointArray(void);

	// serialize functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	bool isModified();
	void reconnectPointers();
	void resetPointers();

	C3DPoint* Add();
	C3DPoint* Add(const C3DPoint& point);
	
	bool isa(string& className) const
	{
		if (className == "C3DPointArray")
			return true;

		return false;
	};
	string getClassName() const {return string("C3DPointArray");};

	C3DPointArray& operator=(const C3DPointArray& oldArray);

	void transform(Matrix* pTransMat);

	C3DPoint* getPointByID(int ID);

};
#endif
