#ifndef __CDempsterShafer__
#define __CDempsterShafer__

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */
#include <vector>
#include <iostream>
using namespace std;

#include "CharString.h"

//****************************************************************************
//*************************** Forward Declarations ***************************

 
//****************************************************************************
//************* Decision rule for Dempster-Shafer classification *************

enum eDECISIONRULE { eMAXSUP = 0,       // Class of maximum support
                     eMAXSUPLGDUB,      // Class of maximum support with
                                        // support larger than dubiety
                     eMAXMASS,          // Class of maximum probability mass
                     eMAXMASSLGDUB,     // Class of maximum probability mass
                                        // with support larger than dubiety
                     eMAXPLAUS,         // Class of maximum plausibility
                     eMAXPLAUSLGDUB  }; // Class of maximum plausibility
                                        // with support larger than dubiety


//****************************************************************************
//****************************************************************************

class CDempsterShafer 
{
//*****************************************************************************
//
// Class CDempsterShafer is a generic class encapsulating methods and data 
// for classification based on the Dempster-Shafer evidential theory. 
// For the theoretical background and names of mathematical entities, cf. 
// the following publication:
//
// Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik:
// Using the Dempster-Shafer Method for the Fusion of LIDAR Data and Multi-
// spectral Images for Building Detection. 
// Information Fusion 6(4), pp. 283-300.
//
// The class design is optimised for multiple use of one instance of class
// DempsterShafer. For instance, if a Dempster Shafer classification process
// involving c classes and s sensors is to be carried out for each pixel of
// a digital image, a CDempsterShafer object for c classes and s sensors should
// be initialised in the beginning, which is to be used for the classification
// of each pixel, using the reset methods before initialising the probability
// masses.
//
//*****************************************************************************

  protected:

//=============================================================================
//=================================== Data ====================================
    
	  unsigned long nClasses;            // Number of "simple" classes 
   
	  vector<vector<float> *> initialProbMasses;
                                         // The initial probability masses as 
                                         // they are assigned to the classes  
                                         // of 2^{Theta} by the sensors.
                                         // initialProbMasses[i] is the
                                         // vector of probability masses 
                                         // from sensor i
                                         // (initialProbMasses[i])[j]
                                         // is the probability mass assigned
                                         // to class j from sensor i 
                                         // Order of classes, e.g. for four
                                         // classes C1, C2, C3, C4
                                         // (i.e., nClasses == 4):
                                         // Index j    Class
                                         //    0         C1
                                         //    1         C2
                                         //    2         C3
                                         //    3         C4
                                         //    4         C1 or C2
                                         //    5         C1 or C3
                                         //    6         C1 or C4
                                         //    7         C2 or C3
                                         //    8         C2 or C4
                                         //    9         C3 or C4
                                         //   10         C1 or C2 or C3
                                         //   11         C1 or C2 or C4
                                         //   12         C1 or C3 or C4
                                         //   13         C2 or C3 or C4
                                         //   14         C1 or C2 or C3 or C4 = Theta
     
	  vector<float> combinedProbMasses;  // The combined probability masses as they
                                         // are computed from 
                                         // initialProbMasses.
                                         // combinedProbMasses[i] is the
                                         // combined probability mass of class
                                         // i of 2^{Theta}. The length of the vector
	                                     // is, thus, equal to the number of classes 
	                                     // in 2^{Theta} - 1, i.e. (2^nClasses) - 1 
	                                     // (because the empty set is not considered).
	                                     // Class indices as described above

	  vector<float> support;             // The support as computed from 
                                         // combinedProbMasses.
                                         // support[i] is the support
                                         // of class i of 2^{Theta};
                                         // Class indices as described above


	  vector<float> plausibility;        // The plausibility as computed from 
                                         // combinedProbMasses.
                                         // plausibility[i] is the 
                                         // plausibility of class i of 2^{Theta};
                                         // Class indices as described above


	  float conflict;                    // The current conflict


	  vector<CCharString> classNames;    // Class names for all classes in 2^{Theta}
                                         // (for protocols)


	  vector<CCharString> sensorNames;   // Sensor names (for protocols)


	  vector<int> intersection;          // Auxiliary variable for fast 
                                         // computation of the combined 
                                         // probability masses.
                                         // Initialised by the constructor: 
                                         // intersection[i] is the class index
                                         // of the intersection of class 
                                         // i mod ( ||2^{Theta}|| - 1) with
                                         // class i / ( ||2^{Theta}|| - 1), 
                                         // and -1 if the intersection is empty.

	  vector<CCharString> names;         // Auxiliary variable, containing the initial
                                         // class names for all classes in 2^{Theta}


//=============================================================================

   public:

//=============================================================================
//=============================== Constructors  ===============================
 
	   CDempsterShafer();

// Default constructor, initialising an object for 0 classes and 0 sensors;
// An object thus created has to be initialised using method v_init(...). 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   CDempsterShafer(unsigned long Classes, unsigned long Sensors);

// Construct an object for Classes ("simple") classes and Sensors 
// sensors. The lengths of the vectors are set accordingly, and all probability 
// masses etc. are set to 0. intersection is initialised, too.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


//=============================================================================
//================================ Destructor  ================================
  
	   virtual ~CDempsterShafer();

//=============================================================================
//============================== Initialise data ==============================

	   void init(unsigned long Classes, unsigned long Sensors);

// Initialise an existing object for Classes ("simple") classes and  
// Sensors sensors. The lengths of the vectors are set accordingly, and  
// all probability masses etc. are set to 0. 
// intersection is initialised, too.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//================================ Query data  ================================
   
	   unsigned long getNSensors() const { return this->initialProbMasses.size(); };

// returns the number of sensors
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   
	   unsigned long getNClasses() const { return this->nClasses; };

// returns the number of simple classes, i.e. ||Theta||
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   
	   unsigned long getNCombinedClasses() const 
	   { return this->combinedProbMasses.size(); };

// returns the number of combined classes without the empty set, i.e. 
// ||2^Theta|| - 1
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   float getConflict() const { return this->conflict; };

// returns the conflict
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   float getCombinedProbability(unsigned long Class) const 
	   { return this->combinedProbMasses[Class]; };

// returns the combined probability mass of class Class, with 
// 0 <= Class < getNCombinedClasses()
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  
	   float getSupport(unsigned long Class) const  
	   { return this->support[Class]; };

// returns the support of class Iu_class, with 
// 0 <= Class < getNCombinedClasses()
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   float getPlausibility(unsigned long Class) const
	   { return this->plausibility[Class]; };

// returns the plausibility of class Iu_class, with 
// 0 <= Class < getNCombinedClasses()
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   float getDubiety(unsigned long Class) const;

// returns the dubiety of class Class, with 
// 0 <= Class < getNCombinedClasses()
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   CCharString getSensorName(unsigned long Sensor) const
	   { return this->sensorNames[Sensor]; };

// returns a string describing the name of sensor Sensor, with 
// 0 <= Sensor < getNSensors()
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   CCharString getClassName (unsigned long Class)  const
	   { return this->classNames[Class]; };

// returns a string describing the name of class Class, with 
// 0 <= Class < getNCombinedClasses()
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
       
//=============================================================================
//========================== Set sensor / class name ==========================

	   void setSensorName(unsigned long Sensor, const char *name);

// The name of the sensor at index Sensor is set to name
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void setClassName(unsigned long Class, const char *name);

// The name of the class at index Class is set to name
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//======================= Manipulate probability masses =======================

// Once an instance of class CDempsterShafer has been created, it can (and 
// should) be used multiple times. 
//
// The following steps have to be carried out:
//
// 1. Reset the probability masses etc. (v_resetAllProbabilityMasses();)
// 2. Assign the initial probability masses (v_assignProbabilityMass(...)
//    It is the application's responsibility to comply with the rules for 
//    these initial probability masses p_{ij}, that is:
//    a) 0 <= p_{ij} <= 1
//    b) Sum_j (p_{ij}) = 1 (thus, the sum over all classes is 1 for each sensor)
// 3. Compute the combined probability masses, support, and plausibility
//    (v_computeCombinedProbabilityMasses();)
// 4. Get the index of the class according to the decision criterion
//    (i_getClass(...) or i_getBestClass(...)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void resetInitialProbabilityMasses();

// reset the initial probability masses to 0
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void resetFinalProbabilityMasses();

// reset the combined probability masses, the supports, the plausibilities, 
// and the conflict to 0
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void resetAllProbabilityMasses();

// carry out resetInitialProbabilityMasses(); and resetFinalProbabilityMasses();
// Has to be done before probability masses are assigned.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


	   void assignProbabilityMass(unsigned long Class, unsigned long Sensor, float m);

// Assign the probability mass m from sensor Sensor to class Class.
// Preconditions (not checked):
// 1. 0 <= Sensor <  gettNSensors()
// 2. 0 <= Class  <  getNCombinedClasses()
// 3. 0 <= m   <= 1
// 4. After all probability masses have been assigned, the sum of the probability
//    masses has to be 1 for each sensor
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void computeCombinedProbabilityMasses();

// compute the combined probability masses, the plausibilities, and the 
// support of each class in 2^{Theta}.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   int getClass(eDECISIONRULE rule) const;

// Get the index of the class in Theta with the maximum score according to
// the decision rule rule. If the result is the rejection class, the
// return value will be nClasses.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   int getRankBestClass(eDECISIONRULE rule, int rank, float &value) const;

// Get the index of the class in Theta with the Ii_rank-best score according to
// the decision rule rule. If the result is the rejection class, the
// return value will be nClasses. 
// value: returns the score of that class 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//========= Methods for printing the data of an instance of the class =========

	   CCharString getPrintString() const;

// returns a formatted string for output to protocol
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  
	   void print(ostream &XrC_ostream) const
	   { XrC_ostream << getPrintString() << endl; };

// prints the formatted protocol string to XrC_ostream
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void print() const { print (cout); };
// prints the formatted protocol string to cout
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//================= Auxiliary methods for printing protocolls =================

	   static CCharString getRuleString(eDECISIONRULE rule);

// returns a string describing the decision rule rule, 
// e.g. "Maximum support" for Ie_rule == eMAXSUP
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================

  protected:

//=============================================================================
//====== Initialise / delete data vectors in initialProbMasses ======

	  void initVectors(unsigned long Classes);

	  void deleteVectors();


//=============================================================================
//====================== Initialise the vector names  ======================


	  void addCombinations(CCharString &currentString, int level,
		                   int nElements, unsigned long &index, 
						   int start);
// recursive method for initialising the vector names, which is used in 
// method init() to initialise the auxiliary variable intersection
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//================= Get index of intersection of two classes  =================

	  int getIntersection(int set1, int set2) const
	  { return this->intersection[set1 * getNCombinedClasses() + set2]; };

// returns the class index of the intersection of the class of index
// set1 and the class of index set2.
// For instance (cf. example above), getIntersection(8,10) returns 1:
// Index 8 corresponds to class {C2 or C4}, Index 10 to {C1 or C2 or C3}.
// The intersection of these classes is C2, and the return value is the
// index of C2, thus 1.
// If the intersection of two classes is empty, the return value is -1.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    
//=============================================================================
//=== Recursive auxiliary method for computing combined probability masses  ===

	  void computeProbabilityMasses(double currentProbabilityMass, 
                                    int intersection, int Sensor);


//=============================================================================
//===== Auxiliary methods for determining the optimal class according to  ===== 
//========================== a certain decision rule ==========================


	  int getMaxProbabilityMassClass() const;
 
	  int getMaxProbabilityMassLgDubietyClass() const;

	  int getMaxSupportClass() const;
 
	  int getMaxSupportLgDubietyClass() const;

	  int getMaxPlausibilityClass() const;
 
	  int getMaxPlausibilityLgDubietyClass() const;

//=============================================================================

};

//****************************************************************************

#endif
