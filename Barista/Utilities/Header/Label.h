// Label.h: interface for the CLabel class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CLabel__
#define __CLabel__
#include "CharString.h"

#define CLABEL_LENGTH 32

class CLabel
{
public:
	CLabel();
	CLabel(const char* pChar);
	CLabel(const CLabel& Label);

	virtual		~CLabel();

	bool		Set(const char *pChar);
	bool		Set(const CLabel& Label);

	bool		SetAlpha(const char *pChar);
	bool		SetNumber(int val);

	int			GetLength() const;

	CLabel		operator++(int i);
	CLabel		operator+=(int i);
	CLabel		operator+=(char* str);
	CLabel		operator--(int i);

	CLabel		operator++();
	CLabel		operator--();

	bool		operator==(const CLabel& label) const;
	bool		operator==(const char* label) const;

	bool		operator!=(const CLabel& label) const;

	CLabel		operator=(const char* pChar);

	bool		operator<(const CLabel& label) const;
	bool		operator>(const CLabel& label) const;
	

	int			GetNumber() const;
	CCharString	GetAlpha() const;

	bool		MatchesTemplate(CLabel& Temp);
	void		MakeTemplate(CLabel& Label);

	const char* GetChar() const;
	int			find(const char* str) const;
    int         reverseFind(const char* str) const;

	bool		isaCode() const;
	bool		isaNugget() const;
private: 
	
	int		GetNumericStart() const;
 	bool	HasInvalidCharacters(const char *pChar);
	void	removeInvalidCharacters(char* dst, const char *src);


// Members
private: 
	char m_pChar[CLABEL_LENGTH + 1]; // leave room for \0 at end

};

#endif // __CLabel__



