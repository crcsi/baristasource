// *
// * Title:        RobustWeightFunction<p>
// * Description:  <p>
// * Copyright:    Copyright (c) Franz Rottensteiner<p>
// * Company:      University of Melbourne<p>
// * @author Franz Rottensteiner
// @version 1.0
// *

#ifndef __CRobustWeightFunction__
#define __CRobustWeightFunction__

class CRobustWeightFunction
{
  protected:
	  double halfweight;
	  double slant;
	  double markwidth;
	  double a;
	  double b;

  public:

	  CRobustWeightFunction();

	  ~CRobustWeightFunction();

	  double getWeight(double residual) const;

	  bool setParameters(double hw, double sl, double mw);

	  bool setParameters(double hw, double mw);

	  double getHalfweight() const { return halfweight; };
	  double getSlant() const { return slant; };
	  double getMarkwidth() const { return markwidth; };

};

#endif

