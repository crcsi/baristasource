#ifndef __CITUTM__
#define __CITUTM__

typedef struct _datum 
{
	double majorAxis;	/* semi-major axis (m)		    */
	double minorAxis;	/* semi-minor axis (m)		    */
	double e;			/* numeric eccentricity 	    */
	double e2;			/* numeric eccentricity squared */
	double e1;			/* numeric second eccentricity  */
	
	double xOff;		/* offset relative to WGS_84 (m)*/
	double yOff;		/* offset relative to WGS_84 (m)*/
	double zOff;		/* offset relative to WGS_84 (m)*/
}   DATUM;

#define  WGS_66					0x00
#define  WGS_72					0x01
#define  WGS_84					0x02
#define  GRS_80					0x03
#define  GRS_83					0x04
#define  KRASSOVSKY				0x05
#define  BESSEL_1841			0x06
#define  CLARKE_1866			0x07
#define  CLARKE_1880			0x08
#define	 AIRY					0x09
#define  AIRY_MODIFIED			0x10
#define  AUSTRALIAN_NATIONAL	0x11
#define  BESSEL_1841_(NAMIBIA)	0x12
#define  EVEREST				0x13
#define  EVEREST_MODIFIED		0x14
#define  HELMERT_1906			0x15
#define  HOUGH					0x16
#define  INTERNATIONAL_09		0x17
#define  INTERNATIONAL_67		0x18
#define  INTERNATIONAL_75		0x19
#define  MERCURY_60				0x20
#define  MODIFIED_MERCURY_68	0x21
#define  MODIFIED_FISCHER_60	0x22
#define  SPHERE_6370997			0x23


#define M_PI           	3.14159265358979323846
#define M_PI_2         	1.57079632679489661923
#define M_PI_4         	0.78539816339744830962
#define M_PI           	3.14159265358979323846
#define M_PI_2         	1.57079632679489661923
#define M_PI_4         	0.78539816339744830962

#define TWO_PI  		(M_PI*2.0)

#define MAX_VAL 		4
#define DBLLONG 		4.61168601e18
#define EPSLN   		1.0e-10
#define MAXLONG         0x7fffffff  

// Simple Functions Definition
#define HALF_PI			(M_PI*0.5)
#define TWO_PI 			(M_PI*2.0)
#define EPSLN			1.0e-10
#define R2D				(180.0/M_PI)
#define D2R				(M_PI/180.0)
#define SQUARE(x)       x * x						/* x**2 */
#define CUBE(x)			x * x * x					/* x**3 */
#define QUAD(x)			x * x * x * x				/* x**4 */
#define GMAX(A, B)      ((A) > (B) ? (A) : (B))		/* assign maximum of a and b */
#define GMIN(A, B)      ((A) < (B) ? (A) : (B))		/* assign minimum of a and b */
#define IMOD(A, B)      (A) - (((A) / (B)) * (B))	/* Integer mod function */

#define SQUARE(x)       x * x

class CITUTM
{
public:
    CITUTM(void);
    ~CITUTM(void);

public:

void		sincos(double val, double *sin_val, double *cos_val); 
int			sign(double x); 
int			calc_utm_zone(double lon); 
double		asinz(double con);
double		msfnz(double eccent,double sinphi,double cosphi);
double		qsfnz(double eccent, double sinphi, double cosphi);
double		phi1z(double eccent,double qs, int *flag);
double		phi2z(double eccent,double ts, int *flag);
double		phi3z(double ml,double e0,double e1,double e2,double e3, int *flag);
double		phi4z(double eccent,double e0,double e1,double e2,double e3,double a,double b,double *c,double *phi);
double		paksz(double ang, int *flag);
double		pakcz(double pak);
double		pakr2dm(double pak);
double		tsfnz(double eccent,double phi,double sinphi);
double		adjust_lon(double x);
double		e0fn(double x); 
double		e1fn(double x); 
double		e2fn(double x); 
double		e3fn(double x); 
double		e4fn(double x);
double		mlfn(double e0,double e1,double e2,double e3,double phi);

int UTM_Init(
		double r_maj,			/* major axis	*/
		double r_min,			/* minor axis	*/
		int zone);			    /* zone number	*/

int TM_Init(
		double r_maj,			/* major axis	*/
		double r_min,			/* minor axis	*/
		int zone);			    /* zone number	*/

int UTM_Forward(
		double lon,	/* (I) Longitude (degrees)		*/
		double lat,	/* (I) Latitude  (degrees)		*/
		double *x,	/* (O) X projection coordinate 	(easting) */
		double *y);	/* (O) Y projection coordinate 	(northing */

int UTM_Inverse(
	double x,		/* (Input) X projection coordinate */
	double y,		/* (Input) Y projection coordinate */
	double *lon,	/* (Output) Longitude (in Degrees) */
	double *lat);	/* (Output) Latitude  (in Degrees) */

int Ellipsoid_Init(int ellipsoid_index, DATUM *datum);

int	Geographic2Geocentric(
	int		ellipSoid_index,           /* The inex for type of EllipSoid */
	double	Lon,double Lat,double Alt, /* The latitude, longitude (degrees) and altitude */
	double	*GX,double *GY,double *GZ); /* The geocentrical coords */

int	Geocentric2Geographic(
		int    ellipSoid_index,              /* The inex for type of EllipSoid */
		double GX,double GY,double GZ,       /* The geocentrical coords */
		double *Lon,double *Lat,double *Alt); /* The latitude, longitude (degrees) and altitude */

int Geocentric2TangentPlane(
		int    ellipSoid_index,                 /* The inex for type of EllipSoid */
		double orig_lon, double orig_lat,       /* The geographic coords (in degrees) of the tangent plane's original point */
		double GX,double GY,double GZ,          /* The geocentrical coords */
		double *r_GX,double *r_GY,double *r_GZ); /* The tangent plane coords */

int TangentPlane2Geocentric(
		int    ellipSoid_index,                 /* The inex for type of EllipSoid */
		double orig_lon, double orig_lat,       /* The geographic coords (in degrees) of the tangent plane's original point */
		double GX,double GY,double GZ,          /* The tangent plane coords */
		double *r_GX,double *r_GY,double *r_GZ); /* The geocentrical coords */

int Geographic2TangentPlane(
		int    ellipSoid_index,                 /* The inex for type of EllipSoid */
		double orig_lon, 
		double orig_lat, 
		double orig_hig,                        /* The geographic coords (in degrees) of the tangent plane's original point */
		double lon,double lat,double alt,       /* The geographic coords (in degrees & meters) */
		double *r_GX,double *r_GY,double *r_GZ); /* The tangent plane coords */
int TangentPlane2Geographic(
		int    ellipSoid_index,           /* The inex for type of EllipSoid */
		double orig_lon, 
		double orig_lat, 
		double orig_alt,                  /* The geographic coords (in degrees) of the tangent plane's original point */
		double GX,double GY,double GZ,    /* The tangent plane coords */
		double *r_lon,
		double *r_lat,
		double *r_alt);                    /* The geographic coords (in degrees & meters) */

};
#endif
