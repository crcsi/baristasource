// abstract base class for serializable objects using CStructuredFile

#ifndef __CSerializable__
#define __CSerializable__

#include "CharString.h"
#include "HandleManager.h"
#include "ZObject.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"


class CSerializable : public ZObject
{
public:
 CSerializable();
 virtual ~CSerializable();

 // Virtual functions: declaring virtual: functionality of a virtual function can be over-ridden in its derived classes. 
 virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
 virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);

 virtual void reconnectPointers() = 0;
 virtual void resetPointers();

 //virtual CCharString getClassName() = 0;
 virtual bool isModified() = 0;

 void setModified();
 bool bModified;
 static CHandleManager handleManager;
 CSerializable* getOldPointer();

	void setParent(CSerializable* pParent);
	CSerializable* getParent();

	virtual	void setName(CCharString name);
	CCharString getName();
	CSerializable* getBasePointer();

	void clone(CSerializable* pSrc);

	static int restoreCount;
protected:
	CSerializable* pOldPointer;
	CSerializable* pParent;
	CCharString name;
};


#endif
