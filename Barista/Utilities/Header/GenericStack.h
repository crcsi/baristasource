#ifndef __CGenericStack__
#define __CGenericStack__

#include <vector>
using namespace std;

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

template <class T> class CGenericStack
{
  protected:

	  vector<T> stack;

	  int currMax;

  public:
	 
	  CGenericStack(int StackSize = 5): currMax(0), stack(StackSize) { };

	  virtual ~CGenericStack() { };

	  int MaxSize() const { return stack.size(); };

	  int CurrentSize() const { return currMax; }
	  void reset() { currMax = 0; };

	  const T &operator[](int index) const { return stack[index]; };

};

template <class T> class CGenericStackAscending : public CGenericStack<T>
{
  public:
 
	  CGenericStackAscending(int StackSize = 5): CGenericStack<T>(StackSize) { };

	  virtual ~CGenericStackAscending() { };

	  bool checkInsertRecord(const T &rec);
};
 
template <class T> class CGenericStackDescending : public CGenericStack<T>
{
  public:
 
	  CGenericStackDescending(int StackSize = 5): CGenericStack<T>(StackSize) { };

	  virtual ~CGenericStackDescending() { };

	  bool checkInsertRecord(const T &rec);
};
 
#include "GenericStack.cpp"

#endif

