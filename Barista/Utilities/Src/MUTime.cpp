// MUTime.cpp: implementation of the CMUTime class.
//
//////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "utilities.h"

#include "MUTime.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMUTime::CMUTime()
{
	time(&m_Time);

}

CMUTime::CMUTime(CMUTime& src)
{
	m_Time = src.m_Time;
}

CMUTime::~CMUTime()
{

}

CCharString CMUTime::Format(const char* fmt)
{
	struct tm* pTm;

	char string[MAX_MUTIME_STRING_LENGTH + 1];

	assert (fmt != NULL);

	pTm = localtime( &m_Time );

	strftime( string, MAX_MUTIME_STRING_LENGTH, fmt, pTm);

	CCharString ret = string;

	return ret;
}

CMUTime CMUTime::GetCurrentTime()
{
	CMUTime ret;

	return ret;
}

bool CMUTime::SetFromString(const char *fmt, const char *datestr)
{
	// Standard time formations , normal "%d %B, %Y   %X"
	//
	// %a	Abbreviated weekday name
	// %A	Full weekday name
	// %b	Abbreviated month name
	// %B	Full month name
	// %c	Date and time representation appropriate for locale
	// %d	Day of month as decimal number (01  31)
	// %H	Hour in 24-hour format (00  23)
	// %I	Hour in 12-hour format (01  12)
	// %j	Day of year as decimal number (001  366)
	// %m	Month as decimal number (01  12)
	// %M	Minute as decimal number (00  59)
	// %p	Current locales A.M./P.M. indicator for 12-hour clock
	// %S	Second as decimal number (00  59)
	// %U	Week of year as decimal number, with Sunday as first day of week (00  53)
	// %w	Weekday as decimal number (0  6; Sunday is 0)
	// %W	Week of year as decimal number, with Monday as first day of week (00  53)
	// %x	Date representation for current locale
	// %X	Time representation for current locale
	// %y	Year without century, as decimal number (00  99)
	// %Y	Year with century, as decimal number
	//
	// %a, %A, %d, %U, %w, %W, %z and %Z are not supported
 
	//"%d %B, %Y   %X"	


	char* format[21]={"%a","%A","%b","%B","%c","%d","%H","%I","%j","%m",
		            "%M","%p","%S","%U","%w","%W","%x","%X","%y","%Y","%z"};

	char *monthAbb[12]={"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
					"Sep", "Oct", "Nov", "Dec"};
	char *month[12]={"January", "February", "March", "April", "May", "June",
					"July", "August", "September", "October",
					"November", "December"};
/*   {"AM", "PM"}

        , { "M/d/yy" }
        , { "dddd, MMMM dd, yyyy" }
        , { "H:mm:ss" }*/
	/*char *weekdayLong[7]={""}
	char *weekdayShort[7]={"MON","TUE",""}*/

	char *seps=" ,:/\t",*token,*charpos;
	char buffer[MAX_MUTIME_STRING_LENGTH];
	char seperator;
	CCharString typ,value;
	CCharString text=datestr;

	strcpy(buffer,fmt);
   
	struct tm pTM;
	pTM = *localtime( &m_Time ); //to get time shift correctly set
	
/*	pTM.tm_sec   //Seconds after minute (0  59)
	pTM.tm_min   //Minutes after hour (0  59)
	pTM.tm_hour  //Hours after midnight (0  23)
	pTM.tm_mday  //Day of month (1  31)
	pTM.tm_mon   //Month (0  11; January = 0)
	pTM.tm_year  //Year (current year minus 1900)
*/
	int pos;
	token = strtok( buffer, seps );
	while( token != NULL )
	{
		typ=token;
		seperator=fmt[token-buffer+strlen(token)];

		if (seperator!='\0')
		{
			pos=text.Find(seperator);
			if (pos==-1)
				return false;
			value=text.Left(pos);
		}
		else
			value=text;


		//TRACE2("Typ=%s Value=%s \n ",typ.GetChar(),value.GetChar());

		if (typ=="%b") //Abbreviated month name
		{
			for(int i=0; i<11; i++)
			{
				if ((CCharString)monthAbb[i]==value)
				{
					pTM.tm_mon=i;
					break;
				}
			}
		}
		else if (typ=="%B") //Full month name
		{
			for(int i=0; i<11; i++)
			{
				if ((CCharString)month[i]==value)
				{
					pTM.tm_mon=i;
					break;
				}
			}
		}
		else if (typ=="%d") //Day of month as decimal number (01  31)
		{
			sscanf(value.GetChar(),"%d", &(pTM.tm_mday));
		}
		else if (typ=="%H") //Hour in 24-hour format (00  23)
		{
			sscanf(value.GetChar(),"%d",&(pTM.tm_hour));
		}
		else if (typ=="%I") //Hour in 12-hour format (01  12)
		{
			sscanf(value.GetChar(),"%d",&(pTM.tm_hour));
		}
		else if (typ=="%m") //Month as decimal number (01  12)
		{
			sscanf(value.GetChar(),"%d",&(pTM.tm_mon));
		}
		else if (typ=="%M") //Minute as decimal number (00  59)
		{
			sscanf(value.GetChar(),"%d",&(pTM.tm_min));
		}
		else if (typ=="%p") //Current locales A.M./P.M. indicator for 12-hour clock
		{
			if (value=="PM")
			{
				if (pTM.tm_hour!=12)
					pTM.tm_hour+=12;
			}
			else
			{
				if (pTM.tm_hour==12)
					pTM.tm_hour=0;
			}

		}
		else if (typ=="%S") //Second as decimal number (00  59)
		{
			sscanf(value.GetChar(),"%d",&(pTM.tm_sec));
		}
		else if (typ=="%y") //Year without century, as decimal number (00  99)
		{
			sscanf(value.GetChar(),"%d",&(pTM.tm_year));
		}
		else if (typ=="%Y") //Year with century, as decimal number
		{
			sscanf(value.GetChar(),"%d",&(pTM.tm_year));
		}
		else if (typ=="%x") //Date representation for current locale
		{
			sscanf(text,"%d/%d/%d",&(pTM.tm_mon),&(pTM.tm_mday),&(pTM.tm_year));
			pTM.tm_mon-=1;
		}
		else if (typ=="%X") //Time representation for current locale
		{
			sscanf(text,"%d:%d:%d",&(pTM.tm_hour),&(pTM.tm_min),&(pTM.tm_sec));
		}
		else if (typ=="%c") //Date and time representation appropriate for locale
		{
			sscanf(text,"%d/%d/%d %d:%d:%d",&(pTM.tm_mon),&(pTM.tm_mday),&(pTM.tm_year),
											&(pTM.tm_hour),&(pTM.tm_min),&(pTM.tm_sec));
			pTM.tm_mon-=1;
		}

		charpos=token+strlen(token);
		token = strtok( NULL, seps );
		if (token!=NULL)
			text=text.Right(text.GetLength()-pos-(token-charpos));
	}
 
	if (pTM.tm_year<50)
		pTM.tm_year+=100; 

	m_Time=mktime(&pTM);
	return true;
}


