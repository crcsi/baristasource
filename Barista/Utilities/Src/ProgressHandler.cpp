#include "ProgressHandler.h"


void CProgressHandler::setProgressListener(CProgressListener* l)
{
    this->listener = l;
}

void CProgressHandler::removeProgressListener()
{
    this->listener = 0;
}
