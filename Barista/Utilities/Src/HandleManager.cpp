#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <string.h>

#include "HandleManager.h"
#include "UnsignedLong.h"
#include "Pointer.h"
#include "Serializable.h"

CHandleManager::CHandleManager(void)
{
    this->sorted = false;

}

CHandleManager::~CHandleManager(void)
{
    int hmm = 0;
}

bool CHandleManager::registerPointers(CSerializable* pOldPointer, CSerializable* pNewPointer)
{

    /*
    for (int i = 0; i < this->oldPointers.GetSize(); i++)
	{
		CPointer* pPointer = this->oldPointers.GetAt(i); 

		if (pPointer->pointer == pOldPointer)
		{
			int hmm = 0;
		}
	}*/

	CCharString str(pNewPointer->getClassName().c_str());

	this->classNames.Add(str);

    CPointer* pPointer = this->oldPointers.Add();
    pPointer->pointer = pOldPointer;
        
    pPointer = this->newPointers.Add();
    pPointer->pointer = pNewPointer;

    this->sorted = false;

    return true;
}

/*
CSerializable* CHandleManager::getNewPointer2(CSerializable* pOldPointer)
{
    if (!this->sorted)
        this->sort();

        int i;
    for(i = 0; i < this->oldPointers.GetSize(); i++)
    {
        CPointer* pTestPointer = this->oldPointers.GetAt(i);
        
        if(pTestPointer->pointer == pOldPointer)
            break;
    }

    if(i < this->newPointers.GetSize())
        return this->newPointers.GetAt(i)->pointer;    
    else
    {
//        assert(false);
        return NULL; // @@@
    }
}
*/

/**
 * Does a binary search 
 */
CSerializable* CHandleManager::getNewPointer(CSerializable* oldPointer)//, const char* className)
{
    if (!this->sorted)
        this->sort();

    // calculate initial search position.
    int lowerBound = 0;
    int upperBound = this->oldPointers.GetSize() - 1;
    int index = (lowerBound + upperBound) / 2;
    bool found = false;

    CPointer* testPointer = NULL;

    while( (found == false) && (lowerBound <= upperBound))
    {
        testPointer = this->oldPointers.GetAt(index);

        if (testPointer->pointer == oldPointer)
		{
			return this->newPointers.GetAt(index)->pointer;
			/*
			for (int i = index; i < this->oldPointers.GetSize(); i++)
			{
				testPointer = this->oldPointers.GetAt(i);
		        if (testPointer->pointer != oldPointer)
					break;

				if (this->classNames.GetAt(i)->CompareNoCase(className))
					return this->newPointers.GetAt(i)->pointer;
			}
			for (int i = index-1; i >= 0; i--)
			{
				testPointer = this->oldPointers.GetAt(i);
		        if (testPointer->pointer != oldPointer)
					break;

				if (this->classNames.GetAt(i)->CompareNoCase(className))
					return this->newPointers.GetAt(i)->pointer;
			}

			return NULL;
			*/
		}

        if (testPointer->pointer >= oldPointer)
            upperBound = index - 1;
        else
            lowerBound = index + 1;

       	index = (int)(((lowerBound + upperBound) / 2.0) + 0.5);
//        index = (lowerBound + upperBound) / 2;
    }

    return NULL;
}


void CHandleManager::reset()
{
    this->newPointers.RemoveAll();
    this->oldPointers.RemoveAll();
}

void CHandleManager::sort()
{
    if (this->oldPointers.GetSize() < 2)
        return;

    this->quickSort(0, this->oldPointers.GetSize()-1);

    this->sorted = true;
}

void CHandleManager::quickSort(int L, int R)
{
    int i = L;
    int j = R;

    CPointer* x = this->oldPointers.GetAt((L + R)/ 2);

    do
    {
        while (this->oldPointers.GetAt(i)->pointer < x->pointer) 
            i++;
        
        while (x->pointer < this->oldPointers.GetAt(j)->pointer) 
            j--;
        
        if (i <= j)
        {
            this->oldPointers.Swap(i, j);
            this->newPointers.Swap(i, j);
            i++; 
            j--;
        }
    } while (!(i > j));
    
    if (L < j)
        this->quickSort(L, j);
    
    if (i < R)
        this->quickSort(i, R);
}

