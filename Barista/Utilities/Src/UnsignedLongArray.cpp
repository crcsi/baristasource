#include "UnsignedLongArray.h"

CUnsignedLongArray::CUnsignedLongArray(int nGrowBy) :
	CMUObjectArray<CUnsignedLong, CUnsignedLongPtrArray>(nGrowBy)
{
}

CUnsignedLongArray::~CUnsignedLongArray(void)
{
}
