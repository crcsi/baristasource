/**
 * @class CLabel 
 * point label class
 * is basically a string but with some special behaviors
 * 
 */

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <assert.h>

#include "Label.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// CLabel::CLabel()
//
//
CLabel::CLabel()
{
	// Initialize string
	m_pChar[0] = '\0';

}

//////////////////////////////////////////////////////////////////////
// CLabel::CLabel(const char* pChar)
//
//
CLabel::CLabel(const char* pChar)
{
    if (pChar == NULL)
        this->Set("");
    else
	    this->Set(pChar);
}

//////////////////////////////////////////////////////////////////////
// CLabel(const CLabel& Label)
//
//
CLabel::CLabel(const CLabel& Label)
{
	Set(Label.m_pChar);
}

//////////////////////////////////////////////////////////////////////
// CLabel::~CLabel()
//
//
CLabel::~CLabel()
{
}

//////////////////////////////////////////////////////////////////////
// bool CLabel::Set(const char *pChar)
//
//
bool CLabel::Set(const char *pChar)
{
	// Sanity check
	assert(pChar != 0);

	int len = strlen(pChar);

	// Sanity check
	if ( (len < 0) || (len > CLABEL_LENGTH) )
		return false;

	char buf[1024];

	this->removeInvalidCharacters(buf, pChar);

	// do the copy
	strcpy(m_pChar, buf);

	// Force upper case
	for ( int i = 0; i < len; i++ )
		m_pChar[i] = toupper(m_pChar[i]);

	return true;
}

//////////////////////////////////////////////////////////////////////
// bool CLabel::Set(const CLabel& Label)
//
//
bool CLabel::Set(const CLabel& Label)
{
	// do the copy
	strcpy(m_pChar, Label.m_pChar);

	return true;
}

//////////////////////////////////////////////////////////////////////
// bool CLabel::SetAlpha(const char *pChar)
//
//
bool CLabel::SetAlpha(const char *pChar)
{
	int n = GetNumber();

	if ( !Set(pChar) )
		return false;

	if ( !SetNumber(n) )
		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////
// bool CLabel::SetNumber(int val)
//
//
bool CLabel::SetNumber(int val)
{
	int index = GetNumericStart();

	char tmp[CLABEL_LENGTH];
	sprintf(tmp, "%d", val);

	int len = strlen(tmp);

	// Check if it fits in m_pData
	if ( (index + len) > CLABEL_LENGTH)
		return false;

	// Check for existing numeric field
	if ( index == GetLength() )
	{
		// Append numeric part to end
		strcpy(&m_pChar[index], tmp);
	}
	else
	{
		// Replace existing numeric part
		m_pChar[index] = '\0';
		strcpy(&m_pChar[index], tmp);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////
// CCharString CLabel::GetAlpha()
//
//
CCharString CLabel::GetAlpha() const
{
	CCharString str;
	int index = GetNumericStart();

	// This is the case were there is not a number
	if (index > 0)
	{		
		str = &m_pChar[0];
		if (index < str.GetLength()) str = str.Left(index);
	}

	return str;
}

//////////////////////////////////////////////////////////////////////
// int	CLabel::GetNumber()
//
//
int CLabel::GetNumber() const
{
	int n = 0;
	int index = GetNumericStart();

	// This is the case were there is not a number
	if ( index == GetLength() )
		return 0;

	// Read the number
	sscanf(&m_pChar[index], "%d", &n);

	return n;
}

//////////////////////////////////////////////////////////////////////
// int	CLabel::GetLength()
//
//
int	CLabel::GetLength() const
{
	return strlen(m_pChar);
}

//////////////////////////////////////////////////////////////////////
// int CLabel::GetNumericStart()
//
//
int CLabel::GetNumericStart() const
{
	bool bInField = false;

	int n = strlen(m_pChar);

	// Start look form end of string to the beginning
        int i;
	for (i = n - 1; i >=0; i--)
	{
		int c = (int)m_pChar[i];

		// Check if we found an alpha or a numeric
		if (isdigit(c))
		{
			// We are now in the numeric feild
			bInField = true;
			continue;
		}
		else
		{
			// negative sign OK if we have been seeing digits
			if (bInField && c == '-')
				return i;
		}
		// Found the start
		break;
	}

	return i + 1;
}

//////////////////////////////////////////////////////////////////////
// CLabel CLabel::operator++()
// 
// prefix ++
//
CLabel CLabel::operator++()
{
	// Just call the postfix
	return (*this)++;
}

//////////////////////////////////////////////////////////////////////
// CLabel CLabel::operator--( )
//
// prefix --
//
CLabel CLabel::operator--()
{
	// Just call the postfix
	return (*this)--;
}

//////////////////////////////////////////////////////////////////////
// CLabel CLabel::operator++(int i)
//
// postfix ++
//
CLabel CLabel::operator++(int i)
{
	// Get the number
	int n = GetNumber();

	// Increment the number
	n++;

	// Set the number
	SetNumber(n);

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CLabel CLabel::operator--(int i)
//
// postfix --
//
CLabel CLabel::operator--(int i)
{
	// Get the number
	int n = GetNumber();

	// Decrement the number
	n--;

	// Set the number
	SetNumber(n);

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CLabel CLabel::operator+=(int i)
//
// postfix ++
//
CLabel CLabel::operator+=(int i)
{
	// Get the number
	int n = GetNumber();

	// Increment the number by i
	n += i;

	// Set the number
	SetNumber(n);

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CLabel CLabel::operator+=(char*str)
//
// postfix ++
//
CLabel CLabel::operator+=(char* str)
{
	if (this->GetLength() + ::strlen(str) > CLABEL_LENGTH)
	{
		//assert(false);
		return *this;
	}

	::strcat(this->m_pChar, str);

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CLabel::operator==()
//
//
bool CLabel::operator==(const CLabel& label) const
{
	// Varify they are the same length
	if (GetLength() != label.GetLength())
		return false;

	// Compare each character
	for (int i = 0; i < GetLength(); i++)
	{
		if ( m_pChar[i] != label.m_pChar[i] )
			return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////
// CLabel::operator==()
//
//
bool CLabel::operator==(const char* label) const
{
	// Varify they are the same length
	if (this->GetLength() != strlen(label))
		return false;

	// Compare each character
	for (int i = 0; i < GetLength(); i++)
	{
		if ( m_pChar[i] != label[i] )
			return false;
	}

	return true;
}

void CLabel::MakeTemplate(CLabel& Label)
{
	*this = Label;

	int ns = Label.GetNumericStart();

	if (ns == -1)
		return;

	m_pChar[ns] = '\0';
}

//////////////////////////////////////////////////////////////////////
// Tests that this label mathes the first chars of Temp
//
//
bool CLabel::MatchesTemplate(CLabel& Temp)
{
	if (Temp.GetLength() == 0)
		return false;

	// Compare each character 
	for (int i = 0; i < Temp.GetLength(); i++)
	{
		if ( m_pChar[i] != Temp.m_pChar[i] )
			return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////
// CLabel::operator!=()
//
//
bool CLabel::operator!=(const CLabel& label) const
{
	if (*this == label)
		return false;
	return true;
}

//////////////////////////////////////////////////////////////////////
// CLabel operator=(const char* pChar)
//
//
CLabel CLabel::operator=(const char* pChar)
{
    if (pChar == NULL)
        this->Set("");
    else
        this->Set(pChar);

	return *this;
}

bool CLabel::isaCode() const
{
	if ( (this->find("CODE") != -1) && (this->find("_") == -1) )
		return true;
	if ( (this->find("CC") != -1) && (this->find("_") == -1) )
		return true;
	if ( (this->find("PMX") != -1) && (this->find("_") == -1) )
		return true;

	return false;
}

bool CLabel::isaNugget() const
{
	if ( (this->find("CODE") != -1) && (this->find("_") != -1) )
		return true;
	if ( (this->find("CC") != -1) && (this->find("_") != -1) )
		return true;
	if ( (this->find("PMX") != -1) && (this->find("_") != -1) )
		return true;

	return false;
}


//////////////////////////////////////////////////////////////////////
// operator<(const CLabel& label)
//
//
bool CLabel::operator<(const CLabel& label) const
{
	// special case wereby coded target labels are first (and nuggets last)
	if (this->isaCode() && !label.isaCode())
		return true;

	if (this->isaNugget() && !label.isaNugget())
		return false;

	if (label.isaCode() && !this->isaCode())
		return false;

	if (label.isaNugget() && !this->isaNugget())
		return true;

	int index1 = GetNumericStart();
	int index2 = label.GetNumericStart();
	
	// check for ==
	if ( *this == label )
		return false;

	// Check char by char up to the legnth of shorter string
	for (int i = 0; (i < index1 && i < index2); i++)
	{
		// if any character is greater its NOT LESS
		if ( m_pChar[i] > label.m_pChar[i] )
			return false;

		// if any character is less it IS LESS
		if ( m_pChar[i] < label.m_pChar[i] )
			return true;
	}

	// if the first (this) is the longer its NOT less
	if ( index1 > index2 )
		return false;

	// if the first (this) is the shorter it IS less
	if ( index1 < index2 )
		return true;

	// the alpha portions are the same... check the numeric portion
	int n1 = GetNumber();
	int n2 = label.GetNumber();

	return n1 < n2;
}

//////////////////////////////////////////////////////////////////////
// operator>(const CLabel& label)
//
//
bool CLabel::operator>(const CLabel& label) const
{
	if (*this < label)
		return false;
	return true;
}

//////////////////////////////////////////////////////////////////////
// const char* GetChar()
//
//
const char* CLabel::GetChar() const
{
	return (const char*)m_pChar;
}
					
					
//////////////////////////////////////////////////////////////////////
// bool HasInvalidCharacters(const char *pChar)
//
//
bool CLabel::HasInvalidCharacters(const char *pChar)
{
	int len = strlen(pChar);

	for (int i = 0; i < len; i++)
	{
		// If its alphaNumeric its OK
		if (isalnum(pChar[i]))
			continue;

		// Followinf are specific non-alphanumrics that are OK

		if (pChar[i] == '_')
			continue;

		if (pChar[i] == '-')
			continue;

		// Character not allowed
		return true;
	}

	// No invalid character detected
	return false;
}

void CLabel::removeInvalidCharacters(char* dst, const char *src)
{
	int len = strlen(src);

	int j = 0;
	for (int i = 0; i < len; i++)
	{
		// If its alphaNumeric its OK
		if (isalnum(src[i]))
		{
			dst[j] = (char)::toupper(src[i]);
			j++;
			continue;
		}

		// Followinf are specific non-alphanumrics that are OK

		if (src[i] == '_')
		{
			dst[j] = src[i];
			j++;
			continue;
		}

		if (src[i] == '-')
		{
			dst[j] = src[i];
			j++;
			continue;
		}

		// Character not allowed
	}

	dst[j] = 0;
}

/**
 * returns the index of the first occurance of the given string
 * @param str the string to find
 * @return the position or -1 if not found
 */
int  CLabel::find(const char* str) const
{
	int len = strlen(str);
	
	if (len == 0)
		return -1;

	for (int i = 0; i <= this->GetLength() - len; i++)
	{
		int j;
		for (j = 0; j < len; j++)
		{
			if (this->m_pChar[i+j] != str[j]) 
				break;
		}

		if (j == len)
			return i;
	}

	return -1;
}

/**
 * returns the index of the last occurance of the given string
 * @param str the string to find
 * @return the position or -1 if not found
 */
int CLabel::reverseFind(const char* str) const
{
	int len = strlen(str);
	
	if (len == 0)
		return -1;

	for (int i = this->GetLength() - len; i >= 0 ; i--)
	{
		int j;
		for (j = 0; j < len; j++)
		{
			if (this->m_pChar[i+j] != str[j]) 
				break;
		}

		if (j == len)
			return i;
	}

	return -1;
}