#include <stdlib.h>
#include <float.h>
#include <math.h>

#include "AdjustNormEqu.h"

#include "AdjustMatrix.h"



//****************************************************************************
//
//  Class       : CNormalEquationMatrix
//
//  Description :
//
//****************************************************************************

CNormalEquationMatrix::CNormalEquationMatrix ( unsigned long unknowns ) :
			doubles(numElements( unknowns ), 0.), nUnknowns( unknowns ), 
			epsilon( 1e-7 )
{
};

//============================================================================

CNormalEquationMatrix::~CNormalEquationMatrix() {};

//============================================================================

unsigned long CNormalEquationMatrix::getIndex(unsigned long row, unsigned long col)
{
	if ( row > col )
	{
		unsigned long u = col;
		col = row;
		row = u;
	};

	return ( ( col * ( col + 1 ) ) / 2 + row );
};

void CNormalEquationMatrix::setEpsilon( double Epsilon )
{
	if ( Epsilon > DBL_EPSILON ) this->epsilon = Epsilon;
};

//============================================================================

void CNormalEquationMatrix::initialize ( unsigned long unknowns, double value )
{
	if ( unknowns > 0 )
	{
		this->nUnknowns = unknowns;
		doubles::resize(numElements( unknowns ));
	};
};

//============================================================================

void CNormalEquationMatrix::initialize (double value )
{
	for (unsigned int i = 0; i < this->size(); ++i) doubles::operator [](i) = value;
};

//============================================================================

void CNormalEquationMatrix::addSubMatrix(int row, int col, CAdjustMatrix &Nsub ) 
{
	if (row > col)
	{
		int i = row; row = col; col = i;
		Nsub.transpose();
	};

	unsigned long FirstIndex = getIndex(row,col);

	int delta = col + 1;

	if (row == col)
	{
		for ( int c = 0; c < Nsub.getCols(); c++, FirstIndex +=  delta)
		{
			unsigned long Index = FirstIndex;
			for ( int r = 0; r <= c; r++, Index++)
			{
				doubles::operator [](Index) += Nsub(r,c);
			};
		}
	}
	else
	{
		for ( int c = 0; c < Nsub.getCols(); c++, FirstIndex +=  delta)
		{
			unsigned long Index = FirstIndex;
			for ( int r = 0; r < Nsub.getRows(); r++, Index++)
			{
				doubles::operator [](Index) += Nsub(r,c);
			};
		}
	}
};

//============================================================================

int CNormalEquationMatrix::choleskyError( int i, double &Nii, double &Rii ) const
{
	i = -i;

	Nii = i;
	Rii = Nii;
	return i;
};

//============================================================================

bool CNormalEquationMatrix::solveCholesky( CNormalEquationMatrix &N, int &nErrors ) 
{
	bool ret = true;
	if ( N.getNUnknowns() != this->nUnknowns ) ret = false;
	else
	{
		nErrors = 0;
		double nii, rii;
		unsigned int jxa, ix, jx;
		unsigned int jxb = 0;
		unsigned int Index = jxb; int i = 0 ;
		double sum = N[ Index ];
		i++;
		if ( sum <= this->epsilon )
		{
			choleskyError( i, N[ Index ], rii );
			nErrors++;
		}
		else
		{
			doubles::operator [](Index) = sqrt( sum );
			Index++;
			do
			{
				jxa = Index; ix = jxb;
				sum = N[ Index ];
				do
				{
					doubles::operator [](Index) = sum / doubles::operator [](ix);
					Index++;
					sum = N[ Index ];
					nii = sum;
					jx = jxa;
					ix++;
					while ( jx < Index )
					{
						sum -= doubles::operator [](ix) * doubles::operator [](jx);
						jx++;
						ix++;
					};
				}
				while ( ix < Index );

				i = abs( i ) + 1;
				if ( sum <= epsilon * nii )
				{
					choleskyError( i, nii, rii );
					nErrors ++;
					if ( nii == 0.0 ) nii = 1.0;
					doubles::operator [](Index) = nii;
				}
				else doubles::operator [](Index) = sqrt( sum );
				Index++;
			}
			while ( Index < this->size() );
		};
	};

	return ret;
};

//============================================================================

bool CNormalEquationMatrix::invertR( const CNormalEquationMatrix &R ) 
{
	bool ret = true;
	if (R.getNUnknowns() != this->nUnknowns) ret = false;
	else
	{
		int ibo;
		int Index = this->size() - 1;
		int jr = this->nUnknowns;
		while ( Index >= 0 )
		{
			int ir = ibo = Index;
			doubles::operator [](Index) = 1 / R[ ir ];
			Index --;
			while ( ir > 1 )
			{
				double sum = 0;
				ir = Index;
				int incr = jr;
				for ( int ib = ibo; ib >= Index + 1; ib-- )
				{
				sum -= R[ ir ] * doubles::operator [](ib);
				incr--;
				ir -= incr;
				};
				doubles::operator [](Index) = sum / R[ ir ];
				Index--;
			};
			jr--;
		};
	};

	return ret;
};

//============================================================================

bool CNormalEquationMatrix::solveQxx( const CNormalEquationMatrix &RinvT ) 
{
	bool ret = true;
	if ( RinvT.getNUnknowns() != this->nUnknowns) ret = false;
	else
	{
		int ibo = this->size() - 1;
		int ix, jx, i, nx, j, incr;
		int Index;
		i = jx = Index = incr = 0;
		nx = j = -1;
		while ( Index < ibo )
		{
			nx++; j += nx + 1;
			incr = nx;
			do
			{
				ix = i; jx = j;
				double sum = 0 ;
				while ( jx <= ibo )
				{
					sum += RinvT[ ix ] * RinvT[ jx ];
					incr++;
					ix += incr;
					jx += incr;
				};

				doubles::operator [](Index) = sum;
				Index++; i++; incr = nx;
			}
			while ( ix != jx );
		};
	};

	return ret;
};

//============================================================================

bool CNormalEquationMatrix::invert( CNormalEquationMatrix &N ) 
{
	int nErrors = 0;
	bool ret = solveCholesky( N, nErrors );

	if ( ret)
	{
		ret = invertR( *this );

		if (ret) ret = solveQxx( *this );
	};
	return ret;
};

//****************************************************************************
//
//   E n d   o f   c l a s s   C N o r m a l E q u a t i o n M a t r i x
//
//****************************************************************************

//****************************************************************************
//
//  Class       : CtVector
//
//  Description :
//
//****************************************************************************

CtVector::CtVector( unsigned long unknowns ) : doubles( unknowns ) {};

//============================================================================

void CtVector::initialize (unsigned long unknowns, double value)
{
	if ( unknowns > 0 ) 
	{
		doubles::resize(unknowns);
		if (fabs(value) > DBL_EPSILON) this->initialize(value);
	}
};

//============================================================================

void CtVector::initialize (double value)
{
	for (unsigned int i = 0; i < this->getNUnknowns(); ++i)
	{
		(*this)[i] = value;
	};
};

//============================================================================

void CtVector::addVector(int row, CAdjustMatrix &Tsub)
{
	for ( long r = 0; r < Tsub.getRows(); ++r)
	{
		(*this)[r + row] += Tsub(r,0);
	};
};

//============================================================================

bool CtVector::reduceFromR( const CNormalEquationMatrix &R, const CtVector &Atl )
{
	bool ret = true;
	if ((R.getNUnknowns() != getNUnknowns()) || (Atl.getNUnknowns() != getNUnknowns()))
		ret = false;
	else
	{
		int jxa, ix, jx;
		int jxb = -1;
		for ( unsigned long Index = 0; Index < getNUnknowns(); Index++ )
		{
			jxb += Index + 1;
			jxa = jxb - Index;
			jx = 0;
			(*this)[Index] = Atl[ Index ];
			for ( ix = jxa; ix < jxb; ix++ )
			{
				(*this)[ Index ] -= R( ix ) * (*this)[ jx ];
				jx++;
			};
			(*this)[ Index ] = (*this)[ Index ] / R( jxb );		
		};
	};

	return ret;
};

//============================================================================

bool CtVector::reduceFromRinv( const CNormalEquationMatrix &RinvT, const CtVector &Atl ) 
{
	bool ret = true;
	if ((RinvT.getNUnknowns() != getNUnknowns()) || (Atl.getNUnknowns() != getNUnknowns()))
		ret = false;
	else
	{
		int ix, jx;
		int jxb = RinvT.numElements() - getNUnknowns();
		int jxa = RinvT.numElements() - 1;
		for (int Index = getNUnknowns() - 1; Index >= 0; Index-- )
		{
			jx = Index;
			(*this)[ Index ] = 0;
			for ( ix = jxa; ix >= jxb; ix-- )
			{
				(*this)[ Index ] += RinvT( ix ) * Atl[ jx ] ;
				jx--;
			};
			jxb -= Index;
			jxa -= Index - 1;
		};
	};

	return ret;
};

//============================================================================

bool CtVector::solveFromR( const CNormalEquationMatrix &R, const CtVector &g )
{
	bool ret = true;
	if ((R.getNUnknowns() != getNUnknowns()) || (g.getNUnknowns() != getNUnknowns()))
		ret = false;
	else
	{
		int jx = 0;
		int jxb = R.numElements();
		for ( int Index = getNUnknowns() - 1; Index >= 0; Index-- )
		{
			jxb--;
			int ix = jxb;
			int lx = getNUnknowns() - 1;
			jx++;
			(*this)[ Index ] = g[ Index ];
			for ( int l = 1; l < jx; l++ )
			{
				(*this)[ Index ] -= R( ix ) * (*this)[ lx ];
				ix -= lx;
				lx--;
			};
			(*this)[ Index ] /= R( ix );
		};
	};

	return ret;
};

//============================================================================

bool CtVector::solveFromRinv(const CNormalEquationMatrix &RinvT, const CtVector &g) 
{
	bool ret = true;
	if ((RinvT.getNUnknowns() != getNUnknowns()) || (g.getNUnknowns() != getNUnknowns()))
		ret = false;
	else
	{
		int ix, jx;
		int jxa = 0;
		int jxb = RinvT.numElements() - getNUnknowns();
		for ( unsigned long Index = 0; Index < getNUnknowns(); Index++ )
		{
			jx = Index;
			ix = jxa;
			(*this)[ Index ] = 0;
			while ( ix <= jxb )
			{
				(*this)[ Index ] += RinvT( ix ) * g[ jx ];
				jx++;
				ix += jx;
			};

			jxb++;
			jxa += Index + 2;
		};
	};

	return ret;
};

//============================================================================

bool CtVector::solve(CNormalEquationMatrix &N, const CtVector &Atl)
{
	bool ret = true;
	int nErrors = 0;
	CNormalEquationMatrix R(N.getNUnknowns());
	if ( !R.numElements() ) ret = false;
	else
	{
		ret = R.solveCholesky(N, nErrors );
		if (ret && (nErrors == 0 ))
		{
			ret = reduceFromR(R, Atl );
			if (ret) ret = solveFromR(R, *this );
		}
		else ret = false;
	};
	return ret;
};

bool CtVector::solve(CNormalEquationMatrix &N, CNormalEquationMatrix &Qxx, const CtVector &Atl)
{
	bool ret = true;
	int nErrors = 0;
	CNormalEquationMatrix R(N.getNUnknowns());
	if ( !R.numElements() ) ret = false;
	else
	{
		ret = R.solveCholesky(N, nErrors );
		if (ret && (nErrors == 0 ))
		{
			ret = reduceFromR(R, Atl );
			if (ret) ret = solveFromR(R, *this );
			ret = R.invertR(R);
			if (ret) ret = Qxx.solveQxx( R );
		}
		else ret = false;
	};
	return ret;
};

//============================================================================

//****************************************************************************
//
//   E n d   o f   c l a s s   C t v e c t o r
//
//****************************************************************************
/*
//==========================================================================
 
 double PH_vtPv::get_Qvv(int I)
  {
   Init_Ci(); 
   Nc = I;
   if (Status.is_set(ini_Ci))
    { if ((Nc < 0) || (Nc != Ni))
       { 
        Nc = abs(Nc);
        Ni = Nc;
        C = Ci;
        A_to_C();
       };
      if (Nc == 0) Qvv = 2; else
       {
        if (Status.is_set(ini_Cj))
         {
          delete (Cj) ;
          Nj = Ni;
          Status.del(ini_Cj);
         };
        Cj = Ci;
        C_to_Q();
       };
    } else Qvv = -2; 
   return(1-Qvv); 
  };
 
//==========================================================================
 
 double PH_vtPv::get_Qvv(int I,int J)
  {
   Init_Ci();
   Init_Cj();
   if ((!Status.is_set(ini_Ci)) || (!Status.is_set(ini_Cj)) 
        || ((I == 0) || (J == 0))) Qvv = -1; else
    {
     if ((Ni<0) || (Ni !=I))
      {
       Ni = Nc = abs(I);
       C = Ci;
       A_to_C();
      };
     if ((Nj<0) || (Nj !=J))
      {
       Nj = Nc = abs(J);
       C = Cj;
       A_to_C();
      };
     C_to_Q();
    };
   return Qvv;
  };
 
//=========================================================================
 
 void PH_vtPv::SigmaNaught(){
  if (Status.is_set(cal_x)  && (Rows_of_A<=default_Size_of_A)) 
   {
   Init_v(); 
   vtPv = 0;
   int i = 0; 
   int iright = i+N_Unknowns;
   int ileft = i;
   while(ileft<Rows_of_A){
    double sum = 0;
    int ix = 0; 
    while(i<iright){
     sum+=(*A)[i]*(*x)[ix];
     ix++; i++;
    };
     (*v)[ileft] = sum - (*l)[ileft]; 
     vtPv +=(*v)[ileft]*(*v)[ileft]; 
    iright+=N_Unknowns;
    ileft++;
   };
  Status.set(cal_v);
  } else 
     {
      Status.del(cal_v);
      if (Status.is_set(cal_x)) Error = Too_many_Rows;
       else Error = x_too_early;
     };
 };
 
//==========================================================================
 
 void PH_vtPv::SigmaNaught(unsigned int rows)
  {
   Rows_of_A = rows;
   SigmaNaught();
  };
 
//==========================================================================
 
 void PH_vtPv::A_to_C()
  {
   if ((Status.is_set(cal_Rinv)) && (Status.is_set(ini_A)))
    {
     int ax0 = (Nc - 1) * (N_Unknowns);
     int ax;
     int rx,rxup;
     rx = rxup = 0;
     Index = 0;
     while (Index<N_Unknowns)
      {
       (*C)[Index] = 0; 
       ax = ax0;
       while(rx<=rxup)
        {
         (*C)[Index] += (*A)[ax]*(*B)[rx];
         ax++; rx++;
        };
       Index++;
       rxup += Index +1;
      };
    } else
       {
        if (!Status.is_set(cal_Rinv)) Error = Rinv_too_early;
 
       };
  };
 
//==========================================================================
 
 void PH_vtPv::to_Q()
  {
   if ((Status.is_set(ini_Ci)) && ((Status.is_set(ini_Cj)) || (Ci == Cj)))
    {
     Index = 0;
     Qvv = 0;
     while (Index<N_Unknowns)
      {
       Qvv += (*Ci)[Index] * (*Cj)[Index];
       Index++;
      };
    };
  };
 
*/
