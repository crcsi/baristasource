/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <strstream>
using namespace std;

#include <math.h>
#include <float.h>
#include "DempsterShafer.h"


//****************************************************************************
//****************************************************************************

//=============================================================================

CDempsterShafer::CDempsterShafer () : conflict(0.0f), nClasses(0)
{
};

//=============================================================================

CDempsterShafer::CDempsterShafer (unsigned long Classes, unsigned long Sensors): 
            conflict(0.0f), nClasses(Classes)
{
	init(Classes, Sensors);
};

//=============================================================================

CDempsterShafer::~CDempsterShafer ()
{
	deleteVectors();
};

//=============================================================================
    
void CDempsterShafer::init(unsigned long Classes, unsigned long Sensors)
{
	unsigned long nCombined = (unsigned long) pow(2.0, (double) Classes) - 1;
	this->initialProbMasses.resize(Sensors, 0);
	this->combinedProbMasses.resize(nCombined, 0.0f);
	this->intersection.resize(nCombined * nCombined);
	this->names.resize(nCombined);
	this->support.resize(nCombined, 0.0f);
	this->plausibility.resize(nCombined, 0.0f);
	this->classNames.resize(nCombined);
	this->sensorNames.resize(Sensors);

	this->nClasses = Classes;
	this->conflict = 0.0f;

	initVectors(Classes);

	unsigned long index = 0; 

	for (unsigned long i = 0; i < this->nClasses; ++i)
	{
		CCharString str;
		this->addCombinations(str, 0, i, index,0);
	};
  
	index = 0;

	for (unsigned long i = 0; i < this->names.size(); ++i)
	{
		for (unsigned long j = 0; j < this->names.size(); ++j, ++index)
		{
			CCharString intersStr;
			CCharString part = this->names[i];

			int next = part.Find(" ");

			while (next > 0)
			{
				CCharString currentStr = part.Left(next);
				part = part.Right(part.GetLength() - next - 1);
				if (this->names[j].Find(currentStr) >= 0) intersStr += currentStr + " ";
				next = part.Find(" ");
			};

			int intersRes = -1;

			for (unsigned long u = 0; (u < this->names.size()) && (intersRes < 0); ++u)
			{
				if (intersStr == this->names[u]) intersRes = u;
			};

			intersection[index] = intersRes;
		};
	};

	this->classNames = this->names;

	for (unsigned long i = 0; i < this->getNSensors(); ++i)
	{
		this->sensorNames[i] = "S";
		this->sensorNames[i].addInteger(i);
	};
};

//=============================================================================
  
void CDempsterShafer::addCombinations(CCharString &currentString, int level,
									  int nElements, unsigned long &index, 
									  int start)
{
	if (level == nElements)
	{
		while (start < (int) this->nClasses)
		{
			this->names[index] = currentString;
			this->names[index].addInteger(start);
			this->names[index] += " ";
			++index;
			++start;
		};
	}
	else
	{	
		while (start < (int) this->nClasses)
		{
			CCharString str = currentString;
			str.addInteger(start); 
			str += " ";
			this->addCombinations(str, level + 1, nElements, index, start + 1);
			++start;
		};
	}; 
};

//=============================================================================
    
CCharString CDempsterShafer::getRuleString(eDECISIONRULE rule)
{
	CCharString str;

	switch(rule)
	{
       case eMAXSUP        : str = "Maximum support"; break;
       case eMAXSUPLGDUB   : str = "Maximum support larger than dubiety"; break;
       case eMAXMASS       : str = "Maximum probability mass"; break;
       case eMAXMASSLGDUB  : str = "Maximum probability mass larger than dubiety"; break;
       case eMAXPLAUS      : str = "Maximum plausibility"; break;
       case eMAXPLAUSLGDUB : str = "Maximum plausibility larger than dubiety"; break;
       default             : str = "Not valid!"; break;
	};


	return str;
};

//=============================================================================
    
void CDempsterShafer::setSensorName(unsigned long Sensor, const char *name)
{
	this->sensorNames[Sensor] = name;
};

//=============================================================================
    
void CDempsterShafer::setClassName(unsigned long Class, const char *name)
{
	CCharString str; 
	str.addInteger(Class);

	for (unsigned long i = 0; i < this->classNames.size(); i++)
	{
		this->classNames[i].replace(str, name);
	};
};

//=============================================================================

CCharString CDempsterShafer::getPrintString() const
{
  
	ostrstream ostrstr;
	ostrstr << "\nDempster Shafer probabilities:\n==============================\n\n";
	ostrstr.setf(ios::fixed,ios::floatfield);
	ostrstr.precision(3);

	ostrstr.width(this->getNClasses() * 2 + 3);
  
	ostrstr<< " Class | ";
	for (unsigned long j = 0; j < this->getNSensors(); ++j)
	{
		ostrstr.width(6);
		ostrstr << this->sensorNames[j];
	};
 
	ostrstr << " | Combined | Support | Plausib. | Dubiety \n";
 
	for (unsigned long j = 0; j < this->getNSensors() * 6 + this->getNClasses() * 2 + 3; ++j)
	{
		ostrstr << "-";
	};
  
	ostrstr << "------------------------------------------\n";
 
	for (unsigned long i = 0; i < this->getNCombinedClasses(); ++i)
	{
		ostrstr.width(this->getNClasses() * 2);
		ostrstr <<this->classNames[i] <<" | ";

		for (unsigned long j = 0; j < this->getNSensors(); ++j)
		{
			ostrstr.width(6);
			ostrstr << (*(this->initialProbMasses)[j])[i];
		};

		ostrstr << " | ";
		ostrstr.width(8);
		ostrstr << this->combinedProbMasses[i];
		ostrstr << " | ";
		ostrstr.width(7);
		ostrstr << this->support[i];
		ostrstr << " | ";
		ostrstr.width(8);
		ostrstr << this->plausibility[i];
		ostrstr << " | ";
		ostrstr.width(6);
		ostrstr << this->getDubiety(i);
		ostrstr<<"\n"; 
	};


	for (unsigned long j = 0; j < this->getNSensors() * 6 + this->getNClasses() * 2 + 3; ++j)
	{
		ostrstr << "-";
	};
 
	ostrstr << "------------------------------------------\n";
	ostrstr << "Conflict: ";
	ostrstr.width(6);
	ostrstr << this->conflict << "\n"; 

	for (unsigned long j = 0; j < this->getNSensors() * 6 + this->getNClasses() * 2 + 3; ++j)
	{
		ostrstr << "-";
	};
  
	ostrstr << "------------------------------------------\n" << ends;
  
	char *z = ostrstr.str();
  
	CCharString str(z);
	delete [] z;

	return str;
};

//=============================================================================

void CDempsterShafer::computeCombinedProbabilityMasses()
{
	for (unsigned long u = 0; u < this->getNCombinedClasses(); ++u) 
	{
		double currentProbability = (*initialProbMasses[0])[u];
		int intersection = u;
		if (this->getNSensors() > 1)
			this->computeProbabilityMasses(currentProbability, intersection, 1);
		else this->combinedProbMasses[u] = (float) currentProbability; 
	};

	double OneMinusConflict = 1.0f / (1.0f - this->conflict);
	for (unsigned long u = 0; u < this->getNCombinedClasses(); ++u)
	{
		this->combinedProbMasses[u] *= (float) OneMinusConflict;
	};

	for (unsigned long i = 0; i < this->getNCombinedClasses(); ++i)
	{
		for (unsigned long j = 0; j < this->getNCombinedClasses(); ++j)
		{
			if (this->getIntersection(i,j) == j)
			{
				this->support[i] += this->combinedProbMasses[j];
			};
		};
	};

	for (unsigned long i = 0; i < this->getNCombinedClasses(); ++i)
	{
		for (unsigned long j = 0; j < this->getNCombinedClasses(); ++j)
		{
			if (this->getIntersection(i,j) >= 0)
			{
				this->plausibility[i] += this->combinedProbMasses[j];
			};
		};
	};
};

//============================================================================

float CDempsterShafer::getDubiety(unsigned long Class) const 
{ 
	if (Class == this->getNCombinedClasses() - 1) return 0.0f;
	unsigned long negationClass = this->getNCombinedClasses() - (2 + Class);
	return this->support[negationClass]; 
};

//============================================================================

int CDempsterShafer::getMaxSupportLgDubietyClass() const
{
	int Class = this->getNClasses();
	double suppMax = -FLT_MAX;
	for (unsigned long u = 0; u < this->getNClasses(); ++u)
	{
		if ((this->support[u] > this->getDubiety(u)) && (this->support[u] > suppMax))
		{
			Class = u;
			suppMax = this->support[u];
		};
	};

	return Class;
};

//============================================================================

int CDempsterShafer::getMaxSupportClass() const
{
	int Class = this->getNClasses();
	double suppMax = -FLT_MAX;

	for (unsigned long u = 0; u < this->getNClasses(); ++u)
	{
		if (this->support[u] > suppMax)
		{ 
			Class = u;
			suppMax = this->support[u];
		};
	};

	return Class;
};

//============================================================================

int CDempsterShafer::getMaxProbabilityMassLgDubietyClass() const
{
	int Class = this->getNClasses();
	double probMax = -FLT_MAX;

	for (unsigned long u = 0; u < this->getNClasses(); ++u)
	{
		if ((this->support[u] > getDubiety(u)) && (this->combinedProbMasses[u] > probMax))
		{
			Class = u;
			probMax = this->combinedProbMasses[u];
		};
	};

	return Class;
};

//============================================================================

int CDempsterShafer::getMaxProbabilityMassClass() const
{
	int Class = getNClasses();
	double probMax = -FLT_MAX;

	for (unsigned long u = 0; u < this->getNClasses(); ++u)
	{
		if (this->combinedProbMasses[u] > probMax)
		{
			Class = u;
			probMax = this->combinedProbMasses[u];
		};
	};

	return Class;
};

//============================================================================

int CDempsterShafer::getMaxPlausibilityLgDubietyClass() const
{
	int Class = getNClasses();
	double plausMax = -FLT_MAX;

	for (unsigned long u = 0; u < this->getNClasses(); ++u)
	{
		if ((this->support[u] > getDubiety(u)) && (this->plausibility[u] > plausMax))
		{
			Class = u;
			plausMax = this->plausibility[u];
		};
	};

	return Class;
};

//============================================================================

int CDempsterShafer::getMaxPlausibilityClass() const
{
	int Class = getNClasses();
	double plausMax = -FLT_MAX;

	for (unsigned long u = 0; u < getNClasses(); ++u)
	{
		if (this->plausibility[u] > plausMax)
		{
			Class = u;
			plausMax = this->plausibility[u];
		};
  };

  return Class;
};

//============================================================================

int CDempsterShafer::getClass(eDECISIONRULE rule) const
{
	int Class = 0;
	if (rule == eMAXSUP)             Class = getMaxSupportClass();
	else if (rule == eMAXSUPLGDUB)   Class = getMaxSupportLgDubietyClass();
	else if (rule == eMAXPLAUS)      Class = getMaxPlausibilityClass();
	else if (rule == eMAXMASS)       Class = getMaxProbabilityMassClass();
	else if (rule == eMAXPLAUSLGDUB) Class = getMaxPlausibilityLgDubietyClass();
	else if (rule == eMAXMASSLGDUB)  Class = getMaxProbabilityMassLgDubietyClass();

	return Class;
};
    
//============================================================================

int CDempsterShafer::getRankBestClass(eDECISIONRULE rule, int rank, float &value) const
{
	vector<float> localValues; 
	if ((rule == eMAXSUP) || (rule == eMAXSUPLGDUB)) localValues = this->support;
	else if ((rule == eMAXPLAUS) || (rule == eMAXPLAUSLGDUB)) localValues = this->plausibility;
    else if ((rule == eMAXMASS) || (rule == eMAXMASSLGDUB)) localValues = this->combinedProbMasses;
  
	vector<int> classVector(this->nClasses);
	for (unsigned long i = 0; i < this->nClasses; ++i) classVector[i] = i;

	for (unsigned long i = 0; i < this->nClasses; ++i)
	{
		int iMax = i;
		float valMax = localValues[i];
		for (unsigned long j = i + 1; j < this->nClasses; ++j)
		{
			if (localValues[j] > valMax)
			{
				iMax = j;			
				valMax = localValues[j];
			}; 
		};
   
		int iSwap = classVector[iMax];
		classVector[iMax] = classVector[i];
		classVector[i] = iSwap;
		float fSwap = localValues[iMax];
		localValues[iMax] = localValues[i];
		localValues[i] = fSwap;
	};

	value = localValues[rank];

	return classVector[rank];
};

//============================================================================

void CDempsterShafer::resetInitialProbabilityMasses()
{
	for (unsigned long u = 0; u < this->getNSensors(); ++u)
	{
		vector<float> *vec = this->initialProbMasses[u];
		for (unsigned long i = 0; i < this->getNSensors(); ++i) (*vec)[i] = 0.0f;
	};
};

//============================================================================

void CDempsterShafer::resetAllProbabilityMasses()
{
	resetInitialProbabilityMasses();  
	resetFinalProbabilityMasses();
};

//============================================================================

void CDempsterShafer::resetFinalProbabilityMasses()
{
	for (unsigned long u = 0; u < this->combinedProbMasses.size(); ++u)
	{
		this->combinedProbMasses[u] = 0.0f;
		this->support[u] = 0.0f;
		this->plausibility[u] = 0.0f;
	}

	this->conflict = 0.0f;
};

//============================================================================

void CDempsterShafer::assignProbabilityMass(unsigned long Class, 
											unsigned long Sensor, 
                                            float m)
{
	(*initialProbMasses[Sensor])[Class] = m;
};

//============================================================================

void CDempsterShafer::initVectors(unsigned long Classes)
{
	for (unsigned long u = 0; u < this->getNSensors(); ++u)
	{
		this->initialProbMasses[u] = 
			new vector<float>((unsigned long) pow(2.0, (double) Classes) - 1, 0.0f);
	};
 };

//=============================================================================

void CDempsterShafer::deleteVectors()
{
	for (unsigned long u = 0; u < this->getNSensors(); ++u)
	{
		delete this->initialProbMasses[u];
		this->initialProbMasses[u] = 0;
	};
};

//============================================================================

void CDempsterShafer::computeProbabilityMasses(double currentProbabilityMass, 
											   int intersection, int Sensor)
{
	if (fabs(currentProbabilityMass) > FLT_EPSILON)
	{
		int Class = 0;
		if (Sensor + 1 == this->getNSensors())
		{
			while (Class < (int) this->getNCombinedClasses())
			{
				int locIntersection = this->getIntersection(intersection, Class);
				float prob = (float) currentProbabilityMass * (*initialProbMasses[Sensor])[Class];
				if (locIntersection < 0) this->conflict += prob;
				else this->combinedProbMasses[locIntersection] += prob;
				++Class;
			};
		}
		else
		{
			while (Class < (int) this->getNCombinedClasses())
			{
				int locIntersection = this->getIntersection(intersection, Class);
				double prob = currentProbabilityMass * (*initialProbMasses[Sensor])[Class];
				if (locIntersection < 0) this->conflict += (float) prob;
				else this->computeProbabilityMasses(prob, locIntersection, Sensor + 1);
				++Class;
			};
		};
	};
};

//=============================================================================


//****************************************************************************
