#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

#include "3DPointArray.h"

C3DPointArray::C3DPointArray(int nGrowBy) : 
 CMUObjectArray<C3DPoint, C3DPointPtrArray>(nGrowBy)
{
}

C3DPointArray::~C3DPointArray(void)
{
}

void C3DPointArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CCharString valueStr;
	CCharString elementStr;
	CStructuredFileSection* pNewSection; 

	for (int i=0; i< this->GetSize(); i++)
	{
		C3DPoint* point = this->GetAt(i);
		pNewSection = pStructuredFileSection->addChild();
		point->serializeStore(pNewSection);
	}
}

void C3DPointArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("C3DPoint"))
		{
			C3DPoint* point = this->Add();
			point->serializeRestore(pChild);
		}
		
	}
}

bool C3DPointArray::isModified()
{
	return this->bModified;
}

void C3DPointArray::reconnectPointers()
{
	CSerializable::reconnectPointers();
}


C3DPointArray& C3DPointArray::operator=(const C3DPointArray& oldArray)
{
	if (this == &oldArray)
		return *this;

	this->RemoveAll();

	// copy all points
	for (int i=0;i < oldArray.GetSize();i++)
		this->Add(*(oldArray.GetAt(i)));

	return *this;
}

C3DPoint* C3DPointArray::Add()
{
	C3DPoint* pPoint = CMUObjectArray<C3DPoint, C3DPointPtrArray>::Add();
	pPoint->setParent(this);
	return pPoint;
}

C3DPoint* C3DPointArray::Add(const C3DPoint& point)
{
	C3DPoint* pNewPoint = CMUObjectArray<C3DPoint, C3DPointPtrArray>::Add(point);
	pNewPoint->setParent(this);
	return pNewPoint;
}

void C3DPointArray::resetPointers()
{
	CSerializable::resetPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		C3DPoint* pPointArray = this->GetAt(i);
		pPointArray->resetPointers();
	}
}

/** 
 * perfmorms a 3D point transformation on all points in array
 */
void C3DPointArray::transform(Matrix* pTransMat)
{
	for (int i = 0; i < this->GetSize(); i++)
	{ 
		C3DPoint* point = this->GetAt(i);
		point->transform(pTransMat);
	}
}

C3DPoint* C3DPointArray::getPointByID(int ID)
{
	for (int i = 0; i < this->GetSize(); i++)
	{
		C3DPoint* pTestPoint = this->GetAt(i);
		
		if(pTestPoint->id == ID)
			return pTestPoint;
	}

	return NULL;
}
