#include "Token.h"
#include "Serializable.h"

//CMemoryAllocator CToken::memoryAllocator(sizeof(CToken));
#define SERIALIZABLE_LENGTH 8
#define DOUBLE_LENGTH 26
#define INT_LENGTH 11
#define BOOL_LENGTH 5
CToken::CToken(void)
{
    this->token = NULL;
    this->value = NULL;
    this->key = NULL;
}

CToken::CToken(const char* str)
{
    int len = ::strlen(str) +1;
    this->token = new char[len];

    ::memset(this->token, 0, len);

	this->parseToken();
}

bool CToken::isKey(const char* key)
{
    if (::strcmp(this->key, key) == 0)
        return true;

    return false;
}

void CToken::setToken(const char* str)
{
    int len = ::strlen(str) +1;

    if (this->token)
        delete [] this->token;
    this->token = NULL;

    this->token = new char[len];

    ::strcpy(this->token, str);

	this->parseToken();
}

void CToken::parseToken()
{
    this->key = ::strtok(this->token, "=");
    this->value = ::strtok(NULL, "=");
}

const char* CToken::getKey()
{
	return this->key;
}

CToken::~CToken(void)
{
    if (this->token)
        delete [] this->token;
    this->token = NULL;
}

void CToken::setPointerValue(const char* key, CSerializable* pSerializable)
{
    int len = ::strlen(key) + SERIALIZABLE_LENGTH + 2;

    if (this->token != NULL)
        delete [] this->token;
    this->token = NULL;

    this->token = new char[len];
	sprintf(this->token, "%s=%p", key, pSerializable);
    this->parseToken();
}

void CToken::setValue(const char* key, const char* pChar)
{
    int len = ::strlen(key) + ::strlen(pChar) + 2;

    if (this->token != NULL)
        delete [] this->token;
    this->token = NULL;

    this->token = new char[len];

	sprintf(this->token, "%s=%s", key, pChar);
    this->parseToken();
}

void CToken::setValue(const char* key, double d)
{
    int len = ::strlen(key) + DOUBLE_LENGTH + 2;

    if (this->token != NULL)
        delete [] this->token;
    this->token = NULL;

    this->token = new char[len];
	sprintf(this->token, "%s=%.18e", key, d);
    this->parseToken();
}

void CToken::setValue(const char* key, int i)
{
    int len = ::strlen(key) + INT_LENGTH + 2;

    if (this->token != NULL)
        delete [] this->token;
    this->token = NULL;

    this->token = new char[len];
	sprintf(this->token, "%s=%d", key, i);
    this->parseToken();
}

void CToken::setValue(const char* key, bool b)
{
    int len = ::strlen(key) + BOOL_LENGTH + 2;

    if (this->token != NULL)
        delete [] this->token;
    this->token = NULL;

    this->token = new char[len];

    if (b)
	    sprintf(this->token, "%s=true", key);
    else
	    sprintf(this->token, "%s=false", key);

    this->parseToken();
}

const char* CToken::getValue()
{
	return this->value;
}

unsigned long CToken::getUnsignedLongValue()
{
	unsigned long longValue;
	::sscanf(this->value, "%lu", &longValue);
	return longValue;
}

CSerializable* CToken::getPointerValue()
{
	CSerializable* pointer;
	::sscanf(this->value, "%p", &pointer);
	return pointer;
}

double CToken::getDoubleValue()
{
	double doubleValue;
	::sscanf(this->value, "%lf", &doubleValue);
	return doubleValue;
}

// parse the value into separate double values (used for matrix serialization)
void CToken::getDoubleValues(double* pDoubleArray, int n)
{
    char* cp = this->value;
    char* tmp = new char[::strlen(cp)+1];
    ::strcpy(tmp, cp);

    char* token = ::strtok(tmp, " ");

 	for(int i = 0; (i < n) && (token != NULL); i++)
	{
		::sscanf(token, "%lf", &pDoubleArray[i]);
        token = ::strtok(NULL, " ");
	}

    delete [] tmp;
}

int CToken::getIntValue()
{
	int intValue;
	::sscanf(this->value, "%d", &intValue);
	return intValue;
}

bool CToken::getBoolValue()
{
    if(::strcmp(this->value, "true") == 0)
		return true;
	else
		return false;
}

void CToken::getDoublesClass(doubles& pDoubles)
{
    char* cp = this->value;
    char* tmp = new char[::strlen(cp)+1];
    ::strcpy(tmp, cp);


    char* token = ::strtok(tmp, " ");

 	for(unsigned int i = 0; (i < pDoubles.size()) && (token != NULL); i++)
	{
		::sscanf(token, "%lf", &pDoubles[i]);
        token = ::strtok(NULL, " ");
	}

    delete [] tmp;
}

void CToken::getVectorClass(vector<int>& intArray)
{
    char* cp = this->value;
    char* tmp = new char[::strlen(cp)+1];
    ::strcpy(tmp, cp);


    char* token = ::strtok(tmp, " ");

 	for(unsigned int i = 0; (i < intArray.size()) && (token != NULL); i++)
	{
		::sscanf(token, "%d", &intArray[i]);
        token = ::strtok(NULL, " ");
	}

    delete [] tmp;
}

