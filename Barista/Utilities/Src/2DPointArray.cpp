#include "2DPointPtrArray.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
#include "2DPointArray.h"

C2DPointArray::C2DPointArray(int nGrowBy) : 
 CMUObjectArray<C2DPoint, C2DPointPtrArray>(nGrowBy)
{
}

C2DPointArray::C2DPointArray(const C2DPointArray &ptarray) : 
 CMUObjectArray<C2DPoint, C2DPointPtrArray>(ptarray)
{
}

C2DPointArray::~C2DPointArray(void)
{
}
	
C2DPointArray &C2DPointArray::operator = (const C2DPointArray &ptarray)
{
	if (this != &ptarray)
	{
		this->RemoveAll();
		for (int i = 0; i < ptarray.GetSize(); ++i)
		{
			this->Add(*ptarray.GetAt(i));
		}
	}

	return *this;
};

void C2DPointArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CCharString valueStr;
	CCharString elementStr;
	CStructuredFileSection* pNewSection; 

	for (int i=0; i< this->GetSize(); i++)
	{
		C2DPoint* point = this->GetAt(i);
		pNewSection = pStructuredFileSection->addChild();
		point->serializeStore(pNewSection);
	}

}

void C2DPointArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("C2DPoint"))
		{
			C2DPoint* point = this->Add();
			point->serializeRestore(pChild);
		}	
	}
}

bool C2DPointArray::isModified()
{
	return this->bModified;
}

void C2DPointArray::reconnectPointers()
{
	CSerializable::reconnectPointers();
}

C2DPoint* C2DPointArray::Add()
{
	C2DPoint* pPoint = CMUObjectArray<C2DPoint, C2DPointPtrArray>::Add();
	pPoint->setParent(this);
	return pPoint;
}

C2DPoint* C2DPointArray::Add(const C2DPoint& point)
{
	C2DPoint* pNewPoint = CMUObjectArray<C2DPoint, C2DPointPtrArray>::Add(point);
	pNewPoint->setParent(this);
	return pNewPoint;
}


void C2DPointArray::resetPointers()
{
	CSerializable::resetPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		C2DPoint* pPointArray = this->GetAt(i);
		pPointArray->resetPointers();
	}
}

