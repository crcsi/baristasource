#include <math.h>
#include "doubles.h"

doubles::doubles(void)
{
}

doubles::doubles(int n)
{
	if (n > 0)
		this->resize(n);

	for (int i = 0; i < n; i++)
		(*this)[i] = 0.0;

}

doubles::doubles(int n, double val)
{
	if (n > 0)
		this->resize(n);

	for (int i = 0; i < n; i++)
		(*this)[i] = val;

}

doubles::~doubles(void)
{
}

void doubles::swapElements(long i1, long i2)
{
	double element = (*this)[i2];
	(*this)[i2] = (*this)[i1];
	(*this)[i1] = element;
};

void doubles::PartialSort(long low, long high)
{
	if ( low < high )   // stopping condition for recursion!
	{
		long lo = low;
		long hi = high + 1;
		double element = (*this)[low];

		for ( ; ; )
		{
			while ((*this)[ ++lo ] < element );
			while ((*this)[ --hi ] > element );
			if ( lo < hi ) this->swapElements( lo, hi );
			else break;
		}
		this->swapElements( low, hi );
		PartialSort( low, hi - 1 );
		PartialSort( hi + 1, high );
	}
};

void doubles::sort()
{
	// put the greatest value to the last position in the vector:
	long maxI = size() - 1;
	if ( maxI > 0 )
	{
		double maxD = (*this)[ maxI ];
		for ( long i = maxI - 1; i >= 0; --i )
			if ( (*this)[ i ] > maxD ) 
			{ 
				maxI = i; 
				maxD = (*this)[ maxI ]; 
			}
			this->swapElements( maxI, this->size()  - 1);
			PartialSort( 0, size() - 2); 
	}
}

double doubles::round(double num)
{
	double d = floor(num);
	double diff = num-d;
	if (diff >= 0.5)
		return d+1;
	else
		return d;
}

void doubles::PartialSort(long low, long high, doubles &indices)
{
	if ( low < high )   // stopping condition for recursion!
	{
		long lo = low;
		long hi = high + 1;
		double element = (*this)[low];

		for ( ; ; )
		{
			while ((*this)[ ++lo ] < element );
			while ((*this)[ --hi ] > element );
			if ( lo < hi ) 
			{
				this->swapElements( lo, hi );
				indices.swapElements(lo,hi);
			}
			else break;
		}
		this->swapElements( low, hi );
		indices.swapElements(low, hi);
		PartialSort( low, hi - 1, indices);
		PartialSort( hi + 1, high, indices);
	}
};

void doubles::sort(doubles &indices)
{
	// put the greatest value to the last position in the vector:
	indices.clear();
	indices.resize(this->size());
	for (unsigned int i = 0; i < this->size(); i++)
		indices[i] = i;
	long maxI = this->size() - 1;
	if ( maxI > 0 )
	{
		double maxD = (*this)[ maxI ];
		for ( long i = maxI - 1; i >= 0; --i )
			if ( (*this)[ i ] > maxD ) 
			{ 
				maxI = i; 
				maxD = (*this)[ maxI ]; 
			}
			this->swapElements( maxI, this->size()  - 1);
			indices.swapElements(maxI, this->size()  - 1);
			PartialSort( 0, size() - 2,indices); 
	}
}