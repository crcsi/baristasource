#include <float.h>
#include "sun.h"
#include "utilities.h"

//============================================================================

// const double sun::rho_ = M_PI / 180.0;
// const double sun::d_90_  = M_PI / 2.0;

//============================================================================
   
double sun::getTimeHours(struct tm *timeStruct)
{ 
	double timeHrs = (double(timeStruct->tm_sec) / 60.0 + double(timeStruct->tm_min)) / 60.0;
	timeHrs += double(timeStruct->tm_hour);
	return timeHrs; 
};

//============================================================================

double sun::getTimeSeconds(struct tm *timeStruct)
{
	int month = timeStruct->tm_mon + 1; 
	int year = timeStruct->tm_year + 1900; 

	int i = (month + 9) / 12;
	i = (7 * (year + i)) / 4;

	int j = (275 * month) / 9;

	double yr = (double) year;
	double dt = -i + j + timeStruct->tm_mday;

	return ((367.0f * yr + dt - 694006.5 + (sun::getTimeHours(timeStruct) / 24.0)) / 36525.0);
};

//============================================================================

void sun::computeEphemeries(struct tm *timeStruct, 
                            double &rightAscension, double &declination, 
                            double &gha, double &semidiam)
{
	double timeSec = sun::getTimeSeconds(timeStruct);

  	double m  = fmod((358.475 + 35999.050 * timeSec), 360.0) * D2R;
	double v  = fmod((   63.0 +   22518.0 * timeSec), 360.0) * D2R;
	double q  = fmod((  332.0 +   33718.0 * timeSec), 360.0) * D2R;
	double j  = fmod((  222.0 +   32964.0 * timeSec), 360.0) * D2R;
	double om = fmod((  101.0 +    1934.0 * timeSec), 360.0) * D2R; 

	double m2 = 2.0 * m;
	double v2 = 2.0 * v;
	double v3 = 3.0 * v;
	double j2 = 2.0 * j;

	double meanLong          = fmod((279.69019 + 36000.76892 * timeSec), 360.0); 
	double eqationOfCentre   = (1.91945 - 0.00479 * timeSec) * sin(m) + 
                                0.02 * sin(m2) + 0.00029*sin(3.0 * m);   

	double lunarPerturbation = 0.00179 * cos((261.0  + 445267.0 * timeSec) * D2R);
    
	double rVenus   = 0.00134 * cos(M_PI * 0.5 + v) + 0.00154 * cos(M_PI * 0.5 + v2) +
                      0.00069 * cos(258.0 * D2R + v2 - m) +
                      0.00043 * cos( 78.0 * D2R + v3 - m) +
                      0.00028 * cos( 51.0 * D2R + v3 - m2);

	double rMars    = 0.00057 * cos(M_PI * 0.5 + q) + 
                      0.00049 * cos(306.0 * D2R + q - m);

	double rJupiter = 0.00200 * cos( 91.0 * D2R + j) +
                      0.00076 * cos(270.0 * D2R + j2) +
                      0.00072 * cos(175.0 * D2R + j - m) +
                      0.00045 * cos(293.0 * D2R + j2 - m);

	double nutationLong = 0.00479 * cos(M_PI * 0.5 - om) + 0.00035 * cos(295.0* D2R + m2);
    
	double apparentLongitude = (meanLong + eqationOfCentre + lunarPerturbation + 
                                rVenus + rMars + rJupiter + nutationLong) * D2R;

	double obliquity = fmod((23.45229 - 0.01301 * timeSec + 0.00256*cos(om)), 360.0)  * D2R;
    
	rightAscension = atan2(sin(apparentLongitude) * cos(obliquity), cos(apparentLongitude));    

	declination = asin(sin(apparentLongitude) * sin(obliquity));

	gha = getTimeHours(timeStruct) * 15.0 + 99.69130 + 36000.76892 * timeSec +  0.917 * nutationLong - rightAscension / D2R;
	gha = fmod(gha, 360.0) * D2R;

	semidiam = 0.26696 + 0.00447 * cos(m);
};

//============================================================================

C2DPoint sun::getHorizonCoordinates(struct tm *timeStruct,
                                    double longitude, 
									double latitude,
                                    double meridianConvergence )
{
	double rightAscension, declination, gha, semidiam;

	latitude             *= D2R;
	longitude            *= D2R;
	meridianConvergence  *= D2R;
  
	sun::computeEphemeries(timeStruct, rightAscension, declination, gha, semidiam);

	double hourAngle = longitude + gha;

	hourAngle = fmod(hourAngle, 2.0 * M_PI);

	C2DPoint horizon;

// spherical cosine theorem:  
// cos(z) = cos(90-lat) * cos(90-dec) + sin(90-lat) * sin (90-lat) * cos(hour)
//        = sin(lat) * sin (dec) + cos(lat) * cos (dec) * cos (hour)


	double cosdec = cos(declination);
	horizon.y = acos(sin(latitude) * sin(declination) +  cos(latitude) * cosdec * cos(hourAngle));
	horizon.x = sin(horizon.y);
  
	if (fabs(horizon.x) < FLT_EPSILON) horizon.x = 0.0;
	else horizon.x = sin(hourAngle) * cosdec / horizon.x;
  
	horizon.x = asin(horizon.x);
	
	horizon.x += M_PI;
	horizon.x += meridianConvergence;

	return horizon;
};


//============================================================================

C3DPoint sun::getDirectionVector(struct tm *timeStruct,
                                 double longitude, 
								 double latitude,
                                 double meridianConvergence)
{
	C3DPoint dirHz = getHorizonCoordinates(timeStruct, longitude, latitude, meridianConvergence);
  
	dirHz.x = M_PI * 0.5 - dirHz.x;
	

	C3DPoint dirCart;
	double sinZ = sin(dirHz.y);
	dirCart.x = -cos(dirHz.x) * sinZ;
	dirCart.y = -sin(dirHz.x) * sinZ;
	dirCart.z = -cos(dirHz.y);

	return dirCart;
};

//============================================================================

double sun::getRefraction(double elevation, double temperature, double pressure)
{
	double dataArr[][3] ={
             7.50, 6.88, 0.15,
             7.67, 6.75, 0.15,
             7.83, 6.62, 0.15,
             8.00, 6.50, 0.15,
             8.17, 6.37, 0.15,
             8.33, 6.25, 0.15,
             8.50, 6.13, 0.15,
             8.67, 6.02, 0.15,
             8.83, 5.92, 0.15,
             9.00, 5.82, 0.15,
             9.17, 5.72, 0.15,
             9.33, 5.83, 0.15,
             9.50, 5.53, 0.15,
            10.00, 5.26, 0.15,
            10.33, 5.10, 0.15,
            10.67, 4.95, 0.14,
            11.00, 4.81, 0.14,
            11.33, 4.67, 0.14,
            11.67, 4.54, 0.14,
            12.00, 4.42, 0.14,
            12.30, 4.25, 0.14,
            13.00, 4.09, 0.14,
            13.50, 3.93, 0.14,
            14.00, 3.78, 0.14,
            14.30, 3.65, 0.14,
            15.00, 3.53, 0.14,
            15.50, 3.42, 0.14,
            16.00, 3.32, 0.14,
            16.50, 3.32, 0.14,
            17.00, 3.12, 0.14,
            17.50, 3.02, 0.14,
            18.00, 2.93, 0.14,
            18.50, 2.85, 0.14,
            19.00, 2.77, 0.14,
            19.50, 2.70, 0.14,
            20.00, 2.62, 0.14,
            21.00, 2.48, 0.14,
            22.00, 2.36, 0.14,
            23.00, 2.25, 0.14,
            24.00, 2.15, 0.14,
            25.00, 2.05, 0.14,
            26.00, 1.96, 0.13,
            27.00, 1.88, 0.13,
            28.00, 1.80, 0.13,
            29.00, 1.73, 0.13,
            30.00, 1.66, 0.13,
            32.00, 1.53, 0.13,
            34.00, 1.42, 0.13,
            36.00, 1.32, 0.13,
            38.00, 1.23, 0.12,
            40.00, 1.15, 0.11,
            42.00, 1.07, 0.11,
            44.00, 1.00, 0.11,
            46.00, 0.93, 0.10,
            48.00, 0.86, 0.10,
            50.00, 0.80, 0.09,
            55.00, 0.67, 0.08,
            60.00, 0.55, 0.07,
            65.00, 0.45, 0.06,
            70.00, 0.35, 0.05,
            80.00, 0.17, 0.03,
            90.00, 0.00, 0.00 };

	double pmult = 0.0009744 * pressure    + 0.02;
	double tmult = -0.003085 * temperature + 1.03;
	double vert  = elevation * M_PI / 180.0;
  
	for (int i = 0; i < 60 ; i++)
	{
		if((vert > dataArr[i][0]) && (vert < dataArr[i+1][0]))
		{
			double de       = dataArr [i + 1][0] - dataArr [i][0];
			double dr       = dataArr [i + 1][1] - dataArr [i][1];
			double dp       = dataArr [i + 1][2] - dataArr [i][2];
			double fraction = vert - dataArr[i][0];
			double ref      = dataArr [i][1] + dr * (fraction / de);
			double parallax = dataArr [i][2] + dp * (fraction / de);
			ref             = (ref * pmult * tmult - parallax) * 60.0;

			return ref;
		};

	};
    
	return 0.0;
};

//============================================================================
