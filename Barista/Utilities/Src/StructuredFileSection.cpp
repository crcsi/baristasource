#include "StructuredFileSectionArray.h"
#include "SystemUtilities.h"
#include "StructuredFileSection.h"

CStructuredFileSection::CStructuredFileSection(void): beg(-1), end(-1), pParent(0)
{

	this->pChildren = new CStructuredFileSectionArray();
}

CStructuredFileSection::CStructuredFileSection(const CFileName &projectPath): 
			beg(-1), end(-1), pParent(0), ProjectPath(projectPath)
{
	this->pChildren = new CStructuredFileSectionArray();
}

CStructuredFileSection::~CStructuredFileSection(void)
{
	if(this->pChildren != NULL)
	{
		delete this->pChildren;
		this->pChildren = NULL;
	}
}

CStructuredFileSection* CStructuredFileSection::getParent()
{
	return this->pParent;
}

void CStructuredFileSection::setParent(CStructuredFileSection* pParent)
{
	this->pParent = pParent;
}

CStructuredFileSectionArray* CStructuredFileSection::getChildren()
{
	return this->pChildren;
}

CStructuredFileSection* CStructuredFileSection::addChild()
{
	CStructuredFileSection* pChild = this->pChildren->Add();
	pChild->setParent(this);
	pChild->setProjectPath(this->ProjectPath);
	return pChild;
}

CToken* CStructuredFileSection::addToken()
{
	return this->tokens.Add();
}

CTokenArray* CStructuredFileSection::getTokens()
{
	return &this->tokens;
}


void CStructuredFileSection::setName(const CCharString &Name)
{
	this->name = Name;
}

void CStructuredFileSection::setName(const string& Name)
{
	this->name = Name.c_str();
}

void CStructuredFileSection::setName(const char* pName)
{
	this->name = pName;
}

void CStructuredFileSection::setProjectPath(const CFileName &projectPath)
{
	this->ProjectPath = projectPath;
	if (!this->ProjectPath.IsEmpty())
	{
		if (this->ProjectPath[this->ProjectPath.GetLength() - 1] != CSystemUtilities::dirDelim)
		{
			this->ProjectPath += CSystemUtilities::dirDelimStr;
		}
	}
};
