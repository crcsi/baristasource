#include "PrimeFinder.h"

CPrimeFinder::CPrimeFinder(void)
{
}

CPrimeFinder::~CPrimeFinder(void)
{
}

CMUIntegerArray* CPrimeFinder::getPrimes(int nPrimes)
{
    this->primes.RemoveAll();

    this->primes.Add(2);

    int x = 3;

    while(this->primes.GetSize() < nPrimes)
    {
        bool prime = true;     // prime is 1, not prime is 0      
//        for(int i = 2; i <= x/2; i++)  
        for(int i = 0; i < this->primes.GetSize(); i++)  
        {
            int *pInt = this->primes.GetAt(i);
//            int z = x % i;     // z = remainder of x divided by div
            int z = x % *pInt;     // z = remainder of x divided by div

            if (z == 0)      // if the remainder IS ZERO
                prime = false;    // then x was NOT A PRIME
        }
        if (prime) 
        {
            this->primes.Add(x);
        }

        x+=2;
    }

    return &this->primes;
}
