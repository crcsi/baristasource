#include <math.h>
#include <vector>
using namespace std;

#include "newmat.h"
#include "statistics.h"
#include "utilities.h"

float statistics::normalQuantil = 1.96f;

float statistics::chiSquareQuantils[MAXDOF] = 
     { 1.00000000f,  3.841455338f,  5.991476357f,  7.814724703f,  9.487728465f, 
      11.07048257f, 12.591577420f, 14.067127260f, 15.507312490f, 16.918960160f };


float fisherTable[380] = 
{
              1.00f,   2.00f,   3.00f,   4.00f,   5.00f,   6.00f,   7.00f,   8.00f,   9.00f,  10.00f,  15.00f,  20.00f,  25.00f,  30.00f,  40.00f,  50.00f, 100.00f, 200.00f,1000.00f,
/*    1 */  161.00f, 200.00f, 216.00f, 225.00f, 230.00f, 234.00f, 237.00f, 239.00f, 241.00f, 242.00f, 246.00f, 248.00f, 249.00f, 250.00f, 251.00f, 252.00f, 253.00f, 254.00f, 254.00f,
/*    2 */   18.51f,  19.00f,  19.16f,  19.25f,  19.30f,  19.33f,  19.35f,  19.37f,  19.38f,  19.39f,  19.45f,  19.44f,  19.45f,  19.46f,  19.47f,  19.48f,  19.49f,  19.49f,  19.49f,
/*    3 */   10.13f,   9.99f,   9.28f,   9.12f,   9.01f,   8.94f,   8.89f,   8.85f,   8.81f,   8.79f,   8.70f,   8.66f,   8.64f,   8.62f,   8.59f,   8.58f,   8.55f,   8.54f,   8.53f,
/*    4 */    7.71f,   6.94f,   6.59f,   6.39f,   6.26f,   6.16f,   6.09f,   6.04f,   6.00f,   5.96f,   5.85f,   5.80f,   5.77f,   5.75f,   5.72f,   5.70f,   5.66f,   5.65f,   5.63f,
/*    5 */    6.61f,   5.79f,   5.41f,   5.19f,   5.05f,   4.95f,   4.88f,   4.82f,   4.77f,   4.74f,   4.62f,   4.56f,   4.53f,   4.50f,   4.46f,   4.44f,   4.41f,   4.39f,   4.36f,
/*    6 */    5.99f,   5.14f,   4.76f,   4.53f,   4.39f,   4.28f,   4.21f,   4.15f,   4.10f,   4.06f,   3.94f,   3.87f,   3.84f,   3.81f,   3.77f,   3.75f,   3.71f,   3.69f,   3.67f,
/*    7 */    5.59f,   4.74f,   4.35f,   4.12f,   3.97f,   3.87f,   3.79f,   3.73f,   3.68f,   3.64f,   3.51f,   3.44f,   3.41f,   3.38f,   3.34f,   3.32f,   3.27f,   3.25f,   3.23f,
/*    8 */    5.32f,   4.46f,   4.07f,   3.84f,   3.69f,   3.58f,   3.50f,   3.44f,   3.39f,   3.35f,   3.22f,   3.15f,   3.11f,   3.08f,   3.05f,   3.02f,   2.97f,   2.95f,   2.93f,
/*    9 */    5.12f,   4.26f,   3.86f,   3.63f,   3.48f,   3.37f,   3.29f,   3.23f,   3.18f,   3.14f,   3.01f,   2.93f,   2.89f,   2.86f,   2.83f,   2.80f,   2.76f,   2.73f,   2.71f,
/*   10 */    4.96f,   4.10f,   3.71f,   3.48f,   3.33f,   3.22f,   3.14f,   3.07f,   3.02f,   2.98f,   2.84f,   2.77f,   2.73f,   2.70f,   2.66f,   2.64f,   2.59f,   2.56f,   2.54f,
/*   15 */    4.54f,   3.68f,   3.29f,   3.06f,   2.90f,   2.79f,   2.71f,   2.64f,   2.59f,   2.54f,   2.40f,   2.33f,   2.29f,   2.25f,   2.20f,   2.18f,   2.12f,   2.10f,   2.07f,
/*   20 */    4.35f,   3.49f,   3.10f,   2.87f,   2.71f,   2.60f,   2.51f,   2.45f,   2.39f,   2.35f,   2.20f,   2.12f,   2.08f,   2.04f,   1.99f,   1.97f,   1.91f,   1.88f,   1.84f,
/*   25 */    4.24f,   3.39f,   2.99f,   2.76f,   2.60f,   2.49f,   2.40f,   2.34f,   2.28f,   2.24f,   2.09f,   2.01f,   1.96f,   1.92f,   1.87f,   1.84f,   1.78f,   1.75f,   1.71f,
/*   30 */    4.17f,   3.32f,   2.92f,   2.69f,   2.53f,   2.42f,   2.33f,   2.27f,   2.21f,   2.16f,   2.01f,   1.93f,   1.88f,   1.84f,   1.79f,   1.76f,   1.70f,   1.66f,   1.62f,
/*   40 */    4.08f,   3.23f,   2.84f,   2.61f,   2.45f,   2.34f,   2.25f,   2.18f,   2.12f,   2.08f,   1.92f,   1.84f,   1.79f,   1.74f,   1.69f,   1.66f,   1.59f,   1.55f,   1.51f,
/*   50 */    4.03f,   3.18f,   2.79f,   2.56f,   2.40f,   2.29f,   2.20f,   2.13f,   2.07f,   2.03f,   1.87f,   1.78f,   1.73f,   1.69f,   1.63f,   1.60f,   1.52f,   1.48f,   1.44f,
/*  100 */    3.94f,   3.09f,   2.70f,   2.46f,   2.31f,   2.19f,   2.10f,   2.03f,   1.97f,   1.93f,   1.77f,   1.68f,   1.62f,   1.57f,   1.52f,   1.48f,   1.39f,   1.34f,   1.28f,
/*  200 */    3.89f,   3.04f,   2.65f,   2.42f,   2.26f,   2.14f,   2.06f,   1.98f,   1.93f,   1.88f,   1.71f,   1.62f,   1.57f,   1.52f,   1.46f,   1.41f,   1.32f,   1.26f,   1.19f,
/* 1000 */    3.83f,   3.00f,   2.60f,   2.36f,   2.21f,   2.10f,   2.01f,   1.94f,   1.88f,   1.83f,   1.66f,   1.57f,   1.51f,   1.46f,   1.39f,   1.35f,   1.24f,   1.17f,   1.00f
};

long fisherIndex(long redundancy)
{
	if (redundancy <= 0)  return  0;
	if (redundancy >= 1000) return 18; 
	for (int i_ = 0; i_ < 19; ++i_)  
		if (fisherTable[i_] >= redundancy) return (i_ - 1);

	return 18;
};


double statistics::fisherFractil95(long red1, long red2)
{
	if ((red1 >= 1000)  && (red2 >= 1000)) return 1.0f;

	int index1 = fisherIndex(red1);
	int index2 = fisherIndex(red2);

	float fisherFract = 0.0f;

	int lineIndex1 = (index1 + 1) * 19;
	int lineIndex2 = (index2 + 2) * 19;


	if (red1 >= 1000)
	{
   
		fisherFract =  fisherTable[lineIndex2 + index1] - fisherTable[lineIndex1 + index1];
		fisherFract *= (red2 - fisherTable[index2]);
		fisherFract /= (fisherTable[index2 + 1] - fisherTable[index2]);
		fisherFract += fisherTable[lineIndex1 + index1];
	}
	else if (red2 >= 1000)
	{
		fisherFract = fisherTable[lineIndex1 + index1 + 1] - fisherTable[lineIndex1 + index1];
		fisherFract *= (red1 - fisherTable[index1]);
		fisherFract /= (fisherTable[index1 + 1] - fisherTable[index1]);
		fisherFract += fisherTable[lineIndex1 + index1];
	}
	else
	{
		float fisher1 = fisherTable[lineIndex1 + index1 + 1] - fisherTable[lineIndex1 + index1];
		fisher1 *= (red1 - fisherTable[index1]);
		fisher1 /= (fisherTable[index1 + 1] - fisherTable[index1]);
		fisher1 += fisherTable[lineIndex1 + index1];

		float fisher2 = fisherTable[lineIndex2+ index1 + 1] - fisherTable[lineIndex2 + index1];
		fisher2 *= (red1 - fisherTable[index1]);
		fisher2 /= (fisherTable[index1 + 1] - fisherTable[index1]);
		fisher2 += fisherTable[(index2 + 2) * 19 + index1];

		fisherFract = fisher1 + (fisher2 - fisher1) * (red2 - fisherTable[index2]) / (fisherTable[index2 + 1] - fisherTable[index2]);
	};
   
  
	return fisherFract;
};

double statistics::factorial(long n)
{
	if (n == 0) return 1.0f;
	double fact = (double ) abs(n);
	--n;

	while (n  > 1)
	{
		fact *= n;
		--n;
	};

	return fact;
};

double statistics::gammaNby2(long n)
{ 
	if (!(n % 2)) return factorial((n / 2) - 1);


	//double gamma = sqrt(M_PI) * pow(2.0f, 1 - n);
	double gamma = sqrt((double) M_PI);

	if (n > 1)
	{
	 	int i1 = (n - 1);
		int i2 = i1 / 2;

		while (i1 > i2)
		{
			gamma *= double(i1) * 0.5;
			--i1;
		};

		gamma *= pow(2.0, -i1);
	}
	return gamma;
};

double statistics::betaNMby2(double x, long n1, long n2)
{
	if (x > 0.5f) return (1.0f - betaNMby2(1.0f - x, n2, n1));

	double alpha   = double (n1) * 0.5f;
	double beta    = double (n2) * 0.5f;
	double prev    = 1.0f;
	double sum     = 1.0f;
	double counter = alpha + beta;
	double denom   = alpha + 1.0f;
  
	for (unsigned long u = 0; u < 100; ++u)
	{
		prev = prev * (counter * x) / denom;
		sum     += prev;
		counter += 1.0f;
		denom   += 1.0f;
	};

  
  
	return sum * gammaNby2(n2 + n1) * pow(x, alpha) * 
           pow(1.0f - x, beta) / 
          (gammaNby2(n1) * gammaNby2(n2) * alpha);
};

double  statistics::normalDistribution(double x)
{
	double a1 = 0.4361836f;
	double a2 = -0.1201676f;
	double a3 = 0.9372980;
	double p  = 0.33267f;
	double t;
	if (x < 0) t = 1.0f / (1.0f - p * x);
	else t = 1.0f / (1.0f + p * x);

	double dist = exp(-0.5f * x * x) * 
          (((a3 * t + a2) * t + a1) * t) / sqrt(2.0f * M_PI);

	if (x > 0)  dist = 1.0f - dist;

	return dist;
};

//=============================================================================

 double  statistics::normalDensity(double x)
 {
	 return (1.0f / sqrt(2.0f * M_PI)) * exp(-x * x * 0.5f);
 };

//=============================================================================

 double  statistics::normalFractil(double alpha)
 {
	 double c0 = 2.515517;
	 double c1 = 0.802853;
	 double c2 = 0.010328;
	 double d1 = 1.432788;
	 double d2 = 0.189269;
	 double d3 = 0.001308;
	 double t;

	 if (alpha < 0.5f) t = sqrt(-2*log(alpha));
	 else t = sqrt(-2*log(1.0f - alpha));
   
	 double fractil = t - (c0 + c1 * t + c2 * t * t) / (1.0f  + d1 * t + d2 * t * t + d3 * t * t * t);

	 if (alpha < 0.5f) fractil = -fractil;
	 return fractil;
 };

double  statistics::chiSquareDistribution(double x, long n)
{
	switch(n) 
	{
	 case 2: return chiSquareDistribution2(x);
		break;           

	 case 4: return chiSquareDistribution4(x);
		break;           

	 case 6: return chiSquareDistribution6(x);
		break;           

	 default: break;            
	};
  
	int i = n + 2;

	double summand = x / double (i);
	double sum = summand;
	i += 2;

	while (summand > 0.0000000001f)
	{
		summand *= x / double (i);
		sum += summand;
		i += 2;
	};
  
	sum += 1.0f;
	x /= 2.0f;

	double dist = pow(x, double (n) / 2.0f);
	double  dist1 =  gammaNby2(n + 2);
	dist = dist * exp(-x) / dist1;
	return dist * sum;
};

double statistics::chiSquareDistribution2(double x)
{
	return(double (1.0f) - exp(x * double (-0.5f)));
};

double statistics::chiSquareDistribution4(double x)
{
	x *= double (0.5f); 
	return(double (1.0f) - exp(-x) * (double (1.0f) + x)); 
};

double statistics::chiSquareDistribution6(double x)
{
	x *= double (0.5f); 
	return (double (1.0f) - exp(-x) * (1.0f + x + x * x * double (0.5f))); 
};

double statistics::chiSquareDensity(double x, long n)
{
	switch(n)
	{
	 case 2: return chiSquareDensity2(x);
		break;           
	 case 4: return chiSquareDensity4(x);
		break;           
 	 case 6: return chiSquareDensity6(x);
		break;           
	 default: break;           
	};

	double nby2 = (double ) n;
	nby2 *= 0.5f;

	return (exp(-x * 0.5f) * pow(x, nby2 - 1.0f) /(pow((double) 2.0f, nby2) * gammaNby2(n)));
};

double statistics::chiSquareDensity2(double x)
{
	return(double (0.5f) * exp(x * double (-0.5f))); 
};

double statistics::chiSquareDensity4(double x)
{
	return(double (0.25f) * x * exp(x * double (-0.5f)));
};

double statistics::chiSquareDensity6(double x)
{
	 return (double (0.0625f) *  x *  x * exp(x * double (-0.5f)));
};

double statistics::chiSquareFractil(double alpha, long n)
{
	double  fractil = 0.0f;

	if (n == 2) fractil = -2.0f * log(1.0f - alpha);
	else
	{
		double normFractil = normalFractil(alpha);
		double hlp = 2.0f / (9.0f * double (n));
		fractil = normFractil * sqrt(hlp) + 1.0f - hlp;
		fractil = double(n) * fractil * fractil * fractil;
		if (n < 250)
		{
			double  density = 1;
			double  alpha1 = alpha;

			do
			{
				fractil += (alpha - alpha1) / density;
				density = chiSquareDensity(fractil, n);
				alpha1 = chiSquareDistribution(fractil, n);
			} while (fabs(alpha - alpha1) > 0.0000001f);
		}
	};

  return fractil;
};
   
double statistics::fisherDensity(double x, long n1, long n2)
{
	double n1by2     = double(n1) * 0.5f;
	double n2by2     = double(n2) * 0.5f;
	double mpowm2    = pow(double(n1), n1by2);
	double npown2    = pow(double(n2), n2by2);
	double gammanm2  = gammaNby2(n1 + n2);
	double gammam2   = gammaNby2(n1);
	double gamman2   = gammaNby2(n2);
	double wDenom    = double(n2) + double(n1) * x;
	double wDenomPow = pow(wDenom, n1by2 + n2by2);
	double wpow      = pow(x, n1by2 - 1.0f);

	return gammanm2 * mpowm2 * npown2 * wpow / 
          (gammam2  * gamman2 * wDenomPow);
};

double statistics::fisherDistribution(double x, long n1, long n2)
{
	double w = double (n2) / (double (n2) + double (n1) * x); 
	return 1.0f - betaNMby2(w, n1, n2); 
};

double statistics::fisherFractil(double alpha, long n1, long n2)
{
	double fractil     = 0.0f;
	double normFractil = normalFractil(alpha);
	double lambda      = (normFractil * normFractil - 3.0f) / 6.0f;
	double help1       = (n1 == 1) ? 1.0f / double(n1) : 1.0f / (double(n1) - 1.0f);
	double help2       = (n2 == 1) ? 1.0f / double(n2) : 1.0f / (double(n2) - 1.0f);
	double h           = 2.0f / (help1 + help2);
  
	double  w = normFractil * sqrt(h + lambda) / h - 
		        (help1 - help2) * (lambda + 5.0f / 6.0f - 2.0f / (3.0f * h));

	fractil = exp(2.0f * w);

	double  density = 1;
	double  alpha1  = alpha;

	do    
	{
     fractil += (alpha - alpha1) / density;
       
     density = fisherDensity(fractil, n1,  n2);
     alpha1  = fisherDistribution(fractil, n1,  n2);
    } while (fabs(alpha - alpha1) > 0.0000001f);
 
	return fractil;
};

void statistics::getReducedVector(const Matrix &U1,       const Matrix &U2,
                                  const ColumnVector &X1, const ColumnVector &X2, 
                                  const Matrix &covar1,   const Matrix &covar2,
                                  int rank,               
								  ColumnVector &dist,     Matrix &covarDist)
{
	vector<unsigned int> indexVec(U1.Nrows());
	vector<double>       distVec (U1.Nrows());

	for (int i = 0; i < U1.Nrows(); ++i)
	{
		indexVec[i] = i;
		distVec [i] = U1.Row(i + 1).SumSquare() + U2.Row(i + 1).SumSquare();
	};

	for (int i = 0; i < U1.Nrows(); ++i)
	{
     double distMax = distVec[i];
     unsigned int indexMax = i;

	 for (int j = i + 1; j < U1.Nrows(); ++j)
	 {
		 if (distVec[j] > distMax)
		 {
			 indexMax = j;
			 distMax  = distVec[j];
		 };
	 };

	 if (indexMax != i)
	 {
		 float auxDist = (float) distVec[i];
		 distVec[i] = distVec[indexMax];
		 distVec[indexMax] = auxDist;
         unsigned int auxIndex = indexVec[i];
		 indexVec[i] = indexVec[indexMax];
		 indexVec[indexMax] = auxIndex;
	 };
	};

	Matrix U1red(rank, U1.Ncols());
	Matrix U2red(rank, U2.Ncols());

	for (int i = 0; i < rank; ++i)
	{
		
// g++ doesn't like this as GetSUbMatrix has only a private constructor!!
// to fix the problem we copy every value 
//		int currIdx = indexVec[i] + 1;
//     	U1red.Row(i + 1) = U1.Row(currIdx);
//		U2red.Row(i + 1) = U2.Row(currIdx);
     	
     	// we are using the element function, so the index is different!
     	int currIdx = indexVec[i];
     	
     	for (int k=0; k < U1.Ncols(); k++)
     		U1red.element(i, k) = U1.element(currIdx,k);
     		
     	for (int k=0; k < U1.Ncols(); k++)
     		U2red.element(i, k) = U2.element(currIdx,k);     		
     		
	};

	covarDist = U1red * covar2 * U1red.t() + U2red * covar1 * U2red.t();
	dist = U1red * X2;
};

double statistics::testMetric(const Matrix &U1,       const Matrix &U2,
                              const ColumnVector &X1, const ColumnVector &X2, 
                              const Matrix &covar1,   const Matrix &covar2,
                              int rank)
{
	ColumnVector distance(rank);
	Matrix covariance(rank, rank);
	getReducedVector(U1, U2, X1, X2, covar1, covar2, rank, distance, covariance);

	Matrix d = distance.t() * covariance.i() * distance;
	return d(1,1);
 };

void statistics::setNormalQuantil (float S)
{
	normalQuantil = float(fabs(normalFractil((1.0f - S) * 0.5f))); 
};

void statistics::setChiSquareQuantils(float S)
{
	chiSquareQuantils[0] = 1.0f;
	for (int i = 1; i < MAXDOF; ++i)
		chiSquareQuantils[i] = float(chiSquareFractil(S, i));
};
  