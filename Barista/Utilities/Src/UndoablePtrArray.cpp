#include "UndoablePtrArray.h"
#include "UndoAble.h"

CUndoablePtrArray::CUndoablePtrArray(int nGrowBy) :
    CMUObjectPtrArray<CUndoable>(nGrowBy)
{

}

CUndoablePtrArray::~CUndoablePtrArray(void)
{
    // Delete all object added to this ptr array
    while (this->GetSize() >0)
    {
        CUndoable* pUndo = this->GetAt(0);
        delete pUndo;
        this->RemoveAt(0);
    }
}

void CUndoablePtrArray::undo()
{
    if (this->GetSize() == 0)
        return;

    CUndoable* pUndo = this->GetAt(this->GetSize() - 1);
    pUndo->undo();

    delete pUndo;
    this->RemoveAt(this->GetSize() - 1);
}
