#include "ProtocolHandler.h"
#include "CharStringArray.h"
#include "SystemUtilities.h"

const int CProtocolHandler::scr = 2;
const int CProtocolHandler::err = 4;
const int CProtocolHandler::prt = 8;
const CCharString CProtocolHandler::defaultError =  "error.out";
const CCharString CProtocolHandler::defaultProt = "protocol.out";

#ifdef WIN32
CCharString CProtocolHandler::defaultDirectory = ".\\";
#else
CCharString CProtocolHandler::defaultDirectory = "./";
#endif



CProtocolHandler::CProtocolHandler(): errFile (0), protFile(0), scrStream(0)
{ 
	this->open(this->scr, "");
};

CProtocolHandler::~CProtocolHandler()
{
	this->close(this->scr + this->err + this->prt);
};
  	  
bool CProtocolHandler::isOpen(int maskOpen) const
{
	bool ret = true;
	if (maskOpen & this->err) ret &= (this->errFile != 0);
	if (maskOpen & this->prt) ret &= (this->protFile != 0);
	if (maskOpen & this->scr) ret &= (this->scrStream != 0);

	return ret;
};

bool CProtocolHandler::open(int maskOpen, CCharString name, bool append)
{
	bool ret = false;
	if (maskOpen == this->err)
    {
		if (name.IsEmpty()) name = this->defaultError;

		if (this->errFile) delete this->errFile;
		if (append) this->errFile = new ofstream(name.GetChar(), ios::app);
		else this->errFile = new ofstream(name.GetChar());
		if (!this->errFile->good()) 
		{
			delete this->errFile;
			this->errFile = 0;
		}
		else ret = true;
	}
	else if (maskOpen == this->prt)
    {
		if (name.IsEmpty())  name = this->defaultProt;

		if (this->protFile) delete this->protFile;
		if (append) this->protFile = new ofstream(name.GetChar(), ios::app);
		else this->protFile = new ofstream(name.GetChar());
		if (!this->protFile->good()) 
		{
			delete this->protFile;
			this->protFile = 0;
		}
		else ret = true;
	}
	else if (maskOpen == this->scr)
    {
		this->scrStream = &cout;
		ret = true;
	}

	return ret;
}

void CProtocolHandler::close(int maskClose)
{
	if (this->actError(maskClose))
    {		
		delete this->errFile;
		this->errFile = 0;
	}

	if (actProt(maskClose))
    {
		delete this->protFile;
		this->protFile = 0;
	}

	if (actScr(maskClose)) this->scrStream = 0;
};

void CProtocolHandler::print(const CCharString &text, int mask)
{
	if (actError(mask)) *this->errFile   << text << endl;
	if (actProt(mask))  *this->protFile  << text << endl;
	if (actScr(mask))   *this->scrStream << text << endl;
};

void CProtocolHandler::print(ostrstream &textstream, int mask)
{
	textstream << ends;
	char *z= textstream.str();
	CCharString text(z);
	delete[] z;
	this->print(text, mask);
};

void CProtocolHandler::setDefaultDirectory(const CCharString &dir) 
{ 
	this->defaultDirectory = dir;
	if (!this->defaultDirectory.IsEmpty())
	{
		if (this->defaultDirectory[this->defaultDirectory.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->defaultDirectory += CSystemUtilities::dirDelimStr;

	}
}

CProtocolHandler protHandler;

eOUTPUTMODE outputMode (eMEDIUM);