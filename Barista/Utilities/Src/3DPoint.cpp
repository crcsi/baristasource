
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
#include "3DPoint.h"
#include "newmat.h"

C3DPoint::C3DPoint(const CPointBase &p) : CPointBase(3), bSelected (false), 
			x(coordinates[0]), y(coordinates[1]), z(coordinates[2])
{  
	this->id = p.id; 
	this->dot = p.dot; 
	
	int minDim = p.getDimension() < this->dim ? p.getDimension() : this->dim;
	int i = 0;
	for (; i < minDim; ++i) this->coordinates[i] = p[i];
	for (; i < 3; ++i) this->coordinates[i] = 0.0;
};

//================================================================================

void C3DPoint::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CToken* pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("x", (double)this->x);

	pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("y", (double)this->y);

	pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("z", (double)this->z);

	pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("dot", (double)this->dot);

	pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("id", (int)this->id);


}

//================================================================================

void C3DPoint::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* pToken = pStructuredFileSection->getTokens()->GetAt(i);

		if(pToken->isKey("x"))
			this->x= (double) pToken->getDoubleValue();
		
		if(pToken->isKey("y"))
			this->y= (double) pToken->getDoubleValue();

		if(pToken->isKey("z"))
			this->z= (double) pToken->getDoubleValue();

		if(pToken->isKey("dot"))
			this->dot= (double) pToken->getDoubleValue();

		if(pToken->isKey("id"))
			this->id= (int) pToken->getIntValue();
	}


}

//================================================================================

bool C3DPoint::isModified()
{
	return this->bModified;
}

//================================================================================

void C3DPoint::reconnectPointers()
{
	CSerializable::reconnectPointers();
}

//================================================================================

void C3DPoint::resetPointers()
{
	CSerializable::resetPointers();
}

//================================================================================

void C3DPoint::setSelected(bool status)
{
	this->bSelected = status;
}

//================================================================================

bool C3DPoint::isSelected() const
{
	return this->bSelected;
}

//================================================================================

/**
 * Transforms the point to a coordinate system define by the given 4x4 trans matrix
 * @param pTransMat a 4x4 homogeniuos matrix
 *
 */
void C3DPoint::transform(Matrix* pTransMat)
{
	Matrix tmp(4,1);
	tmp(1,1) = this->x;
	tmp(2,1) = this->y;
	tmp(3,1) = this->z;
	tmp(4,1) = 1;
	
	Matrix transed = *pTransMat * tmp;

	this->x = transed.element(0, 0);
	this->y = transed.element(1, 0);
	this->z = transed.element(2, 0);
}

//================================================================================

bool C3DPoint::writeDXF( ostream& dxf, int *lineCodes, int decimals,
		                 double xfactor, double yfactor, double zfactor) const
{
  if (!dxf) return false;
  dxf.precision(decimals);
  dxf << lineCodes[ 0 ] << "\n" << x * xfactor << "\n" 
	  << lineCodes[ 1 ] << "\n" << y * yfactor << "\n"
      << lineCodes[ 2 ] << "\n" << z * zfactor << "\n70\n32\n";

  return dxf.good();
}
 