#include <strstream>
using namespace std;

#include "DempShSensorRecord.h"

//****************************************************************************
//
// C l a s s   S e n s o r R e c o r d
//
//****************************************************************************
   
//=============================================================================

CCharString CDempShSensorRecord::getParameterString() const
{
	ostrstream oss;
	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(5);

	oss << this->prbMin << " " << this->prbMax << " " 
        << this->Xmin   << " " << this->Xmax   << " " << ends;

	char *z = oss.str();
	CCharString str(z);
	delete [] z;

	return str;
};

//=============================================================================

void CDempShSensorRecord::initParametersFromString(const CCharString &str)
{
	istrstream iss(str.GetChar());

	iss >> this->prbMin;
	iss >> this->prbMax;
	iss >> this->Xmin;
	iss >> this->Xmax;
	this->diffProbMaxMin = this->prbMax - this->prbMin; 
    this->diffXMaxMin = this->Xmax - this->Xmin;
	this->OneByDiffXMaxMin = float(1.0) / this->diffXMaxMin;
};
   
//=============================================================================

bool CDempShSensorRecord::initEmpiricalModel(const CCharString &fileName)
{
	bool ret = true;
	CCharString str = "\nReading parameters for empirical model from file: ";
	str += fileName;
  

	ifstream infile(fileName);
	this->values.clear();
	this->probMasses.clear();

	int fileGood = infile.good();

	while (fileGood)
	{
		float val, prob;
		infile >> val;
   
		fileGood = infile.good();
		if (fileGood) infile >> prob;
		fileGood = infile.good();
		if (fileGood) 
		{
			this->values.push_back(val);
			this->probMasses.push_back(prob);
		}
	};
  
	if (this->values.size() < 2)
	{
		this->values.clear();
		this->probMasses.clear();
		ret = false;
   };

	return ret;
};

//=============================================================================

CCharString CDempShSensorRecord::getProtocolStringEmpirical(const CCharString &headerString) const
{ 
	if (!this->getNumberOfEmpiricalRecords()) 
		return getProtocolStringCubic(headerString);

	return headerString + "Empirical distribution";
};

//=============================================================================

float CDempShSensorRecord::getProbCubic(float X) const
{
	float m = this->prbMin;
	if (X > this->Xmin)
	{
		if (X > this->Xmax) m = this->prbMax;
		else
		{
			X = this->convert01(X);
			return (this->prbMin + this->diffProbMaxMin * X * X * (3.0f - 2.0f * X));
		}
	}

	return m;
};

//=============================================================================

float CDempShSensorRecord::getProbEmpirical(float X) const
{
	if (X <= this->values[0]) return this->probMasses[0];
	if (X >= this->values[this->values.size() - 1]) 
      return this->probMasses[this->probMasses.size() - 1];

	unsigned long u = 0;

	while (X > this->values[u]) ++u;

	float v1 = this->values[u - 1];
	float v2 = this->values[u];

	float p1 = this->probMasses[u - 1];
	float p2 = this->probMasses[u];
  
	return (p1 + (X - v1) * (p2 - p1) / (v2 - v1));
};
      
//=============================================================================

void CDempShSensorRecord::initEmpiricalModel(const vector<float> &Values, 
											 const vector<float> &ProbabilityMasses)
{
	this->values     = Values;
	this->probMasses = ProbabilityMasses;
};

//=============================================================================

CCharString CDempShSensorRecord::getDescriptor(const CCharString &sensorName, int precision) const
{
	ostrstream oss;
	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(precision);
	oss << sensorName << " Min: ";
	oss.width(10);
	oss << this->Xmin << ", Max: ";
	oss.width(10);
	oss << this->Xmax << ", Pmin: ";
  
	oss.precision(1);
	oss.width(5);
	oss << this->prbMin * 100.0f << "%, Pmax: ";
	oss.width(5);
	oss << this->prbMax * 100.0f << "%" << ends;

	char *z = oss.str();
	CCharString str(z);
	delete [] z;

	return str;
};
