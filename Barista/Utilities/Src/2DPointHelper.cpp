#include "2DPoint.h"

#include <math.h>
#include "2DPointHelper.h"


C2DPointHelper::C2DPointHelper(void)
{
}

C2DPointHelper::~C2DPointHelper(void)
{
}

double C2DPointHelper::distance(const C2DPoint &pt1, const C2DPoint &pt2)
{
	return sqrt( (pt2.x-pt1.x)*(pt2.x-pt1.x) + (pt2.y-pt1.y)*(pt2.y-pt1.y) );
}