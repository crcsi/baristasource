#include "ProgressListener.h"

CProgressListener::CProgressListener(): progressValue(0.0), titleString("")
{
}

CProgressListener::~CProgressListener()
{
}

void CProgressListener::setProgressValue(double percent)
{
    this->progressValue = percent;
}


double  CProgressListener::getProgress() const
{
    return this->progressValue;
}