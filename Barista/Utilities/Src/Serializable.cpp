/**
 * @class CSerializable
 *
 * base class of serialible objects
 * @see CStructuredFile
 * @see CStructuredFileSection
 * 
 * 
 * 
 * @author Harry Hanley
 * @version 1.0 
 * @date 20-jan-2004
 *
 */

#include "StructuredFile.h"


#include "Serializable.h"

CHandleManager CSerializable::handleManager;
int	CSerializable::restoreCount = 0;

/*
#ifdef _DEBUG
CPointerArray CSerializablePointerArray;
#endif
*/

CSerializable::CSerializable()
{
    /*
#ifdef _DEBUG
	CPointer* p = CSerializablePointerArray.Add();
	p->pointer = this;
#endif
*/

	// keep track of our pointer
	this->pOldPointer = this;
	this->pParent = NULL;
}

CSerializable::~CSerializable()
{
    /*
#ifdef _DEBUG
	for (int i = 0; i < CSerializablePointerArray.GetSize(); i++)
	{
		CPointer* p = CSerializablePointerArray.GetAt(i);
		if (p->pointer == this)
		{
			CSerializablePointerArray.RemoveAt(i);
			break;
		}
	}
#endif
    */
}

/**
 * 
 */
 
CSerializable* CSerializable::getBasePointer()
{
	return this;
}


void CSerializable::setModified()
{
	this->bModified = true;
}

CSerializable* CSerializable::getOldPointer()
{
	return this->pOldPointer;
}

void CSerializable::resetPointers()
{
	this->pOldPointer = this;
}

void CSerializable::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	this->bModified = false;

	// unique section label
	pStructuredFileSection->setName(this->getClassName());

	// unique handle
	CToken* pNewToken = pStructuredFileSection->addToken();
	pNewToken->setPointerValue("this", this->pOldPointer);

	// parent
	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setPointerValue("parent", this->pParent);
}

void CSerializable::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::restoreCount++;

	//@@@ check for divide by zero

	int percent = 0;

	if(CStructuredFile::sectionCount > 0)
		percent = 100*CSerializable::restoreCount / CStructuredFile::sectionCount ;

	this->bModified = false;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* pToken = pStructuredFileSection->getTokens()->GetAt(i);
		
//		CCharString key = pToken->getKey();

		if(pToken->isKey("this"))
		{
			this->pOldPointer = pToken->getPointerValue();
			
			this->handleManager.registerPointers(this->pOldPointer, this->getBasePointer());
		}
		else if(pToken->isKey("parent"))
			this->pParent = (CSerializable*)pToken->getPointerValue();
	}
}

void CSerializable::setParent(CSerializable* pParent)
{
	this->pParent = pParent;
}

CSerializable* CSerializable::getParent()
{
	return this->pParent;
}

void CSerializable::reconnectPointers()
{
	CSerializable* pOld = this->pParent;
	this->pParent = (CSerializable*)this->handleManager.getNewPointer(pOld);//, "CSerializable");
}

void CSerializable::setName(CCharString name)
{
	this->name = name;
}

void CSerializable::clone(CSerializable* pSrc)
{
	CStructuredFileSection section;

	if (pSrc->getClassName() != this->getClassName())
	{
		assert(false);
		return;
	}

	pSrc->serializeStore(&section);
	this->serializeRestore(&section);

	this->reconnectPointers();
	this->resetPointers();
	CSerializable::handleManager.reset();
}


CCharString CSerializable::getName()
{
	return this->name;
}


