/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#ifdef WIN32
#include <direct.h>
#include <io.h>
#else
#include <sys/param.h>
#include <stdlib.h>
#endif


#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>


#include "SystemUtilities.h"
#include "CharStringArray.h"


#ifdef WIN32
 const char CSystemUtilities::dirDelim = '\\';
 const CCharString CSystemUtilities::dirDelimStr = "\\";
#else
 const char CSystemUtilities::dirDelim = '/';
 const CCharString CSystemUtilities::dirDelimStr = "/";

// map function calls
#define _strtime(s) {time_t t = time(0); strftime(s, MAX_LENGTH, "%H:%M:%S", localtime (&t));}
#define _strdate(s) {time_t t = time(0); strftime(s, MAX_LENGTH, "%d %b %Y", localtime (&t));}

#endif




//==============================================================================

int CSystemUtilities::systemCommand( const CCharString &sysCommand )
{
	return system( sysCommand.GetChar() );
}

//==============================================================================

int CSystemUtilities::diskFree( unsigned int drive, double &totalSpace,
								double &freeSpace )
{


	unsigned int err( -1 ), nrDf( drive );

#ifdef WIN32
	if ( nrDf > 0 && nrDf <= 26 )
	{
		struct _diskfree_t df = {0};

		unsigned long DriveMask = _getdrives();

		if (DriveMask)
		{
			unsigned int dfr( 1 );
			bool isDf( false );
			while( DriveMask )
			{
			  if( DriveMask & 1 )
			  {
				if( nrDf == dfr )
				{
				  isDf = true;
				  break;
				}
			  }
			  ++dfr;
			  DriveMask >>= 1;
			}

			if (isDf )
			{
				err = _getdiskfree( nrDf, &df );
				if( !err )
				{
					double bytespercluster =  (double) df.sectors_per_cluster *
											  (double) df.bytes_per_sector;

					totalSpace = (double) df.total_clusters * bytespercluster;

					freeSpace  = (double) df.avail_clusters * bytespercluster;
				}
			}
		}
	}
 #else

 #endif
	return err;
}

//==============================================================================

int CSystemUtilities::dir(CCharStringArray &FileList, CCharString Path, CCharString Filter )
{
	FileList.RemoveAll();

	if ( Path.GetLength() == 0 ) Path = CCharString( "." ) + CSystemUtilities::dirDelimStr;
	else if ( Path[Path.GetLength()-1] != CSystemUtilities::dirDelim )
		Path += CSystemUtilities::dirDelimStr;

#ifdef WIN32
	//from: http://www.albert-weinert.de/vcfaq/html.htm

	WIN32_FIND_DATA w32fd;
	HANDLE hFind = FindFirstFile ( Path + Filter, &w32fd );
	if ( hFind == INVALID_HANDLE_VALUE ) return 1;
	do
	{
		CCharString fileName( w32fd.cFileName );
		FileList.Add(Path + fileName);

	} while ( FindNextFile ( hFind, &w32fd ) );

	FindClose( hFind );

	return 0;
#else
	const char *filename = "temp.tmp";

	CCharString SysCall = "ls " + Path + Filter + " > " + CCharString( filename );

	int error = systemCommand( SysCall );
	if ( error == 0 )
	{
		char *line = new char[256];
		ifstream Ifile( filename, ios::in );
		do
		{
		  Ifile.getline(line,256);
		  if ( !Ifile.good() ) error = 1;
		  else
		  {
			FileList.Add( line );
		  }
		}
		while ( ( Ifile.good() ) && ( !error ) );

		delete [] line;
		error = 0;
		Ifile.close();
		error = del( filename );
	}
	else del( filename );

	return error;
#endif
}


//==============================================================================

int CSystemUtilities::dirS(CCharStringArray &FileList,
						   CCharString Path,
						   CCharString Filter )
{
	FileList.RemoveAll();

	if ( Path.GetLength() == 0 ) Path = CCharString( "." ) + CSystemUtilities::dirDelimStr;
	else if ( Path[Path.GetLength()-1] != CSystemUtilities::dirDelim )
		Path += CSystemUtilities::dirDelimStr;


#ifdef WIN32
	// 1. find all subdirectories
	CCharStringArray subDirList;
    WIN32_FIND_DATA w32fd;
    HANDLE hFind = FindFirstFile ( Path + "*", &w32fd );

	if ( hFind == INVALID_HANDLE_VALUE ) return 1;
	// the directory "." should always be found! Otherwise Path does not exist, so return error

	do
	{
		if ( w32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			CCharString directoryName( w32fd.cFileName );
			if ( directoryName != CCharString(".") && directoryName != CCharString("..") )
				subDirList.Add( Path + directoryName + CSystemUtilities::dirDelimStr );
		}
	}
	while ( FindNextFile ( hFind, &w32fd ) );

	FindClose( hFind );

	// call dir in "Path"
	dir( FileList, Path, Filter );


	// 2. call recursively dir /s in all subdirectories
	for (int i = 0; i < subDirList.GetSize(); ++i)
	{
		CCharStringArray subDirFileList;

		if ( dirS( subDirFileList, *subDirList.GetAt(i), Filter ) == 0 )
		{
			for (int j = 0; j < subDirFileList.GetSize(); ++j)
				FileList.Add(*subDirFileList.GetAt(j));
		}
	}
#else

#endif

	return ( FileList.GetSize() == 0 ); // if nothing found return 1
}

//==============================================================================

int CSystemUtilities::delS( CCharString Path, CCharString Filter )
{
	CCharStringArray FileList;
	int err(0);

	if ( Path.GetLength() == 0 ) Path = CCharString( "." ) + CSystemUtilities::dirDelimStr;
	else if ( Path[Path.GetLength()-1] != CSystemUtilities::dirDelim )
		Path += CSystemUtilities::dirDelimStr;


#ifdef WIN32
	// 1. find all subdirectories
	CCharStringArray subDirList;
    WIN32_FIND_DATA w32fd;
    HANDLE hFind = FindFirstFile ( Path + "*", &w32fd );

	if ( hFind == INVALID_HANDLE_VALUE ) return 1;
	// the directory "." should always be found! Otherwise Path does not exist, so return error

	do
	{
		if ( w32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			CCharString directoryName( w32fd.cFileName );
			if ( directoryName != CCharString(".") && directoryName != CCharString(".."))
				subDirList.Add( Path + directoryName + CSystemUtilities::dirDelimStr );
		}
	}
	while ( FindNextFile ( hFind, &w32fd ) );

	FindClose( hFind );

	// 2. del files in "Path"

	dir( FileList,  Path );
	for (int i = 0; i < FileList.GetSize(); ++i) err = del( *FileList.GetAt(i) );

	// 3. del all files in all subdirectories

	for (int i = 0; i < subDirList.GetSize(); ++i)
    {
		  CCharStringArray subDirFileList;
		  dirS( subDirFileList, *subDirList.GetAt(i), Filter);
		  for (int j = 0; j < subDirFileList.GetSize(); ++j) err = del( *subDirFileList.GetAt(j) );

		  // call recursively del /s in all subdirectories
		  err = delS( *subDirList.GetAt(i), Filter );
		  if ( err ) return err;
		  // del current subdirectory
		  err = rmDir(*subDirList.GetAt(i) );
    }

	// 4. del directory path
	err = rmDir( Path );
#else

#endif

	return err;
}

//==============================================================================

int CSystemUtilities::del( const CCharString& filename )
{
	return remove(filename.GetChar());
}

//=============================================================================

CCharString CSystemUtilities::getCwd( int bufsize )
{

#ifdef WIN32
	char *cwd = new char [ bufsize ];
	_getcwd( cwd, bufsize );
	CCharString str(cwd);

	if ( str.GetLength() > 0 )
	{
		if (str[str.GetLength() - 1] != CSystemUtilities::dirDelim )
			str += CSystemUtilities::dirDelimStr;
	}
	delete [] cwd;
	return str;

#else
	return CCharString();
#endif
}

//=============================================================================

CCharString CSystemUtilities::evaluateEnvironmentVariables(const CCharString& Path )
{
	CCharString newPath = Path;
	CCharString newPathPart = Path;

#ifdef WIN32
	CCharString beginEnv = "%", endEnv = "%";
#else
	CCharString beginEnv = "$",  endEnv = CSystemUtilities::dirDelimStr;
#endif

	// evaluate environment variables and replace them on C_newPath

	int pos1 = newPathPart.Find(beginEnv);

	while (pos1 >= 0)
	{
		newPath = newPathPart.Left(pos1);
		newPathPart = newPathPart.Right(newPathPart.GetLength() - (pos1 + 1));

		int pos2 = newPathPart.Find(endEnv);
		CCharString envName = newPathPart.Left(pos2);
		CCharString evaluated = getenv( envName );
		if (evaluated.GetLength() > 0) newPath += evaluated;

		pos1 = newPathPart.Find( beginEnv);
		if (pos1 < 0) newPath += newPathPart;
	} // end while

	return newPath;
}
//=============================================================================

CCharString CSystemUtilities::getEnvVar( const CCharString& env )
{
	return CCharString( getenv( env.GetChar() ) );
}

//=============================================================================

int CSystemUtilities::chDir( CCharString& newDirectory )
{
	newDirectory = evaluateEnvironmentVariables(newDirectory);
#ifndef WIN32
	return chdir( newDirectory.GetChar() );
#else // PC
	return _chdir( newDirectory.GetChar() );
#endif
}

//=============================================================================

int CSystemUtilities::chDir( const CCharString& newDirectory )
{
	CCharString newDir = evaluateEnvironmentVariables(newDirectory);
#ifndef WIN32
	return chdir( newDir.GetChar());
#else // PC
	return _chdir( newDir.GetChar() );
#endif
}

//=============================================================================

int CSystemUtilities::move ( const CCharString& oldFileName,
                             const CCharString& newFileName,
							 bool overWrite )
{
	int error;

#ifndef WIN32
	  // rename (stdio.h) overwrites existing files
	  if (!overWrite && fileExists( newFileName ) ) error = 1;
	  else error = rename(oldFileName.GetChar(), newFileName.GetChar());
#else
	  // rename (stdio.h) gives an error if a file with the new name exists
	  if (overWrite && fileExists(oldFileName) && oldFileName != newFileName )
		  del( newFileName );
	  error = rename(oldFileName.GetChar(), newFileName.GetChar());
#endif

  return error;
}

//=============================================================================

int CSystemUtilities::copy(const CCharString& oldFileName,
						   const CCharString& newFileName )
{
#ifndef WIN32
	CCharString systemCall = "cp " + oldFileName + " " + newFileName;

	return systemCommand( systemCall );
#else //get rid of open DOS windows under NT

	CFileName filename(oldFileName);
	CCharString path = filename.GetPath();
	filename.SetPath("");

	CCharStringArray FileList;
	dir( FileList, path, filename );
	if (FileList.GetSize() < 1) return -2;

	filename = newFileName;

	CCharString newPath = filename.GetPath();
	filename.SetPath("");

	if ((filename == CCharString(".")) || (filename == CCharString("..")))
	{
		newPath = filename + dirDelimStr;
		filename = CCharString("");
	}

	CFileName inpFileName = filename;

	for(int i = 0; i < FileList.GetSize(); ++i)
	{
		CFileName oldFile = *FileList.GetAt(i);

		if (filename == "")
		{
			filename = oldFile;
			filename.SetPath(newPath);
		}
		else filename = newFileName;

		char *zBuf = new char [512];

		if (_fullpath( zBuf, oldFile.GetChar(), 512 ))
			oldFile = CCharString((const char *) zBuf);

		if (_fullpath( zBuf, filename.GetChar(), 512 ))
			filename = CCharString((const char *) zBuf);

		delete [] zBuf;

		CCharString localFilename = filename;

		if (!localFilename.CompareNoCase(oldFileName))
		{
			ifstream inputFile(oldFileName,ios::binary);
			ofstream outputFile(filename,ios::out|ios::binary);
			if (inputFile.fail() || outputFile.fail()) return -1;
			int buflen = 1024;
			zBuf = new char[buflen];

			while (!inputFile.eof())
			{
				inputFile.read(zBuf, buflen);
				outputFile.write(zBuf, inputFile.gcount());
			}
			delete [] zBuf;
		}
	}

	return 0;
#endif
}

//==============================================================================

bool CSystemUtilities::fileExists (const CCharString& fileName )
{
	bool ret = false;

	if (fileName.GetLength() > 0)
	{
		ifstream file ( fileName.GetChar());
		if (file.good()) ret = true;
	}

	return ret;
}

//=============================================================================

int CSystemUtilities::fileStats (const CCharString& fileName,
                                 struct _stat& stats )
{
#ifdef WIN32
	int fh = _open( fileName.GetChar(), _O_RDONLY );

	if( fh == -1 ) return fh; // Open failed - file doesn't exist.

	int res = _fstat( fh, &stats );
	_close( fh );
	return res;
#else
	return 0;
#endif


}

//=============================================================================

int CSystemUtilities::fileModDate(const CCharString& fileName, time_t& modDate)
{

#ifdef WIN32
	modDate = 0;
	struct _stat fileStat;

	int res = fileStats( fileName, fileStat );

	if( res == 0 ) modDate = fileStat.st_mtime;

	return res;
#else
	return 0;
#endif
}


//=============================================================================

int CSystemUtilities::mkdir( const CCharString& dirPath)
{
#ifndef WIN32
  return systemCommand( "mkdir -p "+ dirPath );
#else
  return _mkdir( dirPath.GetChar() );
#endif
}

//=============================================================================

int CSystemUtilities::rmDir( const CCharString& dirPath )
{
#ifndef WIN32
  return rmdir( dirPath );
#else
  return _rmdir( dirPath );
#endif
}

//=============================================================================

bool CSystemUtilities::dirExists( const CCharString& dirPath )
{
	CCharString cDir = getCwd();
	if (!dirPath.GetLength()) return 1;

	if ( !chDir( dirPath ) )
	{ // No error occured --> Directory exists.
		chDir( cDir );
		return 1;
	}

	return 0;
}

//=============================================================================

int CSystemUtilities::createDirectoryTree( const CCharString& directory )
{
	CFileName remainDir(directory);

	bool hasDrive = (remainDir.GetDrive() >= 0);

	CCharString currdir;
	int err = 0;

	int pos = remainDir.Find(dirDelimStr);

	if ((pos >= 0) && hasDrive)
	{
		currdir += remainDir.Left(pos) + dirDelimStr;
		remainDir = remainDir.Right(remainDir.GetLength() - (pos + 1));
		pos = remainDir.Find( dirDelimStr );
	}

	while ((!err) && (pos >= 0))
	{
		currdir += remainDir.Left(pos) + dirDelimStr;
		remainDir = remainDir.Right(remainDir.GetLength() - (pos + 1));

		err = mkdir( currdir );
		if ( ( errno == EEXIST ) || ( errno == EACCES ) ) err = 0;

		pos = remainDir.Find( dirDelimStr );
  }

  return err;
}

//===============================================================================

bool CSystemUtilities::validDir( const CCharString& directory )
{
	if( directory.GetLength() < 3 ) return false;

	bool res = true;

	char zDrive = directory[0 ];

	if (!(( zDrive >= 'A' && zDrive <= 'Z' || zDrive >= 'a' && zDrive <= 'z' ) &&
          (directory[1] == ':' && directory[2] == dirDelim) ))
	{
		res = false;
	}
	else
	{
		CCharString dirRight = directory.Right(directory.GetLength() - 3);
        int j = dirRight.Find(dirDelim);
		while( j > 0 && res)
		{
			CCharString dirPart = dirRight.Left(j);
			dirRight = dirRight.Right(dirRight.GetLength() - (j + 1));
			res = dirPart.GetLength() > 0;

			for( int k = 0; res && k < dirPart.GetLength(); k++ )
				res = !( dirPart[ k ] == '/' || dirPart[ k ] == '\\' ||
				         dirPart[ k ] == ':' || dirPart[ k ] == '*'  ||
						 dirPart[ k ] == '"' || dirPart[ k ] == '?'  ||
						 dirPart[ k ] == '<' || dirPart[ k ] == '>'  ||
						 dirPart[ k ] == '|' );

			j = dirRight.Find(dirDelim);
		}

		if (res )
		{
			for(int k = 0; res && k < dirRight.GetLength(); k++ )
				res = !(dirRight[ k ] == '/' || dirRight[ k ] == '\\' ||
                        dirRight[ k ] == ':' || dirRight[ k ] == '*'  ||
						dirRight[ k ] == '"' || dirRight[ k ] == '?'  ||
						dirRight[ k ] == '<' || dirRight[ k ] == '>'  ||
						dirRight[ k ] == '|' );
		}
	}
  return res;
}

//===============================================================================

CCharString CSystemUtilities::timeString()
{
  size_t MAX_LENGTH = 10;  // used for macro under linux!!
  char *zTimeString = new char[MAX_LENGTH];
  _strtime(zTimeString);

  CCharString timeString(zTimeString);
  delete [] zTimeString;
  return timeString;
}

//=============================================================================

CCharString CSystemUtilities::dateString()
{
  size_t MAX_LENGTH = 10;  // used for macro under linux!!
  char *zdateString = new char[MAX_LENGTH];
  _strdate(zdateString);

  CCharString dateString(zdateString);
  delete[] zdateString;

  return dateString;
}

//==============================================================================
#ifdef WIN32
int CSystemUtilities::setRegVar(const CCharString &subKey,
                                const CCharString &variable,
                                const CCharString &value,
								const DWORD       &W_type, HKEY hKey)
{
#ifdef WIN32
	HKEY   hKeyLoc;
	DWORD  disp;

	if (RegCreateKeyEx(hKey, subKey.GetChar(), 0, NULL, REG_OPTION_NON_VOLATILE,
                       KEY_WRITE, NULL, &hKeyLoc, &disp) != ERROR_SUCCESS)
	{
		return -1;
	}
	else
	{
		if (RegSetValueEx(hKeyLoc, variable.GetChar(), 0, W_type,
			              (unsigned char * ) value.GetChar(), value.GetLength()+1) != ERROR_SUCCESS)
		{
			return -1;
		}

		RegCloseKey(hKeyLoc);
	}
#endif
	return 0;
}

//==============================================================================

int CSystemUtilities::queryRegVar(const CCharString& subKey,
								  const CCharString& variable,
								  CCharString& valueS,
								  unsigned long &valueU,
								  DWORD &W_type,
								  HKEY hKey)
{


	int retVal = 0;
	HKEY hKeyLoc;

	if (RegOpenKeyEx(hKey, subKey.GetChar(), 0, KEY_READ, &hKeyLoc) != ERROR_SUCCESS)
	{
		retVal = -1;
	}
	else
	{
		DWORD  size = 1024;

		if (RegQueryValueEx(hKeyLoc, variable.GetChar(), NULL, &W_type, NULL, &size) != ERROR_SUCCESS)
		{
			retVal = -1;
		}
		else
		{
			unsigned char *zData = new unsigned char[size];
			unsigned long *zWordData = (unsigned long *) zData;

			if (RegQueryValueEx(hKeyLoc, variable.GetChar(), NULL, &W_type, zData, &size ) != ERROR_SUCCESS)
			{
				retVal = -1;
			}
			else
			{
				if ((W_type == REG_DWORD) ||
					(W_type == REG_DWORD_LITTLE_ENDIAN) ||
					(W_type == REG_DWORD_BIG_ENDIAN))
					valueU = *zWordData;
				else if (W_type == REG_SZ)
					valueS = CCharString((const char *) zData);
			}

			delete [] zData;
		}

		RegCloseKey(hKeyLoc);
	}

	return retVal;
}

//==============================================================================

int CSystemUtilities::deleteRegVar(const CCharString &subKey,
                                   const CCharString &variable,
                                   HKEY hKey)
{
	HKEY hKeyLoc;
	if (RegOpenKeyEx(hKey, subKey.GetChar(), 0, KEY_ALL_ACCESS,&hKeyLoc) != ERROR_SUCCESS)
	{
		return -1;
	}
	else
	{
		if (RegDeleteValue(hKeyLoc, variable.GetChar()) != ERROR_SUCCESS)
		{
			return -1;
		}
		RegCloseKey(hKeyLoc);
	}
	return 0;
}
#endif

//==============================================================================

int CSystemUtilities::fileAccess(const CCharString& fileName, int  mode)
{
#ifdef WIN32
	return _access( fileName.GetChar(), mode );
#else
	return access( fileName.GetChar(), mode );
#endif
}


//===========================================================================

CFileName CSystemUtilities::RelPath(const CFileName &filename,
									const CFileName& relTo )
{
	CFileName relToLoc = relTo;

	int fDrive = filename.GetDrive();
    int rDrive = relTo.GetDrive();

	CFileName relPath;
	if (fDrive != rDrive) relPath = filename.GetPath();
	else
	{
		CCharString path = filename.GetPath();
		if (!path.IsEmpty())
		{
			if (path[path.GetLength() -1] != dirDelim) path += dirDelimStr;
		}
        CCharString relToPath = relToLoc.GetPath();
		if (!relToPath.IsEmpty())
		{
			if (relToPath[relToPath.GetLength() -1] != dirDelim) relToPath += dirDelimStr;
		}

#ifdef _MSC_VER
		path.MakeUpper();
		relToPath.MakeUpper();
#endif

		CCharString fullPath = path;
		// delete common directories starting from the beginning.

		int pos1 = path.Find( dirDelim );
		int pos2 = relToPath.Find(dirDelim);
		while ((pos1 >= 0) && (pos1 <= relToPath.GetLength()) && (path.Left(pos1) == relToPath.Left(pos2)))
		{
			path = path.Right( path.GetLength() - (pos1 + 1));
			relToPath = relToPath.Right(relToPath.GetLength() - (pos2 + 1));
			pos1 = path.Find(dirDelim);
			pos2 = relToPath.Find(dirDelim);
		}

		// add for each dir-delimiters in C_relToPath a "..\" to C_path

		CCharString back("..");
		back += dirDelimStr;

		pos2 = relToPath.Find(dirDelim);

		while (pos2 >= 0)
		{
			path = back + path;

			relToPath = relToPath.Right(relToPath.GetLength() - (pos2 + 1));
			pos2 = relToPath.Find(dirDelim);
		}
		relPath = path;
	}
    return relPath;
}

CFileName CSystemUtilities::AbsPath(const CFileName &filename)
{
	CFileName AbsolutePath;

	size_t maxLength = 2048;
	char *pAbsolutePath = new char [maxLength];

#ifdef WIN32
	if (_fullpath( pAbsolutePath, filename.GetChar(), maxLength) != 0)
		AbsolutePath = pAbsolutePath;
#else
	if (realpath(filename.GetChar(), pAbsolutePath) == 0)
	{
		char *pPath = new char [maxLength];
		if ( getcwd(pPath,maxLength) != 0)
		{

			AbsolutePath = pPath;
			AbsolutePath += CSystemUtilities::dirDelim;
			AbsolutePath += filename;
		}

		delete [] pPath;

	}
	else
	{
		AbsolutePath = pAbsolutePath;
	}
#endif
	delete [] pAbsolutePath;

	return AbsolutePath;
};
