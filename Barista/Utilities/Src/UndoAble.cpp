#include "UndoAble.h"

CUndoable::CUndoable(void)
{
}

CUndoable::~CUndoable(void)
{
}

bool  CUndoable::isa(string& className) const
	{
		if (className == "CUndoable")
			return true;

		return false;
	};

string  CUndoable::getClassName() const {return string("CUndoable");};	