#include <float.h>

#include "StructuredFileSection.h"
#include "2DPoint.h"

//================================================================================

void C2DPoint::normalise()
{
	double len = this->getNorm();
	if (fabs(len) > FLT_EPSILON) *this /= len;
 };

//================================================================================

void C2DPoint::swap()
{
	double d = this->x;
	this->x = this->y;
	this->y = d;
};

//================================================================================

void C2DPoint::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CToken* pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("x", (double)this->x);

	pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("y", (double)this->y);

	pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("id", (int)this->id);

}

//================================================================================

void C2DPoint::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* pToken = pStructuredFileSection->getTokens()->GetAt(i);

		if(pToken->isKey("x"))
			this->x= (double) pToken->getDoubleValue();
		
		if(pToken->isKey("y"))
			this->y= (double) pToken->getDoubleValue();

		if(pToken->isKey("id"))
			this->id= (int) pToken->getIntValue();
	}

}

//================================================================================

bool C2DPoint::isModified()
{
	return this->bModified;
}

//================================================================================

void C2DPoint::reconnectPointers()
{
	CSerializable::reconnectPointers();
}

//================================================================================

void C2DPoint::resetPointers()
{
	CSerializable::resetPointers();
}

//================================================================================

bool C2DPoint::writeDXF( ostream& dxf, int *lineCodes, int decimals, 
						 double xfactor, double yfactor) const
{
  if (!dxf) return false;
  dxf.precision(decimals);
  dxf << lineCodes[ 0 ] << "\n" << x * xfactor << "\n" << lineCodes[ 1 ] 
      << "\n" << y * yfactor << "\n"
      << lineCodes[ 2 ] << "\n" << 0.0 << "\n70\n32\n";

  return dxf.good();
}

//================================================================================
