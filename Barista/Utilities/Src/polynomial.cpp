#include <math.h>
#include "Polynomial.h"
#include "newmat.h"

CPolynomial::CPolynomial(int degree) : coefficients(degree + 1),covariance(0)
{
	this->covariance = new Matrix(degree + 1, degree + 1);
	*(this->covariance) =0;
	 for (int i=0; i<= degree; i++)
		this->covariance->element(i, i) = 1.0;
}

//=============================================================================

CPolynomial::CPolynomial(const CPolynomial &poly): coefficients(poly.coefficients)
{
	this->covariance = new Matrix (*poly.covariance);

}

//=============================================================================

CPolynomial &CPolynomial::operator = (const CPolynomial & poly)
{
	if (this->coefficients.size() != poly.coefficients.size())
		this->coefficients.resize(poly.coefficients.size());

	for (unsigned int i=0; i<this->coefficients.size(); i++)
		this->coefficients[i] = poly.coefficients[i];
	
	(*this->covariance) = *poly.covariance;

	return *this;
}



CPolynomial::~CPolynomial()
{
	delete this->covariance;
};

void CPolynomial::setCovariance(const Matrix& covar)
{
	if (covar.Nrows() != this->covariance->Nrows() || covar.Ncols() != this->covariance->Ncols())
		return;

	(*this->covariance) = covar;
}

const Matrix& CPolynomial::getCovariance()const
{
	return *this->covariance;
}

//=============================================================================

void CPolynomial::operator *=(double factor)
{
	for (unsigned int i = 0; i < this->coefficients.size(); ++i)
	{
		this->coefficients[i] *= factor;
	}

	if (this->covariance) *(this->covariance) *= factor * factor;
};

//=============================================================================

double CPolynomial::functionValueAt(double argument) const
{
  double value = 0.0f;

  for (int exp = this->getDegree(); exp >= 0; --exp)
  {
	  value = value * argument + coefficients[exp];
  };

  return value;
};

//=============================================================================

double CPolynomial::firstDerivativeAt(double argument) const
{
  double value = 0.0f;
  for (int exp = this->getDegree() - 1; exp >= 0; --exp)
  {
    value = value * argument + double(exp + 1) * this->coefficients[exp + 1];;
  }

  return value;
};

//=============================================================================

bool CPolynomial::zeroCrossing(double approx, double &zero) const
{
  int iterations = 0;
  double relEpsilon = 1.0f;
  zero = approx;
  double value = 1;
  while ((relEpsilon >= 1E-7) && (iterations < 100) && (fabs(value)  > 1E-7))
  {
    iterations++;
    double firstDerivative = firstDerivativeAt(zero);
    if (firstDerivative == 0.0) return false;

	value = functionValueAt(zero);
    double correction = functionValueAt(zero) / firstDerivative;
    zero -= correction;
    relEpsilon = fabs(correction / zero);
  }

  return (fabs(value) < 1E-7);
};

CPolynomial CPolynomial::getFirstDerivative() const
{
	int newDegree = this->getDegree() - 1;
	if (newDegree < 0) 
	{

		CPolynomial poly(0);
		poly.coefficient(0) = 0.0f;
		return poly;
	}
		
	CPolynomial poly(newDegree);
	for (int i = 0; i <= newDegree; ++i)
	{
		poly.coefficient(i) = double(i + 1) * this->coefficients[i+1];
	}

	return poly;
}



void CPolynomial::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CCharString valueStr;
	CCharString elementStr;

	CSerializable::serializeStore(pStructuredFileSection);

	valueStr = "";

	// coefficients
	CToken* pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("size of coefficients", (int)this->coefficients.size());


	for (unsigned int i = 0; i < this->coefficients.size(); i++)
		{
			double element = this->coefficients[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("coefficients", valueStr.GetChar());

	valueStr.Empty();
	elementStr.Empty();	

	pNewToken = pStructuredFileSection->addToken();
	for (unsigned int i=0; i< this->coefficients.size(); i++)
	{
		for (unsigned int k=0; k< this->coefficients.size(); k++)
		{
			double element = this->covariance->element(i,k);
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}
	}

	pNewToken->setValue("covariance", valueStr.GetChar());	



}

void CPolynomial::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* pToken = pStructuredFileSection->getTokens()->GetAt(i);

		if (pToken->isKey("size of coefficients"))
			this->coefficients.resize(pToken->getIntValue());
		else if (pToken->isKey("coefficients"))
		{
			if (this->coefficients.size()> 0)
				pToken->getDoublesClass(this->coefficients);	
		}
		else if (pToken->isKey("covariance"))
		{
			int size = this->coefficients.size();
			double* tmpArray = new double [size*size];
			pToken->getDoubleValues(tmpArray,size*size);

			if(this->covariance)
				delete this->covariance;
			
			this->covariance = new Matrix(size,size);
			
			for (int i=0; i< size; i++)
			{
				for (int k=0; k< size; k++)
				{
					this->covariance->element(i,k) = tmpArray[i*size+k];
				}	 
			}

			if (tmpArray) delete [] tmpArray;
		}

	}
}

bool CPolynomial::isModified()
{
	return this->bModified;
}

void CPolynomial::reconnectPointers()
{
	CSerializable::reconnectPointers();
}

void CPolynomial::resetPointers()
{
	CSerializable::resetPointers();
}

void CPolynomial::setCovarElement(unsigned int row, unsigned int col,double value)
{
	if (row >= this->coefficients.size() || col >= this->coefficients.size())
		return;

	this->covariance->element(row,col)= value;
}