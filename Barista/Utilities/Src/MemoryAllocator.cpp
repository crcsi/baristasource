#include <memory.h>
#include <stdlib.h> 
#include <malloc.h>
#include <assert.h>

#ifndef NULL
#define NULL 0
#endif

#include "MemoryAllocator.h"

CMemoryAllocator::CMemoryAllocator(size_t objectSize, int nObjects /*= 1000*/)
{
	this->pNext = NULL;
	this->objectSize = objectSize;
	this->nObjects = nObjects;

	this->pMemBlock = (unsigned char*)::malloc(this->objectSize*this->nObjects);
	this->next = 0; 
	this->allocList = new int[nObjects];

	this->full = false;
	::memset(this->allocList, 0, nObjects*sizeof(int));
}

CMemoryAllocator::~CMemoryAllocator(void)
{
	delete this->allocList;
	delete this->pMemBlock;

	if (this->pNext != NULL)
        delete this->pNext;
}

unsigned char* CMemoryAllocator::newObject()
{
	if (!this->full)
	{
		for (int i = 0; i < this->nObjects; i++)
		{
			if (this->next == this->nObjects)
				this->next = 0;

			// did we find a free one?
			if (this->allocList[this->next] == 0)
			{
				this->allocList[this->next] = 1;

				int offset = this->next*this->objectSize;
				this->next++;

				return this->pMemBlock + offset;
			}

			this->next++;
		}
	}

	// No free spots
	this->full = true;

	if (this->pNext == NULL)
		this->pNext = new CMemoryAllocator(this->objectSize, this->nObjects);

	return this->pNext->newObject();
}

void CMemoryAllocator::deleteObject(unsigned char* pObject)
{
	// compute index of object
	int index = (pObject - this->pMemBlock)/this->objectSize;

	if ( (index < 0) || (index >= this->nObjects) )
	{
		if (this->pNext != NULL)
			this->pNext->deleteObject(pObject);
		else
			assert(false);
	}
	else
	{
		this->allocList[index] = 0;
		this->next = index;
		this->full = false;
	}
}

