// FileArchive.cpp: implementation of the CFileArchive class.
//
//////////////////////////////////////////////////////////////////////

#include <string.h>
#include <assert.h>

#include "Label.h"

#include "FileArchive.h"

int CFileArchive::READ = ios::in | ios::binary;
int CFileArchive::WRITE = ios::out | ios::binary;

//////////////////////////////////////////////////////////////////////
// CFileArchive::CFileArchive( const char* szName, int nMode) :
//		fstream(szName, nMode, nProt)
//
//
CFileArchive::CFileArchive( const char* szName, int nMode) :
		fstream(szName, (ios_base::openmode)nMode)
{
	assert(nMode == READ || nMode == WRITE);

	// keep track of how the "archive" was open
	m_nMode = nMode;
}

CFileArchive::CFileArchive()
{
}

void CFileArchive::Open(const char* szName, int nMode)
{
	fstream::open(szName, (ios_base::openmode)nMode);
}

//////////////////////////////////////////////////////////////////////
//
//
//
CFileArchive::~CFileArchive()
{

}

//////////////////////////////////////////////////////////////////////
// bool CFileArchive::IsStoring()
//
//
bool CFileArchive::IsStoring()
{
	// if m_nMode was set to ios::in, it must be for reading
	if (m_nMode & ios::in)
		return false;
	else
		return true;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(double d)
//
//
CFileArchive& CFileArchive::operator<<(double d)
{
	write((char*)&d, sizeof(double));

	return *this;
}


//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(long l)
//
//
CFileArchive& CFileArchive::operator<<(long l)
{
	write((char*)&l, sizeof(long));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(unsigned long l)
//
//
CFileArchive& CFileArchive::operator<<(unsigned long l)
{
	write((char*)&l, sizeof(unsigned long));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(short s)
//
//
CFileArchive& CFileArchive::operator<<(short s)
{
	write((char*)&s, sizeof(short));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(unsigned short s)
// 
//
CFileArchive& CFileArchive::operator<<(unsigned short s)
{
	write((char*)&s, sizeof(unsigned short));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(const char* pChar)
//
//
CFileArchive& CFileArchive::operator<<(const char* pChar)
{
	long l = strlen(pChar);

	*this << l;

	write(pChar, l);

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(char c)
//
//
CFileArchive& CFileArchive::operator<<(char c)
{
	write(&c, sizeof(char));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(unsigned char c)
//
//
CFileArchive& CFileArchive::operator<<(unsigned char c)
{
	write((char *)&c, sizeof(unsigned char));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(double& d)
//
//
CFileArchive& CFileArchive::operator>>(double& d)
{
	read((char*) &d, sizeof(double));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(long& l)
//
//
CFileArchive& CFileArchive::operator>>(long& l)
{
	read((char*) &l, sizeof(long));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(unsigned long& l)
//
//
CFileArchive& CFileArchive::operator>>(unsigned long& l)
{
	read((char*) &l, sizeof(unsigned long));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(short& s)
//
//
CFileArchive& CFileArchive::operator>>(short& s)
{
	read((char*) &s, sizeof(short));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(unsigned short& s)
// 
//
CFileArchive& CFileArchive::operator>>(unsigned short& s)
{
	read((char*) &s, sizeof(unsigned short));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(char*& pChar)
//
//
CFileArchive& CFileArchive::operator>>(char*& pChar)
{
	// Sanity check
	assert (pChar == (char*)0);

	long l;
	
	*this >> l;

	pChar = new char[l + 1];

	read(pChar, l);

	pChar[l] = '\0';

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(char& c)
//
//
CFileArchive& CFileArchive::operator>>(char& c)
{
	read(&c, sizeof(char));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(unsigned char& c)
//
//
CFileArchive& CFileArchive::operator>>(unsigned char& c)
{
	read((char *)&c, sizeof(unsigned char));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(CCharString& String)
//
//
CFileArchive& CFileArchive::operator<<(CCharString& String)
{
	const char* pChar = String.GetChar();

	char nullChar = '\0';

	if (pChar == (char*)0)
		pChar = &nullChar;

	*this << pChar;
	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(CCharString& String)
//
//
CFileArchive& CFileArchive::operator>>(CCharString& String)
{
	char* pChar = (char*)0;

	*this >> pChar;

	String = pChar;

	delete [] pChar;
	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& operator<<(bool b)
//
//
CFileArchive& CFileArchive::operator<<(bool b)
{
	write((char*)&b, sizeof(bool));

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& operator>>(bool& b)
//
//
CFileArchive& CFileArchive::operator>>(bool& b)
{
	read((char*)&b, sizeof(bool));

	return *this;
}


//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator<<(CLabel& Label)
//
//
CFileArchive& CFileArchive::operator<<(CLabel& Label)
{
	const char* pChar = Label.GetChar();
	*this << pChar;
	return *this;
}

//////////////////////////////////////////////////////////////////////
// CFileArchive& CFileArchive::operator>>(CLabel& Label)
//
//
CFileArchive& CFileArchive::operator>>(CLabel& Label)
{
	char* pChar = (char*)0;

	*this >> pChar;

	Label = pChar;

	delete [] pChar;

	return *this;
}



