#include "Stack.h"

CStack::CStack()
{
}

CStack::~CStack()
{
    this->clearStack();
}

void CStack::clearStack()
{
    int size = this->GetSize();

    for (int i = 0; i < size; i++)
    {
        CStructuredFileSection* pSection = this->GetAt(i);
        delete pSection;
    }

    this->RemoveAll();
}

void CStack::push(CStructuredFileSection* pAddSection)
{
    this->InsertAt(0, pAddSection);

    int size = this->GetSize();

    if(size > MAX_STACK)
    {
        CStructuredFileSection* pSection = this->GetAt(MAX_STACK);
        delete pSection;
        this->RemoveAt(MAX_STACK);
    }
}

CStructuredFileSection* CStack::pop()
{
    int size = this->GetSize();

    if (size == 0)
        return NULL;
    
    CStructuredFileSection* pSection = this->GetAt(0);

    this->RemoveAt(0);

    return pSection;
}
