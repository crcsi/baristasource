#include "ITUTM.h"
#include <stdio.h>
#include <math.h>

static double r_major;		/* major axis 				*/
static double r_minor;		/* minor axis 				*/
static double scale_factor;	/* scale factor				*/
static double lon_center;	/* Center longitude (projection center) */
static double lat_origin;	/* center latitude			*/
static double e0,e1,e2,e3;	/* eccentricity constants		*/
static double e,es,esp;		/* eccentricity constants		*/
static double ml0;			/* small value m			*/
static double false_northing;	/* y offset in meters			*/
static double false_easting;	/* x offset in meters			*/
static int    ind;			/* spherical flag			*/

CITUTM::CITUTM(void)
{
}

CITUTM::~CITUTM(void)
{
}

double CITUTM::asinz (double con)
{
	if (fabs(con) > 1.0)
   	{
		if (con > 1.0)
			con = 1.0;
		else
			con = -1.0;
	}
	return(asin(con));
}

int CITUTM::calc_utm_zone(double lon)
{
	return((long)(((lon + 180.0) / 6.0) + 1.0));
}

void CITUTM::sincos(double val, double *sin_val, double *cos_val)
{
	*sin_val = sin(val);
	*cos_val = cos(val);
}


int CITUTM::sign(double x)
{
	return((x < 0.0) ? -1 : 1);
}


/* Functions to compute the constants e0, e1, e2, and e3 which are used
   in a series for calculating the distance along a meridian.  The
   input x represents the eccentricity squared.
*/
double 	CITUTM::e0fn(double x)
{
	return(1.0-0.25*x*(1.0+x/16.0*(3.0+1.25*x)));
}

double 	CITUTM::e1fn(double x)
{
	return(0.375*x*(1.0+0.25*x*(1.0+0.46875*x)));
}

double 	CITUTM::e2fn(double x)
{
	return(0.05859375*x*x*(1.0+0.75*x));
}

double 	CITUTM::e3fn(double x)
{
	return(x*x*x*(35.0/3072.0));
}

/* Function to compute the constant e4 from the input of the eccentricity
   of the spheroid, x.  This constant is used in the Polar Stereographic
   projection.
*/
double 	CITUTM::e4fn(double x)
{
	double con;
	double com;

	con = 1.0 + x;
	com = 1.0 - x;

 	return (sqrt((pow(con,con))*(pow(com,com))));
}

/* Function computes the value of M which is the distance along a meridian
   from the Equator to latitude phi.
*/
double 	CITUTM::mlfn(double e0,double e1,double e2,double e3,double phi)
{
	return(e0*phi-e1*sin(2.0*phi)+e2*sin(4.0*phi)-e3*sin(6.0*phi));
}


/* Function to adjust a longitude angle to range from -180 to 180 radians
   added if statments
*/
double 	CITUTM::adjust_lon(double x)
{
	int count = 0;

	for(;;) {
		if (fabs(x)<=M_PI) break;

		else if (((long) fabs(x / M_PI)) < 2)
	   		x -= (sign(x) *TWO_PI);

		else if (((long) fabs(x / TWO_PI)) < MAXLONG)
	   		x -= (((long)(x / TWO_PI))*TWO_PI);

		else if (((long) fabs(x / (MAXLONG * TWO_PI))) < MAXLONG)
	   		x -= (((long)(x / (MAXLONG * TWO_PI))) * (TWO_PI *
	   		MAXLONG));

		else if (((long) fabs(x / (DBLLONG * TWO_PI))) < MAXLONG)
	   		x -= (((long)(x / (DBLLONG * TWO_PI))) * (TWO_PI *
	   		DBLLONG));

		else
	   		x -= (sign(x) *TWO_PI);
		count++;
		if (count > MAX_VAL) break;
	}

	return(x);
}


/* Initialize the Universal Transverse Mercator (UTM) projection */
int CITUTM::UTM_Init(
		double r_maj,			/* major axis	*/
		double r_min,			/* minor axis	*/
		int zone)			    /* zone number	*/
{
	double temp;		/* temporary variable */

	if ((fabs(float(zone)) < 1) || (fabs(float(zone)) > 60)) {
		printf("Illegal zone number\n");
		return(0);
	}

	r_major = r_maj;
	r_minor = r_min;
	scale_factor = 0.9996;
	lat_origin = 0.0;
	lon_center = ((6 * fabs(float(zone))) - 183) * D2R;
	false_easting = 500000.0;
	false_northing = (zone < 0) ? 10000000.0 : 0.0;

	temp = r_minor / r_major;
	es = 1.0 - SQUARE(temp);
	e = sqrt(es);
	e0 = e0fn(es);
	e1 = e1fn(es);
	e2 = e2fn(es);
	e3 = e3fn(es);
	ml0 = r_major * mlfn(e0, e1, e2, e3, lat_origin);
	esp = es / (1.0 - es);

	ind = (es < .00001) ? 1 : 0;

	return(1);
}

//this is only valid for Bhutan
int CITUTM::TM_Init(
		double r_maj,			/* major axis	*/
		double r_min,			/* minor axis	*/
		int zone)			    /* zone number	*/
{
	double temp;		/* temporary variable */

	if ((fabs(float(float(zone))) < 1) || (fabs(float(zone)) > 60)) {
		printf("Illegal zone number\n");
		return(0);
	}

	r_major = r_maj;
	r_minor = r_min;
	scale_factor = 1.0;
	lat_origin = 0.0;
	lon_center = 90.00 * D2R;
	false_easting = 150000.0;
	false_northing = (zone < 0) ? 10000000.0 : 0.0;

	temp = r_minor / r_major;
	es = 1.0 - SQUARE(temp);
	e = sqrt(es);
	e0 = e0fn(es);
	e1 = e1fn(es);
	e2 = e2fn(es);
	e3 = e3fn(es);
	ml0 = r_major * mlfn(e0, e1, e2, e3, lat_origin);
	esp = es / (1.0 - es);

	ind = (es < .00001) ? 1 : 0;

	return(1);
}

/* Universal Transverse Mercator forward equations--mapping lat,long to x,y
	Note:  The algorithm for UTM is exactly the same as TM and therefore
	  if a change is implemented, also make the change to TMFOR.c
*/
int CITUTM::UTM_Forward(
		double lon,	/* (I) Longitude (degrees)		*/
		double lat,	/* (I) Latitude  (degrees)		*/
		double *x,	/* (O) X projection coordinate 	(easting) */
		double *y)	/* (O) Y projection coordinate 	(northing */
{
	double delta_lon;			/* Delta longitude (Given longitude - center */
	double sin_phi, cos_phi;	/* sin and cos value		*/
	double al, als;				/* temporary values			*/
	double b;					/* temporary values			*/
	double c, t, tq;			/* temporary values			*/
	double con, n, ml;			/* cone constant, small m	*/

	lon = lon*D2R;
	lat = lat*D2R;

	/* Forward equations */
	delta_lon = adjust_lon(lon - lon_center);
	sincos(lat, &sin_phi, &cos_phi);

	/* This part was in the fortran code and is for the spherical form
	----------------------------------------------------------------*/
	if (ind != 0) {
		b = cos_phi * sin(delta_lon);
		if ((fabs(fabs(b) - 1.0)) < EPSLN) {
	   		printf("Point projects into infinity in utm-for\n");
	   		return(93);
	   	}
		else {
	   		*x = .5 * r_major * scale_factor * log((1.0 + b)/(1.0 - b));

	   		con = acos(cos_phi * cos(delta_lon)/sqrt(1.0 - b*b));
	   		if (lat < 0) con = -con;

	   		*y = r_major * scale_factor * (con - lat_origin);

	   		return(1);
	   	}
	}

	al  = cos_phi * delta_lon;
	als = SQUARE(al);
	c   = esp * SQUARE(cos_phi);
	tq  = tan(lat);
	t   = SQUARE(tq);
	con = 1.0 - es * SQUARE(sin_phi);
	n   = r_major / sqrt(con);
	ml  = r_major * mlfn(e0, e1, e2, e3, lat);

	*x  = scale_factor * n * al * (1.0 + als / 6.0 * (1.0 - t + c + als / 20.0 *
	      (5.0 - 18.0 * t + SQUARE(t) + 72.0 * c - 58.0 * esp))) + false_easting;

	*y  = scale_factor * (ml - ml0 + n * tq * (als * (0.5 + als / 24.0 *
	      (5.0 - t + 9.0 * c + 4.0 * SQUARE(c) + als / 30.0 * (61.0 - 58.0 * t
	      + SQUARE(t) + 600.0 * c - 330.0 * esp))))) + false_northing;

	return(1);
}

int CITUTM::UTM_Inverse(
	double x,		/* (Input) X projection coordinate */
	double y,		/* (Input) Y projection coordinate */
	double *lon,	/* (Output) Longitude (in Degrees) */
	double *lat)	/* (Output) Latitude  (in Degrees) */
{
	double con,phi;				/* temporary angles	   */
	double delta_phi;			/* difference between longitudes*/
	long   i;					/* counter variable		*/
	double sin_phi, cos_phi, tan_phi;	/* sin cos and tangent values */
	double c, cs, t, ts, n, r, d, ds;	/* temporary variables		*/
	double f, h, g, temp;		/* temporary variables		*/
	long   max_iter = 6;		/* maximun number of iterations	*/

	/* fortran code for spherical form
	--------------------------------*/
	if (ind != 0) {
		f = exp(x/(r_major * scale_factor));
		g = .5 * (f - 1/f);
		temp = lat_origin + y/(r_major * scale_factor);
		h = cos(temp);

		con = sqrt((1.0 - h * h)/(1.0 + g * g));

		*lat = asinz(con);

		if (temp < 0) *lat = -*lat;

		if ((g == 0) && (h == 0))
	  		*lon = lon_center;
		else
	  		*lon = adjust_lon(atan2(g,h) + lon_center);

		*lat = *lat*R2D;
		*lon = *lon*R2D;

	  	return(1);
	}

	/* Inverse equations
	  -----------------*/
	x = x - false_easting;
	y = y - false_northing;

	con = (ml0 + y / scale_factor) / r_major;
	phi = con;

	for (i=0;;i++)
	{
	   	delta_phi=((con + e1 * sin(2.0*phi)
	   		- e2 * sin(4.0*phi) + e3 * sin(6.0*phi)) / e0) - phi;

		phi += delta_phi;

		if (fabs(delta_phi) <= EPSLN) break;

		if (i >= max_iter)
		{
			printf("Latitude failed to converge in UTM-INVERSE");
			return(95);
		}
	}
	if (fabs(phi) < HALF_PI)
	{
		sincos(phi, &sin_phi, &cos_phi);
		tan_phi = tan(phi);
		c    = esp * SQUARE(cos_phi);
		cs   = SQUARE(c);
		t    = SQUARE(tan_phi);
		ts   = SQUARE(t);
		con  = 1.0 - es * SQUARE(sin_phi);
		n    = r_major / sqrt(con);
		r    = n * (1.0 - es) / con;
		d    = x / (n * scale_factor);
		ds   = SQUARE(d);

		*lat = phi - (n * tan_phi * ds / r) * (0.5 - ds / 24.0 * (5.0 + 3.0 * t +
			10.0 * c - 4.0 * cs - 9.0 * esp - ds / 30.0 * (61.0 + 90.0 * t +
			298.0 * c + 45.0 * ts - 252.0 * esp - 3.0 * cs)));

		*lon = adjust_lon(lon_center + (d * (1.0 - ds / 6.0 * (1.0 + 2.0 * t +
			c - ds / 20.0 * (5.0 - 2.0 * c + 28.0 * t - 3.0 * cs + 8.0 * esp +
			24.0 * ts))) / cos_phi));
	}
	else
	{
		*lat = HALF_PI * sign(y);
		*lon = lon_center;
	}

	*lat = *lat*R2D;
	*lon = *lon*R2D;

	return(1);
}

int CITUTM::Ellipsoid_Init(int ellipsoid_index, DATUM *datum)
{
	double	flattening;

	switch (ellipsoid_index) {
		case AIRY:
			datum->majorAxis = 6377563.396;   
			datum->minorAxis = 6356256.910;
			flattening       = 0.0033408506;
			break;

		case AIRY_MODIFIED:
			datum->majorAxis = 6377340.189;   
			datum->minorAxis = 6356034.448;
			flattening       = 0.0033408506;
			break;

		case AUSTRALIAN_NATIONAL:
			datum->majorAxis = 6378160.000;   
			datum->minorAxis = 6356774.719;
			flattening       = 0.0033528919;
			break;

		case BESSEL_1841:
			datum->majorAxis = 6377397.155;   
			datum->minorAxis = 6356078.963;
			flattening       = 0.0033427732;
			break;

		case BESSEL_1841_(NAMIBIA):
			datum->majorAxis = 6377483.865;   
			datum->minorAxis = 6356165.383;
			flattening       = 0.0033427732;
			break;

		case CLARKE_1866:
			datum->majorAxis = 6378206.400;   
			datum->minorAxis = 6356583.800;
			flattening       = 0.0033900753;
			break;

		case CLARKE_1880:
			datum->majorAxis = 6378249.145;   
			datum->minorAxis = 6356514.870;
			flattening       = 0.0034075614;
			break;

		case EVEREST:
			datum->majorAxis = 6377276.345;   
			datum->minorAxis = 6356075.413;
			flattening       = 0.0033244493;
			break;

		case EVEREST_MODIFIED:
			datum->majorAxis = 6377304.063;   
			datum->minorAxis = 6356103.039;
			flattening       = 0.0033244493;
			break;

		case GRS_80:
			datum->majorAxis = 6378137.000;   
			datum->minorAxis = 6356752.314;
			flattening       = 0.0033528107;
			break;

		case HELMERT_1906:
			datum->majorAxis = 6378200.000;   
			datum->minorAxis = 6356818.169;
			flattening       = 0.0033523299;
			break;

		case HOUGH:
			datum->majorAxis = 6378270.000;   
			datum->minorAxis = 6356794.343;
			flattening       = 0.0033670034;
			break;

		case INTERNATIONAL_09:
			datum->majorAxis = 6378388.000;   
			datum->minorAxis = 6356911.946;
			flattening       = 0.0033670034;
			break;

		case INTERNATIONAL_67:
			datum->majorAxis = 6378157.500;   
			datum->minorAxis = 6356772.200;
			flattening       = 0.0033528962;
			break;

		case INTERNATIONAL_75:
			datum->majorAxis = 6378140.000;   
			datum->minorAxis = 6356755.288;
			flattening       = 0.0033528132;
			break;

		case KRASSOVSKY:
			datum->majorAxis = 6378245.000;   
			datum->minorAxis = 6356863.019;
			flattening       = 0.0033523299;
			break;

		case MERCURY_60:
			datum->majorAxis = 6378166.000;   
			datum->minorAxis = 6356784.284;
			flattening       = 0.0033523299;
			break;

		case MODIFIED_MERCURY_68:
			datum->majorAxis = 6378150.000;   
			datum->minorAxis = 6356768.337;
			flattening       = 0.0033523299;
			break;

		case MODIFIED_FISCHER_60:
			datum->majorAxis = 6378155.000;   
			datum->minorAxis = 6356773.321;
			flattening       = 0.0033523299;
			break;

		case WGS_66:
			datum->majorAxis = 6378145.000;   
			datum->minorAxis = 6356759.769;
			flattening       = 0.0033528919;
			break;

		case WGS_72:
			datum->majorAxis = 6378135.000;   
			datum->minorAxis = 6356750.520;
			flattening       = 0.0033527795;
			break;

		case SPHERE_6370997:
			datum->majorAxis = 6370997.000;   
			datum->minorAxis = 6370997.000;
			flattening       = 0.0000000000;
			break;

		case WGS_84:
			datum->majorAxis = 6378137.000;   
			datum->minorAxis = 6356752.314;
			flattening       = 0.0033528107;
			break;

		case GRS_83:
			datum->majorAxis = 6378136.000;   
			datum->minorAxis = 6356751.317;
			flattening       = 0.0033528107;
			break;
	}

	datum->e2 = flattening * (2.0 - flattening);
	datum->e1 = sqrt(2.0/flattening-1.0)/(1.0/flattening-1.0);
	datum->e  = sqrt(datum->e2);

	datum->xOff = datum->yOff = datum->zOff = 0.0;

	return 1;
}
 
int	CITUTM::Geographic2Geocentric(
	int		ellipSoid_index,           /* The inex for type of EllipSoid */
	double	Lon,double Lat,double Alt, /* The latitude, longitude (degrees) and altitude */
	double	*GX,double *GY,double *GZ) /* The geocentrical coords */
{
	double	e2,N;
	DATUM	datum;
	
	Lat = Lat*D2R;
	Lon = Lon*D2R;
	
	Ellipsoid_Init(ellipSoid_index,&datum);
	
	e2 = datum.e2;
	N = datum.majorAxis/sqrt(1.0-e2*sin(Lat)*sin(Lat));
	
	*GX = (N+Alt)*cos(Lat)*cos(Lon);
	*GY = (N+Alt)*cos(Lat)*sin(Lon);
	*GZ = (N*(1.0-e2)+Alt)*sin(Lat);

	return(1);
}


int	CITUTM::Geocentric2Geographic(
		int    ellipSoid_index,              /* The inex for type of EllipSoid */
		double GX,double GY,double GZ,       /* The geocentrical coords */
		double *Lon,double *Lat,double *Alt) /* The latitude, longitude (degrees) and altitude */
{
	double	e2,N;
	double	Lamda,Phai,Hight,Lamda0,Phai0,Hight0;
	DATUM	datum;
	
	Ellipsoid_Init(ellipSoid_index,&datum);
	
	e2 = datum.e2;
	N = datum.majorAxis/sqrt(1.0-e2*sin(0.0)*sin(0.0));
	
	Lamda = atan2(GY,GX);
	Phai  = atan(GZ/sqrt(GX*GX+GY*GY)+e2*N*sin(0.0)/sqrt(GX*GX+GY*GY));
	Hight = sqrt(GX*GX+GY*GY+(GZ+e2*N*sin(0.0))*(GZ+e2*N*sin(0.0)))-N;
	
	while (1) {
		N = datum.majorAxis/sqrt(1.0-e2*sin(Phai)*sin(Phai));
	
		Lamda0 = atan2(GY,GX);
		Phai0  = atan(GZ/sqrt(GX*GX+GY*GY)+e2*N*sin(Phai)/sqrt(GX*GX+GY*GY));
		Hight0 = sqrt(GX*GX+GY*GY+(GZ+e2*N*sin(Phai))*(GZ+e2*N*sin(Phai)))-N;
		Hight0 = GX/(cos(Lamda0)*cos(Phai0))-N;
		
		if (fabs(Lamda0-Lamda) < 0.000000000000001 &&
		    fabs(Phai0-Phai) < 0.000000000000001 &&
		    fabs(Hight0-Hight) < 0.000000000000001) {
		    Lamda = Lamda0;
			Phai  = Phai0;
			Hight = Hight0;
			break;
		}
		
		Lamda = Lamda0;
		Phai  = Phai0;
		Hight = Hight0;
	}
	
	*Lon = Lamda*R2D;
	*Lat = Phai*R2D;
	*Alt = Hight;

	return(1);
}

int CITUTM::Geocentric2TangentPlane(
		int    ellipSoid_index,                 /* The inex for type of EllipSoid */
		double orig_lon, double orig_lat,       /* The geographic coords (in degrees) of the tangent plane's original point */
		double GX,double GY,double GZ,          /* The geocentrical coords */
		double *r_GX,double *r_GY,double *r_GZ) /* The tangent plane coords */
{
	double	gx0,gy0,gz0;

	Geographic2Geocentric(ellipSoid_index,orig_lon,orig_lat,0.0,&gx0,&gy0,&gz0);

	orig_lon *= D2R;
	orig_lat *= D2R;

	*r_GX = -sin(orig_lon)              *(GX-gx0) + cos(orig_lon)              *(GY-gy0);
	*r_GY = -sin(orig_lat)*cos(orig_lon)*(GX-gx0) - sin(orig_lat)*sin(orig_lon)*(GY-gy0) + cos(orig_lat)*(GZ-gz0);
	*r_GZ =  cos(orig_lat)*cos(orig_lon)*(GX-gx0) + cos(orig_lat)*sin(orig_lon)*(GY-gy0) + sin(orig_lat)*(GZ-gz0);

	return 1;
}

int CITUTM::TangentPlane2Geocentric(
		int    ellipSoid_index,                 /* The inex for type of EllipSoid */
		double orig_lon, double orig_lat,       /* The geographic coords (in degrees) of the tangent plane's original point */
		double GX,double GY,double GZ,          /* The tangent plane coords */
		double *r_GX,double *r_GY,double *r_GZ) /* The geocentrical coords */
{
	double	gx0,gy0,gz0;

	Geographic2Geocentric(ellipSoid_index,orig_lon,orig_lat,0.0,&gx0,&gy0,&gz0);

	orig_lon *= D2R;
	orig_lat *= D2R;

	*r_GX = gx0 - sin(orig_lon)*GX - sin(orig_lat)*cos(orig_lon)*GY + cos(orig_lat)*cos(orig_lon)*GZ;
	*r_GY = gy0 + cos(orig_lon)*GX - sin(orig_lat)*sin(orig_lon)*GY + cos(orig_lat)*sin(orig_lon)*GZ;
	*r_GZ = gx0                    + cos(orig_lat)              *GY + sin(orig_lat)              *GZ;

	return 1;
}

int CITUTM::Geographic2TangentPlane(
		int    ellipSoid_index,                 /* The inex for type of EllipSoid */
		double orig_lon, 
		double orig_lat, 
		double orig_hig,                        /* The geographic coords (in degrees) of the tangent plane's original point */
		double lon,double lat,double alt,       /* The geographic coords (in degrees & meters) */
		double *r_GX,double *r_GY,double *r_GZ) /* The tangent plane coords */
{
	double	a,b,e,e2,N,N0,D,l;
	DATUM	datum;
	
	Ellipsoid_Init(ellipSoid_index,&datum);

	orig_lon *= D2R;
	orig_lat *= D2R;
	lon      *= D2R;
	lat      *= D2R;

	a  = datum.majorAxis;
	b  = datum.minorAxis;
	e2 = datum.e2;
	e  = datum.e;
	N  = a/sqrt(1.0-e2*sin(lat)*sin(lat));
	N0 = a/sqrt(1.0-e2*sin(orig_lat)*sin(orig_lat));
	l  = lon-orig_lon;
	D  = e2*(N0*sin(orig_lat)-N*sin(lat));

	*r_GX = (N+alt)*cos(lat)*sin(l);
	*r_GY = (N+alt)*(sin(lat)*cos(orig_lat)-sin(orig_lat)*cos(lat)*cos(l))+D*cos(orig_lat);
	*r_GZ = (N+alt)*(sin(lat)*sin(orig_lat)+cos(orig_lat)*cos(lat)*cos(l))-(N0+orig_hig)+D*sin(orig_lat);

	return 1;
}

int CITUTM::TangentPlane2Geographic(
		int    ellipSoid_index,           /* The inex for type of EllipSoid */
		double orig_lon, 
		double orig_lat, 
		double orig_alt,                  /* The geographic coords (in degrees) of the tangent plane's original point */
		double GX,double GY,double GZ,    /* The tangent plane coords */
		double *r_lon,
		double *r_lat,
		double *r_alt)                    /* The geographic coords (in degrees & meters) */
{
	double	a,b,e,e2,N,N0,A,B,C,D;
	double	lat0,lon0,alt0,lat,lon,alt;
	DATUM	datum;
	
	Ellipsoid_Init(ellipSoid_index,&datum);

	orig_lon *= D2R;
	orig_lat *= D2R;

	a  = datum.majorAxis;
	b  = datum.minorAxis;
	e2 = datum.e2;
	e  = datum.e;
	N0 = a/sqrt(1.0-e2*sin(orig_lat)*sin(orig_lat));

	lat0 = orig_lat;
	lon0 = orig_lon;
	alt0 = orig_alt;

	while (1) {
		N = a/sqrt(1.0-e2*sin(lat0)*sin(lat0));
		A = (1.0+orig_alt/N0)*cos(orig_lat)-sin(orig_lat)*GY/N0+cos(orig_lat)*GZ/N0;
		B = (1.0+orig_alt/N0)*sin(orig_lat)+cos(orig_lat)*GY/N0+sin(orig_lat)*GZ/N0;
		C = sqrt(A*A+(GX/N0)*(GX/N0));
		D = e2*(N0*sin(orig_lat)-N*sin(lat0));

		lon = atan2(GX,N0*A)+orig_lon;
		lat = atan(B/C-(D*lat0)/(N0*C));
		alt = GX/(cos(lat0)*sin(lon-orig_lon))-N;
		
		if (fabs(lon0-lon) < 0.000000000000001 &&
		    fabs(lat0-lat) < 0.000000000000001 &&
		    fabs(alt0-alt) < 0.000000000000001) {
			break;
		}
		
		lon0 = lon;
		lat0 = lat;
		alt0 = alt;
	}

	*r_lon = lon*R2D;
	*r_lat = lat*R2D;
	*r_alt = alt;

	return 1;
}