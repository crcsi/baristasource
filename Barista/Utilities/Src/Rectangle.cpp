#include "2DPoint.h"
#include "Rectangle.h"

CRectangle::CRectangle(void)
{
	this->init = false;
}

CRectangle::~CRectangle(void)
{
}


CRectangle::CRectangle(double x_left,double y_top,double x_right,double y_bottom)
{
	this->x = x_left;
	this->y = y_top;
	this->width = x_right - x_left;
	this->height = y_bottom - y_top;
	this->init =true;
}
CRectangle::CRectangle(const C2DPoint& left_top,double height, double width)
{
	this->x = left_top.x;
	this->y = left_top.y;
	this->width = width;
	this->height =height;
	this->init =true;
}

CRectangle::CRectangle(const C2DPoint& left_top, const C2DPoint& right_bottom)
{
	this->x = left_top.x;
	this->y = left_top.y;
	this->width = right_bottom.x - left_top.x;
	this->height = right_bottom.y -left_top.y;
	this->init =true;
}

bool CRectangle::isInitialized()
{	return this->init;
}

void CRectangle::Clear()
{
	if (this->init)
		this->init = false;
}


void CRectangle::fill(double x_left,double y_top,double x_right,double y_bottom)
{
	this->x = x_left;
	this->y = y_top;
	this->width = x_right - x_left;
	this->height = y_bottom - y_top;
	this->init =true;
}

void CRectangle::fill(const C2DPoint& left_top,double height, double width)
{
	this->x = left_top.x;
	this->y = left_top.y;
	this->width = width;
	this->height =height;
	this->init =true;
}

void CRectangle::fill(const C2DPoint& left_top, const C2DPoint& right_bottom)
{
	this->x = left_top.x;
	this->y = left_top.y;
	this->width = right_bottom.x - left_top.x;
	this->height = right_bottom.y -left_top.y;
	this->init =true;
}


bool CRectangle::contains(const C2DPoint& p)
{
	return (p.x > this->x && 
			p.x < (this->x+this->width) &&
			p.y > this->y &&
			p.y < (this->y+this->height));
}

bool CRectangle::contains(const CRectangle& r)
{
	return (r.x > this->x && 
			(r.x+r.width) < (this->x+this->width) &&
			r.y > this->y &&
			(r.y+r.height) < (this->y+this->height));
}