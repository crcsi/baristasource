/**
 * @class CStructuredFile
 *
 * File class for object serialization
 * @see CSerializable
 * @see CSutrcturedFileSection
 * 
 * 
 * 
 * @author Harry Hanley
 * @version 1.0 
 * @date 20-jan-2004
 *
 */
 
 #include <fstream>
 #include <time.h>
 

#include "StructuredFile.h"
#include "StructuredFileSectionArray.h"
#include "Serializable.h"

#include "TextFile.h"

#define SPACE "\t"

int CStructuredFile::sectionCount = 0;
int CStructuredFile::byteCount = 0;
int CStructuredFile::currentByte = 0;

CStructuredFile::CStructuredFile(void)
{
	this->pData = NULL;
	this->index = 0;
	this->lastMark = -1;
	this->fileLength = 0;
	this->endOfFile = false;
	this->pOutputFile = NULL;
	this->currentTabLevel = 0;
	this->firstSection = true;
	this->timeLength1 = 0;

	// Introduced a char* for speed
	this->tokenStringLength = 100;
	this->tokenString = new char[this->tokenStringLength+1];
	::memset(this->tokenString, 0, this->tokenStringLength+1);
}

CStructuredFile::~CStructuredFile(void)
{
	if(this->pData != NULL)
	{
		delete [] this->pData;
		this->pData = NULL;
	}

	if(this->pOutputFile != NULL)
	{
		delete this->pOutputFile;
		this->pOutputFile = NULL;
	}

	if (this->tokenString != NULL)
	{
		delete [] this->tokenString;
		this->tokenString = NULL;
	}
}

bool CStructuredFile::readFile(const char* pFileName)
{
	ifstream fs(pFileName, ios::in | ios::binary);
	
	// calculate file size
	fs.seekg(0, ios::end);
	this->fileLength = fs.tellg();
	
	if (this->fileLength < 0)
	{
		return false;
	}

	fs.seekg(0, ios::beg);

	CStructuredFile::byteCount = this->fileLength;
	CStructuredFile::currentByte = 0;
	CStructuredFile::sectionCount = 0;
	CSerializable::restoreCount = 0;

	if(this->pData != NULL)
		delete [] this->pData;

	// set this->pData to correct size
	this->pData = new char[this->fileLength];
    char* tmp = new char[this->fileLength];

	::memset(this->pData, 0, this->fileLength);
	::memset(tmp, 0, this->fileLength);

	// read entire file
	fs.read(tmp, this->fileLength);

    int ii = 0;
    for (int i = 0; i < this->fileLength; i++)
    {
        if ( (tmp[i] < 32) || (tmp[i] >126) )
            continue;

        this->pData[ii] = tmp[i];
        ii++;
    }

    delete [] tmp;
    this->fileLength = ii;

	this->parse(&this->mainSection);

	return true;
}

void CStructuredFile::trimTokenLength()
{
	int length = ::strlen(this->tokenString);

	if (length == 0)
		return;

	// tmp copy
	char* tmp = new char[length + 1];

	// copy string
	::strcpy(tmp, this->tokenString);

	// re-init string
	::memset(this->tokenString, 0, this->tokenStringLength);

	// find the index of the first acceptable char
	int firstGood = -1;

	for(int i = 0; i < length; i++)
	{
		if(firstGood == -1)
		{
			if (tmp[i] > 32 && tmp[i] < 127)
			{
				firstGood = i;
				break;
			}
			
		}
	}

	// find the index of the last acceptable char
	int lastGood = -1;

	for(int i = length - 1; i >= 0 ; i--)
	{
		if(lastGood == -1)
		{
			if (tmp[i] > 32 && tmp[i] < 127)
			{
				lastGood = i;
				break;
			}
		}
	}

	for(int index = 0, i = firstGood; i <= lastGood ; i++)
	{
		// only allow spaces or normal characters in the new string
		if(tmp[i] >= 32 || tmp[i] < 127)
		{
			this->tokenString[index] = tmp[i];
			index++;
		}
	}

	delete [] tmp;

}

void CStructuredFile::verifyTokenStringLength(int length)
{
	if (length > this->tokenStringLength)
	{
		delete [] this->tokenString;
		this->tokenString = new char[length+1];
		this->tokenStringLength = length;
		::memset(this->tokenString, 0, this->tokenStringLength+1);
	}
}

void CStructuredFile::parse(CStructuredFileSection* pSection)
{
   clock_t start, finish;

//	CCharString tokenString;

	while (this->index < this->fileLength)
	{
		CStructuredFile::currentByte = this->index;
		if (this->pData[this->index] == '{')
		{
			this->currentTabLevel++;

			// reverse find name
			//CCharString name;
            int len = this->index - this->lastMark;
            char* name = new char[len];

			// 'reverse' find the section name
			int i, ii = 0;
			for (i = this->lastMark + 1, ii = 0; i < this->index; i++, ii++)
                name[ii] = this->pData[i];
            name[ii] = 0;
				//name += this->pData[i];

			//name.trim();

			this->lastMark = this->index;
			this->index++;

			if(this->firstSection) // parsing 1st section
			{
				CStructuredFile::sectionCount++;
				this->firstSection = false;
				pSection->setName(name);

				this->parse(pSection); // recurse
			}
			else
			{
				CStructuredFile::sectionCount++;
				CStructuredFileSection* pChild = pSection->addChild();
				pChild->setName(name);

				this->parse(pChild); // recurse
			}

            delete [] name;
		}
		else if (this->pData[this->index] == '}')
		{
			this->currentTabLevel--;
			this->lastMark = this->index;
			this->index++;
			return;
		}
		else if (this->pData[this->index] == ';')
		{
			// add token string
			//tokenString = "";

			// 'reverse' find the section name
			this->verifyTokenStringLength(this->index - this->lastMark - 1);
                        
			int i, j;
			for (j = 0, i = this->lastMark + 1; i < this->index; i++, j++)
				tokenString[j] = this->pData[i];
			tokenString[j] = 0;

			//this->trimTokenLength();

			CToken* pNewToken = pSection->addToken();
			//CCharString str(tokenString);

start = ::clock();
			pNewToken->setToken(tokenString);
finish = ::clock();
this->timeLength1 += (finish - start);

			this->lastMark = this->index;
			this->index++;
		}
		else
		{
			this->index++;
		}
	}

	assert(this->currentTabLevel == 0);
}

////////////////////////////////////////////////////////
// Write file
////////////////////////////////////////////////////////
bool CStructuredFile::writeFile(const char* pFileName)
{
	if(this->pOutputFile != NULL)
		delete this->pOutputFile;

	this->pOutputFile = new CTextFile(pFileName, CTextFile::WRITE);

	this->writeSection(&this->mainSection);

	return true;
}

void CStructuredFile::writeSection(CStructuredFileSection* pSection)
{
	CCharString tabString;
	CCharString extraTabString;
	CCharString record;
	
	// put the right # of tabs in
	for(int t = 0; t < this->currentTabLevel; t++)
		tabString += SPACE;

	record = tabString + pSection->getName();
	this->pOutputFile->WriteLine(record);

	record = tabString + "{";
	this->pOutputFile->WriteLine(record);

	extraTabString = tabString + SPACE;
	
	CTokenArray* pTokenArray = pSection->getTokens();

	for(int i = 0; i < pTokenArray->GetSize(); i++)
	{
		CToken* pToken = pTokenArray->GetAt(i);
		record = extraTabString;
		record += pToken->getKey();
		record += "=";

        if (pToken->getValue() != NULL)
    		record += pToken->getValue();

		record += ";";
		this->pOutputFile->WriteLine(record);
	}

	CStructuredFileSectionArray* pChildren = pSection->getChildren();

	this->currentTabLevel++;

	for(int j = 0; j < pChildren->GetSize(); j++)
		this->writeSection(pChildren->GetAt(j));

	record = tabString + "}";
	this->pOutputFile->WriteLine(record);

	this->currentTabLevel--;
}

CStructuredFileSection* CStructuredFile::getMainSection()
{
	return &this->mainSection;
}

