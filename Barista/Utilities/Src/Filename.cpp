//////////////////////////////////////////////
// FileName.cpp CFileName definition file
//
// Implementation
//
#include <stdio.h>
#include <ctype.h>
#include <math.h>

#ifdef WIN32
#include <direct.h>
#endif

#include "Filename.h"
#include "SystemUtilities.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CFileName::CFileName() 
{
}

CFileName::CFileName(const char* psz) : CCharString(psz)
{
	makeFilenameCompatbleWithOS();
}

CFileName::CFileName(const CCharString& String) : CCharString(String)
{
	makeFilenameCompatbleWithOS();
}

CFileName::CFileName(const CFileName& FileName) : CCharString(FileName)
{
	makeFilenameCompatbleWithOS();
}

CFileName::~CFileName()
{
}

//////////////////////////////////////////////
// CFileName::GetDrive() const
//
// Gets the drive index number A=1, B=2 etc
// -1 would mean no drive letter found
//
int	CFileName::GetDrive() const
{
	if (this->GetLength() < 2) return -1;

	CCharString tmp;

	// Get the C: part and make sure it is upper case	
	tmp = Left(2);
	tmp.MakeUpper();
	
	// May not have drive letter
	if (tmp[1] != ':')
		return -1;
	
	return (tmp[0] - 'A' + 1);
}

/////////////////////////////////////////////
// CFileName::GetPath() const
//
// Returns the full path of the file.
//
CCharString	CFileName::GetPath() const
{
	int i = ReverseFind(CSystemUtilities::dirDelim);

	// remove the filename
	// (path does not include trailing backslash)
	if( i != -1 )
		return Left(i);
	
	// backslash not found, look for drive letter path
	i = GetLength();
	if( i > 1 )
	{
		const char* pData = (const char*)*((CCharString*)this);
		if( pData[1] == ':' )
			return Left(2);
	}

	// no path could be found
	return "";
}



/////////////////////////////////////////////
// CFileName::GetFileName() const		
//
// Returns the FileName of the file.
//
CCharString	CFileName::GetFileName() const
{
	CCharString FileName;

	int i = ReverseFind(CSystemUtilities::dirDelim);

	int n = GetLength();

	// If path is in there remove it
	if (i != -1)
		FileName = Right(n - i - 1);
	else
		FileName = *this; 

	// If the Ext is there, remove it.
	i = FileName.ReverseFind('.');
	if (i != -1)
		FileName = FileName.Left(i);

	return FileName;
}

//////////////////////////////////////////////////////////////////////////////
// CFileName::GetFileExt() const
//
// Returns the file extension of the file. 
//
CCharString	CFileName::GetFileExt() const
{
	int e = ReverseFind('.');
	int s = ReverseFind(CSystemUtilities::dirDelim);
	//int s = ReverseFind('/');

	// check if slash comes after dot (directory with extension)
	if ( e < s )
		return "";

	// if we found the dot
	if( e != -1 )
	{
		int n = GetLength();
		return Right(n - e - 1);
	}

	return "";
}

//////////////////////////////////////////////////////////////////////////////
// CFileName::GetFullFileName() const
//
// Returns the full filename plus extension
//
CCharString CFileName::GetFullFileName() const
{
    int i = ReverseFind(CSystemUtilities::dirDelim);

	int n = GetLength();

	// If path is in there remove it
	if (i != -1)
		return Right(n - i - 1);
	else
		return *this; 
}
//////////////////////////////////////////////////////////////////////////////
// CFileName::SetPathCurrent()
//
// Changes the FileName's path to the CWD
//
bool CFileName::SetPathCurrent()
{
	// Make a big buffer
	char buf[FILENAME_MAX_PATH];

        #ifdef WIN32
	if( _getcwd(buf, FILENAME_MAX_PATH) == (char *)0 )
		return false;
        #else
	if( getcwd(buf, FILENAME_MAX_PATH) == (char *)0 )
		return false;
        #endif
        
	CCharString Path(buf);

	// Now set the path	
	return SetPath(Path);
}

//////////////////////////////////////////////////////////////////////////////
// bool CFileName::SetPath(CCharString Path)
//
// Changes the FileName's path to Path
//
bool CFileName::SetPath(const CCharString &Path)
{
	CCharString FileName = GetFileName();
	CCharString Ext = GetFileExt();

	*this = Path;

	int n = Path.GetLength();

	// Make sure path ends in a backslash
	if (n > 0)
	{
		if (Path[n - 1] != CSystemUtilities::dirDelim)
			*this += CSystemUtilities::dirDelimStr;
	}

	// Add a filename and ext if one exists
	if (!FileName.IsEmpty())
	{
		*this = *this + FileName + "." + Ext;
	}
	
	return true;
}

/////////////////////////////////////////////
// bool CFileName::SetFileName(CCharString FileName)
//
// Changes the FileName's path to Path
//
bool CFileName::SetFileName(const CCharString &FileName)
{
	CCharString Path = GetPath();
	CCharString Ext = GetFileExt();

	// Insert the new filename with the old Path and Ext
	*this = Path + CSystemUtilities::dirDelimStr + FileName + "." + Ext;

	return true;
}

/////////////////////////////////////////////
//Changes the FileName's ext to Ext
bool CFileName::SetFileExt(const CCharString &Ext)
{
	CCharString Path = GetPath();
	CCharString FileName = GetFileName();

	// Insert the new filename with the old Path and Ext
	*this = Path + CSystemUtilities::dirDelimStr + FileName + "." + Ext;

	return true;
}

//////////////////////////////////////////////////////////////////////////////
// CFileName::SetFullFileName(CCharString FileName)
//
// Changes the full filename to FileName
//
bool CFileName::SetFullFileName(const CCharString &FileName)
{
	CCharString Path = GetPath();

	// Insert the new filename with the old Path and Ext
	*this = Path + CSystemUtilities::dirDelimStr + FileName;

	return true;
}

/////////////////////////////////////////////
// CFileName::IncrementFileName()
//
//
bool CFileName::IncrementFileName()
{              
	// This will be the new numeric field
	long number;

	// Get the Filename with no path or ext
	CCharString FileName = GetFileName();

	int n = FileName.GetLength();
	
	// Sanity Check
	if (n == 0)
		return false;

	// Find the numeric portion if any
        int i;
	for (i = n - 1; i >= 0; i--)
	{
		if (isdigit(FileName[i]) != 0)
			continue;
		else
			break;				
	}
	
	// pos is the position
	int pos = i + 1;
	
	// Name is the Filename with out the numeric field
	CCharString Name = FileName.Left(pos);
	
	// Special case of no numeric field
	if (pos == n)
		number = 1;
	else
	{
		CCharString tmp = FileName.Right(n - pos);
		sscanf(tmp, "%ld", &number);
		number++;
	}

	int FieldLength = n - pos;//(int)log10(number) + 1;
	
	// Make the format string.  This insures leading zeros.
	CCharString Format;
	Format.Format("%%0%dd", FieldLength);
	
	CCharString strNumber;
	strNumber.Format(Format, number);
	
	// Finally the new filename	
	CCharString NewFileName = Name + strNumber;
	SetFileName(NewFileName);
	
	return true;
}

CFileName& CFileName::operator=(const CCharString& String)
{
	if (this != &String)
	{
		this->set(String);
		makeFilenameCompatbleWithOS();
	}

	return *this;
}

CFileName& CFileName::operator=(const CFileName& FileName)
{
	if (this != &FileName)
	{	
		this->set(FileName);
		makeFilenameCompatbleWithOS();
	}

	return *this;
}

CFileName& CFileName::operator=(const char* pChar)
{
	this->set(pChar);
	makeFilenameCompatbleWithOS();
	return *this;
}

void CFileName::makeFilenameCompatbleWithOS()
{
	for (int i = 0; i < this->GetLength(); ++i)
	{
#ifdef WIN32
		if (this->m_pChar[i] == '/') this->m_pChar[i] = CSystemUtilities::dirDelim;
#else
		if (this->m_pChar[i] == '\\') this->m_pChar[i] = CSystemUtilities::dirDelim;
#endif
	}
};