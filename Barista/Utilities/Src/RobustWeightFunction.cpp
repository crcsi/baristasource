// *
// * Title:        RobustWeightFunction<p>
// * Description:  <p>
// * Copyright:    Copyright (c) Franz Rottensteiner<p>
// * Company:      University of Melbourne<p>
// * @author Franz Rottensteiner
// @version 1.0
// *

#include <math.h>
#include <float.h>
#include "robustWeightFunction.h"


CRobustWeightFunction::CRobustWeightFunction() 
{
	setParameters(1.0, 3.0);
}


CRobustWeightFunction::~CRobustWeightFunction()
{
}

bool CRobustWeightFunction::setParameters(double hw, double sl, double mw)
{
	bool ret = true;
	if ((hw < FLT_EPSILON) || (sl < FLT_EPSILON)) ret = false;
	else
	{
		this->halfweight = hw;
		this->slant      = sl;
		this->markwidth  = mw;
		this->a          = 1.0 / this->halfweight;
		this->b          = 4.0 * this->halfweight / this->slant;
	}

	return ret;
};


bool CRobustWeightFunction::setParameters(double hw, double mw)
{
	return setParameters(hw, hw, mw);
}

double CRobustWeightFunction::getWeight(double residual) const
{
	residual = fabs(residual);
	if (residual > this->markwidth)
	{
		return 0.0f;
	}
	else
	{
		return (1.0 / (1.0 + pow(this->a * residual, b)));
	}
}