#include <strstream> 
using namespace std;

#include "PointBase.h"
#include "CharString.h"

//=============================================================================

CPointBase::CPointBase(): coordinates(0),dim(0),id(-1),dot(0.0)
{
}

//=============================================================================

CPointBase::CPointBase(int dimension): coordinates(0),dim(dimension),id(-1),dot(0.0)
{
	this->coordinates = new double [dimension];
	
	for (int i=0; i < dimension; i++)
		this->coordinates[i] = 0.0;
}

//=============================================================================

CPointBase::CPointBase(const CPointBase& point) : coordinates(0),dim(point.dim),
                                                  id(point.id),dot(point.dot)
{
	this->coordinates = new double [point.dim];

	for (int i=0; i < point.dim; i++)
		this->coordinates[i] = point.coordinates[i];
}

//=============================================================================

CPointBase::CPointBase(const CPointBase* point) : coordinates(0), dim(point->dim),
                                                   id(point->id),dot(point->dot)
{
	this->coordinates = new double [point->dim];

	for (int i=0; i < point->dim; i++)
		this->coordinates[i] = point->coordinates[i];
}

//=============================================================================

CPointBase::~CPointBase(void)
{
	delete [] this->coordinates;
	this->coordinates = 0;
}

//=============================================================================

CPointBase &CPointBase::operator = (const CPointBase & point)
{ 
	if (point.dim != this->dim)
	{
		delete [] this->coordinates;
		this->coordinates = new double [point.dim];
	}
		
	for (int i=0; i < point.dim; i++)
		this->coordinates[i] = point.coordinates[i];

	this->dim = point.dim;
	this->id = point.id;
	this->dot = point.dot;

	return *this; 
} 	

//=============================================================================

CCharString CPointBase::getString(CCharString delim, int width, int prec) const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(prec);

	if (delim.IsEmpty()) delim = " ";


	for (int i = 0; i < this->dim; ++i)
	{
		oss.width(width);
		oss << this->coordinates[i] << delim.GetChar();
	}

	oss << ends;
	char *z = oss.str();

	CCharString str(z);
	delete [] z;

	return str;
};
