// CharString.cpp: implementation of the CCharString class.
//
// Copyright 2000 Harry Hanley May 4 2000
//
//////////////////////////////////////////////////////////////////////

#include <strstream>
using namespace std;

#include <assert.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#ifndef WIN32
#define _strtime(s) {time_t t = time(0); strftime(s, 99, "%H:%M:%S", localtime (&t));}
#define _strdate(s) {time_t t = time(0); strftime(s, 99, "%d %b %Y", localtime (&t));}
#endif

#include "CharString.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// CCharString::CCharString()
//
//
CCharString::CCharString()
{
	_Init();
}

//////////////////////////////////////////////////////////////////////
// CCharString::_Init()
//
//
void CCharString::_Init()
{
	// Initialize
	m_pChar = (char*)0;
	m_BufferSize = 0;

	set("\0");
}
	
//////////////////////////////////////////////////////////////////////
// CCharString::CCharString(int number, int digits, bool leadingZeroes)
//
//
CCharString::CCharString(long number, int digits, bool leadingZeroes)
{
	_Init();

	ostrstream oss;
	oss.width(digits);
	oss << number << ends;
	char *c = oss.str();
	set(c);
	delete [] c;

	if (leadingZeroes)
	{
		int len = strlen(m_pChar);

		for (int i = 0; i < len; ++i)
		{
			if (m_pChar[i] == ' ') m_pChar[i] = '0';
		}
	}
}

//////////////////////////////////////////////////////////////////////
// CCharString::CCharString(int number, int digits, bool leadingZeroes, bool sign)
//
//
CCharString::CCharString(long number, int digits, bool leadingZeroes, bool sign)
{ // this function takes care of sign of the number if bool sign = true
	// if bool sign = false then this constructor is same as CCharString::CCharString(long number, int digits, bool leadingZeroes)
	if (sign)
	{
		bool csign; // true for + and false for -
		if (number < 0)
		{
			csign = false;
			number = -number;
		}
		else
			csign = true;

		//copy without sign
		_Init();
		ostrstream oss;
		oss.width(digits);
		oss << number << ends;
		char *c = oss.str();
		set(c);
		delete [] c;

		if (leadingZeroes)
		{
			if (csign)
				m_pChar[0] = '+';
			else
				m_pChar[0] = '-';

			int len = strlen(m_pChar);

			for (int i = 1; i < len; ++i)
			{
				if (m_pChar[i] == ' ') m_pChar[i] = '0';
				else break;
			}
		}
		else
		{
			int len = strlen(m_pChar);
			int i;
			for (i = 0; i < len; ++i)
			{				
				if (m_pChar[i] != ' ') break;
			}
			if (csign)
				m_pChar[i-1] = '+';
			else
				m_pChar[i-1] = '-';
		}
	}
	else
	{
		CCharString(number,digits,leadingZeroes);
		/*_Init();

		ostrstream oss;
		oss.width(digits);
		oss << number << ends;
		char *c = oss.str();
		set(c);
		delete [] c;

		if (leadingZeroes)
		{
			int len = strlen(m_pChar);

			for (int i = 0; i < len; ++i)
			{
				if (m_pChar[i] == ' ') m_pChar[i] = '0';
			}
		}*/
	}
}

/*
//////////////////////////////////////////////////////////////////////
// CCharString::CCharString(int number, int digits, bool leadingZeroes)
//
//
CCharString::CCharString(unsigned long number, int digits, bool leadingZeroes)
{
	_Init();

	ostrstream oss;
	oss.width(digits);
	oss << number << ends;
	char *c = oss.str();
	set(c);
	delete [] c;

	if (leadingZeroes)
	{
		int len = strlen(m_pChar);

		for (int i = 0; i < len; ++i)
		{
			if (m_pChar[i] == ' ') m_pChar[i] = '0';
		}
	}
}
*/
//////////////////////////////////////////////////////////////////////
// CCharString::CCharString(double number, int digits, int precision)
//
//

CCharString::CCharString(double number, int digits, int precision)
{
	_Init();

	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(precision);
	oss.width(digits);
	oss << number << ends;
	char *c = oss.str();
	set(c);
	delete [] c;
	
};

//////////////////////////////////////////////////////////////////////
// CCharString::CCharString(double number, int digits, int precision, bool leadingZeroes, bool sign)
//
//

CCharString::CCharString(double number, int digits, int precision, bool leadingZeroes, bool sign)
{// this function takes care of sign of the number if bool sign = true
	// if bool sign = false then this constructor is same as CCharString::CCharString(double number, int digits, int precision)
	if (sign)
	{
		bool csign; // true for + and false for -
		if (number < 0)
		{
			csign = false;
			number = -number;
		}
		else
			csign = true;

		//copy without sign
		_Init();
		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(precision);
		oss.width(digits);
		oss << number << ends;
		char *c = oss.str();
		set(c);
		delete [] c;

		if (leadingZeroes)
		{
			if (csign)
				m_pChar[0] = '+';
			else
				m_pChar[0] = '-';

			int len = strlen(m_pChar);

			for (int i = 1; i < len; ++i)
			{
				if (m_pChar[i] == ' ') m_pChar[i] = '0';
				else break;
			}
		}
		else
		{
			int len = strlen(m_pChar);
			int i;
			for (i = 0; i < len; ++i)
			{				
				if (m_pChar[i] != ' ') break;
			}
			if (csign)
				m_pChar[i-1] = '+';
			else
				m_pChar[i-1] = '-';
		}
	}
	else
	{
		CCharString(number,digits,precision);
		/*_Init();

		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(precision);
		oss.width(digits);
		oss << number << ends;
		char *c = oss.str();
		set(c);
		delete [] c;*/
	}
};


//////////////////////////////////////////////////////////////////////
// CCharString::setScientificNumber(double number, int digits, int precision, bool leadingZeroes, bool sign)
//
//

void CCharString::setScientificNumber(double number, int precision, bool leadingZeroesInPower, int MaxPowerDigits)
{// minimum digits for Power is 1; if leadingZeroesInPower = false, then MaxPowerDigits is ignored;
	// if leadingZeroesInPower = true, then MaxPowerDigits will be the number of digits in power
	// if the required power digits is more than MaxPowerDigits, then then MaxPowerDigits is ignored;
	//{	// we assume maximum 3 digits for power
		bool csign; // true for + and false for -
		if (number < 0)
		{
			csign = false;
			number = -number;
		}
		else
			csign = true;
		
		int pow = 0;
		char powSign[2];
		powSign[0] = '+';
		powSign[1] = '\0';
		if (number == 0)
		{
			//pow = 0;
			//powSign[0] = '+';
		}
		else if (number < 1) // example: 0.012589
		{
			powSign[0] = '-';
			number *= 10;
			pow++;
			while (number < 1)
			{
				number *= 10;
				pow++;	
			}
		}
		else if (number > 9) //example: 25.552023
		{
			powSign[0] = '+';
			number /= 10;
			pow++;
			while (number > 9)
			{
				number /= 10;
				pow++;
			}
		}
		// else already in scientific format

		int powerDigits, digits;		
		if (pow < 10)
		{
			powerDigits = 1;
			//digits = 5 + precision + powerDigits; //format: +1.123456E-p
		}
		else if (pow > 9 && pow < 100)
		{
			powerDigits = 2;
			//digits = 5 + precision + powerDigits; //format: +1.123456E-pp
		}
		else if (pow > 99 && pow < 1000)
		{
			powerDigits = 3;
			//digits = 5 + precision + powerDigits; //format: +1.123456E-ppp
		}
		if (leadingZeroesInPower && powerDigits < MaxPowerDigits)
			powerDigits = MaxPowerDigits;
		//else ignore argumnet: MaxPowerDigits
		digits = 5 + precision + powerDigits;

		char powerPlace[5];
		CCharString pw(pow,powerDigits,true);
		//_itoa(pow,powerPlace,10);
		strcpy(powerPlace,pw.GetChar());
		int pDig = powerDigits+2;
		char p[10];
		p[0] = '\0';
		strcat(p,"E");
		strcat(p,powSign);
		strcat(p,powerPlace);
		//powerPlace[0] = 'E';
		//if 
		//int i = 1;
		//for (; i < powerDigits; i++)
		//	powerPlace[i] = ' ';
		//powerPlace[i] = '\0';

		//copy without sign
		_Init();
		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(precision);
		oss.width(digits-pDig); //pDig spaces for power digits
		oss << number << ends;
		char *c = oss.str();
		strcat(c,p);
		set(c);
		delete [] c;

		//if (leadingZeroes)
		//{
			if (csign)
				m_pChar[0] = '+';
			else
				m_pChar[0] = '-';

			//int len = strlen(m_pChar);

			//for (int i = 1; i < len; ++i)
			//{
			//	if (m_pChar[i] == ' ') m_pChar[i] = '0';
			//	else break;
			//}
		//}
		//else
		//{
		//	int len = strlen(m_pChar);
		//	int i;
		//	for (i = 0; i < len; ++i)
		//	{				
		//		if (m_pChar[i] != ' ') break;
		//	}
		//	if (csign)
		//		m_pChar[i-1] = '+';
		//	else
		//		m_pChar[i-1] = '-';
		//}
	//}
	//else
	//{
	//	CCharString(number,digits,precision);
		/*_Init();

		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(precision);
		oss.width(digits);
		oss << number << ends;
		char *c = oss.str();
		set(c);
		delete [] c;*/
	//}
};

//////////////////////////////////////////////////////////////////////
// CCharString::CCharString(const char* pChar)
//
//
CCharString::CCharString(const char* pChar)
{
	_Init();

	bool b = set(pChar);

	// Check status
	if (!b)
		assert(false);
}

//////////////////////////////////////////////////////////////////////
// CCharString::CCharString(CCharString& String)
//
//

CCharString::CCharString(const CCharString& String)
{
	_Init();

	bool b = set(String.m_pChar);

	// Check status
	if (!b)
		assert(false);
}

CCharString::CCharString(const string &String)
{
	const char *pc = String.c_str();
	_Init();

	bool b = set(pc);

	// Check status
	if (!b)
		assert(false);
}


//////////////////////////////////////////////////////////////////////
// CCharString::~CCharString()
//
//
CCharString::~CCharString()
{
	if (m_pChar != (char*)0)
		delete [] m_pChar;

	m_pChar = (char*)0;
}

//////////////////////////////////////////////////////////////////////
// bool CCharString::Set(const char *pChar)
//
//
bool CCharString::set(const char *pChar)
{
	// Sanity check
	if (pChar == (char*)0)
		return true;

	int len = strlen(pChar);

	if (m_BufferSize == 0)
	{
		// New string, allocate memory
		
		// Sanity Check
		assert(m_pChar == (void*)0);

		// Allocate enough room for '\0' terminator
		_NewBuffer(len + 1);
	}
	else if (m_BufferSize <= len)
	{
		// Need to Allocate more momory

		// Sanity Check
		assert(m_pChar != (char*)0);

		// Allocate enough room for '\0' terminator
		_NewBuffer(len + 1);
	}

	// do the copy
	strcpy(m_pChar, pChar);

	return true;
}
	
void CCharString::addInteger(int i, int digits)
{
	ostrstream oss;
	if (digits > 0) oss.width(digits);
	oss << i << ends;
	char *z = oss.str();
	*this += z;
	delete[] z;
};

//////////////////////////////////////////////////////////////////////
// bool CCharString::IsEmpty() const
//
// empties buffer
//
bool CCharString::IsEmpty() const
{
	if (m_pChar == (char*)0)
		return true;

	if (m_pChar[0] == '\0')
		return true;

	return false;
}

//////////////////////////////////////////////////////////////////////
// void CCharString::NewBuffer(int length)
//
// Delete memory and reallocates to current size
//
void CCharString::_NewBuffer(int length)
{
	// If the memory is there free it
	if (m_pChar != (void*)0)
		delete [] m_pChar;

	// Allocate space
	m_pChar = new char[length];

	// Set the buffer size
	m_BufferSize = length;
}

//////////////////////////////////////////////////////////////////////
// bool CCharString::Set(const CCharString& Label)
//
//
bool CCharString::set(const CCharString& String)
{
	// do the copy
	return set(String.m_pChar);

}

//////////////////////////////////////////////////////////////////////
// int	CCharString::GetLength()
//
//
int	CCharString::GetLength() const
{
	if (m_pChar == (void*)0)
		return 0;
	else
		return strlen(m_pChar);
}

bool CCharString::replace(const CCharString &findStr, const CCharString &replaceByStr)
{
	int whereIs = this->Find(findStr);
	if (whereIs < 0) return false;

	CCharString left  = this->Left(whereIs);
	CCharString right = this->Right(this->GetLength() - (whereIs + findStr.GetLength()));
	*this = left + replaceByStr + right;

	return true;
};

//////////////////////////////////////////////////////////////////////
// CCharString::operator==(const CCharString& String)
//
//
bool CCharString::operator==(const CCharString& String)
{
	// Varify they are the same length
	if (GetLength() != String.GetLength())
		return false;

	// Compare each character
	for (int i = 0; i < GetLength(); i++)
	{
		if ( m_pChar[i] != String.m_pChar[i] )
			return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////
// CCharString::operator==(const char* pString)
//
//
bool CCharString::operator==(const char* pString)
{
	CCharString tempstring = pString;

	// Varify they are the same length
	if (GetLength() != tempstring.GetLength())
		return false;

	// Compare each character
	for (int i = 0; i < GetLength(); i++)
	{
		if ( m_pChar[i] != tempstring.m_pChar[i] )
			return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////
// CCharString::operator!=(const CCharString& String)
//
//
bool CCharString::operator!=(const CCharString& String)
{
	if (*this == String)
		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////
// CCharString operator=(const char* pChar)
//
//
CCharString& CCharString::operator=(const char* pChar)
{
	set(pChar);

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CCharString& CCharString::operator=(CCharString&)
//
//
CCharString& CCharString::operator=(const CCharString& String)
{
	set(String.m_pChar);

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CCharString& CCharString::operator+=(CCharString& String)
//
//
CCharString& CCharString::operator+=(const CCharString& String)
{
	*this += String.m_pChar;

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CCharString& CCharString::operator+=(int c)
//
//
CCharString& CCharString::operator+=(int c)
{
	char CH[2];
	
	CH[0] = (char)c;
	CH[1] = '\0';
	
	*this += CH;

	return *this;
}

//////////////////////////////////////////////////////////////////////
// CCharString& CCharString::operator+=(const char* pChar)
//
//
CCharString& CCharString::operator+=(const char* pChar)
{
	// Check if it's empty
	if (m_BufferSize == 0)
	{
		if (pChar)
			set(pChar);

		return *this;
	}

	if (!pChar)
		return *this;

	// We now expect that m_pChar points to something
	assert(m_pChar != (char*)0);

	int len1 = GetLength();
	int len2 = strlen(pChar);
	int total = len1 + len2;

	// Does the new string fit ?
	if (m_BufferSize <= total)
	{
		int NewSize = total + 1;
		// make new buffer
		char* pTmp = new char[NewSize];

		// Copy the first piece
		strcpy(pTmp, m_pChar);

		// Copy the second piece
		strcpy(&pTmp[len1], pChar);

		// delete old pointer
		delete [] m_pChar;

		// Re-assign pointer
		m_pChar = pTmp;
		m_BufferSize = NewSize;

		return *this;
	}
	else
	{
		// String fits, then just copy the pieces

		// Copy the second piece
		strcpy(&m_pChar[len1], pChar);

		return *this;
	}
}

//////////////////////////////////////////////////////////////////////
// CCharString::operatorconst char*() const
//
// johannes: can make problems, if m_pChar = NULL
CCharString::operator const char*() const
{
	return (const char*)m_pChar;
}

//////////////////////////////////////////////////////////////////////
// const char* GetChar()
//
//
const char* CCharString::GetChar() const
{
	return (const char*)m_pChar;
}

//////////////////////////////////////////////////////////////////////
// int CCharString::Find(char c) const
//
//
int CCharString::Find(char c) const
{
	if (m_pChar == (void*)0)
		return -1;

	// Search the string
	int len = GetLength();
	for (int i = 0; i < len; i++)
	{
		if (m_pChar[i] == c)
			return i;
	}

	// Did not find 'c'
	return -1;
}

//////////////////////////////////////////////////////////////////////
// int CCharString::FindNextNumberLength()
//
//
int CCharString::FindNextNumberLength()
{	//finds length of next scientific number starting from beginning of the string
	if (m_pChar == (void*)0)
		return -1;

	// Search the string
	int len = GetLength();
	int found = 0;
	bool foundE = false;
	for (int i = 0; i < len; i++)
	{
		if (m_pChar[i] == '+' || m_pChar[i] == '-')
			found++;
		else if (foundE == false && (m_pChar[i] == 'e' || m_pChar[i] == 'E'))
			foundE = true;
		else if (foundE == true && (m_pChar[i] < '0' || m_pChar[i] > '9') && found == 2)
			found++;

		if (found == 3)
			return i;
	}

	// Did not find 'c'
	return -1;
}

int CCharString::Find(const CCharString& String) const
{
    return this->Find((char*)String.GetChar());
}



//////////////////////////////////////////////////////////////////////
// int CCharString::Find(char* pChar) const
//
//
int CCharString::Find(char* pChar) const
{
	// sanity
	if( m_pChar == (void*)0 )
		return -1;

	assert(pChar != (void*)0);

	// Search the string
	int len = GetLength();
	int len2 = strlen(pChar);

	for (int i = 0; i <= len - len2; i++)
	{
                int j;
		for (j = 0; j < len2; j++)
		{
			if (m_pChar[i + j] != pChar[j])
				break;
		}

		// Check if a match was found
		if (j == len2)
			return i;
	}

	// Did not find 'c'
	return -1;
}

//////////////////////////////////////////////////////////////////////
// CCharString CCharString::Left(int n) const
//
//
CCharString CCharString::Left(int n) const
{
	CCharString dest;

	assert(m_pChar != (void*)0);

	int len = GetLength();

	// Sanity checks
	assert(n <= len);

	// Allocate dest buffer
	dest._NewBuffer(n + 1);

	strncpy(dest.m_pChar, m_pChar, n);

	// Null Terminate
	dest.m_pChar[n] = '\0';

	return dest;
}

//////////////////////////////////////////////////////////////////////
// CCharString CCharString::Right(int n) const
//
//
CCharString CCharString::Right(int n) const
{
	CCharString dest;

	assert(m_pChar != (void*)0);

	int len = GetLength();

	// Sanity checks
	assert(n <= len);

	// Allocate dest buffer
	dest._NewBuffer(n + 1);

	strncpy(dest.m_pChar, &m_pChar[len - n], n);

	// Null Terminate
	dest.m_pChar[n] = '\0';

	return dest;
}

//////////////////////////////////////////////////////////////////////
// CCharString CCharString::Mid(int l, int r) const
//
//
CCharString CCharString::Mid(int l, int r) const
{
	CCharString dest;

	assert(m_pChar != (void*)0);

	int n = r - l + 1;
	int len = GetLength();

	// Sanity checks
	assert(n <= len);
	assert(r < len);
	assert(l < len);

	// Allocate dest buffer
	dest._NewBuffer(n + 1);

	strncpy(dest.m_pChar, &m_pChar[l], n);

	// Null Terminate
	dest.m_pChar[n] = '\0';

	return dest;
}


//////////////////////////////////////////////////////////////////////
// int CCharString::ReverseFind(char c) const
//
//
int CCharString::ReverseFind(char c) const
{
	// Check for null
	if (m_pChar == (void*)0)
		return -1;

	// Search the string (backwards)
	for (int i = GetLength() - 1; i >= 0; i--)
	{
		if (m_pChar[i] == c)
			return i;
	}

	// Did not find 'c'
	return -1;
}

//////////////////////////////////////////////////////////////////////
// bool CCharString::MakeUpper()
//
// force upper case
//
bool CCharString::MakeUpper()
{
	int len = GetLength();

	for ( int i = 0; i < len; i++ )
		m_pChar[i] = toupper(m_pChar[i]);

	return true;
}

//////////////////////////////////////////////////////////////////////
// bool CCharString::MakeLower()
//
// force lower case
//
bool CCharString::MakeLower()
{
	int len = GetLength();

	for ( int i = 0; i < len; i++ )
		m_pChar[i] = tolower(m_pChar[i]);

	return true;
}

//////////////////////////////////////////////////////////////////////
// bool CCharString::Empty()
//
//
void CCharString::Empty()
{
	if (m_pChar == (char*)0)
		return;

	m_pChar[0] = '\0';
}

//////////////////////////////////////////////////////////////////////
// void CCharString::_AddChar(char ch)
//
//
void CCharString::_AddChar(char ch)
{
	char buf[2];

	buf[0] = ch;
	buf[1] = '\0';

	*this += buf;
}

//////////////////////////////////////////////////////////////////////
// CCharString::Format(const char* pFmtString, ...)
//
//
void CCharString::Format(const char* pFmtString, ...)
{
	// Clear String
	Empty();

	// Walk the string
	va_list marker;
	va_start(marker, pFmtString);


	_FormatCore (pFmtString, marker);

	va_end(marker);
}

//////////////////////////////////////////////////////////////////////
// CCharString::_FormatCore (const char* pFmtString, va_list& marker)
//
//
void CCharString::_FormatCore (const char* pFmtString, va_list& marker)
{
	for (;;) 
	{
		// Locate next % sign, copy chunk, and exit if no more
		const char* pNext = strchr (pFmtString, '%');

		// Check for end of Format string
		if (pNext == NULL)
			break;

		// Check for regular characters to add
		if (pNext > pFmtString)
			_CoreAddChars (pFmtString, (int)(pNext - pFmtString));

		// Move just beyond '%'
		pFmtString = pNext + 1;

		// We're at a parameter
		if (!_FormatOneValue (pFmtString, marker))
			break;		// Copy rest of string and return
	}

	if (pFmtString[0] != 0)
		*this += pFmtString;
}

//////////////////////////////////////////////////////////////////////
// void CCharString::_CoreAddChars(const char* s, int size)
//
//
void CCharString::_CoreAddChars(const char* s, int size)
{
	// Allocate
	char* pBuffer = new char[size +1];

	// Copy
	strncpy(pBuffer, s, (size_t)size);

	// Null Terminate
	pBuffer[size] = '\0';

	// Concatinate
	*this += pBuffer;

	// Free
	delete [] pBuffer;
}

//////////////////////////////////////////////////////////////////////
// bool CCharString::_FormatOneValue (const char*& FmtString, va_list& marker)
//
//
bool CCharString::_FormatOneValue (const char*& FmtString, va_list& marker)
{
	// Start copying format specifier to a local buffer
	char fsbuf[FORMAT_MAX_FIELD_LENGTH];
	fsbuf[0] = '%';
	int fsp = 1;
	char valbuf[FORMAT_MAX_FIELD_LENGTH];

	char ch;

	for (;;)
	{
		// Sanity check
		if (fsp >= sizeof(fsbuf)) 
		{
			assert(false);
			return false;
		}

		// Check for end of format string.
		ch = FmtString[0];

		if (ch == 0)
			return false;
		
		FmtString++;

		// Chars that may exist in the format prefix
		const char fprefix[] = "-+0 #*.123456789hlL";

		// Did we find a prefix char?
		if (strchr (fprefix, ch) == NULL) 
			break;

		// We found a prefix character so add it to 
		// fsbuf and continue
		fsbuf[fsp] = ch;
		fsp++;
	}

	// 's' is the most important parameter specifier type
	if (ch == 's') 
	{
		// Get the value to be formated
		const char* string = va_arg (marker, const char*);

		fsbuf[fsp] = ch;
		fsbuf[fsp+1] = '\0';

		valbuf[0] = '\0';

		sprintf (valbuf, fsbuf, string);
		*this += valbuf;

		return true;
	}

	// Chars that format an integer value
	const char intletters[] = "cCdiouxX";

	if (strchr (intletters, ch) != NULL) 
	{
		fsbuf[fsp] = ch;
		fsbuf[fsp+1] = 0;

		int value = va_arg (marker, int);
		sprintf (valbuf, fsbuf, value);

		*this += valbuf;
		return true;
	}

	// Chars that format a double value
	const char dblletters[] = "eEfgG";

	if (strchr (dblletters, ch) != NULL) 
	{
		fsbuf[fsp] = ch;
		fsbuf[fsp+1] = 0;

		double value = va_arg (marker, double);
		sprintf (valbuf, fsbuf, value);

		*this += valbuf;
		return true;
	}

	// 'Print pointer' is supported
	if (ch == 'p') 
	{
		fsbuf[fsp] = ch;
		fsbuf[fsp+1] = 0;

		void* value = va_arg (marker, void*);
		sprintf (valbuf, fsbuf, value);

		*this += valbuf;
		return true;
	}

	// If we fall here, the character does not represent an item
	_AddChar (ch);
	return true;
}

//////////////////////////////////////////////////////////////////////
// bool CCharString::CompareNoCase(const CCharString& String)
//
// compares strings, doesn't consider case
//
bool CCharString::CompareNoCase(const CCharString& String) const
{
	if (strlen(this->m_pChar) != strlen(String.m_pChar))
		return false;

	if (strcmp(this->m_pChar, String.m_pChar) == 0)
		return true;

	return CompareNoCase(String.m_pChar);
}

//////////////////////////////////////////////////////////////////////
// bool CCharString::CompareNoCase(const char* pChar)
//
// compares strings, doesn't consider case
//
bool CCharString::CompareNoCase(const char* pChar) const
{
	// Check null pointer conditions
	if (pChar == (char*)0)
	{
		if (m_pChar == (char*)0)
			return true;
		else
			return false;
	}
	else if (m_pChar == (char*)0)
		return false;

	int len1 = GetLength();
	int len2 = strlen(pChar);

	// they have to be the same length for a chance
	if (len1 != len2)
		return false;
         
        char* tmp1 = new char[len1+1];
        char* tmp2 = new char[len2+1];
        strcpy(tmp1, this->m_pChar);
        strcpy(tmp2, pChar);
        
        int i;
        for (i = 0; i < len1; i++)
            tmp1[i] = toupper(tmp1[i]);
        for (i = 0; i < len2; i++)
            tmp2[i] = toupper(tmp2[i]);
       
	//int rv = strnicmp(m_pChar, pChar, len1);
	int rv = strncmp(tmp1, tmp2, len1);

    delete [] tmp1;
    delete [] tmp2;
        
	if (rv == 0)
            return true;

	return false;
}

void CCharString::CentreJustify(int cols)
{
	// Check null pointer conditions
	if (m_pChar == (char*)0)
		return;

	int len = GetLength();

	if (len >= cols)
		return;

	CCharString newString;

	int dif = cols - len;

        int i;
	for (i = 0; i < dif/2; i++)
		newString += " ";

	for (i = 0; i < GetLength(); i++)
		newString += m_pChar[i];

	*this = newString;
}

ostream& operator<<(ostream& s, const CCharString& rString)
{
	s << rString.GetChar();
	return s;
}

////////////////////////////////////////////////////////
// trims out white spaces before and after this string
// white spaces include everything below ASCII code 33
//				    and everything above ASCII code 127
////////////////////////////////////////////////////////
void CCharString::trim()
{
	int length = ::strlen(this->m_pChar);

	if (length == 0)
		return;

	char* tmp = new char[length+1];
	::strcpy(tmp, this->m_pChar);
	::memset(this->m_pChar, 0, length);

	// find the index of the first acceptable char
	int firstGood = -1;

	for(int i = 0; i < length; i++)
	{
		if(firstGood == -1)
		{
			if (tmp[i] > 32 && tmp[i] < 127)
			{
				firstGood = i;
				break;
			}
			
		}
	}

	// find the index of the last acceptable char
	int lastGood = -1;

	for(int i = length - 1; i >= 0 ; i--)
	{
		if(lastGood == -1)
		{
			if (tmp[i] > 32 && tmp[i] < 127)
			{
				lastGood = i;
				break;
			}
		}
	}

	for(int index = 0, i = firstGood; i <= lastGood ; i++)
	{
		// only allow spaces or normal characters in the new string
		if(tmp[i] >= 32 || tmp[i] < 127)
		{
			this->m_pChar[index] = tmp[i];
			index++;
		}
	}

	delete [] tmp;
}

bool CCharString::isLessThan(CCharString& str)
{
	int result = ::strcmp(this->GetChar(), str.GetChar());

	if (result < 0)
		return true;

	return false;
}

bool CCharString::isGreaterThan(CCharString& str)
{
	int result = ::strcmp(this->GetChar(), str.GetChar());

	if (result > 0)
		return true;

	return false;
}

bool CCharString::isNumber()
{
	int decimalCount = 0;
	int minusCount = 0;
	int plusCount = 0;
	int ECount = 0;
	for (int i = 0; i < this->GetLength(); i++)
	{
		// make sure there is only one decimal point
		if (this->m_pChar[i] == '.')
		{
			decimalCount++;
			if (decimalCount > 1)
				return false;

			continue;
		}

		if (this->m_pChar[i] == '-')
		{
			minusCount++;
			if (minusCount > 2)
				return false;

			continue;
		}		
		
		if (this->m_pChar[i] == '+')
		{
			plusCount++;
			if (plusCount > 2)
				return false;

			continue;
		}

		if ( (this->m_pChar[i] == 'e') || (this->m_pChar[i] == 'E') )
		{
			ECount++;
			if (ECount > 1)
				return false;

			continue;
		}



		// Make sure it is in this range
		if (!::isalnum(this->m_pChar[i]))
			return false;

		// but not in this range
		if (::isalpha(this->m_pChar[i]))
			return false;
	}

	return true;
}
 
CCharString CCharString::getTimeString() 
{
	char *timeString = new char[9];
	_strtime(timeString);
	CCharString ret(timeString);  
	delete[] timeString;

	return ret;
};

CCharString CCharString::getDateString() 
{
	char *date = new char[9];
	_strdate(date);

	CCharString dateString(date);
	delete[] date;

	return dateString;
};

////////////////////////////////////////////////////////
// trims out white spaces before and after this string
// white spaces include everything below ASCII code 48
//				    and everything above ASCII code 57
////////////////////////////////////////////////////////
void CCharString::trimCharacters()
{
	int length = ::strlen(this->m_pChar);

	if (length == 0)
		return;

	char* tmp = new char[length+1];
	::strcpy(tmp, this->m_pChar);
	::memset(this->m_pChar, 0, length);

	// find the index of the first acceptable char
	int firstGood = -1;

	for(int i = 0; i < length; i++)
	{
		if(firstGood == -1)
		{
			if (tmp[i] >= 48 && tmp[i] <= 57)
			{
				firstGood = i;
				break;
			}
			
		}
	}

	// find the index of the last acceptable char
	int lastGood = -1;

	for(int i = length - 1; i >= 0 ; i--)
	{
		if(lastGood == -1)
		{
			if (tmp[i] >= 48 && tmp[i] <= 57)
			{
				lastGood = i;
				break;
			}
		}
	}

	for(int index = 0, i = firstGood; i <= lastGood ; i++)
	{
		// only allow spaces or normal characters in the new string
		if(tmp[i] >= 48 || tmp[i] <= 57)
		{
			this->m_pChar[index] = tmp[i];
			index++;
		}
	}

	delete [] tmp;
}

CCharString	operator+(const CCharString &String1, const CCharString &String2)
{
	CCharString str(String1);
	str += String2;
	return str;
};

CCharString	operator+(const CCharString &String1, const char *pChar2)
{
	CCharString str(String1);
	str += pChar2;
	return str;
};

CCharString	operator+(const char *pChar1, const CCharString &String2)
{
	CCharString str(pChar1);
	str += String2;
	return str;
};
