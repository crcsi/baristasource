#include <assert.h>
#include "BlockDiagonalMatrix.h"

#include "newmat.h"

CBlockDiagonalMatrix::CBlockDiagonalMatrix()
{
}

CBlockDiagonalMatrix::CBlockDiagonalMatrix(const vector<Matrix *> &Covariance)
{
	for (unsigned int i = 0; i < Covariance.size(); ++i)
	{
		this->addSubMatrix(*Covariance[i]);
	}
}

/**
    * Constructs a block diagonal matrix were each block is the same size
    * @param sr, sc Rows / Cols of the submatrixes
    * @param n number of sub-matrixes
    *
    */
CBlockDiagonalMatrix::CBlockDiagonalMatrix(int sr, int sc, int n): subMatrices(n, (Matrix *) 0)
{
    for (unsigned int i = 0; i < this->subMatrices.size(); i++)
    {
		this->subMatrices[i] = new Matrix(sr, sc);
    }
}

/**  
    * Copy constructor 
    * @param src matrix to be copied
    *
    */
CBlockDiagonalMatrix::CBlockDiagonalMatrix(const CBlockDiagonalMatrix &src): subMatrices(src.subMatrices.size(), 0)
{
    for (unsigned int i = 0; i < this->subMatrices.size(); i++)
    {
		this->subMatrices[i] = new Matrix(*src.subMatrices[i]);
    }
}

CBlockDiagonalMatrix::~CBlockDiagonalMatrix(void)
{
	this->deleteAllSubmatrices();
}
	
void CBlockDiagonalMatrix::deleteAllSubmatrices()
{
	for (unsigned int i = 0; i < this->subMatrices.size(); ++i)
	{
		delete this->subMatrices[i];
		this->subMatrices[i] = 0;
	}

	this->subMatrices.clear();
};

/**
  * Get the number of rows of the entire matrix
  */
unsigned int CBlockDiagonalMatrix::getRowDimension() const 
{
    unsigned int r = 0;
    for (unsigned int i = 0; i < this->subMatrices.size(); i++)
    {
        r += this->subMatrices[i]->Nrows();
    }

    return r;
}
/**
    * Gets the number of columns of entire matrix
    */
unsigned int CBlockDiagonalMatrix::getColumnDimension () const
{
    unsigned int c = 0;
    for (unsigned int i = 0; i < this->subMatrices.size(); i++)
    {
        c += this->subMatrices[i]->Ncols();
    }

    return c;
}
/**
    * Get the summatrix count
    */
unsigned int CBlockDiagonalMatrix::getSubMatrixCount() const
{
    return this->subMatrices.size();
}


/**
    * Gets a submatrix
    * @param index Index of the sub-matrix
    * @return a Matrix that is the sub-matrix at the specified index
    */
Matrix* CBlockDiagonalMatrix::getSubMatrix(int index) const
{
    if (index < 0) return NULL;

    if ((unsigned int) index >= this->subMatrices.size()) return NULL;

    return this->subMatrices[index];
}
/** Maeks a copy of matrix
 *
 *
 */
CBlockDiagonalMatrix &CBlockDiagonalMatrix::operator = (const CBlockDiagonalMatrix &src)
{
	if (this != &src)
	{
		this->deleteAllSubmatrices();

		this->subMatrices.resize(src.subMatrices.size(), 0);

		// add a copy of each sub matrix
		for (unsigned int i = 0; i < this->subMatrices.size(); i++)
		{
			this->subMatrices[i] = new Matrix(*src.subMatrices[i]);
		}
	}

	return *this;
}


/**
  * Add a sub (block)
  *
  */
void CBlockDiagonalMatrix::addSubMatrix(Matrix &M)
{
	this->subMatrices.push_back(new Matrix(M));
}

/**
 * Resized and re-allocated matrix
 *
 */
void CBlockDiagonalMatrix::resize(int sr, int sc, int n)
{
	this->deleteAllSubmatrices();

	this->subMatrices.resize(n, 0);

	for (unsigned int i = 0; i < this->subMatrices.size(); i++)
    {
		this->subMatrices[i] = new Matrix(sr, sc);
	}
}
/**
  * Computes the inverse of the matrix
  *
  * @return a BlockDiagonalMatrix representing the inverse
  */
void CBlockDiagonalMatrix::inverse(CBlockDiagonalMatrix &I) const
{
	I.deleteAllSubmatrices();
	I.subMatrices.resize(this->subMatrices.size(), 0);

    for (unsigned int i = 0; i < this->subMatrices.size(); i++)
    {
        I.subMatrices[i] = new Matrix(this->subMatrices[i]->i());
    }
}


/**
  * Computes the transpose of the matrix
  *
  * @return a BlockDiagonalMatrix representing the inverse
  */
void CBlockDiagonalMatrix::transpose(CBlockDiagonalMatrix &T) const
{
	T.deleteAllSubmatrices();
	T.subMatrices.resize(this->subMatrices.size(), 0);

    for (unsigned int i = 0; i < this->subMatrices.size(); i++)
    {
        T.subMatrices[i]= new Matrix(this->subMatrices[i]->t());
    }
}

/**
  * Multiplies A * B where a is a BlockDiagonal Matrix
  */
void CBlockDiagonalMatrix::times(Matrix &result, const Matrix &B) const
{
    if (this->getColumnDimension() != B.Nrows())
    {
        //throw new IllegalArgumentException("Matrix inner dimensions must agree.");
		assert(false);

		return;
    }

    result.ReSize(this->getRowDimension(), B.Ncols());

    int rowStart = 1;
    for (unsigned int m = 0; m < this->getSubMatrixCount(); m++)
    {
        Matrix* Asub = this->getSubMatrix(m);

		Matrix Bsub = B.SubMatrix(rowStart, rowStart+Asub->Ncols(), 0, B.Ncols());

        Matrix Xsub = (*Asub) * Bsub;

		for (int rr = 1, r = rowStart; r <= rowStart+Asub->Ncols(); r++, rr++)
		{
			for (int cc = 1, c = 0; c <= B.Ncols(); c++, cc++)
			{
				result(r, c) = Xsub(rr, cc);
			}
		}

        rowStart += Asub->Nrows();
    }
}
