#include "AdjustMatrix.h"
#include "newmat.h"
#include "PointBase.h"

CAdjustMatrix::CAdjustMatrix(int r, int c) : rows(r), cols(c)
{
	this->data = new double *[this->rows];
	for (int i = 0; i < this->rows; ++i)
	{
		this->data[i] = new double [this->cols];
	}
};	

CAdjustMatrix::CAdjustMatrix(int r, int c, double val) : rows(r), cols(c)
{
	this->data = new double *[this->rows];
	for (int i = 0; i < this->rows; ++i)
	{
		this->data[i] = new double [this->cols];
		for (int j = 0; j < this->cols; ++j) this->data[i][j] = val;

	}
};	

CAdjustMatrix::CAdjustMatrix(const CAdjustMatrix &mat) : rows(mat.rows), cols(mat.cols)
{
	this->data = new double *[this->rows];
	for (int i = 0; i < this->rows; ++i)
	{
		this->data[i] = new double [this->cols];
		for (int j = 0; j < this->cols; ++j) this->data[i][j] = mat.data[i][j];
	}
}

void CAdjustMatrix::reSize(int r,int c)
{
	for (int i = 0; i < this->rows; ++i)
	{
		delete [] this->data[i];
	}
	delete [] this->data;

	this->rows = r;
	this->cols = c;

	this->data = new double *[this->rows];
	for (int i = 0; i < this->rows; ++i)
	{
		this->data[i] = new double [this->cols];
	}


}

void CAdjustMatrix::reSize(int r,int c,double val)
{
	for (int i = 0; i < this->rows; ++i)
	{
		delete [] this->data[i];
	}
	delete [] this->data;

	this->rows = r;
	this->cols = c;

	this->data = new double *[this->rows];
	for (int i = 0; i < this->rows; ++i)
	{
		this->data[i] = new double [this->cols];
		for (int j = 0; j < this->cols; ++j) this->data[i][j] = val;

	}


}

CAdjustMatrix::~CAdjustMatrix()
{
	for (int i = 0; i < this->rows; ++i)
	{
		delete [] this->data[i];
	}

	delete [] this->data;
};

CAdjustMatrix &CAdjustMatrix::operator = (const CAdjustMatrix &mat)
{
	if (this != &mat) 
	{
			for (int i = 0; i < this->rows; ++i)
			{
				delete [] this->data[i];
			}
			delete [] this->data;

			this->rows = mat.rows;
			this->cols = mat.cols;
	
			this->data = new double *[this->rows];
			for (int i = 0; i < this->rows; ++i)
			{
				this->data[i] = new double [this->cols];
				for (int j = 0; j < this->cols; ++j) this->data[i][j] = mat.data[i][j];
			}
	}
	return *this;
};

CAdjustMatrix operator *(const CAdjustMatrix &A, const double &value)
{
	CAdjustMatrix valueA(A.rows,A.cols);

	for (int i=0; i < A.rows; i++)
	{
		for (int k=0; k < A.cols; k++)
		{
			valueA.data[i][k]= A.data[i][k]*value;
		}
	}

	return valueA;
}

CAdjustMatrix operator *(const CAdjustMatrix &A, const CAdjustMatrix &B)
{
	CAdjustMatrix AB(A.rows, B.cols);

	for (int i = 0; i < A.rows; ++i)
	{
		for (int j = 0; j < B.cols; ++j)
		{
			AB.data[i][j] = 0;
			for (int i1 = 0; i1 < A.cols; ++i1) 
			{
				AB.data[i][j] += A.data[i][i1] * B.data[i1][j];
			}
		}
	}

	return AB;
}; 


CAdjustMatrix operator *(const CAdjustMatrix &A, const CPointBase &p)
{
	CAdjustMatrix AB(A.rows, 1);

	for (int i = 0; i < A.rows; ++i)
	{
		for (int j = 0; j < 1; ++j)
		{
			AB.data[i][j] = 0;
			for (int i1 = 0; i1 < A.cols; ++i1) 
			{
				AB.data[i][j] += A.data[i][i1] * p[i1];
			}
		}
	}

	return AB;
};


void CAdjustMatrix::transpose()
{
	CAdjustMatrix A(this->cols, this->rows);
	for (int i = 0; i < this->rows; ++i)
	{
		for (int j = 0; j < this->cols; ++j) A.data[j][i] = this->data[i][j];
	}

	*this = A;
};

void CAdjustMatrix::invert()
{
	if (this->rows == 1) this->data[0][0] = 1.0 / this->data[0][0];
	else if (this->rows == 2)
	{
		double det = 1.0 / (this->data[0][0] * this->data[1][1] - this->data[1][0] * this->data[0][1]);
		double d = this->data[0][0] * det;
		this->data[0][0] = this->data[1][1]* det;
		this->data[1][1] = d;
		this->data[1][0] = -this->data[1][0] * det;
		this->data[0][1] = this->data[1][0];
	}
	else 
	{
		Matrix B(this->rows, this->rows);
		for (int i = 0; i < this->rows; ++i)	
			for (int j = 0; j < this->rows; ++j) B.element(i,j) = this->data[i][j];
		B = B.i();
		for (int i = 0; i < this->rows; ++i)	
			for (int j = 0; j < this->rows; ++j) this->data[i][j] = B.element(i,j);
	}
};

void CAdjustMatrix::addToMatrix(int startRow, int startCol, Matrix &dest,bool addTransposed)const
{
	for (int r = 0, R = startRow; r < this->rows; r++, R++)
    {
		for (int c = 0, C = startCol; c < this->cols; c++, C++)
        {
			dest.element(R, C) += this->data[r][c];
			if (addTransposed)
				dest.element(C, R) += this->data[r][c];
        }
    }
}
