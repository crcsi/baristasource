// TextFile.cpp: implementation of the CTextFile class.
//
//////////////////////////////////////////////////////////////////////

#include <assert.h>

#include "CharString.h"
#include "newmat.h"

#include "TextFile.h"

int CTextFile::READ = ios::in;
int CTextFile::WRITE = ios::out;
int CTextFile::APPEND = ios::app;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTextFile::CTextFile( const char* szName, int nMode) :
        fstream(szName, (ios_base::openmode)nMode)
{
    assert(nMode == READ || nMode == WRITE || nMode == APPEND);

    // keep track of how the "archive" was open
    m_nMode = nMode;
}

CTextFile::~CTextFile()
{

}

bool CTextFile::ReadLine(CCharString& Record)
{
    Record.Empty();

    int c;
    for (;;)
    {
        c = get();

        if (c == -1)
            break;

        if (c == '\n')
        {
            if (Record.GetLength() == 0) 
                return true;

            break;
        }

        Record += c;
    }

    if (Record.GetLength() == 0)
        return false;

    return true;
}


bool CTextFile::WriteLine(Matrix& m)
{
    CCharString str;
    CCharString str2;

    for (int r = 1; r <= m.Nrows(); r++)
    {
        for (int c = 1; c <= m.Ncols(); c++)
		{
			str.Format("%.13lf\t", m(r, c));
            str2+=str;
        }
        this->WriteLine(str2);
        str2="";
    }

    return true;
}


bool CTextFile::WriteLine(CCharString& Record)
{
    int l = Record.GetLength();

    write(Record.GetChar(), Record.GetLength());
    write("\n", 1);

    return true;
}

bool CTextFile::WriteLine(const char* pRecord)
{
    CCharString tempString(pRecord);
    return this->WriteLine(tempString);
}