// MUIntegerArray.cpp: implementation of the CMUIntegerArray class.
//
//////////////////////////////////////////////////////////////////////

#include "MUIntegerArray.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMUIntegerArray::CMUIntegerArray(int nGrowBy) :
	CMUObjectArray<int, CMUIntegerPtrArray>(nGrowBy)
{

}

CMUIntegerArray::~CMUIntegerArray()
{

}


//////////////////////////////////////////////////////////////////////
// void CMUIntegerArray::AddOnce(int value)
//
// Adds value only if it doesn't already exists in array
// returns true if value was added
//
bool CMUIntegerArray::AddOnce(int value)
{
	for(int i=0; i<GetSize(); i++)
	{
		if (*GetAt(i)==value)
			return false;
	}

	Add(value);
	return true;
}
