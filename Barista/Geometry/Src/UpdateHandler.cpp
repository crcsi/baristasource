#include "UpdateHandler.h"


void CUpdateHandler::addListener(CUpdateListener* listener)
{
    this->listeners.Add(listener); // add a new listener in the listeners list
}

void CUpdateHandler::removeListener(CUpdateListener* listener)
{
	int size = this->listeners.GetSize();
	if (size == 0 ) return;

    int i = this->listeners.indexOf(listener);

    if (i != -1)
        this->listeners.RemoveAt(i);
}


CUpdateListener *CUpdateHandler::getListener(int index) const 
{ 
	if (index >=0 && index < this->listeners.GetSize())
		return listeners.GetAt(index); 
	else
		return NULL;
}


void CUpdateHandler::informListeners_elementAdded(CLabel element) // this function is called when element is added to the XYpointArray using digitizing icon
{
    for (int i = 0; i < this->listeners.GetSize(); i++) // for each of the current listeners
	{
		CUpdateListener* l = this->listeners.GetAt(i);
		l->elementAdded(element); // inform each listener that a new 'element' is added
	}
}

void CUpdateHandler::informListeners_elementDeleted(CLabel element)
{
    for (int i = this->listeners.GetSize() - 1; i >= 0 && this->listeners.GetSize(); i--)
	{
		CUpdateListener* l = this->listeners.GetAt(i);
		l->elementDeleted(element);
	}
}

void CUpdateHandler::informListeners_elementsChanged(CLabel element)
{
    for (int i = 0; i < this->listeners.GetSize(); i++)
	{
		CUpdateListener* l = this->listeners.GetAt(i);
		l->elementsChanged(element);
	}
}

void CUpdateHandler::informListeners_elementSelected(bool selected,CLabel element,int elementIndex)
{
    for (int i = 0; i < this->listeners.GetSize(); i++)
	{
		CUpdateListener* l = this->listeners.GetAt(i);
		l->elementSelected(selected,element,elementIndex);
	}
}