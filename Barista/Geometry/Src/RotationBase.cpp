#include <math.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <strstream>
using namespace std;

#include "newmat.h"

#include "StructuredFileSection.h"
#include "RotationBase.h"

CRotationMatrix &CRotationMatrix::operator = (const CRotationMatrix & r)
{
	for (int i=0 ; i< 9; i++)
		this->m[i] = r.m[i];

	return *this;
}


void CRotationMatrix::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CCharString valueStr;
	CCharString elementStr;

	CToken* pNewToken = pStructuredFileSection->addToken();

	for (int i = 0; i < 9; i++)
		{
			double element = this->m[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	pNewToken->setValue("matrixElements", valueStr.GetChar());	

}

void CRotationMatrix::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		CCharString key = token->getKey();

		if (key.CompareNoCase("matrixElements"))
		{
			token->getDoubleValues(this->m,9);
		}
	}
}


//================================================================================

CCharString CRotationMatrix::getString(const CCharString &lead, int digits) const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(digits);

	int width = digits + 3;
	oss << lead.GetChar(); oss.width(width); oss << this->m[0] << " "; oss.width(width); oss << this->m[1] << " " ; oss.width(width); oss << this->m[2] << "\n" 
		<< lead.GetChar(); oss.width(width); oss << this->m[3] << " "; oss.width(width); oss << this->m[4] << " " ; oss.width(width); oss << this->m[5] << "\n" 
		<< lead.GetChar(); oss.width(width); oss << this->m[6] << " "; oss.width(width); oss << this->m[7] << " " ; oss.width(width); oss << this->m[8] << ends;

	char *z = oss.str();
	CCharString str(z);
	delete [] z;

	return str;
};

//================================================================================

CRotationBase::CRotationBase(int dim,RotationType type) : CPointBase(dim), sinValues(0), cosValues(0),covariance(0),type(type)
{
	this->sinValues = new double [dim];
	this->cosValues = new double [dim];
	this->initCovar();
}

CRotationBase::CRotationBase(const CRotationBase & rb) : CPointBase(rb), sinValues(0), cosValues(0),covariance(0),type(rb.type)
{
	this->sinValues = new double [dim];
	this->cosValues = new double [dim];

	this->rotMat = rb.rotMat;

	for (int i=0; i < dim; i++)
	{
		this->sinValues[i] = rb.sinValues[i];
		this->cosValues[i] = rb.cosValues[i];
	}

	this->setCovariance(*rb.getCovariance()); 
}

void CRotationBase::initCovar()
{
	if (this->covariance) delete this->covariance;
	this->covariance = new Matrix (this->dim,this->dim);

	*(this->covariance) = 0;
	 for (int i=0; i< this->dim; i++)
		this->covariance->element(i, i) = 1.0;
};


CRotationBase::~CRotationBase(void)
{
	delete [] this->sinValues;
	this->sinValues = 0;

	delete [] this->cosValues;
	this->cosValues = 0;

	delete this->covariance;
	this->covariance = 0;
}



void CRotationBase::computeSinAndCos()
{
	for (int i=0; i < this->dim; i++)
	{
		this->sinValues[i] = sin(this->coordinates[i]);
		this->cosValues[i] = cos(this->coordinates[i]);
	}
}

double CRotationBase::rhoGon()
{ 
	return 200 / M_PI;
}

CRotationBase &CRotationBase::operator = (const CRotationBase & rb)
{
	if (rb.dim != this->dim)
	{
		delete [] this->coordinates;
		this->coordinates = new double [rb.dim];
		
		delete [] this->sinValues;
		delete [] this->cosValues;
		
		this->sinValues = new double [rb.dim];
		this->cosValues = new double [rb.dim];

	}
		
	for (int i=0; i < rb.dim; i++)
	{
		this->coordinates[i] = rb.coordinates[i];	
		this->sinValues[i] = rb.sinValues[i];
		this->cosValues[i] = rb.cosValues[i];
	}
	
	this->rotMat = rb.rotMat;

	this->dim = rb.dim;
	this->id = rb.id;
	this->dot = rb.dot;	
	this->setCovariance(*rb.getCovariance());	
	this->type = rb.type;

	return *this;
}


void CRotationBase::reconnectPointers()
{
	CSerializable::reconnectPointers();
	this->rotMat.reconnectPointers();
}

void CRotationBase::resetPointers()
{
	CSerializable::resetPointers();
	this->rotMat.resetPointers();

}


void CRotationBase::setCovariance (const Matrix &covariance)
{
	if (!this->covariance) this->covariance = new Matrix(covariance);
	else *(this->covariance) = covariance;	
}

void CRotationBase::setCovarElement(int row, int col, double value)
{
	assert(row >= 0 && row < this->dim && col >= 0 && col < this->dim);

	this->bModified = true;

	if (row == col) //set all other correlated values to zero
	{
		for (int i=0; i< this->dim; i++)
		{
			this->covariance->element(row,i) = 0.0;
			this->covariance->element(i,col) = 0.0;
		}
		this->covariance->element(row,col) = value*value;
	}
	else
	{
		this->covariance->element(row,col) = value;
		this->covariance->element(col,row) = value;
	}
}


const double CRotationBase::getCovarElement(int row, int col) const
{
	assert(row >= 0 && row < this->dim && col >= 0 && col < this->dim);

	if (row == col) return sqrt(this->covariance->element(row,col));
	else			return this->covariance->element(row,col);
}

const double CRotationBase::getSigma(int index) const
{
	assert(index >= 0 && index < this->dim);

	return sqrt(this->covariance->element(index,index));
}