#include <strstream>
#include <float.h>
#include "StructuredFileSection.h"
#include "Trans2DProjective.h"
#include "newmatap.h"
#include "AdjustNormEqu.h"

CTrans2DProjective::CTrans2DProjective(): 
	a0(0), a1(1), a2(0), 
	b0(0), b1(0), b2(1), 
	c0(1), c1(0), c2(0), P0(0,0)
{
	this->covar = new SymmetricMatrix(9);
	*(this->covar) = 0.0;
}

CTrans2DProjective::CTrans2DProjective(const CTrans2DProjective &trans):
	a0 (trans.a0), a1 (trans.a1), a2 (trans.a2),
    b0 (trans.b0), b1 (trans.b1), b2 (trans.b2), 
    c0 (trans.c0), c1 (trans.c1), c2 (trans.c2), 
	P0 (trans.P0)
{
	this->covar = new SymmetricMatrix(*trans.covar);
};

CTrans2DProjective::CTrans2DProjective(const Matrix &parms, const C2DPoint &p0):
	a1 (parms.element(0, 0)), a2 (parms.element(1, 0)),
    a0 (parms.element(2, 0)), b1 (parms.element(3, 0)),
    b2 (parms.element(4, 0)), b0 (parms.element(5, 0)), 
	c1 (parms.element(6, 0)), c2 (parms.element(7, 0)),
    c0 (parms.element(8, 0)), P0(p0)
{
	this->covar = new SymmetricMatrix(9);
	*(this->covar) = 0.0;
}

	 
CTrans2DProjective::CTrans2DProjective(const Matrix &parms):
	a1 (parms.element(0, 0)), a2 (parms.element(1, 0)),
    a0 (parms.element(2, 0)), b1 (parms.element(3, 0)),
    b2 (parms.element(4, 0)), b0 (parms.element(5, 0)),
	c1 (parms.element(6, 0)), c2 (parms.element(7, 0)),
    c0 (parms.element(8, 0)), P0(0,0)
{
	this->covar = new SymmetricMatrix(9);
	*(this->covar) = 0.0;
}

CTrans2DProjective::~CTrans2DProjective()
{
}

int CTrans2DProjective::getWeightForDirectObs(Matrix &weights, double sigma0, double maxInfluence, double maxDist) const
{ // Not yet implemented! This is a dummy implementation!
	weights.ReSize(9, 1);
	double weight = sigma0 * sigma0;
	weights.element(0,0) = 0.0;
	weights.element(1,0) = weight;
	weights.element(2,0) = weight;
	weights.element(3,0) = 0.0;
	weights.element(4,0) = weight;
	weights.element(5,0) = weight;
	weights.element(6,0) = 0.0;
	weights.element(7,0) = weight;
	weights.element(8,0) = weight;

	return 6;
}

CCharString CTrans2DProjective::getDescriptor(const CCharString &lead, int shiftDigits, int matrixDigits, 
		                                      const CCharString &descrFrom, const CCharString &descrTo) const
{
	int width = 12;
	int constPrec = 2;
	int matrxPrec = 9;

	ostrstream oss1;
	oss1.setf(ios::fixed, ios::floatfield);
	oss1 << "(x" << descrFrom.GetChar() << " - ";
	oss1.precision(constPrec);
	oss1.width(width);
	oss1 << this->P0.x << ")" << ends;
	char *zstrX = oss1.str();

	ostrstream oss2;
	oss2.setf(ios::fixed, ios::floatfield);
	oss2 << "(y" << descrFrom.GetChar() << " - ";
	oss2.precision(constPrec);
	oss2.width(width);
	oss2 << this->P0.y << ")" << ends;
	char *zstrY = oss2.str();

	ostrstream oss3;
	oss3.setf(ios::fixed, ios::floatfield);
	oss3.precision(constPrec);
	oss3 << "[";
	oss3.width(width);
	oss3 << this->c0 << " + ";
	oss3.precision(matrxPrec);
	oss3.width(width);
	oss3 << this->c1 << " * " << zstrX << " + ";
	oss3.width(width);
	oss3 << this->c2 << " * " << zstrY << "]" << ends;
	char *zstrDenom = oss3.str();

	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);

	oss << lead << "Projective transformation: \n\n" << lead << "     x" << descrTo.GetChar() << " = [";		
	oss.width(width);
	oss.precision(constPrec);
	oss << this->a0 << " + ";
	oss.precision(matrxPrec);
	oss.width(width);
	oss << this->a1 << " * " << zstrX << " + ";
	oss.width(width);
	oss << this->a2 << " * " << zstrY << "] / " << zstrDenom << "\n" << lead << "     y" << descrTo.GetChar() << " = [";
	oss.width(width);
	oss.precision(constPrec);
	oss << this->b0 << " + ";
	oss.precision(matrxPrec);
	oss.width(width);
	oss << this->b1 << " * " << zstrX << " + "; 
	oss.width(width);
	oss << this->b2 << " * " << zstrY << "] / " << zstrDenom << ends;

	char *zstr = oss.str();
	CCharString retVal(zstr);

	delete[] zstr;
	delete[] zstrX;
	delete[] zstrY;
	delete[] zstrDenom;

	return retVal;
}

CTrans2DProjective &CTrans2DProjective::operator = (const CTrans2DProjective &trans)
{
	if (this != &trans)
	{
		this->a0 = trans.a0;
		this->a1 = trans.a1;
		this->a2 = trans.a2;
		this->b0 = trans.b0;
		this->b1 = trans.b1;
		this->b2 = trans.b2;
		this->c0 = trans.c0;
		this->c1 = trans.c1;
		this->c2 = trans.c2;
		this->P0 = trans.P0;
		*this->covar = *trans.covar;
	}

	return *this;
};

void CTrans2DProjective::get(Matrix &parms, C2DPoint &p0) const
{
	p0 = this->P0;
	parms.ReSize(9,1);
	parms.element(0, 0) = this->a1;
	parms.element(1, 0) = this->a2;
	parms.element(2, 0) = this->a0;
	parms.element(3, 0) = this->b1;
	parms.element(4, 0) = this->b2;
	parms.element(5, 0) = this->b0;
	parms.element(6, 0) = this->c1;
	parms.element(7, 0) = this->c2;
	parms.element(8, 0) = this->c0;
};

void CTrans2DProjective::set(const Matrix &parms, const C2DPoint &p0)
{
	this->P0 = p0;
	this->a1 = parms.element(0, 0);
	this->a2 = parms.element(1, 0);
	this->a0 = parms.element(2, 0);
	this->b1 = parms.element(3, 0);
	this->b2 = parms.element(4, 0);
	this->b0 = parms.element(5, 0);
	this->c1 = parms.element(6, 0);
	this->c2 = parms.element(7, 0);
	this->c0 = parms.element(8, 0);
};

void CTrans2DProjective::set(const Matrix &parms)
{
	this->P0.x = this->P0.y = 0.0;
	this->a1 = parms.element(0, 0);
	this->a2 = parms.element(1, 0);
	this->a0 = parms.element(2, 0);
	this->b1 = parms.element(3, 0);
	this->b2 = parms.element(4, 0);
	this->b0 = parms.element(5, 0);
	this->c1 = parms.element(6, 0);
	this->c2 = parms.element(7, 0);
	this->c0 = parms.element(8, 0);
};

void CTrans2DProjective::setP0(const C2DPoint &p0)
{
	this->P0 = p0;
};

void CTrans2DProjective::applyP0()
{
	this->a0 += (- this->a1 * this->P0.x - this->a2 * this->P0.y);
	this->b0 += (- this->b1 * this->P0.x - this->b2 * this->P0.y);
	this->c0 += (- this->c1 * this->P0.x - this->c2 * this->P0.y);

	this->P0.x = 0.0;
	this->P0.y = 0.0;
};

void CTrans2DProjective::transform(CXYZPoint &out, const CXYPoint &in) const 
{
	out.setLabel(in.getLabel());

	double xred = in.x - P0.x;
	double yred = in.y - P0.y;
	double X = this->a0 + this->a1 * xred + this->a2 * yred;
	double Y = this->b0 + this->b1 * xred + this->b2 * yred;
	double Z = this->c0 + this->c1 * xred + this->c2 * yred;
	out.x = X / Z;
	out.y = Y / Z;
	out.z = 0.0;

	double Zsq = Z * Z;

	Matrix a(2,2);
	a.element(0,0) = (this->a1 * Z - X * this->c1) / Zsq;
	a.element(1,0) = (this->a2 * Z - X * this->c2) / Zsq;
	a.element(0,1) = (this->b1 * Z - Y * this->c1) / Zsq;
	a.element(1,1) = (this->b2 * Z - Y * this->c2) / Zsq;
	out.getCovariance()->SubMatrix(1,2,1,2) = a * *(in.getCovariance()) * a.t();
}

//================================================================================

Matrix CTrans2DProjective::getProjectionMatrix() const
{
	Matrix M(3,3);
	M.element(0,0) = this->a1;  M.element(0,1) = this->a2;  M.element(0,2) = this->a0 - this->a1 * this->P0.x - this->a2 * this->P0.y;
	M.element(1,0) = this->b1;  M.element(1,1) = this->b2;  M.element(1,2) = this->b0 - this->b1 * this->P0.x - this->b2 * this->P0.y;
	M.element(2,0) = this->c1;  M.element(2,1) = this->c2;  M.element(2,2) = this->c0 - this->c1 * this->P0.x - this->c2 * this->P0.y;

	return M;
};

void CTrans2DProjective::transform(CXYPoint &out, const CXYPoint &in) const 
{
	out.setLabel(in.getLabel());
	double xred = in.x - P0.x;
	double yred = in.y - P0.y;
	double X = this->a0 + this->a1 * xred + this->a2 * yred;
	double Y = this->b0 + this->b1 * xred + this->b2 * yred;
	double Z = this->c0 + this->c1 * xred + this->c2 * yred;
	out.x = X / Z;
	out.y = Y / Z;

	double Zsq = Z * Z;

	Matrix a(2,2);
	a.element(0,0) = (this->a1 * Z - X * this->c1) / Zsq;
	a.element(1,0) = (this->a2 * Z - X * this->c2) / Zsq;
	a.element(0,1) = (this->b1 * Z - Y * this->c1) / Zsq;
	a.element(1,1) = (this->b2 * Z - Y * this->c2) / Zsq;
	out.setCovariance(a * *(in.getCovariance()) * a.t());
}
    
void CTrans2DProjective::transform(C2DPoint &out, const C2DPoint &in) const 
{
	double xred = in.x - P0.x;
	double yred = in.y - P0.y;
	double z = this->c0 + this->c1 * xred + this->c2 * yred;

	out.x = (this->a0 + this->a1 * xred + this->a2 * yred) / z;
	out.y = (this->b0 + this->b1 * xred + this->b2 * yred) / z;
};

bool CTrans2DProjective::invert()
{
	double x0 = this->a0 - this->a1 * this->P0.x - this->a2 * this->P0.y;
	double y0 = this->b0 - this->b1 * this->P0.x - this->b2 * this->P0.y;
	double z0 = this->c0 - this->c1 * this->P0.x - this->c2 * this->P0.y;

    P0.x = P0.y = 0.0;

	CTrans2DProjective old(*this);

	this->a0 = old.a2 * y0 - old.b2 * x0;
	this->a1 = old.b2 * z0 - old.c2 * y0;
	this->a2 = old.c2 * x0 - old.a2 * z0;

	this->b0 = old.b1 * x0 - old.a1 * y0;
	this->b1 = old.c1 * y0 - old.b1 * z0;
	this->b2 = old.a1 * z0 - old.c1 * x0;

	this->c0 = old.a1 * old.b2 - old.a2 * old.b1;
	this->c1 = old.b1 * old.c2 - old.b2 * old.c1;
	this->c2 = old.a2 * old.c1 - old.a1 * old.c2;

	return true;
};

void CTrans2DProjective::prepareAdjustment(CNormalEquationMatrix &N, CtVector &m, 
										   double &vtpv, unsigned long &nObs) const
{
	N.initialize(9, 0.0);
	m.initialize(9, 0.0);
	vtpv = 0.0;
	nObs = 0;
};

void CTrans2DProjective::accumulate(const C2DPoint &p1, const C2DPoint &p2, 
									CNormalEquationMatrix &N, CtVector &m, 
									double &vtpv, unsigned long &nObs)
{
	C2DPoint p = p1 - P0;
	CAdjustMatrix A(2,9);
	A(0,0) = 0;				A(1,0) = p.x;
	A(0,1) = 0;	 			A(1,1) = p.y;
	A(0,2) = 0; 			A(1,2) = 1.0;
	A(0,3) = -p.x; 			A(1,3) = 0.0;
	A(0,4) = -p.y;			A(1,4) = 0.0;
	A(0,5) = -1.0;      	A(1,5) = 0.0;
	A(0,6) = p2.y * p.x; 	A(1,6) = -p2.x * p.x;
	A(0,7) = p2.y * p.y; 	A(1,7) = -p2.x * p.y;
	A(0,8) = p2.y; 			A(1,8) = -p2.x;

	CAdjustMatrix At = A;
	At.transpose();
	CAdjustMatrix Nsub = At * A;
	
	N.addSubMatrix(0, 0, Nsub);
};

bool CTrans2DProjective::solve(CNormalEquationMatrix &N, const CtVector &m,  
		                       double vtpv, unsigned long nObs, double &s0)
{
	this->covar->ReSize(9);
	*(this->covar) = 0;
	
	Try
	{
		SymmetricMatrix N1(9);
		N1 = 0;

		for (int r = 0; r < N1.Nrows(); ++r)
		{
			for (int c = r; c < N1.Ncols(); ++c)
			{
				N1.element(r,c) = N[N.getIndex(r,c)];
			}
		}

		DiagonalMatrix  eigenVal(9);   // eigen values
		Matrix          eigenVec(9,9); // eigen vectors
  
		SVD(N1, eigenVal, eigenVec);     // compute eigen values and vectors
      

		this->a1 = eigenVec.element(0,8);
		this->a2 = eigenVec.element(1,8);
		this->a0 = eigenVec.element(2,8);
		this->b1 = eigenVec.element(3,8);
		this->b2 = eigenVec.element(4,8);
		this->b0 = eigenVec.element(5,8);
		this->c1 = eigenVec.element(6,8);
		this->c2 = eigenVec.element(7,8);
		this->c0 = eigenVec.element(8,8);
	}
    CatchAll //(char* error) 
    {
        return false;
    }

	return true;
};

void CTrans2DProjective::applyFactorToBoth (double fact, Matrix &covar) 
{ 
	this->P0 *= fact;
	this->a0 *= fact;
	this->b0 *= fact;
	this->c1 /= fact;
	this->c2 /= fact;

	Matrix A(9,9);
	A = 0;
	A.element(0,0) = A.element(3,3) = fact;
	A.element(1,1) = A.element(2,2) = A.element(4,4) = A.element(5,5) = A.element(6,6) = 1.0;
	A.element(7,7) = A.element(8,8) = 1.0 / fact;

	covar = A * covar * A.t();
};
	  
void CTrans2DProjective::applyLinearTransformation(double factLeft, double factRight, 
		                                           C2DPoint &shiftLeft, const C2DPoint &shiftRight, 
											       Matrix &covar)
{ 
	this->P0 = this->P0 * factRight + shiftRight;

	this->a0 = this->a0 * factRight * factLeft + shiftLeft.x * factRight * this->c0;
	this->a1 = this->a1 * factLeft + shiftLeft.x * this->c1;
	this->a2 = this->a2 * factLeft + shiftLeft.x * this->c2;

	this->b0 = this->b0 * factRight * factLeft + shiftLeft.y * factRight * this->c0;
	this->b1 = this->b1 * factLeft + shiftLeft.y * this->c1;
	this->b2 = this->b2 * factLeft + shiftLeft.y * this->c2;

	this->c0 = this->c0 * factRight;
	
    
	Matrix A(9,9);
	A = 0;
	A.element(0,0) = factRight * factLeft; A.element(0,6) = shiftLeft.x * factRight;
	A.element(1,1) = factLeft; A.element(1,7) = shiftLeft.x;
	A.element(2,2) = factLeft; A.element(2,8) = shiftLeft.x;
	A.element(3,3) = factRight * factLeft; A.element(3,6) = shiftLeft.y * factRight;
	A.element(4,4) = factLeft; A.element(4,7) = shiftLeft.y;
	A.element(5,5) = factLeft; A.element(5,8) = shiftLeft.y;
	A.element(6,6) = factRight; 
	A.element(7,7) = 1.0;
	A.element(8,8) = 1.0;
	
	covar = A * covar * A.t();
}


void CTrans2DProjective::changeP0(const C2DPoint &newP0)
{ 
	double dx = newP0.x - P0.x;
	double dy = newP0.y - P0.y;
	this->a0 += this->a1 * dx + this->a2 * dy;
	this->b0 += this->b1 * dx + this->b2 * dy;
	this->c0 += this->c1 * dx + this->c2 * dy;
	this->P0 = newP0;
};

void CTrans2DProjective::getDerivativesCoords(double x, double y, Matrix &B) const
{ 
	B.ReSize(2,2);
	
	x -= P0.x;
	y -= P0.y;
	double X = this->a0 + this->a1 * x + this->a2 * y;
	double Y = this->b0 + this->b1 * x + this->b2 * y;
	double Z = this->c0 + this->c1 * x + this->c2 * y;

	double oneByZ = 1.0 / (Z * Z);
	B.element(0,0) = (this->a1 * Z - this->c1 * X) / oneByZ;
	B.element(1,0) = (this->b1 * Z - this->c1 * X) / oneByZ;
	B.element(0,1) = (this->a2 * Z - this->c2 * Y) / oneByZ;
	B.element(1,1) = (this->b2 * Z - this->c2 * Y) / oneByZ;


};  
void CTrans2DProjective::getDerivatives(double x, double y, Matrix &Ax, Matrix &Ay) const
{ 
	Ax.ReSize(9,1);
	Ay.ReSize(9,1);

	x -= P0.x;
	y -= P0.y;
	double X = this->a0 + this->a1 * x + this->a2 * y;
	double Y = this->b0 + this->b1 * x + this->b2 * y;
	double Z = this->c0 + this->c1 * x + this->c2 * y;

	double oneByZ = 1.0 / Z;
	Ax.element(0,0) = oneByZ;
	Ax.element(1,0) = x * oneByZ;
	Ax.element(2,0) = y * oneByZ;
	Ax.element(3,0) = 0.0;
	Ax.element(4,0) = 0.0;
	Ax.element(5,0) = 0.0;
	Ax.element(6,0) = - X * oneByZ * oneByZ;
	Ax.element(7,0) = - x * X * oneByZ * oneByZ;
	Ax.element(8,0) = - y * X * oneByZ * oneByZ;

	Ay.element(0,0) = 0.0;
	Ay.element(1,0) = 0.0;
	Ay.element(2,0) = 0.0;
	Ay.element(3,0) = oneByZ;
	Ay.element(4,0) = x * oneByZ;
	Ay.element(5,0) = y * oneByZ;
	Ay.element(6,0) = - Y * oneByZ * oneByZ;
	Ay.element(7,0) = - x * Y * oneByZ * oneByZ;
	Ay.element(8,0) = - y * Y * oneByZ * oneByZ;

};

void CTrans2DProjective::addVec(const Matrix &dx, int index0)
{
	this->a0 += dx.element(index0, 0); ++index0;
	this->a1 += dx.element(index0, 0); ++index0;
	this->a2 += dx.element(index0, 0); ++index0;
	this->b0 += dx.element(index0, 0); ++index0;
	this->b1 += dx.element(index0, 0); ++index0; 
	this->b2 += dx.element(index0, 0); ++index0;
	this->c0 += dx.element(index0, 0); ++index0;
	this->c1 += dx.element(index0, 0); ++index0; 
	this->c2 += dx.element(index0, 0);
};
