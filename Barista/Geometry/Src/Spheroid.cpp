#include <string.h>
#include <math.h>
#include "utilities.h"

#include "StructuredFileSection.h"

#include "Spheroid.h"

CSpheroid::CSpheroid():a(0), b(0), e(0), f(0), Eccentricity2(0)
{
	this->name = new char[128];
	this->name[0] = '\0';
}

CSpheroid::CSpheroid(const CSpheroid &spheroid): a (spheroid.a), b (spheroid.b),
												 e (spheroid.e), f (spheroid.f),
												 Eccentricity2 (spheroid.Eccentricity2),
												 name(0)
{
	setName(spheroid.name);
};

CSpheroid::CSpheroid(const char *I_name, double semimajor, double inverseFlattening):
					 name(0)
{
	setName(I_name);

    a = semimajor;
    f = 1 / inverseFlattening;
    calcSemiMinor();
    calcEccentricity();
    calcEccentricity2();
}

CSpheroid::~CSpheroid()
{
	delete [] this->name;
}
	
CSpheroid &CSpheroid::operator = (const CSpheroid &other)
{
	if (this != &other)
	{
		this->setName(other.name);

		this->a = other.a; 
		this->b = other.b; 
		this->e = other.e; 
		this->f = other.f; 
		this->Eccentricity2 = other.Eccentricity2; 
	}
	return *this;
};

bool CSpheroid::operator == (const CSpheroid &other)
{
	if (fabs(this->a - other.a) > 1E-5) return false;
	if (fabs(this->b - other.b) > 1E-5) return false;
	return true;
};

void CSpheroid::setName(const char *I_name)
{
	if (this->name) delete [] this->name;
	int length = strlen(I_name);
	this->name = new char[length + 1];
	strcpy(this->name, I_name);
};

double CSpheroid::calcRCPrimeVertical(double lat) const
{
    double s = sin(lat * D2R);
    return (a / (sqrt((1 - e * e * s * s))));
}


double CSpheroid::calcRCSecondVertical(double lat) const
{
    double s = sin(lat * D2R);
	double e2 = e*e;
    return (a *( 1 -e2) / (pow((1 - e2 * s * s),3.0 / 2.0)));	
}

double CSpheroid::calcRadius(double lat) const
{
	return sqrt(this->calcRCPrimeVertical(lat) * this->calcRCSecondVertical(lat));
}

double CSpheroid::calcP(double lat) const
{
	double s = sin(lat * D2R);
	double e2 = e * e;
    double p = (a * (1 - e2)) / (pow(1 - e2 * s * s, 1.5));
    return p;
}

void CSpheroid::calcEccentricity()
{
    e = sqrt(a * a - b * b) / a;
}

void CSpheroid::calcEccentricity2()
{
    Eccentricity2 = (sqrt(a * a - b * b)) / b;
}

void CSpheroid::calcFlattening()
{
    f = (a - b) / a;
}

void CSpheroid::calcSemiMinor()
{
    b = a * (1 - f);
}

double CSpheroid::calcMeridianDistance(double lat) const
{
	double e2 = this->e * this->e;
	double e4 = e2 * e2;
    double e6 = e4 * e2;

    double A0 = 1 - e2 * 0.25 - (3 * e4 / 64) - 5 * e6 / 256;

    double A2 = (e2 + e4 * 0.25 + 15 * e6 / 128) * 0.375;

    double A4 = 15 * (e4 + e6 * 0.75) / 256;

    double A6 = 35 * e6 / 3072;

    double m = this->a * (A0 * lat - A2 * sin(2 * lat) + A4 * sin(4 * lat) - A6 * sin(6 * lat));

    return m;
}

double CSpheroid::calcFootPointLatitude(double NorthingMetric) const
{
	double n = this->f / (2 - this->f);
	double n2 = n * n;
	double n3 = n2 * n;
	double n4 = n2 * n2;
	double G = this->a * (1 - n) * (1 - n2) * (1 + n2 * 2.25 + 225 * n4 / 64) * D2R;
    double o = NorthingMetric / G * D2R;

	double fpl = o + (1.5 * n - 27 * n3 / 32) * sin(2 * o) + (21 * n2 / 16 - 55 * n4 / 32) * sin(4 * o) +
				 151 * n3 * sin(6 * o) / 96 + (1097 * n4 / 512) * sin(8 * o);

    return fpl;
}

void CSpheroid::serializeStore(CStructuredFileSection* pStructuredFileSection)
{ 
	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("name", this->getName());

    token = pStructuredFileSection->addToken();
    token->setValue("a", this->a);

    token = pStructuredFileSection->addToken();
    token->setValue("f", this->f);
};

void CSpheroid::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{

CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("name"))
		{
			const char *storename = token->getValue();
            this->setName(storename);
		}
        else if (key.CompareNoCase("a"))
            this->a = token->getDoubleValue();
        else if (key.CompareNoCase("f"))
            this->f = token->getDoubleValue();
	}
	 
	calcSemiMinor();
    calcEccentricity();
    calcEccentricity2();
};

void CSpheroid::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

void CSpheroid::resetPointers()
{
    CSerializable::resetPointers();
};

CSpheroid WGS84Spheroid(WGS84_NAME, WGS84_A, WGS84_F);

CSpheroid GRS1980Spheroid("GRS 1980", 6378137, 298.257222101);

CSpheroid GRS1967Spheroid("GRS 1967", 6378160.000, 298.247167427 );

CSpheroid BesselSpheroid("Bessel 1841", 6377397.155, 299.1528153513233 );

CSpheroid AustralianNationalSpheroid("Australian National 1966", 6378160.000, 298.25 );

CSpheroid Clarke1866Spheroid("Clarke 1866", 6378206.400, 294.9786982);

CSpheroid Clarke1880Spheroid("Clarke 1880", 6378249.145, 293.465 );

CSpheroid HayfordSpheroid("Hayford 1910", 6378388.000, 297.00 );

CSpheroid IERSSpheroid("IERS 1989", 6378136.000, 298.257 );


