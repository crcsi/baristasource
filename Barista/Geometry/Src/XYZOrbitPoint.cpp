#include "XYZOrbitPoint.h"
#include "XYZPoint.h"
#include "newmat.h"
#include "AdjustMatrix.h"

CXYZOrbitPoint::~CXYZOrbitPoint(void)
{
}


void CXYZOrbitPoint::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CCharString valueStr;
	CCharString elementStr;
	valueStr = "";

	CToken* token = pStructuredFileSection->addToken();
    token->setValue("hasVelocity", this->hasVelocity);

    token = pStructuredFileSection->addToken();
    token->setValue("label", this->getLabel().GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("status", this->status);

    token = pStructuredFileSection->addToken();
    token->setValue("dot", this->dot);

    token = pStructuredFileSection->addToken();
    token->setValue("id", this->id);


	token = pStructuredFileSection->addToken();
    token->setValue("dim", this->dim);

	token = pStructuredFileSection->addToken();
	for (int i = 0; i < this->dim; i++)
		{
			double element = this->coordinates[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	token->setValue("coordinates", valueStr.GetChar());	

	valueStr.Empty();
	elementStr.Empty();	

	token = pStructuredFileSection->addToken();
	for (int i=0; i< this->covariance->Nrows(); i++)
	{
		for (int k=0; k< this->covariance->Ncols(); k++)
		{
			double element = this->covariance->element(i,k);
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}
	}

	token->setValue("covariance", valueStr.GetChar());	
}

void CXYZOrbitPoint::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);

		CCharString key = token->getKey();

		if(key.CompareNoCase("hasVelocity"))
			this->hasVelocity = token->getBoolValue();
		else if(key.CompareNoCase("label"))
			this->label = CLabel(token->getValue());
        else if (key.CompareNoCase("status"))
            this->status = token->getIntValue();
        else if (key.CompareNoCase("dot"))
            this->dot = token->getDoubleValue();
        else if (key.CompareNoCase("id"))
            this->id = token->getIntValue();
        else if (key.CompareNoCase("dim"))
            this->dim = token->getIntValue();
	    else if (key.CompareNoCase("coordinates"))
		{
			if (this->coordinates)
				delete [] this->coordinates;
			this->coordinates = new double [this->dim];

			token->getDoubleValues(this->coordinates,this->dim);
		}
		else if (key.CompareNoCase("covariance"))
		{
			int size = this->hasVelocity ? this->dim : 3;
			double* tmpArray = new double [size*size];
			token->getDoubleValues(tmpArray,size*size);

			if(this->covariance)
				delete this->covariance;
			
			this->covariance = new Matrix(size,size);
			
			for (int i=0; i< size; i++)
			{
				for (int k=0; k< size; k++)
				{
					this->covariance->element(i,k) = tmpArray[i*size+k];
				}	 
			}

			if (tmpArray) delete [] tmpArray;
		}

	}

}

bool CXYZOrbitPoint::isModified()
{
	return this->bModified;
}

void CXYZOrbitPoint::reconnectPointers()
{
	CSerializable::reconnectPointers();
}

void CXYZOrbitPoint::resetPointers()
{
	CSerializable::resetPointers();
}


void CXYZOrbitPoint::setDate(int day,int month,int year)
{
	
}

void CXYZOrbitPoint::setTime(int hour,int min, double sec)
{
	this->dot = hour*3600 + min*60 + sec;
}


void CXYZOrbitPoint::setOrbitPoint(CXYZPoint location,CXYZPoint velocity,int day,int month,int year,int hour,int min,double sec)
{
	(*this)[0] = location.x;
	(*this)[1] = location.y;
	(*this)[2] = location.z;
	(*this)[3] = velocity.x;
	(*this)[4] = velocity.y;
	(*this)[5] = velocity.z;
	this->dot = location.dot;
	Matrix* covar = this->getCovariance();
	Matrix* loc = location.getCovariance();
	Matrix* vel = velocity.getCovariance();
	for (int i=0; i < 3; i++)
		for (int k=0; k < 3; k++)
		{
			covar->element(i,k) = loc->element(i,k);
			covar->element(i+3,k+3) = vel->element(i,k);
		}
	this->setLabel(location.getLabel());
	this->dot =  hour*3600 + min*60 + sec;
}

int CXYZOrbitPoint::getDimension() const
{
	if (this->hasVelocity)
		return this->dim;
	else
		return 3;
}

void CXYZOrbitPoint::setHasVelocity(bool b)
{
	this->hasVelocity =b;
	
	if (!b && this->dim == 6) // resize the covariance matrix to 3
	{
		Matrix* tmp = new Matrix(3,3);
		for (int i=0; i < 3; i++)
			for (int k=0; k < 3; k++)
				tmp->element(i,k) = this->covariance->element(i,k);
		delete this->covariance;
		this->covariance = tmp;
		this->dim = 3;
	}
	else if (b)  dim=6;

}