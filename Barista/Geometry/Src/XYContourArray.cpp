#include <fstream>

#include "XYContourArray.h"
#include "XYPolyLineArray.h"
#include "2DPoint.h"
#include "Serializable.h"
#include "StructuredFileSectionArray.h"
#include "math.h"
#include "XYZPolyLine.h"

CXYContourArray::CXYContourArray(int nGrowBy) : 
	CMUObjectArray<CXYContour, CXYContourPtrArray>(nGrowBy)
{
	this->FileName = "XYContours";
	isActive=false;
}

//============================================================================
CXYContourArray::CXYContourArray(const CXYContourArray* src) : 
	CMUObjectArray<CXYContour, CXYContourPtrArray>(0)
{
	this->FileName = "XYContours";
	isActive=false;
	this->Copy(*src);
}

//============================================================================
CXYContourArray::CXYContourArray(const CXYContourArray& src) : 
	CMUObjectArray<CXYContour, CXYContourPtrArray>(0)
{
	this->FileName = "XYContours";
	isActive=false;
	this->Copy(src);
}

//============================================================================
CXYContourArray::~CXYContourArray(void)
{
}

//============================================================================
CFileName* CXYContourArray::getFileName()
{
    return &this->FileName;
}

//============================================================================
void CXYContourArray::setFileName(const char* filename)
{
    this->FileName = filename;
}

//============================================================================
CXYContour* CXYContourArray::Add()
{
    CXYContour* newline = CMUObjectArray<CXYContour, CXYContourPtrArray>::Add();

 /*   for (int i = 0; i < this->listeners.GetSize(); i++)
    {
        CXYContourArrayListener* l = this->listeners.GetAt(i);

        l->lineAdded();
    }*/

    return newline;
}

//============================================================================
CXYContour* CXYContourArray::Add(const CXYContour& newElement)
{
    CXYContour* newline = CMUObjectArray<CXYContour, CXYContourPtrArray>::Add(newElement);

/*    for (int i = 0; i < this->listeners.GetSize(); i++)
    {
        CXYContourArrayListener* l = this->listeners.GetAt(i);

        l->lineAdded();
    }*/
	
    return newline;
}

//============================================================================
void CXYContourArray::connectContours(float delta)
{
	for (long i = 0; i < this->GetSize(); ++i)
	{
		CXYContour *contour = this->getLine(i);

		for (long j = i + 1; j < this->GetSize(); ++j)
		{
			CXYContour *checkContour = this->getLine(j);
			C2DPoint first1 = *(contour->getPoint(0));
			C2DPoint last1  = *(contour->getPoint(contour->getPointCount() - 1));
			C2DPoint first2 = *(checkContour->getPoint(0));
			C2DPoint last2  = *(checkContour->getPoint(checkContour->getPointCount() - 1));
			C2DPoint deltaff = first2 - first1;
			C2DPoint deltafl = first2 - last1;
			C2DPoint deltalf = last2  - first1;
			C2DPoint deltall = last2  - last1;
			if ((fabs(deltaff.x) < delta) && (fabs(deltaff.y) < delta)) 
			{
				contour->revert();
				contour->append(checkContour);
				this->RemoveAt(j);
				--j;
			}
			else if ((fabs(deltafl.x) < delta) && (fabs(deltafl.y) < delta)) 
			{
				contour->append(checkContour);
				this->RemoveAt(j);
				--j;
			}
			else if ((fabs(deltalf.x) < delta) && (fabs(deltalf.y) < delta)) 
			{
				contour->revert();
				checkContour->revert();
				contour->append(checkContour);
				this->RemoveAt(j);
				--j;
			}
			else if ((fabs(deltall.x) < delta) && (fabs(deltall.y) < delta))
			{
				checkContour->revert();
				contour->append(checkContour);
				this->RemoveAt(j);
				--j;
			}
		};
	};
};


//============================================================================
/**
 * writes XYZPolyLineArray to a TextFile, with 
 * @param cfilename
 * @returns true/false
 */
bool CXYContourArray::writeXYContourTextFile(CFileName cfilename)
{
    FILE	*fp;
	fp = fopen( cfilename.GetChar(),"w");

	fprintf(fp,"Number of Lines: %d\n", this->GetSize());
	
	// Create lines
	for (int i = 0; i < this->GetSize(); i++)
	{
		CXYContour* line = this->GetAt(i);

		fprintf(fp,"Line: %s\n", line->getLabel().GetChar());
		fprintf(fp,"Number of points: %d\n", line->getPointCount());
		fprintf(fp,"Length: %10.3lf\n", line->getLength());
		int *v=line->getIndexVertices();
		fprintf(fp,"Vertices: %d\n", *v);
		fprintf(fp,"		  %d\n", *(v+1));

		for (int j = 0; j < line->getPointCount(); j++)
		{
			C2DPoint *point = line->getPoint(j);

			double x = point->x;
			double y = point->y;
			
			fprintf(fp,"Point: %d %12.3lf %12.3lf\n", j, x, y);
		}
		
		if ( line->isclosed)
		{
			fprintf(fp,"Type: closed\n\n");
		}
		if ( !line->isclosed)
		{
			fprintf(fp,"Type: open\n\n");
		}
	}

    return true; 
}

//============================================================================
/**
 * writes XYContourArray to a VRML1.0File
 * @param cfilename
 * @returns true/false
 */
bool CXYContourArray::writeXYContourVRMLFile(CFileName cfilename)
{
/*    FILE *fp;
	fp = fopen(cfilename.GetChar(),"w");

	CXYZPoint center;
	double sumx = 0.0;
	double sumy = 0.0;
	double sumz = 0.0;
	int pointcount = 0;

	for (int i = 0; i < this->GetSize(); i++)
	{
		CXYContour* line = this->GetAt(i);

		for (int j = 0; j < line->getPointCount(); j++)
		{
			CXYZPoint* point = line->getPoints()->GetAt(j);

			sumx += point->getX();
			sumy += point->getY();
			pointcount++;
		}
	}
	
 	fprintf(fp,"#VRML V1.0 ascii\n\n");

	center.set( sumx/pointcount, sumy/pointcount, sumz/pointcount);

	fprintf(fp,"#	Coordinate system: %s\n", this->getReferenceSystem()->getCoordinateSystemName().GetChar());
	fprintf(fp,"#	Shift for all coordinate values:\n");
	fprintf(fp,"#		X-Shift: %10.2lf\n", center.getX());
	fprintf(fp,"#		Y-Shift: %10.2lf\n", center.getY());
	
	fprintf(fp,"  Material \n");
	fprintf(fp,"  {\n");
	fprintf(fp,"      diffuseColor 1.0 0.0 0.0\n");
	fprintf(fp,"  }\n");
	fprintf(fp," \n");


	// Create lines
	for (int i = 0; i < this->GetSize(); i++)
	{
		CXYContour* line = this->GetAt(i);

		fprintf(fp,"  Separator\n");
		fprintf(fp,"  {\n");
		fprintf(fp,"      Label\n");
		fprintf(fp,"      {\n");
		fprintf(fp,"          label %s\n", line->getLabel().GetChar());
		fprintf(fp,"      }\n");

		fprintf(fp,"      Separator\n");
		fprintf(fp,"      {\n");
		fprintf(fp,"          Coordinate3\n");
		fprintf(fp,"          {\n");
		fprintf(fp,"              point\n");
		fprintf(fp,"              [\n");


		for (int j = 0; j < line->getPointCount(); j++)
		{
			CXYZPoint point = &line->getPoint(j);

			if (this->getReferenceSystem()->getCoordinateSystem() == WGS84_GEOGRAPHIC)
			{
				CWGS84Spheroid spheroid;
				CTrans3D* trans = NULL;

				trans =  new CGeographicToUTM(&spheroid);
				trans->transform(&point, &point);
				delete trans;
			}

			double x = point.getX() - center.getX();
			double y = point.getY() - center.getY();
			double z = point.getZ() - center.getZ();
			
			fprintf(fp,"                  %12.3lf %12.3lf %12.3lf,\n", x, y, z);

		}
		fprintf(fp,"              ]\n");
		fprintf(fp,"          }\n");

		fprintf(fp,"          IndexedLineSet\n");
		fprintf(fp,"          {\n");
		fprintf(fp,"              coordIndex\n");
		fprintf(fp,"              [ \n");

		for (int j = 0; j < line->getPointCount()-1; j++)
		{
			int p0 = j;
			int p1 = j+1;

			fprintf(fp,"                  %d, %d, -1,\n", p0, p1);
		}
		
		if ( line->isclosed)
		{
			fprintf(fp,"                  %d, 0, -1,\n", line->getPointCount()-1);		
		}


		fprintf(fp,"              ]\n");
		fprintf(fp,"          }\n");
		fprintf(fp,"      }\n");
		fprintf(fp,"  }\n");

		fprintf(fp," \n");
	}
	
	fclose(fp);
*/
    return true;

}

//============================================================================
CXYContour* CXYContourArray::getLine(int i)
{
    return this->GetAt(i);
}

//============================================================================
/** SLOW FOR LOTS OF LINES!
 * Gets a line by label (null if not found)
 *
 * @param CCharString Label of line
 */
CXYContour* CXYContourArray::getLine(CCharString Label)
{
    CXYContour* testline = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        testline = this->GetAt(i);

        if (Label.CompareNoCase(testline->getLabel().GetChar()))
            return testline;

        continue;
    }

    return NULL;
}

//============================================================================
/**
 * Serialized storage of a CXYContourArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYContourArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("xyzpolyline filename", this->FileName.GetChar());


    for (int i = 0; i < this->GetSize(); i++)
    {
        CXYContour* line = this->GetAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        line->serializeStore(newSection);
    }

}

//============================================================================
/**
 * Restores a CXYContourArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYContourArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("xycontour filename"))
            this->FileName = token->getValue();

    }

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();
    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);


        if (child->getName().CompareNoCase("CXYContour"))
        {
            CXYContour* line = this->Add();
            line->serializeRestore(child);
        }
    }

}



//============================================================================
/**
 * reconnects pointers
 */
void CXYContourArray::reconnectPointers()
{
    CSerializable::reconnectPointers();
}


//============================================================================
/**
 * resets pointers
 */

void CXYContourArray::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Flag if CXYContourArray is modified
 * returns bool
 */

//============================================================================

bool CXYContourArray::isModified()
{
    for (int i = 0; i < this->GetSize(); i++)
    {
        if (this->GetAt(i)->isModified())
            return true;
    }

    return false;
}

//============================================================================

CFileName CXYContourArray::getItemText()
{
    CFileName itemtext = this->FileName.GetFullFileName();
    return itemtext.GetChar();
}

//============================================================================
	
void CXYContourArray::ContourArrayToDXF(ofstream &dxfFile)
{
	CXYZPolyLine poly;
	CXYContour *tempLine;
	C2DPoint* tempPointC2D;
	CXYZPoint tempPointCXYZ;
	
	int size1 = this->GetSize();
	int size2 = 0;

	for (int i = 0; i < this->GetSize(); i++)
	{	
		poly.deleteLine();
		tempLine = this->getLine(i);
		size2 = tempLine->getPointCount();
		
		for (int j = 0; j < tempLine->getPointCount(); j++)
		{
			tempPointC2D = tempLine->getPoint(j);
			tempPointCXYZ.x = tempPointC2D->x;
			tempPointCXYZ.y = tempPointC2D->y;
			tempPointCXYZ.z = 0.0;
			poly.addPoint(tempPointCXYZ);
		}
	
		poly.writeDXF(dxfFile, tempLine->getLabel().GetChar(), 1, 1);
	}
};

//============================================================================

void  CXYContourArray::ContourArrayToDXF(const CFileName &fn)
{
	ofstream dxfFile(fn.GetChar());
	if (dxfFile.good())
	{
		if (CXYZPolyLine::writeDXFHeader(dxfFile))
		{
			this->ContourArrayToDXF(dxfFile);
			CXYZPolyLine::writeDXFTrailer(dxfFile);
		}
	}
}