#include "TransGridToGeocentric.h"

CTransGridToGeocentric::CTransGridToGeocentric(const CReferenceSystem &sysFrom, 								
											   const CReferenceSystem &sysTo) : 
									CTrans3D(sysFrom, sysTo)
{
}

CTransGridToGeocentric::~CTransGridToGeocentric()
{
}

void CTransGridToGeocentric::transform(C3DPoint &out, const C3DPoint &in) 
{
    this->systemFrom.gridToGeocentric(out, in);
}

void CTransGridToGeocentric::transform(CXYZPoint &out, const CXYZPoint &in) 
{
    this->systemFrom.gridToGeocentric(out, in);
}
	  
void CTransGridToGeocentric::getDerivatives(Matrix &derivatives, const C3DPoint &p) const
{
	this->systemFrom.derivativesGridToGeocentric(derivatives, p);
};
