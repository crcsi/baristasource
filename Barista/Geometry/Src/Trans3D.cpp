#include "StructuredFileSection.h"
#include "3DPoint.h"
#include "newmat.h"
#include "XYZPointArray.h"

#include "Trans3D.h"
CTrans3D::CTrans3D(const CReferenceSystem &sysFrom, const CReferenceSystem &sysTo): 
		systemFrom(sysFrom), systemTo(sysTo)
{
}

CTrans3D::~CTrans3D(void)
{

}
    
void CTrans3D::transform (CXYZPoint &out, const CXYZPoint &in)  
{
	C3DPoint p;
	C3DPoint inCopy(in);

	transform(p, inCopy);
	out.set(in.getLabel(), p.x, p.y, p.z);
	out.setCovariance(in.getCovar()); 
};


void CTrans3D::getDerivatives(Matrix &derivatives, const C3DPoint &p) const
{
	derivatives.ReSize(3,3);
	derivatives.element(0,0) = 1.0; derivatives.element(0,1) = 0.0; derivatives.element(0,2) = 0.0;
	derivatives.element(1,0) = 0.0; derivatives.element(1,1) = 1.0; derivatives.element(1,2) = 0.0;
	derivatives.element(2,0) = 0.0; derivatives.element(2,1) = 0.0; derivatives.element(2,2) = 1.0;
};
