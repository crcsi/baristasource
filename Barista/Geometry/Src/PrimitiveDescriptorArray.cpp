#include "StructuredFileSectionArray.h"
#include "PrimitiveDescriptor.h"

#include "PrimitiveDescriptorArray.h"

CPrimitiveDescriptorArray::CPrimitiveDescriptorArray(void)
{
}

CPrimitiveDescriptorArray::CPrimitiveDescriptorArray(const CPrimitiveDescriptorArray& src):
	dataSets(src.dataSets)
{

}

CPrimitiveDescriptorArray::~CPrimitiveDescriptorArray(void)
{
}

void CPrimitiveDescriptorArray::clear()
{
	this->dataSets.clear();
};

CPrimitiveDescriptorArray &CPrimitiveDescriptorArray::operator = (const CPrimitiveDescriptorArray & src)
{
	this->dataSets.clear();

	
	for (unsigned int i=0; i < src.dataSets.size(); i++)
	{
		this->dataSets.push_back(src.dataSets[i]);
	}
	return *this;
}

bool CPrimitiveDescriptorArray::addPrimitiveDescriptor(const CPrimitiveDescriptor& descriptor)
{
	// do we have a dataset with the same name already?
	for (unsigned int i=0 ; i < this->dataSets.size(); i++)
		if (descriptor.getPrimitiveType() == this->dataSets[i].getPrimitiveType())
			if (descriptor.getDescriptorName().CompareNoCase(this->dataSets[i].getDescriptorName()))
				return false;

	// the first descriptor needs to be a base descriptor
	if ((this->dataSets.size() == 0 )&& (descriptor.getPrimitiveType() != eBase))
		return false;


	// not found, so we add the new descriptor
	this->dataSets.push_back(descriptor);

	return true;
}

bool CPrimitiveDescriptorArray::removeDescriptor(CCharString descriptorName)
{
	// do we have a dataset with the same name already?
	for (unsigned int i=0 ; i < this->dataSets.size(); i++)
		if (descriptorName.CompareNoCase(this->dataSets[i].getDescriptorName()))
		{
			this->dataSets.erase(this->dataSets.begin() + i);			
			return true;
		}

	return false;
}
	
int CPrimitiveDescriptorArray::getIndexOfDescriptor(int PointId) const
{
	for (unsigned int i = 0; i < this->dataSets.size(); ++i)
	{
		if (this->dataSets[i].containsRoofPoint(PointId)) return i;
	}

	return -1;
};


int CPrimitiveDescriptorArray::getCourtYardCount()const
{
	int count =0;
	for (unsigned int i=0 ; i < this->dataSets.size(); i++)
	{
		if (this->dataSets[i].getPrimitiveType() == eCourtYard)
			count++;
	}

	return count;
}

int CPrimitiveDescriptorArray::getSuperStructureCount() const
{
	int count =0;
	for (unsigned int i=0 ; i < this->dataSets.size(); i++)
	{
		if (this->dataSets[i].getPrimitiveType() == eSuperStructure)
			count++;
	}

	return count;
}


void CPrimitiveDescriptorArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{

	CSerializable::serializeStore(pStructuredFileSection);


	CToken* pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("size", (int)this->dataSets.size());

	CStructuredFileSection* pNewSection; 

	for (unsigned int i=0; i< this->dataSets.size(); i++)
	{
		pNewSection = pStructuredFileSection->addChild();
		this->dataSets[i].serializeStore(pNewSection);
	}
}


void CPrimitiveDescriptorArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	int size= 0;
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if (key.CompareNoCase("size"))
		{
			size =  token->getIntValue();
			
		}
    }

	if (size)
	{

		for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
		{
			CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

			if (pChild->getName().CompareNoCase("CPrimitiveDescriptor"))
			{
				CPrimitiveDescriptor prim(eBase,0,"");
				prim.serializeRestore(pChild);
				this->dataSets.push_back(prim);

			}	
		}
	}
}

void CPrimitiveDescriptorArray::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

void CPrimitiveDescriptorArray::resetPointers()
{
    CSerializable::resetPointers();
}

bool CPrimitiveDescriptorArray::isModified()
{
	return this->bModified;
}