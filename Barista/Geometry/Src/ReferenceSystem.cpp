/**
 * @class CReferenceSystem
 * handles CoordinateSystem, Hemisphere and UTM zone
 * for XYZPointArray, DEM, Affine
 */
#include <float.h>
#include <math.h>
#include "3DPoint.h"
#include "XYZPoint.h"
#include "utilities.h"
#include "newmat.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

#include "ReferenceSystem.h"

CReferenceSystem::CReferenceSystem(int I_zone, int I_hemisphere) : spheroid(WGS84_NAME, WGS84_A, WGS84_F), 
			coordinateType(eUndefinedCoordinate), WGS84Flag(true), hasValues(true),
			latitudeOriginOffset(0.0)
{
	setUTMZone(I_zone, I_hemisphere);
}

CReferenceSystem::CReferenceSystem(double latitude, double longitude) : spheroid(WGS84_NAME, WGS84_A, WGS84_F), 
			coordinateType(eUndefinedCoordinate), WGS84Flag(true), hasValues(false),
			latitudeOriginOffset(0.0)
{
	setUTMZone(latitude, longitude);
}

CReferenceSystem::CReferenceSystem(double I_centralMeridian, double I_scale, 
								   double I_latitudeOrigin, double I_FEasting, double I_FNorthing) : 
			spheroid(WGS84_NAME, WGS84_A, WGS84_F), 
			coordinateType(eUndefinedCoordinate), WGS84Flag(true), 
			centralMeridian(I_centralMeridian), scale(I_scale),
			latitudeOrigin(I_latitudeOrigin), FEasting(I_FEasting), FNorthing(I_FNorthing),
			zone(-1), hemisphere(-1), UTMFlag(false), hasValues(false),
			latitudeOriginOffset(0.0)
{
	this->computeLatitudeOffset();
}

CReferenceSystem::CReferenceSystem(const CSpheroid &I_spheroid, int I_zone, int I_hemisphere) : 
			spheroid(I_spheroid), coordinateType(eUndefinedCoordinate), WGS84Flag(true), hasValues(false),
			latitudeOriginOffset(0.0)
{
	if (strcmp(spheroid.getName(),WGS84_NAME) != 0)WGS84Flag = false;
	setUTMZone(I_zone, I_hemisphere);
}

CReferenceSystem::CReferenceSystem(const CSpheroid &I_spheroid, double latitude, double longitude) : 
			spheroid(I_spheroid), coordinateType(eUndefinedCoordinate), WGS84Flag(true), hasValues(false),
			latitudeOriginOffset(0.0)
{
	if (strcmp(spheroid.getName(),WGS84_NAME) != 0)WGS84Flag = false;
	setUTMZone(latitude, longitude);
}

CReferenceSystem::CReferenceSystem(const CSpheroid &I_spheroid, double I_centralMeridian, double I_scale,
					   double I_latitudeOrigin, double I_FEasting, double I_FNorthing) : 
			spheroid(I_spheroid), coordinateType(eUndefinedCoordinate), WGS84Flag(false), 
			centralMeridian(I_centralMeridian), scale(I_scale),
			latitudeOrigin(I_latitudeOrigin), FEasting(I_FEasting), FNorthing(I_FNorthing),
			zone(-1), hemisphere(-1), UTMFlag(false), hasValues(false),
			latitudeOriginOffset(0.0)
{
	this->computeLatitudeOffset();
}

CReferenceSystem::CReferenceSystem(const CReferenceSystem &refSys) : 
			spheroid(refSys.spheroid), coordinateType(refSys.coordinateType), 
			WGS84Flag(refSys.WGS84Flag), centralMeridian(refSys.centralMeridian), 
			scale(refSys.scale),
			latitudeOrigin(refSys.latitudeOrigin), FEasting(refSys.FEasting),
			FNorthing(refSys.FNorthing), zone(refSys.zone), 
			hemisphere(refSys.hemisphere), UTMFlag(refSys.UTMFlag), hasValues(refSys.hasValues),
			latitudeOriginOffset(refSys.latitudeOriginOffset)
{
}

CReferenceSystem::~CReferenceSystem()
{
}

CReferenceSystem &CReferenceSystem::operator = (const CReferenceSystem &refSys)
{
	if (&refSys != this)
	{
		this->spheroid = refSys.spheroid;
		this->coordinateType = refSys.coordinateType;
		this->WGS84Flag = refSys.WGS84Flag;
		this->centralMeridian = refSys.centralMeridian;
		this->scale = refSys.scale;
		this->latitudeOrigin = refSys.latitudeOrigin;
		this->FEasting = refSys.FEasting;
		this->FNorthing = refSys.FNorthing;
		this->zone = refSys.zone;
		this->hemisphere = refSys.hemisphere;
		this->UTMFlag = refSys.UTMFlag;
		this->hasValues = refSys.hasValues;
		this->latitudeOriginOffset = refSys.latitudeOriginOffset;
	};

	return *this;
};


	
bool CReferenceSystem::operator == (const CReferenceSystem &refSys) const
{
	if (this->getSpheroid()		!= refSys.getSpheroid()) return false;
	if (this->coordinateType	!= refSys.coordinateType) return false;
	if (this->WGS84Flag			!= refSys.WGS84Flag) return false;
	if (this->centralMeridian	!= refSys.centralMeridian) return false;
	if (this->scale				!= refSys.scale) return false;
	if (this->latitudeOrigin	!= refSys.latitudeOrigin) return false;
	if (this->FEasting			!= refSys.FEasting) return false;
	if (this->FNorthing			!= refSys.FNorthing) return false;
	if (this->zone				!= refSys.zone) return false;
	if (this->hemisphere		!= refSys.hemisphere) return false;
	if (this->UTMFlag			!= refSys.UTMFlag) return false;
	//if (this->hasValues			!= refSys.hasValues) return false;

	return true;
};

void CReferenceSystem::setSpheroid(const CSpheroid &I_spheroid)
{
	this->spheroid = I_spheroid;
	this->WGS84Flag = false;
};

void CReferenceSystem::setToWGS1984()
{
	this->spheroid = WGS84Spheroid;
	this->WGS84Flag = true;
};

void CReferenceSystem::setUTMZone(int I_zone, int I_hemisphere)
{
	this->FEasting = 500000;
	this->hemisphere = I_hemisphere;
	if (I_hemisphere == NORTHERN) this->FNorthing = 0.0;
	else this->FNorthing = 10000000.0;

	this->scale = 0.9996;
  
	this->zone = I_zone;
	this->centralMeridian = calcUTMCentralMeridian(this->zone);

	this->latitudeOrigin  = 0.0f;
	this->latitudeOriginOffset = 0.0;
	this->UTMFlag = true;
};


void CReferenceSystem::setUTMZone(double latitude, double longitude)
{
	this->FEasting = 500000;

	if (latitude < 0)
    {
        this->hemisphere = SOUTHERN;
		this->FNorthing = 10000000.0;
    }
    else
    {
        this->hemisphere = NORTHERN;
		this->FNorthing = 0.0;
    }

	this->scale = 0.9996;
  
	this->zone = calcUTMZone(longitude);
	this->centralMeridian = calcUTMCentralMeridian(this->zone);

	this->latitudeOrigin  = 0.0f;
	this->latitudeOriginOffset = 0.0;
	
	this->UTMFlag = true;
};

void CReferenceSystem::setTMParameters(double I_centralMeridian, double I_FEasting, 
									   double I_FNorthing, double I_scale, 
									   double I_latitudeOrigin)
{
	centralMeridian = I_centralMeridian;		
	scale = I_scale;		
	latitudeOrigin = I_latitudeOrigin;
	FEasting = I_FEasting;
	FNorthing = I_FNorthing;
	zone = -1;
	hemisphere = -1;
	UTMFlag = false;
	this->computeLatitudeOffset();
};

void CReferenceSystem::computeLatitudeOffset()
{
	this->latitudeOriginOffset = 0.0;
	C3DPoint in(this->latitudeOrigin, this->centralMeridian, 0.0);
	C3DPoint out;

	this->geographicToGrid(out, in);

	this->latitudeOriginOffset = out.y - FNorthing;
};

CCharString CReferenceSystem::getCoordinateSystemName() const
{
    CCharString name = "";

	if (this->WGS84Flag) name = "WGS 1984";
	else name = this->getSpheroid().getName();

	if ( name.IsEmpty() ) name = "Local";

    if (coordinateType == eGEOGRAPHIC) name += " Geographic";
	else if (coordinateType == eGEOCENTRIC) name += " Geocentric";
	else if (coordinateType == eGRID)
	{
		if (this->UTMFlag) 
		{
			name += " UTM";
			CLabel lzone = "";
			lzone += this->zone;
			
			if ( this->getHemisphere() == 0 ) name += " North ";
			else if ( this->getHemisphere() == 1 ) name += " South ";
			name += " Zone ";
			name += lzone.GetChar();
		}
		else name += " TM";

        name += " Grid";
	}

    return name;
}

int CReferenceSystem::calcUTMZone(double longitude) 
{
    int Zone = 0;

    if (longitude < 0) longitude += 360.0;

    if ((longitude >= 0) && (longitude <= 180)) 
		Zone =(int)(longitude/6+31);
    else 
		Zone = (int)(longitude/6-29);
    
	return Zone;
}

double CReferenceSystem::calcUTMCentralMeridian(int Zone)
{
    return double((6 * Zone) - 183);
}

void CReferenceSystem::setCoordinateType(eCoordinateType I_type)
{
	this->coordinateType = I_type;
};


/**
 * Serialized storage of a CReferenceSystem to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CReferenceSystem::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("coordinateType", this->getCoordinateType());

    token = pStructuredFileSection->addToken();
    token->setValue("zone", this->getZone());

    token = pStructuredFileSection->addToken();
    token->setValue("hemisphere", this->getHemisphere());

    token = pStructuredFileSection->addToken();
	token->setValue("centralmeridian", this->getCentralMeridian());

    token = pStructuredFileSection->addToken();
	token->setValue("latitudeorigin", this->getLatitudeOrigin() );
	
	token = pStructuredFileSection->addToken();
	token->setValue("scale", this->getScale() );
	
	token = pStructuredFileSection->addToken();
	token->setValue("easting", this->FEasting );
	
	token = pStructuredFileSection->addToken();
	token->setValue("northing", this->FNorthing );

	token = pStructuredFileSection->addToken();
	token->setValue("isUTM", this->UTMFlag);

	token = pStructuredFileSection->addToken();
	token->setValue("isWGS1984", this->WGS84Flag );
	
	CStructuredFileSection* newSection = pStructuredFileSection->addChild();
    this->spheroid.serializeStore(newSection);
}

/**
 * Restores a CReferenceSystem from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CReferenceSystem::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("coordinateType"))
		{
			int typ = token->getIntValue();
			eCoordinateType ctyp;
			switch(typ)
			{
				case 0:  ctyp = eGEOGRAPHIC; break;
				case 1:  ctyp = eGEOCENTRIC; break;
				case 2:  ctyp = eGRID; break;
				default: ctyp = eUndefinedCoordinate; break;
			}
			this->setCoordinateType(ctyp);
		}
        else if (key.CompareNoCase("zone"))
            this->zone = token->getIntValue();
        else if (key.CompareNoCase("hemisphere"))
            this->hemisphere = token->getIntValue();
        else if (key.CompareNoCase("centralmeridian"))
			this->centralMeridian = token->getDoubleValue();
        else if (key.CompareNoCase("latitudeorigin"))
			this->latitudeOrigin = token->getDoubleValue();
        else if (key.CompareNoCase("scale"))
			this->scale = token->getDoubleValue();
        else if (key.CompareNoCase("easting"))
			this->FEasting = token->getDoubleValue();
        else if (key.CompareNoCase("northing"))
			this->FNorthing = token->getDoubleValue();
        else if (key.CompareNoCase("isUTM"))
		{
			this->UTMFlag = token->getBoolValue();
		}
        else if (key.CompareNoCase("isWGS1984"))
		{
			this->WGS84Flag = token->getBoolValue();
		}
    }
 
	for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CSpheroid"))
        {
            this->spheroid.serializeRestore(child);
        }
    }

	this->computeLatitudeOffset();
}

/**
 * reconnects pointers
 */
void CReferenceSystem::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CReferenceSystem::resetPointers()
{
    CSerializable::resetPointers();
}

void CReferenceSystem::geographicToGrid(C3DPoint &out, const C3DPoint &in) const
{
	double latitude = in.x;
    double longitude = in.y;
    double height = in.z;

    //conduct preliminary  calculations
   
	double primeVertical = spheroid.calcRCPrimeVertical(latitude);
    
	double psi = primeVertical / spheroid.calcP(latitude);
	double w = (longitude - this->centralMeridian) * D2R;

	latitude *= D2R;
    double t = tan(latitude);
	double s = sin(latitude);
	double c = cos(latitude);
	double c2 = c * c;
	double c3 = c2 * c;
	double c5 = c3 * c2;
	double c7 = c5 * c2;
	double w2 = w * w;
	double w3 = w2 * w;
	double w4 = w3 * w;
	double t2 = t * t;
	double t4 = t2 * t2;
	double t6 = t4 * t2;
	double psi2 = psi * psi;
	double psi3 = psi2 * psi;

    //====================== calculate the Easting =========================

    double Term1 = primeVertical * w * c;

    double Term2 = primeVertical *  w3 * c3 * (psi - t2) / 6;

    double Term3 = primeVertical *  w2 * w3 * c5 * (4 * psi3 * (1 - 6 * t2) +
				   (psi2 * (1 + 8 * t2 )) - 2* psi * t2 + t4) / 120;

    double Term4 = primeVertical * (w4 * w3  / 5040 * c7 * (61 - 479 * t2 + 179 * t4 - t6));

    double E1 = scale * (Term1 + Term2 + Term3 + Term4);

    //====================== calculate the Northing =========================

    Term1 = w2 * 0.5 * primeVertical * s * c;

    Term2 = w4 / 24 * primeVertical * s * c3 * (4 * psi2 + psi - t2);

    Term3 = w4 * w2 / 720 * primeVertical * s * c5 * (8 * psi2 * psi2 * (11 - 24 * t2) - 
			28 * psi3 * (1 - 6 * t2) + psi2 * (1 - 32 * t2) - psi * (2 * t2) + t4);

    Term4 = w4 * w4 / 40320 * primeVertical * s * c7 * (1385 - 3111 * t2 + 543 * t4 - t6);

    double N1 = scale * (spheroid.calcMeridianDistance(latitude) + Term1 + Term2 + Term3 + Term4);

    //======================       Add Conversions     =========================
    out.x = E1 + FEasting;
    out.y = N1 + FNorthing - this->latitudeOriginOffset;

   out.z = height;
};

void CReferenceSystem::geographicToGrid(CXYZPoint &out, const CXYZPoint &in) const
{
	C3DPoint p;
	geographicToGrid(p, (C3DPoint) in);
	Matrix A(3,3);
	A = 0;
	A.element(2,2) = 1;
	double delta = 1e-9;
	double oneBydelta = 1e9;
	C3DPoint dPointLat  = in;
	C3DPoint dPointLong = in;
    dPointLat.x  += delta;
    dPointLong.y += delta;
    geographicToGrid(dPointLat,  dPointLat);
    geographicToGrid(dPointLong, dPointLong);
	A.element(0,0) = (dPointLat.x  - p.x) * oneBydelta;
	A.element(1,0) = (dPointLat.y  - p.y) * oneBydelta;
	A.element(0,1) = (dPointLong.x - p.x) * oneBydelta;
	A.element(1,1) = (dPointLong.y - p.y) * oneBydelta;
	out.set(in.getLabel(), p.x, p.y, p.z);
	out.setCovariance(A * in.getCovar() * A.t());
};
	  
void CReferenceSystem::gridToGeographic(C3DPoint &out, const C3DPoint &in) const
{
	double easting = in.x;
	double northing = in.y - this->FNorthing + this->latitudeOriginOffset;
    double height = in.z;

    //calculate preliminary values

	double fpl = spheroid.calcFootPointLatitude(northing / scale);
	double fplDeg = fpl * R2D;
	double primeVertical = spheroid.calcRCPrimeVertical(fplDeg);
	double p = spheroid.calcP(fplDeg);
    double t = tan(fpl);
	double t2 = t * t;
	double t4 = t2 * t2;
	double t6 = t4 * t2;
    double psi = primeVertical / p;
    double psi2 = psi * psi;
    double psi3 = psi2 * psi;
    double psi4 = psi3 * psi;

    // =====================  calculate the Latitude   =========================


	double E1 = easting - FEasting;
    double x = E1 / (this->scale * primeVertical);
	double x2 = x * x;
	double x3 = x2 * x;
	double x5 = x2 * x3;
	double x7 = x2 * x5;

	double tbyscalep = t / (this->scale * p);

    double Term1 = tbyscalep * (x * E1 * 0.5);

    double Term2 = tbyscalep * (E1 * x3 / 24) * (-4 * psi2 + 9 * psi * (1 - t2) + 12* t2);

    double Term3 = tbyscalep * (E1 * x5 / 720) * (8 * psi4 * (11 -24 * t2) -12 * psi3 * 
				   (21 - 71 * t2) + 15 * psi2 * (15 - 98 * t2 + 15 * t4) +
                    180 * psi * (5 * t2 -3 * t4) + 360 * t6);

    double Term4 = tbyscalep * (E1 * x7 / 40320) * (1385 + 3633 * t2 + 4095 * t4 + 1575 * t6);

	out.x = (fpl - Term1 + Term2 - Term3 + Term4) * R2D; //latitude

    //========================calculate the longitude==========================
    double oneByCosFpl = 1.0f / cos(fpl);
    
	Term1 = x * oneByCosFpl;

    Term2 = (psi + 2 * t2 ) * (x3 / 6) * oneByCosFpl;

	Term3 = (x5 / 120) * oneByCosFpl * (-4 * psi3 * (1 - 6 * t2) + psi2 * (9 - 68 * t2) + 72 * psi * t2 + 24 * t4);

    Term4 = (x7 / 5040)* oneByCosFpl * (61 + 662 * t2 + 1320 * t4 + 720 * t);

    double w = Term1 - Term2 + Term3 - Term4;
   
	out.y = this->centralMeridian + w * R2D; // longitude
    out.z = height;
};
  
void CReferenceSystem::gridToGeographic(CXYZPoint &out, const CXYZPoint &in) const
{
	C3DPoint p;
	gridToGeographic(p, (C3DPoint) in);

	Matrix A(3,3);
	A = 0;
	A.element(2,2) = 1;
	double delta = 1e-5;
	double oneBydelta = 1e5;
	C3DPoint dPointLat  = in;
	C3DPoint dPointLong = in;
    dPointLat.x  += delta;
    dPointLong.y += delta;
    gridToGeographic(dPointLat,  dPointLat);
    gridToGeographic(dPointLong, dPointLong);
	A.element(0,0) = (dPointLat.x  - p.x) * oneBydelta;
	A.element(1,0) = (dPointLat.y  - p.y) * oneBydelta;
	A.element(0,1) = (dPointLong.x - p.x) * oneBydelta;
	A.element(1,1) = (dPointLong.y - p.y) * oneBydelta;

	out.set(in.getLabel(), p.x, p.y, p.z);
	out.setCovariance(A * in.getCovar() * A.t());
};

void CReferenceSystem::geographicToGeocentric(C3DPoint &out, const C3DPoint &in) const
{
	double a   = spheroid.getSemiMajor();
	double e2  = spheroid.getEccentricity()  * spheroid.getEccentricity();

    double phi = in.x * D2R;
    double lambda = in.y * D2R;
    double h = in.z;

	double sinphi = sin(phi);
	double cosphi = cos(phi);
	double sinlambda = sin(lambda);
	double coslambda = cos(lambda);
	double sinphi2 = sinphi * sinphi;

	double e2sinphi2 = 1.0 - e2 * sinphi2;
    double v = a / sqrt(e2sinphi2);

	
    out.x = (v + h) * cosphi * coslambda;
    out.y = (v + h) * cosphi * sinlambda;
    out.z = (v*(1-e2)+h) * sinphi;
};

void CReferenceSystem::geographicToGeocentric(CXYZPoint &out, const CXYZPoint &in) const
{
	double a   = spheroid.getSemiMajor();
	double e2  = spheroid.getEccentricity()  * spheroid.getEccentricity();

    double phi = in.getX() * D2R;
    double lambda = in.getY() * D2R;
    double h = in.getZ();

	double sinphi = sin(phi);
	double cosphi = cos(phi);
	double sinlambda = sin(lambda);
	double coslambda = cos(lambda);
	double sinphi2 = sinphi * sinphi;

	double e2sinphi2 = 1.0 - e2 * sinphi2;
    double v = a / sqrt(e2sinphi2);

	
    out.setX((v + h) * cosphi * coslambda);
    out.setY((v + h) * cosphi * sinlambda);
    out.setZ((v*(1-e2)+h) * sinphi);

	Matrix A;
	this->derivativesGeographicToGeocentric(A, out);

    out.setCovariance(A * in.getCovar() * A.t());
	out.setLabel(in.getLabel());
};

void CReferenceSystem::geocentricToGeographic(C3DPoint &out, const C3DPoint &in) const
{
    double x = in.x;
	double y = in.y;
    double z = in.z;

	double a   = spheroid.getSemiMajor();
	double b   = spheroid.getSemiMinor();
	double ep2 = spheroid.getEccentricity2() * spheroid.getEccentricity2();
	double e2  = spheroid.getEccentricity()  * spheroid.getEccentricity();

    double p = sqrt(x*x +y*y);
    double R = sqrt(x*x + y*y + z*z);
    double Beta = atan2(b*z*(R+ep2*b), a*p*R);

	double sb = sin(Beta);
	double cb = cos(Beta);

    double phi = atan2(z+ep2*b*sb * sb * sb, p-a*e2*cb * cb * cb);
    double lambda = atan2(y, x);
	double sinphi = sin(phi);

    double v = a/sqrt(1.0 - e2 * sinphi * sinphi);

    double h = p*cos(phi) + z* sinphi-a*a/v;

    out.x = phi*R2D;
    out.y = lambda*R2D;
    out.z = h;
};

void CReferenceSystem::geocentricToGeographic(CXYZPoint &out, const CXYZPoint &in) const
{
	double x = in.getX();
    double y = in.getY();
    double z = in.getZ();
	double a   = spheroid.getSemiMajor();
	double b   = spheroid.getSemiMinor();
	double ep2 = spheroid.getEccentricity2() * spheroid.getEccentricity2();
	double e2  = spheroid.getEccentricity()  * spheroid.getEccentricity();

    double p = sqrt(x*x +y*y);
    double R = sqrt(x*x + y*y + z*z);
    double Beta = atan2(b*z*(R+ep2*b), a*p*R);
	double sb = sin(Beta);
	double cb = cos(Beta);

    double phi = atan2(z + ep2 * b * sb * sb * sb, p - a * e2 * cb * cb * cb);
    double lambda = atan2(y, x);

	double sinphi = sin(phi);

	double e2sinphi2 = 1.0f - e2 * sinphi * sinphi;

    double v = a / sqrt(e2sinphi2);

    double h = p * cos(phi) + z * sinphi - a * a / v;

    out.setX(phi*R2D);
    out.setY(lambda*R2D);
    out.setZ(h);
    out.setLabel(in.getLabel());
   
	Matrix A;
	this->derivativesGeographicToGeocentric(A, out);
    
	Matrix B = A.i();

    Matrix covmat = B * in.getCovar() * B.t();
    
    out.setCovariance(covmat);
};

void CReferenceSystem::gridToGeocentric(C3DPoint &out, const C3DPoint &in) const
{
	gridToGeographic(out,in);
	geographicToGeocentric(out, out);
};

void CReferenceSystem::gridToGeocentric(CXYZPoint &out, const CXYZPoint &in) const
{
	gridToGeographic(out,in);
	geographicToGeocentric(out, out);
};

void CReferenceSystem::geocentricToGrid(C3DPoint &out, const C3DPoint &in) const
{
	geocentricToGeographic(out, in);
	geographicToGrid(out, out);
};

void CReferenceSystem::geocentricToGrid(CXYZPoint &out, const CXYZPoint &in) const
{
	geocentricToGeographic(out, in);
	geographicToGrid(out, out);
};

void CReferenceSystem::derivativesGeocentricToGeographic(Matrix &derivatives, const C3DPoint &geocentric) const
{
	C3DPoint geographic;
	this->geocentricToGeographic(geographic, geocentric);
    Matrix A;
    this->derivativesGeographicToGeocentric(A, geographic);
	derivatives = A.i();
};

void CReferenceSystem::derivativesGeographicToGeocentric(Matrix &derivatives, const C3DPoint &geographic) const
{
	double phi    = geographic.x * D2R;
    double lambda = geographic.y * D2R;
	double h      = geographic.z;

	double a   = spheroid.getSemiMajor();
	double e2  = spheroid.getEccentricity() * spheroid.getEccentricity();
	double sinphi = sin(phi);
	double cosphi = cos(phi);
	double sinphi2 = sinphi * sinphi;
	double cosphi2 = cosphi * cosphi;
	double sinlambda = sin(lambda);
	double coslambda = cos(lambda);

	double e2sinphi2 = 1.0f - e2 * sinphi2;

    double v = a / sqrt(e2sinphi2);

	double denomInv = 1.0f / (pow(e2sinphi2, 1.5));

    double p_x_phi = -(v+h) * sinphi * coslambda +
                      a * e2 * sinphi * cosphi2 * coslambda * denomInv;

    double p_x_lambda = -(v+h) * cosphi * sinlambda;

    double p_x_h = cosphi * coslambda;

    double p_y_phi = -(v+h) * sinphi * sinlambda +
                        a * e2 * sinphi * cosphi2 * sinlambda * denomInv;

    double p_y_lambda = (v+h) * cosphi * coslambda;

    double p_y_h = cosphi * sinlambda;

    double p_z_phi = (v * (1 - e2) + h) * cosphi +  a * e2 * (1 - e2) * sinphi2 * cosphi * denomInv;

    double p_z_lambda = 0;

    double p_z_h = sinphi;	

	derivatives.ReSize(3, 3);

    derivatives.element(0, 0) = p_x_phi    * D2R;
    derivatives.element(0, 1) = p_x_lambda * D2R;
    derivatives.element(0, 2) = p_x_h;

    derivatives.element(1, 0) = p_y_phi    * D2R;
    derivatives.element(1, 1) = p_y_lambda * D2R;
    derivatives.element(1, 2) = p_y_h;

    derivatives.element(2, 0) = p_z_phi    * D2R;
    derivatives.element(2, 1) = p_z_lambda * D2R;
    derivatives.element(2, 2) = p_z_h;
};

void CReferenceSystem::derivativesGeocentricToGrid(Matrix &derivatives, const C3DPoint &geocentric) const
{
	Matrix geocentricToGeographicMatrix;
	Matrix GeographicToGridMatrix;
	C3DPoint geographic;

	this->derivativesGeocentricToGeographic(geocentricToGeographicMatrix, geocentric);
	this->geocentricToGeographic(geographic, geocentric);
	this->derivativesGeographicToGrid(GeographicToGridMatrix, geographic);
	derivatives = GeographicToGridMatrix * geocentricToGeographicMatrix;
};

void CReferenceSystem::derivativesGridToGeocentric(Matrix &derivatives, const C3DPoint &grid) const
{
	Matrix GeographicToGeocentricMatrix;
	Matrix GridToGeographicMatrix;
	C3DPoint geographic;
	this->derivativesGridToGeographic(GridToGeographicMatrix, grid);
	this->gridToGeographic(geographic, grid);
	this->derivativesGeographicToGeocentric(GeographicToGeocentricMatrix, geographic);

	derivatives = GeographicToGeocentricMatrix * GridToGeographicMatrix;
};

void CReferenceSystem::derivativesGeographicToGrid(Matrix &derivatives, const C3DPoint &geographic) const
{
	double delta = 1e-9;
	double oneBydelta = 1e9;

	C3DPoint grid;
	C3DPoint dPointX(geographic.x + delta, geographic.y,         geographic.z);
	C3DPoint dPointY(geographic.x,         geographic.y + delta, geographic.z);

	this->geographicToGrid(grid, geographic);
    this->geographicToGrid(dPointX, dPointX);
    this->geographicToGrid(dPointY, dPointY);

	derivatives.ReSize(3,3);

	derivatives.element(0,0) = (dPointX.x  - grid.x) * oneBydelta; derivatives.element(0,1) = (dPointY.x - grid.x) * oneBydelta; derivatives.element(0,2) = 0.0;
	derivatives.element(1,0) = (dPointX.y  - grid.y) * oneBydelta; derivatives.element(1,1) = (dPointY.y - grid.y) * oneBydelta; derivatives.element(1,2) = 0.0;
	derivatives.element(2,0) = 0.0;                                derivatives.element(2,1) = 0.0;                               derivatives.element(2,2) = 1.0;
};

void CReferenceSystem::derivativesGridToGeographic(Matrix &derivatives, const C3DPoint &grid) const
{
	double delta = 1e-5;
	double oneBydelta = 1e5;

	C3DPoint geographic;
	C3DPoint dPointX(grid.x + delta, grid.y,         grid.z);
	C3DPoint dPointY(grid.x,         grid.y + delta, grid.z);

	this->gridToGeographic(geographic, (C3DPoint) grid);
    this->gridToGeographic(dPointX, dPointX);
    this->gridToGeographic(dPointY, dPointY);

	derivatives.ReSize(3,3);

 	derivatives.element(0,0) = (dPointX.x  - geographic.x) * oneBydelta; derivatives.element(0,1) = (dPointY.x - geographic.x) * oneBydelta; derivatives.element(0,2) = 0.0;
	derivatives.element(1,0) = (dPointX.y  - geographic.y) * oneBydelta; derivatives.element(1,1) = (dPointY.y - geographic.y) * oneBydelta; derivatives.element(1,2) = 0.0;
	derivatives.element(2,0) = 0.0;                                      derivatives.element(2,1) = 0.0;                                     derivatives.element(2,2) = 1.0;
};

