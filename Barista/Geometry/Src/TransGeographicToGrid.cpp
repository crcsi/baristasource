#include "TransGeographicToGrid.h"

#include <stdio.h>
#include <math.h>
#include "utilities.h"

CTransGeographicToGrid::CTransGeographicToGrid(const CReferenceSystem &sysFrom, 
											   const CReferenceSystem &sysTo) : 
				CTrans3D(sysFrom, sysTo)
{
}

CTransGeographicToGrid::~CTransGeographicToGrid()
{
}

void CTransGeographicToGrid::transform(C3DPoint &out, const C3DPoint &in) 
{
	this->systemTo.geographicToGrid(out, in);
}

void CTransGeographicToGrid::transform(CXYZPoint &out, const CXYZPoint &in) 
{
	this->systemTo.geographicToGrid(out, in);
}

void CTransGeographicToGrid::getDerivatives(Matrix &derivatives, const C3DPoint &p) const
{
	this->systemTo.derivativesGeographicToGrid(derivatives, p);
};
