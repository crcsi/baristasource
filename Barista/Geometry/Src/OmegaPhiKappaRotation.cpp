
#include "newmat.h"
#define _USE_MATH_DEFINES
#include <cmath>

#include "StructuredFileSection.h"
#include "AdjustMatrix.h"
#include "ObsPoint.h"

#include "OmegaPhiKappaRotation.h"

COmegaPhiKappaRotation::COmegaPhiKappaRotation(const CPointBase* p_rho): CRotationBase(3,OPK)
{
	this->updateAngles_Rho(p_rho);
}

COmegaPhiKappaRotation::COmegaPhiKappaRotation(const CRotationMatrix &mat): CRotationBase(3,OPK)
{
	this->updateRotMat(mat);
}

void COmegaPhiKappaRotation::updateAngles_Rho(const CPointBase* angles)
{
	assert( angles->getDimension() == this->dim);
	
	for (int i=0; i < this->dim; i++)
		this->coordinates[i] = (*angles)[i];

	this->computeRotMatFromParameter();
}

void COmegaPhiKappaRotation::updateAngles_Gon(const CPointBase* angles)
{
	assert( angles->getDimension() == this->dim);
	
	for (int i=0; i < this->dim; i++)
		this->coordinates[i] = (*angles)[i] / this->rhoGon() ;


	this->computeRotMatFromParameter();
}

void COmegaPhiKappaRotation::updateRotMat(const CRotationMatrix &mat)
{

	this->rotMat = mat;

	this->computeParameterFromRotMat();
	this->computeSinAndCos();
}


void COmegaPhiKappaRotation::computeRotMatFromParameter()
{
	this->computeSinAndCos();
	this->rotMat(0,0) = this->cosValues[1] * this->cosValues[2];
	this->rotMat(1,0) = this->cosValues[0] * this->sinValues[2] + this->sinValues[0] * this->sinValues[1] * this->cosValues[2];
	this->rotMat(2,0) = this->sinValues[0] * this->sinValues[2] - this->cosValues[0] * this->sinValues[1] * this->cosValues[2];
	this->rotMat(0,1) = - this->cosValues[1] * this->sinValues[2];
	this->rotMat(1,1) = this->cosValues[0] * this->cosValues[2] - this->sinValues[0] * this->sinValues[1] * this->sinValues[2];
	this->rotMat(2,1) = this->sinValues[0] * this->cosValues[2] + this->cosValues[0] * this->sinValues[1] * this->sinValues[2];
	this->rotMat(0,2) = this->sinValues[1];
	this->rotMat(1,2) = -this->sinValues[0] * this->cosValues[1];
	this->rotMat(2,2) = this->cosValues[0] * this->cosValues[1];
 };


bool COmegaPhiKappaRotation::computeParameterFromRotMat()
{
	// omega
	this->coordinates[0] = atan2(- this->rotMat(5), this->rotMat(8));
	if (this->coordinates[0] < 0 && fabs(this->coordinates[0] + M_PI) < 0.1)
		this->coordinates[0] += 2*M_PI;

	// Phi
	this->coordinates[1] = asin( this->rotMat(2));
	if ( this->coordinates[1] < 0 && fabs(this->coordinates[1] + M_PI) < 0.1)
		this->coordinates[1] += 2* M_PI;

	// kappa
	this->coordinates[2] = atan2( -this->rotMat(1), this->rotMat(0) );
	if (this->coordinates[2] < 0 && fabs(this->coordinates[2] + M_PI) < 0.1)
		this->coordinates[2] += 2*M_PI;

	return true;
}

void COmegaPhiKappaRotation::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CCharString valueStr;
	CCharString elementStr;

	CToken* pNewToken = pStructuredFileSection->addToken();

	for (int i = 0; i < 3; i++)
		{
			double element = this->coordinates[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	pNewToken->setValue("rotationAngles", valueStr.GetChar());	

	valueStr.Empty();
	elementStr.Empty();	

	pNewToken = pStructuredFileSection->addToken();
	for (int i=0; i< this->dim; i++)
	{
		for (int k=0; k< this->dim; k++)
		{
			double element = this->covariance->element(i,k);
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}
	}

	pNewToken->setValue("covariance", valueStr.GetChar());	


}

void COmegaPhiKappaRotation::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	double tempAngles[3];

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		CCharString key = token->getKey();

		if (key.CompareNoCase("rotationAngles"))
		{
			token->getDoubleValues(tempAngles,3);
		}
		else if (key.CompareNoCase("covariance"))
		{
			double* tmpArray = new double [this->dim*this->dim];
			token->getDoubleValues(tmpArray,this->dim*this->dim);

			if(this->covariance)
				delete this->covariance;
			
			this->covariance = new Matrix(this->dim,this->dim);
			
			for (int i=0; i< this->dim; i++)
			{
				for (int k=0; k< this->dim; k++)
				{
					this->covariance->element(i,k) = tmpArray[i*this->dim+k];
				}	 
			}

			if (tmpArray) delete [] tmpArray;
		}

	}
	
	// use a temp Point and then updateAngles_Rho() to have only one interface to init the class
	C3DPoint tempPoint(tempAngles[0],tempAngles[1],tempAngles[2]);
	this->updateAngles_Rho(&tempPoint);
	
}

void COmegaPhiKappaRotation::computeDerivation(CPointBase& deriv,const int parameter)const
{
	assert(deriv.getDimension() == 9 && parameter >=0 && parameter <= this->dim);

	if (parameter == 0)	// omega
	{
		deriv[0] = 0.0;	// r00
		deriv[1] = 0.0; // r01
		deriv[2] = 0.0; // r02
		
		deriv[3] = -this->rotMat(6); // r10
		deriv[4] = -this->rotMat(7); // r11
		deriv[5] = -this->rotMat(8); // r12

		deriv[6] = this->rotMat(3); // r20
		deriv[7] = this->rotMat(4); // r21
		deriv[8] = this->rotMat(5); // r22

	}
	else if (parameter == 1)  // Phi
	{
		deriv[0] = -this->sinValues[1] * this->cosValues[2]; // r00
		deriv[1] = this->sinValues[1] * this->sinValues[2]; // r01
		deriv[2] = this->cosValues[1]; // r02
		
		deriv[3] = this->sinValues[0] * this->cosValues[1] * this->cosValues[2]; // r10
		deriv[4] = -this->sinValues[0] * this->cosValues[1] * this->sinValues[2]; // r11
		deriv[5] = this->sinValues[0] * this->sinValues[1]; // r12

		deriv[6] = -this->cosValues[0] * this->cosValues[1] * this->cosValues[2]; // r20
		deriv[7] = this->cosValues[0] * this->cosValues[1] * this->sinValues[2]; // r21
		deriv[8] = -this->cosValues[0] * this->sinValues[1]; // r22
	}
	else // kappa
	{
		deriv[0] = this->rotMat(1); // r00
		deriv[1] = -this->rotMat(0); // r01
		deriv[2] = 0; // r11

		deriv[3] = this->rotMat(4); // r10
		deriv[4] = -this->rotMat(3); // r11
		deriv[5] = 0; // r12

		deriv[6] = this->rotMat(7); // r20
		deriv[7] = -this->rotMat(6); // r21
		deriv[8] = 0.0; // r22
	}

}

void COmegaPhiKappaRotation::computeDerivation(CAdjustMatrix& derivMat) const
{
	assert(derivMat.getRows() == 9 && derivMat.getCols() == 3);

	CObsPoint dOmega(9),dPhi(9),dKappa(9);
	
	this->computeDerivation(dOmega,0);
	this->computeDerivation(dPhi,1);
	this->computeDerivation(dKappa,2);

	for (int i=0; i < 9; i++)
	{
		derivMat(i,0) = dOmega[i];
		derivMat(i,1) = dPhi[i];
		derivMat(i,2) = dKappa[i];
	}
	
}
