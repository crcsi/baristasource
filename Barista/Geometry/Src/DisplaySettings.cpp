#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

#include "DisplaySettings.h"

CDisplaySettings::CDisplaySettings(void) : showEntities3D(false), showLabels3D(false), red(1.0), blue(0.0), green(0.0)
{
}

CDisplaySettings::~CDisplaySettings(void)
{
}

void CDisplaySettings::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("showEntities3D", this->showEntities3D);
    token = pStructuredFileSection->addToken();
	token->setValue("showLabels3D", this->showLabels3D);
    token = pStructuredFileSection->addToken();
	token->setValue("red", this->red);
    token = pStructuredFileSection->addToken();
	token->setValue("blue", this->blue);
    token = pStructuredFileSection->addToken();
	token->setValue("green", this->green);
};

void CDisplaySettings::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);

		CCharString key = token->getKey();

		if(key.CompareNoCase("showEntities3D"))
			this->showEntities3D = token->getBoolValue();
		else if(key.CompareNoCase("showLabels3D"))
            this->showLabels3D = token->getBoolValue();
		else if(key.CompareNoCase("red"))
			this->red = token->getDoubleValue();
		else if(key.CompareNoCase("blue"))
			this->blue = token->getDoubleValue();
		else if(key.CompareNoCase("green"))
			this->green = token->getDoubleValue();

	}


};

bool CDisplaySettings::isModified()
{
	return true;
};

void CDisplaySettings::reconnectPointers()
{

};


void CDisplaySettings::resetPointers()
{

};
