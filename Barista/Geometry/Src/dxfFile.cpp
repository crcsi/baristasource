#include <strstream> 

#include "dxfFile.h"
//============================================================================

CDXFReader::CDXFReader(const CLabel &lab, const CFileName &filename):
                       dxfFileName(filename), pDxfFile(0), firstLabel(lab)
{  
	if (!this->dxfFileName.IsEmpty()) 
	{
		this->pDxfFile = new ifstream((const char *) dxfFileName, ios::binary);
		if (!this->pDxfFile->is_open()) this->closeFile();
	}
};
 
//============================================================================

CDXFReader::~CDXFReader()
{
	this->clear();
};

//============================================================================

bool CDXFReader::isOpen() const
{
	if (!this->pDxfFile) return false;
	return this->pDxfFile->is_open();
};

//============================================================================

void CDXFReader::clear()
{   
	this->closeFile();
};

//============================================================================

void CDXFReader::closeFile()
{   
	if (this->pDxfFile)
	{
		this->pDxfFile->close();
		delete this->pDxfFile;
		this->pDxfFile = 0;
	};
};

bool CDXFReader::setFileName(const CFileName &filename)
{
	this->closeFile();
	return this->open();
};


//============================================================================
 
bool CDXFReader::open()
{
	bool ret = true;
	if (!this->dxfFileName.IsEmpty()) 
		this->pDxfFile = new ifstream((const char *) dxfFileName, ios::binary);
	
	if (!this->pDxfFile) ret = false;
	else  if (!this->pDxfFile->is_open())
	{
		this->closeFile();
		ret = false;
	};
 
	return ret;
};

char *CDXFReader::getLine(ifstream &IStream)
{
	int ct = 1024;
	char *zLine = new char[ct + 1];
	char a;
	long j = 0;
	IStream.read(&a,1);

	if (IStream.fail()) 
	{
		delete[] zLine;
		return 0;
	};

	while ((!IStream.fail()) && (a != 10) && (j < ct))
	{
		if (a != 13)   
		{
			zLine[j] = a;
			++j;
		};

		IStream.read(&a,1);
	};

	zLine[j] = '\0';

	return zLine;
};

bool CDXFReader::readNextCodeAndAttribute(int &code, CCharString &attribute)
{
	bool ret = true;
	int nCount = 1024;

	char *z = this->getLine(*this->pDxfFile);

	if (!z) ret = false;
	else
	{
		istrstream iss(z);
		iss >> code;
		delete[] z;

		z = getLine(*this->pDxfFile);

		if (!z) ret = false;
		else
		{
			attribute = z;
			delete[] z;
		}
	}

	return ret;
};
 
//============================================================================

bool CDXFReader::readDXF(unsigned long &nVerts, unsigned long &nLines, 
						 unsigned long &nPolys, unsigned long &n3dFaces, 
						 bool  addFacesFlag)
{
	bool ret = true;
	this->vertices.RemoveAll();
	this->polylines.RemoveAll();

	if (!this->isOpen()) ret = this->open();

	if (ret)
	{
		int code;
		CCharString attribute;

		int nCodes = 0;

		nVerts = nLines = nPolys = n3dFaces = 0;

		while (ret &= this->readNextCodeAndAttribute(code, attribute))
		{
			++nCodes;
			if (code == 0)
			{
				if ((attribute == "VERTEX") || (attribute == "CIRCLE"))
				{
					C3DPoint v;
					ret = this->readVertex(v);
					if (ret)
					{
						ret = this->addVertex(v);
						if (ret) ++nVerts;
					} 
				} 				
				else if (attribute == "LINE")
				{				
					C3DPoint v1, v2;
					int color;
					if (this->readLine(v1, v2, color))
					{
						ret = this->addLine(v1, v2, color);		
						if (ret) ++nLines;
					};
				}				
				else if ((attribute == "POLYLINE") || (attribute == "LWPOLYLINE"))
				{				
					CXYZPolyLine poly;
					int color;

					if (this->readPolyLine(poly, color))
					{
						ret = this->addPolyline(poly, color);
						if (ret)
						{
							if (addFacesFlag) this->addFace(poly);
							++nPolys;
						}
					}	
				}	
				else if (attribute == "3DFACE")
				{
					CXYZPolyLine poly;
					int color;

					if (this->read3DFace(poly, color))
					{
						ret = this->addPolyline(poly, color);
						if (ret)
						{
							if (addFacesFlag) this->addFace(poly);
							++n3dFaces;
						}
					};
				};
			};

		};

		this->closeFile();
	}

	return ret;
};
  
//============================================================================

bool CDXFReader::readVertex(C3DPoint &v)
{
	int code = 1;
	CCharString attribute;
	bool retVal = true;
	long pos = this->pDxfFile->tellg();

	while ((code != 0) && this->readNextCodeAndAttribute(code, attribute))
	{
		if ((code < 19) && (code > 9))       retVal &= this->getDoubleFromString(attribute, v.x);
		else if ((code < 29) && (code > 19)) retVal &= this->getDoubleFromString(attribute, v.y);
		else if ((code < 39) && (code > 29)) retVal &= this->getDoubleFromString(attribute, v.z);

		if (code == 0) this->pDxfFile->seekg(pos);
		else pos = this->pDxfFile->tellg();
	};
	
	return retVal;
};
  
//============================================================================

bool CDXFReader::readLine(C3DPoint &v1, C3DPoint &v2, int &color)
{
	bool retVal = true;
	v1.x = v1.y = v1.z = 0.0f;
	v2.x = v2.y = v2.z = 0.0f;
	color = 1;
	CCharString inputLine;
	int code = 1;
	long pos = this->pDxfFile->tellg();

	bool useNormal = false;

	C3DPoint axisX, axisY, axisZ;

	while ((code != 0) && this->readNextCodeAndAttribute(code, inputLine))
	{
		switch (code)
		{	
			case 62: 
			{
				double col = 1.0;
				if (this->getDoubleFromString(inputLine, col))
				color = (int) col;
			}
			break;

			case 10: 
			{
				retVal = this->getDoubleFromString(inputLine, v1.x);
			}
			break;

			case 20: 
			{
				retVal = this->getDoubleFromString(inputLine, v1.y);
			}
			break;


			case 30: 
			{
				retVal = this->getDoubleFromString(inputLine, v1.z);
			}
			break;

			case 11: 
			{
				retVal = this->getDoubleFromString(inputLine, v2.x);
			}
			break;

			case 21: 
			{
				retVal = this->getDoubleFromString(inputLine, v2.y);
			}
			break;


			case 31: 
			{
				retVal = this->getDoubleFromString(inputLine, v2.z);
			}
			break;

			case 210: 
			{
				useNormal = true;
				retVal = this->getDoubleFromString(inputLine, axisZ.x);
			};
			break;

			case 220: 
			{
				useNormal = true;
				retVal = this->getDoubleFromString(inputLine, axisZ.y);
			};
			break;

			case 230: 
			{
				useNormal = true;
				retVal = this->getDoubleFromString(inputLine, axisZ.z);
			};
			break;

			default: break;
		};

		if (code == 0) this->pDxfFile->seekg(pos);
		else pos = this->pDxfFile->tellg();
	};

	if (useNormal)
	{
		this->auxExtrusionTrafo(axisX, axisY, axisZ);

		v1 = this->auxExtrusionTrafo(axisX, axisY, axisZ, v1);
		v2 = this->auxExtrusionTrafo(axisX, axisY, axisZ, v2);
	};

	return retVal;
};
   
//============================================================================

bool CDXFReader::readPolyLine(CXYZPolyLine &poly, int &color)
{
	bool retVal = true;

	int code = 1;
	
	CCharString inpLine;
	color = 1;
	C3DPoint axisX, axisY, axisZ;
	double offsetZ = 0.0;

	bool isClosed = false; 
	bool useNormal = false;
	CXYZPoint p;

	while (!((inpLine== "SEQEND") && (code == 0)) && this->readNextCodeAndAttribute(code, inpLine))
	{
		switch (code)
		{
			case 62: 
			{
				double col = 1.0;
				if (this->getDoubleFromString(inpLine, col)) color = (int) col;
			}
			break;

			case 70: 
			{
				long help = atol( inpLine );
				isClosed = (help % 2 != 0);
				if ( help > 8 )
				{
//					cerr << "Error in DXF import: cannot handle this type of polyline" << endl;
				};
			};
			break;

			case 10: break;

			case 20: break;

			case 30: 
			{
				retVal = this->getDoubleFromString(inpLine, offsetZ);
			}
			break;

			case 210: 
			{
				useNormal = true;
				retVal = this->getDoubleFromString(inpLine, axisZ.x);
			};
			break;

			case 220: 
			{
				useNormal = true;
				retVal = this->getDoubleFromString(inpLine, axisZ.y);
			};
			break;

			case 230: 
			{
				useNormal = true;
				retVal = this->getDoubleFromString(inpLine, axisZ.z);
			};
			break;

			case 0: 
			{
				if (inpLine == "VERTEX")
				{
					retVal = this->readVertex(p);
					if (retVal) 
					{
						p.setSX(1.0);
						p.setSY(1.0);
						p.setSZ(1.0);
						p.setLabel(this->firstLabel);
						++this->firstLabel;
						poly.addPoint(p);
					}
				};
			};
			break;

			default: break;
		};

	}; 

	if (isClosed) poly.closeLine();

	if (useNormal)
	{
		this->auxExtrusionTrafo(axisX, axisY, axisZ);
		for (int i = 0; i < poly.getPointCount(); ++i)
		{
			CXYZPoint *P = poly.getPoint(i);
			C3DPoint p = auxExtrusionTrafo(axisX, axisY, axisZ, *P);
			P->x = p.x;
			P->y = p.y;
			P->z = p.z;
		};
	};

	return retVal;
};

 //============================================================================

bool CDXFReader::read3DFace(CXYZPolyLine &poly, int &color)
{
	bool retVal = true;

	int code = 1;
	CCharString inpLine;
	color = 1;

	long pos = this->pDxfFile->tellg();

	CXYZPoint p1, p2, p3, p4;

	bool hasPoint4 = false;

	while ((code != 0) && this->readNextCodeAndAttribute(code, inpLine))
	{
		switch (code)
		{
			case 10: retVal &= this->getDoubleFromString(inpLine, p1.x);
			break;

			case 11: retVal &= this->getDoubleFromString(inpLine, p2.x);
			break;

			case 12: retVal &= this->getDoubleFromString(inpLine, p3.x);
			break;

			case 13: 
			{
				retVal &= this->getDoubleFromString(inpLine, p4.x);
				hasPoint4 = true;
			};
			break;

			case 20: retVal &= this->getDoubleFromString(inpLine, p1.y);
			break;

			case 21: retVal &= this->getDoubleFromString(inpLine, p2.y);
			break;

			case 22: retVal &= this->getDoubleFromString(inpLine, p3.y);
			break;

			case 23: 
			{
				retVal &= this->getDoubleFromString(inpLine, p4.y);
				hasPoint4 = true;
			};
			break;

			case 30: retVal &= this->getDoubleFromString(inpLine, p1.z);
			break;

			case 31: retVal &= this->getDoubleFromString(inpLine, p2.z);
			break;

			case 32: retVal &= this->getDoubleFromString(inpLine, p3.z);
			break;

			case 33: 
			{
				retVal &= this->getDoubleFromString(inpLine, p4.z);
				hasPoint4 = true;
			};
			break;

			default: break;
		};

		if (code == 0) this->pDxfFile->seekg(pos);
		else pos = this->pDxfFile->tellg();
	}; 
	
	p1.setSX(1.0);
	p1.setSY(1.0);
	p1.setSZ(1.0);
	p1.setLabel(this->firstLabel);
	++this->firstLabel;
	p2.setSX(1.0);
	p2.setSY(1.0);
	p2.setSZ(1.0);
	p2.setLabel(this->firstLabel);
	++this->firstLabel;
	p3.setSX(1.0);
	p3.setSY(1.0);
	p3.setSZ(1.0);
	p3.setLabel(this->firstLabel);
	++this->firstLabel;
	poly.addPoint(p1);
	poly.addPoint(p2);
	poly.addPoint(p3);
	if (hasPoint4) 
	{
		p4.setSX(1.0);
		p4.setSY(1.0);
		p4.setSZ(1.0);
		p4.setLabel(this->firstLabel);
		++this->firstLabel;
		poly.addPoint(p4);
	}
	poly.closeLine();

	return retVal;
};

//============================================================================

void CDXFReader::auxExtrusionTrafo(C3DPoint &axisX, C3DPoint &axisY, C3DPoint &axisZ)
{
	float dmy = 1.0f / 64.0f;
	if ((fabs (axisZ.x) < dmy) && (fabs (axisZ.y) < dmy))  
	{
		axisX.x =  axisZ.z;
		axisX.y =  0.0;
		axisX.z = -axisZ.x;
	}
	else
	{
		axisX.x = -axisZ.y;
		axisX.y =  axisZ.x;
		axisX.z =  0.0;
	};

	axisX /= axisX.getNorm();
	axisY = axisZ.crossProduct(axisX);
};

//============================================================================
   
bool CDXFReader::getDoubleFromString(const CCharString &str, double &d)
{
	bool retVal = true;

	char *z = new char [str.GetLength() + 1];
	for (int i = 0; i < str.GetLength(); ++i)
	{
		z[i] = str[i];
	}
	z[str.GetLength()] = '\0';

	istrstream iss(z);
	iss >> d;
	if (iss.fail()) retVal = false;

	delete [] z;

	return retVal;
};


//============================================================================

C3DPoint CDXFReader::auxExtrusionTrafo(const C3DPoint &axisX,
									   const C3DPoint &axisY,
									   const C3DPoint &axisZ, 
									   const C3DPoint &P)
{
	return C3DPoint(axisX.x * P.x + axisY.x * P.y + axisZ.x * P.z,
		            axisX.y * P.x + axisY.y * P.y + axisZ.y * P.z,
					axisX.z * P.x + axisY.z * P.y + axisZ.z * P.z);
};

bool CDXFReader::addVertex(const C3DPoint &vertex)
{
	bool retVal = true;
	CXYZPoint *P = this->vertices.add();

	if (!P) retVal = false;
	else
	{
		P->x = vertex.x;
		P->y = vertex.y;
		P->z = vertex.z;
		P->setSX(1.0);
		P->setSY(1.0);
		P->setSZ(1.0);
		P->setLabel(this->firstLabel);
		++this->firstLabel;
	}

	return retVal;
};

bool CDXFReader::addLine(const C3DPoint &v1, const C3DPoint &v2, int color)
{
	bool retVal = true;

	CXYZPolyLine *poly = this->polylines.add();
	CXYZPoint p1(v1);
	p1.setSX(1.0);
	p1.setSY(1.0);
	p1.setSZ(1.0);
	p1.setLabel(this->firstLabel);
	++this->firstLabel;
	poly->addPoint(p1);
	CXYZPoint p2(v2);
	p2.setSX(1.0);
	p2.setSY(1.0);
	p2.setSZ(1.0);
	p2.setLabel(this->firstLabel);
	++this->firstLabel;
	poly->addPoint(p2);
		
	return retVal;
};

bool CDXFReader::addPolyline(const CXYZPolyLine &poly, int color)
{
	bool retVal = true;

	this->polylines.Add(poly);
	return retVal;
};

bool CDXFReader::addFace(const CXYZPolyLine &poly)
{
	bool retVal = true;
	this->polylines.Add(poly);
	return retVal;
};

