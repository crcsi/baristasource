#include <float.h>

#include "StructuredFileSection.h"

#include "XYContour.h"
/** 
 * @class CXYContour
 * CXYContour, class for CXYContourLines (generated from edge extraction
 *
 * @author Maria Pateraki
 * @version 1.0 
 * @date 12 Mar-2006
 *
 */
CXYContour::CXYContour(void)
{
	this->isclosed = false;
}

CXYContour::~CXYContour(void)
{
}

/**
 * copy constructor, contructor which derives from CXYContour
 * @param p
 */

CXYContour::CXYContour(CXYContour* p)
{
    this->label = p->label;
	this->isclosed = p->isclosed;

    //@ todo copy for PolyLine
 
}

/**
 * Sets Label of CXYContour
 * @param label
 */
void CXYContour::setLabel(CLabel label)
{
    this->bModified = true;
    this->label = label;
}

/**
 * Gets Label of CXYContour
 * returns CLabel
 */
CLabel CXYContour::getLabel()
{
    return this->label;
}
	  
void CXYContour::revert()
{
	unsigned long upper = this->getPointCount() - 1;
	unsigned long lower = 0;
	while (upper > lower)
	{
		this->points.Swap(upper, lower);
		++lower;
		--upper;
	};
};

C2DPoint* CXYContour::getNearestPoint (const C2DPoint &p) const
{
	C2DPoint *nearest = 0;
	double minDist = FLT_MAX;
	for (long i = 0; i < this->getPointCount(); ++i)
	{
		C2DPoint *pc = this->getPoint(i);
		double dist = pc->getDistance(p);
		if (dist < minDist)
		{
			nearest = pc;
			minDist = dist;
		}
	}

	return nearest;
};

int CXYContour::getIndexOfNearestPoint (const C2DPoint &p) const
{
	int nearest = -1;
	double minDist = FLT_MAX;

	for (int i = 0; i < this->getPointCount(); ++i)
	{
		C2DPoint *pc = this->getPoint(i);
		double dist = pc->getDistance(p);
		if (dist < minDist)
		{
			nearest = i;
			minDist = dist;
		}
	}

	return nearest;
};
	  
void CXYContour::append(CXYContour *contour)
{
	for (long i = 0; i < contour->getPointCount(); ++i)
	{
		this->addPoint(*(contour->getPoint(i)));
	}
};

/**
 * Gets Length of CXYContour
 * returns double
 */
double CXYContour::getLength()
{
	double length = 0;
	double dx,dy,dist;
	int size = this->getPoints()->GetSize();

	for ( int n = 0; n < size-1; n++)
	{
		C2DPoint* p1 = this->getPoints()->GetAt(n);
		C2DPoint* p2 = this->getPoints()->GetAt(n+1);
	//	CXYPoint* p1 = this->getPoints()->GetAt(n);
	//	CXYPoint* p2 = this->getPoints()->GetAt(n+1);
		//	length += p1->distanceFrom(p2);
//maria
		dx = p1->x - p2->x;
        dy = p1->y - p2->y;
        dist = sqrt((dx * dx) + (dy * dy));
		length += dist;
	
	}
	
	if (this->isclosed)
	{
		C2DPoint* p1 = this->getPoints()->GetAt(size-1);
		C2DPoint* p2 = this->getPoints()->GetAt(0);
	//	CXYPoint* p1 = this->getPoints()->GetAt(size-1);
	//	CXYPoint* p2 = this->getPoints()->GetAt(0);
		//length += p1->distanceFrom(p2);	
//maria
		dx = p1->x - p2->x;
        dy = p1->y - p2->y;

		dist = sqrt((dx * dx) + (dy * dy));

		length += dist;
		
	}
	
    return length;
}

/**
 * Serialized storage of a CXYZPolyLine to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYContour::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("label", this->getLabel().GetChar());

    token = pStructuredFileSection->addToken();
	token->setValue("isclosed", this->isclosed);

//    token = pStructuredFileSection->addToken();
//	token->setValue("xypoints", this->points.getFileName()->GetChar());

//	CStructuredFileSection* child = pStructuredFileSection->addChild();
//	this->points.serializeStore(child);
}

/**
 * Restores a CXYZPolyLine from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYContour::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("label"))
			this->label = CLabel(token->getValue());

		if(key.CompareNoCase("isclosed"))
			this->isclosed = token->getBoolValue();
    }

  /*  for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CXYPointArray"))
        {
            this->points.serializeRestore(child);
        }
	}*/

}

/**
 * reconnects pointers
 */
void CXYContour::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CXYContour::resetPointers()
{
    CSerializable::resetPointers();
}



/**
 * Flag if CXYContour is modified
 * returns bool
 */
bool CXYContour::isModified()
{
    return this->bModified;
}


/**
 * Gets an Enumeration of Points
 *
 * @return the Enumeration of Points
 */
C2DPointArray* CXYContour::getPoints()
{
    return &this->points;
}

void CXYContour::addPoint(C2DPoint newpoint)
{
    this->points.Add(newpoint);
}

int CXYContour::getPointCount () const
{
    return this->points.GetSize();
}

C2DPoint* CXYContour::getPoint (int index) const
{
   // return (C2DPoint)this->points.GetAt(index);
	return this->points.GetAt(index);
}

double CXYContour::getLength() const
{
	double length = 0.0;
	for (int i = 1; i < this->getPointCount(); ++i)
	{
		length += this->points.GetAt(i - 1)->getDistance(*this->points.GetAt(i));
	}
   
	if (this->isclosed && (this->getPointCount() > 1))
	{
		length += this->points.GetAt(this->getPointCount() - 1)->getDistance(*this->points.GetAt(0));
	}

	return length;
}


void CXYContour::deleteContour()
{
    this->points.RemoveAll();
}
void CXYContour::addVertex(int no,int v)
{
	if(no<0 || no>1)return;

	v_no[no]=v;

	return;
}
int* CXYContour::getIndexVertices()
{
    return (this->v_no);
}


void CXYContour::removePoint(int i)
{
    // check bounds
    if ( (i < 0) || (i > this->points.GetSize()))
        return;

    this->points.RemoveAt(i);
}
      
void CXYContour::removePoints(int iFrom, int iTo)
{
	int nCt = iTo - iFrom + 1;

	this->points.RemoveAt(iFrom, nCt);
};
	 
bool CXYContour::writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
		                  double xfactor, double yfactor) const
{
	int lineCode[ 3 ] = {10, 20, 30};

	dxf.setf(ios::fixed);
	dxf.precision(decimals);

	dxf << "  0\nPOLYLINE\n  8\n" << layer << "\n 62\n " << colour
        << "\n 10\n0.0\n 20\n0.0\n 30\n0.0\n 70\n";

	if (this->isclosed) dxf << "9\n";
	else dxf << "8\n"	;
  	dxf << "66\n 1\n";

	for (int i = 0; i < this->getPointCount(); ++i)
	{
		C2DPoint *p = this->points.GetAt(i);
		dxf << "  0\nVERTEX\n  8\n" << layer << "\n 62\n "  << colour << endl;
    
    
		p->writeDXF( dxf, lineCode, decimals, xfactor, yfactor);
	}
  
	dxf << "  0\nSEQEND\n  8\n0"	<< endl;


	return dxf.good();
};

