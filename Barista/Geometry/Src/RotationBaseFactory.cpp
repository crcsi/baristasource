#include "RotationBaseFactory.h"
#include "RollPitchYawRotation.h" 
#include "QuaternionRotation.h"
#include "OmegaPhiKappaRotation.h"

CRotationBase* CRotationBaseFactory::initRotation(const CPointBase* angles_rho,const RotationType type)
{
	switch (type)
	{
	case RPY		: return new CRollPitchYawRotation(angles_rho);	
	case OPK		: return new COmegaPhiKappaRotation(angles_rho);
	case Quaternion	: return new CQuaternionRotation(angles_rho);
	default			: return 0;				
	}
}

CRotationBase* CRotationBaseFactory::initRotation(const CRotationMatrix &mat,const RotationType type)
{
	switch (type)
	{
	case RPY		: return new CRollPitchYawRotation(mat);	
	case OPK		: return new COmegaPhiKappaRotation(mat);
	case Quaternion	: return new CQuaternionRotation(mat);
	default			: return 0;				
	}
}


CRotationBase* CRotationBaseFactory::initRotation(const CRotationBase* rotBase)
{
	switch (rotBase->getType())
	{
	case RPY		: return new CRollPitchYawRotation((CRollPitchYawRotation*)rotBase);	
	case OPK		: return new COmegaPhiKappaRotation((COmegaPhiKappaRotation*)rotBase);
	case Quaternion	: return new CQuaternionRotation((CQuaternionRotation*)rotBase);
	default			: return 0;				
	}
}