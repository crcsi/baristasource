/**
 * @class CXYZPointArray
 * an array of xyz points 
 */

//addition >>
//#include "ogrsf_frmts.h"
//#include <QMessageBox>
//<< addition
#include "XYZPointArray.h"
#include "ObsPoint.h"
#include "utilities.h"
#include "Trans3DFactory.h"
#include "StructuredFileSectionArray.h"
#include "newmat.h"
#include <algorithm>

CXYZPointArray::CXYZPointArray(int nGrowBy) : 
	CObsPointArray(3,nGrowBy), refSystem(),isboolControl(false),type(eNOTYPE),
	bCheckPoint(false)
{
    this->FileName = "3DPoints";
}

CXYZPointArray::CXYZPointArray(XyzPointType type): 
	CObsPointArray(3), refSystem(),isboolControl(false),type(type),
	bCheckPoint(false)
{
    this->FileName = "3DPoints";
}

CXYZPointArray::CXYZPointArray(const CXYZPointArray& src) : 
	CObsPointArray(3,src.GetGrowBy()), refSystem(src.refSystem), 
	isboolControl(src.isboolControl),type(src.type),bCheckPoint(src.bCheckPoint)
{
	for (int i = 0; i < src.GetSize(); ++i)
	{
		this->add(*src.getAt(i));
	}
}

CXYZPointArray::CXYZPointArray(CXYZPointArray *src) :   
	CObsPointArray(3,src->GetGrowBy()), refSystem(src->refSystem), 
	isboolControl(src->isboolControl),type(src->type),bCheckPoint(src->bCheckPoint)
{
	for (int i = 0; i < src->GetSize(); ++i)
	{
		this->add(*(src->getAt(i)));
	}
}

CXYZPointArray &CXYZPointArray::operator = (const CXYZPointArray & xyzArray)
{
	this->FileName = xyzArray.FileName;
    this->refSystem = xyzArray.refSystem;
	this->isboolControl =xyzArray.isboolControl;
	this->bCheckPoint = xyzArray.bCheckPoint;
	this->type = xyzArray.type;

	this->RemoveAll();

	for (int i = 0; i < xyzArray.GetSize(); ++i)
	{
		CXYZPoint *p = xyzArray.getAt(i);
		this->add(*p);
	}
	
	return *this;
}



CXYZPointArray::~CXYZPointArray(void)
{
}


bool CXYZPointArray::isa(string& className) const
{
    if ( className == "CXYZPointArray")
        return true;

    return false;
}


/**
 * reads XYZPointArray from a TextFile, input of
 * label x y z and label x y z sx sy sz is possible
 * @param cfilename
 * @returns true/false
 */
bool CXYZPointArray::readXYZPointTextFile(const CFileName& cfilename)
{
	FILE* in = fopen(cfilename.GetChar(),"r");
	if (!in )
		return false;

	char delimiters[4] = {' ',',',';','\t'};
	char string [1000];

    for (;;)
    {
        if (!fgets(string,999,in))//!file.ReadLine(str))
        break;

		int n = 0;
		while ( string[n] != 0 )
		{
			for ( int k = 0; k < 4; k++)
			{
				if ( string[n] == delimiters[k] )
				{
					// replace all types of delimters with ' '
					string[n] = delimiters[0];
					break;
				}
			}
			n++;
		}
		
		int delimiterCount = 0;
		bool onDilimiter = false;

		for ( int i = 0; i < n; i++ )
		{
			if ( string[i] == delimiters[0] )
			{
				if ( !onDilimiter) delimiterCount++;
				onDilimiter = true;
			}
			else onDilimiter = false;
		}

		// if line starts with delimiters, ignore first delimiter
		if ( string[0] == delimiters[0] ) delimiterCount--;

		// if line ends with delimiters, ignore last delimiter
		if ( string[n-2] == delimiters[0] ) delimiterCount--;

        if ( (delimiterCount == 3) ) // assume Label x y z, setting sx=sy=sz=0.1
        {
            char buf[1024];
            double x, y, z;

            ::sscanf(string, "%s%lf%lf%lf", buf, &x, &y, &z);

            CCharString tmp = "";

            tmp += buf;
            CLabel label(tmp);

			CObsPoint* point = this->Add();

            point->setLabel(label);
            (*point)[0] = x;
            (*point)[1] = y;
			(*point)[2] = z;

            // set default sigmas
            // set sx,sy,sz to 0.1 m in metric coordinate
            // and to 0.0000001 for slat/slon (~0.1m) and 0.1 m for height
            if ( this->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC )
            {
                point->setCovarElement(0,0,0.0000001);
                point->setCovarElement(1,1,0.0000001);
                point->setCovarElement(2,2,0.1);
            }
            else
            {
                point->setCovarElement(0,0,0.1);
                point->setCovarElement(1,1,0.1);
                point->setCovarElement(2,2,0.1);
            }


        }

        if ( (delimiterCount == 6) ) // assume Label x y z sx sy sz
        {
            char buf[1024];
            double x, y, z, sx, sy, sz;

            ::sscanf(string, "%s%lf%lf%lf%lf%lf%lf", buf, &x, &y, &z, &sx, &sy, &sz);

            CCharString tmp = "";

            tmp += buf;
            CLabel label(tmp);

            CObsPoint* point = this->Add();
            point->setLabel(label);
            (*point)[0] = x;
            (*point)[1] = y;
			(*point)[2] = z;
            point->setCovarElement(0,0,sx);
            point->setCovarElement(1,1,sy);
            point->setCovarElement(2,2,sz);
        } 
    }

    this->FileName = cfilename;

	fclose(in);

    return true;

}

bool CXYZPointArray::writeXYZPointShapefile(CFileName& cfilename)
{
	// source: http://gdal.org/ogr/ogr_apitut.html
	// we start by registering all the drivers, 
	// and then fetch the Shapefile driver as we will need it to create our output file. 
	/*const char *pszDriverName = "ESRI Shapefile";
    OGRSFDriver *poDriver;

    OGRRegisterAll();

    poDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName(
                pszDriverName );
    if( poDriver == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Driver not found!");
		return false;
    }

	// Next we create the datasource.
	OGRDataSource *poDS;

    poDS = poDriver->CreateDataSource((const char *) cfilename, NULL );
    if( poDS == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Datasource creation failed!");
		return false;
    }

	// Now we create the output layer.
	 OGRLayer *poLayer;

    poLayer = poDS->CreateLayer("XYZPointArray", NULL, wkbPoint25D, NULL );
    if( poLayer == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Layer creation failed!");
		return false;
    }

	 //we need to create any attribute fields that should appear on the layer.
	OGRFieldDefn oField( "Name", OFTString );
    oField.SetWidth(32);


    if( poLayer->CreateField( &oField ) != OGRERR_NONE )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Data field creation failed!");
		return false;
    }
	

	//double x, y;
    //char szName[33];
	const char* buf;
    double x, y, z;//, tx[2], ty[2];
	//double ht=0;
	//OGRLineString ls;

	for (int i = 0; i < this->GetSize(); i++)    
    //while( !feof(stdin) 
    //       && fscanf( stdin, "%lf,%lf,%32s", &x, &y, szName ) == 3 )
    {
		

        CObsPoint* point = this->GetAt(i);

        buf = point->getLabel().GetChar();
        x = (*point)[0];
        y = (*point)[1];
        z = (*point)[2];


        OGRFeature *poFeature;

        poFeature = OGRFeature::CreateFeature( poLayer->GetLayerDefn() );
        poFeature->SetField( "Name", buf );
	
        //poFeature->SetField("BHeight", z);

        OGRPoint pt;
		//if(i==0)
		//{
        pt.setX( x );
        pt.setY( y );
		pt.setZ( z );
		//ht = z-ht;
		//tx = x; ty = y;

		//}else
		//{
		//      pt.setX( tx );
        //pt.setY( ty );
		//pt.setZ( z );
		//}

		//ls.addPoint(x,y,z);

        poFeature->SetGeometry( &pt ); 

        if( poLayer->CreateFeature( poFeature ) != OGRERR_NONE )
        {
           //QMessageBox::warning(NULL, " Writing to shapefile!"
			//, "Feature creation failed!");
			return false;
        }

		// Now we create a feature in the file. The OGRLayer::CreateFeature() 
		// does not take ownership of our feature so we clean it up when done with it.
         OGRFeature::DestroyFeature( poFeature );
    }
*/
	return true;
}

/**
 * writes XYZPointArray to a TextFile, with 
 * switch for space/csv separation, csv = 0, space = 1
 * @param cfilename type
 * @returns true/false
 */
bool CXYZPointArray::writeXYZPointTextFile(CFileName& cfilename, int type)
{
    FILE	*fp;
	fp = fopen(cfilename.GetChar(),"w");

	if (!fp) return false;

    for (int i = 0; i < this->GetSize(); i++)
    {           
        const char* buf;
        double x, y, z, sx, sy, sz;

        CObsPoint* point = this->GetAt(i);

        buf = point->getLabel().GetChar();
        x = (*point)[0];
        y = (*point)[1];
        z = (*point)[2];
        sx = point->getCovarElement(0,0);
        sy = point->getCovarElement(1,1);
        sz = point->getCovarElement(2,2);
   
        if ( type == 1 ) // switch for space/csv separation, csv = 0, space = 1
        {
			if ( this->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC )
			{
				::fprintf(fp, "%s %.9f %.9f %.4f %.10f %.10f %.5f\n",  buf, x, y, z, sx, sy, sz);
			}
			else
			{
				::fprintf(fp, "%s %.4f %.4f %.4f %.5f %.5f %.5f\n",  buf, x, y, z, sx, sy, sz);
			}
        }
        else if ( type == 0 )
        {
			if ( this->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC )
			{
				::fprintf(fp, "%s,%.9f,%.9f,%.4f,%.10f,%.10f,%.5f\n",  buf, x, y, z, sx, sy, sz);
			}
			else
			{
				::fprintf(fp, "%s,%.4f,%.4f,%.4f,%.5f,%.5f,%.5f\n",  buf, x, y, z, sx, sy, sz);
			}
        }

    }

	fclose(fp);

    return true;
}

/**
 * writes XYZPointArray to a DD.MMSSsss TextFile
 * (only reasonable for geographic points
 * @param cfilename type
 * @returns true/false
 */
bool CXYZPointArray::writeXYZPointDDMMSSsssTextFile(CFileName& cfilename)
{
    FILE	*fp;
	fp = fopen(cfilename.GetChar(),"w");

	const char* buf;
	double x, y, z;//, sx, sy, sz;
	int latDD, latMM, latSS, lat4s;
	int longDD, longMM, longSS, long4s;
	double latMMsss, latSSsss, latsss;
	double longMMsss, longSSsss, longsss;

    for (int i = 0; i < this->GetSize(); i++)
    {
		CObsPoint* point = this->GetAt(i);

        buf = point->getLabel().GetChar();
        x = (*point)[0];
        y = (*point)[1];
        z = (*point)[2];

		latDD = (int)x;
		latMMsss = fabs(x - (double)latDD)*60.0;
		latMM = (int)latMMsss;
		latSSsss = (latMMsss - (double)latMM)*60.0;
		latSS = (int)latSSsss;
		latsss = (latSSsss - (double)latSS);
		lat4s = (int)(latsss*100000);

		longDD = (int)y;
		longMMsss = fabs(y - (double)longDD)*60.0;
		longMM = (int)longMMsss;
		longSSsss = (longMMsss - (double)longMM)*60.0;
		longSS = (int)longSSsss;
		longsss = (longSSsss - (double)longSS);
		long4s = (int)(longsss*100000);

        //sx = point->getCovarElement(0,0);
        //sy = point->getCovarElement(1,1);
        //sz = point->getCovarElement(2,2);

		//::fprintf(fp, "%s %2d.%02d%02d%05d %2d.%02d%02d%05d %.5lf\n",  buf, latDD, latMM, latSS, lat4s, longDD, longMM, longSS, long4s, z);
		fprintf(fp,"%s ",buf);
		fprintf(fp,"%d.",latDD);
		if (latMM==0)
			fprintf(fp,"00");
		else if (latMM<10)
		{
			fprintf(fp,"0");
			fprintf(fp,"%d",latMM);
		}
		else
			fprintf(fp,"%d",latMM);

		if (latSS==0)
			fprintf(fp,"00");
		else if (latSS<10)
		{
			fprintf(fp,"0");
			fprintf(fp,"%d",latSS);
		}
		else
			fprintf(fp,"%d",latSS);
		
		if (lat4s==0)
			fprintf(fp,"00000 ");
		else if (lat4s<10)
		{
			fprintf(fp,"0000");
			fprintf(fp,"%d ",lat4s);
		}
		else if (lat4s<100)
		{
			fprintf(fp,"000");
			fprintf(fp,"%d ",lat4s);
		}
		else if (lat4s<1000)
		{
			fprintf(fp,"00");
			fprintf(fp,"%d ",lat4s);
		}
		else if (lat4s<10000)
		{
			fprintf(fp,"0");
			fprintf(fp,"%d ",lat4s);
		}
		else
			fprintf(fp,"%d ",lat4s);



		fprintf(fp,"%d.",longDD);
		if (longMM==0)
			fprintf(fp,"00");
		else if (longMM<10)
		{
			fprintf(fp,"0");
			fprintf(fp,"%d",longMM);
		}
		else
			fprintf(fp,"%d",longMM);

		if (longSS==0)
			fprintf(fp,"00");
		else if (longSS<10)
		{
			fprintf(fp,"0");
			fprintf(fp,"%d",longSS);
		}
		else
			fprintf(fp,"%d",longSS);
		
		if (long4s==0)
			fprintf(fp,"00000 ");
		else if (long4s<10)
		{
			fprintf(fp,"0000");
			fprintf(fp,"%d ",long4s);
		}
		else if (long4s<100)
		{
			fprintf(fp,"000");
			fprintf(fp,"%d ",long4s);
		}
		else if (long4s<1000)
		{
			fprintf(fp,"00");
			fprintf(fp,"%d ",long4s);
		}
		else if (long4s<10000)
		{
			fprintf(fp,"0");
			fprintf(fp,"%d ",long4s);
		}
		else
			fprintf(fp,"%d ",long4s);

		fprintf(fp,"%f\n",z);
    }

	fclose(fp);

    return true;
}

/**
 * calculates mean with rms of a XYZPointArray 
 * @returns CXYZPoint
 */
CXYZPoint CXYZPointArray::getMean() const
{
    CXYZPoint MeanPoint;
    MeanPoint.setLabel("Mean");

    int size = this->GetSize();
    double x = 0;
    double y = 0;
    double z = 0;

    for (int i = 0; i < size; i++)
    {
        CObsPoint* NextPoint = this->GetAt(i);
        x += (*NextPoint)[0];
        y += (*NextPoint)[1];
        z += (*NextPoint)[2];
    }

    MeanPoint.setX( x / (double)size);
    MeanPoint.setY( y / (double)size);
    MeanPoint.setZ( z / (double)size);

    // Calculate variances
    double dx = 0;
    double dy = 0;
    double dz = 0;

    for (int i = 0; i < size; i++)
    {
        CObsPoint* NextPoint = this->GetAt(i);
        dx += (MeanPoint.getX() - (*NextPoint)[0]) * ((*NextPoint)[0] - (*NextPoint)[0]);
        dy += (MeanPoint.getY() - (*NextPoint)[1]) * ((*NextPoint)[1] - (*NextPoint)[1]);
        dz += (MeanPoint.getZ() - (*NextPoint)[2]) * ((*NextPoint)[2] - (*NextPoint)[2]);
    }

   
    dx /= (double)size;
    dy /= (double)size;
    dz /= (double)size;

    // Set the sigmas of Meanpoint
    dx = sqrt(dx);
    dy = sqrt(dy);
    dz = sqrt(dz);

    MeanPoint.setSX(dx);
    MeanPoint.setSY(dy);
    MeanPoint.setSZ(dz);

    return MeanPoint;
}

/** 
 * Determine differences of merging labels from two CXYZPointArrays
 *
 * @param CXYZPointArray* PointArray, CXYZPointArray* comparepoints
 */
void CXYZPointArray::differenceByLabel(CXYZPointArray* differences, CXYZPointArray* comparepoints)
{
    double rmsx = 0;
    double rmsy = 0;
    double rmsz = 0;

    double x = 0;
    double y = 0;
    double z = 0;

    int count = 0;

    CObsPoint* tempPoint1 = NULL;
    CObsPoint* tempPoint2 = NULL;
    CCharString tempLabel = "";

    for (int i = 0; i < this->GetSize(); i++)
    {
        tempPoint1 = this->GetAt(i);
		
		if (!tempPoint1->getStatusBit(0))
			continue;

        tempLabel = tempPoint1->getLabel().GetChar();
        tempPoint2 = comparepoints->getPoint(tempLabel);

        if (tempPoint2 == NULL)
            continue;

        x = (*tempPoint1)[0] - (*tempPoint2)[0];
        y = (*tempPoint1)[1]  -(*tempPoint2)[1];
        z = (*tempPoint1)[2]  -(*tempPoint2)[2];

		CObsPoint* newPoint = differences->Add();
		(*newPoint)[0] = x;
		(*newPoint)[1] = y;
		(*newPoint)[2] = z;
		newPoint->setLabel(tempLabel.GetChar());

        rmsx += (x * x);
        rmsy += (y * y);
        rmsz += (z * z);

        count ++;
    }

	if (count)
	{
		rmsx /= (double)count;
		rmsx = sqrt(rmsx);
		rmsy /= (double)count;
		rmsy = sqrt(rmsy);
		rmsz /= (double)count;
		rmsz = sqrt(rmsz);

		CObsPoint* RMS = differences->Add();
		RMS->setLabel("RMS");
		(*RMS)[0] = rmsx;
		(*RMS)[1] = rmsy;
		(*RMS)[2] = rmsz;
	}
}

void CXYZPointArray::differenceByIndex(CXYZPointArray* differences, CXYZPointArray* comparepoints)
{
    double rmsx = 0;
    double rmsy = 0;
    double rmsz = 0;

    int size = min(this->GetSize(), comparepoints->GetSize());

    for (int i = 0; i < size; i ++)
    {
        CObsPoint *p1 = this->GetAt(i);
        CObsPoint *p2 = comparepoints->GetAt(i);

        CXYZPoint diff(*p1);

        diff.setX( (*p1)[0] - (*p2)[0]);
        diff.setY( (*p1)[1] - (*p2)[1]);
        diff.setZ( (*p1)[2] - (*p2)[2]);

        rmsx += (diff.getX() * diff.getX());
        rmsy += (diff.getY() * diff.getY());
        rmsz += (diff.getZ() * diff.getZ());

        differences->Add(diff);
    }

    rmsx /= (double)size;
    rmsx = sqrt(rmsx);

    rmsy /= (double)size;
    rmsy = sqrt(rmsy);

    rmsz /= (double)size;
    rmsz = sqrt(rmsz);

    CObsPoint RMS(3);
    RMS.setLabel("RMS");
    RMS[0] = rmsx;
    RMS[1] = rmsy;
    RMS[2] = rmsz;
    differences->Add(RMS);
}


/**
 *  Gets a Matrix [n][5] where n is the number of common points and
 *  5 represents x, y, X, Y, Z
 * 	x y are from the 2D data and XYZ from the 3D.
 *
 *  @param Scene The Scene whose points will be merged with "this"
 *  @param Labels A Vector of the observation Labels
 */
Matrix CXYZPointArray::getMergedPoints(CCharStringArray* Labels, CBlockDiagonalMatrix* Covariance, CXYPointArray* Scene)
{
    this->sortByLabel();
    Scene->sortByLabel();

    int n1 = this->GetSize();
    int n2 = Scene->GetSize();

    int n;

    // the array can't be larger than the smaller
    if (n1 < n2)
        n = n1;
    else
        n = n2;

    Matrix _merged;
    _merged.ReSize(n, 5);

    int count = 0;

    // remove just in case
    Labels->RemoveAll();

    for (int i = 0; i < this->GetSize(); i++)
    {
        CObsPoint* xyzPoint = this->GetAt(i);

        // see if there is a match
        CObsPoint* xyPoint = Scene->getPoint(xyzPoint->getLabel().GetChar());
        if (xyPoint == NULL)
            continue;

        // Add label
    	CCharString* str;

        str = Labels->Add();
        *str = xyzPoint->getLabel().GetChar();

        // Add covariance
        Matrix cov;

        //cov.ReSize(2,2);
        cov.ReSize(5,5);
        cov = 0;
        cov.element(0, 0) = xyPoint->getCovarElement(0,0) * xyPoint->getCovarElement(0,0);
        cov.element(1, 1) = xyPoint->getCovarElement(1,1) * xyPoint->getCovarElement(1,1);
        cov.element(2, 2) = xyzPoint->getCovarElement(0,0) * xyzPoint->getCovarElement(0,0);
        cov.element(3, 3) = xyzPoint->getCovarElement(1,1) * xyzPoint->getCovarElement(1,1);
        cov.element(4, 4) = xyzPoint->getCovarElement(2,2) * xyzPoint->getCovarElement(2,2);

        Covariance->addSubMatrix(cov);

        //cov.ReSize(3,3);
        //cov = 0;
        //cov.element(0, 0) = xyzPoint->getSX()*xyzPoint->getSX();
        //cov.element(1, 1) = xyzPoint->getSY()*xyzPoint->getSY();
        //cov.element(2, 2) = xyzPoint->getSZ()*xyzPoint->getSZ();

        //Covariance->addSubMatrix(cov);
 
        //Covariance->addSubMatrix(&xyPoint->getCovariance());
        //Covariance->addSubMatrix(&xyzPoint->getCovariance());

        _merged.element(count, 0) = (*xyPoint)[0];
        _merged.element(count, 1) = (*xyPoint)[1];
        _merged.element(count, 2) = (*xyzPoint)[0];
        _merged.element(count, 3) = (*xyzPoint)[1];
        _merged.element(count, 4) = (*xyzPoint)[2];

        count++;
    }

    // now reduce the array to the proper length
    Matrix merged;
    merged.ReSize(count, 5);

    for (int i = 0; i < count; i++)
    {
        merged.element(i, 0) = _merged.element(i, 0);
        merged.element(i, 1) = _merged.element(i, 1);
        merged.element(i, 2) = _merged.element(i, 2);
        merged.element(i, 3) = _merged.element(i, 3);
        merged.element(i, 4) = _merged.element(i, 4);
    }


    return merged;
}

/**
 *  Gets a Matrix [n][4] where n is the number of common points and
 *  4 represents x, y, X, Y
 * 	x y are from the 2D data and XY from the 3D. Neglecting the Z information for e.g. TFW creation
 *
 *  @param Scene The Scene whose points will be merged with "this"
 *  @param Labels A Vector of the observation Labels
 */
Matrix CXYZPointArray::getMergedPointsforGeoreferencing(CCharStringArray* Labels, CBlockDiagonalMatrix* Covariance, CXYPointArray* Scene)
{
    this->sortByLabel();
    Scene->sortByLabel();

    int n1 = this->GetSize();
    int n2 = Scene->GetSize();

    int n;

    // the array can't be larger than the smaller
    if (n1 < n2)
        n = n1;
    else
        n = n2;

    Matrix _merged;
    _merged.ReSize(n, 4);

    int count = 0;

    // remove just in case
    Labels->RemoveAll();

    for (int i = 0; i < this->GetSize(); i++)
    {
        CObsPoint* xyzPoint = this->GetAt(i);

        // see if there is a match
        CObsPoint* xyPoint = Scene->getPoint(xyzPoint->getLabel().GetChar());
        if (xyPoint == NULL)
            continue;

        // Add label
    	CCharString* str;

        str = Labels->Add();
        *str = xyzPoint->getLabel().GetChar();

        // Add covariance
        Matrix cov;
        cov.ReSize(4,4);
        cov = 0;
        cov.element(0, 0) = xyPoint->getCovarElement(0,0) * xyPoint->getCovarElement(0,0);
        cov.element(1, 1) = xyPoint->getCovarElement(1,1) * xyPoint->getCovarElement(1,1);
        cov.element(2, 2) = xyzPoint->getCovarElement(0,0) * xyzPoint->getCovarElement(0,0);
        cov.element(3, 3) = xyzPoint->getCovarElement(1,1) * xyzPoint->getCovarElement(1,1);

        Covariance->addSubMatrix(cov);

        _merged.element(count, 0) = (*xyPoint)[0];
        _merged.element(count, 1) = (*xyPoint)[1];
        _merged.element(count, 2) = (*xyzPoint)[0];
        _merged.element(count, 3) = (*xyzPoint)[1];

        count++;
    }

    // now reduce the array to the proper length
    Matrix merged;
    merged.ReSize(count, 4);

    for (int i = 0; i < count; i++)
    {
        merged.element(i, 0) = _merged.element(i, 0);
        merged.element(i, 1) = _merged.element(i, 1);
        merged.element(i, 2) = _merged.element(i, 2);
        merged.element(i, 3) = _merged.element(i, 3);
    }


    return merged;
}


void CXYZPointArray::setControl(bool iscontrol)
{
    if (iscontrol && this->bCheckPoint)
		this->bCheckPoint = false;

	this->isboolControl = iscontrol;
    this->bModified = true;
}

bool CXYZPointArray::isControl() const
{
    return this->isboolControl;
}

void CXYZPointArray::setCheckPoint(bool isCheckPoint) 
{
	if (isCheckPoint && this->isboolControl)
		this->isboolControl = false;
	
	this->bCheckPoint = isCheckPoint; 
	this->bModified = true;
}

bool CXYZPointArray::isCheckPoint()const
{
	return this->bCheckPoint;
}


/**
 * Serialized storage of a CXYZPointArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYZPointArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("xyzpoint filename", this->FileName.GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("isControl", this->isControl());

    token = pStructuredFileSection->addToken();
    token->setValue("bCheckPoint", this->bCheckPoint);

    token = pStructuredFileSection->addToken();
	token->setValue("Type", this->getPointType());

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->getReferenceSystem()->serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->getDisplaySettings()->serializeStore(child);

    for (int i = 0; i < this->GetSize(); i++)
    {
        CXYZPoint *point = this->getAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        point->serializeStore(newSection);
    }

}

/**
 * Restores a CXYZPointArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYZPointArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("xyzpoint filename"))
            this->FileName = token->getValue();
		else if(key.CompareNoCase("isControl"))
            this->setControl(token->getBoolValue());
		else if(key.CompareNoCase("bCheckPoint"))
            this->setCheckPoint(token->getBoolValue());
		else if(key.CompareNoCase("Type"))
			this->setPointType((XyzPointType)token->getIntValue());

    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refSystem.serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CDisplaySettings"))
        {
			this->displaySettings.serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CXYZPoint"))
        {
            CXYZPoint *point = this->add();
            point->serializeRestore(child);       
			
		}
    }

}


/**
 * Gets ClassName
 * returns a string
 */
string CXYZPointArray::getClassName() const
{
    return string("CXYZPointArray");
}


void CXYZPointArray::setSXSYSZforAll(double sx, double sy, double sz)
{
	if (this->refSystem.getCoordinateType() == eGEOGRAPHIC)
	{
		double RFact =  180.0 / (M_PI * this->refSystem.getSpheroid().getSemiAverage());
		sx *= RFact;
		sy *= RFact;
	}

	for (int i = 0; i < this->GetSize(); i++)
	{
		CObsPoint* point = this->GetAt(i);

		point->setCovarElement(0,0,sx);
		point->setCovarElement(1,1,sy);
		point->setCovarElement(2,2,sz);
	}

	this->informListeners_elementsChanged();
}
	
void CXYZPointArray::setCoordinateType(eCoordinateType typ)
{
	if (this->refSystem.getCoordinateType() != typ)
	{
		CReferenceSystem sysNew(this->refSystem);
		sysNew.setCoordinateType(typ);
		
		//if (this->refSystem.getCoordinateType() == eUndefinedCoordinate)
		//	this->refSystem.setCoordinateType(typ);

		CTrans3D *trafo = CTrans3DFactory::initTransformation(this->refSystem, sysNew);

		if (trafo)
		{
			for (int i = 0; i < this->GetSize(); ++i)
			{
				CXYZPoint *p = this->getAt(i);
				trafo->transform(*p, *p);
			}

		//	this->refSystem.setCoordinateType(typ);
		}
		this->refSystem.setCoordinateType(typ);
	}
};

void CXYZPointArray::getExtends()
{
    int size = this->GetSize();

	if ( size > 0 )
	{
		CObsPoint* Point = this->GetAt(0);

		this->xmin = (*Point)[0];
		this->ymin = (*Point)[1];
		this->zmin = (*Point)[2];


		this->xmax = (*Point)[0];
		this->ymax = (*Point)[1];
		this->zmax = (*Point)[2];

		for (int i = 1; i < size; i++)
		{
			CObsPoint* NextPoint = this->GetAt(i);
			double x = (*NextPoint)[0];
			double y = (*NextPoint)[1];
			double z = (*NextPoint)[2];

			if ( x < this->xmin ) this->xmin = x;
			if ( x > this->xmax ) this->xmax = x;

			if ( y < this->ymin ) this->ymin = y;
			if ( y > this->ymax ) this->ymax = y;

			if ( z < this->zmin ) this->zmin = z;
			if ( z > this->zmax ) this->zmax = z;
		}
	}
}

//addition by M. Awrangjeb on 19 Mar 2009
/*
CXYZPoint *CXYZPointArray::getByLabel(CLabel lbl) const
{
	for (int i = 0; i<this->GetSize(); i++)
	{
		CXYZPoint *xyzp = (CXYZPoint *)this->GetAt(i);
		if (xyzp->getLabel() == lbl)
		{
			return xyzp;
		}
	}
	return NULL;
}
*/
//<<addition