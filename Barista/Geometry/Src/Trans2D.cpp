#include <strstream>

#include "newmat.h"
#include "XYPoint.h"
#include "StructuredFileSection.h"
#include "Trans2D.h"

CTrans2D::CTrans2D(): covar(0)
{
}

CTrans2D::~CTrans2D()
{
	if (this->covar) delete this->covar;
}

void CTrans2D::setCovar(const SymmetricMatrix &Qxx, int index0)
{
	int imax = this->nParameters();
	for (int i = 0; i < imax; ++i)
	{
		for (int j = i; j < imax; ++j)
		{
			this->covar->element(i,j) = Qxx.element(index0 + i, index0 + j);
		}
	}
}

CCharString CTrans2D::getDescriptor(const CCharString &lead, int shiftDigits, int matrixDigits, 
		                            const CCharString &descrFrom, const CCharString &descrTo) const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	int width = 12;
	int constPrec = 2;
	int matrxPrec = 9;

	oss << lead << "Identical transformation: \n\n" << lead << "     x" << descrTo.GetChar() << " = x" << descrFrom.GetChar() << "\n" 
		                                            << lead << "     y" << descrTo.GetChar() << " = y" << descrFrom.GetChar() << ends;		

	char *zstr = oss.str();
	CCharString retVal(zstr);
	delete[] zstr;

	return retVal;
}

void CTrans2D::transform (CXYZPoint &out, const CXYPoint &in) const
{
	out.setLabel(in.getLabel());
	out.x = in.x;
	out.y = in.y;
	out.setSX(in.getSX());
	out.setSY(in.getSY());
	out.setSXY(in.getSXY());
	out.setStatus(in.getStatus());
};

void CTrans2D::transform (CXYPoint &out, const CXYPoint &in) const
{
	out.setLabel(in.getLabel());
	out.x = in.x;
	out.y = in.y;
	out.setSX(in.getSX());
	out.setSY(in.getSY());
	out.setSXY(in.getSXY());
	out.setStatus(in.getStatus());
};

void CTrans2D::transform (C2DPoint &out, const C2DPoint &in) const
{
	out.x = in.x;
	out.y = in.y;
};


bool CTrans2D::invert() 
{ 
	return true;
};

void CTrans2D::transformWindow(int  offsetX,  int  offsetY,  int  width,  int  height,
		                       int &offsetXt, int &offsetYt, int &widtht, int &heightt,
						       int maxWidth,  int maxHeight, int marginX, int marginY) const
{
	C2DPoint p1((double) offsetX, (double) offsetY); 
	C2DPoint p2((double) offsetX + width, (double) offsetY + height);
	C2DPoint p3(p1.x, p2.y),  p4(p2.x, p1.y);
	C2DPoint P1, P2, P3, P4;
	this->transform(P1, p1);		this->transform(P2, p2);
	this->transform(P3, p3);		this->transform(P4, p4);
	if (P2.x < P1.x)
	{
		double dmy = P1.x;
		P1.x = P2.x;
		P2.x = dmy;
	};
	if (P3.x < P1.x)
	{
		double dmy = P1.x;
		P1.x = P3.x;
		P3.x = dmy;
	};
	if (P4.x < P1.x)
	{
		double dmy = P1.x;
		P1.x = P4.x;
		P4.x = dmy;
	};
	if (P2.y < P1.y)
	{
		double dmy = P1.y;
		P1.y = P2.y;
		P2.y = dmy;
	};
	if (P3.y < P1.y)
	{
		double dmy = P1.y;
		P1.y = P3.y;
		P3.y = dmy;
	};
	if (P4.y < P1.y)
	{
		double dmy = P1.y;
		P1.y = P4.y;
		P4.y = dmy;
	}
	if (P3.x > P2.x)
	{
		double dmy = P2.x;
		P2.x = P3.x;
		P3.x = dmy;
	};
	if (P4.x > P2.x)
	{
		double dmy = P2.x;
		P2.x = P4.x;
		P4.x = dmy;
	};
	if (P3.y > P2.y)
	{
		double dmy = P2.y;
		P2.y = P3.y;
		P3.y = dmy;
	};
	if (P4.y > P2.y)
	{
		double dmy = P2.y;
		P2.y = P4.y;
		P4.y = dmy;
	};
	
	offsetXt = (int) floor(P1.x) - marginX;
	offsetYt = (int) floor(P1.y) - marginY;
	widtht   = (int) floor(P2.x + 1) + marginX;
	heightt  = (int) floor(P2.y + 1) + marginY;
	
	if (offsetXt < 0) offsetXt = 0;
	if (offsetYt < 0) offsetYt = 0;
	if (widtht   < 0) widtht   = 0;
	if (heightt  < 0) heightt  = 0;

	if (offsetXt >= maxWidth)  offsetXt = maxWidth  - 1;
	if (offsetYt >= maxHeight) offsetYt = maxHeight - 1;
	if (widtht   >= maxWidth)  widtht   = maxWidth  - 1;
	if (heightt  >= maxHeight) heightt  = maxHeight - 1;
			
	widtht  -= offsetXt;
	heightt -= offsetYt;
};

