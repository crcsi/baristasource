#include "StructuredFileSection.h"

#include "GenericPolyLineArray.h"

CGenericPolyLineArray::CGenericPolyLineArray(int nGrowBy):
CMUObjectArray<GenericPolyLine, CGenericPolyLinePtrArray>(nGrowBy),FileName("PolyLine")
{
}

CGenericPolyLineArray::~CGenericPolyLineArray(void)
{
}


CFileName* CGenericPolyLineArray::getFileName()
{
    return &this->FileName;
}

void CGenericPolyLineArray::setFileName(const char* filename)
{
    this->FileName = filename;
}




GenericPolyLine* CGenericPolyLineArray::Add()
{
    GenericPolyLine* newline = CMUObjectArray<GenericPolyLine, CGenericPolyLinePtrArray>::Add();
	this->informListeners_elementAdded();
    return newline;
}

GenericPolyLine* CGenericPolyLineArray::Add(const GenericPolyLine& newElement)
{
    GenericPolyLine* newline = CMUObjectArray<GenericPolyLine, CGenericPolyLinePtrArray>::Add(newElement);
	this->informListeners_elementAdded();
    return newline;
}


void CGenericPolyLineArray::Remove(GenericPolyLine* pElement)
{
    this->informListeners_elementDeleted(pElement->getLabel());
	CMUObjectArray<GenericPolyLine, CGenericPolyLinePtrArray>::Remove(pElement);
}

void CGenericPolyLineArray::RemoveAt(int nIndex, int nCount)
{
	for (int k = 0; k < nCount; k++)
	{
		CLabel label = this->GetAt(nIndex + k)->getLabel();
		this->informListeners_elementDeleted(label);
	}
	CMUObjectArray<GenericPolyLine, CGenericPolyLinePtrArray>::RemoveAt(nIndex,nCount);
}

void CGenericPolyLineArray::RemoveAll()
{
	this->informListeners_elementDeleted();
	CMUObjectArray<GenericPolyLine, CGenericPolyLinePtrArray>::RemoveAll();
}


void CGenericPolyLineArray::selectElement(const CLabel& element,bool select)
{
	this->informListeners_elementSelected(select,element);

	GenericPolyLine* p = this->getLine(element.GetChar());
	if (p)
		p->setSelected(select);
		
}

void CGenericPolyLineArray::unselectAll()
{
	for (int k=0; k < this->GetSize(); k++)
	{
		GenericPolyLine* p = this->GetAt(k);
		if (p && p->getIsSelected())
			{
				CLabel l = p->getLabel();
				this->selectElement(l,false);
			}
	}
}

GenericPolyLine* CGenericPolyLineArray::getLine(int i)
{
    return this->GetAt(i);
}


/**
 * reconnects pointers
 */
void CGenericPolyLineArray::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CGenericPolyLineArray::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Flag if CXYZPolyLineArray is modified
 * returns bool
 */
bool CGenericPolyLineArray::isModified()
{
    for (int i = 0; i < this->GetSize(); i++)
    {
        if (this->GetAt(i)->isModified())
            return true;
    }

    return false;
}

CFileName CGenericPolyLineArray::getItemText()
{
    CFileName itemtext = this->FileName.GetFullFileName();
    return itemtext.GetChar();
}


GenericPolyLine* CGenericPolyLineArray::getLine(CCharString Label)
{
    GenericPolyLine* testline = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        testline = this->GetAt(i);

        if (Label.CompareNoCase(testline->getLabel().GetChar()))
            return testline;

        continue;
    }

    return NULL;
}