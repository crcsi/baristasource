/**
 * @class CXYPoint
 * an xy point that can have a label
 * and a 2x2 covariance matrix for use in least-sqaures
 */
#include <strstream>
#include <float.h>
#include <math.h>
#include "newmat.h"
#include "StructuredFileSection.h"


#include "XYPoint.h"

CXYPoint::~CXYPoint() 
{ 
}

CXYPoint &CXYPoint::operator = (const CXYPoint & point)
{
	CObsPoint::operator=(point);
	return *this;
}



CCharString CXYPoint::toString(int length, int digits) const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(digits);
	oss << this->label.GetChar() << " ";
	oss.width(length);
	oss << x << " " ;
	oss.width(length);
	oss << y << ends;
	char *z = oss.str();
	CCharString str(z);
	delete [] z;
	return str;
};


void CXYPoint::setFromHomogeneousVector(const ColumnVector &pHom, const Matrix &covarHom)
{   
	double homogeneous = 1.0f / pHom.element(2);
    this->bModified = true;
	this->x = pHom.element(0) * homogeneous;
	this->y = pHom.element(1) * homogeneous;
  
	Matrix jacobian(3,3);
    jacobian = 0.0f;
	jacobian.element(0,0) = jacobian.element(1,1) =  homogeneous;
	jacobian.element(0,2) = -this->x * homogeneous;
	jacobian.element(1,2) = -this->y * homogeneous;

	jacobian = jacobian * covarHom * jacobian.t();
	*covariance = jacobian.SubMatrix(1,2,1,2);
};

/**
 * Sets SX (diagonal element) of CXYPoint in covariance matrix
 * sets all other SX-correlated values to zero
 */
void CXYPoint::setSX (double sigmaX)
{
    this->bModified = true;
    this->covariance->element(0, 0) = sigmaX * sigmaX;
    this->covariance->element(0, 1) = 0.;
    this->covariance->element(1, 0) = 0.;
}

/**
 * Sets SY (diagonal element) of CXYPoint in covariance matrix
 * sets all other SY-correlated values to zero
 */
void CXYPoint::setSY (double sigmaY)
{
    this->bModified = true;
    this->covariance->element(1, 1) = sigmaY * sigmaY;
    this->covariance->element(0, 1) = 0.;
    this->covariance->element(1, 0) = 0.;
}
/**
 * Sets SXY (off-diagonal element) of CXYPoint in covariance matrix
 */
void CXYPoint::setSXY (double sigmaXY)
{
    this->bModified = true;
    this->covariance->element(0, 1) = sigmaXY;
    this->covariance->element(1, 0) = sigmaXY;
}

/**
 * Compares the XY-coordinates of two CXYPoints
 * @param point
 * returns true if coordinates are equal, else false
 */
bool CXYPoint::equals (CXYPoint* point)
{
    if ((this->x == point->x) && (this->y == point->y))
        return true;
    else
        return false;
}
/**
 * Compares the XY-coordinates of two CXYPoints
 * @param point
 * returns true if coordinates are equal, else false
 */
bool CXYPoint::equals (const CXYPoint &point) const
{
    if ((this->x == point.x) && (this->y == point.y))
        return true;
    else
        return false;
}

/**
 * Serialized storage of a CXYPoint to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYPoint::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("label", this->getLabel().GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("x", this->getX());

    token = pStructuredFileSection->addToken();
    token->setValue("y", this->getY());

    token = pStructuredFileSection->addToken();
    token->setValue("sx", this->getSX());

    token = pStructuredFileSection->addToken();
    token->setValue("sy", this->getSY());

    token = pStructuredFileSection->addToken();
    token->setValue("sxy", this->getSXY());

    token = pStructuredFileSection->addToken();
    token->setValue("status", this->status);
}

/**
 * Restores a CXYPoint from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYPoint::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	Matrix localCovar(2,2);
	localCovar = 0;
	bool foundCovarElement = false;
	
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("label"))
			this->label = CLabel(token->getValue());
        else if (key.CompareNoCase("x"))
            this->x = token->getDoubleValue();
        else if (key.CompareNoCase("y"))
            this->y = token->getDoubleValue();
        else if (key.CompareNoCase("sx"))
		{
			double sigmax = token->getDoubleValue();
			localCovar(1,1) = sigmax * sigmax;
			foundCovarElement = true;
		}
        else if (key.CompareNoCase("sy"))
		{
			double sigmay = token->getDoubleValue();
			localCovar(2,2) = sigmay * sigmay;
			foundCovarElement = true;
		}
		else if (key.CompareNoCase("sxy"))
		{
			localCovar(1,2) = localCovar(2,1) = token->getDoubleValue();
			foundCovarElement = true;
		}
		else if (key.CompareNoCase("status"))
		{
			this->status = token->getIntValue();
		}
    }

	if (foundCovarElement) this->setCovariance(localCovar);
}

/**
 * reconnects pointers
 */
void CXYPoint::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CXYPoint::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Flag if CXYPoint is modified
 * returns bool
 */
bool CXYPoint::isModified()
{
    return this->bModified;
}

double CXYPoint::getSX() const    
{ 
	return sqrt(this->covariance->element(0, 0)); 
}
    
double CXYPoint::getSY() const    
{ 
	return sqrt(this->covariance->element(1, 1)); 
}

double CXYPoint::getSXY() const   
{ 
	return this->covariance->element(1, 0); 
}




/**
 * Get the axiator S, assuming the homogeneous part to be 1
 * of point
 * @param point
 */
Matrix CXYPoint::getS() const
{
	Matrix s(3,3);  
	s.element(0,0) =  0.0f; s.element(0,1) = -1.0f; s.element(0,2) =  y;
	s.element(1,0) =  1.0f; s.element(1,1) =  0.0f; s.element(1,2) = -x;
	s.element(2,0) = -y;    s.element(2,1) =  x;    s.element(2,2) = 0.0f;
	return s;
};

