/**
 * @class CXYEllipseArray
 * an array of XYEllipses 
 */
#include "XYEllipseArray.h"
#include "XYEllipsePtrArray.h"

#include "XYEllipse.h"
#include "Serializable.h"
#include "StructuredFileSectionArray.h"
#include "math.h"



CXYEllipseArray::CXYEllipseArray(int nGrowBy) : 
	CMUObjectArray<CXYEllipse, CXYEllipsePtrArray>(nGrowBy)
{
    this->FileName = "XYEllipses";
}

CXYEllipseArray::~CXYEllipseArray(void)
{
}


CFileName* CXYEllipseArray::getFileName()
{
    return &this->FileName;
}

void CXYEllipseArray::setFileName(const char* filename)
{
    this->FileName = filename;
}

CXYEllipse* CXYEllipseArray::Add()
{
    CXYEllipse* newellipse = CMUObjectArray<CXYEllipse, CXYEllipsePtrArray>::Add();

	this->informListeners_elementAdded();

    return newellipse;
}

CXYEllipse* CXYEllipseArray::Add(const CXYEllipse& newElement)
{
    CXYEllipse* newellipse = CMUObjectArray<CXYEllipse, CXYEllipsePtrArray>::Add(newElement);

    this->informListeners_elementAdded();

    return newellipse;
}



CXYEllipse* CXYEllipseArray::getEllipse(int i)
{
    return this->GetAt(i);
}

/** SLOW FOR LOTS OF Ellipse!
 * Gets a Ellipse by label (null if not found)
 *
 * @param CCharString Label of Ellipse
 */
CXYEllipse* CXYEllipseArray::getEllipse(CCharString Label)
{
    CXYEllipse* testEllipse = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        testEllipse = this->GetAt(i);

        if (Label.CompareNoCase(testEllipse->getLabel().GetChar()))
            return testEllipse;

        continue;
    }

    return NULL;
}


/**
 * Serialized storage of a CXYEllipseArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYEllipseArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("XYEllipse filename", this->FileName.GetChar());

    for (int i = 0; i < this->GetSize(); i++)
    {
        CXYEllipse* ellipse = this->GetAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        ellipse->serializeStore(newSection);
    }

}

/**
 * Restores a CXYEllipseArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYEllipseArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("XYEllipse filename"))
            this->FileName = token->getValue();

    }

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();
    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);

        if (child->getName().CompareNoCase("CXYEllipse"))
        {
            CXYEllipse* ellipse = this->Add();
            ellipse->serializeRestore(child);
        }
    }

}

/**
 * reconnects pointers
 */
void CXYEllipseArray::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CXYEllipseArray::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Flag if CXYEllipseArray is modified
 * returns bool
 */
bool CXYEllipseArray::isModified()
{
    for (int i = 0; i < this->GetSize(); i++)
    {
        if (this->GetAt(i)->isModified())
            return true;
    }

    return false;
}

CFileName CXYEllipseArray::getItemText()
{
    CFileName itemtext = this->FileName.GetFullFileName();
    return itemtext.GetChar();
}

/**
 * writes XYEllipseArray to a TextFile, with
 * @param cfilename
 * @returns true/false
 */
bool CXYEllipseArray::writeEllipseTextFile(CFileName cfilename)
{
    FILE	*fp;
	fp = fopen(cfilename.GetChar(),"w");

    for (int i = 0; i < this->GetSize(); i++)
    {           
        const char* buf;

        CXYEllipse* ellipse = this->GetAt(i);

		buf = ellipse->getLabel().GetChar();
		double a = ellipse->getA();
		double b = ellipse->getB();
		double theta = ellipse->getTheta();
		double x0 = ellipse->getX0();
		double y0 = ellipse->getY0();
		
		::fprintf(fp, "%s %f %f %f %f %f\n",  buf, a, b, theta, x0, y0);
	}
	fclose(fp);

    return true;
}

/**
 * writes Ellipse Centers to a TextFile, with
 * @param cfilename
 * @returns true/false
 */
bool CXYEllipseArray::writeEllipseCentersTextFile(CFileName cfilename)
{
    FILE	*fp;
	fp = fopen(cfilename.GetChar(),"w");

    for (int i = 0; i < this->GetSize(); i++)
    {           
        const char* buf;

        CXYEllipse* ellipse = this->GetAt(i);

		CXYPoint center = ellipse->getCenter();
		buf = center.getLabel().GetChar();
		double x0 = center.getX();
		double sx0 = center.getSX();
		double y0 = center.getY();
		double sy0 = center.getSY();
		
		::fprintf(fp, "%s %f %f %f %f\n",  buf, x0, y0, sx0, sy0);
	}
	fclose(fp);

    return true;
}