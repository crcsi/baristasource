/**
 * @class CXYLineSegment
 * an 2D line segment with start and end points, 
 * a projective representation including covariance matrix,
 * and methods to compute the straight line parameters by regression
 */
#include <strstream>
using namespace std;

#include <math.h>
#include <float.h>

#include "newmatap.h"
#include "statistics.h"
#include "XYContour.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
#include "3DadjPlane.h"

#include "XYLineSegment.h"
/**
 * default contructor
 * @param x
 * @param y
 */
CXYLineSegment::CXYLineSegment(void):  
   startPt(0.0f, 0.0f), endPt(1.0f, 0.0f)
{
 set(startPt, endPt);
}

CXYLineSegment::~CXYLineSegment(void)
{
}

/**
 * contructor from start and end points
 * @param I_start
 * @param I_end
 */
CXYLineSegment::CXYLineSegment(const CXYPoint &I_start, const CXYPoint &I_end):  
   CXYLine(I_start, I_end), startPt(I_start), endPt(I_end)
{
    set(startPt, endPt);
} 
/**
 * copy contructor which derives from I_segment
 * @param I_segment
 */
CXYLineSegment::CXYLineSegment(const CXYLineSegment &I_segment):
     CXYLine(I_segment), startPt(I_segment.startPt), endPt(I_segment.endPt)
{
};

/**
 * sets the start and end points
 * @param I_start
 * @param I_end
 */
void CXYLineSegment::set(const CXYPoint &I_start, const CXYPoint &I_end)
{
    this->bModified = true;
 
	this->startPt = I_start;
	this->endPt = I_end;
	this->normal.x = -(endPt.getY() - startPt.getY());
	this->normal.y =   endPt.getX() - startPt.getX();
	this->cog = ((C2DPoint)this->endPt + this->startPt) * 0.5f;
	euclidean = -startPt.innerProduct(normal);
    eigenvalues.x = 0.0f;
	eigenvalues.y = 0.0f;
	initCovarFromPoints();
    normalise();
};

//=============================================================================
  
void CXYLineSegment::initCovarFromPoints()
{
	Matrix s1 = startPt.getS();
	Matrix covar1(3,3);
	covar1 = 0;
	covar1.SubMatrix(1,2,1,2) = *startPt.getCovariance();
	Matrix s2 = endPt.getS();
	Matrix covar2(3,3);
	covar2 = 0;
	covar2.SubMatrix(1,2,1,2) = *endPt.getCovariance();
	
    covariance = s1 * covar2 * s1.t() + s2 * covar1 * s2.t();
	sigma0 = 1.0f;
};

//=============================================================================
	
C3DAdjustPlane CXYLineSegment::getVerticalPlane(double z0) const
{
	return C3DAdjustPlane(*this, z0);
};

//=============================================================================

double CXYLineSegment::getDistanceofBasePointFromStart(const C2DPoint &p) const
{ 
	C2DPoint dir(this->endPt.x - this->startPt.x, this->endPt.y - this->startPt.y);
	dir /= dir.getNorm();
	return (dir.innerProduct(p - this->startPt)); 
};

//=============================================================================
/**
 * Compares the start and end points of two CXYLineSegments
 * @param segment
 * returns true if both start and end points are equal
 */
bool CXYLineSegment::equals (const CXYLineSegment &segment) const
{
	if ((this->startPt.equals(segment.startPt)) && 
		(this->endPt.equals(segment.endPt)))
        return true;
    else
        return false;
}

//=============================================================================
/**
 * Calculates the length of the segment 
 * returns double value of the distance 
 */

double CXYLineSegment::length() const
{
	return this->startPt.distanceFrom(&(this->endPt));
};

//=============================================================================
/**
 * Calculates the nearest distance of point from the segment 
 * @param point
 * returns double value of the distance 
 */    

double CXYLineSegment::distanceFromSegment (const C2DPoint &point) const
{
  C2DPoint distVec = point - startPt;
  C2DPoint dirVec  = this->getDirection();

  if (distVec.innerProduct(dirVec) >= 0)
  {
   distVec = point - endPt;
   if (distVec.innerProduct(dirVec) <= 0)
   {
    distVec = point - getBasePoint(point);
   };
  };

  return distVec.getNorm();
};

//=============================================================================
/**
 * Calculates the nearest distance between this and the segment 
 * @param segment
 * returns double value of the distance 
 */  

double CXYLineSegment::distanceFromSegment(const CXYLineSegment &segment ) const
{
	CXYPoint intersection;
	eIntersect intersect = this->getIntersection( segment, intersection );

	C2DPoint differences;
 
	if ( intersect == eParallel) 
	{
     C2DPoint direction = getDirection();
	 double my11 = direction.innerProduct((C2DPoint)segment.startPt - startPt);
	 double my12 = direction.innerProduct((C2DPoint)segment.endPt   - startPt);
	 double length1 = length();

     if ((my11 < 0) && (my12 < 0)) 
		 differences = (fabs(my11) > fabs(my12)) ?
		 (C2DPoint)segment.endPt - startPt : (C2DPoint)segment.startPt - startPt;
	 else if  ((my11 > length1) && (my12 > length1)) 
		 differences = (fabs(my11) > fabs(my12)) ?
		 (C2DPoint)segment.endPt - endPt : (C2DPoint)segment.startPt - endPt;
	 else 
	 {
		 C2DPoint base1 = segment.getBasePoint(startPt);
		 C2DPoint base2 = segment.getBasePoint(endPt);
		 C2DPoint base3 = getBasePoint(base1);
		 C2DPoint base4 = getBasePoint(base2);
         differences = ((base3 - base1) + (base4 - base2)) * 0.5;
	 };
	}
	else
	{
		C2DPoint diffVec = (C2DPoint)segment.startPt - startPt;
		double my     = -normal.innerProduct(diffVec)         / normal.innerProduct(segment.getDirection());
		double lambda =  segment.normal.innerProduct(diffVec) / segment.normal.innerProduct(getDirection());

		if (lambda < 0)
		{
			if (my < 0 ) differences = (C2DPoint)startPt - segment.startPt;
			else if (my <= segment.length()) return segment.distanceFromLine(startPt);
			else differences = (C2DPoint)startPt - segment.endPt;
		}
		else if (lambda > length())
		{
			if (my < 0) differences = (C2DPoint)endPt - segment.startPt;
			else if (my > segment.length()) differences = (C2DPoint)endPt - segment.endPt;
			else return segment.distanceFromLine(endPt);
		}
		else
		{
			if (my < 0 ) return distanceFromLine(segment.startPt);
			if (my > segment.length() ) return distanceFromLine(segment.endPt);
			else differences = this->getPointOnLine(lambda) - segment.getPointOnLine(my);
		};
	};

	return differences.getNorm();
};

//=============================================================================
/**
 * reverse the segment
 */
void CXYLineSegment::reverse()
{
	normal = -normal;
    euclidean = -euclidean;
    CXYPoint swapPt = startPt;
	startPt = endPt;
	endPt   = swapPt;
};
	
//=============================================================================

C2DPoint CXYLineSegment::getPointOnLine( double dist) const
{ 
	return ( startPt + dist * getDirection() ); 
};

//=============================================================================

C2DPoint CXYLineSegment::getPointOnLinePercent( double percent ) const
{ 
	C2DPoint p1 = startPt;
	C2DPoint p2 = endPt;
	return ( p1 + percent  * (p2 - p1) ); 
};

//=============================================================================
/**
 * Serialized storage of a CXYLineSegment to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYLineSegment::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("x1", this->getStartPt().getX());

    token = pStructuredFileSection->addToken();
    token->setValue("y1", this->getStartPt().getY());

    token = pStructuredFileSection->addToken();
    token->setValue("sx1", this->getStartPt().getSX());

    token = pStructuredFileSection->addToken();
    token->setValue("sy1", this->getStartPt().getSY());

    token = pStructuredFileSection->addToken();
    token->setValue("sxy1", this->getStartPt().getSY());

	token = pStructuredFileSection->addToken();
	token->setValue("x2", this->getEndPt().getX());

    token = pStructuredFileSection->addToken();
    token->setValue("y2", this->getEndPt().getY());

    token = pStructuredFileSection->addToken();
    token->setValue("sx2", this->getEndPt().getSX());

    token = pStructuredFileSection->addToken();
    token->setValue("sy2", this->getEndPt().getSY());

    token = pStructuredFileSection->addToken();
    token->setValue("sxy2", this->getEndPt().getSY());
}

//=============================================================================
/**
 * Restores a CXYLineSegment from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYLineSegment::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	Matrix varcovar1(2,2);
	Matrix varcovar2(2,2);
    varcovar1 = 0.0f;
	varcovar2 = 0.0f;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if (key.CompareNoCase("x1"))
			this->startPt.setX(token->getDoubleValue());
        else if (key.CompareNoCase("y1"))
            this->startPt.setY(token->getDoubleValue());
        else if (key.CompareNoCase("x2"))
			this->endPt.setX(token->getDoubleValue());
        else if (key.CompareNoCase("y2"))
            this->endPt.setY(token->getDoubleValue());
		else if (key.CompareNoCase("sx1"))
            varcovar1(1,1) = token->getDoubleValue();
        else if (key.CompareNoCase("sy1"))
            varcovar1(2,2) = token->getDoubleValue();
		else if (key.CompareNoCase("sxy1"))
            varcovar1(2,1) = varcovar1(1,2) = token->getDoubleValue();
		else if (key.CompareNoCase("sx2"))
            varcovar2(1,1) = token->getDoubleValue();
        else if (key.CompareNoCase("sy2"))
            varcovar2(2,2) = token->getDoubleValue();
		else if (key.CompareNoCase("sxy2"))
            varcovar2(2,1) = varcovar2(1,2) = token->getDoubleValue();
    }
	
    this->startPt.setCovariance(varcovar1);
	this->endPt.setCovariance(varcovar2);
	this->set(this->startPt,this->endPt);
}

//=============================================================================
/**
 * reconnects pointers
 */
void CXYLineSegment::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

//=============================================================================

/**
 * resets pointers
 */
void CXYLineSegment::resetPointers()
{
    CSerializable::resetPointers();
}

//=============================================================================

/**
 * Flag if CXYLineSegment is modified
 * returns bool
 */
bool CXYLineSegment::isModified()
{
    return this->bModified;
}

//=============================================================================

void CXYLineSegment::initCovarFromAdjustment(double I_sigma0)
{
	this->sigma0 = this->eigenvalues.x;
	if (this->pointcount > 2) this->sigma0 = sqrt(this->sigma0/(this->pointcount - 2));
	if ((I_sigma0 < 0) || (this->sigma0 > I_sigma0)) I_sigma0 = this->sigma0;
	I_sigma0 *= I_sigma0;

	Matrix trafMat1 = this->getRotMat();
	Matrix trafMat2(3,3);
	trafMat2(1,1) =  1.0f;  trafMat2(1,2) =  0.0f;  trafMat2(1,3) = 0.0f;
	trafMat2(2,1) =  0.0f;  trafMat2(2,2) =  1.0f;  trafMat2(2,3) = 0.0f;
	trafMat2(3,1) = -cog.x; trafMat2(3,2) = -cog.y; trafMat2(3,3) = 1.0f;

	Matrix trafMat = trafMat2 * trafMat1;
	this->covariance = 0.0f;
	this->covariance(1,1) = I_sigma0 / this->eigenvalues.y;
	this->covariance(3,3) = I_sigma0 / double(this->pointcount);

	C2DPoint dir = this->getDirection();

	double sigma1 = dir.innerProduct(this->startPt - cog);
    double sigma2 = dir.innerProduct(this->endPt   - cog);
	sigma1 = this->covariance(3,3) + this->covariance(1,1) * sigma1 * sigma1;
	sigma2 = this->covariance(3,3) + this->covariance(1,1) * sigma2 * sigma2;
	startPt.setSX(0.0f);
	startPt.setSY(sigma1);
	endPt.setSX(0.0f);
	endPt.setSY(sigma2);

	this->covariance = trafMat * this->covariance * trafMat.t();

	Matrix covarPt(3,3); covarPt = 0.0f;
	covarPt.SubMatrix(1,2,1,2) = (*startPt.getCovariance());
	covarPt = trafMat1 * covarPt * trafMat1.t();
	startPt.setCovariance(covarPt.SubMatrix(1,2,1,2));

	covarPt = 0.0f;
	covarPt.SubMatrix(1,2,1,2) = (*endPt.getCovariance());
	covarPt = trafMat1 * covarPt * trafMat1.t();
	endPt.setCovariance(covarPt.SubMatrix(1,2,1,2));
};

//=============================================================================

void CXYLineSegment::cutOff(const CXYPoint &point1, const CXYPoint &point2)
{
	startPt = this->projectedPoint(point1);
	endPt   = this->projectedPoint(point2);
};
//=============================================================================

bool CXYLineSegment::project(const CXYLineSegment &segment)
{
	C2DPoint dir = (C2DPoint) segment.endPt - (C2DPoint) segment.startPt;
	dir.normalise();
	
	C2DPoint delta1 = (C2DPoint) this->startPt - (C2DPoint) segment.startPt;
	C2DPoint delta2 = (C2DPoint) this->endPt  - (C2DPoint) segment.startPt;

	double d1 = dir.innerProduct(delta1);
	double d2 = dir.innerProduct(delta2);
	if (d1 > d2)
	{
		double d = d1; d1 = d2; d2 = d;
	}

	if (d2 < 0) return false;

	double l = segment.length();

	if (d1 > l) return false;

	if (d1 < 0) d1 = 0.0;
	if (d2 > l) d2 = l;


	this->startPt = segment.startPt + d1 * dir; 
	this->endPt   = segment.startPt + d2 * dir; 
	*this = CXYLineSegment(this->startPt, this->endPt);
	return true;
};

//=============================================================================

void CXYLineSegment::initFromContour(CXYContour *contour, 
									 unsigned long indexFrom,
									 unsigned long indexTo, 
									 double I_sigma0)
{
	computeParametersFromContour(contour, indexFrom, indexTo, I_sigma0);
	
	C2DPoint direction = this->getDirection();
	C2DPoint point1 = cog + direction.innerProduct(*(contour->getPoint(indexFrom)) - cog) * direction;
	C2DPoint point2 = cog + direction.innerProduct(*(contour->getPoint(indexTo))   - cog) * direction;

	startPt.set(point1.x, point1.y);
	endPt.set  (point2.x, point2.y);

	initCovarFromAdjustment(I_sigma0);
};


//=============================================================================
  
CCharString CXYLineSegment::getPrintString(const CCharString &heading, double dx, 
									       double dy, int width, int precision) const
{
	ostrstream oss;
	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(precision);
  
	if (heading.IsEmpty()) oss << "SEGMENT: ";
	else oss << heading.GetChar();
	if (heading.IsEmpty()) oss << " P1: ";

	oss.width(width);
	oss << this->startPt.x + dx << " / ";
	oss.width(width);
	oss << this->startPt.y + dy;

	if (heading.IsEmpty()) oss << ", P2: "; else oss << "  ";
	oss.width(width);
	oss << this->endPt.x + dx << " / ";
	oss.width(width);
	oss << this->endPt.y + dy << ", RMS: " << this->sigma0 << ends;
  
	char *z = oss.str();
  	CCharString str(z);
  	delete[] z;
  	return str;
};

//=============================================================================

void CXYLineSegment::print(ostream &ostr, double dx, double dy, int width, 
						   int precision,const CCharString &heading) const
{
	CCharString pr = this->getPrintString(heading, dx, dy, width, precision);
	ostr << pr.GetChar() << endl;
};
	
//=============================================================================

float CXYLineSegment::isIdenticalAndOverlaps(const CXYLineSegment &line, double &overlap) const
{
	overlap = 0.0;	
	float metric1 = fabs(this->containsPoint(line.startPt));
	float metric2 = fabs(this->containsPoint(line.endPt));
	float metric3 = fabs(line.containsPoint(this->startPt));
	float metric4 = fabs(line.containsPoint(this->endPt));
	 
	float metricA = (metric1 < metric2)? metric2:metric1;
	float metricB = (metric3 < metric4)? metric4:metric3;
	float metric  = (metricA < metricB)? metricB:metricA;

	if (metric < 1.0f)
	{
		double d1 = this->getDistanceofBasePointFromStart(line.startPt);
		double d2 = this->getDistanceofBasePointFromStart(line.endPt);
		if (d2 < d1) 
		{
			double dmy = d1; d1 = d2; d2 = dmy;
		}

		if (d2 < 0.0) metric = -metric;
		else 
		{
			double l = this->length();
			if (d1 > l) metric = -metric;
			else
			{
				if (d1 < 0.0) d1 = 0.0;
				if (d2 > l) d2 = l;
				overlap = d2 - d1;
			}
		}
	}
	else metric = -metric;

	return metric;
};

//=============================================================================

double CXYLineSegment::getMatchDist(const CXYLineSegment &segment, double &overlap) const
{
	C2DPoint diff11 = (C2DPoint) segment.startPt - (C2DPoint) this->startPt;
	C2DPoint diff12 = (C2DPoint) segment.endPt   - (C2DPoint) this->startPt;
	C2DPoint dir = this->getDirection();
	C2DPoint segdir = segment.getDirection();

	float inprod11 = (float) dir.innerProduct(diff11);
	float inprod12 = (float) dir.innerProduct(diff12);
	if ((inprod11 < 0.0f) && (inprod12 < 0.0f)) 
	{
		overlap = 0.0;
		if (inprod11 < inprod12) return diff12.getNorm();
		return diff11.getNorm();
	};

	C2DPoint diff21 = (C2DPoint) segment.startPt - (C2DPoint) this->endPt;
	C2DPoint diff22 = (C2DPoint) segment.endPt   - (C2DPoint) this->endPt;
	float inprod21 = (float) dir.innerProduct(diff21);
	float inprod22 = (float) dir.innerProduct(diff22);
	if ((inprod21 > 0.0f) && (inprod22 > 0.0f))
	{
		overlap = 0.0f;
		if (inprod21 < inprod22) return diff21.getNorm();
		return diff22.getNorm();
	};

	C2DPoint p1 = segment.startPt;
	C2DPoint p2 = segment.endPt;
	C2DPoint fp1, fp2;
	C2DPoint pNormal = this->normal;

	if (inprod11 < 0) 
	{
		double det = segdir.x * this->normal.y - segdir.y * this->normal.x;
		double my = -(this->normal.y * diff11.x - this->normal.x * diff11.y) / det;
		p1  = segment.startPt + my * segdir;
		fp1 = this->startPt;
	}
	else if (inprod21 > 0)
	{
		double det = segdir.x * this->normal.y - segdir.y * this->normal.x;
		double my = (-this->normal.y * diff21.x + this->normal.x * diff21.y) / det;
		p1  = segment.startPt + my * segdir;
		fp1 = this->endPt;
	}
	else
	{
		fp1 = this->getBasePoint(p1);
	};

	if (inprod12 < 0) 
	{
		double det = segdir.x * this->normal.y - segdir.y * this->normal.x;
		double my = (-this->normal.y * diff12.x + this->normal.x * diff12.y) / det;
		p2  = segment.endPt + my * segdir;
		fp2 = this->startPt;
	}
	else if (inprod22 > 0)
	{
		double det = segdir.x * this->normal.y - segdir.y * this->normal.x;
		double my = -(this->normal.y * diff22.x - this->normal.x * diff22.y) / det;
		p2 = segment.endPt + my * segdir;
		fp2 = this->endPt;
	}
	else
	{
		fp2 = getBasePoint(p2);
	};

	C2DPoint delta = fp2 - fp1;

	if (delta.innerProduct(dir) < 0.0)
	{
		C2DPoint p = fp1;
		fp1 = fp2; fp2 = p;
		p   = p1;
		p1  = p2;  p2  = p;
	};

	double ip1 = this->normal.innerProduct(p1 - fp1);
	double ip2 = this->normal.innerProduct(p2 - fp2);
	overlap = fp1.getDistance(fp2) / this->length();

	return fabs(0.5 * (ip1 + ip2));
};

//=============================================================================

bool CXYLineSegment::writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
							  double xfactor, double yfactor) const
{
	int lineCode[ 3 ] = {10, 20, 30};

	dxf.setf(ios::fixed);
	dxf.precision(decimals);

	dxf << "  0\nPOLYLINE\n  8\n" << layer << "\n 62\n " << colour
        << "\n 10\n0.0\n 20\n0.0\n 30\n0.0\n 70\n"
		<< "8\n66\n 1\n"
		<< "  0\nVERTEX\n  8\n" << layer << "\n 62\n "  << colour << endl;
	this->startPt.writeDXF( dxf, lineCode, decimals, xfactor, yfactor);

	dxf << "  0\nVERTEX\n  8\n" << layer << "\n 62\n "  << colour << endl;
	this->endPt.writeDXF( dxf, lineCode, decimals, xfactor, yfactor);
  
	dxf << "  0\nSEQEND\n  8\n0"	<< endl;


	return dxf.good();
}

//=============================================================================
