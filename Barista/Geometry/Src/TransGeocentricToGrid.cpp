#include "TransGeocentricToGrid.h"

#include <math.h>
#include "utilities.h"

CTransGeocentricToGrid::CTransGeocentricToGrid(const CReferenceSystem &sysFrom, 															 
											   const CReferenceSystem &sysTo): 
				CTrans3D(sysFrom, sysTo)
{
}

CTransGeocentricToGrid::~CTransGeocentricToGrid()
{
}

void CTransGeocentricToGrid::transform(C3DPoint &out, const C3DPoint &in)
{
    this->systemTo.geocentricToGrid(out, in);
}

void CTransGeocentricToGrid::transform(CXYZPoint &out, const CXYZPoint &in)
{
    this->systemTo.geocentricToGrid(out, in);
}


void CTransGeocentricToGrid::getDerivatives(Matrix &derivatives, const C3DPoint &p) const
{
	this->systemFrom.derivativesGeocentricToGrid(derivatives, p);
};