/** 
 * @class CXYPolyLine
 * CXYPolyLine, class for XYPolyLines 
 *
 * @author Franz Rottensteiner
 * @version 1.0 
 * @date 24-july-2006
 *
 */

#include <math.h>
#include <float.h>
#include "2DPoint.h"
#include "XYContour.h"
#include "XYLineSegment.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

#include "XYPolyLine.h"

CXYPolyLine::CXYPolyLine(void)
{
	this->obsPoints =  &this->points;
}

CXYPolyLine::~CXYPolyLine(void)
{
}


/**
 * copy constructor, contructor which derives from CXYPolyLine
 * @param p
 */

CXYPolyLine::CXYPolyLine(const CXYPolyLine &p)
{
	this->obsPoints =  &this->points;
	this->label = p.label;
	this->isclosed = p.isclosed;
	this->isSelected = p.isSelected;

	for (long u = 0; u < p.getPointCount(); ++u)
	{
		this->addPoint(p.getPoint(u));
	}
}


CXYPolyLine::CXYPolyLine(const GenericPolyLine &p)
{
	this->obsPoints =  &this->points;
	this->label = p.getLabel();
	this->isclosed = p.isClosed();
	this->isSelected = p.getIsSelected();

	for (long u = 0; u < p.getPointCount(); ++u)
	{
		this->addPoint(p.getPoint(u));
	}
}
/**
 * Serialized storage of a CXYPolyLine to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYPolyLine::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("label", this->getLabel().GetChar());

    token = pStructuredFileSection->addToken();
	token->setValue("isclosed", this->isclosed);

    token = pStructuredFileSection->addToken();
	token->setValue("XYpoints", this->points.getFileName()->GetChar());

	CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->points.serializeStore(child);
}

/**
 * Restores a CXYPolyLine from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYPolyLine::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("label"))
			this->label = CLabel(token->getValue());

		if(key.CompareNoCase("isclosed"))
			this->isclosed = token->getBoolValue();
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CXYPointArray"))
        {
            this->points.serializeRestore(child);
        }
	}

}

/**
 * reconnects pointers
 */
void CXYPolyLine::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CXYPolyLine::resetPointers()
{
    CSerializable::resetPointers();
}



/**
 * Flag if CXYPolyLine is modified
 * returns bool
 */
bool CXYPolyLine::isModified()
{
    return this->bModified;
}

void CXYPolyLine::addPoint(CXYPoint newpoint)
{
    this->points.add(newpoint);
}

void CXYPolyLine::closeLine()
{
	if (getPointCount() > 1)
	{
		isclosed = true;
		addPoint(getPoint (0));
	}
};


//=============================================================================

bool CXYPolyLine::contains(const C2DPoint &p) const
{
	bool Contains = true;
	doubles intersections;
	if (!this->isclosed) Contains = false;
	else
	{
		int inters = this->getIntersections(p.x, intersections);

		if (inters < 1) Contains = false;
		else
		{
			int firstI = -1;

			for (unsigned int i = 0; i < intersections.size() && firstI < 0; ++i)
			{
				if (intersections[i] > p.y) firstI = i;
			}
			if (firstI < 0) Contains = false;
			else
			{
				firstI = intersections.size() - firstI;
				if (!(firstI % 2)) Contains = false;
			}
		}
	}

	return Contains;
};


//=============================================================================

long CXYPolyLine::getIntersections(double I_x, doubles &intersections) const
{
	intersections.clear();

	if (this->getPointCount() > 1)
	{
		long index = 0;
		C2DPoint P1;

		if (this->isclosed)
		{
			P1 = this->getPoint(this->getPointCount() - 1);
			index = 0;
		}
		else 
		{
			P1 = this->getPoint(0);
			index = 1;
		}

		for (; index < this->getPointCount(); ++index)
		{
			C2DPoint P2 = this->getPoint(index);
			double xn = I_x;

			if ((I_x == P1.x) || (I_x == P2.x)) xn -= 10.0f * FLT_EPSILON;
			if (((xn <= P1.x) && (xn >= P2.x)) || ((xn >= P1.x) && (xn <= P2.x)))
			{
				double y  = P1.y;
				double dx = P2.x - P1.x;
				if (fabs(dx) > FLT_EPSILON) y = P1.y + (P2.y - P1.y) * (xn - P1.x) / dx;
				intersections.push_back(y);
			};
			P1 = P2;
		};
  
		if (intersections.size() > 1) intersections.sort();
	};

   
	return intersections.size();
};

//=============================================================================

bool CXYPolyLine::writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
						   double xfactor, double yfactor) const
{
	int lineCode[ 3 ] = {10, 20, 30};

	dxf.setf(ios::fixed);
	dxf.precision(decimals);

	dxf << "  0\nPOLYLINE\n  8\n" << layer << "\n 62\n " << colour
        << "\n 10\n0.0\n 20\n0.0\n 30\n0.0\n 70\n";

	if (this->isclosed) dxf << "9\n";
	else dxf << "8\n"	;
  	dxf << "66\n 1\n";

	for (int i = 0; i < this->getPointCount(); ++i)
	{
		CXYPoint *p = this->points.getAt(i);
		dxf << "  0\nVERTEX\n  8\n" << layer << "\n 62\n "  << colour << endl;
    
    
		p->writeDXF( dxf, lineCode, decimals, xfactor, yfactor);
	}
  
	dxf << "  0\nSEQEND\n  8\n0"	<< endl;


	return dxf.good();
}

//=============================================================================

//@ todo CTrans3D has to bo finished first
/*
CXYPolyLine CXYPolyLine::transform(CTrans3D trans)
{
    CXYPolyLine newline;

    for (int i = 0; i < this->points->GetSize(); i++)
    {
        newline.addPoint(trans.transform(this->getPoint(i)));
    }

    return newline;
}
*/
	
void CXYPolyLine::initFromContour(CXYContour *contour, double I_maxDist,
	                              double I_sigma0)
{
	if (contour->getPointCount() > 1)
	{	 
		I_maxDist *= I_maxDist;
		vector < unsigned char > activityVector(contour->getPointCount(), 0);

// Start with Douglas-Peucker Algorithm: recursive splitting of the contour.
// activityVector[i] tells whether the contour point at position i "survives" or not.

		if ((contour->getPointCount()) &&  (activityVector.size()))
		{
			activityVector[ 0 ] = activityVector[ activityVector.size() - 1 ] = 1;
			unsigned long maxIndex = 0;
			float maxDist = 0;

			if (contour->isclosed)
			{
				// Search for the two initial points on the contour if the contour is closed.

				long j;
				for (j = 1; j < contour->getPointCount(); ++j)
				{
					float dist = (float) contour->getPoint(0)->getDistance(*(contour->getPoint(j)));
					if (dist > maxDist )
					{
						maxDist = dist;
						maxIndex = j;
					};
				};
				if (j == contour->getPointCount()) j = contour->getPointCount() / 2;
			};

			do
			{
// Douglas-Peucker Algorithm: recursive splitting of the contour.
				activityVector[ maxIndex ] = 1;
				unsigned long startIndex = 0;
				unsigned long endIndex = 0;
				unsigned long lastIndex = activityVector.size() - 1;
				maxIndex = 0;
				maxDist = 0;

				while ( startIndex < lastIndex )
				{
					endIndex = startIndex + 1;
					while (!activityVector[endIndex]) ++endIndex;

					C2DPoint polyDir = *(contour->getPoint(startIndex)) - 
						               *(contour->getPoint(endIndex));
					polyDir.normalise();
					C2DPoint startPoint = *(contour->getPoint(startIndex));
					
					for (unsigned long i = startIndex + 1; i < endIndex; ++i )
					{
						C2DPoint currentPoint = *(contour->getPoint(i)); 
						C2DPoint basePoint = startPoint - 
							                 polyDir.innerProduct(startPoint - currentPoint) * polyDir;
						float dist = (float) basePoint.getSquareDistance(currentPoint);

						if (dist > maxDist)
						{
							maxDist = dist;
							maxIndex = i;
						};
					};

					startIndex = endIndex;
				};
			} while ( maxDist > I_maxDist );
     
			// compute adjusting lines through the original contour points
			approximate(contour, activityVector, I_sigma0);  
		};
	};
};
		
int CXYPolyLine::mergeSegments(CXYContour *contour,                       
							   vector <unsigned long> &indicesFrom,
							   vector <unsigned long> &indicesTo,
							   vector <CXYLineSegment> &segmentVector,
							   unsigned long &segmentCount,
							   double I_sigma0)
{
	int changed = 0;
	unsigned long mostSimilarIndex = 0;
    float mostSimilar = FLT_MAX;
	for (unsigned long j = 1; j < segmentCount; ++j)
	{
		float similar = fabs(segmentVector[j].isIdentical(segmentVector[j - 1]));
		if (similar < mostSimilar) 
		{
			mostSimilarIndex = j;
			mostSimilar = similar;
		};
	};
	if ((mostSimilar >= 0.0f) && (mostSimilar <= 1.0f))
	{
		indicesTo[mostSimilarIndex - 1] = indicesTo[mostSimilarIndex];
		segmentVector[mostSimilarIndex - 1].initFromContour(contour, indicesFrom[mostSimilarIndex - 1], indicesTo[mostSimilarIndex - 1], I_sigma0);
        for (unsigned long j = mostSimilarIndex + 1; j < segmentCount; ++j)
		{
			indicesTo[j - 1]     = indicesTo[j];
			indicesFrom[j - 1]   = indicesFrom[j];
			segmentVector[j - 1] = segmentVector[j];
		};
		--segmentCount;
		changed = 1;
	};

	for (unsigned long i = 1; i < segmentCount; ++i)	
	{
		CXYPoint intersectionPoint;
		if (segmentVector[i].getIntersection(segmentVector[i-1], intersectionPoint) == eIntersection)
		{
			double newLength =  segmentVector[i-1].getStartPt().distanceFrom(&intersectionPoint);
			double distDiff = newLength - segmentVector[i-1].length();
			if (fabs(distDiff) > 2.0f)
			{
				C2DPoint start = segmentVector[i-1].getStartPt();
				C2DPoint dir   = segmentVector[i-1].getDirection();
				C2DPoint normal = segmentVector[i-1].getNormal();

				unsigned long jmin, jmax;

				if (distDiff > 0.0f)
				{
					jmin = indicesTo[i - 1]; 
					jmax = indicesTo[i];
				}
				else
				{						
					jmin = indicesFrom[i - 1]; 
					jmax = indicesTo[i - 1];
				};
			
				for (unsigned long j = jmin; j <= jmax; ++j)	
				{					
					C2DPoint diffPt = *(contour->getPoint(j)) - start;
					if ((dir.innerProduct(diffPt) < newLength) && (fabs(normal.innerProduct(diffPt)) < 1.0f))
					{
						indicesTo[i - 1] = j;
						indicesFrom[i]   = j;
					}
					else j = jmax;
				};

				unsigned long removeIdx = segmentCount + 1;
				if (indicesTo[i - 1] - indicesFrom[i - 1] < 1) removeIdx = i - 1;
				 else segmentVector[i - 1].initFromContour(contour, indicesFrom[i - 1], indicesTo[i - 1], I_sigma0);
				if (indicesTo[i] - indicesFrom[i] < 1) removeIdx = i;
				 else segmentVector[i].initFromContour(contour, indicesFrom[i], indicesTo[i], I_sigma0);
					 
				if (removeIdx < segmentCount)
				{
					for (unsigned long j = removeIdx + 1; j < segmentCount; ++j)
					{
						indicesTo[j - 1]     = indicesTo[j];
						indicesFrom[j - 1]   = indicesFrom[j];
						segmentVector[j - 1] = segmentVector[j];
					};
					--segmentCount;
				};

				changed = 1;
			};
		};	
	};

	return changed;
};

void CXYPolyLine::approximate(CXYContour *contour,
                              const vector <unsigned char> &activityVector,
							  double I_sigma0)
{
	vector <unsigned long> indicesFrom(contour->getPointCount());
	vector <unsigned long> indicesTo(contour->getPointCount());
	indicesFrom[0] = 0;
	unsigned long segmentCount = 0;

	for (long i = 1; i < contour->getPointCount(); ++i)
	{
		if (activityVector[i])
		{
         indicesTo[segmentCount] = i;
		 segmentCount++;
		 indicesFrom[segmentCount] = i;
		}
	};

	indicesFrom.resize(segmentCount);
	indicesTo.resize(segmentCount);

	vector <CXYLineSegment> segmentVector(segmentCount);

	for (unsigned long i = 0; i < segmentCount; ++i)
	{
		segmentVector[i].initFromContour(contour, indicesFrom[i], indicesTo[i], I_sigma0);
	};

	int changed = 1;

	int nChanges = -1;
	while ((changed) && (segmentCount > 1) && (nChanges < 20))
	{
		changed = mergeSegments(contour, indicesFrom, indicesTo, 
			                    segmentVector, segmentCount, I_sigma0);
		nChanges += changed;
	};
	
    CLabel PointLabel("L");
	PointLabel += this->label.GetNumber();
    PointLabel += "P";
    PointLabel += 0;
    
    CXYPoint currentPoint = segmentVector[0].getStartPt();
    bool addLastIfClosed = false;
    if (contour->isclosed)
	{
		if (segmentVector[0].getIntersection(segmentVector[segmentCount-1], currentPoint) != eIntersection)
		{
         addLastIfClosed = true;
         currentPoint = segmentVector[0].getStartPt();
		};
	};

	currentPoint.setLabel(PointLabel);
	this->addPoint(currentPoint);

	++PointLabel;

	for (unsigned long i = 1; i < segmentCount; ++i)
	{
	
		if (segmentVector[i].getIntersection(segmentVector[i-1], currentPoint) == eIntersection)
		{
			if ((segmentVector[i - 1].getDirection().innerProduct((C2DPoint)currentPoint - segmentVector[i - 1].getStartPt()) < 0.0f) ||
				(segmentVector[i].getDirection().innerProduct((C2DPoint)currentPoint - segmentVector[i].getEndPt()) > 0.0f))
			{

				currentPoint = segmentVector[i - 1].getEndPt();
				currentPoint.setLabel(PointLabel);
				this->addPoint(currentPoint);
				++PointLabel;

				currentPoint = segmentVector[i].getStartPt();
			}

			currentPoint.setLabel(PointLabel);
			this->addPoint(currentPoint);
			++PointLabel;
		}
		else
		{
			currentPoint = segmentVector[i].getEndPt();
			currentPoint.setLabel(PointLabel);
			this->addPoint(currentPoint);
			++PointLabel;
			currentPoint = segmentVector[i - 1].getStartPt();
			currentPoint.setLabel(PointLabel);
			this->addPoint(currentPoint);
			++PointLabel;
		};   
	};
  
	if (contour->isclosed) 
	{
		if (addLastIfClosed) 
		{
			currentPoint = segmentVector[segmentCount - 1].getEndPt();
			currentPoint.setLabel(PointLabel);
			this->addPoint(currentPoint);
		};
		this->closeLine();
	}
	else
	{
		currentPoint = segmentVector[segmentCount - 1].getEndPt();
		currentPoint.setLabel(PointLabel);
		this->addPoint(currentPoint);
	};
};

