#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

#include "XYZPointArrayArray.h"

CXYZPointArrayArray::CXYZPointArrayArray(int nGrowBy) : 
	CMUObjectArray<CXYZPointArray, CXYZPointArrayPtrArray>(nGrowBy)
{
}

CXYZPointArrayArray::~CXYZPointArrayArray(void)
{
}

void CXYZPointArrayArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    for (int i = 0; i < this->GetSize(); i++)
    {
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();

        CXYZPointArray* xyzs = this->GetAt(i);
        xyzs->serializeStore(newSection);
    }
}
void CXYZPointArrayArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();

    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);

        CXYZPointArray* xyz = this->Add();
        xyz->serializeRestore(child);
    }
}

void CXYZPointArrayArray::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

void CXYZPointArrayArray::resetPointers()
{
    CSerializable::resetPointers();
}

bool CXYZPointArrayArray::isModified()
{
    return false;
}

CXYZPointArray* CXYZPointArrayArray::getPointArray(int i) const
{
    return this->GetAt(i);
}

/** SLOW FOR LOTS OF POINTArrayArrays!
 * Gets a pointarray by label (null if not found)
 *
 * @param CCharString name of pointarray
 */
CXYZPointArray* CXYZPointArrayArray::getPointArray(CCharString name) const
{
    CXYZPointArray* ptArr = NULL;

    for (int i = 0; (i < this->GetSize()) && !ptArr; i++)
    {
        CXYZPointArray* testPointArray = this->GetAt(i);

        if (name.CompareNoCase(testPointArray->getItemText().GetChar()))
            ptArr = testPointArray;
    }

    return ptArr;
}


/** 
 * Gets index by name (null if not found)
 *
 * @param CCharString name of pointarray
 */
int CXYZPointArrayArray::getbyName(CCharString name)
{
    CXYZPointArray* testPointArray = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        testPointArray = this->GetAt(i);

        if (name.CompareNoCase(testPointArray->getFileName()->GetChar()))
            return i;

        continue;
    }

    return -1;
}