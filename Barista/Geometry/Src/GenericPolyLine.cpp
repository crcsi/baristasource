#include "GenericPolyLine.h"
#include <math.h>
#include <float.h>

GenericPolyLine::GenericPolyLine(void): isclosed(false),isSelected(false),obsPoints(0)
{

}

GenericPolyLine::~GenericPolyLine(void)
{

}

CLabel GenericPolyLine::getLabel() const
{
	 return this->label;
}
void GenericPolyLine::setLabel (CLabel label)
{
    this->bModified = true;
    this->label = label;
}



double GenericPolyLine::getLength() const
{ 
	double length = 0;
	int size = this->obsPoints->GetSize();

	for ( int n = 0; n < size-1; n++)
	{
		CObsPoint* p1 = this->obsPoints->GetAt(n);
		CObsPoint* p2 = this->obsPoints->GetAt(n+1);
		length += p1->distanceFrom(p2);
	};

    return length;
}


CObsPoint &GenericPolyLine::getPoint (int index)
{
	return *this->obsPoints->GetAt(index);
}

const CObsPoint &GenericPolyLine::getPoint (int index) const
{
	return *this->obsPoints->GetAt(index);
}


int GenericPolyLine::getPointCount () const
{
    return this->obsPoints->GetSize();
}



double GenericPolyLine::getArea() const
{
	double area = 0.0f; 

	if (this->isclosed)
	{
		CObsPoint pred(this->getPoint(0));
		CObsPoint p1(this->getPoint(this->getPointCount() - 1));
		p1 -= pred;
		CObsPoint p2(p1.getDimension());
  
		for (long u = 0; u < this->getPointCount(); ++u)
		{
			p2 = this->getPoint(u) - pred;
			double yy = p2[1] + p1[1];
			double dx = p2[0] - p1[0];
			double dArea = yy * dx;
			area += dArea;
			p1 = p2;
		};  
	};

  
	return area / double(2.0f);
};

double GenericPolyLine::getAreaAbs() const 
{ 
	return fabs(getArea()); 
};

void GenericPolyLine::deleteLine()
{
    this->obsPoints->RemoveAll();
}


 ePOLYLINEORDER GenericPolyLine::getOrder() const
 {
	 double area = getArea();
	 if (area < 0) return eCOUNTERCLOCKWISE;
	 if (area > 0) return eCLOCKWISE;

	 return eUNDEFINEDORDER;
 };
	
void GenericPolyLine::removePoints(int indexFrom, int indexTo)
{
	int nCount = indexTo - indexFrom + 1;
	this->obsPoints->RemoveAt(indexFrom, nCount);
};

void GenericPolyLine::removePoint(int i)
{
    // check bounds
    if ( (i < 0) || (i > this->obsPoints->GetSize()))
        return;

    this->obsPoints->RemoveAt(i);
}



void GenericPolyLine::reversePolyLine()
{
	this->obsPoints->revert();
};



