#include <math.h>
#include "newmat.h"
#include "RollPitchYawRotation.h"

#include "ObsPoint.h"
#include "StructuredFileSection.h"
#include "QuaternionRotation.h"


CQuaternionRotation::CQuaternionRotation(const CPointBase* p_rho): CRotationBase(4,Quaternion),type(eQQuickBird)
{
	this->updateAngles_Rho(p_rho);
}

CQuaternionRotation::CQuaternionRotation(const CPointBase* p_rho,QuaternionTypes type):CRotationBase(4,Quaternion),type(type)
{
	this->updateAngles_Rho(p_rho);
}

CQuaternionRotation::CQuaternionRotation(const CRotationMatrix &rotMat): CRotationBase(4,Quaternion),type(eQQuickBird)
{
	this->updateRotMat(rotMat);
}

void CQuaternionRotation::updateAngles_Rho(const CPointBase* angles)
{
	assert( angles->getDimension() == this->dim);
	
	double length = sqrt((*angles)[0]*(*angles)[0] + (*angles)[1]*(*angles)[1] + (*angles)[2]*(*angles)[2] + (*angles)[3]*(*angles)[3]);

	for (int i=0; i < this->dim; i++)
		this->coordinates[i] = ((*angles)[i])/length;

	this->computeRotMatFromParameter();
}


void CQuaternionRotation::updateRotMat(const CRotationMatrix &rotMat)
{

	this->rotMat = rotMat;

	this->computeParameterFromRotMat();
}


void CQuaternionRotation::computeRotMatFromParameter()
{
	double q0q0 = this->coordinates[0]*this->coordinates[0];
	double q1q1 = this->coordinates[1]*this->coordinates[1];
	double q2q2 = this->coordinates[2]*this->coordinates[2];
	double q3q3 = this->coordinates[3]*this->coordinates[3];

	double q0q1 = this->coordinates[0]*this->coordinates[1];
	double q0q2 = this->coordinates[0]*this->coordinates[2];
	double q0q3 = this->coordinates[0]*this->coordinates[3];

	double q1q2 = this->coordinates[1]*this->coordinates[2];
	double q1q3 = this->coordinates[1]*this->coordinates[3];

	double q2q3 = this->coordinates[2]*this->coordinates[3];

	if (this->type == eQQuickBird)
	{
		/*Ours
		this->rotMat(0) = 1 - 2*(q2q2 + q3q3);
		this->rotMat(1) = 2*(q1q2 - q0q3);
		this->rotMat(2) = 2*(q0q2 + q1q3);

		this->rotMat(3) = 2*(q1q2 + q0q3);
		this->rotMat(4) = 1 - 2*(q1q1 + q3q3);
		this->rotMat(5) = 2*(q2q3 - q0q1);

		this->rotMat(6) = 2*(q1q3 - q0q2);
		this->rotMat(7) = 2*(q0q1 + q2q3);
		this->rotMat(8) = 1 - 2*(q1q1 + q2q2);	
		*/


		///* Thomas
		this->rotMat(0) = 2*(q0q0 + q3q3)-1;
		this->rotMat(1) = 2*(q0q1 + q2q3);
		this->rotMat(2) = 2*(q0q2 - q1q3);

		this->rotMat(3) = 2*(q0q1 - q2q3);
		this->rotMat(4) = 2*(q1q1 + q3q3)-1;
		this->rotMat(5) = 2*(q1q2 + q0q3);

		this->rotMat(6) = 2*(q0q2 + q1q3);
		this->rotMat(7) = 2*(q1q2 - q0q3);
		this->rotMat(8) = 2*(q2q2 + q3q3)-1;	
		//*/
	}
	else if (this->type == eQALOS)
	{
		this->rotMat(0) = 1 - 2*(q1q1 + q2q2);
		this->rotMat(1) =     2*(q0q1 - q2q3);
		this->rotMat(2) =     2*(q0q2 + q1q3);

		this->rotMat(3) =     2*(q0q1 + q2q3);
		this->rotMat(4) = 1 - 2*(q0q0 + q2q2);
		this->rotMat(5) =     2*(q1q2 - q0q3);
		
		this->rotMat(6) =     2*(q0q2 - q1q3);
		this->rotMat(7) =     2*(q1q2 + q0q3);
		this->rotMat(8) = 1 - 2*(q0q0 + q1q1);

	}
	else if (this->type == eGeoEye1)
	{
		this->rotMat(0) = q0q0 - q1q1 - q2q2 + q3q3;
		this->rotMat(1) = 2*(q0q1 + q2q3);
		this->rotMat(2) = 2*(q0q2 - q1q3);

		this->rotMat(3) = 2*(q0q1 - q2q3);
		this->rotMat(4) = -q0q0 + q1q1 - q2q2 + q3q3;
		this->rotMat(5) = 2*(q1q2 + q0q3);

		this->rotMat(6) = 2*(q0q2 + q1q3);
		this->rotMat(7) = 2*(q1q2 - q0q3);
		this->rotMat(8) = -q0q0 - q1q1 + q2q2 + q3q3;;	

	}
}

bool CQuaternionRotation::computeParameterFromRotMat()
{
	
	assert(false);
	return true;
}


void CQuaternionRotation::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CCharString valueStr;
	CCharString elementStr;

	CToken* pNewToken = pStructuredFileSection->addToken();

	for (int i = 0; i < 4; i++)
		{
			double element = this->coordinates[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	pNewToken->setValue("quaternionParameter", valueStr.GetChar());	

	valueStr.Empty();
	elementStr.Empty();	

	pNewToken = pStructuredFileSection->addToken();
	for (int i=0; i< this->dim; i++)
	{
		for (int k=0; k< this->dim; k++)
		{
			double element = this->covariance->element(i,k);
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}
	}

	pNewToken->setValue("covariance", valueStr.GetChar());	

}

void CQuaternionRotation::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	double tempQ[3];

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		CCharString key = token->getKey();

		if (key.CompareNoCase("quaternionParameter"))
		{
			token->getDoubleValues(tempQ,3);
		}
		else if (key.CompareNoCase("covariance"))
		{
			double* tmpArray = new double [this->dim*this->dim];
			token->getDoubleValues(tmpArray,this->dim*this->dim);

			if(this->covariance)
				delete this->covariance;
			
			this->covariance = new Matrix(this->dim,this->dim);
			
			for (int i=0; i< this->dim; i++)
			{
				for (int k=0; k< this->dim; k++)
				{
					this->covariance->element(i,k) = tmpArray[i*this->dim+k];
				}	 
			}

			if (tmpArray) delete [] tmpArray;
		}

	}
	
	// use a temp Point and then updateAngles_Rho() to have only one init interface
	CObsPoint tempPoint(4);
	tempPoint[0] = tempQ[0];
	tempPoint[1] = tempQ[1];
	tempPoint[2] = tempQ[2];
	tempPoint[3] = tempQ[3];
	this->updateAngles_Rho(&tempPoint);
	
}


void CQuaternionRotation::computeDerivation(CPointBase& deriv,const int parameter)const
{

}

void CQuaternionRotation::computeDerivation(CAdjustMatrix& derivMat) const
{
}

