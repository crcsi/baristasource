/**
 * @class CXYPointArray
 * an array of xy points 
 */
#include "math.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
#include "XYPoint.h"
#include "XYPointArray.h"
#include <algorithm>

CXYPointArray::CXYPointArray(int nGrowBy) : CObsPointArray(2,nGrowBy), isActive(true), type(NOTYPE)
{
    this->FileName = "2DPoints";
}

CXYPointArray::CXYPointArray(xypointtype type) : CObsPointArray(2), isActive(true), type(type)
{
    this->FileName = "2DPoints";
}


CXYPointArray::CXYPointArray(const CXYPointArray* src) : CObsPointArray(2), isActive(src->isActive), type(src->type)
{
	this->FileName = src->FileName;
	for (int i = 0; i < src->GetSize(); ++i)
	{
		CXYPoint *p = src->getAt(i);
		this->add(*p);
	}
}

CXYPointArray &CXYPointArray::operator = (const CXYPointArray & xyArray)
{
	this->isActive = xyArray.isActive;
	this->FileName = xyArray.FileName;
	this->type = xyArray.type;
	this->RemoveAll();

	for (int i = 0; i < xyArray.GetSize(); ++i)
	{
		CObsPoint *p = (CObsPoint*) xyArray.getAt(i);
		this->add(*p);
	}

	return *this;
}


CXYPointArray::CXYPointArray(const CXYPointArray &src) : CObsPointArray(2), isActive(src.isActive), type(src.type)
{
	this->FileName = src.FileName;
	for (int i = 0; i < src.GetSize(); ++i)
	{
		CObsPoint *p = (CObsPoint*)src.getAt(i);
		this->add(*p);
	}
}



CXYPointArray::~CXYPointArray(void)
{
}


/**
 * Gets ClassName
 * returns a CCharString
 */
string CXYPointArray::getClassName() const
{
    return string( "CXYPointArray");
}

bool CXYPointArray::isa(string& className) const
{
    if (className == "CXYPointArray")
        return true;

    return false;
}


/**
 * reads XYPointArray from a TextFile, input of
 * label x y and label x y sx sy is possible
 * @param cfilename
 * @returns true/false
 */
bool CXYPointArray::readXYPointTextFile(CFileName cfilename)
{
 	FILE* in = fopen(cfilename.GetChar(),"r");
	if (!in )
		return false;

	char delimiters[4] = {' ',',',';','\t'};
	char string [1000];

    for (;;)
    {
        if (!fgets(string,999,in))//!file.ReadLine(str))
        break;

		int n = 0;
		while ( string[n] != 0 )
		{
			for ( int k = 0; k < 4; k++)
			{
				if ( string[n] == delimiters[k] )
				{
					// replace all types of delimters with ' '
					string[n] = delimiters[0];
					break;
				}
			}
			n++;
		}
		
		int delimiterCount = 0;
		bool onDilimiter = false;

		for ( int i = 0; i < n; i++ )
		{
			if ( string[i] == delimiters[0] )
			{
				if ( !onDilimiter) delimiterCount++;
				onDilimiter = true;
			}
			else onDilimiter = false;
		}

		// if line starts with delimiters, ignore first delimiter
		if ( string[0] == delimiters[0] ) delimiterCount--;

		// if line ends with delimiters, ignore last delimiter
		if ( string[n-2] == delimiters[0] ) delimiterCount--;

        if ( (delimiterCount == 2) ) // assume Label x y, setting sx=sy=0.5
        {
            char buf[1024];
            double x, y;

            ::sscanf(string, "%s%lf%lf", buf, &x, &y);

            CCharString tmp = "";

            tmp += buf;
            CLabel label(tmp);

			CObsPoint* point = this->Add();

            point->setLabel(label);
            (*point)[0] = x;
            (*point)[1] = y;
			point->setCovarElement(0,0,1.0);
            point->setCovarElement(1,1,1.0);

        }

        if ( (delimiterCount == 4) ) // assume Label x y sx sy
        {
            char buf[1024];
            double x, y, sx, sy;

            ::sscanf(string, "%s%lf%lf%lf%lf", buf, &x, &y, &sx, &sy);

            CCharString tmp = "";

            tmp += buf;
            CLabel label(tmp);

			CObsPoint* point = this->Add();

            point->setLabel(label);
            (*point)[0] = x;
            (*point)[1] = y;
			point->setCovarElement(0,0,sx);
            point->setCovarElement(1,1,sy);

        } 
    }

    this->FileName = cfilename;

	this->informListeners_elementsChanged();

	fclose(in);

    return true;
}

/**
 * writes XYPointArray to a TextFile, with 
 * switch for space/csv separation, csv = 0, space = 1
 * @param cfilename type
 * @returns true/false
 */
bool CXYPointArray::writeXYPointTextFile(CFileName cfilename, int type)
{
    FILE	*fp;
	fp = fopen(cfilename.GetChar(),"w");

    for (int i = 0; i < this->GetSize(); i++)
    {           
        const char* buf;
        double x, y, sx, sy;

        CObsPoint* obsPoint = this->GetAt(i);

        buf = obsPoint->getLabel().GetChar();
        x = (*obsPoint)[0];
        y = (*obsPoint)[1];
        sx = obsPoint->getCovarElement(0,0);
        sy = obsPoint->getCovarElement(1,1);

        if ( type == 1 ) // switch for space/csv separation, csv = 0, space = 1
        {
            ::fprintf(fp, "%s %f %f %f %f\n",  buf, x, y, sx, sy);
        }
        else if ( type == 0 )
        {
            ::fprintf(fp, "%s,%f,%f,%f,%f\n",  buf, x, y, sx, sy);
        }
   

    }

	fclose(fp);

    return true;
}

/**
 * calculates mean with rms of a XYPointArray 
 * @returns CXYPoint
 */
CXYPoint CXYPointArray::getMean() const
{
    CXYPoint MeanPoint;
    MeanPoint.setLabel("Mean");

    int size = this->GetSize();
    double x = 0;
    double y = 0;

    for (int i = 0; i < size; i++)
    {
        CObsPoint* NextPoint = this->GetAt(i);
        x += (*NextPoint)[0];
        y += (*NextPoint)[1]; 
    }

    MeanPoint.setX( x / (double)size);
    MeanPoint.setY( y / (double)size);
   
    // Calculate variances
    double dx = 0;
    double dy = 0;
   
    for (int i = 0; i < size; i++)
    {
        CObsPoint* NextPoint = this->GetAt(i);
        dx += (MeanPoint.getX() - (*NextPoint)[0]) * (MeanPoint.getX() - (*NextPoint)[0]);
        dy += (MeanPoint.getY() - (*NextPoint)[1]) * (MeanPoint.getY() - (*NextPoint)[1]);
    }

   
    dx /= (double)size;
    dy /= (double)size;

    // Set the sigmas of Meanpoint
    dx = sqrt(dx);
    dy = sqrt(dy);

    MeanPoint.setSX(dx);
    MeanPoint.setSY(dy);

    return MeanPoint;
}

/** 
 * Determine differences of merging labels from two CXYPointArrays
 *
 * @param CXYPointArray* PointArray, CXYPointArray* comparepoints
 */
void CXYPointArray::differenceByLabel(CXYPointArray* differences, CXYPointArray* comparepoints)
{
	differences->setPointType(DIFFERENCED);

    double rmsx = 0;
    double rmsy = 0;

    double x = 0;
    double y = 0;

    int count = 0;

    CObsPoint* tempPoint1 = NULL;
    CObsPoint* tempPoint2 = NULL;
    CCharString tempLabel = "";

    for (int i = 0; i < this->GetSize(); i++)
    {
        tempPoint1 = this->GetAt(i);

		if (!tempPoint1->getStatusBit(0))
			continue;


        tempLabel = tempPoint1->getLabel().GetChar();
        tempPoint2 = comparepoints->getPoint(tempLabel);

        if (tempPoint2 == NULL || !tempPoint2->getStatusBit(0))
            continue;

        x = (*tempPoint1)[0] - (*tempPoint2)[0];
        y = (*tempPoint1)[1] - (*tempPoint2)[1];

		CObsPoint* newPoint = differences->Add();
		(*newPoint)[0] = x;
		(*newPoint)[1] = y;
		newPoint->setLabel(tempLabel.GetChar());

        rmsx += (x * x);
        rmsy += (y * y);

        count ++;
    }

    rmsx /= (double)count;
    rmsx = sqrt(rmsx);
    rmsy /= (double)count;
    rmsy = sqrt(rmsy);

	
    CObsPoint* RMS = differences->Add();
    RMS->setLabel("RMS");
    (*RMS)[0] = rmsx;
    (*RMS)[1] = rmsy;
}

void CXYPointArray::differenceByIndex(CXYPointArray* differences, CXYPointArray* comparepoints)
{
	differences->setPointType(DIFFERENCED);

    double rmsx = 0;
    double rmsy = 0;

    int size = min(this->GetSize(), comparepoints->GetSize());

    for (int i = 0; i < size; i ++)
    {
        CObsPoint *p1 = this->GetAt(i);
        CObsPoint *p2 = comparepoints->GetAt(i);

        CXYPoint diff(p1);

        diff.setX( (*p1)[0] - (*p2)[0]);
        diff.setY( (*p1)[1] - (*p2)[1]);

        rmsx += (diff.getX() * diff.getX());
        rmsy += (diff.getY() * diff.getY());

        differences->Add(diff);
    }

    rmsx /= (double)size;
    rmsx = sqrt(rmsx);

    rmsy /= (double)size;
    rmsy = sqrt(rmsy);

    CXYPoint RMS;
    RMS.setLabel("RMS");
    RMS.setX( rmsx);
    RMS.setY( rmsy);
    differences->Add(RMS);
}


/**
 * Serialized storage of a CXYPointArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYPointArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("xypoint filename", this->FileName.GetChar());
	
	token = pStructuredFileSection->addToken();
	token->setValue("isActive", this->getActive());

    token = pStructuredFileSection->addToken();
	token->setValue("Type", this->getPointType());

    for (int i = 0; i < this->GetSize(); i++)
    {
        CXYPoint* point = this->getAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        point->serializeStore(newSection);
    }
}

/**
 * Restores a CXYPointArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYPointArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("xypoint filename"))
            this->FileName = token->getValue();

		if(key.CompareNoCase("isActive"))
			this->setActive(token->getBoolValue());
	
		if(key.CompareNoCase("Type"))
			this->setPointType((xypointtype)token->getIntValue());

    }

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();
    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);
		CXYPoint* point = this->add();
        point->serializeRestore(child);
		
        
    }
}


void CXYPointArray::deletebyArea(int xmin, int xmax, int ymin, int ymax)
{
	int size = this->GetSize();

    for (int i = 0; i < size; i++)
    {
		CObsPoint* point = this->GetAt(i);

		double x = (*point)[0];
		double y = (*point)[1];

        if ( xmin < x && x < xmax && ymin < y && y < ymax)
		{
			this->RemoveAt(i, 1);

			this->informListeners_elementsChanged();

			i--;
			size--;
		}
    }
}

void CXYPointArray::getbyArea(int xmin, int xmax, int ymin, int ymax, CXYPointArray* pointsinArea)
{
	int size = this->GetSize();

    for (int i = 0; i < size; i++)
    {
		CObsPoint* point = this->GetAt(i);

		double x = (*point)[0];
		double y = (*point)[1];

        if ( xmin < x && x < xmax && ymin < y && y < ymax)
		{
			pointsinArea->Add(point);
		}
    }
}

void CXYPointArray::setSXSYforAll(double sx, double sy)
{
	for (int i = 0; i < this->GetSize(); i++)
	{

		CObsPoint* point = this->GetAt(i);


		point->setCovarElement(0,0,sx);
		point->setCovarElement(1,1,sy);
	}

    this->informListeners_elementsChanged();
}

CObsPoint* CXYPointArray::CreateType() const 
{
	 return new CXYPoint; 
}

CObsPoint* CXYPointArray::CreateType(const CObsPoint &p) const 
{ 
	return new CXYPoint(p); 
}
