#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
#include "CharStringArray.h"

#include "ObsPointArray.h"

CObsPointArray::CObsPointArray(int iDim, int nGrowBy) :
	CMUObjectArray<CObsPoint, CObsPointPtrArray>(iDim,nGrowBy), dim(iDim), FileName("Points"), isPointSorted(false)
{
}

CObsPointArray::CObsPointArray(const CObsPointArray& src):
CMUObjectArray<CObsPoint, CObsPointPtrArray>(src), dim(src.dim), FileName(src.FileName), isPointSorted(src.isPointSorted)
{
	this->m_PointerArray.setDim(src.dim);
}

CObsPointArray &CObsPointArray::operator = (const CObsPointArray & obsArray)
{
	this->dim =obsArray.dim;
    this->FileName=obsArray.FileName;
    this->isPointSorted= obsArray.isPointSorted;

	this->RemoveAll();

	for (int i = 0; i < obsArray.GetSize(); ++i)
	{
		CObsPoint *p = obsArray.GetAt(i);
		this->Add(*p);
	}

	return *this;
}



CObsPointArray::~CObsPointArray(void)
{
}

CObsPoint* CObsPointArray::Add()
{
	CObsPoint* pPoint = CMUObjectArray<CObsPoint, CObsPointPtrArray>::Add();
	pPoint->setParent(this);

	this->informListeners_elementAdded(this->getClassName().c_str());


	return pPoint;
}

CObsPoint* CObsPointArray::Add(const CObsPoint& point)
{
	CObsPoint* pNewPoint = CMUObjectArray<CObsPoint, CObsPointPtrArray>::Add(point);
	pNewPoint->setParent(this);

    this->informListeners_elementAdded(this->getClassName().c_str()); // inform the listeners that points added into the list

	return pNewPoint;
}

void CObsPointArray::Remove(CObsPoint* pElement)
{
	
	CLabel label = pElement->getLabel();

	CMUObjectArray<CObsPoint, CObsPointPtrArray>::Remove(pElement);

	this->informListeners_elementDeleted(label);
 
}


void CObsPointArray::RemoveAt(int nIndex, int nCount)
{

	CCharStringArray tmpLabels;

	for (int k = nCount -1; k >= 0; k--)
	{
		CLabel label = this->GetAt(nIndex + k)->getLabel();
		CMUObjectArray<CObsPoint, CObsPointPtrArray>::RemoveAt(nIndex + k,1);

		this->informListeners_elementDeleted(label);
	}

}

void CObsPointArray::RemoveAll()
{
	CMUObjectArray<CObsPoint, CObsPointPtrArray>::RemoveAll();

	this->informListeners_elementDeleted(this->getClassName().c_str());
}


void CObsPointArray::resetPointers()
{
	CSerializable::resetPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		CObsPoint* pPointArray = this->GetAt(i);
		pPointArray->resetPointers();
	}
}

bool CObsPointArray::isModified()
{
   for (int i = 0; i < this->GetSize(); i++)
    {
        if (this->GetAt(i)->isModified())
            return true;
    }

    return false;
}

void CObsPointArray::reconnectPointers()
{
	CSerializable::reconnectPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		CObsPoint* pPointArray = this->GetAt(i);
		pPointArray->reconnectPointers();
	}
}


void CObsPointArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("dim", this->dim);

	token = pStructuredFileSection->addToken();
    token->setValue("filename", this->FileName.GetChar());

	CStructuredFileSection* pNewSection;

	for (int i=0; i< this->GetSize(); i++)
	{
		CObsPoint* point = this->GetAt(i);
		pNewSection = pStructuredFileSection->addChild();
		point->serializeStore(pNewSection);
	}

}

void CObsPointArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);

		CCharString key = token->getKey();

		if(key.CompareNoCase("dim"))
			this->dim = token->getIntValue();
		else if(key.CompareNoCase("filename"))
            this->FileName = token->getValue();
	}

	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("CObsPoint"))
		{
			CObsPoint* point = this->Add();
			point->serializeRestore(pChild);
		}
	}
}

CFileName* CObsPointArray::getFileName()
{
    return &this->FileName;
}

CFileName CObsPointArray::getItemText()
{
    CFileName itemtext = this->FileName.GetFullFileName();
    return itemtext;
}

void CObsPointArray::setFileName(const char* filename)
{
    this->FileName = filename;

	this->informListeners_elementsChanged(this->getClassName().c_str());

}

const char *CObsPointArray::getFileNameChar() const
{
    return this->FileName.GetChar();
}


void CObsPointArray::forceSortPoints()
{
    this->isPointSorted = false;
    this->sortByLabel();
	this->isPointSorted = true;
    this->bModified = true;

}



CObsPoint* CObsPointArray::getPoint(int i)const
{
    return this->GetAt(i);
}

/** SLOW FOR LOTS OF POINTS!  --> we have to change that to binary search !!!!!
 * Gets a point by label (null if not found)
 *
 * @param CCharString Label of point
 */
CObsPoint* CObsPointArray::getPoint(CCharString Label)const
{
    CObsPoint* testPoint = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        testPoint = this->GetAt(i);

        if (Label.CompareNoCase(testPoint->getLabel().GetChar()))
            return testPoint;

        continue;
    }

    return NULL;
}

CObsPoint* CObsPointArray::getPoint_binarySearch(const CLabel& label)
{
    CObsPoint* testPoint = NULL;
  
	int low = 0, high = this->GetSize() - 1;
	int pos;

	while ( high >= low ) 
	{
		pos = ( low + high ) / 2;
		testPoint = this->GetAt(pos);
		int test = strcmp(label.GetChar(),testPoint->getLabel().GetChar());
		if ( test < 0)
			high = pos - 1;
		else if ( test > 0)
			low = pos + 1;
		else 
			return testPoint;

	}
	return NULL;
}
	
int CObsPointArray::getPointIndexBinarySearch(const CLabel& label) const
{
	CObsPoint* testPoint = NULL;
  
	int low = 0, high = this->GetSize() - 1;
	int pos;

	while ( high >= low ) 
	{
		pos = ( low + high ) / 2;
		testPoint = this->GetAt(pos);
		int test = strcmp(label.GetChar(),testPoint->getLabel().GetChar());
		if ( test < 0)
			high = pos - 1;
		else if ( test > 0)
			low = pos + 1;
		else 
			return pos;

	}

	return -1;
};

// we have to change here to binary search as well !!!!
void CObsPointArray::deletebyLabel(CCharString Label)
{
    CObsPoint* testPoint = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        testPoint = this->GetAt(i);

        if (Label.CompareNoCase(testPoint->getLabel().GetChar()))
			return this->RemoveAt(i, 1);

        continue;
    }
}







void CObsPointArray::update()
{
    this->informListeners_elementsChanged(this->getClassName().c_str());

}

void CObsPointArray::revert()
{
	this->m_PointerArray.revert();
	this->isPointSorted = false;
};

void CObsPointArray::setStauts(short status)
{
	for (int k=0; k< this->GetSize(); k++)
	{
		this->GetAt(k)->setStatus(status);
	}

    this->informListeners_elementsChanged(this->getClassName().c_str());
}