#ifdef WIN32

#include "BuildingPtrArray.h"

CBuildingPtrArray::CBuildingPtrArray(int nGrowBy):
	CMUObjectPtrArray<CBuilding>(nGrowBy)
{
}

CBuildingPtrArray::~CBuildingPtrArray(void)
{
}

#endif // WIN32
