#include "PrimitiveDescriptor.h"


CPrimitiveDescriptor::CPrimitiveDescriptor(PrimitiveDescriptorType type,int nPoints,CCharString descriptorName): 
	type(type),roofPoints(nPoints),floorPoints(nPoints), parentDescriptor(-1),descriptorName(descriptorName)
{
}

CPrimitiveDescriptor::CPrimitiveDescriptor(const CPrimitiveDescriptor& src):
	type(src.type), roofPoints(src.roofPoints),	floorPoints(src.floorPoints),
	parentDescriptor(src.parentDescriptor),
	descriptorName(src.descriptorName)
{
}


CPrimitiveDescriptor::~CPrimitiveDescriptor(void)
{
}

bool CPrimitiveDescriptor::containsRoofPoint(int PointId) const
{
	for (unsigned int j = 0; j < this->roofPoints.size(); ++j)
	{
		if (this->roofPoints[j] == PointId) return true;
	}

	return false;
};

CPrimitiveDescriptor &CPrimitiveDescriptor::operator = (const CPrimitiveDescriptor & src)
{
	this->type = src.type;
	this->parentDescriptor = src.parentDescriptor;
	this->roofPoints.resize(src.roofPoints.size());
	this->floorPoints.resize(src.floorPoints.size());
	this->descriptorName = src.descriptorName;

	for (unsigned int i=0; i < this->roofPoints.size(); i++)
	{
		this->roofPoints[i] = src.roofPoints[i];
		this->floorPoints[i] = src.floorPoints[i];
	}
	return *this;
}


void CPrimitiveDescriptor::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CCharString valueStr;
	CCharString elementStr;

	CSerializable::serializeStore(pStructuredFileSection);

	valueStr = "";

	CToken* token = pStructuredFileSection->addToken();
    token->setValue("descriptorName", this->descriptorName.GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("type", this->type);

    token = pStructuredFileSection->addToken();
    token->setValue("parentDescriptor", this->parentDescriptor);


	CToken* pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("size", (int)this->roofPoints.size());

	pNewToken = pStructuredFileSection->addToken();
	for (unsigned int i = 0; i < this->roofPoints.size(); i++)
		{
			int element = this->roofPoints[i];
			elementStr.Format("%d ", element);
			valueStr += elementStr;
		}

	pNewToken->setValue("roofPoints", valueStr.GetChar());	

	valueStr.Empty();
	elementStr.Empty();	

	pNewToken = pStructuredFileSection->addToken();
	for (unsigned int i = 0; i < this->floorPoints.size(); i++)
		{
			int element = this->floorPoints[i];
			elementStr.Format("%d ", element);
			valueStr += elementStr;
		}

	pNewToken->setValue("floorPoints", valueStr.GetChar());


}


void CPrimitiveDescriptor::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if (key.CompareNoCase("descriptorName"))
			this->descriptorName =token->getValue();
		else if(key.CompareNoCase("type"))
			this->type = (PrimitiveDescriptorType)token->getIntValue();
        else if (key.CompareNoCase("parentDescriptor"))
            this->parentDescriptor = token->getIntValue();
        else if (key.CompareNoCase("size"))
		{
			int size =  token->getIntValue();
			this->roofPoints.resize(size);
			this->floorPoints.resize(size);
		}
	    else if (key.CompareNoCase("roofPoints"))
		{
			token->getVectorClass(this->roofPoints);
		}
		else if (key.CompareNoCase("floorPoints"))
		{
			token->getVectorClass(this->floorPoints);
		}

    }
}

void CPrimitiveDescriptor::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

void CPrimitiveDescriptor::resetPointers()
{
    CSerializable::resetPointers();
}

bool CPrimitiveDescriptor::isModified()
{
	return this->bModified;
}