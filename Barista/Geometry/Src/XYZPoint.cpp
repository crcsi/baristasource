
/**
 * @class CXYZPoint
 * an xyz point that can have a label
 * and a 3x3 covariance matrix for use in least-sqaures
 */
 
#include "newmat.h"
#include "XYPoint.h"

#include "StructuredFileSection.h"

#include "XYZPoint.h"
  
/**
 * copy constructor, contructor which derives from CXYPoint
 * @param p
 * @param I_z
 * @param I_sigmaZ
 */
 

CXYZPoint::~CXYZPoint(void)
{
}

CXYZPoint::CXYZPoint(const CXYPoint &p, double I_z, double I_sigmaZ) :
	 CPointBase(3), C3DPoint(p.x,p.y,I_z), CObsPoint(3,p.getLabel(),p.getStatus())
{
	this->setID(p.id);
	this->setDot(p.dot);
	this->initCovarFrom2D(p.getCovariance(), I_sigmaZ);	
}

CXYZPoint &CXYZPoint::operator= (const CXYZPoint &point)
{
	CObsPoint::operator=(point);
	this->bSelected     = point.bSelected; 
	return *this;
};

/**
 * Compares the XYZ-coordinates of two CXYZPoints
 * @param point
 * returns true if coordinates are equal, else false
 */
bool CXYZPoint::equals (CXYZPoint* point)
{
    if ((this->x == point->x) && (this->y == point->y) && (this->z == point->z))
        return true;
    else
        return false;
}


/**
 * Serialized storage of a CXYZPoint to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYZPoint::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("label", this->getLabel().GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("x", this->getX());

    token = pStructuredFileSection->addToken();
    token->setValue("y", this->getY());

    token = pStructuredFileSection->addToken();
    token->setValue("z", this->getZ());

    token = pStructuredFileSection->addToken();
    token->setValue("sx", this->getSX());

    token = pStructuredFileSection->addToken();
    token->setValue("sy", this->getSY());

    token = pStructuredFileSection->addToken();
    token->setValue("sz", this->getSZ());

    token = pStructuredFileSection->addToken();
    token->setValue("sxy", this->getSXY());

    token = pStructuredFileSection->addToken();
    token->setValue("sxz", this->getSXZ());

    token = pStructuredFileSection->addToken();
    token->setValue("syz", this->getSYZ());

    token = pStructuredFileSection->addToken();
    token->setValue("status", this->status);
}

/**
 * Restores a CXYZPoint from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYZPoint::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("label"))
			this->label = CLabel(token->getValue());
        else if (key.CompareNoCase("x"))
            this->x = token->getDoubleValue();
        else if (key.CompareNoCase("y"))
            this->y = token->getDoubleValue();
        else if (key.CompareNoCase("z"))
            this->z = token->getDoubleValue();
        else if (key.CompareNoCase("sx"))
            this->setSX(token->getDoubleValue());
        else if (key.CompareNoCase("sy"))
            this->setSY(token->getDoubleValue());
        else if (key.CompareNoCase("sz"))
            this->setSZ(token->getDoubleValue());
        else if (key.CompareNoCase("sxy"))
            this->setSXY(token->getDoubleValue());
        else if (key.CompareNoCase("sxz"))
            this->setSXZ(token->getDoubleValue());
        else if (key.CompareNoCase("syz"))
            this->setSYZ(token->getDoubleValue());
        else if (key.CompareNoCase("status"))
            this->status = token->getIntValue();
    }

}

/**
 * reconnects pointers
 */
void CXYZPoint::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CXYZPoint::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Flag if CXYZPoint is modified
 * returns bool
 */
bool CXYZPoint::isModified()
{
    return this->bModified;
}

double  CXYZPoint::getSX() const 
{ 
	return sqrt(this->covariance->element(0, 0)); 
}
	  
double CXYZPoint::getSY() const 
{ 
	return sqrt(this->covariance->element(1, 1)); 
}

double CXYZPoint::getSZ() const 
{ 
	return sqrt(this->covariance->element(2, 2)); 
}

double  CXYZPoint::getSXY() const 
{ 
	return this->covariance->element(0, 1); 
}
	  
double CXYZPoint::getSXZ() const 
{ 
	return this->covariance->element(0, 2); 
}

double CXYZPoint::getSYZ() const 
{ 
	return this->covariance->element(1, 2); 
}




/**
 * Sets SX (diagonal element) of CXYZPoint in covariance matrix
 * sets all other SX-correlated values to zero
 */
void CXYZPoint::setSX (double sigmaX)
{
    this->bModified = true;
    this->covariance->element(0, 0) = sigmaX * sigmaX;
    this->covariance->element(0, 1) = 0.;
    this->covariance->element(0, 2) = 0.;
    this->covariance->element(1, 0) = 0.;
    this->covariance->element(2, 0) = 0.;
}

/**
 * Sets SY (diagonal element) of CXYZPoint in covariance matrix
 * sets all other SY-correlated values to zero
 */
void CXYZPoint::setSY (double sigmaY)
{
    this->bModified = true;
    this->covariance->element(0, 1) = 0.;
    this->covariance->element(1, 0) = 0.;
    this->covariance->element(1, 1) = sigmaY * sigmaY;
    this->covariance->element(1, 2) = 0.;
    this->covariance->element(2, 1) = 0.;
}

/**
 * Sets SZ (diagonal element) of CXYZPoint in covariance matrix
 * sets all other SZ-correlated values to zero
 */
void CXYZPoint::setSZ (double sigmaZ)
{
    this->bModified = true;
    this->covariance->element(0, 2) = 0.;
    this->covariance->element(1, 2) = 0.;
    this->covariance->element(2, 0) = 0.;
    this->covariance->element(2, 1) = 0.;
    this->covariance->element(2, 2) = sigmaZ * sigmaZ;
}

void CXYZPoint::setSXY (double sigmaXY)
{
    this->bModified = true;
    this->covariance->element(0, 1) = sigmaXY;
    this->covariance->element(1, 0) = sigmaXY;
};

void CXYZPoint::setSXZ (double sigmaXZ)
{
    this->bModified = true;
    this->covariance->element(0, 2) = sigmaXZ;
    this->covariance->element(2, 0) = sigmaXZ;
};

void CXYZPoint::setSYZ (double sigmaYZ)
{
    this->bModified = true;
    this->covariance->element(1, 2) = sigmaYZ;
    this->covariance->element(2, 1) = sigmaYZ;
};

void CXYZPoint::initCovarFrom2D(Matrix *covar2D, double I_sigmaZ)
 {
	this->covariance->SubMatrix(1,2,1,2) = *covar2D;
	this->covariance->element(2,2) = I_sigmaZ * I_sigmaZ;
	this->covariance->element(0,2) = this->covariance->element(1,2) = 0.0f;
	this->covariance->element(2,0) = this->covariance->element(2,1) = 0.0f;
}
