#include "newmat.h"
#include "utilities.h"
#include "XYZPointArray.h"

#include "TransGeocentricToGeographic.h"

CTransGeocentricToGeographic::CTransGeocentricToGeographic(const CReferenceSystem &sysFrom, 
														   const CReferenceSystem &sysTo): 
				CTrans3D(sysFrom, sysTo)
{

}

CTransGeocentricToGeographic::~CTransGeocentricToGeographic()
{
}

void CTransGeocentricToGeographic::transform(C3DPoint &out, const C3DPoint &in) 
{   
	this->systemFrom.geocentricToGeographic(out, in);
}

void CTransGeocentricToGeographic::transform(CXYZPoint &out, const CXYZPoint &in) 
{ 
	this->systemFrom.geocentricToGeographic(out, in);
};

void CTransGeocentricToGeographic::getDerivatives(Matrix &derivatives, const C3DPoint &p) const
{
	this->systemFrom.derivativesGeocentricToGeographic(derivatives, p);
};
