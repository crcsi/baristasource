#include "XYZOrbitPointArray.h"

CXYZOrbitPointArray::CXYZOrbitPointArray(int nGrowBy): CObsPointArray(6, nGrowBy), createWithVelocity(true)
{
}


CXYZOrbitPointArray::CXYZOrbitPointArray(const CXYZOrbitPointArray& src ): CObsPointArray(src), createWithVelocity(src.createWithVelocity)
{
	for (int i = 0; i < src.GetSize(); ++i)
	{
		CXYZOrbitPoint *p = src.getAt(i);
		this->add(*p);
	}
}


CXYZOrbitPointArray &CXYZOrbitPointArray::operator = (const CXYZOrbitPointArray & orbitArray)
{
	this->createWithVelocity = orbitArray.createWithVelocity;

	this->RemoveAll();

	for (int i = 0; i < orbitArray.GetSize(); ++i)
	{
		CXYZOrbitPoint *p = orbitArray.getAt(i);
		this->add(*p);
	}
	
	return *this;

}


CXYZOrbitPointArray::~CXYZOrbitPointArray(void)
{
}



void CXYZOrbitPointArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CStructuredFileSection* pNewSection; 

	for (int i=0; i< this->GetSize(); i++)
	{
		CXYZOrbitPoint *point = this->getAt(i);
		pNewSection = pStructuredFileSection->addChild();
		point->serializeStore(pNewSection);
	}

}

void CXYZOrbitPointArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("CXYZOrbitPoint"))
		{
			CXYZOrbitPoint *point = this->add();
			point->serializeRestore(pChild);
			
		}	
	}
}
