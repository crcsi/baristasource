#include <strstream>
#include <float.h>
#include "newmat.h"
#include "StructuredFileSection.h"
#include "Trans2DConformal.h"
#include "AdjustNormEqu.h"

CTrans2DConformal::CTrans2DConformal(): a1(1), a2(0), a3(0), a4(0)
{
	this->covar = new SymmetricMatrix(4);
	*(this->covar) = 0.0;
}

CTrans2DConformal::CTrans2DConformal(const Matrix &parms): 
			a1(parms.element(0, 0)), a2(parms.element(1, 0)),
			a3(parms.element(2, 0)), a4(parms.element(3, 0))
{
	this->covar = new SymmetricMatrix(4);
	*(this->covar) = 0.0;
}

CTrans2DConformal::CTrans2DConformal(const CTrans2DConformal &trans): 
			a1(trans.a1), a2(trans.a2), a3(trans.a3), a4(trans.a4)
{
	this->covar = new SymmetricMatrix(*trans.covar);
}

CTrans2DConformal::~CTrans2DConformal(void)
{
}

CTrans2DConformal &CTrans2DConformal::operator = (const CTrans2DConformal &trans)
{
	if (this != &trans)
	{
		this->a1 = trans.a1;
		this->a2 = trans.a2;
		this->a3 = trans.a3;
		this->a4 = trans.a4;
		*this->covar = *trans.covar;
		this->P0 = trans.P0;
	}

	return *this;
};

CCharString CTrans2DConformal::getDescriptor(const CCharString &lead, int shiftDigits, int matrixDigits, 
		                                     const CCharString &descrFrom, 
										     const CCharString &descrTo) const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	int width = 12;
	int constPrec = 2;
	int matrxPrec = 9;

	oss << lead << "Conformal transformation: \n\n" << lead << "     x" << descrTo.GetChar() << " = ";		
	oss.width(width);
	oss.precision(constPrec);
	oss << this->a3 << " + ";
	oss.precision(matrxPrec);
	oss.width(width);
	oss << this->a1 << " * x" << descrFrom.GetChar() << "  - ";
	oss.width(width);
	oss << this->a2 << " * y" << descrFrom.GetChar() << "\n" << lead << "     y" << descrTo.GetChar() << " = ";
	oss.width(width);
	oss.precision(constPrec);
	oss << this->a4 << " + ";
	oss.precision(matrxPrec);
	oss.width(width);
	oss << this->a2 << " * x" << descrFrom.GetChar() << "  + ";
	oss.width(width);
	oss << this->a1 << " * y" << descrFrom.GetChar() << ends;

	char *zstr = oss.str();
	CCharString retVal(zstr);
	delete[] zstr;

	return retVal;
}

void CTrans2DConformal::getParameter(vector<double> &parameter) const
{
	parameter.resize(4);
	parameter[0] = a1;
	parameter[1] = a2;
	parameter[2] = a3;
	parameter[3] = a4;

	
}


void CTrans2DConformal::get(Matrix &parms, C2DPoint &p0) const
{
	p0 = this->P0;
	parms.ReSize(4, 1);
	parms.element(0,0) = this->a1;
	parms.element(1,0) = this->a2;
	parms.element(2,0) = this->a3;
	parms.element(3,0) = this->a4;
};

void CTrans2DConformal::set(const Matrix &parms, const C2DPoint &p0)
{
	this->P0 = p0;
	this->a1 = parms.element(0,0);
	this->a2 = parms.element(1,0);
	this->a3 = parms.element(2,0);
	this->a4 = parms.element(3,0);
};

int CTrans2DConformal::getWeightForDirectObs(Matrix &weights, double sigma0, double maxInfluence, double maxDist) const
{
	weights.ReSize(4, 1);

	double w = fabs(this->a1) >  fabs(this->a2) ? this->a1  : this->a2;
	
	w = sigma0 * maxDist / (w * maxInfluence);
	w *= w;

	weights.element(0,0) = w;
	weights.element(1,0) = w;
	weights.element(2,0) = 0.0;
	weights.element(3,0) = 0.0;

	return 2;
};
	  
void CTrans2DConformal::applyP0()
{
	this->a3 += (- this->a1 * this->P0.x + this->a2 * this->P0.y);
	this->a4 += (- this->a2 * this->P0.x - this->a1 * this->P0.y);
	this->P0.x = 0.0;
	this->P0.y = 0.0;
};

//================================================================================

Matrix CTrans2DConformal::getProjectionMatrix() const
{
	Matrix M(3,3);
	M.element(0,0) = this->a1;  M.element(0,1) = -this->a2;  M.element(0,2) = this->a3 - this->a1 * this->P0.x + this->a2 * this->P0.y;
	M.element(1,0) = this->a2;  M.element(1,1) =  this->a1;  M.element(1,2) = this->a4 - this->a2 * this->P0.x - this->a1 * this->P0.y;
	M.element(2,0) = 0.0;       M.element(2,1) =  0.0;       M.element(2,2) = 1.0;

	return M;
};

void CTrans2DConformal::transform(CXYZPoint &out, const CXYPoint &in) const 
{
	out.setLabel(in.getLabel());

	double x = in.x - this->P0.x;
	double y = in.y - this->P0.y;

	out.x = this->a1 * x - this->a2 * y + this->a3;
	out.y = this->a2 * x + this->a1 * y + this->a4;
	out.z = 0.0;
}
	  
void CTrans2DConformal::transform(CXYPoint &out, const CXYPoint &in) const 
{
	out.setLabel(in.getLabel());
	double x = in.x - this->P0.x;
	double y = in.y - this->P0.y;

	out.x = this->a1 * x - this->a2 * y + this->a3;
	out.y = this->a2 * x + this->a1 * y + this->a4;
}


void CTrans2DConformal::transform(C2DPoint &out, const C2DPoint &in) const 
{
	double x = in.x - this->P0.x;
	double y = in.y - this->P0.y;

	out.x = this->a1 * x - this->a2 * y + this->a3;
	out.y = this->a2 * x + this->a1 * y + this->a4;
};

void CTrans2DConformal::getDerivatives(double x, double y, Matrix &Ax, Matrix &Ay) const
{ 
	Ax.ReSize(4,1);
	Ay.ReSize(4,1);

	Ax.element(0,0) =  1.0;
	Ax.element(1,0) =  0.0;
	Ax.element(2,0) =  x;
	Ax.element(3,0) = -y;

	Ay.element(0,0) = 0.0;
	Ay.element(1,0) = 1.0;
	Ay.element(2,0) = y;
	Ay.element(3,0) = x;
};

void CTrans2DConformal::getDerivativesCoords(double x, double y, Matrix &B) const
{ 
	B.ReSize(2,2);
	B.element(0,0) =  this->a1;
	B.element(0,1) = -this->a2;
	B.element(1,0) =  this->a2;
	B.element(1,1) =  this->a1;
};	
bool CTrans2DConformal::invert()
{
	double det = this->a1 * this->a1 + this->a2 * this->a2;
	if (fabs(det) < FLT_EPSILON) return false;

	det = 1.0 / det;

	this->a1 =  this->a1 * det;
	this->a2 = -this->a2 * det;
	
	double x = this->P0.x;
	double y = this->P0.y;
	this->P0.x = this->a3;
	this->P0.y = this->a4;
	this->a3 = x;
	this->a4 = y;

	return true;
};

void CTrans2DConformal::addVec(const Matrix &dx, int index0)
{
	this->a1 += dx.element(index0, 0); ++index0;
	this->a2 += dx.element(index0, 0); ++index0;
	this->a3 += dx.element(index0, 0); ++index0;
	this->a4 += dx.element(index0, 0); 
};


void CTrans2DConformal::prepareAdjustment(CNormalEquationMatrix &N, CtVector &m, double &vtpv, 
		                             unsigned long &nObs) const
{
	N.initialize(4, 0.0);
	m.initialize(4, 0.0);
	vtpv = 0.0;
	nObs = 0;
};

void CTrans2DConformal::accumulate(const C2DPoint &p1, const C2DPoint &p2, CNormalEquationMatrix &N, 
		                      CtVector &m, double &vtpv, unsigned long &nObs)
{

};

bool CTrans2DConformal::solve(CNormalEquationMatrix &N, const CtVector &m,  
		                 double vtpv, unsigned long nObs, double &s0)
{
	Try
	{
		
	}
    CatchAll //(char* error) 
    {
        return false;
    }

	return true;
};

/*
float CTrans2DConformal::getQvv(const C2DPoint &p1, Matrix &covar) const
{
	C2DPoint p = p1-P0;
	double aqat = 2.0f * (covar.element(1,0) * p.x + covar.element(2,0) * p.y + covar.element(2,1) * p.x * p.y) + 
		          covar.element(1,1) * p.x * p.x + covar.element(2,2) * p.y * p.y + covar.element(0,0);
    aqat = 1.0 - aqat;
	if (aqat < FLT_EPSILON) aqat = FLT_EPSILON;
	return (float) sqrt(aqat);
};*/