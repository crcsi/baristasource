/**
 * @class CXYEllipse
 * an xy ellipse that can have a label
 * and a 5x5 covariance matrix for use in least-sqaures
 */
#include <float.h>


#include "XYPointArray.h"
#include "StructuredFile.h"
#include "Polynomial.h"

#include "XYEllipse.h"

#ifndef M_PI
#define M_PI    3.14159265358979323846  // PI
#endif

CXYEllipse::CXYEllipse(void):sinTheta(0), cosTheta(0)
{
	this->label = "Ellipse";

    this->covariance.ReSize(5, 5);

}

CXYEllipse::~CXYEllipse(void)
{
}

CXYEllipse::CXYEllipse(double a, double b, double theta, double x0, double y0)
{
	this->a = a;
	this->b = b;
	this->theta = theta;
	this->x0 = x0;
	this->y0 = y0;

	this->sinTheta = sin(this->theta);
	this->cosTheta = cos(this->theta);
    this->covariance.ReSize(5, 5);
}

CXYEllipse::CXYEllipse(double a, double b, double theta, double x0, double y0, const CLabel str)
{
	this->a = a;
	this->b = b;
	this->theta = theta;
	this->sinTheta = sin(this->theta);
	this->cosTheta = cos(this->theta);
	this->x0 = x0;
	this->y0 = y0;
	this->label = str;

    this->covariance.ReSize(5, 5);
}

CXYEllipse::CXYEllipse(Matrix parms, const CLabel str)
{
	this->a     = parms.element(0, 0);
	this->b     = parms.element(1, 0);
	this->theta = parms.element(2, 0);
	this->x0    = parms.element(3, 0);
	this->y0    = parms.element(4, 0);
	this->label = str;
	this->sinTheta = sin(this->theta);
	this->cosTheta = cos(this->theta);

    this->covariance.ReSize(5, 5);
}

CXYEllipse::CXYEllipse(Matrix parms)
{
	this->a     = parms.element(0, 0);
	this->b     = parms.element(1, 0);
	this->theta = parms.element(2, 0);
	this->x0    = parms.element(3, 0);
	this->y0    = parms.element(4, 0);
	this->sinTheta = sin(this->theta);
	this->cosTheta = cos(this->theta);

    this->covariance.ReSize(5, 5);
}
	
bool CXYEllipse::init(const vector<C2DPoint> &pointVector)
{
	bool ret = true;
	if (pointVector.size() < 5) ret = false;
	else
	{
		C2DPoint cog(0.0,0.0);
		double xmin = FLT_MAX;
		double xmax = -FLT_MAX;
		double ymin = FLT_MAX;
		double ymax = -FLT_MAX;
		for (unsigned long i = 0; i < pointVector.size(); ++i)
		{
			cog += pointVector[i];
			if (pointVector[i].x < xmin) xmin = pointVector[i].x;
			if (pointVector[i].x > xmax) xmax = pointVector[i].x;
			if (pointVector[i].y < ymin) ymin = pointVector[i].y;
			if (pointVector[i].y > ymax) ymax = pointVector[i].y;
		};

		cog /= (double) pointVector.size();
		xmax -= xmin;
		ymax -= ymin;
		double rho = xmax > ymax ? double(xmax) : double(ymax);

		Matrix x(6,1);
		x = 0.0;
		x(1,1) = x(3,1) = 1.0;
		int maxIt = 25;
		for (int it = 0; it < maxIt; ++it)
		{
			Matrix A(pointVector.size(),6);
			Matrix l(pointVector.size(),1);

			for (unsigned long i = 0; i < pointVector.size(); ++i)
			{
				C2DPoint p = (pointVector[i] - cog) / rho;
				double bx = 2.0 * (x(1,1) * p.x + x(2,1) * p.y + x(4,1));
				double by = 2.0 * (x(3,1) * p.y + x(2,1) * p.x + x(5,1));
				double btb = sqrt(1.0 /(bx*bx + by*by));
				A(i + 1, 1) = p.x * p.x * btb; 
				A(i + 1, 2) = 2.0 * p.x * p.y* btb; 
				A(i + 1, 3) = p.y * p.y* btb; 
				A(i + 1, 4) = 2.0 * p.x* btb;  
				A(i + 1, 5) = 2.0 * p.y* btb;
				A(i + 1, 6) = 1.0 * btb;
				l(i + 1, 1) = -btb * (x(1,1) * p.x * p.x + 2.0 * x(2,1) * p.x * p.y + x(3,1) * p.y * p.y + 2.0 * x(4,1) * p.x + 2.0 * x(5,1) * p.y + x(6,1));
			};

			Matrix At = A.t();
			Matrix N(7,7);
			N = 0.0;
			N.SubMatrix(1,6,1,6) = At*A;
			N(1,7) = N(7,1) = x(3,1);
			N(3,7) = N(7,3) = x(1,1);
			N(2,7) = N(7,2) = -2.0 * x(2,1);
			Matrix n(7,1);
			n.SubMatrix(1,6,1,1) = At * l;
			n(7,1) = 1.0 - x(3,1) * x(1,1) + x(2,1) * x(2,1);

			Matrix Qxx = N.i();
			l =  Qxx * n;

			x(1,1) += l(1,1);
			x(2,1) += l(2,1);
			x(3,1) += l(3,1);
			x(4,1) += l(4,1);
			x(5,1) += l(5,1);
			x(6,1) += l(6,1);
		}

		double subDet = x(2,1) * x(2,1) - x(3,1) * x(1,1);
		this->x0 = rho * (x(3,1) * x(4,1) - x(2,1) * x(5,1)) / subDet + cog.x;
		this->y0 = rho * (x(1,1) * x(5,1) - x(2,1) * x(4,1)) / subDet + cog.y;
		double AminC      = x(1,1) - x(3,1);
		
		if ((fabs(x(2,1)) < DBL_EPSILON) && (fabs(AminC) < DBL_EPSILON))
			this->theta = 0.0;
		else this->theta = atan2(2*x(2,1), -AminC) * 0.5f;
		if (this->theta < 0.0) this->theta += M_PI;

		this->sinTheta = sin(this->theta);
		this->cosTheta = cos(this->theta);

		double AplsC      = x(1,1) + x(3,1);
		double enumerator = 2.0 * (x(1,1) * x(5,1)* x(5,1) + x(3,1) * x(4,1)* x(4,1) + x(2,1)* x(2,1) * x(5,1) 
			                       - 2.0 * x(2,1) * x(4,1) * x(5,1) -  x(1,1) * x(3,1) * x(5,1));
		double aux = sqrt(1.0 + 4.0 * x(2,1) * x(2,1) / (AminC * AminC));
		this->a = enumerator / (subDet * ( AminC * aux - AplsC));
		this->b = enumerator / (subDet * (-AminC * aux - AplsC));
		if ((this->b < 0.0) || (this->a < 0.0)) ret = false;
		else 
		{
			this->a = rho * sqrt(this->a);
			this->b = rho * sqrt(this->b);
			if (this->a < this->b) 
			{
				aux = this->a;
				this->a = this->b;
				this->b = aux;
			}
		}
	}
	return ret;
}

void CXYEllipse::set(double a, double b, double theta, double x0, double y0)
{
	this->a = a;
	this->b = b;
	this->theta = theta;
	this->sinTheta = sin(this->theta);
	this->cosTheta = cos(this->theta);
	this->x0 = x0;
	this->y0 = y0;
}
void CXYEllipse::set(double a, double b, double theta, double x0, double y0, const CLabel str)
{
	this->a = a;
	this->b = b;
	this->theta = theta;
	this->sinTheta = sin(this->theta);
	this->cosTheta = cos(this->theta);
	this->x0 = x0;
	this->y0 = y0;
	this->label = str;
}
void CXYEllipse::set(Matrix parms, const CLabel str)
{
	this->a     = fabs(parms.element(0, 0));
	this->b     = fabs(parms.element(1, 0));
	this->theta = parms.element(2, 0);
	this->sinTheta = sin(this->theta);
	this->cosTheta = cos(this->theta);
	this->x0    = parms.element(3, 0);
	this->y0    = parms.element(4, 0);
	this->label = str;
}
void CXYEllipse::set(Matrix parms)
{
	this->a     = parms.element(0, 0);
	this->b     = parms.element(1, 0);
	this->theta = parms.element(2, 0);
	this->sinTheta = sin(this->theta);
	this->cosTheta = cos(this->theta);
	this->x0    = parms.element(3, 0);
	this->y0    = parms.element(4, 0);
}

CLabel CXYEllipse::getLabel() const
{
	return this->label;
}

void CXYEllipse::setLabel(CLabel label)
{
    this->label = label;
}

CXYPoint CXYEllipse::getCenter() const
{
	CXYPoint center;
	center.set(this->x0, this->y0);

	Matrix cov;
	cov.ReSize(2,2);

	cov.element(0,0) = this->covariance.element(3,3);
	cov.element(1,1) = this->covariance.element(4,4);
	cov.element(0,1) = this->covariance.element(3,4);
	cov.element(1,0) = this->covariance.element(4,3);
	center.setCovariance(cov);

	// Center point label
	//CCharString str("Center");
	CCharString str;
	str += this->label.GetChar();
	center.setLabel(str.GetChar());

	return center;
}

/**
 * Sets ellipse covariance from Matrix
 * @param covariance
 */
void CXYEllipse::setCovariance(const Matrix &covariance)
{

    for ( int i =0; i < 5; i++)
    {
        for ( int j = 0; j < 5; j++)
        {
            this->covariance.element(i, j) = covariance.element(i, j);
        }
    }
}

Matrix* CXYEllipse::getCovariance()
{
    return &this->covariance;
}
	
Matrix CXYEllipse::getRotMatToLocal() const
{
	Matrix rot(2,2);
	rot(1,1) =  this->cosTheta; 	rot(1,2) = this->sinTheta;
	rot(2,1) = -this->sinTheta; 	rot(2,2) = this->cosTheta;
	return rot;
};

C2DPoint CXYEllipse::transformToLocal(C2DPoint p) const
{
	p.x -= this->x0;
	p.y -= this->y0;
	double x = p.x * this->cosTheta + p.y * this->sinTheta;
	p.y = -p.x * this->sinTheta + p.y * this->cosTheta;
	p.x = x;
	return p;
};

CXYPoint CXYEllipse::transformToLocal(CXYPoint p) const
{
	p.x -= this->x0;
	p.y -= this->y0;
	double x = p.x * this->cosTheta + p.y * this->sinTheta;
	p.y = -p.x * this->sinTheta + p.y * this->cosTheta;
	p.x = x;

	Matrix rot = getRotMatToLocal();
	Matrix *covar = p.getCovariance();
	*covar = rot * (*covar) * rot.t();
	return p;
};

C2DPoint CXYEllipse::transformToGlobal(C2DPoint p) const
{
	double x = p.x * this->cosTheta - p.y * this->sinTheta;
	p.y = p.x * this->sinTheta + p.y * this->cosTheta;
	p.x = x;
	p.x += this->x0;
	p.y += this->y0;
    return p;
};

CXYPoint CXYEllipse::transformToGlobal(CXYPoint p) const
{
	double x = p.x * this->cosTheta - p.y * this->sinTheta;
	p.y = p.x * this->sinTheta + p.y * this->cosTheta;
	p.x = x;
	p.x += this->x0;
	p.y += this->y0;

	Matrix rot = getRotMatToLocal();
	Matrix *covar = p.getCovariance();
	*covar = rot.t() * (*covar) * rot;

	return p;
};

double CXYEllipse::getDistance(C2DPoint p, C2DPoint &normal) const
{
	p = this->transformToLocal(p);
	p.x = fabs(p.x);
	p.y = fabs(p.y);

	if (p.y < FLT_EPSILON) return (fabs(p.x - this->a));
	if (p.x < FLT_EPSILON) return (fabs(p.y - this->b));

	double fsq = this->a * this->a - this->b * this->b;
	double auxil = this->b * p.y / fsq;
	CPolynomial poly(4);
	poly.coefficient(4) = 1.0f;
	poly.coefficient(3) = 2.0f * auxil;
	poly.coefficient(2) = (this->a * this->a * p.x*p.x + this->b * this->b * p.y*p.y) / (fsq * fsq) - 1;
	poly.coefficient(1) = - poly.coefficient(3);
	poly.coefficient(0) = -auxil * auxil;
	double approx = p.y / p.getNorm();

	double sint = approx;

	double dist = FLT_MAX;

	if (poly.zeroCrossing(approx, sint))
	{
		double cost = cos(asin(sint));

		C2DPoint base(this->a * cost, this->b * sint);
		normal.x = this->b * cost;
		normal.y = this->a * sint;
		double x = normal.x * this->cosTheta - normal.y * this->sinTheta;
		normal.y = normal.x * this->sinTheta + normal.y * this->cosTheta;
		normal.x = x;
		normal.normalise();

		dist = base.getDistance(p);
	};

	return dist;
};

double CXYEllipse::getDistance(C2DPoint p) const
{
	C2DPoint normal;
	return this->getDistance(p, normal);
}

/**
 * Serialized storage of a CXYEllipse to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYEllipse::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("label", this->getLabel().GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("a", this->a);

    token = pStructuredFileSection->addToken();
    token->setValue("b", this->b);

    token = pStructuredFileSection->addToken();
    token->setValue("theta", this->theta);

    token = pStructuredFileSection->addToken();
    token->setValue("x0", this->x0);

    token = pStructuredFileSection->addToken();
    token->setValue("y0", this->y0);
}

/**
 * Restores a CXYEllipse from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYEllipse::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("label"))
			this->label = CLabel(token->getValue());
        else if (key.CompareNoCase("a"))
            this->a = token->getDoubleValue();
        else if (key.CompareNoCase("b"))
            this->b = token->getDoubleValue();
        else if (key.CompareNoCase("theta"))
            this->theta = token->getDoubleValue();
        else if (key.CompareNoCase("x0"))
            this->x0 = token->getDoubleValue();
        else if (key.CompareNoCase("y0"))
            this->y0 = token->getDoubleValue();
    }
}

/**
 * reconnects pointers
 */
void CXYEllipse::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CXYEllipse::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Flag if CXYEllipse is modified
 * returns bool
 */
bool CXYEllipse::isModified()
{
    return this->bModified;
}


/**
 * Returns an array of edge points on the ellipse
 * Used for display
 */
//void CXYEllipse::getBoarderPoints(CXYPointArray* boarder)
void CXYEllipse::getBoarderPoints(C2DPointArray* boarder)
{
    //CXYPointArray helppoints;
	//CXYPoint helppoint;
	
	C2DPointArray points;
	C2DPoint point;

	// maybe 2DPoint and 2DPointArray would be enough here

	double pi = atan(1.0) * 4.0;
    // Now add a bunch of border points
    double rho = 0.0;

    for (int i = 0; i < 100; i++)
    {
        double r = sqrt(this->a*this->a*this->b*this->b /
			(this->a*this->a*sin(rho)*sin(rho) + this->b*this->b*cos(rho)*cos(rho)));

        double dx = r * cos(rho+this->theta);
        double dy = r * sin(rho+this->theta);

		point.x = this->x0 + dx;
		point.y = this->y0 + dy;
		//point.set( this->x0 + dx, this->y0 + dy);

		boarder->Add(point);
        rho += pi / 50.0;
    }

    return;
}


