#include "ObsPointPtrArray.h"

CObsPointPtrArray::CObsPointPtrArray(int Dim, int nGrowBy):
 CMUObjectPtrArray<CObsPoint>(nGrowBy), dim(Dim)
{
}

CObsPointPtrArray::CObsPointPtrArray(const CObsPointPtrArray &src):
	CMUObjectPtrArray<CObsPoint>(src.GetGrowBy()), dim(src.dim)
{
}	

CObsPointPtrArray::~CObsPointPtrArray(void)
{
}


void CObsPointPtrArray::sortByLabel()
{
    if (this->GetSize() > 0)
		this->qsortByLabel(0, this->GetSize()-1);

}

void CObsPointPtrArray::qsortByLabel(int L, int R)
{
    int i = L;
	int j = R;

    CObsPoint* x = this->GetAt((L + R)/ 2);

   // const char* xlabel = x->getLabel().GetChar();
    CCharString xLabel = x->getLabel().GetChar();
	do
    {
        CCharString iLabel = this->GetAt(i)->getLabel().GetChar();
        while (iLabel.isLessThan(xLabel))
        {
			i++;
            iLabel = this->GetAt(i)->getLabel().GetChar();
        }
        
        CCharString jLabel = this->GetAt(j)->getLabel().GetChar();
        while (xLabel.isLessThan(jLabel))
        {
			j--;
            jLabel = this->GetAt(j)->getLabel().GetChar();
        }
        
		if (i <= j)
        {
			this->Swap(i, j);
			i++; 
			j--;
        }
    } while (!(i > j));
    
	if (L < j)
        this->qsortByLabel(L, j);
    
	if (i < R)
        this->qsortByLabel(i, R);
}

void CObsPointPtrArray::revert()
{
	for (int i = 0, j = this->m_nSize - 1; i < j; ++i, --j)
	{
		CObsPoint *p = this->m_pData[i];
		this->m_pData[i] = this->m_pData[j];
		this->m_pData[j] = p;
	}
};
