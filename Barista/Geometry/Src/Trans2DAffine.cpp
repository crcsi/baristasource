#include <strstream>
#include <float.h>
#include "newmat.h"
#include "AdjustNormEqu.h"
#include "StructuredFileSection.h"
#include "Trans2DAffine.h"


CTrans2DAffine::CTrans2DAffine(): a0(0), a1(1), a2(0), b0(0), b1(0), b2(1), P0(0,0)
{
	this->covar = new SymmetricMatrix(6);
	*(this->covar) = 0.0;
}

// Transformation formula: x = a0 + a1 * (X - P0.x) + a2 * (Y - P0.x)
//                         y = b0 + b1 * (X - P0.x) + b2 * (Y - P0.x)

CTrans2DAffine::CTrans2DAffine(const C2DPoint &p0From, const C2DPoint &p0To, 
		                       C2DPoint p1From,        C2DPoint p1To, 
							   C2DPoint p2From,        C2DPoint p2To) : 
							   P0(p0From), a0(p0To.x), b0(p0To.y),
							   a1(1), a2(0), b1(0), b2(1)
{
	p1From -= p0From;
	p2From -= p0From;
	p1To   -= p0To;
	p2To   -= p0To;

	
	double det = p1From.x * p2From.y - p1From.y * p2From.x;
	if (fabs(det) > FLT_EPSILON)
	{
		det = 1.0 / det;
		double A11 =  p2From.y * det;
		double A22 =  p1From.x * det;
		double A12 = -p1From.y * det;
		double A21 = -p2From.x * det;
		this->a1 = A11 * p1To.x + A12 * p2To.x;
		this->a2 = A21 * p1To.x + A22 * p2To.x;
		this->b1 = A11 * p1To.y + A12 * p2To.y;
		this->b2 = A21 * p1To.y + A22 * p2To.y;
	};

	this->covar = new SymmetricMatrix(6);
	*(this->covar) = 0.0;
};

CTrans2DAffine::CTrans2DAffine(const CTrans2DAffine &trans):
	a0 (trans.a0), a1 (trans.a1), a2 (trans.a2),
    b0 (trans.b0), b1 (trans.b1), b2 (trans.b2), 
	P0 (trans.P0)
{
	this->covar = new SymmetricMatrix(*trans.covar);
};

CTrans2DAffine::CTrans2DAffine(const Matrix &parms, const C2DPoint &p0):
	a1 (parms.element(0, 0)), a2 (parms.element(1, 0)),
    a0 (parms.element(2, 0)), b1 (parms.element(3, 0)),
    b2 (parms.element(4, 0)), b0 (parms.element(5, 0)), 
	P0(p0)
{
	this->covar = new SymmetricMatrix(6);
	*(this->covar) = 0.0;
}

	 
CTrans2DAffine::CTrans2DAffine(const Matrix &parms):
	a1 (parms.element(0, 0)), a2 (parms.element(1, 0)),
    a0 (parms.element(2, 0)), b1 (parms.element(3, 0)),
    b2 (parms.element(4, 0)), b0 (parms.element(5, 0)),
	P0(0,0)
{
	this->covar = new SymmetricMatrix(6);
	*(this->covar) = 0.0;
}

CTrans2DAffine::~CTrans2DAffine()
{
}

void CTrans2DAffine::getParameter(vector<double> &parameter) const
{
	parameter.resize(6);
	parameter[0] = a1;
	parameter[1] = a2;
	parameter[2] = a0;
	parameter[3] = b1;
	parameter[4] = b2;
	parameter[5] = b0;
	
}

int CTrans2DAffine::getWeightForDirectObs(Matrix &weights, double sigma0, double maxInfluence, double maxDist) const
{
	weights.ReSize(6, 1);

	double wa = fabs(this->a1) > fabs(this->a2) ? this->a1 * maxInfluence : this->a2 * maxInfluence;
	double wb = fabs(this->b1) > fabs(this->b2) ? this->b1 * maxInfluence : this->b2 * maxInfluence;
	
	sigma0 *= maxDist;

	wa = sigma0 / wa;
	wb = sigma0 / wb;
	wa *= wa;
	wb *= wb;
	weights.element(0,0) = 0.0;
	weights.element(1,0) = wa;
	weights.element(2,0) = wa;
	weights.element(3,0) = 0.0;
	weights.element(4,0) = wb;
	weights.element(5,0) = wb;

	return 4;
}

CCharString CTrans2DAffine::getDescriptor(const CCharString &lead, int shiftDigits, int matrixDigits, 
										  const CCharString &descrFrom, 
										  const CCharString &descrTo) const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);

	int shiftwidth = int(floor(log10(fabs(this->a0))));
	int w = int(floor(log10(fabs(this->b0))));
	if (w > shiftwidth) shiftwidth = w;
	if (shiftwidth < 1) shiftwidth = 1;
	shiftwidth +=  2 + shiftDigits;

	int matrixwidth = int(floor(log10(fabs(this->a1))));
	w = int(floor(log10(fabs(this->a2))));
	if (w > matrixwidth) matrixwidth = w;
	w = int(floor(log10(fabs(this->b1))));
	if (w > matrixwidth) matrixwidth = w;
	w = int(floor(log10(fabs(this->b2))));
	if (w > matrixwidth) matrixwidth = w;	
	if (matrixwidth < 1) matrixwidth = 1;
	matrixwidth +=  2 + matrixDigits;

	int p0width = int(floor(log10(fabs(this->P0.x))));
	w = int(floor(log10(fabs(this->P0.y))));
	if (w > p0width) p0width = w;
	if (p0width < 1) p0width = 1;
	p0width +=  2 + shiftDigits;

	oss << lead << "Affine transformation: \n\n" << lead << "     x" << descrTo.GetChar() << " = ";		
	oss.width(shiftwidth);
	oss.precision(shiftDigits);
	oss << this->a0;
	if (this->a1 < 0) oss << " - ";
	else oss << " + ";
	oss.precision(matrixDigits);
	oss.width(matrixwidth);
	oss << fabs(this->a1) << " * (x" << descrFrom.GetChar(); 
	if (this->P0.x < 0) oss << " + ";
	else oss << " - ";
	oss.precision(shiftDigits);
	oss.width(p0width);
	oss << fabs(this->P0.x) <<")";
	if (this->a2 < 0) oss << " - ";
	else oss << " + ";	
	oss.precision(matrixDigits);
	oss.width(matrixwidth);
	oss << fabs(this->a2) << " * (y" << descrFrom.GetChar(); 
	if (this->P0.y < 0) oss << " + ";
	else oss << " - ";
	oss.precision(shiftDigits);
	oss.width(p0width);
	oss << fabs(this->P0.y) << ")\n" << lead << "     y" << descrTo.GetChar() << " = ";
	oss.width(shiftwidth);
	oss.precision(shiftDigits);
	oss << this->b0;
	if (this->b1 < 0) oss << " - ";
	else oss << " + ";

	oss.precision(matrixDigits);
	oss.width(matrixwidth);
	oss << fabs(this->b1) << " * (x" << descrFrom.GetChar(); 
	if (this->P0.x < 0) oss << " + ";
	else oss << " - ";
	oss.precision(shiftDigits);
	oss.width(p0width);
	oss << fabs(this->P0.x) << ")";
	if (this->b2 < 0) oss << " - ";
	else oss << " + ";
	oss.precision(matrixDigits);
	oss.width(matrixwidth);
	oss << fabs(this->b2) << " * (y" << descrFrom.GetChar(); 
	if (this->P0.y < 0) oss << " + ";
	else oss << " - ";

	oss.precision(shiftDigits);
	oss.width(p0width);
	oss << fabs(this->P0.y) << ")" << ends;

	char *zstr = oss.str();
	CCharString retVal(zstr);
	delete[] zstr;

	return retVal;
}

CTrans2DAffine &CTrans2DAffine::operator = (const CTrans2DAffine &trans)
{
	if (this != &trans)
	{
		this->a0 = trans.a0;
		this->a1 = trans.a1;
		this->a2 = trans.a2;
		this->b0 = trans.b0;
		this->b1 = trans.b1;
		this->b2 = trans.b2;
		this->P0 = trans.P0;
		*this->covar = *trans.covar;
	}

	return *this;
};

void CTrans2DAffine::get(Matrix &parms, C2DPoint &p0) const
{
	p0 = this->P0;
	parms.ReSize(6,1);
	parms.element(0, 0) = this->a1;
	parms.element(1, 0) = this->a2;
	parms.element(2, 0) = this->a0;
	parms.element(3, 0) = this->b1;
	parms.element(4, 0) = this->b2;
	parms.element(5, 0) = this->b0;
};

void CTrans2DAffine::set(const Matrix &parms, const C2DPoint &p0)
{
	this->P0 = p0;
	this->a1 = parms.element(0, 0);
	this->a2 = parms.element(1, 0);
	this->a0 = parms.element(2, 0);
	this->b1 = parms.element(3, 0);
	this->b2 = parms.element(4, 0);
	this->b0 = parms.element(5, 0);
};

void CTrans2DAffine::set(const Matrix &parms)
{
	this->P0.x = this->P0.y = 0.0;
	this->a1 = parms.element(0, 0);
	this->a2 = parms.element(1, 0);
	this->a0 = parms.element(2, 0);
	this->b1 = parms.element(3, 0);
	this->b2 = parms.element(4, 0);
	this->b0 = parms.element(5, 0);
};

void CTrans2DAffine::setP0(const C2DPoint &p0)
{
	this->P0 = p0;
};

void CTrans2DAffine::applyP0()
{
	this->a0 += (- this->a1 * this->P0.x - this->a2 * this->P0.y);
	this->b0 += (- this->b1 * this->P0.x - this->b2 * this->P0.y);

	this->P0.x = 0.0;
	this->P0.y = 0.0;
};


//================================================================================

Matrix CTrans2DAffine::getProjectionMatrix() const
{
	Matrix M(3,3);
	M.element(0,0) = this->a1;  M.element(0,1) = this->a2;  M.element(0,2) = this->a0 - this->a1 * this->P0.x - this->a2 * this->P0.y;
	M.element(1,0) = this->b1;  M.element(1,1) = this->b2;  M.element(1,2) = this->b0 - this->b1 * this->P0.x - this->b2 * this->P0.y;
	M.element(2,0) = 0.0;       M.element(2,1) = 0.0;       M.element(2,2) = 1.0;

	return M;
};

void CTrans2DAffine::transform(CXYZPoint &out, const CXYPoint &in) const 
{
	out.setLabel(in.getLabel());

	double xred = in.x - P0.x;
	double yred = in.y - P0.y;
	out.x = this->a0 + this->a1 * xred + this->a2 * yred;
	out.y = this->b0 + this->b1 * xred + this->b2 * yred;
	out.z = 0.0;
	Matrix a(2,2);
	a.element(0,0) = this->a1;
	a.element(1,0) = this->a2;
	a.element(0,1) = this->b1;
	a.element(1,1) = this->b2;
	Matrix b(2,6);
	b.element(0,0) = 1.0; b.element(0,1) = xred; b.element(0,2) = yred; b.element(0,3) = 0.0;  b.element(0,4) =  0.0;  b.element(0,5) =  0.0; 
	b.element(1,0) = 0.0; b.element(1,1) =  0.0; b.element(0,2) =  0.0; b.element(0,3) = 1.0;  b.element(0,4) = xred;  b.element(0,5) = yred; 

	out.getCovariance()->SubMatrix(1,2,1,2) = a * *(in.getCovariance()) * a.t() + b * *(this->covar) * b.t();
}

void CTrans2DAffine::transform(CXYPoint &out, const CXYPoint &in) const 
{
	out.setLabel(in.getLabel());
	double xred = in.x - P0.x;
	double yred = in.y - P0.y;
	out.x = this->a0 + this->a1 * xred + this->a2 * yred;
	out.y = this->b0 + this->b1 * xred + this->b2 * yred;

	Matrix a(2,2);
	a.element(0,0) = this->a1;
	a.element(1,0) = this->a2;
	a.element(0,1) = this->b1;
	a.element(1,1) = this->b2;
	Matrix b(2,6);
	b.element(0,0) = 1.0; b.element(0,1) = xred; b.element(0,2) = yred; b.element(0,3) = 0.0;  b.element(0,4) =  0.0;  b.element(0,5) =  0.0; 
	b.element(1,0) = 0.0; b.element(1,1) =  0.0; b.element(1,2) =  0.0; b.element(1,3) = 1.0;  b.element(1,4) = xred;  b.element(1,5) = yred; 

	out.setCovariance(a * *(in.getCovariance()) * a.t() + b * *(this->covar) * b.t());
}
    
void CTrans2DAffine::transform(C2DPoint &out, const C2DPoint &in) const 
{
	double xred = in.x - P0.x;
	double yred = in.y - P0.y;
	out.x = this->a0 + this->a1 * xred + this->a2 * yred;
	out.y = this->b0 + this->b1 * xred + this->b2 * yred;
};

bool CTrans2DAffine::invert()
{
	double det = this->a1 * this->b2 - this->b1 * this->a2;
	if (fabs(det) < FLT_EPSILON) return false;

	det = 1.0 / det;

	double dmy = this->a1;
	this->a1 = this->b2 * det;
	this->b2 = dmy * det;
	this->b1 = -this->b1 * det;
	this->a2 = -this->a2 * det;
	
	dmy = this->P0.x;
	this->P0.x = this->a0;
	this->a0 = dmy;

	dmy = this->P0.y;
	this->P0.y = this->b0;
	this->b0 = dmy;

	return true;
};

void CTrans2DAffine::prepareAdjustment(CNormalEquationMatrix &N, CtVector &m, 
									   double &vtpv, unsigned long &nObs) const
{
	N.initialize(3, 0.0);
	m.initialize(6, 0.0);
	vtpv = 0.0;
	nObs = 0;
};

void CTrans2DAffine::accumulate(const C2DPoint &p1, const C2DPoint &p2, CNormalEquationMatrix &N, 
		                        CtVector &m, double &vtpv, unsigned long &nObs)
{
	C2DPoint p = p1 - P0;
	N[0] += 1.0;  N[1] += p.x;         N[3] += p.y;
	              N[2] += p.x * p.x;   N[4] += p.x * p.y;
				                       N[5] += p.y * p.y;

	m[0] += p2.x;
	m[1] += p.x * p2.x;
	m[2] += p.y * p2.x;
	m[3] += p2.y;
	m[4] += p.x * p2.y;
	m[5] += p.y * p2.y;
	nObs += 2;
	vtpv += p2.x * p2.x + p2.y * p2.y;
};

bool CTrans2DAffine::solve( CNormalEquationMatrix &N, const CtVector &m,  
		                   double vtpv, unsigned long nObs, double &s0)
{
	bool ret = true;

	CNormalEquationMatrix Qxx(3), Rmat(3);
	CtVector mX(3), mY(3), solX(3), solY(3);

	for (unsigned int r = 0; r < N.getNUnknowns(); ++r)
	{
		mX[r] =  m[r];
		mY[r] =  m[r + 3];
	}
	
	int nErrors;
	ret = Rmat.solveCholesky(N, nErrors );

	if (ret && (nErrors == 0 ))
	{
		ret = mX.reduceFromR(Rmat, mX )&& mY.reduceFromR(Rmat, mY );
		if (ret)
		{
			ret = solX.solveFromR(Rmat, mX )&& solY.solveFromR(Rmat, mY );

			ret &= Rmat.invertR(Rmat);
			ret &= Qxx.solveQxx(Rmat);
 			this->covar->ReSize(6); 
			*(this->covar) = 0;

			for (int index = 0, c = 0; c < 3; ++c)
			{
				for (int r = 0; r <= c; ++r, index++)
				{
					this->covar->element(r,c)     = Qxx[index];
					this->covar->element(r+3,c+3) = Qxx[index];
				}
			}

			this->a0 = solX[0];
			this->a1 = solX[1];
			this->a2 = solX[2];
			this->b0 = solY[0];
			this->b1 = solY[1];
			this->b2 = solY[2];	
		}
	};
	
	return ret;
};

float CTrans2DAffine::getQvv(const C2DPoint &p1, Matrix &covar) const
{
	C2DPoint p = p1-P0;
	double aqat = 2.0f * (covar.element(1,0) * p.x + covar.element(2,0) * p.y + covar.element(2,1) * p.x * p.y) + 
		          covar.element(1,1) * p.x * p.x + covar.element(2,2) * p.y * p.y + covar.element(0,0);
    aqat = 1.0 - aqat;
	if (aqat < FLT_EPSILON) aqat = FLT_EPSILON;
	return (float) sqrt(aqat);
};

void CTrans2DAffine::applyFactorToBoth (double fact, Matrix &covar) 
{ 
	this->P0 *= fact;
	this->a0 *= fact;
	this->b0 *= fact;
	Matrix A(6,6);
	A = 0;
	A.element(0,0) = A.element(3,3) = fact;
	A.element(1,1) = A.element(2,2) = A.element(4,4) = A.element(5,5) = 1.0;
	covar = A * covar * A.t();
};
	  
void CTrans2DAffine::applyLinearTransformation(double factLeft, double factRight, 
		                                       C2DPoint &shiftLeft, const C2DPoint &shiftRight, 
											   Matrix &covar) 
{ 
	this->a0 = this->a0 * factLeft + shiftLeft.x;
	this->b0 = this->b0 * factLeft + shiftLeft.y;

	double fact = factLeft / factRight;
	this->P0 = this->P0 * factRight + shiftRight;

	this->a1 *= fact;
	this->a2 *= fact;
	this->b1 *= fact;
	this->b2 *= fact;
	Matrix A(6,6);
	A = 0;
	A.element(0,0) = A.element(3,3) = factLeft;
	A.element(1,1) = A.element(2,2) = A.element(4,4) = A.element(5,5) =  fact;
	covar = A * covar * A.t();
}

void CTrans2DAffine::changeP0(const C2DPoint &newP0)
{ 
	double dx = newP0.x - P0.x;
	double dy = newP0.y - P0.y;
	this->a0 += this->a1 * dx + this->a2 * dy;
	this->b0 += this->b1 * dx + this->b2 * dy;
	this->P0 = newP0;
};

void CTrans2DAffine::getDerivatives(double x, double y, Matrix &Ax, Matrix &Ay) const
{ 
	Ax.ReSize(6,1);
	Ay.ReSize(6,1);

	x -= P0.x;
	y -= P0.y;
	Ax.element(0,0) = 1.0;
	Ax.element(1,0) = x;
	Ax.element(2,0) = y;
	Ax.element(3,0) = 0.0;
	Ax.element(4,0) = 0.0;
	Ax.element(5,0) = 0.0;

	Ay.element(0,0) = 0.0;
	Ay.element(1,0) = 0.0;
	Ay.element(2,0) = 0.0;
	Ay.element(3,0) = 1.0;
	Ay.element(4,0) = x;
	Ay.element(5,0) = y;
};

void CTrans2DAffine::getDerivativesCoords(double x, double y, Matrix &B) const
{ 
	B.ReSize(2,2);
	B.element(0,0) = this->a1;
	B.element(0,1) = this->a2;
	B.element(1,0) = this->b1;
	B.element(1,1) = this->b2;
};	  

void CTrans2DAffine::addVec(const Matrix &dx, int index0)
{
	this->a0 += dx.element(index0, 0); ++index0;
	this->a1 += dx.element(index0, 0); ++index0;
	this->a2 += dx.element(index0, 0); ++index0;
	this->b0 += dx.element(index0, 0); ++index0;
	this->b1 += dx.element(index0, 0); ++index0; 
	this->b2 += dx.element(index0, 0); 
};
