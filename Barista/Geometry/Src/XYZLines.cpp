//============================================================================

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      IPI, University of Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//============================================================================

#include <strstream>
using namespace std;

#include <float.h>
#include "XYZLines.h"

//*****************************************************************************
//
// c l a s s   C X Y Z _ L i n e _ B a s e
//
//*****************************************************************************

double CXYZLineBase::epsilon(DBL_EPSILON);

//=============================================================================

double CXYZLineBase::getEpsilon()
{
  return CXYZLineBase::epsilon;
};

//=============================================================================

void CXYZLineBase::setEpsilon( double Epsilon )
{
	epsilon = fabs(Epsilon);
};


//*****************************************************************************
//
// E n d   o f   c l a s s   C X Y Z _ L i n e _ B a s e
//
//*****************************************************************************


//*****************************************************************************
//
// c l a s s   C X Y Z L i n e
//
//*****************************************************************************


//============================================================================

CXYZLine::CXYZLine(const C3DPoint &p1, const C3DPoint &p2) : point (p1)
{
	this->direction = p2 - p1;
	this->direction /= this->direction.getNorm();
};

//============================================================================

CXYZLine::CXYZLine(const CXYZLine &line) : point(line.point), 
                                           direction(line.direction) 
{};

//============================================================================

CXYZLine::CXYZLine(const CXYZRay &ray) : point(ray.getPoint()), 
                                           direction(ray.getDirection()) 
{};

//============================================================================

CXYZLine::CXYZLine(const CXYZSegment &segment) : point (segment.getPoint1())
{  
	this->direction = segment.getPoint2() - this->point;
	this->direction /= this->direction.getNorm();
}

//============================================================================

double CXYZLine::getDistance(const C3DPoint &p) const
{
	C3DPoint distVector = getBasePoint(p) - p;
	return distVector.getNorm();
};

//=============================================================================

bool CXYZLine::isParallel(const CXYZLine &line) const
{
	C3DPoint crossProd = this->direction.crossProduct(line.direction); 
	return crossProd.getNorm() < getEpsilon();
}

//=============================================================================

bool CXYZLine::isParallel(const CXYZRay &ray) const
{ 
	C3DPoint crossProd = this->direction.crossProduct(ray.getDirection());
	return crossProd.getNorm() < getEpsilon();
};

//=============================================================================

bool CXYZLine::isParallel(const CXYZSegment &segment) const
{
	C3DPoint crossProd = this->direction.crossProduct(segment.getDirection());
	return crossProd.getNorm() < getEpsilon();
};

//=============================================================================

eIntersect CXYZLine::intersect(const CXYZLine &line ) const
{
	C3DPoint pointDifference = line.point - this->point;

	if (this->isParallel(line))
	{
		if (fabs(this->direction.x) > fabs(this->direction.y ))
		{
			if ( fabs(this->direction.x) > fabs(this->direction.z) )
			{
				double ds = pointDifference.x / this->direction.x;
				if (fabs(pointDifference.y - this->direction.y * ds ) < getEpsilon() &&
					fabs(pointDifference.z - this->direction.z * ds ) < getEpsilon())
					return eIncluded;
			}
			else
			{
				// Z largest component of this->direction
				// if you change something here, don't forget to change it below as well
				double ds = pointDifference.z / this->direction.z ;
				if (fabs(pointDifference.x - this->direction.x * ds ) < getEpsilon() &&
					fabs(pointDifference.y - this->direction.y * ds ) < getEpsilon())
					return eIncluded;
			};
		}
		else
		{
			if (fabs(this->direction.y) > fabs(this->direction.z))
			{
				// Y largest component of this->direction
				double ds = (line.point.y - this->point.y ) / this->direction.y ;
				if (fabs(pointDifference.x - this->direction.x * ds ) < getEpsilon() &&
					fabs(pointDifference.z - this->direction.z * ds ) < getEpsilon() )
					return eIncluded;
			}
			else
			{
				// again Z largest component of this->direction
				double ds = pointDifference.z / this->direction.z ;
				if (fabs(pointDifference.x - this->direction.x * ds ) < getEpsilon() &&
					fabs(pointDifference.y - this->direction.y * ds ) < getEpsilon() )
					return eIncluded;
			};
		};

		return eParallel;  	// the lines are parallel, but not identical
	};


	pointDifference /= pointDifference.getNorm();

	double detCopl = (pointDifference.crossProduct(this->direction)).innerProduct(line.direction);

	if (detCopl > getEpsilon()) return eNoIntersection;

	return eIntersection;
};

//=============================================================================

eIntersect CXYZLine::intersect(const CXYZRay &ray) const
{ 
	return ray.intersect(*this); 
};

//=============================================================================

eIntersect CXYZLine::intersect(const CXYZSegment &segment) const
{ 
	return segment.intersect(*this); 
};


//=============================================================================

eIntersect CXYZLine::getIntersection(const CXYZLine &line, C3DPoint &p) const
{
	eIntersect intersectVal = this->intersect(line);

 
	if (intersectVal == eIntersection)
	{
		C3DPoint pointDifference = line.point - this->point;

		double detCopl = (pointDifference.crossProduct(this->direction)).innerProduct(line.direction) / 
			              pointDifference.getNorm();

		if (detCopl > getEpsilon()) return eNoIntersection;

		C3DPoint v1xv2 = this->direction.crossProduct(line.direction);
		v1xv2 /= v1xv2.getNorm();
		C3DPoint nVector = v1xv2.crossProduct(this->direction);

		double my = -nVector.innerProduct(pointDifference) / nVector.innerProduct(line.direction);

		nVector = v1xv2.crossProduct(line.direction);

		double lambda = nVector.innerProduct(pointDifference) / nVector.innerProduct(this->direction);

		p = (this->getPointOnLine(lambda) + line.getPointOnLine(my)) * 0.5;
	};

  
	return intersectVal;
};

//=============================================================================

eIntersect CXYZLine::getIntersection(const CXYZRay &ray, C3DPoint &p) const
{ 
	return ray.getIntersection(*this, p); 
};

//=============================================================================

eIntersect CXYZLine::getIntersection(const CXYZSegment &segment, C3DPoint &p) const
{ 
	return segment.getIntersection(*this, p); 
};

//=============================================================================

double CXYZLine::getDistance(const CXYZLine &line) const
{
	eIntersect intersectVal = this->intersect(line);

	if (intersectVal == eIncluded) return 0.0;

	C3DPoint pointDifference = line.point - this->point;

	if (intersectVal == eParallel) return pointDifference.getNorm();

	C3DPoint v1xv2 = this->direction.crossProduct(line.direction);	
	v1xv2 /= v1xv2.getNorm();
	
	C3DPoint nVector = v1xv2.crossProduct(this->direction);
	double my = -nVector.innerProduct(pointDifference) / nVector.innerProduct(line.direction);

	nVector = v1xv2.crossProduct(line.direction);
	double lambda = nVector.innerProduct(pointDifference) / nVector.innerProduct(this->direction);

	C3DPoint diff = this->getPointOnLine(lambda) - line.getPointOnLine(my);

	return diff.getNorm();
};

//=============================================================================

double CXYZLine::getDistance(const CXYZRay &ray) const
{ 
	return ray.getDistance(*this); 
};

//=============================================================================

double CXYZLine::getDistance(const CXYZSegment &segment) const
{ 
	return segment.getDistance(*this); 
};

//=============================================================================

//*****************************************************************************
//
// E n d   o f   c l a s s   C X Y Z L i n e
//
//*****************************************************************************


//*****************************************************************************
//
// c l a s s   C X Y Z R a y
//
//*****************************************************************************

CXYZRay::CXYZRay(const C3DPoint &p1, const C3DPoint &p2) : point(p1)
{
  this->direction = p2 - p1;
  this->direction  /= this->direction.getNorm();
};

//=============================================================================

CXYZRay::CXYZRay(const CXYZLine &line) : point(line.getPoint() ), 
                                         direction(line.getDirection())
{};

//=============================================================================

CXYZRay::CXYZRay(const CXYZRay &ray) : point(ray.point), direction(ray.direction)
{};

//=============================================================================

CXYZRay::CXYZRay(const CXYZSegment &segment) : point(segment.getPoint1()), 
                                               direction(segment.getDirection())
{};

//=============================================================================

double CXYZRay::getDistance(const C3DPoint &p) const
{
	C3DPoint baseP = this->getBasePoint(p);
	C3DPoint dist = baseP - this->point;
	if (dist.innerProduct(this->direction) >= 0 ) dist = baseP - p;
	return dist.getNorm();
};

//=============================================================================

bool CXYZRay::isParallel( const CXYZLine &line) const
{
	C3DPoint crossProd = this->direction.crossProduct(line.getDirection());
	return crossProd.getNorm() < getEpsilon();
}

//=============================================================================

bool CXYZRay::isParallel(const CXYZRay &ray) const
{
	C3DPoint crossProd = this->direction.crossProduct(ray.getDirection());
	return crossProd.getNorm() < getEpsilon();
};

//=============================================================================

bool CXYZRay::isParallel(const CXYZSegment &segment) const
{
	C3DPoint crossProd = this->direction.crossProduct(segment.getDirection());
	return crossProd.getNorm() < getEpsilon();
};

//=============================================================================

eIntersect CXYZRay::intersect(const CXYZLine &line) const
{
	CXYZLine lin1(*this);
	C3DPoint intersection;
	eIntersect intersectVal = lin1.getIntersection(line, intersection);

	if (intersectVal == eIntersection)
	{
		if (!this->containsPoint(intersection)) intersectVal = eNoIntersection;
	};

	return intersectVal;
};

//=============================================================================

eIntersect CXYZRay::intersect(const CXYZRay &ray) const
{
	CXYZLine lin1(*this);
	CXYZLine lin2(ray);
	C3DPoint intersection;

	eIntersect intersectVal = lin1.getIntersection(lin2, intersection);

	if (intersectVal == eIntersection)
	{
		if ((!this->containsPoint(intersection) ) || (!ray.containsPoint(intersection)))
			intersectVal = eNoIntersection;
	}
	else if (intersectVal == eIncluded)
	{
		if ((!this->containsPoint(ray.point)) && (!ray.containsPoint(this->point))) 
			intersectVal = eNoIntersection;
	};

	return intersectVal;
};

//=============================================================================

eIntersect CXYZRay::intersect(const CXYZSegment &segment) const
{ 
	return segment.intersect(*this); 
};

//=============================================================================

eIntersect CXYZRay::getIntersection(const CXYZLine &line, C3DPoint &p) const
{
	CXYZLine lin1(*this);
	eIntersect intersectVal = lin1.getIntersection(line, p);
	if ((intersectVal == eIntersection) && (!this->containsPoint(p)))
		intersectVal = eNoIntersection;
 
	return intersectVal;
};

//=============================================================================

eIntersect CXYZRay::getIntersection(const CXYZRay &ray, C3DPoint &p) const
{
	CXYZLine lin1(*this);
	CXYZLine lin2(ray);

	eIntersect intersectVal = lin1.getIntersection(ray, p);

	if ((intersectVal == eIntersection) && ((!this->containsPoint(p)) || (!ray.containsPoint(p))))
		intersectVal = eNoIntersection;
	else if (intersectVal == eIncluded)
	{
		if ((!this->containsPoint(ray.point)) || (!ray.containsPoint(this->point))) 
			intersectVal = eNoIntersection;
	};

	return intersectVal;
};

//=============================================================================

eIntersect CXYZRay::getIntersection(const CXYZSegment &segment, C3DPoint &p) const
{ 
	return segment.getIntersection(*this, p); 
};

//=============================================================================

double CXYZRay::getDistance(const CXYZLine &line) const
{
	eIntersect intersectVal = this->intersect(line); 

	if (intersectVal == eIncluded) return 0.0;

	C3DPoint pointDifference = line.getPoint() - this->point;

	if (intersectVal == eParallel ) return pointDifference.getNorm();

	C3DPoint v1xv2 = this->direction.crossProduct(line.getDirection());
	v1xv2 /= v1xv2.getNorm();
	C3DPoint nVector = v1xv2.crossProduct(line.getDirection());

	double lambda = nVector.innerProduct(pointDifference) / nVector.innerProduct(this->direction );
	if ((lambda < 0) && (fabs(lambda) > getEpsilon()))
		return line.getDistance(this->point);

	nVector = v1xv2.crossProduct(this->direction);
	double my = -nVector.innerProduct(pointDifference) / nVector.innerProduct(line.getDirection());

	C3DPoint diff = this->getPointOnLine(lambda) - line.getPointOnLine(my);
	return diff.getNorm();
};

//=============================================================================

double CXYZRay::getDistance(const CXYZRay &ray) const
{
	eIntersect intersectVal = this->intersect(ray);

	if (intersectVal == eIncluded) return 0.0;

	C3DPoint pointDifference = ray.point - this->point;

	if (intersectVal == eParallel) return pointDifference.getNorm();

	C3DPoint v1xv2 = this->direction.crossProduct(ray.getDirection());
	v1xv2 /= v1xv2.getNorm();

	C3DPoint nVector = v1xv2.crossProduct(ray.getDirection() );

	double lambda = nVector.innerProduct(pointDifference) / nVector.innerProduct( this->direction );

	nVector = v1xv2.crossProduct( this->direction );

	double my = -nVector.innerProduct(pointDifference) / nVector.innerProduct(ray.getDirection());

	C3DPoint diff;

	if (lambda < 0)
	{
		if (my < 0) diff = this->point - ray.point;
		else return ray.getDistance(this->point);
	}
	else
	{
		if (my < 0) return this->getDistance(ray.point);
		else diff = getPointOnLine(lambda) - ray.getPointOnLine(my);
	};

	return diff.getNorm();
};

//=============================================================================

double CXYZRay::getDistance(const CXYZSegment &segment) const
{ 
	return segment.getDistance(*this); 
};

//=============================================================================

//*****************************************************************************
//
// E n d   o f   c l a s s   C X Y Z R a y
//
//*****************************************************************************
//*****************************************************************************
//
// c l a s s   C X Y Z S e g m e n t
//
//*****************************************************************************

 CXYZSegment::CXYZSegment() : point1(0.0, 0.0, 0.0), point2(1.0, 0.0, 0.0), 
	                          direction(1.0, 0.0, 0.0) 
 {};

//=============================================================================

 CXYZSegment::CXYZSegment(const C3DPoint &p1, const C3DPoint &p2) :
                          point1(p1), point2(p2) 
{
	this->initDirection();
};

//=============================================================================

 CXYZSegment::CXYZSegment(const CXYZLine &line, double length) :
                          point1(line.getPoint()), 
						  point2(line.getPoint() + length * line.getDirection()),
						  direction(line.getDirection()) 
{};

//=============================================================================

 CXYZSegment::CXYZSegment(const CXYZRay &ray, double length) :
                          point1(ray.getPoint() ),
						  point2(ray.getPoint() + length * ray.getDirection()),
						  direction(ray.getDirection()) 
{};

//=============================================================================

 CXYZSegment::CXYZSegment(const CXYZSegment &segment) :
						  point1(segment.point1), point2(segment.point2),
						  direction(segment.direction) 
{};

//=============================================================================

void CXYZSegment::initDirection() 
{
	this->direction = this->point2 - this->point1;
	this->direction /= this->direction.getNorm();
}

//=============================================================================

double CXYZSegment::getSquareDistance(const C3DPoint &p) const
{
	C3DPoint dist = p - this->point1;
  
  
	if (dist.innerProduct(this->direction) >= 0 )
	{
		dist = p - this->point2;
		if (dist.innerProduct(this->direction) <= 0 )   
			dist = p - this->getBasePoint(p);
	};

	return dist.getSquareNorm();
};

//=============================================================================

double CXYZSegment::getDistance(const C3DPoint &p) const
{
	return sqrt(this->getSquareDistance(p));
};
  
//=============================================================================

double CXYZSegment::getSquareDistanceFromLine(const C3DPoint &p) const
{	
	C3DPoint dist = p - this->getBasePoint(p);
  
	return dist.getSquareNorm();
};
  
//=============================================================================

double CXYZSegment::getDistanceFromLine(const C3DPoint &p) const
{
	return sqrt(this->getSquareDistanceFromLine(p));
};

//=============================================================================

bool CXYZSegment::isParallel(const CXYZLine &line) const
{
  C3DPoint crossProd = this->direction.crossProduct(line.getDirection());
  return crossProd.getNorm() < getEpsilon();
}

//=============================================================================

bool CXYZSegment::isParallel(const CXYZRay &ray) const
{
  C3DPoint crossProd = this->direction.crossProduct(ray.getDirection());
  return crossProd.getNorm() < getEpsilon();
};

//=============================================================================

bool CXYZSegment::isParallel(const CXYZSegment &segment) const
{
  C3DPoint crossProd = this->direction.crossProduct(segment.direction);
  return crossProd.getNorm() < getEpsilon();
};

//=============================================================================

eIntersect CXYZSegment::intersect(const CXYZLine &line) const
{
	CXYZLine lin1(*this);
	C3DPoint intersection; 
	eIntersect intersectVal = lin1.getIntersection(line, intersection);
  
	if ( intersectVal == eIntersection)
	{
		if (!this->containsPoint(intersection))
			intersectVal = eNoIntersection;
	};

	return intersectVal;
};

//=============================================================================

eIntersect CXYZSegment::intersect(const CXYZRay &ray) const
{
	CXYZLine lin1(*this);
	CXYZLine lin2(ray);
	C3DPoint intersection;

	eIntersect intersectVal = lin1.getIntersection(lin2, intersection);

	if (intersectVal == eIntersection)
	{
		if ((!this->containsPoint(intersection)) || (!ray.containsPoint(intersection)))
			intersectVal = eNoIntersection;
	}
	else if (intersectVal == eIncluded)
	{
		if ((!ray.containsPoint(this->point1)) && (!ray.containsPoint(this->point2))) 
			intersectVal = eNoIntersection;
	};

	return intersectVal;
};

//=============================================================================

eIntersect CXYZSegment::intersect(const CXYZSegment &segment) const
{
	CXYZLine lin1(*this);
	CXYZLine lin2(segment);
	C3DPoint intersection;
	eIntersect intersectVal = lin1.getIntersection(lin2, intersection);

	if (intersectVal == eIntersection)
	{
		if ((!this->containsPoint(intersection)) || (!segment.containsPoint(intersection)))
			intersectVal = eNoIntersection;
	}
	else if ( intersectVal == eIncluded )
	{
		if ((!segment.containsPoint(this->point1)) && (!segment.containsPoint(this->point2)))
			intersectVal = eNoIntersection;
	};

	return intersectVal;
};

//=============================================================================

eIntersect CXYZSegment::getIntersection(const CXYZLine &line, C3DPoint &p) const
{
	CXYZLine lin1(*this);

	eIntersect intersectVal = lin1.getIntersection(line, p);
	if ((intersectVal == eIntersection ) && (!this->containsPoint(p)))
		intersectVal = eNoIntersection;

	return intersectVal;
};

//=============================================================================

eIntersect CXYZSegment::getIntersection(const CXYZRay &ray, C3DPoint &p) const
{
	CXYZLine lin1(*this);
	CXYZLine lin2(ray);

	eIntersect intersectVal = lin1.getIntersection(lin2, p);
	if ((intersectVal == eIntersection ) && ((!this->containsPoint(p))) ||  (!ray.containsPoint(p)))
		intersectVal = eNoIntersection;

	return intersectVal;
};

//=============================================================================

eIntersect CXYZSegment::getIntersection(const CXYZSegment &segment, C3DPoint &p) const
{
	CXYZLine lin1(*this);
	CXYZLine lin2(segment);

	eIntersect intersectVal = lin1.getIntersection(lin2, p);
	if ((intersectVal == eIntersection ) && ((!this->containsPoint(p))) || (!segment.containsPoint(p)))
		intersectVal = eNoIntersection;

	return intersectVal;
};

//=============================================================================

double CXYZSegment::getDistance(const CXYZLine &line) const
{
	eIntersect intersectVal = this->intersect(line);

	if (intersectVal == eIncluded) return 0.0;

	C3DPoint pointDifference = line.getPoint() - this->point1;

	if (intersectVal == eParallel ) return pointDifference.getNorm();

	C3DPoint v1xv2 = this->direction.crossProduct(line.getDirection());
	v1xv2 /= v1xv2.getNorm();

	C3DPoint nVector = v1xv2.crossProduct(line.getDirection());

	double lambda = nVector.innerProduct(pointDifference) / nVector.innerProduct(this->direction);

	if ((lambda < 0) && (fabs(lambda) > getEpsilon())) return line.getDistance(this->point1);

	if (lambda > this->getLength()) return line.getDistance(this->point2);

	nVector = v1xv2.crossProduct(this->direction);

	double my = -nVector.innerProduct(pointDifference) / nVector.innerProduct(line.getDirection());


	C3DPoint diff = this->getPointOnLine(lambda) - line.getPointOnLine(my);
	return diff.getNorm();
};

//=============================================================================

double CXYZSegment::getDistance(const CXYZRay &ray) const
{
	eIntersect intersectVal = this->intersect(ray);

	if (intersectVal == eIncluded ) return 0;

	C3DPoint pointDifference = ray.getPoint() - this->point1;

	if (intersectVal == eParallel) return pointDifference.getNorm();

	C3DPoint v1xv2 = this->direction.crossProduct(ray.getDirection());
	v1xv2 /= v1xv2.getNorm();

	C3DPoint nVector = v1xv2.crossProduct(ray.getDirection());

	double lambda = nVector.innerProduct(pointDifference) / nVector.innerProduct(this->direction);

	nVector = v1xv2.crossProduct(this->direction);

	double my = -nVector.innerProduct(pointDifference) / nVector.innerProduct(ray.getDirection());

	C3DPoint diff;

	if (lambda < 0)
	{
		if (my < 0) diff = this->point1 - ray.getPoint();
		else return ray.getDistance(this->point1);
	}
	else if (lambda > this->getLength())
	{
		if (my > 0) return ray.getDistance(this->point2);
		else diff = this->point2 - ray.getPoint();
	}
	else
	{
		if (my < 0) return this->getDistance(ray.getPoint());
		else diff = this->getPointOnLine(lambda) - ray.getPointOnLine(my);
	};

	return diff.getNorm();
};

//=============================================================================

double CXYZSegment::getDistance(const CXYZSegment &segment) const
{
	eIntersect intersectVal = this->intersect(segment);

	if (intersectVal == eIncluded ) return 0;

	C3DPoint pointDifference = segment.point1 - point1;

	if (intersectVal == eParallel ) return pointDifference.getNorm();

	C3DPoint v1xv2 = this->direction.crossProduct(segment.getDirection());
	v1xv2 /= v1xv2.getNorm();

	C3DPoint nVector = v1xv2.crossProduct(segment.getDirection());

	double lambda = nVector.innerProduct(pointDifference) / nVector.innerProduct(this->direction);

	nVector = v1xv2.crossProduct(this->direction);

	double my = -nVector.innerProduct(pointDifference) / nVector.innerProduct(segment.getDirection());

	C3DPoint diff;

	if (lambda < 0)
	{
		if (my < 0 ) diff = this->point1 - segment.point1;
		else if (my <= segment.getLength() ) return segment.getDistance(this->point1);
		else diff = this->point1 - segment.point2;
	}
	else if (lambda > this->getLength())
	{
		if (my < 0) diff = this->point2 - segment.point1;
		else if (my > segment.getLength()) diff = this->point2 - segment.point2;
		else return segment.getDistance(this->point2);
	}
	else
	{
		if (my < 0) return this->getDistance(segment.point1 );
		if (my > segment.getLength()) return this->getDistance(segment.point2);
		else diff = this->getPointOnLine(lambda) - segment.getPointOnLine(my);
	};

	return diff.getNorm();
};
 
//=============================================================================
   	  
double CXYZSegment::getAverageDistance(const CXYZSegment &segment) const
{
	CXYZSegment seg1(segment);
	CXYZSegment seg2(*this);

	double dist = FLT_MAX;
	int p1 =  this->project(seg1);
	int p2 =  segment.project(seg2);

	if (p1 || p2)
	{
		if (p1)
		{
			dist = (seg1.getDistanceFromLine(this->point1) + seg1.getDistanceFromLine(this->point2)) * 0.5;
			if (p2)
			{
				if (seg1.getLength() < seg2.getLength())
					dist = (seg2.getDistanceFromLine(segment.point1) + seg2.getDistanceFromLine(segment.point2)) * 0.5;
			}

		}
		else
		{
			dist = (seg2.getDistanceFromLine(segment.point1) + seg2.getDistanceFromLine(segment.point2)) * 0.5;
		}
	}

	return dist;
};

//=============================================================================

int CXYZSegment::project(CXYZSegment &segment) const
{
	float sLength = (float) segment.getLength();
	C2DPoint lengths(segment.getDirection().innerProduct(this->point1 - segment.point1),
					 segment.getDirection().innerProduct(this->point2 - segment.point1));

	if (lengths.x > lengths.y) lengths.swap();

	if ((lengths.y < 0) || (lengths.x > sLength)) return 0;

	if ((lengths.x > 0) || (lengths.y < sLength))
	{
		C3DPoint p1 = segment.point1;
		if (lengths.x > 0) p1 = segment.getPointOnLine(lengths.x);

		C3DPoint p2 = segment.point2;
		if (lengths.y < sLength) p2 = segment.getPointOnLine(lengths.y);
		segment = CXYZSegment(p1, p2);
	};

	return 1;
};

//=============================================================================

bool CXYZSegment::isIdentical(CXYZSegment &segment, float cosAlpha, 
							  float distThreshold, int projectionFlag) const
{
	CXYZSegment segmentLocal(segment);
	double inProd = this->direction.innerProduct(segment.direction);
	if (fabs(inProd) < cosAlpha) return false;

	float sLength = (float) segment.getLength();
	C2DPoint lengths(segment.direction.innerProduct(this->point1 - segment.point1),
					 segment.direction.innerProduct(this->point2 - segment.point1));

	if (lengths.x > lengths.y) lengths.swap();

	if ((lengths.y < 0) || (lengths.x > sLength)) return 0;

	if ((lengths.x > 0) || (lengths.y < sLength))
	{
		C3DPoint p1 = segment.point1;
		if (lengths.x > 0) p1 = segment.getPointOnLine(lengths.x);

		C3DPoint p2 = segment.point2;
		if (lengths.y < sLength) p2 = segment.getPointOnLine(lengths.y);
		segmentLocal = CXYZSegment(p1, p2);

		if (projectionFlag)  segment = segmentLocal;
	};

	return ((this->getSquareDistance(segmentLocal.point1) < distThreshold) ||
		    (this->getSquareDistance(segmentLocal.point2) < distThreshold));
};

//=============================================================================

void CXYZSegment::invert()
{
	C3DPoint pos = this->point1;
	this->point1 = this->point2;
	this->point2 = pos;
	this->direction = -this->direction;
};

//=============================================================================

CCharString CXYZSegment::getString(CCharString deliminator, int width, int decimals) const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(decimals);

	oss.width(width); 
	oss << this->point1.x << " ";
	oss.width(width); 
	oss << this->point1.y << " ";
	oss.width(width); 
	oss << this->point1.z << deliminator.GetChar();
		
	oss.width(width); 
	oss << this->point2.x << " ";
	oss.width(width); 
	oss << this->point2.y << " ";
	oss.width(width); 
	oss << this->point2.z << ends;

	char *z = oss.str();

	CCharString ret(z);

	delete[] z;
	return ret;
};

//=============================================================================

//*****************************************************************************
//
// E n d   o f   c l a s s   C X Y Z S e g m e n t
//
//*****************************************************************************
