#include <math.h>
#include "TransGridToGeographic.h"
#include "utilities.h"

CTransGridToGeographic::CTransGridToGeographic(const CReferenceSystem &sysFrom, 
											   const CReferenceSystem &sysTo) : 
				CTrans3D(sysFrom, sysTo)
{
}

CTransGridToGeographic::~CTransGridToGeographic()
{
}

void CTransGridToGeographic::transform(C3DPoint &out, const C3DPoint &in) 
{
	this->systemFrom.gridToGeographic(out, in);
}

void CTransGridToGeographic::transform(CXYZPoint &out, const CXYZPoint &in) 
{
	this->systemFrom.gridToGeographic(out, in);
}
	  
void CTransGridToGeographic::getDerivatives(Matrix &derivatives, const C3DPoint &p) const
{
	this->systemFrom.derivativesGridToGeographic(derivatives, p);
};
