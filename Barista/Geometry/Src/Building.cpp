#ifdef WIN32
#include "vraniml.h"

#include "Building.h"
#include "XYPolyLine.h"
#include "XYZPolyLine.h"
#include "XYZPolyLineArray.h"



CBuilding::CBuilding():offset(0,0,0), solid(0), 
			pmin(FLT_MAX, FLT_MAX, FLT_MAX), pmax(-FLT_MAX, -FLT_MAX, -FLT_MAX),
			currentFace(0),isSelected(false)
{
}

CBuilding::CBuilding(const CBuilding &other) : label(other.label), points(other.points), 
			offset(other.offset), pmin(other.pmin),pmax(other.pmax),
			currentFace(0),isSelected(other.isSelected), history(other.history)
{
	this->solid = new vrSolid(*other.solid);
}

CBuilding::~CBuilding()
{
	if (this->solid) delete this->solid;
}

void CBuilding::setOffset(const C3DPoint &Offset)
{
	this->offset = Offset;
};

/**
 * Makes a copy of "this" Building
 * @return the copied building
 */

CBuilding CBuilding::copy()
{
    CBuilding NewBuilding(*this);

    return NewBuilding;
}

CBuilding &CBuilding::operator = (const CBuilding &source)
{
	if (this != &source)
	{
		this->label   = source.label;
		this->points  = source.points;
		this->offset  = source.offset;
		this->pmin    = source.pmin;
		this->pmax    = source.pmax;
		this->history = source.history;

		if (this->solid) delete this->solid;
		if (source.solid) this->solid  = new vrSolid(*source.solid);
		else this->solid = 0;
		this->currentFace = 0;
	}

	return *this;
};

int CBuilding::getPointCount() const
{
    return this->points.GetSize();
}

//================================================================================
	
double CBuilding::getArea() const
{
	CXYZPolyLineArray floors;
	this->getFloors(floors);
	double area = 0.0;

	if (this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();

		while(fPos)
		{	
			vrFace *pface = this->solid->faces.GetNext(fPos);
			if (pface->GetColor(vrWhite) == vrFirebrick) 
			{
				vrLoop *pOuterLoop = pface->GetLoopOut();

				LISTPOS lPos = pface->loops.GetHeadPosition();

				while(lPos)
				{	
					vrLoop *pLoop = pface->loops.GetNext(lPos);
					CXYZPolyLine poly;
					LISTPOS hePos = pLoop->halfedges.GetHeadPosition();
					while(hePos)
					{
						vrHalfEdge *pHe = pLoop->halfedges.GetNext(hePos);
						poly.addPoint(*this->points.getAt(pHe->GetVertex()->GetId()));
					}
					poly.closeLine();
					double dArea = poly.getAreaAbs();
					if (pLoop == pOuterLoop) area += dArea;
					else area -= dArea;
				}
			}
		}
	}

	return area;
};

//================================================================================
void CBuilding::deleteBuilding()
{
    this->points.RemoveAll();
	delete this->solid;
	this->solid = 0;
	this->pmin.x = this->pmin.y = this->pmin.z = FLT_MAX;
	this->pmax.x = this->pmax.y = this->pmax.y = -FLT_MAX;
	this->history.clear();
}

int CBuilding::getNWalls() const
{
	int nwalls = 0; 
	if (this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();

		while(fPos)
		{	
			vrFace *pface = this->solid->faces.GetNext(fPos);
			if (pface->GetColor(vrWhite) == vrWhite) ++nwalls;
		}
	}

	return nwalls;
};

int CBuilding::getNRoofs() const
{
	int nroofs = 0;

	if (this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();

		while(fPos)
		{	
			vrFace *pface = this->solid->faces.GetNext(fPos);
			if (pface->GetColor(vrWhite) == vrFirebrick) ++nroofs;
		}
	}

	return nroofs;
};

int CBuilding::getNFloors() const
{
	int nfloors = 0;

	if (this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();

		while(fPos)
		{	
			vrFace *pface = this->solid->faces.GetNext(fPos);
			if (pface->GetColor(vrWhite) == vrCornflowerBlue) ++nfloors;
		}
	}

	return nfloors;
};

CLabel CBuilding::getLabel() const
{
    return this->label;
}

void CBuilding::setLabel(const CLabel &Label)
{
    this->label = Label;
}


/*
CBuilding CBuilding::transform(CBuilding trans)
{
    CBuilding NewBuilding;

    for (int i = 0; i < this->getPointCount(); i++)
    {
        NewBuilding.addPoint(trans.transform(this->getPoint(i)));
    }

    return NewBuilding;
}
*/

C2DPoint CBuilding::getCentre() const
{
	C2DPoint centre;
	centre.x = centre.y = 0.0;

	for (int j = 0; j < this->getPointCount(); j++)
	{
		CXYZPoint* point = this->points.getAt(j);

		centre.x += (*point)[0];
		centre.y += (*point)[1];
	}
	centre /= this->getPointCount();

	return centre;
};

bool CBuilding::isPointInCurrentFace(C3DPoint p) const
{
	bool ret = true;
	vrLoop *outerLoop = 0;
	if (this->currentFace) outerLoop = this->currentFace->GetLoopOut();

	if (!outerLoop) ret = false;
	else
	{
		p -= this->offset;

		if (!isPointInLoop(outerLoop, p)) ret = false;
		else		
		{
			bool isInInnerLoop = false;
			LISTPOS lpos = this->currentFace->loops.GetHeadPosition();
			
			while (lpos && (!isInInnerLoop))
			{
				vrLoop *pLoop = this->currentFace->loops.GetNext(lpos);
				if (pLoop != outerLoop)
				{
					isInInnerLoop = isPointInLoop(pLoop, p);
				}
			}				
			
			ret = !isInInnerLoop;
		}		
	}

	return ret;
}
	
vrSolid *CBuilding::getPrism(CXYZPolyLine &Roof, CXYZPolyLine &Floor, int firstId, 
							 bool innerCourt, bool addToHistory)
{
	vrSolid *prism = new vrSolid;

	vrVertexArray vertices;
	MFInt32 faceIndices;

	PrimitiveDescriptorType typ = eBase;
	CCharString name = "Base";
	int index = -1;

	if (firstId > 0)
	{
		if (innerCourt) 
		{
			typ = eCourtYard;
			name.Format("C%d", firstId);
		}
		else 
		{
			typ = eSuperStructure;
			name.Format("S%d", firstId);
		}

		if (this->currentFace)
		{
			vrLoop *loopOut = this->currentFace->GetLoopOut();
			if (loopOut)
			{
				LISTPOS hPos = loopOut->halfedges.GetHeadPosition();
				if (hPos)
				{
					vrHalfEdge *he = loopOut->halfedges.GetNext(hPos);
					index = this->history.getIndexOfDescriptor(he->GetVertex()->GetId());
				}
			}
		}
	}
 
	CPrimitiveDescriptor prim(typ, Roof.getPointCount(), name);
	prim.setParentDescriptor(index);

	for (int i = 0; i < Roof.getPointCount(); ++i)
	{		
		faceIndices.AddValue(i);
		C3DPoint p = *Roof.getPoint(i) - this->offset;
		SFVec3f pos(float(p.x), float(p.y), float(p.z));
		vrVertex *pVertex = new vrVertex(pos);
		pVertex->SetId(firstId + 2 * i + 1);
		prim.roofPoint(i) = pVertex->GetId();
		vertices.AddValue(pVertex);
	};

	faceIndices.AddValue(-1);
	prism->BuildFromIndexSet(vertices, faceIndices);

	vertices.Clear();
	faceIndices.Clear();

	LISTPOS fPos1 = prism->faces.GetHeadPosition();

	for (int i = 0; i < Floor.getPointCount(); ++i)
	{		
		faceIndices.AddValue(i);
		C3DPoint p = *Floor.getPoint(i) - this->offset;
		SFVec3f pos(float(p.x), float(p.y), float(p.z));
		vrVertex *pVertex = new vrVertex(pos);
		pVertex->SetId(firstId + 2 * i);
		prim.floorPoint(i) = pVertex->GetId();
		vertices.AddValue(pVertex);
	};

	faceIndices.AddValue(-1);

	vrSolid solidFloor;
	solidFloor.BuildFromIndexSet(vertices, faceIndices);
	solidFloor.Revert();

	LISTPOS fPos2 = solidFloor.faces.GetHeadPosition();

	if (!fPos1 || !fPos2)  
	{
		delete prism;
		prism = 0;
	}
	else
	{
		vrFace *pRoofFace  = prism->faces.GetNext(fPos1);
		vrFace *pFloorFace = solidFloor.faces.GetNext(fPos2);
		pRoofFace->SetColor(vrFirebrick);
		pFloorFace->SetColor(vrCornflowerBlue);
	 
		prism->InsertFace(pFloorFace);

		int i1 = Roof.getPointCount() - 1;
	
		for (int i2 = 0; i2 < Roof.getPointCount() && prism; ++i2)
		{		
			vertices.Clear();
			faceIndices.Clear();

			C3DPoint p = *Floor.getPoint(i1) - this->offset;
			SFVec3f pos(float(p.x), float(p.y), float(p.z));
			vrVertex *pVertex = new vrVertex(pos);
			pVertex->SetId(firstId + 2 * i1);
			vertices.AddValue(pVertex);
			faceIndices.AddValue(0);

			p = *Floor.getPoint(i2) - this->offset;
			pos = SFVec3f(float(p.x), float(p.y), float(p.z));
			pVertex = new vrVertex(pos);
			pVertex->SetId(firstId + 2 * i2);
			vertices.AddValue(pVertex);
			faceIndices.AddValue(1);

			p = *Roof.getPoint(i2) - this->offset;
			pos = SFVec3f(float(p.x), float(p.y), float(p.z));
			pVertex = new vrVertex(pos);
			pVertex->SetId(firstId + 2 * i2 + 1);
			vertices.AddValue(pVertex);
			faceIndices.AddValue(2);

			p = *Roof.getPoint(i1) - this->offset;
			pos = SFVec3f(float(p.x), float(p.y), float(p.z));
			pVertex = new vrVertex(pos);
			pVertex->SetId(firstId + 2 * i1 + 1);
			vertices.AddValue(pVertex);
			faceIndices.AddValue(3);

			vrSolid solidWall;
			solidWall.BuildFromIndexSet(vertices, faceIndices);
			LISTPOS fPos3 = solidWall.faces.GetHeadPosition();

			if (!fPos3)  
			{
				delete prism;
				prism = 0;
			}
			else
			{
				vrFace *pWallFace  = solidWall.faces.GetNext(fPos3);
				pWallFace->SetColor(vrWhite);			
				prism->InsertFace(pWallFace);
			}
			i1 = i2;
		}
	}

	if (addToHistory) this->history.addPrimitiveDescriptor(prim);

	return prism;
};

void CBuilding::checkMinMax(const C3DPoint &p)
{
	if (p.x < this->pmin.x) this->pmin.x = p.x;
	if (p.y < this->pmin.y) this->pmin.y = p.y;
	if (p.z < this->pmin.z) this->pmin.z = p.z;
	if (p.x > this->pmax.x) this->pmax.x = p.x;
	if (p.y > this->pmax.y) this->pmax.y = p.y;
	if (p.z > this->pmax.z) this->pmax.z = p.z;
};
	
bool CBuilding::isPointInLoop(vrLoop *loop, const C3DPoint &p) const
{
	LISTPOS hePos = loop->halfedges.GetHeadPosition();
	bool ret = true;

	int nInters = 0;

	while (hePos && ret)
	{
		vrHalfEdge *he1 = loop->halfedges.GetNext(hePos);
		vrHalfEdge *he2 = he1->next;
		if (!he2) ret = false;
		else
		{
			float x1 = he1->GetVertex()->x;
			float x2 = he2->GetVertex()->x;
			if ((x1 >= p.x) || (x2 > p.x))
			{
				float y1 = he1->GetVertex()->y;
				float y2 = he2->GetVertex()->y;
				if (((y1 <= p.y) && (y2 > p.y)) ||
					((y1 >= p.y) && (y2 < p.y)))
				{
					float diff = y2 - y1;
					if (fabs(diff) > FLT_EPSILON)
					{
						float x = x1 + (x2-x1) * (p.y -y1) / diff;
						if (x > p.x)
							++nInters;
					}
				}
			}
		}
	}

	if (ret) ret = ((nInters % 2) != 0);

	return ret;
};

bool CBuilding::getIntersection(vrFace *face, const C3DPoint &p0, const C3DPoint &dir, 
								C3DPoint &intersection) const
{
	bool ret = true;
	vrLoop *outerLoop = face->GetLoopOut();
	if (!outerLoop) ret = false;
	else	
	{	
		C3DPoint P0 = p0 - this->offset;
		SFVec3f hlp = face->GetNormal();
		C3DPoint normal(hlp.x,hlp.y,hlp.z);
		double d = face->GetD();

		double dot = dir.innerProduct(normal);
		if (fabs(dot) < FLT_EPSILON) ret = false;
		else
		{
			double t = (-d - normal.innerProduct(P0)) / dot;
			if (t < 0) ret = false;
			else
			{
				intersection = P0 + t * dir; 
	  
				if (!isPointInLoop(outerLoop, intersection)) ret = false;
				else
				{
					bool isInInnerLoop = false;
					LISTPOS lpos = face->loops.GetHeadPosition();
					while (lpos && (!isInInnerLoop))
					{
						vrLoop *pLoop = face->loops.GetNext(lpos);
						if (pLoop != outerLoop)
						{
							isInInnerLoop = isPointInLoop(pLoop, intersection);
						}
					}				
					if (isInInnerLoop) ret = false;
					else 
					{
						intersection += this->offset;
					}
				}		
			}
		}
	}

	return ret;
};
	
bool CBuilding::verticalPoleAtCurrentFace(const C3DPoint &p01, const C3DPoint &dir1, 		                          
										  const C3DPoint &p02, const C3DPoint &dir2, 
										  C3DPoint &plow, C3DPoint &phigh)
{
	bool ret = true;

	if (this->currentFace)
	{
		C3DPoint P01 = p01 - this->offset;
		C3DPoint P02 = p02 - this->offset;
		Matrix A(4,4), t1(4,1), t(5,1);
		SymmetricMatrix N(5);
		N = 0;

		A.element(0,0) = dir1.z; A.element(0,1) = 0.0;    A.element(0,2) = -dir1.x; A.element(0,3) =  0.0;
		A.element(1,0) = 0.0;    A.element(1,1) = dir1.z; A.element(1,2) = -dir1.y; A.element(1,3) =  0.0;
		A.element(2,0) = dir2.z; A.element(2,1) = 0;      A.element(2,2) =  0.0;    A.element(2,3) = -dir2.x;
		A.element(3,0) = 0.0;    A.element(3,1) = dir2.z; A.element(3,2) =  0.0;    A.element(3,3) = -dir2.y;
		Matrix At = A.t();
		t1.element(0,0) = P01.x * dir1.z - P01.z * dir1.x;
		t1.element(1,0) = P01.y * dir1.z - P01.z * dir1.y;
		t1.element(2,0) = P02.x * dir2.z - P02.z * dir2.x;
		t1.element(3,0) = P02.y * dir2.z - P02.z * dir2.y;

		t1 = At * t1;
		Matrix SubN = At * A;
		for (int i = 0; i < 4; ++i)
		{
			for (int j = i; j < 4; ++j)
			{
				N.element(i,j) = SubN.element(i, j);
			}
			t.element(i, 0) = t1.element(i,0);
		}

		SFVec3f hlp = this->currentFace->GetNormal();
		C3DPoint normal(hlp.x,hlp.y,hlp.z);
		double d = this->currentFace->GetD();
		N.element(4,0) = normal.x;
		N.element(4,1) = normal.y;
		N.element(4,2) = normal.z;
		t.element(4,0) = -d;

		Try
		{
			Matrix delta = N.i() * t;
			plow.x = delta.element(0,0);
			plow.y = delta.element(1,0);
			plow.z = delta.element(2,0);
			phigh.x = plow.x;
			phigh.y = plow.y;
			phigh.z = delta.element(3,0);
			plow  += this->offset;
			phigh += this->offset;
		}
		CatchAll
		{
			ret = false;
		}
	}
	else ret = false;

	return ret;
};

bool CBuilding::setCurrentFace(const C3DPoint &p0, const C3DPoint &dir, 		 			 
							   C3DPoint &intersection)
{
	bool ret = true;

	if (!this->solid) ret = false;
	else
	{
		vector<vrFace *> roofFaces;
		vector<C3DPoint> Intersections;
		vector<double> IntersectionDistances;
		C3DPoint currentIntersection;

		LISTPOS fPos = this->solid->faces.GetHeadPosition();
		while (fPos)
		{
			vrFace *pFace  = this->solid->faces.GetNext(fPos);
			if (pFace->GetColor(vrWhite) == vrFirebrick)
			{
				if (getIntersection(pFace, p0, dir, currentIntersection))
				{
					roofFaces.push_back(pFace);
					Intersections.push_back(currentIntersection);
					IntersectionDistances.push_back(dir.innerProduct(currentIntersection - p0));
				}
			}
		}

		if (roofFaces.size() < 1) ret = false;
		else
		{
			int indexMin = 0;

			for (unsigned int i = 1; i < roofFaces.size(); ++i)
			{
				if (IntersectionDistances[i] < IntersectionDistances[indexMin])
					indexMin = i;
			}

			this->currentFace = roofFaces[indexMin];
			intersection = Intersections[indexMin];
		}
	}

	return ret;
}
	
bool CBuilding::interpolateAtCurrentFace(C3DPoint &p)
{
	bool ret = true;
	if (!this->currentFace) ret = false;
	else
	{
		p -= this->offset;
		SFVec3f hlp = this->currentFace->GetNormal();
		C3DPoint normal(hlp.x,hlp.y,hlp.z);
		double d = this->currentFace->GetD();

		if (fabs(normal.z) < FLT_EPSILON) ret = false;
		else
		{
			p.z = (-d - normal.x * p.x - normal.y * p.y) / normal.z;
			
		}
		p += this->offset;
	}

	return ret;
};

bool CBuilding::intersectWithCurrentFace(const C3DPoint &p0, 										
										 const C3DPoint &dir, 		 
										 C3DPoint &intersection)
{
	bool ret = true;
	if (!this->currentFace) ret = setCurrentFace(p0, dir, intersection);
	else ret = getIntersection(this->currentFace, p0, dir, intersection);

	return ret;
};
	
void CBuilding::resetCurrentFace()
{
	this->currentFace = 0;
};

bool CBuilding::addInnerCourtyard(CXYZPolyLine &Roof, CXYZPolyLine &Floor, bool resetCurrent)
{
	bool ret = true;
	if ((Roof.getPointCount() != Floor.getPointCount()) || (Roof.getPointCount() < 3) ||
		(!this->solid) || (!this->currentFace)) ret= false;
	else
	{
		Roof.closeLine();
		Floor.closeLine();
		ePOLYLINEORDER order = Roof.getOrder();
		if (order == eCLOCKWISE) 
		{
			Roof.reversePolyLine();
			Floor.reversePolyLine();
		}

		int firstId = this->points.GetSize();

		for (int i = 0; i < Roof.getPointCount(); ++i)
		{
			CXYZPoint *pF = Floor.getPoint(i);
			CXYZPoint *pR = Roof.getPoint(i);
			this->points.add(*pF);
			this->points.add(*pR);
			checkMinMax(*pF);
			checkMinMax(*pR);
		}

		vrSolid *innerCourtyard = getPrism(Roof, Floor, firstId, true, true); 
		if (!innerCourtyard) ret = false;
		else
		{
			vrFace *floorFace = 0;
			vrFace *roofFace = 0;
			LISTPOS fPos = innerCourtyard->faces.GetHeadPosition();
			while (fPos && ((!floorFace) || (!roofFace)))
			{
				vrFace *pFace = innerCourtyard->faces.GetNext(fPos);
				SFColor col = pFace->GetColor(vrWhite);
				if (col == vrCornflowerBlue) floorFace = pFace;
				else if (col == vrFirebrick) roofFace  = pFace;
			}

			vrFace *currentFloorFace = 0;

			fPos = this->solid->faces.GetHeadPosition();
			while (fPos && (!currentFloorFace))
			{
				vrFace *pFace = this->solid->faces.GetNext(fPos);
				if (pFace->GetColor(vrWhite) == vrCornflowerBlue) currentFloorFace = pFace;
			}

			if ((!floorFace) || (!roofFace) || (!currentFloorFace)) ret = false;
			else
			{
				int id = this->solid->faces.GetCount() + 1;
				floorFace->SetIndex(id);
				this->solid->InsertFace(floorFace);
				innerCourtyard->RemoveFace(floorFace);
				floorFace = this->solid->findFace(id);
				floorFace->Revert();
				this->solid->lkfmrh(currentFloorFace, floorFace);

				id = this->solid->faces.GetCount() + 1;
				roofFace->SetIndex(id);
				this->solid->InsertFace(roofFace);
				innerCourtyard->RemoveFace(roofFace);
				roofFace = this->solid->findFace(id);
				roofFace->Revert();
				this->solid->lkfmrh(this->currentFace, roofFace);
						
				innerCourtyard->Revert();
				
				LISTPOS fPos = innerCourtyard->faces.GetHeadPosition();

				while (fPos)  
				{
					vrFace *pface  = innerCourtyard->faces.GetNext(fPos);
					id = this->solid->faces.GetCount() + 1;
					this->solid->InsertFace(pface);
					pface->SetIndex(id);
				}

			}

			delete innerCourtyard;		
		}

		if (this->solid) 
		{	
			this->solid->CalcPlaneEquations();
			this->solid->Renumber();
		}
	};

	if (resetCurrent) resetCurrentFace();

	return ret;
};

int iTest = 0;
void test()
{
	iTest = 1;
 	double d = 1.0, z1 = 1.0, z2 = 0.0;
 	CXYZPoint p(-d, -d, 1);
 	CXYZPolyLine roof1, floor1;
 	roof1.addPoint(p);
 	p.z = z2;
 	floor1.addPoint(p);
 	p.x = d;
 	floor1.addPoint(p);
 	p.z = z1;
    roof1.addPoint(p);
 	p.y = d;
    roof1.addPoint(p);
	p.z = z2;
 	floor1.addPoint(p);
 	p.x = -d;
 	floor1.addPoint(p);
 	p.z = z1;
    roof1.addPoint(p);
 	CBuilding bld;
 	bld.addRoofPlane(roof1, floor1, true);
	roof1.deleteLine();
	floor1.deleteLine();

	z1 = 2.0, z2 = 0.0;

	p.x = 0.0; p.y = -2.0, p.z = z1;
 	roof1.addPoint(p);
 	p.z = z2;
 	floor1.addPoint(p);
 	p.x = 2.0;
 	floor1.addPoint(p);
 	p.z = z1;
    roof1.addPoint(p);
 	p.y = 0.0;
    roof1.addPoint(p);
 	p.z = z2;
 	floor1.addPoint(p);
 	p.x = 0.0;
 	p.z = 1.0;
 	floor1.addPoint(p);
 	p.z = z1;
    roof1.addPoint(p);
 	bld.booleanUnion(roof1, floor1);
}

bool CBuilding::addRoofPlane(CXYZPolyLine &Roof, CXYZPolyLine &Floor, bool setRoofToFirst)
{
	bool ret = true;
	if ((Roof.getPointCount() != Floor.getPointCount()) || (Roof.getPointCount() < 2)) ret= false;
	else
	{
		if (!this->solid)
		{
			for (int i = 0; i < Roof.getPointCount(); ++i)
			{
				this->offset += *Floor.getPoint(i);
			};
			this->offset /= (double) Floor.getPointCount();
			this->offset.x = floor(this->offset.x + 0.5);
			this->offset.y = floor(this->offset.y + 0.5);
			this->offset.z = floor(this->offset.z + 0.5);
		}

		if (Roof.getPointCount() > 2)
		{	
			Roof.closeLine();
			Floor.closeLine();
			ePOLYLINEORDER order = Roof.getOrder();
			if (order == eCLOCKWISE) 
			{
				Roof.reversePolyLine();
				Floor.reversePolyLine();
			}
		}

		int firstId = this->points.GetSize();

		for (int i = 0; i < Roof.getPointCount(); ++i)
		{
			CXYZPoint *pF = Floor.getPoint(i);
			CXYZPoint *pR = Roof.getPoint(i);
			this->points.add(*pF);
			this->points.add(*pR);
			checkMinMax(*pF);
			checkMinMax(*pR);
		}

		if (!this->solid && (Roof.getPointCount() > 2))
		{
			this->solid = getPrism(Roof, Floor, firstId, false, true); 
			if (!this->solid) ret = false;
			else if (setRoofToFirst)
			{
				this->currentFace = 0;
				LISTPOS fPos = this->solid->faces.GetHeadPosition();
				while (fPos && (!this->currentFace))
				{
					vrFace *pFace = this->solid->faces.GetNext(fPos);
					if (pFace->GetColor(vrWhite) == vrFirebrick) this->currentFace = pFace;
				}
			}
		}
		else if (!this->currentFace) ret = false;
		else
		{
			vrSolid *superStructure = getPrism(Roof, Floor, firstId, false, true); 
			if (!superStructure) ret = false;
			else
			{
				vrFace *floorFace = 0;
				LISTPOS fPos = superStructure->faces.GetHeadPosition();
				while (fPos && (!floorFace))
				{
					vrFace *pFace = superStructure->faces.GetNext(fPos);
					if (pFace->GetColor(vrWhite) == vrCornflowerBlue) floorFace = pFace;
				}
				if (!floorFace) ret = false;
				else
				{
					int id = this->solid->faces.GetCount() + 1;
					floorFace->SetIndex(id);
					this->solid->InsertFace(floorFace);
					superStructure->RemoveFace(floorFace);
					floorFace = this->solid->findFace(id);
					this->solid->lkfmrh(this->currentFace, floorFace);
					
					LISTPOS fPos = superStructure->faces.GetHeadPosition();

					while (fPos)  
					{
						vrFace *pface  = superStructure->faces.GetNext(fPos);
						id = this->solid->faces.GetCount() + 1;
						pface->SetIndex(id);
						this->solid->InsertFace(pface);
						if (setRoofToFirst && pface->GetColor(vrWhite) == vrFirebrick)
						{
							this->currentFace = this->solid->findFace(id);
						}
					}
				}

				delete superStructure;
			}
		}

		if (this->solid) 
		{	
			this->solid->CalcPlaneEquations();
			this->solid->Renumber();
		}
	};

	if (!setRoofToFirst || !ret) resetCurrentFace();

	return ret;
};

/**
 * Serialized storage of a CBuilding to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CBuilding::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("label", this->getLabel().GetChar());

	if (this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();

		while (fPos)  
		{
			token = pStructuredFileSection->addToken();
			vrFace *pface  = this->solid->faces.GetNext(fPos);
			CCharString faceString = getFaceDescriptor(pface);
			token->setValue("FACE", faceString.GetChar());	
		}
	}
    token = pStructuredFileSection->addToken();
	token->setValue("xyzpoints", this->points.getFileName()->GetChar());


	CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->points.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->offset.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->history.serializeStore(child);
}

/**
 * Restores a CBuilding from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CBuilding::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	bool hasHistory = false;
	CSerializable::serializeRestore(pStructuredFileSection);

	CCharStringArray faceDescriptors;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("label"))
			this->label = CLabel(token->getValue());
		else if(key.CompareNoCase("FACE"))
		{
			faceDescriptors.Add(token->getValue());
		}
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CXYZPointArray"))
        {
            this->points.serializeRestore(child);
        }
		else if (child->getName().CompareNoCase("C3DPoint"))
        {
            this->offset.serializeRestore(child);
        }
		else if (child->getName().CompareNoCase("CPrimitiveDescriptorArray"))
        {
            this->history.serializeRestore(child);
			hasHistory = true;
        }

	}
	
	initSolidFromFaceDescriptors(faceDescriptors);

	for (int i = 0; i < this->points.GetSize(); ++i)
	{
		checkMinMax(*points.getAt(i));
	}

	if (!hasHistory && this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();
		vrFace *roof  = 0;
		vrFace *floor = 0;
		while (fPos && !roof && !floor)
		{
			vrFace *face = this->solid->faces.GetNext(fPos);
			if (face->GetColor(vrWhite) == vrFirebrick) roof = face;
			else if (face->GetColor(vrWhite) == vrCornflowerBlue) floor = face;
		}

		if (roof && floor)
		{  
			int nmax = roof->GetLoopOut()->halfedges.GetCount();
			if (floor->GetLoopOut()->halfedges.GetCount() > nmax) 
				nmax = floor->GetLoopOut()->halfedges.GetCount();
			CPrimitiveDescriptor prim(eBase,nmax,"Base");
			prim.setParentDescriptor(-1);

			LISTPOS hPos = roof->GetLoopOut()->halfedges.GetHeadPosition();
			int i = 0;

			while (hPos)
			{
				vrHalfEdge *he = roof->GetLoopOut()->halfedges.GetNext(hPos);
				prim.roofPoint(i) = he->GetVertex()->GetId();
				++i;
			}

			for(; i < nmax; ++i) prim.roofPoint(i) = -1;

			hPos = floor->GetLoopOut()->halfedges.GetHeadPosition();
			i = 0;

			while (hPos)
			{
				vrHalfEdge *he = floor->GetLoopOut()->halfedges.GetNext(hPos);
				prim.floorPoint(i) = he->GetVertex()->GetId();
				++i;
			}

			for(; i < nmax; ++i) prim.floorPoint(i) = -1;
		}
	}
}

/**
 * reconnects pointers
 */
void CBuilding::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CBuilding::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Gets ClassName
 * returns a string
 */
string CBuilding::getClassName() const
{
	return string("CBuilding");
}

bool CBuilding::isa(string& className) const
{
    if (className == "CBuilding")
        return true;

    return false;
}

/**
 * Flag if CBuilding is modified
 * returns bool
 */
bool CBuilding::isModified()
{
    return this->bModified;
}
	
char *CBuilding::getLoopDescriptor(vrLoop *loop)
{
	ostrstream oss;

	LISTPOS hpos = loop->halfedges.GetHeadPosition();

	while (hpos)  
	{
		vrHalfEdge *pHe = loop->halfedges.GetNext(hpos);

		oss << pHe->GetVertex()->GetId() <<" ";
	}

	oss << " -1 " << ends;

	return oss.str();
};

CCharString CBuilding::getFaceDescriptor(vrFace *face)
{
	ostrstream oss;
	oss << " Loops ( " << face->loops.GetCount() << " ) ";

	LISTPOS lpos = face->loops.GetHeadPosition();

	vrLoop *pOuterLoop = face->GetLoopOut();
	char *z = getLoopDescriptor(pOuterLoop);
	
	oss << z;
	delete [] z;

	while (lpos)  
	{
		vrLoop *pLoop  = face->loops.GetNext(lpos);
		if (pOuterLoop != pLoop)
		{
			z = getLoopDescriptor(pLoop);	
			oss << z;
			delete [] z;
		};
	};

	SFColor col = face->GetColor(vrWhite);

	if (col == vrFirebrick)
	{
		oss << " ROOF  ";
	}
	else if (col == vrCornflowerBlue)
	{
		oss << " FLOOR ";
	}
	else
	{
		oss << " WALL  ";
	}

	oss << " COLOUR (" << (int) (col.x * 255.0) << " " 
		               << (int) (col.y * 255.0) << " " 
					   << (int) (col.z * 255.0) << " ) TEXTURE ( )" << ends;

	z = oss.str();
	CCharString faceString(z);
	delete [] z;

	return faceString;
};

vrSolid *CBuilding::getLoopSolid(istrstream &Descriptor)
{
	vrVertexArray vertices;
	MFInt32 faceIndices;

	int Id = 0;
	int i = 0;
	Descriptor >> Id;

	do
	{	
		C3DPoint p = *this->points.getAt(Id) - this->offset;
		faceIndices.AddValue(i);
		SFVec3f pos(float(p.x), float(p.y), float(p.z));
		vrVertex *pVertex = new vrVertex(pos);
		pVertex->SetId(Id);
		vertices.AddValue(pVertex);
	
		Descriptor >> Id;
		++i;
	} while (Id != -1);

	faceIndices.AddValue(-1);

	vrSolid *solidLoop = new vrSolid;
	solidLoop->BuildFromIndexSet(vertices, faceIndices);

	return solidLoop;
};

vrSolid *CBuilding::getFaceSolid(CCharString Descriptor)
{
	int idx = Descriptor.Find("Loops (");

	Descriptor = Descriptor.Right(Descriptor.GetLength() - (idx + 7));

	idx = Descriptor.Find(")");

	CCharString hlp = Descriptor.Left(idx);

	Descriptor = Descriptor.Right(Descriptor.GetLength() - (idx + 1));

	SFColor col;
	idx = Descriptor.Find(" ROOF ");
	if (idx >= 0) col = vrFirebrick;
	else
	{
		idx = Descriptor.Find(" FLOOR ");	
		if (idx >= 0) col = vrCornflowerBlue;
		else
		{
			idx = Descriptor.Find(" WALL ");	
			col = vrWhite;
		}
	}

	CCharString loopStr = Descriptor.Left(idx);
	Descriptor = Descriptor.Right(Descriptor.GetLength() - (idx + 7));

	int nloops = 0;

	istrstream iss(hlp.GetChar());
	iss >> nloops;
		
	istrstream issLp(loopStr.GetChar());

	vrSolid *solidFace = getLoopSolid(issLp);
	vrFace *face = solidFace->findFace(0); 
	face->CalcEquation(); 
	face->SetColor(col);

	for (int i = 1; i < nloops; ++i)
	{
		vrSolid *solidLoop = getLoopSolid(issLp);
		vrFace *loopFace = solidLoop->findFace(0); 
		loopFace->SetIndex(1);

		solidFace->InsertFace(loopFace);
		solidLoop->RemoveFace(loopFace);
		loopFace = solidFace->findFace(1);
  		delete solidLoop;
		solidFace->lkfmrh(face, loopFace);	
	};

	return solidFace;
};

void CBuilding::initSolidFromFaceDescriptors(const CCharStringArray &faceDescriptors)
{
	if (this->solid) delete this->solid;
	this->solid = new vrSolid;

	for (int i = 0; i < faceDescriptors.GetSize(); ++i)
	{
		CCharString *descriptor = faceDescriptors.GetAt(i);
		vrSolid *faceSolid = this->getFaceSolid(*descriptor);
		LISTPOS fPos = faceSolid->faces.GetHeadPosition();

		if (fPos)  
		{
			vrFace *pface  = faceSolid->faces.GetNext(fPos);
			this->solid->InsertFace(pface);
		}

		delete faceSolid;
	}

	this->solid->CalcPlaneEquations();
};

void CBuilding::addFacePolys(CXYZPolyLineArray &faceloops, vrFace *face) const
{
	LISTPOS lPos = face->loops.GetHeadPosition();

	while(lPos)
	{	
		vrLoop *pLoop = face->loops.GetNext(lPos);
		CXYZPolyLine *pPoly = faceloops.add();
		LISTPOS hePos = pLoop->halfedges.GetHeadPosition();
		while(hePos)
		{
			vrHalfEdge *pHe = pLoop->halfedges.GetNext(hePos);
			pPoly->addPoint(*this->points.getAt(pHe->GetVertex()->GetId()));
		}
		pPoly->closeLine();
	}
}

void CBuilding::getRoofs(CXYZPolyLineArray &roofs) const
{
	roofs.RemoveAll();

	if (this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();

		while(fPos)
		{	
			vrFace *pface = this->solid->faces.GetNext(fPos);
			if (pface->GetColor(vrWhite) == vrCornflowerBlue) addFacePolys(roofs, pface);
		}
	}
};

void CBuilding::getFloors(CXYZPolyLineArray &floors) const
{
	floors.RemoveAll();

	if (this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();

		while(fPos)
		{	
			vrFace *pface = this->solid->faces.GetNext(fPos);
			if (pface->GetColor(vrWhite) == vrFirebrick) addFacePolys(floors, pface);
		}
	}
};

void CBuilding::getWalls(CXYZPolyLineArray &walls) const
{
	walls.RemoveAll();

	if (this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();

		while(fPos)
		{	
			vrFace *pface = this->solid->faces.GetNext(fPos);
			if (pface->GetColor(vrWhite) == vrWhite) addFacePolys(walls, pface);
		}
	}
};

void CBuilding::getAllVerticesAndEdges(CObsPointArray &vertices, 
									   vector<int> &indicesFrom, 
									   vector<int> &indicesTo) const
{
	vertices = this->points;
	if (this->solid)
	{
		indicesFrom.resize(this->solid->edges.GetCount());
		indicesTo.resize(this->solid->edges.GetCount());
		LISTPOS ePos = this->solid->edges.GetHeadPosition();
		int i = 0;

		while (ePos)
		{
			vrEdge *pE = this->solid->edges.GetNext(ePos);
			indicesFrom[i] = pE->GetVertex(HE1)->GetId();
			indicesTo  [i] = pE->GetVertex(HE2)->GetId();
			++i;
		}
	}
};

void CBuilding::getAllLoops(CXYZPolyLineArray &loops) const
{
	loops.RemoveAll();

	if (this->solid)
	{
		LISTPOS fPos = this->solid->faces.GetHeadPosition();

		while(fPos)
		{	
			vrFace *pface = this->solid->faces.GetNext(fPos);
			addFacePolys(loops, pface);
		}
	}
};

void CBuilding::revert()
{
	if (this->solid) this->solid->Revert();
};


double CBuilding::getRoofHeight() const
{
	return (*(this->points.GetAt(1)))[2];
};

double CBuilding::getFloorHeight() const
{
	return (*(this->points.GetAt(0)))[2];
};

CLabel CBuilding::getFirstPointLabel() const
{
	CLabel pLabel;
	CXYZPoint *p = this->points.getAt(0);
	if (p) pLabel = p->getLabel();
	return pLabel;
};

double CBuilding::getHeight() const
{        
	return (getRoofHeight() - getFloorHeight());
};

void CBuilding::writeTextFile(FILE	*fp)
{	
	fprintf(fp,"Building: %s\n", this->label.GetChar());
	fprintf(fp,"Number of points: %d\n", this->getPointCount());

	for (int j = 0; j < this->getPointCount(); j++)
	{
		CXYZPoint *point = this->points.getAt(j);

		fprintf(fp,"Point: %s %12.3lf %12.3lf %12.3lf\n", point->getLabel().GetChar(), point->x, point->y, point->z);
	}
		
	fprintf(fp,"\n");
};

bool CBuilding::writeVRML2File(vrBrowser &Browser, const C3DPoint &reduction, CReferenceSystem *refsys)
{
	bool ret = true;

	if (!this->solid) ret = false;
	else
	{
		vrSolid s(*(this->solid));

		// change vertex coordinates
		LISTPOS vPos = s.verts.GetHeadPosition();
		while (vPos)
		{
			vrVertex *v = s.verts.GetNext(vPos);
			C3DPoint point = (*this->points.getAt(v->GetId()));

			if (refsys) refsys->geographicToGrid(point, point);

			v->x = float(point.x - reduction.x);
			v->y = float(point.y - reduction.y);
			v->z = float(point.z - reduction.z);
		}

		s.CalcPlaneEquations();

	

		SFRotation Rotation = SFRotation (xyz1, -2.094395);
		vrTransform *pTransform = new vrTransform;
		pTransform->SetRotation (Rotation);
		Browser.AddChild (pTransform);

		vrShape      *pShape   = new vrShape;              // create a shape
		vrAppearance *pApp     = new vrAppearance;         // a shape has an appearance
		vrMaterial   *pMat     = new vrMaterial;           // an appearance has a material
		vrDataSet    *pDataSet = new vrIndexedFaceSet(&s); // a shape has a geometry, in this case a data set

		pMat->SetDiffuseColor(vrWhite);
		pApp->SetMaterial(pMat);
		pShape->SetAppearance(pApp);
		pShape->SetGeometry(pDataSet);
		pTransform->AddChild(pShape);

		SFFloat deltaZ = 200.0;
		SFVec3f vrPosition(float((pmin.x + pmax.x) * 0.5 - reduction.x),
			               float((pmin.y + pmax.y) * 0.5 - reduction.y),
			               float((pmin.z + pmax.z) * 0.5 - reduction.z + deltaZ));
		vrPosition.z += deltaZ;
		SFRotation Orientation = SFRotation (SFVec3f(0.0,0.0,-1.0), 0);
		vrViewpoint *pViewpoint = new vrViewpoint;
		pViewpoint->SetDescription (this->label.GetChar());
		pViewpoint->SetPosition (vrPosition);
		pViewpoint->SetOrientation (Orientation);
		pTransform->AddChild (pViewpoint);
	}

  
	return ret;
};


bool CBuilding::writeVRML1File(FILE *fp, const C3DPoint &reduction, CReferenceSystem *refsys)
{
	bool ret = true;
	if (!this->solid) ret = false;
	else
	{
		fprintf(fp,"  Separator\n");
		fprintf(fp,"  {\n");
		fprintf(fp,"      Label\n");
		fprintf(fp,"      {\n");
		fprintf(fp,"          %s\n", this->label.GetChar());
		fprintf(fp,"      }\n");

		fprintf(fp,"      Separator\n");
		fprintf(fp,"      {\n");
		fprintf(fp,"          Coordinate3\n");
		fprintf(fp,"          {\n");
		fprintf(fp,"              point\n");
		fprintf(fp,"              [\n");

		for (int j = 0; j < this->getPointCount(); j++)
		{
			C3DPoint point = (*this->points.getAt(j));

			if (refsys) refsys->geographicToGrid(point, point);

			point.x -= reduction.x;
			point.y -= reduction.y;
			point.z -= reduction.z;
			fprintf(fp,"                  %12.3lf %12.3lf %12.3lf,\n", point.x, point.y, point.z);
		}
		
		fprintf(fp,"              ]\n");
		fprintf(fp,"          }\n");

		fprintf(fp,"          IndexedLineSet\n");
		fprintf(fp,"          {\n");
		fprintf(fp,"              coordIndex\n");
		fprintf(fp,"              [ \n");

		LISTPOS ePos = this->solid->edges.GetHeadPosition();
		while (ePos)
		{
			vrEdge *pE = this->solid->edges.GetNext(ePos);
			fprintf(fp,"                  %d, %d, -1,\n", 
				    pE->GetVertex(HE1)->GetId(), 
					pE->GetVertex(HE2)->GetId());
		}

		fprintf(fp,"              ]\n");
		fprintf(fp,"          }\n");
		fprintf(fp,"      }\n");
		fprintf(fp,"  }\n");
	}
	return ret;
};


//============================================================================

double CBuilding::getFloorHeight(vrSolid *pSolid) const
{
	vrFace *pFloor = this->getFloor(pSolid);
	double h = FLT_MAX;

	if (pFloor) 
	{
		vrLoop *pFloorLoop = pFloor->GetLoopOut();

		LISTPOS hPos = pFloorLoop->halfedges.GetHeadPosition();
		while (hPos) 
		{
			vrHalfEdge *he = pFloorLoop->halfedges.GetNext(hPos);
			float z = he->GetVertex()->z;
			if (z < h) h = z;
		}; 
	}
	else h = 0.0;

	return h;
};

//============================================================================

void CBuilding::setFloorHeight(vrSolid *pSolid, double newHeight)
{
	vrFace *pFloor = this->getFloor(pSolid);
	
	if (pFloor) 
	{
		LISTPOS lPos = pFloor->loops.GetHeadPosition();
		while (lPos) // loop all loops of f
		{
			vrLoop *pFloorLoop = pFloor->loops.GetNext(lPos);
			LISTPOS hPos = pFloorLoop->halfedges.GetHeadPosition();
			while (hPos) 
			{
				vrHalfEdge *he = pFloorLoop->halfedges.GetNext(hPos);
				he->GetVertex()->z = newHeight;
			}; 
		};

		pFloor->CalcEquation();
	};
};

//============================================================================

vrFace *CBuilding::getFloor(vrSolid *pSolid) const
{
	LISTPOS fPos = pSolid->faces.GetHeadPosition();

	while (fPos) // loop all faces of s
	{
		vrFace *f = pSolid->faces.GetNext(fPos);
		if (f->GetColor(vrWhite) == vrCornflowerBlue) return f;
	}

	return 0; 
}
	
vrVertex *CBuilding::getVertexXY(vrFace *face, vrVertex *v) const
{
	LISTPOS lPos = face->loops.GetHeadPosition();
	while (lPos)
	{
		vrLoop *loop = face->loops.GetNext(lPos);
		LISTPOS hPos = loop->halfedges.GetHeadPosition();
		while (hPos)
		{
			vrHalfEdge *he = loop->halfedges.GetNext(hPos);
			vrVertex *vert = he->GetVertex();
			if ((fabs(v->x - vert->x) < FLT_EPSILON) && (fabs(v->y - vert->y) < FLT_EPSILON))
				return vert;

		}
	}

	return 0;
};

void CBuilding::makeVerticesConsistent(vrSolid *pOldSolid, vrSolid *pAddedSolid)
{
	vrFace *floorOld   = this->getFloor(pOldSolid);
	vrFace *floorAdded = this->getFloor(pAddedSolid);

	LISTPOS vPos = this->solid->verts.GetHeadPosition();
	vector<vrVertex *> newVertices;

	while (vPos)
	{
		vrVertex *v = this->solid->verts.GetNext(vPos);
		int id = (int) v->GetId();
		if (id < 0)
		{
			vrVertex *oldV = getVertexXY(floorOld, v);
			if (!oldV) oldV = getVertexXY(floorAdded, v);
			if (!oldV) newVertices.push_back(v);
			else
			{
				v->SetId(oldV->GetId());
				v->z = oldV->z;
			}
		}
	}

	int indexMin = this->points.GetSize();
	int index = indexMin;
	
	CLabel PtLabel("I");
	PtLabel.SetNumber(index);
	
	CXYZPoint P;

	for (unsigned int i = 0; i < newVertices.size(); ++i)
	{
		vrVertex *v = newVertices[i];
		v->SetId(index);

		P.setLabel(PtLabel);
		P.x = double(v->x) + this->offset.x;
		P.y = double(v->y) + this->offset.y;
		bool isFloor = false;
		vrHalfEdge *he = v->GetHe();
		if (he->GetFace()->GetColor(vrWhite) == vrCornflowerBlue) isFloor = true;
		else
		{
			he = he->GetMate();
			if (he->GetFace()->GetColor(vrWhite) == vrCornflowerBlue) isFloor = true;
		}

		if (!isFloor) P.z = double(v->z) + this->offset.z;
		else
		{
			vrVertex *pPrev = he->Prev()->GetVertex();
			vrVertex *pNext = he->Next()->GetVertex();
			double z1 = pPrev->z;
			double z2 = pNext->z;
			double dx = (pPrev->x - v->x);
			double dy = (pPrev->y - v->y);
			double d1 = dx * dx + dy * dy;
			if (d1 < 0.01) d1 = 0.1;
			d1 = 1.0/d1;
			dx = (pNext->x - v->x);
			dy = (pNext->y - v->y);
			double d2 = dx * dx + dy * dy;
			if (d2 < 0.01) d2 = 0.1;
			d2 = 1.0/d2;
			P.z = (z1 * d1 + z2 * d2) / (d1 + d2) + this->offset.z;
		}

		this->points.add(P);
		++PtLabel;
		++index;
	}
};

//============================================================================

bool CBuilding::booleanUnion(CXYZPolyLine &Roof, CXYZPolyLine &Floor)
{
	bool ret = true;
	if ((Roof.getPointCount() != Floor.getPointCount()) || (Roof.getPointCount() < 3) ||
		(!this->solid) || (!this->currentFace)) ret= false;
	else
	{
		Roof.closeLine();
		Floor.closeLine();
		ePOLYLINEORDER order = Roof.getOrder();
		if (order == eCLOCKWISE) 
		{
			Roof.reversePolyLine();
			Floor.reversePolyLine();
		}
	
		int firstId = this->points.GetSize();

		for (int i = 0; i < Roof.getPointCount(); ++i)
		{
			CXYZPoint *pF = Floor.getPoint(i);
			CXYZPoint *pR = Roof.getPoint(i);
			this->points.add(*pF);
			this->points.add(*pR);
			checkMinMax(*pF);
			checkMinMax(*pR);
		}

		vrSolid *superStructure = getPrism(Roof, Floor, firstId, false, true); 
		if (!superStructure) ret = false;
		else
		{
			Try
			{
				vrSolid *pSolidA = (vrSolid *) this->solid->Clone();
				vrSolid *pSolidB = (vrSolid *) superStructure->Clone();
				vrSolid *pSolidUnion = new vrSolid;

				double height = 0;

				double heightA = getFloorHeight(pSolidA);
				double heightB = getFloorHeight(pSolidB);

				if (heightA > heightB) height = heightB;
				else height = heightA;

				setFloorHeight(pSolidA, height - 2.0);
				setFloorHeight(pSolidB, height - 1.0);
			
/*				pSolidA->Write("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\solidA.brp");
				pSolidB->Write("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\solidB.brp");
				pSolidA->WriteVRML("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\solidA.wrl");
				pSolidB->WriteVRML("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\solidB.wrl");
*/			
				pSolidUnion->BoolOp(pSolidA, pSolidB, &pSolidUnion, vrUNION, 0);

/*				pSolidUnion->WriteVRML("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\union.brp");
				pSolidUnion->WriteVRML("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\union.wrl");
*/				
				delete pSolidA;
				delete pSolidB;
				pSolidA = pSolidB = 0;

				pSolidUnion->Renumber();

				SFVec3f normal(0.0f,0.0f,1.0f);

				vrPlane plane(normal, -height);

				pSolidUnion->Split(plane, &pSolidB, &pSolidA );
				pSolidA->Renumber();

				delete pSolidUnion;
				delete pSolidB;
				pSolidUnion = pSolidA;

				vrFace *pFloor = 0;
				LISTPOS fPos = pSolidUnion->faces.GetHeadPosition();

				normal = -normal;

				while (fPos && (!pFloor))
				{
					vrFace *pFace = pSolidUnion->faces.GetNext(fPos);
					pFace->CalcEquation ();

					if (normal == pFace->GetNormal()) pFloor = pFace;
				};

				if (pFloor) pFloor->SetColor(vrCornflowerBlue);

				pSolidB = this->solid;
				this->solid = pSolidUnion;
/*				pSolidB->Write("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\solidA.brp");
				superStructure->Write("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\solidB.brp");
				this->solid->Write("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\unionraw.brp");
				pSolidB->WriteVRML("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\solidA.wrl");
				superStructure->WriteVRML("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\solidB.wrl");
				this->solid->WriteVRML("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\unionraw.wrl");
*/			
				makeVerticesConsistent(pSolidB, superStructure); 

//				this->solid->Write("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\union.brp");
//				this->solid->WriteVRML("F:\\Projects\\CRC-SI\\Melbourne Quickbird Stereo\\union.wrl");
				
				this->solid->CalcPlaneEquations();
				this->solid->Renumber();

				delete pSolidB;
				delete superStructure;
			}
			
			CatchAll //(char* error) 
			{
				if (superStructure) delete superStructure;
				ret = false;
			}
		}
	}

	this->currentFace = 0;

	return ret;
};


#else

#include "Building.h"
// under linux we just use the Building class as a shell, so that we can still use functions with
// buildings as parameters or return values


CBuilding::CBuilding()
{
}

CBuilding::CBuilding(const CBuilding &other)
{
	
}

CBuilding::~CBuilding()
{

}



#endif // WIN32
 