

#include "ReferenceSystem.h"
#include "TransGeographicToGeocentric.h"
#include "TransGeocentricToGeographic.h"
#include "TransGeographicToGrid.h"
#include "TransGeocentricToGrid.h"
#include "TransGridToGeocentric.h"
#include "TransGridToGeographic.h"

#include "Trans3DFactory.h"

CTrans3D *CTrans3DFactory::initTransformation(const CReferenceSystem &systemFrom, 
											  const CReferenceSystem &systemTo)
{
	CTrans3D *trafo = 0;
	eCoordinateType fromTyp = systemFrom.getCoordinateType();
	eCoordinateType toTyp   = systemTo.getCoordinateType();

	if ((fromTyp != eUndefinedCoordinate) && (toTyp != eUndefinedCoordinate))
	{
		if (fromTyp == toTyp)
		{
			if (fromTyp != eGRID)
				trafo = new CTrans3D(systemFrom, systemTo);
			else if (systemFrom.isUTM() == systemTo.isUTM())
				trafo = new CTrans3D(systemFrom, systemTo);
		}
		else if (fromTyp == eGEOGRAPHIC) trafo = initGeogToX(systemFrom, systemTo);
		else if (fromTyp == eGEOCENTRIC) trafo = initGeocToX(systemFrom, systemTo);
		else if (fromTyp == eGRID) trafo = initGridToX(systemFrom, systemTo);
	}

	return trafo;
};

CTrans3D *CTrans3DFactory::initGeogToX(const CReferenceSystem &systemFrom, 										
									   const CReferenceSystem &systemTo)
{
	CTrans3D *trafo = 0;
	eCoordinateType toTyp = systemTo.getCoordinateType();

	if (toTyp == eGEOCENTRIC) trafo = new CTransGeographicToGeocentric(systemFrom, systemTo);
	else if (toTyp == eGRID) trafo = new CTransGeographicToGrid(systemFrom, systemTo);

	return trafo;
}

CTrans3D *CTrans3DFactory::initGeocToX(const CReferenceSystem &systemFrom, 										
									   const CReferenceSystem &systemTo)
{
	CTrans3D *trafo = 0;
	eCoordinateType toTyp = systemTo.getCoordinateType();

	if (toTyp == eGEOGRAPHIC) trafo = new CTransGeocentricToGeographic(systemFrom, systemTo);
	else if (toTyp == eGRID) trafo = new CTransGeocentricToGrid(systemFrom, systemTo);

	return trafo;
}


CTrans3D *CTrans3DFactory::initGridToX(const CReferenceSystem &systemFrom, 
									   const CReferenceSystem &systemTo)
{
	CTrans3D *trafo = 0;
	eCoordinateType toTyp = systemTo.getCoordinateType();

	if (toTyp == eGEOGRAPHIC) trafo = new CTransGridToGeographic(systemFrom, systemTo);
	else if (toTyp == eGEOCENTRIC) trafo = new CTransGridToGeocentric(systemFrom, systemTo);

	return trafo;
}

