//============================================================================

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      IPI, University of Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//============================================================================

#include <float.h>

#include "3Dplane.h"

//*****************************************************************************

C3DPlane::C3DPlane(  ): point(0.0, 0.0, 0.0), normal(0.0, 0.0, 1.0), planeConstant(0.0)
{}

//===========================================================================

C3DPlane::C3DPlane(const C3DPoint &Point, const C3DPoint &Normal) :
                   point(Point), normal(Normal)
{
	this->normal /= this->normal.getNorm();
	this->planeConstant = this->normal.innerProduct(this->point);
}

//===========================================================================

C3DPlane::C3DPlane(const C3DPoint &p1, const C3DPoint &p2, const C3DPoint &p3):
                   point(p1)
{
	this->normal = (p2 - p1).crossProduct(p3 - p1);
	this->normal /= this->normal.getNorm();
	this->planeConstant = this->normal.innerProduct(this->point);
}

//===========================================================================

C3DPlane::C3DPlane(const C3DPoint &Point, const CXYZLine &line):
                   point(Point)
{
	this->normal = line.getDirection().crossProduct(Point - line.getPoint());
	this->normal /= this->normal.getNorm();
	this->planeConstant = this->normal.innerProduct(this->point);
}

//===========================================================================

C3DPlane::C3DPlane(const CXYZRay & ray) :
                   point(ray.getPoint()), normal(ray.getDirection())
{
	this->planeConstant = this->normal.innerProduct(this->point);
}

//===========================================================================

C3DPlane::C3DPlane(const C3DPlane &plane):
                   point(plane.point), normal(plane.normal), planeConstant(plane.planeConstant)
{}
	  
//===========================================================================

double C3DPlane::getObliqueness() const
{
	double o;
	double z = fabs(this->normal.z);

	if (z < FLT_EPSILON) 
	{
		z = double(FLT_EPSILON);
		o = (1.0 - z) / z;
	}
	else
	{
		o = this->normal.getNorm2D() / z;
	}

	return o;
};

//===========================================================================

double C3DPlane::getHeight(const C2DPoint &p) const
{
	return this->getHeight(p.x, p.y);
};

//===========================================================================

double C3DPlane::getHeight(double x, double y) const
{
	return (this->planeConstant - x * this->normal.x - y * this->normal.y) / this->normal.z;
};

//===========================================================================

void C3DPlane::addHeight(C3DPoint &p) const
{
	C2DPoint P(p.x, p.y);
	p.z = getHeight(P);
};

//===========================================================================

void C3DPlane::resetPlane()
{
	this->point.x  = 0.0;
	this->point.y  = 0.0;
	this->point.z  = 0.0;
	this->normal.x = 0.0;
	this->normal.y = 0.0;
	this->normal.z = 1.0;
	this->planeConstant = 0.0;
};

//===========================================================================

void C3DPlane::copyPlane(const C3DPlane &plane)
{
	this->point         = plane.point;
	this->normal        = plane.normal;
	this->planeConstant = plane.planeConstant;
};

//===========================================================================

int C3DPlane::generateParamDesc(C3DPoint &Point, C3DPoint &vec1, C3DPoint &vec2)
{
	if (!(*this)) return 0; //make sure C_normal_ != 0-vector
	int index; //index of co-ordiante of C_normal_ with largest absolute value
	double x (fabs(this->normal.x)); //absolute values of normal vector components
	double y (fabs(this->normal.y));
	double z (fabs(this->normal.z));
	if (x > y && x >z)  index = 0;
	else if (y > z) index = 1;
	else index = 2;

	vec1[(index+1)%3+1] = 1.0;
	vec1[(index+2)%3+1] = 0.0;

	vec1[index+1] = -(vec1[(index+1)%3+1] * this->normal[(index+1)%3+1]+
					  vec1[(index+2)%3+1] * this->normal[(index+2)%3+1])/this->normal[index+1];
	vec1 /= vec1.getNorm();
	vec2  = this->normal.crossProduct(vec1);
	Point = this->point;

	return 1;
}

//===========================================================================

C3DPoint C3DPlane::getBasePoint(const C3DPoint &p) const
{
  return p - (this->normal.innerProduct(p) - this->planeConstant) * this->normal;
}

//===========================================================================

double C3DPlane::getOrientedDistance (const C3DPoint & p) const
{
  return (this->normal.innerProduct(p) -  this->planeConstant);
}

//===========================================================================

bool C3DPlane::isParallel(const CXYZLine &line) const
{
  double inProd = this->normal.innerProduct(line.getDirection());
  return fabs(inProd) < getEpsilon();
}

//===========================================================================

eIntersect C3DPlane::intersectLine(const C3DPoint &pt, const C3DPoint &dir, double &param) const
{
	double constP = this->normal.innerProduct( this->point);  // using plane formula: C_normal_.innerProduct(C_pointInPlane) = C_normal_.innerProduct(C_point_)
	constP -= this->normal.innerProduct(pt) ;
	double vectP = this->normal.innerProduct(dir);
	if (fabs(vectP) < getEpsilon())
	{
		if (fabs(constP) < getEpsilon()) return eIncluded;
		else return eParallel;
	}

	param = constP / vectP;
	return eIntersection;
}

//===========================================================================

eIntersect C3DPlane::intersect(const CXYZLine &line) const
{
	double dmy;
	return this->intersectLine(line.getPoint(), line.getDirection(), dmy);
}

//===========================================================================

eIntersect C3DPlane::intersect(const CXYZRay &ray) const
{
	double param;
	eIntersect intersectVal = this->intersectLine(ray.getPoint(), ray.getDirection(), param);
	if (intersectVal == eIntersection)
	{
		if (param >= 0.0) return eIntersection;
		else return eNoIntersection;
	}

	return intersectVal;
}

//===========================================================================

eIntersect C3DPlane::intersect(const CXYZSegment &segment) const
{
	double param;
	eIntersect intersectVal = this->intersectLine(segment.getPoint1(), 
												segment.getPoint2() - segment.getPoint1(), 
												param);

	if (param < 0.0 || param > 1.0) intersectVal = eNoIntersection;

	return intersectVal;
}

//===========================================================================

eIntersect C3DPlane::getIntersection(const CXYZLine &line, C3DPoint &p) const
{
	double param;
	eIntersect intersectVal = this->intersectLine(line.getPoint(), line.getDirection(), param);

	p = line.getPointOnLine(param);
	return intersectVal;
}

//===========================================================================

eIntersect C3DPlane::getIntersection(const CXYZRay &ray, C3DPoint &p) const
{
	double param;
	eIntersect intersectVal = this->intersectLine(ray.getPoint(), ray.getDirection(), param);
	if (intersectVal == eIntersection)
	{
		if (param >= 0.0) p = ray.getPointOnLine(param);
		else intersectVal = eNoIntersection;
	}
	return intersectVal;
}

//===========================================================================

eIntersect C3DPlane::getIntersection(const CXYZSegment &segment, C3DPoint &p) const
{
	double param;
	eIntersect intersectVal = this->intersectLine(segment.getPoint1(),
                                                  segment.getPoint2() - segment.getPoint1(), 
												  param);
	
	if (intersectVal != eIntersection ) return intersectVal;
	if (param < 0.0 || param > 1.0)    return eNoIntersection;

	p = (1.0 - param) * segment.getPoint1() + param * segment.getPoint2();
	return intersectVal;
}

//===========================================================================

bool C3DPlane::isParallel(const C3DPlane &plane) const
{
	C3DPoint crossProd = this->normal.crossProduct(plane.normal);
	return crossProd.getNorm() < getEpsilon();
}

//===========================================================================

eIntersect C3DPlane::intersect(const C3DPlane &plane) const
{
	C3DPoint crossProd = this->normal.crossProduct(plane.normal);
	if (crossProd.getNorm() < getEpsilon()) return eIntersection;

	C3DPoint point2point(plane.point - this->point);
	if ( fabs(this->normal.innerProduct(point2point) ) < getEpsilon() ) return eIncluded;

	return eParallel;
}

//===========================================================================

eIntersect C3DPlane::getIntersection(const C3DPlane &plane, CXYZLine& line ) const
{
	C3DPoint crossProd = this->normal.crossProduct(plane.normal);
	// plane formula: n*x=const (n = normal vector, x = point in plane)
	double const1 = this->normal.innerProduct(this->point);                       // plane constant of *this
	double const2 = plane.normal.innerProduct(plane.point);   // plane constant of IrC_plane
	if (crossProd.getNorm() < getEpsilon())
	{
		if ( fabs(const1 - const2) < getEpsilon()) return eIncluded;
		return eParallel;
	}

	C3DPoint intersPt;  // one intersection point along the line
	C3DPoint dmy(crossProd.x * crossProd.x, crossProd.y * crossProd.y, crossProd.z * crossProd.z);

	if (dmy.x > dmy.y && dmy.x > dmy.z )
	{	// x value of C_crossProd_ is biggest
		intersPt.x = this->point.x;
		const1 -= this->normal.x * intersPt.x;
		const2 -= plane.normal.x * intersPt.x;
		double det = 1.0 / (this->normal.y * plane.normal.z - this->normal.z * plane.normal.y);
		intersPt.y = ( const1 * plane.normal.z - this->normal.z * const2) * det;
		intersPt.z = (-const1 * plane.normal.y + this->normal.y * const2) * det;
	}
	else if (dmy.y > dmy.z )
	{	// y value of C_crossProd_ is biggest
		intersPt.y = this->point.y;
		const1 -= this->normal.y * intersPt.y;
		const2 -= plane.normal.y * intersPt.y;
		double det = 1.0 / (this->normal.x * plane.normal.z - this->normal.z * plane.normal.x);
		intersPt.x = ( const1 * plane.normal.z - this->normal.z * const2) * det;
		intersPt.z = (-const1 * plane.normal.x + this->normal.x * const2) * det;
	}
	else
	{	// z value of C_crossProd_ is biggest
		intersPt.z = this->point.z;
		const1 -= this->normal.z * intersPt.z;
		const2 -= plane.normal.z * intersPt.z;
		double det = 1.0 / (this->normal.x * plane.normal.y - this->normal.y * plane.normal.x);
		intersPt.x = ( const1 * plane.normal.y - this->normal.y * const2) * det;
		intersPt.y = (-const1 * plane.normal.x + this->normal.x * const2) * det;
	}

	line = CXYZLine(intersPt, intersPt + crossProd);
	return eIntersection;
}
                          
//===========================================================================

eIntersect C3DPlane::getIntersection(const C3DPlane &plane, C3DPoint &p1,  C3DPoint &p2) const
{
	CXYZLine line(p1, p2);
	eIntersect intersectionVal = this->getIntersection(plane, line);
	if (intersectionVal == eIntersection)
	{
		p1 = line.getBasePoint(p1);
		p2 = line.getBasePoint(p2);
	};

	return intersectionVal;
};

//===========================================================================

eIntersect C3DPlane::getIntersection(const C3DPlane &plane, CXYZSegment &segment ) const
{
	C3DPoint p1(segment.getPoint1());
	C3DPoint p2(segment.getPoint2());
	eIntersect intersectionVal = this->getIntersection(plane, p1, p2);
	if (intersectionVal == eIntersection)
	{
		segment = CXYZSegment(p1, p2);
	};

	return intersectionVal;
};

//===========================================================================
