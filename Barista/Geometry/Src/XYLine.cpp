/**
 * @class CXYLine
 * an 2D line with a projective representation including covariance matrix,
 * and methods to compute its parameters by regression
 */
#include <math.h>
#include <float.h>
#include "newmatap.h"

#include "XYPoint.h"
#include "statistics.h"
#include "XYContour.h"

#include "XYLine.h"

/**
 * default contructor
 * @param x
 * @param y
 */
CXYLine::CXYLine(void): pointcount(0), sigma0(0.0f), covariance(3,3),
						normal(1.0f, 0.0f), eigenvalues(0.0f, 0.0f),
						cog(0.0f, 0.0f), euclidean(0.0f)
{
	covariance = 0.0f;
}

CXYLine::~CXYLine(void)
{
}

/**
 * contructor from start and end points
 * @param p1
 * @param p2
 */
CXYLine::CXYLine(const CXYPoint &p1, const CXYPoint &p2):  
   pointcount(0), sigma0(0.0f)
{
    initFromPoints(p1, p2);
}
/**
 * copy contructor which derives from line
 * @param line
 */
CXYLine::CXYLine(const CXYLine &line):
     normal(line.normal), cog(line.cog), 
	 euclidean(line.euclidean), eigenvalues(line.eigenvalues), 
	 pointcount(line.pointcount), 
     sigma0(line.sigma0), covariance(line.covariance) 
{
};

/**
 * sets the start and end points
 * @param p1
 * @param p2
 */
void CXYLine::initFromPoints(const CXYPoint &p1, const CXYPoint &p2)
{
	this->normal.x = -(p2.getY() - p1.getY());
	this->normal.y =   p2.getX() - p1.getX();
	this->cog = ((C2DPoint)p2 + p1) * 0.5f;
	euclidean = -cog.innerProduct(normal);
    eigenvalues.x = 0.0f;
	eigenvalues.y = 0.0f;
	Matrix s1 = p1.getS();
	Matrix covar1(3,3);
	covar1 = 0;
	covar1.SubMatrix(1,2,1,2) = p1.getCovar();
	Matrix s2 = p2.getS();
	Matrix covar2(3,3);
	covar2 = 0;
	covar2.SubMatrix(1,2,1,2) = p2.getCovar();
	
    covariance = s1 * covar2 * s1.t() + s2 * covar1 * s2.t();
	sigma0 = 1.0f;

    normalise();
};

void CXYLine::normalise()
{
	double norme = normal.getNorm();

    Matrix jacobian = getJacobian();

    normal  /= norme;
    euclidean /= norme;

    covariance = jacobian * covariance * jacobian.t();
};

C2DPoint CXYLine::getDirection() const
{
 C2DPoint direction;
 direction.x =  this->normal.y;
 direction.y = -this->normal.x;
 return direction;
}

 Matrix CXYLine::getCovariance(const C2DPoint &p) const 
 {
  Matrix shift(3,3);
  shift = 0.0f;
  shift(1,1) = shift(2,2) = shift(3,3)  = 1.0f; 

  shift(3,1) = -p.x;
  shift(3,2) = -p.y;
    
  return shift * this->covariance * shift.t();
 };


Matrix CXYLine::getRotMat() const
{
  Matrix rotMat(3,3);
  C2DPoint direction = getDirection();
  rotMat(1,1) = direction.x; rotMat(1,2) = normal.x; rotMat(1,3) = 0.0f;
  rotMat(2,1) = direction.y; rotMat(2,2) = normal.y; rotMat(2,3) = 0.0f;
  rotMat(3,1) = 0.0f;        rotMat(3,2) = 0.0f;     rotMat(3,3) = 1.0f;
  return rotMat;
};

//=============================================================================
   
 ColumnVector CXYLine::getVector() const
 {
  ColumnVector vec(3);
  vec(1) = normal.x;
  vec(2) = normal.y;
  vec(3) = euclidean;
  return vec;
 };

 Matrix CXYLine::getS() const
 {
  Matrix s(3,3); 
  s(1,1) =  0.0f;      s(1,2) = -euclidean; s(1,3) =  normal.y;
  s(2,1) =  euclidean; s(2,2) = 0.0f;       s(2,3) = -normal.x;
  s(3,1) = -normal.y;  s(3,2) = normal.x;   s(3,3) =  0.0f;
  return s;
 };

 Matrix CXYLine::getJacobian() const
 {
  Matrix jacobian(3,3);
  jacobian = 0.0f;

  ColumnVector h(2);
 
  C2DPoint direction = getDirection();
  h(1) = direction.x;
  h(2) = direction.y;

  double squareNorm = direction.getSquareNorm();

  Matrix auxil(2,2); 
  auxil = 0.0f;
  auxil(1,1) = 1.0f; auxil(2,2) = 1.0f;
  jacobian(3,3) = 1.0f;

  auxil -= (h * h.t()) / squareNorm;

  jacobian.SubMatrix(1,2,1,2) = auxil;
  
  auxil = -h.t() * euclidean / squareNorm;
  jacobian.SubMatrix(3,3,1,2) = auxil;
  jacobian /= sqrt(squareNorm);

  return jacobian;
 };
 
//=============================================================================
 
/**
 * Calculates the distance of point from the straight line defined by the segment 
 * @param point
 * returns double value of the  distance 
 */

double CXYLine::distanceFromLine (const C2DPoint &point) const
{
 return fabs(this->normal.innerProduct(point) + euclidean);
};

/**
 * Calculates the distance of point from the straight line defined by the segment 
 * @param point
 * returns double value of the  distance 
 */

double CXYLine::distanceFromLine (const CXYPoint &point) const
{
 return fabs(point.innerProduct(this->normal) + euclidean);
};

/**
 * Calculates the nearest distance of point from the segment 
 * @param point
 * returns double value of the distance 
 */    

/**
 * reverse the segment
 */
void CXYLine::changeDirection()
{
	normal = -normal;
    euclidean = -euclidean;
};

CXYPoint CXYLine::projectedPoint(const CXYPoint &p) const
{
	C2DPoint orthoEnd2D = p + normal;
    CXYPoint orthoEnd(&orthoEnd2D);
	orthoEnd.setSX(0.0f);
	orthoEnd.setSY(0.0f);
	CXYLine orthogonal(p, orthoEnd);
  
	Matrix auxiliary1 = getS();
	ColumnVector C_pointVec_ = auxiliary1 * orthogonal.getVector();

    Matrix auxiliary2 = orthogonal.getS();

	Matrix pointCovar = auxiliary2.t() * this->covariance      * auxiliary2 + 
		                auxiliary1.t() * orthogonal.covariance * auxiliary1;
 
    CXYPoint projection;
	projection.setFromHomogeneousVector(C_pointVec_,pointCovar);
  
	return projection;
};

/**
 * Calculates the projection of a C2DPoint to the segment 
 * @param point
 * returns the projected point
 */    
C2DPoint CXYLine::getBasePoint( const C2DPoint &point ) const
{
	C2DPoint dir = getDirection();
	return (cog + (dir.innerProduct( point - cog) ) * dir);
};

void CXYLine::initCovarFromAdjustment(double I_sigma0)
{
	this->sigma0 = this->eigenvalues.x;
	if (this->pointcount > 2) this->sigma0 = sqrt(this->sigma0/(this->pointcount - 2));
	if ((I_sigma0 < 0) || (this->sigma0 > I_sigma0)) I_sigma0 = this->sigma0;
	I_sigma0 *= I_sigma0;

	Matrix trafMat1 = this->getRotMat();
	Matrix trafMat2(3,3);
	trafMat2(1,1) =  1.0f;  trafMat2(1,2) =  0.0f;  trafMat2(1,3) = 0.0f;
	trafMat2(2,1) =  0.0f;  trafMat2(2,2) =  1.0f;  trafMat2(2,3) = 0.0f;
	trafMat2(3,1) = -cog.x; trafMat2(3,2) = -cog.y; trafMat2(3,3) = 1.0f;

	Matrix trafMat = trafMat2 * trafMat1;
	this->covariance = 0.0f;
	this->covariance(1,1) = I_sigma0 / this->eigenvalues.y;
	this->covariance(3,3) = I_sigma0 / double(this->pointcount);

	this->covariance = trafMat * this->covariance * trafMat.t();
};

float CXYLine::containsPoint(const CXYPoint &point) const
{
	double metric = fabs(point.innerProduct(normal) + euclidean);

	ColumnVector homogPoint(3);
    homogPoint(1) = point.getX(); 
	homogPoint(2) = point.getY(); 
	homogPoint(3) = 1.0f;
	ColumnVector homogLine= this->getVector();
  
	Matrix pointCovar(3,3);
	pointCovar = 0;
	Matrix Covar = point.getCovar();
	pointCovar.SubMatrix(1,2,1,2) = Covar;

	Matrix distCovar = homogPoint.t() * this->covariance * homogPoint + 
		               homogLine.t()  * pointCovar       * homogLine;

	metric = metric * metric / distCovar(1,1);

	metric /= statistics::chiSquareQuantil(1);

	if (metric > 1.0f) metric = -metric;

	return (float) metric;
};

float CXYLine::isIdentical(const CXYLine &segment) const
{
	double metric = statistics::testMetric(this->getS(),      segment.getS(), 
                                           this->getVector(), segment.getVector(), 
                                           this->covariance,  segment.covariance, 2);

  
	metric /= statistics::chiSquareQuantil(2);
	if (metric > 1.0f) metric = -metric;
	return (float) metric;
};

float CXYLine::isParallel(const CXYLine &segment) const
{
	C2DPoint segDir = segment.getDirection();
	double metric = fabs(segDir.innerProduct(normal));
	ColumnVector normVec(2);
	normVec(1) = normal.x; 
	normVec(2) = normal.y;

	ColumnVector segDirVec(2);
	segDirVec(1) = segDir.x; 
	segDirVec(2) = segDir.y;
  
	Matrix covar1 = this->covariance.SubMatrix(1,2,1,2);
	double auxil = covar1(1,1); 
	covar1(1,1) = covar1(2,2); 
	covar1(2,2) = auxil; 

	Matrix covar2 = segment.covariance.SubMatrix(1,2,1,2);

  
	covar1 = normVec.t() * covar2 * normVec + segDirVec.t() * covar1 * segDirVec;
  
	double metricCovar = covar1(1,1);
	metric /= metricCovar;
	metric /= statistics::chiSquareQuantil(1);
	if (metric > 1.0f) metric = -metric;

	return (float) metric;
};

   
eIntersect CXYLine::getIntersection(const CXYLine &segment, 
										   CXYPoint &intersection) const
{
	Matrix s = this->getS();
	ColumnVector pointVec = s * segment.getVector();
  
	Matrix covarPt  = segment.getS();
	covarPt = covarPt.t() * this->covariance    * covarPt + 
		      s.t()       * segment.covariance * s;

	if (fabs(pointVec(3)) < FLT_EPSILON) return eParallel;
   
	intersection.setFromHomogeneousVector(pointVec, covarPt);

	return eIntersection;
};


void CXYLine::initFromVector(C2DPoint *points, unsigned long length)
{
	cog.x = cog.y = 0;

    SymmetricMatrix N(2);          // matrix of eigen value system
    DiagonalMatrix  eigenVal(2);   // eigen values
    Matrix          eigenVec(2,2); // eigen vectors
  
	N = 0.0f; 
				
	for (unsigned long i = 0; i < length; ++i)
	{
		cog += points[i];
	};

	this->pointcount = length;

	this->cog /= (double) this->pointcount;

	for (unsigned long i = 0; i < length; ++i)
	{
		C2DPoint relPos = points[i] - this->cog;
		N(1,1) += relPos.x * relPos.x; N(1,2) += relPos.x * relPos.y;
		                               N(2,2) += relPos.y * relPos.y;
	};

	Jacobi(N, eigenVal, eigenVec);     // compute eigen values and vectors
      
	this->normal.x = -eigenVec(2, 2); 
	this->normal.y =  eigenVec(1, 2);
  
	this->eigenvalues.x = eigenVal(1);
	this->eigenvalues.y = eigenVal(2);
	
	C2DPoint direction = this->getDirection();
	C2DPoint point1 = cog + direction.innerProduct(points[0] - cog) * direction;
	C2DPoint point2 = cog + direction.innerProduct(points[length - 1]   - cog) * direction;

	C2DPoint Delta_ = point2 - point1;
	if (Delta_.innerProduct(direction) < 0.0f) normal = -normal;
    
	euclidean = -normal.innerProduct(cog);
};

void CXYLine::computeParametersFromContour(CXYContour *contour, 
										   unsigned long indexFrom,
										   unsigned long indexTo, 
										   double I_sigma0)
{
	cog.x = cog.y = 0;

    SymmetricMatrix N(2);     // matrix of eigen value system
    DiagonalMatrix  eigenVal(2);   // eigen values
    Matrix          eigenVec(2,2); // eigen vectors
  
	N = 0.0f; 
				
	for (unsigned long i = indexFrom; i <= indexTo; ++i)
	{
		cog += *(contour->getPoint(i));
	};

	this->pointcount = long (indexTo + 1) - long(indexFrom);

	this->cog /= (double) this->pointcount;

	for (unsigned long i = indexFrom; i <= indexTo; ++i)
	{
		C2DPoint relPos = *(contour->getPoint(i)) - this->cog;
		N(1,1) += relPos.x * relPos.x; N(1,2) += relPos.x * relPos.y;
		                               N(2,2) += relPos.y * relPos.y;
	};

	Jacobi(N, eigenVal, eigenVec);     // compute eigen values and vectors
      
	this->normal.x = -eigenVec(2, 2); 
	this->normal.y =  eigenVec(1, 2);
  
	this->eigenvalues.x = eigenVal(1);
	this->eigenvalues.y = eigenVal(2);
	
	C2DPoint direction = this->getDirection();
	C2DPoint point1 = cog + direction.innerProduct(*(contour->getPoint(indexFrom)) - cog) * direction;
	C2DPoint point2 = cog + direction.innerProduct(*(contour->getPoint(indexTo))   - cog) * direction;

	C2DPoint Delta_ = point2 - point1;
	if (Delta_.innerProduct(direction) < 0.0f) normal = -normal;
    
	euclidean = -normal.innerProduct(cog);
};


void CXYLine::initFromContour(CXYContour *contour, 
									 unsigned long indexFrom,
									 unsigned long indexTo, 
									 double I_sigma0)
{
	computeParametersFromContour(contour, indexFrom, indexTo, I_sigma0);
	initCovarFromAdjustment(I_sigma0);
};
