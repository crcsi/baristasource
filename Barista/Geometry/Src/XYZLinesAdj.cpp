//============================================================================

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      IPI, University of Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <float.h>
#include <vector>
using namespace std;

#include "newmatap.h" //for advanced matrix routines (LU;QR,SVD,..)
#include "myexcept.h" //for the exceptions in NM

#include "XYZLinesAdj.h"
#include "3DadjPlane.h"

CXYZHomoSegment::CXYZHomoSegment() : qxx(6), qxx1(4), qxx2(4)
{
	this->initQxxEndPoints();
	this->initQxx();
};

//=============================================================================

CXYZHomoSegment::CXYZHomoSegment(const C3DPoint &p1, const C3DPoint &p2 ) : 
                                 qxx(6), qxx1(4), qxx2(4)
{
	this->constructFromPoints(p1, p2);
	this->initQxxEndPoints();
	this->initQxx();
	this->normalise();
};

//=============================================================================
 
CXYZHomoSegment::CXYZHomoSegment(const CXYZPoint &p1, const CXYZPoint &p2) : 
                                 qxx(6), qxx1(4), qxx2(4)
{
	this->constructFromPoints(p1, p2);
	this->qxx1 = 0.0;
	this->qxx2 = 0.0;

	for (int r = 0; r < p1.getCovar().Nrows(); ++r)
	{
		for (int c = r; c < p1.getCovar().Ncols(); ++c)
		{
			this->qxx1.element(r,c) = p1.getCovar().element(r,c);
		}
	}

	for (int r = 0; r < p2.getCovar().Nrows(); ++r)
	{
		for (int c = r; c < p2.getCovar().Ncols(); ++c)
		{
			this->qxx2.element(r,c) = p2.getCovar().element(r,c);
		}
	}

	this->initQxx();
	this->normalise();
};

//=============================================================================

CXYZHomoSegment::CXYZHomoSegment(const CXYZSegment &segment) : 
                                 qxx(6), qxx1(4), qxx2(4)
{
	this->constructFromPoints(segment.getPoint1(), segment.getPoint2());
	this->initQxxEndPoints();
	this->initQxx();
	this->normalise();
};

//=============================================================================

CXYZHomoSegment::CXYZHomoSegment(const CXYZHomoSegment &segment): 
                                 CXYZSegment(segment), euclidean(segment.euclidean),
								 qxx(segment.qxx), qxx1(segment.qxx1), qxx2(segment.qxx2)   

{
};
   
//=============================================================================

 CXYZHomoSegment::CXYZHomoSegment(const C3DAdjustPlane &plane1, const C3DAdjustPlane &plane2,
                                  const C3DPoint &p1, const C3DPoint &p2) : 
                                 qxx(6), qxx1(4), qxx2(4)
{
	Matrix piBar1 = plane1.getPiBar();
	Matrix piBar2 = plane2.getPiBar();

	ColumnVector vec = piBar1 * plane2.getAsVector();
	this->direction.x = vec.element(0);
	this->direction.y = vec.element(1);
	this->direction.z = vec.element(2);
	this->euclidean.x = vec.element(3);
	this->euclidean.y = vec.element(4);
	this->euclidean.z = vec.element(5);
	
	Matrix mat =  piBar1 *  plane2.getQxxRot() * piBar1.t() + 
		          piBar2 *  plane1.getQxxRot() * piBar2.t();

	C3DAdjustPlane::copyMatrixToSymmetricMatrix (mat, this->qxx); 

	this->normalise();
	this->cutOff(p1, p2);
	C3DPoint delta = this->point2 - this->point1;
	if (delta.innerProduct(this->direction) < 0.0)
	{
		this->direction = -this->direction;
		this->euclidean = -this->euclidean;
	}
};

//=============================================================================

CXYZHomoSegment::~CXYZHomoSegment() 
{
};
 
//=============================================================================
 
void CXYZHomoSegment::cutOff(const C3DPoint &p1, const C3DPoint &p2)
{

	CXYZPoint p(p1);
	p.setCovariance(this->qxx1);
	p = this->getProjectedPoint(p);
	this->point1 = p;
	this->qxx1.ReSize(3);
	C3DAdjustPlane::copyMatrixToSymmetricMatrix(p.getCovar(), this->qxx1);

	p = p2;
	p.setCovariance(this->qxx2);
	p = this->getProjectedPoint(p);
	this->point2 = p;
	this->qxx2.ReSize(3);
	C3DAdjustPlane::copyMatrixToSymmetricMatrix(p.getCovar(), this->qxx2);
};

//=============================================================================

void CXYZHomoSegment::reverse()
{
	this->direction = -this->direction;
	this->euclidean = -this->euclidean;

	C3DPoint swapPt = this->point1;
	this->point1    = this->point2;
	this->point2    = swapPt;

	SymmetricMatrix swapMat = this->qxx1;
	this->qxx1 = this->qxx2;
	this->qxx2 = swapMat;
};

//=============================================================================

SymmetricMatrix CXYZHomoSegment::getQxx(const C3DPoint &p) const 
{
	Matrix shiftMat(6,6);
	shiftMat = 0.0;
	shiftMat.element(0,0) = shiftMat.element(1,1) = shiftMat.element(2,2) = 
		                    shiftMat.element(3,3) = shiftMat.element(4,4) = 
							shiftMat.element(5,5) = 1.0; 

	shiftMat.element(3,1) = -p.z;
	shiftMat.element(3,2) =  p.y;
	shiftMat.element(4,0) =  p.z;
	shiftMat.element(4,2) = -p.x;
	shiftMat.element(5,0) = -p.y;
	shiftMat.element(5,1) =  p.x;

	SymmetricMatrix shiftQxx(6);

	Matrix mtx = shiftMat * this->qxx * shiftMat.t();

	C3DAdjustPlane::copyMatrixToSymmetricMatrix (mtx, shiftQxx); 

	return shiftQxx;
};

//=============================================================================

Matrix CXYZHomoSegment::getJacobi() const
{
	Matrix jacobiMat(6,6);
	jacobiMat = 0.0;

	ColumnVector h(3);
	ColumnVector o(3);
	h.element(0) = this->direction.x;
	h.element(1) = this->direction.y;
	h.element(2) = this->direction.z;
	o.element(0) = this->euclidean.x;
	o.element(1) = this->euclidean.y;
	o.element(2) = this->euclidean.z;

	Matrix auxilMat(3,3); 
	auxilMat = 0.0;
	auxilMat.element(0,0) = 1.0;  auxilMat.element(1,1) = 1.0; auxilMat.element(2,2) = 1.0;
	jacobiMat.SubMatrix(4,6,4,6) = auxilMat;

	double squareNormInv = 1.0 / this->direction.getSquareNorm();

	auxilMat -= (h * h.t()) * squareNormInv;

	jacobiMat.SubMatrix(1,3,1,3) = auxilMat;

	auxilMat = -(h * o.t()) * squareNormInv;
	jacobiMat.SubMatrix(4,6,1,3) = auxilMat;
	jacobiMat *= sqrt(squareNormInv);

	return jacobiMat;
};

//=============================================================================
   
Matrix CXYZHomoSegment::getGamma() const
{
	Matrix gammaMat(4,4);
	gammaMat.element(0,0) =  0.0;               gammaMat.element(0,1) =  this->euclidean.z; gammaMat.element(0,2) = -this->euclidean.y;  gammaMat.element(0,3) = -this->direction.x;
	gammaMat.element(1,0) = -this->euclidean.z; gammaMat.element(1,1) =  0.0;               gammaMat.element(1,2) =  this->euclidean.x;  gammaMat.element(1,3) = -this->direction.y;
	gammaMat.element(2,0) =  this->euclidean.y; gammaMat.element(2,1) = -this->euclidean.x; gammaMat.element(2,2) =  0.0;                gammaMat.element(2,3) = -this->direction.z;
	gammaMat.element(3,0) =  this->direction.x; gammaMat.element(3,1) =  this->direction.y; gammaMat.element(3,2) =  this->direction.z;  gammaMat.element(3,3) =  0.0;

	return gammaMat;
};

//=============================================================================
   
Matrix CXYZHomoSegment::getGammaBar() const
{
	Matrix gammaBarMat(4,4);
	gammaBarMat.element(0,0) =  0.0;               gammaBarMat.element(0,1) =  this->direction.z; gammaBarMat.element(0,2) = -this->direction.y;  gammaBarMat.element(0,3) = -this->euclidean.x;
	gammaBarMat.element(1,0) = -this->direction.z; gammaBarMat.element(1,1) =  0.0;               gammaBarMat.element(1,2) =  this->direction.x;  gammaBarMat.element(1,3) = -this->euclidean.y;
	gammaBarMat.element(2,0) =  this->direction.y; gammaBarMat.element(2,1) = -this->direction.x; gammaBarMat.element(2,2) =  0.0;                gammaBarMat.element(2,3) = -this->euclidean.z;
	gammaBarMat.element(3,0) =  this->euclidean.x; gammaBarMat.element(3,1) =  this->euclidean.y; gammaBarMat.element(3,2) =  this->euclidean.z;  gammaBarMat.element(3,3) =  0.0;

	return gammaBarMat;  
};
	
//=============================================================================
   
float CXYZHomoSegment::isIdenticalStraightLine(const CXYZHomoSegment &seg) const
{
	double metric1 = this->containsPoint(seg.getPoint1());
	double metric2 = this->containsPoint(seg.getPoint2());
	double metric3 = seg.containsPoint(this->getPoint1());
	double metric4 = seg.containsPoint(this->getPoint2());

	double metricA = (metric1 < metric2) ? metric2 : metric1;
	double metricB = (metric3 < metric4) ? metric4 : metric3;

	double metric  = (metricA < metricB) ? metricB : metricA;

	metric /= C3DAdjustPlane::getChiSquareQuantil(2);
	if (metric > 1.0) metric = -metric;
	return (float) metric;

};

//=============================================================================
   
ColumnVector CXYZHomoSegment::getAsVector() const
{
	ColumnVector homogVector(6);
	homogVector.element(0) = this->direction.x;
	homogVector.element(1) = this->direction.y;
	homogVector.element(2) = this->direction.z;
	homogVector.element(3) = this->euclidean.x;
	homogVector.element(4) = this->euclidean.y;
	homogVector.element(5) = this->euclidean.z;
	return homogVector;
};

//=============================================================================

CXYZPoint CXYZHomoSegment::getProjectedPoint(const CXYZPoint &p) const
{
	C3DAdjustPlane plane(p, this->direction);
	ColumnVector pointVec = this->getGamma().t() * plane.getAsVector();

	Matrix mat = plane.getPi();

	mat = mat.t() * this->qxx * mat;

	CXYZPoint projectedPoint(pointVec.element(0), pointVec.element(1), pointVec.element(2));

	C3DAdjustPlane::normalisePoint(projectedPoint, pointVec.element(3), mat);

	return projectedPoint;
};

//=============================================================================

eIntersect CXYZHomoSegment::getIntersection(const C3DAdjustPlane &plane, 
											CXYZPoint &Pint) const
{
	Matrix gammaMat = this->getGamma();
	ColumnVector pointVec = gammaMat.t() * plane.getAsVector();
	Matrix mat = plane.getPi();
	mat = mat.t() * this->qxx * mat + gammaMat.t() * plane.getQxxRot() * gammaMat;

	Pint.x = pointVec.element(0);
	Pint.y = pointVec.element(1);
	Pint.z = pointVec.element(2);

	if (fabs(pointVec.element(3)) < FLT_EPSILON) return eParallel;

	C3DAdjustPlane::normalisePoint(Pint, pointVec.element(3), mat);

	return eIntersection;
};

//=============================================================================

eIntersect CXYZHomoSegment::getIntersection(const CXYZHomoSegment &segment, 
                                            CXYZPoint &Pint) const
{
	C3DPoint diff = segment.getPoint1() - this->point1;

	C3DPoint v1xv2 = this->direction.crossProduct(segment.direction);
	v1xv2 = v1xv2 / v1xv2.getNorm();
	C3DPoint nVector = v1xv2.crossProduct(this->direction);

	double my = -nVector.innerProduct(diff) / nVector.innerProduct(segment.direction);

	nVector = v1xv2.crossProduct(segment.direction);

	double lambda = nVector.innerProduct(diff) / nVector.innerProduct(this->direction);

	CXYZPoint p1 = this->getProjectedPoint(this->getPointOnLine(lambda));
	CXYZPoint p2 = segment.getProjectedPoint(segment.getPointOnLine(my));

	Matrix covInt = (p1.getCovar() + p1.getCovar()) * 0.25;
	Pint.x = (p1.x + p2.x) * 0.5;
	Pint.y = (p1.y + p2.y) * 0.5;
	Pint.z = (p1.z + p2.z) * 0.5;
	Pint.setCovariance(covInt);

	return eIntersection;
};

//=============================================================================

eIntersect CXYZHomoSegment::getIntersection(const CXYZHomoSegment *pSegment, 
                                            CXYZPoint &Pint) const
{
	return getIntersection(*pSegment, Pint);
};

//=============================================================================

float CXYZHomoSegment::containsPoint(const C3DPoint &p) const
{
	CXYZPoint P(p);
	Matrix *qxxP = P.getCovariance();
	(*qxxP) = 0.0;
	return this->containsPoint(P);
};

//=============================================================================

float CXYZHomoSegment::containsPoint(const CXYZPoint &p) const
{
	ColumnVector pointVec(4);
	pointVec.element(0) = p.x; pointVec.element(1) = p.y; pointVec.element(2) = p.z; pointVec.element(3) = 1.0;
	SymmetricMatrix qxxP(4);
	qxxP = 0.0;

	for (int row  = 0; row < 3; ++row)
	{
		for (int col = row; col < 3; ++col)
		{
			qxxP.element(row, col) = p.getCovar().element(row, col);
		}
	}

	double metric = C3DAdjustPlane::testMetric(this->getGammaBar().t(), C3DAdjustPlane::getPiBar(p).t(), 
                                               this->getAsVector(), pointVec, this->qxx, qxxP , 2);

	metric /= C3DAdjustPlane::getChiSquareQuantil(2);
	if (metric > 1.0) metric = -metric;
	return (float) metric;
};

//=============================================================================

void CXYZHomoSegment::normalise()
{
	double norm = this->direction.getNorm();

	Matrix jacobiMat = this->getJacobi();

	this->direction /= norm;
	this->euclidean /= norm;
	jacobiMat = jacobiMat * this->qxx * jacobiMat.t();
	C3DAdjustPlane::copyMatrixToSymmetricMatrix (jacobiMat, this->qxx); 
};

//=============================================================================
   
void CXYZHomoSegment::print(ostream &ostr) const
{
	ostr.setf(ios::fixed,ios::floatfield);
	ostr << this->direction.getString(" ", 12,9) << " " << this->euclidean.getString(" ", 12,3);
	ostr.precision(10);

	CCharString frst = "\nVAR-COVAR";
	for (int r = 0; r < this->qxx.Nrows(); ++r)
	{
		ostr << frst.GetChar();

		for (int c = 0; c < this->qxx.Ncols(); ++c)
		{
			ostr.width(13);
			ostr << this->qxx.element(r,c);
		}
		frst = "\n         ";
	}
	ostr << endl;
};

//=============================================================================
   
void CXYZHomoSegment::initQxxEndPoints()
{
	this->qxx1 = 0.0;
	this->qxx2 = 0.0;
};

//=============================================================================

void CXYZHomoSegment::initQxx()
{
	Matrix pi1 = C3DAdjustPlane::getPi(this->point1);
	Matrix pi2 = C3DAdjustPlane::getPi(this->point2);

	pi1 = pi1 * this->qxx2 * pi1.t() + pi2 * this->qxx1 * pi2.t();
	C3DAdjustPlane::copyMatrixToSymmetricMatrix (pi1, this->qxx); 
};

//=============================================================================

void CXYZHomoSegment::constructFromPoints(const C3DPoint &p1, const C3DPoint &p2)
{
	this->point1 = p1;
	this->point2 = p2;
	this->direction = p2 - p1;
	this->euclidean = p1.crossProduct(p2);
};

//=============================================================================

void CXYZHomoSegment::mergeWithSegment(const CXYZSegment &segment)
{
	double len1 = this->getLength();
	double len2 = segment.getLength();

	C3DPoint cog = (((this->point1 + this->point2) * len1) + 
					((segment.getPoint1() + segment.getPoint2()) * len2)) * 
					( 0.5 / (len1 + len2));


	vector<C3DPoint> posVec(4); 
	vector<double>   distVec(4); 

	posVec [0] = this->point1 - cog;
	distVec[0] = len1;
	posVec [1] = this->point2 - cog;
	distVec[1] = len1;
	posVec [2] = segment.getPoint1() - cog;
	distVec[2] = len2;
	posVec [3] = segment.getPoint2() - cog;
	distVec[3] = len2;

	SymmetricMatrix N(3);     // matrix of eigen value system
	DiagonalMatrix  val(3);   // eigen values
	Matrix          vec(3,3); // eigen vectors
	N.element(0,0) = 0.0;  N.element(0,1) = 0.0;  N.element(0,2) = 0.0; 
					       N.element(1,1) = 0.0;  N.element(1,2) = 0.0;
									              N.element(2,2) = 0.0; 
	double x, y, z, l;

	for (unsigned int i = 0; i < posVec.size(); ++i)
	{
		x = posVec[i].x;  y = posVec[i].y;   z = posVec[i].z;
		l = distVec[i];
		N.element(0,0) += x * x * l;  N.element(0,1) += x * y * l;  N.element(0,2) += x * z * l; 
									  N.element(1,1) += y * y * l;  N.element(1,2) += y * z * l;
																	N.element(2,2) += z * z * l; 
	};

	N /= (len1 + len2);


	Jacobi(N, val, vec);     // compute eigen values and vectors
	int locMax;              // position of greatest element (eigen value)
	if (val.element(0) >= val.element(1) && val.element(0) >= val.element(2)) 
		locMax = 0; 
	else if (val.element(1) >= val.element(2) && val.element(1)  >= val.element(0))
		locMax = 1;   
	else if (val.element(2)  >= val.element(0) && val.element(2)  >= val.element(1))
		locMax = 2; 

	C3DPoint dir = C3DPoint(vec.element(0, locMax), vec.element(1, locMax), vec.element(2, locMax)); 

	CXYZLine line(cog, cog + dir);

	posVec[0] = line.getBasePoint(this->point1);
	posVec[1] = line.getBasePoint(this->point2);
	posVec[2] = line.getBasePoint(segment.getPoint1());
	posVec[3] = line.getBasePoint(segment.getPoint2());

	if (this->direction.innerProduct(dir) < 0) dir = -dir;

	for (unsigned int i = 0; i < posVec.size(); ++i)
	{
		distVec[i] = dir.innerProduct(posVec[i] - cog);
	};
	    
	unsigned long uMin = 0;
	unsigned long uMax = 0;
	double dMin = distVec[0];
	double dMax = distVec[0];

	for (unsigned int k = 1; k < 4; ++k)
	{
		if (distVec[k] < dMin)
		{
			dMin = distVec[k];
			uMin = k;
		};
		if (distVec[k] > dMax)
		{
			dMax = distVec[k];
			uMax = k;
		};
	};
	    
	*this = CXYZSegment(posVec[uMin], posVec[uMax]);
};


//=============================================================================

bool CXYZHomoSegment::writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
							   double xfactor, double yfactor, double zfactor) const
{
	int lineCode[ 3 ] = {10, 20, 30};

	dxf.setf(ios::fixed);
	dxf.precision(decimals);

	dxf << "  0\nPOLYLINE\n  8\n" << layer << "\n 62\n " << colour
        << "\n 10\n0.0\n 20\n0.0\n 30\n0.0\n 70\n"
		<< "8\n66\n 1\n"
		<< "  0\nVERTEX\n  8\n" << layer << "\n 62\n "  << colour << endl;
	this->point1.writeDXF( dxf, lineCode, decimals, xfactor, yfactor, zfactor);

	dxf << "  0\nVERTEX\n  8\n" << layer << "\n 62\n "  << colour << endl;
	this->point2.writeDXF( dxf, lineCode, decimals, xfactor, yfactor, zfactor);
  
	dxf << "  0\nSEQEND\n  8\n0"	<< endl;


	return dxf.good();
}

//=============================================================================
