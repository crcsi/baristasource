/**
 * @class CXYPolyLineArray
 * an array of XYpolylines 
 */
#include "XYPolyLineArray.h"
#include "XYPolyLinePtrArray.h"

#include "XYPolyLine.h"
#include "Serializable.h"
#include "StructuredFileSectionArray.h"
#include "math.h"



CXYPolyLineArray::CXYPolyLineArray(int nGrowBy) :  CGenericPolyLineArray(nGrowBy),
	isActive(true)
{
    this->FileName = "XYLines";
}



CXYPolyLineArray::~CXYPolyLineArray(void)
{
}


/**
 * writes XYPolyLineArray to a TextFile, with 
 * @param cfilename
 * @returns true/false
 */
bool CXYPolyLineArray::writeXYPolyLineTextFile(CFileName cfilename)
{
    FILE	*fp;
	fp = fopen( cfilename.GetChar(),"w");

	fprintf(fp,"Number of Lines: %d\n", this->GetSize());
	
	// Create lines
	for (int i = 0; i < this->GetSize(); i++)
	{
		CXYPolyLine* line = this->getAt(i);

		fprintf(fp,"Line: %s\n", line->getLabel().GetChar());
		fprintf(fp,"Number of points: %d\n", line->getPointCount());
		fprintf(fp,"Length: %10.3lf\n", line->getLength());

		for (int j = 0; j < line->getPointCount(); j++)
		{
			CXYPoint point = &line->getPoint(j);

			double x = point.getX();
			double y = point.getY();
			
			fprintf(fp,"Point: %s %12.3lf %12.3lf\n", point.getLabel().GetChar(), x, y);
		}
		
		if ( line->isClosed())
		{
			fprintf(fp,"Type: closed\n\n");
		}
		if ( !line->isClosed())
		{
			fprintf(fp,"Type: open\n\n");
		}
	}

	fclose(fp);

    return true; 
}

/**
 * writes XYPolyLineArray to a VRML1.0File
 * @param cfilename
 * @returns true/false
 */
bool CXYPolyLineArray::writeXYPolyLineVRMLFile(CFileName cfilename)
{
    FILE *fp;
	fp = fopen(cfilename.GetChar(),"w");

	CXYPoint center;
	double sumx = 0.0;
	double sumy = 0.0;
	double sumz = 0.0;
	int pointcount = 0;

	for (int i = 0; i < this->GetSize(); i++)
	{
		CXYPolyLine* line = this->getAt(i);

		for (int j = 0; j < line->getPointCount(); j++)
		{
			CXYPoint point = line->getPoint(j);

			sumx += point.getX();
			sumy += point.getY();
			pointcount++;
		}
	}
	
 	fprintf(fp,"#VRML V1.0 ascii\n\n");

	center.set( sumx/pointcount, sumy/pointcount);

	fprintf(fp,"#	Shift for all coordinate values:\n");
	fprintf(fp,"#		X-Shift: %10.2lf\n", center.getX());
	fprintf(fp,"#		Y-Shift: %10.2lf\n", center.getY());
	//fprintf(fp,"#		Z-Shift: %10.2lf\n\n", center.getZ());

	fprintf(fp,"  Material \n");
	fprintf(fp,"  {\n");
	fprintf(fp,"      diffuseColor 1.0 0.0 0.0\n");
	fprintf(fp,"  }\n");
	fprintf(fp," \n");


	// Create lines
	for (int i = 0; i < this->GetSize(); i++)
	{
		CXYPolyLine* line = this->getAt(i);

		fprintf(fp,"  Separator\n");
		fprintf(fp,"  {\n");
		fprintf(fp,"      Label\n");
		fprintf(fp,"      {\n");
		fprintf(fp,"          label %s\n", line->getLabel().GetChar());
		fprintf(fp,"      }\n");

		fprintf(fp,"      Separator\n");
		fprintf(fp,"      {\n");
		fprintf(fp,"          Coordinate3\n");
		fprintf(fp,"          {\n");
		fprintf(fp,"              point\n");
		fprintf(fp,"              [\n");


		for (int j = 0; j < line->getPointCount(); j++)
		{
			CXYPoint point = &line->getPoint(j);

			double x = point.getX() - center.getX();
			double y = point.getY() - center.getY();			
			fprintf(fp,"                  %12.3lf %12.3lf,\n", x, y);

		}
		fprintf(fp,"              ]\n");
		fprintf(fp,"          }\n");

		fprintf(fp,"          IndexedLineSet\n");
		fprintf(fp,"          {\n");
		fprintf(fp,"              coordIndex\n");
		fprintf(fp,"              [ \n");

		for (int j = 0; j < line->getPointCount()-1; j++)
		{
			int p0 = j;
			int p1 = j+1;

			fprintf(fp,"                  %d, %d, -1,\n", p0, p1);
		}
		
		if ( line->isClosed())
		{
			fprintf(fp,"                  %d, 0, -1,\n", line->getPointCount()-1);		
		}


		fprintf(fp,"              ]\n");
		fprintf(fp,"          }\n");
		fprintf(fp,"      }\n");
		fprintf(fp,"  }\n");

		fprintf(fp," \n");
	}
	
	fclose(fp);

    return true;

}

CXYPolyLine* CXYPolyLineArray::getLine(int i)
{
    return (CXYPolyLine*)CGenericPolyLineArray::getLine(i);
}

/** SLOW FOR LOTS OF LINES!
 * Gets a line by label (null if not found)
 *
 * @param CCharString Label of line
 */
CXYPolyLine* CXYPolyLineArray::getLine(CCharString Label)
{
    return (CXYPolyLine*)CGenericPolyLineArray::getLine(Label);
}


/**
 * Serialized storage of a CXYPolyLineArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYPolyLineArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("XYpolyline filename", this->FileName.GetChar());

    for (int i = 0; i < this->GetSize(); i++)
    {
        CXYPolyLine* line = this->getAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        line->serializeStore(newSection);
    }

}

/**
 * Restores a CXYPolyLineArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYPolyLineArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("XYpolyline filename"))
            this->FileName = token->getValue();

    }

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();
    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);

        if (child->getName().CompareNoCase("CXYPolyLine"))
        {
            CXYPolyLine* line = this->add();
            line->serializeRestore(child);
        }
    }

}

