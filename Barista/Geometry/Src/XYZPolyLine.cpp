/** 
 * @class CXYZPolyLine
 * CXYZPolyLine, class for XYZPolyLines (generated from monoplotting
 *
 * @author Jochen Willneff
 * @version 1.0 
 * @date 28-june-2005
 *
 */
#include <float.h>

#include "XYZPolyLine.h"
#include "XYPolyLine.h"
#include "doubles.h"
#include "2DPoint.h"
#include "StructuredFileSectionArray.h"

CXYZPolyLine::CXYZPolyLine(void)
{
	this->obsPoints = &this->points;
}

CXYZPolyLine::~CXYZPolyLine(void)
{
}


/**
 * copy constructor, contructor which derives from CXYZPolyLine
 * @param p
 */

CXYZPolyLine::CXYZPolyLine(CXYZPolyLine* p) 
	
{
	this->obsPoints = &this->points;
	this->label = p->label;
	this->isclosed = p->isclosed;
	this->isSelected = p->isSelected;

	for ( long u = 0; u < p->getPointCount(); ++u)
	{
		this->addPoint(p->getPoint(u));
	}
}

CXYZPolyLine::CXYZPolyLine(const CXYZPolyLine &p) 
{
	this->obsPoints = &this->points;
	this->label = p.label;
	this->isclosed = p.isclosed;
	this->isSelected = p.isSelected;

	for ( long u = 0; u < p.getPointCount(); ++u)
	{
		this->addPoint(p.getPoint(u));
	}
}

CXYZPolyLine::CXYZPolyLine(const GenericPolyLine &p)
{
	this->obsPoints = &this->points;
	this->label = p.getLabel();
	this->isclosed = p.isClosed();
	this->isSelected = p.getIsSelected();

	for ( long u = 0; u < p.getPointCount(); ++u)
	{
		this->addPoint(p.getPoint(u));
	}
}


/**
 * copy constructor, contructor which derives from CXYPolyLine 
 * and initialises z coordinates by I_z
 * @param p
 * @param I_z
 * @param I_sigmaZ
 */

CXYZPolyLine::CXYZPolyLine(const CXYPolyLine &p, double I_z, double I_sigmaZ) 
{
	this->obsPoints = &this->points;
	this->label = p.getLabel();
	this->isclosed = p.isClosed();
	this->isSelected = p.getIsSelected();

	for ( long u = 0; u < p.getPointCount(); ++u)
	{
		const CXYPoint& P = p.getPoint(u);
		CXYZPoint point(P.x,P.y,I_z,P.dot);
		point.setLabel(P.getLabel());
		point.setStatus(P.getStatus());
		point.setID(P.id);
		point.getCovariance()->SubMatrix(1,2,1,2) = *P.getCovariance();
		point.setCovarElement(2,2, I_sigmaZ);
		this->addPoint(point);
	}
}


/**
 * Gets Length of CXYZPolyLine
 * returns double
 */
double CXYZPolyLine::getLength()
{
	double length = 0;
	int size = this->getPointCount();

	for ( int n = 0; n < size-1; n++)
	{
		CObsPoint* p1 = this->points.GetAt(n);
		CObsPoint* p2 = this->points.GetAt(n+1);
		length += p1->distanceFrom(p2);
	}
	
	if (this->isclosed)
	{
		CObsPoint* p1 = this->points.GetAt(size-1);
		CObsPoint* p2 = this->points.GetAt(0);
		length += p1->distanceFrom(p2);	
	}
	
    return length;
}

/**
 * Serialized storage of a CXYZPolyLine to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYZPolyLine::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("label", this->getLabel().GetChar());

    token = pStructuredFileSection->addToken();
	token->setValue("isclosed", this->isclosed);

    token = pStructuredFileSection->addToken();
	token->setValue("xyzpoints", this->points.getFileName()->GetChar());

	CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->points.serializeStore(child);
}

/**
 * Restores a CXYZPolyLine from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYZPolyLine::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("label"))
			this->label = CLabel(token->getValue());

		if(key.CompareNoCase("isclosed"))
			this->isclosed = token->getBoolValue();
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CXYZPointArray"))
        {
            this->points.serializeRestore(child);
        }
	}

}

/**
 * reconnects pointers
 */
void CXYZPolyLine::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CXYZPolyLine::resetPointers()
{
    CSerializable::resetPointers();
}



/**
 * Flag if CXYZPolyLine is modified
 * returns bool
 */
bool CXYZPolyLine::isModified()
{
    return this->bModified;
}



void CXYZPolyLine::addPoint(CXYZPoint newpoint)
{
    this->points.add(newpoint);
}


long CXYZPolyLine::getIntersections(double I_x, doubles &intersections) const
{
	intersections.clear();

	if (this->getPointCount() > 1)
	{
		long index = 0;
		CXYZPoint *P1;

		if (this->isclosed)
		{
			P1 = this->points.getAt(this->getPointCount() - 1);
			index = 0;
		}
		else 
		{
			P1 = this->points.getAt(0);
			index = 1;
		}

		for (; index < this->getPointCount(); ++index)
		{
			CXYZPoint *P2 = this->points.getAt(index);
			double xn = I_x;

			if ((I_x == P1->x) || (I_x == P2->x)) xn -= 10.0f * FLT_EPSILON;
			if (((xn <= P1->x) && (xn >= P2->x)) || ((xn >= P1->x) && (xn <= P2->x)))
			{
				double y  = P1->y;
				double dx = P2->x - P1->x;
				if (fabs(dx) > FLT_EPSILON) y = P1->y + (P2->y - P1->y) * (xn - P1->x) / dx;
				intersections.push_back(y);
			};
			P1 = P2;
		};
  
		if (intersections.size() > 1) intersections.sort();
	};

   
	return intersections.size();
};

//=============================================================================

bool CXYZPolyLine::contains(const CXYZPoint &p) const
{
	bool Contains = true;
	doubles intersections;
	if (!this->isclosed) Contains = false;
	else
	{
		int inters = this->getIntersections(p.x, intersections);

		if (inters < 1) Contains = false;
		else
		{
			int firstI = -1;

			for (unsigned int i = 0; i < intersections.size() && firstI < 0; ++i)
			{
				if (intersections[i] > p.y) firstI = i;
			}
			if (firstI < 0) Contains = false;
			else
			{
				firstI = intersections.size() - firstI;
				if (!(firstI % 2)) Contains = false;
			}
		}
	}

	return Contains;
};

//=============================================================================

bool CXYZPolyLine::contains(const CXYZPolyLine &l) const
{

	for (int i = 0; i < l.getPointCount(); ++i)
	{
		if (!contains(l.points.getAt(i))) return false;
	}
	return true;
};
	
//=============================================================================

void CXYZPolyLine::append(const CXYZPolyLine &poly)
{
	for (int i = 0; i < poly.getPointCount(); ++i)
	{
		this->addPoint(poly[i]);
	}
};

//=============================================================================

bool CXYZPolyLine::writeDXFHeader(ofstream &dxf)
{ 
	if ( !dxf ) return false;

	dxf << "  0\nSECTION\n  2\nENTITIES\n";

	return dxf.good();
}

//=============================================================================

bool CXYZPolyLine::writeDXFTrailer(ofstream &dxf)
{
	if ( !dxf ) return false;
	dxf << "  0\nENDSEC\n  0\nEOF\n";
	bool ret = dxf.good();
	dxf.close();
	return ret;
}

//=============================================================================

bool CXYZPolyLine::writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
							double xfactor, double yfactor, double zfactor) const
{
	if (this->getPointCount() < 2) return false;

	int lineCode[ 3 ] = {10, 20, 30};

	dxf.setf(ios::fixed);
	dxf.precision(decimals);

	dxf << "  0\nPOLYLINE\n  8\n" << layer << "\n 62\n " << colour
        << "\n 10\n0.0\n 20\n0.0\n 30\n0.0\n 70\n";

	if (this->isclosed) dxf << "9\n";
	else dxf << "8\n"	;
  	dxf << "66\n 1\n";
	for (int i = 0; i < this->getPointCount(); ++i)
	{
		CXYZPoint *p = this->points.getAt(i);
		dxf << "  0\nVERTEX\n  8\n" << layer << "\n 62\n "  << colour << endl;
    
    
		p->writeDXF( dxf, lineCode, decimals, xfactor, yfactor, zfactor);
	}
  
	dxf << "  0\nSEQEND\n  8\n0"	<< endl;


	return dxf.good();
}

//=============================================================================

void CXYZPolyLine::closeLine()
{
	if (getPointCount() > 1)
	{
		isclosed = true;
	}
};

void CXYZPolyLine::openLine()
{
	if (getPointCount() > 1)
	{
		isclosed = false;
	}
};

//@ todo CTrans3D has to bo finished first
/*
CXYZPolyLine CXYZPolyLine::transform(CTrans3D trans)
{
    CXYZPolyLine newline;

    for (int i = 0; i < this->points->GetSize(); i++)
    {
        newline.addPoint(trans.transform(this->getPoint(i)));
    }

    return newline;
}
*/

