
#include <iostream>
#include <float.h>
#include <math.h>

#include "StructuredFileSection.h"
#include "newmat.h"

#include "ObsPoint.h"


void CObsPoint::initCovar()
{
	if (this->covariance) delete this->covariance;
	this->covariance = new Matrix (this->dim,this->dim);

	*(this->covariance) = 0;
	 for (int i=0; i< this->dim; i++)
		this->covariance->element(i, i) = 1.0;
};

CObsPoint::CObsPoint(int dimension, CLabel label) : 
CPointBase(dimension), label(label),status(1), covariance(0)
{
	this->initCovar();
}

CObsPoint::CObsPoint(int dimension, CLabel label, int status):
CPointBase(dimension), label(label),status(status), covariance(0)
{
	this->initCovar();
}

CObsPoint::CObsPoint(const CObsPoint *point): CPointBase(point), label(point->label), status(point->status), covariance(0)
{
	this->setCovariance(*(point->covariance));
}

CObsPoint::~CObsPoint(void)
{
	delete this->covariance;
}

CObsPoint &CObsPoint::operator = (const CObsPoint & point)
{
	if (point.dim != this->dim)
	{
		delete [] this->coordinates;
		this->coordinates = new double [point.dim];
	}
		
	for (int i=0; i < point.dim; i++)
		this->coordinates[i] = point.coordinates[i];

	this->dim = point.dim;
	this->id = point.id;
	this->dot = point.dot;
	this->label = point.label;
	this->status = point.status;
	(*this->covariance) = (*point.covariance);
	return *this;
}

CObsPoint *CObsPoint::operator = (const CObsPoint * point)
{
	if (point->dim != this->dim)
	{
		delete [] this->coordinates;
		this->coordinates = new double [point->dim];
	}
		
	for (int i=0; i < point->dim; i++)
		this->coordinates[i] = point->coordinates[i];

	this->dim = point->dim;
	this->id = point->id;
	this->dot = point->dot;
	this->label = point->label;
	this->status = point->status;
	(*this->covariance) = (*point->covariance);
	return this;
}

const double CObsPoint::getCovarElement(int row, int col) const
{
	assert(row >= 0 && row < this->dim && col >= 0 && col < this->dim);

	if (row == col) return sqrt(this->covariance->element(row,col));
	else			return this->covariance->element(row,col);
}

const double CObsPoint::getSigma(int index) const
{
	assert(index >= 0 && index < this->dim);

	return sqrt(this->covariance->element(index,index));
}


void CObsPoint::setCovarElement(int row, int col, double value)
{
	assert(row >= 0 && row < this->dim && col >= 0 && col < this->dim);

	this->bModified = true;

	if (row == col) //set all other correlated values to zero
	{
		for (int i=0; i< this->dim; i++)
		{
			this->covariance->element(row,i) = 0.0;
			this->covariance->element(i,col) = 0.0;
		}
		this->covariance->element(row,col) = value*value;
	}
	else
	{
		this->covariance->element(row,col) = value;
		this->covariance->element(col,row) = value;
	}
}

void CObsPoint::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CCharString valueStr;
	CCharString elementStr;

	CSerializable::serializeStore(pStructuredFileSection);

	valueStr = "";

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("label", this->getLabel().GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("status", this->status);

    token = pStructuredFileSection->addToken();
    token->setValue("dot", this->dot);

    token = pStructuredFileSection->addToken();
    token->setValue("id", this->id);


	CToken* pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("dim", this->dim);

	pNewToken = pStructuredFileSection->addToken();
	for (int i = 0; i < this->dim; i++)
		{
			double element = this->coordinates[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	pNewToken->setValue("coordinates", valueStr.GetChar());	

	valueStr.Empty();
	elementStr.Empty();	

	pNewToken = pStructuredFileSection->addToken();
	for (int i=0; i< this->dim; i++)
	{
		for (int k=0; k< this->dim; k++)
		{
			double element = this->covariance->element(i,k);
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}
	}

	pNewToken->setValue("covariance", valueStr.GetChar());	

}

void CObsPoint::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("label"))
			this->label = CLabel(token->getValue());
        else if (key.CompareNoCase("status"))
            this->status = token->getIntValue();
        else if (key.CompareNoCase("dot"))
            this->dot = token->getIntValue();
        else if (key.CompareNoCase("id"))
            this->id = token->getIntValue();
        else if (key.CompareNoCase("dim"))
            this->dim = token->getIntValue();
	    else if (key.CompareNoCase("coordinates"))
		{
			if (this->coordinates)
				delete [] this->coordinates;
			this->coordinates = new double [this->dim];

			token->getDoubleValues(this->coordinates,this->dim);
		}
		else if (key.CompareNoCase("covariance"))
		{
			double* tmpArray = new double [this->dim*this->dim];
			token->getDoubleValues(tmpArray,this->dim*this->dim);

			if(this->covariance)
				delete this->covariance;
			
			this->covariance = new Matrix(this->dim,this->dim);
			
			for (int i=0; i< this->dim; i++)
			{
				for (int k=0; k< this->dim; k++)
				{
					this->covariance->element(i,k) = tmpArray[i*this->dim+k];
				}	 
			}

			if (tmpArray) delete [] tmpArray;
		}

    }
}

void CObsPoint::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

void CObsPoint::resetPointers()
{
    CSerializable::resetPointers();
}

bool CObsPoint::isModified()
{
	return this->bModified;
}

void CObsPoint::setCovariance (const Matrix &covariance)
{
    this->bModified = true;
	if (!this->covariance) this->covariance = new Matrix(covariance);
	else *(this->covariance) = covariance;
}


CObsPoint CObsPoint::operator - () const
{ 
	CObsPoint tmp(this);
	for (int i=0; i < this->dim; i++)
		tmp[i] = -(*this)[i];
	return tmp; 
}

// addition and subtraction of points 
void CObsPoint::operator += ( const CObsPoint &p )
{	
	int size = p.dim < this->dim ? p.dim : this->dim;

	for (int i=0; i < size; i++)
		(*this)[i] += p[i];
}

void CObsPoint::operator -= ( const CObsPoint &p )
{	
	int size = p.dim < this->dim ? p.dim : this->dim;

	for (int i=0; i < size; i++)
		(*this)[i] -= p[i];
}

CObsPoint operator + ( const CObsPoint &p1, const CObsPoint &p2 )
{ 
	int sizeMin = p1.dim < p2.dim ? p1.dim : p2.dim;

	CObsPoint tmp(sizeMin);

	for (int i=0; i< sizeMin; i++)
		tmp[i] = p1[i] + p2[i];	

	return tmp; 
}

CObsPoint operator - ( const CObsPoint &p1, const CObsPoint &p2 )
{ 
	int sizeMin = p1.dim < p2.dim ? p1.dim : p2.dim;

	CObsPoint tmp(sizeMin);

	for (int i=0; i< sizeMin; i++)
		tmp[i] = p1[i] - p2[i];	

	return tmp; 
}

void CObsPoint::operator *= ( double a )
{	
	for (int i=0; i < this->dim; i++)
		(*this)[i] *= a;
}

CObsPoint CObsPoint::operator * ( double a ) const
{	
	CObsPoint tmp(this);

	for (int i=0; i < tmp.dim; i++)
		tmp[i] *= a;
	return tmp; 
}

CObsPoint operator * ( double a, const CObsPoint &p )
{ 
	return  p * a; 
}

void CObsPoint::operator /= ( double a )
{	
	for (int i=0; i < this->dim; i++)
		(*this)[i] /= a;
}

CObsPoint CObsPoint::operator / ( double a ) const
{ 
	CObsPoint tmp(this);

	for (int i=0; i < tmp.dim; i++)
		tmp[i] /= a;
	return tmp; 
}


/**
 * Calculates the distance between two CObsPoints
 * @param point
 * returns double value of the distance 
 */
double CObsPoint::distanceFrom (const CObsPoint* point) const
{
    assert(this->dim == point->dim);
	double sum = 0.0;
	for (int i=0; i< this->dim; i++)
		sum+= (this->coordinates[i] - point->coordinates[i])*(this->coordinates[i] - point->coordinates[i]);

    return sqrt(sum);
}

/**
 * Calculates the distance between two CObsPoints
 * @param point
 * returns double value of the distance 
 */
double CObsPoint::distanceFrom (const CObsPoint &point) const
{
    assert(this->dim == point.dim);
	double sum = 0.0;
	for (int i=0; i< this->dim; i++)
	{
		double diff = this->coordinates[i] - point.coordinates[i];
		sum += diff * diff;
	}

    return sqrt(sum);
}
