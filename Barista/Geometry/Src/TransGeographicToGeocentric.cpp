#include "TransGeographicToGeocentric.h"


CTransGeographicToGeocentric::CTransGeographicToGeocentric(const CReferenceSystem &sysFrom, 								
														   const CReferenceSystem &sysTo) :
				CTrans3D(sysFrom, sysTo)
{
}

CTransGeographicToGeocentric::~CTransGeographicToGeocentric()
{
}

void CTransGeographicToGeocentric::transform(C3DPoint &out, const C3DPoint &in) 
{
	this->systemFrom.geographicToGeocentric(out, in);
}

void CTransGeographicToGeocentric::transform(CXYZPoint &out, const CXYZPoint &in) 
{
	this->systemFrom.geographicToGeocentric(out, in);
}

void CTransGeographicToGeocentric::getDerivatives(Matrix &derivatives, const C3DPoint &p) const
{
	this->systemFrom.derivativesGeographicToGeocentric(derivatives, p);
};
