/**
 * @class CXYZPolyLineArray
 * an array of xyzpolylines 
 */
#include <iostream>
using namespace std;

#include "XYZPolyLineArray.h"
#include "XYZPolyLinePtrArray.h"
#include "Spheroid.h"

#include "XYZPolyLine.h"
#include "Serializable.h"
#include "StructuredFileSectionArray.h"
#include "math.h"

//addition >>
//#include "ogrsf_frmts.h"
//#include <QMessageBox>
//<< addition

CXYZPolyLineArray::CXYZPolyLineArray(int nGrowBy) : CGenericPolyLineArray(nGrowBy)
	
{
    this->FileName = "XYZLines";
}

CXYZPolyLineArray::~CXYZPolyLineArray(void)
{
}



CXYZPolyLine* CXYZPolyLineArray::getLine(int i)
{
	return (CXYZPolyLine*)CGenericPolyLineArray::getLine(i);
}

/**
 * writes XYZPolyLineArray to a TextFile, with 
 * @param cfilename
 * @returns true/false
 */
bool CXYZPolyLineArray::writeXYZPolyLineTextFile(CFileName cfilename)
{
    FILE	*fp;
	fp = fopen( cfilename.GetChar(),"w");

	fprintf(fp,"Number of Lines: %d\n", this->GetSize());
	
	CCharString str = this->getReferenceSystem()->getCoordinateSystemName();
	fprintf(fp,"Coordinate system: %s\n\n", str.GetChar());
	
	// Create lines
	for (int i = 0; i < this->GetSize(); i++)
	{
		CXYZPolyLine* line = this->getAt(i);

		fprintf(fp,"Line: %s\n", line->getLabel().GetChar());
		fprintf(fp,"Number of points: %d\n", line->getPointCount());
		fprintf(fp,"Length: %10.3lf\n", line->getLength());

		for (int j = 0; j < line->getPointCount(); j++)
		{
			CXYZPoint *p = line->getPoint(j);
			
			fprintf(fp,"Point: %s %12.3lf %12.3lf %12.3lf\n", p->getLabel().GetChar(), p->x, p->y, p->z);
		}
		
		if ( line->isClosed())
		{
			fprintf(fp,"Type: closed\n\n");
		}
		if ( !line->isClosed())
		{
			fprintf(fp,"Type: open\n\n");
		}
	}

	fclose(fp);

    return true; 
}

/**
 * writes XYZPolyLineArray to a VRML1.0File
 * @param cfilename
 * @returns true/false
 */
bool CXYZPolyLineArray::writeXYZPolyLineVRMLFile(CFileName cfilename)
{
    FILE *fp;
	fp = fopen(cfilename.GetChar(),"w");

	CXYZPoint center;
	double sumx = 0.0;
	double sumy = 0.0;
	double sumz = 0.0;
	int pointcount = 0;

	for (int i = 0; i < this->GetSize(); i++)
	{
		CXYZPolyLine* line = this->getAt(i);

		for (int j = 0; j < line->getPointCount(); j++)
		{
			CObsPoint* point = line->getPoint(j);

			sumx += (*point)[0];
			sumy += (*point)[1];
			//sumz += point->getZ();
			pointcount++;
		}
	}
	
 	fprintf(fp,"#VRML V1.0 ascii\n\n");

	center.set( sumx/pointcount, sumy/pointcount, sumz/pointcount);

	bool transform = false;

	if (this->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)
	{
		this->getReferenceSystem()->setUTMZone(center.getX(), center.getY());
		this->getReferenceSystem()->geographicToGrid(center, center);
		transform = true;
	}

	fprintf(fp,"#	Coordinate system: %s\n", this->getReferenceSystem()->getCoordinateSystemName().GetChar());
	fprintf(fp,"#	Shift for all coordinate values:\n");
	fprintf(fp,"#		X-Shift: %10.2lf\n", center.getX());
	fprintf(fp,"#		Y-Shift: %10.2lf\n", center.getY());
	//fprintf(fp,"#		Z-Shift: %10.2lf\n\n", center.getZ());

	fprintf(fp,"  Material \n");
	fprintf(fp,"  {\n");
	fprintf(fp,"      diffuseColor 1.0 0.0 0.0\n");
	fprintf(fp,"  }\n");
	fprintf(fp," \n");


	// Create lines
	for (int i = 0; i < this->GetSize(); i++)
	{
		CXYZPolyLine* line = this->getAt(i);

		fprintf(fp,"  Separator\n");
		fprintf(fp,"  {\n");
		fprintf(fp,"      Label\n");
		fprintf(fp,"      {\n");
		fprintf(fp,"          label %s\n", line->getLabel().GetChar());
		fprintf(fp,"      }\n");

		fprintf(fp,"      Separator\n");
		fprintf(fp,"      {\n");
		fprintf(fp,"          Coordinate3\n");
		fprintf(fp,"          {\n");
		fprintf(fp,"              point\n");
		fprintf(fp,"              [\n");


		for (int j = 0; j < line->getPointCount(); j++)
		{
			CXYZPoint point = (*line->getPoint(j));

			if (transform) this->getReferenceSystem()->geographicToGrid(point, point);

			double x = point.getX() - center.getX();
			double y = point.getY() - center.getY();
			double z = point.getZ();// - center.getZ();
			
			fprintf(fp,"                  %12.3lf %12.3lf %12.3lf,\n", x, y, z);

		}
		fprintf(fp,"              ]\n");
		fprintf(fp,"          }\n");

		fprintf(fp,"          IndexedLineSet\n");
		fprintf(fp,"          {\n");
		fprintf(fp,"              coordIndex\n");
		fprintf(fp,"              [ \n");

		for (int j = 0; j < line->getPointCount()-1; j++)
		{
			int p0 = j;
			int p1 = j+1;

			fprintf(fp,"                  %d, %d, -1,\n", p0, p1);
		}
		
		if ( line->isClosed())
		{
			fprintf(fp,"                  %d, 0, -1,\n", line->getPointCount()-1);		
		}


		fprintf(fp,"              ]\n");
		fprintf(fp,"          }\n");
		fprintf(fp,"      }\n");
		fprintf(fp,"  }\n");

		fprintf(fp," \n");
	}
	
	fclose(fp);
    return true;

}


/** SLOW FOR LOTS OF LINES!
 * Gets a line by label (null if not found)
 *
 * @param CCharString Label of line
 */

CXYZPolyLine* CXYZPolyLineArray::getLine(CCharString Label)
{
	return (CXYZPolyLine*)CGenericPolyLineArray::getLine(Label);
}


/**
 * Serialized storage of a CXYZPolyLineArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYZPolyLineArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("xyzpolyline filename", this->FileName.GetChar());

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->getReferenceSystem()->serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->getDisplaySettings()->serializeStore(child);

    for (int i = 0; i < this->GetSize(); i++)
    {
        CXYZPolyLine* line = this->getAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        line->serializeStore(newSection);
    }

}

/**
 * Restores a CXYZPolyLineArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CXYZPolyLineArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("xyzpolyline filename"))
            this->FileName = token->getValue();

    }

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();
    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);

        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refSystem.serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CDisplaySettings"))
        {
			this->displaySettings.serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CXYZPolyLine"))
        {
            CXYZPolyLine* line = this->add();
            line->serializeRestore(child);
        }
    }

}




/**
 * writes XYZPolyLineArray to a DXFFile, with 
 * @param cfilename
 * @returns true/false
 */
bool CXYZPolyLineArray::writeXYZPolyLineDXFFile(CFileName cfilename)
{
	ofstream dxf(cfilename.GetChar());
	bool ret = CXYZPolyLine::writeDXFHeader(dxf);
	const char *layer = "Barista3DLines";
	int colour = 1;
	int decimals = 3;

	if (ret)
	{
		// Create lines
		for (int i = 0; i < this->GetSize(); i++)
		{
			CXYZPolyLine* line = this->getAt(i);
			ret &= line->writeDXF(dxf, layer, colour, decimals);
		}

		ret &= CXYZPolyLine::writeDXFTrailer(dxf);
	}

    return ret; 
}


/**
 * writes XYZPolyLineArray to a shapefile, with 
 * @param cfilename
 * @returns true/false
 */
bool CXYZPolyLineArray::writeXYZPolyLineShapefile(CFileName cfilename)
{
	// source: http://gdal.org/ogr/ogr_apitut.html
	// we start by registering all the drivers, 
	// and then fetch the Shapefile driver as we will need it to create our output file. 
	/*const char *pszDriverName = "ESRI Shapefile";
    OGRSFDriver *poDriver;

    OGRRegisterAll();

    poDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName(
                pszDriverName );
    if( poDriver == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Driver not found!");
		return false;
    }

	// Next we create the datasource.
	OGRDataSource *poDS;

    poDS = poDriver->CreateDataSource((const char *) cfilename, NULL );
    if( poDS == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Datasource creation failed!");
		return false;
    }

	// Now we create the output layer.
	 OGRLayer *poLayer;

    poLayer = poDS->CreateLayer("XYZPolylines", NULL, wkbLineString25D, NULL );
    if( poLayer == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Layer creation failed!");
		return false;
    }


	OGRFieldDefn lField( "Name", OFTString);
	lField.SetWidth(32);

	if( poLayer->CreateField( &lField ) != OGRERR_NONE )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Data field creation failed!");
		return false;
    }

	OGRFieldDefn hField( "Height", OFTReal);
    hField.SetPrecision(3);


    if( poLayer->CreateField( &hField ) != OGRERR_NONE )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Data field creation failed!");
		return false;
    }


	//double x, y;
    //char szName[33];
	//const char* buf;
    double x, y, z;//, tx[2], ty[2];
	double ht, fH, lH;
	

	for (int i = 0; i < this->GetSize(); i++)
	{
		OGRLineString ls;

		CXYZPolyLine* line = this->getAt(i);
		int nPoint = line->getPointCount();
		for (int j = 0; j < nPoint; j++)
		{
			CObsPoint* point = line->getPoint(j);
			x = (*point)[0];
			y = (*point)[1];
			z = (*point)[2];

			ls.addPoint(x,y,z);
			if (j == 0)
				fH = z;
			else if (j == nPoint-1)
				lH = z;
		}

		OGRFeature *poFeature;
        poFeature = OGRFeature::CreateFeature( poLayer->GetLayerDefn() );
		poFeature->SetField("Name", line->getLabel().GetChar());
		ht = lH - fH;
        poFeature->SetField("Height", ht);
		

		poFeature->SetGeometry( &ls ); 
		if( poLayer->CreateFeature( poFeature ) != OGRERR_NONE )
        {
           //QMessageBox::warning(NULL, " Writing to shapefile!"
			//, "Feature creation failed!");
			return false;
        }
		// Now we create a feature in the file. The OGRLayer::CreateFeature() 
		// does not take ownership of our feature so we clean it up when done with it.
		OGRFeature::DestroyFeature( poFeature );
	}
	// Finally we need to close down the datasource in order to ensure headers are 
	// written out in an orderly way and all resources are recovered.
    OGRDataSource::DestroyDataSource( poDS ); 
*/
	return true;
}