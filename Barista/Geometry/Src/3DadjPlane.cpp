//============================================================================

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      IPI, University of Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//============================================================================

#include <float.h>
#include <ostream>
#include <strstream>
using namespace std;

#include "newmatap.h" //for advanced matrix routines (LU;QR,SVD,..)
#include "newmatio.h" //for the output routines
#include "myexcept.h" //for the exceptions in NM

#include "statistics.h"
#include "3DadjPlane.h"
#include "XYLineSegment.h"
#include "XYZLinesAdj.h"

//============================================================================

 float C3DAdjustPlane::quantil = 1.96f;

 float C3DAdjustPlane::chiSquareQuantil[MAXDOF] =
     { 1.00000000f,  3.841455338f,  5.991476357f,  7.814724703f,  9.487728465f,
      11.07048257f, 12.591577420f, 14.067127260f, 15.507312490f, 16.918960160f };

//============================================================================

 C3DAdjustPlane::C3DAdjustPlane() : nPoints(0), sigma0 (-1.0), sumW(0.0),
	      redundancy(0.0), eigenVectorRot(3,3), qxx(4),sums(0.0,0.0,0.0),
		  reduction(0.0,0.0,0.0), squaredSumsXY(0.0,0.0,0.0),
		  squaredSumsZ(0.0,0.0,0.0), eigenValues(0.0,0.0,0.0)
{
	this->eigenVectorRot = 0.0;
	this->qxx = 0.0;
};

//============================================================================

 C3DAdjustPlane::C3DAdjustPlane(const C3DAdjustPlane &plane):
          C3DPlane(plane), nPoints(plane.nPoints), sigma0(plane.sigma0),
		  sumW(plane.sumW), redundancy(plane.redundancy),
          squaredSumsXY(plane.squaredSumsXY), squaredSumsZ(plane.squaredSumsZ),
          sums(plane.sums), qxx(plane.qxx), eigenVectorRot(plane.eigenVectorRot),
		  reduction(plane.reduction), eigenValues(plane.eigenValues)
{
};

//============================================================================

 C3DAdjustPlane::C3DAdjustPlane(const C3DPoint &p, const C3DPoint &n, C3DPoint *pReduction):
          C3DPlane(p, n), nPoints(0), sigma0 (-1.0), sumW(0.0),
	      redundancy(0.0), eigenVectorRot(3,3), qxx(4), sums(0.0,0.0,0.0),
		  reduction(0.0,0.0,0.0), squaredSumsXY(0.0,0.0,0.0),
		  squaredSumsZ(0.0,0.0,0.0), eigenValues(0.0,0.0,0.0)
 {
	 if (pReduction)
	 {
		 this->reduction = *pReduction;
		 this->point -= this->reduction;
		 this->planeConstant = this->normal.innerProduct(this->point);
	 }
 };

//============================================================================

 C3DAdjustPlane::C3DAdjustPlane(const CXYLineSegment &line2D, double z0):
          C3DPlane(C3DPoint(line2D.getCog().x, line2D.getCog().y, z0), line2D.getNormal()),
		  nPoints(line2D.getPointcount()), sigma0(line2D.getSigma0()), sumW(line2D.getPointcount()),
		  redundancy(0), eigenValues(line2D.getEigenvalues()), qxx(4),
          eigenVectorRot(3,3)
{
	if (line2D.getPointcount() > 2) this->redundancy = line2D.getPointcount() - 2;

	Matrix r = line2D.getRotMat();

	this->eigenVectorRot.element(0,0) = r.element(0,0); this->eigenVectorRot.element(0,1) = r.element(0,1); this->eigenVectorRot.element(0,2) = r.element(0,2);
	this->eigenVectorRot.element(1,0) = r.element(1,0); this->eigenVectorRot.element(1,1) = r.element(1,1); this->eigenVectorRot.element(1,2) = r.element(1,2);
	this->eigenVectorRot.element(2,0) = r.element(2,0); this->eigenVectorRot.element(2,1) = r.element(2,1); this->eigenVectorRot.element(2,2) = r.element(2,2);

	Matrix pQxx2D = line2D.getCovarianceCopy();

	this->qxx.element(0,0) = pQxx2D.element(0,0); this->qxx.element(0,1) = pQxx2D.element(0,1); this->qxx.element(0,2) = 0.0f; this->qxx.element(0,3) = this->qxx.element(0,2);
							                      this->qxx.element(1,1) = pQxx2D.element(1,1); this->qxx.element(1,2) = 0.0f; this->qxx.element(1,3) = this->qxx.element(1,2);
															                                    this->qxx.element(2,2) = 0.0f; this->qxx.element(2,3) = 0.0f;
																								                               this->qxx.element(3,3) = this->qxx.element(2,2);
};

//============================================================================

void C3DAdjustPlane::setQuantil (float percentage)
{
	quantil = float(fabs(statistics::normalFractil((1.0f - percentage) * 0.5f)));
};

//============================================================================

void C3DAdjustPlane::setChiSquareQuantils(float percentage)
{
	chiSquareQuantil[0] = 1.0f;
	for (unsigned long u = 1; u < MAXDOF; ++u)
		chiSquareQuantil[u] = float(statistics::chiSquareFractil(percentage, u));
};

//============================================================================

void C3DAdjustPlane::copyMatrixToSymmetricMatrix (const Matrix &mat, SymmetricMatrix &matsym)
{
	if ((matsym.Ncols() != mat.Ncols()) || (matsym.Nrows() != mat.Nrows()))
	{
		if (mat.Ncols() > mat.Nrows()) matsym = SymmetricMatrix(mat.Ncols());
		else matsym = SymmetricMatrix(mat.Nrows());
	};

	for (unsigned long r = 0; r < (unsigned long) mat.Nrows(); ++r)
		for (unsigned long c = r; c < (unsigned long) mat.Ncols(); ++c)
			matsym.element(r, c) = mat.element(r, c);
};

//============================================================================

void C3DAdjustPlane::resetSums(const C3DPoint &reductionPoint)
{
	this->squaredSumsXY.x = this->squaredSumsXY.y = this->squaredSumsXY.z = 0.0;
	this->squaredSumsZ.x  = this->squaredSumsZ.y  = this->squaredSumsZ.z  = 0.0;
	this->sums.x          = this->sums.y          = this->sums.z          = 0.0;
	this->sumW = 0.0;
	this->nPoints = 0;
	this->sigma0 = -1.0;
	this->redundancy = 0.0f;
	this->eigenValues.x = this->eigenValues.y = this->eigenValues.z = 0.0;
	this->qxx = 0.0f;
	this->reduction = reductionPoint;
}

//============================================================================

void C3DAdjustPlane::copySums(const C3DAdjustPlane &plane)
{
	this->squaredSumsXY  = plane.squaredSumsXY;
	this->squaredSumsZ   = plane.squaredSumsZ;
	this->sums           = plane.sums;
	this->sumW           = plane.sumW;
	this->nPoints        = plane.nPoints;
	this->sigma0         = plane.sigma0;
	this->redundancy     = plane.redundancy;
	this->reduction      = plane.reduction;
	this->eigenValues    = plane.eigenValues;
	this->eigenVectorRot = plane.eigenVectorRot;
	this->qxx            = plane.qxx;
};

//============================================================================

void C3DAdjustPlane::reduceSum()
{
	if (this->nPoints > 0)
	{
		this->point = this->sums / this->sumW;
		this->squaredSumsXY.x += this->point.x * this->point.x * this->sumW - 2.0 * this->point.x * this->sums.x;
		this->squaredSumsXY.y += this->point.y * this->point.y * this->sumW - 2.0 * this->point.y * this->sums.y;
		this->squaredSumsXY.z += this->point.x * this->point.y * this->sumW - this->point.x * this->sums.y -
															                  this->point.y * this->sums.x;

		this->squaredSumsZ.x += this->point.z * this->point.x * this->sumW - this->point.z * this->sums.x - this->point.x * this->sums.z;
		this->squaredSumsZ.y += this->point.y * this->point.z * this->sumW - this->point.y * this->sums.z - this->point.z * this->sums.y;
		this->squaredSumsZ.z += this->point.z * this->point.z * this->sumW - 2.0 * this->point.z * this->sums.z;

		this->sums.x = this->sums.y = this->sums.z = 0.0;
	};
}

//============================================================================

void C3DAdjustPlane::deReduceSum()
{
	if (this->nPoints < 1) return;

	this->sums = this->point * this->sumW;
	this->squaredSumsXY.x -= this->point.x * this->point.x * this->sumW - 2.0*this->point.x*this->sums.x;
	this->squaredSumsXY.y -= this->point.y * this->point.y * this->sumW - 2.0*this->point.y*this->sums.y;
	this->squaredSumsXY.z -= this->point.x * this->point.y * this->sumW - this->point.x*this->sums.y - this->point.y*this->sums.x;

	this->squaredSumsZ.x -= this->point.z * this->point.x * this->sumW - this->point.z*this->sums.x - this->point.x*this->sums.z;
	this->squaredSumsZ.y -= this->point.y * this->point.z * this->sumW - this->point.y*this->sums.z - this->point.z*this->sums.y;
	this->squaredSumsZ.z -= this->point.z * this->point.z * this->sumW - 2.0*this->point.z*this->sums.z;
}

//============================================================================

void C3DAdjustPlane::addPoints(unsigned int num, C3DPoint* pPoints, double *pWeights )
{
	if (!pWeights)
	{
		for(unsigned int c = 0; c < num ; c++ ) // no weights given
		{
			this->addPoint(pPoints[c]);
		};
	}
	else
	{
		for(unsigned int c = 0; c < num ; c++ )
		{
			this->addPoint(pPoints[c], pWeights[c]);
		};
	};

	this->sigma0 = -1.0;
}

//============================================================================

void C3DAdjustPlane::addPoints(const vector<C3DPoint> &pointVect,
							   const vector<double> &pVect )
{
	unsigned int nTmp = pointVect.size() ; // number of points, also upper bound of loop

	if (pVect.size() > 0 && pVect.size() < nTmp)  nTmp = pVect.size() ;

	if (pVect.size() == 0) // no weights given
	{
		for (unsigned int c = 0 ; c < nTmp ; c++)
		{
			this->addPoint(pointVect[c]);
		}
	}
	else
	{
		for (unsigned int c = 0 ; c < nTmp ; c++ )
		{
			this->addPoint(pointVect[c], pVect[c]);
		}
	};

	this->sigma0 = -1.0;
};

//============================================================================

void C3DAdjustPlane::addPoint(const C3DPoint& p, const double w)
{
	if (w > 0.0)
	{
		C3DPoint pRed(p);
		pRed -= this->reduction;
		this->squaredSumsXY.x += pRed.x * pRed.x * w;
		this->squaredSumsXY.y += pRed.y * pRed.y * w;
		this->squaredSumsXY.z += pRed.x * pRed.y * w;

		this->squaredSumsZ.x  += pRed.z * pRed.x * w;
		this->squaredSumsZ.y  += pRed.y * pRed.z * w;
		this->squaredSumsZ.z  += pRed.z * pRed.z * w;

		this->sums  += pRed * w;
		this->sumW  += w;
		this->nPoints++;
	};
};

//============================================================================

void C3DAdjustPlane::addPoint(const C3DPoint& p)
{
	C3DPoint pRed(p);
	pRed -= this->reduction;

	this->squaredSumsXY.x += pRed.x * pRed.x;
	this->squaredSumsXY.y += pRed.y * pRed.y;
	this->squaredSumsXY.z += pRed.x * pRed.y;

	this->squaredSumsZ.x  += pRed.z * pRed.x;
	this->squaredSumsZ.y  += pRed.y * pRed.z;
	this->squaredSumsZ.z  += pRed.z * pRed.z;

	this->sums  += pRed;
	this->sumW  += 1.0;
	this->nPoints++;
};

//============================================================================

C3DAdjustPlane C3DAdjustPlane::operator+= (const C3DAdjustPlane& plane)
{
	C3DPoint delta = this->reduction - plane.reduction;
	this->squaredSumsXY   += plane.squaredSumsXY;
	this->squaredSumsXY.x += delta.x * (plane.sumW * (this->reduction.x + plane.reduction.x) - 2.0 * plane.sums.x);
	this->squaredSumsXY.y += delta.y * (plane.sumW * (this->reduction.y + plane.reduction.y) - 2.0 * plane.sums.y);
	this->squaredSumsXY.z += plane.sums.x * (-delta.y) + plane.sums.y * (-delta.x) +
		                     plane.sumW * (this->reduction.x * this->reduction.y - plane.reduction.x * plane.reduction.y);
	this->squaredSumsZ   += plane.squaredSumsZ;
	this->squaredSumsZ.x += plane.sums.x * (-delta.z) + plane.sums.z * (-delta.x) +
						    plane.sumW * (this->reduction.x * this->reduction.z - plane.reduction.x * plane.reduction.z);
	this->squaredSumsZ.y += plane.sums.y * (-delta.z) + plane.sums.z * (-delta.y) +
						    plane.sumW * (this->reduction.y * this->reduction.z - plane.reduction.y * plane.reduction.z);
	this->squaredSumsZ.z += (delta.z) * (plane.sumW * (this->reduction.z + plane.reduction.z) - 2* plane.sums.z);

	this->sums += plane.sums;
	this->sums.x -= plane.sumW * delta.x;
	this->sums.y -= plane.sumW * delta.y;

	this->sumW    += plane.sumW;
	this->nPoints += plane.nPoints;
	this->sigma0 = -1.0;

	return *this;
}
//============================================================================

void C3DAdjustPlane::mergeWithPlane (const C3DAdjustPlane& plane)
{
	*this += plane;
	this->adjust();
};

//============================================================================

double C3DAdjustPlane::adjust( EadjustMode Ie_mode )
{
	this->reduceSum();
	double retVal;
	switch (Ie_mode)
	{
		case eXYpara : retVal = adjustXY();
			  		   break;
		case eYZpara : retVal = adjustYZ();
			  		   break;
		case eZXpara : retVal = adjustZX();
			  		   break;
		default      : retVal = adjust3d();
			  		   break;
	}

	this->deReduceSum();

	this->planeConstant = this->normal.innerProduct(this->point);

	return retVal;
}

//============================================================================

double C3DAdjustPlane::adjustLocation(const C3DPoint &givenNormal)
{
	this->normal = givenNormal;
	this->normal /= this->normal.getNorm();
	this->reduceSum();

	if (this->nPoints < 1 || !this->isWellDefined())
	{
		this->sigma0     = -1.0;
		this->redundancy =  0.0;
		this->handleNoSolution();
	}
	else if (this->nPoints == 1)
	{
		this->sigma0     = 0.0;
	    this->redundancy = 0.0;
	}
	else
	{
		this->sigma0 =   this->normal.x * this->normal.x * this->squaredSumsXY.x
			           + this->normal.y * this->normal.y * this->squaredSumsXY.y
					   + this->normal.z * this->normal.z * this->squaredSumsZ.z
					   + 2.0 * this->normal.x * this->normal.y * this->squaredSumsXY.z
					   + 2.0 * this->normal.y * this->normal.z * this->squaredSumsZ.y
					   + 2.0 * this->normal.z * this->normal.x * this->squaredSumsZ.x ;

		this->redundancy = double(this->nPoints - 1);
		this->sigma0 = sqrt(this->sigma0 / this->redundancy);
	};

	this->deReduceSum();
	return this->sigma0;
}

//============================================================================

double C3DAdjustPlane::adjustXY()
{
	SymmetricMatrix N(2);     // matrix of equation system
	ColumnVector    l(2);     // right side of equ. sys.
	ColumnVector    x(2);     // vector of unknowns (components of plane, more specific: normal vector)

	N.element(0,0) = this->squaredSumsXY.x;  N.element(0,1) = this->squaredSumsXY.z;   l.element(0) = this->squaredSumsZ.x;
									         N.element(1,1) = this->squaredSumsXY.y;   l.element(1) = this->squaredSumsZ.y;
	try
	{
		x = N.i() * l;
	}
	catch(Exception& ex)
	{
		this->redundancy = 0.0;
		this->handleNoSolution();
		return this->sigma0;
	}

	this->normal.x = -x.element(0);
	this->normal.y = -x.element(1);
	this->normal.z = 1.0;
	this->normal /= this->normal.getNorm();

	if (this->nPoints == 3)
	{
		this->sigma0 = 0.0;
		this->redundancy = 0.0;
	}
	else
	{
		//The following line is replaced with the subsequent line due to a runtime memory problem with the function .AsScalar()
		//this->sigma0 = this->squaredSumsZ.z - (C_x.t()*(C_N*C_x)).AsScalar(); // must be >= 0.0
		this->sigma0 = this->squaredSumsZ.z - DotProduct(x, N*x); // must be >= 0.0
		this->redundancy = double(this->nPoints - 3);
		this->sigma0 = sqrt (this->sigma0 / this->redundancy);
	};

	return this->sigma0;
}

//============================================================================

double C3DAdjustPlane::adjustYZ()
{
	SymmetricMatrix N(2);     // matrix of equation system
	ColumnVector    l(2);     // right side of equ. sys.
	ColumnVector    x(2);     // vector of unknowns (components of plane, more specific: normal vector)

	N.element(0,0) = this->squaredSumsXY.y;  N.element(0,1) = this->squaredSumsZ.y;   l.element(0) = this->squaredSumsXY.z;
									         N.element(1,1) = this->squaredSumsZ.z;   l.element(1) = this->squaredSumsZ.x;
	try
	{
		x = N.i() * l;
	}
	catch(Exception& ex)
	{
		this->redundancy = 0.0;
		this->handleNoSolution();
		return this->sigma0;
	};

	this->normal.x =1.0;
	this->normal.y = -x.element(0);
	this->normal.z = -x.element(1);
	this->normal /= this->normal.getNorm();

	if (this->nPoints == 3)
	{
		this->sigma0 = 0.0;
		this->redundancy = 0.0;
	}
	else
	{
		//The following line is replaced with the subsequent line due to a runtime memory problem with the function .AsScalar()
		//this->sigma0 = this->squaredSumsXY.x - (C_x.t()*(C_N*C_x)).AsScalar(); // must be >= 0.0
		this->sigma0 = this->squaredSumsXY.x - DotProduct(x, N * x); // must be >= 0.0
		this->redundancy = double(this->nPoints - 3);
		this->sigma0 = sqrt (this->sigma0 / this->redundancy);
	};

	return this->sigma0;
}

//============================================================================

double C3DAdjustPlane::adjustZX()
{
	SymmetricMatrix N(2);     // matrix of equation system
	ColumnVector    l(2);     // right side of equ. sys.
	ColumnVector    x(2);     // vector of unknowns (components of plane, more specific: normal vector)

	N.element(0,0) = this->squaredSumsZ.z;  N.element(0,1) = this->squaredSumsZ.x;    l.element(0) = this->squaredSumsZ.y;
								            N.element(1,1) = this->squaredSumsXY.x;   l.element(1) = this->squaredSumsZ.x;
	try
	{
		x = N.i() * l;
	}
	catch(Exception& ex)
	{
		this->redundancy = 0.0;
		this->handleNoSolution();
		return this->sigma0;
	}

	this->normal.x = -x.element(1);
	this->normal.y =  1.0;
	this->normal.z = -x.element(0);
	this->normal /= this->normal.getNorm();

	if (this->nPoints == 3)
	{
		this->sigma0 = 0.0;
		this->redundancy = 0.0;
	}
	else
	{
		//The following line is replaced with the subsequent line due to a runtime memory problem with the function .AsScalar()
		//this->sigma0 = this->squaredSumsXY.y - (C_x.t()*(C_N*C_x)).AsScalar(); // must be >= 0.0
		this->sigma0 = this->squaredSumsXY.x - DotProduct(x, N * x); // must be >= 0.0
		this->redundancy = double(this->nPoints - 3);
		this->sigma0 = sqrt (this->sigma0 / this->redundancy);
	};

	return this->sigma0;
}

//============================================================================

double C3DAdjustPlane::adjust3d()
{
	SymmetricMatrix N(3);     // matrix of eigen value system
	DiagonalMatrix  val(3);   // eigen values

	N.element(0,0) = this->squaredSumsXY.x;  N.element(0,1) = this->squaredSumsXY.z;  N.element(0,2) = this->squaredSumsZ.x;
									         N.element(1,1) = this->squaredSumsXY.y;  N.element(1,2) = this->squaredSumsZ.y;
														 				              N.element(2,2) = this->squaredSumsZ.z;
	double determinantXY = this->squaredSumsXY.x * this->squaredSumsXY.y - this->squaredSumsXY.z * this->squaredSumsXY.z;
	// indicates collinearity of the points' projection to XY-plane if its value is 0

	try
	{
		Jacobi(N, val, this->eigenVectorRot);     // compute eigen values and vectors
			this->eigenValues = C3DPoint(val.element(0), val.element(1), val.element(2));
		this->sigma0 = val.element(0);

		this->normal = C3DPoint(this->eigenVectorRot.element(0, 0),
								this->eigenVectorRot.element(1, 0),
								this->eigenVectorRot.element(2, 0));

		if (this->nPoints == 3)
		{
			this->qxx        = 0.0;
			this->sigma0     = 0.0;
			this->redundancy = 0.0;
		}
		else
		{
			this->redundancy = double(this->nPoints - 3);

			if (this->sigma0 > 0)
			{
				this->sigma0 = sqrt(this->sigma0 / this->redundancy);


				if (determinantXY < FLT_EPSILON)
				{// indicates (quasi-)collinearity of the points' projection to XY-plane
					this->sigma0 = -this->sigma0;
				}
				else this->initQxx();
			};
		};
	}
	catch(Exception& ex)
	{
		this->handleNoSolution();
		this->qxx = 0.0;
		this->redundancy = 0.0;
	};


	return this->sigma0;
}

//============================================================================

void C3DAdjustPlane::initFromPoints(const CXYZPoint &P1, const CXYZPoint &P2,
									const CXYZPoint &P3, C3DPoint *pReduction)
{
	if (pReduction) this->reduction = *pReduction;
	else this->reduction.x = this->reduction.y = this->reduction.z = 0.0;

	this->resetSums(this->reduction);
	this->addPoint(P1);
	this->addPoint(P2);
	this->addPoint(P3);

	this->reduceSum();
	SymmetricMatrix N(3);     // matrix of eigen value system
	DiagonalMatrix  val(3);   // eigen values

	N.element(0,0) = this->squaredSumsXY.x;  N.element(0,1) = this->squaredSumsXY.z;  N.element(0,2) = this->squaredSumsZ.x;
									         N.element(1,1) = this->squaredSumsXY.y;  N.element(1,2) = this->squaredSumsZ.y;
														 				              N.element(2,2) = this->squaredSumsZ.z;
	try
	{
		Jacobi(N, val, this->eigenVectorRot);     // compute eigen values and vectors
		this->eigenValues = C3DPoint(val.element(0), val.element(1), val.element(2));
	}
	catch(Exception& ex)
	{
		this->handleNoSolution();
		this->qxx = 0.0;
		this->redundancy = 0.0;
	};

	this->deReduceSum();

	CXYZPoint p1(P1), p2 (P2), p3(P3);
	p1.x -= this->reduction.x; p1.y -= this->reduction.y; p1.z -= this->reduction.z;
	p2.x -= this->reduction.x; p2.y -= this->reduction.y; p2.z -= this->reduction.z;
	p3.x -= this->reduction.x; p3.y -= this->reduction.y; p3.z -= this->reduction.z;
	CXYZHomoSegment L(p1, p2);
	ColumnVector p3Vec = this->getVector3D(p3);
	Matrix gammaBar = L.getGammaBar();
	Matrix gammaBarTrans = gammaBar.t();
	Matrix piBar = this->getPiBar(p3);

	ColumnVector planeVec = gammaBarTrans * p3Vec;

	if (planeVec.element(2) < 0) planeVec = -planeVec;
	this->normal.x = planeVec.element(0);
	this->normal.y = planeVec.element(1);
	this->normal.z = planeVec.element(2);
	this->planeConstant = -planeVec.element(3);

	this->sigma0     = p1.getSigma(2);
	this->redundancy = 0;

	Matrix covPt = this->getQxxHomogeneous(P3);

	C3DPoint pr(0.0,0.0,0.0);
	Matrix Q = gammaBarTrans * covPt * gammaBar +  piBar.t() * L.getQxx(pr) * piBar;
	this->copyMatrixToSymmetricMatrix(Q, this->qxx);
	this->normalise();
};

//============================================================================

void C3DAdjustPlane::initQxx()
{
	double sigSq = this->sigma0 * this->sigma0;
	this->qxx = 0.0;
	this->qxx(2,2) = sigSq / this->eigenValues.y;
	this->qxx(3,3) = sigSq / this->eigenValues.z;
	this->qxx(4,4) = sigSq / double(this->nPoints);

	Matrix rotMat = this->getRot();
	Matrix mtx = rotMat * this->qxx * rotMat.t();

	this->copyMatrixToSymmetricMatrix (mtx, this->qxx);

	this->qxx = this->getQxx(-this->point);
};

//============================================================================

SymmetricMatrix C3DAdjustPlane::getQxx(C3DPoint p) const
{
	Matrix shiftMat(4,4);
	shiftMat = 0.0;
	shiftMat.element(0,0) = shiftMat.element(1,1) = shiftMat.element(2,2) = shiftMat.element(3,3) = 1.0;

	shiftMat.element(3,0) = p.x;
	shiftMat.element(3,1) = p.y;
	shiftMat.element(3,2) = p.z;

	SymmetricMatrix shiftQxx(4);

	Matrix mtx = shiftMat * this->qxx * shiftMat.t();

	this->copyMatrixToSymmetricMatrix (mtx, shiftQxx);

	return shiftQxx;
};

//============================================================================

Matrix C3DAdjustPlane::getPi(const C3DPoint &p)
{
	Matrix piMat(6,4);
	piMat = 0.0;
	piMat.SubMatrix(4,6,1,3) = getS(p);
	piMat.element(0,0) = 1.0; piMat.element(1,1) = 1.0; piMat.element(2,2) = 1.0;
	piMat.element(0,3) = -p.x; piMat.element(1,3) = -p.y; piMat.element(2,3) = -p.z;

	return piMat;
};

//============================================================================

Matrix C3DAdjustPlane::getPiBar(const C3DPoint &p)
{
	Matrix piBar(6,4);
	piBar = 0.0f;
	piBar.SubMatrix(1,3,1,3) = getS(p);

	piBar.element(3,0) = 1.0;  piBar.element(4,1) =  1.0; piBar.element(5,2) =  1.0;
	piBar.element(3,3) = -p.x; piBar.element(4,3) = -p.y; piBar.element(5,3) = -p.z;

	return piBar;
};

//============================================================================

Matrix C3DAdjustPlane::getS(const C3DPoint &p)
{
	Matrix S(3,3);

	S.element(0,0) =  0.0;  S.element(0,1) = -p.z; S.element(0,2) =  p.y;
	S.element(1,0) =  p.z;  S.element(1,1) =  0.0; S.element(1,2) = -p.x;
	S.element(2,0) = -p.y;  S.element(2,1) =  p.x; S.element(2,2) =  0.0;

	return S;
};

//============================================================================

Matrix C3DAdjustPlane::getQxxHomogeneous(const CXYZPoint &p)
{
	Matrix homogCovar(4,4);
	homogCovar = 0.0;

	for (int r = 0; r < 3; ++r)
	{
		for (int c = 0; c < 3; ++c)
		{
			homogCovar.element(r,c) = p.getCovarElement(r,c);
		}
	}

	return homogCovar;
};

//============================================================================

void C3DAdjustPlane::normalisePoint(CXYZPoint &p, double homogeneous, Matrix qxxHomog)
{
	homogeneous = 1.0 / homogeneous;
	p.x *= homogeneous;   p.y *= homogeneous;  p.z *= homogeneous;
	Matrix jacobian(4,4);
	jacobian = 0.0f;
	jacobian.element(0,0) = jacobian.element(1,1) = jacobian.element(2,2) = homogeneous;
	jacobian.element(0,3) = -p.x * homogeneous;
	jacobian.element(1,3) = -p.y * homogeneous;
	jacobian.element(2,3) = -p.z * homogeneous;

	jacobian = jacobian * qxxHomog * jacobian.t();
	Matrix *qxxP = p.getCovariance();
	for (int r = 0; r < 3; ++r)
	{
		for (int c = 0; c < 3; ++c)
		{
			qxxP->element(r,c) = jacobian.element(r,c);
		}
	}
};

//============================================================================

Matrix C3DAdjustPlane::getJacobi() const
{
	Matrix homogMat(3,1);
	double normSqu = this->normal.getSquareNorm();
	double norm    = sqrt(normSqu);

	homogMat.element(0,0) = this->normal.x;
	homogMat.element(1,0) = this->normal.y;
	homogMat.element(2,0) = this->normal.z;
	Matrix jacobiAux = - homogMat * homogMat.t() /  normSqu;
	jacobiAux.element(0,0) += 1.0;
	jacobiAux.element(1,1) += 1.0;
	jacobiAux.element(2,2) += 1.0;

	homogMat = -homogMat * this->planeConstant / normSqu ;
	Matrix jacobi(4,4);
	jacobi.element(0,0) = jacobiAux.element(0,0); jacobi.element(0,1) = jacobiAux.element(0,1); jacobi.element(0,2) = jacobiAux.element(0,2); jacobi.element(0,3) = 0.0;
	jacobi.element(1,0) = jacobiAux.element(1,0); jacobi.element(1,1) = jacobiAux.element(1,1); jacobi.element(1,2) = jacobiAux.element(1,2); jacobi.element(1,3) = 0.0;
	jacobi.element(2,0) = jacobiAux.element(2,0); jacobi.element(2,1) = jacobiAux.element(2,1); jacobi.element(2,2) = jacobiAux.element(2,2); jacobi.element(2,3) = 0.0;
	jacobi.element(3,0) =  homogMat.element(0,0); jacobi.element(3,1) =  homogMat.element(1,0); jacobi.element(3,2) =  homogMat.element(2,0); jacobi.element(3,3) = 1.0;

	return jacobi * (1.0 / norm);
};

//============================================================================

Matrix C3DAdjustPlane::getPi() const
{
	Matrix piMat(6,4);
	piMat = 0.0;
	piMat.element(0,0) = -this->planeConstant;
	piMat.element(0,3) = -this->normal.x;
	piMat.element(1,1) = -this->planeConstant;
	piMat.element(1,3) = -this->normal.y;
	piMat.element(2,2) = -this->planeConstant;
	piMat.element(2,3) = -this->normal.z;
	piMat.element(3,1) = -this->normal.z;
	piMat.element(3,2) =  this->normal.y;
	piMat.element(4,0) =  this->normal.z;
	piMat.element(4,2) = -this->normal.x;
	piMat.element(5,0) = -this->normal.y;
	piMat.element(5,1) =  this->normal.x;

	return piMat;
};

//============================================================================

Matrix C3DAdjustPlane::getPiBar() const
{
	Matrix C_piBar_(6,4);
	C_piBar_ = 0.0f;

								  C_piBar_(1,2) = -this->normal.z; C_piBar_(1,3) =  this->normal.y;
	C_piBar_(2,1) =  this->normal.z;                                 C_piBar_(2,3) = -this->normal.x;
	C_piBar_(3,1) = -this->normal.y; C_piBar_(3,2) =  this->normal.x;

	C_piBar_(4,1) = C_piBar_(5,2) = C_piBar_(6,3) = -this->planeConstant;
	C_piBar_(4,4) = -this->normal.x;
	C_piBar_(5,4) = -this->normal.y;
	C_piBar_(6,4) = -this->normal.z;

	return C_piBar_;
};

//============================================================================

Matrix C3DAdjustPlane::getSNormal() const
{
	Matrix SNormal(3,3);
	SNormal.element(0,0) =             0.0;  SNormal.element(0,1) = -this->normal.z;  SNormal.element(0,2) =  this->normal.y;
	SNormal.element(1,0) =  this->normal.z;  SNormal.element(1,1) =             0.0;  SNormal.element(1,2) = -this->normal.x;
	SNormal.element(2,0) = -this->normal.y;  SNormal.element(2,1) =  this->normal.x;  SNormal.element(2,2) = 0.0;
	return SNormal;
};

//============================================================================

ColumnVector C3DAdjustPlane::getAsVector() const
{
	ColumnVector C_vec_(4);
	C_vec_(1) = this->normal.x;
	C_vec_(2) = this->normal.y;
	C_vec_(3) = this->normal.z;
	C_vec_(4) = -this->planeConstant;
	return C_vec_;
};

//============================================================================

void C3DAdjustPlane::getReducedVector(const Matrix &u1, const Matrix &u2,
                                        const ColumnVector &entity1,
                                        const ColumnVector &entity2,
                                        const Matrix &qxx1, const Matrix &qxx2,
                                        int rank, ColumnVector &dist, Matrix &qdd)
{
	vector<unsigned long>  idxVec(u1.Nrows());
	vector<double> dstVec(u1.Nrows());

	for (unsigned long i = 0; i < idxVec.size(); ++i)
	{
		idxVec[i] = i;
		dstVec[i] = u1.Row(i + 1).SumSquare() * u2.Row(i + 1).SumSquare();
	};

	for (unsigned long i = 0; i < idxVec.size(); ++i)
	{
		double maxVec = dstVec[i];
		unsigned long  maxInd = i;

		for (unsigned long j = i + 1; j < idxVec.size(); ++j)
		{
			if (dstVec[j] > maxVec)
			{
				maxInd = j;
				maxVec = dstVec[j];
			};
		};

		if (maxInd != i)
		{
			double dmyV = dstVec[i];
			dstVec[i] = dstVec[maxInd];
			dstVec[maxInd] = dmyV;
			unsigned long dmyI = idxVec[i];
			idxVec[i] = idxVec[maxInd];
			idxVec[maxInd] = dmyI;
		};
	};


	Matrix u1Red(rank,u1.Ncols());
	Matrix u2Red(rank,u2.Ncols());

	for (int i = 0; i < rank; ++i)
	{
		u1Red.Row(i + 1) = u1.Row(idxVec[i] + 1);
		u2Red.Row(i + 1) = u2.Row(idxVec[i] + 1);
	};

	qdd  = u1Red * qxx2 * u1Red.t() + u2Red * qxx1 * u2Red.t();
	dist = u1Red * entity2;
};

//============================================================================

double C3DAdjustPlane::testMetric(const Matrix &u1, const Matrix &u2,
								  const ColumnVector &entity1,
								  const ColumnVector &entity2,
								  const Matrix &qxx1, const Matrix &qxx2,
								  int rank)
{
	ColumnVector dist(rank);
	Matrix qdd(rank,rank);
	getReducedVector(u1, u2, entity1, entity2, qxx1, qxx2, rank, dist, qdd);

	Matrix d = dist.t() * qdd.i() * dist;
	return d.element(0,0);
};

//============================================================================

void C3DAdjustPlane::normalise()
{
	Matrix jacobi = getJacobi();
	double norm   = this->normal.getNorm();
	this->normal /= norm;
	this->planeConstant = this->normal.innerProduct(this->point);
	Matrix mtx = jacobi * this->qxx * jacobi.t();

	this->copyMatrixToSymmetricMatrix (mtx, this->qxx);
};

//============================================================================

C3DPoint C3DAdjustPlane::getBasePoint(const C3DPoint &p) const
{
	return C3DPlane::getBasePoint(p - this->reduction);
};

//============================================================================

double C3DAdjustPlane::getOrientedDistance (const C3DPoint &p) const
{
	return C3DPlane::getOrientedDistance(p - this->reduction);
};

//============================================================================

double C3DAdjustPlane::getSigmaDist(const C3DPoint &p)  const
{
	ColumnVector P(4);
	P.element(0) = p.x - this->reduction.x;
	P.element(1) = p.y - this->reduction.y;
	P.element(2) = p.z - this->reduction.z;
	P.element(3) = 1.0;

	Matrix m = P.t() * this->qxx * P;

	return sqrt(m.element(0,0));
};

//============================================================================

double C3DAdjustPlane::getHeight(double x, double y) const
{
	double d = (x - this->reduction.x) * this->normal.x +
		       (y - this->reduction.y) * this->normal.y;
	return this->reduction.z + (this->planeConstant - d) / this->normal.z;
};

//============================================================================

double C3DAdjustPlane::getHeight(const C2DPoint &p) const
{
	return this->getHeight(p.x, p.y);
};

//============================================================================

Matrix C3DAdjustPlane::getRot() const
{
	Matrix C_rotMat_(4,4);
	C_rotMat_(1,1) = eigenVectorRot(1,1); C_rotMat_(1,2) = eigenVectorRot(1,2); C_rotMat_(1,3) = eigenVectorRot(1,3); C_rotMat_(1,4) = 0.0f;
	C_rotMat_(2,1) = eigenVectorRot(2,1); C_rotMat_(2,2) = eigenVectorRot(2,2); C_rotMat_(2,3) = eigenVectorRot(2,3); C_rotMat_(2,4) = 0.0f;
	C_rotMat_(3,1) = eigenVectorRot(3,1); C_rotMat_(3,2) = eigenVectorRot(3,2); C_rotMat_(3,3) = eigenVectorRot(3,3); C_rotMat_(3,4) = 0.0f;
	C_rotMat_(4,1) = 0.0f;                   C_rotMat_(4,2) = 0.0f;                   C_rotMat_(4,3) = 0.0f;                   C_rotMat_(4,4) = 1.0f;

	return C_rotMat_;
};

//============================================================================

double C3DAdjustPlane::getCollinearityMeasure() const
{
	double collMeas(-1.0);
	if (this->eigenValues.z > 0.0)
	{
		collMeas = (this->eigenValues.z - this->eigenValues.y) / (this->eigenValues.z + this->eigenValues.y);
	}

	return collMeas;
};

//============================================================================

float C3DAdjustPlane::isCoplanar(const C3DAdjustPlane &plane, float &combinedS0,
								 double sigmaMin) const
{
	if ((this->nPoints <= 2)         || (plane.nPoints <= 2))         return -10.0f;
//	if ((this->sigma0 < FLT_EPSILON) || (plane.sigma0 < FLT_EPSILON)) return -10.0f;

	C3DAdjustPlane commonPlane(*this);

	commonPlane += plane;

	if (commonPlane.nPoints < 4) return -10.0f;

	combinedS0 = (float) commonPlane.adjust();

	if (combinedS0 < FLT_EPSILON) return -10.0f;

	double redIndivd = this->redundancy + plane.redundancy;

	if (redIndivd < FLT_EPSILON)
	{
		if (combinedS0 > sigmaMin) return -1.0f;
		else return 1.0f;
	}

	double rmsCommon = commonPlane.getVtPv() / commonPlane.redundancy;
	double rmsIndivd = this->getVtPv() + plane.getVtPv();

	rmsIndivd /= redIndivd;

	double fisherStatistics = rmsCommon / rmsIndivd;

	double quantil = statistics::fisherFractil95(long (commonPlane.redundancy), long (redIndivd));

    fisherStatistics /= quantil;

	if ((fisherStatistics > 1.0f) && (combinedS0 > sigmaMin)) fisherStatistics = -fisherStatistics;

	return (float) fisherStatistics;
};

//============================================================================

float C3DAdjustPlane::isIdentical(const C3DAdjustPlane &plane) const
{
	double metric = testMetric(this->getPiBar(),    plane.getPiBar(),
		                       this->getAsVector(), plane.getAsVector(),
							   this->qxx,           plane.qxx, 3);

	metric /= C3DAdjustPlane::getChiSquareQuantil(3);
	if (metric > 1.0f) metric = -metric;
	return float(metric);
};

//============================================================================

float C3DAdjustPlane::isParallel(const C3DAdjustPlane &plane) const
{
	ColumnVector vec1(3), vec2(3);
	SymmetricMatrix qxxN1(3), qxxN2(3);

	ColumnVector asVec1 = this->getAsVector();
	ColumnVector asVec2 = plane.getAsVector();

	for (int i = 0; i < 3; ++i)
	{
		vec1.element(i) = asVec1.element(i);
		vec2.element(i) = asVec2.element(i);
		for (int j = i; j < 3; ++j)
		{
			qxxN1.element(i,j) = this->qxx.element(i,j);
			qxxN2.element(i,j) = plane.qxx.element(i,j);
		}
	}


	double metric = testMetric(this->getSNormal(), plane.getSNormal(), vec1, vec2, qxxN1, qxxN2, 2);

	metric /= C3DAdjustPlane::getChiSquareQuantil(3);
	if (metric > 1.0) metric = -metric;
	return (float) metric;
};

//============================================================================

float C3DAdjustPlane::isIncident(const CXYZPoint &p) const
{
	ColumnVector normVec(3);
	normVec.element(0) = this->normal.x;
	normVec.element(1) = this->normal.y;
	normVec.element(2) = this->normal.z;
	ColumnVector pVec = this->getVector3D(p);
	C3DPoint pRed(p.x - this->reduction.x, p.y - this->reduction.y, p.z - this->reduction.z);
	pVec.element(0) = pRed.x;
	pVec.element(1) = pRed.y;
	pVec.element(2) = pRed.z;
	pVec.element(3) = 1.0;

	double metric = this->getDistance(p);
	Matrix qdd = pVec.t()    * this->qxx    * pVec +
		         normVec.t() * p.getCovar() * normVec;

	metric /= sqrt(qdd.element(0,0));
	metric /= quantil;
	if (metric > 1.0f) metric = -metric;
	return float(metric);
};

//============================================================================

ColumnVector C3DAdjustPlane::getVector3D(const C3DPoint &p)
{
	ColumnVector pVec(4);
	pVec.element(0) = p.x;
	pVec.element(1) = p.y;
	pVec.element(2) = p.z;
	pVec.element(3) = 1.0;
	return pVec;
};

//============================================================================

ColumnVector C3DAdjustPlane::getVector2D(const C3DPoint &p)
{
	ColumnVector pVec(3);
	pVec.element(0) = p.x;
	pVec.element(1) = p.y;
	pVec.element(2) = 1.0;
	return pVec;
};

//============================================================================

float C3DAdjustPlane::areIdenticalPoints3D(const CXYZPoint &p1, const CXYZPoint &p2)
{
	SymmetricMatrix qxx1(4), qxx2(4);
	qxx1.element(0,0) = p1.getCovar().element(0,0); qxx1.element(0,1) = p1.getCovar().element(0,1); qxx1.element(0,2) = p1.getCovar().element(0,2); qxx1.element(0,3) = 0.0;
								                    qxx1.element(1,1) = p1.getCovar().element(1,1); qxx1.element(1,2) = p1.getCovar().element(1,2); qxx1.element(1,3) = 0.0;
															                                        qxx1.element(2,2) = p1.getCovar().element(2,2); qxx1.element(2,3) = 0.0;
																							                                                        qxx1.element(3,3) = 0.0;
	qxx2.element(0,0) = p2.getCovar().element(0,0); qxx2.element(0,1) = p2.getCovar().element(0,1); qxx2.element(0,2) = p2.getCovar().element(0,2); qxx2.element(0,3) = 0.0;
								                    qxx2.element(1,1) = p2.getCovar().element(1,1); qxx2.element(1,2) = p2.getCovar().element(1,2); qxx2.element(1,3) = 0.0;
															                                        qxx2.element(2,2) = p2.getCovar().element(2,2); qxx2.element(2,3) = 0.0;
																							                                                        qxx2.element(3,3) = 0.0;

	double metric = testMetric(getPi(p1), getPi(p2), getVector3D(p1), getVector3D(p2), qxx1, qxx2, 3);

	metric /= C3DAdjustPlane::getChiSquareQuantil(3);
	if (metric > 1.0) metric = -metric;
	return float(metric);
};

//============================================================================

float C3DAdjustPlane::areIdenticalPoints2D(CXYZPoint p1,  CXYZPoint p2)
{
	p1.z = 1.0;
	p2.z = 1.0;
	SymmetricMatrix qxx1(3), qxx2(3);
	qxx1.element(0,0)  = p1.getCovar().element(0,0); qxx1.element(0,1) = p1.getCovar().element(0,1); qxx1.element(0,2) = 0.0;
								                     qxx1.element(1,1) = p1.getCovar().element(1,1); qxx1.element(1,2) = 0.0;
															                                         qxx1.element(2,2) = 0.0;
	qxx2.element(0,0)  = p2.getCovar().element(0,0); qxx2.element(0,1) = p2.getCovar().element(0,1); qxx2.element(0,2) = 0.0;
								                     qxx2.element(1,1) = p2.getCovar().element(1,1); qxx2.element(1,2) = 0.0;
															                                         qxx2.element(2,2) = 0.0;

	double metric = testMetric(getS(p1), getS(p2), getVector2D(p1), getVector2D(p2), qxx1, qxx2, 2);

	metric /= C3DAdjustPlane::getChiSquareQuantil(2);
	if (metric > 1.0) metric = -metric;
	return float(metric);
};

//============================================================================

CCharString C3DAdjustPlane::getDumpString() const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);

	oss << "\nPlane: Normal:     " << this->normal.getString(" ", 0, 7)
		<< "\n       COG:        " << this->point.getString(" ", 0, 7);
	oss.precision(3);
	oss << "\n       Euclidean:  " << this->planeConstant
		<< "\n       Sigma0:     " << this->sigma0;
	oss << ends;

	char *z = oss.str();

	CCharString str(z);
	delete[] z;

	return str;
}

//============================================================================

ostream& operator<< (ostream &s, const C3DAdjustPlane &plane)
{
	s << plane.squaredSumsXY.getString(" ", 15, 3) << "  " << plane.squaredSumsZ.getString(" ",15,3)
	  << plane.sums.getString(" ",15,3)
	  << plane.sumW  << "  " << plane.nPoints  << endl;
	return s;
}

//============================================================================
