/**
 * @class CBuildingArray
 * an array of Buildings 
 */
 #ifdef WIN32
#include "vraniml.h"

#include "BuildingArray.h"
#include "Spheroid.h"
#include "XYZPolyLine.h"
#include "XYZPolyLineArray.h"

CBuildingArray::CBuildingArray(int nGrowBy) : 
	CMUObjectArray<CBuilding, CBuildingPtrArray>(nGrowBy)
{
    this->FileName = "Buildings";
}

CBuildingArray::~CBuildingArray(void)
{
}



CFileName* CBuildingArray::getFileName()
{
    return &this->FileName;
}

void CBuildingArray::setFileName(const char* filename)
{
    this->FileName = filename;
}

CBuilding* CBuildingArray::Add()
{
    CBuilding* newbuilding = CMUObjectArray<CBuilding, CBuildingPtrArray>::Add();

	this->informListeners_elementAdded();


    return newbuilding;
}

CBuilding* CBuildingArray::Add(const CBuilding& newElement)
{
    CBuilding* newbuilding = CMUObjectArray<CBuilding, CBuildingPtrArray>::Add(newElement);

	this->informListeners_elementAdded();

    return newbuilding;
}


void CBuildingArray::Remove(CBuilding* pElement)
{
	this->informListeners_elementDeleted(pElement->getLabel());


	CMUObjectArray<CBuilding, CBuildingPtrArray>::Remove(pElement);

}


void CBuildingArray::RemoveAt(int nIndex, int nCount)
{
	vector<CLabel> labels(nCount);

	for (int k = 0; k < nCount; k++)
	{
		CLabel label = this->GetAt(nIndex + k)->getLabel();
		labels.push_back(label);

	}

	CMUObjectArray<CBuilding, CBuildingPtrArray>::RemoveAt(nIndex,nCount);

	for (int k = 0; k < labels.size(); k++)
	{
		this->informListeners_elementDeleted(labels[k]);
	}

}

void CBuildingArray::RemoveAll()
{
    this->informListeners_elementDeleted();

	CMUObjectArray<CBuilding, CBuildingPtrArray>::RemoveAll();


}




/**
 * writes BuildingArray to a TextFile
 * @param cfilename type
 * @returns true/false
 */
bool CBuildingArray::writeBuildingTextFile(CFileName cfilename)
{
    FILE	*fp;
	fp = fopen( cfilename.GetChar(),"w");

	fprintf(fp,"Number of Buildings: %d\n", this->GetSize());
	
	CCharString str = this->getReferenceSystem()->getCoordinateSystemName();
	fprintf(fp,"Coordinate system: %s\n\n", str.GetChar());
	
	// Create buildings
	for (int i = 0; i < this->GetSize(); i++)
	{
		CBuilding* building = this->GetAt(i);
		building->writeTextFile(fp);
	}

	fclose(fp);

    return true; 
}
	
C3DPoint CBuildingArray::getCentre() const
{
	C3DPoint centre;
	
	centre.x = centre.y = centre.z = 0;
	int pointcount = 0;

	for (int i = 0; i < this->GetSize(); i++)
	{
		CBuilding* building = this->GetAt(i);
		
		C2DPoint bldCtr = building->getCentre();
		int bldPntCnt = building->getPointCount();
		centre.x += bldCtr.x * bldPntCnt;
		centre.y += bldCtr.y * bldPntCnt;
		pointcount += bldPntCnt;
	}
	
	centre /= (double) pointcount;

	return centre;
};

/**
 * writes BuildingArray to a VRMLFile, with
 * @param cfilename
 * @returns true/false
 */
bool CBuildingArray::writeBuildingVRMLFile(CFileName cfilename, int version)
{
	CXYZPoint reduction = this->getCentre();
	CReferenceSystem *refTrafo = 0;

	if (this->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)
	{
		refTrafo = this->getReferenceSystem();
		refTrafo->setUTMZone(reduction.getX(), reduction.getY());
		refTrafo->geographicToGrid(reduction, reduction);
	}

	if (version == 1) return writeBuildingVRML1File(cfilename, reduction, refTrafo);
	
	return writeBuildingVRML2File(cfilename, reduction, refTrafo);
}

bool CBuildingArray::writeBuildingVRML1File(CFileName cfilename, const C3DPoint &reduction, CReferenceSystem *refsys)
{
    FILE	*fp;
	fp = fopen( cfilename.GetChar(),"w");

	
 	fprintf(fp,"#VRML V1.0 ascii\n\n");


	fprintf(fp,"#	Coordinate system: %s\n", this->getReferenceSystem()->getCoordinateSystemName().GetChar());
	fprintf(fp,"#	Shift for all coordinate values:\n");
	fprintf(fp,"#		X-Shift: %10.2lf\n", reduction.x);
	fprintf(fp,"#		Y-Shift: %10.2lf\n", reduction.y);
	fprintf(fp,"#		Z-Shift: %10.2lf\n", reduction.z);

	fprintf(fp,"  Material \n");
	fprintf(fp,"  {\n");
	fprintf(fp,"      diffuseColor 1.0 0.0 0.0\n");
	fprintf(fp,"  }\n");
	fprintf(fp," \n");

	// Create buildings
	for (int i = 0; i < this->GetSize(); i++)
	{
		CBuilding* building = this->GetAt(i);
		building->writeVRML1File(fp, reduction, refsys);
	}

	fclose(fp);

    return true;
}

bool CBuildingArray::writeBuildingVRML2File(CFileName cfilename, const C3DPoint &reduction, CReferenceSystem *refsys)
{
	vrBrowser Browser;

	for (int i = 0; i < this->GetSize(); i++)
	{
		CBuilding* building = this->GetAt(i);
		building->writeVRML2File(Browser, reduction, refsys);
	}

 	vrWriteTraverser vrWT;
	vrWT.SetTabStop(2, ' ');      // Change the tab character and the number of characters per indent
	vrWT.SetDigits(3);            // Change the number of digits to write for SFFloat fields
	vrWT.SetVerbose(0);
	
	ostrstream oss;
	oss << "#	Coordinate system: " << this->getReferenceSystem()->getCoordinateSystemName().GetChar() 
		<< "\n" << ends;
	
	char *z = oss.str();
	vrWT.setCoordSysDescriptor(z);
	delete [] z;

	
	ostrstream oss1; 
	oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(3);
	oss1 << "#	Shift for all coordinate values:\n#		X-Shift: ";
	oss1.width(15);
	oss1 << reduction.x << "\n#		Y-Shift: ";
	oss1.width(15);
	oss1 << reduction.y << "\n#		Z-Shift: ";
	oss1.width(15);
	oss1 << reduction.z << "\n\nBackground\n {"
		 << "\n  skyAngle    [ 1.2, 1.5 ]"
		 << "\n  skyColor    [ 0.5 0.7  1, 0.5 0.7  1, 0.5 0.7  1 ]"
		 << "\n  groundAngle [1.57]"
         << "\n  groundColor [ 0.5 0.7  1, 0.5 0.7  1]\n }\n" << ends;

	z = oss1.str();
	vrWT.setReductionDescriptor(z);
	delete [] z;

	vrWT.SetFilename(cfilename.GetChar()); // Set the output filename

	Browser.Traverse(&vrWT);

	return true;
}

CBuilding* CBuildingArray::getBuilding(int i)
{
    return this->GetAt(i);
}

/** SLOW FOR LOTS OF Buildings!
 * Gets a building by label (null if not found)
 *
 * @param CCharString Label of building
 */
CBuilding* CBuildingArray::getBuilding(CCharString Label)
{
    CBuilding* testbuilding = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        testbuilding = this->GetAt(i);

        if (Label.CompareNoCase(testbuilding->getLabel().GetChar()))
            return testbuilding;

        continue;
    }

    return NULL;
}


/**
 * Serialized storage of a CBuildingArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CBuildingArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("Building filename", this->FileName.GetChar());

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->getReferenceSystem()->serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->getDisplaySettings()->serializeStore(child);

    for (int i = 0; i < this->GetSize(); i++)
    {
        CBuilding* building = this->GetAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        building->serializeStore(newSection);
    }

}

/**
 * Restores a CBuildingArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CBuildingArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("Building filename"))
            this->FileName = token->getValue();
    }

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();
    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);

        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refSystem.serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CDisplaySettings"))
        {
			this->displaySettings.serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CBuilding"))
        {
            CBuilding* building = this->Add();
            building->serializeRestore(child);
        }
    }
}

/**
 * reconnects pointers
 */
void CBuildingArray::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CBuildingArray::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Flag if CBuildingArray is modified
 * returns bool
 */
bool CBuildingArray::isModified()
{
    for (int i = 0; i < this->GetSize(); i++)
    {
        if (this->GetAt(i)->isModified())
            return true;
    }

    return false;
}

CFileName CBuildingArray::getItemText()
{
    CFileName itemtext = this->FileName.GetFullFileName();
    return itemtext.GetChar();
}


/**
 * writes BuildingArray to a DXF-File
 * @param cfilename type
 * @returns true/false
 */
bool CBuildingArray::writeBuildingDXFFile(CFileName cfilename, bool relativeHeights)
{
	ofstream dxf(cfilename.GetChar());

	bool ret = CXYZPolyLine::writeDXFHeader(dxf);
	int colour = 1;
	int decimals = 3;

	CXYZPolyLineArray loops;

	if (ret)
	{
		// Create lines
		for (int i = 0; i < this->GetSize(); i++)
		{
			CBuilding* building = this->GetAt(i);
			CCharString blab = building->getLabel().GetAlpha();
			const char *layer = blab.GetChar();

			building->getAllLoops(loops);
			double FloorHeight = building->getPmin().z;

			for (int j = 0; j < loops.GetSize(); ++j)
			{
				CXYZPolyLine* line = loops.getAt(j);
				if (relativeHeights)
				{
					for (int k = 0; k < line->getPointCount(); ++k)
					{
						line->getPoint(k)->z -= FloorHeight;
					}
				}
				ret &= line->writeDXF(dxf, layer, colour, decimals);
			}
		}
		ret &= CXYZPolyLine::writeDXFTrailer(dxf);
	}

    return ret; 
}

void CBuildingArray::selectElement(CLabel& element,bool select,bool forwardIndex)
{
	// save the selection
	CBuilding *b = this->getBuilding(element.GetChar());
	int index = -1;
	if (b)
	{
		b->setSelected(select);
		if (forwardIndex)
			index = this->indexOf(b);
	}
	

	this->informListeners_elementSelected(select,element,index);


}


void CBuildingArray::unselectAll(bool forwardIndex)
{
	
	
	for (int k=0; k < this->GetSize(); k++)
	{
		CBuilding* b = this->GetAt(k);
		if (b && b->getIsSelected())
			this->selectElement(b->getLabel(),false,forwardIndex);
	}
}

#endif // WIN32