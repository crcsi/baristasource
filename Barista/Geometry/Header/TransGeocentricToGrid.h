#ifndef __CTransGeocentricToGrid__
#define __CTransGeocentricToGrid__

#include "Trans3D.h"

class CTransGeocentricToGrid : public CTrans3D
{
  public:

    CTransGeocentricToGrid(const CReferenceSystem &sysFrom, const CReferenceSystem &sysTo);

	~CTransGeocentricToGrid();

    virtual void transform(C3DPoint  &out, const C3DPoint  &in);

    virtual void transform(CXYZPoint &out, const CXYZPoint &in);

	virtual void getDerivatives(Matrix &derivatives, const C3DPoint &p) const;

    bool isa(const char* className) const
    {
        if (::strcmp("CTransGeocentricToGrid", className) == 0)
            return true;

        return false;
    }

	 string getClassName() const
	 {
		  return string("CTransGeocentricToGrid");
	 };
};
#endif
