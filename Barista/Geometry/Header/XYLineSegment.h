#ifndef __CXYLineSegment__
#define __CXYLineSegment__

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */
#include "XYPoint.h"
#include "2DPoint.h"
#include "newmat.h"
#include "XYLine.h"

#include "Serializable.h"
class CXYContour;
class C3DAdjustPlane;

//*****************************************************************************
/*!
 * \brief
 * A 2D segment with a homogeneous representation of the straight line, 
 * variance-covariance matrices of both the homogeneous line parameters and 
 * the end points, and methods for statistical geometric reasoning.
 */
//*****************************************************************************

class CXYLineSegment : public CXYLine, public CSerializable
{
//=============================================================================
//===================================  data ===================================

  protected:

	  //!the line segment's start point    
	  CXYPoint startPt;

	  //! the line segment's end point
	  CXYPoint endPt;


//=============================================================================

  public:

  
	  CXYLineSegment();

	  CXYLineSegment(const CXYPoint &I_start, const CXYPoint &I_end);

	  CXYLineSegment(const CXYLineSegment &I_segment);

	  virtual ~CXYLineSegment();

//=============================================================================
	
	  string getClassName() const { return string("CXYLineSegment"); };

	  bool isa (string& className) const
	  {
		  if (className == "CXYLineSegment") return true;
		  return false;
	  };

//=============================================================================

	  CXYPoint getStartPt() const { return startPt; }; 

	  CXYPoint getEndPt() const  { return endPt; };

	  double length() const;
     
	  //! construct a vertical plane through *this;  
	  //! z0 will be the height of the plane's central point 
	  C3DAdjustPlane getVerticalPlane(double z0) const;

	  bool equals (const CXYLineSegment &segment) const;

	  //! distance functions
	  double distanceFromSegment (const C2DPoint &point) const;

	  double distanceFromSegment(const CXYLineSegment &segment ) const;

	  /// calculate point in distance dist from start point
	  C2DPoint getPointOnLine( double dist) const;

	  /// calculate point in distance percent * length from start point
	  C2DPoint getPointOnLinePercent( double percent ) const;

	  /// get Distance of the projection of p from the start point
	  double getDistanceofBasePointFromStart(const C2DPoint &p) const;

//=============================================================================
	
	  //! distance measure between the two segments this and _segment, to be 
	  //! used for matching.
	  //! segment is projected to *this, which results in an overlap ratio between
	  //! the projection and *this. This overlap ratio (between 0 and 1) will be
	  //! returned in overlap. 
	  //!
	  //! return value: the smallest distance between any end point of the two 
	  //!               segments if overlap == 0
	  //!               the average distance between segment and
	  //!               the projection of segment to *this otherwise
	  double getMatchDist(const CXYLineSegment &segment, double &overlap) const;

   
	  float isIdenticalAndOverlaps(const CXYLineSegment &line, double &overlap) const;

//=============================================================================
   
	  void set(const CXYPoint &I_start, const CXYPoint &I_end);


	  /// reverse segment
	  virtual void reverse();


	  /// Initialise segment from polygon part 
	  virtual void initFromContour(CXYContour *contour, unsigned long indexFrom,
	                               unsigned long indexTo, double I_sigma0);

// The segment will be computed as an adjusting 2D straight line from 
// contour[indexFrom] to contour[indexTo]. The end points will be
// the projections of contour[indexFrom] and contour[indexTo] to the adjusting
// straight line. If I_sigma0 > 0, I_sigma0 will be used as an a priori
// rms error of the weight unit (if the latter is considered to be too
// optimistic)
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 
	  /// cut off segment
 
	  virtual void cutOff(const CXYPoint &point1, const CXYPoint &point2);

// The straight line will remain as it is. The segment will be cut off at
// the projections of point1 and point2 to that straight line.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// project *this to segment 
	  bool project(const CXYLineSegment &segment);


//=============================================================================

	  //!Formatted output of the segment parameters to a string 
	  CCharString getPrintString(const CCharString &heading, double dx, double dy, 
		                         int width, int precision) const;

	  //!Formatted output of the segment parameters to an ostream object
	  void print(ostream &ostr, double dx, double dy, int width, int precision,  
	             const CCharString &heading) const;


//=============================================================================

      //! Auxiliary method for converting the homogeneous 2D point 
      //! (p.x, p.y, homogeneous)^t with its covariance matrix into its 
	  //! Euclidean representation.
	  //! The co-ordinates and the covariance matrix of p are changed. 
	  static void normalisePoint(CXYPoint &p, double homogeneous);


//=============================================================================
// CSerializable functions
 	  void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	  void reconnectPointers();
	  void resetPointers();
	  bool isModified();

//=============================================================================

	//!dxf output of the segment as Polyline
  	bool writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
		          double xfactor = 1.0, double yfactor = 1.0) const;

//=============================================================================
   
 protected:

//=============================================================================

	 //! Initialise covariance matrix from the covariance matrices of the end points
	 virtual void initCovarFromPoints();
	 
	 //! Initialise covariance matrix from the results of the adjustment 
	 //! If I_sigma0 > 0, I_sigma0 will be used as an a priori rms error
	 //! of the weight unit (if the latter is considered to be too optimistic)
	 virtual void initCovarFromAdjustment(double I_sigma0);

};

#endif



