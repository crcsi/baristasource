#ifndef __CTrans2DConformal__
#define __CTrans2DConformal__


#include "XYPoint.h"
#include "XYZPoint.h"
#include "Trans2D.h"

class CTrans2DConformal : public CTrans2D
{
  protected:

	double a1, a2, a3, a4;
	C2DPoint P0;
  public:

	  CTrans2DConformal();
	  CTrans2DConformal(const CTrans2DConformal &trans);

	  CTrans2DConformal(const Matrix&parms);

	  ~CTrans2DConformal();

	  virtual int nParameters() const { return 4; };

	  virtual void getParameter(vector<double> &parameter) const;

   	  virtual CTrans2D *clone() const { return new CTrans2DConformal(*this); };

	  virtual CCharString getDescriptor(const CCharString &lead, int shiftDigits, int matrixDigits, 
		                                const CCharString &descrFrom = "_1", 
										const CCharString &descrTo = "_2") const;

	  virtual CTrans2DConformal &operator = (const CTrans2DConformal &trans);

	  virtual C2DPoint getP0() const { return this->P0; };

	  virtual void setP0(const C2DPoint &p0) {this->P0 = p0;};

	  virtual void applyP0();

	  virtual Matrix getProjectionMatrix() const;

   	  virtual void transform (CXYZPoint &out, const CXYPoint &in) const;

	  virtual void transform (CXYPoint &out, const CXYPoint &in) const;

	  virtual void transform (C2DPoint &out, const C2DPoint &in) const;
	
	  virtual bool invert();

	  virtual void prepareAdjustment(CNormalEquationMatrix &N, CtVector &m, double &vtpv, 
		                             unsigned long &nObs) const;

	  virtual void accumulate(const C2DPoint &p1, const C2DPoint &p2, CNormalEquationMatrix &N, 
		                      CtVector &m, double &vtpv, unsigned long &nObs);

	  virtual bool solve(CNormalEquationMatrix &N, const CtVector &m,  
		                 double vtpv, unsigned long nObs, double &s0);

//	  virtual float getQvv(const C2DPoint &p1, Matrix &covar) const;

	  virtual void addVec(const Matrix &dx, int index0);

	  virtual void getDerivatives(double x, double y, Matrix &Ax, Matrix &Ay) const;

	  virtual void getDerivativesCoords(double x, double y, Matrix &B) const;

	  virtual int getWeightForDirectObs(Matrix &weights, double sigma0, double maxInfluence, double maxDist) const;

	  bool isa(const char* className)
	  {
		  if (::strcmp("CTrans2DConformal", className) == 0)
			  return true;
		  return false;
	  }

	  void set(Matrix* parms);

	  virtual void get(Matrix &parms, C2DPoint &p0) const;

	  virtual void set(const Matrix &parms, const C2DPoint &p0);

};
#endif
