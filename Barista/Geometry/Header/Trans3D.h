#ifndef __CTrans3D__
#define __CTrans3D__

#include "ZObject.h"
#include "ReferenceSystem.h"
#include "3DPoint.h"
#include "XYZPointArray.h"

class Matrix;


class CTrans3D : public ZObject
{
  protected:
	  
	  CReferenceSystem systemFrom;

	  CReferenceSystem systemTo;

  public:

	  CTrans3D(const CReferenceSystem &sysFrom, const CReferenceSystem &sysTo);

	  ~CTrans3D();

	  CReferenceSystem getSystemFrom() const { return systemFrom; }

	  CReferenceSystem getSystemTo() const { return systemTo; }

    /**
     * Transforms the point XYZ; Default: identical transformation!
     * @param XYZ the Point to be transformed
     * @return the transformed point;
     */
    virtual void transform (C3DPoint &out, const C3DPoint &in)
	{
		out.x = in.x;
		out.y = in.y;
		out.z = in.z;
	};

    virtual void transform (CXYZPoint &out, const CXYZPoint &in);

    void transformArray (CXYZPointArray* out, CXYZPointArray* in) 
    {
        out->RemoveAll();

        for (int i = 0; i < in->GetSize(); i++)
        {
            CXYZPoint* newPoint = out->add();
			this->transform(*newPoint, *in->getAt(i));
        };
    }

	virtual void getDerivatives(Matrix &derivatives, const C3DPoint &p) const;

	//virtual CTrans3D getInverse() = 0;

    //@todo must return more than one double
//	virtual double getParameters() = 0;
//	virtual void setParameters(double parms) = 0;
	//virtual const char* getName() = 0;

	 string getClassName() const
	 {
		 return string("CTrans3D");
	 };

	 bool isa (string& className) const
	 {
		if (className == "CTrans3D")
			return true;

		return false;
	 };
};
#endif

