#ifndef __CXYLine__
#define __CXYLine__

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include "2DPoint.h"
#include "newmat.h"

enum eIntersect {eNoIntersection, eIntersection, eParallel, eIncluded, 
                 eTwoIntersections, eTouches };

// eNoIntersection: two lines do not intersect
// eIntersection:   two lines intersect
// eParallel:       two lines are parallel
// eIncluded:       a line is included by the other
// eTwoIntersections: line has two intersections with surface (eg quadric)
// eTouches:          line touches the surface (eg quadric)

class CXYContour;
class CXYPoint;

//*****************************************************************************
/*!
 * \brief
 * A 2D line with a homogeneous representation amd a covariance matrix 
 * and methods for statistical geometric reasoning.
 */
//*****************************************************************************

class CXYLine : public ZObject
{

protected:

    /// the line's normal vector
    C2DPoint normal;

    /// the line's centre of gravity if the straight line is computed by regression
    C2DPoint cog;

    /// euclidean part of the line. Thus, the homogeneous representation of the line is: (normal.x, normal.y, euclidean)
    double euclidean;        

    /// The eigenvalues of the matrix ( sum[(x-x_bar)^2]            sum[(x-x_bar)*(y-y_bar)] )
    ///                               ( sum[(x-x_bar)*(y-y_bar)]    sum[(x-x_bar)^2]         )
    /// if the straight line is computed by regression
    C2DPoint eigenvalues;

	/// number of points used for regression
    unsigned long pointcount;           

	/// rms error of the weight unit in adjustment
    double sigma0; 

	/// variance-covariance matrix of the homogeneous vector, in the order of the homogeneous vector (see above)
    Matrix covariance;    

   

public:

    CXYLine();

    ~CXYLine();

	CXYLine(const CXYLine &line);

    CXYLine(const CXYPoint &p1, const CXYPoint &p2);

	void initFromPoints(const CXYPoint &p1, const CXYPoint &p2);

    C2DPoint getNormal() const { return normal; };

    C2DPoint getDirection() const;

	double   getEuclidean() const { return euclidean; };
   
	C2DPoint getCog() const { return cog; };

	C2DPoint getEigenvalues() const { return eigenvalues; };
   
	unsigned long getPointcount() const { return pointcount; };

	double getSigma0() const { return sigma0; };

	Matrix *getCovariance() { return &covariance; };

	Matrix getCovarianceCopy() const { return covariance; };

	/// variance-covariance relative to p
	Matrix  getCovariance(const C2DPoint &p) const;

	/// homogeneous 3 x 1 vector of the line parameters:  (normal.x, normal.y, euclidean)^t
	ColumnVector getVector() const;

    /// 3x3 homogeneous rotation matrix for lines, rotating the x-axis into the line
    Matrix getRotMat() const;

    /// 3x3 construction matrix (axiator)
    Matrix getS() const;

    /// 3x3 Jacobian matrix 
    Matrix getJacobian() const;

//  For definitions, see 
//  Stefan Heuel, S., 2004. Uncertain Projective Geometry.
//  Statistical Reasoning for Polyhedral Object Reconstruction. 
//  Springer-Verlag, Berlin Heidelberg, Germany.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	string getClassName() const
	 {
		 return string("CXYLine");
	 };

	 bool isa (string& className) const
	 {
		if (className == "CXYLine")
			return true;

		return false;
	 };
   
	// distance functions

    double distanceFromLine (const CXYPoint &point) const;

    double distanceFromLine (const C2DPoint &point) const;

	/// reverse line - multiply normal and euclidean by -1
    void changeDirection();


	/// get base point
    C2DPoint getBasePoint( const C2DPoint &point ) const;
  	
	/// calculate the projection of p to the straight line
    CXYPoint projectedPoint(const CXYPoint &p) const;


	/// Statistical tests of geometric relations 
   float containsPoint(const CXYPoint &point) const;

   float isIdentical(const CXYLine &line) const;

   float isParallel(const CXYLine &line) const;

// The ratio r of the test metric t (cf. PH_3D_AdjustPlane::d_testMetric) 
// and the quantil of the chi square distribution is computed, thus with 
// N = rank of the distance measure:
//      r = t / C_chiSquareQuantil(N)
//
// return value:  r if r <= 1, thus the geometric relation is fulfilled
//               -r if r >  1, thus the geometric relation is not fulfilled
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

   /// calculate the intersection of the straight lines (!)
   eIntersect getIntersection(const CXYLine &line, 
                              CXYPoint &intersection) const;

// intersection will be the intersection between the straight lines (!) of *this 
// and line. 
//
// return value: ePARALLEL  if the lines do not intersect (here: 
//                          homogeneous part of intersection < FLT_EPSILON)
//                          In this case, intersection must not be used further
//               eINTERSECT if the lines intersect



   /// Initialise segment from polygon part 
   virtual void initFromContour(CXYContour *contour, unsigned long indexFrom,
	                             unsigned long indexTo, double I_sigma0);

// The line will be computed as an adjusting 2D straight line from 
// contour[indexFrom] to contour[indexTo]. If I_sigma0 > 0, I_sigma0 will be 
// used as an a priori rms error of the weight unit 
// (if the latter is considered to be too optimistic)
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   
   virtual void initFromVector(C2DPoint *points, unsigned long length);


//=============================================================================

 protected:

  void computeParametersFromContour(CXYContour *contour, 
	                                unsigned long indexFrom,
	                                unsigned long indexTo, double I_sigma0);

  void normalise();

  virtual void initCovarFromAdjustment(double I_sigma0);

};

#endif



