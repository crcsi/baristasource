#ifndef __RotationBase__
#define __RotationBase__

#include "PointBase.h"
#include "Serializable.h"
#include "3DPoint.h"

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

typedef enum { RPY, OPK, Quaternion} RotationType;

class CAdjustMatrix;
class Matrix;
class CStructuredFileSection;

class CRotationMatrix : public CSerializable
{
	public:

	CRotationMatrix() {for (int i=0; i<9; i++) this->m[i] = 0.0;};
	CRotationMatrix(const CRotationMatrix& r) {for (int i=0; i<9; i++) this->m[i] = r.m[i];};
	CRotationMatrix(const double r0,const double r1,const double r2,
					const double r3,const double r4,const double r5,
					const double r6,const double r7,const double r8) 
		{this->m[0] = r0;
		 this->m[1] = r1;
		 this->m[2] = r2;
		 this->m[3] = r3;
		 this->m[4] = r4;
		 this->m[5] = r5;
		 this->m[6] = r6;
		 this->m[7] = r7;
		 this->m[8] = r8;};
	
	CRotationMatrix(const C3DPoint& c1,const C3DPoint& c2,const C3DPoint& c3)
		{this->m[0] = c1.x;
		 this->m[1] = c2.x;
		 this->m[2] = c3.x;
		 this->m[3] = c1.y;
		 this->m[4] = c2.y;
		 this->m[5] = c3.y;
		 this->m[6] = c1.z;
		 this->m[7] = c2.z;
		 this->m[8] = c3.z;};
	

	

	~CRotationMatrix(){};
	
	// operators
	CRotationMatrix &operator = (const CRotationMatrix & r);

	double operator ()(const unsigned char element) const
	{
		assert(element < 9);
		return this->m[element];
	};


	double& operator ()(const unsigned char element)
	{
		assert(element < 9);
		return this->m[element];
	};

	double operator ()(const unsigned char row,const unsigned char col)const
	{
		assert(row < 3 && col < 3);
		return this->m[row*3+col];
	};

	double& operator ()(const unsigned char row,const unsigned char col)
	{
		assert(row < 3 && col < 3);
		return this->m[row*3+col];
	};

	void getFirstRow(double &el0, double &el1, double &el2) const
	{ el0 = m[0]; el1 = m[1]; el2 = m[2]; };
	void getSecondRow(double &el0, double &el1, double &el2) const
	{ el0 = m[3]; el1 = m[4]; el2 = m[5]; };
	void getThirdRow(double &el0, double &el1, double &el2) const
	{ el0 = m[6]; el1 = m[7]; el2 = m[8]; };

	void getFirstCol(double &el0, double &el1, double &el2) const
	{ el0 = m[0]; el1 = m[3]; el2 = m[6]; };
	void getSecondCol(double &el0, double &el1, double &el2) const
	{ el0 = m[1]; el1 = m[4]; el2 = m[7]; };
	void getThirdCol(double &el0, double &el1, double &el2) const
	{ el0 = m[2]; el1 = m[5]; el2 = m[8]; };

	void setFirstRow(double &el0, double &el1, double &el2) 
	{ m[0] = el0; m[1] = el1; m[2] = el2; };
	void setSecondRow(double &el0, double &el1, double &el2) 
	{ m[3] = el0; m[4] = el1; m[5] = el2; };
	void setThirdRow(double &el0, double &el1, double &el2) 
	{ m[6] = el0; m[7] = el1; m[8] = el2; };

	void setFirstCol(double &el0, double &el1, double &el2) 
	{ m[0] = el0; m[3] = el1; m[6] = el2; };
	void setSecondCol(double &el0, double &el1, double &el2) 
	{ m[1] = el0; m[4] = el1; m[7] = el2; };
	void setThirdCol(double &el0, double &el1, double &el2) 
	{ m[2] = el0; m[5] = el1; m[8] = el2; };

	// function for multiplication
	// with vector
	void R_times_x(C3DPoint& result,const C3DPoint& x) const
	{ result.x = m[0]*x.x + m[1]*x.y + m[2]*x.z;
	  result.y = m[3]*x.x + m[4]*x.y + m[5]*x.z;
	  result.z = m[6]*x.x + m[7]*x.y + m[8]*x.z;};

	void RT_times_x(C3DPoint& result,const C3DPoint& x) const
	{ result.x = m[0]*x.x + m[3]*x.y + m[6]*x.z;
	  result.y = m[1]*x.x + m[4]*x.y + m[7]*x.z;
	  result.z = m[2]*x.x + m[5]*x.y + m[8]*x.z;};

	void xT_times_R(C3DPoint& result,const C3DPoint& x) const
	{ this->RT_times_x(result,x);};

	void xT_times_RT(C3DPoint& result,const C3DPoint& x) const
	{ this->R_times_x(result,x);};

	// with matrix
	void R_times_R(CRotationMatrix& result,const CRotationMatrix& r) const
	{
		result.m[0] = this->m[0] * r.m[0] + this->m[1] * r.m[3] + this->m[2] * r.m[6];
		result.m[1] = this->m[0] * r.m[1] + this->m[1] * r.m[4] + this->m[2] * r.m[7];
		result.m[2] = this->m[0] * r.m[2] + this->m[1] * r.m[5] + this->m[2] * r.m[8];
		result.m[3] = this->m[3] * r.m[0] + this->m[4] * r.m[3] + this->m[5] * r.m[6];
		result.m[4] = this->m[3] * r.m[1] + this->m[4] * r.m[4] + this->m[5] * r.m[7];
		result.m[5] = this->m[3] * r.m[2] + this->m[4] * r.m[5] + this->m[5] * r.m[8];
		result.m[6] = this->m[6] * r.m[0] + this->m[7] * r.m[3] + this->m[8] * r.m[6];
		result.m[7] = this->m[6] * r.m[1] + this->m[7] * r.m[4] + this->m[8] * r.m[7];
		result.m[8] = this->m[6] * r.m[2] + this->m[7] * r.m[5] + this->m[8] * r.m[8];
	};

	void R_times_RT(CRotationMatrix& result,const CRotationMatrix& rT) const
	{
		result.m[0] = this->m[0] * rT.m[0] + this->m[1] * rT.m[1] + this->m[2] * rT.m[2];
		result.m[1] = this->m[0] * rT.m[3] + this->m[1] * rT.m[4] + this->m[2] * rT.m[5];
		result.m[2] = this->m[0] * rT.m[6] + this->m[1] * rT.m[7] + this->m[2] * rT.m[8];
		result.m[3] = this->m[3] * rT.m[0] + this->m[4] * rT.m[1] + this->m[5] * rT.m[2];
		result.m[4] = this->m[3] * rT.m[3] + this->m[4] * rT.m[4] + this->m[5] * rT.m[5];
		result.m[5] = this->m[3] * rT.m[6] + this->m[4] * rT.m[7] + this->m[5] * rT.m[8];
		result.m[6] = this->m[6] * rT.m[0] + this->m[7] * rT.m[1] + this->m[8] * rT.m[2];
		result.m[7] = this->m[6] * rT.m[3] + this->m[7] * rT.m[4] + this->m[8] * rT.m[5];
		result.m[8] = this->m[6] * rT.m[6] + this->m[7] * rT.m[7] + this->m[8] * rT.m[8];
	};

	void RT_times_R(CRotationMatrix& result,const CRotationMatrix& r) const
	{
		result.m[0] = this->m[0] * r.m[0] + this->m[3] * r.m[3] + this->m[6] * r.m[6];
		result.m[1] = this->m[0] * r.m[1] + this->m[3] * r.m[4] + this->m[6] * r.m[7];
		result.m[2] = this->m[0] * r.m[2] + this->m[3] * r.m[5] + this->m[6] * r.m[8];
		result.m[3] = this->m[1] * r.m[0] + this->m[4] * r.m[3] + this->m[7] * r.m[6];
		result.m[4] = this->m[1] * r.m[1] + this->m[4] * r.m[4] + this->m[7] * r.m[7];
		result.m[5] = this->m[1] * r.m[2] + this->m[4] * r.m[5] + this->m[7] * r.m[8];
		result.m[6] = this->m[2] * r.m[0] + this->m[5] * r.m[3] + this->m[8] * r.m[6];
		result.m[7] = this->m[2] * r.m[1] + this->m[5] * r.m[4] + this->m[8] * r.m[7];
		result.m[8] = this->m[2] * r.m[2] + this->m[5] * r.m[5] + this->m[8] * r.m[8];
	};

	void RT_times_RT(CRotationMatrix& result,const CRotationMatrix& rT) const
	{
		result.m[0] = this->m[0] * rT.m[0] + this->m[3] * rT.m[1] + this->m[6] * rT.m[2];
		result.m[1] = this->m[0] * rT.m[3] + this->m[3] * rT.m[4] + this->m[6] * rT.m[5];
		result.m[2] = this->m[0] * rT.m[6] + this->m[3] * rT.m[7] + this->m[6] * rT.m[8];
		result.m[3] = this->m[1] * rT.m[0] + this->m[4] * rT.m[1] + this->m[7] * rT.m[2];
		result.m[4] = this->m[1] * rT.m[3] + this->m[4] * rT.m[4] + this->m[7] * rT.m[5];
		result.m[5] = this->m[1] * rT.m[6] + this->m[4] * rT.m[7] + this->m[7] * rT.m[8];
		result.m[6] = this->m[2] * rT.m[0] + this->m[5] * rT.m[1] + this->m[8] * rT.m[2];
		result.m[7] = this->m[2] * rT.m[3] + this->m[5] * rT.m[4] + this->m[8] * rT.m[5];
		result.m[8] = this->m[2] * rT.m[6] + this->m[5] * rT.m[7] + this->m[8] * rT.m[8];
	};

	// transpose the matrix
	void transpose(CRotationMatrix& result) const
	{
		result.m[0] = m[0];
		result.m[1] = m[3];
		result.m[2] = m[6];
		result.m[3] = m[1];
		result.m[4] = m[4];
		result.m[5] = m[7];
		result.m[6] = m[2];
		result.m[7] = m[5];
		result.m[8] = m[8];
	}

	void transpose()
	{
		double swap = m[1];
		m[1] = m[3];
		m[3] = swap;
		
		swap = m[2];
		m[2] = m[6];
		m[6] = swap;
		
		swap = m[5];
		m[5] = m[7];
		m[7] = swap;
	}

	  // serialize functions
	  virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	  virtual bool isModified(){return this->bModified;};
	  virtual void reconnectPointers(){CSerializable::reconnectPointers();};
	
	  virtual bool isa(string& className) const
	  {
		  if (className == "CRotationMatrix")
			  return true;
		  return false;
	  };

	  virtual string getClassName() const {return string("CRotationMatrix"); };  


	  CCharString getString(const CCharString &lead, int digits) const;

	protected:

	double m[9];

	//            / rE[0] rE[1] rE[2] \
	// rotMat =  |  rE[3] rE[4] rE[5]  |
	//            \ rE[6] rE[7] rE[8] /

};
















class CRotationBase	: public CPointBase,  public CSerializable
{

protected:
	double* sinValues;		// stored as radiance values
	double* cosValues;		// stored as radiance values

	CRotationMatrix rotMat;			// rotation Matrix
	Matrix *covariance; // = Matrix.identity(3,3);
	

protected:
	CRotationBase(int dim,RotationType type);
	CRotationBase(const CRotationBase & rb);

	void computeSinAndCos();
	void initCovar();

	RotationType type;
public:

	virtual ~CRotationBase(void);

	double getSinValue(int index)const {assert(index >=0 && index < dim); return this->sinValues[index];};
	double getCosValue(int index)const {assert(index >=0 && index < dim); return this->cosValues[index];};

	virtual void computeRotMatFromParameter() = 0;
	virtual bool computeParameterFromRotMat() = 0;

	virtual void computeDerivation(CPointBase& deriv,const int parameter) const=0;
	virtual void computeDerivation(CAdjustMatrix& derivMat) const=0;

	CRotationMatrix& getRotMat(){return this->rotMat;};
	const CRotationMatrix& getRotMat()const{return this->rotMat;};

	virtual void updateAngles_Rho(const CPointBase* angles) = 0;
	virtual void updateAngles_Gon(const CPointBase* angles) = 0;
	virtual void updateRotMat(const CRotationMatrix &mat) = 0;
	
	double rhoGon();

	virtual CRotationBase &operator = (const CRotationBase & rb);

	virtual void setCovariance (const Matrix &covariance);	
	virtual void setCovarElement(int row, int col, double value);

	virtual Matrix* getCovariance() const { return this->covariance; };
	virtual const Matrix &getCovar() const { return *(this->covariance); };
	virtual const double getCovarElement(int row, int col) const;
	virtual const double getSigma(int index) const;


	RotationType getType()const {return this->type;};

	// function for multiplication
	// with vector
	void R_times_x(C3DPoint& result,const C3DPoint& x) const
	{ this->rotMat.R_times_x(result,x);};

	void RT_times_x(C3DPoint& result,const C3DPoint& x) const
	{ this->rotMat.RT_times_x(result,x);};

	void xT_times_R(C3DPoint& result,const C3DPoint& x) const
	{ this->rotMat.RT_times_x(result,x);};

	void xT_times_RT(C3DPoint& result,const C3DPoint& x) const
	{ this->rotMat.R_times_x(result,x);};

	// with matrix
	void R_times_R(CRotationMatrix& result,const CRotationMatrix& r) const
	{
		this->rotMat.R_times_R(result,r);
	};

	void R_times_RT(CRotationMatrix& result,const CRotationMatrix& rT) const
	{
		this->rotMat.R_times_RT(result,rT);
	};

	void RT_times_R(CRotationMatrix& result,const CRotationMatrix& r) const
	{
		this->rotMat.RT_times_R(result,r);
	};

	void RT_times_RT(CRotationMatrix& result,const CRotationMatrix& rT) const
	{
		this->rotMat.RT_times_RT(result,rT);
	};

	// transpose the matrix
	void transpose(CRotationMatrix& result) const
	{
		this->rotMat.transpose(result);
	}

	void transpose()
	{
		this->rotMat.transpose();
		this->computeParameterFromRotMat();
		this->computeSinAndCos();
	}

	  // serialize functions
	  virtual void serializeStore(CStructuredFileSection* pStructuredFileSection)=0;
	  virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection)=0;
	  virtual bool isModified(){return this->bModified;};
	  virtual void reconnectPointers();
	  virtual void resetPointers();

	  virtual bool isa(string& className) const = 0;

	  virtual string getClassName() const = 0;  


};





#endif


