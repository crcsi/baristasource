#ifndef __CTransGeographicToGeocentric__
#define __CTransGeographicToGeocentric__

#include "Trans3D.h"

class CTransGeographicToGeocentric : public CTrans3D
{
 public:

    CTransGeographicToGeocentric(const CReferenceSystem &sysFrom, 
								 const CReferenceSystem &sysTo);

	~CTransGeographicToGeocentric();

	virtual void transform(C3DPoint  &out, const C3DPoint  &in);
     
	virtual void transform(CXYZPoint &out, const CXYZPoint &in);

	virtual void getDerivatives(Matrix &derivatives, const C3DPoint &p) const;

	bool isa(const char* className) const
    {
         if (::strcmp("CTransGeographicToGeocentric", className) == 0)
             return true;
         return false;
    };
	 
	string getClassName() const
	{
		return string("CTransGeographicToGeocentric");	
	};

};
#endif
