#ifndef __CTransGridToGeographic__
#define __CTransGridToGeographic__

#include "Trans3D.h"

class CTransGridToGeographic : public CTrans3D
{
  public:

	  CTransGridToGeographic(const CReferenceSystem &sysFrom, const CReferenceSystem &sysTo);

	  ~CTransGridToGeographic();

	  virtual void transform(C3DPoint  &out, const C3DPoint  &in);

	  virtual void transform(CXYZPoint &out, const CXYZPoint &in);
	
	  virtual void getDerivatives(Matrix &derivatives, const C3DPoint &p) const;

	  bool isa(const char* className) const
	  {
		  if (::strcmp("CTransGridToGeographic", className) == 0)
			  return true;
		  return false;
	  };

	 string getClassName()	  const
	 {
		  return string("CTransGridToGeographic");
	 };

};
#endif
