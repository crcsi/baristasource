#ifndef __CQuaternionRotation__
#define __CQuaternionRotation__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "RotationBase.h"
typedef enum {eQQuickBird,eQALOS,eGeoEye1}QuaternionTypes;

class CQuaternionRotation : public CRotationBase
{
public:
	CQuaternionRotation(void): CRotationBase(4,Quaternion),type(eQQuickBird) {};
	CQuaternionRotation(const CPointBase* p_rho);
	CQuaternionRotation(const CPointBase* p_rho,QuaternionTypes type);
	CQuaternionRotation(const CRotationMatrix &mat);

	virtual ~CQuaternionRotation(void) {};

	virtual void updateAngles_Rho(const CPointBase* angles);
	virtual void updateAngles_Gon(const CPointBase* angles){assert(false);};
	virtual void updateRotMat(const CRotationMatrix &mat);

	virtual void computeRotMatFromParameter();
	virtual bool computeParameterFromRotMat();

	virtual void computeDerivation(CPointBase& deriv,const int parameter)const;
	virtual void computeDerivation(CAdjustMatrix& derivMat) const;

	  // serialize functions
	  virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	
	  virtual bool isa(string& className) const	
	  {
		  if (className == "CQuaternionRotation")
			  return true;
		  return false;
	  };

	  virtual string getClassName() const {return string("CQuaternionRotation"); };  

	  QuaternionTypes getQuaternionType()const {return this->type;};


protected:

	QuaternionTypes type;

};
#endif
