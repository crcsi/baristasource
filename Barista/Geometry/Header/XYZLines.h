//============================================================================

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      IPI, University of Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//============================================================================

/*!
 * \brief
 * A set of classes describing different versions of 3D lines
 */

//============================================================================

#ifndef __CXYZLINES_H__
#define __CXYZLINES_H__

#include "3DPoint.h"
#include "XYLine.h"
#include "CharString.h"

//*****************************************************************************
//**************************** Forward Declarations ***************************

class CXYZRay;
class CXYZSegment;



//*****************************************************************************
//
// c l a s s   C X Y Z L i n e B a s e
//
//*****************************************************************************

class CXYZLineBase
{

//=============================================================================
//================================== data =====================================

  protected:

	  static double epsilon;

//=============================================================================

  public:

//=============================================================================
//============================= Constructor ===================================

	  CXYZLineBase() {}


//=============================================================================
//=============================== Destructor ==================================

	  ~CXYZLineBase() {};


//=============================================================================
//=============================== query data ==================================

	  static double getEpsilon();


//=============================================================================
//================================ set epsilon ================================

	  static void setEpsilon(double Epsilon);


//=============================================================================
//================================ query validity of geometry =================

	  //! returns true if the geometric properties of the instance are well defined
	  virtual bool isWellDefined() const = 0;

	  //! returns false, if the geometric properties of the instance are not well 
	  //! defined (e.g. normal vector of a plane = null vector, or direction  
	  //! vector of a line = null vector)
	  bool operator! () { return !this->isWellDefined(); }

};

//*****************************************************************************
//
// E n d   o f   c l a s s   C X Y Z L i n e B a s e
//
//*****************************************************************************

//*****************************************************************************
//
// c l a s s   C X Y Z L i n e
//
//*****************************************************************************

class CXYZLine : public CXYZLineBase
{

//=============================================================================
//================================== data =====================================

  protected:
  
	  C3DPoint point;

	  C3DPoint direction;

//=============================================================================

  public:

//=============================================================================
//============================= Constructors ==================================

	  CXYZLine(const C3DPoint &p1, const C3DPoint &p2);

	  CXYZLine(const CXYZLine &line);

	  CXYZLine(const CXYZRay &ray);

	  CXYZLine(const CXYZSegment &segment);


//=============================================================================
//=============================== Destructor ==================================
 
	  ~CXYZLine() {};

//=============================================================================
//=============================== correctness of geometry =====================

	  //! return true if the distance vector is different from 0
	  virtual bool isWellDefined() const 
	  { return this->direction.getSquareNorm() > this->getEpsilon(); }


//=============================================================================
//=============================== query data ==================================

	  C3DPoint getPoint() const { return this->point; }

	  C3DPoint getDirection() const { return this->direction; }

	  C3DPoint getPointOnLine(double distance) const
	  { return (this->point + distance * this->direction); }


//=============================================================================
//=========================== relation to point ===============================

	  C3DPoint getBasePoint(const C3DPoint &p) const
	  { return this->getPointOnLine(this->direction.innerProduct(p - this->point)); }

	  double getDistance(const C3DPoint &p) const;

	  bool containsPoint(const C3DPoint &p) const
	  { return (this->getDistance(p) < getEpsilon()); };


//=============================================================================
//========================== relation to other line ===========================

	  bool isParallel( const CXYZLine &line ) const;

	  bool isParallel( const CXYZRay &ray ) const;

	  bool isParallel( const CXYZSegment &segment ) const;

	  // return value: 1: *this and the input line are parallel
	  //                  (considering epsilon)
	  //               0: *this and the input line are not parallel
	  //                  (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  
	  eIntersect intersect(const CXYZLine &line) const;

	  eIntersect intersect(const CXYZRay &ray) const;

	  eIntersect intersect(const CXYZSegment &segment) const;

	  // return value: eIntersecttion:   *this and the input line intersect
	  //                                (considering epsilon)
	  //               eIncluded:       *this and the input line are identical
	  //                                (considering epsilon)
	  //               eParallel:       *this and the input line are parallel
	  //                                (considering epsilon)
	  //               eNoIntersection: *this and the input line do not intersect
	  //                                (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  eIntersect getIntersection(const CXYZLine &line, C3DPoint &p) const;

	  eIntersect getIntersection(const CXYZRay &ray, C3DPoint &p) const;

	  eIntersect getIntersection(const CXYZSegment &segment, C3DPoint &p) const;

	  // return value: eIntersecttion:   *this and the input line intersect
	  //                                (considering epsilon). Only in this case
	  //                                p contains the co-ordinates of the
	  //                                intersection point
	  //               eIncluded:       *this and the input line are identical
	  //                                (considering epsilon)
	  //               eParallel:       *this and the input line are parallel
	  //                                (considering epsilon)
	  //               eNoIntersection: *this and the input line do not intersect
	  //                                (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	  double getDistance(const CXYZLine &line) const;

	  double getDistance(const CXYZRay &ray) const;

	  double getDistance(const CXYZSegment &segment) const;

	  // return value: minimum distance between *this and the input line
	  //
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
};

//*****************************************************************************
//
// E n d   o f   c l a s s   C X Y Z L i n e
//
//*****************************************************************************

//*****************************************************************************
//
// c l a s s   C X Y Z R a y
//
//*****************************************************************************

class CXYZRay : public CXYZLineBase
{

//=============================================================================
//================================== data =====================================

  protected:

	  C3DPoint point;

	  C3DPoint direction;

//=============================================================================

  public:

//=============================================================================
//============================= Constructors ==================================

	  CXYZRay( const C3DPoint &p1, const C3DPoint &p2);

	  CXYZRay( const CXYZLine &line );

	  CXYZRay( const CXYZRay &ray );

	  CXYZRay( const CXYZSegment &segment );


//=============================================================================
//=============================== Destructor ==================================

	  ~CXYZRay() {};


//=============================================================================
//=============================== correctness of geometry =====================

	  //! return true if the distance vector is different from 0
	  virtual bool isWellDefined() const
	  { return this->direction.getSquareNorm() > getEpsilon(); }

//=============================================================================
//=============================== query data ==================================

 	  C3DPoint getPoint() const { return this->point; }

	  C3DPoint getDirection() const { return this->direction; }

	  C3DPoint getPointOnLine(double distance) const
	  { return (this->point + distance * this->direction); }


//=============================================================================
//=========================== relation to point ===============================

  	  C3DPoint getBasePoint(const C3DPoint &p) const
	  { return this->getPointOnLine(this->direction.innerProduct(p - this->point)); }

	  double getDistance(const C3DPoint &p) const;

	  bool containsPoint(const C3DPoint &p) const
	  { return (this->getDistance(p) < getEpsilon()); };


//=============================================================================
//========================== relation to other line ===========================

	  bool isParallel( const CXYZLine &line ) const;

	  bool isParallel( const CXYZRay &ray ) const;

	  bool isParallel( const CXYZSegment &segment ) const;

	  // return value: 1: *this and the input line are parallel
	  //                  (considering epsilon)
	  //               0: *this and the input line are not parallel
	  //                  (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  
	  eIntersect intersect(const CXYZLine &line) const;

	  eIntersect intersect(const CXYZRay &ray) const;

	  eIntersect intersect(const CXYZSegment &segment) const;

	  // return value: eIntersecttion:   *this and the input line intersect
	  //                                (considering epsilon)
	  //               eIncluded:       *this and the input line are identical
	  //                                (considering epsilon)
	  //               eParallel:       *this and the input line are parallel
	  //                                (considering epsilon)
	  //               eNoIntersection: *this and the input line do not intersect
	  //                                (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  eIntersect getIntersection(const CXYZLine &line, C3DPoint &p) const;

	  eIntersect getIntersection(const CXYZRay &ray, C3DPoint &p) const;

	  eIntersect getIntersection(const CXYZSegment &segment, C3DPoint &p) const;

	  // return value: eIntersecttion:   *this and the input line intersect
	  //                                (considering epsilon). Only in this case
	  //                                p contains the co-ordinates of the
	  //                                intersection point
	  //               eIncluded:       *this and the input line are identical
	  //                                (considering epsilon)
	  //               eParallel:       *this and the input line are parallel
	  //                                (considering epsilon)
	  //               eNoIntersection: *this and the input line do not intersect
	  //                                (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	  double getDistance(const CXYZLine &line) const;

	  double getDistance(const CXYZRay &ray) const;

	  double getDistance(const CXYZSegment &segment) const;

	  // return value: minimum distance between *this and the input line
	  //
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
};

//*****************************************************************************
//
// E n d   o f   c l a s s   C X Y Z R a y
//
//*****************************************************************************
//*****************************************************************************
//
// c l a s s   C X Y Z _ S e g m e n t
//
//*****************************************************************************

class CXYZSegment : public CXYZLineBase
{

//=============================================================================
//================================== data =====================================

  protected:

	  C3DPoint point1;

	  C3DPoint point2;

	  C3DPoint direction;

//=============================================================================

  public:

//=============================================================================
//============================= Constructors ==================================

	  CXYZSegment();

	  CXYZSegment(const C3DPoint &p1, const C3DPoint &p2);

	  CXYZSegment(const CXYZLine &line, double length = 1.0);

	  CXYZSegment(const CXYZRay &ray, double length = 1.0);

	  CXYZSegment(const CXYZSegment &segment);


//=============================================================================
//=============================== Destructor ==================================

	  ~CXYZSegment() {};


//=============================================================================
//=============================== correctness of geometry =====================

	  //! return true if the distance vector is different from 0
	  virtual bool isWellDefined() const
	  { return ( this->point1 - this->point2).getSquareNorm() > getEpsilon(); }

//=============================================================================
//=============================== query data ==================================

 	  C3DPoint getPoint1() const { return this->point1; }

	  C3DPoint getPoint2() const { return this->point2; }

	  C3DPoint getDirection() const { return this->direction; };

	  double getLength() const { return (this->point1 - this->point2).getNorm(); };
	  
	  double getDistanceofBasePointFromStart(const C3DPoint &p) const
	  { return (this->direction.innerProduct(p - this->point1)); };

	  C3DPoint getPointOnLine(double distance) const
	  { return (this->point1 + distance * this->direction); };

	  C3DPoint getPointOnLinePerCent(double perCent) const
	  { return (this->point1 + perCent * 0.01 * (this->point2 - this->point1)); };


//=============================================================================
//============================= String for output =============================

	  CCharString getString(CCharString deliminator = " ", int width = 0, 
	 	                    int decimals = -1) const;


//=============================================================================
//=========================== relation to point ===============================

  
	  C3DPoint getBasePoint(const C3DPoint &p) const
	  { return this->getPointOnLine(this->direction.innerProduct(p - this->point1) ); };

	  double getDistance(const C3DPoint &p) const;

	  double getDistanceFromLine(const C3DPoint &p) const;

	  bool containsPoint(const C3DPoint &p) const
	  { return (this->getDistance(p) < getEpsilon()); };

	  double getSquareDistance( const C3DPoint &p ) const;
  
	  double getSquareDistanceFromLine( const C3DPoint &p ) const;


//=============================================================================
//========================== relation to other line ===========================

	  bool isParallel( const CXYZLine &line ) const;

	  bool isParallel( const CXYZRay &ray ) const;

	  bool isParallel( const CXYZSegment &segment ) const;

	  // return value: 1: *this and the input line are parallel
	  //                  (considering epsilon)
	  //               0: *this and the input line are not parallel
	  //                  (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  
	  eIntersect intersect(const CXYZLine &line) const;

	  eIntersect intersect(const CXYZRay &ray) const;

	  eIntersect intersect(const CXYZSegment &segment) const;

	  // return value: eIntersecttion:   *this and the input line intersect
	  //                                (considering epsilon)
	  //               eIncluded:       *this and the input line are identical
	  //                                (considering epsilon)
	  //               eParallel:       *this and the input line are parallel
	  //                                (considering epsilon)
	  //               eNoIntersection: *this and the input line do not intersect
	  //                                (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  eIntersect getIntersection(const CXYZLine &line, C3DPoint &p) const;

	  eIntersect getIntersection(const CXYZRay &ray, C3DPoint &p) const;

	  eIntersect getIntersection(const CXYZSegment &segment, C3DPoint &p) const;

	  // return value: eIntersecttion:   *this and the input line intersect
	  //                                (considering epsilon). Only in this case
	  //                                p contains the co-ordinates of the
	  //                                intersection point
	  //               eIncluded:       *this and the input line are identical
	  //                                (considering epsilon)
	  //               eParallel:       *this and the input line are parallel
	  //                                (considering epsilon)
	  //               eNoIntersection: *this and the input line do not intersect
	  //                                (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	  double getDistance(const CXYZLine &line) const;

	  double getDistance(const CXYZRay &ray) const;

	  double getDistance(const CXYZSegment &segment) const;
	  double getAverageDistance(const CXYZSegment &segment) const;

	  // return value: minimum distance between *this and the input line
	  //
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  
	  bool isIdentical(CXYZSegment &segment, float cosAlpha, float distThreshold, 
		               int projectionFlag) const;

	  int project(CXYZSegment &segment) const;
	  // check whether segment is possibly identical with *this.
	  //
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	  //! invert *this.
	  void invert();

//=============================================================================

 protected:
 
	 void initDirection();


//=============================================================================

};

//*****************************************************************************
//
// E n d   o f   c l a s s   C X Y Z _ S e g m e n t
//
//*****************************************************************************

#endif
