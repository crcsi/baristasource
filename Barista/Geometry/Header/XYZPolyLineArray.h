#ifndef __CXYZPolyLineArray__
#define __CXYZPolyLineArray__


#include "XYZPolyLine.h"
#include "GenericPolyLineArray.h"
#include "MUObjectArray.h"
#include "CharString.h"

class CXYZPolyLineArray : public CGenericPolyLineArray
{
public:
	CXYZPolyLineArray(int nGrowBy = 0);
    CXYZPolyLineArray(CXYZPolyLineArray* src) {assert(false);};
    CXYZPolyLineArray(const CXYZPolyLineArray& src) {assert(false);};
	~CXYZPolyLineArray(void);

	virtual GenericPolyLine* CreateType() const { return new CXYZPolyLine; };
	virtual GenericPolyLine* CreateType(const GenericPolyLine &p) const { return new CXYZPolyLine(p); };

	virtual int getDimension() const { return 3;};

    bool writeXYZPolyLineTextFile(CFileName cfilename);
    bool writeXYZPolyLineVRMLFile(CFileName cfilename);
    bool writeXYZPolyLineDXFFile(CFileName cfilename);
	//addition >>
	bool writeXYZPolyLineShapefile(CFileName cfilename);
	//<< addition

	CXYZPolyLine *getAt(int index) const { return (CXYZPolyLine *) this->GetAt(index); };
	CXYZPolyLine *add() { return (CXYZPolyLine *) this->Add(); };
	CXYZPolyLine *add(const CXYZPolyLine &p) { return (CXYZPolyLine *) this->Add(p); };

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);


    CXYZPolyLine* getLine(CCharString Label);
    CXYZPolyLine* getLine(int i);

 

    string getClassName() const
    {
        return string("CXYZPolyLineArray");
    };

	bool isa (string& className) const
	{
		if (className == "CXYZPolyLineArray")
			return true;

		return false;
	};

	CDisplaySettings* getDisplaySettings() { return &this->displaySettings;};

protected:
	CDisplaySettings displaySettings;

};

#endif

