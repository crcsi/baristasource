#ifndef __CGenericPolyLineArray__
#define __CGenericPolyLineArray__


#include "GenericPolyLinePtrArray.h"
#include "GenericPolyLine.h"
#include "MUObjectArray.h"
#include "Filename.h"
#include "ReferenceSystem.h"

class CGenericPolyLineArray : public CMUObjectArray<GenericPolyLine, CGenericPolyLinePtrArray>, public CSerializable, public CUpdateHandler
{
public:
	CGenericPolyLineArray(int nGrowBy = 0);
    CGenericPolyLineArray(CGenericPolyLineArray* src) {assert(false);};
    CGenericPolyLineArray(const CGenericPolyLineArray& src) {assert(false);};
	~CGenericPolyLineArray(void);



	virtual int getDimension() const = 0;

    CFileName* getFileName();
    void setFileName(const char* filename);
    CFileName getItemText();

	GenericPolyLine* Add();
	GenericPolyLine* Add(const GenericPolyLine& newElement);

	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();

	virtual void Remove(GenericPolyLine* pElement);
	virtual void RemoveAt(int nIndex, int nCount= 1);
	virtual void RemoveAll();


	void selectElement(const CLabel& element,bool select);
	void unselectAll();

    GenericPolyLine* getLine(CCharString Label);
    GenericPolyLine* getLine(int i);


    string getClassName() const
    {
        return string("CGenericPolyLineArray");
    };

	bool isa (string& className) const
	{
		if (className == "CGenericPolyLineArray")
			return true;

		return false;
	};

   CReferenceSystem* getReferenceSystem()
    {
        return &this->refSystem;
    };

   void setReferenceSystem(const CReferenceSystem &refsys)
    {
		this->refSystem = refsys;
    };


protected:
    CFileName FileName;
    CReferenceSystem refSystem;
};
#endif