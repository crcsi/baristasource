#ifndef __CXYZPoint__
#define __CXYZPoint__

/**
 * Copyright:    Copyright (c) Jochen Willneff<p>
 * Company:      University of Melbourne<p>
 * @author Jochen Willneff
 * @version 1.0
 */
#include "3DPoint.h"
#include "ObsPoint.h"

class CXYPoint;

class CXYZPoint : public C3DPoint, public CObsPoint
{
  public:

  
	CXYZPoint() : CPointBase(3), C3DPoint(), CObsPoint(3) {};
	CXYZPoint(double x, double y, double z) : CPointBase(3), C3DPoint(x,y,z), CObsPoint(3){};
	CXYZPoint(double x, double y, double z, double dot) : CPointBase(3), C3DPoint(x,y,z,dot), CObsPoint(3){};
	CXYZPoint(double x, double y, double z, const CLabel &str) : CPointBase(3), C3DPoint(x,y,z), CObsPoint(3,str){};
	CXYZPoint(C3DPoint* p) : CPointBase(p), C3DPoint(*p), CObsPoint(3){};
	CXYZPoint(const C3DPoint &p) : CPointBase(p), C3DPoint(p), CObsPoint(3){};
	CXYZPoint(CXYZPoint* p) : CPointBase(p), C3DPoint(*p), CObsPoint(p){};
	CXYZPoint(const CXYZPoint &p) : CPointBase(p), C3DPoint(p),CObsPoint(p){};
	CXYZPoint(const CObsPoint &p) :CPointBase(p), C3DPoint(p),CObsPoint(p){};
	CXYZPoint(const CXYPoint &p, double I_z = 0.0f, double I_sigmaZ = 1.0f);
	

	virtual ~CXYZPoint();

	// get functions		
	double getX() const { return this->x; };
	double getY() const { return this->y; };
	double getZ() const { return this->z; };

	double getSX() const;
	double getSY() const;
	double getSZ() const;

	double getSXY() const;
	double getSXZ() const; 
	double getSYZ() const;


	CCharString toString() const;

	void setX (double X);
	void setY (double Y);
	void setZ (double Z);
	void set(double x, double y, double z);
	void set(CLabel label, double x, double y, double z);
	void setSX (double sigmaX);
	void setSY (double sigmaY);
	void setSZ (double sigmaZ);
	void setSXY (double sigmaXY);
	void setSXZ (double sigmaXZ);
	void setSYZ (double sigmaYZ);

	CXYZPoint &operator= (const CXYZPoint &point);
	CXYZPoint &operator= (const C3DPoint &point);

	bool equals (CXYZPoint* point);

	void transformPoint(CXYZPoint* inpoint, int destsystem, int originsystem);
	void transformPoint(CXYZPoint* inpoint, int destsystem, int originsystem, int zone, int hemisphere);

	  
	// CSerializable functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	void reconnectPointers();
	void resetPointers();

	  string getClassName() const
	  {
		  return string("CXYZPoint");
	  };

	  bool isa (string& className) const
	  {
		  if (className == "CXYZPoint")
			  return true;
		  return false;
	  };

	  bool isModified();


protected:

	void initCovarFrom2D(Matrix *covar2D, double I_sigmaZ);

};

//==============================================================================
// *****************************************************************************
//
//                          Inline function definitions 
//
// *****************************************************************************
//==============================================================================




inline CXYZPoint &CXYZPoint::operator= (const C3DPoint &point)
{
	this->x     = point.x; 
	this->y     = point.y; 
	this->z     = point.z; 	  
	this->dot   = point.dot; 
	this->id    = point.id; 
	this->setSelected(point.isSelected()); 

	return *this;
};

/**
 * Sets X coordinate of CXYZPoint
 * @param X 
 */
inline void CXYZPoint::setX(double X)
{
    this->bModified = true;
    this->x = X;
}

/**
 * Sets Y coordinate of CXYZPoint
 * @param Y 
 */
inline void CXYZPoint::setY(double Y)
{
    this->bModified = true;
    this->y = Y;
}

/**
 * Sets Z coordinate of CXYZPoint
 * @param Z 
 */
inline void CXYZPoint::setZ(double Z)
{
    this->bModified = true;
    this->z = Z;
}

/**
 * sets the coordinates
 * @param x
 * @param y
 * @param z
 */
inline void CXYZPoint::set(double x, double y, double z)
{
    this->bModified = true;
    this->x = x;
    this->y = y;
    this->z = z;
}

/**
 * sets the coordinates
 * @param label
 * @param x
 * @param y
 * @param z
 */
inline void CXYZPoint::set(CLabel label, double x, double y, double z)
{
    this->bModified = true;
	this->label = label;
    this->x = x;
    this->y = y;
    this->z = z;
}

#endif


