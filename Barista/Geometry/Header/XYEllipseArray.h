#ifndef __CXYEllipseArray__
#define __CXYEllipseArray__


#include "XYEllipse.h"
#include "XYEllipsePtrArray.h"
#include "MUObjectArray.h"
#include "UpdateHandler.h"
#include "Filename.h"
#include "TextFile.h"
#include "CharString.h"
#include "CharStringArray.h"

class CXYEllipseArray : public CMUObjectArray<CXYEllipse, CXYEllipsePtrArray>, public CSerializable, public CUpdateHandler
{
public:
	CXYEllipseArray(int nGrowBy = 0);
    CXYEllipseArray(CXYEllipseArray* src) {assert(false);};
    CXYEllipseArray(const CXYEllipseArray& src) {assert(false);};
	~CXYEllipseArray(void);


    CFileName* getFileName();
    void setFileName(const char* filename);
    CFileName getItemText();

	CXYEllipse* Add();
	CXYEllipse* Add(const CXYEllipse& newElement);

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    bool isModified();

    CXYEllipse* getEllipse(CCharString Label);
    CXYEllipse* getEllipse(int i);

	bool writeEllipseTextFile(CFileName cfilename);
	bool writeEllipseCentersTextFile(CFileName cfilename);

    string getClassName() const
    {
        return string("CXYEllipseArray");
    };

	bool isa (string& className) const
	{
		if (className == "CXYEllipseArray")
			return true;

		return false;
	};


	bool getActive()
	{
		return this->isActive;
	};

	void setActive(bool isactive)
	{
		this->isActive = isactive;
	};


protected:
    CFileName FileName;
	bool isActive;

};

#endif
