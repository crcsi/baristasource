#ifndef __COmegaPhiKappaRotation__
#define __COmegaPhiKappaRotation__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "RotationBase.h"

/* order of the angles
	coordinates[0]	= Omega  
	coordinates[1]	= Phi 
	coordinates[2]	= Kappa  
*/
class COmegaPhiKappaRotation : public CRotationBase
{
public:
	COmegaPhiKappaRotation(void) : CRotationBase(3,OPK) {};
	COmegaPhiKappaRotation(const CPointBase* p_rho);
	COmegaPhiKappaRotation(const CRotationMatrix &mat);

	virtual ~COmegaPhiKappaRotation(void){};


	virtual void updateAngles_Rho(const CPointBase* angles);
	virtual void updateAngles_Gon(const CPointBase* angles);
	virtual void updateRotMat(const CRotationMatrix &mat);

	virtual void computeRotMatFromParameter();
	virtual bool computeParameterFromRotMat();

	virtual void computeDerivation(CPointBase& deriv,const int parameter)const;
	virtual void computeDerivation(CAdjustMatrix& derivMat) const;



	  // serialize functions
	  virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	  virtual bool isa(string& className) const	
	  {
		  if (className == "COmegaPhiKappaRotation")
			  return true;
		  return false;
	  };

	  virtual string getClassName() const {return string("COmegaPhiKappaRotation"); }; 


};
#endif