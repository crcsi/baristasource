//============================================================================

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      IPI, University of Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//============================================================================

#ifndef __C3DADJPLANE_H__
#define __C3DADJPLANE_H__

#include <vector>
#include "3Dplane.h"
#include "XYZPoint.h"

//****************************************************************************
//*************************** Forward Declarations ***************************

 class CXYLineSegment;


//****************************************************************************
//********************************* Constants ********************************

#define MAXDOF  10


/*!
 * \brief
 * A plane in 3D with methods for computing its parameters by adjustment. 
 * Descendant of C3DPlane, a set of point is used to find the parameters of
 * the plane by a least squares adjustment. The single points may have weights.
 * No check is performed, if the weights are >= 0.
 * There are 4 modes of adjustment:
 * eXYpara ... the plane is parameterised over the xy-plane (z=ax+by+c),
 *             thus the distances of the points in z-direction are minimized
 * eYZpara ...                                     yz-plane
 * eZXpara ...                                     xz-plane
 * e3d     ... the orthogonal distances from the points to the plane are
 *             minimized
 *
 * If the system is not solvable the function is_well_defined() - from
 * C3DPlane, from CXYZLineBase - returns 0.
 */

//******************************************************************************

class C3DAdjustPlane : public C3DPlane 
{

//============================================================================

  protected:  

//============================================================================
//=================================== Data ===================================

	  //! reduction point for numerical stability
	  C3DPoint reduction;      

	  //! sum over x*x, y*y, x*y  for all points ((x*z*p, y*z*p, z*z*p if 
	  //! weights are given). all co-ordinates are reduced by this->reduction 
	  //! for numerical stability
	  C3DPoint squaredSumsXY;  

	  //! sum over x*z, y*z, z*z  for all points (x*x*p, y*y*p, x*y*p if 
	  //! weights are given). 
	  C3DPoint squaredSumsZ;   
   
	  //! sum over x,y, and z for all points (x*p, y*p, z*p if weights are given)
      //! Thus, the centre of gravity this->point  (inherited from C3DPlane) is
      //! this->point = (x_bar, y_bar, z_bar) = (this->sums / this->sumW)
	  C3DPoint sums;           

	  //! sum over all weights
	  double sumW;                  

   
	  //! The eigenvalues of the matrix
      //! ( sum[(x-x_bar)^2]          sum[(x-x_bar)(y-y_bar)]   sum[(x-x_bar)(z-z_bar)] )
      //! ( sum[(x-x_bar)(y-y_bar)]   sum[(y-y_bar)^2]          sum[(y-y_bar)(z-z_bar)] )
      //! ( sum[(x-x_bar)(z-z_bar)]   sum[(y-y_bar)(z-z_bar)]   sum[(z-z_bar)^2]        )
	  C3DPoint eigenValues;    
   
	  //! 3 x 3 Matrix whose rows are the eigenvalues of the above matrix;
	  //! Describes a rotation from the world coordinate system to a system with the 
	  //! X and Y axes parallel to the plane
	  Matrix eigenVectorRot;   
   
     //! The 4 x 4 variance-covariance matrix of the homogeneous plane parameters. 
     //! The homogeneous representation is (notation using variable names inherited from C3DPlane):
	 //! (this->normal.x  this->normal.y  this->normal.z  -this->planeConstant)^t
     //! this->qxx is ordered in the same way.
	  SymmetricMatrix qxx;
   
	  //! number of points used to determine the plane parameters
	  unsigned int nPoints;                     

	  //! sigma0 of adjustment (-1.0 if data is not adjusted)
	  double sigma0;
   
	  //! redundancy of adjustment (0 if data is not adjusted)
	  double redundancy;       

	  //! current quantil of the normal distribution 
	  static float quantil;      


	  //! current quantiles of the Chi square distribution;
	  //! the index to the array is the degrees of freedom
	  static float chiSquareQuantil[MAXDOF];

//============================================================================

  public:  

//============================================================================

	  enum EadjustMode{ eXYpara, eYZpara, eZXpara, e3d };  

//============================================================================
//=============================== Constructors ===============================

	  //! initialise plane by (1,0,0,0)
	  C3DAdjustPlane(); 

	  //! copy constructor
	  C3DAdjustPlane(const C3DAdjustPlane &plane);

	  //! initialise plane with normal n and point p
	  C3DAdjustPlane(const C3DPoint &p, const C3DPoint &n, C3DPoint *pReduction = 0);


	  //! initialise vertical plane through line2D;  z0 will be the height of this->point 
	  C3DAdjustPlane(const CXYLineSegment &line2D, double z0);

//============================================================================
//================================ Query Data ================================

	  //! Get plane parameters as homogeneous 4 x 1 vector of the plane parameters:
	  // (this->normal.x  this->normal.y  this->normal.z  -this->planeConstant)^t
	  ColumnVector getAsVector() const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - - - - -  Query data members - - - - - - - - - - - - - -


	  double getRedundancy()    const { return this->redundancy; };


	  double getVtPv()          const { return this->sigma0 * this->sigma0 * this->redundancy; }


	  double getSigma0()        const { return this->sigma0; }

	  C3DPoint getReduction()    const { return this->reduction; };


	  unsigned int getNPoints() const { return nPoints; }

	  //!projection of p onto *this
	  virtual C3DPoint getBasePoint(const C3DPoint &p) const;
  
	  //!distance from p to *this; positive, if p over *this (looking against the direction of the normal vector)  
	  virtual double getOrientedDistance (const C3DPoint &p) const;

	  double getSigmaDist(const C3DPoint &p)  const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - - Query Eigenvalues/Eigenvectors  - - - - - - - - - - -

   	  C3DPoint getEigenValues() const { return this->eigenValues; };

	  Matrix getEigenVectorRot() const { return this->eigenVectorRot; };

	  C3DPoint getEigenVector1() const 
	  {
		  return C3DPoint(this->eigenVectorRot.element(0,0),
	                      this->eigenVectorRot.element(1,0),
						  this->eigenVectorRot.element(2,0)); 
	  };

	  C3DPoint getEigenVector2() const 
	  {
		  return C3DPoint(this->eigenVectorRot.element(0,1),
	                      this->eigenVectorRot.element(1,1),
						  this->eigenVectorRot.element(2,1)); 
	  };

	  C3DPoint getEigenVector3() const 
	  {
		  return C3DPoint(this->eigenVectorRot.element(0,2),
	                      this->eigenVectorRot.element(1,2),
						  this->eigenVectorRot.element(2,2)); 
	  };
	  
	  //!return height at (p.x, p.y)
	  virtual double getHeight(const C2DPoint &p) const;

	  //!return height at (x, y)
	  virtual double getHeight(double x, double y) const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  //! Get a 4x4 homogeneous rotation matrix for planes corresponding to
	  //! this->eigenVectorRot.
	  Matrix getRot() const;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  double getCollinearityMeasure() const;
   // measure for point collinearity; result is within [0..1.0]:
   // (eigenValues.z - eigenValues.y)/(eigenValues.z + eigenValues.y)
   // returns -1.0 if all eigenvalues are 0
  
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - Query construction matrices / Jacobian matrix - - - - - - - -
//
// For definitions, see 
//   Stefan Heuel, S., 2004. Uncertain Projective Geometry.
//   Statistical Reasoning for Polyhedral Object Reconstruction. 
//   Springer-Verlag, Berlin Heidelberg, Germany.

	  Matrix getJacobi() const;

	  Matrix getPi() const;

	  Matrix getPiBar() const;

	  Matrix getSNormal() const;

// sNormal is the matrix S(normal), i.e. a matrix for computing the 
// cross-product of normal with another 3D vector 
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - Query variance-covariance matrix  - - - - - - - - - - -

	  SymmetricMatrix getQxxRot() const { return this->qxx; };

	  SymmetricMatrix getQxx(C3DPoint p = C3DPoint(0.0, 0.0, 0.0)) const;

// Return the variance-covariance relative to p
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//============================================================================
//========================== Query / set quantiles ===========================

	  static float getQuantil() { return quantil; };

	  static float getChiSquareQuantil (unsigned long degreesOfFreedom)       
	  { return chiSquareQuantil[degreesOfFreedom]; };
   
	  static void setQuantil (float percentage);

	  static void setChiSquareQuantils(float percentage);

// If_percentage is the security percentage, i.e. S = 1 - alpha; by default,
// S = 95%
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//============================================================================

	  CCharString getDumpString() const;

//============================================================================
//==== Normalise the plane, i.e. convert it to its Eucledean representation ==

	  void normalise();

//============================================================================

	  //! Initialise the plane and its covariance matrix from three points
	  void initFromPoints(const CXYZPoint &P1, const CXYZPoint &P2, 
		                  const CXYZPoint &P3, C3DPoint *pReduction);


//============================================================================
//======= Initialise the var-covar matrix from eigenvalues/eigenvectors ======

	  void initQxx();


//============================================================================
// reset all sums (d_xx_, ... d_w_) and u_n_ to 0.0, d_simgma0_ to -1.0

	  void resetSums(const C3DPoint &reductionPoint);    


//============================================================================
// reset all sums (d_xx_, ... d_w_) and u_n_ to 0.0, d_simgma0_ to -1.0

	  void copySums(const C3DAdjustPlane &IrC_other);    


//============================================================================
// set data points. The call of v_setPoints adds the products (x*x, x*y, ...)
// to the current values of these variables. If no weihgts are given, each 
// point is given the weight 1.

	  void addPoints(unsigned int num, C3DPoint* pPoints, double *pWeights = NULL);   

	  void addPoints(const vector<C3DPoint> &pointVect, const vector<double> &pVect);  

//============================================================================
// add products (x*x, x*y, ...) of one point to the sums of the poroducts
// usage: call v_addPoint many times, then later d_adjust to compute plane
   
	  void addPoint(const C3DPoint& p, const double w);

	  void addPoint(const C3DPoint& p);


//============================================================================
// add one plane to the other -> sums (d_xx_, ...) are added to each other
// behaves like addPoint 

	  C3DAdjustPlane operator+= (const C3DAdjustPlane& plane);

//============================================================================
// merge *this with another plane

	  void mergeWithPlane (const C3DAdjustPlane& plane);


//============================================================================
// adjust plane, determine location and normal vector. return value: 
// ret.val.: sigma0 of adjustment;
// sigma0 < 0.0 indicates collinearity of the points' projection to XY-plane

	  double adjust(EadjustMode Ie_mode = e3d);

//============================================================================
// adjust plane with given normal vector, only the location (offset from
// origin) is free. return values like in d_adjust(EadjustMode )

	  double adjustLocation(const C3DPoint& givenNormal);


//============================================================================
// output all data elements: (not sigma, not point, not normal)
  
	  friend ostream& operator<< (ostream& s, const C3DAdjustPlane &plane);

//============================================================================
//======= Methods for computing the test metric for the chi-square test ======

	  static void getReducedVector(const Matrix &u1,   const Matrix &u2,
                                   const ColumnVector  &entity1, 
                                   const ColumnVector  &entity2, 
                                   const Matrix &qxx1, const Matrix &qxx2,
                                   int rank, ColumnVector &dist, Matrix &qdd);

// Auxiliary method for the reduction fo vectors according to Heuel (2004).
//
// entity1, entity2: homogeneous vectors of the two geometric entities that 
//                   shall be tested for some geometric relation
// qxx1, qxx2:       the variance-covariance matrices of entity1 and
//                   entity2, respectively
// u1, u2:           two construction matrices, appropriate for the test
// rank:             degrees of freedom of the test
// 
// If the distance vector d = u1 * entity2 = u2 * entity1 has a dimensionality 
// larger than rank, u1 and u2 have to be reduced consistently to Ii_rank lines. 
//
// dist is the reduced distance vector d' with Ii_rank lines
// qdd is the rank * rank variance-covariance matrix Qdd' of dist
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	  static double testMetric(const Matrix &u1,   const Matrix &u2,
                               const ColumnVector &entity1, 
                               const ColumnVector &entity2, 
                               const Matrix &qxx1, const Matrix &qxx2,
                               int rank);

// entity1, entity2: homogeneous vectors of the two geometric entities that shall
//                   be tested for some geometric relation
// qxx1, qxx2:       the variance-covariance matrices of entity1 and 
//                   entity2, respectively
// u1, u2:           two construction matrices, appropriate for the test
// rank:             degrees of freedom of the test
// 
// The method computes the reduced distance vector d' and its variance-
// covariance matrix Qdd' (cf v_getReducedVector(..))
// The method returns the test metric t = d'^t * Qdd'^-1 * d'
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//============================================================================  
//======================= Check identity of two planes =======================

	  float isIdentical(const C3DAdjustPlane &plane) const;

// Check whether *this is identical to plane by statistical reasoning.
// The ratio r of the test metric t (cf. testMetric) and the quantil of the
// chi square distribution is computed, thus
//      r = t / chiSquareQuantil(3)
//
// return value:  r if r <= 1, thus if the two planes are identical
//               -r if r >  1, thus if the planes are not identical
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//============================================================================  
//===================== Check coplanarity of two planes ======================

	  float isCoplanar(const C3DAdjustPlane &plane, float &combinedS0, 
		               double sigmaMin) const;

// Check whether *this is identical to plane by an F-test of the RMS error of
// combined model vs. the separate model.
// However, if the rms error of the combined model is smaller than sigmaMin,
// coplanarity will be assumed anyway. 
// The ratio r of the test metric t (cf. testMetric) and the quantil of the
// F distribution is computed, thus
//      r = t / chiSquareQuantil(3)
//
// return value:  r if r <= 1, thus if the two planes are coplanar
//               -r if r >  1, thus if the planes are not coplanar
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//============================================================================  
//=====================  Check parallelity of two planes =====================

	  float isParallel(const C3DAdjustPlane &plane) const;


//============================================================================  
//==================== Check incidence of plane and point ====================

	  float isIncident(const CXYZPoint &p) const;


//============================================================================
//=========== Auxiliary methods for handling homogeneous 3D points ===========

	  //! get homogeneous point vector for 3D: (p.x, p.y, p.z, 1)
	  static ColumnVector getVector3D(const C3DPoint &p);

	  //! get homogeneous point vector for 2D: (p.x, p.y, 1)
	  static ColumnVector getVector2D(const C3DPoint &p);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - -  Construction matrices for points - - - - - - - - - - -

	  static void normalisePoint(CXYZPoint &p, double homogeneous, Matrix qxxHomog);

// Convert the homogeneous point 
// (p.x, p.y, p.z, homogeneous) with its covariance matrix qxxHomog
// into its Euclidean representation.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - -  Construction matrices for points - - - - - - - - - - -

	  static Matrix getPi(const C3DPoint &p);
   
	  static Matrix getPiBar(const C3DPoint &p);
   
	  static Matrix getS(const C3DPoint &p);

	  static Matrix getQxxHomogeneous(const CXYZPoint &p);


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - -  Statistical tests for identity of points - - - - - - - - -

	  static float areIdenticalPoints3D(const CXYZPoint &p1, 
                                        const CXYZPoint &p2);


	  static float areIdenticalPoints2D(CXYZPoint p1,  CXYZPoint p2);

// p1, p2: two points in 2D/3D
// The ratio r of the test metric t (cf. testMetric) and the quantil of the
// chi square distribution is computed, thus with N = dimension of IrC_pos1:
//      r = t / chiSquareQuantil(N)
//
// return value:  r if r <= 1, thus if the two points are identical
//               -r if r >  1, thus if the points are not identical
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


//============================================================================

	  //! Auxiliary method for type conversion of matrices
	  //! mat: general matrix, but elements are symmetric (not checked)
	  //! matsym: mat converted into asymmetric one
	  static void copyMatrixToSymmetricMatrix (const Matrix &mat, 
		                                       SymmetricMatrix &matsym);

//============================================================================

  protected:

//============================================================================

	  double adjustXY();   
	  double adjustYZ();   
	  double adjustZX();   
	  double adjust3d();   

//============================================================================  

	  //! reduce the sums to the center of gravity + set point to CoG
	  void reduceSum();   

	  //! make reduction undone + set point to origin (0,0,0)
	  void deReduceSum();   

//============================================================================

	  //! what to do if the system is not solvable  
	  void handleNoSolution()
	  { 
		  this->normal.x = this->normal.y = this->normal.z = 0.0;
		  this->sigma0 = -1.0; 
	  }  


//============================================================================
 }; 

#endif
