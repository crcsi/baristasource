#ifdef WIN32

#ifndef __CBuildingArray__
#define __CBuildingArray__


#include "ReferenceSystem.h"
#include "math.h"

#include "Building.h"
#include "BuildingPtrArray.h"
#include "MUObjectArray.h"
#include "UpdateHandler.h"
#include "Filename.h"
#include "TextFile.h"
#include "CharString.h"
#include "CharStringArray.h"
#include "DisplaySettings.h"

class CBuildingArray : public CMUObjectArray<CBuilding, CBuildingPtrArray>, public CSerializable, public CUpdateHandler
{
public:
	CBuildingArray(int nGrowBy = 0);
    CBuildingArray(CBuildingArray* src) {assert(false);};
    CBuildingArray(const CBuildingArray& src) {assert(false);};
	~CBuildingArray(void);


    bool writeBuildingTextFile(CFileName cfilename);
    bool writeBuildingVRMLFile(CFileName cfilename, int version = 2);

	bool writeBuildingDXFFile(CFileName cfilename, bool relativeHeights);

    CFileName* getFileName();
    void setFileName(const char* filename);
    CFileName  getItemText();

    bool isa(string& className) const
	{
		if (className == "CBuildingArray")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CBuildingArray");
	};

	CBuilding* Add();
	CBuilding* Add(const CBuilding& newElement);

	virtual void Remove(CBuilding* pElement);
	virtual void RemoveAt(int nIndex, int nCount= 1);
	virtual void RemoveAll();

	void selectElement(CLabel& element,bool select,bool forwardIndex=true);

	void unselectAll(bool forwardIndex=true);

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    bool isModified();

    CBuilding* getBuilding(CCharString Label);
    CBuilding* getBuilding(int i);
    
    CReferenceSystem* getReferenceSystem()
    {
        return &this->refSystem;
    };

    void setReferenceSystem(const CReferenceSystem &refsys)
    {
        refSystem = refsys;
    };

	C3DPoint getCentre() const;

	CDisplaySettings* getDisplaySettings() { return &this->displaySettings;};

protected:
    CFileName FileName;
    CReferenceSystem refSystem;
	CDisplaySettings displaySettings;

    bool writeBuildingVRML1File(CFileName cfilename, const C3DPoint &reduction, CReferenceSystem *refsys);

    bool writeBuildingVRML2File(CFileName cfilename, const C3DPoint &reduction, CReferenceSystem *refsys);


};

#endif

#endif // WIN32

