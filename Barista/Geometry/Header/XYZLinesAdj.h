//============================================================================

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      IPI, University of Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

/*!
 * \brief
 * Class CXYZHomoSegment, describing 2D and 3D 
 * segments with homogeneous line representation
 */

#ifndef ___XYZLINESADJ_H___
#define ___XYZLINESADJ_H___


#include "newmat.h" 
#include "XYLineSegment.h"
#include "XYZLines.h"
#include "XYPoint.h"
#include "XYZPoint.h"
#include "XYPolyLine.h"


//*****************************************************************************
//**************************** Forward Declarations ***************************

class C3DAdjustPlane;

//*****************************************************************************
/*!
 * \brief
 * A 3D segment with a homogeneous representation of the straight line, 
 * covariance matrices of both the homogeneous line parameters and the 
 * end points, and methods for statistical geometric reasoning.
 */
//*****************************************************************************

class CXYZHomoSegment : public CXYZSegment
{

  protected:

//=============================================================================
//===================================  data ===================================

	  //! Euclidean part of the straight line; thus, the homogeneous 
	  //! representation of the line is (with direction inherited from CXYZSegment):
	  //! (direction.x, direction.y, direction.z, euclidean.x, euclidean.y, euclidean.z)^t
  	  C3DPoint euclidean;   

	  //! covariance matrix of the homogeneous vector, in the order of the 
	  //! homogeneous vector (see above)
	  SymmetricMatrix qxx;  

	  //! covariance matrix of the endpoint point1
	  SymmetricMatrix qxx1; 

	  //! covariance matrix of the endpoint point2
	  SymmetricMatrix qxx2; 


//=============================================================================

  public:

//=============================================================================
//============================= Constructors ==================================

	  //! initialise straight line by (1,0,0,0,0,0)
	  CXYZHomoSegment();

	  //! initialise segment from p1 to p2; qxx is initialised by zeroes	  
	  CXYZHomoSegment(const C3DPoint &p1, const C3DPoint &p2);

	  //! initialise segment from p1 to p2; qxx is initialised from the 
	  //! covariance matrices of p1 and p2
	  CXYZHomoSegment(const CXYZPoint &p1, const CXYZPoint &p2);

	  //! copy constructor; the covariance matrices one will be initialised by zeroes
	  CXYZHomoSegment(const CXYZSegment &segment );

	  //! copy constructor
	  CXYZHomoSegment(const CXYZHomoSegment &segment );


	  //! Constructed a segment as the intersection of plane1 and plane2.
	  //! The endpoints will be the projections of p1 and p2 to the 
	  //! intersection line.
	  CXYZHomoSegment(const C3DAdjustPlane &plane1, const C3DAdjustPlane &plane2,
                      const C3DPoint &p1,           const C3DPoint &p2);

//=============================================================================

	  //!destructor 
	  virtual ~CXYZHomoSegment();

//=============================================================================
//================================ Query Data ================================


	  //! Get line parameters as the homogeneous 6 x 1 vector of the line parameters:
	  //! (direction.x, direction.y, direction.z, euclidean.x, euclidean.y, euclidean.z)^t
	  ColumnVector getAsVector() const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - - - - -  Query data members - - - - - - - - - - - - - -
   
	  C3DPoint getEuclidean() const { return this->euclidean; };

	  SymmetricMatrix getQxxCopy() const { return this->qxx; };
	  
	  const SymmetricMatrix &getQxx() const { this->qxx; };
   
	  SymmetricMatrix getQxx1Copy() const { return this->qxx1; };
   
	  const SymmetricMatrix getQxx1() const { return this->qxx1; };

	  SymmetricMatrix getQxx2Copy() const { return this->qxx2; };

	  const SymmetricMatrix &getQxx2() const { return this->qxx2; };

	  //! get the covariance relative to p
	  SymmetricMatrix getQxx(const C3DPoint &p = C3DPoint(0.0f,0.0f,0.0f)) const;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - Query construction matrix / Jacobian matrix - - - - - - - -
//
// For definitions, see 
//   Stefan Heuel, S., 2004. Uncertain Projective Geometry.
//   Statistical Reasoning for Polyhedral Object Reconstruction. 
//   Springer-Verlag, Berlin Heidelberg, Germany.
 
	  Matrix getJacobi() const;

	  Matrix getGamma() const;

	  Matrix getGammaBar() const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  float isIdenticalStraightLine(const CXYZHomoSegment &seg) const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  //! Get the projection of p to the straight line, with error propagation.
	  CXYZPoint getProjectedPoint(const CXYZPoint &p) const;


//=============================================================================

	  //! Multiply the direction vector by -1 
	  void reverse();

	  //!Normalise the line, i.e. convert it to its Eucledean representation 
	  void normalise();

	  //! Cut off the segment at the projections of p1 and p2 to the straight line.
	  //! The straight line will remain as it is. The segment will be cut off at
	  void cutOff(const C3DPoint &p1, const C3DPoint &p2);


	  //! Merge *this with _segment, weighted by the segment lengths 
	  void mergeWithSegment(const CXYZSegment &segment);

//=============================================================================

	  //! Compute the intersection point Pint of the straight line (!) 
	  //! corresponding to *this with a plane. 
	  //! return value: eParallel     if the line does not intersect the plane (here: 
	  //!                             homogeneous part of intersection < FLT_EPSILON;
	  //!                             In this case, Pint must not be used further
	  //!               eIntersection if the line and the plane intersect
	  eIntersect getIntersection(const C3DAdjustPlane &plane, CXYZPoint &Pint) const;

    
	  //! Compute the intersection point Pint of the straight lines (!) 
	  //! corresponding to *this and another segment.  
	  //! The intersection of two lines will be computed as the centre of the points
	  //! of minimum distance between the lines (no check for actual intersection)
	  //! return value: eIntersection
	  eIntersect getIntersection(const CXYZHomoSegment *pSegment, 
                                 CXYZPoint &Pint) const;

  
	  //! Compute the intersection point Pint of the straight lines (!) 
	  //! corresponding to *this and another segment.  
	  //! The intersection of two lines will be computed as the centre of the points
	  //! of minimum distance between the lines  (no check for actual intersection)
	  //! return value: eIntersection 
	  eIntersect getIntersection(const CXYZHomoSegment &segment, CXYZPoint &Pint) const;


//=============================================================================

	  //! Statistical tests of containment of p on segment; qxx for P is 
	  //! initialised by zeroes.
	  //! The ratio r of the test metric t (cf. C3DAdjustPlane::testMetric) 
	  //! and the quantil of the chi square distribution is computed, thus with 
	  //! N = rank of the distance measure:r = t / C_chiSquareQuantil(N)
	  //! return value:  r if r <= 1, thus p is on the straight line
	  //!               -r if r >  1, thus p is not on the straight line
	  float containsPoint(const C3DPoint &p) const;

	  //! Statistical tests of containment of p on segment.
	  //! The ratio r of the test metric t (cf. C3DAdjustPlane::testMetric) 
	  //! and the quantil of the chi square distribution is computed, thus with 
	  //! N = rank of the distance measure:r = t / C_chiSquareQuantil(N)
	  //! return value:  r if r <= 1, thus p is on the straight line
	  //!               -r if r >  1, thus p is not on the straight line
	  float containsPoint(const CXYZPoint &point) const;

//=============================================================================

	  //!Formatted output of the segment parameters
	  void print(ostream &ostr) const;

//=============================================================================

	//!dxf output of the segment as Polyline
  	bool writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
		          double xfactor = 1.0, double yfactor = 1.0, double zfactor = 1.0) const;

//=============================================================================

 protected:

//=============================================================================

	 //! Construct the straight line from two points p1 and p2
	 void constructFromPoints(const C3DPoint &p1, const C3DPoint &p2);


	 //! Initialise qxx from the covariance matrices of the end points 
	 void initQxx();

	 //! Initialise qxx and qxx2 by zeroes 
	 void initQxxEndPoints();

 };

#endif
