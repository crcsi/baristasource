#ifndef __COBSPOINT_H__
#define __COBSPOINT_H__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "PointBase.h"

#include "Label.h"
#include "Serializable.h"


class Matrix;
class CObsPointPtrArray;

#define ROBUSTBITBASE 9
#define ADDED_CHECKPOINT 1
#define CHANGED_CONTROLPOINT 2

class CObsPoint : public virtual CPointBase, public virtual CSerializable
{

protected:
	
	  CLabel label;	// the point's label
	  short status;  // a status variable. Each bit can be set/deleted individually

	  /// the point's 3x3 covariance matrix
	  /// because these points are often used in least squares adjustment
	  Matrix *covariance; // = Matrix.identity(3,3);

	CObsPoint(): CPointBase(3), label(""), status(1), covariance(0)
	{
		this->initCovar();
	};


public:
	
	friend class CMUObjectArray<CObsPoint, CObsPointPtrArray>; // CMUObjectArray is a template class

	CObsPoint(int dimension): CPointBase(dimension), label(""), status(1), covariance(0)
	{
		this->initCovar();
	};

	CObsPoint(int dimension, CLabel label);
	CObsPoint(int dimension, CLabel label, int status);
	CObsPoint(const CObsPoint &point): CPointBase(point), label(point.label), status(point.status), covariance(0)
	{
		this->setCovariance(*point.getCovariance()); 
	};

	CObsPoint(const CObsPoint *point);

	virtual ~CObsPoint(void);
	
	// get functions
	virtual short getStatus() const { return this->status; };
	virtual int getStatusBit(unsigned char bit) const;
	virtual CLabel getLabel() const { return this->label; };
	virtual Matrix* getCovariance() const { return this->covariance; };
	virtual const Matrix &getCovar() const { return *(this->covariance); };
	virtual const double getCovarElement(int row, int col) const;
	virtual const double getSigma(int index) const;

	// set functions
	virtual void setLabel (const CLabel label) {this->label = label;};
	virtual void setCovariance (const Matrix &covariance);	
	virtual void setStatus(short Status) { this->status= Status;};
	virtual void setCovarElement(int row, int col, double value);

	virtual void activateStatusBit(unsigned char bit);
	virtual void deactivateStatusBit(unsigned char bit);

	virtual double distanceFrom (const CObsPoint* point) const;
	virtual double distanceFrom (const CObsPoint &point) const;


	// operators
	virtual CObsPoint &operator = (const CObsPoint & point);
	virtual CObsPoint *operator = (const CObsPoint * point);
	
	// unary minus operator
	CObsPoint operator - () const;

	// addition and subtraction of points 
	virtual void operator += ( const CObsPoint &p );

	virtual void operator -= ( const CObsPoint &p );

	friend CObsPoint operator + ( const CObsPoint &p1, const CObsPoint &p2 );

	friend CObsPoint operator - ( const CObsPoint &p1, const CObsPoint &p2 );

	// multiplication and division of points by scalars	
	virtual void operator *= ( double a );

	CObsPoint operator * ( double a ) const;

	friend CObsPoint operator * ( double a, const CObsPoint &p );

	virtual void operator /= ( double a );

	virtual CObsPoint operator / ( double a ) const;


	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();
	
	virtual bool isa(string& className) const
	{
		if (className == "CObsPoint")
			return true;
		return false;
	};

	virtual string getClassName() const {return string("CObsPoint"); };

protected:

	void initCovar();
};


inline int CObsPoint::getStatusBit(unsigned char bit) const
{
	return ( this->status & (1 << bit) );
};

inline void CObsPoint::activateStatusBit(unsigned char bit)
{
	this->status |= ( 1 << bit );
};

inline void CObsPoint::deactivateStatusBit (unsigned char bit)
{
    this->status &= ~( 1 << bit );
};

#endif
