#ifndef __CXYZPointArrayArray__
#define __CXYZPointArrayArray__

#include "XYZPointArray.h"
#include "XYZPointArrayPtrArray.h"

class CXYZPointArrayArray : public CMUObjectArray<CXYZPointArray, CXYZPointArrayPtrArray>, public CSerializable
{
public:
  	CXYZPointArrayArray(int nGrowBy = 0);
    ~CXYZPointArrayArray(void);

    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    CXYZPointArray* getPointArray(CCharString name) const;
    CXYZPointArray* getPointArray(int i) const;
    int getbyName(CCharString name);
    bool isModified();

    string getClassName() const
    {
        return string("CXYZPointArrayArray");
    };

	bool isa (string& className) const
	{
		if (className == "CXYZPointArrayArray")
			return true;

		return false;
	};
};
#endif

