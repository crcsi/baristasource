#ifndef __CSpheroid__
#define __CSpheroid__

#include "Serializable.h"


#define WGS84_NAME "WGS 1984"
#define WGS84_A 6378137
#define WGS84_F 298.257223563

class CSpheroid : public CSerializable 
{
  private:
  
	  char    *name; 

	  double  a; 

	  double  b; 

	  double  e; 

	  double  f; 

	  double Eccentricity2; 

  public:
  
	  CSpheroid();

	  CSpheroid(const char *I_name, double semimajor, double inverseFlattening);

	  CSpheroid(const CSpheroid &spheroid);

	  ~CSpheroid();

	  CSpheroid &operator = (const CSpheroid &other);

	  bool operator == (const CSpheroid &other);
	  bool operator != (const CSpheroid &other) { return !(*this == other); };

	  const char *getName() const { return this->name; };

	  double getSemiMajor() const { return a; };

	  double getSemiMinor() const { return b; };

	  double getSemiAverage() const { return (a + b) * 0.5f; };

	  double getEccentricity() const { return e; };

	  double getFlattening() const { return f; };

	  double getEccentricity2() const { return Eccentricity2; };

	  void setName(const char *I_name);

	  double calcRCPrimeVertical(double lat) const;

	  double calcRCSecondVertical(double lat) const;

	  double calcRadius(double lat) const;

	  double calcFootPointLatitude(double NorthingMetric) const;

	  double calcP(double lat) const;

	  void calcEccentricity();

	  void calcEccentricity2();

	  void calcFlattening();

	  void calcSemiMinor();

	  double calcMeridianDistance(double lat) const;

	  void serializeStore(CStructuredFileSection* pStructuredFileSection);

	  void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	  void reconnectPointers();
	  void resetPointers();

	  string getClassName() const
	  {
		  return string("CSpheroid");
	  };

	  bool isa (string& className) const
	  {
		  if (className == "CSpheroid")		
			  return true;

		  return false;
	  };
    
	  bool isModified()
	  {
		  return this->bModified;
	  };
};

extern CSpheroid WGS84Spheroid;

extern CSpheroid GRS1980Spheroid;

extern CSpheroid GRS1967Spheroid;

extern CSpheroid BesselSpheroid;

extern CSpheroid AustralianNationalSpheroid;

extern CSpheroid Clarke1866Spheroid;

extern CSpheroid Clarke1880Spheroid;

extern CSpheroid HayfordSpheroid;

extern CSpheroid IERSSpheroid;

#endif