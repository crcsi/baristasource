//============================================================================

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      IPI, University of Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//============================================================================

#ifndef __C3DPLANE_H__
#define __C3DPLANE_H__

#include "XYZLines.h"
#include "3DPoint.h"
#include "2DPoint.h"

/*!
 * \brief
 * A 3D plane with methods for computing distances
 */

class  C3DPlane: public CXYZLineBase
{
	protected:

		//! one point in the plane
		C3DPoint point;     

		//! normal vector of the plane (orientated plane) this vector is a unit vector
		C3DPoint normal;   

		//! plane constant for computing distances of points
		double planeConstant; 

  public:

//=============================================================================
//=========================== constructors & destructor =======================

	  //! construct plane as XY - plane
	  C3DPlane();
  
	  //!construct with base point and normal vector
	  C3DPlane(const C3DPoint &Point, const C3DPoint &Normal);
  
  	  //!construct with 3 points (plane containing the 3 points)
	  C3DPlane(const C3DPoint &p1, const C3DPoint &p2, const C3DPoint &p3 );
  
  	  //!construct with 1 point and 1 line (plane containing the line and the point)
	  C3DPlane(const C3DPoint& IrC_point, const CXYZLine & line);


	  //! construct with ray: starting point of the ray is a point in the plane,
	  //! direction of the ray is normal vector of the plane
	  C3DPlane(const CXYZRay &ray);

	  //!copy constructor  
	  C3DPlane( const C3DPlane& IrC_plane );
  
	  ~C3DPlane() { };


//=============================================================================
//=============================== correctness of geometry =====================
 
	  virtual bool isWellDefined() const { return this->normal.getSquareNorm() > getEpsilon(); }

 
//=============================================================================
//============================= query members =================================


	  const C3DPoint &getNormal() const { return this->normal; }

	  const C3DPoint &getPoint()  const { return this->point; }

	  double getPlaneConstant()   const { return this->planeConstant; };
 
	  double getAverageHeight() const { return this->point.z; };


	  //! returns the length of the projection of the normal vector to the xy plane
	  double getNormalXYlength() const { return this->normal.getNorm2D(); };

	  //! get the tangent of the obliqueness angle
	  double getObliqueness() const;

	  //!return height at (p.x, p.y)
	  virtual double getHeight(const C2DPoint &p) const;

	  //!return height at (x, y)
	  virtual double getHeight(double x, double y) const;

	  // !Set p.z to height at (p.x, p.y)
	  void addHeight(C3DPoint &p) const;

//=============================================================================

	  //! reset data to XY axis
	  void resetPlane();

	  //!Change direction of the normal angle
	  void reverseNormal();

//=============================================================================
//================================== copy data ================================
	  
	  //! copy data from plane to *this
	  void copyPlane(const C3DPlane &plane);
 

//=============================================================================
//=========================== return parameter from of plane ==================
  
	//! generate a parameter description of the plane: 
	//! x(u,v)=Point+u*vec1+v*vec2
	//! if normal=0-vector, return 0, 1 otherwise. Point=point,
	//! vec1.cross_prod(vec2) = normal, XrC_vecN are normalized
	  int generateParamDesc(C3DPoint &Point, C3DPoint &vec1, C3DPoint &vec2);

//=============================================================================
//=========================== relation to point ===============================

	  //!projection of p onto *this
	  virtual C3DPoint getBasePoint(const C3DPoint &p) const;
  
	  //!distance from p to *this; positive, if p over *this (looking against the direction of the normal vector)  
	  virtual double getOrientedDistance (const C3DPoint &p) const;

  
	  double getDistance(const C3DPoint &p) const { return fabs(this->getOrientedDistance(p)); };

	  virtual bool containsPoint(const C3DPoint &p) const { return (this->getDistance(p) < getEpsilon()); }


//=============================================================================
//========================== relation to line =================================

	  virtual bool isParallel(const CXYZLine &line) const;

	  bool isParallel(const CXYZRay &ray) const { return isParallel(CXYZLine(ray)); }
	  bool isParallel(const CXYZSegment &segment) const { return isParallel(CXYZLine(segment)); }
	  // return value: 1: *this and the input line are parallel
	  //                  (considering epsilon)
	  //               0: *this and the input line are not parallel
	  //                  (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 
	  virtual eIntersect intersect(const CXYZLine &line) const;
	  virtual eIntersect intersect(const CXYZRay &ray) const;
	  virtual eIntersect intersect(const CXYZSegment &segment) const;
	  //!return value: eIntersecttion:   *this and the input line intersect
	  //!                               (considering epsilon).
	  //!              eIncluded:       *this and the input line are identical
	  //!                               (considering epsilon)
	  //!              eParallel:       *this and the input line are parallel
	  //!                               (considering epsilon)
	  //!              eNoIntersection: *this and the input line do not intersect
	  //!                               (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  virtual eIntersect getIntersection(const CXYZLine &line, C3DPoint &p) const;
	  virtual eIntersect getIntersection(const CXYZRay &ray, C3DPoint &p) const;
	  virtual eIntersect getIntersection(const CXYZSegment &segment, C3DPoint &p) const;
	  //!return value: eIntersecttion:   *this and the input line intersect
	  //!                               (considering epsilon). Only in this case
	  //!                               p contains the co-ordinates of the
	  //!                               intersection point
	  //!              eIncluded:       *this and the input line are identical
	  //!                               (considering epsilon)
	  //!              eParallel:       *this and the input line are parallel
	  //!                               (considering epsilon)
	  //!              eNoIntersection: *this and the input line do not intersect
	  //!                               (considering epsilon)
	  //! p = intersection point if (return value == eIntersecttion)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//=============================================================================
//========================== relation to other plane ==========================

	  virtual bool isParallel(const C3DPlane &plane) const;
	  //!return value: 1: *this and the input plane are parallel
	  //!                 (considering epsilon)
	  //!              0: *this and the input plane are not parallel
	  //!                 (considering epsilon)
	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  
	  //! Check whether this plane intersects with another plane.
	  //! return value: eIntersection:  *this and the input plane intersect
	  //!                               (considering epsilon). 
	  //!               eIncluded:      *this and the input plane identical
	  //!                               (considering epsilon)
	  //!               eParallel:      *this and the input line are parallel
	  //!                               (considering epsilon)
	  virtual eIntersect intersect(const C3DPlane &plane ) const;

	  //! Compute the intersection between *this and plane.
	  //! return value: eIntersection:  *this and the input plane intersect
	  //!               eIncluded:      *this and the input plane identical
	  //!               eParallel:      *this and the input line are parallel
	  //!
	  //! line is the intersection line, if (return value == eIntersection)
	  virtual eIntersect getIntersection(const C3DPlane &plane, CXYZLine &line) const;

	  //! Compute the intersection between *this and plane.
	  //! return value: eIntersection:  *this and the input plane intersect
	  //!               eIncluded:      *this and the input plane identical
	  //!               eParallel:      *this and the input line are parallel
	  //! 
	  //! if (return value == eIntersection), p1 and p2 will be set to the projections
	  //! of the original points p1 and p2 to the intersection line
	  virtual eIntersect getIntersection( const C3DPlane &plane, C3DPoint &p1,  C3DPoint &p2) const;

	  //! Compute the intersection between *this and plane.
	  //! return value: eIntersection:  *this and the input plane intersect
	  //!               eIncluded:      *this and the input plane identical
	  //!               eParallel:      *this and the input line are parallel
	  //! 
	  //! if (return value == eIntersection), the segment end points will be set 
	  //! to the projections of the original segment end points to the intersection line
	  virtual eIntersect getIntersection(const C3DPlane &plane, CXYZSegment &segment) const;

	  //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  protected:
  
	  //! Compute the intersection between the line (pt, dir) and *this, return 
	  //! values eIntersection, eIncluded or eParallel. In the case of eIntersection,
	  //! param is the parameter value of the intersection point along the line.
	  //! dir needs not be a unit vector
	  eIntersect intersectLine(const C3DPoint &pt, const C3DPoint &dir, double &param) const;

};

//*****************************************************************************
//!  inlines of class C3DPlane
//*****************************************************************************

inline
void C3DPlane::reverseNormal()
{
    this->normal = -this->normal;
    this->planeConstant = -this->planeConstant;
}


#endif
