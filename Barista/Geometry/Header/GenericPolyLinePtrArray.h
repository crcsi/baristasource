#ifndef __CGenericPolyLinePtrArray__
#define __CGenericPolyLinePtrArray__

#include "MUObjectPtrArray.h"
#include "GenericPolyLine.h"

class CGenericPolyLinePtrArray : public CMUObjectPtrArray<GenericPolyLine>
{
public:
	CGenericPolyLinePtrArray(int nGrowBy = 0);
	~CGenericPolyLinePtrArray(void);
};
#endif
