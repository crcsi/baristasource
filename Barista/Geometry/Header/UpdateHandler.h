#ifndef __CUpdateHandler__
#define __CUpdateHandler__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "UpdateListenerPtrArray.h"

class CUpdateHandler
{
public:
	CUpdateHandler(void){};
	CUpdateHandler(const CUpdateHandler& handler):listeners(handler.listeners){}
	~CUpdateHandler(void){};

	int getListenerCount() const { return this->listeners.GetSize();};
	CUpdateListener *getListener(int index) const;


    void addListener(CUpdateListener* listener);
    void removeListener(CUpdateListener* listener);

protected:

	virtual void informListeners_elementAdded(CLabel element="");
	virtual void informListeners_elementDeleted(CLabel element="");
	virtual void informListeners_elementsChanged(CLabel element="");
	virtual void informListeners_elementSelected(bool selected,CLabel element="",int elementIndex=-1);

private:
	CUpdateListenerPtrArray listeners;
};
#endif
