#ifndef __COBSPointPtrArray__
#define __COBSPointPtrArray__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "MUObjectPtrArray.h"
#include "ObsPoint.h"

class CObsPointPtrArray : public CMUObjectPtrArray<CObsPoint>
{
protected:
	int dim;

	void qsortByLabel(int L, int R);

	void setDim(int Dim) { this->dim = Dim; }

public:
	CObsPointPtrArray(int Dim, int nGrowBy = 0);
	CObsPointPtrArray(const CObsPointPtrArray &src);
	virtual ~CObsPointPtrArray(void);
    void sortByLabel();

	void revert();

	friend class CObsPointArray;

};
#endif