#ifndef __GenericPolyLine__
#define __GenericPolyLine__


/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */
#include "Serializable.h"
#include "Label.h"
#include "ObsPointArray.h"

//*****************************************************************************
// enum for order of polygon
//*****************************************************************************

 enum ePOLYLINEORDER { eUNDEFINEDORDER, eCLOCKWISE, eCOUNTERCLOCKWISE};

//*****************************************************************************

class GenericPolyLine : public CSerializable
{

protected:

    /// the line's label
    CLabel label;
    /// the line's points
    CObsPointArray *obsPoints;
	/// boolean telling whether the line is closed or not

	bool isclosed;

	bool isSelected;

 public:


    GenericPolyLine(void);

	~GenericPolyLine(void);

    CLabel getLabel() const;

    int getPointCount() const;

	void setSelected(bool b) {this->isSelected = b;};
	bool getIsSelected() const {return this->isSelected;};

	CObsPoint &getPoint (int index);
	const CObsPoint &getPoint (int index) const;

	double getArea() const;

    double getAreaAbs() const;

	ePOLYLINEORDER getOrder() const;

	void reversePolyLine();

    void deleteLine();

	void removePoints(int indexFrom, int indexTo);


    void removePoint(int i);
    //CXYPolyLine transform(CTrans3D trans);

    CCharString toString();

    void set(double x, double y, double z);

    void setLabel (CLabel label);

    // CSerializable functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection){};
	void serializeRestore(CStructuredFileSection* pStructuredFileSection){};

	void reconnectPointers(){};
	void resetPointers(){};
	bool isModified(){ return this->bModified;};

	bool isClosed() const { return isclosed; };
	//void setClosed (bool close) {this->isclosed = close;};
	
	virtual double getLength() const;

    string getClassName() const
    {
        return string("GenericPolyLine");
    };

	bool isa (string& className) const
	{
		if (className == "GenericPolyLine")
			return true;

		return false;
	};

  
	protected:
		
};


#endif






