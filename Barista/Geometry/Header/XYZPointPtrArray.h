#ifndef __CXYZPointPtrArray__
#define __CXYZPointPtrArray__

#include "ObsPointPtrArray.h"

class CXYZPointPtrArray : public CObsPointPtrArray
{
public:
    CXYZPointPtrArray(int nGrowBy = 0);
    ~CXYZPointPtrArray(void);

};

#endif
