#ifndef __CXYPointArrayPtrArray__
#define __CXYPointArrayPtrArray__

#include "MUObjectPtrArray.h"
#include "XYPointArray.h"

class CXYPointArrayPtrArray : public CMUObjectPtrArray<CXYPointArray>
{
public:
    CXYPointArrayPtrArray(int nGrowBy = 0);
    ~CXYPointArrayPtrArray(void);

};

#endif

