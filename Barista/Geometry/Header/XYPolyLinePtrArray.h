#ifndef __CXYPolyLinePtrArray__
#define __CXYPolyLinePtrArray__

#include "MUObjectPtrArray.h"
#include "XYPolyLine.h"


class CXYPolyLinePtrArray : public CMUObjectPtrArray<CXYPolyLine>
{
public:
    CXYPolyLinePtrArray(int nGrowBy = 0);
    ~CXYPolyLinePtrArray(void);

};

#endif
