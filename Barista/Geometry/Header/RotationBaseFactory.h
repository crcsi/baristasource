#ifndef __CRotationBaseFactory__
#define __CRotationBaseFactory__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "RotationBase.h"

class Matrix;
class CRotationBaseFactory
{
public:
	static CRotationBase* initRotation(const CPointBase* angles_rho,const RotationType type);
	static CRotationBase* initRotation(const CRotationMatrix &mat,const RotationType type);
	static CRotationBase* initRotation(const CRotationBase* rotBase);
};
#endif
