#ifndef __RollPitchYawRotation__
#define __RollPitchYawRotation__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "RotationBase.h"

/* order of the angles
	coordinates[0]	= Roll  ( Rotation around x)
	coordinates[1]	= Pitch ( Rotation around y)
	coordinates[2]	= Yaw   ( Rotation around z)
*/


class CRollPitchYawRotation : public CRotationBase
{
public:
	CRollPitchYawRotation(void): CRotationBase(3,RPY) {};
	CRollPitchYawRotation(const CPointBase* p_rho);
	CRollPitchYawRotation(const CRotationMatrix &mat);

	virtual ~CRollPitchYawRotation(void) {};

	virtual void updateAngles_Rho(const CPointBase* angles);
	virtual void updateAngles_Gon(const CPointBase* angles);
	virtual void updateRotMat(const CRotationMatrix &mat);

	virtual void computeRotMatFromParameter();
	virtual bool computeParameterFromRotMat();

	virtual void computeDerivation(CPointBase& deriv,const int parameter)const;
	virtual void computeDerivation(CAdjustMatrix& derivMat) const;

	  // serialize functions
	  virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	  virtual bool isa(string& className) const	
	  {
		  if (className == "CRollPitchYawRotation")
			  return true;
		  return false;
	  };

	  virtual string getClassName() const {return string("CRollPitchYawRotation"); };  

};

#endif
