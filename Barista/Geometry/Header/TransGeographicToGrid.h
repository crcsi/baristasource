#ifndef __CTransGeographicToGrid__
#define __CTransGeographicToGrid__

#include "Trans3D.h"

class CTransGeographicToGrid : public CTrans3D
{
  public:

	  CTransGeographicToGrid(const CReferenceSystem &sysFrom, const CReferenceSystem &sysTo);

	  ~CTransGeographicToGrid();

	  virtual void transform(C3DPoint  &out, const C3DPoint  &in);

	  virtual void transform(CXYZPoint &out, const CXYZPoint &in);

	  virtual void getDerivatives(Matrix &derivatives, const C3DPoint &p) const;

	  bool isa(const char* className) const
	  {
		  if (::strcmp("CTransGeographicToGrid", className) == 0)
			  return true;

		  return false;
	  }


 	 string getClassName()	 const 
	 {
		  return string("CTransGeographicToGrid");
	 };

};
#endif
