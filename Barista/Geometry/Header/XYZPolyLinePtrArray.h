#ifndef __CXYZPolyLinePtrArray__
#define __CXYZPolyLinePtrArray__

#include "MUObjectPtrArray.h"
#include "XYZPolyLine.h"


class CXYZPolyLinePtrArray : public CMUObjectPtrArray<CXYZPolyLine>
{
public:
    CXYZPolyLinePtrArray(int nGrowBy = 0);
    ~CXYZPolyLinePtrArray(void);

};

#endif
