#ifndef __CXYEllipseArrayPtrArray__
#define __CXYEllipseArrayPtrArray__

#include "MUObjectPtrArray.h"
#include "XYEllipseArray.h"

class CXYEllipseArrayPtrArray : public CMUObjectPtrArray<CXYEllipseArray>
{
public:
    CXYEllipseArrayPtrArray(int nGrowBy = 0);
    ~CXYEllipseArrayPtrArray(void);

};

#endif
