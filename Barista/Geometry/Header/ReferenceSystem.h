#ifndef __CReferenceSystem__
#define __CReferenceSystem__

// Coordinate systems
#define NORTHERN  0
#define SOUTHERN  1

//#include "Serializable.h"
#include "Spheroid.h"

enum eCoordinateType 
{ 
	eUndefinedCoordinate = -1,
	eGEOGRAPHIC,
	eGEOCENTRIC,
	eGRID
};

class C3DPoint;
class CXYZPoint;
class Matrix;

class CReferenceSystem : public CSerializable
{

  protected:
    
	  CSpheroid spheroid;				// Spheroid of the reference system

	  eCoordinateType coordinateType;   // Coordinate type of associated point 
										// list in this reference system

// Parameters for the TM mapping 

	  double centralMeridian;			// Central meridian of TM mapping in [deg]

	  double scale;						// Scale factor at central meridian (usually 1, 0.9996 for UTM)

	  double latitudeOrigin;			// latitude of the origin at the central meridian in [deg]
	  double latitudeOriginOffset;		// coordinate offset corresponding to latitude of the origin at the central meridian in [m]

	  double FEasting;

	  double FNorthing;

// specific parameters for UTM

	  int zone;							// UTM zone

	  int hemisphere;					// hemisphere

// flags

	  bool WGS84Flag;

	  bool UTMFlag;

	  bool hasValues;

  public:
  
	  CReferenceSystem(int I_zone = 1, int I_hemisphere = NORTHERN);  // WGS84 with UTM 

	  CReferenceSystem(double latitude, double longitude);  // WGS84 with UTM; compute zone/hemisphere from longitude/latitude

	  CReferenceSystem(double I_centralMeridian, double I_scale,
					   double I_latitudeOrigin, double I_FEasting, double I_FNorthing); // WGS84 with other TM

	  CReferenceSystem(const CSpheroid &I_spheroid, int I_zone, int I_hemisphere); // non-WGS84 with UTM

	  CReferenceSystem(const CSpheroid &I_spheroid, double latitude, double longitude);  // non-WGS84 with UTM; compute zone/hemisphere from longitude/latitude

	  CReferenceSystem(const CSpheroid &I_spheroid, double I_centralMeridian, double I_scale,
					   double I_latitudeOrigin, double I_FEasting, double I_FNorthing); // non-WGS84 with other TM


	  CReferenceSystem(const CReferenceSystem &refSys);  // copy constructor

	  ~CReferenceSystem();

	  CReferenceSystem &operator = (const CReferenceSystem &refSys);
	  
	  bool operator == (const CReferenceSystem &refSys) const;
	  bool operator != (const CReferenceSystem &refSys) const { return !(*this == refSys); };

	  CSpheroid getSpheroid() const { return this->spheroid; };

	  CCharString getCoordinateSystemName() const;

	  eCoordinateType getCoordinateType() const { return this->coordinateType; };

	  int getZone() const { return this->zone; };

	  int getHemisphere() const { return this->hemisphere; };

	  double getScale() const { return this->scale; };

	  double getCentralMeridian() const { return this->centralMeridian; };

	  double getLatitudeOrigin() const { return this->latitudeOrigin; };

	  double getNorthingConstant() const { return this->FNorthing; };

	  double getEastingConstant() const { return this->FEasting; };

	  bool isWGS84() const { return this->WGS84Flag; };

	  bool isUTM() const { return this->UTMFlag; };

	  void setSpheroid(const CSpheroid &I_spheroid);

	  void setToWGS1984();

	  void setCoordinateType(eCoordinateType I_type);

	  void setUTMZone(int I_zone, int I_hemisphere);

	  void setUTMZone(double latitude, double longitude);

	  void setTMParameters(double I_centralMeridian, double I_FEasting = 0.0, 
						   double I_FNorthing = 0.0, double I_scale = 1.0, 
						   double I_latitudeOrigin = 0.0);

	  static int calcUTMZone(double longitude);

	  static double calcUTMCentralMeridian(int Zone);

	  void geographicToGrid(C3DPoint &out, const C3DPoint &in) const;

	  void geographicToGrid(CXYZPoint &out, const CXYZPoint &in) const;

	  void gridToGeographic(C3DPoint &out, const C3DPoint &in) const;

	  void gridToGeographic(CXYZPoint &out, const CXYZPoint &in) const;

	  void geographicToGeocentric(C3DPoint &out, const C3DPoint &in) const;

	  void geographicToGeocentric(CXYZPoint &out, const CXYZPoint &in) const;

	  void geocentricToGeographic(C3DPoint &out, const C3DPoint &in) const;

	  void geocentricToGeographic(CXYZPoint &out, const CXYZPoint &in) const;

	  void gridToGeocentric(C3DPoint &out, const C3DPoint &in) const;

	  void gridToGeocentric(CXYZPoint &out, const CXYZPoint &in) const;

	  void geocentricToGrid(C3DPoint &out, const C3DPoint &in) const;

	  void geocentricToGrid(CXYZPoint &out, const CXYZPoint &in) const;

	
	  void derivativesGeocentricToGeographic(Matrix &derivatives, const C3DPoint &geocentric) const;
	  void derivativesGeographicToGeocentric(Matrix &derivatives, const C3DPoint &geographic) const;
	  void derivativesGeocentricToGrid(Matrix &derivatives, const C3DPoint &geocentric) const;
	  void derivativesGridToGeocentric(Matrix &derivatives, const C3DPoint &grid) const;
	  void derivativesGeographicToGrid(Matrix &derivatives, const C3DPoint &geographic) const;
	  void derivativesGridToGeographic(Matrix &derivatives, const C3DPoint &grid) const;

	  void serializeStore(CStructuredFileSection* pStructuredFileSection);

	  void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	  void reconnectPointers();
	  void resetPointers();

	  string getClassName() const
	  {
		  return string("CReferenceSystem");
	  };

	  bool isa (string& className) const
	  {
		  if (className == "CReferenceSystem")		
			  return true;

		  return false;
	  };
    
	  bool isModified()
	  {
		  return this->bModified;
	  };

    bool gethasValues() const { return this->hasValues;}
	void sethasValues(bool hasvalues){ this->hasValues = hasvalues;}

  protected: 

	void computeLatitudeOffset();
};

#endif

