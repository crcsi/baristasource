#ifndef __CTrans2D__
#define __CTrans2D__

#include "2DPoint.h"
#include "XYPointArray.h"
#include "XYZPointArray.h"
#include "ZObject.h"

class SymmetricMatrix;
class Matrix;
class CtVector;
class CNormalEquationMatrix;

class CTrans2D : public ZObject
{
  protected:

	  SymmetricMatrix *covar;

  public:
  
	  CTrans2D();

	  ~CTrans2D();


	  SymmetricMatrix *getCovar() const { return this->covar; };

	  virtual CTrans2D *clone() const = 0;

  	  virtual C2DPoint getP0() const { return C2DPoint(0,0); };

	  virtual void setP0(const C2DPoint &p0) {};

	  virtual int nParameters() const { return 0; };

	  virtual CTrans2D &operator = (const CTrans2D &trans){return *this;};

	  virtual void getParameter(vector<double> &parameter) const { parameter.resize(0);};
	  /**
	  * Transforms the point XY
	  * @param XY the Point to be transformed
	  * @return the transformed point
	  */
	  virtual void transform (CXYZPoint &out, const CXYPoint &in) const;

	  virtual void transform (CXYPoint &out, const CXYPoint &in) const;

	  virtual void transform (C2DPoint &out, const C2DPoint &in) const;
	  
	  virtual void applyFactorToBoth (double fact, Matrix &covar) { };

	  virtual void applyLinearTransformation(double factLeft, double factRight, 
		                                     C2DPoint &shiftLeft, const C2DPoint &shiftRight, 
											 Matrix &covar) { };

	  virtual CCharString getDescriptor(const CCharString &lead, int shiftDigits, int matrixDigits, 
		                                const CCharString &descrFrom = "_1", 
										const CCharString &descrTo = "_2") const;

	  void transformWindow(int  offsetX,  int  offsetY,  int  width,  int  height,
		                   int &offsetXt, int &offsetYt, int &widtht, int &heightt,
						   int maxWidth,  int maxHeight, int marginX, int marginY) const;

	  void transform (CXYZPointArray* out, CXYPointArray* in) const 
	  {
		  out->RemoveAll();
		  for (int i = 0; i < in->GetSize(); i++)
		  {
			  CXYZPoint* newPoint = out->add();
			  this->transform(*newPoint, *(in->getAt(i)));
		  };
	  }
	 

	  virtual Matrix getProjectionMatrix() const = 0;

	  virtual void get(Matrix &parms, C2DPoint &p0) const = 0;

	  virtual int getWeightForDirectObs(Matrix &weights, double sigma0, double maxInfluence, double maxDist) const = 0;

	  virtual void set(const Matrix &parms, const C2DPoint &p0) = 0;

	  virtual bool invert();

	  virtual void applyP0() = 0;

	  virtual void prepareAdjustment(CNormalEquationMatrix &N, CtVector &m, double &vtpv, unsigned long &nObs) const { };
	  
	  virtual void accumulate(const C2DPoint &p1, const C2DPoint &p2, CNormalEquationMatrix &N, 
		                      CtVector &m, double &vtpv, unsigned long &nObs) {};

	  virtual bool solve(CNormalEquationMatrix &N, const CtVector &m, 
		                 double vtpv, unsigned long nObs, double &s0) { return true; };
	  
	  virtual float getQvv(const C2DPoint &p1, Matrix &covar) const { return 1.0f; };

  	  virtual int getRed(int N) const { return (2 * N - nParameters()); };

	  virtual void changeP0(const C2DPoint &newP0) { };

	  virtual void getDerivatives(double x, double y, Matrix &Ax, Matrix &Ay) const { };
	  virtual void getDerivativesCoords(double x, double y, Matrix &B) const { };

	  virtual void addVec(const Matrix &dx, int index0) { };

	  void setCovar(const SymmetricMatrix &Qxx, int index0);

	  string getClassName() const
	  {
		  return string("CTrans2D");
	  };

	  bool isa (string& className) const
	  {
		  if (className == "CTrans2D")
			  return true;

		  return false;
	  };
};
#endif
