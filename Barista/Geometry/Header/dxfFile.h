#ifndef PH_DXF_HPP
#define PH_DXF_HPP

#include <fstream>
using namespace std;


#include "Filename.h"
#include "3DPoint.h"
#include "Label.h"
#include "XYZPointArray.h"
#include "XYZPolyLineArray.h"

//****************************************************************************

class CDXFReader
{  
  protected:

	  CLabel firstLabel;

	  CFileName dxfFileName;

	  ifstream *pDxfFile;


  public:

	  CDXFReader (const CLabel &lab, const CFileName &filename = CCharString(""));

	  virtual ~CDXFReader ();

	  bool isOpen() const;

	  CCharString getFilename() const { return dxfFileName; };

	  virtual void clear();

	  bool open();

	  void closeFile();

	  bool setFileName(const CFileName &filename);

	  bool readDXF(unsigned long &nVerts, unsigned long &nLines, unsigned long &nPolys,
		           unsigned long &n3dFaces, bool  addFacesFlag);

  	  CXYZPointArray vertices;

	  CXYZPolyLineArray polylines;

//============================================================================

 protected:

//============================================================================

	 virtual bool addVertex(const C3DPoint &vertex);

	 virtual bool addLine(const C3DPoint &v1, const C3DPoint &v2, int color);

	 virtual bool addPolyline(const CXYZPolyLine &poly, int color);

	 virtual bool addFace(const CXYZPolyLine &poly);

//============================================================================

	 bool getDoubleFromString(const CCharString &str, double &d);

	 bool readNextCodeAndAttribute(int &code, CCharString &attribute);


	 char *getLine(ifstream &IStream);


	 void auxExtrusionTrafo(C3DPoint &axisX, C3DPoint &axisY, C3DPoint &axisZ);


	 C3DPoint auxExtrusionTrafo(const C3DPoint &axisX, const C3DPoint &axisY,
                                const C3DPoint &axisZ, const C3DPoint &P);

//============================================================================

	 bool readVertex(C3DPoint &v);

	 bool readLine(C3DPoint &v1, C3DPoint &v2, int &color);

	 bool readPolyLine(CXYZPolyLine &poly, int &color);

	 bool read3DFace(CXYZPolyLine &poly, int &color);

//============================================================================

};

//============================================================================

#endif
