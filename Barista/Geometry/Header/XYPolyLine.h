#ifndef __CXYPolyLine__
#define __CXYPolyLine__


/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */
#include "2DPoint.h"
#include "XYPointArray.h"
#include "GenericPolyLine.h"

//*****************************************************************************
class CXYContour;
class CXYLineSegment;
class doubles;

class CXYPolyLine : public GenericPolyLine
{

protected:

    /// the line's points
    CXYPointArray points;

 public:

    CXYPolyLine(void);
    ~CXYPolyLine(void);
    CXYPolyLine(const CXYPolyLine &p);
	CXYPolyLine(const GenericPolyLine &p);

   void addPoint(CXYPoint newpoint);

	CXYPoint &getPoint (int index) { return *this->points.getAt(index); }
	const CXYPoint &getPoint (int index) const { return *this->points.getAt(index); }


    // CSerializable functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	void reconnectPointers();
	void resetPointers();
	bool isModified();

	void closeLine();


	bool contains(const C2DPoint &p) const;

    string getClassName() const
    {
        return string("CXYPolyLine");
    };

	bool isa (string& className) const
	{
		if (className == "CXYPolyLine")
			return true;

		return false;
	};

	void initFromContour(CXYContour *contour, double I_maxDist,
	                     double I_sigma0);

	// intersect the polyline with line at x = I_x
	// intersections contains the ordered list of y values of all intersections
	// return value: number of intersections
  
	long getIntersections(double I_x, doubles &intersections) const;

  	bool writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
		          double xfactor = 1.0, double yfactor = 1.0) const;

	protected:
		
		void approximate(CXYContour *contour,
                         const vector <unsigned char> &activityVector,
						 double I_sigma0);

		int mergeSegments(CXYContour *contour,
                          vector <unsigned long> &indicesFrom,
						  vector <unsigned long> &indicesTo,
						  vector <CXYLineSegment> &segmentVector,	   
						  unsigned long &segmentCount,
						  double I_sigma0);

};


#endif






