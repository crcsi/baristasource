#ifndef __CObsPointArray__
#define __CObsPointArray__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "ObsPoint.h"
#include "ObsPointPtrArray.h"
#include "MUObjectArray.h"
#include "Serializable.h"
#include "Filename.h"
#include "UpdateHandler.h"

class CObsPointArray : public CMUObjectArray<CObsPoint, CObsPointPtrArray>, public CSerializable, public CUpdateHandler
{
protected:
	int dim;
    CFileName FileName;
    bool isPointSorted;

public:

public:
	CObsPointArray(int iDim, int nGrowBy = 0);
	CObsPointArray(const CObsPointArray& src);


	CObsPointArray &operator = (const CObsPointArray & obsArray);

	virtual ~CObsPointArray(void);

	int getDimension() const { return this->dim;};

	bool getIsSorted() const { return this->isPointSorted; }

    CFileName* getFileName();
    void setFileName(const char* filename);
    CFileName  getItemText();
	const char *getFileNameChar() const;

    CObsPoint* getPoint(CCharString Label) const;
	CObsPoint* getPoint_binarySearch(const CLabel& label);
	int getPointIndexBinarySearch(const CLabel& label) const;
    CObsPoint* getPoint(int i) const;

	virtual void update();

    void forceSortPoints();
    void sortByLabel() {this->m_PointerArray.sortByLabel();};
	void deletebyLabel(CCharString Label);

	void revert();

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();

	virtual CObsPoint* Add();
	virtual CObsPoint* Add(const CObsPoint& point);
	virtual CObsPoint* CreateType() const { return new CObsPoint(dim); };

	virtual void Remove(CObsPoint* pElement);
	virtual void RemoveAt(int nIndex, int nCount= 1);
	virtual void RemoveAll();

	virtual bool isa(string& className) const
	{
		if (className == "CObsPointArray")
			return true;

		return false;
	};

	virtual string getClassName() const {return string("CObsPointArray");};

	void setStauts(short status);

};

#endif
