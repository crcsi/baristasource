#ifndef __CXYContour__
#define __CXYContour__
/**
 * Copyright:    Copyright (c) Maria Pateraki<p>
 * Company:      <p>
 * @author Maria Pateraki
 * @version 1.0
 */
 
#include "Label.h"
#include "newmat.h"
#include "2DPoint.h"
#include "2DPointArray.h"
#include "Serializable.h"

#include <math.h>

class CXYContour :public CSerializable
{
public:
	CXYContour(void);
	~CXYContour(void);
	 CXYContour(CXYContour* p);
	   
	 CLabel getLabel();

	 CXYContour copy();

//	 CXYPointArray* getPoints();
	 C2DPointArray* getPoints();
	 void addPoint(C2DPoint newpoint);
	 int getPointCount() const;

	 // CXYPoint getPoint (int index);
	 C2DPoint* getPoint (int index) const;
	 C2DPoint* getNearestPoint (const C2DPoint &p) const;
	 int getIndexOfNearestPoint (const C2DPoint &p) const;

	  void addVertex(int no,int v);
	  int* getIndexVertices();
	  void deleteContour();
      void removePoint(int i);
      void removePoints(int iFrom, int iTo);

	  double getLength() const;

	  
	   CCharString toString();
	   
	   void set(double x, double y);

	   
	   void setLabel (CLabel label);

    // CSerializable functions
	  void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	  void reconnectPointers();
	  void resetPointers();
	  bool isModified();

	  bool isclosed;
	
	  double getLength();

	  void revert();

	  void append(CXYContour *contour);

	  double getClosingGap() const
	  { 	
		  return this->points.GetAt(0)->getDistance(*(this->points.GetAt(this->points.GetSize() - 1)));
	  }

	  string getClassName() const
	  {
        return string("CXYContour");
      };

	  bool isa (string& className) const
	  {
		if (className == "CXYContour")
			return true;

		return false;
	  };


	  bool writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
		            double xfactor = 1.0, double yfactor = 1.0) const;


protected:

    /// the line's label
    CLabel label;
//    CXYPointArray points;         /* pointer to array of pixels */

	C2DPointArray points;
	int v_no[2];            /* index to vertex, delimimiting *chain */

};
#endif
