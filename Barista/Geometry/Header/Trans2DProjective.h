#ifndef __CTrans2DProjective__
#define __CTrans2DProjective__

#include "XYPoint.h"
#include "XYZPoint.h"
#include "Trans2D.h"

class CTrans2DProjective : public CTrans2D
{
  protected:
 
// Transformation formula: x = [a0 + a1 * (X - P0.x) + a2 * (Y - P0.x)] / [c0 + c1 * (X - P0.x) + c2 * (Y - P0.x)]
//                         y = [b0 + b1 * (X - P0.x) + b2 * (Y - P0.x)] / [c0 + c1 * (X - P0.x) + c2 * (Y - P0.x)]
	double a0, a1, a2, b0, b1, b2, c0, c1, c2;
	C2DPoint P0;

  public:
  
	  CTrans2DProjective();

	  CTrans2DProjective(const CTrans2DProjective &trans);

	  CTrans2DProjective(const Matrix &parms, const C2DPoint &p0);

	  CTrans2DProjective(const Matrix &parms);

	  ~CTrans2DProjective();

	  virtual int nParameters() const { return 9; };

  	  virtual CTrans2D *clone() const { return new CTrans2DProjective(*this); };

	  virtual CCharString getDescriptor(const CCharString &lead, int shiftDigits, int matrixDigits, 
		                                const CCharString &descrFrom = "_1", 
										const CCharString &descrTo = "_2") const;

	  CTrans2DProjective &operator = (const CTrans2DProjective &trans);

	  C2DPoint getP0() const { return this->P0; };

	  virtual void applyP0();

	  virtual void get(Matrix &parms, C2DPoint &p0) const;

	  virtual void set(const Matrix &parms, const C2DPoint &p0);

	  void set(const Matrix &parms);

	  void setP0(const C2DPoint &p0);

	  virtual int getWeightForDirectObs(Matrix &weights, double sigma0, double maxInfluence, double maxDist) const;

	  virtual void getDerivatives(double x, double y, Matrix &Ax, Matrix &Ay) const;

	  virtual void getDerivativesCoords(double x, double y, Matrix &B) const;

	  virtual void applyFactorToBoth (double fact, Matrix &covar);

	  virtual void applyLinearTransformation(double factLeft, double factRight, 
		                                     C2DPoint &shiftLeft, const C2DPoint &shiftRight, 
											 Matrix &covar);

	  virtual Matrix getProjectionMatrix() const;

   	  virtual void transform (CXYZPoint &out, const CXYPoint &in) const;

	  virtual void transform (CXYPoint &out, const CXYPoint &in) const;

	  virtual void transform (C2DPoint &out, const C2DPoint &in) const;
	
	  virtual bool invert();

	  virtual void prepareAdjustment(CNormalEquationMatrix &N, CtVector &m, double &vtpv, 
		                             unsigned long &nObs) const;
	  
	  virtual void accumulate(const C2DPoint &p1, const C2DPoint &p2, CNormalEquationMatrix &N, 
		                      CtVector &m, double &vtpv, unsigned long &nObs);

	  virtual bool solve(CNormalEquationMatrix &N, const CtVector &m,
		                 double vtpv, unsigned long nObs, double &s0);

	  virtual void addVec(const Matrix &dx, int index0);

	  virtual void changeP0(const C2DPoint &newP0);

	  bool isa(const char* className) const
	  {
		  if (::strcmp("CTrans2DProjective", className) == 0)
			  return true;
		  return false;
	  }

	  string getClassName() const
	  {
		  return string("CTrans2DProjective");
	  };

};
#endif
