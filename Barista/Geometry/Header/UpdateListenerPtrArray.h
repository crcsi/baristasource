#ifndef __CUpdateListenerPtrArray__
#define __CUpdateListenerPtrArray__

#include "MUObjectPtrArray.h"
#include "UpdateListener.h"

class CUpdateListenerPtrArray : public CMUObjectPtrArray<CUpdateListener>
{
public:
	CUpdateListenerPtrArray(int nGrowBy=0):
	  CMUObjectPtrArray<CUpdateListener>(nGrowBy){};

	  ~CUpdateListenerPtrArray(void){};
};
#endif
