#ifndef __CXYEllipsePtrArray__
#define __CXYEllipsePtrArray__

#include "MUObjectPtrArray.h"
#include "XYEllipse.h"


class CXYEllipsePtrArray : public CMUObjectPtrArray<CXYEllipse>
{
public:
    CXYEllipsePtrArray(int nGrowBy = 0);
    ~CXYEllipsePtrArray(void);

};

#endif
