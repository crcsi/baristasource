#ifndef __CUpdateListener__
#define __CUpdateListener__


/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Label.h"

class CUpdateListener
{
public:
	CUpdateListener(void){};
	~CUpdateListener(void){};

	virtual void elementAdded(CLabel element="") = 0;
    virtual void elementDeleted(CLabel element="") = 0;
    virtual void elementsChanged(CLabel element="") = 0;
	virtual void elementSelected(bool selected,CLabel element="",int elementIndex=-1){};

};
#endif
