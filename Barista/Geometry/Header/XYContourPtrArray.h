#ifndef __CXYContourPtrArray__
#define __CXYContourPtrArray__
#include "MUObjectPtrArray.h"
#include "XYContour.h"

class CXYContourPtrArray :	public CMUObjectPtrArray<CXYContour>
{
public:
	CXYContourPtrArray(int nGrowBy = 0);
	~CXYContourPtrArray(void);
};
#endif
