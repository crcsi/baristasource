#ifndef __CXYPoint__
#define __CXYPoint__

/**
 * Copyright:    Copyright (c) Harry Hanley<p>
 * Company:      University of Melbourne<p>
 * @author Harry Hanley
 * @version 1.0
 */

#include "2DPoint.h"
#include "ObsPoint.h"

class Matrix;
class ColumnVector;


class CXYPoint : public C2DPoint, public CObsPoint
{
public:

	CXYPoint(): CPointBase(2), C2DPoint(), CObsPoint(2){};
	CXYPoint(double Ix, double Iy) : CPointBase(2), C2DPoint(Ix, Iy), CObsPoint(2){};
	CXYPoint(double Ix, double Iy, const CLabel &str): CPointBase(2), C2DPoint(Ix, Iy), CObsPoint(2 ,str) {};

	CXYPoint(C2DPoint* p): CPointBase(p), C2DPoint(*p), CObsPoint(2){};
	CXYPoint(CXYPoint* p):  CPointBase(p), C2DPoint(*p), CObsPoint(p){};
	CXYPoint(CObsPoint* p):  CPointBase(p), C2DPoint(), CObsPoint(p){};
	CXYPoint(const C2DPoint &p): CPointBase(p), C2DPoint(p),  CObsPoint(2){};
	CXYPoint(const CXYPoint &p): CPointBase(p), C2DPoint(p), CObsPoint(p){};
	CXYPoint(const CObsPoint &p): CPointBase(p), C2DPoint(), CObsPoint(p){};
	virtual ~CXYPoint();

	// get functions
	double getX() const     { return this->x; }; 
    double getY() const     { return this->y; };
	double getSX() const;   
    double getSY() const;
    double getSXY() const;
    Matrix getS() const;

	// set functions
	void setX (double X);
    void setY (double Y);
    void set(double x, double y);
    void setSX (double sigmaX);
    void setSY (double sigmaY);
    void setSXY (double sigmaY);

	// operators
	virtual CXYPoint &operator = (const CXYPoint & point);

	/// unary minus operator
	CXYPoint operator - () const { return CXYPoint ( -x, -y ); };

	  void operator *= ( double a )
	  {	x *= a; y *= a; };

	  CXYPoint operator * ( double a ) const
	  {CXYPoint p(*this);
	  	p*=a;
	  	return p;  };

	void setFromHomogeneousVector(const ColumnVector &pHom, const Matrix &covarHom);
	CCharString toString(int length = 12, int digits = 3) const;
    bool equals (CXYPoint* point);
    bool equals (const CXYPoint &point) const;

	// CSerializable functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	void reconnectPointers();
	void resetPointers();
	bool isModified();

	string getClassName() const
	{
		return string("CXYPoint");
	};

	bool isa (string& className) const
	{
		if (className == "CXYPoint")
			return true;

		return false;
	};

};

//==============================================================================
// *****************************************************************************
//
//                          Inline function definitions 
//
// *****************************************************************************
//==============================================================================


/**
 * Sets X coordinate of CXYPoint
 * @param X 
 */
inline void CXYPoint::setX(double X)
{
   this->x = X;
}

/**
 * Sets Y coordinate of CXYPoint
 * @param Y 
 */
inline void CXYPoint::setY(double Y)
{
   this->y = Y;
}

/**
 * sets the coordinates
 * @param x
 * @param y
 */
inline void CXYPoint::set(double x, double y)
{
    this->bModified = true;
    this->x = x;
    this->y = y;
}


#endif



