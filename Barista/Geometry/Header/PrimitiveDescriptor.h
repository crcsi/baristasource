#ifndef CPrimitiveDescriptor_H
#define CPrimitiveDescriptor_H


#include <vector>
#include "Serializable.h"
#include "StructuredFile.h"
#include "CharString.h"


typedef enum  {eBase,eCourtYard,eSuperStructure} PrimitiveDescriptorType;

class CPrimitiveDescriptor: public CSerializable
{
private:
	CPrimitiveDescriptor(void): type(eBase){};

public:
	CPrimitiveDescriptor(const CPrimitiveDescriptor& src);
	CPrimitiveDescriptor(PrimitiveDescriptorType type,int nPoints,CCharString descriptorName);
	~CPrimitiveDescriptor(void);

	PrimitiveDescriptorType getPrimitiveType() const{return this->type;};
	CCharString getDescriptorName() const {return this->descriptorName;};
	int getParentDescriptor() const { return this->parentDescriptor; };

	void setParentDescriptor(int descriptor) { this->parentDescriptor = descriptor; }

	bool containsRoofPoint(int PointId) const;

	CPrimitiveDescriptor &operator = (const CPrimitiveDescriptor & point);

	int& roofPoint (int index)
	{
		assert( index < (int)this->roofPoints.size());
		return this->roofPoints[index];		
	};
	int  roofPoint(int index)const 
	{
		assert( index < (int)this->roofPoints.size());
		return this->roofPoints[index];		
	};

	int& floorPoint (int index)
	{
		assert( index < (int)this->floorPoints.size());
		return this->floorPoints[index];		
	};
	int  floorPoint(int index)const 
	{
		assert( index < (int)this->floorPoints.size());
		return this->floorPoints[index];		
	};


	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();
	
	virtual bool isa(string& className) const
	{
		if (className == "CPrimitiveDescriptor")
			return true;
		return false;
	};

	virtual string getClassName() const {return string("CPrimitiveDescriptor"); };


protected:
	vector <int> roofPoints;
	vector <int> floorPoints;
	
	PrimitiveDescriptorType type;
	int parentDescriptor;

	CCharString descriptorName;
};
#endif