#ifndef __CDisplaySettings__
#define __CDisplaySettings__


#include "Serializable.h"

class CDisplaySettings : public CSerializable
{
public:
	CDisplaySettings(void);
	~CDisplaySettings(void);

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();

	  string getClassName() const
	  {
		  return string("CDisplaySettings");
	  };

	  bool isa (string& className) const
	  {
		  if (className == "CDisplaySettings")		
			  return true;

		  return false;
	  };

	void setShowEntities3D(bool show) { this->showEntities3D = show;};
	bool getShowEntities3D() const {return this->showEntities3D;};

	void setShowLabels3D(bool show) { this->showLabels3D = show;};
	bool getShowLabels3D() const {return this->showLabels3D;};

	void setColor(float r, float g, float b )
	{ 
		this->red = r;
		this->green = green;
		this->blue = blue;
	};

	void getColor(float r, float g, float b)
	{
		r = (float)this->red;
		b = (float)this->blue;
		g = (float)this->green;
	};

protected:
	bool showEntities3D;
	bool showLabels3D;
	double red;
	double blue;
	double green;
};



#endif