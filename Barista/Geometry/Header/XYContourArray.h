#ifndef __CXYContourArray__
#define __CXYContourArray__
#include "MUObjectArray.h"
#include "XYContour.h"
#include "XYContourPtrArray.h"
#include "Filename.h"
#include "CharString.h"

class CXYContourArray :	public CMUObjectArray<CXYContour, CXYContourPtrArray>, public CSerializable
{
public:
	CXYContourArray(int nGrowBy = 0);
	CXYContourArray(const CXYContourArray* src);
	CXYContourArray(const CXYContourArray& src);
	~CXYContourArray(void);
   
 //   CXYZPolyLineArrayListenerPtrArray listeners;

//    void addListener(CXYZPolyLineArrayListener* listener);
//    void removeListener(CXYZPolyLineArrayListener* listener);

    bool writeXYContourTextFile(CFileName cfilename);
    bool writeXYContourVRMLFile(CFileName cfilename);

    CFileName* getFileName();
    void setFileName(const char* filename);
    CFileName getItemText();

	CXYContour* Add();
	CXYContour* Add(const CXYContour& newElement);

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    bool isModified();

    CXYContour* getLine(CCharString Label);
    CXYContour* getLine(int i);

	void connectContours(float delta = 0.5f);

    string getClassName() const
    {
        return string("CXYContourArray");
    };

	bool isa (string& className) const
	{
		if (className == "CXYContourArray")
			return true;

		return false;
	};

	bool getActive()
	{
		return this->isActive;
	};

	void setActive(bool isactive)
	{
		this->isActive = isactive;
	};

	void ContourArrayToDXF(const CFileName &fn);

	void ContourArrayToDXF(ofstream &dxfFile);

protected:
    CFileName FileName;
	bool isActive;
   
};
#endif
