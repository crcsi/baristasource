#ifndef CPrimitiveDescriptorArray_H
#define CPrimitiveDescriptorArray_H

#include <vector>
#include "PrimitiveDescriptor.h"
#include "Serializable.h"
#include "StructuredFile.h"


class CPrimitiveDescriptorArray: public CSerializable
{
public:
	CPrimitiveDescriptorArray(void);
	CPrimitiveDescriptorArray(const CPrimitiveDescriptorArray& src);
	~CPrimitiveDescriptorArray(void);

	CPrimitiveDescriptorArray &operator = (const CPrimitiveDescriptorArray & src);

	bool addPrimitiveDescriptor(const CPrimitiveDescriptor& descriptor);
	bool removeDescriptor(CCharString descriptorName);

	void clear();

	int getIndexOfDescriptor(int PointId) const;

	int getCourtYardCount()const;
	int getSuperStructureCount() const;

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();
	
	virtual bool isa(string& className) const
	{
		if (className == "CPrimitiveDescriptorArray")
			return true;
		return false;
	};

	virtual string getClassName() const {return string("CPrimitiveDescriptorArray"); };


protected:
	vector<CPrimitiveDescriptor> dataSets;
};
#endif