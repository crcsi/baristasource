#ifndef __CXYPointArray__
#define __CXYPointArray__

#include "ObsPointArray.h"
#include "XYPoint.h"

enum xypointtype {
   MEASURED,
   PROJECTED,
   DIFFERENCED,
   RESIDUALS,
   MONOPLOTTED,
   NOTYPE,
   FIDUCIALMARKS,
   OBJRESIDUALS //added by M. Awrangjeb on 20 Mar 2009
};

class CXYPointArray : public CObsPointArray
{
public:
	CXYPointArray(int nGrowBy = 0);
	CXYPointArray(xypointtype type);
    CXYPointArray(const CXYPointArray* src);
    CXYPointArray(const CXYPointArray& src);

	CXYPointArray &operator = (const CXYPointArray & xyArray);

	~CXYPointArray(void);

    bool readXYPointTextFile(CFileName cfilename);
    bool writeXYPointTextFile(CFileName cfilename, int type);

    bool isa(string& className) const;
    string getClassName() const;
	virtual CObsPoint* CreateType() const;
	virtual CObsPoint* CreateType(const CObsPoint &p) const;

	CXYPoint *getAt(int index) const { return (CXYPoint *) this->GetAt(index); };
	CXYPoint *add() { return (CXYPoint *) this->Add(); };
	CXYPoint *add(const CObsPoint &p) { return (CXYPoint *) this->Add(p); };

	void getbyArea(int xmin, int xmax, int ymin, int ymax, CXYPointArray* pointsinArea);
	void deletebyArea(int xmin, int xmax, int ymin, int ymax);

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    // sorting, merging, differencing, mean
    CXYPoint getMean() const;  
	void differenceByLabel(CXYPointArray* differences, CXYPointArray* comparepoints);
	void differenceByIndex(CXYPointArray* differences, CXYPointArray* comparepoints);
   // void sortPoints();

	void setSXSYforAll(double sx, double sy);

	bool getActive(){return this->isActive;};

	void setActive(bool isactive){this->isActive = isactive;};

	xypointtype getPointType(){	return this->type;}

	void setPointType(xypointtype type)	{this->type = type;	};

protected:
	bool isActive;

	xypointtype type;
};


#endif

