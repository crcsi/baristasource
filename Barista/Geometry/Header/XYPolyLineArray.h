#ifndef __CXYPolyLineArray__
#define __CXYPolyLineArray__


#include "XYPolyLine.h"
#include "GenericPolyLineArray.h"
#include "MUObjectArray.h"
#include "Filename.h"
#include "CharString.h"

class CXYPolyLineArray : public CGenericPolyLineArray
{
public:
	CXYPolyLineArray(int nGrowBy = 0);
    CXYPolyLineArray(CXYPolyLineArray* src) {assert(false);};
    CXYPolyLineArray(const CXYPolyLineArray& src) {assert(false);};
	~CXYPolyLineArray(void);

	virtual int getDimension() const { return 2; };


    bool writeXYPolyLineTextFile(CFileName cfilename);
    bool writeXYPolyLineVRMLFile(CFileName cfilename);


	virtual GenericPolyLine* CreateType() const { return new CXYPolyLine; };
	virtual GenericPolyLine* CreateType(const GenericPolyLine &p) const { return new CXYPolyLine(p); };

	CXYPolyLine *getAt(int index) const { return (CXYPolyLine *) this->GetAt(index); };
	CXYPolyLine *add() { return (CXYPolyLine *) this->Add(); };
	CXYPolyLine *add(const CXYPolyLine &p) { return (CXYPolyLine *) this->Add(p); };


    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    CXYPolyLine* getLine(CCharString Label);
    CXYPolyLine* getLine(int i);


    string getClassName() const
    {
        return string("CXYPolyLineArray");
    };

	bool isa (string& className) const
	{
		if (className == "CXYPolyLineArray")
			return true;

		return false;
	};

	bool getActive()
	{
		return this->isActive;
	};

	void setActive(bool isactive)
	{
		this->isActive = isactive;
	};

protected:

	bool isActive;

};

#endif

