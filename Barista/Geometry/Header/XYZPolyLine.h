#ifndef __CXYZPolyLine__
#define __CXYZPolyLine__


/**
 * Copyright:    Copyright (c) Jochen Willneff<p>
 * Company:      University of Melbourne<p>
 * @author Jochen Willneff
 * @version 1.0
 */
#include "Label.h"
#include "newmat.h"
#include "Serializable.h"
#include "XYZPoint.h"
#include "math.h"
#include "Trans3D.h"
#include "XYPolyLine.h"
#include "GenericPolyLine.h"
#include "XYZPointArray.h"
class CXYPolyLine;
class doubles;

class CXYZPolyLine : public GenericPolyLine
{
protected:

   CXYZPointArray points;

public:

    CXYZPolyLine(void);
    ~CXYZPolyLine(void);
    CXYZPolyLine(CXYZPolyLine* p);
    CXYZPolyLine(const CXYZPolyLine &p);
	CXYZPolyLine(const GenericPolyLine &p);
    CXYZPolyLine(const CXYPolyLine &p, double I_z = 0.0f, double I_sigmaZ = 1.0f);

    CXYZPolyLine copy();

	void addPoint(CXYZPoint newpoint);

	void closeLine();
	void openLine();

	CXYZPoint *getPoint (int index) const { return this->points.getAt(index); }
	
	CXYZPoint &operator[] (unsigned int index) { return *this->points.getAt(index); };
	const CObsPoint &operator[] (unsigned int index) const { return *this->points.getAt(index); };


	virtual double getLength();

    //CXYZPolyLine transform(CTrans3D trans);


    // CSerializable functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	void reconnectPointers();
	void resetPointers();
	bool isModified();


    string getClassName() const
    {
        return string("CXYZPolyLine");
    };

	bool isa (string& className) const
	{
		if (className == "CXYZPolyLine")
			return true;

		return false;
	};

	// intersect the polyline with line at x = I_x
	// intersections contains the ordered list of y values of all intersections
	// return value: number of intersections
	long getIntersections(double I_x, doubles &intersections) const;

	bool contains(const CXYZPoint &p) const;
	bool contains(const CXYZPolyLine &l) const;

	void append(const CXYZPolyLine &poly);

	bool writeDXF(ostream& dxf, const char *layer, int colour, int decimals,
		          double xfactor = 1.0, double yfactor = 1.0, double zfactor = 1.0) const;

	static bool writeDXFHeader(ofstream &dxf);	

	static bool writeDXFTrailer(ofstream &dxf);

};


#endif






