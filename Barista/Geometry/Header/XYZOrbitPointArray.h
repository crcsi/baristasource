#ifndef CXYZORBITPOINTARRAY_H
#define CXYZORBITPOINTARRAY_H

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "XYZOrbitPoint.h"
#include "ObsPointArray.h"
#include "MUObjectArray.h"
#include "Serializable.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

class CXYZOrbitPointArray  : public CObsPointArray
{
protected:
	bool createWithVelocity;
public:
	CXYZOrbitPointArray(int nGrowBy = 0);
	CXYZOrbitPointArray(const CXYZOrbitPointArray& src );
	CXYZOrbitPointArray &operator = (const CXYZOrbitPointArray & orbitArray);

	~CXYZOrbitPointArray(void);

	// serialize functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	virtual CObsPoint* CreateType() const { return new CXYZOrbitPoint(this->createWithVelocity); };
	virtual CObsPoint* CreateType(const CObsPoint &p) const { return new CXYZOrbitPoint(p); };
	virtual CXYZOrbitPoint *getAt(int index) const { return (CXYZOrbitPoint *) this->GetAt(index); };
	virtual CXYZOrbitPoint *add() { return (CXYZOrbitPoint *) this->Add(); };
	virtual CXYZOrbitPoint *add(const CXYZOrbitPoint &p) 
	{ 
		CXYZOrbitPoint* tmp = (CXYZOrbitPoint*)this->Add(p); 
		tmp->setHasVelocity(p.getHasVelocity());
		return tmp;
	};

	bool isa(string& className) const
	{
		if (className == "CXYZOrbitPointArray")
			return true;

		return false;
	};
	string getClassName() const {return string("CXYZOrbitPointArray");};

	void setCreateWithVelocity(bool b) {this->createWithVelocity = b;};
	bool getCreateWithVelocity() {return this->createWithVelocity;};
};


#endif
