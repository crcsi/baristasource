#ifndef __CXYZPointArrayPtrArray__
#define __CXYZPointArrayPtrArray__

#include "MUObjectPtrArray.h"
#include "XYZPointArray.h"
#include "Serializable.h"

class CXYZPointArrayPtrArray : public CMUObjectPtrArray<CXYZPointArray>//, CSerializable
{
public:
    CXYZPointArrayPtrArray(int nGrowBy = 0);
    ~CXYZPointArrayPtrArray(void);

};

#endif
