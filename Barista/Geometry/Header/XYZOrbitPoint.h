#ifndef __CXYZOrbitPoint__
#define __CXYZOrbitPoint__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Serializable.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

#include "ObsPoint.h"
class CXYZPoint;

class CXYZOrbitPoint : public CObsPoint
{
protected:
	bool hasVelocity;
public:
	CXYZOrbitPoint(void): CPointBase(6), CObsPoint(6), hasVelocity(true){};
	CXYZOrbitPoint(bool hasVelocity): CPointBase(hasVelocity ? 6 : 3), CObsPoint(hasVelocity ? 6 : 3), hasVelocity(hasVelocity){};
	CXYZOrbitPoint(const CXYZOrbitPoint &p): CPointBase(p), CObsPoint(p), hasVelocity(p.hasVelocity){};
	CXYZOrbitPoint(const CObsPoint &p): CPointBase(p), CObsPoint(p), hasVelocity(p.getDimension() == 3 ? false:true){};
	CXYZOrbitPoint(CXYZOrbitPoint *p): CPointBase(p), CObsPoint(p), hasVelocity(p->hasVelocity){};
	~CXYZOrbitPoint(void);

// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();
	
	virtual bool isa(string& className) const
	{
		if (className == "CXYZOrbitPoint")
			  return true;
		return false;
	};

	virtual string getClassName() const {return string("CXYZOrbitPoint"); };  

	void setHasVelocity(bool b);
	void setDate(int day,int month,int year);
	void setTime(int hour,int min, double sec);
	void setOrbitPoint(CXYZPoint location,CXYZPoint velocity,int day,int month,int year,int hour,int min,double sec);

	bool getHasVelocity()const {return this->hasVelocity; };
	double getTime(){ return this->dot;};
	virtual int getDimension() const;

};
#endif
