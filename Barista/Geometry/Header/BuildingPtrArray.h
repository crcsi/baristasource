#ifdef WIN32

#ifndef __CBuildingPtrArray__
#define __CBuildingPtrArray__

#include "MUObjectPtrArray.h"
#include "Building.h"


class CBuildingPtrArray : public CMUObjectPtrArray<CBuilding>
{
public:
    CBuildingPtrArray(int nGrowBy = 0);
    ~CBuildingPtrArray(void);

};

#endif

#endif // WIN32
