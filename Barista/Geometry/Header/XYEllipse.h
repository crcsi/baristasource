#ifndef __CXYEllipse__
#define __CXYEllipse__

/**
 * Copyright:    Copyright (c) Jochen Willneff<p>
 * Company:      University of Melbourne<p>
 * @author Jochen Willneff
 * @version 1.0
 */
#include "Label.h"
#include "XYPoint.h"
#include "2DPointArray.h"

#include "newmat.h"
#include "Serializable.h"

class CXYEllipse : public CSerializable
{
protected:
    /// the ellipse's label
    CLabel label;
	
	double a;
	double b;
	double theta;
	double x0;
	double y0;

	double sinTheta;
	double cosTheta;

    /// the ellipse's 5x5 covariance matrix
    Matrix covariance;
public:

    CXYEllipse();
    ~CXYEllipse();
    CXYEllipse(double a, double b, double theta, double x0, double y0);
    CXYEllipse(double a, double b, double theta, double x0, double y0, const CLabel str);
    CXYEllipse(Matrix parms, const CLabel str);
    CXYEllipse(Matrix parms);

	bool init(const vector<C2DPoint> &pointVector);

    void set(double a, double b, double theta, double x0, double y0);
    void set(double a, double b, double theta, double x0, double y0, const CLabel str);

    void set(Matrix parms, const CLabel str);
    void set(Matrix parms);

    CLabel getLabel() const;
    CXYPoint getCenter() const;

	Matrix getRotMatToLocal() const;

    C2DPoint transformToLocal(C2DPoint p) const;

    CXYPoint transformToLocal(CXYPoint p) const;

    C2DPoint transformToGlobal(C2DPoint p) const;

    CXYPoint transformToGlobal(const CXYPoint p) const;

	double getDistance(C2DPoint p, C2DPoint &normal) const;

	double getDistance(C2DPoint p) const;

	//void getBoarderPoints(CXYPointArray* boarder);
	void getBoarderPoints(C2DPointArray* boarder);

	string getClassName() const
	{
	 return string("CXYEllipse");
	};

	bool isa (string& className) const
	{
	if (className == "CXYEllipse")
		return true;

	return false;
	};

    void setLabel (CLabel label);

	double getA()
	{
		return this->a;
	};

	double getB()
	{
		return this->b;
	};

	double getTheta()
	{
		return this->theta;
	};

	double getX0()
	{
		return this->x0;
	};

	double getY0()
	{
		return this->y0;
	};
    
	void setCovariance (const Matrix &covariance);
    Matrix* getCovariance();

	// CSerializable functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	void reconnectPointers();
	void resetPointers();
	bool isModified();
};

#endif
