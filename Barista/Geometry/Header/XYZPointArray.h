#ifndef __CXYZPointArray__
#define __CXYZPointArray__

#include "ObsPointArray.h"

#include "TextFile.h"
#include "Filename.h"
#include "XYPointArray.h"
#include "BlockDiagonalMatrix.h"
#include "ReferenceSystem.h"
#include "math.h"
#include "XYZPoint.h"
#include "DisplaySettings.h"

enum XyzPointType {
	eBUNDLERESULT,
	eMONOPLOTTED,
	eGEOREFERENCED,
	eDIFFERENCED,
	eNOTYPE,
	eRESIDUAL,
	eGCPCHIP
};

class CXYZPointArray : public CObsPointArray
{
public:
	CXYZPointArray(int nGrowBy = 0);
	CXYZPointArray(XyzPointType type);
    CXYZPointArray(CXYZPointArray* src);
    CXYZPointArray(const CXYZPointArray& src);
	
	CXYZPointArray &operator = (const CXYZPointArray & xyzArray);

    ~CXYZPointArray(void);


    bool readXYZPointTextFile(const CFileName& cfilename);
    bool writeXYZPointTextFile(CFileName& cfilename, int type);
    bool writeXYZPointDDMMSSsssTextFile(CFileName& cfilename);
	//addition >>
	bool writeXYZPointShapefile(CFileName& cfilename);
	//<< addition

    bool isa(string& className) const;
    string getClassName() const;
	virtual CObsPoint* CreateType() const { return new CXYZPoint; };
	virtual CObsPoint* CreateType(const CObsPoint &p) const { return new CXYZPoint(p); };
	CXYZPoint *getAt(int index) const { return (CXYZPoint *) this->GetAt(index); };
	CXYZPoint *add() { return (CXYZPoint *) this->Add(); };
	CXYZPoint *add(const CObsPoint &p) { return (CXYZPoint *) this->Add(p); };

	
	//CXYZPoint *getByLabel(CLabel lbl) const;// { return (CXYZPoint *) this->GetAt(index); };
	

    Matrix getMergedPoints(CCharStringArray* Labels, CBlockDiagonalMatrix* Covariance, CXYPointArray* Scene);
    Matrix getMergedPointsforGeoreferencing(CCharStringArray* Labels, CBlockDiagonalMatrix* Covariance, CXYPointArray* Scene);

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    // sorting, merging, differencing, mean

    CXYZPoint getMean() const;  
	void differenceByLabel(CXYZPointArray* differences, CXYZPointArray* comparepoints);
	void differenceByIndex(CXYZPointArray* differences, CXYZPointArray* comparepoints);

	void getExtends();

	double getMinX() const {return this->xmin;}
	double getMinY() const {return this->ymin;}
	double getMinZ() const {return this->zmin;}

	double getMaxX() const {return this->xmax;}
	double getMaxY() const {return this->ymax;}
	double getMaxZ() const {return this->zmax;}

    //void sortPoints();
    void setControl(bool iscontrol);
    bool isControl()const;

	void setCheckPoint(bool isCheckPoint);
	bool isCheckPoint()const;


	void setSXSYSZforAll(double sx, double sy, double sz);

    CReferenceSystem* getReferenceSystem() { return &this->refSystem;};
    
	const CReferenceSystem &getRefSys() const { return this->refSystem;};

	void setReferenceSystem(const CReferenceSystem &refsys)
	{
		this->refSystem = refsys;
		this->informListeners_elementsChanged();
	};

	void setCoordinateType(eCoordinateType typ);

	XyzPointType getPointType()const {return this->type;};
	void setPointType(XyzPointType type) {this->type = type;};


    CDisplaySettings* getDisplaySettings() { return &this->displaySettings;};

protected:
    CReferenceSystem refSystem;
	XyzPointType type;
    bool isboolControl;
	bool bCheckPoint;

	double xmin;
	double ymin;
	double zmin;
	double xmax;
	double ymax;
	double zmax;

	CDisplaySettings displaySettings;
};

#endif

