#ifndef __CBuilding__
#define __CBuilding__


/**
 * Copyright:    Copyright (c) Jochen Willneff<p>
 * Company:      University of Melbourne<p>
 * @author Jochen Willneff
 * @version 1.0
 */
#include <strstream>
using namespace std;

#include "Label.h"
#include "XYZPoint.h"
#include "XYZPointArray.h"
#include "StructuredFile.h"
#include "Trans3D.h"
#include "PrimitiveDescriptorArray.h"

class CXYPolyLine;
class CXYZPolyLine;
class CXYZPolyLineArray;


class vrSolid;
class vrFace;
class vrLoop;
class vrVertex;
class vrBrowser;


class CBuilding : public CSerializable
{

  public:
    CBuilding();
    CBuilding(const CBuilding &other);
    ~CBuilding();

#ifdef WIN32
    CBuilding copy();

    CBuilding &operator = (const CBuilding &source);

	C2DPoint getCentre() const;

	double getRoofHeight() const;
	double getFloorHeight() const;

	int getCourtYardCount()const {return this->history.getCourtYardCount();};
	int getSuperStructureCount()const {return this->history.getSuperStructureCount();};

	bool isPointInCurrentFace(C3DPoint p) const;

	C3DPoint getPmin() const { return pmin; }
	C3DPoint getPmax() const { return pmax; }

	double getHeight() const;
	CLabel getFirstPointLabel() const;

	const CXYZPointArray &getVertices() const { return this->points; };

	void resetCurrentFace();

	bool verticalPoleAtCurrentFace(const C3DPoint &p01, const C3DPoint &dir1, 
		                           const C3DPoint &p02, const C3DPoint &dir2, 
								   C3DPoint &plow, C3DPoint &phigh);

	bool interpolateAtCurrentFace(C3DPoint &p);

	bool intersectWithCurrentFace(const C3DPoint &p0, const C3DPoint &dir, C3DPoint &intersection);

	bool addRoofPlane(CXYZPolyLine &Roof, CXYZPolyLine &Floor, bool setRoofToFirst = false);

	bool addInnerCourtyard(CXYZPolyLine &Roof, CXYZPolyLine &Floor, bool resetCurrent = true);

    int getPointCount() const;

	double getArea() const;

	void deleteBuilding();


    CLabel getLabel () const;
    void setLabel (const CLabel& Label);
    //CBuilding transform(CBuilding trans);

    // CSerializable functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	void reconnectPointers();
	void resetPointers();

	int getNWalls() const;
	int getNRoofs() const;
	int getNFloors() const;

	vrSolid *getSolid() const { return this->solid; };

	void getRoofs(CXYZPolyLineArray &roofs) const;
	void getFloors(CXYZPolyLineArray &floors) const;
	void getWalls(CXYZPolyLineArray &walls) const;
	void getAllLoops(CXYZPolyLineArray &loops) const;

	void getAllVerticesAndEdges(CObsPointArray &vertices, vector<int> &indicesFrom, vector<int> &indicesTo) const;


    bool isa(string& className) const;
    string getClassName() const;
	bool isModified();

	void setOffset(const C3DPoint &Offset);

	void writeTextFile(FILE *fp);

	bool writeVRML1File(FILE *fp, const C3DPoint &reduction, CReferenceSystem *refsys = 0);
	bool writeVRML2File(vrBrowser &Browser, const C3DPoint &reduction, CReferenceSystem *refsys = 0);


	bool getIsSelected()const  {return this->isSelected;};
	void setSelected(bool b) {this->isSelected= b;};

	bool booleanUnion(CXYZPolyLine &Roof, CXYZPolyLine &Floor);

  protected:


	C3DPoint pmin, pmax;

	C3DPoint offset;

	vrSolid *solid;

	vrFace *currentFace;

    /// the line's label
    CLabel label;
    CXYZPointArray points;

	CPrimitiveDescriptorArray history;


	bool isSelected;

	void revert();

	void initSolidFromFaceDescriptors(const CCharStringArray &faceDescriptors);

	vrSolid *getLoopSolid(istrstream &Descriptor);

	vrSolid *getFaceSolid(CCharString Descriptor);
	
	vrSolid *getPrism(CXYZPolyLine &Roof, CXYZPolyLine &Floor, int firstId, 
		              bool innerCourt, bool addToHistory);

	CCharString getFaceDescriptor(vrFace *face);

	char *getLoopDescriptor(vrLoop *loop);
	
	void addFacePolys(CXYZPolyLineArray &faceloops, vrFace *face) const;

	void checkMinMax(const C3DPoint &p);

	bool setCurrentFace(const C3DPoint &p0, const C3DPoint &dir, C3DPoint &intersection);

	bool getIntersection(vrFace *face, const C3DPoint &p0, const C3DPoint &dir, C3DPoint &intersection) const;
	bool isPointInLoop(vrLoop *loop, const C3DPoint &p) const;


	void setFloorHeight(vrSolid *pSolid, double newHeight);
 
	vrFace *getFloor(vrSolid *pSolid) const;

	double getFloorHeight(vrSolid *pSolid) const;

	void makeVerticesConsistent(vrSolid *pOldSolid, vrSolid *pAddedSolid);

	vrVertex *getVertexXY(vrFace *face, vrVertex *v) const;
#endif // WIN32
};



#endif



