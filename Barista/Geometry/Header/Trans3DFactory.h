#ifndef __CTrans3DFactory__
#define __CTrans3DFactory__
#include "Trans3D.h"

class CReferenceSystem;

class CTrans3DFactory
{
  public:
    
	  static CTrans3D *initTransformation(const CReferenceSystem &systemFrom, const CReferenceSystem &systemTo);

  protected:

	  static CTrans3D *initGeogToX(const CReferenceSystem &systemFrom, const CReferenceSystem &systemTo);
	  static CTrans3D *initGeocToX(const CReferenceSystem &systemFrom, const CReferenceSystem &systemTo);
	  static CTrans3D *initGridToX(const CReferenceSystem &systemFrom, const CReferenceSystem &systemTo);
};
#endif

