#ifdef WIN32
#ifndef ___CDempsterShaferBuildingClassifier___
#define ___CDempsterShaferBuildingClassifier___

#include <vector>

#include "CharString.h"
#include "DempsterShafer.h"
#include "DempShSensorRecord.h"
#include "ImgBuf.h"
#include "LookupTable.h"
#include "ProgressHandler.h"

class CDSMRoughness;
class CLabelImage;
class CDSMBuilding;
/*!
 * \brief
 * A class encapsulating the application of the Dempster-Shafer theory for building detection
 * 

    For a description of the methodology please refer to the following publications: 

          Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik:
          Using the Dempster-Shafer Method for the Fusion of LIDAR Data 
          and Multi-spectral Images for Building Detection. 
          Information Fusion 6(4), pp. 283-300.

    and

          Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik:
          Building detection by fusion of airborne laser scanner data and multi-spectral images: 
		  Performance evaluation and sensitivity analysis. 
          ISPRS Journal of Photogrammetry and Remote Sensing 62 (2007), pp. 135�149.

    The digital image of (*this) corresponds to the image of the results of the pixel-wise 
	classification, thus, the pixel value is the class index.
    
	The following classes are distinguished: 
	Building (B), Tree (T), Grassland (G), Bare Soil (S).

	Class indices for Dempster-Shafer if the NDVI is used:
	Index    Class              
       0     B
       1     T
       2     O 
       3     B or T 
       4     B or O 
       5     T or O
       6     B or T or O = Theta


	Class indices for Dempster-Shafer:
    Index    Class              
       0     B
       1     T
       2     G 
       3     S
       4     B or T 
       5     B or G 
       6     B or S
       7     T or G 
       8     T or S
       9     G or S
      10     B or T or G
      11     B or T or S
      12     B or G or S
      13     T or G or S
      14     B or T or G or S = Theta

    The input (sensor) data are passed over to this class by pointers and are taken 
	"as-is"; it is assumed that they are digital "images" with float "grey values".
	These "images" are assumed to cover the same area and to be sampled 
	at the same resolution, thus "pixel on pixel".

	Any combination of the input sensors can be used for classification.
	However, at least one sensor must be used, otherwise the program will crash. 
	Depending on the sensors that are actually used, some classes
	might not be distinguishable; these classes will be merged.
	
 * 
 * \see
 * CBuildingDetector | CDSMBuilding
 */


class CDempsterShaferBuildingClassifier : public CImageBuffer, public CProgressHandler
{
  protected:

//=============================================================================
//=================================== Data ====================================

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - Input "Sensors" - - - - - - - - - - - - - - -
	
	  //! Height differences of the DSM and the DTM in the region of interest. 
	  /*! By default, the DSM is assumed to be the last pulse DSM. */
	   CImageBuffer *pNormDSM;      

	  //! The object encapsulating the methods for an analysis of the DSM roughness according to Prof. Foerstner. 
	  /*! Can be the roughness of the first pulse DSM or the last pulse DSM; here, it is taken "as-is". */
	   CDSMRoughness *pRoughness;  

	  //! The height differences between first and last pulse DSM in the region of interest.
	  /*! If (pFirstLast == 0), these height differences will not be considered. */
	   CImageBuffer *pFirstLast;     

	  //! The vegetation index (NDVI or VVI)
	  /*! If (pVegIndex == 0), the vegetation index will not be considered. 
	      In this case, classes G and S cannot be distinguished; they will be merged to class G. */
	   CImageBuffer *pVegIndex;
	   	   

	  //! The standard deviations of the vegetation index. 
	  /*! If (pVegIndexSigma == 0), it will not be considered. */
	   CImageBuffer *pVegIndexSigma;

	  //! A binary image of existing building pixels, for change detection.
	  /*! If (pExistingBuildingMask == 0), it will not be considered. */
	   CImageBuffer *pExistingBuildingMask;


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - Objects for Dempster-Shafer classification  - - - - - - - -

	  //! Object for pixel-wise classification. 
	   CDempsterShafer dempsterShaferPix; 

	  //! Object for region-wise classification. 
	   CDempsterShafer dempsterShaferReg; 

	  //! Sensor index of the existing building mask if it is used for classification
	   int ExistingIndexPix;        

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - Sensor records encapsulating the model for the probability masses - - -

	  //! Model for the height differences between DSM and DTM in the pixel-wise classification
	   CDempShSensorRecord deltaHRecordPix;        

	  //! Model for the height differences between DSM and DTM in the pixel-wise classification
	   CDempShSensorRecord roughnessStrRecordPix;  

	  //! Model for the isotropy of DSM roughnessin the pixel-wise classification
	   CDempShSensorRecord roughnessIsoRecordPix;  

	  //! Model for the height differences between the first and last pulse DSMs  in the pixel-wise classification
	   CDempShSensorRecord firstLastRecordPix;        

	  //! Model for the NDVI in the pixel-wise classification
	   CDempShSensorRecord NDVIRecordPix;      

	  //! Model for the average height difference betweem DSM and DTM in the regon-wise classification
	   CDempShSensorRecord deltaHRecordReg;       

	  //! Model for the percentage of "homogeneous"  pixels in the regon-wise classification
	   CDempShSensorRecord homogRecordReg;   

	  //! Model for the percentage of "point-like" pixels in the regon-wise classification
	   CDempShSensorRecord pointRecordReg;      

	  //! Right branch of the model for the average NDVI in the regon-wise classification
	   CDempShSensorRecord NDVIRecordReg;    

	  //! Left branch of the model for the average NDVI in the regon-wise classification; used to eliminate very dark areas as water
	   CDempShSensorRecord NDVIRecordReg1;     

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - Control parameters  - - - - - - - - - - - - - -

	  //! A flag describing which of the three sensors (DSM-DTM, DSM pRoughness strength, DSM roughness isotropy) is to be used. 
	  /*! Any bit-wise combination of the following numbers:
          1 DSM roughness isotropy
          2 DSM roughness strength
          4 DSM-DTM */
	   int sensorOption; 

	  //! Significance threshold for DSM roughness. 
	  /*! Isotropy of roughness is  only used for pixels of roughness strength 
	      larger than significantRoughQuantil. */
	   float significantRoughQuantil;  // 

	  //! Class to which the complementary probability (to class T) is assigned.
	  /*! Can be either 
	      14 - B or T or G or S or
	      l12 - B or G or S. */
	   int firstLastComplementaryClass; 

	  //! Multiplication constant for sigma_{NDVI}. 
	  /*! The probability mass sigma_{NDVI} * sigmaMultiplicator 
	      will be assigned to class Theta to model the uncertainty of the 
		  NDVI in the classification process. */
	   float sigmaMultiplicator;

	  //! Maximum sigma_{NDVI} so that NDVI is still used; 
	  /*! If sigma_{NDVI} > sigmaMax,  NDVI will not be considered. sigmaMax = 25%. */
	   float sigmaMax;   

	  //! Probability that a building pixel in the existing building image remains a building pixel
	   float probExisting;

	  //! Probability that a  pixel in the in the existing building image is a building pixel
	   float probBuildExist;

	  //! Flag signalling whether to use the percentage of 'homogeneous' pixels for region-based classification. 
	   bool useHomogeneousForRegions;

//=============================================================================

  public:

//=============================================================================
//==================== Constants (Bits) for sensor option  ====================

	  //! Bit telling whether isotropy of surface roughness should be used in sensor option; value: 1
	  static const int UseRoughIso; 

	  //! Bit telling whether strength of surface roughness should be used in sensor option; value: 2
	  static const int UseRoughStr; 

	  //! Bit telling whether the normalised DSM should be used in sensor option; value: 4
	  static const int UseDz;    


//=============================================================================
//=============================== Constructor  ================================ 


	  CDempsterShaferBuildingClassifier(CImageBuffer  *PNormDSM , 
		                                CDSMRoughness *PRoughness,
										CImageBuffer  *PDSMFirstLast,
										CImageBuffer  *PVegIndex, 
										CImageBuffer  *PVegIndexSigma,
										CImageBuffer  *PExisting,
										float dSqQuantil,
										float SigmaMultiplicator,
										int   SensorOption, 
										bool  useThetaForFirstLast,
										bool  useEmpirical,
										float ProbExisting,
										float ProbBuildExist);

// Input "Sensor" Data, all sampled in the same grid:
//
// PNormDSM:                 Height differences between DSM and DTM 
//                              (must not be 0)
// PNormDSM:               DSM pRoughness (must not be 0)
// PDSMFirstLast:           Height differences between first and last 
//                              pulse DSM; If IpC_dsmFirstPulse == 0, they are 
//                              not used in classification     
// PVegIndex:                Vegetation index (NDVI or VVI); 
//                              If IpC_vegIndex == 0, it is not used in
//                              classification     
// PVegIndexSigma:           Standard deviation of the vegetation index 
//                              If IpC_vegIndexSigma == 0, sigma_{NDVI} is
//                              supposed to be 0 for all pixels. 
//
// Control parameters and flags:
//
// SensorOption:          Option which of the sensors (DSM-DTM, DSM roughness strength, 
//                        DSM rougness isotropy)are to be used in the pixel-wise 
//                        classification; Any combination of the constants UseRoughIso,
//                        UseRoughStr, and UseDz.
// dSqQuantil:            Input for significantRoughQuantil; if dSqQuantil < 0, 
//                        then the current quantil of IpC_foerstner is used.
// useThetaForFirstLast:  Mode for considering the height differences between the first
//                        and last pulses:
//                        true : firstLastComplementaryClass = 14 (B or T or G or S)
//                        false: firstLastComplementaryClass = 12 (B or G or S)
// sigmaMultiplicator:    Input for sigmaMultiplicator
//
// useEmpirical:                 Selects model for the computation of the 
//                              probability masses (linear / cubic / empirical)
//                              If Ie_probMode == eEMPIRICAL, the empirical 
//                              model is read from files which have to be 
//                              available in the current working directory.
//                              If any such file does not exist, the cubic 
//                              model is used instead. For a description of
//                              the file format, see documentation of 
//                              CDempShSensorRecord::initEmpiricalModel(const CCharString &)
//                              in this file.
//
//                              The file names for the individual "Sensors" are
//                              hard coded:
//
//                              Pixel-wise classification:
//
//                              Height differences DSM-DTM:                       DZ_PX.PRB
//                              Strength of DSM Roughness:                        HOMOG_PX.PRB
//                              Isotropy of DSM Roughness:                        ISO_PX.PRB
//                              Height differences between first and last pulses: FL_PX.PRB
//                              NDVI:                                             NDVI_PX.PRB
//
//                              Region-wise classification:
//
//                              Average height difference DSM-DTM:   DZ_REG.PRB
//                              Percentage of "homogeneous" pixels:  HOMOG_REG.PRB
//                              Percentage of "point-like" pixels:   PNT_REG.PRB
//                              Average NDVI:                        NDVI_REG.PRB
//
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//=============================================================================
//================================ Destructor =================================
    
	  virtual ~CDempsterShaferBuildingClassifier ();


//=============================================================================
//================================ Query data =================================

	  bool isRoughnessSignificant(float roughness) const;

// return value:  true  if roughness > significantRoughThresh
//                false otherwise
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


	  float vegIndex(unsigned long index) const;

// return vegetation index at position index or -101 if no VI is available.
// A valid vegetation index will be in the interval [-100,100]
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  float sigmaVegIndex(unsigned long index) const;

// return sigma of vegetation index at position Iu_index or -1 if it is 
// unavailable. The units will be [%].
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - Are specific input sensors to be used or not  - - - - - - -

	  bool useDz() const { return ((sensorOption & UseDz) == UseDz); };

	  bool useRoughStr()  const { return ((sensorOption & UseRoughStr) == UseRoughStr); };

	  bool useRoughIso()  const { return ((sensorOption & UseRoughIso) == 1); };

	  bool useFirstLastPulse() const { return (pFirstLast != 0); };

	  bool useVegIndex() const { return (pVegIndex != 0); };

	  bool useVegIndexSigma() const { return (pVegIndexSigma != 0); };
	  bool useExistingData() const { return (pExistingBuildingMask != 0); };

	  bool isValidPixel(unsigned long index) const
	  {
		  return (this->pNormDSM->pixFlt(index) > -9000.0);
	  };

	  static CLookupTable getClassLookup(int nClasses);

//=============================================================================
//====== Initialise parameters of the probability mass model from files  ======

    void initParametersFromFile(const CCharString &fileName);

// Initialise the parameters, that is f_min_, f_max_, f_prbMin_, f_prbMax_
// (cf. documentation of class CDempShSensorRecord in this file) for the cubic and 
// linear models from file fileName. 
//
// File format:
// In the first line, there has to be the key word DEMPSTER_SHAFER; after that,
// each line contains the model parameters for one input "Sensor" in the 
// format:
// SENSORKEY f_prbMin_  f_prbMax_ f_min_ f_max_
// where SENSORKEY is a keyword for the sensor. Only the parameters for sensors
// available on the file will be changed; for the others, the built-in parameters
// will be used.
//
// Sample file (with default parameters, i_usePercentageForHomogeneity = 0):
// DEMPSTER_SHAFER
// DZ_PIXEL           0.05  0.95   1.50   3.00
// HOMOGENEITY_PIXEL  0.05  0.95   0.60   0.80
// ISOTROPY_PIXEL     0.05  0.95   0.50   0.90
// FIRSTLAST_PIXEL    0.05  0.95   1.50   3.00
// NDVI_PIXEL         0.05  0.95  76.00 166.00
// DZ_REGION          0.01  0.99   2.00   3.00
// HOMOGENEOUS_REGION 0.05  0.95   0.00   0.60
// POINT_REGION       0.05  0.95   0.30   0.75
// NDVI_REGION        0.05  0.95  76.00 166.00
//    
// Comments:  
//
// DZ_PIXEL:           DSM-DTM, pixel-wise classification, min/max in [m]
// HOMOGENEITY_PIXEL:  strength of DSM pRoughness, pixel-wise classification; 
//                     min/max in [%] of pixels. min/max will be set at 
//                     values so that for the smoothest 60% of pixels, the
//                     probability mass (of class T) will be 5%, whereas for 
//                     the roughest 20% (100-80), it will be 95%. 
//                     If i_usePercentageForHomogeneity is 0, the min/max quantiles
//                     of pRoughness strength will be used instead of the percentages, 
//                     and the cubic/linear model will be applied to the pRoughness
//                     values themselves. 
//                     If i_usePercentageForHomogeneity is 1, a lookup table for 
//                     quantiles with a bin size of 2.5% will be created, and teh model
//                     will be applied to the percentages.
//                     (0.6 + 0.8) / 2 = 70% will be the "significance threshold".
// ISOTROPY_PIXEL:     Directedness of DSM pRoughness, pixel-wise classification, 
//                     min/max in [0,1]
// FIRSTLAST_PIXEL:    Height differences between first and last pulse, 
//                     min/max in [m]
// NDVI_PIXEL:         NDVI, pixel-wise classification, min/max in [grey levels],
//                     thus in [0,255]; 
//                     grey level   0 means "no data";
//                     grey level   1 means NDVI = -100%;
//                     grey level 128 means NDVI =    0%;
//                     grey level 255 means NDVI = +100%;
// DZ_REGION:          average DSM-DTM, region-wise classification, min/max in [m]
// HOMOGENEOUS_REGION: percentage of "homogeneous" pixels; region-wise 
//                     classification, min/max in [%]
// POINT_REGION:       percentage of "point-like" pixels; region-wise 
//                     classification, min/max in [%]
// NDVI_REGION:        average NDVI, region-wise classification, min/max
//                     in [grey levels] (as in pixel-wise classification);    
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void initPixelProbabilitiesFromFiles();

    void initRegionProbabilitiesFromFiles();

// Initialise empirical probability mass models for pixel-wise/region-wise
// classification from files with hard-coded names (cf. above - documentation of
// the constructor.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============================== Classification  ==============================
    
    void classifyPixels(eDECISIONRULE rule, int maxIterPostPrc, 
		                const CCharString &path, 
						const CCharString &suffix);

// Pixel-wise classification. *this will be a palette image with the 
// classification results. 
// Ie_rule:           Decision rule for Dempster-Shafer classification
// suffix:        Suffix for output file names
// maxIterPostPrc: Maximum number of iterations for post-processing
//                    (0 if no post processing is to be carried out) 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void classifyRegions(CLabelImage *buildingLabels, 
		                 vector <CDSMBuilding> &regionVector,
                         eDECISIONRULE rule, bool useWaterModel,
						 const CCharString &path,
						 const CCharString &suffixx);

// Region-wise classification. *this will receive the classification results
// for the initial building regions.
//
// IpC_buildingLabels: Building label image before classification, (only used 
//                     for writing the classification results to *this)
// regionVector:   In:  Vector of initial building regions
//                     Out: Same vector, but initial building regions classified
//                          as non-buildings will be reset.
// Ie_rule:           Decision rule for Dempster-Shafer classification
// suffix:        Suffix for output file names
// maxIterPostPrc: Maximum number of iterations for post-processing
//                    (0 if no post processing is to be carried out) 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//===================== Export of (intermediate) results  =====================
   
    void exportPixelResults(const CCharString &fileNameBase,
                              const CCharString &suffix);

// Export *this as palette-TIFF file with lookup-table C_dempsterShaferLUT(...)
// (cf. ph_dsmutils.hpp).
// The file name will be fileNameBase + "_class" + suffix + ".tif"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void exportProbabilitiesPixels(const CCharString &path, const CCharString &suffix) const;

// export the pixel-wise probability masses to grey level TIFF files.
// File names:
// "prob_DZ"   + suffix + ".tif": probability mass for B or T (DSM-DTM)
// "prob_W"    + suffix + ".tif": probability mass for T (pRoughness strength)
// "prob_Q"    + suffix + ".tif": probability mass for T (pRoughness directedness)
// "prob_FL"   + suffix + ".tif": probability mass for T (first -last pulse)
// "prob_NDVI" + suffix + ".tif": probability mass for T or G (NDVI)   
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void exportProbabilitiesRegions(CLabelImage *buildingLabels,
		                            const vector <CDSMBuilding> &regionVector,         
									const CCharString &suffix) const;

// export the region-wise probability masses to grey level TIFF files.
// File names:
// "prob_DZ_reg"   + suffix + ".tif": probability mass for B or T (average DSM-DTM)
// "prob_Hom_reg"  + suffix + ".tif": probability mass for B or G or S (% of homogeneous pixels)
// "prob_Pnt_reg"  + suffix + ".tif": probability mass for T (% of homogeneous pixels)
// "prob_NDVI_reg" + suffix + ".tif": probability mass for T or G (average NDVI)   
// "prob_W_reg"    + suffix + ".tif": probability mass for T (average pRoughness strength)
// "prob_Q_reg"    + suffix + ".tif": probability mass for T (average pRoughness directedness) 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============================= protected methods =============================

   protected:


//=============================================================================
//============================== Initialise data ==============================

    void initDempsterShaferPixel();

// Initialise parameters for the cubic / linear models of the probability masses
// for the pixel-wise classification.
//
// Defaults:
//
// CDempShSensorRecord      f_prbMin_  f_prbMax_   f_min_     f_max_
// deltaHRecordPix            0.05         0.95     1.5 m       3 m
// roughnessStrRecordPix      0.05         0.95     0.6 (60%)   0.8 (80%) 
//                    (if i_usePercentageForHomogeneity == 1, the defaults 
//                    will be
//                    0.05         0.95     1 - 2*Pt    1.0 (100%) 
//                    where Pt = 1-quantile of surface pRoughness analysis
// roughnessIsoRecordPix      0.05         0.95     0.5         0.9
// firstLastRecordPix            0.05         0.95     1.5 m       3 m
// NDVIRecordPix          0.05         0.95     76[gl]    166 [gl]
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void initDempsterShaferRegions();

// Initialise parameters for the cubic / linear models of the probability masses
// for the region-wise classification.
//
// Defaults:
//
// CDempShSensorRecord      f_prbMin_  f_prbMax_   f_min_       f_max_
// deltaHRecordReg            0.01       0.99       2 m          3 m
// homogRecordReg         0.05       0.95       0.0 ( 0%)    0.6  (60%)
// pointRecordReg           0.05       0.95       0.3 (30%)    0.75 (75%)
// NDVIRecordReg          0.05       0.95       76[gl]     166 [gl]
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============================ Post-classification ============================

    void postClassify(const CImageBuffer &dempsterShaferConflict,
                      const CImageBuffer &dempsterShaferSecond,
                      int neighbourhoodConf, int neighbourhoodMax,
                      int maxIterPostPrc);

// Pixel-wise post-classification. 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    int changeUncertain(const CImageBuffer &dempsterShaferConflict,
                        const CImageBuffer &dempsterShaferFirst,
                        const CImageBuffer &dempsterShaferSecond,
                        vector<int> &changedClasses,
                        unsigned long iterationIndex,
						int neighbourhood);

    int changeSingular(const CImageBuffer &dempsterShaferFirst,
                       const CImageBuffer &dempsterShaferSecond,
                       vector<int> &changedClasses,
                       unsigned long iterationIndex,
                       int neighbourhood);


//=============================================================================
//========================= Assign probability masses =========================
  
	void probDZpix(float dZ);

// compute probability mass P_dz for height difference dZ (DSM - DTM) using 
// the model described by e_probMode_.
// P_dz     is assigned to B or T 
// 1 - P_dz is assigned to Theta \ (B or T), thus (G or S)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void probRoughStrPix(float strength);

// compute probability mass P_s for DSM roughess strength strength using the model 
// described by e_probMode_.
// P_s     is assigned to T 
// 1 - P_s is assigned to Theta \ T, thus (B or G or S)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
    
    void probRoughDirPix(float dir, float strength);

// If the DSM pRoughness strength strength is not significant, 1 is assigned to
// Theta and 0 to all other classes.
// If the DSM pRoughness strength strength is not significant, the probability
// mass P_d for DSM roughess directedness dir is computed, using the model 
// described by e_probMode_.
// P_d     is assigned to T 
// 1 - P_d is assigned to Theta \ T, thus (B or G or S)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void probFLpix(float dFL);

// compute probability mass P_fl for height difference dFL between first 
// and last pulse using the model described by e_probMode_.
// P_fl     is assigned to T 
// 1 - P_fl is assigned to Theta \ T, thus (B or G or S), or to Theta,
//          depending on i_firstLastComplementaryClass_
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void probNpix(float NDVI, float sigma);
  
// if NDVI NDVI <= 0 or if sigma_{NDVI} sigma > sigmaMax, the NDVI
// cannot be used; thus, 1 is assigned to Theta and 0 to all other classes.
// Otherwise, the probability mass P_N for the NDVI NDVI is computed, using
// the model described by e_probMode_.
//
// If sigma < 0 (thus, if no sigma is available), 
// P_N     will be assigned to (T or G)
// 1 - P_N will be assigned to Theta \ (T or G), thus (B or S)
//
// If sigma >= 0 (thus, if sigma is available), 
// P_sig = sigma *sigmaMultiplicator is computed as the uncertainty 
// measure for NDVI.
// P_sig                   will be assigned to Theta
// (1 - P_sig) * P_N       will be assigned to (T or G)
// (1 - P_sig) * (1 - P_N) will be assigned to Theta \ (T or G), thus (B or S)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void probExistingData(unsigned char existingValue);

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void probabilityMassesRegion(const CDSMBuilding &region,
		                         bool useWaterModel);

// Assign probability masses for region-wise classification of region
// - Compute the probability mass P_dz (average DSM-DTM)
//   P_dz     is assigned to B or T
//   1 - P_dz is assigned to Theta \ (B or T), thus (G or S)
// - If homogRecordReg.f_prbMax() == homogRecordReg.f_prbMin(), the percentage of 
//   homogeneous pixels is no longer used 
//	 => 1 is assigned to Theta, 0 to all other classes
//   Otherwise, compute the probability mass P_h (homogeneous %)
//   P_h     is assigned to B or G or S
//   1 - P_h is assigned to Theta \ (B or G or S), thus T
// - Compute the probability mass P_p (point-like %)
//   P_p     is assigned to T
//   1 - P_p is assigned to Theta \ T, thus (B or G or S)
// - If the NDVI is available, compute P_N(average NDVI)
//   P_N     is assigned to T or G
//   1 - P_N is assigned to Theta \ (T or G), thus (B or S)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============ Print statistics of Dempster-Shafer classification  ============

    void printDSpix(float dZ, float w,  float q, 
                      float dFL, float NDVI) const;

    void printDSreg(int index, float dz, float homogRatio,  
                    float pointRatio, float NDVI,
                    int Class) const;

// Dump input parameters and initial / combined probability masses, support,
// plausibility, and dubiety to screen (for debugging)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//======================== Print empirical histograms  ========================

    void getHistogramDZ(vector<float> &indices,
                        vector<unsigned long> &histogramBuild,
                        vector<unsigned long> &histogramTree,
                        vector<unsigned long> &histogramGrass,
                        vector<unsigned long> &histogramSoil,
                        const CImageBuffer *conflict, 
                        float width, float max,
                        float maxConflict) const;


    void getHistogramRoughStr(vector<float> &indices,
                              vector<unsigned long> &histogramBuild,
                              vector<unsigned long> &histogramTree,
                              vector<unsigned long> &histogramGrass,
                              vector<unsigned long> &histogramSoil,
                              const CImageBuffer *conflict, 
                              float width, float max,
							  float maxConflict) const;

    void getHistogramRoughDir(vector<float> &indices,
                              vector<unsigned long> &histogramBuild,
                              vector<unsigned long> &histogramTree,
                              vector<unsigned long> &histogramGrass,
                              vector<unsigned long> &histogramSoil,
                              const CImageBuffer *conflict, 
                              float maxConflict) const;

    void getHistogramFL(vector<float> &indices,
                        vector<unsigned long> &histogramBuild,
                        vector<unsigned long> &histogramTree,
                        vector<unsigned long> &histogramGrass,
                        vector<unsigned long> &histogramSoil,
                        const CImageBuffer *conflict, 
                        float width, float min, 
                        float max, float maxConflict) const;

    void getHistogramNDVI(vector<float> &indices,
                          vector<unsigned long> &histogramBuild,
                          vector<unsigned long> &histogramTree,
                          vector<unsigned long> &histogramGrass,
                          vector<unsigned long> &histogramSoil,
                          const CImageBuffer *conflict, 
                          float maxConflict) const;

// Get histograms for all five input "sensors" in the pixel-wise classification,
// divided by the classes of the initial classification. Only pixels with
// a conflict smaller than maxConflict are considered. For limited input
// parameters (surface pRoughness strength and directedness, NDVI), fixed
// histogram spacings are used; otherwise, width, min, and max
// are the bin width and minimum/maximum value of the histogram.
// indices: a vector containing the bin centres of the histograms.
// IpC_conflict: the "conflict image"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    CCharString getHistogramsCSV(const vector<float> &indices,
                                 const vector<unsigned long> &histogramBuild,
                                 const vector<unsigned long> &histogramTree,
                                 const vector<unsigned long> &histogramGrass,
                                 const vector<unsigned long> &histogramSoil,
                                 const CCharString &title) const;

// returns a string containing the histograms in a format corresponding to
// a comma separated file (CSV), for MS EXCEL
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void printHistograms(const CImageBuffer *conflict, float maxConflict) const;

// compute all histograms of pixel-wise classification and write them to
// the protocol in CSV format.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	static CCharString getArgument(CCharString str, const CCharString &keyword);

	static void resetVec(vector<unsigned long> &histo, unsigned long length);

	static void resetVec(vector<float> &indices, unsigned long length);

//=============================================================================
//=============================================================================

};

//****************************************************************************

#endif
#endif // WIN32