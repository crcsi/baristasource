#ifdef WIN32
//===================================================== (020125) === 060317 ===
//
// File        : DSMBuilding.h
//
// Author      : FR
//
// Description : A class for building extraction from DEM
//
//=============================================================================

#ifndef ____CDSMBuilding____
#define ____CDSMBuilding____

#include <fstream>
using namespace std;

#include "CharString.h"
#include "Label.h"
#include "2DPoint.h"
#include "3DPoint.h"
#include "ImgBuf.h"
#include "TFW.h"

//*****************************************************************************
//*************************** Forward Declarations ****************************

class CBuilding; 
class CXYPolyLine;
class CXYZPolyLine;
class CXYPolyLineArray;
class CXYZPolyLineArray;
class CLabelImage;
class CDSMRoughness;
class CXYLineSegment;

//*****************************************************************************
/*!
 * \brief
 * A class corresponding to a building region detected in Airborne Laserscanner data
 * 

   It contains statistical data useful in building detection and, thus, is
   used by class CBuildingDetector, which has a vector of instances of class CDSMBuilding.

   Class CDSMBuilding additionally has data and methods for the 
   geometric reconstruction of a building by a polyhedral model,
   especially by detecting and grouping planar roof faces.
 
   In the context of building detection, it is understood that a building  
   has a raster representation in the building label image of class 
   BuildingDetector, where each pixel belonging to a certain building is marked
   by that building's index (identifier). Thus, the complete representation
   representation of the building is an area in the building label image
   and a corresponding instance of class CDSMBuilding

 * 
 * \see
 * CBuildingDetector | CDempsterShaferBuildingClassifier
 */

class CDSMBuilding
{
  protected:

//=============================================================================
//=================================== Data ====================================

	  int index;
   
	  // Index (number) of the building region. Identical to the index in the
	  // vector BuildingDetector::C_regionVector_ corresponding to this region
  
	  int rMin, cMin, width, height;
	  // Window in the DSM used for building extraction that completely contains
	  // this building regions [pixel co-ordinates]
   
	  C3DPoint pMin, pMax;

	  C3DPoint centreOfGravity;
	  // Centre of gravity of the building

	  C2DPoint extents;
	  // linear building extents, calculated from moments

	  double angle;
	  // main direction of building, computed from moments

	  C3DPoint squaredSums;
	  // Squared sums for computation of moments

	  unsigned long nPixels;
	  // Total number of DSM pixels assigned to this building region

	  unsigned long nPointPixels;
	  // Total number of "point" pixels (classified by polymorphic feature 
	  // extraction) in this building region

	  unsigned long nHomogeneousPixels;
	  // Total number of "homogeneous" pixels (classified by polymorphic feature 
	  // extraction) in this building region

	  float rmsError;
	  // r.m.s. error of DSM heights with respect to averageHeight

	  float averageNDVI;
	  // average NDVI; NDVI is weighted by 1 / (sigma_{NDVI}^2) if sigma_{NDVI}
	  // is available

	  float ndviSigmaSum;
	  // sum(sigma_{NDVI}^2) for considering weights in computing averageNDVI

	  float averageFL;
	  // average Height difference between first and last pulse data

	  float averageHomogeneity;
	  // average DSM roughness strength

	  float averageIsotropy;
	  // average DSM roughness directedness

	  float boundaryGradient;
	  // average gradient at boundary of region
	  // currently only used in an experimental way

	  unsigned long boundaryPixels;
	  // number of boundary pixels of building region

	  CImageBuffer *pDSM;
	  // Data buffer containing an extract of the DSM in the window C_border_
	  // of the overall DSM used for building extraction

	  CImageBuffer *pBuildingMask;

	  CTFW trafo;

	  CBuilding *building;

//=============================================================================

  public:

//=============================================================================
//=============================== Constructors ================================ 
//
// The constructors will initialize all pointers by 0
//
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	 
	  CDSMBuilding();

// Initialize all data by 0 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  CDSMBuilding(int Index);

// Initialize all data by 0 except index, which is initialized by Index
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  CDSMBuilding(const CDSMBuilding &source);

// Initialize the following data from source:
//   trafo, index, nPixels, C_border_, nPointPixels,  
//   nHomogeneousPixels, floorHeight, maxHeight, rmsError, 
//   averageNDVI, averageFL, averageHomogeneity, averageIsotropy, 
//   centreOfGravity, extents, angle, boundaryGradient, 
//   boundaryPixels, squaredSums, ndviSigmaSum
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//================================ Destructor =================================

	  virtual ~CDSMBuilding();


//=============================================================================
//================================ Query data =================================

	  CBuilding *getBuilding() const { return this->building; };

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - Parameters mainly for building detection  - - - - - - - - -

	  int getIndex() const { return this->index; };

	  unsigned long getNPixels() const { return this->nPixels; };
   
	  unsigned long getNPointPixels() const { return this->nPointPixels; };
   
	  unsigned long getNHomogeneousPixels() const { return this->nHomogeneousPixels; };
   
	  double getFloorHeight() const { return this->pMin.z; };

	  C3DPoint getCentreOfGravity() const { return this->centreOfGravity; };

	  C2DPoint getExtents() const { return this->extents; };

	  double getAngle() const { return this->angle; };

	  double getMaxHeight() const { return this->pMax.z; };
   
	  double getAverageHeight() const { return this->centreOfGravity.z; };
        
	  float getRmsError() const { return this->rmsError; };

	  float getAverageNDVI() const { return this->averageNDVI; };

	  float getAverageFL() const { return this->averageFL; };

	  float getAverageHomogeneity() const { return this->averageHomogeneity; };

	  float getAverageIsotropy() const { return this->averageIsotropy; };

	  float getBoundaryGradient() const { return this->boundaryGradient; };

	  unsigned long getBoundaryPixels() const { return this->boundaryPixels; };


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - Numbers used for building detection - - - - - - - - - -

	  float getExtentRatio() const 
	  { return (float) (this->extents.x / this->extents.y); };
      
// ratio between length and width of building
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  float getPointRatio() const 
	  { return ((float) this->nPointPixels) / ((float) this->nPixels); };

// ratio of pixels classified as "point-like" in polymorphic feature 
// extraction for building detection
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   
	  float getHomogeneousRatio() const 
	  { return ((float) this->nHomogeneousPixels) / ((float) this->nPixels); };

// ratio of pixels classified as "homogeneous" in polymorphic feature 
// extraction for building detection
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  CLabel getBuildingLabelFromIndex() const;


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - - - Limits of building  - - - - - - - - - - - - - - -
   
	  int getCMin() const { return cMin; };

	  int getRMin() const { return rMin; };

	  int getWidth() const { return width; };

	  int getHeight() const { return height; };

// Window in the DSM used for building extraction that completely contains
// this building regions [pixel co-ordinates]
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  C2DPoint getPMin2D() const { return C2DPoint(pMin.x, pMin.y); };
   
	  C2DPoint getPMax2D() const { return C2DPoint(pMax.x, pMax.y); };

// C_border transformed to [object co-ordinates]
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  C2DPoint getObjExtents() const 
	  { return C2DPoint (pMax.x - pMin.x, pMax.y - pMin.y); };

// Extents of C_border, transformed to [object co-ordinates]
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  C3DPoint getPMin() const { return this->pMin; };

	  C3DPoint getPMax() const { return this->pMin; };

// 3D box: planimetry: C_border transformed to [object co-ordinates]
//         height:     Min/Max height of DSM
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - -  Is index referring to a pixel inside the building region or not  - - -

	  bool indexIsInBuilding(unsigned long index) const;

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  bool intersectExtents(int RMin, int CMin, int Width, int Height);

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - Co-ordinate transformations - - - - - - - - - - - - -

	  C2DPoint C_reducedPoint(const C2DPoint &pos) const
	  { return C2DPoint(pos.x - centreOfGravity.x, pos.y- centreOfGravity.y); };

	  C3DPoint C_reducedPoint(const C3DPoint &pos) const 
	  { return pos - centreOfGravity; };

	  C3DPoint C_reducedDsmPoint(const C2DPoint &pos, double z) const
	  { return (C3DPoint(pos.x, pos.y, z) - centreOfGravity); };
  

//=============================================================================
//====== Write data to string for making the building region persistent =======

	  CCharString getString() const;

//=============================================================================
//============= Initialise binary building mask from label image  =============

	  void initMask(CLabelImage &LabelImage); 

// In the binary building mask, all pixels where IrC_buildingMask has the 
// label identical to the building index will be set to 1
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 



//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - -  Auxiliary method - - - - - - - - - - - - - - -


// read the data from IrC_message and extract the domain window, using 
// C_border_ and the geocoding parameters.
//
// return value: 1: OK
//               0: format error: the string IC_message was not in the correct
//                  format
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//=============================================================================

   public: 

//=============================================================================
//================= Change index of building region to newIndex ===============

	   void changeIndex(int newIndex) { index = newIndex; };


//=============================================================================
//================ reset all data for building detection to 0 =================
  
	   void resetData(int Index = -1);

// C_origin_, C_pixelSize_, rmsError,  
// centreOfGravity, C_border_, nPixels, extents, angle,  
// floorHeight, maxHeight, nPointPixels, nHomogeneousPixels,   
// averageHomogeneity, averageIsotropy, averageFL, averageNDVI 
// are set to 0
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//================================ init data ==================================

	   void initData(int Index, unsigned long NPixels, int r0, int c0);

// call reset(Index), initialize nPixels by Iu_nPixels and move
// C_border_ to (r0,c0)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//================= Copy data relevant for building detection =================
  
	   void copyData(int Index, const CDSMBuilding &source);

// set index to Index and copy all data for building detection from
// source:
// nPixels, nPointPixels, floorHeight, maxHeight, 
// averageHeight, C_origin_, C_pixelSize_, rmsError, C_border_, 
// and nHomogeneousPixels
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//=============================================================================
 
	   CDSMBuilding &operator= (const CDSMBuilding& source);

//=============================================================================
//====================== Methods for building detection =======================
//
// These methods are to be used after the connected component analysis to 
// compute the following parameters of each building region:
// centreOfGravity, C_border_, nPixels, extents, angle,  
// floorHeight, maxHeight, nPointPixels, nHomogeneousPixels,   
// averageHomogeneity, averageIsotropy, averageFL, averageNDVI
 
	   void initDataEditing();

// Initialize data for pixelwise editing; after that, v_editData has to
// be called for pixels inside the building region to update the following
// data: 
// centreOfGravity, C_border_, nPixels, extents, angle,  
// floorHeight, maxHeight, nPointPixels, nHomogeneousPixels,   
// averageHomogeneity, averageIsotropy, averageFL, averageNDVI
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void editData(float DTMHeight, float DSMHeight, int r, int c, 
		             bool isPoint, bool isHomogeneous, float w, float q);

	   void editData(float DTMHeight, float DSMHeight, int r, int c, 
					 bool isPoint, bool isHomogeneous,  float w, float q, 
					 float NDVI, float DeltaFL, float sigmaNDVI);

// Accumulate data for the pixel at position IrC_pos [Pixel coordinates] to 
// change nPointPixels, nHomogeneousPixels, 
// floorHeight, maxHeight, averageHeight, and C_border_.
//
// If_dtmHeight:     DTM height at position IrC_pos
// If_dsmHeight:     DSM height at position IrC_pos
// Ii_isPoint:       is the pixel classified as a "point pixel" in polymorphic
//                   feature extraction or not
// Ii_isHomogeneous: is the pixel classified as "homogeneous" in polymorphic
//                   feature extraction or not
// If_w:             DSM roughness strength at position IrC_pos
// If_q:             DSM roughness directedness at position IrC_pos
// If_FL:            height difference between first and last pulses at 
//                   position IrC_pos
// Ii_NDVI:          NDVI [0..255] at position IrC_pos
// If_sigmaNDVI:     sigma_{NDVI} at position IrC_pos [%]
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void finishDataEditing(int increaseSize, const C2DPoint &dsmOrg,
                              const C2DPoint &dsmPixSize,
							  int maxWidth, int maxHeight);

// Increase C_border by a security range of Ii_increaseSize pixels in all 
// directions. 
//
// IrC_dsmOrg:     Centre of the "upper left" pixel of the DSM in object 
//                 co-ordinates.
// IrC_dsmPixSize: Pixel size of the DSM (copied to C_pixelSize_) in 
//                 object units
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 



//=============================================================================
//================== Compute RMS error of the average height ==================

	   void computeRMS(CImageBuffer &NormDSM, CLabelImage &LabelImage);

// IrC_normDSM:    the normalised DSM from building detection. 
// IrC_labelImage: the label image from building extraction.
//                 The RMS error is computed from all pixel in IrC_normDSM
//                 where IrC_labelImage == index
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//===================== Compute average boundary gradient =====================

	   void computeBoundaryGradient(CLabelImage &BuildingLabels, 
		                            const CDSMRoughness &roughness);
   
//=============================================================================
//========================== Create prismatic model  ==========================

	   bool createPrismaticModel(const CCharString &dir, float thinningThreshold);


//=============================================================================
//============== Export boundary polygons of the building region ==============

	   CXYZPolyLineArray *getRoofPolys(const CCharString &dir, float thinningThreshold);

// Get the boundary polygons of the building region (2D object co-ordinates).
// The first polygon is the outer loop, the others are inner loops (holes).
// This method only gives the boundary polygons of the results of building
// detection!
//
// If no polygon has been found, 0 will be returned. 
// Otherwise, the number of elements of the vector will be identical to the
// number of detected polygons. The client is responsible for deleting all
// elements of the vector and the vector itself.
//
// thinningThreshold: The threshold for line thinning by the splitting
//                       method.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void exportBoundaryPolys(float thinningThreshold,
		                        ofstream *winputFile,
								ofstream *dxfFile,
								unsigned long &Linecode, 
								int vrmlFlag,
								const CCharString &directory);

// Extract the boundary polygons of the building mask region 
// (2D object co-ordinates) and write them to the WINPUT file XpC_winputFile
// Starting with line code 50000000 + Xru_linecode.
// Xru_linecode will be increased by 1 for each polygon exported.
// if (XpC_dxfFile != 0) the polygons will also be written to the 
// DXF file XpC_dxfFile
// if (Ii_vrmlFlag != 0), a block model (horizontal roof) will be created
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============== Initialize / Clear local copies of the DSM and ===============
//==============     other data for building reconstruction     ===============
  
	   void clearDSMdata();

// Delete the following local data buffers for the window given by C_border_:
//
// pDSM,       pC_buildingMask_, pC_segmentMask_, pC_regionLabelImage_,
// pC_foerstner_, pC_voronoiImage_, pC_distanceImage_
//
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============================= Auxiliary methods =============================

	   CCharString createRegionDir(const CCharString &path) const;

	   void deleteRegionDir(const CCharString &path) const;

// Create / delete directory REGION_NN, where NN is the building number.
// C_createRegionDir() returns the directory name
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   static void getColour(unsigned long index, 
		                     unsigned char &r, unsigned char &g, unsigned char &b);
   

	   void getColour(unsigned char &r, unsigned char &g, unsigned char &b) const 
	   { return getColour(index, r, g, b); };

// return color for the building label image, depending on the building index
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//=============================== Export methods ==============================
   
	   void print (ostream &str) const;

// print parameters of the building region, including individual face 
// parameters, to OrC_ostream
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	
	   void print (const CCharString &string) const;

// Open file IrC_string and print parameters of the building region, including 
// individual face parameters, to that file
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void printDef(const CCharString &directory) const;

// print(createRegionDir(directory) + "region.dat");
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============================= protected methods =============================

  protected: 

	  void expand(int r, int c);

//=============================================================================
//===== Set transformation parameters in a way consistent with C_border_  =====
    
 
	  void setOrgAndPixSize(const C2DPoint &DSMOrg,
		                    const C2DPoint &DSMPixSize);

//=============================================================================
//===================== Compute maximum boundary gradient =====================

	  void maxBoundaryGradient(CLabelImage &BuildingLabels, 
                               const CDSMRoughness &roughness, 
                               const CXYLineSegment &segment,
                               C2DPoint &maxGrad, 
                               int &maxIndexX, int &maxIndexY,
                               bool squareFlag);

// IrC_foerstner:      Object for DSM roughness analysis
// IrC_buildingLabels: Building label image
// Ii_squareFlag:      mode of gradient computation:
//                     0:   max. height difference;
//                     <>0: squared gradient norm
// IrC_segment:        2D segment along which the first derivatives
//                     are sampled.
// This sampled segment of DSM derivatives is analysed within +-3
// pixels from the boundary of the building region (determined from
// IrC_buildingLabels): both the maximum gradient and its index in
// the sampled segment of the gradients is determined. As the segment
// intersects the building region at least twice, the first and
// the last maximum are determined and stored in OrC_maxGrad.x /
// OrC_maxIndex.x and in OrC_maxGrad.y / OrC_maxIndex.y, resp.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//========================== Create prismatic model  ==========================

	  void addPoly(CXYZPolyLine &roofPoly);

// Add a vertical prism to the prismatic model; if roofPoly is the first 
// polygon, the prismatic model will be initialised by that model; all further
// prisms will be cut out from that initial one (i.e., they are considered to
// be holes)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//=============================================================================

};

//**************************************************************************** 
//****************************************************************************

#endif
#endif // WIN32
