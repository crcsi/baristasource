#ifdef WIN32
//===================================================== (020125) === 060317 ===
//
// File        : BuildingReconstructor.h
//
// Author      : FR
//
// Description : A class for building reconstruction
//
//=============================================================================

#ifndef ____CBuildingReconstructor____
#define ____CBuildingReconstructor____


#include "TFW.h"
#include "m_FeatExtract.h"
#include "ProgressHandler.h"
#include "BuildingFaceNeighbourGraph.h"
#include "MultiImageSegmenter.h"

//*****************************************************************************
//*************************** Forward Declarations  ***************************

class CImageBuffer;
class CBuilding;
class CLabelImage;
class CObservationHandler;
class CALSDataSet;
class CALSPointCloudBuffer;
class CALSPoint;
class CHugeImage;
class C3DPoint;
class CRAG;
class C3DAdjustPlane;
class CXYLineSegment;
class CXYZHomoSegment;
class CBuildingFace;
class CLineMatchingHypothesis;

/*!
 * \brief
 * A class for building reconstruction from multiple aerial images and ALS data
 * 
  
   This class is work in progress
	
 * 
 * 
 * 
 */


class CBuildingReconstructor: public MultiImageSegmenter
{

  protected:

//=============================================================================
//=================================== Data ====================================

	  CBuilding *building;
   
	  vector<vector<CXYLineSegment *>> imageStraightLineVector;

	  vector<C2DPoint> imageOffsets;

	  CImageBuffer *pDSMPlanes;

	  CImageBuffer *pDTM;   

	  CALSPointCloudBuffer *ALSpoints;

	  CBuildingFaceNeighbourGraph *planeGraph; 
	
	  vector<vector<CALSPoint *>> pointsInPlanes;

	  vector<unsigned long> ALSPointLabelIndex;

	  unsigned int nALSPoints;

	  C3DPoint reduction;

	  vector<double> smoothness;

	  double resolutionALS;

	  double sigmaALS;

	  int neighbours;

	  double alpha; 

	  double maxAngle;

	  double minHeight; 

	  vector<double> imagePixelSize;

//=============================================================================

  public:

//=============================================================================
//=============================== Constructors ================================ 
	 
	  CBuildingReconstructor(double SearchBuf, int nNeighbours, 
							 double MaxAngle, double SigmaALS, double Alpha, 
							 double dZmin, const FeatureExtractionParameters &Pars);


//=============================================================================
//================================ Destructor =================================

	  virtual ~CBuildingReconstructor();


//=============================================================================
//================================ Query data =================================

	  CBuilding *getBuilding() const { return this->building; };

	  CSensorModel *getSensorModel(int imageIndex) const 
	  { return this->sensorModelVector[imageIndex]; };

//=============================================================================
	  
	  bool initDataForBuilding(CBuilding *Building, 
		                       vector<CObservationHandler *> *imgVec, 
							   vector<CHugeImage *> *hugImgVec, 
							   CALSDataSet *ALS, const CCharString &path, 
							   double pixImg, double pixDSM);


//=============================================================================

	  double getSigmaThreshold(int redundancy) const;

//=============================================================================

	  bool reconstruct();

//=============================================================================

	  void exportBuffersToTiff() const;

	  void exportImagesWithALSToTiff() const;

	  void exportPointCloud() const;

	  void exportEdges() const;

	  
//=============================================================================

  protected:

	  long getObjectLabelIndex(const CALSPoint *pALS) const;
	  long getLabelIndex(const CLabelImage &labels, const CALSPoint *pALS) const;

	  unsigned short getObjectLabel(const CALSPoint *pALS) const;
	  unsigned short getProjectedLabel(unsigned int projectedIndex, const CALSPoint *pALS) const;
	  unsigned short getLabel(const CLabelImage &labels, const CALSPoint *pALS) const;

	  void exportEdges(int index) const;
	  void exportPointCloudSeparated(const char *fnbase) const;

	  void exportNPoints(const CCharString &filename, 
		                 const vector<CBuildingFace *> &planeVector, 
						 const CLabelImage &labels) const;

	  float getDTMHeight(const C2DPoint &p) const;
	  float getDTMHeight(const C3DPoint &p) const;

	  bool initALS(CALSDataSet *ALS, double pixelSize);

	  bool kMeansPlanes(vector<CALSPoint *> &pointVec);


	  bool initDTM(double pixelSize);

	  virtual bool segmentImages();

	  virtual bool projectSegmentedImages();

	  bool initRoughness(double maxDist);

	  bool checkHeights(CLabelImage &labels);

	  void checkCombinedHeights();

	  bool addFace(CBuildingFace *face, int nPtsMin);

	  bool mergeCoplanarFaces(int nPtsMin, bool useVoronoi);

	  void classifyNeighbours();

	  bool initialiseObjectPlaneALS(CBuildingFace *plane, vector<CALSPoint *> &pointVec);

	  bool initialiseObjectPlane(int imageIndex, CBuildingFace *plane, vector<CALSPoint *> &pointVec);

	  bool matchWithObjectPlane(unsigned short objectLabel, int imageIndex, CBuildingFace *plane, vector<CALSPoint *> &pointVec,
		                        vector<vector<CBuildingFace *>> &planeVector, vector<vector<vector<CALSPoint *>>> &planePointVector,
								int minPts);

		
	  CLabelImage *getProjectedPlaneWithPointCoverage(int imageIndex, CBuildingFace *plane, vector<CALSPoint *> &pointVec);

	  CLabelImage *getLabelWithPointCoverage(vector<CALSPoint *> &pointVec);

	  CBuildingFace *computePlanesRobust(const CCharString &heading, CLabelImage &labels,
										 vector<CBuildingFace *> &planeVector, 
		                                 vector<vector<CALSPoint *>> &planePointVector, int nPtsMin,
										 float sigmaMin);

	  CBuildingFace *computePlanesRobust(vector<vector<CBuildingFace *>> &planeVector, 
		                                 vector<vector<vector<CALSPoint *>>> &planePointVector,
										 int &imageIndexOfReturnedFace, int nPtsMin, float sigmaMin);
	  bool resetToOneBuilding();

	  void resetOutsideBuilding();

	  void checkNearPoints(double dist, int nPtsMin);

	  unsigned short getBestFaceLabel(CALSPoint *pALS);

	  bool initialiseFaces(int nPtsMin);
	  
	  bool initialiseFacesALS(int nPtsMin);

	  bool computePlanes(bool writeToProtocol);

	  void getUnusedPointsWithLabels(CLabelImage &pointLabels, vector<CBuildingFace *> &faceVec, vector<vector<CALSPoint *>> &pointVec, double dist);

	  bool computePlanes(const CCharString &heading, 
		                 CLabelImage &labels, 
						 vector<CBuildingFace *> &planes,
						 vector<vector<CALSPoint *>> &pointVec,
						 bool writeToProtocol,
						 bool splitBadFits);

	  bool computePlanesWithDSM(bool writeToProtocol);

	  bool splitBadFits(CLabelImage &labels, vector<CBuildingFace *> &planes, 
		                vector<vector<CALSPoint *>> &pointVec,
						unsigned short plane, 
						bool writeToProtocol);

	  void mergePlanes(int Lab, CLabelImage &labels, vector<CBuildingFace *> &planes, 
		               vector<vector<CALSPoint *>> &pointVec);

	  virtual void resetImageVectors();

	  void resetALS();

	  virtual bool initImage(CObservationHandler *oh, CHugeImage *hugImg, double pixSize);
	  
	  bool computeRobust(CBuildingFace &plane, vector<CALSPoint *> &pointVec, int maxElim, int nPtsMin);
  
	  bool matchEdges(unsigned int minPts);

	  bool findMatchingCandidatesInImages(const CXYZPoint &P1, const CXYZPoint &P2, 
		                                  vector <vector <CXYLineSegment *> > &imageEdges,
								          vector<vector<int> > &indexVector, 
										  vector<ofstream *> &dxfVec);

	  bool findMatchingCandidates(const CXYZPoint &P1, const CXYZPoint &P2, 
		                          CBuildingFace *pFace, CBuildingFaceNeighbourRecord *pNeighbour, 
		                          vector <CLineMatchingHypothesis *> &candidates,
								  float minLength, vector<ofstream *> &dxfVec);

	  void simplifyMultipleCandidates(CBuildingFace *pFace, 
		                              vector <CLineMatchingHypothesis *> &candidates);

	  bool findMatchingCandidates(CBuildingFace *pFace, CBuildingFaceNeighbourRecord *pNeighbour, 
		                          vector <CLineMatchingHypothesis *> &candidates,
								  float minLength, vector<ofstream *> &dxfVec);

	  bool matchEdges(CBuildingFace *face, vector<CALSPoint *> &pointVec,
		              vector <CLineMatchingHypothesis *> &matches, float minLength,
					  vector<ofstream *> &dxfVec, unsigned int minPts);
	  
	  int checkMergeEdges(CBuildingFace *face, vector <CLineMatchingHypothesis *> &matches);

	  void setDSMHeightsToPlanes();

	  void resetPlanes(vector<CBuildingFace *> &planes);

	  void resetPlanes();
	  void mergeTwoPlanes(int merge, int toBeMerged);
	  void exportNPts(const CCharString &filename);
	  
	  CBuildingFace *getBestFaceFromALS(vector<CALSPoint *> &points);

	  void exportToVRML(const CCharString &filename) const;
};

//**************************************************************************** 
//****************************************************************************

#endif

#endif // WIN32