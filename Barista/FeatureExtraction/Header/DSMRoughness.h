#pragma once

#ifndef __CDSMRoughness__
#define __CDSMRoughness__

#include "TFW.h"
#include "2DPoint.h"
#include "3DPoint.h"
#include "ImgBuf.h"
#include "CharString.h"

//*****************************************************************************

class CXYLineSegment;

/*!
 * \brief
 * A class for computing surface roughness parameters of a digital surface models (DSMs).
 * 
  	 This class encapsulates methods and data for two purposes:
	
	<OL CLASS="sublist">
		<LI CLASS="sublist">
			Computing the gradients of a DSM with float co-ordinates
		</LI>
		<LI CLASS="sublist">
			The classification of the pixels of a DSM with float 
            co-ordinates according to the technique of polymorphic 
            feature extraction as it was developed in Bonn 
            (Prof. Foerstner)
		</LI>
	</OL>

	It is assumed that the DSM is represented by a height grid, each height being
	represented by a float value. From the heights, three surface roughness parameters
	can be derived: 

	<OL CLASS="sublist">
		<LI CLASS="sublist">
			The homogeneity measure describes the 'strenght' of surface roughness and
			corresponds to the average second derivative of the DSM in a small window 
			around each pixel
		</LI>
		<LI CLASS="sublist">
			The isotropy measure describes the variation of the changes of the surface 
			normal directions inside a small window
		</LI>
		<LI CLASS="sublist">
			The direction measure describes the direction of change of the surface slope
		</LI>
	</OL>

	Furthermore the class contains a classification of each pixel of the DSM as belonging
	to one of the classes homogeneous, linear, and point-like.

	The details about the homogeneity and isotropy measures of DSMs can be found in: 

	Rottensteiner, F., Trinder, J., Clode, S., Kubik, K., 2004: 
	Using the Dempster Shafer method for the fusion of LIDAR 
	data and multi-spectral images for building detection. 
	In: Information Fusion 6(4), pp. 283-300.

	
 * 
 * \see
 * CBuildingReconstructor | CDempsterShaferBuildingClassifier
 */

class CDSMRoughness 
 {

//=============================================================================

   protected:

//=============================================================================
//=================================== Data ====================================

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - - - - - - Gradients - - - - - - - - - - - - - - - - -

	//! X-component of the DSM gradient (DZ / DX) 
	/*! This variable need not necessarily be initialised; check with hasGradient()*/
    CImageBuffer *pGradientX;       

	//! Y-component of the DSM gradient (DZ / DY) 
	/*! This variable need not necessarily be initialised; check with hasGradient()*/
    CImageBuffer *pGradientY;       

// 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- Surface roughness features and results of polymorphic feature extraction  -

	//! The homogeneity measure as an image data buffer with float of the same extents as the CDSMRoughness object.
    CImageBuffer Homogeneity;    
 
	//! The isotropy measure as an image data  buffer with float of the same extents as the CDSMRoughness object.
	/*! For each pixel, 0 <= Isotropy <= 1. */
    CImageBuffer Isotropy;       
 
	/*! The direction of the gradient in [1/10 gon] as an image data buffer with float elements of the same extents as the CDSMRoughness object */
    CImageBuffer Direction;   

	//! Image data buffer with the classification results of polymorphic feature extraction (8 bits per pixel, 1 band)
	/*! Any pixel might have one of the following grey values:
	    Grey value 0 => "homogeneous"
        Grey value 1 => "linear"
        Grey value 2 => "point-like" */
    CImageBuffer FoerstnerImage;    

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - - Statistics of homogeneity - - - - - - - - - - - - -

	//! Minimum homogeneity
    float minHomogeneity;  
         
	//! Maximum homogeneity
    float maxHomogeneity;  

    //! Mean value of homogeneity
	float meanHomogeneity;          

    //! Median of homogeneity
	float medianHomogeneity;        

	//! Variance of homogeneity
    float varianceHomogeneity;      
    
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - Thresholds for classification of homogeneity  - - - - - - - -

	//! proportion of pixels for computing the threshold for classification of texture: 
    /*! The threshold will be selected so that 100 * quantil % of all pixels 
	   will be classified as "homogeneous". */
    float quantil;                  

	//! Threshold corresponding to quantil. 
	/*! 100 * quantil % of all pixels have a homogeneity value smaller than 
        quantilThreshold */
    float quantilThreshold;         


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - - - - - Transformation  - - - - - - - - - - - - - - -
	
	//! Transformation between the object coordinate system and the pixel coordinate system. 
	/*! The transformation will be initialised by the object coordinates of the upper left
	    corner and the grid widths in both X and Y. Thus, no general affine transformation
		is supported. */
	CTFW trafo;

  	//! A vector containing the percentiles of the homogeneity value, in units of [deltaPercent]. 
	/*! percentiles[index] gives the value of homogeneity for which (index * deltaPercent) * 100 %
	    of the pixels have a smaller homogeneity value. */
	vector <float> percentiles; 

  	//! The unit of the vector of percentiles
	float deltaPercent;             

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - - Auxiliary static variables  - - - - - - - - - - - -
 
	//! An array containing the sinus values of the index (index given in 1/10 gon)
    static float Sines[4000];      

  	//! An array containing the cosinus values of the index (index given in 1/10 gon)
	static float Cosines[4000];     

	//! A boolean variable telling whether Sines and Cosines have been initialised or not.
    static bool isInit;             

//=============================================================================

   public:

//=============================================================================
//================================ Constructor ================================


	//! Constructor initialising all image data buffers
    CDSMRoughness(int offsetX, /*!< column index of the upper left index in the pixel coordinate system of the DSM */  
		          int offsetY, /*!< row index of the upper left index in the pixel coordinate system of the DSM */  
				  int width,   /*!< number of pixels per row  */  
				  int height,  /*!< number of image rows  */  
		          const C2DPoint &luc,  /*!< object coordinates of the left upper corner given by (offsetX/offsetY) of the image buffers  */  
				  const C2DPoint &Delta); /*!< grid width of the DSM  */  

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//================================ Destructor =================================

	//! Destructor deleting all image data buffers
    virtual ~CDSMRoughness ();


//=============================================================================
//================================ Query data =================================

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - Query data with respect to the gradients  - - - - - - - -

	bool hasGradient() const;

// return value: true  if gradient images have been initialised; only in this case
//                     may the other functions be invoked
//               false otherwise
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


	C2DPoint gradient(int index) const 
	{ return  C2DPoint(pGradientX->pixFlt(index), pGradientY->pixFlt(index)); };

// returns the gradient at the position corresponding 
// to index.
// Gradient = (DZ/DX, DZ/DY)^T
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


	float gradientStrength(int index, bool squareFlag) const;

// returns the strenght of the gradient at the position corresponding to index.
// if squareFlag == false, the gradient strenght will be equal to the maximum
// of DZ/DX and DZ/DY.
// Otherwise, the gradient strength will be the squared Euclidean norme of
// gradient(index)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


	float gradientStrength(int index, const C2DPoint &dir) const;

// returns the absolute value of the component of the gradient at the position  
// corresponding to index in direction dir, thus 
// || dir .in. gradient(index) ||
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


    vector <C3DPoint> *pDerivatives(const CXYLineSegment &segment, bool squareFlag) const;

// get a vector of gradient strenghts on a profile from the start point to the
// end point of IrC_segment, with the same pixel size as the DSM. 
//
// segment has to be given in the pixel co-ordinate system.
// 
// The client is responsible for deleting the vector.
//
// The point co-ordinates will be (X, Y, ||grad (X,Y)||, where the gradient
// strength will be computed according to squareFlag (see above).
//
// Nearest neighbour interpolation will be used for points not being directly
// situated on a pixel centre.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - - Query window extent/offset  - - - - - - - - - - - -

	int getOffsetX() const { return this->Homogeneity.getOffsetX(); } 
	int getOffsetY() const { return this->Homogeneity.getOffsetY(); } 
	int getWidth()   const { return this->Homogeneity.getWidth(); } 
	int getHeight()  const { return this->Homogeneity.getHeight(); } 
		            

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - Query surface roughness parameters  - - - - - - - - - -

	float R(int index) const { return Homogeneity.pixFlt(index); };

// Homogeneity at pixel index
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    float Q(int index) const { return Isotropy.pixFlt(index); };

// Isotropy at pixel index
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    float dir(int index) const { return Direction.pixFlt(index); };

// Direction of gradient in [1/10 gon] at pixel index
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    C2DPoint direction(int index) const   
	{ 
         index = (int) floor(Direction.pixFlt(index));
         return C2DPoint(Cosines[index], Sines[index]);    
	};

// Direction vector of gradient at pixel index
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - Query results of texture classification - - - - - - - - -

	int getClassification(int index) const { return FoerstnerImage.pixUChar(index); };

// Texture classification at pixel index
// Return value: 0 => "homogeneous"
//               1 => "linear"
//               2 => "point-like"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    bool isHomogeneous(int index) const { return (getClassification(index) == 0); };

    bool isLine(int index) const  { return (getClassification(index) == 1); };

    bool isPoint(int index) const { return (getClassification(index) == 2); };

    CImageBuffer *classification()  { return &FoerstnerImage; };

// return pointer to image data buffer with classification result
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - Query statistics of homogeneity - - - - - - - - - - - - -

    float getMeanHomogeneity() const { return meanHomogeneity; };

    float getMedianHomogeneity() const { return medianHomogeneity; };

    float getVarianceHomogeneity() const { return varianceHomogeneity; };

    float getMinHomogeneity() const { return minHomogeneity; };

    float getMaxHomogeneity() const { return maxHomogeneity; };


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - Query thresholds for classification of homogeneity  - - - - - -

    float getQuantil() const { return quantil; };

    float getQuantilThreshold() const { return quantilThreshold; };


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- Query threshold for classification of homogeneity without changing data - -
   

    float initQuantilThreshold(float Quantil, CImageBuffer *demBuf) const;

// return value: The value of homogeneity for which 100 * Quantil % of all
//               pixels are smaller.
// If demBuf != 0, only pixels with a valid DEM height are considered
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void getQuantilThresholds(float q1, float q2, float &thr1, float &thr2, 
                              CImageBuffer *demBuf);

// thr1, thr2: The values of homogeneity for which 100 * q1 % and 100 * q1 %,
//             resp,  of all pixels are smaller.
// If demBuf != 0, only pixels with a valid DEM height are considered
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - Query data percentage points  - - - - - - - - - - - -

	vector <float> percentagePoints() const { return this->percentiles; };
 
	float getPercentage(float value) const;

	float getPercentage(unsigned long index) const
	{ return this->getPercentage(this->Homogeneity.pixFlt(index)); };

	void applyPercentagePoints(CImageBuffer &buf) const;

	void applyPercentagePoints();

//=============================================================================
//========================= Initialise Gradient images ========================


	void computeGradients(CImageBuffer &DEMwin, int kernelSize);

// initialise image data buffers and compute the gradients of DEMwin as follows:
// 1.) Gradient in X/Y: 1/2 * [-1 0 1]^T and 1/2 * [-1 0 1], resp.
// 2.) If kernelSize > 1, these images will be convolved with a binomial
//     filter of size kernelSize * kernelSize 
//     (kernelSize has to be odd, otherwise the next highest odd number is
//     chosen)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============= Carry out computations for texture classification =============

    void computeRandQ(CImageBuffer &DEMwin, int foerstnerKernelSize, 
		              int gradientKernelSize, float Quantil,
					  const C2DPoint &luc, const C2DPoint &Delta,
					  bool parameteriseByPercentage);

// compute homogeneity and isotropy for DSM window DEMwin
//
// if (foerstnerKernelSize > 0), then the gradients will be initialised
// using computeGradients(DEMwin, gradientKernelSize)
//
// foerstnerKernelSize (odd number) is the size of a binomial filter by
// which the squares and the mixed products of the partial second derivatives 
// of the DSM will be convolved.
//
// If demBuf != 0, only pixels with a valid DEM height are considered
//
// The homogeneity values will be derived in units of the median of the 
// "original" homogeneity values, thus 50% of the (valid) pixels will have 
// a homogeneity < 1.0
//
// The threshold for homogeneity will be set so that 100 * Quantil % of the
// valid pixels will have a homogeneity smaller than that threshold.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


    void computeHomogeneityStatistics(CImageBuffer *demBuf, int kernelsize);

// compute statistics of homogeneity and threshold quantilThreshold, 
// If demBuf != 0, only pixels with a valid DEM height are considered
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void computeRandQandClassify(CImageBuffer &DEMwin, float Quantil, float thrQ, 
                                 int foerstnerKernelSize, int gradientKernelSize,					 
								 const C2DPoint &luc, const C2DPoint &Delta,
								 bool parameteriseByPercentage);

// compute homegeneity and isotropy for DSM window DEMwin and carry out
// the classification using the threshold derived from Quantil and thrQ
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void classify(float Quantil, float If_thrQ,
                  CImageBuffer *demBuf);

// Classify surface roughness, setting the threshold quantilThreshold for  
// homogeneity so that 100 * Quantil % of the valid pixels (according to demBuf
// if demBuf != 0) will have a homogeneity smaller than quantilThreshold.
//
// Classification: 0 ("homogeneous") if homogeneity < quantilThreshold
//                 else
//                      1 ("linear")     if isotropy <= thrQ
//                      2 ("point-like") if isotropy >  thrQ
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void classify(float thrQ);

// Classify surface roughness, using the threshold quantilThreshold for  
// homogeneity as it is.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void classifyByThreshold(float thrW, float thrQ);

// Classify surface roughness, using the thrW for homogeneity.
//
// Classification: 0 ("homogeneous") if homogeneity < thrW
//                 else
//                      1 ("linear")     if isotropy <= thrQ
//                      2 ("point-like") if isotropy >  thrQ
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//================= Set threshold for homogeneity classification ==============

    float setQuantilThreshold(float Quantil, CImageBuffer *demBuf);

// Set the threshold quantilThreshold for homogeneity so that 
// 100 * Quantil % of the pixels (of the "white pixels" if demBuf != 0) 
// will have a homogeneity smaller than quantilThreshold.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    float setQuantilThreshold(float QuantilInput, float Quantil1, float Quantil2, 
                              float &thr1,   float &thr2, CImageBuffer *demBuf);

// Set the threshold quantilThreshold for homogeneity so that 
// 100 * QuantilInput % of the pixels (of the falid if 
// demBuf != 0) will have a homogeneity smaller than quantilThreshold.
// 
// If demBuf != 0, only pixels with a valid DEM height are considered
//
// Return value: quantilThreshold
// thr1, thr2:   The values of homogeneity for which 100 * Quantil1 % and
//               100 * Quantil2 %, resp,  of all pixels are smaller.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============================== Export functions =============================

    CCharString statisticsString() const;

// returns a formatted string containing the homogeneity statistics for output 
// to screen or protocol
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void exportTiff(const CCharString &path, CCharString filename = "foerstner.tif");

// Export the classification results to TIFF file filename in directory 
// path. 
// Palette: red = "homogeneous", green = "linear", blue = "point-like"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void exportGradient(const CCharString &path, CCharString filename = "gradient.tif",
                        bool squareFlag = true);

// Export the gradient strength results to TIFF file filename in directory 
// path. squareFlag: cf. gradientStrength(...)
// 
// Palette: red = 0, otherwise the grey value is a linear interpolation between
// the maximum and the minimum gradient strengths
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void exportHomogeneity(const CCharString &path, CCharString filename = "homogeneity.tif");

    void exportIsotropy(const CCharString &path, CCharString filename = "isotropy.tif");

    void exportDirections(const CCharString &path, CCharString filename = "directions.tif");

// Export the homogeneity, isotropy, or  directions to TIFF file filename in directory 
// path. 
// 
// The grey value is a linear interpolation between the maximum and the minimum 
// values. In the case of homogeneity, the square root is taken.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

   void exportAll(const CCharString &path, CCharString homogFile = "homogeneity.tif", 
                  CCharString isoFile = "isotropy.tif", 
				  CCharString directionFile = "directions.tif");

// Export homogeneity, isotropy, and directions to TIFF files homogFile,
// isoFile, and directionFile, resp., in directory path. 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    void exportHomogeneityIsotropy(const CCharString &path, 
                                   CCharString homogFile = "homogeneity.tif", 
                                   CCharString isoFile = "isotropy.tif");

// Export homogeneity and isotropy to RGB TIFF files homogFile and 
// isoFile, resp., in directory path. 
//
// The function exportHeightGrid(...) (cf. dsm_utils.hpp) is used for
// that purpose, with 100 / medianHomogeneity as the factor.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//=============================================================================

  protected:

//=============================================================================
//===================== Protected initialisation methods ======================


    void initGradients();

// just initialise image data buffers for partial derivatives
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


    void init(int offsetX, int offsetY, int width, int height, 
		      const C2DPoint &luc, const C2DPoint &Delta);

// just initialise all image data buffers and the transformation according to 
// offsetX, offsetY, width, height, luc, Delta
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//====== Convolve gradients by  kernelSize * kernelSize binomial filter  ======

	void convolveGradients(int kernelSize);


//=============================================================================
//=========== Auxiliary method for computing the median of Wx + Wy  ===========

    float computeMedian(const CImageBuffer &Wx, const CImageBuffer &Wy,  
		                CImageBuffer *demBuf);

// return value: Median of Wx + Wy
// If demBuf != 0, only pixels with a valid DEM height are considered
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================

 };

//****************************************************************************

#endif
