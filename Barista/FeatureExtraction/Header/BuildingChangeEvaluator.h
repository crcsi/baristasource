//============================================================================
//
// File        : BuildingChangeEvaluator.hpp
//
// Author      : FR
//
// Description : Interface of class CBuildingChangeEvaluator, 
//               a class for comparing two sets of polygons
//
//============================================================================

#ifndef ____CBuildingChangeEvaluator____
#define ____CBuildingChangeEvaluator____

#include "ProgressHandler.h"
#include "LabelOverlapVector.h"
#include "LabelImage.h"
#include "TFW.h"

class CImageBuffer;
class C2DPoint;
class C3DPoint;
class CHugeImage;
class CDEM;
class CHistogramFlt;

class CBuildingChangeEvaluator : public CProgressHandler
{   
  protected:

	  CImageBuffer *pCounter;

	  CLabelOverlapVector OverlapsDatasetNew;
	
	  CLabelOverlapVector OverlapsDatasetOld;

	  CLabelImage *labelsNew;

	  CLabelImage *labelsOld;

	  CLabelImage *labelsDeleted;

	  CLabelImage *labelsNewBuilt;

	  CLabelImage *labelsConfirmed;

	  CLabelImage *newState;

	  CImageBuffer pixelEvaluation;
	
	  float areaThreshold;

	  CTFW trafo;

	  CCharString projectDir;

	  vector<unsigned short> deletedBuildings;
	  vector<unsigned short> newBuildings;
	  vector<unsigned short> newBuildingsSplit;
	  vector<unsigned short> confirmedBuildings;
	  vector<unsigned short> changedBuildings;
	  vector<unsigned short> oneToOneOld;
	  vector<unsigned short> oneToOneNew;
	  vector<unsigned short> oneToMOld;
	  vector<unsigned short> MToOneNew;
	  vector<vector<unsigned short> > oneToMNew;
	  vector<vector<unsigned short> > MToOneOld;
	  vector<vector<unsigned short> > MToNOld;
	  vector<vector<unsigned short> > MToNNew;

	  static const unsigned char Background;
	  static const unsigned char Confirmed;
	  static const unsigned char Changed;
	  static const unsigned char OldOnly;
	  static const unsigned char NewOnly;
	  static const unsigned char NewSplit;
	  static const unsigned char NewChanged;
	  static const unsigned char OldChanged;


  public:

//============================================================================

	  CBuildingChangeEvaluator (const CTFW &Trafo, float AreaThreshold,
								const CCharString &Dir);

//============================================================================
//================================ Destructor ================================

	  virtual ~CBuildingChangeEvaluator ();

	  bool init(CLabelImage *LabelsNew, CLabelImage *LabelsOld, 
		        const char *protFileName);

//============================================================================
//================================ Data access ===============================

	  int getWidth() const { return labelsNew->getWidth(); };

	  int getHeight() const { return labelsNew->getHeight(); };

	  C2DPoint getShift() const;

	  C2DPoint getPixSize() const;

	  float getAreaThreshold() const { return areaThreshold; };

	  double getPixArea() const;

	  const CTFW &getTrafo() const  { return trafo; };


	  int getConfirmedIndex(int label) const;

	  int getChangedIndex(int label) const;

	  int getNewIndex(int label) const;

	  int getDeletedIndex(int label) const;

//============================================================================

	  void clear();


//============================================================================

  
	  void detectChanges(float noneVsWeak, float weakVsPartial, float partialVsStrong, 
						 float toleranceSplit, float tolerance,
				         float cellSize, float maxSize);

	  void evaluateResults(CDEM *DSM, CDEM *DTM, float noneVsWeak, 
		                   float weakVsPartial,  float partialVsStrong, 
						   float toleranceSplit, float tolerance,
				           float cellSizeArea,   float maxSizeArea,
						   float cellSizeHeight, float maxSizeHeight);

	  void writeData(CCharString filename) const;

	  void writeData(const CLabelOverlapVector &OldVector, const CLabelOverlapVector &NewVector, 
                     CCharString heading = CCharString(""), CCharString filename = CCharString("")) const;


	  void writeTIFFNew(CLabelOverlapVector &record, CCharString filename) const;


	  void writeTIFFOld(CLabelOverlapVector &record, CCharString filename) const;


	  bool dumpPixResults(const char *FileName, CHugeImage *hug) const;

	  bool dumpNewState(const char *FileName, CHugeImage *hug) const;

	  static bool computeConfusionMatrix(CHugeImage *confusion, 
		                                 CHugeImage *changes, 
								         CHugeImage *changesReference);

//============================================================================

	  static CLookupTable evalLUT();

	  static CLookupTable splitMergeOverlapLUT();


//============================================================================

 protected:

//============================================================================


	 void initPixResults();

	 void compareCurrent(CCharString fileBasis, float noneVsWeak, 
                         float weakVsPartial, float partialVsStrong, 
						 float cellSize, float maxSize);
 
	 void NewDistribution (unsigned long NewIndex, CMultipleLabelOverlapRecord &NewVector);

	 void OldDistribution(unsigned long OldIndex, CMultipleLabelOverlapRecord &OldVector);

	 void detectOneToNull();
	 void detectNullToOne();
	 void detectOneToOne();
	 void detectOneToM();
	 void detectMToOne();
	 void detectMToN();

	 void clarifyMToN(float tolerance);
	 void clarifyMtoOne(float tolerance);
	 void simplifyOnetoM(float tolerance);

	 void simplifyContained();
	 void detectConfirmed(float tolerance);
	 void detectConfirmed(const vector<unsigned short> &oldLabels, 
		                  const vector<unsigned short> &newLabels, int size);

	 void detectConfirmedMultiple(const vector<unsigned short> &oldLabels, 
								  const vector<unsigned short> &newLabels, 
								  const vector<unsigned short> &newLabelsNew, int size);

	 void extractLabels(const vector<unsigned short> &oldlabels,
		                const vector<unsigned short> &newlabels, 
		                CLabelImage &oldL, CLabelImage &newL);



	 void improveSegmentation(float noneVsWeak, float weakVsPartial, float partialVsStrong, 
		                      float toleranceSplit, float cellSize, float maxSize, bool mergeNew);

	 static void putBackLabels(CLabelImage *largeLabels, CLabelImage &smallLabels, bool changeAll, bool initPix);

	 void changedNewLabels(CLabelImage &oldL, CLabelImage &newL, CLabelImage &changed, int size);

	 void checkTopology(CLabelImage &newL, unsigned int minPix, int size);

	 void reportResults(bool oneToNull, bool nullToOne, bool oneToOne,
		                bool confirmed, bool changed, 
						bool NtoOne, bool oneToN, bool NtoM, bool split);

	 void compare(float noneVsWeak, float weakVsPartial, float partialVsStrong, 
		          float toleranceSplit, float tolerance,
				  float cellSize, float maxSize, bool mergeNew);

	 static CCharString getQualityString(const CCharString &heading, 
										 int headingPrecision, 
										 const CCharString &separator,
										 const CHistogramFlt &histo,
										 vector<float> completeness,
										 vector<float> correctness,
										 vector<float> quality);

	 static void computeQualityPars(unsigned long oldTP, unsigned long newTP, 
		                            unsigned long FP, unsigned long FN, 
									float &completeness,  float &correctness, float &quality);

};

#endif
