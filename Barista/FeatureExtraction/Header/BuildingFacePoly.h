#ifndef ___CBUILDINGFACEPOLY___
#define ___CBUILDINGFACEPOLY___

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <vector>
#include "3DPoint.h"
#include "TFW.h"
#include "BuildingFaceNeighbourRecord.h"

//****************************************************************************
//*************************** Forward Declarations ***************************

class CBuildingFace;
class CXYZPolyLine;

//****************************************************************************
/*!
 * \brief
 * A (outer or inner) boundary polygon of a roof face in building reconstruction
   The boundary consists of an ordered sequence 
   of boundary polygon segments (class CBuildingFaceNeighbourRecord), each
   separating exactly two planes and each corresponding to either a step edge
   or an intersection line. The final boundary polygon is determined by a 
   combination of all these individual segments.
              
   Class CBuildingFacePoly contains methods for initialising, improving,
   managing changing, and eliminating these individual segments,
   and for their combinination.
   
   Literature:  Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik:
                Automated Delineation of Roof Planes from LIDAR Data. 
                In: International Archives of the Photogrammetry, Remote 
                Sensing and Spatial Information Sciences Vol. XXXVI - 3/W19, 
                pp. 221-226, Enschede, The Netherlands, September 12-14, 2005.  

 * 
 * \see
 * CBuildingFaceNeighbourRecord | CLabelImage 
 */
class CBuildingFacePoly
{
  protected:

//============================================================================
//=================================== Data ===================================

	  /// The planar roof face this polygon is a boundary of
	  CBuildingFace *pFace;  

	  /// The combined closed polygon
	  CXYZPolyLine *pCombinedPolygon;

	  /// The vector describing the ordered sequence of polygon segments, each 
      //* separating pFace from one other neighbouring plane
	  vector<CBuildingFaceNeighbourRecord *> neighbourVector;
                                   
	   /// Reduction applied to all co-ordinates
	  C3DPoint reduction;

	  /// transformation between pixel and object coordinates
	  CTFW trafo;

//============================================================================

  public:
   

//============================================================================

	  /// Construct an empty polygon in the roof plane face.
	  //* Trafo: Transformation parameters between world and pixel co-ordinate systems.
	  //* Red: Co-ordinate reduction
	  CBuildingFacePoly(CBuildingFace *face, const CTFW &Trafo, const C3DPoint &Red);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// Construct an empty polygon in the roof plane face and initialise the polygon
	  //* by line
	  //* Trafo: Transformation parameters between world and pixel co-ordinate systems.
	  //* Red: Co-ordinate reduction
	  CBuildingFacePoly(CBuildingFace *face, const CXYZPolyLine &line, 
                        const CTFW &Trafo, const C3DPoint &Red);


//============================================================================

	  /// Destructor 
	  virtual ~CBuildingFacePoly();


//============================================================================

	  ///Reset neighbouring segments 
	  void resetNeighbours();

	  ///Reset combined polygon
	  void resetPolygon();


//============================================================================

	  ///Assignment operator: copy all data from poly
	  CBuildingFacePoly &operator= (const CBuildingFacePoly &poly);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	  /// initialise the combined polygon by pLine
	  void copyPoly(const CXYZPolyLine* pLine);

	  /// initialise the combined polygon by Line
	  void copyPoly(const CXYZPolyLine &line)
	  { this->copyPoly(&line); };


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//============================================================================
//================================ Query data ================================

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - - - - - -  Direct queries - - - - - - - - - - - - - - -
   
	  CBuildingFace *getFace() const { return this->pFace; };

   
	  CBuildingFaceNeighbourRecord* getNeighbour(unsigned int index) const
	  { return this->neighbourVector[index]; };
   

	  eNEIGHBORTYPE getNeighbourType(unsigned int index) const 
	  { return this->neighbourVector[index]->getNeighbourType(); };

	  unsigned int getNNeighbours() const { return this->neighbourVector.size(); };

	  CXYZPolyLine *getCombinedPoly() const { return this->pCombinedPolygon; };

	  int getIndexOfNeighbour(CBuildingFaceNeighbourRecord *neighbour) const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - - -  Queries for face indices - - - - - - - - - - - - -
   
	   /// Face index this boundary polygon is situated in
	  unsigned short getFaceIndex() const; 

            
	  /// Face index of the neighbouring face corresponding to the 
	  //* neighbourhood record at index 'index' neighbourVector
   	  unsigned short getNeighbourFaceIndex(unsigned int index) const;
   
   
	  /// Face index of the neighbouring face corresponding to the previous 
	  //* neighbourhood record in the order of this polygon
	  unsigned short getPrevNeighbourFaceIndex(unsigned int index) const;
         
   
	  /// Face index of the neighbouring face corresponding to the next 
	  //* neighbourhood record in the order of this polygon
	  unsigned short getNextNeighbourFaceIndex(unsigned int index) const;
                          

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   
	  /// Get a vector with all neighbourhood records separating the roof plane
	  // this boundary polygon is situated in from the roof plane with the face
	  // index region.
	  vector<CBuildingFaceNeighbourRecord *> *getNeighbours(unsigned short region) const;


//============================================================================

	  /// Remove neighbourhood record at index index in neighbourVector 
	  void removeNeighbourAt(unsigned int index);


	  /// append neighbour to the vector of neighbours
	  void appendNeighbour (CBuildingFaceNeighbourRecord *neighbour);

	  /// insert neighbour to the vector of neighbours at index  
	  void insertNeighbourAt(unsigned int index, CBuildingFaceNeighbourRecord *neighbour);

	  /// insert neighbour to the vector of neighbours at index + 1
	  void insertNeighbourAfter(unsigned int index, CBuildingFaceNeighbourRecord *neighbour);

	  /// replace neighbour by the elements of newVec
	  void replaceNeighbour(CBuildingFaceNeighbourRecord *neighbour, vector<CBuildingFaceNeighbourRecord *> &newNeighbours);

//============================================================================

	  /// Combine individual boundary polygon segments 
	  //* Just concatenate the individual "final polygons" of the neighbourhood records.
	  void combineNeighbours();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

   
	  void combineNeighbours(float intersectionThreshold, 
                             float distanceThreshold, 
                             float alphaThreshold,
                             float selfIntersectionThreshold);

// Combine the individual "final polygons" of the neighbourhood records by
// computing intersections at transitions. 
// 
// First, each transition is checked whether there is an intersection. 
// An intersection is assumed to exist if the corresponding end points of
// two consecutive neighbourhood records are close to the intersection 
// point, i.e., if their distances from the intersection point is smaller
// than If_intersectionThreshold.
// If the corresponding segments are almost parallel or identical, they are 
// just concatenated, and the intersection point is not used. 
// The segments are considered to be almost parallel if the cosine of the
// intersection angle is larger than If_alphaThreshold.
// The resulting polygon is thinned out and self intersections are removed.
// Finally, very short polygon segments are eliminated.
//
// If_intersectionThreshold:     Threshold for determining whether two
//                               polygon segments intersect or not.
// If_distanceThreshold:         Distance threshold for determining whether
//                               two segments are identical.
// If_alphaThreshold:            minimum cosine of the intersection angle
//                               so that two segments are considered to be
//                               parallel.
// If_selfIntersectionThreshold: Threshold for combination of polygon segments:
//                               if the combined polygon intersects itself,
//                               loops having an area smaller than the square
//                               of If_selfIntersectionThreshold will be 
//                               removed.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//============================================================================
//================  Formatted output of neighbourhood records ================

  
	  void printNeighbours(const char *filename) const;


	  void printNeighbours (ostream &anOstream) const;


	  void printNeighbours(const CCharString &heading);

   
	  bool intersection(CBuildingFaceNeighbourRecord *pNeighbour1,
		                CBuildingFaceNeighbourRecord *pNeighbour2,
                        float distThreshold);

// Intersect the outmost segments of the "final polygons" of IpC_neighbour1
// and IpC_neighbour2. If the distance between the intersection point and
// the respective end points of IpC_neighbour1 and IpC_neighbour2 is smaller
// than If_distThreshold, the intersection point is accepted, and it is
// added to the final polygons; in this case, the return value will be 1.
// If the intersection point is not accepted, 0 is returned.


	  void removeSelfIntersections(float lengthThreshold, float areaThreshold);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   

//============================================================================
//==============  Export polygon as break line to a WINPUT file ==============

 
	  void exportDXF(ofstream &anOstream, bool exportCombined, bool exportInitial, 
		             bool exportApprox, bool exportFinal) const;

// if Ii_appendFlag != 0, the polygon will be appended to the file 
//                        IrC_fileName
// Otherwise, the file will be overwritten.


//****************************************************************************
};

#endif
