#ifndef ___CALSFilter___
#define ___CALSFilter___

#include "CharString.h"
#include "Filename.h"
#include "2DPoint.h"
#include "3DPoint.h"
#include "ImgBuf.h"
#include "TFW.h"
#include "ProgressHandler.h"
#include <ANN/ANN.h>

//****************************************************************************
//*************************** Forward Declarations ***************************

class CDEM;
class CALSDataSet;
class CHugeImage;
class CLabelImage;
class CTFW;
class CRobustWeightFunction;
class CGenericPointCloud;
class CHistogramFlt;

//****************************************************************************
//****************************************************************************
/*!
 * \brief
 * A class for filtering ALS point clouds based on morphologic and iterative thresholding of height differences.
 * 
  	 The class takes an existing ALS point cloud and a coarse digital 
	 surface model (DSM) interpolated from these points. 
	 Optional, a Digital Terrain Model (DTM) can be used.
	
 * 
 * \see
 * CALSDataSet | CDEM |CDSMRoughness  
 */

class CALSFilter : public CProgressHandler
{

//=============================================================================
 
  protected:

//=============================================================================
//=================================== Data ====================================

	  C3DPoint pMin, pMax; 
	
	  C2DPoint resolution; 
   
	  /// The ALS point cloud on disk; can be used to retrieve the point cloud into memory	
	  CALSDataSet *pALS;	

	  /// The DEM file representing the Digital Terrain Model (DTM) 
	  CDEM *pFilteredDTMFile;

	  /// a buffer containing the heights of the DSM in the region of interest, as float numbers 	
	  CImageBuffer *pDSM;     

	  /// a binary buffer describing the areas where no ALS points are available
	  CImageBuffer *pNoDtmMask;     

	  /// a buffer containing a filtered version of the DSM
	  CImageBuffer *pFilteredDSM;      

	  /// a buffer containing the approximate heights of the DTM in the region of interest, as float numbers 	 
	  CImageBuffer *pApproxDTM;      
	 
	  /// a buffer containing the heights of the DTM in the region of interest, as float numbers 	 
	  CImageBuffer *pDTM;      

	  float morphFilterSize;
	  // The size of the morphological filter for DTM generation in [m].
	  // If the DTM is read from a SCOP DEM file, it will not be used.

	  float neighbourHeightThreshold;             

	  // Threshold for the height differences between DSM and DTM. 
	  // Used to create the initial building mask.
	  float minBuildHeight;             

	  double sigmaALS;             
	  double alpha;             
	  double maxObliqueness; 
	  int numberIterations;

	  float heightThresholdFilter;

	  float morphFilterSizeForDetails;
	  unsigned int pointsForInterpolation;

	  // The size of the morphological filter for DTM generation in [m].
	  // If the DTM is read from a SCOP DEM file, it will not be used.


	  CCharString projectPath;

	  CTFW trafo;

	  vector<CFileName> tileScratchFiles;
	  vector<C2DPoint> tilePmin;
	  vector<C2DPoint> tilePmax;

//=============================================================================

   public:

//=============================================================================
//=============================== Constructors ================================ 

	   CALSFilter(CALSDataSet *ALS, CDEM *FilteredDTMFile, 
		          float MinBuildHeight,
				  float HeightThreshold, double SigmaALS,
				  float FilterSize, float FilterSizeDetails, 
				  float HeightThresholdFilter, double Alpha,
				  double MaxObliqueness, int NumberIterations,
				  int NpointsForInterpolation,
				  const C2DPoint &Min, const C2DPoint &Max, 						
				  const C2DPoint &Resolution, const CCharString &path);



//=============================================================================
//================================ Destructor =================================

	   virtual ~CALSFilter ();

//=============================================================================


	   bool isInitialised() const { return (this->pDTM != 0); };


//=============================================================================
//============ Generate DTM by hierarchical morphologic filtering  ============

	
	   bool filter();

//=============================================================================
//============================= protected methods =============================

  protected:

//=============================================================================

	  /// Initialise all image data buffers (allocate memory)
	  bool initWindows();

	  /// Delete all image data buffers
	  void clearWindows();

//=============================================================================
		  
	  /// Initialise DSM and tile structure by interpolation from the point cloud
	  void initDSMAndTileStructure();


//=============================================================================

	  /// Calculate statistic threshold for classification of roughness
	  void calcThresh(double &threshSigma);

	  /// Initialise files for classification of roughness and terrain classification
	  void initFiles(ofstream &roughFile, ofstream &smoothFile, ofstream &isolatedFile, 
					 ofstream &smoothFileRelabel, ofstream &mergeFile, int tileIndex);

	  ///Sort the points depending on the roughness
	  void sortLabelPoint(CGenericPointCloud &points, ofstream &roughFile, 
						  ofstream &smoothFile, ofstream &isolatedFile, double threshSigma,
						  double maxDist, vector<unsigned int> &nClassPts,
						  vector<unsigned int> &nFirstIndex, long &nSmooth, 
						  long &nRough, long &nBorder);

	  ///Merging of segments with the same label and controll if they belong together
	  void checkAndMerg(CGenericPointCloud &points, double maxDist,
						vector<unsigned int> &nClassPts,
						vector<unsigned int> &nFirstIndex, ofstream &mergeFile);

	  ///Relabeling of the merged segments
	  void relabel(CGenericPointCloud &points, vector<unsigned int> &nClassPts, int &nClasses,
				   ofstream &smoothFileRelabel);

	  ///Compute Histogram of point numbers per segment
	  void compHisto(CGenericPointCloud &points, vector<unsigned int> &nClassPts, int nClasses, 
					 long nSmooth, long nRough, long nBorder);

	  ///Comput label image for the segments
	  void compLabelImg(CGenericPointCloud &points, int tileindex, const char *suffix);

	  ///Compute average heights above coarse DTM for each segment
	  void compAvrgHigh(CGenericPointCloud &points, vector<unsigned int> &nClassPts,
						vector<double> &averageHeights);

	  ///Initialise pointcloud for groundpoints and classify the points
	  int initGrdCloud(CGenericPointCloud &points, CGenericPointCloud &pointsGrd, 
					   const vector<double> &averageHeights, const vector<unsigned int> &nClassPts);

	  ///Compute approximate DTM from groundpoints
	  void filterApproxDTM(CGenericPointCloud &pointsGrd, int tileIndex);

	  ///Iteration of DTM calculation
	  void iterCalcDTM(CGenericPointCloud &points, vector<unsigned int> &nClassPts, 
		               int nClasses, int tileIndex);

	  ///Export of the single Pointclouds
	  void expTilePointCloud(CGenericPointCloud &points, int tileIndex);

	  ///Compute final DTM of groundpoints
	  void filterFinalDTM(CGenericPointCloud &pointsGrd, int tileIndex, ofstream &DTMKoord);

	  ///Final generation of DTM and classification for terrainpoints
	  void finalDTM(CGenericPointCloud &points, int tileIndex, ofstream &DTMKoord);

	  ///Final classification of points
	  void finalClassPoints(int &off, int &terrain);

	  bool classifyRoughness(double maxDist);

	  bool initRoughness(CGenericPointCloud &points, CHistogramFlt &histoRoughness,
		                 double maxDist, int roughnessCoord);

	  CGenericPointCloud *readPointCloud(const C3DPoint &Min, const C3DPoint &Max, 
		                                 int coords, int annCoords);

//=============================================================================


//=============================================================================

	  void filter(const C3DPoint &Min, const C3DPoint &Max, int &nGrd, int &nOff,
				  float &zmin, float &zmax, int tileIndex, bool filterRobust);


	  void filterDSM(const C3DPoint &Min, const C3DPoint &Max, 
		             float &zmin, float &zmax, int tileIndex);

//=============================================================================

	  void filter(double size, int itCount, float &zDTMmin, float &zDTMmax, 
		          unsigned int &nGrd, unsigned int &nOff, bool FillHoles, bool filterRobust);

//=============================================================================

	  void initTileStructure(double size, C2DPoint &lucT, C2DPoint &deltaT, 
		                     int &nx, int &ny, int &nTotal);

//=============================================================================

	  void fillHoles(bool all);

//=============================================================================

	  void finalClassification(unsigned int &nGrd, unsigned int &nOff);

//=============================================================================

	  bool initPointCloud(CGenericPointCloud &ptCld, int tileIndex, 
		                  C3DPoint Min, C3DPoint Max);

	  void deletePointClouds();

//=============================================================================

	  static void exportFloatBuf(const char *fn, const CImageBuffer &fltBuf, bool uselut);


	  int getFilterSize(float size);

//=============================================================================

};

//****************************************************************************

#endif
