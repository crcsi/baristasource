#ifndef ___CBUILDINGFACENEIGHBOURVECTOR___
#define ___CBUILDINGFACENEIGHBOURVECTOR___

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */
#include <vector>

#include "BuildingFaceNeighbourRecord.h"

class CBuildingFacePoly;

//****************************************************************************
/*!
 * \brief
 * A vector of instances of class CBuildingFaceNeighbourRecord, with 
 * methods for keeping it consistent and for making special queries. 
 * 
 * \see
 * CBuildingFaceNeighbourRecord  | CBuildingFacePoly | CLabelImage 
 */


class CBuildingFaceNeighbourVector
{
  protected:

//============================================================================
//=================================== Data ===================================

	  /// The vector of instances of class CBuildingFaceNeighbourRecord
	  vector <CBuildingFaceNeighbourRecord *> neighbourhoodVector;
      
	  /// The number of valid entries to neighbourhoodVector
	  unsigned int nNeighbourhoods;  

	  double maxDistForIdentity;

//============================================================================

  public:
   
	  /// Constructor: Initialise neighbourhoodVector for 100 entries, nNeighbourhoods by 0
	  CBuildingFaceNeighbourVector(double maxDist);

	  /// Destructor 
	  virtual ~CBuildingFaceNeighbourVector();

//============================================================================

	  /// Reset vector 
	  void reset();

//============================================================================

	  /// Assignment operator 
	  CBuildingFaceNeighbourVector &operator= (const CBuildingFaceNeighbourVector& vec);


//============================================================================
//================================ Query data ================================

   
	  unsigned int getNNeighbourhoods() const { return this->nNeighbourhoods; };

	  CBuildingFaceNeighbourRecord *getNeighbour(unsigned int index) const
	  { return this->neighbourhoodVector[index]; };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	  
	  bool getUpperAndLowerBound(unsigned short Region1, unsigned short Region2, 
		                         unsigned int &lowerBound, unsigned int &upperBound) const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// Check whether rec is contained in the vector. The comparison is based on
	  //* region indices and the geometry of the initial polygon.
	  // return value: pointer to an existing instance of CBuildingFaceNeighbourRecord 
	  //*              if rec is contained in neighbourhoodVector
	  //               0 otherwise
	  CBuildingFaceNeighbourRecord *isContained(CBuildingFaceNeighbourRecord *rec) const;


//============================================================================
//=====================  Add / remove / merge neighbours =====================

	  /// Add a CBuildingFaceNeighbourRecord for Region1 and Region2 to the vector
	  //* if no such record is already contained in it
	  CBuildingFaceNeighbourRecord *addNeighbours(unsigned short Region1, unsigned short Region2,
		                                          CBuildingFacePoly *facePoly,
												  float rmsPlanes, 
		                                          CXYContour *pContour, float sigmaContour, 
												  CTFW &trafo, const C3DPoint &reduction, 
												  float maxDist);
	  /// Add a CBuildingFaceNeighbourRecord to the vector
	  void addNeighbour(CBuildingFaceNeighbourRecord *rec);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// Remove all instances of class CBuildingFaceNeighbourRecord describing a
	  //* neighbourhood between the planar regions Region1 and Region2 from the vector
	  void removeNeighbours(unsigned short Region1, unsigned short Region2);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// Remove all instances of class CBuildingFaceNeighbourRecord describing a
	  //* neighbourhood between the planar region Region1 and any other region from the vector

	  void removeAllNeighbours(unsigned short Region1);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// Merge the two planes Region1 and Region2. In all entries to the 
	  //*  vector, Region1 is replaced by Ii_region2. After that, all records
	  //* (Region1, Region1) are removed.
	  void mergePlanes(unsigned short Region1, unsigned short Region2);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


//============================================================================

	  /// Sort vector by this->region1 (primary) and this->region2 (secondary); 
	  //* this is the default sorting method
	  void sort();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// sort by rmsError (for determining the "most co-planar" planes)
	  void sortByRMS();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

//============================================================================

	  /// Formatted output to ostream 
	  void print (ostream &anOstream) const;

	  /// DXF output to file
	  void exportDXF(ofstream &dxfFile, bool exportInitial, bool exportApprox, 
					 bool exportFinal) const;

	  void exportDXF(const char *dxfFile, bool exportInitial, bool exportApprox, 
					 bool exportFinal) const;

//============================================================================

  protected: 

//============================================================================
//======================  Auxiliary methods for sorting ======================

	  void swapRegions(unsigned int index1, unsigned int index2);
	  void sortPartByRMS(int indexLow, int IndexHigh);


//============================================================================
   
};
 
#endif
