#pragma once

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */


#include "ProgressHandler.h"

class CVImage;
class CFrameCamera;
class CCameraMounting;
class CFrameCameraModel;
class CRotationMatrix;
class CImageBuffer;
class C3DPoint;
class C2DPoint;
class CHugeImage;

class CGenerateEpipolar : public CProgressHandler
{
  protected:
	  // Data
	  CHugeImage *leftImageFile;
	  CHugeImage *rightImageFile;

	  CFrameCameraModel *leftModel;
	  CFrameCameraModel *rightModel;
	  
  public:

	  CGenerateEpipolar(CHugeImage *leftI, CHugeImage *rightI, CFrameCameraModel *leftM, CFrameCameraModel *rightM);
	  
	  virtual ~CGenerateEpipolar();

	  bool initEpipolarOrientations(CFrameCameraModel &oriEpiLeft, CFrameCameraModel &oriEpiRight) const;

	  bool generateEpipolar(CImageBuffer &oriLeft,  CImageBuffer &oriRight, 
		                    CImageBuffer &epiLeft,  CFrameCameraModel &oriEpiLeft, 
							CImageBuffer &epiRight, CFrameCameraModel &oriEpiRight, 
							bool initOris, bool convertTo8Bit = false); 

	  bool generateEpipolar(const C3DPoint &pmin, const C3DPoint &pmax, 
		                    CImageBuffer &epiLeft,  CFrameCameraModel &oriEpiLeft, 
							CImageBuffer &epiRight, CFrameCameraModel &oriEpiRight, 
							bool initOris, bool convertTo8Bit = false); 


  protected:

	  virtual bool initNewOrientations(CFrameCameraModel &oriEpiLeft, CFrameCameraModel &oriEpiRight) const;
	  
	  bool initEpiTrafPar(CFrameCameraModel &oriOrig, CFrameCameraModel &oriEpi, 
		                  CRotationMatrix &epiToOri, C3DPoint &IRPepi, C3DPoint &IRPori) const;


	  bool prepareOriBuf(CHugeImage *hug, 
		                 const CImageBuffer &epiBuf, CImageBuffer &oriBuf, 
		                 const CRotationMatrix &epiToOri, const C3DPoint &IRPepi, 
						 const C3DPoint &IRPori) const;

	  void transformEpiToOrig(const C2DPoint &pEpi, C2DPoint &pOri, 
		                      const CRotationMatrix &epiToOri, const C3DPoint &IRPepi, 
							  const C3DPoint &IRPori) const;


	  bool transformEpi(CHugeImage *hug, const CRotationMatrix &epiToOri, const C3DPoint &IRPepi, 
						const C3DPoint &IRPori, CImageBuffer &epi,
						bool convertTo8Bit); 
};
