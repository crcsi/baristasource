//===================================================== (101215) === 101215 ===
//
// File        : MultiImageSegmenter.h
//
// Author      : FR; 
//              (C) Institute of Photogrammetry and GeoInformation 
//                  University of Hannover
//
// Description : A class for simultaneous segmentation of multiple images
//
//=============================================================================

#ifndef ____CMultiImageSegmenter____
#define ____CMultiImageSegmenter____


#include "TFW.h"
#include "m_FeatExtract.h"
#include "ProgressHandler.h"
#include "XYZPolyLineArray.h"

//*****************************************************************************
//*************************** Forward Declarations  ***************************

class CImageBuffer;
class CLabelImage;
class CObservationHandler;
class CHugeImage;
class C3DPoint;
class CRAG;
class CLineMatchingHypothesis;

/*!
 * \brief
 * A class for the simultaneous segmentation of multiple images
 * 
  
   This class is work in progress
	
 * 
 * 
 * 
 */


class MultiImageSegmenter: public CProgressHandler
{

  protected:

//=============================================================================
//=================================== Data ====================================

	  CTFW DSMtrafo;

	  CTFW imageTrafo;

	  vector<CImageBuffer *> imageVector;
	  vector<CImageBuffer *> occlusionMasks;

	  vector<CSensorModel *> sensorModelVector;

	  vector<CLabelImage *>  imageLabelVector;
	  vector<CLabelImage *>  objectLabelVector;
	  vector<CRAG *>         imageRAGVector;

	  CLabelImage *pCombinedLabels;

	  CImageBuffer *pDSM; 

	  CXYZPolyLineArray BoundaryPolygons;

	  CCharString outputPath;

	  C3DPoint pmin, pmax;

	  FeatureExtractionParameters extractionPars;

	  double bufferSize;


//=============================================================================

  public:

//=============================================================================
//=============================== Constructors ================================ 
	 
	  MultiImageSegmenter();

//=============================================================================
//================================ Destructor =================================

	  virtual ~MultiImageSegmenter();


//=============================================================================
//================================ Query data =================================

	  bool init(CImageBuffer *DSM, const CTFW &TrafoDSM, 
		        vector<CObservationHandler *> *imgVec, 						
				vector<CHugeImage *> *hugImgVec, 
				const CXYZPolyLineArray &BoundaryPolygons, 
				const C3DPoint &PMin, const C3DPoint &PMax,
				double pixImg, double SearchBuf,
				const FeatureExtractionParameters &Pars,
				const CCharString &path);

//=============================================================================

	  virtual bool isInitialised() const { return ((this->pDSM != 0) || (imageVector.size() > 0)); }

//=============================================================================

	  bool segment();

//=============================================================================

	  void exportImageBuffersToTiff(const CCharString &suffix = "") const;
	  void exportDSMToTiff(const CCharString &suffix = "") const;
	  void exportOcclusionBuffersToTiff(const CCharString &suffix = "") const;
	  void exportLabelBuffersToTiff(const CCharString &suffix = "") const;
	  void exportObjectLabelBufferToTiff(const CCharString &suffix = "") const;
	  void exportProjectedLabelBuffersToTiff(const CCharString &suffix = "") const;

//=============================================================================

  protected:

	  long getObjectLabelIndex(const C3DPoint& P) const; 

	  long getLabelIndex(const CLabelImage &labels, const C3DPoint& P) const; 

	  unsigned short getObjectLabel(const C3DPoint &P) const;

	  unsigned short getProjectedLabel(unsigned int projectedIndex, const C3DPoint &P) const;

	  unsigned short getLabel(const CLabelImage &labels, const C3DPoint &P) const;

	  void exportLabelBufferToTiff(int index, const CCharString &suffix) const;

	  float getDSMHeight(const C2DPoint &p) const;
	  float getDSMHeight(const C3DPoint &p) const;

	  bool getImageRegion(CObservationHandler *oh, CHugeImage *hugImg, 
		                  int &offsetX, int &offsetY, 
						  int &width, int &height);

	  virtual bool initImage(CObservationHandler *oh, CHugeImage *hugImg, double pixSize);

	  bool initImages(vector<CObservationHandler *> *imgVec, 
					  vector<CHugeImage *> *hugImgVec, double pixelSize);

	  bool initOcclusionMasks(CImageBuffer *PDSM);

	  virtual bool segmentImages();

	  virtual bool projectSegmentedImages();

	  void removePixelsWithoutOverlap();

	  bool intersectLabels(int l);

	  bool intersectLabels();

	  CImageBuffer *getValidMask(unsigned int imgIndex, double zmax) const;

	  CImageBuffer *getValidObjectMask() const;
	  
	  virtual void resetImageVectors();
	  
	  void morphologicalFilterObjectLabel(unsigned short label, int size) const;

};

//**************************************************************************** 
//****************************************************************************

#endif
