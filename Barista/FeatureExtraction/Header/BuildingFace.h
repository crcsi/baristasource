#ifndef ___CBUILDINGFACE___
#define ___CBUILDINGFACE___

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <vector>

#include "3DPoint.h"
#include "TFW.h"
#include "3DadjPlane.h"

//****************************************************************************
//*************************** Forward Declarations ***************************

class CBuildingFacePoly;
class CBuildingFaceNeighbourRecord;
class CBuildingFaceNeighbourVector;
class CLabelImage;

//****************************************************************************

class CBuildingFace : public C3DAdjustPlane
{

  protected:

//============================================================================
//=================================== Data ===================================
   
	  /// Index of this face in the building's face label image
	  unsigned short index;          
   
	  /// Threshold for statistical "point-in-plane" test, derived from stochastic 
	  //* properties of the plane and the LIDAR points, the latter described by                       
	  //* rmsXY and rmsZ (see below).
	  float distThreshold;     


	  /// Vector of boundary polygons of the face; polyVector[0] is the outer loop,all others are inner loops.
	  vector<CBuildingFacePoly *> polyVector;

	  /// transformation between pixel and object coordinates
	  CTFW trafo;

	  /// r.m.s. error of an X or Y coordinate of a LIDAR point.
	  static float rmsXY;  


	  /// r.m.s. error of a Z coordinate of a LIDAR point
	  static float rmsZ;   


//============================================================================

  public:
   
//============================================================================

	  /// Default constructor: initialise all data by 0
	  CBuildingFace();

	  /// Default constructor: initialise all data by 0, only face index by Index
	  CBuildingFace(unsigned short Index);


	  /// Copy constructor: copy all data
	  CBuildingFace(const CBuildingFace &face);


//============================================================================

	  /// Destructor 
	  virtual ~CBuildingFace();


//============================================================================
//============================ Copy / reset data  ============================

   
	  /// copy all data from face, but set face index to Index
	  void copyData(unsigned short Index, const CBuildingFace &face);

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  /// reset all data to 0
	  void resetData(unsigned short Index, C3DPoint &Red);

	 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  void setTrafo(const CTFW &Trafo);

	  void setIndex(unsigned short Index) { this->index = Index; };

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


//============================================================================
//================================ Query data ================================

   
	  unsigned short getIndex() const { return this->index; };
      
	  float getThreshold() const { return this->distThreshold; };
   
	  float getThreshold(const C3DPoint &p) const;

	  unsigned int getNPolys() const { return this->polyVector.size(); };
   
	  static float getRmsXY()   { return rmsXY; };

	  static float getRmsZ()    { return rmsZ; };

	  /// get the standard deviation of a planimetric co-ordinate of a point in
	  /// the plane, depending on the obliqueness of the plane
	  float getRmsFromRmsZ () const;
   
	  CBuildingFacePoly *getPoly(unsigned int u) const { return this->polyVector[u]; };

	  CBuildingFacePoly *getOuterPoly() const { return this->polyVector[0]; };

	  /// get a vector of pointers to CBuildingFaceNeighbourRecord objects between
	  //* this face and the face with index region
	  vector<CBuildingFaceNeighbourRecord *> *getNeighbours(unsigned short region) const;

	  vector<unsigned short> getNeighbouringFaceIncices() const;


//============================================================================
//================================= Set data =================================


	  /// set the global rms errors of the x and y co-ordinates of LIDAR points (used for deriving thresholds)
	  static void setRmsXY(float RmsXY) { rmsXY = RmsXY; };

	  /// set the global rms errors of the z co-ordinates of LIDAR points (used for deriving thresholds)
	  static void setRmsZ (float RmsZ)  { rmsZ  = RmsZ; };

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  void computeFacePars();

	  void initThreshold();

	  void initThreshold(const C3DPoint &p);

//============================================================================
//============================= Merge two planes =============================


	  void merge(CBuildingFace &plane);

// this plane is merged with IrC_other, i.e. all the sums of IrC_other are 
// added to those of *this, and the new face parameters are computed by 
// adjustment.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//============================================================================
//====================== Compute the boundary polygons  ======================

   
	  void initPolys(CBuildingFaceNeighbourVector &neighbourhoodVector, 
		             const CLabelImage &labelImage, bool useVoronoi);

// initialise the boundary polygon(s) from IrC_labelImage. 
// IrC_labelImage:    face label image
// Ii_useVoronoi   0: the boundary polygons will be initialised as the boundary
//                    of this region in IrC_labelImage
//               <>0: the boundary polygons will be initialised as the boundary
//                    of this region in the voronoi diagram of IrC_labelImage
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

   void checkPolys();

// Make sure that the outer polygon is at index 0 in the vector of boundary 
// polygons; remove polygons that are outside the outer polygon.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


//============================================================================
//============================== Export Methods ==============================

   void print (ostream &anOstream) const;

// print the parameters to OrC_ostream
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

   void print (const char *filename) const;

// print the parameters to file IrC_filename
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

   void exportPolysDXF(ofstream &dxf, bool exportCombined, bool exportInitial, 
		               bool exportApprox, bool exportFinal) const;

// write all boundary polygons to (open) WINPUT file XrC_winput.
// Xru_closed:   WINPUT code of first closed polygon (will be incremented)
// Xru_unClosed: WINPUT code of first polygon that is not closed (will be 
//               incremented)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//============================================================================

  protected: 

 
//============================================================================
//================== reset/insert/remove boundary polygons  ==================

   void resetPolys();

// delete all polygons and set the pointers to 0
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   
   void removePoly(unsigned int Index);

// 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


};

//****************************************************************************

#endif
