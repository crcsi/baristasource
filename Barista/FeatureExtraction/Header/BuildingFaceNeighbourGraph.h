#ifndef __CBuildingFaceNeighbourGraph__
#define __CBuildingFaceNeighbourGraph__


/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <vector>
using namespace std;

#include "BuildingFaceNeighbourVector.h"
#include "TFW.h"

//****************************************************************************
//*************************** Forward Declarations ***************************

class CLabelImage;
class CBuildingFace;
class CBuildingFaceNeighbourRecord;

//****************************************************************************

/*!
 * \brief
 * CBuildingFaceNeighbourGraph is a container class describing a region adjacency graph (RAG) in a digital image.
 * 
  	 It is based on a label image representing the results of a segmentation algorithm.
	 Based on the label image, it contains a vector of image regions with their 
	 radiometric and geometric features, and it detects and models the neighbourhood relations
	 between these regions.
	
 * 
 * \see
 * CSegmentNeighbourhood | CSegmentNeighbourhoodVector | CImageRegion | CLabelImage | CSegmentAMatrix
 */

class CBuildingFaceNeighbourGraph
{
  protected:
	
	  //! The label image that is passed on to CBuildingFaceNeighbourGraph for analysis
	  CLabelImage *pLabelImage;

	  //! A vector of image regions. The entry at index i corresponds to label i in *pLabelImage
	  vector<CBuildingFace *> *pFaceVector;

	  //! A vector containing the neighbourhood relations between the image regions in *pLabelImage
	  CBuildingFaceNeighbourVector *pNeighbourVector;


	  CTFW trafo;

//============================================================================

  public:
   
	  CBuildingFaceNeighbourGraph(const CTFW &Trafo);

	  virtual ~CBuildingFaceNeighbourGraph();
	
//============================================================================

	  bool isInitialised() const { return (this->pFaceVector != 0); };


	  vector<CBuildingFace *> *getFaceVector() const { return this->pFaceVector; };

	  unsigned int getNFaces() const { return this->pFaceVector->size(); }

	  CBuildingFace *getFace(unsigned int index) const { return (*this->pFaceVector)[index]; }

	  int getNNeighbours() const { return this->pNeighbourVector->getNNeighbourhoods(); }

	  CBuildingFaceNeighbourRecord *getNeighbour(unsigned int index) 
	  { return this->pNeighbourVector->getNeighbour(index); }


//============================================================================

	  bool initialiseNeighbours(CLabelImage *plabelimage);

	  void classifyNeighbours(float minLenghtRatio, float minLength, float delta);

	  void combineNeighbourPolygons();

	  void clearNeighbours();

	  void clearFaces();

	  void clearAll();


//============================================================================

	  void exportAll(const char *filenameBase); 

	  void exportPlanes(const char *filename) const; 

	  void exportPolygons(const char *filename, bool exportApprox, 
						  bool exportInitial, bool exportFinal) const; 

	  void exportNeighbourhoods(const char *filename) const; 


//============================================================================

  protected: 


//============================================================================

};

#endif

