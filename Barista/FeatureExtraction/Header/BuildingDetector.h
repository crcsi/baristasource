#ifdef WIN32

#ifndef ___CCBuildingDetector___
#define ___CCBuildingDetector___

#include "CharString.h"
#include "Filename.h"
#include "2DPoint.h"
#include "3DPoint.h"
#include "ImgBuf.h"
#include "TFW.h"
#include "DempsterShafer.h"
#include "ProgressHandler.h"
//****************************************************************************
//*************************** Forward Declarations ***************************

class CDEM;
class CHugeImage;
class vrSolid; 
class CXYPolyLine;
class CXYPolyLineArray;
class CLabelImage;
class CDSMRoughness;
class CDempsterShaferBuildingClassifier;
class CDSMBuilding;
class CBuildingArray;
class CTFW;
class CImageFilter;

//****************************************************************************
//****************************************************************************
/*!
 * \brief
 * A class for detecting buildings from Airborne Laserscanner (ALS) data and/or multispectral images (in the form of the NDVI)
 * 

	The results of building detection will be a "building mask" and a "building label image", 
	both of which can be exported as TIFF files, and a vector of instances of class 
	CDSMBuilding representing the detected building regions.

	Methods for building reconstruction by prismatic models are provided, too

	The following input data files can be used for building detection:
	<OL CLASS="sublist">
		<LI CLASS="sublist">
			A Barista (raster) DEM containing the last pulse DSM created from ALS data.
			This is the only input data file that has to be provided in any case.
		</LI>
		<LI CLASS="sublist">
			A Barista (raster) DEM containing the first pulse DSM created from ALS data.
			If no such DEM is provided, first pulse data will just not be considered.
		</LI>
		<LI CLASS="sublist">
			A Barista (raster) DEM containing the DTM (optional). If no DTM file is provided,
			the DTM will be created by hierarchical morphologic filtering.
		</LI>
		<LI CLASS="sublist">
			A digital orthophoto with float grey values representing the vegetation index (preferably: the NDVI).
			The NDVI can take any value between -100.0 and +100.0. 
			If no NDVI file is provided, the NDVI will not be used.
		</LI>
		<LI CLASS="sublist">
			A digital orthophoto with float grey values representing the standard deviation of the vegetation index (only if 
			the vegetation index is provided) in [%].
			It can be used to modulate the stochastic model for the vegetation index depending on the uncertainty of the vegetation index.
			If it is not provided, no such modulation is carried out.
		</LI>
	</OL>

    For a detailed description of the methodology please refer to the following publications: 

          Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik:
          Using the Dempster-Shafer Method for the Fusion of LIDAR Data 
          and Multi-spectral Images for Building Detection. 
          Information Fusion 6(4), pp. 283-300.

    and

          Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik:
          Building detection by fusion of airborne laser scanner data and multi-spectral images: 
		  Performance evaluation and sensitivity analysis. 
          ISPRS Journal of Photogrammetry and Remote Sensing 62 (2007), pp. 135�149.

	
 * 
 * \see
 * CDSMBuilding | CDempsterShaferBuildingClassifier
 */


class CBuildingDetector : public CProgressHandler
{
  protected:

//=============================================================================
//=================================== Data ====================================

	 //! The region of interest in the object coordinate system currently beingused for building extraction.
	 /*! By default, the window common to both DSM and DTM is used. */
	  C3DPoint pMin, pMax; 
	
	 //! The resolution in the object coordinate system to used for building extraction.
	 /*! By default, the resolution of the DSM is used. */
	  C2DPoint resolution; 
  
	 //! The DEM file representing the Digital Terrain Model (DTM). 
	  CDEM *pDTMFile;	     

	 //! The DEM file representing the Digital Surface Model (DSM) from last pulse data. 
	 /*! It should be computed from the ALS points without smoothing. */
	  CDEM *pDSMLastFile;

	 //! The DEM file representing the Digital Surface Model (DSM) from first pulse data. 
	 /*! It should be computed from the ALS points without smoothing. */
	  CDEM *pDSMFirstFile;
	
	 //! The image file representing the NDVI, in float values ranging from -100.0 to 100.0.
	  CHugeImage *pNDVIFile;        

	 //! The image file representing the standard deviation of the NDVI at each pixel, in float values representing [%]
	  CHugeImage *pNDVISigmaFile;        

	 //! The image file representing an existing data base
	  CHugeImage *pExistingDatabaseFile;        

	 //! A buffer containing the heights of the DTM in the region of interest as float values.
	  CImageBuffer *pDTM;      

	 //! A buffer containing the heights of the DSM in the region of interest as float values.
	  CImageBuffer *pDSM;     

	 //! A buffer containing the height differences between the first and last pulse DSMs in the region of interest as float values.
	  CImageBuffer *pFirstLastDiff;     
	 
	 //! A buffer containing the heights of the normalised DSM in the region of interest as float values.
	  CImageBuffer *pNormDSM; 
  
	 //! A buffer containing the vegetation index (NDVI or VVI) in the region of interest as float values.
	  CImageBuffer *pVegetationIndex;
	 
	 //! A buffer containing the standard deviation of the vegetation index in the region of interest as float values.
	  CImageBuffer *pVegetationIndexSig;

	 //! A binary image data buffer representing the existing building mask
	  CImageBuffer *pExistingBuildingMask;   
	
	 //! Classification results of Dempster - Shafer algorithm.
	  CDempsterShaferBuildingClassifier *pDempsterShafer;

	 //! An object representing surface roughness
	 /*! This object encapsulates the methods for applying the three-way-classification of Polymorphic 
	     Feature extraction according to Foerstner et al. to the DSM heights.
	     It is used to eliminate vegetaion areas. */
	  CDSMRoughness *roughness;  

	  //! An image data buffer representing connected areas of 'homogeneous' pixels; used to eliminate elevated roads
	  CLabelImage *pHomogeneousLabels;   

	 //! A binary image data buffer representing the building mask in all phases of processing.
	  CImageBuffer *pBuildingMask;  

	 //! An image data buffer representing the building label image in all phases of processing.
	 /*! For each pixel of this object, the grey level is the Index of the building region (also the index 
	     to the vector C_regionVector_ of CDSMBuilding objects. Region "0" corresponds to the "background", 
		 i.e., it consists of all pixels not classified as "buildings". */
	  CLabelImage *pBuildingLabels;   
	   
	 //! A vector of instances of class CDSMBuilding, the main results of building detection.
	 /*! The index of the vector is identical to the building index in the building label image. 
	     Thus, C_regionVector_[0] corresponds to the "background" and must not be used for any operation. */
	  vector <CDSMBuilding> regionVector;
	  
	 //! The size of the morphological filter for DTM generation in [m].
	 /*! If the DTM is read from a SCOP DEM file, it will not be used. */
	  float morphFilterSize;

	 //! Threshold for the height differences between DSM and DTM. 
	 /*! Used to create the initial building mask. */
	  float heightThreshold;             
	
	  float existingBuildPercentage;             

	 //! ThName of the TIFF file for storing the building label image. 
	 /*! Default: "labels.tif". */
	  CFileName labelFileName;   
	  
	  CTFW trafo;

	  const CTFW *imageTrafo;

	  const CTFW *existingTrafo;

	  CCharString projectPath;

	  bool useRank;

	  float rankPercentage;

//=============================================================================

   public:

//=============================================================================
//=============================== Constructors ================================ 
//
// The constructors will:
//
// - Check the validity of all input files
// - Prepare the data buffers
// - Read the DSM from a file
// - Read the Vegetation Index (NDVI or VVI) from a file if available
// - Read the standard deviations of the vegetation index from a file if available
// - Initialise the DTM, either by reading it from a file, or by
//   morphological filtering
// - Initialize the buffer for marking "valid" pixels (*pC_validMask_)
// 
// If a first pulse DSM file is available, it has to initialised separately using
// method v_initDSMfirstPulse(...) (see below)
//
// All the above data sets (if available) will be provided for the same region
// of interest (either of the DSM or provided by the user), sampled at the same
// resolution (either of the DSM or provided by the user) either by bilinear
// interpolation (DSM, DTM) or by nearest neighbourhood interpolation (NDVI, 
// Sigma_{NDVI}.
// 
// If any of the above operations fails, bit "negative" will be set to 1
// in C_error_, which can be queried using method i_fail(). If i_fail()
// returns another value than 0, the CBuildingDetector object cannot be
// used.
//
// "dsmanalyse.prt" and "dsmanalyse.err" are opened as protocol and error 
// files, respectively.
//
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   CBuildingDetector(float minBuildingSize, 
		                 CDEM *DSMLast = 0, CDEM *DSMFirst = 0, CDEM *DTM = 0,  
		                 CHugeImage *NDVI = 0, CHugeImage *NDVISigma = 0, 	
						 const CTFW *NDVITFW = 0,
						 CHugeImage *ExistingDatabase = 0,
						 const CTFW *ExistingTFW = 0,
						 float HeightThreshold = 2.5f,
						 const C2DPoint &Resolution = C2DPoint(-1, -1),
						 const C2DPoint &min = C2DPoint(0, 0), 
						 const C2DPoint &max = C2DPoint(-1, -1), 
						 const CCharString &directory = "");

// DSMLast:         DEM corresponding to the last pulse DSM.
// DTM:             DEM corresponding to the DTM.
//                  In the versions without IrC_dtmFilename, the DTM
//                  will be created from morphologic filtering
// minBuildingSize: Size of the morphologic filter for DTM generation.
// IrC_resolution:  The resolution at which DSM and DTM are to be sampled
// If_deltaZ:       height threshold for building extraction.
// IrC_vegIndex:    The name of a file containing a vegetation index (VVI or NDVI)
//                  if IrC_vegIndex == "", no such file is assumed to be available. 
// IrC_vegIndexSigma: The name of a file containing the standard deviation of the VI
//                  the VI. 
//                  if IrC_vegIndexSigma == "", no such file is assumed to be available. 
// The region of interest is initialized by the area covered by C_dsmRDH_.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void initWindows();

//=============================================================================
//================================ Destructor =================================

	   virtual ~CBuildingDetector ();


//=============================================================================
//================================ Query data =================================

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	   
	   bool isInitialised() const { return (this->pNormDSM != 0); }

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - - DSM and DTM resolutions - - - - - - - - - - - - - -
//- - - - - - - - - - - -  (object co-ordinate units) - - - - - - - - - - - - -

 
	   C2DPoint getResolution() const { return this->resolution; };

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - Region of interest as window, and its corners - - - - - - - -
//- - - - - - - - - - - - - - (object co-ordinates) - - - - - - - - - - - - - -

	   C2DPoint getPmin() const { return C2DPoint(this->pMin.x, this->pMin.y); }

	   C2DPoint getWindowNW() const { return C2DPoint(this->pMin.x, this->pMax.y); }

	   C2DPoint getPmax() const { return C2DPoint(this->pMax.x, this->pMax.y); }


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - Region of interest as window, and its size  - - - - - - - -
//- - - - - - - - - - - - - - - - (in [pixels}) - - - - - - - - - - - - - - - -

	   void getWindowSize(long &Width, long &Height) const;

	   void getWindow(long &cmin, long &rmin, long &Width, long &Height) const;


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - Number of pixels inside the region of interest  - - - - - - -

	   unsigned long getNumberOfPixels() const;


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - Are specific data available or not  - - - - - - - - - - 

	   bool hasVegIndex() const { return (pVegetationIndex != 0); };

	   bool hasVegIndexSigma() const { return (pVegetationIndexSig != 0); };

	   bool hasFirstLastDiff() const { return (pFirstLastDiff != 0); };


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - Filename of the label image - - - - - - - - - - - - -

	   CFileName getLabelFileName() const { return labelFileName; };
    

//=============================================================================
//======== Make the results of building detection persistent on a file ========


	   bool makePersistent(CHugeImage *labelHug, CTFW &tfw, const CFileName &filename);

// The results of building detection will be written to two files:
// 1. A list of detected buildings with their most important parameters will be
//    written to file filename
// 2. The label image will be written to labelHug, and the geocoding information
//    to tfw. The filename of labelHug will be used for that.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//=== Set name of the tiff file containing the building label image (output) ==

	   void setLabelFileName(const CFileName &filename);

//=============================================================================

	   void setDTMmethodToMorpho();

	   void setDTMmethodToRank(float rankPercent);

//=============================================================================

	   bool getBuildings(float thinningThreshold,
		                 CBuildingArray &buildings);

//=============================================================================
//============ Generate DTM by hierarchical morphologic filtering  ============

	void generateDTMmorph(int filterFoerstner, float percentageTrees, 
		                  float thresholdQ,    bool useFirstPulse,    
						  int structure,       float minArea,   
						  float minArea0,      float pointThr,  
						  float homThr,        float pointThr0, 
						  float homThr0,       float morphDTM,  
                          float ndviThr,       bool useNDVI);

// With respect to the algorithm, cf. the following publication:
//
// Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik: 
// Building Detection Using LIDAR Data and Multispectral Images. 
// In: Proceedings of the APRS Conference on Digital Image Computing � 
// Techniques and Applications, Vol. II, Sydney (Australia), 2003, pp. 673-682.
//
// DTM generation consists of iterative calls to methods 
// v_initHeightSegmentation(...) and  v_iterateDTMmorph(...).
//
// IC_sMorph:    Size of the square structural element for morphologic 
//               filtering of the initial building mask. 
//               Used to separate building regions connected by thin lines
//               of pixels and to remove small elongated objects (e.g. walls).
// IC_sFilter0:  Size of the structural element for morphological grey scale
//               opening of the DSM in the first iteration of DTM generation.
// If_minArea:   Minimum area of a connected component in [m^2]. 
//               Connected components smaller than If_minArea are discarded.
// If_minArea0:  Minimum area of a building in the first iteration of 
//               DTM generation
// If_pointThr:  Maximum percentage of "point" pixels allowed in a building
//               region (0 <= If_pointThr <= 1)
// If_homThr:    Minimum percentage of homogeneous pixels required in a building
//               region (0 <= If_homThr <= 1)
// If_pointThr0: Maximum percentage of "point" pixels allowed in a building
//               region (0 <= If_pointThr <= 1) in the first iteration of 
//               DTM generation
// If_homThr0:   Minimum percentage of homogeneous pixels required in a building
//               region (0 <= If_homThr <= 1) in the first iteration of 
//               DTM generation
// If_morphDTM:  Minimum size of structural element for morhphological greyscale
//               filtering  in DTM generation.
// Ii_neighbourhood: 8-neighbourhood or 4-neighbourhood for label image creation
// Ii_useNDVI:     0: Do not use the NDVI in the creation of the initial 
//                    building mask for DTM generation
//               <>0: Use the NDVI in the creation of the initial building mask
//                    for DTM generation
// Ii_ndviThr:   Threshold for the NDVI for DTM generation (if it is to be used)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   

//=============================================================================
//============================= Building detection ============================

	   void detectBuildings(bool  firstPulseForRoughness, int filterFoerstner,  
		                    float percentageTree, float thresholdQ, 
							float thresholdArea,  float thresholdPoint, 
							float thresholdHom,   float thresholdNDVI,
							bool  useNDVI, 
							int   minimumFilter, int filterSmallMorph);

// Building detection by rule-based classification. The DTM has to be provided
// as a SCOP RDH file (i.e., there is no hierarchic morphologic filtering 
// for DTM generation).
// With respect to the algorithm, cf. the following publication:
//
// Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik: 
// Building Detection Using LIDAR Data and Multispectral Images. 
// In: Proceedings of the APRS Conference on Digital Image Computing � 
// Techniques and Applications, Vol. II, Sydney (Australia), 2003, pp. 673-682.
//
//
// Parameters for surface roughness analysis:
// ------------------------------------------
//
// firstPulseForRoughness: 
//                   true:  Use last pulse DSM for surface roughness analysis
//                   false: Use First pulse DSM for surface roughness analysis
// filterFoerstner:  Kernel size for the classification of polymorphic feature 
//                   extraction (has to be an odd number)
// percentageTree:   The threshold for classification of homogeneous regions 
//                   will be selected so that percentageTree * 100% of the pixels
//       	         are classified as "non-homogeneous"; 0 <= percentageTree <= 1
// thresholdQ:       Threshold for the classification of non-homogeneous areas
//                   to distinguish "point" and "line" areas in polymorphic feature 
//                   extraction; 0 <= thresholdQ <= 1
//
//
// Parameters for classification:
// ------------------------------
//
// thresholdArea:    Minimum area of a connected component in [m^2]. 
//                   Connected components smaller than thresholdArea are discarded.
// thresholdPoint:   Maximum percentage of "point" pixels allowed in a building
//                   region (0 <= thresholdPoint <= 1)
// thresholdHom:     Minimum percentage of homogeneous pixels required in a building
//                   region (0 <= thresholdHom <= 1)
// thresholdNDVI:    Threshold for the NDVI (if it is to be used)
// useNDVI:          false: Do not use the NDVI in the creation of the initial 
//                          building mask
//                   true:  Use the NDVI in the creation of the initial building mask
// minimumFilter:    Size of the minimum filter for post-classification to separate
//                   trees connected to buildings. If minimumFilter == 0,
//                   post-classification will not be carried out.
// filterSmallMorph: Size of the square structural element for morphologic filtering
//                   of the initial building mask. 
//                   Used to separate building regions connected by thin lines
//                   of pixels and to remove small elongated objects (e.g. walls).
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void detectBuildings(bool  firstPulseForRoughness, int filterFoerstner,  
		                    float percentageTree, float thresholdQ, 
							float thresholdArea,  int   filterSmallMorph,
							float thresholdPoint, float thresholdHom,   
							float thresholdNDVI,  bool  useNDVI,
                            float thresholdArea0, float thresholdPoint0,   
							float thresholdHom0,  float DTMmorphFilter,
                            int   sensorOption, 
							const CFileName &probabilityModelFile,
							float minRoughnessQuantil,
							float sigmaNDVIMultiplicator,
                            eDECISIONRULE decisionRule,
							int   maxPostClassIterations,
                            bool  classifyRegions, 
							int   closingFilter,
							int   minimumFilter,
							bool  createVegetationLayer,
							bool  useThetaForFirstLast,
							float probExisting,
							bool useWaterModel);

// Building detection by Dempster-Shafer fusion. The DTM can either be 
// provided on a DEM file, or it is created by hierarchic morphological
// filtering.
//
// Parameters for surface roughness analysis:
// ------------------------------------------
//
// firstPulseForRoughness: 
//                   true:  Use last pulse DSM for surface roughness analysis
//                   false: Use First pulse DSM for surface roughness analysis
// filterFoerstner:  Kernel size for the classification of polymorphic feature 
//                   extraction (has to be an odd number)
// percentageTree:   The threshold for classification of homogeneous regions 
//                   will be selected so that percentageTree * 100% of the pixels
//       	         are classified as "non-homogeneous"; 0 <= percentageTree <= 1
// thresholdQ:       Threshold for the classification of non-homogeneous areas
//                   to distinguish "point" and "line" areas in polymorphic feature 
//                   extraction; 0 <= thresholdQ <= 1
//
// General parameters:
// -------------------
//
// filterSmallMorph: Size of the square structural element for morphologic 
//                   filtering of the initial building mask. 
//                   Used to separate building regions connected by thin lines
//                   of pixels and to remove small elongated objects (e.g. walls).
// thresholdArea:    Minimum area of a connected component in [m^2]. 
//                   Connected components smaller than thresholdArea are discarded.
//
// 
// Parameters for hierarchical morphological filtering:
// ----------------------------------------------------
//
// thresholdArea0:  Minimum area of a building in the first iteration of 
//                  DTM generation
// thresholdPoint0: Maximum percentage of "point" pixels allowed in a building
//                  region (0 <= thresholdPoint0 <= 1) in the first iteration of 
//                  DTM generation
// thresholdHom0:   Minimum percentage of homogeneous pixels required in a building
//                  region (0 <= thresholdHom0 <= 1) in the first iteration of 
//                  DTM generation
// thresholdPoint:  Maximum percentage of "point" pixels allowed in a building
//                  region (0 <= thresholdPoint <= 1)
// thresholdHom:    Minimum percentage of homogeneous pixels required in a building
//                  region (0 <= thresholdHom <= 1)
// thresholdNDVI:   Threshold for the NDVI for DTM generation (if it is to be used)
// useNDVI:         false: Do not use the NDVI in DTM generation
//                  true:  Use the NDVI in the creation of the initial building mask
// DTMmorphFilter:  Minimum size of structural element for morhphological greyscale
//                  filtering in DTM generation [m^2].
//
//
// Parameters for Dempster-Shafer Classification:
// ----------------------------------------------
//
// sensorOption:           Decide which sensors are to be used (cf. documentation
//                         of constructor of class CDempsterShaferClassifier in 
//                         ph_dmpShBuild.hpp)
// useFirstPulseDSM:       Use first pulse data to compute DSM-DTM (<>0) or 
//                         use lastpulse data (0)
// probabilityModelFile:   Name of ASCII file containing the parameters of the 
//                         model for the probability masses (cf. the documentation 
//                         of CDempsterShaferClassifier::v_initParametersFromFile in
//                         ph_dmpShBuild.hpp)
// minRoughnessQuantil:    Input for CDempsterShaferClassifier::f_significantRoughQuantil_  
//                         (cf. documentation of constructor of class 
//                         CDempsterShaferClassifier in ph_dmpShBuild.hpp)
// sigmaNDVIMultiplicator: Multiplication constant for the standard deviation of 
//                         the NDVI; cf. documentation of class 
//                         CDempsterShaferClassifier in ph_dmpShBuild.hpp)
// decisionRule:           Decision rule for Dempster-Shafer classification
//                         (cf. ph_dempsterShafer.hpp).
// maxPostClassIterations: Maximum number of iterations of post-classification
//                         in pixel-based classification
// classifyRegions:        Apply region-based classification (<>0) or not (0)
//
// Parameters for post processing: 
// -------------------------------
//
// minimumFilter:          Size of the minimum filter for post-classification to 
//                         separate trees connected to buildings. 
//                         If minimumFilter == 0, post-classification will 
//                         not be carried out.
// closingFilter:          Size of square structural element for morphological 
//                         closing of the label images (odd number). 
//                         Morphological closing will increase the building sizes 
//						   at the boundaries and smooth the building boundaries; 
//                         this is meant to compensate for classification 
//                         errors at building outlines.
//                         if (closingFilter < 3), morphological closing 
//                         will not be applied.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//======== Export intermediate and final results of building detection ========

	   void exportBuildingMask (const CFileName &filename = "mask.tif") const;

// Export building mask to binary TIFF file filename
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void exportDSM(const CFileName &filename, bool useLUT = false) const;

	   void exportFirstLastDiff(const CFileName &filename, bool useLUT = false) const;

	   void exportNDVI(const CFileName &filename, bool useLUT = false) const;

	   void exportNDVISigma(const CFileName &filename, bool useLUT = false) const;

	   void exportDTM(const CFileName &filename, bool useLUT = false) const;

	   void exportNormDSM(const CFileName &filename, bool useLUT = false) const;

	   bool dumpNormDSM(const char *FileName, CDEM *dem) const;

	   bool dumpDTM(const char *FileName, CDEM *dem) const;

// if nBands == 1:    Export DSM to TIFF palette file filename, 
//                    merging heights to interval (0 .. 255)
// otherwise:         Export DSM to RGB TIFF file filename, converting the
//                    heights Z to RGB values as follows:
//                    R =  (Z * factor) % 256
//                    G = [(Z * factor) / 256] % 256
//                    B =  (Z * factor) / (256 * 256)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void exportNormDerivatives(const CFileName &path) const ;

// Export classification results of polymorphic feature extraction to
// IrC_path\foerstner.tif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void exportBuildingLabelImage(CFileName fileName = "") const;

// Export building label image to TIFF palette file filename, using label
// 255 for all labels >= 255. If IC_fileName == "", labelFileName will
// be used.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   bool makeLabelImagePersistent(CHugeImage *labelHug, CTFW &tfw) const;

// Export building label image to a 16 bit TIFF file. 
// The filename of labelHug will be used "as-is".
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	   void exportBuildings(const CFileName &regionFile,
                            const CFileName &winputFile,
							float thinningThreshold, unsigned long modelName,
							bool polygonFlag, bool appendFlag, bool deleteFlag,
							bool vrmlFlag);

// Write the data of all building regions in C_regionVector_ to file 
// IrC_regionFile.
// If Ii_polygonFlag == 1, the boundary polygons of the building regions will
// be extracted. They will be thinned out using the threshold 
// If_thinningThreshold (in [pixels]), and they will be written to the
// WINPUT file IrC_winputFile.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============================= protected methods =============================

  protected:

//=============================================================================
//============================ Auxiliary function  ============================

//=============================================================================
//======================= Prepare and erase data buffers ======================

	  void getAdaptedWindow(int ICmin, int IRmin, int IWidth, int IHeight,
		                    int &Cmin, int &Rmin, int &Width, int &Height);

// Auxiliary method changing IC_window so that its corner points co-incide
// with pixels of the DSM. The adapted window is returned.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  void setUpTrafoAndExtents(const C2DPoint &Resolution,
		                        const C3DPoint &min, const C3DPoint &max);


	  bool readDEMWindow(CDEM *pDEM, CImageBuffer &buf, const CCharString &name);

	  bool readImageWindow(CHugeImage *pImg, CImageBuffer &buf, const CCharString &name);

	  bool readExistingDatabase();


// Call the constructors of all image data buffers after the SCOP DTM files
// have been checked.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  bool createDTMbyMorphology(int structureX, int structureY);

// Create the DTM by morphologic filtering of pDSM.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	  void initNormDSM(bool DTMread);

//=============================================================================
//============ Generate DTM by hierarchical morphologic filtering  ============

	void generateDTMmorph(int structure, 
                          float minArea,   float minArea0, 
                          float pointThr,  float homThr, 
                          float pointThr0, float homThr0, 
                          float morphDTM,  int filter0, 
                          float ndviThr,   bool useNDVI);

// With respect to the algorithm, cf. the following publication:
//
// Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik: 
// Building Detection Using LIDAR Data and Multispectral Images. 
// In: Proceedings of the APRS Conference on Digital Image Computing � 
// Techniques and Applications, Vol. II, Sydney (Australia), 2003, pp. 673-682.
//
// DTM generation consists of iterative calls to methods 
// v_initHeightSegmentation(...) and  v_iterateDTMmorph(...).
//
// IC_sMorph:    Size of the square structural element for morphologic 
//               filtering of the initial building mask. 
//               Used to separate building regions connected by thin lines
//               of pixels and to remove small elongated objects (e.g. walls).
// IC_sFilter0:  Size of the structural element for morphological grey scale
//               opening of the DSM in the first iteration of DTM generation.
// If_minArea:   Minimum area of a connected component in [m^2]. 
//               Connected components smaller than If_minArea are discarded.
// If_minArea0:  Minimum area of a building in the first iteration of 
//               DTM generation
// If_pointThr:  Maximum percentage of "point" pixels allowed in a building
//               region (0 <= If_pointThr <= 1)
// If_homThr:    Minimum percentage of homogeneous pixels required in a building
//               region (0 <= If_homThr <= 1)
// If_pointThr0: Maximum percentage of "point" pixels allowed in a building
//               region (0 <= If_pointThr <= 1) in the first iteration of 
//               DTM generation
// If_homThr0:   Minimum percentage of homogeneous pixels required in a building
//               region (0 <= If_homThr <= 1) in the first iteration of 
//               DTM generation
// If_morphDTM:  Minimum size of structural element for morhphological greyscale
//               filtering  in DTM generation.
// Ii_neighbourhood: 8-neighbourhood or 4-neighbourhood for label image creation
// Ii_useNDVI:     0: Do not use the NDVI in the creation of the initial 
//                    building mask for DTM generation
//               <>0: Use the NDVI in the creation of the initial building mask
//                    for DTM generation
// Ii_ndviThr:   Threshold for the NDVI for DTM generation (if it is to be used)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
   
	  void resetRoughness();

// delete roughness if it is no longer required, to save memory
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//==== Apply morphologic filtering and/or minimum filter to building mask =====

	void openBuildingMask(int structure);

	void closeBuildingMask(int structure);

	void openCloseBuildingMask(int structure);

	void minimumBuildingMask(int filter);

// structureX, structureY: Size of a square structural element for morphologic 
//                         morphologic filtering (co-ordinates have to be odd 
//                         numbers > 1).
// filterX, filterY:       Size of the minimum filter
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 
//=============================================================================
//=================== Individual steps of building detection ==================

	void initBuildingMask();

// 1.  Set building mask to 1 at all valid positions where DSM - DTM > heightThreshold
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
// 2.  open builidng mask morphologically (see above)
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void findRegions(float minArea, bool removeBorder);

// 3.  perform connected component analysis and discard regions smaller than
//     If_minArea
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void classifyPolymorphic(int filterFoerstner, float quantil, float thresholdQ,   
		                     bool useFirstPulse);

// 4.  perform the classification of polymorphic feature extraction 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void removeVegetation(float pointThr, float homThr,   
                          float ndviThr, bool evalNDVI);

// 5.  remove initial connected components if their percentage of homologous 
//     pixels is smaller than If_homThr or if their percentage of "point" pixels 
//     is greater than If_pointThr. 
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void splitOffAndRemoveTrees(int minimumFilter, int structureSize, 
                                float minArea, bool removeBorder);
// 6.  apply a minimum filter of size Ii_minimumSize to separate trees connected
//     connected to building regions from these regions. The resulting  
//     mask is opened using a structural element of size Ii_structureSize.
//     New segments larger than If_minArea are detected, and small noisy 
//     regions are flagged using v_removeVegetation(...).
//     These regions are erased in the original building mask, and it is
//     opened once more to erase pixels at the margins of the erased (vegetation)
//     areas. The parameters of the remaining and improved building regions
//     are updated.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//============= Auxiliary method for connected component analysis =============

	void removeBorderRegions(CLabelImage &labelimg);

//=============================================================================
//================ Auxiliary method for closing of label image ================
    
	void closeLabelImage(int structure, bool checkheights);

// Applies a morphologic closing operation using a rectangular structural  
// element of size IC_sMorph to the binary image of building pixels. 
// The voronoi of the origninal building label image is used to decide 
// which building region "new" building pixels are assigned to after closing;
// This results in a "morphological closing" of the label image, without
// joining originally separated building regions.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//===== Update building mask so that it is consistent with the label image ====

	void updateBuildingMask();


//=============================================================================
//============= Remove large regions and compute region parameters ============

	void removeLargeRegions(CLabelImage &labelimg, vector <CDSMBuilding> &regVec, 
                            long maximumNumberOfPixels);

// All regions in labelimg and regVec containing more than 
// maximumNumberOfPixels pixels are discarded, and the computations of the
// parameters of the remaining regions are prepared
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


//=============================================================================
//============= Remove small regions and compute region parameters ============

	void removeSmallRegions(CLabelImage &labelimg, vector <CDSMBuilding> &regVec, 
                            long minimumNumberOfPixels);

// All regions in labelimg and regVec containing less than 
// minimumNumberOfPixels pixels are discarded, and the computations of the
// parameters of the remaining regions are prepared
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 


//=============================================================================
//======== Remove small elongated regions and compute region parameters =======

	void removeSmallElongated(int morphSize);

// All regions erased by morphologic filtering with a structural element of  
// size morphSize are discarded, and the parameters of the remaining regions
// are computed
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void updateRegionParameters(CLabelImage &labelimg, 
                                vector <CDSMBuilding> &regVec);

// After having called removeSmallRegions() and, thus, prepared the 
// computation of the parameters of all regions in regVec, these
// parameters are computed
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

//=============================================================================
//====================== Create the final normalised DSM ======================

	void computeFinalNormDsm();

// The resulting normalised DSM will have
// Z = Z_DSM - Z_MIN in the building regions (thus, DSM heights will be reduced
//                   by the minimum height of the DTM in the building region)
// 0                 outside the building regions
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void computeFinalNormDTM();

//=============================================================================
//=================== Auxiliary Methods for the Constructors ==================

	void morphFilter(float filterSizeMetric, int &structure);

// Create a point with the extents of a square structural element of size 
// If_filterSize for morphologic filtering. If_filterSize is given in [m],
// the return value will be in [pixel]. If If_filterSize corresponds to an 
// even number of pixels, the actual filter size will be increased by 1 pixel.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void initHeightSegmentation(float morphForBuildings, int structureOpen,
                                float vegetationIndexThreshold,
	                            CLabelImage *labelimg,
                                int itCount);

// Apply a morphological grey scale opening to the DSM and create an initial
// normalised DSM (DSM - DTM). Using that normalised DSM, a binary image of 
// building candidate pixels is created:
// All pixels with a normalised DSM > a certain height threshold, an 
// NDVI < Ii_vegetationIndexThreshold (if available) and a height difference
// between first and last pulse DSM < a certain height threshold are considered
// to be building candidate pixels. The binary building candidate image thus
// created is filtered by morphologic opening to separate weakly connected 
// regions and to smooth the building outlines.
//
// f_morphForBuildings: size of the structural element for morphologic grey 
//                      scale opening of the DSM in [m]
// IpC_labelimg:        pointer to a building label image created in the previous
//                      iteration of DTM generation. For all building pixels of
//                      the previous iteration (i.e., where IpC_labelimg <> 0),
//                      the DTM height of the previous iteration is used to compute
//                      the normalised DSM; otherwise, the results of grey scale
//                      opening are used. In the first iteration, IpC_labelimg == 0
//                      (because there was no 
// IC_sMorph:           size of the structural element for morphologic opening
//                      of the binary image of building candidate pixels [pixel]
// Ii_vegetationIndexThreshold: Threshold for the NDVI (if available).
// Ii_itCount:          Iteration count (used to generate output file names).
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void iterateDTMmorph(float minArea, float pointThr, float homThr,  
		                 float ndviThr, int clsSize,  int itCount,   
						 bool useNDVI,  float eliminateSize);

// Carry out one iteration in DTM generation: After a normalised DSM and an
// initial binary image of building pixels have been created using 
// v_initHeightSegmentation(...), rule-based building extraction is applied
// to obtain a building label image (which is used in the next iteration 
// to decide where the morphologic grey scale opening results do not correspond
// to the terrain).
//
// If_minArea:   Minimum area of a connected component in [m^2]. 
//               Connected components smaller than If_minArea are discarded.
// If_pointThr:  Maximum percentage of "point" pixels allowed in a building
//               region (0 <= If_pointThr <= 1)
// If_homThr:    Minimum percentage of homogeneous pixels required in a building
//               region (0 <= If_homThr <= 1)
// Ii_neighbourhood: 8-neighbourhood or 4-neighbourhood for label image creation
// Ii_useNDVI:     0: Do not use the NDVI in the creation of the initial 
//                    building mask for DTM generation
//               <>0: Use the NDVI in the creation of the initial building mask
//                    for DTM generation
// Ii_clsSize:   Size of structural element for morphological closing of building
//               label image in this iteration.
// Ii_ndviThr:   Threshold for the NDVI for DTM generation (if it is to be used)
// Ii_itCount:          Iteration count (used to generate output file names).
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void checkHomogeneityLabels(bool eliminateLabels);

//=============================================================================
//==================== Methods for Dempster-Shafer Fusion  ====================

	void dempsterShafer(int structure, int sensorOption, 
                        float minArea,  eDECISIONRULE rule, 
                        const CCharString &suffix, 
						const CFileName &thresholdFile,
                        float dSqQuantil, 
                        float sigmaMultiplicator,
                        bool classifyRegions,
                        int maxIterationsForPostClass,
                        bool  useThetaForFirstLast,
						float probExisting,
						bool useWaterModel);


// IC_sMorph:     Size of the square structural element for morphologic 
//                filtering of the initial building mask. 
//                Used to separate building regions connected by thin lines
//                of pixels and to remove small elongated objects (e.g. walls).
// Ii_sensorOption: Decide which sensors are to be used (cf. documentation of 
//                constructor of class CDempsterShaferClassifier in ph_dmpShBuild.hpp)
// Ii_neighbourhood: 8-neighbourhood or 4-neighbourhood for label image creation
// If_minArea:    Minimum area of a connected component in [m^2]. 
//                Connected components smaller than If_minArea are discarded.
// Ie_rule:       Decision rule for Dempster-Shafer classification
//                (cf. ph_dempsterShafer.hpp).
// IrC_suffix:    String for creating output filenames.
// IrC_thresholdFile:Name of ASCII file containing the parameters of the model
//                for the probability masses (cf. the documentation of 
//                CDempsterShaferClassifier::v_initParametersFromFile in
//                ph_dmpShBuild.hpp)
// Ii_classifyRegionFlag: Apply region-based classification (<>0) or not (0)
// If_dSqQuantil: Input for CDempsterShaferClassifier::f_significantRoughQuantil_  
//                (cf. documentation of constructor of class CDempsterShaferClassifier 
//                in ph_dmpShBuild.hpp)
// Ie_probabilityMode: Select model for the probability masses (cf. ph_dmpShBuild.hpp)
// Ii_maxIterationsForPostClass: Maximum number of iterations of post-classification
//                in pixel-based classification
// Ii_flModeFlag: Switch between two different models of distributing the 
//                probability masses from the height differences between 
//                first and last pulse:
// If_sigmaMultiplicator: Multiplication constant for the standard deviation of 
//                the NDVI; cf. documentation of class 
//                CDempsterShaferClassifier in ph_dmpShBuild.hpp)
// i_usePercentageForHomogeneity: Select the parameterisation of DSM roughness
//                strength:
//                  0: use the DSM roughness strength values (in units of the 
//                     median) to parameterise the probability mass model for
//                     DSM roughness strength
//                <>0: use the percentage of pixels having a DSM roughness 
//                     strength smaller than a certain valueto parameterise 
//                     the probability mass model for DSM roughness strength
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void dempsterShaferPixels(eDECISIONRULE rule, const CCharString &suffix,
                              int maxIterationsForPostClass);

// Pixel-wise classification based on the Dempster-Shafer theory.
//
// Ie_rule:       Decision rule for Dempster-Shafer classification
//                (cf. ph_dempsterShafer.hpp).
// IrC_suffix:    String for creating output filenames.
// Ii_maxIterationsForPostClass: Maximum number of iterations of post-classification
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void dempsterShaferRegions(eDECISIONRULE rule,const CCharString &suffix,
		                       bool useWaterModel);

// Region-wise classification based on the Dempster-Shafer theory.
//
// Ie_rule:       Decision rule for Dempster-Shafer classification
//                (cf. ph_dempsterShafer.hpp).
// IrC_suffix:    String for creating output filenames.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

	void initHomogeneousLabels(int filterSize);

	static void exportFloatBuf(const char *fn, const CImageBuffer &fltBuf, bool uselut);

	

//=============================================================================
//=========================== Experimental methods  ===========================

	void createVegetationLayer();

	void checkInnerCourtyards(int closeSize);

	void morphoOrRank(const CImageBuffer &source, CImageBuffer &destination, const CImageFilter &filter);

//=============================================================================

};

//****************************************************************************

#endif

#endif //WIN32

