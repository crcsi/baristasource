#ifndef ___BuildingsDetector___
#define ___BuildingsDetector___

#include "CharString.h"
#include "2DPoint.h"
#include "ProgressHandler.h"
#include "FeatExtractPars.h"
#include "XYPointArray.h"
#include "XYContourArray.h"
#include "XYPolyLineArray.h"
#include "doubles.h"
#include "DoubleVector.h"

typedef struct{
	int HistInd[37];//indices 0:35 for histogram Hind and 36: for sum of histogram sum(Hind)
}HindType;

class CALSDataSet;
class CImageBuffer;
class CHugeImage;
class CVImage;
class CTFW;
//class CBestFitLine;
//class CLeastSquaresSolver;
//****************************************************************************
//*************************** Forward Declarations ***************************
 
//****************************************************************************

class BuildingsDetector : public CProgressHandler
{

//=============================================================================
 
  protected:

	CALSDataSet  *pALS;
	CHugeImage *rgbImage;
	CHugeImage *sigmaNDVI8bit;
	CHugeImage *pDEM;
    const CTFW *tfwImage;
	const CTFW *tfwDEM;
	CTFW *tfwMask;
	CImageBuffer *gMask;
    CImageBuffer *upMask;
  	const C2DPoint pMin, pMax; 
	const C2DPoint resolution;
	const C2DPoint resolutionDEM;
	const C2DPoint resolutionMask;
	const C2DPoint resolutionImage;
	CCharString projectPath;
	int minBuildingLength;
	int maxBuildingLength;
	double sqrMaxBuildingLength;
	//double halfSqrMaxBuildingLength;
	double thetaThresh;
	const float heightThr;
	const int   patchSize;
	unsigned int nPoint, nLine;
	int w, h; //width & height of masks 
	int minContourLength;
	bool useNDVIinExtension;
	bool useEntropy;
	bool useHistogram;
	 bool detectionMode; // true for new building detection mode, false for old building refinement
	double dAl; // half size (in pixels) of reactangle around a line C1C2
	double edgeThresh; //threshold to determine building side with respect to line a C1C2
	double ndviThresh; // NDVI threshold
	double entropyThresh;//entropy mask threshold
	double iniBuildingReductionThresh; // percentage at which the initial building dimensions are reduced before extending for final buildings

	//	
	CXYPolyLineArray iniBuildings;
	CXYPolyLineArray finalBuildings;

	//feature extraction
	//FeatureExtractionParameters parameters;

	//extracted lines 
	//CXYPolyLineArray lines;
	vector <double> M, C; //for m and c of extracted lines	C1C2
	vector <bool> isLineActive; //whether lines are active
	//vector <unsigned int> cID; // curve ID for each extracted line: will be used for local adjustment

	//for image
	vector <double> Mimg, Cimg; //for m and c of extracted image lines	C1C2

	CXYPointArray insidePoints;
	CXYPointArray midPoints, midPointsImg;
	doubles lengths;

	CLabel Corner;
	CLabel CurrentLineLabel;
//=============================================================================
	unsigned short binDist;// = 5;
	unsigned short histSize;
	float minMax2MeanRatio;// = 4.0;
	float minMax2MeanRatio2;// = 3.0;
	float minArea2TextureRatio;// = 45.0;
	//%minMax2TextureRatio = 0.20;

	//float Resolution = 0.10; //%0.15m for FF and HB, 0.10m for MV & Knox
	float minAggregateBinHeight;// = 9/Resolution; %9m = 90 pixels for knox & MV, 60 pixels for FF
	float minAggregateBinHeight2;// = 6/Resolution;%6m = 60 pixels for knox & MV, 40 pixels for FF
	float minBinHeight;// = 3/Resolution; %3m = 30 pixels for knox & MV, 20 pixels for FF
	float maxTotal;// = 90/Resolution;
//====================================================

   public:

//=============================== Constructors ================================ 

	   BuildingsDetector(CALSDataSet *ALS, CHugeImage *colorImage, const CTFW *imgTFW,
						const C2DPoint &min, const C2DPoint &max, const C2DPoint &grid,
						const CCharString &directory, bool useNDVI, bool useEntropy, bool useHistogram, bool detectionMode);

	   BuildingsDetector (CALSDataSet *ALS, CHugeImage *colorImage, CHugeImage *dem, const CTFW *imgTFW, const CTFW *demTFW,
									  const C2DPoint &min, const C2DPoint &max, const C2DPoint &grid,  const C2DPoint &gridDEM,
									  const CCharString &directory, bool useNDVI, bool useEntropy, bool useHistogram, bool detectionMode);
	   BuildingsDetector (CHugeImage *colorImage, const CTFW *imgTFW, const CCharString &directory, bool detectionMode);
//================================ Public member functions ====================
	   void setFeartureExtractionParameters();
	   bool maskCreater(CHugeImage *imUpMask, CHugeImage *imGMask, int wdth, int hght);
	   bool maskCreater(CHugeImage *imUpMask, CHugeImage *imGMask, CTFW *imgTFW, bool localDEMheight);
	   //bool maskCreator(CHugeImage *imUpMask, CHugeImage *imGMask, CTFW *imgTFW);

	   //bool extract(CHugeImage *imUpMask, CHugeImage *imGMask, CXYPointArray *xypts, CXYContourArray *xycont, CXYPolyLineArray *xyedges);
	   bool extractMasks(CHugeImage *imUpMask, CHugeImage *imGMask, int wdth, int hght);
	   bool extractMasks(CHugeImage *imUpMask, CHugeImage *imGMask, CTFW *imgTFW);
	   //bool extract(CVImage *imUpMask, CVImage *imGMask);
	   bool extractContours(CXYContourArray &xycont);
	   bool detectCornersAndLines(CXYPointArray &xypts, CXYContourArray &xycont, CXYPolyLineArray &xyedges, bool imageMode = false);
	   void setBufferOfContourXY(CImageBuffer &oriLineX, CImageBuffer &oriLineY, CXYContour *contour);
	   void setContourFromBufferXY(CImageBuffer smoothLineX, CImageBuffer smoothLineY, CXYContourArray &xycont, int index);
	   void extendContourXY(CImageBuffer oriLineX, CImageBuffer oriLineY, CImageBuffer &extLineX, CImageBuffer &extLineY, int W);
	   void contractContourXY(CImageBuffer smoothLineX, CImageBuffer smoothLineY, CImageBuffer &conSmoothX, CImageBuffer &conSmoothY, int W);
	   //void findCornersAndLines(CImageBuffer conSmoothX3, CImageBuffer conSmoothY3, CImageBuffer conSmoothX, CImageBuffer conSmoothY, CXYPointArray &xypts, CXYPolyLineArray &xyedges, unsigned int curCurveID);
	   void findCornersAndLines(CImageBuffer conSmoothX3, CImageBuffer conSmoothY3, CImageBuffer conSmoothX, CImageBuffer conSmoothY, CXYPointArray &xypts, CXYPolyLineArray &xyedges, bool imageMode = false);
	   bool FitLine(CImageBuffer conSmoothX3, CImageBuffer conSmoothY3, int start, int end , double &m, double &c);
	   void applyNDVIandEdgeThresh(CHugeImage *gndMask, CHugeImage *sigmaNDVI, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges);
	   void applyNDVIandEdgeThresh(CHugeImage *gndMask, CHugeImage *sigmaNDVI, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CXYPolyLineArray &ndvidges);
	   void applyNDVIandEdgeThresh(CHugeImage *gndMask, CHugeImage *sigmaNDVI, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CXYPolyLineArray &ndvidges, CHugeImage *entropyMask);

	   void findRectVertices(double m, double c, CXYPoint C1, CXYPoint C2, CXYPoint &P1, CXYPoint &P2, CXYPoint &P3, CXYPoint &P4, double d);
	   void findPointsInside(CHugeImage *hugImg, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, unsigned long &total, unsigned long &nP);
	   void findPointsInside(CHugeImage *hugImg, CHugeImage *hugImg2, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, unsigned long &total, unsigned long &nP, unsigned long &total2, unsigned long &nP2);
	   void findPointsInside(CHugeImage *hugImg, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, CDoubleVector &vals);
	   void findPointsInside(CHugeImage *hugImg, CHugeImage *hugImg2, CHugeImage *hugImg3, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, CDoubleVector &vals, CDoubleVector &vals2, CDoubleVector &vals3);

	   bool isInsideRectangle(CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, CXYPoint P);
	   void findMinMax(CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, long &minX, long &minY, long &maxX, long &maxY, int wd, int ht);
	   void findNDVImeanInside(CHugeImage *sigmaNDVI, CHugeImage *cropImg, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, CXYPointArray &xypts, double &meanNDVI);
	   
	   void adjustLines(CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CXYPolyLineArray &edgesout);
	   void adjustLinesWithImage(CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xyptsImg, CXYContourArray &xycontImg, CXYPolyLineArray &xyedgesImg, CXYPolyLineArray &xyedges);

	   //void extendLines(CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges);
	   void extendLines(CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CHugeImage *entropyMask);
	   void extendSegment(unsigned int index, CXYPoint &C1, CXYPoint P1, CXYPoint C2, CHugeImage *gndMask, bool Flag, CHugeImage *cropImg, CXYPointArray &xypts, double dAL, CHugeImage *entropyMask);
	   void extendSegment(double m, double c, CXYPoint &C1, CXYPoint &P1, CXYPoint C2, CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xypts, double dAL, CHugeImage *entropyMask);
	   //void extendSegment(double m, double c, CXYPoint C1, CXYPoint P1, CXYPoint C2, CXYPoint &C1_1, CXYPoint &P1_1, CHugeImage *IntBinary, CHugeImage *HueBinary, CHugeImage *SatBinary, CHugeImage *gndMask, CHugeImage *upMask, double UpedgeThresh, CHugeImage *cropImg, CXYPointArray &xypts, double dAL);
	   void extendSegment(double m, double c, CXYPoint C1, CXYPoint P1, CXYPoint C2, CXYPoint &C1_1, CXYPoint &P1_1, CHugeImage *Iy, CHugeImage *Hi, CHugeImage *Sq, CHugeImage *gndMask, CHugeImage *upMask, vector <CDoubleVector> iThresh, vector <CDoubleVector> hThresh, vector <CDoubleVector> sThresh, double UpedgeThresh, CHugeImage *cropImg, CXYPointArray &xypts, double dAL, CXYPolyLineArray &xyedges, CXYPolyLineArray &xyedgesImg, CHugeImage *entropyMask);
	   CXYPoint findMidPointAbove(CXYPoint C1, CXYPoint C2, double m, double c, CXYPoint matP);
	   CXYPoint findMirrorPoint(double m, double c, CXYPoint P1);
	   void formInitialBuildings(CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CHugeImage *entropyMask);
	   bool testInBuidlings(int index, CXYPolyLineArray &xyedges);
	   void findFinalBuildings(CHugeImage *gndMask, CHugeImage *upMask, CHugeImage *cropImg, CHugeImage *Iy, CHugeImage *Hi, CHugeImage *Sq, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CXYPolyLineArray &xyedgesImg, CHugeImage *entropyMask);
	   
	   void reduceIniBuildingArea(vector <double> &m12s, vector <double> &m23s, CXYPolyLineArray &xyedges);
	   int checkMaxOverlaps(CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4);
	   void findHistogram(CDoubleVector Hue, CDoubleVector &hThresh, CDoubleVector &Hhist);
	   vector <CDoubleVector> findThresh(CDoubleVector hThresh, CDoubleVector Hhist, long hTotal);
	   //void setBinaryMask(CImageBuffer source, CHugeImage *Binary, vector <CDoubleVector> Thresh);
	   void findExtendedRectangle(double m12, double m23, CXYPoint P1_1, CXYPoint P2_2, CXYPoint P3_2, CXYPoint P4_2, CXYPoint &P1, CXYPoint &P2, CXYPoint &P3, CXYPoint &P4);
	   bool myfn(double i, double j) { return i<j; };
	   CImageBuffer * getGroundMask() {return this->gMask;};
	   //FeatureExtractionParameters getParameters() {return this->parameters;};
	   
	   float findMeanHeight(C2DPoint obsDEM, int n);
	   void getCurveIDs(CXYContourArray &xycont, unsigned int *IDs);
	   void findBlackPix(C2DPoint *p, long &xb, long &yb, long &up, long &dn, long &lf, long &rt);
	   void updateIntersectedCurveID(long row, long col, CXYContourArray &xycont, unsigned int *curveNums, int curCurvID);
//======================Rectify	final buildings ============
	   void initParamenetrs();
	   bool rectifyFinalBuildings(CHugeImage *orientationHugImage, CHugeImage *edgeImage, CXYPolyLineArray &xyedgesImg);
	   bool validate_building(CHugeImage *orientationHugImage, CHugeImage *edgeHugImage, CXYPolyLine *build);
	   void findHistogram(CImageBuffer buf, CImageBuffer bufNum, int *H, int *Hind, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, long minX, long minY, long maxX, long maxY);
	   void evaluateHistogram(int *H, float *ret);
	   vector <unsigned short> findExtremum(float *H, float *Sm);

	   void setBufferOfContourX(CImageBuffer &oriLineX, int *contour, int size);
	   void setContourFromBufferX(CImageBuffer smoothLineX, float *cont);
	   void extendContourX(CImageBuffer oriLineX, CImageBuffer &extLineX, int W);
	   void contractContourX(CImageBuffer smoothLineX, CImageBuffer &conSmoothX, int W);
	   
//================================ Destructor =================================

	   virtual ~BuildingsDetector ();

//=============================================================================
};
#endif
