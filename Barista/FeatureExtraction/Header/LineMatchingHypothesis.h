#ifndef ___CLINEMATCHINGHYPOTHESIS___
#define ___CLINEMATCHINGHYPOTHESIS___

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <vector>
#include <ostream>
using namespace std;

#include "2DPoint.h"

class CXYZPoint;
class C3DPoint;
class CXYLineSegment;
class CXYZHomoSegment;
class CBuildingFaceNeighbourRecord;
class CSensorModel;
class C3DAdjustPlane;

//****************************************************************************
/*!
 * \brief
 * A hypothesis for a 3D line with matching image edges 
 * 
 * 
 */


class CLineMatchingHypothesis
{
  public:

//============================================================================
//=================================== Data ===================================

	  vector < vector <CXYLineSegment *> > imageEdges;
												    
	  vector < vector<int> > indexVector;
      
	  CXYZHomoSegment *pObjectEdge;
	  
	  CBuildingFaceNeighbourRecord *objectEdgeDescriptor;

	  double s0;
//============================================================================

  public:
   
	  /// Constructor: 
	  CLineMatchingHypothesis(int nImages);
	  CLineMatchingHypothesis(const CLineMatchingHypothesis &other);

	  /// Destructor 
	  virtual ~CLineMatchingHypothesis();

//============================================================================

	  /// Reset vectors
	  void reset();


//============================================================================
//================================ Query data ================================

	  bool intersectPlanes(const vector <C3DAdjustPlane *> &planes, 
						   CXYZHomoSegment *segment,
						   C3DPoint &reduction);

	  bool computeIntersection(const vector<CSensorModel *> &sensorModels,
		                       const vector<double> &imagePixelSize,
		                       const vector<C2DPoint> &imageOffsets,
		                       C3DAdjustPlane *plane);
   
	  C3DAdjustPlane *getPlane(CSensorModel *model, double pixSize, 
		                       const C2DPoint &imageOffset, double z, 
							   CXYLineSegment *segment2D, 
							   CXYZPoint &P1, CXYZPoint &P2,
							   C3DPoint &reduction);

	  bool overlaps(CLineMatchingHypothesis &other, double minPercentage) const;

	  bool isIdentical(CLineMatchingHypothesis &other, double minOverlapPercentage) const;

	  void merge(CLineMatchingHypothesis &other); 

//============================================================================

	  /// Formatted output to ostream 
	  void print (ostream &anOstream, vector<ofstream *> *pDxfVec,
		          unsigned int index) const;

	  /// DXF output to file
	  void exportDXF(vector<ofstream*> &imgDxfVec, ofstream *dxfFile3D) const;

//============================================================================

  protected: 

//============================================================================


//============================================================================
   
};
 
#endif
