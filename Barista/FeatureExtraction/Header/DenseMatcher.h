#ifndef DENSEMATCHER_H
#define DENSEMATCHER_H
//============================================================================

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      IPI, University of Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//============================================================================

/*!
 * \brief
 * A wrapper around the OpenCV implementation of SGM
 */

#include <vector>

#include "ProgressHandler.h"
#include "3DPoint.h"
#include "FrameCameraModel.h"

class CFileName;
class CImageBuffer;
class CHugeImage;
class CGenerateEpipolar; 
class CGenericPointCloud;

class CSGMParameters
{
  public:

	  int minDisparity;
	  int numberOfDisparities;
	  int SADWindowSize;
	  int preFilterCap;
	  int uniquenessRatio;
	  int P1, P2;
	  int speckleWindowSize;
	  int speckleRange;
	  int disp12MaxDiff;
	  bool fullDP;

	  CSGMParameters();
	  CSGMParameters(const CSGMParameters &pars);

	  ~CSGMParameters() {};

	  CCharString getDescriptor() const; 

	  void saveToFile(const CCharString &fn) const;

	  bool initFromFile(const CCharString &fn);
};

class CDenseMatcher : public CProgressHandler
{
  protected:

	  CHugeImage *pImageFile1;
	  CHugeImage *pImageFile2;

	  CFrameCameraModel *pModel1;
	  CFrameCameraModel *pModel2;

	  CFrameCameraModel frameCamEpi1;
	  CFrameCameraModel frameCamEpi2;

	  CImageBuffer *epiBuf1;
	  CImageBuffer *epiBuf2;

	  CImageBuffer *parallaxBuf;


	  C3DPoint Pmin;
	  C3DPoint Pmax;

	  C3DPoint Sigma;

	  int MaxParallax;

	  CSGMParameters SGBMpars;

	  CGenerateEpipolar *epiPolarGenerator; 

	  float alpha1, alpha2; 
	  unsigned short offset1, offset2;
	  
	  unsigned int pixelOutliers;

	  float parallaxThreshold;


  public:

	  CDenseMatcher(CHugeImage *Image1, CHugeImage *Image2, 
		            CFrameCameraModel *model1, CFrameCameraModel *model2, 
		            const C3DPoint &pmin, const C3DPoint &pmax, 
		            const CSGMParameters &pars, 
					const C3DPoint &sigma, int sizeOutlierArea);
	
	  ~CDenseMatcher();
	
	  bool isInitialised() const;

	  bool match();

	  bool getPointCloud(CGenericPointCloud **points, vector<unsigned char> &pointCount, 
		                 vector<unsigned char *> &pointColour);

	  void exportEpis(const CCharString &fnBase) const;

	  void exportParallax(const CCharString &fnBase) const;

  protected:
	  
	  void clearFrameCam(CFrameCameraModel &cam);

	  void clearBuffers();

	  void postProcessParallaxes();

	  void initAlphaOffset();
};

#endif