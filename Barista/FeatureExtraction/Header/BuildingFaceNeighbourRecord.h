#ifndef ___CBUILDINGFACENEIGHBOURRECORD___
#define ___CBUILDINGFACENEIGHBOURRECORD___

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */
#include <vector>
#include <ostream>
using namespace std;
#include "CharString.h"
#include "2DPoint.h"
#include "3DPoint.h"
#include "XYZPoint.h"
#include "XYLine.h"

//****************************************************************************
//*************************** Forward Declarations ***************************

class CXYContour;
class CXYPolyLine;
class CXYZPolyLine;
class CXYZHomoSegment;
class C3DAdjustPlane;
class CTFW;
class CBuildingFacePoly;
class CLineMatchingHypothesis;
class CBuildingFaceNeighbourVector;

//****************************************************************************

/// the type of the neighbourhood relation
enum eNEIGHBORTYPE              
{
	eUNDEFINEDNEIGHBOUR = -1,
	eNONEIGHBOUR = 0,
	eCOPLANAR,
    eINTERSECT,
    eSTEPEDGE 
};

//****************************************************************************
/*!
 * \brief
 * CBuildingFaceNeighbourRecord describes a pair of neighbouring roof planes in 
 * building reconstruction, along with their boundary polygon. 
 * An instance of class CBuildingFaceNeighbourRecord can be classified either as a
 * step edge or as an intersection (or, in intermediate stages of building 
 * reconstruction, as co-planar).
 * 
   A CBuildingFaceNeighbourRecord describes a portion of the boundary
   polygon of a roof plane separating exactly two planes.
   Literature:  Franz Rottensteiner, John Trinder, Simon Clode, and Kurt Kubik:
                Automated Delineation of Roof Planes from LIDAR Data. 
                In: International Archives of the Photogrammetry, Remote 
                Sensing and Spatial Information Sciences Vol. XXXVI - 3/W19, 
                pp. 221-226, Enschede, The Netherlands, September 12-14, 2005.  

 * 
 * \see
 * CSegmentNeighbourhood | CSegmentNeighbourhoodVector | CBuildingFacePoly | CLabelImage 
 */

class CBuildingFaceNeighbourRecord
{
  protected:

//============================================================================
//=================================== Data ===================================

   
	  // Index of the first planar region
	  unsigned short region1;
   
	  /// Index of the second planar region; region1 < region2
	  unsigned short region2;                        
   
	  /// Type of the neighbourhood: Step edge or intersection or co-planar
	  eNEIGHBORTYPE eNeighbourType;

	  /// The face polygon this neighbour record belongs to in region1
	  CBuildingFacePoly *facePoly1;

	  /// The face polygon this neighbour record belongs to in region1
	  CBuildingFacePoly *facePoly2;

	  /// The initial polygon as extracted from the voronoi diagram
	  CXYContour *pInitialPoly;

	  /// The approximation of pInitialPoly
	  CXYPolyLine *pApproximatePoly;

	  /// The final polygon segment separating region1 and region2
	  CXYZPolyLine *pFinalPoly;

	  /// Straight line segment being the intersection between the planar segments region1 and region2.
	  CXYZHomoSegment *pIntersection;      
                          
	  /// A quality indicator for the co-planarity of the two planes
	  float rmsError;
                                           

//============================================================================

  public:
   

//============================================================================

	  /// Default constructor: all polygons and pointers are initialised by 0.
	  CBuildingFaceNeighbourRecord();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// Initialise *this as the boundary between roof planes Region1 and Region2
	  //* this->eNeighbourType will be set to eUNDEFINEDNEIGHBOUR
	  //* All the polygons and pointers will be initialised by 0. 
	  CBuildingFaceNeighbourRecord(unsigned short Region1, unsigned short Region2);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	  /// Initialise *this as the boundary between roof planes Region1 and Region2
	  //* this->eNeighbourType will be set to eType.
	  //* All the polygons and pointers will be initialised by 0.
	  CBuildingFaceNeighbourRecord(unsigned short Region1, unsigned short Region2, 
		                           eNEIGHBORTYPE eType);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// Copy constructor: All data members are copied from other, except
	  //* pInitialPoly, pApproximatePoly, pFinalPoly, and pIntersection.
	  //* For these members, new instances will be created and the data will be
	  //* copied from other.pInitialPoly, other.pApproximatePoly, 
	  //* other.pFinalPoly, and other.pIntersection.
	  CBuildingFaceNeighbourRecord(const CBuildingFaceNeighbourRecord &other);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


//============================================================================

	  /// Destructor 
	  virtual ~CBuildingFaceNeighbourRecord();


//============================================================================
//================================ Query data ================================

   
	  unsigned short getRegion1() const { return this->region1; };

	  unsigned short getRegion2() const { return this->region2; };

	  bool hasRegion(unsigned short region);

	  eNEIGHBORTYPE getNeighbourType() const { return this->eNeighbourType; };                

   
	  CXYZHomoSegment *getIntersection() const { return this->pIntersection; };
  
	  CBuildingFacePoly *getFacePoly1() const { return this->facePoly1; };

	  CBuildingFacePoly *getFacePoly2() const { return this->facePoly2; };

	  CXYContour *getInitialPoly () const { return this->pInitialPoly; };

	  CXYPolyLine *getApproximatePoly () const { return this->pApproximatePoly; };

	  CXYZPolyLine *getFinalPoly () const { return this->pFinalPoly; };

	  float getRmsError() const { return this->rmsError; };

   
	  float getOverlapPercentage(const vector <CLineMatchingHypothesis *> &matches) const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - -  Query points/length of "initial polygon" - - - - - - - - -

	  C2DPoint getInitialStartPt() const;

	  C2DPoint getInitialEndPt() const;

	  C2DPoint getInitialPt(unsigned int index) const;

	   /// returns 0 if pInitialPoly has not been initialised yet.
	  double getInitialLength() const;
   
	   eNEIGHBORTYPE classify(CBuildingFaceNeighbourVector *neighbourVector, 
							  float minLenghtRatio, float If_minLength, float delta,
							  float maxDist, float sigmaContour);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - Query points/length/number of points of "final polygon" - - - - -

	  CXYZPoint getFinalStartPt() const;

	  CXYZPoint getFinalEndPt()   const;

	  CXYZPoint getFinalPt(unsigned int index) const;

	  double getFinalLength() const;

	  unsigned int getFinalNumber() const;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// Get formatted string for output to protocol 
	  CCharString getPrintString() const;


//============================================================================

	  /// Assignment operator: All data members are copied from other, except
	  //* pInitialPoly, pApproximatePoly, pFinalPoly, and pIntersection.
	  //* For these members, new instances will be created and the data will be
	  //* copied from other.pInitialPoly, other.pApproximatePoly, 
	  //* other.pFinalPoly, and other.pIntersection.
	  CBuildingFaceNeighbourRecord &operator = 
	                             (const CBuildingFaceNeighbourRecord& other);


//============================================================================

	  /// Comparison operator. Two instances of class 
	  //* CBuildingFaceNeighbourRecord are considered to be identical if 
	  //* region1, region2, eNeighbourType, getFinalEndPt(), and getFinalStartPt() 
	  //* are identical. If any of these data is different, the two instances 
	  //* are considered to be different, too.
	  bool operator == (const CBuildingFaceNeighbourRecord& other);

	  /// Comparison operator. Two instances of class 
	  //* CBuildingFaceNeighbourRecord are considered to be identical if 
	  //* region1, region2, eNeighbourType, getFinalEndPt(), and getFinalStartPt() 
	  //* are identical. If any of these data is different, the two instances 
	  //* are considered to be different, too.
	  bool operator != (const CBuildingFaceNeighbourRecord& other);


//============================================================================

	  /// Set rms error
	  void setRms(float rms) { this->rmsError = rms; };

	  /// if (eType == eINTERSECT) and pIntersection has been initialised, 
	  // pFinalPoly will be initialised as intersection from pIntersection
	  void setType(eNEIGHBORTYPE eType);

	  /// replace regionOld (i.e., either region1 or region2) by regionNew
	  void replaceRegion(unsigned short regionOld, unsigned short regionNew);

	  void setFacePolygon(CBuildingFacePoly *facePolygon);

//============================================================================
//================================ Reset data ================================

	  /// delete pInitialPoly, pApproximatePoly, and pFinalPoly, and set the pointers to 0.
	  void resetPolys();

	  /// delete pIntersection and set pIntersection = 0
	  void resetIntersection();



//============================================================================
//=============  Initialise / manipulate polygons / intersection =============

	  /// Initialise pInitialPoly and pApproximatePoly.
	  //* pContour: Initial polygon derived from the Voronoi diagram. It will be
	  //* transformed to object space as pInitialPoly, and pApproximatePoly
	  //* will be initialised by it. 
	  // sigmaContour: Planimetric standard deviations of pContour.
	  // trafo: Transformation parameters to the world coordinate system.
	  // reduction: Co-ordinate reduction value.
	  // maxDist: Maximum distance for thinning out pInitialPoly   
	  void initPolys(CXYContour *pContour, float sigmaContour, CTFW &trafo, 
		             const C3DPoint &reduction, float maxDist);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


	  /// remove the points with indices i so that indexFrom <= i <= indexTo 
	  //* from the initial polygon pInitialPoly.   
	  void removeFromInitial(unsigned int indexFrom, unsigned int indexTo);
	  
	  /// remove all points except those with indices i so that indexFrom <= i <= indexTo 
	  //* from the initial polygon pInitialPoly and all the others.   
	  void cutOut(unsigned int indexFrom, unsigned int indexTo, float maxDist, float sigmaContour);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	  /// Initialise pIntersection as the intersection of the two planes 
	  //* plane1 and plane3, cut off at the projections of getInitialStartPt() 
	  //* and getInitialEndPt() to the intersection line
	  eIntersect initIntersection();

	  /// Cut off pIntersection at the projections of the end points of 
	  //* intersection to the intersection line
	  void cutOffIntersection(const CXYZHomoSegment &intersection);

	  /// Cut off pIntersection at the projections of getInitialStartPt() and    
	  // getInitialEndPt() to the intersection line
	  void cutOffIntersection();

//============================================================================

	  /// Compute distance between two instances of class CBuildingFaceNeighbourRecord 
	  //* Returns the average distance between the straight line segment corresponding
	  //* to the "final polygon" of *this and the end points of the "final polygon"
	  //* of other.
	  //* If *this is an intersection, the distance will be computed in 3D, if it is
	  //* a step edge, it will be computed in 2D (X,Y) only. If the type of *this 
	  //* has not been determined, the return value will be -1.
   	  double getDistance(CBuildingFaceNeighbourRecord &other);


//============================================================================

	  /// Formatted output to ostream 
	  void print (ostream &anOstream) const;


	  /// DXF output to file
	  void exportDXF(ofstream &dxfFile, bool exportInitial, 
		                                bool exportApprox, 
										bool exportFinal) const;

//============================================================================
 
  protected: 
//============================================================================
 
	  void mergeSegmentCounters(int minPts, vector<int> &counter);
	  void initApproxFinal(float maxDist, float sigmaContour);


//============================================================================

	  /// Check whether region1 < region2 and exchange them if necessary 
	  void checkOrder();

};

#endif
