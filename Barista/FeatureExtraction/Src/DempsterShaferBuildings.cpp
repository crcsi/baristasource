#ifdef WIN32
#include "DempsterShaferBuildings.h"
#include "DSMRoughness.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"
#include "DSMBuilding.h"
#include "LabelImage.h"

 const int CDempsterShaferBuildingClassifier::UseRoughIso  = 1;

 const int CDempsterShaferBuildingClassifier::UseRoughStr  = 2;

 const int CDempsterShaferBuildingClassifier::UseDz        = 4;

//=============================================================================

CDempsterShaferBuildingClassifier::CDempsterShaferBuildingClassifier (
	                                    CImageBuffer  *PNormDSM, 
		                                CDSMRoughness *PRoughness,
										CImageBuffer  *PDSMFirstLast,
										CImageBuffer  *PVegIndex, 
										CImageBuffer  *PVegIndexSigma,
										CImageBuffer  *PExisting,
										float dSqQuantil,
										float SigmaMultiplicator,
										int   SensorOption, 
										bool  useThetaForFirstLast,
										bool  useEmpirical,
										float ProbExisting,
										float ProbBuildExist):
         CImageBuffer(PNormDSM->getOffsetX(), PNormDSM->getOffsetY(), 
			          PNormDSM->getWidth(),  PNormDSM->getHeight(), 1, 8, true, 0), 
         pNormDSM(PNormDSM), pRoughness(PRoughness), pFirstLast(PDSMFirstLast), 
         pVegIndex(PVegIndex), pVegIndexSigma(PVegIndexSigma),
		 pExistingBuildingMask(PExisting),
         sensorOption(SensorOption), firstLastComplementaryClass(0), 
		 sigmaMultiplicator(SigmaMultiplicator),probExisting(ProbExisting),
		 probBuildExist(ProbBuildExist)
{ 
	this->sigmaMax = 25.0;

	significantRoughQuantil = pRoughness->getQuantil();

	if (dSqQuantil > 0) significantRoughQuantil = dSqQuantil;

	initDempsterShaferPixel();
  
  	if (this->useFirstLastPulse()) 
	{
		if (dempsterShaferPix.getNClasses() == 4)
		{
			if (useThetaForFirstLast) firstLastComplementaryClass = 14; // B or T or G or S
			else firstLastComplementaryClass = 12;              // B or G or S  
		}
		else
		{
			if (useThetaForFirstLast) firstLastComplementaryClass = 6; // B or T or O
			else firstLastComplementaryClass = 4;              // B or O
		};
	};

	initDempsterShaferRegions();

	if (useEmpirical)
	{
		initPixelProbabilitiesFromFiles();
		initRegionProbabilitiesFromFiles();
	};
};

//=============================================================================

CDempsterShaferBuildingClassifier::~CDempsterShaferBuildingClassifier()
{
	pNormDSM              = 0;
	pRoughness            = 0;
	pFirstLast            = 0;
	pVegIndex             = 0;
	pVegIndexSigma        = 0;
	pExistingBuildingMask = 0;
};
 
//=============================================================================
 
void CDempsterShaferBuildingClassifier::initDempsterShaferPixel()
{

	int nSensors = 0;
	int nClasses = useVegIndex() ? 4 : 3;	

	if (useDz()) 
	{
		deltaHRecordPix.setIndex(nSensors);    ++nSensors;
	};

	if (useRoughStr()) 
	{
		roughnessStrRecordPix.setIndex(nSensors); ++nSensors;
	}

	if (useRoughIso())
	{
		roughnessIsoRecordPix.setIndex(nSensors); ++nSensors;
	};


	if (useFirstLastPulse()) 
	{
		firstLastRecordPix.setIndex(nSensors); ++nSensors;
	};

	if (useVegIndex())
	{
		NDVIRecordPix.setIndex(nSensors); ++nSensors;
	};

	if (useExistingData())
	{
		ExistingIndexPix = nSensors;
		++nSensors;
	}

	dempsterShaferPix.init(nClasses, nSensors);
     

	dempsterShaferPix.setClassName(0,"B");
	dempsterShaferPix.setClassName(1,"T");
	if (nClasses < 4) dempsterShaferPix.setClassName(2,"S");
	else
	{
		dempsterShaferPix.setClassName(2,"V");
		dempsterShaferPix.setClassName(3,"S");
	};

	if (useDz())        dempsterShaferPix.setSensorName(deltaHRecordPix.getIndex(),    "DH");
	if (useRoughStr())  dempsterShaferPix.setSensorName(roughnessStrRecordPix.getIndex(), "W" );
	if (useRoughIso())  dempsterShaferPix.setSensorName(roughnessIsoRecordPix.getIndex(), "Q"   );
	if (firstLastRecordPix.getIndex()    >= 0) 
		dempsterShaferPix.setSensorName(firstLastRecordPix.getIndex(),   "FL"  );
	if (NDVIRecordPix.getIndex()  >= 0) 
		dempsterShaferPix.setSensorName(NDVIRecordPix.getIndex(), "NDVI");

	if (useExistingData()) dempsterShaferPix.setSensorName(ExistingIndexPix, "OldB");
	
	deltaHRecordPix.setProbMinMax(0.05f, 0.95f);
	roughnessStrRecordPix.setProbMinMax(0.05f, 0.95f);
	roughnessIsoRecordPix.setProbMinMax(0.05f, 0.95f);
	firstLastRecordPix.setProbMinMax(0.05f, 0.95f);
	NDVIRecordPix.setProbMinMax(0.05f, 0.95f);

	deltaHRecordPix.setMinMax(1.5f, 3.0f);
	roughnessStrRecordPix.setMinMax(1.0f - 2.0f * (1.0f - pRoughness->getQuantil()), 1.0f);
 	roughnessIsoRecordPix.setMinMax(0.5f, 0.9f);
	firstLastRecordPix.setMinMax(1.5f, 3.0f);
	NDVIRecordPix.setMinMax(-20.0f, 15.0f);
};

//=============================================================================
 
void  CDempsterShaferBuildingClassifier::initDempsterShaferRegions()
{
	int nSensors = 3;
	deltaHRecordReg.setIndex(0);
	homogRecordReg.setIndex(1);
	pointRecordReg.setIndex(2);
	int nClasses = useVegIndex() ? 4 : 3;

	if (useVegIndex())   
	{
		NDVIRecordReg.setIndex(3);
		NDVIRecordReg1.setIndex(3);
		++nSensors;
	};

	dempsterShaferReg.init(nClasses, nSensors);

	dempsterShaferReg.setClassName(0, "B");
	dempsterShaferReg.setClassName(1, "T");
	dempsterShaferReg.setClassName(2, "V");
	dempsterShaferReg.setSensorName(deltaHRecordReg.getIndex(),    "DH");
	dempsterShaferReg.setSensorName(homogRecordReg.getIndex(), "HOM");
	dempsterShaferReg.setSensorName(pointRecordReg.getIndex(),   "PT");
  
	if (useVegIndex()) 
	{
		dempsterShaferReg.setClassName(3, "S");
		dempsterShaferReg.setSensorName(NDVIRecordReg.getIndex(), "NDVI");
	};


	deltaHRecordReg.setProbMinMax(0.01f, 0.99f);     
	homogRecordReg.setProbMinMax(0.05f, 0.95f);
	pointRecordReg.setProbMinMax(0.05f, 0.95f);
	NDVIRecordReg.setProbMinMax(0.05f, 0.95f);
	NDVIRecordReg1.setProbMinMax(0.05f, 0.95f);

	deltaHRecordReg.setMinMax(2.0f, 3.0f);     
	homogRecordReg.setMinMax(0.0f, 0.6f);
	pointRecordReg.setMinMax(0.3f, 0.75f); 
	NDVIRecordReg.setMinMax(-20.0f, 15.0f);
	NDVIRecordReg1.setMinMax(-30.0, -20.0f);
};
	  
CLookupTable CDempsterShaferBuildingClassifier::getClassLookup(int nClasses)
{
	CLookupTable lut(8,3);

	lut.setColor(255,   0,   0, 0);
	lut.setColor(  0, 128,  64, 1);
	int index = 2;
	if (nClasses > 4)
	{
		lut.setColor(  0, 128, 128, index);
		++index;
	}

	lut.setColor(  0, 255, 128, index);
	++index;
	lut.setColor(128,   0,   0, index);
	++index;
	lut.setColor(  0,   0, 255, index);
	return lut;
};

//=============================================================================
     
bool CDempsterShaferBuildingClassifier::isRoughnessSignificant(float roughness) const
{
	return (roughness > significantRoughQuantil);
};

//=============================================================================
 
float CDempsterShaferBuildingClassifier::vegIndex(unsigned long index) const
{
	float retVal = pVegIndex ? pVegIndex->pixFlt(index): -101.0f;
	return retVal;
};

//=============================================================================
 
float CDempsterShaferBuildingClassifier::sigmaVegIndex(unsigned long index) const
{
	float retVal = pVegIndexSigma ? pVegIndexSigma->pixFlt(index) : -1.0f;
	return retVal;
};
    
//=============================================================================

CCharString CDempsterShaferBuildingClassifier::getArgument(CCharString str, 
														   const CCharString &keyword)
{
	int pos = str.Find(keyword);
	if (pos < 0) return "";
	str = str.Right(str.GetLength() - pos);
	pos = str.Find("(");
	if (pos < 0) return "";
	str = str.Right(str.GetLength() - pos - 1);
	pos = str.Find(")");
	if (pos  < 0) return "";
  
	return str.Left(pos);
}

//=============================================================================

void CDempsterShaferBuildingClassifier::initParametersFromFile(const CCharString &fileName)
{
	ifstream inFile(fileName.GetChar());
	
	int len = 1024;
	char *z = new char[len];

	if (inFile.good())
	{
		inFile.getline(z, len);
		CCharString str = z;
		str.MakeUpper();

		if (str.Find("DEMPSTER_SHAFER") >= 0)
		{
			inFile.getline(z, len);
			while (inFile.good())
			{
				str = z;
				str.MakeUpper();
				CCharString arg = getArgument(str, "DZ_PIXEL");
				if (!arg.IsEmpty()) deltaHRecordPix.initParametersFromString(arg); 
				else
				{
					arg = getArgument(str, "HOMOGENEITY_PIXEL");
					if (!arg.IsEmpty()) 
					{
						roughnessStrRecordPix.initParametersFromString(arg);
					}
					else
					{
						arg = getArgument(str,"ISOTROPY_PIXEL");
						if (!arg.IsEmpty()) roughnessIsoRecordPix.initParametersFromString(arg);
						else
						{
							arg = getArgument(str, "FIRSTLAST_PIXEL");
							if (!arg.IsEmpty()) firstLastRecordPix.initParametersFromString(arg);
							else
							{
								arg = getArgument(str, "NDVI_PIXEL");
								if (!arg.IsEmpty()) NDVIRecordPix.initParametersFromString(arg);
								else
								{
									arg = getArgument(str, "DZ_REGION");
									if (!arg.IsEmpty()) deltaHRecordReg.initParametersFromString(arg);
									else
									{
										arg = getArgument(str, "HOMOGENEOUS_REGION");
										if (!arg.IsEmpty()) homogRecordReg.initParametersFromString(arg);
										else
										{
											arg = getArgument(str, "POINT_REGION");
											if (!arg.IsEmpty()) pointRecordReg.initParametersFromString(arg);
											else
											{
												arg = getArgument(str, "NDVI_REGION");
												if (!arg.IsEmpty()) 
												{
													NDVIRecordReg.initParametersFromString(arg);
													NDVIRecordReg1.setProbMinMax(0.05f, 0.95f);
													float dminMax = (NDVIRecordReg.getXmax() - NDVIRecordReg.getXmin()) * 0.5;
													NDVIRecordReg1.setMinMax(NDVIRecordReg.getXmin() - dminMax, NDVIRecordReg.getXmin());
												}
												else
												{
												}
											};
										};
									}; 
								};            
							}; 
						};			
					};
				};
				inFile.getline(z, len);
			};
		};
	};

 
	delete [] z;
};
    
//=============================================================================

void CDempsterShaferBuildingClassifier::initPixelProbabilitiesFromFiles()
{
  deltaHRecordPix.initEmpiricalModel      ("DZ_PX.PRB");
  roughnessStrRecordPix.initEmpiricalModel("HOMOG_PX.PRB");
  roughnessIsoRecordPix.initEmpiricalModel("ISO_PX.PRB");
  firstLastRecordPix.initEmpiricalModel   ("FL_PX.PRB");
  NDVIRecordPix.initEmpiricalModel        ("NDVI_PX.PRB");
 };
    
//=============================================================================

void CDempsterShaferBuildingClassifier::initRegionProbabilitiesFromFiles()
{
	deltaHRecordReg.initEmpiricalModel("DZ_REG.PRB");
	homogRecordReg.initEmpiricalModel ("HOMOG_REG.PRB");
	pointRecordReg.initEmpiricalModel ("PNT_REG.PRB");
	NDVIRecordReg.initEmpiricalModel  ("NDVI_REG.PRB");
};

//=============================================================================

void CDempsterShaferBuildingClassifier::probDZpix(float dZ)
{
	float mDZ = deltaHRecordPix.getProb(dZ);

	if (dempsterShaferPix.getNClasses() == 4)
	{
		dempsterShaferPix.assignProbabilityMass(4, deltaHRecordPix.getIndex(), mDZ);        // B or T
		dempsterShaferPix.assignProbabilityMass(9, deltaHRecordPix.getIndex(), 1.0f - mDZ); // G or S
	}
	else
	{
		dempsterShaferPix.assignProbabilityMass(3, deltaHRecordPix.getIndex(), mDZ);        // B or T
		dempsterShaferPix.assignProbabilityMass(2, deltaHRecordPix.getIndex(), 1.0f - mDZ); // O
	};
};
  
//=============================================================================

void CDempsterShaferBuildingClassifier::probRoughStrPix(float strength)
{
	float mR = roughnessStrRecordPix.getProb(strength);
  
	dempsterShaferPix.assignProbabilityMass( 1, roughnessStrRecordPix.getIndex(), mR);            // T

	if (dempsterShaferPix.getNClasses() == 4)	
		dempsterShaferPix.assignProbabilityMass(12, roughnessStrRecordPix.getIndex(), 1.0f - mR); // B or G or S
	else 
		dempsterShaferPix.assignProbabilityMass(4, roughnessStrRecordPix.getIndex(), 1.0f - mR);  // B or O
};
    
//=============================================================================

void CDempsterShaferBuildingClassifier::probRoughDirPix(float dir, float strength)
{
/*
	 if (isRoughnessSignificant(strength)) 
	 {
		 float mD = roughnessIsoRecordPix.getProb(dir);

		 dempsterShaferPix.assignProbabilityMass( 1, roughnessIsoRecordPix.getIndex(), mD);            // T
		 if (dempsterShaferPix.getNClasses() == 4)
		 {
			 dempsterShaferPix.assignProbabilityMass(12, roughnessIsoRecordPix.getIndex(), 1.0f - mD); // B or G or S
			 dempsterShaferPix.assignProbabilityMass(14, roughnessIsoRecordPix.getIndex(), 0.0f);      // B or T or G or S
		 } 
		 else
		 {
			 dempsterShaferPix.assignProbabilityMass(4, roughnessIsoRecordPix.getIndex(), 1.0f - mD);  // B or O
			 dempsterShaferPix.assignProbabilityMass(6, roughnessIsoRecordPix.getIndex(), 0.0f);       // B or T or O
		 }
	 }
	 else 
	 {
		 dempsterShaferPix.assignProbabilityMass( 1, roughnessIsoRecordPix.getIndex(), 0.0f);       // T
		 if (dempsterShaferPix.getNClasses() == 4)
		 {
			 dempsterShaferPix.assignProbabilityMass(12, roughnessIsoRecordPix.getIndex(), 0.0f);   // B or G or S
			 dempsterShaferPix.assignProbabilityMass(14, roughnessIsoRecordPix.getIndex(), 1.0f);   // B or T or G or S
		 }
		 else
		 {
			 dempsterShaferPix.assignProbabilityMass(4, roughnessIsoRecordPix.getIndex(), 0.0f);    // B or O
			 dempsterShaferPix.assignProbabilityMass(6, roughnessIsoRecordPix.getIndex(), 1.0f);    // B or T or O
		 };
	 }; 
	 */

	 float mD = roughnessIsoRecordPix.getProb(dir);

	 dempsterShaferPix.assignProbabilityMass( 1, roughnessIsoRecordPix.getIndex(), strength * mD);            // T
	 if (dempsterShaferPix.getNClasses() == 4)
	 {
		 dempsterShaferPix.assignProbabilityMass(12, roughnessIsoRecordPix.getIndex(), strength * (1.0f - mD)); // B or G or S
		 dempsterShaferPix.assignProbabilityMass(14, roughnessIsoRecordPix.getIndex(), 1.0 - strength);      // B or T or G or S
	 } 
	 else
	 {
		 dempsterShaferPix.assignProbabilityMass(4, roughnessIsoRecordPix.getIndex(), strength * (1.0f - mD));  // B or O
		 dempsterShaferPix.assignProbabilityMass(6, roughnessIsoRecordPix.getIndex(), 1.0 - strength);       // B or T or O
	 }	 
};

//=============================================================================

void CDempsterShaferBuildingClassifier::probFLpix(float dFL)
{
	float mFL = firstLastRecordPix.getProb(dFL);

	dempsterShaferPix.assignProbabilityMass( 1, firstLastRecordPix.getIndex(), mFL); // T
	dempsterShaferPix.assignProbabilityMass(firstLastComplementaryClass, firstLastRecordPix.getIndex(), 1.0f - mFL); // see constructor
};
      
//=============================================================================

void CDempsterShaferBuildingClassifier::probNpix(float NDVI, float sigma)
{
	if ((NDVI >= -100.0f) && (sigma <= sigmaMax))
	{
		sigma *= (float) 0.01;
		float mN0  = NDVIRecordPix.getProb(NDVI);

		if (sigma >= 0.0f)
		{
			sigma *= sigmaMultiplicator;
			dempsterShaferPix.assignProbabilityMass( 6, NDVIRecordPix.getIndex(), (1.0f - sigma) * (1.0f -  mN0));  // B or S
			dempsterShaferPix.assignProbabilityMass( 7, NDVIRecordPix.getIndex(), (1.0f - sigma) * mN0);            // T or G
			dempsterShaferPix.assignProbabilityMass(14, NDVIRecordPix.getIndex(), sigma);                           // B or T or G or S
		}
		else
		{
			dempsterShaferPix.assignProbabilityMass( 6, NDVIRecordPix.getIndex(), 1.0f -  mN0);  // B or S
			dempsterShaferPix.assignProbabilityMass( 7, NDVIRecordPix.getIndex(), mN0);          // T or G
			dempsterShaferPix.assignProbabilityMass(14, NDVIRecordPix.getIndex(), 0.0f);
		}
	}
	else
	{
		dempsterShaferPix.assignProbabilityMass( 6, NDVIRecordPix.getIndex(), 0.0f);
		dempsterShaferPix.assignProbabilityMass( 7, NDVIRecordPix.getIndex(), 0.0f);
		dempsterShaferPix.assignProbabilityMass(14, NDVIRecordPix.getIndex(), 1.0f);
	};
};
  
//=============================================================================

void CDempsterShaferBuildingClassifier::probExistingData(unsigned char existingValue)
{
	int BIdx = 0, notBIdx, thetaIdx;
	float BProb, notBProb, ThetaProb;

	if (dempsterShaferPix.getNClasses() == 4)	
	{
		notBIdx =  13;
		thetaIdx = 14;
	}
	else
	{
		notBIdx =  5;
		thetaIdx = 6;
	}

	if (existingValue < 1)
	{	
		BProb     = (1.0 - probExisting); // * probBuildExist;
		notBProb  = 1.0 - BProb;
		ThetaProb = 0.0;
	}
	else if (existingValue == 1)
	{
		BProb     = probExisting; // * probBuildExist;
		notBProb  = 1.0 - BProb; // =(1.0 - probExisting) * (1.0 - probBuildExist);
		ThetaProb = 0.0;
	}
	else
	{
		BProb     = 0.0;
		notBProb  = 0.0;
		ThetaProb = 1.0;
	}

	

	dempsterShaferPix.assignProbabilityMass(    BIdx, ExistingIndexPix, BProb);
	dempsterShaferPix.assignProbabilityMass( notBIdx, ExistingIndexPix, notBProb);
	dempsterShaferPix.assignProbabilityMass(thetaIdx, ExistingIndexPix, ThetaProb);
};

//=============================================================================

void CDempsterShaferBuildingClassifier::printDSpix(float dZ, float w,  
												   float q,  float dFL, 
												   float NDVI) const
{
	ostrstream oss;
	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(3);
	oss << "\n=========================================="
        << "\n"
        << "\nEvaluation of Dempster-Shafer: Input Data:"
        << "\n=========================================="
        << "\ndZ: " << dZ;

	if (useFirstLastPulse()) oss<< ", dZ_fl: " << dFL;
	if (useVegIndex())       oss<< ", NDVI: "  << NDVI;

	oss << ", W: " << w << ", Q: "<< q << "\n\n"
		<< dempsterShaferPix.getPrintString() << "\n\n" << ends;
  
	protHandler.print(oss, protHandler.prt + protHandler.scr);
};
    
//=============================================================================
 
int CDempsterShaferBuildingClassifier::changeUncertain(
                      const CImageBuffer &dempsterShaferConflict,
                      const CImageBuffer &dempsterShaferFirst,
                      const CImageBuffer &dempsterShaferSecond,
                      vector<int> &changedClasses,
                      unsigned long iterationIndex,
					  int neighbourhood)
{
	int fiftyPercent = 127;

	int nChanged = 0;
	neighbourhood /= 2;

	long d1 = -neighbourhood * this->diffX
              -neighbourhood * this->diffY;

	long d2 =  neighbourhood * this->diffX 
              -neighbourhood * this->diffY;
  
	long d3 =  neighbourhood * this->diffX 
		      +neighbourhood * this->diffY;
   
	unsigned long firstIdx = neighbourhood * (this->diffY + 1);
	unsigned long lastInRow = (neighbourhood + 1) * this->diffY - neighbourhood;
	unsigned long maxIdx = this->getNGV() - neighbourhood * this->diffY;

	for (;lastInRow < maxIdx; firstIdx  += this->diffY, lastInRow += this->diffY)
	{
		for (unsigned long idx = firstIdx; idx < lastInRow; ++idx)
		{
			vector<int> classVec(dempsterShaferPix.getNClasses(),0);

			unsigned long uMax = idx + d3;
			for (unsigned long uMin = idx + d1, maxRow = idx + d2; maxRow <= uMax; 
				 uMin += this->diffY, maxRow += this->diffY)
			{
				for (unsigned long curr = uMin; curr <= maxRow; curr++)
				{
					int currClass = dempsterShaferFirst.pixUChar(curr);
					if (currClass < (int) dempsterShaferPix.getNClasses()) 
						++classVec[currClass];
				};
			};

			int maxClass = 0;
			int iMax = classVec[0];

			for (unsigned long i = 1; i < dempsterShaferPix.getNClasses(); ++i)
			{
				if (classVec[i] > iMax)
				{
					maxClass = i;
					iMax = classVec[i];
				};
			};
  
			if (dempsterShaferConflict.pixUChar(idx) > fiftyPercent) //50%
			{
				int currClass = this->pixUChar(idx);
				if ((maxClass == dempsterShaferSecond.pixUChar(idx)) && 
					(currClass < (int) dempsterShaferPix.getNClasses()))
				{
					if (currClass != maxClass)
					{
						unsigned long clsIdx = iterationIndex + (dempsterShaferPix.getNClasses()) * currClass + maxClass;
						changedClasses[clsIdx] += 1;
						nChanged++;
						this->pixUChar(idx) = maxClass;
					};
				};

			};
		};
	};

  return nChanged;
};

//=============================================================================
 
 int CDempsterShaferBuildingClassifier::changeSingular(
                         const CImageBuffer &dempsterShaferFirst,
                         const CImageBuffer &dempsterShaferSecond,
                         vector<int> &changedClasses,
                         unsigned long iterationIndex,
                         int neighbourhood)
{
	int nChanged = 0;
 
	int neighbours = neighbourhood * neighbourhood - 1;
	neighbourhood /= 3;
  
	long d1 = -neighbourhood * this->diffX 
              -neighbourhood * this->diffY;
	long d2 =  neighbourhood * this->diffX 
              -neighbourhood * this->diffY;
	long d3 =  neighbourhood * this->diffX 
              +neighbourhood * this->diffY;
   

	unsigned long firstIdx = neighbourhood * this->diffY + neighbourhood;
	unsigned long  lastInRow = (neighbourhood + 1) * this->diffY - 1;
	unsigned long  maxIdx = this->getNGV() - neighbourhood * this->diffY;

	while (lastInRow < maxIdx)
	{
		for (unsigned long idx = firstIdx; idx < lastInRow; ++idx)
		{
			vector<int> classVec(dempsterShaferPix.getNClasses() + 1,0);
			unsigned long uMax = idx + d3;
			for (unsigned long uMin = idx + d1, maxRow = idx + d2; 
				 maxRow <= uMax; uMin += this->diffY, maxRow += this->diffY)
			{
				for (unsigned long curr = uMin; curr <= maxRow; curr ++)
				{
					int currClass = dempsterShaferFirst.pixUChar(curr);
					if (currClass < (int) dempsterShaferPix.getNClasses()) 
						++classVec[currClass];
				};
			};

			int maxClass = 0;
			int iMax = classVec[0];

			for (unsigned long i = 1; i < dempsterShaferPix.getNClasses(); ++i)
			{
				if (classVec[i] > iMax)
				{
					maxClass = i;
					iMax = classVec[i];
				};
			};

			int currClass = this->pixUChar(idx);

			if (((maxClass == dempsterShaferSecond.pixUChar(idx)) || (iMax >= neighbours)) && 
				(currClass < (int) dempsterShaferPix.getNClasses()))
			{
				if (currClass != maxClass)
				{
					unsigned long clsInd = iterationIndex + (dempsterShaferPix.getNClasses()) * currClass + maxClass;
					changedClasses[clsInd] += 1;
					nChanged++;
					this->pixUChar(idx) = maxClass;
				};
			};
		};

		firstIdx += this->diffY;
		lastInRow += this->diffY;     
	};

	return nChanged;
 };

//=============================================================================
 
void CDempsterShaferBuildingClassifier::postClassify(
                      const CImageBuffer &dempsterShaferConflict,
                      const CImageBuffer &dempsterShaferSecond,
                      int neighbourhoodConf, int neighbourhoodMax,
                      int maxIterPostPrc)
{
 
	unsigned long u_itIndexDelta_ = dempsterShaferPix.getNClasses() * dempsterShaferPix.getNClasses();

	vector<int> changedClasses(u_itIndexDelta_ * maxIterPostPrc,0);


	int iterationcount = 0;
 

	int nChanged = 101;
	
	ostrstream oss;
	oss << "\nPost-classification\n===================\n\n   Maximum number of iterations: " << maxIterPostPrc
		<< "\n   Time: " << CSystemUtilities::timeString().GetChar() << "\n";
 
	if (this->listener)
	{
		this->listener->setTitleString("Post-Classification ...");
		this->listener->setProgressValue(1);
	}

	float fact = 50.0 / (float) maxIterPostPrc;
	while ((nChanged > 100) && (iterationcount < maxIterPostPrc))
	{
		CImageBuffer dempsterShaferFirst(*this);

		unsigned long itIndex = iterationcount * u_itIndexDelta_;
		++iterationcount;

		nChanged = changeUncertain(dempsterShaferConflict, dempsterShaferFirst,
                                   dempsterShaferSecond, changedClasses, itIndex,
                                   neighbourhoodConf);

		if (this->listener) this->listener->setProgressValue(float(2 * iterationcount) * fact);

		nChanged += changeSingular(dempsterShaferFirst, dempsterShaferSecond,
                                   changedClasses, itIndex, neighbourhoodMax);

		if (this->listener) this->listener->setProgressValue(float(2 * iterationcount + 1) * fact);

		oss << "   Iteration " ;
		oss.width(2);
		oss << iterationcount << ": ";
		oss.width(6);
		oss << nChanged << " Pixels changed\n";
	};

	oss << "\nStatistics of post-classification: \n\n   From To  ";
	for (int i = 0; i < iterationcount; ++i) 
	{
		oss.width(6);
		oss << i;
	}

	for (unsigned long i = 0; i < dempsterShaferPix.getNClasses(); ++i)
	{  
		unsigned long u0 = dempsterShaferPix.getNClasses() * i;
		for (unsigned long j = 0; j < dempsterShaferPix.getNClasses(); ++j)
		{ 
			if (i != j)
			{
				oss << "\n    " << dempsterShaferPix.getClassName(i) << "   " 
					<< dempsterShaferPix.getClassName(j) << " ";
				for (long k = 0; k < iterationcount; ++k)
				{
					oss.width(6);					
					oss << changedClasses[u0 + j + k * u_itIndexDelta_];      
				};
			};
		};
	};

	oss << "\n";  

	if (maxIterPostPrc > 0)
	{
		nChanged = 0;
		float f_dZ_ = (deltaHRecordPix.getXmax() + deltaHRecordPix.getXmin()) / 2.0f;
		unsigned char c_noBuildClass_ = useVegIndex() ? 3 : 2;
		unsigned long maxIdx = this->getNGV();
		for (unsigned long idx = 0; idx < maxIdx; idx++)
		{
			if ((this->pixUChar(idx) == 0) && (pNormDSM->pixFlt(idx) < f_dZ_))
			{
				pixUChar(idx) = c_noBuildClass_;
				nChanged++;
			};
		};

		oss.setf(ios::fixed, ios::floatfield); oss.precision(2);
		oss << "\n   " << nChanged << " pixels changed from B to ";

		if (useVegIndex()) oss << "S";
		else oss << "O";

		oss << " due to the nDSM being lower than " << f_dZ_ << " m";   
	};

	oss << "\n\n   Finished post classification; Time: " << CSystemUtilities::timeString().GetChar() 
		<< "\n";
     	
	if (this->listener)
	{
		this->listener->setProgressValue(100);
	}

	protHandler.print(oss, protHandler.prt+protHandler.scr);    
 };

//=============================================================================
 
void CDempsterShaferBuildingClassifier::classifyPixels(eDECISIONRULE rule, 
													   int maxIterPostPrc, 
													   const CCharString &path, 
													   const CCharString &suffix)
{
	ostrstream oss;
	oss << "\nClassifying pixels by Dempster-Shafer\n=====================================\n";
 
	if (useDz()) oss << deltaHRecordPix.getProtocolString("\n      dH (DSM-DTM): ").GetChar();
	else oss << "\n      dH (DSM-DTM):  Not used";

	if (useRoughStr())
	{
		oss << roughnessStrRecordPix.getProtocolString("\n      Homogeneity:  ").GetChar()
			<< "; Model: percentage";
	}
	else oss << "\n      Homogeneity:   Not used";

	if (useRoughIso()) oss << roughnessIsoRecordPix.getProtocolString("\n      Isotropy:     ").GetChar();
	else oss <<  "\n      Isotropy:      Not used";

	if (useFirstLastPulse()) 
	{
		oss << firstLastRecordPix.getProtocolString("\n      FP -LP:       ").GetChar() 
			<< "; 1 - P(FL) is assigned to class ";
		if (firstLastComplementaryClass == 14)      oss << "B or T or G or S";
		else if (firstLastComplementaryClass == 12) oss << "B or G or S";
		else if (firstLastComplementaryClass == 6)  oss << "B or T or O";
		else oss << "B or O";
	}
	else oss << "\n      FP -LP:        Not used";
     
	if (useVegIndex()) 
	{
		oss << NDVIRecordPix.getProtocolString("\n      NDVI:         ").GetChar();
		if (pVegIndexSigma) 
		{
			oss.setf(ios::fixed, ios::floatfield);
			oss.precision(2); 
			oss << "; assigning " << sigmaMultiplicator << " * sigma(NDVI) to B or T or G or S";
		}
		else oss << "; no modulation by sigma(NDVI)";
	}
	else oss << "\n      NDVI:          Not used";

	if (useExistingData())
	{
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(2); 
		oss << "\n      Existing data base:     PB: " << this->probExisting * 100.0f << "%";
	}
	else oss << "\n      Existing data base:          Not used";
  

	oss << "\n      Probability Model: ";

	if (deltaHRecordPix.getNumberOfEmpiricalRecords() <= 0) oss << "Cubic Polynomial";
	else oss <<  "\n      Probability Model: empirical (from file)";

	oss.precision(1);
	oss << " \n      Significance percentage: " << significantRoughQuantil * 100.0f
		<< "%\n      Decision rule:           "  
		<< CDempsterShafer::getRuleString(rule).GetChar() << "\n      Time: " 
		<< CSystemUtilities::timeString().GetChar() << "\n\n";

	protHandler.print(oss, protHandler.prt+protHandler.scr);

	CImageBuffer dempsterShaferFirst (this->offsetX, this->offsetY, this->width, this->height, 1, 8, true);
	CImageBuffer dempsterShaferSecond(this->offsetX, this->offsetY, this->width, this->height, 1, 8, true);
	CImageBuffer dempsterShaferConflict  (this->offsetX, this->offsetY, this->width, this->height, 1, 8, true);
	CImageBuffer dempsterShaferDifference(this->offsetX, this->offsetY, this->width, this->height, 1, 8, true);


  	if (this->listener)
	{
		this->listener->setTitleString("Pixel-wise Dempster-Shafer Classification ...");
		this->listener->setProgressValue(1);
	}

	float fact = 100.0 / (float) this->nGV;

	for (unsigned long idx = 0; idx < this->nGV; idx++)
	{
		if (!isValidPixel(idx)) this->pixUChar(idx) = 4;
		else
		{
			dempsterShaferPix.resetAllProbabilityMasses();
			float R = pRoughness->R(idx);

			if (useDz())             probDZpix(pNormDSM->pixFlt(idx));
			if (useRoughStr())       probRoughStrPix(R);
			if (useRoughIso())       probRoughDirPix(pRoughness->Q(idx), R);
			if (useFirstLastPulse()) probFLpix(pFirstLast->pixFlt(idx));
			if (useVegIndex())       probNpix(vegIndex(idx), sigmaVegIndex(idx));
			if (useExistingData())   probExistingData(pExistingBuildingMask->pixUChar(idx));

			dempsterShaferPix.computeCombinedProbabilityMasses();     
			int MaxClass = dempsterShaferPix.getClass(rule);
			this->pixUChar(idx) = MaxClass;
     
			float second, first;
			if ((rule == eMAXSUP) || (rule == eMAXSUPLGDUB)) first = dempsterShaferPix.getSupport(MaxClass);
			else if ((rule == eMAXPLAUS) || (rule == eMAXPLAUSLGDUB)) first = dempsterShaferPix.getPlausibility(MaxClass);
			else first = dempsterShaferPix.getCombinedProbability(MaxClass);

			dempsterShaferSecond.pixUChar(idx) = dempsterShaferPix.getRankBestClass(rule, 1, second);
			second = first - second;
			dempsterShaferConflict.pixUChar(idx) = (unsigned char) floor(float(255.0f) * dempsterShaferPix.getConflict());
     
			if (outputMode != eMINIMUM)
			{
				if (outputMode == eDEBUG)
				{
					float f_dFL_ = (useFirstLastPulse()) ? pFirstLast->pixFlt(idx) : 0.0f;
					printDSpix(pNormDSM->pixFlt(idx), R,  pRoughness->Q(idx), f_dFL_, vegIndex(idx));
				}
				dempsterShaferFirst.pixUChar(idx)  = (unsigned char) floor(float(255.0f) * first);
				dempsterShaferDifference.pixUChar(idx) = (unsigned char) floor(float(255.0f) * second);
			};
		}; 
		if (!(idx % this->width))
		{
  			if (this->listener)
			{
				this->listener->setProgressValue(fact * (float) idx);
			}
		}
	};
  
	CCharString FNBase = path + "DempsterShafer";
	exportPixelResults(FNBase, suffix);
  
	CCharString str("Pixels classified by Dempster Shafer; Time: ");
	str += CSystemUtilities::timeString() + "\n";
	protHandler.print(str,protHandler.prt+protHandler.scr);

  	if (this->listener)
	{
		this->listener->setProgressValue(100);
	}


	if (maxIterPostPrc)
	{
		int neighbourhoodConf = 5;
		int neighbourhoodMax  = 3;
		postClassify(dempsterShaferConflict, dempsterShaferSecond, neighbourhoodConf, 
                     neighbourhoodMax, maxIterPostPrc);
		FNBase = path + "DempsterShafer_postProc";
		exportPixelResults(FNBase, suffix);
	};


	if ((outputMode == eMAXIMUM) || (outputMode == eDEBUG))
	{
		CLookupTable lut = getClassLookup(dempsterShaferPix.getNClasses());

		CCharString fn = path + "DempsterShafer_max";
		CCharString suffext = suffix + ".tif";
		fn += suffext;
		dempsterShaferFirst.exportToTiffFile(fn.GetChar(), &lut, false);

		fn = path + "DempsterShafer_conf";
		fn += suffext;
		dempsterShaferConflict.exportToTiffFile(fn.GetChar(), 0, false);
		
		fn = path + "DempsterShafer_second";
		fn += suffext;
		dempsterShaferSecond.exportToTiffFile(fn.GetChar(), &lut, false);

		fn = path + "DempsterShafer_diff";
		fn += suffext;
		dempsterShaferDifference.exportToTiffFile(fn.GetChar(), 0, false);
		exportProbabilitiesPixels(path, suffix);
	};
};
          
//=============================================================================
 
void CDempsterShaferBuildingClassifier::exportPixelResults(const CCharString &fileNameBase,
                                                           const CCharString &suffix)
{
	CLookupTable lut = getClassLookup(dempsterShaferPix.getNClasses());
	CCharString fn = fileNameBase +  "_class" + suffix + ".tif";
	this->exportToTiffFile(fn.GetChar(), &lut, false);
};

//=============================================================================
 
void CDempsterShaferBuildingClassifier::classifyRegions(CLabelImage *buildingLabels, 
														vector <CDSMBuilding> &regionVector,
														eDECISIONRULE rule, bool useWaterModel,
														const CCharString &path,
														const CCharString &suffix)
{ 
	bool empiric = deltaHRecordReg.getNumberOfEmpiricalRecords() > 0;
	this->useHomogeneousForRegions = (fabs(homogRecordReg.getPrbMax() - homogRecordReg.getPrbMin()) > 0.01) ||  empiric;

	ostrstream oss1;
	oss1 << "\nClassifying regions by Dempster-Shafer\n======================================\n"
		 << deltaHRecordReg.getProtocolString("\n      dH (DSM-DTM):           ").GetChar();

	if (!this->useHomogeneousForRegions) 
		oss1 << "\n      Homogeneous percentage:  Not used";
	else oss1 << homogRecordReg.getProtocolString("\n      Homogeneous percentage: ").GetChar();

	oss1 << pointRecordReg.getProtocolString("\n      Point percentage:       ").GetChar();
  
	if (useVegIndex()) 
	{
		oss1 << NDVIRecordReg.getDescriptor("\n      NDVI:                   ", 3);
		if (useWaterModel) oss1 << NDVIRecordReg1.getDescriptor("\n          NDVI Transition B-S:", 3);
	}
	else oss1 << "\n      NDVI:                    Not used";

	oss1 << "\n      Probability Model: ";
	if (!empiric) oss1 << "Cubic Polynomial";
	else oss1 << "\n      Probability Model: : empirical (from file)";

	oss1 << "\n      Significance percentage (percentage of homogeneous pixels): ";

	oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(1);
	oss1 << significantRoughQuantil * 100.0f<< "%;\n      Decision rule:           " 
		 << CDempsterShafer::getRuleString(rule).GetChar() << "\n      Time: " 
		 << CSystemUtilities::timeString().GetChar() << "\n\n";

	protHandler.print(oss1, protHandler.prt+protHandler.scr);
	
	vector<unsigned char> classes(regionVector.size());
	classes[0] = 4;

	unsigned long numRegions = 0;
    
  	if (this->listener)
	{
		this->listener->setTitleString("Region-wise Dempster-Shafer Classification ...");
		this->listener->setProgressValue(1);
	}

	ostrstream oss;

	float fact = 100.0 / float(regionVector.size());
	for (unsigned long idx = 1; idx < regionVector.size(); ++idx)
	{
		probabilityMassesRegion(regionVector[idx], useWaterModel);
		dempsterShaferReg.computeCombinedProbabilityMasses();     

		int MaxClass = dempsterShaferReg.getClass(rule);
		classes[idx] = (unsigned char) MaxClass;

		if (outputMode >= eMEDIUM)
		{
			if (MaxClass != 0)
			{
				printDSreg(regionVector[idx].getIndex(), 
				       (float) (regionVector[idx].getAverageHeight() - regionVector[idx].getFloorHeight()),
				       regionVector[idx].getHomogeneousRatio(),  regionVector[idx].getPointRatio(), 
					   regionVector[idx].getAverageNDVI(), MaxClass);
			}
		}
		if (this->listener) this->listener->setProgressValue(float(idx) * fact);

		if (MaxClass == 0) ++numRegions;  
		else regionVector[idx].resetData(); 
	};

	if ((outputMode >= eMAXIMUM) && buildingLabels)
	{
		CImageBuffer exportBuf(*this);
		for (unsigned long u = 0; u < exportBuf.getNGV(); ++u)
		{
			unsigned short label = buildingLabels->getLabel(u);
			exportBuf.pixUChar(u) = classes[label];
		};

		CLookupTable lut = getClassLookup(dempsterShaferPix.getNClasses());

		CCharString fn = path + "DempsterShafer_Reg_class" + suffix + ".tif";

		exportBuf.exportToTiffFile(fn.GetChar(), &lut, false);
	};

  	if (this->listener) this->listener->setProgressValue(100);

	oss << "\nRegions classified by Dempster Shafer\n   Remaining number of building regions: "
		<< numRegions << "\n   Time: " << CSystemUtilities::timeString().GetChar() << "\n";
  
	protHandler.print(oss, protHandler.prt+protHandler.scr);
};

//=============================================================================
 
void CDempsterShaferBuildingClassifier::probabilityMassesRegion(const CDSMBuilding &region,
																bool useWaterModel)
{
	dempsterShaferReg.resetFinalProbabilityMasses();

	float dZ = (float) (region.getAverageHeight() - region.getFloorHeight());
	float mDZ = deltaHRecordReg.getProb(dZ);

	if (dempsterShaferReg.getNClasses() == 4)
	{
		dempsterShaferReg.assignProbabilityMass( 4, deltaHRecordReg.getIndex(), mDZ);         // B or T
		dempsterShaferReg.assignProbabilityMass( 9, deltaHRecordReg.getIndex(), 1.0f - mDZ);  // G or S 
	}
	else
	{
		dempsterShaferReg.assignProbabilityMass( 3, deltaHRecordReg.getIndex(), mDZ);         // B or T
		dempsterShaferReg.assignProbabilityMass( 2, deltaHRecordReg.getIndex(), 1.0f - mDZ);  // O 
	};

	if (!this->useHomogeneousForRegions)
	{ // assign 100% to Theta => don't use the percentage of homogeneous pixels!
		if (dempsterShaferReg.getNClasses() == 4)
			dempsterShaferReg.assignProbabilityMass(14, homogRecordReg.getIndex(), 1.0f);       // B or T or G or S
		else dempsterShaferReg.assignProbabilityMass(6, homogRecordReg.getIndex(), 1.0f); // B or T or O
	}
	else
	{
		float mH  = homogRecordReg.getProb(region.getHomogeneousRatio());

		dempsterShaferReg.assignProbabilityMass( 1, homogRecordReg.getIndex(), 1.0f - mH);  // T

		if (dempsterShaferReg.getNClasses() == 4)
			dempsterShaferReg.assignProbabilityMass(12, homogRecordReg.getIndex(), mH);     // B or G or S
		else dempsterShaferReg.assignProbabilityMass(4, homogRecordReg.getIndex(), mH);     // B or O
	};

	float mP  = pointRecordReg.getProb(region.getPointRatio());

	dempsterShaferReg.assignProbabilityMass( 1, pointRecordReg.getIndex(), mP); // T
	if (dempsterShaferReg.getNClasses() == 4)
		dempsterShaferReg.assignProbabilityMass(12, pointRecordReg.getIndex(), 1.0f - mP);        // B or G or S
	else dempsterShaferReg.assignProbabilityMass(4, pointRecordReg.getIndex(), 1.0f - mP);  // B or O

	if (useVegIndex())
	{
		float ndvi = region.getAverageNDVI();

		if (ndvi >= -100.0f)
		{
			float mN   =  NDVIRecordReg.getProb(ndvi);
			float mN1 = useWaterModel ? NDVIRecordReg1.getProb(ndvi) : 1.0;

			dempsterShaferReg.assignProbabilityMass( 3, NDVIRecordReg.getIndex(), (1.0f - mN1) * (1.0f - mN)); // S
			dempsterShaferReg.assignProbabilityMass( 6, NDVIRecordReg.getIndex(), mN1 * (1.0f - mN));		   // B or S
			dempsterShaferReg.assignProbabilityMass( 7, NDVIRecordReg.getIndex(), mN);
			dempsterShaferReg.assignProbabilityMass(14, NDVIRecordReg.getIndex(), 0.0f);
		}
		else
		{
			dempsterShaferReg.assignProbabilityMass( 3, NDVIRecordReg.getIndex(), 0.0f);
			dempsterShaferReg.assignProbabilityMass( 6, NDVIRecordReg.getIndex(), 0.0f);
			dempsterShaferReg.assignProbabilityMass( 7, NDVIRecordReg.getIndex(), 0.0f);
			dempsterShaferReg.assignProbabilityMass(14, NDVIRecordReg.getIndex(), 1.0f);
		};
	};
};
    
//=============================================================================
 
void CDempsterShaferBuildingClassifier::printDSreg(int index, float dz, float homogRatio,  
												   float pointRatio, float NDVI,
												   int Class) const
{
	ostrstream oss;
	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(3);
	oss << "\n\nEvaluating Region: " << index
		<< "\n\n==========================================" 
        << "\nEvaluation of Dempster-Shafer: Input Data:" 
        << "\n==========================================" 
        << "\ndZ: " << dz << ", Homogeneous ratio: " << homogRatio
        << ", Point ratio: " << pointRatio;
   
	if (useVegIndex()) oss << ", NDVI: " << NDVI;

	oss << "\n" << dempsterShaferReg.getPrintString()
        << "\nClass of maximum probability: " << dempsterShaferReg.getClassName(Class) << "\n";
   
	int option = protHandler.prt;
	if (outputMode == eMAXIMUM) option += protHandler.scr;
	protHandler.print(oss, option);
};

//=============================================================================
 
void CDempsterShaferBuildingClassifier::exportProbabilitiesPixels(const CCharString &path, const CCharString &suffix) const
{
	CCharString suffExt = suffix + ".tif";
	CCharString fn;

	CImageBuffer probabilities(0, 0, this->width, this->height, 1, 8, true);

	if (suffix.IsEmpty())
	{  		
		if (useVegIndex())
		{
			for (unsigned long idx = 0; idx < this->nGV; idx++)
			{
				if (!isValidPixel(idx)) probabilities.pixUChar(idx) = 0;
				else
				{
					float vi = vegIndex(idx);
					float sigma = sigmaVegIndex(idx);
					float p = 0.0;

					if ((vi >= -100.0f) && (sigma <= sigmaMax))
					{
						sigma *= (float) 0.01;
						p  = NDVIRecordPix.getProb(vi) * 255.0;

						if (sigma >= 0.0f)
						{
							sigma *= sigmaMultiplicator;
							p *= (1.0f - sigma);
						}
						if (p < 1.0) p = 1.0;
					}
					

					probabilities.pixUChar(idx) = (unsigned char) p;
				};
			};

			fn = path + CSystemUtilities::dirDelimStr + "prob_NDVI" + suffExt;
			probabilities.exportToTiffFile(fn.GetChar(), 0, false);
		};
			
		if (useFirstLastPulse())
		{
			for (unsigned long idx = 0; idx < this->nGV; idx++)
			{
				if (!isValidPixel(idx)) probabilities.pixUChar(idx) = 0;
				else
				{
					float m = floor(firstLastRecordPix.getProb(pFirstLast->pixFlt(idx)) * 255.0f);
					if (m <= 1.0f) m = 1;
					probabilities.pixUChar(idx) = (unsigned char) m; 
				};
			};

			fn = path + CSystemUtilities::dirDelimStr + "prob_FL" + suffExt;
			probabilities.exportToTiffFile(fn.GetChar(), 0, false);
		};

		if (useRoughStr())
		{
			for (unsigned long idx = 0; idx < this->nGV; idx++)
			{
				if (!isValidPixel(idx)) probabilities.pixUChar(idx) = 0;
				else
				{			
					float m = floor(roughnessStrRecordPix.getProb(pRoughness->R(idx)) * 255.0f);
					if (m <= 1.0f) m = 1;
					probabilities.pixUChar(idx) = (unsigned char) m; 
				};
			};
	     
			fn = path + CSystemUtilities::dirDelimStr + "prob_R" + suffExt;
			probabilities.exportToTiffFile(fn.GetChar(), 0, false);
		}

		if (useRoughIso())
		{
			for (unsigned long idx = 0; idx < this->nGV; idx++)  
			{
				if (!isValidPixel(idx)) probabilities.pixUChar(idx) = 0;
				else
				{
					float m = 0.0f;
/*					if (isRoughnessSignificant(pRoughness->R(idx)))
					{
						m = floor(roughnessIsoRecordPix.getProb(pRoughness->Q(idx)) * 255.0f);
						if (m <= 1.0f) m = 1.0f;
					};

*/
					m = floor(pRoughness->R(idx) * roughnessIsoRecordPix.getProb(pRoughness->Q(idx)) * 255.0f);
					probabilities.pixUChar(idx) = (unsigned char) m; 
				};
			};
	     
			fn = path + CSystemUtilities::dirDelimStr + "prob_Q" + suffExt;
			probabilities.exportToTiffFile(fn.GetChar(), 0, false);  
		}
	}
  
	if (useDz())
	{
		for (unsigned long idx = 0; idx < this->nGV; idx++)
		{
			if (!isValidPixel(idx)) probabilities.pixUChar(idx) = 0;
			else
			{
				float m = floor(deltaHRecordPix.getProb(pNormDSM->pixFlt(idx)) * 255.0f);
				if (m <= 1.0f) m = 1;
				probabilities.pixUChar(idx) = (unsigned char) m; 
			};
		};

		fn = path + CSystemUtilities::dirDelimStr + "prob_DZ" + suffExt; 
		probabilities.exportToTiffFile(fn.GetChar(), 0, false);  
	}
};

//=============================================================================
    
void CDempsterShaferBuildingClassifier::exportProbabilitiesRegions(
	                                CLabelImage *buildingLabels,
		                            const vector <CDSMBuilding> &regionVector,         
									const CCharString &suffix) const
{
	CCharString suffExt = suffix + ".tif";
	CCharString fn;

	vector <unsigned char> probVector(regionVector.size());
 
	probVector[0] = 0;
	CImageBuffer probabilities(0, 0, this->width, this->height, 1, 8, true, 0);

	if (useVegIndex())
	{
		for (unsigned long idx = 1; idx < regionVector.size(); ++idx)
		{
			float m  = NDVIRecordReg.getProb(regionVector[idx].getAverageNDVI()) * 254.0f + 1;
			probVector[idx] = (unsigned char) m;
		};

		for (unsigned long u = 0; u < this->nGV; ++u)
		{
			unsigned short label = buildingLabels->getLabel(u);
			probabilities.pixUChar(u) = probVector[label];
		};

		fn = "prob_NDVI_reg";
		fn += suffExt;   
		probabilities.exportToTiffFile(fn.GetChar(), 0, false);  
    };

	for (unsigned long idx = 1; idx < regionVector.size(); ++idx)
	{
		float m  = homogRecordReg.getProb(regionVector[idx].getHomogeneousRatio()) * 254.0f + 1;
		probVector[idx] = (unsigned char) m;
	}


	for (unsigned long u = 0; u < this->nGV; ++u)
	{
		unsigned long idx = buildingLabels->getLabel(u);
		probabilities.pixUChar(u) = probVector[idx];
	};

	fn = "prob_Hom_reg";
	fn += suffExt;   
	probabilities.exportToTiffFile(fn.GetChar(), 0, false);  
  
	for (unsigned long idx = 1; idx < regionVector.size(); ++idx)
	{
		float m  = pointRecordReg.getProb(regionVector[idx].getPointRatio()) * 254.0f + 1;
		probVector[idx] = (unsigned char) m;
	};   

	for (unsigned long u = 0; u < this->nGV; ++u)
	{
		unsigned long idx = buildingLabels->getLabel(u);
		probabilities.pixUChar(u) = probVector[idx];
	};

	fn = "prob_Pnt_reg";
	fn += suffExt;   
	probabilities.exportToTiffFile(fn.GetChar(), 0, false);  


	for (unsigned long idx = 1; idx < regionVector.size(); ++idx)
	{
		float m = 0.0f;
		if (isRoughnessSignificant(regionVector[idx].getAverageHomogeneity()))
		{
			m = floor(roughnessIsoRecordPix.getProb(regionVector[idx].getAverageIsotropy()) *  254.0f) + 1.0f;
			if (m <= 1.0f) m = 1.0f;
		};
		probVector[idx] = (unsigned char) m;
	};   

	for (unsigned long u = 0; u < this->nGV; ++u)
	{
		unsigned long idx = buildingLabels->getLabel(u);
		probabilities.pixUChar(u) = probVector[idx];
	};

	fn = "prob_Q_reg";
	fn += suffExt;   
	probabilities.exportToTiffFile(fn.GetChar(), 0, false);  
 
	for (unsigned long idx = 1; idx < regionVector.size(); ++idx)
	{
		float m  = roughnessStrRecordPix.getProb(regionVector[idx].getAverageHomogeneity()) * 254.0f + 1;
		probVector[idx] = (unsigned char) m;
	};   

	for (unsigned long u = 0; u < this->nGV; ++u)
	{
		unsigned long idx = buildingLabels->getLabel(u);
		probabilities.pixUChar(u) = probVector[idx];
	};

	fn = "prob_W_reg";
	fn += suffExt;   
	probabilities.exportToTiffFile(fn.GetChar(), 0, false);  
   
	for (unsigned long idx = 1; idx < regionVector.size(); ++idx)
	{
		float m  = deltaHRecordPix.getProb((float)(regionVector[idx].getAverageHeight() - regionVector[idx].getFloorHeight())) * 254.0f + 1;
		probVector[idx] = (unsigned char) m;
	};   

	for (unsigned long u = 0; u < this->nGV; ++u)
	{
		unsigned long idx = buildingLabels->getLabel(u);
		probabilities.pixUChar(u) = probVector[idx];
	};

	fn = "prob_DZ_reg";
	fn += suffExt;   
	probabilities.exportToTiffFile(fn.GetChar(), 0, false);  
};

//=============================================================================

CCharString CDempsterShaferBuildingClassifier::getHistogramsCSV(
                                 const vector<float> &indices,
                                 const vector<unsigned long> &histogramBuild,
                                 const vector<unsigned long> &histogramTree,
                                 const vector<unsigned long> &histogramGrass,
                                 const vector<unsigned long> &histogramSoil,
                                 const CCharString &title) const
{

	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(1);
  
	oss << title;
	  
	for (unsigned long u = 0; u < indices.size(); ++u)
	{	
		oss <<  "," << indices[u];
	};

	oss <<  "\nBuildings";
	  
	for (unsigned long u = 0; u < indices.size(); ++u)
	{	 
		oss <<  "," << histogramBuild[u];
	};

	oss <<  "\nTrees";
	  
	for (unsigned long u = 0; u < indices.size(); ++u)
	{	
		oss <<  "," << histogramTree[u];
	};

	oss <<  "\nGrassland";
	  
	for (unsigned long u = 0; u < indices.size(); ++u)
	{	 
		oss <<  "," << histogramGrass[u];
	};

	oss <<  "\nBare Soil";
	  
	for (unsigned long u = 0; u < indices.size(); ++u)
	{	  
		oss <<  "," << histogramSoil[u];
	};
	
	oss <<  "\n" << ends;

	char *z = oss.str();
	CCharString str(z);
	delete [] z;

	return str;
};

//=============================================================================
 
void CDempsterShaferBuildingClassifier::getHistogramDZ(
                                       vector<float> &indices,
                                       vector<unsigned long> &histogramBuild,
                                       vector<unsigned long> &histogramTree,
                                       vector<unsigned long> &histogramGrass,
                                       vector<unsigned long> &histogramSoil,
                                       const CImageBuffer *conflict, 
                                       float HWidth, float max,
                                       float maxConflict) const
{
	if (outputMode == eMAXIMUM)
	{
		CCharString str = "\nComputing Histograms of height differences between DSM and DTM\n\n";
		if (outputMode == eMAXIMUM) protHandler.print (str, protHandler.prt+protHandler.scr);
		else protHandler.print (str, protHandler.prt+protHandler.scr); 
	};


	unsigned long length = (unsigned long) floor(max / HWidth) + 1;
	resetVec(indices, length);
	resetVec(histogramBuild, length);
	resetVec(histogramTree, length);
	resetVec(histogramGrass, length);
	resetVec(histogramGrass, length);

	for (unsigned long u = 0; u < length; ++u)
	{
		indices[u] = (float(u) + 0.5f) * HWidth;
	};

	for (unsigned long u = 0; u < this->nGV; ++u)
	{
		float f_dZ_ = pNormDSM->pixFlt(u);
		unsigned long histIndex = 0;
		if (f_dZ_ > 0) histIndex = (unsigned long) floor((f_dZ_ / HWidth) + 0.5f);
		if (histIndex >= length) histIndex = length - 1;
   
		int Class = this->pixUChar(u);
		float f_conflict_ = maxConflict * 0.9f;
		if (conflict) f_conflict_ = conflict->pixUChar(u);
		if (f_conflict_ < maxConflict)
		{
			if (Class == 0) ++histogramBuild[histIndex];
			else if (Class == 1) ++histogramTree[histIndex];
			else if (Class == 2) ++histogramGrass[histIndex];
			else if (Class == 3) ++histogramSoil[histIndex];
		};
	};
};

//=============================================================================
 
void CDempsterShaferBuildingClassifier::getHistogramFL(
                                        vector<float> &indices,
                                        vector<unsigned long> &histogramBuild,
                                        vector<unsigned long> &histogramTree,
                                        vector<unsigned long> &histogramGrass,
                                        vector<unsigned long> &histogramSoil,
                                        const CImageBuffer *conflict, 
                                        float HWidth,  float min, 
                                        float max, float maxConflict) const
{
	if (outputMode == eMAXIMUM)
	{
		CCharString str = "\nComputing Histograms of height differences between first and last pulse\n\n";
		if (outputMode == eMAXIMUM) protHandler.print (str, protHandler.prt+protHandler.scr);
		else protHandler.print (str, protHandler.prt+protHandler.scr);
	};

	unsigned long length = (unsigned long) floor(max / HWidth) + 1;

	resetVec(indices, length);
	resetVec(histogramBuild, length);
	resetVec(histogramTree, length);
	resetVec(histogramGrass, length);
	resetVec(histogramGrass, length);

	for (unsigned long u = 0; u < length; ++u)
	{
		indices[u] = (float(u) + 0.5f) * HWidth;
	};
 
	for (unsigned long u = 0; u < this->nGV; ++u)
	{
		float f_fl_ = pFirstLast->pixFlt(u);
		unsigned long histIndex = 0;
		if (f_fl_ > min) 
			histIndex = (unsigned long) floor((f_fl_ / HWidth) + 0.5f);
		if (histIndex >= length) histIndex = length - 1;
   
		float f_conflict_ = maxConflict * 0.9f;
		if (conflict) f_conflict_ = conflict->pixUChar(u);
		if (f_conflict_ < maxConflict)
		{
			int Class = this->pixUChar(u);
			if (Class == 0) ++histogramBuild[histIndex];
			else if (Class == 1) ++histogramTree[histIndex];
			else if (Class == 2) ++histogramGrass[histIndex];
			else if (Class == 3) ++histogramSoil[histIndex];
		};
	};
};

//=============================================================================
 
void CDempsterShaferBuildingClassifier::getHistogramRoughStr(
                                       vector<float> &indices,
                                       vector<unsigned long> &histogramBuild,
                                       vector<unsigned long> &histogramTree,
                                       vector<unsigned long> &histogramGrass,
                                       vector<unsigned long> &histogramSoil,
                                       const CImageBuffer *conflict,
                                       float HWidth, float max,
                                       float maxConflict) const
{
	if (outputMode == eMAXIMUM)
	{
		CCharString str = "\nComputing Histograms of texture strength\n";
		if (outputMode == eMAXIMUM) protHandler.print (str, protHandler.prt+protHandler.scr);
		else protHandler.print (str, protHandler.prt+protHandler.scr);
		cout << endl;
	};

    unsigned long length = (unsigned long) floor(max / HWidth) + 1;
 	resetVec(indices, length);
	resetVec(histogramBuild, length);
	resetVec(histogramTree, length);
	resetVec(histogramGrass, length);
	resetVec(histogramGrass, length);
 
	for (unsigned long u = 0; u < length; ++u)
	{
		indices[u] = (float(u) + 0.5f) * HWidth;
	};

	for (unsigned long u = 0; u < this->nGV; ++u)
	{
		float f_w_ = pRoughness->getPercentage(u);
		unsigned long histIndex = (unsigned long) floor((f_w_ / HWidth) + 0.5f);
		if (histIndex >= length) histIndex = length - 1;
		float f_conflict_ = maxConflict * 0.9f;
		if (conflict) f_conflict_ = conflict->pixUChar(u);
		if (f_conflict_ < maxConflict)
		{
			int Class = this->pixUChar(u);
			if (Class == 0) ++histogramBuild[histIndex];
			else if (Class == 1) ++histogramTree[histIndex];
			else if (Class == 2) ++histogramGrass[histIndex];
			else if (Class == 3) ++histogramSoil[histIndex];
		};
	};
};

//=============================================================================
 
void CDempsterShaferBuildingClassifier::getHistogramRoughDir(
                                       vector<float> &indices,
                                       vector<unsigned long> &histogramBuild,
                                       vector<unsigned long> &histogramTree,
                                       vector<unsigned long> &histogramGrass,
                                       vector<unsigned long> &histogramSoil,
                                       const CImageBuffer *conflict,
                                       float maxConflict) const
{
	if (outputMode == eMAXIMUM)
	{
		CCharString str = "\nComputing Histograms of texture directedness\n\n";
		if (outputMode == eMAXIMUM) protHandler.print (str, protHandler.prt+protHandler.scr);
		else protHandler.print (str, protHandler.prt+protHandler.scr);
	};

	unsigned long length =  100;
	resetVec(indices, length);
	resetVec(histogramBuild, length);
	resetVec(histogramTree, length);
	resetVec(histogramGrass, length);
	resetVec(histogramGrass, length);

	for (unsigned long u = 0; u < length; ++u)
	{
		indices[u] = (float) u;
	};

	for (unsigned long u = 0; u < this->nGV; ++u)
	{
 		float f_w_ = pRoughness->getPercentage(u);
		float f_q_ = pRoughness->Q(u);
		unsigned long histIndex = (unsigned long) floor(f_q_ * 99.9f);

		float f_conflict_ = maxConflict * 0.9f;
		if (conflict) f_conflict_ = conflict->pixUChar(u);
		if (f_conflict_ < maxConflict)
		{
			int Class = this->pixUChar(u);
			if (Class == 0) ++histogramBuild[histIndex];
			else if (Class == 1) ++histogramTree[histIndex];
			else if (Class == 2) ++histogramGrass[histIndex];
			else if (Class == 3) ++histogramSoil[histIndex];
		};
	};
};

//=============================================================================
 
void CDempsterShaferBuildingClassifier::getHistogramNDVI(
                                       vector<float> &indices,
                                       vector<unsigned long> &histogramBuild,
                                       vector<unsigned long> &histogramTree,
                                       vector<unsigned long> &histogramGrass,
                                       vector<unsigned long> &histogramSoil,
                                       const CImageBuffer *conflict,
                                       float maxConflict) const
{
	if (outputMode == eMAXIMUM)
	{
		CCharString str = "\nComputing Histograms of NDVI\n\n";
		if (outputMode == eMAXIMUM) protHandler.print (str, protHandler.prt+protHandler.scr);
		else protHandler.print (str, protHandler.prt+protHandler.scr);
	};

	unsigned long length = 201;
	resetVec(indices, length);
	resetVec(histogramBuild, length);
	resetVec(histogramTree, length);
	resetVec(histogramGrass, length);
	resetVec(histogramGrass, length);

	for (unsigned long u = 0; u < length; ++u)
	{
		indices[u] = (float) u;
	};

	for (unsigned long u = 0; u < this->nGV; ++u)
	{
		float f_conflict_ = maxConflict * 0.9f;
		if (conflict) f_conflict_ = conflict->pixUChar(u);
		if (f_conflict_ < maxConflict)
		{
			float VI = vegIndex(u) + 100.0f;
			unsigned long histIndex = 0;
			if (VI > 0.0f)
			{
				histIndex = (unsigned long) (VI);
			};
			int Class = this->pixUChar(u);
			if (Class == 0) ++histogramBuild[histIndex];
			else if (Class == 1) ++histogramTree[histIndex];
			else if (Class == 2) ++histogramGrass[histIndex];
			else if (Class == 3) ++histogramSoil[histIndex];
		};
	};
};

//=============================================================================
 
void CDempsterShaferBuildingClassifier::printHistograms(const CImageBuffer *conflict,
                                                        float maxConflict) const
{
	vector<float> indices;
	vector<unsigned long> histogramBuild, histogramTree, histogramGrass, histogramSoil;
     
	float HWidth = 0.125f;
	float max    = 25.0f;
	getHistogramDZ(indices, histogramBuild, histogramTree, histogramGrass, 
                   histogramSoil, 0, HWidth, max,maxConflict);


	ostrstream oss;
	oss << "\n\n" << getHistogramsCSV(indices,histogramBuild, histogramTree,histogramGrass, histogramSoil,
                                      "DH (DSM-DTM)") 
		<< "\n\n";


	getHistogramDZ(indices, histogramBuild, histogramTree, histogramGrass, 
                   histogramSoil, conflict, HWidth, max,maxConflict);

	oss << "\n\n" << getHistogramsCSV(indices,histogramBuild, histogramTree,
                                      histogramGrass, histogramSoil, "DH (DSM-DTM)") 
		<< "\n\n";

	if (useFirstLastPulse())
	{
		HWidth = 0.125f;
		max   = 35.0f;
		getHistogramFL(indices, histogramBuild, histogramTree, histogramGrass, 
                       histogramSoil, 0, HWidth, 0.0f, max, maxConflict);

		oss << getHistogramsCSV(indices,histogramBuild, histogramTree, histogramGrass, 
			                    histogramSoil,"DH(FP-LP)")
	        << "\n\n";

		getHistogramFL(indices, histogramBuild, histogramTree, histogramGrass, 
                       histogramSoil, conflict, HWidth, 0.0f, max, maxConflict);

 		oss << getHistogramsCSV(indices,histogramBuild, histogramTree, histogramGrass, 
			                    histogramSoil,"DH(FP-LP)")
	        << "\n\n";
	};

	max   = 70.0f;
	HWidth = 0.25f;

	getHistogramRoughStr(indices, histogramBuild, histogramTree, histogramGrass, 
                         histogramSoil, 0, HWidth, max, maxConflict);

  
	oss << getHistogramsCSV(indices,histogramBuild, histogramTree, histogramGrass, histogramSoil, "W")
		<< "\n\n";

	getHistogramRoughStr(indices, histogramBuild, histogramTree, histogramGrass, 
                         histogramSoil, conflict, HWidth, max, maxConflict);

  
	oss << getHistogramsCSV(indices,histogramBuild, histogramTree, histogramGrass, histogramSoil, "W") 
		<< "\n\n";

	getHistogramRoughDir(indices, histogramBuild, histogramTree, histogramGrass, 
                         histogramSoil, 0, maxConflict);

	oss << getHistogramsCSV(indices,histogramBuild, histogramTree, histogramGrass, histogramSoil, "Q")
		<< "\n\n";

	getHistogramRoughDir(indices, histogramBuild, histogramTree, histogramGrass, 
                         histogramSoil, conflict, maxConflict);

  
	oss << getHistogramsCSV(indices,histogramBuild, histogramTree, histogramGrass, histogramSoil, "Q")
		<< "\n\n";

	if (useVegIndex())
	{
		getHistogramNDVI(indices, histogramBuild, histogramTree, histogramGrass, 
	                     histogramSoil, 0, maxConflict);
 
		oss << getHistogramsCSV(indices,histogramBuild, histogramTree, histogramGrass, histogramSoil, "NDVI") 
			<< "\n\n";  
 

		getHistogramNDVI(indices, histogramBuild, histogramTree, histogramGrass, 
                         histogramSoil, conflict, maxConflict);

		oss << getHistogramsCSV(indices,histogramBuild, histogramTree, histogramGrass, histogramSoil, "NDVI") 
			<< "\n\n";
	};

	protHandler.print(oss, protHandler.prt);
};
	
void CDempsterShaferBuildingClassifier::resetVec(vector<unsigned long> &histo, unsigned long length)
{
	histo.resize(length);
	for (unsigned long i = 0; i < length; ++i) histo[0] = 0;
};

void CDempsterShaferBuildingClassifier::resetVec(vector<float> &indices, unsigned long length)
{
	indices.resize(length);
	for (unsigned long i = 0; i < length; ++i) indices[0] = 0;
};

#endif // WIN32
