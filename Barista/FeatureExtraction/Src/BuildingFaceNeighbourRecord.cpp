/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <float.h>
#include <strstream>
using namespace std;

#include "ProtocolHandler.h"
#include "BuildingFaceNeighbourRecord.h"
#include "XYContour.h"
#include "XYPolyLine.h"
#include "XYZPolyLine.h"
#include "XYZLinesAdj.h"
#include "3DadjPlane.h"
#include "TFW.h"
#include "XYLineSegment.h"
#include "BuildingFacePoly.h"
#include "BuildingFace.h"
#include "LineMatchingHypothesis.h"
#include "BuildingFaceNeighbourVector.h"

//****************************************************************************

CBuildingFaceNeighbourRecord::CBuildingFaceNeighbourRecord() : 
              region1(0), region2(0), eNeighbourType(eUNDEFINEDNEIGHBOUR),
              pInitialPoly (0), pApproximatePoly(0), pFinalPoly(0), 
			  pIntersection(0), facePoly1(0), facePoly2(0), rmsError(0.0f)
{
};

//============================================================================

CBuildingFaceNeighbourRecord::CBuildingFaceNeighbourRecord(unsigned short Region1,
	                                                       unsigned short Region2):
              region1 (Region1), region2(Region2), eNeighbourType(eUNDEFINEDNEIGHBOUR),
              pInitialPoly (0), pApproximatePoly(0), pFinalPoly(0), 
			  pIntersection(0), facePoly1(0), facePoly2(0), rmsError(0.0f)
{
	this->checkOrder();
};

//============================================================================

CBuildingFaceNeighbourRecord::CBuildingFaceNeighbourRecord(unsigned short Region1, 
	                                                       unsigned short Region2, 
		                                                   eNEIGHBORTYPE eType):
              region1 (Region1), region2(Region2), eNeighbourType(eType),
              pInitialPoly (0), pApproximatePoly(0), pFinalPoly(0), 
			  pIntersection(0), facePoly1(0), facePoly2(0), rmsError(0.0f)
{
	this->checkOrder();
}

//============================================================================

CBuildingFaceNeighbourRecord::CBuildingFaceNeighbourRecord(const CBuildingFaceNeighbourRecord &other):
              region1 (other.region1), region2(other.region2), 
			  eNeighbourType(other.eNeighbourType), rmsError(other.rmsError),
              pInitialPoly (0), pApproximatePoly(0), pFinalPoly(0), 
			  pIntersection(0), facePoly1(other.facePoly1), facePoly2(other.facePoly2)
{
	this->checkOrder();
	if (other.pInitialPoly)     this->pInitialPoly     = new CXYContour     (*other.pInitialPoly);
	if (other.pApproximatePoly) this->pApproximatePoly = new CXYPolyLine    (*other.pApproximatePoly);  
	if (other.pFinalPoly)       this->pFinalPoly       = new CXYZPolyLine   (*other.pFinalPoly);  
	if (other.pIntersection)    this->pIntersection    = new CXYZHomoSegment(*other.pIntersection);
};

//============================================================================

CBuildingFaceNeighbourRecord::~CBuildingFaceNeighbourRecord() 
{ 
	this->resetPolys();
	this->resetIntersection();
	this->facePoly1 = 0;
	this->facePoly2 = 0;
};

//============================================================================

void CBuildingFaceNeighbourRecord::checkOrder()
{
	if (this->region2 < this->region1)
	{
		unsigned short dmy = this->region1;
		this->region1 = this->region2;
		this->region2 = dmy;
		CBuildingFacePoly *p = this->facePoly2;
		this->facePoly2 = this->facePoly1;
		this->facePoly1 = p;
	};

	if (this->region1 == 0) this->eNeighbourType = eSTEPEDGE;
}; 
	  
//============================================================================

bool CBuildingFaceNeighbourRecord::hasRegion(unsigned short region)
{
	if (region == this->region1) return true;
	if (region == this->region2) return true;

	return false;
};


//============================================================================

float CBuildingFaceNeighbourRecord::getOverlapPercentage(const vector <CLineMatchingHypothesis *> &matches) const
{
	double overlapLength = 0.0;
	double totalLength = 0.0;
	CXYPoint p1, p2;
	C3DPoint P1, P2;

	vector<bool> used(matches.size(), false);

	for (int i = 1; i < this->pApproximatePoly->getPointCount(); ++i)
	{
		CXYLineSegment polySeg(this->pApproximatePoly->getPoint(i - 1), this->pApproximatePoly->getPoint(i));
		vector<CXYLineSegment> overlapSegments;

		for (unsigned int j = 0; j < matches.size(); ++j)
		{
			if ((matches[j]->objectEdgeDescriptor == this) && (!used[j]))
			{
				P1 = matches[j]->pObjectEdge->getPoint1();
				P2 = matches[j]->pObjectEdge->getPoint2();
				p1.x = P1.x; p1.y = P1.y;
				p2.x = P2.x; p2.y = P2.y;
				CXYLineSegment matchSeg(p1, p2);
				if (matchSeg.project(polySeg)) 
				{
					overlapSegments.push_back(matchSeg);
					used[j] = true;
				}
			}
		}

		for (unsigned int j = 0; j < overlapSegments.size(); ++j)
		{
			for (unsigned int k = j + 1; k < overlapSegments.size(); ++k)
			{
				CXYLineSegment matchSeg = overlapSegments[j];
				if (matchSeg.project(overlapSegments[k]))
				{
					CXYPoint p1 = overlapSegments[j].getStartPt();
					CXYPoint p2 = overlapSegments[j].getEndPt();
					C2DPoint dir = (C2DPoint) p2 - (C2DPoint) p1;
					if (dir.innerProduct((C2DPoint) overlapSegments[k].getStartPt() - (C2DPoint) p1) < 0.0) 
						p1 = overlapSegments[k].getStartPt();

					if (dir.innerProduct((C2DPoint) overlapSegments[k].getEndPt() - (C2DPoint) p2) > 0.0) 
						p2 = overlapSegments[k].getEndPt();

					overlapSegments[j] = CXYLineSegment(p1, p2);
					for (unsigned int l = k + 1; l < overlapSegments.size(); ++l)
					{
						overlapSegments[k - 1] = overlapSegments[k];
					}
					overlapSegments.resize(overlapSegments.size() - 1);
					k = j + 1;
				}
			}
		}

		for (unsigned int j = 0; j < overlapSegments.size(); ++j)
		{
			overlapLength += overlapSegments[j].length();
		}

		totalLength += polySeg.length();
	}

	float percentage = 0.0f;
	if (totalLength > FLT_EPSILON) percentage = float(overlapLength / totalLength);
	return percentage;
};

//============================================================================

C2DPoint CBuildingFaceNeighbourRecord::getInitialStartPt() const 
{ 
	return *this->pInitialPoly->getPoint(0); 
};

//============================================================================

C2DPoint CBuildingFaceNeighbourRecord::getInitialEndPt()   const 
{ 
	return *this->pInitialPoly->getPoint(this->pInitialPoly->getPointCount() - 1); 
};

//============================================================================

C2DPoint CBuildingFaceNeighbourRecord::getInitialPt(unsigned int index) const 
{ 
	return *this->pInitialPoly->getPoint(index); 
};


//============================================================================

double CBuildingFaceNeighbourRecord::getInitialLength() const
{
	double length = 0.0;
	if (this->pInitialPoly) length = this->pInitialPoly->getLength();
	return length;
};

//============================================================================

CXYZPoint CBuildingFaceNeighbourRecord::getFinalStartPt() const 
{ 
	return *this->pFinalPoly->getPoint(0); 
};

//============================================================================

CXYZPoint CBuildingFaceNeighbourRecord::getFinalEndPt() const 
{ 
	return *this->pFinalPoly->getPoint(this->pFinalPoly->getPointCount() - 1); 
};

//============================================================================

CXYZPoint CBuildingFaceNeighbourRecord::getFinalPt(unsigned int index) const 
{ 
	return *this->pFinalPoly->getPoint(index); 
};

//============================================================================

double CBuildingFaceNeighbourRecord::getFinalLength() const 
{ 
	return this->pFinalPoly->getLength(); 
};

//============================================================================

unsigned int CBuildingFaceNeighbourRecord::getFinalNumber() const 
{ 
	return this->pFinalPoly->getPointCount();
}

//============================================================================

CCharString CBuildingFaceNeighbourRecord::getPrintString() const
{
	ostrstream oss;
	oss << "NEIGHBOURS: ";
	oss.width(3);
	oss << this->region1 << " / ";
	oss.width(3);
	oss << this->region2<<"; TYPE: ";
	oss.width(13);

	switch(this->eNeighbourType)
	{
		case eUNDEFINEDNEIGHBOUR: oss << " UNDEFINED";
			                      break;

		case eNONEIGHBOUR:        oss << " UNDEFINED";
			                      break;

		case eCOPLANAR:           oss << " COPLANAR";
			                      break;

		case eINTERSECT:          oss << " INTERSECTION";
			                      break;

		case eSTEPEDGE:           oss << " STEP EDGE";
			                      break;

		default: break;
	};

	oss << "; LENGTH: ";
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(2);
	oss.width(7);
	oss << this->getInitialLength();
	
	oss <<"; ERROR: ";
	oss.precision(3); oss.width(7); 
	oss << this->rmsError << ends;
	char *z = oss.str();
	CCharString str(z);
	delete [] z;
	return str;
};


//============================================================================
    
void CBuildingFaceNeighbourRecord::mergeSegmentCounters(int minPts, vector<int> &counter)
{
	bool changed = true;

	while (changed)
	{
		changed = false;
		for (unsigned int i = 2; i < counter.size(); i++)
		{
			if (((abs(counter[i-1]) <= minPts) && (abs(counter[i])   >   minPts)) || 
				((abs(counter[i-2]) >  minPts) && (abs(counter[i-1]) <=  minPts)))
			{
				counter[i-2] += counter[i];
				if (counter[i] > 0) counter[i-2] += 1;
				else counter[i-2] -= 1;

				for (unsigned int j = i + 1; j < counter.size(); ++j)
				{
					counter[j-2] = counter[j];
				};

				counter.resize(counter.size() - 2);
				changed = true;
			};
		};

		if ((counter.size() > 1) && (abs(counter[0]) <= minPts) && (abs(counter[1]) > minPts))
		{
			counter[0] = counter[1] - counter[0];

			for (unsigned int j = 2; j < counter.size(); ++j)
			{
				counter[j-1] = counter[j];
			};
		
			counter.resize(counter.size() - 1);
			changed = true;
		};
	};

	if (outputMode == eMAXIMUM)
	{
		ostrstream oss;

		for (unsigned int i = 0; i < counter.size(); i++)
		{
			if (counter[i] > 0) oss << "Intersection";
			else oss << "Step edge";
			oss << " ("; oss.width(3);
			oss << abs(counter[i]) << " point";
			if (abs(counter[i]) > 1) oss << "s";
			oss << "); ";
		};

		oss << "\n" << ends;  
		protHandler.print (oss,protHandler.prt);
	};
};
   	 
//============================================================================
	  
void CBuildingFaceNeighbourRecord::initApproxFinal(float maxDist, float sigmaContour)
{
	if (this->pApproximatePoly) delete this->pApproximatePoly;
	this->pApproximatePoly = new CXYPolyLine;
	this->pApproximatePoly->initFromContour(this->pInitialPoly, maxDist, sigmaContour);

	if (this->pFinalPoly) delete this->pFinalPoly;
	this->pFinalPoly = new CXYZPolyLine;
	
	CXYZPoint p3D;
	
	for (int i = 0; i < this->pApproximatePoly->getPointCount(); ++i)
	{
		p3D.x = this->pApproximatePoly->getPoint(i).x;
		p3D.y = this->pApproximatePoly->getPoint(i).y;		
		this->pFinalPoly->addPoint(p3D);
	}

	this->initIntersection();
};

//============================================================================
	  
void CBuildingFaceNeighbourRecord::cutOut(unsigned int indexFrom, unsigned int indexTo, float maxDist, float sigmaContour)
{
	if (indexTo < this->pInitialPoly->getPointCount() - 1) this->removeFromInitial(indexTo + 1, this->pInitialPoly->getPointCount() - 1);
	if (indexFrom > 0) this->removeFromInitial(0, indexFrom - 1);
	
	this->initApproxFinal(maxDist, sigmaContour);
};

//============================================================================

eNEIGHBORTYPE CBuildingFaceNeighbourRecord::classify(CBuildingFaceNeighbourVector *neighbourVector, 
													 float minLenghtRatio, float minLength, float delta,
													 float maxDist, float sigmaContour)
{
	eNEIGHBORTYPE retVal = eSTEPEDGE;
	
	if (this->pIntersection)
	{
		C3DPoint red = this->facePoly1->getFace()->getReduction();
		float deltaFactor = 1.5f;

		float rmsZ = CBuildingFace::getRmsZ();
		float rmsXY1 = rmsZ / (float) this->facePoly1->getFace()->getObliqueness();
		if (rmsXY1 < deltaFactor * delta) rmsXY1 = deltaFactor * delta;  
		else if (rmsXY1 > 5.0f * delta) rmsXY1 = 5.0f * delta;
		float rmsXY2 = rmsZ / (float) this->facePoly2->getFace()->getObliqueness();
		if (rmsXY2 < deltaFactor * delta) rmsXY2 =deltaFactor * delta;  
		else if (rmsXY2 > 5.0f * delta) rmsXY2 = 5.0f * delta;

		unsigned int numberOfPoints = this->pInitialPoly->getPointCount();

		unsigned int nPtsIn  = 0;
		unsigned int nPtsOut = 0;

		C2DPoint *p2D = this->pInitialPoly->getPoint(0);
		CXYZPoint polyPos1(p2D->x, p2D->y, this->facePoly1->getFace()->getHeight(p2D->x + red.x, p2D->y + red.y) - red.z);
		polyPos1.setSX(rmsXY1); polyPos1.setSY(rmsXY1); polyPos1.setSZ(rmsXY1);
		
		CXYZPoint polyPos2(p2D->x, p2D->y, this->facePoly2->getFace()->getHeight(p2D->x + red.x, p2D->y + red.y) - red.z);
		polyPos2.setSX(rmsXY2); polyPos2.setSY(rmsXY2); polyPos2.setSZ(rmsXY2);

		float indicator1 = this->pIntersection->containsPoint(polyPos1);
		float indicator2 = this->pIntersection->containsPoint(polyPos2);
		if (indicator2 < 0) indicator1 = indicator2;

		int prevSegInOut = (indicator1 >= 0.0f) ? 1 : -1;
		int segId = 1;
		vector<int> segCounter(numberOfPoints);
		segCounter[0] = prevSegInOut;

		for (unsigned int i = 1; i < numberOfPoints; i++)
		{
			p2D = this->pInitialPoly->getPoint(i);
			polyPos1.x = p2D->x; 
			polyPos1.y = p2D->y; 
			polyPos1.z = this->facePoly1->getFace()->getHeight(p2D->x + red.x, p2D->y + red.y) - red.z;
			polyPos2.x = p2D->x; 
			polyPos2.y = p2D->y; 
			polyPos2.z = this->facePoly2->getFace()->getHeight(p2D->x + red.x, p2D->y + red.y) - red.z;

			indicator1 = this->pIntersection->containsPoint(polyPos1);
			indicator2 = this->pIntersection->containsPoint(polyPos2);
			if (indicator2 < 0) indicator1 = indicator2;

			int segInOut = (indicator1 >= 0.0f) ? 1 : -1;

			if (segInOut > 0) nPtsIn++;
			else nPtsOut++;

			if (segInOut == prevSegInOut)
			{
				segCounter[segId-1] += segInOut;
			}
			else
			{
				prevSegInOut = segInOut;
				++segId;
				segCounter[segId-1] = segInOut;
			}
		};

		segCounter.resize(segId);

		this->mergeSegmentCounters(1, segCounter);
		this->mergeSegmentCounters(2, segCounter);

		if (segCounter.size() < 2)
		{
			retVal = (nPtsIn >= nPtsOut)? eINTERSECT : eSTEPEDGE;
			if (retVal  == eINTERSECT)
			{
				float newLength = (float) this->pIntersection->getLength();
				this->cutOffIntersection();

				float lenghtRatio = newLength / (float) this->pInitialPoly->getLength();
				if ((lenghtRatio < minLenghtRatio) && (newLength > minLength))
				{
					retVal = eSTEPEDGE;
					this->setType(eSTEPEDGE);
				}
				else 
				{
					this->setType(eINTERSECT);
				};
			}
			else this->setType(eSTEPEDGE);
		}
		else
		{
			vector<CBuildingFaceNeighbourRecord *> newRecords(segCounter.size(), 0);

			for (unsigned int seg = 0; seg < segCounter.size(); ++seg)
			{
				newRecords[seg] = new CBuildingFaceNeighbourRecord(*this);
			}


			unsigned int index0 = 0;

			for (unsigned int seg = 0; seg < segCounter.size(); ++seg)
			{
				unsigned int index1 = index0 + abs(segCounter[seg]);
				newRecords[seg]->cutOut(index0, index1, maxDist, sigmaContour);

				if (segCounter[seg] < 0) newRecords[seg]->setType(eSTEPEDGE);
				else
				{
					float newLength = (float) newRecords[seg]->pIntersection->getLength();
					float lenghtRatio = (float) (newLength / (float) newRecords[seg]->pInitialPoly->getLength());
					if (lenghtRatio < 0.5f) newRecords[seg]->setType(eSTEPEDGE);
					else 
					{
						CXYPoint p1(newRecords[seg]->pIntersection->getPoint1().x, newRecords[seg]->pIntersection->getPoint1().y);
						CXYPoint p2(newRecords[seg]->pIntersection->getPoint2().x, newRecords[seg]->pIntersection->getPoint2().y);
						CXYLineSegment sIntersect(p1, p2);
						double dist = 0;
						for (unsigned int j = 0; j < newRecords[seg]->pInitialPoly->getPointCount(); ++j)
						{
							C2DPoint *p1 = newRecords[seg]->pInitialPoly->getPoint(j);
							dist += sIntersect.distanceFromSegment(*p1);
						}

						dist /= (double) newRecords[seg]->pInitialPoly->getPointCount();
						if (newLength / dist > 0.5)  newRecords[seg]->setType(eINTERSECT);
						else newRecords[seg]->setType(eSTEPEDGE);
					}
				};

				index0 = index1;
			};

			if (this->facePoly1->getNNeighbours() == 1)
			{
				if (newRecords[0]->eNeighbourType == newRecords[newRecords.size() - 1]->eNeighbourType)
				{
					newRecords[newRecords.size() - 1]->pInitialPoly->append(newRecords[0]->pInitialPoly);
					delete newRecords[0];
					newRecords[0] = newRecords[newRecords.size() - 1];
					newRecords.resize(newRecords.size()- 1);
					newRecords[0]->initApproxFinal(maxDist, sigmaContour);
					newRecords[0]->setType(newRecords[0]->eNeighbourType);
				}
			}

			for (unsigned int i = 1; i < newRecords.size(); ++i)
			{
				if (newRecords[i-1]->eNeighbourType == newRecords[i]->eNeighbourType)
				{
					newRecords[i-1]->pInitialPoly->append(newRecords[i]->pInitialPoly);
					delete newRecords[i];
					for (unsigned int j = i + 1; j < newRecords.size(); ++j)
					{
						newRecords[j - 1] = newRecords[j];
					}
					
					newRecords.resize(newRecords.size()- 1);
					newRecords[i-1]->initApproxFinal(maxDist, sigmaContour);
					newRecords[i-1]->setType(newRecords[i-1]->eNeighbourType);
					--i;
				}

			}


			CBuildingFaceNeighbourRecord *newThis = newRecords[0];
			newRecords[0] = this;

			this->facePoly1->replaceNeighbour(this, newRecords);
			this->facePoly2->replaceNeighbour(this, newRecords);

			for (unsigned int seg = 1; seg < newRecords.size(); ++seg)
			{
				neighbourVector->addNeighbour(newRecords[seg]);
			}

			*this = *newThis;

			delete newThis;
			retVal = this->eNeighbourType;
		};
	};

	return retVal;
}

//============================================================================

CBuildingFaceNeighbourRecord &CBuildingFaceNeighbourRecord::operator = 
                       (const CBuildingFaceNeighbourRecord& other)
{
	if ( this == &other )  return *this;

	this->resetPolys(); 
	this->resetIntersection();

	this->region1        = other.region1;
	this->region2        = other.region2; 
	this->rmsError       = other.rmsError;
	this->eNeighbourType = other.eNeighbourType;
	this->facePoly1      = other.facePoly1;
	this->facePoly2      = other.facePoly2;
	this->checkOrder();


	if (other.pInitialPoly)     this->pInitialPoly     = new CXYContour     (*other.pInitialPoly);
	if (other.pApproximatePoly) this->pApproximatePoly = new CXYPolyLine    (*other.pApproximatePoly);  
	if (other.pFinalPoly)       this->pFinalPoly       = new CXYZPolyLine   (*other.pFinalPoly);  
	if (other.pIntersection)    this->pIntersection    = new CXYZHomoSegment(*other.pIntersection);

	return *this;
};

//============================================================================

bool CBuildingFaceNeighbourRecord::operator == 
                       (const CBuildingFaceNeighbourRecord& other)
{
	if (this == &other) return true;

	if ((this->region1         != other.region1) || 
		(this->region2         != other.region2) ||
		(this->eNeighbourType  != other.eNeighbourType) ||
		(this->getFinalEndPt().distanceFrom(other.getFinalEndPt()) > FLT_EPSILON) || 
		(this->getFinalStartPt().distanceFrom(other.getFinalStartPt()) > FLT_EPSILON))
		return false;

	return true;
};

//============================================================================

bool CBuildingFaceNeighbourRecord::operator != 
                       (const CBuildingFaceNeighbourRecord& other)
{
	if (this == &other) return false;

	if ((this->region1         != other.region1) || 
		(this->region2         != other.region2) ||
		(this->eNeighbourType  != other.eNeighbourType) ||
		(this->getFinalEndPt().distanceFrom(other.getFinalEndPt()) > FLT_EPSILON) || 
		(this->getFinalStartPt().distanceFrom(other.getFinalStartPt()) > FLT_EPSILON))
		return true;

	return false;
};

//============================================================================

void CBuildingFaceNeighbourRecord::setType(eNEIGHBORTYPE eType)
{
	this->eNeighbourType = eType;
	if ((eType == eINTERSECT) && this->pIntersection) 
	{
		if (this->pFinalPoly) delete this->pFinalPoly;
		this->pFinalPoly = new CXYZPolyLine;
		CXYZPoint p(this->pIntersection->getPoint1());
		p.setCovariance(this->pIntersection->getQxx1());
		this->pFinalPoly->addPoint(p);
		p = this->pIntersection->getPoint2();
		p.setCovariance(this->pIntersection->getQxx2());
		this->pFinalPoly->addPoint(p);
	}
};

//============================================================================

 void CBuildingFaceNeighbourRecord::setFacePolygon(CBuildingFacePoly *facePolygon)
 {
	 unsigned short reg = facePolygon->getFaceIndex();
	 
	 if (reg == this->region1) this->facePoly1 = facePolygon;
	 if (reg == this->region2) this->facePoly2 = facePolygon;
 };

 //============================================================================

void CBuildingFaceNeighbourRecord::replaceRegion(unsigned short regionOld, unsigned short regionNew)
{
	if (this->region1 == regionOld) this->region1 = regionNew;
	if (this->region2 == regionOld) this->region2 = regionNew;
	this->checkOrder();
};
//============================================================================

void CBuildingFaceNeighbourRecord::resetPolys()
{
	if (this->pInitialPoly)  delete this->pInitialPoly;
	this->pInitialPoly = 0;
	if (this->pApproximatePoly) delete this->pApproximatePoly;
	this->pApproximatePoly = 0;
	if (this->pFinalPoly) delete this->pFinalPoly;
	this->pFinalPoly = 0;
};

//============================================================================

void CBuildingFaceNeighbourRecord::resetIntersection()
{
	if (this->pIntersection) delete this->pIntersection;
	this->pIntersection = 0;
};

//============================================================================

eIntersect CBuildingFaceNeighbourRecord::initIntersection()
{
	if (this->pIntersection) delete this->pIntersection;
	this->pIntersection = 0;

	if ((!this->facePoly1) || (!this->facePoly2)) return eNoIntersection;

	C2DPoint p1(this->getInitialStartPt()), p2(this->getInitialEndPt());
	CBuildingFace *pPlane1 = this->facePoly1->getFace();
	CBuildingFace *pPlane2 = this->facePoly2->getFace();

	if (!pPlane1 || !pPlane2) return eNoIntersection;

	if ((pPlane1->getIndex() == 0) || (pPlane2->getIndex() == 0)) return eNoIntersection;

	C3DPoint red = pPlane1->getReduction();
	C3DPoint P1 (p1.x, p1.y, pPlane1->getHeight(p1.x + red.x, p1.y + red.y) - red.z);
	C3DPoint P2 (p2.x, p2.y, pPlane2->getHeight(p2.x + red.x, p2.y + red.y) - red.z);

	this->pIntersection = new CXYZHomoSegment(*pPlane1, *pPlane2, P1, P2);

	if (pPlane1->isParallel(*pPlane2) >= 0.0f) return eParallel;

	return eIntersection;
};

//============================================================================

void CBuildingFaceNeighbourRecord::cutOffIntersection()
{
	if (this->pIntersection)
	{
		C2DPoint p1(this->getInitialStartPt()), p2(this->getInitialEndPt());
		C3DPoint P1 (p1.x, p1.y, this->pIntersection->getPoint1().z);
		C3DPoint P2 (p2.x, p2.y, this->pIntersection->getPoint2().z);

		this->pIntersection->cutOff(P1, P2);
		
		if (this->pFinalPoly) delete this->pFinalPoly;
		this->pFinalPoly = new CXYZPolyLine;
		CXYZPoint p(this->pIntersection->getPoint1());
		p.setCovariance(this->pIntersection->getQxx1());
		this->pFinalPoly->addPoint(p);
		p = this->pIntersection->getPoint2();
		p.setCovariance(this->pIntersection->getQxx2());
		this->pFinalPoly->addPoint(p);
	};
};

//============================================================================

void CBuildingFaceNeighbourRecord::cutOffIntersection(const CXYZHomoSegment &intersection)
{
	if (this->pIntersection) delete this->pIntersection;

	this->pIntersection = new CXYZHomoSegment(intersection);

	this->cutOffIntersection();
};

//============================================================================

void CBuildingFaceNeighbourRecord::removeFromInitial(unsigned int indexFrom, 
													 unsigned int indexTo)
{
	this->pInitialPoly->removePoints(indexFrom, indexTo);
};

//============================================================================

void CBuildingFaceNeighbourRecord::initPolys(CXYContour *pContour, 
											 float sigmaContour, 
											 CTFW &trafo, 
											 const C3DPoint &reduction, 
											 float maxDist)
{
	this->resetPolys();
	
	this->pInitialPoly = pContour;

	for (int i = 0; i < this->pInitialPoly->getPointCount(); ++i)
	{
		C2DPoint *p = this->pInitialPoly->getPoint(i);
		C3DPoint P;
		trafo.transformObs2Obj(P, *p, 0.0);
		p->x = P.x - reduction.x;
		p->y = P.y - reduction.y;
	}

	this->pApproximatePoly = new CXYPolyLine;
	this->pApproximatePoly->initFromContour(this->pInitialPoly, maxDist, sigmaContour);
	this->pFinalPoly = new CXYZPolyLine;
	
	CXYZPoint p3D;
	
	for (int i = 0; i < this->pApproximatePoly->getPointCount(); ++i)
	{
		p3D.x = this->pApproximatePoly->getPoint(i).x;
		p3D.y = this->pApproximatePoly->getPoint(i).y;		
		this->pFinalPoly->addPoint(p3D);
	}
};


//============================================================================

double CBuildingFaceNeighbourRecord::getDistance(CBuildingFaceNeighbourRecord &other)
{
	double dist = -1.0;

	if (other.eNeighbourType == this->eNeighbourType)
	{
		CXYZPoint P1(this->getFinalStartPt());
		CXYZPoint P2(this->getFinalEndPt());
		CXYZPoint P1o(other.getFinalStartPt());
		CXYZPoint P2o(other.getFinalEndPt());
		

		if (this->eNeighbourType == eSTEPEDGE)
		{
			CXYPoint p1(P1.x, P1.y); CXYPoint p2(P2.x, P2.y);
			CXYLineSegment seg(p1, p2);
			p1.x = P1o.x;  p1.y = P1o.y;  
			p2.x = P2o.x;  p2.y = P2o.y;  

			dist = (seg.distanceFromSegment(p1) + seg.distanceFromSegment(p2)) * 0.5;
		}
		else
		{
			CXYZSegment seg(P1, P2);
			dist = (seg.getDistance(P1o) + seg.getDistance(P2o)) * 0.5;
		};
	};

	return dist;
};

//============================================================================

void CBuildingFaceNeighbourRecord::print(ostream &anOstream) const
{
	anOstream << this->getPrintString() << endl;
};

//============================================================================

void CBuildingFaceNeighbourRecord::exportDXF(ofstream &dxfFile, bool exportInitial, 
		                                     bool exportApprox, 
										     bool exportFinal) const
{
	CCharString str = CCharString(this->region1,3,true) + "_" + CCharString(this->region2,3,true);

	CCharString layer = "initial_" + str;
	if (this->pInitialPoly && exportInitial) this->pInitialPoly->writeDXF(dxfFile, layer.GetChar(), this->region1 % 10, 2);

	layer = "approx_" + str;
	if (this->pApproximatePoly && exportApprox) this->pApproximatePoly->writeDXF(dxfFile, layer.GetChar(), this->region1 % 10, 2);

	layer = "final_" + str;
	if (this->pFinalPoly && exportFinal) this->pFinalPoly->writeDXF(dxfFile, layer.GetChar(), this->region1 % 10, 2);
};


//============================================================================
	  