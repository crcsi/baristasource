/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */
#include <float.h>

#include "LineMatchingHypothesis.h"
#include "XYLineSegment.h"
#include "XYZLinesAdj.h"
#include "3DadjPlane.h"
#include "SensorModel.h"

//============================================================================

CLineMatchingHypothesis::CLineMatchingHypothesis(int nImages):
          imageEdges(nImages), indexVector(nImages), pObjectEdge(0), 
		  objectEdgeDescriptor(0), s0(0.0)
{
};
	  
//============================================================================

CLineMatchingHypothesis::CLineMatchingHypothesis(const CLineMatchingHypothesis &other):
          imageEdges(other.imageEdges), indexVector(other.indexVector), pObjectEdge(0), 
		  objectEdgeDescriptor(other.objectEdgeDescriptor), s0(other.s0)
{
	if (other.pObjectEdge) this->pObjectEdge = new CXYZHomoSegment(*other.pObjectEdge);
};
	
//============================================================================

CLineMatchingHypothesis::~CLineMatchingHypothesis()
{
	this->reset();
};
	
//============================================================================

void CLineMatchingHypothesis::reset()
{
	if (this->pObjectEdge) delete this->pObjectEdge;
	this->pObjectEdge = 0;
	for (unsigned int i = 0; i < this->imageEdges.size(); ++i)
	{
		this->imageEdges[i].clear();
		this->indexVector[i].clear();
	};
	this->objectEdgeDescriptor = 0;
	this->s0 = 0.0;
};

//============================================================================

C3DAdjustPlane *CLineMatchingHypothesis::getPlane(CSensorModel *model, double pixSize, 
												  const C2DPoint &imageOffset, double z, 
												  CXYLineSegment *segment2D,
												  CXYZPoint &P1, CXYZPoint &P2,
												  C3DPoint &reduction)
{
	C3DAdjustPlane *plane = new C3DAdjustPlane;

	CXYZPoint P3;
	CXYPoint p1 = segment2D->getStartPt();
	CXYPoint p2 = segment2D->getEndPt();
	p1.x += imageOffset.x;
	p1.y += imageOffset.y;
	p2.x += imageOffset.x;
	p2.y += imageOffset.y;
	model->transformObs2Obj(P1, p1, z, 0.01f);
	model->transformObs2Obj(P2, p2, z, 0.01f);
	model->transformObs2Obj(P3, p1, z + 1.0, 0.01f);
	P1.setSX(p1.getSX() * pixSize);
	P1.setSY(p1.getSY() * pixSize);
	P1.setSZ(0.01);
	P2.setSX(p1.getSX() * pixSize);
	P2.setSY(p1.getSY() * pixSize);
	P2.setSZ(0.01);
	P3.setSX(p2.getSX() * pixSize);
	P3.setSY(p2.getSY() * pixSize);
	P3.setSZ(0.01);

	plane->initFromPoints(P1, P2, P3, &reduction);

	P1.x -= reduction.x; P1.y -= reduction.y; P1.z -= reduction.z;
	P2.x -= reduction.x; P2.y -= reduction.y; P2.z -= reduction.z;

	return plane;
};

//=============================================================================
 
bool CLineMatchingHypothesis::intersectPlanes(const vector <C3DAdjustPlane *> &planes, 
											  CXYZHomoSegment *segment,
											  C3DPoint &reduction)
 {
	 bool ret = true;
	 double dRms = 1.0;
	 SymmetricMatrix Qxx;

	 CXYZPoint P1(segment->getPoint1());
	 P1 += reduction;
	 CXYZPoint P2(segment->getPoint2());
	 P2 += reduction;

	 double s0apriori = planes[0]->getSigmaDist(P1);

	 int it = 0;
	 int itMax = 15;

	 double distObs = segment->getLength();
	 double wDist = 10 * s0apriori;
	 double w = 1000 * s0apriori;

	 double pll = 10000.0, pvv;

	 while ((dRms > 0.001) && (it < itMax))
	 {
		 pvv = 0.0; 
		 C3DPoint dir = (C3DPoint) P1 - (C3DPoint) P2;
		 C3DAdjustPlane pl1(P1, dir, &reduction);
		 C3DAdjustPlane pl2(P2, dir, &reduction);

		 SymmetricMatrix N(6);
		 ColumnVector AtPl(6);
		 N = 0;
		 AtPl = 0.0;

		 for (unsigned int i = 0; i < planes.size(); ++i)
		 {
			 ColumnVector vec1(3); 
			 vec1.element(0) = planes[i]->getNormal().x; 
			 vec1.element(1) = planes[i]->getNormal().y; 
			 vec1.element(2) = planes[i]->getNormal().z; 
			 ColumnVector vec2(vec1);
			 double dist1 = planes[i]->getOrientedDistance(P1);
			 double dist2 = planes[i]->getOrientedDistance(P2);
			 double sig1 = planes[i]->getSigmaDist(P1);
			 double sig2 = planes[i]->getSigmaDist(P2);
			 if (sig1 < FLT_EPSILON) sig1 = 1.0;
			 if (sig2 < FLT_EPSILON) sig2 = 1.0;
			 double w1 = s0apriori / sig1;
			 double w2 = s0apriori / sig2;
			 vec1 *= w1;
			 vec2 *= w2;
			 double l1 = -dist1 * w1;
			 double l2 = -dist2 * w2;
			 pvv += dist1 * dist1 + dist2 * dist2; 

			 Matrix a1 = (vec1 * vec1.t());
			 Matrix a2 = (vec2 * vec2.t());
			 for (unsigned int row = 0; row < 3; ++row)
			 {
				 for (unsigned int col = row; col < 3; ++col)
				 {
					 N.element(row,     col)     += a1.element(row, col);
					 N.element(row + 3, col + 3) += a2.element(row, col);
				 }
				 AtPl.element(row)     += vec1.element(row) * l1; 
				 AtPl.element(row + 3) += vec2.element(row) * l2; 
			 }

		 }


		 ColumnVector vec1(3);
		 vec1.element(0)= dir.x * w; 
		 vec1.element(1)= dir.y * w; 
		 vec1.element(2)= dir.z * w; 
		 double dist1 = pl1.getOrientedDistance(P1);
		 double dist2 = pl2.getOrientedDistance(P2);
		 double l1 = -dist1 * w;	 
		 double l2 = -dist2 * w;
		 pvv += dist1 * dist1 + dist2 * dist2;

		 Matrix a1 = (vec1 * vec1.t());
		 for (unsigned int row = 0; row < 3; ++row)
		 {
			 for (unsigned int col = row; col < 3; ++col)
			 {
				 N.element(row,     col)     += a1.element(row, col);
				 N.element(row + 3, col + 3) += a1.element(row, col);
			 }
			 AtPl.element(row)     += vec1.element(row) * l1; 
			 AtPl.element(row + 3) += vec1.element(row) * l2; 
		 }

		 ColumnVector vec2(6); 
		 double oneByDist = wDist / distObs;
		 double l = distObs * wDist - dir.getSquareNorm() * oneByDist;
		 pvv += l * l;
		 dir *= oneByDist;
		 vec2.element(0) =  dir.x;   vec2.element(1) =  dir.y;   vec2.element(2) =  dir.z;
		 vec2.element(3) = -dir.x;   vec2.element(4) = -dir.y;   vec2.element(5) = -dir.z;

		 a1 = (vec2 * vec2.t());
		 for (unsigned int row = 0; row < 6; ++row)
		 {
			 for (unsigned int col = row; col < 6; ++col)
			 {
				 N.element(row, col) += a1.element(row, col);
			 }
			 AtPl.element(row)     += vec2.element(row) * l; 
		 }

		 double det = N.Determinant();
		 if (fabs(det) < FLT_EPSILON)
		 {
			 dRms = -1.0;
		 }
		 else
		 {
			 Qxx = N.i();
			 ColumnVector x = Qxx * AtPl;
			 P1.x += x.element(0); P1.y += x.element(1); P1.z += x.element(2); 
			 P2.x += x.element(3); P2.y += x.element(4); P2.z += x.element(5); 
			 if (it == 0)
			 {
				 dRms = 1.0;
			 }
			 else
			 {
				 dRms = fabs(pvv-pll)/(pvv + pll);
			 }
		 }

		 pll = pvv;
		 ++it;
	 } 
	
	 double m0;

	 if (dRms < 0.0) 
	 {
		 m0 = 5.0 * s0apriori;
	 }
	 else
	 {
		 m0 = pvv / 3.0;
		 P1 -= reduction;
		 P2 -= reduction;
		 Matrix *m1 = P1.getCovariance();
		 Matrix *m2 = P2.getCovariance();
		 for (unsigned int row = 0; row < 3; ++row)
		 {
			 for (unsigned int col = 0; col < 3; ++col)
			 {
				 m1->element(row, col) = Qxx.element(row,col) * m0;
				 m2->element(row, col) = Qxx.element(row + 3,col + 3) * m0;
			 }
		 }


		 *segment = CXYZHomoSegment(P1, P2);

		 m0 = sqrt(m0);
	 }

	 this->s0 = m0;
	 return ((m0 / s0apriori) < 1.5);
 };

//============================================================================

bool CLineMatchingHypothesis::computeIntersection(const vector<CSensorModel *> &sensorModels,		                      
												  const vector<double> &imagePixelSize,
												  const vector<C2DPoint> &imageOffsets,
												  C3DAdjustPlane *plane)
{
	C3DPoint reduction = plane->getReduction();
	double z = plane->getPoint().z + reduction.z;
	vector<CXYZHomoSegment *> segV;
	vector<C3DAdjustPlane *> plV;
	CXYZPoint P1, P2;
/*	unsigned int maxs = 0;
	for (unsigned int i = 0; i < this->imageEdges.size(); ++i)
	{
		if (this->imageEdges[i].size() > maxs) maxs = this->imageEdges[i].size();
	}
*/		
	for (unsigned int i = 0; i < this->imageEdges.size(); ++i)	
	{
			
		for (unsigned int j = 0; j < this->imageEdges[i].size(); ++j)
		{
			C3DAdjustPlane *pl = getPlane(sensorModels[i], imagePixelSize[i], imageOffsets[i], z, 
										 this->imageEdges[i][j], P1, P2, reduction);
			CXYZHomoSegment *seg = new CXYZHomoSegment(*pl, *plane, P1, P2);

			plV.push_back(pl);
			segV.push_back(seg);
		}
	}

	int OK = 1;

	CXYZHomoSegment *seg3D = new CXYZHomoSegment(*segV[0]);
	
	for (unsigned int i = 1; OK && (i < segV.size()); ++i)
	{
		CXYZSegment seg(*seg3D);
		OK = segV[i]->project(seg);
		if (OK) seg3D->mergeWithSegment(*segV[i]);
	}

	if (OK)
	{
		plV.push_back(plane);

		if (!intersectPlanes(plV, seg3D, reduction))
		{
			delete seg3D;
			seg3D = 0;
		}
	}
	else
	{
		delete seg3D;
		seg3D = 0;
	}

	for (unsigned int i = 0; i < segV.size(); ++i)
	{
		delete segV[i];
		delete plV[i];
	}

	if (this->pObjectEdge) delete this->pObjectEdge;
	this->pObjectEdge = seg3D;

	return (this->pObjectEdge != 0);
};

//============================================================================
	  
bool CLineMatchingHypothesis::overlaps(CLineMatchingHypothesis &other, double minPercentage) const
{
	bool overlaps = true;

	if (!this->pObjectEdge || !other.pObjectEdge) overlaps = false;
	else
	{
		CXYZSegment seg(*other.pObjectEdge);
	
		if (!this->pObjectEdge->project(seg)) overlaps = false;
		else 
		{
			double percentage1 = seg.getLength() / other.pObjectEdge->getLength();
			CXYZSegment seg1(*(this->pObjectEdge));
		
		//	if (!other.pObjectEdge->project(seg1)) overlaps = false;
			//else
			//{
			//	double percentage2 = seg1.getLength() / this->pObjectEdge->getLength();
			//	overlaps = (percentage1 > minPercentage) && (percentage2 > minPercentage);
			//}
		}
	}


	return overlaps;
};

//============================================================================

bool CLineMatchingHypothesis::isIdentical(CLineMatchingHypothesis &other, double minOverlapPercentag) const
{
	bool isIdentical = this->overlaps(other, minOverlapPercentag);

	if (isIdentical)
	{
		float metric = this->pObjectEdge->isIdenticalStraightLine(*other.pObjectEdge);
		if (metric < 0.0f) 
		{
			isIdentical = false;
		}
	}


	return isIdentical;
};

//============================================================================

void CLineMatchingHypothesis::merge(CLineMatchingHypothesis &other)
{
	for (unsigned int i = 0; i < other.imageEdges.size(); ++i)
	{
		for (unsigned int j = 0; j < other.imageEdges[i].size(); ++j)
		{
			bool contains = false;
			for (unsigned int k = 0; (k < this->imageEdges[i].size()) && (!contains); ++k)
			{
				if (this->imageEdges[i][k] == other.imageEdges[i][j]) contains = true;
			}
			if (!contains) 
			{
				this->imageEdges[i].push_back(other.imageEdges[i][j]);
				this->indexVector[i].push_back(other.indexVector[i][j]);
			}
		}
	}

	if (this->pObjectEdge || other.pObjectEdge)
	{
	
		if (!this->pObjectEdge) this->pObjectEdge = new CXYZHomoSegment(*other.pObjectEdge);
		else
		{
			if (other.pObjectEdge) this->pObjectEdge->mergeWithSegment(*other.pObjectEdge);
		}
	}
};

//============================================================================

void CLineMatchingHypothesis::print (ostream &anOstream, 
									 vector<ofstream *> *pDxfVec,
									 unsigned int index) const
{
	anOstream << "\n Candidate "; anOstream.width(3);
	anOstream << index << ":";
	CCharString s1(" ");
		
	for (unsigned int img = 0 ; img < this->indexVector.size(); ++img)
	{
		anOstream << s1.GetChar() << "image ";
		anOstream.width(2);
		anOstream << img << ": lines ";
		s1 = "\n                ";
				
		for (unsigned int ni = 0 ; ni < this->indexVector[img].size(); ++ni)
		{			
			anOstream << this->indexVector[img][ni] << ", ";
			if (pDxfVec)
			{
				CCharString Layer = "E_" + CCharString(this->indexVector[img][ni], 3, true);				
				this->imageEdges[img][ni]->writeDXF(*(*pDxfVec)[img],Layer.GetChar(), 1, 2, 1,-1);
			}
		}
	}
		
	anOstream << "\n                object edge: " 
		      << this->pObjectEdge->getString(" ", 10, 2) << ", s0: ";
	anOstream.setf(ios::fixed, ios::floatfield);
	anOstream.precision(3);
	anOstream << this->s0 << "\n";
						
};

//============================================================================

void CLineMatchingHypothesis::exportDXF(vector<ofstream*> &imgDxfVec, ofstream *dxfFile3D) const
{
};

//============================================================================
