
#include <strstream>
using namespace std;
#include <float.h>

#include "BuildingChangeEvaluator.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"
#include "filter.h"
#include "HugeImage.h"
#include "sortVector.h"
#include "DEM.h"
#include "histogramFlt.h"

const unsigned char CBuildingChangeEvaluator::Confirmed   =   0;
const unsigned char CBuildingChangeEvaluator::Changed     =  36;
const unsigned char CBuildingChangeEvaluator::NewSplit    =  73;
const unsigned char CBuildingChangeEvaluator::NewChanged  = 110;
const unsigned char CBuildingChangeEvaluator::NewOnly     = 146;
const unsigned char CBuildingChangeEvaluator::OldChanged  = 183;
const unsigned char CBuildingChangeEvaluator::OldOnly     = 219;
const unsigned char CBuildingChangeEvaluator::Background  = 255;

CBuildingChangeEvaluator::CBuildingChangeEvaluator(const CTFW &Trafo, 
												   float AreaThreshold,
												   const CCharString &Dir):
           labelsNew(0), labelsOld(0), pCounter(0), labelsDeleted(0),
		   labelsNewBuilt(0), labelsConfirmed(0), newState(0),
		   areaThreshold(AreaThreshold), projectDir(Dir + "\\")
{
	CSystemUtilities::mkdir( projectDir );

	trafo.copy(Trafo);
};
  
//============================================================================

CBuildingChangeEvaluator::~CBuildingChangeEvaluator ()
{
	clear();
	protHandler.close(protHandler.prt);
};

//============================================================================

bool CBuildingChangeEvaluator::init(CLabelImage *LabelsNew, CLabelImage *LabelsOld, 
									const char *protFileName)
{
	if (this->listener) 
	{
		this->listener->setTitleString("Initialising data ...");
		this->listener->setProgressValue(0.0);
	}

	this->labelsOld = LabelsOld;
	this->labelsNew = LabelsNew;
	protHandler.open(protHandler.prt, projectDir + protFileName, false);
	protHandler.print("\nBuilding change detection\n==========================\n", protHandler.prt + protHandler.scr);
	
	float areaFact = (float) (1.0 / getPixArea());
	ostrstream oss;
	
	oss << "\n" << (this->labelsOld->getNLabels() - 1) << " buildings found in existing data base";
	this->labelsOld->removeSmallLabels((unsigned long) (areaThreshold * areaFact), true);
	oss << " (" << (this->labelsOld->getNLabels() - 1) << " larger than " << areaThreshold<< " m^2).\n";

	oss << "\n" << (this->labelsNew->getNLabels() - 1) << " buildings found in \"new\" data set";
	this->labelsNew->removeSmallLabels((unsigned long) (areaThreshold * areaFact), true);
	oss << " (" << (this->labelsNew->getNLabels() - 1) << " larger than " << areaThreshold<< " m^2).\n\n";
	protHandler.print (oss, protHandler.prt + protHandler.scr);

	OverlapsDatasetOld.setLength(this->labelsOld->getNLabels() - 1);	
	OverlapsDatasetNew.setLength(this->labelsNew->getNLabels() - 1);

	pixelEvaluation.set(0, 0, this->getWidth(), this->getHeight(), 1,8,true,0.0);

	this->labelsOld->dump(projectDir.GetChar(), "Old","",true);
	this->labelsNew->dump(projectDir.GetChar(), "New","",true);
	if (this->listener) 
	{
		this->listener->setProgressValue(100.0);
	}

	return true;
};

//============================================================================

C2DPoint CBuildingChangeEvaluator::getPixSize() const
{
	C2DPoint pixSize(fabs(trafo.getA()), fabs(trafo.getE()));
	return pixSize;
};

//============================================================================
  
C2DPoint CBuildingChangeEvaluator::getShift() const
{
	return trafo.getShift();
};

//============================================================================
  
double CBuildingChangeEvaluator::getPixArea() const
{
	C2DPoint pixS = getPixSize();
	return fabs(pixS.x * pixS.y);
}

//============================================================================

void CBuildingChangeEvaluator::initPixResults()
{
	if (pCounter) delete pCounter;
	long nNewLabelSize = this->labelsNew->getNLabels() + 1;
	long nOldLabelSize = this->labelsOld->getNLabels() + 1;

	pCounter = new CImageBuffer(0,0, nOldLabelSize, nNewLabelSize,1,16,true,0);

	unsigned long nPixBB = 0;    
	unsigned long nPixBN = 0;    
	unsigned long nPixNB = 0;    
	unsigned long nBpixNew = 0;    
	unsigned long nBpixOld = 0;    

	for (unsigned long index = 0; index < pixelEvaluation.getNGV(); ++ index)
	{
		unsigned short newLabel = this->labelsNew->getLabel(index);
		unsigned short oldLabel = this->labelsOld->getLabel(index);
		if (newLabel || oldLabel)
		{
			unsigned long counterIndex = newLabel * nOldLabelSize + oldLabel;
			++(pCounter->pixUShort(counterIndex));

			if (newLabel && oldLabel)
			{
				pixelEvaluation.pixUChar(index) = Confirmed;
				++nPixBB;
				++nBpixNew;
				++nBpixOld;
			}
			else if (newLabel)
			{
				++nPixBN;
				++nBpixNew;
				pixelEvaluation.pixUChar(index) = NewOnly;
			}
			else
			{
				++nPixNB;
				++nBpixOld;
				pixelEvaluation.pixUChar(index) = OldOnly;
			};
		}
		else 
		{
			pixelEvaluation.pixUChar(index) = Background;   
		}; 
	};

	ostrstream oss;
	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(1);
	oss << "\nNumber of building pixels (existing data base):                               ";
	oss.width(10);
	oss << nBpixOld << "\nNumber of building pixels (new dataset):                                      ";
	oss.width(10);
	oss << nBpixNew << "\nNumber of building pixels found in both datasets:                             ";
	oss.width(10);
	oss << nPixBB   << "\nNumber of new building pixels not available in the existing data base:        ";
	oss.width(10);
	oss << nPixBN   << "\nNumber of building pixels in the existing data base not found in the new set: ";
	oss.width(10);
	oss << nPixNB   << "\nNumber of non-building pixels in both data sets:                              ";
	oss.width(10);
	oss <<  pixelEvaluation.getNGV() - (nPixBB + nPixNB + nPixBN) << "\n\n" << ends;

	protHandler.print(oss, protHandler.prt + protHandler.scr);

	for (unsigned long index = 0; index < OverlapsDatasetNew.getLength(); ++ index)
	{
		NewDistribution (index + 1, OverlapsDatasetNew.getRecordByRegion(index + 1));
	};

	for (unsigned long index = 0; index < OverlapsDatasetOld.getLength(); ++ index)
	{
		OldDistribution (index + 1, OverlapsDatasetOld.getRecordByRegion(index + 1));
	};

	for (int r = 0; r < this->labelsNew->getHeight(); ++r)
	{
		unsigned long uMin = r * this->labelsNew->getDiffY();
		for (int c = 0; c < this->labelsNew->getWidth(); ++c)
		{
			unsigned short label = this->labelsNew->getLabel(uMin + c);
		}
	}

	for (int r = 0; r < this->labelsOld->getHeight(); ++r)
	{
		unsigned long uMin = r * this->labelsOld->getDiffY();
		for (int c = 0; c < this->labelsOld->getWidth(); ++c)
		{
			unsigned short labelOld = this->labelsOld->getLabel(uMin + c);
			unsigned short labelNew = this->labelsNew->getLabel(uMin + c);
			if (labelOld > 0)
			{
				OverlapsDatasetOld.getRecordByRegion(labelOld).checkMinMax(r,c);
			}
			if (labelNew > 0)
			{
				OverlapsDatasetNew.getRecordByRegion(labelNew).checkMinMax(r,c);
			}
		}
	}

	CLookupTable lut = evalLUT();
	CCharString fn = this->projectDir + "changesPix.tif";
	pixelEvaluation.exportToTiffFile(fn.GetChar(), &lut, false);
};

//============================================================================

void CBuildingChangeEvaluator::NewDistribution(unsigned long NewIndex, 
											   CMultipleLabelOverlapRecord &NewVector)
{ 
	if (pCounter)
	{
		unsigned long u = NewIndex * pCounter->getWidth();
		unsigned long u1 = 0;
		unsigned long max = u + pCounter->getWidth();
		float pArea = (float) getPixArea();
		float area  = float(labelsNew->getNPixels(NewIndex - 1)) * pArea;

		NewVector.reset(NewIndex, area);

		for(;u < max; ++u, ++u1)
		{
			unsigned long npixels = pCounter->pixUShort(u);

			if (npixels > 0)
			{
				NewVector.add(u1, npixels);
			};
		};

		NewVector.finish();


	} 
	else
	{
		NewVector.clear();
	};
};

//============================================================================

void CBuildingChangeEvaluator::OldDistribution(unsigned long OldIndex, 
											   CMultipleLabelOverlapRecord &OldVector)
{
	if (pCounter)
	{
		unsigned long u = OldIndex;
		unsigned long u1 = 0;
		float pArea = (float) getPixArea();
		float area  = float(labelsOld->getNPixels(OldIndex - 1)) * pArea;

		OldVector.reset(OldIndex, area);

		for(;u < pCounter->getNGV(); ++u1, u += pCounter->getWidth())
		{
			unsigned long npixels = pCounter->pixUShort(u);

			if (npixels > 0)
			{
				OldVector.add(u1, npixels);
			};
		};

		OldVector.finish();
	} 
	else
	{
		OldVector.clear();
	};
};

//============================================================================

void CBuildingChangeEvaluator::clear()
{
	if (pCounter)        delete pCounter;
	if (labelsDeleted)   delete labelsDeleted;
	if (labelsNewBuilt)  delete labelsNewBuilt;
	if (labelsConfirmed) delete labelsConfirmed;
	if (newState)        delete newState;

	pCounter        = 0;
	labelsDeleted   = 0;
	labelsNewBuilt  = 0;
	labelsConfirmed = 0;
	newState        = 0;
};
 
//============================================================================

void CBuildingChangeEvaluator::writeData(CCharString filename) const
{
	if (filename.IsEmpty()) filename = this->projectDir + "OverlapsAll.dat";
	writeData(OverlapsDatasetOld, OverlapsDatasetNew, "", filename);
};

//============================================================================
 
void CBuildingChangeEvaluator::writeData(const CLabelOverlapVector &OldVector, 
										 const CLabelOverlapVector &NewVector, 
										 CCharString heading, CCharString filename) const
{
	if (filename.IsEmpty()) this->projectDir + "OverlapsPercentage.dat";

	ofstream outfile (filename.GetChar());  
	outfile.setf(ios::fixed, ios::floatfield);
	outfile.precision(1);
	CCharString ActualHeading = CCharString("Distribution of old buildings ") + heading + ":";

	OldVector.writeData(outfile, ActualHeading);

	ActualHeading = CCharString("Distribution of new buildings ") + heading + ":";

	NewVector.writeData(outfile, ActualHeading); 
};

//============================================================================
 
void CBuildingChangeEvaluator::compareCurrent(CCharString fileBasis, 
											  float noneVsWeak, 
											  float weakVsPartial, 
											  float partialVsStrong, 
											  float cellSize, float maxSize)
{
	ostrstream oss;
	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(1);

	oss << "\nThresholds for overlap criterion: \n  None    vs Weak [%]:      ";
	oss.width(6);
	oss << noneVsWeak * 100 << "\n  Weak    vs Partial [%]:   ";
	oss.width(6);
	oss << weakVsPartial * 100 << "\n  Partial vs Strong [%]:    ";
	oss.width(6);
	oss << partialVsStrong * 100 << "\n" << ends;

	protHandler.print(oss, protHandler.prt + protHandler.scr);

	CLabelOverlapVector new2(OverlapsDatasetNew);
	CLabelOverlapVector old2(OverlapsDatasetOld);

	old2.classifyPart(noneVsWeak, weakVsPartial, partialVsStrong);
	new2.classifyPart(noneVsWeak, weakVsPartial, partialVsStrong);
	new2.classifyPart(noneVsWeak, weakVsPartial, partialVsStrong);

	if (outputMode >= eMAXIMUM) 
	{
		writeTIFFOld(old2, fileBasis + "old.tif");
		writeData(old2, new2, "", fileBasis + "All.dat");
	}

	unsigned long removed = 0;

	unsigned long maxold = old2.getLength()+1;
	for (unsigned long i = 1; i < maxold; ++i)
	{
		for (unsigned long j = 0; j < old2.getRecordByRegion(i).nOverlaps(); ++j)
		{
			unsigned long newIndex = old2.getRecordByRegion(i).getRegionIndex(j);
			eOVERLAP eOldClass = old2.getRecordByRegion(i).getClassification(j);

			if ((eOldClass == eNONE) || (eOldClass == eWEAK))
			{
				CLabelOverlapIndicator newIndicator;
				int ind = new2.getRecordByRegion(newIndex).getOverlapIndicator(old2.getRecordByRegion(i).getIndex(), newIndicator);

				eOVERLAP eNewClass = newIndicator.getClassification();
				if ((eNewClass == eNONE) || (eNewClass == eWEAK))
				{
					old2.getRecordByRegion(i).removeElement(j);
					--j;
					new2.getRecordByRegion(newIndex).removeElement(ind);
					++removed;
				};
			}; 
		};

		old2.getRecordByRegion(i).sort();
		old2.getRecordByRegion(i).classifyTotal(noneVsWeak, weakVsPartial, partialVsStrong);
	};

	new2.sortComponents();
	new2.classifyTotal(noneVsWeak, weakVsPartial, partialVsStrong);

	old2.printHistogram("\nHistograms for existing data base:", cellSize, maxSize, false);
//	old2.printCoverageHistogram("\nHistogram of coverages for old set:");

	new2.printHistogram("\nHistograms for new data set:", cellSize, maxSize, false);

	writeTIFFOld(old2, fileBasis + "Old_withoutMarginal.tif");
	writeTIFFNew(new2, fileBasis + "New_withoutMarginal.tif");

	ostrstream ss;
	ss.setf(ios::fixed, ios::floatfield); 
	ss.precision(1);
	ss << " (Area threshold: "     << areaThreshold 
	   << " m^2, NONE vs WEAK: "   << noneVsWeak * 100.0f 
	   << "%, WEAK vs PARTIAL: "   << weakVsPartial * 100.0f 
	   << "%, PARTIAL vs STRONG: " << partialVsStrong * 100.0f << "%)" << ends;

	char *zstr = ss.str();
	CCharString str(zstr);
	delete [] zstr;

	if (outputMode >= eMAXIMUM) writeData(old2, new2, str, fileBasis + "Percentage.dat");

	OverlapsDatasetNew = new2;
	OverlapsDatasetOld = old2;
};

 
//============================================================================
 
void CBuildingChangeEvaluator::improveSegmentation(float noneVsWeak, 
												   float weakVsPartial, 
												   float partialVsStrong, 
												   float toleranceSplit, 
												   float cellSize, float maxSize,
												   bool mergeNew)
{
	initPixResults();

	CCharString fileBasis = this->projectDir + CSystemUtilities::dirDelimStr + "Overlap_";
	compareCurrent(fileBasis, noneVsWeak, weakVsPartial, partialVsStrong, 
				   cellSize, maxSize);


	this->detectOneToNull();
	this->detectNullToOne();
	this->detectOneToOne();
	this->detectOneToM();
	this->detectMToOne();
	this->detectMToN();	

	reportResults(false, false, false, false, false, true, false, true, false);
	

	this->clarifyMToN(toleranceSplit);
	this->clarifyMtoOne(toleranceSplit);

	this->labelsNew->initPixelNumbers();
	this->labelsNew->renumberLabels();
	this->labelsNew->updateVoronoi();
	
	OverlapsDatasetNew.setLength(this->labelsNew->getNLabels() - 1);

	initPixResults();

	compareCurrent(fileBasis, noneVsWeak, weakVsPartial, partialVsStrong, 
				   cellSize, maxSize);

    this->detectOneToNull();
	this->detectNullToOne();
	this->detectOneToOne();
	this->detectOneToM();
	this->detectMToOne();
	this->detectMToN();

	reportResults(false, false, false, false, false, true, true, true, false);

	this->simplifyOnetoM(toleranceSplit);
	this->simplifyContained();

	if (mergeNew)
	{
		for (unsigned int i = 0; i < this->oneToMOld.size(); ++i)
		{
			unsigned short label = this->oneToMNew[i][0];
			for (unsigned long u = 0; u < this->labelsNew->getNGV(); ++u)
			{
				for (unsigned int j = 1; j < this->oneToMNew[i].size(); ++j)
				{
					if (this->labelsNew->getLabel(u) == oneToMNew[i][j])
					{
						this->labelsNew->pixUShort(u) = label;
					}
				}
			}
		}
	}

	this->labelsNew->initPixelNumbers();
	this->labelsNew->renumberLabels();
	this->labelsNew->updateVoronoi();
	this->labelsNew->dump(projectDir.GetChar(), "New_improved","",true);
}

//============================================================================
 
void CBuildingChangeEvaluator::compare(float noneVsWeak, float weakVsPartial, 
									   float partialVsStrong, 
									   float toleranceSplit,
									   float tolerance,
									   float cellSize, float maxSize, bool mergeNew)
{
	CCharString fileBasis = this->projectDir + CSystemUtilities::dirDelimStr + "Overlap_";

	improveSegmentation(noneVsWeak, weakVsPartial, partialVsStrong, 
		                toleranceSplit, cellSize, maxSize, mergeNew);

	OverlapsDatasetNew.setLength(this->labelsNew->getNLabels() - 1);

	initPixResults();

	compareCurrent(fileBasis, noneVsWeak, weakVsPartial, partialVsStrong, 
				   cellSize, maxSize);

	this->detectOneToOne();
	this->detectOneToM();
	this->detectMToOne();
	this->detectMToN();

	reportResults(false, false, true, false, false, true, true, true, false);

	if (this->labelsDeleted) delete this->labelsDeleted;
	this->labelsDeleted = new CLabelImage(this->labelsNew->getOffsetX(),this->labelsNew->getOffsetY(),this->labelsNew->getWidth(),this->labelsNew->getHeight(), 8);

	if (this->labelsNewBuilt) delete this->labelsNewBuilt;
	this->labelsNewBuilt = new CLabelImage(this->labelsNew->getOffsetX(),this->labelsNew->getOffsetY(),this->labelsNew->getWidth(),this->labelsNew->getHeight(), 8);

	if (this->labelsConfirmed) delete this->labelsConfirmed;
	this->labelsConfirmed = new CLabelImage(this->labelsNew->getOffsetX(),this->labelsNew->getOffsetY(),this->labelsNew->getWidth(),this->labelsNew->getHeight(), 8);

	this->detectOneToNull();
	this->detectNullToOne();
}

int CBuildingChangeEvaluator::getConfirmedIndex(int label) const
{
	for (unsigned int i = 0; i < this->confirmedBuildings.size(); ++i)
	{
		if (this->confirmedBuildings[i] == label) return i;
	}

	return -1;
};

int CBuildingChangeEvaluator::getChangedIndex(int label) const
{
	for (unsigned int i = 0; i < this->changedBuildings.size(); ++i)
	{
		if (this->changedBuildings[i] == label) return i;
	}

	return -1;
};

int CBuildingChangeEvaluator::getNewIndex(int label) const
{
	for (unsigned int i = 0; i < this->newBuildings.size(); ++i)
	{
		if (this->newBuildings[i] == label) return i;
	}

	return -1;
};

int CBuildingChangeEvaluator::getDeletedIndex(int label) const
{
	for (unsigned int i = 0; i < this->deletedBuildings.size(); ++i)
	{
		if (this->deletedBuildings[i] == label) return i;
	}

	return -1;
};

	 
void CBuildingChangeEvaluator::computeQualityPars(unsigned long oldTP, unsigned long newTP, 
												  unsigned long FP, unsigned long FN,
												  float &completeness, float &correctness, 
												  float &quality)
{
	float denCompl = float(oldTP + FN);
	float denCorr  = float(newTP + FP);

	if (denCompl > FLT_EPSILON) completeness = float(100.0) * oldTP / denCompl;
	else completeness = -1;
	
	if (denCorr > FLT_EPSILON) correctness = float(100.0) * newTP / denCorr;
	else correctness = -1;
		
	if (oldTP == newTP)
	{
		float denQual = denCompl + float(FP);
		if (denQual > FLT_EPSILON) quality = float(100.0) * newTP / denQual;
		else quality = -1;
	}
	else
	{
		if ((completeness < -0.5f) || (correctness < -0.5f)) quality = -1;
		else if ((completeness < FLT_EPSILON) || (correctness < FLT_EPSILON)) quality = 0.0;
		else quality = float(100.0 / ((100.0 / completeness) +  (100.0 / correctness) - 1.0));
	}	
};

void CBuildingChangeEvaluator::evaluateResults(CDEM *DSM, CDEM *DTM, float noneVsWeak, float weakVsPartial, 									 
											   float partialVsStrong, 
											   float toleranceSplit,
											   float tolerance,
											   float cellSizeArea,   float maxSizeArea,
											   float cellSizeHeight, float maxSizeHeight)
{
	if (this->listener) 
	{
		this->listener->setTitleString("Evaluation of Building Detection ...");
		this->listener->setProgressValue(0.0);
	}

	this->compare(noneVsWeak, weakVsPartial, partialVsStrong, toleranceSplit, tolerance, cellSizeArea, maxSizeArea, true);

	protHandler.close(protHandler.prt);
	protHandler.open(protHandler.prt, projectDir + "Evaluate.prt", false);
	protHandler.print("\nError evaluation\n================\n", protHandler.prt + protHandler.scr);

	reportResults(true, true, true, false, false, false, false, false, false);

	vector<vector<float> > oldDSMHeights(this->labelsOld->getNLabels());
	vector<float> oldDTMHeights(this->labelsOld->getNLabels(), FLT_MAX);
	vector<float> oldAreas(this->labelsOld->getNLabels(), 0.0);

	vector<vector<float> > newDSMHeights(this->labelsNew->getNLabels());
	vector<float> newDTMHeights(this->labelsNew->getNLabels(), FLT_MAX);
	vector<float> newAreas(this->labelsNew->getNLabels(), 0.0);

	unsigned long TP = 0, FN = 0, FP = 0;

	C2DPoint delta(fabs(this->trafo.getA()), fabs(this->trafo.getE())); 
	C3DPoint pPix(0, 0, 0), pObj;

	this->trafo.transformObs2Obj(pObj, pPix);
	double xMin = pObj.x;
	double y = pObj.y;
	
	unsigned long indexMax = this->labelsOld->getIndex(this->labelsOld->getHeight() - 1, this->labelsOld->getWidth());

	for (unsigned long index0 = 0; index0 < indexMax; index0 += this->labelsOld->getDiffY(), y -= delta.y)
	{
		unsigned long indexRowMax = index0 + this->labelsOld->getDiffY();
		double x = xMin;
		
		for (unsigned long index = index0; index < indexRowMax; ++index, x += delta.x)
		{
			unsigned short newlabel = this->labelsNew->getLabel(index);
			unsigned short oldlabel = this->labelsOld->getLabel(index);

			if (newlabel || oldlabel)
			{
				float zDSM = DSM ? (float) DSM->getBLInterpolatedHeight (x, y) : -9999.0f;
				float zDTM = DTM ? (float) DTM->getBLInterpolatedHeight (x, y) : -9999.0f;
				
				if (newlabel)
				{
					if (oldlabel) ++TP; 
					else ++FP;
				
					if ((zDTM > -9999.0) && (zDTM < newDTMHeights[newlabel])) newDTMHeights[newlabel] = zDTM;
					if (zDSM > -9999.0) newDSMHeights[newlabel].push_back(zDSM);
					newAreas[newlabel]++;
				}

				if (oldlabel)
				{
					if (!newlabel) ++FN;

					if ((zDTM > -9999.0) && (zDTM < oldDTMHeights[oldlabel])) oldDTMHeights[oldlabel] = zDTM;
					if (zDSM  > -9999.0) oldDSMHeights[oldlabel].push_back(zDSM);
					oldAreas[oldlabel]++;
				}
			}
		}
	}

	float pixArea = float(delta.x * delta.y);

	vector<float> falseNegativeHeights(this->deletedBuildings.size(), 0);
	vector<float> falseNegativeAreas(falseNegativeHeights);
	vector<float> falsePositiveHeights(this->newBuildings.size(), 0);
	vector<float> falsePositiveAreas(falsePositiveHeights);
	vector<float> truePositiveOldHeights(this->oneToOneNew.size(), 0);
	vector<float> truePositiveOldAreas(truePositiveOldHeights);
	vector<float> truePositiveNewHeights(this->oneToOneNew.size(), 0);
	vector<float> truePositiveNewAreas(truePositiveNewHeights);

	for (unsigned int i = 0; i < this->deletedBuildings.size(); ++i)
	{
		int idx = this->deletedBuildings[i];
		int l = oldDSMHeights[idx].size();
		if (l  > 0)
		{
			falseNegativeHeights[i] = oldDSMHeights[idx][l / 2] - oldDTMHeights[idx];
		}
		falseNegativeAreas[i] = oldAreas[idx] * pixArea;
	}

	for (unsigned int i = 0; i < this->newBuildings.size(); ++i)
	{
		int idx = this->newBuildings[i];
		int l = newDSMHeights[idx].size();
		if (l  > 0)
		{
			falsePositiveHeights[i] = newDSMHeights[idx][l / 2] - newDTMHeights[idx];
		}

		falsePositiveAreas[i] = newAreas[idx] * pixArea;
	}

	for (unsigned int i = 0; i < this->oneToOneNew.size(); ++i)
	{
		int idxNew = this->oneToOneNew[i];
		int idxOld = this->oneToOneOld[i];
		int l = newDSMHeights[idxNew].size();
		if (l  > 0)
		{
			truePositiveNewHeights[i] = newDSMHeights[idxNew][l / 2] - newDTMHeights[idxNew];
		}
		
		l = oldDSMHeights[idxOld].size();
		if (l  > 0)
		{
			truePositiveOldHeights[i] = oldDSMHeights[idxOld][l / 2] - oldDTMHeights[idxOld];
		}

		truePositiveNewAreas[i] = newAreas[idxNew] * pixArea;
		truePositiveOldAreas[i] = oldAreas[idxOld] * pixArea;
	}


	CHistogramFlt histAreaTPNew(0.0, maxSizeArea, cellSizeArea);
	CHistogramFlt histAreaTPOld(0.0, maxSizeArea, cellSizeArea);
	CHistogramFlt histAreaFP   (0.0, maxSizeArea, cellSizeArea);
	CHistogramFlt histAreaFN   (0.0, maxSizeArea, cellSizeArea);
	CHistogramFlt histArea     (0.0, maxSizeArea, cellSizeArea);

	CHistogramFlt histHeightTPNew(0.0, maxSizeHeight, cellSizeHeight);
	CHistogramFlt histHeightTPOld(0.0, maxSizeHeight, cellSizeHeight);
	CHistogramFlt histHeightFP   (0.0, maxSizeHeight, cellSizeHeight);
	CHistogramFlt histHeightFN   (0.0, maxSizeHeight, cellSizeHeight);
	CHistogramFlt histHeight     (0.0, maxSizeHeight, cellSizeHeight);
	
	for (unsigned int i = 0; i < falseNegativeHeights.size(); ++i)
	{
		histHeightFN.addValue(falseNegativeHeights[i]);
		histAreaFN.addValue(falseNegativeAreas[i]);
		histHeight.addValue(falseNegativeHeights[i]);
		histArea.addValue(falseNegativeAreas[i]);
	}

	for (unsigned int i = 0; i < falsePositiveHeights.size(); ++i)
	{
		histHeightFP.addValue(falsePositiveHeights[i]);
		histAreaFP.addValue(falsePositiveAreas[i]);
	}

	for (unsigned int i = 0; i < truePositiveNewHeights.size(); ++i)
	{
		histHeightTPNew.addValue(truePositiveNewHeights[i]);
		histAreaTPNew.addValue(truePositiveNewAreas[i]);

		histHeightTPOld.addValue(truePositiveOldHeights[i]);
		histAreaTPOld.addValue(truePositiveOldAreas[i]);

		histHeight.addValue(truePositiveOldHeights[i]);
		histArea.addValue(truePositiveOldAreas[i]);
	}

	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(1);

	oss <<"\n\n Threshold for classification as a true positive (Partial Vs Weak): " << weakVsPartial * 100.0 << "%";

	oss << "\n\nHistogram of area coverage of all existing buildings:\n=====================================================\n\n" 
		<< histArea.getString("Area[m^2]", 0, ",")	
		<< "\n\nHistogram of area coverage of true positives (Old):\n===================================================\n\n" 
		<< histAreaTPOld.getString("Area[m^2]", 1, ",")
		<< "\n\nHistogram of area coverage of true positives (New):\n===================================================\n\n" 
		<< histAreaTPNew.getString("Area[m^2]", 1, ",")
		<< "\n\nHistogram of area coverage of false negatives:\n==============================================\n\n" 
		<< histAreaFN.getString("Area[m^2]", 1, ",")
		<< "\n\nHistogram of area coverage of false positives:\n==============================================\n\n" 
		<< histAreaFP.getString("Area[m^2]", 1, ",");

	if (DSM && DTM)
	{
		oss << "\n\nHistogram of heights of false positives:\n========================================\n\n" 
		    << histHeightFP.getString("Height[m]", 1, ",")	
		    << "\n\nHistogram of heights of all existing buildings:\n===============================================\n\n" 
		    << histHeight.getString("Height[m]", 1, ",")
		    << "\n\nHistogram of heights of true positives (Old):\n=============================================\n\n" 
		    << histHeightTPOld.getString("Height[m]", 1, ",")	
		    << "\n\nHistogram of heights of true positives (New):\n=============================================\n\n" 
		    << histHeightTPNew.getString("Height[m]", 1, ",")	
		    << "\n\nHistogram of heights of false negatives:\n========================================\n\n" 
		    << histHeightFN.getString("Height[m]", 1, ",")
		    << "\n\nHistogram of heights of false positives:\n========================================\n\n" 
		    << histHeightFP.getString("Height[m]", 1, ",");	
	}

	vector<float> completenessArea(histArea.getSize());
	vector<float> correctnessArea(histArea.getSize());
	vector<float> qualityArea(histArea.getSize());
	vector<float> completenessHeight(histHeight.getSize());
	vector<float> correctnessHeight(histHeight.getSize());
	vector<float> qualityHeight(histHeight.getSize());
	vector<float> completenessAreaCum(histArea.getSize());
	vector<float> correctnessAreaCum(histArea.getSize());
	vector<float> qualityAreaCum(histArea.getSize());
	vector<float> completenessHeightCum(histHeight.getSize());
	vector<float> correctnessHeightCum(histHeight.getSize());
	vector<float> qualityHeightCum(histHeight.getSize());

	for (unsigned int i = 0; i < completenessArea.size(); ++i)
	{
		computeQualityPars(histAreaTPOld.value(i), histAreaTPNew.value(i), 
			               histAreaFP.value(i), histAreaFN.value(i),
						   completenessArea[i], correctnessArea[i], qualityArea[i]); 
	}

	for (unsigned int i = 0; i < completenessHeight.size(); ++i)
	{
		computeQualityPars(histHeightTPOld.value(i), histHeightTPNew.value(i), 
			               histHeightFP.value(i), histHeightFN.value(i),
						   completenessHeight[i], correctnessHeight[i], qualityHeight[i]); 
	}

	float pixCompl, pixCorr, pixQult;
	
	computeQualityPars(TP, TP, FP, FN, pixCompl, pixCorr, pixQult); 
	
	unsigned long tpCumOld = histHeightTPOld.value(histHeightTPOld.getSize() - 1);
	unsigned long tpCumNew = histHeightTPNew.value(histHeightTPNew.getSize() - 1);
	unsigned long fpCum    = histHeightFP.value(histHeightFP.getSize() - 1);
	unsigned long fnCum    = histHeightFN.value(histHeightFN.getSize() - 1);
	
	computeQualityPars(tpCumOld, tpCumNew, fpCum, fnCum,
					   completenessHeightCum[histHeightTPOld.getSize() - 1], 
					   correctnessHeightCum[histHeightTPOld.getSize() - 1], 
					   qualityHeightCum[histHeightTPOld.getSize() - 1]); 

	for (int i = int(histHeightTPOld.getSize()) - 2; i >= 0; --i)
	{
		tpCumOld += histHeightTPOld.value(i);
		tpCumNew += histHeightTPNew.value(i);
		fpCum    += histHeightFP.value(i);
		fnCum    += histHeightFN.value(i);

		computeQualityPars(tpCumOld, tpCumNew, fpCum, fnCum, completenessHeightCum[i], 
			               correctnessHeightCum[i], qualityHeightCum[i]); 
	}


	tpCumOld = histAreaTPOld.value(histAreaTPOld.getSize() - 1);
	tpCumNew = histAreaTPNew.value(histAreaTPNew.getSize() - 1);
	fpCum = histAreaFP.value(histAreaTPOld.getSize() - 1);
	fnCum = histAreaFN.value(histAreaTPOld.getSize() - 1);

	computeQualityPars(tpCumOld, tpCumNew, fpCum, fnCum,
					   completenessAreaCum[histAreaTPOld.getSize() - 1], 
					   correctnessAreaCum[histAreaTPOld.getSize() - 1], 
					   qualityAreaCum[histAreaTPOld.getSize() - 1]); 
	
	for (int i = int(histAreaTPOld.getSize()) - 2; i >= 0; --i)
	{
		tpCumOld += histAreaTPOld.value(i);
		tpCumNew += histAreaTPNew.value(i);
		fpCum    += histAreaFP.value(i);
		fnCum    += histAreaFN.value(i);

		computeQualityPars(tpCumOld, tpCumNew, fpCum, fnCum, completenessAreaCum[i], 
			               correctnessAreaCum[i], qualityAreaCum[i]); 
	}

	oss.precision(1);

	oss << "\n\nEvaluation depending on the building area:\n==========================================\n\n" 
		<< getQualityString("Area [m^2]", 0, ",", histArea, completenessArea, correctnessArea, qualityArea)
		<< "\n\nEvaluation depending on the building area (cumulative):\n=======================================================\n\n" 
		<< getQualityString("Area [m^2]", 0, ",", histArea, completenessAreaCum, correctnessAreaCum, qualityAreaCum);
	
	if (DSM && DTM)
	{
		oss << "\n\nEvaluation depending on the building height:\n============================================\n\n" 
		    << getQualityString("Height [m]", 1, ",", histHeight, completenessHeight, correctnessHeight, qualityHeight)
		    << "\n\nEvaluation depending on the building height (cumulative):\n=========================================================\n\n" 
		    << getQualityString("Height [m]", 1, ",", histHeight, completenessHeightCum, correctnessHeightCum, qualityHeightCum);
	}
	
	oss	<< "\n\nEvaluation on a per-pixel level:\n================================\n\n  Completeness:  "
		<< pixCompl <<"\n  Correcteness:  " << pixCorr <<"\n  Quality:       " << pixQult << "\n\n" << ends;

	protHandler.print(oss, protHandler.prt + protHandler.scr);
}
	 
CCharString CBuildingChangeEvaluator::getQualityString(const CCharString &heading, 
													   int headingPrecision, 
													   const CCharString &separator,
													   const CHistogramFlt &histo,
													   vector<float> completeness,
													   vector<float> correctness,
													   vector<float> quality)
{
	int colWidth = 4;
	int headingWidth = heading.GetLength();
	if (headingWidth < 12) headingWidth = 12;

	CCharString headStr = histo.getDescriptorString(heading, headingPrecision, separator, colWidth);

	ostrstream oss, ossComp, ossCorr, ossQlty; 
	ossComp.setf(ios::fixed, ios::floatfield);
	ossComp.precision(1);
	ossCorr.setf(ios::fixed, ios::floatfield);
	ossCorr.precision(1);
	ossQlty.setf(ios::fixed, ios::floatfield);
	ossQlty.precision(1);
	
	if (headingWidth > heading.GetLength()) 
	{
		oss.width(headingWidth > heading.GetLength());
		oss << " ";
	}

	oss << headStr.GetChar() << ends;
	char *z = oss.str();
	headStr = z;
	delete [] z;
	CCharString str = headStr;

	ossComp << "Completeness" << separator;
	ossCorr << "Correctness " << separator;
	ossQlty << "Quality     " << separator;
	
	for (unsigned int i = 0; i < completeness.size(); ++i)
	{
		ossComp.width(colWidth);
		ossCorr.width(colWidth);
		ossQlty.width(colWidth);
		if (completeness[i] >= 0.0) ossComp << completeness[i];
		else ossComp << " ";
		if (correctness[i] >= 0.0) ossCorr << correctness[i];
		else ossCorr << " ";
		if (quality[i] >= 0.0) ossQlty << quality[i];
		else ossQlty << " ";

		ossComp << separator;
		ossCorr << separator;
		ossQlty << separator;
	}

	ossComp << "\n" << ends;
	ossCorr << "\n" << ends;
	ossQlty << "\n" << ends;
	
	z =  ossComp.str();
	str += z;
	delete [] z;
	z =  ossCorr.str();
	str += z;
	delete [] z;
	z =  ossQlty.str();
	str += z;
	delete [] z;

	return str;
};

void CBuildingChangeEvaluator::detectChanges(float noneVsWeak, float weakVsPartial, 
									   float partialVsStrong, 
									   float toleranceSplit,
									   float tolerance,
									   float cellSize, float maxSize)
{
	if (this->listener) 
	{
		this->listener->setTitleString("Building Change Detection ...");
		this->listener->setProgressValue(0.0);
	}

	CCharString fileBasis = this->projectDir + CSystemUtilities::dirDelimStr + "Overlap_";


	this->compare(noneVsWeak, weakVsPartial, partialVsStrong, toleranceSplit, tolerance, cellSize, maxSize, false);

	for (unsigned long u = 0; u < labelsDeleted->getNGV(); ++ u)
	{
		unsigned short label = this->labelsOld->getLabel(u);
		if (label)
		{
			bool isDel = false;
			for (unsigned int j = 0; j < this->deletedBuildings.size() && !isDel; ++j)
			{
				if (deletedBuildings[j] == label) isDel = true;
			}
			if (isDel) labelsDeleted->pixUShort(u) = label;
			else labelsDeleted->pixUShort(u) = 0;
		}
		else labelsDeleted->pixUShort(u) = 0;
	}

	vector <unsigned short> newBuildingsNew(this->newBuildings);

	for (unsigned long u = 0; u < labelsNewBuilt->getNGV(); ++ u)
	{
		unsigned short label = this->labelsNew->getLabel(u);
		if (label)
		{
			bool isNew = false;
			int k = 0;
			for (unsigned int j = 0; j < this->newBuildings.size() && !isNew; ++j)
			{
				if (newBuildings[j] == label) 
				{
					isNew = true;
					k = this->labelsOld->getNLabels() + j;
					newBuildingsNew[j] = k;
				}
			}
			if (isNew) labelsNewBuilt->pixUShort(u) = (unsigned short) (k);
			else labelsNewBuilt->pixUShort(u) = 0;
		}
		else labelsNewBuilt->pixUShort(u) = 0;
	}

	this->detectConfirmed(tolerance);

	this->newBuildings = newBuildingsNew;


	labelsDeleted->dump(this->projectDir.GetChar(), "deleted");
	labelsNewBuilt->dump(this->projectDir.GetChar(), "newBuilt");
	labelsConfirmed->dump(this->projectDir.GetChar(), "confirmed");


	for (unsigned long index = 0; index < pixelEvaluation.getNGV(); ++index)
	{
		unsigned short newLabel = this->labelsNewBuilt->getLabel(index);
		unsigned short delLabel = this->labelsDeleted->getLabel(index);
		unsigned short conLabel = this->labelsConfirmed->getLabel(index);
		unsigned short oldLabel = this->labelsOld->getLabel(index);

		if (conLabel) 
		{
			bool isConfirmed = false;
			for (unsigned int i = 0; i < confirmedBuildings.size() && !isConfirmed; ++i)
			{
				if (confirmedBuildings[i] == conLabel) isConfirmed = true; 
			}

			if (isConfirmed) pixelEvaluation.pixUChar(index) = Confirmed;
			else if (conLabel == oldLabel) pixelEvaluation.pixUChar(index) = Changed;
			else pixelEvaluation.pixUChar(index) = NewSplit;
		}
		else if (newLabel)
		{
			if (newLabel >= this->labelsOld->getNLabels()) 
			{
				if (newLabel < this->labelsOld->getNLabels() + this->newBuildings.size())
					pixelEvaluation.pixUChar(index) = NewOnly;
				else pixelEvaluation.pixUChar(index) = NewChanged;
			}
			else pixelEvaluation.pixUChar(index) = NewChanged;
		}
		else if (delLabel)
		{
			bool isDel = false;
			for (unsigned int j = 0; j < this->deletedBuildings.size() && !isDel; ++j)
			{
				if (deletedBuildings[j] == delLabel) isDel = true;
			}
			if (isDel) pixelEvaluation.pixUChar(index) = OldOnly;
			else pixelEvaluation.pixUChar(index) = OldChanged;
		}
		else 
		{
			pixelEvaluation.pixUChar(index) = Background;   
		}; 
	};

	CLookupTable lut = evalLUT();
	CCharString fn = this->projectDir + "changes.tif";
	pixelEvaluation.exportToTiffFile(fn.GetChar(), &lut, false);

	if (this->newState) delete this->newState;
	this->newState = new CLabelImage (*this->labelsConfirmed);

	for (unsigned long i = 0; i < this->labelsConfirmed->getNGV(); ++i)
	{
		unsigned short label = this->labelsNewBuilt->getLabel(i);
		if (label) this->newState->pixUShort(i) = label;
	}

	this->newState->initPixelNumbers();
	this->newState->dump(this->projectDir.GetChar(), "newState8Bit");

	reportResults(true, true, false, true, true, false, false, false, true);

	if (this->listener) 
	{
		this->listener->setProgressValue(100.0);
	}
};

bool CBuildingChangeEvaluator::dumpPixResults(const char *FileName, CHugeImage *hug) const
{
	CLookupTable lut = evalLUT();
	return hug->dumpImage(FileName, pixelEvaluation, &lut);
};

bool CBuildingChangeEvaluator::dumpNewState(const char *FileName, CHugeImage *hug) const
{
	CLookupTable lut = this->newState->get16BitLookup();
	return hug->dumpImage(FileName, *this->newState, &lut);
};
	  
bool CBuildingChangeEvaluator::computeConfusionMatrix(CHugeImage *confusion, 
													  CHugeImage *changes, 
													  CHugeImage *changesReference) 
{
	bool ret = true;

	if ((changes->getWidth()  != changesReference->getWidth()) || 
		(changes->getHeight() != changesReference->getHeight()) || 
		(changes->getBands() > 1) || (changesReference->getBands() > 1) ||
		(changes->getBpp() != 8)  || (changesReference->getBpp() != 8)) ret = false;
	else
	{
		if (confusion) ret = confusion->prepareWriting(confusion->getFileName()->GetChar(), 
			                            changes->getWidth(), changes->getHeight(), 
										changes->getBpp(), changes->getBands());

		if (ret)
		{
			unsigned long cooccurrence[8][8];
			unsigned char greyVal[8][8];
			int coocSize = 8;
			unsigned char gv0 = 0;

			for (int i = 0; i < coocSize; ++i, gv0 += 32)
			{
				for (int j = 0; j < coocSize; ++j)
				{
					cooccurrence[i][j] = 0;
					greyVal[i][j] = gv0 + (unsigned char) j;
				}
			}
			unsigned char cooccurrenceLut[256];
			unsigned int i = 0;
			for (; i < Changed; ++i)    cooccurrenceLut[i] = 0;
			for (; i < NewSplit; ++i)   cooccurrenceLut[i] = 1;
			for (; i < NewChanged; ++i) cooccurrenceLut[i] = 2;
			for (; i < NewOnly; ++i)    cooccurrenceLut[i] = 3;
			for (; i < OldChanged; ++i) cooccurrenceLut[i] = 4;
			for (; i < OldOnly; ++i)    cooccurrenceLut[i] = 5;
			for (; i < Background; ++i) cooccurrenceLut[i] = 6;
			for (; i < 256; ++i)        cooccurrenceLut[i] = 7;

			CImageBuffer tile(0, 0, changes->getTileWidth(), changes->getTileHeight(), changes->getBands(), 
				              changes->getBpp(), true);

			for(int TR = 0; TR < changes->getHeight(); TR += changes->getTileHeight())
			{
				for (int TC = 0; TC < changes->getWidth(); TC += changes->getTileWidth())
				{
					unsigned char *changesBuf    = changes->getTile(TR, TC);
					unsigned char *changesRefBuf = changesReference->getTile(TR, TC);
					if (!changesBuf || !changesRefBuf) ret = false;
					else
					{
						long cMax = tile.getWidth();
						long rMax = tile.getHeight();
						if (TC + cMax > changes->getWidth())  cMax = changes->getWidth()  - TC;
						if (TR + rMax > changes->getHeight()) rMax = changes->getHeight() - TR;

						for (unsigned long r = 0; r < (unsigned long) rMax; r++)
						{
							unsigned long c0 = r * tile.getDiffY();
							for (unsigned long c = 0; c < (unsigned long) cMax; c++)
							{
								unsigned long idx = c0 + c;
								unsigned char chgCl = cooccurrenceLut[changesBuf[idx]];
								unsigned char refCl = cooccurrenceLut[changesRefBuf[idx]];
								cooccurrence[refCl][chgCl] += 1;
								tile.pixUChar(idx) = greyVal[refCl][chgCl];
							}
						}					

						if (confusion) ret &= confusion->dumpTile(tile);
					}
				}
			}

			if (confusion) ret &= confusion->finishWriting();

			if (ret)
			{
				ostrstream oss; 
				oss.setf(ios::fixed, ios::floatfield);
				oss << "\nConfusion Matrix:\n=================\n       , Confirmed (automatic), "
					<< "Changed (automatic), New/Split (automatic), New/Changed (automatic), "
					<< "New (automatic), Old/Changed (automatic),  Old (automatic), Background (automatic)\n"
					<< "Confirmed (reference) ,";
				for (int j = 0; j < coocSize; ++j)
				{
					oss.width(10); oss << cooccurrence[0][j] << ",";
				}
				oss << "\nChanged (reference) ,";
				for (int j = 0; j < coocSize; ++j)
				{
					oss.width(10); oss << cooccurrence[1][j] << ",";
				}
				oss << "\nNew/Split (reference) ,";
				for (int j = 0; j < coocSize; ++j)
				{
					oss.width(10); oss << cooccurrence[2][j] << ",";
				}
				oss << "\nNew/Changed (reference) ,";
				for (int j = 0; j < coocSize; ++j)
				{
					oss.width(10); oss << cooccurrence[3][j] << ",";
				}
				oss << "\nNew (reference) ,";
				for (int j = 0; j < coocSize; ++j)
				{
					oss.width(10); oss << cooccurrence[4][j] << ",";
				}
				oss << "\nOld/Changed (reference) ,";
				for (int j = 0; j < coocSize; ++j)
				{
					oss.width(10); oss << cooccurrence[5][j] << ",";
				}
				oss << "\nOld (reference) ,";
				for (int j = 0; j < coocSize; ++j)
				{
					oss.width(10); oss << cooccurrence[6][j] << ",";
				}
				oss << "\nBackground(reference) ,";
				for (int j = 0; j < coocSize; ++j)
				{
					oss.width(10); oss << cooccurrence[7][j] << ",";
				}
				oss << "\n";
				protHandler.print(oss,protHandler.prt);
			}
		}
	}

	return ret;
};

//============================================================================
  
void CBuildingChangeEvaluator::writeTIFFOld(CLabelOverlapVector &record, 
                                            CCharString filename) const
{
	CImageBuffer localBuf(this->pixelEvaluation, false);
	localBuf.setAll(0.0);

	for (unsigned long index = 0; index < localBuf.getNGV(); ++index)
	{
		unsigned short newLabel = this->labelsNew->getLabel(index);
		unsigned short oldLabel = this->labelsOld->getLabel(index);
		if (oldLabel)
		{
			if (!newLabel) localBuf.pixUChar(index) = 255;
			else if (record.getRecordByRegion(oldLabel).nOverlaps() == 0)
				localBuf.pixUChar(index) = 254;
			else localBuf.pixUChar(index) = (unsigned char) record.getRecordByRegion(oldLabel).nOverlaps();
		};
	};

	CLookupTable lut = splitMergeOverlapLUT();

	localBuf.exportToTiffFile(filename.GetChar(),&lut,false);

	for (unsigned long index = 0; index < localBuf.getNGV(); ++index)
	{
		unsigned short newLabel = this->labelsNew->getLabel(index);
		unsigned short oldLabel = this->labelsOld->getLabel(index);
		if (oldLabel)
		{
			eOVERLAP eClass = record.getRecordByRegion(oldLabel).getClassification();
			switch ( eClass)
			{
				case eSTRONG:  localBuf.pixUChar(index) = 4; break;
				case ePARTIAL: localBuf.pixUChar(index) = 3; break;
				case eWEAK:    localBuf.pixUChar(index) = 2; break;
				case eNONE:    localBuf.pixUChar(index) = 1; break;
				default:       localBuf.pixUChar(index) = 0; break;
			};
		}
		else localBuf.pixUChar(index) = 0;
	};

	CFileName file(filename);
	file.SetFileName(file.GetFileName() + "_Class");
	localBuf.exportToTiffFile(file.GetChar(),&lut,false);
};
 
//============================================================================
  
void CBuildingChangeEvaluator::writeTIFFNew(CLabelOverlapVector &record, 
                                            CCharString filename) const
{
	CImageBuffer localBuf(this->pixelEvaluation, false);
	localBuf.setAll(0.0);

	for (unsigned long index = 0; index < localBuf.getNGV(); ++index)
	{
		unsigned short newLabel = this->labelsNew->getLabel(index);
		unsigned short oldLabel = this->labelsOld->getLabel(index);
		if ((newLabel) && (newLabel < record.getLength()))
		{
			if (!oldLabel) localBuf.pixUChar(index) = 255;
			else if (record.getRecordByRegion(newLabel).nOverlaps() == 0)
				localBuf.pixUChar(index)= 254;   
			else localBuf.pixUChar(index) = (unsigned char) record.getRecordByRegion(newLabel).nOverlaps();
		};
	};

	CLookupTable lut = splitMergeOverlapLUT();

	localBuf.exportToTiffFile(filename.GetChar(),&lut,false);

	for (unsigned long index = 0; index < localBuf.getNGV(); ++index)
	{
		unsigned short newLabel = this->labelsNew->getLabel(index);
		unsigned short oldLabel = this->labelsOld->getLabel(index);
		if ((newLabel) && (newLabel < record.getLength()))
		{
			eOVERLAP eClass = record.getRecordByRegion(newLabel).getClassification();
			switch ( eClass)
			{
				case eSTRONG:  localBuf.pixUChar(index) = 4; break;
				case ePARTIAL: localBuf.pixUChar(index) = 3; break;
				case eWEAK:    localBuf.pixUChar(index) = 2; break;
				case eNONE:    localBuf.pixUChar(index) = 1; break;
				default:       localBuf.pixUChar(index) = 0; break;
			};
		}
		else localBuf.pixUChar(index) = 0;
	};

	CFileName file(filename);
	file.SetFileName(file.GetFileName() + "_Class");
	localBuf.exportToTiffFile(file.GetChar(),&lut,false);
};

//============================================================================
  
CLookupTable CBuildingChangeEvaluator::evalLUT()
{
	CLookupTable lut(8, 3);
	lut.setColor(220, 220,   0, Confirmed);
	lut.setColor(255, 255, 128, Changed);
	lut.setColor(255,   0, 255, NewSplit);
	lut.setColor(  0, 220,   0, NewChanged);
	lut.setColor(255,   0,   0, NewOnly);
	lut.setColor(180, 180, 255, OldChanged);
	lut.setColor(  0,   0, 255, OldOnly);
	lut.setColor(255, 255, 255, Background);

	return lut;
}

CLookupTable CBuildingChangeEvaluator::splitMergeOverlapLUT()
{
	CLookupTable lut(8, 3);
	lut.setColor(255, 255, 255, 0);
	lut.setColor(  0,   0, 255, 1);
	lut.setColor(  0, 255,   0, 2);
	lut.setColor(255,   0,   0, 3);
	lut.setColor(255, 255,   0, 4);
	lut.setColor(  0, 255, 255, 5);
	lut.setColor(255,   0, 255, 6);

	for (int i = 7; i < lut.getMaxEntries(); ++i)
	{
		lut.setColor(255,   0, 255, i);
	};

	lut.setColor(128,   0,   0, 254);
	lut.setColor(  0,   0,   0, 255);

	return lut;
};
	 
void CBuildingChangeEvaluator::reportResults(bool oneToNull, bool nullToOne, bool oneToOne, 
											 bool confirmed, bool changed,   bool NtoOne, 
											 bool oneToN,    bool NtoM,		 bool split)
{
	float areaFact = (float) getPixArea();


	if (oneToOne)
	{
		ostrstream oss;
		oss.setf(ios::fixed,ios::floatfield);
		oss.precision(1);

		oss << "\nBuildings with a correspondence of 1 : 1 (old / new):\n=====================================================\n";
		
		for (unsigned int i = 0; i < oneToOneOld.size(); ++i)
		{
			oss.width(5); 
			oss << oneToOneOld[i] << " / ";			
			oss.width(5); 
			oss << oneToOneNew[i] << " ";
			if (!(i % 10)) oss << "\n";
		}

		oss << "\n\nAltogether " << oneToOneOld.size()  << " buildings found to be 1 : 1.\n" << ends;
		protHandler.print (oss, protHandler.prt + protHandler.scr);
	}

	if (oneToN)
	{
		ostrstream oss;
		oss.setf(ios::fixed,ios::floatfield);
		oss.precision(1);
		oss << "\nBuildings with a correspondence of 1 : M (old : new):\n=====================================================\n";
		
		for (unsigned int i = 0; i < oneToMNew.size(); ++i)
		{		
			oss.width(5); 
			oss << oneToMOld[i] << ": ";

			vector<unsigned short> newLabels = oneToMNew[i];

			for (unsigned int j = 0; j < newLabels.size(); ++j)
			{
				oss.width(5); 
				oss << newLabels[j];
				if (j != newLabels.size() - 1)  oss << " / ";
			}

			oss << "\n";
		}

		oss << "\n\nAltogether " << oneToMOld.size() << " buildings found to be 1 : M.\n" << ends;
		protHandler.print (oss, protHandler.prt + protHandler.scr);
	}

	if (NtoOne)
	{
		ostrstream oss;
		oss.setf(ios::fixed,ios::floatfield);
		oss.precision(1);
		oss << "\nBuildings with a correspondence of M : 1 (new : old):\n=====================================================\n";
		
		for (unsigned int i = 0; i < OverlapsDatasetNew.getLength(); ++i)
		{
			if (OverlapsDatasetNew.getRecordByIndex(i).nOverlaps() > 1)
			{
				unsigned long newIndex = OverlapsDatasetNew.getRecordByIndex(i).getIndex();
				bool isMToOne = true;

				for (unsigned int j = 0; j < OverlapsDatasetNew.getRecordByIndex(i).nOverlaps() && isMToOne; ++j)
				{
					unsigned long oldIndex = OverlapsDatasetNew.getRecordByIndex(i).getRegionIndex(j);
					if (OverlapsDatasetOld.getRecordByRegion(oldIndex).nOverlaps() != 1)
					{
						isMToOne = false;
					}
					else
					{
						if (OverlapsDatasetOld.getRecordByRegion(oldIndex).getRegionIndex(0) != newIndex)
						{
							isMToOne = false;
						}
					}
				}
				if (isMToOne)
				{
					vector<unsigned short> oldLabels;

					MToOneNew.push_back((unsigned short) newIndex);
					oss.width(5); oss << newIndex << ": ";

					for (unsigned int j = 0; j < OverlapsDatasetNew.getRecordByIndex(i).nOverlaps(); ++j)
					{
						unsigned long oldIndex = OverlapsDatasetNew.getRecordByIndex(i).getRegionIndex(j);
						oldLabels.push_back((unsigned short) oldIndex);
						oss.width(5); oss << oldIndex;
						if (j != OverlapsDatasetNew.getRecordByIndex(i).nOverlaps() - 1)  oss << " / ";
					}
					MToOneOld.push_back(oldLabels);
					oss << "\n";
				}
			}
		}

		oss << "\n\nAltogether " << MToOneNew.size() << " buildings found to be M : 1.\n" << ends;
		protHandler.print (oss, protHandler.prt + protHandler.scr);
	}

	if (NtoM)
	{
		ostrstream oss;
		oss.setf(ios::fixed,ios::floatfield);
		oss.precision(1);

		oss << "\nBuildings with a correspondence of M : N (old : new):\n=====================================================\n";

		for (unsigned int i = 0; i < MToNOld.size(); ++i)
		{
			vector<unsigned short> oldLabels = MToNOld[i];
			vector<unsigned short> newLabels = MToNNew[i];
			
			oss << "\n ";
			for (unsigned int j = 0; j < oldLabels.size(); ++j)
			{
				oss.width(5);
				oss << oldLabels[j];
				if (j !=  oldLabels.size() - 1) oss << " / ";
			};
			oss << " : ";
			for (unsigned int j = 0; j < newLabels.size(); ++j)
			{
				oss.width(5);
				oss << newLabels[j];
				if (j !=  newLabels.size() - 1) oss << " / ";
			};	
		}

		oss << "\n\nAltogether " << MToNOld.size() << " tuple"; 
		if (MToNOld.size() != 1) oss << "s";
		oss << " of buildings found to be M : N.\n" << ends;
		protHandler.print (oss, protHandler.prt + protHandler.scr);
	}

	if (oneToNull)
	{
		float totalArea = 0.0;

		ostrstream oss;
		oss.setf(ios::fixed,ios::floatfield);
		oss.precision(1);

		oss << "\nList of deleted buildings:\n==========================\n";
		
		for (unsigned int i = 0; i < deletedBuildings.size(); ++i)
		{
			float area = float(this->labelsOld->getNPixels(deletedBuildings[i])) * areaFact;
			oss.width(5); 
			oss << deletedBuildings[i] << " (" << area << ")\n";	
			totalArea += area;
		}

		oss << "\n\nNumber of deleted buildings: " << deletedBuildings.size() 
			<< "; Total area: " << totalArea << " [m^2].\n" << ends;

		protHandler.print (oss, protHandler.prt + protHandler.scr);
	}

	if (nullToOne)
	{
		float totalArea = 0.0;

		CLabelImage *plabelImg = this->labelsNew;
		
		for (unsigned int i = 0; i < newBuildings.size(); ++i)
		{
			if (newBuildings[i] > this->labelsNew->getNLabels()) plabelImg = this->newState;
		}

		ostrstream oss;
		oss.setf(ios::fixed,ios::floatfield);
		oss.precision(1);

		oss << "\n\nList of new buildings:\n======================\n";	

		for (unsigned int i = 0; i < newBuildings.size(); ++i)
		{
			float area = float(plabelImg->getNPixels(newBuildings[i])) * areaFact;
			oss.width(5); 
			oss << newBuildings[i] << " (" << area << ")\n";		
			totalArea += area;
		}

		oss << "\n\nNumber of new buildings: " << newBuildings.size() 
			<< "; Total area: " << totalArea << " [m^2].\n" << ends;
		protHandler.print (oss, protHandler.prt + protHandler.scr);
	}

	if (confirmed)
	{		
		ostrstream oss;
		oss.setf(ios::fixed,ios::floatfield);
		oss.precision(1);
		oss << "\n\nList of confirmed buildings:\n============================\n";
	
		for (unsigned int i = 0; i < confirmedBuildings.size(); ++i)
		{
			float area = float(labelsOld->getNPixels(confirmedBuildings[i])) * areaFact;
			
			oss.width(5); 
			oss << confirmedBuildings[i] << " (" << area << ")\n";	
		}
	
		oss << "\n\nNumber of confirmed buildings: " << this->confirmedBuildings.size() << ends;
		protHandler.print (oss, protHandler.prt + protHandler.scr);

	}

	if (changed)
	{
		ostrstream oss;
		oss.setf(ios::fixed,ios::floatfield);
		oss.precision(1);
		oss << "\n\nList of changed buildings:\n==========================\n";
		for (unsigned int i = 0; i < changedBuildings.size(); ++i)
		{
			float area = float(labelsOld->getNPixels(changedBuildings[i])) * areaFact;
			oss.width(5); 
			oss << changedBuildings[i] << " (" << area << ")\n";	
		}

		oss << "\n\nNumber of changed buildings: " << this->changedBuildings.size() << ends;
		protHandler.print (oss, protHandler.prt + protHandler.scr);
	}

	if (split)
	{
		ostrstream oss;
		oss.setf(ios::fixed,ios::floatfield);
		oss.precision(1);
		oss << "\n\nList of new buildings split off from existing buildings:\n========================================================\n";
		for (unsigned int i = 0; i < newBuildingsSplit.size(); ++i)
		{
			if (!(i % 10))  oss << "\n";
			oss.width(5); 
			oss << newBuildingsSplit[i] << " ";	
		}

		oss << "\n\nNumber of new buildings split off from existing ones: " << this->newBuildingsSplit.size() << ends;
		protHandler.print (oss, protHandler.prt + protHandler.scr);
	}
};

void CBuildingChangeEvaluator::detectOneToNull()
{
	this->deletedBuildings.clear();

	for (unsigned int i = 0; i < OverlapsDatasetOld.getLength(); ++i)
	{
		if (OverlapsDatasetOld.getRecordByIndex(i).nOverlaps() == 0)
		{
			int index = OverlapsDatasetOld.getRecordByIndex(i).getIndex();
			deletedBuildings.push_back(index);
		}
	}
}
	
void CBuildingChangeEvaluator::detectNullToOne()
{
	this->newBuildings.clear();

	for (unsigned int i = 0; i < OverlapsDatasetNew.getLength(); ++i)
	{
		if (OverlapsDatasetNew.getRecordByIndex(i).nOverlaps() == 0)
		{
			int index = OverlapsDatasetNew.getRecordByIndex(i).getIndex();
			newBuildings.push_back(index);
		}
	}
};

void CBuildingChangeEvaluator::detectOneToOne()
{
	oneToOneOld.clear();
	oneToOneNew.clear();

	for (unsigned int i = 0; i < OverlapsDatasetOld.getLength(); ++i)
	{
		if (OverlapsDatasetOld.getRecordByIndex(i).nOverlaps() == 1)
		{
			unsigned long oldIndex = OverlapsDatasetOld.getRecordByIndex(i).getIndex();
			unsigned long newIndex = OverlapsDatasetNew.getRecordByRegion(OverlapsDatasetOld.getRecordByIndex(i).getRegionIndex(0)).getIndex();
			if (OverlapsDatasetNew.getRecordByRegion(newIndex).nOverlaps() == 1)
			{
				if (OverlapsDatasetNew.getRecordByRegion(newIndex).getRegionIndex(0) == oldIndex)
				{
					oneToOneOld.push_back((unsigned short) oldIndex);
					oneToOneNew.push_back((unsigned short) newIndex);
				}
			}
		}
	}
};

void CBuildingChangeEvaluator::detectOneToM()
{
	oneToMOld.clear();
    oneToMNew.clear();
	for (unsigned int i = 0; i < OverlapsDatasetOld.getLength(); ++i)
	{
		if (OverlapsDatasetOld.getRecordByIndex(i).nOverlaps() > 1)
		{
			unsigned long oldIndex = OverlapsDatasetOld.getRecordByIndex(i).getIndex();
			bool isOneToM = true;

			for (unsigned int j = 0; j < OverlapsDatasetOld.getRecordByIndex(i).nOverlaps() && isOneToM; ++j)
			{
				unsigned long newIndex = OverlapsDatasetOld.getRecordByIndex(i).getRegionIndex(j);
				if (OverlapsDatasetNew.getRecordByRegion(newIndex).nOverlaps() != 1)
				{
					isOneToM = false;
				}
				else
				{
					if (OverlapsDatasetNew.getRecordByRegion(newIndex).getRegionIndex(0) != oldIndex)
					{
						isOneToM = false;
					}
				}
			}
			if (isOneToM)
			{
				oneToMOld.push_back((unsigned short) oldIndex);

				vector<unsigned short> newLabels;

				for (unsigned int j = 0; j < OverlapsDatasetOld.getRecordByIndex(i).nOverlaps(); ++j)
				{
					unsigned long newIndex = OverlapsDatasetOld.getRecordByIndex(i).getRegionIndex(j);
					newLabels.push_back((unsigned short) newIndex);
				}

				oneToMNew.push_back(newLabels);
			}
		}
	}
};

void CBuildingChangeEvaluator::detectMToOne()
{
	MToOneNew.clear();
	MToOneOld.clear();
	
	for (unsigned int i = 0; i < OverlapsDatasetNew.getLength(); ++i)
	{
		if (OverlapsDatasetNew.getRecordByIndex(i).nOverlaps() > 1)
		{
			unsigned long newIndex = OverlapsDatasetNew.getRecordByIndex(i).getIndex();
			bool isMToOne = true;

			for (unsigned int j = 0; j < OverlapsDatasetNew.getRecordByIndex(i).nOverlaps() && isMToOne; ++j)
			{
				unsigned long oldIndex = OverlapsDatasetNew.getRecordByIndex(i).getRegionIndex(j);
				if (OverlapsDatasetOld.getRecordByRegion(oldIndex).nOverlaps() != 1)
				{
					isMToOne = false;
				}
				else
				{
					if (OverlapsDatasetOld.getRecordByRegion(oldIndex).getRegionIndex(0) != newIndex)
					{
						isMToOne = false;
					}
				}
			}
			if (isMToOne)
			{
				vector<unsigned short> oldLabels;

				MToOneNew.push_back((unsigned short) newIndex);

				for (unsigned int j = 0; j < OverlapsDatasetNew.getRecordByIndex(i).nOverlaps(); ++j)
				{
					unsigned long oldIndex = OverlapsDatasetNew.getRecordByIndex(i).getRegionIndex(j);
					oldLabels.push_back((unsigned short) oldIndex);
				}
				MToOneOld.push_back(oldLabels);
			}
		}
	}
};

void CBuildingChangeEvaluator::detectMToN()
{
	MToNOld.clear();
	MToNNew.clear();

	for (unsigned int i = 0; i < OverlapsDatasetNew.getLength(); ++i)
	{
		if (OverlapsDatasetNew.getRecordByIndex(i).nOverlaps() > 1)
		{
			unsigned long newIndex = OverlapsDatasetNew.getRecordByIndex(i).getIndex();
			bool isMToOne = true;

			for (unsigned int j = 0; j < OverlapsDatasetNew.getRecordByIndex(i).nOverlaps() && isMToOne; ++j)
			{
				unsigned long oldIndex = OverlapsDatasetNew.getRecordByIndex(i).getRegionIndex(j);
				if (OverlapsDatasetOld.getRecordByRegion(oldIndex).nOverlaps() != 1)
				{
					isMToOne = false;
				}
				else
				{
					if (OverlapsDatasetOld.getRecordByRegion(oldIndex).getRegionIndex(0) != newIndex)
					{
						isMToOne = false;
					}
				}
			}

			if (!isMToOne)
			{
				vector<unsigned short> oldlab, newlab;
				newlab.push_back((unsigned short) newIndex);
			
				for (unsigned int j = 0; j < OverlapsDatasetNew.getRecordByIndex(i).nOverlaps() ; ++j)
				{
					unsigned long oldIndex = OverlapsDatasetNew.getRecordByIndex(i).getRegionIndex(j);
					bool found = false;

					for (unsigned int k = 0; k < oldlab.size() && !found; ++k)
					{
						found = oldlab[k] == oldIndex;
					}
					if (!found) oldlab.push_back((unsigned short) oldIndex);
			
					for (unsigned int k = 0; k < OverlapsDatasetOld.getRecordByRegion(oldIndex).nOverlaps() ; ++k)
					{
						unsigned long newIndex1 = OverlapsDatasetOld.getRecordByRegion(oldIndex).getRegionIndex(k);
						found = false;
						for (unsigned int l = 0; l < newlab.size() && !found; ++l)
						{
							found = newlab[l] == newIndex1;
						}
						if (!found) newlab.push_back((unsigned short) newIndex1);
			
					}
				}

				MToNOld.push_back(oldlab);
				MToNNew.push_back(newlab);
			}
		}
	}
};

void CBuildingChangeEvaluator::extractLabels(const vector<unsigned short> &oldlabels, 
											 const vector<unsigned short> &newlabels, 
		                                     CLabelImage &oldL, CLabelImage &newL)
{
	int rmin = OverlapsDatasetOld.getRecordByRegion(oldlabels[0]).getRmin();
	int cmin = OverlapsDatasetOld.getRecordByRegion(oldlabels[0]).getCmin();
	int rmax = OverlapsDatasetOld.getRecordByRegion(oldlabels[0]).getRmax();
	int cmax = OverlapsDatasetOld.getRecordByRegion(oldlabels[0]).getCmax();

	for (unsigned int j = 1; j < oldlabels.size(); ++j)
	{
		int r = OverlapsDatasetOld.getRecordByRegion(oldlabels[j]).getRmin();
		int c = OverlapsDatasetOld.getRecordByRegion(oldlabels[j]).getCmin();
		if (r < rmin) rmin = r;
		if (r > rmax) rmax = r;
		if (c < cmin) cmin = c;
		if (c > cmax) cmax = c;
		r = OverlapsDatasetOld.getRecordByRegion(oldlabels[j]).getRmax();
		c = OverlapsDatasetOld.getRecordByRegion(oldlabels[j]).getCmax();
		if (r < rmin) rmin = r;
		if (r > rmax) rmax = r;
		if (c < cmin) cmin = c;
		if (c > cmax) cmax = c;
	};

	for (unsigned int j = 0; j < newlabels.size(); ++j)
	{
		int r = OverlapsDatasetNew.getRecordByRegion(newlabels[j]).getRmin();
		int c = OverlapsDatasetNew.getRecordByRegion(newlabels[j]).getCmin();
		if (r < rmin) rmin = r;
		if (r > rmax) rmax = r;
		if (c < cmin) cmin = c;
		if (c > cmax) cmax = c;
		r = OverlapsDatasetNew.getRecordByRegion(newlabels[j]).getRmax();
		c = OverlapsDatasetNew.getRecordByRegion(newlabels[j]).getCmax();
		if (r < rmin) rmin = r;
		if (r > rmax) rmax = r;
		if (c < cmin) cmin = c;
		if (c > cmax) cmax = c;
	}

	rmin-=2; if (rmin < 0) rmin = 0;
	cmin-=2; if (cmin < 0) cmin = 0;
	rmax+=2; if (rmax > labelsOld->getHeight() - 1) rmax = labelsOld->getHeight() - 1;
	cmax+=2; if (rmax > labelsOld->getWidth()  - 1) cmax = labelsOld->getWidth()  - 1;

	oldL.set(cmin, rmin, cmax - cmin + 1, rmax - rmin + 1, 1,16,true); 
	newL.set(cmin, rmin, cmax - cmin + 1, rmax - rmin + 1, 1,16,true); 

	unsigned long indexMinOld = this->labelsOld->getIndex(rmin, cmin);

	for (unsigned long indexMin = 0, indexMaxRow = oldL.getWidth(); indexMin < oldL.getNGV(); 
		 indexMaxRow += oldL.getDiffY(), indexMin += oldL.getDiffY(), 
		 indexMinOld += this->labelsOld->getDiffY())
	{
		for (unsigned long index = indexMin, indexOld = indexMinOld; index < indexMaxRow; ++index, ++indexOld)
		{
			unsigned short labelOld = this->labelsOld->getLabel(indexOld);
			unsigned short labelNew = this->labelsNew->getLabel(indexOld);
			unsigned short label = 0;

			if (labelOld)
			{
				for (unsigned int j = 0; j < oldlabels.size() && !label; ++j)
				{
					if (labelOld == oldlabels[j]) label = labelOld;
				}
			}
			oldL.pixUShort(index) = label;
			label = 0;

			if (labelNew)
			{
				for (unsigned int j = 0; j < newlabels.size() && !label; ++j)
				{
					if (labelNew == newlabels[j]) label = labelNew;
				}
			}
			newL.pixUShort(index) = label;
		}
	}

}

void CBuildingChangeEvaluator::putBackLabels(CLabelImage *largeLabels, CLabelImage &smallLabels, 
												bool changeAll, bool initPix)
{
	unsigned long indexMinLarge = largeLabels->getIndex(smallLabels.getOffsetY(), smallLabels.getOffsetX());

	for (unsigned long indexMin = 0, indexMaxRow = smallLabels.getWidth(); 
		 indexMin < smallLabels.getNGV(); 
		 indexMaxRow += smallLabels.getDiffY(), indexMin += smallLabels.getDiffY(), 
		 indexMinLarge += largeLabels->getDiffY())
	{
		for (unsigned long index = indexMin, indexLarge = indexMinLarge; index < indexMaxRow; ++index, ++indexLarge)
		{
			unsigned short labelLarge = largeLabels->getLabel(indexLarge);
			unsigned short labelSmall = smallLabels.getLabel(index);
			if (labelSmall && (labelLarge || changeAll) && (labelSmall != labelLarge)) 
				largeLabels->pixUShort(indexLarge) = labelSmall;
		}
	}

	if (initPix) largeLabels->initPixelNumbers();
}
	
void CBuildingChangeEvaluator::changedNewLabels(CLabelImage &oldL, CLabelImage &newL, CLabelImage &changed, int size)
{
	CImageBuffer mask(newL.getOffsetX(), newL.getOffsetY(), newL.getWidth(), newL.getHeight(), 1, 1, true);
	changed.set(newL.getOffsetX(), newL.getOffsetY(), newL.getWidth(), newL.getHeight(), 1, 16, true);
	for (unsigned long i = 0; i < newL.getNGV(); ++i)
	{
		unsigned short oldlabel = oldL.getLabel(i);
		unsigned short newlabel = newL.getLabel(i);
		if (newlabel && !oldlabel) mask.pixUChar(i) = 1;
		else mask.pixUChar(i) = 0;
	}

	CImageBuffer mask1(mask, false);
	CImageFilter morpho(eMorphological, size, size, 1);
	morpho.binaryMorphologicalOpen(mask, mask1, 1);

	changed.initFromMask(mask1, 1);

	for (unsigned long i = 0; i < oldL.getNGV(); ++i)
	{
		unsigned short changedL = changed.getLabel(i);
		if (changedL)
		{
			oldL.pixUShort(i) = changedL + (unsigned short) this->labelsNew->getNLabels();
		}
	}
}

void CBuildingChangeEvaluator::checkTopology(CLabelImage &newL, unsigned int minPix, int size)
{
	CLabelImage labM(newL);
	labM.morphologicalOpen(size,0);

	for (unsigned int j = newL.getNLabels(); j < labM.getNLabels(); ++j) 
	{
		if (labM.getNPixels(j) < minPix) labM.removeLabel(j, false);
	}
	
	labM.updateVoronoi();
		
	for (unsigned int m = 0; m < newL.getNGV(); ++m)
	{
		if (newL.getLabel(m)) newL.pixUShort(m) = labM.getVoronoi(m);
	}
};

void CBuildingChangeEvaluator::clarifyMToN(float tolerance)
{
	CLabelImage Old(0, 0, 1, 1, 8); 
	CLabelImage New(0, 0, 1, 1, 8); 
	CLabelImage Changed(0, 0, 1, 1, 8); 

	int size = (int) floor(tolerance / fabs(this->trafo.getA()));

	for (unsigned int i = 0; i < MToNOld.size(); ++i)
	{
		vector<unsigned short> oldLabels = MToNOld[i];
		vector<unsigned short> newLabels = MToNNew[i];

		extractLabels(oldLabels, newLabels, Old, New);

		changedNewLabels(Old, New, Changed, size);

		Old.initPixelNumbers();
		Old.updateVoronoi();
		New.initPixelNumbers();
		New.updateVoronoi();

		int nNewLabels = Changed.getNLabels() - 1;
		vector<unsigned short> newChangedLabels;

		for (int j = 0; j < nNewLabels; ++j)
		{
			newChangedLabels.push_back((unsigned short) this->labelsNew->getNLabels() + j + 1);
		}
		if (nNewLabels > 0) nNewLabels = newChangedLabels[newChangedLabels.size() - 1];
		else nNewLabels = this->labelsNew->getNLabels();

		for (unsigned int j = 0; j < newLabels.size(); ++j)
		{
			if (OverlapsDatasetNew.getRecordByRegion(newLabels[j]).nOverlaps() > 1)
			{
				unsigned short label0 = (unsigned short) OverlapsDatasetNew.getRecordByRegion(newLabels[j]).getRegion(0).getIndex();
				vector<unsigned short> otherLabels;
				vector<unsigned short> otherLabelsNew;
				for (unsigned int k = 1; k < OverlapsDatasetNew.getRecordByRegion(newLabels[j]).nOverlaps(); ++k)
				{
					otherLabels.push_back((unsigned short) OverlapsDatasetNew.getRecordByRegion(newLabels[j]).getRegion(k).getIndex());
					otherLabelsNew.push_back((unsigned short) nNewLabels + k);
				}

				for (unsigned int k = 0; k < Old.getNGV(); ++k)
				{
					unsigned short Lnew = New.getLabel(k);
					if (Lnew == newLabels[j])
					{
						unsigned short Lold = Old.getVoronoi(k);
						if (Lold != label0)
						{
							unsigned short la = 0;
							for (unsigned int l = 0; l < otherLabels.size() && !la; ++l)
							{
								if (Lold == otherLabels[l]) la = otherLabelsNew[l];
							}
							if (!la) 
							{
								for (unsigned int l = 0; l < newChangedLabels.size() && !la; ++l)
								{
									if (Lold == newChangedLabels[l]) la = newChangedLabels[l];
								}
							}

							New.pixUShort(k) = la;
						}
					}
				}

			}
		}

		New.initPixelNumbers();

		checkTopology(New, 20, 3);

        if (outputMode > eMEDIUM)
		{
			ostrstream oss;
			oss << "MN_new_" << i << ends;
			char *z = oss.str();
		    New.dump(this->projectDir.GetChar(), z);
			delete [] z;
		}
			
		putBackLabels(this->labelsNew, New, false, true);

	}
}	 

void CBuildingChangeEvaluator::clarifyMtoOne(float tolerance)
{
	CLabelImage Old(0, 0, 1, 1, 8); 
	CLabelImage New(0, 0, 1, 1, 8); 
	CLabelImage Changed(0, 0, 1, 1, 8); 

	int size = (int) floor(tolerance / fabs(this->trafo.getA()));

	for (unsigned int i = 0; i < MToOneOld.size(); ++i)
	{
		vector<unsigned short> oldLabels = MToOneOld[i];
		vector<unsigned short> newLabels;
		newLabels.push_back(MToOneNew[i]);

		extractLabels(oldLabels, newLabels, Old, New);

		changedNewLabels(Old, New, Changed, size);

		Old.initPixelNumbers();
		Old.updateVoronoi();
		New.initPixelNumbers();
		New.updateVoronoi();
	
		int nNewLabels = Changed.getNLabels() - 1;
		vector<unsigned short> newChangedLabels;

		for (int j = 0; j < nNewLabels; ++j)
		{
			newChangedLabels.push_back((unsigned short) this->labelsNew->getNLabels() + j + 1);
		}
		if (nNewLabels > 0) nNewLabels = newChangedLabels[newChangedLabels.size() - 1];
		else nNewLabels = this->labelsNew->getNLabels();
	
		unsigned short label0 = oldLabels[0];
		vector<unsigned short> otherLabelsNew;
		vector<unsigned short> otherLabels;
		for (unsigned int j = 1; j < oldLabels.size(); ++j)
		{
			otherLabels.push_back(oldLabels[j]);
			otherLabelsNew.push_back(nNewLabels + j);
		}


		for (unsigned int j = 0; j < Old.getNGV(); ++j)
		{
			unsigned short Lnew = New.getLabel(j);
			if (Lnew)
			{
				unsigned short Lold = Old.getVoronoi(j);
				if (Lold != label0)
				{
					unsigned short la = 0;
					for (unsigned int l = 0; l < otherLabels.size() && !la; ++l)
					{
						if (Lold == otherLabels[l]) la = otherLabelsNew[l];
					}
					if (!la) 
					{
						for (unsigned int l = 0; l < newChangedLabels.size() && !la; ++l)
						{
							if (Lold == newChangedLabels[l]) la = newChangedLabels[l];
						}
					}

					New.pixUShort(j) = la;				
				}			
			}	
		}

		New.initPixelNumbers();

		checkTopology(New, 20, 3);

        if (outputMode > eMEDIUM)
		{
			ostrstream oss;
			oss << "MOne_new_" << i << ends;
			char *z = oss.str();
		    New.dump(this->projectDir.GetChar(), z);
			delete [] z;
		}
			
		putBackLabels(this->labelsNew, New, false, true);
	}
};

void CBuildingChangeEvaluator::simplifyOnetoM(float tolerance)
{
	CLabelImage Old(0, 0, 1, 1, 8); 
	CLabelImage New(0, 0, 1, 1, 8); 

	int size = (int) floor(tolerance / fabs(this->trafo.getA()));
	size /= 2;
	if (size < 2) size = 2;

	for (unsigned int i = 0; i < oneToMOld.size(); ++i)
	{
		vector<unsigned short> newLabels = oneToMNew[i];
		vector<unsigned short> oldLabels;
		oldLabels.push_back(oneToMOld[i]);

		extractLabels(oldLabels, newLabels, Old, New);

		New.initPixelNumbers();
		New.updateVoronoi();

		New.morphologicalClose(size);
	
		unsigned long uMax = New.getNGV() - New.getDiffY();

		for (unsigned long uMin = New.getDiffX(), uMaxRow = New.getDiffY() - New.getDiffX(); 
			 uMin < uMax; uMin += New.getDiffY(), uMaxRow += New.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += New.getDiffX())
			{
				unsigned short label00 = New.getLabel(u);
				if (label00)
				{
					unsigned short label01 = New.getLabel(u + New.getDiffY());
					if (label01 && (label01 != label00))
					{
						New.mergeLabels(label00, label01, false);
						this->labelsNew->mergeLabels(label00, label01, false);
						label00 = New.getLabel(u);
					}

					unsigned short label10 = New.getLabel(u + New.getDiffX());
					if (label10 && (label10 != label00))
					{
						New.mergeLabels(label00, label10, false);
						this->labelsNew->mergeLabels(label00, label10, false);	
					}
				}
			}
		}

        if (outputMode > eMEDIUM)
		{
			ostrstream oss;
			oss << "OneM_new_" << i << ends;
			char *z = oss.str();
		    New.dump(this->projectDir.GetChar(), z);
			delete [] z;
		}			
	}
};

void CBuildingChangeEvaluator::detectConfirmed(const vector<unsigned short> &oldLabels, 
											   const vector<unsigned short> &newLabels, int size)
{
	CLabelImage Old(0, 0, 1, 1, 8); 
	CLabelImage New(0, 0, 1, 1, 8); 
	extractLabels(oldLabels, newLabels, Old, New);

	CImageBuffer buf1(Old.getOffsetX(), Old.getOffsetY(), Old.getWidth(), Old.getHeight(), 1,1,true);

	// determine deleted parts of oldLabels[0]
	// first mark deleted pixels
	for (unsigned int j = 0; j < New.getNGV(); ++j)
	{
		unsigned short oldLabel = Old.getLabel(j);
		unsigned short newLabel = New.getLabel(j);

		if (oldLabel && !newLabel)
		{
			buf1.pixUChar(j) = 1;
		}
		else buf1.pixUChar(j) = 0;
	}

	// check whether deleted areas are significant by morphologic filtering
	CImageBuffer mask(buf1, false);
	CImageFilter morpho(eMorphological, size, size, 1);
	morpho.binaryMorphologicalOpen(buf1, mask, 1);
	CLabelImage delChanged(mask,1,8);

	if (delChanged.getNLabels() > 1)
	{// There are significant deleted areas
		delChanged.updateVoronoi();
		delChanged.morphologicalClose(size + 1);

		CImageFilter morpho1(eMorphological, 2, 2, 1);
		morpho1.binaryMorphologicalOpen(buf1, mask, 1);

		for (unsigned  int j = 0; j < delChanged.getNGV(); ++j)
		{
			if (!mask.pixUChar(j)) delChanged.pixUShort(j) = 0;
			else if (delChanged.pixUShort(j)) delChanged.pixUShort(j) = oldLabels[0];
		}

		putBackLabels(this->labelsDeleted, delChanged, true, false);
	}

	// determine new parts of oldLabels[0]
	// first mark new pixels
	for (unsigned int j = 0; j < New.getNGV(); ++j)
	{
		unsigned short oldLabel = Old.getLabel(j);
		unsigned short newLabel = New.getLabel(j);

		if (!oldLabel && newLabel)
		{
			buf1.pixUChar(j) = 1;
		}
		else buf1.pixUChar(j) = 0;
	}

	// check whether new areas are significant by morphologic filtering
	morpho.binaryMorphologicalOpen(buf1, mask, 1);
	CLabelImage newChanged(mask,1,8);

	if (newChanged.getNLabels() > 1)
	{
		newChanged.updateVoronoi();

		newChanged.morphologicalClose(size + 1);
		CImageFilter morpho1(eMorphological, 2, 2, 1);
		morpho1.binaryMorphologicalOpen(buf1, mask, 1);

		for (unsigned int j = 0; j < newChanged.getNGV(); ++j)
		{
			if (!mask.pixUChar(j))  newChanged.pixUShort(j) = 0;
			else if (newChanged.pixUShort(j)) newChanged.pixUShort(j) = oldLabels[0];
		}
		putBackLabels(this->labelsNewBuilt, newChanged, true, false);
	}

	if ((this->OverlapsDatasetOld.getRecordByRegion(oldLabels[0]).getClassification() >= ePARTIAL) &&
		(delChanged.getNLabels() <= 1) && (newChanged.getNLabels() <= 1)) 
	{
		unsigned short l = oldLabels[0];
		this->confirmedBuildings.push_back(l);
		for (unsigned long i = 0; i < this->labelsConfirmed->getNGV(); ++i)
		{
			unsigned short label = this->labelsOld->getLabel(i);
			if (label == l) this->labelsConfirmed->pixUShort(i) = label;
		}
	}
	else
	{
		unsigned short l = oldLabels[0];
		this->changedBuildings.push_back(l);
		int maxLab = this->labelsOld->getNLabels();

		for (unsigned long i = 0; i < delChanged.getNGV(); ++i)
		{
			unsigned short chLab = delChanged.getLabel(i);
			if (chLab) Old.pixUShort(i) = maxLab + chLab;
		}

		int dx[8];
		dx[0] =  Old.getDiffX(); dx[1] =  Old.getDiffX() + Old.getDiffY(); 
		dx[2] =  Old.getDiffY(); dx[3] = -Old.getDiffX() + Old.getDiffY(); 
		dx[4] = -Old.getDiffX(); dx[5] = -Old.getDiffX() - Old.getDiffY(); 
		dx[6] = -Old.getDiffY(); dx[7] =  Old.getDiffX() - Old.getDiffY(); 
		unsigned long uMax = Old.getNGV() + dx[5];
		
		for (unsigned long uMin = dx[1], uMaxRow = Old.getDiffY() + dx[3]; uMin < uMax; uMin += Old.getDiffY(), uMaxRow += Old.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += Old.getDiffX())
			{
				int isSingle = true;
				for(unsigned int i = 0; i < 8 && isSingle; ++i)
				{
					if (Old.getLabel(u + dx[i]) == l) isSingle = false;
				}
				if (isSingle) Old.pixUShort(u) = 0;
			}
		}

		Old.initPixelNumbers();

		Old.updateVoronoi();

		for (unsigned long i = 0; i < delChanged.getNGV(); ++i)
		{
			unsigned short chLab = Old.getVoronoi(i);
			if (chLab > maxLab) Old.pixUShort(i) = 0;
		}
	

		putBackLabels(this->labelsConfirmed, Old, true, false);
	}
}

void CBuildingChangeEvaluator::detectConfirmedMultiple(const vector<unsigned short> &oldLabels, 
													   const vector<unsigned short> &newLabels, 
													   const vector<unsigned short> &newLabelsNew, 
													   int size)
{
	CLabelImage Old(0, 0, 1, 1, 8); 
	CLabelImage New(0, 0, 1, 1, 8); 
	extractLabels(oldLabels, newLabels, Old, New);

	CImageBuffer buf1(Old.getOffsetX(), Old.getOffsetY(), Old.getWidth(), Old.getHeight(), 1,1,true);

	// determine deleted parts of oldLabels[0]
	// first mark deleted pixels
	for (unsigned int j = 0; j < New.getNGV(); ++j)
	{
		unsigned short oldLabel = Old.getLabel(j);
		unsigned short newLabel = New.getLabel(j);

		if (oldLabel && !newLabel)
		{
			buf1.pixUChar(j) = 1;
		}
		else buf1.pixUChar(j) = 0;
	}

	// check whether deleted areas are significant by morphologic filtering
	CImageBuffer mask(buf1, false);
	CImageFilter morpho(eMorphological, size, size, 1);
	morpho.binaryMorphologicalOpen(buf1, mask, 1);
	CLabelImage delChanged(mask,1,8);

	if (delChanged.getNLabels() > 1)
	{// There are significant deleted areas
		delChanged.updateVoronoi();
		delChanged.morphologicalClose(size + 1);

		CImageFilter morpho1(eMorphological, 2, 2, 1);
		morpho1.binaryMorphologicalOpen(buf1, mask, 1);

		for (unsigned  int j = 0; j < delChanged.getNGV(); ++j)
		{
			if (!mask.pixUChar(j)) delChanged.pixUShort(j) = 0;
			else if (delChanged.pixUShort(j)) delChanged.pixUShort(j) = oldLabels[0];
		}

		putBackLabels(this->labelsDeleted, delChanged, true, false);
	}

	// determine new parts of oldLabels[0]
	// first mark new pixels
	for (unsigned int j = 0; j < New.getNGV(); ++j)
	{
		unsigned short oldLabel = Old.getLabel(j);
		unsigned short newLabel = New.getLabel(j);

		if (!oldLabel && newLabel)
		{
			buf1.pixUChar(j) = 1;
		}
		else buf1.pixUChar(j) = 0;
	}

	// check whether new areas are significant by morphologic filtering
	morpho.binaryMorphologicalOpen(buf1, mask, 1);
	CLabelImage newChanged(mask,1,8);

	if (newChanged.getNLabels() > 1)
	{
		newChanged.updateVoronoi();

		newChanged.morphologicalClose(size + 1);
		CImageFilter morpho1(eMorphological, 2, 2, 1);
		morpho1.binaryMorphologicalOpen(buf1, mask, 1);

		for (unsigned int j = 0; j < newChanged.getNGV(); ++j)
		{
			if (!mask.pixUChar(j)) newChanged.pixUShort(j) = 0;
			else 
			{
				unsigned short lab = New.pixUShort(j);
					
				if (lab) 
				{
					unsigned short newLab = USHRT_MAX;
					for (unsigned int k = 0; k < newLabelsNew.size() && newLab == USHRT_MAX; ++k)
					{
						if (lab == newLabels[k]) newLab = newLabelsNew[k];

					}
					newChanged.pixUShort(j) = newLab;
				}
			}
		}
		putBackLabels(this->labelsNewBuilt, newChanged, true, false);
	}

	if ((this->OverlapsDatasetOld.getRecordByRegion(oldLabels[0]).getClassification() >= ePARTIAL) &&
		(delChanged.getNLabels() <= 1) && (newChanged.getNLabels() <= 1)) 
	{
		unsigned short l = oldLabels[0];
		this->confirmedBuildings.push_back(l);
		for (unsigned long i = 0; i < this->labelsConfirmed->getNGV(); ++i)
		{
			unsigned short label = this->labelsOld->getLabel(i);
			if (label == l) this->labelsConfirmed->pixUShort(i) = label;
		}
	}
	else
	{
		unsigned short l = oldLabels[0];
		this->changedBuildings.push_back(l);
		int maxLab = this->labelsOld->getNLabels();

		for (unsigned long i = 0; i < delChanged.getNGV(); ++i)
		{
			unsigned short chLab = delChanged.getLabel(i);
			if (chLab) Old.pixUShort(i) = maxLab + chLab;
		}

		int dx[8];
		dx[0] =  Old.getDiffX(); dx[1] =  Old.getDiffX() + Old.getDiffY(); 
		dx[2] =  Old.getDiffY(); dx[3] = -Old.getDiffX() + Old.getDiffY(); 
		dx[4] = -Old.getDiffX(); dx[5] = -Old.getDiffX() - Old.getDiffY(); 
		dx[6] = -Old.getDiffY(); dx[7] =  Old.getDiffX() - Old.getDiffY(); 
		unsigned long uMax = Old.getNGV() + dx[5];
		
		for (unsigned long uMin = dx[1], uMaxRow = Old.getDiffY() + dx[3]; uMin < uMax; uMin += Old.getDiffY(), uMaxRow += Old.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += Old.getDiffX())
			{
				int isSingle = true;
				for(unsigned int i = 0; i < 8 && isSingle; ++i)
				{
					if (Old.getLabel(u + dx[i]) == l) isSingle = false;
				}
				if (isSingle) Old.pixUShort(u) = 0;
			}
		}

		Old.initPixelNumbers();
		Old.updateVoronoi();
		New.initPixelNumbers();
		New.updateVoronoi();

		for (unsigned long i = 0; i < delChanged.getNGV(); ++i)
		{
			unsigned short chLab = Old.getVoronoi(i);
			if (chLab > maxLab) Old.pixUShort(i) = 0;
			else
			{
				unsigned short lab = Old.pixUShort(i);
				if (lab)
				{
					lab = New.getVoronoi(i);
					if (lab != newLabels[0])				
					{
						unsigned short newLab = USHRT_MAX;
						for (unsigned int k = 1; k < newLabelsNew.size() && newLab == USHRT_MAX; ++k)
						{
							if (lab == newLabels[k]) newLab = newLabelsNew[k];

						}
						Old.pixUShort(i) = newLab;
					}
				}
			}
		}
	

		putBackLabels(this->labelsConfirmed, Old, true, false);
	}
};

	
void CBuildingChangeEvaluator::detectConfirmed(float tolerance)
{
	this->confirmedBuildings.clear();
	this->changedBuildings.clear();


	int size = (int) floor(tolerance / fabs(this->trafo.getA()));

	for (unsigned int i = 0; i < this->oneToOneOld.size(); ++i)
	{
		vector<unsigned short> oldLabels, newLabels;
		oldLabels.push_back(oneToOneOld[i]);
		newLabels.push_back(oneToOneNew[i]);

		detectConfirmed(oldLabels, newLabels, size);
	}
	
	newBuildingsSplit.clear();

	for (unsigned int i = 0; i < this->oneToMOld.size(); ++i)
	{
		vector<unsigned short> oldLabels, newLabels, newLabelsNew;
		oldLabels.push_back(oneToMOld[i]);

		newLabels = (oneToMNew[i]);

		int iMax = 0;
		float pMax = OverlapsDatasetOld.getRecordByRegion(oneToMOld[i]).getPercentage(0);

		for (unsigned int j = 1; j < newLabels.size(); ++j)
		{
			float p = OverlapsDatasetOld.getRecordByRegion(oneToMOld[i]).getPercentage(j);
			if (p > pMax)
			{
				pMax = p;
				iMax = j;
			};
		}

		unsigned short l = newLabels[0];
		newLabels[0] = newLabels[iMax];
		newLabels[iMax] = l;

		newLabelsNew.push_back(oneToMOld[i]);

		for (unsigned int j = 1; j < newLabels.size(); ++j)
		{
			newLabelsNew.push_back((unsigned short)(this->newBuildings.size() + this->newBuildingsSplit.size() + labelsOld->getNLabels()));
			this->newBuildingsSplit.push_back(newLabelsNew[j]);
		}

		detectConfirmedMultiple(oldLabels, newLabels, newLabelsNew, size);
	}

	this->labelsNewBuilt->initPixelNumbers();
	this->labelsDeleted->initPixelNumbers();
};
 
void CBuildingChangeEvaluator::simplifyContained()
{
	vector<vector<unsigned short> > neighbours(this->labelsNew->getNLabels());

	unsigned long uMax = this->labelsNew->getNGV() - this->labelsNew->getDiffY();

	for (unsigned long uMin = this->labelsNew->getDiffX(), 
		 uMaxRow = this->labelsNew->getDiffY() - this->labelsNew->getDiffX(); 
		 uMin < uMax; uMin += this->labelsNew->getDiffY(), uMaxRow += this->labelsNew->getDiffY())
	{
		for (unsigned long u = uMin; u < uMaxRow; u += this->labelsNew->getDiffX())
		{
			unsigned short label00 = this->labelsNew->getLabel(u);
			unsigned short label01 = this->labelsNew->getLabel(u + this->labelsNew->getDiffY());
			if (label01 != label00)
			{
				bool isNew = true;
				for (unsigned int j = 0; j < neighbours[label00].size() && isNew; ++j)
				{
					if (neighbours[label00][j] == label01) isNew = false;
				}
				
				if (isNew) neighbours[label00].push_back(label01);

				isNew = true;
				for (unsigned int j = 0; j < neighbours[label01].size() && isNew; ++j)
				{
					if (neighbours[label01][j] == label00) isNew = false;
				}
				
				if (isNew) neighbours[label01].push_back(label00);
			}

			unsigned short label10 = this->labelsNew->getLabel(u +this->labelsNew->getDiffX());
			if (label10 != label00)
			{
				bool isNew = true;
				for (unsigned int j = 0; j < neighbours[label00].size() && isNew; ++j)
				{
					if (neighbours[label00][j] == label10) isNew = false;
				}
				
				if (isNew) neighbours[label00].push_back(label10);

				isNew = true;
				for (unsigned int j = 0; j < neighbours[label10].size() && isNew; ++j)
				{
					if (neighbours[label10][j] == label00) isNew = false;
				}
				
				if (isNew) neighbours[label10].push_back(label00);
			}
			
		}
			
	}

	for (unsigned int i = 1; i < neighbours.size(); ++i)
	{
		if (neighbours[i].size() == 1)
		{
			if (neighbours[i][0] != 0)
			{
				bool isNew = false;

				for (unsigned int j = 0; j < this->newBuildings.size() && !isNew; ++j)
				{
					if (this->newBuildings[j] == i) isNew = true;
				}
				if (isNew) this->labelsNew->mergeLabels(neighbours[i][0], i, false);
			}
		}
	}
};