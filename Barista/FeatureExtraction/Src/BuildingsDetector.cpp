#include <float.h>
#include <strstream>
#include <algorithm>
#include <vector>
#include "BuildingsDetector.h"
#include "HugeImage.h"
#include "BestFitLine.h"
#include "LeastSquaresSolver.h"
#include "ALSPointCloud.h"
#include "ALSTile.h"
#include "ALSData.h"
#include "TFW.h"
#include "LookupTableHelper.h"
//#include "Bui_ProgressDlg.h"
//#include "ProgressThread.h"
#include "statistics.h"
#include "LabelImage.h"
#include "BlockDiagonalMatrix.h"
#include "constants.h"
#include "SystemUtilities.h"
//class CBestFitLine;
//class CLeastSquaresSolver;
using namespace std;

BuildingsDetector::BuildingsDetector (CALSDataSet *ALS, CHugeImage *colorImage, const CTFW *imgTFW,
									  const C2DPoint &min, const C2DPoint &max, const C2DPoint &grid,
									  const CCharString &directory, bool useNDVI, bool useEntropy, bool useHistogram, bool detectionMode):
		pALS(ALS), rgbImage(colorImage), sigmaNDVI8bit(0), tfwImage(imgTFW), gMask(0), upMask(0), 
		pMin(min), pMax(max), resolution(grid), projectPath(directory), edgeThresh(0.7), ndviThresh(48),
		heightThr(2.5), patchSize((int)(450*0.15/0.25+1)), nPoint(0), nLine(0), Corner("Cor_"), CurrentLineLabel("Lin_"),
		thetaThresh(22.5), iniBuildingReductionThresh(0.15), useNDVIinExtension(useNDVI), useEntropy(useEntropy), useHistogram(useHistogram),
		detectionMode(true)
		
{// minContourLength((int)(20*0.15/grid.x))	
	//dAl(2/grid.x), 
	//minBuildingLength(3), maxBuildingLength(50), sqrMaxBuildingLength(50*50), 

	this->resolutionImage.x = abs(this->tfwImage->getA());//image resolution	
	this->resolutionImage.y = this->resolutionImage.x;
	

	this->resolutionMask.x = 0.25;
	this->resolutionMask.y = 0.25;
	this->dAl = 2/this->resolutionMask.x;
	this->minBuildingLength = (int)(3/this->resolutionMask.x);
	this->maxBuildingLength = (int)(50/this->resolutionMask.x);
	this->sqrMaxBuildingLength = this->maxBuildingLength*this->maxBuildingLength;

	this->minContourLength = (int) (3.0/this->resolutionMask.x);//minBuildingLength = 3m

	//ndvi thresold for 3 & 4 bands images
	if (rgbImage->getBands() > 3)
		this->ndviThresh = 10;
	else
		this->ndviThresh = 48;

	this->entropyThresh = 0.30;

	/*
	//setFeartureExtractionParameters();
	this->parameters.Mode = eCanny;
	this->parameters.NoiseModel = eGaussian;
	this->parameters.setAlpha((float)0.1);	
	this->parameters.lineThinningThreshold = 0.5;
	this->parameters.minLineLength = 3.0;
	this->parameters.extractEdges = true;
	this->parameters.extractPoints = false;
	this->parameters.extractRegions = false;
	this->parameters.setDerivativeFilter(eDiffXGauss,1.0); //type = eDiffXGauss or eSobelX; sigma = 1.0
	//this->parameters.setWatershedSmoothingIterations(it); // for watershed segmentation technique
	*/
	CurrentLineLabel += 0;

	this->pDEM = 0;
	this->tfwDEM = 0;

	if (!this->projectPath.IsEmpty())
	{
		if (this->projectPath[this->projectPath.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->projectPath += CSystemUtilities::dirDelimStr;
	}
};

BuildingsDetector::BuildingsDetector (CALSDataSet *ALS, CHugeImage *colorImage, CHugeImage *dem, const CTFW *imgTFW, const CTFW *demTFW,
									  const C2DPoint &min, const C2DPoint &max, const C2DPoint &grid,  const C2DPoint &gridDEM,
									  const CCharString &directory, bool useNDVI, bool useEntropy, bool useHistogram, bool detectionMode):
		pALS(ALS), rgbImage(colorImage), pDEM(dem), sigmaNDVI8bit(0), tfwImage(imgTFW), tfwDEM(demTFW), gMask(0), upMask(0), 
		pMin(min), pMax(max), resolution(grid), resolutionDEM(gridDEM), projectPath(directory), edgeThresh(0.7), ndviThresh(48),
		heightThr(2.5), patchSize((int)(450*0.15/0.25+1)), nPoint(0), nLine(0), Corner("Cor_"), CurrentLineLabel("Lin_"),
		thetaThresh(22.5), iniBuildingReductionThresh(0.15), useNDVIinExtension(useNDVI), useEntropy(useEntropy), useHistogram(useHistogram),
		detectionMode(detectionMode)
// minContourLength((int)(20*0.15/grid.x))		
{//dAl(2/grid.x), 
	//minBuildingLength(3), maxBuildingLength(50), sqrMaxBuildingLength(50*50), 

	this->resolutionImage.x = abs(this->tfwImage->getA());//image resolution	
	this->resolutionImage.y = this->resolutionImage.x;
	

	this->resolutionMask.x = 0.25;
	this->resolutionMask.y = 0.25;
	this->dAl = 2/this->resolutionMask.x;
	this->minBuildingLength = (int)(3/this->resolutionMask.x);
	this->maxBuildingLength = (int)(50/this->resolutionMask.x);
	this->sqrMaxBuildingLength = this->maxBuildingLength*this->maxBuildingLength;

	this->minContourLength = (int) (3.0/this->resolutionMask.x);//minBuildingLength = 3m

	//ndvi thresold for 3 & 4 bands images
	if (rgbImage->getBands() > 3)
		this->ndviThresh = 10;
	else
		this->ndviThresh = 48;

	this->entropyThresh = 0.30;

	/*
	//setFeartureExtractionParameters();
	this->parameters.Mode = eCanny;
	this->parameters.NoiseModel = eGaussian;
	this->parameters.setAlpha((float)0.1);	
	this->parameters.lineThinningThreshold = 0.5;
	this->parameters.minLineLength = 3.0;
	this->parameters.extractEdges = true;
	this->parameters.extractPoints = false;
	this->parameters.extractRegions = false;
	this->parameters.setDerivativeFilter(eDiffXGauss,1.0); //type = eDiffXGauss or eSobelX; sigma = 1.0
	//this->parameters.setWatershedSmoothingIterations(it); // for watershed segmentation technique
	*/
	CurrentLineLabel += 0;
};

BuildingsDetector::BuildingsDetector (CHugeImage *colorImage, const CTFW *imgTFW, const CCharString &directory, bool detectionMode):
		rgbImage(colorImage), tfwImage(imgTFW), projectPath(directory), heightThr(2.5), patchSize((int)(450*0.15/0.25+1)), detectionMode(detectionMode)
// minContourLength((int)(20*0.15/grid.x))		
{//dAl(2/grid.x), 
	//minBuildingLength(3), maxBuildingLength(50), sqrMaxBuildingLength(50*50), 

	this->resolutionImage.x = abs(this->tfwImage->getA());//image resolution	
	this->resolutionImage.y = this->resolutionImage.x;

/////////////////
//probably following codes is not required
/////////////////

	/*
	this->resolutionMask.x = 0.25;
	this->resolutionMask.y = 0.25;
	this->dAl = 2/this->resolutionMask.x;
	this->minBuildingLength = (int)(3/this->resolutionMask.x);
	this->maxBuildingLength = (int)(50/this->resolutionMask.x);
	this->sqrMaxBuildingLength = this->maxBuildingLength*this->maxBuildingLength;

	//ndvi thresold for 3 & 4 bands images
	if (rgbImage->getBands() > 3)
		this->ndviThresh = 10;
	else
		this->ndviThresh = 48;

	//setFeartureExtractionParameters();
	this->parameters.Mode = eCanny;
	this->parameters.NoiseModel = eGaussian;
	this->parameters.setAlpha((float)0.1);	
	this->parameters.lineThinningThreshold = 0.5;
	this->parameters.minLineLength = 3.0;
	this->parameters.extractEdges = true;
	this->parameters.extractPoints = false;
	this->parameters.extractRegions = false;
	this->parameters.setDerivativeFilter(eDiffXGauss,1.0); //type = eDiffXGauss or eSobelX; sigma = 1.0
	//this->parameters.setWatershedSmoothingIterations(it); // for watershed segmentation technique

	CurrentLineLabel += 0;

	*/
};

BuildingsDetector::~BuildingsDetector ()
{
	this->pALS            = 0;
	this->gMask           = 0;
    this->upMask          = 0;
	this->rgbImage        = 0;
	this->tfwImage         = 0;
	if (this->sigmaNDVI8bit)
	{
		delete this->sigmaNDVI8bit;
		this->sigmaNDVI8bit =0 ;
	}
	if (this->gMask)
	{
		delete this->gMask;
		this->gMask = 0;
	}

	if (this->upMask)
	{
		delete this->upMask;
		this->upMask = 0;
	}
};               

void setFeartureExtractionParameters()
{

}

bool BuildingsDetector::extractMasks(CHugeImage *imUpMask, CHugeImage *imGMask, CTFW *maskTFW)
{
	bool ret = this->maskCreater(imUpMask, imGMask, maskTFW, true);
	//bool ret = this->maskCreator(imUpMask, imGMask, maskTFW);

	//CLabelImage labels(this->gMask->getOffsetX(), this->gMask->getOffsetY(), this->gMask->getWidth(), this->gMask->getHeight(), 8);
	//CFeatureExtractionThread *thread = new CFeatureExtractionThread(imGMask, 0, xypts, xycont, xyedges, &labels, this->parameters);

	//bui_ProgressDlg dlg(thread, thread);
	//dlg.exec();

	return ret;
}

//bool BuildingsDetector::extract(CHugeImage *imUpMask, CHugeImage *imGMask, CXYPointArray *xypts, CXYContourArray *xycont, CXYPolyLineArray *xyedges)
bool BuildingsDetector::extractMasks(CHugeImage *imUpMask, CHugeImage *imGMask, int wdth, int hght)
{
	bool ret = this->maskCreater(imUpMask, imGMask, wdth, hght);
	
	//CLabelImage labels(this->gMask->getOffsetX(), this->gMask->getOffsetY(), this->gMask->getWidth(), this->gMask->getHeight(), 8);
	//CFeatureExtractionThread *thread = new CFeatureExtractionThread(imGMask, 0, xypts, xycont, xyedges, &labels, this->parameters);

	//bui_ProgressDlg dlg(thread, thread);
	//dlg.exec();

	return ret;
}
/*
bool BuildingsDetector::extract(CVImage *imUpMask, CVImage *imGMask)
{
	this->maskCreater(imUpMask->getHugeImage(), imGMask->getHugeImage());
	//imUpMask->getXY
	//CFeatureExtractionThread *thread = new CFeatureExtractionThread(buf,this->boundaryPoly, image->getXYPoints(), 
	//		                                                            image->getXYContours(), image->getXYEdges(),
	//													                &labels, this->parameters);

	//bui_ProgressDlg dlg(thread, thread);
	//dlg.exec();

	return true;
}
*/

//bool BuildingsDetector::maskCreator(CHugeImage *imUpMask, CHugeImage *imGMask, CTFW *imgTFW)
//{

//}

bool BuildingsDetector::maskCreater (CHugeImage *imUpMask, CHugeImage *imGMask, CTFW *maskTFW, bool localDEMheight)
{
	if (this->listener) 
	{
		this->listener->setTitleString("Generating masks ...");
		this->listener->setProgressValue(1);
	}

	//get TFW
	Matrix parms(6,1);
	parms.element(0, 0) =  this->resolutionMask.x;//this->resolution.x;
	parms.element(1, 0) =  0.0;
	parms.element(2, 0) =  this->pMin.x;
	parms.element(3, 0) =  0.0;
	parms.element(4, 0) = -this->resolutionMask.y; //this->resolution.y;
	parms.element(5, 0) =  this->pMax.y;
	maskTFW->setAll(parms);
	this->tfwMask = maskTFW;

	//calculate image dimensions
	//if ((resolution.x < FLT_EPSILON) || (resolution.y < FLT_EPSILON)) return false;
	//this->w = int (floor((pMax.x - pMin.x) / resolution.x) + 1.0);
	//this->h = int (floor((pMax.y - pMin.y) / resolution.y) + 1.0);
	this->w = int (floor((pMax.x - pMin.x) / this->resolutionMask.x) + 1.0);
	this->h = int (floor((pMax.y - pMin.y) / this->resolutionMask.y) + 1.0);
	if ((this->w <= 0) || (this->h <= 0)) return false;
	
	//this->w = wdth;
	//this->h = hght;
	bool Ok(true);

	double demW=0, demH=0; 
	if (pDEM) //DEM will be used for mask generation
	{
		demW = this->pDEM->getWidth();
		demH = this->pDEM->getHeight();
	}	

	//initialize buffers for two masks
	this->gMask = new CImageBuffer;
	this->upMask = new CImageBuffer;

	//Ok&=this->gMask->set((long)pMin.x, (long)pMin.y, this->w, this->h, 1, 8, false, 0);
	//Ok&=this->upMask->set((long)pMin.x, (long)pMin.y, this->w, this->h, 1, 8, false,255);
	//this->w += 1;
	//this->h += 1;
	Ok&=this->gMask->set(0, 0, this->w, this->h, 1, 8, false, 0);
	Ok&=this->upMask->set(0, 0, this->w, this->h, 1, 8, false,255);

	//Ok&=this->gMask->set(0, 0, this->w, this->h, 1, 1, true, 0);
	//Ok&=this->upMask->set(0, 0, this->w, this->h, 1, 1, true, 1);

	if (!Ok) return false;

//====================================================	
	double yul = pMax.y;

	//extension of each tile in metres
	C2DPoint tileExt(this->resolutionMask.x * this->patchSize, this->resolutionMask.x * this->patchSize);

	C3DPoint min, max;

	min.z =    -1.0;
    max.z =    1e3;
    
	vector <double > Hvec;
    vector <double > Xvec;
	vector <double > Yvec;
	
	vector <int> ind;
    vector <int> bFreq;
	vector <int> tBin;
    vector <int>::iterator iter;

	int newCapacity		= 0;
	int level			= 0;
	
	CALSPoint ** pointvec ;
    
	int nTiles = (int((pMax.x-pMin.x)/tileExt.x)+1)*(int((pMax.y-pMin.y)/tileExt.y)+1);
	int finished = 0;

	long total = 0; long T = this->pALS->getNPoints();
	for(int TR = 0; TR < this->h; TR += this->patchSize, yul -= tileExt.y) 
	{//first go by height: up->down
		double xul = pMin.x;

		min.y = yul - tileExt.y ;
		max.y = yul ;

		
		for (int TC = 0; TC < this->w; TC += this->patchSize, xul  += tileExt.x)
		{//then go by width: left->right
			min.x = xul ;
			max.x = xul + tileExt.x ;
            this->nPoint=this->pALS->getPointCloud(min, max, pointvec, newCapacity, 2048,level);
			total += this->nPoint;
			if ( this->nPoint<= 0) 
			{ //this code seems useless
				int width = this->patchSize;
				int height = this->patchSize;
				if (TR + height > this->h) height = this->h - TR;
				if (TC + width > this->w ) width = this->w - TC;
			}
			else 
			{
				Hvec.clear();
                Xvec.clear();
				Yvec.clear();
				
				ind.clear();
				bFreq.clear();
    			tBin.clear();
									
				//int nEchos = pALS->getMaxEchos();
			    //CALSPoint ** pointvector = new CALSPoint * [this->nPoint];
				  
        		//for  (int i = 0; i < this->nPoint ; ++i) pointvector[i] = new CALSPoint(nEchos);

				unsigned int sumPoints=0,addindx=0;

				while (sumPoints < this->nPoint)
				{
					 //pointvector[sumPoints]= pointvec[addindx+sumPoints];
					 //CALSPoint * p = pointvector[sumPoints];
					 //double z=pointvector[sumPoints]->getCoordinateAt(2);
					 //double x=pointvector[sumPoints]->getCoordinateAt(0);
					 //double y=pointvector[sumPoints]->getCoordinateAt(1);
					double x = pointvec[addindx+sumPoints]->getCoordinateAt(0);
					double y = pointvec[addindx+sumPoints]->getCoordinateAt(1);
					double z = pointvec[addindx+sumPoints]->getCoordinateAt(2);

					 Xvec.push_back(x);
					 Yvec.push_back(y);
					 Hvec.push_back(z);
					 
	 				 sumPoints ++;
				}
				addindx =+ this->nPoint;
				
				//====================Height threshold calculation =============
				double gThresh;
				int nbr	= (int)(this->resolution.x/this->resolutionMask.x)-1;// -1; //2
				if (!this->pDEM)//no DEM, so estimate height threshold using histograms of height values in ALS data
				{
					double maxH = *max_element(Hvec.begin(), Hvec.end());
					double minH = *min_element(Hvec.begin(), Hvec.end());
									
					int dBin	= 2;				
			        
					double nBin = floor(maxH-minH)/dBin;
									
					for (int i= (int)floor(minH); i<(int)floor(maxH); ++i) 
						tBin.push_back(i);
									
					iter=tBin.end();
					tBin.push_back(*(iter-1)+ dBin);


					for (int k=0;k < nBin;++k)
					{
					   for	(int hCount=0; hCount< (int)Hvec.size();++hCount) 
					   {
						   if(Hvec.at(hCount) >= tBin.at(k) && Hvec.at(hCount) < tBin.at(k+1))  ind.push_back(hCount);
					   }

					   bFreq.push_back(ind.size());
					   ind.clear();
					}
					
					int indx = distance(bFreq.begin(),find (bFreq.begin(), bFreq.end(), *max_element(bFreq.begin(), bFreq.end())));
					gThresh = tBin.at(indx) + heightThr;	
				}
				else //DEM is available, so use the average DEM value as height threshold
				{
					if (!localDEMheight)
					{
						C3DPoint TopLeftObj(xul,yul,0), BotRightObj(xul+tileExt.x,yul-tileExt.y,0);
						C2DPoint TopLeftObs, BotRightObs;
						this->tfwDEM->transformObj2Obs(TopLeftObs,TopLeftObj);
						this->tfwDEM->transformObj2Obs(BotRightObs,BotRightObj);
						if (TopLeftObs.x < 0)
							TopLeftObs.x = 0;
						if (TopLeftObs.y < 0)
							TopLeftObs.y = 0;					
						if (BotRightObs.x > demW-1)
							BotRightObs.x = demW-1;
						if (BotRightObs.y > demH-1)
							BotRightObs.y = demH-1;

						int demTileWidth = (int)(BotRightObs.x-TopLeftObs.x+1), demTileHeight = (int)(BotRightObs.y-TopLeftObs.y+1);
						CImageBuffer demBuf;
						pDEM->getRect(demBuf,(int)TopLeftObs.y,demTileHeight,(int)TopLeftObs.x,demTileWidth);
						vector<float> mean, sdev, med, min, max, fact, diff;
						demBuf.getStatistics(0, min, max, mean, med, sdev, true, -9999);
						gThresh = mean.at(0) + heightThr;
					}
					else
					{//use local DEM height while checking each LIDAR point
					// check code below
					}
				}
				
					//==================== Mask generation =============

					for (unsigned int hIndex=0; hIndex < Hvec.size(); ++hIndex)
					{
           				C3DPoint obj(Xvec.at(hIndex),Yvec.at(hIndex),Hvec.at(hIndex));
						C2DPoint obs;
						this->tfwMask->transformObj2Obs(obs,obj);
						
						if (this->pDEM && localDEMheight) // get local DEM height as the ground height
						{
							C2DPoint obsDEM;
							CImageBuffer demBuf;
							float ht;
							this->tfwDEM->transformObj2Obs(obsDEM,obj);							
							this->pDEM->getRect(demBuf,(long)obsDEM.y,1,(long)obsDEM.x,1);
							ht = demBuf.pixFlt(0);
							int nn = 1;
							while (ht == -9999)
							{
								ht = findMeanHeight(obsDEM, nn);
								nn++;
							}
							gThresh = ht + heightThr;
						}

						if (obs.x-nbr > 0 && obs.y-nbr > 0 && obs.x+nbr < this->w && obs.y+nbr < this->h)
						{
							
							if ( Hvec.at(hIndex) >= gThresh)
							{
								for (int imI= (int)(obs.x-nbr); imI <= (int)(obs.x+nbr); ++imI)
								{
									for  (int imJ=(int)(obs.y-nbr); imJ <= (int)(obs.y+nbr); ++imJ)
									{
										unsigned long imIndx=imJ*this->w+imI;
										this->upMask->pixUChar(imIndx)=0;
									}
								}
							}
					    	

						   else //if ( Hvec.at(hIndex) < gThresh)
						   {
							   for (int imI=(int)(obs.x-nbr); imI <= (int)(obs.x+nbr); ++imI)
								{
									for  (int imJ=(int)(obs.y-nbr); imJ <= (int)(obs.y+nbr); ++imJ)
									{
										unsigned long imIndx=imJ*this->w+imI;
										this->gMask->pixUChar(imIndx)=255;
										//this->gMask->pixUChar(imIndx)=1;
										//int here = 1;
									}
								}
							}
				       
						}//if
  					}//for
				/*}
				else //DEM is available, so use the average DEM value as height threshold
				{
					//====================Height threshold calculation =============
					C3DPoint TopLeftObj(xul,yul,0), BotRightObj(xul+tileExt.x,yul+tileExt.y,0);
					C2DPoint TopLeftObs, BotRightObs;
					this->tfwDEM->transformObj2Obs(TopLeftObs,TopLeftObj);
					this->tfwDEM->transformObj2Obs(BotRightObs,BotRightObj);
					if (TopLeftObs.x < 0)
						TopLeftObs.x = 0;
					if (TopLeftObs.y < 0)
						TopLeftObs.y = 0;					
					if (BotRightObs.x > demW-1)
						BotRightObs.x = demW-1;
					if (BotRightObs.y > demH-1)
						BotRightObs.y = demH-1;

					int demTileWidth = (int)(BotRightObs.x-TopLeftObs.x+1), demTileHeight = (int)(BotRightObs.y-TopLeftObs.y+1);
					CImageBuffer demBuf;
					pDEM->getRect(demBuf,(int)TopLeftObs.y,demTileHeight,(int)TopLeftObs.x,demTileWidth);
					vector<float> mean, sdev, med, min, max, fact, diff;
					demBuf.getStatistics(0, min, max, mean, med, sdev);
					gThresh = mean.at(0) + heightThr;	

					int nbr		= 2;

					//==================== Mask generation =============
				}*/

			}//else

			finished++;
			this->listener->setProgressValue(100*finished/nTiles);
		}  //for
	}//for
	
	CCharString fn(this->projectPath);
	CCharString fnUpmask = fn+imUpMask->getFileNameChar()+".hug";
    CCharString fnGmask = fn+imGMask->getFileNameChar()+".hug";
	
	CLookupTable lut(8,3);
	bool ret=imUpMask->dumpImage(fnUpmask.GetChar(), *upMask, &lut);
	ret=imGMask->dumpImage(fnGmask.GetChar(), *gMask, &lut);
	if (!ret) return false;

	this->listener->setProgressValue(100);

	return ret;
};

float BuildingsDetector::findMeanHeight(C2DPoint obsDEM, int n)
{
	float ht; int bufH, bufW;
	long topY, topX;
	long demW, demH; 
	if (this->pDEM) //DEM will be used for mask generation
	{
		demW = this->pDEM->getWidth();
		demH = this->pDEM->getHeight();
	}
	else
		return -9999;
	long Px = (long) obsDEM.x;
	long Py = (long) obsDEM.y;

	//find height of the neighbour
	if (Py - n >= 0)
	{
		topY = Py - n;
		if (Py + n <= demH-1)
			bufH = 2*n+1;
		else
			bufH = demH - topY;
	}
	else
	{
		topY = 0;
		if (Py + n <= demH-1)
			bufH = Py+1+n;
		else
			bufH = demH - topY;
	}

	//find width of the neighbour
	if (Px - n >= 0)
	{
		topX = Px - n;
		if (Px + n <= demW-1)
			bufW = 2*n+1;
		else
			bufW = demW - topX;
	}
	else
	{
		topX = 0;
		if (Px + n <= demW-1)
			bufW = Px+1+n;
		else
			bufW = demW - topX;
	}

	//get the neigbour in buffer
	CImageBuffer buf;
	this->pDEM->getRect(buf,topY,bufH,topX,bufW);

	long tPoint = 0;
	double tValue = 0.0;
	long i,j;
	CXYPoint P;

	long offX = buf.getOffsetX();
	long offY = buf.getOffsetY();
	//float *pixels = buf.getPixF();
	for (i = topX; i <= topX+bufW-1; i++)
	{
		P.x = i;
		for (j = topY; j <= topY+bufH-1; j++)
		{			
			P.y = j;
			
			unsigned long rowInd, colInd;
			colInd = (unsigned long)P.x - offX;
			rowInd = (unsigned long)P.y - offY;
			unsigned long index = rowInd*bufW + colInd;
			float val = buf.pixFlt(index);
			if (val != -9999)
			{
				tValue += val;
				tPoint++;
			}
		}
	}

	//find average height
	ht = (float)tValue/tPoint;


	return ht;
}

bool BuildingsDetector::maskCreater (CHugeImage *imUpMask, CHugeImage *imGMask, int wdth, int hght)
{
	//calculate image dimensions
	if ((resolution.x < FLT_EPSILON) || (resolution.y < FLT_EPSILON)) return false;
	//this->w = int (floor((pMax.x - pMin.x) / resolution.x) + 1.0);
	//this->h = int (floor((pMax.y - pMin.y) / resolution.y) + 1.0);
	//if ((this->w <= 0) || (this->h <= 0)) return false;
	
	this->w = wdth;
	this->h = hght;
	bool Ok(true);

	double demW=0, demH=0; 
	if (pDEM) //DEM will be used for mask generation
	{
		demW = this->pDEM->getWidth();
		demH = this->pDEM->getHeight();
	}	

	//initialize buffers for two masks
	this->gMask = new CImageBuffer;
	this->upMask = new CImageBuffer;

	//Ok&=this->gMask->set((long)pMin.x, (long)pMin.y, this->w, this->h, 1, 8, false, 0);
	//Ok&=this->upMask->set((long)pMin.x, (long)pMin.y, this->w, this->h, 1, 8, false,255);
	//this->w += 1;
	//this->h += 1;
	Ok&=this->gMask->set(0, 0, this->w, this->h, 1, 8, false, 0);
	Ok&=this->upMask->set(0, 0, this->w, this->h, 1, 8, false,255);

	//Ok&=this->gMask->set(0, 0, this->w, this->h, 1, 1, true, 0);
	//Ok&=this->upMask->set(0, 0, this->w, this->h, 1, 1, true, 1);

	if (!Ok) return false;

//====================================================	
	double yul = pMax.y;

	//extension of each tile in metres
	C2DPoint tileExt(resolution.x * this->patchSize, resolution.y * this->patchSize);

	C3DPoint min, max;

	min.z =    -1.0;
    max.z =    1e3;
    
	vector <double > Hvec;
    vector <double > Xvec;
	vector <double > Yvec;
	
	vector <int> ind;
    vector <int> bFreq;
	vector <int> tBin;
    vector <int>::iterator iter;

	int newCapacity		= 0;
	int level			= 0;
	
	CALSPoint ** pointvec ;
    
	long total = 0; long T = this->pALS->getNPoints();
	for(int TR = 0; TR < this->h; TR += this->patchSize, yul -= tileExt.y) 
	{//first go by height: up->down
		double xul = pMin.x;

		min.y = yul - tileExt.y ;
		max.y = yul ;

		
		for (int TC = 0; TC < this->w; TC += this->patchSize, xul  += tileExt.x)
		{//then go by width: left->right
			min.x = xul ;
			max.x = xul + tileExt.x ;
            this->nPoint=this->pALS->getPointCloud(min, max, pointvec, newCapacity, 2048,level);
			total += this->nPoint;
			if ( this->nPoint<= 0) 
			{ //this code seems useless
				int width = this->patchSize;
				int height = this->patchSize;
				if (TR + height > this->h) height = this->h - TR;
				if (TC + width > this->w ) width = this->w - TC;
			}
			else 
			{
				Hvec.clear();
                Xvec.clear();
				Yvec.clear();
				
				ind.clear();
				bFreq.clear();
    			tBin.clear();
									
				//int nEchos = pALS->getMaxEchos();
			    //CALSPoint ** pointvector = new CALSPoint * [this->nPoint];
				  
        		//for  (int i = 0; i < this->nPoint ; ++i) pointvector[i] = new CALSPoint(nEchos);

				unsigned int sumPoints=0,addindx=0;

				while (sumPoints < this->nPoint)
				{
					 //pointvector[sumPoints]= pointvec[addindx+sumPoints];
					 //CALSPoint * p = pointvector[sumPoints];
					 //double z=pointvector[sumPoints]->getCoordinateAt(2);
					 //double x=pointvector[sumPoints]->getCoordinateAt(0);
					 //double y=pointvector[sumPoints]->getCoordinateAt(1);
					double x = pointvec[addindx+sumPoints]->getCoordinateAt(0);
					double y = pointvec[addindx+sumPoints]->getCoordinateAt(1);
					double z = pointvec[addindx+sumPoints]->getCoordinateAt(2);

					 Xvec.push_back(x);
					 Yvec.push_back(y);
					 Hvec.push_back(z);
					 
	 				 sumPoints ++;
				}
				addindx =+ this->nPoint;
				
				//====================Height threshold calculation =============
				double gThresh;
				int nbr		= 2;
				if (!this->pDEM)//no DEM, so estimate height threshold using histograms of height values in ALS data
				{
					double maxH = *max_element(Hvec.begin(), Hvec.end());
					double minH = *min_element(Hvec.begin(), Hvec.end());
									
					int dBin	= 2;				
			        
					double nBin = floor(maxH-minH)/dBin;
									
					for (int i= (int)floor(minH); i<(int)floor(maxH); ++i) 
						tBin.push_back(i);
									
					iter=tBin.end();
					tBin.push_back(*(iter-1)+ dBin);


					for (int k=0;k < nBin;++k)
					{
					   for	(int hCount=0; hCount< (int)Hvec.size();++hCount) 
					   {
						   if(Hvec.at(hCount) >= tBin.at(k) && Hvec.at(hCount) < tBin.at(k+1))  ind.push_back(hCount);
					   }

					   bFreq.push_back(ind.size());
					   ind.clear();
					}
					
					int indx = distance(bFreq.begin(),find (bFreq.begin(), bFreq.end(), *max_element(bFreq.begin(), bFreq.end())));
					gThresh = tBin.at(indx) + heightThr;	
				}
				else //DEM is available, so use the average DEM value as height threshold
				{
					C3DPoint TopLeftObj(xul,yul,0), BotRightObj(xul+tileExt.x,yul-tileExt.y,0);
					C2DPoint TopLeftObs, BotRightObs;
					this->tfwDEM->transformObj2Obs(TopLeftObs,TopLeftObj);
					this->tfwDEM->transformObj2Obs(BotRightObs,BotRightObj);
					if (TopLeftObs.x < 0)
						TopLeftObs.x = 0;
					if (TopLeftObs.y < 0)
						TopLeftObs.y = 0;					
					if (BotRightObs.x > demW-1)
						BotRightObs.x = demW-1;
					if (BotRightObs.y > demH-1)
						BotRightObs.y = demH-1;

					int demTileWidth = (int)(BotRightObs.x-TopLeftObs.x+1), demTileHeight = (int)(BotRightObs.y-TopLeftObs.y+1);
					CImageBuffer demBuf;
					pDEM->getRect(demBuf,(int)TopLeftObs.y,demTileHeight,(int)TopLeftObs.x,demTileWidth);
					vector<float> mean, sdev, med, min, max, fact, diff;
					demBuf.getStatistics(0, min, max, mean, med, sdev);
					gThresh = mean.at(0) + heightThr;
				}
				
					//==================== Mask generation =============

					for (unsigned int hIndex=0; hIndex < Hvec.size(); ++hIndex)
					{
           				C3DPoint obj(Xvec.at(hIndex),Yvec.at(hIndex),Hvec.at(hIndex));
						C2DPoint obs;
						this->tfwImage->transformObj2Obs(obs,obj);
						
						if (obs.x-nbr > 0 && obs.y-nbr > 0 && obs.x+nbr < this->w && obs.y+nbr < this->h)
						{
							
							if ( Hvec.at(hIndex) >= gThresh)
							{
								for (int imI= (int)(obs.x-nbr); imI <= (int)(obs.x+nbr); ++imI)
								{
									for  (int imJ=(int)(obs.y-nbr); imJ <= (int)(obs.y+nbr); ++imJ)
									{
										unsigned long imIndx=imJ*this->w+imI;
										this->upMask->pixUChar(imIndx)=0;
									}
								}
							}
					    	

						   else //if ( Hvec.at(hIndex) < gThresh)
						   {
							   for (int imI=(int)(obs.x-nbr); imI <= (int)(obs.x+nbr); ++imI)
								{
									for  (int imJ=(int)(obs.y-nbr); imJ <= (int)(obs.y+nbr); ++imJ)
									{
										unsigned long imIndx=imJ*this->w+imI;
										this->gMask->pixUChar(imIndx)=255;
										//this->gMask->pixUChar(imIndx)=1;
										//int here = 1;
									}
								}
							}
				       
						}//if
  					}//for
				/*}
				else //DEM is available, so use the average DEM value as height threshold
				{
					//====================Height threshold calculation =============
					C3DPoint TopLeftObj(xul,yul,0), BotRightObj(xul+tileExt.x,yul+tileExt.y,0);
					C2DPoint TopLeftObs, BotRightObs;
					this->tfwDEM->transformObj2Obs(TopLeftObs,TopLeftObj);
					this->tfwDEM->transformObj2Obs(BotRightObs,BotRightObj);
					if (TopLeftObs.x < 0)
						TopLeftObs.x = 0;
					if (TopLeftObs.y < 0)
						TopLeftObs.y = 0;					
					if (BotRightObs.x > demW-1)
						BotRightObs.x = demW-1;
					if (BotRightObs.y > demH-1)
						BotRightObs.y = demH-1;

					int demTileWidth = (int)(BotRightObs.x-TopLeftObs.x+1), demTileHeight = (int)(BotRightObs.y-TopLeftObs.y+1);
					CImageBuffer demBuf;
					pDEM->getRect(demBuf,(int)TopLeftObs.y,demTileHeight,(int)TopLeftObs.x,demTileWidth);
					vector<float> mean, sdev, med, min, max, fact, diff;
					demBuf.getStatistics(0, min, max, mean, med, sdev);
					gThresh = mean.at(0) + heightThr;	

					int nbr		= 2;

					//==================== Mask generation =============
				}*/

			}//else
		}  //for
	}//for
	
	CCharString fn(this->projectPath);
	CCharString fnUpmask = fn+imUpMask->getFileNameChar()+".hug";
    CCharString fnGmask = fn+imGMask->getFileNameChar()+".hug";
	
	CLookupTable lut(8,3);
	bool ret=imUpMask->dumpImage(fnUpmask.GetChar(), *upMask, &lut);
	ret=imGMask->dumpImage(fnGmask.GetChar(), *gMask, &lut);
	if (!ret) return false;

	return ret;
};

void BuildingsDetector::initParamenetrs()
{	
	float res = (float)this->resolutionImage.x;

	binDist = 5;
	histSize = 180/binDist;
	minMax2MeanRatio = 4.0;
	minMax2MeanRatio2 = 3.0;
	minArea2TextureRatio = 45.0;
	minAggregateBinHeight = (float)9.0/res; //%9m = 90 pixels for knox & MV, 60 pixels for FF
	minAggregateBinHeight2 = (float)6.0/res;//%6m = 60 pixels for knox & MV, 40 pixels for FF
	minBinHeight = (float)3.0/res; //%3m = 30 pixels for knox & MV, 20 pixels for FF
	maxTotal = (float)90.0/res; //%90m = 900 pixels for knox & MV, 600 pixels for FF
}

void BuildingsDetector::findHistogram(CImageBuffer buf, CImageBuffer bufNum, int *H, int *Hind, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, long minX, long minY, long maxX, long maxY)
{	
	vector <HindType> HindAll;
	vector <int> CurveNumber;
	CurveNumber.clear();
	HindAll.clear();
	//hind.HistInd[0] = 0;
	int i, j, k, l;
	for (i = 0; i < this->histSize; i++)
	{
		H[i] = 0;
		Hind[i] = 0;
	}

//%             L4
//%        P1---------P4
//%    L1  |   P      |    
//%        |          |L3
//%       P2----------P3
//%             L2


	//long minX, minY, maxX, maxY, i, j;
	//findMinMax(P1,P2,P3,P4,minX,minY,maxX,maxY,hugImg->getWidth(),hugImg->getHeight());
	//if (minX 
	unsigned long wd, ht;
	wd = maxX-minX+1;
	ht = maxY-minY+1;

	//CImageBuffer buf;
	//hugImg->getRect(buf,minY,ht,minX,wd);
	//set(0, 0, this->w, this->h, 1, 8, false, 0);

	//long tPoint = 0;
	//double tValue = 0.0;

	CXYPoint P;
	double theta;
	long offX = buf.getOffsetX();
	long offY = buf.getOffsetY();
	int curnum = 0;
	//float *pixels = buf.getPixF();
	for (i = minX; i <= maxX; i++)
	{
		P.x = i;
		for (j = minY; j <= maxY; j++)
		{			
			P.y = j;
			if (isInsideRectangle(P1,P2,P3,P4,P))
			{	
				/*CXYPoint *Padd = xypts.add();
				Padd->x = P.x;
				Padd->y = P.y;
				Padd->setLabel("");*/

				//tPoint++;
				unsigned long rowInd, colInd;
				colInd = (unsigned long)P.x - offX;
				rowInd = (unsigned long)P.y - offY;
				unsigned long index = rowInd*wd + colInd;
				//float v = buf.pixUChar(index);
				//if (this->rgbImage->getBands() == 3)
				//	tValue += buf.pixUChar(index);
				//else // for 4 bands image
				//	tValue += buf.pixFlt(index);
				//double val = pixels[index];
				//tValue += pixels[index];

				double oriRadian = buf.pixFlt(index);
				int curveNum = (int)bufNum.pixFlt(index);

				if (curveNum > 0)
				{
					for (k = 0; k < curnum; k++)
						if (CurveNumber.at(k) == curveNum)
							break;
					if (k < curnum)//already added curve
					{
						//will update histogram at position k in Hind
					}
					else//new curve; add a new histogram to Hind
					{
						curnum++;
						CurveNumber.push_back(curveNum);
						HindType hind;
						for (l = 0; l <= 36; l++)
							hind.HistInd[l] = 0;
						HindAll.push_back(hind);
						k = curnum - 1;//will update the new histogram at position k in Hind
					}
					theta = 180*oriRadian/PI;
					theta = theta + 90; //% range: -90 to 90 is transferred to 0 to 180
					index = (long) ceil(theta/this->binDist)-1;
					if (index < 0)
						index = 0;
					else if (index >= this->histSize)
						index = this->histSize-1;
					
					H[index] += 1;
					HindAll.at(k).HistInd[index] += 1;
					HindAll.at(k).HistInd[36] += 1;//add to sum of histogram
				}
				
				//test
				//HindType hindcheck = HindAll.at(k);

				//int here = 1;
			}
		}
	}

	//%find longest individual line
	int maxLineLength = -1, maxID = -1;
	for (i = 0; (unsigned int)i < HindAll.size(); i++)
	{
		int thisLength = HindAll.at(i).HistInd[36];
		if (thisLength >= this->minBinHeight && thisLength > maxLineLength)
		{
			maxLineLength = thisLength;
			maxID = i;
		}
	}
	if (maxID > -1)
	{
		for (i = 0; i < 36; i++)
			Hind[i] = HindAll.at(maxID).HistInd[i];
	}
}

void BuildingsDetector::setBufferOfContourX(CImageBuffer &oriLineX, int *contour, int width)
 {
	//int width = contour->getPointCount();
	oriLineX.set(0,0,width,1,1,32,true);
	//oriLineY.set(0,0,width,1,1,32,true);

	for (int j = 0; j < width; j++)
	{
		//C2DPoint *p = contour->getPoint(j);
		oriLineX.pixFlt(j) = (float) contour[j];
		//oriLineY.pixFlt(j) = (float) p->y;

		float f = oriLineX.pixFlt(j);
	}
 }

void BuildingsDetector::extendContourX(CImageBuffer oriLineX, CImageBuffer &extLineX, int W)
 {
	int len = (int) oriLineX.getNGV();
	int L = len+2*W;
	extLineX.set(0,0,L,1,1,32,true);
	//extLineY.set(0,0,L,1,1,32,true);

	int i , index, j;
	//extend beginning
	float Vx1 = 2*oriLineX.pixFlt(0);
	//float Vy1 = 2*oriLineY.pixFlt(0);
	for (i = 0; i < W; i++)
	{
		index = W+1-i;
		extLineX.pixFlt(i) = Vx1 - oriLineX.pixFlt(index);
		//extLineY.pixFlt(i) = Vy1 - oriLineY.pixFlt(index);
		float f = extLineX.pixFlt(i);
	}

	//set line as it is in the middle
	for (i = 0; i < len; i++)
	{
		index = W+i;
		extLineX.pixFlt(index) = oriLineX.pixFlt(i);
		//extLineY.pixFlt(index) = oriLineY.pixFlt(i);
		float f = extLineX.pixFlt(index);
	}

	//extend at end
	Vx1 = 2*oriLineX.pixFlt(len-1);
	//Vy1 = 2*oriLineY.pixFlt(len-1);
	for (i = 0; i < W; i++)
	{
		index = len+W+i;
		j = len-2-i; 
		extLineX.pixFlt(index) = Vx1 - oriLineX.pixFlt(j);
		//extLineY.pixFlt(index) = Vy1 - oriLineY.pixFlt(j);
		float f = extLineX.pixFlt(index);
	}
 }

void BuildingsDetector::contractContourX(CImageBuffer smoothLineX, CImageBuffer &conSmoothX, int W)
 {
	int L = (int) smoothLineX.getNGV();
	int len = L-2*W;
	conSmoothX.set(0,0,len,1,1,32,true);
	//conSmoothY.set(0,0,len,1,1,32,true);

	int i, index;
	for (i = 0; i < len; i++)
	{
		index = W+i;
		conSmoothX.pixFlt(i) = smoothLineX.pixFlt(index);
		//conSmoothY.pixFlt(i) = smoothLineY.pixFlt(index);
		float f = conSmoothX.pixFlt(i);
	}
 }

void BuildingsDetector::setContourFromBufferX(CImageBuffer smoothLineX, float *cont)
 {
	int width = (int) smoothLineX.getNGV();
	//CXYContour *cont = xycontour.Add();
	for (int j = 0; j < width; j++)
	{
		//C2DPoint p;
		//p.x = smoothLineX.pixFlt(j);
		//p.y = smoothLineY.pixFlt(j);
		//cont->addPoint(p);
		cont[j] = smoothLineX.pixFlt(j);

		float f = smoothLineX.pixFlt(j);
	}
	//xycont.RemoveAt(index);
	//xycont.InsertAt(index,cont);
 }
vector <unsigned short> BuildingsDetector::findExtremum(float *H, float *Sm)
{	
	vector <unsigned short> ext;
	//find maxima points	
	unsigned short *extremum;
	unsigned short N = this->histSize;
	extremum = new unsigned short[N+2]; //maximum extremums
	

    int i,j,n = 0; //extremums counter
    float Search = 1;
    
    for (j = 0; j < N-1; j++)
	{
		//if ((K[j+1]-K[j])*Search > 0)
		if ((H[j+1]-H[j])*Search > 0)
		{			
            extremum[n] = j;
            Search = -Search;
			n = n+1;
		}
		*Sm += H[j];
	}
	*Sm += H[j];

	ext.clear();

    if (n > 0)
	{
		if (extremum[0] > 0)
			ext.push_back(0);
        for (i = 1; i < n; i += 2)
			ext.push_back(extremum[i]);

        if (extremum[n-1] < N-1 && Search == -1)
			ext.push_back(N-1);
	}


	if (extremum)
	{
		delete[] extremum;
		extremum = NULL;
	}

	return ext;
}
void BuildingsDetector::evaluateHistogram(int *H, float *retval)
{
	CImageFilter gau(eGauss,0,1,0.5); // sizeX = 0 for unknown, sizeY = 1 to be fixed at 1 and sigma = 0.5
	int W = gau.getOffsetX();
	float Hs[36];
	CImageBuffer oriH, extH, smoothH, conSmoothH;

	setBufferOfContourX(oriH,H,36);
	extendContourX(oriH,extH,W);
	gau.convolve(extH,smoothH,false);
	contractContourX(smoothH,conSmoothH,W);
	setContourFromBufferX(conSmoothH,Hs);

	float Sm = 0;
	vector <unsigned short> extremum = findExtremum(Hs,&Sm);//Sm = sum(Hs)

	unsigned short sz = extremum.size(), szsigbin2 = 0;

	//if (sz > 0)
	short i, j;
	unsigned short *extAngle = new unsigned short[sz];
	doubles Yext;
	doubles ids, idext;
	float hVal, hValo;
	double sum = 0;
	float maxH = 0;

	Yext.clear();
	ids.clear();
	idext.clear();
	for (i = 0; i < sz; i++)
	{
		extAngle[i] = (extremum.at(i)+1) * this->binDist;

		hVal = Hs[extremum.at(i)];
		//hValo = H[extremum.at(i)];

		//sum += hVal;
		
		Yext.push_back(hVal);
		
		//if (hValo > maxH)
		//	maxH = hValo;

		//if (hValo >= this->minAggregateBinHeight)
		//	idext.push_back(i);

		//if (hValo >= this->minAggregateBinHeight2)
		//	szsigbin2++;
	}
	doubles Yexto = Yext;
	Yext.sort(ids);//Yext: sorted in ascending order, ids contains original indices (of the unsorted elements)
	double mn = Sm/36;
	
	//find if any two maxima bins reside at 90 degree distant
	unsigned short angle90 = 0;
	vector <unsigned short> posAngle;
	posAngle.clear();
	unsigned int id;
	for (i = sz-1; i >= 0; i--)
	{
		id = (unsigned int)ids[i];
		if (Yext.at(i) > mn) //for peaks above the mean
		{
			posAngle.push_back(extAngle[id]);
		}
	}

	unsigned short szpos = posAngle.size();
	for (i = 0; i < szpos-1; i++)
	{
		unsigned short thisAngle = posAngle.at(i);
		for (j = i+1; j < szpos; j++)
		{
			float dist = (float)abs(posAngle.at(j) - thisAngle)/90;//angular distance = dist * 90
			if (dist == 1.0 || dist == 2.0)
				angle90++;
		}
	}
	

	//find the maximum bin height in H: if the maximum bin is the concatenation of first AND last bins
	if (sz > 1)
		if (extremum.at(0) == 0 && extremum.at(sz-1) == this->histSize-1 && H[0] >= this->minBinHeight && H[this->histSize-1] >= this->minBinHeight)
			if ((H[0] + H[this->histSize-1]) > maxH)
				maxH = (float)(H[0] + H[this->histSize-1]);

	//find extrema in original H
	extremum = findExtremum((float *)H,&Sm);//Sm = sum(H)
	sz = extremum.size();	
	for (i = 0; i < sz; i++)
	{	
		hValo = (float)H[extremum.at(i)];

		//sum += hVal;
		
		//Yext.push_back(hVal);
		
		if (hValo > maxH)
			maxH = hValo;

		if (hValo >= this->minAggregateBinHeight)
			idext.push_back(i);

		//if (hValo >= this->minAggregateBinHeight2)
		//	szsigbin2++;
	}

	//find average bin height in between all significant bins
	double avg = -1.0, sm = 0.0;
	unsigned short szsigbin = idext.size();

	if (szsigbin > 1)//if more than one extremum
	{
		for (i = 0; i < szsigbin-1; i++)//if we have 
		{
			unsigned short st = extremum.at((unsigned short)idext[i])+1;
			unsigned short en = extremum.at((unsigned short)idext[i+1])-1;
			if (st <= en)
			{
				double smloc = 0.0;
				for (j = st; j <= en; j++)
					smloc += H[j];
				sm += smloc/(en-st+1);
			}
			else //in the case of successive maxima in which case this is not a building
				sm += minAggregateBinHeight;           
		}
		avg = sm/(szsigbin-1);
	}


	//count sum(H>=minAggregateBinHeight2) and sum(H>=minAggregateBinHeight)
	szsigbin2 = 0;
	szsigbin = 0;
	for (i = 0; i < 36; i++)
	{
		if (H[i] >= this->minAggregateBinHeight2)
			szsigbin2++;
		if (H[i] >= this->minAggregateBinHeight)
			szsigbin++;
	}
	retval[0] = (float)angle90;
	retval[1] = (float)maxH;//max(H)
	if (mn > 0)
		retval[2] = (float)((double)maxH/mn);//max(H)/mean(H)
	else
		retval[2] = 0;
	retval[3] = (float)Sm;//sum(H)
	retval[4] = (float)avg;
	retval[5] = (float)szsigbin2;//sum(H>=minAggregateBinHeight2)
	retval[6] = (float)szsigbin;//sum(H>=minAggregateBinHeight)

	//delete allocated memories
	if (extAngle)
	{
		delete[] extAngle;
		extAngle = NULL;
	}
	/*if (Yext)
	{
		delete[] Yext;
		Yext = NULL;
	}*/

}
bool BuildingsDetector::validate_building(CHugeImage *orientationHugImage, CHugeImage *edgeHugImage, CXYPolyLine *build)
{
	int H[36];	
	int Hind[36];

	bool validated = false;
	CXYPoint P1, P2, P3, P4;
	P1 = build->getPoint(0);
	P2 = build->getPoint(1);
	P3 = build->getPoint(2);
	P4 = build->getPoint(3);

	long minX, minY, maxX, maxY;
	findMinMax(P1,P2,P3,P4,minX,minY,maxX,maxY,orientationHugImage->getWidth(),orientationHugImage->getHeight());
	//if (minX 
	unsigned long wd, ht;
	wd = maxX-minX+1;
	ht = maxY-minY+1;

	//from orientation histogram
	CImageBuffer buf;
	orientationHugImage->getRect(buf,minY,ht,minX,wd);
	//from edgeImage for edge numbers
	CImageBuffer bufNum;
	edgeHugImage->getRect(bufNum,minY,ht,minX,wd);

	findHistogram(buf, bufNum, H, Hind, P1, P2, P3, P4, minX, minY, maxX, maxY);

	float retval[7];
	evaluateHistogram(H,retval);

	//find area in pixels to texture pixel ratio
	/*C3DPoint p1obs(P1.x, P1.y, 0.0);
	C3DPoint p2obs(P2.x, P2.y, 0.0);
	C3DPoint p3obs(P3.x, P3.y, 0.0);
	C3DPoint P1obj,P2obj,P3obj;
	this->tfwImage->transformObs2Obj(P1obj, p1obs);
	this->tfwImage->transformObs2Obj(P2obj, p2obs);
	this->tfwImage->transformObs2Obj(P3obj, p3obs);

	float res = (float) abs(this->tfwImage->getA());//image resolution	
	double Lp = sqrt((P1obj.x-P2obj.x)*(P1obj.x-P2obj.x) + (P1obj.y-P2obj.y)*(P1obj.y-P2obj.y))/res;
    double Wp = sqrt((P3obj.x-P2obj.x)*(P3obj.x-P2obj.x) + (P3obj.y-P2obj.y)*(P3obj.y-P2obj.y))/res;
	*/
	double Lp = sqrt((P1.x-P2.x)*(P1.x-P2.x) + (P1.y-P2.y)*(P1.y-P2.y));
    double Wp = sqrt((P3.x-P2.x)*(P3.x-P2.x) + (P3.y-P2.y)*(P3.y-P2.y));
    double area2TexturePixelRatio = (Lp*Wp)/retval[3];// ret[3] = sum(H)

	//((area2TexturePixelRatio>= minArea2TextureRatio) || 
	//(sum(H>=minAggregateBinHeight2) > 1	&& ret(1,4) >= minMax2MeanRatio2) || 
	//(sum(H>=minAggregateBinHeight) > 1	&& ret(1,8) <= minAggregateBinHeight2) || 
	//(ret(1,2) >= minAggregateBinHeight	&& ret(1,7) <= maxTotal) || 
	//(ret(1,4) >= minMax2MeanRatio			&& ret(1,2) >= minBinHeight));
	if ((area2TexturePixelRatio >= this->minArea2TextureRatio) ||
		(retval[5] > 1								&& retval[2] >= minMax2MeanRatio2) || //retval[5] = sum(H>=minAggregateBinHeight2) and retval[2] = max(H)/mean(H)
		(retval[6] > 1								&& retval[4] <= this->minAggregateBinHeight2) || //retval[6] sum(H>=minAggregateBinHeight and retval[4] = avg: average bin height in between all significant bins
		(retval[1] >= this->minAggregateBinHeight	&& retval[3] <= this->maxTotal) || //retval[1] = max(H) and retval[3] = sum(H)
		(retval[2] >= this->minMax2MeanRatio		&& retval[1] >= this->minBinHeight))//retval[2]= max(H)/mean(H) and retval[1] = max(H)
	{
		validated = true;
	}
	else//this candidate could not be valiadated as a building using Hall, so we use not Hind
	{//(retInd(1,2) >= minBinHeight && ret(1,7) <= maxTotal)

		float retIndval[7];
		evaluateHistogram(Hind,retIndval); 

		if (retIndval[1] >= this->minBinHeight && retIndval[3] <= this->maxTotal)
		{
			validated = true;
		}
		
	}
	return validated;
}


bool BuildingsDetector::rectifyFinalBuildings(CHugeImage *orientationHugImage, CHugeImage *edgeHugImage, CXYPolyLineArray &xyedgesImg)
{	
	bool ret = true;

	if (this->listener)
	 {
		 this->listener->setTitleString("Rectifying candidate buildings ...");
		 this->listener->setProgressValue(0);
	 }

	int sz = xyedgesImg.GetSize();
	initParamenetrs();
	

	CCharString fn = this->projectPath + "buidling_detection_output_rectified.txt";
	FILE *fpout = fopen(fn,"w");
	
	CXYPoint P1, P2, P3, P4;
	C3DPoint obj;

	for (int i = 0; i < sz; i++)
	{
		CXYPolyLine *build = xyedgesImg.getAt(i);
		bool validated = validate_building(orientationHugImage, edgeHugImage, build);
		if (validated == false)
		{
			xyedgesImg.RemoveAt(i,1);
			i--;
			sz--;
		}
		else
		{
			P1 = build->getPoint(0);
			P2 = build->getPoint(1);
			P3 = build->getPoint(2);
			P4 = build->getPoint(3);

			//show
			//convert points to object coordinate system			
			//P1
			this->tfwImage->transformObs2Obj(obj,P1,0.0);
			fprintf(fpout,"%f\t%f\t",obj.x,obj.y);
			//this->tfwImage->transformObj2Obs(P1img,obj);
			//P2
			this->tfwImage->transformObs2Obj(obj,P2,0.0);
			fprintf(fpout,"%f\t%f\t",obj.x,obj.y);
			//this->tfwImage->transformObj2Obs(P2img,obj);
			//P3
			this->tfwImage->transformObs2Obj(obj,P3,0.0);
			fprintf(fpout,"%f\t%f\t",obj.x,obj.y);
			//this->tfwImage->transformObj2Obs(P3img,obj);
			//P4
			this->tfwImage->transformObs2Obj(obj,P4,0.0);
			fprintf(fpout,"%f\t%f\n",obj.x,obj.y);
			//this->tfwImage->transformObj2Obs(P4img,obj);
		}
		if (sz > 0)
			this->listener->setProgressValue(100*(i+1)/sz);
		else
			this->listener->setProgressValue(100);

		//test
		//FILE *fp = fopen("c:\\test.txt","w");
		//fprintf(fp,"%d\n",i);
		//fclose(fp);

		//if (i==3)
		//	int here = 1;
	}
	fclose(fpout);
	return ret;
}

void BuildingsDetector::findFinalBuildings(CHugeImage *gndMask, CHugeImage *upMask, CHugeImage *cropImg, CHugeImage *Iy, CHugeImage *Hi, CHugeImage *Sq, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CXYPolyLineArray &xyedgesImg, CHugeImage *entropyMask)
{

	if (this->listener)
	 {
		 this->listener->setTitleString("Extending candidate buildings ...");
		 this->listener->setProgressValue(1);
	 }

	vector <double> m12s, m23s;
	CDoubleVector inten, hue, sat, IThresh, Ihist, HThresh, Hhist, SThresh, Shist;
	reduceIniBuildingArea(m12s,m23s,xyedges);
	double dAL = 0.35/this->resolutionMask.x;
	this->edgeThresh = 0.4;
	double UpedgeThresh = 0.9;
	long iTotal, hTotal, sTotal;
	CXYPolyLine *poly, *poly1; 
	CXYPoint P1, P2, P3, P4, P1_1, P2_1, P3_1, P4_1;
	CXYPoint P1f, P2f, P3f, P4f;
	CXYPoint P1img, P2img, P3img, P4img;
	C3DPoint obj;
	unsigned long tot, rl; double perc;
	int i, ret;
	//bool Flag;
	double m12, m23;
	this->finalBuildings.RemoveAll();

	//CImageBuffer iSource, hSource, sSource;
	//Iy->getRect(iSource,0,Iy->getHeight(),0,Iy->getWidth());
	//Hi->getRect(hSource,0,Hi->getHeight(),0,Hi->getWidth());
	//Sq->getRect(sSource,0,Sq->getHeight(),0,Sq->getWidth());
	//FILE *fp = fopen("I:\\Buildings\\log.txt","w");
	//fprintf(fp,"total = %d\n",this->iniBuildings.GetSize());
	//fclose(fp);
	CCharString fn = this->projectPath + "buidling_detection_output.txt";
	FILE *fpout = fopen(fn,"w");
	//for (i = 0; i < 1; i++)
	int tini = this->iniBuildings.GetSize();
	for (i = 0; i < tini; i++)
	{
		//FILE *fp = fopen("I:\\Buildings\\log.txt","a");
		//Flag = true;
		m12 = m12s.at(i);
		m23 = m23s.at(i);

		// 4 points
		poly = this->iniBuildings.getAt(i);
		P1 = poly->getPoint(0);
		P2 = poly->getPoint(1);
		P3 = poly->getPoint(2);
		P4 = poly->getPoint(3);
		
		ret = checkMaxOverlaps(P1, P2, P3, P4); //returns maximum how many points are inside an already detected final buildings
		findPointsInside(upMask,P1,P2,P3,P4,tot,rl);
		perc = double(rl)/tot;
		if (ret < 4 && perc >= 0.7) // not all four points are inside an already detected final buildings
		{
			//findPointsInside(Iy,P1,P2,P3,P4,inten);
			//findPointsInside(Hi,P1,P2,P3,P4,hue);
			//findPointsInside(Sq,P1,P2,P3,P4,sat);

			//convert points to image coordinate system			
			//P1
			this->tfwMask->transformObs2Obj(obj,P1,0.0);
			this->tfwImage->transformObj2Obs(P1img,obj);
			//P2
			this->tfwMask->transformObs2Obj(obj,P2,0.0);
			this->tfwImage->transformObj2Obs(P2img,obj);
			//P3
			this->tfwMask->transformObs2Obj(obj,P3,0.0);
			this->tfwImage->transformObj2Obs(P3img,obj);
			//P4
			this->tfwMask->transformObs2Obj(obj,P4,0.0);
			this->tfwImage->transformObj2Obs(P4img,obj);

			/*poly1 = xyedgesImg.add();
			poly1->addPoint(P1img);
			poly1->addPoint(P2img);
			poly1->addPoint(P3img);
			poly1->addPoint(P4img);
			poly1->addPoint(P1img);*/

			findPointsInside(Iy,Hi,Sq,P1img,P2img,P3img,P4img,inten,hue,sat);

			iTotal = inten.GetSize();
			hTotal = hue.GetSize();
			sTotal = sat.GetSize();

			findHistogram(inten, IThresh, Ihist);
			findHistogram(hue, HThresh, Hhist);
			findHistogram(sat, SThresh, Shist);
			
			vector <CDoubleVector> iThresh = findThresh(IThresh, Ihist, iTotal);
			vector <CDoubleVector> hThresh = findThresh(HThresh, Hhist, hTotal);
			vector <CDoubleVector> sThresh = findThresh(SThresh, Shist, sTotal);
			
			//CHugeImage *IntBinary = new CHugeImage, *HueBinary = new CHugeImage, *SatBinary = new CHugeImage;
			//IntBinary->prepareWriting("intBinary",Iy->getWidth(),Iy->getHeight(),8,1);
			//HueBinary->prepareWriting("hueBinary",Hi->getWidth(),Hi->getHeight(),8,1);
			//SatBinary->prepareWriting("satBinary",Sq->getWidth(),Sq->getHeight(),8,1);
			//setBinaryMask(iSource,IntBinary,iThresh);
			//setBinaryMask(hSource,HueBinary,hThresh);
			//setBinaryMask(sSource,SatBinary,sThresh);

			//extendSegment(m23,0,P1,P2,P4,P1_1,P2_1,IntBinary,HueBinary,SatBinary,gndMask,upMask,UpedgeThresh,cropImg,xypts,dAL);
			//extendSegment(m12,0,P2,P3,P1,P2_1,P3_1,IntBinary,HueBinary,SatBinary,gndMask,upMask,UpedgeThresh,cropImg,xypts,dAL);
			//extendSegment(m23,0,P3,P4,P2,P3_1,P4_1,IntBinary,HueBinary,SatBinary,gndMask,upMask,UpedgeThresh,cropImg,xypts,dAL);
			//extendSegment(m12,0,P4,P1,P3,P4_1,P1_1,IntBinary,HueBinary,SatBinary,gndMask,upMask,UpedgeThresh,cropImg,xypts,dAL);

			extendSegment(m23,0,P1,P2,P4,P1_1,P2_1,Iy,Hi,Sq,gndMask,upMask,iThresh,hThresh,sThresh,UpedgeThresh,cropImg,xypts,dAL,xyedges,xyedgesImg,entropyMask);
			P1f = P1_1;
			extendSegment(m12,0,P2,P3,P1,P2_1,P3_1,Iy,Hi,Sq,gndMask,upMask,iThresh,hThresh,sThresh,UpedgeThresh,cropImg,xypts,dAL,xyedges,xyedgesImg,entropyMask);
			P2f = P2_1;
			extendSegment(m23,0,P3,P4,P2,P3_1,P4_1,Iy,Hi,Sq,gndMask,upMask,iThresh,hThresh,sThresh,UpedgeThresh,cropImg,xypts,dAL,xyedges,xyedgesImg,entropyMask);
			P3f = P3_1;
			extendSegment(m12,0,P4,P1,P3,P4_1,P1_1,Iy,Hi,Sq,gndMask,upMask,iThresh,hThresh,sThresh,UpedgeThresh,cropImg,xypts,dAL,xyedges,xyedgesImg,entropyMask);
			P4f = P4_1;

			//extendSegment(double m, double c, CXYPoint C1, CXYPoint P1, CXYPoint C2, CXYPoint &C1_1, CXYPoint &P1_1, CHugeImage *Iy, CHugeImage *Hi, CHugeImage *Sq, CHugeImage *gndMask, CHugeImage *upMask, vector <CDoubleVector> iThresh, vector <CDoubleVector> hThresh, vector <CDoubleVector> sThresh, double UpedgeThresh, CHugeImage *cropImg, CXYPointArray &xypts, double dAL)

			findExtendedRectangle(m12, m23, P1f, P2f, P3f, P4f, P1, P2, P3, P4);

			poly = this->finalBuildings.add();
			poly->addPoint(P1);
			poly->addPoint(P2);
			poly->addPoint(P3);
			poly->addPoint(P4);

			poly = xyedges.add();
			poly->addPoint(P1);
			poly->addPoint(P2);
			poly->addPoint(P3);
			poly->addPoint(P4);
			poly->addPoint(P1);

			//show
			//convert points to image coordinate system			
			//P1
			this->tfwMask->transformObs2Obj(obj,P1,0.0);
			fprintf(fpout,"%f\t%f\t",obj.x,obj.y);
			this->tfwImage->transformObj2Obs(P1img,obj);
			//P2
			this->tfwMask->transformObs2Obj(obj,P2,0.0);
			fprintf(fpout,"%f\t%f\t",obj.x,obj.y);
			this->tfwImage->transformObj2Obs(P2img,obj);
			//P3
			this->tfwMask->transformObs2Obj(obj,P3,0.0);
			fprintf(fpout,"%f\t%f\t",obj.x,obj.y);
			this->tfwImage->transformObj2Obs(P3img,obj);
			//P4
			this->tfwMask->transformObs2Obj(obj,P4,0.0);
			fprintf(fpout,"%f\t%f\n",obj.x,obj.y);
			this->tfwImage->transformObj2Obs(P4img,obj);

			poly1 = xyedgesImg.add();
			poly1->addPoint(P1img);
			poly1->addPoint(P2img);
			poly1->addPoint(P3img);
			poly1->addPoint(P4img);
			poly1->addPoint(P1img);

			

			 /*
			 if (IntBinary)
			 {
				 delete IntBinary;
				 IntBinary = NULL;
			 }

			 if (HueBinary)
			 {
				 delete HueBinary;
				 HueBinary = NULL;
			 }

			 if (SatBinary)
			 {
				 delete SatBinary;
				 SatBinary = NULL;
			 }*/

			//fprintf(fp,"%d = success.\n",i);
		}//if (ret < 4)
		//else
			//fprintf(fp,"%d = failure.\n",i);

		//fclose(fp);
		
		this->listener->setProgressValue(100*(i+1)/tini);
	}
	fclose(fpout);
}

void BuildingsDetector::extendSegment(double m, double c, CXYPoint C1, CXYPoint P1, CXYPoint C2, CXYPoint &C1_1, CXYPoint &P1_1, CHugeImage *Iy, CHugeImage *Hi, CHugeImage *Sq, CHugeImage *gndMask, CHugeImage *upMask, vector <CDoubleVector> iThresh, vector <CDoubleVector> hThresh, vector <CDoubleVector> sThresh, double UpedgeThresh, CHugeImage *cropImg, CXYPointArray &xypts, double dAL, CXYPolyLineArray &xyedges, CXYPolyLineArray &xyedgesImg, CHugeImage *entropyMask)
{
	double dAlThresh = 0.1/this->resolutionMask.x;
	//this->dAl = 1.5
	//double m = this->M.at(index);
	//double c = this->C.at(index);
	double m_left = -1/m;
	double c_left = P1.y - m_left*P1.x;
	//CXYPolyLine *poly, *poly1; 
	//four verstices of the ground mask
	//CXYPoint iP1(0,0), iP2(0,gndMask->getHeight()-1), iP3(gndMask->getWidth()-1,gndMask->getHeight()-1), iP4(gndMask->getWidth()-1,0);
	//CXYPoint Mp, P1pa, P2pa, P3pa, P4pa, mirP1, mirP3pa, mirP4pa;
	CXYPoint P1pa, P2pa, P3pa, P4pa;
	CXYPoint C1img, P1img, P1paimg, P2paimg, P3paimg, P4paimg;
	C3DPoint obj;
	double len1, len2;
	unsigned long tInt, tHue, tSat, tGnd, tAGnd, rInt, rHue, rSat, rGnd, rAGnd, i, k, tx, rx;
	double IntInfo, HueInfo, SatInfo, MgInfo, MInfo, PerX;
	CDoubleVector inten, hue, sat;
	//bool ret1, ret2, retp;
	double prev_MgInfo = 1.0;
    bool MgFlag = 0, execute;
	double meanNDVI;
	while(true)
	{
		//Mp.x = (C1.x+P1.x)/2;
		//Mp.y = (C1.y+P1.y)/2;

		//ret1 = isInsideRectangle(iP1,iP2,iP3,iP4,C1);
		//ret2 = isInsideRectangle(iP1,iP2,iP3,iP4,P1);
		//retp = isInsideRectangle(iP1,iP2,iP3,iP4,Mp);
		//if ((ret1 && retp) || (ret2 && retp))
		//	retp = true;
        //else
		//	retp = false;

		//if (retp) // if either C1 & mid-point or P1 & mid-point are within the ground mask
		//{
			 findRectVertices(m_left,c_left,C1,P1,P1pa, P2pa, P3pa, P4pa, dAL);

			/*poly = xyedges.add();
			poly->addPoint(P1pa);
			poly->addPoint(P2pa);
			poly->addPoint(P4pa);
			poly->addPoint(P3pa);
			poly->addPoint(P1pa);*/

			  //convert points to image coordinate system
			//C1
			this->tfwMask->transformObs2Obj(obj,C1,0.0);
			this->tfwImage->transformObj2Obs(C1img,obj);
			//P1
			this->tfwMask->transformObs2Obj(obj,P1,0.0);			
			this->tfwImage->transformObj2Obs(P1img,obj);
			//P1pa
			this->tfwMask->transformObs2Obj(obj,P1pa,0.0);
			this->tfwImage->transformObj2Obs(P1paimg,obj);
			//P2pa
			this->tfwMask->transformObs2Obj(obj,P2pa,0.0);
			this->tfwImage->transformObj2Obs(P2paimg,obj);
			//P3pa
			this->tfwMask->transformObs2Obj(obj,P3pa,0.0);
			this->tfwImage->transformObj2Obs(P3paimg,obj);
			//P4pa
			this->tfwMask->transformObs2Obj(obj,P4pa,0.0);
			this->tfwImage->transformObj2Obs(P4paimg,obj);

			/*poly1 = xyedgesImg.add();
			poly1->addPoint(P1paimg);
			poly1->addPoint(P2paimg);
			poly1->addPoint(P4paimg);
			poly1->addPoint(P3paimg);
			poly1->addPoint(P1paimg);*/

			  len1 = sqrt((C2.x-P1pa.x)*(C2.x-P1pa.x) + (C2.y-P1pa.y)*(C2.y-P1pa.y));
              len2 = sqrt((C2.x-P2pa.x)*(C2.x-P2pa.x) + (C2.y-P2pa.y)*(C2.y-P2pa.y));

			  //mirP1 = findMirrorPoint(m,c,P1);

			  if (len1 > len2)
			  {
				  //findPointsInside(IntBinary,P3pa,P1pa,C1,P1,tInt,rInt);
				  //findPointsInside(HueBinary,P3pa,P1pa,C1,P1,tHue,rHue);
				  //findPointsInside(SatBinary,P3pa,P1pa,C1,P1,tSat,rSat);
				  //findPointsInside(Iy,P3pa,P1pa,C1,P1,inten);
				  //findPointsInside(Hi,P3pa,P1pa,C1,P1,hue);
				  //findPointsInside(Sq,P3pa,P1pa,C1,P1,sat);
				  findPointsInside(Iy,Hi,Sq,P3paimg,P1paimg,C1img,P1img,inten,hue,sat);
				  findPointsInside(gndMask,upMask,P3pa,P1pa,C1,P1,tGnd,rGnd,tAGnd,rAGnd);
				  findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P3paimg,P1paimg,C1img,P1img,xypts,meanNDVI);
				  //findPointsInside(gndMask,P3pa,P1pa,C1,P1,tGnd,rGnd);
				  //findPointsInside(upMask,P3pa,P1pa,C1,P1,tAGnd,rAGnd);

				  //M_left = find_ALS(A,R,M,P3pa,P1pa,C1,P1);
                  //mirP3pa = findMirrorPoint(m,c,P3pa);
				  //findPointsInside(gndMask,mirP3pa,P1pa,C1,mirP1,tBelow,rBelow);
                  //M_oth = find_ALS(A,R,M,mirP3pa,P1pa,C1,mirP1);                                
				  
				  if (this->useEntropy)
				  {
					findPointsInside(entropyMask,P3paimg,P1paimg,C1img,P1img,tx,rx);				
					PerX = (double)(tx - rx)/tx;
				  }
			  }
              else // C2 and P2pa in opposite sides                                
			  {
				  //findPointsInside(IntBinary,P1,C1,P2pa,P4pa,tInt,rInt);
				  //findPointsInside(HueBinary,P1,C1,P2pa,P4pa,tHue,rHue);
				  //findPointsInside(SatBinary,P1,C1,P2pa,P4pa,tSat,rSat);
				  //findPointsInside(Iy,P1,C1,P2pa,P4pa,inten);
				  //findPointsInside(Hi,P1,C1,P2pa,P4pa,hue);
				  //findPointsInside(Sq,P1,C1,P2pa,P4pa,sat);				  
				  findPointsInside(Iy,Hi,Sq,P1img,C1img,P2paimg,P4paimg,inten,hue,sat);
				  findPointsInside(gndMask,upMask,P1,C1,P2pa,P4pa,tGnd,rGnd,tAGnd,rAGnd);
				  findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P1img,C1img,P2paimg,P4paimg,xypts,meanNDVI);
				  //findPointsInside(gndMask,P1,C1,P2pa,P4pa,tGnd,rGnd);
				  //findPointsInside(upMask,P1,C1,P2pa,P4pa,tAGnd,rAGnd);

				  //M_left = find_ALS(A,R,M,P1,C1,P2pa,P4pa);
                  //mirP4pa = findMirrorPoint(m,c,P4pa);
				  //findPointsInside(gndMask,mirP1,C1,P2pa,mirP4pa,tBelow,rBelow);
                  //M_oth = find_ALS(A,R,M,mirP1,C1,P2pa,mirP4pa);

				  if (this->useEntropy)
				  {
					findPointsInside(entropyMask,P1img,C1img,P2paimg,P4paimg,tx,rx);				
					PerX = (double)(tx - rx)/tx;
				  }
			  }
			  //nP = (tAbove > tBelow)?tAbove:tBelow;


			  tInt = inten.GetSize();
			  tHue = hue.GetSize();
			  tSat = sat.GetSize();
			  rInt = 0; rHue = 0; rSat = 0;
			  if (tInt>0)
			  {
				  for (i=0; i<tInt; i++)
				  {
					  double pixval = inten.GetAt(i);
					  for (k=0;k<iThresh.size();k++)
					 {
						 CDoubleVector T = iThresh.at(k);
						 if (pixval >= T.GetAt(0) && pixval <= T.GetAt(1))
						 {
							 rInt++;
							 break;
						 }
					 }
				  }

				  for (i=0; i<tHue; i++)
				  {
					  double pixval = hue.GetAt(i);
					  for (k=0;k<hThresh.size();k++)
					 {
						 CDoubleVector T = hThresh.at(k);
						 if (pixval >= T.GetAt(0) && pixval <= T.GetAt(1))
						 {
							 rHue++;
							 break;
						 }
					 }
				  }

				  for (i=0; i<tSat; i++)
				  {
					  double pixval = sat.GetAt(i);
					  for (k=0;k<sThresh.size();k++)
					 {
						 CDoubleVector T = sThresh.at(k);
						 if (pixval >= T.GetAt(0) && pixval <= T.GetAt(1))
						 {
							 rSat++;
							 break;
						 }
					 }
				  }

			  IntInfo = ((double)rInt)/tInt;
			  HueInfo = ((double)rHue)/tHue;
			  SatInfo = ((double)rSat)/tSat;
			  MgInfo = ((double)rGnd)/tGnd;
			  MInfo = ((double)rAGnd)/tAGnd;

              //MoInfo = ((double)rBelow)/nP;
              //if (Flag)
				//F = (MInfo >= edgeThresh) && (MoInfo < edgeThresh);
              //else
                //F = MInfo >= edgeThresh;

			   if (MgFlag == false && MgInfo < 0.1)
			   {
                    MgFlag = true;
			   }

			   if (this->useNDVIinExtension)
				   if (meanNDVI <= this->ndviThresh)
						execute = MInfo >= UpedgeThresh  && IntInfo >= this->edgeThresh && HueInfo >= this->edgeThresh && SatInfo >= this->edgeThresh;
				   else if (this->useEntropy)
						execute = MInfo >= UpedgeThresh  && IntInfo >= this->edgeThresh && HueInfo >= this->edgeThresh && SatInfo >= this->edgeThresh && PerX <= this->entropyThresh;
				   else
					   execute = false;
			   else
				   execute = MInfo >= UpedgeThresh  && IntInfo >= this->edgeThresh && HueInfo >= this->edgeThresh && SatInfo >= this->edgeThresh;
			   if (execute)
			   {
				   //if (len1 > len2)
					   //N_left = find_ALS(A,R,N,P3pa,P1pa,C1,P1);
					//   findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P3pa,P1pa,C1,P1,xypts,meanNDVI);
				   //else // C2 and P2pa in opposite sides
					//   findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P1,C1,P2pa,P4pa,xypts,meanNDVI);						
					   //N_left = find_ALS(A,R,N,P1,C1,P2pa,P4pa);                                          
				   if (MgFlag == false || (MgFlag == true && prev_MgInfo >= MgInfo))
				   {
					   prev_MgInfo = MgInfo;
					   if (len1 > len2)
					   {
						   C1 = P1pa;
                           P1 = P3pa;
					   }
					   else // C2 and P1pa in opposite sides
					   {
						   C1 = P2pa;
                           P1 = P4pa;
					   }
				   }
				   else
				   {
					   //dAL = dAL/2;                      
                       //if (dAL < 0.4)
						   break;
				   }
			   }
               else
			   {
				   dAL = dAL/2;
                   if (dAL < dAlThresh) //0.1)
						break;
                        
			   }           
		}
		else // stop extending C1
			break;
	}

	C1_1 = C1;
	P1_1 = P1;
}
void BuildingsDetector::findExtendedRectangle(double m12, double m23, CXYPoint P1_1, CXYPoint P2_2, CXYPoint P3_2, CXYPoint P4_2, CXYPoint &P1, CXYPoint &P2, CXYPoint &P3, CXYPoint &P4)
{
	double c12 = P1_1.y - m12*P1_1.x;
    double c23 = P2_2.y - m23*P2_2.x;
	double c34 = P3_2.y - m12*P3_2.x;
    double c41 = P4_2.y - m23*P4_2.x;
                    
                    P1.x = (c41-c12)/(m12-m23);
                    P1.y = m12*P1.x + c12;
                    
                    P2.x = (c23-c12)/(m12-m23);
                    P2.y = m12*P2.x + c12;
                    
                    P3.x = (c23-c34)/(m12-m23);
                    P3.y = m12*P3.x + c34;
                    
                    P4.x = (c41-c34)/(m12-m23);
                    P4.y = m12*P4.x + c34;
}
/*
void BuildingsDetector::extendSegment(double m, double c, CXYPoint C1, CXYPoint P1, CXYPoint C2, CXYPoint &C1_1, CXYPoint &P1_1, CHugeImage *IntBinary, CHugeImage *HueBinary, CHugeImage *SatBinary, CHugeImage *gndMask, CHugeImage *upMask, double UpedgeThresh, CHugeImage *cropImg, CXYPointArray &xypts, double dAL)
{
	double dAlThresh = 0.1/this->resolution.x;
	//this->dAl = 1.5
	//double m = this->M.at(index);
	//double c = this->C.at(index);
	double m_left = -1/m;
	double c_left = P1.y - m_left*P1.x;
	
	//four verstices of the ground mask
	//CXYPoint iP1(0,0), iP2(0,gndMask->getHeight()-1), iP3(gndMask->getWidth()-1,gndMask->getHeight()-1), iP4(gndMask->getWidth()-1,0);
	//CXYPoint Mp, P1pa, P2pa, P3pa, P4pa, mirP1, mirP3pa, mirP4pa;
	CXYPoint P1pa, P2pa, P3pa, P4pa;
	double len1, len2;
	unsigned long tInt, tHue, tSat, tGnd, tAGnd, rInt, rHue, rSat, rGnd, rAGnd;
	double IntInfo, HueInfo, SatInfo, MgInfo, MInfo;
	//bool ret1, ret2, retp;
	double prev_MgInfo = 1.0;
    bool MgFlag = 0;
	while(true)
	{
		//Mp.x = (C1.x+P1.x)/2;
		//Mp.y = (C1.y+P1.y)/2;

		//ret1 = isInsideRectangle(iP1,iP2,iP3,iP4,C1);
		//ret2 = isInsideRectangle(iP1,iP2,iP3,iP4,P1);
		//retp = isInsideRectangle(iP1,iP2,iP3,iP4,Mp);
		//if ((ret1 && retp) || (ret2 && retp))
		//	retp = true;
        //else
		//	retp = false;

		//if (retp) // if either C1 & mid-point or P1 & mid-point are within the ground mask
		//{
			 findRectVertices(m_left,c_left,C1,P1,P1pa, P2pa, P3pa, P4pa, dAL);

			  len1 = sqrt((C2.x-P1pa.x)*(C2.x-P1pa.x) + (C2.y-P1pa.y)*(C2.y-P1pa.y));
              len2 = sqrt((C2.x-P2pa.x)*(C2.x-P2pa.x) + (C2.y-P2pa.y)*(C2.y-P2pa.y));

			  //mirP1 = findMirrorPoint(m,c,P1);

			  if (len1 > len2)
			  {
				  findPointsInside(IntBinary,P3pa,P1pa,C1,P1,tInt,rInt);
				  findPointsInside(HueBinary,P3pa,P1pa,C1,P1,tHue,rHue);
				  findPointsInside(SatBinary,P3pa,P1pa,C1,P1,tSat,rSat);
				  findPointsInside(gndMask,P3pa,P1pa,C1,P1,tGnd,rGnd);
				  findPointsInside(upMask,P3pa,P1pa,C1,P1,tAGnd,rAGnd);

				  //M_left = find_ALS(A,R,M,P3pa,P1pa,C1,P1);
                  //mirP3pa = findMirrorPoint(m,c,P3pa);
				  //findPointsInside(gndMask,mirP3pa,P1pa,C1,mirP1,tBelow,rBelow);
                  //M_oth = find_ALS(A,R,M,mirP3pa,P1pa,C1,mirP1);                                
			  }
              else // C2 and P2pa in opposite sides                                
			  {
				  findPointsInside(IntBinary,P1,C1,P2pa,P4pa,tInt,rInt);
				  findPointsInside(HueBinary,P1,C1,P2pa,P4pa,tHue,rHue);
				  findPointsInside(SatBinary,P1,C1,P2pa,P4pa,tSat,rSat);
				  findPointsInside(gndMask,P1,C1,P2pa,P4pa,tGnd,rGnd);
				  findPointsInside(upMask,P1,C1,P2pa,P4pa,tAGnd,rAGnd);

				  //M_left = find_ALS(A,R,M,P1,C1,P2pa,P4pa);
                  //mirP4pa = findMirrorPoint(m,c,P4pa);
				  //findPointsInside(gndMask,mirP1,C1,P2pa,mirP4pa,tBelow,rBelow);
                  //M_oth = find_ALS(A,R,M,mirP1,C1,P2pa,mirP4pa);
			  }
			  //nP = (tAbove > tBelow)?tAbove:tBelow;


			  IntInfo = ((double)rInt)/tInt;
			  HueInfo = ((double)rHue)/tHue;
			  SatInfo = ((double)rSat)/tSat;
			  MgInfo = ((double)rGnd)/tGnd;
			  MInfo = ((double)rAGnd)/tAGnd;

              //MoInfo = ((double)rBelow)/nP;
              //if (Flag)
				//F = (MInfo >= edgeThresh) && (MoInfo < edgeThresh);
              //else
                //F = MInfo >= edgeThresh;

			   if (MgFlag == false && MgInfo < 0.1)
			   {
                    MgFlag = true;
			   }

			   if (MInfo >= UpedgeThresh  && IntInfo >= this->edgeThresh && HueInfo >= this->edgeThresh && SatInfo >= this->edgeThresh)
			   {
				   //if (len1 > len2)
					   //N_left = find_ALS(A,R,N,P3pa,P1pa,C1,P1);
					//   findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P3pa,P1pa,C1,P1,xypts,meanNDVI);
				   //else // C2 and P2pa in opposite sides
					//   findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P1,C1,P2pa,P4pa,xypts,meanNDVI);						
					   //N_left = find_ALS(A,R,N,P1,C1,P2pa,P4pa);                                          
				   if (MgFlag == false || (MgFlag == true && prev_MgInfo >= MgInfo))
				   {
					   prev_MgInfo = MgInfo;
					   if (len1 > len2)
					   {
						   C1 = P1pa;
                           P1 = P3pa;
					   }
					   else // C2 and P1pa in opposite sides
					   {
						   C1 = P2pa;
                           P1 = P4pa;
					   }
				   }
				   else
				   {
					   //dAL = dAL/2;                      
                       //if (dAL < 0.4)
						   break;
				   }
			   }
               else
			   {
				   dAL = dAL/2;
                   if (dAL < dAlThresh) //0.1)
						break;
                        
			   }           
		//}
		//else // stop extending C1
		//	break;
	}

	C1_1 = C1;
	P1_1 = P1;
}
*/
/*void BuildingsDetector::setBinaryMask(CImageBuffer source, CHugeImage *Binary, vector <CDoubleVector> Thresh)
 {
	 int i,j;
	 unsigned int k;
	 CImageBuffer *dest = new CImageBuffer;	 
	 int Wd = source.getWidth(), Ht = source.getHeight();
	 dest->set(0, 0, Wd, Ht, 1, 8, false, 0);	 
	 bool flag;
	 for (i=0;i<Ht;i++)
	 {
		 for (j=0;j<Wd;j++)
		 {
			 unsigned long index = i*Wd+j;
			 float pixval = source.pixFlt(index);
			 flag = false;
			 for (k=0;k<Thresh.size();k++)
			 {
				 CDoubleVector T = Thresh.at(k);
				 if (pixval >= T.GetAt(0) && pixval <= T.GetAt(1))
				 {
					 flag = true;
					 break;
				 }
			 }
			 if (flag)
				 dest->pixUChar(index) = 255;
			 else
				 dest->pixUChar(index) = 0;

		 }
	 }

	 CCharString fn(this->projectPath);
	 CCharString fnUpmask = fn+Binary->getFileNameChar()+".hug";
   

	 CLookupTable lut(8,3);
	 Binary->dumpImage(fnUpmask.GetChar(), *dest, &lut);

	 if (dest)
	 {
		 delete dest;
		 dest = NULL;
	 }
 }*/

vector <CDoubleVector> BuildingsDetector::findThresh(CDoubleVector hThresh, CDoubleVector Hhist, long T)
{
	vector <CDoubleVector> Thresh;
	CDoubleVector V;
	int N = Hhist.GetSize();
	if (N == 1)
	{
		V.Add(hThresh.GetAt(0));
		V.Add(hThresh.GetAt(1));
		Thresh.push_back(V);
		return Thresh;
	}
	//T = sum(Hhist,2);
	double PThresh = 0.97; //%percentage of pixels to be counted
	double PThresh_t = 0.9;
	//extremum = find_extremum(Hhist');

	//find maxima points	
	int *extremum;
	if (N>3)
		extremum = new int[N+2]; //maximum extremums
	else
		extremum = new int[5]; //minimum extremums
    int i,j,k,n = 0; //extremums counter
    float Search = 1;
    
    for (j = 0; j < N-1; j++)
	{
		//if ((K[j+1]-K[j])*Search > 0)
		if ((Hhist.GetAt(j+1)-Hhist.GetAt(j))*Search > 0)
		{			
            extremum[n] = j;
            Search = -Search;
			n = n+1;
		}
	}
    if (n%2 == 0)
	{		
        extremum[n] = N-1;
        n = n+1;
	}

	if (n>0)
	{
		if (extremum[n-1] < N-1)
		{
			extremum[n] = N-2;//[extremum N-1 N];
			n = n+1;
			extremum[n] = N-1;
			n = n+1;
		}
        
        if (extremum[0] > 0)
		{
			int *ext = new int[n];
			for (i=0; i<n; i++)//back up
				ext[i] = extremum[i];

            extremum[0] = 0; // [1 2 extremum];
			extremum[1] = 1; 
			
			//copy return
			for (i=0; i<n; i++)//back up
				extremum[i+2] = ext[i];

			n = n+2;

			if (ext)
			{
				delete[] ext;
				ext = NULL;
			}
		}
	}

	int ThSize = n/2;

	vector <CDoubleVector> S, STh;
	doubles Ssum;
	double sum, summ;
	S.clear();
	STh.clear();
	Thresh.clear();	
	V.Add(0);
	V.Add(0);
	for (i=0; i < ThSize; i++)
	{
		int stID = extremum[2*i];
		int enID = extremum[2*i+2];
		S.push_back(Hhist.GetDataInRange(stID,enID,sum));
		//S{i} = Hhist(1,extremum(1,2*i-1):extremum(1,2*i+1));
		//STh{i} = hThresh(1,extremum(1,2*i-1):extremum(1,2*i+1)+1);
		STh.push_back(hThresh.GetDataInRange(stID,enID+1,summ));
		//Ssum(i,1) = sum(S{i},2);
		Ssum.push_back(sum);
			
		Thresh.push_back(V);
	}

	if (ThSize == 0)
	{
        //Thresh = hThresh;
	}
    else
	{

    //[stSum indices] = sort(Ssum,'descend');
		doubles indices;
		doubles stSum = Ssum;
		stSum.sort(indices); // sort in ascending order: largest is at the end of list
		//indices contain original indices of sorted elements

		double Tsum = 0;
		for (i=ThSize-1; i>= 0; i--)
		{
			int id = (int) indices.at(i);
			double sm = stSum.at(i);
			CDoubleVector smHist = S.at(id);
			CDoubleVector smInd = STh.at(id);

			doubles stHist;
			doubles indx;
			//stHist = (doubles) smHist;
			for (k = 0; k < smHist.GetSize(); k++)
			{
				stHist.push_back(smHist.GetAt(k));
			}
			//[stHist indx] = sort(smHist,'descend');
			stHist.sort(indx);

			double tSum = 0; int mn = 9999, mx = -9999;
			for (j=stHist.size()-1; j>= 0; j--)
			{
				tSum += stHist.at(j);
				double per = tSum/sm;				
				if ((int)indx.at(j) < mn)
					mn = (int)indx.at(j);
				if ((int)indx.at(j) > mx)
					mx = (int)indx.at(j);
				if (per>PThresh)
					break;
				//end       
			}   
			//int indMat = indx.at(1,1:j);
			//mn = min(indMat);
			//mx = max(indMat);
			//mn = (int)indx.at(mn);
			//mx = (int)indx.at(mx);
			V.SetAt(0,smInd.GetAt(mn));
			V.SetAt(1,smInd.GetAt(mx+1));
			Thresh.at(i) = V;
			//Thresh.SetAt(i,smInd.GetAt(mn);
			//Thresh.SetAt(i,2) = smInd(1,mx+1);
			Tsum += sm;
			double Perc = Tsum/T;
			if (Perc>PThresh_t)
				break;
			//end
		}
	}

	if (extremum)
	{
		delete[] extremum;
		extremum = NULL;
	}
	return Thresh;
}

void BuildingsDetector::findHistogram(CDoubleVector Hue, CDoubleVector &hThresh, CDoubleVector &Hhist)
{
	double hsSize = 0.1;
	double multi = 10.0;
	
	double mn = multi * Hue.GetMinimum();
	double mx = multi * Hue.GetMaximum();

	double minH = floor(mn)/multi;
	double maxH = ceil(mx)/multi;

	hThresh.CleanUp();
	Hhist.CleanUp();
	hThresh.Add(minH);
	int nBin = 0;
	for (double d = minH+hsSize; d <= maxH; d += hsSize)
	{
		hThresh.Add(d);
		Hhist.Add(0);
		nBin++;
	}
	/*hThresh = [minH:hsSize:maxH];
	nBin = size(hThresh,2)-1;
	if (hThresh(1,nBin+1) < maxH)
		nBin = nBin+1;
		hThresh(1,nBin+1) = hThresh(1,nBin)+hsSize;
		//maxH = hThresh(1,nBin+1);
	end
	
	Hhist = zeros(1,nBin);

	for i = 1:nBin
		Hhist(1,i) = sum(Hue >= hThresh(1,i) & Hue < hThresh(1,i+1));
	end
		*/

	for (int i = 0; i < Hue.GetSize(); i++)
	{
		double v = Hue.GetAt(i);
		int toBin = (int) ((v-minH)/hsSize);
		Hhist.SetAt(toBin,Hhist.GetAt(toBin)+1);
	}
}



int BuildingsDetector::checkMaxOverlaps(CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4)
{	//returns maximum how many points are inside an already detected final buildings

	int ret = 0, i, j, k;
	int sz = this->finalBuildings.GetSize();
	CXYPoint p, p1, p3, CP, Pj1, Pj2, Pj3, Pj4, mp12_1, mp12_2, mp12_3, mp23_1, mp23_2, mp23_3, mp34_1, mp34_2, mp34_3, mp41_1, mp41_2, mp41_3;
	CXYPolyLine *poly; 
	vector <CXYPoint> cp;
	double difX, difY, cp_sqr;
	vector <int> id;
	if (sz > 0)
	{
		for (i = 0; i < sz; i++)
		{
			poly = this->finalBuildings.getAt(i);
			p1 = poly->getPoint(0);
			p3 = poly->getPoint(2); 
			p.x = (p1.x + p3.x)/2;
			p.y = (p1.y + p3.y)/2;
			cp.push_back(p);
		}

		CP.x = (P1.x + P3.x)/2;
		CP.y = (P1.y + P3.y)/2;

		for (j = 0; j < sz; j++)
		{
		//O = ones(size(Bground,1),1);
		//max_sqr_dist = max_buildingLength * max_buildingLength;
			difX = cp.at(j).x - CP.x;
			difY = cp.at(j).y - CP.y;
			cp_sqr = difX*difX + difY*difY;
			if (cp_sqr <= this->sqrMaxBuildingLength)
				id.push_back(j);
		}   
		//id = find(cp_sqr <= max_sqr_dist);
    /*    %%%    
        %plot([P1.x P2.x], [P1.y P2.y],'-g','LineWidth', 1); hold on;
        %plot([P2.x P3.x], [P2.y P3.y],'-g','LineWidth', 1); hold on;
        %plot([P3.x P4.x], [P3.y P4.y],'-g','LineWidth', 1); hold on;
        %plot([P4.x P1.x], [P4.y P1.y],'-g','LineWidth', 1); hold on;
        %%%*/
		for (k = 0; k < (int)id.size(); k++)
		{
			j = id.at(k);
            //%if (i ~= j && Flag(j,1) == 1)
			poly = this->finalBuildings.getAt(j);
			Pj1 = poly->getPoint(0); 
			Pj2 = poly->getPoint(1);  
			Pj3 = poly->getPoint(2);  
			Pj4 = poly->getPoint(3); 
                
                //mp12_1.x = (Pj1.x + Pj2.x)/2;		mp12_1.y = (Pj1.y + Pj2.y)/2;
                //mp12_2.x = (Pj1.x + mp12_1.x)/2;	mp12_2.y = (Pj1.y + mp12_1.y)/2;
                //mp12_3.x = (mp12_1.x + Pj2.x)/2;	mp12_3.y = (mp12_1.y + Pj2.y)/2;
            //    %plot(mp12_1.x,mp12_1.y,'+g'); plot(mp12_2.x,mp12_2.y,'+b'); plot(mp12_3.x,mp12_3.y,'+r');
                
                //mp23_1.x = (Pj3.x + Pj2.x)/2;		mp23_1.y = (Pj3.y + Pj2.y)/2;
                //mp23_2.x = (Pj3.x + mp23_1.x)/2;	mp23_2.y = (Pj3.y + mp23_1.y)/2;
               // mp23_3.x = (mp23_1.x + Pj2.x)/2;	mp23_3.y = (mp23_1.y + Pj2.y)/2;
           //     %plot(mp23_1.x,mp23_1.y,'+g'); plot(mp23_2.x,mp23_2.y,'+b'); plot(mp23_3.x,mp23_3.y,'+r');
                
               // mp34_1.x = (Pj3.x + Pj4.x)/2;		mp34_1.y = (Pj3.y + Pj4.y)/2;
                //mp34_2.x = (Pj3.x + mp34_1.x)/2;	mp34_1.y = (Pj3.y + mp34_1.y)/2;
                //mp34_3.x = (mp34_1.x + Pj4.x)/2;	mp34_1.y = (mp34_1.y + Pj4.y)/2;
          //      % plot(mp34_1.x,mp34_1.y,'+g'); plot(mp34_2.x,mp34_2.y,'+b'); plot(mp34_3.x,mp34_3.y,'+r');
                 
               // mp41_1.x = (Pj1.x + Pj4.x)/2;		mp41_1.y = (Pj1.y + Pj4.y)/2;
               // mp41_2.x = (Pj1.x + mp41_1.x)/2;	mp41_2.y = (Pj1.y + mp41_1.y)/2;
               // mp41_3.x = (mp41_1.x + Pj4.x)/2;	mp41_3.y = (mp41_1.y + Pj4.y)/2;
          //      % plot(mp41_1.x,mp41_1.y,'+g'); plot(mp41_2.x,mp41_2.y,'+b'); plot(mp41_3.x,mp41_3.y,'+r');
                 
          //      %ret = testInRectangles(P1, P2, P3, P4, [Pj1;Pj2;Pj3;Pj4;mp12_1;mp12_2;mp12_3;mp23_1;mp23_2;mp23_3;mp34_1;mp34_2;mp34_3;mp41_1;mp41_2;mp41_3]);
          //      %ret = testInRectangles_number(P1, P2, P3, P4, [Pj1;Pj2;Pj3;Pj4]);

                //ret = testInRectangles_number(Pj1,Pj2,Pj3,Pj4, [P1; P2; P3; P4]);
				int ret1 = 0;
				if (isInsideRectangle(Pj1,Pj2,Pj3,Pj4,P1)) ret1++;
				if (isInsideRectangle(Pj1,Pj2,Pj3,Pj4,P2)) ret1++;
				if (isInsideRectangle(Pj1,Pj2,Pj3,Pj4,P3)) ret1++;
				if (isInsideRectangle(Pj1,Pj2,Pj3,Pj4,P4)) ret1++;
				if (ret1 > ret)
					ret = ret1;
                
           /*      %%%    
                %plot([Pj1.x Pj2.x], [Pj1.y Pj2.y],'-b','LineWidth', 1); hold on;
                %plot([Pj2.x Pj3.x], [Pj2.y Pj3.y],'-b','LineWidth', 1); hold on;
                %plot([Pj3.x Pj4.x], [Pj3.y Pj4.y],'-b','LineWidth', 1); hold on;
                %plot([Pj4.x Pj1.x], [Pj4.y Pj1.y],'-b','LineWidth', 1); hold on;
                %%%
*/
                //if (ret > 0)                    
                    //ids = [ids; [j ret]]; 
                //end            
		}// for k
	}//if (sz > 0)
	return ret;
}

void BuildingsDetector::reduceIniBuildingArea(vector <double> &m12s, vector <double> &m23s, CXYPolyLineArray &xyedges)
{
	int i = 0;
	CXYPolyLine *poly; 
	CXYPoint P1, P2, P3, P4, Cp, Q1, Q2, Q3, Q4, Q12L, Q12R, Q23U, Q23D, Q34L, Q34R, Q41U, Q41D;
	double m12, m23, c12, c23, c34, c41, crop12, crop23, Y[2], Y_diff[2];
	m12s.clear(); m23s.clear();
	for (; i < this->iniBuildings.GetSize(); i++)
	{
		// 4 points
		poly = this->iniBuildings.getAt(i);
		P1 = poly->getPoint(0);
		P2 = poly->getPoint(1);
		P3 = poly->getPoint(2);
		P4 = poly->getPoint(3);

		// %centerpoint
		//BGroundInfo(i,1:2) = [(P1(1,1) + P3(1,1))/2 (P1(1,2) + P3(1,2))/2];
		Cp.x = (P1.x + P3.x)/2;
		Cp.y = (P1.y + P3.y)/2;

		//% m, c for P1P2 or P3P4
		m12 = (P2.y - P1.y)/(P2.x - P1.x); //% m12 or m34
		c12 = P1.y - m12*P1.x; //%c12
		c34 = P3.y - m12*P3.x; //%c34
		//% m, c for P2P3 or P4P1
		m23 = (P2.y - P3.y)/(P2.x - P3.x); //% m23 or m41
		c23 = P3.y - m23*P3.x; //%c23
		c41 = P4.y - m23*P4.x;//%c41
	    
		crop12 = this->iniBuildingReductionThresh * sqrt((P1.x-P2.x)*(P1.x-P2.x) + (P1.y-P2.y)*(P1.y-P2.y)); //%crop34
		crop23 = this->iniBuildingReductionThresh * sqrt((P3.x-P2.x)*(P3.x-P2.x) + (P3.y-P2.y)*(P3.y-P2.y)); //%crop41
	    
		//Cp = BGroundInfo(i,1:2);
		//plot(Cp.x, Cp.y, 'or');
		//m12 = BGroundInfo(i,3); %m34
		//c12 = BGroundInfo(i,4);
		//c34 = BGroundInfo(i,5);
		//m23 = BGroundInfo(i,6); %m41
		//c23 = BGroundInfo(i,7);
		//c41 = BGroundInfo(i,8);

		//process P1P2
		//[Q1 Q2 Q3 Q4] = find_rectVertices(m12,c12,crop23,P1,P2);     
		findRectVertices(m12,c12,P1,P2,Q1,Q2,Q3,Q4,crop23);
		Y[0] = m12*Cp.x + c12;
		Y_diff[0] = Y[0]-Cp.y;
		Y[1] = m12*Q1.x + c12;
		Y_diff[1] = Y[1]-Q1.y;
		if ((Y_diff[0] <= 0 && Y_diff[1] <= 0) || (Y_diff[0]  > 0 && Y_diff[1] > 0)) //% Cp & Q1 are on same side
		{
			Q12L = Q1;
			Q12R = Q3;        
		}
		else
		{
			Q12L = Q2;
			Q12R = Q4;
		}


		//process P2P3
		// [Q1 Q2 Q3 Q4] = find_rectVertices(m23,c23,crop12,P2,P3); 
		findRectVertices(m23,c23,P2,P3,Q1,Q2,Q3,Q4,crop12);
		//Y = m23*[Cp.x; Q1(1,1)] + c23;
		//Y_diff = Y-[Cp(1,2); Q1(1,2)];
		Y[0] = m23*Cp.x + c23;
		Y_diff[0] = Y[0]-Cp.y;
		Y[1] = m23*Q1.x + c23;
		Y_diff[1] = Y[1]-Q1.y;
		if ((Y_diff[0] <= 0 && Y_diff[1] <= 0) || (Y_diff[0]  > 0 && Y_diff[1] > 0))// % Cp & Q1 are on same side
		{
			Q23U = Q1;
			Q23D = Q3;
		}
		else
		{
			Q23U = Q2;
			Q23D = Q4;
		}

		//process P3P4
		//[Q1 Q2 Q3 Q4] = find_rectVertices(m12,c34,crop23,P4,P3);     
		findRectVertices(m12,c34,P4,P3,Q1,Q2,Q3,Q4,crop23);
		//Y = m12*[Cp(1,1); Q1(1,1)] + c34;
		//Y_diff = Y-[Cp(1,2); Q1(1,2)];
		Y[0] = m12*Cp.x + c34;
		Y_diff[0] = Y[0]-Cp.y;
		Y[1] = m12*Q1.x + c34;
		Y_diff[1] = Y[1]-Q1.y;
		if ((Y_diff[0] <= 0 && Y_diff[1] <= 0) || (Y_diff[0]  > 0 && Y_diff[1] > 0)) //% Cp & Q1 are on same side
		{
			Q34L = Q1;
			Q34R = Q3;        
		}
		else
		{
			Q34L = Q2;
			Q34R = Q4;
		}

		//process P4P1
		//[Q1 Q2 Q3 Q4] = find_rectVertices(m23,c41,crop12,P1,P4);     
		findRectVertices(m23,c41,P1,P4,Q1,Q2,Q3,Q4,crop12);
		//Y = m23*[Cp(1,1); Q1(1,1)] + c41;
		//Y_diff = Y-[Cp(1,2); Q1(1,2)];
		Y[0] = m23*Cp.x + c41;
		Y_diff[0] = Y[0]-Cp.y;
		Y[1] = m23*Q1.x + c41;
		Y_diff[1] = Y[1]-Q1.y;
		if ((Y_diff[0] <= 0 && Y_diff[1] <= 0) || (Y_diff[0]  > 0 && Y_diff[1] > 0)) //% Cp & Q1 are on same side
		{
			Q41U = Q1;
			Q41D = Q3;
		}
		else
		{
			Q41U = Q2;
			Q41D = Q4;
		}

		//y-axis cuts
		c12 = Q12L.y - m12*Q12L.x;
		c23 = Q23U.y - m23*Q23U.x;
		c34 = Q34L.y - m12*Q34L.x;
		c41 = Q41U.y - m23*Q41U.x;

		 P1.x = (c41-c12)/(m12-m23);
		 P1.y = m12*P1.x + c12;
		 //plot(P1.x, P1.y, 'ob');

		 P2.x = (c23-c12)/(m12-m23);
		 P2.y = m12*P2.x + c12;
		 //plot(P2.x, P2.y, 'ob');

		 P3.x = (c23-c34)/(m12-m23);
		 P3.y = m12*P3.x + c34;
		 //plot(P3.x, P3.y, 'ob');

		 P4.x = (c41-c34)/(m12-m23);
		 P4.y = m12*P4.x + c34;
		 //plot(P4.x, P4.y, 'ob');

		//update
		poly->removePoints(0,3);
		poly->addPoint(P1);
		poly->addPoint(P2);
		poly->addPoint(P3);
		poly->addPoint(P4);

		m12s.push_back(m12);
		m23s.push_back(m23);

		//add to show
		/*poly = xyedges.add();
		poly->addPoint(P1);
		poly->addPoint(P2);
		poly->addPoint(P3);
		poly->addPoint(P4);	
		poly->addPoint(P1);*/
	}
}

void BuildingsDetector::formInitialBuildings(CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CHugeImage *entropyMask)
{
	if (this->listener)
	 {
		 this->listener->setTitleString("Formig candidate buildings ...");
		 this->listener->setProgressValue(1);
	 }

	double dAL = 1.5/this->resolutionMask.x;
	doubles indices;
	doubles lengthArray = this->lengths;
	lengthArray.sort(indices); // sort in ascending order: largest is at the end of list
	//indices contain original indices of sorted elements

	vector <bool> Tag;
	int i, index;
	CXYPolyLine *poly; 
	CXYPoint P1, P2, P3, P4, Pin, Q1, Q2, Q3, Q4, Q1in, Q3in;
	double m12, c12, Y[2], Y_diff[2], m, c, Len1, Len2;
	//bool ret;
	Tag.clear();
	for (i = 0; i < (int)this->nLine; i++)
	{
		Tag.push_back(false);
	}

	this->iniBuildings.RemoveAll();
	for (i = this->nLine-1; i>=0; i--)
	//for (i = this->nLine-1; i>=this->nLine-5; i--)
	{
		index = (unsigned int)indices[i];
		if (!Tag.at(index) && this->isLineActive.at(index))  //if it is not used yet
		{
			if (!testInBuidlings(index,xyedges))
			{
				poly = xyedges.getAt(index);
				P1 = poly->getPoint(0);
				P2 = poly->getPoint(1);
				m12 = this->M.at(index);
				c12 = this->C.at(index);

				//process L34
                findRectVertices(m12,c12,P1,P2,Q1,Q2,Q3,Q4,dAL);

				//show
				/*CXYPolyLine rectVertices;
				rectVertices.addPoint(Q1);
				rectVertices.addPoint(Q2);
				rectVertices.addPoint(Q4);
				rectVertices.addPoint(Q3);
				rectVertices.addPoint(Q1);
				xyedges.add(rectVertices);*/
				//show

				Pin = this->insidePoints.getPoint(index);

                Y[0] = m12*Pin.x + c12;
				Y_diff[0] = Y[0] - Pin.y;

				Y[1] = m12*Q1.x + c12;
				Y_diff[1] = Y[1] - Q1.y;

                if ((Y_diff[0] <= 0 && Y_diff[1] <= 0) || (Y_diff[0]  > 0 && Y_diff[1] > 0)) //% both C1 of Seg2 and in-point of Seg1 are in same side
				{
                    Q1in = Q1;
                    Q3in = Q3;
                    
                    //Q2in = Q2;
                    //Q4in = Q4;                    
				}
                else
				{
                    Q1in = Q2;
                    Q3in = Q4;
                    
                    //Q2in = Q1;
                    //Q4in = Q3;                    
				}

				m = -1/m12;
				c = Q1in.y - m*Q1in.x;
				extendSegment(m, c, Q1in, Q3in, P1, gndMask, cropImg, xypts, dAL, entropyMask);
				P3 = Q3in; P4 = Q1in;
				extendSegment(m12, c12, P2, P3, P1, gndMask, cropImg, xypts, dAL, entropyMask);
				extendSegment(m12, c12, P4, P1, P2, gndMask, cropImg, xypts, dAL, entropyMask);

				Len1 = sqrt((P1.x-P4.x)*(P1.x-P4.x) + (P1.y-P4.y)*(P1.y-P4.y));
                Len2 = sqrt((P1.x-P2.x)*(P1.x-P2.x) + (P1.y-P2.y)*(P1.y-P2.y));

				if (Len1 < this->minBuildingLength || Len2 < this->minBuildingLength)
				{
                    //%discard Seg1 as building edge
                    //Tag(i,1) = 9999;                    
                    //bNumber = bNumber-1;
				}
                else
				{
                    /*ret1 = testInRectanglesAxisParallel(ImgRect1(1,:), ImgRect1(2,:), ImgRect1(3,:), ImgRect1(4,:), [P1;P2;P3;P4]);
                    if (ret1)
                       % plot([P1(1,1) P2(1,1)], [P1(1,2) P2(1,2)],'-k','LineWidth', 3); hold on;
                        %plot([P2(1,1) P3(1,1)], [P2(1,2) P3(1,2)],'-k','LineWidth', 3); hold on;
                        %plot([P3(1,1) P4(1,1)], [P3(1,2) P4(1,2)],'-k','LineWidth', 3); hold on;
                        %plot([P4(1,1) P1(1,1)], [P4(1,2) P1(1,2)],'-k','LineWidth', 3); hold on;   
                    end*/
					poly = this->iniBuildings.add();
					poly->addPoint(P1);
					poly->addPoint(P2);
					poly->addPoint(P3);
					poly->addPoint(P4);
					Tag.at(index) = true;

					//show
				/*CXYPolyLine rectVertices1;
				rectVertices1.addPoint(P1);
				rectVertices1.addPoint(P2);
				rectVertices1.addPoint(P3);
				rectVertices1.addPoint(P4);
				rectVertices1.addPoint(P1);
				xyedges.add(rectVertices1);*/
				//show
					//for show
					poly = xyedges.add();
					poly->addPoint(P1);
					poly->addPoint(P2);
					poly->addPoint(P3);
					poly->addPoint(P4);
					poly->addPoint(P1);
				}

			}
			else
				this->isLineActive.at(index) = false;
		}

		this->listener->setProgressValue(100*(this->nLine-i)/this->nLine);
	}//for loop


}

void BuildingsDetector::extendSegment(double m, double c, CXYPoint &C1, CXYPoint &P1, CXYPoint C2, CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xypts, double dAL, CHugeImage *entropyMask)
{
	double dAlThresh = 0.4/this->resolutionMask.x;
	//this->dAl = 1.5
	//double m = this->M.at(index);
	//double c = this->C.at(index);
	double m_left = -1/m;
	double c_left = P1.y - m_left*P1.x;
	
	//four verstices of the ground mask
	CXYPoint iP1(0,0), iP2(0,gndMask->getHeight()-1), iP3(gndMask->getWidth()-1,gndMask->getHeight()-1), iP4(gndMask->getWidth()-1,0);
	CXYPoint Mp, P1pa, P2pa, P3pa, P4pa, mirP1, mirP3pa, mirP4pa;
	CXYPoint C1img,P1img,P1paimg, P2paimg, P3paimg, P4paimg;
	C3DPoint obj;

	double len1, len2;
	unsigned long tAbove, rAbove, txAbove,rxAbove,txBelow,rxBelow;
	double MInfo, meanNDVI,abvPerX,blwPerX;
	bool ret1, ret2, retp;
	while(true)
	{
		Mp.x = (C1.x+P1.x)/2;
		Mp.y = (C1.y+P1.y)/2;

		ret1 = isInsideRectangle(iP1,iP2,iP3,iP4,C1);
		ret2 = isInsideRectangle(iP1,iP2,iP3,iP4,P1);
		retp = isInsideRectangle(iP1,iP2,iP3,iP4,Mp);
		if ((ret1 && retp) || (ret2 && retp))
			retp = true;
        else
			retp = false;

		if (retp) // if either C1 & mid-point or P1 & mid-point are within the ground mask
		{
			 findRectVertices(m_left,c_left,C1,P1,P1pa, P2pa, P3pa, P4pa, dAL);

			 //convert points to image coordinate system
			//C1
			this->tfwMask->transformObs2Obj(obj,C1,0.0);
			this->tfwImage->transformObj2Obs(C1img,obj);
			//P1
			this->tfwMask->transformObs2Obj(obj,P1,0.0);			
			this->tfwImage->transformObj2Obs(P1img,obj);
			//P1pa
			this->tfwMask->transformObs2Obj(obj,P1pa,0.0);
			this->tfwImage->transformObj2Obs(P1paimg,obj);
			//P2pa
			this->tfwMask->transformObs2Obj(obj,P2pa,0.0);
			this->tfwImage->transformObj2Obs(P2paimg,obj);
			//P3pa
			this->tfwMask->transformObs2Obj(obj,P3pa,0.0);
			this->tfwImage->transformObj2Obs(P3paimg,obj);
			//P4pa
			this->tfwMask->transformObs2Obj(obj,P4pa,0.0);
			this->tfwImage->transformObj2Obs(P4paimg,obj);

			  len1 = sqrt((C2.x-P1pa.x)*(C2.x-P1pa.x) + (C2.y-P1pa.y)*(C2.y-P1pa.y));
              len2 = sqrt((C2.x-P2pa.x)*(C2.x-P2pa.x) + (C2.y-P2pa.y)*(C2.y-P2pa.y));

			  //mirP1 = findMirrorPoint(m,c,P1);

			  if (len1 > len2)
			  {
				  findPointsInside(gndMask,P3pa,P1pa,C1,P1,tAbove,rAbove);
				  //M_left = find_ALS(A,R,M,P3pa,P1pa,C1,P1);
                  //mirP3pa = findMirrorPoint(m,c,P3pa);
				  //findPointsInside(gndMask,mirP3pa,P1pa,C1,mirP1,tBelow,rBelow);
                  //M_oth = find_ALS(A,R,M,mirP3pa,P1pa,C1,mirP1);                                
			  }
              else // C2 and P2pa in opposite sides                                
			  {
				  findPointsInside(gndMask,P1,C1,P2pa,P4pa,tAbove,rAbove);
				  //M_left = find_ALS(A,R,M,P1,C1,P2pa,P4pa);
                  //mirP4pa = findMirrorPoint(m,c,P4pa);
				  //findPointsInside(gndMask,mirP1,C1,P2pa,mirP4pa,tBelow,rBelow);
                  //M_oth = find_ALS(A,R,M,mirP1,C1,P2pa,mirP4pa);
			  }
			  //nP = (tAbove > tBelow)?tAbove:tBelow;

			  //MInfo = ((double)rAbove)/nP;
			  MInfo = ((double)rAbove)/tAbove;
              //MoInfo = ((double)rBelow)/nP;
              //if (Flag)
				//F = (MInfo >= edgeThresh) && (MoInfo < edgeThresh);
              //else
                //F = MInfo >= edgeThresh;

			   if (MInfo >= edgeThresh)
			   {
				   if (len1 > len2)
					   //N_left = find_ALS(A,R,N,P3pa,P1pa,C1,P1);
					   findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P3paimg,P1paimg,C1img,P1img,xypts,meanNDVI);
				   else // C2 and P2pa in opposite sides
					   findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P1img,C1img,P2paimg,P4paimg,xypts,meanNDVI);						
					   //N_left = find_ALS(A,R,N,P1,C1,P2pa,P4pa);                                          
				   if (meanNDVI <= this->ndviThresh)
					   if (len1 > len2)
					   {
						   C1 = P1pa;
                           P1 = P3pa;
					   }
					   else // C2 and P1pa in opposite sides
					   {
						   C1 = P2pa;
                           P1 = P4pa;
					   }
				   else if (this->useEntropy)
				   {
						if (len1 > len2)
						{
							findPointsInside(entropyMask,P3paimg,P1paimg,C1img,P1img,txAbove,rxAbove);				
							abvPerX = (double)(txAbove - rxAbove)/txAbove;
							if (abvPerX > this->entropyThresh)
							{
								dAL = dAL/2;                      
								if (dAL < dAlThresh)//0.4)
									break;
							}
							else
							{
								C1 = P1pa;
								P1 = P3pa;
							}
						}
						else
						{
							findPointsInside(entropyMask,P1img,C1img,P2paimg,P4paimg,txBelow,rxBelow);				
							blwPerX = (double)(txBelow - rxBelow)/txBelow;
							if (blwPerX > this->entropyThresh)
							{
								dAL = dAL/2;                      
								if (dAL < dAlThresh)//0.4)
									break;
							}
							else
							{
								C1 = P2pa;
								P1 = P4pa;
							}
						}
				   }
				   else
				   {
					   dAL = dAL/2;                      
                       if (dAL < dAlThresh)//0.4)
						   break;
				   }
			   }
               else
			   {
				   dAL = dAL/2;
                   if (dAL < dAlThresh)//0.4)
						break;
                        
			   }           
		}
		else // stop extending C1
			break;
	}
}

bool BuildingsDetector::testInBuidlings(int index, CXYPolyLineArray &xyedges)
{
	CXYPoint P1, P2, P3, P4, C1, C2, *Pmid, *PmidAbove;
	CXYPolyLine *poly; 
	int i = 0;
	poly = xyedges.getAt(index);					
	C1 = poly->getPoint(0);
	C2 = poly->getPoint(1);
	Pmid = this->midPoints.getAt(index);
	PmidAbove = this->insidePoints.getAt(index);
	for (; i < this->iniBuildings.GetSize(); i++)
	{
		poly = this->iniBuildings.getAt(i);
		P1 = poly->getPoint(0);
		P2 = poly->getPoint(1);
		P3 = poly->getPoint(2);
		P4 = poly->getPoint(3);

		if (this->isInsideRectangle(P1,P2,P3,P4,C1))
			return true;
		else if (this->isInsideRectangle(P1,P2,P3,P4,C2))
			return true;
		else if (this->isInsideRectangle(P1,P2,P3,P4,*Pmid))
			return true;
		else if (this->isInsideRectangle(P1,P2,P3,P4,*PmidAbove))
			return true;
	}
	return false;
}

void BuildingsDetector::adjustLinesWithImage(CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xyptsImg, CXYContourArray &xycontImg, CXYPolyLineArray &xyedgesImg, CXYPolyLineArray &xyedges)
{	//

//	this function has not been completed yet, 27 Oct 2011
//
	///

	if (this->listener)
	 {
		 this->listener->setTitleString("Adjusting mask lines using image lines ...");
		 this->listener->setProgressValue(1);
	 }

	int width = this->rgbImage->getWidth();
	int height = this->rgbImage->getHeight();
	
	//get image lines
	//convert image 2 grey scale
	CHugeImage *Grey= new CHugeImage;
	CCharString appDir = CSystemUtilities::getEnvVar("APPDATA") + CSystemUtilities::dirDelimStr + "Barista" + CSystemUtilities::dirDelimStr;
	Grey->prepareWriting(appDir + "Grey Scale", width, height, 8, 1);
	CLookupTable lut(8,3); Grey->setLookupTable(&lut);
	bool OK = this->rgbImage->RGBtoGrey(Grey);
	if (!OK)
	{			
		//QMessageBox::warning( this, "Edge orientation estimation was not successful!", "Conversion to grey scale image failed!");
		return;
	}
	//get xy contours	
	//CXYContourArray xycontour;
	CImageBuffer edgeBuf (0, 0, width, height, Grey->getBands(), 32, true,FLT_MIN);//(0, 0, this->width, this->height, this->bands, 16, true, 0);
	int curveLen = (int)(4.5/this->resolutionImage.x);
	Grey->detectAndTrackEdges(&xycontImg, &edgeBuf, curveLen);

	if (this->listener)
	 {
		 this->listener->setTitleString("Adjusting mask lines using image lines ...");
		 this->listener->setProgressValue(5);
	 }

	//get lines
	detectCornersAndLines(xyptsImg,xycontImg,xyedgesImg,true);
	unsigned int nLineImg = (unsigned int) xyedgesImg.GetSize();


	//adjust now
	CXYPolyLine *poly;
	int i, index, adsum;//, tid;
	unsigned int j;
	unsigned int total = 0, T = 0, Tv = 0;
	//CXYPointArray midPoints;
	CXYPoint C1, C2, P1, P2, Pin, P1c, P2c, Pinc, Q1, Q2, CP, Q1c, Q2c;//, P1, P2, P3, P4, *matP, P_avb1, P_avb2, midPoint, *P;
	vector <bool> F, V; // false: never, true: previously adjusted
	bool Fl;
	double difX, difY, cp_sqr, m, m1, theta, theta1, thetadiff, mper, thetaper, rot;
	vector <unsigned int> ids;
	doubles dSqr, indx;
	for (unsigned int idx = 0; idx < this->nLine; idx++)
	{
		//P = midPoints.add();		
		F.push_back(false);
		V.push_back(false);
		if (this->isLineActive.at(idx))
		{
			T++;
			//poly = xyedges.getAt(idx);					
			//C1 = poly->getPoint(0);
			//C2 = poly->getPoint(1);			
			//P->x = (C1.x + C2.x)/2;
			//P->y = (C1.y + C2.y)/2;
		}
	}

	double pi = 4.0*atan(1.0);
	CXYPoint midP, Pm;
	//for (i = 46; i>=46; i--)
	//FILE *fp = fopen("I:\\Buildings\\Adjustment.txt","w");
	//fclose(fp);
	for (i = this->nLine-1; i>=0; i--)
	{		
		if ((total + Tv) == T)
			break;
		index = i;
		if (this->isLineActive.at(index) && !V.at(index) && !F.at(index)) // if this line is still active and not visited yet
		{
			midP = this->midPoints.getPoint(index);
			ids.clear();// tid = 0;
			dSqr.clear();
			for (j = 0; j < nLineImg; j++)
			{
				//if (this->isLineActive.at(j))
				//{
					Pm = this->midPointsImg.getPoint(j);
					difX = midP.x - Pm.x;
					difY = midP.y - Pm.y;
					cp_sqr = difX*difX + difY*difY;
					if (cp_sqr <= this->sqrMaxBuildingLength)// && !F.at(j))
					{
						ids.push_back(j);
						dSqr.push_back(cp_sqr);
					}
				//}
			}
			dSqr.sort(indx);
			unsigned int idimg = (unsigned int) ids.at((unsigned int)indx.at(0));

			poly = xyedgesImg.getAt(idimg);					
			Q1 = poly->getPoint(0);
			Q2 = poly->getPoint(1);	
			adsum = 0;

			//to test
			//poly1 = edgesout.add();
			//poly1->addPoint(Q1);
			//poly1->addPoint(Q2);
			//test
			//for (k = 8; k < 9; k++)
			//for (k = 0; k < ids.size(); k++)
			//{
				//id = ids.at(k);
				//if (index != id)
				//{   //taking centre point of line id as axis-origin do the adjustment
					CP = this->midPoints.getPoint(index);
                    Q1c.x = Q1.x - CP.x;	Q1c.y = Q1.y - CP.y;
                    Q2c.x = Q2.x - CP.x;	Q2c.y = Q2.y - CP.y;

					
					m = (Q2c.x - Q1c.x)/(Q2c.y - Q1c.y);
                    theta = 180.0*atan(m)/pi;
                    mper = -1/m;
                    thetaper = 180.0*atan(mper)/pi;

					poly = xyedges.getAt(index);
					P1 = poly->getPoint(0);
                    P2 = poly->getPoint(1); 
					Pin = this->insidePoints.getPoint(index);

					//to test
					//poly1 = edgesout.add();
					//poly1->addPoint(P1);
					//poly1->addPoint(P2);
					//test

					P1c.x = P1.x - CP.x;	P1c.y = P1.y - CP.y;
                    P2c.x = P2.x - CP.x;	P2c.y = P2.y - CP.y;
                    Pinc.x = Pin.x - CP.x;	Pinc.y = Pin.y - CP.y;

					m1 = (P2c.x - P1c.x)/(P2c.y - P1c.y);
                    theta1 = 180.0*atan(m1)/pi;
                    thetadiff = abs(theta-theta1);


					Fl = false;
                    if (thetadiff <= thetaThresh || abs(thetadiff-180.0) <= thetaThresh) //% parallel to line at j of L1
					{
						rot = theta-theta1;
                        Fl = true;
                        adsum = adsum+1;
					}
                    else
					{
						thetadiff = abs(thetaper-theta1);
                        if (thetadiff <= thetaThresh || abs(thetadiff-180.0) <= thetaThresh) //% perpendicular to line at j of L1
						{
							rot = thetaper-theta1;
                            Fl = true;
                            adsum = adsum+1;
						}
					}

					double rotrad, cost, sint, rotmat[2][2], mm, cc;
					CXYPoint P1i, P2i, Pini, P1r, P2r, Pinr;
					if (Fl) //% rotate and adjust line at k
					{					
                        rotrad = pi*rot/180.0;
                        cost = cos(rotrad); sint = sin(rotrad);
                        rotmat[0][0] = cost;  rotmat[0][1] = sint; rotmat[1][0] = -sint; rotmat[1][1] = cost;
                    
                        P1i.x = rotmat[0][0]*P1c.x + rotmat[0][1]*P1c.y;
						P1i.y = rotmat[1][0]*P1c.x + rotmat[1][1]*P1c.y;
						P2i.x = rotmat[0][0]*P2c.x + rotmat[0][1]*P2c.y;
						P2i.y = rotmat[1][0]*P2c.x + rotmat[1][1]*P2c.y;
						Pini.x = rotmat[0][0]*Pinc.x + rotmat[0][1]*Pinc.y;
						Pini.y = rotmat[1][0]*Pinc.x + rotmat[1][1]*Pinc.y;

						//F(id(k,1),1) = 1; 
                        //%V(id(k,1),1) = V(id(k,1),1)+1; 
                        P1r.x = P1i.x + CP.x;		P1r.y = P1i.y + CP.y;
                        P2r.x = P2i.x + CP.x;		P2r.y = P2i.y + CP.y;
                        Pinr.x = Pini.x + CP.x;		Pinr.y = Pini.y + CP.y;
                        
						//update line
						F.at(index) = true;
						poly->removePoints(0,1);
						poly->addPoint(P1r);
						poly->addPoint(P2r);
						this->insidePoints.SetAt(index,Pinr);
						
						//to test
						//poly1 = edgesout.add();
						//poly1->addPoint(P1r);
						//poly1->addPoint(P2r);
						//test

                        mm = (P2r.y - P1r.y)/(P2r.x - P1r.x);
                        cc = P1r.y -  mm*P1r.x;

						this->M.at(index) = mm;
						this->C.at(index) = cc;
					}
					else
					{
						V.at(index) = true;
						Tv++;
						//make this line inactive
						//this->isLineActive.at(id) = false;
					} //%if Fl

				//}
			//}
			if (adsum>0 && !F.at(index))
			{
				adsum = adsum+1;
				F.at(index) = true;                     
			}
            total = total+adsum;
		}		

		this->listener->setProgressValue(100*(this->nLine-i)/this->nLine);
		//FILE *fp = fopen("I:\\Buildings\\Adjustment.txt","a");
		//fprintf(fp,"%d lines adjusted out of %d\n",total + Tv,this->nLine);
		//fclose(fp);
	}	
}
void BuildingsDetector::adjustLines(CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CXYPolyLineArray &edgesout)
{
	if (this->listener)
	 {
		 this->listener->setTitleString("Adjusting lines ...");
		 this->listener->setProgressValue(1);
	 }

	doubles indices;
	doubles lengthArray = this->lengths;
	lengthArray.sort(indices); // sort in ascending order: largest is at the end of list
	//indices contain original indices of sorted elements

	CXYPolyLine *poly, *poly1;
	int i, index, id, adsum;//, tid;
	unsigned int j, k;
	unsigned int total = 0, T = 0, Tv = 0;
	//CXYPointArray midPoints;
	CXYPoint C1, C2, P1, P2, Pin, P1c, P2c, Pinc, Q1, Q2, CP, Q1c, Q2c;//, P1, P2, P3, P4, *matP, P_avb1, P_avb2, midPoint, *P;
	vector <bool> F, V; // false: never, true: previously adjusted
	bool Fl;
	double difX, difY, cp_sqr, m, m1, theta, theta1, thetadiff, mper, thetaper, rot;
	vector <unsigned int> ids;
	for (unsigned int idx = 0; idx < this->nLine; idx++)
	{
		//P = midPoints.add();		
		F.push_back(false);
		V.push_back(false);
		if (this->isLineActive.at(idx))
		{
			T++;
			//poly = xyedges.getAt(idx);					
			//C1 = poly->getPoint(0);
			//C2 = poly->getPoint(1);			
			//P->x = (C1.x + C2.x)/2;
			//P->y = (C1.y + C2.y)/2;
		}
	}

	double pi = 4.0*atan(1.0);
	CXYPoint midP, Pm;
	//for (i = 46; i>=46; i--)
	//FILE *fp = fopen("I:\\Buildings\\Adjustment.txt","w");
	//fclose(fp);
	for (i = this->nLine-1; i>=0; i--)
	{		
		if ((total + Tv) == T)
			break;
		index = (unsigned int)indices[i];
		if (this->isLineActive.at(index) && !V.at(index)) // if this line is still active and not visited yet
		{
			midP = this->midPoints.getPoint(index);
			ids.clear();// tid = 0;
			for (j = 0; j < this->nLine; j++)
			{
				if (this->isLineActive.at(j))
				{
					Pm = this->midPoints.getPoint(j);
					difX = midP.x - Pm.x;
					difY = midP.y - Pm.y;
					cp_sqr = difX*difX + difY*difY;
					if (cp_sqr <= this->sqrMaxBuildingLength && !F.at(j))
						ids.push_back(j);
				}
			}

			poly = xyedges.getAt(index);					
			Q1 = poly->getPoint(0);
			Q2 = poly->getPoint(1);	
			adsum = 0;

			//to test
			poly1 = edgesout.add();
			poly1->addPoint(Q1);
			poly1->addPoint(Q2);
			//test
			//for (k = 8; k < 9; k++)
			for (k = 0; k < ids.size(); k++)
			{
				id = ids.at(k);
				if (index != id)
				{   //taking centre point of line id as axis-origin do the adjustment
					CP = this->midPoints.getPoint(id);
                    Q1c.x = Q1.x - CP.x;	Q1c.y = Q1.y - CP.y;
                    Q2c.x = Q2.x - CP.x;	Q2c.y = Q2.y - CP.y;

					
					m = (Q2c.x - Q1c.x)/(Q2c.y - Q1c.y);
                    theta = 180.0*atan(m)/pi;
                    mper = -1/m;
                    thetaper = 180.0*atan(mper)/pi;

					poly = xyedges.getAt(id);
					P1 = poly->getPoint(0);
                    P2 = poly->getPoint(1); 
					Pin = this->insidePoints.getPoint(id);

					//to test
					//poly1 = edgesout.add();
					//poly1->addPoint(P1);
					//poly1->addPoint(P2);
					//test

					P1c.x = P1.x - CP.x;	P1c.y = P1.y - CP.y;
                    P2c.x = P2.x - CP.x;	P2c.y = P2.y - CP.y;
                    Pinc.x = Pin.x - CP.x;	Pinc.y = Pin.y - CP.y;

					m1 = (P2c.x - P1c.x)/(P2c.y - P1c.y);
                    theta1 = 180.0*atan(m1)/pi;
                    thetadiff = abs(theta-theta1);


					Fl = false;
                    if (thetadiff <= thetaThresh || abs(thetadiff-180.0) <= thetaThresh) //% parallel to line at j of L1
					{
						rot = theta-theta1;
                        Fl = true;
                        adsum = adsum+1;
					}
                    else
					{
						thetadiff = abs(thetaper-theta1);
                        if (thetadiff <= thetaThresh || abs(thetadiff-180.0) <= thetaThresh) //% perpendicular to line at j of L1
						{
							rot = thetaper-theta1;
                            Fl = true;
                            adsum = adsum+1;
						}
					}

					double rotrad, cost, sint, rotmat[2][2], mm, cc;
					CXYPoint P1i, P2i, Pini, P1r, P2r, Pinr;
					if (Fl) //% rotate and adjust line at k
					{					
                        rotrad = pi*rot/180.0;
                        cost = cos(rotrad); sint = sin(rotrad);
                        rotmat[0][0] = cost;  rotmat[0][1] = sint; rotmat[1][0] = -sint; rotmat[1][1] = cost;
                    
                        P1i.x = rotmat[0][0]*P1c.x + rotmat[0][1]*P1c.y;
						P1i.y = rotmat[1][0]*P1c.x + rotmat[1][1]*P1c.y;
						P2i.x = rotmat[0][0]*P2c.x + rotmat[0][1]*P2c.y;
						P2i.y = rotmat[1][0]*P2c.x + rotmat[1][1]*P2c.y;
						Pini.x = rotmat[0][0]*Pinc.x + rotmat[0][1]*Pinc.y;
						Pini.y = rotmat[1][0]*Pinc.x + rotmat[1][1]*Pinc.y;

						//F(id(k,1),1) = 1; 
                        //%V(id(k,1),1) = V(id(k,1),1)+1; 
                        P1r.x = P1i.x + CP.x;		P1r.y = P1i.y + CP.y;
                        P2r.x = P2i.x + CP.x;		P2r.y = P2i.y + CP.y;
                        Pinr.x = Pini.x + CP.x;		Pinr.y = Pini.y + CP.y;
                        
						//update line
						F.at(id) = true;
						poly->removePoints(0,1);
						poly->addPoint(P1r);
						poly->addPoint(P2r);
						this->insidePoints.SetAt(id,Pinr);
						
						//to test
						poly1 = edgesout.add();
						poly1->addPoint(P1r);
						poly1->addPoint(P2r);
						//test

                        mm = (P2r.y - P1r.y)/(P2r.x - P1r.x);
                        cc = P1r.y -  mm*P1r.x;

						this->M.at(id) = mm;
						this->C.at(id) = cc;
					}
					else
					{
						V.at(id) = true;
						Tv++;
						//make this line inactive
						//this->isLineActive.at(id) = false;
					} //%if Fl

				}
			}
			if (adsum>0 && !F.at(index))
			{
				adsum = adsum+1;
				F.at(index) = true;                     
			}
            total = total+adsum;
		}		

		this->listener->setProgressValue(100*(this->nLine-i)/this->nLine);
		//FILE *fp = fopen("I:\\Buildings\\Adjustment.txt","a");
		//fprintf(fp,"%d lines adjusted out of %d\n",total + Tv,this->nLine);
		//fclose(fp);
	}	
}

void BuildingsDetector::extendLines(CHugeImage *gndMask, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CHugeImage *entropyMask)
{
	if (this->listener)
	 {
		 this->listener->setTitleString("Extending lines ...");
		 this->listener->setProgressValue(1);
	 }

	double dAL = 3.0/this->resolutionMask.x;
	doubles indices;
	doubles lengthArray = this->lengths;
	lengthArray.sort(indices); // sort in ascending order: largest is at the end of list
	//indices contain original indices of sorted elements
	
	int i, index;
	CXYPolyLine *poly;//, *poly1;
	CXYPoint C1, C2, P1, P2, P3, P4, *matP, P_avb1, P_avb2, Pm, midPointAbove; 
	double m,c,Y, Y_diff, Yp1, Y_diffp1;	
	//FILE *fp = fopen("I:\\Buildings\\Extension.txt","w");
	//fclose(fp);
	
	for (i = this->nLine-1; i>=0; i--)
	{
		//if ((this->nLine-i > 1400 && this->nLine-i < 1420) || (this->nLine-i > 1657 && this->nLine-i < 1680) || (this->nLine-i > 1775 && this->nLine-i < 1800) || (this->nLine-i > 2023 && this->nLine-i < 2050))
		//{
		//FILE *fp = fopen("I:\\Buildings\\Extension.txt","a");
		index = (unsigned int)indices[i];
		if (this->isLineActive.at(index))
		{
			poly = xyedges.getAt(index);					
			C1 = poly->getPoint(0);
			C2 = poly->getPoint(1);
			m = this->M.at(index);
			c = this->C.at(index);			
			matP = this->insidePoints.getAt(index);

			findRectVertices(m, c, C1, C2, P1, P2, P3, P4, dAL);

			Y = m*matP->x + c;
			Y_diff = Y-matP->y;
                    
			Yp1 = m*P1.x + c;
			Y_diffp1 = Yp1-P1.y;
                    
			if ((Y_diff < 0 && Y_diffp1 < 0) || (Y_diff > 0 && Y_diffp1 > 0))
			{
				P_avb1 = P1;
				P_avb2 = P3;                        
			}
			else
			{
				P_avb1 = P2;
				P_avb2 = P4;                        
			}

			extendSegment(index,C1,P_avb1,C2,gndMask,true,cropImg,xypts,dAL,entropyMask); // extend left
			extendSegment(index,C2,P_avb2,C1,gndMask,true,cropImg,xypts,dAL,entropyMask); // extend right

			//update
			poly->removePoints(0,1);
			poly->addPoint(C1);
			poly->addPoint(C2);
			this->lengths[index] = sqrt((C1.x-C2.x)*(C1.x-C2.x) + (C1.y-C2.y)*(C1.y-C2.y));
			Pm.x = (C1.x+C2.x)/2;
			Pm.y = (C1.y+C2.y)/2;
			this->midPoints.SetAt(index,Pm);
			midPointAbove = findMidPointAbove(C1,C2,m,c,matP);
			this->insidePoints.SetAt(index,midPointAbove);
			//test the above updates
			//poly1 = xyedges.getAt(index);
			//P1 = poly1->getPoint(0);
			//P2 = poly1->getPoint(1);
			//P3 = this->insidePoints.getAt(index);
			//test is okay
			//fprintf(fp,"line # %d extended out of %d\n",this->nLine-i,this->nLine);

			//test
			/*int here;
			if (this->nLine-i == 1400)
				here = 1;
			if (this->nLine-i == 1657)
				here = 2;
			if (this->nLine-i == 1775)
				here = 3;*/
			//test
		}

		this->listener->setProgressValue(100*(this->nLine-i)/this->nLine);
		//fclose(fp);
		//}//if
	}
	
	/*num[0] = 5;
	num[1] = 4;
	num[2] = 6;
	num[3] = 3;
	num[4] = 8;
	num[5] = 2;
	num[6] = 7;	
	doubles indices;
	num.sort(indices);*/
}

CXYPoint BuildingsDetector::findMidPointAbove(CXYPoint C1, CXYPoint C2, double m, double c, CXYPoint matP)
{
// parallel line going through matP
	CXYPoint midP, P;
	double m1, m2, c1, c2;
	midP.x = (C1.x+C2.x)/2;
	midP.y = (C1.y+C2.y)/2;

	m1 = m;
	c1 = matP.y - m1*matP.x;

	//perpendicular line through midP
	m2 = -1/m;
	c2 = midP.y - m2*midP.x;

	P.x = (c2-c1)/(m1-m2);
	P.y = m1*P.x+c1;

	return P;
}

void BuildingsDetector::extendSegment(unsigned int index, CXYPoint &C1, CXYPoint P1, CXYPoint C2, CHugeImage *gndMask, bool Flag, CHugeImage *cropImg, CXYPointArray &xypts, double dAL, CHugeImage *entropyMask)
{
	//double dAlThresh = 0.4/this->resolution.x;
	double dAlThresh = 1;//this->resolution.x;
	double m = this->M.at(index);
	double c = this->C.at(index);
	double m_left = -1/m;
	double c_left = P1.y - m_left*P1.x;
	
	//four verstices of the ground mask
	CXYPoint iP1(0,0), iP2(0,gndMask->getHeight()-1), iP3(gndMask->getWidth()-1,gndMask->getHeight()-1), iP4(gndMask->getWidth()-1,0);
	CXYPoint P1pa, P2pa, P3pa, P4pa, mirP1, mirP3pa, mirP4pa;
	CXYPoint C1img, P1img, P1paimg, P2paimg, P3paimg, P4paimg;
	C3DPoint obj;
	double len1, len2;
	unsigned long tAbove, rAbove, tBelow, rBelow, nP, txAbove, rxAbove, txBelow, rxBelow;
	double MInfo, MoInfo, meanNDVI, abvPerX, blwPerX;
	bool F;
	while(true)
	{
		if (isInsideRectangle(iP1,iP2,iP3,iP4,C1) && isInsideRectangle(iP1,iP2,iP3,iP4,P1)) // if both C1 and P1 are within the ground mask
		{
			 findRectVertices(m_left,c_left,C1,P1,P1pa, P2pa, P3pa, P4pa, dAL);

			  len1 = sqrt((C2.x-P1pa.x)*(C2.x-P1pa.x) + (C2.y-P1pa.y)*(C2.y-P1pa.y));
              len2 = sqrt((C2.x-P2pa.x)*(C2.x-P2pa.x) + (C2.y-P2pa.y)*(C2.y-P2pa.y));

			  mirP1 = findMirrorPoint(m,c,P1);

			  if (len1 > len2)
			  {
				  findPointsInside(gndMask,P3pa,P1pa,C1,P1,tAbove,rAbove);
				  //M_left = find_ALS(A,R,M,P3pa,P1pa,C1,P1);
                  mirP3pa = findMirrorPoint(m,c,P3pa);
				  findPointsInside(gndMask,mirP3pa,P1pa,C1,mirP1,tBelow,rBelow);
                  //M_oth = find_ALS(A,R,M,mirP3pa,P1pa,C1,mirP1);                                
			  }
              else // C2 and P2pa in opposite sides                                
			  {
				  findPointsInside(gndMask,P1,C1,P2pa,P4pa,tAbove,rAbove);
				  //M_left = find_ALS(A,R,M,P1,C1,P2pa,P4pa);
                  mirP4pa = findMirrorPoint(m,c,P4pa);
				  findPointsInside(gndMask,mirP1,C1,P2pa,mirP4pa,tBelow,rBelow);
                  //M_oth = find_ALS(A,R,M,mirP1,C1,P2pa,mirP4pa);
			  }
			  nP = (tAbove > tBelow)?tAbove:tBelow;

			  MInfo = ((double)rAbove)/nP;
              MoInfo = ((double)rBelow)/nP;
              if (Flag)
				F = (MInfo >= edgeThresh) && (MoInfo < edgeThresh);
              else
                F = MInfo >= edgeThresh;

			   if (F)
			   {
				   //convert points to image coordinate system
					//C1
					this->tfwMask->transformObs2Obj(obj,C1,0.0);
					this->tfwImage->transformObj2Obs(C1img,obj);
					//P1
					this->tfwMask->transformObs2Obj(obj,P1,0.0);			
					this->tfwImage->transformObj2Obs(P1img,obj);
					//P1pa
					this->tfwMask->transformObs2Obj(obj,P1pa,0.0);
					this->tfwImage->transformObj2Obs(P1paimg,obj);
					//P2pa
					this->tfwMask->transformObs2Obj(obj,P2pa,0.0);
					this->tfwImage->transformObj2Obs(P2paimg,obj);
					//P3pa
					this->tfwMask->transformObs2Obj(obj,P3pa,0.0);
					this->tfwImage->transformObj2Obs(P3paimg,obj);
					//P4pa
					this->tfwMask->transformObs2Obj(obj,P4pa,0.0);
					this->tfwImage->transformObj2Obs(P4paimg,obj);

				   if (len1 > len2)
					   //N_left = find_ALS(A,R,N,P3pa,P1pa,C1,P1);
					   findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P3paimg,P1paimg,C1img,P1img,xypts,meanNDVI);
				   else // C2 and P2pa in opposite sides
					   findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P1img,C1img,P2paimg,P4paimg,xypts,meanNDVI);						
					   //N_left = find_ALS(A,R,N,P1,C1,P2pa,P4pa);                                          
				   if (meanNDVI <= ndviThresh)
					   if (len1 > len2)
					   {
						   C1 = P1pa;
                           P1 = P3pa;
					   }
					   else // C2 and P1pa in opposite sides
					   {
						   C1 = P2pa;
                           P1 = P4pa;
					   }
				   else if (this->useEntropy)
				   {
						if (len1 > len2)
						{
							findPointsInside(entropyMask,P3paimg,P1paimg,C1img,P1img,txAbove,rxAbove);				
							abvPerX = (double)(txAbove - rxAbove)/txAbove;
							if (abvPerX > this->entropyThresh)
							{
								dAL = dAL/2;                      
								if (dAL < dAlThresh)//0.4)
									break;
							}
							else
							{
								C1 = P1pa;
								P1 = P3pa;
							}
						}
						else
						{
							findPointsInside(entropyMask,P1img,C1img,P2paimg,P4paimg,txBelow,rxBelow);				
							blwPerX = (double)(txBelow - rxBelow)/txBelow;
							if (blwPerX > this->entropyThresh)
							{
								dAL = dAL/2;                      
								if (dAL < dAlThresh)//0.4)
									break;
							}
							else
							{
								C1 = P2pa;
								P1 = P4pa;
							}
						}
				   }
				   else
				   {
					   dAL = dAL/2;                      
                       if (dAL < dAlThresh)//0.4)
						   break;
				   }
			   }
               else
			   {
				   dAL = dAL/2;
                   if (dAL < dAlThresh)//0.4)
						break;
                        
			   }           
		}
		else // stop extending C1
			break;
	}
}

CXYPoint BuildingsDetector::findMirrorPoint(double m, double c, CXYPoint P)
{
	CXYPoint mP;

	double m1 = -1/m;
	double c1 = P.y-m1*P.x;

	//find intersection point
	CXYPoint intP;
	intP.x = (c-c1)/(m1-m);
	intP.y = m*intP.x+c;

	//find mirror point
	mP.x = 2*intP.x - P.x;
	mP.y = 2*intP.y - P.y;

	return mP;
}

 bool BuildingsDetector::extractContours(CXYContourArray &xycont)
 {	
	 if (this->listener)
	 {
		 this->listener->setTitleString("Extracting contours ...");
		 this->listener->setProgressValue(1);
	 }
	CImageBuffer edgePixelBufferFrom(*this->gMask);
	CImageBuffer edgePixelBufferTo;
	edgePixelBufferFrom.markEdgePixels(edgePixelBufferTo);
	CXYContourArray xycontour;
	bool ret = edgePixelBufferTo.getContours(xycontour);

/*	CLabelImage *LblImage;
	LblImage->initFromMask(*this->gMask, 1);
	CXYContourArray xycontour;
	bool ret = LblImage->getContours(xycontour,1,false);*/

	this->listener->setProgressValue(75);

	xycont.RemoveAll();
	for (int i = 0; i < xycontour.GetSize(); i++)
	{
		CXYContour *contour = xycontour.GetAt(i);
		if (contour->getPointCount() > minContourLength)
		{
			CXYContour *cont = xycont.Add();
			*cont = *contour;	
		}
	}

	this->listener->setProgressValue(100);
	return ret;
 }

void BuildingsDetector::findBlackPix(C2DPoint *p, long &xb, long &yb, long &up, long &dn, long &lf, long &rt)
{
	long y = (long)p->y, x = (long)p->x;
	long index = this->gMask->getIndex(y,x);
	unsigned char maskVal = this->gMask->pixUChar(index);
	long nPixel = this->gMask->getNGV();

	//check neighbour of p until we get a black pixel
	int nn = 0; long stx, enx, sty, eny, c, r;
	while (maskVal > 0)
	{
		nn = nn+1;
        stx = x-nn;
        enx = x+nn;
        sty = y-nn;
        eny = y+nn;
        if (stx < 0)
			stx = 0;            
        if (sty < 0)
            sty = 0;
		if (enx >= this->w)
            enx = this->w-1;
        if (eny >= this->h)
            eny = this->h-1;
        
        for (r = sty; r <= eny; r++)
		{
			for (c = stx; c <= enx; c++)
			{
                index = this->gMask->getIndex(r,c);
				maskVal = this->gMask->pixUChar(index);
                if (maskVal == 0)
				{
					xb = c;
                    yb = r;
                    break;
				}
			} //for c
            if (maskVal == 0)
				break;
		}//for r
	}//while

	//now we have the nearest black pixel (xb,yb)
	
	//find nearest wihte pixel on its above
    up = yb-1;
	index = this->gMask->getIndex(up,xb);
	if (index >= 0 && index < nPixel)
		maskVal = this->gMask->pixUChar(index);
    while (up >= 0 && maskVal == 0)
	{
        up = up-1;
		index = this->gMask->getIndex(up,xb);
		if (index >= 0 && index < nPixel)
			maskVal = this->gMask->pixUChar(index);
	}
    //find nearest wihte pixel on its down
    dn = yb+1;
	index = this->gMask->getIndex(dn,xb);
	if (index >= 0 && index < nPixel)
		maskVal = this->gMask->pixUChar(index);
    while (dn < this->h && maskVal == 0)
	{
        dn = dn+1;
		index = this->gMask->getIndex(dn,xb);
		if (index >= 0 && index < nPixel)
			maskVal = this->gMask->pixUChar(index);
	}
    //find nearest wihte pixel on its left
    lf = xb-1;
	index = this->gMask->getIndex(yb,lf);
	if (index >= 0 && index < nPixel)
		maskVal = this->gMask->pixUChar(index);
    while (lf >= 0 && maskVal == 0)
	{
        lf = lf-1;
		index = this->gMask->getIndex(yb,lf);
		if (index >= 0 && index < nPixel)
			maskVal = this->gMask->pixUChar(index);
	}
    //find nearest wihte pixel on its right
    rt = xb+1;
	index = this->gMask->getIndex(yb,rt);
	if (index >= 0 && index < nPixel)
		maskVal = this->gMask->pixUChar(index);
	while (rt < this->w && maskVal == 0)
	{
        rt = rt+1;
		index = this->gMask->getIndex(yb,rt);
		if (index >= 0 && index < nPixel)
			maskVal = this->gMask->pixUChar(index);
	}
    
    lf = lf+1;
    rt = rt-1;
    up = up+1;
    dn = dn-1;
}

void BuildingsDetector::updateIntersectedCurveID(long row, long col, CXYContourArray &xycont, unsigned int *curveNums, int curCurvID)
{
	int Sz = xycont.GetSize();
	int curCurvNum = curveNums[curCurvID];
    int newCurvNum = -1;
    //int id = -1;
    int max_sqr_dist = 4; //intersection at maximum 2 pixels distant
	int i,j;
	//long x = row;
	//long y = col;
	CXYContour *contour;
	C2DPoint *p;
	int nPoint;
	long diffX, diffY;
	long sqrDist;
    for (i = 0; i < Sz; i++)
	{
		if (i != curCurvID && curveNums[i] != curCurvNum)
		{
			contour = xycont.GetAt(i);
			nPoint = contour->getPointCount();
			for (j = 0; j < nPoint; j++)
			{
				p = contour->getPoint(j);
				diffX = (long)(p->x - col);
				diffY = (long)(p->y - row);
				sqrDist = diffX*diffX + diffY*diffY;
				if (sqrDist <= max_sqr_dist)
				{
					newCurvNum = curveNums[i];
                    //get the smaller of two as new curve number for both curCurvID and i
                    if (newCurvNum < curCurvNum)                        
						curveNums[curCurvID] = newCurvNum;
                    else
					{
                        curveNums[i] = curCurvNum;                        
					}
					break;
				}
			}//for j			
		}//if (i != curCurvID && curveNums[i] != curCurvNum)
		if (newCurvNum >= 0)
			break;
	}//for i
}

void BuildingsDetector::getCurveIDs(CXYContourArray &xycont, unsigned int *IDs)
{
	int i,Sz;
	Sz = xycont.GetSize();
	for (i = 0; i < Sz; i++)
	{
		CXYContour *contour = xycont.GetAt(i);
		int nPoint = contour->getPointCount();
		long xb, yb, up, dn, lf, rt;

		//check with the start point of curve i
		C2DPoint *p = contour->getPoint(0);
		/*% find vertical & horizontal lines passing through the nearest black pixel of this point 
    % and these two lines indicates maxium extents of black areas in the
    % ground mask, along & across this point
    % (xb,yb) = nearest black pixel, up = vertical lines's up point's x
    % value, dn = vertical lines's  down point's x value, lf = horizontal
    % lines's left point's y value, rt =  horizontal lines's right point's y value
	*/
		findBlackPix(p, xb, yb, up, dn, lf, rt);
		/* if vertical lines's up point is more than two pixels away from this
    % curve end, check if this up point intersects any curve other than
    % curve i
	*/
		if ((yb - up) > 2)		
			updateIntersectedCurveID(up, xb, xycont, IDs, i); //row, col, edges, IDs to be updated, current line i
		//check same for other ends of vertical & horizontal lines
		if ((dn - yb) > 2)		
			updateIntersectedCurveID(dn, xb, xycont, IDs, i); //row, col, edges, IDs to be updated, current line i
		if ((xb - lf) > 2)		
			updateIntersectedCurveID(yb, lf, xycont, IDs, i); //row, col, edges, IDs to be updated, current line i
		if ((rt - xb) > 2)		
			updateIntersectedCurveID(yb, rt, xycont, IDs, i); //row, col, edges, IDs to be updated, current line i		


		//check the same with the end point of curve i
		p = contour->getPoint(nPoint-1);
		findBlackPix(p, xb, yb, up, dn, lf, rt);
		if ((yb - up) > 2)		
			updateIntersectedCurveID(up, xb, xycont, IDs, i); //row, col, edges, IDs to be updated, current line i		
		if ((dn - yb) > 2)		
			updateIntersectedCurveID(dn, xb, xycont, IDs, i); //row, col, edges, IDs to be updated, current line i
		if ((xb - lf) > 2)		
			updateIntersectedCurveID(yb, lf, xycont, IDs, i); //row, col, edges, IDs to be updated, current line i
		if ((rt - xb) > 2)		
			updateIntersectedCurveID(yb, rt, xycont, IDs, i); //row, col, edges, IDs to be updated, current line i	
	}
}

 bool BuildingsDetector::detectCornersAndLines(CXYPointArray &xypts, CXYContourArray &xycont, CXYPolyLineArray &xyedges, bool imageMode)
 {
	 if (this->listener)
	 {
		 this->listener->setTitleString("Extracting corners & lines ...");
		 this->listener->setProgressValue(0);
	 }

	 //find polygonal approximations of contours
	/* if (xycont.GetSize())
	{
		xyedges.RemoveAll();

		CLabel EdgeLabel("L");
		EdgeLabel += 0; 
		double sigma0 = 0.33f;
		float probability = 0.95f;

		statistics::setNormalQuantil (probability);
		statistics::setChiSquareQuantils(probability);

		CXYContourArray localContours(xycont);
//		localContours.connectContours();
		for (long i = 0; i < localContours.GetSize(); ++i)
		{
			CXYContour *contour = localContours.GetAt(i);
			CXYPolyLine poly;
			poly.setLabel(EdgeLabel);
			++EdgeLabel;
			poly.initFromContour(contour, 2.5, sigma0);
			xyedges.add(poly);
		};
	};*/
	 //return true;

	 //find curve IDs
	 int i,Sz;
	 Sz = xycont.GetSize();
	 //unsigned int *IDs = new unsigned int[Sz];
	 //for (i = 0; i < Sz; i++)
	//	 IDs[i] = i+1;
	 //getCurveIDs(xycont, IDs);

	 this->listener->setProgressValue(1);


	 //find corners on contours
	CImageFilter gau3(eGauss,0,1,3); // sizeX = 0 for unknown, sizeY = 1 to be fixed at 1 and sigma = 3
	CImageFilter gau4(eGauss,0,1,4);	
	CImageFilter gau4m3 = gau4;
	gau4m3.subtractFilter(gau3);
	
	CImageBuffer oriLineX, oriLineY, extLineX, extLineY, smoothLineX, smoothLineY, conSmoothX, conSmoothY, conSmoothX3, conSmoothY3, conSmoothX4, conSmoothY4;

	int w, W;
	CXYContourArray xycontour;
	w = gau3.getOffsetX();
	W = gau4.getOffsetX();	
	
	if (!imageMode)
	{
		this->M.clear();
		this->C.clear();	
		//this->cID.clear();
	}
	else
	{
		this->Mimg.clear();
		this->Cimg.clear();	
	}
	for (i = 0; i < Sz; i++)
	{
		CXYContour *contour = xycont.GetAt(i);
		setBufferOfContourXY(oriLineX,oriLineY,contour);
		
		//convolve for sigma = 3		
		extendContourXY(oriLineX,oriLineY,extLineX,extLineY,w);
		gau3.convolve(extLineX,smoothLineX,false);
		gau3.convolve(extLineY,smoothLineY,false);
		contractContourXY(smoothLineX,smoothLineY,conSmoothX3,conSmoothY3,w);
		setContourFromBufferXY(conSmoothX3,conSmoothY3,xycontour,i);

		//convolve for sigma = 4		
		//extendContourXY(oriLineX,oriLineY,extLineX,extLineY,W);
		//gau4.convolve(extLineX,smoothLineX,false);
		//gau4.convolve(extLineY,smoothLineY,false);
		//contractContourXY(smoothLineX,smoothLineY,conSmoothX4,conSmoothY4,W);
		//setContourFromBufferXY(conSmoothX4,conSmoothY4,xycontour,i);
		

		//convolve for gau4m3 and find corners
		extendContourXY(oriLineX,oriLineY,extLineX,extLineY,W);
		gau4m3.convolve(extLineX,smoothLineX,false);
		gau4m3.convolve(extLineY,smoothLineY,false);
		contractContourXY(smoothLineX,smoothLineY,conSmoothX,conSmoothY,W);		
		//findCornersAndLines(conSmoothX3,conSmoothY3,conSmoothX,conSmoothY,xypts,xyedges,IDs[i]);
		findCornersAndLines(conSmoothX3,conSmoothY3,conSmoothX,conSmoothY,xypts,xyedges,imageMode);
		
		this->listener->setProgressValue(1+90*(i+1)/Sz);

		//test
		//FILE *fp = fopen("c:\\test.txt","w");
		//fprintf(fp,"%d\n",i);
		//fclose(fp);

		//if (i==2)
		//	int here = 1;
	}

	//gau.convolve(*this->gMask,destination,true);
	xycont.RemoveAll();
	xycont.Copy(xycontour);

	this->listener->setProgressValue(100);

	//if (IDs)
	//{
	//	delete[] IDs;
	//	IDs = NULL;
	//}

	bool ret = true;
	return ret;

 }

 void BuildingsDetector::findCornersAndLines(CImageBuffer conSmoothX3, CImageBuffer conSmoothY3, CImageBuffer conSmoothX, CImageBuffer conSmoothY, CXYPointArray &xypts, CXYPolyLineArray &xyedges, bool imageMode)
 {
	int i;
	int N = conSmoothX.getNGV();
	float *K = new float[N];
	double K_thresh = 0.05; //0.2; // standard threshold 0.05 to detect all corners

	// find curvatures
	for (i = 0; i < N; i++)
	{
		K[i] = conSmoothX.pixFlt(i)*conSmoothX.pixFlt(i) + conSmoothY.pixFlt(i)*conSmoothY.pixFlt(i);
	}

	//find maxima points
	int *extremum = new int[N]; //maximum extremums
    int n = 0; //extremums counter
    float Search = 1;
    
    for (int j = 0; j < N-1; j++)
	{
		if ((K[j+1]-K[j])*Search > 0)
		{			
            extremum[n] = j;
            Search = -Search;
			n = n+1;
		}
	}
    if (n%2 == 0)
	{		
        extremum[n] = N;
        n = n+1;
	}

	int start = 0;
	double m, c; bool fit;
	for (i = 1; i < n; i = i+2)
	{
		if (K[extremum[i]] > K_thresh)
		{
			Corner++;
			CObsPoint *P = xypts.Add();
			//P->setLabel(Corner);
			P->setLabel("C");
			(*P)[0] = conSmoothX3.pixFlt(extremum[i]);
			(*P)[1] = conSmoothY3.pixFlt(extremum[i]);
			fit = FitLine(conSmoothX3,conSmoothY3,start,extremum[i],m,c);
			if (fit)
			{	
				CXYPolyLine poly, objPoly;
				//poly.setLabel(this->CurrentLineLabel);
				poly.setLabel("");
				this->CurrentLineLabel++;
				//poly.initFromContour(contour, this->extractionParameters.lineThinningThreshold, sigma0);				
				
				double mp;
				if (m == 0)
					mp = FLT_MAX;
				else
					mp = -1/m;
				//C3DPoint obj1,obj2;
				//C2DPoint obs1(conSmoothX3.pixFlt(start),conSmoothY3.pixFlt(start)), obs2(conSmoothX3.pixFlt(extremum[i]),conSmoothY3.pixFlt(extremum[i]));
				//this->tfwImage->transformObs2Obj(obj1,obs1,0);
				//this->tfwImage->transformObs2Obj(obj2,obs2,0);				
				/*
				float P1[2], P2[2];				
				P1[1] = conSmoothY3.pixFlt(start);
				P1[0] = conSmoothX3.pixFlt(start);
				P2[1] = conSmoothY3.pixFlt(extremum[i]);
				P2[0] = conSmoothX3.pixFlt(extremum[i]);
*/
				double c1 = conSmoothY3.pixFlt(start) - mp*conSmoothX3.pixFlt(start);
                double c2 = conSmoothY3.pixFlt(extremum[i]) - mp*conSmoothX3.pixFlt(extremum[i]);
				
				//double c1 = obj1.y-mp*obj1.x;
                //double c2 = obj2.y-mp*obj2.x;

				//C3DPoint obj3,obj4;
                //obj3.x  = (c1-c) / (m-mp);
                //obj3.y = m*obj3.x + c;
				//obj3.z = 0;

                //obj4.x = (c2-c) / (m-mp);
                //obj4.y = m*obj4.x + c;
				//obj4.z = 0;

				CXYPoint C1, C2;
				//this->tfwImage->transformObj2Obs(C1,obj3);
				//this->tfwImage->transformObj2Obs(C2,obj4);

				C1.x  = (c1-c) / (m-mp);
                C1.y = m*C1.x + c;
				C2.x  = (c2-c) / (m-mp);
                C2.y = m*C2.x + c;				

				poly.addPoint(C1);
				poly.addPoint(C2);
				xyedges.add(poly);
				if (!imageMode)
				{
					this->M.push_back(m);
					this->C.push_back(c);
					//this->cID.push_back(curCurveID);
					this->isLineActive.push_back(true);
				}
				else
				{
					this->Mimg.push_back(m);
					this->Cimg.push_back(c);

					//get and set the mid points for image lines
					CXYPoint *Pm = this->midPointsImg.add();
					Pm->x = (C1.x+C2.x)/2;
					Pm->y = (C1.y+C2.y)/2;					
				}
				//record the extracted line
				/*C3DPoint obj1,obj2;
				this->tfwMask->transformObs2Obj(obj1,C1,0.0);
				this->tfwMask->transformObs2Obj(obj2,C2,0.0);
				C1.x = obj1.x;
				C1.y = obj1.y;
				C2.x = obj2.x;
				C2.y = obj2.y;
				//m = (C2.y - C1.y)/(C2.x - C1.x);
				//c = C1.y - m*C1.x;
				objPoly.addPoint(C1);
				objPoly.addPoint(C2);
				this->lines.add(objPoly);*/				
			}
			start = extremum[i];
		}

	}

	if (!imageMode)
		this->nLine = xyedges.GetSize();
	/*CXYPolyLine poly;
	CXYPoint C1, C2;
	C1.x = 10;
	C1.y = 20;
	C2.x = 100;
	C2.y = 200;
	poly.addPoint(C1);
	poly.addPoint(C2);
	xyedges.add(poly);
*/
	//delete allocated memories
	if (K)
	{
		delete[] K;
		K = NULL;
	}

	if (extremum)
	{
		delete[] extremum;
		extremum = NULL;
	}
 }

bool BuildingsDetector::FitLine(CImageBuffer conSmoothX3, CImageBuffer conSmoothY3, int start, int end , double &m, double &c)
{
	int i, j;
	int nObs = end-start+1;
	bool ret = false;
	//tests
	/*CXYPoint P1, P2;
	P1.x = conSmoothX3.pixFlt(start);
	P1.y = conSmoothY3.pixFlt(start) - this->h;
	P2.x = conSmoothX3.pixFlt(end);
	P2.y = conSmoothY3.pixFlt(end) - this->h;
	double m1 = (P2.y - P1.y)/(P2.x-P1.x);
	double c1 = P1.y - m1*P1.x;
	FILE *fp = fopen("points.txt","w");
*/
	if (nObs > this->minContourLength)
	{
		Matrix obs;
		obs.ReSize(nObs, 2);
		CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();
		for (i = start, j = 0; i <= end; i++, j++)
		{
			//C3DPoint obj;
			//C2DPoint obss(conSmoothX3.pixFlt(i),conSmoothY3.pixFlt(i));
			//this->tfwImage->transformObs2Obj(obj,obss,0);
			
			obs.element(j, 0) = conSmoothX3.pixFlt(i);
			obs.element(j, 1) = conSmoothY3.pixFlt(i);
			
			//tests
			//fprintf(fp,"%f %f\n",	obs.element(j, 0), obs.element(j, 1));

			//obs.element(j, 0) = obj.x;
			//obs.element(j, 1) = obj.y;

			// Add covariance
			Matrix cov;
			cov.ReSize(2,2);
			cov = 0;
			cov.element(0, 0) = 0.1;
			cov.element(1, 1) = 0.1;
			//cov.element(0, 1) = point->getCovarElement(0,1);
			//cov.element(1, 0) = point->getCovarElement(1,0);
			covariance->addSubMatrix(cov);
		}

		CBestFitLine bfl;
		CLeastSquaresSolver lss(&bfl, &obs, NULL, NULL, covariance);
		lss.solve();

		Matrix* parms = lss.getSolution();

		c = (*parms)(1, 1);
		m = (*parms)(2, 1);
		ret = true;

		if (covariance)
		{
			delete covariance;
			covariance = NULL;
		}
	}
	//fclose(fp);
	return ret;
}

 void BuildingsDetector::setBufferOfContourXY(CImageBuffer &oriLineX, CImageBuffer &oriLineY, CXYContour *contour)
 {
	int width = contour->getPointCount();
	oriLineX.set(0,0,width,1,1,32,true);
	oriLineY.set(0,0,width,1,1,32,true);

	for (int j = 0; j < width; j++)
	{
		C2DPoint *p = contour->getPoint(j);
		oriLineX.pixFlt(j) = (float) p->x;
		oriLineY.pixFlt(j) = (float) p->y;
	}
 }

 void BuildingsDetector::setContourFromBufferXY(CImageBuffer smoothLineX, CImageBuffer smoothLineY, CXYContourArray &xycontour, int index)
 {
	int width = (int) smoothLineX.getNGV();
	CXYContour *cont = xycontour.Add();
	for (int j = 0; j < width; j++)
	{
		C2DPoint p;
		p.x = smoothLineX.pixFlt(j);
		p.y = smoothLineY.pixFlt(j);
		cont->addPoint(p);
	}
	//xycont.RemoveAt(index);
	//xycont.InsertAt(index,cont);
 }

 void BuildingsDetector::extendContourXY(CImageBuffer oriLineX, CImageBuffer oriLineY, CImageBuffer &extLineX, CImageBuffer &extLineY, int W)
 {
	int len = (int) oriLineX.getNGV();
	int L = len+2*W;
	extLineX.set(0,0,L,1,1,32,true);
	extLineY.set(0,0,L,1,1,32,true);

	int i , index, j;
	//extend beginning
	float Vx1 = 2*oriLineX.pixFlt(0);
	float Vy1 = 2*oriLineY.pixFlt(0);
	for (i = 0; i < W; i++)
	{
		index = W-i;
		extLineX.pixFlt(i) = Vx1 - oriLineX.pixFlt(index);
		extLineY.pixFlt(i) = Vy1 - oriLineY.pixFlt(index);
	}

	//set line as it is in the middle
	for (i = 0; i < len; i++)
	{
		index = W+i;
		extLineX.pixFlt(index) = oriLineX.pixFlt(i);
		extLineY.pixFlt(index) = oriLineY.pixFlt(i);
	}

	//extend at end
	Vx1 = 2*oriLineX.pixFlt(len-1);
	Vy1 = 2*oriLineY.pixFlt(len-1);
	for (i = 0; i < W; i++)
	{
		index = len+W+i;
		j = len-2-i; 
		extLineX.pixFlt(index) = Vx1 - oriLineX.pixFlt(j);
		extLineY.pixFlt(index) = Vy1 - oriLineY.pixFlt(j);
	}
 }

 void BuildingsDetector::contractContourXY(CImageBuffer smoothLineX, CImageBuffer smoothLineY, CImageBuffer &conSmoothX, CImageBuffer &conSmoothY, int W)
 {
	int L = (int) smoothLineX.getNGV();
	int len = L-2*W;
	conSmoothX.set(0,0,len,1,1,32,true);
	conSmoothY.set(0,0,len,1,1,32,true);

	int i, index;
	for (i = 0; i < len; i++)
	{
		index = W+i;
		conSmoothX.pixFlt(i) = smoothLineX.pixFlt(index);
		conSmoothY.pixFlt(i) = smoothLineY.pixFlt(index);
	}
 }

void BuildingsDetector::applyNDVIandEdgeThresh(CHugeImage *gndMask, CHugeImage *sigmaNDVI, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CXYPolyLineArray &ndvidges, CHugeImage *entropyMask)
 {
	 if (this->listener)
	 {
		 this->listener->setTitleString("Applying NDVI & edge thresholds on lines ...");
		 this->listener->setProgressValue(1);
	 }

	double dAL = this->dAl;
	unsigned int i;
	CXYPolyLine *poly, rectVertices, *poly1;//, *objpoly;
	CXYPoint C1, C2, P1, P2, P3, P4, C1img, C2img, P1img, P2img, P3img, P4img, P1a, P2a, P3a, P4a;
	C3DPoint obj;
	unsigned long nP;
	double abvPer, blwPer, abvPerX, blwPerX;

	if (this->rgbImage->getBands() == 3)
	{
		//convert the 32 bit float NDVI image to 8 bit greyscale image
		this->sigmaNDVI8bit = new CHugeImage;
		//CLookupTable lut(8,3);
		//this->sigmaNDVI8bit->setLookupTable(&lut);
		CCharString fn(this->projectPath);
		CCharString fname = fn+"sigmaNDVI8bit"+".hug";
		this->sigmaNDVI8bit->prepareWriting(fname, sigmaNDVI->getWidth(), sigmaNDVI->getHeight(), 8, 1);
		this->sigmaNDVI8bit->floatToGrey(sigmaNDVI);
	}
	else
		this->sigmaNDVI8bit = sigmaNDVI;

	this->lengths.clear();
	this->lengths.resize(this->nLine);
	double m,c,m1,c1;
	this->insidePoints.RemoveAll();
	this->midPoints.RemoveAll();
	//FILE *fp = fopen("I:\\Buildings\\NDVIedgeThreshold.txt","w");
	//fclose(fp);
	for (i = 0; i < this->nLine && this->isLineActive.at(i); i++)//this->nLine; i++)
	//for (i = 0; i < 1 && this->isLineActive.at(i); i++)
	{
		//FILE *fp = fopen("I:\\Buildings\\NDVIedgeThreshold.txt","a");
		poly = xyedges.getAt(i);		
		//objpoly = this->lines.getAt(i);
		C1 = poly->getPoint(0);
		C2 = poly->getPoint(1);
		m = this->M.at(i);
		c = this->C.at(i);
		findRectVertices(m, c, C1, C2, P1, P2, P3, P4, dAL);

		//convert points to image coordinate system
		//C1
		this->tfwMask->transformObs2Obj(obj,C1,0.0);
		//obj = objpoly->getPoint(0);
		this->tfwImage->transformObj2Obs(C1img,obj);
		//C2
		this->tfwMask->transformObs2Obj(obj,C2,0.0);
		//obj = objpoly->getPoint(1);
		this->tfwImage->transformObj2Obs(C2img,obj);
		//P1
		this->tfwMask->transformObs2Obj(obj,P1,0.0);
		this->tfwImage->transformObj2Obs(P1img,obj);
		//P2
		this->tfwMask->transformObs2Obj(obj,P2,0.0);
		this->tfwImage->transformObj2Obs(P2img,obj);
		//P3
		this->tfwMask->transformObs2Obj(obj,P3,0.0);
		this->tfwImage->transformObj2Obs(P3img,obj);
		//P4
		this->tfwMask->transformObs2Obj(obj,P4,0.0);
		this->tfwImage->transformObj2Obs(P4img,obj);

		//add to project's ground mask to show
		//C3DPoint obj1(P1.x,P1.y,0), obj2(P2.x,P2.y,0), obj3(P3.x,P3.y,0), obj4(P4.x,P4.y,0);
		//C2DPoint obs1,obs2,obs3,obs4;
		//this->tfwImage->transformObj2Obs(obs1,obj1);
		//this->tfwImage->transformObj2Obs(obs2,obj2);
		//this->tfwImage->transformObj2Obs(obs3,obj3);
		//this->tfwImage->transformObj2Obs(obs4,obj4);
		//rectVertices.addPoint(obs1);
		//rectVertices.addPoint(obs2);
		//rectVertices.addPoint(obs4);
		//rectVertices.addPoint(obs3);

		/*rectVertices.addPoint(P1);
		rectVertices.addPoint(P2);
		rectVertices.addPoint(P4);
		rectVertices.addPoint(P3);
		rectVertices.addPoint(P1);
		xyedges.add(rectVertices);*/

 		unsigned long tAbove, rAbove, tBelow, rBelow, txAbove, rxAbove, txBelow, rxBelow;
		double meanNDVI;
		findPointsInside(gndMask,P1,C1,C2,P3,tAbove,rAbove);
		findPointsInside(gndMask,C1,P2,P4,C2,tBelow,rBelow);
		nP = (tAbove > tBelow)?tAbove:tBelow;
		//if (nP != 0) 
		//{
			abvPer = (double)rAbove/nP;
			blwPer = (double)rBelow/nP;
		//}
		//else
		//{
		//	abvPer = 
		//}

		//just to show
		/*CXYPoint *Pt1 = xypts.add();
		Pt1->x = (P1.x + P3.x)/2;
		Pt1->y = (P1.y + P3.y)/2;
		Pt1->setLabel("mid13");
		Pt1 = xypts.add();
		Pt1->x = (P2.x + P4.x)/2;
		Pt1->y = (P2.y + P4.y)/2;
		Pt1->setLabel("mid24");*/			
		// remove above test code		
		CXYPoint *Pt = this->insidePoints.add();
		CXYPoint *Pm = this->midPoints.add();
		if (abvPer < this->edgeThresh && blwPer < this->edgeThresh)
		{
			this->isLineActive.at(i) = false;
		}
		else if (abvPer >= this->edgeThresh)
		{
			findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P1img,C1img,C2img,P3img,xypts,meanNDVI);
			if (meanNDVI <= this->ndviThresh)
			{
				Pt->x = (P1.x + P3.x)/2;
				Pt->y = (P1.y + P3.y)/2;
				Pm->x = (C1.x+C2.x)/2;
				Pm->y = (C1.y+C2.y)/2;

				//test code
				poly1 = ndvidges.add();
				poly1->addPoint(C1img);
				poly1->addPoint(C2img);
				CXYPoint *Pt1 = xypts.add();
				Pt1->x = (P1img.x + P3img.x)/2;
				Pt1->y = (P1img.y + P3img.y)/2;
				//fprintf(fp,"%d (successful %d) out of %d lines were NDVI and edge thresholded.\n",i+1,ndvidges.GetSize(),this->nLine);
			}
			else if (this->useEntropy)
			{
				m1 = (P3.y-P1.y)/(P3.x-P1.x);
				c1 = P1.y - m1*P1.x;
				
				findRectVertices(m1, c1, P1, P3, P1a, P2a, P3a, P4a, 1);//at 1m distance

				findPointsInside(entropyMask,P1a,P3a,P4a,P2a,txAbove,rxAbove);
				
				abvPerX = (double)(txAbove - rxAbove)/txAbove;

				if (abvPerX > this->entropyThresh)
					this->isLineActive.at(i) = false;
				else
				{
					Pt->x = (P1.x + P3.x)/2;
					Pt->y = (P1.y + P3.y)/2;
					Pm->x = (C1.x+C2.x)/2;
					Pm->y = (C1.y+C2.y)/2;

					//test code
					poly1 = ndvidges.add();
					poly1->addPoint(C1img);
					poly1->addPoint(C2img);
					CXYPoint *Pt1 = xypts.add();
					Pt1->x = (P1img.x + P3img.x)/2;
					Pt1->y = (P1img.y + P3img.y)/2;
					//fprintf(fp,"%d (successful %d) out of %d lines were NDVI and edge thresholded.\n",i+1,ndvidges.GetSize(),this->nLine);
				}
			}
			else
				this->isLineActive.at(i) = false;
		}
		else
		{
			findNDVImeanInside(this->sigmaNDVI8bit,cropImg,C1img,P2img,P4img,C2img,xypts,meanNDVI);
			if (meanNDVI <= this->ndviThresh)
			{
				Pt->x = (P2.x + P4.x)/2;
				Pt->y = (P2.y + P4.y)/2;
				Pm->x = (C1.x+C2.x)/2;
				Pm->y = (C1.y+C2.y)/2;

				//test code
				poly1 = ndvidges.add();
				poly1->addPoint(C1img);
				poly1->addPoint(C2img);
				CXYPoint *Pt1 = xypts.add();
				Pt1->x = (P2img.x + P4img.x)/2;
				Pt1->y = (P2img.y + P4img.y)/2;
				//fprintf(fp,"%d (successful %d) out of %d lines were NDVI and edge thresholded.\n",i+1,ndvidges.GetSize(),this->nLine);
			}
			else if (this->useEntropy)
			{
				m1 = (P4.y-P2.y)/(P4.x-P2.x);
				c1 = P2.y - m1*P2.x;
				
				findRectVertices(m1, c1, P2, P4, P1a, P2a, P3a, P4a, 1);//at 1m distance

				findPointsInside(entropyMask,P1a,P3a,P4a,P2a,txBelow,rxBelow);
				
				blwPerX = (double)(txBelow - rxBelow)/txBelow;

				if (blwPerX > this->entropyThresh)
					this->isLineActive.at(i) = false;
				else
				{
					Pt->x = (P2.x + P4.x)/2;
					Pt->y = (P2.y + P4.y)/2;
					Pm->x = (C1.x+C2.x)/2;
					Pm->y = (C1.y+C2.y)/2;

					//test code
					poly1 = ndvidges.add();
					poly1->addPoint(C1img);
					poly1->addPoint(C2img);
					CXYPoint *Pt1 = xypts.add();
					Pt1->x = (P2img.x + P4img.x)/2;
					Pt1->y = (P2img.y + P4img.y)/2;
					//fprintf(fp,"%d (successful %d) out of %d lines were NDVI and edge thresholded.\n",i+1,ndvidges.GetSize(),this->nLine);
				}
			}
			else
				this->isLineActive.at(i) = false;
		}
		//int here = 1;

		this->lengths[i] = sqrt((C1.x-C2.x)*(C1.x-C2.x) + (C1.y-C2.y)*(C1.y-C2.y));

		//xypts.add(*Pt);

		this->listener->setProgressValue(100*(i+1)/this->nLine);
		//fclose(fp);
	}
	
 }

 void BuildingsDetector::applyNDVIandEdgeThresh(CHugeImage *gndMask, CHugeImage *sigmaNDVI, CHugeImage *cropImg, CXYPointArray &xypts, CXYPolyLineArray &xyedges, CXYPolyLineArray &ndvidges)
 {
	 if (this->listener)
	 {
		 this->listener->setTitleString("Applying NDVI & edge thresholds on lines ...");
		 this->listener->setProgressValue(1);
	 }

	double dAL = this->dAl;
	unsigned int i;
	CXYPolyLine *poly, rectVertices, *poly1;//, *objpoly;
	CXYPoint C1, C2, P1, P2, P3, P4, C1img, C2img, P1img, P2img, P3img, P4img;
	C3DPoint obj;
	unsigned long nP;
	double abvPer, blwPer;

	if (this->rgbImage->getBands() == 3)
	{
		//convert the 32 bit float NDVI image to 8 bit greyscale image
		this->sigmaNDVI8bit = new CHugeImage;
		//CLookupTable lut(8,3);
		//this->sigmaNDVI8bit->setLookupTable(&lut);
		CCharString fn(this->projectPath);
		CCharString fname = fn+"sigmaNDVI8bit"+".hug";
		this->sigmaNDVI8bit->prepareWriting(fname, sigmaNDVI->getWidth(), sigmaNDVI->getHeight(), 8, 1);
		this->sigmaNDVI8bit->floatToGrey(sigmaNDVI);
	}
	else
		this->sigmaNDVI8bit = sigmaNDVI;

	this->lengths.clear();
	this->lengths.resize(this->nLine);
	double m,c;
	this->insidePoints.RemoveAll();
	this->midPoints.RemoveAll();
	//FILE *fp = fopen("I:\\Buildings\\NDVIedgeThreshold.txt","w");
	//fclose(fp);
	for (i = 0; i < this->nLine && this->isLineActive.at(i); i++)//this->nLine; i++)
	//for (i = 0; i < 1 && this->isLineActive.at(i); i++)
	{
		//FILE *fp = fopen("I:\\Buildings\\NDVIedgeThreshold.txt","a");
		poly = xyedges.getAt(i);		
		//objpoly = this->lines.getAt(i);
		C1 = poly->getPoint(0);
		C2 = poly->getPoint(1);
		m = this->M.at(i);
		c = this->C.at(i);
		findRectVertices(m, c, C1, C2, P1, P2, P3, P4, dAL);

		//convert points to image coordinate system
		//C1
		this->tfwMask->transformObs2Obj(obj,C1,0.0);
		//obj = objpoly->getPoint(0);
		this->tfwImage->transformObj2Obs(C1img,obj);
		//C2
		this->tfwMask->transformObs2Obj(obj,C2,0.0);
		//obj = objpoly->getPoint(1);
		this->tfwImage->transformObj2Obs(C2img,obj);
		//P1
		this->tfwMask->transformObs2Obj(obj,P1,0.0);
		this->tfwImage->transformObj2Obs(P1img,obj);
		//P2
		this->tfwMask->transformObs2Obj(obj,P2,0.0);
		this->tfwImage->transformObj2Obs(P2img,obj);
		//P3
		this->tfwMask->transformObs2Obj(obj,P3,0.0);
		this->tfwImage->transformObj2Obs(P3img,obj);
		//P4
		this->tfwMask->transformObs2Obj(obj,P4,0.0);
		this->tfwImage->transformObj2Obs(P4img,obj);

		//add to project's ground mask to show
		//C3DPoint obj1(P1.x,P1.y,0), obj2(P2.x,P2.y,0), obj3(P3.x,P3.y,0), obj4(P4.x,P4.y,0);
		//C2DPoint obs1,obs2,obs3,obs4;
		//this->tfwImage->transformObj2Obs(obs1,obj1);
		//this->tfwImage->transformObj2Obs(obs2,obj2);
		//this->tfwImage->transformObj2Obs(obs3,obj3);
		//this->tfwImage->transformObj2Obs(obs4,obj4);
		//rectVertices.addPoint(obs1);
		//rectVertices.addPoint(obs2);
		//rectVertices.addPoint(obs4);
		//rectVertices.addPoint(obs3);

		/*rectVertices.addPoint(P1);
		rectVertices.addPoint(P2);
		rectVertices.addPoint(P4);
		rectVertices.addPoint(P3);
		rectVertices.addPoint(P1);
		xyedges.add(rectVertices);*/

 		unsigned long tAbove, rAbove, tBelow, rBelow;
		double meanNDVI;
		findPointsInside(gndMask,P1,C1,C2,P3,tAbove,rAbove);
		findPointsInside(gndMask,C1,P2,P4,C2,tBelow,rBelow);
		nP = (tAbove > tBelow)?tAbove:tBelow;
		//if (nP != 0) 
		//{
			abvPer = (double)rAbove/nP;
			blwPer = (double)rBelow/nP;
		//}
		//else
		//{
		//	abvPer = 
		//}

		//just to show
		/*CXYPoint *Pt1 = xypts.add();
		Pt1->x = (P1.x + P3.x)/2;
		Pt1->y = (P1.y + P3.y)/2;
		Pt1->setLabel("mid13");
		Pt1 = xypts.add();
		Pt1->x = (P2.x + P4.x)/2;
		Pt1->y = (P2.y + P4.y)/2;
		Pt1->setLabel("mid24");*/			
		// remove above test code		
		CXYPoint *Pt = this->insidePoints.add();
		CXYPoint *Pm = this->midPoints.add();
		if (abvPer < this->edgeThresh && blwPer < this->edgeThresh)
		{
			this->isLineActive.at(i) = false;
		}
		else if (abvPer >= this->edgeThresh)
		{
			findNDVImeanInside(this->sigmaNDVI8bit,cropImg,P1img,C1img,C2img,P3img,xypts,meanNDVI);
			if (meanNDVI <= this->ndviThresh)
			{
				Pt->x = (P1.x + P3.x)/2;
				Pt->y = (P1.y + P3.y)/2;
				Pm->x = (C1.x+C2.x)/2;
				Pm->y = (C1.y+C2.y)/2;

				//test code
				poly1 = ndvidges.add();
				poly1->addPoint(C1img);
				poly1->addPoint(C2img);
				CXYPoint *Pt1 = xypts.add();
				Pt1->x = (P1img.x + P3img.x)/2;
				Pt1->y = (P1img.y + P3img.y)/2;
				//fprintf(fp,"%d (successful %d) out of %d lines were NDVI and edge thresholded.\n",i+1,ndvidges.GetSize(),this->nLine);
			}
			else
				this->isLineActive.at(i) = false;
		}
		else
		{
			findNDVImeanInside(this->sigmaNDVI8bit,cropImg,C1img,P2img,P4img,C2img,xypts,meanNDVI);
			if (meanNDVI <= this->ndviThresh)
			{
				Pt->x = (P2.x + P4.x)/2;
				Pt->y = (P2.y + P4.y)/2;
				Pm->x = (C1.x+C2.x)/2;
				Pm->y = (C1.y+C2.y)/2;

				//test code
				poly1 = ndvidges.add();
				poly1->addPoint(C1img);
				poly1->addPoint(C2img);
				CXYPoint *Pt1 = xypts.add();
				Pt1->x = (P2img.x + P4img.x)/2;
				Pt1->y = (P2img.y + P4img.y)/2;
				//fprintf(fp,"%d (successful %d) out of %d lines were NDVI and edge thresholded.\n",i+1,ndvidges.GetSize(),this->nLine);
			}
			else
				this->isLineActive.at(i) = false;
		}
		//int here = 1;

		this->lengths[i] = sqrt((C1.x-C2.x)*(C1.x-C2.x) + (C1.y-C2.y)*(C1.y-C2.y));

		//xypts.add(*Pt);

		this->listener->setProgressValue(100*(i+1)/this->nLine);
		//fclose(fp);
	}
	
 }

 void BuildingsDetector::findNDVImeanInside(CHugeImage *hugImg, CHugeImage *cropImg, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, CXYPointArray &xypts, double &meanNDVI)
 {
//%             L4
//%        P1---------P4
//%    L1  |   P      |    
//%        |          |L3
//%       P2----------P3
//%             L2


	long minX, minY, maxX, maxY, i, j;
	findMinMax(P1,P2,P3,P4,minX,minY,maxX,maxY,hugImg->getWidth(),hugImg->getHeight());
	//if (minX 
	unsigned long wd, ht;
	wd = maxX-minX+1;
	ht = maxY-minY+1;

	CImageBuffer buf;
	hugImg->getRect(buf,minY,ht,minX,wd);
	//set(0, 0, this->w, this->h, 1, 8, false, 0);

	long tPoint = 0;
	double tValue = 0.0;

	CXYPoint P;

	long offX = buf.getOffsetX();
	long offY = buf.getOffsetY();
	//float *pixels = buf.getPixF();
	for (i = minX; i <= maxX; i++)
	{
		P.x = i;
		for (j = minY; j <= maxY; j++)
		{			
			P.y = j;
			if (isInsideRectangle(P1,P2,P3,P4,P))
			{	
				/*CXYPoint *Padd = xypts.add();
				Padd->x = P.x;
				Padd->y = P.y;
				Padd->setLabel("");*/

				tPoint++;
				unsigned long rowInd, colInd;
				colInd = (unsigned long)P.x - offX;
				rowInd = (unsigned long)P.y - offY;
				unsigned long index = rowInd*wd + colInd;
				//float v = buf.pixUChar(index);
				if (this->rgbImage->getBands() == 3)
					tValue += buf.pixUChar(index);
				else // for 4 bands image
					tValue += buf.pixFlt(index);
				//double val = pixels[index];
				//tValue += pixels[index];
			}
		}
	}
	meanNDVI = tValue/tPoint;

	/*CCharString fn(this->projectPath);
	fn = fn+cropImg->getFileNameChar()+".hug";
    
	
	CLookupTable lut(8,3);
	bool ret=cropImg->dumpImage(fn.GetChar(), buf, &lut);*/
 }

void BuildingsDetector::findPointsInside(CHugeImage *hugImg, CHugeImage *hugImg2, CHugeImage *hugImg3, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, CDoubleVector &vals, CDoubleVector &vals2, CDoubleVector &vals3)
{
//%             L4
//%        P1---------P4
//%    L1  |   P      |    
//%        |          |L3
//%       P2----------P3
//%             L2
	
	long minX, minY, maxX, maxY, i, j, nItem = 0;
	findMinMax(P1,P2,P3,P4,minX,minY,maxX,maxY,hugImg->getWidth(),hugImg->getHeight());
	//if (minX 
	unsigned long wd, ht;
	wd = maxX-minX+1;
	ht = maxY-minY+1;

	CImageBuffer buf, buf2, buf3;
	hugImg->getRect(buf,minY,ht,minX,wd);
	hugImg2->getRect(buf2,minY,ht,minX,wd);
	hugImg3->getRect(buf3,minY,ht,minX,wd);
	//set(0, 0, this->w, this->h, 1, 8, false, 0);

	//total = 0;
	//long int nP = 0;
	vals.CleanUp();
	vals2.CleanUp();
	vals3.CleanUp();

	CXYPoint P;

	long offX = buf.getOffsetX();
	long offY = buf.getOffsetY();
	for (i = minX; i <= maxX; i++)
	{
		P.x = i;
		for (j = minY; j <= maxY; j++)
		{			
			P.y = j; //P.x = (minX+maxX)/2; P.y = (minY+maxY)/2;
			if (isInsideRectangle(P1,P2,P3,P4,P))
			{
				//total++;
				unsigned long rowInd, colInd;
				colInd = (unsigned long)P.x - offX;
				rowInd = (unsigned long)P.y - offY;
				unsigned long index = rowInd*wd + colInd;
				vals.Add(buf.pixFlt(index));
				vals2.Add(buf2.pixFlt(index));
				vals3.Add(buf3.pixFlt(index));
				//if (buf.pixUChar(index) == 0)
				//{
					//nP++;
//
//				}
			}
			//int h3 = 3;
		}
		//int h2 = 2;
	}
	//int h1 = 1;
 }

 void BuildingsDetector::findPointsInside(CHugeImage *hugImg, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, CDoubleVector &vals)
 {
//%             L4
//%        P1---------P4
//%    L1  |   P      |    
//%        |          |L3
//%       P2----------P3
//%             L2
	
	long minX, minY, maxX, maxY, i, j, nItem = 0;
	findMinMax(P1,P2,P3,P4,minX,minY,maxX,maxY,hugImg->getWidth(),hugImg->getHeight());
	//if (minX 
	unsigned long wd, ht;
	wd = maxX-minX+1;
	ht = maxY-minY+1;

	CImageBuffer buf;
	hugImg->getRect(buf,minY,ht,minX,wd);
	//set(0, 0, this->w, this->h, 1, 8, false, 0);

	//total = 0;
	//long int nP = 0;
	vals.CleanUp();

	CXYPoint P;

	long offX = buf.getOffsetX();
	long offY = buf.getOffsetY();
	for (i = minX; i <= maxX; i++)
	{
		P.x = i;
		for (j = minY; j <= maxY; j++)
		{			
			P.y = j; //P.x = (minX+maxX)/2; P.y = (minY+maxY)/2;
			if (isInsideRectangle(P1,P2,P3,P4,P))
			{
				//total++;
				unsigned long rowInd, colInd;
				colInd = (unsigned long)P.x - offX;
				rowInd = (unsigned long)P.y - offY;
				unsigned long index = rowInd*wd + colInd;
				vals.Add(buf.pixFlt(index));
				//if (buf.pixUChar(index) == 0)
				//{
					//nP++;
//
//				}
			}
			//int h3 = 3;
		}
		//int h2 = 2;
	}
	//int h1 = 1;
 }

 void BuildingsDetector::findPointsInside(CHugeImage *hugImg, CHugeImage *hugImg2, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, unsigned long &total, unsigned long &nP, unsigned long &total2, unsigned long &nP2)
{
//%             L4
//%        P1---------P4
//%    L1  |   P      |    
//%        |          |L3
//%       P2----------P3
//%             L2


	long minX, minY, maxX, maxY, i, j;
	findMinMax(P1,P2,P3,P4,minX,minY,maxX,maxY,hugImg->getWidth(),hugImg->getHeight());
	//if (minX 
	unsigned long wd, ht;
	wd = maxX-minX+1;
	ht = maxY-minY+1;

	CImageBuffer buf,buf2;
	hugImg->getRect(buf,minY,ht,minX,wd);
	hugImg2->getRect(buf2,minY,ht,minX,wd);
	//set(0, 0, this->w, this->h, 1, 8, false, 0);

	total = 0;
	nP = 0;
	total2 = 0;
	nP2 = 0;

	CXYPoint P;

	long offX = buf.getOffsetX();
	long offY = buf.getOffsetY();
	for (i = minX; i <= maxX; i++)
	{
		P.x = i;
		for (j = minY; j <= maxY; j++)
		{			
			P.y = j; //P.x = (minX+maxX)/2; P.y = (minY+maxY)/2;
			if (isInsideRectangle(P1,P2,P3,P4,P))
			{
				total++; total2++;
				unsigned long rowInd, colInd;
				colInd = (unsigned long)P.x - offX;
				rowInd = (unsigned long)P.y - offY;
				unsigned long index = rowInd*wd + colInd;
				if (buf.pixUChar(index) == 0)
					nP++;
				if (buf2.pixUChar(index) == 0)
					nP2++;
			}
		}
	}
 }

 void BuildingsDetector::findPointsInside(CHugeImage *hugImg, CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, unsigned long &total, unsigned long &nP)
 {
//%             L4
//%        P1---------P4
//%    L1  |   P      |    
//%        |          |L3
//%       P2----------P3
//%             L2


	long minX, minY, maxX, maxY, i, j;
	findMinMax(P1,P2,P3,P4,minX,minY,maxX,maxY,hugImg->getWidth(),hugImg->getHeight());
	//if (minX 
	unsigned long wd, ht;
	wd = maxX-minX+1;
	ht = maxY-minY+1;

	CImageBuffer buf;
	hugImg->getRect(buf,minY,ht,minX,wd);
	//set(0, 0, this->w, this->h, 1, 8, false, 0);

	total = 0;
	nP = 0;

	CXYPoint P;

	long offX = buf.getOffsetX();
	long offY = buf.getOffsetY();
	for (i = minX; i <= maxX; i++)
	{
		P.x = i;
		for (j = minY; j <= maxY; j++)
		{			
			P.y = j; //P.x = (minX+maxX)/2; P.y = (minY+maxY)/2;
			if (isInsideRectangle(P1,P2,P3,P4,P))
			{
				total++;
				unsigned long rowInd, colInd;
				colInd = (unsigned long)P.x - offX;
				rowInd = (unsigned long)P.y - offY;
				unsigned long index = rowInd*wd + colInd;
				if (buf.pixUChar(index) == 0)
					nP++;
			}
		}
	}
 }

 void BuildingsDetector::findMinMax(CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, long &minX, long &minY, long &maxX, long &maxY, int wd, int ht)
 {
	 // find minX and maxX
	if (P1.x <= P2.x)
	{
		minX = (long) floor(P1.x);
		maxX = (long) ceil(P2.x);
	}
	else
	{
		minX = (long) floor(P2.x);
		maxX = (long) ceil(P1.x);
	}
	if (P3.x < minX)
		minX = (long) floor(P3.x);
	else if (P3.x > maxX)
		maxX = (long) ceil(P3.x);
	if (P4.x < minX)
		minX = (long) floor(P4.x);
	else if (P4.x > maxX)
		maxX = (long) ceil(P4.x);

	// find minY and maxY
	if (P1.y <= P2.y)
	{
		minY = (long) floor(P1.y);
		maxY = (long) ceil(P2.y);
	}
	else
	{
		minY = (long) floor(P2.y);
		maxY = (long) ceil(P1.y);
	}
	if (P3.y < minY)
		minY = (long) floor(P3.y);
	else if (P3.y > maxY)
		maxY = (long) ceil(P3.y);
	if (P4.y < minY)
		minY = (long) floor(P4.y);
	else if (P4.y > maxY)
		maxY = (long) ceil(P4.y);

	if (minX  < 0)
		minX = 0;
	if (minY < 0)
		minY = 0;	
	if (maxX < 0)	
		maxX = 0;
	if (maxY < 0)
		maxY = 0;	
	if (minX >= wd)
		minX = wd-1;	
	if (maxX >= wd)
		maxX = wd-1;
	if (minY >= ht)
		minY = ht-1;
	if (maxY >= ht)
		maxY = ht-1;
 }

 bool BuildingsDetector::isInsideRectangle(CXYPoint P1, CXYPoint P2, CXYPoint P3, CXYPoint P4, CXYPoint P)
{
	// calculate (twice, multiplied by 2) areas of clockwise triangles with P
	// for clockwise areas (of all 4) triangles (P1P2P, P2P3P, P3P4P, P4P1P) should be -ve, if P is inside the quadrangle
	// for anti-clockwise all areas should be +ve, if P is inside the quadrangle
	double a1 = P2.x*P.y - P2.y*P.x - P1.x*P.y + P1.y*P.x + P1.x*P2.y - P1.y*P2.x;
	double a2 = P3.x*P.y - P3.y*P.x - P2.x*P.y + P2.y*P.x + P2.x*P3.y - P2.y*P3.x;
	double a3 = P4.x*P.y - P4.y*P.x - P3.x*P.y + P3.y*P.x + P3.x*P4.y - P3.y*P4.x;
	double a4 = P1.x*P.y - P1.y*P.x - P4.x*P.y + P4.y*P.x + P4.x*P1.y - P4.y*P1.x;
	
	if ((a1<0 && a2<0 && a3<0 && a4<0) || (a1>0 && a2>0 && a3>0 && a4>0))
		return true;
	else
		return false;
}

 void BuildingsDetector::findRectVertices(double m, double c, CXYPoint C1, CXYPoint C2, CXYPoint &P1, CXYPoint &P2, CXYPoint &P3, CXYPoint &P4, double d)
 {
//     P1---------P3
//     |          |d
//    C1----------C2
//     |          |d
//     P2---------P4
	//double m = this->M.at(index);
	//double d = this->dAl;
	//double c = this->C.at(index);

	double co1 = C1.y + C1.x/m;  // y-intersection of first perpendicular line
	double co2 = C2.y + C2.x/m;  // y-intersection of second perpendicular line

	double e = 1 + (1/(m*m));
	double mde = -2*e;
	double sd = d*d;

	//coeffiecints of first quadratic equeation to solve
	double A = e;
	double B = mde*C1.x;
	double C = C1.x*C1.x*e - sd;

	double dA = 2*A;
	double Det = sqrt(B*B - 4*A*C);

	P1.x = (-B + Det)/dA;
	P1.y = -P1.x/m + co1;

	P2.x = (-B - Det)/dA;
	P2.y = -P2.x/m + co1;

	//coeffiecints of first quadratic equeation to solve
	//A = e;
	B = mde*C2.x;
	C = C2.x*C2.x*e - sd;

	//dA = A*A;
	Det = sqrt(B*B - 4*A*C);

	P3.x = (-B + Det)/dA;
	P3.y = -P3.x/m + co2;

	P4.x = (-B - Det)/dA;
	P4.y = -P4.x/m + co2;
 }