#ifdef WIN32
#include <strstream> 
using namespace std;
#include <float.h> 

#include "DSMBuilding.h"

#include "utilities.h"
#include "LabelImage.h"
#include "SystemUtilities.h"
#include "DSMRoughness.h"
#include "filter.h"
#include "XYZPolyLineArray.h"
#include "XYPolyLineArray.h"
#include "XYContourArray.h"
#include "XYZPoint.h"
#include "Building.h"

//=============================================================================

CDSMBuilding::CDSMBuilding() : index (0), 
              rMin(0), cMin(0), width(0), height(0), pMin(0,0,0), pMax(0,0,0), 
			  centreOfGravity(0,0,0), extents(0,0), angle(0), squaredSums(0,0,0), 
			  nPixels(0), nPointPixels (0), nHomogeneousPixels(0), 
			  rmsError(0.0f), averageFL(0.0f), averageNDVI (0.0f), ndviSigmaSum(0.0f),
			  averageHomogeneity(0.0f), averageIsotropy(0.0f),
			  boundaryPixels(0), boundaryGradient(0.0f), 
			  pDSM(0), pBuildingMask(0), building(0)
{ 
};

//=============================================================================

CDSMBuilding::CDSMBuilding(int Index) : index (Index),
              rMin(0), cMin(0), width(0), height(0), pMin(0,0,0), pMax(0,0,0), 
			  centreOfGravity(0,0,0), extents(0,0), angle(0), squaredSums(0,0,0), 
			  nPixels(0), nPointPixels (0), nHomogeneousPixels(0), rmsError(0.0f), 
			  averageFL(0.0f), averageNDVI (0.0f), ndviSigmaSum(0.0f),
			  averageHomogeneity(0.0f), averageIsotropy(0.0f),
			  boundaryPixels(0), boundaryGradient(0.0f), 
			  pDSM(0), pBuildingMask(0), building(0)
    
{ 
};

//=============================================================================

CDSMBuilding::CDSMBuilding(const CDSMBuilding &source) : trafo(source.trafo), 
              index(source.index), width(source.width), height(source.height), 
		      pMin(source.pMin), pMax(source.pMax),  
		      centreOfGravity(source.centreOfGravity), angle(source.angle), 
   		      extents(source.extents), nPixels(source.nPixels), 
		      nPointPixels(source.nPointPixels),
		      nHomogeneousPixels(source.nHomogeneousPixels),
			  rmsError(source.rmsError), 
			  averageNDVI (source.averageNDVI), ndviSigmaSum(source.ndviSigmaSum), 
			  averageFL(source.averageFL),
			  averageHomogeneity(source.averageHomogeneity), 
			  averageIsotropy(source.averageIsotropy),
			  boundaryGradient(source.boundaryGradient), 
			  boundaryPixels(source.boundaryPixels),
			  squaredSums(source.squaredSums),
			  pDSM(0), pBuildingMask(0), building(0)
{
	if (source.pDSM) 
		this->pDSM = new CImageBuffer(*source.pDSM);

	if (source.pBuildingMask) 
		this->pBuildingMask = new CImageBuffer(*source.pBuildingMask);

	if (source.building) this->building = new CBuilding(*source.building);
};

//=============================================================================

CDSMBuilding::~CDSMBuilding()
{
	if (this->pDSM)          delete this->pDSM; 
	if (this->pBuildingMask) delete this->pBuildingMask; 
	if (this->building)      delete this->building;

	this->pDSM          = 0;
	this->pBuildingMask = 0; 
	this->building      = 0;
};

//=============================================================================

CLabel CDSMBuilding::getBuildingLabelFromIndex() const
{
	ostrstream oss;
	oss << "BLD"; 
	if (this->index < 1) oss << "-1";
	else
	{
		int nZeros = 8 - (int) floor(log10((double) this->index));
		for (int i = 0; i < nZeros; ++i) oss << "0";
		oss << this->index;
	}

	oss << ends;
	char *z = oss.str();
	CLabel lab(z);
	delete [] z;

	return lab;
};

//=============================================================================
   
CCharString CDSMBuilding::getString() const
{
	ostrstream ostrstr;
	ostrstr.setf(ios::fixed,ios::floatfield);
	ostrstr.precision(3);

    ostrstr<< "REGION("   << this->index 
           << ")NPIX("    << this->nPixels 
		   << ")BORDER("  << this->pMin.x << " " << this->pMin.y << " " << this->pMin.z 
           << " "         << this->pMax.x << " " << this->pMax.y << " " << this->pMax.z 
		   << ")PIXSIZE(" << fabs(this->trafo.getA()) 
           << ")POINT("   << this->nPointPixels 
           << ")HOMOG("   << this->nHomogeneousPixels 
           << ")HAVG("    << this->centreOfGravity.z
           << ")NDVI("    << this->averageNDVI
           << ")FRSTLST(" << this->averageFL
           << ")HOMAVG("  << this->averageHomogeneity
           << ")ISOAVG("  << this->averageIsotropy
		   << ")COG("     << this->centreOfGravity.x << " " << this->centreOfGravity.y
           << ")EXT("     << this->extents.x << " " << this->extents.y
           << ")RMS("     << this->rmsError
           << ")ANG("     << this->angle
           << ")SQR("     << this->squaredSums.x << " " << this->squaredSums.y << " " << this->squaredSums.z
           << ")" << ends;

	char *z = ostrstr.str();
	CCharString str(z);
	delete [] z;
  
	return str;
};
//=============================================================================

CCharString CDSMBuilding::createRegionDir(const CCharString &path) const
{
	ostrstream oss;
	oss << "REGION_" << this->index << ends;
	char *z = oss.str();

	CCharString dir(z);
	delete [] z;
	dir = path + CSystemUtilities::dirDelimStr + dir;

	CSystemUtilities::mkdir(dir);

	dir += CSystemUtilities::dirDelimStr;
  
	return dir;
};

//=============================================================================

void CDSMBuilding::deleteRegionDir(const CCharString &path) const
{
	ostrstream oss;
	oss << "REGION_" << this->index << ends;
	char *z = oss.str();

	CCharString dir(z);
	delete [] z;

	dir = path + CSystemUtilities::dirDelimStr + dir;

	CSystemUtilities::delS(dir);

 };


//=============================================================================

bool CDSMBuilding::indexIsInBuilding(unsigned long index) const
{
	return (this->pBuildingMask->pixUChar(index) != 0);
};

//=============================================================================

void CDSMBuilding::getColour(unsigned long index, 
		                     unsigned char &r, unsigned char &g, unsigned char &b)
{
	if (index < 1) r = g = b = 255;
	else
	{
  		r = (unsigned char) ((unsigned long) 37 * index) % 255;
		g = (unsigned char) index % 255;
		b = (unsigned char) (255 - (((unsigned long)  47 * index) % 255));
	}
};

//=============================================================================

void CDSMBuilding::copyData(int Index, const CDSMBuilding &source)
{
	this->index              = Index;
	this->rMin               = source.rMin;
	this->cMin               = source.cMin; 
	this->width              = source.width;
	this->height             = source.height;
	this->pMin               = source.pMin;
	this->pMax               = source.pMax;
	this->trafo              = source.trafo;
	this->centreOfGravity    = source.centreOfGravity;
	this->extents            = source.extents;
	this->angle              = source.angle;
	this->squaredSums        = source.squaredSums;
	this->nPixels            = source.nPixels;
	this->nPointPixels       = source.nPointPixels;
	this->nHomogeneousPixels = source.nHomogeneousPixels;
	this->rmsError           = source.rmsError;
	this->averageNDVI        = source.averageNDVI;
	this->ndviSigmaSum       = source.ndviSigmaSum;
	this->averageFL          = source.averageFL;
	this->averageHomogeneity = source.averageHomogeneity;
	this->averageIsotropy    = source.averageIsotropy;
	this->boundaryGradient   = source.boundaryGradient;
	this->boundaryPixels     = source.boundaryPixels;
};

//=============================================================================

CDSMBuilding &CDSMBuilding::operator= (const CDSMBuilding& source)
{
	if (this != &source)
	{
		this->copyData(source.index, source);
		if (this->pBuildingMask) delete pBuildingMask;
		if (this->pDSM)          delete pDSM;
		if (this->building)      delete this->building;
		this->pBuildingMask = 0;   
		this->pDSM          = 0;
		this->building      = 0;
	};

	return *this; 
};

//=============================================================================

void CDSMBuilding::resetData(int Index)
{
	this->index              = Index; 
	this->rMin               = 0;
	this->cMin               = 0;
	this->width              = 1;
	this->height             = 1;
	this->nPixels            = 0;
	this->nPointPixels       = 0;
	this->nHomogeneousPixels = 0;
	this->rmsError           = 0.0f; 
	this->averageNDVI        = 0.0f;
  	this->averageNDVI        = 0.0f;
  	this->ndviSigmaSum       = 0.0f;
  	this->averageFL          = 0.0f;
  	this->averageHomogeneity = 0.0f;
  	this->averageIsotropy    = 0.0f;
  	this->boundaryGradient   = 0.0f; 
  	this->boundaryPixels     = 0;

  	this->centreOfGravity.x  = 0.0f;
	this->centreOfGravity.y  = 0.0f;
	this->centreOfGravity.z  = 0.0f;
  	this->extents.x  = 0.0f;
  	this->extents.y  = 0.0f;
  	this->angle = 0.0f;

	this->pMin.x = this->pMin.y = this->pMin.z = 0.0f;
	this->pMax.x = this->pMax.y = this->pMax.z = 0.0f;
     
  	this->squaredSums.x = 0.0f;
  	this->squaredSums.y = 0.0f;
  	this->squaredSums.z = 0.0f;
  	this->trafo.reset();
};

void CDSMBuilding::expand(int r, int c)
{
	int rmax = this->rMin + height - 1;
	int cmax = this->cMin + width  - 1;
	if (r < this->rMin) this->rMin   = r;
	if (c < this->cMin) this->cMin   = c;
	if (r > rmax) this->height = r + 1 - this->rMin;
	if (c > cmax) this->width  = c + 1 - this->cMin;
};
   
//=============================================================================

void CDSMBuilding::initData(int Index, unsigned long NPixels, int r0, int c0)
{
	this->resetData(Index);
	this->nPixels = NPixels;
	this->rMin   = r0;
	this->cMin   = c0;
	this->width  = 1;
	this->height = 1;
};

//=============================================================================

void CDSMBuilding::initDataEditing()
{
	this->nPixels = 0;
 	this->pMin.z = FLT_MAX;
  	this->pMax.z = -FLT_MAX;
  	this->nPointPixels = 0;
  	this->nHomogeneousPixels = 0;
  	this->averageNDVI = 0.0f;
  	this->ndviSigmaSum = 0.0f;
  	this->averageFL = 0.0f;
	this->averageHomogeneity = 0.0f;
	this->averageIsotropy = 0.0f;
	this->boundaryGradient = 0.0f; 
	this->boundaryPixels   = 0;

  	this->centreOfGravity.x  = 0.0f;
	this->centreOfGravity.y  = 0.0f;
	this->centreOfGravity.z  = 0.0f;
  	this->extents.x  = 0.0f;
  	this->extents.y  = 0.0f;
  	this->angle = 0.0f;  
  	this->squaredSums.x = 0.0f;
  	this->squaredSums.y = 0.0f;
  	this->squaredSums.z = 0.0f;
};

//=============================================================================
 
void CDSMBuilding::editData(float DTMHeight, float DSMHeight, int r, int c, 
		                    bool isPoint, bool isHomogeneous, float w, float q)
{
	++nPixels; 
	if (this->pMin.z > DTMHeight) this->pMin.z = DTMHeight;
	if (this->pMax.z < DSMHeight) this->pMax.z = DSMHeight;

	this->centreOfGravity.z += DSMHeight;

	expand(r, c);

	if (isPoint)       nPointPixels++;
	if (isHomogeneous) nHomogeneousPixels++;

	averageHomogeneity  += w;
	averageIsotropy     += q;
	centreOfGravity.x   += c;
	centreOfGravity.y   += r;
	squaredSums.x       += double(c) * double(c);
	squaredSums.y       += double(r) * double(r);
	squaredSums.z       += double(c) * double(r);
};
 
//=============================================================================

void CDSMBuilding::editData(float DTMHeight, float DSMHeight, int r, int c, 
	                        bool isPoint, bool isHomogeneous,  float w, float q, 
							float NDVI, float DeltaFL, float sigmaNDVI)
{
	this->editData(DTMHeight, DSMHeight, r, c, isPoint, isHomogeneous, w, q);
	this->averageFL   += DeltaFL;

	if (sigmaNDVI > 0.0f) 
	{
		sigmaNDVI = 1.0f / (sigmaNDVI * sigmaNDVI);
		if (sigmaNDVI > 10000.0f) sigmaNDVI = 10000.0f;
	}
	else
	{
		sigmaNDVI = 0.1f;
    };

	this->averageNDVI  += sigmaNDVI * NDVI;
	this->ndviSigmaSum += sigmaNDVI;
};

//=============================================================================

void CDSMBuilding::finishDataEditing(int increaseSize, const C2DPoint &dsmOrg,
									 const C2DPoint &dsmPixSize,
									 int maxWidth, int maxHeight)
{
	int rMax = this->rMin + this->height + increaseSize;
	int cMax = this->cMin + this->width + increaseSize;
	this->cMin -= increaseSize;
	this->rMin -= increaseSize;
	if (this->rMin < 0) this->rMin = 0;
	if (this->cMin < 0) this->cMin = 0;
	if (rMax >= maxHeight) rMax = maxHeight - 1;
	if (cMax >= maxWidth)  cMax = maxWidth  - 1;
	this->width  = cMax - cMin + 1;
	this->height = rMax - rMin + 1;
  
	if (this->ndviSigmaSum > FLT_EPSILON) this->averageNDVI /= ndviSigmaSum;

	if (nPixels > 0)
	{
		this->averageFL          /= nPixels;
		this->averageHomogeneity /= nPixels;
		this->averageIsotropy    /= nPixels;

		this->centreOfGravity    /= nPixels;

		C3DPoint sqrPos(centreOfGravity.x * centreOfGravity.x,
                        centreOfGravity.y * centreOfGravity.y,
                        centreOfGravity.x * centreOfGravity.y);

   
		this->squaredSums -= sqrPos * nPixels;

		double hlp = squaredSums.x - squaredSums.y;
   
		this->angle = 100.0f + 100.0f / M_PI * atan2(2 * squaredSums.z, hlp);
  
		hlp = sqrt(hlp * hlp + 4 * squaredSums.z * squaredSums.z) / 2.0f;

		extents.x = extents.y = (squaredSums.x + squaredSums.y) / 2.0f;
		extents.x += hlp;
		extents.y -= hlp;
		extents.x = sqrt(12.0f * extents.x / nPixels);
		extents.y = sqrt(12.0f * extents.y / nPixels);
	}
	else    
	{
		this->centreOfGravity.x = this->centreOfGravity.y = this->centreOfGravity.z = 0.0;
		this->extents.x = this->extents.y = 0.0f;
		this->angle = 0.0f;  
		this->squaredSums.x = this->squaredSums.y =this->squaredSums.z = 0.0f;
	};

	this->setOrgAndPixSize(dsmOrg, dsmPixSize);
	double z = this->centreOfGravity.z;
	C2DPoint p(this->centreOfGravity.x - this->cMin, this->centreOfGravity.y - this->rMin);
	this->trafo.transformObs2Obj(this->centreOfGravity, p);
	this->centreOfGravity.z = z;

	this->extents.x *= dsmPixSize.x;
	this->extents.y *= dsmPixSize.y;
};
   
//=============================================================================

void CDSMBuilding::computeRMS(CImageBuffer &NormDSM, CLabelImage &LabelImage)
{
	this->rmsError = 0;
	float relativeHeight = float (this->centreOfGravity.z - this->pMin.z);

	for (unsigned long u = 0; u < NormDSM.getNGV(); ++u)
	{
		if (LabelImage.getLabel(u) == this->index)
		{
			float dh = NormDSM.pixFlt(u) - relativeHeight;
			this->rmsError += dh * dh;
		};

	};

	this->rmsError = (float) sqrt(rmsError / (this->nPixels - 1));
};

//=============================================================================

void CDSMBuilding::setOrgAndPixSize(const C2DPoint &DSMOrg,
									const C2DPoint &DSMPixSize)
{
	int rmax = this->rMin + this->height - 1;
	int cmax = this->cMin + this->width  - 1;
	this->pMin.x = DSMOrg.x + double(this->cMin) * DSMPixSize.x;
	this->pMin.y = DSMOrg.y - double(rmax)       * DSMPixSize.y;
	this->pMax.x = DSMOrg.x + double(cmax)       * DSMPixSize.x;
	this->pMax.y = DSMOrg.y - double(this->rMin) * DSMPixSize.y;

	C2DPoint shift(this->pMin.x, this->pMax.y);
	this->trafo.setFromShiftAndPixelSize(shift, DSMPixSize);
};

//=============================================================================
	  
bool CDSMBuilding::intersectExtents(int RMin, int CMin, int Width, int Height)
{
	bool ret = true;

	int rmax = this->rMin + this->height - 1;
	int cmax = this->cMin + this->width  - 1;

	int OtherRMax = RMin + Height - 1;
	int OtherCMax = CMin + Width  - 1;

	if ((this->rMin > OtherRMax) || (this->cMin > OtherCMax) ||
		(rmax < RMin) || (cmax < CMin)) ret = false;
	else
	{
		if (this->cMin < CMin) this->cMin = CMin;
		if (this->rMin < RMin) this->rMin = RMin;
		if (cmax > OtherCMax) this->width  = OtherCMax + 1 - this->cMin;
		if (rmax > OtherRMax) this->height = OtherRMax + 1 - this->rMin;
	}
	
	return ret;
};

//=============================================================================

void CDSMBuilding::clearDSMdata()
{
	if (this->pDSM)          delete this->pDSM;
	if (this->pBuildingMask) delete this->pBuildingMask;
	if (this->building)      delete this->building;
 
	this->pDSM          = 0;
	this->pBuildingMask = 0;
	this->building      = 0;
};

//=============================================================================

void CDSMBuilding::initMask(CLabelImage &LabelImage)
{
	if (!this->pBuildingMask) 
		this->pBuildingMask = new CImageBuffer(0, 0, this->width, this->height,1,1,true);
	else this->pBuildingMask->set(0, 0, this->width, this->height,1,1,true);

	unsigned long indexMin      = LabelImage.getIndex(this->rMin, this->cMin);
	unsigned long indexMax      = LabelImage.getIndex(this->rMin + this->height, 
		                                              this->cMin + this->width);
	unsigned long indexMaxInRow = LabelImage.getIndex(this->rMin, this->cMin + this->width);

	unsigned long indexMask = 0;

	for (; indexMaxInRow < indexMax; indexMaxInRow += LabelImage.getDiffY(), indexMin += LabelImage.getDiffY())
	{
		for (unsigned long index = indexMin; index < indexMaxInRow; index += LabelImage.getDiffX(), indexMask += this->pBuildingMask->getDiffX())
		{ 
			if (LabelImage.getLabel(index) == this->index)
				this->pBuildingMask->pixUChar(indexMask) = 1;
			else
				this->pBuildingMask->pixUChar(indexMask) = 0;
		}
	}; 
};

//=============================================================================

void CDSMBuilding::computeBoundaryGradient(CLabelImage &LabelImage, 
										   const CDSMRoughness &roughness)
{
	this->boundaryGradient = 0.0f;
	this->boundaryPixels = 0;
	CImageBuffer borderMask(this->cMin, this->rMin, this->width, this->height,1,1,true, 0);

	CImageBuffer gradBuf(this->cMin, this->rMin, this->width, this->height, 1, 32, true, 0);

	unsigned long firstLabel   = LabelImage.getIndex(this->rMin, this->cMin);
	unsigned long lastLabelRow = LabelImage.getIndex(this->rMin, this->cMin + this->width - 1);
	unsigned long lastLabel    = LabelImage.getIndex(this->rMin + this->height - 1, this->cMin + this->width - 1);

	for (unsigned long firstLocal = 0; lastLabelRow < lastLabel; lastLabelRow += LabelImage.getDiffY(), 
		                                                         firstLabel += LabelImage.getDiffY(),
																 firstLocal += borderMask.getDiffY())
	{
		unsigned long u = firstLocal;
		for (unsigned long labelIdx = firstLabel; labelIdx < lastLabelRow; labelIdx += LabelImage.getDiffX(), 
			                                                               u += borderMask.getDiffY())
		{
			float gradientStr = roughness.gradientStrength(labelIdx, 1);
			gradBuf.pixFlt(u) = sqrt(gradientStr);
			unsigned short label  = LabelImage.getLabel(labelIdx);
			unsigned short labelX = LabelImage.getLabel(labelIdx + LabelImage.getDiffX());
			unsigned short labelY = LabelImage.getLabel(labelIdx + LabelImage.getDiffY());

			if (((label == this->index) && ((labelX != this->index) || (labelY != this->index)) ) ||
				((label != this->index) && ((labelX == this->index) || (labelY == this->index))))

			{
				borderMask.pixUChar(u)  = 1;
				boundaryGradient += gradientStr;
				++boundaryPixels;
			};  
		};
	};
  
	CImageBuffer borderMask1(borderMask);
	CImageFilter morpho(eMorphological, 5, 5, 1.0);
	morpho.binaryMorphologicalClose(borderMask1,borderMask,1);

	boundaryGradient = 0.0f;
	boundaryPixels = 0;

	for (unsigned long u = 0; u < borderMask.getNGV(); ++u)
	{
		if (borderMask.pixUChar(u))
		{
			boundaryGradient += sqrt(roughness.gradientStrength(u, 1));
			++boundaryPixels;
		};
	};
  
	if (boundaryPixels) boundaryGradient /= boundaryPixels;
	boundaryGradient = sqrt(boundaryGradient);
};
  
//=============================================================================

void CDSMBuilding::maxBoundaryGradient(CLabelImage &LabelImage, 
									   const CDSMRoughness &roughness, 
									   const CXYLineSegment &segment,
									   C2DPoint &maxGrad, 
									   int &maxIndexX, int &maxIndexY,
									   bool squareFlag)
{
	int firstIndex = -1;
	int lastIndex = -1;

	vector <C3DPoint> *pDerivatives = roughness.pDerivatives(segment, squareFlag);
  
	for (unsigned long u = 0; u < pDerivatives->size(); ++u) 
	{
		int r = (int) floor((*pDerivatives)[u].y + 0.5);
		int c = (int) floor((*pDerivatives)[u].x + 0.5);
		unsigned long labelIndex = LabelImage.getIndex(r,c);
		unsigned short label = LabelImage.getLabel(labelIndex);

		if (label != this->index) 
		{
			(*pDerivatives)[u].y = 0;
		}
		else 
		{
			if (firstIndex < 0) firstIndex = u;
			lastIndex = u;

			(*pDerivatives)[u].y = label;
		};
	};


	int firstMin = firstIndex - 3;
	if (firstMin < 0) firstMin = 0;
	int firstMax = firstIndex + 2;
	if (firstMax >= lastIndex) firstMax = (firstIndex + lastIndex) / 2;

	int lastMin = lastIndex - 2;
	if (lastMin <= firstIndex) lastMin = (firstIndex + lastIndex) / 2;
	int lastMax = lastIndex + 3;
	if (lastMax >= (int) pDerivatives->size()) lastMax = pDerivatives->size() - 1;

	maxIndexX = -1;
	maxIndexY = -1;
	maxGrad.x  = -FLT_MAX;
	maxGrad.y  = -FLT_MAX;
  
	if (firstIndex >= 0)
	{
		for (long u = firstMin; u <= firstMax; ++u)
		{
			if ((*pDerivatives)[u].z > maxGrad.x)
			{
				maxGrad.x = (*pDerivatives)[u].z;
				maxIndexX = u;
			};
		};

		for (long u = lastMin; u <= lastMax; ++u)
		{
			if ((*pDerivatives)[u].z > maxGrad.y)
			{
				maxGrad.y = (*pDerivatives)[u].z;
				maxIndexY = u;
			};
		};
	};

	delete pDerivatives;
};

//=============================================================================

void CDSMBuilding::print (ostream &str) const
{
	str.setf(ios::fixed, ios::floatfield);
	str << "REGION (";
	str.width(4);
	str << index << ")\n  WINDOW: ";
	str.width(5);
	str << this->rMin << " / ";
	str.width(5);
	str << this->cMin << " - ";
	str.width(5);
	str << this->rMin + this->height - 1 << " / ";
	str.width(5);
	str << this->cMin + this->width - 1 << "\n  ORIGIN: " ;
	str.precision(3);
	str.width(15);
	str << pMin.x << " ";
	str.width(15);
	str << pMax.y << "\n  RESOLUTION: " << fabs(this->trafo.getA())
		<< "\n  NPIX: ";
	str.width(10);

	str << nPixels << "\n  CENTRE: " ;
	str.width(10);
	str << centreOfGravity.x << " / ";
	str.width(10);
	str << centreOfGravity.y 		   
		<< "\n  EXTENTS: ";
	str.width(10);
	str << extents.x << " / ";
	str.width(10);
	str << extents.y << "\n  ANGLE: ";
	str.precision(4);
	str << angle <<"\n  HMIN: ";
	str.width(10);
	str.precision(3);
	str << getFloorHeight() <<"\n  HMAX: ";
	str.width(10);
	str << getMaxHeight() <<"\n  HAVG: ";
	str.width(10);
	str << centreOfGravity.z <<"\n  RMS: ";
	str.width(10);
	str << rmsError <<"\n  POINT PERCENTAGE: ";
	str.width(10);
	str.precision(2);
	str << getPointRatio() * 100.0f<<"%\n  HOMOGENEOUS PERCENTAGE: ";
	str.width(10);
	str.precision(2);
	str << getHomogeneousRatio() * 100.0f<<"%\n  NDVI: ";
	str.width(10);
	str.precision(0);
	str << getAverageNDVI() << "\n  AVERAGE FIRST- LAST PULSE: ";
	str.width(10);
	str.precision(2);
	str << getAverageFL() << " m\n  AVERAGE HOMOGENEITY: ";
	str.width(10);
	str.precision(2);
	str << getAverageHomogeneity() << " \n  AVERAGE ISOTROPY: ";
	str.width(10);
	str.precision(2);
	str << getAverageIsotropy() << "\n  COLOR: ";

	unsigned char r, g, b;
	getColour(r, g, b);
	str.width(4);
	str << r << " ";
	str.width(4);
	str << g << " ";
	str.width(4);
	str << b << "\nENDREGION" << endl;
 };

//=============================================================================

void CDSMBuilding::print (const CCharString &filename) const
{
	ofstream str(filename.GetChar());
	print(str);
};

//=============================================================================

void CDSMBuilding::printDef(const CCharString &directory) const
{
	print(createRegionDir(directory) + "region.dat");
};

//=============================================================================

CXYZPolyLineArray *CDSMBuilding::getRoofPolys(const CCharString &dir, float thinningThreshold)
{
	float closeMin = 2.0;

	CXYZPolyLineArray *polylines = 0;

	int fact = 3;

	thinningThreshold = thinningThreshold * fact / (float) fabs(trafo.getA());

	CImageBuffer edgePixelBuffer;

	edgePixelBuffer.resample(*pBuildingMask, fact);
/*

	if (this->index == 421) 
	{
	    ostrstream oss;
		oss << dir.GetChar() << "\\BLD\\fact" << this->index << ".tif" << ends;
		char *z = oss.str();
		edgePixelBuffer.exportToTiffFile(z, 0, false);
		delete[]z;
	}
*/	
	CImageBuffer multipliedWin;
	multipliedWin.set(edgePixelBuffer.getOffsetX(), edgePixelBuffer.getOffsetY(), edgePixelBuffer.getWidth(), edgePixelBuffer.getHeight(),1,1, true);

	CImageFilter morpho(eMorphological, fact, fact, 1.0);

	morpho[0] = 0; morpho[1] = 1; morpho[2] = 0;
	morpho[3] = 1; morpho[4] = 1; morpho[5] = 1;
	morpho[6] = 0; morpho[7] = 1; morpho[8] = 0;

	morpho.binaryMorphologicalOpen(edgePixelBuffer, multipliedWin, 1);
/*
	if (this->index == 421) 
	{
	    ostrstream oss;
		oss << dir.GetChar() << "\\BLD\\factmorph" << this->index << ".tif" << ends;
		char *z = oss.str();
		multipliedWin.exportToTiffFile(z, 0, false);
		delete[]z;
	}
*/
	if (multipliedWin.markEdgePixels(edgePixelBuffer)) 
	{
/*		if (this->index == 421) 
		{
			ostrstream oss;
			oss << dir.GetChar() << "\\BLD\\edgels" << this->index << ".tif" << ends;
			char *z = oss.str();
			edgePixelBuffer.exportToTiffFile(z, 0, false);
			delete[]z;
		}
*/
		CXYContourArray contours;
		if (edgePixelBuffer.getContours(contours))
		{
			CTFW multipleTFW;
			C2DPoint multiplePixSize(fabs(this->trafo.getA()), fabs(this->trafo.getE()));
			multiplePixSize /= (double) fact;
			C2DPoint multiplePixShift(this->trafo.getShift());
			multiplePixShift.x -= multiplePixSize.x;
			multiplePixShift.y += multiplePixSize.y;

			multipleTFW.setFromShiftAndPixelSize(multiplePixShift, multiplePixSize);

			polylines = new CXYZPolyLineArray;

			for (int i = 0; i < contours.GetSize(); ++i)
			{
				CXYContour *contour = contours.GetAt(i);
				if (contour->getClosingGap() >= closeMin)
				{
					contour->removePoint(contour->getPointCount() - 1);
				}

				if ((contour->getClosingGap() < closeMin) && (contour->getPointCount() > 3))
				{
					contour->addPoint(*(contour->getPoint(0)));

					CXYPolyLine poly;
					poly.initFromContour(contour, thinningThreshold, fact);
					poly.closeLine();
					CXYZPolyLine *poly3D = polylines->add();

					CXYZPoint p;
					double z = this->getAverageHeight();

					for (int j = 0; j < poly.getPointCount(); ++j)
					{
						multipleTFW.transformObs2Obj(p, (C2DPoint) poly.getPoint(j), z);
						poly3D->addPoint(p);
					}
					poly3D->closeLine();
				}
			};

			if (polylines->GetSize() == 0) 
			{
				delete polylines;
				polylines = 0;
			}
			else
			{
				int imax = 0;
				float maxArea = (float) polylines->GetAt(imax)->getAreaAbs();
				for (int i = 1; i < polylines->GetSize(); ++i)
				{
					float area = (float) polylines->GetAt(i)->getAreaAbs();
					if (area > maxArea)
					{
						maxArea = area;
						imax = i;
					}
				}

				if (imax > 0)
				{
					CXYZPolyLineArray *oldPolylines = polylines;
					polylines = new CXYZPolyLineArray;
					polylines->Add(*oldPolylines->GetAt(imax));

					for (int i = 0; i < oldPolylines->GetSize(); ++i)
					{
						if (imax != i)
						{
							polylines->Add(*oldPolylines->GetAt(i));
						}
					}

					delete oldPolylines;
				}

				for (int i = 1; i < polylines->GetSize(); ++i)
				{
					if (!polylines->getAt(0)->contains(*polylines->getAt(i)))
					{
						polylines->RemoveAt(i);
					}
				}
			}
		}
	}

	return polylines;

};

//=============================================================================

void CDSMBuilding::addPoly(CXYZPolyLine &roofPoly)
{
	CXYZPolyLine floor;
	double z = this->getFloorHeight();

	CLabel floorLabel("F01");
	CLabel roofLabel("R01");
	if (this->building) 
	{
		floorLabel += this->building->getPointCount();
		roofLabel += this->building->getPointCount();
	}
	for (int i = 0; i < roofPoly.getPointCount(); ++ i)
	{
		CXYZPoint *p = roofPoly.getPoint(i);
		p->setLabel(roofLabel);
		roofLabel++;
		CXYZPoint p1(p); 
		p1.setLabel(floorLabel);
		floorLabel++;
		p1.z = z;
		floor.addPoint(p1);
	}

	if (!this->building) 
	{
		this->building = new CBuilding;	
		this->building->setLabel(this->getBuildingLabelFromIndex());
		this->building->addRoofPlane(roofPoly, floor, true);
	}
    else
    {
		this->building->addInnerCourtyard(roofPoly, floor, false);
    };
};

//=============================================================================

bool CDSMBuilding::createPrismaticModel(const CCharString &dir, float thinningThreshold)
{
	bool ret = true;

	if (this->building) delete this->building;
	this->building = 0;

	CXYZPolyLineArray *roofPolys = this->getRoofPolys(dir, thinningThreshold);
	if (!roofPolys) ret =  false;
	else
	{
		for(int pIdx= 0; pIdx < roofPolys->GetSize(); ++pIdx)
		{
			CXYZPolyLine *roof = roofPolys->getAt(pIdx);
			this->addPoly(*roof);
		}

		delete roofPolys;

		building->resetCurrentFace();
	};


	return ret;
};

//=============================================================================

void CDSMBuilding::exportBoundaryPolys(float thinningThreshold,
									   ofstream *winputFile,
									   ofstream *dxfFile,
									   unsigned long &linecode, 
									   int vrmlFlag, const CCharString &directory)
{
	if (this->createPrismaticModel(directory, thinningThreshold))
	{
		CXYZPolyLineArray floorPolys;
		this->building->getFloors(floorPolys);

		ofstream *locWinputFile = 0;
		if (winputFile)  locWinputFile = winputFile;
		else locWinputFile = new ofstream (createRegionDir(directory) + "poly.wnp");

		if (!winputFile) 
			*locWinputFile << "  99999991     0.000     0.000     0.000\n"
  			               << "  99999998     0.000     0.000     0.000\n";

		for (int j = 0; j < floorPolys.GetSize(); ++j)
		{
			CXYZPolyLine *floor = floorPolys.getAt(j);

			// export

			++linecode;
		};

	
		if (!winputFile) 
		{
			*locWinputFile << "  99999999     0.000     0.000     0.000\n";
			delete locWinputFile;
		}
	}
};
#endif // WIN32
