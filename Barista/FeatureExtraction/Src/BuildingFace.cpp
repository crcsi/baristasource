/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <strstream>
using namespace std;

#include <float.h>

#include "newmat.h"

#include "BuildingFace.h"
#include "BuildingFacePoly.h"
#include "BuildingFaceNeighbourVector.h"
#include "XYZPolyLine.h"
#include "LabelImage.h"

//****************************************************************************

float CBuildingFace::rmsXY = 0.25f;

float CBuildingFace::rmsZ  = 0.075f;


//============================================================================

CBuildingFace::CBuildingFace(): index (0), distThreshold(0.0f)
{
};

//============================================================================

CBuildingFace::CBuildingFace(unsigned short Index): index(Index), distThreshold(0.0f)
{
};

//============================================================================

CBuildingFace::CBuildingFace(const CBuildingFace &face): C3DAdjustPlane(face), 
                  index(face.index), distThreshold(face.distThreshold),
				  trafo(face.trafo)
{
};

//============================================================================

CBuildingFace::~CBuildingFace()
{
	this->resetPolys();
};

//============================================================================

float CBuildingFace::getRmsFromRmsZ () const
{
	float pixSize = (float) this->trafo.getScaleX();

	float TwoPix  = 2.0f * pixSize;
	float FivePix = 5.0f * pixSize;

	float rmsXY = CBuildingFace::rmsZ / (float) this->getObliqueness();
	if (rmsXY < TwoPix) rmsXY = TwoPix;
	else if (rmsXY > FivePix) rmsXY = FivePix;

	return rmsXY;
};
	 
//============================================================================

float CBuildingFace::getThreshold(const C3DPoint &p) const
{
	ColumnVector vec(4);
	vec.element(0) = p.x;
	vec.element(1) = p.y;
	vec.element(2) = p.z;
	vec.element(3) = 1.0;
	Matrix val = vec.t() * this->qxx * vec;
	float thresh = (float)  sqrt(val.element(0, 0) + (this->normal.x * this->normal.x + 
		                                              this->normal.y * this->normal.y) * rmsXY * rmsXY  + 
													  this->normal.z * this->normal.z * rmsZ * rmsZ);
	thresh *= this->quantil;

	return thresh;
};
 
//============================================================================
	  
vector<CBuildingFaceNeighbourRecord *> *CBuildingFace::getNeighbours(unsigned short region) const
{
	vector<CBuildingFaceNeighbourRecord *> *pVec = new vector<CBuildingFaceNeighbourRecord *>;

	for (unsigned int i = 0; i < this->polyVector.size(); ++i)
	{  
		vector<CBuildingFaceNeighbourRecord *> *pVec1 = this->polyVector[i]->getNeighbours(region);
		if (pVec1)
		{
			unsigned int uP = pVec->size();
			pVec->resize(uP + pVec1->size());
			for (unsigned int u = 0; u < pVec1->size(); ++u)
			{
				(*pVec)[uP + u] = (*pVec1)[u];
			};

			delete pVec1;
		};
	};

	if (pVec->size() == 0) 
	{
		delete pVec;
		pVec = 0;
	};

	return pVec ;
};

//============================================================================
	  
vector<unsigned short> CBuildingFace::getNeighbouringFaceIncices() const
{
	vector<unsigned short> NeighboursIndexVec;

	for (unsigned int i = 0; i < this->polyVector.size(); ++i)
	{  
		for (unsigned int j = 0; j < this->polyVector[i]->getNNeighbours(); ++j)
		{  
			CBuildingFaceNeighbourRecord* pNeighbour = this->polyVector[i]->getNeighbour(j);
			unsigned short neigh = pNeighbour->getRegion1();
			if (neigh == this->index) neigh = pNeighbour->getRegion2();
			bool found = false;
			for (unsigned int k = 0; (k < NeighboursIndexVec.size()) && !found; ++k)
			{
				found = (NeighboursIndexVec[k] == neigh);
			}

			if (!found) NeighboursIndexVec.push_back(neigh);
		};
	};

	
	return NeighboursIndexVec;
};

//============================================================================

void CBuildingFace::setTrafo(const CTFW &Trafo)
{
	this->trafo = Trafo;
};

//============================================================================

void CBuildingFace::resetPolys()
{
	for (unsigned int u = 0; u < this->polyVector.size(); ++u)
	{
		if (this->polyVector[u]) delete this->polyVector[u];
		this->polyVector[u] = 0;
	};

	this->polyVector.clear();
};

//============================================================================

void CBuildingFace::copyData(unsigned short Index, const CBuildingFace &face)
{
	this->copyPlane(face);
	this->copySums(face);
	this->trafo         = face.trafo;
	this->index         = Index; 
	this->distThreshold = face.distThreshold;
};

//============================================================================

void CBuildingFace::resetData(unsigned short Index, C3DPoint &Red)
{
	this->resetPlane();
	this->resetSums(Red);
	this->index         = Index; 
	this->distThreshold = 0.0f;
	this->resetPolys();
};

//============================================================================

void CBuildingFace::computeFacePars()
{
	if (this->nPoints > 0)
	{
		this->adjust(); 
		this->initThreshold();
	};
};
	  
//============================================================================

void CBuildingFace::initThreshold()
{
	initThreshold(C3DPoint(5.0, 5.0, 1.0));
};
	  
//============================================================================

void CBuildingFace::initThreshold(const C3DPoint &p)
{
	if (this->sigma0 < FLT_EPSILON) 
	{
		this->distThreshold = -FLT_MIN;
		this->sigma0 = FLT_MAX;
	}
	else
	{
		this->distThreshold = getThreshold(this->point + p);
	};
};

//============================================================================

void CBuildingFace::exportPolysDXF(ofstream &dxf, bool exportCombined, 
								   bool exportInitial, bool exportApprox, 
								   bool exportFinal) const
{
	for (unsigned int up = 0; up < this->polyVector.size(); ++up)
	{
		this->polyVector[up]->exportDXF(dxf, exportCombined, exportInitial, exportApprox, exportFinal);
	};
};

//============================================================================

void CBuildingFace::merge(CBuildingFace &face)
{
	*this += face;
	this->computeFacePars();

	face.resetData(-1, this->reduction);
};

//============================================================================

void CBuildingFace::checkPolys()
{
	if (this->polyVector.size() > 1)
	{
		if (this->polyVector[0]->getCombinedPoly())
		{
			float maxArea = (float) this->polyVector[0]->getCombinedPoly()->getAreaAbs();
			unsigned int uMax = 0;
			for (unsigned int u = 1; u < this->polyVector.size(); ++u)
			{
				float area = (float)  this->polyVector[u]->getCombinedPoly()->getAreaAbs();
				if (area > maxArea)
				{
					uMax = u;
					maxArea = area;
				};
			};

			if (uMax > 0)
			{
				CBuildingFacePoly *pSwap = this->polyVector[0];
				this->polyVector[0] = this->polyVector[uMax];
				this->polyVector[uMax] = pSwap;
			};

			CBuildingFacePoly *pOuterPoly = this->polyVector[0];

			for (unsigned int u = 1; u < this->polyVector.size(); ++u)
			{
				bool isContained = true;
				for (int u1 = 0; (u1 < this->polyVector[u]->getCombinedPoly()->getPointCount()) && isContained; ++u1)
				{
					CXYZPoint *p = this->polyVector[u]->getCombinedPoly()->getPoint(u1);
					isContained = pOuterPoly->getCombinedPoly()->contains(*p);
				};

				if (!isContained) this->removePoly(u);
			};
		}
	};
};

//============================================================================

void CBuildingFace::removePoly(unsigned int Index)
{
	if (this->polyVector.size())
	{
		if (this->polyVector[Index]) delete this->polyVector[Index];

		for (unsigned int u = Index + 1; u < this->polyVector.size(); ++u)
		{
			this->polyVector[u - 1] = this->polyVector[u];
		};

		this->polyVector.resize(this->polyVector.size() - 1);
	}
};

//============================================================================

void CBuildingFace::initPolys(CBuildingFaceNeighbourVector &neighbourhoodVector, 
		                      const CLabelImage &labelImage, bool useVoronoi)
{
	this->resetPolys();
	
	double pixS = this->trafo.getScaleX();
	float sig = 3.0f * (float) pixS;
	float maxDist = 2.5f * (float) pixS;
	float rmsPlanes = 1.0f;

	vector<vector<CXYContour *>> contourNeighbourVector; 
	vector<vector<unsigned short>> neighbours;

	if (labelImage.getContours(contourNeighbourVector, neighbours, this->index, useVoronoi))
	{						  
		for (unsigned int p = 0; p < contourNeighbourVector.size(); ++p)
		{
			CBuildingFacePoly *poly = new CBuildingFacePoly(this, this->trafo, this->reduction);

			for (unsigned int k = 0; k < contourNeighbourVector[p].size(); ++k)
			{
				CXYContour *c = contourNeighbourVector[p][k];
				CBuildingFaceNeighbourRecord *pNeighbour = 
					neighbourhoodVector.addNeighbours(this->index, neighbours[p][k], poly, rmsPlanes, c, sig, 
					                                  this->trafo, this->reduction, maxDist);
				poly->appendNeighbour(pNeighbour);
			}

			this->polyVector.push_back(poly);
		}
	}

	this->checkPolys();
};

//============================================================================

void CBuildingFace::print(ostream &anOstream) const
{
	C3DPoint curSqrXY, curSqrZ;
	curSqrXY.x = this->squaredSumsXY.x - this->point.x * this->point.x * this->nPoints;
	curSqrXY.y = this->squaredSumsXY.y - this->point.y * this->point.y * this->nPoints;
	curSqrXY.z = this->squaredSumsXY.z - this->point.x * this->point.y * this->nPoints;
	curSqrZ.x  = this->squaredSumsZ.x  - this->point.x * this->point.z * this->nPoints;
	curSqrZ.y  = this->squaredSumsZ.y  - this->point.y * this->point.z * this->nPoints;
	curSqrZ.z  = this->squaredSumsZ.z  - this->point.z * this->point.z * this->nPoints;
	anOstream << "  FACE (";
	anOstream.width(4);
	anOstream << this->index           << ")\n    NPIX:  ";
	anOstream.width(10);
	anOstream << this->nPoints         << "\n    CENTRE: ";
	anOstream.width(15);
	anOstream.setf(ios::fixed, ios::floatfield);
	anOstream.precision(3);
	anOstream << this->point.getString(" ",15,3)  << "\n    NORMAL: "
		      << this->normal.getString(" ",15,9) << "\n    CONST:  ";
	anOstream.width(10);
	anOstream << this->planeConstant << "\n    RMS:    ";
	anOstream.width(10);
	anOstream << this->sigma0        << "\n    THRESH: ";
	anOstream.width(10);
	anOstream << this->distThreshold        <<  "\n    EigenVal:  "
		      << this->eigenValues.getString(" ",15,5) << "\n    EigenVector 1:  "
			  << this->getEigenVector1().getString(" ",15,9) << "\n    EigenVector 2:  "
			  << this->getEigenVector2().getString(" ",15,9) << "\n    EigenVector 3:  "
			  << this->getEigenVector3().getString(" ",15,9) << "\n    Qxx: ";
	
	anOstream.setf(ios::fixed, ios::floatfield);
	anOstream.precision(9);
	int width = 20;
	anOstream.width(width);

	anOstream << this->qxx.element(0,0);  anOstream.width(width);
	anOstream << this->qxx.element(0,1);  anOstream.width(width);
	anOstream << this->qxx.element(0,2);  anOstream.width(width);
	anOstream << this->qxx.element(0,3) << "\n         " << this->qxx.element(1,0);  anOstream.width(width);
	anOstream << this->qxx.element(1,1);  anOstream.width(width);
	anOstream << this->qxx.element(1,2);  anOstream.width(width);
	anOstream << this->qxx.element(1,3) << "\n         "
			  << this->qxx.element(2,0);  anOstream.width(width);
	anOstream << this->qxx.element(2,1);  anOstream.width(width);
	anOstream << this->qxx.element(2,2);  anOstream.width(width);
	anOstream << this->qxx.element(2,3) << "\n         " << this->qxx.element(3,0);  anOstream.width(width);
	anOstream << this->qxx.element(3,1);  anOstream.width(width);
	anOstream << this->qxx.element(3,2);  anOstream.width(width);
	anOstream << this->qxx.element(3,3) << "\n    SQRXY:  "
			  << curSqrXY.getString(" ",15,5) << "\n    SQRZ:  "
			  << curSqrZ.getString(" ",15,5)  << "\n  ENDFACE"<<endl;
};

//============================================================================

void CBuildingFace::print(const char *filename) const
{
	ofstream ofile(filename);
	this->print(ofile);
};

//****************************************************************************
//****************************************************************************
