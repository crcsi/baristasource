/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <strstream>
using namespace std;

#include "BuildingFacePoly.h"
#include "BuildingFace.h"
#include "ProtocolHandler.h"
#include "XYZPolyLine.h"
#include "XYZLines.h"
#include "XYZLinesAdj.h"

//============================================================================

CBuildingFacePoly::CBuildingFacePoly(CBuildingFace *face, const CTFW &Trafo, const C3DPoint &Red) : 
                  pFace(face), pCombinedPolygon(0), trafo(Trafo), reduction (Red)
{
};

//============================================================================

CBuildingFacePoly::CBuildingFacePoly(CBuildingFace *face, const CXYZPolyLine &line, 
                                     const CTFW &Trafo, const C3DPoint &Red): 
                  pFace(face), pCombinedPolygon(0), trafo(Trafo), reduction (Red)
{
	this->pCombinedPolygon = new CXYZPolyLine(line);
};

//============================================================================

CBuildingFacePoly::~CBuildingFacePoly()
{
	this->resetPolygon();
	this->resetNeighbours();
	this->pFace = 0;
};
	  
//============================================================================

int CBuildingFacePoly::getIndexOfNeighbour(CBuildingFaceNeighbourRecord *neighbour) const
{
	for (unsigned int i = 0; i < this->neighbourVector.size(); ++i)
	{
		if (this->neighbourVector[i] == neighbour) return i;
	}

	return -1;
};

//============================================================================

unsigned short CBuildingFacePoly::getFaceIndex() const
{
	return this->pFace->getIndex();
};

//============================================================================

unsigned short CBuildingFacePoly::getNeighbourFaceIndex(unsigned int index) const
{
	CBuildingFaceNeighbourRecord* pNeighbour = this->neighbourVector[index];
	if (this->getFaceIndex() == pNeighbour->getRegion1())  return pNeighbour->getRegion2();

	return pNeighbour->getRegion1();
};

//============================================================================

unsigned short CBuildingFacePoly::getPrevNeighbourFaceIndex(unsigned int index) const
{
	if (!this->neighbourVector.size()) return 0;

	index = (index + this->neighbourVector.size() - 1 ) % this->neighbourVector.size();

	return this->getNeighbourFaceIndex(index);
};

//============================================================================

unsigned short CBuildingFacePoly::getNextNeighbourFaceIndex(unsigned int index) const
{

	if (!this->neighbourVector.size()) return 0;

	index = (index + this->neighbourVector.size() + 1 ) % this->neighbourVector.size();

	return this->getNeighbourFaceIndex(index);
};
                          
//============================================================================

vector<CBuildingFaceNeighbourRecord *> *CBuildingFacePoly::getNeighbours(unsigned short region) const
{
	vector<CBuildingFaceNeighbourRecord *> *pVec = new vector<CBuildingFaceNeighbourRecord *>;

	for (unsigned int u = 0; u < this->neighbourVector.size(); ++u)
	{
		if (this->getNeighbourFaceIndex(u) == region) pVec->push_back(this->neighbourVector[u]);
	};

	if (pVec->size() == 0)
	{
		delete pVec;
		pVec = 0;
	}
	

	return pVec;
};

//============================================================================

CBuildingFacePoly &CBuildingFacePoly::operator = (const CBuildingFacePoly &poly)
{
	if (this == &poly) return *this;

	this->reduction = poly.reduction;
	this->trafo     = poly.trafo;
	this->pFace     = poly.pFace;
	
	this->resetNeighbours();
	this->resetPolygon();

	this->neighbourVector.resize(poly.neighbourVector.size());

	for (unsigned int u = 0; u < this->neighbourVector.size(); ++u)
	{
		this->neighbourVector[u] =  new CBuildingFaceNeighbourRecord(*poly.neighbourVector[u]);
	}

	if (poly.pCombinedPolygon) this->pCombinedPolygon = new CXYZPolyLine(*poly.pCombinedPolygon);

	return *this;
};

//============================================================================

void CBuildingFacePoly::resetNeighbours()
{
	for (unsigned int u = 0; u < this->neighbourVector.size(); ++u)
	{
		if (this->neighbourVector[u]) 
		{
			this->neighbourVector[u] = 0;
		};
	};

	this->neighbourVector.clear();
};

//=============================================================================

void CBuildingFacePoly::resetPolygon()
{
	if (this->pCombinedPolygon) delete this->pCombinedPolygon;
	this->pCombinedPolygon = 0;
};


//============================================================================

void CBuildingFacePoly::printNeighbours(ostream &anOstream) const
{
	for (unsigned int u = 0; u < this->neighbourVector.size(); ++u)
	{
		this->neighbourVector[u]->print(anOstream);
	};

	anOstream << endl;
};

//============================================================================

void CBuildingFacePoly::printNeighbours(const char *filename) const
{
	ofstream outFile(filename);
	if (!(!outFile))
	{
		this->printNeighbours(outFile);
	};
};

//============================================================================

void CBuildingFacePoly::appendNeighbour (CBuildingFaceNeighbourRecord *neighbour)
{
	this->neighbourVector.push_back(neighbour);
};

//============================================================================

void CBuildingFacePoly::insertNeighbourAt(unsigned int index, CBuildingFaceNeighbourRecord *neighbour)
{
	this->neighbourVector.push_back(neighbour);

	for (unsigned int i = index + 1; i < this->neighbourVector.size(); ++i) 
	{
		this->neighbourVector[i] = this->neighbourVector[i - 1];
	}

	this->neighbourVector[index] = neighbour;
};


//============================================================================

void CBuildingFacePoly::insertNeighbourAfter(unsigned int index, CBuildingFaceNeighbourRecord *neighbour)
{
	return this->insertNeighbourAt(index + 1, neighbour);
};
	  
//============================================================================

void CBuildingFacePoly::replaceNeighbour(CBuildingFaceNeighbourRecord *neighbour, 
										 vector<CBuildingFaceNeighbourRecord *> &newNeighbours)
{
	int indexOfNeighbour = this->getIndexOfNeighbour(neighbour);
	if (indexOfNeighbour >= 0)
	{
		unsigned int oldSize = this->neighbourVector.size();
		CBuildingFaceNeighbourRecord *nextRec = this->neighbourVector[(indexOfNeighbour + 1 )% this->neighbourVector.size()];
		CBuildingFaceNeighbourRecord *prevRec = this->neighbourVector[(indexOfNeighbour + this->neighbourVector.size() - 1 ) % this->neighbourVector.size()];

		if (newNeighbours.size() > 1)
		{
			this->neighbourVector.resize(this->neighbourVector.size() + newNeighbours.size() - 1);
			for (int i = oldSize - 1; i > indexOfNeighbour; --i)
			{
				this->neighbourVector[i + newNeighbours.size() - 1] = this->neighbourVector[i];
			};
		};
		
		bool reverse = false;

		if (oldSize > 1)
		{
			CXYZPoint pE = neighbour->getFinalEndPt();
			CXYZPoint pS = neighbour->getFinalStartPt();
	
			double d1 = prevRec->getFinalStartPt().distanceFrom(pS);
			double d2 = prevRec->getFinalEndPt().distanceFrom(pS);
			double dsP = (d1 < d2) ? d1 : d2;

			d1 = nextRec->getFinalStartPt().distanceFrom(pS);
			d2 = nextRec->getFinalEndPt().distanceFrom(pS);
			double dsN = (d1 < d2) ? d1 : d2;

			if (dsP > dsN) reverse = true;		
		}

		if (reverse)
		{
			for (unsigned int i = 0; i < newNeighbours.size(); ++i)
			{
				this->neighbourVector[i + indexOfNeighbour] = newNeighbours[newNeighbours.size() - 1 - i];
			};
		}
		else
		{
			for (unsigned int i = 0; i < newNeighbours.size(); ++i)
			{
				this->neighbourVector[i + indexOfNeighbour] = newNeighbours[i];
			};
		}
	};
};

//============================================================================

void CBuildingFacePoly::removeNeighbourAt(unsigned int index)
{
	if (this->neighbourVector.size() > 0)
	{
		for (unsigned int i = index + 1; i < this->neighbourVector.size(); ++i) 
		{
			this->neighbourVector[i - 1] = this->neighbourVector[i];
		}

		this->neighbourVector.resize(this->neighbourVector.size() - 1);
	};
};

//============================================================================

void CBuildingFacePoly::exportDXF(ofstream &anOstream, bool exportCombined, 
								  bool exportInitial, bool exportApprox, 
								  bool exportFinal) const
{
	for (unsigned int u = 0; u < this->neighbourVector.size(); ++u)
	{
		this->neighbourVector[u]->exportDXF(anOstream, exportInitial, exportApprox, exportFinal);
		if (exportCombined && this->pCombinedPolygon) 
		{
			ostrstream oss;
			oss << "combined_" << this->pFace->getIndex() << ends;
			char *layer = oss.str();
			this->pCombinedPolygon->writeDXF(anOstream, layer, 1, 2);
			delete[] layer;
		}
	};
};

//============================================================================

void CBuildingFacePoly::printNeighbours(const CCharString &heading)
{
	ostrstream oss;
	oss << heading.GetChar() << "\nList of neighbours to face ";
	oss.width(3);
	oss << this->getFaceIndex() << ":\n+++++++++++++++++++++++++++++++\n";

	for (unsigned int u = 0; u < this->neighbourVector.size(); ++u)
	{
		oss << this->neighbourVector[u]->getPrintString().GetChar() << "\n";
	};

	oss << "\n" << ends;
	protHandler.print(oss, protHandler.prt);
};

//============================================================================

void CBuildingFacePoly::copyPoly(const CXYZPolyLine* pLine)
{
	this->resetPolygon();
	this->pCombinedPolygon = new CXYZPolyLine (*pLine);
};

//============================================================================

void CBuildingFacePoly::combineNeighbours()
{
	if (this->neighbourVector.size() > 0)
	{
		if (this->neighbourVector[0]->getFinalPoly())
		{
			this->copyPoly(this->neighbourVector[0]->getFinalPoly());

			for (unsigned int u = 1; u < this->neighbourVector.size(); ++u) 
			{
				CXYZPolyLine *pPoly = this->neighbourVector[u]->getFinalPoly();
				double d00 = pPoly->getPoint(0)->distanceFrom(*(this->pCombinedPolygon->getPoint(0)));
				double dM0 = pPoly->getPoint(pPoly->getPointCount() - 1)->distanceFrom(*(this->pCombinedPolygon->getPoint(0)));
				double d0N = pPoly->getPoint(0)->distanceFrom(*(this->pCombinedPolygon->getPoint(this->pCombinedPolygon->getPointCount() - 1)));
				double dMN = pPoly->getPoint(pPoly->getPointCount() - 1)->distanceFrom(*(this->pCombinedPolygon->getPoint(this->pCombinedPolygon->getPointCount() - 1)));

				if ((d00 < dM0) && (d00 < d0N) && (d00 < dMN))
				{
					this->pCombinedPolygon->reversePolyLine();
					this->pCombinedPolygon->append(*pPoly);
				}
				else if ((dM0 < d0N) && (dM0 < dMN))
				{
					CXYZPolyLine p(*this->pCombinedPolygon);
					this->copyPoly(pPoly);
					this->pCombinedPolygon->append(p);
				}
				else if (d0N < dMN)
				{
					this->pCombinedPolygon->append(*pPoly);
				}
				else
				{
					CXYZPolyLine p(*pPoly);
					p.reversePolyLine();
					this->pCombinedPolygon->append(p);
				}	
			}
		}

		if (!this->pCombinedPolygon->isClosed()) this->pCombinedPolygon->closeLine();
	};
};

//============================================================================

void CBuildingFacePoly::combineNeighbours(float intersectionThreshold, 
                                          float distanceThreshold, 
										  float alphaThreshold,
										  float selfIntersectionThreshold)
{
	ostrstream oss;
	oss << this->getFaceIndex() << ends;
	char *z = oss.str();
	CCharString f(z);
	delete [] z;

	if (this->neighbourVector[0]->getFinalPoly())
	{
		if (this->neighbourVector.size() < 2) 
		{
			if (this->neighbourVector.size() == 1)
			{
				this->copyPoly(this->neighbourVector[0]->getFinalPoly());
			}
		}
		else
		{ 
			for (unsigned int u = 0; u < this->neighbourVector.size(); ++u)
			{
				bool usePoly = true;
				CBuildingFaceNeighbourRecord *pPrevRec = 0;

				if (u == 0) pPrevRec = this->neighbourVector[this->neighbourVector.size() - 1];
				else pPrevRec = this->neighbourVector[u -1 ];

				if (pPrevRec)
				{
					if (intersection(pPrevRec, this->neighbourVector[u], intersectionThreshold))
					{
						this->pCombinedPolygon->removePoint(this->pCombinedPolygon->getPointCount() - 1);
						this->pCombinedPolygon->addPoint(pPrevRec->getFinalEndPt());	     
					}
					else
					{
						CXYZSegment segment1(pPrevRec->getFinalStartPt(), pPrevRec->getFinalEndPt());          
						CXYZSegment segment2(this->neighbourVector[u]->getFinalStartPt(), this->neighbourVector[u]->getFinalEndPt());
						float inprod = (float) segment1.getDirection().innerProduct(segment2.getDirection());
						if (inprod > alphaThreshold)
						{
							C3DPoint basePoint1 = segment1.getBasePoint(segment2.getPoint1());
							C3DPoint basePoint2 = segment1.getBasePoint(segment2.getPoint2());
							C3DPoint diff1 = basePoint1 - segment2.getPoint1();
							C3DPoint diff2 = basePoint1 - segment2.getPoint2();
							if ((diff1.getNorm() < distanceThreshold) || (diff2.getNorm() < distanceThreshold))
								usePoly = false;
						};         
					};
				};

				if (u == 0) this->copyPoly(this->neighbourVector[u]->getFinalPoly());
				else if (usePoly) this->pCombinedPolygon->append(*this->neighbourVector[u]->getFinalPoly());
			};
		};

		if (!this->pCombinedPolygon->isClosed()) this->pCombinedPolygon->closeLine();

		double pixSize = this->trafo.getScaleX();
		float approxThreshold = 0.01f * float(pixSize);

//		this->pCombinedPolygon->approximate(approxThreshold);

		float lengthThr = (float) pixSize;

		this->removeSelfIntersections(lengthThr, selfIntersectionThreshold);
//		this->pCombinedPolygon->approximate(approxThreshold);

		unsigned int numberOfPoints = this->pCombinedPolygon->getPointCount();
		if (numberOfPoints > 1)
		{ 
			unsigned int i1 = numberOfPoints - 1, i2 = numberOfPoints - 2;
			if ((*this->pCombinedPolygon)[i1].distanceFrom((*this->pCombinedPolygon)[i2]) < 0.001f)
			{
				this->pCombinedPolygon->removePoints(i2, i1);
			};
		};

		if (!this->pCombinedPolygon->isClosed()) this->pCombinedPolygon->closeLine();
	}
};
	  
//============================================================================

bool CBuildingFacePoly::intersection(CBuildingFaceNeighbourRecord *pNeighbour1,
									 CBuildingFaceNeighbourRecord *pNeighbour2,
									 float distThreshold)
{
	bool retVal = false;

	CXYZPoint endPoint1   = pNeighbour1->getFinalEndPt();
	CXYZPoint startPoint2 = pNeighbour2->getFinalStartPt();

	if ((pNeighbour1->getFinalNumber() == 2) &&  (pNeighbour2->getFinalNumber() == 2))
	{
		CXYZLine line1(pNeighbour1->getFinalStartPt(), endPoint1);
		CXYZLine line2(startPoint2, pNeighbour2->getFinalEndPt());

		C3DPoint Pinter;
		line1.setEpsilon(trafo.getScaleX());
		eIntersect eInters = line1.getIntersection(line2, Pinter);
		CXYZPoint pIntersection(Pinter);

		if (eInters == eIntersection)
		{
			if ((endPoint1.distanceFrom(&pIntersection)   < distThreshold) ||		
				(startPoint2.distanceFrom(&pIntersection) < distThreshold))
			{
				CXYZPoint *p = pNeighbour1->getFinalPoly()->getPoint(pNeighbour1->getFinalNumber() - 1);
				p->x = pIntersection.x;  p->y = pIntersection.y;  p->z = pIntersection.z;  
				CXYZPoint *p2 = pNeighbour2->getFinalPoly()->getPoint(pNeighbour2->getFinalNumber() - 1);
				p = pNeighbour2->getFinalPoly()->getPoint(0);
				p2->x = p->x;  p2->y = p->y;  p2->z = p->z;  
				p->x = pIntersection.x;  p->y = pIntersection.y;  p->z = pIntersection.z;  
				retVal = true;
			};
		}
		else if (eInters == eIncluded)
		{
			CXYZHomoSegment segment1(pNeighbour1->getFinalStartPt(),endPoint1);
			CXYZHomoSegment segment2(startPoint2,pNeighbour2->getFinalEndPt());

			segment1.mergeWithSegment(segment2);

			if (segment1.getLength() > segment2.getLength())
			{
				pNeighbour2->getFinalPoly()->removePoints(0, pNeighbour2->getFinalNumber() - 1);
				CXYZPoint *p = pNeighbour1->getFinalPoly()->getPoint(0);
				*p = segment1.getPoint1();
				p = pNeighbour1->getFinalPoly()->getPoint(pNeighbour1->getFinalNumber() - 1);
				*p = segment1.getPoint2();
			}
			else
			{
				pNeighbour1->getFinalPoly()->removePoints(0, pNeighbour1->getFinalNumber() - 1);
				CXYZPoint *p = pNeighbour2->getFinalPoly()->getPoint(0);
				*p = segment1.getPoint1();
				p = pNeighbour2->getFinalPoly()->getPoint(pNeighbour2->getFinalNumber() - 1);
				*p = segment1.getPoint2();
			};
		};
	}
	else if ((pNeighbour1->getFinalNumber() > 1) && (pNeighbour2->getFinalNumber() > 1))
	{
		CXYZLine line1((*pNeighbour1->getFinalPoly())[pNeighbour1->getFinalNumber() - 2], endPoint1);
		CXYZLine line2(startPoint2, (*pNeighbour2->getFinalPoly())[1]);

		line1.setEpsilon(trafo.getScaleX());
		C3DPoint  Pinter;
		if (line1.getIntersection(line2, Pinter) == eIntersection)
		{
			CXYZPoint pIntersection(Pinter);
			C3DPoint diff1(endPoint1.x   - Pinter.x, endPoint1.y   - Pinter.y, endPoint1.z   - Pinter.z);
			C3DPoint diff2(startPoint2.x - Pinter.x, startPoint2.y - Pinter.y, startPoint2.z - Pinter.z);
			float area = fabs(diff1.crossProduct(diff2).getNorm());
			float sqrThr = distThreshold * distThreshold;

			if ((diff1.getSquareNorm() < sqrThr) || (diff2.getSquareNorm() < sqrThr) || (area < 3.0f * sqrThr))
			{
				CXYZPoint *p = pNeighbour1->getFinalPoly()->getPoint(pNeighbour1->getFinalNumber() - 1);
				p->x = pIntersection.x;  p->y = pIntersection.y;  p->z = pIntersection.z;  
				p = pNeighbour2->getFinalPoly()->getPoint(0);
				p->x = pIntersection.x;  p->y = pIntersection.y;  p->z = pIntersection.z;  

				retVal = true;
			};
		};   
	};

	return retVal;
};

//============================================================================

void CBuildingFacePoly::removeSelfIntersections(float lengthThreshold, float areaThreshold)
{
	unsigned int maxPts  = this->pCombinedPolygon->getPointCount();
	if (maxPts > 3)
	{
		areaThreshold *= areaThreshold;

		for (int u11 = 0; u11 < maxPts; ++u11)
		{
			unsigned int u12 = (u11 + maxPts + 1) % maxPts;
			unsigned int u21 = (u11 + 2) % maxPts;
			unsigned int u22 = (u21 + maxPts + 1) % maxPts;

			CXYZSegment segment1((*this->pCombinedPolygon)[u11], (*this->pCombinedPolygon)[u12]);
			CXYZSegment segment2((*this->pCombinedPolygon)[u21], (*this->pCombinedPolygon)[u22]);

			C3DPoint Pinters;
			if ((segment2.getLength() < lengthThreshold) && !((u21 == maxPts - 1) && (u22 == 0)))
			{
				unsigned int remInd = u22;
				if (u22 == maxPts - 1) remInd = u21;
				this->pCombinedPolygon->removePoint(remInd);
				maxPts--;
				u11--;
			}
			else if (segment1.getIntersection(segment2, Pinters) == eIntersection)
			{
				CXYZPoint pIntersection(Pinters);
				CXYZPoint pos1  = (*this->pCombinedPolygon->getPoint(u12)) - (CObsPoint) pIntersection;
				CXYZPoint pos2  = (*this->pCombinedPolygon->getPoint(u21)) - (CObsPoint) pIntersection;
				float area = fabs(pos1.crossProduct(pos2).getNorm());

				if (area < areaThreshold)
				{
					*this->pCombinedPolygon->getPoint(u12) = pIntersection;
					this->pCombinedPolygon->removePoint(u21);
					maxPts--;
					u11--;
				};
			};
		};

		if (!this->pCombinedPolygon->isClosed()) this->pCombinedPolygon->closeLine();
	};
};

//============================================================================