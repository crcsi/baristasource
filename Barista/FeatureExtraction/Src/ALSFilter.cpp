#include <float.h>
#include <strstream>
using namespace std;
#include "newmatap.h" //for advanced matrix routines (LU;QR,SVD,..)
#include "newmatio.h" //for the output routines
#include "myexcept.h" //for the exceptions in NM

#include "statistics.h"
#include "ALSFilter.h"
#include "ALSData.h"
#include "ALSPoint.h"
#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "DEM.h"
#include "LookupTableHelper.h"
#include "LabelImage.h"
#include "filter.h"
#include "robustWeightFunction.h"
#include "histogramFlt.h"
#include "3DadjPlane.h"

#include "GenericPointCloud.h"

//****************************************************************************
//****************************************************************************

CALSFilter::CALSFilter (CALSDataSet *ALS, CDEM *FilteredDTMFile, 
		                float MinBuildHeight,
				        float HeightThreshold, double SigmaALS,
						float FilterSize, float FilterSizeDetails, 
						float HeightThresholdFilter, double Alpha,
						double MaxObliqueness,  int NumberIterations, 
						int NpointsForInterpolation,
						const C2DPoint &Min, const C2DPoint &Max, 						
						const C2DPoint &Resolution, const CCharString &path):
         pALS(ALS), pFilteredDTMFile(FilteredDTMFile), pApproxDTM(0), 
		 pDSM(0), pDTM(0), pFilteredDSM(0),
		 neighbourHeightThreshold(HeightThreshold), morphFilterSize(FilterSize),
		 pMin(Min), pMax(Max), resolution(Resolution), sigmaALS(SigmaALS),
		 alpha(Alpha), maxObliqueness(MaxObliqueness), numberIterations(NumberIterations),
		 heightThresholdFilter(HeightThresholdFilter), pNoDtmMask(0),
		 morphFilterSizeForDetails(FilterSizeDetails), pointsForInterpolation(NpointsForInterpolation),
		 minBuildHeight(MinBuildHeight)

{
	if (this->pALS && this->pFilteredDTMFile)
	{
		CFileName fn(this->pALS->getFileNameChar());
		if (path.IsEmpty()) this->projectPath = fn.GetPath();
		else this->projectPath = path;
		if (!this->projectPath.IsEmpty())
		{
			if (this->projectPath[this->projectPath.GetLength() - 1] != CSystemUtilities::dirDelim)
				this->projectPath += CSystemUtilities::dirDelimStr;
		}
		
		if (!this->initWindows())
		{
			this->clearWindows();

			ostrstream oss; 
			oss << "\nError in ALS Filtering: Cannot initialise image data buffers!" << ends;
			protHandler.print(oss, protHandler.prt);
		}
	}
};

//=============================================================================

CALSFilter::~CALSFilter ()
{
	this->clearWindows();

	this->pALS             = 0;
	this->pFilteredDTMFile = 0;

	protHandler.close(protHandler.prt);
};
 
//=============================================================================
  
void CALSFilter::clearWindows()
{
	if (this->pFilteredDSM) delete this->pFilteredDSM; 
	if (this->pApproxDTM)       delete this->pApproxDTM; 
	if (this->pDSM)             delete this->pDSM; 
	if (this->pDTM)     delete this->pDTM;
	if (this->pNoDtmMask)       delete this->pNoDtmMask;

	this->pFilteredDSM = 0;
	this->pApproxDTM       = 0; 
	this->pDSM             = 0;
	this->pDTM     = 0;
	this->pNoDtmMask       = 0;
};

//=============================================================================
  
bool CALSFilter::initWindows()
{
	bool ret = true;
	this->clearWindows();

	int Width  = (int) floor((this->pMax.x - this->pMin.x)  / this->resolution.x + 1.5);
	int Height = (int) floor((this->pMax.y - this->pMin.y)  / this->resolution.x + 1.5);

	if ((Width < 1) || (Height < 1) || (resolution.x < FLT_EPSILON)) ret = false;
	else
	{
		C2DPoint luc(this->pMin.x, this->pMax.y);
		this->pFilteredDSM = new CImageBuffer(0,0,Width, Height, 1, 32, false); 
		this->pApproxDTM       = new CImageBuffer(0,0,Width, Height, 1, 32, false); 
		this->pDSM             = new CImageBuffer(0,0,Width, Height, 1, 32, false); 
		this->pDTM     = new CImageBuffer(0,0,Width, Height, 1, 32, false); 
		this->pNoDtmMask       = new CImageBuffer(0,0,Width, Height, 1,  1, true); 
		

		if (this->pApproxDTM && this->pDSM && this->pDTM &&  this->pFilteredDSM && this->pNoDtmMask)
		{
			C2DPoint shift(this->pMin.x, this->pMax.y);
			this->trafo.setFromShiftAndPixelSize(shift, this->resolution);
			this->trafo.setRefSys(pFilteredDTMFile->getTFW()->getRefSys());
		}
		else this->clearWindows();
	}
	return ret;
};
  
//=============================================================================

void CALSFilter::fillHoles(bool all)
{

	if (this->listener) 
	{
		this->listener->setTitleString("Detecting no data areas ...");
		this->listener->setProgressValue(1);
	}

	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss << "\nDetecting data voids in DSM\n===========================\n\n Time: " << CSystemUtilities::timeString().GetChar() <<"\n";

	float zf;

	if (!all)
	{
		float z;
		for (unsigned long u = 0; u < this->pNoDtmMask->getNGV(); ++u)
		{
			z = this->pDSM->pixFlt(u);
			if (z <= -9999.0) this->pNoDtmMask->pixUChar(u) = 0;
			else
			{
				zf = this->pDTM->pixFlt(u);
				if (zf <= -9999.0) this->pNoDtmMask->pixUChar(u) = 1;
				else this->pNoDtmMask->pixUChar(u) = 0;
			}
		}
	}
	else
	{
		for (unsigned long u = 0; u < this->pNoDtmMask->getNGV(); ++u)
		{
			zf = this->pDTM->pixFlt(u);
			if (zf <= -9999.0) this->pNoDtmMask->pixUChar(u) = 1;
			else this->pNoDtmMask->pixUChar(u) = 0;
		}
	}

	if (this->listener) 
	{
		this->listener->setProgressValue(16.6f);
	}

	CLabelImage labels(*(this->pNoDtmMask), 1, 4);
	if (this->listener) 
	{
		this->listener->setProgressValue(33.3f);
	}

	float sumNeighbourHeights;
	unsigned short nN;
	short l;
	float z;

	for (unsigned long u = 0; u < labels.getNGV(); u++)
	{
		l = labels.getLabel(u);
		if (labels.getNPixels(l) == 1)
		{
			nN = 0;
			unsigned long udelta = u + labels.getDiffX();
			z = this->pDTM->pixFlt(udelta);
			if (z > -9999.0)
			{
				sumNeighbourHeights = z;
				++nN;
			}
			udelta += labels.getDiffY();
			z = this->pDTM->pixFlt(udelta);
			if (z > -9999.0)
			{
				sumNeighbourHeights += z;
				++nN;
			}
			udelta -= labels.getDiffX();
			z = this->pDTM->pixFlt(udelta);
			if (z > -9999.0)
			{
				sumNeighbourHeights += z;
				++nN;
			}
			udelta -= labels.getDiffX();
			z = this->pDTM->pixFlt(udelta);
			if (z > -9999.0)
			{
				sumNeighbourHeights += z;
				++nN;
			}
			udelta -= labels.getDiffY();
			z = this->pDTM->pixFlt(udelta);
			if (z > -9999.0)
			{
				sumNeighbourHeights += z;
				++nN;
			}
			udelta -= labels.getDiffY();
			z = this->pDTM->pixFlt(udelta);
			if (z > -9999.0)
			{
				sumNeighbourHeights += z;
				++nN;
			}
			udelta += labels.getDiffX();
			z = this->pDTM->pixFlt(udelta);
			if (z > -9999.0)
			{
				sumNeighbourHeights += z;
				++nN;
			}
			udelta += labels.getDiffX();
			z = this->pDTM->pixFlt(udelta);
			if (z > -9999.0)
			{
				sumNeighbourHeights += z;
				++nN;
			}
			if (nN > 0)
			{
				z = sumNeighbourHeights / (float) nN;
				this->pDTM->pixFlt(u) = z;
				labels.pixUShort(u) = 0;
			}	
		}
	}

	labels.computePixelNumbers();
	labels.renumberLabels();

	if (this->listener) 
	{
		this->listener->setProgressValue(50.0f);
	}

	labels.updateVoronoi();
	if (outputMode > eMEDIUM)
	{
		labels.dump(this->projectPath.GetChar(), "empty","",false, false);
	}

	if (this->listener)  
	{
		this->listener->setProgressValue(66.6f);
	}

	vector<int> rmin(labels.getNLabels() + 1, labels.getHeight() + 1);
	vector<int> rmax(labels.getNLabels() + 1, -1);
	vector<int> cmin(labels.getNLabels() + 1, labels.getWidth() + 1);
	vector<int> cmax(labels.getNLabels() + 1, -1);

	if (this->listener) 
	{
		this->listener->setProgressValue(83.2);
	}

	short d;
	
	for (long r = 0; r < this->pNoDtmMask->getHeight(); ++r)
	{
		unsigned long u = r * this->pNoDtmMask->getDiffY();
		for (long c = 0; c < this->pNoDtmMask->getWidth(); ++c, ++u)
		{
			l = labels.getLabel(u);
			if (!l)
			{
				d = labels.getDistance(u);
				if (d < 10) 
				{
					l = labels.getVoronoi(u);
					labels.pixUShort(u) = l;
					if (rmin[l] > r) rmin[l] = r;
					if (rmax[l] < r) rmax[l] = r;
					if (cmin[l] > c) cmin[l] = c;
					if (cmax[l] < c) cmax[l] = c;
				}
			}
			else
			{
				if (rmin[l] > r) rmin[l] = r;
				if (rmax[l] < r) rmax[l] = r;
				if (cmin[l] > c) cmin[l] = c;
				if (cmax[l] < c) cmax[l] = c;
			}
		}
	}

	if (outputMode > eMEDIUM)
	{
		labels.dump(this->projectPath.GetChar(), "empty_larger");
	}

	oss << "\n    Number of no data areas: " << labels.getNLabels() 
		<< "\n\nFilling data voids in DSM\n=========================\n\n Time: " << CSystemUtilities::timeString().GetChar() <<"\n";
	protHandler.print(oss, protHandler.prt);
	
	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}

	if (this->listener) 
	{
		this->listener->setTitleString("Filling no data areas ...");
		this->listener->setProgressValue(1);
	}

	double dWeight = this->pALS->getPointDist().x > this->pALS->getPointDist().y ?
		this->pALS->getPointDist().x : this->pALS->getPointDist().y;

	dWeight *= dWeight;

	float sumPix = 0;

	for (unsigned int reg = 1; reg < labels.getNLabels(); ++reg)
	{
		sumPix += labels.getNPixels(reg);
	}

	float oneBySumpix = (fabs(sumPix) > FLT_EPSILON) ? 100.0f / sumPix : 1.0f;
	sumPix = 0;

	for (unsigned int reg = 1; reg < labels.getNLabels(); ++reg)
	{
		sumPix += labels.getNPixels(reg);

		vector<float> rVec(labels.getNPixels(reg)), cVec(labels.getNPixels(reg)), zVec(labels.getNPixels(reg));
		unsigned long nHeights = 0;
		
		long uMin    = labels.getIndex(rmin[reg], cmin[reg]);
		long uMaxRow = labels.getIndex(rmin[reg], cmax[reg]) + 1; 
		long uMax    = labels.getIndex(rmax[reg], cmax[reg]) + 1; 
		for (int r = rmin[reg]; uMin < uMax; uMin += labels.getDiffY(), uMaxRow += labels.getDiffY(), ++r)
		{
			for (long u = uMin, c = cmin[reg]; u < uMaxRow; ++u, ++c)
			{
				if (labels.getLabel(u) == reg)
				{
					z = this->pDTM->pixFlt(u);
					if (z > -9999.0)
					{
						if (nHeights >= rVec.size())
						{
							rVec.resize(rVec.size() * 2);
							cVec.resize(rVec.size() * 2);
							zVec.resize(rVec.size() * 2);
						}
						rVec[nHeights] = float(r);
						cVec[nHeights] = float(c);
						zVec[nHeights] = float(z);
						++nHeights;
					}
				}
			}
		}

		CGenericPointCloud ptcCld(nHeights, 3);
		C3DPoint p;
		for (unsigned int i = 0; i < nHeights; ++i)
		{
			p.x = double(cVec[i]);
			p.y = double(rVec[i]);
			p.z = double(zVec[i]);
			ptcCld.addPoint(p);
		}

		ptcCld.init(); 
		ANNpoint queryPt = annAllocPt(3);

		uMin    = labels.getIndex(rmin[reg], cmin[reg]);
		uMaxRow = labels.getIndex(rmin[reg], cmax[reg]) + 1; 
		
		unsigned long nPixels = 0;

		for (int r = rmin[reg]; uMin < uMax; uMin += labels.getDiffY(), uMaxRow += labels.getDiffY(), ++r)
		{
			for (long u = uMin, c = cmin[reg]; u < uMaxRow; ++u, ++c)
			{
				if ((this->pDTM->pixFlt(u) <= -9999.0) && (labels.getLabel(u) == reg))
				{
					queryPt[0] = c;
					queryPt[1] = r;
					double Z = ptcCld.interpolateMovingHorizontalPlane(queryPt, 400, 2, 160000, dWeight);
					this->pDTM->pixFlt(u) = float(Z);
					nPixels++;
				}
			}
		}

		if (outputMode > eMEDIUM)
		{
			ostrstream oss1;
			oss1.setf(ios::fixed, ios::floatfield);
			oss1 << "    Label ";
			oss1.width(5);
			oss1 << reg << ": ";
			oss1.width(7);
			oss1 << nPixels << " heights interpolated from ";
			oss1.width(7);
			oss1 << nHeights << " points";
			protHandler.print(oss1, protHandler.prt);
		}

		if (this->listener) 
		{
			float prog = sumPix * oneBySumpix;
			this->listener->setProgressValue(prog);
		}
		annDeallocPt(queryPt);
	}

	ostrstream oss1;
	oss1.setf(ios::fixed, ios::floatfield);
	oss << "\n    Data voids filled; Time: " << CSystemUtilities::timeString().GetChar() <<"\n";

	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}

};
	  
//=============================================================================

bool CALSFilter::initPointCloud(CGenericPointCloud &ptCld, int tileIndex, 		               
								C3DPoint Min, C3DPoint Max)
{
	bool ret = true;
	ostrstream oss;
	oss << tileIndex << ends;
	char *z = oss.str();
	CFileName fn = this->projectPath + "SCR" + CSystemUtilities::dirDelimStr + z + CCharString(".scr");

	delete [] z;
	this->tileScratchFiles.push_back(fn);
	C2DPoint m(Min.x, Min.y);
	tilePmin.push_back(m);
	m.x = Max.x; m.y = Max.y;
	tilePmax.push_back(m);

	if (tileIndex == 0)
	{
		CCharString path = fn.GetPath();
		CSystemUtilities::createDirectoryTree(path + CSystemUtilities::dirDelimStr);
	}

	CALSPoint **pointvec = 0;
	int PtCldCapacity = 0;

	C2DPoint delta = 10.0 * this->pALS->getPointDist();
	Min.x -= delta.x;
	Min.y -= delta.y;
	Max.x += delta.x;
	Max.y += delta.y;

	int nPts = this->pALS->getPointCloud(Min, Max, pointvec, PtCldCapacity, 131072, 0);

	if (nPts > 0)
	{
		ptCld.prepareCapacity(nPts);

		C3DPoint Pobj, Ppix;
		CALSPoint *p;
		ANNpoint pA;
		for (int i = 0; i < nPts; ++i)
		{
			p = pointvec[i];
			Pobj = p->getLastEcho();
			this->trafo.transformObj2Obs(Ppix, Pobj);
			Ppix.x = floor(Ppix.x + 0.5);
			Ppix.y = floor(Ppix.y + 0.5);

			if ((Ppix.x >= 0) && (Ppix.y >= 0) && 
				(Ppix.x < this->pApproxDTM->getWidth()) && 
				(Ppix.y < this->pApproxDTM->getHeight()))
			{
				long idx = this->pApproxDTM->getIndex(long(Ppix.y), long (Ppix.x));
				pA = ptCld.addPoint(Pobj);
				pA[3] = double(idx);
			}
		}
	}
	ret = ptCld.dump(this->tileScratchFiles[tileIndex].GetChar());
	//else ret = false;

	for (int i = 0; i < PtCldCapacity; ++i)
	{
		delete pointvec[i];
		pointvec[i] = 0;
	}
	delete [] pointvec;

	ptCld.init(); 


	return ret;
};

//=============================================================================

void CALSFilter::deletePointClouds()
{
	CCharString path;
	if (this->tileScratchFiles.size() > 0) 
	{
		path = this->tileScratchFiles[0].GetPath();

		for (unsigned int i = 0; i < this->tileScratchFiles.size(); ++i)
		{
			CSystemUtilities::del(this->tileScratchFiles[i]);
		}

		CSystemUtilities::rmDir(path);
	}
};

//=============================================================================

void CALSFilter::filter(const C3DPoint &Min, const C3DPoint &Max, int &nGrd, 
						int &nOff, float &zmin, float &zmax, int tileIndex, 
						bool filterRobust)
{
	nGrd = nOff = 0;

	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	if (outputMode > eMEDIUM)
	{
		oss << "    Filtering Tile: ";
		oss.width(10);
		oss << Min.x << " / ";
		oss.width(10);
		oss << Min.y << ", ";
		oss.width(10);
		oss <<  Max.x << " / ";
		oss.width(10);
		oss << Max.y;
	}

	CGenericPointCloud allPoints(0, 4);

	C3DPoint MinExt;
	C3DPoint MaxExt;
	MinExt.z = Min.z;
	MinExt.x = Min.x - 10;
	MinExt.y = Min.y - 10;
	MaxExt.z = Max.z;
	MaxExt.x = Max.x + 10;
	MaxExt.y = Max.y + 10;

	if (int(this->tileScratchFiles.size()) > tileIndex) allPoints.init(this->tileScratchFiles[tileIndex].GetChar());
	else initPointCloud(allPoints, tileIndex, MinExt, MaxExt);

	CGenericPointCloud terrainPtCld(allPoints.getNpoints(), 4);

	if (allPoints.getNpoints() > 0)
	{
		C3DPoint Pobj;
		ANNpoint p, pt;
		long idx;

		for (int i = 0; i < allPoints.getNpoints(); ++i)
		{ 
			p = allPoints.getPt(i);
			Pobj.x = p[0]; Pobj.y = p[1]; Pobj.z = p[2];
			idx = long(p[3]);
			float zP = float(Pobj.z);
			float dzDTM = zP - this->pApproxDTM->pixFlt(idx);
			float dzDTM1 = (this->pFilteredDSM != 0) ? (zP - this->pFilteredDSM->pixFlt(idx)) : dzDTM;
			float zDSM = this->pDSM->pixFlt(idx);
			if ((dzDTM < this->minBuildHeight) && (zP - zDSM < this->neighbourHeightThreshold) && (dzDTM1 < this->neighbourHeightThreshold))
			{
				pt = terrainPtCld.addPoint(Pobj);
				pt[3] = p[3];
				++nGrd;
			}
			else
			{
				++nOff;
			}
		}
	}

	terrainPtCld.init(); 
	allPoints.clear();

	int width  = int (floor((Max.x - Min.x) / this->resolution.x) + 1);
	int height = int (floor((Max.y - Min.y) / this->resolution.x) + 1);

	double distMax = this->pALS->getPointDist().x > this->pALS->getPointDist().y ?
		this->pALS->getPointDist().x : this->pALS->getPointDist().y;

	double dWeight = distMax * distMax;

	double distMaxMin = distMax * 2.0;
	distMax *= 10.0;

	CRobustWeightFunction positiveBranch, negativeBranch;
	CRobustWeightFunction *pPositiveBranch = 0, *pNegativeBranch = 0;
	/*
	if (filterRobust)
	{
		positiveBranch.setParameters(this->robustHalfweight, this->heightThreshold);
		negativeBranch.setParameters(3.0 * this->robustHalfweight, 3.0 * this->heightThreshold);
		pPositiveBranch = &positiveBranch;
		pNegativeBranch = &negativeBranch;
	}
*/
	C2DPoint Pobj(Min.x, Max.y), Ppix;
	this->trafo.transformObj2Obs(Ppix, Pobj);
	Ppix.x = floor(Ppix.x + 0.5);
	Ppix.y = floor(Ppix.y + 0.5);
	long index0 = this->pApproxDTM->getIndex(long(Ppix.y), long(Ppix.x));

	float *bufInter = this->pDTM->getPixF();
	float *bufOld = this->pApproxDTM->getPixF();

	terrainPtCld.interpolateRasterForALS(bufInter, bufOld, width, height, this->pApproxDTM->getDiffY(),
							             index0, Min.x, Max.y, this->resolution.x, this->resolution.y, 
										 Ppix.x, Ppix.y, this->pointsForInterpolation, zmin, zmax,
										 distMax, distMaxMin, dWeight, pPositiveBranch, pNegativeBranch);

	if (outputMode > eMEDIUM)
	{
		oss << ", Terrain: ";
		oss.width(10);
		oss << nGrd;
		oss << ", Off-Terrain: ";
		oss.width(10);
		oss << nOff;
		protHandler.print(oss, protHandler.prt);
	}
};
	  
//=============================================================================

void CALSFilter::filterDSM(const C3DPoint &Min, const C3DPoint &Max, 
						   float &zmin, float &zmax, int tileIndex)
{
	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	if (outputMode >= eMEDIUM)
	{
		oss << "    Filtering Tile: ";
		oss.width(10);
		oss << Min.x << " / ";
		oss.width(10);
		oss << Min.y << ", ";
		oss.width(10);
		oss <<  Max.x << " / ";
		oss.width(10);
		oss << Max.y;
	}

	CGenericPointCloud allPoints(0, 5, 3);

	C3DPoint MinExt;
	C3DPoint MaxExt;
	MinExt.z = Min.z;
	MinExt.x = Min.x - 10;
	MinExt.y = Min.y - 10;
	MaxExt.z = Max.z;
	MaxExt.x = Max.x + 10;
	MaxExt.y = Max.y + 10;

	if (int(this->tileScratchFiles.size()) > tileIndex) allPoints.init(this->tileScratchFiles[tileIndex].GetChar());
	else initPointCloud(allPoints, tileIndex, MinExt, MaxExt);

	allPoints.setAnnDim(2);
	
	if (allPoints.getNpoints() == 0)
	{
		ostrstream oss3; oss3.setf(ios::fixed, ios::floatfield);
		oss3.precision(3);
		oss3 << "\n    Tile " << tileIndex + 1 << " does not contain points\n" << ends;
		protHandler.print(oss3, protHandler.prt);
	}
	else
	{
		if (outputMode > eMEDIUM)
		{

			ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
			oss1.precision(3);
			oss1 << "\nExport of pointclouds for single tiles\n" << ends;
			protHandler.print(oss1, protHandler.prt);
			expTilePointCloud(allPoints, tileIndex);
		}

		int width  = int (floor((Max.x - Min.x) / this->resolution.x) + 1);
		int height = int (floor((Max.y - Min.y) / this->resolution.x) + 1);

		double distMax = this->pALS->getPointDist().x > this->pALS->getPointDist().y ?
			this->pALS->getPointDist().x : this->pALS->getPointDist().y;

		double dWeight = distMax * distMax;

		double distMaxMin = distMax * 2.0;
		distMax *= 30.0;

		C2DPoint Pobj(Min.x, Max.y), Ppix;
		this->trafo.transformObj2Obs(Ppix, Pobj);
		Ppix.x = floor(Ppix.x + 0.5);
		Ppix.y = floor(Ppix.y + 0.5);
		long index0 = this->pApproxDTM->getIndex(long(Ppix.y), long(Ppix.x));

		if (outputMode > eMEDIUM)
		{
			ostrstream oss2; oss2.setf(ios::fixed, ios::floatfield);
			oss2.precision(3);
			oss2 << "\nParameters for DSM-Interpolation:"
				 << "\n=========================================\n"
				 << "\nHeight of DSM: " << height
				 << "\nWidth of DSM: " << width
				 << "\nMaximum of distance: " << distMax
				 << "\nDistance between max and min : " << distMaxMin
				 << "\nFirst index of DSM: " << index0 << "\n" << ends;
			protHandler.print(oss2, protHandler.prt);
		}

		float *bufInter = this->pDSM->getPixF();

		allPoints.interpolateRasterForALS(bufInter, 0, width, height, this->pApproxDTM->getDiffY(),
										  index0, Min.x, Max.y, this->resolution.x, this->resolution.y, 
										  Ppix.x, Ppix.y, this->pointsForInterpolation, zmin, zmax,									
										  distMax, distMax, dWeight, 0, 0, this->pNoDtmMask->getPixUC());
	}
};

//=============================================================================

void CALSFilter::initTileStructure(double size, C2DPoint &lucT, C2DPoint &deltaT, 
								   int &nx, int &ny,  int &nTotal)
{
	nx = int(floor((this->pMax.x - this->pMin.x) / size)) + 1;
	ny = int(floor((this->pMax.y - this->pMin.y) / size)) + 1;


	deltaT.x = (this->pMax.x - this->pMin.x) / double (nx);
	deltaT.y = (this->pMax.y - this->pMin.y) / double (ny);

	deltaT.x = this->resolution.x * (1.0 + floor(deltaT.x / this->resolution.x));
	deltaT.y = this->resolution.y * (1.0 + floor(deltaT.y / this->resolution.y));

	nx = int(floor((this->pMax.x - this->pMin.x) / deltaT.x));
	ny = int(floor((this->pMax.y - this->pMin.y) / deltaT.y)) ;

	lucT.x = this->pMin.x;
	lucT.y = this->pMax.y;

	nTotal = (nx + 1) * (ny + 1);
};

//=============================================================================

void CALSFilter::initDSMAndTileStructure()
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(2);

	CCharString st = "\nInitialising the digital surface model:\n";
	int sl = st.GetLength();
	for (int i = 0; i < sl; ++i) st += "=";
	oss << st.GetChar() << "\n\n   Time: "
		<< CSystemUtilities::timeString().GetChar() << "\n" << ends;
	protHandler.print(oss, protHandler.prt);

	if (this->listener) 
	{
		this->listener->setTitleString("Generating DSM ...");
		this->listener->setProgressValue(1);
	}

	int nx, ny, nTotal;
	C2DPoint delta, pluc;

	C2DPoint extent(this->pMax.x - this->pMin.x, this->pMax.y - this->pMin.y);
	C3DPoint ALSextent(this->pALS->getPMax() - this->pALS->getPMin());
	double nPointsEstimated = double(this->pALS->getNPoints()) *(extent.x * extent.y) / (ALSextent.x * ALSextent.y);
	if (nPointsEstimated > this->pALS->getNPoints()) nPointsEstimated = (double) this->pALS->getNPoints();
	double size = floor(nPointsEstimated / 1000000.0) + 1;
	if (size < 2)
	{
		size =  (extent.x < extent.y) ? extent.y : extent.x;
	}
	else
	{
		size = floor(sqrt(size)) + 1;
		size =  (extent.x < extent.y) ? extent.y / size : extent.x / size;
	}

	tilePmin.clear();
	tilePmax.clear();

	this->initTileStructure(size, pluc, delta, nx, ny,  nTotal);

	ostrstream oss2; oss2.setf(ios::fixed, ios::floatfield);
	oss2.precision(3);
	oss2 << "\nTile Structure:"
		 << "\n==============\n"
		 << "\n Pmin: " << this->pMin.x << " / " << this->pMin.y
		 << "\n Pmax: " << this->pMax.x << " / " << this->pMax.y
		 << "\n Tile extent: " << delta.x << " / "  << delta.y
		 << "\n Number of tiles in X / Y: " << nx + 1 << " / " << ny + 1
		 << "\n Total number of tiles: " << nTotal << "\n";
	protHandler.print(oss2, protHandler.prt);
	
	float zDTMmin =  FLT_MAX;
	float zDTMmax = -FLT_MAX;

	C3DPoint pllc, pruc;
	pllc.z = this->pALS->getPMin().z;
	pruc.z = this->pALS->getPMax().z;

	int nTiles = 0;

	for (; pluc.y > this->pMin.y; pluc.y -= delta.y)
	{
		pruc.y = pluc.y;
		pllc.y = pruc.y - delta.y;
		if (pllc.y < this->pMin.y) pllc.y = this->pMin.y;

		for (pllc.x = pluc.x; pllc.x < this->pMax.x; pllc.x += delta.x, ++nTiles)
		{
			float zmin, zmax;
			pruc.x = pllc.x + delta.x;
			if (pruc.x > this->pMax.x) pruc.x = this->pMax.x;

			this->filterDSM(pllc, pruc, zmin, zmax, nTiles);
			if (zDTMmin > zmin) zDTMmin = zmin;
			if (zDTMmax < zmax) zDTMmax = zmax;
			if (this->listener) 
			{
				float prog = float(nTiles) / float(nTotal) * 100.0f;
				this->listener->setProgressValue(prog);
			}

		}
	}

	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}
	
	ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(3);
	oss1 << "\n Finished DSM initialisation \n Time: " << CSystemUtilities::timeString().GetChar() 
		 << "\n" << ends;;
	
	protHandler.print(oss1, protHandler.prt);
	
	CCharString fn = this->projectPath + "DSM.tif";
	exportFloatBuf(fn.GetChar(), *(this->pDSM), true);

	// Generation of noData Mask
	/*
	float z;
	for (unsigned long u = 0; u < this->pNoDtmMask->getNGV(); ++u)
	{
		z = this->pDSM->pixFlt(u);
		if (z <= -9999.0) this->pNoDtmMask->pixUChar(u) = 0;
		else this->pNoDtmMask->pixUChar(u) = 255;
	}*/
	
	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}
};
	
//=============================================================================

void CALSFilter::filter(double size, int itCount, float &zDTMmin, float &zDTMmax, 
						unsigned int &nGrd, unsigned int &nOff, bool FillHoles, 
						bool filterRobust)
{
	int ptsForInt = this->pointsForInterpolation;

	CCharString suffix;
	if (itCount >= 0)
	{
		ostrstream suffOss;
		if (itCount < 10) suffOss << "0";
		suffOss << itCount << ends;
		char *z = suffOss.str();
		suffix = z;
		delete [] z;
	}
	else
	{
		this->pointsForInterpolation = 9;
		suffix = "for DSM generation";
	}

	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(2);

	CCharString st = " filtering process " + suffix;
	int sl = st.GetLength();
	st = "\nStarting " + st + "\n=========";
	for (int i = 0; i < sl; ++i) st += "=";

	oss << st.GetChar() << "\n\n   Height threshold:        " << this->neighbourHeightThreshold                  
		<< "\n   Minimum building height: " << this->minBuildHeight << "\n   Time: "
		<< CSystemUtilities::timeString().GetChar() << "\n" << ends;
		
	protHandler.print(oss, protHandler.prt);

	if (this->listener) 
	{
		this->listener->setTitleString("Filtering ALS data ...");
		this->listener->setProgressValue(1);
	}

	int nx, ny, nTotal;
	C2DPoint delta, pluc;

	this->initTileStructure(size, pluc, delta, nx, ny,  nTotal);
	
	zDTMmin =  FLT_MAX;
	zDTMmax = -FLT_MAX;

	C3DPoint pllc, pruc;
	pllc.z = this->pALS->getPMin().z;
	pruc.z = this->pALS->getPMax().z;

	nGrd = 0;
	nOff = 0;

	int nTiles = 0;

	for (; pluc.y > this->pMin.y; pluc.y -= delta.y)
	{
		pruc.y = pluc.y;
		pllc.y = pruc.y - delta.y;
		if (pllc.y < this->pMin.y) pllc.y = this->pMin.y;

		for (pllc.x = pluc.x; pllc.x < this->pMax.x; pllc.x += delta.x, ++nTiles)
		{
			float zmin, zmax;
			int ng, no;
			pruc.x = pllc.x + delta.x;
			if (pruc.x > this->pMax.x) pruc.x = this->pMax.x;
			this->filter(pllc, pruc, ng, no, zmin, zmax, nTiles, filterRobust);
			if (zDTMmin > zmin) zDTMmin = zmin;
			if (zDTMmax < zmax) zDTMmax = zmax;
			nGrd += ng;
			nOff += no;
			if (this->listener) 
			{
				float prog = float(nTiles) / float(nTotal) * 100.0f;
				this->listener->setProgressValue(prog);
			}

		}
	}

	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}
	
	ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(3);
	oss1 << "\n Finished filtering process " << suffix.GetChar() << "\n Number of Terrain Points:     ";
	oss1.width(15);
	oss1 << nGrd;
	oss1 << "\n Number of Off-Terrain Points: ";
	oss1.width(15);
	oss1 << nOff << "\n Time: " << CSystemUtilities::timeString().GetChar() << "\n" << ends;;
	
	protHandler.print(oss1, protHandler.prt);
	
	CCharString fn;
	if (itCount >= 0)
	{
		fn = this->projectPath + "DTM_Filtered_withHoles_" + suffix + ".tif";
	}
	else
	{
		fn = this->projectPath + "DSM_Filtered_withHoles_" + suffix + ".tif";
	}
	exportFloatBuf(fn.GetChar(),  *(this->pDTM), true);

	if (FillHoles) this->fillHoles(false);
	else
	{
		float z, zf;
		for (unsigned long u = 0; u < this->pNoDtmMask->getNGV(); ++u)
		{
			z = this->pDSM->pixFlt(u);
			if (z <= -9999.0) this->pNoDtmMask->pixUChar(u) = 0;
			else
			{
				zf = this->pDTM->pixFlt(u);
				if (zf <= -9999.0) this->pNoDtmMask->pixUChar(u) = 1;
				else this->pNoDtmMask->pixUChar(u) = 0;
			}
		}
	}

	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}

	if (itCount >= 0)
	{
		fn = this->projectPath + "DTM_Filtered_NoHoles_" + suffix + ".tif";
	}
	else
	{
		this->pointsForInterpolation = ptsForInt;
		fn = this->projectPath + "DSM_Filtered_NoHoles_" + suffix + ".tif";
	}
	
	if (FillHoles) exportFloatBuf(fn.GetChar(),  *(this->pDTM), true);
};
	
//=============================================================================

CGenericPointCloud *CALSFilter::readPointCloud(const C3DPoint &Min, const C3DPoint &Max, 
		                                       int coords, int annCoords)
{
	CGenericPointCloud *pPoints = new CGenericPointCloud(0, coords, annCoords);

	CALSPoint **pointvec = 0;
	int PtCldCapacity = 0;

	int nPts = this->pALS->getPointCloud(Min, Max, pointvec, PtCldCapacity, 131072, 0);

	if (nPts > 0)
	{
		pPoints->prepareCapacity(nPts);

		for (int i = 0; i < nPts; ++i)
		{
			pPoints->addPoint(pointvec[i]->getLastEcho());
		}
	}
	else
	{
		delete pPoints;
		pPoints = 0;
	}

	for (int i = 0; i < PtCldCapacity; ++i)
	{
		delete pointvec[i];
		pointvec[i] = 0;
	}
	delete [] pointvec;


	return pPoints;
};

//=============================================================================

bool CALSFilter::initRoughness(CGenericPointCloud &points, CHistogramFlt &histoRoughness,
							   double maxDist, int roughnessCoord)
{
	bool ret = points.getNpoints() > 0;

	if (ret)
	{
		int nMin = this->pointsForInterpolation;
		double tanAngle = tan(this->maxObliqueness);

		points.init(); 

		float fac = 100.0f / float(points.getNpoints());

		C3DPoint reductionPoint = points.getPmin();
		reductionPoint.x = floor(reductionPoint.x * 0.001) * 1000.0;
		reductionPoint.y = floor(reductionPoint.y * 0.001) * 1000.0;
		reductionPoint.z = 0.0;

		double *sqrDist = new double[this->pointsForInterpolation];
		ANNpointArray neigh = new ANNpoint[this->pointsForInterpolation]; 
		ANNpoint pA;
		C3DPoint p, pN;
		int nNeigh;
		C2DPoint rowColPoint;
		C3DAdjustPlane plane;

		int nP = points.getNpoints() / 100;

		for (int i = 0; i < points.getNpoints(); ++i)
		{
			if ((this->listener) && (!(i % nP)))
			{
				float val = float(i) * fac;
				this->listener->setProgressValue(val);
			}

			pA = points.getPt(i);
			pA[roughnessCoord] = 0.0;

			p.x = pA[0]; p.y = pA[1]; p.z = pA[2];
			nNeigh = points.getNearestNeighbours(neigh, sqrDist, this->pointsForInterpolation, p, maxDist);

			pA[4] = 0;

			if (nNeigh < nMin)
			{
				pA[roughnessCoord] = -1.0;
			}
			else
			{
				plane.resetPlane();
				plane.resetSums(reductionPoint);

				for (int j = 0; j < nNeigh; ++j)
				{
					pN.x = neigh[j][0];  pN.y = neigh[j][1];  pN.z = neigh[j][2];
					plane.addPoint(pN);
				}

				pA[roughnessCoord] = plane.adjust();

				if (pA[roughnessCoord] > FLT_EPSILON)
				{
				  histoRoughness.addValue((float) pA[roughnessCoord]);
				  if (plane.getObliqueness() > tanAngle) pA[roughnessCoord] = 1000.0;
				}
			}
		}

		delete [] neigh;
		delete [] sqrDist;
	}

	return ret;
};

//=============================================================================

void CALSFilter::calcThresh(double &threshSigma)
{
	int nMin = this->pointsForInterpolation;
	float q = (float) statistics::chiSquareFractil(1.0f - (float) this->alpha, this->pointsForInterpolation - 3);
	threshSigma = this->sigmaALS * sqrt(q / double(this->pointsForInterpolation - 3));
};

//=============================================================================

void CALSFilter::initFiles(ofstream &roughFile, ofstream &smoothFile, ofstream &isolatedFile,
						   ofstream &smoothFileRelabel, ofstream &mergeFile, int tileIndex)
{
	CCharString fnbase = "tile" + CCharString(tileIndex,2,true);

	CCharString fn = this->projectPath + fnbase + "_rough.xyz";
	roughFile.open(fn.GetChar());
	fn = this->projectPath + fnbase + "_smooth.xyz";
	smoothFile.open(fn.GetChar());
	fn = this->projectPath + fnbase + "_isolated.xyz";
	isolatedFile.open(fn.GetChar());
	fn = this->projectPath + fnbase + "_smoothRelabel.xyz";
	smoothFileRelabel.open(fn.GetChar());
	fn = this->projectPath + fnbase + "_smoothMerge.xyz";
	mergeFile.open(fn.GetChar());

	roughFile.setf(ios::fixed, ios::floatfield);
	roughFile.precision(3);
	smoothFile.setf(ios::fixed, ios::floatfield);
	smoothFile.precision(3);
	isolatedFile.setf(ios::fixed, ios::floatfield);
	isolatedFile.precision(3);
	smoothFileRelabel.setf(ios::fixed, ios::floatfield);
	smoothFileRelabel.precision(3);
	mergeFile.setf(ios::fixed, ios::floatfield);
	mergeFile.precision(3);
};

//=============================================================================

void CALSFilter::checkAndMerg(CGenericPointCloud &points, double maxDist,
							  vector<unsigned int> &nClassPts,
							  vector<unsigned int> &nFirstIndex, ofstream &mergeFile)
{
	double *sqrDist = new double[this->pointsForInterpolation];
	ANNpointArray neigh = new ANNpoint[this->pointsForInterpolation]; 
	ANNpoint pA;
	C3DPoint p;
	int nNeigh;

	float fac = 100.0f / float(points.getNpoints());
	int nP = points.getNpoints() / 100;

	for (int i = 0; i < points.getNpoints(); ++i)
	{ 
		// check whether neighbouring segments can be merged

		if ((this->listener) && (!(i % nP)))
		{
			float val = float(i) * fac;
			this->listener->setProgressValue(val);
		}

		pA = points.getPt(i);
			
		unsigned int cl = (unsigned int) pA[4];

		if (cl > 0)
		{
			p.x = pA[0]; p.y = pA[1]; p.z = pA[2];
			nNeigh = points.getNearestNeighbours(neigh, sqrDist, this->pointsForInterpolation, p, maxDist);
				
			vector<unsigned int> exchange;

			for (int j = 0; j < nNeigh; ++j)
			{
				int clMerge = (int) floor(neigh[j][4] + 0.5);

				if ((clMerge > 0) && (clMerge != cl) &&  (fabs(neigh[j][2] - p.z) < this->neighbourHeightThreshold))
				{ // if there is a neighbour that has another label and if its height difference is small, 
				  // the two labels are scheduled for merging
					unsigned int k = 0; 
					bool found = false;
					for (k = 0; (k < exchange.size()) && !found; ++k)
					{
						 found = exchange[k] == clMerge;
					}
					if (!found) exchange.push_back(clMerge);
				}
			}

			if (exchange.size() > 0)
			{ 
				//This is where the labels are merged

				unsigned int firstIdx = nFirstIndex[exchange[0]];
				for (unsigned int k = 0; k < exchange.size(); ++k)
				{
					if (nFirstIndex[exchange[k]] < firstIdx) firstIdx = nFirstIndex[exchange[k]];
				}

				for (unsigned int k = firstIdx; k < (unsigned int) points.getNpoints(); ++k)
				{
					ANNpoint pk = points.getPt(k);
					if (pk[4] > 0)
					{
						for (unsigned int l = 0; l < exchange.size(); ++l)
						{
							if (pk[4] == exchange[l]) pk[4] = cl;
						}
					}
				}

				if (firstIdx < nFirstIndex[cl]) nFirstIndex[cl] = firstIdx;

				for (unsigned int k = 0; k < exchange.size(); ++k)
				{
					nClassPts[cl] += nClassPts[exchange[k]];
					nClassPts[exchange[k]] = 0;
				}
			}
		}
	}

	for (int i = 0; i < points.getNpoints(); ++i)
	{
		pA = points.getPt(i);

		unsigned int cl = (unsigned int) pA[4];

		if (cl > 0)
		{
			if (outputMode >= eMAXIMUM)
			{
				mergeFile << pA[0] << " " << pA[1] << " " << pA[2] << " " << cl << "\n";
			}
		}
	}

	delete [] neigh;
	delete [] sqrDist;
};

//=============================================================================

void CALSFilter::sortLabelPoint(CGenericPointCloud &points, ofstream &roughFile, 
								ofstream &smoothFile, ofstream &isolatedFile, 
								double threshSigma, double maxDist, 
								vector<unsigned int> &nClassPts, 
								vector<unsigned int> &nFirstIndex, long &nSmooth, 
								long &nRough, long &nBorder)
{ 
	double *sqrDist = new double[this->pointsForInterpolation];
	ANNpointArray neigh = new ANNpoint[this->pointsForInterpolation];
	ANNpoint pA;
	C3DPoint p;
	int nNeigh;
	C2DPoint rowColPoint;
	C3DPoint pNeigh;
	C2DPoint rowColPointNeigh;
		
	int maxClass = 0;

	float fac = 100.0f / float(points.getNpoints());
	int nP = points.getNpoints() / 100;

	// classification based on surface roughness:
	// 1) less than nMin Points found in the neighbourhood or a negative value of surface roughness
	//    => 'isolated point'
	// 2) large surface roughness or large obliqueness of the plane => 'rough' point
	// 3) small surface roughness: 
	//    if the point is close to the smoothed DSM => 'smooth point'
	//    if the point is not close to the smoothed DSM => 'rough point'
	//
	// Smooth points will also be assigned an initial segment label as coordinate 4

	for (int i = 0; i < points.getNpoints(); ++i)
	{
		if ((this->listener) && (!(i % nP)))
		{
			float val = float(i) * fac;
			this->listener->setProgressValue(val);
		}

		pA = points.getPt(i);
		if (pA[3] < FLT_EPSILON) // => 'isolated point'
		{
			if (outputMode >= eMAXIMUM)
			{
				isolatedFile << pA[0] << " " << pA[1] << " " << pA[2] << "\n";
			}
			nBorder++;
		}
		else if (pA[3] > threshSigma) // => 'rough' point
		{
			if (outputMode >= eMAXIMUM)
			{
				roughFile    << pA[0] << " " << pA[1] << " " << pA[2] << "\n";
			}
			nRough++;
		}
		else
		{
			float z;
			p.x = pA[0]; p.y = pA[1]; p.z = pA[2];
			this->trafo.transformObj2Obs(rowColPoint, p);
			this->pFilteredDSM->getInterpolatedColourVecBilin(rowColPoint.y, rowColPoint.x, &z);
			z = float(pA[2]) - z;
			if (z < this->minBuildHeight) // the point is close to the smoothed DSM => 'smooth point'
			{	
				// the point is assigned an initial segment label

				unsigned int cl = (unsigned int) pA[4]; // get the label of the point

				p.x = pA[0]; p.y = pA[1]; p.z = pA[2];
				nNeigh = points.getNearestNeighbours(neigh, sqrDist, this->pointsForInterpolation, p, maxDist);

				if (cl == 0)// the point has not received a label yet
				{
					for (int j = 0; (j < nNeigh) && (cl == 0); ++j)					
					{// check whether any of the point's neighbours has already received a label 
						cl = (unsigned int) neigh[j][4];
					}

					if (cl == 0)
					{// None of the point's neighbours has received a label yet => initialise a new label index
						++maxClass;
						nClassPts.push_back(1);
						pA[4] = maxClass;
						cl = maxClass;
						nFirstIndex.push_back(i);
					}
					else
					{
						pA[4] = cl;
						nClassPts[cl] ++;
					}
				}

				for (int j = 0; j < nNeigh; ++j)
				{ // check all neighbours and set their label index to cl if 
				  // 1) the point is also potentially a smooth point
				  // 2) the height difference between the point and the neighbour is smaller than a threshold
				  // 3) the neighbour has not yet been assigned to another segment.

					float zNeigh;
					pNeigh.x = neigh[j][0]; pNeigh.y = neigh[j][1]; pNeigh.z = neigh[j][2];
					this->trafo.transformObj2Obs(rowColPointNeigh, pNeigh);
					this->pFilteredDSM->getInterpolatedColourVecBilin(rowColPointNeigh.y, rowColPointNeigh.x, &zNeigh);
					zNeigh = float(neigh[j][2]) - zNeigh;

					if ((neigh[j][3] <= threshSigma) && (neigh[j][3] >= FLT_EPSILON) && (zNeigh < this->minBuildHeight) && (fabs(neigh[j][2] - p.z) < this->neighbourHeightThreshold))
					{
						int clMerge = (int) floor(neigh[j][4] + 0.5);
						if (clMerge == 0)
						{
							neigh[j][4] = cl;
							nClassPts[cl] ++;
						}
					}
				}
				if (outputMode >= eMAXIMUM)
				{
					smoothFile << pA[0] << " " << pA[1] << " " << pA[2] << " " << cl << "\n";
				}
				nSmooth++;
			}
			else // the point is not close to the smoothed DSM => 'rough point'
			{
				if (outputMode >= eMAXIMUM)
				{
					roughFile    << pA[0] << " " << pA[1] << " " << pA[2] << "\n";
				}
				nRough++;
			}
		}			
	}

	delete [] neigh;
	delete [] sqrDist;
};

//=============================================================================

void CALSFilter::relabel(CGenericPointCloud &points, vector<unsigned int> &nClassPts,
						 int &nClasses, ofstream &smoothFileRelabel)
{
	vector<int> newLabels(nClassPts.size(), 0);
	nClasses = 0;
	ANNpoint pA;

	float fac = 100.0f / float(points.getNpoints());
	int nP = points.getNpoints() / 100;

	// compute point numbers per segment
	for (unsigned int i = 1; i < nClassPts.size(); ++i)
	{
		if (nClassPts[i] > 0)
		{
			++nClasses;
			newLabels[i] = nClasses;
		}
		nClassPts[i] = 0;
	}

	// renumber labels so that there are no empty labels
	for (int i = 0; i < points.getNpoints(); ++i)
	{
		if ((this->listener) && (!(i % nP)))
		{
			float val = float(i) * fac;
			this->listener->setProgressValue(val);
		}

		pA = points.getPt(i);
			
		unsigned int cl = (unsigned int) pA[4];

		if (cl > 0)
		{
			cl = (unsigned int) newLabels[cl];
			pA[4] = cl;
			++nClassPts[cl];
			if (outputMode >= eMAXIMUM)
			{
				smoothFileRelabel   << pA[0] << " " << pA[1] << " " << pA[2] << " " << cl << "\n";
			}
		}
	}
};

//=============================================================================

void CALSFilter::compHisto(CGenericPointCloud &points, vector<unsigned int> &nClassPts, int nClasses, long nSmooth, long nRough, long nBorder)
{
	nClassPts.resize(nClasses + 1);

	ANNpoint pA;
	int indMax = 1;
	unsigned int nMax = nClassPts[1];

	// Testing purposes: compute histogram of point numbers per segment

	for (unsigned int i = 1; i < nClassPts.size(); ++i)
	{
		if (nClassPts[i] > nMax) 
		{
			indMax = i;
			nMax = nClassPts[i];
		}
	}

	CHistogramFlt histoN(0.5f,(float) nMax, 1.0f);

	for (unsigned int i = 1; i < nClassPts.size(); ++i)
	{
		histoN.addValue((float) nClassPts[i]);
	}
	
	if (outputMode >= eMAXIMUM)
	{
		CCharString	fn = this->projectPath + "maxClass.xyz";
		ofstream mf(fn.GetChar());
		mf.setf(ios::fixed, ios::floatfield);
		mf.precision(2);

		for (int i = 0; i < points.getNpoints(); ++i)
		{
			pA = points.getPt(i);
			if (pA[4] == indMax)
			{
				mf << pA[0] << " " << pA[1] << " " << pA[2] << "\n";
			}
		}
	}

	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);
	oss //<< "\n" << histoN.getString(" Histogram of point counts in classes: ", 3, " ")
		<< "\n Number of classes after relabel: " << nClasses
		<< "\n Largest class: " << indMax << " with " << nMax 
		<< "\n Time: "
		<< CSystemUtilities::timeString().GetChar() << "\n" << ends;

	protHandler.print(oss, protHandler.prt);

};

//=============================================================================

void CALSFilter::compLabelImg(CGenericPointCloud &points, int tileIndex, const char *suffix)
{
	CImageBuffer homogMask(*(this->pNoDtmMask), false);
	homogMask.setAll(0);

	CCharString fnbase = CCharString("Homog_") + CCharString(tileIndex+1,2,true) + suffix;
			
	// compute segment label image
	ANNpoint pA;
	C3DPoint p;
	C2DPoint rowColPoint;
	CLabelImage homogLabels(homogMask, 1, 4);

	for (int i = 0; i < points.getNpoints(); ++i)
	{
		pA = points.getPt(i);

		if (pA[4] > 0)
		{
			p.x = pA[0]; p.y = pA[1]; p.z = pA[2];
			this->trafo.transformObj2Obs(rowColPoint, p);
			long row = (long) floor(rowColPoint.y + 0.5);
			long col = (long) floor(rowColPoint.x + 0.5);

			if ((col < homogMask.getWidth() - 1)  && (col > 0) && 
				(row < homogMask.getHeight() - 1) && (row > 0))
			{
				long index = homogLabels.getIndex(row, col);
				homogLabels.pixUShort(index) = (unsigned short) pA[4];
			}
		}
	}

	homogLabels.setMargin(1,0);
	
//	homogLabels.updateVoronoi();

	if (outputMode >= eMEDIUM)
	{
		homogLabels.dump(this->projectPath.GetChar(), fnbase, "_Points");
	}
/*
	for (unsigned int i = 0; i < this->pDSM->getNGV(); ++i)
	{
		if (homogLabels.getDistance(i) <= 7)
		{
			if (this->pDSM->pixFlt(i) > -9999.0f)
				homogMask.pixUChar(i) = 1;
		}
	}

	homogMask.setMargin(1,0);

	int structure = (int) floor(this->morphFilterSizeForDetails / this->resolution.x + 0.5);
	if (structure < 1) structure = 1;

	CImageFilter morphFilter(eMorphological, structure, structure);
	CImageBuffer mask(homogMask);
	morphFilter.binaryMorphologicalOpen(mask, homogMask, 1.0);

	homogLabels.initFromMask(homogMask, 1);
		
	if (outputMode >= eMEDIUM)
	{
		homogLabels.dump(this->projectPath.GetChar(), fnbase, "_Rough");
	}
*/
};

//=============================================================================

void CALSFilter::compAvrgHigh(CGenericPointCloud &points, vector<unsigned int> &nClassPts, 
							  vector<double> &averageHeights)
{
	// compute average heights above coarse DTM for each segment
	ANNpoint pA;
	C3DPoint p;
	C2DPoint rowColPoint;
	int nClassPtsLng = nClassPts.size();
	nClassPts.clear();
	nClassPts.resize(nClassPtsLng, 0);

	for (int i = 0; i < points.getNpoints(); ++i)
	{
		pA = points.getPt(i);
		unsigned int cl = (unsigned int) pA[4];
		
		if (cl > 0)
		{
			float z;
			p.x = pA[0]; p.y = pA[1]; p.z = pA[2];
			this->trafo.transformObj2Obs(rowColPoint, p);
			this->pApproxDTM->getInterpolatedColourVecBilin(rowColPoint.y, rowColPoint.x, &z);
			if (z > DEM_NOHEIGHT)
			{
				averageHeights[cl] += (pA[2] - double(z));
				++nClassPts[cl];
			}
		}
	}

	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);
	oss << "\nAverage hights(label,height): ";

	for (unsigned int i = 1; i < averageHeights.size(); ++i)
	{
		if (nClassPts[i] > 0)
		{
			averageHeights[i] /= (double) nClassPts[i];
			oss << "\n" << i << " H: " << averageHeights[i] << " , N: " << nClassPts[i] ;
		}
	}

	oss << "\n" << ends;
	if (outputMode >= eMAXIMUM)
	{
		protHandler.print(oss, protHandler.prt);
	}
};
	  
//=============================================================================

int CALSFilter::initGrdCloud(CGenericPointCloud &points, CGenericPointCloud &pointsGrd,
							 const vector<double> &averageHeights, 
							 const vector<unsigned int> &nClassPts)
{
	int nGrdPoints = 0;
	ANNpoint pA;
	C3DPoint p;
	// carry out initial classification of the segments: 
	// if the average height of the segment is close to the coarse DTM, it is a 
	// 'terrain segment', otherwise it is an 'off-terrain segment'.

	unsigned int nMax = int (floor(this->morphFilterSize * this->morphFilterSize) / (this->resolution.x * this->resolution.y));
	for (int i = 0; i < points.getNpoints(); ++i)
	{
		pA = points.getPt(i);
		unsigned int cl = (unsigned int) pA[4];
		if (cl > 0)
		{
			if (((averageHeights[cl] < this->minBuildHeight) || (nClassPts[cl] > nMax)) && 
				(nClassPts[cl] > this->pointsForInterpolation))
			{
				nGrdPoints++;
				p.x = pA[0]; p.y = pA[1]; p.z = pA[2];
				pointsGrd.addPoint(p);
			}
		}
	}

	ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(3);
	oss1 << "\nNumber of Groundpoints: " << nGrdPoints
		 << "\n\n" << ends;
	protHandler.print(oss1, protHandler.prt);

	return nGrdPoints;
};

//=============================================================================

void CALSFilter::filterApproxDTM(CGenericPointCloud &pointsGrd, int tileIndex)
{
	C3DPoint Min = this->tilePmin[tileIndex];
	C3DPoint Max = this->tilePmax[tileIndex];
	Min.x = Min.x + 10;
	Min.y = Min.y + 10;
	Max.x = Max.x - 10;
	Max.y = Max.y - 10;

	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	if (outputMode > eMEDIUM)
	{
		oss << "    Filtering Tile: ";
		oss.width(10);
		oss << Min.x << " / ";
		oss.width(10);
		oss << Min.y << ", ";
		oss.width(10);
		oss <<  Max.x << " / ";
		oss.width(10);
		oss << Max.y;
	}

	int width  = int (floor((Max.x - Min.x) / this->resolution.x) + 1);
	int height = int (floor((Max.y - Min.y) / this->resolution.x) + 1);

	float zmin, zmax;

	double distMax = this->pALS->getPointDist().x > this->pALS->getPointDist().y ?
					 this->pALS->getPointDist().x : this->pALS->getPointDist().y;

	double dWeight = distMax * distMax;

	distMax = 5000;

	C2DPoint Pobj(Min.x, Max.y), Ppix;
	this->trafo.transformObj2Obs(Ppix, Pobj);
	Ppix.x = floor(Ppix.x + 0.5);
	Ppix.y = floor(Ppix.y + 0.5);
	long index0 = this->pApproxDTM->getIndex(long(Ppix.y), long(Ppix.x));

	float *bufInter = this->pApproxDTM->getPixF();

	pointsGrd.interpolateRasterForALS(bufInter, 0, width, height, this->pApproxDTM->getDiffY(),
							          index0, Min.x, Max.y, this->resolution.x, this->resolution.y, 
									  Ppix.x, Ppix.y, this->pointsForInterpolation, zmin, zmax,									
									  distMax, distMax, dWeight, 0, 0);
};

//=============================================================================

void CALSFilter::iterCalcDTM(CGenericPointCloud &points, vector<unsigned int> &nClassPts,
							 int nClasses, int tileIndex)
{
	int nGrdPointsOld = points.getNpoints()+1;
	int nGrdPoints = 0;
	int nIterationen = 0;

	for (int j = 0; j <= this->numberIterations; ++j)
	{		
		if (nGrdPoints != nGrdPointsOld)
		{
			nIterationen++;

			vector<double> averageHeights(nClasses + 1, 0.0);
			compAvrgHigh(points, nClassPts, averageHeights);

			nGrdPointsOld = nGrdPoints;

			CGenericPointCloud pointsGrd(points.getNpoints(), 3, 2);
			nGrdPoints = initGrdCloud(points, pointsGrd, averageHeights, nClassPts);
			pointsGrd.init();
			
			if (outputMode >= eMAXIMUM)
			{
				CCharString fnbase = CCharString(tileIndex+1,2,true) + "_it_" + CCharString(j,2,true);
				CCharString fn = this->projectPath + "Grd_" + fnbase + ".xyz";
				pointsGrd.dump(fn.GetChar(), 2);
			}

			filterApproxDTM(pointsGrd, tileIndex);

			if (outputMode >= eMAXIMUM)
			{
				CCharString fnbase = CCharString(tileIndex+1,2,true) + "_it_" + CCharString(j,2,true);
				CCharString fn = this->projectPath + "dtm_" + fnbase + ".tif";
				this->exportFloatBuf(fn.GetChar(), *(this->pApproxDTM), true);
			}
		}
	}

	ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(3);
	oss1 << "\nNumber of used iterations: " << nIterationen - 1
		 << "\n\n" << ends;
	protHandler.print(oss1, protHandler.prt);
};

//=============================================================================

void CALSFilter::expTilePointCloud(CGenericPointCloud &points, int tileIndex)
{
	ANNpoint pA;

    char buf[20];
	ofstream TilePointFile;
	CCharString fn = this->projectPath + "PointfileTile";
	fn = fn + _itoa(tileIndex, buf, 10);
	fn = fn +".xyz";
	TilePointFile.open(fn.GetChar());
	TilePointFile.setf(ios::fixed, ios::floatfield);
	TilePointFile.precision(2);
	
	for (int i = 0; i < points.getNpoints(); ++i)
	{
		pA = points.getPt(i);
		TilePointFile << pA[0] << " " << pA[1] << " " << pA[2] << "\n";
	}

	TilePointFile.close();
};

//=============================================================================

void CALSFilter::filterFinalDTM(CGenericPointCloud &pointsGrd, int tileIndex, ofstream &DTMKoord)
{
	C3DPoint Min = this->tilePmin[tileIndex];
	C3DPoint Max = this->tilePmax[tileIndex];
	Min.x = Min.x + 10;
	Min.y = Min.y + 10;
	Max.x = Max.x - 10;
	Max.y = Max.y - 10;

	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	if (outputMode > eMEDIUM)
	{
		oss << "    Filtering Tile: ";
		oss.width(10);
		oss << Min.x << " / ";
		oss.width(10);
		oss << Min.y << ", ";
		oss.width(10);
		oss <<  Max.x << " / ";
		oss.width(10);
		oss << Max.y;
	}

	int width  = int (floor((Max.x - Min.x) / this->resolution.x) + 1);
	int height = int (floor((Max.y - Min.y) / this->resolution.x) + 1);

	float zmin, zmax;

	double distMax = this->pALS->getPointDist().x > this->pALS->getPointDist().y ?
					 this->pALS->getPointDist().x : this->pALS->getPointDist().y;

	double dWeight = distMax * distMax;

	distMax = 5000;

	C2DPoint Pobj(Min.x, Max.y), Ppix;
	this->trafo.transformObj2Obs(Ppix, Pobj);
	Ppix.x = floor(Ppix.x + 0.5);
	Ppix.y = floor(Ppix.y + 0.5);
	long index0 = this->pDTM->getIndex(long(Ppix.y), long(Ppix.x));

	float *bufInter = this->pDTM->getPixF();
	float *approxBuf = this->pApproxDTM->getPixF();

	CRobustWeightFunction negW, posW;
	double negHW = this->minBuildHeight;
	double posHW = this->sigmaALS * 3;
	double negMark = 100;//this->minBuildHeight * 20;
	double posMark = 100;//this->minBuildHeight * 20;
	negW.setParameters(negHW, negMark);
	posW.setParameters(posHW, posMark);
	pointsGrd.interpolateRasterFinalDTM(bufInter, approxBuf, width, height, this->pDTM->getDiffY(),
							            index0, Min.x, Max.y, this->resolution.x, this->resolution.y, 
									    Ppix.x, Ppix.y, this->pointsForInterpolation, zmin, zmax,									
									    distMax, distMax, dWeight, &posW, &negW, DTMKoord);
};

//=============================================================================

void CALSFilter::finalDTM(CGenericPointCloud &points, int tileIndex, ofstream &DTMKoord)
{
	ANNpoint pA;
	C3DPoint p;
	C2DPoint rowColPoint;
	CGenericPointCloud pointsGrd(points.getNpoints(), 3, 2);
	CGenericPointCloud pointsObj(points.getNpoints(), 3, 2);
	int nObjPoints = 0;
	int nGrdPoints = 0;

	CHistogramFlt histoHeight(0.0f, 50.0f, 0.5f);

	for (int i = 0; i < points.getNpoints(); ++i)
	{
		pA = points.getPt(i);
		float z;
		double deltaz;
		p.x = pA[0]; p.y = pA[1]; p.z = pA[2];

		this->trafo.transformObj2Obs(rowColPoint, p);
		this->pApproxDTM->getInterpolatedColourVecBilin(rowColPoint.y, rowColPoint.x, &z);
		deltaz = (pA[2] - double(z));

		if ((deltaz < this->heightThresholdFilter) && (fabs(deltaz) < this->minBuildHeight))
		{
			pointsGrd.addPoint(p);
			nGrdPoints++;
		}
		else
		{
			pointsObj.addPoint(p);
			nObjPoints++;
		}

		histoHeight.addValue((float) deltaz);
	}

	pointsGrd.init();
	pointsObj.init();

	if (outputMode >= eMAXIMUM)
	{
		CCharString fnbase = CCharString(tileIndex+1,2,true);
		CCharString fn = this->projectPath + "Grd_Tile" + fnbase + ".xyz";
		pointsGrd.dump(fn.GetChar(), 2);

		fnbase = CCharString(tileIndex+1,2,true);
		fn = this->projectPath + "Obj_Tile" + fnbase + ".xyz";
		pointsObj.dump(fn.GetChar(), 2);
	}

	filterFinalDTM(pointsGrd, tileIndex, DTMKoord);

	ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(3);
	if (outputMode >= eMAXIMUM)
	{
		oss1 << "\n" << histoHeight.getString(" Histogram of Heightdifferences: ", 3, " ") 
			 << "\n Number of Terrainpoints: " << nGrdPoints
			 << "\n Number of Offterrainpoints: " << nObjPoints
			 << "\n" << ends;
	}
	else
	{
		oss1 << "\n Number of Terrainpoints: " << nGrdPoints
			 << "\n Number of Offterrainpoints: " << nObjPoints
			 << "\n" << ends;
	}

	protHandler.print(oss1, protHandler.prt);

};

//=============================================================================

 void CALSFilter::finalClassPoints(int &off, int &terrain)
 {
	ofstream offFile;
	ofstream terrainFile;
	ofstream classPointFile;


	CCharString fn = this->projectPath + "offPoints.xyz";
	offFile.open(fn.GetChar());
	fn = this->projectPath + "classifiedPoints.xyz";
	classPointFile.open(fn.GetChar());
	fn = this->projectPath + "terrainPoints.xyz";
	terrainFile.open(fn.GetChar());
	offFile.setf(ios::fixed, ios::floatfield);
	offFile.precision(3);
	classPointFile.setf(ios::fixed, ios::floatfield);
	classPointFile.precision(3);
	terrainFile.setf(ios::fixed, ios::floatfield);
	terrainFile.precision(3);

	ANNpoint pA;
	C3DPoint p;
	C2DPoint rowColPoint;

	CHistogramFlt histoFinalHeight(0.0f, 50.0f, 0.5f);

	double thr = this->sigmaALS * 3.0;
	if (thr < this->heightThresholdFilter) thr = this->heightThresholdFilter;

	for (unsigned int tileIndex = 0; tileIndex < this->tileScratchFiles.size(); ++tileIndex)
	{	
		// Read ALS point cloud into memory
		CGenericPointCloud tilePoints(0, 5, 3);
		tilePoints.init(this->tileScratchFiles[tileIndex].GetChar());
		C2DPoint tilePmin = this->tilePmin[tileIndex];
		C2DPoint tilePmax = this->tilePmax[tileIndex];
		tilePmin.x = tilePmin.x + 10;
		tilePmin.y = tilePmin.y + 10;
		tilePmax.x = tilePmax.x - 10;
		tilePmax.y = tilePmax.y - 10;

		for (int i = 0; i < tilePoints.getNpoints(); ++i)
		{
			pA = tilePoints.getPt(i);
			float z;
			double deltaz;
			p.x = pA[0]; p.y = pA[1]; p.z = pA[2];
			if ((p.x >= tilePmin.x) && (p.x < tilePmax.x) && (p.y >= tilePmin.y) && (p.y < tilePmax.y))
			{
				this->trafo.transformObj2Obs(rowColPoint, p);
				this->pDTM->getInterpolatedColourVecBilin(rowColPoint.y, rowColPoint.x, &z);
				deltaz = (pA[2] - double(z));

				if (fabs(deltaz) < thr)
				{
					terrain++;
					terrainFile << p.x << " " << p.y << " " << p.z << "\n";
					classPointFile << p.x << " " << p.y << " " << p.z << " 1" << "\n";
				}
				else
				{
					off++;
					offFile << p.x << " " << p.y << " " << p.z << "\n";
					classPointFile << p.x << " " << p.y << " " << p.z << " 0" << "\n";
				}

				histoFinalHeight.addValue((float) deltaz);
			}
		}
	}
	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);
	oss << "\n" << histoFinalHeight.getString(" Histogram of final Heightdifferences: ", 3, " ")
		<< "\n Time: "
		<< CSystemUtilities::timeString().GetChar() << "\n" << ends;
	
	if (outputMode >= eMAXIMUM)
	{
		protHandler.print(oss, protHandler.prt);
	}

	offFile.close();
	terrainFile.close();
	classPointFile.close();

 };

//=============================================================================

bool CALSFilter::classifyRoughness(double maxDist)
{
	//Calculate statistic threshold
	double threshSigma;
	calcThresh(threshSigma);

	ofstream DTMKoord;
	CCharString fn = this->projectPath + "DTM.xyz";
	remove(fn.GetChar());
	DTMKoord.open(fn.GetChar(), ios::out|ios::app);
	DTMKoord.setf(ios::fixed, ios::floatfield);
	DTMKoord.precision(3);

	int sumLabel = 0;
	
	ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(3);
	oss1 << "\n\nComputing Surface roughness\n===========================\n\n     Time: "
		 << CSystemUtilities::timeString().GetChar()
		 << "\n\nNumber of Tiles: " << this->tileScratchFiles.size()
		 << "\n\n" << ends;
	protHandler.print(oss1, protHandler.prt);

	for (unsigned int i = 0; i < this->tileScratchFiles.size(); ++i )
	{	
		int tileIndex = i;

		if (this->listener) 
		{
			this->listener->setTitleString("Reading point cloud ...");
			this->listener->setProgressValue(1);
		}

		// Read ALS point cloud into memory
		CGenericPointCloud tilePoints(0, 5, 3);
		tilePoints.init(this->tileScratchFiles[tileIndex].GetChar());

		if (tilePoints.getNpoints() == 0)
		{
			continue;
		}

		//Initialise Files

		ofstream roughFile;
		ofstream smoothFile;
		ofstream isolatedFile;
		ofstream smoothFileRelabel;
		ofstream mergeFile;

		if (outputMode >= eMAXIMUM)
		{
			initFiles(roughFile, smoothFile, isolatedFile, smoothFileRelabel, mergeFile, i+1);
		}

		ostrstream oss6; oss6.setf(ios::fixed, ios::floatfield);
		oss6.precision(3);
		oss6 << "\n Tile index: " << tileIndex + 1
			 << "\n Pmin: " << this->tilePmin[tileIndex].x + 10 << " / "  << this->tilePmin[tileIndex].y + 10
			 << "\n Pmax: " << this->tilePmax[tileIndex].x - 10 << " / "  << this->tilePmax[tileIndex].y - 10
			 << "\n\nInitialising surface roughness " << tileIndex + 1
			 << "\n\n     Time: " << CSystemUtilities::timeString().GetChar() << "\n\n" << ends;
		protHandler.print(oss6, protHandler.prt);

		if (this->listener) 
		{
			this->listener->setTitleString("Computing Roughness ...");
			this->listener->setProgressValue(0);
		}

		CHistogramFlt histoRoughness(0.0f, 1.5f, 0.05f);

		// initialise surface roughness; coordinate 3 of each point is the roughness
		this->initRoughness(tilePoints, histoRoughness, maxDist, 3);

		ostrstream oss2; oss2.setf(ios::fixed, ios::floatfield);
		oss2.precision(3);
		if (outputMode >= eMAXIMUM)
		{
			oss2 << "\n" << histoRoughness.getString(" Histogram of sigma values: ", 3, " ")
				 << "\n Estimated sigma Z a priori: " << this->sigmaALS
				 << "\n Threshold for sigma values from Chi square: " << threshSigma 
				 << " (alpha = " << this->alpha * 100.0f <<" %)" << "\n" << ends;
			protHandler.print(oss2, protHandler.prt);
		}
		else
		{
			oss2 << "\n Estimated sigma Z a priori: " << this->sigmaALS
				 << "\n Threshold for sigma values from Chi square: " << threshSigma 
				 << " (alpha = " << this->alpha * 100.0f <<" %)" << "\n" << ends;
			protHandler.print(oss2, protHandler.prt);
		}

		if (this->listener) 
		{
			this->listener->setTitleString("Classifying Roughness ...");
			this->listener->setProgressValue(0);
		}

		long nSmooth = 0;
		long nRough = 0;
		long nBorder = 0;

		vector<unsigned int> nClassPts(1);
		vector<unsigned int> nFirstIndex(1);

		ostrstream oss4; oss4.setf(ios::fixed, ios::floatfield);
		oss4.precision(3);
		oss4 << "\nClassification of roughness and labeling\n" << ends;
		protHandler.print(oss4, protHandler.prt);

		sortLabelPoint(tilePoints, roughFile, smoothFile, isolatedFile, threshSigma, maxDist, nClassPts, nFirstIndex, nSmooth, nRough, nBorder);

		ostrstream oss12; oss12.setf(ios::fixed, ios::floatfield);
		oss12.precision(3);
		oss12 << "\n Number of smooth points: " << nSmooth
			  << "\n Number of rough points: " << nRough
			  << "\n Number of isolated points: " << nBorder
			  << "\n Number of classes/labels before relabel: " << nClassPts.size()-1
			  << "\n" << ends;
		protHandler.print(oss12, protHandler.prt);

		if (this->listener) 
		{
			this->listener->setTitleString("Merging Smooth patches ...");
			this->listener->setProgressValue(0);
		}

		int nClasses;

		if (nClassPts.size() > 1)
		{
			ostrstream oss7; oss7.setf(ios::fixed, ios::floatfield);
			oss7.precision(3);
			oss7 << "\nMerging of segments\n" << ends;
			protHandler.print(oss7, protHandler.prt);
			
			if (outputMode > eMEDIUM) compLabelImg(tilePoints, i, "_orig");

			checkAndMerg(tilePoints, maxDist, nClassPts, nFirstIndex, mergeFile);

			ostrstream oss8; oss8.setf(ios::fixed, ios::floatfield);
			oss8.precision(3);
			oss8 << "\nRelabeling of the classes\n" << ends;
			protHandler.print(oss8, protHandler.prt);
			relabel(tilePoints, nClassPts, nClasses, smoothFileRelabel);

			ostrstream oss9; oss9.setf(ios::fixed, ios::floatfield);
			oss9.precision(3);
			oss9 << "\nComputing histogram\n" << ends;
			protHandler.print(oss9, protHandler.prt);
			compHisto(tilePoints, nClassPts, nClasses, nSmooth, nRough, nBorder);

			if (outputMode > eMEDIUM)
			{
				ostrstream oss10; oss10.setf(ios::fixed, ios::floatfield);
				oss10.precision(3);
				oss10 << "\nCompute of labelimage\n" << ends;
				protHandler.print(oss10, protHandler.prt);
				compLabelImg(tilePoints, i, "_merged");
			}

			if (this->listener) 
			{
				this->listener->setTitleString("DTM Approximation ...");
			}

			ostrstream oss11; oss11.setf(ios::fixed, ios::floatfield);
			oss11.precision(3);
			oss11 << "\nIterative calculation of DTM" 
				  << "\nMaximum number of Iterations: " << this->numberIterations
				  << "\n"<< ends;
			protHandler.print(oss11, protHandler.prt);
			iterCalcDTM(tilePoints, nClassPts, nClasses, i);
		}

		if (this->listener) 
		{
			this->listener->setTitleString("Final DTM Generation ...");
		}

		ostrstream oss13; oss13.setf(ios::fixed, ios::floatfield);
		oss13.precision(3);
		oss13 << "\nFinal calculation of DTM\n" << ends;
		protHandler.print(oss13, protHandler.prt);
		finalDTM(tilePoints, i, DTMKoord);

		tilePoints.dump(this->tileScratchFiles[tileIndex].GetChar());

		sumLabel = sumLabel + nClasses;

		smoothFile.close();
		roughFile.close();
		isolatedFile.close();
		smoothFileRelabel.close();
		mergeFile.close();

		ostrstream oss5; oss5.setf(ios::fixed, ios::floatfield);
		oss5.precision(3);
		oss5 << "\nRoughness classification for tile " << tileIndex + 1 << " finished; Time: "
			 << CSystemUtilities::timeString().GetChar() << "\n" << ends;
		protHandler.print(oss5, protHandler.prt);
	}

	if (this->listener) 
	{
		this->listener->setTitleString("Final Classification ...");
	}

	int Off = 0;
	int Terrain = 0;
	DTMKoord.close();

	finalClassPoints(Off, Terrain);

	for (unsigned int i = 0; i < this->pApproxDTM->getNGV(); ++i)
	{
		if (!this->pNoDtmMask->pixUChar(i)) this->pApproxDTM->pixFlt(i) = -9999.0;
	}

	for (unsigned int i = 0; i < this->pDTM->getNGV(); ++i)
	{
		if (!this->pNoDtmMask->pixUChar(i)) this->pDTM->pixFlt(i) = -9999.0;
	}

	if (outputMode >= eMEDIUM)
	{
		CCharString fn = this->projectPath + "DTM_approx_final.tif";
		this->exportFloatBuf(fn.GetChar(), *pApproxDTM, true);
		fn = this->projectPath + "DTM_final.tif";
		this->exportFloatBuf(fn.GetChar(), *pDTM, true);
	}

	ostrstream oss3; oss3.setf(ios::fixed, ios::floatfield);
	oss3.precision(3);
	oss3 << "\n Roughness classification for all tiles finished"
		 << "\n Number of Labels for all tiles: " << sumLabel
		 << "\n Number of Offterrainpoints for all tiles: " << Off
		 << "\n Number of Terrainpoints for all tiles: " << Terrain
		 << "\n Time: "
		 << CSystemUtilities::timeString().GetChar() << "\n" << ends;
	protHandler.print(oss3, protHandler.prt);

	return true; 
};

//=============================================================================

bool CALSFilter::filter()
{
/*
	Output Level:
	eMINIMUM = only DSM, final DTM, classified points, terrain points and off terrain points
	eMEDIUM = eMINIMUM with aproximated DSM and DTM
	eMAXXIMUM = All possible results vor all tiles
*/

	bool ret = this->isInitialised();
	if (ret)
	{
		double maxDist = this->pALS->getPointDist().x;
		if (this->pALS->getPointDist().y > maxDist) maxDist = this->pALS->getPointDist().y;
		maxDist *= 5.0;

		ostrstream oss0; oss0.setf(ios::fixed, ios::floatfield);
		oss0.precision(2);

		if (this->listener) 
		{
			this->listener->setTitleString("Preprocessing input data ...");
			this->listener->setProgressValue(1);
		}

		int structure = getFilterSize(this->morphFilterSize);
		int structureDetail = getFilterSize(this->morphFilterSizeForDetails);
		
		float zDTMmin = FLT_MAX, zDTMmax = -FLT_MAX;
     
		int DSMStructureSx = int(floor(this->pALS->getPointDist().x / float(this->resolution.x) + 0.5));
		int DSMStructureSy = int(floor(this->pALS->getPointDist().y / float(this->resolution.y) + 0.5));

		int DSMstructure = (DSMStructureSx > DSMStructureSy)? 3 * DSMStructureSx : 3 * DSMStructureSy;
		if (DSMstructure < 3) DSMstructure = 3; 

		this->initDSMAndTileStructure();

		CImageFilter morphFil1(eMorphological, DSMstructure, DSMstructure);
		morphFil1.greyMorphologicalOpen(*(this->pDSM), *(this->pFilteredDSM), -9999.0);
		int structure1 = this->getFilterSize(this->morphFilterSize);
		CImageFilter morphFil2(eMorphological, structure1, structure1);

/*		
		//Eliminating borderproblem by setting border to minimal hight
		int i0 = 0; // index of left upper corner
		int i1 = this->pDSM->getDiffY() - this->pDSM->getDiffX(); // right upper corner
		int i2 = (this->pDSM->getHeight() - 1) *  this->pDSM->getDiffY(); // left lower corner
		int i3 = i2 + i1; // right lower corner
		float z0 = this->pDSM->pixFlt(i0);
		float z1 = this->pDSM->pixFlt(i1);
		float z2 = this->pDSM->pixFlt(i2);
		float z3 = this->pDSM->pixFlt(i3);
		if (z0 <=  -9999.0) this->pDSM->pixFlt(i0) = (float)this->pMin.z;
		if (z1 <=  -9999.0) this->pDSM->pixFlt(i1) = (float)this->pMin.z;
		if (z2 <=  -9999.0) this->pDSM->pixFlt(i2) = (float)this->pMin.z;
		if (z3 <=  -9999.0) this->pDSM->pixFlt(i3) = (float)this->pMin.z;
*/
		morphFil2.greyMorphologicalOpen(*(this->pDSM), *(this->pApproxDTM), -9999.0);

/*		this->pDSM->pixFlt(i0) = z0;
		this->pDSM->pixFlt(i1) = z1;
		this->pDSM->pixFlt(i2) = z2;
		this->pDSM->pixFlt(i3) = z3;
*/
/*
		for (unsigned int i = 0; i < this->pDSM->getNGV();++i)
		{
			if (!this->pNoDtmMask->pixUChar(i))
			{
				this->pFilteredDSM->pixFlt(i) = DEM_NOHEIGHT;
				this->pApproxDTM->pixFlt(i) = DEM_NOHEIGHT;
			}
		}
*/
		if (outputMode >= eMEDIUM)
		{
			CCharString fn = this->projectPath + "DSM_filtered.tif";
			this->exportFloatBuf(fn.GetChar(), *pFilteredDSM, true);
			fn = this->projectPath + "DTM_approx_start.tif";
			this->exportFloatBuf(fn.GetChar(), *pApproxDTM, true);
		}
		oss0 << "\n Preprocessing finished; Time: " << CSystemUtilities::timeString().GetChar() << "\n" << ends;
		protHandler.print(oss0, protHandler.prt);
	

		classifyRoughness(maxDist);

		for (unsigned int i = 0; i < this->pDSM->getNGV();++i)
		{
			if (!this->pNoDtmMask->pixUChar(i))
			{
				this->pFilteredDSM->pixFlt(i) = DEM_NOHEIGHT;
				this->pApproxDTM->pixFlt(i) = DEM_NOHEIGHT;
			}
		}



		if (this->listener) 
		{
			this->listener->setProgressValue(100);
		}

		this->deletePointClouds();

		if (ret)
		{
			ret = this->pFilteredDTMFile->dumpImage(this->pFilteredDTMFile->getFileNameChar(), *this->pDTM, 0);
		}

		ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
		oss1.precision(2);
		oss1 << "\n Classification finished; Time: " << CSystemUtilities::timeString().GetChar() << "\n" << ends;
		protHandler.print(oss1, protHandler.prt);
	}

	return ret;
};

//=============================================================================

int CALSFilter::getFilterSize(float size)
{
	int structure = (int) floor(size / this->resolution.x + 0.5);
	if (structure < 1) structure = 1;

	return structure;
};

//=============================================================================

void CALSFilter::finalClassification(unsigned int &nGrd, unsigned int &nOff)
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(2);

	oss << "\nFinal Classification\n====================\n\n   Height threshold: " << this->neighbourHeightThreshold << "\n   Time: "
		<< CSystemUtilities::timeString().GetChar() << "\n" << ends;
		
	protHandler.print(oss, protHandler.prt);

	CCharString fn1 = this->projectPath + "TerrainPoints.xyz";
	CCharString fn2 = this->projectPath + "OffTerrainPoints.xyz";
	ofstream grdFile(fn1.GetChar());
	ofstream offFile(fn2.GetChar());
	grdFile.setf(ios::fixed, ios::floatfield);
	offFile.setf(ios::fixed, ios::floatfield);
	grdFile.precision(2);
	offFile.precision(2);

	int nx, ny;
	nx = int(floor((this->pMax.x - this->pMin.x) / (250.0 * this->resolution.x))) + 1;
	ny = int(floor((this->pMax.y - this->pMin.y) / (250.0 * this->resolution.x))) + 1;

	C2DPoint delta((this->pMax.x - this->pMin.x) / double (nx), (this->pMax.y - this->pMin.y) / double (ny));

	delta.x = this->resolution.x * (1.0 + floor(delta.x / this->resolution.x));
	delta.y = this->resolution.y * (1.0 + floor(delta.y / this->resolution.y));

	nx = int(floor((this->pMax.x - this->pMin.x) / delta.x));
	ny = int(floor((this->pMax.y - this->pMin.y) / delta.y)) ;

	C2DPoint pluc(this->pMin.x, this->pMax.y);

	C3DPoint pllc, pruc;
	pllc.z = this->pALS->getPMin().z;
	pruc.z = this->pALS->getPMax().z;
	if (this->listener) 
	{
		this->listener->setTitleString("Filtering ALS data ...");
		this->listener->setProgressValue(1);
	}

	int nTotal = (nx + 1) * (ny + 1);

	nGrd = 0;
	nOff = 0;

	int nTiles = 0;

	for (; pluc.y > this->pMin.y; pluc.y -= delta.y)
	{
		pruc.y = pluc.y;
		pllc.y = pruc.y - delta.y;
		if (pllc.y < this->pMin.y) pllc.y = this->pMin.y;

		for (pllc.x = pluc.x; pllc.x < this->pMax.x; pllc.x += delta.x, ++nTiles)
		{
			pruc.x = pllc.x + delta.x;
			if (pruc.x > this->pMax.x) pruc.x = this->pMax.x;
			CGenericPointCloud allPoints(0, 4);

			if (int(this->tileScratchFiles.size()) > nTiles) allPoints.init(this->tileScratchFiles[nTiles].GetChar());
			else initPointCloud(allPoints, nTiles, this->pMin, this->pMax);

			if (allPoints.getNpoints() > 0)
			{
				C3DPoint Pobj;
				ANNpoint p;
				long idx;

				for (int i = 0; i < allPoints.getNpoints(); ++i)
				{ 
					p = allPoints.getPt(i);
					Pobj.x = p[0]; Pobj.y = p[1]; Pobj.z = p[2];
					if ((Pobj.x >= this->pMin.x) && (Pobj.y >= this->pMin.y) && 
						(Pobj.x <  this->pMax.x) && (Pobj.y < this->pMax.y))
					{
						idx = long(p[3]);
						float zP = float(Pobj.z);
						float zDTM = this->pApproxDTM->pixFlt(idx);
						float zDSM = this->pDSM->pixFlt(idx);
						if ((zP - zDTM < this->neighbourHeightThreshold) && (zP - zDSM < this->neighbourHeightThreshold))
						{
							grdFile << Pobj.x << " " << Pobj.y << " " << Pobj.z << "\n";
							++nGrd;
						}
						else
						{
							offFile << Pobj.x << " " << Pobj.y << " " << Pobj.z << "\n";
							++nOff;
						}
					}
				}
			}

			allPoints.clear();

		
			if (this->listener) 
			{
				float prog = float(nTiles) / float(nTotal) * 100.0f;
				this->listener->setProgressValue(prog);
			}
		}
	}
	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}
	
	ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(3);
	oss1 << "\n Finished final filtering process\n Number of Terrain Points:     ";
	oss1.width(15);
	oss1 << nGrd;
	oss1 << "\n Number of Off-Terrain Points: ";
	oss1.width(15);
	oss1 << nOff << "\n Time: " << CSystemUtilities::timeString().GetChar() << "\n" << ends;;
	
	protHandler.print(oss1, protHandler.prt);
};

//=============================================================================

void CALSFilter::exportFloatBuf(const char *fn, const CImageBuffer &fltBuf, bool uselut)
{	
	CImageBuffer exportBuf(0,0, fltBuf.getWidth(), fltBuf.getHeight(), 1, 8, true);

	float maxVal = -FLT_MAX;
	float minVal =  FLT_MAX;

	for (unsigned long u = 0; u < fltBuf.getNGV(); ++u)
	{
		float val = fltBuf.pixFlt(u);
		if (val != DEM_NOHEIGHT)
		{
			if (val > maxVal) maxVal = val;
			if (val < minVal) minVal = val;
		}
	}

	float fact = maxVal - minVal;
	if (fact < FLT_EPSILON) fact = 1;
	else fact = 255.0f / fact;

	for (unsigned long u = 0; u < fltBuf.getNGV(); ++u)
	{
		float val = fltBuf.pixFlt(u);
		if (val == DEM_NOHEIGHT) exportBuf.pixUChar(u) = 0;
		else 
		{
			val = (val - minVal) * fact;
			exportBuf.pixUChar(u) = (unsigned char) val;
		}
	}

	CLookupTable lut(8,3);

	CLookupTable *pLut = 0;

	if (uselut) 
	{
		lut = CLookupTableHelper::getDSMLookup();
		pLut = &lut;
	}
		
	exportBuf.exportToTiffFile(fn, pLut, false);
};

//=============================================================================
