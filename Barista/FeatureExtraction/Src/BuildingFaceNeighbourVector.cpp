/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */
#include <fstream>
using namespace std;

#include "BuildingFaceNeighbourVector.h"
#include "XYZPolyLine.h"

//****************************************************************************

CBuildingFaceNeighbourVector::CBuildingFaceNeighbourVector(double maxDist): 
           neighbourhoodVector(100, 0), nNeighbourhoods(0), maxDistForIdentity(maxDist)
{
};

//============================================================================

CBuildingFaceNeighbourVector::~CBuildingFaceNeighbourVector() 
{
};

//============================================================================

void CBuildingFaceNeighbourVector::reset()
{
	for (unsigned int i = 0; i < this->neighbourhoodVector.size(); ++i)
	{
		if (this->neighbourhoodVector[i]) delete this->neighbourhoodVector[i];
		this->neighbourhoodVector[i] = 0;
	}

	this->nNeighbourhoods = 0;
};

//============================================================================

CBuildingFaceNeighbourVector &CBuildingFaceNeighbourVector::operator = (const CBuildingFaceNeighbourVector &vec)
{
	if ( this == &vec) return *this;

	this->reset();
	this->neighbourhoodVector.resize(vec.nNeighbourhoods, 0);
	this->nNeighbourhoods = vec.nNeighbourhoods;

	for (unsigned int u = 0; u < this->nNeighbourhoods; ++u)
	{
		this->neighbourhoodVector[u] = new CBuildingFaceNeighbourRecord(*vec.neighbourhoodVector[u]);
	};

	return *this;
};
	  
//============================================================================

bool CBuildingFaceNeighbourVector::getUpperAndLowerBound(unsigned short Region1, 
														 unsigned short Region2, 
														 unsigned int &lowerBound, 
														 unsigned int &upperBound) const
{
	if (Region1 > Region2)
	{
		unsigned short swapR = Region1;
		Region1 = Region2;
		Region2 = swapR;
	}

	lowerBound = 0;
	upperBound = this->nNeighbourhoods;

	if (!nNeighbourhoods) return false;
	
	if ((this->neighbourhoodVector[lowerBound]->getRegion1() > Region1) ||
		(this->neighbourhoodVector[upperBound - 1]->getRegion1() < Region1)) return false;

	unsigned int auxiliaryUpper = upperBound;
	unsigned int auxiliaryLower = lowerBound;

	while (auxiliaryUpper >= auxiliaryLower) 
	{
		lowerBound = ( auxiliaryUpper + auxiliaryLower ) / 2;
		unsigned short tstReg = this->neighbourhoodVector[lowerBound]->getRegion1();
		if (tstReg >= Region1) auxiliaryUpper = lowerBound - 1;
		else auxiliaryLower = lowerBound + 1;
	}

	auxiliaryUpper = upperBound;
	auxiliaryLower = lowerBound;

	while (auxiliaryUpper >= auxiliaryLower) 
	{
		upperBound = ( auxiliaryUpper + auxiliaryLower ) / 2;
		unsigned short tstReg = this->neighbourhoodVector[lowerBound]->getRegion1();
		if (tstReg > Region1) auxiliaryUpper = lowerBound - 1;
		else auxiliaryLower = lowerBound + 1;
	}

	return true;
};

//============================================================================

CBuildingFaceNeighbourRecord *CBuildingFaceNeighbourVector::isContained(CBuildingFaceNeighbourRecord *rec) const
{
	unsigned int lowerBound, upperBound;
														 
	for (unsigned int i = 0; i < this->nNeighbourhoods; ++i)
	{
		CBuildingFaceNeighbourRecord *pNeighbour = this->neighbourhoodVector[i];
		if ((pNeighbour->hasRegion(rec->getRegion1())) && (pNeighbour->hasRegion(rec->getRegion2())))
		{
			C2DPoint p1N = pNeighbour->getInitialStartPt();
			C2DPoint p2N = pNeighbour->getInitialEndPt();
			C2DPoint p1R = rec->getInitialStartPt();
			C2DPoint p2R = rec->getInitialEndPt();
			if (((p1N.getDistance(p1R) < this->maxDistForIdentity) &&
				 (p2N.getDistance(p2R) < this->maxDistForIdentity)) ||
				((p1N.getDistance(p2R) < this->maxDistForIdentity) && 
				 (p2N.getDistance(p1R) < this->maxDistForIdentity))) return pNeighbour;
		}
	}

	return 0;
};

//============================================================================

void CBuildingFaceNeighbourVector::addNeighbour(CBuildingFaceNeighbourRecord *rec)
{
	if (this->nNeighbourhoods >= this->neighbourhoodVector.size()) 
			this->neighbourhoodVector.resize(this->nNeighbourhoods + 100, 0);

	this->neighbourhoodVector[this->nNeighbourhoods] = rec;
	++this->nNeighbourhoods;
};

//============================================================================

CBuildingFaceNeighbourRecord *CBuildingFaceNeighbourVector::addNeighbours(unsigned short Region1, 
																		  unsigned short Region2,
																		  CBuildingFacePoly *facePoly,
		                                                                  float rmsPlanes,  
																		  CXYContour *pContour, 
																		  float sigmaContour, 
																		  CTFW &trafo, 
																		  const C3DPoint &reduction, 
																		  float maxDist)
{
	CBuildingFaceNeighbourRecord *newRec = new CBuildingFaceNeighbourRecord(Region1, Region2);
	newRec->setRms(rmsPlanes);
	newRec->initPolys(pContour, sigmaContour, trafo, reduction, maxDist); 

	CBuildingFaceNeighbourRecord *rec = this->isContained(newRec);
	if (rec) delete newRec;
	else 
	{
		rec = newRec;
		this->addNeighbour(rec);
	};

	rec->setFacePolygon(facePoly);

	return rec;
};

//============================================================================

void CBuildingFaceNeighbourVector::removeNeighbours(unsigned short Region1, unsigned short Region2)
{
	unsigned newSize = 0;
	for (unsigned int i = 0; i < this->nNeighbourhoods; ++i)
	{
		CBuildingFaceNeighbourRecord *pNeighbour = this->neighbourhoodVector[i];
		if (pNeighbour)
		{
			if ((pNeighbour->hasRegion(Region1)) && (pNeighbour->hasRegion(Region2)))
			{
				delete pNeighbour;
				this->neighbourhoodVector[i] = 0;
			}
			else
			{
				this->neighbourhoodVector[newSize] = pNeighbour;
				++newSize;
			}
		}
	}

	this->nNeighbourhoods = newSize;
};

//============================================================================

void CBuildingFaceNeighbourVector::removeAllNeighbours(unsigned short Region1)
{
	unsigned newSize = 0;
	for (unsigned int i = 0; i < this->nNeighbourhoods; ++i)
	{
		CBuildingFaceNeighbourRecord *pNeighbour = this->neighbourhoodVector[i];
		if (pNeighbour)
		{
			if (pNeighbour->hasRegion(Region1))
			{
				delete pNeighbour;
				this->neighbourhoodVector[i] = 0;
			}
			else
			{
				this->neighbourhoodVector[newSize] = pNeighbour;
				++newSize;
			}
		}
	}

	this->nNeighbourhoods = newSize;
};

//============================================================================

void CBuildingFaceNeighbourVector::mergePlanes(unsigned short Region1, unsigned short Region2)
{
	this->removeNeighbours(Region1, Region2);

	for (unsigned int index = 0; index < this->nNeighbourhoods; ++index)
	{
		this->neighbourhoodVector[index]->replaceRegion(Region1, Region2);
	};

	this->removeNeighbours(Region1, Region1);
	this->removeNeighbours(Region2, Region2);
};

//============================================================================

void CBuildingFaceNeighbourVector::sort()
{
	for (unsigned int i = 0; i < this->nNeighbourhoods; ++i)
	{
		for (unsigned int j = i + 1; j < this->nNeighbourhoods; ++j)
		{
			if (this->neighbourhoodVector[j]->getRegion1() < this->neighbourhoodVector[i]->getRegion1())
			{
				this->swapRegions(i, j);
			}
			else if (this->neighbourhoodVector[j]->getRegion1() ==  this->neighbourhoodVector[i]->getRegion1())
			{
				if (this->neighbourhoodVector[j]->getRegion2() <  this->neighbourhoodVector[i]->getRegion2())
				{
					this->swapRegions(i, j);
				}
			};
		};
	};
};

//============================================================================

void CBuildingFaceNeighbourVector::sortByRMS()
{
	if (this->nNeighbourhoods > 1)
	{
		int iMin = this->nNeighbourhoods - 1;
		CBuildingFaceNeighbourRecord *pMin = this->neighbourhoodVector[iMin];

		for (int i = iMin - 1; i >= 0; --i )
		{
			if (this->neighbourhoodVector[i]->getRmsError() < pMin->getRmsError()) 
			{ 
				iMin = i; 
				pMin = this->neighbourhoodVector[iMin]; 
			}
		}

		this->swapRegions(iMin, this->nNeighbourhoods - 1);

		this->sortPartByRMS(0, this->nNeighbourhoods - 2);
	};
};

//============================================================================

void CBuildingFaceNeighbourVector::sortPartByRMS(int indexLow, int IndexHigh)
{
	if (indexLow < IndexHigh)   // stopping condition for recursion!
	{
		int idLo = indexLow;
		int idHi = IndexHigh + 1;
		CBuildingFaceNeighbourRecord *pRec = this->neighbourhoodVector [indexLow];

		for ( ; ; )
		{
			while (this->neighbourhoodVector[++idLo]->getRmsError() > pRec->getRmsError() );
			while (this->neighbourhoodVector[--idHi]->getRmsError() < pRec->getRmsError() );
			if (idLo < idHi) this->swapRegions(idLo, idHi);
			else break;
		}

		this->swapRegions(indexLow, idHi);
		this->sortPartByRMS(indexLow, idHi - 1);
		this->sortPartByRMS(idHi + 1, IndexHigh);
	}
};

//============================================================================

void CBuildingFaceNeighbourVector::swapRegions(unsigned int index1, unsigned int index2)
{
	CBuildingFaceNeighbourRecord *pRecord = this->neighbourhoodVector[index1];
	this->neighbourhoodVector[index1] = this->neighbourhoodVector[index2];
	this->neighbourhoodVector[index2] = pRecord;
};

//============================================================================

void CBuildingFaceNeighbourVector::print(ostream &anOstream) const
{
	for (unsigned int i = 0; i < this->nNeighbourhoods; ++i)
	{
		this->neighbourhoodVector[i]->print(anOstream);
	};
};

//============================================================================
	  
void CBuildingFaceNeighbourVector:: exportDXF(ofstream &dxfFile, 
											  bool exportInitial, 
											  bool exportApprox, 
											  bool exportFinal) const
{
	for (unsigned int i = 0; i < this->nNeighbourhoods; ++i)
	{
		this->neighbourhoodVector[i]->exportDXF(dxfFile, exportInitial, exportApprox, exportFinal);
	};
};
	  
//============================================================================
	  
void CBuildingFaceNeighbourVector:: exportDXF(const char *dxfFile, 
											  bool exportInitial, 
											  bool exportApprox, 
											  bool exportFinal) const
{
	ofstream dxf(dxfFile);
	if (dxf.good()) 
	{
		CXYZPolyLine::writeDXFHeader(dxf);
		this->exportDXF(dxf, exportInitial, exportApprox, exportFinal);
		CXYZPolyLine::writeDXFTrailer(dxf);
	}
};

//============================================================================


