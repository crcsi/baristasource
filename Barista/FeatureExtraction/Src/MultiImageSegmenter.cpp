#include <float.h>
#include <strstream>
using namespace std;

#include "MultiImageSegmenter.h"

#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "ObservationHandler.h"
#include "HugeImage.h"
#include "RAG.h"
#include "ImgBuf.h"
#include "LabelImage.h"
#include "XYPolyLine.h"
#include "XYZPolyLine.h"
#include "LabelMatcher.h"

/*
#include "statistics.h"
*/

//=============================================================================

MultiImageSegmenter::MultiImageSegmenter() : pDSM(0), outputPath(), bufferSize(0),
		pCombinedLabels(0)
{ 
};

//=============================================================================

MultiImageSegmenter::~MultiImageSegmenter()
{
	this->resetImageVectors();
};

//=============================================================================

bool MultiImageSegmenter::init(CImageBuffer *DSM, const CTFW &TrafoDSM,
		                       vector<CObservationHandler *> *imgVec, 						
                			   vector<CHugeImage *> *hugImgVec, 
							   const CXYZPolyLineArray &BoundaryPolygons, 
						       const C3DPoint &PMin, const C3DPoint &PMax,
						       double pixImg, double SearchBuf,
						       const FeatureExtractionParameters &Pars,
						       const CCharString &path)
{ 
	this->DSMtrafo = TrafoDSM;
	this->resetImageVectors();
	this->BoundaryPolygons = BoundaryPolygons;
	this->pDSM = DSM;
	this->outputPath = path;
	this->extractionPars = Pars; 
	this->bufferSize = SearchBuf;
	this->pmin = PMin;
	this->pmax = PMax;

	if (!outputPath.IsEmpty())
	{
		CSystemUtilities::mkdir(outputPath);

		if (outputPath[outputPath.GetLength() - 1] != CSystemUtilities::dirDelim)
			outputPath += CSystemUtilities::dirDelimStr;
	}
	

	ostrstream oss1;
	oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(2);
	oss1 << "S e g m e n t i n g   M u l t i p l e   I m a g e s\n===================================================\n\n"
		 << "\n Date: " << CSystemUtilities::dateString() 
		 << "\n Starting Time: " << CSystemUtilities::timeString() << "\n\n";
	oss1 << ends;
	protHandler.print(oss1, protHandler.prt);

	if (pixImg > FLT_EPSILON)
	{
		this->pmin.x = floor ((this->pmin.x - this->bufferSize) / pixImg) * pixImg;							
		this->pmin.y = floor ((this->pmin.y - this->bufferSize) / pixImg) * pixImg;							
				
		this->pmax.x = floor ((this->pmax.x + this->bufferSize) / pixImg + 1.0) * pixImg;							
		this->pmax.y = floor ((this->pmax.y + this->bufferSize) / pixImg + 1.0) * pixImg;							

		if ((pmax.x > this->pmin.x) && (pmax.y > this->pmin.y))
		{		
			if ((imgVec && hugImgVec) && (pixImg > FLT_EPSILON))
			{
				if (imgVec->size() == hugImgVec->size())
					this->initImages(imgVec, hugImgVec, pixImg);
			}
		}
	}

	if (this->isInitialised())
	{
		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(2);
		oss << "\n Input data initialised; Time: " << CSystemUtilities::timeString() << "\n" << ends;
		protHandler.print(oss, protHandler.prt);
	}
	else
	{
		protHandler.print(" Error: could not initialise data", protHandler.prt); 
		return false;
	}

	return true;
};

//=============================================================================

long MultiImageSegmenter::getLabelIndex(const CLabelImage &labels, const C3DPoint& P) const
{
	C2DPoint obs;
	long retIndex = -1;

	this->imageTrafo.transformObj2Obs(obs, P);
	obs.x = floor(obs.x - labels.getOffsetX() + 0.5);
	obs.y = floor(obs.y - labels.getOffsetY() + 0.5);

	if ((obs.x >= 0) && (obs.y >= 0) && 
		(obs.x < labels.getWidth()) &&
		(obs.y < labels.getHeight()))
	{
		retIndex = labels.getIndex(long(obs.y), long(obs.x));	
	};

	return retIndex;
}; 


//=============================================================================
	
long MultiImageSegmenter::getObjectLabelIndex(const C3DPoint& P) const
{
	return this->getLabelIndex(*this->pCombinedLabels, P);
}; 

//=============================================================================

unsigned short MultiImageSegmenter::getObjectLabel(const C3DPoint& P) const
{
	unsigned short label = 0;
	long index = this->getObjectLabelIndex(P);
	if (index >= 0)
	{
		label = this->pCombinedLabels->getLabel(index);
	}

	return label;
};

//=============================================================================

unsigned short MultiImageSegmenter::getProjectedLabel(unsigned int projectedIndex, const C3DPoint& P) const
{
	unsigned short label = 0;
	long index = this->getObjectLabelIndex(P);
	if (index >= 0)
	{
		label = this->objectLabelVector[projectedIndex]->getLabel(index);
	}

	return label;
};

//=============================================================================

unsigned short MultiImageSegmenter::getLabel(const CLabelImage &labels, const C3DPoint& P) const
{
	unsigned short label = 0;
	long index = this->getLabelIndex(labels, P);
	if (index >= 0)
	{
		label = labels.getLabel(index);
	}

	return label;
};

//=============================================================================

bool MultiImageSegmenter::initImages(vector<CObservationHandler *> *imgVec, 
					                 vector<CHugeImage *> *hugImgVec, 
									 double pixelSize)
{
	bool ret = false;

	C2DPoint shift(this->pmin.x, this->pmax.y);
	C2DPoint pix(pixelSize, pixelSize);

	this->imageTrafo.setFromShiftAndPixelSize(shift, pix);

	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(2);

	oss << " Initialising Images:\n ====================\n   Pixel size in object space: " << pixelSize << " [m]\n";
			
	for (unsigned int i = 0; i < imgVec->size(); ++i)
	{
		bool r1 = initImage((*imgVec)[i], (*hugImgVec)[i], pixelSize);
		ret = ret || r1;

		if (r1)
		{
			CImageBuffer *buf = this->imageVector[this->imageVector.size() - 1];
			oss << "    Image: " << (*imgVec)[i]->getFileNameChar() << " :   Offset: ";
			oss.width(6); oss << buf->getOffsetX() << " / ";
			oss.width(6); oss << buf->getOffsetY() << " ;  Size: ";
			oss.width(4); oss << buf->getWidth() << " x ";
			oss.width(4); oss << buf->getHeight() << "\n";
		}
	}

	oss << ends;
	protHandler.print(oss, protHandler.prt);

	return ret;
};

//=============================================================================

bool MultiImageSegmenter::initImage(CObservationHandler *oh, CHugeImage *hugImg, double pixSize)
{
	bool ret = true;
	CSensorModel *model = oh->getCurrentSensorModel();
	
	int width  = int(floor((this->pmax.x - this->pmin.x) / pixSize)) + 1;
	int height = int(floor((this->pmax.y - this->pmin.y) / pixSize)) + 1;

	if ((width < 5) || (height < 5) || (!model) || (hugImg->getBpp() > 16)) ret = false;
	else
	{
		int offsetX, offsetY, Width, Height;
		ret = this->getImageRegion(oh,  hugImg, offsetX, offsetY, Width, Height);

		if (ret)
		{
			CImageBuffer *img      = new CImageBuffer(offsetX, offsetY, Width, Height, hugImg->getBands(), hugImg->getBpp(), true);
			CLabelImage  *imglabel = new CLabelImage(offsetX, offsetY, Width, Height, 8);
			CLabelImage  *objlabel = new CLabelImage(0, 0, width, height, 8);
			CRAG         *rag      = new CRAG;

			if ((!img) || (!imglabel) || (!objlabel) || (!rag)) ret = false;
			else
			{
				ret = hugImg->getRect(*img);
			}

			if (ret) 
			{
				this->pCombinedLabels = new CLabelImage(0, 0, width, height, 8);
				if (!this->pCombinedLabels) ret = false;
			}
			if (ret)
			{
				this->sensorModelVector.push_back(model);
				this->imageVector.push_back(img);
				this->imageLabelVector.push_back(imglabel);
				this->objectLabelVector.push_back(objlabel);
				this->imageRAGVector.push_back(rag);
			}
			else
			{
				if (img)      delete img;
				if (imglabel) delete imglabel;
				if (objlabel) delete objlabel;
				if (rag)      delete rag;
				if (this->pCombinedLabels) delete this->pCombinedLabels;
				this->pCombinedLabels = 0;
			}
		}
	}

	return ret;
};

//=============================================================================
	  
bool MultiImageSegmenter::getImageRegion(CObservationHandler *oh, CHugeImage *hugImg, 
										 int &offsetX, int &offsetY, 
										 int &width, int &height)
{
	CSensorModel *model = oh->getCurrentSensorModel();
	bool ret = true;

	if (!model) ret = false;
	else
	{
		C3DPoint P1(pmin.x, pmin.y, pmin.z);
		C3DPoint P2(pmin.x, pmax.y, pmin.z);
		C3DPoint P3(pmax.x, pmin.y, pmin.z);
		C3DPoint P4(pmax.x, pmax.y, pmin.z);
		C3DPoint P5(pmin.x, pmin.y, pmax.z);
		C3DPoint P6(pmin.x, pmax.y, pmax.z);
		C3DPoint P7(pmax.x, pmin.y, pmax.z);
		C3DPoint P8(pmax.x, pmax.y, pmax.z);
        C2DPoint obs;
		model->transformObj2Obs(obs,P1);
		offsetX = int(floor(obs.x));
		offsetY = int(floor(obs.y));
		int Xmax = offsetX;
		int Ymax = offsetY;
		model->transformObj2Obs(obs,P2);
		int x = int(floor(obs.x));
		int y = int(floor(obs.y));
		if (x < offsetX) offsetX = x;
		if (y < offsetY) offsetY = y;
		if (x > Xmax) Xmax = x;
		if (y > Ymax) Ymax = y;
		model->transformObj2Obs(obs,P3);
		x = int(floor(obs.x));
		y = int(floor(obs.y));
		if (x < offsetX) offsetX = x;
		if (y < offsetY) offsetY = y;
		if (x > Xmax) Xmax = x;
		if (y > Ymax) Ymax = y;
		model->transformObj2Obs(obs,P4);
		x = int(floor(obs.x));
		y = int(floor(obs.y));
		if (x < offsetX) offsetX = x;
		if (y < offsetY) offsetY = y;
		if (x > Xmax) Xmax = x;
		if (y > Ymax) Ymax = y;
		model->transformObj2Obs(obs,P5);
		x = int(floor(obs.x));
		y = int(floor(obs.y));
		if (x < offsetX) offsetX = x;
		if (y < offsetY) offsetY = y;
		if (x > Xmax) Xmax = x;
		if (y > Ymax) Ymax = y;
		model->transformObj2Obs(obs,P6);
		x = int(floor(obs.x));
		y = int(floor(obs.y));
		if (x < offsetX) offsetX = x;
		if (y < offsetY) offsetY = y;
		if (x > Xmax) Xmax = x;
		if (y > Ymax) Ymax = y;
		model->transformObj2Obs(obs,P7);
		x = int(floor(obs.x));
		y = int(floor(obs.y));
		if (x < offsetX) offsetX = x;
		if (y < offsetY) offsetY = y;
		if (x > Xmax) Xmax = x;
		if (y > Ymax) Ymax = y;
		model->transformObj2Obs(obs,P8);
		x = int(floor(obs.x));
		y = int(floor(obs.y));
		if (x < offsetX) offsetX = x;
		if (y < offsetY) offsetY = y;
		if (x > Xmax) Xmax = x;
		if (y > Ymax) Ymax = y;

		offsetX -= 3;
		offsetY -= 3;
		Xmax += 3;
		Ymax += 3;
		
		int origWidth  = Xmax - offsetX + 1;
		int origHeight = Ymax - offsetY + 1;

		if (offsetX < 0) offsetX = 0;
		if (offsetY < 0) offsetY = 0;
		if (Xmax + 1 > hugImg->getWidth())  Xmax = hugImg->getWidth()  - 1;
		if (Ymax + 1 > hugImg->getHeight()) Ymax = hugImg->getHeight() - 1;

		width  = Xmax - offsetX + 1;
		height = Ymax - offsetY + 1;

		origWidth -= width;
		origHeight -= height;

		if ((width < 3) || (height < 3)) ret = false;
		else if ((origWidth > 20) || (origHeight > 20)) ret = false;
	}

	return ret;
};

//=============================================================================
	  
void MultiImageSegmenter::resetImageVectors()
{
	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		delete this->imageVector[i];
		delete this->imageLabelVector[i];
		delete this->objectLabelVector[i];
		delete this->imageRAGVector[i];

		this->imageVector[i]       = 0;
		this->imageLabelVector[i]  = 0;
		this->objectLabelVector[i] = 0; 
		this->sensorModelVector[i] = 0;
		this->imageRAGVector[i]    = 0;
	}


	if (this->pCombinedLabels) delete this->pCombinedLabels;
	this->pCombinedLabels = 0;

	this->imageVector.clear();
	this->sensorModelVector.clear();
	this->imageLabelVector.clear();
	this->objectLabelVector.clear();
	this->imageRAGVector.clear();
};

//=============================================================================

void MultiImageSegmenter::exportImageBuffersToTiff(const CCharString &suffix) const
{
	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		ostrstream oss;
		oss << "_" << i  << ends;
		char *z = oss.str();
		
		CCharString s1 = z + suffix + CCharString(".tif");

		CCharString str = this->outputPath + "image" + s1;

		this->imageVector[i]->exportToTiffFile(str.GetChar(), 0, true); 

		delete [] z;
	}

};
		  
//=============================================================================

void MultiImageSegmenter::exportDSMToTiff(const CCharString &suffix) const
{
	if (this->pDSM)
	{
		CCharString filename = this->outputPath + "DSM" + suffix + ".tif";
		this->pDSM->exportToTiffFile(filename.GetChar(), 0, true); 
	}
};
	
//=============================================================================

void MultiImageSegmenter::exportOcclusionBuffersToTiff(const CCharString &suffix) const
{
	for (unsigned int i = 0; i < this->occlusionMasks.size(); ++i)
	{
		ostrstream oss;
		oss << "_" << i  << ends;
		char *z = oss.str();
		
		CCharString s1 = z + suffix + CCharString(".tif");

		CCharString str = this->outputPath + "occl" + s1;

		this->occlusionMasks[i]->exportToTiffFile(str.GetChar(), 0, true); 

		delete [] z;
	}
};

//=============================================================================
	  
void MultiImageSegmenter::exportLabelBufferToTiff(int index, const CCharString &suffix) const
{
	ostrstream oss;
	oss << "_" << index  << ends;
	char *z = oss.str();
		
	this->imageLabelVector[index]->dump(this->outputPath.GetChar(), suffix.GetChar(), z); 

	delete [] z;
};

//=============================================================================

void MultiImageSegmenter::exportLabelBuffersToTiff(const CCharString &suffix) const
{
	for (unsigned int i = 0; i < this->imageLabelVector.size(); ++i)
	{
		this->exportLabelBufferToTiff(i, suffix);
	}
};
		
//=============================================================================

void MultiImageSegmenter::exportProjectedLabelBuffersToTiff(const CCharString &suffix) const
{
	for (unsigned int i = 0; i < this->objectLabelVector.size(); ++i)
	{
		ostrstream oss;
		oss << suffix.GetChar() << "_" << i << ends;
		char *z = oss.str();
		
		this->objectLabelVector[i]->dump(this->outputPath.GetChar(), "proj", z); 

		delete [] z;
	}
};
		
//=============================================================================

void MultiImageSegmenter::exportObjectLabelBufferToTiff(const CCharString &suffix) const
{
	if (this->pCombinedLabels)
		this->pCombinedLabels->dump(this->outputPath.GetChar(), "combined", suffix.GetChar()); 
};
//=============================================================================

CImageBuffer *MultiImageSegmenter::getValidMask(unsigned int imgIndex, double zmax) const
{
	CImageBuffer *validMask = 0;
	CImageBuffer *img = this->imageVector[imgIndex];
		
	if (this->BoundaryPolygons.GetSize())
	{
		C2DPoint p1, p2;
		C3DPoint P3(0,0,0);
		this->imageTrafo.transformObj2Obs(p1, P3);
		P3.x = this->bufferSize;
		this->imageTrafo.transformObj2Obs(p2, P3);

		int structureSize = int(floor(p1.getDistance(p2))) + 1;
		validMask = new CImageBuffer(img->getOffsetX(), img->getOffsetY(), img->getWidth(), img->getHeight(), 1, 1, true,0);
		CImageBuffer buf(*validMask);

		for (int j = 0; j < this->BoundaryPolygons.GetSize(); ++j)
		{
			CXYZPolyLine *pBoundaryPolygon = this->BoundaryPolygons.getAt(j);
			CXYPolyLine transformedBoundary;
			CXYPoint p;

			for (int k = 0; k < pBoundaryPolygon->getPointCount(); ++k)
			{
				CXYZPoint P = (*pBoundaryPolygon->getPoint(k));
				this->sensorModelVector[imgIndex]->transformObj2Obs(p, P);
				transformedBoundary.addPoint(p);
			}

			if (!transformedBoundary.isClosed()) transformedBoundary.closeLine();

			buf.fillByValue(transformedBoundary, 1);
			
			for (int k = 0; k < pBoundaryPolygon->getPointCount(); ++k)
			{
				CXYZPoint P = (*pBoundaryPolygon->getPoint(k));
				P.z = zmax;
				this->sensorModelVector[imgIndex]->transformObj2Obs(p, P);
				transformedBoundary.getPoint(k) = p;
			}

			buf.fillByValue(transformedBoundary, 1);
		}
		CImageFilter filter(eMorphological, structureSize, structureSize);
		filter.binaryMorphologicalClose(buf, *validMask, 1.0);

		if (outputMode > eMEDIUM)
		{
			CCharString indexString(imgIndex, 2, true);
			CCharString fn = this->outputPath + "valid_" + indexString + ".tif";
			validMask->exportToTiffFile(fn.GetChar(),0,true);
		}
	}

	return validMask;
};

//=============================================================================

bool MultiImageSegmenter::segmentImages()
{
	bool ret = true;

	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		CImageBuffer *img = this->imageVector[i];
		CImageBuffer *validMask = this->getValidMask(i, this->pmax.z);

		CFeatureExtractor extractor(this->extractionPars);
		extractor.setRoi(this->imageVector[i], validMask);
		extractor.extractFeatures(0,0,0,this->imageLabelVector[i], this->imageRAGVector[i], true);

		if (validMask) 
		{		
			delete validMask;
		}
	}

	return ret;
};
 	 
//=============================================================================
	 	  
CImageBuffer *MultiImageSegmenter::getValidObjectMask() const
{
	CImageBuffer *validMask = new CImageBuffer(objectLabelVector[0]->getOffsetX(), 
		                                       objectLabelVector[0]->getOffsetY(), 
		                                       objectLabelVector[0]->getWidth(), 
											   objectLabelVector[0]->getHeight(), 1, 1, true,0);

	if (!this->BoundaryPolygons.GetSize()) validMask->setAll(1.0f);
	else
	{
		CXYZPolyLine *pBoundaryPolygon = this->BoundaryPolygons.getAt(0);
		CXYPolyLine transformedBoundary;
		CXYPoint p;

		for (int k = 0; k < pBoundaryPolygon->getPointCount(); ++k)
		{
			this->imageTrafo.transformObj2Obs(p, *(pBoundaryPolygon->getPoint(k)));
			transformedBoundary.addPoint(p);
		}

		if (!transformedBoundary.isClosed()) transformedBoundary.closeLine();

		validMask->fillByValue(transformedBoundary, 1);
	}

	return validMask;
};
 	 
//=============================================================================

bool MultiImageSegmenter::projectSegmentedImages()
{
	bool ret = true;
		
	CImageBuffer *validMask = this->getValidObjectMask();
	int size = (int) floor(this->bufferSize / this->imageTrafo.getScaleX() + 0.5);
	if (size > 1)
	{
		if (!(size % 2)) size++;
		CImageBuffer vM(*validMask);
		vM.setMargin(1,0.0f);
		CImageFilter morph(eMorphological, size, size);
		morph.binaryMorphologicalClose(vM, *validMask,1.0);
	}

	CCharString fn = this->outputPath + "valid.tif";
	validMask->exportToTiffFile(fn.GetChar(), 0, true);

	for (unsigned int imgInd = 0; imgInd < objectLabelVector.size(); ++imgInd)
	{
		CLabelImage  *objLab  = objectLabelVector[imgInd];
		CLabelImage  *imgLab  = imageLabelVector[imgInd];
		CSensorModel *model   = this->sensorModelVector[imgInd];
		CImageBuffer *occlMsk = 0;
		if (imgInd < this->occlusionMasks.size()) occlMsk = this->occlusionMasks[imgInd];


		vector<bool> validLabels(imgLab->getNLabels(), false);

		C3DPoint objPt;
		C2DPoint imgPt;
		C2DPoint imgProjPt;
		float h;

		for (int r = 0; r < objLab->getHeight(); ++r)
		{
			unsigned long u = r * objLab->getDiffY();
			for (int c = 0; c < objLab->getWidth(); ++c, ++u)
			{
				bool use = true;
				if (occlMsk)
				{
					if (!occlMsk->pixUChar(u)) use = false;
				}

				if (!use) objLab->pixUShort(u) = 0;
				else
				{
					imgPt.y = r;
					imgPt.x = c;
					this->imageTrafo.transformObs2Obj(objPt, imgPt, this->pmax.z); 
					if (this->pDSM)
					{
						this->DSMtrafo.transformObj2Obs(imgPt, objPt);
						imgPt.x -= this->pDSM->getOffsetX();
						imgPt.y -= this->pDSM->getOffsetY();
						if (this->pDSM->getInterpolatedColourVecBilin(imgPt.y, imgPt.x, &h))
						{
							objPt.z = (double) h;
						}
					}

					model->transformObj2Obs(imgProjPt, objPt);
					imgProjPt.x = floor(imgProjPt.x - double(imgLab->getOffsetX()) + 0.5);
					imgProjPt.y = floor(imgProjPt.y - double(imgLab->getOffsetY()) + 0.5);

					if ((imgProjPt.x > 1) && (imgProjPt.x + 1 < imgLab->getWidth()) && 
						(imgProjPt.y > 1) && (imgProjPt.y + 1 < imgLab->getHeight()))
					{
						unsigned long index = (unsigned long) imgLab->getIndex((long) imgProjPt.y, (long) imgProjPt.x);
						unsigned short l = imgLab->pixUShort(index);
						objLab->pixUShort(u) = l;
						if (validMask->pixUChar(u) > 0) validLabels[l] = true;
					}
				}
			}
		}

		objLab->initPixelNumbers();

		for (unsigned int i = 1; i < validLabels.size(); ++i)
		{
			if (!validLabels[i])
			{
				if (i < objLab->getNLabels()) objLab->removeLabel(i, false);
				if (i < imgLab->getNLabels()) imgLab->removeLabel(i, false);
			}
		}

		objLab->removeEmptyLabels(true);
		imgLab->removeEmptyLabels(true);
		objLab->updateVoronoi();
		imgLab->updateVoronoi();
	}

	delete validMask;


	return ret; 
};
	 
//=============================================================================

void MultiImageSegmenter::removePixelsWithoutOverlap()
{
	for (unsigned int i = 0; i < this->objectLabelVector[0]->getNGV(); ++i)
	{
		unsigned short nNotZero = 0;

		for (unsigned int j = 0; j < this->objectLabelVector.size(); ++j)
		{
			unsigned short label = this->objectLabelVector[j]->getLabel(i);
			if (label) ++nNotZero;
			else
			{
				if (this->objectLabelVector[j]->getDistance(i) <= 7) ++nNotZero;
			}
		}

		if (nNotZero < this->objectLabelVector.size())
		{
			for (unsigned int j = 0; j < this->objectLabelVector.size(); ++j)
			{
				this->objectLabelVector[j]->pixUShort(i) = 0;
			}
		}
	}
			
	for (unsigned int j = 0; j < this->objectLabelVector.size(); ++j)
	{
		this->objectLabelVector[j]->removeEmptyLabels(true);
	}
}

//=============================================================================

float MultiImageSegmenter::getDSMHeight(const C2DPoint &p) const
{
	C3DPoint obj(p.x, p.y, 0.0);
	C2DPoint obs;	
	this->DSMtrafo.transformObj2Obs(obs, obj);

	obs.x -= this->pDSM->getOffsetX();
	obs.y -= this->pDSM->getOffsetY();


	float z;
	this->pDSM->getInterpolatedColourVecBilin(obs.y, obs.x, &z);

	return z;
};
	  
//=============================================================================

float MultiImageSegmenter::getDSMHeight(const C3DPoint &p) const
{
	C2DPoint obs;	
	this->DSMtrafo.transformObj2Obs(obs, p);

	obs.x -= this->pDSM->getOffsetX();
	obs.y -= this->pDSM->getOffsetY();


	float z;
	this->pDSM->getInterpolatedColourVecBilin(obs.y, obs.x, &z);

	return z;
};

//=============================================================================
	  
bool MultiImageSegmenter::initOcclusionMasks(CImageBuffer *PDSM)
{
	bool ret = true;
	vector<float> minVec(1);
	vector<float> maxVec(1);
	PDSM->getMinMax(0,minVec, maxVec);
	float zMax = maxVec[0];

	for (unsigned int i = 0; ret && (i < this->objectLabelVector.size()); ++i)
	{
		CLabelImage *l = objectLabelVector[i];
		CImageBuffer *b = new CImageBuffer(l->getOffsetX(), l->getOffsetY(), l->getWidth(), l->getHeight(), 1,1,true,1.0f);
		if (!b) ret = false;
		else this->occlusionMasks.push_back(b);
	}

	if (!ret)
	{
		for (unsigned int i = 0; i < this->occlusionMasks.size(); ++i)
		{
			delete this->occlusionMasks[i];
			this->occlusionMasks[i] = 0;
		}
		this->occlusionMasks.clear();
	}
	else
	{
		for (unsigned int i = 0; i < this->occlusionMasks.size(); ++i)
		{
			CImageBuffer *b = this->occlusionMasks[i];
			CSensorModel *m = this->sensorModelVector[i];
			C2DPoint imgPt1, imgPt2, imgProjPt;
			C3DPoint objPt1, objPt2;
			float h;

			for (int r = 0; r < b->getHeight(); ++r)
			{
				unsigned long u = r * b->getDiffY();
				for (int c = 0; c < b->getWidth(); ++c, ++u)
				{
					imgPt1.y = r;
					imgPt1.x = c;
					this->imageTrafo.transformObs2Obj(objPt1, imgPt1, zMax); 
					this->DSMtrafo.transformObj2Obs(imgPt1, objPt1);
					imgPt1.x -= PDSM->getOffsetX();
					imgPt1.y -= PDSM->getOffsetY();
					if (PDSM->getInterpolatedColourVecBilin(imgPt1.y, imgPt1.x, &h))
					{
						objPt1.z = (double) h;
					}
					
					m->transformObj2Obs(imgProjPt, objPt1);
					m->transformObs2Obj(objPt2, imgProjPt, zMax);
					this->DSMtrafo.transformObj2Obs(imgPt2, objPt2);
					imgPt2.x -= PDSM->getOffsetX();
					imgPt2.y -= PDSM->getOffsetY();

					if (imgPt2.x < 0)
					{
						double fac = -imgPt1.x / (imgPt2.x - imgPt1.x);
						imgPt2.x = 0.0;
						imgPt2.y = imgPt1.y + fac * (imgPt2.y - imgPt1.y);
						objPt2.z = objPt1.z + fac * (objPt2.z - objPt1.z);
					}
					if (imgPt2.y < 0)
					{
						double fac = -imgPt1.y / (imgPt2.y - imgPt1.y);
						imgPt2.x = imgPt1.x + fac * (imgPt2.x - imgPt1.x);
						imgPt2.y = 0.0;
						objPt2.z = objPt1.z + fac * (objPt2.z - objPt1.z);
					}
					if (imgPt2.x > (b->getWidth() - 1))
					{
						double fac = (b->getWidth()-imgPt1.x) / (imgPt2.x - imgPt1.x);
						imgPt2.x = b->getWidth() - 1;
						imgPt2.y = imgPt1.y + fac * (imgPt2.y - imgPt1.y);
						objPt2.z = objPt1.z + fac * (objPt2.z - objPt1.z);
					}
					if (imgPt2.y > (b->getHeight() - 1))
					{
						double fac = (b->getHeight()-1-imgPt1.y) / (imgPt2.y - imgPt1.y);
						imgPt2.x = imgPt1.x + fac * (imgPt2.x - imgPt1.x);
						imgPt2.y = b->getHeight() - 1;
						objPt2.z = objPt1.z + fac * (objPt2.z - objPt1.z);
					}

					C2DPoint diff = imgPt2 - imgPt1;
					double dz = objPt2.z - objPt1.z;
					unsigned char bw = 255;

					if ((fabs(diff.x) < 1.0) && (fabs(diff.y) < 1.0))
					{
						if ((fabs(diff.x) > FLT_EPSILON) || (fabs(diff.y) > FLT_EPSILON))
						{
							if (!PDSM->getInterpolatedColourVecBilin(imgPt2.y, imgPt2.x, &h)) bw = 0;
							else if (h > objPt2.z) bw = 0;
						}
					}
					else
					{
						dz /= diff.getNorm();
						diff.normalise();
						int nMax;
						if (fabs(diff.x) > fabs(diff.y)) nMax = abs(int(floor((imgPt2.x - imgPt1.x) / diff.x) + 1));
						else nMax = abs(int(floor((imgPt2.y - imgPt1.y) / diff.y) + 1));
						imgPt1 += diff;
						objPt1.z += dz;
						for (int n = 1; (n < nMax) && (bw > 0); n++, imgPt1 += diff, objPt1.z += dz)
						{
							if (!PDSM->getInterpolatedColourVecBilin(imgPt1.y, imgPt1.x, &h)) bw = 0;
							else 
							{
								h -= (float) objPt1.z;
								if (h > 0.15f) bw = 0;
							}
						}

					};

					b->pixUChar(u) = bw;
				}
			}
		}
	}

	return ret;
};

//=============================================================================

bool MultiImageSegmenter::intersectLabels(int l)
{
	ostrstream oss;
	oss << l << ends;
	char *z = oss.str();
	CCharString str(z);
	delete [] z;

	CLabelImage combLabel(*this->pCombinedLabels);
	CLabelMatcher matcher(0.0f);
	bool ret = matcher.init(&combLabel, this->objectLabelVector[l], 3);

	if (ret)
	{
		matcher.determineOverlaps(0.1f, 0.5f, 0.9f,  3);
		matcher.getCombinedLabelImage(*this->pCombinedLabels);
		matcher.writeData(this->outputPath + "labelOverlap" + str + ".txt");
		this->pCombinedLabels->dump(this->outputPath.GetChar(), "combined", str.GetChar());
	}

	return ret;
};
	  
//=============================================================================

bool MultiImageSegmenter::intersectLabels()
{
	bool ret = true;

	if (this->objectLabelVector.size() < 2) ret = false;
	else
	{
		*pCombinedLabels = *this->objectLabelVector[0];

		for (unsigned int i = 1; i < this->objectLabelVector.size() && ret; ++i)
		{
			ret = this->intersectLabels(i);
		}
	}
	
	return ret;
};

//=============================================================================

void MultiImageSegmenter::morphologicalFilterObjectLabel(unsigned short label, int size) const
{
	CImageBuffer mask;

	this->pCombinedLabels->getMaskForLabel(mask, label, 1, false);
	
	CImageFilter filter (eMorphological, size, size);
	CImageBuffer mskHelp(mask);
	mskHelp.setMargin(1, 0.0f);

	filter.binaryMorphologicalOpen(mskHelp, mask, 1);
	
	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{
		if (mask.pixUChar(u)) mskHelp.pixUChar(u) = 0;
		else mskHelp.pixUChar(u) = 1;
	}

	mskHelp.setMargin(1, 0.0f);

	filter.binaryMorphologicalOpen(mskHelp, mask, 1);

	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{
		if (mask.pixUChar(u)) mask.pixUChar(u) = 0;
		else mask.pixUChar(u) = 1;
	}

	mask.setMargin(3, 0.0f);

	CLabelImage LabelsMask(mask, 1.0f, 8);

	unsigned short lMax = 1;
	unsigned long nMax = LabelsMask.getNPixels(1);
	for (unsigned long k = 2; k < LabelsMask.getNLabels(); ++ k)
	{
		if (LabelsMask.getNPixels(k) > nMax)
		{
			lMax = (unsigned short) k;
			nMax = LabelsMask.getNPixels(k);
		}
	};
	
	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{			
		if (LabelsMask.getLabel(u) == lMax)  this->pCombinedLabels->pixUShort(u) = label;
		else if (this->pCombinedLabels->pixUShort(u) == label) this->pCombinedLabels->pixUShort(u) = 0;
	}
};

//=============================================================================

bool MultiImageSegmenter::segment()
{
	bool ret = this->isInitialised();

	if (ret) ret = this->segmentImages();

	if (ret)
	{
		ret = this->projectSegmentedImages();

		for (unsigned int i = 0; i < this->objectLabelVector.size(); ++i)
		{
			this->objectLabelVector[i]->morphologicalOpen(2,1);
		}

//		this->removePixelsWithoutOverlap();
//		if (ret) ret = intersectLabels();
//		this->exportObjectLabelBufferToTiff();
	}

	return ret; 
};

//=============================================================================
