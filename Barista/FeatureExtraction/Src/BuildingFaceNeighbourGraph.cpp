
/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include "BuildingFaceNeighbourGraph.h"
#include "BuildingFacePoly.h"
#include "BuildingFace.h"
#include "LabelImage.h"
#include "ProtocolHandler.h"
#include "XYZPolyLine.h"

//==================================================================================

CBuildingFaceNeighbourGraph::CBuildingFaceNeighbourGraph(const CTFW &Trafo) : 
     pLabelImage(0), pFaceVector(0), pNeighbourVector(0), trafo(Trafo)
{
	this->pFaceVector = new vector<CBuildingFace *>;
};

//==================================================================================

CBuildingFaceNeighbourGraph::~CBuildingFaceNeighbourGraph()
{
	this->clearAll();
};
	
//==================================================================================
	  
bool CBuildingFaceNeighbourGraph::initialiseNeighbours(CLabelImage *plabelimage)
{
	bool ret = true;		
	this->clearNeighbours();

	if ((!plabelimage) || (!this->pFaceVector)) ret = false;
	else
	{
		this->pLabelImage   = plabelimage;
		double maxDistForComparison = this->trafo.getScaleX() * 2.0;
		this->pLabelImage->resetVoronoiByDist(10);
	
		this->pNeighbourVector = new CBuildingFaceNeighbourVector(maxDistForComparison);	
		if (!this->pNeighbourVector) ret = false;
		else
		{
			for (unsigned short i = 1; i < this->pFaceVector->size(); ++i)
			{
				this->getFace(i)->initPolys(*this->pNeighbourVector, *this->pLabelImage, true);
			}
		}

		if (!ret) this->clearNeighbours();
	}

	return ret;
};

//==================================================================================

void CBuildingFaceNeighbourGraph::clearNeighbours()
{
	if (this->pNeighbourVector) delete this->pNeighbourVector;
	this->pNeighbourVector = 0;
};
		  
//==================================================================================

void CBuildingFaceNeighbourGraph::clearFaces()
{
	if (this->pFaceVector) 
	{
		for (unsigned int i = 0; i < this->pFaceVector->size(); ++i)
		{
			delete (*(this->pFaceVector))[i];
			(*(this->pFaceVector))[i] = 0;
		}

		delete this->pFaceVector;
		this->pFaceVector = 0;
	}	
};
		  	  
//==================================================================================

void CBuildingFaceNeighbourGraph::clearAll()
{
	this->clearNeighbours();
	this->clearFaces();
	this->pLabelImage = 0;
};

//==================================================================================

void CBuildingFaceNeighbourGraph::exportAll(const char *filenameBase) 
{
	CCharString pathBase = protHandler.getDefaultDirectory() + filenameBase;
	this->exportPlanes(pathBase + "planes.tif");

	this->pLabelImage->dump(protHandler.getDefaultDirectory(), filenameBase);

	CCharString fn = pathBase + "neighbours.txt";
	this->exportNeighbourhoods(fn.GetChar());

	fn = pathBase + "neighbours.dxf";
	this->exportPolygons(fn.GetChar(), true, true, true);
}; 

//============================================================================

void CBuildingFaceNeighbourGraph::classifyNeighbours(float minLenghtRatio, float minLength, float delta)
{
	double pixS = this->trafo.getScaleX();
	float sig = 3.0f * (float) pixS;
	float maxDist = 2.5f * (float) pixS;

	for (int n = 0; n < this->getNNeighbours(); ++n)
	{	 
		CBuildingFaceNeighbourRecord *pNeighbour = this->getNeighbour(n);
		if (pNeighbour->initIntersection() != eIntersection)
		{
			pNeighbour->setType(eSTEPEDGE);
			ostrstream oss;
			pNeighbour->print(oss);
			protHandler.print(oss, protHandler.prt);
		}
		else
		{
			pNeighbour->classify(this->pNeighbourVector, minLenghtRatio, minLength, delta, maxDist, sig);
			ostrstream oss;
			pNeighbour->print(oss);
			protHandler.print(oss, protHandler.prt);
		}
	}
}

//============================================================================
	  
void CBuildingFaceNeighbourGraph::combineNeighbourPolygons()
{
	for (unsigned int i = 0; i < this->pFaceVector->size(); i++)
	{
		CBuildingFace *face = (*this->pFaceVector)[i];
		C3DPoint red = face->getReduction();

		for (unsigned int j = 0; j < face->getNPolys(); ++j)
		{
			face->getPoly(j)->combineNeighbours();
			CXYZPolyLine *pPoly = face->getPoly(j)->getCombinedPoly();
			if (!pPoly->isClosed()) pPoly->closeLine();
			
			for (int k = 0; k < pPoly->getPointCount(); ++k)
			{
				pPoly->getPoint(k)->z = face->getHeight(pPoly->getPoint(k)->x + red.x, pPoly->getPoint(k)->y + red.y) - red.z;
			};		
		};
	};
};

//============================================================================

void CBuildingFaceNeighbourGraph::exportPlanes(const char *filename) const
{
	if (this->pFaceVector)
	{	
		ofstream regionFile(filename);

		int nReg = 0;
		for (unsigned int k = 1; k < this->pFaceVector->size(); ++k)
		{
			CBuildingFace *face = this->getFace(k);
			if (face) face->print(regionFile);
		}
	}
}; 

//==================================================================================

void CBuildingFaceNeighbourGraph::exportPolygons(const char *filename, bool exportApprox, 
												 bool exportInitial, bool exportFinal) const 
{
	if (this->pNeighbourVector)
	{
		this->pNeighbourVector->exportDXF(filename, exportInitial, exportApprox, exportFinal);
	}
}; 
  
//============================================================================

void CBuildingFaceNeighbourGraph::exportNeighbourhoods(const char *filename) const
{
	if (this->pNeighbourVector)
	{
		ofstream of(filename);
		this->pNeighbourVector->print(of);
	}
}; 

//==================================================================================

