#include <float.h>
#include <limits.h>
#include <strstream>
#include "DenseMatcher.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"
#include "ImgBuf.h"
#include "utilities.h"
#include "sortVector.h"
#include "filter.h"
#include "3DPoint.h"
#include "SensorModel.h"
#include "GenerateEpipolar.h"
#include "HugeImage.h"
#include "GenericPointCloud.h"
#include "LabelImage.h"

#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
using namespace cv;


//********************************************************************************
//********************************************************************************
//********************************************************************************
//********************************************************************************

CSGMParameters::CSGMParameters(): preFilterCap(63), SADWindowSize(3), fullDP(false), 
								  uniquenessRatio(10), speckleWindowSize(100), 
								  speckleRange(32), disp12MaxDiff(1), 
								  minDisparity(0), numberOfDisparities(50), 
								  P1(8), P2(32)
{
}
	
//================================================================================

CSGMParameters::CSGMParameters(const CSGMParameters &pars): preFilterCap(pars.preFilterCap), 
                                                            SADWindowSize(pars.SADWindowSize), 
														    fullDP(pars.fullDP), 
														    uniquenessRatio(pars.uniquenessRatio), 
														    speckleWindowSize(pars.speckleWindowSize), 
														    speckleRange(pars.speckleRange), 
														    disp12MaxDiff(pars.disp12MaxDiff), 
														    minDisparity(pars.minDisparity), 
														    numberOfDisparities(pars.numberOfDisparities), 
														    P1(pars.P1), P2(pars.P2)														   
{
};
	
//================================================================================

CCharString CSGMParameters::getDescriptor() const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);

	oss << "\nSGM Parameters:\n==============\n\n   minDisparity: " << minDisparity 
		<< "\n   numberOfDisparities: " << numberOfDisparities << "\n   SADWindowSize: " 
		<<  SADWindowSize << "\n   preFilterCap: " << preFilterCap << "\n   uniquenessRatio: "
		<< uniquenessRatio << "\n   P1: " << P1 << "\n   P2: " << P2 << "\n   speckleWindowSize: "
		<< speckleWindowSize << "\n   speckleRange: " << speckleRange << "\n   disp12MaxDiff: "
		<< disp12MaxDiff << "\n   fullDP: " << (int) fullDP << ends; 

	char*z = oss.str();
	CCharString zS (z);
	delete [] z;

	return zS;
}; 
	  
//================================================================================

void CSGMParameters::saveToFile(const CCharString &fn) const
{
	ofstream saveFile(fn.GetChar());
	saveFile.setf(ios::fixed, ios::floatfield);

	saveFile << "MinDisparity("        << this->minDisparity        << ")\n"
		     << "numberOfDisparities(" << this->numberOfDisparities << ")\n"
		     << "SADWindowSize("       << this->SADWindowSize       << ")\n"
		     << "preFilterCap("        << this->preFilterCap        << ")\n"
		     << "uniquenessRatio("     << this->uniquenessRatio     << ")\n"
		     << "speckleWindowSize("   << this->speckleWindowSize   << ")\n"
		     << "speckleRange("        << this->speckleRange        << ")\n"
		     << "disp12MaxDiff("       << this->disp12MaxDiff       << ")\n"
		     << "P1("                  << this->P1                  << ")\n"
		     << "P2("                  << this->P2                  << ")\n"
		     << "fullDP("              << int(this->fullDP)         << ")\n";
};

//================================================================================

bool CSGMParameters::initFromFile(const CCharString &fn) 
{
	ifstream inFile(fn.GetChar());
	int len = 2048;
	char *z = new char[len];
	bool ret = true;
	if (inFile.good())
	{
		inFile.getline(z, len);

		while (inFile.good())
		{
			CCharString arg(z);
			int pos1 = arg.Find("(");
			int pos2 = arg.Find(")");
			if ((pos1 > 0) && (pos2 > pos1))
			{
				CCharString key = arg.Left(pos1);
				arg = arg.Left(pos2);
				arg = arg.Right(arg.GetLength() - pos1 - 1);
				istrstream iss(arg.GetChar());
				int intArg;
				iss >> intArg;
					
				if (key == "MinDisparity")             this->minDisparity = intArg;
				else if (key == "numberOfDisparities") this->numberOfDisparities = intArg;
				else if (key == "SADWindowSize")       this->SADWindowSize = intArg;
				else if (key == "preFilterCap")        this->preFilterCap = intArg;
				else if (key == "uniquenessRatio")     this->uniquenessRatio = intArg;
				else if (key == "speckleWindowSize")   this->speckleWindowSize = intArg;
				else if (key == "speckleRange")        this->speckleRange = intArg;
				else if (key == "disp12MaxDiff")       this->disp12MaxDiff = intArg;
				else if (key == "P1")                  this->P1 = intArg;
				else if (key == "P2")                  this->P2 = intArg;
				else if (key == "fullDP")              this->fullDP = (intArg > 0);
			}
			inFile.getline(z, len);
		}
	}
	else ret = false;

	delete [] z;

	return ret;
};

//********************************************************************************
//********************************************************************************
//********************************************************************************
//********************************************************************************

CDenseMatcher::CDenseMatcher(CHugeImage *Image1, CHugeImage *Image2, 
		                     CFrameCameraModel *model1, CFrameCameraModel *model2, 
							 const C3DPoint &pmin, const C3DPoint &pmax, 
							 const CSGMParameters &pars, const C3DPoint &sigma, int sizeOutlierArea): 
		pImageFile1(Image1), pImageFile2(Image2), pModel1(model1), pModel2(model2), epiBuf1(0), epiBuf2(0), parallaxBuf(0),
		Pmin(pmin), Pmax(pmax), Sigma(sigma), SGBMpars(pars), epiPolarGenerator(0), parallaxThreshold(0.0f), 
		alpha1(1.0f), alpha2(1.0f), offset1(0), offset2(0), pixelOutliers(sizeOutlierArea)
{
	this->listener = NULL;

	this->epiPolarGenerator = new CGenerateEpipolar(this->pImageFile1, this->pImageFile2, this->pModel1, this->pModel2);

	this->epiBuf1 = new CImageBuffer;
	this->epiBuf2 = new CImageBuffer;
	if (!this->epiPolarGenerator->generateEpipolar(this->Pmin, this->Pmax,*this->epiBuf1, this->frameCamEpi1, *this->epiBuf2, this->frameCamEpi2, true, true))
	{	
		this->clearFrameCam(this->frameCamEpi1);
		this->frameCamEpi2.init("c", this->frameCamEpi2.getCameraPosition(), 0, 0, 0);
		this->clearBuffers();
	}
	else
	{
		C2DPoint pLow1, pLow2, pHigh1, pHigh2;
		C3DPoint P = (this->Pmax + this->Pmin) * 0.5;
		P.z = this->Pmin.z;
		this->frameCamEpi1.transformObj2Obs(pLow1, P);
		this->frameCamEpi2.transformObj2Obs(pLow2, P);
		double c = this->frameCamEpi1.getCamera()->getIRP().z;
		double B = this->frameCamEpi1.getCameraPosition().distanceFrom(this->frameCamEpi2.getCameraPosition());
		double H = this->frameCamEpi1.getCameraPosition().z - this->Pmin.z;

		this->parallaxThreshold = float(1.0 * c * B / (H * H)); 

		pLow1.x -= double(this->epiBuf1->getOffsetX());
		pLow1.y -= double(this->epiBuf1->getOffsetY());
		pLow2.x -= double(this->epiBuf2->getOffsetX());
		pLow2.y -= double(this->epiBuf2->getOffsetY());
		P.z = this->Pmax.z;
		this->frameCamEpi1.transformObj2Obs(pHigh1, P);
		this->frameCamEpi2.transformObj2Obs(pHigh2, P);
		pHigh1.x -= double(this->epiBuf1->getOffsetX());
		pHigh1.y -= double(this->epiBuf1->getOffsetY());
		pHigh2.x -= double(this->epiBuf2->getOffsetX());
		pHigh2.y -= double(this->epiBuf2->getOffsetY());

		double dparallaxLow  = pLow1.x  - pLow2.x;
		double dparallaxHigh = pHigh1.x - pHigh2.x;
		this->SGBMpars.minDisparity = int (floor(dparallaxLow / 16.0) * 16);
		this->SGBMpars.numberOfDisparities = int (floor(dparallaxHigh / 16.0 + 1.0) * 16) - this->SGBMpars.minDisparity;

		this->parallaxBuf = new CImageBuffer(this->epiBuf1->getOffsetX(), this->epiBuf1->getOffsetY(),
			                                 this->epiBuf1->getWidth(), this->epiBuf1->getHeight(), 
											 1, 32, false);
	}
}

//================================================================================

CDenseMatcher::~CDenseMatcher()
{
	this->pImageFile1 = 0;
	this->pImageFile2 = 0;
	this->pModel1     = 0;
	this->pModel2     = 0;
	
	if (this->epiPolarGenerator) delete this->epiPolarGenerator;
	this->epiPolarGenerator = 0;

	this->clearFrameCam(this->frameCamEpi1);
	this->frameCamEpi2.init("c", this->frameCamEpi2.getCameraPosition(), 0, 0, 0);
	
	this->clearBuffers();
}

//================================================================================

void CDenseMatcher::clearFrameCam(CFrameCameraModel &cam)
{
	if (cam.getCamera())         delete cam.getCamera();
	if (cam.getCameraMounting()) delete cam.getCameraMounting();
	cam.init("c", cam.getCameraPosition(), 0, 0, 0);
};

//================================================================================
 
void CDenseMatcher::clearBuffers()
{
	if (this->epiBuf1)     delete this->epiBuf1;
	if (this->epiBuf2)     delete this->epiBuf2;
	if (this->parallaxBuf) delete this->parallaxBuf;

	this->epiBuf1     = 0;
	this->epiBuf2     = 0;
	this->parallaxBuf = 0;
};
//================================================================================
		 
bool CDenseMatcher::isInitialised() const 
{ 
	return (this->epiBuf1 != 0); 
}

//================================================================================

void CDenseMatcher::initAlphaOffset()
{
	this->alpha1  = 1.0f;
	this->offset1 = 0;
	this->alpha2  = 1.0f;
	this->offset2 = 0;
	
	if (this->epiBuf1->getBpp() > 8)
	{
		unsigned short maxGV;
		maxGV = this->offset1 = this->epiBuf1->pixUShort(0);
		for (unsigned int i = 0; i < this->epiBuf1->getNGV(); ++i)
		{
			unsigned short pix = this->epiBuf1->pixUShort(i);
			if (pix > 0)
			{
				if (pix < this->offset1) this->offset1 = pix;
				if (pix > maxGV)         maxGV = pix;
			}
		}
		this->alpha1  = 255.0f / float(maxGV - this->offset1);
	}

	if (this->epiBuf2->getBpp() > 8)
	{
		unsigned short maxGV;
		maxGV = this->offset2 = this->epiBuf2->pixUShort(0);

		for (unsigned int i = 0; i < this->epiBuf2->getNGV(); ++i)
		{
			unsigned short pix = this->epiBuf2->pixUShort(i);
			if (pix > 0)
			{
				if (pix < this->offset2) this->offset2 = pix;
				if (pix > maxGV)         maxGV = pix;
			}
		}
		this->alpha2  = 255.0f / float(maxGV - this->offset2);		
	}
};

//================================================================================

bool CDenseMatcher::match()
{
	bool success = true;

	StereoSGBM sgbm;
	sgbm.preFilterCap        = this->SGBMpars.preFilterCap;
	sgbm.SADWindowSize       = this->SGBMpars.SADWindowSize;
	sgbm.P1                  = this->SGBMpars.P1;
	sgbm.P2                  = this->SGBMpars.P2;
	sgbm.minDisparity        = this->SGBMpars.minDisparity;
	sgbm.numberOfDisparities = this->SGBMpars.numberOfDisparities;
	sgbm.uniquenessRatio     = this->SGBMpars.uniquenessRatio;
	sgbm.speckleWindowSize   = this->SGBMpars.speckleWindowSize;
	sgbm.speckleRange	     = this->SGBMpars.speckleRange;
	sgbm.disp12MaxDiff       = this->SGBMpars.disp12MaxDiff;
	sgbm.fullDP              = this->SGBMpars.fullDP;

	int imageType    = CV_8UC1;
	int imageTypeSGM = CV_8UC1;

	if (this->epiBuf1->getBpp() == 8)
	{
		if (this->epiBuf1->getBands() > 1) 
		{
			imageType    = CV_8UC3;
			imageTypeSGM = CV_8UC3;
		}

	}
	else 
	{
		imageType    = CV_8UC1;
		imageTypeSGM = CV_8UC1;
		if (this->epiBuf1->getBands() > 1) 
		{
			imageType    = CV_8UC3;
			imageTypeSGM = CV_8UC3;
		}
	}

	Mat img1(this->epiBuf1->getHeight(), this->epiBuf1->getWidth(), imageType);
	Mat img2(this->epiBuf2->getHeight(), this->epiBuf2->getWidth(), imageType);

	int bnd = (this->epiBuf1->getBands() == 1)? 1:3;

	if (this->epiBuf1->getBpp() == 8)
	{
		for (int r = 0; r < this->epiBuf1->getHeight(); ++r)
		{
			long d0 =  r * this->epiBuf1->getDiffY();
			unsigned char *rowR1 = img1.ptr<unsigned char>(r);
			unsigned char *rowR2 = img2.ptr<unsigned char>(r);
			unsigned char *r1O = &(this->epiBuf1->getPixUC()[d0]);
			unsigned char *r2O = &(this->epiBuf2->getPixUC()[d0]);
			long nMax = this->epiBuf1->getDiffY();
			for(int newIdx = 0, oldIdx = 0; oldIdx < nMax; oldIdx += this->epiBuf1->getDiffX())
			{
				for (int idx = 0; idx < bnd; ++idx, ++newIdx)
				{
					int i = oldIdx + idx;
					rowR1[newIdx] = r1O[i];
					rowR2[newIdx] = r2O[i];
				}
			}
		}
	}
	else 
	{
		this->initAlphaOffset();

		register unsigned char g1, g2;

		for (int r = 0; r < this->epiBuf1->getHeight(); ++r)
		{
			long d0 =  r * this->epiBuf1->getDiffY();
			unsigned char *rowR1 = img1.ptr<unsigned char>(r);
			unsigned char *rowR2 = img2.ptr<unsigned char>(r);
			unsigned short *r1O = &(this->epiBuf1->getPixUS()[d0]);
			unsigned short *r2O = &(this->epiBuf2->getPixUS()[d0]);
			long nMax = this->epiBuf1->getDiffY();
			for(int newIdx = 0, oldIdx = 0; oldIdx < nMax; oldIdx += this->epiBuf1->getDiffX())
			{
				for (int idx = 0; idx < bnd; ++idx, ++newIdx)
				{
					int i = oldIdx + idx;
					if (r1O[i] == 0) g1 = 0;
					else
					{
						g1 = (unsigned char) floor(float(r1O[i] - this->offset1) * this->alpha1);
						if (g1 == 0) g1 = 1;
					}

					if (r2O[i] == 0) g2 = 0;
					else
					{
						g2 = (unsigned char) floor(float(r2O[i] - this->offset2) * this->alpha2);
						if (g2 == 0) g2 = 1;
					}
					rowR1[newIdx] = g1;
					rowR2[newIdx] = g2;
				}
			}
		}
	}

	Mat disp;

	sgbm(img1, img2, disp);

	img1.convertTo(img1, CV_8UC1);
	img2.convertTo(img2, CV_8UC1);

	float fac = 1.0f / 16.0f;

	float minD = (float) this->SGBMpars.minDisparity - 1;
	for (int r = 0; r < this->parallaxBuf->getHeight(); ++r)
	{
		long d0 =  r * this->parallaxBuf->getDiffY();

		short *rowR1 = disp.ptr<short>(r);
		unsigned char *rowI1 = img1.ptr<unsigned char>(r);
		unsigned char *rowI2 = img2.ptr<unsigned char>(r);

		float *r1O = &(this->parallaxBuf->getPixF()[d0]);

		for(int c = 0; c < this->parallaxBuf->getWidth(); c++)
		{				
			if ((rowI1[c] > 0) && (rowI2[c] > 0))
			{
				r1O[c] = float(rowR1[c]) * fac;
				if (r1O[c] >= this->SGBMpars.minDisparity)
				{
					int i2 = c + int(floor(r1O[c] + 0.5f));
					if ((i2 >= 0) && (i2 < this->parallaxBuf->getWidth()))
						if (rowI2[i2] <= 0) r1O[c] = minD;
				}
			}
			else r1O[c] = minD;

		}
	}

	if (this->pixelOutliers) this->postProcessParallaxes();
	return success;
}
	  
//================================================================================
	  
void CDenseMatcher::postProcessParallaxes()
{
	CImageBuffer mask(0, 0, this->parallaxBuf->getWidth(), this->parallaxBuf->getHeight(),1, 1, true, 0);

	CImageBuffer mask1(mask);

	int rmax =  this->parallaxBuf->getHeight() - 1;
	int cmax =  this->parallaxBuf->getWidth()  - 1;

	if (this->parallaxThreshold > 1) this->parallaxThreshold = 1.0;

	for (int r = 1; r < rmax; ++r)
	{
		long d0 =  r * this->parallaxBuf->getDiffY();

		for(int c = 1; c < cmax; c++)
		{			
			int i = d0 + c;
			float disp = this->parallaxBuf->pixFlt(i);
			if (disp >= this->SGBMpars.minDisparity)
			{
				float dx   = this->parallaxBuf->pixFlt(i - this->parallaxBuf->getDiffX());
				float dy   = this->parallaxBuf->pixFlt(i - this->parallaxBuf->getDiffY());
				if ((dx >= this->SGBMpars.minDisparity) && (dy >= this->SGBMpars.minDisparity))
				{
					dx = fabs(dx - disp); 
					dy = fabs(dy - disp); ;
					if (dy  > dx) dx = dy;

					if (dx <= this->parallaxThreshold) mask.pixUChar(i) = 255;
					else mask1.pixUChar(i) = 255;
				}
				else mask1.pixUChar(i) = 255;
			}
		}
	}

	CImageBuffer *mb = new CImageBuffer(mask);

	CImageFilter fil(eMorphological, 3,3);

	fil.binaryMorphologicalOpen(*mb, mask, 0.0);

	delete mb; 

	CLabelImage labels(mask, 1.0, 4);

	vector<float> diffDisp(4);
	vector<int>   idxDisp(4);
	
	for (unsigned long i = 0; i < this->parallaxBuf->getNGV(); ++i)
	{
		if (mask1.pixUChar(i) > 0)
		{
			float disp  = this->parallaxBuf->pixFlt(i);
			idxDisp[0]  = i - this->parallaxBuf->getDiffX();
			idxDisp[1]  = i - this->parallaxBuf->getDiffY();
			idxDisp[2]  = i + this->parallaxBuf->getDiffX();
			idxDisp[3]  = i + this->parallaxBuf->getDiffY();
			diffDisp[0] = this->parallaxBuf->pixFlt(idxDisp[0]);
			diffDisp[1] = this->parallaxBuf->pixFlt(idxDisp[1]);
			diffDisp[2] = this->parallaxBuf->pixFlt(idxDisp[2]);
			diffDisp[3] = this->parallaxBuf->pixFlt(idxDisp[3]);
			float minD = FLT_MAX;
			int minIdx = -1;

			for (unsigned int k = 0; k < diffDisp.size(); ++k)
			{
				if (diffDisp[k] >= this->SGBMpars.minDisparity) 
				{
					float D = (float) fabs(diffDisp[k] - disp);
					if ((D < minD) && (labels.getLabel(idxDisp[k])))
					{
						minD = D;
						minIdx = k;
					}
				}
			}

			if ((minIdx >= 0) && (minD <= this->parallaxThreshold)) 
			{
				labels.pixUShort(i) = labels.getLabel(idxDisp[minIdx]);
				mask1.pixUChar(i) = 0;
			}
		}
	}
	
	float minDisp = (float) this->SGBMpars.minDisparity - 1;
	for (unsigned long i = 0; i < this->parallaxBuf->getNGV(); ++i)
	{
		if (mask1.pixUChar(i) > 0)
		{
			this->parallaxBuf->pixFlt(i) = minDisp;
		}
	}

	labels.renumberLabels();
	labels.computePixelNumbers();

	labels.dump(protHandler.getDefaultDirectory().GetChar(), "parallaxes1");   

	labels.updateVoronoi();

	vector<bool> labelsToRemove(labels.getNLabels() + 1, false);

	for (unsigned int i = 1; i < labels.getNLabels(); ++i)
	{
		if (labels.getNPixels(i) < this->pixelOutliers)
		{
			labelsToRemove[i] = true;
		}
	}

	labelsToRemove[0] = true;

	float disp = float (this->SGBMpars.minDisparity - 1);

	for (unsigned int i = 0; i < this->parallaxBuf->getNGV(); ++i)
	{
		unsigned short l = labels.getVoronoi(i);
		
		if (labelsToRemove[l])
		{
			this->parallaxBuf->pixFlt(i) = disp;
		}
	}
};

//================================================================================
	  
bool CDenseMatcher::getPointCloud(CGenericPointCloud **points, 
								  vector<unsigned char> &pointCount, 
								  vector<unsigned char*> &pointColour) 
{
	bool ret = true;

	int capacity = this->parallaxBuf->getWidth() * this->parallaxBuf->getHeight();

	C3DPoint ErrThreshold = this->Sigma; // * 1.95;

	C3DPoint Pmodel, Pobj;
	C2DPoint Pimage;
	C3DPoint IRP1 = this->frameCamEpi1.getCamera()->getIRP();
	C3DPoint IRP2 = this->frameCamEpi2.getCamera()->getIRP();
	CRotationMatrix rot = this->frameCamEpi1.getCameraRotation()->getRotMat();
	C3DPoint ERP1 = this->frameCamEpi1.getCameraPosition();
	
	double base = this->frameCamEpi1.getCameraPosition().distanceFrom(this->frameCamEpi2.getCameraPosition());
	double dIRP1x = double(this->epiBuf1->getOffsetX())- IRP1.x;
	double dIRP1y = double(this->epiBuf1->getOffsetY())- IRP1.y;

	double dIRP2x = IRP2.x - IRP1.x + double(this->epiBuf1->getOffsetX()- this->epiBuf2->getOffsetX());

	double parallax;

	double cB = IRP1.z * base;
	double oneByC = 1.0 / IRP1.z;

	CGenericPointCloud newPoints(capacity,3,3);

	vector<unsigned char *> newPointColour(this->epiBuf1->getBands());
	for (unsigned char b = 0; b < this->epiBuf1->getBands(); ++b)
	{
		newPointColour[b] = new unsigned char[capacity];
	}


	ANNkd_tree *kdTree = 0;
	if (*points) 
	{
		(*points)->setAnnDim(3);
		kdTree = (*points)->getKdTree();
	}

	ANNpointArray annQueryPoints = new ANNpoint [1];
	ANNidxArray   nnIdx   = new ANNidx[1];  // near neighbor indices
	ANNdistArray  nnDists = new ANNdist[1]; // near neighbor distances
	ANNpoint queryPt = annAllocPt(3);


	for (int r = 0; r < this->parallaxBuf->getHeight(); ++r)
	{
		long u0 = r * this->parallaxBuf->getDiffY();
		long imgIdx = r * this->epiBuf1->getDiffY();
		Pimage.y = double(r) + dIRP1y;
		
		for (int c = 0; c < this->parallaxBuf->getWidth(); ++c, imgIdx += epiBuf1->getDiffX())
		{
			double disp = double(this->parallaxBuf->pixFlt(u0 + c)); 
			if (disp >= this->SGBMpars.minDisparity)
			{
				Pimage.x = double(c) + dIRP1x;
				parallax = disp + dIRP2x; 
				Pmodel.z = -cB / parallax;
				double f = oneByC * Pmodel.z;
				Pmodel.x = -f * Pimage.x;
				Pmodel.y =  f * Pimage.y;
				rot.R_times_x(Pobj, Pmodel);
				Pobj += ERP1;
				if ((Pobj.x >= this->Pmin.x) && (Pobj.y >= this->Pmin.y) &&
					(Pobj.x <= this->Pmax.x) && (Pobj.y <= this->Pmax.y))
				{
					bool newPt = true;
					if (kdTree)
					{
						for (int k = 0; k < 3; ++k) queryPt[k] = Pobj[k];
						kdTree->annkSearch(queryPt, 1, nnIdx, nnDists, 0.0); 

						newPt = false;
						ANNpoint p = (*points)->getPt(nnIdx[0]);
						for (int k = 0; (k < 3) && (!newPt); ++k) 
						{
							if (fabs(queryPt[k] - p[k]) > ErrThreshold[k]) newPt = true;
						}
					}
					

					if (newPt)
					{
						for (unsigned short b = 0; b < this->epiBuf1->getBands(); ++b)
						{
							newPointColour[b][newPoints.getNpoints()] = this->epiBuf1->pixUChar(imgIdx + b);
						}

						newPoints.addPoint(Pobj);
					}
					else
					{
						int idx = nnIdx[0];
						ANNpoint p = (*points)->getPt(idx);
						double fac = 1.0 / double(pointCount[idx] + 1);
						for (int k = 0; k < 3; ++k) p[k] = (p[k] * double(pointCount[idx]) + Pobj[k]) * fac;
						
						for (int k = 0; k < this->epiBuf1->getBands(); ++k)
						{
							unsigned char newCol = this->epiBuf1->pixUChar(imgIdx + k);
							pointColour[k][idx] = (unsigned char)((float(pointColour[k][idx]) * float(pointCount[idx]) + float(newCol)) * float(fac));
						}

						++pointCount[idx];
					}
				}
			}
		}
	}

	delete [] annQueryPoints;
	delete [] nnIdx;
	delete [] nnDists;
	annDeallocPt(queryPt);

	pointCount.resize(pointCount.size() + newPoints.getNpoints(), 1);

	if (pointColour.size() < 1) 
	{
		pointColour = newPointColour;
	}
	else
	{
		vector<unsigned char*> oldPointColour = pointColour;		
		int delta = pointCount.size() - (*points)->getNpoints();

		for (unsigned char b = 0; b < this->epiBuf1->getBands(); ++b)
		{
			pointColour[b] = new unsigned char [pointCount.size()];
			memcpy(pointColour[b], oldPointColour[b], (*points)->getNpoints() * sizeof(unsigned char));
			delete [] oldPointColour[b];
			memcpy(&(pointColour[b][(*points)->getNpoints()]), newPointColour[b], delta * sizeof(unsigned char));
			delete [] newPointColour[b];
		}
	}

	if (!*points) 
	{
		*points = new CGenericPointCloud(capacity,3,3);
	}

	(*points)->merge(newPoints, false);


	return ret;
};

//================================================================================
	  
void CDenseMatcher::exportEpis(const CCharString &fnBase) const
{
	if (this->epiBuf1)
	{
		CCharString fn = fnBase + "_epi1.tif";
		this->epiBuf1->exportToTiffFile(fn.GetChar(), 0, false);
	}

	if (this->epiBuf2)
	{
		CCharString fn = fnBase + "_epi2.tif";
		this->epiBuf2->exportToTiffFile(fn.GetChar(), 0, false);
	}
};

//================================================================================
	  
void CDenseMatcher::exportParallax(const CCharString &fnBase) const
{
	if (this->parallaxBuf)
	{
		CCharString fn = fnBase + "_par.tif";
		float minDisp =  FLT_MAX;
		float maxDisp = -FLT_MAX;
		for (unsigned long i = 0; i < this->parallaxBuf->getNGV(); ++i)
		{
			float paral = this->parallaxBuf->pixFlt(i);
			if (paral >= this->SGBMpars.minDisparity)
			{
				if (paral < minDisp) minDisp = paral;
				if (paral > maxDisp) maxDisp = paral;
			}
		}

		CImageBuffer exportBuf(0,0, this->parallaxBuf->getWidth(), this->parallaxBuf->getHeight(), 1,8,true);
		float fac = 254.0f / (maxDisp - minDisp);

		for (unsigned long i = 0; i < this->parallaxBuf->getNGV(); ++i)
		{
			unsigned char c = 0;
			float paral = this->parallaxBuf->pixFlt(i);
			if (paral >= this->SGBMpars.minDisparity)
			{
				c = unsigned char(1 + (paral - minDisp) * fac); 
			}
			
			exportBuf.pixUChar(i) = c;
		}


		CLookupTable lut(8,3); 
		lut.setColor(0, 0, 255, 0);

		exportBuf.exportToTiffFile(fn.GetChar(), &lut, false);
	}

};

//================================================================================
