#include "float.h"

#include "GenerateEpipolar.h"
#include "ImgBuf.h"
#include "FrameCameraModel.h"
#include "HugeImage.h"
#include "RotationBase.h"


CGenerateEpipolar::CGenerateEpipolar(CHugeImage *leftI, CHugeImage *rightI, 
									 CFrameCameraModel *leftM, CFrameCameraModel *rightM) : 
	leftImageFile(leftI), rightImageFile(rightI), leftModel(leftM), rightModel(rightM)
{

}

//================================================================================

CGenerateEpipolar::~CGenerateEpipolar()
{
	this->leftImageFile  = 0;
	this->rightImageFile = 0;
	this->leftModel      = 0;
	this->rightModel     = 0;
}

//================================================================================
	  
inline void CGenerateEpipolar::transformEpiToOrig(const C2DPoint &pEpi, C2DPoint &pOri, 		                    
												  const CRotationMatrix &epiToOri, 
												  const C3DPoint &IRPepi, 
												  const C3DPoint &IRPori) const
{
	C3DPoint p(pEpi.x - IRPepi.x, pEpi.y - IRPepi.y, IRPepi.z);
	C3DPoint pr;
    epiToOri.R_times_x(pr, p);
	double fac = IRPori.z / pr.z;
	pOri.x = IRPori.x + fac * pr.x;
	pOri.y = IRPori.y + fac * pr.y;
};
	  	  
//================================================================================

bool CGenerateEpipolar::initNewOrientations(CFrameCameraModel &oriEpiLeft, 
											CFrameCameraModel &oriEpiRight) const
{
	bool ret = true;


	CFrameCamera *leftCam    = oriEpiLeft.getCamera();
	CFrameCamera *rightCam   = oriEpiRight.getCamera();
	CCameraMounting *mount   = oriEpiLeft.getCameraMounting();

	mount->setRotation(this->leftModel->getCameraMounting()->getRotation());
	CXYZPoint p(0.0,0.0,0.0);
	mount->setShift(p);
	leftCam->setIRP(this->leftModel->getCamera()->getIRP());
	leftCam->setIRPSymmetry(this->leftModel->getCamera()->getIRPSymmetry());
	
	rightCam->setIRP(this->rightModel->getCamera()->getIRP());
	rightCam->setIRPSymmetry(this->rightModel->getCamera()->getIRPSymmetry());

	oriEpiLeft.setRefSys(this->leftModel->getRefSys());
	oriEpiRight.setRefSys(this->rightModel->getRefSys());

	this->leftModel->getCameraRotation()->R_times_x(p, this->leftModel->getCameraMounting()->getShift());
	CXYZPoint leftP0 = this->leftModel->getCameraPosition() + (CObsPoint) p;

	this->rightModel->getCameraRotation()->R_times_x(p, this->rightModel->getCameraMounting()->getShift());
	CXYZPoint rightP0 = this->rightModel->getCameraPosition() + (CObsPoint) p;

	oriEpiLeft.setCameraPosition(leftP0);
	oriEpiRight.setCameraPosition(rightP0);

	CRotationBase *pRotL = oriEpiLeft.getCameraRotation();
	CRotationBase *pRotR = oriEpiRight.getCameraRotation();

	C3DPoint i,j,k;
	i = (C3DPoint) rightP0 - (C3DPoint) leftP0;
	i /= i.getNorm();
	CRotationMatrix combinedRot;
	this->leftModel->getCameraRotation()->getRotMat().R_times_R(combinedRot, this->leftModel->getCameraMounting()->getRotation().getRotMat());
	combinedRot.getThirdCol(k.x, k.y, k.z);

	j = k.crossProduct(i);
	j /= j.getNorm();
	k = i.crossProduct(j);
	
	combinedRot.setFirstCol(i.x, i.y, i.z);
	combinedRot.setSecondCol(j.x, j.y, j.z);
	combinedRot.setThirdCol(k.x, k.y, k.z);
	combinedRot.R_times_RT(pRotL->getRotMat(), mount->getRotation().getRotMat());

	pRotL->computeParameterFromRotMat();
	pRotR->getRotMat() = pRotL->getRotMat();
	pRotR->computeParameterFromRotMat();


	return ret;
};
	   	  
//================================================================================

bool CGenerateEpipolar::initEpiTrafPar(CFrameCameraModel &oriOrig, 
									   CFrameCameraModel &oriEpi, 
									   CRotationMatrix &epiToOri, 
									   C3DPoint &IRPepi, C3DPoint &IRPori) const
{
	CRotationMatrix rMori = oriOrig.getCameraMounting()->getRotation().getRotMat();
	CRotationMatrix rOri  = oriOrig.getCameraRotation()->getRotMat();
	CRotationMatrix rMepi = oriEpi.getCameraMounting()->getRotation().getRotMat();
	CRotationMatrix rEpi  = oriEpi.getCameraRotation()->getRotMat();

	CRotationMatrix r1,r2;
	rEpi.R_times_R(r1, rMepi);
	rMori.RT_times_RT(r2, rOri);
	r2.R_times_R(epiToOri, r1);

	IRPepi = oriEpi.getCamera()->getIRP();
	IRPori = oriOrig.getCamera()->getIRP();

	return true;
};

//================================================================================

bool CGenerateEpipolar::prepareOriBuf(CHugeImage *hug, 
									  const CImageBuffer &epiBuf, CImageBuffer &oriBuf, 
									  const CRotationMatrix &epiToOri, 
									  const C3DPoint &IRPepi, 
									  const C3DPoint &IRPori) const
{
	bool ret = true;

	int npts = 4;
	vector<C3DPoint> pEpi(npts);
	vector<C2DPoint> pOri(npts);

	pEpi[0].x = epiBuf.getOffsetX();
	pEpi[0].y = epiBuf.getOffsetY();
	pEpi[1].x = pEpi[0].x + epiBuf.getWidth();
	pEpi[1].y = pEpi[0].y;
	pEpi[2].x = pEpi[0].x;
	pEpi[2].y = pEpi[0].y + epiBuf.getHeight();
	pEpi[3].x = pEpi[1].x;
	pEpi[3].y = pEpi[2].y;
//	pEpi[4].x = 2366.7;
//	pEpi[4].y = 1687.3;

	C2DPoint pmin( FLT_MAX,  FLT_MAX);
	C2DPoint pmax(-FLT_MAX, -FLT_MAX);
	C3DPoint p;

	for (unsigned int i = 0; i < pEpi.size(); ++i)
	{
		pEpi[i].z = 0.0;
		pEpi[i] -= IRPepi;
		pEpi[i].z = -pEpi[i].z;
		epiToOri.R_times_x(p, pEpi[i]);
		double fac = IRPori.z / p.z;
		pOri[i].x = IRPori.x + fac * p.x;
		pOri[i].y = IRPori.y + fac * p.y;

		if (i < 4)
		{
			if (pOri[i].x < pmin.x) pmin.x = pOri[i].x;
			if (pOri[i].y < pmin.y) pmin.y = pOri[i].y;
			if (pOri[i].x > pmax.x) pmax.x = pOri[i].x;
			if (pOri[i].y > pmax.y) pmax.y = pOri[i].y;
		}
	}
	double security = 3.0;
	pmin.x = floor(pmin.x - security + 0.5); 
	pmin.y = floor(pmin.y - security + 0.5); 
	pmax.x = floor(pmax.x + security + 0.5); 
	pmax.y = floor(pmax.y + security + 0.5); 
	if (pmin.x < 0) pmin.x = 0;
	if (pmin.y < 0) pmin.y = 0;

	if (pmax.x > hug->getWidth())  pmax.x = hug->getWidth();
	if (pmax.y > hug->getHeight()) pmax.y = hug->getHeight();

	long w = long(pmax.x - pmin.x);
	long h = long(pmax.y - pmin.y);
	if ((w < 1) || (h < 1)) ret = false;
	else ret = oriBuf.set(long(pmin.x), long(pmin.y), w, h, epiBuf.getBands(), hug->bpp, epiBuf.getUnsignedFlag());

	return ret;
};

//================================================================================

bool CGenerateEpipolar::transformEpi(CHugeImage *hug, const CRotationMatrix &epiToOri, 
									 const C3DPoint &IRPepi,  const C3DPoint &IRPori, 
									 CImageBuffer &epi, bool convertTo8Bit)
{
	bool ret = true;

	unsigned short *tilebufferShrt = 0;
	unsigned char  *tilebufferUchr = 0;

	if (epi.getBpp() == 8)  tilebufferUchr = epi.getPixUC();
	else tilebufferShrt = epi.getPixUS(); // (this->bpp == 16)

	CImageBuffer oriBuf;

	C2DPoint pEpi0(epi.getOffsetX(), epi.getOffsetY()); 
	C2DPoint pEpi = pEpi0;
	C2DPoint pOrig;

	ret = prepareOriBuf(hug, epi, oriBuf, epiToOri, IRPepi, IRPori);

	if (ret)
	{		
		ret = hug->getRect(oriBuf);

		if (ret)
		{
			if (hug->bpp == 8)
			{
				for (int r = 0; r < epi.getHeight(); ++r, pEpi.y += 1.0)
				{
					pEpi.x = pEpi0.x;
					unsigned long idx = r * epi.getDiffY(); 
					for (int c = 0; c < epi.getWidth(); ++c, pEpi.x += 1.0, idx += epi.getDiffX())
					{
						transformEpiToOrig(pEpi, pOrig, epiToOri, IRPepi, IRPori);
						oriBuf.getInterpolatedColourVecBilin(pOrig, &tilebufferUchr[idx]);
					}
				}
			}
			else if (!convertTo8Bit)
			{
				for (int r = 0; r < epi.getHeight(); ++r, pEpi.y += 1.0)
				{
					pEpi.x = pEpi0.x;
					unsigned long idx = r * epi.getDiffY(); 

					for (int c = 0; c < epi.getWidth(); ++c, pEpi.x += 1.0, idx += epi.getDiffX())
					{
						transformEpiToOrig(pEpi, pOrig, epiToOri, IRPepi, IRPori);
						oriBuf.getInterpolatedColourVecBilin(pOrig, &tilebufferShrt[idx]);
					}
				}
			}		
			else
			{
				unsigned short maxGV, offset;
				maxGV = offset = oriBuf.pixUShort(0);
				for (int r = 0; r < oriBuf.getHeight(); ++r)
				{
					unsigned long idx = r * oriBuf.getDiffY(); 
					unsigned long idxMax = idx +  oriBuf.getDiffY(); 
					for (; idx < idxMax; idx += oriBuf.getDiffX())
					{
						for (int b = 0; b < oriBuf.getBands(); ++b)
						{
							unsigned short pix = oriBuf.pixUShort(idx + b);
							if (pix > 0)
							{
								if (pix < offset) offset = pix;
								if (pix > maxGV)         maxGV = pix;
							}
						}
					}
				}
				
				float alpha1  = 254.0f / float(maxGV - offset);
	
				unsigned short *gv = new unsigned short[epi.getBands()];

				for (int r = 0; r < epi.getHeight(); ++r, pEpi.y += 1.0)
				{
					pEpi.x = pEpi0.x;
					unsigned long idx = r * epi.getDiffY(); 
					unsigned long idxMax = idx +  epi.getDiffY(); 

					for (; idx < idxMax; pEpi.x += 1.0, idx += epi.getDiffX())
					{
						transformEpiToOrig(pEpi, pOrig, epiToOri, IRPepi, IRPori);
						oriBuf.getInterpolatedColourVecBilin(pOrig, gv);
						for (unsigned short u = 0; u < epi.getBands(); ++u)
						{
							if (gv[u] == 0) tilebufferUchr[idx + u] = 0;
							else 
							{
								tilebufferUchr[idx + u] = (unsigned char) (alpha1 * (gv[u] - offset)) + 1;
							}
						}
					}
				}
				delete [] gv;
			}
		};
	};

	return ret;
}; 
  
//================================================================================

bool CGenerateEpipolar::initEpipolarOrientations(CFrameCameraModel &oriEpiLeft, 
												 CFrameCameraModel &oriEpiRight) const
{
	bool ret = true; 

	CCameraMounting *mount = 0; 

	if ((!oriEpiLeft.getCamera())  || (!oriEpiLeft.getCameraRotation())  || (!oriEpiLeft.getCameraMounting()))
	{
		CFrameCamera *leftCam   = new CFrameCamera (*this->leftModel->getCamera());
		mount  = new CCameraMounting;
		if ((!leftCam)  || (!mount)) ret = false;
		else
		{
			leftCam->setNCols(this->leftImageFile->width);
			leftCam->setNRows(this->leftImageFile->height);

			CRollPitchYawRotation *prpyL = new CRollPitchYawRotation;
			ret = oriEpiLeft.init("left_frameCam", this->leftModel->getCameraPosition(),
					              prpyL, mount, leftCam);
		}
	}

	mount = oriEpiLeft.getCameraMounting();

	if ((!oriEpiRight.getCamera())  || (!oriEpiRight.getCameraRotation())  || (!oriEpiRight.getCameraMounting()))
	{
		CFrameCamera *rightCam  = new CFrameCamera (*this->rightModel->getCamera());
		if ((!rightCam)  || (!mount)) ret = false;
		else
		{
			rightCam->setNCols(this->rightImageFile->width);
			rightCam->setNRows(this->rightImageFile->height);

			CRollPitchYawRotation *prpyL = new CRollPitchYawRotation;
			ret = oriEpiRight.init("right_frameCam", this->rightModel->getCameraPosition(),
					               prpyL, mount, rightCam);
		}
	}


	if (ret) ret = this->initNewOrientations(oriEpiLeft, oriEpiRight);

	return ret;
};
	  
//================================================================================

bool CGenerateEpipolar:: generateEpipolar(CImageBuffer &oriLeft,  CImageBuffer &oriRight, 
		                    CImageBuffer &epiLeft,  CFrameCameraModel &oriEpiLeft, 
							CImageBuffer &epiRight, CFrameCameraModel &oriEpiRight, 
							bool initOris, bool convertTo8Bit)
{
	bool ret = true; 

	if (initOris) ret = this->initEpipolarOrientations(oriEpiLeft, oriEpiRight);

	if (ret)
	{
	}

	return ret;
}; 

	  
//================================================================================

bool CGenerateEpipolar::generateEpipolar(const C3DPoint &pmin, const C3DPoint &pmax, 		                
										 CImageBuffer &epiLeft,  CFrameCameraModel &oriEpiLeft, 							
										 CImageBuffer &epiRight, CFrameCameraModel &oriEpiRight, 
										 bool initOris, bool convertTo8Bit)
{
	bool ret = true; 

	if (initOris) ret = this->initEpipolarOrientations(oriEpiLeft, oriEpiRight);

	CRotationMatrix epiToOriL, epiToOriR;
	C3DPoint IRPepiL, IRPepiR, IRPoriL, IRPoriR;
		
	ret &= initEpiTrafPar(*this->leftModel,  oriEpiLeft,  epiToOriL, IRPepiL, IRPoriL);
	ret &= initEpiTrafPar(*this->rightModel, oriEpiRight, epiToOriR, IRPepiR, IRPoriR);

	if (ret)
	{
		vector<C3DPoint> corners(8);
		vector<C2DPoint> cornersImageL(8);
		vector<C2DPoint> cornersImageR(8);
		C2DPoint pMinL(FLT_MAX, FLT_MAX), pMaxL(-FLT_MAX, -FLT_MAX);
		C2DPoint pMinR(FLT_MAX, FLT_MAX), pMaxR(-FLT_MAX, -FLT_MAX);

		corners[0] = corners[4] = pmin; corners[4].z = pmax.z;
		corners[2] = corners[6] = pmax; corners[2].z = pmin.z;

		corners[1].x = corners[5].x = pmin.x; 
		corners[1].y = corners[5].y = pmax.y; 
		corners[1].z = pmin.z;
		corners[5].z = pmax.z;
		
		corners[3].x = corners[7].x = pmax.x; 
		corners[3].y = corners[7].y = pmin.y; 
		corners[3].z = pmin.z;
		corners[7].z = pmax.z;

		for (unsigned int ind = 0; ind < corners.size(); ++ind)
		{
			oriEpiLeft.transformObj2Obs(cornersImageL[ind], corners[ind]);
			oriEpiRight.transformObj2Obs(cornersImageR[ind], corners[ind]);
			if (cornersImageL[ind].x < pMinL.x) pMinL.x = cornersImageL[ind].x;
			if (cornersImageL[ind].y < pMinL.y) pMinL.y = cornersImageL[ind].y;
			if (cornersImageL[ind].x > pMaxL.x) pMaxL.x = cornersImageL[ind].x;
			if (cornersImageL[ind].y > pMaxL.y) pMaxL.y = cornersImageL[ind].y;
			if (cornersImageR[ind].x < pMinR.x) pMinR.x = cornersImageR[ind].x;
			if (cornersImageR[ind].y < pMinR.y) pMinR.y = cornersImageR[ind].y;
			if (cornersImageR[ind].x > pMaxR.x) pMaxR.x = cornersImageR[ind].x;
			if (cornersImageR[ind].y > pMaxR.y) pMaxR.y = cornersImageR[ind].y;
		}

		if (pMinL.y > pMinR.y) pMinL.y = pMinR.y;
		if (pMaxL.y < pMaxR.y) pMaxL.y = pMaxR.y;

		pMinL.x = floor(pMinL.x + 0.5) - 100;
		pMaxL.x = floor(pMaxL.x + 0.5) + 100;
		pMinR.x = floor(pMinR.x + 0.5) - 100;
		pMaxR.x = floor(pMaxR.x + 0.5) + 100;
		
		long offsetY = long(floor(pMinL.y  + 0.5));
		long h = long(pMaxL.y - pMinL.y) + 1;

		
		int bands = this->leftImageFile->getBands();
		int bpp   = this->leftImageFile->getBpp();
		long wL = long(pMaxL.x - pMinL.x) + 1;
		long wR = long(pMaxR.x - pMinR.x) + 1;
		long w = (wL > wR) ? wL : wR;
		
		if (convertTo8Bit) bpp = 8;
		ret = epiLeft.set (long(pMinL.x), offsetY, w, h, bands, bpp, true);
		if (ret) ret = epiRight.set(long(pMinR.x), offsetY, w, h, bands, bpp, true);

		if (ret) ret = transformEpi(this->leftImageFile,  epiToOriL, IRPepiL, IRPoriL, epiLeft,  convertTo8Bit); 
		if (ret) ret = transformEpi(this->rightImageFile, epiToOriR, IRPepiR, IRPoriR, epiRight, convertTo8Bit); 
	};

	return ret; 
}; 

//================================================================================
