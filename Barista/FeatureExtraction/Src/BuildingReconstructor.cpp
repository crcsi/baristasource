#ifdef WIN32

#include <float.h>
#include <strstream>
using namespace std;

#include "vraniml.h"

#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "statistics.h"

#include "BuildingReconstructor.h"
#include "ObservationHandler.h"
#include "ImgBuf.h"
#include "Building.h"
#include "histogramFlt.h"
#include "HugeImage.h"
#include "ALSData.h"
#include "ALSPointCloud.h"
#include "XYZPolyLineArray.h"
#include "3DadjPlane.h"
#include "RAG.h"
#include "LabelMatcher.h"
#include "XYLineSegment.h"
#include "XYZLinesAdj.h"
#include "XYPolyLineArray.h"
#include "BuildingFace.h"
#include "BuildingFacePoly.h"
#include "LineMatchingHypothesis.h"
#include "sortVector.h"

//=============================================================================

CBuildingReconstructor::CBuildingReconstructor(double SearchBuf, int nNeighbours, 
											   double MaxAngle, double SigmaALS, double Alpha,  
											   double dZmin, const FeatureExtractionParameters &Pars) :
		building (0), pDTM(0), pDSMPlanes(0), ALSpoints (0), nALSPoints(0), alpha(Alpha), 
		sigmaALS(SigmaALS), neighbours(nNeighbours), minHeight(dZmin), 
		resolutionALS(0), planeGraph(0), maxAngle(MaxAngle)
{ 
	this->extractionPars = Pars;
	this->bufferSize = SearchBuf;

	CBuildingFace::setRmsXY(2.0f * float(SigmaALS));
	CBuildingFace::setRmsZ (float (SigmaALS));
};

//=============================================================================

CBuildingReconstructor::~CBuildingReconstructor()
{
	this->resetALS();
	this->resetImageVectors();
	this->resetPlanes();

	this->building = 0;
};
	  

//=============================================================================
	  	  
double CBuildingReconstructor::getSigmaThreshold(int redundancy) const
{
	double fact = 3.0;

	if (redundancy > 0) 
	{
		fact = sqrt(statistics::chiSquareFractil(1.0f - this->alpha, redundancy) / double (redundancy));
	}
		
	return this->sigmaALS * fact;
};

//=============================================================================
	  	  
long CBuildingReconstructor::getLabelIndex(const CLabelImage &labels, const CALSPoint *pALS) const
{
	return MultiImageSegmenter::getLabelIndex(labels, pALS->getLastEcho());
}; 

//=============================================================================
	
long CBuildingReconstructor::getObjectLabelIndex(const CALSPoint *pALS) const
{
	return MultiImageSegmenter::getObjectLabelIndex(pALS->getLastEcho());
}; 

//=============================================================================

unsigned short CBuildingReconstructor::getObjectLabel(const CALSPoint *pALS) const
{
	return MultiImageSegmenter::getObjectLabel(pALS->getLastEcho());
};

//=============================================================================

unsigned short CBuildingReconstructor::getProjectedLabel(unsigned int projectedIndex, const CALSPoint *pALS) const
{
	return MultiImageSegmenter::getProjectedLabel(projectedIndex, pALS->getLastEcho());
};

//=============================================================================

unsigned short CBuildingReconstructor::getLabel(const CLabelImage &labels, const CALSPoint *pALS) const
{
	return MultiImageSegmenter::getLabel(labels, pALS->getLastEcho());
};

//=============================================================================

bool CBuildingReconstructor::initDataForBuilding(CBuilding *Building, 		                            
												 vector<CObservationHandler *> *imgVec, 
												 vector<CHugeImage *> *hugImgVec, 
												 CALSDataSet *ALS, const CCharString &path,
												 double pixImg, double pixDSM)
{ 
	this->resetALS();
	this->resetImageVectors();
	this->resetPlanes();
	this->building = Building;

	this->outputPath = path;
    this->outputPath = path;

	if (!outputPath.IsEmpty())
	{
		CSystemUtilities::mkdir(outputPath);

		if (outputPath[outputPath.GetLength() - 1] != CSystemUtilities::dirDelim)
			outputPath += CSystemUtilities::dirDelimStr;
	}
	
	bool ret = false;

	if (this->building)
	{
		ostrstream oss1;
		oss1.setf(ios::fixed, ios::floatfield);
		oss1.precision(2);
		oss1 << "B u i l d i n g   R e c o n s t r u c t i o n\n=============================================\n\n"
			<< "Building: " << this->building->getLabel().GetChar() << "\n Date: " << CSystemUtilities::dateString() 
			<< "\n Starting Time: " << CSystemUtilities::timeString() << "\n\n";
		oss1 << ends;
		protHandler.print(oss1, protHandler.prt);

		this->pmin = this->building->getPmin();
		this->pmax = this->building->getPmax();
		double pixDef = -1.0;
		
		this->building->getFloors(this->BoundaryPolygons);

		if ((imgVec && hugImgVec) && (imgVec->size() > 0) && (hugImgVec->size() > 0))
		{
			if (pixImg < FLT_EPSILON)
			{
				C2DPoint p((*hugImgVec)[0]->getWidth(), (*hugImgVec)[0]->getHeight());
				C3DPoint P0, Px, Py;
				(*imgVec)[0]->getCurrentSensorModel()->transformObs2Obj(P0, p, this->pmax.z);
				p.x += 1.0;
				(*imgVec)[0]->getCurrentSensorModel()->transformObs2Obj(Px, p, this->pmax.z);
				p.x -= 1.0;
				p.y += 1.0;
				(*imgVec)[0]->getCurrentSensorModel()->transformObs2Obj(Py, p, this->pmax.z);

				Px -= P0;
				Py -= P0;

				pixImg = 0.5 * (Px.getNorm() +  Py.getNorm());
				pixImg =  floor (pixImg / 0.05) * 0.05;
				if (pixImg < FLT_EPSILON) pixImg = 0.05;

				pixDef = pixImg;
			}
			else pixDef = pixImg;

			pixImg = pixDef;
		}
		
		if (ALS)
		{
			C2DPoint p = ALS->getPointDist();
			this->resolutionALS = (fabs(p.x) + fabs(p.y)) * 0.5;

			if (pixDSM < FLT_EPSILON)
			{
				pixDSM = this->resolutionALS;

				if (pixDef > FLT_EPSILON)
				{
					pixDSM = floor (pixDSM / pixImg) * pixImg;							
					if (pixDSM < pixImg) pixDSM = pixImg;
				}
			}

			if (pixDef < FLT_EPSILON) pixDef = pixDSM;
		}


		if (pixDef > FLT_EPSILON)
		{
			this->pmin.x = floor ((this->pmin.x - this->bufferSize) / pixDef) * pixDef;							
			this->pmin.y = floor ((this->pmin.y - this->bufferSize) / pixDef) * pixDef;							
					
			this->pmax.x = floor ((this->pmax.x + this->bufferSize) / pixDef + 1.0) * pixDef;							
			this->pmax.y = floor ((this->pmax.y + this->bufferSize) / pixDef + 1.0) * pixDef;							

			if ((pmax.x > this->pmin.x) && (pmax.y > this->pmin.y))
			{		
				if ((imgVec && hugImgVec) && (pixDef > FLT_EPSILON))
				{
					if (imgVec->size() == hugImgVec->size())
					{
						this->initImages(imgVec, hugImgVec, pixImg);
						if (outputMode >= eMEDIUM) this->exportImageBuffersToTiff();
					}
				}

				if (ALS) 
				{
					this->initALS(ALS, pixDSM);	
					if (outputMode >= eMEDIUM) 
					{
						this->exportDSMToTiff();
						this->exportOcclusionBuffersToTiff();
						this->exportImagesWithALSToTiff();
					}
				}
			}
		}

		if (!this->pDSM)
		{
			if (this->imageVector.size() < 2) this->resetImageVectors();
		}

		if (this->isInitialised())
		{
			this->reduction = (this->pmin + this->pmax) * 0.5;
			this->reduction.x = floor(this->reduction.x + 0.5);
			this->reduction.y = floor(this->reduction.y + 0.5);
			this->reduction.z = floor(this->reduction.z + 0.5);

			imagePixelSize.resize(this->imageVector.size());
			imageOffsets.resize(this->imageVector.size());

			for (unsigned int i = 0; i < this->imageVector.size(); ++i)
			{
				C2DPoint p(this->imageVector[i]->getOffsetX(), this->imageVector[i]->getOffsetY());
				C3DPoint P0, Px, Py;
				this->sensorModelVector[i]->transformObs2Obj(P0, p, this->pmax.z);
				p.x += 1.0;
				this->sensorModelVector[i]->transformObs2Obj(Px, p, this->pmax.z);
				p.x -= 1.0;
				p.y += 1.0;
				this->sensorModelVector[i]->transformObs2Obj(Py, p, this->pmax.z);

				Px -= P0;
				Py -= P0;

				imagePixelSize[i] = floor((0.5 * (Px.getNorm() +  Py.getNorm())) * 100.0 + 0.5) * 0.01;
				imageOffsets[i].x = this->imageVector[i]->getOffsetX() +  (this->imageVector[i]->getWidth() / 2);
				imageOffsets[i].y = this->imageVector[i]->getOffsetY() +  (this->imageVector[i]->getHeight() / 2);
			}

			this->planeGraph = new CBuildingFaceNeighbourGraph(this->imageTrafo);
			CCharString fn = this->outputPath + "combined_labels.tfw";
			this->imageTrafo.writeTextFile(fn.GetChar());

			ostrstream oss;
			oss.setf(ios::fixed, ios::floatfield);
			oss.precision(2);
			oss << "\n Coordinate reduction: " << this->reduction.getString(" ", 10, 3).GetChar()
			    << "\n\n Input data initialised; Time: " << CSystemUtilities::timeString() << "\n" << ends;
			protHandler.print(oss, protHandler.prt);

			ret = true;
		}
		else
		{
			protHandler.print(" Error: could not initialise data", protHandler.prt); 
		}

	}
	else protHandler.print("Cannot reconstruct building: building not initialised",protHandler.prt);

	return ret;
};

//=============================================================================

void CBuildingReconstructor::resetPlanes()
{
	delete this->planeGraph;
	this->planeGraph = 0;
};

//=============================================================================

void CBuildingReconstructor::resetPlanes(vector<CBuildingFace *> &planes)
{
	for (unsigned int i = 0; i < planes.size(); ++i)
	{
		delete planes[i];
		planes[i] = 0;
	}

	planes.clear();
};

//=============================================================================

bool CBuildingReconstructor::initImage(CObservationHandler *oh, CHugeImage *hugImg, double pixSize)
{
	bool ret = MultiImageSegmenter::initImage(oh, hugImg, pixSize);

	if (ret)
	{
		vector<CXYLineSegment *> v;
		imageStraightLineVector.push_back(v);
	}

	return ret;
};

//=============================================================================

bool CBuildingReconstructor::initALS(CALSDataSet *ALS, double pixelSize)
{
	bool ret = true;

	C2DPoint shift(this->pmin.x, this->pmax.y);
	C2DPoint pix(pixelSize, pixelSize);

	float zmin, zmax;

	this->DSMtrafo.setFromShiftAndPixelSize(shift, pix);

	int width  = int(floor((this->pmax.x - this->pmin.x) / pixelSize)) + 1;
	int height = int(floor((this->pmax.y - this->pmin.y) / pixelSize)) + 1;

	if ((width < 5) || (height < 5)) ret = false;
	else
	{
		this->pDSM = new CImageBuffer(0, 0, width, height, 1, 32, false);
		this->pDTM = new CImageBuffer(0, 0, width, height, 1, 32, false);
		if ((!this->pDSM) || (!this->pDTM))
		{
			if (this->pDSM) delete this->pDSM;
			if (this->pDTM) delete this->pDTM;
			this->pDSM = 0;
			this->pDTM = 0;
			ret = false;
		}
		else
		{
			this->ALSpoints = new CALSPointCloudBuffer(2);
			if (!this->ALSpoints) ret = false;
			else ret = (this->ALSpoints->get(*ALS, this->pmin, this->pmax, 0) > 0);

			if (ret) 
			{
				this->ALSpoints->interpolateRaster(this->pDSM->getPixF(), this->pDSM->getWidth(), this->pDSM->getHeight(), 
					                               this->pDSM->getWidth(), this->pmin.x, this->pmax.y, pixelSize, pixelSize, 
												   4, 1, eMovingHzPlane, zmin, zmax);

				for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
				{
					this->ALSpoints->getPoint(i)->id = i;
				}

				double maxDist = ALS->getPointDist().x;
				if (ALS->getPointDist().y > maxDist) maxDist = ALS->getPointDist().y;
				maxDist *= 5.0;

				ret  = this->initRoughness(maxDist);
				ret &= this->initDTM(pixelSize);
				if (ret) 
				{
					this->pDSMPlanes = new CImageBuffer(*this->pDSM);
					if (!this->pDSMPlanes) ret = false;
					else if (!this->initOcclusionMasks(this->pDSM)) ret = false;
				}
			}
			if (!ret) 
			{
				if (this->ALSpoints)  delete this->ALSpoints;
				if (this->pDSM)       delete this->pDSM;
				if (this->pDSMPlanes) delete this->pDSMPlanes;
				if (this->pDTM)       delete this->pDTM;
				this->ALSpoints  = 0;
				this->pDSM       = 0;
				this->pDSMPlanes = 0;
				this->pDTM       = 0;
			}
		}
		
		if (ret)
		{
			vector<float> minVec(1), maxVec(1);
			this->pDTM->getMinMax(0, minVec, maxVec);

			ostrstream oss;
			oss.setf(ios::fixed, ios::floatfield);
			oss.precision(2);
			oss << "\n ALS data:\n =========\n\n   Number of Points: "
				<< this->ALSpoints->getNpoints() 
				<< "\n   DSM: LUC:            " << this->pmin.x << " / " << this->pmax.y 
				<< "\n        Pixel size:     " << pixelSize
				<< "\n        Size:           " << this->pDSM->getWidth() << " x " << this->pDSM->getHeight()
				<< "\n        min/max height: " << zmin << " / " << zmax
				<< "\n   DTM: min/max height: " << minVec[0] << " / " << maxVec[0] << ends;

			protHandler.print(oss, protHandler.prt);
		}
	}


	return ret;
};

//=============================================================================
	  
void CBuildingReconstructor::resetImageVectors()
{
	MultiImageSegmenter::resetImageVectors();

	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		for (unsigned int j = 0; j < this->imageStraightLineVector[i].size(); ++j)
		{
			delete this->imageStraightLineVector[i][j];
			this->imageStraightLineVector[i][j] = 0;
		}
	}
};

//=============================================================================

void CBuildingReconstructor::resetALS()
{
	if (this->ALSpoints) delete this->ALSpoints;

	this->ALSpoints = 0;

	this->smoothness.clear();
	if (this->pDSM) delete this->pDSM;
	this->pDSM = 0;
	if (this->pDTM) delete this->pDTM;
	this->pDTM = 0;
	if (this->pDSMPlanes) delete this->pDSMPlanes;
	this->pDSMPlanes = 0;

	for (unsigned int i = 0; i < this->occlusionMasks.size(); ++i)
	{
		delete this->occlusionMasks[i];
		this->occlusionMasks[i]    = 0;
	}

	this->occlusionMasks.clear();
};

//=============================================================================

void CBuildingReconstructor::exportImagesWithALSToTiff() const
{
	if (this->ALSpoints)
	{
		for (unsigned int i = 0; i < this->imageVector.size(); ++i)
		{
			ostrstream oss;
			oss << "image_ALS_" << i  << ".tif" << ends;
			char *z = oss.str();
			CCharString str = this->outputPath + z;
			delete [] z;

			CImageBuffer imgBuf(*this->imageVector[i]);
			C3DPoint P;
			C2DPoint obs;
			unsigned short rALS = 0;
			unsigned short gALS = 255;
			unsigned short bALS = 255;

			if ((imgBuf.getBpp() > 8))
			{
				vector<float> minVec(imgBuf.getBands());
				vector<float> maxVec(imgBuf.getBands());
				imgBuf.getMinMax(1, minVec, maxVec);
				if (imgBuf.getBands() < 3)
				{
					gALS = unsigned short ((minVec[0] + maxVec[0])* 0.5f);
					bALS = gALS;
				}
				else
				{
					gALS = unsigned short ((minVec[1] + maxVec[1])* 0.5f);
					bALS = unsigned short ((minVec[2] + maxVec[2])* 0.5f);
				}
			}

			for (int j = 0; j < this->ALSpoints->getNpoints(); ++j)
			{
				CALSPoint *p = this->ALSpoints->getPoint(j);
				P = p->getEcho(1);
				this->sensorModelVector[i]->transformObj2Obs(obs,P);
				long row = long(obs.y) - imgBuf.getOffsetY();
				long col = long(obs.x) - imgBuf.getOffsetX();
				long index = imgBuf.getIndex(row, col);
				if ((index >= 0) && (index < (long) imgBuf.getNGV()))
				{
					if (imgBuf.getBpp() == 8) 
					{
						imgBuf.pixUChar(index    ) = 0;
						imgBuf.pixUChar(index + 1) = 255;
						imgBuf.pixUChar(index + 2) = 255;
					}
					else
					{
						imgBuf.pixUShort(index    ) = rALS;
						imgBuf.pixUShort(index + 1) = gALS;
						imgBuf.pixUShort(index + 2) = bALS;
					}
				}
			}
					
			imgBuf.exportToTiffFile(str.GetChar(), 0, true); 
		
		};
	};
};
		
//=============================================================================

void CBuildingReconstructor::exportBuffersToTiff() const
{
	this->exportImageBuffersToTiff();
	this->exportDSMToTiff();
	this->exportImagesWithALSToTiff();
	this->exportLabelBuffersToTiff();
	this->exportObjectLabelBufferToTiff();	
	this->exportProjectedLabelBuffersToTiff();

	if (this->pDSMPlanes)
	{
		CCharString filename = this->outputPath + "DSM_planes.tif";
		this->pDSMPlanes->exportToTiffFile(filename.GetChar(), 0, true); 
	}

	if (this->pDTM)
	{
		CCharString filename = this->outputPath + "DTM.tif";
		this->pDTM->exportToTiffFile(filename.GetChar(), 0, true); 
	}
};

//=============================================================================
 	  
void CBuildingReconstructor::exportEdges(int index) const
{
	int lineCode[ 3 ] = {10, 20, 30};
		ostrstream oss;
		oss << this->outputPath.GetChar() << "edges_" << index  << ".dxf" <<ends;
		char *z = oss.str();
		ofstream dxfFile(z);
		if (dxfFile.good())
		{
			CXYZPolyLine::writeDXFHeader(dxfFile);

			dxfFile.setf(ios::fixed);
			dxfFile.precision(1);
			CXYZPoint p;

			for (unsigned int j = 0; j < this->imageStraightLineVector[index].size(); ++j)
			{
				CXYZPolyLine poly;
				p.x =  this->imageStraightLineVector[index][j]->getStartPt().x;
				p.y = -this->imageStraightLineVector[index][j]->getStartPt().y;
				poly.addPoint(p);
				p.x =  this->imageStraightLineVector[index][j]->getEndPt().x;
				p.y = -this->imageStraightLineVector[index][j]->getEndPt().y;
				poly.addPoint(p);
				ostrstream oss1;
				oss1 << "EDGE_";
				if (j < 1000) oss1 << "0";
				if (j <  100) oss1 << "0";
				if (j <   10) oss1 << "0";
				oss1 << j << ends;
				char *layer = oss1.str();
				poly.writeDXF(dxfFile, layer, 1, 1);
				delete [] layer;
			}
			CXYZPolyLine::writeDXFTrailer(dxfFile);
		}
		delete [] z;
};

//=============================================================================
 	  
void CBuildingReconstructor::exportEdges() const
{ 

	for (unsigned int i = 0; i < this->imageStraightLineVector.size(); ++i)
	{
		exportEdges(i);
	}
};

//=============================================================================
 
void CBuildingReconstructor::exportPointCloud() const
{
	if (this->ALSpoints)
	{
		CCharString filename = this->outputPath + "ALS_LP.xyz";
		this->ALSpoints->writeXYZ(filename.GetChar(), 1);
	}
};

//=============================================================================

bool CBuildingReconstructor::segmentImages()
{
	bool ret = true;
	
	CCharString msg(" Segmenting Images:\n ==================\n   Time: ");
	msg += CSystemUtilities::timeString();
			
	protHandler.print(msg, protHandler.prt);

	FeatureExtractionParameters edgePars = this->extractionPars;
	edgePars.extractEdges = true;
	edgePars.extractRegions = false;
	edgePars.Mode = eFoerstner;
	
	double zmax = this->pmax.z;
	if (this->ALSpoints)
	{
		zmax = this->ALSpoints->getPmax().z;
	}

	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		CImageBuffer *img = this->imageVector[i];
		CImageBuffer *validMask = this->getValidMask(i, zmax);

		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(2);
		oss << i << ends;
		char *z = oss.str();
		CCharString suffix(z); 
		delete [] z;

		msg = "\n Segmenting image " + suffix + "; Time: " + CSystemUtilities::timeString();
		protHandler.print(msg, protHandler.prt);

		CFeatureExtractor extractor(this->extractionPars);
		extractor.setRoi(this->imageVector[i], validMask);
		CCharString titleStr; 
		if (this->listener)
		{
			titleStr = this->listener->getTitleString();
			CCharString newT = "Segmentation (" + CCharString(i + 1, 1, false) + "/" + CCharString(imageVector.size(), 1, false) + ")";
			this->listener->setTitleString(newT);
			extractor.setProgressListener(this->listener);
		}
		extractor.extractFeatures(0,0,0,this->imageLabelVector[i], this->imageRAGVector[i], true);

		if (this->listener)
		{
			this->listener->setTitleString(titleStr.GetChar());
		}
		ostrstream oss1;
		oss1.setf(ios::fixed, ios::floatfield);
		oss1.precision(2);
		oss1 << this->imageLabelVector[i]->getNLabels() << ends;
		z = oss1.str();
		msg = "     ";
		msg += z;
		msg += " segments found; Time: " + CSystemUtilities::timeString();
		delete [] z;
		protHandler.print(msg, protHandler.prt);

		extractor.setPars(edgePars);
		double sigMin = 0.5;
	  
		this->exportLabelBufferToTiff(i, "");

		CXYContourArray xycont;
		CXYPolyLineArray xyedges;
		if (this->listener)
		{
			CCharString newT = "Edge detection (" + CCharString(i + 1, 1, false) + " / " + CCharString(imageVector.size(), 1, false) + ")";
			this->listener->setTitleString(newT);
			extractor.setProgressListener(this->listener);
		}
		extractor.extractFeatures(0, &xycont, &xyedges, 0, 0, false);
		if (this->listener)
		{
			this->listener->setTitleString(titleStr.GetChar());
		}
		if (outputMode > eMEDIUM)
		{
			CCharString indexString(i, 2, true);
			CCharString fn = this->outputPath + "img_" + indexString + "_texture.tif";	
			extractor.exportClassification(fn.GetChar());
		}

		for (int j = 0; j < xyedges.GetSize(); ++j)
		{
			CXYPolyLine *poly = xyedges.getAt(j);
			for (int k = 1; k < poly->getPointCount(); ++k)
			{
				CXYPoint P1 = poly->getPoint(k - 1);
				CXYPoint P2 = poly->getPoint(k);
				if (P1.getDistance(P2) > edgePars.minLineLength)
				{
					P1.x -= this->imageOffsets[i].x;
					P1.y -= this->imageOffsets[i].y;
					P2.x -= this->imageOffsets[i].x;
					P2.y -= this->imageOffsets[i].y;
					if (P1.getSX() < sigMin) P1.setSX(sigMin);
					if (P2.getSX() < sigMin) P2.setSX(sigMin);
					if (P1.getSY() < sigMin) P1.setSY(sigMin);
					if (P2.getSY() < sigMin) P2.setSY(sigMin);
					CXYLineSegment *newSeg = new CXYLineSegment(P1, P2);
					this->imageStraightLineVector[i].push_back(newSeg);
				}
			}
		}
		ostrstream oss2;
		oss2.setf(ios::fixed, ios::floatfield);
		oss2.precision(2);
		oss2 << this->imageStraightLineVector[i].size() << ends;
		z = oss2.str();
		msg = "     ";
		msg += z;
		msg += " edges found; Time: " + CSystemUtilities::timeString();
		delete [] z;
		protHandler.print(msg, protHandler.prt);

		this->exportEdges(i);

		if (validMask) 
		{		
			delete validMask;
		}
	}

	msg = "\n Segmentation finished; time: " + CSystemUtilities::timeString() + "\n";
	protHandler.print(msg, protHandler.prt);

	return ret;
};
	  	 
//=============================================================================

bool CBuildingReconstructor::projectSegmentedImages()
{
	CCharString msg("\n Projecting segmented Images:\n ============================\n   Time: ");
	msg += CSystemUtilities::timeString();
	protHandler.print(msg, protHandler.prt);

	CImageBuffer *dmy = this->pDSM;

	this->pDSM = this->pDSMPlanes;

	bool ret = MultiImageSegmenter::projectSegmentedImages();

	this->pDSM = dmy;

	msg = "\n Projection finished; time: " + CSystemUtilities::timeString() + "\n";
	protHandler.print(msg, protHandler.prt);
	
	return ret; 
};
	 
//=============================================================================

float CBuildingReconstructor::getDTMHeight(const C2DPoint &p) const
{
	C3DPoint obj(p.x, p.y, 0.0);
	C2DPoint obs;	
	this->DSMtrafo.transformObj2Obs(obs, obj);

	obs.x -= this->pDTM->getOffsetX();
	obs.y -= this->pDTM->getOffsetY();


	float z;
	this->pDTM->getInterpolatedColourVecBilin(obs.y, obs.x, &z);

	return z;
};
	  
//=============================================================================

float CBuildingReconstructor::getDTMHeight(const C3DPoint &p) const
{
	C2DPoint obs;	
	this->DSMtrafo.transformObj2Obs(obs, p);

	obs.x -= this->pDTM->getOffsetX();
	obs.y -= this->pDTM->getOffsetY();


	float z;
	this->pDTM->getInterpolatedColourVecBilin(obs.y, obs.x, &z);

	return z;
};

//=============================================================================

bool CBuildingReconstructor::initDTM(double pixelSize)
{
	bool ret = true;
	if (this->ALSpoints)
	{
		CCharString fn = this->outputPath + "ground.xyz";
		ofstream groundFile(fn.GetChar());
		fn = this->outputPath + "notGround.xyz";
		ofstream notGroundFile(fn.GetChar());

		groundFile.setf(ios::fixed, ios::floatfield);
		groundFile.precision(2);
		notGroundFile.setf(ios::fixed, ios::floatfield);
		notGroundFile.precision(2);

		double threshSigma = this->getSigmaThreshold(this->neighbours - 3);

		CGenericPointCloud DTMpoints(this->ALSpoints->getNpoints(),3,2);
		CGenericPointCloud OffTerrpoints(this->ALSpoints->getNpoints(),3,2);

		C3DPoint p;
		float minz = (float) this->pmin.z;
		CHistogramFlt histoHeight((float) floor(this->pmin.z), (float) floor(this->pmax.z + 1), 0.10f);

		for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
		{
			CALSPoint *pPtALS = this->ALSpoints->getPoint(i);
			int index = pPtALS->getLastEchoIndex() + 2;
			float z = (float) (*pPtALS)[index];
			histoHeight.addValue(z);
		}

		minz = histoHeight.getPercentile(0.01);

		
		for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
		{
			p = this->ALSpoints->getPoint(i)->getLastEcho();
			double z = p.z - minz;
			double s = this->smoothness[i];
			if ((this->smoothness[i] > FLT_EPSILON) && (this->smoothness[i] < threshSigma))
			{
				if ((p.z - minz) < this->minHeight)
				{
					DTMpoints.addPoint(p);
				}
			}
		}
		float zmin, zmax;

		int nDTMPts = 9;

		if (DTMpoints.getNpoints() < nDTMPts)
		{
			double sum = 0;
			for (int  k = 0; k < DTMpoints.getNpoints(); ++k)
			{
				ANNpoint p = DTMpoints.getPt(k);
				sum += p[2];
			}

			if (DTMpoints.getNpoints() < 1) sum = this->pmin.z;
			else sum /= double(DTMpoints.getNpoints());
			this->pDTM->setAll(float(sum));
		}
		else
		{
			DTMpoints.init();

			DTMpoints.interpolateRaster(this->pDTM->getPixF(), this->pDTM->getWidth(), this->pDTM->getHeight(), 
										this->pDTM->getWidth(), this->pmin.x, this->pmax.y, pixelSize, pixelSize, 
										nDTMPts, eMovingHzPlane, zmin, zmax, 2, -1.0);

			DTMpoints.clear(); 
		}
		for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
		{
			p = this->ALSpoints->getPoint(i)->getLastEcho();

			if ((p.z - this->getDTMHeight(p)) < this->minHeight)
			{
				DTMpoints.addPoint(p);
				groundFile << p.x << " " << p.y << " " << p.z << "\n";
			}
			else 
			{
				notGroundFile << p.x << " " << p.y << " " << p.z << "\n";
				OffTerrpoints.addPoint(p);
			}
		}
	
		DTMpoints.init();

		DTMpoints.interpolateRaster(this->pDTM->getPixF(), this->pDTM->getWidth(), this->pDTM->getHeight(), 
					                this->pDTM->getWidth(), this->pmin.x, this->pmax.y, pixelSize, pixelSize, 
									4, eMovingHzPlane, zmin, zmax, 2, -1.0);

		CImageBuffer otPts(*this->pDSM);

		if (this->pDTM)
		{
			CCharString filename = this->outputPath + "DTM.tif";
			this->pDTM->exportToTiffFile(filename.GetChar(), 0, true); 
		}

		OffTerrpoints.init();
		OffTerrpoints.interpolateRaster(otPts.getPixF(), otPts.getWidth(), otPts.getHeight(), otPts.getWidth(), 
										this->pmin.x, this->pmax.y, pixelSize, pixelSize, 4, eMovingHzPlane, zmin, zmax, 
										this->resolutionALS, 2, -1.0);

		CCharString filename = this->outputPath + "DSM_off.tif";
		otPts.exportToTiffFile(filename.GetChar(), 0, true); 

		for (unsigned long u = 0; u < otPts.getNGV(); ++u)
		{
			float h = otPts.pixFlt(u);
			if (h > DEM_NOHEIGHT) 
			{
				this->pDSM->pixFlt(u) = h;
			}
		}
	}


	return ret;
};

//=============================================================================

bool CBuildingReconstructor::initRoughness(double maxDist)
{
	bool ret = true;

	this->smoothness.clear();
	if (this->ALSpoints)
	{
		this->smoothness.resize(this->ALSpoints->getNpoints());
		int nMin = this->neighbours;
		double tanAngle = tan(this->maxAngle);

	
		double *sqrDist = new double[this->neighbours];
		CALSPoint **neigh = new CALSPoint*[this->neighbours]; 
		ANNpoint pA;
		C3DPoint p, pN;
		int nNeigh;
		C2DPoint rowColPoint;
		C3DAdjustPlane plane;

		for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
		{
			pA = this->ALSpoints->getPt(i);
			smoothness[i] = 0.0;

			p.x = pA[0]; p.y = pA[1]; p.z = pA[2];
			nNeigh = this->ALSpoints->getNearestNeighbours(neigh, sqrDist, this->neighbours, p, maxDist);

			if (nNeigh < nMin)
			{
				smoothness[i] = -1.0;
			}
			else
			{
				plane.resetPlane();
				plane.resetSums(this->reduction);

				for (int j = 0; j < nNeigh; ++j)
				{
					short lastEchoIdx = neigh[j]->getLastEchoIndex();
					pN.x = neigh[j]->getCoordinate(lastEchoIdx);  
					pN.y = neigh[j]->getCoordinate(lastEchoIdx + 1);  
					pN.z = neigh[j]->getCoordinate(lastEchoIdx + 2);
					plane.addPoint(pN);
				}

				smoothness[i] = plane.adjust();

				if (smoothness[i] > FLT_EPSILON)
				{
				  if (plane.getObliqueness() > tanAngle) smoothness[i] = 1000.0;
				}
			}
		}

		delete [] neigh;
		delete [] sqrDist;
	}

	return ret;
};


//=============================================================================

bool CBuildingReconstructor::checkHeights(CLabelImage &labels)
{
	bool ret = true;
	if (this->ALSpoints)
	{
		vector<unsigned int> N(labels.getNLabels(), 0);
		vector<double> sumZ(labels.getNLabels(), 0.0);
		for (int j = 0; j < this->ALSpoints->getNpoints(); ++j)
		{
			C3DPoint p = this->ALSpoints->getPoint(j)->getLastEcho();
			C2DPoint obs;
			this->imageTrafo.transformObj2Obs(obs, p);
			obs.x = floor(obs.x - labels.getOffsetX() + 0.5);
			obs.y = floor(obs.y - labels.getOffsetY() + 0.5);
			if ((obs.x >= 1) && (obs.x < labels.getWidth()) && 
				(obs.y >= 1) && (obs.y < labels.getHeight()))
			{
				long index = labels.getIndex(long(obs.y), long(obs.x));
				unsigned short label = labels.getLabel(index);
				if (label)
				{
					N[label] += 1;
					sumZ[label] += (p.z - double(this->getDTMHeight(p)));
				}
			}
		}

		for (unsigned int j = 1; j < sumZ.size(); ++j)
		{
			if (N[j] > 1)
			{
				sumZ[j] /= double(N[j]);
				if (sumZ[j] < this->minHeight)
				{
					labels.removeLabel(j, false);
				}
			}
		}
	}

	return ret;
};
	  
//=============================================================================

void CBuildingReconstructor::checkCombinedHeights()
{
	vector<float>  maxHeights(this->pCombinedLabels->getNLabels(), -FLT_MAX);
	vector<double> avgHeights(this->pCombinedLabels->getNLabels(), 0.0);;
	vector<int> nLarger(this->pCombinedLabels->getNLabels(), 0);;
	vector<int> nGrd(this->pCombinedLabels->getNLabels(), 0);;
	vector<int> nNotGrd(this->pCombinedLabels->getNLabels(), 0);;

	for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
	{
		unsigned short label = this->pCombinedLabels->getLabel(i);
		if (label)
		{
			float zDTM = this->pDTM->pixFlt(i);
			float zDOM = this->pDSM->pixFlt(i);
			float dZ = zDOM - zDTM;
			if (dZ > maxHeights[label]) maxHeights[label] = dZ;
			if (dZ > this->minHeight) nLarger[label]++;
			avgHeights[label] += dZ;
		}
	}
	
	for (int j = 0; j < this->ALSpoints->getNpoints(); ++j)
	{
		C3DPoint p = this->ALSpoints->getPoint(j)->getLastEcho();
		float zDTM = this->getDTMHeight(p); 
		unsigned short label = MultiImageSegmenter::getObjectLabel(p);
		if (label)
		{
			if ((p.z - zDTM) > this->minHeight) ++nNotGrd[label];
			else ++nGrd[label];
		}
	}

	double percentage = 0.1;

	for (unsigned int i = 1; i < avgHeights.size(); ++i)
	{
		unsigned int nPix = this->pCombinedLabels->getNPixels(i);
		int nPixPerc = (int) floor(double(nPix) * percentage);
		if (nPix)
		{
			avgHeights[i] /= double(nPix);
			if (((avgHeights[i]  < this->minHeight) && (nLarger[i] < nPixPerc)) ||
				((nGrd[i] > nNotGrd[i]) && nGrd[i] > 0))
			{
				this->pCombinedLabels->removeLabel(i, false);
				for (int j = 0; j < this->ALSpoints->getNpoints(); ++j)
				{
					if (this->ALSPointLabelIndex[j] == i) this->ALSPointLabelIndex[j] = 0;
				}
				this->pointsInPlanes[i].resize(0);
				this->planeGraph->getFace(i)->resetData(i, this->reduction);
			}
		}
	}
	this->pCombinedLabels->removeEmptyLabels(false);
	this->pCombinedLabels->updateVoronoi();
};
	  
//=============================================================================
 
bool CBuildingReconstructor::computeRobust(CBuildingFace &plane, 
										   vector<CALSPoint *> &pointVec, 
										   int maxElim, int nPtsMin)
{
	bool ret = false;

	if ((pointVec.size() > 3) && (pointVec.size() > (unsigned int) nPtsMin))
	{
		int nElim = 0;

		do 
		{
			double threshSigma = this->getSigmaThreshold((int)plane.getRedundancy());
			unsigned int iMax = 0;
			double resmax = plane.getDistance(pointVec[0]->getLastEcho());
			for (unsigned int j = 1; j < pointVec.size(); ++j)
			{
				double res = plane.getDistance(pointVec[j]->getLastEcho());
				if (res > resmax)
				{
					resmax = res;
					iMax = j;
				}
			}

			if ((resmax < plane.getThreshold()) && (plane.getSigma0() < threshSigma) && (pointVec.size() > (unsigned int) nPtsMin)) ret = true;
			else
			{
				plane.resetSums(this->reduction);

				for (unsigned int j = 0; j < pointVec.size(); ++j)
				{
					if (j!= iMax) plane.addPoint(pointVec[j]->getLastEcho());
				}

				double s0 = plane.adjust();

				for (unsigned int i = iMax + 1; i < pointVec.size(); ++i)
				{
					pointVec[i-1] = pointVec[i];
				};
				pointVec.resize(pointVec.size() - 1);

				++nElim;
			}
		} while ((plane.getRedundancy() > 1) && (nElim < maxElim) && (!ret));
	}
	else if (pointVec.size() >= (unsigned int) nPtsMin)
	{
		ret = true;
	}
	

	return ret;
};

//=============================================================================

bool CBuildingReconstructor::splitBadFits(CLabelImage &labels, 
										  vector<CBuildingFace *> &planes, 
		                                  vector<vector<CALSPoint *>> &pointVec,
										  unsigned short plane,
										  bool writeToProtocol)
{
	bool ret = true;

	CALSPoint *pOutlier = 0;
	double scale = this->resolutionALS * 0.5 / this->imageTrafo.getScaleX();

/*	if (checkForOneOutlier(*planes[plane], pointVec[plane], pOutlier))
	{
		CBuildingFace *pl = new CBuildingFace;
		pl->resetData(planes.size(), this->reduction);
		planes.push_back(pl);
		vector<CALSPoint *> v;
		v.push_back(pOutlier);
		pointVec.push_back(v);
		C2DPoint obs;
		this->imageTrafo.transformObj2Obs(obs, pOutlier->getLastEcho());
		long rmin = long(floor(obs.y + 0.5)) - labels.getOffsetY();
		long cmin = long(floor(obs.x + 0.5)) - labels.getOffsetX();
		int w = 1 + 2 * (int) floor(scale + 0.5);
		rmin -= w;
		cmin -= w;
		long rmax = rmin + 2 * w + 1;
		long cmax = cmin + 2 * w + 1;
		unsigned short newLabel = (unsigned short) labels.getNLabels();
		for (int r = rmin; r < rmax; ++r)
		{
			long indx = r * labels.getDiffY();
			for (int c = cmin; c < cmax; ++c)
			{
				labels.pixUShort(indx) = newLabel;
			}
		}
		labels.setBoundariesToZero(false, true, true, 0);
	}
	else
	{*/
		ostrstream of;
		of.setf(ios::fixed, ios::floatfield);			
		of.precision(2);
						
		of <<"\n Splitting Plane " << plane << "\n";
		
		CXYContourArray contours;

		if (!this->pCombinedLabels->getContours(contours, plane, false))
		{
			of << "\n no contours found!";
			ret = false;
		}
		else
		{	
			CXYContour *pC = contours.GetAt(0);
			CXYPolyLine poly;
			CXYZPolyLine poly3D;
			poly.initFromContour(pC, scale, 1.0);
			poly.closeLine();
	//		of << "\n\nPoly: \n ";
			CXYZPoint Pobj;
			CXYZPoint *P0 = 0;
			C2DPoint diff;
			CGenericPointCloud ptCld(pC->getPointCount(), 3, 3);


			for (int i = 0; i < poly.getPointCount(); ++i)
			{
				Pobj.x = poly.getPoint(i).x;
				Pobj.y = poly.getPoint(i).y;
				if (P0)
				{
					diff.x = P0->x - Pobj.x;
					diff.y = P0->y - Pobj.y;
					if (diff.getSquareNorm() > FLT_EPSILON) 
					{
						poly3D.addPoint(Pobj);
						ptCld.addPoint(Pobj);
					}
				}
				else 
				{
					poly3D.addPoint(Pobj);
					ptCld.addPoint(Pobj);
					P0 = poly3D.getPoint(0);
				}
	//			of << "\n" << Pobj.getString(" ", 7, 2);
			}

			poly3D.closeLine();

			ptCld.Triangulate();
				
			ostrstream dxfstr;
			dxfstr << this->outputPath.GetChar() << "plane_" << plane << "_tri.dxf" << ends;
			char *z = dxfstr.str(); 
			ptCld.exportTriangulation(z);
			delete [] z;

			CLabelImage planeTriang(*this->pCombinedLabels);
			planeTriang.setAll(0.0f);

			for (int i = 0; i < ptCld.getNTriangles(); ++i)
			{
				CXYPolyLine poly = ptCld.getTriangle2D(i);
				planeTriang.fill(poly, i + 1);
			}

			planeTriang.initPixelNumbers();
			planeTriang.updateVoronoi();

			for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
			{
				if (this->pCombinedLabels->getLabel(i) != plane) planeTriang.pixUShort(i) = 0;
				else planeTriang.pixUShort(i) = planeTriang.getVoronoi(i);
			}

			int structure = (int) floor(scale + 0.5);
			if (structure < 2) structure = 3;
			planeTriang.morphologicalOpen(structure,0);

			for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
			{
				if (this->pCombinedLabels->getLabel(i) != plane) planeTriang.pixUShort(i) = 0;
				else planeTriang.pixUShort(i) = planeTriang.getVoronoi(i);
			}

			planeTriang.renumberLabels();
			planeTriang.initPixelNumbers();
			planeTriang.updateVoronoi();

			ostrstream tifstr;
			tifstr << "plane_" << plane << "_tri" << ends;
			z = tifstr.str(); 
			planeTriang.dump(this->outputPath.GetChar(),z);
			delete [] z;

			vector<vector<CALSPoint *>> triangPointVec(planeTriang.getNLabels());
			vector<CBuildingFace *> triangPlanes(planeTriang.getNLabels());

			for (unsigned int i = 1; i < triangPlanes.size(); ++i)
			{
				triangPlanes[i] = new CBuildingFace;
				triangPlanes[i]->resetSums(this->reduction);
			}

			C2DPoint obs;
			for (unsigned int i = 0; i < pointVec[plane].size(); ++i)
			{
				C3DPoint p = pointVec[plane][i]->getLastEcho();
				long index = MultiImageSegmenter::getObjectLabelIndex(p);
				unsigned short newlabel = planeTriang.getLabel(index);
				if (newlabel > 0)
				{
					triangPointVec[newlabel].push_back(pointVec[plane][i]);
					triangPlanes[newlabel]->addPoint(p);
				}
			}

			for (unsigned int i = 1; i < triangPointVec.size(); ++i)
			{
				unsigned int s = triangPointVec[i].size();
				of << "\n   triangle " << i << ": " << s << " points";
				if (s > 0)
				{
					for (unsigned int j = 0; j < triangPointVec[i].size(); ++j)
					{
						C3DPoint p = triangPointVec[i][j]->getLastEcho();
						of << "\n" << p.getString(" ", 7, 2).GetChar();
					}
					if (s >= 3)
					{
						if (s == 3)
						{
							CXYZPoint p0, p1,p2;
							p0 = triangPointVec[i][0]->getLastEcho();
							p0.setSX(this->sigmaALS);  p0.setSY(this->sigmaALS);  p0.setSZ(this->sigmaALS);
							p1 = triangPointVec[i][1]->getLastEcho();
							p1.setSX(this->sigmaALS);  p1.setSY(this->sigmaALS);  p1.setSZ(this->sigmaALS);
							p2 = triangPointVec[i][2]->getLastEcho();
							p2.setSX(this->sigmaALS);  p2.setSY(this->sigmaALS);  p2.setSZ(this->sigmaALS);
							triangPlanes[i]->initFromPoints(p0, p1, p2, &this->reduction);
							of <<"\n      Plane " << i << " ( " 
							   << s << " points)";
						}				
						else 
						{
							triangPlanes[i]->computeFacePars();
							double s0 = triangPlanes[i]->getSigma0();
							double threshSigma = this->getSigmaThreshold((int) triangPlanes[i]->getRedundancy());

							of <<"\n      Plane " << i << " from adjustment ( " 
							   << s << " points): sigma0 = " << s0 << ", threshold: " << threshSigma;

						}
					}
				}
			}

			this->mergePlanes(plane, planeTriang, triangPlanes, triangPointVec);

			unsigned short diffLabels = (unsigned short) labels.getNLabels() - 2;

			*planes[plane]   = *triangPlanes[1];
			planes[plane]->setIndex(plane);
			planes[plane]->setTrafo(this->imageTrafo);

			pointVec[plane] = triangPointVec[1];
			for (unsigned int i = 2; i < triangPlanes.size(); ++i)
			{
				triangPlanes[i]->setIndex(diffLabels + i);
				triangPlanes[i]->setTrafo(this->imageTrafo);
				planes.push_back(triangPlanes[i]);
				pointVec.push_back(triangPointVec[i]);
				triangPlanes[i] = 0;
			}

			for (unsigned int i = 0; i < labels.getNGV(); ++i)
			{
				if (labels.getLabel(i) == plane)
				{
					unsigned short nl = planeTriang.getLabel(i);
					if (!nl) labels.pixUShort(i) = 0;
					else if (nl > 1) labels.pixUShort(i) = diffLabels + nl;
				}
			}

			labels.initPixelNumbers();
			labels.updateVoronoi();
			ostrstream tifstr2;
			tifstr2 << "plane_" << plane << "_mrg" << ends;
			z = tifstr2.str(); 
			planeTriang.dump(this->outputPath.GetChar(),z);
			delete [] z;

			this->resetPlanes(triangPlanes);
		}


		protHandler.print(of, protHandler.prt);
//    }
	return ret;
};
	  
//=============================================================================

void CBuildingReconstructor::mergePlanes(int Lab, CLabelImage &labels, 
										 vector<CBuildingFace *> &planes, 
										 vector<vector<CALSPoint *>> &pointVec)
{
	if (planes.size() > 1)
	{
		int nChanged = 1;
		unsigned int maxPoints = 4;
		do
		{
			if (!nChanged) maxPoints = 3;

			nChanged = 0;
			bool  **isNeighbour = new bool*  [planes.size()];
			for (unsigned int j = 0; j < planes.size(); ++j)
			{
				isNeighbour[j] = new bool  [planes.size()];
				memset(isNeighbour[j], 0, planes.size() * sizeof(bool));
			}

			for (unsigned int uMin = labels.getDiffY() + 1, iMaxRow = labels.getDiffY() + labels.getDiffY(); 
				 uMin < labels.getNGV(); uMin += labels.getDiffY(), iMaxRow += labels.getDiffY())
			{
				for (unsigned int i = uMin; i < iMaxRow; i += labels.getDiffX())
				{
					unsigned short label00 = labels.getLabel(i);
					if (label00)
					{
						unsigned short label10 = labels.getLabel(i - labels.getDiffX());
						unsigned short label01 = labels.getLabel(i - labels.getDiffY());
						if (label10 && (label10 != label00))
						{
							isNeighbour[label10][label00] = true;
							isNeighbour[label00][label10] = true;
						}

						if (label01 && (label01 != label00))
						{
							isNeighbour[label01][label00] = true;
							isNeighbour[label00][label01] = true;
						}
					}
				}
			}

			float distMin = FLT_MAX;
			unsigned int iMin, jMin;

			for (unsigned int i = 1; i < planes.size(); ++i)
			{
				for (unsigned int j = i + 1; j < planes.size(); ++j)
				{
					if ((isNeighbour[i][j]) && (planes[i]->getNPoints() >= maxPoints) && (planes[j]->getNPoints() >= maxPoints))
					{
						float s0;
						float dist = planes[i]->isCoplanar(*planes[j], s0, this->sigmaALS);
						long red = (long) (planes[i]->getRedundancy() + planes[j]->getRedundancy() + 3);
						double threshSigma = this->getSigmaThreshold(red);

						if ((dist > 0.0) && (dist < distMin) && (s0 < threshSigma))
						{
							iMin = i;
							jMin = j;
							distMin = dist;
						}
					}
				}
			}

			if ((distMin < 100.0) && (distMin > 0.0))
			{
				for (unsigned int i = 0; i < pointVec[jMin].size(); ++i)
				{
					pointVec[iMin].push_back(pointVec[jMin][i]);
				}

				pointVec[jMin].clear();
				planes[jMin]->resetSums(this->reduction);
				planes[iMin]->resetSums(this->reduction);

				for (unsigned int i = 0; i < pointVec[iMin].size(); ++i)
				{
					planes[iMin]->addPoint(pointVec[iMin][i]->getLastEcho());
				}
				
				planes[iMin]->computeFacePars();

				labels.mergeLabels(jMin, iMin, false);

				nChanged = 1;
			}

			for (unsigned int j = 0; j < planes.size(); ++j)
			{
				delete [] isNeighbour[j];
			}
			
			delete [] isNeighbour;
		} while ((nChanged > 0) || (maxPoints > 3));

		for (unsigned int i = 0; i < labels.getNGV(); ++i)
		{
			unsigned short l = labels.getLabel(i);
			if (l)
			{
				if (pointVec[l].size() < 1) labels.pixUShort(i) = 0;
			}
		}

		labels.initPixelNumbers();
		labels.updateVoronoi();
		
		if (Lab > 0)
		{
			for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
			{
				if (this->pCombinedLabels->getLabel(i) != Lab) labels.pixUShort(i) = 0;
				else labels.pixUShort(i) = labels.getVoronoi(i);
			}
		}
		CXYZPoint P;

		do
		{
			nChanged = 0;
			bool  **isNeighbour = new bool*  [planes.size()];
			for (unsigned int j = 0; j < planes.size(); ++j)
			{
				isNeighbour[j] = new bool  [planes.size()];
				memset(isNeighbour[j], 0, planes.size() * sizeof(bool));
			}

			for (unsigned int uMin = labels.getDiffY() + 1, iMaxRow = labels.getDiffY() + labels.getDiffY(); 
				 uMin < labels.getNGV(); uMin += labels.getDiffY(), iMaxRow += labels.getDiffY())
			{
				for (unsigned int i = uMin; i < iMaxRow; i += labels.getDiffX())
				{
					unsigned short label00 = labels.getLabel(i);
					if (label00)
					{
						unsigned short label10 = labels.getLabel(i - labels.getDiffX());
						unsigned short label01 = labels.getLabel(i - labels.getDiffY());
						if (label10 && (label10 != label00))
						{
							isNeighbour[label10][label00] = true;
							isNeighbour[label00][label10] = true;
						}

						if (label01 && (label01 != label00))
						{
							isNeighbour[label01][label00] = true;
							isNeighbour[label00][label01] = true;
						}
					}
				}
			}

			float distMin = FLT_MAX;
			unsigned int iMin, jMin;

			for (unsigned int i = 1; i < planes.size(); ++i)
			{
				if (planes[i]->getNPoints() == 1)
				{
					for (unsigned int j = 1; j < planes.size(); ++j)
					{
						if ((isNeighbour[i][j]) && (planes[j]->getNPoints() >= maxPoints))
						{
							P = pointVec[i][0]->getLastEcho();
							P.setSX(this->sigmaALS); P.setSY(this->sigmaALS); P.setSZ(this->sigmaALS); 
							float dist = planes[j]->isIncident(P);
							if ((dist > 0.0) && (dist < distMin))
							{
								iMin = i;
								jMin = j;
								distMin = dist;
							}
						}
					}
				}
			}

			if ((distMin < 1.0) && (distMin > 0.0))
			{
				for (unsigned int i = 0; i < pointVec[jMin].size(); ++i)
				{
					pointVec[iMin].push_back(pointVec[jMin][i]);
				}

				pointVec[jMin].clear();
				planes[jMin]->resetSums(this->reduction);
				planes[iMin]->resetSums(this->reduction);

				for (unsigned int i = 0; i < pointVec[iMin].size(); ++i)
				{
					planes[iMin]->addPoint(pointVec[iMin][i]->getLastEcho());
				}
				
				planes[iMin]->computeFacePars();

				labels.mergeLabels(jMin, iMin, false);

				nChanged = 1;
			}

			for (unsigned int j = 0; j < planes.size(); ++j)
			{
				delete [] isNeighbour[j];
			}
			
			delete [] isNeighbour;
		} while (nChanged > 0);

		if (Lab > 0)
		{
			for (unsigned int i = 0; i < labels.getNGV(); ++i)
			{
				unsigned short l = labels.getLabel(i);
				if (l)
				{
					if (pointVec[l].size() < 1) labels.pixUShort(i) = 0;
				}
			}
		}

		labels.renumberLabels();
		labels.initPixelNumbers();
		labels.updateVoronoi();
		int currSize = 1;

		vector<CBuildingFace *> planesToBeDeleted;

		for (unsigned int i = 1; i < pointVec.size(); ++i)
		{
			if (pointVec[i].size())
			{
				pointVec[currSize] = pointVec[i];
				planes[currSize]   = planes[i];
				++currSize;
			}
			else planesToBeDeleted.push_back(planes[i]);
		}
		for (unsigned int i = 0; i < planesToBeDeleted.size(); ++i)
		{
			delete planesToBeDeleted[i];
			planesToBeDeleted[i] = 0;
		}

		planes.resize(currSize);
		pointVec.resize(currSize);

		if (Lab > 0)
		{
			for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
			{
				if (this->pCombinedLabels->getLabel(i) != Lab) labels.pixUShort(i) = 0;
				else labels.pixUShort(i) = labels.getVoronoi(i);
			}
		}

		labels.setBoundariesToZero(false, false, false, 0);
		if (Lab > 0)
		{
			unsigned short labelToKeep = 0;
			int nMax = 0;
			for (unsigned int i = 1; i < planes.size(); ++i)
			{
				int nPts = planes[i]->getNPoints();
				if (nPts== 3)
				{
					if (nMax < nPts) 
					{
						nMax = nPts;
						labelToKeep = i;
					}
				}
				else if (nPts)
				{
					double threshSigma = this->getSigmaThreshold((int) planes[i]->getRedundancy());
					if (planes[i]->getSigma0() < threshSigma)
					{
						if (nPts > nMax) 
						{
							nMax = nPts;
							labelToKeep = i;
						}
					}
				};
			}
			if (labelToKeep < 1) labelToKeep = 1;
			if (labelToKeep != 1)
			{
				 labels.exchangeLabels(1, labelToKeep);
				 CBuildingFace *pl = planes[1];
				 planes[1] = planes[labelToKeep];
				 planes[labelToKeep] = pl;
				 vector<CALSPoint *> v = pointVec[1];
				 pointVec[1] = pointVec[labelToKeep];
				 pointVec[labelToKeep] = v;
			}
		}
	}

};
	  
//=============================================================================

void CBuildingReconstructor::exportNPts(const CCharString &filename)
{
	bool ret = true;

	CImageBuffer exportBuf(0,0,this->pCombinedLabels->getWidth(), this->pCombinedLabels->getHeight(),1,8,true, 255.0f);

	for (unsigned int k = 0; k < exportBuf.getNGV(); ++k)
	{
		unsigned short label = this->pCombinedLabels->getLabel(k);
		if (label)
		{
			unsigned int nPts = this->pointsInPlanes[label].size();
			unsigned char cPts = 254;
			if (nPts < 254) cPts = (unsigned char) nPts;
			exportBuf.pixUChar(k) = nPts;
		}
	}

	exportBuf.exportToTiffFile(filename.GetChar(), 0, false);
};

//=============================================================================
	  
void CBuildingReconstructor::exportNPoints(const CCharString &filename, 
										   const vector<CBuildingFace *> &planeVector, 
										   const CLabelImage &labels) const
{
	CImageBuffer exportBuf(0, 0, labels.getWidth(), labels.getHeight(),1,8,true, 255.0f);

	for (unsigned int k = 0; k < exportBuf.getNGV(); ++k)
	{
		unsigned short label = labels.getLabel(k);
		if (label)
		{
			int nPts = planeVector[label]->getNPoints();
			unsigned char cPts = 254;
			if (nPts < 254) cPts = (unsigned char) nPts;
			exportBuf.pixUChar(k) = nPts;
		};
	}

	exportBuf.exportToTiffFile(filename.GetChar(), 0, false);
};

//=============================================================================

bool CBuildingReconstructor::computePlanes(const CCharString &heading, 
		                                   CLabelImage &labels, 
										   vector<CBuildingFace *> &planes,
										   vector<vector<CALSPoint *>> &pointVec,
										   bool writeToProtocol,
						                   bool splitBadFits)
{
	bool ret = true;

	if (this->ALSpoints)
	{
		if (planes.size() > labels.getNLabels())
		{
			for (unsigned int i = labels.getNLabels(); i < planes.size(); ++i)
			{
				delete planes[i];
				planes[i] = 0;
			}
			planes.resize(labels.getNLabels());
		}
		else if (planes.size() < labels.getNLabels())
		{
			planes.resize(labels.getNLabels(), 0);
		}
		for (unsigned int i = 0; i < planes.size(); ++i)
		{
			if (!planes[i]) 
			{
				planes[i]  = new CBuildingFace;
				planes[i]->setTrafo(this->imageTrafo);
			}
		}				

		pointVec.resize(planes.size());
			
		for (unsigned int i = 1; i < planes.size(); ++i)
		{
			planes[i]->resetData(i, this->reduction);
		}

		for (int j = 0; j < this->ALSpoints->getNpoints(); ++j)
		{
			C3DPoint p = this->ALSpoints->getPoint(j)->getLastEcho();
			if (p.z - this->getDTMHeight(p) > this->minHeight)
			{
				C2DPoint obs;
				this->imageTrafo.transformObj2Obs(obs, p);
				obs.x = floor(obs.x - labels.getOffsetX() + 0.5);
				obs.y = floor(obs.y - labels.getOffsetY() + 0.5);
				if ((obs.x >= 1) && (obs.x < labels.getWidth()) && 
					(obs.y >= 1) && (obs.y < labels.getHeight()))
				{
					long index = labels.getIndex(long(obs.y), long(obs.x));
					unsigned short label = labels.getLabel(index);
					if (label)
					{
						planes[label]->addPoint(p);
						pointVec[label].push_back(this->ALSpoints->getPoint(j));
					}
				}
			}
		}

		for (unsigned int j = 1; j < planes.size(); ++j)
		{
			int nPts = planes[j]->getNPoints();

			if (nPts > 3) 
			{
				planes[j]->computeFacePars();
				double sigma = planes[j]->getSigma0();
				double threshSigma = this->getSigmaThreshold((int) planes[j]->getRedundancy());

				if (writeToProtocol)
				{
					ostrstream of;
					of.setf(ios::fixed, ios::floatfield);
					of.precision(2);
					of <<"\n " << heading << " Plane " << j << " from adjustment ( " 
						<< nPts << " points): sigma0 = " << sigma << ", threshold: " << threshSigma;
					protHandler.print(of, protHandler.prt);
					protHandler.print(planes[j]->getDumpString(),protHandler.prt);
				}

				if ((nPts > 7) && (sigma > threshSigma) && (splitBadFits))
				{
					this->splitBadFits(labels, planes, pointVec, j, writeToProtocol);
				}
			}
			else if (nPts == 3)
			{
				CXYZPoint P1(pointVec[j][0]->getLastEcho());
				CXYZPoint P2(pointVec[j][1]->getLastEcho());
				CXYZPoint P3(pointVec[j][2]->getLastEcho());
				P1.setSX(this->sigmaALS);   P1.setSY(this->sigmaALS);   P1.setSZ(this->sigmaALS);
				P2.setSX(this->sigmaALS);   P2.setSY(this->sigmaALS);   P2.setSZ(this->sigmaALS);
				P3.setSX(this->sigmaALS);   P3.setSY(this->sigmaALS);   P3.setSZ(this->sigmaALS);

				planes[j]->initFromPoints(P1,P2,P3, &this->reduction);
				if (writeToProtocol)
				{
					ostrstream of;
					of.setf(ios::fixed, ios::floatfield);
					of.precision(2);
					of <<"\n " << heading << " Plane " << j << " from " << nPts << " points";
					protHandler.print(of, protHandler.prt);
					protHandler.print(planes[j]->getDumpString(),protHandler.prt);
				}

			}
			else planes[j]->resetPlane();
		}
	}
	
//	if (splitBadFits) this->mergePlanes(-1, labels, planes, pointVec);

	return ret;
}
	  
//=============================================================================

void CBuildingReconstructor::setDSMHeightsToPlanes()
{
	for (unsigned int j = 1; j < this->planeGraph->getNFaces(); ++j)
	{
		CBuildingFace *face = this->planeGraph->getFace(j);
		if (face->getNPoints() >= 3)
		{
			double sigma = face->getSigma0();

			double threshSigma = this->getSigmaThreshold((int) face->getRedundancy());

			sigma /= threshSigma;
			C2DPoint obs;
			C3DPoint obj;
			for (int row = 0; row < this->pDSM->getHeight(); ++row)
			{
				int i0 = this->pDSM->getDiffY() * row;

				for (int col = 0; col < this->pDSM->getHeight(); ++col)
				{
					obs.x = col + this->pDSM->getOffsetX();
					obs.y = row + this->pDSM->getOffsetY();
					this->DSMtrafo.transformObs2Obj(obj, obs, 0.0);
					long index = MultiImageSegmenter::getObjectLabelIndex(obj);
					if (index >= 0)
					{
						unsigned short label = this->pCombinedLabels->getVoronoi(index);
						if ((label == j) && (this->pCombinedLabels->getDistance(index) <= 10))
						{
							obs.x = obj.x;
							obs.y = obj.y;
							obj.z = face->getHeight(obs);
							this->pDSMPlanes->pixFlt(i0 + col) = float(obj.z);
						}
					}
				}
			}
		}
	};
};

//=============================================================================

bool CBuildingReconstructor::computePlanes(bool writeToProtocol)
{
	bool ret = true;

	if (this->ALSpoints)
	{
		for (unsigned int i = 0; i < this->objectLabelVector.size(); ++i)
		{
			ostrstream of;
			of.setf(ios::fixed, ios::floatfield);
			of.precision(2);
			of <<"\n Image " << i << ", " << ends;
			char *z = of.str();
			CCharString heading (z);
			delete [] z;

			vector<CBuildingFace *> planes(this->objectLabelVector[i]->getNLabels());
			vector<vector<CALSPoint *>> pointVec(planes.size());
			
			this->computePlanes(heading, *this->objectLabelVector[i], planes, pointVec, writeToProtocol, false);

			for (unsigned int j = 1; j < planes.size(); ++j)
			{
				if (planes[j]->getNPoints() > 3)
				{
					double sigma = planes[j]->getSigma0();

					double q = statistics::chiSquareFractil(1.0f - alpha, (long) planes[j]->getRedundancy());
					double threshSigma = this->sigmaALS * sqrt(q / planes[j]->getRedundancy());

					sigma /= threshSigma;
					if (sigma < 1.0)
					{
						C2DPoint obs;
						C3DPoint obj;
						for (int row = 0; row < this->pDSM->getHeight(); ++row)
						{
							int i0 = this->pDSM->getDiffY() * row;

							for (int col = 0; col < this->pDSM->getHeight(); ++col)
							{
								obs.x = col + this->pDSM->getOffsetX();
								obs.y = row + this->pDSM->getOffsetY();
								this->DSMtrafo.transformObs2Obj(obj, obs, 0.0);
								this->imageTrafo.transformObj2Obs(obs, obj);
								obs.x = floor(obs.x + 0.5) - objectLabelVector[i]->getOffsetX();
								obs.y = floor(obs.y + 0.5) - objectLabelVector[i]->getOffsetY();
								if ((obs.x >= 1) && (obs.y < this->objectLabelVector[i]->getWidth()) && 
									(obs.y >= 1) && (obs.y < this->objectLabelVector[i]->getHeight()))
								{
									long index = this->objectLabelVector[i]->getIndex(long(obs.y), long(obs.x));
									unsigned short label = this->objectLabelVector[i]->getLabel(index);
									if (label == j)
									{
										obs.x = obj.x;
										obs.y = obj.y;
										obj.z = planes[j]->getHeight(obs);
										this->pDSMPlanes->pixFlt(i0 + col) = float(obj.z);
									}
								}
							}
						}
					}
				}
			}

			this->imageLabelVector[i]->removeEmptyLabels(true);
			this->objectLabelVector[i]->removeEmptyLabels(true);
			this->resetPlanes(planes);
		}
	}

	return ret;
};

//=============================================================================

bool CBuildingReconstructor::computePlanesWithDSM(bool writeToProtocol)
{
	bool ret = true;
	
	double w = 1.0 / (this->sigmaALS * this->sigmaALS);

	for (unsigned int i = 1; i < this->planeGraph->getNFaces(); ++i)
	{
		if (this->pointsInPlanes[i].size() < 3)
		{
			CBuildingFace *face = this->planeGraph->getFace(i);

			face->resetSums(this->reduction);
			for (unsigned int j = 0; j < this->pointsInPlanes[i].size(); ++j)
			{
				face->addPoint(this->pointsInPlanes[i][j]->getLastEcho(), w);
			}
		};
	};


	w = (this->pmax.z - this->pmin.z) / 6.0;		
	w = 1.0 / (w * w);
		
	for (unsigned int j = 0; j < this->pCombinedLabels->getNGV(); ++j)
	{
		unsigned short l = this->pCombinedLabels->getLabel(j);
		if (this->pointsInPlanes[l].size() < 3)
		{
			long r, c, b;
			this->pCombinedLabels->getRowColBand(j, r, c, b);
			C3DPoint obj;
			C2DPoint p((double) c, (double) r);
			float z;
			this->pDSM->getInterpolatedColourVecBilin(r,c,&z);
			this->imageTrafo.transformObs2Obj(obj, p, z);
			this->planeGraph->getFace(l)->addPoint(obj, w);
		};
	}

	for (unsigned int i = 1; i < this->planeGraph->getNFaces(); ++i)
	{
		if (this->pointsInPlanes[i].size() < 3)
		{
			this->planeGraph->getFace(i)->computeFacePars();
		};
	};

	return ret;
};
	 	  

//=============================================================================

bool CBuildingReconstructor::findMatchingCandidatesInImages(const CXYZPoint &P1, const CXYZPoint &P2, 
												            vector <vector <CXYLineSegment *> > &imageEdges,
															vector<vector<int> > &indexVector,
															vector<ofstream *> &dxfVec)
{
	float angleThresh = (float) cos(45.0 / 180.0 * M_PI);
	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		imageEdges[i].clear();
		indexVector[i].clear();

		CXYPoint pimg1, pimg2;
		this->sensorModelVector[i]->transformObj2Obs(pimg1, P1);
		this->sensorModelVector[i]->transformObj2Obs(pimg2, P2);
		pimg1.x -= this->imageOffsets[i].x;
		pimg1.y -= this->imageOffsets[i].y;
		pimg2.x -= this->imageOffsets[i].x;
		pimg2.y -= this->imageOffsets[i].y;
		CXYLineSegment imgSeg(pimg1, pimg2);
		imgSeg.writeDXF(*dxfVec[i], "BACK", 2, 2, 1,-1);

		for (unsigned int e = 0; e < this->imageStraightLineVector[i].size(); ++e)
		{
			double inner = imgSeg.getNormal().innerProduct(this->imageStraightLineVector[i][e]->getNormal());
			if (fabs(inner) > angleThresh)
			{
				double Overlap;
				double dist = imgSeg.getMatchDist(*(this->imageStraightLineVector[i][e]), Overlap); 
				
//				float dist = imgSeg.isIdenticalAndOverlaps(*(this->imageStraightLineVector[i][e]), Overlap);
				if ((dist > 0.0f ) && (dist <= 5.0f) && (Overlap > 0.1))//(Overlap > 2.0))
				{
					imageEdges[i].push_back(this->imageStraightLineVector[i][e]);
					indexVector[i].push_back(e);
				}
			}
		}			
	}

	return true;
};


//=============================================================================

void CBuildingReconstructor::simplifyMultipleCandidates(CBuildingFace *pFace, 
														vector <CLineMatchingHypothesis *> &candidates)
{
	bool merged = false;
	double minOvlp = 0.5;

	do
	{
		merged = false;
		int iMerge1 = -1;
		int iMerge2 = -1;
		double sMin = FLT_MAX; 

		for (unsigned int c1 = 0; c1 < candidates.size(); ++c1)
		{
			for (unsigned int c2 = c1 + 1; c2 < candidates.size(); ++c2)
			{
				if (candidates[c1]->overlaps(*candidates[c2], minOvlp)) 
				{
					CLineMatchingHypothesis mergedCandidate(*candidates[c1]);
					mergedCandidate.merge(*candidates[c2]);
					if (mergedCandidate.computeIntersection(this->sensorModelVector, this->imagePixelSize, this->imageOffsets, pFace))
					{
						if (mergedCandidate.s0 < sMin)
						{
							sMin = mergedCandidate.s0;
							iMerge1 = c1;
							iMerge2 = c2;
						}
					}
				}
			}
		}

		if ((iMerge1 >= 0) && (iMerge2 >= 0) && (sMin < 1.5))
		{
			if ((sMin  < 2.5 * candidates[iMerge1]->s0) && (sMin  < 2.5 * candidates[iMerge2]->s0)) 
			{
				merged = true;
				CLineMatchingHypothesis *mergedCandidate = new CLineMatchingHypothesis(*candidates[iMerge1]);
				mergedCandidate->merge(*candidates[iMerge2]);
				mergedCandidate->computeIntersection(this->sensorModelVector, this->imagePixelSize, this->imageOffsets, pFace);
				if (iMerge1 > iMerge2) 
				{ 
					int k = iMerge1; iMerge1 = iMerge2; iMerge2 = k; 
				}

				delete candidates[iMerge1];
				candidates[iMerge1] = mergedCandidate;
				delete candidates[iMerge2];
				for (unsigned int k = iMerge2 + 1; k < candidates.size(); ++k)
				{
					candidates[k - 1] = candidates[k];
				}

				candidates.resize(candidates.size() - 1);
			}
		}

	} while (merged && (candidates.size() > 1)); 

	if (candidates.size() > 1)
	{
		for (unsigned int c1 = 0; c1 < candidates.size(); ++c1)
		{
			for (unsigned int c2 = c1 + 1; c2 < candidates.size(); ++c2)
			{
				if (candidates[c1]->overlaps(*candidates[c2], minOvlp)) 
				{
					int iMin;
					if (candidates[c1]->s0 <= candidates[c2]->s0)
					{
						delete candidates[c2];
						iMin = c2;
					}
					else
					{
						delete candidates[c1];
						iMin = c1;
					}

					for (unsigned int k = iMin + 1; k < candidates.size(); ++k)
					{
						candidates[k - 1] = candidates[k];
					}

					candidates.resize(candidates.size() - 1);
				}
			}
		}
	}
/*
		int identical = -1;
	for (unsigned int c = 0; (c < candidates.size()) && (identical < 0); ++c)
    {
						if (candidates[c]->overlaps(*h, minOvlp)) identical = (int) c;
					}
					if (identical >= 0)
					{
						CLineMatchingHypothesis *mergedCandidate = new CLineMatchingHypothesis(*candidates[identical]);
						mergedCandidate->merge(*h);
						bool mergedOK = mergedCandidate->computeIntersection(this->sensorModelVector, this->imagePixelSize, this->imageOffsets, pFace);

						if (mergedOK)
						{
							if (((mergedCandidate->s0 > 2.0 * h->s0) || 
								 (mergedCandidate->s0 > 2.0 * candidates[identical]->s0)) && 
								 (mergedCandidate->s0 > 1.0)) mergedOK = false;
						}

						if (mergedOK)
						{
							delete candidates[identical];
							candidates[identical] = mergedCandidate;
							delete h;
						}
						else 
						{
							if (candidates[identical]->s0 <= h->s0)
							{
								delete h;
							}
							else
							{
								delete candidates[identical];
								candidates[identical] = h;
							}
						}
					}
					else candidates.push_back(h);
					*/
}

//=============================================================================

bool CBuildingReconstructor::findMatchingCandidates(const CXYZPoint &P1, const CXYZPoint &P2, 
													CBuildingFace *pFace, CBuildingFaceNeighbourRecord *pNeighbour, 
													vector <CLineMatchingHypothesis *> &candidates,
													float minLength,
													vector<ofstream *> &dxfVec)
{
	bool ret = true;
	candidates.clear();

	vector<vector<CXYLineSegment *>> segmentVec(this->imageVector.size());			
	vector<vector<int>> indexVec(this->imageVector.size());
			
	findMatchingCandidatesInImages(P1, P2, segmentVec, indexVec, dxfVec);

	vector<CXYLineSegment *> parVec(this->imageVector.size());

	for (unsigned int imgIdx1 = 0; imgIdx1 < this->imageVector.size(); ++imgIdx1)
	{
		for(unsigned int imgIdx2 = imgIdx1 + 1; imgIdx2 < this->imageVector.size(); ++imgIdx2)
		{
			for (unsigned int img1 = 0; img1 < segmentVec[imgIdx1].size(); ++img1)
			{
				for (unsigned int img2 = 0; img2 < segmentVec[imgIdx2].size(); ++img2)
				{
					CLineMatchingHypothesis *h = new CLineMatchingHypothesis(this->imageVector.size());
					h->imageEdges[imgIdx1].push_back(segmentVec[imgIdx1][img1]);
					h->imageEdges[imgIdx2].push_back(segmentVec[1][img2]);
					h->indexVector[imgIdx1].push_back(indexVec[imgIdx1][img1]);
					h->indexVector[imgIdx2].push_back(indexVec[imgIdx2][img2]);
					h->objectEdgeDescriptor = pNeighbour;
					if (h->computeIntersection(this->sensorModelVector, this->imagePixelSize, this->imageOffsets, pFace))
					{
						if (h->pObjectEdge->getLength() < minLength)
						{
							delete h;
						}
						else
						{ 
							candidates.push_back(h);
						}
					}
					else
					{
						delete h;
					}
				};
			};
		};
	}
	if (candidates.size() > 1) this->simplifyMultipleCandidates(pFace, candidates);

	return (candidates.size() > 0);
};

//=============================================================================

bool CBuildingReconstructor::findMatchingCandidates(CBuildingFace *pFace, 
													CBuildingFaceNeighbourRecord *pNeighbour, 
													vector <CLineMatchingHypothesis *> &candidates,
													float minLength,
													vector<ofstream *> &dxfVec)
{
	bool ret = true;
	CXYPolyLine *poly = pNeighbour->getApproximatePoly();
	candidates.clear();
	double minOvlp = 0.5;

	if (poly)
	{
		for (int pt = 1; pt < poly->getPointCount(); ++pt)
		{
			CXYPoint p1 = poly->getPoint(pt - 1);
			CXYPoint p2 = poly->getPoint(pt);
			if (p1.getDistance(p2) > minLength)
			{
				CXYZPoint P1(p1.x + this->reduction.x, p1.y + this->reduction.y, 0.0);
				CXYZPoint P2(p2.x + this->reduction.x, p2.y + this->reduction.y, 0.0);
				P1.z = pFace->getHeight(P1.x, P1.y);
				P2.z = pFace->getHeight(P2.x, P2.y);
				vector <CLineMatchingHypothesis *> newCandidates;
				if (findMatchingCandidates(P1, P2, pFace, pNeighbour, newCandidates, 
					                       minLength, dxfVec))
				{
					for (unsigned int cand = 0; cand < newCandidates.size(); ++cand)
					{
						CLineMatchingHypothesis *h = newCandidates[cand];

						int identical = -1;
						for (unsigned int c = 0; (c < candidates.size()) && (identical < 0); ++c)
						{
							if (candidates[c]->isIdentical(*h, minOvlp)) identical = (int) c;
						}
						if (identical >= 0)
						{
							CLineMatchingHypothesis *mergedCandidate = new CLineMatchingHypothesis(*candidates[identical]);
							mergedCandidate->merge(*h);
									
							if (mergedCandidate->computeIntersection(this->sensorModelVector, this->imagePixelSize, this->imageOffsets, pFace))
							{
								delete candidates[identical];
								candidates[identical] = mergedCandidate;
								delete h;
							}
							else 
							{
								candidates.push_back(h);
								/*if (candidates[identical]->s0 <= h->s0)
								{
									delete h;
								}
								else
								{
									delete candidates[identical];
									candidates[identical] = h;
								}
								*/
							}
						}						
						else
						{
							candidates.push_back(h);
						}
					};
				};
			};
		};
	};

	return (candidates.size() > 0);
};

//=============================================================================

bool CBuildingReconstructor::matchEdges(CBuildingFace *face, vector<CALSPoint *> &pointVec,
										vector <CLineMatchingHypothesis *> &matches, 
										float minLength, vector<ofstream *> &dxfVec, 
										unsigned int minPts)
{
	bool ret = false;

	for (unsigned int p = 0; p < face->getNPolys(); ++p)
	{
		CBuildingFacePoly *poly = face->getPoly(p);

		for (unsigned int n = 0; n < poly->getNNeighbours(); ++n)
		{
			CBuildingFaceNeighbourRecord *pNeighbour = poly->getNeighbour(n);
			if ((pNeighbour->getRegion1() == 0) || (pNeighbour->getRegion1() >= face->getIndex()) || (pNeighbour->getRegion1() < face->getIndex()))
			{
				vector <CLineMatchingHypothesis *> candidates;

				if (this->pointsInPlanes[face->getIndex()].size() > minPts) 
				{			
					findMatchingCandidates(face, pNeighbour, candidates, minLength, dxfVec);
				}
				
				ostrstream oss;
				oss << "\n Matching candidates for the edge separating regions " 
					<< pNeighbour->getRegion1() << " and " << pNeighbour->getRegion2() << ": \n";

				if (candidates.size() > 0)
				{
					ret = true;
					unsigned int delta = matches.size();
					matches.resize(delta + candidates.size(), 0);

					for (unsigned int c = 0; c < candidates.size(); ++c)
					{
						candidates[c]->print(oss, &dxfVec, c);
												
						matches[delta + c] = candidates[c];
						candidates[c] = 0;
					}
				}
				else
				{
					oss << "No matches found\n";
				}

				protHandler.print(oss, protHandler.prt);
			}
		}
	}

	return ret;
};

//=============================================================================
	  
void CBuildingReconstructor::mergeTwoPlanes(int merge, int toBeMerged)
{
	ostrstream oss;
	oss << "\n adding face " << toBeMerged << " to face " << merge << "\n";

	this->pCombinedLabels->mergeLabels(toBeMerged, merge, false);
	for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
	{
		if ((this->pCombinedLabels->getVoronoi(i) == merge) &&
			(!this->pCombinedLabels->getLabel(i)) &&
			(this->pCombinedLabels->getDistance(i) <= 10))
			this->pCombinedLabels->pixShort(i) = merge;
	}
	
	if (this->pointsInPlanes[toBeMerged].size())
	{
		for (unsigned int j = 0; j < this->pointsInPlanes[toBeMerged].size(); ++j)
		{
			this->pointsInPlanes[merge].push_back(this->pointsInPlanes[toBeMerged][j]);
		}

		this->pointsInPlanes[toBeMerged].clear();
		this->planeGraph->getFace(toBeMerged)->resetData(toBeMerged, this->reduction);
		this->planeGraph->getFace(merge)->resetData(merge, this->reduction);

		for (unsigned int j = 0; j < this->pointsInPlanes[merge].size(); ++j)
		{
			this->planeGraph->getFace(merge)->addPoint(this->pointsInPlanes[merge][j]->getLastEcho());
		}

		this->planeGraph->getFace(merge)->adjust();

		oss << this->planeGraph->getFace(merge)->getDumpString().GetChar() << "\n";
	}

	CCharString s = CCharString(merge, 3, true) + CCharString("_") + CCharString(toBeMerged, 3, true) ;
	this->pCombinedLabels->dump(this->outputPath.GetChar(), "merge", s.GetChar());

	protHandler.print(oss, protHandler.prt);
};

//=============================================================================
	  
int CBuildingReconstructor::checkMergeEdges(CBuildingFace *face, vector <CLineMatchingHypothesis *> &matches)
{
	int nMerged = 0;

	float percentage = 0.5;

	double maxDist = this->imageTrafo.getScaleX() * 3.0;
	CLabelImage faceLab(this->pCombinedLabels->getOffsetX(), this->pCombinedLabels->getOffsetY(), 
		                this->pCombinedLabels->getWidth(), this->pCombinedLabels->getHeight(), 
						this->pCombinedLabels->getNeighbourhood());
	CXYPolyLine imgPoly;
	vector <CLineMatchingHypothesis *> matchesCopy;
	matchesCopy.push_back(matches[0]);

	for (unsigned int i = 1; i < matches.size(); ++i)
	{
		bool found = false;
		for (unsigned int j = 0; (j < matchesCopy.size()) && (!found); ++j)
		{
			unsigned int nId = 0;
			for (unsigned int k = 0; k < this->imageVector.size(); ++k)
			{
				if (matchesCopy[j]->indexVector[k] == matches[i]->indexVector[k]) ++nId;
			}
			if (nId == this->imageVector.size()) found = true;
		}


		for (unsigned int j = 0; (j < matchesCopy.size()) && (!found); ++j)
		{
			if (matches[i]->pObjectEdge->getAverageDistance(*matchesCopy[j]->pObjectEdge) < maxDist)
			{
				if (matches[i]->s0 < matchesCopy[j]->s0)
				{
					matchesCopy[j] = matches[i];
				}	
				found = true;
			}
		}

		if (!found) matchesCopy.push_back(matches[i]);
	}

	CGenericPointCloud ptCld(matchesCopy.size() * 4, 2, 3);

	for (unsigned int i = 0; i < matchesCopy.size(); ++i)
	{
		CXYPoint p1, p2;
		CXYZPoint P = matchesCopy[i]->pObjectEdge->getPoint1();
		P.x += this->reduction.x; P.y += this->reduction.y; 
		this->imageTrafo.transformObj2Obs(p1, P);
		P = matchesCopy[i]->pObjectEdge->getPoint2();
		P.x += this->reduction.x; P.y += this->reduction.y; 
		this->imageTrafo.transformObj2Obs(p2, P);
								
		ptCld.addPoint(p1);
		ptCld.addPoint(p2);
		if (i == 0)
		{
			imgPoly.addPoint(p1);
			imgPoly.addPoint(p2);
		}
		else 
		{
			CXYPoint imgP0 = imgPoly.getPoint(0);
			CXYPoint imgP1 = imgPoly.getPoint(imgPoly.getPointCount() - 1);
			double dist11 = imgP0.getDistance(p1);
			double dist12 = imgP0.getDistance(p2);
			double dist21 = imgP1.getDistance(p1);
			double dist22 = imgP1.getDistance(p2);
			if ((dist11 <= dist12) && (dist11 <= dist21)  && (dist11 <= dist22))
			{
				imgPoly.reversePolyLine();
				imgPoly.addPoint(p1);
				imgPoly.addPoint(p2);
			}
			else if ((dist12 <= dist11) && (dist12 <= dist21)  && (dist12 <= dist22))
			{
				imgPoly.reversePolyLine();
				imgPoly.addPoint(p2);
				imgPoly.addPoint(p1);
			}
			else if ((dist21 <= dist11) && (dist21 <= dist12)  && (dist21 <= dist22))
			{
				imgPoly.addPoint(p1);
				imgPoly.addPoint(p2);
			}
			else
			{
				imgPoly.addPoint(p2);
				imgPoly.addPoint(p1);
			}
		}
	}

	imgPoly.closeLine();

	ptCld.Triangulate();
	for (int i = 0; i < ptCld.getNTriangles(); ++i)
	{
		CXYPolyLine polyTri = ptCld.getTriangle2D(i);
		faceLab.fillByValue(polyTri, face->getIndex());
	}
	CCharString suff(face->getIndex(),3,true);
	faceLab.dump(this->outputPath.GetChar(), "face_poly", suff.GetChar());

	vector<unsigned int> nOvl(this->pCombinedLabels->getNLabels(), 0);
	CImageBuffer mask(0,0,this->pCombinedLabels->getWidth(), this->pCombinedLabels->getHeight(), 1, 1, true, 0.0);

	for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
	{
		unsigned short l1 = faceLab.getLabel(i);
		unsigned short l2 = this->pCombinedLabels->getLabel(i);
		if (l1 && (l2) && (l1 != l2)) nOvl[l2]++;
		if (this->pCombinedLabels->getVoronoi(i)) mask.pixUChar(i) = 255;
	}

	double thresh = 0.667;
	double thresh1 = 0.90;
	for (unsigned int i = 1; i < nOvl.size(); ++i)
	{
		unsigned int nPts = this->pCombinedLabels->getNPixels(i);
		if ((nOvl[i] > 0) && (nPts > 0) && (this->pointsInPlanes[i].size() < 3))
		{
			nPts = (unsigned int) floor(thresh * double(nPts));
			vector<CBuildingFaceNeighbourRecord *> *pVec = face->getNeighbours(i);
			if ((nOvl[i] > nPts) && pVec)
			{
				bool merge = true;

				for (unsigned int nn = 0; (nn  < pVec->size()) && merge; ++nn)
				{
				
					float perc = (*pVec)[nn]->getOverlapPercentage(matchesCopy);
					if (perc > percentage) merge = false;
				}

				for (unsigned int j = 0; (j < this->pointsInPlanes[i].size()) && merge; ++j)
				{
					CXYZPoint p(this->pointsInPlanes[i][j]->getLastEcho());
					p.setSX(this->sigmaALS);
					p.setSY(this->sigmaALS);
					p.setSZ(this->sigmaALS);
					if (face->isIncident(p) < 0.0f) merge = false;
				}
				if (merge)
				{
					float combined;
					if (face->isCoplanar(*(this->planeGraph->getFace(i)), combined, this->sigmaALS) < 0.0) 
					{
						merge = false;
						nPts = (unsigned int) floor(thresh1 * double(this->pCombinedLabels->getNPixels(i)));
						if (this->pointsInPlanes[i].size() > 0) merge = true;
						else if (nOvl[i] > nPts)
						{
							merge = true;
						}
					}
				}
				if (merge)
				{
					this->mergeTwoPlanes(face->getIndex(), i);
					++nMerged;
				}
			}
			if (pVec) delete pVec;
		}
	}

	if (nMerged) 
	{
		for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
		{
			if (!mask.pixUChar(i)) this->pCombinedLabels->pixUShort(i) = 0;
		}
		this->pCombinedLabels->setBoundariesToZero(false, false, false, 0);
		this->planeGraph->initialiseNeighbours(this->pCombinedLabels);
	}
	return nMerged;
};

//=============================================================================

bool CBuildingReconstructor::matchEdges(unsigned int minPts)
{
	bool ret = true;
	CCharString suff(minPts, 1, false); 
	this->pCombinedLabels->dump(this->outputPath.GetChar(), "combined", CCharString("beforeMerging_" + suff).GetChar());
	this->pCombinedLabels->computePixelNumbers();

	double zAvg = (this->pmin.z + this->pmax.z) * 0.5;

	vector<ofstream *> dxfVec(this->imageVector.size());

	CCharString f = this->outputPath + "edges_3D.dxf";
	ofstream dxf3D(f.GetChar());

	CXYZPolyLine::writeDXFHeader(dxf3D);

	for (unsigned int i = 0; i < dxfVec.size(); ++i)
	{
		ostrstream oss;
		oss << this->outputPath.GetChar() << "match_" << i << ".dxf" << ends;
		char *z = oss.str();
		dxfVec[i] = new ofstream(z);
		CXYZPolyLine::writeDXFHeader(*dxfVec[i]);
		delete [] z;
	}

	float minLength = 3.0f * (float) this->imageTrafo.getScaleX();

	for (unsigned int f = 1; f < this->planeGraph->getNFaces(); ++f)
	{
		vector <CLineMatchingHypothesis *> matches;
							  
		if (this->matchEdges(this->planeGraph->getFace(f), this->pointsInPlanes[f], matches, minLength, dxfVec, minPts))
		{
//			if (this->checkMergeEdges(this->planeGraph->getFace(f), matches) > 0) --f;

/*			
			if (pointsInPlanes[i].size() < 3)
			{
				ostrstream oss;
				oss << "\n Computing new plane: " << i << " \n\n old version: " << this->planeVector[i].getDumpString().GetChar() <<"\n";

				this->planeVector[i].resetSums(this->reduction);
				double w = 1.0 / (this->sigmaALS * this->sigmaALS);
				for (unsigned int k = 0; k < this->pointsInPlanes[i].size(); ++k)
				{
					C3DPoint p = this->pointsInPlanes[i][k]->getLastEcho();
					this->planeVector[i].addPoint(p, w);
					oss << "\nALS Point: " << p.getString(" ", 7, 2);
				}

				for (unsigned int k = 0; k < HomogVec.size(); ++k)
				{
					C3DPoint p = HomogVec[k]->getPoint1();
					p += this->reduction;
					oss << "\n3D Edge from: " << p.getString(" ", 7, 2)
						<< ", w: " << sqrt(HomogVec[k]->getQxx1().element(2,2));
					
					w = 1.0 / HomogVec[k]->getQxx1().element(2,2);
					this->planeVector[i].addPoint(p,w);
					p = HomogVec[k]->getPoint2();
					p += this->reduction;
					oss << "  to: " << p.getString(" ", 7, 2)
						<< ", w: " << sqrt(HomogVec[k]->getQxx2().element(2,2));
					w = 1.0 / HomogVec[k]->getQxx2().element(2,2);
					this->planeVector[i].addPoint(p,w);
				}

				double sigma = this->planeVector[i].computeFacePars();
				oss <<"\n Plane " << i << ": " << this->planeVector[i].getDumpString().GetChar() <<"\n";

				protHandler.print(oss, protHandler.prt);
			}
		*/
		};

		for (unsigned int c = 0; c < matches.size(); ++c)
		{
			matches[c]->pObjectEdge->writeDXF(dxf3D, "3D", 1, 2);
			delete matches[c];
		}
	}

	for (unsigned int i = 0; i < dxfVec.size(); ++i)
	{
		CXYZPolyLine::writeDXFTrailer(*dxfVec[i]);
		delete dxfVec[i];
	}
	CXYZPolyLine::writeDXFTrailer(dxf3D);

	setDSMHeightsToPlanes();

	CCharString nptsStr = this->outputPath + "nPts_" + suff + ".tif";
	this->exportNPts(nptsStr);
	return ret;
};
	  	  
//=============================================================================

CLabelImage *CBuildingReconstructor::getLabelWithPointCoverage(vector<CALSPoint *> &pointVec)
{
	CLabelImage *pALSLabels = new CLabelImage(*this->pCombinedLabels);
	pALSLabels->setAll(0);

	C2DPoint obs;
		
	for (unsigned int j = 0; j < pointVec.size(); ++j)
	{
		long index = this->getObjectLabelIndex(pointVec[j]);
		if (index >= 0) pALSLabels->pixUShort(index) = 1;
	}

	pALSLabels->updateVoronoi();

	return pALSLabels;
}

//=============================================================================
	  	  
CLabelImage *CBuildingReconstructor::getProjectedPlaneWithPointCoverage(int imageIndex, CBuildingFace *plane, vector<CALSPoint *> &pointVec)
{
	CLabelImage *labObj = this->objectLabelVector[imageIndex];
	CLabelImage *labImg = this->imageLabelVector[imageIndex];

	vector<CALSPoint *> pV(this->ALSpoints->getNpoints(), 0);
	unsigned int nP = 0;
	double thresh = 2.0 * this->sigmaALS; 
	if (plane->getNPoints() > 3) thresh = plane->getThreshold();
	for (int n = 0; n < this->ALSpoints->getNpoints(); ++ n)
	{
		CALSPoint *pALS = this->ALSpoints->getPoint(n);
		C3DPoint P = pALS->getLastEcho();
		if (plane->getDistance(P) < thresh)
		{
			pV[nP] = pALS;
			++nP;
		}
	}

	pV.resize(nP);

	CLabelImage *pALSLabels = this->getLabelWithPointCoverage(pV);

	unsigned short labelObj = plane->getIndex();

	ostrstream oss; oss << "_" << imageIndex << "_" << labelObj << ends;
	char *z = oss.str();
	CCharString suffix(z);
	delete [] z;
	
	unsigned short maxDist = (unsigned short) floor((this->resolutionALS / this->imageTrafo.getScaleX()) * sqrt(5.0) * 0.5 * 5.0 + 0.5);

	if (outputMode > eMEDIUM)
	{
		bool exportDist = (outputMode > eMEDIUM);
		pALSLabels->dump(this->outputPath.GetChar(), "ALS_Points",suffix.GetChar(), false, exportDist);
	}

	pALSLabels->setAll(0);

	C3DPoint obj;
	C2DPoint obs;

	vector<unsigned long> coincidence(labImg->getNLabels() + 1, 0);

	int maxRow = this->pCombinedLabels->getHeight() - 1;
	int maxCol = this->pCombinedLabels->getWidth()  - 1;

	for (int row = 1; row < maxRow; ++row)
	{			
		unsigned long u0 = this->pCombinedLabels->getDiffY() * row;

		for (int col = 1; col < maxCol; ++col)		
		{			
			unsigned long u = u0 + col;
			obs.x = col + this->pCombinedLabels->getOffsetX();		
			obs.y = row + this->pCombinedLabels->getOffsetY();					
			this->imageTrafo.transformObs2Obj(obj, obs, 0.0);		

//			if (pALSLabels->getDistance(u) <= maxDist)
//			{
				obj.z = plane->getHeight(obj.x, obj.y);
				this->sensorModelVector[imageIndex]->transformObj2Obs(obs, obj);
				obs.x = floor(obs.x + 0.5) - labImg->getOffsetX();
				obs.y = floor(obs.y + 0.5) - labImg->getOffsetY();

				if ((obs.x >= 1) && (obs.x < labImg->getWidth()) && 			
					(obs.y >= 1) && (obs.y < labImg->getHeight()))					
				{
					long imgIndex = labImg->getIndex(long(obs.y), long(obs.x));
					unsigned short label = labImg->getLabel(imgIndex);
					pALSLabels->pixUShort(u) = label;
					if (labObj->getLabel(u) ==  labelObj) ++coincidence[label];
				}
//			}
		}
	};

	if (outputMode > eMEDIUM) 
		pALSLabels->dump(this->outputPath.GetChar(), "BackProjected", suffix.GetChar());
	unsigned int lmax = 1, nmax = coincidence[1];

	for (unsigned int i = 2; i < coincidence.size(); ++i)
	{
		if (coincidence[i] > nmax) 
		{
			lmax = i;
			nmax = coincidence[i];
		}
	}
	
	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{
		if ((pALSLabels->getLabel(u) != lmax)  || (pALSLabels->getDistance(u) > maxDist))
		{
			pALSLabels->pixUShort(u) = 0;
		}
	}

	return pALSLabels;
};

//=============================================================================

bool CBuildingReconstructor::initialiseObjectPlane(int imageIndex, CBuildingFace *plane, vector<CALSPoint *> &pointVec)
{
	bool ret = true;

	unsigned short newLabel = (unsigned short) this->pCombinedLabels->getNLabels(); 

	CLabelImage *pALSLabels = this->getProjectedPlaneWithPointCoverage(imageIndex, plane, pointVec);


	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{
		if ((pALSLabels->getLabel(u) > 0) && (!this->pCombinedLabels->getLabel(u))) this->pCombinedLabels->pixUShort(u) = newLabel;
	}

	delete pALSLabels;

	if (outputMode > eMEDIUM)
	{
		CCharString str(newLabel,3,true);
		str += "_o";
		this->exportObjectLabelBufferToTiff(str.GetChar());
	}

	return ret;
};

//=============================================================================
	  
bool CBuildingReconstructor::initialiseObjectPlaneALS(CBuildingFace *plane, vector<CALSPoint *> &pointVec)
{
	bool ret = true;

	unsigned short newLabel = (unsigned short) this->pCombinedLabels->getNLabels(); 

	vector<CALSPoint *> pV(this->ALSpoints->getNpoints(), 0);
	unsigned int nP = 0;
	double thresh = 3.0 * plane->getSigma0(); 
//	if (plane->getNPoints() > 3) thresh = plane->getThreshold();
	for (int n = 0; n < this->ALSpoints->getNpoints(); ++ n)
	{
		CALSPoint *pALS = this->ALSpoints->getPoint(n);
		C3DPoint P = pALS->getLastEcho();
		double dz = P.z - this->getDTMHeight(P);

		if ((plane->getDistance(P) < thresh) && (dz > this->minHeight) && (!this->ALSPointLabelIndex[pALS->id]))
		{
			pV[nP] = pALS;
			++nP;
		}
	}

	pV.resize(nP);

	CLabelImage *pALSLabels = this->getLabelWithPointCoverage(pV);

	pALSLabels->dump(this->outputPath.GetChar(), "ALS_PTS");

	unsigned short maxDist = (unsigned short) floor((this->resolutionALS / this->imageTrafo.getScaleX()) * sqrt(5.0) * 0.5 * 5.0 + 0.5);

	CImageBuffer mask(0,0,this->pCombinedLabels->getWidth(), this->pCombinedLabels->getHeight(), 1,1,true, 0.0);

	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{
		if ((pALSLabels->getDistance(u) < maxDist) && (!this->pCombinedLabels->getLabel(u))) mask.pixUChar(u) = 1;
	}


	CImageFilter filter (eMorphological, 3, 3);
	CImageBuffer in(mask);
	in.setMargin(1, 0.0f);

	filter.binaryMorphologicalOpen(in, mask, 1);

	pALSLabels->initFromMask(mask, 1.0);

	pALSLabels->dump(this->outputPath.GetChar(), "ALS_PTS_AREA");

	if (pALSLabels->getNLabels() > 2)
	{
		vector<unsigned int>nPoints(pALSLabels->getNLabels(), 0);

		for (unsigned int u = 0; u < pointVec.size(); ++u)
		{
			nPoints[this->getLabel(*pALSLabels, pointVec[u])]++;
		}

		int bestLabel = 0;
		int nL = 0;

		for (unsigned int u = 1; u < nPoints.size(); ++u)
		{
			if (nPoints[u] > nL)
			{
				bestLabel = u;
				nL = nPoints[u];
			}
		}

		if (!bestLabel) ret = false;
		else
		{
			for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
			{
				if (pALSLabels->getLabel(u) == bestLabel) this->pCombinedLabels->pixUShort(u) = newLabel;
			}
		}
	}
	else
	{
		for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
		{
			if (pALSLabels->getLabel(u) > 0) this->pCombinedLabels->pixUShort(u) = newLabel;
		}
	}

	delete pALSLabels;

	return ret;
};

//=============================================================================
	  
bool CBuildingReconstructor::matchWithObjectPlane(unsigned short objectLabel, 
												  int imageIndex, CBuildingFace *plane, 
												  vector<CALSPoint *> &pointVec,
												  vector<vector<CBuildingFace *>> &planeVector, 
												  vector<vector<vector<CALSPoint *>>> &planePointVector,
												  int minPts)
{
	bool ret = true;
	
	CImageBuffer nOvlBuf(this->pCombinedLabels->getOffsetX(), this->pCombinedLabels->getOffsetY(),
		                 this->pCombinedLabels->getWidth(), this->pCombinedLabels->getHeight(), 1, 8, true, 0.0f);

	CImageBuffer DoNotUseMask(this->pCombinedLabels->getOffsetX(), this->pCombinedLabels->getOffsetY(),
		                      this->pCombinedLabels->getWidth(), this->pCombinedLabels->getHeight(), 1, 1, true, 0.0f);

	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{
		if (this->pCombinedLabels->getLabel(u) == objectLabel)
		{
			nOvlBuf.pixUChar(u) += 1;
		}
	}

	for (unsigned int iImg = 0; iImg < this->objectLabelVector.size(); ++iImg)
	{
		if (iImg != imageIndex)
		{
			vector<unsigned long> overlapVector(this->objectLabelVector[iImg]->getNLabels() + 1, 0);

			for (unsigned long u = 0; u < this->objectLabelVector[iImg]->getNGV(); ++u)
			{
				if (this->pCombinedLabels->getLabel(u) == objectLabel)
				{
					unsigned short l = this->objectLabelVector[iImg]->pixUShort(u);
					++overlapVector[l];
				}
			}

			vector<unsigned short> labelVector;
			vector<unsigned long> overlapLabelVector;

			for (unsigned int j = 1; j < overlapVector.size(); ++j)
			{
				if (overlapVector[j])
				{
					if ((planeVector[iImg][j]) && (planePointVector[iImg][j].size() > 3))
					{
						double q = statistics::chiSquareFractil(1.0f - alpha, (long) planeVector[iImg][j]->getRedundancy());
						double threshSigma = this->sigmaALS * sqrt(q / planeVector[iImg][j]->getRedundancy());
						if (planeVector[iImg][j]->getSigma0() < threshSigma)
						{
//							float combinedS0;
							float fRatio = plane->isIdentical(*planeVector[iImg][j]); //>isCoplanar(*planeVector[iImg][j], combinedS0, threshSigma);
							if (fRatio >= 0.0f)
							{
								int nPtsOvlp = 0;
								C2DPoint obs;

								for (unsigned int k = 0; k < planePointVector[iImg][j].size(); ++k)
								{
									unsigned short label = this->getObjectLabel(planePointVector[iImg][j][k]);
									if (label == objectLabel) ++nPtsOvlp;
								}

								if (nPtsOvlp > 1)
								{
									float thr = 0.05f;
									float r1 = float(nPtsOvlp) / float(plane->getNPoints());
									float r2 = float(nPtsOvlp) / float(planeVector[iImg][j]->getNPoints());
									if ((r1 > thr) || (r2 > thr))
									{
										labelVector.push_back(j);
										overlapLabelVector.push_back(overlapVector[j]);
									};
								}
							}
							else
							{
								for (unsigned long u = 0; u < this->objectLabelVector[iImg]->getNGV(); ++u)
								{
									if ((this->pCombinedLabels->getLabel(u) != objectLabel) && (this->objectLabelVector[iImg]->getLabel(u) == j))
									{
										DoNotUseMask.pixUChar(u) = 1;
									}
								}
							}
						}
					};
				};
			};

			for (unsigned int j = 0; j < labelVector.size(); ++j)
			{
				ostrstream oss1; oss1 << "\n Merging plane " << labelVector[j] << " from image " << iImg;
				protHandler.print(oss1, protHandler.prt);
				CLabelImage *pLabels = this->getProjectedPlaneWithPointCoverage(iImg, planeVector[iImg][labelVector[j]], planePointVector[iImg][labelVector[j]]);
				for (unsigned long u = 0; u < this->objectLabelVector[iImg]->getNGV(); ++u)
				{
					unsigned short l = pLabels->pixUShort(u);
					if (l > 0)
					{
						nOvlBuf.pixUChar(u) += 1;
					}
				}

				delete pLabels;
			}

		};
	};

	CLookupTable lookup(8,3); 
	int r = 255, g = 255, b = 255;
	lookup.setColor(r,g,b,0);
	r = 255, g = 0, b = 0;
	lookup.setColor(r,g,b,1);
	r = 255, g = 255, b = 0;
	lookup.setColor(r,g,b,2);
	r = 0, g = 255, b = 0;
	lookup.setColor(r,g,b,3);

	CCharString fn = outputPath.GetChar() + CCharString("nOvlp_") + CCharString(minPts, 1, false) + "_" + CCharString(objectLabel, 3, true) + ".tif";
	nOvlBuf.exportToTiffFile(fn.GetChar(), &lookup, false);
	fn = outputPath.GetChar() + CCharString("doNotUse_") + CCharString(minPts, 1, false) + "_" + CCharString(objectLabel, 3, true) + ".tif";

	unsigned char nThr  = 1; // (this->imageLabelVector.size() > 3) ? 2 : 1;
	unsigned char nThr2 = (this->imageLabelVector.size() > 3) ? 2 : 1;

	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{			
		unsigned char n = nOvlBuf.pixUChar(u);			
		if ((n >= nThr) && ((!this->pCombinedLabels->getLabel(u)) || 
			(this->pCombinedLabels->getLabel(u) == objectLabel)) && 
			((DoNotUseMask.pixUChar(u) == 0) || n >= nThr2)) nOvlBuf.pixUChar(u) = 1;
		else nOvlBuf.pixUChar(u) = 0;
	}

	DoNotUseMask.exportToTiffFile(fn.GetChar(), 0, false);

	CImageFilter filter (eMorphological, 3, 3);
	CImageBuffer in(nOvlBuf);
	in.setMargin(1, 0.0f);

	filter.binaryMorphologicalOpen(in, nOvlBuf, 1);
	
	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{
		if (nOvlBuf.pixUChar(u)) in.pixUChar(u) = 0;
		else in.pixUChar(u) = 1;
	}

	in.setMargin(1, 0.0f);

	filter.binaryMorphologicalOpen(in, nOvlBuf, 1);

	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
	{
		if (nOvlBuf.pixUChar(u)) nOvlBuf.pixUChar(u) = 0;
		else nOvlBuf.pixUChar(u) = 1;
	}

	nOvlBuf.setMargin(3, 0.0f);

	CLabelImage lI(nOvlBuf, 1.0f, 8);

	if (lI.getNLabels() < 2)
	{
		ret = false;
		for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
		{			
			if (this->pCombinedLabels->getLabel(u) == objectLabel)  this->pCombinedLabels->pixUShort(u) = 0;
		}
	}
	else
	{
		vector<unsigned int> nPixOvlVec(lI.getNLabels(), 0);
		for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
		{		
			unsigned short l1 = lI.getLabel(u);
			unsigned short l0 = this->pCombinedLabels->getLabel(u);
			if ((l0 == objectLabel) && (l1)) nPixOvlVec[l1]++;
		}

		unsigned short lMax = 1;
		unsigned long nMax = nPixOvlVec[1];
		for (unsigned short k = 2; k < nPixOvlVec.size(); ++ k)
		{
			if (nPixOvlVec[k] > nMax)
			{
				lMax = k;
				nMax = nPixOvlVec[k];
			}
		};
		
		for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
		{			
			if (lI.getLabel(u) == lMax)  this->pCombinedLabels->pixUShort(u) = objectLabel;
			else if (this->pCombinedLabels->pixUShort(u) == objectLabel) this->pCombinedLabels->pixUShort(u) = 0;
		}
	}
	return ret;
};

//=============================================================================

bool CBuildingReconstructor::addFace(CBuildingFace *face, int nPtsMin)
{
	bool ret = true;

	if (this->planeGraph->getFaceVector()->size() < 1) 
	{
		CBuildingFace *f = new CBuildingFace(0);
		this->planeGraph->getFaceVector()->push_back(f);
	}

	this->planeGraph->getFaceVector()->push_back(face);

	face->setTrafo(this->imageTrafo);

	if (this->pointsInPlanes.size() == 0) 
		this->pointsInPlanes.resize(this->pointsInPlanes.size() + 2);
	else this->pointsInPlanes.resize(this->pointsInPlanes.size() + 1);

	unsigned short faceLabel = face->getIndex();

	if (outputMode > eMEDIUM)
	{
		CCharString str(faceLabel,3,true);
		str += "_before";
		this->exportObjectLabelBufferToTiff(str.GetChar());
	}

	float distThresh = face->getThreshold();
	if (this->pointsInPlanes[faceLabel].size() <= 3) distThresh = (float) (2.0 * this->sigmaALS);

	C2DPoint obs;

	for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
	{
		C3DPoint pt = this->ALSpoints->getPoint(i)->getLastEcho();
	
		if (face->getDistance(pt) < distThresh) 
		{
			unsigned short l = MultiImageSegmenter::getObjectLabel(pt);
			if (l == faceLabel) this->pointsInPlanes[faceLabel].push_back(this->ALSpoints->getPoint(i));
		}
	}
 		
	face->resetSums(this->reduction);

	for (unsigned int j = 0; j < this->pointsInPlanes[faceLabel].size(); ++j)
	{
		face->addPoint(this->pointsInPlanes[faceLabel][j]->getLastEcho());		
	}

	double s0 = face->adjust();
	
	if (face->getNPoints() == 3)
	{
		CXYZPoint P1(this->pointsInPlanes[faceLabel][0]->getLastEcho());
		CXYZPoint P2(this->pointsInPlanes[faceLabel][1]->getLastEcho());
		CXYZPoint P3(this->pointsInPlanes[faceLabel][2]->getLastEcho());
		P1.setSX(this->sigmaALS);   P1.setSY(this->sigmaALS);   P1.setSZ(this->sigmaALS);
		P2.setSX(this->sigmaALS);   P2.setSY(this->sigmaALS);   P2.setSZ(this->sigmaALS);
		P3.setSX(this->sigmaALS);   P3.setSY(this->sigmaALS);   P3.setSZ(this->sigmaALS);

		face->initFromPoints(P1,P2,P3, &this->reduction);
	}

	ret = this->computeRobust(*face, this->pointsInPlanes[faceLabel], 5, nPtsMin);

	if (ret) 
	{
		ret = face->getObliqueness() < tan(this->maxAngle);
		if (ret && (face->getRedundancy() > 0))
		{
			double threshSigma = this->getSigmaThreshold((int) face->getRedundancy());
			ret = (face->getSigma0() < threshSigma);
		}
	}
	if (ret)
	{
		for (unsigned int k = 0; k < this->pointsInPlanes[faceLabel].size(); ++k)
		{
			int idx = this->pointsInPlanes[faceLabel][k]->id;
			this->ALSPointLabelIndex[idx] = faceLabel;
		}

		CLabelImage *pALSlabels = this->getLabelWithPointCoverage(this->pointsInPlanes[faceLabel]);
			
		unsigned short maxDist = (unsigned short) floor((this->resolutionALS / this->imageTrafo.getScaleX()) * sqrt(5.0) * 0.5 * 5.0 + 0.5);

		for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
		{	
			if ((this->pCombinedLabels->getLabel(u) == faceLabel) && (pALSlabels->getDistance(u) > maxDist))
				this->pCombinedLabels->pixUShort(u) = 0;
		};

		delete pALSlabels;
		
		int morphFilter = 2;

		this->morphologicalFilterObjectLabel(faceLabel, morphFilter);

		for (unsigned int i = 0; i < this->objectLabelVector.size(); ++i)
		{
			bool mustFilter = false;
			for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
			{			
				if (this->pCombinedLabels->getLabel(u) == faceLabel) 
				{
					if (this->objectLabelVector[i]->getLabel(u)) mustFilter = true;
					this->objectLabelVector[i]->pixUShort(u) = 0;
				}
			};

			if (mustFilter) 
			{
				this->objectLabelVector[i]->morphologicalOpen(morphFilter,1);
				CCharString str(faceLabel,3,true);
				str += "_" + CCharString(nPtsMin, 1, false);
				str += "_" + CCharString(i, 1, false);
				this->objectLabelVector[i]->dump(this->outputPath.GetChar(), "proj", str.GetChar()); 
			}
		}

		this->pCombinedLabels->initPixelNumbers();
		ret = (this->pCombinedLabels->getNPixels(faceLabel) > 1);

		if (ret)
		{
			ostrstream oss;
			oss.setf(ios::fixed, ios::floatfield);
			oss.precision(3);
			oss <<"\n New Plane: "; oss.width(3); oss << face->getIndex() << " ( ";
			oss.width(4); oss<< face->getNPoints() << " points)\n" << ends;	
			char *z = oss.str();
			CCharString s = z + face->getDumpString();
			delete [] z;
			protHandler.print(s, protHandler.prt);
		}

//if (faceLabel == 14) kMeansPlanes(this->pointsInPlanes[faceLabel]);

		CCharString str(nPtsMin,1,false);
		str += "_" + CCharString(faceLabel,3,true);
		this->exportObjectLabelBufferToTiff(str.GetChar());
	}

	if (!ret)
	{
		for (unsigned int i = 0; i < this->objectLabelVector.size(); ++i)
		{
			for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u)
			{			
				if (this->pCombinedLabels->getLabel(u) == faceLabel) 
				{
					this->objectLabelVector[i]->pixUShort(u) = 0;
					this->pCombinedLabels->pixUShort(u) = 0;
				}
			};

			this->objectLabelVector[i]->morphologicalOpen(2,1);
		}

		delete face;
		this->planeGraph->getFaceVector()->resize(this->planeGraph->getFaceVector()->size() - 1);
		this->pointsInPlanes.resize(this->pointsInPlanes.size() - 1);
	}

	return ret;
};

//=============================================================================

bool CBuildingReconstructor::kMeansPlanes(vector<CALSPoint *> &pointVec)
{
	if (pointVec.size() <= (unsigned int) this->neighbours * 2) return false;

	bool ret = 0;

	CGenericPointCloud pts(pointVec.size(), 3, 3);
	for (unsigned int i = 0; i < pointVec.size(); ++i)
	{
		C3DPoint p = pointVec[i]->getLastEcho();
		pts.addPoint(p);
	}

	pts.init();

	vector<C3DPoint> parVec(pts.getNpoints());

	double *sqrDist = new double[this->neighbours];
	ANNpointArray neigh = new ANNpoint[this->neighbours]; 
	ANNpoint pA;
	C3DPoint p, pN;
	int nNeigh;
	C2DPoint rowColPoint;
	C3DAdjustPlane plane;

	C3DPoint minPt( FLT_MAX, FLT_MAX, FLT_MAX);
	C3DPoint maxPt(-FLT_MAX,-FLT_MAX,-FLT_MAX);

	CCharString fn = this->outputPath + "pars.xyz";
	ofstream file3(fn.GetChar());
		
	CSortVector<double> v0(pts.getNpoints(), 0);
	CSortVector<double> v1(pts.getNpoints(), 0);
	CSortVector<double> v2(pts.getNpoints(), 0);
	for (int i = 0; i < pts.getNpoints(); ++i)
	{
		plane.resetSums(this->reduction);
		pA = pts.getPt(i);
		p.x = pA[0]; p.y = pA[1]; p.z = pA[2]; 
		nNeigh = pts.getNearestNeighbours(neigh, sqrDist, this->neighbours, p);
		for (int j = 0; j < nNeigh; ++j)
		{
			pN.x = neigh[j][0]; pN.y = neigh[j][1]; pN.z = neigh[j][2];
			plane.addPoint(pN);
		}
		plane.adjust();
		C3DPoint normal = plane.getNormal();
		C3DPoint COG = plane.getPoint();
		double d = plane.getPlaneConstant();
		normal.z = 1.0 / normal.z;
		parVec[i].x = normal.x * normal.z;
		parVec[i].y = normal.y * normal.z;
		parVec[i].z = -(d * normal.z + parVec[i].x * COG.x + parVec[i].y * COG.y + COG.z);
		if (parVec[i].x < minPt.x) minPt.x = parVec[i].x;
		if (parVec[i].y < minPt.y) minPt.y = parVec[i].y;
		if (parVec[i].z < minPt.z) minPt.z = parVec[i].z;
		if (parVec[i].x > maxPt.x) maxPt.x = parVec[i].x;
		if (parVec[i].y > maxPt.y) maxPt.y = parVec[i].y;
		if (parVec[i].z > maxPt.z) maxPt.z = parVec[i].z;
		v0[i] = parVec[i].x;
		v1[i] = parVec[i].y;
		v2[i] = parVec[i].z;

		normal = parVec[i] * 1000.0;
		file3 << normal.getString(" ", 7, 5) << "\n";
	}

	delete [] sqrDist;
	delete [] neigh; 

	v0.sortAscending();
	v1.sortAscending();
	v2.sortAscending();
	int idx = (int) floor(float(pts.getNpoints()) * 0.05f);
	minPt.x = v0[idx];
	minPt.y = v1[idx];
	minPt.z = v2[idx];
	maxPt.x = v0[pts.getNpoints() - idx];
	maxPt.y = v1[pts.getNpoints() - idx];
	maxPt.z = v2[pts.getNpoints() - idx];

	idx = (int) floor(float(pts.getNpoints()) * 0.5f);

	for (int i = 0; i < v0.size(); ++i)
	{
		v0[i] = fabs(v0[i] - v0[idx]);
		v1[i] = fabs(v1[i] - v1[idx]);
		v2[i] = fabs(v2[i] - v2[idx]);
	}
		
	v0.sortAscending();
	v1.sortAscending();
	v2.sortAscending();
	idx = (int) floor(float(pts.getNpoints()) * 0.9f);

	double d0 = v0[idx];
	double d1 = v1[idx];
	double d2 = v2[idx];

	vector<unsigned char> indicator(pts.getNpoints(), 255);

	int nChanged;
	int maxChanged = pts.getNpoints() / 20; 
	if (maxChanged < 1) maxChanged = 1;
	int nIter = 0;

	C3DPoint P1, P2, P1new, P2new, diff1, diff2;
	P1new = maxPt;
	P2new = minPt;

	do
	{
		CCharString fn = this->outputPath + "cl1.xyz";
		ofstream file1(fn.GetChar());
		fn = this->outputPath + "cl2.xyz";
		ofstream file2(fn.GetChar());

		unsigned int n1 = 0; 
		unsigned int n2 = 0; 
		P1 = P1new;
		P2 = P2new;
		P1new.x = P1new.y = P1new.z = 0.0;
		P2new.x = P2new.y = P2new.z = 0.0;
		nChanged = 0;

		for (int i = 0; i < pts.getNpoints(); ++i)
		{
			diff1 = P1 - parVec[i];
			double dist1 = diff1.getSquareNorm();
			diff2 = P2 - parVec[i];
			double dist2 = diff2.getSquareNorm();
			unsigned char cl;

			if (dist1 < dist2)
			{
				if ((fabs(diff1.x) < d0) && (fabs(diff1.y) < d1) && (fabs(diff1.z) < d2))
				{
					pA = pts.getPt(i);
					p.x = pA[0]; p.y = pA[1]; p.z = pA[2]; 		
					file1 << p.getString(" ", 7, 2)<< endl; 
					P1new += parVec[i];;
					cl = 0;
					++n1;
				}
				else
				{
					cl = 255;
				}
			}
			else
			{
				if ((fabs(diff2.x) < d0) && (fabs(diff2.y) < d1) && (fabs(diff2.z) < d2))
				{

					pA = pts.getPt(i);
					p.x = pA[0]; p.y = pA[1]; p.z = pA[2]; 		
					file2 << p.getString(" ", 7, 2)<< endl; 
					P2new += parVec[i];
					cl = 1;
					++n2;
				}
				else
				{
					cl = 255;
				}
			}
			if (indicator[i] != cl) ++nChanged;

			indicator[i] = cl;
		}

		P1new /= double(n1);
		P2new /= double(n2);

		++nIter; 
	} while ((nIter < 100) && (nChanged > maxChanged));

	return ret;
};

//=============================================================================

CBuildingFace *CBuildingReconstructor::computePlanesRobust(const CCharString &heading, 
														   CLabelImage &labels,
														   vector<CBuildingFace *> &planeVector, 
														   vector<vector<CALSPoint *>> &planePointVector,
														   int nPtsMin,
														   float sigmaMin)
{
	CBuildingFace *pFace = 0;

	float fMax = -1;
	unsigned int jMax = 0;
	double tanAngle = tan(this->maxAngle);

	this->computePlanes(heading, labels, planeVector, planePointVector, false, false);

	this->checkHeights(labels);

	for (unsigned int l = 0; l < planeVector.size(); ++l)
	{
		if (this->computeRobust(*planeVector[l], planePointVector[l], 30, nPtsMin))
		{
			if (planePointVector[l].size() > 3)
			{
				 CBuildingFace currFace(*planeVector[l]);

				 planePointVector[l].resize(0);
				 planeVector[l]->resetSums(this->reduction);

				 for (int k = 0; k < this->ALSpoints->getNpoints(); ++k)
				 {
					 C3DPoint p = this->ALSpoints->getPoint(k)->getLastEcho();
					 unsigned short label = MultiImageSegmenter::getLabel(labels, p);

					 if (label == l)
					 {
						 if (currFace.getDistance(p) < currFace.getThreshold())
						 {
							 planeVector[l]->addPoint(p);
							 planePointVector[l].push_back(this->ALSpoints->getPoint(k));
						 }
					 }
				 }

				 planeVector[l]->adjust();
			}

			if (this->computeRobust(*planeVector[l], planePointVector[l], 10, nPtsMin))
			{
				 if (planeVector[l]->getObliqueness() < tanAngle)
				 {
					 double sigma = planeVector[l]->getSigma0();
					 double q = statistics::chiSquareFractil(1.0f - alpha, (long) planeVector[l]->getRedundancy());
					 double threshSigma = this->sigmaALS * sqrt(q / planeVector[l]->getRedundancy());

					 ostrstream of;
					 of.setf(ios::fixed, ios::floatfield);
					 of.precision(3);
					 of << heading << " Plane ";
					 of.width(3); of << l << " from adjustment (";
					 of.width(4); of << planeVector[l]->getNPoints()  << " points): sigma0 = ";
					 of.width(6); of << sigma << ", threshold: ";
					 of.width(6); of << threshSigma;
					 protHandler.print(of, protHandler.prt);

					 if (sigma < sigmaMin) sigma = sigmaMin; 
					 float f = float(planeVector[l]->getNPoints()) / float(sigma);

					 if (f > fMax)
					 {
						 fMax = f;
						 jMax = l;
					 }
				 }
			}
		}
 
		if (fMax > 0) pFace = planeVector[jMax];
	}

	return pFace;
};

//=============================================================================

CBuildingFace *CBuildingReconstructor::computePlanesRobust(vector<vector<CBuildingFace *>> &planeVector, 													
														   vector<vector<vector<CALSPoint *>>> &planePointVector,
														   int &imageIndexOfReturnedFace, int nPtsMin, float sigmaMin)
{
	CBuildingFace *pFace = 0;

	vector<CBuildingFace *> maxPlanes(this->imageVector.size(), 0);

	for (unsigned int i = 0; i < this->objectLabelVector.size(); ++i)
	{
		ostrstream oss;
		oss << "\n Initialising planes from image " << i << ": " << ends;
		protHandler.print(oss, protHandler.prt);

		maxPlanes[i] = this->computePlanesRobust("         ", *this->objectLabelVector[i], planeVector[i], planePointVector[i], nPtsMin, sigmaMin);
	}

	float fMax = -1; 
	imageIndexOfReturnedFace = -1;
	
	for (unsigned int j = 0; (j < maxPlanes.size()) && (imageIndexOfReturnedFace < 0); ++j)
	{
		if (maxPlanes[j]) 
		{
			float sig = (float) maxPlanes[j]->getSigma0();
			if (sig < sigmaMin) sig = sigmaMin;
			fMax = float(maxPlanes[j]->getNPoints()) / sig;
			imageIndexOfReturnedFace = j;
		}
	}
			
	if (imageIndexOfReturnedFace >= 0) 
	{
		ostrstream of;
		of.setf(ios::fixed, ios::floatfield);
		of.precision(3);

		for (unsigned int j = 0; j < maxPlanes.size(); ++j)
		{
			if (maxPlanes[j])
			{
				of <<"\n   Maximum plane from image " << j << " : "; 
				of.width(3); of << maxPlanes[j]->getIndex() << ", ";
				of.width(4); of << maxPlanes[j]->getNPoints() << " points, sigma0 = " << maxPlanes[j]->getSigma0();					

				float sig = (float) maxPlanes[j]->getSigma0();
				if (sig < 0.01f) sig = 0.01f;
				float f =  float(maxPlanes[j]->getNPoints()) / float(maxPlanes[j]->getSigma0());

				if (f > fMax)
				{
					fMax = f;
					imageIndexOfReturnedFace = j;
				}
			}	
		}

		pFace = maxPlanes[imageIndexOfReturnedFace];
		of <<"\n\n   Overall maximum plane is plane " << pFace->getIndex() << " from image " << imageIndexOfReturnedFace;					
		protHandler.print(of, protHandler.prt);				
	}
		
	return pFace;
};
	  
//=============================================================================

bool CBuildingReconstructor::initialiseFaces(int nPtsMin)
{
	bool ret = true;
	CCharString titleStr; 
	if (this->listener)
	{
		titleStr = this->listener->getTitleString();
	}

	float sigMin = float(this->sigmaALS * 0.5);

	bool planeFound = true;
	while (planeFound)
	{		
		if (this->listener)
		{
			this->listener->setTitleString("Initialising planes");
		}

		vector<vector<CBuildingFace *>> planeVector(this->imageVector.size());	 
		vector<vector<vector<CALSPoint *>>> planePointVector(this->imageVector.size());

		int imageIndex;

		CBuildingFace *pFace = computePlanesRobust(planeVector, planePointVector, imageIndex, nPtsMin, sigMin);
		if (!pFace) planeFound = false;
		else
		{
			unsigned short objectLabel = (unsigned short) this->pCombinedLabels->getNLabels(); 
			unsigned short faceLabel = pFace->getIndex();

			if (this->listener)
			{
				CCharString newT = "Adding plane " + CCharString(objectLabel, 1, false);
				this->listener->setTitleString(newT);
			}

			bool found = this->initialiseObjectPlane(imageIndex, pFace, planePointVector[imageIndex][faceLabel]);
			if (found)
			{
				found = this->matchWithObjectPlane(objectLabel, imageIndex, pFace, 
												   planePointVector[imageIndex][faceLabel],
												   planeVector, planePointVector, nPtsMin);
				if (found)
				{
					CBuildingFace *newFace = new CBuildingFace(*pFace);
					newFace->setIndex(objectLabel);

					found = this->addFace(newFace, nPtsMin);
				}
			}

			if (!found)
			{
				for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
				{
					if (this->pCombinedLabels->getLabel(i) == objectLabel) 
						this->pCombinedLabels->pixUShort(i) = 0;
					if (this->objectLabelVector[imageIndex]->getLabel(i) == faceLabel)
						this->objectLabelVector[imageIndex]->pixUShort(i) = 0;

				}
			}
		}

		for (unsigned int i = 0; i < planeVector.size(); ++i)
		{
			for (unsigned int j = 0; j < planeVector[i].size(); ++j)
			{
				if (planeVector[i][j]) delete planeVector[i][j];
				planeVector[i][j] = 0;
			}

			planeVector[i].clear();
		}
	}

	if (this->listener)
	{
		this->listener->setTitleString(titleStr);
	}

	if (this->planeGraph->getFaceVector()->size() < 2) ret = false;

	return ret;
};
	  
//=============================================================================

bool CBuildingReconstructor::initialiseFacesALS(int nPtsMin)
{
	bool ret = true;
	CCharString titleStr; 
	if (this->listener)
	{
		titleStr = this->listener->getTitleString();
	}

	float sigMin = float(this->sigmaALS * 0.5);

	protHandler.print("\n\nInitialising planes from ALS points\n===================================", protHandler.prt);

	vector<CALSPoint *> points;
	CBuildingFace *pFace = this->getBestFaceFromALS(points);
	
	while (pFace)
	{
		pFace->initThreshold();
		protHandler.print("\nBest Plane:", protHandler.prt);

		protHandler.print(pFace->getDumpString(), protHandler.prt);

		if (this->listener)
		{
			this->listener->setTitleString("Initialising planes");
		}
		
		unsigned short objectLabel = (unsigned short) this->pCombinedLabels->getNLabels(); 
		unsigned short faceLabel = pFace->getIndex();

		if (this->listener)
		{
			CCharString newT = "Adding plane " + CCharString(objectLabel, 1, false);
			this->listener->setTitleString(newT);
		}

		

		bool found = this->initialiseObjectPlaneALS(pFace, points);
		if (found)
		{
			CBuildingFace *newFace = new CBuildingFace(*pFace);
			newFace->setIndex(objectLabel);
			found = this->addFace(newFace, nPtsMin);
		}

		if (!found)
		{
			for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
			{
				if (this->pCombinedLabels->getLabel(i) == objectLabel) 
					this->pCombinedLabels->pixUShort(i) = 0;
			}
		}

		delete pFace;
		pFace = 0;
	
		if (found) pFace = this->getBestFaceFromALS(points);
	}

	if (this->listener)
	{
		this->listener->setTitleString(titleStr);
	}

	if (this->planeGraph->getFaceVector()->size() < 2) ret = false;

	return ret;
};
	  
//=============================================================================

bool CBuildingReconstructor::resetToOneBuilding()
{
	bool ret = true;

	unsigned short maxDist = (unsigned short) floor((this->resolutionALS / this->imageTrafo.getScaleX()) * sqrt(5.0) * 0.5 * 5.0 + 0.5);
//	maxDist *= 2; // about 2 times the ALS resolution!

	CLabelImage ALSlab(*this->pCombinedLabels);
	ALSlab.setAll(0.0f);
	for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
	{
		C3DPoint p = this->ALSpoints->getPoint(i)->getLastEcho();
		
		p.z -= this->getDTMHeight(p);
		if (p.z < this->minHeight)
		{
			long index = MultiImageSegmenter::getObjectLabelIndex(p);
			if (index >= 0) ALSlab.pixUShort(index) = 1;
		}
	}

	ALSlab.updateVoronoi();
	CImageBuffer mask(0,0, this->pCombinedLabels->getWidth(), this->pCombinedLabels->getHeight(), 1,1,true, 0.0f);

	for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
	{
		if ((ALSlab.getDistance(i) < maxDist) && (this->pCombinedLabels->getLabel(i) == 0)) mask.pixUChar(i) = 0;
		else mask.pixUChar(i) = 1;
	}
/*
	this->pCombinedLabels->updateVoronoi();


	for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
	{
		if (this->pCombinedLabels->getDistance(i) < maxDist) mask.pixUChar(i) = 1;
	}
*/
	mask.setMargin(1, 0.0f);
	CLabelImage bldgs(mask, 1.0, 8);
	int maxLabel = 0;
	unsigned int npix = 0;

	for (unsigned int i = 1; i < bldgs.getNLabels(); ++i)
	{
		if (bldgs.getNPixels(i) > npix)
		{		
			maxLabel = i;
			npix = bldgs.getNPixels(i);
		}
	}


	for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
	{
		unsigned short lab = bldgs.getLabel(i);
		if ((lab) && (lab != maxLabel))
		{
			this->pCombinedLabels->pixUShort(i) = 0;
		}
	}

	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);
	oss << "\n Removing planes not belonging to the building\n =============================================";

	vector<CBuildingFace *> *faceVec = this->planeGraph->getFaceVector();
	unsigned int nPlanes = 1;

	CImageBuffer *validMask = this->getValidObjectMask();
	vector<unsigned int> validPixels(faceVec->size(), 0);

	for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u) 
	{
		unsigned short l = this->pCombinedLabels->getLabel(u);

		if (l)
		{
			if (validMask->pixUChar(u)) ++validPixels[l];
		}			
	};

	delete validMask;

	float percentage = 1.0f / 3.0f;
	for (unsigned int i = 1; i < validPixels.size(); ++i)
	{
		int thr = (int) floor(float(this->pCombinedLabels->getNPixels(i)) * percentage + 0.5f);
		if (validPixels[i] < thr) validPixels[i] = 0;
	}

	for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
	{
		unsigned short lab = this->pCombinedLabels->getLabel(i);
		if ((lab) && (!validPixels[lab]))
		{
			this->pCombinedLabels->pixUShort(i) = 0;
		}
	}
	this->pCombinedLabels->initPixelNumbers();

	for (unsigned int i = 1; i < faceVec->size(); ++i)
	{
		if (i < this->pCombinedLabels->getNLabels())
		{
			if ((this->pCombinedLabels->getNPixels(i) > 0) && (validPixels[i] > 0))
			{
				if (i != nPlanes)
				{
					*(*faceVec)[nPlanes] = *(*faceVec)[i];
					(*faceVec)[nPlanes]->setIndex(nPlanes);
					this->pointsInPlanes[nPlanes] = this->pointsInPlanes[i];
					
					for (unsigned int k = 0; k < this->pointsInPlanes[nPlanes].size(); ++k)
					{
						int idx = this->pointsInPlanes[nPlanes][k]->id;
						this->ALSPointLabelIndex[idx] = nPlanes;
					}

					for (unsigned long u = 0; u < this->pCombinedLabels->getNGV(); ++u) 
					{
						if (this->pCombinedLabels->pixUShort(u) == i) 
							this->pCombinedLabels->pixUShort(u) = nPlanes;
					};
				};
				oss << "\n\nPlane "; oss.width(3); oss << nPlanes << " ("; oss.width(4); 
				oss << (*this->planeGraph->getFaceVector())[i]->getNPoints() << " points)" 
					<< (*this->planeGraph->getFaceVector())[i]->getDumpString();
				++nPlanes;
			}
			else
			{
				for (unsigned int k = 0; k < this->pointsInPlanes[i].size(); ++k)
				{
					int idx = this->pointsInPlanes[i][k]->id;
					this->ALSPointLabelIndex[idx] = 0;
				}
			}
		}
		else
		{
			for (unsigned int k = 0; k < this->pointsInPlanes[i].size(); ++k)
			{
				int idx = this->pointsInPlanes[i][k]->id;
				this->ALSPointLabelIndex[idx] = 0;
			}
		}
	}

	

	protHandler.print(oss,protHandler.prt);

	for (unsigned int i = nPlanes; i < faceVec->size(); ++i)
	{
		delete (*faceVec)[i];
		(*faceVec)[i] = 0;
	};

	faceVec->resize(nPlanes);
	this->pointsInPlanes.resize(nPlanes); 

	this->pCombinedLabels->initPixelNumbers();
	this->pCombinedLabels->updateVoronoi();

	return ret;
};
	  	  
//=============================================================================
  
unsigned short CBuildingReconstructor::getBestFaceLabel(CALSPoint *pALS)
{
	unsigned short faceLabel = 0;
	CALSPoint **neighbours = new CALSPoint*[this->neighbours];
	double *distances = new double[this->neighbours];

	C3DPoint p = pALS->getLastEcho();
	int nNeighbours = this->ALSpoints->getNearestNeighbours(neighbours, distances, this->neighbours, p);

	bool nextPlanePointIsClose = false;
	double thr = 2.0 * this->resolutionALS;
	
	for (int i = 1; (i < nNeighbours) && (!nextPlanePointIsClose); ++i)
	{
		unsigned short l = this->ALSPointLabelIndex[neighbours[i]->id];
		if (l)
		{
			if (distances[i] < thr) nextPlanePointIsClose = true;
		}
	}		

	if (nextPlanePointIsClose)
	{
		vector<unsigned short> labelVec;

		for (int i = 0; i < nNeighbours; ++i)
		{
			unsigned short l = this->ALSPointLabelIndex[neighbours[i]->id];
			if (l)
			{
				bool found = false;
				for (unsigned int j = 0; (j < labelVec.size()) && (!found); ++j)
				{
					found = (l == labelVec[j]);
				}
				if (!found) labelVec.push_back(l);
			}
		}

		double minDist = FLT_MAX;

		for (unsigned int i = 0; i < labelVec.size(); ++i)
		{
			unsigned short l = labelVec[i];
			CBuildingFace *face = (*this->planeGraph->getFaceVector())[l];
			float distThresh = face->getThreshold();
		
			double dist = face->getDistance(p);
			if ((dist < distThresh) && (dist < minDist))
			{
				minDist = dist;
				faceLabel = l;
			}
		}
	}
	delete [] neighbours;
	delete [] distances;

	return faceLabel;
};

//=============================================================================
  	
CBuildingFace *CBuildingReconstructor::getBestFaceFromALS(vector<CALSPoint *> &points)
{
	CBuildingFace *pFace = 0;
	points.resize(this->neighbours);
	float thresh = this->getSigmaThreshold(this->neighbours - 3);
	CALSPoint **ALSPTS = new CALSPoint *[this->neighbours];
	double *sqrDist = new double[this->neighbours];

	for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
	{
		CALSPoint *pALS = this->ALSpoints->getPoint(i);
		if (this->ALSPointLabelIndex[pALS->id] < 1)
		{// pALS has not been assigned to any plane yet
			C3DPoint p = pALS->getLastEcho();
			float z = this->getDTMHeight(p);
			if ((p.z - z) > this->minHeight)
			{ // p is more than minHeight above the terrain
				int nNeigh = this->ALSpoints->getNearestNeighbours(ALSPTS, sqrDist, this->neighbours, p);
				if (nNeigh == this->neighbours)
				{// we have found neighbours neighbours
					bool idFound = false;
					for(int j = 0; (j < this->neighbours) && (!idFound); ++j)
					{
						if (this->ALSPointLabelIndex[ALSPTS[j]->id] > 0) idFound = true;
					}
					if (!idFound)
					{ // now we can be sure that all neighbours have not been assigned a plane yet!
					  // compute adjusting plane
						CBuildingFace f(1);
						f.resetData(1, this->reduction);

						for(int j = 0; (j < this->neighbours); ++j)
						{
							f.addPoint(ALSPTS[j]->getLastEcho());
						}
						float sig0 = (float) f.adjust();
						if ((sig0 > 0.0f) && (sig0 < thresh))
						{ //now we are sure that this is a good plane!
							if (!pFace)
							{// this is the first good plane
								pFace = new CBuildingFace(f);
								for(int j = 0; (j < this->neighbours); ++j)
								{
									points[j] = ALSPTS[j];
								}
							}
							else
							{
								if (sig0 < (float) pFace->getSigma0())
								{ // the new plane is better than the old one
									pFace->copyData(1,f);
									for(int j = 0; (j < this->neighbours); ++j)
									{
										points[j] = ALSPTS[j];
									}
								}
							}
						}
					}
				}
			}
		}
	}

	delete [] ALSPTS;
	delete [] sqrDist;

	return pFace;
};

//=============================================================================

void CBuildingReconstructor::getUnusedPointsWithLabels(CLabelImage &pointLabels, 
													   vector<CBuildingFace *> &faceVec, 
													   vector<vector<CALSPoint *>> &pointVec, 
													   double dist)
{

	this->ALSpoints->setAnnDim(3);
	vector<CALSPoint *> addedPoints; 
	pointLabels.set(*this->pCombinedLabels);

	unsigned short maxDist = (unsigned short) floor((dist / this->imageTrafo.getScaleX()) * sqrt(5.0) * 0.5 * 5.0 + 0.5);

	CLabelImage ALSPointLabels(*this->pCombinedLabels);
	ALSPointLabels.setAll(0.0);
	vector<CBuildingFace *> *faceGraph = this->planeGraph->getFaceVector();

	bool added = true;

	while (added)
	{// see whether points can be added to neighbouring planes
		added = false;
		int nPts1 = 0;
		int nPts2 = 0;
		for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
		{
			CALSPoint *pALS = this->ALSpoints->getPoint(i);
			C3DPoint P = pALS->getLastEcho();
			double dz = P.z - this->getDTMHeight(P);
			if (dz > this->minHeight)
			{// the point is an off-terrain point
				long index = this->getObjectLabelIndex(this->ALSpoints->getPoint(i));
				if ((index >= 0) && (ALSPointLabelIndex[pALS->id] < 1))
				{// the point is inside the image and it has not been assigned a plane yet
					unsigned short l = this->pCombinedLabels->getLabel(index);
					if (l)
					{ // the point is inside the area covered by the plane "face"
						CBuildingFace *face = (*this->planeGraph->getFaceVector())[l];
						float distThresh = face->getThreshold();
						double dist = face->getDistance(P);
						if (dist < distThresh)
						{ // the point is sufficiently close to the plane and thus is added to the plane
							this->pointsInPlanes[l].push_back(pALS);
							ALSPointLabelIndex[pALS->id] = l;
							added = true;
							++nPts1;
						}
					}
					else
					{
						l = this->getBestFaceLabel(pALS);
						if (l)
						{// l is the plane near to the point to which the point is closest
							addedPoints.push_back(pALS);
							ALSPointLabelIndex[pALS->id] = l;
							ALSPointLabels.pixUShort(index) = l;
							added = true;
							++nPts2;
						}
					}
				}
			}
		}
		ostrstream oss;
		if (nPts1 || nPts2)
		{
			oss << "\n Region growing: ";
			if (nPts1 )
			{
				oss << "Points added inside existing planes: " << nPts1;
				if (nPts2) oss << "\n                 ";
			}
			if (nPts2) oss << "New points added: " << nPts2;
			protHandler.print(oss, protHandler.prt);
		}
	}
	ALSPointLabels.updateVoronoi();
	unsigned short maxD = (unsigned short) floor((this->resolutionALS / this->imageTrafo.getScaleX()) * sqrt(5.0) * 0.5 * 5.0 + 0.5);

	for (unsigned int i = 0; i < ALSPointLabels.getNGV(); ++i)
	{
		if ((ALSPointLabels.getDistance(i) < maxD) && (this->pCombinedLabels->getLabel(i) == 0))
		{
			ALSPointLabels.pixUShort(i) = ALSPointLabels.getVoronoi(i);
		}
	}

	ALSPointLabels.dump(this->outputPath.GetChar(), "ALS_NEWPTS");

	ALSPointLabels.setBoundariesToZero(false, false, false, 0);
	
	CImageBuffer buf(0,0,ALSPointLabels.getWidth(), ALSPointLabels.getHeight(), 1,1,true, 0.0);

	for (unsigned int i = 0; i < ALSPointLabels.getNGV(); ++i)
	{
		if (ALSPointLabels.getLabel(i) == 0) buf.pixUChar(i) = 0;
		else buf.pixUChar(i) = 1;
	}

	pointLabels.initFromMask(buf, 1.0);
	pointLabels.dump(this->outputPath.GetChar(), "ALS_NEWPTS_RENUMBER");

	faceVec.resize(pointLabels.getNLabels(), 0);
	pointVec.resize(pointLabels.getNLabels());

	for (unsigned int i = 0; i < faceVec.size(); ++i)
	{
		faceVec[i] = new CBuildingFace(i);
		faceVec[i]->resetData(i, this->reduction);
		faceVec[i]->setTrafo(this->imageTrafo);
	}

	for (int i = 0; i < (int) addedPoints.size(); ++i)
	{
		long index = this->getObjectLabelIndex(addedPoints[i]);
		unsigned short label = pointLabels.getLabel(index);
		if (label)
		{
			faceVec[label]->addPoint(addedPoints[i]->getLastEcho());
			pointVec[label].push_back(addedPoints[i]);

		}
		else
		{
			ALSPointLabelIndex[addedPoints[i]->id] = 0;
		}
	}
}


//=============================================================================

void CBuildingReconstructor::checkNearPoints(double dist, int nPtsMin)
{			
	vector<CBuildingFace *> faceVec;
	vector<vector<CALSPoint *>> pointVec;
	CLabelImage pointLabels(this->pCombinedLabels->getOffsetX(), this->pCombinedLabels->getOffsetY(), 
		                    this->pCombinedLabels->getWidth(),this->pCombinedLabels->getHeight(), 
							this->pCombinedLabels->getNeighbourhood());

	this->getUnusedPointsWithLabels(pointLabels, faceVec, pointVec, dist);

	unsigned int dx = pointLabels.getDiffX();
	unsigned int dy = pointLabels.getDiffY();
	unsigned int uMax = pointLabels.getNGV() - dy - dx;
				

	for (unsigned int i = 0; i < faceVec.size(); ++i)
	{
		if (pointVec[i].size() > 0)
		{
			if (faceVec[i]->getNPoints() < nPtsMin)
			{
				unsigned short labelToBeAdded = this->ALSPointLabelIndex[pointVec[i][0]->id];
				bool toBeAdded = false;

				// check whether faceVec[i] is still a neighbour of labelToBeAdded 
				for (unsigned int u = pointLabels.getDiffY() + pointLabels.getDiffX(); (u < uMax) && (!toBeAdded); ++u)
				{
					if (pointLabels.getLabel(u) == i) 
					{
						if ((this->pCombinedLabels->getLabel(u - dy) == labelToBeAdded) ||
							(this->pCombinedLabels->getLabel(u - dx) == labelToBeAdded) ||
							(this->pCombinedLabels->getLabel(u + dy) == labelToBeAdded) ||
							(this->pCombinedLabels->getLabel(u + dx) == labelToBeAdded))
						{
							toBeAdded = true;
						}
					}
				}

				if (toBeAdded)
				{
					// check whether the points can be added to the plane labelToBeAdded without hurting the s0 threshold
					CBuildingFace face (*(*this->planeGraph->getFaceVector())[labelToBeAdded]);
					face.mergeWithPlane(*faceVec[i]);
					if (face.getSigma0() > this->getSigmaThreshold(face.getRedundancy())) toBeAdded = false;
				}

				if (toBeAdded)
				{
					CBuildingFace *pFace = (*this->planeGraph->getFaceVector())[labelToBeAdded];
					pFace->mergeWithPlane(*faceVec[i]);
					pFace->initThreshold();
					
					for (unsigned int k = 0; k < pointVec[i].size(); ++k)
					{
						this->pointsInPlanes[labelToBeAdded].push_back(pointVec[i][k]);
					}

					for (unsigned int u = 0; u < pointLabels.getNGV(); ++u)
					{
						if (pointLabels.getLabel(u) == i) this->pCombinedLabels->pixUShort(u) = labelToBeAdded; 
					}

					ostrstream oss;
					oss << "\nPlane " << labelToBeAdded <<": " << pointVec[i].size() << " points added" 
						<< pFace->getDumpString().GetChar();
					protHandler.print(oss, protHandler.prt);
				}
				else
				{
					for (unsigned int k = 0; k < pointVec[i].size(); ++k)
					{
						ALSPointLabelIndex[pointVec[i][k]->id] = 0;
					}
				}

				delete faceVec[i];
				faceVec[i] = 0;
				pointVec[i].clear();
			}
			else
			{
				faceVec[i]->computeFacePars();
				
				bool add = false;
				
				if (this->computeRobust(*faceVec[i], pointVec[i], 10, nPtsMin))
				{
					if (faceVec[i]->getObliqueness() < tan(this->maxAngle))
					{
						unsigned short l = this->planeGraph->getFaceVector()->size();

						double sigma = faceVec[i]->getSigma0();
						double q = statistics::chiSquareFractil(1.0f - alpha, (long) faceVec[i]->getRedundancy());
						double threshSigma = this->sigmaALS * sqrt(q / faceVec[i]->getRedundancy());

						ostrstream of;
						of.setf(ios::fixed, ios::floatfield);
						of.precision(3);
						of << "        " << " Plane ";
						of.width(3); of << l << " from adjustment (";
						of.width(4); of << faceVec[i]->getNPoints()  << " points): sigma0 = ";
						of.width(6); of << sigma << ", threshold: ";
						of.width(6); of << threshSigma;
						protHandler.print(of, protHandler.prt);
						add = true;						

						CBuildingFace *face = new CBuildingFace(*faceVec[i]);

						face->setIndex(l);

						for (unsigned int u = 0; u < pointLabels.getNGV(); ++u)
						{
							if (pointLabels.getLabel(u) == i) this->pCombinedLabels->pixUShort(u) = l;
						}
						add = this->addFace(face, nPtsMin);
					}
				};

				if (!add)
				{
					for (unsigned int k = 0; k < pointVec[i].size(); ++k)
					{
						ALSPointLabelIndex[pointVec[i][k]->id] = 0;
					}
				}

				pointVec[i].clear();
				delete faceVec[i];
				faceVec[i] = 0;
			}
		}
		else
		{
			delete faceVec[i];
			faceVec[i] = 0;
		}
	}
	this->pCombinedLabels->initPixelNumbers();
	this->pCombinedLabels->updateVoronoi();
}

//=============================================================================
  
void CBuildingReconstructor::exportPointCloudSeparated(const char *fnbase) const
{
	vector<CBuildingFace *> *faceVec = this->planeGraph->getFaceVector();

	vector<ofstream *> fileVec (faceVec->size(), 0);

	CCharString fn = this->outputPath + "nonClassGrd.xyz";
	ofstream gdf(fn.GetChar());
	fn = this->outputPath + "nonClassOffGrd.xyz";
	ofstream ogf(fn.GetChar());

	vector<CCharString> rgbVec(faceVec->size());
	int r,g,b;

	for (unsigned int i = 1; i < faceVec->size(); ++i)
	{
		if (i > 0)
		{
			CCharString fn = this->outputPath + CCharString(fnbase) + "_" + CCharString(i, 3, true) + ".xyz";
			fileVec[i] = new ofstream(fn.GetChar());
		}

		CLabelImage::getColour(i, r, g, b);
		rgbVec[i]= " " + CCharString(r,3,false) + " " + CCharString(g,3,false) + " " + CCharString(b,3,false);
	}

	for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
	{
		CALSPoint *pALS = this->ALSpoints->getPoint(i);
		C3DPoint p = pALS->getLastEcho();
		CCharString s = p.getString(" ", 10, 3);
		int idx = this->ALSPointLabelIndex[pALS->id];

		if (idx > 0)
		{
			if (idx < fileVec.size()) (*fileVec[idx]) << s.GetChar() << rgbVec[idx] << "\n";				
		}
		else
		{
			double z = this->getDTMHeight(p);
			if ((p.z - z) > this->minHeight)
			{
				ogf << s.GetChar() << " 255 255 255\n";				
			}
			else
			{
				gdf << s.GetChar() << " 128 128 128\n";				
			}
		}
	}

	for (unsigned int i = 1; i < fileVec.size(); ++i)
	{
		delete fileVec[i];
	};
};

//=============================================================================

bool CBuildingReconstructor::mergeCoplanarFaces(int nPtsMin, bool useVoronoi)
{
	bool merged = false;
	vector<CBuildingFace *> *faceVec = this->planeGraph->getFaceVector();
	unsigned short tolerance = (unsigned short) floor(this->resolutionALS / this->imageTrafo.getScaleX() + 0.5) * 10;
				
	CCharString str = "\n\n Merging co-planar faces\n =======================\n\n   Time: " + CSystemUtilities::timeString() + "\n\n List of planes:\n ===============\n";
	protHandler.print(str, protHandler.prt);

			
	do
	{
		merged = false;
		SymmetricMatrix Cooccurrence(faceVec->size());
		Cooccurrence = -10000.0;
		float rmsmin = -FLT_MAX;
		float rmscombined;
		unsigned short label1Min, label2Min;
		double smin = this->sigmaALS * 0.5;

		for (unsigned int i = this->pCombinedLabels->getDiffY() + this->pCombinedLabels->getDiffX(); i < this->pCombinedLabels->getNGV(); ++i)
		{
			unsigned short label2 = this->pCombinedLabels->getLabel(i);
			if (useVoronoi && (!label2))
			{
				if (this->pCombinedLabels->getDistance(i) < tolerance) label2 = this->pCombinedLabels->getVoronoi(i);
			}

			CBuildingFace *pFace2 = (*faceVec)[label2];

			if (label2 && (pFace2->getNPoints() >= (unsigned int) nPtsMin))
			{
				unsigned short label1 = this->pCombinedLabels->getLabel(i - this->pCombinedLabels->getDiffX());
				if (useVoronoi && (!label1))
				{
					if (this->pCombinedLabels->getDistance(i - this->pCombinedLabels->getDiffX()) < tolerance) 
						label1 = this->pCombinedLabels->getVoronoi(i - this->pCombinedLabels->getDiffX());
				}

				CBuildingFace *pFace1 = (*faceVec)[label1];

				if (label1 && (label2 != label1) && (pFace1->getNPoints() >= nPtsMin) && (Cooccurrence.element(label1, label2) < -9999.0))
				{
					Cooccurrence.element(label1, label2) = pFace2->isCoplanar(*pFace1, rmscombined, smin);

					if ((Cooccurrence.element(label1, label2) > 0.0) && (rmscombined < fabs(rmsmin)))
					{
						rmsmin = rmscombined;
						label2Min = label2;
						label1Min = label1;
					}
				}

				label1 = this->pCombinedLabels->getLabel(i - this->pCombinedLabels->getDiffY());
				if (useVoronoi && (!label1))
				{
					if (this->pCombinedLabels->getDistance(i - this->pCombinedLabels->getDiffY()) < tolerance) 
						label1 = this->pCombinedLabels->getVoronoi(i - this->pCombinedLabels->getDiffY());
				}

				pFace1 = (*faceVec)[label1];

				if (label1 && (label2 != label1) && (pFace1->getNPoints() >= nPtsMin) && (Cooccurrence.element(label1, label2) < -9999.0))
				{
					Cooccurrence.element(label1, label2) = pFace2->isCoplanar(*pFace1, rmscombined, smin);

					if ((Cooccurrence.element(label1, label2) > 0.0) && (rmscombined < fabs(rmsmin)))
					{
						rmsmin = rmscombined;
						label2Min = label2;
						label1Min = label1;
					}
				}
			}
		}

		if (rmsmin > 0.0)
		{
			merged = true;
			unsigned int oldI = this->pointsInPlanes[label2Min].size();
			this->pCombinedLabels->mergeLabels(label1Min, label2Min, false);
			this->pointsInPlanes[label2Min].resize(this->pointsInPlanes[label2Min].size() + this->pointsInPlanes[label1Min].size());
			for (unsigned int i = 0; i < this->pointsInPlanes[label1Min].size(); ++i)
			{
				this->pointsInPlanes[label2Min][i + oldI] = this->pointsInPlanes[label1Min][i];
			}
			this->pointsInPlanes[label1Min].resize(0);

			(*faceVec)[label2Min]->merge(*(*faceVec)[label1Min]);
			(*faceVec)[label1Min]->resetData(-1, this->reduction);
			ostrstream oss;
			oss.setf(ios::fixed, ios::floatfield);
			oss << "\n  Merging planes "; oss.width(3); oss << label1Min << " and "; oss.width(3);
			oss << label2Min << "\n    Combined plane: "; oss.width(3); oss << (*faceVec)[label2Min]->getIndex() << "( ";
			oss.width(4); oss <<  (*faceVec)[label2Min]->getNPoints() << " points):" <<  (*faceVec)[label2Min]->getDumpString().GetChar();
			protHandler.print(oss, protHandler.prt);
			this->exportObjectLabelBufferToTiff();
		}


	} while (merged);

	str = "\n\n Co-planar faces merged, Time: " + CSystemUtilities::timeString() + "\n\n List of planes:\n ===============\n";
	protHandler.print(str, protHandler.prt);

	this->pCombinedLabels->renumberLabels();
	int nFaces = 1;
	for (unsigned int i = 1; i < faceVec->size(); ++i)
	{
		if ((*faceVec)[i]->getNPoints() > 0)
		{
			*(*faceVec)[nFaces] = *(*faceVec)[i];
			(*faceVec)[nFaces]->setIndex(nFaces);
			this->pointsInPlanes[nFaces] = this->pointsInPlanes[i];

			for (int k = 0; k < this->pointsInPlanes[nFaces].size(); ++k)
			{
				int idx = this->pointsInPlanes[nFaces][k]->id;
				this->ALSPointLabelIndex[idx] = nFaces;
			}

			ostrstream oss;
			oss.setf(ios::fixed, ios::floatfield);
			oss << "\n    Combined plane: "; oss.width(3); oss << (*faceVec)[nFaces]->getIndex() << "( ";
			oss.width(4); oss <<  (*faceVec)[nFaces]->getNPoints() << " points):" <<  (*faceVec)[nFaces]->getDumpString().GetChar();
			protHandler.print(oss, protHandler.prt);

			++nFaces;
		}
		else
		{
			for (int k = 0; k < this->pointsInPlanes[i].size(); ++k)
			{
				int idx = this->pointsInPlanes[i][k]->id;
				this->ALSPointLabelIndex[idx] = 0;
			}
		}
	}

	for (unsigned int i = nFaces; i < faceVec->size(); ++i)
	{
		delete (*faceVec)[i];
	}
	faceVec->resize(nFaces);
	this->pointsInPlanes.resize(nFaces);

	return (nFaces > 1);
};

//=============================================================================
	 
void CBuildingReconstructor::resetOutsideBuilding()
{
	unsigned short maxDist = (unsigned short) floor((this->resolutionALS / this->imageTrafo.getScaleX()) * sqrt(5.0) * 0.5 * 5.0 + 0.5);
	maxDist *= 2; // about 2 times the ALS resolution!

	CLabelImage ALSlab(*this->pCombinedLabels);
	ALSlab.setAll(0.0f);
	for (int i = 0; i < this->ALSpoints->getNpoints(); ++i)
	{
		C3DPoint p = this->ALSpoints->getPoint(i)->getLastEcho();
		
		p.z -= this->getDTMHeight(p);
		if (p.z < this->minHeight)
		{
			long index = MultiImageSegmenter::getObjectLabelIndex(p);
			if (index >= 0) ALSlab.pixUShort(index) = 1;
		}
	}

	ALSlab.updateVoronoi();
	CImageBuffer mask(0,0, this->pCombinedLabels->getWidth(), this->pCombinedLabels->getHeight(), 1,1,true, 0.0f);

	for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
	{
		if (ALSlab.getDistance(i) < maxDist) mask.pixUChar(i) = 0;
		else mask.pixUChar(i) = 1;
	}

	mask.setMargin(1, 0.0f);
	CCharString fn = this->outputPath + "mask_grd.tif";
	mask.exportToTiffFile(fn.GetChar(), 0, false);

	CLabelImage bldgs(mask, 1.0, 8);
	int maxLabel = 0;
	unsigned int npix = 0;

	for (unsigned int i = 1; i < bldgs.getNLabels(); ++i)
	{
		if (bldgs.getNPixels(i) > npix)
		{		
			maxLabel = i;
			npix = bldgs.getNPixels(i);
		}
	}


	for (unsigned int i = 0; i < this->pCombinedLabels->getNGV(); ++i)
	{
		unsigned short lab = bldgs.getLabel(i);
		if ((lab) && (lab != maxLabel))
		{
			for (unsigned int j =0; j < this->objectLabelVector.size();++j)
				this->objectLabelVector[j]->pixUShort(i) = 0;
			bldgs.pixUShort(i) = 0;
		}
	}
};

//=============================================================================

void CBuildingReconstructor::classifyNeighbours()
{
	protHandler.print("\n\n Classifying neighbourhood relationships:\n ========================================\n", protHandler.prt);
	this->planeGraph->classifyNeighbours(0.5f, float(this->imageTrafo.getScaleX()) * 3.0f, float (this->resolutionALS));
};

//=============================================================================

void CBuildingReconstructor::exportToVRML(const CCharString &filename) const
{
	vrBrowser Browser;

	for (unsigned int i = 0; i < this->planeGraph->getFaceVector()->size(); i++)
	{
		CBuilding building;
		CBuildingFace *face = (*this->planeGraph->getFaceVector())[i];
		if (face->getNPolys())
		{
			CXYZPolyLine roofPoly(*face->getPoly(0)->getCombinedPoly());
			CXYZPolyLine floorPoly(roofPoly);
			for (int k = 0; k < roofPoly.getPointCount(); ++k)
			{
				roofPoly.getPoint(k)->x += this->reduction.x;
				roofPoly.getPoint(k)->y += this->reduction.y;
				roofPoly.getPoint(k)->z += this->reduction.z;
				floorPoly.getPoint(k)->x += this->reduction.x;
				floorPoly.getPoint(k)->y += this->reduction.y;
				floorPoly.getPoint(k)->z = this->pmin.z;
			}

			building.addRoofPlane(roofPoly, floorPoly, true);

			for (unsigned int j = 1; j < face->getNPolys(); ++j)
			{
				CXYZPolyLine roofPoly1(*face->getPoly(j)->getCombinedPoly());
				CXYZPolyLine floorPoly1(roofPoly1);
				for (int k = 0; k < roofPoly1.getPointCount(); ++k)
				{
					roofPoly1.getPoint(k)->x += this->reduction.x;
					roofPoly1.getPoint(k)->y += this->reduction.y;
					roofPoly1.getPoint(k)->z += this->reduction.z;
					floorPoly1.getPoint(k)->x += this->reduction.x;
					floorPoly1.getPoint(k)->y += this->reduction.y;
					floorPoly1.getPoint(k)->z = this->pmin.z;
				}

				building.addInnerCourtyard(roofPoly1, floorPoly1);
			}

			building.writeVRML2File(Browser, this->reduction);
		}
	}

 	vrWriteTraverser vrWT;
	vrWT.SetTabStop(2, ' ');      // Change the tab character and the number of characters per indent
	vrWT.SetDigits(3);            // Change the number of digits to write for SFFloat fields
	vrWT.SetVerbose(0);
	
	ostrstream oss;
	oss << "#	Coordinate system: " << this->sensorModelVector[0]->getRefSys().getCoordinateSystemName().GetChar() 
		<< "\n" << ends;
	
	char *z = oss.str();
	vrWT.setCoordSysDescriptor(z);
	delete [] z;

	
	ostrstream oss1; 
	oss1.setf(ios::fixed, ios::floatfield);
	oss1.precision(3);
	oss1 << "#	Shift for all coordinate values:\n#		X-Shift: ";
	oss1.width(15);
	oss1 << reduction.x << "\n#		Y-Shift: ";
	oss1.width(15);
	oss1 << reduction.y << "\n#		Z-Shift: ";
	oss1.width(15);
	oss1 << reduction.z << "\n\nBackground\n {"
		 << "\n  skyAngle    [ 1.2, 1.5 ]"
		 << "\n  skyColor    [ 0.5 0.7  1, 0.5 0.7  1, 0.5 0.7  1 ]"
		 << "\n  groundAngle [1.57]"
         << "\n  groundColor [ 0.5 0.7  1, 0.5 0.7  1]\n }\n" << ends;

	z = oss1.str();
	vrWT.setReductionDescriptor(z);
	delete [] z;

	vrWT.SetFilename(filename.GetChar()); // Set the output filename

	Browser.Traverse(&vrWT);	
};

//=============================================================================

bool CBuildingReconstructor::reconstruct()
{
	ALSPointLabelIndex.resize(this->ALSpoints->getNpoints(), 0);

	bool ret = this->segment();

	if (ret)
	{
		this->exportProjectedLabelBuffersToTiff("_orig");
//		this->resetOutsideBuilding();

		this->exportProjectedLabelBuffersToTiff();
	
//		int nMin = 8;
		int nMin = 4;
		
		ret = this->initialiseFaces(nMin);
		this->checkCombinedHeights();
		
		if (ret)
		{
			ret = this->resetToOneBuilding();
			this->exportObjectLabelBufferToTiff("_beforeMerge5_orig");
			ret = this->mergeCoplanarFaces(nMin, false);
			this->pCombinedLabels->dump(this->outputPath.GetChar(), "combined_beforeInitALS","", true, true);
			
			this->initialiseFacesALS(nMin);
			this->pCombinedLabels->dump(this->outputPath.GetChar(), "combined_afterInitALS","", true, true);

			
			this->initialiseFaces(3);
			this->pCombinedLabels->dump(this->outputPath.GetChar(), "combined_beforeCheckALS","", true, true);
			this->checkNearPoints(2.0 * this->resolutionALS, 5);
			this->pCombinedLabels->dump(this->outputPath.GetChar(), "combined_afterCheckALS","", true, true);

			ret = this->resetToOneBuilding();

			CCharString str("\n\n Merging co-planar faces\n =======================\n\n Time: ");
			str += CSystemUtilities::timeString() + "\n";
			protHandler.print(str, protHandler.prt);

			this->exportObjectLabelBufferToTiff("_beforeMerge4");

			str = "\n\n Merging co-planar faces\n =======================\n\n Time: ";
			str += CSystemUtilities::timeString() + "\n";
			protHandler.print(str, protHandler.prt);

			ret = this->mergeCoplanarFaces(4, false);
			this->exportObjectLabelBufferToTiff("_beforeMerge3");

			ret = this->mergeCoplanarFaces(3, false);
			this->exportObjectLabelBufferToTiff("_beforeMerge");

			ret = this->mergeCoplanarFaces(3, true);
			this->exportObjectLabelBufferToTiff();
			this->exportPointCloudSeparated("face_mrg");
			
			if (ret) 
			{
				int tolerance = (int) floor(this->resolutionALS / this->imageTrafo.getScaleX() + 0.5);
				this->pCombinedLabels->makeGapsSmallByVoronoi(tolerance, false, true, true);
			
				this->exportObjectLabelBufferToTiff("_gaps");
			
	

				ret = this->planeGraph->initialiseNeighbours(this->pCombinedLabels);

//				this->classifyNeighbours();
				
//				this->planeGraph->combineNeighbourPolygons();
				
				CCharString filename = this->outputPath + "neighbours.dxf";
				this->planeGraph->exportPolygons(filename.GetChar(), true, false, false); //true);

//				filename = this->outputPath + "bld.wrl";
//				this->exportToVRML(filename);
//				this->matchEdges(100);
			}

			protHandler.print("\nEnd", protHandler.prt);

	
/*		
		
		if (ret) ret = computePlanes(true);
		if (ret) ret = intersectLabels(0,1);
		if (ret) checkCombinedHeights();
		if (ret) ret = this->computePlanes(" Combined label image: ", *this->pCombinedLabels, 
			                               *this->planeGraph->getFaceVector(), this->pointsInPlanes, true, true);
		if (ret) ret = this->computePlanesWithDSM(true);
		if (ret) 
		{
			ret = this->planeGraph->initialiseNeighbours(this->pCombinedLabels);
			CCharString filename = this->outputPath + "neighbours.dxf";
			this->planeGraph->exportPolygons(filename.GetChar(), true, false, true);

		}

		if (ret) ret = this->matchEdges(3);
		if (ret) ret = this->matchEdges(2);
		if (ret) ret = this->matchEdges(1);
		
		this->exportBuffersToTiff();

		this->exportPointCloud();
*/
		}
		else
		{
			this->exportPointCloudSeparated("face");
		};
	}

	return ret; 
};

//=============================================================================
#endif // WIN32