#include <float.h>
#include <strstream>
using namespace std;

#include "DSMRoughness.h"
#include "utilities.h"
#include "newmat.h"
#include "filter.h"
#include "sortVector.h"
#include "DEM.h"
#include "LookupTable.h"
#include "SystemUtilities.h"
#include "XYLineSegment.h"



//****************************************************************************
//****************************************************************************
 
 float CDSMRoughness::Sines[4000];
 float CDSMRoughness::Cosines[4000];
    
 bool CDSMRoughness::isInit = false;

 CDSMRoughness::CDSMRoughness (int offsetX, int offsetY, int width, int height, 
							   const C2DPoint &luc, const C2DPoint &Delta): 
         pGradientX(0), pGradientY(0), meanHomogeneity (0.0f), 
	     medianHomogeneity(0.0f), varianceHomogeneity(0.0f), minHomogeneity(0.0f),
         maxHomogeneity(0.0f), quantil(0.5f), quantilThreshold (0.0f),
		 deltaPercent(0.001f)
{
	init(offsetX, offsetY, width, height, luc, Delta);

	if (!isInit)
	{
		float rho10 = float(M_PI / 2000.0f);

		for (unsigned long u = 0; u < 4000; ++u)
		{
			Sines[u]   = sin(u * rho10);
			Cosines[u] = cos(u * rho10);
		};

		isInit = true;
	};
};

//=============================================================================

CDSMRoughness::~CDSMRoughness ()
{
	if (pGradientX) delete pGradientX; 
	if (pGradientY) delete pGradientY;
};

//=============================================================================

void CDSMRoughness::init(int offsetX, int offsetY, int width, int height, 
		                 const C2DPoint &luc, const C2DPoint &Delta)
{
	Matrix parms(6,1);
	parms.element(0, 0) =  fabs(Delta.x);
    parms.element(1, 0) =  0.0;
    parms.element(2, 0) =  luc.x;
	parms.element(3, 0) =  0.0;
    parms.element(4, 0) = -fabs(Delta.y);
    parms.element(5, 0) =  luc.y;

	this->trafo.setAll(parms);

	this->Homogeneity.set(offsetX, offsetY, width, height, 1, 32, false);
	this->Isotropy.set(offsetX, offsetY, width, height, 1, 32, false);
	this->Direction.set(offsetX, offsetY, width, height, 1, 32, false);
	this->FoerstnerImage.set(offsetX, offsetY, width, height, 1, 8, true);

	if (this->pGradientX)
		this->pGradientX->set(offsetX, offsetY, width, height, 1, 32, false);

	if (this->pGradientY)
		this->pGradientY->set(offsetX, offsetY, width, height, 1, 32, false);
};

//=============================================================================

bool CDSMRoughness::hasGradient() const 
{
	return ((pGradientX != 0) && (pGradientY != 0)); 
};


//=============================================================================

float CDSMRoughness::gradientStrength(int index, bool squareFlag) const
{
	C2DPoint grad = this->gradient(index);
 
	if (squareFlag) return (float) grad.getSquareNorm();

	grad.x = fabs(grad.x);

	grad.y = fabs(grad.y);

	return (grad.x > grad.y)? (float) grad.x : (float) grad.y;
};

//=============================================================================

float CDSMRoughness::gradientStrength(int index, const C2DPoint &dir) const
{ 
	C2DPoint grad = gradient(index);
	return (float) fabs(grad.innerProduct(dir));
};


//=============================================================================

void CDSMRoughness::initGradients()
{
	if (!this->pGradientX) this->pGradientX = new CImageBuffer;
	if (!this->pGradientY) this->pGradientY = new CImageBuffer;
 

	this->pGradientX->set(this->getOffsetX(), 
		                  this->getOffsetY(), 
						  this->getWidth(), 
						  this->getHeight(), 1, 32, false);
	this->pGradientY->set(this->getOffsetX(), 
		                  this->getOffsetY(), 
						  this->getWidth(), 
						  this->getHeight(), 1, 32, false);
};

//============================================================================

void CDSMRoughness::computeGradients(CImageBuffer &DEMwin, int kernelSize)
{
	if (kernelSize > 0)
	{
		this->initGradients();

	     CImageFilter filter(eDiffX, 1, 3);
		 filter.convolve(DEMwin, *pGradientX, 1);
		 filter.set(eDiffY, 3, 1);
		 filter.convolve(DEMwin, *pGradientY, 1);

		 this->convolveGradients(kernelSize);
	};
};
		           
//============================================================================

void CDSMRoughness::convolveGradients(int kernelSize)
{
	kernelSize = (kernelSize / 2) * 2 + 1;
 
	if (kernelSize > 1)
	{
		CImageFilter filter(eBinom, kernelSize, kernelSize);

		CImageBuffer temp(*pGradientX);
		filter.convolve(temp, *pGradientX, true);

		temp.set(*pGradientY);
		filter.convolve(temp, *pGradientY, true);  
	};
};
                      
//============================================================================

float CDSMRoughness::computeMedian(const CImageBuffer &Wx, const CImageBuffer &Wy,
		                           CImageBuffer *demBuf)
{
	CSortVector<float> sVec(Homogeneity.getNGV(), 0);

	if (!demBuf)
	{
		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
		{
			sVec[u] = Wx.pixFlt(u) + Wy.pixFlt(u);
		};
	}
	else
	{
		unsigned long validIndex = 0;

		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
  		{
			if (demBuf->pixFlt(u) > DEM_NOHEIGHT)
			{
				sVec[validIndex] = Wx.pixFlt(u) + Wy.pixFlt(u);
				++validIndex;
			}
		};

		sVec.resize(validIndex, 0);
	};

	sVec.sortAscending();

	return sVec[sVec.size() / 2];
};

//============================================================================

void CDSMRoughness::computeRandQ(CImageBuffer &DEMwin, int foerstnerKernelSize, 
								 int gradientKernelSize, float Quantil,
								 const C2DPoint &luc, const C2DPoint &Delta,
								 bool parameteriseByPercentage)
{
	this->quantil = Quantil;
	if (this->quantil > 1.0f) this->quantil = 0.5f;

	this->init(DEMwin.getOffsetX(), DEMwin.getOffsetY(), DEMwin.getWidth(), DEMwin.getHeight(), luc, Delta);

	if (gradientKernelSize > 0) this->initGradients();
  
	CImageFilter filter(eDiffX, 1, 3);

	CImageBuffer *pDelta = new CImageBuffer(this->getOffsetX(), 
		                                    this->getOffsetY(), 
						                    this->getWidth(), 
						                    this->getHeight(), 1, 32, false);
	filter.convolve(DEMwin,*pDelta, true);

	vector<float> sigmaNX;
	pDelta->getGaussianNoise(1, 100.0, sigmaNX);
 
	float sigmaX = sqrt(2.0f) / sigmaNX[0];
  
	for (unsigned long u = 0; u < pDelta->getNGV(); u++)
	{
		pDelta->pixFlt(u) *= sigmaX; // => delta ~ N(0,sqrt(2))
	};

	CImageBuffer *pDXX= new CImageBuffer(this->getOffsetX(), 
		                                 this->getOffsetY(), 
						                 this->getWidth(), 
						                 this->getHeight(), 1, 32, false);
	filter.convolve(*pDelta,*pDXX, true);

	filter.set(eDiffY, 3, 1);
  
	CImageBuffer *pDXY= new CImageBuffer(this->getOffsetX(), 
		                                 this->getOffsetY(), 
						                 this->getWidth(), 
						                 this->getHeight(), 1, 32, false);
 	filter.convolve(*pDelta,*pDXY, true);

	filter.convolve(DEMwin,*pDelta, true);

	vector<float> sigmaNY;
	pDelta->getGaussianNoise(1, 100.0, sigmaNY);

	float sigmaY = sqrt(2.0f) / sigmaNY[0];
  
	for (unsigned long u = 0; u < pDelta->getNGV(); u++)
	{
		pDelta->pixFlt(u) *= sigmaY; // => delta ~ N(0,sqrt(2))
	};

	CImageBuffer *pDYY= new CImageBuffer(this->getOffsetX(), 
		                                 this->getOffsetY(), 
						                 this->getWidth(), 
						                 this->getHeight(), 1, 32, false);
 	filter.convolve(*pDelta,*pDYY, true);
	filter.set(eDiffX, 1, 3);

	CImageBuffer *pDYX= new CImageBuffer(this->getOffsetX(), 
		                                 this->getOffsetY(), 
						                 this->getWidth(), 
						                 this->getHeight(), 1, 32, false);
 
 	filter.convolve(*pDelta,*pDYX, true);

	delete pDelta;
	pDelta = 0;

	if (gradientKernelSize > 0) convolveGradients(gradientKernelSize);
  
	CImageBuffer *pDXXXY= new CImageBuffer(this->getOffsetX(), 
		                                   this->getOffsetY(), 
						                   this->getWidth(), 
						                   this->getHeight(), 1, 32, false);
	CImageBuffer *pDYYYX= new CImageBuffer(this->getOffsetX(), 
		                                   this->getOffsetY(), 
						                   this->getWidth(), 
						                   this->getHeight(), 1, 32, false);

	for (unsigned long u = 0; u < pDYYYX->getNGV(); u++)
	{
		pDXXXY->pixFlt(u) = pDXX->pixFlt(u) * pDXY->pixFlt(u);
		pDYYYX->pixFlt(u) = pDYY->pixFlt(u) * pDYX->pixFlt(u);
		pDXX->pixFlt(u)   = pDXX->pixFlt(u) * pDXX->pixFlt(u);   
		pDXY->pixFlt(u)   = pDXY->pixFlt(u) * pDXY->pixFlt(u);
		pDYY->pixFlt(u)   = pDYY->pixFlt(u) * pDYY->pixFlt(u);
		pDYX->pixFlt(u)   = pDYX->pixFlt(u) * pDYX->pixFlt(u);
	};   
 
	filter.set(eBinom, foerstnerKernelSize, foerstnerKernelSize);

	CImageBuffer dRXX(this->getOffsetX(), 
		              this->getOffsetY(), 
					  this->getWidth(), 
					  this->getHeight(), 1, 32, false);

	filter.convolve(*pDXX, dRXX, true);
	delete pDXX;  pDXX = 0;

	CImageBuffer dRXY(this->getOffsetX(), 
		              this->getOffsetY(), 
					  this->getWidth(), 
					  this->getHeight(), 1, 32, false);
	filter.convolve(*pDXY, dRXY, true);
	delete pDXY;  pDXY = 0;

 
	CImageBuffer dRYX(this->getOffsetX(), 
		              this->getOffsetY(), 
					  this->getWidth(), 
					  this->getHeight(), 1, 32, false);
	filter.convolve(*pDYX, dRYX, true);
	delete pDYX;  pDYX = 0;

	CImageBuffer dRYY(this->getOffsetX(), 
		              this->getOffsetY(), 
					  this->getWidth(), 
					  this->getHeight(), 1, 32, false);
	filter.convolve(*pDYY, dRYY, true);
	delete pDYY;  pDYY = 0;


	CImageBuffer dRXXXY(this->getOffsetX(), 
		                this->getOffsetY(), 
					    this->getWidth(), 
					    this->getHeight(), 1, 32, false);
	filter.convolve(*pDXXXY, dRXXXY, true);
	delete pDXXXY;  pDXXXY = 0;


	CImageBuffer dRYYYX(this->getOffsetX(), 
		                this->getOffsetY(), 
					    this->getWidth(), 
					    this->getHeight(), 1, 32, false);
  	filter.convolve(*pDYYYX, dRYYYX, true);
	delete pDYYYX;  pDYYYX = 0;

	float rhoN = float(1000.0f / M_PI);
  
	for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
	{
		float n11 = dRXX.pixFlt(u)   + dRYX.pixFlt(u);
		float n12 = dRXXXY.pixFlt(u) + dRYYYX.pixFlt(u);
		float n22 = dRXY.pixFlt(u)   + dRYY.pixFlt(u);

		Homogeneity.pixFlt(u) = n11 + n22;

		if (Homogeneity.pixFlt(u) < FLT_EPSILON) Isotropy.pixFlt(u) = 0;
		else
		{
			float det =  n11 * n22 - n12 * n12;
			Isotropy.pixFlt(u) = 4 * det / (Homogeneity.pixFlt(u) * Homogeneity.pixFlt(u));
		};
    
		Direction.pixFlt(u) = floor(rhoN * atan2(2.0f * n12, n11 - n22) + 0.5f);
		if (Direction.pixFlt(u) < 0) Direction.pixFlt(u) += 2000.0f; 
	};

	computeHomogeneityStatistics(&DEMwin, foerstnerKernelSize + 4);
	if (parameteriseByPercentage) 
	{
		this->applyPercentagePoints();
		this->quantilThreshold = this->quantil;
	}
}

//============================================================================
    
CCharString CDSMRoughness::statisticsString() const
{
	ostrstream oss;
	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(3);
	oss << "\n   Statistics of homogeneity:\n     Mean:               " << meanHomogeneity
		<< "\n     Median:             " << medianHomogeneity
		<< "\n     Standard deviation: ";
	oss.precision(1);
	oss << sqrt(varianceHomogeneity);
	oss.precision(3);
	oss << "\n     Min:                " << minHomogeneity
		<< "\n     Max:                " << maxHomogeneity
		<< "\n     Percentage:         ";
	oss.precision(1);
	oss << quantil * 100.0f;
	oss.precision(3);
	oss << "\n     Threshold:          " << quantilThreshold << ends;

	char *z = oss.str();

	CCharString str(z);

	delete[] z;

	return str;
};

//============================================================================

void CDSMRoughness::computeHomogeneityStatistics(CImageBuffer *demBuf, int kernelsize)
{
	meanHomogeneity = medianHomogeneity = varianceHomogeneity = 0.0f;
	minHomogeneity = FLT_MAX;
	maxHomogeneity = -FLT_MAX;
   
	CSortVector<float> sVec(Homogeneity.getNGV(), 0);
	
	if (!demBuf)
	{
		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
		{
			float R = Homogeneity.pixFlt(u);
			sVec[u] = R;
			meanHomogeneity += R;
			varianceHomogeneity += R * R;
			if (R < minHomogeneity) minHomogeneity = R;
			if (R > maxHomogeneity) maxHomogeneity = R;
		};
	}
	else
	{
		CImageBuffer buf(0,0, demBuf->getWidth(), demBuf->getHeight(),1,1,true);
		for (unsigned long u = 0; u < demBuf->getNGV(); u++)
		{
			if (demBuf->pixFlt(u) > DEM_NOHEIGHT) buf.pixUChar(u) = 0;
			else buf.pixUChar(u) = 1;
		}

		CImageFilter morpho(eMorphological, kernelsize, kernelsize, 1);

		CImageBuffer buf1(buf, false);
		morpho.binaryMorphologicalClose(buf,buf1,1);

		unsigned long validIndex = 0;
		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
		{
			if (buf1.pixUChar(u) == 0)	
			{
				float R = Homogeneity.pixFlt(u);
				sVec[u] = R;
				meanHomogeneity += R;
				varianceHomogeneity += R * R;
				if (R < minHomogeneity) minHomogeneity = R;
				if (R > maxHomogeneity) maxHomogeneity = R;
				++validIndex;
			}
			else demBuf->pixFlt(u) = DEM_NOHEIGHT;
		};

		sVec.resize(validIndex, 0.0);
	};

  
	sVec.sortAscending();
  
	unsigned long quantilIndex = (unsigned long)(floor(0.5f * float(sVec.size())));

	medianHomogeneity = sVec[quantilIndex];
  
	quantilIndex = (unsigned long)(floor(quantil * float(sVec.size())));
	quantilThreshold = sVec[quantilIndex];
  
  
	if (sVec.size() > 1)
	{
		varianceHomogeneity = (varianceHomogeneity - meanHomogeneity * meanHomogeneity / float(sVec.size())) / float(sVec.size()-1);
		meanHomogeneity /= float(sVec.size());
	}
	else varianceHomogeneity = 0.0f;

	percentiles.resize(int(floor(1.0 / this->deltaPercent)) + 1);
	float deltaPercentage = this->deltaPercent * sVec.size();
	float index = deltaPercentage;

	percentiles[0] = 0.0f;
 
	for (unsigned long u = 1; u <  percentiles.size(); ++u, index += deltaPercentage)
	{
		long dmIdx = (unsigned long) floor(index) - 1;
		if (dmIdx >= sVec.size()) dmIdx = sVec.size() - 1;
		else if (dmIdx < 0) dmIdx = 0;

		percentiles[u] = sVec[dmIdx];
	};

};

//============================================================================

float CDSMRoughness::getPercentage(float value) const
{
	float ret = 0.0;

	if (value > 0)
	{
		if (value >= this->percentiles[this->percentiles.size() - 1]) ret = 1.0;
		else
		{
			int imin = 0, imax = this->percentiles.size() - 1;

			while (imax - imin > 1)
			{
				int i = (imin + imax) / 2;

				if (this->percentiles[i] <= value) imin = i;
				else imax = i;
			}

			float v1 = percentiles[imin];
			float v2 = percentiles[imax];
			float diff = v2 - v1;

			if (diff < FLT_EPSILON) ret = float(double(imin) * this->deltaPercent);
			else
			{
				ret = float(imin) + (value - v1) / diff;
				ret *= float(this->deltaPercent);
			}
		}
	}

	return ret;
};

//============================================================================

void CDSMRoughness::applyPercentagePoints(CImageBuffer &buf) const
{
	buf.set(this->getOffsetX(), this->getOffsetY(), this->getWidth(), this->getHeight(), 1, 32, false);
	for (unsigned long i = 0; i < this->Homogeneity.getNGV(); ++i)
	{
		buf.pixFlt(i) = getPercentage(i);
	}
};

//============================================================================

void CDSMRoughness::applyPercentagePoints()
{
	for (unsigned long i = 0; i < this->Homogeneity.getNGV(); ++i)
	{
		this->Homogeneity.pixFlt(i) = getPercentage(i);
	}
};

//============================================================================
 
void CDSMRoughness::computeRandQandClassify(CImageBuffer &DEMwin, 
											float Quantil, float thrQ,                             
											int foerstnerKernelSize, 
											int gradientKernelSize,
											const C2DPoint &luc, 
											const C2DPoint &Delta,
											bool parameteriseByPercentage)
{
	computeRandQ(DEMwin, foerstnerKernelSize, gradientKernelSize, Quantil, 
		         luc, Delta, parameteriseByPercentage); 

	classify(thrQ);
};
  
//============================================================================

float CDSMRoughness::initQuantilThreshold(float Quantil, CImageBuffer *demBuf) const
{
	if (Quantil > 1.0f) Quantil = 0.5f;
	else if (Quantil < 0.0f) Quantil = 0.5f;
  
	CSortVector<float> sVec(Homogeneity.getNGV(), 0);
	
	if (!demBuf)
	{
		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
		{
			sVec[u] = Homogeneity.pixFlt(u);
		};
	}
	else
	{
		unsigned long validIndex = 0;
		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
		{
			if (demBuf->pixFlt(u) > DEM_NOHEIGHT)	
			{
				sVec[u] = Homogeneity.pixFlt(u);
				++validIndex;
			};
		};

		sVec.resize(validIndex, 0);
	};

  
	sVec.sortAscending();

	float thr = sVec[(unsigned long)(floor(Quantil * float(sVec.size())))];
  
	return thr;
};

//============================================================================

float CDSMRoughness::setQuantilThreshold(float Quantil, CImageBuffer *demBuf)
{
	this->quantil = Quantil;
	if (this->quantil > 1.0f) this->quantil = 0.5f;
	this->quantilThreshold = initQuantilThreshold(Quantil, demBuf);
 
	return this->quantilThreshold;
 };

//============================================================================

float CDSMRoughness::setQuantilThreshold(float quantilInput, float qu1, float qu2, 
                                         float &thr1, float &thr2, 
                                         CImageBuffer *demBuf)
{
	this->quantil = quantilInput;
	if (this->quantil > 1.0f) this->quantil = 0.5f;

	CSortVector<float> sVec(Homogeneity.getNGV(), 0);
	
	if (!demBuf)
	{
		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
		{
			sVec[u] = Homogeneity.pixFlt(u);
		};
	}
	else
	{
		unsigned long validIndex = 0;
		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
		{
			if (demBuf->pixFlt(u) > DEM_NOHEIGHT)	
			{
				sVec[u] = Homogeneity.pixFlt(u);
				++validIndex;
			};
		};

		sVec.resize(validIndex, 0);
	};

	sVec.sortAscending();

	this->quantilThreshold = sVec[(unsigned long)(floor(quantil * float(sVec.size())))];

	thr1 = sVec[(unsigned long)(floor(qu1 * float(sVec.size())))];
	thr2 = sVec[(unsigned long)(floor(qu2 * float(sVec.size())))];

	return this->quantilThreshold;
};

//============================================================================

void CDSMRoughness::getQuantilThresholds(float qu1, float qu2, float &thr1, 
										 float &thr2, CImageBuffer *demBuf)
{
	CSortVector<float> sVec(Homogeneity.getNGV(), 0);
	
	if (!demBuf)
	{
		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
		{
			sVec[u] = Homogeneity.pixFlt(u);
		};
	}
	else
	{
		unsigned long validIndex = 0;
		for (unsigned long u = 0; u < Homogeneity.getNGV(); u++)
		{
			if (demBuf->pixFlt(u) > DEM_NOHEIGHT)	
			{
				sVec[u] = Homogeneity.pixFlt(u);
				++validIndex;
			};
		};

		sVec.resize(validIndex, 0);
	};

	sVec.sortAscending();
  
	thr1 = sVec[(unsigned long)(floor(qu1 * float(sVec.size())))];
	thr2 = sVec[(unsigned long)(floor(qu2 * float(sVec.size())))];
};

//============================================================================

void CDSMRoughness::classify(float thrQ)
{
	for (unsigned long u = 0; u < Homogeneity.getNGV(); u++) 
	{
		if (Homogeneity.pixFlt(u) <  this->quantilThreshold) FoerstnerImage.pixUChar(u) = 0;   
		else if (Isotropy.pixFlt(u) < thrQ) FoerstnerImage.pixUChar(u) = 1;
		else FoerstnerImage.pixUChar(u) = 2;
	};
};

//============================================================================

void CDSMRoughness::classify(float Quantil, float thrQ, 
                             CImageBuffer *demBuf)
{
	setQuantilThreshold(Quantil, demBuf);
	classify(thrQ);
};

//============================================================================

void CDSMRoughness::classifyByThreshold(float thrW, float thrQ)
{
	thrW *= medianHomogeneity;

	for (unsigned long u = 0; u < Homogeneity.getNGV(); u++) 
	{
		if (Homogeneity.pixFlt(u) <  thrW) FoerstnerImage.pixUChar(u) = 0;   
		else if (Isotropy.pixFlt(u) < thrQ) FoerstnerImage.pixUChar(u) = 1;
		else FoerstnerImage.pixUChar(u) = 2;
	};
};

//============================================================================

void CDSMRoughness::exportTiff(const CCharString &path, CCharString filename)
{
	CLookupTable lut(8,3);

	lut.setColor(255,   0,   0, 0);
	lut.setColor(  0, 255,   0, 1);
	lut.setColor(  0,   0, 255, 2);
	
	
	if (path != "") 
	{
		filename = path + CSystemUtilities::dirDelimStr + filename;
	}
	
	FoerstnerImage.exportToTiffFile(filename.GetChar(), &lut, false);
};
    
//===========================================================================

void CDSMRoughness::exportDirections(const CCharString &path, 
									 CCharString filename)
{
	if (path != "")  
	{
		filename = path + CSystemUtilities::dirDelimStr + filename;
	};

	Direction.exportToTiffFile(filename.GetChar(), 0, true);
};

//============================================================================

void CDSMRoughness::exportHomogeneityIsotropy(const CCharString &path, 
                                              CCharString homogFile, 
                                              CCharString isoFile)
{
	if (path != "") 
	{
		homogFile = path + CSystemUtilities::dirDelimStr + homogFile;
		isoFile   = path + CSystemUtilities::dirDelimStr + isoFile;
	};
  
	Homogeneity.exportToTiffFile(homogFile.GetChar(), 0, true);
	Isotropy.exportToTiffFile(isoFile.GetChar(), 0, true);
};

//============================================================================

void CDSMRoughness::exportAll(const CCharString &path, CCharString homogFile,   
							  CCharString isoFile, CCharString directionFile)
{
	this->exportHomogeneity(path, homogFile); 
	this->exportIsotropy(path, isoFile);
	this->exportDirections(path, directionFile);
};

//============================================================================

void CDSMRoughness::exportIsotropy(const CCharString &path, CCharString filename)
{
	if (path != "") filename = path + CSystemUtilities::dirDelimStr + filename;
	Isotropy.exportToTiffFile(filename.GetChar(), 0, true);
};

//============================================================================

void CDSMRoughness::exportHomogeneity(const CCharString &path, CCharString filename)
{
	if (path != "") filename = path + CSystemUtilities::dirDelimStr + filename;

	this->Homogeneity.exportToTiffFile(filename.GetChar(), 0, true);
};

//============================================================================

void CDSMRoughness::exportGradient(const CCharString &path, 
								   CCharString filename,
								   bool squareFlag)
{
	if (this->pGradientX &&this-> pGradientY) 
	{
		CImageBuffer gradStrength(this->getOffsetX(), this->getOffsetY(), 
						          this->getWidth(),   this->getHeight(), 1, 32, false);

		for (unsigned long u = 0; u < gradStrength.getNGV(); ++u)
		{
			gradStrength.pixFlt(u) = this->gradientStrength(u,squareFlag);
		};

		CLookupTable lut(8,3);
		lut.setColor(255,   0,   0, 0);
 
		if (path != "") filename = path + CSystemUtilities::dirDelimStr + filename;
		gradStrength.exportToTiffFile(filename.GetChar(), &lut, true);
	};
};

//============================================================================
    
vector <C3DPoint> *CDSMRoughness::pDerivatives(const CXYLineSegment &segment, bool squareFlag) const
{
	vector <C3DPoint> *pvec = 0;

	C2DPoint dir = segment.getDirection();

	C2DPoint pos1 = segment.getStartPt();
	C2DPoint pos2 = segment.getEndPt();
  
	double pixelsizeX = fabs(this->trafo.getA());

	if (fabs(dir.x) < fabs(dir.y)) 
	{
		int yMin = int(floor(pos1.y + 0.5f));
		int yMax = int(floor(pos2.y + 0.5f));

		if (yMin > yMax)
		{
			pos1 = pos2;
			int dmy = yMin;
			yMin = yMax;
			yMax = dmy;
		};

		pvec = new vector <C3DPoint>(yMax - yMin + 1);

		double dirXbyY = dir.x / dir.y;

		for (unsigned long u = 0; u < pvec->size(); ++yMin, ++u)
		{
			(*pvec)[u].y = yMin;
			(*pvec)[u].x = pos1.x + (double(yMin) - pos1.y) * dirXbyY;
			int index = FoerstnerImage.getIndex(long(floor((*pvec)[u].y + 0.5)), long(floor((*pvec)[u].x + 0.5)));
			(*pvec)[u].z = sqrt(this->gradientStrength(index, squareFlag) / pixelsizeX);
		};
	}
	else 
	{
		int xMin = int(floor(pos1.x + 0.5f));
		int xMax = int(floor(pos2.x + 0.5f));

		if (xMin > xMax)
		{
			pos1 = pos2;
			int dmy = xMin;
			xMin = xMax;
			xMax = dmy;
		};

		pvec = new vector <C3DPoint>(xMax - xMin + 1);
   
		double dirYByX = dir.y / dir.x;

		for (unsigned long u = 0; u < pvec->size(); ++xMin, ++u)
		{
			(*pvec)[u].x = xMin;
			(*pvec)[u].y = pos1.y + (double(xMin) - pos1.x) * dirYByX;
			int index = FoerstnerImage.getIndex(long(floor((*pvec)[u].y + 0.5)), long(floor((*pvec)[u].x + 0.5)));
			(*pvec)[u].z = sqrt(this->gradientStrength(index, squareFlag)  / pixelsizeX);
		};
	};

  return pvec;
};

//****************************************************************************
