#ifdef WIN32
#include <float.h>
#include <strstream>
using namespace std;

#include "BuildingDetector.h"
#include "DSMBuilding.h"
#include "DempsterShaferBuildings.h"
#include "DSMRoughness.h"
#include "LabelImage.h"
#include "DEM.h"
#include "filter.h"
#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "BuildingArray.h"
#include "LookupTableHelper.h"

//****************************************************************************
//****************************************************************************

CBuildingDetector::CBuildingDetector (float minBuildingSize, 
									  CDEM *DSMLast, CDEM *DSMFirst, CDEM *DTM, 
									  CHugeImage *NDVI, CHugeImage *NDVISigma, 	 
									  const CTFW *NDVITFW,
									  CHugeImage *ExistingDatabase,
									  const CTFW *ExistingTFW,
									  float HeightThreshold,
									  const C2DPoint &Resolution,
									  const C2DPoint &min, const C2DPoint &max, 
									  const CCharString &directory):
         pDSMLastFile(DSMLast), pDSMFirstFile(DSMFirst), pDTMFile(DTM), 
		 pNDVIFile(NDVI), pNDVISigmaFile(NDVISigma), 
		 pExistingDatabaseFile(ExistingDatabase), 
		 imageTrafo (NDVITFW), existingTrafo(ExistingTFW),
		 pDTM(0), pDSM(0), pFirstLastDiff(0), pNormDSM(0), 
		 pVegetationIndex(0), pVegetationIndexSig(0), 
	 	 roughness(0),  pBuildingMask(0),  pBuildingLabels(0), pHomogeneousLabels(0),
		 pDempsterShafer(0), pExistingBuildingMask(0),
         heightThreshold(HeightThreshold), labelFileName("labels.tif"), 
		 morphFilterSize(minBuildingSize),
		 pMin(min), pMax(max), resolution(Resolution), 
		 projectPath(directory),
		 useRank(false), rankPercentage(0.05)
{
	if (!this->projectPath.IsEmpty())
	{
		if (this->projectPath[this->projectPath.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->projectPath += CSystemUtilities::dirDelimStr;
	}
};

//=============================================================================

CBuildingDetector::~CBuildingDetector ()
{
	if (this->pDTM)                   delete this->pDTM; 
	if (this->pDSM)                   delete this->pDSM; 
	if (this->pFirstLastDiff)         delete this->pFirstLastDiff; 
	if (this->pNormDSM)               delete this->pNormDSM;
	if (this->pVegetationIndex)       delete this->pVegetationIndex;
	if (this->pVegetationIndexSig)    delete this->pVegetationIndexSig;
	if (this->pDempsterShafer)        delete this->pDempsterShafer;
	if (this->roughness)              delete this->roughness;
	if (this->pBuildingMask)          delete this->pBuildingMask;
	if (this->pBuildingLabels)        delete this->pBuildingLabels;
	if (this->pHomogeneousLabels)     delete this->pHomogeneousLabels;
	if (this->pExistingBuildingMask)  delete this->pExistingBuildingMask;

	this->pDSMLastFile          = 0; 
	this->pDSMFirstFile         = 0;
	this->pDTMFile              = 0;
	this->pNDVIFile             = 0;
	this->pNDVISigmaFile        = 0;
	this->pExistingDatabaseFile = 0;

	this->pDTM                  = 0; 
	this->pDSM                  = 0; 
	this->pFirstLastDiff        = 0; 
	this->pNormDSM              = 0;
	this->pVegetationIndex      = 0;
	this->pVegetationIndexSig   = 0;
	this->pDempsterShafer       = 0;
	this->roughness             = 0;
	this->pBuildingMask         = 0;
	this->pBuildingLabels       = 0;
	this->pHomogeneousLabels    = 0;
	this->pExistingBuildingMask = 0;

	this->regionVector.clear();

	protHandler.close(protHandler.prt);
};

//=============================================================================
  
void CBuildingDetector::setUpTrafoAndExtents(const C2DPoint &Resolution,
		                                     const C3DPoint &min, const C3DPoint &max)
{
	CDEM *theDSM = this->pDSMLastFile;
	if (!theDSM) theDSM = this->pDSMFirstFile;

	if (theDSM)
	{
		if ((Resolution.x <=0 ) || (Resolution.y <= 0))
		{
			this->resolution.x = theDSM->getgridX();
			this->resolution.y = theDSM->getgridY();
		}
		else this->resolution = Resolution;

		if ((max.x <= min.x) || (max.y <= min.y))
		{
			this->pMax.x = theDSM->getmaxX();
			this->pMax.y = theDSM->getmaxY();
			this->pMin.x = theDSM->getminX();
			this->pMin.y = theDSM->getminY();
		}
		else 
		{
			this->pMax = max;
			this->pMin = min;
			if (this->pMin.x < theDSM->getminX()) this->pMin.x = theDSM->getminX();
			if (this->pMin.y < theDSM->getminY()) this->pMin.y = theDSM->getminY();
			if (this->pMax.x > theDSM->getmaxX()) this->pMax.x = theDSM->getmaxX();
			if (this->pMax.y > theDSM->getmaxY()) this->pMax.y = theDSM->getmaxY();
		}

		this->pMax.x = this->pMin.x + floor((this->pMax.x - this->pMin.x) / this->resolution.x + 0.5) * this->resolution.x;
		this->pMax.y = this->pMin.y + floor((this->pMax.y - this->pMin.y) / this->resolution.y + 0.5) * this->resolution.y;	

		C2DPoint shift(this->pMin.x, this->pMax.y);
		this->trafo.setFromShiftAndPixelSize(shift, this->resolution);
		this->trafo.setRefSys(theDSM->getTFW()->getRefSys());
	}
};

//=============================================================================
  	   
void CBuildingDetector::getWindowSize(long &Width, long &Height) const
{
	Width = (long) floor((this->pMax.x - this->pMin.x)  / this->resolution.x + 1.5);
	Height = (long) floor((this->pMax.y - this->pMin.y)  / this->resolution.x + 1.5);
};

//=============================================================================
  
void CBuildingDetector::getWindow(long &cmin, long &rmin, long &Width, long &Height) const
{
	cmin = 0;
	rmin = 0;
	getWindowSize(Width, Height);
};

//=============================================================================
  
bool CBuildingDetector::readDEMWindow(CDEM *pDEM, CImageBuffer &buf, const CCharString &name)
{
	bool ret = true;

	long width, height;
	getWindowSize(width, height);
	
	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	oss << "\nReading " << name.GetChar() << "\n========";
	for (int i = 0; i < name.GetLength(); ++i) oss << "=";
	oss << "\n\n File: " << pDEM->getFileNameChar();

	if (!buf.set(0, 0, width, height, 1, 32, false)) 
	{
		oss << "\nError allocating memory for buffer:" << width << " x " << height << " pixels\n";
		ret = false;
	}
	else
	{
		long minXR   = (long) floor((this->pMin.x    - pDEM->getminX()) / pDEM->getgridX());
		long minYR   = (long) floor((pDEM->getmaxY() - this->pMax.y)    / pDEM->getgridY());
		long maxXR   = (long) floor((this->pMax.x    - pDEM->getminX()) / pDEM->getgridX() + 0.5) + 1;
		long maxYR   = (long) floor((pDEM->getmaxY() - this->pMin.y)    / pDEM->getgridY() + 0.5) + 1;
		long widthR  = maxXR - minXR;
		long heightR = maxYR - minYR;

		C2DPoint fact(this->resolution.x / pDEM->getgridX(), this->resolution.y / pDEM->getgridY()); 

		C2DPoint minPoint((this->pMin.x    - pDEM->getminX()) / pDEM->getgridX() - minXR, 
						  (pDEM->getmaxY() - this->pMax.y)    / pDEM->getgridY() - minYR);

		C2DPoint maxPoint((this->pMax.x    - pDEM->getmaxX()) / pDEM->getgridX() - minXR, 
						  (pDEM->getminY() - this->pMin.y)    / pDEM->getgridY() - minYR);
		if (fabs(maxPoint.x - floor(maxPoint.x)) > FLT_EPSILON) widthR++;
		if (fabs(maxPoint.y - floor(maxPoint.y)) > FLT_EPSILON) heightR++;


		oss << "\n Window: min(" << this->pMin.x << " / " << this->pMin.y << "), max("
			<< this->pMax.x << " / " << this->pMax.y << ")\n Resolution: "
			<< this->resolution.x << " / " << this->resolution.y << ", Extents: "
			<< width << " / " << height << " [pixels]";

		if ((fabs(minPoint.x - floor(minPoint.x))   > FLT_EPSILON) || 
			(fabs(minPoint.y - floor(minPoint.x))   > FLT_EPSILON) ||
			(fabs(fact.x - 1.0) > FLT_EPSILON) || (fabs(fact.y - 1.0) > FLT_EPSILON) || 
			(widthR != width) || (heightR != height))
		{
			oss << "\n Bilinear interpolation from original file\n Time: " << CSystemUtilities::timeString().GetChar();

			CImageBuffer rawBuf;

			if (!rawBuf.set(minXR, minYR, widthR, heightR, 1, 32, false, DEM_NOHEIGHT)) 
			{
				oss << "\nError allocating memory for buffer:" << widthR << " x " << heightR << " pixels\n";

				ret = false;
			}
			else if (!pDEM->getRect(rawBuf)) 
			{
				oss << "\nError reading raw buffer: min(" << minXR << " / " << minYR 
					<< ", extents: " << widthR << " x " << heightR << " pixels\n";

				ret = false;
			}
			else
			{
				float factP = 100.0 / float(height);
				C2DPoint P = minPoint;
				long deltaXY = rawBuf.getDiffX() + rawBuf.getDiffY();
				
				for (long r = 0; r < height; ++r, minPoint.y += fact.y)
				{
					long rOld = (long) floor(minPoint.y);
					unsigned long uMinNew = r    * buf.getDiffY();
					unsigned long uMaxNew = uMinNew + buf.getDiffY();

					if ((rOld >= 0) && (rOld + 1 < heightR))
					{
						unsigned long uMinOld = rOld * rawBuf.getDiffY();
						P.x = minPoint.x;
						P.y = minPoint.y;
						float y = (float) P.y - (float) rOld;

						for (unsigned long u = uMinNew; u < uMaxNew; u += buf.getDiffX(), P.x += fact.x)
						{
							long cOld = (long) floor(P.x);
							if ((cOld >= 0) && (cOld + 1 < widthR))
							{
								unsigned long uOld = uMinOld + cOld;
								float Z00 = rawBuf.pixFlt(uOld); 
								float Z10 = rawBuf.pixFlt(uOld + rawBuf.getDiffX());
								float Z01 = rawBuf.pixFlt(uOld + rawBuf.getDiffY());
								float Z11 = rawBuf.pixFlt(uOld + deltaXY);
								if ((Z00 > DEM_NOHEIGHT) && (Z10 > DEM_NOHEIGHT) && 
									(Z01 > DEM_NOHEIGHT) && (Z11 > DEM_NOHEIGHT))
								{
									float x = (float) P.x - cOld;
									float a1 = Z10-Z00;
									float a2 = Z01-Z00;
									float a3 = Z11-Z00 - a1 - a2;
									float z = Z00  + a1 * x + a2 * y + a3 * x * y;
									buf.pixFlt(u) = z;
								}
								else 
								{
									buf.pixFlt(u) = DEM_NOHEIGHT;
								}
							}
							else 
							{
								buf.pixFlt(u) = DEM_NOHEIGHT;
							}
						}
					}
					else
					{
						for (unsigned long u = uMinNew; u < uMaxNew; u += buf.getDiffX())
						{
							buf.pixFlt(u) = DEM_NOHEIGHT;		
						}
					}
					if (this->listener) 
					{
						this->listener->setProgressValue(float(r) * factP);
					}
				}
			}
		}
		else
		{
			if (this->listener) pDEM->setProgressListener(this->listener);

			buf.setOffset(minXR, minYR);
			oss << "\n Reading window from file \n Time: " << CSystemUtilities::timeString().GetChar();
			ret = pDEM->getRect(buf);
			if (!ret)				
				oss << "\nError reading raw buffer: min(" << buf.getOffsetX() << " / " << buf.getOffsetY() 
					<< ", extents: " << buf.getWidth() << " x " << buf.getHeight() << " pixels\n";
			
			buf.setOffset(0, 0);
		}
	}
	
	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}
	
	if (ret) oss << "\n " << name.GetChar() << " read successfully; Time: " 
		         << CSystemUtilities::timeString().GetChar();

	oss  << "\n"<< ends;

	protHandler.print(oss, protHandler.prt);

	return ret;
}

//=============================================================================
  
bool CBuildingDetector::readImageWindow(CHugeImage *pImg, CImageBuffer &buf, 
										const CCharString &name)
{
	bool ret = true;
	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	oss << "\nReading " << name.GetChar() << "\n========";

	for (int i = 0; i < name.GetLength(); ++i) oss << "=";

	oss << "\n\n File: " << pImg->getFileNameChar();


	if (!imageTrafo) 
	{
		oss << "\n Error reading image: Transformation not initialized! ";
		ret = false;
	}
	else
	{
		long width, height;
		getWindowSize(width, height);

		C2DPoint imageRes(fabs(imageTrafo->getA()), fabs(imageTrafo->getE()));
		C2DPoint imageShift = imageTrafo->getShift();

		long minX   = (long) floor((this->pMin.x - imageShift.x)  / this->resolution.x);
		long minY   = (long) floor((imageShift.y - this->pMax.y)  / this->resolution.y);
		
		if (!buf.set(0, 0, width, height, 1, 32, false, DEM_NOHEIGHT)) 
		{
			oss << "\nError allocating memory for buffer:" << width << " x " << height << " pixels\n";
			ret = false;
		}
		else
		{
			long minXR   = (long) floor((this->pMin.x  - imageShift.x) / imageRes.x);
			long minYR   = (long) floor((imageShift.y  - this->pMax.y) / imageRes.y);
			long maxXR   = (long) floor((this->pMax.x  - imageShift.x) / imageRes.x + 0.5) + 1;
			long maxYR   = (long) floor((imageShift.y  - this->pMin.y) / imageRes.y + 0.5) + 1;
	
			long widthR  = maxXR - minXR;
			long heightR = maxYR - minYR;

			C2DPoint fact(this->resolution.x /  imageRes.x, this->resolution.y / imageRes.y); 
			C2DPoint minPoint((this->pMin.x - imageShift.x) / imageRes.x - minXR, 
							  (imageShift.y - this->pMax.y) / imageRes.y - minYR);

			C2DPoint maxPoint(imageShift.x + (pImg->getWidth()  - 1) * imageRes.x,
				              imageShift.y - (pImg->getHeight() - 1) * imageRes.y);
				
			maxPoint.x = (this->pMax.x - maxPoint.x)   / imageRes.x - minXR;
			maxPoint.x = (maxPoint.y   - this->pMin.y) / imageRes.y - minYR;

			if (fabs(maxPoint.x - floor(maxPoint.x)) > FLT_EPSILON) widthR++;
			if (fabs(maxPoint.y - floor(maxPoint.y)) > FLT_EPSILON) heightR++;

			oss << "\n Window: min(" << this->pMin.x << " / " << this->pMin.y << "), max("
				<< this->pMax.x << " / " << this->pMax.y << ")\n Resolution: "
				<< this->resolution.x << " / " << this->resolution.y <<", Extents: "
				<< width << " / " << height << " [pixels]";

			if ((fabs(minPoint.x)   > FLT_EPSILON) || (fabs(minPoint.y)   > FLT_EPSILON) ||
				(fabs(fact.x - 1.0) > FLT_EPSILON) || (fabs(fact.y - 1.0) > FLT_EPSILON) || 
				(widthR != width) || (heightR != height))
			{
				oss << "\n Bilinear interpolation\n Time: " << CSystemUtilities::timeString().GetChar();

				CImageBuffer rawBuf;

				if (!rawBuf.set(minXR, minYR, widthR, heightR, 1, 32, false, DEM_NOHEIGHT)) 
				{
					oss << "\nError allocating memory for buffer:" << widthR << " x " << heightR << " pixels\n";
					ret = false;
				}
				else if (!pImg->getRect(rawBuf)) 
				{
					oss << "\nError reading raw buffer: min(" << minXR << " / " << minYR 
						<< ", extents: " << widthR << " x " << heightR << " pixels\n";

					ret = false;
				}
				else
				{
					float factP = 100.0 / float(height);

					C2DPoint P = minPoint;
					long deltaXY = rawBuf.getDiffX() + rawBuf.getDiffY();
					
					for (long r = 0; r < height; ++r, minPoint.y += fact.y)
					{
						long rOld = (long) floor(minPoint.y);
						unsigned long uMinNew = r    * buf.getDiffY();
						unsigned long uMaxNew = uMinNew + buf.getDiffY();

						if ((rOld >= 0) && (rOld + 1 < heightR))
						{
							unsigned long uMinOld = rOld * rawBuf.getDiffY();
							P.x = minPoint.x;
							P.y = minPoint.y;

							float y = (float) P.y - (float) rOld;

							for (unsigned long u = uMinNew; u < uMaxNew; u += buf.getDiffX(), P.x += fact.x)
							{
								long cOld = (long) floor(P.x);
								if ((cOld >= 0) && (cOld + 1 < widthR))
								{
									unsigned long uOld = uMinOld + cOld;
									float Z00 = rawBuf.pixFlt(uOld); 
									float Z10 = rawBuf.pixFlt(uOld + rawBuf.getDiffX());
									float Z01 = rawBuf.pixFlt(uOld + rawBuf.getDiffY());
									float Z11 = rawBuf.pixFlt(uOld + deltaXY);
									if ((Z00 > -100.0) && (Z10 > -100.0) && 
										(Z01 > -100.0) && (Z11 > -100.0))
									{
										float x = (float) P.x - cOld;
										float a1 = Z10-Z00;
										float a2 = Z01-Z00;
										float a3 = Z11-Z00 - a1 - a2;
										float z = Z00  + a1 * x + a2 * y + a3 * x * y;
										buf.pixFlt(u) = z;
									}
									else
									{
										buf.pixFlt(u) = DEM_NOHEIGHT;
									}
								}
								else
								{
									buf.pixFlt(u) = DEM_NOHEIGHT;
								}
							}
						}
						else
						{
							for (unsigned long u = uMinNew; u < uMaxNew; u += buf.getDiffX())
							{
								buf.pixFlt(u) = DEM_NOHEIGHT;		
							}
						}
						if (this->listener) 
						{
							this->listener->setProgressValue(float(r) * factP);
						}
					}
				}
				
			}
			else 
			{
				if (this->listener) pImg->setProgressListener(this->listener);
				buf.setOffset(minXR, minYR);
				oss << "\n Reading window from file \n Time: " << CSystemUtilities::timeString().GetChar();
				ret = pImg->getRect(buf);
				if (!ret)				
					oss << "\nError reading raw buffer: min(" << buf.getOffsetX() << " / " << buf.getOffsetY() 
						<< ", extents: " << buf.getWidth() << " x " << buf.getHeight() << "pixels\n";

				buf.setOffset(0, 0);
			}
		}
	}

	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}
	
	if (ret) oss << "\n " << name.GetChar() << " read successfully; Time: " 
		         << CSystemUtilities::timeString().GetChar();

	oss  << "\n"<< ends;

	protHandler.print(oss, protHandler.prt);

	return ret;
}
	  
bool CBuildingDetector::readExistingDatabase()
{
	this->existingBuildPercentage = 0.0;

	bool ret = true;
	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	oss << "\nReading existing building data base\n===================================";
	oss << "\n\n File: " << pExistingDatabaseFile->getFileNameChar();

	if (!existingTrafo) 
	{
		oss << "\n Error reading existing building data base: Transformation not initialized! ";
		ret = false;
	}
	else
	{
		long width  = this->pExistingBuildingMask->getWidth();
		long height = this->pExistingBuildingMask->getHeight();
		C2DPoint imageRes(fabs(existingTrafo->getA()), fabs(existingTrafo->getE()));
		C2DPoint imageShift = existingTrafo->getShift();

		long minX   = (long) floor((this->pMin.x - imageShift.x)  / this->resolution.x);
		long minY   = (long) floor((imageShift.y - this->pMax.y)  / this->resolution.y);
		
		long minXR   = (long) floor((this->pMin.x  - imageShift.x) / imageRes.x);
		long minYR   = (long) floor((imageShift.y  - this->pMax.y) / imageRes.y);
		long maxXR   = (long) floor((this->pMax.x  - imageShift.x) / imageRes.x + 0.5) + 1;
		long maxYR   = (long) floor((imageShift.y  - this->pMin.y) / imageRes.y + 0.5) + 1;

		long widthR  = maxXR - minXR;
		long heightR = maxYR - minYR;

		C2DPoint fact(this->resolution.x /  imageRes.x, this->resolution.y / imageRes.y); 
		C2DPoint minPoint((this->pMin.x - imageShift.x) / imageRes.x - minXR, 
						  (imageShift.y - this->pMax.y) / imageRes.y - minYR);

		C2DPoint maxPoint(imageShift.x + (pExistingDatabaseFile->getWidth()  - 1) * imageRes.x,
			              imageShift.y - (pExistingDatabaseFile->getHeight() - 1) * imageRes.y);
			
		maxPoint.x = (this->pMax.x - maxPoint.x)   / imageRes.x - minXR;
		maxPoint.x = (maxPoint.y   - this->pMin.y) / imageRes.y - minYR;

		if (fabs(maxPoint.x - floor(maxPoint.x)) > FLT_EPSILON) widthR++;
		if (fabs(maxPoint.y - floor(maxPoint.y)) > FLT_EPSILON) heightR++;

		oss << "\n Window: min(" << this->pMin.x << " / " << this->pMin.y << "), max("
			<< this->pMax.x << " / " << this->pMax.y << ")\n Resolution: "
			<< this->resolution.x << " / " << this->resolution.y <<", Extents: "
			<< width << " / " << height << " [pixels]";

		CImageBuffer rawBuf;
		bool interpolate = ((fabs(minPoint.x)   > FLT_EPSILON) || (fabs(minPoint.y)   > FLT_EPSILON) ||
			                (fabs(fact.x - 1.0) > FLT_EPSILON) || (fabs(fact.y - 1.0) > FLT_EPSILON) || 
							(widthR != width) || (heightR != height));

		if (interpolate)
		{
			if (!rawBuf.set(minXR, minYR, widthR, heightR, 1, 16, true)) 
			{
				oss << "\nError allocating memory for buffer:" << widthR << " x " << heightR << " pixels\n";
				ret = false;
			}
			else oss << "\n Nearest neighbourhood interpolation\n Time: " << CSystemUtilities::timeString().GetChar();
		}
		else
		{
			if (!rawBuf.set(this->pExistingBuildingMask->getOffsetX(), this->pExistingBuildingMask->getOffsetY(), 
				            this->pExistingBuildingMask->getWidth(), this->pExistingBuildingMask->getHeight(), 1, 16, true)) 
			{
				oss << "\nError allocating memory for buffer:" << this->pExistingBuildingMask->getWidth()
					<< " x " << this->pExistingBuildingMask->getHeight() << " pixels\n";
				ret = false;
			}
			else oss << "\n Reading window from file \n Time: " << CSystemUtilities::timeString().GetChar();
		};

		if (ret)
		{
			if (!pExistingDatabaseFile->getRect(rawBuf)) 
			{
				oss << "\nError reading raw buffer: min(" << rawBuf.getOffsetX() 
					<< " / " << rawBuf.getOffsetY() 
					<< ", extents: " << rawBuf.getWidth() << " x " << rawBuf.getHeight() << " pixels\n";

				ret = false;
			}
		}

		int nBuild = 0;
		int nNotBuild = 0;
		if (ret && interpolate)
		{	
			float factP = 100.0 / float(height);

			C2DPoint P = minPoint;
			long deltaXY = rawBuf.getDiffX() + rawBuf.getDiffY();
			
			for (long r = 0; r < height; ++r, minPoint.y += fact.y)
			{
				long rOld = (long) floor(minPoint.y);
				unsigned long uMinNew = r    * this->pExistingBuildingMask->getDiffY();
				unsigned long uMaxNew = uMinNew + this->pExistingBuildingMask->getDiffY();

				if ((rOld >= 0) && (rOld < heightR))
				{
					unsigned long uMinOld = rOld * rawBuf.getDiffY();
					P.x = minPoint.x;
					P.y = minPoint.y;

					float y = (float) P.y - (float) rOld;

					for (unsigned long u = uMinNew; u < uMaxNew; u += this->pExistingBuildingMask->getDiffX(), P.x += fact.x)
					{
						long cOld = (long) floor(P.x);
						if ((cOld >= 0) && (cOld < widthR))
						{
							if (rawBuf.pixUShort(uMinOld + cOld) > 0) 
							{
								this->pExistingBuildingMask->pixUChar(u) = 1;
								++nBuild;
							}
							else 
							{
								this->pExistingBuildingMask->pixUChar(u) = 0;
								++nNotBuild;
							}

						}
						else
						{
							this->pExistingBuildingMask->pixUChar(u) = 255;		
						}
					}
				}
				else
				{
					for (unsigned long u = uMinNew; u < uMaxNew; u += this->pExistingBuildingMask->getDiffX())
					{
						this->pExistingBuildingMask->pixUChar(u) = 255;		
					}
				}
				if (this->listener) 
				{
					this->listener->setProgressValue(float(r) * factP);
				}
			}
			
		}
		else 
		{			
			for (unsigned long u = 0; u < rawBuf.getNGV(); u++)
			{
				if (rawBuf.pixUShort(u) > 0) 
				{
					this->pExistingBuildingMask->pixUChar(u) = 1;			
					++nBuild;
				}
				else 
				{
					this->pExistingBuildingMask->pixUChar(u) = 0;
					++nNotBuild;
				}
			}
		}
		this->existingBuildPercentage = nBuild + nNotBuild;
		if (this->existingBuildPercentage < 0.5f) this->existingBuildPercentage = 0.0;
		else this->existingBuildPercentage = float(nBuild) / this->existingBuildPercentage;
	}

	if (this->listener) 
	{
		this->listener->setProgressValue(100);
	}
	
	if (ret) oss << "\n existing building data base read successfully; Time: " 
		         << CSystemUtilities::timeString().GetChar();

	oss  << "\n"<< ends;

	protHandler.print(oss, protHandler.prt);

	return ret;
};

//=============================================================================
  
void CBuildingDetector::initWindows()
{
	if (this->pDTM)                  delete this->pDTM; 
	if (this->pDSM)                  delete this->pDSM; 
	if (this->pFirstLastDiff)        delete this->pFirstLastDiff; 
	if (this->pNormDSM)              delete this->pNormDSM;
	if (this->pVegetationIndex)      delete this->pVegetationIndex;
	if (this->pVegetationIndexSig)   delete this->pVegetationIndexSig;
	if (this->pDempsterShafer)       delete this->pDempsterShafer;
	if (this->roughness)             delete this->roughness;
	if (this->pBuildingMask)         delete this->pBuildingMask;
	if (this->pBuildingLabels)       delete this->pBuildingLabels;
	if (this->pHomogeneousLabels)    delete this->pHomogeneousLabels;
	if (this->pExistingBuildingMask) delete this->pExistingBuildingMask;

	this->pDTM                  = 0; 
	this->pDSM                  = 0; 
	this->pFirstLastDiff        = 0; 
	this->pNormDSM              = 0;
	this->pVegetationIndex      = 0;
	this->pVegetationIndexSig   = 0;
	this->pDempsterShafer       = 0;
	this->roughness             = 0;
	this->pBuildingMask         = 0;
	this->pBuildingLabels       = 0;
	this->pHomogeneousLabels    = 0;
	this->pExistingBuildingMask = 0;

	this->regionVector.clear();

	if (pDSMFirstFile || pDSMLastFile)
	{
		setUpTrafoAndExtents(this->resolution, this->pMin, this->pMax);

		bool OK = false;

		if (this->pDSMLastFile && !this->pDSMFirstFile)
		{
			this->pDSM = new CImageBuffer;
			if (this->listener) 
			{
				this->listener->setTitleString("Reading DSM ...");
				this->listener->setProgressValue(1);
			}
			OK = this->readDEMWindow(this->pDSMLastFile, *this->pDSM, "DSM");
		}
		else if (!this->pDSMLastFile && this->pDSMFirstFile)
		{
			this->pDSM = new CImageBuffer;
			if (this->listener) 
			{
				this->listener->setTitleString("Reading DSM ...");
				this->listener->setProgressValue(1);
			}

			OK = this->readDEMWindow(this->pDSMFirstFile, *this->pDSM, "DSM");
		}
		else if (this->pDSMFirstFile && this->pDSMLastFile)
		{
			this->pDSM = new CImageBuffer;		
			this->pFirstLastDiff = new CImageBuffer;
			if (this->listener) 
			{
				this->listener->setTitleString("Reading DSM(Last Pulse) ...");
				this->listener->setProgressValue(1);
			}

			OK  = this->readDEMWindow(this->pDSMLastFile,  *this->pDSM, "DSM (Last Pulse)");

			if (this->listener) 
			{
				this->listener->setTitleString("Reading DSM(First Pulse) ...");
				this->listener->setProgressValue(1);
			}

			OK &= this->readDEMWindow(this->pDSMFirstFile, *this->pFirstLastDiff, "DSM (First Pulse)");
			if (OK) 
			{
				for (unsigned long u = 0; u < this->pDSM->getNGV(); ++u)
				{
					float hLast  = this->pDSM->pixFlt(u);
					float hFirst = this->pFirstLastDiff->pixFlt(u);
					float delta = 0.0;

					if ((hLast  > DEM_NOHEIGHT) || (hFirst > DEM_NOHEIGHT))
					{
						if (hLast <= DEM_NOHEIGHT) this->pDSM->pixFlt(u) = hFirst;
						else if (hFirst <= DEM_NOHEIGHT) this->pFirstLastDiff->pixFlt(u) = hLast;
						else 
						{
							delta = hFirst - hLast;
							if (delta < 0.0) delta = 0.0;
						}
					}
					
					this->pFirstLastDiff->pixFlt(u) = delta;
				}
			}
		}

		if (OK)
		{
			CCharString fn = this->projectPath + "dsm_win.tif";
			this->exportDSM(fn, true);
			fn = this->projectPath + "FirstLast_win.tif";
			this->exportFirstLastDiff(fn, true);

			if (this->pNDVIFile)
			{
				if (this->listener) 
				{
					this->listener->setTitleString("Reading NDVI ...");
					this->listener->setProgressValue(1);
				}

				pVegetationIndex = new CImageBuffer;
				OK = this->readImageWindow(pNDVIFile, *pVegetationIndex, "NDVI");
				if (OK && this->pNDVISigmaFile)
				{
					if (this->listener) 
					{
						this->listener->setTitleString("Reading Sigma(NDVI) ...");
						this->listener->setProgressValue(1);
					}
					pVegetationIndexSig = new CImageBuffer;
					OK = this->readImageWindow(pNDVISigmaFile, *pVegetationIndexSig, "Sigma(NDVI)");

				}
			}
		}

		if (OK)
		{
			CCharString fn = this->projectPath + "NDVI_win.tif";
			this->exportNDVI(fn);
			fn = this->projectPath + "sigmaNDVI_win.tif";
			this->exportNDVISigma(fn);

			long offsetX = this->pDSM->getOffsetX();
			long offsetY = this->pDSM->getOffsetY();
			long width   = this->pDSM->getWidth();
			long height  = this->pDSM->getHeight();
			C2DPoint luc(this->pMin.x, this->pMax.y);

			this->pNormDSM        = new CImageBuffer;
			this->pBuildingMask   = new CImageBuffer;
			this->pBuildingLabels = new CLabelImage(offsetX, offsetY, width, height, 8);
			this->pHomogeneousLabels = new CLabelImage(offsetX, offsetY, width, height, 8);
			OK = ((this->pBuildingLabels->getWidth() == width) && (this->pBuildingLabels->getHeight() == height));
			OK &= this->pNormDSM->set(offsetX, offsetY, width, height, 1, 32, false, 0);
			OK &= this->pBuildingMask->set(offsetX, offsetY, width, height, 1, 1, true, 0);
			if (OK)
			{  
				this->roughness = new CDSMRoughness(offsetX, offsetY, width, height, luc, this->resolution);
				if (this->listener) this->listener->setProgressValue(100);
			}
		}

		if (OK)
		{
			if (this->pExistingDatabaseFile)
			{
				this->pExistingBuildingMask = new CImageBuffer;
				OK = this->pExistingBuildingMask->set(this->pDSM->getOffsetX(), 
					                                  this->pDSM->getOffsetY(), 
													  this->pDSM->getWidth(),
													  this->pDSM->getHeight(), 1, 8, true, 255);
				if (OK) 
				{
					if (this->listener) 
					{
						this->listener->setTitleString("Reading Existing Database ...");
						this->listener->setProgressValue(1);
					}

					OK = readExistingDatabase(); 
					if (OK)
					{
						CCharString fn = this->projectPath + "Existing_win.tif";
						this->pExistingBuildingMask->exportToTiffFile(fn.GetChar(), 0, true);
					}
					if (this->listener)  this->listener->setProgressValue(100);
				}
			}
		}

		if (OK)
		{
			this->pDTM = new CImageBuffer;
			if (this->pDTMFile)
			{
				if (this->listener) 
				{
					this->listener->setTitleString("Reading DTM ...");
					this->listener->setProgressValue(1);
				}

				OK = this->readDEMWindow(this->pDTMFile, *this->pDTM, "DTM");
				if (OK) this->initNormDSM(true);
				
				if (this->listener)  this->listener->setProgressValue(100);
			}
			else 
			{
				int structure;
				this->morphFilter(morphFilterSize, structure);
				ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
				oss.precision(3);

				oss << "\nGenerating coarse DTM by ";
				if (this->useRank) oss << "dual rank filtering";
				else oss << "morphologic filtering";

				oss << "\n==============================================\n\n Filter size: " 
					<< morphFilterSize << " [m] (" << structure << " [pixels])";
				if (this->useRank) oss << ", percentage: " << this->rankPercentage * 100.0 << "%";
				oss << "\n Time: " 
					<< CSystemUtilities::timeString().GetChar() << "\n";


				OK = this->pDTM->set(this->pDSM->getOffsetX(), this->pDSM->getOffsetY(), this->pDSM->getWidth(),this->pDSM->getHeight(), 1, 32, false);
				if (OK) 
				{
					if (this->listener) 
					{
						this->listener->setTitleString("Creating initial DTM ...");
						this->listener->setProgressValue(1);
					}

					OK = this->createDTMbyMorphology(structure, structure);

					if (this->listener) this->listener->setProgressValue(100);

					oss << " DTM generated; Time: " 
					<< CSystemUtilities::timeString().GetChar() << "\n";

				}
				else oss << " Error allocating buffer (" << this->pDSM->getWidth() << " x " << this->pDSM->getHeight() << " pixels)\n";

				oss << ends;

				protHandler.print(oss, protHandler.prt);
			}
		}

		if (OK)
		{
			CCharString fn = this->projectPath + "DTM_win.tif";
			this->exportDTM(fn, true);
			fn = this->projectPath + "DSM_norm_win.tif";
			this->exportNormDSM(fn, true);
		}

		if (!OK)
		{
			if (this->pDTM)                  delete this->pDTM;
			if (this->pDSM)                  delete this->pDSM;
			if (this->pFirstLastDiff)        delete this->pFirstLastDiff;
			if (this->pVegetationIndex)      delete this->pVegetationIndex;
			if (this->pVegetationIndexSig)   delete this->pVegetationIndexSig;
			if (this->pNormDSM)              delete this->pNormDSM;
			if (this->pBuildingMask)         delete this->pBuildingMask;
			if (this->pBuildingLabels)       delete this->pBuildingLabels;
			if (this->pHomogeneousLabels)    delete this->pHomogeneousLabels;
			if (this->roughness)             delete this->roughness;
			if (this->pExistingBuildingMask) delete this->pExistingBuildingMask;
		
			this->pDTM                  = 0;
			this->pDSM                  = 0;
			this->pFirstLastDiff        = 0;
			this->pVegetationIndex      = 0;
			this->pVegetationIndexSig   = 0;
			this->pNormDSM              = 0;
			this->pBuildingMask         = 0;
			this->pBuildingLabels       = 0;
			this->roughness             = 0;
			this->pHomogeneousLabels    = 0;
			this->pExistingBuildingMask = 0;
		}
	}
};

//=============================================================================

void CBuildingDetector::morphFilter(float filterSizeMetric, int &structure)
{  
	structure = (int) floor(filterSizeMetric / this->resolution.x);
	if (structure < 3) structure = 3;
	else if ((structure % 2) == 0)  structure += 1;
};
 
//=============================================================================

bool CBuildingDetector::makePersistent(CHugeImage *labelHug, CTFW &tfw, const CFileName &filename)
{
	bool ret = true;

	ofstream outFile(filename.GetChar());
	if (outFile.good())
	{
		outFile.setf(ios::fixed, ios::floatfield);
		outFile.precision(3);

		if (!makeLabelImagePersistent(labelHug, tfw)) ret = false;
		else
		{
			outFile << "BUILDINGS(" << regionVector.size() 
				    << ")LABELS("   << labelHug->getFileName()->GetChar() 
					<< ")DSM(";
			if (this->pDSMLastFile) outFile<< pDSMLastFile->getFileNameChar();
			else if (this->pDSMFirstFile) outFile<< pDSMFirstFile->getFileNameChar();

			outFile << ")DOMAIN("   << this->pMin.x << " " << this->pMin.y << " / " 
					<< this->pMax.x << " " << this->pMax.y << ")PIXSIZE("  
					<< this->resolution.x << " " << this->resolution.y << ")\n";

			for (unsigned long u = 1; u < regionVector.size(); ++u)
			{
				CCharString str = regionVector[u].getString();
				outFile<< str.GetChar() << endl;
			}
		}
	}
	else ret = false; 

	return ret;
};
	
//=============================================================================

void CBuildingDetector::setLabelFileName(const CFileName &filename)
{
	labelFileName = filename;
};

//=============================================================================

unsigned long CBuildingDetector::getNumberOfPixels() const
{
	if (!pDSM) return 0;

	long widht, height;
	this->getWindowSize(widht, height);
	
	return widht * height;
};

//=============================================================================

void CBuildingDetector::resetRoughness()
{
	if (roughness) delete roughness; 
	roughness = 0;
};

//============================================================================

void CBuildingDetector::removeSmallRegions(CLabelImage &labelimg, 
										   vector <CDSMBuilding> &regVec, 
                                           long minimumNumberOfPixels)
{
	int nRegions = 1;

	int maxRegions = (int) regVec.size() - 1;

	for (int i = 1; i < maxRegions; ++i)
	{
		if (labelimg.getNPixels(i) > (unsigned long) minimumNumberOfPixels)
		{
			long frstIdx = -1;
			for (long idx = 0; idx < labelimg.getNGV() && frstIdx < 0; ++idx)
			{
				if (labelimg.getLabel(idx) == i) frstIdx = idx;
			}
			long r, c, bnd;
			labelimg.getRowColBand(frstIdx, r, c, bnd);

			if (i != nRegions) labelimg.replaceLabel(i, nRegions, false);
			regVec[nRegions].initData(nRegions, labelimg.getNPixels(i),r,c);
			++nRegions;
		} 
		else if (labelimg.getNPixels(i)) labelimg.removeLabel (i, false);
	};

	regVec[0].resetData();

	regVec.resize(nRegions);
};

  
//============================================================================

void CBuildingDetector::removeLargeRegions(CLabelImage &labelimg, 
										   vector <CDSMBuilding> &regVec, 
										   long maximumNumberOfPixels)
{
	int nRegions = 1;

	int maxRegions = (int) regVec.size() - 1;

	for (int i = 1; i < maxRegions; ++i)
	{
		if (labelimg.getNPixels(i) < (unsigned long) maximumNumberOfPixels)
		{
			long frstIdx = -1;
			for (long idx = 0; idx < labelimg.getNGV() && frstIdx < 0; ++idx)
			{
				if (labelimg.getLabel(idx) == i) frstIdx = idx;
			}
			long r, c, bnd;
			labelimg.getRowColBand(frstIdx, r, c, bnd);

			if (i != nRegions) labelimg.replaceLabel(i, nRegions, false);
			regVec[nRegions].initData(nRegions, labelimg.getNPixels(i),r,c);
			++nRegions;

		} 
		else if (labelimg.getNPixels(i)) labelimg.removeLabel (i, false);
	};

	regVec[0].resetData();

	regVec.resize(nRegions);
};

//============================================================================
 
void CBuildingDetector::removeSmallElongated(int morphSize)
{
	if ((morphSize > 2) && (regionVector.size() > 1))
	{
		if ((morphSize % 2) == 0) --morphSize;
		
		updateBuildingMask();
	
		CImageBuffer inputMask(*pBuildingMask);
		CImageFilter morpho(eMorphological, morphSize, morphSize, 1);
		morpho.binaryMorphologicalOpen(inputMask, *pBuildingMask, 0);


		CLabelImage labelMask(pBuildingMask->getOffsetX(), pBuildingMask->getOffsetY(), 
			                  pBuildingMask->getWidth(),   pBuildingMask->getHeight(), 8);

		for (unsigned long u = 0; u < pBuildingMask->getNGV(); u++)
		{
			 if (pBuildingMask->pixUChar(u)) 
				 labelMask.pixUShort(u) = pBuildingLabels->getLabel(u);
			   else labelMask.pixUShort(u) = 0;
		};

		updateRegionParameters(labelMask, regionVector);

		unsigned long numRegions = 0;

		for (unsigned long u = 0; u < regionVector.size(); u++)
		{
			 if (regionVector[u].getFloorHeight() < FLT_MAX)
			 {
				  ++numRegions;
				  if (u != numRegions) 
				  {
					  pBuildingLabels->replaceLabel(u,numRegions, false);
					  regionVector[numRegions].copyData(numRegions, regionVector[u]);
				  };
			 }
			 else pBuildingLabels->removeLabel(u, false);
		};

		regionVector.resize(numRegions + 1);

		updateRegionParameters(*pBuildingLabels, regionVector);
		updateBuildingMask();
	};
};

//=============================================================================

void CBuildingDetector::setDTMmethodToMorpho() 
{ 
	this->useRank = false; 
};

//=============================================================================

void CBuildingDetector::setDTMmethodToRank(float rankPercent) 
{ 
	this->useRank = true; 
	this->rankPercentage = rankPercent; 
	if (this->rankPercentage > 1) this->rankPercentage *= float(0.01);
	else if (this->rankPercentage < 0) this->rankPercentage = 0.0;
};

//=============================================================================

void CBuildingDetector::morphoOrRank(const CImageBuffer &source, 
									 CImageBuffer &destination, 
									 const CImageFilter &filter)
{

	if (!this->useRank) filter.greyMorphologicalOpen(source, destination, DEM_NOHEIGHT);
	else
	{
		int rank = (int) floor(float(filter.getSizeX()) * this->rankPercentage); 
		if (rank > 100) rank = 100;
		else if (rank < 2) rank = 2;
		if (rank > filter.getSizeX()) rank = filter.getSizeX() - 1;
		filter.greyRankOpen(rank, source, destination, DEM_NOHEIGHT);
	}
};
	  
void CBuildingDetector::initNormDSM(bool DTMread)
{
	
	if (DTMread)
	{
		for (unsigned long u = 0; u < pDSM->getNGV(); ++u)
		{
			float dsmH = pDSM->pixFlt(u);
			float dtmH = pDTM->pixFlt(u);
			if ((dsmH <= DEM_NOHEIGHT) || (dtmH <= DEM_NOHEIGHT))
			{
				pDTM->pixFlt(u)     = DEM_NOHEIGHT;
				pDSM->pixFlt(u)     = DEM_NOHEIGHT;
				pNormDSM->pixFlt(u) = DEM_NOHEIGHT;
			}
			else 
			{
				float delta = dsmH - dtmH;
				if (delta < 0) pNormDSM->pixFlt(u) = 0;
				else pNormDSM->pixFlt(u) = delta;
			}
		};
	}
	else
	{
		for (unsigned long u = 0; u < pDSM->getNGV(); ++u)
		{
			if (pDSM->pixFlt(u) <= DEM_NOHEIGHT)
			{
				pDTM->pixFlt(u)     = DEM_NOHEIGHT;
				pNormDSM->pixFlt(u) = DEM_NOHEIGHT;
			}
			else pNormDSM->pixFlt(u) = pDSM->pixFlt(u) - pDTM->pixFlt(u);
		};
	};
};

bool CBuildingDetector::createDTMbyMorphology(int structureX, int structureY)
{
	if (!this->pDSM || !this->pDTM) return false;

	CImageFilter morpho(eMorphological, structureX, structureY, 1);

	morphoOrRank(*pDSM, *pDTM, morpho);

	initNormDSM(false);

	return true;
};

//=============================================================================

void CBuildingDetector::initHeightSegmentation(float morphForBuildings, int structureOpen, 
											   float vegetationIndexThreshold,
											   CLabelImage *labelimg,
											   int itCount)
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(2);

	oss << "\n   Initialising building mask:\n";
	CCharString label; 
	label.addInteger(itCount);

	float firstLastThreshold = 2.0f;

	int structure;
	this->morphFilter(morphForBuildings, structure);

	if (labelimg)
	{
		if (this->useRank) 
			oss << "      Rank filter of DSM: " << morphForBuildings <<" [m] (" << structure <<" pixels)" 
			    << "; percentage: " << this->rankPercentage << " %";
		else
			oss << "      Morphological opening of DSM: " << morphForBuildings <<" [m] (" << structure <<" pixels)";
		
		oss << "\n      Substituting heights for " << (labelimg->getNLabels() - 1) 
			<< " buildings from previous iteration\n";

		CImageBuffer dtm (*pDTM);
		CImageFilter morpho(eMorphological, structure, structure, 1);

		this->morphoOrRank(*pDSM, *pDTM, morpho);

		for (unsigned long u = 0; u < pDTM->getNGV(); ++u)
		{
			if (labelimg->getLabel(u) > 0) pDTM->pixFlt(u) = dtm.pixFlt(u);
			
			if (this->pDSM->pixFlt(u) <= DEM_NOHEIGHT)
			{
				pDTM->pixFlt(u) = dtm.pixFlt(u);
				pNormDSM->pixFlt(u) = DEM_NOHEIGHT;
			}
			else pNormDSM->pixFlt(u) = pDSM->pixFlt(u) - pDTM->pixFlt(u);

		};
	}
	else oss << "      Using original coarse DTM\n";
		
		
	oss << "      Height Threshold: " << heightThreshold << "\n";
	for (unsigned long u = 0; u < pDTM->getNGV(); u++)
	{
		if ((this->pDSM->pixFlt(u) <= DEM_NOHEIGHT) || (pNormDSM->pixFlt(u) < heightThreshold))
			pBuildingMask->pixUChar(u) = 0;
		else
			pBuildingMask->pixUChar(u) = 1;
	};

	if (hasVegIndex())
	{
		oss << "      NDVI Threshold: " << vegetationIndexThreshold << "\n";

		unsigned long nChanged = 0;

		for (unsigned long u = 0; u < pDTM->getNGV(); u++)
		{
			float VI = pVegetationIndex->pixFlt(u);

			if (pVegetationIndexSig)
			{
				float difference = VI - vegetationIndexThreshold;
				float diffThr = 3.0f * pVegetationIndexSig->pixFlt(u);

				if (difference < diffThr) VI = -101.0; 
			};

			if (VI > vegetationIndexThreshold) 
			{
				if (pBuildingMask->pixUChar(u))
				{
					++nChanged;
				    pBuildingMask->pixUChar(u) = 0;
				}
			};
		}; 
	};

	if (this->hasFirstLastDiff())
	{
		oss << "      First-Last Pulse Threshold: " << firstLastThreshold << "\n";

		unsigned long nChanged = 0;

		for (unsigned long u = 0; u < pDTM->getNGV(); u++)
		{
			if (pFirstLastDiff->pixFlt(u) > firstLastThreshold)
			{
				if (pBuildingMask->pixUChar(u))
				{
					++nChanged;
				    pBuildingMask->pixUChar(u) = 0;
				}
			}
		}; 
	};

	oss << "      Opening building mask with " << structureOpen << " Pixels";
	protHandler.print(oss, protHandler.prt);

	// Reset border pixels to 0:
	pBuildingMask->setMargin(1, 0);
	openBuildingMask(structureOpen);
};

//============================================================================

void CBuildingDetector::updateBuildingMask()
{
  for (unsigned long u = 0; u < pDTM->getNGV(); u++)
  {
	  if (this->pBuildingLabels->getLabel(u) > 0) this->pBuildingMask->pixUChar(u) = 1;
	  else this->pBuildingMask->pixUChar(u) = 0;
  };
};
//=============================================================================
   
void CBuildingDetector::openBuildingMask(int structure)
{
	if ((structure > 0) && (structure > 1))
	{
		CImageBuffer buf(*pBuildingMask);
		CImageFilter morpho(eMorphological, structure, structure, 1);
		morpho.binaryMorphologicalOpen(buf,*pBuildingMask, 0.0);
	};
};

//=============================================================================
   
void CBuildingDetector::closeBuildingMask(int structure)
{
 	if ((structure > 0) && (structure > 1))
	{
		CImageBuffer buf(*pBuildingMask);
		CImageFilter morpho(eMorphological, structure, structure, 1);
		morpho.binaryMorphologicalClose(buf,*pBuildingMask, 0.0);
	};
};

//=============================================================================
   
void CBuildingDetector::minimumBuildingMask(int structure)
{
	CImageBuffer buf(*pBuildingMask);
	CImageFilter maxFilter(eMinimumFilter, structure, structure, 1);
	maxFilter.binaryMinimumFilter(buf, *pBuildingMask, 0);
};

//=============================================================================
   
void CBuildingDetector::openCloseBuildingMask(int structure)
{
	CImageFilter morpho(eMorphological, structure, structure, 1);

	CImageBuffer openOutputMask(*pBuildingMask, false);
	morpho.binaryMorphologicalOpen(*pBuildingMask, openOutputMask, 0.0);

	CImageBuffer closeOutputMask(*pBuildingMask, false);
	morpho.binaryMorphologicalClose(openOutputMask, closeOutputMask, 0.0);

	for (long u = 0; u < this->pBuildingMask->getNGV(); ++u)
	{
		if (pBuildingMask->pixUChar(u)) 
			pBuildingMask->pixUChar(u) = closeOutputMask.pixUChar(u);
	};
};

//=============================================================================
   
void CBuildingDetector::closeLabelImage(int structure, bool checkheights)
{
	pBuildingLabels->updateVoronoi();
	
	pBuildingLabels->morphologicalClose(structure);
	
	if (checkheights)
	{
		for (int i = 0; i < this->pNormDSM->getNGV(); ++i)	
		{
			if (this->pNormDSM->pixFlt(i) < 1.0f) pBuildingLabels->pixUShort(i) = 0;
		}
		
		pBuildingLabels->morphologicalOpen(structure,0);

		for (int i = this->regionVector.size(); i < pBuildingLabels->getNLabels(); ++i)
		{
			pBuildingLabels->removeLabel(i, false);
		}
	};

	pBuildingLabels->renumberLabels();

	updateRegionParameters(*pBuildingLabels,regionVector);
	updateBuildingMask();
};
                      
//=============================================================================
    
void CBuildingDetector::iterateDTMmorph(float minArea, float pointThr, float homThr,  
										float ndviThr, int clsSize,  int itCount,   
										bool useNDVI, float eliminateSize)
{
	CCharString label; 
	label.addInteger(itCount);
	label = this->projectPath + CCharString("labels_") + label;
	
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(2);

	long minimumNumberOfPixels = (long) (minArea / (resolution.x * resolution.y));

	pBuildingLabels->initFromMask(*pBuildingMask, 1);

	long nRegions = pBuildingLabels->getNLabels();

	oss << "\n   Found " << (nRegions - 1) << " building regions"
		<< "\n   Eliminating Buildings smaller than " << minArea << " m^2 (" 
		<< minimumNumberOfPixels << " Pixels)";


	regionVector.resize(nRegions + 1);

	if (outputMode >= eMAXIMUM)
	{
		CFileName fn = label + ".tif";
		this->exportBuildingLabelImage(fn);
	}

	removeSmallRegions(*pBuildingLabels, regionVector,  minimumNumberOfPixels);

	oss << "\n   Number of remaining building regions: " << (this->regionVector.size() - 1);

	if (outputMode >= eMAXIMUM)
	{
		CFileName fn = label + "_min.tif";
		this->exportBuildingLabelImage(fn);
	}

	updateRegionParameters(*pBuildingLabels, regionVector);

	updateBuildingMask(); 

	oss << "\n   Eliminating small elongated regions; filter size: " << eliminateSize << " m^2 (" 
		<< int(floor(eliminateSize / (float) resolution.x)) << " Pixels)";

	removeSmallElongated(int(floor(eliminateSize / (float) resolution.x)));

	oss << "\n   Number of remaining building regions: " << (this->regionVector.size() - 1);

	if (outputMode >= eMAXIMUM)
	{
		CFileName fn = label + "_long.tif";
		this->exportBuildingLabelImage(fn);
	}

	oss << "\n   Eliminating vegetation regions; point percentage: " << pointThr * 100.0
		<< " %, homogeneous percentage: " << homThr * 100.0
		<< " %";

	if (useNDVI) oss << ", NDVI threshold: " << ndviThr << "%\n";

	removeVegetation(pointThr, homThr, ndviThr, useNDVI);
	updateBuildingMask();     

	oss << "\n   Number of remaining building regions: " << (this->regionVector.size() - 1);

	if (outputMode >= eMAXIMUM)
	{
		CFileName fn = label +  "_veg.tif";
		this->exportBuildingLabelImage(fn);
	}

	protHandler.print(oss, protHandler.prt);
};
	
void CBuildingDetector::checkHomogeneityLabels(bool eliminateLabels)
{

	if ((this->regionVector.size() > 1) && (this->pHomogeneousLabels->getNLabels() > 1))
	{
		int len = this->regionVector.size() * this->pHomogeneousLabels->getNLabels();
		unsigned int *occurrences = new unsigned int[len];
		memset(occurrences, 0, len * sizeof(unsigned int));

		for (unsigned int u = 0; u < this->pBuildingLabels->getNGV(); ++u)
		{
			unsigned short label1 = this->pBuildingLabels->pixUShort(u);
			unsigned short label2 = this->pHomogeneousLabels->pixUShort(u);
			if (label1 && label2)
			{
			
				unsigned long idx = label1 * this->pHomogeneousLabels->getNLabels() + label2;
				++occurrences[idx];
			}
		}

		int nRegions = 1;

		int maxRegions = (int) this->regionVector.size();

		for (int i = 1; i < maxRegions; ++i)
		{
			int maxLabel = 0;
			unsigned int maxOcc = 0;

			int minPix = (int) floor(float(this->regionVector[i].getNPixels()) * 0.5);
			int maxPix = this->regionVector[i].getNPixels() * 5;
			int occIdx0 = i * this->pHomogeneousLabels->getNLabels();
			int occIdxMax = occIdx0 + this->pHomogeneousLabels->getNLabels();
			for (int j = occIdx0 + 1; j < occIdxMax; ++j)
			{
				if (occurrences[j] > maxOcc)
				{
					maxOcc = occurrences[j];
					maxLabel = j - occIdx0;
				}
			}
			unsigned int maxPixOcc = this->pHomogeneousLabels->getNPixels(maxLabel);

			if ((maxOcc < minPix) || (maxPixOcc < maxPix))
			{
				if (i != nRegions)
				{
					this->pBuildingLabels->replaceLabel(i, nRegions, false);
					this->regionVector[nRegions].copyData(nRegions, this->regionVector[i]);
				}
				++nRegions;
			} 
			else if (this->pBuildingLabels->getNPixels(i)) this->pBuildingLabels->removeLabel (i, false);
		};

		this->regionVector[0].resetData();

		this->regionVector.resize(nRegions);

		delete [] occurrences;
	}

	if (eliminateLabels)
	{
		vector<float> Heights(this->pHomogeneousLabels->getNLabels());
		for (int i = 0; i < Heights.size(); ++i) Heights[i] = 0.0;
		vector<int> HeightsPixels(this->pHomogeneousLabels->getNLabels());
		for (int i = 0; i < HeightsPixels.size(); ++i) HeightsPixels[i] = 0;

		for (unsigned int u = 0; u < this->pBuildingLabels->getNGV(); ++u)
		{
			unsigned short label2 = this->pHomogeneousLabels->pixUShort(u);
			if (label2)
			{
				float Z = this->pNormDSM->pixFlt(u);
				if (Z  > DEM_NOHEIGHT)
				{
					HeightsPixels[label2]++;
					Heights[label2]+= Z;
				}
			}
		}

		for (int i = 1; i < Heights.size(); ++i)
		{	
			if (HeightsPixels[i]) Heights[i] /= float(HeightsPixels[i]);
		}

		if (outputMode > eMEDIUM) 
		{
			this->exportNormDSM(this->projectPath + "NORMBEFORE.tif", true);
			this->exportDTM(this->projectPath + "DTMBEFORE.tif", true);
			ofstream heightFile(this->projectPath + "heights.dat");
			heightFile.setf(ios::fixed, ios::floatfield);
			heightFile.precision(2);

			for (int i = 1; i < Heights.size(); ++i)
			{	
				heightFile << "Label: "; 
				heightFile.width(5);
				heightFile << i << ", height: "; 
				heightFile.width(6);
				heightFile << Heights[i] << ", size: ";
				heightFile.width(6);
				heightFile << HeightsPixels[i] << "\n";
			}
		}
		for (unsigned int u = 0; u < this->pBuildingLabels->getNGV(); ++u)
		{
			float Z = this->pNormDSM->pixFlt(u);
			if (Z  > DEM_NOHEIGHT)
			{		
				unsigned short label2 = this->pHomogeneousLabels->pixUShort(u);
				if (label2)
				{
					if (Heights[label2] < 1.0f)
					{
						this->pNormDSM->pixFlt(u) = 0.0;
					}
				}
				if (this->pNormDSM->pixFlt(u) < 1.0f) this->pDTM->pixFlt(u) = this->pDSM->pixFlt(u);
			}
		}

		if (outputMode > eMEDIUM) 
		{
			this->exportNormDSM(this->projectPath + "NORMAFTER.tif", true);
			this->exportDTM(this->projectPath + "DTMAFTER.tif", true);
		}
	}
};

void CBuildingDetector::initHomogeneousLabels(int filterSize)
{
	CImageBuffer *hlp = new CImageBuffer(*this->pBuildingMask, false);

	float deltaMax = this->getResolution().x;
	if (deltaMax < 1.0) deltaMax = 1.0;

	unsigned long maxRow = 2 * this->pNormDSM->getDiffY() - this->pNormDSM->getDiffX();
	unsigned long maxOverall = this->pNormDSM->getNGV() - this->pNormDSM->getDiffY();

	for (unsigned long iMin = this->pNormDSM->getDiffX() +  this->pNormDSM->getDiffY(); 
		 iMin < maxOverall; iMin += this->pNormDSM->getDiffY(), maxRow += this->pNormDSM->getDiffY())
	{
		for (unsigned long i = iMin; i< maxRow; ++ i)
		{
			float Z = this->pDSM->pixFlt(i);
			if (Z <= DEM_NOHEIGHT)
			{
				hlp->pixUChar(i) = 0;
			}
			else 
			{
				float ZXm1 = this->pDSM->pixFlt(i - this->pNormDSM->getDiffX());
				float ZXp1 = this->pDSM->pixFlt(i + this->pNormDSM->getDiffX());
				float ZYm1 = this->pDSM->pixFlt(i - this->pNormDSM->getDiffY());
				float ZYp1 = this->pDSM->pixFlt(i + this->pNormDSM->getDiffY());
				float fmax = 0;
				if ((ZXm1 > DEM_NOHEIGHT) && (ZXp1 > DEM_NOHEIGHT) && 
					(ZYm1 > DEM_NOHEIGHT) && (ZYp1 > DEM_NOHEIGHT))
				{
					float dZX = fabs(ZXp1 - ZXm1);
					float dZY = fabs(ZYp1 - ZYm1);
					if (dZX > dZY) fmax = dZX;
					else fmax = dZY;
				}

				if ((this->roughness->isHomogeneous(i)) && (fmax < deltaMax)) hlp->pixUChar(i) = 1;
				else hlp->pixUChar(i) = 0;		
			}
		}
	}

	hlp->setMargin(1,0);

	CImageBuffer *hlp1 = new CImageBuffer(*hlp, false);

	CImageFilter morpho1(eMorphological, filterSize, filterSize, 1);
	morpho1.binaryMorphologicalOpen(*hlp,  *hlp1, 1);
	delete hlp;
	if (outputMode >= eMAXIMUM)
	{
		CCharString c = this->projectPath + "homogBin.tif";
		hlp1->exportToTiffFile(c.GetChar(),0, true);
	}

	this->pHomogeneousLabels->initFromMask(*hlp1, 1);
	delete hlp1;
	if (outputMode >= eMEDIUM)
	{
		this->pHomogeneousLabels->dump(this->projectPath.GetChar(), "homog");
	}

	CLabelImage *p = this->pBuildingLabels;
	this->pBuildingLabels = this->pHomogeneousLabels;
	if (outputMode >= eMAXIMUM)
	{
		CCharString c = this->projectPath + "homogLabels.tif";
		exportBuildingLabelImage(c);
	}

	this->pBuildingLabels = p;
}

//=============================================================================

void CBuildingDetector::generateDTMmorph(int structure, 								
										 float minArea,   float minArea0, 
										 float pointThr,  float homThr, 
										 float pointThr0, float homThr0, 
										 float morphDTM,  int filter0, 
										 float ndviThr,   bool useNDVI)
{
	int iterationCount = 1;
/*
	CImageBuffer test(0, 0, this->pDTM->getWidth(), this->pDTM->getHeight(), 1, 8, true, 0.0);

	unsigned long uMax = this->pDTM->getNGV() - this->pDTM->getDiffY();
	for (unsigned long uMin = this->pDTM->getDiffY() + this->pDTM->getDiffX(); 
		 uMin < uMax; uMin += this->pDTM->getDiffY())
	{
		unsigned long uMaxRow = uMin + this->pDTM->getDiffY() - this->pDTM->getDiffX();
		for (unsigned long u = uMin; u < uMaxRow; u += this->pDTM->getDiffX())
		{
			float Z   = this->pDTM->pixFlt(u);
			float ZdX = this->pDTM->pixFlt(u - this->pDTM->getDiffX());
			float ZdY = this->pDTM->pixFlt(u - this->pDTM->getDiffY());
			if ((Z > DEM_NOHEIGHT) && (ZdX > DEM_NOHEIGHT) && (ZdY > DEM_NOHEIGHT))
			{
				float dx = fabs(ZdX - Z);
				float dy = fabs(ZdY - Z);
				float dmax = (dx > dy) ? dx : dy;
				if (dmax > 0.5f) test.pixUChar(u) = 1;
				else test.pixUChar(u) = 0;
			}
			else test.pixUChar(u) = 1;
		}
	}

	CCharString c = this->projectPath + "deltaDTM.tif";
	test.exportToTiffFile(c.GetChar(), 0, false);

	CImageBuffer test1(test);
	test1.setMargin(1,1);

	CImageFilter morpho(eMorphological, 3, 3, 1);
	morpho.binaryMorphologicalClose(test1, test, 1);
	
	for (int i = 0; i < test.getNGV(); ++i) test.pixUChar(i) = 1 - test.pixUChar(i);

	c = this->projectPath + "deltaDTMMorph.tif";
	test.exportToTiffFile(c.GetChar(), 0, false);

	CLabelImage labels(test, 1,8);
	c = this->projectPath;
	labels.dump(c.GetChar(), "deltaDTM");
*/
/*
	CImageBuffer test(*this->pDTM);
	int structuret;
	this->morphFilter(morphDTM, structuret);
	if (!this->pVegetationIndex) useNDVI = false;

	CImageFilter morpho(eMorphological, structuret, structuret, 1);
	morpho.greyMorphologicalOpen(*pDSM,  test, DEM_NOHEIGHT);

	for (unsigned long i = 0; i < this->pNormDSM->getNGV(); ++i)
	{
		float terrain1 = this->pDTM->pixFlt(i);		
		float terrain2 = test.pixFlt(i);

		if ((terrain1 <= DEM_NOHEIGHT) || (terrain2 <= DEM_NOHEIGHT))
		{
			test.pixFlt(i) = DEM_NOHEIGHT;
		}
		else
		{
			terrain2 -= terrain1;
			if (terrain2 < this->heightThreshold)
				terrain2 = 0;
			else if (useNDVI)
			{
				float VI = this->pVegetationIndex->pixFlt(i) - ndviThr;
				if (VI > 0)
				{
					if (this->pVegetationIndexSig)
					{
						if (VI < 2.0 * this->pVegetationIndexSig->pixFlt(i))
							terrain2 = 0;
					}
					else terrain2 = 0;
				}
			}
			test.pixFlt(i) = terrain2;
		}
	}
 
	CCharString c = this->projectPath + "diffMorph.tif";
	exportFloatBuf(c.GetChar(), test, true);
*/

	int hsize = (int) floor(morphDTM * 0.3 / this->resolution.x);
	
	initHomogeneousLabels(hsize);

	if ((!this->pDTMFile) && (morphFilterSize >= 100.0f) && (structure < 7))

	{
		this->initHeightSegmentation(morphFilterSize, filter0, ndviThr, 0, iterationCount) ;
	}
	else this->initHeightSegmentation(morphFilterSize, structure, ndviThr, 0, iterationCount) ;

//	for (int i = 0; i < this->pBuildingMask->getNGV(); ++i)
//	{
//		if (this->roughness->isLine(i)) this->pBuildingMask->pixUChar(i) = 0;
//	}

//	openBuildingMask(structure);

	CCharString label; 
	label.addInteger(iterationCount);
	label = this->projectPath + CCharString("labels_") + label;
	if (outputMode >= eMAXIMUM)
	{
		CCharString fn = this->projectPath + "mask_ini.tif";
		this->pBuildingMask->exportToTiffFile(fn.GetChar(), 0, true);
	}

	if ((!pDTMFile) && (morphFilterSize > morphDTM))
	{
		this->iterateDTMmorph(minArea0, pointThr0, homThr0, ndviThr, 5, iterationCount, useNDVI, morphFilterSize / 2);
		
		checkHomogeneityLabels(false);
		if (outputMode >= eMAXIMUM)
		{
			CCharString fn = label +  "_hom.tif";
			this->exportBuildingLabelImage(fn);
		}

		closeLabelImage(5, false);
	
		if (outputMode >= eMAXIMUM)
		{
			CCharString fn = label +  "_cl.tif";
			this->exportBuildingLabelImage(fn);
		};

		morphFilterSize /= 2;

		iterationCount++;

		label = ""; 
		label.addInteger(iterationCount);
		label = this->projectPath + CCharString("labels_") + label;

		if (morphFilterSize > morphDTM) 
		{
			filter0 /= 2;
			if (filter0 < 7)  filter0 = 7;
			else if (!(filter0  % 2)) ++filter0; 
			
			this->initHeightSegmentation(morphFilterSize, filter0, ndviThr, pBuildingLabels, iterationCount) ;
		};


		if (morphFilterSize > morphDTM)
		{
			minArea0  = minArea;
			pointThr0 = (pointThr0 + pointThr) / 2.0f;
			homThr0   = (homThr0 + homThr) / 2.0f;
			
			float elimLong = morphFilterSize / 2.0;
			if (elimLong > morphDTM) elimLong = morphDTM;

			this->iterateDTMmorph(minArea0, pointThr0, homThr0, ndviThr, 3, iterationCount, useNDVI, elimLong);

			checkHomogeneityLabels(false);

			if (outputMode >= eMAXIMUM)
			{
				CCharString fn = label +  "_hom.tif";
				this->exportBuildingLabelImage(fn);
			}

			closeLabelImage(3, false);

			if (outputMode >= eMAXIMUM)
			{
				CCharString fn = label +  "_cl.tif";
				this->exportBuildingLabelImage(fn);
			}

			iterationCount++;
		};

		this->initHeightSegmentation(morphDTM, structure, ndviThr, pBuildingLabels, iterationCount) ; 
	};

	float zDTMmin = FLT_MAX, zDTMmax = -FLT_MAX;
	float zDOMmin = FLT_MAX, zDOMmax = -FLT_MAX;

	for (unsigned long i = 0; i < this->pNormDSM->getNGV(); ++i)
	{
		float terrain = this->pDTM->pixFlt(i);		
		float surface = this->pDSM->pixFlt(i);

		if ((terrain <= DEM_NOHEIGHT) || (surface <= DEM_NOHEIGHT))
		{
			this->pNormDSM->pixFlt(i) = DEM_NOHEIGHT;
		}
		else
		{
			float normDSM = surface - terrain;
			if (normDSM < 0) normDSM = 0;
			if (terrain < zDTMmin) zDTMmin = terrain;
			if (terrain > zDTMmax) zDTMmax = terrain;
			if (normDSM < zDOMmin) zDOMmin = normDSM;
			if (normDSM > zDOMmax) zDOMmax = normDSM;

			this->pNormDSM->pixFlt(i) = normDSM;
		}
	}

	checkHomogeneityLabels(true);

	
	ostrstream oss; oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	oss << "\nDTM generation finished \n=======================\n\n Minimum / maximum terrain height: ";
	oss.width(8);
	oss << zDTMmin << " / ";
	oss.width(8);
	oss << zDTMmax << " [m]\n Minimum / maximum nDSM height:    ";
	oss.width(8);
	oss << zDOMmin << " / ";
	oss.width(8);
	oss << zDOMmax << " [m]\n Time: " 
		<< CSystemUtilities::timeString().GetChar() << "\n" << ends;

	protHandler.print(oss, protHandler.prt);
};

//============================================================================

void CBuildingDetector::classifyPolymorphic(int filterFoerstner, float quantil, float thresholdQ,   
											bool useFirstPulse)
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	oss << "\nClassification of surface roughness ";
	if (useFirstPulse && this->hasFirstLastDiff()) oss << " from first pulse data ";
	oss << "\n Filter size: " << filterFoerstner << " pixels\n Quantil for classification: " 
		<< quantil * 100 << "%\n Threshold for istropy: " << thresholdQ << "\n Time: "
		<< CSystemUtilities::timeString().GetChar() << "\n";

	if (useFirstPulse && this->hasFirstLastDiff())
	{
		CImageBuffer foerstDSM(*pFirstLastDiff);

		for (unsigned long u = 0; u < foerstDSM.getNGV(); ++u)
		{
			foerstDSM.pixFlt(u) += pDSM->pixFlt(u);
		};

		roughness->computeRandQandClassify(foerstDSM, quantil, thresholdQ, filterFoerstner, 0, 
											this->getWindowNW(), this->resolution, true);
	}
	else
	{
		roughness->computeRandQandClassify(*pDSM, quantil, thresholdQ, filterFoerstner, 0, 
										   this->getWindowNW(), this->resolution, true);
	};

	oss << "\n Roughness computed; Time: " << CSystemUtilities::timeString().GetChar() << "\n" << ends;

	protHandler.print(oss, protHandler.prt);
};

//=============================================================================
   
void CBuildingDetector::initBuildingMask()
{
	for (unsigned long u = 0; u < this->pBuildingLabels->getNGV(); u++)
	{
		if ((pNormDSM->pixFlt(u) > DEM_NOHEIGHT) && (pNormDSM->pixFlt(u) > heightThreshold))
			this->pBuildingMask->pixUChar(u) = 1;
		else 
			this->pBuildingMask->pixUChar(u) = 0;
	};

	// Reset border pixels to 0:
	pBuildingMask->setMargin(1, 0);  
};    

//============================================================================

void CBuildingDetector::removeBorderRegions(CLabelImage &labelimg)
{
	unsigned long uMax = labelimg.getDiffY() * 2;
	for (unsigned long u = labelimg.getDiffY(); u < uMax; u += labelimg.getDiffX())
	{
		unsigned short label = labelimg.getLabel(u);
		if (label > 0) labelimg.removeLabel(label, false);
	};

	uMax = (labelimg.getWidth() - 1) * labelimg.getDiffY();
	for (unsigned long u = uMax - labelimg.getDiffY(); u < uMax; u += labelimg.getDiffX())
	{
		unsigned short label = labelimg.getLabel(u);
		if (label > 0) labelimg.removeLabel(label, false);
	};

	uMax = labelimg.getWidth() * labelimg.getDiffY() - 2 * labelimg.getDiffX();
	for (unsigned long u = labelimg.getDiffY()- 2 * labelimg.getDiffX(); u < uMax; u += labelimg.getDiffY())
	{
		unsigned short label = labelimg.getLabel(u);
		if (label > 0) labelimg.removeLabel(label, false);
	};


	uMax = (labelimg.getWidth() - 1) * labelimg.getDiffY() + 2 * labelimg.getDiffX();
	for (unsigned long u = labelimg.getDiffX(); u < uMax; u += labelimg.getDiffY())
	{
		unsigned short label = labelimg.getLabel(u);
		if (label > 0) labelimg.removeLabel(label, false);
	};
};
//============================================================================

void CBuildingDetector::findRegions(float minArea, bool removeBorder)
{
	if (pBuildingMask && pBuildingLabels)
	{
		int minimumNumberOfPixels = (int) floor(minArea / (resolution.x * resolution.y));

		pBuildingLabels->initFromMask(*pBuildingMask, 1);
		long nRegions = pBuildingLabels->getNLabels();
	
		if (removeBorder) removeBorderRegions(*pBuildingLabels);

		regionVector.resize(nRegions + 1);

		removeSmallRegions(*pBuildingLabels, regionVector, minimumNumberOfPixels);

		updateRegionParameters(*pBuildingLabels, regionVector);

		computeFinalNormDsm();	
	}; 
};          

//============================================================================
 
void CBuildingDetector::removeVegetation(float pointThr, float homThr,   
										 float ndviThr,  bool evalNDVI)
{
	float ratioThr = 10.0f;

	if (regionVector.size() > 1)
	{
		unsigned long numRegions = 0;

		for (unsigned long u = 1; u < regionVector.size(); ++u)
		{
			if ((regionVector[u].getPointRatio()       < pointThr) &&
				(regionVector[u].getHomogeneousRatio() > homThr) &&		
				((!evalNDVI) || (regionVector[u].getAverageNDVI() < ndviThr) || 
				(regionVector[u].getHomogeneousRatio() > 2 * homThr)) &&
				(regionVector[u].getExtentRatio() < ratioThr))
			{
				++numRegions;
				if (u != numRegions) 
				{
					pBuildingLabels->replaceLabel(u, numRegions, false);
					regionVector[numRegions].copyData(numRegions, regionVector[u]);
				};
			}
			else
			{
				pBuildingLabels->removeLabel(u, false);
			};
		};

		regionVector.resize(numRegions + 1);
	};

	computeFinalNormDsm();
};

//============================================================================

void CBuildingDetector::computeFinalNormDTM()
{
}

//============================================================================

void CBuildingDetector::computeFinalNormDsm()
{
	for (unsigned long u = 0; u < pBuildingLabels->getNGV(); ++u)
	{
		unsigned short label = pBuildingLabels->getLabel(u);

		if (label > 0)
		{
			float difference =  pDSM->pixFlt(u) - regionVector[label].getFloorHeight(); 
			pNormDSM->pixFlt(u) = (difference > 0) ? difference : 0.0f;	
		}
		else pNormDSM->pixFlt(u) = 0.0f;
	};
};

//============================================================================

void CBuildingDetector::updateRegionParameters(CLabelImage &labelimg, 
											   vector <CDSMBuilding> &regVec)
{
	for (unsigned long j = 0; j < regVec.size(); ++j)
	{
		regVec[j].initDataEditing(); 
	};

	long rmin = labelimg.getOffsetY();
	long rmax = labelimg.getHeight() + rmin;

	long cmin = labelimg.getOffsetX();
	long cmax = labelimg.getWidth() + cmin;
	
	unsigned long minIndex = 0;
	
	if ((!this->hasVegIndex()) && (!this->hasFirstLastDiff()))
	{
		for(; rmin < rmax; ++rmin, minIndex += labelimg.getDiffY())
		{
			long c = cmin;
			for(unsigned long index = minIndex; c < cmax; index += labelimg.getDiffX(), ++c)
			{
				unsigned long label = labelimg.getLabel(index);
				if (label > 0)
				{
					float dtmHeight = pDTM->pixFlt(index);
					float dsmHeight = pDSM->pixFlt(index);
					regVec[label].editData(dtmHeight, dsmHeight, rmin, c, 
						                   roughness->isPoint(index),
										   roughness->isHomogeneous(index),
										   roughness->R(index), roughness->Q(index));
				};
			};
		};
	}
	else if (!this->hasVegIndex())
	{
		for(; rmin < rmax; ++rmin, minIndex += labelimg.getDiffY())
		{
			long c = cmin;
			for(unsigned long index = minIndex; c < cmax; index += labelimg.getDiffX(), ++c)
			{
				unsigned long label = labelimg.getLabel(index);
				if (label > 0)
				{
					float dtmHeight = pDTM->pixFlt(index);
					float dsmHeight = pDSM->pixFlt(index);
					regVec[label].editData(dtmHeight, dsmHeight, rmin, c, 
						                   roughness->isPoint(index),
										   roughness->isHomogeneous(index),
										   roughness->R(index), roughness->Q(index),
										   0.0, pFirstLastDiff->pixFlt(index), 101.0);
				};
			};
		};
	}
	else if (!this->hasFirstLastDiff())
	{
		for(; rmin < rmax; ++rmin, minIndex += labelimg.getDiffY())
		{
			long c = cmin;
			for(unsigned long index = minIndex; c < cmax; index += labelimg.getDiffX(), ++c)
			{
				unsigned long label = labelimg.getLabel(index);
				if (label > 0)
				{
					float dtmHeight = pDTM->pixFlt(index);
					float dsmHeight = pDSM->pixFlt(index);
					float sigVeg = (pVegetationIndexSig == 0) ? 0.0 : pVegetationIndexSig->pixFlt(index);

					regVec[label].editData(dtmHeight, dsmHeight, rmin, c, 
						                   roughness->isPoint(index),
										   roughness->isHomogeneous(index),
										   roughness->R(index), roughness->Q(index),
										   pVegetationIndex->pixFlt(index), 0.0f, sigVeg);	
				}; 
			};
		};
	}
	else
	{
		for(; rmin < rmax; ++rmin, minIndex += labelimg.getDiffY())
		{
			long c = cmin;
			for(unsigned long index = minIndex; c < cmax; index += labelimg.getDiffX(), ++c)
			{
				unsigned long label = labelimg.getLabel(index);
				if (label > 0)
				{
					float dtmHeight = pDTM->pixFlt(index);
					float dsmHeight = pDSM->pixFlt(index);
					float sigVeg = (pVegetationIndexSig == 0) ? 0.0 : pVegetationIndexSig->pixFlt(index);

					regVec[label].editData(dtmHeight, dsmHeight, rmin, c, 
						                   roughness->isPoint(index),
										   roughness->isHomogeneous(index),
										   roughness->R(index), roughness->Q(index),
										   pVegetationIndex->pixFlt(index), 
										   pFirstLastDiff->pixFlt(index), 	
										   sigVeg);
				}; 
			};
		};
	};

	long w = this->pBuildingLabels->getWidth();
	long h = this->pBuildingLabels->getHeight();
	regVec[0].finishDataEditing(5, getWindowNW(), resolution, w, h);

	for (unsigned long j = 1; j < regVec.size(); ++j)
	{
		regVec[j].finishDataEditing(5, getWindowNW(), resolution, w, h);
		regVec[j].computeRMS(*pNormDSM, labelimg);
	};
};

//============================================================================
 
void CBuildingDetector::splitOffAndRemoveTrees(int minimumFilter, int structureSize,                               
											   float minArea, bool removeBorder)
{
	updateBuildingMask();

	minimumBuildingMask(minimumFilter);
	openBuildingMask(structureSize);

	CLabelImage treeLabelImage(pBuildingLabels->getOffsetX(), pBuildingLabels->getOffsetY(), 
		                       pBuildingLabels->getWidth(),   pBuildingLabels->getHeight(), 8);

	treeLabelImage.initFromMask(*pBuildingMask, 1);

	long nRegions = treeLabelImage.getNLabels();

	removeBorderRegions(treeLabelImage);

	vector<CDSMBuilding> treeRegionVector(nRegions + 1);

	removeSmallRegions(treeLabelImage, treeRegionVector,4);

	updateRegionParameters(treeLabelImage, treeRegionVector);

	nRegions = 0;

	for (unsigned long u = 1; u < treeRegionVector.size(); ++u)
	{
		if (treeRegionVector[u].getPointRatio() < 0.5f)
			treeLabelImage.removeLabel(u, false);
	};

	for (unsigned long u = 0; u < this->pBuildingLabels->getNGV(); ++u)
	{
		if (treeLabelImage.getLabel(u) > 0) pBuildingMask->pixUChar(u) = 1;
		else pBuildingMask->pixUChar(u) = 0;
	};

	CImageBuffer inputMask(*pBuildingMask);
	CImageFilter maxFilter(eMaximumFilter, minimumFilter, minimumFilter, 1);
	maxFilter.binaryMaximumFilter(inputMask, *pBuildingMask, 0);

	treeLabelImage.initFromMask(*pBuildingMask, 1);

	nRegions = treeLabelImage.getNLabels();


	for (unsigned long u = 0; u < this->pBuildingLabels->getNGV(); ++u)
	{
		if (treeLabelImage.getLabel(u)  > 0) pBuildingLabels->pixUShort(u) = 0;
	};

	updateBuildingMask();

	openBuildingMask(structureSize);

	findRegions(minArea, removeBorder);
	updateBuildingMask();
}; 

//=============================================================================

void CBuildingDetector::dempsterShafer(int structure, int sensorOption, 
									   float minArea,  eDECISIONRULE rule, 									
									   const CCharString &suffix, 
									   const CFileName &thresholdFile,
									   float dSqQuantil, 
									   float sigmaMultiplicator,
									   bool classifyRegions,
									   int maxIterationsForPostClass,
									   bool  useThetaForFirstLast,
									   float probExisting,
									   bool useWaterModel)
{
	if (!pDempsterShafer) 
		pDempsterShafer = new CDempsterShaferBuildingClassifier(pNormDSM, roughness,
							                                    pFirstLastDiff, 
																pVegetationIndex, 
																pVegetationIndexSig,
																pExistingBuildingMask,
																dSqQuantil, sigmaMultiplicator,
																sensorOption, 
																useThetaForFirstLast,
																false,
																probExisting,
																this->existingBuildPercentage);

	if (!thresholdFile.IsEmpty()) 
		pDempsterShafer->initParametersFromFile(thresholdFile);

	pDempsterShafer->setProgressListener(this->listener);

	dempsterShaferPixels(rule, suffix, maxIterationsForPostClass);

	if (this->listener)
	{
		this->listener->setTitleString("Generating initial building regions");
		this->listener->setProgressValue(1);
	}

	openBuildingMask(structure);

	if (this->listener) this->listener->setProgressValue(5);

	int minimumNumberOfPixels = (int)floor((minArea / (resolution.x * resolution.y)));
	
	pBuildingLabels->initFromMask(*pBuildingMask, 1);

	if (this->listener) this->listener->setProgressValue(33);

	ostrstream oss;
	oss << "\nGeneration of initial building regions\n======================================\n\n   Time: " << CSystemUtilities::timeString().GetChar() << "\n";

	long nRegions = pBuildingLabels->getNLabels();

	regionVector.resize(nRegions + 1);

	oss << "\n   " << nRegions << " initial building regions detected; Time: " << CSystemUtilities::timeString().GetChar() << "\n";

	if (outputMode >= eMAXIMUM)
	{
		this->exportBuildingLabelImage(this->projectPath + "labels_pix.tif");
	}

	removeSmallRegions(*pBuildingLabels, regionVector, minimumNumberOfPixels);
	if (this->listener) this->listener->setProgressValue(66);

	oss << "\n   " << nRegions - regionVector.size() + 1 << " regions eliminated because they were smaller than " << minArea <<" m^2";

	nRegions = regionVector.size() - 1;

	if (outputMode >= eMAXIMUM)
	{
		this->exportBuildingLabelImage(this->projectPath + "labels_sml.tif");
	}

	updateRegionParameters(*pBuildingLabels, regionVector);

	checkHomogeneityLabels(false);

	oss << "\n   " << nRegions - regionVector.size() + 1 << " regions eliminated because they were not bordered by a height step";

	
	if (outputMode >= eMEDIUM)
	{
		this->exportBuildingLabelImage(this->projectPath + "labels_initial.tif");
	}

	nRegions = regionVector.size() - 1;

	oss << "\n   " << nRegions << " building regions were preliminarily accepted; Time: " << CSystemUtilities::timeString().GetChar() << "\n";
	
	protHandler.print(oss, protHandler.prt + protHandler.scr);

	if (this->listener) this->listener->setProgressValue(100);

	if (classifyRegions)
	{
		dempsterShaferRegions(rule, suffix, useWaterModel);

		if (outputMode >= eMAXIMUM) this->exportBuildingLabelImage(this->projectPath + "labels_ds.tif");

		this->pBuildingLabels->updateVoronoi();
		checkInnerCourtyards(2);

		if (outputMode >= eMAXIMUM) this->exportBuildingLabelImage(this->projectPath + "labels_cty.tif");

		updateBuildingMask();
	}
	else
	{	
		this->pBuildingLabels->updateVoronoi();
		checkInnerCourtyards(2);
		updateBuildingMask();
		pBuildingMask->setMargin(1, 0);
	};

};

//=============================================================================

void CBuildingDetector::dempsterShaferPixels(eDECISIONRULE rule, 
											 const CCharString &suffix,
											 int maxIterationsForPostClass)
{
	pDempsterShafer->classifyPixels(rule, maxIterationsForPostClass, this->projectPath, suffix);

	for (unsigned long u = 0; u < this->pBuildingMask->getNGV(); ++u)
	{
		if (pDempsterShafer->pixUChar(u) == 0) this->pBuildingMask->pixUChar(u) = 1;
		else  this->pBuildingMask->pixUChar(u) = 0;
	};

	pBuildingMask->setMargin(1, 0);
};
  
//=============================================================================
 
void CBuildingDetector::dempsterShaferRegions(eDECISIONRULE rule,
											  const CCharString &suffix,
											  bool useWaterModel)
{ 
	pDempsterShafer->classifyRegions(pBuildingLabels, regionVector, rule, useWaterModel, 
									 this->projectPath, suffix);

	unsigned long numRegions = 0;

	for (unsigned long u = 1; u < regionVector.size(); ++u)
	{
		if (regionVector[u].getIndex() > 0)
		{
			++numRegions;
			if (u != numRegions) 
			{
				pBuildingLabels->replaceLabel(u, numRegions, false);
				regionVector[numRegions].copyData(numRegions, regionVector[u]);
			};
		}
		else pBuildingLabels->removeLabel(u, false);
	};

	pBuildingLabels->renumberLabels();
	regionVector.resize(numRegions + 1);

	updateBuildingMask();
	pBuildingMask->setMargin(1, 0);
};
	
  
//=============================================================================
 
void CBuildingDetector::createVegetationLayer()
{  
	CImageBuffer vegetationMask(*pBuildingMask, false);

	for (unsigned long u = 0; u < pBuildingMask->getNGV(); u++)
	{
		if (pDempsterShafer->pixUChar(u) <= 1) vegetationMask.pixUChar(u) = 1;
		else vegetationMask.pixUChar(u) = 0;
	};

	CImageBuffer inputMask(vegetationMask);
	CImageFilter morpho(eMorphological, 3, 3, 1);
	morpho.binaryMorphologicalOpen(inputMask, vegetationMask, 0);

	for (unsigned long u = 0; u < pBuildingMask->getNGV(); u++)
	{
		if (pBuildingLabels->getLabel(u)) vegetationMask.pixUChar(u) = 0;
	};

	inputMask = vegetationMask;
	
	morpho.binaryMorphologicalClose(inputMask, vegetationMask, 0);

	vegetationMask.exportToTiffFile("vegetation.tif", 0, false);
};

//=============================================================================

void CBuildingDetector::generateDTMmorph(int filterFoerstner, float percentageTrees, 
										 float thresholdQ,   
										 bool useFirstPulse, int structure, 								
										 float minArea,   float minArea0, 
										 float pointThr,  float homThr, 
										 float pointThr0, float homThr0, 
										 float morphDTM,  
										 float ndviThr,   bool useNDVI)
{
	classifyPolymorphic(filterFoerstner, 1.0 - percentageTrees, thresholdQ, useFirstPulse);
	this->roughness->exportAll(this->projectPath);
	this->roughness->exportTiff(this->projectPath, "foerstner.tif");

	int filter0 = (int) floor(7.5f / resolution.x);
	if (filter0 < 5) filter0 = 5;
	else if (!(filter0 % 2)) filter0++;
	
	if (!pDTMFile)
	{
		generateDTMmorph(structure, minArea, minArea0, pointThr, homThr, 
		                 pointThr0, homThr0, morphDTM, filter0,  ndviThr, useNDVI);

		CCharString fn = this->projectPath + "DTM_morph.tif";
		this->exportDTM(fn, true);
		fn = this->projectPath + "DSM_normMorph.tif";
		this->exportNormDSM(fn, true);
	}
}

//=============================================================================

void CBuildingDetector::checkInnerCourtyards(int closeSize)
{
	CImageBuffer buf(*this->pBuildingMask, false);
	for (unsigned long i = 0; i < buf.getNGV(); ++i)
	{
		if (this->pBuildingLabels->getLabel(i) > 0) buf.pixUChar(i) = 0;
		else buf.pixUChar(i) = 1;
	}

	buf.setMargin(0, 0);

	if (closeSize > 1)
	{
		CImageBuffer buf1(buf, true);
		CImageFilter morpho(eMorphological, closeSize, closeSize, 1);
		morpho.binaryMorphologicalClose(buf1, buf, 1);
	}

	CLabelImage innerLabels(buf, 1, 8);
	if (outputMode > eMEDIUM) innerLabels.dump(this->projectPath.GetChar(), "inner0");

	int maxPix = 0.0;
	for (int i = 1; i < this->regionVector.size(); ++i)
	{
		if (this->regionVector[i].getNPixels() > maxPix) maxPix = this->regionVector[i].getNPixels();
	}

	for (int i = 1; i < innerLabels.getNLabels(); ++i)
	{
		if (innerLabels.getNPixels(i) > maxPix) innerLabels.removeLabel(i, false);
	}

	innerLabels.renumberLabels();
	if (outputMode > eMEDIUM) innerLabels.dump(this->projectPath.GetChar(), "innerMin");

	vector <double> HeightVector(innerLabels.getNLabels());

	for (int i = 0; i < HeightVector.size(); ++i) HeightVector[i] = 10000;

	for (unsigned long i = 0; i < buf.getNGV(); ++i)
	{
		unsigned short label = innerLabels.getLabel(i);
		if (label > 0) 
		{
			float z = this->pNormDSM->pixFlt(i);
//			HeightVector[label] += z;
			if (z < HeightVector[label]) HeightVector[label] = z;
		}
	}

//	for (int i = 1; i < HeightVector.size(); ++i) HeightVector[i] /= (double) innerLabels.getNPixels(i);

	for (int i = 1; i < HeightVector.size(); ++i) 
	{
		if (HeightVector[i] < this->heightThreshold) innerLabels.removeLabel(i, false);
	}

	innerLabels.renumberLabels();

	if (outputMode > eMEDIUM) innerLabels.dump(this->projectPath.GetChar(), "inner");

	this->pBuildingLabels->updateVoronoi();

	for (unsigned long i = 0; i < buf.getNGV(); ++i)
	{
		if (innerLabels.getLabel(i) > 0) 
		{
			this->pBuildingLabels->pixShort(i) = this->pBuildingLabels->getVoronoi(i);
		}
	}

	updateRegionParameters(*pBuildingLabels, regionVector);
};

//=============================================================================

void CBuildingDetector::detectBuildings(bool  firstPulseForRoughness, int filterFoerstner,  
										float percentageTree, float thresholdQ, 
										float thresholdArea,  int   filterSmallMorph,
										float thresholdPoint, float thresholdHom,   
										float thresholdNDVI,  bool  useNDVI,
										float thresholdArea0, float thresholdPoint0,   
										float thresholdHom0,  float DTMmorphFilter,
										int   sensorOption,  
										const CFileName &probabilityModelFile,
										float minRoughnessQuantil,
										float sigmaNDVIMultiplicator,
										eDECISIONRULE decisionRule,
										int   maxPostClassIterations,
										bool  classifyRegions, 
										int   closingFilter,
										int   minimumFilter,
										bool  createVegetationLayer,
										bool  useThetaForFirstLast,
										float probExisting,
										bool useWaterModel)
{
	if (this->listener) 
	{
		this->listener->setTitleString("Creating coarse DTM ...");
		this->listener->setProgressValue(1);
	}

	if (!pDTMFile)
		generateDTMmorph(filterFoerstner,  percentageTree, thresholdQ,      firstPulseForRoughness, 
			             filterSmallMorph, thresholdArea,  thresholdArea0, 
						 thresholdPoint,   thresholdHom,   thresholdPoint0, thresholdHom0, 
						 DTMmorphFilter,   thresholdNDVI,  useNDVI);
	else
	{
		classifyPolymorphic(filterFoerstner, 1.0 - percentageTree, thresholdQ, firstPulseForRoughness);
		this->roughness->exportAll(this->projectPath);
		this->roughness->exportTiff(this->projectPath, "foerstner.tif");
	}

	if (this->listener) 
	{
		this->listener->setTitleString("Dempster-Shafer classification ...");
		this->listener->setProgressValue(1);
	}

	this->dempsterShafer(filterSmallMorph, sensorOption, thresholdArea, decisionRule, "", 
		                 probabilityModelFile, minRoughnessQuantil, sigmaNDVIMultiplicator, 
						 classifyRegions, maxPostClassIterations, useThetaForFirstLast,
						 probExisting, useWaterModel);

	if (this->listener) this->listener->setProgressValue(100);
	

	if (closingFilter > 1) 
	{
		closeLabelImage(closingFilter, true);
	}

	if (minimumFilter > 0) 
	{
		splitOffAndRemoveTrees(minimumFilter, filterSmallMorph, thresholdArea, false);
		removeVegetation(thresholdPoint, thresholdHom, thresholdNDVI, useNDVI);
		updateBuildingMask();
	};

	computeFinalNormDsm();

	if (createVegetationLayer) this->createVegetationLayer();

	if (pDempsterShafer) delete pDempsterShafer;
	pDempsterShafer = 0;
};

//============================================================================

void CBuildingDetector::detectBuildings(bool  firstPulseForRoughness, int filterFoerstner,  
										float percentageTree, float thresholdQ, 
										float thresholdArea,  float thresholdPoint, 
										float thresholdHom,   float thresholdNDVI,
										bool  useNDVI, 
										int   minimumFilter, int filterSmallMorph)
{
	classifyPolymorphic(filterFoerstner, 1.0 - percentageTree, thresholdQ, firstPulseForRoughness);
	
	initBuildingMask();

	openBuildingMask(filterSmallMorph);

	findRegions(thresholdArea, true);

	removeVegetation(thresholdPoint, thresholdHom, thresholdNDVI, useNDVI);

	if (minimumFilter) 
		splitOffAndRemoveTrees(minimumFilter, filterSmallMorph, thresholdArea, true);
	else 
		updateBuildingMask();
};

//============================================================================

void CBuildingDetector::exportFloatBuf(const char *fn, const CImageBuffer &fltBuf, bool uselut)
{	
	CImageBuffer exportBuf(0,0, fltBuf.getWidth(), fltBuf.getHeight(), 1, 8, true);

	float maxVal = -FLT_MAX;
	float minVal =  FLT_MAX;

	for (unsigned long u = 0; u < fltBuf.getNGV(); ++u)
	{
		float val = fltBuf.pixFlt(u);
		if (val != DEM_NOHEIGHT)
		{
			if (val > maxVal) maxVal = val;
			if (val < minVal) minVal = val;
		}
	}

	float fact = maxVal - minVal;
	if (fact < FLT_EPSILON) fact = 1;
	else fact = 255.0 / fact;

	for (unsigned long u = 0; u < fltBuf.getNGV(); ++u)
	{
		float val = fltBuf.pixFlt(u);
		if (val == DEM_NOHEIGHT) exportBuf.pixUChar(u) = 0;
		else 
		{
			val = (val - minVal) * fact;
			exportBuf.pixUChar(u) = (unsigned char) val;
		}
	}

	CLookupTable lut(8,3);

	CLookupTable *pLut = 0;

	if (uselut) 
	{
		lut = CLookupTableHelper::getDSMLookup();
		pLut = &lut;
	}
		
	exportBuf.exportToTiffFile(fn, pLut, false);
};

//============================================================================

void CBuildingDetector::exportDSM(const CFileName &filename, bool uselut) const
{
	if (this->pDSM) exportFloatBuf(filename.GetChar(), *(this->pDSM), uselut);
};

//============================================================================

void CBuildingDetector::exportFirstLastDiff(const CFileName &filename, bool uselut) const
{
	if (this->pFirstLastDiff) 
		exportFloatBuf(filename.GetChar(), *(this->pFirstLastDiff), uselut);
};

//============================================================================

void CBuildingDetector::exportNDVI(const CFileName &filename, bool uselut) const
{
	if (this->pVegetationIndex)
		exportFloatBuf(filename.GetChar(), *(this->pVegetationIndex), uselut);
};

//============================================================================

void CBuildingDetector::exportNDVISigma(const CFileName &filename, bool uselut) const
{
	if (this->pVegetationIndexSig) 
		exportFloatBuf(filename.GetChar(), *(this->pVegetationIndexSig), uselut);
};

//============================================================================

void CBuildingDetector::exportDTM(const CFileName &filename, bool uselut) const
{
	if (this->pDTM) exportFloatBuf(filename.GetChar(), *(this->pDTM), uselut);
};

void CBuildingDetector::exportNormDSM(const CFileName &filename, bool uselut) const
{
	if (this->pNormDSM)
		exportFloatBuf(filename.GetChar(), *(this->pNormDSM), uselut);
};

bool CBuildingDetector::dumpNormDSM(const char *FileName, CDEM *dem) const
{
	bool ret = true;

	if (!this->pNormDSM) ret = false;
	else
	{
		ret = dem->dumpImage(FileName, *this->pNormDSM, 0);
		if (ret) dem->setTFW(this->trafo);
	}

	return ret;
};


bool CBuildingDetector::dumpDTM(const char *FileName, CDEM *dem) const
{
	bool ret = true;

	if (!this->pDTM) ret = false;
	else
	{
		ret = dem->dumpImage(FileName, *this->pDTM, 0);
		if (ret) dem->setTFW(this->trafo);
	}

	return ret;
};

//============================================================================

void CBuildingDetector::exportNormDerivatives(const CFileName &path) const
{
	if (roughness) roughness->exportTiff(path);
};

//============================================================================

void CBuildingDetector::exportBuildingMask(const CFileName &filename) const
{
	if (this->pBuildingMask) 
		this->pBuildingMask->exportToTiffFile(filename.GetChar(), 0, true);
};

//=============================================================================

void CBuildingDetector::exportBuildingLabelImage(CFileName fileName) const
{
	if (fileName.IsEmpty()) fileName = labelFileName;

	CLookupTable lut(8,3);

	for (int i = 0; i < lut.getMaxEntries(); ++i)
	{
		unsigned char r, g, b;
		CDSMBuilding::getColour(i, r,g,b);
		lut.setColor(r, g, b, i);
	};

	CImageBuffer exportWin(0,0,this->pBuildingLabels->getWidth(), this->pBuildingLabels->getHeight(), 1, 8, true);

	for (unsigned long i = 0; i < pBuildingLabels->getNGV(); ++i)
	{
		unsigned short label = pBuildingLabels->getLabel(i);
		if (label > 254) label = label % 254 + 1;

		exportWin.pixUChar(i) = (unsigned char) label;
	};

	exportWin.exportToTiffFile(fileName.GetChar(), &lut, false);
};

//=============================================================================

bool CBuildingDetector::makeLabelImagePersistent(CHugeImage *labelHug, CTFW &tfw) const
{
	bool OK = (pBuildingLabels != 0);
	if (OK)
	{
		CFileName fileName = *(labelHug->getFileName());
		OK = labelHug->writeLabelImage(fileName.GetChar(), *pBuildingLabels);
		if (OK)
		{
			tfw.copy(trafo);
		}
	}

	return OK;
};


//============================================================================

bool CBuildingDetector::getBuildings(float thinningThreshold, 
									 CBuildingArray &buildings)
{
	buildings.RemoveAll();

	if (this->listener) 
	{
		this->listener->setTitleString("Generating building polygons ...");
		this->listener->setProgressValue(1);
	}
	float fact = 100.0 / float(regionVector.size());

	for (unsigned long j = 1; j < regionVector.size(); ++j)
	{    
		regionVector[j].initMask(*this->pBuildingLabels);
		if (regionVector[j].createPrismaticModel(this->projectPath, thinningThreshold))
			buildings.Add(*regionVector[j].getBuilding());
		else 
		{
			int hmm = 0;
		}

		if (this->listener) this->listener->setProgressValue(float(j) * fact);
	};
	
	if (this->listener) this->listener->setProgressValue(100);

	buildings.setReferenceSystem(trafo.getRefSys());
	return true;
};

//============================================================================

void CBuildingDetector::exportBuildings(const CFileName &regionFile,                            
										const CFileName &winputFile,
										float thinningThreshold, unsigned long modelName,
										bool polygonFlag, bool appendFlag, bool deleteFlag,
										bool vrmlFlag)
{
	ofstream *pWrl = 0; 
	if (vrmlFlag) 
	{
		pWrl = new ofstream ("block.wrl");
		*pWrl << "#VRML V2.0 utf8\n"	
			  << "#\n"
			  << "# Cooperative Research Centre for Spatial Information\n"
			  << "# The University of Melbourne\n"
			  << "# website: www.crcsi.com.au\n"
			  << "#\n"
			  << "# Exported using an extended version of the\n"
			  << "# VRaniML(tm) VRML 2.0 C++ Class Library v0.9.04\n"
			  << "# Copyright (c) 1997-1999 by Great Hill Corporation\n"
			  << "# website: www.greathill.com\n"
			  << "# email:   info@greathill.com\n"
			  << "# phone:   (800) 949-3334\n"
			  << "#\n"
			  << "Background {\n"
			  << "skyAngle    [ 1.2, 1.5 ]\n"
			  << "  skyColor    [ 0.5 0.7  1, 0.5 0.7  1, 0.5 0.7  1 ]  groundAngle [1.57]\n"
			  << "groundColor [ 0.5 0.7  1, 0.5 0.7  1]\n"
			  << "}\n";
	};

	ofstream *pRegFile = 0;
	if (!regionFile.IsEmpty()) pRegFile = new ofstream(regionFile.GetChar());

	CFileName dxfFileName(winputFile);
	dxfFileName.SetFileExt(".dxf");
	ofstream dxfFile(dxfFileName.GetChar());
	CXYZPolyLine::writeDXFHeader(dxfFile);

	ofstream *pWinput = 0;

	if (polygonFlag)
	{
		if (appendFlag) pWinput = new ofstream(winputFile.GetChar(),ios::app);
		else pWinput = new ofstream(winputFile.GetChar());
		*pWinput << "  99999991     0.000     0.000     0.000\n"
				 << "  " << modelName << "     0.000     0.000     0.000" << endl;
	};

	unsigned long lineCode = 1;
	
	for (unsigned long j = 1; j < regionVector.size(); ++j)
	{    
		if (pRegFile) regionVector[j].print(*pRegFile);
		regionVector[j].exportBoundaryPolys(thinningThreshold, pWinput, &dxfFile, lineCode, vrmlFlag, this->projectPath);
		if (deleteFlag) regionVector[j].deleteRegionDir(this->projectPath);

		if (vrmlFlag) 
		   *pWrl << "Inline { url \"REGION_" << j << "\\blockModel.wrl\" }" << endl;
	};

	CXYZPolyLine::writeDXFTrailer(dxfFile);

	if (pWinput)
	{
		*pWinput    << "  99999999     0.000     0.000     0.000\n";
		delete pWinput;
	};

	if (pRegFile) delete pRegFile;
	if (vrmlFlag) delete pWrl;
};
#endif // WIN32
