#ifndef __CORTHOBatchProgram__
#define __CORTHOBatchProgram__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "BatchProgram.h"

#include "ORTHOBatchParameters.h"

class CORTHOBatchProgram : public CBatchProgram
{
public:
	CORTHOBatchProgram(CFileName parametersFile);
	virtual ~CORTHOBatchProgram();


protected:
	virtual bool runProgram();
	virtual CXMLDataHandler*  getXMLDataHandler();

private:

	bool computeOrhtoPhotos(CORTHOBatchParameters& parameters) const;

private:
	CORTHOBatchParameters parameters;

};



#endif //__CORTHOBatchProgram__