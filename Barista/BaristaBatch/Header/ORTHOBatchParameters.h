#ifndef __CORTHOBatchParameters__
#define __CORTHOBatchParameters__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include <vector>
#include "ORTHOProject.h"

class CORTHOBatchParameters
{
public:
	CORTHOBatchParameters() : bicubic(false), useAnchorPoints(true), maxDistAnchors(10){};
	~CORTHOBatchParameters(){};

	bool getUseAnchorPoints() const { return this->useAnchorPoints;};
	void setUseAnchorPoints(bool b) { this->useAnchorPoints = b;};

	bool getUseBicubicInterpolation() const { return this->bicubic;};
	void setUseBicubicInterpolation(bool b) { this->bicubic = b;};

	int getMaxDistAnchorPoints() const { return this->maxDistAnchors;};
	void setMaxDistAnchorPoints(int  maxDist) { this->maxDistAnchors = maxDist;};

	unsigned int getNumberOfProjects() const { return this->projectFiles.size();};
	void addProject(const CORTHOProject& projectFile);
	CORTHOProject& getProject(unsigned int index) { return this->projectFiles[index];};

private:
	vector<CORTHOProject> projectFiles;

	bool useAnchorPoints;
	int maxDistAnchors;
	bool bicubic;

};






#endif // __CORTHOBatchParameters__