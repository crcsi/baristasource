#ifndef __CAVNIRSTRIP__
#define __CAVNIRSTRIP__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "AVNIRScene.h"




class CAVNIRStrip
{
public:
	CAVNIRStrip() : mergeOrbits(false) {};

	unsigned int getNumberOFScenes() const { return this->scenes.size();};

	void addScene(const CAVNIRScene& scene) { this->scenes.push_back(scene);};
	
	CAVNIRScene& getScene(unsigned int index) { return this->scenes[index];};

	void setMergeOrbit(bool bMerge) { this->mergeOrbits = bMerge;};

	bool processStrip();


private:
	
	bool mergeOrbit();
	
	vector<CAVNIRScene> scenes;
	bool mergeOrbits;

};



#endif __CAVNIRSTRIP__