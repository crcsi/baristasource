#ifndef __CGABATHPROGRAM__
#define __CGABATHPROGRAM__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "BatchProgram.h"


#include "GABatchParameters.h"

class CGABatchProgram : public CBatchProgram
{
public:

	CGABatchProgram(CFileName parametersFile);
	virtual ~CGABatchProgram();

protected:
	virtual bool runProgram();
	virtual CXMLDataHandler*  getXMLDataHandler();

private:

	bool initProject(CGABatchParameters& parameters) const;
	bool processPRISM(CGABatchParameters& parameters) const;
	bool processAVNIR2(CGABatchParameters& parameters) const;
	bool loadObjectPoints(CGABatchParameters& parameters) const;
	bool loadDEMs(CGABatchParameters& parameters) const;
	bool loadOrthoImages(CGABatchParameters& parameters) const;

private:
	CGABatchParameters parameters;
};



#endif //__CGABATHPROGRAM__