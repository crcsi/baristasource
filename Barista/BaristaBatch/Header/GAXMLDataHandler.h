#ifndef __GAXMLDATAHANDLER__
#define __GAXMLDATAHANDLER__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "XMLDataHandler.h"
#include "GABatchParameters.h"

class CGAXMLDataHandler : public CXMLDataHandler
{
public:
	CGAXMLDataHandler(CGABatchParameters& parameters);
	~CGAXMLDataHandler(void);

    virtual void endElement(const XMLCh* const name);
	virtual void startElement(const XMLCh* const name, AttributeList& attributes);


private:
 
	CGABatchParameters& data;
	unsigned int BARISTA_BATCH_VERSION;

	bool readProjectSetup;
	bool	readMatchingParameters;
	bool		readPatchSize;
	bool		readIterationSteps;
	bool		readMinCorrelation;
	bool		readMinHeight;
	bool		readMaxHeight;
	bool		readSigma8Bit;
	bool		readSigma16Bit;
	bool		readSigmaGeo;
	bool		readXYZPointName;
	bool	readDefaultRefSys;
	bool		readTMParameters;
	bool	readAdjustmentSettings;
	bool readPRISMStrips;
	bool	readPRISMStrip;
	bool		readPRISMScene;
	bool			readMergeScene;
	bool readAVNIRStrips;
	bool	readAVNIRStrip;
	bool		readAVNIRScene;
	bool readDEMFiles;
	bool	readDEMFile;
	bool readOrthoImages;
	bool	readOrthoImage;
	bool readObjectPoints;
	bool	readObjectPointArray;


	CPRISMStrip stripPRISM;
	CPRISMScene scenePRISM;
	CAVNIRStrip stripAVNIR;
	CAVNIRScene sceneAVNIR;

	CGeoImage geoImage;
	CObjectPoints objectPointArray;
	eCoordinateType type;

	CCharString projectName;
	CCharString templateName;
	CCharString srcDir;
	CCharString destDir;
	CCharString tfwFile;
	CCharString mergeName;
	CCharString mergeCamera;

	double meanGroundHeight;
	
	double doubleValue;
	double doubleValue1;
	int intValue;
	CCharString string;
	
	bool mergeOrbit;
	bool deleteHugFiles;
	bool initWallis;

	// match parameters
	CMatchingParameters cm;

	CReferenceSystem defaultRefSys;
	double centralMeridian;			
	double scale;						
	double latitudeOrigin;			
	double FEasting;
	double FNorthing;

	//dem parameters
	double demOffset;
	double demScale;
	double demNoDataValue;
	double demSigma_Z;
	bool demOffsetFlag;
	bool demScaleFlag;
	bool demNoDataValueFlag;
	bool demSigma_ZFlag;
};
#endif //__GAXMLDATAHANDLER__