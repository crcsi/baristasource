#ifndef __CORTHOXMLDATAHANDLER__
#define __CORTHOXMLDATAHANDLER__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "XMLDataHandler.h"
#include "ORTHOBatchParameters.h"


class CORTHOXMLDataHandler : public CXMLDataHandler
{
public:
	CORTHOXMLDataHandler(CORTHOBatchParameters& parameters);
	~CORTHOXMLDataHandler(void);

    virtual void endElement(const XMLCh* const name);
	virtual void startElement(const XMLCh* const name, AttributeList& attributes);


private:
 
	void parseAttributeListOrthoSettings(const AttributeList& attributes,bool& bicubic, bool& anchor,int& maxDistAnchor,double& gsd) const;
	void parseAttributeListProjectSettings(const AttributeList& attributes, bool& saveProject,bool& deleteSrc, bool& deleteOrthoHUG) const;

	
	CORTHOBatchParameters& data;
	unsigned int ORTHOGENERATION_BATCH_VERSION;

	bool readOrthoProject;

	bool useBicubicInterpolationGlobal;
	bool useAnchorPointsGloabl;
	int  maxDistAnchorPointsGlobal;
	double  gsdGlobal;

	bool saveProjectGlobal;
	bool deleteSrcGlobal;
	bool deleteOrthoHugGlobal;

	CCharString projectName;

	CORTHOProject orthoProject;

};
#endif //__CORTHOXMLDATAHANDLER__