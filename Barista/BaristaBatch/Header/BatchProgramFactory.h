#ifndef __BATCHPROGRAMFACTORY__
#define __BATCHPROGRAMFACTORY__

#include "BatchProgram.h"

class CBatchProgramFactroy
{
public:
	static CBatchProgram* createBatchProgram(int argc, char* argv[]);
	static void printHelpText();
	
	
private:
	CBatchProgramFactroy(){};
	~CBatchProgramFactroy(){};
	static const char *Usage[];
};


#endif // __BATCHPROGRAMFACTORY__

