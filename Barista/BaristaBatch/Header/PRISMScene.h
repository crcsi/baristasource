#ifndef __CPRISMSCENE__
#define __CPRISMSCENE__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */
#include <vector>
using namespace std;

#include "Filename.h"
#include "ImageImporter.h"


class CPRISMScene
{
public:
	CPRISMScene() : mergeScene(false),deleteHugeFilesOfSubImages(false),initWallisFilter(false)	{};
	
	~CPRISMScene();

	void setDirectory(CFileName directory,CFileName hugeFileDirectory = "");
	
	void setInitWallisFilter(bool b) { this->initWallisFilter = b;};

	void setMergeScene(bool merge,bool deleteHugFilesOfSubImages,CFileName mergeName ="",CCharString cameraName="");

	bool processScene();


	unsigned int getNumberOfImportedImages() const { return this->importData.size();};

	CFileName getNameOfImportedImage(unsigned int index)const { return importData[index].filename; };

	CVImage* getFirstImageOfScene();

private:

	bool loadScene();

	bool mergeSubImages();

	bool deleteHugeFiles();

	CFileName directory;
	CFileName hugefileDirectory;
	bool mergeScene;
	bool initWallisFilter;
	CFileName mergeName;
	CCharString cameraName;
	bool deleteHugeFilesOfSubImages;
	
	vector<CImageImportData> importData;

};




#endif __CPRISMSCENE__