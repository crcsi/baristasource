#ifndef __COBJECTPOINTS__
#define __COBJECTPOINTS__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Filename.h"
#include "ReferenceSystem.h"


class CObjectPoints
{
public:
	CObjectPoints() : useDefaultRefSys(false),isControl(false),isCheckPoint(false),
					coordType(eUndefinedCoordinate){};

	void setFileName(CFileName name,eCoordinateType type) {this->filename = name;
													 this->coordType =type; };
	CFileName  getFileName() const { return this->filename;};

	void setIsControl() { this->isControl = true;};
	bool getIsControl() const { return this->isControl;};

	void setIsCheckPoint() { this->isCheckPoint = true;};
	bool getIsCheckPoint() const { return this->isCheckPoint;};

	void setUseDefaultRefSys() { this->useDefaultRefSys = true;};
	bool getUseDefaultRefSys() const { return this->useDefaultRefSys;};
	eCoordinateType getCoordinateType() const { return this->coordType;};

	bool loadObjectPoints(const CReferenceSystem& defaultRefSys);

	CCharString getErrorMessage() const { return this->errorMessage;};
private:
	CFileName filename;
	
	bool isControl;
	bool isCheckPoint;

	bool useDefaultRefSys;
	eCoordinateType coordType;

	CCharString errorMessage;
};



#endif __COBJECTPOINTS__