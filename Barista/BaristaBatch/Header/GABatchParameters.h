#ifndef __CGABATHPARAMETERS__
#define __CGABATHPARAMETERS__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "MatchingParameters.h"
#include "PRISMStrip.h"
#include "AVNIRStrip.h"
#include "ObjectPoints.h"
#include "GeoImage.h"

class CGABatchParameters 
{
public:
	CGABatchParameters();
	~CGABatchParameters(){};

	// project names
	void setProjectName(CFileName name);
	CFileName getProjectName() const { return this->projectName;}

	void setTemplateProjectName(CFileName name);
	CFileName getTemplateProjectName() const { return this->templateProjectName;}


	// arrays 
	unsigned int getNumberOfPRISMStrips() const { return this->stripsPRISM.size();};
	void addPRISMStrip(const CPRISMStrip& strip);
	CPRISMStrip& getPRISMStrip(unsigned int index) { return this->stripsPRISM[index];};


	unsigned int getNumberOfAVNIRStrips() const { return this->stripsAVNIR.size();};
	void addAVNIRStrip(const CAVNIRStrip& strip);
	CAVNIRStrip& getAVNIRStrip(unsigned int index) { return this->stripsAVNIR[index];};


	unsigned int getNumberOfObjectPoints() const { return this->objectPoints.size();};
	void addObjectPoints(const CObjectPoints& points);
	CObjectPoints& getObjectPoints(unsigned int index) { return this->objectPoints[index];};

	unsigned int getNumberOfDEMFiles() const { return this->demFiles.size();};
	void addDEMFile(const CGeoImage& image);
	CGeoImage& getDEMFile(unsigned int index) { return this->demFiles[index];};

	unsigned int getNumberOfOrthoImageFiles() const { return this->orthoImages.size();};
	void addOrthoImageFile(const CGeoImage& image);
	CGeoImage& getOrthoImageFile(unsigned int index) { return this->orthoImages[index];};


	// single data
	void setMeanGroundHeight(double height) { this->meanGroundHeight = height;
											 this->hasMeanGroundHeight = true;};
	double getMeanGroundHeight()const { return this->meanGroundHeight;};
	bool getHasMeanGroundHeight() const { return this->hasMeanGroundHeight;};


	void setMatchingParameters(const CMatchingParameters& mp) { this->matchingParameters = mp;
																this->hasMatchingParameters = true;};
	CMatchingParameters getMatchingParameters()const { return this->matchingParameters;};
	bool getHasMatchingParameters() const { return this->hasMatchingParameters;};


	void setDefaultReferenceSystem(const CReferenceSystem& rs) { this->refSys = rs;
																this->hasDefaultRefSys = true;};
	CReferenceSystem getDefaultReferenceSystem()const { return this->refSys;};
	bool getHasDefaultReferenceSystem() const { return this->hasDefaultRefSys;};
	
private:

	CFileName projectName;

	CFileName templateProjectName;

	vector<CPRISMStrip> stripsPRISM;

	vector<CAVNIRStrip> stripsAVNIR;

	vector<CGeoImage> demFiles;

	vector<CGeoImage> orthoImages;

	vector<CObjectPoints> objectPoints;	

	bool hasMeanGroundHeight;
	double meanGroundHeight;

	bool hasMatchingParameters;
	CMatchingParameters matchingParameters;	

	bool hasDefaultRefSys;
	CReferenceSystem refSys;

};






#endif //__CGABATHPARAMETERS__