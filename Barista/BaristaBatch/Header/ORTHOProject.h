#ifndef __CORTHOProject__
#define __CORTHOProject__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Filename.h"

#define DEFAULT_SAVE_PROJECT true
#define DEFAULT_DELETE_SRC_HUGEFILES false
#define DEFAULT_DELETE_ORTHO_HUGEFILES false


class CORTHOProject
{
public:
	CORTHOProject();
	CORTHOProject(const CORTHOProject& src);
	~CORTHOProject();


	bool processProject();


	bool getUseAnchorPoints() const { return this->useAnchorPoints;};
	void setUseAnchorPoints(bool b) { this->useAnchorPoints = b;};

	bool getUseBicubicInterpolation() const { return this->bicubic;};
	void setUseBicubicInterpolation(bool b) { this->bicubic = b;};

	int getMaxDistAnchorPoints() const { return this->maxDistAnchors;};
	void setMaxDistAnchorPoints(int  maxDist) { this->maxDistAnchors = maxDist;};

	
	CFileName getSrcProjectFileName() const { return this->srcProjectFileName;};
	void setSrcProjectFileName(const CFileName& srcName)  { this->srcProjectFileName = srcName;};

	CFileName getDestProjectFileName();
	void setDestProjectFileName(const CFileName& destName)  { this->destProjectFileName = destName;};

	double getGSD()const {return this->gsd ;};
	void setGSD(double newGsd)  { this->gsd = newGsd;};

	bool getSaveProject() const { return this->saveProject;};
	void setSaveProject(bool b) { this->saveProject = b;};

	bool getDeleteSrcHugeFiles() const { return this->deleteSrcHugeFiles;};
	void setDeleteSrcHugeFiles(bool b) { this->deleteSrcHugeFiles = b;};

	bool getDeleteOrthoHugeFiles() const { return this->deleteOrthoHugeFiles;};
	void setDeleteOrthoHugeFiles(bool b) { this->deleteOrthoHugeFiles = b;};


private:

	CFileName srcProjectFileName;
	CFileName destProjectFileName;

	bool useAnchorPoints;
	int maxDistAnchors;
	bool bicubic;

	double gsd;

	bool saveProject;
	bool deleteSrcHugeFiles;
	bool deleteOrthoHugeFiles;

};



#endif //__CORTHOProject__