#ifndef __CAVNIRScene__
#define __CAVNIRScene__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include <vector>
using namespace std;

#include "Filename.h"
#include "ImageImporter.h"

class CAVNIRScene
{
public:
	CAVNIRScene() : mergeBands(false),deleteHugeFilesOfSubImages(false),initWallisFilter(false)	{};
	~CAVNIRScene();

	void setDirectory(CFileName directory,CFileName hugeFileDirectory = "");
	
	void setInitWallisFilter(bool b) { this->initWallisFilter = b;};

	void setMergeBands(bool merge,bool deleteHugFilesOfSubImages,CFileName mergeName ="",CCharString cameraName="");

	bool processScene();

	unsigned int getNumberOfImportedImages() const { return this->importData.size();};

	CFileName getNameOfImportedImage(unsigned int index)const { return importData[index].filename; };

	CVImage* getFirstImageOfScene();
	
private:

	bool loadScene();

	bool mergeImageBands();

	bool deleteHugeFiles();

	CFileName directory;
	CFileName hugefileDirectory;
	bool mergeBands;
	bool initWallisFilter;
	CFileName mergeName;
	CCharString cameraName;
	bool deleteHugeFilesOfSubImages;

	vector<CImageImportData> importData;

};




#endif __CAVNIRScene__