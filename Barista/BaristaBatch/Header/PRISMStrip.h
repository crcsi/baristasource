#ifndef __CPRISMSTRIP__
#define __CPRISMSTRIP__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "PRISMScene.h"

class CPRISMStrip
{
public:
	CPRISMStrip() : mergeOrbits(false) {};

	unsigned int getNumberOFScenes() const { return this->scenes.size();};

	void addScene(const CPRISMScene& scene) { this->scenes.push_back(scene);};
	
	CPRISMScene& getScene(unsigned int index) { return this->scenes[index];};

	void setMergeOrbit(bool bMerge) { this->mergeOrbits = bMerge;};

	bool processStrip();

private:
	
	bool mergeOrbit();
	
	vector<CPRISMScene> scenes;
	bool mergeOrbits;

};



#endif __CPRISMSTRIP__