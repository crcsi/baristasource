#ifndef __BATCHPROGRAM__
#define __BATCHPROGRAM__


/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "Filename.h"
#include <vector>

class CXMLDataHandler;
class CXMLErrorHandler;



class CBatchProgram
{
public:

	virtual ~CBatchProgram(){};

	// starts the batch program, this should be the only publicly available function for the user 
	bool startBatchProgram(CFileName exePath);

protected:
	
	// the parseFile() function parses the file stored in the data member parametersFile. 
	// The derived class needs to provide the XMLDataHandler to store the read information.	
	bool parseFile();
	
	// The derived classes implement this function. 
	// It's called after the file has been successfully parsed.
	virtual bool runProgram() = 0;
	
	// The derived classes provide a XMLDataHandler which takes care of parsing and 
	// storing the needed information. 
	virtual CXMLDataHandler*  getXMLDataHandler() = 0;

	// The derived classes can implement also an XMLErrorHandler to customise the error handling. 
	// By default warnings and errors are stored in the class members 'messages' and 'errors' 
	// and written to the protocol.
	virtual CXMLErrorHandler* getXMLErrorHandler();

	
	bool writeProject(const CFileName& projectFileName) const;

	CBatchProgram(CFileName _parametersFile);
	
protected:
	// xml file for batch processing
	CFileName parametersFile;
	
	// dtd file to analize xml structure of the file
	CFileName dtdFile;

	// save warnings and errors from the parser
	vector<CCharString> messages;
	vector<CCharString> errors;

};


#endif // __BATCHPROGRAM__
