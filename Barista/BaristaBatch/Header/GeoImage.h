#ifndef __CGEOIMAGE__
#define __CGEOIMAGE__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Filename.h"
#include "TFW.h"


class CGeoImage
{
public:
	CGeoImage(): isDEM(false),useDefaultRefSys(false), bSetSigma_Z(false), bSetScale(false), bSetOffset(false), bSetNoDataValue(false) {};

	void setFileName(CFileName name,CFileName tfwFile,bool isDEM,CFileName hugFileDir="");
	CFileName  getFileName() const { return this->filename;};
	CFileName getTFWFile() const { return this->tfwFileName;};

	void setUseDefaultRefSys() { this->useDefaultRefSys = true;};
	bool getUseDefaultRefSys() const { return this->useDefaultRefSys;};

	eCoordinateType getCoordinateType() const { return this->coordType;};
	void setCoordinateType(eCoordinateType coordType) { this->coordType = coordType;};

	bool loadImage(const CReferenceSystem& defaultRefSys);

	void setInitWallisFilter(bool b) { this->initWallisFilter = b;};

	void setSigmaZ(double sigz) {this->Sigma_Z = sigz; this->bSetSigma_Z = true;};
	bool isSetSigmaZ() const {return this->bSetSigma_Z;};
	double getSigmaZ() const {return this->Sigma_Z;};
	
	void setScale(double scl) {this->Scale = scl; this->bSetScale = true;};
	bool isSetScale() const {return this->bSetScale;};
	double getScale() const {return this->Scale;};

	void setOffset(double off) {this->Offset = off; this->bSetOffset = true;};
	bool isSetOffset() const {return this->bSetOffset;};
	double getOffset() const {return this->Offset;};

	void setNoDataValue(double nodv) {this->NoDataValue = nodv; this->bSetNoDataValue = true;};
	bool isSetNoDataValue() const {return this->bSetNoDataValue;};
	double getNoDataValue() const {return this->NoDataValue;};

private:
	CFileName filename;
	CFileName hugeFileDir;
	CFileName tfwFileName;
	bool isDEM;
	bool initWallisFilter;
	bool useDefaultRefSys;
	eCoordinateType coordType;
	
	bool bSetSigma_Z;
	double Sigma_Z;
	
	bool bSetScale;
	double Scale;

	bool bSetOffset;
	double Offset;

	bool bSetNoDataValue;
	double NoDataValue;
};



#endif __CGEOIMAGE__