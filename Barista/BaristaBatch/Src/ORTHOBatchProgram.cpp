#include "ORTHOBatchProgram.h"


#include "BaristaProject.h"
#include "ProtocolHandler.h"
#include "ORTHOXMLDataHandler.h"


CORTHOBatchProgram::CORTHOBatchProgram(CFileName parametersFile) :
	CBatchProgram(parametersFile)
{
}

CORTHOBatchProgram::~CORTHOBatchProgram()
{

}

CXMLDataHandler*  CORTHOBatchProgram::getXMLDataHandler()
{
	return new CORTHOXMLDataHandler(this->parameters);
}

bool CORTHOBatchProgram::runProgram()
{
	
	bool ret = this->computeOrhtoPhotos(this->parameters);

	return ret;
}


bool CORTHOBatchProgram::computeOrhtoPhotos(CORTHOBatchParameters& parameters) const
{
	bool ret = true;

	ostrstream oss;

	oss << "processing ortho photo projects\n" 
		<< "===============================\n\n"
		<< "project files found: " << parameters.getNumberOfProjects() << "\n";
	
	protHandler.print(oss,CProtocolHandler::prt);


	for (unsigned int pro = 0; pro < parameters.getNumberOfProjects(); pro++)
	{
		ostrstream oss1;
		oss1 << "processing project: " << pro + 1 << "\n";
		oss1 << "-------------------\n";
		protHandler.print(oss1,CProtocolHandler::prt);


		CORTHOProject& orthoProject = parameters.getProject(pro);
		orthoProject.processProject();

	}


	return ret;
}