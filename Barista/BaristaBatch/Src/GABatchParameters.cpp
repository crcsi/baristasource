#include "GABatchParameters.h"

CGABatchParameters::CGABatchParameters() : 
	meanGroundHeight(0.0), hasMeanGroundHeight(false),
	hasMatchingParameters(false),hasDefaultRefSys(false)
{
}


void CGABatchParameters::addPRISMStrip(const CPRISMStrip& strip)
{
	this->stripsPRISM.push_back(strip);
}

void CGABatchParameters::addAVNIRStrip(const CAVNIRStrip& strip)
{
	this->stripsAVNIR.push_back(strip);
}

void CGABatchParameters::addObjectPoints(const CObjectPoints& points)
{
	this->objectPoints.push_back(points);
}



void CGABatchParameters::addDEMFile(const CGeoImage& image)
{
	this->demFiles.push_back(image);
}

void CGABatchParameters::addOrthoImageFile(const CGeoImage& image)
{
	this->orthoImages.push_back(image);
}


void CGABatchParameters::setProjectName(CFileName name)
{ 
	if (!name.IsEmpty())
	{
		if (!name.GetFileExt().CompareNoCase("bar"))
			name += ".bar";
	}
	
	this->projectName = name;
}

void CGABatchParameters::setTemplateProjectName(CFileName name) 
{ 
	if (!name.IsEmpty())
	{
		if (!name.GetFileExt().CompareNoCase("bar"))
			name += ".bar";
	}

	this->templateProjectName = name;
}
