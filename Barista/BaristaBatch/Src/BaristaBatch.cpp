#include "BaristaBatch.h"

#include "BatchProgramFactory.h"
#include "Filename.h"

int main(int argc, char* argv[])
{

	//addition>>
	//char fl[100];
	//fl = "I:\06901\Prism.xml";
	//argv[2] = "I:/06901/Prism.xml";
	//argv[2,2] = '\';
	//argv[2,8] = '\';
	//<<addition

	CBatchProgram* program = CBatchProgramFactroy::createBatchProgram(argc,argv);

	if (program)
	{
		CFileName exeFile(argv[0]);
		CCharString exePath = exeFile.GetPath();
		program->startBatchProgram(exePath);

		delete program;
		program =  NULL;
	}
	else
	{
		// print help information
		CBatchProgramFactroy::printHelpText();
	}

	return 0;
}

