#include "AVNIRStrip.h"

#include "BaristaProject.h"
#include "ProtocolHandler.h"


bool CAVNIRStrip::processStrip()
{
	ostrstream oss;
	oss << "number of scenes in strip: " << this->getNumberOFScenes() << "\n";
	protHandler.print(oss,CProtocolHandler::prt);
	
	bool ret = true;
	for (unsigned int scene = 0; scene < this->getNumberOFScenes(); scene++)
	{
		ostrstream oss1;
		oss1 << "processing scene: " << scene + 1 << "\n";
		oss1 << "--------------------";
		protHandler.print(oss1,CProtocolHandler::prt);
		
		CAVNIRScene& s = this->getScene(scene);
		ret = s.processScene();
	}

	this->mergeOrbit();

	return ret;
}


bool CAVNIRStrip::mergeOrbit()
{
	bool ret = true;

	if (this->mergeOrbits)
	{
		ostrstream oss;
		
		oss << "merging orbits of strip\n";
		oss << "-----------------------\n";
		vector<CPushBroomScanner*> scanner;

		for (unsigned int scene=0; scene < this->getNumberOFScenes(); scene++)
		{
			CAVNIRScene& s = this->getScene(scene);
			CVImage* firstImageOfScene = NULL;
			// all images of a scene will share orbit path and attitudes already, so we only check the first one
			if (firstImageOfScene = s.getFirstImageOfScene())
			{
				CPushBroomScanner* nextScanner = firstImageOfScene->getPushbroom(); 

				bool newScanner = true;

				// check if we got the path already
				for (unsigned int i=0; newScanner && i < scanner.size(); i++)
				{
					if (nextScanner && nextScanner->getOrbitPath() == scanner[i]->getOrbitPath() )
						newScanner = false;
				}

				// check if we got the attitudes already
				for (unsigned int i=0; newScanner && i < scanner.size(); i++)
				{
					if (nextScanner && nextScanner->getOrbitAttitudes() == scanner[i]->getOrbitAttitudes() )
						newScanner = false;
				}

				// add if e don't have the scanner already
				if (newScanner)
				{
					oss << "";				
					scanner.push_back(nextScanner);
				}

			}
		}

		oss << "orbits found: " << scanner.size() << "\n";

		if (scanner.size() > 1)
		{
			for (unsigned int i=0; i < scanner.size(); i++)
				oss << "orbit: " << scanner[i]->getOrbitPath()->getFileNameChar() << "\n"; ;
			
			ret = project.mergeOrbits(scanner);
			
			if (ret)
				oss << "successfully merged\n";

		}
		
		protHandler.print(oss,CProtocolHandler::prt);
	}


	return ret;
}

