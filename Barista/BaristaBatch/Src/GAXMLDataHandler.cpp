#include "GAXMLDataHandler.h"


#include <xercesc/sax/AttributeList.hpp>
#include "IniFile.h"
#include "SystemUtilities.h"


CGAXMLDataHandler::CGAXMLDataHandler(CGABatchParameters& parameters) : 
	data(parameters),BARISTA_BATCH_VERSION(0),mergeOrbit(false),deleteHugFiles(false),
	initWallis(false)
{

	this->readProjectSetup = false;
		this->readMatchingParameters = false;
			this->readPatchSize = false;
			this->readIterationSteps = false;
			this->readMinCorrelation = false;
			this->readMinHeight = false;
			this->readMaxHeight = false;
			this->readSigma8Bit = false;
			this->readSigma16Bit = false;
			this->readSigmaGeo = false;
			this->readXYZPointName = false;
		this->readDefaultRefSys = false;
			this->readTMParameters = false;
		this->readAdjustmentSettings = false;
	this->readPRISMStrips = false;
		this->readPRISMStrip = false;
			this->readPRISMScene = false;
				this->readMergeScene = false;
	this->readAVNIRStrips = false;
		this->readAVNIRStrip = false;
			this->readAVNIRScene = false;

	this->readDEMFiles = false;
		this->readDEMFile = false;

	this->readOrthoImages = false;
		this->readOrthoImage = false;

	this->readObjectPoints = false;
		this->readObjectPointArray = false;

		this->demNoDataValueFlag = false;
		this->demOffsetFlag = false;
		this->demScaleFlag = false;
		this->demSigma_ZFlag = false;

}

CGAXMLDataHandler::~CGAXMLDataHandler(void)
{
}


void CGAXMLDataHandler::endElement(const XMLCh* const name )
{
	this->tagDepth--;


	switch (this->tagDepth)
	{
	case 1:	{
				if (this->readProjectSetup)
				{
					this->projectName = CSystemUtilities::AbsPath(this->projectName);
					this->data.setProjectName(this->projectName);
					
					if (!this->templateName.IsEmpty())
						this->templateName = CSystemUtilities::AbsPath(this->templateName);
					this->data.setTemplateProjectName(this->templateName);
					
					this->data.setMeanGroundHeight(this->meanGroundHeight);


				}
				this->readProjectSetup = false;
				this->readPRISMStrips = false;
				this->readAVNIRStrips = false;
				this->readDEMFiles = false;
				this->readOrthoImages = false;
				this->readObjectPoints = false;
				break;
			}

	case 2: {
				if (this->readPRISMStrip)
				{
					this->stripPRISM.setMergeOrbit(this->mergeOrbit);
					this->data.addPRISMStrip(this->stripPRISM);

					// reset the object
					this->stripPRISM = CPRISMStrip();
				}
				else if (this->readAVNIRStrip)
				{
					this->stripAVNIR.setMergeOrbit(this->mergeOrbit);
					this->data.addAVNIRStrip(this->stripAVNIR);

					// reset the object
					this->stripAVNIR = CAVNIRStrip();
				}
				else if (this->readDEMFile)
				{
					if (!this->srcDir.IsEmpty())
						this->srcDir = CSystemUtilities::AbsPath(this->srcDir);

					if (!this->destDir.IsEmpty())
						this->destDir = CSystemUtilities::AbsPath(this->destDir);

					if (!this->tfwFile.IsEmpty())
						this->tfwFile = CSystemUtilities::AbsPath(this->tfwFile);
					
					if (this->demSigma_ZFlag)
					{
						this->geoImage.setSigmaZ(this->demSigma_Z);
					}
					if (this->demOffsetFlag)
					{
						this->geoImage.setOffset(this->demOffset);
					}
					if (this->demScaleFlag)
					{
						this->geoImage.setScale(this->demScale);
					}
					if (this->demNoDataValueFlag)
					{
						this->geoImage.setNoDataValue(this->demNoDataValue);
					}

					this->geoImage.setFileName(this->srcDir,this->tfwFile,true,this->destDir);
					this->data.addDEMFile(this->geoImage);
					
					// reset objects
					this->srcDir = "";
					this->destDir = "";
					this->tfwFile = "";					
					this->demSigma_ZFlag = false;
					this->demNoDataValueFlag = false;
					this->demScaleFlag = false;
					this->demOffsetFlag = false;
					this->geoImage = CGeoImage();

				}
				else if (this->readOrthoImage)
				{
					if (!this->srcDir.IsEmpty())
						this->srcDir = CSystemUtilities::AbsPath(this->srcDir);

					if (!this->destDir.IsEmpty())
						this->destDir = CSystemUtilities::AbsPath(this->destDir);

					if (!this->tfwFile.IsEmpty())
						this->tfwFile = CSystemUtilities::AbsPath(this->tfwFile);
					
					
					this->geoImage.setFileName(this->srcDir,this->tfwFile,false,this->destDir);
					this->geoImage.setInitWallisFilter(this->initWallis);
					this->data.addOrthoImageFile(this->geoImage);

					// reset objects
					this->srcDir = "";
					this->destDir = "";
					this->tfwFile = "";
					this->initWallis=false;
					this->geoImage = CGeoImage();
				}
				else if (this->readObjectPointArray)
				{
					if (!this->srcDir.IsEmpty())
						this->srcDir = CSystemUtilities::AbsPath(this->srcDir);
					
					this->objectPointArray.setFileName(this->srcDir,this->type);
					this->data.addObjectPoints(this->objectPointArray);

					// reset object
					this->srcDir = "";
					this->objectPointArray = CObjectPoints();

				}
				else if (this->readMatchingParameters)
				{
					this->data.setMatchingParameters(this->cm);
				}
				else if (this->readDefaultRefSys)
				{
					this->data.setDefaultReferenceSystem(this->defaultRefSys);
				}
				else if (this->readAdjustmentSettings)
				{
					bool useForward;
					bool excludeTie;
					double minRobustFac;
					double maxRobustPercentage;
					inifile.getAdjustmentSettings(useForward,excludeTie,maxRobustPercentage,minRobustFac);
					maxRobustPercentage = this->doubleValue;
					minRobustFac = this->doubleValue1;
					inifile.setAdjustmentSettings(useForward,excludeTie,maxRobustPercentage,minRobustFac);
				}


				this->readMatchingParameters = false;
				this->readDefaultRefSys = false;
				this->readAdjustmentSettings = false;
				this->readPRISMStrip = false;
				this->readAVNIRStrip = false;
				this->readDEMFile = false;
				this->readOrthoImage = false;
				this->readObjectPointArray = false;
				break;
			}

	case 3: {
				if (this->readPRISMScene)
				{
					if (!this->srcDir.IsEmpty())
						this->srcDir = CSystemUtilities::AbsPath(this->srcDir);

					if (!this->destDir.IsEmpty())
						this->destDir = CSystemUtilities::AbsPath(this->destDir);
					
					this->scenePRISM.setDirectory(this->srcDir,this->destDir);
					this->scenePRISM.setInitWallisFilter(this->initWallis);
					this->stripPRISM.addScene(this->scenePRISM);

					// reset objects
					this->srcDir = "";
					this->destDir = "";
					this->initWallis = false;
					this->scenePRISM = CPRISMScene();
				}
				else if (this->readAVNIRScene)
				{
					if (!this->srcDir.IsEmpty())
						this->srcDir = CSystemUtilities::AbsPath(this->srcDir);

					if (!this->destDir.IsEmpty())
						this->destDir = CSystemUtilities::AbsPath(this->destDir);
					
					
					this->sceneAVNIR.setDirectory(this->srcDir,this->destDir);
					this->sceneAVNIR.setInitWallisFilter(this->initWallis);
					this->stripAVNIR.addScene(this->sceneAVNIR);

					// reset objects
					this->srcDir = "";
					this->destDir = "";
					this->initWallis = false;
					this->sceneAVNIR = CAVNIRScene();

				}
				else if (this->readPatchSize)
				{
					this->cm.setPatchSize(this->intValue);
				}
				else if (this->readIterationSteps)
				{
					this->cm.setIterationSteps(this->intValue);
				}
				else if (this->readMinCorrelation)
				{
					this->cm.setRhoMin(this->doubleValue);
				}
				else if (this->readMinHeight)
				{
					this->cm.setMinZ(this->doubleValue);
				}
				else if (this->readMaxHeight)
				{
					this->cm.setMaxZ(this->doubleValue);
				}
				else if (this->readSigma8Bit)
				{
					this->cm.setSigmaGrey8Bit(this->doubleValue);
				}
				else if (this->readSigma16Bit)
				{
					this->cm.setSigmaGrey16Bit(this->doubleValue);
				}
				else if (this->readSigmaGeo)
				{
					this->cm.setSigmaGeom(this->doubleValue);
				}
				else if (this->readXYZPointName)
				{
					this->cm.setXYZFileName(this->string);
				}
				else if (this->readTMParameters)
				{
					this->defaultRefSys.setTMParameters(this->centralMeridian,this->FEasting,this->FNorthing,this->scale,this->latitudeOrigin);
				}

				this->readPatchSize = false;
				this->readIterationSteps = false;
				this->readMinCorrelation = false;
				this->readMinHeight = false;
				this->readMaxHeight = false;
				this->readSigma8Bit = false;
				this->readSigma16Bit = false;
				this->readSigmaGeo = false;
				this->readXYZPointName = false;
				this->readTMParameters = false;
				this->readPRISMScene = false;
				this->readAVNIRScene = false;

				break;
			}
	case 4: {
				if (this->readMergeScene)
				{
					if (this->readPRISMScene)
						this->scenePRISM.setMergeScene(true,this->deleteHugFiles,this->mergeName,this->mergeCamera);
					else if (this->readAVNIRScene)
						this->sceneAVNIR.setMergeBands(true,this->deleteHugFiles,this->mergeName,this->mergeCamera);

					this->deleteHugFiles = false;
					this->mergeName = "";
					this->mergeCamera = "";

				}
				this->readMergeScene = false;

			}
	}


}

void CGAXMLDataHandler::startElement(const XMLCh* const name , AttributeList&  attributes)
{
	this->xmlTag[this->tagDepth] = XMLString::transcode(name);

	this->read = noRead;

	switch (this->tagDepth)
	{
	case 0:		{
					if (attributes.getLength() == 1)
					{
						XMLSize_t idx = 0;
						this->BARISTA_BATCH_VERSION = atoi(XMLString::transcode(attributes.getValue(idx)));
					}
				}

	case 1:		{
				if (this->xmlTag[this->tagDepth].CompareNoCase("PROJECT_SETUP"))
					this->readProjectSetup = true;
				else if (this->xmlTag[this->tagDepth].CompareNoCase("PRISM_STRIPS"))
					this->readPRISMStrips = true;
				else if (this->xmlTag[this->tagDepth].CompareNoCase("AVNIR_STRIPS"))
					this->readAVNIRStrips = true;
				else if (this->xmlTag[this->tagDepth].CompareNoCase("DEM_FILES"))
					this->readDEMFiles = true;
				else if (this->xmlTag[this->tagDepth].CompareNoCase("ORTHO_IMAGES"))
					this->readOrthoImages = true;
				else if (this->xmlTag[this->tagDepth].CompareNoCase("OBJECT_POINTS"))
					this->readObjectPoints = true;

				break;
				}
	case 2:		{
					if (this->readProjectSetup)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("PROJECT_NAME"))
						{
							this->stringReader = &this->projectName;
							this->read = readString;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("PROJECT_TEMPLATE"))
						{
							this->stringReader = &this->templateName;
							this->read = readString;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("MEAN_GROUND_HEIGHT"))
						{
							this->doubleReader = &this->meanGroundHeight;
							this->read = readDouble;

						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("MATCHING_PARAMETERS"))
						{
							this->readMatchingParameters = true;
							this->cm.reset();

							for (unsigned int i=0; i < attributes.getLength(); i++)
							{
								CCharString aName = XMLString::transcode(attributes.getName(i));
								CCharString aValue = XMLString::transcode(attributes.getValue(i));

								if (aName.CompareNoCase("bandMode"))
								{
									if (aValue.CompareNoCase("ALL_IMAGES"))
										this->cm.setBandMode(eNONIDENTICALBANDS);
									else if (aValue.CompareNoCase("IDENTICAL_BANDS_ONLY"))
										this->cm.setBandMode(eIDENTICALBANDS);
								}
								else if (aName.CompareNoCase("bppMode"))
								{
									if (aValue.CompareNoCase("ALL_IMAGES"))
										this->cm.setBppMode(eALLBPPS);
									else if (aValue.CompareNoCase("IDENTICAL_BPP_ONLY"))
										this->cm.setBppMode(eIDENTICALBPPS);
								}
								else if (aName.CompareNoCase("transferMode"))
								{
									if (aValue.CompareNoCase("USE_TFW"))
										this->cm.setTrafoMode(eLSMTFW);
									else if (aValue.CompareNoCase("NO_TFW"))
										this->cm.setTrafoMode(eLSMNOTFW);
								}
								else if (aName.CompareNoCase("scaleMode"))
								{
									if (aValue.CompareNoCase("TEMPLATE"))
										this->cm.setScaleMode(eLSMUSETEMP);
									else if (aValue.CompareNoCase("SEARCH_IMAGE"))
										this->cm.setScaleMode(eLSMUSESEARCH);
								}
							}


						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("DEFAULT_REFSYS"))
						{
							this->readDefaultRefSys = true;

							if (attributes.getLength() == 1)
							{
								XMLSize_t idx = 0;
								CCharString test = XMLString::transcode(attributes.getValue(idx));
								

								if (test.CompareNoCase("WGS84"))
									this->defaultRefSys.setToWGS1984();
								else if (test.CompareNoCase("GRS1980"))
									this->defaultRefSys.setSpheroid(GRS1980Spheroid);
								else if (test.CompareNoCase("GRS1967"))
									this->defaultRefSys.setSpheroid(GRS1980Spheroid);
								else if (test.CompareNoCase("BESSEL"))
									this->defaultRefSys.setSpheroid(BesselSpheroid);
								else if (test.CompareNoCase("AUSTRALIAN_NATIONAL"))
									this->defaultRefSys.setSpheroid(AustralianNationalSpheroid);
								else if (test.CompareNoCase("CLARKE1866"))
									this->defaultRefSys.setSpheroid(Clarke1866Spheroid);
								else if (test.CompareNoCase("CLARKE1880"))
									this->defaultRefSys.setSpheroid(Clarke1880Spheroid);
								else if (test.CompareNoCase("HAYFORD"))
									this->defaultRefSys.setSpheroid(HayfordSpheroid);
								else if (test.CompareNoCase("IERS"))
									this->defaultRefSys.setSpheroid(IERSSpheroid);
							}

						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("ADJUSTMENT_SETTINGS"))
						{
							this->readAdjustmentSettings = true;

							for (unsigned int i=0; i < attributes.getLength(); i++)
							{
								CCharString aName = XMLString::transcode(attributes.getName(i));
								CCharString aValue = XMLString::transcode(attributes.getValue(i));

								if (aName.CompareNoCase("excludeTiePoints"))
								{
									bool useForward;
									bool excludeTie;
									double minRobustFac;
									double maxRobustPercentage;
									inifile.getAdjustmentSettings(useForward,excludeTie,maxRobustPercentage,minRobustFac);
									excludeTie = aValue.CompareNoCase("TRUE");
									inifile.setAdjustmentSettings(useForward,excludeTie,maxRobustPercentage,minRobustFac);
								}
							}

						}
					}
					else if (this->readPRISMStrips)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("PRISM_STRIP"))
						{
							this->readPRISMStrip = true;
							if (attributes.getLength() == 1 )
							{
								XMLSize_t idx = 0;
								CCharString test(XMLString::transcode(attributes.getValue(idx)));
								this->mergeOrbit = test.CompareNoCase("TRUE");
							}
						}
					}
					else if (this->readAVNIRStrips)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("AVNIR_STRIP"))
						{
							this->readAVNIRStrip = true;
							if (attributes.getLength() == 1 )
							{
								XMLSize_t idx = 0;
								CCharString test(XMLString::transcode(attributes.getValue(idx)));
								this->mergeOrbit = test.CompareNoCase("TRUE");
							}
						}

					}
					else if (this->readDEMFiles)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("DEM_FILE"))
						{
							this->readDEMFile = true;

							for (unsigned int i=0; i < attributes.getLength(); i++)
							{
								CCharString aName = XMLString::transcode(attributes.getName(i));
								CCharString aValue = XMLString::transcode(attributes.getValue(i));

								if (aName.CompareNoCase("coordinateType"))
								{
									if (aValue.CompareNoCase("GEOCENTRIC"))
										this->geoImage.setCoordinateType(eGEOCENTRIC);
									else if (aValue.CompareNoCase("GEOGRAPHIC"))
										this->geoImage.setCoordinateType(eGEOGRAPHIC);
									else if (aValue.CompareNoCase("GRID"))
										this->geoImage.setCoordinateType(eGRID);


								}
								else if (aName.CompareNoCase("useDefaultRefSys"))
								{
									if (aValue.CompareNoCase("TRUE"))
										this->geoImage.setUseDefaultRefSys();
								}
							}

						}

					}
					else if (this->readOrthoImages)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("ORTHO_IMAGE"))
						{
							this->readOrthoImage = true;

							for (unsigned int i=0; i < attributes.getLength(); i++)
							{
								CCharString aName = XMLString::transcode(attributes.getName(i));
								CCharString aValue = XMLString::transcode(attributes.getValue(i));

								if (aName.CompareNoCase("coordinateType"))
								{
									if (aValue.CompareNoCase("GEOCENTRIC"))
										this->geoImage.setCoordinateType(eGEOCENTRIC);
									else if (aValue.CompareNoCase("GEOGRAPHIC"))
										this->geoImage.setCoordinateType(eGEOGRAPHIC);
									else if (aValue.CompareNoCase("GRID"))
										this->geoImage.setCoordinateType(eGRID);


								}
								else if (aName.CompareNoCase("useDefaultRefSys"))
								{
									if (aValue.CompareNoCase("TRUE"))
										this->geoImage.setUseDefaultRefSys();
								}
								else if (aName.CompareNoCase("initWallisFilter"))
								{
									if (aValue.CompareNoCase("TRUE"))
										this->initWallis = true;
									else 
										this->initWallis = false;
								}
							}


						}
					}
					else if (this->readObjectPoints)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("OBJECT_POINT_ARRAY"))
						{
							this->readObjectPointArray = true;

							for (unsigned int i=0; i < attributes.getLength(); i++)
							{
								CCharString aName = XMLString::transcode(attributes.getName(i));
								CCharString aValue = XMLString::transcode(attributes.getValue(i));

								if (aName.CompareNoCase("coordinateType"))
								{
									if (aValue.CompareNoCase("GEOCENTRIC"))
										this->type = eGEOCENTRIC;
									else if (aValue.CompareNoCase("GEOGRAPHIC"))
										this->type = eGEOGRAPHIC;
									else if (aValue.CompareNoCase("GRID"))
										this->type = eGRID;


								}
								else if (aName.CompareNoCase("useDefaultRefSys"))
								{
									if (aValue.CompareNoCase("TRUE"))
										this->objectPointArray.setUseDefaultRefSys();
								}
								else if (aName.CompareNoCase("status"))
								{
									if (aValue.CompareNoCase("CONTROL_POINTS"))
										this->objectPointArray.setIsControl();
									else if (aValue.CompareNoCase("CHECK_POINTS"))
										this->objectPointArray.setIsCheckPoint();
								}
							}

						}
					}

					break;
				}

	case 3:		{
					if (this->readPRISMStrip)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("PRISM_SCENE"))
						{
							this->readPRISMScene = true;
							
							for (unsigned int i=0; i < attributes.getLength(); i++)
							{
								CCharString aName = XMLString::transcode(attributes.getName(i));
								CCharString aValue = XMLString::transcode(attributes.getValue(i));

								if (aName.CompareNoCase("initWallisFilter"))
								{
									if (aValue.CompareNoCase("TRUE"))
										this->initWallis = true;
									else 
										this->initWallis = false;

								}
							}

						}
					}
					else if (this->readAVNIRStrip)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("AVNIR_SCENE"))
						{
							this->readAVNIRScene = true;

							for (unsigned int i=0; i < attributes.getLength(); i++)
							{
								CCharString aName = XMLString::transcode(attributes.getName(i));
								CCharString aValue = XMLString::transcode(attributes.getValue(i));

								if (aName.CompareNoCase("initWallisFilter"))
								{
									if (aValue.CompareNoCase("TRUE"))
										this->initWallis = true;
									else 
										this->initWallis = false;

								}
							}
						}

					}
					else if (this->readDEMFile || this->readOrthoImage)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("SRC_DIRECTORY"))
						{
							this->read = readString;
							this->stringReader = &this->srcDir;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("DEST_DIRECTORY"))
						{
							this->read = readString;
							this->stringReader = &this->destDir;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("TFW_FILE"))
						{
							this->read = readString;
							this->stringReader = &this->tfwFile;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("SIGMA_Z"))
						{
							this->doubleReader = &this->demSigma_Z;
							this->read = readDouble;
							this->demSigma_ZFlag = true;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("OFFSET"))
						{
							this->doubleReader = &this->demOffset;
							this->read = readDouble;
							this->demOffsetFlag = true;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("SCALE"))
						{
							this->doubleReader = &this->demScale;
							this->read = readDouble;
							this->demScaleFlag = true;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("NO_DATA_VALUE"))
						{
							this->doubleReader = &this->demNoDataValue;
							this->read = readDouble;
							this->demNoDataValueFlag = true;
						}
					}
					else if (this->readObjectPointArray)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("FILE_NAME"))
						{
							this->read = readString;
							this->stringReader = &this->srcDir;
						}
					}
					else if (this->readMatchingParameters)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("PATCH_SIZE"))
						{
							this->readPatchSize = true;
							this->read = readInt;
							this->intReader = &this->intValue;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("ITERATION_STEPS"))
						{
							this->readIterationSteps = true;
							this->read = readInt;
							this->intReader = &this->intValue;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("MIN_CORRELATION"))
						{
							this->readMinCorrelation = true;
							this->read = readDouble;
							this->doubleReader = &this->doubleValue;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("MIN_HEIGHT"))
						{
							this->readMinHeight = true;
							this->read = readDouble;
							this->doubleReader = &this->doubleValue;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("MAX_HEIGHT"))
						{
							this->readMaxHeight = true;
							this->read = readDouble;
							this->doubleReader = &this->doubleValue;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("SIGMA_8BIT"))
						{
							this->readSigma8Bit = true;
							this->read = readDouble;
							this->doubleReader = &this->doubleValue;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("SIGMA_16BIT"))
						{
							this->readSigma16Bit = true;
							this->read = readDouble;
							this->doubleReader = &this->doubleValue;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("SIGMA_GEOMETRY"))
						{
							this->readSigmaGeo = true;
							this->read = readDouble;
							this->doubleReader = &this->doubleValue;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("XYZPOINTS"))
						{
							this->readXYZPointName = true;
							this->read = readString;
							this->stringReader = &this->string;
						}

					}
					else if (this->readDefaultRefSys)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("UTM_PARAMETERS"))
						{
							int zone = 0;
							int hemisphere = NORTHERN;

							for (unsigned int i=0; i < attributes.getLength(); i++)
							{
								CCharString aName = XMLString::transcode(attributes.getName(i));
								CCharString aValue = XMLString::transcode(attributes.getValue(i));

								if (aName.CompareNoCase("zone"))
								{
									zone = atoi(aValue.GetChar());
								}
								else if (aName.CompareNoCase("hemisphere"))
								{
									if (aValue.CompareNoCase("NORTHERN"))
										hemisphere = NORTHERN;
									else if (aValue.CompareNoCase("SOUTHERN"))
										hemisphere = SOUTHERN;

								}
							}

							this->defaultRefSys.setUTMZone(zone,hemisphere);

						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("TM_PARAMETERS"))
						{
							this->readTMParameters = true;
						}
					}
					else if (this->readAdjustmentSettings)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("MAX_ROBUST_PERCENTAGE"))
						{
							this->read = readDouble;
							this->doubleReader = &this->doubleValue;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("MIN_ROBUST_FACTOR"))
						{
							this->read = readDouble;
							this->doubleReader = &this->doubleValue1;
						}
					}

					break;
				}
	case 4:		{
					if (this->readPRISMScene || this->readAVNIRScene)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("SRC_DIRECTORY"))
						{
							this->read = readString;
							this->stringReader = &this->srcDir;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("DEST_DIRECTORY"))
						{
							this->read = readString;
							this->stringReader = &this->destDir;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("MERGE_SCENE"))
						{
							this->readMergeScene = true;
							if (attributes.getLength() == 1 )
							{
								XMLSize_t idx = 0;
								CCharString test(XMLString::transcode(attributes.getValue(idx)));
								this->deleteHugFiles = test.CompareNoCase("TRUE");
							}
						}
					}
					else if (this->readTMParameters)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("CENTRAL_MERIDIAN"))
						{
							this->read = readDouble;
							this->doubleReader = &this->centralMeridian;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("SCALE"))
						{
							this->read = readDouble;
							this->doubleReader = &this->scale;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("LATITUDE_ORIGIN"))
						{
							this->read = readDouble;
							this->doubleReader = &this->latitudeOrigin;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("FEASTING"))
						{
							this->read = readDouble;
							this->doubleReader = &this->FEasting;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("FNORTHING"))
						{
							this->read = readDouble;
							this->doubleReader = &this->FNorthing;
						}
					}

					break;
				}
	case 5:		{
					if (this->readMergeScene)
					{
						if (this->xmlTag[this->tagDepth].CompareNoCase("MERGED_FILE_NAME"))
						{
							this->read = readString;
							this->stringReader = &this->mergeName;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("MERGED_CAMERA_NAME"))
						{
							this->read = readString;
							this->stringReader = &this->mergeCamera;
						}
					}

					break;
				}
	}

	this->tagDepth++;

}



