#include "GeoImage.h"
#include "BaristaProject.h"
#include "ImageImporter.h"
#include "ProtocolHandler.h"

void CGeoImage::setFileName(CFileName name,CFileName tfwFile,bool isDEM,CFileName hugFileDir) 
{
	this->filename = name;
	this->tfwFileName = tfwFile; 
	this->isDEM = isDEM;
	this->hugeFileDir = hugFileDir;
}





bool CGeoImage::loadImage(const CReferenceSystem& defaultRefSys)
{
	bool ret = true;

	CImageImportData d;
	CImageImporter importer;
	ret = importer.initImportData(d,this->isDEM,this->filename,this->hugeFileDir);

	ostrstream oss;
	oss << "analysing File " << d.filename.GetFileName() << "...";
	
	if (ret)
	{
		oss << "ok\n";
		oss << "importing File " << d.filename.GetFileName() << "...";

		if (!this->tfwFileName.IsEmpty())
		{
			d.tfwSource = CImageImportData::eExternalFile;
			d.tfwFilename = this->tfwFileName;
		}

		d.initWallisFilter = this->initWallisFilter;
		
		if (this->isSetSigmaZ())
			d.sigmaZ = this->getSigmaZ();
		if (this->isSetNoDataValue())
			d.NoDataValue = this->getNoDataValue();
		if (this->isSetOffset())
			d.Z0 = this->getOffset();
		if (this->isSetScale())
			d.scale = this->getScale();


		ret = importer.importImage(d, this->isDEM);
	}

	if (ret)
	{
		CReferenceSystem local = d.tfw.getRefSys();
		if (this->useDefaultRefSys)
		{
			local = defaultRefSys;
		}

		local.setCoordinateType(this->coordType);

		d.tfw.setRefSys(local);
		
		if (d.vbase)
			d.vbase->setTFW(d.tfw);

		if (d.vbase && !d.tfw.getRefSys().gethasValues())
			oss << "warning, TFW information incomplete\n\n";
		else
			oss << "ok\n\n";

	}
	else
	{
		d.deleteImageFile();
		oss << "failed with error message: " << importer.getErrorMessage().GetChar() << "\n";
	}

	protHandler.print(oss,CProtocolHandler::prt);

	return ret;
}