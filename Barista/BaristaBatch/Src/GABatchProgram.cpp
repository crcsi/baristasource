#include "GABatchProgram.h"

#include "BaristaProject.h"

#include "ProtocolHandler.h"

#include "ImageImporter.h"
#include "ImageMerger.h"

#include "GAXMLDataHandler.h"


CGABatchProgram::CGABatchProgram(CFileName parametersFile) :
	CBatchProgram(parametersFile)
{

}

CGABatchProgram::~CGABatchProgram()
{

}

bool CGABatchProgram::runProgram()
{
	// init project
	bool ret = this->initProject(this->parameters);

	// process PRISM
	if (ret)
		ret = this->processPRISM(this->parameters);

	// process AVNIR-2
	if (ret)
		ret = this->processAVNIR2(this->parameters);

	if (ret)
		ret = this->loadDEMs(this->parameters);

	if (ret)
		ret = this->loadOrthoImages(this->parameters);

	if (ret)
		ret = this->loadObjectPoints(this->parameters);

	if (ret)
	{
		CReferenceSystem refsys = this->parameters.getDefaultReferenceSystem();
		refsys.setCoordinateType(eGRID);
		project.insertSatelliteFootprintsToLines(refsys);
	}

	if (ret)
		ret = this->writeProject(this->parameters.getProjectName());
	
	return ret;
}


CXMLDataHandler*  CGABatchProgram::getXMLDataHandler()
{
	// the parser will delete the memory of the dataHandler!!
	return new CGAXMLDataHandler(this->parameters);
}


bool CGABatchProgram::initProject(CGABatchParameters& parameters) const
{
	bool ret = true;

	ostrstream oss;

	oss << "initialising project\n" 
		<< "====================\n\n";


	if (!parameters.getTemplateProjectName().IsEmpty())
	{
		oss << "Loading template project " << parameters.getTemplateProjectName().GetChar() << " ...";
		project.readProject(parameters.getTemplateProjectName().GetChar());
		
		oss << "ok\n";
	}


	if (parameters.getHasMeanGroundHeight())
	{
		oss << "setting mean ground height to: " << parameters.getMeanGroundHeight() << "\n";
		project.setMeanHeight(parameters.getMeanGroundHeight());
	}

	if (parameters.getHasMatchingParameters())
	{
		oss << "setting	matching parameters\n";
		project.setMatchingParameter(parameters.getMatchingParameters());
	}



	if (!parameters.getProjectName().IsEmpty())
	{
		oss << "Creating project " << parameters.getProjectName().GetChar() << "\n";
		
		project.writeProject(parameters.getProjectName().GetChar());
		project.setFileName(parameters.getProjectName().GetChar());
	}
	else
	{
		oss << "Empty filename!";
		ret = false;
	}

	protHandler.print(oss,CProtocolHandler::prt);

	return ret;
}


bool CGABatchProgram::processPRISM(CGABatchParameters& parameters) const
{
	bool ret = true;

	ostrstream oss;

	oss << "processing ALOS PRISM strips\n" 
		<< "============================\n\n"
		<< "strips available: " << parameters.getNumberOfPRISMStrips() << "\n";

	protHandler.print(oss,CProtocolHandler::prt);

	for (unsigned int strip = 0; strip < parameters.getNumberOfPRISMStrips(); strip++)
	{
		ostrstream oss1;
		oss1 << "processing strip: " << strip + 1 << "\n";
		oss1 << "--------------------\n";
		protHandler.print(oss1,CProtocolHandler::prt);


		CPRISMStrip& prismStrip = parameters.getPRISMStrip(strip);
		prismStrip.processStrip();

	}

	return ret;
}


bool CGABatchProgram::processAVNIR2(CGABatchParameters& parameters) const
{
	bool ret = true;

	ostrstream oss;

	oss << "processing ALOS AVNIR-2  strips\n" 
		<< "===============================\n\n"
		<< "strips available: " << parameters.getNumberOfAVNIRStrips() << "\n";

	protHandler.print(oss,CProtocolHandler::prt);

	for (unsigned int strip = 0; strip < parameters.getNumberOfAVNIRStrips(); strip++)
	{
		ostrstream oss1;
		oss1 << "processing strip: " << strip + 1 << "\n";
		oss1 << "--------------------\n";
		protHandler.print(oss1,CProtocolHandler::prt);


		CAVNIRStrip& prismStrip = parameters.getAVNIRStrip(strip);
		prismStrip.processStrip();

	}


	return ret;
}


bool CGABatchProgram::loadObjectPoints(CGABatchParameters& parameters) const
{
	bool ret = true;

	ostrstream oss;

	oss << "loading object point files\n" 
		<< "============================\n\n"
		<< "files available: " << parameters.getNumberOfObjectPoints() << "\n";
	

	for (unsigned int i=0; i < parameters.getNumberOfObjectPoints(); i++)
	{
		CObjectPoints& op = parameters.getObjectPoints(i);
		
		oss << "loading: " << op.getFileName().GetChar() << "\n";
		op.loadObjectPoints(parameters.getDefaultReferenceSystem());
	}

	protHandler.print(oss,CProtocolHandler::prt);
	return ret;
}


bool CGABatchProgram::loadDEMs(CGABatchParameters& parameters) const
{
	bool ret = true;
	
	ostrstream oss;

	oss << "loading DEM files\n" 
		<< "=================\n\n"
		<< "files available: " << parameters.getNumberOfDEMFiles() << "\n";
	protHandler.print(oss,CProtocolHandler::prt);


	for (unsigned int i=0; i < parameters.getNumberOfDEMFiles(); i++)
	{
		ostrstream oss1;
		oss1 << "processing dem file: " << i + 1 << "\n";
		oss1 << "-----------------------\n";
		protHandler.print(oss1,CProtocolHandler::prt);
		
		CGeoImage& gi = parameters.getDEMFile(i);
		gi.loadImage(parameters.getDefaultReferenceSystem());
	}

	return ret;
}

bool CGABatchProgram::loadOrthoImages(CGABatchParameters& parameters) const
{
	bool ret = true;

	ostrstream oss;

	oss << "loading ortho images\n" 
		<< "====================\n\n"
		<< "files available: " << parameters.getNumberOfOrthoImageFiles() << "\n";
	protHandler.print(oss,CProtocolHandler::prt);


	for (unsigned int i=0; i < parameters.getNumberOfOrthoImageFiles(); i++)
	{
		ostrstream oss1;
		oss1 << "processing ortho image file: " << i + 1 << "\n";
		oss1 << "------------------------------\n";
		protHandler.print(oss1,CProtocolHandler::prt);
		
		CGeoImage& gi = parameters.getOrthoImageFile(i);
		gi.loadImage(parameters.getDefaultReferenceSystem());
	}

	return ret;
}

