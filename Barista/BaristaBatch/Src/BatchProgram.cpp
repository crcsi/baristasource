#include "BatchProgram.h"

#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "IniFile.h"
#include "XMLParser.h"
#include "XMLErrorHandler.h"
#include "BaristaProject.h"

CBatchProgram::CBatchProgram(CFileName _parametersFile) : 
	dtdFile("baristaBatch.dtd"), parametersFile(_parametersFile)
{

}


CXMLErrorHandler* CBatchProgram::getXMLErrorHandler()
{
	// the parser will delete the memory of the errorHandler!!
	return new CXMLErrorHandler(this->messages,this->errors,this->errors) ;
}


bool CBatchProgram::startBatchProgram(CFileName exePath)
{
	bool ret = true;

	// determine absolute path 
	this->parametersFile = CSystemUtilities::AbsPath(this->parametersFile);

	if (CSystemUtilities::fileExists(this->parametersFile) && !exePath.IsEmpty())	
	{

		
		CFileName protocolFile = this->parametersFile;
		protocolFile.SetFileExt("prt");
		protHandler.open(CProtocolHandler::prt, protocolFile.GetChar(), false);

		ostrstream oss,oss1;
		oss << "Barista Batch Program:\n"
			<< "======================\n" 
			<< "\nStart time: " << CSystemUtilities::timeString().GetChar() << "\n";
		protHandler.print(oss,CProtocolHandler::prt);


		// check if we have to copy the dtd file
		CCharString parameterPath = this->parametersFile.GetPath();

		bool copy = parameterPath != exePath;

		CFileName fn = parameterPath + CSystemUtilities::dirDelimStr + this->dtdFile;

		if (copy)
		{
			// copy file from Barista bin directory
			CFileName fn_system = exePath + CSystemUtilities::dirDelimStr + this->dtdFile;

			if (CSystemUtilities::fileExists(fn_system))
				CSystemUtilities::copy(fn_system, fn);
			else
			{
				ostrstream oss2;

				oss2 << "file : " << fn_system.GetChar() << " not found!\n"
					<< "Execution of  batch program not possible.\n\n";
				protHandler.print(oss2,CProtocolHandler::prt);
				ret = false;
			}

		}

		// init project from iniFile
		if (CSystemUtilities::fileExists(inifile.getIniFileName()))
			inifile.readIniFile();

		// change directory to parameters file directory,
		// that allows us to use relative paths
		CSystemUtilities::chDir(this->parametersFile.GetPath());

		if (ret)
			ret = this->parseFile();

		if (ret)
			ret = this->runProgram();

		oss1 << "End time: " << CSystemUtilities::timeString().GetChar() << "\n"
			 << "\nProgram finished.\n";
		protHandler.print(oss1,CProtocolHandler::prt);
		protHandler.close(CProtocolHandler::prt);

		if (copy) CSystemUtilities::del(fn);


	}
	else
	{
		CCharString error;
		error.Format("File %s not found.",this->parametersFile.GetChar());
		cout << error.GetChar() << "\n";
		ret = false;
	}


	return ret;
}

bool CBatchProgram::parseFile()
{
	bool ret = false;

	ostrstream oss;

	oss << "reading of batch file \n" 
		<< "=====================\n\n";

	oss << "parsing file: " << parametersFile.GetChar() << "\n" ;

	CXMLParser parser;

	// the parser will delete the memory of the handlers!!
	ret = parser.parseFile(this->parametersFile,this->getXMLDataHandler(),this->getXMLErrorHandler());

	if (errors.size())
		ret = false;

	if (messages.size() || errors.size())
	{
		oss << "\n";
		oss << "messages from parser (in order of occurrence)\n";
		oss << "---------------------------------------------\n\n";


		for (unsigned int i=0; i < messages.size(); i++)
		{
			oss << messages[i].GetChar() << "\n";
		}


		for (unsigned int i=0; i < errors.size(); i++)
		{
			oss << errors[i].GetChar() << "\n";
		}
	}

	protHandler.print(oss,CProtocolHandler::prt);
	return ret;
}


bool CBatchProgram::writeProject(const CFileName& projectFileName) const
{
	bool ret = true;

	ostrstream oss;
	oss << "\n" << "writing project " << projectFileName.GetChar() << " ...\n";
	protHandler.print(oss,CProtocolHandler::prt);	
	
	project.writeProject(projectFileName.GetChar());
	project.setFileName(projectFileName.GetChar());

	return ret;
}