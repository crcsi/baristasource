#include "ORTHOProject.h"
#include "BaristaProject.h"
#include "ProtocolHandler.h"
#include "OrthoPhotoVector.h"
#include "OrthoImageGenerator.h"
#include "ImageImporter.h"
#include "ImageExporter.h"
#include "SystemUtilities.h"


CORTHOProject::CORTHOProject() : 
	bicubic(DEFAULT_USE_BICUBIC), useAnchorPoints(DEFAULT_USE_ANCHOR_POINTS),
	maxDistAnchors(DEFAULT_ANCHOR_WIDTH),gsd(DEFAULT_GSD),saveProject(DEFAULT_SAVE_PROJECT),
	deleteSrcHugeFiles(DEFAULT_DELETE_SRC_HUGEFILES),deleteOrthoHugeFiles(DEFAULT_DELETE_ORTHO_HUGEFILES)
{
	
}

CORTHOProject::~CORTHOProject() 
{
	
}

CORTHOProject::CORTHOProject(const CORTHOProject& src) :
	bicubic(src.bicubic), useAnchorPoints(src.useAnchorPoints), 
	maxDistAnchors(src.maxDistAnchors),srcProjectFileName(src.srcProjectFileName),
	destProjectFileName(src.destProjectFileName),gsd(src.gsd),saveProject(src.saveProject),
	deleteSrcHugeFiles(src.deleteSrcHugeFiles),deleteOrthoHugeFiles(src.deleteOrthoHugeFiles)
{

}

bool CORTHOProject::processProject()
{
	bool ret = true;

	ostrstream oss;
	oss << "reading project file: " << this->srcProjectFileName.GetChar() << "\n";
	protHandler.print(oss,CProtocolHandler::prt);


	project.readProject(this->srcProjectFileName.GetChar());


	COrthoPhotoVector imageVector;
	// check project and init the vector
	imageVector.init(NULL,NULL,NULL);
	imageVector.setMaxExtentsForAll();

	// used to store the reimported ortho images, gives us access to the huge image 
	// to delete it!
	vector<CVImage*> orthoImagesInProject(imageVector.size(),NULL);

	// the user can only select this for the project, if something goes wrong with a single image
	// we should be able to adjust this settings
	vector<bool> deleteSourceHugeFiles(imageVector.size(),this->getDeleteSrcHugeFiles());
	vector<bool> deleteOrthoHugeFiles(imageVector.size(), this->getDeleteOrthoHugeFiles());

	ostrstream oss1;
	oss1 << "initialising ortho photo generation\n";
	oss1 << "-----------------------------------\n";
	oss1 << "suitable images found: " << imageVector.size() << "\n";
	for (unsigned int i=0; i < imageVector.size(); i++)
	{
		oss1 << imageVector[i].getInputImg()->getFileNameChar() << "\n";
	}
	
	oss1 << "\n";
	if (imageVector.getDEM())
	{
		oss1 << "DEM found: " << imageVector.getDEM()->getFileName()->GetChar() <<"\n";
	}
	else
	{
		oss1 << "no DEM found! Ortho photo generation not posible for this project!\n";
		ret = false;
	}
	protHandler.print(oss1,CProtocolHandler::prt);

	if (ret)
	{
		for (unsigned int i = 0; i < imageVector.size(); ++i)
		{

			COrthoImageGenerator orthogenerator;
			orthogenerator.setLLX(imageVector[i].getLLC().x);
			orthogenerator.setLLY(imageVector[i].getLLC().y);
			orthogenerator.setURX(imageVector[i].getRUC().x);
			orthogenerator.setURY(imageVector[i].getRUC().y);
			orthogenerator.setGsd(this->gsd);
			orthogenerator.setImage(imageVector[i].getInputImg());
			orthogenerator.setDEM(imageVector.getDEM());
			orthogenerator.setBicubic(this->getUseBicubicInterpolation());
			orthogenerator.setUseAnchor(this->getUseAnchorPoints());
			orthogenerator.setAnchorWidth(this->getMaxDistAnchorPoints());
			orthogenerator.orthoimage = imageVector[i].getOrtho();


			ostrstream oss2;
			oss2 << "creating ortho photo: " << imageVector[i].getOrthoFileName() << "... ";
			protHandler.print(oss2,CProtocolHandler::prt);
			// create the ortho image
			bool success = orthogenerator.makeOrthoImage();
			
			ostrstream oss3;
			if (success)
			{
				
				oss3 << "ok\n";

				// export ortho
				CImageExporter& imageExporter = imageVector[i].getImageExporter();
				imageExporter.writeSettings();

				oss3 << "exporting ortho photo: " << imageExporter.getFilename().GetChar() << "... ";
				protHandler.print(oss3,CProtocolHandler::prt);

				success = imageExporter.writeImage();

				ostrstream oss4;
				if (success)
				{
					
					oss4 << "ok\n";
					// reimport into project
					CImageImporter imageImporter;
					CImageImportData importData;
					
					imageImporter.initImportData(importData,false,imageVector[i].getOrthoFileName(),"");
					imageImporter.importImage(importData,false);
					
					// the ortho image in the imageVector is just local
					// we need the pointer of the image in the project
					orthoImagesInProject[i] = importData.vimage;
					
				}
				else
				{
					oss4 << "failed! File could not be exported! Open Barista and export file manually!\n";
					// something went wrong when exporting the image, maybe not supported export settings??
					// so we just import the huge file and leave the export to the user
					CImageImporter imageImporter;
					CImageImportData importData;
					
					imageImporter.initImportData(importData,false,imageVector[i].getOrthoHugeFileName(),"");
					imageImporter.importImage(importData,false);

					// we should not delete the huge file in such case,
					// give the user the chance to save the file manually
					deleteOrthoHugeFiles[i] = false;
				}
				protHandler.print(oss4,CProtocolHandler::prt);
			}
			else
			{
				oss3 << "failed\n";
				protHandler.print(oss3,CProtocolHandler::prt);
				
				// ortho photo generation failed so we better keep the source image
				deleteSourceHugeFiles[i] = false;
				// has not been created
				deleteOrthoHugeFiles[i] = false;
			}

		}
	}





	if (this->getDeleteSrcHugeFiles())
	{
		ostrstream oss6;
		oss6 << "deleting .hug files of source images:\n";

		for (unsigned int i = 0; i < imageVector.size(); ++i)
		{
			if (imageVector[i].getInputImg() )
			{
				
				// thats the file we want to delete
				CFileName hname = *imageVector[i].getInputImg()->getHugeImage()->getFileName();
				
				if (deleteSourceHugeFiles[i] )
				{
					// remove also according vimage from project, of no use anymore
					project.deleteImage(imageVector[i].getInputImg());

					oss6 << "deleting: " << hname.GetChar() << "\n";

					CSystemUtilities::del(hname);
				}
				else
					oss6 << "skiped: " << hname.GetChar() << "\n";
			}
		}
			
		protHandler.print(oss6,CProtocolHandler::prt);
	}

	if (this->getDeleteOrthoHugeFiles())
	{
	
		ostrstream oss6;
		oss6 << "deleting .hug files of ortho images:\n";

		for (unsigned int i = 0; i < orthoImagesInProject.size(); ++i)
		{
			if (orthoImagesInProject[i])
			{
				// thats the file we want to delete
				CFileName hname = *orthoImagesInProject[i]->getHugeImage()->getFileName();

				if (deleteOrthoHugeFiles[i])
				{
					// remove also according vimage from project, of no use anymore
					project.deleteImage(orthoImagesInProject[i]);

					oss6 << "deleting: " << hname.GetChar() << "\n";

					CSystemUtilities::del(hname);
				}
				else
					oss6 << "skiped: " << hname.GetChar() << "\n";
			}
		}

		protHandler.print(oss6,CProtocolHandler::prt);
	}

	if (this->getSaveProject())
	{
		ostrstream oss5;
		oss5 << "writing project file: " << this->getDestProjectFileName().GetChar() << "\n\n\n";
		protHandler.print(oss5,CProtocolHandler::prt);

		project.writeProject(this->getDestProjectFileName().GetChar());
	}


	project.close();

	return ret;
}

CFileName CORTHOProject::getDestProjectFileName() 
{ 
	if (this->destProjectFileName.IsEmpty())
		this->destProjectFileName = this->srcProjectFileName;

	return this->destProjectFileName;
}