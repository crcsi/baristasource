#include "BatchProgramFactory.h"

#include "GABatchProgram.h"
#include "ORTHOBatchProgram.h"
#include <iostream>


const char *CBatchProgramFactroy::Usage[] =  {
	"BaristaBatch program: ("__DATE__")\n",
	"\nUsage     :  BaristaBatch[.exe] option <parametersFile.xml>\n\n",
      "OPTIONS :\n",
        "           -h[elp]                 Shows this help information\n",
        "           -p[rojectSetup]         Use <parametersFile.xml> for project setup\n",
        "           -o[rthoGeneration]      Use <parametersFile.xml> for ortho photo generation\n\n",
        ""
};



CBatchProgram* CBatchProgramFactroy::createBatchProgram(int argc, char* argv[])
{
	CBatchProgram* program = NULL;
	
	if (argc >= 2) 
	{                                
		// process command line arguments
		CCharString option(argv[1]);
		if (option.GetLength() > 1 && option[0] == '-') 
		{
			CCharString mode = option.Right(option.GetLength() - 1) ;
			if (mode.CompareNoCase("h") || mode.CompareNoCase("help"))
			{
				// just show error message
			}
			else if ((mode.CompareNoCase("p") || mode.CompareNoCase("projectSetup")) && argc == 3)
			{
				program = new CGABatchProgram(argv[2]);
			}
			else if ((mode.CompareNoCase("o") || mode.CompareNoCase("orthoGeneration")) && argc == 3)
			{
				program  = new CORTHOBatchProgram(argv[2]);
			}
		}
	}

	return program;
}

void CBatchProgramFactroy::printHelpText()
{
	for (int j = 0; strlen (CBatchProgramFactroy::Usage[j]); j++)
	{
		cout <<  Usage[j];
	}
}
