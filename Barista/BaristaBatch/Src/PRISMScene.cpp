#include "PRISMScene.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"
#include "ImageMerger.h"
#include "BaristaProject.h"

CPRISMScene::~CPRISMScene()
{
	for (unsigned int i=0; i < this->importData.size(); i++)
	{
		this->importData[i].deleteImageFile();
	}
}



void CPRISMScene::setDirectory(CFileName newDirectory,CFileName newHugeFileDirectory)
{

	if (!newDirectory.IsEmpty())
	{
		if (newDirectory.GetChar()[newDirectory.GetLength() - 1] != CSystemUtilities::dirDelim)
		{
			newDirectory += CSystemUtilities::dirDelimStr;
		}
	}

	if (!newHugeFileDirectory.IsEmpty())
	{
		if (newHugeFileDirectory.GetChar()[newHugeFileDirectory.GetLength() - 1] != CSystemUtilities::dirDelim)
		{
			newHugeFileDirectory += CSystemUtilities::dirDelimStr;
		}
	}

	this->directory = newDirectory;
	this->hugefileDirectory = newHugeFileDirectory;

}

void CPRISMScene::setMergeScene(bool merge,bool deleteHugFilesOfSubImages,CFileName mergeName,CCharString cameraName)
{
	this->mergeScene = merge;
	this->deleteHugeFilesOfSubImages = deleteHugFilesOfSubImages;

	if (mergeName.IsEmpty())
		this->mergeName = "mergedALOS_PRISMScene";
	else
		this->mergeName = mergeName;

	
	project.getUniqueImageName(this->mergeName);
	
	this->mergeName = CSystemUtilities::AbsPath(this->mergeName);

	

	if (cameraName.IsEmpty())
		this->cameraName = "mergedCamera";
	else
		this->cameraName = cameraName;

	project.getUniqueCameraName(this->cameraName);

}


bool CPRISMScene::processScene()
{
	bool ret = true;

	if (ret)
		ret = this->loadScene();

	if (ret)
		ret = this->mergeSubImages();

	return ret;
}


bool CPRISMScene::loadScene()
{
	bool ret = false;

	this->importData.clear();

	ostrstream oss;
	oss << "loading images from directory: " << this->directory.GetChar() << "\n";


	CCharStringArray fileList;
	fileList.RemoveAll();
	CCharStringArray filters;
	filters.Add("IMG-*__N");
	filters.Add("IMG-*__F");
	filters.Add("IMG-*__B");
	filters.Add("IMG-*__W");
	

	for (int i=0; i < filters.GetSize() && !fileList.GetSize(); i++)
	{
		CSystemUtilities::dir(fileList,this->directory,filters.GetAt(i)->GetChar());
	}

	oss << "Files found: " << fileList.GetSize();

	protHandler.print(oss,CProtocolHandler::prt);

	if (fileList.GetSize())
	{

		for (int i=0; i < fileList.GetSize(); i++)
		{
			CImageImportData d;
			this->importData.push_back(d);
			
			unsigned int index = this->importData.size()-1;
			CImageImporter importer;
			ret = importer.initImportData(this->importData[index],false,*fileList.GetAt(i),this->hugefileDirectory);
			
			ostrstream oss1,oss2;
			oss1 << "analysing File " << this->importData[index].filename.GetFileName() << "...";
			
			if (ret)
			{
				oss1 << "ok\n";
				oss1 << "importing File " << this->importData[index].filename.GetFileName() << "...";
				protHandler.print(oss1,CProtocolHandler::prt);
		

				// we want to load the pushbroom data as well
				this->importData[index].bImportPushBroom = true;
				this->importData[index].satType = Alos;
				this->importData[index].pushBroomFilename = this->importData[index].filename;

				// when we keep the imported images then check if we need to init the wallis filter
				if (!this->mergeScene && this->initWallisFilter)
					this->importData[index].initWallisFilter = true;

				ret = importer.importImage(this->importData[index],false);
			}

			if (ret)
				oss2 << "import ok\n";
			else
			{
				this->importData[index].deleteImageFile();
				this->importData.pop_back();			

				oss2 << "failed with error message: " << importer.getErrorMessage().GetChar();
			}

			protHandler.print(oss2,CProtocolHandler::prt);

		}
	}
	else
		ret = false;

	return ret;
}



bool CPRISMScene::mergeSubImages()
{
	bool ret = true;

	if (this->mergeScene)
	{


		ostrstream oss;
		oss << "merging ALOS PRISM sub images\n";
		oss << "------------------------------------\n\n";
		oss << "name of merged image: " << this->mergeName.GetChar() << "\n";
		oss << "name of merged camera: " << this->cameraName.GetChar() << "\n";
		

		CImageMerger merger;
		CVImage* newImage = project.addImage(this->mergeName,false);
		

		vector<CVImage*> inputImages;

		for (unsigned int i=0; i < this->importData.size(); i++)
			if (this->importData[i].vimage)
				inputImages.push_back(this->importData[i].vimage);

		CImageMerger::sortImages(inputImages);

		oss << "Images found to merge: " << inputImages.size() << "\n";

		if (inputImages.size() > 1)
		{
			unsigned int startPos = CImageMerger::findBoundary(inputImages[0]->getHugeImage(),false);
			unsigned int endPos = CImageMerger::findBoundary(inputImages[inputImages.size() -1 ]->getHugeImage(),true);

			ret = merger.init(inputImages,startPos,endPos,this->cameraName,CImageMerger::eBiLinear,newImage);

			if (!ret)
			{
				oss << " error while initialising: " << merger.getErrorMessage().GetChar() << "\n";
				protHandler.print(oss,CProtocolHandler::prt);
			}
			else
			{
				oss  << "merging ...";
				
				protHandler.print(oss,CProtocolHandler::prt);
				
				merger.setInitWallisFilter(this->initWallisFilter);
				ret = merger.mergeImages();

				
				if (!ret)
				{
					ostrstream oss1;
					oss1 << "error while merging images: "<< merger.getErrorMessage().GetChar() << "\n";
					protHandler.print(oss1,CProtocolHandler::prt);
				}
				else
				{
					ostrstream oss1;
					oss1 << "           ok\n";
					oss1 << "removing sub images from project\n";
					protHandler.print(oss1,CProtocolHandler::prt);
					
					this->deleteHugeFiles();

					for (unsigned int i=0; i < inputImages.size(); i++)
					{
						project.deleteImage(inputImages[i]);
					}

					// when merging the orbits we will now use the merged image
					this->importData[0].vimage = newImage;
				}
			}
		}
	}

	return ret;
}


bool CPRISMScene::deleteHugeFiles()
{
	bool ret = true;

	if (this->deleteHugeFilesOfSubImages)
	{
		ostrstream oss;
		oss << "deleting .hug files \n";
		protHandler.print(oss,CProtocolHandler::prt);
		
		for (unsigned int i=0; i < this->importData.size(); i++)
		{
			if (this->importData[i].vimage)
			{
				this->importData[i].vimage->getHugeImage()->closeHugeFile();
				CSystemUtilities::del(*(this->importData[i].vimage->getHugeImage()->getFileName()));
			}
		}
	}

	return ret;
}


CVImage* CPRISMScene::getFirstImageOfScene()
{
	CVImage* vimage = NULL;

	if (this->importData.size())
		vimage = importData[0].vimage;

	return vimage;
}
