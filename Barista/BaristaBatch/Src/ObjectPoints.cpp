#include "ObjectPoints.h"
#include "BaristaProject.h"
#include "Trans3DFactory.h"

bool CObjectPoints::loadObjectPoints(const CReferenceSystem& defaultRefSys)
{
	bool ret = true;

	CReferenceSystem local;
	
	if (this->useDefaultRefSys)
	{
		local = defaultRefSys;
	}

	local.setCoordinateType(this->coordType);

	CXYZPointArray* points = project.addXYZPoints(this->filename,local,false,true,eNOTYPE);

	if (points->getReferenceSystem()->getCoordinateType() != eGEOCENTRIC && this->isControl)
	{
		CReferenceSystem refsys(points->getRefSys());
		refsys.setCoordinateType(eGEOCENTRIC);
		CFileName newname = *points->getFileName() + " (Geocentric)";
		CXYZPointArray* Geocentricpoints = project.addXYZPoints(newname, refsys, true, false,eNOTYPE);

		CTrans3D* trans = CTrans3DFactory::initTransformation(points->getRefSys(),Geocentricpoints->getRefSys());

		if (trans)
		{
			trans->transformArray(Geocentricpoints, points);
			delete trans;

			points = Geocentricpoints;
		}
	}

	vector<CVImage*> images;

	for (int i=0; i < project.getNumberOfImages(); i++)
		images.push_back(project.getImageAt(i));

	
	CReferenceSystem refsys(points->getRefSys());
	refsys.setCoordinateType(eGEOCENTRIC);

	CTrans3D* trans = CTrans3DFactory::initTransformation(points->getRefSys(),refsys);
	
	if (trans)
	{
		CXYZPointArray tmpArray;
		tmpArray.setReferenceSystem(refsys);
		trans->transformArray(&tmpArray, points);
		ret = project.backProjectXYZPoints(images,tmpArray,this->errorMessage,true,true);	
		delete trans;
	}
	else
		ret = project.backProjectXYZPoints(images,*points,this->errorMessage,true,true);		

	if (this->isControl)
		project.setControlPoints(points);
	else if (this->isCheckPoint)
		project.setCheckPoints(points);


	return ret;
}