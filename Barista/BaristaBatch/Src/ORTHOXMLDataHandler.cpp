#include "ORTHOXMLDataHandler.h"


#include <xercesc/sax/AttributeList.hpp>
#include "IniFile.h"
#include "SystemUtilities.h"
#include "OrthoPhotoVector.h"

CORTHOXMLDataHandler::CORTHOXMLDataHandler(CORTHOBatchParameters& parameters) : 
	data(parameters),ORTHOGENERATION_BATCH_VERSION(0),useBicubicInterpolationGlobal(DEFAULT_USE_BICUBIC),
	useAnchorPointsGloabl(DEFAULT_USE_ANCHOR_POINTS),maxDistAnchorPointsGlobal(DEFAULT_ANCHOR_WIDTH),
	gsdGlobal(DEFAULT_GSD),
	saveProjectGlobal(DEFAULT_SAVE_PROJECT),deleteSrcGlobal(DEFAULT_DELETE_SRC_HUGEFILES),
	deleteOrthoHugGlobal(DEFAULT_DELETE_ORTHO_HUGEFILES)
{
	this->readOrthoProject = false;
}

CORTHOXMLDataHandler::~CORTHOXMLDataHandler(void)
{
}


void CORTHOXMLDataHandler::endElement(const XMLCh* const name )
{
	this->tagDepth--;


	switch (this->tagDepth)
	{
	case 1:	{
				if (this->readOrthoProject)
				{
					this->orthoProject.setSrcProjectFileName(CSystemUtilities::AbsPath(this->projectName));
					this->data.addProject(this->orthoProject);
				}
				this->readOrthoProject = false;
				break;
			}
	}
}

void CORTHOXMLDataHandler::startElement(const XMLCh* const name , AttributeList&  attributes)
{
	this->xmlTag[this->tagDepth] = XMLString::transcode(name);

	this->read = noRead;

	switch (this->tagDepth)
	{
	case 0:		{
					if (attributes.getLength() == 1)
					{
						XMLSize_t idx = 0;
						this->ORTHOGENERATION_BATCH_VERSION = atoi(XMLString::transcode(attributes.getValue(idx)));
					}
				}

	case 1:		{
				if (this->xmlTag[this->tagDepth].CompareNoCase("ORTHO_SETTINGS"))
				{
					this->parseAttributeListOrthoSettings(attributes,this->useBicubicInterpolationGlobal,this->useAnchorPointsGloabl,this->maxDistAnchorPointsGlobal,this->gsdGlobal);
				}
				else if (this->xmlTag[this->tagDepth].CompareNoCase("PROJECT_SETTINGS"))
				{
					this->parseAttributeListProjectSettings(attributes,this->saveProjectGlobal,this->deleteSrcGlobal,this->deleteOrthoHugGlobal);
				}
				else if (this->xmlTag[this->tagDepth].CompareNoCase("ORTHO_PROJECT"))
				{
					this->readOrthoProject = true;
					
					// init project with global values
					this->orthoProject.setUseBicubicInterpolation(this->useBicubicInterpolationGlobal);
					this->orthoProject.setUseAnchorPoints(this->useAnchorPointsGloabl);
					this->orthoProject.setMaxDistAnchorPoints(this->maxDistAnchorPointsGlobal);
					this->orthoProject.setGSD(this->gsdGlobal);
					this->orthoProject.setSaveProject(this->saveProjectGlobal);
					this->orthoProject.setDeleteSrcHugeFiles(this->deleteSrcGlobal);
					this->orthoProject.setDeleteOrthoHugeFiles(this->deleteOrthoHugGlobal);
				}

				break;

				}
	case 2:		{
					if (this->readOrthoProject)
					{

						if (this->xmlTag[this->tagDepth].CompareNoCase("PROJECT_NAME"))
						{
							this->projectName = ""; 
							this->stringReader = &this->projectName;
							this->read = readString;
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("ORTHO_SETTINGS"))
						{
							bool bicubic,useAnchor;
							int maxDist;
							double gsd;
							
							// parse local settings
							this->parseAttributeListOrthoSettings(attributes,bicubic,useAnchor,maxDist,gsd);

							// save in current project
							this->orthoProject.setUseBicubicInterpolation(bicubic);
							this->orthoProject.setUseAnchorPoints(useAnchor);
							this->orthoProject.setMaxDistAnchorPoints(maxDist);
							this->orthoProject.setGSD(gsd);
							
						}
						else if (this->xmlTag[this->tagDepth].CompareNoCase("PROJECT_SETTINGS"))
						{
							bool save,deleteSrc,deleteHUG;
							// parse local settings
							this->parseAttributeListProjectSettings(attributes,save,deleteSrc,deleteHUG);
							this->orthoProject.setSaveProject(save);
							this->orthoProject.setDeleteSrcHugeFiles(deleteSrc);
							this->orthoProject.setDeleteOrthoHugeFiles(deleteHUG);

						}
					}
				}
	}
	this->tagDepth++;

}

void CORTHOXMLDataHandler::parseAttributeListOrthoSettings(const AttributeList& attributes,bool& bicubic, bool& anchor,int& maxDistAnchor,double& gsd)const
{
	for (unsigned int i=0; i < attributes.getLength(); i++)
	{
		CCharString aName = XMLString::transcode(attributes.getName(i));
		CCharString aValue = XMLString::transcode(attributes.getValue(i));

		if (aName.CompareNoCase("interpolationMethod"))
		{
			if (aValue.CompareNoCase("BILINEAR"))
				bicubic = false;
			else if (aValue.CompareNoCase("BICUBIC"))
				bicubic = true;
		}
		else if (aName.CompareNoCase("useAnchorPoints"))
		{
			if (aValue.CompareNoCase("TRUE"))
				anchor = true;
			else if (aValue.CompareNoCase("FALSE"))
				anchor = false;
		}
		else if (aName.CompareNoCase("maxDistanceAnchorPoints"))
		{
			maxDistAnchor = atoi(aValue.GetChar());
		}
		else if (aName.CompareNoCase("gsd"))
		{
			gsd = atof(aValue.GetChar());
		}
	}

}
void CORTHOXMLDataHandler::parseAttributeListProjectSettings(const AttributeList& attributes, bool& saveProject,bool& deleteSrc, bool& deleteOrthoHUG) const
{
	for (unsigned int i=0; i < attributes.getLength(); i++)
	{
		CCharString aName = XMLString::transcode(attributes.getName(i));
		CCharString aValue = XMLString::transcode(attributes.getValue(i));

		if (aName.CompareNoCase("saveProject"))
		{
			if (aValue.CompareNoCase("TRUE"))
				saveProject = true;
			else if (aValue.CompareNoCase("FALSE"))
				saveProject = false;
		}
		else if (aName.CompareNoCase("delete_original_hugefiles"))
		{
			if (aValue.CompareNoCase("TRUE"))
				deleteSrc = true;
			else if (aValue.CompareNoCase("FALSE"))
				deleteSrc = false;
		}
		else if (aName.CompareNoCase("delete_ortho_hugefiles"))
		{
			if (aValue.CompareNoCase("TRUE"))
				deleteOrthoHUG = true;
			else if (aValue.CompareNoCase("FALSE"))
				deleteOrthoHUG = false;
		}

	}

}