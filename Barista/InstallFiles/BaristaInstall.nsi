;--------------------------------
;General defines and settings
;--------------------------------
;!define TRIAL_VERSION

!define PRODUCT_NAME "Barista"
!define PRODUCT_VERSION "3.1"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "BaristaSetup${PRODUCT_VERSION}.exe"


InstallDir "$PROGRAMFILES\Barista"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
ShowInstDetails show
ShowUnInstDetails show


;--------------------------------
;Includes
;--------------------------------
; allows us to use if, elseif ...
!include "LogicLib.nsh"

; allows us to change the look of the installer
!include "MUI2.nsh"



;--------------------------------
;Variables

 Var MUI_TEMP
 Var STARTMENU_FOLDER



;--------------------------------
; MUI Settings
;--------------------------------
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

;Start Menu Folder Page Configuration
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Barista" 
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Barista"

; Welcome page
!insertmacro MUI_PAGE_WELCOME 

!define MUI_LICENSEPAGE_RADIOBUTTONS 
!insertmacro MUI_PAGE_LICENSE src/licence.txt

!insertmacro MUI_PAGE_COMPONENTS

; Directory page
!insertmacro MUI_PAGE_DIRECTORY

!insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_FOLDER

; Instfiles page
!insertmacro MUI_PAGE_INSTFILES

; Finish page
!insertmacro MUI_PAGE_FINISH

; Language files
!insertmacro MUI_LANGUAGE "English"


; !insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_COMPONENTS
!insertmacro MUI_UNPAGE_INSTFILES




;--------------------------------
;section "Barista"
;--------------------------------

Section "!Barista Program Files" SEC01
  Call checkUser
  
  SetShellVarContext all
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer

  File "src\Barista.exe"
  File "src\BaristaBatch.exe"
  File "src\baristaBatch.dtd"
  File "src\Barista.ico"
  File "src\Barista.chm"
  File "src\BaristaManual.pdf"
  File "src\splash.png"
  File "src\CRCSILogo.png"  
  File "src\cv210.dll"
  File "src\cxcore210.dll"
  File "src\Qt5Core.dll"
  File "src\Qt5OpenGL.dll"
  File "src\Qt5Gui.dll"
  File "src\Qt5Widgets.dll"
  File "src\gdal200.dll"
  File "src\libexpat.dll"
  File "src\NCSEcw.dll"
  File "src\ANN.dll"
  File "src\xerces-c_3_1.dll"
  File "src\vraniml.dll"
  File "src\vrutils.dll"
  File "src\cameraFile.ini"

  SetOutPath "$INSTDIR\platforms"
  SetOverwrite ifnewer
  File "src\platforms\qwindows.dll"
  
  ;SetOutPath "$INSTDIR\plugins\imageformats"
  ;SetOverwrite ifnewer  
  ;File "src\plugins\imageformats\qjpeg4.dll"
  ;File "src\plugins\imageformats\qmng4.dll"
  ;File "src\plugins\imageformats\qsvg4.dll"
  
  SetOutPath "$INSTDIR\Microsoft.VC120.CRT"
  SetOverwrite ifnewer
  File "src\Microsoft.VC120.CRT\msvcp120.dll"
  File "src\Microsoft.VC120.CRT\msvcr120.dll"
  File "src\Microsoft.VC120.CRT\vccorlib120.dll"
  
  SetOutPath "$INSTDIR"
 
 !insertmacro MUI_STARTMENU_WRITE_BEGIN Application 
  CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
  

  CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Barista.lnk" "$INSTDIR\Barista.exe" "" "$INSTDIR\Barista.ico"
  CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\BaristaManual Online.lnk" "$INSTDIR\Barista.chm" "" ""
  CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\BaristaManual Printed.lnk" "$INSTDIR\BaristaManual.pdf" "" ""

  CreateShortCut "$DESKTOP\Barista.lnk" "$INSTDIR\Barista.exe" "" "$INSTDIR\Barista.ico"
  CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\uninst.exe"
 !insertmacro MUI_STARTMENU_WRITE_END 

  
  WriteRegStr HKCR ".bar" "" "BaristaFile"
  WriteRegStr HKCR "BaristaFile" "" "Barista Project File"
  WriteRegStr HKCR "BaristaFile\shell" "" "open"
  WriteRegStr HKCR "BaristaFile\DefaultIcon" "" "$INSTDIR\Barista.ico"
  WriteRegStr HKCR "BaristaFile\shell\open\command" "" \
    '$INSTDIR\Barista.exe "%1"'
   
   
SectionEnd

;--------------------------------
;section registry 
;--------------------------------



Section -Post
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\Barista.ico"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
SectionEnd


Section -un.Post
  DeleteRegKey  "${PRODUCT_UNINST_ROOT_KEY}" "${PRODUCT_UNINST_KEY}" 
SectionEnd




;--------------------------------
;checkUser function
;--------------------------------

!macro checkUser un
  Function ${un}checkUser
 	 
 	 ClearErrors
 	 UserInfo::GetName
 	 Pop $0
 	 UserInfo::GetAccountType
 	 Pop $1
 	 ${If} $1 == "User"
		MessageBox MB_OK 'User "$0" is just a regular user'
		Abort "Can't uninstall Barista without administor rights!"
 	 ${Elseif} $1 == "Guest"
		MessageBox MB_OK 'User "$0" is a guest'
		Abort "Can't uninstall Barista without administor rights!"
 	 ${EndIf}
  FunctionEnd
!macroend

!insertmacro checkUser ""
!insertmacro checkUser "un."




;--------------------------------
;uninstall sections for barista
;--------------------------------


Section  "!un.Barista Program Files" un.SEC01

  Call un.checkUser

  SetShellVarContext all
 
  Delete "$INSTDIR\uninst.exe"
  Delete "$INSTDIR\Barista.exe"
  Delete "$INSTDIR\BaristaBatch.exe"
  Delete "$INSTDIR\baristaBatch.dtd"
  Delete "$INSTDIR\Barista.ico"
  Delete "$INSTDIR\Barista.chm"
  Delete "$INSTDIR\BaristaManual.pdf"
  Delete "$INSTDIR\splash.png"
  Delete "$INSTDIR\CRCSILogo.png"  
  
  
  Delete "$INSTDIR\Qt5Core.dll"
  Delete "$INSTDIR\Qt5Gui.dll"
  Delete "$INSTDIR\Qt5OpenGL.dll"
  Delete "$INSTDIR\Qt5Widgets.dll"
  Delete "$INSTDIR\gdal200.dll"
  Delete "$INSTDIR\cv210.dll"
  Delete "$INSTDIR\cxcore210.dll"
  Delete "$INSTDIR\libexpat.dll"
  Delete "$INSTDIR\NCSEcw.dll"
  Delete "$INSTDIR\ANN.dll"
  Delete "$INSTDIR\xerces-c_3_1.dll"
  Delete "$INSTDIR\vraniml.dll"
  Delete "$INSTDIR\vrutils.dll"  
  Delete "$INSTDIR\cameraFile.ini"
  
  Delete "$INSTDIR\platforms\qwindows.dll"
  RMDir "$INSTDIR\platforms"
  
  Delete "$INSTDIR\Microsoft.VC120.CRT\msvcp120.dll"
  Delete "$INSTDIR\Microsoft.VC120.CRT\msvcr120.dll"
  Delete "$INSTDIR\Microsoft.VC120.CRT\vccorlib120.dll"
  
  RMDir "$INSTDIR\Microsoft.VC120.CRT"
  
  !insertmacro MUI_STARTMENU_GETFOLDER "Application" $MUI_TEMP
  
   Delete "$SMPROGRAMS\$MUI_TEMP\Uninstall.lnk"
   Delete "$SMPROGRAMS\$MUI_TEMP\BaristaManual Online.lnk"
   Delete "$SMPROGRAMS\$MUI_TEMP\BaristaManual Printed.lnk"
   Delete "$SMPROGRAMS\$MUI_TEMP\Barista.lnk"
  
   StrCpy $MUI_TEMP "$SMPROGRAMS\$MUI_TEMP"
   startMenuDeleteLoop:
     ClearErrors
     RMDir $MUI_TEMP
     GetFullPathName $MUI_TEMP "$MUI_TEMP\.."    
     IfErrors startMenuDeleteLoopDone   
     StrCmp $MUI_TEMP $SMPROGRAMS startMenuDeleteLoopDone startMenuDeleteLoop	
   startMenuDeleteLoopDone: 
  

  DeleteRegKey /ifempty HKCU "Software\Barista"
  
  Delete "$DESKTOP\Barista.lnk"

  RMDir "$INSTDIR"

  Delete "$APPDATA\$PRODUCT_NAME\Barista.ini"
  RMDir "$APPDATA\$PRODUCT_NAME"
  

  SetAutoClose false
SectionEnd


;--------------------------------
;Descriptions for the installer sections
;--------------------------------

 ;Language strings
LangString DESC_SEC01 ${LANG_ENGLISH} "Installs the Barista software package on your computer."

 ;Assign language strings to sections
 !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SEC01} $(DESC_SEC01)
 !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Descriptions for the uninstaller sections
;--------------------------------

LangString DESC_UNSEC01 ${LANG_ENGLISH} "Uninstalls the Barista software package on your computer."

!insertmacro MUI_UNFUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${un.SEC01} $(DESC_UNSEC01)
!insertmacro MUI_UNFUNCTION_DESCRIPTION_END
