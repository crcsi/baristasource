XCOPY /D /Y ..\bin\Release\Win32\ANN.dll           			.\src
XCOPY /D /Y ..\bin\Release\Win32\Barista.chm       			.\src
XCOPY /D /Y ..\bin\Release\Win32\Barista.exe       			.\src
XCOPY /D /Y ..\bin\Release\Win32\Barista.ico       			.\src
XCOPY /D /Y ..\bin\Release\Win32\vraniml.dll       			.\src 
XCOPY /D /Y ..\bin\Release\Win32\vrutils.dll       			.\src
XCOPY /D /Y ..\bin\Release\Win32\gdal200.dll        		.\src
XCOPY /D /Y ..\bin\Release\Win32\cv210.dll         			.\src
XCOPY /D /Y ..\bin\Release\Win32\cxcore210.dll     			.\src
XCOPY /D /Y ..\bin\Release\Win32\libexpat.dll      			.\src
XCOPY /D /Y ..\bin\Release\Win32\NCSEcw.dll      			.\src
XCOPY /D /Y ..\bin\Release\Win32\xerces-c_3_1.dll  			.\src
XCOPY /D /Y ..\bin\Release\Win32\Qt5Core.dll       			.\src
XCOPY /D /Y ..\bin\Release\Win32\Qt5Gui.dll        			.\src
XCOPY /D /Y ..\bin\Release\Win32\Qt5OpenGL.dll     			.\src
XCOPY /D /Y ..\bin\Release\Win32\Qt5Widgets.dll     		.\src
XCOPY /D /Y ..\bin\Release\Win32\BaristaManual.pdf 			.\src
XCOPY /D /Y ..\bin\Release\Win32\baristaBatch.dtd   		.\src
XCOPY /D /Y ..\bin\Release\Win32\BaristaBatch.exe   		.\src

if not exist .\src\platforms mkdir .\src\platforms
XCOPY /D /Y ..\bin\Release\Win32\platforms\qwindows.dll   	.\src\platforms

pause
