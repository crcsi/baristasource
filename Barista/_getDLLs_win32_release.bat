@echo off
set qtdir=%1

Xcopy /Y ..\external\ann\ann_1.1.2\Win32\Release\ANN.dll								.\bin\release\Win32
Xcopy /Y ..\external\gdal\gdal-2.0.2\Win32\bin\gdal200.dll								.\bin\release\Win32
Xcopy /Y ..\external\Expat\Expat-2.0.1\Win32\libexpat.dll								.\bin\release\Win32
XCOPY /Y ..\external\ecwsdk5.2.1\redistributable\vc120\Win32\NCSEcw.dll					.\bin\release\Win32
XCOPY /Y ..\external\xerces\xerces-c-3.1.2\Build\Win32\VC12\Release\xerces-c_3_1.dll	.\bin\release\Win32
XCOPY /Y ..\external\VRaniML\SDK\dll\Win32\vrutils.dll									.\bin\release\Win32
XCOPY /Y ..\external\VRaniML\SDK\dll\Win32\vraniml.dll									.\bin\release\Win32
XCOPY /Y %qtdir%\bin\Qt5Gui.dll															.\bin\release\Win32
XCOPY /Y %qtdir%\bin\Qt5Core.dll														.\bin\release\Win32
XCOPY /Y %qtdir%\bin\Qt5OpenGL.dll														.\bin\release\Win32
XCOPY /Y %qtdir%\bin\Qt5Widgets.dll														.\bin\release\Win32
XCOPY /Y ..\external\openCV\OpenCV-2.1.0\Win32\Release\cv210.dll   						.\bin\release\Win32
XCOPY /Y ..\external\openCV\OpenCV-2.1.0\Win32\Release\cxcore210.dll    				.\bin\release\Win32

XCOPY /Y .\support\splash.png															.\bin\release\Win32
XCOPY /Y .\support\Barista.chm															.\bin\release\Win32
XCOPY /Y .\support\BaristaManual.pdf													.\bin\release\Win32
XCOPY /Y .\support\CRCSILogo.png     													.\bin\release\Win32
XCOPY /Y .\support\cameraFile.ini    													.\bin\release\Win32
XCOPY /Y .\BaristaBatch\baristaBatch.dtd      											.\bin\release\Win32
XCOPY /Y .\Barista.ico      															.\bin\release\Win32

if not exist .\bin\release\Win32\platforms mkdir .\bin\release\Win32\platforms
XCOPY /Y %qtdir%\plugins\platforms\qwindows.dll											.\bin\release\Win32\platforms