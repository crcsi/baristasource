#include "stdio.h"
#include "ALSPoint.h"

short CALSPoint::getEchoIndex(short echo) const
{
	if ((echo <= 0) || (this->dim <= 3)) return 0;

	return 3;
};

double CALSPoint::getFirstMinusLast() const
{
	return (this->coordinates[2] - this->coordinates[this->dim - 1]);
};

void CALSPoint::addEcho(double x, double y, double z, short intensity)
{
	int nEchos = this->nEchos();
	double *newCoords = new double[this->dim + 3];
	int i;
	for (i = 0; i < dim; ++i) newCoords[i] = this->coordinates[i];
	newCoords[i] = x; ++i;
	newCoords[i] = y; ++i;
	newCoords[i] = z; 
	delete [] this->coordinates;
	this->coordinates = newCoords;

	short *newIntensities = new short[nEchos + 1];

	for (i = 0; i < nEchos; ++i) newIntensities[i] = this->intensities[i];
	newIntensities[i] = intensity;
	delete [] this->intensities;
	this->intensities = newIntensities;
	this->dim += 3;
}

bool CALSPoint::dump(FILE* fp) const
{
	short nEchos = this->nEchos();
	bool ret = (fwrite(&nEchos, sizeof(short), 1, fp) == 1);
	double d = this->dot;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	ret &= (fwrite(this->coordinates, sizeof(double), this->dim, fp) == this->dim);
	ret &= (fwrite(this->intensities, sizeof(short), nEchos, fp) == nEchos);

    return ret;
};

bool CALSPoint::read(FILE* fp)
{
	short nEchos = 1;
	bool ret = (fread(&nEchos, sizeof(short), 1, fp) == 1);
	if (ret)
	{
		delete[] this->coordinates;
		delete[] this->intensities;
		this->dim = nEchos * 3;
		this->coordinates = new double[this->dim];
		this->intensities = new short[nEchos];
		double d;
		ret &= (fread(&d, sizeof(double), 1, fp) == 1);
		this->dot = d;
		ret &= (fread(this->coordinates, sizeof(double), this->dim, fp) == this->dim);
		ret &= (fread(this->intensities, sizeof(short), nEchos, fp) == nEchos);
	}
	return ret;
};

CALSPoint &CALSPoint::operator =(const CALSPoint &p)
{
	if (this != &p) 
	{
		int echos = p.nEchos();
		if (this->dim != p.dim)
		{
			short *newIntensities = new short[echos];
			delete [] this->intensities;
			this->intensities = newIntensities;
		}

		for (int i = 0; i < echos; ++i) this->intensities[i] = p.intensities[i];
			

		CPointBase::operator =(p);
	}

	return *this;
};

void CALSPoint::copy(const CALSPoint &p)
{
	this->id = p.id;
	this->dot = p.dot;

	if (this->dim < p.dim)
	{
		int nEchos = p.dim / 3;
		delete[] this->coordinates;
		delete[] this->intensities;
		this->intensities = new short[nEchos];
		this->coordinates = new double[p.dim];
		this->dim = p.dim;
		memcpy(this->coordinates, p.coordinates, p.dim  * sizeof(double));
		memcpy(this->intensities, p.intensities, nEchos * sizeof(short));
	}
	else if (this->dim == p.dim)
	{
		int nEchos = p.dim / 3;
		memcpy(this->coordinates, p.coordinates, p.dim  * sizeof(double));
		memcpy(this->intensities, p.intensities, nEchos * sizeof(short));
	}
	else
	{
		memcpy(this->coordinates, p.coordinates, 3  * sizeof(double));
		this->intensities[0] = p.intensities[0];

		memcpy(&(this->coordinates[3]), p.coordinates, 3  * sizeof(double));
		this->intensities[1] = p.intensities[0];
	}
	
	

};
