#include <float.h>
#include "newmat.h"
#include "ALSPointCloud.h"
#include "ALSData.h"


//================================================================================

CALSPointCloudBuffer::CALSPointCloudBuffer(int AnnDim) : CPointCloud(3, AnnDim),pointVec(0)
{
	this->pMin.x = this->pMin.y = this->pMin.z = FLT_MAX;
	this->pMax.x = this->pMax.y = this->pMax.z = -FLT_MAX;
}

//================================================================================
	   
CALSPointCloudBuffer::~CALSPointCloudBuffer()
{
	this->clear();
};
 
//================================================================================
	  
bool CALSPointCloudBuffer::initKDTree()
{
	if ((this->annDim < 2) || (this->annDim > this->dimension)) return false;

	if (this->kdTree) delete this->kdTree;
	this->kdTree = 0;
		
	this->kdTree = new ANNkd_tree(this->annPoints, this->npoints, annDim, 5);

	if (!this->kdTree) return false;
	
	return true;
};

//================================================================================
	  
bool CALSPointCloudBuffer::setAnnDim(int anndim)
{
	if (this->annDim != anndim)
	{
		if ((anndim < 2) || (anndim > this->dimension)) return false;

		if (this->kdTree) delete this->kdTree;
		this->kdTree = 0;
		
		this->annDim = anndim;
		this->kdTree = new ANNkd_tree(this->annPoints, this->npoints, annDim, 5);

		if (!this->kdTree) return false;
	}

	return true;
};

//================================================================================

CALSPoint *CALSPointCloudBuffer::addPoint(double x, double y, double z, int intensity)
{
	if (this->kdTree) 
	{
		delete this->kdTree;
		this->kdTree = 0;
	}


	if (this->npoints >= this->capacity)
	{
		int oldCap = this->capacity; 
		this->capacity += 1000;

		CALSPoint **newPointVec = new CALSPoint *[this->capacity];
		
		if (this->pointVec)
		{
			for (int i = 0; i < oldCap; ++i)
			{
				newPointVec[i] = this->pointVec[i]; 
				this->pointVec[i] = 0;
			}
			for (int i = oldCap; i < this->capacity; ++i)
			{
				newPointVec[i] = new CALSPoint(); 
			}
			delete [] this->pointVec;
		}
		else
		{
			for (int i = 0; i < this->capacity; ++i)
			{
				newPointVec[i] = new CALSPoint(); 
			} 
		}

		this->pointVec = newPointVec;

		if (this->annPoints) delete [] this->annPoints;

		this->annPoints = new ANNpoint [this->capacity];
			
		for (int i = 0; i < this->capacity; ++i)
		{
			this->annPoints[i] = this->pointVec[i]->getAnnPoint(0);
		}
	}

	CALSPoint *p = this->pointVec[this->npoints];
	++this->npoints;
	

	(*p)[0] = x;	
	(*p)[1] = y;	
	(*p)[2] = z;
	
	if (x < this->pMin.x) this->pMin.x = x;
	if (x > this->pMax.x) this->pMax.x = x;
	if (y < this->pMin.y) this->pMin.y = y;
	if (y > this->pMax.y) this->pMax.y = y;
	if (z < this->pMin.z) this->pMin.z = z;
	if (z > this->pMax.z) this->pMax.z = z;

	return p;
};

//================================================================================

int CALSPointCloudBuffer::get(CALSDataSet &als, const C3DPoint &min, const C3DPoint &max, int level)
{
	this->refsys = als.getReferenceSystem();

	if (this->kdTree) 
	{
		delete this->kdTree;
		this->kdTree = 0;
	}

	this->pMin.x = this->pMin.y = this->pMin.z = FLT_MAX;
	this->pMax.x = this->pMax.y = this->pMax.z = -FLT_MAX;
	
	int newCapacity = this->capacity;
	this->npoints = als.getPointCloud(min, max, this->pointVec, newCapacity, 2048, level);
	
	if (this->npoints > 0)
	{
		if (this->capacity != newCapacity)
		{
			this->capacity = newCapacity;
			if (this->annPoints) delete [] this->annPoints;

			this->annPoints = new ANNpoint [this->capacity];
			
			for (int i = 0; i < this->capacity; ++i)
			{
				this->annPoints[i] = this->pointVec[i]->getAnnPoint(0);
			}
		}

		for (int i = 0; i < this->npoints; ++i)
		{
			double x = (*this->pointVec[i])[0];
			double y = (*this->pointVec[i])[1];
			double z = (*this->pointVec[i])[2];
			if (x < this->pMin.x) this->pMin.x = x;
			if (x > this->pMax.x) this->pMax.x = x;
			if (y < this->pMin.y) this->pMin.y = y;
			if (y > this->pMax.y) this->pMax.y = y;
			if (z < this->pMin.z) this->pMin.z = z;
			if (z > this->pMax.z) this->pMax.z = z;
		}

		this->kdTree = new ANNkd_tree(this->annPoints, this->npoints, annDim, 5); // 5 for the best splitting suggested by the author of ANN library
	}
	

	return this->npoints;
};
	  
void CALSPointCloudBuffer::clear()
{
	this->resetTriangulation();

	if (this->pointVec)
	{
		for (int i = 0; i < this->capacity; ++i) 
		{
			if (this->pointVec[i]) delete this->pointVec[i];
		}

		delete [] this->pointVec;
		this->pointVec = 0;
		this->npoints = 0;
		this->capacity = 0;
	}

	CPointCloud::clear();
};

//================================================================================
	  
void CALSPointCloudBuffer::interpolateRaster(float *buf, int width, int height, 
											 int bufwidth,
											 double x0, double y0, double dx, 
											 double dy, int N, int echo,
											 eInterpolationMethod method, 
											 float &zmin, float &zmax)
{
	zmin = FLT_MAX;
	zmax = -FLT_MAX;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);

		ANNpoint queryPt = annAllocPt(annDim);
	
		queryPt[1] = y0;

		for (int r = 0; r < height; ++r, queryPt[1] -= dy)
		{
			long idx = r * bufwidth;
			long idxmax = idx + bufwidth;
			queryPt[0] = x0;

			for (; idx < idxmax; ++idx, queryPt[0] += dx)
			{
				this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
				double Z = -9999.0;
				
				if (method == eMovingTiltedPlane) Z = interpolateMovingPlaneEcho(N, echo, queryPt);
				else if (method == eMovingHzPlane) Z = interpolateMovingHorizontalPlaneEcho(N, echo);

				buf[idx] = (float) Z;
				
				if (Z < zmin)  zmin = (float) Z;
				if (Z > zmax)  zmax = (float) Z;
			}
		}

		annDeallocPt(queryPt);
	}
};

//================================================================================

void CALSPointCloudBuffer::interpolateRaster(float *buf, int width, int height, int bufwidth,
											 double x0, double y0, double dx, double dy, 
											 int N, int echo, eInterpolationMethod method, 
											 float &zmin, float &zmax,
											 double maxDist)
{
	zmin = FLT_MAX;
	zmax = -FLT_MAX;


	if (this->isInitialised())
	{
		unsigned long iMax = height * bufwidth; 

		if (this->npoints < 1)
		{
			for (unsigned long i = 0 ; i < iMax; i++)
			{
				buf[i] = (float) -9999.0f;
			}
		}
		else
		{
			if (N > this->npoints)  N = this->npoints;
			this->initQueryPoints(N);

			ANNdist sqRad = maxDist * maxDist;
			ANNpoint queryPt = annAllocPt(annDim);

			queryPt[1] = y0;

			for (unsigned long iMin = 0 ; iMin < iMax; iMin += bufwidth, queryPt[1] -= dy)
			{

				unsigned long idx = iMin;
				unsigned long idxmax = idx + bufwidth;
				queryPt[0] = x0;

				if (method == eMovingTiltedPlane) 
				{
					for (; idx < idxmax; ++idx, queryPt[0] += dx)
					{

						this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
						for (int k = 0; k < N; ++k)
						{
							if (this->nnDists[k] > sqRad) this->nnDists[k] = ANN_DIST_INF;
						}

						double Z = interpolateMovingPlaneEcho(N, echo, queryPt);
					
						buf[idx] = (float) Z;
						
						if (Z < zmin)  zmin = (float) Z;
						if (Z > zmax)  zmax = (float) Z;
					}
				}
				else if (method == eMovingHzPlane) 
				{
					for (; idx < idxmax; ++idx, queryPt[0] += dx)
					{
	
						this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
						for (int k = 0; k < N; ++k)
						{
							if (this->nnDists[k] > sqRad) this->nnDists[k] = ANN_DIST_INF;
						}

						double Z = interpolateMovingHorizontalPlaneEcho(N, echo);
						
						buf[idx] = (float) Z;
						
						if (Z < zmin)  zmin = (float) Z;
						if (Z > zmax)  zmax = (float) Z;
					}
				}
			}
		
			annDeallocPt(queryPt);
		}
	}

};

double CALSPointCloudBuffer::interpolateMovingHorizontalPlaneEcho(int N, int echo)
{
	double Z = -9999.0;
	double sumW = 0.0;
	double sumZ = 0.0f;

	for (int i = 0; i < N; ++i)
	{
		if ((this->nnIdx[i] != ANN_NULL_IDX) && (this->nnDists[i] != ANN_DIST_INF))
		{
			CALSPoint *p = this->pointVec[this->nnIdx[i]];
			double w = this->nnDists[i];
			if (w < 0.01f) w = 0.01f;
			w = 1.0 / w;
			sumZ += (*p)[p->getEchoIndex(echo) + 2] * w;
			sumW += w;
		};
	}

	if (sumW > FLT_EPSILON) Z = sumZ / sumW;

	return Z;
};

double CALSPointCloudBuffer::interpolateMovingPlaneEcho(int N, int echo, const ANNpoint &queryPt)
{
	double Z = -9999.0;
	SymmetricMatrix NMat(3);
	Matrix Atl(3,1);
	NMat = 0;
	Atl = 0;

	double zmin = FLT_MAX;
	double zmax = -FLT_MAX;

	int usedPts = 0;

	for (int i = 0; i < N; ++i)
	{
		if ((this->nnIdx[i] != ANN_NULL_IDX) && (this->nnDists[i] != ANN_DIST_INF))
		{
			CALSPoint *p = this->pointVec[this->nnIdx[i]];
			double w = this->nnDists[i];
			if (w < 0.01f) w = 0.01f;
			w = 1.0 / w;

			int echoidx = p->getEchoIndex(echo);
			double x = (*p)[echoidx]     - queryPt[0];
			double y = (*p)[echoidx + 1] - queryPt[1];
			double z = (*p)[echoidx + 2];
			double xw = x * w;
			double yw = y * w;
			
			NMat.element(0,0) += w; NMat.element(0,1) += xw;      NMat.element(0,2) += yw; 
			                        NMat.element(1,1) += x * xw;  NMat.element(1,2) += x * yw; 
			                                                      NMat.element(2,2) += y * yw; 

			Atl.element(0,0) += z *  w;
			Atl.element(1,0) += z * xw;
			Atl.element(2,0) += z * yw;

			++usedPts;
			if (z < zmin) zmin = z;
			if (z > zmax) zmax = z;	
		};
	}
	
	if (usedPts < 3)
	{
		if (NMat.element(0,0) > FLT_EPSILON) Z = Atl.element(0,0) / NMat.element(0,0);
	}
	else
	{
		Try
		{
			Matrix Qxx = NMat.i();
			Atl =  Qxx * Atl;
			Z = Atl.element(0,0);
			if (Z < zmin) Z = zmin;
			else if (Z > zmax) Z = zmax;
		}		 
		CatchAll 
		{
			// do nothing, but leave Z at -9999
		}
	}

	return Z;
};

void CALSPointCloudBuffer::interpolateIntensity(unsigned short*buf, int width, int height, 
											    double x0, double y0, double dx, 
												double dy, int N, int echo, 
												eInterpolationMethod method)
{
	if (this->isInitialised())
	{
		this->initQueryPoints(N);

		ANNpoint queryPt = annAllocPt(annDim);

		queryPt[1] = y0;

		for (int r = 0; r < height; ++r, queryPt[1] -= dy)
		{
			long idx = r * width;
			long idxmax = idx + width;
			queryPt[0] = x0;

			for (; idx < idxmax; ++idx, queryPt[0] += dx)
			{
				this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
				double sumW = 0.0;
				double sumI = 0.0f;

				for (int i = 0; i < N; ++i)
				{
					if (this->nnIdx[i] != ANN_NULL_IDX)
					{
						CALSPoint *p = this->pointVec[this->nnIdx[i]];
						double w = this->nnDists[i];
						if (w < 0.01f) w = 0.01f;
						w = 1.0 / w;
						sumI += p->intensity(echo) * w;
						sumW += w;
					};
				}

				if (fabs(sumW) < FLT_EPSILON) buf[idx] = 0;
				else
				{
					double I = sumI / sumW;
					buf[idx] = (unsigned short) I;
				}
			}
		}

		annDeallocPt(queryPt);
	}
};

void CALSPointCloudBuffer::interpolateIntensity(unsigned short*buf, int width, int height, 
											    double x0, double y0, double dx, 
												double dy, int N, int echo, 
												eInterpolationMethod method, double maxDist)
{
	if (this->isInitialised())
	{
		this->initQueryPoints(N);
		ANNpoint queryPt = annAllocPt(annDim);
		ANNdist sqRad = maxDist * maxDist;
		
		queryPt[1] = y0;

		for (int r = 0; r < height; ++r, queryPt[1] -= dy)
		{
			long idx = r * width;
			long idxmax = idx + width;
			queryPt[0] = x0;

			for (; idx < idxmax; ++idx, queryPt[0] += dx)
			{
				this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
				for (int k = 0; k < N; ++k)
				{
					if (this->nnDists[k] > sqRad) this->nnDists[k] = ANN_DIST_INF;
				}

				double sumW = 0.0;
				double sumI = 0.0f;

				for (int i = 0; i < N; ++i)
				{
					if ((this->nnIdx[i] != ANN_NULL_IDX) && (this->nnDists[i] != ANN_DIST_INF))
					{
						CALSPoint *p = this->pointVec[this->nnIdx[i]];
						double w = this->nnDists[i];
						if (w < 0.01f) w = 0.01f;
						w = 1.0 / w;
						sumI += p->intensity(echo) * w;
						sumW += w;
					};
				}

				if (fabs(sumW) < FLT_EPSILON) buf[idx] = 0;
				else
				{
					double I = sumI / sumW;
					buf[idx] = (unsigned short) I;
				}
			}
		}

		annDeallocPt(queryPt);
	}
};

int CALSPointCloudBuffer::getNearestNeighbours(CALSPoint **neighbours, double *sqrDist, int N, const C3DPoint &p)
{
	int ret = 0;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);
		ANNpoint queryPt = annAllocPt(annDim);
		for (int i = 0; i < annDim; ++i) queryPt[i] = p[i];
		
		this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 

		annDeallocPt(queryPt);
		
		
		for (int i = 0; i < N; ++i)
		{
			if (this->nnIdx[i] != ANN_NULL_IDX)
			{
				neighbours[i] = this->pointVec[this->nnIdx[i]];
				sqrDist[i] = sqrt(this->nnDists[i]);
				++ret;
			}
			else
			{
				neighbours[i] = 0;
				sqrDist[i] = 0;
			}
		};

	}

	return ret;
};
 	 
int CALSPointCloudBuffer::getNearestNeighbours(CALSPoint ** neighbours, double *sqrDist, int N, const C3DPoint &p, double maxDist)
{
	int ret = 0;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);
		ANNdist sqRad = maxDist * maxDist;
		ANNpoint queryPt = annAllocPt(annDim);

		for (int i = 0; i < annDim; ++i) queryPt[i] = p[i];
		
		this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
		for (int k = 0; k < N; ++k)
		{
			if (this->nnDists[k] > sqRad) this->nnDists[k] = ANN_DIST_INF;
		}

		annDeallocPt(queryPt);
		
		
		for (int i = 0; i < N; ++i)
		{
			if ((this->nnIdx[i] != ANN_NULL_IDX) && (this->nnDists[i] != ANN_DIST_INF))
			{
				neighbours[i] = this->pointVec[this->nnIdx[i]];
				sqrDist[i] = sqrt(this->nnDists[i]);
				++ret;
			}
			else
			{
				neighbours[i] = 0;
				sqrDist[i] = 0;
			}
		};
	}

	return ret;
};
 
void CALSPointCloudBuffer::writeXYZ(const char *filename, int echo) const
{
	FILE *fp = fopen(filename, "wt");
	if (fp)
	{
		for (int i = 0; i < this->npoints; ++i)
		{
			CALSPoint *p = this->pointVec[i];
			int idx = p->getEchoIndex(echo);
			fprintf( fp, "%12.2lf %12.2lf %8.2lf\n", (*p)[idx], (*p)[idx + 1], (*p)[idx + 2] );
		}

		fclose(fp);
	}
};
