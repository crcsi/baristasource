#include "newmat.h"
#include "GenericPointCloud.h"
#include "Prediction.h"


CLinearPrediction::CLinearPrediction(CPointCloud *pPointcloud, int N, double SigmaZ, int CoordIdx, double deltaD0) : 
		Vzz(-1.00), sigmaZ(SigmaZ), pcloud(pPointcloud), covarPar(1.0),
		InterpolationPoints(0), nInterpolationPoints(0), nnDists(0),
		coordIdx(CoordIdx)
{
	if (this->pcloud)
	{
		if (this->pcloud->getNpoints() > 1)
		{
			this->initInterpolationPoints(N);
			this->initTrend();
			this->initCovarPars(deltaD0);
		}
	}
}

//================================================================================

CLinearPrediction::~CLinearPrediction()
{
	this->clear();
};

//================================================================================

void CLinearPrediction::clear()
{
	this->pcloud = 0;

	if (this->InterpolationPoints) 
	{
		delete [] this->InterpolationPoints;
		delete [] this->nnDists;
		this->InterpolationPoints = 0;
		this->nnDists = 0;
		this->nInterpolationPoints = 0;
	}
};

//================================================================================

void CLinearPrediction::initInterpolationPoints(int N)
{
	if (N > this->nInterpolationPoints)
	{
		if (this->InterpolationPoints) delete [] this->InterpolationPoints;
		if (this->nnDists) delete [] this->nnDists;

		this->InterpolationPoints = new ANNpoint [N];
		this->nnDists = new ANNdist[N]; // near neighbor distances
		this->nInterpolationPoints = N;
	}
};
 
//================================================================================

void CLinearPrediction::initTrend()
{
	double sumx = 0.0, sumy = 0.0, sumz = 0.0;
	double sumxx = 0.0, sumxy = 0.0, sumxz = 0.0,sumyy = 0.0, sumyz = 0.0;
	double x, y, z;

	for (int i = 0; i < this->pcloud->getNpoints(); ++i)
	{
		ANNpoint p = this->pcloud->getPt(i);
		x = p[0];
		y = p[1];
		z = p[this->coordIdx];
		sumx  += x;      sumy  += y;      sumz  += z;
		sumxx += x * x;  sumxy += x * y;  sumxz += x * z;
		                 sumyy += y * y;  sumyz += y * z;
	}

	double oneByN = 1.0 / double(this->pcloud->getNpoints());
	sumxx -= sumx * sumx * oneByN;  sumxy -= sumx * sumy * oneByN;  sumxz -= sumx * sumz * oneByN;
	                                sumyy -= sumy * sumy * oneByN;  sumyz -= sumy * sumz * oneByN;

	this->Cog.x = sumx * oneByN;
	this->Cog.y = sumy * oneByN;
	this->Cog.z = sumz * oneByN;

	double det = 1.0 / (sumxx * sumyy - sumxy * sumxy);
	double qxx =  sumyy * det;
	double qxy = -sumxy * det;
	double qyy =  sumxx * det;

	this->TrendParameters.x = sumxz * qxx + sumyz * qxy;
	this->TrendParameters.y = sumxz * qxy + sumyz * qyy;
	this->TrendParameters.z = 0.0;
};

//================================================================================

void CLinearPrediction::initCovarPars(double deltaD0)
{
	double deltaD = deltaD0;
	double dx = this->pcloud->getPmax().x - this->pcloud->getPmin().x;
	double dy = this->pcloud->getPmax().y - this->pcloud->getPmin().y;
	double lmax = sqrt(dx * dx + dy * dy);

	vector<unsigned long> hist(int(floor(lmax / deltaD)) + 1, 0);
	vector<double> sumzizj(hist.size(), 0.0);

	vector<double> zred(this->pcloud->getNpoints());

	this->Vzz = 0.0;

	double z;

	ANNpoint pi, pj;

	for (int i = 0; i < this->pcloud->getNpoints(); ++i)
	{
		pi = this->pcloud->getPt(i);
		z = pi[this->coordIdx] - this->getTrend(pi[0], pi[1]);
		zred[i] = z;
		this->Vzz += z * z;
	}

	this->Vzz /= double(this->pcloud->getNpoints());

	if (this->Vzz > this->sigmaZ * this->sigmaZ) 
		this->Vzz -= this->sigmaZ * this->sigmaZ;
	else this->Vzz -= 1.0;
	if (this->Vzz < 0.0) this->Vzz = 1.0;

	double xi, yi, zi, zj;
	unsigned int d;

	for (int i = 0; i < this->pcloud->getNpoints(); ++i)
	{
		pi = this->pcloud->getPt(i);
		xi = pi[0];
		yi = pi[1];
		zi = zred[i];
		for (int j = i + 1; j < this->pcloud->getNpoints(); ++j)
		{
			pj = this->pcloud->getPt(j);
			dx = xi - pj[0];
			dy = yi - pj[1];
			d = (unsigned int)(floor(sqrt(dx * dx + dy * dy) / deltaD));
			if (d >= hist.size()) d = hist.size() - 1;

			zj = zred[j];
			hist[d]++;
			sumzizj[d] += zi * zj;
		}
	}

	int N = 0;
	int nMin = 2 * this->pcloud->getNpoints();

	unsigned int iDelta = 0;

	for (; iDelta < hist.size() && N < nMin; ++iDelta)
	{
		N += hist[iDelta];
	}

	deltaD = double(iDelta) * deltaD0;
	vector<unsigned long> histNew(int(floor(lmax / deltaD)) + 1, 0);
	vector<double> sumzizjNew(histNew.size(), 0.0);

	double c, w, sumc = 0.0, sumw = 0.0, dC = deltaD * 0.5;

	for (unsigned int i = 0, inew = 0; inew < histNew.size() ; ++inew, dC += deltaD)
	{
		for (unsigned int j = 0; j < iDelta && i < hist.size(); ++j, ++i)
		{
			histNew[inew]    += hist[i];
			sumzizjNew[inew] += sumzizj[i];
		}

		if (histNew[inew] > 0)
		{
			sumzizjNew[inew] /= double(histNew[inew]);
			c = dC / sqrt(log(abs(this->Vzz / sumzizjNew[inew])));
			w = 1.0 / (dC * dC);
			sumc += w * c;
			sumw += w;
		}
	}

	this->covarPar = sumc / sumw;
	this->covarPar = 1.0 / (this->covarPar * this->covarPar);
};

//================================================================================

double CLinearPrediction::getTrend(double x, double y) const
{
	x -= this->Cog.x;
	y -= this->Cog.y;

	return (x * TrendParameters.x + y * TrendParameters.y + this->Cog.z);
};

//================================================================================

void CLinearPrediction::interpolateRaster(float *buf, int width, int height, int bufwidth,
							              double x0, double y0, double dx, double dy, 
		                                  float &zmin, float &zmax)
{
	zmin = FLT_MAX;
	zmax = -FLT_MAX;

	if (this->isInitialised())
	{
		SymmetricMatrix C(this->pcloud->getNpoints());				
		Matrix Zmat(this->pcloud->getNpoints(), 1);

		ANNpoint pi, pj;
		double x, y, z, dX, dY;
		for (int i = 0; i < this->pcloud->getNpoints(); ++i)
		{
			pi = this->pcloud->getPt(i);
			x = pi[0];
			y = pi[1];
			z = pi[this->coordIdx];
			C.element(i,i) = this->Vzz;
			Zmat.element(i,0) = z - this->getTrend(x, y);
	
			for (int j = i+1; j < this->pcloud->getNpoints(); ++j)
			{
				pj = this->pcloud->getPt(j);
				dX = pj[0] - x;
				dY = pj[1] - y;
				C.element(i,j) = this->getCovar(dX, dY);
			}
		}

		Matrix W;

		Try 
		{
			W = C.i() * Zmat;
		}
		CatchAll
		{
			int imax = bufwidth * height;
			for (int i = 0; i < imax; ++i) buf[i] = -9999.0f;

			return;
		}
	

		C3DPoint P;

		P.y = y0;

		Matrix ZmatInt(1, this->pcloud->getNpoints()), R;

		for (int r = 0; r < height; ++r, P.y -= dy)
		{
			long idx = r * bufwidth;
			long idxmax = idx + bufwidth;
			P.x = x0;

			for (; idx < idxmax; ++idx, P.x += dx)
			{
				double Z = -9999.0;
	
				for (int i = 0; i < this->pcloud->getNpoints(); ++i)
				{
					pi = this->pcloud->getPt(i);
					x = pi[0];
					y = pi[1];
					dX = P.x - x;
					dY = P.y - y;
					ZmatInt.element(0, i) = this->getCovar(dX, dY);
				}

				R = ZmatInt * W;
				Z = R.element(0,0) + this->getTrend(P.x, P.y);

				buf[idx] = (float) Z;
				
				if (Z < zmin)  zmin = (float) Z;
				if (Z > zmax)  zmax = (float) Z;
			}		
		}
	}
};

//================================================================================

void CLinearPrediction::interpolateRaster(float *buf, int width, int height, int bufwidth,
							              double x0, double y0, double dx, double dy, 
		                                  int N, float &zmin, float &zmax)
{
	zmin = FLT_MAX;
	zmax = -FLT_MAX;

	if (this->isInitialised())
	{
		this->initInterpolationPoints(N);

		C3DPoint P;

		P.y = y0;

		for (int r = 0; r < height; ++r, P.y -= dy)
		{
				long idx = r * bufwidth;
				long idxmax = idx + bufwidth;
				P.x = x0;

				for (; idx < idxmax; ++idx, P.x += dx)
				{
					int np = this->pcloud->getNearestNeighbours(this->InterpolationPoints, this->nnDists, N, P); 
					double Z = -9999.0;
					if (np > 0)
					{
						SymmetricMatrix C(np);
						Matrix Zmat(np, 1);
						Matrix Zmat0(1, np);

						double x, y, z, dx, dy;
						for (int i = 0; i < np; ++i)
						{
							x = this->InterpolationPoints[i][0];
							y = this->InterpolationPoints[i][1];
							z = this->InterpolationPoints[i][this->coordIdx];
							C.element(i,i) = this->Vzz;
							Zmat.element(i,0) = z - this->getTrend(x, y);
							dx = P.x - x;
							dy = P.y - y;
							Zmat0.element(0, i) = this->getCovar(dx, dy);

							for (int j = i+1; j < np; ++j)
							{
								dx = this->InterpolationPoints[j][0] - x;
								dy = this->InterpolationPoints[j][1] - y;
								C.element(i,j) = this->getCovar(dx, dy);
							}
						}
						Matrix W = C.i() * Zmat;
						Matrix res = Zmat0 * W;
						Z = res.element(0,0) + this->getTrend(P.x, P.y);
					}

					buf[idx] = (float) Z;
					
					if (Z < zmin)  zmin = (float) Z;
					if (Z > zmax)  zmax = (float) Z;
				}		
		}
	}
};

//================================================================================
