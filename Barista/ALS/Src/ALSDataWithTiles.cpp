#include <strstream>
using namespace std;

#ifdef WIN32
#include <io.h>
#else
#include <fcntl.h>
#endif

#include <float.h>
#include "SystemUtilities.h"
#include "ALSDataWithTiles.h"
#include "ALSPointCloud.h"

CALSDataSetWithTiles::CALSDataSetWithTiles(const char *filename, const CReferenceSystem &ref, bool open, const C2DPoint &delta) : 
		CALSDataSet(filename, ref, open), nTilesX(0), nTilesY(0), TileSize(1.0,1.0)
{	
#ifdef WIN32
	offset = 0;
#else
	offset.__pos = 0;
#endif	
	
	this->pointDist = delta;	 
}

//================================================================================

CALSDataSetWithTiles::CALSDataSetWithTiles(const char *filename, const CReferenceSystem &ref, 
										   CALSPoint **pointList, int &npoints, const C2DPoint &delta): 
		CALSDataSet(filename, ref, false), nTilesX(0), nTilesY(0), TileSize(1.0,1.0)
{
#ifdef WIN32
	offset = 0;
#else
	offset.__pos = 0;
#endif

	this->pointDist = delta;
	this->init(pointList, npoints, false);
}

//================================================================================

bool CALSDataSetWithTiles::init(vector<CALSDataSet *> &datasetVec)
{
	this->pointDist = datasetVec[0]->getPointDist();

	bool OK = true;

	if (datasetVec.size() > 0)
	{
		this->fptr = fopen(this->getFileNameChar(), "w+b");
	
		if (this->fptr)
		{
			this->hasTime = datasetVec[0]->getHasTime();
			this->hasIntensity = datasetVec[0]->getHasIntensity();

			bool OK = this->mergeDatasets(datasetVec);
			if (OK) 
			{
				if (this->listener)
				{
					CCharString str = this->listener->getTitleString();
					str += ": sublevel";
					this->listener->setTitleString(str.GetChar());
				}

				OK = this->initSublevels();
			}
			if (OK) OK = this->finishWriting(this->fptr);
			if (OK) this->initTilingStatistics();

			if (!OK) 
			{
				this->destroy();
				nTilesX = nTilesY = 0;
			}
		}
		else OK = false;
	}
	else OK = false;
	return OK;
}

//================================================================================

CALSDataSet *CALSDataSetWithTiles::getClone() const
{
	CALSDataSet *data = new CALSDataSetWithTiles(this->getFileNameChar(), this->getReferenceSystem(), true, this->pointDist);

	return data;
};

//================================================================================

CALSDataSetWithTiles::~CALSDataSetWithTiles()
{
}

//================================================================================

void CALSDataSetWithTiles::initTileStructure(C3DPoint &Pmin, C3DPoint &Pmax, int npoints, bool changeMinMax)
{
	this->nTilesX = int (floor(sqrt(double(npoints) / 1024))) + 1;
	this->nTilesY = this->nTilesX;
	int nTiles = nTilesX * nTilesY;

	TileSize.x = floor((Pmax.x - Pmin.x) / double(nTilesX)) + 1.0;
	TileSize.y = floor((Pmax.y - Pmin.y) / double(nTilesY)) + 1.0;

	if (changeMinMax)
	{
		Pmin.x = floor(Pmin.x);
		Pmin.y = floor(Pmin.y);
		Pmax.x = floor(Pmax.x) + 1.0;
		Pmax.y = floor(Pmax.y) + 1.0;
	}

	this->nTilesX = int (floor((Pmax.x - Pmin.x) / TileSize.x)  + 1);
	this->nTilesY = int (floor((Pmax.y - Pmin.y) / TileSize.y) + 1);

	this->tileVec.resize(this->nTilesX * this->nTilesY);

	int TileIndex = 0;

	for (int i = 0; i < nTilesY; ++i)
	{
		C2DPoint TileMin(Pmin.x, Pmax.y - double (i + 1) * TileSize.y);
		C2DPoint TileMax(Pmin.x + TileSize.x, Pmax.y - double (i) * TileSize.y);
		for (int j = 0; j < nTilesX; ++j, ++TileIndex, TileMin.x += TileSize.x , TileMax.x += TileSize.x )
		{
			CALSTile *tile = new CALSTile(TileMin,TileMax,0);
			this->tileVec[TileIndex] = tile;
		}
	}
};

//================================================================================

bool CALSDataSetWithTiles::init(CALSPoint **pointvec, int &npoints, bool initSublevels)
{
	bool ret = true;
	
	if (this->fptr) this->close();
	this->destroy();

	if (npoints < 1) ret = false;
	else
	{
		CALSPoint *p = pointvec[0];
		double x = (*p)[0];
		double y = (*p)[1];
		double z = (*p)[2];
		this->pMin.x = x;
		this->pMin.y = y;
		this->pMin.z = z;
		this->pMax = this->pMin;

		for (int i = 1; i < npoints; ++i)
		{
			p = pointvec[i];
			x = (*p)[0];
			y = (*p)[1];
			z = (*p)[2];
			if (this->pMin.x > x) this->pMin.x = x;
			if (this->pMax.x < x) this->pMax.x = x;
			if (this->pMin.y > y) this->pMin.y = y;
			if (this->pMax.y < y) this->pMax.y = y;
			if (this->pMin.z > z) this->pMin.z = z;
			if (this->pMax.z < z) this->pMax.z = z;
		}

		this->initTileStructure(this->pMin, this->pMax, npoints, true);

		int nCurr = 0;

		for (int k = 0; k < npoints; ++k) 
		{		
			CALSPoint *p = pointvec[k];

			int index = this->getTileIndex((*p)[0], (*p)[1]);
			if ((index < 0) || (index > int(this->tileVec.size())))
			{
				pointvec[nCurr] = pointvec[k];
				++nCurr;
			}
			else
			{
				this->tileVec[index]->addPoint(pointvec[k]);
				pointvec[k] = 0;
			}
		}

		npoints = nCurr;

		this->initTilingStatistics();

		this->fptr = fopen(this->getFileNameChar(), "w+b");
	
		if (!this->fptr) ret = false;
		else ret = this->dump(initSublevels);
	}

	return ret;
};

//================================================================================

int CALSDataSetWithTiles::getTileIndex(double x, double y) const
{
	x -= this->pMin.x;
	y -= this->pMax.y;
	x /= this->TileSize.x;
	y /= -this->TileSize.y;
	return int(floor(y)) * this->nTilesX + int(floor(x));
};

//================================================================================
	 
C2DPoint CALSDataSetWithTiles::getTileSize() const
{
	return this->TileSize;
};

//================================================================================

void CALSDataSetWithTiles::destroy()
{
	for (unsigned int i = 0; i < this->tileVec.size(); ++i) 
	{
		delete this->tileVec[i]; 
		this->tileVec[i] = 0;
	}

	this->tileVec.clear();

	for (unsigned int i = 0; i < this->subLevels.size(); ++i) 
	{
		delete this->subLevels[i]; 
		this->subLevels[i] = 0;
	}

	this->subLevels.clear();
};
  
//================================================================================

void CALSDataSetWithTiles::clear()
{
	for (unsigned int i = 0; i < this->tileVec.size(); ++i)
	{
		this->tileVec[i]->deletePoints();
	}
};
	 
//================================================================================
 
void CALSDataSetWithTiles::clear(int level)
{
	if (level < int(this->subLevels.size()))
	{
		this->subLevels[level]->clear();
	}
};

//================================================================================

void CALSDataSetWithTiles::addTile(CALSTile *tile)
{
	this->tileVec.push_back(tile);
};

//================================================================================

void CALSDataSetWithTiles::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName relPath;
	CFileName fileSaveName = this->fileName;

	if (!Path.IsEmpty())
	{
		relPath = CSystemUtilities::RelPath(this->fileName, Path);
		fileSaveName.SetPath(relPath);
	}

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("ALS_data_filename", fileSaveName);
	token = pStructuredFileSection->addToken();
	token->setValue("ALS_hasTime", this->hasTime);
	token = pStructuredFileSection->addToken();
	token->setValue("ALS_hasIntensity", this->hasIntensity);
	token = pStructuredFileSection->addToken();
	token->setValue("ALS_PulseMode", this->pulsMode);


	CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->refsys.serializeStore(child);
};

//================================================================================

void CALSDataSetWithTiles::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);
	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName CWD;
	if (!Path.IsEmpty())
	{
		CWD = CSystemUtilities::getCwd();
		CSystemUtilities::chDir(Path);
	}

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("ALS_data_filename"))
			this->fileName = token->getValue();
		else if(key.CompareNoCase("ALS_hasTime"))
			this->hasTime = token->getBoolValue();
		else if(key.CompareNoCase("ALS_hasIntensity"))
			this->hasIntensity = token->getBoolValue();
		else if(key.CompareNoCase("ALS_PulseMode"))
			this->pulsMode = (eALSPulseModeType) token->getIntValue();
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refsys.serializeRestore(child);
        }
    }

	if (!Path.IsEmpty())
	{
		this->fileName = CSystemUtilities::AbsPath(this->fileName);
		CSystemUtilities::chDir(CWD);
	}

	CALSDataSet::retrieve();
};
//================================================================================

bool CALSDataSetWithTiles::dump(bool initSublevels)
{
	this->initStatistics();
	bool ret = this->writeHeader(this->fptr);
	for (unsigned int i = 0; i < this->tileVec.size(); ++i)
	{
		ret &= this->tileVec[i]->dump(this->fptr);
	}

	this->finishWriting(this->fptr);

	return ret;
};

//================================================================================

void CALSDataSetWithTiles::initStatistics()
{
	this->nPoints = 0;

	for (int i = 0; i < this->nTiles(); ++i)
	{
		this->nPoints += tileVec[i]->getNPoints();
	}
};

//================================================================================

bool CALSDataSetWithTiles::writeHeader(FILE* pFile) 
{
	bool ret = true;
	fgetpos(pFile, &this->offset);
	const char *hdr = "BARISTA_ALST__1";
	ret = (fwrite(hdr, sizeof(char), 15, pFile) == 15);
	ret &= (fwrite(&this->nPoints, sizeof(int), 1, pFile) == 1);
	ret &= (fwrite(&this->nTilesX, sizeof(int), 1, pFile) == 1);
	ret &= (fwrite(&this->nTilesY, sizeof(int), 1, pFile) == 1);
	double d = this->TileSize.x;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);
	d = this->TileSize.y;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);

	int nSubLevels = this->subLevels.size();
	ret &= (fwrite(&nSubLevels, sizeof(int), 1, pFile) == 1);
	ret &= (fwrite(&this->subLevelOffset, sizeof(fpos_t), 1, pFile) == 1);

	d = this->pMin.x;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);
	d = this->pMin.y;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);
	d = this->pMin.z;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);
	d = this->pMax.x;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);
	d = this->pMax.y;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);
	d = this->pMax.z;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);
	d = this->pointDist.x;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);
	d = this->pointDist.y;
	ret &= (fwrite(&d, sizeof(double), 1, pFile) == 1);
	
	for (int i = 0; i < this->nTiles(); ++i)
	{
		this->tileVec[i]->dumpHdr(pFile);
	}

	return ret;
};

//================================================================================

bool CALSDataSetWithTiles::finishWriting(FILE* pFile)
{
	fgetpos(pFile, &this->subLevelOffset);
	this->initStatistics();
	fsetpos(pFile, &this->offset);
	bool ret = this->writeHeader(pFile);
	this->close();
	return ret;
};

//================================================================================

bool CALSDataSetWithTiles::retrieveHdr(FILE* FP, int &nSubLevels)
{
	char hdr[17];
	hdr[0] = '\0';
	bool ret = (fread(&hdr, sizeof(char), 15, FP) == 15);
	hdr[15] = '\0';

	if (strcmp(&hdr[0],"BARISTA_ALSTILE") == 0) ret = true;
	else if (strcmp(&hdr[0],"BARISTA_ALST__1") == 0) ret = true;
	else ret = false;

	ret &= (fread(&this->nPoints, sizeof(int), 1, FP) == 1);	
	ret &= (fread(&this->nTilesX, sizeof(int), 1, FP) == 1);
	ret &= (fread(&this->nTilesY, sizeof(int), 1, FP) == 1);
	
	double d;
	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->TileSize.x = d;
	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->TileSize.y = d;

	ret &= (fread(&nSubLevels, sizeof(int), 1, FP) == 1);
	ret &= (fread(&this->subLevelOffset, sizeof(fpos_t), 1, FP) == 1);

	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->pMin.x = d;
	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->pMin.y = d;
	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->pMin.z = d;
	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->pMax.x = d;
	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->pMax.y = d;
	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->pMax.z = d;
	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->pointDist.x = d;
	ret &= (fread(&d, sizeof(double), 1, FP) == 1);
	this->pointDist.y = d;

	return ret;
}

//================================================================================

bool CALSDataSetWithTiles::retrieveOld(FILE* FP, int &nSubLevels)
{
	fsetpos(FP, &this->offset);

	bool ret = retrieveHdr(FP, nSubLevels);

	if (ret)
	{
		int ntiles = this->nTilesX * this->nTilesY;
		vector<fpos_t> tileOffsets(ntiles);
		fpos_t  toffset;

		for (int i = 0; i < ntiles; ++i) 
		{
			ret &= (fread(&toffset, sizeof(fpos_t), 1, FP) == 1);
			tileOffsets[i] = toffset;
		}

		this->tileVec.resize(ntiles);

		for (int i = 0; i < ntiles; ++i) 
		{
			CALSTile *t = new CALSTile;			
			this->tileVec[i] = t;
			toffset = tileOffsets[i];
 			fsetpos(FP, &toffset);
			t->retrieve(FP);
		}
	}

	return ret;
};


//================================================================================
	  
bool CALSDataSetWithTiles::retrieve(FILE* FP)
{	
	fsetpos(FP, &this->offset);


	char hdr[17];
	hdr[0] = '\0';
	bool ret = (fread(&hdr, sizeof(char), 15, FP) == 15);
	hdr[15] = '\0';

	if ((strcmp(&hdr[0],"BARISTA_ALSTILE") == 0) && (this->offset < 1))
	{
		if (!this->convertFormat()) return false;		
		FP = this->fptr;
		fsetpos(FP, &this->offset);
	};

	int nSubLevels;

	fsetpos(FP, &this->offset);

	ret &= retrieveHdr(FP, nSubLevels);

	if (!this->nTiles() && ret)
	{
		int ntiles = this->nTilesX * this->nTilesY;
		this->tileVec.resize(ntiles);

		for (int i = 0; i < ntiles; ++i) 
		{
			CALSTile *t = new CALSTile;			
			this->tileVec[i] = t;
			t->retrieveHdr(FP);
		}

		CALSDataSetWithTiles *prevLevel = this;

		float fact = 0.5;
		for (int i = 0; i < nSubLevels; ++i)
		{
			CALSDataSetWithTiles *data = new CALSDataSetWithTiles(this->fileName.GetChar(), this->refsys, false, this->pointDist * fact);
			subLevels.push_back(data);
			data->offset = prevLevel->subLevelOffset;
			data->retrieve(this->fptr);
			prevLevel = data;
			fact *= 0.5;
		}
	}

	this->initTilingStatistics();
	this->initMinCommonLevel();

	return ret;
};

//================================================================================
	  
bool CALSDataSetWithTiles::convertFormat()
{
	bool ret = true;

	CCharString fnScr = this->fileName + ".scr";

	fclose(this->fptr);

	CSystemUtilities::copy(this->fileName, fnScr);

	FILE *fpSCR = fopen(fnScr.GetChar(), "r+b");

	this->fptr = fopen(this->getFileNameChar(), "w+b");

	if (!this->fptr || !fpSCR) ret = false;
	else
	{
		int nSubLevels;
		
		ret = retrieveOld(fpSCR, nSubLevels);
		
		this->writeHeader(this->fptr);

		int ntiles = this->nTilesX * this->nTilesY;

		for (int i = 0; i < ntiles; ++i) 
		{
			this->tileVec[i]->read(fpSCR);
			this->tileVec[i]->dump(this->fptr);
			this->tileVec[i]->deletePoints();
		}
		
        fpos_t slo = this->subLevelOffset;
		fgetpos(this->fptr, &this->subLevelOffset);
		fsetpos(this->fptr, &this->offset);

		this->writeHeader(this->fptr);

		CALSDataSetWithTiles *prevLevel = this;

		float fact = 0.5;
		for (int i = 0; i < nSubLevels; ++i)
		{
			CALSDataSetWithTiles *data = new CALSDataSetWithTiles(this->fileName.GetChar(), this->refsys, false, this->pointDist * fact);
			data->offset = slo;
			fsetpos(fpSCR, &slo);
			
			ret = data->retrieveOld(fpSCR, nSubLevels);
			slo = data->subLevelOffset;
			subLevels.push_back(data);
			
			int ntilesData = data->nTilesX * data->nTilesY;
			fsetpos(this->fptr, &prevLevel->subLevelOffset);

			data->writeHeader(this->fptr);

			for (int i = 0; i < ntilesData; ++i) 
			{
				data->tileVec[i]->read(fpSCR);
				data->tileVec[i]->dump(this->fptr);
				data->tileVec[i]->deletePoints();
			}

			fgetpos(this->fptr, &data->subLevelOffset);
			fsetpos(this->fptr, &prevLevel->subLevelOffset);
			data->writeHeader(this->fptr);

			prevLevel = data;
			fact *= 0.5;
		}

		fsetpos(this->fptr, &this->offset);

		this->writeHeader(this->fptr);

		for (int i = 0; i < nSubLevels; ++i) 
		{
			delete subLevels[i];
		}

		subLevels.clear();

		for (int i = 0; i < ntiles; ++i) 
		{
			delete this->tileVec[i];
		}

		this->tileVec.clear();
	}

	

	fclose(this->fptr);
	this->fptr = fopen(this->getFileNameChar(), "r+b");

	fclose(fpSCR);
	CSystemUtilities::del(fnScr);

	return ret;
};

//================================================================================

void CALSDataSetWithTiles::clearTile(int index) 
{
	if (index < this->nTiles())
	{
		this->tileVec[index]->deletePoints();
	}
};

//================================================================================

void CALSDataSetWithTiles::clearTile(int index, int level) 
{
	if (level == 0) this->clearTile(index);
	else if (level <= int(this->subLevels.size()))
	{
		this->subLevels[level-1]->clear(index);
	}

};

//================================================================================

void CALSDataSetWithTiles::readTile(int index, FILE *fp)
{
	fpos_t lOff =  this->tileVec[index]->getOffset();
	fsetpos(fp, &lOff);
	this->tileVec[index]->read(fp);
};

//================================================================================

void CALSDataSetWithTiles::getminMaxTileIndices(const C3DPoint &pmin, const C3DPoint &pmax, 
		                                        int &rowMin, int &rowMax, 
												int &colMin, int &colMax) const
{
	double diff = this->pMax.y - pmax.y;
	rowMin = (diff < 0) ? 0 : int(floor(diff / this->TileSize.y)) - 1;
	if (rowMin < 0) rowMin = 0;
	
	diff = this->pMax.y - pmin.y;
	rowMax = (diff < 0) ? 0 : int(floor(diff / this->TileSize.y)) + 1;
	if (rowMax >= this->nTilesY) rowMax =  this->nTilesY - 1;

	diff = pmin.x - this->pMin.x;
	colMin = (diff < 0) ? 0 : int(floor(diff / this->TileSize.x)) - 1;
	if (colMin < 0) colMin = 0;
	
	diff = pmax.x - this->pMin.x;
	colMax = (diff < 0) ? 0 : int(floor(diff / this->TileSize.x)) + 1;
	if (colMax >= this->nTilesX) colMax =  this->nTilesX - 1;
};

//================================================================================
 
int CALSDataSetWithTiles::getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, int level)
{
	if (level > 0)
	{
		level--;
		if (level >= int(this->subLevels.size())) level = int(subLevels.size()) - 1;
		if (level < 0) level = 0;
		else
		{
			return this->subLevels[level]->getPointCloud(pmin, pmax, pointvec, 0);
		}
	}

	bool wasOpen = (this->fptr != 0);

	if (!wasOpen)
	{
		this->fptr = fopen(this->getFileNameChar(), "r+b");
	
		if (!this->fptr) return 0;
	}

	int ncloudPoints = 0;
	int ncloudCapacity = 2048;
	pointvec = new CALSPoint *[ncloudCapacity];
	memset(pointvec, 0, ncloudCapacity * sizeof(CALSPoint *));

	int rowMin, rowMax, colMin, colMax;
	getminMaxTileIndices(pmin, pmax, rowMin, rowMax, colMin, colMax);

	for (int iTile = rowMin; iTile <= rowMax; ++iTile)
	{
		for (int jTile = colMin; jTile <= colMax; ++jTile)
		{
			CALSTile *t = this->tileVec[iTile * this->nTilesX + jTile];
			
			if (t->overlap(pmin, pmax))
			{
				bool OK = true;
				if (!t->isInitialised()) OK = t->read(this->fptr);
		
				if (OK) 
				{
					for (int iPt = 0; iPt < t->getNPoints(); ++iPt)
					{
						CALSPoint *p = (*t)[iPt];
						if (((*p)[0] >= pmin.x) && ((*p)[1] >= pmin.y) && 
							((*p)[0] <= pmax.x) && ((*p)[1] <= pmax.y))
						{
							CALSPoint *pNew = new CALSPoint(*p);
							if (ncloudPoints + 1 >= ncloudCapacity)
							{
								ncloudCapacity += 2048;
								CALSPoint ** newPts = new CALSPoint *[ncloudCapacity];
								memset(newPts, 0, ncloudCapacity * sizeof(CALSPoint *));
								memcpy(newPts, pointvec, ncloudPoints * sizeof(CALSPoint *));
								delete [] pointvec;
								pointvec = newPts;
							}

							pointvec[ncloudPoints] = pNew;
							++ncloudPoints;
						}
					}
				}
			}

			t->deletePoints();
		}
	}

	if (!ncloudPoints)
	{
		delete [] pointvec;
		pointvec = 0;
	}
	else
	{
		CALSPoint ** newPts = new CALSPoint *[ncloudPoints];
		memcpy(newPts, pointvec, ncloudPoints * sizeof(CALSPoint *));
		delete [] pointvec;						
		pointvec = newPts;
	}

	if (!wasOpen) this->close();

	return ncloudPoints;
};

//================================================================================

int CALSDataSetWithTiles::getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, 
										int &capacity, int dCapacity, int level)
{
	if (level > 0)
	{
		level--;
		if (level >= int(this->subLevels.size())) level = int(subLevels.size()) - 1;
		if (level < 0) level = 0;
		else
		{
			return this->subLevels[level]->getPointCloud(pmin, pmax, pointvec, capacity, dCapacity, 0);
		}
	}

	bool wasOpen = (this->fptr != 0);

	if (!wasOpen)
	{
		this->fptr = fopen(this->getFileNameChar(), "r+b");
	
		if (!this->fptr) return 0;
	}

	int ncloudPoints = 0;
	int nEchos = this->getMaxEchos();

	if (!capacity)
	{
		pointvec = new CALSPoint *[dCapacity];
		capacity = dCapacity;
		
		for (int i = 0; i < capacity; ++i)
		{
			pointvec[i] = new CALSPoint(nEchos);
		}
	}

	int rowMin, rowMax, colMin, colMax;
	getminMaxTileIndices(pmin, pmax, rowMin, rowMax, colMin, colMax);

	for (int iTile = rowMin; iTile <= rowMax; ++iTile)
	{
		for (int jTile = colMin; jTile <= colMax; ++jTile)
		{
			CALSTile *t = this->tileVec[iTile * this->nTilesX + jTile];
			
			if (t->overlap(pmin, pmax))
			{
				bool OK = true;
				if (!t->isInitialised()) OK = t->read(this->fptr);
		
				if (OK) 
				{
					for (int iPt = 0; iPt < t->getNPoints(); ++iPt)
					{
						CALSPoint *p = (*t)[iPt];
						if (((*p)[0] >= pmin.x) && ((*p)[1] >= pmin.y) && 
							((*p)[0] <= pmax.x) && ((*p)[1] <= pmax.y))
						{
							if (ncloudPoints + 1 >= capacity)
							{
								capacity += dCapacity;
								CALSPoint ** newPts = new CALSPoint *[capacity];
								int n = 0;
								for (; n < ncloudPoints; ++n) newPts[n] = pointvec[n];
								delete [] pointvec;
								pointvec = newPts;
								for (; n < capacity; ++n)
								{
									pointvec[n] = new CALSPoint(nEchos);
								}
							}

							pointvec[ncloudPoints]->copy(*p);
							
							++ncloudPoints;
						}
					}
				}
			}

			t->deletePoints();
		}
	}

	if (!wasOpen) this->close();

	return ncloudPoints;
};

//================================================================================

void CALSDataSetWithTiles::initMinCommonLevel()
{
	this->minCommonSublevel = this->subLevels.size();
};

//================================================================================
	  
bool CALSDataSetWithTiles::mergeDatasets(vector<CALSDataSet *> &datasetVec)
{
	bool ret = true;

	this->pMin = datasetVec[0]->getPMin();
	this->pMax = datasetVec[0]->getPMax();
	int npoints = datasetVec[0]->getNPoints();

	for (unsigned int i = 1; i < datasetVec.size(); ++i)
	{
		if (this->pMin.x > datasetVec[i]->getPMin().x) this->pMin.x = datasetVec[i]->getPMin().x;
		if (this->pMin.y > datasetVec[i]->getPMin().y) this->pMin.y = datasetVec[i]->getPMin().y;
		if (this->pMin.z > datasetVec[i]->getPMin().z) this->pMin.z = datasetVec[i]->getPMin().z;
		if (this->pMax.x < datasetVec[i]->getPMax().x) this->pMax.x = datasetVec[i]->getPMax().x;
		if (this->pMax.y < datasetVec[i]->getPMax().y) this->pMax.y = datasetVec[i]->getPMax().y;
		if (this->pMax.z < datasetVec[i]->getPMax().z) this->pMax.z = datasetVec[i]->getPMax().z;
		npoints += datasetVec[i]->getNPoints();
	}
		
	initTileStructure (this->pMin, this->pMax, npoints, false);

	ret = this->writeHeader(this->fptr);


	if (ret) 
	{	
		float pfac = float(100.0) / float(this->tileVec.size());

		for (unsigned int i = 0; i < this->tileVec.size(); ++i)
		{
			CALSTile *t = this->tileVec[i];
			C3DPoint tmin(t->getXmin(), t->getYmin(), t->getZmin());
			C3DPoint tmax(t->getXmax(), t->getYmax(), t->getZmax());

			for (unsigned int j = 0; j < datasetVec.size(); ++j)
			{
				CALSPoint **pointvec = 0;
				int npts = datasetVec[j]->getPointCloud(tmin, tmax, pointvec, 0);
				if (npts)
				{
					for (int k = 0; k < npts; ++k) 
					{
						this->tileVec[i]->addPoint(pointvec[k]);
						pointvec[k] = 0;
					}

					delete [] pointvec;
				}
			}

			this->tileVec[i]->dump(this->fptr);
			this->tileVec[i]->deletePoints();
			if (this->listener)
			{
				float perc = float(i) * pfac;
				this->listener->setProgressValue(perc);
			}
		}

		fgetpos(this->fptr, &this->subLevelOffset);
	}
	return ret;
};

//================================================================================

bool CALSDataSetWithTiles::initSublevels()
{
	bool ret = true;
	if (!this->nPoints) this->initStatistics();

	int npts = this->nPoints;

	double fact = 2.0;

	CALSDataSetWithTiles *prevLevel = this;

	CCharString titleString;
	if (this->listener)
	{
		titleString = this->listener->getTitleString();
	}

	while (npts > 10000)
	{
		CALSDataSetWithTiles *data = new CALSDataSetWithTiles(this->fileName.GetChar(), this->refsys, false, this->pointDist * fact);
		subLevels.push_back(data);
		int err = fsetpos(this->fptr, &prevLevel->subLevelOffset);
		data->offset = prevLevel->subLevelOffset;
		data->pMin = this->pMin;
		data->pMax = this->pMax;
		npts /= 4;
		data->initTileStructure (data->pMin, data->pMax, npts, false);
		data->writeHeader(this->fptr);
#ifdef WIN32
		fpos_t offset1 = 0;
#else
		fpos_t offset1;
		offset1.__pos = 0;
#endif
		fgetpos(this->fptr, &offset1);

		if (this->listener)
		{
			ostrstream oss; 

			oss << titleString.GetChar() << subLevels.size() << ends;
			char *z = oss.str();
			this->listener->setTitleString(z);
			delete [] z;
			this->listener->setProgressValue(0.0);
		}

		float pfac = float(100.0) / float(data->tileVec.size());

		CALSPointCloudBuffer pointCloud(2);

		C3DPoint tmin, tmax;
		for (unsigned int i = 0; i < data->tileVec.size(); ++i)
		{
			CALSTile *tile = data->tileVec[i];
			CALSPoint *neighbours[4];
			double sqrDist[4];
			
			tmin.x = tile->getXmin();  tmin.y = tile->getYmin(); tmin.z = tile->getZmin();
			tmax.x = tile->getXmax();  tmax.y = tile->getYmax(); tmax.z = tile->getZmax();

			int nPtsTile = pointCloud.get(*prevLevel, tmin, tmax, 0);

			for (int j = 0; j < nPtsTile; ++j)
			{
				CALSPoint *p = pointCloud.getPoint(j);
				p->id = -1;
			}

			int N = nPtsTile >= 4 ? 4 : nPtsTile;

			double Zlow = FLT_MAX;
			CALSPoint *pLow = 0;

			for (int j = 0; j < nPtsTile; ++j)
			{
				CALSPoint *p = pointCloud.getPoint(j);
				int Nneigh = pointCloud.getNearestNeighbours(neighbours,sqrDist, N, *p);

				for (int k = 0; k < Nneigh; ++k)
				{				
					CALSPoint *pTst = neighbours[k];

					if (pTst->id > 0)
					{					
						pLow = 0; 
						k = Nneigh;
					}					
					else if  ((*pTst)[2] < Zlow)
					{
						Zlow = (*pTst)[2];
						pLow = pTst;
					};
				}

				if (pLow == p)
				{
					p->id = 1;
					CALSPoint *pNew = new CALSPoint(*p);
					tile->addPoint(pNew);
				}
			};

			fsetpos(this->fptr, &offset1);
			tile->dump(this->fptr);
			tile->deletePoints();
			fgetpos(this->fptr, &offset1);

			if (this->listener)
			{
				float perc = float(i) * pfac;
				this->listener->setProgressValue(perc);
			}
		}

		data->subLevelOffset = offset1;
		data->initStatistics();
		fsetpos(this->fptr, &data->offset);
		data->writeHeader(this->fptr);

		npts = data->nPoints;
		prevLevel = data;
		fact *= 2.0;
	}

	if (this->subLevels.size() > 0) fsetpos(this->fptr, &this->subLevelOffset);
	return ret;
};

//================================================================================

bool CALSDataSetWithTiles::readOptechFile(ifstream &inFile, const C2DPoint &offset, long filesize)
{
	bool ret = true;
	if (this->fptr) this->close();
	this->destroy();

	float pfact = float(100.0) / float(filesize);

	int lineLen = 1024;
	char *line = new char[lineLen];
	CALSPoint *p;
	int MaxPoint = 1000000;
	CALSPoint **pointVec = new CALSPoint *[MaxPoint];
	for (int i = 0; i < MaxPoint; ++i) pointVec[i] = 0;

	int pts = 0;

	p = getALSPointOptech(inFile, line, lineLen, offset);
	vector<CALSDataSet *> datasetVec;

	while (p)
	{	
		pointVec[pts] = p;
		++pts;
		if (pts >= MaxPoint)
		{
			ostrstream oss;
			oss << "_scr" << (datasetVec.size() + 1) << ends;
			char *z = oss.str();
			CFileName fn(this->fileName);
			fn.SetFileName(fn.GetFileName() + z);
			delete[] z;

			CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys,pointVec,pts, this->pointDist);
			datasetVec.push_back(pData);
			for (int i = 0; i < pts; ++i) delete pointVec[i];
			pts = 0;
			p = getALSPointOptech(inFile, line, lineLen, offset);
		}
		else p = getALSPointOptech(inFile, line, lineLen, offset);
	}
	
	if (pts)
	{
		ostrstream oss;
		oss << "_scr" << (datasetVec.size() + 1) << ends;
		char *z = oss.str();
		CFileName fn(this->fileName);
		fn.SetFileName(fn.GetFileName() + z);
		delete[] z;

		CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys,pointVec,pts, this->pointDist);
		datasetVec.push_back(pData);
		for (int i = 0; i < pts; ++i) delete pointVec[i];
		pts = 0;
	}
	
	if (datasetVec.size() < 1) ret = false;

	delete [] pointVec;
	
	if (ret)
	{
		this->fptr = fopen(this->getFileNameChar(), "w+b");
	
		if (!this->fptr) ret = false;
		else ret = this->mergeDatasets(datasetVec);
	}

	for (unsigned int i = 0; i < datasetVec.size(); ++i) 
	{
		CFileName fn = datasetVec[i]->getFileNameChar();
		delete datasetVec[i];
		CSystemUtilities::del(fn);
	}

	if (ret) 
	{
		ret = this->initSublevels();
		if (ret) ret = this->finishWriting(this->fptr);
		this->initTilingStatistics();
	}

	if (this->listener)	this->listener->setProgressValue(100.0);

	delete [] line;

	return ret;
};

//================================================================================

bool CALSDataSetWithTiles::readLASFile1(FILE *inFile, const C2DPoint &offset, long filesize)
{
	if (this->fptr) this->close();
	this->destroy();

	CFileName fn(this->fileName);
	fn.SetPath("");
	CCharString titleString = this->listener->getTitleString();


	if (this->listener)
	{
		this->listener->setTitleString(titleString + ": Reading points");
		this->listener->setProgressValue(0.0);
	}

	unsigned char versionMajor, versionMinor, PointDataFormat;						 
	unsigned short HeaderSize, pointDataRecordLength;
	unsigned long pointOffset, NumberOfPointRecords;
	C3DPoint Scale, Offset;

	bool ret = readLASFileHeader(inFile, versionMajor, versionMinor, HeaderSize, pointOffset, 
		                         PointDataFormat, pointDataRecordLength,NumberOfPointRecords, 
								 Scale, Offset);

	if (ret && (versionMajor < 2) && (PointDataFormat < 2) && (NumberOfPointRecords > 0))
	{
		float pfact = float(100.0) / float(NumberOfPointRecords);
		this->pMin.x -= offset.x;
		this->pMin.y -= offset.y;
		this->pMax.x -= offset.x;
		this->pMax.y -= offset.y;
		Offset.x -= offset.x;
		Offset.y -= offset.y;

		if (PointDataFormat == 0) this->hasTime = false;
		else this->hasTime = true;
		this->hasIntensity = true;

		int MaxPoint = NumberOfPointRecords > 1000000 ? 1000000 : NumberOfPointRecords;
		CALSPoint **pointVec = new CALSPoint *[MaxPoint];
		for (int i = 0; i < MaxPoint; ++i) pointVec[i] = 0;

		int pts = 0;

		vector<CALSDataSet *> datasetVec(NumberOfPointRecords / 1000000 + 1, 0);
		int iDataSet = 0;

#ifdef WIN32
		fpos_t offsetFile = pointOffset;
#else
		fpos_t offsetFile;
		offsetFile.__pos = pointOffset;
#endif
		


		fsetpos(inFile, &offsetFile);

#ifdef _DEBUG

		CFileName txtFileName(this->fileName);
		txtFileName += ".txt";
		ofstream txtFile(txtFileName.GetChar());
#endif
		long PulseCount[5];
		for (int i = 0; i < 5; ++i) PulseCount[i] = 0;
		for (unsigned int i = 0; i < NumberOfPointRecords && ret; )
		{	
			CALSPoint *p = (PointDataFormat == 0) ? this->getALSPointLASRecordFormat0(inFile, Scale, Offset) : 
				                                    this->getALSPointLASRecordFormat1(inFile, Scale, Offset);
			if (!p) ret = false;
			else
			{
				int nEchos = p->nEchos();
#ifdef _DEBUG
				txtFile.setf(ios::fixed,ios::floatfield);
				txtFile.precision(6);
				txtFile << p->dot << " ";
				txtFile.precision(2);
				for (int k = 0; k < nEchos; ++k)
				{
					txtFile << p->x(k) << " " <<  p->y(k) << " " <<  p->z(k) << " " << p->intensity(k) << " "; 

				}

				txtFile << "\n";
#endif

				pointVec[pts] = p;
				++pts;

				assert(nEchos <= 5);

				++PulseCount[nEchos-1];
				i += nEchos;

				if (this->listener && ((pts % 10000) == 0))
				{
					float perc = float(i) * pfact;
					this->listener->setProgressValue(perc);
				}

				if (pts >= MaxPoint)
				{
					ostrstream oss;
					oss << "_scr" << (iDataSet + 1) << ends;
					char *z = oss.str();
					CFileName fn(this->fileName);
					fn.SetFileName(fn.GetFileName() + z);
					delete[] z;

					CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys,pointVec,pts, this->pointDist);
					
					if (iDataSet == datasetVec.size()) datasetVec.push_back(pData);
					else  datasetVec[iDataSet] = pData;

					++iDataSet;
					for (int k = 0; k < pts; ++k) delete pointVec[k];
					pData->clear();
					pts = 0;
				}
			}
		}
		
		if (pts)
		{
			ostrstream oss;
			oss << "_scr" << (iDataSet + 1) << ends;
			char *z = oss.str();
			CFileName fn(this->fileName);
			fn.SetFileName(fn.GetFileName() + z);
			delete[] z;

			CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys,pointVec,pts, this->pointDist);
					
			if (iDataSet == datasetVec.size()) datasetVec.push_back(pData);
			else  datasetVec[iDataSet] = pData;

			++iDataSet;

			for (int i = 0; i < pts; ++i) delete pointVec[i];
			pData->clear();
			pts = 0;
		}

		datasetVec.resize(iDataSet);
		delete [] pointVec;
		
		if (datasetVec.size() < 1) ret = false;
		else ret = true;

		
		if (ret)
		{
			if (this->listener)
			{
				this->listener->setTitleString(titleString + ": Merging tiles");
				this->listener->setProgressValue(0.0);
			}

			this->fptr = fopen(this->getFileNameChar(), "w+b");
		
			if (!this->fptr) ret = false;
			else ret = this->mergeDatasets(datasetVec);


			
		}

		for (unsigned int i = 0; i < datasetVec.size(); ++i) 
		{
			CFileName fn = datasetVec[i]->getFileNameChar();
			delete datasetVec[i];
			CSystemUtilities::del(fn);
		}

		if (ret) 
		{
			if (this->listener)
			{
				this->listener->setTitleString(titleString + ": sublevel ");
			}

			ret = this->initSublevels();
			if (ret) ret = this->finishWriting(this->fptr);
			this->initTilingStatistics();
		}

		if (this->listener)	this->listener->setProgressValue(100.0);
	}
	return ret;
};

bool CALSDataSetWithTiles::readASCIIFileWithTime(ifstream &inFile, const C2DPoint &offset, 
										bool multiple, bool timeFirst, long filesize)
{
	bool ret = true;
	if (this->fptr) this->close();
	this->destroy();

	float pfact = float(100.0) / float(filesize);

	int lineLen = 1024;
	char *line = new char[lineLen];
	CALSPoint *p;
	int MaxPoint = 1000000;
	CALSPoint **pointVec = new CALSPoint *[MaxPoint];
	for (int i = 0; i < MaxPoint; ++i) pointVec[i] = 0;

	int pts = 0;

	vector<CALSDataSet *> datasetVec;

	p = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);

	while (p)
	{	
		pointVec[pts] = p;
		++pts;
		if (pts >= MaxPoint)
		{
			ostrstream oss;
			oss << "_scr" << (datasetVec.size() + 1) << ends;
			char *z = oss.str();
			CFileName fn(this->fileName);
			fn.SetFileName(fn.GetFileName() + z);
			delete[] z;

			CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys,pointVec,pts, this->pointDist);
			datasetVec.push_back(pData);
			for (int i = 0; i < pts; ++i) delete pointVec[i];
			pts = 0;
			p = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
		}
		else p = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
	}

	if (pts)
	{
		ostrstream oss;
		oss << "_scr" << (datasetVec.size() + 1) << ends;
		char *z = oss.str();
		CFileName fn(this->fileName);
		fn.SetFileName(fn.GetFileName() + z);
		delete[] z;

		CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys,pointVec,pts, this->pointDist);
		datasetVec.push_back(pData);
		for (int i = 0; i < pts; ++i) delete pointVec[i];
		pts = 0;
	}
	
	if (datasetVec.size() < 1) ret = false;

	delete [] pointVec;

	if (ret)
	{
		this->fptr = fopen(this->getFileNameChar(), "w+b");
	
		if (!this->fptr) ret = false;
		else ret = this->mergeDatasets(datasetVec);
	}

	for (unsigned int i = 0; i < datasetVec.size(); ++i) delete datasetVec[i];

	if (ret) 
	{
		ret = this->initSublevels();
		if (ret) ret = this->finishWriting(this->fptr);
		this->initTilingStatistics();
	}

	if (this->listener)	this->listener->setProgressValue(100.0);

	delete [] line;

	return ret;
};


bool CALSDataSetWithTiles::readASCIIFileWithoutTime(ifstream &inFile, const C2DPoint &offset, 									
										   bool multiple, long filesize)
{
	bool ret = true;
	if (this->fptr) this->close();
	this->destroy();

	float pfact = float(100.0) / float(filesize);

	int lineLen = 1024;
	char *line = new char[lineLen];
	CALSPoint *p;
	int MaxPoint = 1000000;
	CALSPoint **pointVec = new CALSPoint *[MaxPoint];
	for (int i = 0; i < MaxPoint; ++i) 
	{
		pointVec[i] = 0;
	}

	int pts = 0;

	vector<CALSDataSet *> datasetVec;

	p = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);

	while (p)
	{	
		pointVec[pts] = p;
		++pts;
		if (pts >= MaxPoint)
		{
			ostrstream oss;
			oss << "_scr" << (datasetVec.size() + 1) << ends;
			char *z = oss.str();
			CFileName fn(this->fileName);
			fn.SetFileName(fn.GetFileName() + z);
			delete[] z;

			CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys,pointVec,pts, this->pointDist);
			datasetVec.push_back(pData);
			for (int i = 0; i < pts; ++i) delete pointVec[i];
			pts = 0;
			p = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);
		}
		else p = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);
	}
	
	if (pts)
	{
		ostrstream oss;
		oss << "_scr" << (datasetVec.size() + 1) << ends;
		char *z = oss.str();
		CFileName fn(this->fileName);
		fn.SetFileName(fn.GetFileName() + z);
		delete[] z;

		CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys,pointVec,pts, this->pointDist);
		datasetVec.push_back(pData);
		for (int i = 0; i < pts; ++i) delete pointVec[i];
		pts = 0;
	}
	
	if (datasetVec.size() < 1) ret = false;

	delete [] pointVec;

	if (ret)
	{
		this->fptr = fopen(this->getFileNameChar(), "w+b");
	
		if (!this->fptr) ret = false;
		else ret = this->mergeDatasets(datasetVec);
	}

	for (unsigned int i = 0; i < datasetVec.size(); ++i) delete datasetVec[i];

	if (ret) 
	{
		ret = this->initSublevels();
		if (ret) ret = this->finishWriting(this->fptr);
		this->initTilingStatistics();
	}


	if (this->listener)	this->listener->setProgressValue(100.0);

	delete [] line;

	return ret;
};

bool CALSDataSetWithTiles::readToposysFile(ifstream &inFile, const C2DPoint &offset, long filesize)
{
	bool ret = true;
	if (this->fptr) this->close();
	this->destroy();

	float pfact = float(100.0) / float(filesize);

	CALSPoint *p;
	int MaxPoint = 1000000;
	CALSPoint **pointVec = new CALSPoint *[MaxPoint];
	for (int i = 0; i < MaxPoint; ++i) pointVec[i] = 0;

	int pts = 0;
	vector<CALSDataSet *> datasetVec;

	p = getALSPointTopoSys(inFile, offset);

	while (p)
	{	
		pointVec[pts] = p;
		++pts;
		if (pts >= MaxPoint)
		{
			ostrstream oss;
			oss << "_scr" << (datasetVec.size() + 1) << ends;
			char *z = oss.str();
			CFileName fn(this->fileName);
			fn.SetFileName(fn.GetFileName() + z);
			delete[] z;

			CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys, pointVec, pts, this->pointDist);
			datasetVec.push_back(pData);
			for (int i = 0; i < pts; ++i) delete pointVec[i];
			pts = 0;
			p = getALSPointTopoSys(inFile, offset);
		}
		else p = getALSPointTopoSys(inFile, offset);
	}
		
	if (pts)
	{
		ostrstream oss;
		oss << "_scr" << (datasetVec.size() + 1) << ends;
		char *z = oss.str();
		CFileName fn(this->fileName);
		fn.SetFileName(fn.GetFileName() + z);
		delete[] z;

		CALSDataSetWithTiles *pData = new CALSDataSetWithTiles(fn.GetChar(), this->refsys,pointVec,pts, this->pointDist);
		datasetVec.push_back(pData);
		for (int i = 0; i < pts; ++i) delete pointVec[i];
		pts = 0;
	}
	
	if (datasetVec.size() < 1) ret = false;

	delete [] pointVec;

	if (ret)
	{
		this->fptr = fopen(this->getFileNameChar(), "w+b");
	
		if (!this->fptr) ret = false;
		else ret = this->mergeDatasets(datasetVec);
	}

	for (unsigned int i = 0; i < datasetVec.size(); ++i) delete datasetVec[i];

	if (ret) 
	{
		ret = this->initSublevels();
		if (ret) ret = this->finishWriting(this->fptr);
		this->initTilingStatistics();
	}


	if (this->listener)	this->listener->setProgressValue(100.0);


	return ret;	
};

bool CALSDataSetWithTiles::exportXYZ(const CCharString &FileName, int echo, bool exportTime, bool exportI)
{
	bool ret = true;

	bool wasOpen = true;

	if (!this->fptr)
	{
		wasOpen = false;
		this->fptr = fopen(this->getFileNameChar(), "r+b");
	}

	if (this->fptr)
	{
		ofstream outFile(FileName.GetChar());
		if (outFile.good())
		{
			outFile.setf(ios::fixed, ios::floatfield);
		
			for (unsigned int i = 0; i < this->tileVec.size(); ++i)
			{
				CALSTile *t = this->getTile(i);
				t->read(this->fptr);
				
				for(int iPt = 0; iPt < t->getNPoints(); ++iPt)
				{
					
					CALSPoint *p = (*t)[iPt];
					if (exportTime) 
					{
						outFile.precision(7);
						outFile << p->dot << " ";
					}
					outFile.precision(2);
					int ptEcho = echo;
					if (ptEcho >= p->nEchos()) ptEcho = p->nEchos() - 1;
					outFile << p->x(ptEcho) << " " << p->y(ptEcho) << " " << p->z(ptEcho);
					if (exportI) outFile << " " << p->intensity(echo);
					outFile << "\n";
				}				

				t->deletePoints();
			}			
		}
		else ret = false;

		if (!wasOpen) this->close();
	}
	else ret = false;

	return ret;
};
