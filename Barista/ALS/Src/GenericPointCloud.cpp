#include <float.h>
#include "GenericPointCloud.h"
#include "XYZPointArray.h"
#include "XYPointArray.h"
#include "robustWeightFunction.h"
#include "XYZPolyLine.h"
#include "XYPolyLine.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"

CPointCloud::CPointCloud(int dim, int anndim) : 
		npoints(0), kdTree(0), capacity(0), annPoints(0), annDim(anndim), 
		dimension(dim), nQueryPoints(0), annQueryPoints(0), nnIdx(0), nnDists(0), 
		EPSILON(0.000001), lastTriangle(-1), triangles(0), edges(0), 
		nEdges(0), nTriangles(0), firstEdgePerVertex(0)
{
}

//================================================================================

CPointCloud::~CPointCloud()
{
	this->resetTriangulation();

	this->clear();
};

//================================================================================

void CPointCloud::clear()
{
	if (this->kdTree) 
	{
		delete this->kdTree;
		this->kdTree = 0;
	}

	if (this->annPoints) 
	{
		delete [] this->annPoints;
		this->annPoints = 0;
	}


	this->npoints = 0;
	this->capacity = 0;

	if (this->annQueryPoints) 
	{
		delete [] this->annQueryPoints;
		delete [] this->nnIdx;
		delete [] this->nnDists;
		this->annQueryPoints = 0;
		this->nnIdx = 0;
		this->nnDists = 0;
		this->nQueryPoints = 0;
	}
};

//================================================================================

void CPointCloud::initQueryPoints(int N)
{
	if (N > this->nQueryPoints)
	{
		if (this->annQueryPoints) delete [] this->annQueryPoints;
		if (this->nnIdx)   delete [] this->nnIdx;
		if (this->nnDists) delete [] this->nnDists;

		this->annQueryPoints = new ANNpoint [N];
		this->nnIdx   = new ANNidx[N];  // near neighbor indices
		this->nnDists = new ANNdist[N]; // near neighbor distances
		this->nQueryPoints = N;
	}
};
	  
//================================================================================

void CPointCloud::getTriangle(int i, ANNpoint &P1, ANNpoint &P2, ANNpoint &P3) const
{
	P1 = this->annPoints[this->triangles[i].p[0]];
	P2 = this->annPoints[this->triangles[i].p[1]];
	P3 = this->annPoints[this->triangles[i].p[2]];
};
	  
//================================================================================

void CPointCloud::getTriangle(int i, C3DPoint &P1, C3DPoint &P2, C3DPoint &P3) const
{
	ANNpoint p1, p2, p3;
	this->getTriangle(i, p1, p2, p3);
	P1.x = p1[0];   P1.y = p1[1]; 
	P2.x = p2[0];   P2.y = p2[1]; 
	P3.x = p3[0];   P3.y = p3[1]; 

	if (this->dimension > 2) 
	{
		P1.z = p1[2]; 
		P2.z = p2[2];
		P3.z = p3[2]; 
	}
	else
	{
		P1.z = 0.0; 
		P2.z = 0.0; 
		P3.z = 0.0; 
	}
};
	 
//================================================================================
 
CXYPolyLine CPointCloud::getTriangle2D(int i) const
{
	CXYPolyLine poly;
	C2DPoint p1, p2, p3;
	this->getTriangle(i, p1, p2, p3);
	poly.addPoint(p1);
	poly.addPoint(p2);
	poly.addPoint(p3);
	poly.closeLine();

	return poly;
};

//================================================================================
 
CXYZPolyLine CPointCloud::getTriangle3D(int i) const
{
	CXYZPolyLine poly;
	C3DPoint p1, p2, p3;
	this->getTriangle(i, p1, p2, p3);
	poly.addPoint(p1);
	poly.addPoint(p2);
	poly.addPoint(p3);
	poly.closeLine();

	return poly;
};
	  
//================================================================================

void CPointCloud::getTriangle(int i, C2DPoint &P1, C2DPoint &P2, C2DPoint &P3) const
{
	ANNpoint p1, p2, p3;
	this->getTriangle(i, p1, p2, p3);
	P1.x = p1[0];   P1.y = p1[1]; 
	P2.x = p2[0];   P2.y = p2[1]; 
	P3.x = p3[0];   P3.y = p3[1]; 
};

//================================================================================

int CPointCloud::getNearestNeighbours(ANNpointArray &neighbours, double *sqrDist, int N, const C3DPoint &p)
{
	int ret = 0;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);

		ANNpoint queryPt = annAllocPt(annDim);
		for (int i = 0; i < annDim; ++i) queryPt[i] = p[i];
	
		this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 

		annDeallocPt(queryPt);
		
		
		for (int i = 0; i < N; ++i)
		{
			if (this->nnIdx[i] != ANN_NULL_IDX)
			{
				neighbours[i] = this->annPoints[nnIdx[i]];
				sqrDist[i] = this->nnDists[i];
				++ret;
			}
			else
			{
				neighbours[i] = 0;
				sqrDist[i] = 0;
				i = N;
			}
		};
	}

	return ret;
};
 
//================================================================================
	 
int CPointCloud::getNearestNeighbours(ANNpointArray  &neighbours, double *sqrDist, 
									  int N, const C3DPoint &p, double maxDist)
{
	int ret = 0;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);

		ANNdist sqRad = maxDist * maxDist;
		ANNpoint queryPt = annAllocPt(annDim);
		for (int i = 0; i < annDim; ++i) queryPt[i] = p[i];
		
		this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
		for (int k = 0; k < N; ++k)
		{
			if (this->nnDists[k] > sqRad) this->nnDists[k] = ANN_DIST_INF;
		}

		annDeallocPt(queryPt);
		
		for (int i = 0; i < N; ++i)
		{
			if ((this->nnIdx[i] != ANN_NULL_IDX) && (this->nnDists[i] != ANN_DIST_INF))
			{
				neighbours[i] = this->annPoints[this->nnIdx[i]];
				sqrDist[i] = this->nnDists[i];
				++ret;
			}
			else
			{
				neighbours[i] = 0;
				sqrDist[i] = 0;
				i = N;
			}
		};
	}

	return ret;
};
	
//================================================================================
  
double CPointCloud::interpolateMovingHorizontalPlane(const ANNpoint &queryPt, int N, 
													 int CoordInd, double maxDistSqare, 
													 double minsquareDistForWeight,
													 double &minDist)
{
	double val = -9999.0;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);

		this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
		for (int k = 0; k < N; ++k)
		{
			if (this->nnDists[k] > maxDistSqare) this->nnDists[k] = ANN_DIST_INF;
		}
			
		val = interpolateMovingHorizontalPlane(N, CoordInd, minsquareDistForWeight, minDist);
	}

	return val;
};

//================================================================================

double CPointCloud::interpolateMovingHorizontalPlane(const ANNpoint &queryPt, int N, int CoordInd, 
													 double minsquareDistForWeight,
													 double &minDist)
{
	double val = -9999.0;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);
				
		this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
			
		val = interpolateMovingHorizontalPlane(N, CoordInd, minsquareDistForWeight, minDist);
	}

	return val;
};

//================================================================================

double CPointCloud::interpolateMovingPlane(const ANNpoint &queryPt, int N, int CoordInd, double maxDistSqare,
									       double minsquareDistForWeight, double &minDist)
{
	double val = -9999.0;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);

		this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
		for (int k = 0; k < N; ++k)
		{
			if (this->nnDists[k] > maxDistSqare) this->nnDists[k] = ANN_DIST_INF;
		}
			
		val = interpolateMovingPlane(N, CoordInd, queryPt, minsquareDistForWeight, minDist);
	}

	return val;
};

//================================================================================

double CPointCloud::interpolateMovingPlane(const ANNpoint &queryPt, int CoordInd, int N, 
										   double minsquareDistForWeight, double &minDist)
{
	double val = -9999.0;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);
				
		this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
			
		val = interpolateMovingPlane(N, CoordInd, queryPt, minsquareDistForWeight, minDist);
	}

	return val;
};
   
//================================================================================

void CPointCloud::interpolateRaster(float *buf, int width, int height, int bufwidth,
							        double x0, double y0, double dx, double dy, 
		                            int N, eInterpolationMethod method, 
									float &zmin, float &zmax, int CoordInd,
									double minsquareDistForWeight)
{
	zmin = FLT_MAX;
	zmax = -FLT_MAX;

	double minDist;

	if (this->isInitialised())
	{
		this->initQueryPoints(N);

		ANNpoint queryPt = annAllocPt(annDim);

		queryPt[1] = y0;

		if (method == eMovingTiltedPlane) 
		{
			for (int r = 0; r < height; ++r, queryPt[1] -= dy)
			{
				long idx = r * bufwidth;
				long idxmax = idx + bufwidth;
				queryPt[0] = x0;

				for (; idx < idxmax; ++idx, queryPt[0] += dx)
				{
					this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
					double Z = -9999.0;
					Z = interpolateMovingPlane(N, CoordInd, queryPt, minsquareDistForWeight, minDist);
					buf[idx] = (float) Z;
					if (Z > -9999.0)
					{
						if (Z < zmin)  zmin = (float) Z;
						if (Z > zmax)  zmax = (float) Z;
					}
				}
			}
		}
		else if (method == eMovingHzPlane)
		{
			for (int r = 0; r < height; ++r, queryPt[1] -= dy)
			{
				long idx = r * bufwidth;
				long idxmax = idx + bufwidth;
				queryPt[0] = x0;

				for (; idx < idxmax; ++idx, queryPt[0] += dx)
				{
					this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
					double Z = -9999.0;
					Z = interpolateMovingHorizontalPlane(N, CoordInd, minsquareDistForWeight, minDist);

					buf[idx] = (float) Z;
					if (Z > -9999.0)
					{
						if (Z < zmin)  zmin = (float) Z;
						if (Z > zmax)  zmax = (float) Z;
					}
				}
			}
		}

		annDeallocPt(queryPt);
	}
};

//================================================================================

void CPointCloud::interpolateRaster(float *buf, int width, int height, int bufwidth,
							        double x0, double y0, double dx, double dy, 
									int N, eInterpolationMethod method, 
									float &zmin, float &zmax,
									double maxDist, int CoordInd, double minsquareDistForWeight)
{
	zmin = FLT_MAX;
	zmax = -FLT_MAX;

	if (this->isInitialised())
	{
		double minDist;
		this->initQueryPoints(N);

		ANNdist sqRad = maxDist * maxDist;
		ANNpoint queryPt = annAllocPt(annDim);
	
		queryPt[1] = y0;

		if (method == eMovingTiltedPlane) 
		{
			for (int r = 0; r < height; ++r, queryPt[1] -= dy)
			{
				long idx = r * bufwidth;
				long idxmax = idx + bufwidth;
				queryPt[0] = x0;

				for (; idx < idxmax; ++idx, queryPt[0] += dx)
				{
					this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
					for (int k = 0; k < N; ++k)
					{
						if (this->nnDists[k] > sqRad) this->nnDists[k] = ANN_DIST_INF;
					}
					double Z = -9999.0;
					
					Z = interpolateMovingPlane(N, CoordInd, queryPt, minsquareDistForWeight, minDist);
					
					buf[idx] = (float) Z;
					
					if (Z > -9999.0)
					{
						if (Z < zmin)  zmin = (float) Z;
						if (Z > zmax)  zmax = (float) Z;
					}
				}
			}
		}
		else if (method == eMovingHzPlane)
		{
			for (int r = 0; r < height; ++r, queryPt[1] -= dy)
			{
				long idx = r * bufwidth;
				long idxmax = idx + bufwidth;
				queryPt[0] = x0;

				for (; idx < idxmax; ++idx, queryPt[0] += dx)
				{
					this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0); 
					for (int k = 0; k < N; ++k)
					{
						if (this->nnDists[k] > sqRad) this->nnDists[k] = ANN_DIST_INF;
					}

					double Z = -9999.0;
					
					Z = interpolateMovingHorizontalPlane(N, CoordInd, minsquareDistForWeight, minDist);

					buf[idx] = (float) Z;
					if (Z > -9999.0)
					{
						if (Z < zmin)  zmin = (float) Z;
						if (Z > zmax)  zmax = (float) Z;
					}
				}
			}
		}

		annDeallocPt(queryPt);
	}
};

//================================================================================

double CPointCloud::interpolateMovingHorizontalPlane(int N, int CoordInd, 
													 double minsquareDistForWeight, 
													 double &minDist)
{
	double Z = -9999.0;
	double sumW = 0.0;
	double sumZ = 0.0f;

	for (int i = 0; i < N; ++i)
	{
		if ((this->nnIdx[i] != ANN_NULL_IDX) && (this->nnDists[i] != ANN_DIST_INF))
		{
			ANNpoint p = this->annPoints[this->nnIdx[i]];
			double w = this->nnDists[i];
			if (w < minsquareDistForWeight) w = minsquareDistForWeight;
			w = 1.0 / w;
			sumZ += p[CoordInd] * w;
			sumW += w;
		}
		else i = N;
	}

	if (sumW > FLT_EPSILON)
	{
		Z = sumZ / sumW;
		minDist = sqrt(this->nnDists[0]);
	}
	else minDist = FLT_MAX;

	return Z;
};

//================================================================================

double CPointCloud::interpolateMovingPlane(int N, int CoordInd, 
										   const ANNpoint &queryPt, 
										   double minsquareDistForWeight,
										   double &minDist)
{
	double Z = -9999.0;
	SymmetricMatrix NMat(3);
	Matrix Atl(3,1);
	NMat = 0;
	Atl = 0;

	double zmin = FLT_MAX;
	double zmax = -FLT_MAX;

	int usedPts = 0;

	for (int i = 0; i < N; ++i)
	{
		if ((this->nnIdx[i] != ANN_NULL_IDX) && (this->nnDists[i] != ANN_DIST_INF))
		{
			ANNpoint p = this->annPoints[this->nnIdx[i]];
			double w = this->nnDists[i];
			if (w < minsquareDistForWeight) w = minsquareDistForWeight;
			w = 1.0 / w;

			double x = p[0] - queryPt[0];
			double y = p[1] - queryPt[1];
			double z = p[CoordInd];
			double xw = x * w;
			double yw = y * w;
			
			NMat.element(0,0) += w; NMat.element(0,1) += xw;      NMat.element(0,2) += yw; 
			                        NMat.element(1,1) += x * xw;  NMat.element(1,2) += x * yw; 
			                                                      NMat.element(2,2) += y * yw; 

			Atl.element(0,0) += z *  w;
			Atl.element(1,0) += z * xw;
			Atl.element(2,0) += z * yw;

			++usedPts;
			if (z < zmin) zmin = z;
			if (z > zmax) zmax = z;	
		}
		else i = N;
	}
	
	if (usedPts < 3)
	{
		if (NMat.element(0,0) > FLT_EPSILON)
		{
			Z = Atl.element(0,0) / NMat.element(0,0);
			minDist = sqrt(this->nnDists[0]);
		}
		else minDist = FLT_MAX;
	}
	else
	{
		Try
		{
			Matrix Qxx = NMat.i();
			Atl =  Qxx * Atl;
			minDist = sqrt(this->nnDists[0]);

			Z = Atl.element(0,0);
			if (Z < zmin) Z = zmin;
			else if (Z > zmax) Z = zmax;
		}		 
		CatchAll 
		{
			minDist = FLT_MAX; 
		}
	}

	return Z;
};


//================================================================================

int CPointCloud::CircumCircle(ANNpoint p0, ANNpoint p1, ANNpoint p2, ANNpoint p3, ANNpoint &pc, double &r) const
{
	double m1, m2, mx1, mx2, my1, my2;
	double dx, dy, rsqr, drsqr;

	// Check for coincident points 
	if(abs(p1[1] - p2[1]) < EPSILON && abs(p2[1] - p3[1]) < EPSILON)
		return(false);

	if(abs(p2[1] - p1[1]) < EPSILON)
	{ 
		m2 = - (p3[0] - p2[0]) / (p3[1] - p2[1]);
		mx2 = (p2[0] + p3[0]) * 0.5;
		my2 = (p2[1] + p3[1]) * 0.5;
		pc[0] = (p2[0] + p1[0]) * 0.5;
		pc[1] = m2 * (pc[0] - mx2) + my2;
	}
	else if(abs(p3[1] - p2[1]) < EPSILON)
	{
		m1 = - (p2[0] - p1[0]) / (p2[1] - p1[1]);
		mx1 =  (p1[0] + p2[0]) * 0.5;
		my1 =  (p1[1] + p2[1]) * 0.5;
		pc[0] = (p3[0] + p2[0]) * 0.5;
		pc[1] = m1 * (pc[0] - mx1) + my1;
	}
	else
	{
		m1 = - (p2[0] - p1[0]) / (p2[1] - p1[1]); 
		m2 = - (p3[0] - p2[0]) / (p3[1] - p2[1]); 
		mx1 = (p1[0] + p2[0]) * 0.5; 
		mx2 = (p2[0] + p3[0]) * 0.5;
		my1 = (p1[1] + p2[1]) * 0.5;
		my2 = (p2[1] + p3[1]) * 0.5;
		pc[0] = (m1 * mx1 - m2 * mx2 + my2 - my1) / (m1 - m2); 
		pc[1] = m1 * (pc[0] - mx1) + my1; 
	}

	dx = p2[0] - pc[0];
	dy = p2[1] - pc[1];
	rsqr = dx * dx + dy * dy;
	r = sqrt(rsqr); 
	dx = p0[0] - pc[0];
	dy = p0[1] - pc[1];
	drsqr = dx * dx + dy * dy;
	
	return((drsqr <= rsqr) ? true : false);
}


//================================================================================

void CPointCloud::findOuterTriangles(ANNpoint &p1, ANNpoint &p2,ANNpoint &p3) const
{
	double dx = this->pMax.x - this->pMin.x;
	double dy = this->pMax.y - this->pMin.y;
	double dmax = (dx > dy) ? dx : dy;
	double xmid = (this->pMax.x + this->pMin.x) * 0.5;
	double ymid = (this->pMax.y + this->pMin.y) * 0.5;

//    Set up the supertriangle
//    his is a triangle which encompasses all the sample points.
//    The supertriangle coordinates are added to the end of the
//    vertex list. The supertriangle is the first triangle in
//    the triangle list.

	p1[0] = xmid - 20 * dmax;
	p1[1] = ymid - dmax;
	p2[0] = xmid;
	p2[1] = ymid + 20 * dmax;
	p3[0] = xmid + 20 * dmax;
	p3[1] = ymid - dmax;
}


//================================================================================
	  
void CPointCloud::resetTriangulation()
{
	if (this->firstEdgePerVertex) delete [] this->firstEdgePerVertex;
	if (this->edges)     delete [] this->edges;
	if (this->triangles) delete [] this->triangles;
	this->edges              = 0;
	this->triangles          = 0;
	this->firstEdgePerVertex = 0;

	this->nEdges = 0;
	this->nTriangles = 0;
	this->lastTriangle = -1;
};

//================================================================================
	
int CPointCloud::findEdgeIndex(int p1, int p2) const
{
	for (int i = 0; i < this->nEdges; ++i)
	{
		if ((this->edges[i].p[0] == p1) && (this->edges[i].p[1] == p2)) return i;
		if ((this->edges[i].p[0] == p2) && (this->edges[i].p[1] == p1)) return i;
	}

	return -1;
};
	 	
//================================================================================
 
int CPointCloud::checkInsertEdge(int triangle, int p1, int p2)
{
	int edgeIndex = this->findEdgeIndex(p1, p2);

	if (edgeIndex < 0)
	{
		edgeIndex = this->nEdges;
		this->edges[this->nEdges].p[0] = p1;
		this->edges[this->nEdges].p[1] = p2;
		this->edges[this->nEdges].t[0] = triangle;
		this->edges[this->nEdges].t[1] = -1;
		if (this->firstEdgePerVertex[p1] < 0) this->firstEdgePerVertex[p1] = this->nEdges;
		++this->nEdges;
	}
	else
	{
		this->edges[edgeIndex].t[1] = triangle;
	}

	return edgeIndex;
};

//================================================================================

void CPointCloud::exportTriangulation(const char *filename) const
{
	ofstream dxf(filename);
	CXYZPolyLine::writeDXFHeader(dxf);

	for (int i = 0; i < this->nTriangles; ++i)
	{
		CXYZPolyLine poly;
		CXYZPoint p;
		p.x = this->annPoints[this->triangles[i].p[0]][0];
		p.y = this->annPoints[this->triangles[i].p[0]][1];
		if (this->dimension > 2) p.z = this->annPoints[this->triangles[i].p[0]][2];
		poly.addPoint(p);
		p.x = this->annPoints[this->triangles[i].p[1]][0];
		p.y = this->annPoints[this->triangles[i].p[1]][1];
		if (this->dimension > 2) p.z = this->annPoints[this->triangles[i].p[1]][2];
		poly.addPoint(p);
		p.x = this->annPoints[this->triangles[i].p[2]][0];
		p.y = this->annPoints[this->triangles[i].p[2]][1];
		if (this->dimension > 2) p.z = this->annPoints[this->triangles[i].p[2]][2];
		poly.addPoint(p);
		poly.closeLine();

		poly.writeDXF(dxf, "Tri", 1, 3);
	}

	CXYZPolyLine::writeDXFTrailer(dxf);
};
	
//================================================================================
	  
bool CPointCloud::right(int edgeIndex, double x, double y, int triangle) const
{
	ANNpoint p0, p1; 

	if (this->edges[edgeIndex].t[0] == triangle)
	{
		p0 = this->annPoints[this->edges[edgeIndex].p[0]];
		p1 = this->annPoints[this->edges[edgeIndex].p[1]];
	}
	else
	{
		p0 = this->annPoints[this->edges[edgeIndex].p[1]];
		p1 = this->annPoints[this->edges[edgeIndex].p[0]];
	}

	if (((p1[1] - p0[1]) * (x - p0[0]) - (p1[0] - p0[0]) * (y - p0[1])) < 0) return false;

	return true;
};
		  
//================================================================================

int CPointCloud::getNearestPoint(int triangle, double x, double y) const
{
	double dx = this->annPoints[this->triangles[triangle].p[0]][0] - x;
	double dy = this->annPoints[this->triangles[triangle].p[0]][1] - y;

	double distMin = dx * dx + dy * dy;
	int imin = 0;

	dx = this->annPoints[this->triangles[triangle].p[1]][0] - x;
	dy = this->annPoints[this->triangles[triangle].p[1]][1] - y;

	double dist = dx * dx + dy * dy;
	if (dist < distMin) 
	{
		distMin = dist;
		imin = 1;
	}

	dx = this->annPoints[this->triangles[triangle].p[2]][0] - x;
	dy = this->annPoints[this->triangles[triangle].p[2]][1] - y;

	dist = dx * dx + dy * dy;
	if (dist < distMin) 
	{
		distMin = dist;
		imin = 2;
	}

	return imin;
};

//================================================================================

int CPointCloud::getNextTriangleZwickelIndex(int triangle, double x, double y, bool &hasZwickel) const
{
	int newIndex = -1;
	hasZwickel = false;

	bool right0 = right(this->triangles[triangle].e[0], x, y, triangle);
	bool right1 = right(this->triangles[triangle].e[1], x, y, triangle);

	if (right0 && right1)
	{
		hasZwickel = true;
		int edgeIdx = this->triangles[triangle].e[2];
		if (this->edges[edgeIdx].t[0] == triangle) newIndex = this->edges[edgeIdx].t[1];
		else newIndex = this->edges[edgeIdx].t[0];

	}
	else
	{
		bool right2 = right(this->triangles[triangle].e[2], x, y, triangle);

		if (right0 && right2)
		{
			hasZwickel = true;
			int edgeIdx = this->triangles[triangle].e[1];
			if (this->edges[edgeIdx].t[0] == triangle) newIndex = this->edges[edgeIdx].t[1];
			else newIndex = this->edges[edgeIdx].t[0];
		}
		else if (right1 && right2)
		{
			hasZwickel = true;
			int edgeIdx = this->triangles[triangle].e[0];
			if (this->edges[edgeIdx].t[0] == triangle) newIndex = this->edges[edgeIdx].t[1];
			else newIndex = this->edges[edgeIdx].t[0];
		}

	}

	return newIndex;
}
	   
//================================================================================

int CPointCloud::getPrevEdge(int triangle, int edge) const
{
	if (triangle < 0) return -1;
	for (int i = 0; i < 3; ++i)
	{
		if (this->triangles[triangle].e[i] == edge)
			return this->triangles[triangle].e[(i + 2)%3];		
	}

	return -1;
};
	  
//================================================================================

int CPointCloud::getNextEdge(int triangle, int edge) const
{
	if (triangle < 0) return -1;
	for (int i = 0; i < 3; ++i)
	{
		if (this->triangles[triangle].e[i] == edge)
			return this->triangles[triangle].e[(i + 1)%3];		
	}

	return -1;
};

//================================================================================

int CPointCloud::moveToNextTriangleIndex(int triangle, double x, double y, int *visited, int nVisited) const
{
	bool hasZwickel;
	int newIndex = getNextTriangleZwickelIndex(triangle, x, y, hasZwickel);

	for (int i = 0; i < nVisited; ++i)
	{
		if (visited[i] == newIndex) 
		{
			hasZwickel = false;
			i = nVisited + 1;
		}
	}

	if (!hasZwickel)
	{
		int pNearest = this->triangles[triangle].p[getNearestPoint(triangle, x, y)];

		int edge0 = this->firstEdgePerVertex[pNearest];

		if (edge0 >= 0)
		{
			int edgeC = edge0;
			do
			{
				if ((this->edges[edgeC].t[0] != triangle) && (this->edges[edgeC].t[0] >= 0))
					newIndex = getNextTriangleZwickelIndex(this->edges[edgeC].t[0], x, y, hasZwickel);
				for (int i = 0; i < nVisited; ++i)
				{
					if (visited[i] == newIndex) 
					{
						hasZwickel = false;
						i = nVisited + 1;
					}
				}

				if (hasZwickel && (newIndex != triangle)) return newIndex;
				if ((this->edges[edgeC].t[1] != triangle) && (this->edges[edgeC].t[1] >= 0))
					newIndex = getNextTriangleZwickelIndex(this->edges[edgeC].t[1], x, y, hasZwickel);

				for (int i = 0; i < nVisited; ++i)
				{
					if (visited[i] == newIndex) 
					{
						hasZwickel = false;
						i = nVisited + 1;
					}
				}

				if (hasZwickel && (newIndex != triangle)) return newIndex;

				newIndex = -1;

				if (this->edges[edgeC].p[0] == pNearest)
				{
					edgeC = this->getPrevEdge(this->edges[edgeC].t[0], edgeC);
				}
				else
				{
					edgeC = this->getPrevEdge(this->edges[edgeC].t[1], edgeC);
				}
			}
			while ((edgeC != edge0) && (edgeC >= 0));

			if (edgeC < 0)
			{
				edgeC = edge0;

				do
				{
					if ((this->edges[edgeC].t[0] != triangle) && (this->edges[edgeC].t[0] >= 0))
						newIndex = getNextTriangleZwickelIndex(this->edges[edgeC].t[0], x, y, hasZwickel);
					for (int i = 0; i < nVisited; ++i)
					{
						if (visited[i] == newIndex) 
						{
							hasZwickel = false;
							i = nVisited + 1;
						}
					}

					if (hasZwickel && (newIndex != triangle)) return newIndex;
					if ((this->edges[edgeC].t[1] != triangle) && (this->edges[edgeC].t[1] >= 0))
						newIndex = getNextTriangleZwickelIndex(this->edges[edgeC].t[1], x, y, hasZwickel);
					for (int i = 0; i < nVisited; ++i)
					{
						if (visited[i] == newIndex) 
						{
							hasZwickel = false;
							i = nVisited + 1;
						}
					}

					if (hasZwickel && (newIndex != triangle)) return newIndex;

					newIndex = -1;

					if (this->edges[edgeC].p[0] == pNearest)
					{
						edgeC = this->getNextEdge(this->edges[edgeC].t[1], edgeC);
					}
					else
					{
						edgeC = this->getNextEdge(this->edges[edgeC].t[0], edgeC);
					}
				}
				while ((edgeC != edge0) && (edgeC >= 0));
			}
		}
	}
	return newIndex;
};

//================================================================================
	  
bool CPointCloud::isInTriangle(int triangle, const C2DPoint &p) const
{
	if (!right(this->triangles[triangle].e[0], p.x, p.y, triangle)) return false;
	if (!right(this->triangles[triangle].e[1], p.x, p.y, triangle)) return false;
	if (!right(this->triangles[triangle].e[2], p.x, p.y, triangle)) return false;

	return true;
};

//================================================================================

int CPointCloud::findTriangleIndex(const C2DPoint &p) const
{
	if ((this->lastTriangle < 0) || (this->lastTriangle >= this->nTriangles)) this->lastTriangle = 0;

	int maxVisited = 100;
	int *visited = new int[maxVisited];
	int nVisited = 0;

	while (this->lastTriangle >= 0) 
	{
		if (this->isInTriangle(this->lastTriangle, p)) 
		{
			delete [] visited;
			return this->lastTriangle;
		}

		if (nVisited >= maxVisited) 
		{
			maxVisited += 100;
			int *oldVis = visited;
			visited = new int[maxVisited];
			memcpy(visited, oldVis, nVisited * sizeof(int));
			delete [] oldVis;
		}

		visited[nVisited] = this->lastTriangle;
		nVisited++;

		this->lastTriangle = moveToNextTriangleIndex(this->lastTriangle, p.x, p.y, visited, nVisited);
	} 

	delete [] visited;

/*
// brute force
	for (int i = 0; i < this->nTriangles; ++i)
	{
		if (this->isInTriangle(i,p)) return i;
	}
	*/
	return -1;
};


//================================================================================

int ANNpointCompare(const void *v1, const void *v2)
{
	ANNpoint p1 = (ANNpoint) v1;
	ANNpoint p2 = (ANNpoint) v2;

	if(p1[0] < p2[0]) return -1;

	if(p1[0] > p2[0]) return 1;
	
	return 0;
}

//================================================================================

///////////////////////////////////////////////////////////////////////////////
// Triangulate() :
//   Triangulation subroutine
//   Takes as input NV vertices in array pxyz
//   Returned is a list of ntri triangular faces in the array v
//   These triangles are arranged in a consistent clockwise order.
//   The triangle array 'v' should be malloced to 3 * nv
//   The vertex array pxyz must be big enough to hold 3 more points
//   The vertex array must be sorted in increasing x values say
//
//   qsort(p,nv,sizeof(XYZ),XYZCompare);
///////////////////////////////////////////////////////////////////////////////
int CPointCloud::Triangulate()
{
	this->resetTriangulation();
	int status = 0;
	int inside;
	int i, j, k;
	double r;

	int nmax = this->npoints + 3;
	int qsortSize = this->dimension + 1;
	double *AnnPointsSortedCoords = new double[nmax * qsortSize];
	ANNpointArray AnnPointsSorted = new ANNpoint[nmax];

	if (outputMode > eMEDIUM)
		protHandler.print(CCharString ("Sorting points at ") + CSystemUtilities::timeString(), protHandler.prt);

	for (i = 0, j = 0; i < this->npoints; ++i, j += qsortSize) 
	{
		AnnPointsSorted[i] = &AnnPointsSortedCoords[j];
		for (int k = 0; k < this->dimension; ++k) AnnPointsSorted[i][k] = this->annPoints[i][k];
		AnnPointsSorted[i][this->dimension] = i;
	}

	for (; i < nmax; ++i, j += qsortSize) 
	{
		AnnPointsSorted[i] = &AnnPointsSortedCoords[j];
	}

	qsort(AnnPointsSortedCoords, this->npoints, qsortSize * sizeof(double), ANNpointCompare);
	
	if (outputMode > eMEDIUM)
		protHandler.print(CCharString ("Triangulating points at ") + CSystemUtilities::timeString(), protHandler.prt);


//    Set up the supertriangle
//    his is a triangle which encompasses all the sample points.
//    The supertriangle coordinates are added to the end of the
//    vertex list. The supertriangle is the first triangle in
//    the triangle list.

	this->findOuterTriangles(AnnPointsSorted[this->npoints], AnnPointsSorted[this->npoints + 1], AnnPointsSorted[this->npoints + 2]);

// Allocate memory for the completeness list, flag for each triangle
	int trimax = 4 * this->npoints;
	int *complete = new int[trimax];
	this->triangles = new ITRIANGLE[trimax];

// Allocate memory for the edge list
	int emax = trimax;
	this->edges = new IEDGE[emax];

	this->triangles[0].p[0] = this->npoints;
	this->triangles[0].p[1] = this->npoints + 1;
	this->triangles[0].p[2] = this->npoints + 2;
	complete[0] = false;
	this->nTriangles = 1;

//    Include each point one at a time into the existing mesh

	ANNpoint p;
	ANNpoint centre = new double[this->dimension];
	int nedge;

	for(i = 0; i < this->npoints; i++)
	{
		p = AnnPointsSorted[i];
		nedge = 0;
/*
     Set up the edge buffer.
     If the point (xp,yp) lies inside the circumcircle then the
     three edges of that triangle are added to the edge buffer
     and that triangle is removed.
*/
		for(j = 0; (j < this->nTriangles); j++)
		{
			if (complete[j]) continue;

			inside = CircumCircle(p, AnnPointsSorted[this->triangles[j].p[0]], 
				                     AnnPointsSorted[this->triangles[j].p[1]], 
									 AnnPointsSorted[this->triangles[j].p[2]], centre, r);

			if (centre[0] + r < p[0]) complete[j] = true;
    
			if (inside)
			{
/* Check that we haven't exceeded the edge list size */
				if(nedge + 3 >= emax)
				{
					emax += 1000;
					IEDGE * p_EdgeTemp = new IEDGE[emax];
					for (int i = 0; i < nedge; i++)  p_EdgeTemp[i] = edges[i];   
					delete [] edges;
					edges = p_EdgeTemp;
				}

				edges[nedge+0].p[0] = this->triangles[j].p[0];
				edges[nedge+0].p[1] = this->triangles[j].p[1];
				edges[nedge+1].p[0] = this->triangles[j].p[1];
				edges[nedge+1].p[1] = this->triangles[j].p[2];
				edges[nedge+2].p[0] = this->triangles[j].p[2];
				edges[nedge+2].p[1] = this->triangles[j].p[0];
				nedge += 3;
				this->triangles[j] = this->triangles[this->nTriangles - 1];
				complete[j] = complete[this->nTriangles - 1];
				this->nTriangles--;
				j--;
			}
		}
/*
  Tag multiple edges
  Note: if all triangles are specified anticlockwise then all
  interior edges are opposite pointing in direction.
*/
		for(j = 0; j < nedge - 1; j++)
		{
			for(k = j + 1; k < nedge; k++)
			{
				if((edges[j].p[0] == edges[k].p[1]) && (edges[j].p[1] == edges[k].p[0]))
				{
					edges[j].p[0] = -1;
					edges[j].p[1] = -1;
					edges[k].p[0] = -1;
					edges[k].p[1] = -1;
				}
				/* Shouldn't need the following, see note above */
				if((edges[j].p[0] == edges[k].p[0]) && (edges[j].p[1] == edges[k].p[1]))
				{
					edges[j].p[0] = -1;
					edges[j].p[1] = -1;
					edges[k].p[0] = -1;
					edges[k].p[1] = -1;
				}
			}
		}
/*
     Form new triangles for the current point
     Skipping over any tagged edges.
     All edges are arranged in clockwise order.
*/
		for(j = 0; j < nedge; j++) 
		{
			if(edges[j].p[0] < 0 || edges[j].p[1] < 0)
				continue;
			
			this->triangles[this->nTriangles].p[0] = edges[j].p[0];
			this->triangles[this->nTriangles].p[1] = edges[j].p[1];
			this->triangles[this->nTriangles].p[2] = i;
			complete[this->nTriangles] = false;
			this->nTriangles++;
		}
	}
/*
      Remove triangles with supertriangle vertices
      These are triangles which have a vertex number greater than nv
*/

	if (outputMode > eMEDIUM)
		protHandler.print(CCharString ("Removing Super-triangles at ") + CSystemUtilities::timeString(), protHandler.prt);

	for (i = 0; i < this->nTriangles; i++) 
	{
		if (this->triangles[i].p[0] >= this->npoints || this->triangles[i].p[1] >= this->npoints || this->triangles[i].p[2] >= this->npoints) 
		{
			this->triangles[i] = this->triangles[this->nTriangles - 1];
			this->nTriangles--;
			i--;
		}
		else
		{
			this->triangles[i].p[0] = (int) AnnPointsSorted[this->triangles[i].p[0]][this->dimension];
			this->triangles[i].p[1] = (int) AnnPointsSorted[this->triangles[i].p[1]][this->dimension];
			this->triangles[i].p[2] = (int) AnnPointsSorted[this->triangles[i].p[2]][this->dimension];
		}
	}

	ITRIANGLE *existing = this->triangles;
	this->triangles = new ITRIANGLE[this->nTriangles];
	memcpy(this->triangles,existing,this->nTriangles * sizeof(ITRIANGLE));

	this->firstEdgePerVertex = new int[this->npoints];
	for (int i = 0; i < this->npoints; ++i) this->firstEdgePerVertex[i] = -1;

	this->nEdges = 3 * this->nTriangles;
	delete [] this->edges;

	this->edges = new IEDGE[this->nEdges];
	this->nEdges = 0;

	if (outputMode > eMEDIUM)
		protHandler.print(CCharString ("Initialising edges at ") + CSystemUtilities::timeString(), protHandler.prt);

	for (int triIndex = 0; triIndex < this->nTriangles; ++triIndex)
	{
		/*		this->triangles[triIndex].e[0] = checkInsertEdge(triIndex , this->triangles[triIndex].p[0], this->triangles[triIndex].p[1]);
		this->triangles[triIndex].e[1] = checkInsertEdge(triIndex , this->triangles[triIndex].p[1], this->triangles[triIndex].p[2]);
		this->triangles[triIndex].e[2] = checkInsertEdge(triIndex , this->triangles[triIndex].p[2], this->triangles[triIndex].p[0]);
		*/
		this->edges[this->nEdges].p[0] = this->triangles[triIndex].p[0];
		this->edges[this->nEdges].p[1] = this->triangles[triIndex].p[1];
		this->edges[this->nEdges].t[0] = triIndex;
		this->edges[this->nEdges].t[1] = -1;
		this->triangles[triIndex].e[0] = this->nEdges;
		if (this->firstEdgePerVertex[this->triangles[triIndex].p[0]] < 0) this->firstEdgePerVertex[this->triangles[triIndex].p[0]] = this->nEdges;
		++this->nEdges;

		this->edges[this->nEdges].p[0] = this->triangles[triIndex].p[1];
		this->edges[this->nEdges].p[1] = this->triangles[triIndex].p[2];
		this->edges[this->nEdges].t[0] = triIndex;
		this->edges[this->nEdges].t[1] = -1;
		if (this->firstEdgePerVertex[this->triangles[triIndex].p[1]] < 0) this->firstEdgePerVertex[this->triangles[triIndex].p[1]] = this->nEdges;
		this->triangles[triIndex].e[1] = this->nEdges;
		++this->nEdges;

		this->edges[this->nEdges].p[0] = this->triangles[triIndex].p[2];
		this->edges[this->nEdges].p[1] = this->triangles[triIndex].p[0];
		this->edges[this->nEdges].t[0] = triIndex;
		this->edges[this->nEdges].t[1] = -1;
		if (this->firstEdgePerVertex[this->triangles[triIndex].p[2]] < 0) this->firstEdgePerVertex[this->triangles[triIndex].p[2]] = this->nEdges;
		this->triangles[triIndex].e[2] = this->nEdges;
		++this->nEdges;

	}

	int nMaxEdge = this->nEdges;
	this->nEdges = 0;

	for (int i = 0; i < nMaxEdge; ++i)
	{
		if (this->edges[i].t[0] >= 0)
		{
			for (int k = 0; k < 3; ++k)
			{
				if (this->triangles[this->edges[i].t[0]].e[k] == i) this->triangles[this->edges[i].t[0]].e[k] = this->nEdges;
				if (this->firstEdgePerVertex[this->triangles[this->edges[i].t[0]].p[k]] == i) this->firstEdgePerVertex[this->triangles[this->edges[i].t[0]].p[k]]  = this->nEdges;
			}

			this->edges[this->nEdges] = this->edges[i];
			bool found = false;
			for (int j = i + 1; (j < nMaxEdge) && (!found); ++j)
			{
				if ((this->edges[this->nEdges].p[0] == this->edges[j].p[1]) && (this->edges[this->nEdges].p[1] == this->edges[j].p[0]))
				{
					this->edges[this->nEdges].t[1] = this->edges[j].t[0];
					for (int k = 0; k < 3; ++k)
					{
						if (this->triangles[this->edges[j].t[0]].e[k] == j) this->triangles[this->edges[j].t[0]].e[k] = this->nEdges;
						if (this->firstEdgePerVertex[this->triangles[this->edges[j].t[0]].p[k]] == i) this->firstEdgePerVertex[this->triangles[this->edges[j].t[0]].p[k]]  = this->nEdges;
					}
					this->edges[j].t[0] = -1;
					found = true;
				}
			}

			this->nEdges++;
		}
	}


	delete [] existing;
	delete [] complete;
	delete [] centre;

	if (outputMode > eMEDIUM)
		protHandler.print(CCharString ("Edges initialised at ") + CSystemUtilities::timeString(), protHandler.prt);

	if (AnnPointsSorted) 
	{
		delete [] AnnPointsSorted;
		delete [] AnnPointsSortedCoords;
		AnnPointsSorted = 0;
		AnnPointsSortedCoords = 0;
	}

	return 0;
} 

//================================================================================
	  
double CPointCloud::interpolateTriangulation(const C2DPoint &p, int CoordInd) const
{
	double Value = -9999.0;

	int index = this->findTriangleIndex(p);
	if (index >= 0)
	{
		ANNpoint p1 = this->annPoints[this->triangles[index].p[0]];
		ANNpoint p2 = this->annPoints[this->triangles[index].p[1]];
		ANNpoint p3 = this->annPoints[this->triangles[index].p[2]];
        double x1 = 0.0, y1 = 0.0, z1 = p1[CoordInd];
		double x2 = p2[0] - p1[0], y2 = p2[1] - p1[1], z2 = p2[CoordInd] - z1;
		double x3 = p3[0] - p1[0], y3 = p3[1] - p1[1], z3 = p3[CoordInd] - z1;
		double x  = p.x - p1[0], y = p.y - p1[1];
		double det = x2 * y3 - y2 * x3;
		if (fabs(det) > FLT_EPSILON)
		{
			det = 1.0 / det;
			double ax = ( y3 * z2 - y2 * z3) * det;
			double ay = (-x3 * z2 + x2 * z3) * det;
			Value = z1 + ax * x + ay * y;
		}
	}

	return Value;
};
	  
double CPointCloud::interpolateTriangulation(const C2DPoint &p, int CoordInd, double &minDist) const
{
	double value = interpolateTriangulation(p, CoordInd);

	minDist = FLT_MAX;

	if (this->lastTriangle >= 0)
	{
		double dx = p.x - this->annPoints[this->triangles[this->lastTriangle].p[0]][0];
		double dy = p.x - this->annPoints[this->triangles[this->lastTriangle].p[0]][1];
		minDist = dx * dx + dy * dy;
		double dist;

		for (int i = 1; i < 3; ++i)
		{
			dx = p.x - this->annPoints[this->triangles[this->lastTriangle].p[i]][0];
			dy = p.x - this->annPoints[this->triangles[this->lastTriangle].p[i]][1];
			dist = dx * dx + dy * dy;
			if (dist < minDist) minDist = dist;
		}

		minDist = sqrt(minDist);
	}

	return value;
};

//================================================================================
	  
bool CPointCloud::interpolateRasterTriangulation(float *buf, int width, int height, int bufwidth,
							                            double x0, double y0, double dx, double dy, 
		                                                float &zmin, float &zmax, int CoordInd)
{
	bool ret = true;

	zmin = FLT_MAX;
	zmax = -FLT_MAX;

	if (!this->nTriangles) this->Triangulate();
	if (!this->nTriangles) ret = false;
	else
	{
		C2DPoint p;
		p.y = y0;

		int firstTriangleInLine = -1;

		for (int r = 0; r < height; ++r, p.y -= dy)
		{
			long idx = r * bufwidth;
			long idxmax = idx + width;
			p.x = x0;

			this->lastTriangle = firstTriangleInLine;
			firstTriangleInLine = -1;

			for (; idx < idxmax; ++idx, p.x += dx)
			{			
				double Z = interpolateTriangulation(p, CoordInd);

				buf[idx] = (float) Z;
				
				if (Z > -9999.0)
				{
					if (Z < zmin)  zmin = (float) Z;
					if (Z > zmax)  zmax = (float) Z;
					if (firstTriangleInLine < 0) firstTriangleInLine = this->lastTriangle;
				}
			}
		}
		
	}


	return ret;
}

//********************************************************************************
//***** Class CGenericPointCloud
//********************************************************************************

CGenericPointCloud::CGenericPointCloud(int Capacity, int dim, int anndim) :
		CPointCloud(dim, anndim), coordinateVector(0)
{
	this->dimension = dim < 3 ? 3 : dim;
	this->annDim = anndim < 2 ? 2 : anndim;
	if (this->annDim > this->dimension) this->annDim = this->dimension;

	this->pMin.x = this->pMin.y = this->pMin.z = FLT_MAX;
	this->pMax.x = this->pMax.y = this->pMax.z = -FLT_MAX;
	
	this->capacity = Capacity > 0? Capacity : 0;
	this->npoints = 0;
	
	if (this->capacity > 0)
	{
		this->coordinateVector = new double [this->capacity * this->dimension];
		this->annPoints = new ANNpoint [this->capacity];

		for (int i = 0, c = 0; i < this->capacity; ++i, c+= this->dimension)
		{
			this->annPoints[i] = &(this->coordinateVector[c]);
		}
	}	
}

CGenericPointCloud::CGenericPointCloud(const CXYZPointArray &Points, int dim, int anndim) : 
		CPointCloud(dim, anndim), coordinateVector(0)
{
	this->init(Points, this->dimension, this->annDim);
}

//================================================================================

CGenericPointCloud::CGenericPointCloud(const CXYPointArray &Points, int dim)  : 
		CPointCloud(dim, 2), coordinateVector(0)
{
	this->init(Points, this->dimension);
}

//================================================================================
		 
CGenericPointCloud::~CGenericPointCloud()
{
	this->clear();
};

//================================================================================
	  
bool CGenericPointCloud::setAnnDim(int anndim)
{
	if (this->annDim != anndim)
	{
		if ((anndim < 2) || (anndim > this->dimension)) return false;

		if (this->kdTree) delete this->kdTree;
		this->kdTree = 0;
		
		this->annDim = anndim;
		this->kdTree = new ANNkd_tree(this->annPoints, this->npoints, annDim, 5);
	}
	else if (!this->kdTree) this->init();
	
	return (this->kdTree != 0);
};

//================================================================================

void CGenericPointCloud::init(const CXYZPointArray &Points, int dim, int anndim)
{
	this->clear();

	this->dimension = dim < 3 ? 3 : dim;
	this->annDim = anndim < 2 ? 2 : anndim;
	if (this->annDim > this->dimension) this->annDim = this->dimension;

	this->pMin.x = this->pMin.y = this->pMin.z = FLT_MAX;
	this->pMax.x = this->pMax.y = this->pMax.z = -FLT_MAX;
	
	this->npoints = Points.GetSize();
	
	if (this->npoints > 0)
	{
		this->capacity = this->npoints;

		this->coordinateVector = new double [this->capacity * this->dimension];
		this->annPoints = new ANNpoint [this->capacity];

		for (int i = 0, c = 0; i < this->npoints; ++i, c+= this->dimension)
		{
			this->annPoints[i] = &(this->coordinateVector[c]);
			CXYZPoint *p = Points.getAt(i);
			this->coordinateVector[c    ] = p->x;
			this->coordinateVector[c + 1] = p->y;
			this->coordinateVector[c + 2] = p->z;
			if (this->pMin.x > p->x) this->pMin.x = p->x;
			if (this->pMax.x < p->x) this->pMax.x = p->x;
			if (this->pMin.y > p->y) this->pMin.y = p->y;
			if (this->pMax.y < p->y) this->pMax.y = p->y;
			if (this->pMin.z > p->z) this->pMin.z = p->z;
			if (this->pMax.z < p->z) this->pMax.z = p->z;
		}

		this->kdTree = new ANNkd_tree(this->annPoints, this->npoints, annDim, 5);
	}	
};

//================================================================================

void CGenericPointCloud::init(const CXYPointArray &Points, int dim)
{
	this->clear();

	this->dimension = dim < 2 ? 2 : dim;
	this->annDim = 2;

	this->pMin.x = this->pMin.y = this->pMin.z = FLT_MAX;
	this->pMax.x = this->pMax.y = this->pMax.z = -FLT_MAX;
	
	this->npoints = Points.GetSize();
	
	if (this->npoints > 0)
	{
		this->capacity = this->npoints;

		this->coordinateVector = new double [this->capacity * this->dimension];
		this->annPoints = new ANNpoint [this->capacity];

		for (int i = 0, c = 0; i < this->npoints; ++i, c+= this->dimension)
		{
			this->annPoints[i] = &(this->coordinateVector[c]);
			CXYPoint *p = Points.getAt(i);
			this->coordinateVector[c    ] = p->x;
			this->coordinateVector[c + 1] = p->y;
			if (this->pMin.x > p->x) this->pMin.x = p->x;
			if (this->pMax.x < p->x) this->pMax.x = p->x;
			if (this->pMin.y > p->y) this->pMin.y = p->y;
			if (this->pMax.y < p->y) this->pMax.y = p->y;
		}

		this->kdTree = new ANNkd_tree(this->annPoints, this->npoints, annDim, 5);
	}	
};

//================================================================================

void CGenericPointCloud::add(const CGenericPointCloud &pointcloud, bool initTree)
{
	if (pointcloud.npoints > 0)
	{
		bool kept = true;

		ANNpointArray newAnnPoints = this->annPoints;
		double *newCoords = this->coordinateVector;

		if (this->npoints + pointcloud.npoints > this->capacity)
		{
			kept = false;
			this->capacity += pointcloud.capacity;
			newCoords = new double [this->capacity * this->dimension];
			newAnnPoints = new ANNpoint [this->capacity];

			if (this->npoints > 0)
			{
				memcpy(newCoords, this->coordinateVector, this->npoints * this->dimension * sizeof(double));
				for (int i = 0, c = 0; i < this->npoints; ++i, c+= this->dimension)
				{
					newAnnPoints[i] = &(newCoords[c]);
				}	
			}
		}

		int cCoords = this->npoints * this->dimension;

		ANNpointArray neighbours = new ANNpoint[1];
		C3DPoint P;

		double d;

		for (int i = 0; i < pointcloud.npoints; ++i)
		{
			ANNpoint p = pointcloud.annPoints[i];
			P.x = p[0]; P.y = p[1]; P.z = 0;

			if (!this->getNearestNeighbours(neighbours, &d, 1, P, 0.01))
			{
				newAnnPoints[this->npoints] = &(newCoords[cCoords]);
				for (int k = 0; k < this->dimension; ++k, ++cCoords)
				{
					newCoords[cCoords] = p[k];
				}

				this->npoints++;
			}
		}

		if (this->pMin.x > pointcloud.pMin.x) this->pMin.x = pointcloud.pMin.x;
		if (this->pMax.x < pointcloud.pMax.x) this->pMax.x = pointcloud.pMax.x;
		if (this->pMin.y > pointcloud.pMin.y) this->pMin.y = pointcloud.pMin.y;
		if (this->pMax.y < pointcloud.pMax.y) this->pMax.y = pointcloud.pMax.y;

		delete[] neighbours;

		if (this->kdTree) delete this->kdTree;

		if (!kept)
		{
			if (this->annPoints) delete [] this->annPoints;
			this->annPoints = newAnnPoints;
			if (this->coordinateVector) delete [] this->coordinateVector;
			this->coordinateVector = newCoords;
		}

		if (initTree) this->kdTree = new ANNkd_tree(this->annPoints, this->npoints, this->annDim, 5);
		else this->kdTree = 0;
	}	

};

//================================================================================

void CGenericPointCloud::merge(const CGenericPointCloud &pointcloud, bool initTree)
{
	if (pointcloud.npoints > 0)
	{
		bool kept = true;

		ANNpointArray newAnnPoints = this->annPoints;
		double *newCoords = this->coordinateVector;

		int ptSum = this->npoints + pointcloud.npoints;
		if (ptSum > this->capacity)
		{
			kept = false;
			this->capacity = ptSum;
			newCoords = new double [this->capacity * this->dimension];
			newAnnPoints = new ANNpoint [this->capacity];

			if (this->npoints > 0)
			{
				memcpy(newCoords, this->coordinateVector, this->npoints * this->dimension * sizeof(double));
				for (int i = 0, c = 0; i < this->npoints; ++i, c+= this->dimension)
				{
					newAnnPoints[i] = &(newCoords[c]);
				}	
			}
		}

		int cCoords = this->npoints * this->dimension;

		double *c1 = &newCoords[cCoords];

		memcpy(c1, pointcloud.coordinateVector, pointcloud.npoints * this->dimension * sizeof(double));

		for (int i = 0; i < pointcloud.npoints; ++i, cCoords += this->dimension)
		{
			newAnnPoints[this->npoints] = &(newCoords[cCoords]);
			this->npoints++;
		}

		if (this->pMin.x > pointcloud.pMin.x) this->pMin.x = pointcloud.pMin.x;
		if (this->pMax.x < pointcloud.pMax.x) this->pMax.x = pointcloud.pMax.x;
		if (this->pMin.y > pointcloud.pMin.y) this->pMin.y = pointcloud.pMin.y;
		if (this->pMax.y < pointcloud.pMax.y) this->pMax.y = pointcloud.pMax.y;

		if (this->kdTree) delete this->kdTree;

		if (!kept)
		{
			if (this->annPoints) delete [] this->annPoints;
			this->annPoints = newAnnPoints;
			if (this->coordinateVector) delete [] this->coordinateVector;
			this->coordinateVector = newCoords;
		}

		if (initTree) this->kdTree = new ANNkd_tree(this->annPoints, this->npoints, this->annDim, 5);
		else this->kdTree = 0;
	}	

};

//================================================================================

void CGenericPointCloud::clear()
{
	if (this->coordinateVector)
	{
		delete [] this->coordinateVector;
		this->coordinateVector = 0;
		this->npoints = 0;
		this->capacity = 0;
	}

	CPointCloud::clear();
};

//================================================================================

void CGenericPointCloud::prepareCapacity(int growBy, bool growInAnyCase)
{
	if (this->capacity < 1)
	{
		this->capacity = growBy;
		this->coordinateVector = new double [this->capacity * this->dimension];
		this->annPoints = new ANNpoint [this->capacity];
	}
	else if ((this->npoints == this->capacity) || (growInAnyCase))
	{
		int oldCap = this->capacity;
		this->capacity += growBy;
		
		double *oldV = this->coordinateVector;
		this->coordinateVector = new double [this->capacity * this->dimension];
		memcpy(this->coordinateVector,oldV,oldCap * this->dimension * sizeof(double));
		delete[] oldV;

		delete[] this->annPoints;
		this->annPoints = new ANNpoint [this->capacity];

		for (int i = 0, c = 0; i < this->npoints; ++i, c+= this->dimension)
		{
			this->annPoints[i] = &(this->coordinateVector[c]);
		}
	}
};

//================================================================================

ANNpoint CGenericPointCloud::addPoint(const C3DPoint &p)
{
	if (this->kdTree) 
	{
		delete this->kdTree;
		this->kdTree = 0;
	}

	this->prepareCapacity(1000);

	int c = this->npoints * this->dimension;

	this->annPoints[this->npoints] = &(this->coordinateVector[c]);			
	ANNpoint pRet = this->annPoints[this->npoints];
	++this->npoints;

	this->coordinateVector[c    ] = p.x;
	this->coordinateVector[c + 1] = p.y;
	this->coordinateVector[c + 2] = p.z;

	if (this->pMin.x > p.x) this->pMin.x = p.x;
	if (this->pMax.x < p.x) this->pMax.x = p.x;
	if (this->pMin.y > p.y) this->pMin.y = p.y;
	if (this->pMax.y < p.y) this->pMax.y = p.y;
	if (this->pMin.z > p.z) this->pMin.z = p.z;
	if (this->pMax.z < p.z) this->pMax.z = p.z;

	return pRet;
};

//================================================================================

ANNpoint CGenericPointCloud::addPoint(const C2DPoint &p)
{
	if (this->kdTree) 
	{
		delete this->kdTree;
		this->kdTree = 0;
	}

	this->prepareCapacity(1000);

	int c = this->npoints * this->dimension;

	this->annPoints[this->npoints] = &(this->coordinateVector[c]);			
	ANNpoint pRet = this->annPoints[this->npoints];
	++this->npoints;

	this->coordinateVector[c    ] = p.x;
	this->coordinateVector[c + 1] = p.y;

	if (this->pMin.x > p.x) this->pMin.x = p.x;
	if (this->pMax.x < p.x) this->pMax.x = p.x;
	if (this->pMin.y > p.y) this->pMin.y = p.y;
	if (this->pMax.y < p.y) this->pMax.y = p.y;

	return pRet;
};
	  
//================================================================================

void CGenericPointCloud::removePoint(int index)
{
	if (index < this->npoints)
	{
		if (this->kdTree) 
		{
			delete this->kdTree;
			this->kdTree = 0;
		}

		int cmax = this->npoints * this->dimension;
		
		for (int c = (index + 1) * this->dimension; c < cmax; c ++)
		{
			this->coordinateVector[c - this->dimension] = this->coordinateVector[c];
		}

		this->npoints -= 1;
	}
};

//================================================================================

void CGenericPointCloud::init()
{
	if (this->kdTree) 
	{
		delete this->kdTree;
		this->kdTree = 0;
	}

	if (this->npoints > 0)
	{
		this->kdTree = new ANNkd_tree(this->annPoints, this->npoints, annDim, 5);
	}	
};

//================================================================================

void CGenericPointCloud::dump(const char *filename, int indexZ) const
{
	if ((this->dimension > 2) && (this->dimension > indexZ))
	{
		FILE *fp = fopen(filename, "wt");
		if (fp)
		{
			for (int i = 0; i < this->npoints; ++i)
			{
				ANNpoint p = this->annPoints[i];
				fprintf( fp, "%12.2lf %12.2lf %8.2lf\n", p[0], p[1], p[indexZ] );
			}

			fclose(fp);
		}
	}
};

//================================================================================

bool CGenericPointCloud::dump(const char *filename) const
{
	bool ret = true;
    FILE *fp = fopen(filename, "w+b");
	if (!fp) ret = false;
	else
	{
		double d = this->pMin.x;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->pMin.y;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->pMin.z;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->pMax.x;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->pMax.y;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->pMax.z;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);

		ret &= (fwrite(&this->npoints, sizeof(int), 1, fp) == 1);
		ret &= (fwrite(&this->annDim, sizeof(int), 1, fp) == 1);
		ret &= (fwrite(&this->dimension, sizeof(int), 1, fp) == 1);

		int nRecords = this->npoints * this->dimension;
		ret &= (fwrite(this->coordinateVector, sizeof(double), nRecords, fp) == nRecords);
		fclose(fp);
	}

	return ret;
};

//================================================================================

bool CGenericPointCloud::init(const char *filename)
{
	bool ret = true;
	this->clear();
    FILE *fp = fopen(filename, "r+b");
	if (!fp) ret = false;
	else
	{
		double d;

		ret &= (fread(&d, sizeof(double), 1, fp) == 1);
		this->pMin.x = d;
		ret &= (fread(&d, sizeof(double), 1, fp) == 1);
		this->pMin.y = d;
		ret &= (fread(&d, sizeof(double), 1, fp) == 1);
		this->pMin.z = d;
		ret &= (fread(&d, sizeof(double), 1, fp) == 1);
		this->pMax.x = d;
		ret &= (fread(&d, sizeof(double), 1, fp) == 1);
		this->pMax.y = d;
		ret &= (fread(&d, sizeof(double), 1, fp) == 1);
		this->pMax.z = d;
	
		ret &= (fread(&this->npoints, sizeof(int), 1, fp) == 1);
		ret &= (fread(&this->annDim, sizeof(int), 1, fp) == 1);
		ret &= (fread(&this->dimension, sizeof(int), 1, fp) == 1);
		if (ret)
		{
			this->prepareCapacity(this->npoints);
			for (int i = 0, c = 0; i < this->npoints; ++i, c+= this->dimension)
			{
				this->annPoints[i] = &(this->coordinateVector[c]);
			}

			int nRecords = this->npoints * this->dimension;
			ret &= (fread(this->coordinateVector, sizeof(double), nRecords, fp) == nRecords);
			if (!ret) this->clear();
			else this->init();
		}

		fclose(fp);
	}

	return ret;
};

//================================================================================

bool CGenericPointCloud::interpolateRasterForALS(float *interpolatedBuf, float *approxBuf, 
											     int width, int height, int bufwidth,
											     long index0, double x0, double y0, double dx, double dy, 
												 int offsetX, int offsetY,
											     int N, float &zmin, float &zmax,
											     double maxDistInterpolate,
												 double maxDistMinDist,
												 double minsquareDistForWeight, 
											     CRobustWeightFunction *positiveBranch, 
											     CRobustWeightFunction *negativeBranch,
												 unsigned char *usedBuf)
{
	bool ret = this->dimension >= 3 && this->isInitialised();
	if (ret)
	{
		double dxInv = 1.0 / dx;
		double dyInv = 1.0 / dy;
		double wInv = 1.0  / minsquareDistForWeight;
		maxDistInterpolate *= maxDistInterpolate;
		maxDistMinDist *= maxDistMinDist;
		zmin = FLT_MAX;
		zmax = -FLT_MAX;
		bool q1, q2, q3, q4;
		short nQ;
		int NPoints;
		int ErrorCount = 0;

		this->initQueryPoints(N);

		ANNpoint queryPt = annAllocPt(annDim);

		queryPt[1] = y0;
	
		long maxIdxAppx = bufwidth * height;


		double x, y, z;
		float Z;

		double sw, sx, sy, sz, xx, xy, xz, yy, yz, w, xw, yw, res, fac, syred, swred, szred;
		double ZpMin, ZpMax;
		for (int r = 0; r < height; ++r, queryPt[1] -= dy, index0 += bufwidth)
		{
			long idx = index0;
			long idxmax = idx + width;
			queryPt[0] = x0;


			for (; idx < idxmax; ++idx, queryPt[0] += dx)
			{
				Z = -9999.0;
				q1 = q2 = q3= q4 = false;
				nQ = 0;
				sw = sx = sy = sz = xx = xy = xz = yy = yz = 0.0;
				ZpMin =  FLT_MAX; 
				ZpMax = -FLT_MAX;

				this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0);
				if (this->nnDists[0] < maxDistMinDist)
				{
					for (NPoints = 0; (NPoints < N) && 
									  (this->nnIdx[NPoints] != ANN_NULL_IDX) && 
									  (this->nnDists[NPoints] != ANN_DIST_INF)
									  /*begin*/ && (this->nnDists [NPoints] < maxDistInterpolate) /*end*/; ++NPoints)
					{		
						ANNpoint ap = this->annPoints[this->nnIdx[NPoints]];
						x = ap[0] - queryPt[0];
						y = ap[1] - queryPt[1];
						z = ap[2];

						if (this->nnDists [NPoints] < minsquareDistForWeight) w = 1.0 / minsquareDistForWeight;
						else w = 1.0 / this->nnDists [NPoints];

						if (positiveBranch && negativeBranch && approxBuf) 
						{
							long dX = (long) floor((ap[0] - double(x0)) * dxInv + 0.5); 
							long dY = (long) floor((ap[1] - double(y0)) * -dyInv+ 0.5);
							if ((dX >= 0) && (dY >= 0) && (dX < width) && (dY < height))
							{
								long index = (dY + offsetY) * bufwidth + dX + offsetX;								
								res = z - double(approxBuf[index]);
								if (res < 0) w *= negativeBranch->getWeight(res);
								else w *= positiveBranch->getWeight(res);
							}
						}

						if (w > FLT_EPSILON)
						{
							if (x > FLT_EPSILON)
							{
								if (y > FLT_EPSILON)
								{
									if (!q1) ++nQ;
									q1 = true;
								}
								else if (y < -FLT_EPSILON)
								{
									if (!q4) ++nQ;
									q4 = true;
								}
							}
							else if (x < -FLT_EPSILON)
							{
								if (y > FLT_EPSILON)
								{
									if (!q2) ++nQ;
									q2 = true;
								}
								else if (y < -FLT_EPSILON)
								{
									if (!q3) ++nQ;
									q3 = true;
								}
							}


							xw = x * w;
							yw = y * w;
							if (ZpMin > z) ZpMin = z;
							if (ZpMax < z) ZpMax = z;

							sw += w; sx += xw;      sy += yw;      sz += z * w;
									 xx += x * xw;  xy += x * yw;  xz += z * xw;
													yy += y * yw;  yz += z * yw;
						}
					}

					if((sw == 0)||(nQ < 4))
					{
						++ErrorCount;
					}
			
					if ((nQ > 3) && (xx > FLT_EPSILON))
					{
						fac = sx / xx;
						swred = sw - sx * fac;  syred = sy - xy * fac; szred = sz - xz * fac;

						fac = xy / xx;
						sy -= sx * fac;  yy -= xy * fac; yz -= xz * fac;

						if (yy > FLT_EPSILON)
						{
							fac = syred / xx;
							sw = swred - sy * fac;
							sz = szred - yz * fac;
						}
					}
					
					if (fabs(sw) > FLT_EPSILON)
					{
						Z = float(sz / sw);

						if (ZpMin > Z) Z = (float) ZpMin;
						if (ZpMax < Z) Z = (float) ZpMax;
					
						if (Z < zmin)  zmin = (float) Z;
						if (Z > zmax)  zmax = (float) Z;
					}
				}
				
				interpolatedBuf[idx] = (float) Z;
				if (usedBuf)
				{
					if (Z > -9999.0f) usedBuf[idx] = 255;
					else 
					{
						interpolatedBuf[idx] = (float) (this->annPoints[this->nnIdx[0]])[2];
						usedBuf[idx] = 0;
					}
				}
			}
		}

		if (positiveBranch && negativeBranch && approxBuf)
		{
			ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
			oss1.precision(3);
			oss1 << "\n Number of Errorpoints: " << ErrorCount
				 << "\n" << ends;
			protHandler.print(oss1, protHandler.prt);
		}

		annDeallocPt(queryPt);
	}

	return ret;
};

//================================================================================

bool CGenericPointCloud::interpolateRasterFinalDTM(float *interpolatedBuf, float *approxBuf, 
											       int width, int height, int bufwidth,
											       long index0, double x0, double y0, double dx, double dy, 
												   int offsetX, int offsetY,
											       int N, float &zmin, float &zmax,
											       double maxDistInterpolate,
												   double maxDistMinDist,
												   double minsquareDistForWeight, 
											       CRobustWeightFunction *positiveBranch, 
											       CRobustWeightFunction *negativeBranch, ofstream &DTMKoord,
												   unsigned char *usedBuf)
{
	bool ret = this->dimension >= 3 && this->isInitialised();
	if (ret)
	{
		double dxInv = 1.0 / dx;
		double dyInv = 1.0 / dy;
		double wInv = 1.0  / minsquareDistForWeight;
		maxDistInterpolate *= maxDistInterpolate;
		maxDistMinDist *= maxDistMinDist;
		zmin = FLT_MAX;
		zmax = -FLT_MAX;
		bool q1, q2, q3, q4;
		short nQ;
		int NPoints;
		int ErrorCount = 0;

		this->initQueryPoints(N);

		ANNpoint queryPt = annAllocPt(annDim);

		queryPt[1] = y0;
	
		long maxIdxAppx = bufwidth * height;


		double x, y, z;
		float Z;

		double sw, sx, sy, sz, xx, xy, xz, yy, yz, w, xw, yw, res, fac, syred, swred, szred;
		double ZpMin, ZpMax;
		for (int r = 0; r < height; ++r, queryPt[1] -= dy, index0 += bufwidth)
		{
			long idx = index0;
			long idxmax = idx + width;
			queryPt[0] = x0;


			for (; idx < idxmax; ++idx, queryPt[0] += dx)
			{
				Z = -9999.0;
				q1 = q2 = q3= q4 = false;
				nQ = 0;
				sw = sx = sy = sz = xx = xy = xz = yy = yz = 0.0;
				ZpMin =  FLT_MAX; 
				ZpMax = -FLT_MAX;

				this->kdTree->annkSearch(queryPt, N, this->nnIdx, this->nnDists, 0.0);
				if (this->nnDists[0] < maxDistMinDist)
				{
					for (NPoints = 0; (NPoints < N) && 
									  (this->nnIdx[NPoints] != ANN_NULL_IDX) && 
									  (this->nnDists[NPoints] != ANN_DIST_INF)
									  /*begin*/ && (this->nnDists [NPoints] < maxDistInterpolate) /*end*/; ++NPoints)
					{		
						ANNpoint ap = this->annPoints[this->nnIdx[NPoints]];
						x = ap[0] - queryPt[0];
						y = ap[1] - queryPt[1];
						z = ap[2];

						if (this->nnDists [NPoints] < minsquareDistForWeight) w = 1.0 / minsquareDistForWeight;
						else w = 1.0 / this->nnDists [NPoints];

						if (positiveBranch && negativeBranch && approxBuf) 
						{
							long dX = (long) floor((ap[0] - double(x0)) * dxInv + 0.5); 
							long dY = (long) floor((ap[1] - double(y0)) * -dyInv+ 0.5);
							if ((dX >= 0) && (dY >= 0) && (dX < width) && (dY < height))
							{
								long index = (dY + offsetY) * bufwidth + dX + offsetX;								
								res = z - double(approxBuf[index]);
								if (res < 0) w *= negativeBranch->getWeight(res);
								else w *= positiveBranch->getWeight(res);
							}
						}

						if (w > FLT_EPSILON)
						{
							if (x > FLT_EPSILON)
							{
								if (y > FLT_EPSILON)
								{
									if (!q1) ++nQ;
									q1 = true;
								}
								else if (y < -FLT_EPSILON)
								{
									if (!q4) ++nQ;
									q4 = true;
								}
							}
							else if (x < -FLT_EPSILON)
							{
								if (y > FLT_EPSILON)
								{
									if (!q2) ++nQ;
									q2 = true;
								}
								else if (y < -FLT_EPSILON)
								{
									if (!q3) ++nQ;
									q3 = true;
								}
							}


							xw = x * w;
							yw = y * w;
							if (ZpMin > z) ZpMin = z;
							if (ZpMax < z) ZpMax = z;

							sw += w; sx += xw;      sy += yw;      sz += z * w;
									 xx += x * xw;  xy += x * yw;  xz += z * xw;
													yy += y * yw;  yz += z * yw;
						}
					}

					if((sw == 0)||(nQ < 4))
					{
						++ErrorCount;
					}
			
					if ((nQ > 3) && (xx > FLT_EPSILON))
					{
						fac = sx / xx;
						swred = sw - sx * fac;  syred = sy - xy * fac; szred = sz - xz * fac;

						fac = xy / xx;
						sy -= sx * fac;  yy -= xy * fac; yz -= xz * fac;

						if (yy > FLT_EPSILON)
						{
							fac = syred / xx;
							sw = swred - sy * fac;
							sz = szred - yz * fac;
						}
					}
					
					if (fabs(sw) > FLT_EPSILON)
					{
						Z = float(sz / sw);

						if (ZpMin > Z) Z = (float) ZpMin;
						if (ZpMax < Z) Z = (float) ZpMax;
					
						if (Z < zmin)  zmin = (float) Z;
						if (Z > zmax)  zmax = (float) Z;
					}
				}

				if (Z != -9999.0f)
				{
					DTMKoord << queryPt[0] << " " << queryPt[1] << " " << Z << endl;
				}
				
				interpolatedBuf[idx] = (float) Z;
				if (usedBuf)
				{
					if (Z > -9999.0f) usedBuf[idx] = 255;
					else 
					{
						interpolatedBuf[idx] = (float) (this->annPoints[this->nnIdx[0]])[2];
						usedBuf[idx] = 0;
					}
				}
			}
		}

		if (positiveBranch && negativeBranch && approxBuf)
		{
			ostrstream oss1; oss1.setf(ios::fixed, ios::floatfield);
			oss1.precision(3);
			oss1 << "\n Number of Errorpoints: " << ErrorCount
				 << "\n" << ends;
			protHandler.print(oss1, protHandler.prt);
		}

		annDeallocPt(queryPt);
	}

	return ret;
};
