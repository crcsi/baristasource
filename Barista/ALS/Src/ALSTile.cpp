#include "ALSTile.h"
#include "XYLine.h"


CALSTile::~CALSTile()
{
	deletePoints();
};


bool CALSTile::overlap(const C3DPoint &Pmin, const C3DPoint &Pmax) const
{
	if (this->xmax < Pmin.x) return false;
	if (this->xmin > Pmax.x) return false;
	if (this->ymax < Pmin.y) return false;
	if (this->ymin > Pmax.y) return false;

	return true;
};
 
bool CALSTile::isInside(const C3DPoint &P) const
{
	if ((P.x < this->xmin) || (P.x > this->xmax)) return false;
	if ((P.y < this->ymin) || (P.y > this->ymax)) return false;

	return true;
};

bool CALSTile::isInside(const CALSPoint *P) const
{
	if (((*P)[0] < this->xmin) || ((*P)[0] > this->xmax)) return false;
	if (((*P)[1] < this->ymin) || ((*P)[1] > this->ymax)) return false;

	return true;
};

void CALSTile::addPoint(CALSPoint *p) 
{ 
	if (this->nPoints >= this->capacity)
	{
		this->capacity += 2048;
		CALSPoint **newVec = new CALSPoint *[this->capacity];
		int i = 0;
		for (i = 0; i < this->nPoints; ++i) 
		{
			newVec[i] = this->points[i];
		}

		for (; i < this->capacity; ++i) newVec[i] = 0;

		delete [] this->points;

		this->points = newVec;
	}

	this->points[this->nPoints] = p; 
	this->nPoints++; 
	double z = (*p)[2];

	if (z > this->zmax) this->zmax = z;
	if (z < this->zmin) this->zmin = z;
};

void CALSTile::deletePoint(int index)
{
	if (index < this->capacity)
	{
		delete this->points[index];
		for (int i = index + 1; i < this->nPoints; ++i)
		{
			this->points[i - 1] = this->points[i];
		}
		this->points[this->nPoints - 1] = 0;
		--this->nPoints;
		if (!this->nPoints) 
		{
			delete [] this->points;
			this->points = 0;
			this->capacity = 0;
		}
	}
};

void CALSTile::deletePoints()
{
	if (this->capacity)
	{
		for (int i = 0; i < this->nPoints; ++i) 
		{
			delete this->points[i];
			this->points[i] = 0;
		}

		delete [] this->points;
	}

	this->capacity = 0;
};

bool CALSTile::dump(FILE* fp) 
{
	bool ret = true;

	fgetpos(fp, &this->offset);
	ret = (fwrite(&this->nPoints, sizeof(int), 1, fp) == 1);

	double d = this->xmin;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->ymin;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->zmin;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->xmax;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->ymax;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->zmax;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);

	ret &= (fwrite(&this->level, sizeof(int), 1, fp) == 1);
	
	for (int i = 0; i < this->nPoints; ++i) 
	{
		CALSPoint *p = (*this)[i];
		ret &= p->dump(fp);
	}

	return ret;
};

//================================================================================

bool CALSTile::dumpHdr(FILE* fp) 
{
	bool ret = true;

	ret = (fwrite(&this->offset, sizeof(fpos_t), 1, fp) == 1);
	ret &= (fwrite(&this->nPoints, sizeof(int), 1, fp) == 1);

	double d[6];
	d[0] = this->xmin;
	d[1] = this->ymin;
	d[2] = this->zmin;
	d[3] = this->xmax;
	d[4] = this->ymax;
	d[5] = this->zmax;

	ret &= (fwrite(&d, sizeof(double), 6, fp) == 6);
	ret &= (fwrite(&this->level, sizeof(int), 1, fp) == 1);

	return ret;
};

//================================================================================

bool CALSTile::retrieve(FILE* fp)
{
	fgetpos(fp, &this->offset);
	
	this->deletePoints();

	bool ret = (fread(&this->nPoints, sizeof(int), 1, fp) == 1);

	double d[6];
	ret &= (fread(&d, sizeof(double), 6, fp) == 6);
	this->xmin = d[0];
	this->ymin = d[1];
	this->zmin = d[2];
	this->xmax = d[3];
	this->ymax = d[4];
	this->zmax = d[5];
	ret &= (fread(&this->level, sizeof(int), 1, fp) == 1);

	return ret;
};

bool CALSTile::retrieveHdr(FILE* fp)
{
	this->deletePoints();

	bool ret = (fread(&this->offset, sizeof(fpos_t), 1, fp) == 1);

	ret &= (fread(&this->nPoints, sizeof(int), 1, fp) == 1);

	double d[6];
	ret &= (fread(&d, sizeof(double), 6, fp) == 6);
	this->xmin = d[0];
	this->ymin = d[1];
	this->zmin = d[2];
	this->xmax = d[3];
	this->ymax = d[4];
	this->zmax = d[5];
	ret &= (fread(&this->level, sizeof(int), 1, fp) == 1);

	return ret;
};

bool CALSTile::read(FILE* fp)
{
	fsetpos(fp, &this->offset);

	int npts;
	bool ret = (fread(&npts, sizeof(int), 1, fp) == 1);

	double d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->xmin = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->ymin = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->zmin= d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->xmax = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->ymax = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->zmax = d;
	ret &= (fread(&this->level, sizeof(int), 1, fp) == 1);

	if (npts > 0)
	{
		if (this->capacity > 0)
		{
			if (npts > this->nPoints)
			{
				CALSPoint **ptsNew = new CALSPoint *[npts];
				this->capacity = npts;
				for (int i = 0; i < this->nPoints; ++i) ptsNew[i] = this->points[i];
				for (int i = this->nPoints; i < this->capacity; ++i) ptsNew[i] = new CALSPoint;

				delete [] this->points;
				this->points = ptsNew;
			}
			else if (npts < this->nPoints)
			{
				for (int i = this->nPoints; i < this->nPoints; i++)
				{
					delete this->points[i];
					this->points[i] = 0;
				}

				this->nPoints = npts;
				if ((npts < 1) && (this->capacity > 0)) this->deletePoints();
			}
		}
		else
		{
			this->nPoints = npts;
			this->capacity = this->nPoints;
			this->points = new CALSPoint *[this->capacity];
			for (int i = 0; i < this->nPoints; ++i) this->points[i] = new CALSPoint;
		}

		for (int i = 0; i < this->nPoints; ++i) 
		{
			CALSPoint *p = this->points[i];
			ret &= p->read(fp);
		}
	}

	return ret;
};
