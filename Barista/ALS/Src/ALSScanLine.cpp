#include "ALSScanLine.h"

CALSScanLine::CALSScanLine(const CALSScanLine &l) : pointCapacity(l.pointCapacity), centre(l.centre),
dir(l.dir), width(l.width), nbytes(l.nbytes), npoints(l.npoints), zmin(l.zmin), zmax(l.zmax)

{
	this->pointVector = new CALSPoint *[pointCapacity];
	int i = 0;
	for (i = 0; i < l.npoints; ++i) 
	{
		this->pointVector[i] = new CALSPoint(*l[i]);
	}

	for (; i < this->pointCapacity; ++i) this->pointVector[i] = 0;
}
	 
CALSScanLine::~CALSScanLine()
{
	this->reset();
}

void CALSScanLine::addPoint(CALSPoint *p) 
{ 
	nbytes += p->getRecordSize(); 

	if (this->npoints >= this->pointCapacity)
	{
		if (!this->pointCapacity) this->pointCapacity = 200;
		 else this->pointCapacity += 25;
		if (this->pointCapacity < this->npoints) this->pointCapacity = this->npoints;

		CALSPoint **newVec = new CALSPoint *[pointCapacity];
		int i = 0;
		for (i = 0; i < this->npoints; ++i) 
		{
			newVec[i] = this->pointVector[i];
		}
		for (; i < this->pointCapacity; ++i) newVec[i] = 0;
		
		delete [] pointVector;

		this->pointVector = newVec;
	}

	this->pointVector[npoints] = p; 
	npoints++; 
};

void CALSScanLine::removePoint(int index)
{
	CALSPoint *p = pointVector[index];
	nbytes -= p->getRecordSize(); 
	delete p;
	
	for (int i = index + 1; i < this->npoints; ++i) this->pointVector[i - 1] = this->pointVector[i];
	this->pointVector[this->npoints - 1] = 0;
	this->npoints--;
}
	  
void CALSScanLine::merge(CALSScanLine &l)
{
	for (int i = 0; i < l.npoints; ++i) 
	{
		this->addPoint(l[i]);
		l[i] = 0;
	}
	if (l.zmin < this->zmin) this->zmin = l.zmin;
	if (l.zmax > this->zmax) this->zmax = l.zmax;

	l.reset();
};

void CALSScanLine::reset()
{
	this->deletePoints();
	this->width.x = -1.0;
	this->width.y = -1.0;
	npoints = 0;
};

void CALSScanLine::initStatistics()
{
	if (this->nPoints() > 0)
	{
		this->nbytes = 8 * sizeof(double) + sizeof(int) + sizeof(long);

		CALSPoint *p1 = (*this)[0];
		CALSPoint *p2 = (*this)[nPoints() - 1];

		this->centre.x = ((*p1)[0] + (*p2)[0]) * 0.5;
		this->centre.y = ((*p1)[1] + (*p2)[1]) * 0.5;
		this->dir.x = (*p2)[0] - (*p1)[0];
		this->dir.y = (*p2)[1] - (*p1)[1];
		this->width.y = dir.getNorm();
		this->width.x = 0.0;
		this->dir *= 0.5f;

		this->zmin = FLT_MAX;
		this->zmax = -FLT_MAX;

		C2DPoint normal(dir.y, -dir.x);
		normal.normalise();
		double euc = -centre.innerProduct(normal);
		
		for (int i = 0; i < this->nPoints(); ++i)
		{
			CALSPoint *p = (*this)[i];

			double d = fabs((*p)[0] * normal.x + (*p)[1] * normal.y + euc);
			if (d > this->width.x) this->width.x = d;
			nbytes += p->getRecordSize();


			if ((*p)[2] < this->zmin) this->zmin = (*p)[2];
			if ((*p)[2] > this->zmax) this->zmax = (*p)[2];
		}

		this->width.x *= 2.0;
	}
};

bool CALSScanLine::dump(FILE* fp) 
{
	bool ret = true;

	if (this->nPoints() < 1) ret = false;
	else
	{
		if (this->width.x < 0) this->initStatistics();
		int npts = this->nPoints();
		fgetpos(fp, &this->offset);
		ret = (fwrite(&npts, sizeof(int), 1, fp) == 1);
		ret &= (fwrite(&this->nbytes, sizeof(long), 1, fp) == 1);

		double d = this->centre.x;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->centre.y;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->dir.x;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->dir.y;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->width.x;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->width.y;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		ret &= (fwrite(&this->zmin, sizeof(double), 1, fp) == 1);
		ret &= (fwrite(&this->zmax, sizeof(double), 1, fp) == 1);
		
		for (int i = 0; i < this->npoints; ++i) 
		{
			CALSPoint *p = (*this)[i];
			ret &= p->dump(fp);
		}
	}
	return ret;
};

bool CALSScanLine::retrieve(FILE* fp)
{
	fgetpos(fp, &this->offset);
	bool ret = (fread(&this->npoints, sizeof(int), 1, fp) == 1);
	ret &= (fread(&this->nbytes, sizeof(long), 1, fp) == 1);
	double d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->centre.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->centre.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->dir.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->dir.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->width.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->width.y = d;
	ret &= (fread(&this->zmin, sizeof(double), 1, fp) == 1);
	ret &= (fread(&this->zmax, sizeof(double), 1, fp) == 1);

	return ret;
}

bool CALSScanLine::read(FILE* fp, bool getOffset)
{
	if (getOffset) fgetpos(fp, &this->offset);
	else fsetpos(fp, &this->offset);

	bool ret = (fread(&this->npoints, sizeof(int), 1, fp) == 1);
	ret &= (fread(&this->nbytes, sizeof(long), 1, fp) == 1);
	double d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->centre.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->centre.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->dir.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->dir.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->width.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->width.y = d;
	ret &= (fread(&this->zmin, sizeof(double), 1, fp) == 1);
	ret &= (fread(&this->zmax, sizeof(double), 1, fp) == 1);

	if (!this->pointVector) 
	{
		this->pointCapacity = this->nPoints();
		this->pointVector = new CALSPoint *[this->pointCapacity];
		for (int i = 0; i < this->pointCapacity; ++i)
			this->pointVector[i] = 0;
	};

	for (int i = 0; i < this->npoints; ++i) 
	{
		CALSPoint *p = new CALSPoint;
		ret &= p->read(fp);
		if (!ret) delete p;
		else (*this)[i] = p;
	};

	return ret;
};

void CALSScanLine::deletePoints()
{
	if (this->pointVector)
	{
		for (int i = 0; i < this->npoints; ++i) 
		{
			CALSPoint *p = (*this)[i];
			if (p)
			{
				delete p;
				(*this)[i] = 0;
			}
		}
		delete [] this->pointVector;
	}
	
	this->pointVector = 0;
	this->pointCapacity = 0;
};

bool CALSScanLine::isActive() const
{ 
	bool ret = true;
	if (nPoints() <= 0) ret = false;
	else if (pointCapacity <= 0) ret = false;

	return ret;
}

bool CALSScanLine::potentialOverlap(const C3DPoint &pMin, const C3DPoint &pMax)
{
	bool ret = true;

	

	return ret;
};
