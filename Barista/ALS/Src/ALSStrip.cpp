#include "ALSStrip.h"
#include "XYLine.h"

CALSStrip::CALSStrip(const CALSStrip &s) : centre(s.centre),
dir(s.dir), width(s.width), nbytes(s.nbytes), dpointDist(s.dpointDist), capacity(s.capacity),
nScanlines(s.nScanlines), sinPhi (s.sinPhi), cosPhi(s.cosPhi), nPoints(s.nPoints), nSublevelOffset(s.nSublevelOffset)
{
	scanlines = new CALSScanLine *[this->capacity];
	
	int i = 0;
	
	for (i = 0; i < s.nScanlines; ++i) 
	{
		scanlines[i] = new CALSScanLine(*s[i]);
	}
	
	for (; i < this->capacity; ++i) this->scanlines[i] = 0;

	for (i = 0; i < s.getNSubLevels(); ++i)
	{
		CALSStrip *sublevel = new CALSStrip(*s.sublevels[i]);
		this->sublevels.push_back(sublevel);
	}
}

CALSStrip::~CALSStrip()
{
	for (int i = 0; i < this->nScanlines; ++i) 
	{
		delete this->scanlines[i];
		this->scanlines[i] = 0;
	}
	delete [] this->scanlines;
	this->scanlines = 0;

	for (int i = 0; i < this->getNSubLevels(); ++i)
	{
		delete this->sublevels[i];
		this->sublevels[i] = 0;
	}
};

 void CALSStrip::addScanline(CALSScanLine *l) 
 { 
	 if (this->nScanlines >= this->capacity)
	 {
		this->capacity += 100;
		CALSScanLine **newVec = new CALSScanLine *[this->capacity];
		int i = 0;
		for (i = 0; i < this->nScanlines; ++i) 
		{
			newVec[i] = this->scanlines[i];
		}
		for (; i < this->capacity; ++i) newVec[i] = 0;

		delete [] scanlines;

		this->scanlines = newVec;
	}

	this->scanlines[this->nScanlines] = l; 
	this->nScanlines++; 

 };

void CALSStrip::deleteScanLines() 
{ 
	for (int i = 0; i < nScans(); ++i) this->deleteScanLine(i); 

	for (int i = 0; i < this->getNSubLevels(); ++i)
	{
		CALSStrip *s = this->sublevels[i];
		for (int i = 0; i < s->nScans(); ++i) s->deleteScanLine(i); 
	}
};

void CALSStrip::deleteScanLines(int level) 
{ 
	CALSStrip *s = this;

	if (level > 0) --level;
	
	if (level < this->getNSubLevels())
	{
		if (level > 0) s = this->sublevels[level];

		for (int i = 0; i < s->nScans(); ++i) s->deleteScanLine(i); 
	}
};

void CALSStrip::getMinMax(C3DPoint &Min, C3DPoint &Max)
{
	C2DPoint normal(dir.y, -dir.x);
	normal.normalise();
	normal *= this->width.x * 0.5;

	C2DPoint p1 = this->centre + this->dir;
	C2DPoint p2 = p1 + normal;
	p1 -= normal;
	C2DPoint p3 = this->centre - this->dir;
	C2DPoint p4 = p3 + normal;
	p3 -= normal;
	Min.x = p1.x;
	if (p2.x < Min.x) Min.x = p2.x;
	if (p3.x < Min.x) Min.x = p3.x;
	if (p4.x < Min.x) Min.x = p4.x;
	Min.y = p1.y;
	if (p2.y < Min.y) Min.y = p2.y;
	if (p3.y < Min.y) Min.y = p3.y;
	if (p4.y < Min.y) Min.y = p4.y;
	Max.x = p1.x;
	if (p2.x > Max.x) Max.x = p2.x;
	if (p3.x > Max.x) Max.x = p3.x;
	if (p4.x > Max.x) Max.x = p4.x;
	Max.y = p1.y;
	if (p2.y > Max.y) Max.y = p2.y;
	if (p3.y > Max.y) Max.y = p3.y;
	if (p4.y > Max.y) Max.y = p4.y;
	Min.z = this->zmin;
	Max.z = this->zmax;
};
	  
int CALSStrip::getMinScanIdx(double xMin)
{
	int idx = 0;

	if (xMin > -this->width.y * 0.5f) 
	{
		idx = (int) floor((xMin + this->width.y * 0.5) / this->scanDist());
		if (idx < 0) idx = 0;
		else if (idx >= this->nScanlines) idx = this->nScanlines - 1;

		int didx = 0;
		int maxIt = 25;
		int it = 0;

		do
		{
			if (idx < 0) idx = 0;
			else if (idx >= this->nScanlines) idx = this->nScanlines - 1;

			CALSScanLine *l = (*this)[idx];
			C2DPoint p1 = l->getCentre() + l->getDir();
			C2DPoint p2 = l->getCentre() - l->getDir();
			this->toStripCoords(p1.x, p1.y, p1.x, p1.y);
			this->toStripCoords(p2.x, p2.y, p2.x, p2.y);
			if (p2.x > p1.x) p1.x = p2.x;
			p1.x += l->getWidth().x * 0.5;

			double dx = xMin - p1.x;

			if (fabs(dx) > this->scanDist())
			{
				didx = (int) floor(dx / this->scanDist());
				idx += didx;
			}
			else didx = 0;

			++it;
		} while ((didx) && (it < maxIt));

		idx -= 2;

		if (idx < 0) idx = 0;
		else if (idx >= this->nScanlines) idx = this->nScanlines - 1;
	};
	return idx;
};

int CALSStrip::getMaxScanIdx(double xMax)
{
	int idx = this->nScanlines - 1;

	if (xMax < this->width.y * 0.5f) 
	{
		idx = (int) floor((xMax + this->width.y * 0.5) / this->scanDist());
		if (idx < 0) idx = 0;
		else if (idx >= this->nScanlines) idx = this->nScanlines - 1;

		int didx = 0;

		int maxIt = 25;
		int it = 0;

		do
		{
			if (idx < 0) idx = 0;
			else if (idx >= this->nScanlines) idx = this->nScanlines - 1;

			CALSScanLine *l = (*this)[idx];
			C2DPoint p1 = l->getCentre() + l->getDir();
			C2DPoint p2 = l->getCentre() - l->getDir();
			this->toStripCoords(p1.x, p1.y, p1.x, p1.y);
			this->toStripCoords(p2.x, p2.y, p2.x, p2.y);
			if (p2.x < p1.x) p1.x = p2.x;
			p1.x -= l->getWidth().x * 0.5;

			double dx = xMax - p1.x;

			if (fabs(dx) > this->scanDist())
			{
				didx = (int) floor(dx / this->scanDist());
				idx += didx;
			}
			else didx = 0;
			++it;

		} while ((didx) && (it < maxIt));

		idx += 2;

		if (idx < 0) idx = 0;
		else if (idx >= this->nScanlines) idx = this->nScanlines - 1;
	};

	return idx;
};

bool CALSStrip::overlap(C3DPoint pmin, C3DPoint pmax, int &minScIdx, int &maxScIdx)
{
	minScIdx = -1;
	maxScIdx = -1;

	C2DPoint p1(pmin.x, pmin.y);
	C2DPoint p2(pmax.x, pmin.y);
	C2DPoint p3(pmax.x, pmax.y);
	C2DPoint p4(pmin.x, pmax.y);

	this->toStripCoords(p1.x, p1.y, p1.x, p1.y);
	this->toStripCoords(p2.x, p2.y, p2.x, p2.y);
	this->toStripCoords(p3.x, p3.y, p3.x, p3.y);
	this->toStripCoords(p4.x, p4.y, p4.x, p4.y);
	 
	double dx = this->dir.getNorm();
	double dy = this->dir.y / dx;

	bool ret = true;

	double maxX, maxY;
	maxY = this->width.x * 0.5;
	maxX = this->width.y * 0.5 + 50.0;
	if (((p1.x < -maxX) && (p2.x < -maxX) && (p3.x < -maxX) && (p4.x < -maxX)) ||
		((p1.y < -maxY) && (p2.y < -maxY) && (p3.y < -maxY) && (p4.y < -maxY)) ||
		((p1.x >  maxX) && (p2.x >  maxX) && (p3.x >  maxX) && (p4.x >  maxX)) ||
		((p1.y >  maxY) && (p2.y >  maxY) && (p3.y >  maxY) && (p4.y >  maxY))) ret = false;
	else
	{
		double xMin = p1.x; 
		if (p2.x < xMin) xMin = p2.x;
		if (p3.x < xMin) xMin = p3.x;
		if (p4.x < xMin) xMin = p4.x;
		double xMax = p1.x;
		if (p2.x > xMax) xMax = p2.x;
		if (p3.x > xMax) xMax = p3.x;
		if (p4.x > xMax) xMax = p4.x;

		xMin -= 50.0;
		xMax += 50.0;
		minScIdx = this->getMinScanIdx(xMin);
		maxScIdx = this->getMaxScanIdx(xMax);		
	}

	return ret;
};
	  
void CALSStrip::initStatistics()
{
	if (this->nScans() > 0)
	{
		this->nbytes = 9 * sizeof(double) + 3 * sizeof(int) + 2 * sizeof(long);

		long npoints = 0; 
		double var = 0.0;
		this->dpointDist = 0.0;

		for (int i = 0; i < this->nScans(); ++i)
		{
			CALSScanLine *l = (*this)[i];
			npoints += l->nPoints();
			var += l->nPoints() * l->nPoints();
			nbytes += l->sizeByte();
			this->dpointDist += l->pointDist();
		}

		this->dpointDist /= (double) this->nScans();
		
		double d1 = npoints;
		var -= d1 * d1 / double(this->nScans());
			
		if (this->nScans() > 1)
			var = sqrt(var / double(this->nScans() - 1));
		else
			var = 1.0f;


		npoints /= this->nScans();

		int nlinesIn = 0;
		long minPoints = npoints - long(2.0 * var);
		if (var <= 1.0) minPoints--;

		C2DPoint *pointVec = new C2DPoint[this->nScans()];
		for (int i = 0; i < this->nScans(); ++i)
		{
			CALSScanLine *l = (*this)[i];
			if (l ->nPoints() > minPoints)
			{
				pointVec[nlinesIn] = l->getCentre();
				nlinesIn++;
			}
		}

		if (nlinesIn > 1)
		{
			CXYLine line; 

			line.initFromVector(pointVec, nlinesIn);

			C2DPoint P1 = line.getBasePoint((*this)[0]->getCentre());
			C2DPoint P2 = line.getBasePoint((*this)[nScans() - 1]->getCentre());

			
			this->centre = (P1 + P2) * 0.5;
			this->dir    =  P2 - P1;
			
			this->width.y = dir.getNorm();
			this->width.x = 0.0;
			this->dir *= 0.5f;

			C2DPoint P;
				
			this->nPoints = 0;
			this->zmin = FLT_MAX;
			this->zmax = -FLT_MAX;

			for (int i = 0; i < this->nScans(); ++i)
			{
				CALSScanLine *l = (*this)[i];
				CALSPoint *p = (*l)[0];
				P.x = p->x(0); P.y = p->y(0); 
				double d = line.distanceFromLine (P);
				if (d > this->width.x) this->width.x = d;
							
				p = (*l)[l->nPoints() - 1];
				P.x = p->x(0); P.y = p->y(0); 
				d = line.distanceFromLine (P);
				if (d > this->width.x) this->width.x = d;

				this->nPoints += l->nPoints();
				if (this->zmin > l->getZmin()) this->zmin = l->getZmin();
				if (this->zmax < l->getZmax()) this->zmax = l->getZmax();
			}
		}
		else
		{
			CALSScanLine *l = (*this)[0];
			
			this->centre = pointVec[0];
			this->dir    =  l->getDir();
			double hlp = this->dir.x;
			this->dir.x = this->dir.y;
			this->dir.y = hlp;
			this->dir.normalise();
			
			this->width.y = l->getWidth().y;
			this->width.x = l->getWidth().x;
			
			C2DPoint P;
				
			this->nPoints = 0;
			this->zmin = FLT_MAX;
			this->zmax = -FLT_MAX;

			for (int i = 0; i < this->nScans(); ++i)
			{
				CALSScanLine *l = (*this)[i];
				this->nPoints += l->nPoints();
				if (this->zmin > l->getZmin()) this->zmin = l->getZmin();
				if (this->zmax < l->getZmax()) this->zmax = l->getZmax();
			}
		}

		delete [] pointVec;

		this->width.x *= 2.0;
		this->cosPhi = this->dir.getNorm();
		this->sinPhi = this->dir.y / this->cosPhi;
		this->cosPhi = this->dir.x / this->cosPhi;
		this->nSublevelOffset = this->nbytes;
	}
};

bool CALSStrip::dump(FILE* fp, bool first) 
{
	bool ret = true;

	if (this->nScans() < 1) ret = false;
	else
	{
		if (this->width.x < 0) this->initStatistics();
		fgetpos(fp, &this->offset);
		ret = (fwrite(&this->nScanlines, sizeof(int), 1, fp) == 1);
		ret &= (fwrite(&this->nPoints, sizeof(int), 1, fp) == 1);
		ret &= (fwrite(&this->nbytes, sizeof(long), 1, fp) == 1);
		ret &= (fwrite(&this->nSublevelOffset, sizeof(long), 1, fp) == 1);
		int nSubLevels = getNSubLevels();
		ret &= (fwrite(&nSubLevels, sizeof(int), 1, fp) == 1);

		double d = this->centre.x;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->centre.y;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->dir.x;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->dir.y;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->width.x;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		d = this->width.y;
		ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
		ret &= (fwrite(&this->dpointDist, sizeof(double), 1, fp) == 1);
		ret &= (fwrite(&this->zmin, sizeof(double), 1, fp) == 1);
		ret &= (fwrite(&this->zmax, sizeof(double), 1, fp) == 1);

		for (int i = 0; i < this->nScanlines; ++i) 
		{
			CALSScanLine *l = (*this)[i];
			ret &= l->dump(fp);
		}

		if (first)
		{
			if (this->nPoints > 1000)
			{
				initSublevel();
				nSubLevels = 1;
				this->sublevels[nSubLevels - 1]->dump(fp, false);
				this->nbytes += sublevels[nSubLevels - 1]->nbytes;

				while (this->sublevels[nSubLevels - 1]->nPoints > 1000)
				{
					initSublevel();
					nSubLevels++;
					this->sublevels[nSubLevels - 1]->dump(fp, false);
					this->nbytes += sublevels[nSubLevels - 1]->nbytes;
				}
			}

			for (int i = 0; i < this->getNSubLevels(); ++i) 
			{
				this->sublevels[i]->deleteScanLines();
			}

			fpos_t offsetCurr;
			fgetpos(fp, &offsetCurr);
#ifdef WIN32			
			fpos_t offsetSub  = this->offset + 2 * sizeof(int);
#else
			fpos_t offsetSub;
			offsetSub.__pos = this->offset.__pos + 2 * sizeof(int);
#endif

			fsetpos(fp, &offsetSub);
			ret &= (fwrite(&this->nbytes, sizeof(long), 1, fp) == 1);
			ret &= (fwrite(&this->nSublevelOffset, sizeof(long), 1, fp) == 1);
			nSubLevels = getNSubLevels();
			ret &= (fwrite(&nSubLevels, sizeof(int), 1, fp) == 1);
			fsetpos(fp, &offsetCurr);	
		}
	}

	return ret;
};
	  
void CALSStrip::swapVec(vector<CALSPoint *> &pointVec, vector<double> &inprod, long i1, long i2)
{
	CALSPoint *p = pointVec[i2];
	pointVec[i2] = pointVec[i1];
	pointVec[i1] = p;

	double d = inprod[i2];
	inprod[i2] = inprod[i1];
	inprod[i1] = d;
};

void CALSStrip::sortVecPartial(vector<CALSPoint *> &pointVec, vector<double> &inprod, long low, long high)
{
	if ( low < high )   // stopping condition for recursion!
	{
		long lo = low;
		long hi = high + 1;
		double d = inprod[low];

		for ( ; ; )
		{
			while (inprod[ ++lo ] < d );
			while (inprod[ --hi ] > d );
			if ( lo < hi ) swapVec(pointVec, inprod, lo, hi);
			else break;
		}
		swapVec(pointVec, inprod, low, hi );
		sortVecPartial(pointVec, inprod,  low, hi - 1 );
		sortVecPartial(pointVec, inprod,  hi + 1, high );
	}
};

void CALSStrip::sortVec(vector<CALSPoint *> &pointVec, vector<double> &inprod)
{
	// put the greatest value to the last position in the vector:
	long maxI = inprod.size() - 1;
	if ( maxI > 0 )
	{
		double maxD = inprod[ maxI ];
		for ( long i = maxI - 1; i >= 0; --i )
			if ( inprod[ i ] > maxD ) 
			{ 
				maxI = i; 
				maxD = inprod[ maxI ]; 
			}
			this->swapVec(pointVec, inprod, maxI, inprod.size()  - 1);
			sortVecPartial(pointVec, inprod,  0, inprod.size() - 2); 
	}
};

void CALSStrip::initSublevel()
{
	CALSStrip *sublevel = new CALSStrip;
	CALSStrip *prevlevel = this;
	if (this->getNSubLevels() > 0) prevlevel =  this->sublevels[this->getNSubLevels() - 1];

	for (int iScn = 1; iScn < prevlevel->nScans(); iScn += 2)
	{
		CALSScanLine *l1 = (*prevlevel)[iScn - 1];
		CALSScanLine *l2 = (*prevlevel)[iScn];
		int combPts = l1->nPoints() + l2->nPoints();

		vector<CALSPoint *> pointVec(combPts);

		vector<double> inprod(combPts);
		C2DPoint lDir = l1->getDir();
		lDir.normalise();
		C2DPoint lCentre = l1->getCentre();

		C2DPoint delta;

		for (int i = 0; i < l1->nPoints(); ++i)
		{
			CALSPoint *p = (*l1)[i];
			pointVec[i] = p;
			delta.x = (*p)[0] - lCentre.x;
			delta.y = (*p)[1] - lCentre.y;
			inprod[i] = delta.innerProduct(lDir);
		}

		for (int i = 0; i < l2->nPoints(); ++i)
		{
			int inew = i + l1->nPoints();
			CALSPoint *p = (*l2)[i];
			pointVec[inew] = p;
			delta.x = (*p)[0] - lCentre.x;
			delta.y = (*p)[1] - lCentre.y;
			inprod[inew] = delta.innerProduct(lDir);
		}

		sortVec(pointVec, inprod);

		CALSScanLine *l = new CALSScanLine;

		C3DPoint pmin, p;

		for (int i = 3; i < combPts; i += 4)
		{
			int iMin = i;
			pmin = pointVec[i]->getLastEcho();

			for (int j = 1; j < 4; ++j)
			{
				int ij = i-j;
				p = pointVec[ij]->getLastEcho();
				if (p.z < pmin.z)
				{
					pmin.z = p.z;
					iMin = ij;
				}
			}
			CALSPoint *pNew = new CALSPoint(*pointVec[iMin]);
			l->addPoint(pNew);
		}
		
		if (l->nPoints())
		{
		
			l->initStatistics();
			sublevel->addScanline(l);
		}
		else delete l;
	}

	sublevel->initStatistics();

	this->sublevels.push_back(sublevel);
};

bool CALSStrip::retrieve(FILE* fp, bool first)
{
	fgetpos(fp, &this->offset);
	int nscn;
	bool ret = (fread(&nscn, sizeof(int), 1, fp) == 1);
	ret &= (fread(&this->nPoints, sizeof(int), 1, fp) == 1);
	ret &= (fread(&this->nbytes, sizeof(long), 1, fp) == 1);
	ret &= (fread(&this->nSublevelOffset, sizeof(long), 1, fp) == 1);
	int nSubLevels;
	ret &= (fread(&nSubLevels, sizeof(int), 1, fp) == 1);

	double d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->centre.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->centre.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->dir.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->dir.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->width.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->width.y = d;
	ret &= (fread(&this->dpointDist, sizeof(double), 1, fp) == 1);
	ret &= (fread(&this->zmin, sizeof(double), 1, fp) == 1);
	ret &= (fread(&this->zmax, sizeof(double), 1, fp) == 1);

	if (!this->nScans())
	{
		fpos_t offsetL;
		fgetpos(fp, &offsetL);

		for (int i = 0; i < nscn; ++i) 
		{
			fsetpos(fp, &offsetL);
			CALSScanLine *l = new CALSScanLine;			
			this->addScanline(l);
			l->retrieve(fp);
#ifdef WIN32			
			offsetL += l->sizeByte();
#else
			offsetL.__pos += l->sizeByte();
#endif			
		}
	}

	this->cosPhi = this->dir.getNorm();
	this->sinPhi = this->dir.y / this->cosPhi;
	this->cosPhi = this->dir.x / this->cosPhi;

	if (first)
	{
#ifdef WIN32			
		fpos_t offsetS = this->offset + this->nSublevelOffset;
#else
		fpos_t offsetS;
		offsetS.__pos = this->offset.__pos + this->nSublevelOffset;
#endif	
		for (int i = 0; i < nSubLevels; ++i)
		{
			fsetpos(fp, &offsetS);
			CALSStrip *sublevel = new CALSStrip;
			this->sublevels.push_back(sublevel);
			sublevel->retrieve(fp, false);
#ifdef WIN32			
			offsetS += sublevel->nSublevelOffset;
#else
			offsetS.__pos += sublevel->nSublevelOffset;
#endif			
		}
	}
	return ret;
};

bool CALSStrip::read(FILE* fp)
{
	fgetpos(fp, &this->offset);
	int nscn;
	bool ret = (fread(&nscn, sizeof(int), 1, fp) == 1);
	ret &= (fread(&this->nPoints, sizeof(int), 1, fp) == 1);
	ret &= (fread(&this->nbytes, sizeof(long), 1, fp) == 1);
	ret &= (fread(&this->nSublevelOffset, sizeof(long), 1, fp) == 1);
	int nSubLevels;
	ret &= (fread(&nSubLevels, sizeof(int), 1, fp) == 1);
	double d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->centre.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->centre.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->dir.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->dir.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->width.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->width.y = d;
	ret &= (fread(&this->dpointDist, sizeof(double), 1, fp) == 1);
	ret &= (fread(&this->zmin, sizeof(double), 1, fp) == 1);
	ret &= (fread(&this->zmax, sizeof(double), 1, fp) == 1);

	bool haslines = (this->nScans() > 0);

	for (int i = 0; i < nscn; ++i) 
	{
		CALSScanLine *l;
		if (haslines) l = (*this)[i];
		else l = new CALSScanLine;
		ret &= l->read(fp);
		if (!haslines)
		{
			if (ret) this->addScanline(l);
			else delete l;
		}
	}

	for (int i = 0; i < nSubLevels; ++i)
	{
	}

	this->cosPhi = this->dir.getNorm();
	this->sinPhi = this->dir.y / this->cosPhi;
	this->cosPhi = this->dir.x / this->cosPhi;

#ifdef WIN32
	fpos_t offsetS = this->offset + this->nSublevelOffset;
#else
	fpos_t offsetS;
	offsetS.__pos = this->offset.__pos + this->nSublevelOffset;
#endif
	for (int i = 0; i < nSubLevels; ++i)
	{
		fsetpos(fp, &offsetS);
		CALSStrip *sublevel = new CALSStrip;
		this->sublevels.push_back(sublevel);
		sublevel->retrieve(fp, false);

#ifdef WIN32		
		offsetS += sublevel->nSublevelOffset;
#else
		offsetS.__pos += sublevel->nSublevelOffset;
#endif		
	}
	return ret;
};

void CALSStrip::readScanLine(int index, FILE *fp)
{
	fpos_t lOff =  (*this)[index]->getOffset();
	fsetpos(fp, &lOff);
	(*this)[index]->read(fp);
};
