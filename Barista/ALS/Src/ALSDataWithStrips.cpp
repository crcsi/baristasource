#include <strstream>
using namespace std;

#ifdef WIN32
#include <io.h>
#else
#include <fcntl.h>
#endif

#include <float.h>
#include "SystemUtilities.h"
#include "ALSDataWithStrips.h"

CALSDataSetWithStrips::CALSDataSetWithStrips(const char *filename, const CReferenceSystem &ref, bool open) : 
		CALSDataSet(filename, ref, open)
{
	if (open) CALSDataSet::retrieve();
}
	 
CALSDataSetWithStrips::~CALSDataSetWithStrips()
{
}

CALSDataSet *CALSDataSetWithStrips::getClone() const
{
	CALSDataSet *data = new CALSDataSetWithStrips(this->getFileNameChar(), this->getReferenceSystem(), true);

	return data;
}

void CALSDataSetWithStrips::destroy()
{
	 for (int i = 0; i < nStrips(); ++i) 
	 {
		 delete this->stripVec[i]; 
		 this->stripVec[i] = 0;
	 }

	 this->stripVec.clear();
};

void CALSDataSetWithStrips::addStrip(CALSStrip *strip)
{
	this->stripVec.push_back(strip);
};

void CALSDataSetWithStrips::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName relPath;
	CFileName fileSaveName = this->fileName;

	if (!Path.IsEmpty())
	{
		relPath = CSystemUtilities::RelPath(this->fileName, Path);
		fileSaveName.SetPath(relPath);
	}

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("ALS_data_filename", fileSaveName);
	token = pStructuredFileSection->addToken();
	token->setValue("ALS_hasTime", this->hasTime);
	token = pStructuredFileSection->addToken();
	token->setValue("ALS_hasIntensity", this->hasIntensity);
	token = pStructuredFileSection->addToken();
	token->setValue("ALS_PulseMode", this->pulsMode);


	CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->refsys.serializeStore(child);
};

void CALSDataSetWithStrips::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);
	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName CWD;
	if (!Path.IsEmpty())
	{
		CWD = CSystemUtilities::getCwd();
		CSystemUtilities::chDir(Path);
	}

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("ALS_data_filename"))
			this->fileName = token->getValue();
		else if(key.CompareNoCase("ALS_hasTime"))
			this->hasTime = token->getBoolValue();
		else if(key.CompareNoCase("ALS_hasIntensity"))
			this->hasIntensity = token->getBoolValue();
		else if(key.CompareNoCase("ALS_PulseMode"))
			this->pulsMode = (eALSPulseModeType) token->getIntValue();
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refsys.serializeRestore(child);
        }
    }

	if (!Path.IsEmpty())
	{
		this->fileName = CSystemUtilities::AbsPath(this->fileName);
		CSystemUtilities::chDir(CWD);
	}

	CALSDataSet::retrieve();
};

void CALSDataSetWithStrips::initStatistics()
{
	this->pMin.x = this->pMin.y = this->pMin.z = FLT_MAX;
	this->pMax.x = this->pMax.y = this->pMax.z = -FLT_MAX;
	this->pointDist.x = this->pointDist.y = 0.0;
	this->nPoints = 0;

	if (this->nStrips() > 0)
	{
		for (int i = 0; i < this->nStrips(); ++i)
		{
			C3DPoint Max, Min;
			this->stripVec[i]->getMinMax(Min, Max);
			if (Min.x < this->pMin.x) this->pMin.x = Min.x;
			if (Min.y < this->pMin.y) this->pMin.y = Min.y;
			if (Min.z < this->pMin.z) this->pMin.z = Min.z;
			if (Max.x > this->pMax.x) this->pMax.x = Max.x;
			if (Max.y > this->pMax.y) this->pMax.y = Max.y;
			if (Max.z > this->pMax.z) this->pMax.z = Max.z;
			this->pointDist.x += this->stripVec[i]->scanDist();
			this->pointDist.y += this->stripVec[i]->pointDist();
			this->nPoints += stripVec[i]->getNPoints();
		}
		this->pointDist *= 1.0 / (double) this->nStrips();
	}	
};

bool CALSDataSetWithStrips::writeHeader(FILE* fp) 
{
	bool ret = true;
	int nstr = this->nStrips();
	const char *hdr = "BARISTA_LIDAR";
	ret = (fwrite(hdr, sizeof(char), 13, fp) == 13);
	ret &= (fwrite(&nstr, sizeof(int), 1, fp) == 1);
	ret &= (fwrite(&this->nPoints, sizeof(int), 1, fp) == 1);
	double d = this->pMin.x;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->pMin.y;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->pMin.z;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->pMax.x;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->pMax.y;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->pMax.z;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->pointDist.x;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);
	d = this->pointDist.y;
	ret &= (fwrite(&d, sizeof(double), 1, fp) == 1);

	return ret;
};

bool CALSDataSetWithStrips::finishWriting(FILE* fp)
{
	this->initStatistics();
	
#ifdef WIN32
	fpos_t offset = 0;
#else
	fpos_t offset;
	offset.__pos = 0;
#endif	
	
	
    fsetpos(fp, &offset);
	bool ret = this->writeHeader(fp);
	this->close();
	return ret;
};

bool CALSDataSetWithStrips::retrieve(FILE* fp)
{
#ifdef WIN32
	fpos_t offset = 0;
#else
	fpos_t offset;
	offset.__pos = 0;
#endif	
    fsetpos(fp, &offset);
	char hdr[15];
	hdr[0] = '\0';
	bool ret = (fread(&hdr, sizeof(char), 13, fp) == 13);
	hdr[13] = '\0';

	ret &= (strcmp(&hdr[0],"BARISTA_LIDAR") == 0);
	
	int nstr;
	
	ret &= (fread(&nstr, sizeof(int), 1, fp) == 1);
	ret &= (fread(&this->nPoints, sizeof(int), 1, fp) == 1);
	
	double d;

	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->pMin.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->pMin.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->pMin.z = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->pMax.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->pMax.y = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->pMax.z = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->pointDist.x = d;
	ret &= (fread(&d, sizeof(double), 1, fp) == 1);
	this->pointDist.y = d;
	
	if (!this->nStrips() && ret)
	{
		fgetpos(fp, &offset);
		for (int i = 0; i < nstr; ++i) 
		{
			fsetpos(fp, &offset);
			CALSStrip *s = new CALSStrip;			
			this->addStrip(s);
			s->retrieve(fp);
#ifdef WIN32			
			offset += s->sizeByte();
#else
			offset.__pos += s->sizeByte();
#endif			
		}
	}

	this->initTilingStatistics();
	this->initMinCommonLevel();

	return ret;
};

void CALSDataSetWithStrips::deleteStrip(int index) 
{
	this->stripVec[index]->deleteScanLines();
};

void CALSDataSetWithStrips::deleteStrip(int index, int level) 
{
	this->stripVec[index]->deleteScanLines(level);
};

void CALSDataSetWithStrips::readStrip(int index, FILE *fp)
{
	fpos_t lOff =  this->stripVec[index]->getOffset();
	fsetpos(fp, &lOff);
	this->stripVec[index]->read(fp);
};

int CALSDataSetWithStrips::getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, int level)
{
	int ncloudPoints = 0;
	int ncloudCapacity = 2048;
	pointvec = new CALSPoint *[ncloudCapacity];

	for (int iStr = 0; iStr < this->nStrips(); ++iStr)
	{
		CALSStrip *sl = this->stripVec[iStr];
		CALSStrip *s = sl;

		int lmin, lmax;
		int lev = level - 1;
		if (lev >= sl->getNSubLevels()) lev = sl->getNSubLevels() - 1;
		
		if (lev >= 0) s = sl->getSubLevel(lev);

		if (s->overlap(pmin, pmax, lmin, lmax))
		{
			for (int iScan = lmin; iScan < lmax; ++iScan)
			{
				CALSScanLine *l = (*s)[iScan];
				bool OK = true;
				if (!l->isActive())
				{
					OK = l->read(this->fptr, false);
				}
				
				if (OK) 
				{
					for (int iPt = 0; iPt < l->nPoints(); ++iPt)
					{
						CALSPoint *p = (*l)[iPt];
						if (((*p)[0] >= pmin.x) && ((*p)[1] >= pmin.y) && 
							((*p)[0] <= pmax.x) && ((*p)[1] <= pmax.y))
						{
							CALSPoint *pNew = new CALSPoint(*p);
							if (ncloudPoints + 1 >= ncloudCapacity)
							{
								ncloudCapacity += 2048;
								CALSPoint ** newPts = new CALSPoint *[ncloudCapacity];
								for (int n = 0; n < ncloudPoints; ++n) newPts[n] = pointvec[n];
								delete [] pointvec;
								pointvec = newPts;
							}
	
							pointvec[ncloudPoints] = pNew;
							++ncloudPoints;
						}
					}
				}
			}
		}
		else 
		{
			s->deleteScanLines();
		}
	}

	if (!ncloudPoints)
	{
		delete [] pointvec;
		pointvec = 0;
	}

	return ncloudPoints;
};

int CALSDataSetWithStrips::getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, int &capacity, int dCapacity, int level)
{
	int ncloudPoints = 0;

	int nEchos = this->getMaxEchos();

	if (!capacity)
	{
		pointvec = new CALSPoint *[dCapacity];
		capacity = dCapacity;
		
		for (int i = 0; i < capacity; ++i)
		{
			pointvec[i] = new CALSPoint(nEchos);
		}
	}

	for (int iStr = 0; iStr < this->nStrips(); ++iStr)
	{
		CALSStrip *sl = this->stripVec[iStr];
		CALSStrip *s = sl;

		int lmin, lmax;
		int lev = level - 1;
		if (lev >= sl->getNSubLevels()) lev = sl->getNSubLevels() - 1;
		
		if (lev >= 0) s = sl->getSubLevel(lev);

		if (s->overlap(pmin, pmax, lmin, lmax))
		{
			for (int iScan = lmin; iScan < lmax; ++iScan)
			{
				CALSScanLine *l = (*s)[iScan];
				bool OK = true;
				if (!l->isActive())
				{
					OK = l->read(this->fptr, false);
				}
				
				if (OK) 
				{
					for (int iPt = 0; iPt < l->nPoints(); ++iPt)
					{
						CALSPoint *p = (*l)[iPt];
						if (((*p)[0] >= pmin.x) && ((*p)[1] >= pmin.y) && 
							((*p)[0] <= pmax.x) && ((*p)[1] <= pmax.y))
						{
							if (ncloudPoints + 1 >= capacity)
							{
								capacity += dCapacity;
								CALSPoint ** newPts = new CALSPoint *[capacity];
								int n = 0;
								for (; n < ncloudPoints; ++n) newPts[n] = pointvec[n];
								delete [] pointvec;
								pointvec = newPts;
								for (; n < capacity; ++n)
								{
									pointvec[n] = new CALSPoint(nEchos);
								}
							}
	
							pointvec[ncloudPoints]->copy(*p);
							++ncloudPoints;
						}
					}
				}
			}
		}
		else 
		{
			s->deleteScanLines();
		}
	}


	return ncloudPoints;
};
void CALSDataSetWithStrips::initMinCommonLevel()
{
	if (this->stripVec.size() > 1)
	{
		this->minCommonSublevel = this->stripVec[0]->getNSubLevels();

		for (unsigned int i = 1; i < this->stripVec.size(); ++i)
		{
			int nLevCurr = this->stripVec[i]->getNSubLevels();

			if (nLevCurr < this->minCommonSublevel) this->minCommonSublevel = nLevCurr;
		}
	}
	else 
	{
		this->minCommonSublevel = 0;
	}
};

bool CALSDataSetWithStrips::readOptechFile(ifstream &inFile, const C2DPoint &offset, long filesize)
{
	bool ret = true;

	float pfact = float(100.0) / float(filesize);

	int lineLen = 1024;
	char *line = new char[lineLen];
	CALSPoint *p1, *p2, *p3;
	
	p1 = getALSPointOptech(inFile, line, lineLen, offset);
	p2 = getALSPointOptech(inFile, line, lineLen, offset);
	p3 = getALSPointOptech(inFile, line, lineLen, offset);
	
	if (p1 && p2 && p3)
	{
		double deltaT  = p2->getTime() - p1->getTime();
		double deltaT1 = p3->getTime() - p2->getTime();
		while (p1 && p2 && p3 && (fabs(deltaT - deltaT1) > FLT_EPSILON))
		{
			delete p1;
			p1 = p2;
			p2 = p3;
			p3  = getALSPointOptech(inFile, line, lineLen, offset);
			deltaT  = deltaT1;
			deltaT1 = p3->getTime() - p2->getTime();
		}

		if (!ret)
		{
			if (p1) delete p1;
			if (p2) delete p2;
			if (p3) delete p3;
		}
		else
		{
			this->writeHeader(this->fptr);
	
			CALSScanLine *l = new CALSScanLine;
			CALSStrip *s = new CALSStrip;
			l->addPoint(p1);
			l->addPoint(p2); 
			l->addPoint(p3); 
			C2DPoint deltaP((*p3)[0] - (*p2)[0], (*p3)[1] - (*p2)[1]);
			deltaP.normalise();
			C2DPoint deltaPNew;
			CALSPoint *prevP = p3;
			double deltaDeltaT = 3. * deltaT;
			
			while (p3)
			{
				p3 = getALSPointOptech(inFile, line, lineLen, offset);
				if (!p3) 
				{
					if (l->nPoints()) 
					{
						l->initStatistics();
						s->addScanline(l);
					}
					else delete l;
					if (s->nScans())
					{
						this->addStrip(s);
						s->dump(this->fptr);
						s->deleteScanLines();
					}
					else delete s;
				}
				else
				{
					deltaPNew.x = (*p3)[0] - (*prevP)[0]; 
					deltaPNew.y = (*p3)[1] - (*prevP)[1]; 
					deltaPNew.normalise();
					deltaT1 = fabs(p3->dot - prevP->dot);
					if (deltaT1 > 1.0)
					{
						l->initStatistics();	
						s->addScanline(l);
						this->addStrip(s);
						s->dump(this->fptr);
						s->deleteScanLines();
						s = new CALSStrip;
						l = new CALSScanLine;
						l->addPoint(p3);
						prevP = p3;
						p3 = getALSPointOptech(inFile, line, lineLen, offset);
						deltaP.x = (*p3)[0] - (*prevP)[0]; 
						deltaP.y = (*p3)[1] - (*prevP)[1]; 
						deltaP.normalise();
						l->addPoint(p3);
						prevP = p3;
						long posF = inFile.tellg( );
						float pval = float(posF) * pfact;
						if (this->listener)						
							this->listener->setProgressValue(pval);

					} 
					else if ((deltaPNew.innerProduct(deltaP) < 0) || (fabs(deltaT1 - deltaT) > deltaDeltaT))
					{
						CALSScanLine *l1 = 0;
						l->initStatistics();
						if (s->nScans() > 0)
						{
							l1 = (*s)[s->nScans() - 1];
							if (l1->getDir().innerProduct(l->getDir()) > 0.0)
							{
								l1->merge(*l);
								l1->initStatistics();
							}
							else l1 = 0;
						}

						if (!l1)
						{
							s->addScanline(l);
							l = new CALSScanLine;
						}

						l->addPoint(p3);
						prevP = p3;
						p3 = getALSPointOptech(inFile, line, lineLen, offset);
						deltaP.x = (*p3)[0] - (*prevP)[0]; 
						deltaP.y = (*p3)[1] - (*prevP)[1];
						deltaP.normalise();
						deltaT1 = fabs(p3->dot - prevP->dot);
						if (deltaT1 > 1.0)
						{						
							this->addStrip(s);
							s->dump(this->fptr);
							s->deleteScanLines();
							s = new CALSStrip;
							l->reset();
							l->addPoint(p3);
							prevP = p3;
							p3 = getALSPointOptech(inFile, line, lineLen, offset);
							deltaP.x = (*p3)[0] - (*prevP)[0]; 
							deltaP.y = (*p3)[1] - (*prevP)[1]; 
							deltaP.normalise();
						}

						l->addPoint(p3);
						prevP = p3;
						long posF = inFile.tellg( );
						float pval = float(posF) * pfact;
						if (this->listener)						
							this->listener->setProgressValue(pval);

					}
					else
					{
						l->addPoint(p3);
						prevP = p3;
					}
				}
			}
			this->finishWriting(this->fptr);
		}
	}
	else ret = false;

	if (this->listener)	this->listener->setProgressValue(100.0);

	delete [] line;

	return ret;
};

bool CALSDataSetWithStrips::readLASFile1(FILE *inFile, const C2DPoint &offset, long filesize)
{
	bool ret = false;
/*
	float pfact = float(100.0) / float(filesize), PointDataFormat;

	unsigned char versionMajor, versionMinor;						 
	unsigned short HeaderSize, pointDataRecordLength;
	unsigned long pointOffset, NumberOfPointRecords;
	C3DPoint Scale, Offset;

	ret = readLASFileHeader(inFile, versionMajor, versionMinor, HeaderSize, pointOffset, 
		                    PointDataFormat, pointDataRecordLength,NumberOfPointRecords, 
							Scale, Offset);

	if (ret)
	{

		fpos_t offset = pointOffset;
fsetpos(FP, &offset);

		if (PointDataFormat == 0)
		{
		}
		else if (PointDataFormat == 1)
		{
			ofstream outFile(TxtName.GetChar());
			outFile.setf(ios::fixed, ios::floatfield);

			long X,Y,Z;
			unsigned short Intensity;
			unsigned char flags, classification, scanangelRank, FileMarker;
			unsigned char Nreturns, returnNumber;
			unsigned char left = 1 + 2 + 4;
			unsigned char center = 8 + 16 + 32;
			unsigned char aD = 64;
			unsigned char EF = 128;
			unsigned char left1 = 128 + 64 + 32;
			unsigned char center1 = 4 + 8 + 16;
			unsigned char aD1 = 2;
			unsigned char EF1 = 1;
			
			bool scanDir;
			bool EdgeOfFlightline;
			unsigned short UserBitField;
			double GPStime;
			
			int S = sizeof(X) + sizeof(Y) + sizeof(Z) + sizeof(Intensity) + sizeof(flags) + sizeof(classification) + sizeof(scanangelRank) + sizeof(FileMarker) + sizeof(UserBitField) + sizeof(GPStime);
			C3DPoint P;
			bool newline = true;
			for (int i = 0; i < NumberOfPointRecords; ++i)
			{
				fread(&X, sizeof(long), 1, FP);
				fread(&Y, sizeof(long), 1, FP);
				fread(&Z, sizeof(long), 1, FP);
				fread(&Intensity, sizeof(unsigned short), 1, FP);
				fread(&flags, sizeof(unsigned char), 1, FP);
				fread(&classification, sizeof(unsigned char), 1, FP);
				fread(&scanangelRank, sizeof(unsigned char), 1, FP);
				fread(&FileMarker, sizeof(unsigned char), 1, FP);
				fread(&UserBitField, sizeof(unsigned short), 1, FP);
				fread(&GPStime, sizeof(double), 1, FP);
				P.dot = GPStime;
				P.x = double(X) * Scale.x + Offset.x;
				P.y = double(Y) * Scale.y + Offset.y;
				P.z = double(Z) * Scale.z + Offset.z;
				returnNumber = flags & left;
				Nreturns = (flags & center);
				Nreturns = Nreturns >> 3;
				unsigned char SD = flags & aD;
			    scanDir = ((flags & aD) > 0);
			    EdgeOfFlightline = ((flags & EF) > 0);

				unsigned char returnNumber1 = flags & left1;
				unsigned char Nreturns1 = (flags & center1);
				Nreturns1 = Nreturns1 >> 3;
				bool scanDir1 = ((flags & aD1) > 0);
			    bool EdgeOfFlightline1= ((flags & EF1) > 0);;
			
				if (newline) 
				{
					outFile.precision(7); 
					outFile << P.dot << " ";
					outFile.precision(2);
				}

				outFile   << P.x << "  " << P.y << " " << P.z << " " << Intensity << " " ;

				if (returnNumber == Nreturns) 
				{
					outFile   << "\n";
					newline = true;
				}
				else newline = false;
			}
		}

		int hmm = 3;
	}
	int lineLen = 1024;
	char *line = new char[lineLen];
	CALSPoint *p1, *p2, *p3;
	
	p1 = getALSPointOptech(inFile, line, lineLen, offset);
	p2 = getALSPointOptech(inFile, line, lineLen, offset);
	p3 = getALSPointOptech(inFile, line, lineLen, offset);
	
	if (p1 && p2 && p3)
	{
		double deltaT  = p2->getTime() - p1->getTime();
		double deltaT1 = p3->getTime() - p2->getTime();
		while (p1 && p2 && p3 && (fabs(deltaT - deltaT1) > FLT_EPSILON))
		{
			delete p1;
			p1 = p2;
			p2 = p3;
			p3  = getALSPointOptech(inFile, line, lineLen, offset);
			deltaT  = deltaT1;
			deltaT1 = p3->getTime() - p2->getTime();
		}

		if (!ret)
		{
			if (p1) delete p1;
			if (p2) delete p2;
			if (p3) delete p3;
		}
		else
		{
			this->writeHeader(this->fp);
	
			CALSScanLine *l = new CALSScanLine;
			CALSStrip *s = new CALSStrip;
			l->addPoint(p1);
			l->addPoint(p2); 
			l->addPoint(p3); 
			C2DPoint deltaP((*p3)[0] - (*p2)[0], (*p3)[1] - (*p2)[1]);
			deltaP.normalise();
			C2DPoint deltaPNew;
			CALSPoint *prevP = p3;
			double deltaDeltaT = 3. * deltaT;
			
			while (p3)
			{
				p3 = getALSPointOptech(inFile, line, lineLen, offset);
				if (!p3) 
				{
					if (l->nPoints()) 
					{
						l->initStatistics();
						s->addScanline(l);
					}
					else delete l;
					if (s->nScans())
					{
						this->addStrip(s);
						s->dump(this->fp);
						s->deleteScanLines();
					}
					else delete s;
				}
				else
				{
					deltaPNew.x = (*p3)[0] - (*prevP)[0]; 
					deltaPNew.y = (*p3)[1] - (*prevP)[1]; 
					deltaPNew.normalise();
					deltaT1 = fabs(p3->dot - prevP->dot);
					if (deltaT1 > 1.0)
					{
						l->initStatistics();	
						s->addScanline(l);
						this->addStrip(s);
						s->dump(this->fp);
						s->deleteScanLines();
						s = new CALSStrip;
						l = new CALSScanLine;
						l->addPoint(p3);
						prevP = p3;
						p3 = getALSPointOptech(inFile, line, lineLen, offset);
						deltaP.x = (*p3)[0] - (*prevP)[0]; 
						deltaP.y = (*p3)[1] - (*prevP)[1]; 
						deltaP.normalise();
						l->addPoint(p3);
						prevP = p3;
						long posF = inFile.tellg( );
						float pval = float(posF) * pfact;
						if (this->listener)						
							this->listener->setProgressValue(pval);

					} 
					else if ((deltaPNew.innerProduct(deltaP) < 0) || (fabs(deltaT1 - deltaT) > deltaDeltaT))
					{
						CALSScanLine *l1 = 0;
						l->initStatistics();
						if (s->nScans() > 0)
						{
							l1 = (*s)[s->nScans() - 1];
							if (l1->getDir().innerProduct(l->getDir()) > 0.0)
							{
								l1->merge(*l);
								l1->initStatistics();
							}
							else l1 = 0;
						}

						if (!l1)
						{
							s->addScanline(l);
							l = new CALSScanLine;
						}

						l->addPoint(p3);
						prevP = p3;
						p3 = getALSPointOptech(inFile, line, lineLen, offset);
						deltaP.x = (*p3)[0] - (*prevP)[0]; 
						deltaP.y = (*p3)[1] - (*prevP)[1];
						deltaP.normalise();
						deltaT1 = fabs(p3->dot - prevP->dot);
						if (deltaT1 > 1.0)
						{						
							this->addStrip(s);
							s->dump(this->fp);
							s->deleteScanLines();
							s = new CALSStrip;
							l->reset();
							l->addPoint(p3);
							prevP = p3;
							p3 = getALSPointOptech(inFile, line, lineLen, offset);
							deltaP.x = (*p3)[0] - (*prevP)[0]; 
							deltaP.y = (*p3)[1] - (*prevP)[1]; 
							deltaP.normalise();
						}

						l->addPoint(p3);
						prevP = p3;
						long posF = inFile.tellg( );
						float pval = float(posF) * pfact;
						if (this->listener)						
							this->listener->setProgressValue(pval);

					}
					else
					{
						l->addPoint(p3);
						prevP = p3;
					}
				}
			}
			this->finishWriting(this->fp);
		}
	}
	else ret = false;
}
	if (this->listener)	this->listener->setProgressValue(100.0);

	delete [] line;
*/
	return ret;
};

bool CALSDataSetWithStrips::readASCIIFileWithTime(ifstream &inFile, const C2DPoint &offset, 
										bool multiple, bool timeFirst, long filesize)
{
	bool ret = true;
	float pfact = float(100.0) / float(filesize);

	int lineLen = 1024;
	char *line = new char[lineLen];
	CALSPoint *p1, *p2, *p3;
	
	p1 = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
	p2 = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
	p3 = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
	
	if (p1 && p2 && p3)
	{
		double deltaT  = p2->getTime() - p1->getTime();
		double deltaT1 = p3->getTime() - p2->getTime();
		while (p1 && p2 && p3 && (fabs(deltaT - deltaT1) > FLT_EPSILON))
		{
			delete p1;
			p1 = p2;
			p2 = p3;
			p3  = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
			deltaT  = deltaT1;
			deltaT1 = p3->getTime() - p2->getTime();
		}

		if (!ret)
		{
			if (p1) delete p1;
			if (p2) delete p2;
			if (p3) delete p3;
		}
		else
		{
			this->writeHeader(this->fptr);
	
			CALSScanLine *l = new CALSScanLine;
			CALSStrip *s = new CALSStrip;
			l->addPoint(p1);
			l->addPoint(p2); 
			l->addPoint(p3); 
			C2DPoint deltaP((*p3)[0] - (*p2)[0], (*p3)[1] - (*p2)[1]);
			deltaP.normalise();
			C2DPoint deltaPNew;
			CALSPoint *prevP = p3;
			double deltaDeltaT = 3. * deltaT;
			
			while (p3)
			{
				p3 = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
				if (!p3) 
				{
					if (l->nPoints()) 
					{
						l->initStatistics();
						s->addScanline(l);
					}
					else delete l;
					if (s->nScans())
					{
						this->addStrip(s);
						s->dump(this->fptr);
						s->deleteScanLines();
					}
					else delete s;
				}
				else
				{
					deltaPNew.x = (*p3)[0] - (*prevP)[0]; 
					deltaPNew.y = (*p3)[1] - (*prevP)[1]; 
					deltaPNew.normalise();
					deltaT1 = fabs(p3->dot - prevP->dot);
					if (deltaT1 > 1.0)
					{
						l->initStatistics();	
						s->addScanline(l);
						this->addStrip(s);
						s->dump(this->fptr);
						s->deleteScanLines();
						s = new CALSStrip;
						l = new CALSScanLine;
						l->addPoint(p3);
						prevP = p3;
						p3 = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
						deltaP.x = (*p3)[0] - (*prevP)[0]; 
						deltaP.y = (*p3)[1] - (*prevP)[1]; 
						deltaP.normalise();
						l->addPoint(p3);
						prevP = p3;
						long posF = inFile.tellg( );
						float pval = float(posF) * pfact;
						if (this->listener)						
							this->listener->setProgressValue(pval);

					} 
					else if ((deltaPNew.innerProduct(deltaP) < 0) || (fabs(deltaT1 - deltaT) > deltaDeltaT))
					{
						CALSScanLine *l1 = 0;
						l->initStatistics();
						if (s->nScans() > 0)
						{
							l1 = (*s)[s->nScans() - 1];
							if (l1->getDir().innerProduct(l->getDir()) > 0.0)
							{
								l1->merge(*l);
								l1->initStatistics();
							}
							else l1 = 0;
						}

						if (!l1)
						{
							s->addScanline(l);
							l = new CALSScanLine;
						}

						l->addPoint(p3);
						prevP = p3;
						p3 = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
						deltaP.x = (*p3)[0] - (*prevP)[0]; 
						deltaP.y = (*p3)[1] - (*prevP)[1];
						deltaP.normalise();
						deltaT1 = fabs(p3->dot - prevP->dot);
						if (deltaT1 > 1.0)
						{						
							this->addStrip(s);
							s->dump(this->fptr);
							s->deleteScanLines();
							s = new CALSStrip;
							l->reset();
							l->addPoint(p3);
							prevP = p3;
							p3 = getALSPointWithTime(inFile, line, lineLen, offset, multiple, timeFirst);
							deltaP.x = (*p3)[0] - (*prevP)[0]; 
							deltaP.y = (*p3)[1] - (*prevP)[1]; 
							deltaP.normalise();
						}

						l->addPoint(p3);
						prevP = p3;
						long posF = inFile.tellg( );
						float pval = float(posF) * pfact;
						if (this->listener)						
							this->listener->setProgressValue(pval);

					}
					else
					{
						l->addPoint(p3);
						prevP = p3;
					}
				}
			}
			this->finishWriting(this->fptr);
		}
	}
	else ret = false;

	if (this->listener)	this->listener->setProgressValue(100.0);

	delete [] line;

	return ret;
};


bool CALSDataSetWithStrips::readASCIIFileWithoutTime(ifstream &inFile, const C2DPoint &offset, 									
										   bool multiple, long filesize)
{
	bool ret = true;
	float pfact = float(100.0) / float(filesize);

	int lineLen = 1024;
	char *line = new char[lineLen];
	CALSPoint *p1, *p2, *p3;
	
	p1 = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);
	p2 = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);
	p3 = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);
	
	if (p1 && p2 && p3)
	{
		this->writeHeader(this->fptr);

		CALSScanLine *l = new CALSScanLine;
		CALSStrip *s = new CALSStrip;
		l->addPoint(p1);
		l->addPoint(p2); 
		l->addPoint(p3); 
		C2DPoint deltaP((*p3)[0] - (*p2)[0], (*p3)[1] - (*p2)[1]);
		deltaP.normalise();
		C2DPoint deltaPNew;
		CALSPoint *prevP = p3;
		
		while (p3)
		{
			p3 = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);
			if (!p3) 
			{
				if (l->nPoints()) 
				{
					l->initStatistics();
					s->addScanline(l);
				}
				else delete l;
				if (s->nScans())
				{
					this->addStrip(s);
					s->dump(this->fptr);
					s->deleteScanLines();
				}
				else delete s;
			}
			else
			{
				deltaPNew.x = (*p3)[0] - (*prevP)[0]; 
				deltaPNew.y = (*p3)[1] - (*prevP)[1]; 
				if (deltaPNew.getNorm() > 100.0)
				{
					l->initStatistics();	
					s->addScanline(l);
					this->addStrip(s);
					s->dump(this->fptr);
					s->deleteScanLines();
					s = new CALSStrip;
					l = new CALSScanLine;
					l->addPoint(p3);
					prevP = p3;
					p3 = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);
					deltaP.x = (*p3)[0] - (*prevP)[0]; 
					deltaP.y = (*p3)[1] - (*prevP)[1]; 
					deltaP.normalise();
					l->addPoint(p3);
					prevP = p3;
					long posF = inFile.tellg( );
					float pval = float(posF) * pfact;
					if (this->listener)						
						this->listener->setProgressValue(pval);

				} 
				else if (deltaPNew.innerProduct(deltaP) < 0)
				{
					CALSScanLine *l1 = 0;
					l->initStatistics();
					if (s->nScans() > 0)
					{
						l1 = (*s)[s->nScans() - 1];
						if (l1->getDir().innerProduct(l->getDir()) > 0.0)
						{
							l1->merge(*l);
							l1->initStatistics();
						}
						else l1 = 0;
					}

					if (!l1)
					{
						s->addScanline(l);
						l = new CALSScanLine;
					}

					l->addPoint(p3);
					prevP = p3;
					p3 = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);
					if (!p3)
					{
						if (l->nPoints() > 1) 
						{
							l->initStatistics();
							s->addScanline(l);
						}
						else delete l;

						if (s->nScans())
						{
							this->addStrip(s);
							s->dump(this->fptr);
							s->deleteScanLines();
						}
						else delete s;
					}
					else
					{
						deltaP.x = (*p3)[0] - (*prevP)[0]; 
						deltaP.y = (*p3)[1] - (*prevP)[1];
						if (deltaP.getNorm() > 100.0)
						{						
							this->addStrip(s);
							s->dump(this->fptr);
							s->deleteScanLines();
							s = new CALSStrip;
							l->reset();
							l->addPoint(p3);
							prevP = p3;
							p3 = getALSPointWithoutTime(inFile, line, lineLen, offset, multiple);
							deltaP.x = (*p3)[0] - (*prevP)[0]; 
							deltaP.y = (*p3)[1] - (*prevP)[1]; 
							deltaP.normalise();
						}

						l->addPoint(p3);
						prevP = p3;
						long posF = inFile.tellg( );
						float pval = float(posF) * pfact;
						if (this->listener)						
							this->listener->setProgressValue(pval);
					}
				}
				else
				{
					l->addPoint(p3);
					prevP = p3;
				}
			}
		}
		this->finishWriting(this->fptr);
	}
	else ret = false;
	
	if (this->listener)	this->listener->setProgressValue(100.0);

	delete [] line;

	return ret;
};

bool CALSDataSetWithStrips::readToposysFile(ifstream &inFile, const C2DPoint &offset, long filesize)
{
	bool ret = true;
	float pfact = float(85.0) / float(filesize);

	CALSPoint *p1, *p2, *p3;
	
	p1 = getALSPointTopoSys(inFile, offset);
	p2 = getALSPointTopoSys(inFile, offset);
	p3 = getALSPointTopoSys(inFile, offset);
	
	if (p1 && p2 && p3)
	{
		this->writeHeader(this->fptr);

		CALSScanLine *l = new CALSScanLine;
		CALSStrip *s = new CALSStrip;
		l->addPoint(p1);
		l->addPoint(p2); 
		l->addPoint(p3); 
		C2DPoint deltaP((*p3)[0] - (*p2)[0], (*p3)[1] - (*p2)[1]);
		deltaP.normalise();
		C2DPoint deltaPNew;
		CALSPoint *prevP = p3;
		
		while (p3)
		{
			p3 = getALSPointTopoSys(inFile, offset);
			if (!p3) 
			{
				if (l->nPoints()) 
				{
					l->initStatistics();
					s->addScanline(l);
				}
				else delete l;
				if (s->nScans())
				{
					this->addStrip(s);
					s->dump(this->fptr);
					s->deleteScanLines();
				}
				else delete s;
			}
			else
			{
				deltaPNew.x = (*p3)[0] - (*prevP)[0]; 
				deltaPNew.y = (*p3)[1] - (*prevP)[1]; 
				if ((deltaPNew.getNorm() > 100.0) || (deltaPNew.innerProduct(deltaP) < 0))
				{
					CALSScanLine *l1 = 0;
					l->initStatistics();
					if (s->nScans() > 0)
					{
						l1 = (*s)[s->nScans() - 1];
						if (l1->getDir().innerProduct(l->getDir()) > 0.0)
						{
							l1->merge(*l);
							l1->initStatistics();
						}
						else l1 = 0;
					}

					if (!l1)
					{
						s->addScanline(l);
						l = new CALSScanLine;
					}

					l->addPoint(p3);
					prevP = p3;
					long posF = inFile.tellg( );
					float pval = float(posF) * pfact;
					if (this->listener)						
						this->listener->setProgressValue(pval);

				}
				else
				{
					l->addPoint(p3);
					prevP = p3;
				}
		}
		}
		this->finishWriting(this->fptr);
	}
	else ret = false;

	if (this->listener)	this->listener->setProgressValue(100.0);

	return ret;
};

bool CALSDataSetWithStrips::exportXYZ(const CCharString &FileName, int echo, bool exportTime, bool exportI)
{
	bool wasOpen = true;

	bool ret = true;

	if (!this->fptr)
	{
		wasOpen = false;
		this->fptr = fopen(this->getFileNameChar(), "r+b");
	}

	if (this->fptr)
	{
		ofstream outFile(FileName.GetChar());
		if (outFile.good())
		{
			outFile.setf(ios::fixed, ios::floatfield);
			outFile.precision(2);

			for (int i = 0; i < this->nStrips(); ++i) this->dumpStripXYZ(i, outFile, echo, exportTime, exportI);
		}
		else ret = false;

		if (!wasOpen) this->close();
	}

	return ret;
};


void CALSDataSetWithStrips::dumpStripXYZ(int strip, const CCharString &FileName, int echo, bool exportTime, bool exportI)
{
	if (strip < this->nStrips())
	{
		bool wasOpen = true;

		if (!this->fptr)
		{
			wasOpen = false;
			this->fptr = fopen(this->getFileNameChar(), "r+b");
		}

		if (this->fptr)
		{
			ofstream outFile(FileName.GetChar());
			if (outFile.good())
			{
				outFile.setf(ios::fixed, ios::floatfield);
				outFile.precision(2);

				this->dumpStripXYZ(strip, outFile, echo, exportTime, exportI);
			}
			if (!wasOpen) this->close();
		}
	}
};

void CALSDataSetWithStrips::dumpStripXYZ(int strip, ofstream &outFile, int echo, bool exportTime, bool exportI)
{
	CALSStrip *s = this->getStrip(strip);
	s->read(this->fptr);
		
	int prec = outFile.precision();

	for(int i = 0; i < s->nScans(); ++i)
	{
		CALSScanLine *l = (*s)[i];

		for (int j = 0; j < l->nPoints(); ++j)
		{
			CALSPoint *p = (*l)[j];
			if (exportTime) 
			{
				outFile.precision(7);
				outFile << p->dot << " ";
				outFile.precision(prec);
			}
			outFile << p->x(echo) << " " << p->y(echo) << " " << p->z(echo);
			if (exportI) outFile << " " << p->intensity(echo);
			outFile << "\n";
		}				
	}

	s->deleteScanLines(0);
};

