#include <strstream>
using namespace std;

#ifdef WIN32
#include <io.h>
#else
#include <fcntl.h>
#endif

#include <float.h>
#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "ALSData.h"
#include "ALSPoint.h"

#define MAXLASPULSES 5
#define LEFTBITS    7  //1 + 2 + 4;
#define CENTREBITS 56  // 8 + 16 + 32;
#define ANGLEBIT   64
#define EFbit     128
	
CALSDataSet::CALSDataSet(const char *filename, const CReferenceSystem &ref, bool open) : 
		fileName(filename), fptr(0), refsys(ref), nPoints(0), pulsMode(eALSFirstLast),
		hasTime(false), hasIntensity(false), minCommonSublevel(0)
{
	if (open) this->retrieve();
}

//================================================================================

CALSDataSet::~CALSDataSet()
{
	destroy();
	close();
}

//================================================================================

int CALSDataSet::getMaxEchos() const
{
	if ((this->pulsMode == eALSFirst) || (this->pulsMode == eALSLast)) return 1;
	
	if (this->pulsMode == eALSFirstLast) return 2;

	return 2;
};

//================================================================================

void CALSDataSet::setReferenceSystem(const CReferenceSystem &ref)
{
	this->refsys = ref;
};

//================================================================================

bool CALSDataSet::retrieve()
{
	if (this->fptr) this->close();

	this->destroy();

	this->fptr = fopen(this->getFileNameChar(), "r+b");

	if (!this->fptr) return false;

	return this->retrieve(this->fptr);
};

//================================================================================

void CALSDataSet::close()
{
	if (this->fptr)
	{
		fclose(this->fptr);
		this->fptr = 0;
	}
};
	
//================================================================================

// ofstream *outFiles[MAXLASPULSES + 1] = {0,0,0,0,0, 0};

bool CALSDataSet::readLASFileHeader(FILE *inFile, unsigned char &versionMajor, unsigned char &versionMinor,
									unsigned short &HeaderSize, unsigned long &pointOffset,
									 unsigned char  &PointDataFormat, unsigned short &pointDataRecordLength,
									 unsigned long  &NumberOfPointRecords, C3DPoint &Scale, C3DPoint &Offset)
{
/*	if (!outFiles[0])
	{
		CCharString path = protHandler.getDefaultDirectory();
		for (int i = 0; i < MAXLASPULSES; ++i)
		{
			ostrstream oss;
			oss << "ALS_Return_" << (i + 1) << ".txt" << ends;
			char *z = oss.str();
			CFileName fn(z);
			delete[] z;
			fn.SetPath(path);
			outFiles[i] = new ofstream (fn.GetChar());
			outFiles[i]->setf(ios::fixed,ios::floatfield);
		}
		CFileName fn("all_order.txt");
		fn.SetPath(path);
		outFiles[MAXLASPULSES] = new ofstream (fn.GetChar());
		outFiles[MAXLASPULSES]->setf(ios::fixed,ios::floatfield);
	}
*/
	bool ret = true;
#ifdef WIN32
	fpos_t offsetFile = 0;
#else
	fpos_t offsetFile;
	offsetFile.__pos = 0;
#endif	
	fsetpos(inFile, &offsetFile);

	unsigned long pointsByReturn[MAXLASPULSES];
	unsigned long numberOfVariableRecords;

	char headerInfo[64];
	char *c = headerInfo;
	unsigned short *us = (unsigned short *) c;
	unsigned long *ul = (unsigned long *) c;

	fread(c, 4, 1, inFile);
	if ((headerInfo[0] != 'L') || (headerInfo[1]!= 'A') || (headerInfo[2]!= 'S')|| (headerInfo[3]!= 'F')) ret = false;
	else
	{
		fread(us, sizeof(unsigned short), 1, inFile);
		fread(us, sizeof(unsigned short), 1, inFile);
		fread(ul, sizeof(unsigned long),  1, inFile);
		fread(us, sizeof(unsigned short), 1, inFile);
		fread(us, sizeof(unsigned short), 1, inFile);
		fread(c, 8, 1, inFile);
		fread(&versionMajor, sizeof(unsigned char), 1, inFile);
		fread(&versionMinor, sizeof(unsigned char), 1, inFile);
		fread(c, 32, 1, inFile);
		fread(c, 32, 1, inFile);
		fread(us, sizeof(unsigned short), 1, inFile);
		fread(us, sizeof(unsigned short), 1, inFile);
		fread(&HeaderSize, sizeof(unsigned short), 1, inFile);
		fread(&pointOffset, sizeof(unsigned long), 1, inFile);
		fread(&numberOfVariableRecords, sizeof(unsigned long), 1, inFile);
		fread(&PointDataFormat, sizeof(unsigned char), 1, inFile);
		fread(&pointDataRecordLength, sizeof(unsigned short), 1, inFile);
		fread(&NumberOfPointRecords, sizeof(unsigned long), 1, inFile);
		fread(pointsByReturn, sizeof(unsigned long), MAXLASPULSES, inFile);
		fread(&Scale.x,  sizeof(double), 1, inFile);
		fread(&Scale.y,  sizeof(double), 1, inFile);
		fread(&Scale.z,  sizeof(double), 1, inFile);
		fread(&Offset.x, sizeof(double), 1, inFile);
		fread(&Offset.y, sizeof(double), 1, inFile);
		fread(&Offset.z, sizeof(double), 1, inFile);
		fread(&pMax.x,   sizeof(double), 1, inFile);
		fread(&pMin.x,   sizeof(double), 1, inFile);
		fread(&pMax.y,   sizeof(double), 1, inFile);
		fread(&pMin.y,   sizeof(double), 1, inFile);
		fread(&pMax.z,   sizeof(double), 1, inFile);
		fread(&pMin.z,   sizeof(double), 1, inFile);

		struct VariableRecord
		{
			unsigned short Reserved;
			char UserID[16];
			unsigned short RecordID;
			unsigned short RecordLengthAfterHeader;
			char Description[32];
		};

		int s = sizeof(VariableRecord);
		VariableRecord *VariableRecords= new VariableRecord[numberOfVariableRecords];

		vector<char *> contentVector(numberOfVariableRecords);
		
		for (unsigned int i = 0; i < numberOfVariableRecords; ++i)
		{
			if (fread(&VariableRecords[i], sizeof(VariableRecord), 1, inFile) == 1)
			{
				char *recContent = new char[VariableRecords[i].RecordLengthAfterHeader];
				contentVector[i] = recContent;
				fread(recContent, sizeof(char), VariableRecords[i].RecordLengthAfterHeader, inFile);
			}
			else 
			{
				contentVector[i] = 0;
			}
			int hmmm = 0;
		}	

		for (unsigned int i = 0; i < numberOfVariableRecords; ++i)
		{
			if (contentVector[i]) delete [] contentVector[i];
		}

		delete [] VariableRecords;
	}

	return ret;
};

//================================================================================

void CALSDataSet::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName relPath;
	CFileName fileSaveName = this->fileName;

	if (!Path.IsEmpty())
	{
		relPath = CSystemUtilities::RelPath(this->fileName, Path);
		fileSaveName.SetPath(relPath);
	}

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("ALS_data_filename", fileSaveName);
	token = pStructuredFileSection->addToken();
	token->setValue("ALS_hasTime", this->hasTime);
	token = pStructuredFileSection->addToken();
	token->setValue("ALS_hasIntensity", this->hasIntensity);
	token = pStructuredFileSection->addToken();
	token->setValue("ALS_PulseMode", this->pulsMode);


	CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->refsys.serializeStore(child);
};

void CALSDataSet::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);
	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName CWD;
	if (!Path.IsEmpty())
	{
		CWD = CSystemUtilities::getCwd();
		CSystemUtilities::chDir(Path);
	}

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("ALS_data_filename"))
			this->fileName = token->getValue();
		else if(key.CompareNoCase("ALS_hasTime"))
			this->hasTime = token->getBoolValue();
		else if(key.CompareNoCase("ALS_hasIntensity"))
			this->hasIntensity = token->getBoolValue();
		else if(key.CompareNoCase("ALS_PulseMode"))
			this->pulsMode = (eALSPulseModeType) token->getIntValue();
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refsys.serializeRestore(child);
        }
    }

	if (!Path.IsEmpty())
	{
		this->fileName = CSystemUtilities::AbsPath(this->fileName);
		CSystemUtilities::chDir(CWD);
	}

	this->retrieve();
};

void CALSDataSet::initTilingStatistics()
{
	this->tileNPointsAcross = 100;
	this->tileNPointsDown = 100;

	this->metricWidth = this->getPMax().x - this->getPMin().x;
	this->metricHeight = this->getPMax().y - this->getPMin().y;

	this->nPointsAcross = (int)(this->metricWidth/this->pointDist.x + 0.5);
	this->nPointsDown = (int)(this->metricHeight/this->pointDist.y + 0.5);

	this->tileSizeAcross = this->tileNPointsAcross*this->pointDist.x;
	this->tileSizeDown = this->tileNPointsDown*this->pointDist.y;

	this->tilesAcross = (int)(this->metricWidth/this->tileSizeAcross +1);
	this->tilesDown = (int)(this->metricHeight/this->tileSizeDown +1);

	int hmm =9;

}

bool CALSDataSet::read(const char *filename, eALSFormatType eFormat, eALSPulseModeType ePulseType, const C2DPoint &offset)
{
	bool ret = true;

	if (this->fptr) this->close();
	this->destroy();
	this->fptr = fopen(this->getFileNameChar(), "w+b");
	if (!this->fptr) ret = false;

	CFileName fn(filename);
	protHandler.setDefaultDirectory(fn.GetPath()); 

	FILE *in = fopen(filename, "r+b");
#ifdef WIN32	
	long filesize = _lseek(_fileno(in), 0, SEEK_END);
#else
	long filesize = lseek(fileno(in), 0, SEEK_END);
#endif
	if (eFormat == eALSLAS)
	{
		ret = readLASFile1(in, offset, filesize);
		fclose(in);
	}
	else
	{
		fclose(in);


		ifstream inFile;
		if (eFormat == eALSTopoSys) inFile.open(filename, ios::binary);
		else inFile.open(filename);

		ret &= inFile.good();

		if (ret)
		{
			this->pulsMode = ePulseType;
			this->hasIntensity = true;
			this->hasTime = true;

			if (eFormat == eALSTXYZI)
			{
				ret = readOptechFile(inFile, offset, filesize);
			}
			else if (eFormat ==  eALSTopoSys)
			{
				this->hasIntensity = false;
				this->hasTime = false;

				ret = this->readToposysFile(inFile, offset, filesize);
			}
			else
			{
				bool timefirst = false, multiple = false;

				if (this->pulsMode == eALSFirstLast) multiple = true;

				if ((eFormat == eALSXYZIT) || (eFormat == eALSXYZI)) this->hasIntensity = true;
				else this->hasIntensity = false;

				if (eFormat == eALSTXYZ) timefirst = true;

				if ((eFormat == eALSXYZ) || (eFormat == eALSXYZI))
				{
					this->hasTime = false;
					ret = this->readASCIIFileWithoutTime(inFile, offset, multiple, filesize);
				}
				else if ((eFormat == eALSXYZIT) || (eFormat == eALSTXYZ))
					ret = this->readASCIIFileWithTime(inFile, offset, multiple, timefirst, filesize);
				else ret = false;
			}
		}
	}
	if (this->nPoints < 1) ret = false;

	this->initMinCommonLevel();

	return ret;
}

CALSPoint *CALSDataSet::getALSPointOptech(ifstream &inFile, char *line, int length, const C2DPoint &offset)
{
	CALSPoint *p = 0;
	
	inFile.getline(line, length);
	if (!inFile.fail())
	{
		double dot, xf, yf, zf, xl, yl, zl;
		short intf, intl;
		int nechos = 1;

		strstream ss(line, length);
		ss >> dot;
		bool OK = !ss.fail();
		ss >> xl;
		OK &= !ss.fail();
		ss >> yl;
		OK &= !ss.fail();
		ss >> zl;
		OK &= !ss.fail();
		ss >> intl;
		if(ss.fail()) intl = 0;
		if (OK)
		{
			bool second = true;
			ss >> xf;
			second  = !ss.fail();
			ss >> yf;
			second &= !ss.fail();
			ss >> zf;
			second &= !ss.fail();
			ss >> intf;
			if(ss.fail()) intf = 0;

			if (second) nechos = 2;
			else
			{
				xf   = xl;
				yf   = yl;
				zf   = zl;
				intf = intl;
			};

			p = new CALSPoint(nechos);
			
			p->dot = dot;
			(*p)[0] = xf - offset.x;
			(*p)[1] = yf - offset.y;
			(*p)[2] = zf;
			p->intensity(0) = intf;
			
			if (nechos > 1)
			{
				(*p)[3] = xl - offset.x;
				(*p)[4] = yl - offset.y;
				(*p)[5] = zl;
				p->intensity(1) = intl;
			}
			
		}
	}

	return p;
};

CALSPoint *CALSDataSet::getALSPointWithTime(ifstream &inFile, char *line, int length, 
											const C2DPoint &offset, bool multiple, 
											bool timeFirst)
{
	CALSPoint *p = 0;
	
	inFile.getline(line, length);
	if (!inFile.fail())
	{
		double dot, xf, yf, zf, xl, yl, zl;
		short intf = 0, intl = 0;
		int nechos = 1;

		bool OK = true;

		strstream ss(line, length);
		ss >> dot;		
		OK = !ss.fail();
		ss >> xl;
		OK &= !ss.fail();
		ss >> yl;
		OK &= !ss.fail();
		ss >> zl;
		OK &= !ss.fail();
		if (this->hasIntensity)
		{
			ss >> intl;
			if(ss.fail()) intl = 0;
		}

		if (!timeFirst)
		{
			double d = zl;
			zl = yl;
			yl = xl;
			xl = dot;
			dot = d;
		}

		if (OK && multiple)
		{
			ss >> xf;
			multiple  = !ss.fail();
			ss >> yf;
			multiple &= !ss.fail();
			ss >> zf;
			multiple &= !ss.fail();
	
			if (this->hasIntensity)
			{
				ss >> intf;
				if(ss.fail()) intf = 0;
			}

			if (multiple) 
			{
				nechos = 2;
			}
			else
			{
				xf   = xl;
				yf   = yl;
				zf   = zl;
				intf = intl;
			};
		}

		if (OK)
		{
			p = new CALSPoint(nechos);
			
			p->dot = dot;
			(*p)[0] = xf - offset.x;
			(*p)[1] = yf - offset.y;
			(*p)[2] = zf;
			p->intensity(0) = intf;
			
			if (nechos > 1)
			{
				(*p)[3] = xl - offset.x;
				(*p)[4] = yl - offset.y;
				(*p)[5] = zl;
				p->intensity(1) = intl;
			}
			
		}
	}

	return p;
};

bool CALSDataSet::getPointLASRecordFormat0(FILE *inFile, const C3DPoint &Scale, const C3DPoint &Offset,
		                                   double &X, double &Y, double &Z, unsigned short &i, 
		                                   unsigned char &classification, unsigned char &FileMarker, 
										   unsigned char &Nreturns, unsigned char &returnNumber,
										   unsigned short &UserBitField,
										   bool &scanDir, bool &EdgeOfFlightline)
{
	unsigned char flags;
	char scanAngleRank;
	long x, y, z;
	if (fread(&x, sizeof(long), 1, inFile) != 1) return false;
	if (fread(&y, sizeof(long), 1, inFile) != 1) return false;
	if (fread(&z, sizeof(long), 1, inFile) != 1) return false;
	if (fread(&i, sizeof(unsigned short), 1, inFile) != 1) return false;
	if (fread(&flags, sizeof(unsigned char), 1, inFile) != 1) return false;
	if (fread(&classification, sizeof(unsigned char), 1, inFile) != 1) return false;
	if (fread(&scanAngleRank, sizeof(char), 1, inFile) != 1) return false;
	if (fread(&FileMarker, sizeof(unsigned char), 1, inFile) != 1) return false;
	if (fread(&UserBitField, sizeof(unsigned short), 1, inFile) != 1) return false;
	returnNumber = flags & LEFTBITS;
	Nreturns = (flags & CENTREBITS);
	Nreturns = Nreturns >> 3;
	scanDir = ((flags & ANGLEBIT) > 0);
	EdgeOfFlightline = ((flags & EFbit) > 0);

	X = double(x) * Scale.x + Offset.x;
	Y = double(y) * Scale.y + Offset.y;
	Z = double(z) * Scale.z + Offset.z;

	if ((Nreturns > MAXLASPULSES) || (returnNumber > Nreturns)) return false;

	return true;
};

bool CALSDataSet::getPointLASRecordFormat1(FILE *inFile, const C3DPoint &Scale, const C3DPoint &Offset,
		                                   double &X, double &Y, double &Z, unsigned short &i, 
		                                   unsigned char &classification, unsigned char &FileMarker, 
										   unsigned char &Nreturns, unsigned char &returnNumber,
										   unsigned short &UserBitField,
										   bool &scanDir, bool &EdgeOfFlightline,
										   double &t)
{
	bool ret = this->getPointLASRecordFormat0(inFile, Scale, Offset, X, Y, Z, i, classification, FileMarker, 
											  Nreturns, returnNumber, UserBitField, scanDir, EdgeOfFlightline);

	ret &= (fread(&t, sizeof(double), 1, inFile) == 1);

	return ret;
};

CALSPoint *CALSDataSet::getALSPointLASRecordFormat1(FILE *inFile, const C3DPoint &Scale, const C3DPoint &Offset)
{
	fpos_t ptOffset;

	CALSPoint *p = 0;
	double X[MAXLASPULSES],Y[MAXLASPULSES],Z[MAXLASPULSES];
	unsigned short Intensity[MAXLASPULSES];
	double x, y, z, t;
	unsigned short i, UserBitField;
	unsigned char classification, FileMarker;
	unsigned char Nreturns, returnNumber, nMaxReturns;
	bool scanDir, EdgeOfFlightline;
	
	double GPSTime[MAXLASPULSES];
	bool read[MAXLASPULSES];
	for (int i = 0; i < MAXLASPULSES; ++i) read[i] = false;
	double firstTime;

	bool finished = false;
	
	int echoIndex = 0;
	int nEchos = 1;

	bool OK = this->getPointLASRecordFormat1(inFile, Scale, Offset, x, y, z, i, classification, FileMarker, 
											 nMaxReturns, returnNumber, UserBitField, scanDir, 
											 EdgeOfFlightline, t);
	if (returnNumber > nEchos) 
	{
		finished = true;
	}
	else
	{
		echoIndex = returnNumber - 1;
	}

	X[echoIndex] = x;
	Y[echoIndex] = y;
	Z[echoIndex] = z;
	Intensity[echoIndex] = i;
	GPSTime[echoIndex] = t;
	read[echoIndex] = true;
	firstTime = t;
/*

	ostrstream oss1;
	oss1.setf(ios::fixed, ios::floatfield);

	oss1.precision(6); oss1.width(15); oss1 << t << " ";
	oss1.precision(2); oss1.width(9);  oss1 << X[echoIndex] << " ";
	oss1.width(10);    oss1 << Y[echoIndex] << "  ";
	oss1.width(6);     oss1 << Z[echoIndex] << " ";
	oss1.width(4);     oss1 << i  << ends;
	char *zS = oss1.str();

	*(outFiles[returnNumber - 1]) << zS << "\n";	
	*(outFiles[MAXLASPULSES]) << zS << " " << int(returnNumber) << " " << int(nMaxReturns) << " " 
							  << int(scanDir) << " " << int(EdgeOfFlightline) << "\n";

	delete [] zS;
*/
	while ((!finished) && OK && (returnNumber < nMaxReturns))
	{
		fgetpos(inFile, &ptOffset);

		OK = this->getPointLASRecordFormat1(inFile, Scale, Offset, x, y, z, i, classification, FileMarker, 
											 Nreturns, returnNumber, UserBitField, scanDir, 
											 EdgeOfFlightline, t);
	
		if ((Nreturns < nMaxReturns) || (t != firstTime) || (read[returnNumber - 1]))
		{
			fsetpos(inFile, &ptOffset);
			finished = true;
		}
		else
		{
			echoIndex = nEchos;
			nEchos += 1;
			if (returnNumber > nEchos) 
			{
				finished = true;
			}			
			else
			{
				echoIndex = returnNumber - 1;
			}

			X[echoIndex] = x;
			Y[echoIndex] = y;
			Z[echoIndex] = z;
			Intensity[echoIndex] = i;
			GPSTime[echoIndex] = t;
			read[echoIndex] = true;

/*
			ostrstream oss;
			oss.setf(ios::fixed, ios::floatfield);

			oss.precision(6); oss.width(15); oss << t << " ";
			oss.precision(2); oss.width(9);  oss << X[echoIndex] << " ";
			oss.width(10);    oss << Y[echoIndex] << "  ";
			oss.width(6);     oss << Z[echoIndex] << " ";
			oss.width(4);     oss << i  << ends;
			zS = oss.str();

			*(outFiles[returnNumber - 1]) << zS << "\n";	
			*(outFiles[MAXLASPULSES]) << zS << " " << int(returnNumber) << " " << int(Nreturns) << " " 
									  << int(scanDir) << " " << int(EdgeOfFlightline) << "\n";

			delete [] zS;
*/
		}
	}

	if (OK && (nEchos > 0))
	{
		p = new CALSPoint(nEchos);
		p->dot = GPSTime[0];

		for (int i = 0, coordI = 0; i < MAXLASPULSES; ++i)
		{
			if (read[i])
			{
				(*p)[coordI] = X[i]; ++coordI;
				(*p)[coordI] = Y[i]; ++coordI;
				(*p)[coordI] = Z[i]; ++coordI;
				p->intensity(i) = Intensity[i];
			}
		}
	}

	return p;
};

CALSPoint *CALSDataSet::getALSPointLASRecordFormat0(FILE *inFile, const C3DPoint &Scale, const C3DPoint &Offset)
{
	CALSPoint *p = 0;
	double X[MAXLASPULSES],Y[MAXLASPULSES],Z[MAXLASPULSES];
	unsigned short Intensity[MAXLASPULSES];
	long x, y, z;
	unsigned short i;
	unsigned char flags, classification, scanAngleRank, FileMarker;
	unsigned char Nreturns, returnNumber;
				
	bool scanDir;
	bool EdgeOfFlightline;
	unsigned short UserBitField;

	bool finished = false;
	bool OK = true;

	int nEchos = 0;

	int echoIndex;

	do
	{
		echoIndex = nEchos;
		nEchos += 1;

		fread(&x, sizeof(long), 1, inFile);
		fread(&y, sizeof(long), 1, inFile);
		fread(&z, sizeof(long), 1, inFile);
		fread(&i, sizeof(unsigned short), 1, inFile);
		fread(&flags, sizeof(unsigned char), 1, inFile);
		fread(&classification, sizeof(unsigned char), 1, inFile);
		fread(&scanAngleRank, sizeof(unsigned char), 1, inFile);
		fread(&FileMarker, sizeof(unsigned char), 1, inFile);
		fread(&UserBitField, sizeof(unsigned short), 1, inFile);

		returnNumber = flags & LEFTBITS;
		Nreturns = (flags & CENTREBITS);
		Nreturns = Nreturns >> 3;
		scanDir = ((flags & ANGLEBIT) > 0);
		EdgeOfFlightline = ((flags & EFbit) > 0);

		if ((Nreturns > MAXLASPULSES) || (returnNumber > Nreturns)) OK = false;
		else
		{
			if (returnNumber > nEchos) 
			{
				finished = true;
			}
			else
			{
				echoIndex = returnNumber - 1;
			}

			X[echoIndex] = double(x) * Scale.x + Offset.x;
			Y[echoIndex] = double(y) * Scale.y + Offset.y;
			Z[echoIndex] = double(z) * Scale.z + Offset.z;
			Intensity[echoIndex] = i;
		}
	} while (!finished && (returnNumber < Nreturns) && OK);

	if (OK && (nEchos > 0))
	{
		p = new CALSPoint(nEchos);
		p->dot = 0.0;

		for (int i = 0, coordI = 0; i < nEchos; ++i)
		{
			(*p)[coordI] = X[i]; ++coordI;
			(*p)[coordI] = Y[i]; ++coordI;
			(*p)[coordI] = Z[i]; ++coordI;
			p->intensity(i) = Intensity[i];
		}
	}

	return p;
};

CALSPoint *CALSDataSet::getALSPointWithoutTime(ifstream &inFile, char *line, int length, 
											   const C2DPoint &offset, bool multiple)
{
	CALSPoint *p = 0;
	
	inFile.getline(line, length);

	//addition >>
	//replace all ',' (comma) by ' ' (space)
	int i = 0;
	while (line[i] != '\0')
	{
		if (line[i] == ',')
			line[i] = ' ';
		i++;
	}
	//<< addition
	if (!inFile.fail())
	{
		double xf, yf, zf, xl, yl, zl;
		short intf = 0, intl = 0;
		int nechos = 1;

		bool OK = true;

		strstream ss(line, length);
		ss >> xl;
		OK &= !ss.fail();
		ss >> yl;
		OK &= !ss.fail();
		ss >> zl;
		OK &= !ss.fail();
		if (this->hasIntensity)
		{
			ss >> intl;
			if(ss.fail()) intl = 0;
		}

		if (OK && multiple)
		{
			ss >> xf;
			multiple  = !ss.fail();
			ss >> yf;
			multiple &= !ss.fail();
			ss >> zf;
			multiple &= !ss.fail();
	
			if (this->hasIntensity)
			{
				ss >> intf;
				if(ss.fail()) intf = 0;
			}

			if (multiple) 
			{
				nechos = 2;
			}
			else
			{
				xf   = xl;
				yf   = yl;
				zf   = zl;
				intf = intl;
			}
		}
		else 
		{
			xf   = xl;
			yf   = yl;
			zf   = zl;
			intf = intl;
		};


		if (OK)
		{
			p = new CALSPoint(nechos);
			
			p->dot = 0.0;
			(*p)[0] = xf - offset.x;
			(*p)[1] = yf - offset.y;
			(*p)[2] = zf;
			p->intensity(0) = intf;
			
			if (nechos > 1)
			{
				(*p)[3] = xl - offset.x;
				(*p)[4] = yl - offset.y;
				(*p)[5] = zl;
				p->intensity(1) = intl;
			}
			
		}
	}

	return p;
};


CALSPoint *CALSDataSet::getALSPointTopoSys(ifstream &inFile, const C2DPoint &offset)
{
	CALSPoint *p = 0;
	int length = 12;
	char line[12];
	
	inFile.read(line, length);
	if (!inFile.fail())
	{
		int *r, *h, *z;

		h = (int *) &line[0];
		r = (int *) &line[4];
		z = (int *) &line[8];
		
		p = new CALSPoint(1);
			
		p->dot = 0.0;
		(*p)[0] = double(*r) * 0.01 - offset.x;
		(*p)[1] = double(*h) * 0.01 - offset.y;
		(*p)[2] = double(*z) * 0.01;
		p->intensity(0) = 0;				
	}

	return p;
};

int CALSDataSet::getPointCloudTile(int row, int col, CALSPoint **&pointvec, int &capacity, int dCapacity, int level)
{
	C3DPoint tilemin(this->pMin.x + col*this->tileSizeAcross, this->pMin.y + row*this->tileSizeDown, this->pMin.z);
	C3DPoint tilemax(this->pMin.x + (col+1)*this->tileSizeAcross, this->pMin.y + (row+1)*this->tileSizeDown, this->pMax.z);

	return this->getPointCloud(tilemin, tilemax, *&pointvec, capacity, dCapacity, level);
	
}

void CALSDataSet::getTileExtension(int row, int col, C3DPoint &tul, C3DPoint &tll, C3DPoint &tlr, C3DPoint &tur)
{
	tll.x = this->pMin.x + col*this->tileSizeAcross;
	tll.y = this->pMin.y + row*this->tileSizeDown;
	tll.z = this->pMin.z;

	tur.x = this->pMin.x + (col+1)*this->tileSizeAcross;
	tur.y = this->pMin.y + (row+1)*this->tileSizeDown;
	tur.z = this->pMin.z;

	tul.x = this->pMin.x + col*this->tileSizeAcross;
	tul.y = this->pMin.y + (row+1)*this->tileSizeDown;
	tul.z = this->pMin.z;

	tlr.x = this->pMin.x + (col+1)*this->tileSizeAcross;
	tlr.y = this->pMin.y + row*this->tileSizeDown;
	tlr.z = this->pMin.z;

	if ( row == this->tilesDown -1)
	{
		tul.y = this->pMax.y;
		tur.y = this->pMax.y;
	}
		
	if ( col == this->tilesAcross -1 )
	{
		tlr.x = this->pMax.x;
		tur.x = this->pMax.x;
	}
}
