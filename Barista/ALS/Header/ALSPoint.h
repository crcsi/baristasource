#ifndef ___ALSPOINT_HPP___
#define ___ALSPOINT_HPP___

#include "PointBase.h"
#include "3DPoint.h"
#include <ANN/ANN.h>


class CALSPoint : public  CPointBase
{
  protected:

	  short *intensities;

  public:

	  CALSPoint() : CPointBase(3) 
	  { this->intensities = new short[1]; };
	 
	  CALSPoint(int nEchos) : CPointBase(3 * nEchos) 
	  { this->intensities = new short[nEchos]; };
	  
	  CALSPoint(const CALSPoint &p) : CPointBase(p)
	  { 
		  int np = dim / 3; 
		  this->intensities = new short[np]; 
		  for (int i = 0; i < np; ++i) 
			  this->intensities[i] = p.intensities[i];
	  };

	  
	  virtual ~CALSPoint() { delete [] this->intensities; };

	  CALSPoint &operator =(const CALSPoint &p);

	  short nEchos() const { return this->dim / 3; }

	  short getEchoIndex(short echo) const;

	  short getLastEchoIndex() const { return this->dim - 3; };

	  double getTime() const { return this->dot; };

	  double &getCoordinate(int index) { return this->coordinates[index]; };

	  double getFirstMinusLast() const;

	  short &intensity(int echo) { return this->intensities[echo]; };

	  const short &intensity(int echo) const { return this->intensities[echo]; };

	  double &x(int echo) { return this->coordinates[echo * 3]; };

	  const double &x(int echo) const { return this->coordinates[echo * 3]; };

	  double &y(int echo) { return this->coordinates[echo * 3 + 1]; };

	  const double &y(int echo) const { return this->coordinates[echo * 3 + 1]; };

	  double &z(int echo) { return this->coordinates[echo * 3 + 2]; };

	  const double &z(int echo) const { return this->coordinates[echo * 3 + 2]; };

	  C3DPoint getEcho(int echo) const 
	  { 
		  int index = echo * 3;
		  return C3DPoint(this->id, this->coordinates[index], this->coordinates[index + 1], this->coordinates[index + 2], this->dot);
	  }

	  C3DPoint getFirstEcho() const 
	  { 
		  return C3DPoint(this->id, this->coordinates[0], this->coordinates[1], this->coordinates[2], this->dot);
	  }

	  C3DPoint getLastEcho() const 
	  { 
		  return C3DPoint(this->id, this->coordinates[this->dim - 3], this->coordinates[this->dim - 2], this->coordinates[this->dim - 1], this->dot);
	  }

	  void addEcho(double x, double y, double z, short intensity);

	  // format on file: n time x0 y0 z0 x1 y1 z1 ... xn yn zn i0 i1 ... in
	  // with n = nEchos(); 
	  // n: short; time, coordinates: double; intensities: short
	  bool dump(FILE* fp) const;

	  bool read(FILE* fp);

	  short getRecordSize() const { return sizeof(short) + sizeof(double) + this->nEchos() * (3 * sizeof(double) + sizeof(short)); };

	  ANNpoint getAnnPoint(int echo) const { return &this->coordinates[echo * 3]; }

	  void copy(const CALSPoint &p);
};

#endif