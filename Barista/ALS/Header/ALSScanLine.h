#ifndef ___ALSSCANLINE_HPP___
#define ___ALSSCANLINE_HPP___
#include <float.h>

#include "ALSPoint.h"
#include "2DPoint.h"

class CALSScanLine
{
  protected:

	  C2DPoint centre;
	  C2DPoint dir;
  	  C2DPoint width;
	  double zmin, zmax;

	  int npoints;
	  
	  long nbytes;

	  fpos_t offset;

	  CALSPoint **pointVector;

	  long pointCapacity;

  public:

	  CALSScanLine(): pointVector(0), pointCapacity(0), npoints(0), width(-1.0,-1.0), 
		              nbytes(8 * sizeof(double) + sizeof(int) + sizeof(long)),
					  zmin(FLT_MAX), zmax(-FLT_MAX) { };
	  
	  CALSScanLine(const CALSScanLine &l);
	  
	  ~CALSScanLine();

	  fpos_t getOffset() const { return this->offset; };

	  bool potentialOverlap(const C3DPoint &pMin, const C3DPoint &pMax);

	  int nPoints() const { return npoints; };
 
	  void addPoint(CALSPoint *p);

	  void removePoint(int index);
	  
	  void merge(CALSScanLine &l);

	  void reset();

	  void deletePoints();

	  bool isActive() const;

	  void initStatistics();
	  
	  CALSPoint *operator[] (int index) const { assert(index < npoints); return pointVector[index]; }
	  CALSPoint *&operator[] (int index) { assert(index < npoints); return pointVector[index]; }

	  double pointDist() const { return this->width.y / this->nPoints(); };

	  // Format: nPoints nbytes centre.x centre.y dir.x dir.y width.x width.y point_0 ... point_N
	  // nPoints: int, nbytes: long, centre.x .. width.y: double; point_i: see CALSPoint.h
	  bool dump(FILE* fp);

	  bool read(FILE* fp, bool getOffset = true);

	  bool retrieve(FILE* fp);

	  long sizeByte() const { return this->nbytes; };

	  const C2DPoint &getCentre() const { return this->centre; }
	  const C2DPoint &getDir() const { return this->dir; }
	  const C2DPoint &getWidth() const { return this->width; }
	  double getZmin() const { return this->zmin; };
	  double getZmax() const { return this->zmax; };
};

#endif