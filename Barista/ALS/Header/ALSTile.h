#ifndef ___ALSTILE_HPP___
#define ___ALSTILE_HPP___
#include <float.h>

#include "ALSPoint.h"
#include "2DPoint.h"

class CALSTile
{
  protected:

	  double xmin, xmax, ymin, ymax, zmin, zmax;
	
	  fpos_t offset;

	  CALSPoint **points;

	  int nPoints;

	  int capacity;

	  int level;
		

  public:
 
	  CALSTile(): points(0), capacity(0), xmin(FLT_MAX), ymin(FLT_MAX), zmin(FLT_MAX), 
						       xmax(-FLT_MAX), ymax(-FLT_MAX), zmax(-FLT_MAX), nPoints(0), level(0) 
		{
#ifdef WIN32
			this->offset = 0;
#else
			this->offset.__pos = 0;
#endif			 
		};
	 
	  CALSTile(C2DPoint Pmin, C2DPoint Pmax, int Level): points(0), capacity(0), xmin(Pmin.x), ymin(Pmin.y), zmin(FLT_MAX), 
						       xmax(Pmax.x), ymax(Pmax.y), zmax(-FLT_MAX), nPoints(0), level(Level) 
		{
#ifdef WIN32
			this->offset = 0;
#else
			this->offset.__pos = 0;
#endif		 
		};
	  
	  ~CALSTile();

	  fpos_t getOffset() const { return this->offset; };

	  double getXmin() const { return this->xmin; };
	  double getYmin() const { return this->ymin; };
	  double getZmin() const { return this->zmin; };
	  double getXmax() const { return this->xmax; };
	  double getYmax() const { return this->ymax; };
	  double getZmax() const { return this->zmax; };

	  C2DPoint getExtents() const { return C2DPoint(this->xmax - this->xmin, this->ymax - this->ymin); };

	  int getNPoints() const { return nPoints; };
 	  
	  CALSPoint * operator[] (int index) const { return points[index]; }

	  CALSPoint *&operator[] (int index)  { return points[index]; }

	  bool isInitialised() const { return capacity != 0; }

	  bool overlap(const C3DPoint &Pmin, const C3DPoint &Pmax) const;

	  bool isInside(const C3DPoint &P) const;

	  bool isInside(const CALSPoint *P) const;

	  void addPoint(CALSPoint *p);

 	  void deletePoint(int index);

	  void deletePoints();

	  bool dump(FILE* fp);

	  bool dumpHdr(FILE* fp);

	  bool read(FILE* fp);

	  bool retrieve(FILE* fp);

	  bool retrieveHdr(FILE* fp);


  protected:
	
};

#endif