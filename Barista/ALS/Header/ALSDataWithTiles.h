#ifndef ___ALSDATAWITHTILES_HPP___
#define ___ALSDATAWITHTILES_HPP___

#include "ALSData.h"
#include "ALSTile.h"
#include "3DPoint.h"
#include "Filename.h"
#include "Serializable.h"
#include "ReferenceSystem.h"
#include "ProgressHandler.h"

class CALSDataSetWithTiles : public CALSDataSet   
{
  protected:

	  fpos_t offset;

	  fpos_t subLevelOffset;

	  vector<CALSTile *> tileVec;

	  int nTilesX, nTilesY;

	  C2DPoint TileSize;

	  vector<CALSDataSetWithTiles *> subLevels;

  public:

	  CALSDataSetWithTiles(const char *filename, const CReferenceSystem &ref, bool open, const C2DPoint &delta); 

	  CALSDataSetWithTiles(const char *filename, const CReferenceSystem &ref, CALSPoint **pointList, int &npoints, const C2DPoint &delta); 
	 	  
	  virtual ~CALSDataSetWithTiles();

	
	  virtual int nTiles() const { return this->tileVec.size(); };

	  virtual bool hasStrips() const { return false; };

	  virtual bool hasTiles() const { return true; };
  
	  virtual CALSTile *getTile(int index) const { return this->tileVec[index]; };

	  virtual void initStatistics();

 	  void getminMaxTileIndices(const C3DPoint &pmin, const C3DPoint &pmax, 
		                        int &rowMin, int &rowMax, int &colMin, int &colMax) const;
 
	  virtual int getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, int level);

	  virtual int getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, int &capacity, int dCapacity, int level);

	  virtual CALSDataSet *getClone() const;

	  // CSerializable functions
	  bool isa(string& className)  const { return (className == "CALSDataSetWithTiles"); };
	  string getClassName()  const { return string( "CALSDataSetWithTiles"); };
	  virtual void reconnectPointers() { CSerializable::reconnectPointers(); };
	  virtual void resetPointers() { CSerializable::resetPointers(); };
	  virtual bool isModified() { return this->bModified; };
 
	  void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	 	
	  virtual bool exportXYZ(const CCharString &FileName, int echo, bool exportTime, bool exportI);

	  virtual void clear();
	  
	  virtual void clear(int level);

	  bool init(CALSPoint **pointvec, int &npoints, bool initSublevels);

	  bool init(vector<CALSDataSet *> &datasetVec);

	  int getTileIndex(double x, double y) const;

	  C2DPoint getTileSize() const;


  protected:

	  void addTile(CALSTile *tile);

 	  virtual bool readOptechFile(ifstream &inFile, const C2DPoint &offset, long filesize);

  	  virtual bool readASCIIFileWithTime(ifstream &inFile, const C2DPoint &offset, bool multiple, 
								         bool timeFirst, long filesize);

  	  virtual bool readASCIIFileWithoutTime(ifstream &inFile, const C2DPoint &offset, bool multiple, 
									        long filesize);

  	  virtual bool readLASFile1(FILE *inFile, const C2DPoint &offset, long filesize);

  	  virtual bool readToposysFile(ifstream &inFile, const C2DPoint &offset, long filesize);
	 
	  virtual bool retrieve(FILE* FP);
	  
	  bool retrieveOld(FILE* FP, int &nSubLevels);
	  
	  bool retrieveHdr(FILE* FP, int &nSubLevels);

	  virtual void destroy();

	  virtual void initMinCommonLevel();

	  bool writeHeader(FILE* pFile);

	  bool finishWriting(FILE* pFile);

	  bool dump(bool initSublevels);

	  void readTile(int index, FILE *FP);

	  void clearTile(int index);

	  void clearTile(int index, int level);

	  void initTileStructure (C3DPoint &Pmin, C3DPoint &Pmax, int npoints, bool changeMinMax);

	  bool mergeDatasets(vector<CALSDataSet *> &datasetVec);

	  bool initSublevels();

	  bool convertFormat();
};

#endif