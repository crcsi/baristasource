#ifndef ___ALSDATA_HPP___
#define ___ALSDATA_HPP___

#include "3DPoint.h"
#include "2DPoint.h"
#include "Filename.h"
#include "Serializable.h"
#include "ReferenceSystem.h"
#include "ProgressHandler.h"

class CALSStrip;
class CALSTile;
class CALSPoint;

enum eALSFormatType { eALSXYZ = 0, eALSXYZI, eALSXYZIT, eALSTXYZ, eALSTXYZI, eALSTopoSys, eALSLAS };

enum eALSPulseModeType { eALSFirst = 0, eALSLast, eALSFirstLast };

class CALSDataSet : public CSerializable, public CProgressHandler   
{
  protected:

	  C3DPoint pMin;

	  C3DPoint pMax;

	  C2DPoint pointDist;

	  CFileName fileName;

	  FILE* fptr;
	  
	  int nPoints;

	  CReferenceSystem refsys;

	  eALSPulseModeType pulsMode;

	  bool hasTime;

	  bool hasIntensity;

	  int minCommonSublevel;

	  // for tiling in 3D display
	  int tilesAcross;
	  int tilesDown;

	  int tileNPointsAcross;
	  int tileNPointsDown;

	  double tileSizeAcross;
	  double tileSizeDown;

	  double metricWidth;
	  double metricHeight;

	  int nPointsAcross;
	  int nPointsDown;
	  // end for tiling in 3D display

  public:

	  CALSDataSet(const char *filename, const CReferenceSystem &ref, bool open); 
	 
	  virtual ~CALSDataSet();

	  const char *getFileNameChar() const { return this->fileName.GetChar(); };
	  
	  C3DPoint getPMin() const { return this->pMin; };

	  C3DPoint getPMax() const { return this->pMax; };

	  C2DPoint getPointDist() const { return this->pointDist; };

	  int getNPoints() const { return this->nPoints; };
  
	  int getMinCommonSublevel() const { return this->minCommonSublevel; };


	  virtual int nStrips() const { return 0; };

	  virtual int nTiles() const { return 0; };

	  virtual bool hasStrips() const = 0;

	  virtual bool hasTiles() const = 0;

	  virtual CALSStrip *getStrip(int index) const { return 0; };

	  virtual CALSTile *getTile(int index) const { return 0; };

	  eALSPulseModeType getPulsMode() const { return this->pulsMode; };

	  int getMaxEchos() const;

	  
	  bool getHasTime() const { return this->hasTime; };

	  bool getHasIntensity() const { return this->hasIntensity; };

	  CReferenceSystem getReferenceSystem() const { return this->refsys;  }

	  void setReferenceSystem(const CReferenceSystem &ref);

	  virtual void initStatistics() = 0;

	  void initTilingStatistics();
	  
	  bool read(const char *filename, eALSFormatType eFormat, eALSPulseModeType ePulseType, const C2DPoint &offset);

	  bool retrieve();

	  void close();

	  virtual CALSDataSet *getClone() const = 0;

	  virtual int getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, int level) = 0;

	  virtual int getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, int &capacity, int dCapacity, int level) = 0;

	  int getPointCloudTile(int row, int col, CALSPoint **&pointvec, int &capacity, int dCapacity, int level);
	
	  // CSerializable functions
	  bool isa(string& className)  const { return (className == "CALSDataSet"); };
	  string getClassName()  const { return string( "CALSDataSet"); };
	  virtual void reconnectPointers() { CSerializable::reconnectPointers(); };
	  virtual void resetPointers() { CSerializable::resetPointers(); };
	  virtual bool isModified() { return this->bModified; };
 
	  virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	
	  virtual void clear() = 0;

	  virtual void clear(int level) = 0;
	  
	  virtual bool exportXYZ(const CCharString &Path, int echo, bool exportTime, bool exportI) = 0;


	// for tiling in 3D display
	int getTilesAcross() const { return  this->tilesAcross;};
	int getTilesDown() const { return  this->tilesDown;};
	int getTileCount() const { return  this->tilesDown*this->tilesAcross;};

	int getTileNPointsAcross() const { return  this->tileNPointsAcross;};
	int getTileNPointsDown() const { return  this->tileNPointsDown;};
	int getTileNPoints() const { return  this->tileNPointsDown*this->tileNPointsAcross;};

	double getTileSizeAcross() const { return  this->tileSizeAcross;};
	double getTileSizeDown() const { return  this->tileSizeDown;};

	double getMetricWidth() const { return  this->metricWidth;};
	double getMetricHeight() const { return  this->metricHeight;};

	int getNPointsAcross() const { return  this->nPointsAcross;};
	int getNPointsDown() const { return  this->nPointsDown;};

	void getTileExtension(int row, int col, C3DPoint &tul, C3DPoint &tll, C3DPoint &tlr, C3DPoint &tur);


	// end for tiling in 3D display

  protected:

	  virtual void destroy() { };

  	  virtual bool readOptechFile(ifstream &inFile, const C2DPoint &offset, long filesize) = 0;

  	  virtual bool readASCIIFileWithTime(ifstream &inFile, const C2DPoint &offset, bool multiple, 
								         bool timeFirst, long filesize) = 0;

  	  virtual bool readASCIIFileWithoutTime(ifstream &inFile, const C2DPoint &offset, bool multiple, 
											long filesize) = 0;
	
  	  virtual bool readToposysFile(ifstream &inFile, const C2DPoint &offset, long filesize) = 0;

  	  virtual bool readLASFile1(FILE *inFile, const C2DPoint &offset, long filesize) = 0;

	  bool readLASFileHeader(FILE *inFile, unsigned char &versionMajor, unsigned char &versionMinor,
							 unsigned short &HeaderSize, unsigned long &pointOffset,
							 unsigned char  &PointDataFormat, unsigned short &pointDataRecordLength,
							 unsigned long  &NumberOfPointRecords, C3DPoint &Scale, C3DPoint &Offset);

	  virtual bool retrieve(FILE* FP) = 0;

	  virtual void initMinCommonLevel() = 0;

	  CALSPoint *getALSPointOptech(ifstream &inFile, char *line, int length, const C2DPoint &offset);

	  CALSPoint *getALSPointWithTime(ifstream &inFile, char *line, int length, 
		                             const C2DPoint &offset, bool multiple, bool timeFirst);

	  CALSPoint *getALSPointWithoutTime(ifstream &inFile, char *line, int length, const C2DPoint &offset, 
										bool multiple);

	  CALSPoint *getALSPointTopoSys(ifstream &inFile, const C2DPoint &offset);

	  CALSPoint *getALSPointLASRecordFormat0(FILE *inFile, const C3DPoint &Scale, const C3DPoint &Offset);

	  CALSPoint *getALSPointLASRecordFormat1(FILE *inFile, const C3DPoint &Scale, const C3DPoint &Offset);
	  
	  bool getPointLASRecordFormat1(FILE *inFile, const C3DPoint &Scale, const C3DPoint &Offset,
		                            double &X, double &Y, double &Z, unsigned short &i, 
		                            unsigned char &classification, unsigned char &FileMarker, 
									unsigned char &Nreturns, unsigned char &returnNumber,
									unsigned short &UserBitField,
									bool &scanDir, bool &EdgeOfFlightline,
									double &t);


	  bool getPointLASRecordFormat0(FILE *inFile, const C3DPoint &Scale, const C3DPoint &Offset,
		                            double &X, double &Y, double &Z, unsigned short &i, 
		                            unsigned char &classification, unsigned char &FileMarker, 
									unsigned char &Nreturns, unsigned char &returnNumber,
									unsigned short &UserBitField,
									bool &scanDir, bool &EdgeOfFlightline);

};

#endif