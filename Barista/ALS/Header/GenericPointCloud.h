#ifndef ___GENERICPOINTCLOUD_HPP___
#define ___GENERICPOINTCLOUD_HPP___

#include "3DPoint.h"

#include <ANN/ANN.h>
#include "ReferenceSystem.h"
#include "3DPoint.h"
#include "2DPoint.h"

class CXYZPointArray;
class CXYPointArray;
class CXYPolyLine;
class CXYZPolyLine;
class CRobustWeightFunction;

enum eInterpolationMethod { eMovingHzPlane, eMovingTiltedPlane, eTriangulation };

struct ITRIANGLE{int p[3]; int e[3]; };

struct IEDGE{int p[2]; int t[2]; };


class CPointCloud
{
  protected:

	  C3DPoint pMin;

	  C3DPoint pMax;

	  ANNkd_tree *kdTree;

	  ANNpointArray annPoints;

	  ANNpointArray annQueryPoints;
	
	  ANNidxArray nnIdx;

	  ANNdistArray nnDists;

	  int nQueryPoints;

	  int npoints;
	  
	  int capacity;

	  int annDim;

	  int dimension;

	  ITRIANGLE *triangles;

	  IEDGE *edges;

	  int *firstEdgePerVertex;

	  int nEdges, nTriangles;

	  double EPSILON;

	  mutable int lastTriangle;

  public:

	  CPointCloud(int dim, int anndim);

	  virtual ~CPointCloud();

	  virtual bool setAnnDim(int anndim) = 0;

	  int getNpoints()    const { return this->npoints; };
	  int getNTriangles() const { return this->nTriangles; };
	  int getNedges()     const { return this->nEdges; };
 
	  C3DPoint getPmin() const { return this->pMin; };
	  C3DPoint getPmax() const { return this->pMax; };

	  ANNpoint getPt(int index) const { return annPoints[index]; };
	 
	  int getDim() const { return dimension; };

	  int getAnnDim() const { return annDim; };

	  bool isInitialised() const { return (this->kdTree != 0); }

	  void getTriangle(int i, ANNpoint &P1, ANNpoint &P2, ANNpoint &P3) const;

	  void getTriangle(int i, C3DPoint &P1, C3DPoint &P2, C3DPoint &P3) const;

	  void getTriangle(int i, C2DPoint &P1, C2DPoint &P2, C2DPoint &P3) const;

	  void getEdgeIndices(int idx, int &idxP1, int &idxP2) const
	  { 
		  idxP1 = this->edges[idx].p[0]; idxP2 = this->edges[idx].p[1]; 
	  };

	  CXYPolyLine getTriangle2D(int i) const;

	  CXYZPolyLine getTriangle3D(int i) const;

	  void initQueryPoints(int N);

	  virtual void clear();

	  int getNearestNeighbours(ANNpointArray &neighbours, double *sqrDist, int N, const C3DPoint &p);

	  int getNearestNeighbours(ANNpointArray  &neighbours, double *sqrDist, int N, const C3DPoint &p, double maxDist);

	  double interpolateMovingHorizontalPlane(const ANNpoint &queryPt, int N, int CoordInd, 
		                                      double maxDistSqare, double minsquareDistForWeight, double &minDist);

	  double interpolateMovingHorizontalPlane(const ANNpoint &queryPt, int N, int CoordInd, double minsquareDistForWeight, 
		                                     double &minDist);

	  double interpolateMovingPlane(const ANNpoint &queryPt, int N, int CoordInd, double maxDistSqare, 
									double minsquareDistForWeight, 
									double &minDist);

	  double interpolateMovingPlane(const ANNpoint &queryPt, int CoordInd, int N,
									double minsquareDistForWeight, double &minDist);

	  void interpolateRaster(float *buf, int width, int height, int bufwidth,
							 double x0, double y0, double dx, double dy, 
		                     int N, eInterpolationMethod method, float &zmin, float &zmax, int CoordInd,
									double minsquareDistForWeight);

	  void interpolateRaster(float *buf, int width, int height, int bufwidth,
							 double x0, double y0, double dx, double dy, 
		                     int N, eInterpolationMethod method, float &zmin, float &zmax,
							 double maxDist, int CoordInd,
									double minsquareDistForWeight);

	  bool interpolateRasterTriangulation(float *buf, int width, int height, int bufwidth,
							              double x0, double y0, double dx, double dy, 
		                                  float &zmin, float &zmax, int CoordInd);

	  double interpolateTriangulation(const C2DPoint &p, int CoordInd) const;
	  double interpolateTriangulation(const C2DPoint &p, int CoordInd, double &minDist) const;

	  int Triangulate();


	  void resetTriangulation();

	  void exportTriangulation(const char *filename) const;

	  int findTriangleIndex(const C2DPoint &p) const;

	  ANNkd_tree *getKdTree() { return this->kdTree; };

  protected:

	  double interpolateMovingHorizontalPlane(int N, int CoordInd, double minsquareDistForWeight, double &minDist);

	  double interpolateMovingPlane(int N, int CoordInd, const ANNpoint &queryPt, double minsquareDistForWeight, double &minDist);

	  int CircumCircle(ANNpoint p0, ANNpoint p1, ANNpoint p2, ANNpoint p3, ANNpoint &pc, double &r) const;

	  void findOuterTriangles(ANNpoint &p1, ANNpoint &p2,ANNpoint &p3) const;

	  bool isInTriangle(int triangle, const C2DPoint &p) const;

	  int moveToNextTriangleIndex(int triangle, double x, double y, int *visited, int nVisited) const;
	  int getNextTriangleZwickelIndex(int triangle, double x, double y, bool &hasZwickel) const;

	  int getPrevEdge(int triangle, int edge) const;
	  int getNextEdge(int triangle, int edge) const;

	  int findEdgeIndex(int p1, int p2) const;

	  bool right(int edgeIndex, double x, double y, int triangle) const;

	  int getNearestPoint(int triangle, double x, double y) const;

	  int checkInsertEdge(int triangle, int p1, int p2);

};


//********************************************************************************
//
// Class CGenericPointCloud
//
//********************************************************************************

class CGenericPointCloud: public CPointCloud
{
  protected:

	  double *coordinateVector;
	

  public:

	  CGenericPointCloud(int Capacity, int dim, int anndim = 2); 

	  CGenericPointCloud(const CXYZPointArray &Points, int dim, int anndim = 2); 

	  CGenericPointCloud(const CXYPointArray &Points, int dim); 

	  virtual ~CGenericPointCloud();

	  virtual bool setAnnDim(int anndim);

	  void init(const CXYZPointArray &Points, int dim, int anndim = 2);

	  void init(const CXYPointArray &Points, int dim);

	  ANNpoint addPoint(const C3DPoint &p);

	  ANNpoint addPoint(const C2DPoint &p);
	 	
	  void removePoint(int index);

	  void add(const CGenericPointCloud &pointcloud, bool initTree);
	  void merge(const CGenericPointCloud &pointcloud, bool initTree);

	  void init();

	  virtual void clear();

	  void dump(const char *filename, int indexZ) const;

	  bool dump(const char *filename) const;
	  bool init(const char *filename);

	  void prepareCapacity(int growBy, bool growInAnyCase = false);

	  bool interpolateRasterForALS(float *interpolatedBuf, float *approxBuf, 
		                           int width, int height, int bufwidth,
								   long index0, double x0, double y0, double dx, double dy, 
		                           int offsetX, int offsetY,
								   int N, float &zmin, float &zmax,
							       double maxDistInterpolate,
								   double maxDistMinDist,
								   double minsquareDistForWeight, 
								   CRobustWeightFunction *positiveBranch, 
								   CRobustWeightFunction *negativeBranch,
								   unsigned char *usedBuf = 0);
		  
	  bool interpolateRasterFinalDTM(float *interpolatedBuf, float *approxBuf, 
		                             int width, int height, int bufwidth,
								     long index0, double x0, double y0, double dx, double dy, 
		                             int offsetX, int offsetY,
									 int N, float &zmin, float &zmax,
							         double maxDistInterpolate,
								     double maxDistMinDist,
								     double minsquareDistForWeight, 
								     CRobustWeightFunction *positiveBranch, 
								     CRobustWeightFunction *negativeBranch, ofstream &DTMKoord,
								     unsigned char *usedBuf = 0);
};


#endif