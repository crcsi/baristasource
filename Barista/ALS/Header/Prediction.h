#ifndef ___PREDICTION_HPP___
#define ___PREDICTION_HPP___

#include <ANN/ANN.h>

#include "3DPoint.h"

class CPointCloud;

class CLinearPrediction
{
  protected:

	  C3DPoint TrendParameters;
	  C3DPoint Cog;

	  double Vzz;

	  double sigmaZ;

	  double covarPar;

	  CPointCloud *pcloud;

	  ANNpointArray InterpolationPoints;
	
	  ANNdistArray nnDists;

	  int nInterpolationPoints;

	  int coordIdx;

  public:

	  CLinearPrediction(CPointCloud *pPointcloud, int N, double SigmaZ, int CoordIdx, double deltaD0);

	  ~CLinearPrediction();

	  
	  bool isInitialised() const { return this->Vzz > 0.0; };

	  void interpolateRaster(float *buf, int width, int height, int bufwidth,
							 double x0, double y0, double dx, double dy, 
		                     int N, float &zmin, float &zmax);

	  void interpolateRaster(float *buf, int width, int height, int bufwidth,
							 double x0, double y0, double dx, double dy, 
		                     float &zmin, float &zmax);


  protected:

	  double getTrend(double x, double y) const;

	  void initInterpolationPoints(int N);

	  void initTrend();

	  void initCovarPars(double deltaD0);

	  void clear();

	  double getCovar(double dx, double dy) const { return getCovar(dx * dx + dy * dy); };

	  double getCovar(double sqrDist) const { return this->Vzz * exp(-sqrDist * this->covarPar); };
};

#endif