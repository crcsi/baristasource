#ifndef ___ALSPOINTCLOUD_HPP___
#define ___ALSPOINTCLOUD_HPP___

#include "3DPoint.h"

#include <ANN/ANN.h>
#include "ReferenceSystem.h"
#include "GenericPointCloud.h"
#include "ALSPoint.h"

class CALSDataSet;

class CALSPointCloudBuffer : public CPointCloud
{
  protected:

	  CALSPoint **pointVec;

	  CReferenceSystem refsys;

  public:

	  CALSPointCloudBuffer(int AnnDim); 

	  ~CALSPointCloudBuffer();

	  virtual bool setAnnDim(int anndim);
	  bool initKDTree();

	  CALSPoint *addPoint(double x, double y, double z, int intensity); 

	  int get(CALSDataSet &als, const C3DPoint &min, const C3DPoint &max, int level);
	 
	  CReferenceSystem getReferenceSystem() const { return this->refsys;  }

	  virtual void clear();

	  int getNearestNeighbours(CALSPoint ** neighbours, double *sqrDist, int N, const C3DPoint &p);

	  int getNearestNeighbours(CALSPoint ** neighbours, double *sqrDist, int N, const C3DPoint &p, double maxDist);

	  CALSPoint *getPoint(int index) const { return pointVec[index]; };

	  void interpolateRaster(float *buf, int width, int height, int bufwidth,
							 double x0, double y0, double dx, double dy, 
		                     int N, int echo, eInterpolationMethod method, float &zmin, float &zmax);

	  void interpolateRaster(float *buf, int width, int height, int bufwidth,
							 double x0, double y0, double dx, double dy, 
		                     int N, int echo, eInterpolationMethod method, float &zmin, float &zmax,
							 double maxDist);

	  void interpolateIntensity(unsigned short *buf, int width, int height, double x0, double y0, 
								double dx, double dy, int N, int echo, eInterpolationMethod method);

	  void interpolateIntensity(unsigned short *buf, int width, int height, double x0, double y0, 
								double dx, double dy, int N, int echo, eInterpolationMethod method,
							    double maxDist);


	  void writeXYZ(const char *filename, int echo) const;

 protected:

	  double interpolateMovingHorizontalPlaneEcho(int N, int echo);

	  double interpolateMovingPlaneEcho(int N, int echo, const ANNpoint &queryPt);

};

#endif