#ifndef ___ALSDATAWITHSTRIPS_HPP___
#define ___ALSDATAWITHSTRIPS_HPP___

#include "ALSData.h"
#include "ALSStrip.h"
#include "3DPoint.h"
#include "Filename.h"
#include "Serializable.h"
#include "ReferenceSystem.h"
#include "ProgressHandler.h"

class CALSDataSetWithStrips : public CALSDataSet   
{
  protected:

	  vector<CALSStrip *> stripVec;

	 

  public:

	  CALSDataSetWithStrips(const char *filename, const CReferenceSystem &ref, bool open); 
	 
	  ~CALSDataSetWithStrips();

	  virtual int nStrips() const { return this->stripVec.size(); };

	  virtual bool hasStrips() const { return true; };

	  virtual bool hasTiles() const { return false; };

	  virtual CALSStrip *getStrip(int index) const { return this->stripVec[index]; };

	  virtual void initStatistics();
  
	  virtual int getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, int level);

	  virtual int getPointCloud(const C3DPoint &pmin, const C3DPoint &pmax, CALSPoint **&pointvec, int &capacity, int dCapacity, int level);

	  virtual CALSDataSet *getClone() const;

	
	  // CSerializable functions
	  bool isa(string& className)  const { return (className == "CALSDataSetWithStrips"); };
	  string getClassName()  const { return string( "CALSDataSetWithStrips"); };
	  virtual void reconnectPointers() { CSerializable::reconnectPointers(); };
	  virtual void resetPointers() { CSerializable::resetPointers(); };
	  virtual bool isModified() { return this->bModified; };
 
	  void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	 	
	  virtual bool exportXYZ(const CCharString &Path, int echo, bool exportTime, bool exportI);

	  virtual void clear() { for (int i = 0; i < nStrips(); ++i) this->deleteStrip(i); };
	  
	  virtual void clear(int level) { for (int i = 0; i < nStrips(); ++i) this->deleteStrip(i, level); };
	  
	  void dumpStripXYZ(int strip, const CCharString &FileName, int echo, bool exportTime, bool exportI);


  protected:

	  void dumpStripXYZ(int strip, ofstream &outFile, int echo, bool exportTime, bool exportI);

  	  virtual bool readOptechFile(ifstream &inFile, const C2DPoint &offset, long filesize);

  	  virtual bool readASCIIFileWithTime(ifstream &inFile, const C2DPoint &offset, bool multiple, 
								         bool timeFirst, long filesize);

  	  virtual bool readASCIIFileWithoutTime(ifstream &inFile, const C2DPoint &offset, bool multiple, 
									        long filesize);

  	  virtual bool readToposysFile(ifstream &inFile, const C2DPoint &offset, long filesize);

  	  virtual bool readLASFile1(FILE *inFile, const C2DPoint &offset, long filesize);

	  bool writeHeader(FILE* FP);

	  bool finishWriting(FILE* FP);

	  virtual bool retrieve(FILE* FP);

	  void readStrip(int index, FILE *FP);

	  void deleteStrip(int index);

	  void deleteStrip(int index, int level);

	  virtual void destroy();

	  void addStrip(CALSStrip *strip);

	  void initMinCommonLevel();

};

#endif