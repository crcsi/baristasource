#ifndef ___ALSSTRIP_HPP___
#define ___ALSSTRIP_HPP___
#include <float.h>

#include "ALSScanLine.h"
#include "2DPoint.h"

class CALSStrip
{
  protected:

	  C2DPoint centre;
	  C2DPoint dir;
  	  C2DPoint width;
	  double dpointDist;

	  double zmin, zmax;

	  fpos_t offset;

	  long nbytes;

	  long nSublevelOffset;

	  CALSScanLine **scanlines;

	  int nScanlines;

	  int nPoints;

	  int capacity;

	  double sinPhi, cosPhi;

	  vector<CALSStrip *> sublevels;

  public:

	  CALSStrip(): scanlines(0), capacity(0), nScanlines(0), width(-1.0,-1.0), 
		           dpointDist(0), nbytes(9 * sizeof(double) + 3 * sizeof(int) + 2 * sizeof(long)),
				   sinPhi (0), cosPhi(1), nPoints(0), zmin(FLT_MAX), zmax(-FLT_MAX), nSublevelOffset(0) { };
	  
	  CALSStrip(const CALSStrip &s);
	  
	  ~CALSStrip();

	  fpos_t getOffset() const { return this->offset; };

	  int nScans() const { return nScanlines; };

	  int getNPoints() const { return nPoints; };

	  void addScanline(CALSScanLine *l);

	  void initStatistics();
	  
	  void getMinMax(C3DPoint &Min, C3DPoint &Max);
	  
	  int getNSubLevels() const { return sublevels.size(); };

	  CALSStrip *getSubLevel(int index) const { return sublevels[index]; };

	  CALSScanLine * operator[] (int index) const { return scanlines[index]; }
	  CALSScanLine *&operator[] (int index)  { return scanlines[index]; }
	  
	  double scanDist()  const { return this->width.y / this->nScans(); };
	  double pointDist() const { return dpointDist; };

	  bool dump(FILE* fp) { return dump(fp, true); };

	  bool read(FILE* fp);

	  long sizeByte() const { return this->nbytes; };

	  bool retrieve(FILE* fp) { return retrieve(fp, true); };

	  void readScanLine(int index, FILE *fp);

	  void deleteScanLine(int index) { (*this)[index]->deletePoints(); };

	  void deleteScanLines();

	  void deleteScanLines(int level);

	  bool overlap(C3DPoint pmin, C3DPoint pmax, int &minScIdx, int &maxScIdx);

	  void toStripCoords(double X, double Y, double &xs, double &ys)
	  {
		  X -= this->centre.x;             Y -= this->centre.y;
		  xs =  cosPhi * X + sinPhi * Y;   ys = -sinPhi * X + cosPhi * Y;
	  }

	  void toWorldCoords(double xs, double ys, double &X, double &Y)
	  {
		  X =  cosPhi * xs - sinPhi * ys;  Y = sinPhi * xs + cosPhi * ys;
		  X += this->centre.x;             Y += this->centre.y;
	  }

	  double getZmin() const { return this->zmin; };

	  double getZmax() const { return this->zmax; };

  protected:

	  bool dump(FILE* fp, bool first);
	  bool retrieve(FILE* fp, bool first);

	  int getMinScanIdx(double xMin);

	  int getMaxScanIdx(double xMax);

	  void initSublevel();

	  void sortVec(vector<CALSPoint *> &pointVec, vector<double> &inprod);

	  void sortVecPartial(vector<CALSPoint *> &pointVec, vector<double> &inprod, long low, long high);

	  void swapVec(vector<CALSPoint *> &pointVec, vector<double> &inprod, long i1, long i2);
	
};

#endif