// BitmapHeader.cpp: implementation of the CBitmapHeader class.
//
//////////////////////////////////////////////////////////////////////

#include <cstring>
#include <fstream>
#include <assert.h>

using namespace std;

#include "BitmapHeader.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBitmapHeader::CBitmapHeader(const char* pFileName)
{
	assert(pFileName != NULL);

	m_FileName = pFileName;

	strcpy(m_Format, "\0");
	m_Rows = 0;
	m_Cols = 0;
	m_Channels = 0;
	m_nBitsPerChannel = 0;
	m_tlx = 0;
	m_tly = 0;
	m_dx = 0;
	m_dy = 0;
	m_Creation = 0;
	m_Update = 0;
	m_bRead = false;

}

CBitmapHeader::CBitmapHeader()
{
	strcpy(m_Format, "\0");
	m_Rows = 0;
	m_Cols = 0;
	m_Channels = 0;
	m_nBitsPerChannel = 0;
	m_tlx = 0;
	m_tly = 0;
	m_dx = 0;
	m_dy = 0;
	m_Creation = 0;
	m_Update = 0;
	m_bRead = false;

}
//////////////////////////////////////////////////////////////////////
// CBitmapHeader::~CBitmapHeader()
//
//
CBitmapHeader::~CBitmapHeader()
{
}

void CBitmapHeader::SetFileName(const char* pFileName)
{
	assert(pFileName != NULL);

	m_FileName = pFileName;

}

//////////////////////////////////////////////////////////////////////
// CBitmapHeader::_Read()
//
//
bool CBitmapHeader::_Read()
{
	char ch[2];
	long hd_size = -1;
	short bitcount = -1;

	// Open file
	ifstream bmp_image(m_FileName, ios::binary);

	// Sanity check
	if (!bmp_image)
	{
		// Close file
		bmp_image.close();
		return false;
	}

	// Check for "BM" which confirms it is a bitmap file
	bmp_image.get(ch[0]);
	bmp_image.get(ch[1]);

	if (strncmp(ch,"BM",2) != 0)
	{
		// Close file
		bmp_image.close();
		return false;
	}

	// Skipt first 14 bytes
	bmp_image.seekg(14,ios::beg);
	bmp_image.read((char *) &hd_size,sizeof(long));

	// Header size has to be 40 bytes to be valid
	if (hd_size!=40)
	{
		// Close file
		bmp_image.close();
		return false;
	}

	// Read width
	bmp_image.seekg(0,ios::cur);
	bmp_image.read((char *) &m_Cols,sizeof(long));

	// Read height
	bmp_image.seekg(0,ios::cur);
	bmp_image.read((char *) &m_Rows,sizeof(long));

	// Read bitcount (number of bits per pixel)
	bmp_image.seekg(2,ios::cur);
	bmp_image.read((char *) &bitcount,sizeof(short));

	// For now only doing 24 bits
	if (bitcount!=24)
	{
		// Close file
		bmp_image.close();
		return false;
	}

	// Pad out rest of header
	strcpy(m_Format, "PIPS");
	m_nBitsPerChannel = 8;
	m_Channels = 3;
	m_Creation = time(NULL);
	m_tlx = 0;
	m_tly = 0;
	m_dx = 0;
	m_dy = 0;

	// Close file
	bmp_image.close();

	m_bRead = true;
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// CBitmapHeader::GetBufferSize()
//
//
long CBitmapHeader::GetBufferSize()
{
	long rv = m_Rows * m_Cols * m_Channels;

	return rv;
}

///////////////////////////////////////////////////////////////////////////////
// CBitmapHeader::GetRows()
//
//
int	CBitmapHeader::GetRows()
{
	if (!m_bRead)
		_Read();

	return m_Rows;
}

///////////////////////////////////////////////////////////////////////////////
// CBitmapHeader::GetCols()
//
//
int	CBitmapHeader::GetCols()
{
	if (!m_bRead)
		_Read();

	return m_Cols;
}

///////////////////////////////////////////////////////////////////////////////
// CBitmapHeader::GetNumberOfChannels()
//
//
int	CBitmapHeader::GetNumberOfChannels()
{
	if (!m_bRead)
		_Read();

	return m_Channels;
}

///////////////////////////////////////////////////////////////////////////////
// CBitmapHeader::GetNumberOfBitPerChannel()
//
//
int CBitmapHeader::GetNumberOfBitsPerChannel()
{
	if (!m_bRead)
		_Read();

	return m_nBitsPerChannel;
}

