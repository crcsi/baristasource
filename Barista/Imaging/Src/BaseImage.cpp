#include <math.h>
#include "BaseImage.h"
#include "memory.h"
#include "ImageTile.h"
#include "histogram.h"
#include <algorithm>

CBaseImage::CBaseImage(void): halfRes(0), doubleSize(0)
{
	
    this->scanlineBuf = (unsigned char*)0;
}

CBaseImage::~CBaseImage(void)
{
    if (this->scanlineBuf != (unsigned char*)0)
	{
		delete [] this->scanlineBuf;
		this->scanlineBuf = NULL;
	}

	this->deleteHistograms();
}
	
int CBaseImage::getNumberOfPyramidLevels() const
{
	int nLevels = 1;
	if (this->halfRes) nLevels += this->halfRes->getNumberOfPyramidLevels();
	return nLevels;
};

bool CBaseImage::getBrightenedRect(unsigned char* buf, int top, int height, int left, int width, double multiplier)
{
    this->getRect(buf, top, height, left, width);

    if (this->getBpp() != 8)
        return false;

    int pixel, r, g, b;
    if (this->getBands() == 1)
    {
        for (int i = 0; i < height*width; i++)
        {
            pixel = (int)(buf[i]*multiplier+0.5);
            pixel = min(max(0, pixel), 255);
			buf[i] = pixel;
        }
    }
    else
    {
        for (int i = 0; i < 4*height*width; i+=4)
        {
            b = (int)(buf[i]*multiplier+0.5);
            g = (int)(buf[i+1]*multiplier+0.5);
            r = (int)(buf[i+2]*multiplier+0.5);

            b = min(max(0, b), 255);
            g = min(max(0, g), 255);
            r = min(max(0, r), 255);

			buf[i] = b;
            buf[i+1] = g;
            buf[i+2] = r;
        }
    }

    return true;
}

bool CBaseImage::getBrightenedRect(unsigned char* buf, int top, int height, int left, int width, int destHeight, int destWidth, double multiplier)
{
    this->getRect(buf, top, height, left, width, destHeight, destWidth);

    if (this->getBpp() != 8)
        return false;

    int pixel, r, g, b;
    if (this->getBands() == 1)
    {
        for (int i = 0; i < destHeight*destWidth; i++)
        {
            pixel = (int)(buf[i]*multiplier+0.5);
            pixel = min(max(0, pixel), 255);
            buf[i] = pixel;
        }
    }
    else
    {
        for (int i = 0; i < 4*destHeight*destWidth; i+=4)
        {
            b = (int)(buf[i]*multiplier+0.5);
            g = (int)(buf[i+1]*multiplier+0.5);
            r = (int)(buf[i+2]*multiplier+0.5);

            b = min(max(0, b), 255);
            g = min(max(0, g), 255);
            r = min(max(0, r), 255);

            buf[i] = b;
            buf[i+1] = g;
            buf[i+2] = r;
        }
    }

    return true;
}

/**
 * returns a volatile pointer to the pixel at coords x, y
 * if the image is eq. rgb 8 bit the returned pointer buf[0] = blue
 *                                                    buf[1] = green
 *                                                    buf[2] = red
 *
 * if the image is eq. rgb 16bit the returned pointer shuld be cast to 
 * unsigned short* then the same is true eq.          buf[0] = blue
 *                                                    buf[1] = green
 *                                                    buf[2] = red
 * 
 * if the image is eq. rgb float the returned pointer shuld be cast to 
 * float* then the same is true eq.                   buf[0] = blue
 *                                                    buf[1] = green
 *                                                    buf[2] = red
 *
 *
 */
unsigned char* CBaseImage::getPixel(int x,int y)
{

	 // pixel size in bytes
    int pixelSize= this->pixelSizeBytes();
	unsigned char* buf = this->scanline(y);

	return buf+x*pixelSize;
}

bool CBaseImage::getInterpolatedPixel(unsigned char* buf, double col, double row)
{

	/*
	/// new version
	int irow = (int)row;
	int icol = (int)col;

	if ( irow == 0 || irow == this->width || icol == 0 || icol == this->height )
	{
		this->getOnePixel(buf, icol, irow);
		return true;
	}

	float drow = (float)row - (float)irow;
	float dcol = (float)col - (float)icol;

	float p00, p01, p10, p11;
	
	if ( this->bpp == 8 )
	{
		unsigned char* buf00 = new unsigned char[this->bands*this->bpp];
		unsigned char* buf01 = new unsigned char[this->bands*this->bpp];
		unsigned char* buf10 = new unsigned char[this->bands*this->bpp];
		unsigned char* buf11 = new unsigned char[this->bands*this->bpp];

		this->getOnePixel(buf00, icol, irow);
		this->getOnePixel(buf01, icol, irow +1);
		this->getOnePixel(buf10, icol + 1, irow);
		this->getOnePixel(buf11, icol + 1, irow + 1);

		for ( int i = 0; i < this->bands; i++)
		{

			p00 = (float)buf00[i];
			p01 = (float)buf01[i];
			p10 = (float)buf10[i];
			p11 = (float)buf11[i];

			float a0 = p00;   
			float a1 = p10 - p00;
			float a2 = p01 - p00;
			float a3 = p11 - a0 - a1 - a2;					

			float fval = (float)(a0 + a1*drow + a2*dcol + a3*drow*dcol);
			unsigned char val = (unsigned char)fval;

			buf[i] = (unsigned char)val;
		}

		delete buf00;
		delete buf01;
		delete buf10;
		delete buf11;
	}
	if ( this->bpp == 16 )
	{
		unsigned short* shortbuf = (unsigned short*)buf;

		unsigned short* buf00 = new unsigned short[this->bands*this->bpp];
		unsigned short* buf01 = new unsigned short[this->bands*this->bpp];
		unsigned short* buf10 = new unsigned short[this->bands*this->bpp];
		unsigned short* buf11 = new unsigned short[this->bands*this->bpp];

		this->getOnePixel((unsigned char*)buf00, icol, irow);
		this->getOnePixel((unsigned char*)buf01, icol, irow +1);
		this->getOnePixel((unsigned char*)buf10, icol + 1, irow);
		this->getOnePixel((unsigned char*)buf11, icol + 1, irow + 1);

		for ( unsigned long i = 0; i < (unsigned long)this->bands; i++)
		{
			p00 = (float)buf00[i];
			p01 = (float)buf01[i];
			p10 = (float)buf10[i];
			p11 = (float)buf11[i];

			float a0 = p00;   
			float a1 = p10 - p00;
			float a2 = p01 - p00;
			float a3 = p11 - a0 - a1 - a2;					

			float fval = (float)(a0 + a1*drow + a2*dcol + a3*drow*dcol);
			unsigned short uval = (unsigned short)fval;
			shortbuf[i] = uval;
		}

		delete buf00;
		delete buf01;
		delete buf10;
		delete buf11;
	}

	return true;
	*/

	/// end of new version
//////////////////

	int irow = (int)row;
	int icol = (int)col;

	if ( irow == 0 || irow == this->width || icol == 0 || icol == this->height )
	{
		unsigned char *buf1 = this->getPixel(icol, irow);
		memcpy(buf, buf1, this->pixelSizeBytes());
		return true;
	}

	float drow = (float)row - (float)irow;
	float dcol = (float)col - (float)icol;

	float p00, p01, p10, p11;
	
	int delta = (this->bands == 3) ? 4 : this->bands;

	if ( this->bpp == 8 )
	{
		
		unsigned char *readBuf = new unsigned char[4 * delta];

		this->getRect(readBuf, irow, 2, icol, 2);
		
		for ( int i = 0; i < this->bands; i++)
		{
			int j = i;
			p00 = (float)readBuf[j]; j += delta;
			p01 = (float)readBuf[j]; j += delta;
			p10 = (float)readBuf[j]; j += delta;
			p11 = (float)readBuf[j]; 

			float a0 = p00;   
			float a1 = p10 - p00;
			float a2 = p01 - p00;
			float a3 = p11 - a0 - a1 - a2;					

			float fval = (float)(a0 + a1*drow + a2*dcol + a3*drow*dcol);
			unsigned char val = (unsigned char)fval;

			buf[i] = (unsigned char)val;
		}

		delete [] readBuf;
	}
	if ( this->bpp == 16 )
	{
		unsigned short* shortbuf = (unsigned short*)buf;

		unsigned short* readBuf = new unsigned short[4 * delta];
		
		this->getRect((unsigned char*)readBuf, irow, 2, icol, 2);

		for ( unsigned long i = 0; i < (unsigned long)this->bands; i++)
		{
			int j = i;
			p00 = (float)readBuf[j]; j += delta;
			p01 = (float)readBuf[j]; j += delta;
			p10 = (float)readBuf[j]; j += delta;
			p11 = (float)readBuf[j]; 


			float a0 = p00;   
			float a1 = p10 - p00;
			float a2 = p01 - p00;
			float a3 = p11 - a0 - a1 - a2;					

			float fval = (float)(a0 + a1*drow + a2*dcol + a3*drow*dcol);
			unsigned short uval = (unsigned short)fval;
			shortbuf[i] = uval;
		}

		delete [] readBuf;
	}

	return true;
	
}

/*
	unsigned short* pixel;
	
	

	// 16 bit grey scale image
    if ( (bands == 1) && (pixelSize == 2) )
    {
        pixel = (unsigned short*)(buf+x*pixelSize);
		val=(long)(*pixel);
   
    }
    else if ( (bands == 3) && (pixelSize == 8) ) //16-bit rgb image
    {
     
		switch (band){
			case 0://red
				 pixel = (unsigned short*)(buf+x*pixelSize+4);
				 break;
			case 1://green
				 pixel = (unsigned short*)(buf+x*pixelSize+2);
				 break;
			case 2://blue
				 pixel = (unsigned short*)(buf+x*pixelSize+0);
				break;

		}
          val =(long)( *pixel);
                
    }
    else if ( (bands == 4) && (pixelSize == 8) ) //4 bands, 16-bit rgb image
    {
		switch (band){
			case 0://red
				pixel = (unsigned short*)(buf+x*pixelSize+2);
				break;
			case 1://green
				pixel = (unsigned short*)(buf+x*pixelSize+4);
				break;

			case 2://blue
				pixel = (unsigned short*)(buf+x*pixelSize+6);
				break;
			case 3://NIR
				pixel = (unsigned short*)(buf+x*pixelSize+0);
				break;
		}
		 val =(long)( *pixel);
              
    }

    else if ( (bands == 4) && (pixelSize == 4) ) //4 bands, 8-bit rgb image
    {
       switch (band){
			case 0://red
				val =(long)( buf[x*pixelSize+3]);
				break;
			case 1://green
				val =(long)( buf[x*pixelSize+2]);
				break;
			case 2://blue
				val = (long)(buf[x*pixelSize+1]);
				break;
			case 3://nir
				val = (long)( buf[x*pixelSize+0]);
				break;
	   }
    }
    else
    {
	  pixel = (unsigned short*)(buf+x*pixelSize);
		val=(long)(*pixel);
    }
    
    return val;
	
}
*/
bool CBaseImage::getTileZoomUpBrightened(unsigned char* buf, int R, int C, int zoomUp, double multiplier)
{

    // determine the rect in the original image
    int tileWidth = this->getTileWidth();
    int tileHeight = this->getTileHeight();

    R = (R/tileHeight)*tileHeight;
    C = (C/tileWidth)*tileWidth;

    int r1 = R/zoomUp;
    int c1 = C/zoomUp;

    unsigned char* srctmp = this->getTile(r1, c1);
    
    if (srctmp == NULL)
        return false;
        
    if (buf == NULL)
        return false;

    if (this->getBpp() != 8)
        return false;

    // set up the look up table
    char lut[256];
    for (int i = 0; i < 256; i++)
    {
        int pixel = (int)(i*multiplier+0.5);
        pixel = min(max(0, pixel), 255);
        lut[i] = pixel;
    }


    unsigned char* src = new unsigned char[tileWidth*tileHeight*this->pixelSizeBytes()];

    //::memcpy(src, srctmp, tileWidth*tileHeight*this->pixelSizeBytes());

    int destHeight = tileHeight/zoomUp;
    int destWidth = tileWidth/zoomUp;

    if (this->getBands() == 1)
    {
        for (int i = 0; i < tileWidth*tileHeight; i++)
            src[i] = lut[srctmp[i]];
    }
    else
    {
        for (int i = 0; i < 4*tileWidth*tileHeight; i+=4)
        {
            src[i] = lut[srctmp[i]];
            src[i+1] = lut[srctmp[i+1]];
            src[i+2] = lut[srctmp[i+2]];
        }
    }

    int pixelSize = this->pixelSizeBytes();
    
    int rStart = r1 % tileHeight;
    int rEnd = rStart + (tileHeight/zoomUp);
    int cStart = c1 % tileWidth;
    int cEnd = cStart + (tileWidth/zoomUp);

    int nBytesPerLine = tileWidth*pixelSize;
    int destLineOffset = 0;
    int srcLineOffset = rStart*nBytesPerLine;

    for (int r = rStart, destR = 0; r < rEnd; r++, destR += zoomUp)
    {
        for (int c = cStart, destC = 0; c < cEnd; c++, destC += zoomUp)
        {
            destLineOffset = destR*nBytesPerLine;
            for (int i = 0; i < zoomUp; i++)
            {
                for (int j = 0; j < zoomUp; j++)
                    memcpy(buf+destLineOffset+(destC+j)*pixelSize, src+srcLineOffset+c*pixelSize, pixelSize);

                destLineOffset += nBytesPerLine;
            }
        }

        srcLineOffset += nBytesPerLine;
    }

    delete [] src;

 return true;
}

bool CBaseImage::getTileZoomDownBrightened(unsigned char* buf,int r, int c, int zoomDown, double multiplier)
{
//    if (!this->getTileZoomDown(buf, r, c, zoomDown))
//        return false;

    int tileWidth = this->getTileWidth();
    int tileHeight = this->getTileHeight();

    // set up the look up table
    char lut[256];
    for (int i = 0; i < 256; i++)
    {
        int pixel = (int)(i*multiplier+0.5);
        pixel = min(max(0, pixel), 255);
        lut[i] = pixel;
    }

    if (this->getBands() == 1)
    {
        for (int i = 0; i < tileWidth*tileHeight; i++)
            buf[i] = lut[buf[i]];
    }
    else
    {
        for (int i = 0; i < 4*tileWidth*tileHeight; i+=4)
        {
            buf[i] = lut[buf[i]];
            buf[i+1] = lut[buf[i+1]];
            buf[i+2] = lut[buf[i+2]];
        }
    }


    return true;
}

bool CBaseImage::setRect(unsigned char* buf, int rectTop, int rectHeight, int rectLeft, int rectWidth)
{
 //   if ( (rectTop < 0) || (rectTop+rectHeight) > this->getHeight())
   //     return false;

//    if ( (rectLeft < 0) || (rectLeft+rectWidth) > this->getWidth())
//        return false;

    // pixel size in bytes
    int pixelSize = this->pixelSizeBytes();
    
    CImageTile* imageTile = 0;

    int leftTile = rectLeft/this->tileWidth;

    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (rectLeft < 0) && (rectLeft%this->tileWidth != 0) )
        leftTile--;

    int rightTile = (rectLeft+rectWidth)/this->tileWidth;
    
    int tileTouchedAcross = rightTile-leftTile+1;
    
    int topTile = rectTop/this->tileHeight;
    
    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (rectTop < 0) && (rectTop%this->tileHeight != 0) )
        topTile--;

    int bottomTile = (rectTop+rectHeight)/this->tileHeight;
    int tileTouchedDown = bottomTile-topTile+1;
    
    // this outer loop is for determining the current tile only
    for (int i = 0, tr = rectTop; i < tileTouchedDown; i++, tr+= this->tileHeight)
    {
        for (int j = 0, tc = rectLeft; j < tileTouchedAcross; j++, tc+= this->tileWidth)
        {
            // the current tile
            this->getImageTile(&imageTile, tr, tc);
        
            // sanity check
            if (imageTile == NULL)
                continue;
            
            // the tile extents
            int tileTop = imageTile->getTileRow() * this->tileHeight;
            int tileLeft = imageTile->getTileCol() * this->tileWidth;
            int tileBottom = tileTop + this->tileHeight;
            int tileRight = tileLeft + this->tileWidth;

            // the source data
            unsigned char* dest = imageTile->getData();
            
            int lineOffset;
            int tileLineOffset;
            int rowInTile;
            int rowInRect;
            int colInTile;
            int colInRect;
            
            // now fill the destination buf
            for (int r = max(rectTop, tileTop); r < min(tileBottom, rectTop+rectHeight); r++)
            {
                rowInTile = r-tileTop;
                rowInRect = r-rectTop;
            
                lineOffset = rowInRect*rectWidth*pixelSize;
                tileLineOffset = rowInTile*this->tileWidth*pixelSize;
                
                int cLeft = max(rectLeft, tileLeft);
                int cRight = min(rectLeft+rectWidth, tileRight);
                int w = cRight - cLeft;
            
                colInTile = cLeft-tileLeft;
                colInRect = cLeft-rectLeft;
            
 //               memcpy(buf+lineOffset+colInRect*pixelSize, src+tileLineOffset+colInTile*pixelSize, pixelSize*w);
                memcpy(dest+tileLineOffset+colInTile*pixelSize, buf+lineOffset+colInRect*pixelSize, pixelSize*w);
            }
        }
    }    

//    if (this->halfRes)
//    {
//        unsigned char* tmp = new unsigned char[rectWidth/2*rectHeight/2*pixelSize];
//        this->createHalfResRect(tmp, buf, rectHeight, rectWidth);
//        this->halfRes->setRect(tmp, rectTop/2, rectHeight/2, rectLeft/2, rectWidth/2);
//    }

    return true;
}

bool CBaseImage::createHalfResTile(int r, int c)
{
    // This calculation simply makes r and c the upper left corner
    r = (r/this->tileHeight)*this->tileHeight;
    c = (c/this->tileWidth)*this->tileWidth;

    // our current tile row and col
    int tileCol = c/this->tileWidth;
    int tileRow = r/this->tileHeight;
    
    // this is the CImageTile we are working on
    unsigned char* dest = this->getTile(r, c);
    
    // R, C the row col in the double size image
    int R = r*2;
    int C = c*2;

    // the four tiles from the double size image
    unsigned char* t1 = this->doubleSize->getTile(R, C);

    // first put every other pixel from the upper left tile
    unsigned char* src = 0;

    int pixelSize = this->pixelSizeBytes();
    if (t1 != NULL)
    {
        src = t1;
        for (int rr = 0; rr < this->tileHeight/2; rr++)
        {
            int lineOffset = rr*this->tileWidth*pixelSize;
            int lineOffsetSrc = rr*2*this->tileWidth*pixelSize;

            for (int cc = 0; cc < this->tileWidth/2; cc++)
            {
                int srcOffset = lineOffsetSrc+cc*2*pixelSize;
                
                memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
            }
        }
    }

    unsigned char* t2 = this->doubleSize->getTile(R, C+this->tileWidth);
    // the the upper right tile
    if (t2 != NULL)
    {
        src = t2;
        for (int rr = 0; rr < this->tileHeight/2; rr++)
        {
            int lineOffset = rr*this->tileWidth*pixelSize;
            int lineOffsetSrc = rr*2*this->tileWidth*pixelSize;
            for (int cc = this->tileWidth/2; cc < this->tileWidth; cc++)
            {
                int srcOffset = lineOffsetSrc+(cc-this->tileWidth/2)*2*pixelSize;
                
                memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
            }
        }
    }
    
    unsigned char* t3 = this->doubleSize->getTile(R+this->tileHeight, C);
    // the lower left tile
    if (t3 != NULL)
    {
        src = t3;
        for (int rr = this->tileHeight/2; rr < this->tileHeight; rr++)
        {
            int lineOffset = rr*this->tileWidth*pixelSize;

            int lineOffsetSrc = (rr-this->tileHeight/2)*2*this->tileWidth*pixelSize;
            for (int cc = 0; cc < this->tileWidth/2; cc++)
            {
                //int srcOffset = lineOffset+cc*2*pixelSize;
                int srcOffset = lineOffsetSrc+cc*2*pixelSize;
                
                memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
            }
        }
    }
    
    unsigned char* t4 = this->doubleSize->getTile(R+this->tileHeight, C+this->tileWidth);
    // the lower right tile
    if (t4 != NULL)
    {
        src = t4;
        for (int rr = this->tileHeight/2; rr < this->tileHeight; rr++)
        {
            int lineOffset = rr*this->tileWidth*pixelSize;
            int lineOffsetSrc = (rr-this->tileHeight/2)*2*this->tileWidth*pixelSize;
            for (int cc = this->tileWidth/2; cc < this->tileWidth; cc++)
            {
                //int srcOffset = lineOffset+(cc-this->tileWidth/2)*2*pixelSize;
                    int srcOffset = lineOffsetSrc+(cc-this->tileWidth/2)*2*pixelSize;
                
                memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
            }
        }
    }

    return true;
}


/**
 * Gets a tile of imagery into given buf
 * RGB imagery is returned BGR0BGR0...
 * @param r, c any row and column inside the tile
 *
 * @return a volatile memory buf 
 * i.e. get the data and use it or copy it but don't expect 
 * it to be around and by no means delete it!!!
 *
 */
unsigned char* CBaseImage::getTile(int r, int c)
{
    CImageTile* imageTile;
    
    if (!this->getImageTile(&imageTile, r, c))
        return 0;

    return imageTile->getData();
}


/**
  * Gets one pixel from imagery into the supplied buf
  * @param unsigned char* buf the destination buffer
  * RGB imagery is returned BGR0BGR0...
  * @param buf, col, row
  * @return false for failure
  *
  */
bool CBaseImage::getOnePixel(unsigned char* buf, int col, int row)
{
    CImageTile* imageTile = NULL;

    int tc = col/this->tileWidth;
    int tr = row/this->tileHeight;
    
	// the current tile
	this->getImageTile(&imageTile, row, col);

	// the source data
	unsigned char* src = imageTile->getData();

	int rowInTile = row - tr*this->tileHeight;
	int colInTile = col - tc*this->tileWidth;

	int index = 0;

	if ( this->bands == 1 )
		index = rowInTile*this->tileWidth+colInTile;
	if ( this->bands == 3 || this->bands == 4)
		index = 4*(rowInTile*this->tileWidth+colInTile);

	if ( this->bpp == 8 )
	{
		for ( int i = 0; i < this->bands; i++)
		{
			buf[i] = src[index+i];
		}
	}
	if ( this->bpp == 16 )
	{
		unsigned short* shortbuf = (unsigned short*)buf;
		unsigned short* shortsrc = (unsigned short*)src;

		memcpy(shortbuf, shortsrc+index, this->bands*2);
	}

    return true;
}


CLookupTable CBaseImage::get16To8ConversionLookupTable() const
{
	CLookupTable lut(this->bpp, this->bands);

	int maxVal = 0, minVal = 0;

	if (this->bpp == 16)
	{
		if (this->bands == 1)
		{
			maxVal = this->Histograms[0]->percentile(0.98);
			minVal = this->Histograms[0]->percentile(0.02);
		}
		else if (this->bands == 3)
        {
            int MinRed   = this->Histograms[2]->percentile(0.1);
            int MaxRed   = this->Histograms[2]->percentile(0.999);
            int MinGreen = this->Histograms[1]->percentile(0.08);
            int MaxGreen = this->Histograms[1]->percentile(0.997);
            int MinBlue  = this->Histograms[0]->percentile(0.1);
            int MaxBlue  = this->Histograms[0]->percentile(0.999);

			if ((MinRed < MinGreen) && (MinRed < MinBlue)) minVal = MinRed;
			else if (MinGreen < MinBlue) minVal = MinGreen;
			else minVal = MinBlue;

			if ((MaxRed > MaxGreen) && (MaxRed > MaxBlue)) maxVal = MaxRed;
			else if (MaxGreen > MaxBlue) maxVal = MaxGreen;
			else maxVal = MaxBlue;
        }
		else if (this->bands == 4)
        {
            int MinNIR   = this->Histograms[3]->percentile(0.02);
            int MaxNIR   = this->Histograms[3]->percentile(0.95);
            int MinRed   = this->Histograms[2]->percentile(0.02);
            int MaxRed   = this->Histograms[2]->percentile(0.95);
            int MinGreen = this->Histograms[1]->percentile(0.05);
            int MaxGreen = this->Histograms[1]->percentile(0.985);
            int MinBlue  = this->Histograms[0]->percentile(0.05);
            int MaxBlue  = this->Histograms[0]->percentile(0.95);

			if ((MinNIR < MinRed) && (MinNIR < MinGreen) && (MinNIR < MinBlue)) minVal = MinNIR;
			else if ((MinRed < MinGreen) && (MinRed < MinBlue)) minVal = MinRed;
			else if (MinGreen < MinBlue) minVal = MinGreen;
			else minVal = MinBlue;

			if ((MaxNIR > MaxRed) && (MaxNIR > MaxGreen) && (MaxNIR > MaxBlue)) maxVal = MaxNIR;
			else if ((MaxRed > MaxGreen) && (MaxRed > MaxBlue)) maxVal = MaxRed;
			else if (MaxGreen > MaxBlue) maxVal = MaxGreen;
			else maxVal = MaxBlue;
        }
    }
	else if (this->bpp == 32)
	{
		maxVal = this->Histograms[0]->value(0);
        minVal = this->Histograms[0]->value(1);
	}

	if (maxVal > minVal)
	{
		float fact = (float)255.0 / (maxVal - minVal);

		for (int i= 0; i < lut.getMaxEntries(); i++)
		{
			int newVal = (int) floor(float(i - minVal) * fact + 0.5);
			if (newVal < 0) newVal = 0;
			else if (newVal > 255) newVal = 255;
			lut[i] = newVal;
		}
	};

	return lut;
};


void CBaseImage::deleteHistograms()
{
	for (unsigned int i = 0; i < this->Histograms.size(); i++)
	{
		delete this->Histograms[i];
		this->Histograms[i] = NULL;
	}

	this->Histograms.clear();
	
	if (this->halfRes)
	{
		this->halfRes->deleteHistograms();
	}
};

void CBaseImage::initHistograms()
{
	this->deleteHistograms();

	this->Histograms.resize(this->bands);
    for (int i = 0; i < this->bands; i++)
    {
        this->Histograms[i] = new CHistogram(this->bpp);
	};

};


/**
 * get the historam for the given band
 * @param band the band to histogram (default = 0)
 * @return a pointer to the CHistogram object
 * @see CHistogram
 */
CHistogram* CBaseImage::getHistogram(int band,int pyramidLevel)
{
    if ( (band < 0) || (band >= this->bands) ||  pyramidLevel < 0)
        return NULL;

	if (pyramidLevel > 0 && this->halfRes)
		return this->halfRes->getHistogram(band, --pyramidLevel);
	else if (pyramidLevel > 0 && !this->halfRes)
		return NULL;

	return this->Histograms[band];
}


const vector<CHistogram*>& CBaseImage::getHistograms(int pyramidLevel) const
{
	// when pyramidLevel < 0 just return the histograms of this level 0
	// when pyramidLevel > number of pyramides return histograms of highes level

	if (pyramidLevel > 0 && this->halfRes)
		return this->halfRes->getHistograms(--pyramidLevel);

	return this->Histograms;
}

