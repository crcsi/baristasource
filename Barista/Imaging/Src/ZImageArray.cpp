#include "ZImageArray.h"

CZImageArray::CZImageArray(int nGrowBy) :
	CMUObjectArray<CZImage, CZImagePtrArray>(nGrowBy)
{
}

CZImageArray::~CZImageArray(void)
{
}

/*
void CZImageArray::removeZImage(CZImage* img)
{
	this->Remove(img);
}

// reading
void CZImageArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	for(int i = 0; i < this->GetSize(); i++)
	{
		CZImage* pImg = this->GetAt(i);

		CStructuredFileSection* pNewSection = pStructuredFileSection->addChild();

		pImg->serializeStore(pNewSection);
	}
}

// writing
void CZImageArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
	{
		CStructuredFileSection* pSection = pStructuredFileSection->getChildren()->GetAt(i);
	
		if(pSection->getName().CompareNoCase("CZImage"))
		{
			CZImage* pImg = this->Add();
			pImg->serializeRestore(pSection);
		}
	}
}

bool CZImageArray::isModified()
{
	for(int i = 0; i < this->GetSize(); i++)
	{
		CZImage* pImg = this->GetAt(i);
		if (pImg->isModified())
			return true;
	}

	return false;
}

void CZImageArray::reconnectPointers()
{
	CSerializable::reconnectPointers();

	for(int i = 0; i < this->GetSize(); i++)
	{
		CZImage* pImg = this->GetAt(i);
		pImg->reconnectPointers();
	}
}

void CZImageArray::resetPointers()
{
	CSerializable::resetPointers();

	for(int i = 0; i < this->GetSize(); i++)
	{
		CZImage* pImg = this->GetAt(i);
		pImg->resetPointers();
	}
}

CZImage* CZImageArray::Add()
{
	CZImage* pImg = CMUObjectArray<CZImage, CZImagePtrArray>::Add();
	pImg->setParent(this);
	return pImg;
}

CZImage* CZImageArray::Add(const CZImage& img)
{
	CZImage* pNewImg = CMUObjectArray<CZImage, CZImagePtrArray>::Add(img);
	pNewImg->setParent(this);
	return pNewImg;
}

*/