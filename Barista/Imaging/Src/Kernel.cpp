#include "Kernel.h"

CKernel::CKernel(int size)
{
    this->size = size;
    this->data = new float*[size];

    for (int i = 0; i < size; i++)
    {
        this->data[i] = new float[size];
    }
}

CKernel::~CKernel(void)
{
    for (int i = 0; i < size; i++)
    {
        delete [] this->data[i];
    }

    delete [] this->data;
}
