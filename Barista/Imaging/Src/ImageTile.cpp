/** 
 * @class CImageTile
 * 
 * 
 * @author Harry Hanley
 * @version 1.0 
 * @date 20-jan-2005
 *
 */

#include "ImageTile.h"
#include "memory.h"
#include "ZRasterHelper.h"

#ifndef NULL
#define NULL 0
#endif

/**
 * Constructor
 */
CImageTile::CImageTile(int w, int h, int bpp, int bands)
{
	this->width = w;			
	this->height = h;			
	this->bpp = bpp;			
	this->bands = bands;			

	int pixelSize = this->pixelSizeBytes();

	this->dataSize = this->width*this->height*pixelSize;
	this->data = ZRasterHelper::alloc(this->dataSize);
}

/**
 * deconstructor
 */
CImageTile::~CImageTile(void)
{
	if (this->data != NULL)
	{
		delete [] this->data;
		this->data = NULL;
	}
}

int CImageTile::getWidth()
{
	return this->width;
}

int CImageTile::getHeight()
{
	return this->height;
}

unsigned char* CImageTile::getData()
{
	return this->data;
}

int CImageTile::getBpp()
{
	return this->bpp;
}

int CImageTile::getBands()
{
	return this->bands;
}

void CImageTile::setData(unsigned char* data)
{
	memcpy(this->data, data, this->dataSize);
}

void CImageTile::setTileRowCol(int r, int c)
{
	this->tileRow = r;
	this->tileCol = c;
}

int CImageTile::getTileRow()
{
	return this->tileRow;
}

int CImageTile::getTileCol()
{
	return this->tileCol;
}

/**
 * computes the pixel size in bytes
 * @return the number of byte a pixel takes
 */
int CImageTile::pixelSizeBytes()
{
    int pixelSize;
    
    if (this->bands == 3)
        pixelSize = 4*this->bpp/8;
    else if (this->bands == 4)
        pixelSize = 4*this->bpp/8;
    else 
        pixelSize = this->bands * this->bpp/8;

    return pixelSize;
}