/** 
 * @class CZImage
 * Provides functionally for handling huge 50MB - many Gig image files.
 * Creates a *.hug file that is a tiled and pyramided image database.
 * The hug file has the same name as the image file with .hug appended
 * 
 * 
 * @author Harry Hanley
 * @version 1.0 
 * @date 20-jan-2005
 *
 */
#include "ZImage.h"
#include "ImageFile.h"
#include "ImageTile.h"
#include "ImgBuf.h"
#include "string.h"
#include "histogram.h"
#include "math.h"
#include "Kernel.h"

#ifndef max 
#define max(a,b) (((a) > (b) ) ? (a) : (b))
#endif

#ifndef min 
#define min(a,b) (((a) < (b) ) ? (a) : (b))
#endif

#define MAX_MEG 20000000
#define PAN_SHARPEN_FLOAT_LIMIT 500

int CZImage::tileCreationCount = 0;
int CZImage::totalTileCount = 0;

#define HUG_FILE_VERSION 1

/** 
* Default constructor
*/
CZImage::CZImage(void)
{
    strcpy(this->filename, "");
//    this->version = HUG_FILE_VERSION;
    this->bpp = 0;
    this->bands = 0;
    this->width = 0;
    this->height = 0;
    this->tilesAcross = 0;
    this->tilesDown = 0;
    this->tileWidth = 0;
    this->tileHeight = 0;
    this->halfRes = NULL;
    this->doubleSize = NULL;
    this->tiles = 0;
    this->nBufferedTiles = 0;

//    this->bits = NULL;
    this->listener = NULL;
}

CZImage::CZImage(int w, int h, int bpp, int bands)
{
    strcpy(this->filename, "");
//    this->version = HUG_FILE_VERSION;
    this->bpp = 0;
    this->bands = 0;
    this->width = 0;
    this->height = 0;
    this->tilesAcross = 0;
    this->tilesDown = 0;
    this->tileWidth = 0;
    this->tileHeight = 0;
    this->halfRes = NULL;
    this->doubleSize = NULL;
    this->tiles = 0;
    this->nBufferedTiles = 0;

//    this->bits = NULL;
    this->listener = NULL;

    this->openNew(w, h, bpp, bands);
}

/**
 * destructor 
 * recurses the link list of CZImages
 */
CZImage::~CZImage(void)
{
    if (this->halfRes != NULL)
    {
        delete this->halfRes;
        this->halfRes = NULL;
    }
    
    for (int i = 0; i < this->nBufferedTiles; i++)
    {
        delete this->tiles[i];
        this->tiles[i] = NULL;
    }
    
    delete [] this->tiles;
    
//    if (this->bits != NULL)
 //       delete this->bits;
}

/*
void CZImage::deleteTiles(void)
{
    if (this->halfRes != NULL)
    {
        delete this->halfRes;
        this->halfRes = NULL;
    }
    
    for (int i = 0; i < this->nBufferedTiles; i++)
    {
        delete this->tiles[i];
        this->tiles[i] = NULL;
    }
    
    delete this->tiles;
    
//    if (this->bits != NULL)
 //       delete this->bits;
}
*/
/** 
 * Opens the CZImage for writing. Creates the image.
 *
 * @param filename The filename of the dest *.hug
 * @param int w, h the width and height of the image
 * @param bpp bits per pixel
 * @param bands number of bands (channels) 
 * @return true for success / false for failure
 */
bool CZImage::openNew(int w, int h, int bpp, int bands)
{
    CZImage::tileCreationCount = 0;
    this->width = w;
    this->height = h;
    this->bands = bands;
    this->bpp = bpp;

	// HBH!!!
	this->tileWidth = min(TILE_SIZE, w);
	this->tileHeight = min(TILE_SIZE, h);

    //strcpy(this->filename, filename);
    //this->fp = fopen(this->filename, "w+b");

    // sanity check
	/*
    if (this->fp == NULL)
        return false;   if (this->doubleSize == NULL)
        this->dataOffset = this->headerSize();
	*/

    // create with NULL create an empty shell
    return this->create(NULL);
}

/** 
 * Opens the CZImage for reading. 
 *
 * @param imageFile A CImageFile object (must have been previously created)
 * @return true for success / false for failure
 */
bool CZImage::openR(CImageFile* imageFile)
{    
	this->width = imageFile->getWidth();
    this->height = imageFile->getHeight();

	// HBH!!!
	this->tileWidth = min(TILE_SIZE, width);
	this->tileHeight = min(TILE_SIZE, height);

	// how could either width or height be zero ?
	if(this->width <= 0 || this->height <= 0)
		return false;

    CZImage::tileCreationCount = 0;
	CZImage::totalTileCount = this->computeTotalTileCount(this->width, this->height);

    this->bpp = imageFile->getBpp();
    this->bands = imageFile->bands();    
 
    return this->create(imageFile);
   
    return false;
}

/*
bool CZImage::setRect(unsigned char* buf, int rectTop, int rectHeight, int rectLeft, int rectWidth)
{
 //   if ( (rectTop < 0) || (rectTop+rectHeight) > this->getHeight())
   //     return false;

//    if ( (rectLeft < 0) || (rectLeft+rectWidth) > this->getWidth())
//        return false;

    // pixel size in bytes
    int pixelSize = this->pixelSizeBytes();
    
    CImageTile* imageTile = NULL;

    int leftTile = rectLeft/this->tileWidth;

    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (rectLeft < 0) && (rectLeft%this->tileWidth != 0) )
        leftTile--;

    int rightTile = (rectLeft+rectWidth)/this->tileWidth;
    
    int tileTouchedAcross = rightTile-leftTile+1;
    
    int topTile = rectTop/this->tileHeight;
    
    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (rectTop < 0) && (rectTop%this->tileHeight != 0) )
        topTile--;

    int bottomTile = (rectTop+rectHeight)/this->tileHeight;
    int tileTouchedDown = bottomTile-topTile+1;
    
    // this outer loop is for determining the current tile only
    for (int i = 0, tr = rectTop; i < tileTouchedDown; i++, tr+= this->tileHeight)
    {
        for (int j = 0, tc = rectLeft; j < tileTouchedAcross; j++, tc+= this->tileWidth)
        {
            // the current tile
            this->getImageTile(&imageTile, tr, tc);
        
            // sanity check
            if (imageTile == NULL)
                continue;
            
            // the tile extents
            int tileTop = imageTile->getTileRow() * this->tileHeight;
            int tileLeft = imageTile->getTileCol() * this->tileWidth;
            int tileBottom = tileTop + this->tileHeight;
            int tileRight = tileLeft + this->tileWidth;

            // the source data
            unsigned char* dest = imageTile->getData();
            
            int lineOffset;
            int tileLineOffset;
            int rowInTile;
            int rowInRect;
            int colInTile;
            int colInRect;
            
            // now fill the destination buf
            for (int r = max(rectTop, tileTop); r < min(tileBottom, rectTop+rectHeight); r++)
            {
                rowInTile = r-tileTop;
                rowInRect = r-rectTop;
            
                lineOffset = rowInRect*rectWidth*pixelSize;
                tileLineOffset = rowInTile*this->tileWidth*pixelSize;
                
                int cLeft = max(rectLeft, tileLeft);
                int cRight = min(rectLeft+rectWidth, tileRight);
                int w = cRight - cLeft;
            
                colInTile = cLeft-tileLeft;
                colInRect = cLeft-rectLeft;
            
 //               memcpy(buf+lineOffset+colInRect*pixelSize, src+tileLineOffset+colInTile*pixelSize, pixelSize*w);
                memcpy(dest+tileLineOffset+colInTile*pixelSize, buf+lineOffset+colInRect*pixelSize, pixelSize*w);
            }
        }
    }    

//    if (this->halfRes)
//    {
//        unsigned char* tmp = new unsigned char[rectWidth/2*rectHeight/2*pixelSize];
//        this->createHalfResRect(tmp, buf, rectHeight, rectWidth);
//        this->halfRes->setRect(tmp, rectTop/2, rectHeight/2, rectLeft/2, rectWidth/2);
//    }

    return true;
}
*/

/**
 * sets the pixel data. imagery is written to disk
 * @param buf the source buffer
 * @param r, c the upper left corner of the tile
 *
 */
void CZImage::setTile(unsigned char* buf, int r, int c)
{
    unsigned char* dest = this->getTile(r, c);

    if (dest == NULL)
        return;
        
    int pixelSize = this->pixelSizeBytes();
    
    ::memcpy(dest, buf, this->tileWidth*this->tileHeight*pixelSize);
    float* hmm = (float*)buf;
    //file_offset offset = this->computeFileOffset(r, c);
    //CZImage::SEEK(this->fp, &offset);

    //fwrite(dest, sizeof(unsigned char), this->tileWidth*this->tileHeight*pixelSize, this->fp);
    
    if (this->halfRes != NULL)
    {
        this->halfRes->createHalfResTile(r/2, c/2);

        unsigned char* buf = this->halfRes->getTile(r/2, c/2);
        this->halfRes->setTile(buf, r/2, c/2);
    }
}

/**
 * computes the pixel size in bytes
 * @return the number of byte a pixel takes
 */
int CZImage::pixelSizeBytes()
{
    int pixelSize;
    
    if (this->bands == 3)
	{
		pixelSize = 4*this->bpp/8;
	}
    else if (this->bands == 4)
        pixelSize = 4*this->bpp/8;
	else
        pixelSize = this->bpp/8;

    return pixelSize;
}

int CZImage::computeTotalTileCount(int h, int w)
{
    // calculate the number of tiles
    int across = w / this->tileWidth;

    // if there is a remainder, we need one more tile
    if (w % this->tileWidth != 0)
        across++;

    int down = h / this->tileHeight;

    // if there is a remainder, we need one more tile
    if (h % this->tileHeight != 0)
        down++;

    if ( (w < 2 * this->tileWidth) && (h < 2 * this->tileHeight) )
    {
        return across*down;
    }
    else
    {
        return across*down + this->computeTotalTileCount(h/2, w/2);
    }
}

/**
 * sets up a listener that queries the progress of the create
 * @param CImagingProgressListener the listener object
 *
 */
/*
void CZImage::setProgressListener(CImagingProgressListener* l)
{
    this->listener = l;
}
*/
/**
 * remove the listener. Should be called after a create IF a listener was used
 *
 */
/*
void CZImage::removeProgressListener()
{
    this->listener = NULL;
}
*/
/**
 * Protected function to create the image database.
 * @return false for failure
 *
 */
bool CZImage::create(CImageFile* imageFile)
{
   // calculate the number of tiles
    this->tilesAcross = this->width / this->tileWidth;
    
    // if there is a remainder, we need one more tile
    if (this->width % this->tileWidth != 0)
        this->tilesAcross++;
    
    this->tilesDown = this->height / this->tileHeight;
    
    // if there is a remainder, we need one more tile
    if (this->height % this->tileHeight != 0)
        this->tilesDown++;
    
	// Why would you not have square tiles? HBH!!!
    //this->tileWidth = TILE_SIZE;
    //this->tileHeight = TILE_SIZE;
    
    this->nBufferedTiles = this->tilesAcross*this->tilesDown;
    this->tiles = new CImageTile*[this->nBufferedTiles];

    for (int i = 0; i < this->nBufferedTiles; i++)
        this->tiles[i] = new CImageTile(this->tileWidth, this->tileHeight, this->bpp, this->bands);
    
    long fullWidth = this->tilesAcross*this->tileWidth;
    long fullHeight = this->tilesDown*this->tileHeight;
    
    // do we need to consider a sub image?
	bool subImage;
    
    if ( (this->width < 2 * this->tileWidth) && (this->height < 2 * this->tileHeight) )
        subImage = false;
    else
        subImage = true;

	subImage = false;

    int pixelSize = this->pixelSizeBytes();;
    
    // Write the tiles
    if (this->doubleSize == NULL) // I must be the top most image pyramid
    {
        unsigned char* line;
        //line = new unsigned char[this->width*pixelSize];
		line = ZRasterHelper::alloc(this->tileWidth*this->tilesAcross*pixelSize);//new unsigned char[this->tileWidth*this->tilesAcross*pixelSize]; 
       // memset(line, 0, this->tileWidth*this->tilesAcross*pixelSize);
        
        for (int r = 0, R=0; r < this->height; r+= this->tileHeight, R++)
        {
            // create all tiles accross
            for (int rowInTile = 0; rowInTile < this->tileHeight; rowInTile++)
            {
                if (imageFile) 
                    imageFile->readLine(line, r+rowInTile);
				else
					memset(line, 205, this->tileWidth*this->tilesAcross*pixelSize);

                for (int tileCol = 0; tileCol < this->tilesAcross; tileCol++)
                {

                    int tileOffset = rowInTile*this->tileWidth*pixelSize;
                    int lineOffset = (tileCol*this->tileWidth)*pixelSize;
                    
                    int tileRow = r/this->tilesDown;
					int tileIndex = R*this->tilesAcross+tileCol;

                    tiles[tileIndex]->setTileRowCol(R, tileCol);
     
                    unsigned char* buf = tiles[tileIndex]->getData();

					int* d = (int*)(buf+tileOffset);
					int* s = (int*)(line+lineOffset);

					int size = this->tileWidth*pixelSize;

					while (size >=4)
					{
						*d++ = *s++;
						size -= 4;
					}
					//ZRasterHelper::memcpy(buf+tileOffset, line+lineOffset, this->tileWidth*pixelSize);
                    //memcpy(buf+tileOffset, line+lineOffset, this->tileWidth*pixelSize);
                }
            }
		}
        
        delete [] line;
  
    }
    else // I must be a sub image
    {
        // this is a loop over tiles
        for (int r = 0; r < this->height; r+= this->tileHeight)
        {
            for (int c = 0; c < this->width; c+= this->tileWidth)
            {    
                // our current tile row and col
                int tileCol = c/this->tileWidth;
                int tileRow = r/this->tileHeight;
                
				int tileIndex = tileRow*this->tilesAcross+tileCol;

                // this is the CImageTile we are working on
//                int index = tileCol;
                unsigned char* dest = this->tiles[tileIndex]->getData();

                this->tiles[tileIndex]->setTileRowCol(tileRow, tileCol);
                //memset(dest, 127, this->tileWidth*this->tileHeight*pixelSize);
                
                // R, C the row col in the double size image
                int R = r*2;
                int C = c*2;
                
                // The following calls will moved the file pos so remember it
/*
                file_offset wayBack;
                
                CZImage::TELL(this->fp, &wayBack);
*/
                // first put every other pixel from the upper left tile
                unsigned char* src = NULL;
               
               // the four tiles from the double size image
               unsigned char* t1 = this->doubleSize->getTile(R, C);
 
                if (t1 != NULL)    
                {
                    src = t1;
                    for (int rr = 0; rr < this->tileHeight/2; rr++)
                    {
                        int lineOffset = rr*this->tileWidth*pixelSize;
                        int lineOffsetSrc = rr*2*this->tileWidth*pixelSize;

                        for (int cc = 0; cc < this->tileWidth/2; cc++)
                        {
                            int srcOffset = lineOffsetSrc+cc*2*pixelSize;
                           
                            memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
                        }
                    }
                }

                unsigned char* t2 = this->doubleSize->getTile(R, C+this->tileWidth);
                
                // the the upper right tile
                if (t2 != NULL)
                {
                    src = t2;
                    for (int rr = 0; rr < this->tileHeight/2; rr++)
                    {
                        int lineOffset = rr*this->tileWidth*pixelSize;
                        int lineOffsetSrc = rr*2*this->tileWidth*pixelSize;
                        for (int cc = this->tileWidth/2; cc < this->tileWidth; cc++)
                        {
                           int srcOffset = lineOffsetSrc+(cc-this->tileWidth/2)*2*pixelSize;
                            
                            memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
                        }
                    }
                }
                
                unsigned char* t3 = this->doubleSize->getTile(R+this->tileHeight, C);
                
                // the lower left tile
                if (t3 != NULL)
                {
                    src = t3;
                    for (int rr = this->tileHeight/2; rr < this->tileHeight; rr++)
                    {
                       int lineOffset = rr*this->tileWidth*pixelSize;

                       int lineOffsetSrc = (rr-this->tileHeight/2)*2*this->tileWidth*pixelSize;
                       for (int cc = 0; cc < this->tileWidth/2; cc++)
                        {
                            //int srcOffset = lineOffset+cc*2*pixelSize;
                            int srcOffset = lineOffsetSrc+cc*2*pixelSize;
                            
                            memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
                        }
                    }
                }
                
                unsigned char* t4 = this->doubleSize->getTile(R+this->tileHeight, C+this->tileWidth);
                
                // the lower right tile
                if (t4 != NULL)
                {
                    src = t4;
                    for (int rr = this->tileHeight/2; rr < this->tileHeight; rr++)
                    {
                        int lineOffset = rr*this->tileWidth*pixelSize;
                        int lineOffsetSrc = (rr-this->tileHeight/2)*2*this->tileWidth*pixelSize;
                        for (int cc = this->tileWidth/2; cc < this->tileWidth; cc++)
                        {
                            //int srcOffset = lineOffset+(cc-this->tileWidth/2)*2*pixelSize;
                             int srcOffset = lineOffsetSrc+(cc-this->tileWidth/2)*2*pixelSize;
                          
                            memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
                        }
                    }
                }
                
                // go back
/*   
				CZImage::SEEK(this->fp, &wayBack);

                fwrite(dest, sizeof(unsigned char), this->tileWidth*this->tileHeight*pixelSize, this->fp);
*/
                CZImage::tileCreationCount++;

                if (this->listener != NULL)
                {
                    double p = 100.0*(double)CZImage::tileCreationCount/(double)CZImage::totalTileCount;
                    this->listener->setProgressValue(p);
                }
            }
        }
    }
    
	if (subImage)
    {
        // now write the sub image
        this->halfRes = new CZImage();
        this->halfRes->bpp = this->bpp;
        this->halfRes->bands = this->bands;
        this->halfRes->width = this->width/2;
        this->halfRes->height = this->height/2;
        
        this->halfRes->doubleSize = this;
//        this->halfRes->fp = this->fp;
//        this->halfRes->headerOffset = offset;
//        this->halfRes->dataOffset = offset + this->headerSize();
        this->halfRes->setProgressListener(this->listener);
        this->halfRes->create(imageFile);
    }

    return true;
}

bool CZImage::createHalfResRect(unsigned char* dest, unsigned char* src, int rectHeight, int rectWidth)
{
    unsigned char* tmpd = dest;
    unsigned char* tmps = src;
    int pixelsize = this->pixelSizeBytes();
    for (int R = 0, r = 0; R < rectHeight && r < rectHeight/2; R+= 2, r++)
    {
        tmpd = dest + r*rectWidth/2;
        tmps = src + r*rectWidth;
        for (int C = 0, c = 0; C < rectWidth && c < rectWidth/2; C += 2, c++)
        {
            memcpy(tmpd, tmps, pixelsize);
            tmpd+=pixelsize;
            tmps+=pixelsize;
        }
    }

    return true;
}

/**
 * creates the half res tile. this is a protected function used for creating the pyramid levels
 * the "this" pointer is the halfres image, therefore this->doubleRes must exist
 * @param r image row of half res image
 * @param c image col of half res image
 * @return false for failure
 */

/*
bool CZImage::createHalfResTile(int r, int c)
{
    // This calculation simply makes r and c the upper left corner
    r = (r/this->tileHeight)*this->tileHeight;
    c = (c/this->tileWidth)*this->tileWidth;

    // our current tile row and col
    int tileCol = c/this->tileWidth;
    int tileRow = r/this->tileHeight;
    
    // this is the CImageTile we are working on
    unsigned char* dest = this->getTile(r, c);
    
    // R, C the row col in the double size image
    int R = r*2;
    int C = c*2;

    // the four tiles from the double size image
    unsigned char* t1 = this->doubleSize->getTile(R, C);

    // first put every other pixel from the upper left tile
    unsigned char* src = NULL;

    int pixelSize = this->pixelSizeBytes();
    if (t1 != NULL)
    {
        src = t1;
        for (int rr = 0; rr < this->tileHeight/2; rr++)
        {
            int lineOffset = rr*this->tileWidth*pixelSize;
            int lineOffsetSrc = rr*2*this->tileWidth*pixelSize;

            for (int cc = 0; cc < this->tileWidth/2; cc++)
            {
                int srcOffset = lineOffsetSrc+cc*2*pixelSize;
                
                memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
            }
        }
    }

    unsigned char* t2 = this->doubleSize->getTile(R, C+this->tileWidth);
    // the the upper right tile
    if (t2 != NULL)
    {
        src = t2;
        for (int rr = 0; rr < this->tileHeight/2; rr++)
        {
            int lineOffset = rr*this->tileWidth*pixelSize;
            int lineOffsetSrc = rr*2*this->tileWidth*pixelSize;
            for (int cc = this->tileWidth/2; cc < this->tileWidth; cc++)
            {
                int srcOffset = lineOffsetSrc+(cc-this->tileWidth/2)*2*pixelSize;
                
                memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
            }
        }
    }
    
    unsigned char* t3 = this->doubleSize->getTile(R+this->tileHeight, C);
    // the lower left tile
    if (t3 != NULL)
    {
        src = t3;
        for (int rr = this->tileHeight/2; rr < this->tileHeight; rr++)
        {
            int lineOffset = rr*this->tileWidth*pixelSize;

            int lineOffsetSrc = (rr-this->tileHeight/2)*2*this->tileWidth*pixelSize;
            for (int cc = 0; cc < this->tileWidth/2; cc++)
            {
                //int srcOffset = lineOffset+cc*2*pixelSize;
                int srcOffset = lineOffsetSrc+cc*2*pixelSize;
                
                memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
            }
        }
    }
    
    unsigned char* t4 = this->doubleSize->getTile(R+this->tileHeight, C+this->tileWidth);
    // the lower right tile
    if (t4 != NULL)
    {
        src = t4;
        for (int rr = this->tileHeight/2; rr < this->tileHeight; rr++)
        {
            int lineOffset = rr*this->tileWidth*pixelSize;
            int lineOffsetSrc = (rr-this->tileHeight/2)*2*this->tileWidth*pixelSize;
            for (int cc = this->tileWidth/2; cc < this->tileWidth; cc++)
            {
                //int srcOffset = lineOffset+(cc-this->tileWidth/2)*2*pixelSize;
                    int srcOffset = lineOffsetSrc+(cc-this->tileWidth/2)*2*pixelSize;
                
                memcpy(dest+lineOffset+cc*pixelSize, src+srcOffset, pixelSize);
            }
        }
    }

    return true;
}
*/

/**
 * Gets a tile of imagery into given buf
 * RGB imagery is returned BGR0BGR0...
 * @param r, c any row and column inside the tile
 *
 * @return a volatile memory buf 
 * i.e. get the data and use it or copy it but don't expect 
 * it to be around and by no means delete it!!!
 *
 */
/*
unsigned char* CZImage::getTile(int r, int c)
{
    CImageTile* imageTile;
    
    if (!this->getImageTile(&imageTile, r, c))
        return NULL;

    return imageTile->getData();
}
*/

/**
 * Gets a CImageTile object 
 * @param r, c any row and column inside the tile
 * @return false for failure 
 *
 */
bool CZImage::getImageTile(CImageTile** imageTile, int r, int c)
{
    if (this->tiles == NULL)
        return false;
    
    // sanity check
    if ( (r < 0) || (c < 0) )
        return false;
    
    int tileRow = r/this->tileHeight;
    int tileCol = c/this->tileWidth;
    
    if (tileRow >= this->tilesDown)
        return false;
    
    if (tileCol >= this->tilesAcross)
        return false;
    
    // first check the buffered tiles
    for (int i = 0; i < this->nBufferedTiles; i++)
    {
        CImageTile* testTile = this->tiles[i];
        
        // make sure its there
        if (testTile == NULL)
            continue;
        
        if ((testTile->getTileRow() == tileRow) && (testTile->getTileCol() == tileCol))
        {
            *imageTile = testTile;
            return true;
        }
    }
    
	return false;
    // better buffer the tile
    
}

/**
  * Gets a rectangle of imagery into the supplied buf
  * @param unsigned char* buf the destination buffer
  * RGB imagery is returned BGR0BGR0...
  * @param rectTop, rectHeight, rectLeft, rectWidth the rectangle extents
  * @return false for failure
  *
  */
bool CZImage::getRect(unsigned char* buf, int rectTop, int rectHeight, int rectLeft, int rectWidth)
{
    // pixel size in bytes
    int pixelSize = this->pixelSizeBytes();
    
    CImageTile* imageTile = NULL;

    int leftTile = rectLeft/this->tileWidth;

    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (rectLeft < 0) && (rectLeft%this->tileWidth != 0) )
        leftTile--;

    int rightTile = (rectLeft+rectWidth)/this->tileWidth;
    
    int tileTouchedAcross = rightTile-leftTile+1;
    
    int topTile = rectTop/this->tileHeight;
    
    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (rectTop < 0) && (rectTop%this->tileHeight != 0) )
        topTile--;

    int bottomTile = (rectTop+rectHeight)/this->tileHeight;
    int tileTouchedDown = bottomTile-topTile+1;
    
    // this outer loop is for determining the current tile only
    for (int i = 0, tr = rectTop; i < tileTouchedDown; i++, tr+= this->tileHeight)
    {
        for (int j = 0, tc = rectLeft; j < tileTouchedAcross; j++, tc+= this->tileWidth)
        {
            // the current tile
            this->getImageTile(&imageTile, tr, tc);
        
            // sanity check
            if (imageTile == NULL)
                continue;
            
            // the tile extents
            int tileTop = imageTile->getTileRow() * this->tileHeight;
            int tileLeft = imageTile->getTileCol() * this->tileWidth;
            int tileBottom = tileTop + this->tileHeight;
            int tileRight = tileLeft + this->tileWidth;

            // the source data
            unsigned char* src = imageTile->getData();
            
            int lineOffset;
            int tileLineOffset;
            int rowInTile;
            int rowInRect;
            int colInTile;
            int colInRect;
            
            // now fill the destination buf
            for (int r = max(rectTop, tileTop); r < min(tileBottom, rectTop+rectHeight); r++)
            {
                rowInTile = r-tileTop;
                rowInRect = r-rectTop;
            
                lineOffset = rowInRect*rectWidth*pixelSize;
                tileLineOffset = rowInTile*this->tileWidth*pixelSize;
                
                int cLeft = max(rectLeft, tileLeft);
                int cRight = min(rectLeft+rectWidth, tileRight);
                int w = cRight - cLeft;
            
                colInTile = cLeft-tileLeft;
                colInRect = cLeft-rectLeft;
            
                memcpy(buf+lineOffset+colInRect*pixelSize, src+tileLineOffset+colInTile*pixelSize, pixelSize*w);
            }
        }
    }    

    return true;
}

/**
  * Gets a rectangle of imagery into the supplied buf, however the imagery
  * is resampled to fill destination rect
  * @param unsigned char* buf the destination buffer
  * RGB imagery is returned BGR0BGR0...
  * @param rectTop, rectHeight, rectLeft, rectWidth the source rectangle extents
  * @param destHeight, destWidth the width or height of the destination rect.
  * @return false for failure
  */
bool CZImage::getRect(unsigned char* buf, int rectTop, int rectHeight, int rectLeft, int rectWidth, int destHeight, int destWidth)
{
    double xfactor;
    double yfactor;
    double factor;

    // are we scaling by width 
    xfactor = (double)destWidth/rectWidth;
    yfactor = (double)destHeight/rectHeight;
    
    factor = min(xfactor, yfactor);

    int pixelSize = this->pixelSizeBytes();
    
    // if there is no  halfres or we are at a .5 or greater zoom
    // the reason for the > 0.5 zoom is this ensures we are down-sampling
    // which produces a better result
    if ( (factor > 0.5) || (this->halfRes == NULL) )
    {
        unsigned char *src = new unsigned char[rectHeight*rectWidth*this->pixelSizeBytes()];
        //memset(src, 0, rectHeight*rectWidth*this->pixelSizeBytes());

        if (!this->getRect(src, rectTop, rectHeight, rectLeft, rectWidth))
        {
            delete [] src;
            return false;
        }
        
        double xStep = (double)rectWidth/(double)destWidth;
        double yStep = (double)rectHeight/(double)destHeight;
        double x = 0;
        double y = 0;
		int C;
		int c;

		if (this->bands == 1 && this->bpp == 8)
		{
			for (int r = 0; r < destHeight; r++)
			{
				int R = (int)y;
				int lineOffsetSrc = R*rectWidth;
				int lineOffsetDest = r*destWidth;
				x = 0;

				for (c = 0; c < destWidth; c++)
				{
					C = (int)x;
					buf[lineOffsetDest+c] = src[lineOffsetSrc+C];
					x += xStep;
				}
	            
				y+= yStep;
			}
		}
		else
		{
			for (int r = 0; r < destHeight; r++)
			{
				int R = (int)y;
				int lineOffsetSrc = R*rectWidth*pixelSize;
				int lineOffsetDest = r*destWidth*pixelSize;
				x = 0;

				for (c = 0; c < destWidth; c++)
				{
					C = (int)x;
					::memcpy(buf+lineOffsetDest+c*pixelSize, src+lineOffsetSrc+C*pixelSize, pixelSize);
					x += xStep;
				}
	            
				y+= yStep;
			}
		}

        delete [] src;
        return true;
    }
        
    return this->halfRes->getRect(buf, rectTop/2, rectHeight/2, rectLeft/2, rectWidth/2, destHeight, destWidth);
}

bool CZImage::getLine(unsigned char* buf, int row)
{
    return true;
}

/*bool CZImage::getPixel(unsigned char* buf, int r, int c)
{
    // must actually do something here !
	//return this->getRect(buf, r, 1, c, 1);

	*buf = *(this->scanline(r)+c*this->pixelSizeBytes());

	return true;
}*/

bool CZImage::setPixel(unsigned char* buf, int r, int c)
{
    return true;
}

/** 
 * @return the Bits per pixel
 */
int CZImage::getBpp() const
{
    return this->bpp;
}

/** 
 * @return the number of bands (i.e RGB = 3)
 */
int CZImage::getBands() const
{
    return this->bands;
}

/** 
 * @return the image width
 */
int CZImage::getWidth() const
{
    return this->width;
}

/** 
 * @return the image height
 */
int CZImage::getHeight() const
{
    return this->height;
}

/** 
 * @return the tile width
 */
int CZImage::getTileWidth() const
{
 return this->tileWidth;
}

/** 
 * @return the tile height
 */
int CZImage::getTileHeight() const
{
 return this->tileHeight;
}

/** 
 * get a tile of imagery zoomed (resampled) according to the zoomUp factor
 * @param buf the destination buffer for the imagery.
 * @param R, C the row, col in the larger (virtual) image system
 * @param zoomUp the zoomfactor
 * @return false if failed
 *
 */
bool CZImage::getTileZoomUp(CImageBuffer &dest, int R, int C, int zoomUp)
{
    // determine the rect in the original image
    R = (R/this->tileHeight)*this->tileHeight;
    C = (C/this->tileWidth)*this->tileWidth;

    int r1 = R/zoomUp;
    int c1 = C/zoomUp;

    unsigned char* src = this->getTile(r1, c1);
    
    if (src == NULL)
        return false;

	int pixelSize;
    
    if (this->bands == 3)
        pixelSize = 4*this->bpp/8;
    else if (this->bands == 4)
        pixelSize = 4*this->bpp/8;
	else
        pixelSize = this->bpp/8;

    int rStart = r1 % this->tileHeight;
    int rEnd = rStart + (this->tileHeight/zoomUp);
    int cStart = c1 % this->tileWidth;
    int cEnd = cStart + (this->tileWidth/zoomUp);

    int nBytesPerLine = this->tileWidth*pixelSize;
    int destLineOffset = 0;
    int srcLineOffset = rStart*nBytesPerLine;

	unsigned char* tmpBuf = NULL;

	if (this->bpp == 8)
		tmpBuf = dest.getPixUC();
	else if (this->bpp == 16)
		tmpBuf = (unsigned char*)dest.getPixUS();
	else if (this->bpp == 32)
		tmpBuf =  (unsigned char*)dest.getPixF();

	if (!tmpBuf)
		return false;

    for (int r = rStart, destR = 0; r < rEnd; r++, destR += zoomUp)
    {
        for (int c = cStart, destC = 0; c < cEnd; c++, destC += zoomUp)
        {
            destLineOffset = destR*nBytesPerLine;
            for (int i = 0; i < zoomUp; i++)
            {
                for (int j = 0; j < zoomUp; j++)
                {
                    memcpy(tmpBuf+destLineOffset+(destC+j)*pixelSize, src+srcLineOffset+c*pixelSize, pixelSize);
                }
                destLineOffset += nBytesPerLine;
            }
        }

        srcLineOffset += nBytesPerLine;
    }

 return true;
}

/** 
 * get a tile of imagery zoomed (resampled) according to the zoomDown factor
 * @param buf the destination buffer for the imagery.
 * @param R, C the row, col in the smaller (virtual) image system
 * @param zoomDown the zoomfactor
 * @return false if failed
 *
 */
bool CZImage::getTileZoomDown(CImageBuffer &dest, int R, int C, int zoomDown)
{
    if (zoomDown <= 1)
        return false;

    // determine the rect in the original image
    // This calculation simply makes R, C the upper left corner
    R = (R/this->tileHeight)*this->tileHeight;
    C = (C/this->tileWidth)*this->tileWidth;

    int pixelSize = this->pixelSizeBytes();
  
	unsigned char* tmpBuf = NULL;

	if (this->bpp == 8)
		tmpBuf = dest.getPixUC();
	else if (this->bpp == 16)
		tmpBuf = (unsigned char*)dest.getPixUS();
	else if (this->bpp == 32)
		tmpBuf =  (unsigned char*)dest.getPixF();

	if (!tmpBuf)
		return false;


    // we have this pyramid, we must use it.... maybe
    if ( (this->halfRes == NULL) ) 
    {
        R*=zoomDown;
        C*=zoomDown;
        for (int ty = 0, r = R; ty < zoomDown; ty++, r += this->tileHeight)
        {
            for (int tx = 0, c = C; tx < zoomDown; tx++, c += this->tileWidth)
            {
                unsigned char* tile = this->getTile(r, c);
                if (!tile)
                    continue;

                unsigned char* pixel = NULL;                
                unsigned char* dest = NULL;
                int starty = ty*(this->tileHeight/zoomDown);
                int startx = tx*(this->tileWidth/zoomDown);
                for (int ii = starty, i = 0; i < this->tileHeight; ii++, i+= zoomDown)
                {
                    for (int jj = startx, j = 0; j < this->tileWidth; jj++, j+= zoomDown)
                    {
                        pixel = tile+(i*this->tileWidth + j)*this->pixelSizeBytes();
                        dest = tmpBuf+(ii*this->tileWidth + jj)*this->pixelSizeBytes();
                        memcpy(dest, pixel, this->pixelSizeBytes());
                    }
                }
            }
        }
    }
    else
    {
        int Z = zoomDown/2;
        if (Z == 1)
        {
            unsigned char* tmp = this->halfRes->getTile(R, C);
            if (tmp == NULL)
                return false;

            memcpy(tmpBuf, tmp, this->tileHeight*this->tileWidth*pixelSize);
        }
        else
        {
            this->halfRes->getTileZoomDown(dest, R, C, Z);
        }
    }

    return true;
}

/**
  * @return a char* containing the original file name
  */
char* CZImage::getFilename()
{
    return this->filename;
}

bool CZImage::convolveFloat(CZImage* src, CKernel* kernel)
{
    if (this->bpp != 32)
        return false;

    // loop over all tiles
    int rectWidth = this->tileWidth + (kernel->size/2)*2;
    int rectHeight = this->tileHeight + (kernel->size/2)*2;

    unsigned char* rect = new unsigned char[rectWidth*rectHeight*src->pixelSizeBytes()];
    bool border;
    for (int R = 0; R < this->height; R+= this->tileHeight)
    {
        for (int C = 0; C < this->width; C += this->tileWidth)
        {
            float* dest = (float*)this->getTile(R, C);

            int rectLeft = C - kernel->size/2;
            int rectTop = R - kernel->size/2;

            src->getRect(rect, rectTop, rectHeight, rectLeft, rectWidth);

            for (int r = R; r < R+this->tileHeight; r++)
            {
                int rowInTile = r % this->tileHeight;
                int rowInRect = rowInTile + kernel->size/2;

                int lineOffsetTile = rowInTile*this->tileWidth;
                int lineOffsetRect = rowInRect*rectWidth;

                for (int c = C; c < C + this->tileWidth; c++)
                {
                    int colInTile = c % this->tileWidth;
                    int colInRect = colInTile + kernel->size/2;

                    border = (r < kernel->size/2) || (r >= this->height-kernel->size/2) || (c < kernel->size/2) || (c >= this->width - kernel->size/2);

                    // take care of border colums
                    if (border)
                    {
                        dest[lineOffsetTile+colInTile] = (float)rect[lineOffsetRect + colInRect];
                        continue;
                    }

                    // loop over kernel
                    float pixel = 0;
                    int innerRowInRect = rowInRect - kernel->size/2;
			        for(int rr = r - kernel->size/2, kr = 0; rr <= r + kernel->size/2; rr++, kr++) 
			        {
                        lineOffsetRect = innerRowInRect*rectWidth;

                        int innerColInRect = colInRect - kernel->size/2;
				        for(int cc = c - kernel->size/2, kc = 0; cc <= c + kernel->size/2; cc++, kc++) 
				        {
                            pixel += (float)rect[lineOffsetRect + innerColInRect]*kernel->data[kr][kc];
                            innerColInRect++;
				        }

                        innerRowInRect++;
			        }

                    dest[lineOffsetTile+colInTile] = pixel;

                } //for (c = C; c < C + this->tileWidth; c++)
            } //for (int r = R; r < R+this->tileHeight; r++)

            // save the tile
            this->setTile((unsigned char*)dest, R, C);

        } //for (int C = 0; C < this->width; C += this->tileWidth)

    } //for (int R = 0; R < this->height; R+= this->tileHeight)

    delete [] rect;

    return true;
}

bool CZImage::convolve(CZImage* src, CKernel* kernel)
{
    if (this->bpp == 32)
        return this->convolveFloat(src, kernel);

    if ( (this->bpp == 8) && (this->getBands() == 1) )
    {
        return this->convolve8Bit(src, kernel);
    }

    return false;
}

bool CZImage::convolve8Bit(CZImage* src, CKernel* kernel)
{
    this->openNew(src->getWidth(), src->getHeight(), src->getBpp(), src->getBands());

    // loop over all tiles
    int rectWidth = this->tileWidth + (kernel->size/2)*2;
    int rectHeight = this->tileHeight + (kernel->size/2)*2;

    unsigned char* rect = new unsigned char[rectWidth*rectHeight*src->pixelSizeBytes()];
    bool border;
    for (int R = 0; R < this->height; R+= this->tileHeight)
    {
        for (int C = 0; C < this->width; C += this->tileWidth)
        {
            unsigned char* dest = (unsigned char*)this->getTile(R, C);

            int rectLeft = C - kernel->size/2;
            int rectTop = R - kernel->size/2;

            src->getRect(rect, rectTop, rectHeight, rectLeft, rectWidth);

            for (int r = R; r < R+this->tileHeight; r++)
            {
                int rowInTile = r % this->tileHeight;
                int rowInRect = rowInTile + kernel->size/2;

                int lineOffsetTile = rowInTile*this->tileWidth;
                int lineOffsetRect = rowInRect*rectWidth;

                for (int c = C; c < C + this->tileWidth; c++)
                {
                    int colInTile = c % this->tileWidth;
                    int colInRect = colInTile + kernel->size/2;

                    border = (r < kernel->size/2) || (r >= this->height-kernel->size/2) || (c < kernel->size/2) || (c >= this->width - kernel->size/2);

                    // take care of border colums
                    if (border)
                    {
                        dest[lineOffsetTile+colInTile] = rect[lineOffsetRect + colInRect];
                        continue;
                    }

                    // loop over kernel
                    float pixel = 0;
                    int innerRowInRect = rowInRect - kernel->size/2;
			        for(int rr = r - kernel->size/2, kr = 0; rr <= r + kernel->size/2; rr++, kr++) 
			        {
                        lineOffsetRect = innerRowInRect*rectWidth;

                        int innerColInRect = colInRect - kernel->size/2;
				        for(int cc = c - kernel->size/2, kc = 0; cc <= c + kernel->size/2; cc++, kc++) 
				        {
                            float val = rect[lineOffsetRect + innerColInRect];
                            pixel += val*kernel->data[kr][kc];
                            innerColInRect++;
				        }

                        innerRowInRect++;
			        }

                    // clamp pixel
                    if (pixel > 255)
                        pixel = 255;

                    dest[lineOffsetTile+colInTile] = (unsigned char)(pixel + 0.5);

                } //for (c = C; c < C + this->tileWidth; c++)
            } //for (int r = R; r < R+this->tileHeight; r++)

            // save the tile
            this->setTile((unsigned char*)dest, R, C);

        } //for (int C = 0; C < this->width; C += this->tileWidth)

    } //for (int R = 0; R < this->height; R+= this->tileHeight)

    delete [] rect;

    return true;

}

bool CZImage::floatToRGB(CZImage* src)
{
    // sanity checks
    if (src->bpp != 32)
        return false;

    if (this->bands != 3)
        return false;

    if (this->bpp != 8)
        return false;

    if ( (src->width != this->width) ||
         (src->height != this->height) )
         return false;

    // loop over all tiles in image
    // find the overall max and min in the float image
    float MAXR = -99999;
    float MINR = 99999;
    float MAXG = -99999;
    float MING = 99999;
    float MAXB = -99999;
    float MINB = 99999;

    for (int i = 0; i < src->tilesDown; i++)
    {
        for (int j = 0; j < src->tilesAcross; j++)
        {
            // the source buffer 
            float* srcTile = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);

            for (int r = 0; r < src->tileHeight; r++)
            {
                int lineOffset = r*src->tileWidth*4;
                for (int c = 0; c < src->tileWidth; c++)
                {
                    float pixelB = srcTile[lineOffset+ 4*c + 0];
                    float pixelG = srcTile[lineOffset+ 4*c + 1];
                    float pixelR = srcTile[lineOffset+ 4*c + 2];

                    if (pixelB > MAXB)
                        MAXB = pixelB;
                    if (pixelB < MINB)
                        MINB = pixelB;

                    if (pixelG > MAXG)
                        MAXG = pixelG;
                    if (pixelG < MING)
                        MING = pixelG;

                    if (pixelR > MAXR)
                        MAXR = pixelR;
                    if (pixelR < MINR)
                        MINR = pixelR;

                }
            }
        }
    }

    float rangeB = MAXB - MINB;
    float rangeG = MAXG - MING;
    float rangeR = MAXR - MINR;

    int destPixelSize = this->pixelSizeBytes();

    //unsigned char* destination = new unsigned char[src->tileHeight*src->tileWidth*destPixelSize];
 
    if (this->bpp == 8)
    {
        for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer 
                float* srcTile = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);
                unsigned char* dest = this->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth*4;
                    //int destLineOffset = r*this->tileWidth*4;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        float pixelB = srcTile[lineOffset+ 4*c + 0];
                        float pixelG = srcTile[lineOffset+ 4*c + 1];
                        float pixelR = srcTile[lineOffset+ 4*c + 2];
    
                        dest[lineOffset+4*c+0] = (unsigned char)(255.0f*((pixelB - MINB)/rangeB)+0.5);
                        dest[lineOffset+4*c+1] = (unsigned char)(255.0f*((pixelG - MING)/rangeG)+0.5);
                        dest[lineOffset+4*c+2] = (unsigned char)(255.0f*((pixelR - MINR)/rangeR)+0.5);
                    }
                }

                this->setTile(dest, i*src->tileHeight, j*src->tileWidth);
            }
        }
    }
    else
    {
        return false;
    }

    return true;

}


/**
 * converts a floating point image (src) to a grey scale image (this)
 * @param src the input grey scale image
 * @return false for failure
 *
 */
bool CZImage::floatToGrey(CZImage* src)
{
    // sanity checks
    if (src->bpp != 32)
        return false;

    if (this->bands != 1)
        return false;

    if ( (src->width != this->width) ||
         (src->height != this->height) )
         return false;

    // loop over all tiles in image
    // find the overall max and min in the float image
    float MAX = -99999;
    float MIN = 99999;
    for (int i = 0; i < src->tilesDown; i++)
    {
        for (int j = 0; j < src->tilesAcross; j++)
        {
            // the source buffer 
            float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);

            for (int r = 0; r < src->tileHeight; r++)
            {
                int lineOffset = r*src->tileWidth;
                for (int c = 0; c < src->tileWidth; c++)
                {
                    float pixel = FLOAT[lineOffset+c];
                    if (pixel > MAX)
                        MAX = pixel;
                    if (pixel < MIN && pixel>=0)
                        MIN = pixel;
                    if (pixel < 0)
                    {
                        int hmm = 0;
                    }
                }
            }
        }
    }

    float range = MAX - MIN;
    int destPixelSize = this->pixelSizeBytes();

    unsigned char* destination = new unsigned char[src->tileHeight*src->tileWidth*destPixelSize];
    if (this->bpp == 8)
    {
        for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer 
                float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);
                //unsigned char* buf = dest->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth;
                    int destLineOffset = r*this->tileWidth;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        float pixel = FLOAT[lineOffset+c];
    
						if (pixel<0) pixel = 0;

                        destination[destLineOffset+c] = (unsigned char)(255.0f*((pixel - MIN)/range)+0.5);
                    }
                }

                this->setTile(destination, i*src->tileHeight, j*src->tileWidth);
            }
        }
    }
    else if (this->bpp == 16)
    {
        for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer 
                float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);
                unsigned short* buf = (unsigned short*)this->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth;
                    int destLineOffset = r*this->tileWidth;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        float pixel = FLOAT[lineOffset+c];
    
						if (pixel<0) pixel = 0;

                        buf[destLineOffset+c] = (unsigned char)(65535.0f*((pixel - MIN)/range)+0.5);
                    }
                }

                this->setTile((unsigned char*)buf, i*src->tileHeight, j*src->tileWidth);

            }
        }
    }
    else
    {
        delete [] destination; // @@@
        return false;
    }

    delete [] destination;
    return true;
}

/**
 * transforms a hsv pixel to rgb
 * 
 * @param rgb[] the output rgb pixel
 * @param hsv[] the input hsv pixel
 */
void CZImage::HSVtoRGB(unsigned char rgb[3], float hsv[3])
{
    float r, b, g;
    
    // hue undefined / no saturation
    if ( ( hsv[0] < 0) || (hsv[1] == 0.0f) )
    {
        rgb[0] = (unsigned char)(hsv[2]*255);
        rgb[1] = (unsigned char)(hsv[2]*255);
        rgb[2] = (unsigned char)(hsv[2]*255);
        
        return;
    }
    
    int Hi = (int)(floor(hsv[0]/60.0f));
    
    float f = hsv[0]/60.f - (float)Hi;
    
    float p = hsv[2] * (1.f - hsv[1]);
    float q = hsv[2] * (1.f - (hsv[1]*f));
    float t = hsv[2] * (1.f - (hsv[1] * (1.f-f)));
    
    if (Hi == 0)
    {
        r = hsv[2];
        g = t;
        b = p;
    }
    else if (Hi == 1)
    {
        r = q;
        g = hsv[2];
        b = p;
    }
    else if (Hi == 2)
    {
        r = p;
        g = hsv[2];
        b = t;
    }
    else if (Hi == 3)
    {
        r = p;
        g = q;
        b = hsv[2];
    }
    else if (Hi == 4)
    {
        r = t;
        g = p;
        b = hsv[2];
    }
    else if (Hi == 5)
    {
        r = hsv[2];
        g = p;
        b = q;
    }

    rgb[0] = (unsigned char)(r*255.f + 0.5);
    rgb[1] = (unsigned char)(g*255.f + 0.5);
    rgb[2] = (unsigned char)(b*255.f + 0.5);
    
}    
        
/**
 * transforms an rbg pixel to hsv space
 * Hue is 0 to 360
 * @param hsv[] the output hsv values
 * @param rgb[] the input rgb values (stored as bgr)
 */
void CZImage::RGBtoHSV(float hsv[3], unsigned char rgb[3])
{
    float R = rgb[0]/255.0f;
    float G = rgb[1]/255.0f;
    float B = rgb[2]/255.0f;

    float themin = min(R, min(G, B));
    float themax = max(R, max(G, B));

    float delta = (themax - themin);

    // Hue
    if (delta > 0)
    {
        if (R == themax)
            hsv[0] = 60*(G-B)/delta;
        else if (G == themax)
            hsv[0] = 120 + 60*(B-R)/delta;
        else if (B == themax)
            hsv[0] = 240 + 60*(R-G)/delta;
        
        if (hsv[0] < 0)
            hsv[0] += 360;
 
    }
    else // hue is undefined
    {
        hsv[0] = -1.0f;
    }
    
    // Saturation
    hsv[1] = delta/themax;

    // Value
    hsv[2] = themax;
}

void CZImage::histogram(long* grey)
{
	::memset(grey, 0, 256*sizeof(long));

    int pixSize = this->pixelSizeBytes();
    int w = this->getWidth();
    int h = this->getHeight();
    int tileSize = pixSize*this->tileWidth*this->tileHeight;

    unsigned char* buf = new unsigned char[w*h*pixSize];

    this->getRect(buf, 0, h, 0, w);

    if (pixSize == 1)
    {
        for (int i = 0; i < w*h; i++)
        {
            grey[buf[i]]++;
        }
    }
    else
    {
        for (int i = 0; i < pixSize*w*h; i+=pixSize)
        {
            grey[buf[i]]++;
        }
    }

    delete [] buf;

    /*
    for (int i = 0; i < this->tilesDown; i++)
    {
        for (int j = 0; j < this->tilesAcross; j++)
        {
            unsigned char* tile = this->getTile(i, j);
            unsigned char* tmp = tile;

            if (pixSize == 1)
            {
                for (tmp = tile; tmp < tile+tileSize; tmp++)
                    grey[*tmp]++;
            }
            else
            {
                for (tmp = tile; tmp < tile+tileSize; tmp++)
                {
                    int pix = (int)((double)(tmp[0]+tmp[1]+tmp[2])/3.0 + 0.5);
                    grey[pix]++;
                }
            }
        }
    }
    */
}

/*
unsigned char* CZImage::getBits()
{
    if (this->bits == NULL)
    {
        this->bits = new unsigned char[this->getWidth()*this->getHeight()*this->pixelSizeBytes()];

        this->getRect(this->bits, 0, this->getHeight(), 0, this->getWidth());
    }

    return this->bits;
}
*/

bool CZImage::rotateLeft(CZImage* dest)
{
    int destWidth = this->getHeight();
    int destHeight = this->getWidth();
    int bpp = this->getBpp();
    int bands = this->getBands();
    int pixelsize = this->pixelSizeBytes();

    dest->openNew(destWidth, destHeight, bpp, bands);

    unsigned char* flip = new unsigned char[destHeight*pixelsize];

    for (int r = 0; r < this->getHeight(); r++)
    {
        unsigned char* line = this->scanline(r);
        unsigned char* tmp = flip+pixelsize*(destHeight-1);
        for (int i = 0; i < destHeight; i++)
        {
            ::memcpy(tmp, line, pixelsize);
            line += pixelsize;
            tmp -= pixelsize;
        }

        dest->setRect(flip, 0, destHeight, r, 1);
    }

    delete [] flip;

    if (this->halfRes)
    {
        for (int i = 0; i < dest->tilesDown; i++)
        {
            for (int j = 0; j < dest->tilesAcross; j++)
            {
                dest->halfRes->createHalfResTile(i*dest->tileHeight, j*dest->tileWidth);
                unsigned char* buf = dest->halfRes->getTile(i*dest->tileHeight, j*dest->tileWidth);
                dest->halfRes->setTile(buf, i*dest->tileHeight, j*dest->tileWidth);
            }
        }
    }

    return true;
}


bool CZImage::rotateRight(CZImage* dest)
{
    int destWidth = this->getHeight();
    int destHeight = this->getWidth();
    int bpp = this->getBpp();
    int bands = this->getBands();
    int pixelsize = this->pixelSizeBytes();

    dest->openNew(destWidth, destHeight, bpp, bands);

    for (int r = 0; r < this->getHeight(); r++)
    {
        unsigned char* line = this->scanline(r);

        dest->setRect(line, 0, destHeight, this->getHeight()-r-1, 1);
    }

    if (this->halfRes)
    {
        for (int i = 0; i < dest->tilesDown; i++)
        {
            for (int j = 0; j < dest->tilesAcross; j++)
            {
                dest->halfRes->createHalfResTile(i*dest->tileHeight, j*dest->tileWidth);
                unsigned char* buf = dest->halfRes->getTile(i*dest->tileHeight, j*dest->tileWidth);
                dest->halfRes->setTile(buf, i*dest->tileHeight, j*dest->tileWidth);
            }
        }
    }

    return true;
}

bool CZImage::rotate180(CZImage* dest)
{
    int destWidth = this->getWidth();
    int destHeight = this->getHeight();
    int bpp = this->getBpp();
    int bands = this->getBands();
    int pixelsize = this->pixelSizeBytes();

    dest->openNew(destWidth, destHeight, bpp, bands);

    unsigned char* flip = new unsigned char[destWidth*pixelsize];

    for (int r = 0; r < this->getHeight(); r++)
    {
        unsigned char* line = this->scanline(r);
        unsigned char* tmp = flip+pixelsize*(destWidth-1);
        for (int i = 0; i < destWidth; i++)
        {
            ::memcpy(tmp, line, pixelsize);
            line += pixelsize;
            tmp -= pixelsize;
        }

        dest->setRect(flip, destHeight-r-1, 1, 0, destWidth);
    }

    delete [] flip;

    if (this->halfRes)
    {
        for (int i = 0; i < dest->tilesDown; i++)
        {
            for (int j = 0; j < dest->tilesAcross; j++)
            {
                dest->halfRes->createHalfResTile(i*dest->tileHeight, j*dest->tileWidth);
                unsigned char* buf = dest->halfRes->getTile(i*dest->tileHeight, j*dest->tileWidth);
                dest->halfRes->setTile(buf, i*dest->tileHeight, j*dest->tileWidth);
            }
        }
    }

    return true;
}


bool CZImage::mirror(CZImage* dest)
{
    int destWidth = this->getWidth();
    int destHeight = this->getHeight();
    int bpp = this->getBpp();
    int bands = this->getBands();
    int pixelsize = this->pixelSizeBytes();

    dest->openNew(destWidth, destHeight, bpp, bands);

    unsigned char* flip = new unsigned char[destWidth*pixelsize];

    for (int r = 0; r < this->getHeight(); r++)
    {
        unsigned char* line = this->scanline(r);
        unsigned char* tmp = flip+pixelsize*(destWidth-1);
        for (int i = 0; i < destWidth; i++)
        {
            ::memcpy(tmp, line, pixelsize);
            line += pixelsize;
            tmp -= pixelsize;
        }

        dest->setRect(flip, r, 1, 0, destWidth);
    }

    delete [] flip;

    if (this->halfRes)
    {
        for (int i = 0; i < dest->tilesDown; i++)
        {
            for (int j = 0; j < dest->tilesAcross; j++)
            {
                dest->halfRes->createHalfResTile(i*dest->tileHeight, j*dest->tileWidth);
                unsigned char* buf = dest->halfRes->getTile(i*dest->tileHeight, j*dest->tileWidth);
                dest->halfRes->setTile(buf, i*dest->tileHeight, j*dest->tileWidth);
            }
        }
    }

    return true;
}
/**
 * makes a copy of 'this' image brightened by the factor
 * note this allocates memory that will have to be deleted!!!
 * @param factor the brightness multiplier
 *
 */
CZImage* CZImage::brighten(double factor)
{
    CZImage* image = new CZImage;

    image->openNew(this->getWidth(), this->getHeight(), this->getBpp(), this->getBands());

    // set up the look up table
    char lut[256];
    for (int i = 0; i < 256; i++)
    {
        int pixel = (int)(i*factor+0.5);
        pixel = min(max(0, pixel), 255);
        lut[i] = pixel;
    }

    unsigned char* src;
    unsigned char* dest;

    for (int r = 0; r < this->height; r+= this->tileHeight)
    {
        for (int c = 0; c < this->width; c+= this->tileWidth)
        {
            src = this->getTile(r, c);
            dest = image->getTile(r, c);

            if (this->getBands() == 1)
            {
                for (int i = 0; i < tileWidth*tileHeight; i++)
                    dest[i] = lut[src[i]];
            }
            else
            {
                for (int i = 0; i < 4*tileWidth*tileHeight; i+=4)
                {
                    dest[i] = lut[src[i]];
                    dest[i+1] = lut[src[i+1]];
                    dest[i+2] = lut[src[i+2]];
                }
            }

            image->setTile(dest, r, c);
        }
    }

    return image;
}

bool CZImage::resample(CZImage* dest, int width, int height)
{
    unsigned char* buf = new unsigned char[width*height*this->pixelSizeBytes()];
    
    if(!this->getRect(buf, 0, this->getHeight(), 0, this->getWidth(), height, width))
        return false;
    
    if(!dest->openNew(width, height, this->getBpp(), this->getBands()))
        return false;

    if(!dest->setRect(buf, 0, height, 0, width))
        return false;

    if(buf)
        delete [] buf;
    
    return true;
}