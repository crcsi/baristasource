/** 
 * @class CGDALFile
 * concrete GDALFile image file class
 * a wrapper around the gdal library
 * 
 * @author Jochen Willneff
 * @version 1.0 
 * @date 11-may-2006
 */

#include "gdal_priv.h"
#include <ogr_spatialref.h>

#include "GDALFile.h"
#include "ImageTile.h"
#include "LookupTable.h"
#include "ImgBuf.h"
#include "SystemUtilities.h"
#include "histogram.h"
#include "tiffiop.h"

extern "C"
{
    #include "jpeglib.h"
    #include "jerror.h"
#ifndef WIN32
	#include "jpegint.h"
#endif
}



// forward declarations 
static int modifiedJPEGPreDecode(TIFF* tif, tsample_t s);

TIFFPreMethod  original_tif_predecode = NULL;


// define error handler, so we always initialize the buffer with 0 in case of an error
#ifndef READ_ERROR
#define READ_ERROR(buffer,sizeOfBuffer) {memset(buffer, 0, sizeOfBuffer);\
					return false;}
#endif



CGDALFile::CGDALFile() : CImageFile(eGDALFile,"unkown"),needsModifiedJPEGLib(false),tif(NULL),
						bHeaderRead(false),linedata(NULL),bline(NULL),rline(NULL),gline(NULL),
						nline(NULL),scanLineBuffer(NULL),gdal(NULL),
						tiles(NULL),tilesAcross(0),tilesDown(0),
						tileByteSize(0),tileWidth(0),tileHeight(0),nBufferedTiles(0),tileBuffer(NULL),
						lastBufferedTile(-1),sizeofpixel(0),NoData1(DEM_NOHEIGHT),NoData2(DEM_NOHEIGHT),
						NoData3(DEM_NOHEIGHT),NoData4(DEM_NOHEIGHT),bandInterleave(false)
{
}


CGDALFile::CGDALFile(const char* filename) : CImageFile(eGDALFile,filename),needsModifiedJPEGLib(false),tif(NULL),	
					bHeaderRead(false),linedata(NULL),bline(NULL),rline(NULL),gline(NULL),
					nline(NULL),scanLineBuffer(NULL),gdal(NULL),
					tiles(NULL),tilesAcross(0),tilesDown(0),
					tileByteSize(0),tileWidth(0),tileHeight(0),nBufferedTiles(0),tileBuffer(NULL),
					lastBufferedTile(-1),sizeofpixel(0),NoData1(DEM_NOHEIGHT),NoData2(DEM_NOHEIGHT),
					NoData3(DEM_NOHEIGHT),NoData4(DEM_NOHEIGHT),bandInterleave(false)
{
}

CGDALFile::CGDALFile(const CGDALFile& src) : CImageFile(src),needsModifiedJPEGLib(src.needsModifiedJPEGLib),
					tif(NULL),bHeaderRead(src.bHeaderRead),linedata(NULL),bline(NULL),rline(NULL),gline(NULL),
					nline(NULL),scanLineBuffer(NULL),gdal(NULL),
					tiles(NULL),tilesAcross(src.tilesAcross),tilesDown(src.tilesDown),
					tileByteSize(0),tileWidth(src.tileWidth),tileHeight(src.tileHeight),nBufferedTiles(0),tileBuffer(NULL),
					lastBufferedTile(-1),sizeofpixel(src.sizeofpixel),NoData1(src.NoData1),NoData2(src.NoData2),
					NoData3(src.NoData3),NoData4(src.NoData4),channelExport(src.channelExport),
					compressionExport(src.compressionExport),compressionQualityExport(src.compressionQualityExport),
					exportGeoData(src.exportGeoData),exportSeperatFile(src.exportSeperatFile),fileNameGeoData(src.fileNameGeoData),
					bandInterleave(src.bandInterleave)

{
	if (this->bHeaderRead)
	{
		// create the tile buffer
		this->nBufferedTiles = this->tilesAcross;

		if (!this->tiles) 
		{
			this->tiles = new CImageTile*[this->nBufferedTiles];
			for (int i = 0; i < this->tilesAcross; i++)
			{
				this->tiles[i] = NULL;
			}
		}
		else
		{
			for (int i = 0; i < this->tilesAcross; i++)
    		{
				if (this->tiles[i]) delete this->tiles[i];
				this->tiles[i] = NULL;
			}
		}
	}


}

CGDALFile::~CGDALFile()
{
    if(this->scanLineBuffer != NULL)
	{
        delete [] this->scanLineBuffer;
		this->scanLineBuffer = NULL;
	}
    if(this->linedata != NULL)
	{
        delete [] this->linedata;
		this->linedata = NULL;
	}
    if(this->rline != NULL)
	{
        delete [] this->rline;
		this->rline = NULL;
	}
    if(this->bline != NULL)
	{
        delete [] this->bline;
		this->bline = NULL;
	}
    if(this->gline != NULL)
	{
        delete [] this->gline;
		this->gline = NULL;
	}
    if(this->nline != NULL)
	{
        delete [] this->nline;
		this->nline = NULL;
	}

	this->closeFile();
	
	if (this->tiles)
	{
			
		for (int i = 0; i < this->tilesAcross; i++)
		{
			if (this->tiles[i] != NULL)
				delete this->tiles[i];
		}

		delete [] this->tiles;
		this->tiles = 0;
	}

    if(this->tileBuffer != NULL)
	{
        delete [] this->tileBuffer;
		this->tileBuffer = 0;
	}	

}





bool CGDALFile::closeFile()
{
    if(this->gdal != NULL)
	{
		GDALClose(gdal);
		this->gdal = NULL;
	}

	if (this->tif)
	{
		TIFFClose(this->tif);
		this->tif = NULL;

		// reset the global function pointer
		original_tif_predecode = NULL;

	}

	return true;
}

/**
 *  writes a tiff file from a CBaseImage
 *
 * @param fileName the output file name
 * @param image the src image
 *
 */
bool CGDALFile::write(const char* filename, CBaseImage* image)
{

	if (filename == NULL || image == NULL)
		return false;

	strcpy(this->filename, filename);	
	
	CFileName fn(this->filename);
	char **options = NULL;		// options to create the file


	// no filename set
	if (fn.CompareNoCase("unknown"))
		return false;

	// the basic informations are not set
	if (this->nCols == -1 || this->nRows == -1 || this->nBands == -1 || this->bpp == -1)
		return false;

	if (this->bpp == 8) this->sizeofpixel = GDT_Byte;
	else if (this->bpp == 16) this->sizeofpixel = GDT_UInt16;
	else if (this->bpp == 32) this->sizeofpixel =  GDT_Float32;

	CCharString driverName;
	CFileName ext(fn.GetFileExt());	
	bool tiffIsTemp;


	// get the driver naame and set options for every format
	if (ext.CompareNoCase("tif"))
	{
		tiffIsTemp= false;
		if (this->tiled)
		{
			options = CSLSetNameValue( options, "TILED", "YES" );
			options = CSLSetNameValue( options, "BLOCKXSIZE", "512" );
			options = CSLSetNameValue( options, "BLOCKYSIZE", "512" );

		}

		
		long sizeOfImage = this->nRows * this->nCols * this->nBands * this->sizeofpixel;
		
		if (sizeOfImage > 4000000000) // actually 2^32-1 but we also need space for the header
			options = CSLSetNameValue( options, "BIGTIFF", "YES" );
		
		options = CSLSetNameValue( options, "COMPRESS", this->compressionExport.GetChar() );
		options = CSLSetNameValue( options, "PROFILE", "GDALGeoTIFF" );


		if (this->compressionExport.CompareNoCase("Jpeg"))
		{
			char quality[4];
			sprintf(quality,"%d",this->compressionQualityExport);
			options = CSLSetNameValue( options, "JPEG_QUALITY", quality);
		}
		
		//PHOTOMETRIC=[MINISBLACK/MINISWHITE/RGB/CMYK/YCBCR/CIELAB/ICCLAB/ITULAB]
		if ( this->nBands > 1 )
		{
			options = CSLSetNameValue( options, "PHOTOMETRIC", "RGB" );

			if (this->bandInterleave)
				options = CSLSetNameValue( options, "INTERLEAVE", "BAND" );
			else
				options = CSLSetNameValue( options, "INTERLEAVE", "PIXEL" );
		}
		else
			options = CSLSetNameValue( options, "PHOTOMETRIC", "MINISBLACK" );




	}
	
	else if (ext.CompareNoCase("jpg"))
	{
		driverName = "JPEG";
		
		char quality[4];
		sprintf(quality,"%d",this->compressionQualityExport);
		
		options = CSLSetNameValue( options,"QUALITY", quality);
		tiffIsTemp= true;
	}
	else if (ext.CompareNoCase("jp2"))
	{
		driverName = "JP2ECW";
		//options = CSLSetNameValue( options, "WORLDFILE", "ON");
		tiffIsTemp= true;
	}
	else if (ext.CompareNoCase("ecw"))
	{
		driverName = "ECW";
		//options = CSLSetNameValue( options, "WORLDFILE", "ON");
		tiffIsTemp= true;
	}
	else	// no match with known driver
		return false;

	GDALAllRegister();

	// load driver
	GDALDriver *driverTIFF;
	GDALDataset *dataSetTIFF; 

	// get the tiff driver
	driverTIFF = GetGDALDriverManager()->GetDriverByName("GTIFF");
	CFileName tiffFileName;

	// change the file name when writing other formats then TIFF
	if (tiffIsTemp) tiffFileName = fn.GetPath() + CSystemUtilities::dirDelimStr + fn.GetFileName() + "_temp.tif";
	else
		tiffFileName = filename;

	// create the actual dataset
	dataSetTIFF = driverTIFF->Create(tiffFileName.GetChar(),this->nCols,this->nRows,this->nBands,(GDALDataType)this->sizeofpixel,options );

	// add colortable if available
	if ( this->getColorIndexed() )
	{
		GDALColorTable gdallut = GDALColorTable(GPI_RGB);

		int entrycount = this->lut->getMaxEntries();
		int rval, bval, gval;
		GDALColorEntry gdalentry;

		for ( int i =0; i < entrycount; i++)
		{
			this->lut->getColor(rval, bval, gval, i);
			gdalentry.c1 = rval;
			gdalentry.c2 = bval;
			gdalentry.c3 = gval;

			gdallut.SetColorEntry(i, &gdalentry); 
		}

		dataSetTIFF->GetRasterBand(1)->SetColorTable(&gdallut);
	}


	// we have to write 2 files, so the progress bar goes only till 50%
	if (tiffIsTemp)
		this->listenerMaxValue = 50;
	else
		this->listenerMaxValue = 100;

	// let's write the data
	if (!this->writeData(image,dataSetTIFF,this->tiled))
	{
		GDALClose( dataSetTIFF );
		return false;
	}
	
	// and geo data
	if ( !tiffIsTemp && this->exportGeoData)
	{
		this->writeGeoData(image,dataSetTIFF);
	
		if ( this->exportSeperatFile)
			this->tfw->writeTextFile(this->fileNameGeoData.GetChar());
	}


	
		
	// close the file
	GDALClose( dataSetTIFF );


	// finished?
	if (tiffIsTemp == false)
		return true;

	// ok, let's reopen the file and copy it to the actual format
	dataSetTIFF = (GDALDataset *) GDALOpen( tiffFileName.GetChar(), GA_ReadOnly );

	GDALDriver *driver;
	GDALDataset *dataSet; 
	
	driver = GetGDALDriverManager()->GetDriverByName(driverName.GetChar());
	dataSet = driver->CreateCopy(this->filename,dataSetTIFF,FALSE,NULL,NULL,NULL);

	bool ret = dataSet ? true : false;
	if (this->listener != NULL)
	{
		this->listener->setProgressValue(98);
	}
	
	// and geo data
	if (this->exportGeoData)
	{
		if ( this->exportSeperatFile && this->tfw)
			this->tfw->writeTextFile(this->fileNameGeoData.GetChar());
		else
			this->writeGeoData(image,dataSetTIFF);
	}


	GDALClose (dataSetTIFF);
	GDALClose( dataSet);

	// delete the temp tiff file
	remove(tiffFileName);

	if (this->listener != NULL)
	{
		this->listener->setProgressValue(100);
	}

	return ret;
}

/**
 *  writes a tiff file from a CBaseImage
 *
 * @param fileName the output file name
 * @param image the src image
 *
 */
bool CGDALFile::writeTIFF(const char* filename, const CImageBuffer &image)
{
	if (filename == NULL ) return false;

	strcpy(this->filename, filename);	
	
	CFileName fn(this->filename);
	char **options = NULL;		// options to create the file


	// no filename set
	if (fn.CompareNoCase("unknown"))
		return false;

	// set the basic information 
	this->nCols          = image.getWidth();
	this->nRows          = image.getHeight();
	this->nBands         = image.getBands();
	this->bpp            = image.getBpp();
	this->tiled          = false;
	this->bandInterleave = false;
	this->colorIndexed   = false;
	this->exportGeoData  = false; 

	this->compressionExport = "None";
	
	if (this->nCols <= 0 || this->nRows <= 0 || this->nBands <= 0 || this->bpp == -1)
		return false;

	if (this->bpp == 8) this->sizeofpixel = GDT_Byte;
	else if (this->bpp == 16) this->sizeofpixel = GDT_UInt16;
	else if (this->bpp == 32) this->sizeofpixel =  GDT_Float32;


	long sizeOfImage = this->nRows * this->nCols * this->nBands * this->sizeofpixel;
		
	if (sizeOfImage > 4000000000) // actually 2^32-1 but we also need space for the header
		options = CSLSetNameValue( options, "BIGTIFF", "YES" );
	
	options = CSLSetNameValue( options, "COMPRESS", this->compressionExport.GetChar() );
	options = CSLSetNameValue( options, "PROFILE", "GDALGeoTIFF" );
	
	//PHOTOMETRIC=[MINISBLACK/MINISWHITE/RGB/CMYK/YCBCR/CIELAB/ICCLAB/ITULAB]
	if ( this->nBands > 1 )
	{
		options = CSLSetNameValue( options, "PHOTOMETRIC", "RGB" );
		options = CSLSetNameValue( options, "INTERLEAVE", "PIXEL" );
	}
	else
		options = CSLSetNameValue( options, "PHOTOMETRIC", "MINISBLACK" );

	GDALAllRegister();

	// load driver
	GDALDriver  *driverTIFF  = GetGDALDriverManager()->GetDriverByName("GTIFF");
	GDALDataset *dataSetTIFF = driverTIFF->Create(filename,this->nCols,this->nRows,this->nBands,(GDALDataType)this->sizeofpixel,options );

	vector<GDALRasterBand*> rasterBands(this->nBands, 0);

	int xBlockSize = this->nCols;
	int yBlockSize = 1;

	unsigned char*  bufferUC = image.getPixUC();
	unsigned short* bufferUS = image.getPixUS();


	// get the 4 bands
	for (unsigned int i = 0; i < rasterBands.size(); ++i)
	{  
		rasterBands[i] = dataSetTIFF->GetRasterBand(i + 1);
	}

	if (this->nBands == 1) rasterBands[0]->SetColorInterpretation(GCI_GrayIndex);
	else if (this->nBands == 3)
	{
		rasterBands[0]->SetColorInterpretation(GCI_RedBand);
		rasterBands[1]->SetColorInterpretation(GCI_GreenBand);
		rasterBands[2]->SetColorInterpretation(GCI_BlueBand);
	}
	else if (this->nBands == 4)
	{
		rasterBands[0]->SetColorInterpretation(GCI_RedBand);
		rasterBands[1]->SetColorInterpretation(GCI_GreenBand);
		rasterBands[2]->SetColorInterpretation(GCI_BlueBand);
		rasterBands[3]->SetColorInterpretation(GCI_AlphaBand);
	}
	else 
	{
		GDALClose( dataSetTIFF );
		return false;
	}

	int offsetInWriteBuffer = this->nBands == 1? 1 : 4;
	int delta = yBlockSize * xBlockSize * offsetInWriteBuffer;// * this->sizeofpixel;
	int byteOffset = 0;

	// write the data, step by step
	for (int i = 0; i < this->nRows; i += yBlockSize, byteOffset += delta)
	{
		// adjust the last blocksize
		if ((i + yBlockSize) > this->nRows) yBlockSize = this->nRows - i;
		
		for (int band =0; band < this->nBands; band++)
		{
				rasterBands[band]->RasterIO(GF_Write, 
											0, 
											i, 
											xBlockSize, 
											yBlockSize, 
											bufferUC == NULL ? (unsigned char*) (bufferUS + band + byteOffset) :(unsigned char*) (bufferUC + band + byteOffset),
											xBlockSize,
											yBlockSize, 
											(GDALDataType)this->sizeofpixel, 
											offsetInWriteBuffer*this->sizeofpixel,
											0 );
		}
	};

	// close the file
	GDALClose( dataSetTIFF );

	if (this->listener != NULL)
	{
		this->listener->setProgressValue(100);
	}

	return true;
}

/**
 * private function that reads the header
 *
 */
bool CGDALFile::readHeader()
{
	if ( this->gdal == NULL)
	{
		GDALAllRegister();
		this->gdal = (GDALDataset *) GDALOpen( this->filename, GA_ReadOnly );
		if ( this->gdal == NULL)
			return false;
	}

	// addition to read RapidEye NITF 2.0 header for RPCs; gdal cannot read RPCs from RapidEye header
	// by M Awrangjeb on 2 Nov 2010
	// addition >>
	FILE* fp = fopen(this->filename,"r");
	//FILE* fout = fopen("RPC.txt","w");
	//fprintf(fout,"File name: %s\n\n\n\n",this->filename);
	char buffer[2280]; // maximum header length 2278
	if (!fp) 
	{
		fclose(fp);
		return false;
	}
	else
	{
		fgets(buffer,2279,fp);
		fclose(fp);
	}
	char FHDR[10];
	for (int i = 0; i < 9; i++)
		FHDR[i] = buffer[i];
	FHDR[9] = '\0';
	char ONAME[9]; // RapidEye
	for (int i = 0; i < 8; i++)
		ONAME[i] = buffer[300+i];
	ONAME[8] = '\0';
	CCharString fHeader(FHDR), fOriginator(ONAME);
	if (fHeader.CompareNoCase("NITF02.00") && fOriginator.CompareNoCase("RapidEye"))
	{//read RPCs
		if (this->tfw) delete this->tfw; // delete TFW if any
		this->rpc.sethasValues(false); // has RPCs

		CCharString Buf(buffer);
		CCharString Temp;
		Matrix parms(90,1);

		//line offset
		Temp = Buf.Mid(1012,1017);
		sscanf(Temp.GetChar(),"%lf",&parms.element(0, 0));
		//fprintf(fout,"LINE_OFF %s\n",Temp.GetChar());

		//sample offset
		Temp = Buf.Mid(1018,1022);
		sscanf(Temp.GetChar(),"%lf",&parms.element(1, 0));
		//fprintf(fout,"SAMP_OFF %s\n",Temp.GetChar());

		//Latitude offset
		Temp = Buf.Mid(1023,1030);
		sscanf(Temp.GetChar(),"%lf",&parms.element(2, 0));
		//fprintf(fout,"LAT_OFF %s\n",Temp.GetChar());

		//Longitude offset
		Temp = Buf.Mid(1031,1039);
		sscanf(Temp.GetChar(),"%lf",&parms.element(3, 0));
		//fprintf(fout,"LONG_OFF %s\n",Temp.GetChar());

		//Height offset
		Temp = Buf.Mid(1040,1044);
		sscanf(Temp.GetChar(),"%lf",&parms.element(4, 0));
		//fprintf(fout,"HEIGHT_OFF %s\n",Temp.GetChar());

		//line scale
		Temp = Buf.Mid(1045,1050);
		sscanf(Temp.GetChar(),"%lf",&parms.element(5, 0));
		//fprintf(fout,"LINE_SCALE %s\n",Temp.GetChar());

		//sample scale
		Temp = Buf.Mid(1051,1055);
		sscanf(Temp.GetChar(),"%lf",&parms.element(6, 0));
		//fprintf(fout,"SAMP_SCALE %s\n",Temp.GetChar());

		//Latitude scale
		Temp = Buf.Mid(1056,1063);
		sscanf(Temp.GetChar(),"%lf",&parms.element(7, 0));
		//fprintf(fout,"LAT_SCALE %s\n",Temp.GetChar());

		//Longitude scale
		Temp = Buf.Mid(1064,1072);
		sscanf(Temp.GetChar(),"%lf",&parms.element(8, 0));
		//fprintf(fout,"LONG_SCALE %s\n",Temp.GetChar());

		//Height scale
		Temp = Buf.Mid(1073,1077);
		sscanf(Temp.GetChar(),"%lf",&parms.element(9, 0));
		//fprintf(fout,"HEIGHT_SCALE %s\n",Temp.GetChar());

		/*
		int st_linnum = 1078; //start index of line numerator, 12 bytes each and total 240 bytes for 20 coeffitients
		int st_linden = st_linnum+240; //start index of line denominator, 12 bytes each and total 240 bytes for 20 coeffitients
		int st_samnum = st_linden+240; //start index of sample numerator, 12 bytes each and total 240 bytes for 20 coeffitients
		int st_samden = st_samnum+240; //start index of sample denominator, 12 bytes each and total 240 bytes for 20 coeffitients
		int st, en;
		for (int i=0; i < 20; i++)
		{
			// for line numerator coeffitients
			st = st_linnum+i*12; en = st+11; //start and end indices of current coeffitient
			Temp = Buf.Mid(st,en);
			sscanf(Temp.GetChar(),"%lf",&parms.element(10+i, 0));
			//fprintf(fout,"LINE_NUM_COEFF   %s\n",Temp.GetChar());

			// for line denominator coeffitients
			st = st_linden+i*12; en = st+11; //start and end indices of current coeffitient
			Temp = Buf.Mid(st,en);
			sscanf(Temp.GetChar(),"%lf",&parms.element(30+i, 0));
			//fprintf(fout,"LINE_DEN_COEFF   %s\n",Temp.GetChar());

			// for sample numerator coeffitients
			st = st_samnum+i*12; en = st+11; //start and end indices of current coeffitient
			Temp = Buf.Mid(st,en);
			sscanf(Temp.GetChar(),"%lf",&parms.element(50+i, 0));
			//fprintf(fout,"SAMPLE_NUM_COEFF %s\n",Temp.GetChar());

			// for sample denominator coeffitients
			st = st_samden+i*12; en = st+11; //start and end indices of current coeffitient
			Temp = Buf.Mid(st,en);
			sscanf(Temp.GetChar(),"%lf",&parms.element(70+i, 0));
			//fprintf(fout,"SAMPLE_DEN_COEFF %s\n",Temp.GetChar());
		}
		//fclose(fout);
		*/
		int len = Buf.GetLength();
		Temp = Buf.Mid(1078,len-1);
		//int posp = Temp.FindNextNumberLength();
		
		int i, pos;
		CCharString coeff, tmp;

		//line numerator coefficients
		for (i = 0; i < 20; i++)
		{
			pos = Temp.FindNextNumberLength();
			coeff = Temp.Left(pos);
			sscanf(coeff.GetChar(),"%lf",&parms.element(10+i, 0));
			len = Temp.GetLength();
			tmp = Temp.Mid(pos,len-1);
			Temp = tmp;
		}

		//line denominator coefficients
		for (i = 0; i < 20; i++)
		{
			pos = Temp.FindNextNumberLength();
			coeff = Temp.Left(pos);
			sscanf(coeff.GetChar(),"%lf",&parms.element(30+i, 0));
			len = Temp.GetLength();
			tmp = Temp.Mid(pos,len-1);
			Temp = tmp;
		}

		//sample numerator coefficients
		for (i = 0; i < 20; i++)
		{
			pos = Temp.FindNextNumberLength();
			coeff = Temp.Left(pos);
			sscanf(coeff.GetChar(),"%lf",&parms.element(50+i, 0));
			len = Temp.GetLength();
			tmp = Temp.Mid(pos,len-1);
			Temp = tmp;
		}

		//sample denominator coefficients
		for (i = 0; i < 20; i++)
		{
			pos = Temp.FindNextNumberLength();
			coeff = Temp.Left(pos);
			sscanf(coeff.GetChar(),"%lf",&parms.element(70+i, 0));
			len = Temp.GetLength();
			tmp = Temp.Mid(pos,len-1);
			Temp = tmp;
		}

		this->rpc.setAll(parms);

		/*
		Matrix parms(90,1);

			parms.element(0, 0) = sRPC.dfLINE_OFF;
			parms.element(1, 0) = sRPC.dfSAMP_OFF;
			parms.element(2, 0) = sRPC.dfLAT_OFF;
			parms.element(3, 0) = sRPC.dfLONG_OFF;
			parms.element(4, 0) = sRPC.dfHEIGHT_OFF;
			parms.element(5, 0) = sRPC.dfLINE_SCALE;
			parms.element(6, 0) = sRPC.dfSAMP_SCALE;
			parms.element(7, 0) = sRPC.dfLAT_SCALE;
			parms.element(8, 0) = sRPC.dfLONG_SCALE;
			parms.element(9, 0) = sRPC.dfHEIGHT_SCALE;

			for (int i=0; i < 20; i++)
			{
				parms.element(10 + i, 0) = sRPC.adfLINE_NUM_COEFF[i];
				parms.element(30 + i, 0) = sRPC.adfLINE_DEN_COEFF[i];
				parms.element(50 + i, 0) = sRPC.adfSAMP_NUM_COEFF[i];
				parms.element(70 + i, 0) = sRPC.adfSAMP_DEN_COEFF[i];

			}
			
			this->rpc.setAll(parms);
		*/
	}
	else
	{ // the following codes within this else statement (for TFW & RPCs) are from original file before the above change
		// << addition
		if (this->tfw) delete this->tfw;
		this->tfw = new CTFW();

		// Get TFW information
		this->tfw->setFileName("GEOTIFFInfo");
		double parms[6];
		
		if (this->gdal->GetGeoTransform(parms) == CE_None)
		{	
				
			this->tfw->sethasValues(true);
			Matrix parMat(6,1);
			parMat.element(0,0) = parms[1]; 
			parMat.element(1,0) = parms[2]; 
			parMat.element(2,0) = parms[0]; 
			parMat.element(3,0) = parms[4]; 
			parMat.element(4,0) = parms[5]; 
			parMat.element(5,0) = parms[3];
		 
			CCharString rasterSpace = this->gdal->GetMetadataItem(GDALMD_AREA_OR_POINT);
			if (rasterSpace != CCharString(GDALMD_AOP_POINT))
			{
				parMat.element(2,0) += (parms[1] + parms[4]) * 0.5; // pixel centre!! 
				parMat.element(5,0) += (parms[2] + parms[5]) * 0.5;
			}

			// set reference system values

			CReferenceSystem refSys;
			refSys.sethasValues(true);

			// read the information
			const char * string = this->gdal->GetProjectionRef();
			OGRSpatialReference refInfo(string);
			
			if (string != NULL)
			{
				// check spheriod
				OGRErr errorCodeSemiMajor;
				OGRErr errorCodeInvFlattening;

				double semiMajor = refInfo.GetSemiMajor(&errorCodeSemiMajor);
				double invFlattening = refInfo.GetInvFlattening(&errorCodeInvFlattening);

				if (errorCodeSemiMajor != 0 || errorCodeInvFlattening != 0)
					refSys.sethasValues(false);
				else
				{
					// get information from string
					CSpheroid spheroid("", semiMajor,invFlattening);

					// compare with known spheroides
					if (spheroid == WGS84Spheroid) refSys.setToWGS1984();
					else if ( spheroid == GRS1980Spheroid ) refSys.setSpheroid(GRS1980Spheroid);
					else if ( spheroid == GRS1967Spheroid ) refSys.setSpheroid(GRS1967Spheroid);
					else if ( spheroid == BesselSpheroid ) refSys.setSpheroid(BesselSpheroid);
					else if ( spheroid == AustralianNationalSpheroid ) refSys.setSpheroid(AustralianNationalSpheroid);
					else if ( spheroid == Clarke1866Spheroid ) refSys.setSpheroid(Clarke1866Spheroid);
					else if ( spheroid == Clarke1880Spheroid ) refSys.setSpheroid(Clarke1880Spheroid);
					else if ( spheroid == HayfordSpheroid ) refSys.setSpheroid(HayfordSpheroid);
					else if ( spheroid == IERSSpheroid ) refSys.setSpheroid(IERSSpheroid);
					else // not known 
					{
						refSys.sethasValues(false);
					}
				}

				// check coordinate type
				if (refInfo.IsGeographic())
				{
					refSys.setCoordinateType(eGEOGRAPHIC);
					if ((fabs(parMat.element(1,0)) < FLT_EPSILON) && (fabs(parMat.element(3,0)) < FLT_EPSILON))
					{
						// switch longitude and latitude
						double swap = parMat.element(2,0); 
						parMat.element(2,0) = parMat.element(5,0); 
						parMat.element(5,0) = swap;
						swap = parMat.element(0,0); 
						parMat.element(0,0) = parMat.element(3,0); 
						parMat.element(3,0) = swap;
						swap = parMat.element(1,0); 
						parMat.element(1,0) = parMat.element(4,0); 
						parMat.element(4,0) = swap;
					}

					refSys.setUTMZone(parMat.element(2,0), parMat.element(5,0));
				}

				else if( refInfo.IsProjected())
				{
					refSys.setCoordinateType(eGRID);
					CCharString pszProjection(refInfo.GetAttrValue( "PROJECTION" ));
			        
					if (!pszProjection.CompareNoCase(SRS_PT_TRANSVERSE_MERCATOR))
					{
						refSys.sethasValues(false);
					}
					else
					{
						int isNorth;
						int zone = refInfo.GetUTMZone(&isNorth);
						if (zone > 0) refSys.setUTMZone(zone, isNorth != 0 ? NORTHERN : SOUTHERN);
						else
						{
							refSys.setTMParameters(refInfo.GetNormProjParm( SRS_PP_CENTRAL_MERIDIAN, 0.0), 
												   refInfo.GetNormProjParm( SRS_PP_FALSE_EASTING, 0.0 ), 
												   refInfo.GetNormProjParm( SRS_PP_FALSE_NORTHING, 0.0), 
												   refInfo.GetProjParm( SRS_PP_SCALE_FACTOR, 1.0 ), 
												   refInfo.GetNormProjParm( SRS_PP_LATITUDE_OF_ORIGIN, 0.0 ) );		
						}
					}
				}
				else
					refSys.sethasValues(false);

			}
			else
				refSys.sethasValues(false);
			
			this->tfw->setRefSys(refSys);
			this->tfw->setAll(parMat);
		}
		else
			this->tfw->sethasValues(false);
		
		// check for RPC parameters
		this->rpc.sethasValues(false);

		GDALRPCInfo sRPC;
	   
		char **papszMD = this->gdal->GetMetadata(""); 
		if(GDALExtractRPCInfo( papszMD, &sRPC ))
		{
			Matrix parms(90,1);

			parms.element(0, 0) = sRPC.dfLINE_OFF;
			parms.element(1, 0) = sRPC.dfSAMP_OFF;
			parms.element(2, 0) = sRPC.dfLAT_OFF;
			parms.element(3, 0) = sRPC.dfLONG_OFF;
			parms.element(4, 0) = sRPC.dfHEIGHT_OFF;
			parms.element(5, 0) = sRPC.dfLINE_SCALE;
			parms.element(6, 0) = sRPC.dfSAMP_SCALE;
			parms.element(7, 0) = sRPC.dfLAT_SCALE;
			parms.element(8, 0) = sRPC.dfLONG_SCALE;
			parms.element(9, 0) = sRPC.dfHEIGHT_SCALE;

			for (int i=0; i < 20; i++)
			{
				parms.element(10 + i, 0) = sRPC.adfLINE_NUM_COEFF[i];
				parms.element(30 + i, 0) = sRPC.adfLINE_DEN_COEFF[i];
				parms.element(50 + i, 0) = sRPC.adfSAMP_NUM_COEFF[i];
				parms.element(70 + i, 0) = sRPC.adfSAMP_DEN_COEFF[i];

			}
			
			this->rpc.setAll(parms);
		}
		// addition >>
	} //else 
	// << addition
	int nb = this->gdal->GetRasterCount();

	this->setBands(nb);


	/// check if bands are labeled/in RGB order or else if four channels
	if (!this->hasColourInformations)
	{

		if( nb == 1)
		{
			this->colorChannel1 = 0;
			this->NoData1 = (float)this->gdal->GetRasterBand(1)->GetNoDataValue();
		}
		else if( nb == 3 )
		{
			this->NoData1 = (float)this->gdal->GetRasterBand(1)->GetNoDataValue();
			this->NoData2 = (float)this->gdal->GetRasterBand(2)->GetNoDataValue();
			this->NoData3 = (float)this->gdal->GetRasterBand(3)->GetNoDataValue();

			GDALColorInterp color1 = this->gdal->GetRasterBand(1)->GetColorInterpretation();;
			GDALColorInterp color2 = this->gdal->GetRasterBand(2)->GetColorInterpretation();
			GDALColorInterp color3 = this->gdal->GetRasterBand(3)->GetColorInterpretation();

			// select color for channel 1
			if (color1 == GCI_RedBand)
				this->colorChannel1 = 0;
			else if (color1 == GCI_GreenBand)
				this->colorChannel1 = 1;
			else if (color1 == GCI_BlueBand)
				this->colorChannel1 = 2;
			else
				this->colorChannel1 = 0;
			
			// select color for channel 2
			if (color2 == GCI_RedBand)
				this->colorChannel2 = 0;
			else if (color2 == GCI_GreenBand)
				this->colorChannel2 = 1;
			else if (color2 == GCI_BlueBand)
				this->colorChannel2 = 2;
			else
				this->colorChannel2 = 1;

			// select color for channel 3
			if (color3 == GCI_RedBand)
				this->colorChannel3 = 0;
			else if (color3 == GCI_GreenBand)
				this->colorChannel3 = 1;
			else if (color3 == GCI_BlueBand)
				this->colorChannel3 = 2;
			else
				this->colorChannel3 = 2;
		
			int hmm=9;

		}
		if( nb == 4 )
		{
			this->NoData1 = (float)this->gdal->GetRasterBand(1)->GetNoDataValue();
			this->NoData2 = (float)this->gdal->GetRasterBand(2)->GetNoDataValue();
			this->NoData3 = (float)this->gdal->GetRasterBand(3)->GetNoDataValue();
			this->NoData4 = (float)this->gdal->GetRasterBand(4)->GetNoDataValue();

			GDALColorInterp color1 = this->gdal->GetRasterBand(1)->GetColorInterpretation();;
			GDALColorInterp color2 = this->gdal->GetRasterBand(2)->GetColorInterpretation();
			GDALColorInterp color3 = this->gdal->GetRasterBand(3)->GetColorInterpretation();
			GDALColorInterp color4 = this->gdal->GetRasterBand(4)->GetColorInterpretation();

			// default 
			this->colorChannel1 = 0;
			this->colorChannel2 = 1;
			this->colorChannel3 = 2;
			this->colorChannel4 = 3;


			bool hasRed = true;
			bool hasGreen = true;
			bool hasBlue = true;
			bool hasAlpha = true;

			// select red channel
			if ( GCI_RedBand == color1)
				this->colorChannel1 = 0;
			else if (GCI_RedBand == color2)
				this->colorChannel2 = 0;
			else if (GCI_RedBand == color3)
				this->colorChannel3 = 0;
			else if (GCI_RedBand == color4)
				this->colorChannel4 = 0;
			else
				hasRed = false;

			
			// select green channel
			if ( GCI_GreenBand == color1)
				this->colorChannel1 = 1;
			else if (GCI_GreenBand == color2)
				this->colorChannel2 = 1;
			else if (GCI_GreenBand == color3)
				this->colorChannel3 = 1;
			else if (GCI_GreenBand== color4)
				this->colorChannel4 = 1;
			else
				hasGreen = false;

			// select blue channel
			if ( GCI_BlueBand == color1)
				this->colorChannel1 = 2;
			else if (GCI_BlueBand == color2)
				this->colorChannel2 = 2;
			else if (GCI_BlueBand == color3)
				this->colorChannel3 = 2;
			else if (GCI_BlueBand== color4)
				this->colorChannel4 = 2;
			else
				hasBlue = false;

			// select Alpha / NIR channel
			if ( GCI_AlphaBand == color1 || GCI_GrayIndex == color1)
				this->colorChannel1 = 3;
			else if (GCI_AlphaBand == color2 || GCI_GrayIndex == color2)
				this->colorChannel2 = 3;
			else if (GCI_AlphaBand== color3 || GCI_GrayIndex == color3)
				this->colorChannel3 = 3;
			else if (GCI_AlphaBand== color4 || GCI_GrayIndex == color4)
				this->colorChannel4 = 3;
			else
				hasAlpha = false;

			// no color information 
			if (!hasRed && !hasGreen && !hasBlue && hasAlpha )
			{
				if (this->colorChannel1 == 3)
				{
					this->colorChannel2 = 0;
					this->colorChannel3 = 1;
					this->colorChannel4 = 2;
				}
				else
				{
					this->colorChannel1 = 0;
					this->colorChannel2 = 1;
					this->colorChannel3 = 2;
				}

			}

			if (!hasAlpha && hasRed && hasGreen && hasBlue)
			{
				if (this->colorChannel1 == 0)
					this->colorChannel4 = 3;
				else
					this->colorChannel1 = 3;
			}

		}
	}

	this->setHeight(this->gdal->GetRasterYSize());
	this->setWidth(this->gdal->GetRasterXSize());

	// check if 8bit or 16bit or 32bit
	GDALDataType gdaldepth = this->gdal->GetRasterBand(1)->GetRasterDataType();

	if ( gdaldepth == GDT_Byte)
	{
		this->setBpp(8);
		this->sizeofpixel = 1;
	}
	else if ( gdaldepth == GDT_UInt16)
	{
		this->setBpp(16);
		this->sizeofpixel = 2;
	}
	else if ( gdaldepth == GDT_Int16)
	{
		this->setBpp(16);
		this->sizeofpixel = 2;
	}
	else if ( gdaldepth == GDT_Float64)
	{
		this->setBpp(64);
		this->sizeofpixel = 8;
	}
	else if ( gdaldepth == GDT_Float32)
	{
		this->setBpp(32);
		this->sizeofpixel= 4;
	}
	else if (gdaldepth == GDT_Int32)
	{
		this->setBpp(32);
		this->sizeofpixel= 4;
	}
	else if (gdaldepth == GDT_UInt32)
	{
		this->setBpp(32);
		this->sizeofpixel= 4;
	}
	else
	{
		
	}

	int xblocksize;
	int yblocksize;
	gdal->GetRasterBand(1)->GetBlockSize(&xblocksize , &yblocksize);

	if ( xblocksize != this->nCols || yblocksize != this->nRows)
	{
		this->tiled = true;
		this->tileWidth = xblocksize;
		this->tileHeight = yblocksize;

		this->tilesAcross = (this->nCols + this->tileWidth - 1) / this->tileWidth;
		this->tilesDown = (this->nRows + this->tileHeight - 1) / this->tileHeight;

		// need to buffer at least tilesAcross tiles
		this->nBufferedTiles = this->tilesAcross;

		if (!this->tiles) 
		{
			this->tiles = new CImageTile*[this->nBufferedTiles];
			for (int i = 0; i < this->tilesAcross; i++)
			{
				this->tiles[i] = NULL;
			}
		}
		else
		{
			for (int i = 0; i < this->tilesAcross; i++)
	    	{
				if (this->tiles[i]) delete this->tiles[i];
				this->tiles[i] = NULL;
		    }
		}

		int size = 0;
		//size = xblocksize*yblocksize;
		size = xblocksize*yblocksize*this->sizeofpixel;
		this->tileBuffer = new unsigned char[size];

		
	}
	
	// check if it is a palette index
	GDALColorInterp checkit = this->gdal->GetRasterBand(1)->GetColorInterpretation();
	if ( checkit == GCI_PaletteIndex) 
	{
		this->setColorIndexed(true);
	}

	if ( this->getColorIndexed() ) this->readLookupTableInfo();


	// check if we need to read the file with the modified libjpeg
	// this is necessary for 16 bit tiff images with jpeg compression
	// only works with single band images!
    this->needsModifiedJPEGLib = false;
	CCharString driverName(GDALGetDriverShortName( this->gdal->GetDriver()));
	if (this->bpp == 16 && this->nBands == 1 && driverName.CompareNoCase("GTiff")) // check only for 16 bit tiff images
	{
		char** papszMetadata;
		papszMetadata = this->gdal->GetMetadata("IMAGE_STRUCTURE");

		// go thru all metadata and search for the compression mode
		if( CSLCount(papszMetadata) > 0 )
		{
			for( int i = 0; papszMetadata[i] != NULL; i++ )
			{
				CCharString compression(papszMetadata[i]);
				if (compression.Find("COMPRESSION") >=0 && compression.Find("JPEG") >=0)
				{
					this->needsModifiedJPEGLib = true;
					break;
				}
				  
		   }
		}
	}

	this->bHeaderRead = true;

	return true;
}


/**
 * gets the image width
 * @return the width
 */
int CGDALFile::getWidth()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->nCols;
}

/**
 * gets the image height
 * @return the height
 */
int CGDALFile::getHeight()
{

	if (!this->bHeaderRead)
		this->readHeader();

	return this->nRows;
}

/**
 * @return the number of bits per pixel
 */
int CGDALFile::getBpp()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->bpp;
}

/**
 * @return the number of bands (eg RGB = 3)
 */
int CGDALFile::bands()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->nBands;
}

/**
 * Reads one scan line into buffer.
 *
 * @param buffer the destination buffer
 * @param iRow the row to read
 * @return false for failure
 */
bool CGDALFile::readLine(unsigned char* buffer, int iRow)
{
	if (iRow >= this->nRows)
		return false;

	if ( !this->needsModifiedJPEGLib && this->gdal == NULL )
	{
		GDALAllRegister();
		this->gdal = (GDALDataset *) GDALOpen( this->filename, GA_ReadOnly );

	}
	else if (this->needsModifiedJPEGLib && this->tif == NULL)
	{
		// check if the global function pointer is initialized
		// if yes, somebody is using it, so we can't continue here
		if (original_tif_predecode)
			return false;

		this->tif = TIFFOpen(this->filename,"r");
	}

	if ( this->linedata == NULL)
	{
		this->linedata = (unsigned char*) new int[this->nCols*this->nBands];
	}

	if ( this->nBands > 1 )
	{
		if ( this->rline == NULL)
		{
			this->rline = (unsigned char*) new int[this->nCols];
		}
		if ( this->gline == NULL)
		{
			this->gline = (unsigned char*) new int[this->nCols];
		}
		if ( this->bline == NULL)
		{
			this->bline = (unsigned char*) new int[this->nCols];
		}
		if ( this->nline == NULL && this->bands() == 4)
		{
			this->nline = (unsigned char*) new int[this->nCols];
		}
	}

	if ( this->tiled )
	{
		return this->readTiledLine(buffer, iRow);
	}
	else
	{
		if (this->nBands == 3)
		{
			return this->read3BandLine(buffer, iRow);
		}
		else if (this->nBands == 4)
		{
			return this->read4BandLine(buffer, iRow);
		}
		else if (this->nBands == 1)
		{
			return this->read1BandLine(buffer, iRow);
		}
	}

	return true;
}
    
bool CGDALFile::getRect(CImageBuffer &buffer)
{
	bool OK = true;

	if ( !this->needsModifiedJPEGLib && this->gdal == NULL )
	{
		GDALAllRegister();
		this->gdal = (GDALDataset *) GDALOpen( this->filename, GA_ReadOnly );

	}
	else if (this->needsModifiedJPEGLib && this->tif == NULL)
	{
		// check if the global function pointer is initialized
		// if yes, somebody is using it, so we can't continue here
		if (original_tif_predecode)
			return false;

		this->tif = TIFFOpen(this->filename,"r");
	}

	if (!this->gdal) OK = false;
	else
	{
		int llcx = buffer.getOffsetX();
		int llcy = buffer.getOffsetY();
		int w = buffer.getWidth();
		int h = buffer.getHeight();
		int c = buffer.getBands();
		int b = buffer.getBpp();

		if ((c != this->bands()) || (b != this->getBpp()) || (llcx < 0) || (llcy < 0) ||
			(w < 0) || (h < 0) || (llcx + w > this->getWidth()) || 
			(llcy + h > this->getHeight())) OK = false;
		else
		{
			unsigned char *pData;
			GDALDataType bufType;

			int nPixelSpace = buffer.getDiffX();
			int nLineSpace  = buffer.getDiffY();  
			int nBandSpace  = 1;

			if (b <= 8) 
			{
				pData   = buffer.getPixUC();
				bufType = GDT_Byte;
				nBandSpace = sizeof(unsigned char);
			}
			else if (b == 16)
			{
				if (buffer.getUnsignedFlag()) 
				{
					pData   = (unsigned char *) buffer.getPixUS();
					bufType = GDT_UInt16;
				}
				else 
				{
					pData   = (unsigned char *) buffer.getPixS();
					bufType = GDT_Int16;
				}

				nBandSpace = sizeof(unsigned short);
			}
			else 
			{
				pData   = (unsigned char *) buffer.getPixF();
				bufType = GDT_Float32;
				nBandSpace = sizeof(float);
			}

			if (!pData) OK = false;
			else
			{
				nPixelSpace *= nBandSpace;
				nLineSpace  *= nBandSpace;  

				if (this->gdal->RasterIO(GF_Read, llcx,  llcy, w, h, pData, w, h, 
					bufType, c, NULL,  nPixelSpace,  nLineSpace,  nBandSpace) == CE_Failure) OK = false;
			}
		}

	}


	return OK;
};

bool CGDALFile::read1BandLine(unsigned char* buffer, int iRow,unsigned int numberOfRowsToRead)
{
	GDALColorInterp checkit = gdal->GetRasterBand(1)->GetColorInterpretation();

	GDALDataType depth = gdal->GetRasterBand(1)->GetRasterDataType();

	unsigned int outputSize =  this->nCols*this->sizeofpixel*numberOfRowsToRead;

	if (this->scanLineBuffer == NULL)
	{
		this->scanLineBuffer = (unsigned char*) new int[this->nCols * numberOfRowsToRead];
	}

	if ( this->getColorIndexed() )
	{
		GDALRasterBand* pBand = this->gdal->GetRasterBand(1);
		pBand->RasterIO(GF_Read,0,iRow,this->nCols,numberOfRowsToRead,this->scanLineBuffer,this->nCols,numberOfRowsToRead,depth,0,0);
		
		if (this->getBpp() == 8)
		{
			memcpy(buffer, this->scanLineBuffer, outputSize);
		}
		else if (this->getBpp() == 16)
		{
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;
			memcpy(buffer, shortScanLineBuffer,outputSize );
		}
	}
	if ( checkit == GCI_GrayIndex )
	{
		GDALRasterBand* pBand = this->gdal->GetRasterBand(1);
		pBand->RasterIO(GF_Read,0,iRow,this->nCols,numberOfRowsToRead,this->scanLineBuffer,this->nCols,numberOfRowsToRead,depth,0,0);
		
		if (this->getBpp() == 8)
		{
			memcpy(buffer, this->scanLineBuffer, outputSize);
		}
		else if (this->getBpp() == 16)
		{
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;
			memcpy(buffer, shortScanLineBuffer, outputSize);
		}
	}
	// for float import of DEM data
	else if ( checkit == GCI_Undefined)
	{
		GDALRasterBand* pBand = this->gdal->GetRasterBand(1);
		pBand->RasterIO(GF_Read,0,iRow,this->nCols,numberOfRowsToRead,this->scanLineBuffer,this->nCols,numberOfRowsToRead,depth,0,0);
		
		if (this->getBpp() == 8)
		{
			memcpy(buffer, this->scanLineBuffer, outputSize);
		}
		if (this->getBpp() == 16)
		{
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;
			memcpy(buffer, shortScanLineBuffer, outputSize);
		}
		if (this->getBpp() == 32)
		{
			float* floatLineBuffer = (float*)this->scanLineBuffer;
			memcpy(buffer, floatLineBuffer, this->nCols*4*numberOfRowsToRead);
		}
	}

	return true;	
}




bool CGDALFile::read3BandLine(unsigned char* buffer, int iRow)
{
	GDALDataType depth = gdal->GetRasterBand(1)->GetRasterDataType();
	
	//channel 1
	if (this->colorChannel1 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->rline,this->nCols,1,depth,0,0);
	else if (this->colorChannel1 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->rline,this->nCols,1,depth,0,0);
	else if (this->colorChannel1 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->rline,this->nCols,1,depth,0,0);
	else
		return false;

	// channel 2
	if (this->colorChannel2 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->gline,this->nCols,1,depth,0,0);
	else if (this->colorChannel2 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->gline,this->nCols,1,depth,0,0);
	else if (this->colorChannel2 == 2) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->gline,this->nCols,1,depth,0,0);
	else
		return false;

	// channel 3
	if (this->colorChannel3 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->bline,this->nCols,1,depth,0,0);
	else if (this->colorChannel3 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->bline,this->nCols,1,depth,0,0);
	else if (this->colorChannel3 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->bline,this->nCols,1,depth,0,0);
	else
		return false;


	if (this->getBpp() == 8)
	{
		for (int c = 0; c < this->nCols; c++)
		{
			this->linedata[4*c]   = this->rline[c];
			this->linedata[4*c+1] = this->gline[c];
			this->linedata[4*c+2] = this->bline[c];
			this->linedata[4*c+3] = 0;
		}
		
		memcpy(buffer, this->linedata, this->nCols*4*this->sizeofpixel);
	}
	else if (this->getBpp() == 16)
	{
		unsigned short* rBuffer = (unsigned short*)this->rline;
		unsigned short* gBuffer = (unsigned short*)this->gline;
		unsigned short* bBuffer = (unsigned short*)this->bline;

		unsigned short* shortScanLineBuffer = (unsigned short*)this->linedata;

		for (int c = 0; c < this->nCols; c++)
		{
			shortScanLineBuffer[4*c]   = rBuffer[c];
			shortScanLineBuffer[4*c+1] = gBuffer[c];
			shortScanLineBuffer[4*c+2] = bBuffer[c];
			shortScanLineBuffer[4*c+3] = 0;
		}
		
		memcpy(buffer, shortScanLineBuffer, this->nCols*4*this->sizeofpixel);
	}

	return true;	
}

bool CGDALFile::read4BandLine(unsigned char* buffer, int iRow)
{
	GDALDataType depth = gdal->GetRasterBand(1)->GetRasterDataType();

	// channel 1
	if (this->colorChannel1 == 0)
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->rline,this->nCols,1,depth,0,0);
	else if (this->colorChannel1 == 1) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->rline,this->nCols,1,depth,0,0);
	else if (this->colorChannel1 == 2) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->rline,this->nCols,1,depth,0,0);
	else if (this->colorChannel1 == 3) 
		this->gdal->GetRasterBand(4)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->rline,this->nCols,1,depth,0,0);
	else
		return false;

	// channel 2
	if (this->colorChannel2 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->gline,this->nCols,1,depth,0,0);
	else if (this->colorChannel2 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->gline,this->nCols,1,depth,0,0);
	else if (this->colorChannel2 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->gline,this->nCols,1,depth,0,0);
	else if (this->colorChannel2 == 3 ) 
		this->gdal->GetRasterBand(4)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->gline,this->nCols,1,depth,0,0);
	else
		return false;

	// channel 3
	if (this->colorChannel3 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->bline,this->nCols,1,depth,0,0);
	else if (this->colorChannel3 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->bline,this->nCols,1,depth,0,0);
	else if (this->colorChannel3 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->bline,this->nCols,1,depth,0,0);
	else if (this->colorChannel3 == 3 ) 
		this->gdal->GetRasterBand(4)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->bline,this->nCols,1,depth,0,0);
	else
		return false;


	// channel 4
	if (this->colorChannel4 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->nline,this->nCols,1,depth,0,0);
	else if (this->colorChannel4 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->nline,this->nCols,1,depth,0,0);
	else if (this->colorChannel4 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->nline,this->nCols,1,depth,0,0);
	else if (this->colorChannel4 == 3 ) 
		this->gdal->GetRasterBand(4)->RasterIO(GF_Read,0,iRow,this->nCols,1,this->nline,this->nCols,1,depth,0,0);
	else
		return false;


	if (this->getBpp() == 8)
	{
		for (int c = 0; c < this->nCols; c++)
		{
			this->linedata[4*c]   = this->rline[c];
			this->linedata[4*c+1] = this->gline[c];
			this->linedata[4*c+2] = this->bline[c];
			this->linedata[4*c+3] = this->nline[c];
		}
		
		memcpy(buffer, this->linedata, this->nCols*4*this->sizeofpixel);
	}
	else if (this->getBpp() == 16)
	{
		unsigned short* rBuffer = (unsigned short*)this->rline;
		unsigned short* gBuffer = (unsigned short*)this->gline;
		unsigned short* bBuffer = (unsigned short*)this->bline;
		unsigned short* nBuffer = (unsigned short*)this->nline;

		unsigned short* shortScanLineBuffer = (unsigned short*)this->linedata;

		for (int c = 0; c < this->nCols; c++)
		{
			shortScanLineBuffer[4*c]   = rBuffer[c];
			shortScanLineBuffer[4*c+1] = gBuffer[c];
			shortScanLineBuffer[4*c+2] = bBuffer[c];
			shortScanLineBuffer[4*c+3] = nBuffer[c];
		}
		
		memcpy(buffer, shortScanLineBuffer, this->nCols*4*this->sizeofpixel);
	}

	return true;	
}


/**
 * gets a tile of imagery RGB data is returned BGR0
 *
 * @param r, c the row and column of upper left of tile
 * @return the buffer containing the pixels
 */
unsigned char* CGDALFile::getTile(int r, int c)
{
	if (this->tiles == NULL)
		return false;

	int tileRow = r/this->tileHeight;
	int tileCol = c/this->tileWidth;

	// first check the buffered tiles
	for (int i = 0; i < this->nBufferedTiles; i++)
	{
		CImageTile* imageTile = this->tiles[i];

		// make sure its there
		if (imageTile == NULL)
			continue;

		if ((imageTile->getTileRow() == tileRow) && (imageTile->getTileCol() == tileCol))
		{
			return imageTile->getData();
		}
	}

	// better buffer the tile

	// increment index (and make sure its in range)
	this->lastBufferedTile++;
	if (this->lastBufferedTile >= this->nBufferedTiles)
			this->lastBufferedTile = 0;

	// allocate new one if nessesary
	if (this->tiles[this->lastBufferedTile] == NULL)
		this->tiles[this->lastBufferedTile] = new CImageTile(this->tileWidth, this->tileHeight, this->bpp, this->nBands);

	CImageTile* imageTile = this->tiles[this->lastBufferedTile];

	imageTile->setTileRowCol(tileRow, tileCol);

	// read tile and convert to our format
	unsigned char* buffer = imageTile->getData();

	if (this->nBands == 3)
	{
		if (this->bpp == 8)
		{
			int channel;
			for ( int b = 0; b < 3; b++)
			{
				// use the channel order set by the user!
				if (b == 0)
					channel = this->colorChannel1;
				else if (b == 1)
					channel = this->colorChannel2;
				else if (b == 2)
					channel = this->colorChannel3;
				else
					channel = b;	// default rgb order

				this->gdal->GetRasterBand(channel+1)->ReadBlock(tileCol, tileRow, this->tileBuffer);

				for (int r = 0; r < (int)this->tileHeight; r++)
				{
					int lineOffsetQ = r*this->tileWidth*4;
					int lineOffsetT = r*this->tileWidth;

					for (int c = 0; c < (int)this->tileWidth; c++)
					{
						int q = c*4;
						buffer[lineOffsetQ+q+b] = this->tileBuffer[lineOffsetT+c];
					}
				}
			}
		}
		if (this->bpp == 16)
		{
			unsigned long channel;

			unsigned short* shortbuffer = (unsigned short*)buffer;

			for ( int b = 0; b < 3; b++)
			{

				// use the channel order set by the user!
				if (b == 0)
					channel = this->colorChannel1;
				else if (b == 1)
					channel = this->colorChannel2;
				else if (b == 2)
					channel = this->colorChannel3;
				else
					channel = b;	// default rgb order

				unsigned short* shortilebuffer = (unsigned short*)this->tileBuffer;
				this->gdal->GetRasterBand(channel+1)->ReadBlock(tileCol, tileRow, shortilebuffer);

				for (unsigned long r = 0; r < (unsigned long)this->tileHeight; r++)
				{
					unsigned long lineOffsetQ = r*this->tileWidth*4;
					unsigned long lineOffsetT = r*this->tileWidth;

					for (unsigned long c = 0; c < (unsigned long)this->tileWidth; c++)
					{
						unsigned long q = c*4;
						unsigned long indexbuffer = lineOffsetQ+q+b;
						unsigned long indextilebuffer = lineOffsetT+c;
						shortbuffer[indexbuffer] = shortilebuffer[indextilebuffer];
					}
				}
			}
		}
	}
	else if (this->nBands == 4)
	{
		if (this->bpp == 8)
		{
			for ( int b = 0; b < 4; b++)
			{
				int channel;
				// use the channel order set by the user!
				if (b == 0)
					channel = this->colorChannel1;
				else if (b == 1)
					channel = this->colorChannel2;
				else if (b == 2)
					channel = this->colorChannel3;
				else if (b == 3)
					channel = this->colorChannel4;
				else
					channel = b;	// default rgba order


				this->gdal->GetRasterBand(channel+1)->ReadBlock(tileCol, tileRow, this->tileBuffer);

				for (int r = 0; r < (int)this->tileHeight; r++)
				{
					int lineOffsetQ = r*this->tileWidth*4;
					int lineOffsetT = r*this->tileWidth;

					for (int c = 0; c < (int)this->tileWidth; c++)
					{
						int q = c*4;
						buffer[lineOffsetQ+q+b] = this->tileBuffer[lineOffsetT+c];
					}
				}
			}
		}
		if (this->bpp == 16)
		{
			unsigned long channel;

			unsigned short* shortbuffer = (unsigned short*)buffer;

			for ( int b = 0; b < 4; b++)
			{

				// use the channel order set by the user!
				if (b == 0)
					channel = this->colorChannel1;
				else if (b == 1)
					channel = this->colorChannel2;
				else if (b == 2)
					channel = this->colorChannel3;
				else if (b == 3)
					channel = this->colorChannel4;
				else
					channel = b;	// default rgba order

				unsigned short* shortilebuffer = (unsigned short*)this->tileBuffer;
				this->gdal->GetRasterBand(channel+1)->ReadBlock(tileCol, tileRow, shortilebuffer);

				for (unsigned long r = 0; r < (unsigned long)this->tileHeight; r++)
				{
					unsigned long lineOffsetQ = r*this->tileWidth*4;
					unsigned long lineOffsetT = r*this->tileWidth;

					for (unsigned long c = 0; c < (unsigned long)this->tileWidth; c++)
					{
						unsigned long q = c*4;
						unsigned long indexbuffer = lineOffsetQ+q+b;
						unsigned long indextilebuffer = lineOffsetT+c;
						shortbuffer[indexbuffer] = shortilebuffer[indextilebuffer];
					}
				}
			}
		}
	}
	else if (this->nBands == 1)
	{
		if (this->needsModifiedJPEGLib && this->tif)
		{
			this->readTileWithModifiedJPEGLib(tileCol, tileRow, buffer);
		}
		else
			this->gdal->GetRasterBand(1)->ReadBlock(tileCol, tileRow, buffer);
	}


	return imageTile->getData();
}

bool CGDALFile::readTiledLine(unsigned char* buffer, int iRow)
{
    int rowInTile = iRow % this->tileHeight;
	
    int rowOffset = 0;
	
	if (this->nBands == 3)
	{
		if ( this->bpp == 8 )
		{
			rowOffset = rowInTile*this->tileWidth*4;
		}
		if ( this->bpp == 16 )
		{
			rowOffset = rowInTile*this->tileWidth*8;
		}
	}
	else if (this->nBands == 4)
	{
		if ( this->bpp == 8 )
		{
			rowOffset = rowInTile*this->tileWidth*4;
		}
		if ( this->bpp == 16 )
		{
			rowOffset = rowInTile*this->tileWidth*8;
		}
	}
	else
	{
        rowOffset = rowInTile*this->tileWidth*this->bpp/8;
	}

	for (int c = 0; c < this->nCols; c+= this->tileWidth)
	{
		unsigned char* src = this->getTile(iRow, c); // get the tile that includes this r, c
		
		if (this->nBands == 3)
		{
			if ( this->bpp == 8 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (unsigned int)this->nCols)
				{
					memcpy(buffer+4*c, src+rowOffset, (this->nCols-c)*4);
				}
				else
				{
					memcpy(buffer+4*c, src+rowOffset, this->tileWidth*4);
				}
			}
			if ( this->bpp == 16 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (unsigned int)this->nCols)
				{
					memcpy(buffer+8*c, src+rowOffset, (this->nCols-c)*8);
				}
				else
				{
					memcpy(buffer+8*c, src+rowOffset, this->tileWidth*8);
				}
			}
		}
		else if (this->nBands == 4)
		{
			if ( this->bpp == 8 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (unsigned int)this->nCols)
				{
					memcpy(buffer+4*c, src+rowOffset, (this->nCols-c)*4);
				}
				else
				{
					memcpy(buffer+4*c, src+rowOffset, this->tileWidth*4);
				}
			}
			if ( this->bpp == 16 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (unsigned int)this->nCols)
				{
					memcpy(buffer+8*c, src+rowOffset, (this->nCols-c)*8);
				}
				else
				{
					memcpy(buffer+8*c, src+rowOffset, this->tileWidth*8);
				}
			}
		}
		else
		{
			// the last tile in a row extends beyond the image
			if (c+this->tileWidth > (unsigned int)this->nCols)
			{
				memcpy(buffer+c*this->bpp/8, src+rowOffset, (this->nCols-c)*this->bpp/8);
			}
			else
			{
				memcpy(buffer+c*this->bpp/8, src+rowOffset, this->tileWidth*this->bpp/8);
			}
		}
	}

	return true;
}

bool CGDALFile::writeGeoData(CBaseImage* image, GDALDataset* dataSet)
{

	if (this->tfw == NULL) 
		return false;

	// information which is contained in TFW file; Barista uses the centre of the pixel as reference!
/*
	double param[6] = { this->tfw->getC() - 0.5 * (this->tfw->getA() + this->tfw->getB()),
						this->tfw->getA(),
						this->tfw->getD(),
						this->tfw->getF() - 0.5 * (this->tfw->getD() + this->tfw->getE()),
						this->tfw->getB(),
						this->tfw->getE()};
*/
	double param[6] = { this->tfw->getC() - 0.5 * (this->tfw->getA() + this->tfw->getB()),
						
						this->tfw->getA(),
						this->tfw->getB(),
						this->tfw->getF() - 0.5 * (this->tfw->getD() + this->tfw->getE()),
						this->tfw->getD(),
						this->tfw->getE()};


	dataSet->SetGeoTransform( param );

	// information which is contained in the CReferenceSystem
	CReferenceSystem refSys = this->tfw->getRefSys();
	OGRSpatialReference info;

	if ( refSys.getSpheroid() == WGS84Spheroid )
		info.SetWellKnownGeogCS("WGS84");
	else
	{
		// spheroid parameters
		CCharString spheroidname(refSys.getSpheroid().getName());
		double semiMajor = refSys.getSpheroid().getSemiMajor();
		double inverseF = 1.0/refSys.getSpheroid().getFlattening();

		info.SetGeogCS(spheroidname.GetChar(), spheroidname.GetChar(),spheroidname.GetChar(),semiMajor,inverseF);
	}

	if (refSys.getCoordinateType() == eGEOGRAPHIC)
	{
		dataSet->SetProjection("Geographic");
	}
	else if( refSys.getCoordinateType() == eGRID )
	{
		if ( refSys.isUTM() )
		{
			int isNorth = refSys.getHemisphere();
			int zone = refSys.getZone();

			if ( isNorth == NORTHERN )
				info.SetUTM(zone, true);
			else if ( isNorth == SOUTHERN )
				info.SetUTM(zone, false);
		}
		else 
		{
			double dfCenterLat =  refSys.getLatitudeOrigin();
			double dfCenterLong = refSys.getCentralMeridian();
			double dfScale = refSys.getScale();
			double dfFalseEasting = refSys.getEastingConstant();
			double dfFalseNorthing = refSys.getNorthingConstant();

			info.SetTM(dfCenterLat,dfCenterLong,dfScale,dfFalseEasting,dfFalseNorthing);
		}
	}
	else if( refSys.getCoordinateType() == eGEOCENTRIC )
	{
		//info.SetGeogCS
	}
	else 
		info.SetLocalCS("Undefined Reference System");


    char *infoWKT = NULL;
    info.exportToWkt( &infoWKT );
    dataSet->SetProjection( infoWKT );
    CPLFree( infoWKT );

	return true;
}


bool CGDALFile::writeData(CBaseImage* image,GDALDataset* dataSet, bool tiled)
{
	bool returnValue =false;

	if (this->nBands == 1)
	{
		returnValue = this->write1Band(image,dataSet,tiled);
	}
	else if (this->nBands == 3)
	{
		returnValue = this->write3Bands(image,dataSet,tiled);
	}
	else if (this->nBands == 4)
	{
		returnValue = this->write4Bands(image,dataSet,tiled);
	}
	else
		returnValue = false;


	return returnValue;
}


void CGDALFile::setExportFileOptions(CCharString& channelCombination,
							  CCharString& compression,
							  int compressionQuality,
							  bool exportGeoData,
							  bool exportSeperatFile,
							  bool bandInterleave,
							  CFileName fileNameGeoData)
{
	this->channelExport = channelCombination;
	this->compressionExport = compression;
	this->compressionQualityExport = compressionQuality;
	this->exportGeoData = exportGeoData;
	this->exportSeperatFile = exportSeperatFile;
	this->bandInterleave = bandInterleave;
	if (this->exportSeperatFile) this->fileNameGeoData = fileNameGeoData;

}




bool CGDALFile::write1Band(CBaseImage* image,GDALDataset* dataSet,bool tiled)
{
	GDALRasterBand *band1 = NULL;

	int inputBands = image->getBands() == 1 ? 1 : 4;	// we always use 1 or 4 in the hugeImage, never 3!
	int inputBpp = image->getBpp();

	int outputBands = this->nBands;
	int outputBpp = this->bpp;
	
	bool convertFrom16To8Bit = inputBpp == 16 && outputBpp == 8;
	bool convertFrom32To16Bit = inputBpp == 32 && outputBpp == 16 ;
	bool convertFrom32To8Bit = inputBpp == 32 && outputBpp == 8;
	bool computeIntensity = this->channelExport.CompareNoCase("Intensity") && inputBands > 1;

	// we may have to convert from 16 to 8 bit later
	CLookupTable lt(inputBpp,image->getBands());
	// compute the lookup table 
	if (convertFrom16To8Bit	)
	{
		lt = image->get16To8ConversionLookupTable();
	}

	unsigned char* bufferUC = NULL;
	unsigned short* bufferUS = NULL;
	float* bufferF = NULL;

	int xBlockSize;
	int yBlockSize;

	if (tiled)
	{
		xBlockSize = image->getTileWidth();
		yBlockSize = image->getTileHeight();
	}
	else
	{
		xBlockSize = this->nCols;
		yBlockSize = 1;//image->getTileHeight();
	}

	// get the memory
	if (inputBpp == 32)  
	{
		if (convertFrom32To16Bit)
		{	if (bufferUS == NULL)
				bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands * 2];
			bufferF = (float *) bufferUS;
		}
		else 
		{
			if (bufferUC == NULL)
				bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands * 4];
			bufferF = (float *) bufferUC;
		}
	}
	else if (inputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}

	if (outputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}	

	// get the band and set the lable
	band1 = dataSet->GetRasterBand(1);
	band1->SetColorInterpretation(GCI_GrayIndex);

	int offsetInReadBuffer = 0;		
	int offsetInWriteBuffer = inputBands;//(convertFrom16To8Bit && (inputBands > 1)) ? 0  : inputBands;
	// if necessary, select the right channel
	if (inputBands > 1)
	{
		if (this->channelExport.CompareNoCase("Red"))
			offsetInReadBuffer = 0;
		else if (this->channelExport.CompareNoCase("Green"))
			offsetInReadBuffer = 1;
		else if (this->channelExport.CompareNoCase("Blue"))
			offsetInReadBuffer = 2;
		else if (this->channelExport.CompareNoCase("Alpha"))
			offsetInReadBuffer = 3;
		else if (this->channelExport.CompareNoCase("Intensity"))
			offsetInReadBuffer = 0;
	}

	// write the data, step by step
	if (tiled)		// write in tiled blocks
	{
		unsigned char* tileBufferUC= NULL;
		unsigned short* tileBufferUS = NULL;

		int tilesAcross = (this->nCols + xBlockSize - 1) / xBlockSize;
		int tilesDown =   (this->nRows + yBlockSize - 1) / yBlockSize;
		int count=0;

		for (int row=0; row < this->nRows; row+= yBlockSize)
		{
			for (int col=0; col < this->nCols; col+=xBlockSize)
			{
				count++;
				// read	from the hugeImage
				if (inputBpp == 16)
				{
					tileBufferUS = (unsigned short*)image->getTile(row,col);
					if (convertFrom16To8Bit)
						copyData(tileBufferUS,bufferUC,xBlockSize,yBlockSize,offsetInReadBuffer,0,inputBands,outputBands,lt,computeIntensity);
					else
						copyData(tileBufferUS,bufferUS,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,outputBands,computeIntensity);
				}
				else if (inputBpp == 8)
				{
					tileBufferUC = image->getTile(row,col);	
					copyData(tileBufferUC,bufferUC,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,outputBands,computeIntensity);
				}
				else if (inputBpp == 32)
				{
					tileBufferUC = image->getTile(row,col);	

					if (convertFrom32To16Bit || convertFrom32To8Bit)
					{
						float MAX = image->getHistogram()->getMax();
						float MIN = image->getHistogram()->getMin();
						float range = MAX - MIN;

						float* tileBufferF = (float*) tileBufferUC;
						 for (int r = 0; r < image->tileHeight; r++)
						 {
							 int offset = r*image->tileWidth;

							 for (int c = 0; c < image->tileWidth; c++)
							 {
								float pixel = tileBufferF[offset+c];

								if (pixel < 0) pixel = 0;

								if (convertFrom32To8Bit)
									bufferUC[offset+c] = (unsigned char)(255.0f*((pixel - MIN)/range)+0.5);
								else
									bufferUS[offset+c] = (unsigned short)(65535.0f*((pixel - MIN)/range)+0.5);
							}
						}
					}
					else
						copyData((float *)tileBufferUC,bufferF,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,outputBands,computeIntensity);
				}
				else
				{
					return false;
				}

				int tileRow = row/yBlockSize;
				int tileCol = col/xBlockSize;
	  
				band1->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));

				if (this->listener != NULL)
				{
					double per = (float)count / (float) (tilesAcross*tilesDown);
					this->listener->setProgressValue(per*this->listenerMaxValue);
				}
			}
		}
	}
	else // write row by row
	{
		int sizePerPixel = this->bpp/8;
		for (int i=0; i < this->nRows; i+=yBlockSize)
		{
			// adjust the last blocksize
			if ((i+yBlockSize) > this->nRows)
				yBlockSize = this->nRows - i;

			// read from the hugeImage
			if (inputBpp == 16)
			{
				image->getRect((unsigned char*)bufferUS, i, yBlockSize, 0, xBlockSize);

				if (convertFrom16To8Bit)
					copyData(bufferUS,bufferUC,xBlockSize,yBlockSize,offsetInReadBuffer,offsetInReadBuffer,inputBands,inputBands,lt,computeIntensity);
				else if (computeIntensity)
					copyData(bufferUS,bufferUS,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,inputBands,computeIntensity);			
			}
			else if (inputBpp == 8)
			{
				image->getRect(bufferUC, i, yBlockSize, 0, xBlockSize);
				if (computeIntensity)
					copyData(bufferUC,bufferUC,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,inputBands,computeIntensity);
			}
			else if (inputBpp == 32)
			{
				if (convertFrom32To16Bit)
					image->getRect((unsigned char*)bufferUS, i, yBlockSize, 0, xBlockSize);
				else
					image->getRect(bufferUC, i, yBlockSize, 0, xBlockSize);

				if (convertFrom32To16Bit || convertFrom32To8Bit)
				{
					float MAX = image->getHistogram()->getMax();
					float MIN = image->getHistogram()->getMin();
					float range = MAX - MIN;

					float* tmpBufferF = convertFrom32To16Bit ? (float*)bufferUS : (float*) bufferUC;
   
					for (int c = 0; c < xBlockSize; c++)
					{
						float pixel = tmpBufferF[c];

						if (pixel < 0) pixel = 0;

						if (convertFrom32To8Bit)
							bufferUC[c] = (unsigned char)(255.0f*((pixel - MIN)/range)+0.5);
						else
							bufferUS[c] = (unsigned short)(65535.0f*((pixel - MIN)/range)+0.5);
					}
				}
				else
					copyData((float*)bufferUC,bufferF,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,outputBands,computeIntensity);
			}

			// write to disk
			band1->RasterIO(GF_Write,	// write flag 
								0,		// offset cols
								i,		// offset rows
								xBlockSize, // number of cols to be written
								yBlockSize,		 // number of rows to be written
								bufferUC == NULL ?
								(unsigned char*)(bufferUS + offsetInReadBuffer) :
								(unsigned char*)(bufferUC + offsetInReadBuffer), // src buffer + offset for single color channels
								xBlockSize,	// number of cols in the buffer to be read
								yBlockSize,			// number of rows in the buffer to be read
								(GDALDataType)this->sizeofpixel,   // dataType
								offsetInWriteBuffer*sizePerPixel,		   // offset between 2 pixels in one row for single color channels
								0												// offset between 2 rows 	
								);	

			if (this->listener != NULL)
			{
				double per = (float)i / (float) this->nRows;
				this->listener->setProgressValue(per*this->listenerMaxValue);
			}
		}
	}

	if (bufferUC != NULL)
		delete [] bufferUC;
	if (bufferUS != NULL)
		delete [] bufferUS;

	return true;
}

bool CGDALFile::write3Bands(CBaseImage* image,GDALDataset* dataSet,bool tiled)
{

	vector<GDALRasterBand*> rasterBands;
	rasterBands.resize(3);

	vector<int> channel;
	channel.resize(3);

	unsigned char* buffer = NULL;

	bool returnValue = true;

	if (dataSet == NULL)
		return false;

	int step=1;  // number of rows, written in one call
	int inputBands = image->getBands() == 1 ? 1 : 4;	// we always use 1 or 4 in the hugeImage, never 3!
	int inputBpp = image->getBpp();

	int outputBands = this->nBands;
	int outputBpp = this->bpp;

	bool convertFrom16To8Bit = inputBpp == 16 && outputBpp == 8;

	// we may have to convert from 16 to 8 bit later
	CLookupTable lt(inputBpp,3);
	// compute the lookup table 
	if (convertFrom16To8Bit	)
	{
		lt = image->get16To8ConversionLookupTable();
	}

	unsigned char* bufferUC = NULL;
	unsigned short* bufferUS = NULL;

	int xBlockSize;
	int yBlockSize;

	if (tiled)
	{
		xBlockSize = image->getTileWidth();
		yBlockSize = image->getTileHeight();
	}
	else
	{
		xBlockSize = this->nCols;
		yBlockSize = image->getTileHeight();
	}

	// get the memory
	if (inputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}

	if (outputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}	

	// get the 3 bands
	rasterBands[0] = dataSet->GetRasterBand(1);
	rasterBands[1] = dataSet->GetRasterBand(2);
	rasterBands[2] = dataSet->GetRasterBand(3);
	
	// set the correct band lables and select channels
	if (this->channelExport.CompareNoCase("RGB"))
	{
		rasterBands[0]->SetColorInterpretation(GCI_RedBand);
		rasterBands[1]->SetColorInterpretation(GCI_GreenBand);
		rasterBands[2]->SetColorInterpretation(GCI_BlueBand);
		channel[0] = 0;	
		channel[1] = 1;
		channel[2] = 2;	
	}
	else if (this->channelExport.CompareNoCase("CIR"))
	{
		rasterBands[0]->SetColorInterpretation(GCI_AlphaBand);
		rasterBands[1]->SetColorInterpretation(GCI_RedBand);
		rasterBands[2]->SetColorInterpretation(GCI_GreenBand);
		channel[0] = 3;
		channel[1] = 0;
		channel[2] = 1;
	}
	else
		returnValue = false;

	int offsetInWriteBuffer = 4;
	
	if (tiled)
	{
		int tilesAcross = (this->nCols + xBlockSize - 1) / xBlockSize;
		int tilesDown =   (this->nRows + yBlockSize - 1) / yBlockSize;
		int count=0;

		unsigned char* tileBufferUC = NULL;
		unsigned short* tileBufferUS = NULL;

		for (int row=0; row < this->nRows; row+= yBlockSize)
		{
			for (int col=0; col < this->nCols; col+=xBlockSize)
			{
				count++;
				int tileRow = row/yBlockSize;
				int tileCol = col/xBlockSize;				
				
				// read	from the hugeImage
				if (inputBpp == 16)
				{
					tileBufferUS = (unsigned short*)image->getTile(row,col);
					if (convertFrom16To8Bit)
					{
						for (int band = 0; band < 3; band++)
						{
							copyData(tileBufferUS,bufferUC,xBlockSize,yBlockSize,channel[band],0,4,1,lt,false);
							rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
						}
					}
					else
					{
						for (int band = 0; band < 3; band++)
						{
							copyData(tileBufferUS,bufferUS,xBlockSize,yBlockSize,channel[band],4,1,false);
							rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
						}
					}

				}
				else
				{
					tileBufferUC = image->getTile(row,col);	
					for (int band = 0; band < 3; band++)
					{
						copyData(tileBufferUC,bufferUC,xBlockSize,yBlockSize,channel[band],4,1,false);
						rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
					}
				}

				if (this->listener != NULL)
				{
					double per = (float)count / (float) (tilesAcross*tilesDown);
					this->listener->setProgressValue(per*this->listenerMaxValue);
				}
			}
		}


	}
	else
	{
		// write the data, step by step
		for (int i=0; i < this->nRows && returnValue; i+=yBlockSize)
		{
			// adjust the last blocksize
			if ((i+yBlockSize) > this->nRows)
				yBlockSize = this->nRows - i;
		
			if (inputBpp == 16)
			{	
				image->getRect((unsigned char*)bufferUS, i, yBlockSize, 0, xBlockSize);
				if (convertFrom16To8Bit)
				{
					copyData(bufferUS,bufferUC,xBlockSize*inputBands,yBlockSize,0,0,1,1,lt,false);
				}
			}
			else
			{
				image->getRect(bufferUC, i, yBlockSize, 0, xBlockSize);
			}
			

			for (int band=0; band < 3; band++)
				rasterBands[band]->RasterIO(GF_Write, 
											0, 
											i, 
											xBlockSize, 
											yBlockSize, 
											bufferUC == NULL ? (unsigned char*) (bufferUS + channel[band]) :(unsigned char*) (bufferUC + channel[band]),
											xBlockSize, 
											yBlockSize, 
											(GDALDataType)this->sizeofpixel, 
											offsetInWriteBuffer*this->sizeofpixel,
											0 );

			if (this->listener != NULL)
			{
				double per = (float)i / (float) this->nRows;
				this->listener->setProgressValue(per*this->listenerMaxValue);
			}
		}		
	}
	
	if (bufferUC != NULL)
		delete [] bufferUC;
	if (bufferUS != NULL)
		delete [] bufferUS;
	
	return true;
}

bool CGDALFile::write4Bands(CBaseImage* image,GDALDataset* dataSet,bool tiled)
{
	vector<GDALRasterBand*> rasterBands;
	rasterBands.resize(4);

	if (dataSet == NULL)
		return false;

	int inputBands = 4;	
	int inputBpp = image->getBpp();

	int outputBands = 4;
	int outputBpp = this->bpp;

	bool convertFrom16To8Bit = inputBpp == 16 && outputBpp == 8;

	// we may have to convert from 16 to 8 bit later
	CLookupTable lt(inputBpp,image->getBands());
	// compute the lookup table 
	if (convertFrom16To8Bit	)
	{
		lt = image->get16To8ConversionLookupTable();
	}

	int xBlockSize;
	int yBlockSize;

	if (tiled)
	{
		xBlockSize = image->getTileWidth();
		yBlockSize = image->getTileHeight();
	}
	else
	{
		xBlockSize = this->nCols;
		yBlockSize = image->getTileHeight();
	}

	unsigned char* bufferUC = NULL;
	unsigned short* bufferUS = NULL;

	// get the memory
	if (inputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}

	if (outputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}

	// get the 4 bands
	rasterBands[0] = dataSet->GetRasterBand(1);
	rasterBands[1] = dataSet->GetRasterBand(2);
	rasterBands[2] = dataSet->GetRasterBand(3);
	rasterBands[3] = dataSet->GetRasterBand(4);

	rasterBands[0]->SetColorInterpretation(GCI_RedBand);
	rasterBands[1]->SetColorInterpretation(GCI_GreenBand);
	rasterBands[2]->SetColorInterpretation(GCI_BlueBand);
	rasterBands[3]->SetColorInterpretation(GCI_AlphaBand);

	int offsetInWriteBuffer = 4;
	if (tiled)
	{
		int tilesAcross = (this->nCols + xBlockSize - 1) / xBlockSize;
		int tilesDown =   (this->nRows + yBlockSize - 1) / yBlockSize;
		int count=0;

		unsigned char* tileBufferUC = NULL;
		unsigned short* tileBufferUS = NULL;

		for (int row=0; row < this->nRows; row+= yBlockSize)
		{
			for (int col=0; col < this->nCols; col+=xBlockSize)
			{
				count++;
				int tileRow = row/yBlockSize;
				int tileCol = col/xBlockSize;				
				
				// read	from the hugeImage
				if (inputBpp == 16)
				{
					tileBufferUS = (unsigned short*)image->getTile(row,col);
					if (convertFrom16To8Bit)
					{
						for (int band = 0; band < 4; band++)
						{
							copyData(tileBufferUS,bufferUC,xBlockSize,yBlockSize,band,0,4,1,lt,false);
							rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
						}
					}
					else
					{
						for (int band = 0; band < 4; band++)
						{
							copyData(tileBufferUS,bufferUS,xBlockSize,yBlockSize,band,4,1,false);
							rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
						}
					}

				}
				else
				{
					tileBufferUC = image->getTile(row,col);	
					for (int band = 0; band < inputBands; band++)
					{
						copyData(tileBufferUC,bufferUC,xBlockSize,yBlockSize,band,4,1,false);
						rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
					}
				}

				if (this->listener != NULL)
				{
					double per = (float)count / (float) (tilesAcross*tilesDown);
					this->listener->setProgressValue(per*this->listenerMaxValue);
				}
			}
		}
	}
	else
	{
		// write the data, step by step
		for (int i=0; i < this->nRows; i+=yBlockSize)
		{
			// adjust the last blocksize
			if ((i+yBlockSize) > this->nRows)
				yBlockSize = this->nRows - i;
		
			if (inputBpp == 16)
			{	
				image->getRect((unsigned char*)bufferUS, i, yBlockSize, 0, xBlockSize);
				if (convertFrom16To8Bit)
					copyData(bufferUS,bufferUC,xBlockSize*inputBands,0,yBlockSize,0,1,1,lt,false);
			}
			else
				image->getRect(bufferUC, i, yBlockSize, 0, xBlockSize);


			for (int band =0; band < inputBands; band++)
				rasterBands[band]->RasterIO(GF_Write, 
											0, 
											i, 
											xBlockSize, 
											yBlockSize, 
											bufferUC == NULL ? (unsigned char*) (bufferUS + band) :(unsigned char*) (bufferUC + band),
											xBlockSize,
											yBlockSize, 
											(GDALDataType)this->sizeofpixel, 
											offsetInWriteBuffer*this->sizeofpixel,
											0 );
		
			if (this->listener != NULL)
			{
				double per = (float)i / (float) this->nRows;
				this->listener->setProgressValue(per*this->listenerMaxValue);
			}
		
		}
	}

	if (bufferUC != NULL)
		delete [] bufferUC;
	if (bufferUS != NULL)
		delete [] bufferUS;

	return true;
}





bool CGDALFile::readFileInformation()
{
	if (!this->bHeaderRead)
	{
		this->readHeader();
		if (this->gdal)
			GDALClose(this->gdal);

		this->gdal = NULL;
	}

	return this->bHeaderRead;
}



void CGDALFile::readLookupTableInfo()
{
	GDALColorTable* table = this->gdal->GetRasterBand(1)->GetColorTable();

	int entrycount = table->GetColorEntryCount();

	if (this->lut) delete this->lut;
	this->lut = new CLookupTable ( this->bpp, 3);

	int rval, bval, gval;

	int lutsize = this->lut->getMaxEntries();

	for (int i = 0; i < entrycount; i++ )
	{
		const GDALColorEntry* color = table->GetColorEntry(i);

		rval = (int)color->c1;
		gval = (int)color->c2;
		bval = (int)color->c3;
		
		this->lut->setColor(rval, gval, bval, i);
	}
}


void CGDALFile::writeLookupTableInfo()
{
	if ( this->lut )
	{
		GDALColorTable gdallut = GDALColorTable(GPI_Gray);

		int entrycount = this->lut->getMaxEntries();
		int rval, bval, gval;
		GDALColorEntry gdalentry;

		for ( int i =0; i < entrycount; i++)
		{
			this->lut->getColor(rval, bval, gval, i);

			gdalentry.c1 = rval;
			gdalentry.c2 = gval;
			gdalentry.c3 = bval;
			gdalentry.c4 = 0;

			gdallut.SetColorEntry(i, &gdalentry); 

		}

		int hmm =9;


		for (int i = 0; i < gdallut.GetColorEntryCount(); i++ )
		{
			const GDALColorEntry* color = gdallut.GetColorEntry(i);

			rval = (int)color->c1;
			gval = (int)color->c2;
			bval = (int)color->c3;
		}
	}
}

float CGDALFile::getNoDataValue(int band)
{
	if ( band == 1 )
		return this->NoData1;
	else if ( band == 2 )
		return this->NoData2;
	else if ( band == 3 )
		return this->NoData3;
	else if ( band == 4 )
		return this->NoData4;
	else return -9999.0;
}




bool CGDALFile::readTileWithModifiedJPEGLib(int tileCol, int tileRow,void* buffer)
{
	// compute size of the tile to be read
	int blockSizeDecoded = this->sizeofpixel * this->tileHeight * this->tileWidth;

	if (!this->needsModifiedJPEGLib || !this->tif)
		READ_ERROR(buffer,blockSizeDecoded);

	// save pointer of original jpeg function
	if (!original_tif_predecode)
		original_tif_predecode = this->tif->tif_predecode;

	if (!original_tif_predecode)
		READ_ERROR(buffer,blockSizeDecoded);

	TIFFDirectory *td = &this->tif->tif_dir;

	// overwrite the current preDecode function with our own
	this->tif->tif_predecode = modifiedJPEGPreDecode;

	
	// compute the id of the tile 
	int nBlocksPerRow = this->nCols/ this->tileWidth;
    int tileID = tileCol + tileRow * nBlocksPerRow;
	int blockSizeEncoded = td->td_stripbytecount[tileID];
	
// todo: compute right size for strip reading!

	int ret=0;
	if( TIFFIsTiled(this->tif))
		ret = TIFFReadEncodedTile(this->tif,tileID,buffer,blockSizeDecoded);
	else
		ret = TIFFReadEncodedStrip(this->tif,tileID,buffer,blockSizeDecoded);

	// we couldn't read the tile
	if( ret == -1 )
		READ_ERROR(buffer,blockSizeDecoded);

	return true;
}


//############################################################
// modified tif library functions

static int modifiedJPEGPreDecode(TIFF* tif, tsample_t s)
{
//	The original function will fail when comparing the tif depth (16 bit) with the jpeg depth (8 bit).
//	To fix the problem without changing the tif library we change the tif depth temporarily.
//

	TIFFDirectory *td = &tif->tif_dir;
	td->td_bitspersample = 8;
	int ret = (*original_tif_predecode)(tif,s);
	td->td_bitspersample =16;

	if (!ret)
		return ret;

	// we can't overwrite the jpeg functions earlier because the tif_data pointer isn't 
	// initialized earlier

	// cast the pointer the get the j_decompress_ptr
	j_decompress_ptr decompressor = (j_decompress_ptr)tif->tif_data;
	
	if (!decompressor->is_decompressor)
		return 0;
	
	// for these kind of images we use the modified version of the functions
	// this allows us still to read normal jpeg files with the original functions
	decompressor->main->process_data;// = modified_process_data_simple_main;

	return 1;
}


