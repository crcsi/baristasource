/** 
 * @class CRAWFile
 * @author Jochen Willneff
 * @version 1.0 
 * @date 27-july-2007
 */

#include "RAWFile.h"
#include "ImageTile.h"
#include "ImgBuf.h"

#include "SystemUtilities.h"

/** 
  * Construct the CRAWFile with the given file name
  * no io is performed...
  *
  */


CRAWFile::CRAWFile(const char* filename, float Zoffset, float scale) : CImageFile(eRAWFile,filename),linedata(NULL), bline(NULL), gline(NULL), rline(NULL)
				   , nline(NULL), scanLineBuffer(NULL), fin(NULL), readfromEnd(false)
{
	this->scale = scale;
	this->Z0 = Zoffset;

	strcpy(this->filename, filename);
	this->listenerMaxValue = 0;
}


/**
 * standard destructor
 *  
 */
CRAWFile::~CRAWFile()
{
    if(this->scanLineBuffer != NULL)
	{
        delete [] this->scanLineBuffer;
		this->scanLineBuffer = NULL;
	}
    if(this->linedata != NULL)
	{
        delete [] this->linedata;
		this->linedata = NULL;
	}
    if(this->rline != NULL)
	{
        delete [] this->rline;
		this->rline = NULL;
	}
    if(this->bline != NULL)
	{
        delete [] this->bline;
		this->bline = NULL;
	}
    if(this->gline != NULL)
	{
        delete [] this->gline;
		this->gline = NULL;
	}
    if(this->nline != NULL)
	{
        delete [] this->nline;
		this->nline = NULL;
	}
	
	if ( this->fin ) fclose(this->fin);
	this->fin = NULL;


}

bool CRAWFile::open()
{
	this->fin = fopen(this->filename, "rb");
	if (!fin)
		return false;
	else return true;
	
}

bool CRAWFile::readHeader()
{
	FILE* fp  = fopen(this->filename, "r");

	if (!fp)
	{
		return false;
	}

	// read first character and try to decide type of file
	char buf[5];
	fread(&buf, sizeof(char), 4, fp);
	buf[4] = '\0';
	fclose(fp);

	CCharString typekey(buf);

	if (typekey.CompareNoCase("DSBB"))
		this->bHeaderRead = this->readSurfer6Header();
	else if (typekey.CompareNoCase("DSRB"))
		this->bHeaderRead = this->readSurfer7Header();
	else
		this->bHeaderRead = false;

	return this->bHeaderRead;
}

bool CRAWFile::readSurfer7Header()
{
	FILE* fgrd = fopen(this->filename, "rb");

	if ( !fgrd) 
	{
		fclose(fgrd);
		return false;
	}

	long dummy = 0;
	fread(&dummy, sizeof(long), 1, fgrd);
	fread(&dummy, sizeof(long), 1, fgrd);

	long version = 0;
	fread(&version, sizeof(long), 1, fgrd);

	// 9 long (32) + 8 double (64) = 800 bit = 100 bytes
	if ( version == 1 ) this->HeaderSize = 100; 

	fread(&dummy, sizeof(long), 1, fgrd);
	fread(&dummy, sizeof(long), 1, fgrd);
	fread(&this->nRows, sizeof(long), 1, fgrd);
	fread(&this->nCols, sizeof(long), 1, fgrd);
	fread(&this->xmin, sizeof(double), 1, fgrd);
	fread(&this->ymin, sizeof(double), 1, fgrd);
	fread(&this->gridx, sizeof(double), 1, fgrd);
	fread(&this->gridy, sizeof(double), 1, fgrd);
	fread(&this->zmin, sizeof(double), 1, fgrd);
	fread(&this->zmax, sizeof(double), 1, fgrd);

	double dval;
	fread(&dval, sizeof(double), 1, fgrd);
	fread(&dval, sizeof(double), 1, fgrd);
	this->NoDataValue = (float)dval;
	this->toptobottom = true;

	this->readfromEnd = true;

	this->filebpp = 64;
	this->bpp = 32;

	this->setSizeofDestPixel();
	this->setSizeofFilePixel();

	this->setBands(1);

	this->BigEndian = false;


	this->createTFW();

	fclose(fgrd);
	return true;

	/*
	0x42525344 long Tag: Id for Header section
	4 long Tag: Size of Header section
	1 long Header Section: Version
	0x44495247 long Tag: ID indicating a grid section
	72 long Tag: Length in bytes of the grid section
	5 long Grid Section: nRow
	10 long Grid Section: nCol
	0.0 double Grid Section: xLL
	0.0 double Grid Section: yLL
	1.0 double  Grid Section: xSize
	1.75 double Grid Section: ySize
	25.0 double Grid Section: zMin
	101.6 double Grid Section: zMax
	0.0 double Grid Section: Rotation
	1.70141e38 double Grid Section: BlankValue
	0x41544144 long Tag: ID indicating a data section
	400 long Tag: Length in bytes of the data section (5 rows x 10 columns x 8 bytes per double)
	Z11, Z12,  double Data Section: First (lowest) row of matrix. 10 doubles
	Z21, Z22,  double Data Section: Second row of matrix. 10 doubles
	Z31, Z32,  double Data Section: Third row of matrix. 10 doubles
	Z41, Z42,  double Data Section: Fourth row of matrix. 10 doubles
	Z51, Z52,   Data Section: Fifth row of matrix. 10 doubles
	 */
}

bool CRAWFile::readSurfer6Header()
{
	FILE* fgrd = fopen(this->filename, "rb");

	if ( !fgrd) 
	{
		fclose(fgrd);
		return false;
	}

	char dchar[1024];
	long dummy = 0;
	fread(&dchar, sizeof(char), 4, fgrd);
	fread(&this->nCols, sizeof(short), 1, fgrd);
	fread(&this->nRows, sizeof(short), 1, fgrd);
	fread(&this->xmin, sizeof(double), 1, fgrd);
	fread(&this->xmax, sizeof(double), 1, fgrd);
	fread(&this->ymin, sizeof(double), 1, fgrd);
	fread(&this->ymax, sizeof(double), 1, fgrd);
	fread(&this->zmin, sizeof(double), 1, fgrd);
	fread(&this->zmax, sizeof(double), 1, fgrd);


	this->gridx = (this->xmax - this->xmin)/(this->nCols -1);
	this->gridy = (this->ymax - this->ymin)/(this->nRows -1);

	this->createTFW();

	this->NoDataValue = 1.7014100e+38f;
	this->toptobottom = false;

	this->readfromEnd = false;
	this->filebpp = 32;
	this->bpp = 32;
	this->setBands(1);

	this->setSizeofDestPixel();
	this->setSizeofFilePixel();

	// 4 char (8), 2 short (16), 6 double (64) = 448 bit = 56 bytes
	this->HeaderSize = 56;

	this->BigEndian = false;

	fclose(fgrd);
	return true;

	/*
	id char 4 byte identification string DSBB which identifies the file as a Surfer 6 binary grid file.
	nx short number of grid lines along the X axis (columns)
	ny short number of grid lines along the Y axis (rows)
	xlo double minimum X value of the grid
	xhi double maximum X value of the grid
	ylo double minimum Y value of the grid
	yhi double maximum Y value of the grid
	zlo double minimum Z value of the grid
	zhi double maximum Z value of the grid
	z11, z12,  float first row of the grid. 
	Each row has a constant Y coordinate. 
	The first row corresponds to ylo, and the last row corresponds to yhi. 
	Within each row, the Z values are ordered from xlo to xhi.
	z21, z22,  float second row of the grid
	z31, z32,  float third row of the grid
	 float all other rows of the grid up to yhi
	 */
}

/**
 * gets the image width
 * @return the width
 */
int CRAWFile::getWidth()
{
	return this->nCols;
}

/**
 * gets the image height
 * @return the height
 */
int CRAWFile::getHeight()
{
	return this->nRows;
}

/**
 * @return the number of bits per pixel
 */
int CRAWFile::getBpp()
{
	return this->bpp;
}

/**
 * @return the number of bands (eg RGB = 3)
 */
int CRAWFile::bands()
{
	return this->nBands;
}

/**
 * gets the gridx
 * @return gridx
 */
double CRAWFile::getgridX()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->gridx;
}

/**
 * gets the gridy
 * @return gridy
 */
double CRAWFile::getgridY()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->gridy;
}

/**
 * gets the xmin
 * @return xmin
 */
double CRAWFile::getminX()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->xmin;
}

/**
 * gets the ymin
 * @return ymin
 */
double CRAWFile::getminY()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->ymin;
}



/**
 * Reads one scan line into buffer.
 *
 * @param buffer the destination buffer
 * @param iRow the row to read
 * @return false for failure
 */
bool CRAWFile::readLine(unsigned char* buffer, int iRow)
{
	if ( !this->fin )
	{
		if ( this->open() )
		{
			// this might be used if fin is found closed
			// calculate fin offset

			//file_offset offset = this->computeFileOffset(iRow);
			//CRAWFile::SEEK(this->fin, &offset);

			//file_offset coffset;
			//CRAWFile::TELL(this->fin, &coffset);


			// set fin to start of data
			file_offset offset = this->computeFileOffset(iRow);
			CRAWFile::SEEK(this->fin, &offset);
		}
		else return false;
	}

	if ( this->readfromEnd )
	{
		// set fin for reading from the bottom
		file_offset offset = this->computeFileOffset(iRow);
		CRAWFile::SEEK(this->fin, &offset);
	}


	if (iRow >= this->nRows)
		return false;

	if ( this->linedata == NULL)
	{
		this->linedata = (unsigned char*) new int[this->nCols*this->nBands];
	}

	if ( this->nBands > 1 )
	{
		if ( this->rline == NULL)
		{
			this->rline = (unsigned char*) new int[this->nCols];
		}
		if ( this->gline == NULL)
		{
			this->gline = (unsigned char*) new int[this->nCols];
		}
		if ( this->bline == NULL)
		{
			this->bline = (unsigned char*) new int[this->nCols];
		}
		if ( this->nline == NULL && this->bands() == 4)
		{
			this->nline = (unsigned char*) new int[this->nCols];
		}
	}


	if (this->nBands == 3)
	{
		return this->read3BandLine(buffer, iRow);
	}
	else if (this->nBands == 4)
	{
		return this->read4BandLine(buffer, iRow);
	}
	else if (this->nBands == 1)
	{
		return this->read1BandLine(buffer, iRow);
	}
	
	return true;
}

bool CRAWFile::read1BandLine(unsigned char* buffer, int iRow)
{
	if ( !this->fin ) return false;

	if (this->scanLineBuffer == NULL)
	{
		this->scanLineBuffer = (unsigned char*) new int[this->nCols];
	}

	if (this->filebpp == 8)
	{
		if ( this->bpp == 8 )
		{
			unsigned char Z;
			
			for(int j = 0; j < this->nCols; j++)
			{
				fread(&Z, sizeof(unsigned char), 1, this->fin);
				buffer[j] = Z;
			}
		}
		else if ( this->bpp == 16 )
		{
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;
			unsigned short Z;
			float cleanZ = DEM_NOHEIGHT;
			float Zval = DEM_NOHEIGHT;


			for(int j = 0; j < this->nCols; j++)
			{
				fread(&Z, sizeof(unsigned short), 1, this->fin);

				if ( this->BigEndian)
				{
					char* dummy = (char*)&Z;
					int stop =9;
					// swap the order of the bytes
					char a = dummy[0];
					dummy[0] = dummy[1];
					dummy[1] = a;
				}

				shortScanLineBuffer[j] = (unsigned short)(Z);
			}
			memcpy(buffer, shortScanLineBuffer, this->nCols*this->sizeofDestPixel);
		}
		else if ( this->bpp == 32 )
		{
			float* floatLineBuffer = (float*)this->scanLineBuffer;
			unsigned char Z;

			for(int j = 0; j < this->nCols; j++)
			{
				fread(&Z, sizeof(unsigned char), 1, this->fin);

				if ( this->BigEndian)
				{
					char* dummy = (char*)&Z;
					int stop =9;
					// swap the order of the bytes
					char a = dummy[0];
					dummy[0] = dummy[1];
					dummy[1] = a;
				}

				floatLineBuffer[j] = (float)(Z);
			}
			memcpy(buffer, floatLineBuffer, this->nCols*this->sizeofDestPixel);
		}
		else
			return false;


	}
	if (this->filebpp == 16)
	{
		if ( this->bpp == 16 )
		{
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;

			unsigned short uZ;
			signed short Z;
			float Zval = DEM_NOHEIGHT;

			for(int j = 0; j < this->nCols; j++)
			{
				if ( this->interpretation )
				{
					fread(&Z, sizeof(signed short), 1, this->fin);
					if ( this->BigEndian)
					{
						char* dummy = (char*)&Z;
						// swap the order of the bytes
						char a = dummy[0];
						dummy[0] = dummy[1];
						dummy[1] = a;
					}
					shortScanLineBuffer[j] = (unsigned short) Z;
				}
				else
				{
					fread(&uZ, sizeof(unsigned short), 1, this->fin);
					if ( this->BigEndian)
					{
						char* dummy = (char*)&uZ;
						// swap the order of the bytes
						char a = dummy[0];
						dummy[0] = dummy[1];
						dummy[1] = a;
					}
					shortScanLineBuffer[j] = uZ;
				}
			}
			
			memcpy(buffer, shortScanLineBuffer, this->nCols*this->sizeofDestPixel);
		}
		else if ( this->bpp == 32 )
		{
			float* floatLineBuffer = (float*)this->scanLineBuffer;
	
			unsigned short uZ;
			signed short Z;

			for(int j = 0; j < this->nCols; j++)
			{
				if ( this->interpretation )
				{
					fread(&Z, sizeof(signed short), 1, this->fin);
					if ( this->BigEndian)
					{
						char* dummy = (char*)&Z;
						// swap the order of the bytes
						char a = dummy[0];
						dummy[0] = dummy[1];
						dummy[1] = a;
					}
					floatLineBuffer[j] = (float)(Z);
				}
				else
				{
					fread(&uZ, sizeof(unsigned short), 1, this->fin);
					if ( this->BigEndian)
					{
						char* dummy = (char*)&uZ;
						// swap the order of the bytes
						char a = dummy[0];
						dummy[0] = dummy[1];
						dummy[1] = a;
					}

					floatLineBuffer[j] = (float)(uZ);
				}
			}
			
			memcpy(buffer, floatLineBuffer, this->nCols*this->sizeofDestPixel);
		}
		else
			return false;
	}
	if (this->filebpp == 32)
	{
		if ( this->bpp == 32 )
		{
			float* floatLineBuffer = (float*)this->scanLineBuffer;
	
			float tZ;
			float Z = DEM_NOHEIGHT;

			for(int j = 0; j < this->nCols; j++)
			{
				if ( this->interpretation )
					fread(&Z, sizeof(float), 1, this->fin);
				else
				{
					fread(&tZ, sizeof(float), 1, this->fin);
					//Z = (unsigned char)tZ;
					Z = (unsigned char)tZ;
				}

				if ( this->BigEndian)
				{
					char* dummy = (char*)&Z;
					// swap the order of the bytes
					char a = dummy[0];
					dummy[0] = dummy[1];
					dummy[1] = a;
				}
				
				floatLineBuffer[j] = (float)(Z);
			}
			
			memcpy(buffer, floatLineBuffer, this->nCols*this->sizeofDestPixel);
		}
	}
	if (this->filebpp == 64)
	{
		float* floatLineBuffer = (float*)this->scanLineBuffer;

		double Z;

		for(int j = 0; j < this->nCols; j++)
		{
			fread(&Z, sizeof(double), 1, this->fin);

			floatLineBuffer[j] = (float)(Z);
		}
		
		memcpy(buffer, floatLineBuffer, this->nCols*this->sizeofDestPixel);
	}


	return true;	
}




bool CRAWFile::read3BandLine(unsigned char* buffer, int iRow)
{
	if (this->bpp == 8)
	{
		unsigned char val;

		if ( this->interleaved )
		{
			for (int c = 0; c < this->nCols; c++)
			{
				fread(&val, sizeof(unsigned char), 1, this->fin);
				this->linedata[4*c]   = val;

				fread(&val, sizeof(unsigned char), 1, this->fin);
				this->linedata[4*c+1] = val;

				fread(&val, sizeof(unsigned char), 1, this->fin);
				this->linedata[4*c+2] = val;
				this->linedata[4*c+3] = 0;
			}
		}
		else
		{
			file_offset offset = this->HeaderSize + this->sizeofFilePixel*iRow*this->nCols;
			CRAWFile::SEEK(this->fin, &offset);
			for (int c = 0; c < this->nCols; c++)
			{
				fread(&val, sizeof(unsigned char), 1, this->fin);
				this->rline[c]   = val;
			}

			offset = this->HeaderSize + this->sizeofFilePixel*(iRow*this->nCols + this->nCols*this->nRows);
			CRAWFile::SEEK(this->fin, &offset);	
			for (int c = 0; c < this->nCols; c++)
			{
				fread(&val, sizeof(unsigned char), 1, this->fin);
				this->gline[c]   = val;
			}

			offset = this->HeaderSize + this->sizeofFilePixel*(iRow*this->nCols + 2*this->nCols*this->nRows);
			CRAWFile::SEEK(this->fin, &offset);
			for (int c = 0; c < this->nCols; c++)
			{
				fread(&val, sizeof(unsigned char), 1, this->fin);
				this->bline[c]   = val;
			}

			for (int c = 0; c < this->nCols; c++)
			{
				this->linedata[4*c]   = this->rline[c];
				this->linedata[4*c+1]   = this->gline[c];
				this->linedata[4*c+2]   = this->bline[c];
				this->linedata[4*c+3] = 0;
			}
		}

		
		memcpy(buffer, this->linedata, 4*this->nCols*this->sizeofDestPixel);
	}
	else if (this->bpp == 16)
	{
		unsigned short val;
		unsigned short* shortScanLineBuffer = (unsigned short*)this->linedata;
		for (int c = 0; c < this->nCols; c++)
		{
			fread(&val, sizeof(unsigned short), 1, this->fin);
			shortScanLineBuffer[4*c]   = val;

			fread(&val, sizeof(unsigned short), 1, this->fin);
			shortScanLineBuffer[4*c+1] = val;

			fread(&val, sizeof(unsigned short), 1, this->fin);
			shortScanLineBuffer[4*c+2] = val;
			shortScanLineBuffer[4*c+3] = 0;
		}
		
		memcpy(buffer, shortScanLineBuffer, 4*this->nCols*this->sizeofDestPixel);
	}

	return true;	
}

bool CRAWFile::read4BandLine(unsigned char* buffer, int iRow)
{
	if (this->bpp == 8)
	{
		unsigned char val;
		for (int c = 0; c < this->nCols; c++)
		{
			fread(&val, sizeof(unsigned char), 1, this->fin);
			this->linedata[4*c]   = val;

			fread(&val, sizeof(unsigned char), 1, this->fin);
			this->linedata[4*c+1] = val;

			fread(&val, sizeof(unsigned char), 1, this->fin);
			this->linedata[4*c+2] = val;

			fread(&val, sizeof(unsigned char), 1, this->fin);
			this->linedata[4*c+3] = val;
		}
		
		memcpy(buffer, this->linedata, 4*this->nCols*this->sizeofDestPixel);
	}
	else if (this->bpp == 16)
	{
		unsigned short val;
		unsigned short* shortScanLineBuffer = (unsigned short*)this->linedata;
		for (int c = 0; c < this->nCols; c++)
		{
			fread(&val, sizeof(unsigned short), 1, this->fin);
			shortScanLineBuffer[4*c]   = val;

			fread(&val, sizeof(unsigned short), 1, this->fin);
			shortScanLineBuffer[4*c+1] = val;

			fread(&val, sizeof(unsigned short), 1, this->fin);
			shortScanLineBuffer[4*c+2] = val;

			fread(&val, sizeof(unsigned short), 1, this->fin);
			shortScanLineBuffer[4*c+3] = val;
		}
		
		memcpy(buffer, shortScanLineBuffer, 4*this->nCols*this->sizeofDestPixel);
	}

	return true;
}

bool CRAWFile::write(const char* filename, CBaseImage* image)
{
	return false;
}


void CRAWFile::setSizeofFilePixel() 
{ 
	if ( this->filebpp == 8)
	{
		this->sizeofFilePixel = 1;
	}
	else if ( this->filebpp == 16)
	{
		this->sizeofFilePixel = 2;
	}
	else if ( this->filebpp == 32)
	{
		this->sizeofFilePixel = 4;
	}
	else if ( this->filebpp == 64)
	{
		this->sizeofFilePixel = 8;
	}

}

void CRAWFile::setSizeofDestPixel() 
{ 
	if ( this->bpp == 8)
	{
		this->sizeofDestPixel = 1;
	}
	else if ( this->bpp == 16)
	{
		this->sizeofDestPixel = 2;
	}
	else if ( this->bpp == 32)
	{
		this->sizeofDestPixel = 4;
	}
	else if ( this->bpp == 64)
	{
		this->sizeofDestPixel = 4;
	}

}

void CRAWFile::createTFW()
{
	if (this->tfw) delete this->tfw;

	// create TFW
	this->tfw = new CTFW();
	// Get TFW information
	this->tfw->setFileName("TFWInfo");
	C2DPoint shift(this->xmin, this->ymin + (this->nRows -1) * this->gridy);
	C2DPoint gridding(this->gridx, this->gridy);
	this->tfw->setFromShiftAndPixelSize(shift, gridding);
}


file_offset CRAWFile::computeFileOffset(int row)
{
	file_offset offset;

	// calculate fin offset
	if ( !this->readfromEnd)
	{
		offset = this->HeaderSize + this->bands()*this->sizeofFilePixel*this->nCols*row;
	}
	else
	{
		offset = this->HeaderSize +  this->bands()*this->sizeofFilePixel*this->nCols*(this->nRows -1 - row);
	}
	
	return offset;

}

/**
 * wrapper for seek to work with > 32 bit file pointers
 */
void CRAWFile::SEEK(FILE* fp, file_offset* offset)
{
#ifdef WIN32
    fsetpos(fp, offset);
#else
    fseeko(fp, *offset, SEEK_SET); // linux
#endif
}


/**
 * wrapper for tell to work with > 32 bit file pointers
 */
void CRAWFile::TELL(FILE* fp, file_offset* offset)
{
#ifdef WIN32
    fgetpos(fp, offset);
#else
    *offset = ftello(fp); // linux
#endif
}

