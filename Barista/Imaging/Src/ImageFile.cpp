/** @class CImageFile
 * Abstract image file class
 * 
 * @author Harry Hanley
 * @version 1.0 
 * @date 20-jan-2005
 */
 
#include "ImageFile.h"
#include "string.h"
#include "LookupTable.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/**
 * Default constructor
 */
CImageFile::CImageFile(eImageFileType imageType): nRows(-1), nCols(-1), nBands(-1), bpp(-1), nextLineToWrite(0),
			lut(0), colorIndexed(false), Z0(0), scale(1.0), sigmaZ(0.5),hasColourInformations(false),
			interleaved(true), BigEndian(false), interpretation(true), toptobottom(true),type(imageType),
			colorChannel1(0),colorChannel2(1),colorChannel3(2),colorChannel4(3),tiled(false),tfw(NULL)
{
    strcpy(this->filename, "unknown");
	this->hugeFileName = filename;
}


CImageFile::CImageFile(eImageFileType imageType,const char* filename): nRows(-1), nCols(-1), nBands(-1), bpp(-1), nextLineToWrite(0),
			lut(0), colorIndexed(false), Z0(0), scale(1.0), sigmaZ(0.5),hasColourInformations(false),
			interleaved(true), BigEndian(false), interpretation(true), toptobottom(true),type(imageType),
			colorChannel1(0),colorChannel2(1),colorChannel3(2),colorChannel4(3),tiled(false),tfw(NULL)
{
    if (filename)
		strcpy(this->filename, filename);
	else
		strcpy(this->filename, "unknown");

	this->hugeFileName = filename;
}

CImageFile::CImageFile(const CImageFile& src) : type(src.type), nRows(src.nRows), nCols(src.nCols), nBands(src.nBands), bpp(src.bpp), nextLineToWrite(src.nextLineToWrite),
			colorIndexed(src.colorIndexed), Z0(src.Z0), scale(src.scale), sigmaZ(src.sigmaZ),hasColourInformations(src.hasColourInformations),
			interleaved(src.interleaved), BigEndian(src.BigEndian), interpretation(src.interpretation), toptobottom(src.toptobottom),
			colorChannel1(src.colorChannel1),colorChannel2(src.colorChannel2),colorChannel3(src.colorChannel3),colorChannel4(src.colorChannel4),
			rpc(src.rpc),tiled(src.tiled),tfw(NULL),lut(NULL)
{
	if (src.lut)
		this->lut = new CLookupTable(*lut);

	if (src.tfw)
		this->tfw = new CTFW(*src.tfw);

    if (src.filename)
		strcpy(this->filename, src.filename);
	else
		strcpy(this->filename, "unknown");
	
	this->hugeFileName = filename;

}




CImageFile::~CImageFile()
{
	if (this->lut) delete this->lut;
	this->lut = NULL;

	if (this->tfw )	delete this->tfw;
	
	this->tfw = NULL;
}

/**
 * @return the  image width
 */
int CImageFile::getWidth()
{
	return this->nCols;
}

/**
 * @return the  image height
 */
int CImageFile::getHeight()
{
	return this->nRows;
}

/**
 * sets the nCols field
 */
void CImageFile::setWidth(int w)
{
	this->nCols = w;
}

/**
 * sets the nRows field
 */
void CImageFile::setHeight(int h)
{
 this->nRows = h;
}

/**
 * sets the bpp field
 */
void CImageFile::setBpp(int bpp)
{
    this->bpp = bpp;
}

/**
 * sets the nBands field
 */
void CImageFile::setBands(int bands)
{
    this->nBands = bands;
}

/**
 * @return the value of the bpp field
 */
int CImageFile::getBpp()
{
    return this->bpp;
}

/**
 * @return the value of the nBands field
 */
int CImageFile::bands()
{
    return this->nBands;
}

/**
 * virtual function the read a scanline
 */
bool CImageFile::readLine(unsigned char* pBuffer, int iRow)
{
    return false;
}

/**
 * virtual function the write a scanline
 */
bool CImageFile::writeLine(unsigned char* pBuffer, int iRow)
{
    return false;
}

/**
 * @return the filename field
 */ 
char* CImageFile::getFilename()
{
    return this->filename;
}


int CImageFile::getColourInterpretation(int channel)
{
	if (channel == 0)
		return this->colorChannel1;
	else if (channel == 1)
		return this->colorChannel2;
	else if (channel == 2)
		return this->colorChannel3;
	else if (channel == 3)
		return this->colorChannel4;
	else
		return 0;
}


void CImageFile::setColourInformations(int channel1,int channel2,int channel3,int channel4)
{
	this->hasColourInformations = true;
	
	this->colorChannel1 = channel1;
	this->colorChannel2 = channel2;
	this->colorChannel3 = channel3;
	this->colorChannel4 = channel4;

}

CFileName CImageFile::getHugeFileName()const
{
	return this->hugeFileName;
}

void CImageFile::setHugeFileName(CFileName filename)
{
	this->hugeFileName = filename;
}


void CImageFile::setTFW(const CTFW* externalTfw)
{
	if (this->tfw) delete this->tfw;
	this->tfw = NULL;
	
	if (externalTfw) 
		this->tfw = new CTFW(*externalTfw);
}

void CImageFile::setLookupTable(const CLookupTable* externalLut)
{
	if (this->lut) delete this->lut;
	this->lut = NULL;
	this->colorIndexed = false;	

	if (externalLut) 
	{
		this->lut = new CLookupTable(*externalLut);
		this->colorIndexed = true;	
	}

}
