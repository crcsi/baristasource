/** 
 * @class CASCIIDEMFile
 * class for reading DEM data and fill DEM
 * 
 * @author Jochen Willneff
 * @version 1.0 
 * @date 20-mar-2006
 */

#include <vector>
#include <float.h>
#include <math.h>
//#include <QMessageBox>

#include "DEM.h"
#include "Filename.h"
#include "ASCIIDEMFile.h"


CASCIIDEMFile::~CASCIIDEMFile(void)
{
	if (this->zdata) delete [] this->zdata;
	this->zdata = NULL;

	this->close();
}

CASCIIDEMFile::CASCIIDEMFile(): CImageFile(eASCIIDEMFile),zdata(NULL),fp(NULL),
								bHeaderRead(false)
{
	
	this->setBands(1);
	this->setBpp(32);
}

/** 
  * Construct the CASCIIDEMFile with the given file name
  * no io is performed...
  *
  */
CASCIIDEMFile::CASCIIDEMFile(const char* filename, float noDataValue ):
	CImageFile(eASCIIDEMFile,filename),zdata(NULL),fp(NULL),
								bHeaderRead(false)
{
	this->NoDataValue = noDataValue;
	this->setBands(1);
	this->setBpp(32);

}


/**
 * private function to ensure the actual FILE* is closed
 *
 */
void CASCIIDEMFile::close()
{
	if (this->fp != NULL)
	{
		fclose(this->fp);
		this->fp = NULL;
	}
}

int CASCIIDEMFile::convertFromXYZ(const char* xyzfilename)
{
	FILE *fil;

	fil = fopen(xyzfilename, "r");

	if (!fil)
		return 1; // the input file could not be openned

	fseek(fil,0,SEEK_END); // set the file pointer at the end of the file (with offset 0)
	double fileSize= ftell(fil); // size of file from beginning to the file pointer (currently at the end, set by the previous instruction)
	fseek(fil,0,SEEK_SET); // set the file pointer at the beginning of the file (with offset 0)

	long len = 2048 * 2048;
	vector<float> xVec(len), yVec(len);
	vector<float> zVec(len);

	int i = 0;

	double x, y;
	float z;

	double xMin = FLT_MAX, yMin = FLT_MAX, xMax = -FLT_MAX, yMax = -FLT_MAX; //FLT_MAX = maximum float value
	double gridX = -1.0, gridY = -1.0;

	double deltaX, deltaY;
	char buffer[2000];

	if (fgets(buffer,1999,fil) != NULL)
	{
		sscanf(buffer, "%lf %lf %f", &x, &y, &z);
		deltaX = x; deltaY = y;
		xMin = xMax = x;
		yMin = yMax = y;
		x -= deltaX;
		y -= deltaY;
		xVec[i] = (float)x;
		yVec[i] = (float)y;
		zVec[i] = (float)z;
		i += 1;
	}
	else 
	{
		fclose(fil);
		return 2; // error in reading the input file
	}

	while (fgets(buffer,1999,fil) != NULL)
	{
		if (sscanf(buffer, "%lf %lf %f", &x, &y, &z) != 3)
			break;

		if (x < xMin) xMin = x;
		if (x > xMax) xMax = x;
		if (y < yMin) yMin = y;
		if (y > yMax) yMax = y;
		x -= deltaX;
		y -= deltaY;
		xVec[i] = (float)x;
		yVec[i] = (float)y;
		zVec[i] = (float)z;

		if ((gridX < 0) && (xVec[i] != xVec[i - 1])) 
			gridX = fabs(xVec[i] - xVec[i - 1]);
		if ((gridY < 0) && (yVec[i] != yVec[i - 1])) 
			gridY = fabs(yVec[i] - yVec[i - 1]);

		if ((gridX>0) && (fabs(xVec[i] - xVec[i - 1]) > 0))
		{			
			int diff = (int)fabs(xVec[i] - xVec[i - 1]);
			div_t divresult;
			divresult = div(diff,gridX);
			if (divresult.rem>0)
				return 3; // The input file does not represent an appropriate DEM grid
		}
		if ((gridY>0) && (fabs(yVec[i] - yVec[i - 1]) > 0))
		{			
			int diff = (int)fabs(yVec[i] - yVec[i - 1]);
			div_t divresult;
			divresult = div(diff,gridY);
			if (divresult.rem>0)
				return 3; // The input file does not represent an appropriate DEM grid
		}

		++i;
		
		if (i >= len)
		{
			len += 2048 * 2048;
			xVec.resize(len);
			yVec.resize(len);
			zVec.resize(len);
		}
		if (this->listener)
			this->listener->setProgressValue(((double)ftell(fil)/(double)fileSize)*80);
	};

	if (i < len)
	{
		for (int k =i; k < len; k++)
			zVec[k] = DEM_NOHEIGHT; // DEM_NOHEIGHT = no data value
	}

	fclose(fil);

	int rows = int(floor((yMax - yMin) / gridY) + 1);
	int cols = int(floor((xMax - xMin) / gridX) + 1);

	int length = rows * cols;

	if ( length <= 0 ) return 3; // The input file does not represent an appropriate DEM grid

	float *zBuf = new float[length];
	for (int j = 0; j < length; ++j) 
		zBuf[j] = DEM_NOHEIGHT;  // DEM_NOHEIGHT = no data value
    deltaX = xMin - deltaX; // consider top-left corner as origin
    deltaY = yMax - deltaY;

	for (int j = 0; j < i; ++j) 
	{
		int r = (int)(floor ((deltaY - yVec[j]) / gridY));
		int c = (int)(floor ((xVec[j] - deltaX) / gridX));
		zBuf[r * cols + c] = zVec[j];

	}

//////////////////////////////////////////////////////////
	this->nCols = cols;
	this->nRows = rows;
	this->gridx = gridX;
	this->gridy = gridY;
	this->xmin = xMin;
	this->ymin = yMin;
	this->xmax = xMax;
	this->ymax = yMax;

	this->createTFW();

	CFileName copy(xyzfilename);
	copy += ".txt";

	const char *out = copy.GetChar();
	fil = fopen(out, "w");

	fprintf(fil, "%d\n", this->nRows);
	fprintf(fil, "%d\n", this->nCols);
	fprintf(fil, "%.8f\n", this->gridx);
	fprintf(fil, "%.8f\n", this->gridy);
	fprintf(fil, "%.8f\n", this->xmin);
	fprintf(fil, "%.8f\n", this->ymin);

	char nlc = '\n';
	for (int r = 0; r < this->nRows; r++)
	{
		for (int c = 0; c < this->nCols; c++)
		{
		//	float value = zBuf[r * this->nCols + c];
		//	fprintf(fil, "%.3f%c", value == this->NoDataValue ? DEM_NOHEIGHT : value, ' ');
			fprintf(fil, "%.3f%c", zBuf[r * this->nCols + c], ' ');
		}
		fprintf(fil, "%c", nlc);

		if (this->listener)
		{
			this->listener->setTitleString("Writing converted ASCII DEM ...");		
			this->listener->setProgressValue(80.0 + ((double)(r)/(double)this->nRows)*20);
		}
	}

	fclose(fil);

	return 0; // 0: success in reading the input XYZ file
};

/**
 * function that reads the ASCIIDEM header
 *
 */
bool CASCIIDEMFile::readHeader()
{
	if (this->fp != NULL)
		this->close();

	this->fp = fopen(filename, "r");

	if (!fp)
		return false;

	// read first characters and try to decide type of file
	char buf[5];
	fread(&buf, sizeof(char), 4, this->fp);
	buf[4] = '\0';
	this->close();

	CCharString typekey(buf);

	if ( typekey.CompareNoCase("DSAA"))
		this->bHeaderRead =  this->readSurferHeader();
	else if (typekey.CompareNoCase("DSRB"))
		this->bHeaderRead = false;
	else if (typekey.CompareNoCase("DSBB"))
		this->bHeaderRead = false;
	else if (typekey.CompareNoCase("NCOL"))
		this->bHeaderRead = this->readARCInfoHeader();
	else
		this->bHeaderRead = this->readBaristaHeader();

	return this->bHeaderRead;
}


/**
 * gets the image width
 * @return the width
 */
int CASCIIDEMFile::getWidth()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->nCols;
}

/**
 * gets the image height
 * @return the height
 */
int CASCIIDEMFile::getHeight()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->nRows;
}

/**
 * gets the gridx
 * @return gridx
 */
double CASCIIDEMFile::getgridX()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->gridx;
}

/**
 * gets the gridy
 * @return gridy
 */
double CASCIIDEMFile::getgridY()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->gridy;
}

/**
 * gets the xmin
 * @return xmin
 */
double CASCIIDEMFile::getminX()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->xmin;
}

/**
 * gets the ymin
 * @return ymin
 */
double CASCIIDEMFile::getminY()
{
	if (!this->bHeaderRead)
		this->readHeader();

	return this->ymin;
}



/**
 * Reads one scan line into buffer.
 *
 * @param buffer the destination buffer
 * @param iRow the row to read
 * @return false for failure
 */
bool CASCIIDEMFile::readLine(unsigned char* buffer, int iRow)
{
	if (iRow >= this->nRows)
		return false;

	if (!this->bHeaderRead)
		this->readHeader();

	if (this->zdata == NULL)
	{
		this->zdata = new float[this->nCols];
	}
	
	float pix;


	for(int j = 0; j < this->nCols; j++)
	{
		fscanf(this->fp, "%f", &pix);

		//if ( pix == this->NoDataValue ) pix = DEM_NOHEIGHT;
		
		this->zdata[j] = pix;
	}

	memcpy(buffer, this->zdata, this->nCols*4);

	return true;
}

/**
 *  writes a ASCIIDEM file from a CBaseImage
 *
 * @param fileName the output file name
 * @param image the src image
 *
 */
bool CASCIIDEMFile::write(const char* filename, CBaseImage* image)
{
    this->nCols = image->getWidth();
    this->nRows = image->getHeight();

    return true;
}

bool CASCIIDEMFile::readBaristaHeader()
{
	if (this->fp != NULL)
		this->close();

	this->fp = fopen(filename, "r");

	if (!this->fp)
		return false;

////////////////
	char delimiters[2] = {' ','\t'};
	char string [1000];

	// read first six lines and check if they contains only one value
	for ( int k = 0; k < 6; k++ )
	{
		fgets(string,999,this->fp);

		int n = 0;
		while ( string[n] != 0 )
		{
			for ( int k = 0; k < 2; k++)
			{
				if ( string[n] == delimiters[k] )
				{
					// replace all types of delimters with ' '
					string[n] = delimiters[0];
					break;
				}
			}
			n++;
		}
			
		int delimiterCount = 0;
		bool onDilimiter = false;

		for ( int i = 0; i < n; i++ )
		{
			if ( string[i] == delimiters[0] )
			{
				if ( !onDilimiter) delimiterCount++;
				onDilimiter = true;
			}
			else onDilimiter = false;
		}

		// if line starts with delimiters, ignore first delimiter
		if ( string[0] == delimiters[0] ) delimiterCount--;

		// if line ends with delimiters, ignore last delimiter
		if ( string[n-2] == delimiters[0] ) delimiterCount--;

		if ( delimiterCount > 0 ) return false;
	}

	// if okay close/open to be back at start
	this->close();
	this->fp = fopen(filename, "r");

	if (!this->fp)
		return false;
///////////////

	fscanf(fp, "%d", &this->nRows);
	fscanf(fp, "%d", &this->nCols);
	fscanf(fp, "%lf", &this->gridx);
	fscanf(fp, "%lf", &this->gridy);
	fscanf(fp, "%lf", &this->xmin);
	fscanf(fp, "%lf", &this->ymin);

	if ( this->nRows <= 0 ) return false;
	if ( this->nCols <= 0 ) return false;
	if ( this->gridx <= 0 ) return false;
	if ( this->gridy <= 0 ) return false;

	this->createTFW();

	return true;
}

bool CASCIIDEMFile::readARCInfoHeader()
{
	if (this->fp != NULL)
		this->close();

	this->fp = fopen(filename, "r");

	if (!fp)
		return false;

	//ARCInfo ASCII header looks like this:
	//NCOLS 1451
	//NROWS 1451
	//XLLCORNER 67399.5
	//YLLCORNER 358649.5
	//CELLSIZE 1.
	//NODATA_VALUE -9999.

	char buf[1024];
	fscanf(fp, "%s %d", &buf, &this->nCols);
	fscanf(fp, "%s %d", &buf, &this->nRows);
	fscanf(fp, "%s %lf", &buf, &this->xmin);
	fscanf(fp, "%s %lf", &buf, &this->ymin);
	fscanf(fp, "%s %lf", &buf, &this->gridx);
	fscanf(fp, "%s %lf", &buf, &this->NoDataValue);

	this->gridx = this->gridy;

	this->createTFW();

	return true;
}

bool CASCIIDEMFile::readSurferHeader()
{
	if (this->fp != NULL)
		this->close();

	this->fp = fopen(filename, "r");

	if (!fp)
		return false;
	
	// Surfer ASCII Header looks like this:
	//DSAA
	//1071 1453
	//508150.8354 532761.5308
	//5239107.586 5272493.971
	//-138.25022468479 1263.3960095946

	// noDataValue from Surfer (not in header)  1.70141e38
	this->NoDataValue = (float)1.70141e38;
	this->toptobottom = false;

	char buf[1024];
	double dxmin;
	double dxmax;
	double dymin;
	double dymax;
	double dzmin;
	double dzmax;

	fscanf(fp, "%s", &buf);
	fscanf(fp, "%d", &this->nCols);
	fscanf(fp, "%d", &this->nRows);
	fscanf(fp, "%lf", &dxmin);
	fscanf(fp, "%lf", &dxmax);
	fscanf(fp, "%lf", &dymin);
	fscanf(fp, "%lf", &dymax);
	fscanf(fp, "%lf", &dzmin);
	fscanf(fp, "%lf", &dzmax);

	this->xmin = dxmin;
	this->ymin = dymin;
	this->gridx = (dxmax - dxmin)/(this->nCols -1);
	this->gridy = (dymax - dymin)/(this->nRows -1);

	this->createTFW();

	return true;
}

void CASCIIDEMFile::createTFW()
{
	if (this->tfw) delete this->tfw;

	// create TFW
	this->tfw = new CTFW();
	// Get TFW information
	this->tfw->setFileName("TFWInfo");
	C2DPoint shift(this->xmin,  this->ymin + (this->nRows -1) * this->gridy);
	C2DPoint gridding(this->gridx, this->gridy);
	this->tfw->setFromShiftAndPixelSize(shift, gridding);
}
