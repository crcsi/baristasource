/**
 * @class CHugeImage
 * Provides functionally for handling huge 50MB - many Gig image files.
 * Creates a *.hug file that is a tiled and pyramided image database.
 * The hug file has the same name as the image file with .hug appended
 *
 *
 * @author Harry Hanley
 * @version 1.0
 * @date 20-jan-2005
 *
 *
 */
#include <float.h>
#include "newmatap.h"
#include "SystemUtilities.h"

#include "HugeImage.h"
#include "ImageFile.h"
#include "ImageTile.h"
#include "string.h"
#include "histogram.h"
#include "XYContourArray.h"
#include "XYPointArray.h"
#include "math.h"
#include "Kernel.h"
#include "m_FeatExtract.h"
#include "ImgBuf.h"
#include "LabelImage.h"
#include "ImageRegion.h"
#include "statistics.h"
#include "XYEllipse.h"
#include "Trans2D.h"
#include "LabelImage.h"
#include "SystemUtilities.h"
#include "LookupTableHelper.h"
#include "histogramFlt.h"
#include "TransferFunctionFactory.h"
#include "IHSTransferFunction.h"
#include "WallisFilter.h"
//addition >>
//#ifndef FINAL_RELEASE
	#include <cv.h>
	#include <cxcore.h>
	//#include <highgui.h>
	using namespace cv;
//#endif
//<< addition

#define MAX_MEG 20000000
#define PAN_SHARPEN_FLOAT_LIMIT 500

int CHugeImage::tileCreationCount = 0;
int CHugeImage::totalTileCount = 0;

#define HUG_FILE_VERSION 1

/**
* Default constructor
*/
CHugeImage::CHugeImage(void) : pixelSize(1), tiles(0), resamplingType(eBilinear),
	transferFunction(0),intensityHistogram(0)
{
 //   this->imageFile = NULL;
    this->fp = NULL;

    //strcpy(this->filename, "");

	this->filename = "";

    this->version = HUG_FILE_VERSION;
    this->bpp = 0;
    this->bands = 0;
    this->width = 0;
    this->height = 0;
    this->tilesAcross = 0;
    this->tilesDown = 0;
    this->tileWidth = 0;
    this->tileHeight = 0;
    this->halfRes = NULL;
    this->doubleSize = NULL;
    this->nBufferedTiles = 0;
    this->lastBufferedTile = -1;

    this->headerOffset = 0;
    this->dataOffset = -1; // SIZE_OF_HEADER;   if (this->doubleSize == NULL)
    this->dataOffset = this->headerSize();

    this->listener = NULL;
	this->roi = NULL;
	this->Brightness = 0;

	this->RChannel = 0;
	this->GChannel = 1;
	this->BChannel = 2;

}

/**
 * destructor
 * recurses the link list of CHugeImages
 */
CHugeImage::~CHugeImage(void)
{
	this->closeHugeFile();

	if (this->halfRes != NULL)
    {
        delete this->halfRes;
        this->halfRes = NULL;
    }

    deleteTiles();
    deleteHistograms();
	deleteTransferFunction();

 /*   // only the top image will close the file
    if (this->doubleSize == NULL)
    {
        if (this->fp != NULL)
            fclose(this->fp);

        this->fp = NULL;
    }
*/
	if(this->roi!=NULL)
	{
		delete[] roi;
		roi=NULL;
	}


	if ( this->listener == NULL)
		this->listener = NULL;

}


void  CHugeImage::changeBrightness(int factor)
{
	if (this->transferFunction)
	{
		
		CHistogram* hist = NULL;
		if (this->getBands()==1)
		{
			hist = this->getHistogram();
			
		}
		else
		{
			hist = this->getIntensityHistogram();
		}

		if (hist)
		{
			this->Brightness += factor;
			this->transferFunction->changeBrightness(hist,this->Brightness);
		}
	}

}

int CHugeImage::getBrightness() const
{
	return this->Brightness;
}

double CHugeImage::getCoordOffset(int pyrLevel) const
{
	double coordOffset = 0.0;
	for (int i = 1; i <= pyrLevel; ++i) coordOffset += pow(2.0, (double) i - 2);
	return coordOffset;
};

/**
 * Opens the CHugeImage for reading. Retrieves the huge image data base.
 *
 * @param filename The filename of the source *.hug
 * @return true for success / false for failure
 */
bool CHugeImage::open(const char* filename, bool retrieve)
{
	bool b = true;

	this->setFileName(filename);

	// always close the file first before overwriting the pointer!
	// this alows us to delete the .hug file while barista is running
	if (this->fp != NULL)
	{
		fclose(this->fp);
		this->fp = NULL;
	}

    this->fp = fopen(this->filename, "r+b");
    if (this->fp == NULL)
    {
        b =  false;
    }
	else
	{
		// read the database
		if (retrieve)
		{
			b = this->retrieve(this->fp);


			if ( !b && this->fp != NULL)
			{
				fclose(this->fp);
				this->fp = NULL;
			}

			// init the transferFunction properly
			if (b)
			{
				// check if we have to init the transfer function
				if ((this->transferFunction && !this->transferFunction->isInitialized()) ||
					!this->transferFunction)
					this->initDefaultTransferFunction();
				else if (this->transferFunction && this->bands > 1)
				{
					
					// if we have a transfer function and more the 1 band, we also need the intensity histogram
					// first try reading the file from disk
					if (!this->readIntensityHistogram())				
					{
						// ok, then compute it
						int level = this->computeImageLevel(4000);
						this->computeIntensityHistogram(level);
					}
				}
			}
		}
		else
		{
			CHugeImage* halfHuge = (CHugeImage*)this->halfRes;

			while (halfHuge)
			{
				halfHuge->fp = this->fp;
				halfHuge = (CHugeImage*) halfHuge->halfRes;
			}
		}
	}

    return b;
}



bool CHugeImage::openOnly(const char* filename, int w, int h, int bpp, int bands)
{
	deleteHistograms();
    deleteTiles();

    CHugeImage::tileCreationCount = 0;
    this->width = w;
    this->height = h;
    this->bands = bands;
    this->bpp = bpp;
	this->pixelSize = this->pixelSizeBytes();

	// set image display to default
	this->setDefaultChannels();

    this->setFileName(filename);

	// always close the file first before overwriting the pointer!
	// this alows us to delete the .hug file while barista is running
	if (this->fp != NULL)
	{
		fclose(this->fp);
		this->fp = NULL;
	}

    this->fp = fopen(this->filename, "w+b");

    // sanity check
    if (this->fp == NULL)
        return false;

	if (this->doubleSize == NULL)
        this->dataOffset = this->headerSize();

	return true;
};


/**
 * Opens the CHugeImage for reading. Creates the huge image data base if
 * necessary, or just reopens an existing database
 *
 * @param imageFile A CImageFile object (must have been previously create)
 * @param forceRecreate forces recreation of huge image database
 * @return true for success / false for failuremakeQImage
 */
bool CHugeImage::open(CImageFile* imageFile, bool forceRecreate /*= false*/ )
{

	CHugeImage::tileCreationCount = 0;

    CHugeImage::totalTileCount = this->computeTotalTileCount( imageFile->getHeight(),imageFile->getWidth());

	this->setFileName(imageFile->getHugeFileName().GetChar());

	this->addHUGExtension();

    this->bpp = imageFile->getBpp();
    this->bands = imageFile->bands();
	this->pixelSize = this->pixelSizeBytes();

	// set image display to default
	this->setDefaultChannels();

    this->width = imageFile->getWidth();
    this->height = imageFile->getHeight();

    if (forceRecreate) // force recreation of database
    {

		// always close the file first before overwriting the pointer!
		// this alows us to delete the .hug file while barista is running
		if (this->fp != NULL)
		{
			fclose(this->fp);
			this->fp = NULL;
		}

		this->fp = fopen(this->filename, "w+b");

        // sanity check
        if (this->fp == NULL)
            return false;

		if (this->doubleSize == NULL)
			this->dataOffset = this->headerSize();

        this->create(imageFile);
    }
    else
    {

		// always close the file first before overwriting the pointer!
		// this alows us to delete the .hug file while barista is running
		if (this->fp != NULL)
		{
			fclose(this->fp);
			this->fp = NULL;
		}


        // is it there?
        this->fp = fopen(this->filename, "r+b");
        if (this->fp == NULL)
        {
			// no, create it
            this->fp = fopen(this->filename, "w+b");

            if (this->fp == NULL)
                return false;

            return this->create(imageFile);
        }
        else
        {
			// read the existing database
            bool b = this->retrieve(this->fp);

            if (!b && this->version == -1) // the hug needs recreated (wrong version)
            {
                this->version = HUG_FILE_VERSION;
                fclose(this->fp);
				this->fp = 0;
                bool ret = this->open(imageFile, true);
				return ret;
            }
            else
            {
				// ignore old lookup tables
				if (imageFile->getColorIndexed() && imageFile->getLookupTable())
				{
					this->setLookupTable(*imageFile->getLookupTable());
				}
				else
					this->initDefaultTransferFunction();
				CHugeImage::tileCreationCount = CHugeImage::totalTileCount;
                return true;
				
            }
        }
    }

    return false;
}

/**
 * Private function to read existing database
 * @param FILE* of database file
 * @return false for failure
 */
bool CHugeImage::retrieve(FILE* fp)
{
	this->fp = fp;
    // seek to start of headerdataOffset
//    fseek(fp, this->headerOffset, SEEK_SET);
    CHugeImage::SEEK(fp, &this->headerOffset);

    // these are the header fields
    fread(&this->version, sizeof(long), 1, fp);
    if (this->version != HUG_FILE_VERSION)
    {
        this->version = -1; // this is an indication that the hug file needs re-created
        return false;
    }

    fread(&this->bpp, sizeof(long), 1, fp);
    fread(&this->bands, sizeof(long), 1, fp);
    fread(&this->width, sizeof(long), 1, fp);
    fread(&this->height, sizeof(long), 1, fp);
    fread(&this->tilesAcross, sizeof(long), 1, fp);
    fread(&this->tilesDown, sizeof(long), 1, fp);
    fread(&this->tileWidth, sizeof(long), 1, fp);
    fread(&this->tileHeight, sizeof(long), 1, fp);


	this->pixelSize = this->pixelSizeBytes();

    // the offset to the first image is simply the size of the header
    if (this->doubleSize == NULL)
        this->dataOffset = this->headerSize();

    // is there a half res sub image?
    file_offset offset = 0;

    int hmm = sizeof(file_offset);
    fread(&offset, sizeof(file_offset), 1, fp);

	this->initHistograms();

	if (this->bands > 1 && !this->intensityHistogram)
		this->initIntensityHistogram();

	for (int i = 0; i < bands; i++)
    {
		this->Histograms[i]->readHistogram(this->fp);
		if (this->bpp != 8)
			this->Histograms[i]->updateMinMax(0.02,0.98);
    }

    initTiles(this->tilesAcross);

    if (offset != 0) // yes, read it!!!
    {
		CHugeImage* halfHuge = (CHugeImage*)this->halfRes;

		if (!this->halfRes)
		{
			this->halfRes = new CHugeImage();

			halfHuge = (CHugeImage*) this->halfRes;

			// we're the halfsize's doublesize image
			halfHuge->doubleSize = this;

			halfHuge->headerOffset = offset;
			halfHuge->dataOffset = offset + this->headerSize();
		}

        halfHuge->retrieve(fp);
    }

   return true;
}

/*
bool CHugeImage::setRect(unsigned char* buf, int top, int height, int left, int width)
{
    return false;
}
*/

/**
 * sets the pixel data. imagery is written to disk
 * @param buf the source buffer
 * @param r, c the upper left corner of the tile
 *
 */
void CHugeImage::setTile(unsigned char* buf, int r, int c)
{
    unsigned char* dest = this->getTile(r, c);

    if (dest == NULL)
        return;

    this->pixelSize = this->pixelSizeBytes();

    ::memcpy(dest, buf, this->tileWidth*this->tileHeight * this->pixelSize);
    file_offset offset = this->computeFileOffset(r, c);
    CHugeImage::SEEK(this->fp, &offset);

    fwrite(dest, sizeof(unsigned char), this->tileWidth*this->tileHeight * this->pixelSize, this->fp);

    if (this->halfRes != NULL)
    {
        this->halfRes->createHalfResTile(r/2, c/2);

        unsigned char* buf = this->halfRes->getTile(r/2, c/2);
        this->halfRes->setTile(buf, r/2, c/2);
    }
}

/**
 * computes the pixel size in bytes
 * @return the number of byte a pixel takes
 */
int CHugeImage::pixelSizeBytes()
{
    int pixS;

    if (this->bands == 3)
        pixS = 4*this->bpp/8;
    else if (this->bands == 4)
        pixS = 4*this->bpp/8;
    else
        pixS = this->bands * this->bpp/8;

    return pixS;
}

int CHugeImage::computeTotalTileCount(int h, int w)
{


	// calculate the number of tiles
    int across = w / TILE_SIZE;

    // if there is a remainder, we need one more tile
    if (w % TILE_SIZE != 0)
        across++;


    int down = h / TILE_SIZE;

    // if there is a remainder, we need one more tile
    if (h % TILE_SIZE != 0)
        down++;


    if ( (w < 2 * TILE_SIZE) && (h < 2 * TILE_SIZE) )
    {
        return across*down;
    }
    else
    {
        return across*down + this->computeTotalTileCount(h/2, w/2);
    }
}


/**
 * used in the create process to help generate the image histogram
 * @param lineBuf a pointer to a line of imagery
 */
void CHugeImage::updateHistogram(unsigned char* lineBuf)
{
	unsigned short *shrtBuf = 0;
	float *fltBuf = 0;
	if (this->bpp == 16) shrtBuf = (unsigned short *) lineBuf;
	else if (this->bpp == 32) fltBuf = (float *) lineBuf;
	int diffX = this->bands;
	if (diffX == 3) diffX = 4;
	int max = this->width * diffX;

	if (this->bpp == 8)
	{
		if (this->intensityHistogram)
		{
			for (int c = 0; c < max; c += diffX)
			{
				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{
					this->Histograms[ibnd]->increment(lineBuf[c + ibnd]);
				}

				this->intensityHistogram->increment( int((lineBuf[c] + lineBuf[c + 1] + lineBuf[c + 2])/3.0));
			}
		}
		else
		{
			for (int c = 0; c < max; c += diffX)
			{
				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{
					this->Histograms[ibnd]->increment(lineBuf[c + ibnd]);
				}
			}
		}
	}
	else if (this->bpp == 16)
	{
		if (this->intensityHistogram)
		{
			for (int c = 0; c < max; c += diffX)
			{
				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{
					this->Histograms[ibnd]->increment(shrtBuf[c + ibnd]);
				}

				this->intensityHistogram->increment( int(float(shrtBuf[c] + shrtBuf[c + 1] + shrtBuf[c + 2])/3.0));
			}
		}
		else
		{
			for (int c = 0; c < max; c += diffX)
			{
				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{
					this->Histograms[ibnd]->increment(shrtBuf[c + ibnd]);
				}
			}
		}
	}
	else if (this->bpp == 32) // float image
	{
		for (int c = 0; c < max; c += diffX)
		{
			for (int ibnd = 0; ibnd < this->bands; ++ibnd)
			{
					float val = fltBuf[c + ibnd];
					if (val != -9999.0)
						this->Histograms[ibnd]->checkFltMinMax(val);
			}
		}
	}

	for (int ibnd = 0; ibnd < this->bands; ++ibnd)
	{
		this->Histograms[ibnd]->updateMinMax(0.02,0.98);
	}

	if (this->intensityHistogram)
		this->intensityHistogram->updateMinMax(0.02,0.98);

}

bool CHugeImage::requiresSubImage() const
{
    if ( (this->width < 2 * TILE_SIZE) && (this->height < 2 * TILE_SIZE) )
        return false;

	return true;
};

file_offset CHugeImage::getSubImageOffset() const
{
    file_offset offset = 0;

    if (this->requiresSubImage())
    {
		long fullWidth = this->tilesAcross*this->tileWidth;
		long fullHeight = this->tilesDown*this->tileHeight;
        file_offset w = fullWidth;
        file_offset h = fullHeight;
        file_offset p = this->pixelSize;
        offset = w*h*p + this->dataOffset;
    }

	return offset;
};

bool CHugeImage::createEmpty()
{
	initAndWriteHeader();

    unsigned long length = this->tileWidth*this->tileHeight * this->pixelSize;
	unsigned char* tile = new unsigned char[length];
	memset(tile, 0, length);

    for (int r = 0; r < this->height; r+= this->tileHeight)
    {
		// create all tiles accross
		for (int c = 0; c < this->width; c += this->tileWidth)
		{
			fwrite(tile, sizeof(unsigned char), length, this->fp);
			CHugeImage::tileCreationCount++;
            if (this->listener != NULL)
            {
                double p = 100.0*(double)CHugeImage::tileCreationCount/(double)CHugeImage::totalTileCount;
                 this->listener->setProgressValue(p);
			}
		}
	}


	delete [] tile;

	if (this->requiresSubImage()) createSubImages(true);

    return false;
};

void CHugeImage::fillSublevelBufferMovingAverage(int R, int C, unsigned char *dest)
{
	int diffX = (bands == 3) ? 4 : bands;
	unsigned long nGV = (2 * this->doubleSize->tileHeight + 1) * (2 * this->doubleSize->tileWidth + 1);
	unsigned long newMax = this->tileHeight * this->tileWidth * diffX;
	long oldDiffY = (2 * this->doubleSize->tileWidth + 1) * diffX;
	long oldDiffX = 2 * diffX;
	long oldDiffDiag = oldDiffY + diffX;
	long newDiffY = this->tileWidth * diffX;

	unsigned char *srcUC = new unsigned char[nGV * pixelSizeBytes()];

	this->doubleSize->getRect(srcUC,R, 2 * this->tileHeight + 1, C, 2 * this->doubleSize->tileWidth + 1);

	if (this->bpp == 8)
	{
		for (int bnd = 0; bnd < diffX; ++bnd)
		{
			unsigned long newMin = bnd;
			unsigned long oldMin = bnd;
			for (; newMin < newMax; oldMin += 2 * oldDiffY, newMin += newDiffY)
			{
				unsigned long newMaxInRow = newMin + newDiffY;
				unsigned long oldind = oldMin;
				for (unsigned long newind = newMin; newind < newMaxInRow; newind += diffX, oldind += oldDiffX)
				{
					unsigned short g = srcUC[oldind] + srcUC[oldind + diffX] + srcUC[oldind + oldDiffY] + srcUC[oldind + oldDiffDiag];
					g /= 4;
					dest[newind] = (unsigned char) g;
				}
			}
		}
	}
	else if (this->bpp == 16 )
	{
		unsigned short *srcUS = (unsigned short *) srcUC;
		unsigned short *dstUS = (unsigned short *) dest;

		for (int bnd = 0; bnd < diffX; ++bnd)
		{
			unsigned long newMin = bnd;
			unsigned long oldMin = bnd;
			for (; newMin < newMax; oldMin += 2 * oldDiffY, newMin += newDiffY)
			{
				unsigned long newMaxInRow = newMin + newDiffY;
				unsigned long oldind = oldMin;
				for (unsigned long newind = newMin; newind < newMaxInRow; newind += diffX, oldind += oldDiffX)
				{
					unsigned int g = srcUS[oldind] + srcUS[oldind + diffX] + srcUS[oldind + oldDiffY] + srcUS[oldind + oldDiffDiag];
					g /= 4;
					dstUS[newind] = (unsigned short) g;
				}
			}
		}
	}
	else if (this->bpp == 32)
	{
		float *srcFT = (float *) srcUC;
		float *dstFT = (float *) dest;

		for (int bnd = 0; bnd < diffX; ++bnd)
		{
			unsigned long newMin = bnd;
			unsigned long oldMin = bnd;
			for (; newMin < newMax; oldMin += 2 * oldDiffY, newMin += newDiffY)
			{
				unsigned long newMaxInRow = newMin + newDiffY;
				unsigned long oldind = oldMin;
				for (unsigned long newind = newMin; newind < newMaxInRow; newind += diffX, oldind += oldDiffX)
				{
					float g = srcFT[oldind] + srcFT[oldind + diffX] + srcFT[oldind + oldDiffY] + srcFT[oldind + oldDiffDiag];
					g *= 0.25;
					dstFT[newind] = g;
				}
			}
		}
	}
	else
	{
			// do nothing otherwise
	}

	delete [] srcUC;
};

void CHugeImage::fillSublevelBufferNearestNeighbour(int R, int C, unsigned char *dest)
{
	int diffX = (bands == 3) ? 4 : bands;

	unsigned long newMax = this->tileHeight * this->tileWidth * diffX;
	long oldDiffY = (2 * this->doubleSize->tileWidth) * diffX;
	long oldDiffX = 2 * diffX;
	long newDiffY = this->tileWidth * diffX;

	unsigned char  *srcUC = new unsigned char[4 * this->doubleSize->tileHeight * this->doubleSize->tileWidth * pixelSizeBytes()];

	this->doubleSize->getRect(srcUC, R, 2 * this->tileHeight, C, 2 * this->doubleSize->tileWidth);

	if (this->bpp == 8)
	{
		for (int bnd = 0; bnd < diffX; ++bnd)
		{
			unsigned long newMin = bnd;
			unsigned long oldMin = bnd;
			for (; newMin < newMax; oldMin += 2 * oldDiffY, newMin += newDiffY)
			{
				unsigned long newMaxInRow = newMin + newDiffY;
				unsigned long oldind = oldMin;
				for (unsigned long newind = newMin; newind < newMaxInRow; newind += diffX, oldind += oldDiffX)
				{
					dest[newind] = srcUC[oldind];
				}
			}
		}
	}
	else if (this->bpp == 16 )
	{
		unsigned short *srcUS = (unsigned short *) srcUC;
		unsigned short *dstUS = (unsigned short *) dest;

		for (int bnd = 0; bnd < diffX; ++bnd)
		{
			unsigned long newMin = bnd;
			unsigned long oldMin = bnd;
			for (; newMin < newMax; oldMin += 2 * oldDiffY, newMin += newDiffY)
			{
				unsigned long newMaxInRow = newMin + newDiffY;
				unsigned long oldind = oldMin;
				for (unsigned long newind = newMin; newind < newMaxInRow; newind += diffX, oldind += oldDiffX)
				{
					dstUS[newind] = srcUS[oldind];
				}
			}
		}
	}
	else if (this->bpp == 32)
	{
		float *srcFT = (float *) srcUC;
		float *dstFT = (float *) dest;

		for (int bnd = 0; bnd < diffX; ++bnd)
		{
			unsigned long newMin = bnd;
			unsigned long oldMin = bnd;
			for (; newMin < newMax; oldMin += 2 * oldDiffY, newMin += newDiffY)
			{
				unsigned long newMaxInRow = newMin + newDiffY;
				unsigned long oldind = oldMin;
				for (unsigned long newind = newMin; newind < newMaxInRow; newind += diffX, oldind += oldDiffX)
				{
					dstFT[newind] = srcFT[oldind];
				}
			}
		}
	}
	else
	{
		// do nothing otherwise
	}

	delete [] srcUC;
};

bool CHugeImage::createSubImage()
{
	// this is a loop over tiles
	for (int r = 0; r < this->height; r+= this->tileHeight)
	{
		for (int c = 0; c < this->width; c+= this->tileWidth)
		{
			// our current tile row and col
			int tileCol = c/this->tileWidth;
			int tileRow = r/this->tileHeight;

			// this is the CImageTile we are working on
			int index = tileCol;
			unsigned char* dest = this->tiles[index]->getData();
			this->tiles[index]->setTileRowCol(tileRow, tileCol);
			memset(dest, 230, this->tileWidth*this->tileHeight * this->pixelSize);

			// R, C the row col in the double size image
			int R = r*2;
			int C = c*2;

			// The following calls will moved the file pos so remember it
			file_offset wayBack;
			CHugeImage::TELL(this->fp, &wayBack);

			// first put every other pixel from the upper left tile
			// the four tiles from the double size image
////////////////////// begin of new sub image creation //////////////////

			if (!this->transferFunction || !this->transferFunction->isColorIndexed()) this->fillSublevelBufferMovingAverage(R, C, dest);
			else this->fillSublevelBufferNearestNeighbour(R, C, dest);

			// go back
			CHugeImage::SEEK(this->fp, &wayBack);

			fwrite(dest, sizeof(unsigned char), this->tileWidth*this->tileHeight * this->pixelSize, this->fp);

			CHugeImage::tileCreationCount++;

			if (this->listener != NULL)
			{
				double p = 100.0*(double)CHugeImage::tileCreationCount/(double)CHugeImage::totalTileCount;
				this->listener->setProgressValue(p);
			}
		}
	}

	return true;
}

bool CHugeImage::createSubImages(bool empty)
{
	CHugeImage* halfHuge = (CHugeImage*)this->halfRes;

	if (!this->halfRes)
	{
		this->halfRes = new CHugeImage();
		halfHuge = (CHugeImage*) this->halfRes;

		halfHuge->bpp = this->bpp;
		halfHuge->bands = this->bands;
		halfHuge->width = this->width/2;
		halfHuge->height = this->height/2;
		halfHuge->pixelSize = halfHuge->pixelSizeBytes();
		halfHuge->doubleSize = this;
		halfHuge->fp = this->fp;
		halfHuge->headerOffset = this->getSubImageOffset();
		halfHuge->dataOffset = halfHuge->headerOffset + this->headerSize();
		halfHuge->listener = this->listener;
//		halfHuge->colorIndexed = this->colorIndexed;
	}

	if (!empty)
	{
		halfHuge->initAndWriteHeader();
		halfHuge->createSubImage();
		if (halfHuge->requiresSubImage()) return halfHuge->createSubImages(empty);

		return true;
	}

	return halfHuge->create(NULL);
};

/**
 * Protected function to create the image database.
 * @return false for failure
 *
 */
bool CHugeImage::create(CImageFile* imageFile)
{
	if (!imageFile) return createEmpty();

	bool doubleToFloat = false;
	bool ushortToFloat = false;
	bool uchartoFloat = false;

	if (this->bpp == 64)
	{
		this->bpp = 32;
		this->pixelSize = 4;
		doubleToFloat = true;
	}
	else if (this->isDEM())
	{
		if (this->bpp == 16)
		{
			ushortToFloat = true;
			this->bpp = 32;
			this->pixelSize = 4;
		}
		else if (this->bpp == 8)
		{
			uchartoFloat = true;
			this->bpp = 32;
			this->pixelSize = 4;
		}
	}

   	initAndWriteHeader();

	long bSize = this->tileWidth*this->tilesAcross * this->pixelSize;
	if (doubleToFloat) bSize *= 2;

	// first put every other pixel from the upper left tile
    unsigned char* src = NULL;

    // Write the tiles for planar config = 1 images
    if (this->doubleSize == NULL) // I must be the top most image pyramid
	{
        unsigned char* line = new unsigned char[bSize];
		double *dline = (double *) line;
		short *sline = ( short *) line;
		float  *fline = (float *) line;
		int MaxTileWidth = this->tileWidth * this->tilesAcross;

        memset(line, 0, bSize);

        for (int r = 0; r < this->height; r+= this->tileHeight)
        {
			int rowTileMax = r + this->tileHeight; 
			bool last = false;
			if (rowTileMax > this->height)
			{
				rowTileMax = this->height;
				last = true;
			}

			int tileDelta = this->tileWidth * this->pixelSize;


            // create all tiles accross
            for (int rowInTile = r, tileOffset = 0; rowInTile < rowTileMax; rowInTile++, tileOffset += tileDelta)
			{
             //   memset(line, 0, this->width*pixelSize);
				memset(line, 0, bSize);
				if(imageFile)
				{
                    imageFile->readLine(line, rowInTile);

					float scale = imageFile->getScale();
					float Z0 = imageFile->getZ0();
					float nodata =  imageFile->getNoDataValue();

					if (this->bpp == 32)
					{
						if (doubleToFloat)
						{
							for (int i = 0; i < this->width; ++i)
							{
								fline[i] = (float) dline[i];
								if (fabs(fline[i] - nodata) < FLT_EPSILON ) fline[i] = DEM_NOHEIGHT;
								else fline[i] = Z0 + scale*fline[i];
							}
						}
						else if (ushortToFloat)
						{
							for (int i = this->width - 1; i >= 0; --i)
							{
								fline[i] = (float) sline[i];
								if (fabs(fline[i] - nodata) < FLT_EPSILON ) fline[i] = DEM_NOHEIGHT;
								else fline[i] = Z0 + scale*fline[i];
							}

						}
						else if (uchartoFloat)
						{
							for (int i = this->width - 1; i >= 0; --i)
							{
								fline[i] = (float) line[i];
								if (fabs(fline[i] - nodata) < FLT_EPSILON ) fline[i] = DEM_NOHEIGHT;
								else fline[i] = Z0 + scale*fline[i];
							}
						}
						else
						{
							for (int i = 0; i < this->width; ++i)
							{
								if (fabs(fline[i] - nodata) < FLT_EPSILON ) fline[i] = DEM_NOHEIGHT;
								else fline[i] = Z0 + scale*fline[i];
							}
						}

						for (int i = this->width; i < MaxTileWidth; ++i)
						{
							fline[i] = DEM_NOHEIGHT; 
						}
					}
                    this->updateHistogram(line);
				}

                for (int tileCol = 0; tileCol < this->tilesAcross; tileCol++)
                {
                    int lineOffset = (tileCol*this->tileWidth) * this->pixelSize;
					int tileRow = r/this->tileHeight;
                    tiles[tileCol]->setTileRowCol(tileRow, tileCol);

                    unsigned char* buf = tiles[tileCol]->getData();

                    memcpy(buf+tileOffset, line+lineOffset, this->tileWidth * this->pixelSize);
                }
			}

			if (last)
			{
				for (int rowInTile = rowTileMax, tileOffset = tileDelta * (rowTileMax - r); rowInTile < r + this->tileHeight; rowInTile++, tileOffset += tileDelta)
				{
				 //   memset(line, 0, this->width*pixelSize);
					if (this->bpp == 32)
					{
							for (int i = 0; i < MaxTileWidth; ++i)
							{
								fline[i] = DEM_NOHEIGHT; 
							}
					}
					else
					{
						memset(line, 0, bSize);
					}
	
					for (int tileCol = 0; tileCol < this->tilesAcross; tileCol++)
					{
						int lineOffset = (tileCol*this->tileWidth) * this->pixelSize;
						int tileRow = r/this->tileHeight;
						tiles[tileCol]->setTileRowCol(tileRow, tileCol);

						unsigned char* buf = tiles[tileCol]->getData();

						memcpy(buf+tileOffset, line+lineOffset, this->tileWidth * this->pixelSize);
					}
				}
			}

            // write these tiles
            for (int i = 0; i < this->tilesAcross; i++)
            {
                unsigned char* data = tiles[i]->getData();

                fwrite(data, sizeof(unsigned char), this->tileWidth*this->tileHeight * this->pixelSize, this->fp);
                CHugeImage::tileCreationCount++;

                if (this->listener != NULL)
                {
                    double p = 100.0*(double)CHugeImage::tileCreationCount/(double)CHugeImage::totalTileCount;
                    this->listener->setProgressValue(p);
                }
            }
        }

        delete [] line;

        // write the   histogram (only written for top level image)
        writeHistograms();
    }


	// ckeck if we are creating a colorIndexed image
	if (imageFile->getLookupTable())
		this->setLookupTable(*imageFile->getLookupTable());
	else
		this->initDefaultTransferFunction();

    if (this->requiresSubImage()) createSubImages(false);

	


	return true;
}

void CHugeImage::initAndWriteHeader()
{
	this->version = HUG_FILE_VERSION;
   	this->initHistograms();
	this->initIntensityHistogram();

    // calculate the number of tiles
    this->tilesAcross = this->width / TILE_SIZE;

    // if there is a remainder, we need one more tile
    if (this->width % TILE_SIZE != 0)
        this->tilesAcross++;

    this->tilesDown = this->height / TILE_SIZE;

    // if there is a remainder, we need one more tile
    if (this->height % TILE_SIZE != 0)
        this->tilesDown++;

 // Why would you not have square tiles?
    this->tileWidth = TILE_SIZE;
    this->tileHeight = TILE_SIZE;

    initTiles(this->tilesAcross);

    for (int i = 0; i < this->nBufferedTiles; i++)
    {
        this->tiles[i] = new CImageTile(this->tileWidth, this->tileHeight, this->bpp, this->bands);
    }

    // these are the header fields
    fwrite(&this->version, sizeof(long), 1, this->fp);
    fwrite(&this->bpp, sizeof(long), 1, this->fp);
    fwrite(&this->bands, sizeof(long), 1, this->fp);
    fwrite(&this->width, sizeof(long), 1, this->fp);
    fwrite(&this->height, sizeof(long), 1, this->fp);
    fwrite(&this->tilesAcross, sizeof(long), 1, this->fp);
    fwrite(&this->tilesDown, sizeof(long), 1, this->fp);
    fwrite(&this->tileWidth, sizeof(long), 1, this->fp);
    fwrite(&this->tileHeight, sizeof(long), 1, this->fp);

	this->pixelSize = this->pixelSizeBytes();

    // the offset to the first image is simply the size of the header
    if (this->doubleSize == NULL)
        this->dataOffset = this->headerSize();

	file_offset offset = this->getSubImageOffset();

    fwrite(&offset, sizeof(file_offset), 1, this->fp);

    // now write the histogram(s)
    for (int i = 0; i < bands; i++)
    {
        this->Histograms[i]->writeHistogram(this->fp);
    }

};

void CHugeImage::writeHistograms()
{
	if (this->fp)
	{
        file_offset wayBack;

        CHugeImage::TELL(this->fp, &wayBack);

        file_offset histogramOffset = this->dataOffset;

        for (int i = 0; i < this->bands; i++)
        {
            CHistogram* histogram = this->Histograms[i];
            histogramOffset -= histogram->getSize()*sizeof(unsigned long);
        }

        CHugeImage::SEEK(this->fp, &histogramOffset);

        for (int i = 0; i < this->bands; i++)
        {
            this->Histograms[i]->writeHistogram(this->fp);
        }

        // go back
        CHugeImage::SEEK(this->fp, &wayBack);
	}
};


/**
 * computes the offset in the *.hug file to the given tile
 * @param r, c the upper left of the tile
 * @return the offset
 */
file_offset CHugeImage::computeFileOffset(int r, int c)
{
    // sanity check
    if ( (r < 0) || (c < 0) )
        return 0;

    int tileRow = r/this->tileHeight;
    int tileCol = c/this->tileWidth;

    if (tileRow >= this->tilesDown)
        return 0;

    if (tileCol >= this->tilesAcross)
        return 0;

    // the size in bytes of a single tile
    int bytesInSize = this->tileWidth*this->tileHeight * this->pixelSize;

    file_offset tileRow_file_offset  = tileRow;
    file_offset tilesAcross_file_offset  = this->tilesAcross;
    file_offset tileCol_file_offset  = tileCol;
    file_offset bytesInSize_file_offset  = bytesInSize;
    file_offset offset = (tileRow_file_offset *tilesAcross_file_offset +tileCol_file_offset )*bytesInSize_file_offset  + this->dataOffset;

    return offset;

}

/**
 * Gets a CImageTile object
 * @param r, c any row and column inside the tile
 * @return false for failure
 *
 */
bool CHugeImage::getImageTile(CImageTile** imageTile, int r, int c)
{
    if (this->tiles == NULL)
        return false;

    // sanity check
    if ( (r < 0) || (c < 0) )
        return false;

	if (!this->fp) this->fp = fopen(this->filename, "r+b");

    if (!this->fp)
    {
        return false;
    }

	if (!this->doubleSize)
	{
		CHugeImage *hr = (CHugeImage *) this->halfRes;

		while (hr)
		{
			hr->fp = this->fp;
			hr = (CHugeImage *) hr->halfRes;
		}
	}

    int tileRow = r/this->tileHeight;
    int tileCol = c/this->tileWidth;

    if (tileRow >= this->tilesDown)
        return false;

    if (tileCol >= this->tilesAcross)
        return false;

    // first check the buffered tiles
    for (int i = 0; i < this->nBufferedTiles; i++)
    {
        CImageTile* testTile = this->tiles[i];

        // make sure its there
        if (testTile)
		{
			if ((testTile->getTileRow() == tileRow) && (testTile->getTileCol() == tileCol))
			{
				*imageTile = testTile;
				return true;
			}
		}
    }

    // better buffer the tile

    // increment index (and make sure its in range)
    this->lastBufferedTile++;
    if (this->lastBufferedTile >= this->nBufferedTiles)
        this->lastBufferedTile = 0;

    // allocate new one if nessesary
    if (this->tiles[this->lastBufferedTile] == NULL)
        this->tiles[this->lastBufferedTile] = new CImageTile(this->tileWidth, this->tileHeight, this->bpp, this->bands);

    CImageTile* tmpTile = this->tiles[this->lastBufferedTile];

    tmpTile->setTileRowCol(tileRow, tileCol);

    // read tile and convert to our format
    unsigned char* buffer = tmpTile->getData();

    file_offset offset = this->computeFileOffset(r, c);

    //fseek(this->fp, offset, SEEK_SET);
    CHugeImage::SEEK(this->fp, &offset);

    fread(buffer, sizeof(unsigned char), this->getTileBytes(), this->fp);

    *imageTile = tmpTile;
    return true;
}

/**
  * Gets a rectangle of imagery into the supplied buf
  * @param unsigned char* buf the destination buffer
  * RGB imagery is returned RGB0RGB0...
  * @param rectTop, rectHeight, rectLeft, rectWidth the rectangle extents
  * @return false for failure
  *
  */
bool CHugeImage::getRect(unsigned char* buf, int rectTop, int rectHeight, int rectLeft, int rectWidth)
{
    CImageTile* imageTile = NULL;

    int leftTile = rectLeft/this->tileWidth;

    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (rectLeft < 0) && (rectLeft%this->tileWidth != 0) )
        leftTile--;

    int rightTile = (rectLeft+rectWidth)/this->tileWidth;

    int tileTouchedAcross = rightTile-leftTile+1;

    int topTile = rectTop/this->tileHeight;

    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (rectTop < 0) && (rectTop%this->tileHeight != 0) )
        topTile--;

    int bottomTile = (rectTop+rectHeight)/this->tileHeight;
    int tileTouchedDown = bottomTile-topTile+1;

    // this outer loop is for determining the current tile only
    for (int i = 0, tr = rectTop; i < tileTouchedDown; i++, tr+= this->tileHeight)
    {
        for (int j = 0, tc = rectLeft; j < tileTouchedAcross; j++, tc+= this->tileWidth)
        {
            // the current tile
            this->getImageTile(&imageTile, tr, tc);

            // sanity check
            if (imageTile == NULL)
                continue;

            // the tile extents
            int tileTop = imageTile->getTileRow() * this->tileHeight;
            int tileLeft = imageTile->getTileCol() * this->tileWidth;
            int tileBottom = tileTop + this->tileHeight;
            int tileRight = tileLeft + this->tileWidth;

            // the source data
            unsigned char* src = imageTile->getData();

            int lineOffset;
            int tileLineOffset;
            int rowInTile;
            int rowInRect;
            int colInTile;
            int colInRect;

            // now fill the destination buf
            for (int r = max(rectTop, tileTop); r < min(tileBottom, rectTop+rectHeight); r++)
            {
                rowInTile = r-tileTop;
                rowInRect = r-rectTop;

                lineOffset = rowInRect*rectWidth * this->pixelSize;
                tileLineOffset = rowInTile*this->tileWidth * this->pixelSize;

                int cLeft = max(rectLeft, tileLeft);
                int cRight = min(rectLeft+rectWidth, tileRight);
                int w = cRight - cLeft;

                colInTile = cLeft-tileLeft;
                colInRect = cLeft-rectLeft;

                memcpy(buf + lineOffset + colInRect * this->pixelSize,
					   src + tileLineOffset + colInTile * this->pixelSize, this->pixelSize * w);
            }
        }
    }

    return true;
}

/**
  * Gets a rectangle of imagery into the supplied buf, however the imagery
  * is resampled to fill destination rect
  * @param unsigned char* buf the destination buffer
  * RGB imagery is returned RGB0RGB0...
  * @param rectTop, rectHeight, rectLeft, rectWidth the source rectangle extents
  * @param destHeight, destWidth the width or height of the destination rect.
  * @return false for failure
  */
bool CHugeImage::getRect(unsigned char* buf, int rectTop, int rectHeight, int rectLeft, int rectWidth, int destHeight, int destWidth)
{
    double xfactor;
    double yfactor;
    double factor;

    // are we scaling by width
    xfactor = (double)destWidth/rectWidth;
    yfactor = (double)destHeight/rectHeight;

    factor = min(xfactor, yfactor);

    // if there is no  halfres or we are at a .5 or greater zoom
    // the reason for the > 0.5 zoom is this ensures we are down-sampling
    // which produces a better result
    if ( (factor >= 0.5) || (this->halfRes == NULL) )
    {
        CImageBuffer srcBuf(rectLeft,rectTop,rectWidth,rectHeight,this->bands,this->bpp,true);
		
		//unsigned char *src = new unsigned char[rectHeight*rectWidth * this->pixelSize];
		unsigned char *src = srcBuf.getPixUC();

		if (srcBuf.getBpp() == 16)
			src = (unsigned char*)srcBuf.getPixUS();
		else if (srcBuf.getBpp() == 32)
			src = (unsigned char*)srcBuf.getPixF();
		
		if (src == NULL)
			return false;

        if (!this->getRect(src, rectTop, rectHeight, rectLeft, rectWidth))
        {
            return false;
        }

		// we need the image at level 0, only this image has the transfer function
		int zoomDownFactor = 1;
		CHugeImage* himage = this->getLevelNUllImage(zoomDownFactor);
		if (himage && himage->getTransferFunction())
		{	
			srcBuf.setOffset(srcBuf.getOffsetX() * zoomDownFactor,srcBuf.getOffsetY() * zoomDownFactor);
			himage->transferFunction->applyTransferFunction(srcBuf,srcBuf,rectHeight,rectWidth,zoomDownFactor,false);
		}


        double xStep = (double)rectWidth/(double)destWidth;
        double yStep = (double)rectHeight/(double)destHeight;
        double x = 0;
        double y = 0;

        for (int r = 0; r < destHeight; r++)
        {
            int R = (int)y;
            int lineOffsetSrc = R*rectWidth*pixelSize;
            int lineOffsetDest = r*destWidth*pixelSize;
            x = 0;
            for (int c = 0; c < destWidth; c++)
            {
                int C = (int)x;
                ::memcpy(buf+lineOffsetDest+c*pixelSize, src+lineOffsetSrc+C*pixelSize, pixelSize);
                x += xStep;
            }

            y+= yStep;
        }

      //  delete [] src;
        return true;
    }

    return this->halfRes->getRect(buf, rectTop/2, rectHeight/2, rectLeft/2, rectWidth/2, destHeight, destWidth);
}

bool CHugeImage::getRect(CImageBuffer &buffer, int top, int height, int left, int width)
{
	if ((buffer.getOffsetY() != top) || (buffer.getOffsetX() != left) ||
		(buffer.getWidth() != width) || (buffer.getHeight() != height) ||
		(buffer.getBands() != this->bands) || (buffer.getBpp() != this->bpp) ||
		((buffer.getBpp() == 16) && (!buffer.getUnsignedFlag())))
	{
		buffer.set(left, top, width, height, this->bands, this->bpp, true);
	};

	bool ret = false;
	if (this->bpp == 8) ret = this->getRect(buffer.getPixUC(), top, height, left, width);
	else if (this->bpp == 16) ret = this->getRect((unsigned char *) buffer.getPixUS(), top, height, left, width);
	else if (this->bpp == 32) ret = this->getRect((unsigned char *) buffer.getPixF(), top, height, left, width);
	return ret;
};

bool CHugeImage::getRect(CImageBuffer &buffer)
{
	return getRect(buffer, buffer.getOffsetY(), buffer.getHeight(), buffer.getOffsetX(), buffer.getWidth());
};
 
bool CHugeImage::getRect(CImageBuffer &buffer, const CTrans2D *trafo, eResamplingType resType)
{
	bool ret = true;

	int rmin, h, cmin, w;

	if (!trafo) ret = false;
	else
	{
		int margin = 2;
		if (resType == eBilinear) margin = 3;
		else if (resType == eBicubic) margin = 4;
		trafo->transformWindow(buffer.getOffsetX(), buffer.getOffsetY(), buffer.getWidth(), buffer.getHeight(),
			                   cmin, rmin,w,h,this->getWidth(), this->getHeight(), margin, margin);

		CImageBuffer readBuf(cmin,rmin, w, h, this->bands,this->bpp, true, 0.0);
		ret = this->getRect(readBuf);
		if (ret) ret = buffer.resample(readBuf, *trafo, resType);
	}

	return ret;
};

bool CHugeImage::getRect(CImageBuffer &buffer, int level)
{
	CBaseImage *pyrLevel = this;
	int clev = 0;
	while (pyrLevel && clev < level)
	{
		clev++;
		pyrLevel = pyrLevel->halfRes;
	}
	if (!pyrLevel) return false;

	bool ret = false;
	if ((this->bands != buffer.getBands()) || (this->bpp != buffer.getBpp()))
	{
		buffer.set(buffer.getOffsetX(), buffer.getOffsetY(), buffer.getWidth(), buffer.getHeight(), this->bands, this->bpp, buffer.getUnsignedFlag());
	}

	if (this->bpp == 8) ret = pyrLevel->getRect(buffer.getPixUC(), buffer.getOffsetY(), buffer.getHeight(), buffer.getOffsetX(), buffer.getWidth());
	else if (this->bpp == 16) ret = pyrLevel->getRect((unsigned char *) buffer.getPixUS(), buffer.getOffsetY(), buffer.getHeight(), buffer.getOffsetX(), buffer.getWidth());
	else if (this->bpp == 32) ret = pyrLevel->getRect((unsigned char *) buffer.getPixF(), buffer.getOffsetY(), buffer.getHeight(), buffer.getOffsetX(), buffer.getWidth());
	return ret;
};

bool CHugeImage::setRect(unsigned char* buf, int top, int Height, int left, int Width)
{
    int pixelSize = this->pixelSizeBytes();

    CImageTile* imageTile = 0;

    int leftTile = left/this->tileWidth;

    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (left < 0) && (left%this->tileWidth != 0) )
        leftTile--;

    int rightTile = (left + Width)/this->tileWidth;

    int tileTouchedAcross = rightTile-leftTile+1;

    int topTile = top/this->tileHeight;

    // if the requested rect extends into negative tile, make sure its calculated correctly
    if ( (top < 0) && (top%this->tileHeight != 0) )
        topTile--;

    int bottomTile = (top+Height)/this->tileHeight;
    int tileTouchedDown = bottomTile-topTile+1;

    // this outer loop is for determining the current tile only
    for (int i = 0, tr = top; i < tileTouchedDown; i++, tr+= this->tileHeight)
    {
        for (int j = 0, tc = left; j < tileTouchedAcross; j++, tc+= this->tileWidth)
        {
            // the current tile
            this->getImageTile(&imageTile, tr, tc);

            // sanity check
            if (imageTile == NULL)
                continue;

            // the tile extents
            int tileTop = imageTile->getTileRow() * this->tileHeight;
            int tileLeft = imageTile->getTileCol() * this->tileWidth;
            int tileBottom = tileTop + this->tileHeight;
            int tileRight = tileLeft + this->tileWidth;

            // the source data
            unsigned char* dest = imageTile->getData();

            int lineOffset;
            int tileLineOffset;
            int rowInTile;
            int rowInRect;
            int colInTile;
            int colInRect;

            // now fill the destination buf
            for (int r = max(top, tileTop); r < min(tileBottom, top+Height); r++)
            {
                rowInTile = r-tileTop;
                rowInRect = r-top;

                lineOffset = rowInRect*Width*pixelSize;
                tileLineOffset = rowInTile*this->tileWidth*pixelSize;

                int cLeft = max(left, tileLeft);
                int cRight = min(left+Width, tileRight);
                int w = cRight - cLeft;

                colInTile = cLeft-tileLeft;
                colInRect = cLeft-left;

 //               memcpy(buf+lineOffset+colInRect*pixelSize, src+tileLineOffset+colInTile*pixelSize, pixelSize*w);
                memcpy(dest+tileLineOffset+colInTile*pixelSize, buf+lineOffset+colInRect*pixelSize, pixelSize*w);
            }
			this->setTile((unsigned char*)dest, tr, tc);
        }
    }

	return true;
};

bool CHugeImage::setRect(const CImageBuffer &buffer)
{
	bool ret = true;

	if (buffer.getBands() != this->bands) ret = false;

	else
	{
		if ((this->bpp != buffer.getBpp()) ||
			((buffer.getBpp() == 16) && (!buffer.getUnsignedFlag())))
		{
			unsigned char *buf = new unsigned char[buffer.getBufferLength()];
			if (!buf) ret = false;
			else
			{
				if (buffer.getBpp() == 32)
				{
					float *fBuf = buffer.getPixF();
					if (this->bpp == 8)
					{
						for (unsigned long u = 0; u < buffer.getNGV(); ++u)
						{
							buf[u] = (unsigned char) fBuf[u];
						};
					}
					else if (this->bpp == 16)
					{
						unsigned short *uBuf = (unsigned short *) buf;
						for (unsigned long u = 0; u < buffer.getNGV(); ++u)
						{
							uBuf[u] = (unsigned short) fBuf[u];
						};
					}
					else ret = false;
				}
				else if (buffer.getBpp() == 16)
				{
					if (!buffer.getUnsignedFlag())
					{
						short *sBuf = buffer.getPixS();

						if (this->bpp == 8)
						{
							for (unsigned long u = 0; u < buffer.getNGV(); ++u)
							{
								buf[u] = (unsigned char) sBuf[u];
							};
						}
						else if (this->bpp == 16)
						{
							unsigned short *uBuf = (unsigned short *) buf;
							for (unsigned long u = 0; u < buffer.getNGV(); ++u)
							{
								uBuf[u] = (unsigned short) sBuf[u];
							};
						}
						else ret = false;
					}
					else
					{
						unsigned short *usBuf = buffer.getPixUS();

						if (this->bpp == 8)
						{
							for (unsigned long u = 0; u < buffer.getNGV(); ++u)
							{
								buf[u] = (unsigned char) usBuf[u];
							};
						}
						else ret = false;
					};
				}
				else if ((buffer.getBpp() == 1) || (buffer.getBpp() == 8))
				{
					unsigned char *ucBuf = buffer.getPixUC();

					if (this->bpp == 16)
					{
						unsigned short *uBuf = (unsigned short *) buf;
						for (unsigned long u = 0; u < buffer.getNGV(); ++u)
						{
							uBuf[u] = (unsigned short) ucBuf[u];
						};
					}
					else ret = false;
				}
				else ret = false;

				if (ret)
				{
					ret = setRect(buffer.getPixUC(), buffer.getOffsetY(), buffer.getHeight(),
						          buffer.getOffsetX(), buffer.getWidth());
				};
				delete [] buf;
			};
		}
		else
		{
			unsigned char *buf = 0;

			if ((buffer.getBpp() == 8) || (buffer.getBpp() == 1))
				buf = buffer.getPixUC();
			else if (buffer.getBpp() == 16)
				buf = (unsigned char *) buffer.getPixUS();
			else if (buffer.getBpp() == 32)
				buf = (unsigned char *) buffer.getPixF();

			if (!buf)  ret = false;
			else ret = setRect(buf, buffer.getOffsetY(), buffer.getHeight(),
				               buffer.getOffsetX(), buffer.getWidth());
		}
	};
	return ret;
};

bool CHugeImage::getLine(unsigned char* buf, int row)
{
	return this->getRect(buf, row, 1, 0, this->width);
    //return true;
}

bool CHugeImage::getPixel(unsigned char* buf, int r, int c)
{
    return true;
}

bool CHugeImage::setPixel(unsigned char* buf, int row, int col)
{

	// attempt to set single pixels, as setRect seem not doing the right stuff, Jochen

    // pixel size in bytes
    int pixelSize = this->pixelSizeBytes();

    CImageTile* imageTile = 0;

	this->getImageTile(&imageTile, row, col);

	// the source data
	unsigned char* dest = imageTile->getData();

    int imageTileRow = row/this->tileHeight;
    int imageTileCol = col/this->tileWidth;

    if (imageTileRow >= this->tilesDown)
        return false;

    if (imageTileCol >= this->tilesAcross)
        return false;

	int tilerow = row - imageTileRow*this->tileHeight;
	int tilecol = col - imageTileCol*this->tileWidth;

    if (tilerow >= this->tileHeight)
        return false;

    if (tilecol >= this->tileWidth)
        return false;

	int offsetinTile = tilerow*this->tileWidth + tilecol;

	//memcpy(dest+offsetinTile*pixelSize, buf, pixelSize);
	//memcpy(dest+offsetinTile, buf, pixelSize);
	int value = buf[0];
	memset(dest+offsetinTile, value, 1);

    return true;
}

/**
 * @return the Bits per pixel
 */
int CHugeImage::getBpp() const
{
 return this->bpp;
}

/**
 * @return the number of bands (i.e RGB = 3)
 */
int CHugeImage::getBands() const
{
 return this->bands;
}

/**
 * @return the image width
 */
int CHugeImage::getWidth() const
{
 return this->width;
}

/**
 * @return the image height
 */
int CHugeImage::getHeight() const
{
 return this->height;
}

/**
 * @return the tile width
 */
int CHugeImage::getTileWidth() const
{
 return this->tileWidth;
}

/**
 * @return the tile height
 */
int CHugeImage::getTileHeight() const
{
 return this->tileHeight;
}

/**
 * get a tile of imagery zoomed (resampled) according to the zoomUp factor
 * @param buf the destination buffer for the imagery.
 * @param R, C the row, col in the larger (virtual) image system
 * @param zoomUp the zoomfactor
 * @return false if failed
 *
 */
bool CHugeImage::getTileZoomUp(CImageBuffer& dest, int R, int C, int zoomUp)
{
	// determine the rect in the original image
	R = (R/this->tileHeight)*this->tileHeight;
	C = (C/this->tileWidth)*this->tileWidth;

	int r1 = R/zoomUp;
	int c1 = C/zoomUp;

	if ((zoomUp < 2) || (this->resamplingType == eNearestNeighbour))
	{
		CImageBuffer src(r1,c1,this->tileWidth,this->tileHeight,this->bands,this->bpp,this->getBpp() < 32 ? true : false);
		this->getTileBuf(r1,c1,src);

		if (this->transferFunction)
		{
				// correct the offset: 
			// getTileBuf writes TileRow*width as offset
			// getRect writes just row as offset
			// applyTransferFunction expects row as offset
			src.setOffset(src.getOffsetX()/ src.getWidth(),src.getOffsetY()/ src.getHeight());
				
			
			// the huge image has only full tiles, so if the last tile
			// exceeds the image extents we need to correct the actual height and/or width
			// the wallis filter only has parameters for pixel inside the image
			int actualWidth = src.getWidth();
			int actualHeight = src.getHeight();
			
			if ((actualWidth + src.getOffsetX()) > this->width)
				actualWidth -= actualWidth + src.getOffsetX() - this->width;

			if ((actualHeight + src.getOffsetY()) > this->height)
				actualHeight -= actualHeight + src.getOffsetY() - this->height;

			this->transferFunction->applyTransferFunction(src,src,actualHeight,actualWidth,zoomUp,true);
		}


		int rStart = r1 % this->tileHeight;
		int rEnd = rStart + (this->tileHeight/zoomUp);
		int cStart = c1 % this->tileWidth;
		int cEnd = cStart + (this->tileWidth/zoomUp);

		int nBytesPerLine = this->tileWidth*pixelSize;
		int destLineOffset = 0;
		int srcLineOffset = rStart*nBytesPerLine;
	
		unsigned char* tmpDestBuf = NULL;
		unsigned char* tmpSrcBuf = NULL;
		
		if (this->bpp == 8)
		{
			tmpDestBuf = dest.getPixUC();
			tmpSrcBuf = src.getPixUC();
		}
		else if (this->bpp == 16)
		{
			tmpDestBuf = (unsigned char*)dest.getPixUS();
			tmpSrcBuf = (unsigned char*)src.getPixUS();
		}
		else if (this->bpp == 32)
		{
			tmpDestBuf =  (unsigned char*)dest.getPixF();
			tmpSrcBuf = (unsigned char*)src.getPixF();
		}


		if (!tmpDestBuf || !tmpSrcBuf)
			return false;


		for (int r = rStart, destR = 0; r < rEnd; r++, destR += zoomUp)
		{
			for (int c = cStart, destC = 0; c < cEnd; c++, destC += zoomUp)
			{
				destLineOffset = destR*nBytesPerLine;
				for (int i = 0; i < zoomUp; i++)
				{
					for (int j = 0; j < zoomUp; j++)
					{
						memcpy(tmpDestBuf+destLineOffset+(destC+j)*pixelSize, tmpSrcBuf+srcLineOffset+c*pixelSize, pixelSize);
					}
					destLineOffset += nBytesPerLine;
				}
			}

			srcLineOffset += nBytesPerLine;
		}
	}
	else
	{
		int rStart = r1 % this->tileHeight;
		int rEnd = rStart + (this->tileHeight/zoomUp);
		int cStart = c1 % this->tileWidth;
		int cEnd = cStart + (this->tileWidth/zoomUp);

		int w1 = cEnd - cStart  + 15;
		int h1 = rEnd - rStart  + 15;

		CImageBuffer src(c1 - 7, r1 - 7, w1, h1, this->bands, this->bpp, this->getBpp() < 32 ? true : false);

		if (!this->getRect(src)) return false;

		if (this->transferFunction)
		{
			// the huge image has only full tiles, so if the last tile
			// exceeds the image extents we need to correct the actual height and/or width
			// the wallis filter only has parameters for pixel inside the image
			if ((w1 + src.getOffsetX()) > this->width)
				w1 -= w1 + src.getOffsetX() - this->width;

			if ((h1 + src.getOffsetY()) > this->height)
				h1 -= h1 + src.getOffsetY() - this->height;
			
			this->transferFunction->applyTransferFunction(src,src,h1,w1,zoomUp,true);
		}

		int nBytesPerLine = this->tileWidth*pixelSize;
		int destLineOffset = 0;
		int srcLineOffset = rStart*nBytesPerLine;

		double OneByFact = 1.0 / double(zoomUp);
        C2DPoint p0(double(c1) -0.5 * OneByFact - 0.5, double(r1) -0.5 * OneByFact - 0.5);
        C2DPoint p = p0;
		int deltaC = (this->bands == 3)? 4 : this->bands;
		int deltaR = deltaC * this->tileWidth;

		if (this->bpp == 8)
		{
			unsigned char *ucBuf =  dest.getPixUC();
			for (int destR = 0; destR < this->tileHeight; ++destR, p0.y += OneByFact)
			{
				p = p0;
				int offset = destR * deltaR;
				int lineMax = offset + deltaR;
				if (this->resamplingType == eBilinear)
				{
					for (; offset < lineMax; offset += deltaC, p.x += OneByFact)
					{
						src.getInterpolatedColourVecBilin(p, ucBuf + offset);
					}
				}
				else
				{
					for (; offset < lineMax; offset += deltaC, p.x += OneByFact)
					{
						src.getInterpolatedColourVecBicub(p, ucBuf + offset);
					}
				}
			}
		}
		else if (this->bpp == 16)
		{
			unsigned short *shrtBf = (unsigned short *) dest.getPixUS();
			for (int destR = 0; destR < this->tileHeight; ++destR, p0.y += OneByFact)
			{
				p = p0;
				int offset = destR * deltaR;
				int lineMax = offset + deltaR;
				if (this->resamplingType == eBilinear)
				{
					for (; offset < lineMax; offset += deltaC, p.x += OneByFact)
					{
						src.getInterpolatedColourVecBilin(p, shrtBf + offset);
					}
				}
				else
				{
					for (; offset < lineMax; offset += deltaC, p.x += OneByFact)
					{
						src.getInterpolatedColourVecBicub(p, shrtBf + offset);
					}
				}
			}
		}
		else if (this->bpp == 32)
		{
			float *fltBf = (float *) dest.getPixF();
			for (int destR = 0; destR < this->tileHeight; ++destR, p0.y += OneByFact)
			{
				p = p0;
				int offset = destR * deltaR;
				int lineMax = offset + deltaR;
				if (this->resamplingType == eBilinear)
				{
					for (; offset < lineMax; offset += deltaC, p.x += OneByFact)
					{
						src.getInterpolatedColourVecBicub(p, fltBf + offset);
					}
				}
				else
				{
					for (; offset < lineMax; offset += deltaC, p.x += OneByFact)
					{
						src.getInterpolatedColourVecBilin(p, fltBf + offset);
					}
				}
			}
		}
	}

 return true;
}

/**
 * get a tile of imagery zoomed (resampled) according to the zoomDown factor
 * @param buf the destination buffer for the imagery.
 * @param R, C the row, col in the smaller (virtual) image system
 * @param zoomDown the zoomfactor
 * @return false if failed
 *
 */
bool CHugeImage::getTileZoomDown(CImageBuffer &dest, int R, int C, int zoomDown)
{
    if (zoomDown <= 1)
        return false;

    // determine the rect in the original image
    // This calculation simply makes R, C the upper left corner
    R = (R/this->tileHeight)*this->tileHeight;
    C = (C/this->tileWidth)*this->tileWidth;

    // we have this pyramid, we must use it.... maybe
    if ( this->halfRes == NULL )
    {
		bool usf = (this->bpp < 32) ? true : false;
		CImageBuffer locBuf(0,0, this->width, this->height, this->bands, this->bpp, usf, 0.0f);
		if (!this->getRect(locBuf)) return false;


		// we need the image at level 0, only this image has the transfer function
		int zoomDownFactor = 1;
		CHugeImage*himage = this->getLevelNUllImage(zoomDownFactor);
		if (himage && himage->getTransferFunction())
		{
			int actualWidth = locBuf.getWidth();
			int actualHeight = locBuf.getHeight();

			if (((actualWidth + locBuf.getOffsetX())) > this->width)
				actualWidth -= actualWidth + locBuf.getOffsetX() - this->width;

			if (((actualHeight + locBuf.getOffsetY())) > this->height)
				actualHeight -= actualHeight + locBuf.getOffsetY() - this->height;


			himage->transferFunction->applyTransferFunction(locBuf,locBuf,actualHeight,actualWidth,zoomDownFactor,false);
		}



		int diffX = this->bands == 3 ? 4 : this->bands;
		int diffY = diffX * this->tileWidth;
		
		unsigned char* buf = dest.getPixUC();
		unsigned short *sbuf = (unsigned short *) dest.getPixUS();
		float *fbuf = (float *) dest.getPixF();

		for (int rBuf = 0, rImg = 0; rBuf < this->tileHeight && rImg < this->height; ++rBuf, rImg += zoomDown)
		{
			int idxBuf0 = rBuf * diffY;
			int idxImg0 = rImg * locBuf.getDiffY();
			for (int cBuf = 0, cImg = 0; cBuf < this->tileWidth && cImg < this->width; ++cBuf, cImg += zoomDown)
			{
				int idxBuf = idxBuf0 + cBuf * diffX;
				int idxImg = idxImg0 + cImg * locBuf.getDiffX();
				for (int i = 0; i < this->bands; ++i)
				{
					if (locBuf.getBpp() <= 8) 
					{
						buf[idxBuf + i] = locBuf.pixUChar(idxImg + i);
					}
					else if ((locBuf.getBpp() == 16) && locBuf.getUnsignedFlag())
					{
						sbuf[idxBuf + i] = locBuf.pixUShort(idxImg + i);
					}
					else  if (locBuf.getBpp() == 32) 
					{
						fbuf[idxBuf + i] = locBuf.pixFlt(idxImg + i);
					}
				}
			}
		}


		

    }
    else
    {
        int Z = zoomDown/2;
        if (Z == 1)
        {
			CHugeImage* himage = (CHugeImage*) this->halfRes;
			himage->getTileBuf(R,C,dest);
		
			// we need the image at level 0, only this image has the transfer function
			int zoomDownFactor = 1;
			himage = himage->getLevelNUllImage(zoomDownFactor);
			if (himage && himage->getTransferFunction())
			{
				// correct the offset: 
				// getTileBuf writes TileRow*width as offset
				// getRect writes just row as offset
				// applyTransferFunction expects row as offset
				dest.setOffset(dest.getOffsetX()/ dest.getWidth(),dest.getOffsetY()/ dest.getHeight());
				
				// width and height of a tile will always be 512x512 but the actual image might not go so far
				int actualWidth = dest.getWidth();
				int actualHeight = dest.getHeight();

				if (((actualWidth + dest.getOffsetX())) > this->halfRes->width)
					actualWidth -= actualWidth + dest.getOffsetX() - this->halfRes->width;

				if (((actualHeight + dest.getOffsetY())) > this->halfRes->height)
					actualHeight -= actualHeight + dest.getOffsetY() - this->halfRes->height;

				himage->transferFunction->applyTransferFunction(dest,dest,actualHeight,actualWidth,zoomDownFactor,false);
			}


        }
        else
        {
            this->halfRes->getTileZoomDown(dest, R, C, Z);
        }
    }

    return true;
}

CFileName* CHugeImage::getFileName()
{
    return &this->filename;
}
const char *CHugeImage::getFileNameChar() const
{
	return this->filename;
};

void CHugeImage::setFileName(const char* filename)
{
    this->filename = filename;
}

void CHugeImage::addHUGExtension()
{
	if ( this->filename.IsEmpty() )
	{
		CCharString hug(this->getFileNameChar());
		hug += ".hug";
		this->filename = hug;
	}
	else
	{
		this->filename += ".hug";
	}
}

void CHugeImage::addHUGExtension(const char* filename)
{
	this->setFileName(filename);
	this->addHUGExtension();
}


/**
 * protected function to compute the header size
 * @return the size (int bytes) of the header
 */
file_offset CHugeImage::headerSize()
{
    file_offset size = 9*sizeof(long)+sizeof(file_offset);

    for (int i = 0; i < this->bands; i++)
    {
        if (this->bpp == 8)
            size += (file_offset)256*sizeof(unsigned long);
        else
            size += (file_offset)65536*sizeof(unsigned long);
    }

    return size;
}
bool CHugeImage::imageRegistration(CHugeImage* rgb, CHugeImage* pan)
{


return true;

}

bool CHugeImage::getTileBuf(int r, int c, CImageBuffer &buffer)
{
	bool ret = true;

	CImageTile* imageTile;

    if (!this->getImageTile(&imageTile, r, c)) ret = false;
	else
	{
		// the offset used to be wrong when r and c are not exactly the start row and col of a tile
		//long offsetX = c * this->tileWidth;
		//long offsetY = r * this->tileHeight;	
		
		// compute the correct offset with the imageTile row and col
		long offsetX = imageTile->getTileCol() * this->tileWidth  * this->tileWidth;
		long offsetY = imageTile->getTileRow() * this->tileHeight * this->tileHeight;

		if ((buffer.getWidth() != this->tileWidth) || (buffer.getHeight() != this->tileHeight) ||
			(buffer.getBands() != this->bands) || (buffer.getBpp() != this->bpp) ||
			((buffer.getBpp() == 16) && (!buffer.getUnsignedFlag())))
		{
			ret = buffer.set(offsetX, offsetY, this->tileWidth, this->tileHeight, this->bands, this->bpp, true);
		}
		else ret = buffer.setOffset(offsetX, offsetY);

		if (ret)
		{
			unsigned char *buf1 = imageTile->getData();
			unsigned char *buf2;
			if (buffer.getBpp() == 8) buf2 = buffer.getPixUC();
			else if (buffer.getBpp() == 16) buf2 = (unsigned char *) buffer.getPixUS();
			else buf2 = (unsigned char *) buffer.getPixF();
			memcpy(buf2,buf1, buffer.getBufferLength());
		}
	}

    return ret;
};

bool CHugeImage::prepareWriting(const char* filename, int w, int h, int bpp, int bands)
{
	bool ret = openOnly(filename, w, h, bpp, bands);
	if (ret)
	{
		initAndWriteHeader();
		for (int i = 0; i < this->bands; i++)
		{
			this->Histograms[i]->reset();
		}
	}

	return ret;
};


bool CHugeImage::dumpTile(const CImageBuffer &tile)
{
	bool ret = true;
	if (this->bpp == 8) this->dumpTile(tile.getPixUC());
	else if (this->bpp == 16) this->dumpTile((unsigned char *) tile.getPixUS());
	else if (this->bpp == 32) this->dumpTile((unsigned char *) tile.getPixF());
	else ret = false;

	this->updateHistograms(tile);

	return ret;
}

bool CHugeImage::dumpTile(unsigned char *tile)
{
	if ((!this->fp) || (!tile)) return false;

	fwrite(tile, sizeof(unsigned char), this->getTileBytes(), this->fp);
	return true;
};

bool CHugeImage::finishWriting()
{
	this->writeHistograms();

	bool ret = true;

    if (this->requiresSubImage())
	{
		this->resetTiles();
		ret = createSubImages(false);
	}
	
	this->closeHugeFile();
	// this will initialize the image properly
	this->open(this->getFileNameChar(),true);
	this->closeHugeFile();

	return ret;
};

void CHugeImage::setProgressValue(float p)
{
	if ( this->listener != NULL ) this->listener->setProgressValue(p);
};

bool CHugeImage::updateHistograms (const CImageBuffer &buf)
{
	if (buf.getBpp() != this->bpp) return false;


	unsigned int rMax = this->height - buf.getOffsetY();
	unsigned int cMax = this->width  - buf.getOffsetX();

	if (this->bpp == 8)
	{
		for (unsigned long idxM0 = 0, r = 0; (r < rMax) && (idxM0 < buf.getNGV()); idxM0  += buf.getDiffY(), r++)
		{
			unsigned long idxMMx = idxM0 + buf.getDiffY();
			for (unsigned long idxM = idxM0, c = 0; (c < cMax) && (idxM < idxMMx); idxM  += buf.getDiffX(), c++)
			{
				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{
					this->Histograms[ibnd]->increment(buf.pixUChar(idxM + ibnd));
				}
			}
		}
	}
	else if (this->bpp == 16)
	{
		for (unsigned long idxM0 = 0, r = 0; (r < rMax) && (idxM0 < buf.getNGV()); idxM0  += buf.getDiffY(), r++)
		{
			unsigned long idxMMx = idxM0 + buf.getDiffY();
			for (unsigned long idxM = idxM0, c = 0; (c < cMax) && (idxM < idxMMx); idxM  += buf.getDiffX(), c++)
			{
				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{
					this->Histograms[ibnd]->increment(buf.pixUShort(idxM + ibnd));
				}
			}
		}
	}
	else if (this->bpp == 32)
	{
		for (unsigned long idxM0 = 0, r = 0; (r < rMax) && (idxM0 < buf.getNGV()); idxM0  += buf.getDiffY(), r++)
		{
			unsigned long idxMMx = idxM0 + buf.getDiffY();
			for (unsigned long idxM = idxM0, c = 0; (c < cMax) && (idxM < idxMMx); idxM  += buf.getDiffX(), c++)
			{
				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{
					float val = buf.pixFlt(idxM + ibnd);
					if (val != -9999.0)
						this->Histograms[ibnd]->checkFltMinMax(val);
				}
			}
		}
	}

	for (int ibnd = 0; ibnd < this->bands; ++ibnd)
	{
		this->Histograms[ibnd]->updateMinMax(0.02,0.98);
	}

	return true;
};

bool CHugeImage::resample(const char *fileName, long width, long height, CHugeImage *input, const CTrans2D &trafo, eResamplingType resType)
{
	if (!prepareWriting(fileName, width, height, input->bpp, input->bands)) return false;

	float nTiles = float(this->tilesDown * this->tilesAcross);

	CImageBuffer outputBuffer(0, 0, this->tileWidth, this->tileHeight, this->bands, this->bpp, true);

    for(int TR = 0; TR < this->height; TR += this->tileHeight)
	{
        for (int TC = 0; TC < this->width; TC += this->tileWidth)
        {
			outputBuffer.setOffset(TC, TR);
			int offsetx, offsety, inputHeight, inputWidth;
			trafo.transformWindow(TC, TR,  this->tileWidth,  this->tileHeight,
								  offsetx, offsety, inputWidth, inputHeight,
								  input->width,  input->height, 4, 4);

			if ((inputWidth <= 0) || (inputHeight <= 0)) outputBuffer.setAll(0);
			else
			{
				CImageBuffer inputBuffer (offsetx, offsety, inputWidth, inputHeight, this->bands, this->bpp, true);
				input->getRect(inputBuffer);
				outputBuffer.resample(inputBuffer, trafo, resType);
			}

			if (this->bpp == 8) this->dumpTile(outputBuffer.getPixUC());
			else this->dumpTile((unsigned char *) outputBuffer.getPixUS());

			this->updateHistograms(outputBuffer);

			float pvalue = (float)((TR / this->tileHeight) * this->tilesAcross + (TC / this->tileWidth)) / nTiles;

			if ( this->listener != NULL )
			{
				this->listener->setProgressValue(pvalue);
			}
		}
    }

	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(1);
	}

    return finishWriting();
}

void CHugeImage::getCovar(SymmetricMatrix &Covariance)
{
	Covariance.ReSize(this->bands);
	Covariance = 0.0;
	CImageBuffer buf(0, 0, this->tileWidth, this->tileHeight, this->bands, this->bpp, true);

	float nTiles = 2.0f * float(this->tilesDown * this->tilesAcross);

	float *meanVal = new float[this->bands];
	float *colourVec = new float[this->bands];

	int l = this->bands * (this->bands + 1) / 2;
	double *Cmat = new double[this->bands * (this->bands + 1) / 2];
	for (int i = 0; i < l; ++i) Cmat[i] = 0.0;


	for (int i = 0; i < this->bands; ++i)
	{
		meanVal[i] = this->Histograms[i]->mean();
	}

	for(int TR = 0; TR < this->height; TR += this->tileHeight)
	{
        for (int TC = 0; TC < this->width; TC += this->tileWidth)
        {
			this->getTileBuf(TR, TC, buf);

			unsigned int rMax = this->height - buf.getOffsetY();
			unsigned int cMax = this->width  - buf.getOffsetX();

			for (unsigned long idxM0 = 0, r = 0; (r < rMax) && (idxM0 < buf.getNGV()); idxM0  += buf.getDiffY(), r++)
			{
				unsigned long idxMMx = idxM0 + buf.getDiffY();
				for (unsigned long idxM = idxM0, c = 0; (c < cMax) && (idxM < idxMMx); idxM  += buf.getDiffX(), c++)
				{
					buf.getColourVec(idxM, colourVec);
					for (int ibnd = 0; ibnd < this->bands; ++ibnd)
					{
						colourVec[ibnd] -= meanVal[ibnd];
					}
					int icmt = 0;
					for (int ibnd = 0; ibnd < this->bands; ++ibnd)
					{
						for (int jbnd = ibnd; jbnd < this->bands; ++jbnd, ++icmt)
						{
							Cmat[icmt] += colourVec[ibnd] * colourVec[jbnd];
						}
					}

				}
			}


			float pvalue = (float)((TR / this->tileHeight) * this->tilesAcross + (TC / this->tileWidth)) / nTiles;

			if ( this->listener != NULL )
			{
				this->listener->setProgressValue(pvalue);
			}
		}
    }

	double npix = 1.0 / double(this->width * this->height - 1);

	for (int i = 0; i < l; ++i) Cmat[i] *= npix;

	int icmt = 0;
	for (int ibnd = 0; ibnd < this->bands; ++ibnd)
	{
		for (int jbnd = ibnd; jbnd < this->bands; ++jbnd, ++icmt)
		{
			Covariance.element(ibnd, jbnd) = Cmat[icmt];
		}
	}

	delete [] meanVal;
	delete [] Cmat;
	delete [] colourVec;
};

/**
 * create a pan sharpened image in "this"
 * the given rgb and pan images must be of the same resolution (for now)
 * @param rgb the rgb source image
 * @param pan the pan source image
 * @param kernelSize the size of the processing kernel
 * @return false for failure
 */
bool CHugeImage::panSharpen(CHugeImage* multi, CHugeImage* pan, float correctIR, bool PCA, const CTrans2D *trafo, eResamplingType resType)
{
	if ((multi->bands < 3)  || (multi->bands > 4) || (pan->bands != 1) ||
		(((multi->width != pan->width) || (multi->height != pan->height)) && (!trafo)) ||
		(multi->bpp != pan->bpp) || (this->bands != multi->bands)) return false;

	const char *fncp = &(this->filename[0]);
	if (!prepareWriting(fncp, pan->width, pan->height, multi->bpp, multi->bands)) return false;

	float nTiles = float(this->tilesDown * this->tilesAcross);

	CImageBuffer panBuffer  (0, 0, this->tileWidth, this->tileHeight, pan->bands, pan->bpp, true);
	CImageBuffer multiBuffer(0, 0, this->tileWidth, this->tileHeight, multi->bands, multi->bpp, true);

	if ((multi->bands != 4) && (correctIR >= 0.0)) correctIR = -1.0;
	else if (correctIR > 1.0) correctIR = -1.0;
	float maxVal = (float) multi->getHistogram(0)->percentile(0.95);

	for (int ibnd = 1; ibnd < 3; ++ibnd)
	{
		float val = (float) multi->getHistogram(ibnd)->percentile(0.95);
		if (val > maxVal) maxVal = val;
	}

	float panFactor = (float) 0.85;

	panFactor = maxVal / (panFactor * pan->getHistogram()->percentile(0.95));

	if (panFactor > 1) panFactor = 1;
	else if (panFactor < 0.5f) panFactor = 0.5f;

	double *rotPCA = 0;
	float  *meanValPCA = 0;
	float  PanMean = 0;

	if (PCA)
	{
		SymmetricMatrix Covariance;
		multi->getCovar(Covariance);

		DiagonalMatrix  eigenVal(multi->bands);   // eigen values
		Matrix          eigenVec(multi->bands, multi->bands); // eigen vectors


		Jacobi(Covariance, eigenVal, eigenVec);     // compute eigen values and vectors

		meanValPCA = new float[this->bands];

		for (int i = 0; i < this->bands; ++i)
		{
			meanValPCA[i] = multi->Histograms[i]->mean();
		}

		PanMean = pan->getHistogram()->mean();

		rotPCA = new double [this->bands * this->bands];
		for (int ibnd = 0; ibnd < this->bands; ++ibnd)
		{
			int iPCA = this->bands * ibnd;
			for (int jbnd = 0; jbnd <  this->bands; ++jbnd)
			{
				rotPCA[iPCA + jbnd] = eigenVec.element(ibnd, jbnd);
			}
		}
	};

    for(int TR = 0; TR < this->height; TR += this->tileHeight)
	{
        for (int TC = 0; TC < this->width; TC += this->tileWidth)
        {
			bool res = pan->getTileBuf(TR, TC, panBuffer);
			if (trafo)
			{
				multiBuffer.setOffset(TC, TR);
				res &= multi->getRect(multiBuffer, trafo, resType);
			}
			else res &= multi->getTileBuf(TR, TC, multiBuffer);

			if (res)
			{
				if (PCA) res = multiBuffer.pansharpen(panBuffer, rotPCA, meanValPCA, PanMean);
				else res = multiBuffer.pansharpen1(panBuffer, correctIR, panFactor);
			}
			if (res)
			{
				if (this->bpp == 8)
				{
					this->dumpTile(multiBuffer.getPixUC());

				}
				else
				{
					this->dumpTile((unsigned char *) multiBuffer.getPixUS());
				}

				this->updateHistograms(multiBuffer);
			}

			float pvalue = (float)((TR / this->tileHeight) * this->tilesAcross + (TC / this->tileWidth)) / nTiles;

			if ( this->listener != NULL )
			{
				this->listener->setProgressValue(pvalue);
			}
		}
    }

	if (meanValPCA) delete [] meanValPCA;
	if (rotPCA) delete [] rotPCA;

	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(1);
	}

    return finishWriting();
}

//addition >>
/*
This function detects and tracks edges on a grey scale image.
*/
bool CHugeImage::detectAndTrackEdges(CXYContourArray *xycontour, CImageBuffer *edgeBuf, int minLength)
{
	if (this->bands > 1)  return false;

//get grey-scale image in a buffer
	CImageBuffer GreyBuffer;
	this->getRect(GreyBuffer,0,this->height,0,this->width);
	//extract edges
	if (this->listener)
	 {
		 this->listener->setTitleString("Extracting edges ...");
		 this->listener->setProgressValue(2);
	 }

	//define and convert the grey-scale image to an image in openCV format
	//IplImage *cvgrey = this->convertToOpenCVimage();	
	int depth;
	if (this->bpp == 8)
		depth = IPL_DEPTH_8U;
	else if (this->bpp == 16)
		depth = IPL_DEPTH_16U;

	// Pixel depth in bits: 
    //   IPL_DEPTH_8U, IPL_DEPTH_8S, 
    //   IPL_DEPTH_16U,IPL_DEPTH_16S, 
    //   IPL_DEPTH_32S,IPL_DEPTH_32F, 
    //   IPL_DEPTH_64F

	IplImage *cvgrey = cvCreateImage(cvSize(this->width,this->height),depth,this->bands);
	CImageBuffer buf;
	unsigned char pixel; 
	//for single band, 8 bit image
	if (this->bpp == 8 && this->bands == 1)
	{
		int step = this->width;//cvgrey->widthStep/sizeof(uchar);//it seems cvgrey->widthStep = this->width+1
		//uchar* data    = (uchar *)cvgrey->imageData;
		for (long r = 0; r < this->height; r++)
		{
			//long rDataSize = r*step;
			for (long c = 0; c < this->width; c++)
			{
				//get the pixel from this image
				this->getRect(buf,r,1,c,1);						
				pixel = buf.pixUChar(0);						
				//assign this pixel value to greyOpenCV image										
				//data[rDataSize+c] = (unsigned char) pixel;
				((uchar *)(cvgrey->imageData + r*cvgrey->widthStep))[c] = pixel;				

			}
		}
	}

    IplImage  *edges = cvCreateImage(cvGetSize(cvgrey), IPL_DEPTH_8U, 1);
	cvCanny( cvgrey, edges, 50, 200, 3);
	if (this->listener)
	 {
		 //this->listener->setTitleString("Extracting edges ...");
		 this->listener->setProgressValue(100);
	 }
	//test
	//			int height1     = edges->height;
	//			int width1      = edges->width;
	//			int step1       = edges->widthStep/sizeof(uchar);

	if (this->listener)
	 {
		 this->listener->setTitleString("Constructing edge image ...");
		 this->listener->setProgressValue(0);
	 }
	// track edges
	//possibly add and show this edge-image in Barista
	int Gap_size = 1, step, stepb;	
	//CImageBuffer edgeBuf (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);//(0, 0, this->width, this->height, this->bands, 16, true, 0);
	CImageBuffer edgeBuffer (0, 0, this->width+2*Gap_size, this->height+2*Gap_size, this->bands, 8, false, 0);
	double tPixel = this->height*this->width;
	if (this->bpp == 8 && this->bands == 1)
	{
		step = this->width;//edges->widthStep/sizeof(uchar);//it seems edges->widthStep = this->width+1
		stepb = step+2*Gap_size;
		//uchar* data    = (uchar *)edges->imageData;
		for (long r = 0; r < this->height; r++)
		{
			//long rDataSize = r*step;
			long rDataSizeBuf = (r+Gap_size)*stepb;
			long ind;
			for (long c = 0; c < this->width; c++)
			{
				//get pixel value from the edge image
				//long ind = rDataSize+c;
				//unsigned char p = data[ind];
				unsigned char p = ((uchar *)(edges->imageData + r*edges->widthStep))[c];
				//set the pixel from to the Barista edge image				
				ind = rDataSizeBuf+(c+Gap_size);				
				edgeBuffer.pixUChar(ind) = p;						
				//if ( p > 255)
				//	int h = 1;
			}
			double pval = (double)(ind+1)/tPixel;
			if (this->listener)
			 {
				 //this->listener->setTitleString("Constructing edge image ...");
				 this->listener->setProgressValue(pval*100);
			 }
		}
	}
	
	//CLookupTable luted(8,3);
	//ret = edgeImg->dumpImage(edgeImg->getFileNameChar(), edgeBuffer, &luted);
	//if (!ret) return false;
	

	
	if (this->listener)
	{
		this->listener->setTitleString("Tracking edges ...");
		this->listener->setProgressValue(0);
	}

	//CXYContourArray xycontour;
	CXYContour cont1, cont2;

	xycontour->RemoveAll();
	long numCont = 0;
	float numContAccepted = 0;
	long nPixels = edgeBuffer.getNGV();
	if (this->bpp == 8 && this->bands == 1)
	{
		long r, c, nextR, nextC, ind, indo;
		long ht = edgeBuffer.getHeight();
		long wd = edgeBuffer.getWidth();
		//int step = edges->widthStep/sizeof(uchar);
		//uchar* data    = (uchar *)edges->imageData;
		//find first position of edge pixel
		long oldR = Gap_size, oldC = Gap_size;
		while(1)//while outer
		{
			cont1.deleteContour();
			cont2.deleteContour();

			bool found = false;
			
			for (r = Gap_size; r < ht-Gap_size; r++)
			{
				long rDataSize = r*stepb;
				for (c = Gap_size; c < wd-Gap_size; c++)
				{
					//get pixel value from the edge image
					ind = rDataSize+c;
					if (ind >= nPixels)
						break;					
					unsigned char p = edgeBuffer.pixUChar(ind);
					if (p == 255)
					{
						found = true;
						break;
					}						
				}
				if (found)
					break;
			}
			if (found) // a contour point is found, so build the contour with neighbour edge points
			{
				//preserve old r and c
				oldR = r;
				oldC = c;

				//change info for next contour								
				edgeBuffer.pixUChar(ind) = 0;
				found = false;

				//build current contour in a direction
				C2DPoint newpoint;
				newpoint.x = c;
				newpoint.y = r;
				cont1.addPoint(newpoint);
				cont2.addPoint(newpoint);
				
				while (1)//while inner 1
				{
					bool notFound = false;
					indo = ind;//preserve current pixel position

					//check point on top of the current point
					nextR = r-1;
					nextC = c;
					//ind = nextR*stepb+nextC;//
					ind = indo - stepb;//up neighbour
					//if (ind >= nPixels)
					//	break;
					if (edgeBuffer.pixUChar(ind) != 255)//if 1
					{
						//check point on left of the current point
						nextR = r;
						nextC = c-1;
						//ind = nextR*stepb+nextC;//
						ind = indo -1;//left neighbour
						//if (ind >= nPixels)
						//	break;
						if (edgeBuffer.pixUChar(ind) != 255)//if 2
						{
							//check point on right of the current point
							nextR = r;
							nextC = c+1;
							//ind = nextR*stepb+nextC;//
							ind = indo +1;//right neighbour
							//if (ind >= nPixels)
							//	break;
							if (edgeBuffer.pixUChar(ind) != 255)//if 3
							{
								//check point on bottom of the current point
								nextR = r+1;
								nextC = c;
								//ind = nextR*stepb+nextC;//
								ind = indo + stepb;//down neighbour
								//if (ind >= nPixels)
								//	break;
								if (edgeBuffer.pixUChar(ind) != 255)//if 4
								{
									//check point on top-left of the current point
									nextR = r-1;
									nextC = c-1;
									//ind = nextR*stepb+nextC;//
									ind = indo - stepb - 1;//up-left neighbour
									//if (ind >= nPixels)
									//	break;
									if (edgeBuffer.pixUChar(ind) != 255)//if 5
									{
										//check point on top-right of the current point
										nextR = r-1;
										nextC = c+1;
										//ind = nextR*stepb+nextC;//
										ind = indo - stepb + 1;//up-right neighbour
										//if (ind >= nPixels)
										//	break;
										if (edgeBuffer.pixUChar(ind) != 255)//if 6
										{
											//check point on bottom-left of the current point
											nextR = r+1;
											nextC = c-1;
											//ind = nextR*stepb+nextC;//
											ind = indo + stepb - 1;//down-left neighbour
											//if (ind >= nPixels)
											//	break;
											if (edgeBuffer.pixUChar(ind) != 255)//if 7
											{
												//check point on bottom-right of the current point
												nextR = r+1;
												nextC = c+1;
												//ind = nextR*stepb+nextC;//
												ind = indo + stepb + 1;//down-right neighbour
												//if (ind >= nPixels)
												//	break;
												if (edgeBuffer.pixUChar(ind) != 255)//if 8
												{
													notFound = true;
												}//if 8
											}//if 7
										}//if 6
									}//if 5
								}//if 4
							}//if 3
						}//if 2
					}//if 1

					//test
					//FILE *fp = fopen("c:\\test.txt","w");
					//fprintf(fp,"%d\t%d\t%d\t%d\t%d\t%ld",numCont,r,c,nextR,nextC,ind);
					//fclose(fp);
					//if (numCont == 21 && r == 214 && c == 62 && nextR == 215 && nextC == 62)
					//	int here = 1;
					//

					if (!notFound)//an nearest edge-point was found
					{
						r = nextR;
						c = nextC;
						newpoint.x = c;
						newpoint.y = r;
						cont1.addPoint(newpoint);
						edgeBuffer.pixUChar(ind) = 0;
					}
					else//end of building edge from this end
						break; 					
				}//while inner	1		

				//build current contour in another direction
				newpoint = *cont2.getPoint(0);
				r = (long)newpoint.y;
				c = (long)newpoint.x;
				ind = r*stepb + c;
				while (1)//while inner 2
				{
					bool notFound = false;
					indo = ind;//preserve current pixel position

					//check point on top of the current point
					nextR = r-1;
					nextC = c;
					//ind = nextR*stepb+nextC;
					ind = indo - stepb;
					//if (ind >= nPixels)
					//	break;
					if (edgeBuffer.pixUChar(ind) != 255)//if 1
					{
						//check point on left of the current point
						nextR = r;
						nextC = c-1;
						//ind = nextR*stepb+nextC;
						ind = indo - 1;
						//if (ind >= nPixels)
						//	break;
						if (edgeBuffer.pixUChar(ind) != 255)//if 2
						{
							//check point on right of the current point
							nextR = r;
							nextC = c+1;
							//ind = nextR*stepb+nextC;
							ind = indo + 1;
							//if (ind >= nPixels)
							//	break;
							if (edgeBuffer.pixUChar(ind) != 255)//if 3
							{
								//check point on bottom of the current point
								nextR = r+1;
								nextC = c;
								//ind = nextR*stepb+nextC;
								ind = indo + stepb;
								//if (ind >= nPixels)
								//	break;
								if (edgeBuffer.pixUChar(ind) != 255)//if 4
								{
									//check point on top-left of the current point
									nextR = r-1;
									nextC = c-1;
									//ind = nextR*stepb+nextC;
									ind = indo - stepb - 1;
									//if (ind >= nPixels)
									//	break;
									if (edgeBuffer.pixUChar(ind) != 255)//if 5
									{
										//check point on top-right of the current point
										nextR = r-1;
										nextC = c+1;
										//ind = nextR*stepb+nextC;
										ind = indo - stepb + 1;
										//if (ind >= nPixels)
										//	break;
										if (edgeBuffer.pixUChar(ind) != 255)//if 6
										{
											//check point on bottom-left of the current point
											nextR = r+1;
											nextC = c-1;
											//ind = nextR*stepb+nextC;
											ind = indo + stepb - 1;
											//if (ind >= nPixels)
											//	break;
											if (edgeBuffer.pixUChar(ind) != 255)//if 7
											{
												//check point on bottom-right of the current point
												nextR = r+1;
												nextC = c+1;
												//ind = nextR*stepb+nextC;
												ind = indo + stepb + 1;
												//if (ind >= nPixels)
												//	break;
												if (edgeBuffer.pixUChar(ind) != 255)//if 8
												{
													notFound = true;
												}//if 8
											}//if 7
										}//if 6
									}//if 5
								}//if 4
							}//if 3
						}//if 2
					}//if 1

					//test
					//FILE *fp = fopen("c:\\test1.txt","w");
					//fprintf(fp,"%d\t%d\t%d\t%d\t%d\t%ld",numCont,r,c,nextR,nextC,ind);
					//fclose(fp);
					//if (numCont == 21 && r == 106 && c == 100 && nextR == 107 && nextC == 101)
					//	int here = 1;
					//

					if (!notFound)//an nearest edge-point was found
					{
						r = nextR;
						c = nextC;
						newpoint.x = c;
						newpoint.y = r;
						cont2.addPoint(newpoint);
						edgeBuffer.pixUChar(ind) = 0;
					}
					else//end of building edge from this end
						break; 					
				}//while inner 2

				//now join the two edge segments cont1 & cont2 and store in contourArray
				long len1 = cont1.getPointCount();
				long len2 = cont2.getPointCount();
				numCont += 1;
				//test
				
				//if (numCont == 21)
				//	int here = 1;
				if (len1+len2-1 > minLength)
				{
					numContAccepted += 1;
					//if (numContAccepted == 12)
					//	int here = 1;
					CXYContour *cont = xycontour->Add();
					for (int i = cont1.getPointCount()-1; i >= 0; i--)
					{
						newpoint = *cont1.getPoint(i);
						newpoint.x -= Gap_size;
						newpoint.y -= Gap_size;
						cont->addPoint(newpoint);

						r = (long) newpoint.y;
						c = (long) newpoint.x;
						ind = r*step+c;
						//if (ind >= nPixels)
						//	break;
						edgeBuf->pixFlt(ind) = numContAccepted;
					}
					for (int i = 1; i < cont2.getPointCount(); i++)
					{
						newpoint = *cont2.getPoint(i);
						newpoint.x -= Gap_size;
						newpoint.y -= Gap_size;
						cont->addPoint(newpoint);

						r = (long) newpoint.y;
						c = (long) newpoint.x;
						ind = r*step+c;
						//if (ind >= nPixels)
						//	break;
						edgeBuf->pixFlt(ind) = numContAccepted;
					}
				}//if (len1+len2 >= minLength)
			}//if (found)
			else //no edge remains
				break;

			double pval = 100* (double)((oldR-1)*stepb+oldC)/tPixel;
			if (this->listener)
			{
				//this->listener->setTitleString("Tracking edges ...");
				this->listener->setProgressValue(pval);
			}
		}//while outer

	}//if (this->bpp == 8 && this->bands == 1)

	if (this->listener)
	{
		//this->listener->setTitleString("Tracking edges ...");
		this->listener->setProgressValue(100);
	}

	return true;
}

/*
This function computes edge orientation of a grey scale image.
*/
bool CHugeImage::computeEdgeOrientation(CHugeImage* edgeOrientation, CHugeImage* edgeImg, float resImage)
{
	//#ifndef FINAL_RELEASE
	int minLen = (int) ceil(3/resImage);//at least 3m long edges

	if (this->listener) 
	{
		this->listener->setTitleString("Computing edge orientation ...");
		this->listener->setProgressValue(0);
	}

	if ((this->bands > 1) || (!edgeOrientation))  return false;

	//const char *entropyFileName = entropy->getFileNameChar();
	//const char *edgeOriFileName = edgeOrientation->getFileNameChar();

	//if (!entropy->prepareWriting(entropyFileName, this->width, this->height, 32, 1)) return false;
	//if (!edgeOrientation->prepareWriting(edgeOriFileName, this->width, this->height, 32, 1)) return false;

	float nTiles = float(this->tilesDown * this->tilesAcross);

	//CImageBuffer entropyBuffer (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);
	CImageBuffer edgeOriBuffer (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);

	//define to assign the grey-scale image to an image in openCV format
	//IplImage *greyOpenCV = cvCreateImage(cvSize(this->width,this->height),IPL_DEPTH_8U,1);
	/*
	// 4 pixels around the image will be used for symmetric padding
	long *rowIds = new long [this->height+8]; 
	long *colIds = new long [this->width+8];
	rowIds[0] = 3; rowIds[1] = 2; rowIds[2] = 1; rowIds[3] = 0;
	colIds[0] = 3; colIds[1] = 2; colIds[2] = 1; colIds[3] = 0;
	long i, curIndex = 4;
	for (i = 0; i < this->height; i++)
		rowIds[curIndex++] = i;
	rowIds[curIndex++] = this->height-1; rowIds[curIndex++] = this->height-2; rowIds[curIndex++] = this->height-3; rowIds[curIndex++] = this->height-4;
	curIndex = 4;
	for (i = 0; i < this->width; i++)
		colIds[curIndex++] = i;
	colIds[curIndex++] = this->width-1; colIds[curIndex++] = this->width-2; colIds[curIndex++] = this->width-3; colIds[curIndex++] = this->width-4;
	//now rowIds and colIds contains indices of padded image with respect to the grey-scale image

	//test
	//FILE *fp = fopen("testout.txt","w");
	//fclose(fp);
	//FILE *fp1 = fopen("testin.txt","w");
	//fclose(fp1);

		
	//works only for grey-scale image
	if (this->getBpp() == 8)
	{	
		float log2 = log((float)2.0);
		unsigned long nPixels = entropyBuffer.getNGV();
		double hist[256];
		long rPad, cPad, minR, maxR, minC, maxC;
		//find entropy for pixel at (r,c)
		for (long r = 0; r < this->height; r++)
		{	

			//row index of this pixel at padded image
			rPad = r+4;
			minR = rPad-4;//top row index for the neighbourhood in the padded image
			maxR = rPad+4;//bottom row index for the neighbourhood in the padded image
			long rDataSize = r*this->width;
			for (long c = 0; c < this->width; c++)
			{
				//col index of this pixel at padded image
				cPad = c+4;
				minC = cPad-4;//left col index for the neighbourhood in the padded image
				maxC = cPad+4;//right col index for the neighbourhood in the padded image

				//initialize the histogram before estimation for each neighbourhood around each pixel (r,c)
				for (i = 0; i < 256; i++)
					hist[i] = 0;
				
				//test
				//FILE *fp2 = fopen("testtrack.txt","w");
				//fclose(fp2);
				//FILE *fp3 = fopen("testtrackin.txt","w");
				//fclose(fp3);
				
				//if (r == 194 && c == 220)
				//	int here = 1;
		
				//get pixel values & build histogram
				int count = 0;
				for (long rP = minR; rP <= maxR; rP++)
				{
					long rowId = rowIds[rP];
					for (long cP = minC; cP <= maxC; cP++)
					{										
						long colId = colIds[cP];
						unsigned short pixel;
						//access the pixel						
						CImageBuffer buf;

						//test						
						//FILE *fp3 = fopen("testtrackin.txt","a");
						//fprintf(fp3,"%d\t%d\t%d\t%d\t%d\t%d\n",r,c,rP,cP,rowId,colId);
						//fclose(fp3);

						this->getRect(buf,rowId,1,colId,1);						
						pixel = buf.pixUChar(0);
						
						//assign this pixel value to greyOpenCV
						//int height     = greyOpenCV->height;
						//int width      = greyOpenCV->width;
						//int step       = greyOpenCV->widthStep/sizeof(uchar);
						//uchar* data    = (uchar *)greyOpenCV->imageData;
						//data[r*step+c] = (unsigned char) pixel;					
						

						//test
						//FILE *fp2 = fopen("testtrack.txt","a");
						//fprintf(fp2,"%d\t%d\t%d\t%d\t%d\t%d\t%d\n",r,c,rP,cP,rowId,colId,pixel);
						//fclose(fp2);

						//update the histogram
						hist[pixel]++;
						count++;
					}//for (long cP = minC
				}//for (long rP = minR

				// now we have the histogram to calculate entropy at (r,c)
				// avoid zero frequencies
				double ent = 0.0;
				for (i = 0; i < 256; i++)
				{
					if (hist[i] != 0)
					{
						hist[i] /= count; //normalize the frequency by dividing the total pixels in the neighbourhood
						//the math.h function log() calculates natural logarithm; so we need to convert log(hist[i]) to log_2(hist[i])
						ent += hist[i]*log(hist[i])/log2; //log_2(hist[i]) = log(hist[i])/log(2)
					}
				}
				//long indexEntropy = r*this->width + c;
				ent = -ent;
				long entIndex = rDataSize + c;//rDataSize = r*this->width;

				//test
				//FILE *fp1 = fopen("testin.txt","a");
				//fprintf(fp1,"%d\t%d\t%3.5f\n",r,c,ent);
				//fclose(fp1);

				entropyBuffer.pixFlt(entIndex) = (float)ent;

				//test				
				FILE *fp = fopen("testout.txt","a");
				fprintf(fp,"%d\t%d\t%3.5f\n",r,c,ent);
				fclose(fp);

				//if (r == 231 && c == 467)
				//	int here = 1;
				
				//if (r == 231 && c == 467)
				//	int here = 1;

				float pvalue = (float)((entIndex+1)/nPixels);

				if ( this->listener != NULL )
				{
					this->listener->setProgressValue(100*pvalue);
				}

			}//for (long c = 0; 
		}//for (long r = 0;

	}//if (this->getBpp() == 8)
	else
		return false;
	
	CLookupTable lut(8,3);
	bool ret = entropy->dumpImage(entropy->getFileNameChar(), entropyBuffer, &lut);
	if (!ret) return false;
	
	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(100);
	}
	
	// free memories
	if (rowIds)
	{
		delete []rowIds;
		rowIds = NULL;
	}
	if (colIds)
	{
		delete []colIds;
		colIds = NULL;
	}
	

	if (this->listener) 
	{
		this->listener->setTitleString("Calculation of edge orientation ...");
		this->listener->setProgressValue(1);
	}

*/
	CXYContourArray xycontour;
	CImageBuffer edgeBuf (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);//(0, 0, this->width, this->height, this->bands, 16, true, 0);
	detectAndTrackEdges(&xycontour, &edgeBuf, minLen);

	/*
	//get grey-scale image in a buffer
	CImageBuffer GreyBuffer;
	this->getRect(GreyBuffer,0,this->height,0,this->width);
	//extract edges
	if (this->listener)
	 {
		 this->listener->setTitleString("Extracting edges ...");
		 this->listener->setProgressValue(2);
	 }

	//define and convert the grey-scale image to an image in openCV format
	//IplImage *cvgrey = this->convertToOpenCVimage();	
	int depth;
	if (this->bpp == 8)
		depth = IPL_DEPTH_8U;
	else if (this->bpp == 16)
		depth = IPL_DEPTH_16U;

	// Pixel depth in bits: 
    //   IPL_DEPTH_8U, IPL_DEPTH_8S, 
    //   IPL_DEPTH_16U,IPL_DEPTH_16S, 
    //   IPL_DEPTH_32S,IPL_DEPTH_32F, 
    //   IPL_DEPTH_64F

	IplImage *cvgrey = cvCreateImage(cvSize(this->width,this->height),depth,this->bands);
	CImageBuffer buf;
	unsigned char pixel; 
	//for single band, 8 bit image
	if (this->bpp == 8 && this->bands == 1)
	{
		int step = this->width;//cvgrey->widthStep/sizeof(uchar);//it seems cvgrey->widthStep = this->width+1
		//uchar* data    = (uchar *)cvgrey->imageData;
		for (long r = 0; r < this->height; r++)
		{
			//long rDataSize = r*step;
			for (long c = 0; c < this->width; c++)
			{
				//get the pixel from this image
				this->getRect(buf,r,1,c,1);						
				pixel = buf.pixUChar(0);						
				//assign this pixel value to greyOpenCV image										
				//data[rDataSize+c] = (unsigned char) pixel;
				((uchar *)(cvgrey->imageData + r*cvgrey->widthStep))[c] = pixel;				

			}
		}
	}

    IplImage  *edges = cvCreateImage(cvGetSize(cvgrey), IPL_DEPTH_8U, 1);
	cvCanny( cvgrey, edges, 50, 200, 3);
	if (this->listener)
	 {
		 //this->listener->setTitleString("Extracting edges ...");
		 this->listener->setProgressValue(100);
	 }
	//test
	//			int height1     = edges->height;
	//			int width1      = edges->width;
	//			int step1       = edges->widthStep/sizeof(uchar);

	if (this->listener)
	 {
		 this->listener->setTitleString("Constructing edge image ...");
		 this->listener->setProgressValue(0);
	 }
	// track edges
	//possibly add and show this edge-image in Barista
	int Gap_size = 1, minLength = 20, step, stepb;	
	//CImageBuffer edgeBuf (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);//(0, 0, this->width, this->height, this->bands, 16, true, 0);
	CImageBuffer edgeBuffer (0, 0, this->width+2*Gap_size, this->height+2*Gap_size, this->bands, 8, false, 0);
	double tPixel = this->height*this->width;
	if (this->bpp == 8 && this->bands == 1)
	{
		step = this->width;//edges->widthStep/sizeof(uchar);//it seems edges->widthStep = this->width+1
		stepb = step+2*Gap_size;
		//uchar* data    = (uchar *)edges->imageData;
		for (long r = 0; r < this->height; r++)
		{
			//long rDataSize = r*step;
			long rDataSizeBuf = (r+Gap_size)*stepb;
			long ind;
			for (long c = 0; c < this->width; c++)
			{
				//get pixel value from the edge image
				//long ind = rDataSize+c;
				//unsigned char p = data[ind];
				unsigned char p = ((uchar *)(edges->imageData + r*edges->widthStep))[c];
				//set the pixel from to the Barista edge image				
				ind = rDataSizeBuf+(c+Gap_size);				
				edgeBuffer.pixUChar(ind) = p;						
				//if ( p > 255)
				//	int h = 1;
			}
			double pval = (double)(ind+1)/tPixel;
			if (this->listener)
			 {
				 //this->listener->setTitleString("Constructing edge image ...");
				 this->listener->setProgressValue(pval*100);
			 }
		}
	}
	
	//CLookupTable luted(8,3);
	//ret = edgeImg->dumpImage(edgeImg->getFileNameChar(), edgeBuffer, &luted);
	//if (!ret) return false;
	

	
	if (this->listener)
	{
		this->listener->setTitleString("Tracking edges ...");
		this->listener->setProgressValue(0);
	}

	//CXYContourArray xycontour;
	CXYContour cont1, cont2;

	xycontour.RemoveAll();
	long numCont = 0;
	float numContAccepted = 0;
	long nPixels = edgeBuffer.getNGV();
	if (this->bpp == 8 && this->bands == 1)
	{
		long r, c, nextR, nextC, ind, indo;
		long ht = edgeBuffer.getHeight();
		long wd = edgeBuffer.getWidth();
		//int step = edges->widthStep/sizeof(uchar);
		//uchar* data    = (uchar *)edges->imageData;
		//find first position of edge pixel
		long oldR = Gap_size, oldC = Gap_size;
		while(1)//while outer
		{
			cont1.deleteContour();
			cont2.deleteContour();

			bool found = false;
			
			for (r = Gap_size; r < ht-Gap_size; r++)
			{
				long rDataSize = r*stepb;
				for (c = Gap_size; c < wd-Gap_size; c++)
				{
					//get pixel value from the edge image
					ind = rDataSize+c;
					if (ind >= nPixels)
						break;					
					unsigned char p = edgeBuffer.pixUChar(ind);
					if (p == 255)
					{
						found = true;
						break;
					}						
				}
				if (found)
					break;
			}
			if (found) // a contour point is found, so build the contour with neighbour edge points
			{
				//preserve old r and c
				oldR = r;
				oldC = c;

				//change info for next contour								
				edgeBuffer.pixUChar(ind) = 0;
				found = false;

				//build current contour in a direction
				C2DPoint newpoint;
				newpoint.x = c;
				newpoint.y = r;
				cont1.addPoint(newpoint);
				cont2.addPoint(newpoint);
				
				while (1)//while inner 1
				{
					bool notFound = false;
					indo = ind;//preserve current pixel position

					//check point on top of the current point
					nextR = r-1;
					nextC = c;
					//ind = nextR*stepb+nextC;//
					ind = indo - stepb;//up neighbour
					//if (ind >= nPixels)
					//	break;
					if (edgeBuffer.pixUChar(ind) != 255)//if 1
					{
						//check point on left of the current point
						nextR = r;
						nextC = c-1;
						//ind = nextR*stepb+nextC;//
						ind = indo -1;//left neighbour
						//if (ind >= nPixels)
						//	break;
						if (edgeBuffer.pixUChar(ind) != 255)//if 2
						{
							//check point on right of the current point
							nextR = r;
							nextC = c+1;
							//ind = nextR*stepb+nextC;//
							ind = indo +1;//right neighbour
							//if (ind >= nPixels)
							//	break;
							if (edgeBuffer.pixUChar(ind) != 255)//if 3
							{
								//check point on bottom of the current point
								nextR = r+1;
								nextC = c;
								//ind = nextR*stepb+nextC;//
								ind = indo + stepb;//down neighbour
								//if (ind >= nPixels)
								//	break;
								if (edgeBuffer.pixUChar(ind) != 255)//if 4
								{
									//check point on top-left of the current point
									nextR = r-1;
									nextC = c-1;
									//ind = nextR*stepb+nextC;//
									ind = indo - stepb - 1;//up-left neighbour
									//if (ind >= nPixels)
									//	break;
									if (edgeBuffer.pixUChar(ind) != 255)//if 5
									{
										//check point on top-right of the current point
										nextR = r-1;
										nextC = c+1;
										//ind = nextR*stepb+nextC;//
										ind = indo - stepb + 1;//up-right neighbour
										//if (ind >= nPixels)
										//	break;
										if (edgeBuffer.pixUChar(ind) != 255)//if 6
										{
											//check point on bottom-left of the current point
											nextR = r+1;
											nextC = c-1;
											//ind = nextR*stepb+nextC;//
											ind = indo + stepb - 1;//down-left neighbour
											//if (ind >= nPixels)
											//	break;
											if (edgeBuffer.pixUChar(ind) != 255)//if 7
											{
												//check point on bottom-right of the current point
												nextR = r+1;
												nextC = c+1;
												//ind = nextR*stepb+nextC;//
												ind = indo + stepb + 1;//down-right neighbour
												//if (ind >= nPixels)
												//	break;
												if (edgeBuffer.pixUChar(ind) != 255)//if 8
												{
													notFound = true;
												}//if 8
											}//if 7
										}//if 6
									}//if 5
								}//if 4
							}//if 3
						}//if 2
					}//if 1

					//test
					//FILE *fp = fopen("c:\\test.txt","w");
					//fprintf(fp,"%d\t%d\t%d\t%d\t%d\t%ld",numCont,r,c,nextR,nextC,ind);
					//fclose(fp);
					//if (numCont == 21 && r == 214 && c == 62 && nextR == 215 && nextC == 62)
					//	int here = 1;
					//

					if (!notFound)//an nearest edge-point was found
					{
						r = nextR;
						c = nextC;
						newpoint.x = c;
						newpoint.y = r;
						cont1.addPoint(newpoint);
						edgeBuffer.pixUChar(ind) = 0;
					}
					else//end of building edge from this end
						break; 					
				}//while inner	1		

				//build current contour in another direction
				newpoint = *cont2.getPoint(0);
				r = (long)newpoint.y;
				c = (long)newpoint.x;
				ind = r*stepb + c;
				while (1)//while inner 2
				{
					bool notFound = false;
					indo = ind;//preserve current pixel position

					//check point on top of the current point
					nextR = r-1;
					nextC = c;
					//ind = nextR*stepb+nextC;
					ind = indo - stepb;
					//if (ind >= nPixels)
					//	break;
					if (edgeBuffer.pixUChar(ind) != 255)//if 1
					{
						//check point on left of the current point
						nextR = r;
						nextC = c-1;
						//ind = nextR*stepb+nextC;
						ind = indo - 1;
						//if (ind >= nPixels)
						//	break;
						if (edgeBuffer.pixUChar(ind) != 255)//if 2
						{
							//check point on right of the current point
							nextR = r;
							nextC = c+1;
							//ind = nextR*stepb+nextC;
							ind = indo + 1;
							//if (ind >= nPixels)
							//	break;
							if (edgeBuffer.pixUChar(ind) != 255)//if 3
							{
								//check point on bottom of the current point
								nextR = r+1;
								nextC = c;
								//ind = nextR*stepb+nextC;
								ind = indo + stepb;
								//if (ind >= nPixels)
								//	break;
								if (edgeBuffer.pixUChar(ind) != 255)//if 4
								{
									//check point on top-left of the current point
									nextR = r-1;
									nextC = c-1;
									//ind = nextR*stepb+nextC;
									ind = indo - stepb - 1;
									//if (ind >= nPixels)
									//	break;
									if (edgeBuffer.pixUChar(ind) != 255)//if 5
									{
										//check point on top-right of the current point
										nextR = r-1;
										nextC = c+1;
										//ind = nextR*stepb+nextC;
										ind = indo - stepb + 1;
										//if (ind >= nPixels)
										//	break;
										if (edgeBuffer.pixUChar(ind) != 255)//if 6
										{
											//check point on bottom-left of the current point
											nextR = r+1;
											nextC = c-1;
											//ind = nextR*stepb+nextC;
											ind = indo + stepb - 1;
											//if (ind >= nPixels)
											//	break;
											if (edgeBuffer.pixUChar(ind) != 255)//if 7
											{
												//check point on bottom-right of the current point
												nextR = r+1;
												nextC = c+1;
												//ind = nextR*stepb+nextC;
												ind = indo + stepb + 1;
												//if (ind >= nPixels)
												//	break;
												if (edgeBuffer.pixUChar(ind) != 255)//if 8
												{
													notFound = true;
												}//if 8
											}//if 7
										}//if 6
									}//if 5
								}//if 4
							}//if 3
						}//if 2
					}//if 1

					//test
					//FILE *fp = fopen("c:\\test1.txt","w");
					//fprintf(fp,"%d\t%d\t%d\t%d\t%d\t%ld",numCont,r,c,nextR,nextC,ind);
					//fclose(fp);
					//if (numCont == 21 && r == 106 && c == 100 && nextR == 107 && nextC == 101)
					//	int here = 1;
					//

					if (!notFound)//an nearest edge-point was found
					{
						r = nextR;
						c = nextC;
						newpoint.x = c;
						newpoint.y = r;
						cont2.addPoint(newpoint);
						edgeBuffer.pixUChar(ind) = 0;
					}
					else//end of building edge from this end
						break; 					
				}//while inner 2

				//now join the two edge segments cont1 & cont2 and store in contourArray
				long len1 = cont1.getPointCount();
				long len2 = cont2.getPointCount();
				numCont += 1;
				//test
				
				//if (numCont == 21)
				//	int here = 1;
				if (len1+len2-1 > minLength)
				{
					numContAccepted += 1;
					//if (numContAccepted == 12)
					//	int here = 1;
					CXYContour *cont = xycontour.Add();
					for (int i = cont1.getPointCount()-1; i >= 0; i--)
					{
						newpoint = *cont1.getPoint(i);
						newpoint.x -= Gap_size;
						newpoint.y -= Gap_size;
						cont->addPoint(newpoint);

						r = (long) newpoint.y;
						c = (long) newpoint.x;
						ind = r*step+c;
						//if (ind >= nPixels)
						//	break;
						edgeBuf.pixFlt(ind) = numContAccepted;
					}
					for (int i = 1; i < cont2.getPointCount(); i++)
					{
						newpoint = *cont2.getPoint(i);
						newpoint.x -= Gap_size;
						newpoint.y -= Gap_size;
						cont->addPoint(newpoint);

						r = (long) newpoint.y;
						c = (long) newpoint.x;
						ind = r*step+c;
						//if (ind >= nPixels)
						//	break;
						edgeBuf.pixFlt(ind) = numContAccepted;
					}
				}//if (len1+len2 >= minLength)
			}//if (found)
			else //no edge remains
				break;

			double pval = 100* (double)((oldR-1)*stepb+oldC)/tPixel;
			if (this->listener)
			{
				//this->listener->setTitleString("Tracking edges ...");
				this->listener->setProgressValue(pval);
			}
		}//while outer

	}//if (this->bpp == 8 && this->bands == 1)

	if (this->listener)
	{
		//this->listener->setTitleString("Tracking edges ...");
		this->listener->setProgressValue(100);
	}
	*/

	CLookupTable luted(8,3);
	bool ret = edgeImg->dumpImage(edgeImg->getFileNameChar(), edgeBuf, &luted);
	if (!ret) return false;

	/*
	CImageBuffer edgePixelBufferFrom(GreyBuffer);
	CImageBuffer edgePixelBufferTo;
	edgePixelBufferFrom.markEdgePixels(edgePixelBufferTo);
	CXYContourArray xycontour;
	ret = edgePixelBufferTo.getContours(xycontour);
	if (!ret) return false;
	//CLabelImage *LblImage;
	//LblImage->initFromMask(*this->gMask, 1);
	//CXYContourArray xycontour;
	//bool ret = LblImage->getContours(xycontour,1,false);

	this->listener->setProgressValue(25);

	CXYContourArray xycont;
	int minContourLength = 20; //min 2m for images with 10cm resolution and 3m for images with 15cm resolution
	CImageBuffer oriLineX, oriLineY, extLineX, extLineY, smoothLineX, smoothLineY, conSmoothX, conSmoothY;
	long Sz = xycontour.GetSize();

	xycont.RemoveAll();
	for (i = 0; i < Sz; i++)
	{
		CXYContour *contour = xycontour.GetAt(i);
		if (contour->getPointCount() > minContourLength)
		{
			CXYContour *cont = xycont.Add();
			*cont = *contour;	
		}
	}

	this->listener->setProgressValue(50);
	*/

	if (this->listener)
	{
		this->listener->setTitleString("Computing orientation ...");
		this->listener->setProgressValue(100);
	}

	//smooth edges & estimate grdaient orientation
	CImageBuffer oriLineX, oriLineY, extLineX, extLineY, smoothLineX, smoothLineY, conSmoothX, conSmoothY;
	long Sz = xycontour.GetSize();
	CImageFilter gau3(eGauss,0,1,3); // sizeX = 0 for unknown, sizeY = 1 to be fixed at 1 and sigma = 3
	int w = gau3.getOffsetX();
	int step = this->width;
	for (int i = 0; i < Sz; i++)
	{
		CXYContour *contour = xycontour.GetAt(i);
		setBufferOfContour(oriLineX,oriLineY,contour);

		//convolve for sigma = 3		
		extendContour(oriLineX,oriLineY,extLineX,extLineY,w);
		gau3.convolve(extLineX,smoothLineX,false);
		gau3.convolve(extLineY,smoothLineY,false);
		contractContour(smoothLineX,smoothLineY,conSmoothX,conSmoothY,w);

		long numP = contour->getPointCount();
		C2DPoint *pt = contour->getPoint(0);
		long r = (long)pt->y;
		long c = (long)pt->x;
		long ind = r*step+c;
		float prevX = conSmoothX.pixFlt(0); 
		float prevY = conSmoothY.pixFlt(0); 
		float xs1 = conSmoothX.pixFlt(1); 
		float ys1 = conSmoothY.pixFlt(1); 
		edgeOriBuffer.pixFlt(ind) = atan((ys1-prevY)/(xs1-prevX));		
		float nextX, nextY, resX, resY;
		resX = xs1;
		resY = ys1;
		for (long j = 1; j < numP-1; j++)
		{
			nextX = conSmoothX.pixFlt(j+1);
			nextY = conSmoothY.pixFlt(j+1);

			pt = contour->getPoint(j);
			r = (long)pt->y;
			c = (long)pt->x;
			ind = r*step+c;
			edgeOriBuffer.pixFlt(ind) = atan((nextY-prevY)/(nextX-prevX));

			prevX = resX;
			prevY = resY;

			resX = nextX;
			resY = nextY;
		}
		pt = contour->getPoint(numP-1);
		r = (long)pt->y;
		c = (long)pt->x;
		ind = r*step+c;
		edgeOriBuffer.pixFlt(ind) = atan((resY-prevY)/(resX-prevX));

		//test
					//FILE *fp = fopen("c:\\test2.txt","w");
					//fprintf(fp,"Curve number %ld out of %ld",i+1,Sz);
					//fclose(fp);
					//if (numCont == 21 && r == 106 && c == 100 && nextR == 107 && nextC == 101)
					//	int here = 1;
		//			//

		double pval = 100 * (double)(i+1)/(double)Sz;
		if (this->listener)
		{
			//this->listener->setTitleString("Computing orientation ...");
			this->listener->setProgressValue(pval);
		}
	}

	
	if (this->listener)
	{
		//this->listener->setTitleString("Computing orientation ...");
		this->listener->setProgressValue(100);
	}

	CLookupTable lute(8,3);
	ret = edgeOrientation->dumpImage(edgeOrientation->getFileNameChar(), edgeOriBuffer, &lute);
	if (!ret) return false;

	 //   for(int TR = 0; TR < this->height; TR += this->tileHeight)
	//{
    //    for (int TC = 0; TC < this->width; TC += this->tileWidth)
     //   {
			//this->getTileBuf(TR, TC, GreyBuffer);
			//CHistogram* histogram = this->Histograms[0];
			/*
			if (GreyBuffer.getBpp() == 8)
			{
				for (unsigned long uNDVI = 0, uRGBI = 0; uNDVI < vvibuffer.getNGV(); uNDVI += vvibuffer.getDiffX(), uRGBI += rgbiBuffer.getDiffX())
				{
					float bandN = rgbiBuffer.pixUChar(uRGBI + pidx1);
					float bandP = rgbiBuffer.pixUChar(uRGBI + pidx2);

					float sum = bandP + bandN;
					float vi = -101.0f;
					float sigvi = -1.0;

					if (sum > FLT_EPSILON)
					{
						sum = 1.0f / sum;
						vi = 100.0f * (bandP - bandN) * sum;
						sigvi = 200.0f * sum * sum * sqrt(bandP * bandP * varP + bandN * bandN * varN);

						if (sigvi > 100.0) sigvi = 100.0;
					};

					vvibuffer.pixFlt(uNDVI) = vi;
					sigbuffer.pixFlt(uNDVI) = sigvi;
				}
			}
			
			else if (GreyBuffer.getBpp() == 16)
			{
				for (unsigned long uNDVI = 0, uRGBI = 0; uNDVI < vvibuffer.getNGV(); uNDVI += vvibuffer.getDiffX(), uRGBI += rgbiBuffer.getDiffX())
				{
					float bandN = rgbiBuffer.pixUShort(uRGBI + pidx1);
					float bandP = rgbiBuffer.pixUShort(uRGBI + pidx2);

					float sum = bandP + bandN;
					float vi = -101.0f;
					float sigvi = -1.0;

					if (sum > FLT_EPSILON)
					{
						sum = 1.0f / sum;
						vi = 100.0f * (bandP - bandN) * sum;
						sigvi = 200.0f * sum * sum * sqrt(bandP * bandP * varP + bandN * bandN * varN);
						if (sigvi > 100.0) sigvi = 100.0;
					};

					vvibuffer.pixFlt(uNDVI) = vi;
					sigbuffer.pixFlt(uNDVI) = sigvi;
				}
			}

			this->dumpTile((unsigned char *) vvibuffer.getPixF());
			sigma->dumpTile((unsigned char *) sigbuffer.getPixF());

			this->updateHistograms(vvibuffer);
			sigma->updateHistograms(sigbuffer);

			float pvalue = (float)((TR / this->tileHeight) * this->tilesAcross + (TC / this->tileWidth)) / nTiles;

			if ( this->listener != NULL )
			{
				this->listener->setProgressValue(pvalue);
			}
			*/
//		}
//    }
	/*
	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(1);
	}

	sigma->finishWriting();

    return finishWriting();
*/
	
	//#endif

	return true;
}
/*
This function computes entropy of a grey scale image.
For entropy at each pixel (r,c) we consider 9by9 neighbourhood around this pixel; symmteric padding is used around image boundery
 */
bool CHugeImage::computeEntropy(CHugeImage* entropy)
{
	if (this->listener) 
	{
		this->listener->setTitleString("Computing entropy ...");
		this->listener->setProgressValue(0);
	}

	if ((this->bands > 1) || (!entropy))  return false;

	//const char *entropyFileName = entropy->getFileNameChar();
	//const char *edgeOriFileName = edgeOrientation->getFileNameChar();

	//if (!entropy->prepareWriting(entropyFileName, this->width, this->height, 32, 1)) return false;
	//if (!edgeOrientation->prepareWriting(edgeOriFileName, this->width, this->height, 32, 1)) return false;

	float nTiles = float(this->tilesDown * this->tilesAcross);

	CImageBuffer entropyBuffer (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);
	//CImageBuffer edgeOriBuffer (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);

	//define to assign the grey-scale image to an image in openCV format
	//IplImage *greyOpenCV = cvCreateImage(cvSize(this->width,this->height),IPL_DEPTH_8U,1);
	
	// 4 pixels around the image will be used for symmetric padding
	long *rowIds = new long [this->height+8]; 
	long *colIds = new long [this->width+8];
	rowIds[0] = 3; rowIds[1] = 2; rowIds[2] = 1; rowIds[3] = 0;
	colIds[0] = 3; colIds[1] = 2; colIds[2] = 1; colIds[3] = 0;
	long i, curIndex = 4;
	for (i = 0; i < this->height; i++)
		rowIds[curIndex++] = i;
	rowIds[curIndex++] = this->height-1; rowIds[curIndex++] = this->height-2; rowIds[curIndex++] = this->height-3; rowIds[curIndex++] = this->height-4;
	curIndex = 4;
	for (i = 0; i < this->width; i++)
		colIds[curIndex++] = i;
	colIds[curIndex++] = this->width-1; colIds[curIndex++] = this->width-2; colIds[curIndex++] = this->width-3; colIds[curIndex++] = this->width-4;
	//now rowIds and colIds contains indices of padded image with respect to the grey-scale image

	//test
	//FILE *fp = fopen("testout.txt","w");
	//fclose(fp);
	//FILE *fp1 = fopen("testin.txt","w");
	//fclose(fp1);

		
	//works only for grey-scale image
	if (this->getBpp() == 8)
	{	
		float log2 = log((float)2.0);
		unsigned long nPixels = entropyBuffer.getNGV();
		double hist[256];
		long rPad, cPad, minR, maxR, minC, maxC;
		//find entropy for pixel at (r,c)
		for (long r = 0; r < this->height; r++)
		{	

			//row index of this pixel at padded image
			rPad = r+4;
			minR = rPad-4;//top row index for the neighbourhood in the padded image
			maxR = rPad+4;//bottom row index for the neighbourhood in the padded image
			long rDataSize = r*this->width;
			for (long c = 0; c < this->width; c++)
			{
				//col index of this pixel at padded image
				cPad = c+4;
				minC = cPad-4;//left col index for the neighbourhood in the padded image
				maxC = cPad+4;//right col index for the neighbourhood in the padded image

				//initialize the histogram before estimation for each neighbourhood around each pixel (r,c)
				for (i = 0; i < 256; i++)
					hist[i] = 0;
				
				//test
				//FILE *fp2 = fopen("testtrack.txt","w");
				//fclose(fp2);
				//FILE *fp3 = fopen("testtrackin.txt","w");
				//fclose(fp3);
				
				//if (r == 194 && c == 220)
				//	int here = 1;
		
				//get pixel values & build histogram
				int count = 0;
				for (long rP = minR; rP <= maxR; rP++)
				{
					long rowId = rowIds[rP];
					for (long cP = minC; cP <= maxC; cP++)
					{										
						long colId = colIds[cP];
						unsigned short pixel;
						//access the pixel						
						CImageBuffer buf;

						//test						
						//FILE *fp3 = fopen("testtrackin.txt","a");
						//fprintf(fp3,"%d\t%d\t%d\t%d\t%d\t%d\n",r,c,rP,cP,rowId,colId);
						//fclose(fp3);

						this->getRect(buf,rowId,1,colId,1);						
						pixel = buf.pixUChar(0);
						
						//assign this pixel value to greyOpenCV
						//int height     = greyOpenCV->height;
						//int width      = greyOpenCV->width;
						//int step       = greyOpenCV->widthStep/sizeof(uchar);
						//uchar* data    = (uchar *)greyOpenCV->imageData;
						//data[r*step+c] = (unsigned char) pixel;					
						

						//test
						//FILE *fp2 = fopen("testtrack.txt","a");
						//fprintf(fp2,"%d\t%d\t%d\t%d\t%d\t%d\t%d\n",r,c,rP,cP,rowId,colId,pixel);
						//fclose(fp2);

						//update the histogram
						hist[pixel]++;
						count++;
					}//for (long cP = minC
				}//for (long rP = minR

				// now we have the histogram to calculate entropy at (r,c)
				// avoid zero frequencies
				double ent = 0.0;
				for (i = 0; i < 256; i++)
				{
					if (hist[i] != 0)
					{
						hist[i] /= count; //normalize the frequency by dividing the total pixels in the neighbourhood
						//the math.h function log() calculates natural logarithm; so we need to convert log(hist[i]) to log_2(hist[i])
						ent += hist[i]*log(hist[i])/log2; //log_2(hist[i]) = log(hist[i])/log(2)
					}
				}
				//long indexEntropy = r*this->width + c;
				ent = -ent;
				long entIndex = rDataSize + c;//rDataSize = r*this->width;

				//test
				//FILE *fp1 = fopen("testin.txt","a");
				//fprintf(fp1,"%d\t%d\t%3.5f\n",r,c,ent);
				//fclose(fp1);

				entropyBuffer.pixFlt(entIndex) = (float)ent;

				//test				
				//FILE *fp = fopen("testout.txt","a");
				//fprintf(fp,"%d\t%d\t%3.5f\n",r,c,ent);
				//fclose(fp);

				//if (r == 231 && c == 467)
				//	int here = 1;
				
				//if (r == 231 && c == 467)
				//	int here = 1;

				double pvalue = ((double)(entIndex+1)/(double)nPixels);
				
				if ( this->listener != NULL )
				{										
					this->listener->setProgressValue(100*pvalue);
				}

			}//for (long c = 0; 
		}//for (long r = 0;

	}//if (this->getBpp() == 8)
	else
		return false;
	
		CCharString appDir = CSystemUtilities::getEnvVar("APPDATA") + CSystemUtilities::dirDelimStr + "Barista" + CSystemUtilities::dirDelimStr;
	CLookupTable lut(8,3);
	CHugeImage *ent = new CHugeImage;
	bool ret = ent->dumpImage(appDir + "EntropyBuffer", entropyBuffer, &lut);
	if (!ret) return false;
	
	CHugeImage *entGrey = new CHugeImage;
	entGrey->prepareWriting(appDir + "EntropyGrey", this->width, this->height, 8, 1);
	//CLookupTable lut1(8,3);
	//entGrey->setLookupTable(&lut1);
	ret = entGrey->floatToGrey(ent);
	if (!ret) return false;

	CLookupTable lut2(8,3);
	entropy->setLookupTable(&lut2);
	entropy->prepareWriting(entropy->getFileNameChar(),this->width,this->height,8,1);
	ret = entropy->GreyToBinary(entGrey,0.8);
	if (!ret) return false;

	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(100);
	}
	
	// free memories
	if (rowIds)
	{
		delete []rowIds;
		rowIds = NULL;
	}
	if (colIds)
	{
		delete []colIds;
		colIds = NULL;
	}

	if (ent)
	{
		delete ent;
		ent = NULL;
	}

	if (entGrey)
	{
		delete entGrey;
		entGrey = NULL;
	}

	return true;

}
/*
This function computes entropy and edge orientation for a grey scale image.
For entropy at each pixel (r,c) we consider 9by9 neighbourhood around this pixel; symmteric padding is used around image boundery
 */
/*
bool CHugeImage::computeEntropyAndEdgeOrientation(CHugeImage* entropy, CHugeImage* edgeOrientation, CHugeImage* edgeImg)
{
	//#ifndef FINAL_RELEASE

	if (this->listener) 
	{
		this->listener->setTitleString("Calculation of entropy ...");
		this->listener->setProgressValue(1);
	}

	if ((this->bands > 1) || (!entropy) || (!edgeOrientation))  return false;

	//const char *entropyFileName = entropy->getFileNameChar();
	//const char *edgeOriFileName = edgeOrientation->getFileNameChar();

	//if (!entropy->prepareWriting(entropyFileName, this->width, this->height, 32, 1)) return false;
	//if (!edgeOrientation->prepareWriting(edgeOriFileName, this->width, this->height, 32, 1)) return false;

	float nTiles = float(this->tilesDown * this->tilesAcross);

	CImageBuffer entropyBuffer (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);
	CImageBuffer edgeOriBuffer (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);

	//define to assign the grey-scale image to an image in openCV format
	//IplImage *greyOpenCV = cvCreateImage(cvSize(this->width,this->height),IPL_DEPTH_8U,1);
	
	// 4 pixels around the image will be used for symmetric padding
	long *rowIds = new long [this->height+8]; 
	long *colIds = new long [this->width+8];
	rowIds[0] = 3; rowIds[1] = 2; rowIds[2] = 1; rowIds[3] = 0;
	colIds[0] = 3; colIds[1] = 2; colIds[2] = 1; colIds[3] = 0;
	long i, curIndex = 4;
	for (i = 0; i < this->height; i++)
		rowIds[curIndex++] = i;
	rowIds[curIndex++] = this->height-1; rowIds[curIndex++] = this->height-2; rowIds[curIndex++] = this->height-3; rowIds[curIndex++] = this->height-4;
	curIndex = 4;
	for (i = 0; i < this->width; i++)
		colIds[curIndex++] = i;
	colIds[curIndex++] = this->width-1; colIds[curIndex++] = this->width-2; colIds[curIndex++] = this->width-3; colIds[curIndex++] = this->width-4;
	//now rowIds and colIds contains indices of padded image with respect to the grey-scale image

	//test
	//FILE *fp = fopen("testout.txt","w");
	//fclose(fp);
	//FILE *fp1 = fopen("testin.txt","w");
	//fclose(fp1);

		
	//works only for grey-scale image
	if (this->getBpp() == 8)
	{	
		float log2 = log((float)2.0);
		unsigned long nPixels = entropyBuffer.getNGV();
		double hist[256];
		long rPad, cPad, minR, maxR, minC, maxC;
		//find entropy for pixel at (r,c)
		for (long r = 0; r < this->height; r++)
		{	

			//row index of this pixel at padded image
			rPad = r+4;
			minR = rPad-4;//top row index for the neighbourhood in the padded image
			maxR = rPad+4;//bottom row index for the neighbourhood in the padded image
			long rDataSize = r*this->width;
			for (long c = 0; c < this->width; c++)
			{
				//col index of this pixel at padded image
				cPad = c+4;
				minC = cPad-4;//left col index for the neighbourhood in the padded image
				maxC = cPad+4;//right col index for the neighbourhood in the padded image

				//initialize the histogram before estimation for each neighbourhood around each pixel (r,c)
				for (i = 0; i < 256; i++)
					hist[i] = 0;
				
				//test
				//FILE *fp2 = fopen("testtrack.txt","w");
				//fclose(fp2);
				//FILE *fp3 = fopen("testtrackin.txt","w");
				//fclose(fp3);
				
				//if (r == 194 && c == 220)
				//	int here = 1;
		
				//get pixel values & build histogram
				int count = 0;
				for (long rP = minR; rP <= maxR; rP++)
				{
					long rowId = rowIds[rP];
					for (long cP = minC; cP <= maxC; cP++)
					{										
						long colId = colIds[cP];
						unsigned short pixel;
						//access the pixel						
						CImageBuffer buf;

						//test						
						//FILE *fp3 = fopen("testtrackin.txt","a");
						//fprintf(fp3,"%d\t%d\t%d\t%d\t%d\t%d\n",r,c,rP,cP,rowId,colId);
						//fclose(fp3);

						this->getRect(buf,rowId,1,colId,1);						
						pixel = buf.pixUChar(0);
						
						//assign this pixel value to greyOpenCV
						//int height     = greyOpenCV->height;
						//int width      = greyOpenCV->width;
						//int step       = greyOpenCV->widthStep/sizeof(uchar);
						//uchar* data    = (uchar *)greyOpenCV->imageData;
						//data[r*step+c] = (unsigned char) pixel;					
						

						//test
						//FILE *fp2 = fopen("testtrack.txt","a");
						//fprintf(fp2,"%d\t%d\t%d\t%d\t%d\t%d\t%d\n",r,c,rP,cP,rowId,colId,pixel);
						//fclose(fp2);

						//update the histogram
						hist[pixel]++;
						count++;
					}//for (long cP = minC
				}//for (long rP = minR

				// now we have the histogram to calculate entropy at (r,c)
				// avoid zero frequencies
				double ent = 0.0;
				for (i = 0; i < 256; i++)
				{
					if (hist[i] != 0)
					{
						hist[i] /= count; //normalize the frequency by dividing the total pixels in the neighbourhood
						//the math.h function log() calculates natural logarithm; so we need to convert log(hist[i]) to log_2(hist[i])
						ent += hist[i]*log(hist[i])/log2; //log_2(hist[i]) = log(hist[i])/log(2)
					}
				}
				//long indexEntropy = r*this->width + c;
				ent = -ent;
				long entIndex = rDataSize + c;//rDataSize = r*this->width;

				//test
				//FILE *fp1 = fopen("testin.txt","a");
				//fprintf(fp1,"%d\t%d\t%3.5f\n",r,c,ent);
				//fclose(fp1);

				entropyBuffer.pixFlt(entIndex) = (float)ent;

				//test				
				FILE *fp = fopen("testout.txt","a");
				fprintf(fp,"%d\t%d\t%3.5f\n",r,c,ent);
				fclose(fp);

				//if (r == 231 && c == 467)
				//	int here = 1;
				
				//if (r == 231 && c == 467)
				//	int here = 1;

				float pvalue = (float)((entIndex+1)/nPixels);

				if ( this->listener != NULL )
				{
					this->listener->setProgressValue(100*pvalue);
				}

			}//for (long c = 0; 
		}//for (long r = 0;

	}//if (this->getBpp() == 8)
	else
		return false;
	
	CLookupTable lut(8,3);
	bool ret = entropy->dumpImage(entropy->getFileNameChar(), entropyBuffer, &lut);
	if (!ret) return false;
	
	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(100);
	}
	
	// free memories
	if (rowIds)
	{
		delete []rowIds;
		rowIds = NULL;
	}
	if (colIds)
	{
		delete []colIds;
		colIds = NULL;
	}
	

	if (this->listener) 
	{
		this->listener->setTitleString("Calculation of edge orientation ...");
		this->listener->setProgressValue(1);
	}


	//get grey-scale image in a buffer
	CImageBuffer GreyBuffer;
	this->getRect(GreyBuffer,0,this->height,0,this->width);
	//extract edges
	if (this->listener)
	 {
		 this->listener->setTitleString("Extracting contours ...");
		 this->listener->setProgressValue(2);
	 }

	//define and convert the grey-scale image to an image in openCV format
	//IplImage *cvgrey = this->convertToOpenCVimage();	
	int depth;
	if (this->bpp == 8)
		depth = IPL_DEPTH_8U;
	else if (this->bpp == 16)
		depth = IPL_DEPTH_16U;

	// Pixel depth in bits: 
    //   IPL_DEPTH_8U, IPL_DEPTH_8S, 
    //   IPL_DEPTH_16U,IPL_DEPTH_16S, 
    //   IPL_DEPTH_32S,IPL_DEPTH_32F, 
    //   IPL_DEPTH_64F

	IplImage *cvgrey = cvCreateImage(cvSize(this->width,this->height),depth,this->bands);
	CImageBuffer buf;
	unsigned char pixel; 
	//for single band, 8 bit image
	if (this->bpp == 8 && this->bands == 1)
	{
		int step = this->width;//cvgrey->widthStep/sizeof(uchar);//it seems cvgrey->widthStep = this->width+1
		//uchar* data    = (uchar *)cvgrey->imageData;
		for (long r = 0; r < this->height; r++)
		{
			//long rDataSize = r*step;
			for (long c = 0; c < this->width; c++)
			{
				//get the pixel from this image
				this->getRect(buf,r,1,c,1);						
				pixel = buf.pixUChar(0);						
				//assign this pixel value to greyOpenCV image										
				//data[rDataSize+c] = (unsigned char) pixel;
				((uchar *)(cvgrey->imageData + r*cvgrey->widthStep))[c] = pixel;				

			}
		}
	}

    IplImage  *edges = cvCreateImage(cvGetSize(cvgrey), IPL_DEPTH_8U, 1);
	cvCanny( cvgrey, edges, 50, 200, 3);
	
	//test
	//			int height1     = edges->height;
	//			int width1      = edges->width;
	//			int step1       = edges->widthStep/sizeof(uchar);

	// track edges
	//possibly add and show this edge-image in Barista
	int Gap_size = 1, minLength = 20, step, stepb;	
	CImageBuffer edgeBuf (0, 0, this->width, this->height, this->bands, 32, true,FLT_MIN);//(0, 0, this->width, this->height, this->bands, 16, true, 0);
	CImageBuffer edgeBuffer (0, 0, this->width+2*Gap_size, this->height+2*Gap_size, this->bands, 8, false, 0);
	if (this->bpp == 8 && this->bands == 1)
	{
		step = this->width;//edges->widthStep/sizeof(uchar);//it seems edges->widthStep = this->width+1
		stepb = step+2*Gap_size;
		//uchar* data    = (uchar *)edges->imageData;
		for (long r = 0; r < this->height; r++)
		{
			//long rDataSize = r*step;
			long rDataSizeBuf = (r+Gap_size)*stepb;
			for (long c = 0; c < this->width; c++)
			{
				//get pixel value from the edge image
				//long ind = rDataSize+c;
				//unsigned char p = data[ind];
				unsigned char p = ((uchar *)(edges->imageData + r*edges->widthStep))[c];
				//set the pixel from to the Barista edge image				
				long ind = rDataSizeBuf+(c+Gap_size);				
				edgeBuffer.pixUChar(ind) = p;						
				//if ( p > 255)
				//	int h = 1;
			}
		}
	}
	
	//CLookupTable luted(8,3);
	//ret = edgeImg->dumpImage(edgeImg->getFileNameChar(), edgeBuffer, &luted);
	//if (!ret) return false;
	

	
	CXYContourArray xycontour;
	CXYContour cont1, cont2;

	xycontour.RemoveAll();
	long numCont = 0;
	float numContAccepted = 0;
	long nPixels = edgeBuffer.getNGV();
	if (this->bpp == 8 && this->bands == 1)
	{
		long r, c, nextR, nextC, ind, indo;
		long ht = edgeBuffer.getHeight();
		long wd = edgeBuffer.getWidth();
		//int step = edges->widthStep/sizeof(uchar);
		//uchar* data    = (uchar *)edges->imageData;
		//find first position of edge pixel
		while(1)//while outer
		{
			cont1.deleteContour();
			cont2.deleteContour();

			bool found = false;
			for (r = 0; r < ht; r++)
			{
				long rDataSize = r*stepb;
				for (c = 0; c < wd; c++)
				{
					//get pixel value from the edge image
					ind = rDataSize+c;
					if (ind >= nPixels)
						break;					
					unsigned char p = edgeBuffer.pixUChar(ind);
					if (p == 255)
					{
						found = true;
						break;
					}						
				}
				if (found)
					break;
			}
			if (found) // a contour point is found, so build the contour with neighbour edge points
			{
				//change info for next contour								
				edgeBuffer.pixUChar(ind) = 0;
				found = false;

				//build current contour in a direction
				C2DPoint newpoint;
				newpoint.x = c;
				newpoint.y = r;
				cont1.addPoint(newpoint);
				cont2.addPoint(newpoint);
				
				while (1)//while inner 1
				{
					bool notFound = false;
					indo = ind;//preserve current pixel position

					//check point on top of the current point
					nextR = r-1;
					nextC = c;
					//ind = nextR*stepb+nextC;//
					ind = indo - stepb;//up neighbour
					//if (ind >= nPixels)
					//	break;
					if (edgeBuffer.pixUChar(ind) != 255)//if 1
					{
						//check point on left of the current point
						nextR = r;
						nextC = c-1;
						//ind = nextR*stepb+nextC;//
						ind = indo -1;//left neighbour
						//if (ind >= nPixels)
						//	break;
						if (edgeBuffer.pixUChar(ind) != 255)//if 2
						{
							//check point on right of the current point
							nextR = r;
							nextC = c+1;
							//ind = nextR*stepb+nextC;//
							ind = indo +1;//right neighbour
							//if (ind >= nPixels)
							//	break;
							if (edgeBuffer.pixUChar(ind) != 255)//if 3
							{
								//check point on bottom of the current point
								nextR = r+1;
								nextC = c;
								//ind = nextR*stepb+nextC;//
								ind = indo + stepb;//down neighbour
								//if (ind >= nPixels)
								//	break;
								if (edgeBuffer.pixUChar(ind) != 255)//if 4
								{
									//check point on top-left of the current point
									nextR = r-1;
									nextC = c-1;
									//ind = nextR*stepb+nextC;//
									ind = indo - stepb - 1;//up-left neighbour
									//if (ind >= nPixels)
									//	break;
									if (edgeBuffer.pixUChar(ind) != 255)//if 5
									{
										//check point on top-right of the current point
										nextR = r-1;
										nextC = c+1;
										//ind = nextR*stepb+nextC;//
										ind = indo - stepb + 1;//up-right neighbour
										//if (ind >= nPixels)
										//	break;
										if (edgeBuffer.pixUChar(ind) != 255)//if 6
										{
											//check point on bottom-left of the current point
											nextR = r+1;
											nextC = c-1;
											//ind = nextR*stepb+nextC;//
											ind = indo + stepb - 1;//down-left neighbour
											//if (ind >= nPixels)
											//	break;
											if (edgeBuffer.pixUChar(ind) != 255)//if 7
											{
												//check point on bottom-right of the current point
												nextR = r+1;
												nextC = c+1;
												//ind = nextR*stepb+nextC;//
												ind = indo + stepb + 1;//down-right neighbour
												//if (ind >= nPixels)
												//	break;
												if (edgeBuffer.pixUChar(ind) != 255)//if 8
												{
													notFound = true;
												}//if 8
											}//if 7
										}//if 6
									}//if 5
								}//if 4
							}//if 3
						}//if 2
					}//if 1

					//test
					FILE *fp = fopen("c:\\test.txt","w");
					fprintf(fp,"%d\t%d\t%d\t%d\t%d\t%ld",numCont,r,c,nextR,nextC,ind);
					fclose(fp);
					if (numCont == 21 && r == 214 && c == 62 && nextR == 215 && nextC == 62)
						int here = 1;
					//

					if (!notFound)//an nearest edge-point was found
					{
						r = nextR;
						c = nextC;
						newpoint.x = c;
						newpoint.y = r;
						cont1.addPoint(newpoint);
						edgeBuffer.pixUChar(ind) = 0;
					}
					else//end of building edge from this end
						break; 					
				}//while inner	1		

				//build current contour in another direction
				newpoint = *cont2.getPoint(0);
				r = (long)newpoint.y;
				c = (long)newpoint.x;
				ind = r*stepb + c;
				while (1)//while inner 2
				{
					bool notFound = false;
					indo = ind;//preserve current pixel position

					//check point on top of the current point
					nextR = r-1;
					nextC = c;
					//ind = nextR*stepb+nextC;
					ind = indo - stepb;
					//if (ind >= nPixels)
					//	break;
					if (edgeBuffer.pixUChar(ind) != 255)//if 1
					{
						//check point on left of the current point
						nextR = r;
						nextC = c-1;
						//ind = nextR*stepb+nextC;
						ind = indo - 1;
						//if (ind >= nPixels)
						//	break;
						if (edgeBuffer.pixUChar(ind) != 255)//if 2
						{
							//check point on right of the current point
							nextR = r;
							nextC = c+1;
							//ind = nextR*stepb+nextC;
							ind = indo + 1;
							//if (ind >= nPixels)
							//	break;
							if (edgeBuffer.pixUChar(ind) != 255)//if 3
							{
								//check point on bottom of the current point
								nextR = r+1;
								nextC = c;
								//ind = nextR*stepb+nextC;
								ind = indo + stepb;
								//if (ind >= nPixels)
								//	break;
								if (edgeBuffer.pixUChar(ind) != 255)//if 4
								{
									//check point on top-left of the current point
									nextR = r-1;
									nextC = c-1;
									//ind = nextR*stepb+nextC;
									ind = indo - stepb - 1;
									//if (ind >= nPixels)
									//	break;
									if (edgeBuffer.pixUChar(ind) != 255)//if 5
									{
										//check point on top-right of the current point
										nextR = r-1;
										nextC = c+1;
										//ind = nextR*stepb+nextC;
										ind = indo - stepb + 1;
										//if (ind >= nPixels)
										//	break;
										if (edgeBuffer.pixUChar(ind) != 255)//if 6
										{
											//check point on bottom-left of the current point
											nextR = r+1;
											nextC = c-1;
											//ind = nextR*stepb+nextC;
											ind = indo + stepb - 1;
											//if (ind >= nPixels)
											//	break;
											if (edgeBuffer.pixUChar(ind) != 255)//if 7
											{
												//check point on bottom-right of the current point
												nextR = r+1;
												nextC = c+1;
												//ind = nextR*stepb+nextC;
												ind = indo + stepb + 1;
												//if (ind >= nPixels)
												//	break;
												if (edgeBuffer.pixUChar(ind) != 255)//if 8
												{
													notFound = true;
												}//if 8
											}//if 7
										}//if 6
									}//if 5
								}//if 4
							}//if 3
						}//if 2
					}//if 1

					//test
					FILE *fp = fopen("c:\\test1.txt","w");
					fprintf(fp,"%d\t%d\t%d\t%d\t%d\t%ld",numCont,r,c,nextR,nextC,ind);
					fclose(fp);
					if (numCont == 21 && r == 106 && c == 100 && nextR == 107 && nextC == 101)
						int here = 1;
					//

					if (!notFound)//an nearest edge-point was found
					{
						r = nextR;
						c = nextC;
						newpoint.x = c;
						newpoint.y = r;
						cont2.addPoint(newpoint);
						edgeBuffer.pixUChar(ind) = 0;
					}
					else//end of building edge from this end
						break; 					
				}//while inner 2

				//now join the two edge segments cont1 & cont2 and store in contourArray
				long len1 = cont1.getPointCount();
				long len2 = cont2.getPointCount();
				numCont += 1;
				//test
				
				if (numCont == 21)
					int here = 1;
				if (len1+len2-1 > minLength)
				{
					numContAccepted += 1;
					CXYContour *cont = xycontour.Add();
					for (i = cont1.getPointCount()-1; i >= 0; i--)
					{
						newpoint = *cont1.getPoint(i);
						newpoint.x -= Gap_size;
						newpoint.y -= Gap_size;
						cont->addPoint(newpoint);

						r = (long) newpoint.y;
						c = (long) newpoint.x;
						ind = r*step+c;
						//if (ind >= nPixels)
						//	break;
						edgeBuf.pixFlt(ind) = numContAccepted;
					}
					for (i = 1; i < cont2.getPointCount(); i++)
					{
						newpoint = *cont2.getPoint(i);
						newpoint.x -= Gap_size;
						newpoint.y -= Gap_size;
						cont->addPoint(newpoint);

						r = (long) newpoint.y;
						c = (long) newpoint.x;
						ind = r*step+c;
						//if (ind >= nPixels)
						//	break;
						edgeBuf.pixFlt(ind) = numContAccepted;
					}
				}//if (len1+len2 >= minLength)
			}//if (found)
			else //no edge remains
				break;
		}//while outer

	}//if (this->bpp == 8 && this->bands == 1)

	CLookupTable luted(8,3);
	ret = edgeImg->dumpImage(edgeImg->getFileNameChar(), edgeBuf, &luted);
	if (!ret) return false;	

	//smooth edges & estimate grdaient orientation
	CImageBuffer oriLineX, oriLineY, extLineX, extLineY, smoothLineX, smoothLineY, conSmoothX, conSmoothY;
	long Sz = xycontour.GetSize();
	CImageFilter gau3(eGauss,0,1,3); // sizeX = 0 for unknown, sizeY = 1 to be fixed at 1 and sigma = 3
	int w = gau3.getOffsetX();

	for (i = 0; i < Sz; i++)
	{
		CXYContour *contour = xycontour.GetAt(i);
		setBufferOfContour(oriLineX,oriLineY,contour);

		//convolve for sigma = 3		
		extendContour(oriLineX,oriLineY,extLineX,extLineY,w);
		gau3.convolve(extLineX,smoothLineX,false);
		gau3.convolve(extLineY,smoothLineY,false);
		contractContour(smoothLineX,smoothLineY,conSmoothX,conSmoothY,w);

		long numP = contour->getPointCount();
		C2DPoint *pt = contour->getPoint(0);
		long r = (long)pt->y;
		long c = (long)pt->x;
		long ind = r*step+c;
		float prevX = conSmoothX.pixFlt(0); 
		float prevY = conSmoothY.pixFlt(0); 
		float xs1 = conSmoothX.pixFlt(1); 
		float ys1 = conSmoothY.pixFlt(1); 
		edgeOriBuffer.pixFlt(ind) = atan((ys1-prevY)/(xs1-prevX));		
		float nextX, nextY, resX, resY;
		resX = xs1;
		resY = ys1;
		for (long j = 1; j < numP-1; j++)
		{
			nextX = conSmoothX.pixFlt(j+1);
			nextY = conSmoothY.pixFlt(j+1);

			pt = contour->getPoint(j);
			r = (long)pt->y;
			c = (long)pt->x;
			ind = r*step+c;
			edgeOriBuffer.pixFlt(ind) = atan((nextY-prevY)/(nextX-prevX));

			prevX = resX;
			prevY = resY;

			resX = nextX;
			resY = nextY;
		}
		pt = contour->getPoint(numP-1);
		r = (long)pt->y;
		c = (long)pt->x;
		ind = r*step+c;
		edgeOriBuffer.pixFlt(ind) = atan((resY-prevY)/(resX-prevX));
	}

	CLookupTable lute(8,3);
	ret = edgeOrientation->dumpImage(edgeOrientation->getFileNameChar(), edgeOriBuffer, &lute);
	if (!ret) return false;
	
	//#endif

	return true;
}
*/
/*
IplImage *CHugeImage::convertToOpenCVimage()
{
	int depth;
	if (this->bpp == 8)
		depth = IPL_DEPTH_8U;
	else if (this->bpp == 16)
		depth = IPL_DEPTH_16U;

	// Pixel depth in bits: 
    //   IPL_DEPTH_8U, IPL_DEPTH_8S, 
    //   IPL_DEPTH_16U,IPL_DEPTH_16S, 
    //   IPL_DEPTH_32S,IPL_DEPTH_32F, 
    //   IPL_DEPTH_64F

	IplImage *imgOpenCV = cvCreateImage(cvSize(this->width,this->height),depth,this->bands);
	CImageBuffer buf;
	unsigned short pixel; 
	//for single band, 8 bit image
	if (this->bpp == 8 && this->bands == 1)
	{
		int step = imgOpenCV->widthStep/sizeof(uchar);
		uchar* data    = (uchar *)imgOpenCV->imageData;
		for (long r = 0; r < this->height; r++)
		{
			for (long c = 0; c < this->width; c++)
			{
				//get the pixel from this image
				this->getRect(buf,r,1,c,1);						
				pixel = buf.pixUChar(0);						
				//assign this pixel value to greyOpenCV image										
				data[r*step+c] = (unsigned char) pixel;
			}
		}
	}

	return imgOpenCV;
}
*/

 void CHugeImage::extendContour(CImageBuffer oriLineX, CImageBuffer oriLineY, CImageBuffer &extLineX, CImageBuffer &extLineY, int W)
 {
	int len = (int) oriLineX.getNGV();
	int L = len+2*W;
	extLineX.set(0,0,L,1,1,32,true);
	extLineY.set(0,0,L,1,1,32,true);

	int i , index, j;
	//extend beginning
	float Vx1 = 2*oriLineX.pixFlt(0);
	float Vy1 = 2*oriLineY.pixFlt(0);
	for (i = 0; i < W; i++)
	{
		index = W+1-i;
		extLineX.pixFlt(i) = Vx1 - oriLineX.pixFlt(index);
		extLineY.pixFlt(i) = Vy1 - oriLineY.pixFlt(index);
	}

	//set line as it is in the middle
	for (i = 0; i < len; i++)
	{
		index = W+i;
		extLineX.pixFlt(index) = oriLineX.pixFlt(i);
		extLineY.pixFlt(index) = oriLineY.pixFlt(i);
	}

	//extend at end
	Vx1 = 2*oriLineX.pixFlt(len-1);
	Vy1 = 2*oriLineY.pixFlt(len-1);
	for (i = 0; i < W; i++)
	{
		index = len+W+i;
		j = len-2-i; 
		extLineX.pixFlt(index) = Vx1 - oriLineX.pixFlt(j);
		extLineY.pixFlt(index) = Vy1 - oriLineY.pixFlt(j);
	}
 }

 void CHugeImage::contractContour(CImageBuffer smoothLineX, CImageBuffer smoothLineY, CImageBuffer &conSmoothX, CImageBuffer &conSmoothY, int W)
 {
	int L = (int) smoothLineX.getNGV();
	int len = L-2*W;
	conSmoothX.set(0,0,len,1,1,32,true);
	conSmoothY.set(0,0,len,1,1,32,true);

	int i, index;
	for (i = 0; i < len; i++)
	{
		index = W+i;
		conSmoothX.pixFlt(i) = smoothLineX.pixFlt(index);
		conSmoothY.pixFlt(i) = smoothLineY.pixFlt(index);
	}
 }

 void CHugeImage::setBufferOfContour(CImageBuffer &oriLineX, CImageBuffer &oriLineY, CXYContour *contour)
 {
	int width = contour->getPointCount();
	oriLineX.set(0,0,width,1,1,32,true);
	oriLineY.set(0,0,width,1,1,32,true);

	for (int j = 0; j < width; j++)
	{
		C2DPoint *p = contour->getPoint(j);
		oriLineX.pixFlt(j) = (float) p->x;
		oriLineY.pixFlt(j) = (float) p->y;
	}
 }

//<< addition

/*

 */
bool CHugeImage::computeVegetationIndex(CHugeImage* rgbi, CHugeImage* sigma, bool NDVI)
{
	if ((rgbi->bands < 3) || (!sigma))  return false;

	const char *fncp = &(this->filename[0]);
	const char *sigFncp = sigma->getFileNameChar();

	if (!prepareWriting(fncp, rgbi->width, rgbi->height, 32, 1)) return false;
	if (!sigma->prepareWriting(sigFncp, rgbi->width, rgbi->height, 32, 1)) return false;

	float nTiles = float(this->tilesDown * this->tilesAcross);

	CImageBuffer vvibuffer (0, 0, this->tileWidth, this->tileHeight, this->bands, this->bpp, true);
	CImageBuffer sigbuffer (0, 0, this->tileWidth, this->tileHeight, this->bands, this->bpp, true);
	CImageBuffer rgbiBuffer(0, 0, this->tileWidth, this->tileHeight, rgbi->bands, rgbi->bpp, true);

	int pidx1 = 0;
	int pidx2 = 3;

	if (!NDVI) pidx2 = 1;
	else if (rgbi->bands == 3) // CIR
	{
		pidx1 = 1;
		pidx2 = 0;
	}

	float varP = 0.0, varN = 0.0;
	vector<float> sigmaN;

	for(int TR = 0; TR < this->height; TR += this->tileHeight)
	{
        for (int TC = 0; TC < this->width; TC += this->tileWidth)
        {
			rgbi->getTileBuf(TR, TC, rgbiBuffer);

			if (rgbiBuffer.getGaussianNoise(0, 1.0, sigmaN))
			{
				float sigN = sigmaN[pidx1];
				float sigP = sigmaN[pidx2];
				if (sigN > varN) varN = sigN;
				if (sigP > varP) varP = sigP;
			}
		}
	}

	varN *= varN;
	varP *= varP;

    for(int TR = 0; TR < this->height; TR += this->tileHeight)
	{
        for (int TC = 0; TC < this->width; TC += this->tileWidth)
        {
			rgbi->getTileBuf(TR, TC, rgbiBuffer);

			if (rgbiBuffer.getBpp() == 8)
			{
				for (unsigned long uNDVI = 0, uRGBI = 0; uNDVI < vvibuffer.getNGV(); uNDVI += vvibuffer.getDiffX(), uRGBI += rgbiBuffer.getDiffX())
				{
					float bandN = rgbiBuffer.pixUChar(uRGBI + pidx1);
					float bandP = rgbiBuffer.pixUChar(uRGBI + pidx2);

					float sum = bandP + bandN;
					float vi = -101.0f;
					float sigvi = -1.0;

					if (sum > FLT_EPSILON)
					{
						sum = 1.0f / sum;
						vi = 100.0f * (bandP - bandN) * sum;
						sigvi = 200.0f * sum * sum * sqrt(bandP * bandP * varP + bandN * bandN * varN);

						if (sigvi > 100.0) sigvi = 100.0;
					};

					vvibuffer.pixFlt(uNDVI) = vi;
					sigbuffer.pixFlt(uNDVI) = sigvi;
				}
			}
			else if (rgbiBuffer.getBpp() == 16)
			{
				for (unsigned long uNDVI = 0, uRGBI = 0; uNDVI < vvibuffer.getNGV(); uNDVI += vvibuffer.getDiffX(), uRGBI += rgbiBuffer.getDiffX())
				{
					float bandN = rgbiBuffer.pixUShort(uRGBI + pidx1);
					float bandP = rgbiBuffer.pixUShort(uRGBI + pidx2);

					float sum = bandP + bandN;
					float vi = -101.0f;
					float sigvi = -1.0;

					if (sum > FLT_EPSILON)
					{
						sum = 1.0f / sum;
						vi = 100.0f * (bandP - bandN) * sum;
						sigvi = 200.0f * sum * sum * sqrt(bandP * bandP * varP + bandN * bandN * varN);
						if (sigvi > 100.0) sigvi = 100.0;
					};

					vvibuffer.pixFlt(uNDVI) = vi;
					sigbuffer.pixFlt(uNDVI) = sigvi;
				}
			}

			this->dumpTile((unsigned char *) vvibuffer.getPixF());
			sigma->dumpTile((unsigned char *) sigbuffer.getPixF());

			this->updateHistograms(vvibuffer);
			sigma->updateHistograms(sigbuffer);

			float pvalue = (float)((TR / this->tileHeight) * this->tilesAcross + (TC / this->tileWidth)) / nTiles;

			if ( this->listener != NULL )
			{
				this->listener->setProgressValue(pvalue);
			}
		}
    }

	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(1);
	}

	sigma->finishWriting();

    return finishWriting();
}

bool CHugeImage::cropImage(const char *oriFilename, CHugeImage *input, int offsetX, int offsetY, int newWidth, int newHeight)
{

	this->filename = oriFilename;

	this->addHUGExtension();
	const char *fncp = &(this->filename[0]);

	if (!prepareWriting(fncp, newWidth, newHeight, input->bpp, input->bands)) return false;

	float nTiles = float(this->tilesDown * this->tilesAcross);

	int tileOffsetX = offsetX;
	int tileOffsetY = offsetY;

	CImageBuffer imgBuffer (offsetX, offsetY, this->tileWidth, this->tileHeight, this->bands, this->bpp, true);

    for(int TR = 0; TR < this->height; TR += this->tileHeight, tileOffsetY += this->tileHeight)
	{
		tileOffsetX = offsetX;

        for (int TC = 0; TC < this->width; TC += this->tileWidth, tileOffsetX += this->tileWidth)
        {
			imgBuffer.setOffset(tileOffsetX, tileOffsetY);

			if (!input->getRect(imgBuffer)) return false;
			imgBuffer.setOffset(TC, TR);

			this->dumpTile(imgBuffer);

			float pvalue = (float)((TR / this->tileHeight) * this->tilesAcross + (TC / this->tileWidth)) / nTiles;

			if ( this->listener != NULL )
			{
				this->listener->setProgressValue(pvalue);
			}
		}
    }

	bool ret = finishWriting();

	// copy if exist
	if (input->transferFunction)
		this->setTransferFunction(input->transferFunction,true,true);
	else
		this->initDefaultTransferFunction();


	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(1);
	}

    return ret;
}

bool CHugeImage::mergeImageBands(const char *fn, CHugeImage *r, CHugeImage *g, CHugeImage *b, CHugeImage *IR)
{
	if ((r->bands  != 1)         || (g->bands  != 1)         || (b->bands  != 1))         return false;
	if ((r->bpp    != g->bpp)    || (r->bpp    != b->bpp)    || (b->bpp    != g->bpp))    return false;
	if ((r->width  != g->width)  || (r->width  != b->width)  || (b->width  != g->width))  return false;
	if ((r->height != g->height) || (r->height != b->height) || (b->height != g->height)) return false;


	this->bands = 3;
	this->bpp = r->bpp;

	if (IR)
	{
		if ((IR->bands != 1)         || (r->bpp    != IR->bpp) ||
			(r->width  != IR->width) || (r->height != IR->height)) return false;
		this->bands = 4;
	}

	if (!prepareWriting(fn, r->width, r->height, this->bpp, this->bands)) return false;

	float nTiles = float(this->tilesDown * this->tilesAcross);

	CImageBuffer rBuffer(0, 0, this->tileWidth, this->tileHeight, r->bands, r->bpp, true);
	CImageBuffer gBuffer(0, 0, this->tileWidth, this->tileHeight, r->bands, r->bpp, true);
	CImageBuffer bBuffer(0, 0, this->tileWidth, this->tileHeight, r->bands, r->bpp, true);
	CImageBuffer iBuffer(0, 0, this->tileWidth, this->tileHeight, r->bands, r->bpp, true);
	CImageBuffer mBuffer(0, 0, this->tileWidth, this->tileHeight, this->bands, this->bpp, true);


    for(int TR = 0; TR < this->height; TR += this->tileHeight)
	{
        for (int TC = 0; TC < this->width; TC += this->tileWidth)
        {
			r->getTileBuf(TR, TC, rBuffer);
			g->getTileBuf(TR, TC, gBuffer);
			b->getTileBuf(TR, TC, bBuffer);
			if (IR) IR->getTileBuf(TR, TC, iBuffer);

			if (this->bpp == 8)
			{
				for (unsigned long u = 0, uRGBI = 0; u < rBuffer.getNGV(); u += rBuffer.getDiffX(), uRGBI += mBuffer.getDiffX())
				{
					mBuffer.pixUChar(uRGBI)     = rBuffer.pixUChar(u);
					mBuffer.pixUChar(uRGBI + 1) = gBuffer.pixUChar(u);
					mBuffer.pixUChar(uRGBI + 2) = bBuffer.pixUChar(u);
					if (IR) mBuffer.pixUChar(uRGBI + 3) = iBuffer.pixUChar(u);
				}


			}
			else if (this->bpp == 16)
			{
				for (unsigned long u = 0, uRGBI = 0; u < rBuffer.getNGV(); u += rBuffer.getDiffX(), uRGBI += mBuffer.getDiffX())
				{
					mBuffer.pixUShort(uRGBI)     = rBuffer.pixUShort(u);
					mBuffer.pixUShort(uRGBI + 1) = gBuffer.pixUShort(u);
					mBuffer.pixUShort(uRGBI + 2) = bBuffer.pixUShort(u);
					if (IR) mBuffer.pixUShort(uRGBI + 3) = iBuffer.pixUShort(u);
				}
			}

			dumpTile(mBuffer);

			float pvalue = (float)((TR / this->tileHeight) * this->tilesAcross + (TC / this->tileWidth)) / nTiles;

			if ( this->listener != NULL )
			{
				this->listener->setProgressValue(pvalue);
			}
		}
    }

	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(1);
	}

    return finishWriting();
};

bool CHugeImage::regionGrowing(const C2DPoint &p, CXYPolyLineArray *xyedges)
{
	float probability = 0.99f;
	statistics::setNormalQuantil (probability);
	statistics::setChiSquareQuantils(probability);

	bool ret = true;
    long bufwidth = 400;
	long bufheight = 500;
	long bufOffsetX = p.x < bufwidth  / 2 ? 0: long(p.x - bufwidth  / 2);
	long bufOffsetY = p.y < bufheight / 2 ? 0: long(p.y - bufheight / 2);
	if ((bufOffsetX + bufwidth)  > this->width)  bufwidth  = width  - bufOffsetX;
	if ((bufOffsetY + bufheight) > this->height) bufheight = height - bufOffsetY;
	bool unsignedFlag = (this->bpp == 16) ? true:false;
    CImageBuffer img (bufOffsetX, bufOffsetY, bufwidth, bufheight, this->bands, this->bpp, unsignedFlag);
	this->getRect(img);
	img.exportToTiffFile("E:\\projects\\imgGrow.tif", 0, true);
	CLabelImage labelimg(bufOffsetX, bufOffsetY, bufwidth, bufheight, 8);

	CImageRegion region(0,this->bands);
	labelimg.initNewLabel(img,p,3,region);
	labelimg.growRegion(region, img);
	labelimg.dump("E:\\projects\\","grow", "", false, true);
	labelimg.morphologicalOpen(3,0);
	labelimg.dump("E:\\projects\\","grow_Op_3", "", false, true);
	labelimg.morphologicalClose(3);
	labelimg.dump("E:\\projects\\","grow_Cl_3", "", false, true);

	return ret;
};

int CHugeImage::extractEdgesLink(CXYContourArray *xycont)
{
	int ret = true;
	// Maria's defaults for edge extraction
	float sigmaSmooth = 1.0;
	float significance=90;
	float mmax=-1e+38F,scale=0.0,mmin=1e+38F;

	FeatureExtractionParameters parameters;
	parameters.Mode = eCanny;
	parameters.setDerivativeFilter(eDiffXGauss, sigmaSmooth);

	CFeatureExtractor extractor (parameters);

	CXYPolyLine *poly = 0;
	extractor.setRoi(this->roi, poly);
	extractor.edgesLink(significance, xycont);
	extractor.resetRoi();

	return ret;
}
bool CHugeImage::convolveFloat(CHugeImage* src, CKernel* kernel)
{
    if (this->bpp != 32)
        return false;

    // loop over all tiles
    int rectWidth = this->tileWidth + (kernel->size/2)*2;
    int rectHeight = this->tileHeight + (kernel->size/2)*2;

    unsigned char* rect = new unsigned char[rectWidth*rectHeight*src->pixelSizeBytes()];
    bool border;
    for (int R = 0; R < this->height; R+= this->tileHeight)
    {
        for (int C = 0; C < this->width; C += this->tileWidth)
        {
            float* dest = (float*)this->getTile(R, C);

            int rectLeft = C - kernel->size/2;
            int rectTop = R - kernel->size/2;

            src->getRect(rect, rectTop, rectHeight, rectLeft, rectWidth);

            for (int r = R; r < R+this->tileHeight; r++)
            {
                int rowInTile = r % this->tileHeight;
                int rowInRect = rowInTile + kernel->size/2;

                int lineOffsetTile = rowInTile*this->tileWidth;
                int lineOffsetRect = rowInRect*rectWidth;

                for (int c = C; c < C + this->tileWidth; c++)
                {
                    int colInTile = c % this->tileWidth;
                    int colInRect = colInTile + kernel->size/2;

                    border = (r < kernel->size/2) || (r >= this->height-kernel->size/2) || (c < kernel->size/2) || (c >= this->width - kernel->size/2);

                    // take care of border colums
                    if (border)
                    {
                        dest[lineOffsetTile+colInTile] = (float)rect[lineOffsetRect + colInRect];
                        continue;
                    }

                    // loop over kernel
                    float pixel = 0;
                    int innerRowInRect = rowInRect - kernel->size/2;
			        for(int rr = r - kernel->size/2, kr = 0; rr <= r + kernel->size/2; rr++, kr++)
			        {
                        lineOffsetRect = innerRowInRect*rectWidth;

                        int innerColInRect = colInRect - kernel->size/2;
				        for(int cc = c - kernel->size/2, kc = 0; cc <= c + kernel->size/2; cc++, kc++)
				        {
                            pixel += (float)rect[lineOffsetRect + innerColInRect]*kernel->data[kr][kc];
                            innerColInRect++;
				        }

                        innerRowInRect++;
			        }

                    dest[lineOffsetTile+colInTile] = pixel;

                } //for (c = C; c < C + this->tileWidth; c++)
            } //for (int r = R; r < R+this->tileHeight; r++)

            // save the tile
            this->setTile((unsigned char*)dest, R, C);

        } //for (int C = 0; C < this->width; C += this->tileWidth)

    } //for (int R = 0; R < this->height; R+= this->tileHeight)

    delete [] rect;

    return true;
}


bool CHugeImage::convolve(CHugeImage* src, CKernel* kernel)
{
    if (this->bpp == 32)
        return this->convolveFloat(src, kernel);

    return false;
}

/**
 * creates a new intensity (grey-scale) image from the given IImage and pan image
 * the IImage has (probably) come from an RGB image converted to grey scale
 * the resultante image is a float image
 * @param IImage the intesity source image
 * @param pan the pan source image
 * @param kernelSize the size of the processing kernel
 * @return false for failure
 */
bool CHugeImage::createIntensity(CHugeImage* IImage, CHugeImage* pan, int kernelSize)
{
    int count = 0;
    float sqrt3 = (float)sqrt(3.0);

    if ( (this->width != IImage->width) || ( this->width != pan->width) ||
         (this->height != IImage->height) || ( this->height != pan->height) )
         return false;

    if ( (this->bands != 1) || (IImage->bands != 1) || (pan->bands != 1) )
        return false;

    ///@todo no support for 16-bit imagery
    if ( (IImage->bpp != 8) || (pan->bpp != 8) )
        return false;

    if (this->bpp != 32)
        return false;

    if (kernelSize/2 > this->tileHeight) // this is a silly situation... but....
        return false;

    float meanI = 0.0;
    float meanPan = 0.0;
    float RMSI = 0.0;
    float RMSPan = 0.0;
    bool border;

    //loop over all tiles
    int rectWidth = this->tileWidth + (kernelSize/2)*2;
    int rectHeight = this->tileHeight + (kernelSize/2)*2;

    unsigned char* panRect = new unsigned char[rectWidth*rectHeight];
    unsigned char* IRect = new unsigned char[rectWidth*rectHeight];
    float kernelArea = (float)(kernelSize*kernelSize);

    unsigned char panPixel;
    unsigned char IPixel;

    for (int R = 0; R < this->height; R+= this->tileHeight)
    {
        for (int C = 0; C < this->width; C += this->tileWidth)
        {
            float* dest = (float*)this->getTile(R, C);
  //          unsigned char* panTile = pan->getTile(R, C);
  //          unsigned char* ITile = IImage->getTile(R, C);

            int rectLeft = C - kernelSize/2;
            int rectTop = R - kernelSize/2;

            pan->getRect(panRect, rectTop, rectHeight, rectLeft, rectWidth);
            IImage->getRect(IRect, rectTop, rectHeight, rectLeft, rectWidth);
            float iPixel;
            float pPixel;

            for (int r = R; r < R+this->tileHeight; r++)
            {
                int rowInTile = r % this->tileHeight;
                int rowInRect = rowInTile + kernelSize/2;

                int lineOffsetTile = rowInTile*this->tileWidth;
                int lineOffsetRect = rowInRect*rectWidth;

                for (int c = C; c < C + this->tileWidth; c++)
                {
                    int colInTile = c % this->tileWidth;
                    int colInRect = colInTile + kernelSize/2;

                    border = (r < kernelSize/2) || (r >= this->height-kernelSize/2) || (c < kernelSize/2) || (c >= this->width - kernelSize/2);

                    // take care of border colums
                    if (border)
                    {
                        dest[lineOffsetTile+colInTile] = (float)panRect[lineOffsetRect + colInRect];
                        continue;
                    }

                    meanI = 0.0;
                    meanPan = 0.0;
                    RMSI = 0.0;
                    RMSPan = 0.0;

                    panPixel = panRect[lineOffsetRect + colInRect];
                    IPixel = IRect[lineOffsetRect + colInRect];

                    // loop over kernel
                    int innerRowInRect = rowInRect - kernelSize/2;
			        for(int rr = r - kernelSize/2; rr <= r + kernelSize/2; rr++)
			        {
                        lineOffsetRect = innerRowInRect*rectWidth;

                        int innerColInRect = colInRect - kernelSize/2;
				        for(int cc = c - kernelSize/2; cc <= c + kernelSize/2; cc++)
				        {
					        iPixel = (float)IRect[lineOffsetRect + innerColInRect]*sqrt3;
                            pPixel = (float)panRect[lineOffsetRect + innerColInRect];

					        meanI += iPixel;
					        meanPan += pPixel;
					        RMSI += iPixel*iPixel;
					        RMSPan += pPixel*pPixel;

                            innerColInRect++;
				        }

                        innerRowInRect++;
			        }

			        meanI = meanI/kernelArea;
			        meanPan = meanPan/kernelArea;

			        RMSI = (float)sqrt(RMSI/kernelArea + meanI*meanI);
			        RMSPan = (float)sqrt(RMSPan/kernelArea + meanPan*meanPan);

                    // if the intensity image is clamped
	                if ( (IPixel == 0) || (IPixel == 255) ||(RMSPan == 0.0))
                    {
                        dest[lineOffsetTile+colInTile] = (float)panPixel;
                    }
                    else
			        {
                        count++;
                        dest[lineOffsetTile+colInTile] = (panPixel-meanPan) * RMSI/RMSPan + meanI;
                    }

                    dest[lineOffsetTile+colInTile] = Min(dest[lineOffsetTile+colInTile], PAN_SHARPEN_FLOAT_LIMIT);

                } //for (c = C; c < C + this->tileWidth; c++)
            } //for (int r = R; r < R+this->tileHeight; r++)

            // save the tile
            this->setTile((unsigned char*)dest, R, C);

        } //for (int C = 0; C < this->width; C += this->tileWidth)

    } //for (int R = 0; R < this->height; R+= this->tileHeight)

    delete [] panRect;
    delete [] IRect;

    return true;
}

bool CHugeImage::floatToRGB(CHugeImage* src)
{
    // sanity checks
    if (src->bpp != 32)
        return false;

    if (this->bands != 3)
        return false;

    if (this->bpp != 8)
        return false;

    if ( (src->width != this->width) ||
         (src->height != this->height) )
         return false;

    // loop over all tiles in image
    // find the overall max and min in the float image
    float MAXR = -99999;
    float MINR = 99999;
    float MAXG = -99999;
    float MING = 99999;
    float MAXB = -99999;
    float MINB = 99999;

    for (int i = 0; i < src->tilesDown; i++)
    {
        for (int j = 0; j < src->tilesAcross; j++)
        {
            // the source buffer
            float* srcTile = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);

            for (int r = 0; r < src->tileHeight; r++)
            {
                int lineOffset = r*src->tileWidth*4;
                for (int c = 0; c < src->tileWidth; c++)
                {
                    float pixelR = srcTile[lineOffset+ 4*c + 0];
                    float pixelG = srcTile[lineOffset+ 4*c + 1];
                    float pixelB = srcTile[lineOffset+ 4*c + 2];

                    if (pixelR > MAXR)
                        MAXR = pixelR;
                    if (pixelR < MINR)
                        MINR = pixelR;

                    if (pixelG > MAXG)
                        MAXG = pixelG;
                    if (pixelG < MING)
                        MING = pixelG;

                    if (pixelB > MAXB)
                        MAXB = pixelB;
                    if (pixelB < MINB)
                        MINB = pixelB;
                }
            }
        }
    }

    float rangeR = MAXR - MINR;
    float rangeG = MAXG - MING;
    float rangeB = MAXB - MINB;

    int destPixelSize = this->pixelSizeBytes();

    //unsigned char* destination = new unsigned char[src->tileHeight*src->tileWidth*destPixelSize];

    if (this->bpp == 8)
    {
        for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer
                float* srcTile = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);
                unsigned char* dest = this->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth*4;
                    //int destLineOffset = r*this->tileWidth*4;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        float pixelR = srcTile[lineOffset+ 4*c + 0];
                        float pixelG = srcTile[lineOffset+ 4*c + 1];
                        float pixelB = srcTile[lineOffset+ 4*c + 2];

                        dest[lineOffset+4*c+0] = (unsigned char)(255.0f*((pixelR - MINR)/rangeR)+0.5);
                        dest[lineOffset+4*c+1] = (unsigned char)(255.0f*((pixelG - MING)/rangeG)+0.5);
                        dest[lineOffset+4*c+2] = (unsigned char)(255.0f*((pixelB - MINB)/rangeB)+0.5);
                    }
                }

                this->setTile(dest, i*src->tileHeight, j*src->tileWidth);
            }
        }
    }
    else
    {
        return false;
    }

    return true;

}

/**
 * converts a grey-scale image (src) [0,255] to a binary image (this) {0,255}
 * @param src the input grey scale image
 * @return false for failure
 * this(i,j) = 255 (white) if src(i,j) > Level*256, 0 (black) otherwise.
 */
bool CHugeImage::GreyToBinary(CHugeImage* src, double Level)
{
    // sanity checks
	if (src->bpp != 8 && this->bpp != 8)
        return false;

	if (this->bands != 1 && src->bands != 1)
        return false;

    if ( (src->width != this->width) ||
         (src->height != this->height) )
         return false;
	unsigned char pixelLevel = (unsigned char)floor(Level*256);

    // loop over all tiles in image
	/* 
	// find the overall max and min in the float image
    float MAX = -99999;
    float MIN = 99999;	
    for (int i = 0; i < src->tilesDown; i++)
    {
        for (int j = 0; j < src->tilesAcross; j++)
        {
            // the source buffer
            float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);

            for (int r = 0; r < src->tileHeight; r++)
            {
                int lineOffset = r*src->tileWidth;
                for (int c = 0; c < src->tileWidth; c++)
                {
                    float pixel = FLOAT[lineOffset+c];
                    if (pixel > MAX)
                        MAX = pixel;
                    if (pixel < MIN && pixel>=0)
                        MIN = pixel;
                }
            }
        }
    }

    float range = MAX - MIN;
	*/
    int destPixelSize = this->pixelSizeBytes();

    unsigned char* destination = new unsigned char[src->tileHeight*src->tileWidth*destPixelSize];
    //if (this->bpp == 8)
    //{
        for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer
                unsigned char *pixels = (unsigned char *)src->getTile(i*src->tileHeight, j*src->tileWidth);
                //unsigned char* buf = dest->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth;
                    int destLineOffset = r*this->tileWidth;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        unsigned char pixel = pixels[lineOffset+c];

						if (pixel > pixelLevel) 
							destination[destLineOffset+c] = 255;
						else
							destination[destLineOffset+c] = 0;
                    }
                }

                this->setTile(destination, i*src->tileHeight, j*src->tileWidth);
            }
        }
    //}
		/*
    else if (this->bpp == 16)
    {
        for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer
                float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);
                unsigned short* buf = (unsigned short*)this->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth;
                    int destLineOffset = r*this->tileWidth;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        float pixel = FLOAT[lineOffset+c];

						if (pixel<0) pixel = 0;

                        buf[destLineOffset+c] = (unsigned char)(65535.0f*((pixel - MIN)/range)+0.5);
                    }
                }

                this->setTile((unsigned char*)buf, i*src->tileHeight, j*src->tileWidth);

            }
        }
    }
    else
    {
        return false;
    }
	*/

    delete [] destination;
    return true;


}

/**
 * converts a floating point image (src) to a grey scale image (this)
 * @param src the input grey scale image
 * @return false for failure
 *
 */
bool CHugeImage::floatToGrey(CHugeImage* src)
{
    // sanity checks
    if (src->bpp != 32)
        return false;

    if (this->bands != 1)
        return false;

    if ( (src->width != this->width) ||
         (src->height != this->height) )
         return false;

    // loop over all tiles in image
    // find the overall max and min in the float image
    float MAX = -99999;
    float MIN = 99999;	
    for (int i = 0; i < src->tilesDown; i++)
    {
        for (int j = 0; j < src->tilesAcross; j++)
        {
            // the source buffer
            float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);

            for (int r = 0; r < src->tileHeight; r++)
            {
                int lineOffset = r*src->tileWidth;
                for (int c = 0; c < src->tileWidth; c++)
                {
                    float pixel = FLOAT[lineOffset+c];
                    if (pixel > MAX)
                        MAX = pixel;
                    if (pixel < MIN && pixel>=0)
                        MIN = pixel;
                }
            }
        }
    }

    float range = MAX - MIN;
    int destPixelSize = this->pixelSizeBytes();

    unsigned char* destination = new unsigned char[src->tileHeight*src->tileWidth*destPixelSize];
    if (this->bpp == 8)
    {
        for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer
                float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);
                //unsigned char* buf = dest->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth;
                    int destLineOffset = r*this->tileWidth;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        float pixel = FLOAT[lineOffset+c];

						if (pixel < 0) pixel = 0;
						
						destination[destLineOffset+c] = (unsigned char)(255.0f*((pixel - MIN)/range)+0.5);
                    }
                }

                this->setTile(destination, i*src->tileHeight, j*src->tileWidth);
            }
        }
    }
    else if (this->bpp == 16)
    {
        for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer
                float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);
                unsigned short* buf = (unsigned short*)this->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth;
                    int destLineOffset = r*this->tileWidth;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        float pixel = FLOAT[lineOffset+c];

						if (pixel<0) pixel = 0;

                        buf[destLineOffset+c] = (unsigned char)(65535.0f*((pixel - MIN)/range)+0.5);
                    }
                }

                this->setTile((unsigned char*)buf, i*src->tileHeight, j*src->tileWidth);

            }
        }
    }
    else
    {
        return false;
    }

    delete [] destination;
    return true;


}

/**
 * transforms a hsv pixel to rgb
 *
 * @param rgb[] the output rgb pixel
 * @param hsv[] the input hsv pixel
 */
void CHugeImage::HSVtoRGB(unsigned char rgb[3], float hsv[3])
{
    float r, b, g;

    // hue undefined / no saturation
    if ( ( hsv[0] < 0) || (hsv[1] == 0.0f) )
    {
        rgb[0] = (unsigned char)(hsv[2]*255);
        rgb[1] = (unsigned char)(hsv[2]*255);
        rgb[2] = (unsigned char)(hsv[2]*255);

        return;
    }

    int Hi = (int)(floor(hsv[0]/60.0f));

    float f = hsv[0]/60.f - (float)Hi;

    float p = hsv[2] * (1.f - hsv[1]);
    float q = hsv[2] * (1.f - (hsv[1]*f));
    float t = hsv[2] * (1.f - (hsv[1] * (1.f-f)));

    if (Hi == 0)
    {
        r = hsv[2];
        g = t;
        b = p;
    }
    else if (Hi == 1)
    {
        r = q;
        g = hsv[2];
        b = p;
    }
    else if (Hi == 2)
    {
        r = p;
        g = hsv[2];
        b = t;
    }
    else if (Hi == 3)
    {
        r = p;
        g = q;
        b = hsv[2];
    }
    else if (Hi == 4)
    {
        r = t;
        g = p;
        b = hsv[2];
    }
    else if (Hi == 5)
    {
        r = hsv[2];
        g = p;
        b = q;
    }

    rgb[0] = (unsigned char)(r*255.f + 0.5);
    rgb[1] = (unsigned char)(g*255.f + 0.5);
    rgb[2] = (unsigned char)(b*255.f + 0.5);

}

/**
 * transforms an rbg pixel to hsv space
 * Hue is 0 to 360
 * @param hsv[] the output hsv values
 * @param rgb[] the input rgb values (stored as bgr)
 */
void CHugeImage::RGBtoHSV(float hsv[3], unsigned char rgb[3])
{
    float R = rgb[0]/255.0f;
    float G = rgb[1]/255.0f;
    float B = rgb[2]/255.0f;

    float themin = min(R, min(G, B));
    float themax = max(R, max(G, B));

    float delta = (themax - themin);

    // Hue
    if (delta > 0)
    {
        if (R == themax)
            hsv[0] = 60*(G-B)/delta;
        else if (G == themax)
            hsv[0] = 120 + 60*(B-R)/delta;
        else if (B == themax)
            hsv[0] = 240 + 60*(R-G)/delta;

        if (hsv[0] < 0)
            hsv[0] += 360;

    }
    else // hue is undefined
    {
        hsv[0] = -1.0f;
    }

    // Saturation
    hsv[1] = delta/themax;

    // Value
    hsv[2] = themax;
}

/**
 * Transforms a single RGB image into 3 images: Y,I,Q: intensity (Y), hue (I) and saturation (Q) images
 * @param YImage the intensity image (must be an 32-bit (float) single band image)
 * @param IImage the Saturation image (must be an 32-bit (float) single band image)
 * @param QImage the Hue image (must be an 32-bit (float) single band image)
 * @return false for failure
 */
bool CHugeImage::makeYIQImages(CHugeImage* YImage, CHugeImage* IImage, CHugeImage* QImage)
{
    //float sqrt6 = (float)sqrt(6.0);
    //float sqrt2 = (float)sqrt(2.0);
    // sanity check
    if (this->bands < 3)
        return false;

    // @todo not supporting 16-bit rgb... yet
    if (this->bpp != 8)
        return false;

    // sanity check
   // @todo not supporting 16-bit rgb... yet
   if ( (YImage->bands != 1) || (YImage->bpp != 32) )
        return false;

    // sanity check
    if ( (IImage->bands != 1) || (IImage->bpp != 32) )
        return false;

    // sanity check
    if ( (QImage->bands != 1) || (QImage->bpp != 32) )
        return false;

    // sanity check
    if ( (this->width != YImage->width ) ||
         (this->width != IImage->width ) ||
         (this->width != QImage->width ) ||
         (this->height != YImage->height ) ||
         (this->height != IImage->height ) ||
         (this->height != QImage->height ) )
         return false;

    int pixelSizeRGB = this->pixelSizeBytes();

    // the YImage, IImage and QImage have the same size
    int pixelSizeFLOAT = YImage->pixelSizeBytes();

    // loop over all tiles in image
    for (int i = 0; i < this->tilesDown; i++)
    {
        for (int j = 0; j < this->tilesAcross; j++)
        {
            // the source buffer
            /// @todo this is working for 8-bit RGB only
            unsigned char* RGB = this->getTile(i*this->tileHeight, j*this->tileWidth);

            // the destination buffers
            float* Y = (float*)YImage->getTile(i*this->tileHeight, j*this->tileWidth);
            float* I = (float*)IImage->getTile(i*this->tileHeight, j*this->tileWidth);
            float* Q = (float*)QImage->getTile(i*this->tileHeight, j*this->tileWidth);

            int R, G, B;
            // loop over pixels in tile
            for (int r = 0; r < this->tileHeight; r++)
            {
                int offsetRGB = r*this->tileWidth*pixelSizeRGB;
                int offsetFLOAT = r*this->tileWidth;
                int offsetGrey = r*this->tileWidth;
                int COL = 0;
                for (int c = 0; c < this->tileWidth; c++)
                {
                    R = RGB[offsetRGB+COL+0];
                    G = RGB[offsetRGB+COL+1];
                    B = RGB[offsetRGB+COL+2];

					float rr=(float)(R/255.0);
					float gg=(float)(G/255.0);
					float bb=(float)(B/255.0);
					float y;
					float i;
					float q;
					m_RGBtoYIQ( rr, gg, bb, &y, &i, &q );

					Y[offsetGrey+c]=y;
					I[offsetFLOAT+c]=i;
					Q[offsetFLOAT+c]=q;

               //     I[offsetGrey+c] = (unsigned char)((R+G+B)/3.0);
			   //     S[offsetFLOAT+c] = (float)(R-G)/sqrt2;
			   //     H[offsetFLOAT+c] = (float)(R+G-2.0*B)/sqrt6;

                    COL += pixelSizeRGB;
                }
            }

            YImage->setTile((unsigned char*)Y, i*this->tileHeight, j*this->tileWidth);
            IImage->setTile((unsigned char*)I, i*this->tileHeight, j*this->tileWidth);
            QImage->setTile((unsigned char*)Q, i*this->tileHeight, j*this->tileWidth);
        }
    }

    return true;
}

//addition >>
/**
 * Transforms a single RGB image into grey scale image: Grey
 * @param Grey the intensity image (must be an 8-bit single band image)
 * @param IImage the Saturation image (must be an 32-bit (flaot) single band image)
 * @param QImage the Hue image (must be an 32-bit (float) single band image)
 * @return false for failure
 */
bool CHugeImage::RGBtoGrey(CHugeImage* Grey)
{
    // sanity check
    if (this->bands < 3 || this->bpp != 8)
        return false;

	if ( (Grey->bands != 1) || (Grey->bpp != 8) )
        return false;

	if ( (this->width != Grey->width ) ||
         (this->height != Grey->height )
		)
         return false;

    int pixelSizeRGB = this->pixelSizeBytes();

    // the Grey scale pixel size
    int pixelSizeGrey = Grey->pixelSizeBytes();

    // loop over all tiles in image
    for (int i = 0; i < this->tilesDown; i++)
    {
        for (int j = 0; j < this->tilesAcross; j++)
        {
            // the source buffer
            /// @todo this is working for 8-bit RGB only
            unsigned char* RGB = this->getTile(i*this->tileHeight, j*this->tileWidth);

            // the destination buffers
            //float* Y = (float*)YImage->getTile(i*this->tileHeight, j*this->tileWidth);
            //float* I = (float*)IImage->getTile(i*this->tileHeight, j*this->tileWidth);
            //float* Q = (float*)QImage->getTile(i*this->tileHeight, j*this->tileWidth);
			unsigned char *I = (unsigned char *)Grey->getTile(i*this->tileHeight, j*this->tileWidth);

            int R, G, B;
            // loop over pixels in tile
            for (int r = 0; r < this->tileHeight; r++)
            {
                int offsetRGB = r*this->tileWidth*pixelSizeRGB;
                //int offsetFLOAT = r*this->tileWidth;
                int offsetGrey = r*this->tileWidth;
                int COL = 0;
                for (int c = 0; c < this->tileWidth; c++)
                {
                    R = RGB[offsetRGB+COL+0];
                    G = RGB[offsetRGB+COL+1];
                    B = RGB[offsetRGB+COL+2];

					//float rr=(float)(R/255.0);
					//float gg=(float)(G/255.0);
					//float bb=(float)(B/255.0);
					//float y;
					//float i;
					//float q;
					//m_RGBtoYIQ( rr, gg, bb, &y, &i, &q );
					unsigned char Intensity = (unsigned char) (0.299*R + 0.587*G + 0.114*B);
					I[offsetGrey+c] = Intensity;
					//Y[offsetGrey+c]=y;
					//I[offsetFLOAT+c]=i;
					//Q[offsetFLOAT+c]=q;

               //     I[offsetGrey+c] = (unsigned char)((R+G+B)/3.0);
			   //     S[offsetFLOAT+c] = (float)(R-G)/sqrt2;
			   //     H[offsetFLOAT+c] = (float)(R+G-2.0*B)/sqrt6;

                    COL += pixelSizeRGB;
                }
            }

            //YImage->setTile((unsigned char*)Y, i*this->tileHeight, j*this->tileWidth);
            //IImage->setTile((unsigned char*)I, i*this->tileHeight, j*this->tileWidth);
            //QImage->setTile((unsigned char*)Q, i*this->tileHeight, j*this->tileWidth);
			Grey->setTile((unsigned char*)I, i*this->tileHeight, j*this->tileWidth);
        }
    }

    return true;
}
//addition <<


/**
 * Transforms a single RGB image into 3 images. Intensity, hue and saturation images
 * @param IImage the intensity image (must be an 8-bit single band image)
 * @param SImage the Saturation image (must be an 32-bit (flaot) single band image)
 * @param HImage the Hue image (must be an 32-bit (float) single band image)
 * @return false for failure
 */
bool CHugeImage::makeISHImages(CHugeImage* IImage, CHugeImage* SImage, CHugeImage* HImage)
{
    float sqrt6 = (float)sqrt(6.0);
    float sqrt2 = (float)sqrt(2.0);
    // sanity check
    if (this->bands!= 3)
        return false;

    // @todo not supporting 16-bit rgb... yet
    if (this->bpp != 8)
        return false;

    // sanity check
   // @todo not supporting 16-bit rgb... yet
   if ( (IImage->bands != 1) || (IImage->bpp != 8) )
        return false;

    // sanity check
    if ( (SImage->bands != 1) || (SImage->bpp != 32) )
        return false;

    // sanity check
    if ( (HImage->bands != 1) || (HImage->bpp != 32) )
        return false;

    // sanity check
    if ( (this->width != IImage->width ) ||
         (this->width != SImage->width ) ||
         (this->width != HImage->width ) ||
         (this->height != IImage->height ) ||
         (this->height != SImage->height ) ||
         (this->height != HImage->height ) )
         return false;

    int pixelSizeRGB = this->pixelSizeBytes();

    // the IImage, SImage and HImage have the same size
    int pixelSizeFLOAT = IImage->pixelSizeBytes();

    // loop over all tiles in image
    for (int i = 0; i < this->tilesDown; i++)
    {
        for (int j = 0; j < this->tilesAcross; j++)
        {
            // the source buffer
            /// @todo this is working for 8-bit RGB only
            unsigned char* RGB = this->getTile(i*this->tileHeight, j*this->tileWidth);

            // the destination buffers
            unsigned char* I = IImage->getTile(i*this->tileHeight, j*this->tileWidth);
            float* S = (float*)SImage->getTile(i*this->tileHeight, j*this->tileWidth);
            float* H = (float*)HImage->getTile(i*this->tileHeight, j*this->tileWidth);

            int R, G, B;
            // loop over pixels in tile
            for (int r = 0; r < this->tileHeight; r++)
            {
                int offsetRGB = r*this->tileWidth*pixelSizeRGB;
                int offsetFLOAT = r*this->tileWidth;
                int offsetGrey = r*this->tileWidth;
                int COL = 0;
                for (int c = 0; c < this->tileWidth; c++)
                {
                    R = RGB[offsetRGB+COL+0];
                    G = RGB[offsetRGB+COL+1];
                    B = RGB[offsetRGB+COL+2];

					float rr=(float)R;
					float gg=(float)G;
					float bb=(float)B;
					float i;
					float s;
					float h;
					m_RGBtoHSI( rr, gg, bb, &h, &s, &i );

					I[offsetGrey+c]=(unsigned char)(i);
					S[offsetFLOAT+c]=s;
					H[offsetFLOAT+c]=h;

               //     I[offsetGrey+c] = (unsigned char)((R+G+B)/3.0);
			   //     S[offsetFLOAT+c] = (float)(R-G)/sqrt2;
			   //     H[offsetFLOAT+c] = (float)(R+G-2.0*B)/sqrt6;

                    COL += pixelSizeRGB;
                }
            }

            IImage->setTile(I, i*this->tileHeight, j*this->tileWidth);
            SImage->setTile((unsigned char*)S, i*this->tileHeight, j*this->tileWidth);
            HImage->setTile((unsigned char*)H, i*this->tileHeight, j*this->tileWidth);
        }
    }

    return true;
}



/**
 * wrapper for seek to work with > 32 bit file pointers
 */
void CHugeImage::SEEK(FILE* fp, file_offset* offset)
{
#ifdef WIN32
    fsetpos(fp, offset);
#else
    fseeko(fp, *offset, SEEK_SET); // linux
#endif
}

/**
 * wrapper for tell to work with > 32 bit file pointers
 */
void CHugeImage::TELL(FILE* fp, file_offset* offset)
{
#ifdef WIN32
    fgetpos(fp, offset);
#else
    *offset = ftello(fp); // linux
#endif
}

void CHugeImage::m_RGBtoYIQ( float r, float g, float b, float *y, float *i, float *q )
{
	/*
	y = intensity
	i = hue
	q = saturation
	*/
	*y = (float) (0.299*r + 0.587*g + 0.114*b);
	*i = (float) (0.596*r - 0.274*g - 0.322*b);
	*q = (float) (0.211*r - 0.523*g + 0.312*b);
}

void CHugeImage::m_RGBtoHSI( float r, float g, float b, float *h, float *s, float *i )
{
	float m_min, m_mid, m_max, m_delta;
	double domainoffset, domainbase,oneSixth;

	oneSixth=1.0/6.0;
	domainbase=0.0;

	if (r>g && r>b)
	{
      m_max = r;
      m_mid = max(g, b);
      m_min = min(g, b);
    }
	else
	{
		if (g>b)
		{
			m_max = g;
			m_mid = max(r, b);
			m_min = min(r, b);
		}
		else
		{
			m_max = b;
			m_mid = max(r, g);
			m_min = min(r, g);
		}
	}


	*i = (float)((r+g+b)/3.0);
	*s = 0.0;
	*h = 0.0;

	m_delta = m_max - m_min;

	if( m_delta==0 || *i== 0 )
	{
		// r = g = b = 0                // s = 0, i is undefined
		*s = 0;
		*h = 0;
		return;
	}
	else
	{
		*s = (float)(1-(3.0*m_min/(float)(r+g+b)));                // s

		domainoffset= ((m_mid-m_min) / (m_max-m_min)) / 6.0;

		if( r == m_max )
		{
			if(m_mid == g)
			{
				// green is ascending
				domainbase = 0;
				// red domain
			}
			else
			{
				// blue is descending
				domainbase = 5.0/6.0;
				// magenta domain
				domainoffset = oneSixth - domainoffset;
			}
		}
		else
		{
			if (g==m_max)
			{
				if (m_mid==b)
				{
					// blue is ascending
					domainbase = 2.0/6.0;
					// green domain
				}
				else
				{
					// red is ascending
					domainbase = 1.0/6.0;
					// yellow domain
					domainoffset = oneSixth - domainoffset;
				}
			}
			else
			{
				if (m_mid==r)
				{
					// red is ascending
					domainbase = 4.0/6.0;
					// blue domain
				}
				else
				{
					// green is descending
					domainbase = 3.0/6.0;
					// cyan domain
					domainoffset = oneSixth - domainoffset;
				}
			}
		}
		*h = (float)(domainbase + domainoffset);
	}
}

void CHugeImage::m_HSItoRGB( float *r, float *g, float *b, float h, float s, float i)
{
	float domainoffset;
	if(i == 0)
	{
		*r = *g = *b = 0.0;
		return;
	}
	else
	{
		if( s == 0 )
		{
			// achromatic (grey)
			*r = *g = *b = i;
			return;
		}

		domainoffset =0.0;

		if(h<1.0/6.0)
		{
			domainoffset= h;
			*r=i;
			*b= i *(1-s);
			*g= *b + (i- *b)* domainoffset*6.0f;
		}
		else
		{
			if(h<2.0/6.0)
			{
				domainoffset= h - 1.0f/6.0f;
				*g=i;
				*b= i *(1-s);
				*r= *g + (i- *b)* domainoffset*6.0f;
			}
			else
			{
				if(h<3.0/6.0)
				{
					domainoffset= h - 2.0f/6.0f;
					*g=i;
					*r= i *(1-s);
					*b= *r + (i- *r)* domainoffset*6.0f;
				}
				else
				{
					if(h<4.0/6.0)
					{
						domainoffset= h - 3.0f/6.0f;
						*b=i;
						*r= i *(1-s);
						*g= *b + (i- *r)* domainoffset*6.0f;
					}
					else
					{
						if(h<5.0/6.0)
						{
							domainoffset= h - 4.0f/6.0f;
							*b=i;
							*g= i *(1-s);
							*r= *g + (i- *g)* domainoffset*6.0f;
						}
						else
						{
							domainoffset= h - 5.0f/6.0f;
							*r=i;
							*g= i *(1-s);
							*b= *r + (i- *g)* domainoffset*6.0f;
						}
					}
				}
			}
		}
	}
}


m_Roi* CHugeImage::getRoiPtr()
{
	return this->roi;
}
void CHugeImage::setRoi(int ox,int oy,int nx,int ny,int flg)
{
	int flg_dbls;
	int nimg=1;
	m_Roi *troi;


	if (roi!=NULL)
	{
		delete[] (roi);
		roi=NULL;
	}

	// doublets are mainly used for matching
	if(flg==DOUBLET_OFF || DOUBLET_ON)
	{
		flg_dbls=flg;
	}
	else
	{
		flg_dbls= DOUBLET_OFF;
	}
	switch(flg_dbls)
	{
		case DOUBLET_OFF:
			nimg=1;
			break;

		case DOUBLET_ON:
			nimg=2;
			break;
	}

	this->roi=new m_Roi[nimg];

	if(roi==NULL)
	{
		return	;
	}
	for(int i=0;i<nimg;i++)
	{
		troi=(roi+i);
		troi->direc_flag=DIREC_OFF;
		troi->pix_flag=CORR_GVALUES;
		troi->filtered = FIL_NONE;
	}

	unsigned char *tbuf= new unsigned char[nx*ny*this->pixelSize];

	this->getRect(tbuf,oy,ny,ox,nx);

	if (this->getBpp() == 16)
	{
		this->roi->set(ox,oy,nx,ny,this->getBands(),this->getBpp(), true);
		unsigned short *rbuf=this->roi->getPixUS();
		memcpy(rbuf,(unsigned short*)tbuf,nx*ny*this->pixelSize*sizeof(unsigned char));
		rbuf=NULL;
	}
	else if ((this->getBpp() == 8) || (this->getBpp() == 1))
	{
		this->roi->set(ox,oy,nx,ny,this->getBands(),this->getBpp(), true);
		unsigned char *rbuf=this->roi->getPixUC();
		memcpy(rbuf,tbuf,nx*ny*this->pixelSize*sizeof(unsigned char));
		rbuf=NULL;
	}
	else if (this->getBpp() == 32)
	{
		this->roi->set(ox,oy,nx,ny,this->getBands(),this->getBpp(), true);
		unsigned char *rbuf = (unsigned char *) this->roi->getPixF();
		memcpy(rbuf,tbuf,nx*ny*this->pixelSize*sizeof(unsigned char));
		rbuf=NULL;
	}
	delete [] tbuf;

}


int  CHugeImage::RadiometricEnhancement(WallisParam par)
{
	float *fpix;
	int ret=M_OK;

	ret=this->roi->convertToFloat();
	if(ret==M_ERROR)return ret;

	fpix =(float*)malloc(sizeof(float)*this->roi->getWidth()*this->roi->getHeight());

	memset(fpix,0,sizeof(float)*this->roi->getWidth()*this->roi->getHeight());

	this->roi->m_enhance(fpix,par);

	ret=this->roi->storeOriginalBuffer();

	memcpy(roi->getPixF(),fpix,sizeof(float)*this->roi->getWidth()*this->roi->getHeight());


	if(this->getBpp()==16)
	{
		ret=this->roi->convertToUnsignedShort();
		if(ret==M_ERROR)return ret;

		this->setRect((unsigned char*)this->roi->getPixUS(),this->roi->getOffsetY(),this->roi->getHeight(),this->roi->getOffsetX(),this->roi->getWidth());

	}else
	{
		ret=this->roi->convertToUnsignedChar();
		if(ret==M_ERROR)return ret;

		this->setRect(this->roi->getPixUC(),this->roi->getOffsetY(),this->roi->getHeight(),this->roi->getOffsetX(),this->roi->getWidth());
	}



	free(fpix);


	this->roi->filtered = FIL_DONE;

	return ret;


	return true;
}


bool CHugeImage::computeMeanAndStdDev(doubles& mean,
									  doubles& stdDev,
									  int& nParametersX,
									  int& nParametersY,
									  int windowSizeX,
									  int windowSizeY,
									  int rectTop,
									  int rectHeight,
									  int rectLeft,
									  int rectWidth)
{


	if ( rectTop < 0 || rectTop > this->height ||
		(rectTop+rectHeight) > this->height ||
		rectLeft < 0 || rectLeft > this->width ||
		(rectLeft+rectWidth) > this->width)
	{
		return false;
	}

	if (windowSizeX < 2 || windowSizeY < 2)
		return false;

	int halfWindowSizeX = windowSizeX/2;
	int halfWindowSizeY = windowSizeY/2;
	
	// compute the number of parameters
	nParametersX = rectWidth  / halfWindowSizeX;
	int nAdditionalX = (rectWidth %(nParametersX*halfWindowSizeX)) ? 2 : 1; // always add one more at the end
	nParametersX += nAdditionalX; 

	nParametersY = rectHeight / halfWindowSizeY;
	int nAdditionalY = (rectHeight % (nParametersY*halfWindowSizeY)) ? 2 : 1; // always add one more at the end
	nParametersY += nAdditionalY;
	
	// resize the arrays
	mean.resize(nParametersX*nParametersY);
	stdDev.resize(nParametersX*nParametersY);	

	if (mean.size() != nParametersX*nParametersY || 
		stdDev.size() != nParametersX*nParametersY)
		return false; // not enough memory??

	// determine needed tiles
 	// x direction
    int firstTileX = rectLeft/this->tileWidth;
    int lastTileX = (rectLeft+rectWidth)/this->tileWidth;
    int nTilesX = lastTileX-firstTileX+1;

	// y direction
    int firstTileY = rectTop/this->tileHeight;
	int lastTileY = (rectTop+rectHeight)/this->tileHeight;
    int nTilesY = lastTileY-firstTileY+1;

	// save the sizes and start row of each tile, that saves the if's in the loops 
	// and therefor time ...
	vector<int> tileSizeX(nTilesX);
	vector<int> tileSizeY(nTilesY);

	vector<int> startCol(nTilesX);
	vector<int> startRow(nTilesY);

	for (int i=0; i < nTilesX; i++)
	{
		tileSizeX[i] = this->tileWidth;
		startCol[i] = 0;
	}

	// correct first start col and last tileSize
	tileSizeX[nTilesX-1] = rectLeft + rectWidth - lastTileX * this->tileWidth;
	startCol[0] = rectLeft - firstTileX * this->tileWidth;

	for (int i=0; i < nTilesY; i++)
	{
		tileSizeY[i] = this->tileHeight;
		startRow[i] = 0;
	}

	// correct first start row and last tileSize
	tileSizeY[nTilesY-1] =  rectTop + rectHeight - lastTileY * this->tileHeight;
	startRow[0] = rectTop - firstTileY * this->tileHeight;

	// variables for the loops
	int indexParameter = 0;
	int indexParameterY = 0;
	int indexBuf=0;
	int rowOffset=0;
	CImageTile* imageTile = NULL;

	float iHalfWindowSizeX = 1.0f / halfWindowSizeX;
	float iHalfWindowSizeY = 1.0f / halfWindowSizeY;

	double value,value2;
	int neighbour[3] = {1,nParametersX,nParametersX+1};

	unsigned char* ucBuf = NULL;
	unsigned short* usBuf = NULL;
	float* fBuf = NULL;

	int tileX,rowTile,colTile,colImage;
	int rowWindow,colWindow;

	double (*accessPixel)(unsigned char* tileData,int index);
	
	int diffX = 1;
	if (bands == 1)
	{
		if (bpp == 8)
			accessPixel = &access8Bit_1Band;
		else if (bpp == 16)
			accessPixel = &access16Bit_1Band;
		else 
			accessPixel = &access32Bit_1Band;

	}
	else if (bands == 3 || bands == 4)
	{
		diffX = 4;
		if (bpp == 8)
			accessPixel = &access8Bit_MultiBand;
		else if (bpp == 16)
			accessPixel = &access16Bit_MultiBand;
		else 
			return false;
	}
	else
		return false;


	for (int tileY=0,rowImage=rectTop; tileY < nTilesY; tileY++,rowImage+=this->tileHeight)
	{
		// index of first row of current tile in coordinate system of the selected window,
		// so it will be negative if the window is inside of the tile
		rowWindow = (tileY + firstTileY) * this->tileHeight - rectTop;

		for (tileX=0,colImage=rectLeft; tileX < nTilesX; tileX++,colImage+=this->tileWidth)
		{
			
			colWindow =  (tileX + firstTileX) * this->tileWidth - rectLeft;

			if (!this->getImageTile(&imageTile,rowImage,colImage))
				return false;

			 ucBuf = imageTile->getData();
			
			 // loop over tile
			 for (rowTile = startRow[tileY]; rowTile < tileSizeY[tileY]; rowTile++)
			 {
				 // the buffer has always the size 	this->tileWidth* this->tileHeight, also for last tile			
				 rowOffset = rowTile * this->tileWidth * diffX;
				 indexParameterY = int(float(rowWindow + rowTile) * iHalfWindowSizeY) * nParametersX;

				 for (colTile = startCol[tileX]; colTile < tileSizeX[tileX]; colTile++)
				 {
					indexBuf = rowOffset + colTile * diffX;
					indexParameter = indexParameterY + int(float(colWindow + colTile) * iHalfWindowSizeX);
					value = accessPixel(ucBuf,indexBuf);
					value2 = value*value;

					// every subwindow contributes to 4 parameters
					mean[indexParameter] += value;
					stdDev[indexParameter] += value2;
					
					mean[indexParameter+neighbour[0]] += value;
					stdDev[indexParameter+neighbour[0]] += value2;

					mean[indexParameter+neighbour[1]] += value;
					stdDev[indexParameter+neighbour[1]] += value2;

					mean[indexParameter+neighbour[2]] += value;
					stdDev[indexParameter+neighbour[2]] += value2;

				 }
			}
		} // tile X direction

		if (this->listener != NULL)
		{
			double p = 100.0*double(tileY)/double(nTilesY);
			this->listener->setProgressValue(p);
		}
	} // tile Y direction


	// finally compute mean and stdDev
	int nPixWindow = windowSizeX * windowSizeY;
	rowOffset = 0;

	double iNPixWindow = 1.0 / double(nPixWindow);
	double iRedundancy = 1.0 / double(nPixWindow -1);

	// the last col or/and row might be shorter, so we have less pixel then nPixWindow
	int lastColSize = rectWidth - (nParametersX - nAdditionalX) * halfWindowSizeX + halfWindowSizeX;
	int lastRowSize = rectHeight - (nParametersY - nAdditionalY) * halfWindowSizeY + halfWindowSizeY; 

	double iNPixLastCol = 1.0 / double(lastColSize * windowSizeY);
	double iRedundancyLastCol = 1.0 / double(lastColSize * windowSizeY - 1);
	
	double iNPixLastRow = 1.0 / double(lastRowSize * windowSizeX);
	double iRedundancyLastRow = 1.0 / double(lastRowSize * windowSizeX - 1);

	for (int rowParameter=1; rowParameter < nParametersY-nAdditionalY; rowParameter++)
	{
		rowOffset = rowParameter * nParametersX;
		for (int colParameter=1; colParameter < nParametersX-nAdditionalX; colParameter++)
		{
			indexParameter = rowOffset+colParameter;
			value = mean[indexParameter];
			stdDev[indexParameter] = (float) sqrt((stdDev[indexParameter] - value * value * iNPixWindow ) * iRedundancy);
			mean[indexParameter] = value * iNPixWindow;
		}

		// consider last col width
		if (nAdditionalX == 2)
		{
			indexParameter++;
			value = mean[indexParameter];
			stdDev[indexParameter] = (float) sqrt((stdDev[indexParameter] - value * value * iNPixLastCol ) * iRedundancyLastCol);
			mean[indexParameter] = value * iNPixLastCol ;			
		}

		// first and last value in every row are identical to second and second last value
		mean[rowOffset] = mean[rowOffset+1];
		stdDev[rowOffset] = stdDev[rowOffset+1];

		mean[indexParameter + 1] = mean[indexParameter];
		stdDev[indexParameter + 1] = stdDev[indexParameter];
	}


	// consider last row height
	if (nAdditionalY == 2)
	{
		rowOffset = (nParametersY - nAdditionalY) * nParametersX;
		// loop over second last row
		for (int colParameter=1; colParameter < nParametersX-nAdditionalX; colParameter++)
		{
			indexParameter = rowOffset+colParameter;
			value = mean[indexParameter];
			stdDev[indexParameter] = (float) sqrt((stdDev[indexParameter] - value * value * iNPixLastRow ) * iRedundancyLastRow);
			mean[indexParameter] = value * iNPixLastRow;
		}

		// consider last col width, so this is the last window in the lower right corner
		if (nAdditionalX == 2)
		{
			indexParameter++;
			value = mean[indexParameter];
			stdDev[indexParameter] = (float) sqrt((stdDev[indexParameter] - value * value / double(lastColSize * lastRowSize) ) / double(lastColSize * lastRowSize - 1));
			mean[indexParameter] = value / double(lastColSize * lastRowSize);
		}

		// first and last value in every row are identical to second and second last value
		mean[rowOffset] = mean[rowOffset+1];
		stdDev[rowOffset] = stdDev[rowOffset+1];

		mean[indexParameter + 1] = mean[indexParameter];
		stdDev[indexParameter + 1] = stdDev[indexParameter];
			
	}

	// first and last row are identical to second and second last row
	rowOffset = nParametersX * (nParametersY-1);
	for (int i=0; i < nParametersX; i++)
	{
		mean[i] = mean[i+nParametersX];
		stdDev[i] = stdDev[i+nParametersX];

		indexParameter = i+rowOffset;
		mean[indexParameter] = mean[indexParameter - nParametersX];
		stdDev[indexParameter] = stdDev[indexParameter - nParametersX];

	}

	return true;
}



/**
 * converts a floating point image (src) to a grey scale image (this)
 * for the cases of a DEM
 * @param src the input grey scale image
 * @return false for failure
 *
 */
bool CHugeImage::floatDEMToGrey(CHugeImage* src, float max, float min, bool showContours)
{
    // sanity checks
    if (src->bpp != 32)
        return false;

    if (this->bands != 1)
        return false;

    if ( (src->width != this->width) ||
         (src->height != this->height) )
         return false;

    float MAX = max;
    float MIN = min;
    float range = MAX - MIN;
    int destPixelSize = this->pixelSizeBytes();


    if (this->bpp == 8)
    {
		unsigned char* destination = new unsigned char[src->tileHeight*src->tileWidth*destPixelSize];
		for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer
                float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);
                //unsigned char* buf = dest->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth;
                    int destLineOffset = r*this->tileWidth;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        float pixel = FLOAT[lineOffset+c];

						if (pixel < 0) pixel = 0;

						if (showContours)
						{
							int newpixel = int(255.0f*((pixel - MIN)/range)+0.5);

							if (newpixel == 20 ||
								newpixel == 40 ||
								newpixel == 60 ||
								newpixel == 80 ||
								newpixel == 100 ||
								newpixel == 120 ||
								newpixel == 140 ||
								newpixel == 160 ||
								newpixel == 180 ||
								newpixel == 200 ||
								newpixel == 220 ||
								newpixel == 240  )
							{
								newpixel =0;
								pixel = (float) newpixel;
							}
						}

                        destination[destLineOffset+c] = (unsigned char)(255.0f*((pixel - MIN)/range)+0.5);
                    }
                }

                this->setTile(destination, i*src->tileHeight, j*src->tileWidth);
            }
        }
		delete [] destination;
    }
    else if (this->bpp == 16)
    {
		unsigned short* destination = new unsigned short[src->tileHeight*src->tileWidth*destPixelSize];
		for (int i = 0; i < src->tilesDown; i++)
        {
            for (int j = 0; j < src->tilesAcross; j++)
            {
                // the source buffer
                float* FLOAT = (float*)src->getTile(i*src->tileHeight, j*src->tileWidth);
                //unsigned char* buf = dest->getTile(i*src->tileHeight, j*src->tileWidth);

                for (int r = 0; r < src->tileHeight; r++)
                {
                    int lineOffset = r*src->tileWidth;
                    int destLineOffset = r*this->tileWidth;

                    for (int c = 0; c < src->tileWidth; c++)
                    {
                        float pixel = FLOAT[lineOffset+c];

						if (pixel < 0) pixel = 0;

						if (showContours)
						{
							int newpixel = int(65535.0f*((pixel - MIN)/range)+0.5);

							if (newpixel == 20 ||
								newpixel == 40 ||
								newpixel == 60 ||
								newpixel == 80 ||
								newpixel == 100 ||
								newpixel == 120 ||
								newpixel == 140 ||
								newpixel == 160 ||
								newpixel == 180 ||
								newpixel == 200 ||
								newpixel == 220 ||
								newpixel == 240  )
							{
								newpixel =0;
								pixel = (float) newpixel;
							}
						}

                        destination[destLineOffset+c] = (unsigned short)(65535.0f*((pixel - MIN)/range)+0.5);
                    }
                }

                this->setTile((unsigned char*)destination, i*src->tileHeight, j*src->tileWidth);
            }
        }
    }
	else
		return false;


    return true;


}

int CHugeImage::getRChannel() const
{
	return this->RChannel;
}

int CHugeImage::getBChannel() const
{
	return this->BChannel;
}

int CHugeImage::getGChannel() const
{
	return this->GChannel;
}

void CHugeImage::setRChannel(int rchannel)
{
	this->RChannel = rchannel;
}

void CHugeImage::setGChannel(int gchannel)
{
	this->GChannel = gchannel;
}

void CHugeImage::setBChannel(int bchannel)
{
	this->BChannel = bchannel;
}

void CHugeImage::setChannels(int r, int g, int b)
{
	this->RChannel = r;
	this->GChannel = g;
	this->BChannel = b;
}
void CHugeImage::setResamplingType(eResamplingType EResamplingType)
{
	this->resamplingType = EResamplingType;
};

void CHugeImage::setDefaultChannels()
{
	this->RChannel = 0;
	this->GChannel = 0;
	this->BChannel = 0;

	if ( this->bands == 1)
	{
		this->RChannel = 0;
		this->GChannel = 1;
		this->BChannel = 2;
	}
	if ( this->bands == 3)
	{
		this->RChannel = 0;
		this->GChannel = 1;
		this->BChannel = 2;
	}
	if ( this->bands > 3 )
	{
		this->RChannel = 0;
		this->GChannel = 1;
		this->BChannel = 2;
	}

	this->Brightness = 0;
}


/**
 * used to refresh the image histogram
 */
void CHugeImage::refreshHistograms()
{
	// reset the histograms to zero
	for (int i = 0; i < this->bands; i++)
	{
		this->Histograms[i]->reset();
	}

	unsigned char* lineBuf = new unsigned char[this->width * this->pixelSize];

	for( int r =0; r < this->height; r++)
	{
		this->getRect(lineBuf, r, 1, 0, this->width);
		this->updateHistogram(lineBuf);
	}

	delete [] lineBuf;

	// write the   histogram (only written for top level image)
	file_offset wayBack;

	CHugeImage::TELL(this->fp, &wayBack);

	file_offset histogramOffset = this->dataOffset;

	for (int i = 0; i < this->bands; i++)
	{
		CHistogram* histogram = this->Histograms[i];
		histogramOffset -= histogram->getSize()*sizeof(unsigned long);
	}

	CHugeImage::SEEK(this->fp, &histogramOffset);

	for (int i = 0; i < this->bands; i++)
	{
		CHistogram* histogram = this->Histograms[i];
		fwrite(histogram->data(), sizeof(unsigned long), histogram->getSize(), this->fp);
	}

	// go back
	CHugeImage::SEEK(this->fp, &wayBack);
}

void CHugeImage::initTiles(int iNumber)
{
	if ((this->nBufferedTiles != iNumber) || (!this->tiles))
	{
		this->deleteTiles();

		this->nBufferedTiles = iNumber;

		this->tiles = new CImageTile*[this->nBufferedTiles];
		for (int i = 0; i < this->nBufferedTiles; i++)
		{
			this->tiles[i] = NULL;
		}
	};
};

void CHugeImage::resetTiles()
{
	if (this->tiles)
	{
		for (int i = 0; i < this->nBufferedTiles; i++)
		{
			delete this->tiles[i];
			this->tiles[i] = NULL;
		}
	}

	this->lastBufferedTile = -1;
	if (this->scanlineBuf) delete [] this->scanlineBuf;
	this->scanlineBuf = 0;

	if (this->halfRes)
	{
		CHugeImage *phalf = (CHugeImage *) this->halfRes;
		phalf->resetTiles();
	}
};

void CHugeImage::deleteTiles()
{
	if (this->tiles)
	{
		for (int i = 0; i < this->nBufferedTiles; i++)
		{
			delete this->tiles[i];
			this->tiles[i] = NULL;
		}
	}

	delete [] this->tiles;
	this->tiles = 0;
	this->lastBufferedTile = -1;

	if (this->scanlineBuf) delete [] this->scanlineBuf;
	this->scanlineBuf = 0;

	if (this->halfRes)
	{
		CHugeImage *phalf = (CHugeImage *) this->halfRes;
		phalf->deleteTiles();
	}
};




void CHugeImage::initIntensityHistogram()
{
	
	if (this->bands > 1)
	{
		this->deleteIntensityHistogram();
		this->intensityHistogram = new CHistogram(this->bpp);
	}	
}



void CHugeImage::deleteIntensityHistogram()
{
	if (this->intensityHistogram)
		delete this->intensityHistogram;
	this->intensityHistogram = NULL;
}



/**
 * Serialized storage of a CHugeImage to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CHugeImage::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();

	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName relPath;
	CFileName fileSaveName = this->filename;

	if (!Path.IsEmpty())
	{
		relPath = CSystemUtilities::RelPath(this->filename, Path);
		fileSaveName.SetPath(relPath);
	}

	token->setValue("HugName", fileSaveName.GetChar());

	token = pStructuredFileSection->addToken();
	token->setValue("Red Channel", this->RChannel);

 	token = pStructuredFileSection->addToken();
	token->setValue("Green Channel", this->GChannel);

 	token = pStructuredFileSection->addToken();
	token->setValue("Blue Channel", this->BChannel);

 	token = pStructuredFileSection->addToken();
	token->setValue("Brightness", this->Brightness);

 	token = pStructuredFileSection->addToken();
	token->setValue("ResamplingType", this->resamplingType);


	if (this->transferFunction)
	{
		CStructuredFileSection* child = pStructuredFileSection->addChild();
		this->transferFunction->serializeStore(child);
	}

	if (this->intensityHistogram)
		this->writeIntensityHistogram();

}

/**
 * Restores a CHugeImage from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CHugeImage::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName CWD;
	if (!Path.IsEmpty())
	{
		CWD = CSystemUtilities::getCwd();
		CSystemUtilities::chDir(Path);
	}

	bool isColorIndexed = false;
	CFileName lutFileName("");



	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);

        CCharString key = token->getKey();

		if (key.CompareNoCase("HugName"))
            this->filename = token->getValue();
		else if (key.CompareNoCase("Red Channel"))
			this->RChannel = token->getIntValue();
		else if (key.CompareNoCase("Green Channel"))
			this->GChannel = token->getIntValue();
		else if (key.CompareNoCase("Blue Channel"))
			this->BChannel = token->getIntValue();
		else if (key.CompareNoCase("Brightness"))
			this->Brightness = token->getIntValue();
		else if (key.CompareNoCase("ResamplingType"))
			this->resamplingType = (eResamplingType)token->getIntValue();
		else if (key.CompareNoCase("ColorIndexed"))
			isColorIndexed = token->getBoolValue();
		else if (key.CompareNoCase("LUTName"))
			lutFileName = token->getValue();
	}


	this->deleteTransferFunction();
	bool foundTransferFunction = false;


	// we try first to read a transfer function object from the project
    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CTransferFunction"))
        {
			this->transferFunction = new CTransferFunction(0,0);
			this->transferFunction->serializeRestore(child);
			foundTransferFunction = true;
        }
		else if (child->getName().CompareNoCase("CIHSTransferFunction"))
		{
			this->transferFunction = new CIHSTransferFunction(0);
			this->transferFunction->serializeRestore(child);
			foundTransferFunction = true;
		}
        else if (child->getName().CompareNoCase("CWallisFilter"))
        {
			this->transferFunction = new CWallisFilter(8,1);
			this->transferFunction->serializeRestore(child);
			foundTransferFunction = true;
        }
    }

	if (!Path.IsEmpty())
	{
		this->filename = CSystemUtilities::AbsPath(this->filename);
		if (lutFileName.GetLength())
			lutFileName = CSystemUtilities::AbsPath(lutFileName);
		CSystemUtilities::chDir(CWD);
	}

	// init the huge file 
	this->open(this->filename.GetChar(), true);
	this->closeHugeFile();


	// check if the project has an old lookup table, if so init the transfer function
	// with it
	if ( !foundTransferFunction && isColorIndexed && lutFileName.GetLength())
	{
		this->deleteTransferFunction();
		this->transferFunction = new CTransferFunction(this->bpp,3);
		foundTransferFunction = this->transferFunction->read(lutFileName.GetChar());
	}

	if (this->Brightness && this->transferFunction )
	{
		this->changeBrightness(0);
	}

}

/**
 * reconnects pointers
 */
void CHugeImage::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CHugeImage::resetPointers()
{
    CSerializable::resetPointers();
}


/**
 * Flag if CHugeImage is modified
 * returns bool
 */
bool CHugeImage::isModified()
{
    ///@todo when the points are serializable we can loop them...
    return this->bModified;
}


void CHugeImage::closeHugeFile()
{
	// find the top level and close the file 
	CHugeImage* topLevel = this;
	while (topLevel->doubleSize) 
	{
		topLevel = (CHugeImage*)topLevel->doubleSize;
	}
	
	if (topLevel)
	{
		// close only the top level
		if (topLevel->fp != NULL)
		{
			fclose(topLevel->fp);
			topLevel->fp = NULL;
		}

		// reset all the other levels to NULL
		while (topLevel->halfRes)
		{
			CHugeImage *phalf = (CHugeImage *) topLevel->halfRes;
			phalf->fp = NULL;
			topLevel =  phalf;
		}


	}
}

bool CHugeImage::writeLabelImage(const char *Filename, const CLabelImage &labelImage)
{
	this->setFileName(Filename);
	CLookupTable LUT = CLabelImage::get16BitLookup();
	this->setLookupTable(LUT);

	bool OK = this->prepareWriting(filename, labelImage.getWidth(), labelImage.getHeight(), labelImage.getBpp(), labelImage.getBands());

	if (OK)
	{
		CImageBuffer tile(0, 0, this->getTileWidth(), this->getTileHeight(), labelImage.getBands(), labelImage.getBpp(), labelImage.getUnsignedFlag());

		for(int TR = 0; TR < this->getHeight(); TR += this->getTileHeight())
		{
			for (int TC = 0; TC < this->getWidth(); TC += this->getTileWidth())
			{
				unsigned long idxM0 = labelImage.getIndex(TR,TC);
				long cMax = tile.getWidth();
				if (TC + cMax > this->getWidth()) cMax = this->getWidth() - TC;

				for (unsigned long idxM1 = 0; ((idxM0 < labelImage.getNGV()) && (idxM1 < tile.getNGV())); idxM0  += labelImage.getDiffY(), idxM1 += tile.getDiffY())
				{
					unsigned long idxMMx = idxM1 + tile.getDiffY();
					for (unsigned long idxM = idxM1, c = 0; (c < (unsigned long)(cMax)) && (idxM < idxMMx); idxM  += tile.getDiffX(), c++)
					{
						tile.pixUShort(idxM) = labelImage.pixUShort(idxM0 + c);
					}
				}

				OK &= this->dumpTile(tile);
			}
		}
		OK &= this->finishWriting();
	}

	return OK;
};


bool CHugeImage::dumpImage(const char *Filename, const CImageBuffer &Image, CLookupTable *LUT)
{
	this->setFileName(Filename);
	if (LUT) 
		this->setLookupTable(*LUT);

	bool OK = this->prepareWriting(filename, Image.getWidth(), Image.getHeight(), Image.getBpp(), Image.getBands());

	if (OK)
	{
		CImageBuffer tile(0, 0, this->getTileWidth(), this->getTileHeight(), Image.getBands(), Image.getBpp(), Image.getUnsignedFlag());

		for(int TR = 0; TR < this->getHeight(); TR += this->getTileHeight())
		{
			for (int TC = 0; TC < this->getWidth(); TC += this->getTileWidth())
			{
				unsigned long idxM0 = Image.getIndex(TR,TC);
				long cMax = tile.getWidth();
				if (TC + cMax > this->getWidth()) cMax = this->getWidth() - TC;

				if (this->bpp <= 8)
				{
					for (unsigned long idxM1 = 0; ((idxM0 < Image.getNGV()) && (idxM1 < tile.getNGV())); idxM0  += Image.getDiffY(), idxM1 += tile.getDiffY())
					{
						unsigned long idxMMx = idxM1 + tile.getDiffY();
						for (unsigned long idxM = idxM1, c = 0; (c < (unsigned long)(cMax)) && (idxM < idxMMx); idxM  += tile.getDiffX(), c++)
						{
							tile.pixUChar(idxM) = Image.pixUChar(idxM0 + c);
						}
					}

				}
				else if (this->bpp == 16)
				{
					for (unsigned long idxM1 = 0; ((idxM0 < Image.getNGV()) && (idxM1 < tile.getNGV())); idxM0  += Image.getDiffY(), idxM1 += tile.getDiffY())
					{
						unsigned long idxMMx = idxM1 + (unsigned long)(tile.getDiffY());
						for (unsigned long idxM = idxM1, c = 0; (c < (unsigned long)(cMax)) && (idxM < idxMMx); idxM  += tile.getDiffX(), c++)
						{
							tile.pixUShort(idxM) = Image.pixUShort(idxM0 + c);
						}
					}
				}
				else if (this->bpp == 32)
				{
					for (unsigned long idxM1 = 0; ((idxM0 < Image.getNGV()) && (idxM1 < tile.getNGV())); idxM0  += Image.getDiffY(), idxM1 += tile.getDiffY())
					{
						unsigned long idxMMx = idxM1 + (unsigned long)(tile.getDiffY());
						for (unsigned long idxM = idxM1, c = 0; (c < (unsigned long)(cMax)) && (idxM < idxMMx); idxM  += tile.getDiffX(), c++)
						{
							tile.pixFlt(idxM) = Image.pixFlt(idxM0 + c);
						}
					}
				}

				OK &= this->dumpTile(tile);
			}
		}
		OK &= this->finishWriting();
	}

	return OK;
};


bool CHugeImage::flip(bool vertical, bool horizontal )
{
	// write image contents to a temporary file
	CFileName tmp(this->filename.GetChar());
	tmp += ".raw";

	FILE* ftmp = fopen(tmp.GetChar(), "wb");

	if ( !ftmp)
	{
		fclose(ftmp);
		return false;
	}

	int psize = this->pixelSizeBytes();
	unsigned char* linedata = new unsigned char[width*psize];

	// write vertically flipped
	for (int i = 0; i < this->height; i++)
	{
		bool gotLine = this->getRect(linedata, height - i -1, 1, 0, width);
        fwrite(linedata, sizeof(unsigned char), width*psize, ftmp);
	}

	fclose(ftmp);
	ftmp = NULL;

	delete [] linedata;

	int tileRows = this->getTileHeight();
	int tileCols = this->getTileWidth();

	float numberOfTileRows = (float)this->tilesAcross*this->tilesDown*tileRows ;
	float count =0;

	unsigned char* tiledata = new unsigned char[tileRows*tileCols*psize];
	ftmp = fopen(tmp.GetChar(), "rb");

	if ( !ftmp)
	{
		fclose(ftmp);
		return false;
	}

	for(int TR = 0; TR < this->tilesDown; TR++)
	{
		for (int TC = 0; TC < this->tilesAcross; TC++)
		{
			// fill tile
			for (int i = 0; i < tileRows && (TR*tileRows+i) <= this->height; i++)
			{
				file_offset offset =((TR*tileRows + i)*this->width + TC*tileCols)*psize;
				CHugeImage::SEEK(ftmp, &offset);

				if ( (TC+1)*tileCols > this->width )
				{
					int howmuch = this->width - TC*tileCols;
					fread(tiledata+i*tileCols*psize, sizeof(unsigned char), howmuch*psize, ftmp);
				}
				else
					fread(tiledata+i*tileCols*psize, sizeof(unsigned char), tileCols*psize, ftmp);
			}

			if (this->listener != NULL)
			{
				double p = 100.0*(double)(TR*this->tilesAcross + TC)/(double)this->tilesAcross*this->tilesDown;
				this->listener->setProgressValue(p);
			}

			this->setTile(tiledata, TR*tileRows, TC*tileCols);
		}
	}

	fclose(ftmp);
	ftmp = NULL;

	remove(tmp.GetChar());

	//this->resetTiles();


	return true;
}


bool CHugeImage::computeStatistics(vector<double>& avg,vector<double>& rms,vector<double>& min,vector<double>& max,vector<double>& dev,CHistogramFlt& hist,int startRow,int startCol,int height, int width)
{

	avg.resize(this->bands);
	rms.resize(this->bands);
	min.resize(this->bands);
	max.resize(this->bands);
	dev.resize(this->bands);

	for (int i=0; i<this->bands; i++)
	{
		avg[i] =0.0;
		rms[i] =0.0;
		dev[i] =0.0;
		min[i] =FLT_MAX;
		max[i] =FLT_MIN;
	}


	long count =0;

	if ( startRow < 0 || startRow > this->height)
		return false;

	if ( startCol < 0 || startCol > this->width)
		return false;

	int endRow = startRow + height;
	int endCol = startCol + width;

	if (endRow <= startRow || endRow > this->height)
		return false;

	if (endCol <= startCol || endCol > this->width)
		return false;


	int firstTileR = startRow / this->tileHeight;
	int firstTileC = startCol / this->tileWidth;
	int lastTileR = endRow / this->tileHeight;
	int lastTileC = endCol / this->tileWidth;

	float  *values = new float (4);
	bool unsignedFlag = (this->bpp == 32) ? false:true;
	int rectHeight = this->tileHeight,rectWidth = this->tileWidth;

	for ( int r = firstTileR,offsetR = startRow;r <= lastTileR;r++, offsetR += rectHeight)
	{
		// set first to full height
		rectHeight = this->tileHeight;
		
		// correct rectHeight for first and last tile
		if (r == firstTileR)
			rectHeight -= startRow - (firstTileR)*this->tileHeight;
		if (r == lastTileR)
			rectHeight -= (lastTileR + 1) * this->tileHeight - endRow;

		for (int c = firstTileC,offsetC = startCol;c <= lastTileC;c++, offsetC += rectWidth)
		{
			// set first to full width
			rectWidth = this->tileWidth;

			// correct rectWidth for first and last tile
			if (c == firstTileC)
				rectWidth -= startCol - (firstTileC)*this->tileWidth;
			if (c == lastTileC)
				rectWidth -= (lastTileC +1) * this->tileWidth - endCol;

			CImageBuffer buffer(offsetC,offsetR,rectWidth,rectHeight,this->bands,this->bpp,unsignedFlag);
			this->getRect(buffer);

			for (unsigned long index =0; index < buffer.getNGV(); index += buffer.getDiffX())
			{			
				buffer.getColourVec(index,values);
				if (values[0] !=  DEM_NOHEIGHT)
				{
					count++;
					for (int i=0; i< this->bands; i++)
					{
						double d = values[i];
						avg[i] += d;
						dev[i] += d*d;
						if (d < min[i]) min[i] = d;
						if (d > max[i]) max[i] = d;
						if (hist.getSize())
							hist.addValue(values[i]);
					}
				}
			}

		}
	}

	if (count > 1)
	{
		for (int i=0; i< this->bands; i++)
		{
			avg[i] /= count;
			dev[i] /= count;
			rms[i] = sqrt(dev[i]);
			avg[i] /= count;
			dev[i] = rms[i]*rms[i] - avg[i]*avg[i];
			dev[i] = sqrt(dev[i]);

		/*	rms[i] = dev[i] - avg[i]* avg[i]* count;
			rms[i] /= count - 1;
			rms[i] = sqrt(rms[i]);
			dev[i] /= count;
			dev[i] = sqrt(dev[i]);*/
		}
	}
	else 
	{
		for (int i=0; i< this->bands; i++)
		{
			min[i] = 0.0;
			max[i] = 0.0;
		}
	}
	
	delete [] values;

	return true;

}

void CHugeImage::deleteTransferFunction()
{
    if (this->transferFunction != NULL)
    {
        delete this->transferFunction;
        this->transferFunction = NULL;
    }
}

void CHugeImage::initDefaultTransferFunction()
{
	// just in case we haven't done that outside of this function
	this->deleteTransferFunction();


	if (this->bands == 1)
	{	
		if (this->bpp != 32)
		{
			if (this->bpp != 8)
				this->transferFunction = CTransferFunctionFactory::createLinearStretch(this->filename,this->bpp,this->Histograms[0]->getMin(),this->Histograms[0]->getMax());
			else this->transferFunction = CTransferFunctionFactory::createDirectTransfer(this->filename, this->bands, this->bpp);
		}
		else
			this->transferFunction = CTransferFunctionFactory::createLinearStretch(this->filename,this->bpp,this->Histograms[0]->getMin(),this->Histograms[0]->getMax());
	}
	else
	{
		if (this->intensityHistogram)
			delete this->intensityHistogram;
		this->intensityHistogram = NULL;
		
		int pyramideLevel = computeImageLevel(4000);
		
		if (!this->computeIntensityHistogram(pyramideLevel))
			this->transferFunction = CTransferFunctionFactory::createDirectTransfer(this->filename, this->bands, this->bpp);
		else
		{
			if (this->bpp != 8)
				this->transferFunction = CTransferFunctionFactory::createLinearStretchIHS(this->filename,this->bpp,this->intensityHistogram->getMin(),this->intensityHistogram->getMax());
			else this->transferFunction = CTransferFunctionFactory::createDirectTransfer(this->filename, this->bands, this->bpp);
		}
	}

}

int CHugeImage::computeImageLevel(int maxSize)
{
	int maxExtend = MAX(this->width,this->height);
	int pyramideLevel = 0;
	while (maxExtend > maxSize)
	{
		pyramideLevel++;
		maxExtend /=2;
	}

	return pyramideLevel;
}

bool CHugeImage::computeIntensityHistogram(int pyramidLevel)
{
	if (this->bands == 1 || this->bpp > 16 )
	{
		delete this->intensityHistogram;
		this->intensityHistogram = NULL;
		return false;
	}

	CHugeImage* himage = this;
	// we always store the intensity histogram at level 0
	while (pyramidLevel && himage->halfRes)
	{
		pyramidLevel--;
		himage = (CHugeImage*)himage->halfRes;
	}

	if (!himage || !computeIntensityHistogram(himage))
	{
		delete this->intensityHistogram;
		this->intensityHistogram = NULL;
		return false;
	}

	this->intensityHistogram->updateMinMax(0.02,0.98);
	return true;


}

bool CHugeImage::computeIntensityHistogram(CHugeImage* himage)
{
	if (this->intensityHistogram)
		delete this->intensityHistogram;
	this->intensityHistogram = NULL;

	this->intensityHistogram = new CHistogram(this->bpp);


	unsigned char* srcUC = NULL;
	unsigned short* srcUS = NULL;
	unsigned int lineOffset=0;
	unsigned int pixelOffset= 0;
	unsigned int rowHeight = 0;
	unsigned int colWidth = 0;

	if (this->bpp == 8)
	{	
		for (int i = 0; i < himage->tilesDown; i++)
		{
			if (((i+1)*himage->tileHeight) <= himage->height)
				rowHeight = himage->tileHeight;
			else if ( i > 0)
				rowHeight = himage->height - i  * himage->tileHeight;
			else // i ==0
				rowHeight = himage->height;

			for (int j = 0; j < himage->tilesAcross; j++)
			{
				srcUC = himage->getTile(i*himage->tileHeight, j*himage->tileWidth);

				if (((j+1)*himage->tileWidth) <= himage->width)
					colWidth = himage->tileWidth;
				else if ( j > 0)
					colWidth = himage->width - j  * himage->tileWidth;
				else // j ==0
					colWidth = himage->width;
					

				for (unsigned int r = 0; r < rowHeight; r++)
				{
					lineOffset = r*colWidth*this->bands;
					for (unsigned int c = 0; c < colWidth; c++)
					{
						pixelOffset = lineOffset + this->bands*c;
						this->intensityHistogram->increment( int((srcUC[pixelOffset] + srcUC[pixelOffset + 1] + srcUC[pixelOffset + 2])/3.0));                   
					}
				}
			}
		}
	}
	else if (this->bpp == 16)
	{	
		for (int i = 0; i < himage->tilesDown; i++)
		{
			if (((i+1)*himage->tileHeight) <= himage->height)
				rowHeight = himage->tileHeight;
			else if ( i > 0)
				rowHeight = himage->height - i  * himage->tileHeight;
			else // i ==0
				rowHeight = himage->height;
				

			for (int j = 0; j < himage->tilesAcross; j++)
			{
				srcUS = (unsigned short*)himage->getTile(i*himage->tileHeight, j*himage->tileWidth);

				if (((j+1)*himage->tileWidth) <= himage->width)
					colWidth = himage->tileWidth;
				else if ( j > 0)
					colWidth = himage->width - j  * himage->tileWidth;
				else // j ==0
					colWidth = himage->width;

				for (unsigned int r = 0; r < rowHeight; r++)
				{
					lineOffset = r*colWidth*this->bands;
					for (unsigned int c = 0; c < colWidth; c++)
					{
						pixelOffset = lineOffset + this->bands*c;
						this->intensityHistogram->increment( int((srcUS[pixelOffset] + srcUS[pixelOffset + 1] + srcUS[pixelOffset + 2])/3.0));                   
					}
				}
			}
		}
	}

	return true;
}


const CTransferFunction* CHugeImage::getTransferFunction() const
{
	if (this->doubleSize)
	{
		CHugeImage *hug = (CHugeImage *) doubleSize;
		return hug->getTransferFunction();
	}

	return this->transferFunction;

}



void CHugeImage::setTransferFunction(CTransferFunction* transferFunction, bool copyFunction,bool deleteOldFunction)
{
	
	// only when valid function
	if (!transferFunction)
		return;
	
	CTransferFunction* tmp = NULL;
	
	// copy if requested
	if (copyFunction)
		tmp = CTransferFunctionFactory::create(transferFunction);
	else
		tmp = transferFunction;

	// check if successfully initialized
	if (!tmp)
		return;

	if (deleteOldFunction)
		this->deleteTransferFunction();


	this->transferFunction = tmp;

	if (this->transferFunction->isColorIndexed())
		this->setResamplingType(eNearestNeighbour);


	this->Brightness = 0;
	
}

void CHugeImage::setLookupTable(const CLookupTable &lookup)
{
	this->deleteTransferFunction();
	this->transferFunction = CTransferFunctionFactory::createFromLUT(CTransferFunction::eColorIndexed,this->filename,lookup);
	this->setResamplingType(eNearestNeighbour);
	
};

bool CHugeImage::addLookupTable(const char* filename)
{
	CLookupTable lut(this->bpp,this->bands);
	
	if (filename && lut.read(filename))
	{
		this->deleteTransferFunction();	
		this->setLookupTable(lut);		
	}
	else
		return false;

	return true;

	
}

CTransferFunction* CHugeImage::getTransferFunction()
{

	return this->transferFunction;
		
}

bool CHugeImage::readIntensityHistogram()
{
	CFileName intName;
	this->getIntensityHistogramName(intName);

	FILE *in = fopen(intName.GetChar(),"rb");

	if (in)
	{
		bool ret = false;
		unsigned long size;
		unsigned long nRead =fread(&size,sizeof(unsigned long),1,in);
		
		if (!this->intensityHistogram)
			this->initIntensityHistogram();

		if (nRead == 1 && this->intensityHistogram->getSize() == size)
		{
			this->intensityHistogram->readHistogram(in);
			ret = true;
		}

		fclose(in);

		return ret;

	}
	else
		return false;
}

void CHugeImage::writeIntensityHistogram()
{
	CFileName intName;
	this->getIntensityHistogramName(intName);

	FILE *out = fopen(intName.GetChar(),"wb");

	unsigned long size = this->intensityHistogram->getSize();
	
	fwrite(&size,sizeof(unsigned long),1,out);
	this->intensityHistogram->writeHistogram(out);

	fclose(out);
}

void CHugeImage::getIntensityHistogramName(CFileName& intName) const
{
	intName = this->filename.Left(this->filename.GetLength() - 3);
	intName += "int";


}

CHugeImage* CHugeImage::getLevelNUllImage(int& zoomDownFactor)
{
	// the zoom factor has to be according to the image were this function has been called!!

	CBaseImage* searchImage = this;
	while (searchImage->doubleSize)
	{
		zoomDownFactor*=2;
		searchImage = searchImage->doubleSize;
	}

	return (CHugeImage*) searchImage;
}

CHugeImage *CHugeImage::getPyramidLevel(int level)
{
	CHugeImage *lev = this;
	for (int l = 1; (l <= level) && lev; ++l)
	{
		lev = (CHugeImage*) lev->halfRes;
	}

	return lev;
}

bool CHugeImage::createFromHugeImage(CHugeImage* oldHugeImage,bool applyTransferFunction)
{

	bool ret = this->prepareWriting(this->getFileNameChar(),oldHugeImage->width,oldHugeImage->height,oldHugeImage->bpp,oldHugeImage->bands);
	CTransferFunction* tmpTransferFunction = NULL;
	CLookupTable tmpLUT(oldHugeImage->bpp,3);

	if (applyTransferFunction && oldHugeImage->transferFunction)
	{
		tmpTransferFunction = oldHugeImage->transferFunction->copy();

		if (oldHugeImage->bands == 1)
			oldHugeImage->transferFunction->getUnmappedLUT(*tmpTransferFunction,oldHugeImage->getHistogram());
		else
			oldHugeImage->transferFunction->getUnmappedLUT(*tmpTransferFunction,oldHugeImage->getIntensityHistogram());
	}


	CImageBuffer srcBuf(0,0,oldHugeImage->tileWidth,oldHugeImage->tileHeight,oldHugeImage->bands,oldHugeImage->bpp,oldHugeImage->bpp != 32);
	int rowOffset = 0;
	unsigned long index;
	int r,g,b;
	double size = 100.0 / double(this->width * this->height);
	
	for (int tileRow = 0; tileRow < oldHugeImage->height; tileRow+= oldHugeImage->tileHeight)
	{
		for (int tileCol = 0; tileCol < oldHugeImage->width; tileCol+= oldHugeImage->tileWidth)
		{
			oldHugeImage->getTileBuf(tileRow,tileCol,srcBuf);
			
			// this is to apply the wallis filter to the new image
			if (oldHugeImage->transferFunction)
			{
				// correct the offset: 
				// getTileBuf writes TileRow*width as offset
				// getRect writes just row as offset
				// applyTransferFunction expects row as offset
				srcBuf.setOffset(srcBuf.getOffsetX()/ srcBuf.getWidth(),srcBuf.getOffsetY()/ srcBuf.getHeight());
							
				// the huge image has only full tiles, so if the last tile
				// exceeds the image extents we need to correct the actual height and/or width
				// the wallis filter only has parameters for pixel inside the image
				int actualWidth = srcBuf.getWidth();
				int actualHeight = srcBuf.getHeight();
				
				if ((actualWidth + srcBuf.getOffsetX()) > oldHugeImage->width)
					actualWidth -= actualWidth + srcBuf.getOffsetX() - oldHugeImage->width;

				if ((actualHeight + srcBuf.getOffsetY()) > oldHugeImage->height)
					actualHeight -= actualHeight + srcBuf.getOffsetY() - oldHugeImage->height;

				oldHugeImage->transferFunction->applyTransferFunction(srcBuf,srcBuf,actualHeight,actualWidth,1,true);
			}	

			if (applyTransferFunction && tmpTransferFunction)
			{
				if (oldHugeImage->bands == 1)
				{
					// now we also apply the lookup table, but not the mapping to 0-255
					if (oldHugeImage->bpp == 8)
					{
						for (int row=0; row < srcBuf.getHeight(); row++)
						{
							rowOffset = row * srcBuf.getWidth() * srcBuf.getDiffX();
							for (int col=0; col < srcBuf.getWidth(); col++)
							{
								index  = rowOffset+col*srcBuf.getDiffX(); ;
								tmpTransferFunction->getColor(r,g,b,srcBuf.pixUChar(index));
								srcBuf.pixUChar(index) = r;
							}
						}
					}
					else if (oldHugeImage->bpp == 16)
					{
						for (int row=0; row < srcBuf.getHeight(); row++)
						{
							rowOffset = row * srcBuf.getWidth() * srcBuf.getDiffX();
							for (int col=0; col < srcBuf.getWidth(); col++)
							{
								index  = rowOffset+col*srcBuf.getDiffX(); ;
								tmpTransferFunction->getColor(r,g,b,srcBuf.pixShort(index));
								srcBuf.pixShort(index) = r;
							}
						}
					}
				}
				else
				{
					if (oldHugeImage->bpp == 8)
					{
						for (int row=0; row < srcBuf.getHeight(); row++)
						{
							rowOffset = row * srcBuf.getWidth() * srcBuf.getDiffX();
							for (int col=0; col < srcBuf.getWidth(); col++)
							{
								index  = rowOffset+col*srcBuf.getDiffX(); ;
								tmpTransferFunction->getColor(r,g,b,srcBuf.pixUChar(index),srcBuf.pixUChar(index + 1),srcBuf.pixUChar(index + 2));
								srcBuf.pixUChar(index    ) = r;
								srcBuf.pixUChar(index + 1) = g;
								srcBuf.pixUChar(index + 2) = b;
							}
						}
					}
					else if (oldHugeImage->bpp == 16)
					{
						for (int row=0; row < srcBuf.getHeight(); row++)
						{
							rowOffset = row * srcBuf.getWidth() * srcBuf.getDiffX();
							for (int col=0; col < srcBuf.getWidth(); col++)
							{
								index  = rowOffset+col*srcBuf.getDiffX(); ;
								tmpTransferFunction->getColor(r,g,b,srcBuf.pixShort(index),srcBuf.pixShort(index + 1),srcBuf.pixShort(index + 2));
								srcBuf.pixUChar(index    ) = r;
								srcBuf.pixUChar(index + 1) = g;
								srcBuf.pixUChar(index + 2) = b;
							}
						}
					}
				}
			}

			this->dumpTile(srcBuf);

			if (this->listener)
			{
				double value = double(tileRow * this->width + tileCol) * size; 
				this->listener->setProgressValue(value);
			}
			
		}
	}

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
	}

	ret = this->finishWriting();

	// keep old transfer function when color indexed image
	if (oldHugeImage->transferFunction && oldHugeImage->transferFunction->getTransferFunctionType() == CTransferFunction::eColorIndexed)
		this->setTransferFunction(oldHugeImage->transferFunction,true,true);

	this->open(this->getFileNameChar(),true);	
	

	if (tmpTransferFunction)
		delete tmpTransferFunction;
	tmpTransferFunction = NULL;
	return ret;
}

bool CHugeImage::computeHistogramsForROI(int rectTop,int rectHeight,int rectLeft,int rectWidth,vector<CHistogram*>& histograms,CHistogram& intensityHistogram)
{
	if ( rectTop < 0 || rectTop > this->height ||
		(rectTop+rectHeight) > this->height ||
		rectLeft < 0 || rectLeft > this->width ||
		(rectLeft+rectWidth) > this->width)
	{
		return false;
	}

	if (histograms.size() != this->bands)
		return false;

	for (int i=0; i < this->bands; i++)
		if (histograms[i]->getSize() != this->Histograms[0]->getSize())
			return false;

	if (this->bands > 1 && intensityHistogram.getSize() != this->Histograms[0]->getSize())
		return false;

	// determine needed tiles
 	// x direction
    int firstTileX = rectLeft/this->tileWidth;
    int lastTileX = (rectLeft+rectWidth)/this->tileWidth;
    int nTilesX = lastTileX-firstTileX+1;

	// y direction
    int firstTileY = rectTop/this->tileHeight;
	int lastTileY = (rectTop+rectHeight)/this->tileHeight;
    int nTilesY = lastTileY-firstTileY+1;

	// save the sizes and start row of each tile, that saves the if's in the loops 
	// and therefor time ...
	vector<int> tileSizeX(nTilesX);
	vector<int> tileSizeY(nTilesY);

	vector<int> startCol(nTilesX);
	vector<int> startRow(nTilesY);

	for (int i=0; i < nTilesX; i++)
	{
		tileSizeX[i] = this->tileWidth;
		startCol[i] = 0;
	}

	// correct first start col and last tileSize
	tileSizeX[nTilesX-1] = rectLeft + rectWidth - lastTileX * this->tileWidth;
	startCol[0] = rectLeft - firstTileX * this->tileWidth;

	for (int i=0; i < nTilesY; i++)
	{
		tileSizeY[i] = this->tileHeight;
		startRow[i] = 0;
	}

	// correct first start row and last tileSize
	tileSizeY[nTilesY-1] =  rectTop + rectHeight - lastTileY * this->tileHeight;
	startRow[0] = rectTop - firstTileY * this->tileHeight;

	unsigned char* ucBuf = NULL;
	unsigned short* usBuf = NULL;
	float* fBuf = NULL;
	CImageTile* imageTile = NULL;

	// variables for the loops
	int indexBuf=0;
	int rowOffset=0;
	int tileX,rowTile,colTile,colImage,band;
	int diffX = bands > 1 ? 4 : 1;
	unsigned long intensity=0;
	double fact = 1.0 /3.0;
	float value;

	for (int tileY=0,rowImage=rectTop; tileY < nTilesY; tileY++,rowImage+=this->tileHeight)
	{
		for (tileX=0,colImage=rectLeft; tileX < nTilesX; tileX++,colImage+=this->tileWidth)
		{
			if (!this->getImageTile(&imageTile,rowImage,colImage))
				return false;

			 ucBuf = imageTile->getData();

			 if (this->bpp == 8)
			 {
				 // loop over tile
				 for (rowTile = startRow[tileY]; rowTile < tileSizeY[tileY]; rowTile++)
				 {
					 // the buffer has always the size 	this->tileWidth* this->tileHeight, also for last tile			
					 rowOffset = rowTile * this->tileWidth * diffX;

					 for (colTile = startCol[tileX]; colTile < tileSizeX[tileX]; colTile++)
					 {
						indexBuf = rowOffset + colTile * diffX;
						intensity=0;
						for (band = 0; band < this->bands; band++)
						{
							histograms[band]->increment(ucBuf[indexBuf+band]);
							intensity += ucBuf[indexBuf+band];
						}

						if (this->bands > 1)
							intensityHistogram.increment(int(fact * intensity));
					 }
				}
			 }
			 else if (this->bpp == 16)
			 {
				 usBuf = (unsigned short*)ucBuf;
				 // loop over tile
				 for (rowTile = startRow[tileY]; rowTile < tileSizeY[tileY]; rowTile++)
				 {
					 // the buffer has always the size 	this->tileWidth* this->tileHeight, also for last tile			
					 rowOffset = rowTile * this->tileWidth * diffX;

					 for (colTile = startCol[tileX]; colTile < tileSizeX[tileX]; colTile++)
					 {
						indexBuf = rowOffset + colTile * diffX;
						intensity=0;
						for (band = 0; band < this->bands; band++)
						{
							histograms[band]->increment(usBuf[indexBuf+band]);
							intensity += usBuf[indexBuf+band];
						}

						if (this->bands > 1)
							intensityHistogram.increment(int(fact * intensity));
					 }
				}
			 }
			 else if (this->bpp == 32)
			 {
				 fBuf = (float*)ucBuf;
				 // loop over tile
				 for (rowTile = startRow[tileY]; rowTile < tileSizeY[tileY]; rowTile++)
				 {
					 // the buffer has always the size 	this->tileWidth* this->tileHeight, also for last tile			
					 rowOffset = rowTile * this->tileWidth * diffX;

					 for (colTile = startCol[tileX]; colTile < tileSizeX[tileX]; colTile++)
					 {
						indexBuf = rowOffset + colTile * diffX;
						intensity=0;
						for (band = 0; band < this->bands; band++)
						{
							value = fBuf[indexBuf+band];
							if (value != -9999.0)
								histograms[band]->checkFltMinMax(value);
						}
					 }
				}
			 }
		} // tile X direction

		if (this->listener != NULL)
		{
			double p = 100.0*double(tileY)/double(nTilesY);
			this->listener->setProgressValue(p);
		}
	} // tile Y direction


	for (int band = 0; band < this->bands; band++)
	{
		histograms[band]->updateMinMax(0.02,0.98);
	}

	
	return true;
}

CWallisFilter* CHugeImage::createWallisFilter(int windowSizeX,
									  int windowSizeY,
									  double contrastParameter, 
								      double brightnessParameter,
								      double mean,
								      double stdDev,
									  bool writeParametersToFile)
{
	CWallisFilter* wallis = NULL;
	
	if (this->bands == 1)
	{
		wallis = (CWallisFilter*)CTransferFunctionFactory::createWallisFilterFunction(this->filename,this->bpp,this->Histograms[0]->getMin(),this->Histograms[0]->getMax());
	}
	else if (this->intensityHistogram)
	{
		wallis = (CWallisFilter*)CTransferFunctionFactory::createWallisFilterFunction(this->filename,this->bpp,this->intensityHistogram->getMin(),this->intensityHistogram->getMax());
	}

	// when wallis is initialized then we could read the filter directly from a file
	// check also if we could read a file that matches the parameters we want
	if ( wallis &&
		(!wallis->isInitialized() || 
		 !wallis->hasIdenticalParameters(windowSizeX,windowSizeY,contrastParameter,brightnessParameter,mean,stdDev,0,0,this->width,this->height)))
	{
		int nParametersX,nParametersY;
		// ok, then we have to compute the parameters
		if (this->computeMeanAndStdDev(wallis->getMeanVector(),wallis->getStdDevVector(),nParametersX,nParametersY,windowSizeX,windowSizeY,0,this->height,0,this->width))
		{
			wallis->setParameters(	windowSizeX,
									windowSizeY,
									nParametersX,
									nParametersY,
									contrastParameter,
									brightnessParameter,
									mean,
									stdDev,
									0,
									0,
									this->width,
									this->height);

			wallis->getMeanVector().clear();
			wallis->getStdDevVector().clear();

			if (writeParametersToFile)
				wallis->writeWallisFilterFile();
		}
		else
		{
			delete wallis;
			wallis = NULL;
		}
		
	}


	return wallis;
}



bool CHugeImage::getImageTile(CImageBuffer& imageBuf,int tileRow,int tileCol,float zoomFactor)
{
	bool ret = true;

	imageBuf.set(tileCol,tileRow,this->getTileWidth(),this->getTileHeight(),this->getBands(),this->getBpp(),this->getBpp() < 32 ? true : false);

	int Z = (int)(zoomFactor);

	if (Z >= 1)
        ret = this->getTileZoomUp(imageBuf, tileRow, tileCol, Z);
	else if (Z == 0)
    {
        Z = (int)(1.0/zoomFactor + 0.5);
        ret = this->getTileZoomDown(imageBuf, tileRow, tileCol, Z);
    }


	return ret;
}

bool CHugeImage::getRect(CImageBuffer &buffer, int rectTop, int rectHeight, int rectLeft, int rectWidth, int destHeight, int destWidth)
{
	bool ret = true;

    if (destHeight == -1)
    {
        double factor = (double)destWidth/(double)rectWidth;
        destHeight = (int)((double)rectHeight*factor+0.5);
    }
    else if (destWidth == -1)
    {
        double factor = (double)destHeight/(double)rectHeight;
        destWidth = (int)((double)rectWidth*factor+0.5);
    }

	ret = buffer.set(rectLeft,rectTop,destWidth,destHeight,this->getBands(),this->getBpp(),this->getBpp() < 32 ? true : false);

	if (ret)
	{
		if (this->getBpp() == 8)
			ret = this->getRect(buffer.getPixUC(),rectTop, rectHeight, rectLeft, rectWidth, destHeight, destWidth);
		else if (this->getBpp() == 16)
			ret = this->getRect((unsigned char*)buffer.getPixUS(),rectTop, rectHeight, rectLeft, rectWidth, destHeight, destWidth);
		else if (this->getBpp() == 32)
			ret = this->getRect((unsigned char*)buffer.getPixF(),rectTop, rectHeight, rectLeft, rectWidth, destHeight, destWidth);
	}

	return ret;
}
