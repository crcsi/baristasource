/** 
 * @class CG3DFile
 * class for reading DEM data and fill DEM
 * 
 * @author Jochen Willneff
 * @version 1.0 
 * @date 20-mar-2006
 */

#include "G3DFile.h"
#include "Filename.h"

CG3DFile::~CG3DFile(void)
{
	if (this->zdata) delete [] this->zdata;
	this->zdata = 0;

	this->close();
}

CG3DFile::CG3DFile() : CImageFile(eG3DFile)
{
	this->bHeaderRead = false;
	this->fp = 0;
	this->setBands(1);
	this->setBpp(32);
}

/** 
  * Construct the CG3DFile with the given file name
  * no io is performed...
  *
  */
CG3DFile::CG3DFile(const char* filename) : CImageFile(eG3DFile,filename)
{
	this->bHeaderRead = false;
	this->fp = 0;
	this->setBands(1);
	this->setBpp(32);
}


/**
 * private function to ensure the actual FILE* is closed
 *
 */
void CG3DFile::close()
{
	if (this->fp != NULL)
	{
		fclose(this->fp);
		this->fp = NULL;
	}
}


/**
 * gets the image width
 * @return the width
 */
int CG3DFile::getWidth()
{
	if (!this->bHeaderRead)
		this->read();

	return this->nCols;
}

/**
 * gets the image height
 * @return the height
 */
int CG3DFile::getHeight()
{
	if (!this->bHeaderRead)
		this->read();

	return this->nRows;
}

/**
 * gets the gridx
 * @return gridx
 */
double CG3DFile::getgridX()
{
	if (!this->bHeaderRead)
		this->read();

	return this->gridx;
}

/**
 * gets the gridy
 * @return gridy
 */
double CG3DFile::getgridY()
{
	if (!this->bHeaderRead)
		this->read();

	return this->gridy;
}

/**
 * gets the xmin
 * @return xmin
 */
double CG3DFile::getminX()
{
	if (!this->bHeaderRead)
		this->read();

	return this->xmin;
}

/**
 * gets the ymin
 * @return ymin
 */
double CG3DFile::getminY()
{
	if (!this->bHeaderRead)
		this->read();

	return this->ymin;
}


/**
 * @return the number of bits per pixel, set to 32
 */
int CG3DFile::getBpp()
{
	return this->bpp;
}

/**
 * @return the number of bands, set to 1
 */
int CG3DFile::bands()
{
	return this->nBands;
}


/**
 * Reads one scan line into buffer.
 *
 * @param buffer the destination buffer
 * @param iRow the row to read
 * @return false for failure
 */
bool CG3DFile::readLine(unsigned char* buffer, int iRow)
{
	if (iRow >= this->nRows)
		return false;

	memcpy(buffer, this->zdata+this->nCols*iRow, this->nCols*4);
	return true;
}

/**
 *  writes a G3D file from a CBaseImage
 *
 * @param fileName the output file name
 * @param image the src image
 *
 */
bool CG3DFile::write(const char* filename, CBaseImage* image)
{
    this->nCols = image->getWidth();
    this->nRows = image->getHeight();

    return true;
}


bool CG3DFile::read()
{

	if (this->fp != NULL)
		this->close();

	this->fp = fopen(filename, "rb");

	if (!fp)
		return false;

	int dum1 = 0;

	fread(&dum1, sizeof(int), 1, fp);
	fread(&this->xmin, sizeof(float), 1, fp);
	fread(&this->xmax, sizeof(float), 1, fp);
	fread(&this->ymin, sizeof(float), 1, fp);
	fread(&this->ymax, sizeof(float), 1, fp);
	fread(&this->zmin, sizeof(float), 1, fp);
	fread(&this->zmax, sizeof(float), 1, fp);
	fread(&dum1, sizeof(int), 1, fp);

	fread(&dum1, sizeof(int), 1, fp);
	fread(&this->xmin, sizeof(float), 1, fp);
	fread(&this->ymin, sizeof(float), 1, fp);
	fread(&this->gridx, sizeof(float), 1, fp);
	fread(&this->nCols, sizeof(short), 1, fp);
	fread(&this->nRows, sizeof(short), 1, fp);
	fread(&dum1, sizeof(int), 1, fp);

	this->zdata = new float[this->nCols*this->nRows];

	for(int i = 0; i < this->nRows; i++)
	{
		fread((char*)(&dum1), sizeof(int), 1, fp);
		fread((char*)(this->zdata + (this->nRows-1-i)*this->nCols), sizeof(float), this->nCols, fp);
		fread((char*)(&dum1), sizeof(int), 1, fp);
	}

	this->close();

	
	for(int i = 0; i < this->nCols*this->nRows; i++)
	{
		float Z = this->zdata[i];

		//if ( Z == DEMG3D_NOHEIGHT)
		if ( Z > 10000000 || Z < -10000000)
		{
			this->zdata[i] = -9999.0;
		}
	}
	

	this->bHeaderRead = true;

	return true;
}

