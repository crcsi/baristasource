#include <float.h>

#include "AVNIR2File.h"
#include "gdal_priv.h"

#define DEGREE 0

CAVNIR2File::CAVNIR2File() : CGDALFile(),firstRowOffset(0),sizeOffset(1),
							avnirBuffer(0),offsetFunction(DEGREE),offsetSecondPixel(0)
{
	this->type = eAVNIR2File;
	this->initOffsetFunction(0.0);


}

CAVNIR2File::CAVNIR2File(const CGDALFile& gdal) : CGDALFile(gdal),firstRowOffset(0),
							sizeOffset(1),avnirBuffer(0),offsetFunction(DEGREE),offsetSecondPixel(0)
{
	this->type = eAVNIR2File;
	this->initOffsetFunction(0.0);
}

CAVNIR2File::CAVNIR2File(const char* filename) : CGDALFile(filename),firstRowOffset(0),
							sizeOffset(1),avnirBuffer(0),offsetFunction(DEGREE),offsetSecondPixel(0)
{
	this->type = eAVNIR2File;
	this->initOffsetFunction(0.0);
}

CAVNIR2File::~CAVNIR2File()
{
	if (!this->avnirBuffer)
		delete this->avnirBuffer;
	this->avnirBuffer = NULL;

}

int CAVNIR2File::getHeight()
{
	return this->nRows - (int(this->offsetFunction.functionValueAt(0)) + 2);
}

void CAVNIR2File::initOffsetFunction(double theta)
{
	if (this->offsetFunction.getDegree() != DEGREE)
	{
		CPolynomial p(DEGREE);
		this->offsetFunction = p;
	}

	this->offsetFunction.setCoefficient(0,5.0 / cos(theta));
}

bool CAVNIR2File::readLine(unsigned char* buffer, int iRow)
{
	bool ret = true;

	if ( ret && !this->needsModifiedJPEGLib && this->gdal == NULL )
	{
		GDALAllRegister();
		this->gdal = (GDALDataset *) GDALOpen( this->filename, GA_ReadOnly );

	}

	if (!this->gdal)
		ret = false;

	// we can only handle untiled AVNIR images with one band and 8 bit
	if (ret && (this->tileWidth != this->nCols || this->nBands != 1 || this->bpp != 8)) // read at least the data
	{
		if (iRow < 0 || iRow >= this->nRows)
			ret = false;
		else
			ret = CGDALFile::readLine(buffer,iRow);
	}
	else if (ret)
	{
		// compute first the offset for every pixel
		if (this->ccdPos.size() != this->nCols || this->offsetRow.size() != this->nCols || !this->avnirBuffer )
		{
			if (this->avnirBuffer)
				delete this->avnirBuffer;
			this->avnirBuffer = NULL;

			this->ccdPos.resize(this->nCols);
			this->offsetRow.resize(this->nCols);

			float min = FLT_MAX;
			float max = FLT_MIN;

			// compute offsets
			for (unsigned int i =0; i < this->ccdPos.size(); i+=2)
			{
				this->ccdPos[i] = float(this->offsetFunction.functionValueAt(double(i)));
				
				if (this->ccdPos[i] < min)
					min = this->ccdPos[i];
				if (this->ccdPos[i] > max)
					max = this->ccdPos[i];
			}
		
			if (min > max)
			{
				float tmp = max;
				max = min;
				min = tmp;
			}
			
			this->firstRowOffset = int(min);  
			this->sizeOffset = (unsigned int)((max - min) + 1.0);

			// if max and min are different then we have to interpolate, so we need one row more
			if ( fabs(max - this->firstRowOffset) > FLT_EPSILON)
			{
				this->sizeOffset++;
				this->offsetSecondPixel = this->nCols;
			}

			// ccdPos refers to firstRowOffset, so correct the pos
			// ccdPos will store the variation within the pixel
			// offsetRow will store the according row in avnirBuffer
			for (unsigned int i =0; i < this->ccdPos.size(); i+=2)
			{
				// now with regard to avnirBuffer
				this->ccdPos[i] -=  float(this->firstRowOffset);
				
				// cut full integer, the row within avnirBuffer
				this->offsetRow[i] = int(this->ccdPos[i]);
				
				// save pos within column
				this->ccdPos[i] -= float(this->offsetRow[i]);

				// compute as index in one dimensional array
				this->offsetRow[i] = this->offsetRow[i] * this->nCols + i;

			}		

			// create buffer with sizeOffset rows
			this->avnirBuffer = new unsigned char[this->nCols * this->sizeofpixel * this->sizeOffset];
			
		}
		

		if (iRow < 0 || 
			iRow >= this->nRows || 
			(iRow + this->firstRowOffset) < 0 || 
			((unsigned int)(iRow + this->firstRowOffset) + this->sizeOffset) >= (unsigned int) (this->nRows))
		{
			ret = false;
		}
		else
		{
			this->read1BandLine(buffer,iRow);
			this->read1BandLine(this->avnirBuffer,iRow + this->firstRowOffset,this->sizeOffset);
		

			unsigned char firstPixel;
			float secondPixel;
			float diffGreyValues;

			for (unsigned int i=0; i < this->ccdPos.size(); i+=2)
			{
				firstPixel = this->avnirBuffer[this->offsetRow[i]];
				secondPixel = this->avnirBuffer[this->offsetRow[i] + this->offsetSecondPixel];
				diffGreyValues = secondPixel - float(firstPixel);
				
				// interpolate the new gray value linear
				buffer[i] = firstPixel + (unsigned char)(diffGreyValues * this->ccdPos[i]);
			}

			ret = true;
		}
	}

	return ret;
}
