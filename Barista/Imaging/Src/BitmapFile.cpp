// MUBitmapFile.cpp: implementation of the CBitmapFile class.
//
//////////////////////////////////////////////////////////////////////
#include <assert.h>

#include "BitmapFile.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBitmapFile::CBitmapFile() : CImageFile(eBITMAPFile)
{
	this->pFileBuf = NULL;
	this->fp = NULL;
	this->bHeaderRead = false;
	this->bPadded = false;
	
}

CBitmapFile::CBitmapFile(const char* pFileName) : CImageFile(eBITMAPFile,pFileName)
{
	this->pFileBuf = NULL;
	this->fp = NULL;
	this->Header.SetFileName(pFileName);
	this->bHeaderRead = false;
	this->bPadded = false;
}

CBitmapFile::~CBitmapFile()
{
	if (this->pFileBuf != NULL)
		delete [] this->pFileBuf;

    if (this->fp != NULL)
    {
        fclose(this->fp);
        this->fp = NULL;
    }

}

bool CBitmapFile::readLine(unsigned char* pBuffer, int iRow)
{
	if (iRow == 0)
	{
		this->nCols = this->Header.GetCols();
		this->nRows = this->Header.GetRows();
		this->nBands = this->Header.GetNumberOfChannels();
		this->bpp = this->Header.GetNumberOfBitsPerChannel();

		assert(this->bpp == 8);
		assert(this->nBands == 3);

		fp = fopen(this->filename, "rb");

		//Open(this->FileName, CFileArchive::READ);

		//if (is_open() == 0)
		//	return false;

		fseek(fp, 2, SEEK_SET);
//		seekg(2,ios::beg);
		fread((char *)&this->FileSize, 1, sizeof(long), fp);

		fseek(fp, 10, SEEK_SET);

//		seekg(10,ios::beg);
		fread((char *) &this->HeaderSize, 1, sizeof(long), fp);

		// RecordLength is the length of a line of data 
		// PLUS any padding for byte alignement, so it
		// might be slightly loner that number of cols
		this->RecordLength = (this->FileSize - this->HeaderSize) / this->nRows;

		if (this->pFileBuf != NULL)
			delete [] this->pFileBuf;

		this->pFileBuf = new unsigned char[this->RecordLength];

		this->bHeaderRead = true;
	}

	long offset = this->HeaderSize + (this->nRows - iRow - 1) * this->RecordLength;

	fseek(fp, offset, SEEK_SET);
//	seekg(offset, ios::beg);

	// Read the line 
	fread((char *) this->pFileBuf, this->RecordLength, 1, fp);

	unsigned char* src = this->pFileBuf;
	unsigned char* dest = pBuffer;

	for (int i = 0; i < this->nCols; i++)
	{
		::memcpy(dest, src, 3);
		dest[3] = 0;

		dest += 4;
		src += 3;
	}

	return true;
}

bool CBitmapFile::writeLine(unsigned char* pBuffer, int iRow)
{
	// A bitmap file is written upside down so...
	// write the header before any lines are written
	if (iRow == this->nRows - 1)
	{
		// Declare local variables
		long bfSize,bfOffBits,biSize,biWidth,biHeight,biCompression,biSizeImage,biXPelsPerMeter,biYPelsPerMeter,biClrUsed,biClrImportant;
		short reserved,biPlanes,biBitCount;

		// Record length
		this->RecordLength = this->nBands * this->nCols;

		// Make sure the record length is 4-byte aligned
		this->bPadded = false;
		if ((this->RecordLength % 4) != 0)
		{
			this->bPadded = true;
			this->RecordLength += (4 - (this->RecordLength % 4));
		}

		// Assign bitmap parameters
		char ch[]="BM";
		bfSize = (long)(this->nRows * this->RecordLength + BMP_TOTAL_HEADER_SIZE);
		reserved = 0;
		bfOffBits = BMP_TOTAL_HEADER_SIZE;
		biSize = BMP_TOTAL_HEADER_SIZE - BMP_COMMON_HEADER_SIZE;
		biWidth = (long)this->nCols;
		biHeight = (long)this->nRows;
		biPlanes = 1;

		if (this->nBands == 3)
			biBitCount = 24; // RGB only for now
		else
			biBitCount = 8;

		biCompression = 0;
		biSizeImage = 0;
		biXPelsPerMeter = 0;
		biYPelsPerMeter = 0;
		biClrUsed = 0;
		biClrImportant = 0;

		// Write tital head 54 bytes = 14 + 40
		//
		this->fp = fopen(this->filename, "rb");
		//Open(this->FileName, CFileArchive::WRITE);

		// Write BITMAP common header file (14 bytes)
		fputc(ch[0], fp);
		fputc(ch[1], fp);
		fwrite(&bfSize, sizeof(long), 1, this->fp);
		fwrite(&reserved, sizeof(short), 1, this->fp);
		fwrite(&reserved, sizeof(short), 1, this->fp);
		fwrite(&bfOffBits, sizeof(long), 1, this->fp);

		// Write Windows BITMAPINFOHEADER image file (40 bytes)
		fwrite(&biSize, sizeof(long), 1, this->fp);
		fwrite(&biWidth, sizeof(long), 1, this->fp);
		fwrite(&biHeight, sizeof(long), 1, this->fp);
		fwrite(&biPlanes, sizeof(short), 1, this->fp);
		fwrite(&biBitCount, sizeof(short), 1, this->fp);
		fwrite(&biCompression, sizeof(long), 1, this->fp);
		fwrite(&biSizeImage, sizeof(long), 1, this->fp);
		fwrite(&biXPelsPerMeter, sizeof(long), 1, this->fp);
		fwrite(&biYPelsPerMeter, sizeof(long), 1, this->fp);
		fwrite(&biClrUsed, sizeof(long), 1, this->fp);
		fwrite(&biClrImportant, sizeof(long), 1, this->fp);

		this->pFileBuf = new unsigned char[this->RecordLength];
	}

	// Write data (BIL)
	long ChannelSize = this->nCols;
	int iCount = 0;

	for (int iCol = 0; iCol < this->nCols; iCol++)
	{
		for (int iChannel = this->nBands - 1; iChannel >= 0; iChannel--)
		{
			long ChannelOffset = ChannelSize * iChannel;
			long index = ChannelOffset + iCol;
			this->pFileBuf[iCount] = pBuffer[index];
			iCount++;
		}
	}

	// Pad with blanks if necessary
	if (this->bPadded)
	{
		for (int iPad = 0; iPad < (this->RecordLength - (3 * this->nCols)); iPad++)
		{
//			unsigned int c = (unsigned int)"\0";
			this->pFileBuf[iCount] = '\0';//(unsigned char) c;
			iCount++;
		}
	}

	// Put line on disk
	fwrite(this->pFileBuf, (size_t)this->RecordLength, 1, this->fp);	

	return true;
}

int CBitmapFile::getWidth()
{
	this->nCols = this->Header.GetCols();
	return this->nCols;
}

int CBitmapFile::getHeight()
{
	this->nRows = this->Header.GetRows();
	return this->nRows;
}

int CBitmapFile::getBpp()
{
	this->bpp = this->Header.GetNumberOfBitsPerChannel();
	return this->bpp;
}

int CBitmapFile::bands()
{
	this->nBands = this->Header.GetNumberOfChannels();
	return this->nBands;
}




