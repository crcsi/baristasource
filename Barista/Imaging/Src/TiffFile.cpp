/** 
 * @class CTiffFile
 * concrete tiff image file class
 * a wrapper around the libtiff library
 * support for large imagery > 100MB is a prime concern in this class
 * uses libtiff http://www.remotesensing.org/libtiff/
 * 
 * @author Harry Hanley
 * @version 1.0 
 * @date 20-jan-2005
 */

#include "TiffFile.h"
#include "tiffio.h"
#include "ZRasterHelper.h"
#include <string.h>
#include <fstream>

#ifndef WIN32
//#define	isTiled(tif)	(((tif)->tif_flags & TIFF_ISTILED) != 0)
#endif

/**
 * Default constructor
 *
 */
CTiffFile::CTiffFile() : CImageFile(eTIFFFile)
{
	this->bHeaderRead = false;
	this->tif = NULL;
	this->scanLineBuffer = NULL;
	this->frameBuffer = NULL;
	this->tiles = NULL;
	this->tilesAcross = 0;
	this->tilesDown = 0;
	this->tileByteSize = 0;
	this->tileWidth = 0;
	this->tileHeight = 0;
	this->nBufferedTiles = 0;
	this->tileBuffer = NULL;
	this->lastBufferedTile = -1;
	this->planarConfig = 0;
	this->fp = 0;
	this->stripOffset = -1;
	this->tiled = false;
	this->tiledWrite = true;
}

/** 
  * Construct the CTiffFile with the given file name
  * no io is performed...
  *
  */
CTiffFile::CTiffFile(const char* filename) : CImageFile(eTIFFFile,filename)
{
	this->bHeaderRead = false;
	this->tif = NULL;
	this->scanLineBuffer = NULL;
	this->frameBuffer = NULL;
	this->tiles = NULL;
	this->tilesAcross = 0;
	this->tilesDown = 0;
	this->tileByteSize = 0;
	this->tileWidth = 0;
	this->tileHeight = 0;
	this->nBufferedTiles = 0;
	this->tileBuffer = NULL;
	this->lastBufferedTile = -1;
	this->planarConfig = 0;
	this->fp = 0;
	this->stripOffset = -1;
	this->tiled = false;
	this->tiledWrite = true;
}


/**
 *  writes a tile of imagery to the tif file
 *
 * @param buf the source buffer RGB data is organised as BGR0BGR0
 * @param r, c the upper left row col defining corner of tile
 * @return false for failure
 */
bool CTiffFile::writeTile(unsigned char* buf, int r, int c)
{
    if (this->nBands == 3)
    {
		if ( this->bpp == 8 )
		{
			// create tmp buffer
			unsigned char* RGB = new unsigned char[this->tileWidth*this->tileHeight*3];

			// reorder the bytes to RGBRGB... from BGR0BGR0...
			for (int i = 0; i < (int)(this->tileWidth*this->tileHeight); i++)
			{
				RGB[i*3+0] = buf[i*4+2];
				RGB[i*3+1] = buf[i*4+1];
				RGB[i*3+2] = buf[i*4+0];
			}

			TIFFWriteTile(this->tif, RGB, c, r, 0, 0);

			// delete tmp buffer
			delete [] RGB;
		}
		if ( this->bpp == 16 )
		{
			unsigned short* shortbuf = (unsigned short*)buf;

			// create tmp buffer
			unsigned short* RGB = new unsigned short[this->tileWidth*this->tileHeight*3];
			
			// reorder the bytes to RGBRGB... from BGR0BGR0...
			for (unsigned long i = 0; i < (unsigned long)(this->tileWidth*this->tileHeight); i++)
			{
				unsigned long i3 = i*3;
				unsigned long i4 = i*4;
				
				RGB[i3+0] = shortbuf[i4+2];
				RGB[i3+1] = shortbuf[i4+1];
				RGB[i3+2] = shortbuf[i4+0];
			}

			TIFFWriteTile(this->tif, RGB, c, r, 0, 0);

			// delete tmp buffer
			delete [] RGB;
		}
    }
    else if (this->nBands == 4)
    {
		if ( this->bpp == 8 )
		{
			// create tmp buffer
			unsigned char* RGBN = new unsigned char[this->tileWidth*this->tileHeight*4];

			for (int i = 0; i < (int)(this->tileWidth*this->tileHeight); i++)
			{
				RGBN[i*4+0] = buf[i*4+2];
				RGBN[i*4+1] = buf[i*4+1];
				RGBN[i*4+2] = buf[i*4+0];
				RGBN[i*4+3] = buf[i*4+3];
			}

			TIFFWriteTile(this->tif, RGBN, c, r, 0, 0);

			// delete tmp buffer
			delete [] RGBN;
		}
		if ( this->bpp == 16 )
		{
			unsigned short* shortbuf = (unsigned short*)buf;

			// create tmp buffer
			unsigned short* RGBN = new unsigned short[this->tileWidth*this->tileHeight*4];
			
			// reorder the bytes to RGBNRGBN... from BGRNBGRN...
			for (unsigned long i = 0; i < (unsigned long)(this->tileWidth*this->tileHeight); i++)
			{
				unsigned long i4 = i*4;
				
				RGBN[i4+0] = shortbuf[i4+0];
				RGBN[i4+1] = shortbuf[i4+1];
				RGBN[i4+2] = shortbuf[i4+2];
				RGBN[i4+3] = shortbuf[i4+3];
			}

			TIFFWriteTile(this->tif, RGBN, c, r, 0, 0);

			// delete tmp buffer
			delete [] RGBN;
		}
    }
    else
    {
        // grey scale fits normally
        TIFFWriteTile(this->tif, buf, c, r, 0, 0);
    }

    return true;
}

/**
 *  writes a tiff file from a CBaseImage
 *
 * @param fileName the output file name
 * @param image the src image
 *
 */
bool CTiffFile::write(const char* filename, CBaseImage* image)
{
    this->nCols = image->getWidth();
    this->nRows = image->getHeight();
    this->nBands = image->getBands();
    this->bpp = image->getBpp();
    ::strcpy(this->filename, filename);

	if(this->tiledWrite)
	{
		this->tiled = true;
		this->tileWidth = image->getTileWidth();
		this->tileHeight = image->getTileHeight();
		
		// write header
		this->_writeHeader();
	    
		for (int r = 0; r < this->nRows; r+= this->tileHeight)
		{
			for (int c = 0; c < this->nCols; c+= this->tileWidth)
			{
				this->writeTile(image->getTile(r, c), r, c);
			}
		}
	}
	else
	{
		this->tileHeight = 0;
		this->tileWidth = 0;

		this->stripOffset = image->pixelSizeBytes() - this->nBands;

		for (int r=0; r < this->nRows; r++)
		{
			unsigned char *pBitsLine = new unsigned char[this->nCols*image->pixelSizeBytes()];
			image->getRect(pBitsLine, r, 1, 0, this->nCols);

			// header will written within the function
			writeLine(pBitsLine, r);
			delete [] pBitsLine;
		}
	}

    return true;
}

/**
 * standard destructor
 *  
 */
CTiffFile::~CTiffFile()
{
	if (this->tiles)
	{
			
		for (int i = 0; i < this->tilesAcross; i++)
		{
			if (this->tiles[i] != NULL)
				delete this->tiles[i];
		}

		delete [] this->tiles;
	}

    if(this->scanLineBuffer != NULL)
        delete [] this->scanLineBuffer;

	if (this->frameBuffer != NULL)
		delete [] this->frameBuffer;

    if(this->tileBuffer != NULL)
        delete [] this->tileBuffer;

	_close();
}

/**
 * private function to ensure the actual FILE* is closed
 *
 */
void CTiffFile::_close()
{
    this->tileWidth = 0;
	this->tileHeight = 0;
	this->nBufferedTiles = 0;
	this->tileBuffer = NULL;

	if (this->tif != NULL)
	{
		TIFFClose(this->tif);
		this->tif = NULL;
	}
	else if (this->fp != NULL)
	{
		fclose(this->fp);
		this->fp = NULL;
	}
}

/**
 * private function that reads the tiff header
 *
 */
bool CTiffFile::_readHeader()
{
	if (this->tif != NULL)
		_close();

	this->tif = TIFFOpen(this->filename, "r");

	if (this->tif == NULL)	
		return false;

    TIFFGetField(this->tif, TIFFTAG_IMAGEWIDTH, &this->nCols);              // uint32 width;
    TIFFGetField(this->tif, TIFFTAG_IMAGELENGTH, &this->nRows);             // uint32 height;
	TIFFGetField(this->tif, TIFFTAG_BITSPERSAMPLE, &this->bpp); 
	TIFFGetField(this->tif, TIFFTAG_SAMPLESPERPIXEL, &this->nBands); 
	TIFFGetField(this->tif, TIFFTAG_PLANARCONFIG, &this->planarConfig); 
	TIFFGetField(this->tif, TIFFTAG_STRIPOFFSETS, &this->stripOffset);

	int rps = 0;
	TIFFGetField(this->tif, TIFFTAG_ROWSPERSTRIP, &rps);
	
	if (TIFFIsTiled(this->tif))
	{
		this->tiled = true;
		TIFFGetField(this->tif, TIFFTAG_TILEWIDTH, &this->tileWidth);
		TIFFGetField(this->tif, TIFFTAG_TILELENGTH, &this->tileHeight);
		//TIFFGetField(this->tif, TIFFTAG_TILEBYTECOUNTS, &tileSize);
		
		this->tilesAcross = (this->nCols + this->tileWidth - 1) / this->tileWidth;
		this->tilesDown = (this->nRows + this->tileHeight - 1) / this->tileHeight;

		// need to buffer at least tilesAcross tiles
		this->nBufferedTiles = this->tilesAcross;

		this->tiles = new CImageTile*[this->nBufferedTiles];

		int size = 0;
		size = ::TIFFTileSize(this->tif);
		this->tileBuffer = new unsigned char[size];

		for (int i = 0; i < this->tilesAcross; i++)
		{
			this->tiles[i] = NULL;
		}
	}

	this->scanLineSize = 0;
	
	this->scanLineSize  = (uint16)::TIFFScanlineSize(this->tif);

	//this->scanLineBuffer = new unsigned char[this->scanLineSize];

	this->bHeaderRead = true;
	return true;
}

/**
 * This doesn't actually "write" the header, it just prepares some header fields
 * This info gets written out automatically when the file is closed (or the object 
 * goes out of scope)
 */
bool CTiffFile::_writeHeader()
{
	if (this->tif != NULL)
	{
		TIFFClose(this->tif);
		this->tif = NULL;
	}

	this->tif = TIFFOpen(this->filename, "w");

	if (this->tif == NULL)
		return false;
		

    TIFFSetField(this->tif, TIFFTAG_BITSPERSAMPLE, this->bpp);
    TIFFSetField(this->tif, TIFFTAG_SAMPLESPERPIXEL, this->nBands);

    TIFFSetField(this->tif, TIFFTAG_IMAGEWIDTH, this->nCols);
    TIFFSetField(this->tif, TIFFTAG_IMAGELENGTH, this->nRows);

    TIFFSetField(this->tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(this->tif, TIFFTAG_ROWSPERSTRIP, 1);

    //TIFFSetField(this->tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);

	if (this->nBands == 3)
	{
		TIFFSetField(this->tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);

		long tiffresolution = 72;
		TIFFSetField(this->tif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
		TIFFSetField(this->tif, TIFFTAG_XRESOLUTION, (long)tiffresolution);
		TIFFSetField(this->tif, TIFFTAG_YRESOLUTION, (long)tiffresolution);

		if ( this->bpp == 16)
		{
			TIFFSetField(this->tif, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
		}
		
	}
	else if (this->nBands == 4)
	{
		TIFFSetField(this->tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);

		long tiffresolution = 72;
		TIFFSetField(this->tif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
		TIFFSetField(this->tif, TIFFTAG_XRESOLUTION, tiffresolution);
		TIFFSetField(this->tif, TIFFTAG_YRESOLUTION, tiffresolution);
	}
	else
	{
		TIFFSetField(this->tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	}
		
    if (this->tileHeight > 0)
    {
        // sanity check
        if (this->tileWidth == 0)
            return false;

		// calculate the number of tiles
		this->tilesAcross = this->nCols / this->tileWidth;

		// if there is a remainder, we need one more tile
		if (this->nCols % this->tileWidth != 0)
			this->tilesAcross++;

		this->tilesDown = this->nRows / (int)this->tileHeight;

		// if there is a remainder, we need one more tile
		if (this->nRows % this->tileHeight != 0)
			this->tilesDown++;

		TIFFSetField(this->tif, TIFFTAG_TILEWIDTH, (long)this->tileWidth);
		TIFFSetField(this->tif, TIFFTAG_TILELENGTH, (long)this->tileHeight);

		TIFFSetField(this->tif, TIFFTAG_TILEOFFSETS, (long)this->tilesAcross*this->tilesDown);
		TIFFSetField(this->tif, TIFFTAG_TILEBYTECOUNTS, (long)this->tilesAcross*this->tilesDown);
    }

	return true;
}

/**
 * gets the image width
 * @return the width
 */
int CTiffFile::getWidth()
{
	if (!this->bHeaderRead)
		_readHeader();

	return this->nCols;
}

/**
 * gets the image height
 * @return the height
 */
int CTiffFile::getHeight()
{
	if (!this->bHeaderRead)
		_readHeader();

	return this->nRows;
}

/**
 * @return the number of bits per pixel
 */
int CTiffFile::getBpp()
{
	if (!this->bHeaderRead)
		_readHeader();

	return this->bpp;
}

/**
 * @return the number of bands (eg RGB = 3)
 */
int CTiffFile::bands()
{
	if (!this->bHeaderRead)
		_readHeader();

	return this->nBands;
}

/**
 * @return returns the size in bytes of a row of data as it would
 *  be  returned  in a call to readLine
 */
int CTiffFile::getScanLineSize()
{
	return ::TIFFScanlineSize(this->tif);
}

/**
 * gets a tile of imagery RGB data is returned BGR0
 *
 * @param r, c the row and column of upper left of tile
 * @return the buffer containing the pixels
 */
unsigned char* CTiffFile::getTile(int r, int c)
{
	if (this->tiles == NULL)
		return false;

	int tileRow = r/this->tileHeight;
	int tileCol = c/this->tileWidth;

	// first check the buffered tiles
	for (int i = 0; i < this->nBufferedTiles; i++)
	{
		CImageTile* imageTile = this->tiles[i];

		// make sure its there
		if (imageTile == NULL)
			continue;

		if ((imageTile->getTileRow() == tileRow) && (imageTile->getTileCol() == tileCol))
		{
			return imageTile->getData();
		}
	}

	// better buffer the tile

	// increment index (and make sure its in range)
	this->lastBufferedTile++;
	if (this->lastBufferedTile >= this->nBufferedTiles)
			this->lastBufferedTile = 0;

	// allocate new one if nessesary
	if (this->tiles[this->lastBufferedTile] == NULL)
		this->tiles[this->lastBufferedTile] = new CImageTile(this->tileWidth, this->tileHeight, this->bpp, this->nBands);

	CImageTile* imageTile = this->tiles[this->lastBufferedTile];

	imageTile->setTileRowCol(tileRow, tileCol);

	// read tile and convert to our format
	unsigned char* buffer = imageTile->getData();
	if (this->nBands == 3)
	{
		// are the byte stored RGBRGB etc... 
		if (this->planarConfig == PLANARCONFIG_CONTIG)
		{
			::TIFFReadTile(this->tif, this->tileBuffer, c, r, 0, 0);
			if ( this->bpp == 8 )
			{
				for (int r = 0; r < (int)this->tileHeight; r++)
				{
					int lineOffsetQ = r*this->tileWidth*4;
					int lineOffsetT = r*this->tileWidth*3;

					for (int c = 0; c < (int)this->tileWidth; c++)
					{
						int q = c*4;
						int t = c*3;

						buffer[lineOffsetQ+q+0] = this->tileBuffer[lineOffsetT+t+2];
						buffer[lineOffsetQ+q+1] = this->tileBuffer[lineOffsetT+t+1];
						buffer[lineOffsetQ+q+2] = this->tileBuffer[lineOffsetT+t+0];
						buffer[lineOffsetQ+q+3] = 0;
					}
				}
			}
			if ( this->bpp == 16 )
			{
				unsigned short* shortbuf = (unsigned short*)this->tileBuffer;
				unsigned short* RGB0 = (unsigned short*)buffer;

				for (unsigned long i = 0; i < (unsigned long)(this->tileWidth*this->tileHeight); i++)
				{
					unsigned long i3 = i*3;
					unsigned long i4 = i*4;
					
					RGB0[i4+0] = shortbuf[i3+2];
					RGB0[i4+1] = shortbuf[i3+1];
					RGB0[i4+2] = shortbuf[i3+0];
					RGB0[i4+3] = 0;
				}
			}
		}
		else // the bytes are stored RRRRRRR......GGGGGGG.......BBBBBB i.e. different planes
		{
			for (int i = 0; i < 3; i++)
			{
				::TIFFReadTile(this->tif, this->tileBuffer, c, r, 0, i);

				int channel;
				if (i == 0)
					channel = 2;
				else if (i == 1)
					channel = 1;
				else
					channel = 0;

				for (int r = 0; r < (int)this->tileHeight; r++)
				{
					int lineOffsetQ = r*this->tileWidth*4;
					int lineOffsetT = r*this->tileWidth;

					for (int c = 0; c < (int)this->tileWidth; c++)
					{
						int q = c*4;

						buffer[lineOffsetQ+q+channel] = this->tileBuffer[lineOffsetT+c];
					}
				}
			}
		}

	}
	else if (this->nBands == 4)
	{
		// are the byte stored RGBNRGBN etc... 
		if (this->planarConfig == PLANARCONFIG_CONTIG)
		{
			::TIFFReadTile(this->tif, this->tileBuffer, c, r, 0, 0);
			if ( this->bpp == 8 )
			{
				for (int r = 0; r < (int)this->tileHeight; r++)
				{
					int lineOffsetQ = r*this->tileWidth*4;
					int lineOffsetT = r*this->tileWidth*4;

					for (int c = 0; c < (int)this->tileWidth; c++)
					{
						int q = c*4;
						int t = c*4;

						buffer[lineOffsetQ+q+0] = this->tileBuffer[lineOffsetT+t+2];
						buffer[lineOffsetQ+q+1] = this->tileBuffer[lineOffsetT+t+1];
						buffer[lineOffsetQ+q+2] = this->tileBuffer[lineOffsetT+t+0];
						buffer[lineOffsetQ+q+3] = this->tileBuffer[lineOffsetT+t+3];
					}
				}
			}
			if ( this->bpp == 16 )
			{
				unsigned short* shortbuf = (unsigned short*)this->tileBuffer;
				unsigned short* RGBN = (unsigned short*)buffer;

				for (unsigned long i = 0; i < (unsigned long)(this->tileWidth*this->tileHeight); i++)
				{
					unsigned long i4 = i*4;
					
					RGBN[i4+0] = shortbuf[i4+0];
					RGBN[i4+1] = shortbuf[i4+1];
					RGBN[i4+2] = shortbuf[i4+2];
					RGBN[i4+3] = shortbuf[i4+3];
				}
			}
		}
		else // the bytes are stored RRRRRRR......GGGGGGG.......BBBBBB i.e. different planes
		{
			for (int i = 0; i < 3; i++)
			{
				::TIFFReadTile(this->tif, this->tileBuffer, c, r, 0, i);

				int channel;
				if (i == 0)
					channel = 2;
				else if (i == 1)
					channel = 1;
				else
					channel = 0;

				for (int r = 0; r < (int)this->tileHeight; r++)
				{
					int lineOffsetQ = r*this->tileWidth*4;
					int lineOffsetT = r*this->tileWidth;

					for (int c = 0; c < (int)this->tileWidth; c++)
					{
						int q = c*4;

						buffer[lineOffsetQ+q+channel] = this->tileBuffer[lineOffsetT+c];
					}
				}
			}
		}
	}
	else if (this->nBands == 1)
	{
		::TIFFReadTile(this->tif, buffer, c, r, 0, 0);
	}

	return imageTile->getData();
}

/**
 * Reads one scan line into buffer.
 *
 * @param buffer the destination buffer
 * @param iRow the row to read
 * @return false for failure
 */
bool CTiffFile::readLine(unsigned char* buffer, int iRow)
{
	if (iRow >= this->nRows)
		return false;

	if (this->scanLineBuffer == NULL)
	{
	//	this->scanLineBuffer = new unsigned char[this->scanLineSize];
		this->scanLineBuffer = ZRasterHelper::alloc(this->scanLineSize);
	}

    if (this->tiled)
    {
		return this->_readTiledLine(buffer, iRow);
		
	}
	else
	{
		// for rgb imagery we want BGR0 BGR0 etc...
		if (this->nBands == 3)
		{
			return this->_read3BandLine(buffer, iRow);
		}
		else if (this->nBands == 4) // for rgbi imagery we want BGRI BGRI etc...
		{
			return this->_read4BandLine(buffer, iRow);
		}
		else if (this->nBands == 1)
		{
//maria
		//	if (true)
		//		return this->_directRead(buffer, iRow);	
		//	else
				TIFFReadScanline(this->tif, buffer, iRow); // 1 band, 8 bit or 16 bit, works 
		}
	}

	return true;
}

bool CTiffFile::_directRead(unsigned char* buffer, int iRow)
{
	if (iRow >= this->nRows)
		return false;

	if (iRow == 0)
	{
		int size = this->getWidth()*this->getHeight();
		this->frameBuffer = ZRasterHelper::alloc(size);//new unsigned char[size];

		unsigned char* buf = this->frameBuffer;
		for (int i = 0; i < this->nRows; i++)
		{
			TIFFReadScanline(this->tif, buf, i);
			//::TIFFReadRawStrip(this->tif, 1, buf, this->nRows*this->nCols);
			buf += this->nCols;
		}

		::TIFFClose(this->tif);
		this->tif = NULL;

	}

	int offset = this->nCols*iRow;

	memcpy(buffer, this->frameBuffer+offset, this->nCols);

	/*
	int* d = (int*)(buffer);
	int* s = (int*)(this->frameBuffer+offset);

	int size = this->nCols*this->getBpp()/8;

	while (size >=4)
	{
		*d++ = *s++;
		size -= 4;
	}
*/
	return true;
}

bool CTiffFile::_read4BandLine(unsigned char* buffer, int iRow)
{
	if (this->planarConfig == 1)
	{
		TIFFReadScanline(this->tif, this->scanLineBuffer, iRow);

		if (this->getBpp() == 8)
		{
			//@todo 4 bands, 8 bit, planarConfig 1, to be tested
			// tested with flower image from tifflib examples
			// and show negative
			for (int c = 0; c < this->nCols; c++)
			{
				int q = c*4;

				buffer[q+0] = this->scanLineBuffer[q+3];
				buffer[q+1] = this->scanLineBuffer[q+2];
				buffer[q+2] = this->scanLineBuffer[q+1];
				buffer[q+3] = this->scanLineBuffer[q+0];
			}
		}
		else if (this->getBpp() == 16)
		{
			// 4 band, 16 bit, planarConfig 1, works
			unsigned short* shortBuffer = (unsigned short*)buffer;
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;

			for (int c = 0; c < this->nCols; c++)
			{
				int q = c*4;

				shortBuffer[q+0] = shortScanLineBuffer[q+3];
				shortBuffer[q+1] = shortScanLineBuffer[q+2];
				shortBuffer[q+2] = shortScanLineBuffer[q+1];
				shortBuffer[q+3] = shortScanLineBuffer[q+0];
			}
		}
	}
	else if ( this->planarConfig == 2)
	{
		if (this->getBpp() == 8)
		{
			// 4 bands, 8 bit, planarConfig 2, works
			for (int i = 0; i < 4; i++)
			{
				int row = i*this->nRows + iRow;
				int ch;

				if (i == 0)
					ch = 3;
				else if (i == 1)
					ch = 2;
				else if (i == 2)
					ch = 1;
				else if (i == 3)
					ch = 0;

				int error = TIFFReadScanline(this->tif, this->scanLineBuffer, iRow, i);

				for (int c = 0; c < this->nCols; c++)
				{
					int q = c*4;
					int t = c;

					buffer[q+ch] = this->scanLineBuffer[t];
				}
			}
		}
		else if (this->getBpp() == 16)
		{
			unsigned short* shortBuffer = (unsigned short*)buffer;
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;

			// 4 bands, 16 bits, planarConfig 2, works
			for (int i = 0; i < 4; i++)
			{
				int row = i*this->nRows + iRow;
				int ch;

				if (i == 0)
					ch = 3;
				else if (i == 1)
					ch = 2;
				else if (i == 2)
					ch = 1;
				else if (i == 3)
					ch = 0;

				int error = TIFFReadScanline(this->tif, shortScanLineBuffer, iRow, i);

				for (int c = 0; c < this->nCols; c++)
				{
					int q = c*4;
					int t = c;

					shortBuffer[q+ch] = shortScanLineBuffer[t];
				}
			}
		}
	}

	return true;
}

bool CTiffFile::_read3BandLine(unsigned char* buffer, int iRow)
{
	if ( this->planarConfig == 1)
	{
		TIFFReadScanline(this->tif, this->scanLineBuffer, iRow);

		if (this->getBpp() == 8)
		{
			// 3 bands, 8 bit, planarConfig 1, works
			for (int c = 0; c < this->nCols; c++)
			{
				int q = c*4;
				int t = c*3;

				buffer[q+0] = this->scanLineBuffer[t+2];
				buffer[q+1] = this->scanLineBuffer[t+1];
				buffer[q+2] = this->scanLineBuffer[t+0];
				buffer[q+3] = 0;
			}
		}
		else if (this->getBpp() == 16)
		{
			unsigned short* shortBuffer = (unsigned short*)buffer;
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;

			// 3 bands, 16 bit, planarConfig 1, works
			for (int c = 0; c < this->nCols; c++)
			{
				int q = c*4;
				int t = c*3;

				shortBuffer[q+0] = shortScanLineBuffer[t+2];
				shortBuffer[q+1] = shortScanLineBuffer[t+1];
				shortBuffer[q+2] = shortScanLineBuffer[t+0];
				shortBuffer[q+3] = 0;
			}
		}
	}
	else if ( this->planarConfig == 2)
	{
		if (this->getBpp() == 8)
		{
			// 3 band, 8 bit, planarConfig 2, works
			for (int i = 0; i < 3; i++)
			{
				int row = i*this->nRows + iRow;
				int ch;

				if (i == 0)
					ch = 2;
				else if (i == 1)
					ch = 1;
				else if (i == 2)
					ch = 0;

				int error = TIFFReadScanline(this->tif, this->scanLineBuffer, iRow, i);

				for (int c = 0; c < this->nCols; c++)
				{
					int q = c*4;
					int t = c;

					buffer[q+ch] = this->scanLineBuffer[t];
				}
			}
		}
		else if (this->getBpp() == 16)
		{
			unsigned short* shortBuffer = (unsigned short*)buffer;
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;

			// 3 bands, 16 bit, planarConfig 2, works
			for (int i = 0; i < 3; i++)
			{
				int row = i*this->nRows + iRow;
				int ch;

				if (i == 0)
					ch = 2;
				else if (i == 1)
					ch = 1;
				else if (i == 2)
					ch = 0;

				int error = TIFFReadScanline(this->tif, shortScanLineBuffer, iRow, i);

				for (int c = 0; c < this->nCols; c++)
				{
					int q = c*4;
					int t = c;

					shortBuffer[q+ch] = shortScanLineBuffer[t];
				}
			}
		}
	}

	return true;
}


bool CTiffFile::_readTiledLine(unsigned char* buffer, int iRow)
{
    int rowInTile = iRow % this->tileHeight;
	
    int rowOffset = 0;
	
	if (this->nBands == 3)
	{
		if ( this->bpp == 8 )
		{
			rowOffset = rowInTile*this->tileWidth*4;
		}
		if ( this->bpp == 16 )
		{
			rowOffset = rowInTile*this->tileWidth*8;
		}
	}
	else if (this->nBands == 4)
	{
		if ( this->bpp == 8 )
		{
			rowOffset = rowInTile*this->tileWidth*4;
		}
		if ( this->bpp == 16 )
		{
			rowOffset = rowInTile*this->tileWidth*8;
		}
	}
	else
	{
        rowOffset = rowInTile*this->tileWidth*this->bpp/8;
	}

	for (int c = 0; c < this->nCols; c+= this->tileWidth)
	{
		unsigned char* src = this->getTile(iRow, c); // get the tile that includes this r, c
		
		if (this->nBands == 3)
		{
			if ( this->bpp == 8 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (uint32)this->nCols)
				{
					memcpy(buffer+4*c, src+rowOffset, (this->nCols-c)*4);
				}
				else
				{
					memcpy(buffer+4*c, src+rowOffset, this->tileWidth*4);
				}
			}
			if ( this->bpp == 16 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (uint32)this->nCols)
				{
					memcpy(buffer+8*c, src+rowOffset, (this->nCols-c)*8);
				}
				else
				{
					memcpy(buffer+8*c, src+rowOffset, this->tileWidth*8);
				}
			}
		}
		else if (this->nBands == 4)
		{
			if ( this->bpp == 8 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (uint32)this->nCols)
				{
					memcpy(buffer+4*c, src+rowOffset, (this->nCols-c)*4);
				}
				else
				{
					memcpy(buffer+4*c, src+rowOffset, this->tileWidth*4);
				}
			}
			if ( this->bpp == 16 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (uint32)this->nCols)
				{
					memcpy(buffer+8*c, src+rowOffset, (this->nCols-c)*8);
				}
				else
				{
					memcpy(buffer+8*c, src+rowOffset, this->tileWidth*8);
				}
			}
		}
		else
		{
			// the last tile in a row extends beyond the image
			if (c+this->tileWidth > (uint32)this->nCols)
			{
				memcpy(buffer+c*this->bpp/8, src+rowOffset, (this->nCols-c)*this->bpp/8);
			}
			else
			{
				memcpy(buffer+c*this->bpp/8, src+rowOffset, this->tileWidth*this->bpp/8);
			}
		}
	}

	return true;
}


/** 
 * Write the image Stripwise, make sure that setTiledWrite() is set
 *
 * @param unsigned char* pBuffer buffer of an image row i
 * @param int iRow image row i 
 * @return true for success / false for failure
 */
bool CTiffFile::writeLine(unsigned char* pBuffer, int iRow)
{
	bool ret = true;

	if (iRow == 0) // Write the header etc.
	{
		_writeHeader();

	}
	
	int stripSize = this->nBands * this->nCols * this->bpp / 8;

	unsigned char* stripBuffer = new unsigned char[stripSize];
	// If there is only one channel do it the easy way
	if (this->nBands == 1)
	{
		TIFFWriteRawStrip(this->tif, iRow, pBuffer, stripSize);
	}
	else if (this->nBands == 2)
	{
		ret = false; 
	}
	else
	{
		// compute the memory offset for iRow in the Strip buffer
		int size = this->bpp / 8; // size in bytes of a pixel

		int li_skip = this->stripOffset * (this->bpp / 8); // Pixel interleaved skip
		int pi_index = 0;
		int li_index = 0;

		for (int c = 0; c < this->nCols; c++)
		{
			for (int i = 0; i < this->nBands; i++)
			{
				memcpy(stripBuffer + pi_index, pBuffer + li_index, size);
				li_index += size;
				pi_index += size;
			}
			li_index += li_skip;
		}

		TIFFWriteRawStrip(this->tif, iRow, stripBuffer, stripSize);
	}

	delete [] stripBuffer;
	
	return ret;
}

void CTiffFile::setTiledWrite(bool tiled)
{
	this->tiledWrite = tiled;
}

void CTiffFile::setUnTiledWrite()
{
	this->tiledWrite = false;
}
