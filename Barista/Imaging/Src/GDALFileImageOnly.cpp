/** 
 * @class CGDALFileImageOnly
 * concrete GDALFile image file class
 * a wrapper around the gdal library
 * 
 * @author Franz Rottensteiner
 * @version 1.0 
 * @date 11-june-2009
 */

#include "gdal_priv.h"
#include <ogr_spatialref.h>

#include "GDALFileImageOnly.h"
#include "ImageTile.h"
#include "LookupTable.h"
#include "ImgBuf.h"
#include "SystemUtilities.h"
#include "histogram.h"


// define error handler, so we always initialize the buffer with 0 in case of an error
#ifndef READ_ERROR
#define READ_ERROR(buffer,sizeOfBuffer) {memset(buffer, 0, sizeOfBuffer);\
					return false;}
#endif



CGDALFileImageOnly::CGDALFileImageOnly() : tiled(false), bHeaderRead(false), 
                        linedata(NULL),bline(NULL),rline(NULL),gline(NULL),
						nline(NULL),scanLineBuffer(NULL),gdal(NULL),tiles(NULL),
						tileByteSize(0),nBufferedTiles(0),tileBuffer(NULL), colorIndexed(false),
						lastBufferedTile(-1),sizeofpixel(0),NoData1(DEM_NOHEIGHT),NoData2(DEM_NOHEIGHT),
						NoData3(DEM_NOHEIGHT),NoData4(DEM_NOHEIGHT),bandInterleave(false),
						lut(0),colorChannel1(0),colorChannel2(1),colorChannel3(2),colorChannel4(3),hasColourInformations(false)
{
}


CGDALFileImageOnly::CGDALFileImageOnly(const char* filename) : bHeaderRead(false), linedata(NULL), 
                    tiled(false), bline(NULL),rline(NULL),gline(NULL), nline(NULL),
					scanLineBuffer(NULL),gdal(NULL), tiles(NULL), tileByteSize(0), 
					nBufferedTiles(0),tileBuffer(NULL),colorIndexed(false),
					lastBufferedTile(-1),sizeofpixel(0),NoData1(DEM_NOHEIGHT),NoData2(DEM_NOHEIGHT),
					NoData3(DEM_NOHEIGHT),NoData4(DEM_NOHEIGHT),bandInterleave(false), lut(0),
					colorChannel1(0),colorChannel2(1),colorChannel3(2),colorChannel4(3),
					hasColourInformations(false)
{
}

CGDALFileImageOnly::CGDALFileImageOnly(const CGDALFileImageOnly& src) : CBaseImage(src),
					bHeaderRead(src.bHeaderRead),linedata(NULL),bline(NULL),rline(NULL),gline(NULL),
					nline(NULL), scanLineBuffer(NULL), tiled(src.tiled), gdal(NULL),colorIndexed(src.colorIndexed),
					tiles(NULL), tileByteSize(0), nBufferedTiles(0),tileBuffer(NULL),
					lastBufferedTile(-1),sizeofpixel(src.sizeofpixel),NoData1(src.NoData1),NoData2(src.NoData2),
					NoData3(src.NoData3),NoData4(src.NoData4),channelExport(src.channelExport),
					compressionExport(src.compressionExport),compressionQualityExport(src.compressionQualityExport),
					exportGeoData(src.exportGeoData),exportSeperatFile(src.exportSeperatFile),fileNameGeoData(src.fileNameGeoData),
					bandInterleave(src.bandInterleave), lut(0),
					colorChannel1(src.colorChannel1),colorChannel2(src.colorChannel2),colorChannel3(src.colorChannel3),
					colorChannel4(src.colorChannel4),hasColourInformations(src.hasColourInformations)
{
	if (src.lut) this->lut = new CLookupTable(src.lut);

	if (this->bHeaderRead)
	{
		// create the tile buffer
		this->nBufferedTiles = this->tilesAcross;

		if (!this->tiles) 
		{
			this->tiles = new CImageTile*[this->nBufferedTiles];
			for (int i = 0; i < this->tilesAcross; i++)
			{
				this->tiles[i] = NULL;
			}
		}
		else
		{
			for (int i = 0; i < this->tilesAcross; i++)
    		{
				if (this->tiles[i]) delete this->tiles[i];
				this->tiles[i] = NULL;
			}
		}
	}


}

CGDALFileImageOnly::~CGDALFileImageOnly()
{
	if (this->lut) delete this->lut;
	this->lut = 0;

    if(this->scanLineBuffer != NULL)
	{
        delete [] this->scanLineBuffer;
		this->scanLineBuffer = NULL;
	}
    if(this->linedata != NULL)
	{
        delete [] this->linedata;
		this->linedata = NULL;
	}
    if(this->rline != NULL)
	{
        delete [] this->rline;
		this->rline = NULL;
	}
    if(this->bline != NULL)
	{
        delete [] this->bline;
		this->bline = NULL;
	}
    if(this->gline != NULL)
	{
        delete [] this->gline;
		this->gline = NULL;
	}
    if(this->nline != NULL)
	{
        delete [] this->nline;
		this->nline = NULL;
	}

	this->closeFile();
	
	if (this->tiles)
	{
			
		for (int i = 0; i < this->tilesAcross; i++)
		{
			if (this->tiles[i] != NULL)
				delete this->tiles[i];
		}

		delete [] this->tiles;
		this->tiles = 0;
	}

    if(this->tileBuffer != NULL)
	{
        delete [] this->tileBuffer;
		this->tileBuffer = 0;
	}	

}

int CGDALFileImageOnly::pixelSizeBytes()
{
    int pixS;

    if (this->bands == 3)
        pixS = 4*this->bpp/8;
    else if (this->bands == 4)
        pixS = 4*this->bpp/8;
    else
        pixS = this->bands * this->bpp/8;

    return pixS;
}

/**
 * @return the Bits per pixel
 */
int CGDALFileImageOnly::getBpp() const
{
 return this->bpp;
}

/**
 * @return the number of bands (i.e RGB = 3)
 */
int CGDALFileImageOnly::getBands() const
{
 return this->bands;
}

/**
 * @return the image width
 */
int CGDALFileImageOnly::getWidth() const
{
 return this->width;
}

/**
 * @return the image height
 */
int CGDALFileImageOnly::getHeight() const
{
 return this->height;
}

//********************************************************************************
//********************************************************************************
// dummy implementations of otherwise purely virtual functions

bool CGDALFileImageOnly::create(CImageFile* imageFile)
{
	return true;
};

bool CGDALFileImageOnly::getRect(unsigned char* buf, int top, int height, int left, int width)
{
	return true;
};
    
bool CGDALFileImageOnly::getRect(unsigned char* buf, int top, int height, int left, int width, int destHeight, int destWidth)
{
	return true;
};

void CGDALFileImageOnly::setTile(unsigned char* buf, int r, int c)
{
};
    
bool CGDALFileImageOnly::getImageTile(CImageTile** imageTile, int r, int c)
{
	return true;
};

bool CGDALFileImageOnly::getTileZoomUp(CImageBuffer& dest, int r, int c, int zoomUp)
{
	return true;
};
    
bool CGDALFileImageOnly::getTileZoomDown(CImageBuffer &dest,int r, int c, int zoomDown)
{
	return true;
};

//********************************************************************************
//********************************************************************************

bool CGDALFileImageOnly::closeFile()
{
    if(this->gdal != NULL)
	{
		GDALClose(gdal);
		this->gdal = NULL;
	}

	return true;
}

/**
 *  writes a tiff file from a CBaseImage
 *
 * @param fileName the output file name
 * @param image the src image
 *
 */
bool CGDALFileImageOnly::write(const char* filename, CBaseImage* image)
{

	if (filename == NULL || image == NULL)
		return false;

	this->FileName =  filename;	
	
	CFileName fn(this->FileName);
	char **options = NULL;		// options to create the file


	// no filename set
	if (fn.CompareNoCase("unknown"))
		return false;

	// the basic informations are not set
	if (this->width == -1 || this->height == -1 || this->bands == -1 || this->bpp == -1)
		return false;

	if (this->bpp == 8) this->sizeofpixel = GDT_Byte;
	else if (this->bpp == 16) this->sizeofpixel = GDT_UInt16;
	else if (this->bpp == 32) this->sizeofpixel =  GDT_Float32;

	CCharString driverName;
	CFileName ext(fn.GetFileExt());	
	bool tiffIsTemp;


	// get the driver naame and set options for every format
	if (ext.CompareNoCase("tif"))
	{
		tiffIsTemp= false;
		if (this->tiled)
		{
			options = CSLSetNameValue( options, "TILED", "YES" );
			options = CSLSetNameValue( options, "BLOCKXSIZE", "512" );
			options = CSLSetNameValue( options, "BLOCKYSIZE", "512" );

		}

		
		long sizeOfImage = this->height * this->width * this->bands * this->sizeofpixel;
		
		if (sizeOfImage > 4000000000) // actually 2^32-1 but we also need space for the header
			options = CSLSetNameValue( options, "BIGTIFF", "YES" );
		
		options = CSLSetNameValue( options, "COMPRESS", this->compressionExport.GetChar() );
		options = CSLSetNameValue( options, "PROFILE", "GDALGeoTIFF" );


		if (this->compressionExport.CompareNoCase("Jpeg"))
		{
			char quality[4];
			sprintf(quality,"%d",this->compressionQualityExport);
			options = CSLSetNameValue( options, "JPEG_QUALITY", quality);
		}
		
		//PHOTOMETRIC=[MINISBLACK/MINISWHITE/RGB/CMYK/YCBCR/CIELAB/ICCLAB/ITULAB]
		if ( this->bands > 1 )
		{
			options = CSLSetNameValue( options, "PHOTOMETRIC", "RGB" );

			if (this->bandInterleave)
				options = CSLSetNameValue( options, "INTERLEAVE", "BAND" );
			else
				options = CSLSetNameValue( options, "INTERLEAVE", "PIXEL" );
		}
		else
			options = CSLSetNameValue( options, "PHOTOMETRIC", "MINISBLACK" );




	}
	
	else if (ext.CompareNoCase("jpg"))
	{
		driverName = "JPEG";
		
		char quality[4];
		sprintf(quality,"%d",this->compressionQualityExport);
		
		options = CSLSetNameValue( options,"QUALITY", quality);
		tiffIsTemp= true;
	}
	else if (ext.CompareNoCase("jp2"))
	{
		driverName = "JP2ECW";
		//options = CSLSetNameValue( options, "WORLDFILE", "ON");
		tiffIsTemp= true;
	}
	else if (ext.CompareNoCase("ecw"))
	{
		driverName = "ECW";
		//options = CSLSetNameValue( options, "WORLDFILE", "ON");
		tiffIsTemp= true;
	}
	else	// no match with known driver
		return false;

	GDALAllRegister();

	// load driver
	GDALDriver *driverTIFF;
	GDALDataset *dataSetTIFF; 

	// get the tiff driver
	driverTIFF = GetGDALDriverManager()->GetDriverByName("GTIFF");
	CFileName tiffFileName;

	// change the file name when writing other formats then TIFF
	if (tiffIsTemp) tiffFileName = fn.GetPath() + CSystemUtilities::dirDelimStr + fn.GetFileName() + "_temp.tif";
	else
		tiffFileName = filename;

	// create the actual dataset
	dataSetTIFF = driverTIFF->Create(tiffFileName.GetChar(),this->width,this->height,this->bands,(GDALDataType)this->sizeofpixel,options );

	// add colortable if available
	if ( this->colorIndexed )
	{
		GDALColorTable gdallut = GDALColorTable(GPI_RGB);

		int entrycount = this->lut->getMaxEntries();
		int rval, bval, gval;
		GDALColorEntry gdalentry;

		for ( int i =0; i < entrycount; i++)
		{
			this->lut->getColor(rval, bval, gval, i);
			gdalentry.c1 = rval;
			gdalentry.c2 = bval;
			gdalentry.c3 = gval;

			gdallut.SetColorEntry(i, &gdalentry); 
		}

		dataSetTIFF->GetRasterBand(1)->SetColorTable(&gdallut);
	}


	// we have to write 2 files, so the progress bar goes only till 50%
	if (tiffIsTemp)
		this->listenerMaxValue = 50;
	else
		this->listenerMaxValue = 100;

	// let's write the data
	if (!this->writeData(image,dataSetTIFF,this->tiled))
	{
		GDALClose( dataSetTIFF );
		return false;
	}
	
	// and geo data
	if ( !tiffIsTemp && this->exportGeoData)
	{
		this->writeGeoData(image,dataSetTIFF);	
	}


	
		
	// close the file
	GDALClose( dataSetTIFF );


	// finished?
	if (tiffIsTemp == false)
		return true;

	// ok, let's reopen the file and copy it to the actual format
	dataSetTIFF = (GDALDataset *) GDALOpen( tiffFileName.GetChar(), GA_ReadOnly );

	GDALDriver *driver;
	GDALDataset *dataSet; 
	
	driver = GetGDALDriverManager()->GetDriverByName(driverName.GetChar());
	dataSet = driver->CreateCopy(this->FileName,dataSetTIFF,FALSE,NULL,NULL,NULL);

	bool ret = dataSet ? true : false;
	if (this->listener != NULL)
	{
		this->listener->setProgressValue(98);
	}
	
	// and geo data
	if (this->exportGeoData)
	{
		this->writeGeoData(image,dataSetTIFF);
	}


	GDALClose (dataSetTIFF);
	GDALClose( dataSet);

	// delete the temp tiff file
	remove(tiffFileName);

	if (this->listener != NULL)
	{
		this->listener->setProgressValue(100);
	}

	return ret;
}


/**
 * private function that reads the header
 *
 */
bool CGDALFileImageOnly::readHeader()
{
	if ( this->gdal == NULL)
	{
		GDALAllRegister();
		this->gdal = (GDALDataset *) GDALOpen( this->FileName.GetChar(), GA_ReadOnly );
		if ( this->gdal == NULL)
			return false;
	}

	int nb = this->gdal->GetRasterCount();

	this->bands = nb;


	/// check if bands are labeled/in RGB order or else if four channels
	if (!this->hasColourInformations)
	{

		if( nb == 1)
		{
			this->colorChannel1 = 0;
			this->NoData1 = (float)this->gdal->GetRasterBand(1)->GetNoDataValue();
		}
		else if( nb == 3 )
		{
			this->NoData1 = (float)this->gdal->GetRasterBand(1)->GetNoDataValue();
			this->NoData2 = (float)this->gdal->GetRasterBand(2)->GetNoDataValue();
			this->NoData3 = (float)this->gdal->GetRasterBand(3)->GetNoDataValue();

			GDALColorInterp color1 = this->gdal->GetRasterBand(1)->GetColorInterpretation();;
			GDALColorInterp color2 = this->gdal->GetRasterBand(2)->GetColorInterpretation();
			GDALColorInterp color3 = this->gdal->GetRasterBand(3)->GetColorInterpretation();

			// select color for channel 1
			if (color1 == GCI_RedBand)
				this->colorChannel1 = 0;
			else if (color1 == GCI_GreenBand)
				this->colorChannel1 = 1;
			else if (color1 == GCI_BlueBand)
				this->colorChannel1 = 2;
			else
				this->colorChannel1 = 0;
			
			// select color for channel 2
			if (color2 == GCI_RedBand)
				this->colorChannel2 = 0;
			else if (color2 == GCI_GreenBand)
				this->colorChannel2 = 1;
			else if (color2 == GCI_BlueBand)
				this->colorChannel2 = 2;
			else
				this->colorChannel2 = 1;

			// select color for channel 3
			if (color3 == GCI_RedBand)
				this->colorChannel3 = 0;
			else if (color3 == GCI_GreenBand)
				this->colorChannel3 = 1;
			else if (color3 == GCI_BlueBand)
				this->colorChannel3 = 2;
			else
				this->colorChannel3 = 2;
		
			int hmm=9;

		}
		if( nb == 4 )
		{
			this->NoData1 = (float)this->gdal->GetRasterBand(1)->GetNoDataValue();
			this->NoData2 = (float)this->gdal->GetRasterBand(2)->GetNoDataValue();
			this->NoData3 = (float)this->gdal->GetRasterBand(3)->GetNoDataValue();
			this->NoData4 = (float)this->gdal->GetRasterBand(4)->GetNoDataValue();

			GDALColorInterp color1 = this->gdal->GetRasterBand(1)->GetColorInterpretation();;
			GDALColorInterp color2 = this->gdal->GetRasterBand(2)->GetColorInterpretation();
			GDALColorInterp color3 = this->gdal->GetRasterBand(3)->GetColorInterpretation();
			GDALColorInterp color4 = this->gdal->GetRasterBand(4)->GetColorInterpretation();

			// default 
			this->colorChannel1 = 0;
			this->colorChannel2 = 1;
			this->colorChannel3 = 2;
			this->colorChannel4 = 3;


			bool hasRed = true;
			bool hasGreen = true;
			bool hasBlue = true;
			bool hasAlpha = true;

			// select red channel
			if ( GCI_RedBand == color1)
				this->colorChannel1 = 0;
			else if (GCI_RedBand == color2)
				this->colorChannel2 = 0;
			else if (GCI_RedBand == color3)
				this->colorChannel3 = 0;
			else if (GCI_RedBand == color4)
				this->colorChannel4 = 0;
			else
				hasRed = false;

			
			// select green channel
			if ( GCI_GreenBand == color1)
				this->colorChannel1 = 1;
			else if (GCI_GreenBand == color2)
				this->colorChannel2 = 1;
			else if (GCI_GreenBand == color3)
				this->colorChannel3 = 1;
			else if (GCI_GreenBand== color4)
				this->colorChannel4 = 1;
			else
				hasGreen = false;

			// select blue channel
			if ( GCI_BlueBand == color1)
				this->colorChannel1 = 2;
			else if (GCI_BlueBand == color2)
				this->colorChannel2 = 2;
			else if (GCI_BlueBand == color3)
				this->colorChannel3 = 2;
			else if (GCI_BlueBand== color4)
				this->colorChannel4 = 2;
			else
				hasBlue = false;

			// select Alpha / NIR channel
			if ( GCI_AlphaBand == color1 || GCI_GrayIndex == color1)
				this->colorChannel1 = 3;
			else if (GCI_AlphaBand == color2 || GCI_GrayIndex == color2)
				this->colorChannel2 = 3;
			else if (GCI_AlphaBand== color3 || GCI_GrayIndex == color3)
				this->colorChannel3 = 3;
			else if (GCI_AlphaBand== color4 || GCI_GrayIndex == color4)
				this->colorChannel4 = 3;
			else
				hasAlpha = false;

			// no color information 
			if (!hasRed && !hasGreen && !hasBlue && hasAlpha )
			{
				if (this->colorChannel1 == 3)
				{
					this->colorChannel2 = 0;
					this->colorChannel3 = 1;
					this->colorChannel4 = 2;
				}
				else
				{
					this->colorChannel1 = 0;
					this->colorChannel2 = 1;
					this->colorChannel3 = 2;
				}

			}

			if (!hasAlpha && hasRed && hasGreen && hasBlue)
			{
				if (this->colorChannel1 == 0)
					this->colorChannel4 = 3;
				else
					this->colorChannel1 = 3;
			}

		}
	}

	this->height = this->gdal->GetRasterYSize();
	this->width  = this->gdal->GetRasterXSize();

	// check if 8bit or 16bit
	GDALDataType gdaldepth = this->gdal->GetRasterBand(1)->GetRasterDataType();

	if ( gdaldepth == GDT_Byte)
	{
		this->bpp = 8;
		this->sizeofpixel = 1;
	}
	else if ( gdaldepth == GDT_UInt16)
	{
		this->bpp = 16;
		this->sizeofpixel = 2;
	}
	else if ( gdaldepth == GDT_Int16)
	{
		this->bpp = 16;
		this->sizeofpixel = 2;
	}
	else if ( gdaldepth == GDT_Float64)
	{
		this->bpp = 64;
		this->sizeofpixel = 8;
	}
	else if ( gdaldepth == GDT_Float32)
	{
		this->bpp = 32;
		this->sizeofpixel= 4;
	}
	else if (gdaldepth == GDT_Int32)
	{
		this->bpp = 32;
		this->sizeofpixel= 4;
	}
	else
	{
		
	}

	int xblocksize;
	int yblocksize;
	gdal->GetRasterBand(1)->GetBlockSize(&xblocksize , &yblocksize);

	if ( xblocksize != this->width || yblocksize != this->height)
	{
		this->tiled = true;
		this->tileWidth = xblocksize;
		this->tileHeight = yblocksize;

		this->tilesAcross = (this->width + this->tileWidth - 1) / this->tileWidth;
		this->tilesDown = (this->height + this->tileHeight - 1) / this->tileHeight;

		// need to buffer at least tilesAcross tiles
		this->nBufferedTiles = this->tilesAcross;

		if (!this->tiles) 
		{
			this->tiles = new CImageTile*[this->nBufferedTiles];
			for (int i = 0; i < this->tilesAcross; i++)
			{
				this->tiles[i] = NULL;
			}
		}
		else
		{
			for (int i = 0; i < this->tilesAcross; i++)
	    	{
				if (this->tiles[i]) delete this->tiles[i];
				this->tiles[i] = NULL;
		    }
		}

		int size = 0;
		//size = xblocksize*yblocksize;
		size = xblocksize*yblocksize*this->sizeofpixel;
		this->tileBuffer = new unsigned char[size];

		
	}
	
	// check if it is a palette index
	GDALColorInterp checkit = this->gdal->GetRasterBand(1)->GetColorInterpretation();
	if ( checkit == GCI_PaletteIndex) 
	{
		this->colorIndexed = true;
	}

	if ( this->colorIndexed ) this->readLookupTableInfo();

	this->bHeaderRead = true;

	return true;
}

/**
 * Reads one scan line into buffer.
 *
 * @param buffer the destination buffer
 * @param iRow the row to read
 * @return false for failure
 */
bool CGDALFileImageOnly::readLine(unsigned char* buffer, int iRow)
{
	if (iRow >= this->height)
		return false;

	if (this->gdal == NULL )
	{
		GDALAllRegister();
		this->gdal = (GDALDataset *) GDALOpen( this->FileName.GetChar(), GA_ReadOnly );

	}

	if ( this->linedata == NULL)
	{
		this->linedata = (unsigned char*) new int[this->width*this->bands];
	}

	if ( this->bands > 1 )
	{
		if ( this->rline == NULL)
		{
			this->rline = (unsigned char*) new int[this->width];
		}
		if ( this->gline == NULL)
		{
			this->gline = (unsigned char*) new int[this->width];
		}
		if ( this->bline == NULL)
		{
			this->bline = (unsigned char*) new int[this->width];
		}
		if ( this->nline == NULL && this->bands == 4)
		{
			this->nline = (unsigned char*) new int[this->width];
		}
	}

	if ( this->tiled )
	{
		return this->readTiledLine(buffer, iRow);
	}
	else
	{
		if (this->bands == 3)
		{
			return this->read3BandLine(buffer, iRow);
		}
		else if (this->bands == 4)
		{
			return this->read4BandLine(buffer, iRow);
		}
		else if (this->bands == 1)
		{
			return this->read1BandLine(buffer, iRow);
		}
	}

	return true;
}
    
bool CGDALFileImageOnly::getRect(CImageBuffer &buffer)
{
	bool OK = true;

	if (this->gdal == NULL )
	{
		GDALAllRegister();
		this->gdal = (GDALDataset *) GDALOpen( this->FileName.GetChar(), GA_ReadOnly );

	}

	if (!this->gdal) OK = false;
	else
	{
		int llcx = buffer.getOffsetX();
		int llcy = buffer.getOffsetY();
		int w = buffer.getWidth();
		int h = buffer.getHeight();
		int c = buffer.getBands();
		int b = buffer.getBpp();

		if ((c != this->bands) || (b != this->getBpp()) || (llcx < 0) || (llcy < 0) ||
			(w < 0) || (h < 0) || (llcx + w > this->getWidth()) || 
			(llcy + h > this->getHeight())) OK = false;
		else
		{
			unsigned char *pData;
			GDALDataType bufType;

			int nPixelSpace = buffer.getDiffX();
			int nLineSpace  = buffer.getDiffY();  
			int nBandSpace  = 1;

			if (b <= 8) 
			{
				pData   = buffer.getPixUC();
				bufType = GDT_Byte;
				nBandSpace = sizeof(unsigned char);
			}
			else if (b == 16)
			{
				if (buffer.getUnsignedFlag()) 
				{
					pData   = (unsigned char *) buffer.getPixUS();
					bufType = GDT_UInt16;
				}
				else 
				{
					pData   = (unsigned char *) buffer.getPixS();
					bufType = GDT_Int16;
				}

				nBandSpace = sizeof(unsigned short);
			}
			else 
			{
				pData   = (unsigned char *) buffer.getPixF();
				bufType = GDT_Float32;
				nBandSpace = sizeof(float);
			}

			if (!pData) OK = false;
			else
			{
				nPixelSpace *= nBandSpace;
				nLineSpace  *= nBandSpace;  

				if (this->gdal->RasterIO(GF_Read, llcx,  llcy, w, h, pData, w, h, 
					bufType, c, NULL,  nPixelSpace,  nLineSpace,  nBandSpace) == CE_Failure) OK = false;
			}
		}

	}


	return OK;
};

bool CGDALFileImageOnly::read1BandLine(unsigned char* buffer, int iRow,unsigned int numberOfRowsToRead)
{
	GDALColorInterp checkit = gdal->GetRasterBand(1)->GetColorInterpretation();

	GDALDataType depth = gdal->GetRasterBand(1)->GetRasterDataType();

	unsigned int outputSize =  this->width*this->sizeofpixel*numberOfRowsToRead;

	if (this->scanLineBuffer == NULL)
	{
		this->scanLineBuffer = (unsigned char*) new int[this->width * numberOfRowsToRead];
	}

	if ( this->colorIndexed)
	{
		GDALRasterBand* pBand = this->gdal->GetRasterBand(1);
		pBand->RasterIO(GF_Read,0,iRow,this->width,numberOfRowsToRead,this->scanLineBuffer,this->width,numberOfRowsToRead,depth,0,0);
		
		if (this->bpp == 8)
		{
			memcpy(buffer, this->scanLineBuffer, outputSize);
		}
		else if (this->bpp == 16)
		{
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;
			memcpy(buffer, shortScanLineBuffer,outputSize );
		}
	}
	if ( checkit == GCI_GrayIndex )
	{
		GDALRasterBand* pBand = this->gdal->GetRasterBand(1);
		pBand->RasterIO(GF_Read,0,iRow,this->width,numberOfRowsToRead,this->scanLineBuffer,this->width,numberOfRowsToRead,depth,0,0);
		
		if (this->getBpp() == 8)
		{
			memcpy(buffer, this->scanLineBuffer, outputSize);
		}
		else if (this->getBpp() == 16)
		{
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;
			memcpy(buffer, shortScanLineBuffer, outputSize);
		}
	}
	// for float import of DEM data
	else if ( checkit == GCI_Undefined)
	{
		GDALRasterBand* pBand = this->gdal->GetRasterBand(1);
		pBand->RasterIO(GF_Read,0,iRow,this->width,numberOfRowsToRead,this->scanLineBuffer,this->width,numberOfRowsToRead,depth,0,0);
		
		if (this->getBpp() == 8)
		{
			memcpy(buffer, this->scanLineBuffer, outputSize);
		}
		if (this->getBpp() == 16)
		{
			unsigned short* shortScanLineBuffer = (unsigned short*)this->scanLineBuffer;
			memcpy(buffer, shortScanLineBuffer, outputSize);
		}
		if (this->getBpp() == 32)
		{
			float* floatLineBuffer = (float*)this->scanLineBuffer;
			memcpy(buffer, floatLineBuffer, this->width*4*numberOfRowsToRead);
		}
	}

	return true;	
}




bool CGDALFileImageOnly::read3BandLine(unsigned char* buffer, int iRow)
{
	GDALDataType depth = gdal->GetRasterBand(1)->GetRasterDataType();
	
	//channel 1
	if (this->colorChannel1 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->width,1,this->rline,this->width,1,depth,0,0);
	else if (this->colorChannel1 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->width,1,this->rline,this->width,1,depth,0,0);
	else if (this->colorChannel1 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->width,1,this->rline,this->width,1,depth,0,0);
	else
		return false;

	// channel 2
	if (this->colorChannel2 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->width,1,this->gline,this->width,1,depth,0,0);
	else if (this->colorChannel2 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->width,1,this->gline,this->width,1,depth,0,0);
	else if (this->colorChannel2 == 2) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->width,1,this->gline,this->width,1,depth,0,0);
	else
		return false;

	// channel 3
	if (this->colorChannel3 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->width,1,this->bline,this->width,1,depth,0,0);
	else if (this->colorChannel3 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->width,1,this->bline,this->width,1,depth,0,0);
	else if (this->colorChannel3 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->width,1,this->bline,this->width,1,depth,0,0);
	else
		return false;


	if (this->getBpp() == 8)
	{
		for (int c = 0; c < this->width; c++)
		{
			this->linedata[4*c]   = this->rline[c];
			this->linedata[4*c+1] = this->gline[c];
			this->linedata[4*c+2] = this->bline[c];
			this->linedata[4*c+3] = 0;
		}
		
		memcpy(buffer, this->linedata, this->width*4*this->sizeofpixel);
	}
	else if (this->getBpp() == 16)
	{
		unsigned short* rBuffer = (unsigned short*)this->rline;
		unsigned short* gBuffer = (unsigned short*)this->gline;
		unsigned short* bBuffer = (unsigned short*)this->bline;

		unsigned short* shortScanLineBuffer = (unsigned short*)this->linedata;

		for (int c = 0; c < this->width; c++)
		{
			shortScanLineBuffer[4*c]   = rBuffer[c];
			shortScanLineBuffer[4*c+1] = gBuffer[c];
			shortScanLineBuffer[4*c+2] = bBuffer[c];
			shortScanLineBuffer[4*c+3] = 0;
		}
		
		memcpy(buffer, shortScanLineBuffer, this->width*4*this->sizeofpixel);
	}

	return true;	
}

bool CGDALFileImageOnly::read4BandLine(unsigned char* buffer, int iRow)
{
	GDALDataType depth = gdal->GetRasterBand(1)->GetRasterDataType();

	// channel 1
	if (this->colorChannel1 == 0)
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->width,1,this->rline,this->width,1,depth,0,0);
	else if (this->colorChannel1 == 1) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->width,1,this->rline,this->width,1,depth,0,0);
	else if (this->colorChannel1 == 2) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->width,1,this->rline,this->width,1,depth,0,0);
	else if (this->colorChannel1 == 3) 
		this->gdal->GetRasterBand(4)->RasterIO(GF_Read,0,iRow,this->width,1,this->rline,this->width,1,depth,0,0);
	else
		return false;

	// channel 2
	if (this->colorChannel2 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->width,1,this->gline,this->width,1,depth,0,0);
	else if (this->colorChannel2 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->width,1,this->gline,this->width,1,depth,0,0);
	else if (this->colorChannel2 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->width,1,this->gline,this->width,1,depth,0,0);
	else if (this->colorChannel2 == 3 ) 
		this->gdal->GetRasterBand(4)->RasterIO(GF_Read,0,iRow,this->width,1,this->gline,this->width,1,depth,0,0);
	else
		return false;

	// channel 3
	if (this->colorChannel3 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->width,1,this->bline,this->width,1,depth,0,0);
	else if (this->colorChannel3 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->width,1,this->bline,this->width,1,depth,0,0);
	else if (this->colorChannel3 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->width,1,this->bline,this->width,1,depth,0,0);
	else if (this->colorChannel3 == 3 ) 
		this->gdal->GetRasterBand(4)->RasterIO(GF_Read,0,iRow,this->width,1,this->bline,this->width,1,depth,0,0);
	else
		return false;


	// channel 4
	if (this->colorChannel4 == 0 )
		this->gdal->GetRasterBand(1)->RasterIO(GF_Read,0,iRow,this->width,1,this->nline,this->width,1,depth,0,0);
	else if (this->colorChannel4 == 1 ) 
		this->gdal->GetRasterBand(2)->RasterIO(GF_Read,0,iRow,this->width,1,this->nline,this->width,1,depth,0,0);
	else if (this->colorChannel4 == 2 ) 
		this->gdal->GetRasterBand(3)->RasterIO(GF_Read,0,iRow,this->width,1,this->nline,this->width,1,depth,0,0);
	else if (this->colorChannel4 == 3 ) 
		this->gdal->GetRasterBand(4)->RasterIO(GF_Read,0,iRow,this->width,1,this->nline,this->width,1,depth,0,0);
	else
		return false;


	if (this->getBpp() == 8)
	{
		for (int c = 0; c < this->width; c++)
		{
			this->linedata[4*c]   = this->rline[c];
			this->linedata[4*c+1] = this->gline[c];
			this->linedata[4*c+2] = this->bline[c];
			this->linedata[4*c+3] = this->nline[c];
		}
		
		memcpy(buffer, this->linedata, this->width*4*this->sizeofpixel);
	}
	else if (this->getBpp() == 16)
	{
		unsigned short* rBuffer = (unsigned short*)this->rline;
		unsigned short* gBuffer = (unsigned short*)this->gline;
		unsigned short* bBuffer = (unsigned short*)this->bline;
		unsigned short* nBuffer = (unsigned short*)this->nline;

		unsigned short* shortScanLineBuffer = (unsigned short*)this->linedata;

		for (int c = 0; c < this->width; c++)
		{
			shortScanLineBuffer[4*c]   = rBuffer[c];
			shortScanLineBuffer[4*c+1] = gBuffer[c];
			shortScanLineBuffer[4*c+2] = bBuffer[c];
			shortScanLineBuffer[4*c+3] = nBuffer[c];
		}
		
		memcpy(buffer, shortScanLineBuffer, this->width*4*this->sizeofpixel);
	}

	return true;	
}


/**
 * gets a tile of imagery RGB data is returned BGR0
 *
 * @param r, c the row and column of upper left of tile
 * @return the buffer containing the pixels
 */
unsigned char* CGDALFileImageOnly::getTile(int r, int c)
{
	if (this->tiles == NULL)
		return false;

	int tileRow = r/this->tileHeight;
	int tileCol = c/this->tileWidth;

	// first check the buffered tiles
	for (int i = 0; i < this->nBufferedTiles; i++)
	{
		CImageTile* imageTile = this->tiles[i];

		// make sure its there
		if (imageTile == NULL)
			continue;

		if ((imageTile->getTileRow() == tileRow) && (imageTile->getTileCol() == tileCol))
		{
			return imageTile->getData();
		}
	}

	// better buffer the tile

	// increment index (and make sure its in range)
	this->lastBufferedTile++;
	if (this->lastBufferedTile >= this->nBufferedTiles)
			this->lastBufferedTile = 0;

	// allocate new one if nessesary
	if (this->tiles[this->lastBufferedTile] == NULL)
		this->tiles[this->lastBufferedTile] = new CImageTile(this->tileWidth, this->tileHeight, this->bpp, this->bands);

	CImageTile* imageTile = this->tiles[this->lastBufferedTile];

	imageTile->setTileRowCol(tileRow, tileCol);

	// read tile and convert to our format
	unsigned char* buffer = imageTile->getData();

	if (this->bands == 3)
	{
		if (this->bpp == 8)
		{
			int channel;
			for ( int b = 0; b < 3; b++)
			{
				// use the channel order set by the user!
				if (b == 0)
					channel = this->colorChannel1;
				else if (b == 1)
					channel = this->colorChannel2;
				else if (b == 2)
					channel = this->colorChannel3;
				else
					channel = b;	// default rgb order

				this->gdal->GetRasterBand(channel+1)->ReadBlock(tileCol, tileRow, this->tileBuffer);

				for (int r = 0; r < (int)this->tileHeight; r++)
				{
					int lineOffsetQ = r*this->tileWidth*4;
					int lineOffsetT = r*this->tileWidth;

					for (int c = 0; c < (int)this->tileWidth; c++)
					{
						int q = c*4;
						buffer[lineOffsetQ+q+b] = this->tileBuffer[lineOffsetT+c];
					}
				}
			}
		}
		if (this->bpp == 16)
		{
			unsigned long channel;

			unsigned short* shortbuffer = (unsigned short*)buffer;

			for ( int b = 0; b < 3; b++)
			{

				// use the channel order set by the user!
				if (b == 0)
					channel = this->colorChannel1;
				else if (b == 1)
					channel = this->colorChannel2;
				else if (b == 2)
					channel = this->colorChannel3;
				else
					channel = b;	// default rgb order

				unsigned short* shortilebuffer = (unsigned short*)this->tileBuffer;
				this->gdal->GetRasterBand(channel+1)->ReadBlock(tileCol, tileRow, shortilebuffer);

				for (unsigned long r = 0; r < (unsigned long)this->tileHeight; r++)
				{
					unsigned long lineOffsetQ = r*this->tileWidth*4;
					unsigned long lineOffsetT = r*this->tileWidth;

					for (unsigned long c = 0; c < (unsigned long)this->tileWidth; c++)
					{
						unsigned long q = c*4;
						unsigned long indexbuffer = lineOffsetQ+q+b;
						unsigned long indextilebuffer = lineOffsetT+c;
						shortbuffer[indexbuffer] = shortilebuffer[indextilebuffer];
					}
				}
			}
		}
	}
	else if (this->bands == 4)
	{
		if (this->bpp == 8)
		{
			for ( int b = 0; b < 4; b++)
			{
				int channel;
				// use the channel order set by the user!
				if (b == 0)
					channel = this->colorChannel1;
				else if (b == 1)
					channel = this->colorChannel2;
				else if (b == 2)
					channel = this->colorChannel3;
				else if (b == 3)
					channel = this->colorChannel4;
				else
					channel = b;	// default rgba order


				this->gdal->GetRasterBand(channel+1)->ReadBlock(tileCol, tileRow, this->tileBuffer);

				for (int r = 0; r < (int)this->tileHeight; r++)
				{
					int lineOffsetQ = r*this->tileWidth*4;
					int lineOffsetT = r*this->tileWidth;

					for (int c = 0; c < (int)this->tileWidth; c++)
					{
						int q = c*4;
						buffer[lineOffsetQ+q+b] = this->tileBuffer[lineOffsetT+c];
					}
				}
			}
		}
		if (this->bpp == 16)
		{
			unsigned long channel;

			unsigned short* shortbuffer = (unsigned short*)buffer;

			for ( int b = 0; b < 4; b++)
			{

				// use the channel order set by the user!
				if (b == 0)
					channel = this->colorChannel1;
				else if (b == 1)
					channel = this->colorChannel2;
				else if (b == 2)
					channel = this->colorChannel3;
				else if (b == 3)
					channel = this->colorChannel4;
				else
					channel = b;	// default rgba order

				unsigned short* shortilebuffer = (unsigned short*)this->tileBuffer;
				this->gdal->GetRasterBand(channel+1)->ReadBlock(tileCol, tileRow, shortilebuffer);

				for (unsigned long r = 0; r < (unsigned long)this->tileHeight; r++)
				{
					unsigned long lineOffsetQ = r*this->tileWidth*4;
					unsigned long lineOffsetT = r*this->tileWidth;

					for (unsigned long c = 0; c < (unsigned long)this->tileWidth; c++)
					{
						unsigned long q = c*4;
						unsigned long indexbuffer = lineOffsetQ+q+b;
						unsigned long indextilebuffer = lineOffsetT+c;
						shortbuffer[indexbuffer] = shortilebuffer[indextilebuffer];
					}
				}
			}
		}
	}
	else if (this->bands == 1)
	{
		this->gdal->GetRasterBand(1)->ReadBlock(tileCol, tileRow, buffer);
	}


	return imageTile->getData();
}

bool CGDALFileImageOnly::readTiledLine(unsigned char* buffer, int iRow)
{
    int rowInTile = iRow % this->tileHeight;
	
    int rowOffset = 0;
	
	if (this->bands == 3)
	{
		if ( this->bpp == 8 )
		{
			rowOffset = rowInTile*this->tileWidth*4;
		}
		if ( this->bpp == 16 )
		{
			rowOffset = rowInTile*this->tileWidth*8;
		}
	}
	else if (this->bands == 4)
	{
		if ( this->bpp == 8 )
		{
			rowOffset = rowInTile*this->tileWidth*4;
		}
		if ( this->bpp == 16 )
		{
			rowOffset = rowInTile*this->tileWidth*8;
		}
	}
	else
	{
        rowOffset = rowInTile*this->tileWidth*this->bpp/8;
	}

	for (int c = 0; c < this->width; c+= this->tileWidth)
	{
		unsigned char* src = this->getTile(iRow, c); // get the tile that includes this r, c
		
		if (this->bands == 3)
		{
			if ( this->bpp == 8 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (unsigned int)this->width)
				{
					memcpy(buffer+4*c, src+rowOffset, (this->width-c)*4);
				}
				else
				{
					memcpy(buffer+4*c, src+rowOffset, this->tileWidth*4);
				}
			}
			if ( this->bpp == 16 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (unsigned int)this->width)
				{
					memcpy(buffer+8*c, src+rowOffset, (this->width-c)*8);
				}
				else
				{
					memcpy(buffer+8*c, src+rowOffset, this->tileWidth*8);
				}
			}
		}
		else if (this->bands == 4)
		{
			if ( this->bpp == 8 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (unsigned int)this->width)
				{
					memcpy(buffer+4*c, src+rowOffset, (this->width-c)*4);
				}
				else
				{
					memcpy(buffer+4*c, src+rowOffset, this->tileWidth*4);
				}
			}
			if ( this->bpp == 16 )
			{
				// the last tile in a row extends beyond the image
				if (c+this->tileWidth > (unsigned int)this->width)
				{
					memcpy(buffer+8*c, src+rowOffset, (this->width-c)*8);
				}
				else
				{
					memcpy(buffer+8*c, src+rowOffset, this->tileWidth*8);
				}
			}
		}
		else
		{
			// the last tile in a row extends beyond the image
			if (c+this->tileWidth > (unsigned int)this->width)
			{
				memcpy(buffer+c*this->bpp/8, src+rowOffset, (this->width-c)*this->bpp/8);
			}
			else
			{
				memcpy(buffer+c*this->bpp/8, src+rowOffset, this->tileWidth*this->bpp/8);
			}
		}
	}

	return true;
}

bool CGDALFileImageOnly::writeGeoData(CBaseImage* image, GDALDataset* dataSet)
{
	return true;
}


bool CGDALFileImageOnly::writeData(CBaseImage* image,GDALDataset* dataSet, bool tiled)
{
	bool returnValue =false;

	if (this->bands == 1)
	{
		returnValue = this->write1Band(image,dataSet,tiled);
	}
	else if (this->bands == 3)
	{
		returnValue = this->write3Bands(image,dataSet,tiled);
	}
	else if (this->bands == 4)
	{
		returnValue = this->write4Bands(image,dataSet,tiled);
	}
	else
		returnValue = false;


	return returnValue;
}


void CGDALFileImageOnly::setExportFileOptions(CCharString& channelCombination,
							  CCharString& compression,
							  int compressionQuality,
							  bool exportGeoData,
							  bool exportSeperatFile,
							  bool bandInterleave,
							  CFileName fileNameGeoData)
{
	this->channelExport = channelCombination;
	this->compressionExport = compression;
	this->compressionQualityExport = compressionQuality;
	this->exportGeoData = exportGeoData;
	this->exportSeperatFile = exportSeperatFile;
	this->bandInterleave = bandInterleave;
	if (this->exportSeperatFile) this->fileNameGeoData = fileNameGeoData;

}




bool CGDALFileImageOnly::write1Band(CBaseImage* image,GDALDataset* dataSet,bool tiled)
{
	GDALRasterBand *band1 = NULL;

	int inputBands = image->getBands() == 1 ? 1 : 4;	// we always use 1 or 4 in the hugeImage, never 3!
	int inputBpp = image->getBpp();

	int outputBands = this->bands;
	int outputBpp = this->bpp;
	
	bool convertFrom16To8Bit = inputBpp == 16 && outputBpp == 8;
	bool convertFrom32To16Bit = inputBpp == 32 && outputBpp == 16 ;
	bool convertFrom32To8Bit = inputBpp == 32 && outputBpp == 8;
	bool computeIntensity = this->channelExport.CompareNoCase("Intensity") && inputBands > 1;

	// we may have to convert from 16 to 8 bit later
	CLookupTable lt(inputBpp,image->getBands());
	// compute the lookup table 
	if (convertFrom16To8Bit	)
	{
		lt = image->get16To8ConversionLookupTable();
	}

	unsigned char* bufferUC = NULL;
	unsigned short* bufferUS = NULL;
	float* bufferF = NULL;

	int xBlockSize;
	int yBlockSize;

	if (tiled)
	{
		xBlockSize = image->getTileWidth();
		yBlockSize = image->getTileHeight();
	}
	else
	{
		xBlockSize = this->width;
		yBlockSize = 1;//image->getTileHeight();
	}

	// get the memory
	if (inputBpp == 32)  
	{
		if (convertFrom32To16Bit)
		{	if (bufferUS == NULL)
				bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands * 2];
			bufferF = (float *) bufferUS;
		}
		else 
		{
			if (bufferUC == NULL)
				bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands * 4];
			bufferF = (float *) bufferUC;
		}
	}
	else if (inputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}

	if (outputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}	

	// get the band and set the lable
	band1 = dataSet->GetRasterBand(1);
	band1->SetColorInterpretation(GCI_GrayIndex);

	int offsetInReadBuffer = 0;		
	int offsetInWriteBuffer = inputBands;//(convertFrom16To8Bit && (inputBands > 1)) ? 0  : inputBands;
	// if necessary, select the right channel
	if (inputBands > 1)
	{
		if (this->channelExport.CompareNoCase("Red"))
			offsetInReadBuffer = 0;
		else if (this->channelExport.CompareNoCase("Green"))
			offsetInReadBuffer = 1;
		else if (this->channelExport.CompareNoCase("Blue"))
			offsetInReadBuffer = 2;
		else if (this->channelExport.CompareNoCase("Alpha"))
			offsetInReadBuffer = 3;
		else if (this->channelExport.CompareNoCase("Intensity"))
			offsetInReadBuffer = 0;
	}

	// write the data, step by step
	if (tiled)		// write in tiled blocks
	{
		unsigned char* tileBufferUC= NULL;
		unsigned short* tileBufferUS = NULL;

		int tilesAcross = (this->width + xBlockSize - 1) / xBlockSize;
		int tilesDown =   (this->height + yBlockSize - 1) / yBlockSize;
		int count=0;

		for (int row=0; row < this->height; row+= yBlockSize)
		{
			for (int col=0; col < this->width; col+=xBlockSize)
			{
				count++;
				// read	from the hugeImage
				if (inputBpp == 16)
				{
					tileBufferUS = (unsigned short*)image->getTile(row,col);
					if (convertFrom16To8Bit)
						copyData(tileBufferUS,bufferUC,xBlockSize,yBlockSize,offsetInReadBuffer,0,inputBands,outputBands,lt,computeIntensity);
					else
						copyData(tileBufferUS,bufferUS,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,outputBands,computeIntensity);
				}
				else if (inputBpp == 8)
				{
					tileBufferUC = image->getTile(row,col);	
					copyData(tileBufferUC,bufferUC,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,outputBands,computeIntensity);
				}
				else if (inputBpp == 32)
				{
					tileBufferUC = image->getTile(row,col);	

					if (convertFrom32To16Bit || convertFrom32To8Bit)
					{
						float MAX = image->getHistogram()->getMax();
						float MIN = image->getHistogram()->getMin();
						float range = MAX - MIN;

						float* tileBufferF = (float*) tileBufferUC;
						 for (int r = 0; r < image->tileHeight; r++)
						 {
							 int offset = r*image->tileWidth;

							 for (int c = 0; c < image->tileWidth; c++)
							 {
								float pixel = tileBufferF[offset+c];

								if (pixel < 0) pixel = 0;

								if (convertFrom32To8Bit)
									bufferUC[offset+c] = (unsigned char)(255.0f*((pixel - MIN)/range)+0.5);
								else
									bufferUS[offset+c] = (unsigned short)(65535.0f*((pixel - MIN)/range)+0.5);
							}
						}
					}
					else
						copyData((float *)tileBufferUC,bufferF,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,outputBands,computeIntensity);
				}
				else
				{
					return false;
				}

				int tileRow = row/yBlockSize;
				int tileCol = col/xBlockSize;
	  
				band1->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));

				if (this->listener != NULL)
				{
					double per = (float)count / (float) (tilesAcross*tilesDown);
					this->listener->setProgressValue(per*this->listenerMaxValue);
				}
			}
		}
	}
	else // write row by row
	{
		int sizePerPixel = this->bpp/8;
		for (int i=0; i < this->height; i+=yBlockSize)
		{
			// adjust the last blocksize
			if ((i+yBlockSize) > this->height)
				yBlockSize = this->height - i;

			// read from the hugeImage
			if (inputBpp == 16)
			{
				image->getRect((unsigned char*)bufferUS, i, yBlockSize, 0, xBlockSize);

				if (convertFrom16To8Bit)
					copyData(bufferUS,bufferUC,xBlockSize,yBlockSize,offsetInReadBuffer,offsetInReadBuffer,inputBands,inputBands,lt,computeIntensity);
				else if (computeIntensity)
					copyData(bufferUS,bufferUS,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,inputBands,computeIntensity);			
			}
			else if (inputBpp == 8)
			{
				image->getRect(bufferUC, i, yBlockSize, 0, xBlockSize);
				if (computeIntensity)
					copyData(bufferUC,bufferUC,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,inputBands,computeIntensity);
			}
			else if (inputBpp == 32)
			{
				if (convertFrom32To16Bit)
					image->getRect((unsigned char*)bufferUS, i, yBlockSize, 0, xBlockSize);
				else
					image->getRect(bufferUC, i, yBlockSize, 0, xBlockSize);

				if (convertFrom32To16Bit || convertFrom32To8Bit)
				{
					float MAX = image->getHistogram()->getMax();
					float MIN = image->getHistogram()->getMin();
					float range = MAX - MIN;

					float* tmpBufferF = convertFrom32To16Bit ? (float*)bufferUS : (float*) bufferUC;
   
					for (int c = 0; c < xBlockSize; c++)
					{
						float pixel = tmpBufferF[c];

						if (pixel < 0) pixel = 0;

						if (convertFrom32To8Bit)
							bufferUC[c] = (unsigned char)(255.0f*((pixel - MIN)/range)+0.5);
						else
							bufferUS[c] = (unsigned short)(65535.0f*((pixel - MIN)/range)+0.5);
					}
				}
				else
					copyData((float*)bufferUC,bufferF,xBlockSize,yBlockSize,offsetInReadBuffer,inputBands,outputBands,computeIntensity);
			}

			// write to disk
			band1->RasterIO(GF_Write,	// write flag 
								0,		// offset cols
								i,		// offset rows
								xBlockSize, // number of cols to be written
								yBlockSize,		 // number of rows to be written
								bufferUC == NULL ?
								(unsigned char*)(bufferUS + offsetInReadBuffer) :
								(unsigned char*)(bufferUC + offsetInReadBuffer), // src buffer + offset for single color channels
								xBlockSize,	// number of cols in the buffer to be read
								yBlockSize,			// number of rows in the buffer to be read
								(GDALDataType)this->sizeofpixel,   // dataType
								offsetInWriteBuffer*sizePerPixel,		   // offset between 2 pixels in one row for single color channels
								0												// offset between 2 rows 	
								);	

			if (this->listener != NULL)
			{
				double per = (float)i / (float) this->height;
				this->listener->setProgressValue(per*this->listenerMaxValue);
			}
		}
	}

	if (bufferUC != NULL)
		delete [] bufferUC;
	if (bufferUS != NULL)
		delete [] bufferUS;

	return true;
}

bool CGDALFileImageOnly::write3Bands(CBaseImage* image,GDALDataset* dataSet,bool tiled)
{

	vector<GDALRasterBand*> rasterBands;
	rasterBands.resize(3);

	vector<int> channel;
	channel.resize(3);

	unsigned char* buffer = NULL;

	bool returnValue = true;

	if (dataSet == NULL)
		return false;

	int step=1;  // number of rows, written in one call
	int inputBands = image->getBands() == 1 ? 1 : 4;	// we always use 1 or 4 in the hugeImage, never 3!
	int inputBpp = image->getBpp();

	int outputBands = this->bands;
	int outputBpp = this->bpp;

	bool convertFrom16To8Bit = inputBpp == 16 && outputBpp == 8;

	// we may have to convert from 16 to 8 bit later
	CLookupTable lt(inputBpp,3);
	// compute the lookup table 
	if (convertFrom16To8Bit	)
	{
		lt = image->get16To8ConversionLookupTable();
	}

	unsigned char* bufferUC = NULL;
	unsigned short* bufferUS = NULL;

	int xBlockSize;
	int yBlockSize;

	if (tiled)
	{
		xBlockSize = image->getTileWidth();
		yBlockSize = image->getTileHeight();
	}
	else
	{
		xBlockSize = this->width;
		yBlockSize = image->getTileHeight();
	}

	// get the memory
	if (inputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}

	if (outputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}	

	// get the 3 bands
	rasterBands[0] = dataSet->GetRasterBand(1);
	rasterBands[1] = dataSet->GetRasterBand(2);
	rasterBands[2] = dataSet->GetRasterBand(3);
	
	// set the correct band lables and select channels
	if (this->channelExport.CompareNoCase("RGB"))
	{
		rasterBands[0]->SetColorInterpretation(GCI_RedBand);
		rasterBands[1]->SetColorInterpretation(GCI_GreenBand);
		rasterBands[2]->SetColorInterpretation(GCI_BlueBand);
		channel[0] = 0;	
		channel[1] = 1;
		channel[2] = 2;	
	}
	else if (this->channelExport.CompareNoCase("CIR"))
	{
		rasterBands[0]->SetColorInterpretation(GCI_AlphaBand);
		rasterBands[1]->SetColorInterpretation(GCI_RedBand);
		rasterBands[2]->SetColorInterpretation(GCI_GreenBand);
		channel[0] = 3;
		channel[1] = 0;
		channel[2] = 1;
	}
	else
		returnValue = false;

	int offsetInWriteBuffer = 4;
	
	if (tiled)
	{
		int tilesAcross = (this->width + xBlockSize - 1) / xBlockSize;
		int tilesDown =   (this->height + yBlockSize - 1) / yBlockSize;
		int count=0;

		unsigned char* tileBufferUC = NULL;
		unsigned short* tileBufferUS = NULL;

		for (int row=0; row < this->height; row+= yBlockSize)
		{
			for (int col=0; col < this->width; col+=xBlockSize)
			{
				count++;
				int tileRow = row/yBlockSize;
				int tileCol = col/xBlockSize;				
				
				// read	from the hugeImage
				if (inputBpp == 16)
				{
					tileBufferUS = (unsigned short*)image->getTile(row,col);
					if (convertFrom16To8Bit)
					{
						for (int band = 0; band < 3; band++)
						{
							copyData(tileBufferUS,bufferUC,xBlockSize,yBlockSize,channel[band],0,4,1,lt,false);
							rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
						}
					}
					else
					{
						for (int band = 0; band < 3; band++)
						{
							copyData(tileBufferUS,bufferUS,xBlockSize,yBlockSize,channel[band],4,1,false);
							rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
						}
					}

				}
				else
				{
					tileBufferUC = image->getTile(row,col);	
					for (int band = 0; band < 3; band++)
					{
						copyData(tileBufferUC,bufferUC,xBlockSize,yBlockSize,channel[band],4,1,false);
						rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
					}
				}

				if (this->listener != NULL)
				{
					double per = (float)count / (float) (tilesAcross*tilesDown);
					this->listener->setProgressValue(per*this->listenerMaxValue);
				}
			}
		}


	}
	else
	{
		// write the data, step by step
		for (int i=0; i < this->height && returnValue; i+=yBlockSize)
		{
			// adjust the last blocksize
			if ((i+yBlockSize) > this->height)
				yBlockSize = this->height - i;
		
			if (inputBpp == 16)
			{	
				image->getRect((unsigned char*)bufferUS, i, yBlockSize, 0, xBlockSize);
				if (convertFrom16To8Bit)
				{
					copyData(bufferUS,bufferUC,xBlockSize*inputBands,yBlockSize,0,0,1,1,lt,false);
				}
			}
			else
			{
				image->getRect(bufferUC, i, yBlockSize, 0, xBlockSize);
			}
			

			for (int band=0; band < 3; band++)
				rasterBands[band]->RasterIO(GF_Write, 
											0, 
											i, 
											xBlockSize, 
											yBlockSize, 
											bufferUC == NULL ? (unsigned char*) (bufferUS + channel[band]) :(unsigned char*) (bufferUC + channel[band]),
											xBlockSize, 
											yBlockSize, 
											(GDALDataType)this->sizeofpixel, 
											offsetInWriteBuffer*this->sizeofpixel,
											0 );

			if (this->listener != NULL)
			{
				double per = (float)i / (float) this->height;
				this->listener->setProgressValue(per*this->listenerMaxValue);
			}
		}		
	}
	
	if (bufferUC != NULL)
		delete [] bufferUC;
	if (bufferUS != NULL)
		delete [] bufferUS;
	
	return true;
}

bool CGDALFileImageOnly::write4Bands(CBaseImage* image,GDALDataset* dataSet,bool tiled)
{
	vector<GDALRasterBand*> rasterBands;
	rasterBands.resize(4);

	if (dataSet == NULL)
		return false;

	int inputBands = 4;	
	int inputBpp = image->getBpp();

	int outputBands = 4;
	int outputBpp = this->bpp;

	bool convertFrom16To8Bit = inputBpp == 16 && outputBpp == 8;

	// we may have to convert from 16 to 8 bit later
	CLookupTable lt(inputBpp,image->getBands());
	// compute the lookup table 
	if (convertFrom16To8Bit	)
	{
		lt = image->get16To8ConversionLookupTable();
	}

	int xBlockSize;
	int yBlockSize;

	if (tiled)
	{
		xBlockSize = image->getTileWidth();
		yBlockSize = image->getTileHeight();
	}
	else
	{
		xBlockSize = this->width;
		yBlockSize = image->getTileHeight();
	}

	unsigned char* bufferUC = NULL;
	unsigned short* bufferUS = NULL;

	// get the memory
	if (inputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}

	if (outputBpp == 16)  
	{
		if (bufferUS == NULL)
			bufferUS = new unsigned short [xBlockSize * yBlockSize * inputBands];
	}
	else
	{
		if (bufferUC == NULL)
			bufferUC = new unsigned char [xBlockSize * yBlockSize * inputBands];
	}

	// get the 4 bands
	rasterBands[0] = dataSet->GetRasterBand(1);
	rasterBands[1] = dataSet->GetRasterBand(2);
	rasterBands[2] = dataSet->GetRasterBand(3);
	rasterBands[3] = dataSet->GetRasterBand(4);

	rasterBands[0]->SetColorInterpretation(GCI_RedBand);
	rasterBands[1]->SetColorInterpretation(GCI_GreenBand);
	rasterBands[2]->SetColorInterpretation(GCI_BlueBand);
	rasterBands[3]->SetColorInterpretation(GCI_AlphaBand);

	int offsetInWriteBuffer = 4;
	if (tiled)
	{
		int tilesAcross = (this->width + xBlockSize - 1) / xBlockSize;
		int tilesDown =   (this->height + yBlockSize - 1) / yBlockSize;
		int count=0;

		unsigned char* tileBufferUC = NULL;
		unsigned short* tileBufferUS = NULL;

		for (int row=0; row < this->height; row+= yBlockSize)
		{
			for (int col=0; col < this->width; col+=xBlockSize)
			{
				count++;
				int tileRow = row/yBlockSize;
				int tileCol = col/xBlockSize;				
				
				// read	from the hugeImage
				if (inputBpp == 16)
				{
					tileBufferUS = (unsigned short*)image->getTile(row,col);
					if (convertFrom16To8Bit)
					{
						for (int band = 0; band < 4; band++)
						{
							copyData(tileBufferUS,bufferUC,xBlockSize,yBlockSize,band,0,4,1,lt,false);
							rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
						}
					}
					else
					{
						for (int band = 0; band < 4; band++)
						{
							copyData(tileBufferUS,bufferUS,xBlockSize,yBlockSize,band,4,1,false);
							rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
						}
					}

				}
				else
				{
					tileBufferUC = image->getTile(row,col);	
					for (int band = 0; band < inputBands; band++)
					{
						copyData(tileBufferUC,bufferUC,xBlockSize,yBlockSize,band,4,1,false);
						rasterBands[band]->WriteBlock(tileCol,tileRow,bufferUC == NULL ?(unsigned char*)(bufferUS) :(unsigned char*)(bufferUC));
					}
				}

				if (this->listener != NULL)
				{
					double per = (float)count / (float) (tilesAcross*tilesDown);
					this->listener->setProgressValue(per*this->listenerMaxValue);
				}
			}
		}
	}
	else
	{
		// write the data, step by step
		for (int i=0; i < this->height; i+=yBlockSize)
		{
			// adjust the last blocksize
			if ((i+yBlockSize) > this->height)
				yBlockSize = this->height - i;
		
			if (inputBpp == 16)
			{	
				image->getRect((unsigned char*)bufferUS, i, yBlockSize, 0, xBlockSize);
				if (convertFrom16To8Bit)
					copyData(bufferUS,bufferUC,xBlockSize*inputBands,0,yBlockSize,0,1,1,lt,false);
			}
			else
				image->getRect(bufferUC, i, yBlockSize, 0, xBlockSize);


			for (int band =0; band < inputBands; band++)
				rasterBands[band]->RasterIO(GF_Write, 
											0, 
											i, 
											xBlockSize, 
											yBlockSize, 
											bufferUC == NULL ? (unsigned char*) (bufferUS + band) :(unsigned char*) (bufferUC + band),
											xBlockSize,
											yBlockSize, 
											(GDALDataType)this->sizeofpixel, 
											offsetInWriteBuffer*this->sizeofpixel,
											0 );
		
			if (this->listener != NULL)
			{
				double per = (float)i / (float) this->height;
				this->listener->setProgressValue(per*this->listenerMaxValue);
			}
		
		}
	}

	if (bufferUC != NULL)
		delete [] bufferUC;
	if (bufferUS != NULL)
		delete [] bufferUS;

	return true;
}





bool CGDALFileImageOnly::readFileInformation()
{
	if (!this->bHeaderRead)
	{
		this->readHeader();
		if (this->gdal)
			GDALClose(this->gdal);

		this->gdal = NULL;
	}

	return this->bHeaderRead;
}



void CGDALFileImageOnly::readLookupTableInfo()
{
	GDALColorTable* table = this->gdal->GetRasterBand(1)->GetColorTable();

	int entrycount = table->GetColorEntryCount();

	if (this->lut) delete this->lut;
	this->lut = new CLookupTable ( this->bpp, 3);

	int rval, bval, gval;

	int lutsize = this->lut->getMaxEntries();

	for (int i = 0; i < entrycount; i++ )
	{
		const GDALColorEntry* color = table->GetColorEntry(i);

		rval = (int)color->c1;
		gval = (int)color->c2;
		bval = (int)color->c3;
		
		this->lut->setColor(rval, gval, bval, i);
	}
}


void CGDALFileImageOnly::writeLookupTableInfo()
{
	if ( this->lut )
	{
		GDALColorTable gdallut = GDALColorTable(GPI_Gray);

		int entrycount = this->lut->getMaxEntries();
		int rval, bval, gval;
		GDALColorEntry gdalentry;

		for ( int i =0; i < entrycount; i++)
		{
			this->lut->getColor(rval, bval, gval, i);

			gdalentry.c1 = rval;
			gdalentry.c2 = gval;
			gdalentry.c3 = bval;
			gdalentry.c4 = 0;

			gdallut.SetColorEntry(i, &gdalentry); 

		}

		int hmm =9;


		for (int i = 0; i < gdallut.GetColorEntryCount(); i++ )
		{
			const GDALColorEntry* color = gdallut.GetColorEntry(i);

			rval = (int)color->c1;
			gval = (int)color->c2;
			bval = (int)color->c3;
		}
	}
}

float CGDALFileImageOnly::getNoDataValue(int band)
{
	if ( band == 1 )
		return this->NoData1;
	else if ( band == 2 )
		return this->NoData2;
	else if ( band == 3 )
		return this->NoData3;
	else if ( band == 4 )
		return this->NoData4;
	else return -9999.0;
}



