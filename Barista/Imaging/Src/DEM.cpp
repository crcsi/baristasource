#include <vector>
#include "DEM.h"
#include <memory.h>
#include <float.h>
#include <ANN/ANN.h>

#include "ALSData.h"
#include "ALSPointCloud.h"
#include "StructuredFile.h"
#include "Serializable.h"
#include "ImgBuf.h"
#include "XYZPolyLineArray.h"
#include "XYPolyLine.h"
#include "XYContour.h"
#include "XYContourArray.h"
#include "2DPoint.h"
#include "SystemUtilities.h"
#include "doubles.h"
#include "Prediction.h"


#include "Trans3DFactory.h"

#include "GDALFile.h"

/* This had to be included due to compilation issues
 * @author dArE / cRoNk / HaNlEy
 * @version 1.0
 *                             __
 *                           /'_/)
 *                         ,/_  /
 *                        /    /
 *                  /'_'/'   '/'_'7,
 *               /'/    /    /    /"
 *              ('(    '    '     _~/
 *               \                 '
 *                '\'   \           _7
 *                  \              (
 *                   \               \.
 *
 */

CDEM::CDEM(void) : sigmaZ(1.0)
{
	this->isloaded = false;
	this->isgridded = false;
	this->isinitialized = false;

	this->Pmin.z = FLT_MAX;
	this->Pmax.z = -FLT_MAX;
}

CDEM::~CDEM(void)
{
}

/*
CFileName* CDEM::getFileName()
{
    return &this->FileName;
}

void CDEM::setFileName(const char* filename)
{
    this->FileName = filename;
}
*/
CFileName CDEM::getItemText()
{
    CFileName itemtext = this->filename.GetFullFileName();
    return itemtext.GetChar();
}


bool CDEM::getisloaded()
{
    return this->isloaded;
}

void CDEM::setisloaded(bool loadstatus)
{
    this->isloaded = loadstatus;
}
	
void CDEM::setSigmaZ(float sigma)
{
	if (sigma > 0) this->sigmaZ = sigma;
};

void CDEM::load(bool initDEMBorder)
{   
	if (initDEMBorder)
	{
		if (this->demBorder3DLines.GetSize() < 1)
			//this->initBorder(); //previous function; we added new function to speed up
			//addition >>
			this->initOutsideBorder();
			//<< addition
	}
	
    if (this->getisloaded())
    {
        return;
    }

	this->getmaxX();
	this->getmaxY();
	this->getmaxZ();
	this->getminZ();

    this->getisgridded();
}

bool CDEM::getisgridded()
{

    /// @todo at the moment gridded is allways true after this
    /// see getNumPoints
	if ((this->getWidth() * this->getHeight()) == this->getNumPoints())
    {
        this->isgridded = true;
    }

    return this->isgridded;
}

int CDEM::getNumPoints ()
{
    int NumPoints = (int)(this->getWidth()*this->getHeight());

	return NumPoints;
}

double CDEM::getminZ()
{
	if (this->getisloaded())
	{
		return this->Pmin.z;
	}

	if (fabs(this->Pmax.z + FLT_MAX) < FLT_EPSILON ||
		fabs(this->Pmin.z - FLT_MAX) < FLT_EPSILON)
	{

		double maxheight = -FLT_MAX;
		double minheight =  FLT_MAX;

		unsigned char* buf = new unsigned char[this->getWidth()*this->pixelSizeBytes()];

		float currentZ;

		for (int r = 0; r < this->getHeight(); r++)
		{
			this->getLine(buf, r);

			for (int i = 0, j = 0; i < this->getWidth(); i++, j+=4)
			{
				float* fPixel = (float*)(buf+j);

				currentZ = *fPixel;
				if (currentZ != DEM_NOHEIGHT)
				{
					if (currentZ > maxheight) maxheight = currentZ;
					if (currentZ < minheight) minheight = currentZ;
				}
			}
		}

		this->Pmax.z = (float)maxheight;
		this->Pmin.z = (float)minheight;

		delete [] buf;
	}

	this->setisloaded(true);

	return this->Pmin.z;
}

double CDEM::getmaxZ()
{

	if (this->getisloaded())
	{
		return this->Pmax.z;
	}

	if (fabs(this->Pmax.z + FLT_MAX) < FLT_EPSILON ||
		fabs(this->Pmin.z - FLT_MAX) < FLT_EPSILON)
	{
		double maxheight = -FLT_MAX;
		double minheight =  FLT_MAX;

		unsigned char* buf = new unsigned char[this->getWidth()*this->pixelSizeBytes()];

		float currentZ;

		for (int r = 0; r < this->getHeight(); r++)
		{
			this->getLine(buf, r);

			for (int i = 0, j = 0; i < this->getWidth(); i++, j+=4)
			{
				float* fPixel = (float*)(buf+j);

				currentZ = *fPixel;

				if (currentZ != DEM_NOHEIGHT)
				{
					if (currentZ > maxheight) maxheight = currentZ;
					else if (currentZ < minheight) minheight = currentZ;
				}
			}
		}

		this->Pmax.z = (float)maxheight;
		this->Pmin.z = (float)minheight;

		delete [] buf;
	}

	this->setisloaded(true);
	return this->Pmax.z;
}

double CDEM::getgridX() const
{
	if ( this->tfw.getRefSys().getCoordinateType() == eGEOGRAPHIC )
		return fabs(this->tfw.getB());
	else
		return fabs(this->tfw.getA());
};

double CDEM::getgridY() const
{
	if ( this->tfw.getRefSys().getCoordinateType() == eGEOGRAPHIC )
		return fabs(this->tfw.getD());
	else
		return fabs(this->tfw.getE());
};

void CDEM::setTFW(const CTFW &tfw)
{
	if (&tfw != &this->tfw) this->tfw.copy(tfw);
	C2DPoint p(0, this->height - 1);
	this->tfw.transformObs2Obj(this->Pmin, p, this->Pmin.z);
	p.x = this->width - 1;
	p.y = 0;
	this->tfw.transformObs2Obj(this->Pmax, p, this->Pmax.z);
	this->tfw.sethasValues(true);
};

void CDEM::setTFW(const C2DPoint &shift, const C2DPoint &gridSize)
{
	this->tfw.setFromShiftAndPixelSize(shift, gridSize);
	this->setTFW(this->tfw);
	this->tfw.sethasValues(true);
};

void CDEM::worldToPixel(C3DPoint &pixel, const C3DPoint &world) const
{
	this->tfw.transformObj2Obs(pixel, world);
};

void CDEM::worldToPixel(C2DPoint &pixel, const C3DPoint &world) const
{
	this->tfw.transformObj2Obs(pixel, world);
};

void CDEM::pixelToWorld(C3DPoint &world, const C3DPoint &pixel)const
{
	this->tfw.transformObs2Obj(world, pixel);
};

void CDEM::pixelToWorld(C3DPoint &world, const C2DPoint &pixel)const
{
	this->tfw.transformObs2Obj(world, pixel, world.z);
};

double CDEM::getBLInterpolatedHeight(double X, double Y, 
									 double &g00, double &g10, 
									 double &g01, double &g11)
{
	g00 = g10 = g01 = g11 = 0.0;

    float Z = -9999.0;
   
	if ((X >= this->getminX()) && (X <= this->getmaxX()) &&
	    (Y >= this->getminY()) && (Y <= this->getmaxY()))
	{
		C3DPoint World(X,Y, 0),Pix;
		this->worldToPixel(Pix, World);

		int col = (int) floor(Pix.x);
		int row = (int) floor(Pix.y);

		Pix.x -= col;
		Pix.y -= row;

		float *buf = new float [4];

		// treat some special cases
		if (col == this->getWidth() - 1)
		{
			// xmax corner case
			if (row == this->getHeight() - 1) 
			{
				this->getRect((unsigned char *) buf, row, 1, col, 1);
				g00 = 1.0;
				Z = buf[0];
			}
			else 
			// "normal" xmax corner case
			{
				// interpolation along the edge
				this->getRect((unsigned char *) buf, row, 2, col, 1);

				if ((buf[0] != DEM_NOHEIGHT) && (buf[1] != DEM_NOHEIGHT))
				{
					g00 =(1.0 - Pix.y);
					g01 = Pix.y;
					Z = float(g00 * buf[0] + g01 * buf[1]);
				}
			}
		}
		else if (row == this->getHeight() - 1)
		{
			// interpolation along the edge
			this->getRect((unsigned char *) buf, row, 1, col, 2);
			if ((buf[0] != DEM_NOHEIGHT) && (buf[1] != DEM_NOHEIGHT))
			{
				g00 =(1.0 - Pix.x);
				g10 = Pix.x;
					
				Z = float(g00 * buf[0] + g10 * buf[1]);
			}
		}
		else 
		{
			// general case
			this->getRect((unsigned char *) buf, row, 2, col, 2);
			float f00 = buf[0];
			float f10 = buf[1];
			float f01 = buf[2];
			float f11 = buf[3];

			if (f00 == DEM_NOHEIGHT || f10 == DEM_NOHEIGHT ||
				f01 == DEM_NOHEIGHT || f11 == DEM_NOHEIGHT) 
			{
				if(f00 == DEM_NOHEIGHT && f10 == DEM_NOHEIGHT &&
				   f01 == DEM_NOHEIGHT && f11 == DEM_NOHEIGHT) 
				{
				   Z = (float)DEM_NOHEIGHT;   	    	    
				}
				else
				{
					if(f00 != DEM_NOHEIGHT) Z = f00;
					if(f10 != DEM_NOHEIGHT) Z = f10;
					if(f01 != DEM_NOHEIGHT) Z = f01;
					if(f11 != DEM_NOHEIGHT) Z = f11;

				   Z = (float)DEM_NOHEIGHT;   	    	    
				}
			}
			else
			{
				double xy = Pix.x * Pix.y;
				g00 = 1.0 - Pix.x - Pix.y + xy;
				g10 = Pix.x - xy;
				g01 = Pix.y - xy;
				g11 = xy;

				/*double a0=f00;  
				double a1=f10 - f00;
				double a2=f01 - f00;
				double a3=f11 - a0 - a1 - a2;					

				Z = (float)(a0 + a1 * Pix.x + a2 * Pix.y + a3 * Pix.x * Pix.y);	
				*/
				Z = float(g00 * f00 + g01 * f01 + g10 * f10 + g11 * f11);
			};
		}

		delete [] buf;
	}

	return Z;
};

double CDEM::getBLInterpolatedHeight (double X, double Y)
{
	double g00, g10, g01, g11;

	return getBLInterpolatedHeight(X, Y, g00, g10, g01, g11);
}

double CDEM::getBLInterpolatedHeight(double X, double Y, float &sigma)
{
	double g00, g10, g01, g11;

	double z = getBLInterpolatedHeight(X, Y, g00, g10, g01, g11);

	sigma = float(this->sigmaZ * sqrt(g00* g00 + g10 * g10 + g01 * g01 + g11 * g11));

	return z;
};

// Added by Mehdi
double CDEM::getBLInterpolatedHeight (double X, double Y, double LLX, double LLY, double LRX, double LRY,
									  double ULX, double ULY, double URX, double URY)
{
	float Z = -9999.0;

	if ((X >= this->getminX()) && (X <= this->getmaxX()) &&
		(Y >= this->getminY()) && (Y <= this->getmaxY()))
	{
		// check for boundary extents
		// check for low-left X
		if (LLX < this->getminX())
		{
			LLX= this->getminX();
			ULX= this->getminX();
		}
        // check for low-right X
		if (LRX > this->getmaxX())
		{
			LRX= this->getmaxX();
			URX= this->getmaxX();
		}
        // check for up-left Y
		if (ULY > this->getmaxY())
		{
			ULY= this->getmaxY();
			URY= this->getmaxY();
		}
        // check for low-left Y
		if (LLY < this->getminY())
		{
			LLY= this->getminY();
			LRY= this->getminY();
		}

		vector <int> ro;
		vector <int> col;
		vector <float> Hvalues; 
		C3DPoint Pix;

		C3DPoint WorldLL(LLX,LLY, 0);
		this->worldToPixel(Pix, WorldLL);
		col.push_back((int) floor (Pix.x));
		ro.push_back((int) floor (Pix.y));

		C3DPoint WorldLR(LRX,LRY, 0);
		this->worldToPixel(Pix, WorldLR);
		col.push_back((int) floor (Pix.x));
		ro.push_back((int) floor (Pix.y));

		C3DPoint WorldUL(ULX,ULY, 0);
		this->worldToPixel(Pix, WorldUL);
		col.push_back((int) floor (Pix.x));
		ro.push_back((int) floor (Pix.y));

		C3DPoint WorldUR(URX,URY, 0);
		this->worldToPixel(Pix, WorldUR);
		col.push_back((int) floor (Pix.x));
		ro.push_back((int) floor (Pix.y));

		float *buf = new float;
   
		for (int i=0; i< col.size(); i++)
		{
					this->getRect((unsigned char *) buf, ro[i], 1, col[i], 1);
					Hvalues.push_back(buf[0]);
		}

			if (Hvalues[0] == DEM_NOHEIGHT || Hvalues[1] == DEM_NOHEIGHT ||
				Hvalues[2] == DEM_NOHEIGHT || Hvalues[3] == DEM_NOHEIGHT) 
			{

				double g00, g10, g01, g11;
				return getBLInterpolatedHeight(X, Y, g00, g10, g01, g11);
			}

			else

			{

				C3DPoint World(X,Y, 0);
				this->worldToPixel(Pix, World);
				
				// interpolation in col direction
				double FR1, FR2;

				double DENx = col[1] - col[0];
				double NUM1x = col[1] - Pix.x;
				double NUM2x = Pix.x - col[0];
								
				FR1= (NUM1x/DENx)*Hvalues[0] + (NUM2x/DENx)*Hvalues[1];
				FR2= (NUM1x/DENx)*Hvalues[2] + (NUM2x/DENx)*Hvalues[3];

				// interpolation in row direction
				double DENy = ro[0] - ro[2];
				double NUM1y = ro[0] - Pix.y;
				double NUM2y = Pix.y - ro[2];

				Z =	 (NUM1y/DENy)*FR2 + (NUM2y/DENy)*FR1;

			}
							
		delete [] buf;

	}

	return Z;
};

// Added by Mehdi
double CDEM::getBLInterpolatedHeight (double X, double Y, double gridSizeMaster)
{

	double halfGrid, LLX, LLY, LRX, LRY, ULX, ULY, URX, URY;
    
	halfGrid= gridSizeMaster/2;
	
	LLX= X - halfGrid;
	LLY= Y - halfGrid;

	LRX= X + halfGrid;
	LRY= Y - halfGrid;

	ULX= X - halfGrid;
	ULY= Y + halfGrid;

	URX= X + halfGrid;
	URY= Y + halfGrid;
	
	return 	getBLInterpolatedHeight(X, Y, LLX, LLY, LRX, LRY, ULX, ULY, URX, URY);
};


void CDEM::markEdgePixels(float *rowbuf, CImageBuffer &edgebuf, int row)
{
	long u = row * edgebuf.getWidth();
	for (long c = 0; c < this->getWidth(); ++c)
	{
		if (rowbuf[c] != DEM_NOHEIGHT) edgebuf.pixUChar(u + c) = 1;
	}
};
	    
void CDEM::markEdgePixels(float *rowbuf1, float *rowbuf2, CImageBuffer &edgebuf, int row)
{
	long u = row * edgebuf.getWidth();
    long uMax = this->getWidth() - 1;

	if (rowbuf2[0]    != DEM_NOHEIGHT) edgebuf.pixUChar(u) = 1;
	if (rowbuf2[uMax] != DEM_NOHEIGHT) edgebuf.pixUChar(u + uMax) = 1;
	u++;

	for (long c = 1; c < uMax; ++c, ++u)
	{
		float p0 = rowbuf2[c];
		float pxM1 = rowbuf2[c-1];
		float pxP1 = rowbuf2[c+1];
		float pyM1 = rowbuf1[c];

		if (p0 != DEM_NOHEIGHT)
		{
			if ((pxM1 == DEM_NOHEIGHT) || (pyM1 == DEM_NOHEIGHT) ||
				(pxP1 == DEM_NOHEIGHT)) edgebuf.pixUChar(u) = 1;
		}
		else
		{
			if (pxM1 != DEM_NOHEIGHT) edgebuf.pixUChar(u - 1)   = 1;
			if (pxP1 != DEM_NOHEIGHT) edgebuf.pixUChar(u + 1)   = 1;
			if (pyM1 != DEM_NOHEIGHT) edgebuf.pixUChar(u - edgebuf.getDiffY()) = 1;
		}
	}
};


bool CDEM::extractOneLine(CImageBuffer &edgebuf, CXYContour &poly, doubles &Heights)
{
	unsigned long uStart; 
	poly.deleteContour();
	Heights.clear();

	for (uStart = 0;  (uStart < edgebuf.getNGV()) && (edgebuf.pixUChar(uStart) < 1); ++uStart);

	if (uStart >= edgebuf.getNGV()) return false;

	long dr[9] = {0,  1,  1,  0, -1, -1, -1, 0, 1};
	long dc[9] = {0,  0, -1, -1, -1,  0,  1, 1, 1};
	long r, c, bnd;
    long delta[9];
	delta[0] =  0;
	delta[1] =  edgebuf.getDiffY();
	delta[2] =  edgebuf.getDiffY() - edgebuf.getDiffX();
	delta[3] = -edgebuf.getDiffX();
	delta[4] = -edgebuf.getDiffX() - edgebuf.getDiffY();
	delta[5] = -edgebuf.getDiffY();
	delta[6] = -edgebuf.getDiffY() + edgebuf.getDiffX();
	delta[7] =  edgebuf.getDiffX();
	delta[8] =  edgebuf.getDiffX() + edgebuf.getDiffY();

    // local neighbourhood around central pixel:
	// -------------
	// | 4 | 5 | 6 |
	// |------------
	// | 3 | 0 | 7 | ---> c
	// |------------
	// | 2 | 1 | 8 |
	// |------------
	//       |
	//       V
	//       r

	edgebuf.getRowColBand(uStart, r,c,bnd);

	C2DPoint P(c, r);
	poly.addPoint(P);
	float h;
	unsigned char *hbuf = (unsigned char *) &h;

	this->getRect(hbuf, r, 1, c, 1);

	Heights.push_back(h);

	int prevI = 1;

	do
	{
		edgebuf.pixUChar(uStart) = 0;

		bool found = false;

		for (int i = prevI; i < 9; ++i)
		{
			long r1 = r + dr[i];
			long c1 = c + dc[i];
			if ((r1 >= 0) && (r1 < edgebuf.getHeight()) && 
				(c1 >= 0) && (c1 < edgebuf.getWidth()))
			{
				long u = uStart + delta[i];
				if (edgebuf.pixUChar(u))
				{
					r = r1;
					c = c1;
					uStart = u;
					P.x = c;
					P.y = r;
					poly.addPoint(P);
					this->getRect(hbuf, r, 1, c, 1);
					Heights.push_back(h);
					found = true;
					prevI = i - 1;
					if (prevI < 1) prevI = 8;
					i = 9;
				}
			}
		}
		if (!found)
		{
			for (int i = 1; i < prevI; ++i)
			{
				long r1 = r + dr[i];
				long c1 = c + dc[i];
				if ((r1 >= 0) && (r1 < edgebuf.getHeight()) && 
					(c1 >= 0) && (c1 < edgebuf.getWidth()))
				{
					long u = uStart + delta[i];
					if (edgebuf.pixUChar(u))
					{
						r = r1;
						c = c1;
						uStart = u;
						P.x = c;
						P.y = r;
						poly.addPoint(P);
						this->getRect(hbuf, r, 1, c, 1);
						Heights.push_back(h);
						found = true;
						prevI = i - 1;
						if (prevI < 1) prevI = 8;
						i = 9;
					}
				}
			}
		}
	} while (edgebuf.pixUChar(uStart) > 0);

	return (poly.getPointCount() > 0);
};

//addition >>
float CDEM::getNearestMeanHeight(int x, int y)
{
	float z, Hd = -9999.0, sum;
	int nn = 0, maXnn = 9;
	int stx, sty, enx, eny, i, j, count;
	unsigned char *hbuf = (unsigned char *) &z;
    while (Hd == -9999.0)
	{
        nn = nn+1;
        stx = x-nn;
        enx = x+nn;
        sty = y-nn;
        eny = y+nn;
        
		if (stx < 0)
            stx = 0;
        
        if (sty < 0)
            sty = 0;
        
		if (enx >= this->width)
            enx = this->width-1;
        
		if (eny >= this->height)
            eny = this->height-1;
        
		count = 0; sum = 0;
		for (i = stx; i <= enx; i++)
		{
			for (j=sty; j <= eny; j++)
			{
				//this->getPixel((unsigned char *) &z, i, j);
				this->getRect(hbuf, i, 1, j, 1);
				if (z != -9999.0)
				{
					count++;
					sum += z;
				}
			}
		}

		if (count > 0)
			Hd = sum/count;        
		
		if (nn == maXnn)
			break;
	}

	return Hd;
}

void CDEM::addNewPoint(CXYZPolyLine *newpoly, CXYPoint  p)
{
	float z;
	CXYZPoint P;
	unsigned char *hbuf = (unsigned char *) &z;

	this->pixelToWorld(P, p);
	//this->getPixel((unsigned char *) &z, (int)p.x, (int)p.y);
	this->getRect(hbuf, (int)p.x, 1, (int)p.y, 1);
	if (z == -9999.0)
	{
		z = getNearestMeanHeight((int)p.x, (int)p.y);
	}
	if (z != -9999.0)
		P.z = z;
	else
		P.z = (this->getminZ() + this->getmaxZ())/2;
	newpoly->addPoint(P);
}

bool CDEM::initOutsideBorder()
{	//remove previous lines
	this->demBorder3DLines.RemoveAll();
	
	//add a new border line
	CXYZPolyLine *newpoly = this->demBorder3DLines.add();

	int i;
	//float z;
	CXYPoint  p;
	//CXYZPoint P;
	double maxDist = 500.0;
	double resX = this->getgridX();
	int maxXDistPix = (int) floor(maxDist/resX);
	double resY = this->getgridY();
	int maxYDistPix = (int) floor(maxDist/resY);
	
	//go through first row
	p.y = 0;
	for (i = 0; i < this->width-1; i += maxXDistPix)
	{
		p.x = i;
		addNewPoint(newpoly,p);
		/*
		this->pixelToWorld(P, p);
		z = this->getPixel((unsigned char *) &z, p.x, p.y);
		if (z == -9999.0)
		{
			z = getNearestMeanHeight(p.x, p.y);
		}
		if (z != -9999.0)
			P.z = z;
		else
			P.z = 0;
		newpoly->addPoint(P);
		*/
	}

	//go through last column
	p.x = this->width-1;
	for (i = 0; i < this->height-1; i += maxYDistPix)
	{
		p.y = i;
		addNewPoint(newpoly,p);
		/*this->pixelToWorld(P, p);
		z = this->getPixel((unsigned char *) &z, p.x, p.y);
		if (z == -9999.0)
		{
			z = getNearestMeanHeight(p.x, p.y);
		}
		if (z != -9999.0)
			P.z = z;
		else
			P.z = 0;
		newpoly->addPoint(P);*/
	}

	//go through last row
	p.y = this->height-1;
	for (i = this->width-1; i > 0; i -= maxXDistPix)
	{
		p.x = i;
		addNewPoint(newpoly,p);
		/*this->pixelToWorld(P, p);
		z = this->getPixel((unsigned char *) &z, p.x, p.y);
		if (z == -9999.0)
		{
			z = getNearestMeanHeight(p.x, p.y);
		}
		if (z != -9999.0)
			P.z = z;
		else
			P.z = 0;
		newpoly->addPoint(P);*/
	}

	//go through first column
	p.x = 0;
	for (i = this->height-1; i > 0; i -= maxYDistPix)
	{
		p.y = i;
		addNewPoint(newpoly,p);
		/*this->pixelToWorld(P, p);
		z = this->getPixel((unsigned char *) &z, p.x, p.y);
		if (z == -9999.0)
		{
			z = getNearestMeanHeight(p.x, p.y);
		}
		if (z != -9999.0)
			P.z = z;
		else
			P.z = 0;
		newpoly->addPoint(P);*/
	}

	newpoly->closeLine();
	return true;
}
//<< addition

bool CDEM::initBorder()
{
	this->demBorder3DLines.RemoveAll();

	float *buf1 = new float[this->getWidth()];
	float *buf2 = new float[this->getWidth()];
	
	CImageBuffer borderEdgePixels(0, 0, this->getWidth(), this->getHeight(), 1, 1, true, 0);
	this->getLine((unsigned char *) buf1, 0);

	markEdgePixels(buf1, borderEdgePixels, 0);

	for (int r = 1; r < this->getHeight(); r++)
	{
		this->getLine((unsigned char *) buf2, r);

        markEdgePixels(buf1, buf2, borderEdgePixels, r);

		float *hlp = buf1;
		buf1 = buf2;
		buf2 = hlp;
	}

	markEdgePixels(buf1, borderEdgePixels, this->getHeight() - 1);

	double maxDist = 500.0;

	CXYContourArray contourArray;
	CXYContour *contour = contourArray.Add();

	vector<doubles> HeightVec;
	doubles heights;

	while (extractOneLine(borderEdgePixels, *contour, heights))
	{
		if (contour->getPointCount() > 2)
		{
			HeightVec.push_back(heights);
			contour = contourArray.Add();
		}
	}

	contourArray.RemoveAt(contourArray.GetSize() - 1);

	vector<bool> ClosedVec;


	float closeMin = 2.0;

	for (int i = 0; i < contourArray.GetSize(); ++i)
	{
		contour = contourArray.GetAt(i);

		if (contour->getClosingGap() < closeMin)
		{
			ClosedVec.push_back(true);
		}
		else ClosedVec.push_back(false);
	}

	for (int i = 0; i < contourArray.GetSize(); ++i)
	{
		contour = contourArray.GetAt(i);
		heights = HeightVec[i];
		CXYPolyLine poly;
		poly.initFromContour(contour, 2.0f, 1.0);

		CXYZPolyLine *newpoly = this->demBorder3DLines.add();

		CXYZPoint P1, P2, P3;

		CXYPoint  p = poly.getPoint(0);
		this->pixelToWorld(P1, p);
		int idx = contour->getIndexOfNearestPoint(p);
		P1.z = heights[idx];
		newpoly->addPoint(P1);
	
		for (int j = 1; j < poly.getPointCount(); ++j)
		{
			p = poly.getPoint(j);
			this->pixelToWorld(P2, p);	
			idx = contour->getIndexOfNearestPoint(p);
			P2.z = heights[idx];

			double dist = P2.distanceFrom(&P1);

			if (dist > maxDist)
			{
				C3DPoint dir = (C3DPoint) P2 - (C3DPoint) P1;
				dir /= dir.getNorm();
				int n = (int) floor(dist / maxDist) + 1;
				double deltaD = dist / double(n);
				for (int j = 1; j < n; ++j)
				{
					P3 = P1 + double(j) * deltaD * dir;		
					this->worldToPixel(p, P3);	
					idx = contour->getIndexOfNearestPoint(p);
					P3.z = heights[idx];
					newpoly->addPoint(P3);
				};

			};

			newpoly->addPoint(P2);
			P1 = P2;
		}
		
		if (ClosedVec[i]) newpoly->closeLine();
	}

	return true;
};


CXYZPolyLineArray* CDEM::getBorder()
{
	return &this->demBorder3DLines;
}

void CDEM::writeG3D(const char* filename)
{
	this->init();

	FILE	*fp;
	fp = fopen(filename,"w");

	int dum =0;
	float minx = (float)this->getminX();
	float maxx = (float)this->getmaxX();
	float miny = (float)this->getminY();
	float maxy = (float)this->getmaxY();
	float minz = (float)this->getminZ();
	float maxz = (float)this->getmaxZ();

	float gridx = (float)this->getgridX();
	short height = this->getHeight();
	short width = this->getWidth();

	fwrite(&dum, sizeof(int), 1, fp);
	fwrite(&minx, sizeof(float), 1, fp);
	fwrite(&maxx, sizeof(float), 1, fp);
	fwrite(&miny, sizeof(float), 1, fp);
	fwrite(&maxy, sizeof(float), 1, fp);
	fwrite(&minz, sizeof(float), 1, fp);
	fwrite(&maxz, sizeof(float), 1, fp);
	fwrite(&dum, sizeof(int), 1, fp);

	fwrite(&dum, sizeof(int), 1, fp);
	fwrite(&minx, sizeof(float), 1, fp);
	fwrite(&miny, sizeof(float), 1, fp);
	fwrite(&gridx, sizeof(float), 1, fp);
	fwrite(&width, sizeof(short), 1, fp);
	fwrite(&height, sizeof(short), 1, fp);
	fwrite(&dum, sizeof(int), 1, fp); 

	unsigned char* buf = new unsigned char[this->pixelSizeBytes()*width];

	for(int i=0; i<height; i++)
	{
		fwrite(&i, sizeof(int), 1, fp);

		this->getRect(buf, i, 1, 0, width);

		for(int j=0; j < width; j++)
		{
			float* Z = (float*)(buf+4*j);
			float val = *Z;
			fwrite(&val, sizeof(float), 1, fp);			
		}
		
		//fwrite(&buf, sizeof(float), width, fp);

		fwrite(&i, sizeof(int), 1, fp);
	}

	delete [] buf;
	fclose(fp);
}

void CDEM::writeASCII(const char* filename)
{
	this->init();

	FILE	*fp;
	fp = fopen(filename,"w");

	fprintf(fp, "%d\n", this->getHeight());
	fprintf(fp, "%d\n", this->getWidth());

	if ( this->getTFW()->getRefSys().getCoordinateType() == eGEOGRAPHIC )
	{
		fprintf(fp, "%.14lf\n", this->getgridX());
		fprintf(fp, "%.14lf\n", this->getgridY());
		fprintf(fp, "%.14lf\n", this->getminY());
		fprintf(fp, "%.14lf\n", this->getminX());
	}
	else
	{
		fprintf(fp, "%10.3lf\n", this->getgridX());
		fprintf(fp, "%10.3lf\n", this->getgridY());
		fprintf(fp, "%10.3lf\n", this->getminX());
		fprintf(fp, "%10.3lf\n", this->getminY());
	}


	float* Z;
	float val;

	int height = this->getHeight();
	int width = this->getWidth();

	unsigned char* buf = new unsigned char[width*this->pixelSizeBytes()];

	for(int i = 0; i < height; i++)
	{
		this->getRect(buf, i, 1, 0, width);

		for(int j=0; j < width; j++)
		{
			Z = (float*)(buf+4*j);
			val = *Z;

			fprintf(fp, " %10.3lf", val);
		}
		fprintf(fp, "\n");
	}

	delete  [] buf;

	fclose(fp);
}

void CDEM::writeASCIIVerticalFlip(const char* filename)
{
	this->init();

	int height = this->getHeight();
	int width = this->getWidth();

	FILE	*fp;
	fp = fopen(filename,"w");

	fprintf(fp, "%d\n", height);
	fprintf(fp, "%d\n", width);

	if ( this->getTFW()->getRefSys().getCoordinateType() == eGEOGRAPHIC )
	{
		fprintf(fp, "%.14lf\n", this->getgridX());
		fprintf(fp, "%.14lf\n", this->getgridY());
		fprintf(fp, "%.14lf\n", this->getminY());
		fprintf(fp, "%.14lf\n", this->getminX());
	}
	else
	{
		fprintf(fp, "%10.3lf\n", this->getgridX());
		fprintf(fp, "%10.3lf\n", this->getgridY());
		fprintf(fp, "%10.3lf\n", this->getminX());
		fprintf(fp, "%10.3lf\n", this->getminY());
	}

	float* Z;
	float val;

	unsigned char* buf = new unsigned char[width*this->pixelSizeBytes()];

	for(int i = 0; i < height; i++)
	{
		this->getRect(buf, height - 1 - i, 1, 0, width);

		for(int j=0; j < width; j++)
		{
			Z = (float*)(buf+4*j);
			val = *Z;

			fprintf(fp, " %10.3lf", val);
		}
		fprintf(fp, "\n");
	}

	delete [] buf;
	fclose(fp);
}

void CDEM::writeTIFF(const char* filename)
{
	CGDALFile gf;
	
	// set basic informations
	gf.setHeight(this->getHeight());
	gf.setWidth(this->getWidth());
	gf.setBpp(this->bpp);
	gf.setBands(1);
	CCharString str1 = "Intensity";
	CCharString str2 = "None";
	gf.setExportFileOptions(str1, str2, 75, true, false,false, "");
	gf.setTiled(true);

	gf.setTFW(&this->tfw);

	gf.write(filename,this);
};

void CDEM::init()
{
	if (this->isinitialized)
		return;

	this->getminZ();

	this->isinitialized = true;
}



void CDEM::interpolateHeight(CObsPoint *out, CObsPoint* in)
{
	double Z = this->getBLInterpolatedHeight((*in)[0], (*in)[1]);

	out->setLabel(in->getLabel());
	(*out)[0] = (*in)[0];
	(*out)[1] = (*in)[1];
	(*out)[2] = Z;
}


void CDEM::interpolateHeight(CXYZPointArray *out, CXYZPointArray* in)
{
	for( int i = 0; i < in->GetSize(); i++)
	{
		CObsPoint* inpoint = in->GetAt(i);
		CObsPoint* outpoint = out->Add();
		this->interpolateHeight(outpoint, inpoint);
	}
}

void CDEM::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CHugeImage::serializeStore(pStructuredFileSection);

	CToken* token = pStructuredFileSection->addToken();
	token->setValue("MinX", this->getminX());

    token = pStructuredFileSection->addToken();
	token->setValue("MinY", this->getminY());

	token = pStructuredFileSection->addToken();
	token->setValue("MinZ", this->getminZ());

    token = pStructuredFileSection->addToken();
	token->setValue("MaxZ", this->getmaxZ());

    token = pStructuredFileSection->addToken();
	token->setValue("gridX", this->getgridX());

    token = pStructuredFileSection->addToken();
	token->setValue("gridY", this->getgridY());

    token = pStructuredFileSection->addToken();
	token->setValue("sigmaZ", this->sigmaZ);

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->tfw.serializeStore(child);
}

void CDEM::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CHugeImage::serializeRestore(pStructuredFileSection);

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);

        if (child->getName().CompareNoCase("CTFW"))
        {
            this->tfw.serializeRestore(child);
        }
	}

	C2DPoint grid;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
        CCharString key = token->getKey();

		if(key.CompareNoCase("MinX"))
			this->Pmin.x = token->getDoubleValue();
		else if(key.CompareNoCase("MinY"))
			this->Pmin.y = token->getDoubleValue();
		else if(key.CompareNoCase("MinZ"))
			this->Pmin.z = token->getDoubleValue();
		else if(key.CompareNoCase("MaxZ"))
			this->Pmax.z = token->getDoubleValue();
		else if(key.CompareNoCase("gridX"))
			grid.x = token->getDoubleValue();
		else if(key.CompareNoCase("gridY"))
			grid.y = token->getDoubleValue();
		else if(key.CompareNoCase("sigmaZ"))		
			this->sigmaZ = float(token->getDoubleValue());
	}
	//addition >>
	if (this->tfw.getRefSys().getCoordinateType() == eGEOGRAPHIC)
	{
		this->Pmax.x = this->Pmin.x + (this->height - 1) * grid.x;
		this->Pmax.y = this->Pmin.y + (this->width - 1) * grid.y;
	}
	else if (this->tfw.getRefSys().getCoordinateType() == eGRID)
	{//<< addition
		this->Pmax.x = this->Pmin.x + (this->width - 1) * grid.x;
		this->Pmax.y = this->Pmin.y + (this->height - 1) * grid.y;
		//addition >>
	}
	//<< addition
}


void CDEM::reconnectPointers()
{
	CSerializable::reconnectPointers();
}

void CDEM::resetPointers()
{
	CSerializable::resetPointers();
}


bool CDEM::isModified()
{
    ///@todo when the points are serializable we can loop them... 
    return this->bModified;
	
}

void CDEM::initialize(const char *filename, const C2DPoint &pmin, 
					  const C2DPoint &pmax, const C2DPoint &gridS)
{
	this->Pmin.x = pmin.x;
	this->Pmax.x = pmax.x;
	this->Pmin.y = pmin.y;
	this->Pmax.y = pmax.y;
	this->Pmin.z = FLT_MAX;
	this->Pmax.z = -FLT_MAX;
	int w = int (floor((this->Pmax.x - this->Pmin.x) / gridS.x) + 1.0);
	int h = int (floor((this->Pmax.y - this->Pmin.y) / gridS.y) + 1.0);

	this->width  = w;
	this->height = h;
	this->bpp = 32;
	this->bands = 1;

	C2DPoint shift(this->Pmin.x, this->Pmax.y);
	this->setTFW(shift, gridS);
};

bool CDEM::interpolate(CALSDataSet &als, const char *filename, const C2DPoint &pmin, 
					   const C2DPoint &pmax, const C2DPoint &gridS, int N, int echo, int level,
					   eInterpolationMethod method, double maxDist, double minsquareDistForWeight)
{
	this->Pmin.x = pmin.x;
	this->Pmax.x = pmax.x;
	this->Pmin.y = pmin.y;
	this->Pmax.y = pmax.y;
	this->Pmin.z = FLT_MAX;
	this->Pmax.z = -FLT_MAX;
	int w = int (floor((this->Pmax.x - this->Pmin.x) / gridS.x) + 1.0);
	int h = int (floor((this->Pmax.y - this->Pmin.y) / gridS.y) + 1.0);
	if ((w <= 0) || (h <= 0)) return false;

	this->width  = w;
	this->height = h;

	C2DPoint shift(this->Pmin.x, this->Pmax.y);
	this->setTFW(shift, gridS);

	if ((gridS.x < FLT_EPSILON) || (gridS.y < FLT_EPSILON)) return false;

	if (!prepareWriting(filename, w, h, 32, 1)) return false;

	CALSPointCloudBuffer pointcloud(2);

	CImageBuffer buffer  (0, 0, this->tileWidth, this->tileHeight, 1, this->bpp, false);

	double yul = this->Pmax.y;

	C2DPoint tileExt(gridS.x * this->tileWidth, gridS.y * this->tileHeight);

	C3DPoint min, max;

	min.z =      -1.00;
    max.z =    1000.00;
	double overlap = (als.getPointDist().x > als.getPointDist().y) ? als.getPointDist().x : als.getPointDist().y;
	overlap *= 10;

	float minz, maxz;

	float pfact = (float) 85.0 / float(this->tilesDown * this->tilesAcross);


    for(int TR = 0; TR < this->height; TR += this->tileHeight, yul -= tileExt.y) 
	{
		double xul = this->Pmin.x;

		min.y = yul - tileExt.y - overlap;
		max.y = yul + overlap;

        for (int TC = 0; TC < this->width; TC += this->tileWidth, xul  += tileExt.x)
        {
			min.x = xul - overlap;
			max.x = xul + tileExt.x + overlap;

			if (pointcloud.get(als, min, max, level) <= 0) buffer.setAll(DEM_NOHEIGHT);
			else
			{
				int w = this->tileWidth;
				int h = this->tileHeight;
				if (TR + h > this->height) h = this->height - TR;
				if (TC + w > this->width ) w = this->width  - TC;
				if (method == eTriangulation)
				{
					pointcloud.interpolateRasterTriangulation(buffer.getPixF(), w, h, buffer.getDiffY(), xul, yul, gridS.x, gridS.y, minz, maxz, 2);
				}
				else
				{			
					if (maxDist <= FLT_EPSILON)
						pointcloud.interpolateRaster(buffer.getPixF(), w, h, this->tileWidth, xul, yul, 
													 gridS.x, gridS.y, N, echo, method, minz, maxz);
					else
						pointcloud.interpolateRaster(buffer.getPixF(), w, h, this->tileWidth, xul, yul, 
													 gridS.x, gridS.y, N, echo, method, minz, maxz, maxDist);
				}
				if (minz < this->Pmin.z) this->Pmin.z = minz;
				if (maxz > this->Pmax.z) this->Pmax.z = maxz;
			}

			this->dumpTile((unsigned char *) buffer.getPixF());
			this->updateHistograms(buffer);	
			
			if ( this->listener != NULL )
			{
				float pvalue = (float)((TR / this->tileHeight) * this->tilesAcross + (TC / this->tileWidth))* pfact;			
				this->listener->setProgressValue(pvalue);		
			}
		}
	}

	als.clear();

	this->isloaded = true;
	this->isgridded = true;
	this->isinitialized = true;

    bool ret = this->finishWriting();
		
	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(100.0);
	}

	return ret;
};


bool CDEM::interpolate(const CXYZPointArray &points, const char *filename, const C2DPoint &pmin, 
					   const C2DPoint &pmax, const C2DPoint &gridS, int N, 
					   eInterpolationMethod method, double maxDist, double minsquareDistForWeight)
{
	if (points.GetSize() < 1) return false;

	this->Pmin.x = pmin.x;
	this->Pmax.x = pmax.x;
	this->Pmin.y = pmin.y;
	this->Pmax.y = pmax.y;
	float zmin =  FLT_MAX;
	float zmax = -FLT_MAX;
	int w = int (floor((this->Pmax.x - this->Pmin.x) / gridS.x) + 1.0);
	int h = int (floor((this->Pmax.y - this->Pmin.y) / gridS.y) + 1.0);
	if ((w <= 0) || (h <= 0)) return false;

	this->width  = w;
	this->height = h;

	C2DPoint shift(this->Pmin.x, this->Pmax.y);
	this->setTFW(shift, gridS);

	if ((gridS.x < FLT_EPSILON) || (gridS.y < FLT_EPSILON)) return false;

	if (!prepareWriting(filename, w, h, 32, 1)) return false;

	CImageBuffer buffer  (0, 0, this->tileWidth, this->tileHeight, 1, this->bpp, false);

	double yul = this->Pmax.y;

	C2DPoint tileExt(gridS.x * this->tileWidth, gridS.y * this->tileHeight);

	CGenericPointCloud PtCld(points, 3);

//	CLinearPrediction predict(&PtCld, N, 1.0, 2, gridS.x);


	float pfact = (float) 85.0 / float(this->tilesDown * this->tilesAcross);

    for(int TR = 0; TR < this->height; TR += this->tileHeight, yul -= tileExt.y) 
	{
		double xul = this->Pmin.x;

        for (int TC = 0; TC < this->width; TC += this->tileWidth, xul  += tileExt.x)
        {
			int w = this->tileWidth;
			int h = this->tileHeight;
			if (TR + h > this->height) 
			{
				buffer.setAll(-9999.0);
				h = this->height - TR;
			}
			if (TC + w > this->width ) 
			{
				w = this->width  - TC;
				buffer.setAll(-9999.0);
			}
	
//			predict.interpolateRaster(buffer.getPixF(), w, h, buffer.getDiffY(), xul, yul, gridS.x, gridS.y, zmin, zmax);

			if (method == eTriangulation)
			{
				PtCld.interpolateRasterTriangulation(buffer.getPixF(), w, h, buffer.getDiffY(), xul, yul, gridS.x, gridS.y, zmin, zmax, 2);
			}
			else
			{
				if (maxDist <= FLT_EPSILON)
					PtCld.interpolateRaster(buffer.getPixF(), w, h, buffer.getDiffY(), xul, yul, gridS.x, gridS.y, N, method, zmin, zmax, 2, minsquareDistForWeight);
				else 
					PtCld.interpolateRaster(buffer.getPixF(), w, h, buffer.getDiffY(), xul, yul, gridS.x, gridS.y, N, method, zmin, zmax, maxDist, 2, minsquareDistForWeight);		    		
			}

			this->dumpTile((unsigned char *) buffer.getPixF());
			this->updateHistograms(buffer);	
			
			if ( this->listener != NULL )
			{
				float pvalue = (float)((TR / this->tileHeight) * this->tilesAcross + (TC / this->tileWidth))* pfact;			
				this->listener->setProgressValue(pvalue);		
			}
		}
	}

	this->Pmin.z = zmin;
	this->Pmax.z = zmax;
	
	this->isloaded = true;
	this->isgridded = true;
	this->isinitialized = true;

    bool ret = this->finishWriting();
		
	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(100.0);
	}

	return ret;
};


bool CDEM::cropTFW(const CTFW &TFW, double ulx, double uly, double Width, double Height)
{
	bool ret = TFW.gethasValues();
	if (ret)
	{
		this->tfw.copy(TFW);
		this->tfw.crop(int(ulx), int(uly), int(Width), int(Height));
		this->setTFW(this->tfw);
	}

	return ret;
}

bool CDEM::mergefromDEMs(vector<CDEM*> mergeDEMList)
{
	if ( mergeDEMList.size() < 1) return false;

	int nDEM = mergeDEMList.size();

	CFileName asciiName(*this->getFileName());

	this->addHUGExtension();
	
	bool okay = this->prepareWriting(this->getFileName()->GetChar(), this->width , this->height, 32, 1);

	this->setResamplingType(eNearestNeighbour);

	double xmin = this->getminX();
	double ymin = this->getminY();
	double gridy = this->getgridY();
	double gridx = this->getgridX();

	double y;
	double x;

	float newz = DEM_NOHEIGHT;

	int tileRows = this->getTileHeight();
	int tileCols = this->getTileWidth();
	unsigned long put;

	CImageBuffer tile(0, 0, tileCols, tileRows, 1, 32,true, -9999.0);
	
	float numberOfTileRows = (float)this->tilesAcross*this->tilesDown*tileRows ;
	float count =0;

	if (this->listener)
	{
		this->setMaxListenerValue(80.0);
	}

	for(int TR = 0; TR < this->height; TR += tileRows) 
	{
		for (int TC = 0; TC < this->width; TC += tileCols)
		{
			tile.setOffset(TC, TR);
			tile.setAll(DEM_NOHEIGHT);
			
			// fill tile
			for (int i = 0; i < tileRows && (TR+i) <= this->height; i++)
			{
				count +=1.0;

				for (int j = 0; j < tileCols && (TC+j) <= this->width; j++)
				{
					if ( this->GetTFW().getRefSys().getCoordinateType() == eGEOGRAPHIC )
					{
						x = xmin + (this->height -1 - (TR+i))*gridx;
						y = ymin + (TC+j)*gridy;
					}
					else
					{
						x = xmin + (TC+j)*gridx;
						y = ymin + (this->height -1 - (TR+i))*gridy;
					}

					int index = 0;

					while ( newz == DEM_NOHEIGHT && index < nDEM)
					{
						CDEM* currentDEM = mergeDEMList[index];
						newz = (float)currentDEM->getBLInterpolatedHeight(x, y);
						index++;
					}
					put = (unsigned long)(i*tileCols + j);
					tile.pixFlt(put) = newz;
					newz = DEM_NOHEIGHT;
				}

				if (this->listener)
				{
					this->listener->setProgressValue(count/numberOfTileRows*100.0);
				}
			}
			this->dumpTile(tile);
		}
	}
	
	if (this->listener)
	{
		this->setMaxListenerValue(100.0);
	}

	bool finishwriting = this->finishWriting();

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
	}

	/*
	this->refreshHistograms();

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
	}
	*/

	return true;
}

bool CDEM::differenceDEMs(CDEM* masterDEM, CDEM* compareDEM)
{
	CFileName asciiName(*this->getFileName());
	this->addHUGExtension();
	bool okay = this->prepareWriting(this->getFileName()->GetChar(), this->width , this->height, 32, 1);
	this->setResamplingType(eNearestNeighbour);

	double xmin = this->getminX();
	double ymin = this->getminY();
	double gridy = this->getgridY();
	double gridx = this->getgridX();

	double y;
	double x;

	int nDiff = 0;
	double sumZZ = 0;
	double sumDZ = 0;
	double sigmaZ =0;
	double meanDZ = 0;

	float masterz = DEM_NOHEIGHT;
	float comparez = DEM_NOHEIGHT;
	float newz = DEM_NOHEIGHT;

	int tileRows = this->getTileHeight();
	int tileCols = this->getTileWidth();
	unsigned long put;

	CImageBuffer tile(0, 0, tileCols, tileRows, 1, 32,true, DEM_NOHEIGHT);
	
	float numberOfTileRows = (float)this->tilesAcross*this->tilesDown*tileRows ;
	float count =0;

	if (this->listener)
	{
		this->setMaxListenerValue(80.0);
	}

	for(int TR = 0; TR < this->height; TR += tileRows) 
	{
		for (int TC = 0; TC < this->width; TC += tileCols)
		{
			tile.setOffset(TC, TR);
			tile.setAll(DEM_NOHEIGHT);
			
			// fill tile
			for (int i = 0; i < tileRows && (TR+i) <= this->height; i++)
			{
				count +=1.0;
				//y = ymin + (rows -1 - (TR+i))*gridy;

				for (int j = 0; j < tileCols && (TC+j) <= this->width; j++)
				{
					//x = xmin + (TC+j)*gridx;

					//////////////
					if ( this->GetTFW().getRefSys().getCoordinateType() == eGEOGRAPHIC )
					{
						x = xmin + (this->height -1 - (TR+i))*gridx;
						y = ymin + (TC+j)*gridy;
					}
					else
					{
						x = xmin + (TC+j)*gridx;
						y = ymin + (this->height -1 - (TR+i))*gridy;
					}
					/////////////

					// Modified by Mehdi to include corner-based interpolation strategy

 					if ( masterDEM->getgridX() <= compareDEM->getgridX())
					{
						masterz = (float)masterDEM->getBLInterpolatedHeight(x, y);
						comparez = (float)compareDEM->getBLInterpolatedHeight(x, y);
					}
					else if (masterDEM->getgridX() > compareDEM->getgridX())
					{
					 	masterz = (float)masterDEM->getBLInterpolatedHeight(x, y);
						double masterDEMgridSize;
						masterDEMgridSize = masterDEM->getgridX();
						comparez = (float)compareDEM->getBLInterpolatedHeight(x, y, masterDEMgridSize);
					}
				
					
					if ( masterz == DEM_NOHEIGHT || comparez == DEM_NOHEIGHT )
					{
						newz = -9999.0;
					}
					else
					{
						newz = comparez - masterz;

						nDiff++;
						sumZZ += newz*newz;
						sumDZ += newz;

					}

					put = (unsigned long)(i*tileCols + j);
					tile.pixFlt(put) = newz;
					newz = DEM_NOHEIGHT;
				}

				if (this->listener)
				{
					this->listener->setProgressValue(count/numberOfTileRows*100.0);
				}
			}

			this->dumpTile(tile);
		}
	}

	if (this->listener)
	{
		this->setMaxListenerValue(100.0);
	}

	bool finishwriting = this->finishWriting();

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
	}

	if ( nDiff > 2)
	{
		sigmaZ = sqrt(sumZZ/nDiff);
		meanDZ = sumDZ/nDiff;

		double SM =0;

		double tsm =0;

		/////////////////////////////

		unsigned char* buf = new unsigned char[this->getWidth()*this->pixelSizeBytes()];

		for (int r = 0; r < this->height; r++)
		{
			this->getLine(buf, r);

			for (int i = 0, j = 0; i < this->width; i++, j+=4)
			{
				float* fPixel = (float*)(buf+j);

				float currentZ = *fPixel;
				if (currentZ != DEM_NOHEIGHT)
				{
					tsm += (currentZ - meanDZ)*(currentZ - meanDZ);

				}
			}
		}

		delete [] buf;
		////////////////////////////////

		SM = sqrt(tsm/nDiff);

		this->setSigmaZ(float(SM));

		int stop =9;
	}

	return true;
}


bool CDEM::transformDEM(CDEM* srcDEM)
{
	CFileName asciiName(*this->getFileName());
	this->addHUGExtension();
	bool okay = this->prepareWriting(this->getFileName()->GetChar(), this->width , this->height, 32, 1);
	this->setResamplingType(eNearestNeighbour);

	double xmin = this->getminX();
	double ymin = this->getminY();
	double gridy = this->getgridY();
	double gridx = this->getgridX();

	double y;
	double x;

	float srcz = DEM_NOHEIGHT;
	float newz = DEM_NOHEIGHT;

	int tileRows = this->getTileHeight();
	int tileCols = this->getTileWidth();
	unsigned long put;

	CImageBuffer tile(0, 0, tileCols, tileRows, 1, 32,true, DEM_NOHEIGHT);
	
	float numberOfTileRows = (float)this->tilesAcross*this->tilesDown*tileRows ;
	float count =0;

	if (this->listener)
	{
		this->setMaxListenerValue(80.0);
	}


	CTrans3D* trans = CTrans3DFactory::initTransformation(this->GetTFW().getRefSys(), srcDEM->GetTFW().getRefSys());

	C3DPoint in;
	C3DPoint out;

	for(int TR = 0; TR < this->height; TR += tileRows) 
	{
		for (int TC = 0; TC < this->width; TC += tileCols)
		{
			tile.setOffset(TC, TR);
			tile.setAll(DEM_NOHEIGHT);
			
			// fill tile
			for (int i = 0; i < tileRows && (TR+i) <= this->height; i++)
			{
				count +=1.0;
				//y = ymin + (rows -1 - (TR+i))*gridy;

				for (int j = 0; j < tileCols && (TC+j) <= this->width; j++)
				{
					//x = xmin + (TC+j)*gridx;

					//////////////
					if ( this->GetTFW().getRefSys().getCoordinateType() == eGEOGRAPHIC )
					{
						x = xmin + (this->height -1 - (TR+i))*gridx;
						y = ymin + (TC+j)*gridy;
					}
					else
					{
						x = xmin + (TC+j)*gridx;
						y = ymin + (this->height -1 - (TR+i))*gridy;
					}
					/////////////

					in.x = x;
					in.y = y;

					trans->transform(out, in);

					srcz = (float)srcDEM->getBLInterpolatedHeight(out.x, out.y);

					if ( srcz == DEM_NOHEIGHT  )
					{
						newz = DEM_NOHEIGHT;
					}
					else
					{
						newz = srcz;
					}

					put = (unsigned long)(i*tileCols + j);
					tile.pixFlt(put) = newz;
					newz = DEM_NOHEIGHT;
				}

				if (this->listener)
				{
					this->listener->setProgressValue(count/numberOfTileRows*100.0);
				}
			}

			this->dumpTile(tile);
		}
	}
	
	if (this->listener)
	{
		this->setMaxListenerValue(100.0);
	}
	
	bool finishwriting = this->finishWriting();

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
	}


	return true;
}

bool CDEM::Operation (CDEM* srcDEM, double constValue, double nullval, string operation)
{
	CFileName asciiName(*this->getFileName());
	this->addHUGExtension();
	bool okay = this->prepareWriting(this->getFileName()->GetChar(), this->width , this->height, 32, 1);
	this->setResamplingType(eNearestNeighbour);

	double xmin = this->getminX();
	double ymin = this->getminY();
	double gridy = this->getgridY();
	double gridx = this->getgridX();

	double y;
	double x;

	float srcz = DEM_NOHEIGHT;
	float newz = DEM_NOHEIGHT;

	int tileRows = this->getTileHeight();
	int tileCols = this->getTileWidth();
	unsigned long put;

	CImageBuffer tile(0, 0, tileCols, tileRows, 1, 32,true, DEM_NOHEIGHT);

	float numberOfTileRows = (float)this->tilesAcross*this->tilesDown*tileRows ;
	float count =0;

	if (this->listener)
	{
		this->setMaxListenerValue(80.0);
	}


	CTrans3D* trans = CTrans3DFactory::initTransformation(this->GetTFW().getRefSys(), srcDEM->GetTFW().getRefSys());

	C3DPoint in;
	C3DPoint out;

	for(int TR = 0; TR < this->height; TR += tileRows) 
	{
		for (int TC = 0; TC < this->width; TC += tileCols)
		{
			tile.setOffset(TC, TR);
			tile.setAll(DEM_NOHEIGHT);

			// fill tile
			for (int i = 0; i < tileRows && (TR+i) <= this->height; i++)
			{
				count +=1.0;
				//y = ymin + (rows -1 - (TR+i))*gridy;

				for (int j = 0; j < tileCols && (TC+j) <= this->width; j++)
				{
					//x = xmin + (TC+j)*gridx;

					//////////////
					if ( this->GetTFW().getRefSys().getCoordinateType() == eGEOGRAPHIC )
					{
						x = xmin + (this->height -1 - (TR+i))*gridx;
						y = ymin + (TC+j)*gridy;
					}
					else
					{
						x = xmin + (TC+j)*gridx;
						y = ymin + (this->height -1 - (TR+i))*gridy;
					}
					/////////////

					in.x = x;
					in.y = y;

					trans->transform(out, in);

					srcz = (float)srcDEM->getBLInterpolatedHeight(out.x, out.y);
					
					string opGreater=">";
					string opsmaller="<";
					string opEqual="=";


					if (operation==opGreater) 
					{
						if (srcz > constValue || srcz == DEM_NOHEIGHT) 
							newz = DEM_NOHEIGHT;
						else 	newz = srcz;
					}

					else if (operation==opsmaller)
					{
						if (srcz < constValue || srcz == DEM_NOHEIGHT) 
							newz = DEM_NOHEIGHT;
						else 	newz = srcz;
					}

					else if (operation==opEqual)
					{
						if (srcz == constValue || srcz == DEM_NOHEIGHT) 
							newz = DEM_NOHEIGHT;
						else 	newz = srcz;
					}

					put = (unsigned long)(i*tileCols + j);
					tile.pixFlt(put) = newz;
					newz = DEM_NOHEIGHT;
				}

				if (this->listener)
				{
					this->listener->setProgressValue(count/numberOfTileRows*100.0);
				}
			}

			this->dumpTile(tile);
		}
	}

	if (this->listener)
	{
		this->setMaxListenerValue(100.0);
	}

	bool finishwriting = this->finishWriting();

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
	}


	return true;
}  

int CDEM::getTilePointVector(int row, int col, int level, vector<C3DPoint> &points)
{
	CHugeImage *hug = this;

	C2DPoint Delta(this->getgridX(), this->getgridY());

	for (int i = 0; i < level; ++i)
	{
		//hug = (CHugeImage *) hug->halfRes;
		//row/= 2;
		//col/= 2;
		Delta *= 2.0;
		//if (!hug) 
			//return -1;
	}

	float *zBuf = (float *) hug->getTile(row * hug->tileHeight, col * hug->tileWidth);

	if (!zBuf) 
		return -1;


	int npoints = 0;
 
	C3DPoint P0(this->Pmin.x + double(col * hug->tileWidth)  * this->getgridX(),
		        this->Pmax.y - double(row * hug->tileHeight) * this->getgridY(), 0.0);

	C3DPoint P;

	int index =0;

	int stepwidth = (int)pow(2.0,level);

//	points.resize(this->tileWidth*this->tileHeight/(stepwidth*stepwidth));


	float MinGrey = float(this->getminZ());
	float MaxGrey = float(this->getmaxZ());
	float greyFactor = 255.0f  / (MaxGrey - MinGrey);
	float scaledVal;


	for (int r = 0; r < hug->tileHeight; r += stepwidth, P0.y -= Delta.y)
	{
		P = P0;
		for (int c = 0; c < hug->tileWidth; c += stepwidth, P.x += Delta.x)
		{
			index = r*hug->tileHeight + c;
			if (zBuf[index] > DEM_NOHEIGHT)
			{
				P.z = zBuf[index];
				

				scaledVal = (float(P.z) - MinGrey) * greyFactor;
				if (scaledVal > 255.0) 
					scaledVal = 255;
				else if (scaledVal < 0.0) 
					scaledVal= 0;

				P.id = int(scaledVal);
				points[npoints] = P;
				++npoints;
			}
		}
	}

//	points.resize(npoints);
	return npoints;


	/*

	points.resize(this->tileWidth*this->tileHeight);

	int npoints = 0;
 
	C3DPoint P0(this->Pmin.x + double(col * hug->tileWidth)  * Delta.x,
		        this->Pmax.y - double(row * hug->tileHeight) * Delta.y, 0.0);

	C3DPoint P;

	for (int r = 0, i = 0; r < hug->tileHeight; ++r, P0.y -= Delta.y)
	{
		P = P0;
		for (int c = 0; c < hug->tileWidth; ++c, ++i, P.x += Delta.x)
		{
			if (zBuf[i] > DEM_NOHEIGHT)
			{
				P.z = zBuf[i];
				points[npoints] = P;
				++npoints;
			}
		}
	}

	points.resize(npoints);
	return npoints;
	*/
}
