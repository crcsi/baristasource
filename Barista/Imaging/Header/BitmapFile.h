// MUBitmapFile.h: interface for the CBitmapFile class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CBitmapFile__
#define __CBitmapFile__
#include "BitmapHeader.h"
#include "ImageFile.h"

#define BMP_TOTAL_HEADER_SIZE 54
#define BMP_COMMON_HEADER_SIZE 14

class CBitmapFile : public CImageFile  
{
private:
	CBitmapHeader	Header;
	long			FileSize;
	long			HeaderSize;
	bool			bHeaderRead;
	int				RecordLength;
	bool			bPadded;

	unsigned char*	pFileBuf;

	FILE* fp;
public:
	CBitmapFile();
	CBitmapFile(const char* pFileName);
	CBitmapFile(int nRows, int iCols, int nChannels, int nBitsPerChannel);
	virtual ~CBitmapFile();

    int getWidth();
    int getHeight();
    int getBpp();
    int bands();

    virtual bool readLine(unsigned char* pBuffer, int iRow);
    virtual bool writeLine(unsigned char* pBuffer, int iRow);

	virtual bool write(const char* filename, CBaseImage* image){return false;};

private:
	bool _ReadLine(int iRow);
};

#endif // __CBitmapFile__

