// MUImageFile.h: interface for the CImageFile class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CImageFile__
#define __CImageFile__

#include "stdio.h"
#include "BaseImage.h"
#include "ProgressHandler.h"
#include "Filename.h"
#include "RPC.h"
#include "TFW.h"


#define MAX_FILENAME 1024

class CTFW;
class CLookupTable;

class CImageFile : public CProgressHandler
{
public:
	enum eImageFileType {eGDALFile,
						 eRAWFile,
						 eASCIIDEMFile,
						 eTIFFFile,
						 eBITMAPFile,
						 eJPEGFile,
						 eJPEGTHUMBNAILFile,
						 eNEFFile,
						 eTIFFTHUMBNAILFile,
						 eG3DFile,
						 eAVNIR2File};
protected:
    char filename[MAX_FILENAME];
	CFileName hugeFileName;

    int nRows;
    int nCols;
    int nBands;
    int bpp;
	bool colorIndexed;
	CLookupTable* lut;
    
    int nextLineToWrite;

	// red/gray: 0, green: 1, blue: 2, alpha/ir: 3
	int colorChannel1;
	int colorChannel2;
	int colorChannel3;
	int colorChannel4;

	bool hasColourInformations;

	float Z0;
	float scale;
	float NoDataValue;
	float sigmaZ;

	bool interleaved;
	bool BigEndian;
	bool interpretation;
	bool toptobottom;
	
	eImageFileType type;

	bool tiled;

	CRPC rpc;
	CTFW* tfw;

public:

	
	
	
	CImageFile(eImageFileType imageType);
    CImageFile(const CImageFile& src);
    CImageFile(eImageFileType imageType,const char* pFileName);
   
	eImageFileType getFileType() const { return this->type;};
	
	virtual ~CImageFile();

    virtual int getWidth();
    virtual int getHeight();
    virtual int getBpp();
    virtual int bands();

    virtual void setWidth(int w);
    virtual void setHeight(int h);
    virtual void setBpp(int bpp);
    virtual void setBands(int nBands);

    virtual bool readLine(unsigned char* pBuffer, int iRow);
    virtual bool writeLine(unsigned char* pBuffer, int iRow);

    virtual bool write(const char* filename, CBaseImage* image) = 0;
    char* getFilename();
	//addition >>
	void setFileName(const char *fname){strcpy(this->filename,fname);}
	//<< addition
	bool getColorIndexed() const { return this->colorIndexed;}
	void setColorIndexed(bool indexed)  { this->colorIndexed = indexed;}

	void setLookupTable(const CLookupTable* lut);
	const CLookupTable* getLookupTable() const { return this->lut;}

	int  getColourInterpretation(int channel);
	void setColourInformations(int channel1,int channel2=0,int channel3=0,int channel4=0);

	void setSigmaZ(float sigmaZ) { this->sigmaZ = sigmaZ;}
	void setNoDataValue(float NoDataValue) { this->NoDataValue = NoDataValue;}
	void setScale(float scale) { this->scale = scale;}
	void setZ0(float Z0) { this->Z0 = Z0;}

	void setToptoBottom(bool toptobottom) { this->toptobottom = toptobottom;};
	bool getToptoBottom() { return this->toptobottom;};

	void setBigEndian(bool BigEndian) { this->BigEndian = BigEndian;};
	void setInterleaved(bool interleaved) { this->interleaved = interleaved;};
	void setInterpretation(bool interpretation) { this->interpretation = interpretation;};

	float getNoDataValue() {return this->NoDataValue;};
	float getScale() {return this->scale;};
	float getZ0() {return this->Z0;};
	float getSigmaZ() {return this->sigmaZ;};

	CFileName getHugeFileName()const;

	void setHugeFileName(CFileName filename);

	void setTFW(const CTFW* externalTfw);
	const CTFW* getTFW() const {return this->tfw;};

	//addition >>
	void setRPC(const CRPC extrpc){this->rpc.copy(extrpc);};
	//<< addition
	const CRPC& getRPC() {return this->rpc;};
	
	virtual bool closeFile(){return true;};

	void setTiled(bool tiled) {this->tiled = tiled;};

	virtual void setExportFileOptions(CCharString& channelCombination,
									CCharString& compression,
									int compressionQuality,
									bool exportGeoData,
									bool exportSeperatFile,
									bool bandInterleave,
									CFileName fileNameGeoData) {};

};

#endif


