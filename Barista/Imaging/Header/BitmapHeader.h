// BitmapHeader.h: interface for the CBitmapHeader class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CBitmapHeader__
#define __CBitmapHeader__

#include "Filename.h"

#include <ctime>

class CBitmapHeader  
{

protected:
	char			m_Format[5];
	long			m_Rows;
	long			m_Cols;
	unsigned short	m_Channels;
	unsigned short	m_nBitsPerChannel;
	double			m_tlx;
	double			m_tly;
	double			m_dx;
	double			m_dy;
	time_t			m_Creation;
	time_t			m_Update;

	bool			m_bRead;
	CFileName		m_FileName;

private:
	bool _Read();

public:
	CBitmapHeader(const char* pFileName);
	CBitmapHeader();

	virtual ~CBitmapHeader();

	long GetBufferSize();
	int GetRows();
	int GetCols();
	int GetNumberOfChannels();
	int GetNumberOfBitsPerChannel();
	void SetFileName(const char* pFileName);
};

#endif // __CBitmapHeader__

