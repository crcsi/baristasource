#ifndef __CZImage__
#define __CZImage__

#include "stdio.h"

#include "CharString.h"
#include "ZRasterHelper.h"

//#define __USE_FILE_OFFSET64
#include "sys/types.h"

#ifdef WIN32
#define file_offset fpos_t
#else
#define file_offset off_t
#endif

#include "ImageFile.h"
#include "BaseImage.h"

class CImageTile;

class CHistogram;
class CKernel;

// always square tiles 
#define TILE_SIZE 512 

class CZImage : public CBaseImage
{
public:

    CZImage(int w, int h, int bpp, int bands);

    CZImage(void);
    ~CZImage(void);

	bool isa(string& className) const 
	{
		if (className == "CZImage")
			return true;

		return false;
	};

	string getClassName() const {return string("CZImage");};

    bool openNew(int w, int h, int bpp, int bands);

	bool openR(CImageFile* imageFile);

  //  unsigned char* getTile(int r, int c);
    void setTile(unsigned char* buf, int r, int c);

    bool getTileZoomUp(CImageBuffer &dest, int r, int c, int zoomUp);
    bool getTileZoomDown(CImageBuffer &dest,int r, int c, int zoomDown);
    
    bool rotateLeft(CZImage* dest);
    bool rotateRight(CZImage* dest);
    bool rotate180(CZImage* dest);
	bool mirror(CZImage* dest);

    bool getImageTile(CImageTile** imageTile, int r, int c); 

    bool getRect(unsigned char* buf, int top, int height, int left, int width);
    bool getRect(unsigned char* buf, int top, int height, int left, int width, int destHeight, int destWidth);
  //  bool setRect(unsigned char* buf, int top, int height, int left, int width);
    bool getLine(unsigned char* buf, int row);
    //bool getPixel(unsigned char* buf, int r, int c);
    bool setPixel(unsigned char* buf, int r, int c);

    bool convolve(CZImage* src, CKernel* kernel);
    bool convolveFloat(CZImage* src, CKernel* kernel);
    bool convolve8Bit(CZImage* src, CKernel* kernel);

    CZImage* brighten(double factor);

    bool floatToGrey(CZImage* dest);
    bool floatToRGB(CZImage* src);

    int getBpp() const;
    int getBands() const;
    int getWidth() const;
    int getHeight() const;
    int getTileWidth() const;
    int getTileHeight() const;
    
    char* getFilename();
    int pixelSizeBytes();


    static int tileCreationCount;
    static int totalTileCount;
    int computeTotalTileCount(int w, int h);
//    CHistogram* getHistogram(int band = 0);
    void histogram(long* grey);

    void RGBtoHSV(float hsv[3], unsigned char rgb[3]);
    void HSVtoRGB(unsigned char rgb[3], float hsv[3]);
    
    //bool isa(const char* className);

    bool resample(CZImage* dest, int width, int height);

    bool isValid()
    {
        if (this->getWidth() > 0)
            return true;
        return false;
    };

    static bool isaTIFF(const char* name)
    {
        CCharString str(name);

        int index = str.Find(".TIF");
        if (index == str.GetLength() - 4)
            return true;

        index = str.Find(".tif");
        if (index == str.GetLength() - 4)
            return true;

        return false;
    };    

	static bool isaTIFFthumbnail3(const char* name)
    {
        CCharString str(name);

        int index = str.Find(".TIF.thumbnail3.jpg");
        if (index == str.GetLength() - (4+11+4))
            return true;

        index = str.Find(".tif.thumbnail3.jpg");
        if (index == str.GetLength() - (4+11+4))
            return true;

        return false;
    };   

	static bool isaNEF(const char* name)
    {
        CCharString str(name);

        int index = str.Find(".NEF");
        if (index == str.GetLength() - 4)
            return true;

        index = str.Find(".nef");
        if (index == str.GetLength() - 4)
            return true;

        return false;
    };

	static bool isaNEFthumbnail3(const char* name)
    {
        CCharString str(name);

        int index = str.Find(".NEF.thumbnail3.jpg");
        if (index == str.GetLength() - (4+11+4))
            return true;

        index = str.Find(".nef.thumbnail3.jpg");
        if (index == str.GetLength() - (4+11+4))
            return true;

        return false;
    };   

    static bool isaJPG(const char* name)
    {
        CCharString str(name);

		int index = str.Find("thumbnail3.jpg");
		
		if(index != -1)
			return false;

        index = str.Find(".JPG");
        if (index == str.GetLength() - 4)
            return true;

        index = str.Find(".jpg");
        if (index == str.GetLength() - 4)
            return true;

        return false;
    };

	static bool isaJPGthumbnail3(const char* name)
    {
        CCharString str(name);

        int index = str.Find(".JPG.thumbnail3.jpg");
        if (index == str.GetLength() - (4+11+4))
            return true;

        index = str.Find(".jpg.thumbnail3.jpg");
        if (index == str.GetLength() - (4+11+4))
            return true;

        return false;
    };

    static bool isaBMPthumbnail3(const char* name)
    {
        CCharString str(name);

        int index = str.Find(".BMP.thumbnail3.jpg");
        if (index == str.GetLength() - (4+11+4))
            return true;

        index = str.Find(".bmp.thumbnail3.jpg");
		if (index == str.GetLength() - (4+11+4))
            return true;

        return false;
    };

	static bool isaBMP(const char* name)
    {
        CCharString str(name);

        int index = str.Find(".BMP");
        if (index == str.GetLength() - 4)
            return true;

        index = str.Find(".bmp");
        if (index == str.GetLength() - 4)
            return true;

        return false;
    };

    bool crop(CZImage* dest, int top, int height, int left, int width)
    {
		unsigned char* buf = ZRasterHelper::alloc(height*width*this->pixelSizeBytes());// unsigned char[height*width*this->pixelSizeBytes()];

        if (!this->getRect(buf, top, height, left, width))
            return false;

        dest->openNew(width, height, this->getBpp(), this->getBands());

        dest->setRect(buf, 0, height, 0, width);

        delete buf;

        return true;
    };

    //unsigned char* getBits();


//	void deleteTiles(void);

protected:
    bool create(CImageFile* imageFile);

  //  static void SEEK(FILE* fp, file_offset* offset);
 //   static void TELL(FILE* fp, file_offset* offset);
 //   file_offset computeFileOffset(int r, int c);
    bool createHalfResTile(int r, int c);
    bool createHalfResRect(unsigned char* dest, unsigned char* src, int rectHeight, int rectWidth);
 //   void updateHistogram(unsigned char* lineBuf);
//   file_offset headerSize();

protected:

    /// good old FILE pointer
    //FILE* fp;

    /// filename of the *.hug file
    char filename[MAX_FILENAME];

    /// the version of the hug file
    //long version;
    /// bits per pixel (8-bits = byte) (16-bits = short) (32-bits = float)
/*
	long bpp; 
    /// number of bands (eg. RGB = 3)
    long bands;
    /// image width
    long width;
    /// image height
    long height;
    /// number of tiles across image
    long tilesAcross;
    /// number of tiles down image
    long tilesDown;
    /// tile width
    long tileWidth;
    /// tile height
    long tileHeight;

	/// pointer to the next image down in the pyramid (NULL if none)
    CZImage* halfRes;
    /// pointer to next image up in pyramid (NULL if none)
    CZImage* doubleSize;
*/
    
    /// @par transient members
    
    /// tile buffer array 
    CImageTile** tiles;
    
    /// offset in database to the image header
 //   file_offset headerOffset; 
    
    /// offset in file to image pixel data
 //   file_offset dataOffset;
    
    /// number of buffered tiles  
    long nBufferedTiles;
    
    /// the last (most recently) buffered tile
  //  int lastBufferedTile;


};

#endif

