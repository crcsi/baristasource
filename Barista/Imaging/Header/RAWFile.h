#ifndef __CRAWFile__
#define __CRAWFile__

#include "ImageFile.h"
#include "DEM.h"

//#define __USE_FILE_OFFSET64
#include "sys/types.h"

#ifdef WIN32
#define file_offset fpos_t
#else
#define file_offset off_t
#endif

class CBaseImage;

class CRAWFile : public CImageFile  
{
public:

	CRAWFile(const char* filename, float Zoffset = 0.0, float scale = 1.0);	

	virtual ~CRAWFile();

	int getWidth();
	int getHeight();
	int getBpp();
	int bands();

	double getgridX();
	double getgridY();
	double getminX();
	double getminY();

	bool open();
	
	bool readHeader();
	bool readSurfer6Header();
	bool readSurfer7Header();
	bool readLine(unsigned char* pBuffer, int iRow);

	bool read1BandLine(unsigned char* buffer, int iRow);
	bool read3BandLine(unsigned char* buffer, int iRow);
	bool read4BandLine(unsigned char* buffer, int iRow);

    bool write(const char* filename, CBaseImage* image);
	
	void setHeaderSize(int HeaderSize) { this->HeaderSize = HeaderSize;};
	int getHeaderSize() {return this->HeaderSize;};

	int getSizeofFilePixel() { return this->sizeofFilePixel;};

	void setSizeofFilePixel();
	void setSizeofDestPixel();

	void setFileBpp(int filebpp) { this->filebpp = filebpp;};
	int getFileBpp() { return this->filebpp;};

private: // Members

	FILE *fin;

	int sizeofFilePixel;
	int sizeofDestPixel;
	unsigned char* scanLineBuffer;

	unsigned char *linedata;
	unsigned char *rline;
	unsigned char *gline;
	unsigned char *bline;
	unsigned char *nline;

    /// flag if header info has been read
	bool	bHeaderRead;
	double xmin, xmax;
	double ymin, ymax;
	double zmin, zmax;
	double gridx, gridy;

	bool readfromEnd;
	
	int filebpp;
	int HeaderSize;

private: // Functions

	static void SEEK(FILE* fp, file_offset* offset);
    static void TELL(FILE* fp, file_offset* offset);
	file_offset computeFileOffset(int row);

	void createTFW();

};

#endif
