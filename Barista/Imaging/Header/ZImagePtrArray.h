#pragma once

#include "ZImage.h"
#include "MUObjectPtrArray.h"

class CZImagePtrArray : public CMUObjectPtrArray<CZImage>
{

public:

	CZImagePtrArray(int nGrowBy = 0);
	~CZImagePtrArray(void);
};
