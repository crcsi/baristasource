#ifndef __CASCIIDEMFile__
#define __CASCIIDEMFile__

#include "ImageFile.h"
#include "DEM.h"


class CBaseImage;
class CASCIIDEMFile : public CImageFile
{
public:
	CASCIIDEMFile();
	~CASCIIDEMFile(void);

	CASCIIDEMFile(const char* filename, float noDataValue = DEM_NOHEIGHT);

	bool readHeader();

    bool write(const char* filename, CBaseImage* image);

	int getWidth();
	int getHeight();

	double getgridX();
	double getgridY();
	double getminX();
	double getminY();

	bool readLine(unsigned char* pBuffer, int iRow);

	float *zdata;
	int convertFromXYZ(const char* xyzfilename);

private: // Functions

	bool readBaristaHeader();
	bool readARCInfoHeader();
	bool readSurferHeader();
	void close();

	void createTFW();

private: // Members

    /// flag if header info has been read
	bool			bHeaderRead;
	FILE*			fp;

	double xmin, xmax;
	double ymin, ymax;
	double zmin, zmax;
	double gridx, gridy;
};

#endif
