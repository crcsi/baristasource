#ifndef __CHugeImage__
#define __CHugeImage__

#include "stdio.h"

//#define __USE_FILE_OFFSET64
#include "sys/types.h"

#ifdef WIN32
#define file_offset fpos_t
#else
#define file_offset off_t
#endif

#include "ImageFile.h"
#include "Serializable.h"
#include "Filename.h"
#include "BaseImage.h"
#include "m_Roi.h"
#include "TransferFunction.h"

//addition >>
//#include <cv.h>
//#include <cxcore.h>
//#include <highgui.h>
//using namespace cv;
//<< addition

class CImageTile;
class CHistogram;
class CXYContourArray;
class CXYPointArray;
class CImageRegistration;
class CKernel;
class CImageBuffer;
class FeatureExtractionParameters;
class CXYPolyLineArray;
class CTrans2D;
class Matrix;
class SymmetricMatrix;
class CLabelImage;
class CHistogramFlt;
class CWallisFilter;

// always square tiles
#define TILE_SIZE 512

class CHugeImage : public CSerializable, public CBaseImage
{
public:
    CHugeImage(void);
    ~CHugeImage(void);

	virtual bool isDEM() const { return false; };

	//! create new image from CImageFile class
    bool open(CImageFile* imageFile, bool forceRecreate = false);

	//! open existing huge image
    bool open(const char* filename, bool retrieve);


	// public functions to query image data
	// ------------------------------------

	//! query image tile at requested zoom factor, the image buffer will be resized
	bool getImageTile(CImageBuffer& imageBuf,int tileRow,int tileCol,float zoomFactor);

	//! query image data at level 0, image buffer defines extents 
    bool getRect(CImageBuffer &buffer);

	//! query image data at pyramid level 0 from rect, image buffer will be resized
    bool getRect(CImageBuffer &buffer, int top, int height, int left, int width);

	//! query image data at pyramid level 0 transforming original image data using trafo, image buffer defines extents to be transformed
    bool getRect(CImageBuffer &buffer, const CTrans2D *trafo, eResamplingType resType);

	//! query image data at requested pyramid level, image buffer defines extents 
    bool getRect(CImageBuffer &buffer, int level);

	//! query image data from rect and resample to destHeight and destWidth, pyramid level depends on output size
	bool getRect(CImageBuffer &buffer, int top, int height, int left, int width, int destHeight, int destWidth);

	//! query image data, buf needs to be allocated already with the correct extents 
    bool getRect(unsigned char* buf, int top, int height, int left, int width);

	//! query image data, buf needs to be allocated already with the correct extents,  
    bool getRect(unsigned char* buf, int top, int height, int left, int width, int destHeight, int destWidth);






	bool readIntensityHistogram();

	void writeIntensityHistogram();

	void getIntensityHistogramName(CFileName& intName) const;

	double getCoordOffset(int pyrLevel) const;


    virtual bool setRect(unsigned char* buf, int top, int height, int left, int width);
    bool setRect(const CImageBuffer &buffer);

    bool getLine(unsigned char* buf, int row);
    bool getPixel(unsigned char* buf, int r, int c);
    bool setPixel(unsigned char* buf, int r, int c);


	void m_RGBtoHSI( float r, float g, float b, float *h, float *s, float *i );

	void m_HSItoRGB( float *r, float *g, float *b, float h, float s, float i);

	bool makeISHImages(CHugeImage* IImage, CHugeImage* SImage, CHugeImage* HImage);

	void m_RGBtoYIQ( float r, float g, float b, float *y, float *i, float *q );

	bool makeYIQImages(CHugeImage* YImage, CHugeImage* IImage, CHugeImage* QImage);
	//addition >>
	bool RGBtoGrey(CHugeImage* Grey);
	bool computeEntropy(CHugeImage* entropy);
	bool detectAndTrackEdges(CXYContourArray *xycontour, CImageBuffer *edgeBuf, int minLength);
	bool computeEdgeOrientation(CHugeImage* edgeOrientation, CHugeImage* edgeImg, float resImage);
	//bool computeEntropyAndEdgeOrientation(CHugeImage* entropy, CHugeImage* edgeOrientation, CHugeImage* edgeImg);
	void setBufferOfContour(CImageBuffer &oriLineX, CImageBuffer &oriLineY, CXYContour *contour);
	void extendContour(CImageBuffer oriLineX, CImageBuffer oriLineY, CImageBuffer &extLineX, CImageBuffer &extLineY, int W);
	void contractContour(CImageBuffer smoothLineX, CImageBuffer smoothLineY, CImageBuffer &conSmoothX, CImageBuffer &conSmoothY, int W);
	//IplImage *convertToOpenCVimage();
	//<< addition
    bool panSharpen(CHugeImage* rgb, CHugeImage* pan, float correctIR, bool PCA, const CTrans2D *trafo, eResamplingType resType);

    bool resample(const char *fileName, long width, long height, CHugeImage *input,
		          const CTrans2D &trafo, eResamplingType resType);

	void getCovar(SymmetricMatrix &Covariance);

	bool imageRegistration(CHugeImage* rgb, CHugeImage* pan);

    bool computeVegetationIndex(CHugeImage *rgbi, CHugeImage* sigma, bool NDVI = true);

    bool cropImage(const char *HugFilename, CHugeImage *input, int offsetX, int offsetY, int newWidth, int newHeight);

    bool mergeImageBands(const char *filename, CHugeImage *r, CHugeImage *g, CHugeImage *b, CHugeImage *IR);

	bool writeLabelImage(const char *Filename, const CLabelImage &labelImage);

	bool dumpImage(const char *Filename, const CImageBuffer &Image, CLookupTable *LUT);

	int extractEdgesLink(CXYContourArray *xycont);

	bool regionGrowing(const C2DPoint &p, CXYPolyLineArray *xyedges);

    bool createIntensity(CHugeImage* IImage, CHugeImage* pan, int kernalSize);

    bool convolve(CHugeImage* src, CKernel* kernel);
    bool convolveFloat(CHugeImage* src, CKernel* kernel);
    bool convolve8Bit(CHugeImage* src, CKernel* kernel);

    bool floatToGrey(CHugeImage* src);
    bool floatDEMToGrey(CHugeImage* src, float max, float min, bool showContours);
    bool floatToRGB(CHugeImage* src);
	bool GreyToBinary(CHugeImage* src, double Level);

    virtual int getBpp() const;
    virtual int getBands() const;
    virtual int getWidth()const;
    virtual int getHeight()const;
    virtual int getTileWidth()const;
    virtual int getTileHeight()const;

    CFileName* getFileName();
	virtual const char *getFileNameChar() const;

    void setFileName(const char* filename);
	void addHUGExtension();
	void addHUGExtension(const char* filename);
	

    int pixelSizeBytes();

	m_Roi* getRoiPtr();

	void setRoi(int ox,int oy,int nx,int ny,int flg=DOUBLET_OFF);

	int  RadiometricEnhancement(WallisParam par);

    static int tileCreationCount;
    static int totalTileCount;
    int computeTotalTileCount(int h, int w);
    
	CHistogram* getIntensityHistogram() {return this->intensityHistogram;};

    void RGBtoHSV(float hsv[3], unsigned char rgb[3]);
    void HSVtoRGB(unsigned char rgb[3], float hsv[3]);

	string getClassName() const
	{
		return string ("CHugeImage");
	};

	bool isa(string& className) const
	{
		if (className == "CHugeImage")
			return true;

		return false;
	};

    // CSerializable
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    bool isModified();

	// for setting image channel display
	void setRChannel(int rchannel);
	void setGChannel(int gchannel);
	void setBChannel(int bchannel);
	void setResamplingType(eResamplingType EResamplingType);
	void setChannels(int r, int g, int b);

	int getRChannel() const;
	int getGChannel() const;
	int getBChannel() const;
	eResamplingType getResamplingType() const { return resamplingType; };

	void setDefaultChannels();

	void refreshHistograms();

	void initTiles(int iNumber);

	void resetTiles();

	C2DPoint getCentre() const { return C2DPoint(double(this->width) * 0.5, double(this->height) * 0.5); }


	void closeHugeFile();

	bool prepareWriting(const char* filename, int w, int h, int bpp, int bands);

	bool dumpTile(const CImageBuffer &tile);

	bool finishWriting();

	long getTileBytes() const { return this->tileWidth*this->tileHeight * this->pixelSize; };

	void setProgressValue(float p);

	bool getColorIndexed() const { return this->transferFunction ? this->transferFunction->isColorIndexed() : false;}

	const CTransferFunction* getTransferFunction() const;

	CTransferFunction* getTransferFunction();

	void setTransferFunction(CTransferFunction* transferFunction, bool copyFunction,bool deleteOldFunction);


	void setLookupTable(const CLookupTable &lookup);

	bool addLookupTable(const char* filename);


	bool flip(bool vertical, bool horizontal = false);

	void changeBrightness(int value);

	int getBrightness() const;
	
	bool computeStatistics(vector<double>& avg,vector<double>& rms,vector<double>& min,vector<double>& max,vector<double>& dev,CHistogramFlt& hist,int startRow,int startCol,int height, int width);

	bool computeIntensityHistogram(int pyramidLevel);

	bool computeMeanAndStdDev(doubles& mean,doubles& stdDev,int& nParametersX,int& nParametersY,int windowSizeX,int windowSizeY,int rectTop,int rectHeight,int rectLeft,int rectWidth);

	bool createFromHugeImage(CHugeImage* oldHugeImage,bool applyTransferFunction);

	bool computeHistogramsForROI(int rectTop,int rectHeight,int rectLeft,int rectWidth,vector<CHistogram*>& histograms,CHistogram& intensityHistogram);

	CWallisFilter* createWallisFilter(int windowSizeX,
									  int windowSizeY,
									  double contrastParameter, 
								      double brightnessParameter,
								      double mean,
								      double stdDev,
									  bool writeParametersToFile);

	CHugeImage* getPyramidLevel(int level);

protected:

	bool getTileBuf(int r, int c, CImageBuffer &buffer);

    void setTile(unsigned char* buf, int r, int c);

    bool getImageTile(CImageTile** imageTile, int r, int c);

    bool getTileZoomUp(CImageBuffer &dest, int r, int c, int zoomUp);

    bool getTileZoomDown(CImageBuffer &dest,int r, int c, int zoomDown);



	static inline double access8Bit_1Band(unsigned char* buf,int index)
	{
		return double(buf[index]);
	};

	static inline double access16Bit_1Band(unsigned char* buf,int index)
	{
		return double(((unsigned short*)buf)[index]);
	};

	static inline double access32Bit_1Band(unsigned char* buf,int index)
	{
		return double(((float*)buf)[index]);
	};

	static inline double access8Bit_MultiBand(unsigned char* buf,int index)
	{
		return (double(buf[index])+double(buf[index+1])+double(buf[index+2]))*0.3333333333;
	};

	static inline double access16Bit_MultiBand(unsigned char* buf,int index)
	{
		return (double(((unsigned short*)buf)[index])+double(((unsigned short*)buf)[index+1])+double(((unsigned short*)buf)[index+2])) *0.3333333333;
	};
	 

	bool dumpTile(unsigned char *tile);

	bool updateHistograms (const CImageBuffer &buf);

	bool computeIntensityHistogram(CHugeImage* himage);

	int computeImageLevel(int maxSize);

	

	bool openOnly(const char* filename, int w, int h, int bpp, int bands);

    bool retrieve(FILE*fp);
    bool create(CImageFile* imageFile);
	bool createEmpty();

	bool createSubImages(bool empty);
	bool createSubImage();

	bool requiresSubImage() const;

	file_offset getSubImageOffset() const;

    static void SEEK(FILE* fp, file_offset* offset);
    static void TELL(FILE* fp, file_offset* offset);
    file_offset computeFileOffset(int r, int c);
    bool createHalfResTile(int r, int c);
    void updateHistogram(unsigned char* lineBuf);
    file_offset headerSize();

	void initIntensityHistogram();

	void deleteIntensityHistogram();

	void deleteTransferFunction();

	void initDefaultTransferFunction();

	void deleteTiles();

	void writeHistograms();

	void initAndWriteHeader();

	CHugeImage* getLevelNUllImage(int& zoomDownFactor);


protected:

	void fillSublevelBufferMovingAverage(int R, int C, unsigned char *dest);
	void fillSublevelBufferNearestNeighbour(int R, int C, unsigned char *dest);

	int pixelSize;
    /// good old FILE pointer
    FILE* fp;

    /// filename of the *.hug file
	CFileName filename;

	//bool colorIndexed;
	//CLookupTable* lut;
	CTransferFunction* transferFunction;

    /// filename of the *.lut file
	//CFileName lutname;

    /// the version of the hug file
    long version;

	CHistogram* intensityHistogram;

    /// @par transient members

    /// tile buffer array
    CImageTile** tiles;

    /// offset in database to the image header
    file_offset headerOffset;

    /// offset in file to image pixel data
    file_offset dataOffset;

    /// number of buffered tiles
    long nBufferedTiles;

    /// the last (most recently) buffered tile
    int lastBufferedTile;

    /// an optional progress listener
//    CImagingProgressListener* listener;

	m_Roi   *roi;

	int RChannel;
	int GChannel;
	int BChannel;

	eResamplingType resamplingType;

	int Brightness;
};

#endif

