#pragma once
#include "HugeImage.h"
#include "Serializable.h"

#include "Filename.h"
#include "XYZPointArray.h"
#include "TFW.h"
#include "3DPoint.h"
#include "XYZPolyLineArray.h"
#include "GenericPointCloud.h"

class CALSDataSet;
class CXYContour;
class CXYPolyLine;
class CXYZPolyLine;
class doubles;


class CDEM : public CHugeImage
{
  protected:

 //   CFileName FileName;

    bool isloaded;
    bool isgridded;
	bool isinitialized;
	C3DPoint Pmin;
	C3DPoint Pmax;
	float sigmaZ;

	CXYZPolyLineArray demBorder3DLines;

	CTFW tfw;

  public:

	CDEM(void);
	~CDEM(void);
    CDEM(CDEM* src) {assert(false);};
    CDEM(const CDEM& src) {assert(false);};

	virtual bool isDEM() const { return true; };

	void initialize(const char *filename, const C2DPoint &pmin, 
					const C2DPoint &pmax, const C2DPoint &gridS);

	bool interpolate(CALSDataSet &als, const char *filename, const C2DPoint &pmin, 
					 const C2DPoint &pmax, const C2DPoint &gridS, int N, int echo,
					 int level, eInterpolationMethod method, double maxDist, double minsquareDistForWeight);

	bool interpolate(const CXYZPointArray &points, const char *filename, const C2DPoint &pmin, 
					 const C2DPoint &pmax, const C2DPoint &gridS, int N, eInterpolationMethod method, 
					 double maxDist, double minsquareDistForWeight);

	string getClassName() const
	{
		return string ("CDEM");
	};

	bool isa(string& className) const
	{
		if (className == "CDEM")
			return true;

		return false;
	};

//    CFileName* getFileName();
 //   void setFileName(const char* filename);
    CFileName  getItemText();

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    bool isModified();

    void load(bool initDEMBorder= true);
    bool getisloaded();
    void setisloaded(bool loadstatus);

    bool getisgridded();
    int getNumPoints ();

	float getSigmaZ() const { return this->sigmaZ; };

	void setSigmaZ(float sigma);

    double getminZ();
    double getmaxZ();
    double getmaxX() const { return this->Pmax.x; };
	double getmaxY() const { return this->Pmax.y; };
	double getminX() const { return this->Pmin.x; };
    double getminY() const { return this->Pmin.y; };
    double getgridX() const;
    double getgridY() const;
	double getBLInterpolatedHeight (double X, double Y);

	C3DPoint getPMin() const { return this->Pmin; };
	C3DPoint getPMax() const { return this->Pmax; };

	double getBLInterpolatedHeight (double X, double Y, float &sigma);
    double getBLInterpolatedHeight (double X, double Y, double gridSizeMaster);
	double getBLInterpolatedHeight (double X, double Y, double LLX, double LLY, double LRX, double LRY,
		                                                double ULX, double ULY, double URX, double URY);

	void interpolateHeight(CXYZPointArray* out, CXYZPointArray* in);
	void interpolateHeight(CObsPoint* out, CObsPoint* in);

	CXYZPolyLineArray* getBorder();
	bool initBorder();
	//addition >>
	bool initOutsideBorder();
	float getNearestMeanHeight(int r, int c);
	void addNewPoint(CXYZPolyLine *newpoly, CXYPoint  p);
	//void loadOutside(bool initOutsideDEMBorder= true);
	//<< addiiton
	void setTFW(const CTFW &tfw);

	void setTFW(const C2DPoint &shift, const C2DPoint &gridSize);

	bool cropTFW(const CTFW &TFW, double ulx, double uly, double width, double height);

	bool mergefromDEMs(vector<CDEM*> mergeDEMList);
	bool differenceDEMs(CDEM* masterDEM, CDEM* compareDEM);


	bool transformDEM(CDEM* srcDEM);
	bool Operation(CDEM* srcDEM, double constValue, double nullval, string operation);

	const CTFW &GetTFW() const { return this->tfw; };
	CTFW* getTFW() {return &this->tfw;};
	void resetSensorModel(CSensorModel* model){ model->sethasValues(false);};

	void init();

	void writeG3D(const char* filename);
	void writeASCII(const char* filename);
	void writeTIFF(const char* filename);
	void writeASCIIVerticalFlip(const char* filename);

	void worldToPixel(C3DPoint &pixel, const C3DPoint &world) const;
	void worldToPixel(C2DPoint &pixel, const C3DPoint &world) const;

	void pixelToWorld(C3DPoint &world, const C3DPoint &pixel) const;
	void pixelToWorld(C3DPoint &world, const C2DPoint &pixel) const;

	int getTilePointVector(int row, int col, int level, vector<C3DPoint> &points);

  protected:

	double getBLInterpolatedHeight(double X, double Y, double &g00, double &g10, double &g01, double &g11);

    bool extractOneLine(CImageBuffer &edgebuf, CXYContour &poly, doubles &Heights);
    void markEdgePixels(float *rowbuf1, float *rowbuf2, CImageBuffer &edgebuf, int row);
    void markEdgePixels(float *rowbuf, CImageBuffer &edgebuf, int row);

};
