#pragma once
#include "memory.h"

class ZRasterHelper
{
public:
	ZRasterHelper(void);

	~ZRasterHelper(void);

	static unsigned char* alloc(int size)
	{
		// ensure 4-byte alignment
		int intbytes = sizeof(int); // 4 ??
		return new unsigned char [intbytes * ((size + intbytes - 1) / intbytes)];
	};

	static void memcpy(unsigned char* dst, const unsigned char* src, int size)
	{
		::memcpy(dst, src, size);
		
		/*
		int* s = (int*)src;
		int* d = (int*)dst;

		//int len = (size+3)/4;

		while (size >=4)
		{
			*d++ = *s++;
            size -= 4;
		}
		*/
	};
};
