#ifndef __CTiffFile__
#define __CTiffFile__

#include "ImageFile.h"
#include "tiffio.h"
#include "ImageTile.h"

class CBaseImage;
class CTiffFile : public CImageFile  
{
public:
	CTiffFile();

	CTiffFile(const char* filename);

    bool write(const char* filename, CBaseImage* image);

//	CTiffFile(const char* pFilename, int Rows, int Cols, int Channels, int BitsPerChannel);
	virtual ~CTiffFile();

	int getWidth();
	int getHeight();
	int getBpp();
	int bands();

	bool readLine(unsigned char* pBuffer, int iRow);

	bool writeLine(unsigned char* pBuffer, int iRow);

	int getScanLineSize();

	unsigned char* getTile(int r, int c); // gets the tile that include this r, c

    bool writeTile(unsigned char* buf, int r, int c);
	void setTiledWrite(bool tiled);
	void setUnTiledWrite();

private: // Functions

	bool _readTiledLine(unsigned char* buffer, int iRow);
	bool _read3BandLine(unsigned char* buffer, int iRow);
	bool _read4BandLine(unsigned char* buffer, int iRow);


	bool _readHeader();
	void _close();
	bool _seekToImageFD();

	bool _writeHeader();
	bool _directRead(unsigned char* buffer, int iRow);

private: // Members

    /// flag if header info has been read
	bool			bHeaderRead;

    // the TIFF pointer
	TIFF*			tif;

	FILE*			fp;

    /// tile buffer (used if the file was tiled)
	CImageTile**	tiles;

    /// the number of tiles (if any) in the tile buffer
	int nBufferedTiles;
	unsigned char* tileBuffer;
	int lastBufferedTile;

	uint32 tileWidth;
	uint32 tileHeight;
	int tilesAcross;
	int tilesDown;
	int tileByteSize;
	uint16 planarConfig;
	uint16 scanLineSize;
	uint16 stripOffset;

	unsigned char* scanLineBuffer;
	unsigned char* frameBuffer;
	bool tiled;
	bool tiledWrite;
};

#endif


