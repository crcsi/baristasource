#ifndef __CGDALFile__
#define __CGDALFile__

#include "ImageFile.h"
#include "LookupTable.h"
#include "tiffio.h"
#include "Filename.h"

class CBaseImage;
class CImageTile;
class GDALDataset;
class CImageBuffer;

class CGDALFile : public CImageFile  
{
public:
	CGDALFile();
	CGDALFile(const CGDALFile& src);
	CGDALFile(const char* filename);

    bool write(const char* filename, CBaseImage* image);

    bool writeTIFF(const char* filename, const CImageBuffer &image);
	
	void readLookupTableInfo();
	void writeLookupTableInfo();

	virtual ~CGDALFile();

	virtual bool closeFile();

	int getWidth();
	int getHeight();
	int getBpp();
	int bands();

	bool readFileInformation();
	float getNoDataValue(int band);

	bool readLine(unsigned char* pBuffer, int iRow);

	bool getHasHeader() { return this->bHeaderRead;};

	unsigned char* getTile(int r, int c); // gets the tile that include this r, c

	virtual void setExportFileOptions(CCharString& channelCombination,
									CCharString& compression,
									int compressionQuality,
									bool exportGeoData,
									bool exportSeperatFile,
									bool bandInterleave,
									CFileName fileNameGeoData);

    bool getRect(CImageBuffer &buffer);

protected: // functions
	bool writeGeoData(CBaseImage* image, GDALDataset* dataSet);
	bool writeData  (CBaseImage* image,GDALDataset* dataSet,bool tiled);
	bool write1Band (CBaseImage* image,GDALDataset* dataSet,bool tiled);
	bool write3Bands(CBaseImage* image,GDALDataset* dataSet,bool tiled);
	bool write4Bands(CBaseImage* image,GDALDataset* dataSet,bool tiled);


	bool read1BandLine(unsigned char* buffer, int iRow,unsigned int numberOfRowsToRead = 1);
	bool read3BandLine(unsigned char* buffer, int iRow);
	bool read4BandLine(unsigned char* buffer, int iRow);
	bool readTiledLine(unsigned char* buffer, int iRow);

	bool readTileWithModifiedJPEGLib(int tileCol, int tileRow,void* buffer);

	bool readHeader();
	

	void copyData(const float* src, float* dest,int width,int height,int channel,int offsetSrc,int offsetDest,bool intensity=false)
	{

		int maxSrc = width*height*offsetSrc;
		int maxDest = width*height*offsetDest;
		if (intensity)
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = (float)(((float)src[iSrc] + (float)src[iSrc+1] + (float)src[iSrc+2])/3.0);
		else
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = src[iSrc + channel];
	};


	void copyData(const unsigned char* src,unsigned char* dest,int width,int height,int channel,int offsetSrc,int offsetDest,bool intensity=false)
	{

		int maxSrc = width*height*offsetSrc;
		int maxDest = width*height*offsetDest;
		if (intensity)
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = (unsigned char)(((float)src[iSrc] + (float)src[iSrc+1] + (float)src[iSrc+2])/3.0);
		else
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = src[iSrc + channel];
	};


	void copyData(const unsigned short* src,unsigned short* dest,int width,int height,int channel,int offsetSrc,int offsetDest,bool intensity=false)
	{
		int maxSrc = width*height*offsetSrc;
		int maxDest = width*height*offsetDest;		

		if (intensity)
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = (unsigned short)(((float)src[iSrc] + (float)src[iSrc+1] + (float)src[iSrc+2])/3.0);
		else
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = src[iSrc + channel];
	};

	void copyData(	const unsigned short* src,   // source buffer
					unsigned char* dest,		// dest buffer
					int width,					// width of the buffers
					int height,					// height of the buffers
					int channelSrc,				// channel selects one band if src has more then 1 band, 
					int channelDest,			// channel to store the band
					int offsetSrc,				// offset between two pixels to be copied (1 -> copy every pixel, use 4 in combination with channel!)
					int offsetDest,             // offset between two pixels to be copied (1 -> copy every pixel, use 4 in combination with channel!)
					CLookupTable& lt,			// lookuptable to convefrt from 16 to 8 bit
					bool intensity=false)		// compute intensity and copy to the red channel (first band)
	{
		int maxSrc = width*height*offsetSrc;
		int maxDest = width*height*offsetDest;
		if (intensity)
			for (int iSrc=0,iDest=0; iSrc < maxSrc  && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = (unsigned char)lt[(unsigned short)(((float)src[iSrc] + (float)src[iSrc+1] + (float)src[iSrc+2])/3.0)];
		else
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest+ channelDest] = (unsigned char)lt[src[iSrc + channelSrc]];
	};

protected: // Members
    /// flag if header info has been read
	bool			bHeaderRead;
	GDALDataset* gdal;

	int sizeofpixel;

	float NoData1;
	float NoData2;
	float NoData3;
	float NoData4;

	unsigned char* scanLineBuffer;

	unsigned char *linedata;
	unsigned char *rline;
	unsigned char *gline;
	unsigned char *bline;
	unsigned char *nline;

    /// tile buffer (used if the file was tiled)
	CImageTile**	tiles;

    /// the number of tiles (if any) in the tile buffer
	int nBufferedTiles;
	unsigned char* tileBuffer;
	int lastBufferedTile;

	unsigned int tileWidth;
	unsigned int tileHeight;
	int tilesAcross;
	int tilesDown;
	int tileByteSize;


	CCharString channelExport;
	CCharString compressionExport;
	int compressionQualityExport;
	bool exportGeoData;
	bool exportSeperatFile;
	bool bandInterleave;
	CFileName fileNameGeoData;

	bool needsModifiedJPEGLib;
	TIFF* tif;
};

#endif
