#ifndef __AVNIR2File__
#define __AVNIR2File__

#include "GDALFile.h"
#include "Polynomial.h"


class CAVNIR2File : public CGDALFile  
{
public:

	CAVNIR2File();
	
	CAVNIR2File(const CGDALFile& gdal);

	CAVNIR2File(const char* filename);

	virtual ~CAVNIR2File();

	virtual bool readLine(unsigned char* pBuffer, int iRow);
	
	void initOffsetFunction(double theta);

	virtual int getHeight();

protected:


	vector<float> ccdPos;
	vector<int> offsetRow;

	unsigned int offsetSecondPixel;

	CPolynomial offsetFunction;

	unsigned char* avnirBuffer;

	int firstRowOffset;
	unsigned int sizeOffset;

};

#endif
