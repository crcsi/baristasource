#pragma once
#include "ZObject.h"
#include "LookupTable.h"

#ifndef NULL
#define NULL (void*)0
#endif


#ifndef Min 
#define Min(a,b) (((a) < (b) ) ? (a) : (b))
#endif

class CImageFile;
class CImageTile;
class CImageBuffer;
class CHistogram;

#define	DEM_NOHEIGHT -9999.0

#include "ProgressHandler.h"

class CBaseImage : public ZObject, public CProgressHandler
{
	// @@@ these were commented cronk march 2007
	friend class CZImage;
	//friend class CHugeImage;

public:
	CBaseImage(void);
	virtual ~CBaseImage();

	virtual bool create(CImageFile* imageFile) = 0;
	virtual int getBpp()        const { return this->bpp; };
	virtual int getBands()      const { return this->bands; };
    virtual int getWidth()      const { return this->width; };
    virtual int getHeight()     const { return this->height; };
    virtual int getTileWidth()  const { return this->tileWidth; };
    virtual int getTileHeight() const { return this->tileHeight; };
	

	int getNumberOfPyramidLevels() const;

    virtual bool getRect(unsigned char* buf, int top, int height, int left, int width) = 0;
    virtual bool getRect(unsigned char* buf, int top, int height, int left, int width, int destHeight, int destWidth) = 0;
    virtual bool setRect(unsigned char* buf, int top, int height, int left, int width);

    virtual unsigned char* getTile(int r, int c);
	virtual bool getImageTile(CImageTile** imageTile, int r, int c) = 0;

    virtual void setTile(unsigned char* buf, int r, int c) = 0;
    virtual bool getTileZoomUp(CImageBuffer& dest, int r, int c, int zoomUp) = 0;
    virtual bool getTileZoomDown(CImageBuffer &dest,int r, int c, int zoomDown) = 0;

	virtual unsigned char* getPixel(int x,int y);

	bool getOnePixel(unsigned char* buf, int col, int row);

	bool getInterpolatedPixel(unsigned char* buf, double x,double y);
	bool createHalfResTile(int r, int c);
	

	bool getTileZoomUpBrightened(unsigned char* buf, int r, int c, int zoomUp, double multiplier);
    bool getTileZoomDownBrightened(unsigned char* buf,int r, int c, int zoomDown, double multiplier);

    virtual int pixelSizeBytes() = 0;
    
	CHistogram* getHistogram(int band = 0,int pyramidLevel = 0);
	const vector<CHistogram*>& getHistograms(int pyramidLevel = 0) const;

	CLookupTable get16To8ConversionLookupTable() const;

    bool getBrightenedRect(unsigned char* buf, int top, int height, int left, int width, double multiplier);
    bool getBrightenedRect(unsigned char* buf, int top, int height, int left, int width, int destHeight, int destWidth, double multiplier);

    unsigned char* scanline(int l)
    {
        if (this->scanlineBuf == (unsigned char*)0)
            this->scanlineBuf = new unsigned char[this->lineSizeBytes()];

        this->getRect(this->scanlineBuf, l, 1, 0, this->getWidth());

        return this->scanlineBuf;
    };

    int lineSizeBytes()
    {
        return this->getWidth() * this->pixelSizeBytes();
    };

	
//protected:
public: // @@@ workaround for now
    unsigned char* scanlineBuf;

	long bpp; 
    /// number of bands (eg. RGB = 3)
    long bands;
    /// image width
    long width;
    /// image height
    long height;
    /// number of tiles across image
    long tilesAcross;
    /// number of tiles down image
    long tilesDown;
    /// tile width
    long tileWidth;
    /// tile height
    long tileHeight;

	/// pointer to the next image down in the pyramid (NULL if none)
    CBaseImage* halfRes;
    /// pointer to next image up in pyramid (NULL if none)
    CBaseImage* doubleSize;

    vector<CHistogram*> Histograms;

	virtual void deleteHistograms();

	void initHistograms();


};
