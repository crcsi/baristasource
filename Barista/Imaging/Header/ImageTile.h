#ifndef __CImageTile__
#define __CImageTile__

// Image tile currently supporting 2 image types
// 8-bit grey scale: each pixel 0-255
// 24-bit RGB with data organized BGR0BGR0 etc...
class CImageTile
{
public:
	CImageTile(int w, int h, int bpp, int bands);
	~CImageTile(void);

	int getWidth();
	int getHeight();
	unsigned char* getData();
	int getBpp();
	int getBands();

	void setTileRowCol(int r, int c);
	int getTileRow();
	int getTileCol();

	void setData(unsigned char* data);
    int pixelSizeBytes();

protected:
	int width;				// width of tile
	int height;				// height of tile
	unsigned char* data;	// pointer to image data
	int bpp;				// bits per pixel
	int bands;				// number of bands (i.e. RGB == 3)
	int dataSize;			// size of this->data

	int tileRow;			// the tile row from the full image
	int tileCol;			// the tile col from the full image
};

#endif


