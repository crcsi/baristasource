#pragma once
#include "MUObjectArray.h"
#include "MUObjectPtrArray.h"
#include "ZImagePtrArray.h"
#include "ZImage.h"
#include "ZImageArray.h"
#include "Serializable.h"

class CZImageArray : public CMUObjectArray<CZImage, CZImagePtrArray> //, public CSerializable
{
public:
	CZImageArray(int nGrowBy = 0);
	~CZImageArray(void);
/*
	bool isa(string& className) const 
	{
		if (className == "CZImageArray")
			return true;

		return false;
	};
	string getClassName() const {return string("CZImageArray");};

	void removeZImage(CZImage* img);

	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	bool isModified();
	void reconnectPointers();
	void resetPointers();
	CZImage* Add();
	CZImage* Add(const CZImage& img);
	*/
};
