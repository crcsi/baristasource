#ifndef __CG3DFile__
#define __CG3DFile__

#include "ImageFile.h"

#define	DEMG3D_NOHEIGHT 1e+17

class CBaseImage;
class CG3DFile : public CImageFile
{
public:
	CG3DFile();
	~CG3DFile(void);

	CG3DFile(const char* filename);

    bool write(const char* filename, CBaseImage* image);

	int getWidth();
	int getHeight();
	int getBpp();
	int bands();

	double getgridX();
	double getgridY();
	double getminX();
	double getminY();

	bool readLine(unsigned char* pBuffer, int iRow);

	float *zdata;

private: // Functions

	bool read();
	void close();

private: // Members

    /// flag if header info has been read
	bool			bHeaderRead;
	FILE*			fp;

	float xmin, xmax;
	float ymin, ymax;
	float zmin, zmax;

	float gridx, gridy;
};

#endif
