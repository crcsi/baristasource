// MUNEFFile.h: interface for the CNEFFile class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CNEFFile__
#define __CNEFFile__

#include "ImageFile.h"

class CNEFFile : public CImageFile   
{
public:
	CNEFFile();
	CNEFFile(const char* pFileName);

	virtual ~CNEFFile();

	int getWidth();
	int getHeight();
	int getBpp();
	int bands();

	bool readLine(unsigned char* pBuffer, int iRow);
    bool write(const char* filename, CBaseImage* image);
    void writeRGB();
	void writeGreenChannel();
	void demosaicRGB(unsigned char* src, unsigned char* dest);

private: // functions
	bool readHeader();
	//void init();

private: // Members

	bool							bHeaderRead;
	FILE*							fp;

	unsigned char* scanLineBuffer;
};

#endif // __CNEFFile__


