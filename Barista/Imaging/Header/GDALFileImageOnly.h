#ifndef __CGDALFileImageOnly__
#define __CGDALFileImageOnly__

#include "BaseImage.h"
#include "LookupTable.h"
#include "Filename.h"

class CImageTile;
class GDALDataset;

class CGDALFileImageOnly : public CBaseImage  
{
public:
	CGDALFileImageOnly();
	CGDALFileImageOnly(const CGDALFileImageOnly& src);
	CGDALFileImageOnly(const char* filename);

    bool write(const char* filename, CBaseImage* image);
	
	void readLookupTableInfo();
	void writeLookupTableInfo();

	virtual ~CGDALFileImageOnly();

	virtual bool closeFile();

	bool readFileInformation();
	float getNoDataValue(int band);

	bool readLine(unsigned char* pBuffer, int iRow);

	bool getHasHeader() { return this->bHeaderRead;};

    int pixelSizeBytes();

	virtual int getBpp() const;
    virtual int getBands() const;
    virtual int getWidth()const;
    virtual int getHeight()const;

	string getClassName() const
	{
		return string ("CGDALFileImageOnly");
	};

	bool isa(string& className) const
	{
		if (className == "CGDALFileImageOnly")
			return true;

		return false;
	};
	
	unsigned char* getTile(int r, int c); // gets the tile that include this r, c

	virtual void setExportFileOptions(CCharString& channelCombination,
									CCharString& compression,
									int compressionQuality,
									bool exportGeoData,
									bool exportSeperatFile,
									bool bandInterleave,
									CFileName fileNameGeoData);

    bool getRect(CImageBuffer &buffer);

	// dummy implementations of otherwise purely virtual functions
	virtual bool create(CImageFile* imageFile);
    virtual bool getRect(unsigned char* buf, int top, int height, int left, int width);
    virtual bool getRect(unsigned char* buf, int top, int height, int left, int width, int destHeight, int destWidth);
	virtual bool getImageTile(CImageTile** imageTile, int r, int c);
    virtual void setTile(unsigned char* buf, int r, int c);
    virtual bool getTileZoomUp(CImageBuffer& dest, int r, int c, int zoomUp);
    virtual bool getTileZoomDown(CImageBuffer &dest,int r, int c, int zoomDown);

protected: // functions
	bool writeGeoData(CBaseImage* image, GDALDataset* dataSet);
	bool writeData  (CBaseImage* image,GDALDataset* dataSet,bool tiled);
	bool write1Band (CBaseImage* image,GDALDataset* dataSet,bool tiled);
	bool write3Bands(CBaseImage* image,GDALDataset* dataSet,bool tiled);
	bool write4Bands(CBaseImage* image,GDALDataset* dataSet,bool tiled);


	bool read1BandLine(unsigned char* buffer, int iRow,unsigned int numberOfRowsToRead = 1);
	bool read3BandLine(unsigned char* buffer, int iRow);
	bool read4BandLine(unsigned char* buffer, int iRow);
	bool readTiledLine(unsigned char* buffer, int iRow);

	bool readHeader();
	

	void copyData(const float* src, float* dest,int width,int height,int channel,int offsetSrc,int offsetDest,bool intensity=false)
	{

		int maxSrc = width*height*offsetSrc;
		int maxDest = width*height*offsetDest;
		if (intensity)
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = (float)(((float)src[iSrc] + (float)src[iSrc+1] + (float)src[iSrc+2])/3.0);
		else
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = src[iSrc + channel];
	};


	void copyData(const unsigned char* src,unsigned char* dest,int width,int height,int channel,int offsetSrc,int offsetDest,bool intensity=false)
	{

		int maxSrc = width*height*offsetSrc;
		int maxDest = width*height*offsetDest;
		if (intensity)
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = (unsigned char)(((float)src[iSrc] + (float)src[iSrc+1] + (float)src[iSrc+2])/3.0);
		else
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = src[iSrc + channel];
	};


	void copyData(const unsigned short* src,unsigned short* dest,int width,int height,int channel,int offsetSrc,int offsetDest,bool intensity=false)
	{
		int maxSrc = width*height*offsetSrc;
		int maxDest = width*height*offsetDest;		

		if (intensity)
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = (unsigned short)(((float)src[iSrc] + (float)src[iSrc+1] + (float)src[iSrc+2])/3.0);
		else
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = src[iSrc + channel];
	};

	void copyData(	const unsigned short* src,   // source buffer
					unsigned char* dest,		// dest buffer
					int width,					// width of the buffers
					int height,					// height of the buffers
					int channelSrc,				// channel selects one band if src has more then 1 band, 
					int channelDest,			// channel to store the band
					int offsetSrc,				// offset between two pixels to be copied (1 -> copy every pixel, use 4 in combination with channel!)
					int offsetDest,             // offset between two pixels to be copied (1 -> copy every pixel, use 4 in combination with channel!)
					CLookupTable& lt,			// lookuptable to convefrt from 16 to 8 bit
					bool intensity=false)		// compute intensity and copy to the red channel (first band)
	{
		int maxSrc = width*height*offsetSrc;
		int maxDest = width*height*offsetDest;
		if (intensity)
			for (int iSrc=0,iDest=0; iSrc < maxSrc  && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest] = (unsigned char)lt[(unsigned short)(((float)src[iSrc] + (float)src[iSrc+1] + (float)src[iSrc+2])/3.0)];
		else
			for (int iSrc=0,iDest=0; iSrc < maxSrc && iDest < maxDest; iSrc+=offsetSrc,iDest+=offsetDest)
				dest[iDest+ channelDest] = (unsigned char)lt[src[iSrc + channelSrc]];
	};

protected: // Members
    /// flag if header info has been read
	bool			bHeaderRead;
	GDALDataset* gdal;
    CCharString FileName;

	int sizeofpixel;

	float NoData1;
	float NoData2;
	float NoData3;
	float NoData4;

	unsigned char* scanLineBuffer;

	unsigned char *linedata;
	unsigned char *rline;
	unsigned char *gline;
	unsigned char *bline;
	unsigned char *nline;

    /// tile buffer (used if the file was tiled)
	CImageTile**	tiles;

    /// the number of tiles (if any) in the tile buffer
	int nBufferedTiles;
	unsigned char* tileBuffer;
	int lastBufferedTile;

	bool tiled;
	int tileByteSize;
	bool colorIndexed;
	// red/gray: 0, green: 1, blue: 2, alpha/ir: 3
	int colorChannel1;
	int colorChannel2;
	int colorChannel3;
	int colorChannel4;

	bool hasColourInformations;


	CCharString channelExport;
	CCharString compressionExport;
	int compressionQualityExport;
	bool exportGeoData;
	bool exportSeperatFile;
	bool bandInterleave;
	CFileName fileNameGeoData;
	CLookupTable* lut;
};

#endif
