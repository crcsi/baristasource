#ifndef __ImageGraph__
#define __ImageGraph__

#include "m_attrib_def.h"


#define MAX_NEIGHBORS     8

#define POS_FIRST 0
#define POS_LAST  1
#define POS_BOTH  2
#define POS_NONE  4


#define  NO_PIX     0
#define  NORM_PIX   1
#define  START_PIX  4
#define  SPLIT_PIX  5
#define  STOP_PIX   7
#define  KPT_PIX    8
#define  COLL_PIX   9

#define  KEYPOINT 255
#define  KEYAREA  127

typedef struct {char type;short int c_no; } XYmapType;

// cases of neighbouring pixels:
// case             0  1  2  3  4  5  6  7  8  9 10 11
static int Lx[12]={ 1, 1, 0,-1,-1, 1, 0, 1, 1, 0,-1, 1};
static int Ly[12]={-1, 0,-1, 0,-1, 0,-1,-1, 0,-1,-1,-1};
static int Rx[12]={ 0,-1,-1, 1, 0,-1, 1,-1,-1, 0, 1,-1};
static int Ry[12]={ 1,-1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1}; 

// case 0      case 1      case 2      case 3
// | | |L|     |R| | |     | |L| |     | | | |
// | |*| |     | |*|L|     | |*| |     |L|*| |
// | |R| |     | | | |     |R| | |     | | |R|
//
// case 4      case 5      case 6      case 7
// |L| | |     | | | |     | |L| |     | | |L|
// | |*| |     | |*|L|     | |*| |     |R|*| |
// | |R| |     |R| | |     | | |R|     | | | |
//
// case 8      case 9      case 10      case 11
// | | | |     | |L| |     |L| | |     | | |L|
// |R|*|L|     | |*| |     | |*| |     | |*| |
// | | | |     | |R| |     | | |R|     |R| | |

// cases:          0  1  2  3  4  5  6  7
static int dx[8]={ 1, 1, 0,-1,-1,-1, 0, 1};
static int dy[8]={ 0,-1,-1,-1, 0, 1, 1, 1};

// |3|2|1|
// |4|*|0|
// |5|6|7|

static int DIR1X[8][9]={
  { 1, 1, 1, 1, 1, 1, 1, 1, 1},{ 0, 0, 1, 1, 1, 1, 1, 1, 1},
  {-1,-1,-1, 0, 0, 0, 1, 1, 1},{-1,-1,-1,-1,-1,-1,-1, 0, 0},
  {-1,-1,-1,-1,-1,-1,-1,-1,-1},{ 0, 0,-1,-1,-1,-1,-1,-1,-1},
  { 1, 1, 1, 0, 0, 0,-1,-1,-1},{ 1, 1, 1, 1, 1, 1, 1, 0, 0}};
static int DIR1Y[8][9]={
  {-1,-1,-1, 0, 0, 0, 1, 1, 1},{-1,-1,-1,-1,-1,-1,-1, 0, 0},
  {-1,-1,-1,-1,-1,-1,-1,-1,-1},{ 0, 0,-1,-1,-1,-1,-1,-1,-1},
  { 1, 1, 1, 0, 0, 0,-1,-1,-1},{ 1, 1, 1, 1, 1, 1, 1, 0, 0},
  { 1, 1, 1, 1, 1, 1, 1, 1, 1},{ 0, 0, 1, 1, 1, 1, 1, 1, 1}};
static int DIR2X[8][9]={
  { 2, 2, 2, 2, 2, 2, 2, 2, 2},{ 0, 1, 0, 1, 2, 2, 2, 2, 2},
  {-2,-1, 0,-1, 0, 1, 0, 1, 2},{-2,-2,-2,-2,-2,-1, 0,-1, 0},
  {-2,-2,-2,-2,-2,-2,-2,-2,-2},{ 0,-1, 0,-1,-2,-2,-2,-2,-2},
  { 2, 1, 0, 1 ,0,-1, 0,-1,-2},{ 2, 2, 2, 2, 2, 1, 0, 1, 0}};
static int DIR2Y[8][9]={
  {-2,-1, 0,-1, 0, 1, 0, 1, 2},{-2,-2,-2,-2,-2,-1, 0,-1, 0},
  {-2,-2,-2,-2,-2,-2,-2,-2,-2},{ 0,-1, 0,-1,-2,-2,-2,-2,-2},
  { 2, 1, 0, 1 ,0,-1, 0,-1,-2},{ 2, 2, 2, 2, 2, 1, 0, 1, 0},
  { 2, 2, 2, 2, 2, 2, 2, 2, 2},{ 0, 1, 0, 1, 2, 2, 2, 2, 2}};

class AIM_Pixel
{
   public:

	   int x,y;             // x and y coordinates of pixel
	   unsigned char type;  // the type of pixel (graph-modules.h) 
	   float gradientStrength;

	   AIM_Pixel() : x(0),y(0),type(0),gradientStrength(0) {};
	   AIM_Pixel(const AIM_Pixel &pix) : x(pix.x),y(pix.y),type(pix.type),gradientStrength(pix.gradientStrength) {};
	   ~AIM_Pixel() {};

	   bool operator < (const AIM_Pixel &pixel) const 
	   { return gradientStrength < pixel.gradientStrength; };
	   bool operator > (const AIM_Pixel &pixel) const 
	   { return gradientStrength > pixel.gradientStrength; };
};

class AIM_Contour
{
   protected:
	   C_attributes *attr;             // user-defined attributes
	   AIM_Pixel *chain;               // pointer to array of pixels 
	   unsigned long p_init,p_alloc;   // initialized and allocated area of *chain
	   unsigned long len;              // # of assigned (active) pixels in *chain 

   public:

	   bool active;                    // toggle (True/False) if contour exists 
	   unsigned long v_no[2];          // index to vertex, delimimiting *chain 

	   AIM_Contour();

	   AIM_Contour(const AIM_Contour &contour);

	   ~AIM_Contour();

	   void deleteAttributes();
	   void initAttributes(bool overwrite);

	   unsigned long getLength() const { return len; };
	   unsigned long getAllocLength() const { return p_alloc; };

	   unsigned long getInitLength() const { return p_init; };

   	   AIM_Contour &operator = (const AIM_Contour &contour);

	   C_attributes* getAttr() const { return attr; };

	   AIM_Pixel &getPixel(unsigned long index) { assert(index < len); return chain[index]; };

	   bool copyFrom(const AIM_Contour &contour);

	   long addP(int firstLast, const AIM_Pixel &p);
	
	   void initChain(unsigned long I_pinit);

	   void deleteChain();
	   void setChain(AIM_Pixel *I_chain, unsigned long newLen, unsigned long newAlloc);

	   void appendChain(const AIM_Contour &contour);

	   void appendChainReverted(const AIM_Contour &contour);

	   void insertChain(const AIM_Contour &contour);

	   void insertChainReverted(const AIM_Contour &contour);

	   long existsPinC(long x, long y);
	
	   unsigned long delP(unsigned long p_no);

	   void cutOffChainAtEnd(unsigned long position);

	   void cutOffChainAtBeginning(unsigned long position);

	   void straightLineFit();

	   float initContourLength();

	   void initDirectionAttributes();

	   void initShapeAttributes(float straightLength, float tolerance, float exponent);

	   int getNrOfPolylines(float tolerance, float exponent);

	   int polygonPtsAdaptive(AIM_Pixel *&points, float &tolerance, float &exponent);

	   static int polygonPtsAdaptive(AIM_Pixel *points, int len, float &tolerance, float &exponent);

	   static int polygonApprox(AIM_Pixel  *curve, int npt, AIM_Pixel *polygon, float tolerance,float exponent);


  protected:

	   static int polygonApproxR(AIM_Pixel *curve, int npt, AIM_Pixel *polygon, float toleranz, float exponent);

	   static bool suppressPoint(AIM_Pixel *points, int start, int stop, float tolerance, float exponent);

	   static float getStraightLineTolerance(float dist, float exponent);

};

class AIM_C_in_Vertex
{
   public:

	   unsigned long c_no;   // id of attached contour 
	   char mul;             // either one or two! if contour closed = 2,
						     // otherwise = 1 
};

class AIM_Vertex
{
   public:

	   bool active;                  // toggle (true/false) if vertex exists
	   long x,y;				     // x and y coordinates of vertex 
	   char ord;                     // number of connected contours    
	   AIM_C_in_Vertex c[MAX_NEIGHBORS]; // references to connected contours 
	   char type;                    // type (graph-modules.h) of vertex 

   public:

	   AIM_Vertex();

	   AIM_Vertex(const AIM_Vertex &vertex);

	   int getPositionOfContour(long contour);
	   
	   void addNeighbour(long contour);
	   void removeNeighbour(long contour, bool removeBoth);
};

  
class AIM_Graph
{
   public:
	   unsigned long width;
	   unsigned long height;     // dimensions of original image
	   unsigned long c_init;
	   unsigned long c_alloc;
	   unsigned long c_max;     // initialized, allocated and max used 
	   unsigned long v_init;
	   unsigned long v_alloc;
	   unsigned long v_max;     // id of contour-segment (resp. vertex) 
	   AIM_Contour *c;          // pointer to contour array 
	   AIM_Vertex  *v;          // pointer to vertex array 
	   G_attributes *attr;      // user-defined attributes 


	   AIM_Graph(unsigned long Cinit, unsigned long Vinit, 
				 long I_width, long I_height);

	   ~AIM_Graph();


	   long getnewV();

	   void deleteV(unsigned long vertex);

	   long getnewC();

	   long copyC(unsigned long source);

	   void deleteC(unsigned long contour);

	   long getNoOfV() const;

	   long getNoOfC() const;

	   void initAttr();

	   long getVertex(long x, long y);

	   int getPosOfVInC(long v_no, long c_no);

	   void mergeContoursAtVertex(long vertIndex, unsigned char type);

	   bool splitContourAt(long sourceContour, long x, long y, int type,
						   long &newContour, long &newVertex);

	   int splitContours();

	   int groupContours(bool onlyCurved);

	   int removeClosed(int lengthThreshold);


	   int clean(float strengthThreshold, float lengthThreshold);

	   void dcelEdgeLinks();

	   void refineEndpoints();

	   void getStatistics(float &avgStrenght, float &avgLength, float *medStrenght, float *medLength);

   protected:

	   int iterateClean(float strengthThreshold, float lengthThreshold);

};

#endif