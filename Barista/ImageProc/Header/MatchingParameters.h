#pragma once

#include <math.h>
#include "Serializable.h"

enum eLSMBANDMODE { eIDENTICALBANDS, eNONIDENTICALBANDS };
enum eLSMBPPMODE { eIDENTICALBPPS, eALLBPPS };
enum eLSMTRAFOMODE { eLSMNOTFW, eLSMTFW };
enum eLSMSCALEMODE { eLSMUSESEARCH = 0, eLSMUSETEMP};

class CMatchingParameters: public CSerializable
{
public:
	CMatchingParameters(void);
	~CMatchingParameters(void);

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    virtual void reconnectPointers();
    virtual void resetPointers();

    virtual bool isModified();

    bool isa(string& className) const;
    string getClassName() const;

	CMatchingParameters &operator = (const CMatchingParameters &parameter);

	// set/get for values
	void setPatchSize(int size) { this->patchsize = size;};
    int getPatchSize() const	{return this->patchsize;};

	int getPyrLevel() const { return this->PyrLevel; };
	void setPyrLevel(int lev)  { this->PyrLevel = lev; };

	void setIterationSteps(int steps) { this->iterationSteps = steps;};
    int getIterationSteps() const	{return this->iterationSteps;};

	void setRhoMin(double rho) { this->rhoMin = rho;};
    double getRhoMin() const	{return this->rhoMin;}

	void setMinZ(double minz) { this->minZ = minz;};
    double getMinZ() const	{return this->minZ;}

	void setMaxZ(double maxz) { this->maxZ = maxz;};
    double getMaxZ() const	{return this->maxZ;}

    double getZ() const	{return (this->maxZ + this->minZ) * 0.5; }

	double getDeltaZ() const {return fabs(this->maxZ - this->minZ) * 0.5; }

	void setSigmaGrey8Bit(double sigma) { this->sigmaGrey8Bit = sigma;};
    double getSigmaGrey8Bit() const	{return this->sigmaGrey8Bit;}

	void setSigmaGrey16Bit(double sigma) { this->sigmaGrey16Bit = sigma;};
    double getSigmaGrey16Bit() const	{return this->sigmaGrey16Bit;}

	void setSigmaGeom(double sigma) { this->sigmaGeom = sigma;};
    double getSigmaGeom()const	{return this->sigmaGeom;}

	void setBandMode(eLSMBANDMODE mode) { this->bandmode = mode;};
    eLSMBANDMODE getBandMode() const	{return this->bandmode;}

	void setBppMode(eLSMBPPMODE mode) { this->bppmode = mode;};
    eLSMBPPMODE getBppMode() const	{return this->bppmode;}

	void setTrafoMode(eLSMTRAFOMODE mode) { this->trafomode = mode;};
    eLSMTRAFOMODE getTrafoMode() const	{return this->trafomode;}

	void setShowMatchWindow(bool bval) { this->showMatchWindow = bval;};
    bool getShowMatchWindow() const	{return this->showMatchWindow;}

	void setXYZFileName(const CCharString &name) 
	{ 
		this->XYZFileName = name;
	};
    CCharString getXYZFileName() const	{return this->XYZFileName; }

	void setScaleMode(eLSMSCALEMODE val) { this->scalemode = val;};
    eLSMSCALEMODE getScaleMode() const	{return this->scalemode;}

	void reset();

protected:
	int patchsize;
	int iterationSteps;
	double rhoMin;
	double minZ;
	double maxZ;
	double sigmaGrey8Bit;
	double sigmaGrey16Bit;
	double sigmaGeom;
	double Z;
	int PyrLevel;

	CCharString XYZFileName;

	eLSMBANDMODE bandmode;
	eLSMBPPMODE bppmode;
	eLSMTRAFOMODE trafomode;
	eLSMSCALEMODE scalemode;

	bool showMatchWindow;


};
