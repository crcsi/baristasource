//============================================================================
//
// File        : LabelOverlapIndicator.h
//
// Author      : FR
//
// Description : Interface of class CLabelOverlapIndicator, a class
//               describing the overlap between two regions in a 
//               two label images
//
//============================================================================

#ifndef ____CLabelOverlapIndicator____
#define ____CLabelOverlapIndicator____

#include "CharString.h"

//****************************************************************************

enum eOVERLAP { eNONE, eWEAK, ePARTIAL, eSTRONG };

class CLabelOverlapIndicator
{ 
  protected:

	  unsigned long index; 

	  unsigned long nPixels;

	  float percentage;

	  eOVERLAP eClassification;

//============================================================================
  
  public:

//============================================================================

	  CLabelOverlapIndicator();

	  CLabelOverlapIndicator(const CLabelOverlapIndicator &indicator);

	  ~CLabelOverlapIndicator () { };


//============================================================================

	  unsigned long getIndex() const { return index; };

	  unsigned long getNPixels() const { return nPixels; };

	  float getPercentage() const { return percentage; };

	  eOVERLAP getClassification() const { return eClassification; };


//============================================================================


	  void init(unsigned long Index, unsigned long NPixels);


	  void finish(unsigned long TotalPixels);


	  eOVERLAP classify(float NoneVsWeak, float WeakVsPartial, 
			  		    float PartialVsStrong);


	  CCharString getString(bool showNone) const;

};

#endif
