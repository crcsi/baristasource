/**
 * Copyright:    Copyright (c) Petra Helmholz<p>
 * Company:      University of Melbourne<p>
 * @author PetraH
 * @version 1.0
 */

#ifndef __CSnakePoint__
#define __CSnakePoint__

#pragma once
#include "2DPoint.h"
#include "SnakePars.h"

class CImageBuffer;
class SnakeExtractionParameters;

/*!
 * \brief
 * Defines a CSnakePoint used for the calculation of snakes derivative from the CPointBase / C2DPoint with additional parameters.
 * 
 * Additional parameters are gradient values of the point in the image in X and Y direction and furthermore
 * coefficients use for the snake calculation ( coeffX = gamma * x + k * gradX, coeffY = gamma * y + k * gradY).
 */


//****************************************************************************

class CSnakePoint : public C2DPoint
{
  protected:

	  unsigned int index;       ///< Index of the pixel chain in the edge image

	  float gradX;				///< Gradient value or GradientVectorField at the vertice position in X direction
	  float gradY;				///< Gradient value or GradientVectorField at the vertice position in Y direction
	  double coefficientsX;		///< Coefficients for the snake calculation (X)
	  double coefficientsY;		///< Coefficients for the snake calculation (Y)

	 	  
  
//============================================================================

  public:
   
	  CSnakePoint() : CPointBase(2), index (0), gradX(0.0), gradY(0.0), coefficientsX(0.0), coefficientsY(0.0) {};
	  CSnakePoint(const CSnakePoint &p) : CPointBase(p), C2DPoint(p),index(p.index), gradX(p.gradX), gradY(p.gradY), 
		                                   coefficientsX(p.coefficientsX), coefficientsY(p.coefficientsY) {};

	  virtual ~CSnakePoint() {};

	  CSnakePoint &operator = (const CSnakePoint &p);
 
	  unsigned int getIndex() const { return this->index; };
	  double getX () const { return this->x; };
	  double getY () const { return this->y; };
	  float getGradX () const { return this->gradX; };
	  float getGradY () const { return this->gradY; };
	  double getCoefficientsX () const { return this->coefficientsX; };
	  double getCoefficientsY () const { return this->coefficientsY; };

	  void setX (double X) {this->x = X; };
	  void setY (double Y) {this->y = Y; };
	  void setIndex (unsigned int Index) {this->index = Index; };

	  void initGradientValues(const CImageBuffer *gradX, const CImageBuffer *gradY, C2DPoint Offset);
	  bool initCoefficients(const SnakeExtractionParameters  *Parameters);

	  void dump(ofstream &oFile) const;



  protected: 

};

#endif
