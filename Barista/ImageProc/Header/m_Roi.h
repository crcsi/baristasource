#pragma once
#include "ZObject.h"
#include "2DPoint.h"
#include "ImgBuf.h"

#include "m_match_errcds.h"
#include "m_match_def.h"

//#include <math.h>

typedef struct{
	int gain_method;
	int method;
	double mean;
	double std;
	int flag_roi;
	int sizex;
	int sizey;
	double bfc;
	double cec;
	int clipp;
	int bits;

}WallisParam ;

class m_Roi :public CImageBuffer
{
  public:

	  m_Roi();

	  virtual ~m_Roi();

	  string getClassName() const
	
	  {
		  return string( "m_Roi");
	  }


	  bool isa(string& className) const

	  {
		  if (className == "m_Roi") return true;
		  return false;
	  }

		
  protected:
	
	  CImageBuffer *backupBuf;

	  int mod_y; // full image size in x 
	              
	  int level;		// level of roi

	  int type;

	  int enable;


	  int m_errval;

  public:

	  int direc_flag; // flag whether directions are stored

	  int pix_flag;   // whether  greyval or edges are used

	  int hasBackupBuffer() const { return (backupBuf != NULL); };

	  int storeOriginalBuffer();

	  int restoreOriginalBuffer();

	  void deleteBackupBuffer();

	  int coeff_set_roi(double a[6],int flag);
	
	  // filter interface
/*	  
	  int	m_filter (int filt_fn,int size);
	
	  int	m_pr_imp_lowpass (int size);

	  int	imp_lowpass (float	*pix,int sx,int sy,int nf);
*/
	  void m_enhance(float *buf_filt,WallisParam param);

	  double         *get_coeff(){return (_coeff);};
	
public:
	int		filtered;	/* whether already filtered or not   	*/
    double	rad_mean;	/* radiometric mean of pixel data    	*/
    double	rad_sdev;	/* radiometric standard deviation    	*/
	double          _coeff[6];
	double          _coeff_inv[6];
	double         _rot_angle;
};
