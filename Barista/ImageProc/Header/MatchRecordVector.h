#ifndef ___CMatchRecordVector_H___
#define ___CMatchRecordVector_H___

#include "2DPoint.h"
#include "XYPointArray.h"
#include "MatchRecord.h"

class Matrix;
class CTrans2D;

enum eRegistrationSensorModel { eAFFINEREGISTRATION, ePROJECTIVEREGISTRATION };

class CMatchRecordVector : protected vector<CMatchRecord *>
{
  protected:

	  C2DPoint cog;

	  int NHypotheses;


  public:

	  CMatchRecordVector(const CXYPointArray &ptArray, const CImageBuffer &buf, int width, int height);

	  CMatchRecordVector() : cog(0,0), NHypotheses(0) {};

	  virtual ~CMatchRecordVector();

	  C2DPoint getCOG() const { return this->cog; };

	  CMatchRecord *&operator[] (int index) 
	  { return vector<CMatchRecord *>::operator [](index); }

	  int nRecords() const { return this->size(); };

	  int getNHypotheses() const { return NHypotheses; };

	  int getIndexByY(float Y) const;

	  void deleteAll();

	  bool computeRobustTrafo(CTrans2D *trafo, Matrix &trafoCovar, CMatchRecordVector &other, 
							  float threshold, double &sigma0);

	  void deleteNoMatches();

	  void clarifyMultipleMatches();

	  void appendVectors(CMatchRecordVector &appendVec, 
		                 CMatchRecordVector &otherVec, 
						 CMatchRecordVector &otherAppendVec);

    protected:

	  bool computeTrafo(CTrans2D *trafo, Matrix &trafoCovar, const CMatchRecordVector &other, double &sigma0);

	  int removeLargeResiduals(float threshold);

};

#endif