#pragma once

#include "XYPoint.h"
#include "ImgBuf.h"
#include "filter.h"
#include "sortVector.h"
#include "ProgressHandler.h"
#include "FeatExtractPars.h"
#include "LabelImage.h"

#include "ImageGraph.h"

// classes of pixels
#define POINT_RELMAX        0
#define POINT_NOMAX        64
#define EDGE_RELMAX       128
#define EDGE_NOMAX        192
#define HOMOGENEOUS       250
#define OUTSIDE_BOUNDARY  255

#define NN 5
#define SMALL			1e-10    

enum feat_enumtype {
	NO_TYPE_SET,
   eEDGEL,
   eEDGE_LINKED,
   eEDGE_STRAIGHT,
   eEDGE_CURVED,
   eEDGE_CLOSED,
   eVERTEX,
   eCHAIN,
   ePOINT_CORNER 
};

class CLabelImage;
class CImageRegion;
class CXYPointArray;
class CXYPolyLineArray;
class CXYPolyLine;
class CXYContourArray;
class CRAG;

class CFeatureExtractor : protected CImageBuffer, public CProgressHandler
{
  protected:
		
	  // *this as an image data buffer contains the results of texture classification,
	  // i.e., each pixel of *this is classified according to one of the following classes:
	  // POINT_RELMAX, POINT_NOMAX, EDGE_RELMAX, EDGE_NOMAX, HOMOGENEOUS  

	CImageBuffer *featRoi;					// local pointer to the image data buffer

	CImageBuffer TextureStrength;		    // strength of image texture (derived from the gradient)

	CImageBuffer *pointStrength;            // strength of image texture for point extraction (Foerstner only)

	CImageBuffer TextureIsotropy;           // isotropy of image texture for point extraction (Foerstner only)

	CImageBuffer GradientDirection;         // direction of smoothed gradient vector

	vector<float> VarNoiseConstant;         // constant portion of noise variance of *featRoi
	vector<float> VarNoiseLinear;           // linear portion of noise variance of *featRoi (Poisson distribution only)
	vector<vector<float> > VarNoiseFactor;   // error propagation factor for image variance

	float varNoise;

	AIM_Graph *graph;

	int margin;
	
	FeatureExtractionParameters extractionParameters;
	CCharString listenerTitleString;

	CXYPolyLine *boundaryPolygon;
	CImageBuffer *validMask;

  public:

	CFeatureExtractor(const FeatureExtractionParameters &I_parameters);

	~CFeatureExtractor();

	void setRoi(CImageBuffer *troi, CXYPolyLine *BoundaryPoly);
	void setRoi(CImageBuffer *troi, CImageBuffer *pValidMask);
	void setPars(const FeatureExtractionParameters &I_parameters) { extractionParameters = I_parameters; }

	void resetRoi()
	{
		this->featRoi=NULL;
		this->boundaryPolygon = 0;
		this->validMask = 0;
	};

	AIM_Graph* getContourGraph() {return this->graph;};

	float &textureStrength(const unsigned long &index) const { return TextureStrength.pixFlt(index); };

	float &textureIsotropy(const unsigned long &index) const { return TextureIsotropy.pixFlt(index); };

	float &gradientDirection(const unsigned long &index) const { return GradientDirection.pixFlt(index); };

	bool isEdgePixel(const unsigned long &index) const { return (this->pixUChar(index) == EDGE_RELMAX); };

	bool isEdgeRegionPixel(const unsigned long &index) const 
	{ return ((this->pixUChar(index) == EDGE_NOMAX) || (this->pixUChar(index) == EDGE_RELMAX)); };

	bool isPointPixel(const unsigned long &index) const { return (this->pixUChar(index) == POINT_RELMAX); };

	bool isPointRegionPixel(const unsigned long &index) const 
	{ return ((this->pixUChar(index) == POINT_NOMAX) || (this->pixUChar(index) == POINT_RELMAX)); };

	bool isHomogeneous(const unsigned long &index) const { return (this->pixUChar(index) == HOMOGENEOUS); };

	int getClass(const unsigned long &index) const { return this->pixUChar(index); };

	int getMargin() const { return margin; };

	bool extractFeatures(CXYPointArray *xypts, CXYContourArray *xycont, 
		                 CXYPolyLineArray *xyedges, CLabelImage *labels,
						 CRAG *rag, bool merge);


	bool edgesLink(float significance, CXYContourArray *xycont);

	
	string getClassName() const
	{	
		return string( "CFeatureExtractor");
	}

	bool isa(string& className) const
	{
		if (className == "CFeatureExtractor")
			return true;
		return false;
	}

	void exportClassification(const char *filename);

  private:

	void initNoiseModelParameters();

	void computeDerivatives(CImageBuffer &gradX, CImageBuffer &gradY);

	void computeGradientMagnitudeAndDirectionCanny(CImageBuffer &gradX, CImageBuffer &gradY);

	void computeGradientMagnitudeAndDirectionFoerstner(CImageBuffer &gradX, CImageBuffer &gradY);

	bool computeWatershed();

	void classifyTexture();

	void removeOutsideBoundary();

	void thinOutPoints();

	void thinOutEdges(CImageBuffer &gradX, CImageBuffer &gradY);

	void extractPoints(CXYPointArray *xyPoints);

	bool extractContours(CXYContourArray *xycont);

	bool extractRegions(CLabelImage *labels, CRAG *rag, bool merge);

	void thinOutContours(CXYContourArray *xycont, CXYPolyLineArray *xyedges);

	void subpixelEdgePoint(C2DPoint &point) const;
	
	void TiffDump(char *filePart) const;
	void TiffDump(short *img, int nbands, char *filename) const;
	void TiffDump(unsigned short *img, int nbands, char *filename) const;
	void TiffDump(float *img, int nbands, char *filename) const;
	void TiffDump(unsigned char *img, int nbands, char *filename) const;

	void canny();

	void extractParallelLines(CImageBuffer &ROI);

	void NoiseEstimation(float c,int Upper,float *mode,float *dmax,float *dmin,float *thresh);


	// necessary for graph generation
	void getStartPoints(CSortVector<AIM_Pixel> &startPoints, float P_th);
	void aim_contour_aggregation(const CSortVector<AIM_Pixel> &startPoints);

	void aim_init_GlobalVar();
	void aim_free_GlobalVar();

	void aim_get_ContourString(const AIM_Pixel &startPoint, float stori, int c_no);

	int aim_delete_string(int c_no);
	int aim_treat_collision(int,int);

	int aim_angle2discrete(float ang,int which);
	int aim_collision(int,int,int,int,int,int);
	void aim_add_kptArea(unsigned char *,int,char);
	void aim_delete_kptArea(int);
	unsigned char aim_get_kptType(int);
	void aim_updateXYMAP(int);
	unsigned char aim_kptCatch(int);
	int aim_treat_endPoint(int,int);

//////////////////////////////////////////////

	void Attribute_graph();

	float Integrate_strength(unsigned long c_no);

	static void sort(int n,float *ra);

	static float diffori(float x,float y);

	static float mapangle(float x,int which);

private:


	bool textureClassification(CImageBuffer &gradX, CImageBuffer &gradY);

	unsigned char *KPTCODE,*START;
	unsigned char TDIR[2];
	int EP[2];
	int MASK3X3[9],SM2[8][9][2],NEXTMASK1[8][9],NEXTMASK2[8][9];
	float MASKWEIGHT[8][9];
	XYmapType *XYMAP;

	
	typedef struct 
	{
	  int address;
	} item_str;

	typedef struct cell 
	{
	  item_str item;
	  struct cell *next;
	} cell_str;

	typedef struct 
	{
	  cell_str *first;
	  cell_str *last;
	} queue_str;

	void queue_init(queue_str *qq);
	int queue_empty(queue_str *qq);
	void queue_add(queue_str *qq, item_str *item_a);
	int queue_first(queue_str *qq, item_str *item_r);

	bool GradientDeriche (CImageBuffer &gradX, CImageBuffer &gradY, float Sigma1, float Sigma2);

	bool Gauss ();

	int TestBorder (unsigned char *map, int Width, int Height, int i, int j, int s);


};
