#ifndef __CLabelImage__
#define __CLabelImage__

#include "ZObject.h"
#include "ImgBuf.h"
#include "newmat.h"
#include "LookupTable.h"
//#include "SegmentAMatrix.h"
//#include "SegmentNeighbourhood.h"


/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

class CXYPolyLine;
class CXYZPolyLine;
class C2DPoint;
class CImageRegion;
class CSegmentAMatrix;

//****************************************************************************

class CLabelImage: public CImageBuffer
{
  protected:

//=============================================================================
  
	  CImageBuffer *pMask;     /// Binary image: auxiliary for opening, closing, etc.

	  CImageBuffer *pVoronoi;  /// Label image corresponding to the voronoi diagram of *this

	  CImageBuffer *pDistance; /// Distance image corresponding to the voronoi diagram

	  vector<unsigned long> numberOfPixels;  // Vector of the numbers of pixels of the extracted labels

	  unsigned long nLabels;

	  int neighbourhood;

	  static CLookupTable lookup;

  public:

//=============================================================================
//======================== Constructors, destructors  =========================
   
	  CLabelImage(long I_offsetX, long I_offset_Y, long I_width, long I_height, int I_neighbourhood);

	  CLabelImage(const CImageBuffer &mask, float value, int I_neighbourhood);
   
	  CLabelImage(const CLabelImage &labelimage);

	  virtual ~CLabelImage();


//=============================================================================
//================================ Query data =================================

   
	  int getNeighbourhood() const { return this->neighbourhood; }

	  unsigned long  getNLabels() const { return nLabels; };
  
	  unsigned long  getNPixels(unsigned long label) const { return numberOfPixels[label]; };
  
	  unsigned short getVoronoi(unsigned long index) const { return pVoronoi->pixUShort(index); };

	  unsigned short getDistance(unsigned long index) const { return pDistance->pixUShort(index); };

	  unsigned short getLabel(unsigned long index) const { return this->pixUShort(index); };
   
	  unsigned short getNeighbourLabel(unsigned short label, const C2DPoint &pos) const;

	  CImageBuffer *getPVoronoi() const { return pVoronoi; };

	  static CLookupTable getLookup() { return CLabelImage::lookup; };

	  static unsigned char convertLabelToUC(unsigned short label);


//=============================================================================

	  string getClassName() const
	  {	
		  return string( "CLabelImage");
	  }

	  bool isa(string& className) const
	  {
		  if (className == "CLabelImage")
			  return true;
		  return false;
	  }

	  static CLookupTable get16BitLookup();

	  virtual bool set(long I_offsetX, long I_offsetY, long I_width, long I_height, int I_bands, int I_bpp, bool I_unsignedFlag);

	  virtual bool set(long I_offsetX, long I_offsetY, long I_width, long I_height, int I_bands, int I_bpp, bool I_unsignedFlag, float value);

	  virtual bool set(const CImageBuffer &buffer);

	  virtual bool setOffset(long I_offsetX, long I_offsetY);

	  virtual bool convertToUnsignedChar() { return false; };

	  virtual bool convertToShort() { return false; };

	  virtual bool convertToFloat() { return false; };

	  virtual bool convertToBinary(float value) { return false; };

	  CLabelImage &operator = (const CLabelImage& labelimage);

	  void clear();

//=============================================================================

	  void initFromMask(const CImageBuffer &I_mask, float value);

	  void initMask(); 

//=============================================================================

	  void setBoundariesToZero(bool renumber, bool updateNumbers,
		                       bool recomputeVoronoi, unsigned short firstLabel);

	  void makeGapsSmallByVoronoi(int tolerance, bool renumber, bool updateNumbers,
		                          bool recomputeVoronoi);


//=============================================================================

	  unsigned short initNewLabel(const CImageBuffer &image, const C2DPoint &centre, 
								  int winSize, CImageRegion &newRegion);

//=============================================================================

	  void fill(const CXYPolyLine &poly, unsigned short label);

	  void fill(const CXYZPolyLine &poly, unsigned short label);

	  void fillVoronoi(const CXYPolyLine &poly, unsigned short label, unsigned short overwriteLabel);

	  void fillVoronoi(const CXYZPolyLine &poly, unsigned short label, unsigned short overwriteLabel);
   
	  void resetVoronoi();

	  void updateVoronoi();
   
	  void updateVoronoi(const CImageBuffer &background, float value);

	  void removeInVoronoi(const CImageBuffer &background, float value);
   
	  void computePixelNumbers();

	  void initPixelNumbers();

	  void changeContradicting();

	  void exchangeLabels(unsigned short label1, unsigned short label2);
   
	  void replaceLabel(unsigned short oldLabel, unsigned short newLabel, bool renumber);
   
	  void removeLabel (unsigned short label, bool renumber);

	  void mergeLabels (unsigned short label1, unsigned short label2, bool renumber);

	  void removeEmptyLabels(bool renumber);
   
	  void removeSmallLabels(unsigned long minimumSize, bool renumber);

	  void renumberLabels();

	  void importNewLabels(const CLabelImage &newLabels, bool renumber);

	  void eraseInVoronoi(const CImageBuffer &I_mask);

	  void morphologicalOpen(int structureSize, unsigned short labelMin);

	  void morphologicalOpenLabel(unsigned short label, int structureSize);

	  void morphologicalClose(int structureSize);

	  long growRegion(CImageRegion &region, const CImageBuffer &image);

	  long growRegions(vector<CImageRegion *> &regionVector, 
		               const CImageBuffer &image, 
					   unsigned short MinRegion, int maxIt);

	  vector<CImageRegion *> *initRegionVector(const CImageBuffer &image);

	  vector<CImageRegion *> *initRegionVector4Labels(const CImageBuffer &image);
	  vector<CImageRegion *> *initRegionVector4Labels();

	  vector<CImageRegion *> *initRegionVector(const CImageBuffer &image, int morphSize);

	  vector<CImageRegion *> *initRegionVector();

//=============================================================================
					 
	  bool getMaskForLabel(CImageBuffer &lMask, unsigned short label, 
		                   int factor, bool useVoronoi) const;

	  bool getContours(CXYContourArray &contours, 
		               unsigned short label, bool useVoronoi) const;

	  bool getContours(vector<vector<CXYContour *> > &contourNeighbourVector, 
		               vector<vector<unsigned short> > &neighbours, 
		               unsigned short label, bool useVoronoi) const;

	  void resetVoronoiByDist(unsigned short minDist);

//=============================================================================



	  void dump(const char *path = "", const char *file = "", const char *suffix = "", bool voronoiFlag = false, bool Ii_distFlag = false);   


	  static void getColour(int region, int &r, int &g, int &b);

  protected:

	  long createLabels();

	  void growRegionN8(unsigned long index, unsigned short region, unsigned long &PixelsInRegion);
	 
	  void growRegionN4(unsigned long index, unsigned short region, unsigned long &PixelsInRegion);
	 
	  void mergeNeighbourRegions();

  	  bool chamfer(int size);

	  void initLookup();

	  long growRegionIt(CImageRegion &region, const CImageBuffer &image);

	  long growRegionsIt(vector<CImageRegion *> &regionVector, 
		                 const CImageBuffer &image, 
					     unsigned short MinRegion);
	  
	  void replaceLabel(unsigned short oldLabel, unsigned short newLabel, 
		                vector<int> &rmin, vector<int> &rmax, 
						vector<int> &cmin, vector<int> &cmax);

	  bool getContours(const CImageBuffer &labels, CXYContourArray &contours, 
		               unsigned short label) const;

	  bool getMaskForLabel(CImageBuffer &lMask, const CImageBuffer &labels, 
		                   unsigned short label, int factor) const;


};
//****************************************************************************

#endif
