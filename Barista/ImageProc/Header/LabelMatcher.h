#ifndef ____CLabelMatcher____
#define ____CLabelMatcher____

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

#include <vector>
using namespace std;

#include "CharString.h"
#include "LabelOverlapVector.h"
#include "ImgBuf.h"
#include "LookupTable.h"
#include "LabelImage.h"
#include "ProgressHandler.h"

/*!
 * \brief
 * CLabelMatcher is a container class for matching of labels from multiple images
 * 
	
 * 
 * \see
 * CMultipleLabelOverlapRecord | CLabelOverlapIndicator
 */
class CLabelMatcher : public CProgressHandler
{   
  protected:

	  CImageBuffer *pCounter;

	  CLabelOverlapVector OverlapsDatasetOne;

	  CLabelOverlapVector OverlapsDatasetTwo;

	  CLabelImage *pLabelsOne;

	  CLabelImage *pLabelsTwo;

	  CLabelImage *pLabelsCombined;

	  CImageBuffer *pMask;

	  float areaThreshold;

	  vector<unsigned short> combinedLabel1Vector;
	  vector<unsigned short> combinedLabel2Vector;

  public:

//============================================================================

	  CLabelMatcher(float AreaThreshold);

//============================================================================
//================================ Destructor ================================

	  virtual ~CLabelMatcher ();

	  bool init(CLabelImage *Labels1, CLabelImage *Labels2, int tolerance);

//============================================================================
//================================ Data access ===============================

	  int getWidth() const { return this->pLabelsOne->getWidth(); };

	  int getHeight() const { return this->pLabelsOne->getHeight(); };

	  float getAreaThreshold() const { return this->areaThreshold; };

//============================================================================

	  void clear();


//============================================================================


	  void determineOverlaps(float noneVsWeak, float weakVsPartial, 
		                     float partialVsStrong, int tolerance);

	  void writeData(CCharString filename) const;

	  void writeData(const CLabelOverlapVector &Vector1, const CLabelOverlapVector &Vector2, 
                     CCharString heading = CCharString(""), CCharString filename = CCharString("")) const;


	  void writeTIFFOne(CLabelOverlapVector &record, CCharString filename) const;


	  void writeTIFFTwo(CLabelOverlapVector &record, CCharString filename) const;

	  void writeLabelsCombined() const;

	  void writeCorrespondences(const CCharString &filename) const;

	  void getCombinedLabelImage(CLabelImage &labels) const;

//============================================================================

	  static CLookupTable splitMergeOverlapLUT();

//============================================================================

 protected:

//============================================================================

	  bool initCounter();

	  static void morphologicalOpen(CLabelImage *pLabelImage, int structSize);

	  void distributionOne(unsigned long indexOne, CMultipleLabelOverlapRecord &vectorOne);

	  void distributionTwo(unsigned long indexTwo, CMultipleLabelOverlapRecord &vectorTwo);


	  void openCombinedLabelImage(int structSize);

	  void compare(float noneVsWeak, float weakVsPartial, float partialVsStrong, 
  		           int tolerance);


	  void initBackgroundMask(CLabelImage *labelImg, int tolerance);

};

#endif
