#ifndef _M_ATTRIB_DEF_H_
#define _M_ATTRIB_DEF_H_

#include "XYLine.h"
#include "2DPoint.h"

#define CLOSED     1
#define STRAIGHT   2
#define CURVED_L   3
#define CURVED_R   4
#define CURVED_S   5
#define CURVED     6

typedef struct{
  unsigned char BreakPts_def;
  float tolerance,exponent;

  unsigned char Straight_def;
  float straight;

  unsigned char R_def;
  float roff,rwidth;

  int ColorSpace;    /* either HSV or Lab or NULL */
  int CompPhoto;    /* Are photometric attributes computed ? */

  unsigned char Rel_def;
  float ProxMaxDist;
  float ColMaxDist,ColMaxWedge,ColCone,ColSigma,ColThresh;
  float ParMaxAngle,ParMinWidth,ParMaxWidth;
  float CorMinAngle,CorMaxAngle,CorMaxDist;
  float AttrMaxDiff,AttrMaxIQR,AttrMaxColor;  

  int xoff,yoff,image_id;
}G_attributes;

typedef struct{
  float mdist;
  float curv;
  float area;
  unsigned char poly;
  unsigned char type;
} AIM_shape;

typedef struct{
  int no;                     /* number of points in region */
  float L[5];                 /* the five quartiles of grey-values */
  int L_robust;               /* robust estimates == True, classical = False */
  int L_out;                  /* number of photometric outliers */
  float L_mean,L_std;         /* photometric attributes */
  int ab_robust;              /* robust estimates == True, classical = False */
  int ab_out;                 /* number of chromatic outliers */
  float ab_mean[2],ab_std[2]; /* chromatic attributes */
} AIM_regions;


typedef struct{
  float pdir[2];               
  float strength;
  float length;
  unsigned char type;
  AIM_shape shape;
  CXYLine line;
  C2DPoint lineEndPoints[2];
//  AIM_regions regions[2];
//  int no_3D;
//  float *ddd;
} C_attributes;


#endif