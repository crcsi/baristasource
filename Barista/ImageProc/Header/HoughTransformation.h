//#ifndef __CHoughTransformation__
//#define __CHoughTransformation__

//#pragma once
#include "ImageRegion.h"


/**
 * Copyright:    Copyright (c) Petra Helmholz<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Petra Helmholz
 * @version 1.0
 */

//****************************************************************************

class CHoughTransformation
{
  protected:

	  float rho_min;

	  float rho_max;

	  float delta_rho;

	  float delta_phi;

	//  unsigned int width,

	 // unsigned int high;

	  int* puffer;
		 
//============================================================================

  public:
   
	  CHoughTransformation();

	  CHoughTransformation(float RhoMin, float RhoMax, float DeltaRho, float DeltaPhi);

	  virtual ~CHoughTransformation();
	
	  float getRhoMin() const { return rho_min; };

	  float getRhoMax() const { return rho_max; };

	  float getDeltaRho() const { return delta_rho; };

	  float getDeltaPhi() const { return delta_phi; };

	//  unsigned float getWidth() const { return width; };

	//  unsigned float getHigh() const { return high; };

	  int* getPuffer() const { return this->puffer;   };

//============================================================================

  protected: 

};

//#endif
