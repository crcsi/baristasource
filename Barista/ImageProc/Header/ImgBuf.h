#ifndef __CImageBuffer__
#define __CImageBuffer__

#include <vector>
using namespace std;

#include "ZObject.h"
#include "2DPoint.h"



enum eResamplingType { eNearestNeighbour, eBilinear, eBicubic };

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

class CXYPoint;
class CHistogram;
class CLookupTable;
class CTrans2D;
class CXYContourArray;
class CXYContour;
class CXYPolyLine;

class CImageBuffer : public ZObject
{
   protected:
	
	 long offsetX; // offsetx relative to parent image 				 	
	
	 long offsetY; // offsety relative to parent image

	 long width;

	 long height;

	 int bands;		// number of bands
	
	 int bpp;		//byte per pixel

	 unsigned long nGV;	//number of pixel * number of bands

	 unsigned long bufferLength;

  	 long diffX;

  	 long diffY;

  	 long diffBnd;

	 union
	 {
		 float   *pixF;
		 unsigned char *pixUC;
		 short   *pixS;
		 unsigned short   *pixUS;
	 };

	 bool unsignedFlag;

   public:

    CImageBuffer();

    CImageBuffer (const CImageBuffer &buf, bool copyValues = true);

	CImageBuffer (long I_offsetX, long I_offset_Y, long I_width, long I_height, 
		          int I_bands, int I_bpp, bool I_unsignedFlag, float value);

	CImageBuffer (long I_offsetX, long I_offset_Y, long I_width, long I_height, 
		          int I_bands, int I_bpp, bool I_unsignedFlag);

	virtual ~CImageBuffer();

	long getOffsetX() const { return offsetX; };	
	
	long getOffsetY() const { return offsetY; };

	C2DPoint getOffset() const { return C2DPoint(offsetX, offsetY); };

	long getWidth() const { return width; };

	long getHeight() const { return height; };

	int getBands() const { return bands; };
	
	int getBpp() const { return bpp; }		 	

	long getDiffX() const { return diffX; };

  	long getDiffY() const { return diffY; };

 	long getDiffBnd() const { return diffBnd; };

	/// Get index differences to neighbouring pixels in 4-neighbourhood:
	vector<int> get4NeigbourIndexDiffs() const; 

	//  Pattern of indices:
	//  ----------------> x
	//  |   | 3 |   |
	//  -------------
	//  | 2 | * | 0 |
	//  -------------
	//  |   | 1 |   |
	//  -------------
	//  |
	//  V
	//  y
	

	/// Get index differences to neighbouring pixels in 8-neighbourhood:
	vector<int> get8NeigbourIndexDiffs() const; 

	//  Pattern of indices:
	//  ----------------> x
	//  | 5 | 6 | 7 |
	//  -------------
	//  | 4 | * | 0 |
	//  -------------
	//  | 3 | 2 | 1 |
	//  -------------
	//  |
	//  V
	//  y

 
	int getBufferLength() const { return bufferLength; };	 	
	
	long getIndex(long row, long column) const 
	{ 
		return (row * width + column) * diffX; 
	};

	float getPercentil(float percent, int margin) const;

	bool getUnsignedFlag() const { return unsignedFlag; };

	unsigned long getNGV() const { return nGV; };

	void getRowColBand(long index, long &row, long &col, long &band) const;

	float *getPixF() const;

	unsigned char *getPixUC() const;

	short *getPixS() const;

	unsigned short *getPixUS() const;

	float &pixFlt( const unsigned long & index ) const
    {
		assert( index >= 0 && index < nGV );
		return pixF[ index ];
	};

	unsigned char &pixUChar( const unsigned long& index ) const
    {
		assert( index >= 0 && index < nGV );
		return pixUC[ index ];
	};

	unsigned short &pixUShort( const unsigned long& index ) const
    {
		assert( index >= 0 && index < nGV );
		return pixUS[ index ];
	};

	short &pixShort( const unsigned long& index ) const
    {
		assert( index >= 0 && index < nGV );
		return pixS[ index ];
	};

	string getClassName() const
	{	
		return string( "CImageBuffer");
	}

	bool isa(string& className) const
	{
		if (className == "CImageBuffer")
			return true;
		return false;
	}

	bool getInterpolatedColourVecBilin(double row, double col, unsigned char *colourVector) const;

	bool getInterpolatedColourVecBilin(double row, double col, unsigned short *colourVector) const;

	bool getInterpolatedColourVecBilin(double row, double col, short *colourVector) const;

	bool getInterpolatedColourVecBilin(double row, double col, float *colourVector) const;


	bool getInterpolatedColourVecBilin(const C2DPoint &p, unsigned char *colourVector) const
	{
		return getInterpolatedColourVecBilin(p.y - this->offsetY, p.x - this->offsetX, colourVector);
	};

	bool getInterpolatedColourVecBilin(const C2DPoint &p, unsigned short *colourVector) const
	{
		return getInterpolatedColourVecBilin(p.y - this->offsetY, p.x - this->offsetX, colourVector);
	};

	bool getInterpolatedColourVecBilin(const C2DPoint &p, short *colourVector) const
	{
		return getInterpolatedColourVecBilin(p.y - this->offsetY, p.x - this->offsetX, colourVector);
	};

	bool getInterpolatedColourVecBilin(const C2DPoint &p, float *colourVector) const
	{
		return getInterpolatedColourVecBilin(p.y - this->offsetY, p.x - this->offsetX, colourVector);
	};

	bool getInterpolatedColourVecBicub(const C2DPoint &p, unsigned char *colourVector) const;

	bool getInterpolatedColourVecBicub(const C2DPoint &p, unsigned short *colourVector) const;

	bool getInterpolatedColourVecBicub(const C2DPoint &p, short *colourVector) const;

	bool getInterpolatedColourVecBicub(const C2DPoint &p, float *colourVector) const;

	float getCorrelation(const CImageBuffer &buf) const;

	bool getCorrelation(const CImageBuffer &search, double xRef, double yRef, double xSeaMin, double ySeaMin,
		                double dxSea, double dySea, int dw, int dh, 
						const C2DPoint &AxisX, const C2DPoint &AxisY, vector<float> &rhoVector, int nSteps) const;

	bool getCorrelation(const CImageBuffer &search, double xRef, double yRef, double xSeaMin, double ySeaMin,
		                double dxSea, double dySea, int dw, int dh, vector<float> &rhoVector,
						int nSteps) const;

	void getColourVec(unsigned long index, float *colourVector) const;

	virtual bool set(long I_offsetX, long I_offsetY, long I_width, long I_height, int I_bands, int I_bpp, bool I_unsignedFlag);

	virtual bool set(long I_offsetX, long I_offsetY, long I_width, long I_height, int I_bands, int I_bpp, bool I_unsignedFlag, float value);

	virtual bool set(const CImageBuffer &buffer);

	virtual bool setOffset(long I_offsetX, long I_offsetY);

	void setAll(float value);

	void setMargin(int marginWidth, float value);

	virtual bool convertToUnsignedChar();

	bool convertToUnsignedChar(CLookupTable& lookupTable,unsigned char* outputBuffer = NULL);

	bool convertToUnsignedShort();

	virtual bool convertToShort();

	virtual bool convertToFloat();

	virtual bool convertToBinary(float value);

	bool extractSubWindow(long subOffsetX, long subOffsetY, long subWidth, long subHeight, 
	                      CImageBuffer &buffer) const;

	bool extractBand(int band, CImageBuffer &buffer);

	bool getMinMax(int marginWidth, vector<float> &min, vector<float> &max) const;

	bool getStatistics(int marginWidth, vector<float> &min, vector<float> &max, 
		               vector<float> &mean, vector<float> &median, vector<float> &sdev, bool Flag = false, float NoDataValue = -9999) const;

	CHistogram *getHistogram(int band);

	bool getGaussianNoise(int marginWidth, float alpha1, float alpha2, float factor, vector<float> &sigmaN);

	bool getPoissonNoise(int marginWidth, float alpha1, float alpha2, float factor, 
		                 vector<float> &sigmaNa, vector<float> &sigmaNb, float &avgGV);

	bool getGaussianNoise(int marginWidth, float factor, vector<float> &sigmaN)
	{
		return getGaussianNoise(marginWidth, 0.15f, 0.35f, factor, sigmaN);
	};

	bool getPoissonNoise(int marginWidth, float factor, vector<float> &sigmaNa, vector<float> &sigmaNb, float &avgGV)
	{
		return getPoissonNoise(marginWidth, 0.15f, 0.35f, factor, sigmaNa, sigmaNb, avgGV);
	};

	void RGBToIntensity();

	void SetToIntensity();

	static float getIntensity (const float &R, const float &G, const float &B);

	bool RGB2IHS();

	bool IHS2RGB(int I_bpp, bool I_unsignedFlag);

	bool pansharpen(const CImageBuffer &pan, float corrVal, float panFactor);
	bool pansharpen1(const CImageBuffer &pan, float corrVal, float panFactor);

	bool pansharpen(const CImageBuffer &pan, double *rotMat, float *mean, float meanPan);

    bool resample(const CImageBuffer &input, const CTrans2D &trafo, eResamplingType resType);

	bool resample(const CImageBuffer &input, int factor);

	void setMeanStdDev(float mean, float standardDev);

	static void RGB2IHS(float r, float g, float b, float &i, float &h, float &s);

	static void IHS2RGB(float i, float h, float s, float &r, float &g, float &b);

	static void RGB2IHS1(float r, float g, float b, float &i, float &h, float &s);

	static void IHS2RGB1(float i, float h, float s, float &r, float &g, float &b);

	float getCorner(C2DPoint approx, int opWidth, int opHeight, CXYPoint &corner, long &redundancy);

	float getCentre(C2DPoint approx, int opWidth, int opHeight, CXYPoint &centre, long &redundancy);

	void getCentreAndCorner(C2DPoint approx, int opWidth, int opHeight, 
		                    CXYPoint &centre, CXYPoint &corner, 
							float &m0sqCentre, float &m0sqCorner, long &redundancy);

	float getCorner(long approxRow, long approxCol, int opWidth, int opHeight, CXYPoint &corner, long &redundancy);

	float getCentre(long approxRow, long approxCol, int opWidth, int opHeight, CXYPoint &centre, long &redundancy);

	void getCentreAndCorner(long approxRow, long approxCol, int opWidth, int opHeight, 
		                    CXYPoint &centre, CXYPoint &corner, 
							float &m0sqCentre, float &m0sqCorner, long &redundancy);


	bool exportToTiffFile(const char *filename, CLookupTable *lookup, bool stretch) const;
	
	bool markEdgePixels(CImageBuffer &edgeBuf) const;

	bool getContours(CXYContourArray &contours, int neighbourhood = 9, 
		             const CImageBuffer *overallImg = 0, 
					 bool useSecondDegree = false, bool checkFourFirst = false);
	
	void fillByValue(const CXYPolyLine &poly, float value);

   protected:

	bool getContour(CXYContour &contour, bool useSecondDegree, bool checkFourFirst);

	bool getContour4Segmentation(CXYContour &contour, const CImageBuffer *overallImg);
	
   	bool allocateBuffer(float value);

	bool allocateBuffer();

	void deleteBuffer();

	bool setUpNormEquCornerCentre(long approxRow,      long approxCol, 
		                          int opWidth,         int opHeight,				   
								  double &NxxCorner,   double &NxyCorner,
								  double &NyyCorner, 
								  double &NxxCentre,   double &NxyCentre,
								  double &NyyCentre, 
								  double &Atl0Corner,  double &Atl1Corner, 
								  double &Atl0Centre,  double &Atl1Centre, 
								  double &omegaCorner, double &omegaCentre,
								  long &redundancy);

	float solveNormEq(const double &Nxx,   const double &Nxy,  const double &Nyy, 
	                  const double &Atl0,  const double &Atl1, const double &omega, 
					  long   redundancy,
					  CXYPoint &result);

	static void sort(long n, float *ra);

	float getSigma(const CHistogram &histogram, unsigned long numberOfPixels, float alpha1, float alpha2);

	static float cubic(float gm1, float g0, float g1, float g2, float dx);

	float *getTransformedTemplate(double xRef, double yRef, int w, int h, 
						          const C2DPoint &AxisX, const C2DPoint &AxisY,
						          double &mean, double &sdev) const;
};

#endif