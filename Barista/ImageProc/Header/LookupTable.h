#ifndef __CLookupTable__
#define __CLookupTable__

#include <vector>
using namespace std;

class CLookupTableHelper;

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

class CLookupTable: public vector < int >
{

	protected:

	  int bpp;

	  int bands;

	  int maxEntries;

  public:

	  CLookupTable( int I_bpp, int I_bands);

	  CLookupTable( const CLookupTable &lookup );

	  CLookupTable( const CLookupTable *pLookup );

	  virtual ~CLookupTable() {  };

	  bool isAllocated() const { return (this->size() != 0); };

	  int getMaxEntries() const { return maxEntries; };

	  int getBands() const { return bands; };

	  int getBpp() const { return bpp; };

	  virtual void getColor( int &r, int &g, int &b, int entry) const
	  {
		  r = (*this)[entry]; entry += maxEntries;
		  g = (*this)[entry]; entry += maxEntries;
		  b = (*this)[entry];
	  };

  
	  virtual void getColor( int &r, int &g, int &b, int red,int green, int blue) const
	  {
		  r = (*this)[red]; green += maxEntries;
		  g = (*this)[green]; blue += 2*maxEntries;
		  b = (*this)[blue];
	  };

	  virtual void setColor(int r, int g, int b, int entry )
	  {		  	
		  (*this)[entry] = r; entry += maxEntries;
		  (*this)[entry] = g; entry += maxEntries;
		  (*this)[entry] = b;
	  };

	  bool isGreyScale() const;   

	  bool isIdentity() const;

	  bool extractComponent( int component = 0 );

	  bool concatenateComponent( const CLookupTable &lookup, int component = 0 );

	  bool convertTo8Bit();

	  bool cutTo8Bit();

	  CLookupTable &operator = (const CLookupTable &other);

	  virtual bool read(const char* filename);

	  virtual bool write(const char* filename) const;

  protected:
  
	  void set(int Bands, int Bpp, bool initGrey);

	  void initGreyscale();
	
	  friend class CLookupTableHelper;
};

#endif
