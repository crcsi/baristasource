#ifndef __CLookupTableHelper__
#define __CLookupTableHelper__

#include <vector>

class CHistogram;
class CLookupTable;
class CCharString;
class CTransferFunction;


using namespace std;

class CLookupTableHelper
{
public:
	CLookupTableHelper(void);
	~CLookupTableHelper(void);

	// lookup tables for 8, 16 and 32 bit
	bool createLinearLUT(CLookupTable& newLUT,int bpp,double minValue, double maxValue,int clipValueMin = 0,int clipValueMax = 255);

	bool createEqualization(CLookupTable& newLUT,const CHistogram* histogram,int bpp, double minValue, double maxValue,int clipValueMin = 0,int clipValueMax = 255);

	bool changeBrightness(CLookupTable& newLUT, int bpp, int brightValue, int clipValueMin = 0,int clipValueMax = 255);

	bool createNormalisation(CLookupTable& newLUT,const CHistogram* histogram,int bpp,double minValue, double maxValue,double centre,double sigma,int clipValueMin = 0,int clipValueMax = 255);

	// lookup tables for dems
	bool createDEMDiffLUT(	CLookupTable& newLUT,
							double minValue, 
							double zeroValue, 
							double maxValue);

	bool createColorLegend(	const CTransferFunction* transferFunction,
							double minValue, 
							double zeroValue, 
							double maxValue,
							vector<int> &r,
							vector<int> &g,
							vector<int> &b,
							vector<CCharString> &text,
							int entries);


	static CLookupTable getDSMLookup();


	
//	void changeBrightness(CLookupTable* lut,CHugeImage* himage,int value);
	void computeFloatClasses(vector<float>& classes,int nClasses,float minValue,float zeroValue,float maxValue);
	
private:

	bool initMinMaxValues(double minValue, double maxValue);
	bool initMinMaxValues(const vector<CHistogram*>& histograms,int bands, int bpp);

	bool matchHistogram(CLookupTable& newLUT,const CHistogram* hist, const vector<double>& sumHistToMatch,int bpp, int minValue, int maxValue);

	void setClipValues(int clipMin,int clipMax,int bpp);

	unsigned short scaleGrey(int brightness, float grey)
	{
		float scaledVal = (grey - float(MinGrey)) * greyFactor + brightness + this->clipValueMin;
		if (scaledVal > this->clipValueMax) 
			scaledVal = float(this->clipValueMax);
		else if (scaledVal < this->clipValueMin) 
			scaledVal= float(this->clipValueMin);
                
        return (unsigned short)(scaledVal);
	}

	unsigned short scaleGrey(int brightness, unsigned short grey)
	{
		float scaledVal = (float(grey) - float(MinGrey)) * greyFactor + brightness + this->clipValueMin;
		if (scaledVal > this->clipValueMax) 
			scaledVal = float(this->clipValueMax);
		else if (scaledVal <  this->clipValueMin) 
			scaledVal=  float(this->clipValueMin);
                
        return (unsigned short)(scaledVal);
	}

	unsigned short scaleColour(unsigned short colour)
	{
		float scaledVal = (float(colour) - float(MinColour)) * colourFactor;
		if (scaledVal > this->clipValueMax) 
			scaledVal = float(this->clipValueMax);
		else if (scaledVal <  this->clipValueMin) 
			scaledVal=  float(this->clipValueMin);
                
        return (unsigned short)(scaledVal);
	}

	unsigned short scaleColour(int brightness, unsigned short colour)
	{
		float scaledVal = (float(colour) - float(MinColour)) * colourFactor + brightness;
		if (scaledVal > this->clipValueMax) 
			scaledVal = float(this->clipValueMax);
		else if (scaledVal <  this->clipValueMin) 
			scaledVal= float( this->clipValueMin);
                
        return (unsigned short)(scaledVal);
	}

	unsigned short brighten(int brightness, unsigned char colour)
	{
		brightness += colour;
		if (brightness > this->clipValueMax) 
			brightness = this->clipValueMax;
		else if (brightness <  this->clipValueMin) 
			brightness=  this->clipValueMin;
                
        return (unsigned short)(brightness);
	}

	int MinGrey;
	int MaxGrey;
	int MinNIR;
	int MaxNIR;
	int MinRed;
	int MaxRed;
	int MinGreen;
	int MaxGreen;
	int MinBlue;
	int MaxBlue;
	int MinColour;
	int MaxColour;
	float colourFactor;
	float greyFactor;

	int clipValueMax;
	int clipValueMin;


};
#endif
