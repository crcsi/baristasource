#ifndef __CImageFilter__
#define __CImageFilter__

#include "ZObject.h"
#include "CharString.h"

class CImageBuffer;

//********************* enum type describing the filter type *********************

enum eFilterType { eAverage,        // moving average filter, arbitrary extent
                   eSobelX,         // Sobel operator in X - direction; extent: 3 x 3 
                   eSobelY,         // Sobel operator in Y - direction; extent: 3 x 3 
                   eLaplaceA,       // Laplace filter, version A; extent: 3 x 3 
                   eLaplaceB,       // Laplace filter, version B; extent: 3 x 3 
                   eLaplaceC,       // Laplace filter, version C; extent: 3 x 3 
                   eLoG,            // LoG filter of arbitrary extent
                   eGauss,          // Gauss filter of arbitrary extent
                   eDiffX,          // First derivative of image in X; extent: 1 x 3
                   eDiffY,          // First derivative of image in Y; extent: 3 x 1
                   eDiff45,         // First derivative of image in 45 deg; extent: 3 x 3
                   eDiff135,        // First derivative of image in 135 deg; extent: 3 x 3
                   eDiffXGauss,     // First derivative of a gaussian in X; arbitrary extent
                   eDiffYGauss,     // First derivative of a gaussian in Y; arbitrary extent
                   eDiffXDeriche,   // First derivatives using the Deriche filter; arbitrary extent 
                   eDiffYDeriche,   // First derivatives using the Deriche filter; arbitrary extent 
				   eBinom,          // Binomial Filter
				   eChamfer3,       // 3*3 chamfering mask
				   eChamfer5,       // 5*5 chamfering mask
                   eMorphological,  // for morphological filtering
                   eMinimumFilter,  // binary minimum filter
                   eMaximumFilter}; // binary maximum filter

class CImageFilter : public ZObject 
{
   protected: 

     
	   int sizeX;

	   int sizeY;
     
	   eFilterType type;

	   float *filterValues;

	   float parameter;

//============================================================================

   public:

	   CImageFilter (eFilterType I_type, int I_sizeX, int I_sizeY, float I_parameter = 0.0f);

	   CImageFilter (const CImageFilter &filter);

	   ~CImageFilter();

	   float &operator[] ( unsigned long index ) const { return this->filterValues[index]; };

	   int operator ! () const { return (this->filterValues == NULL); };

	   int getSizeX() const { return sizeX; };

	   int getSizeY() const { return sizeY; };

	   int getLength() const { return sizeX * sizeY; };

	   eFilterType getType()  const { return type;  };

	   int getOffsetX() const { return sizeX / 2; };

	   int getOffsetY() const { return sizeY / 2; };

	   float getParameter() const { return parameter; };

	   float getSigmaFactor() const;

	   float getVarianceFactor() const;

	   bool isDerivative() const;

	   bool isSeparable() const;

	   bool set(eFilterType I_type, int I_sizeX, int I_sizeY, 
		        float I_parameter = 0.0f);

	   bool convolve(const CImageBuffer &source, CImageBuffer& destination, bool resetMargin) const;

	   bool gradientDeriche(const CImageBuffer &source, CImageBuffer& gradX, CImageBuffer& gradY, float sigma, bool resetMargin);

	   float getDericheFactor(float sigma);

	   bool binaryMorphologicalOpen (const CImageBuffer &source,  CImageBuffer& destination, float value) const;
	   bool binaryMorphologicalClose(const CImageBuffer &source, CImageBuffer& destination, float value) const;

	   bool binaryMinimumFilter (const CImageBuffer &source,  CImageBuffer& destination, float value) const;
	   bool binaryMaximumFilter(const CImageBuffer &source, CImageBuffer& destination, float value) const;

	   bool greyMorphologicalOpen (const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const;
	   bool greyMorphologicalClose (const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const;
	   bool greyRankOpen  (int rank, const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const;
	   bool greyRankClose (int rank, const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const;

	   void subtractFilter(CImageFilter F1);

	   string getClassName() const
	   {	
		   return string( "CImageFilter");
	   }


	   bool isa(string& className) const
	   {
		   if (className == "CImageFilter")
			   return true;
		   return false;
	   }

	   CImageFilter &operator= ( const CImageFilter& filter );

	   void dump(ostream &OutStream) const;

	   CCharString getDescriptor() const;

   protected:

	   bool minimumFilter(const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const;

	   bool maximumFilter(const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const;

	   bool minimumRankFilter(int rank, const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const;

	   bool maximumRankFilter(int rank, const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const;

	   float LoG(float x, float y, float sigma) const;

	   float Gauss2D(float x, float y, float Sigma) const;

	   float Gauss1D(float x, float Sigma) const;
        
	   long Binom(int n, int k) const;

	   void convolveUcUc(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveUcUs(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveUcSh(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveUcFl(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveUsUs(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveUsSh(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveUsFl(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveShUs(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveShSh(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveShFl(const CImageBuffer &source, CImageBuffer& destination) const;

	   void convolveFlFl(const CImageBuffer &source, CImageBuffer& destination) const;

};
  
#endif
