#ifndef __CIHSTransferFunction__
#define __CIHSTransferFunction__

#include "TransferFunction.h"
#include "ImgBuf.h"


class CIHSTransferFunction : public CTransferFunction
{
public:
	CIHSTransferFunction(int bpp);

	CIHSTransferFunction(const CTransferFunction* function);
	CIHSTransferFunction(const CTransferFunction& function);

	CIHSTransferFunction(const CIHSTransferFunction* function);

	CIHSTransferFunction(const CIHSTransferFunction& function);

	~CIHSTransferFunction(void);

	virtual CTransferFunction* copy() const;

	bool initLinearStretchIHS(const CFileName& imageFileName,int bpp,double minValue, double maxValue);

	bool initEqualizationIHS(const CFileName& imageFileName,int bpp, const CHistogram* intensityHist, double minValue, double maxValue);

	bool initNormalisationIHS(const CFileName& imageFileName,int bpp, const CHistogram* intensityHist, double minValue, double maxValue,double centre, double sigma);

	virtual bool updateTransferFunction(const CHistogram* histogram,const vector<double>& parameter);
	
	virtual bool changeBrightness(const CHistogram* histogram, int brightValue);

	// this function will just return the intensity 
	  virtual void getColor( int &r, int &g, int &b, int entry) const
	  {
		  		 
		  r = (*this)[entry]; 
		  g = (*this)[entry]; 
		  b = (*this)[entry];
	  };

	  // here we actually do the ihs trafo
	  virtual void getColor( int &r, int &g, int &b, int red,int green, int blue) const
	  {
		  float i,h,s,rf,gf,bf;
		  CImageBuffer::RGB2IHS((float)red,(float)green,(float)blue,i,h,s);
		  i = (float)(*this)[(int) i];
		  CImageBuffer::IHS2RGB(i,h,s,rf,gf,bf);
		  r = (unsigned char)rf;
		  g = (unsigned char)gf;
		  b = (unsigned char)bf;
	  };

	  virtual void setColor(int r, int g, int b, int entry )
	  {		  	
		  (*this)[entry] = r;

	  };

	virtual bool isa(string& className) const
	{
	  if (className == "CIHSTransferFunction")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CIHSTransferFunction"); }; 

};
#endif