#pragma once

#include "filter.h"

class CFileName;

enum eExtractionMode { eCanny, eFoerstner, eWaterShed };
enum eNoiseModel { eGaussian, ePoisson, eMedian };

class FeatureExtractionParameters
{
  protected:
	
	float HomogeneousThreshold;

	float alpha;

	float f_cor,f_cir;	// test value for corners and circles 
	 
	CImageFilter DerivativeFilterX; 

	CImageFilter DerivativeFilterY;
	 
    CImageFilter IntegrationFilterPoints;

    CImageFilter IntegrationFilterEdges;

    CImageFilter MorphologicFilterRegions;
	 
	int WatershedSmoothingIterations;

	float kForWatershed;

	bool ATKIS;
	unsigned long ATKIS_threshold;

   public:

	 FeatureExtractionParameters();

	 FeatureExtractionParameters(const FeatureExtractionParameters &pars);

	 ~FeatureExtractionParameters() {};

	 eExtractionMode Mode;

	 int distNonmaxEdges, distNonmaxPts; // Suppression distance 
	 bool extractPoints, extractEdges, extractRegions;
	 float qMin, wFactor; // thresholds for q and w; for w it will be wFactor * median(w)
	
	 float lineThinningThreshold;

	 float minLineLength;

	 float RegionSimilarityThreshold;
	 float RegionVarianceRatioThreshold;
	 float RegionTextureThreshold;

	 CImageFilter getDerivativeFilterX() const { return DerivativeFilterX; };

	 CImageFilter getDerivativeFilterY() const { return DerivativeFilterY; };
	 
	 CImageFilter getIntegrationFilterPoints() const { return IntegrationFilterPoints; };

	 CImageFilter getIntegrationFilterEdges()  const { return IntegrationFilterEdges; };
 
	 CImageFilter getMorphologicFilterRegion()  const { return MorphologicFilterRegions; };

	 int getMorphologicFilterSize() const { return MorphologicFilterRegions.getSizeX(); };

	 void setMorphologicFilterSize(int size);

	 int getOperSizeEdges() const { return IntegrationFilterEdges.getSizeX(); };

	 int getOperSizePts() const { return IntegrationFilterPoints.getSizeX();   };
	 	 
	 int getWatershedSmoothingIterations() const { return this->WatershedSmoothingIterations; };

	 void setWatershedSmoothingIterations(int it);

	 float getSigmaSmooth() const;

	 float getPointScale() const;

	 float getLineScale() const;

	 int getTotalOffset() const;

	 float getAlpha() const { return alpha; };

	

	 float getTotalVarianceFactorEdges() const;

	 float getTotalVarianceFactorPoints() const;

	 eNoiseModel NoiseModel;

	 float getKforWatershed() const { return this->kForWatershed; };
	 bool getATKIS() const {return this->ATKIS; };
	 unsigned long getATKIS_threshold () {return this->ATKIS_threshold; };

	 float getHomogeneousThreshold() const { return HomogeneousThreshold; };
	
	 float getCornerThreshold() const {return f_cor; };

	 float getCircleThreshold() const {return f_cir; };

	 bool  setDerivativeFilter (eFilterType typ, float sigma);

	 bool  setIntegrationFilter(eFilterType typ, int sizeEdges, int sizePoints, 
		                         float sigmaEdges, float sigmaPoints);

	 void  setAlpha(float I_alpha);

	 void  setKforWatershed(float k);

	 void  setATKIS(bool atkis_par);

	 void setATKIS_threshold (unsigned long ATKIS_threshold_par);
	 
	 void setHomogeneousThreshold(float percentil, int bands);

	 void writeParameters(const CFileName &fileName) const;

	 bool initParameters(const CFileName &fileName);
};

