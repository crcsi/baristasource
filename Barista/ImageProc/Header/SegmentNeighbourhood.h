#ifndef __CSegmentNeighbourhood__
#define __CSegmentNeighbourhood__

#pragma once
#include "ImageRegion.h"
#include "XYContourArray.h"
#include "SegmentAMatrix.h"

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//****************************************************************************

class CSegmentNeighbourhood
{
  protected:

	  unsigned int index;       /// Index of the pixel chain in the edge image

	  CImageRegion *pRegion1;   /// First segment separated by this boundary

	  CImageRegion *pRegion2;   /// Second segment separated by this boundary

	  CImageRegion CountorFeatures;
   
	  CXYContourArray contours;	/// Edge pixel chains separating the two regions
   
	  float distance;           /// distance metric between the two segments

	  float countor_region1_distance; /// distance metric between the countor and region1

	  float countor_region2_distance; /// distance metric between the countor and region2

	  float boundaryTexture;

	  float varianceRatio;

	 	  
  
//============================================================================

  public:
   
	  CSegmentNeighbourhood();

	  CSegmentNeighbourhood(unsigned int Index, CImageRegion *pR1, CImageRegion *pR2);

	  CSegmentNeighbourhood(const CSegmentNeighbourhood &neighbour);

	  virtual ~CSegmentNeighbourhood();
	
	  unsigned int getIndex() const { return index; };

	  CImageRegion *getRegion1() const { return this->pRegion1;   };

	  CImageRegion *getRegion2() const { return this->pRegion2;   };

	  CImageRegion getCountorFeatures() const {return this->CountorFeatures;};

	  unsigned short getRegionIndex1() const;

	  unsigned short getRegionIndex2() const;
		 
	  CSegmentNeighbourhood &operator = (const CSegmentNeighbourhood &neighbour);
 
	  bool operator < (const CSegmentNeighbourhood &neighbour);
	  bool operator <= (const CSegmentNeighbourhood &neighbour);
	  bool operator >= (const CSegmentNeighbourhood &neighbour);
	  bool operator > (const CSegmentNeighbourhood &neighbour);

	  int getNcontours() const { return this->contours.GetSize(); };
	  
	  CXYContour *getContour(int i) const { return this->contours.GetAt(i); };

	  unsigned long getRegion1NPixels() const {return this->pRegion1->getNPixels();  };

	  unsigned long getRegion2NPixels() const {return this->pRegion2->getNPixels();  };

	  float getDistanceMetric () const { return this->distance; };

	  float getCountorRegion1DistanceMetric () const { return this->countor_region1_distance; };

	  float getCountorRegion2DistanceMetric () const { return this->countor_region2_distance; };

	  float getBoundaryTexture() const { return this->boundaryTexture; };

	  float getVarianceRatio() const { return this->varianceRatio; };

	  void init(unsigned int Index, CImageRegion *pR1, CImageRegion *pR2);

	  void initTexture(const int *boundary, const CImageBuffer &texture);

	  void initVarianceRatio();

	  void initDistance();

	  void initContour(const CLabelImage &labels, const CImageBuffer &ROI, int *neighbourMat);

	  void merge(CLabelImage &labels, int *boundary, CSegmentAMatrix &matrix, const CImageBuffer &img);

	  void closing(CLabelImage &labels, int *BoundaryPixels);
	  
	  void dump(ofstream &oFile) const;

	  void setIndex(unsigned int dimension);

	  void exportContours(ofstream &dxfFile);
	  void exportContoursToBuffer(CImageBuffer &contourBuffer);


  protected: 

};

#endif
