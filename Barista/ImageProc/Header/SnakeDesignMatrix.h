/**
 * Copyright:    Copyright (c) Petra Helmholz<p>
 * Company:      University of Melbourne<p>
 * @author PetraH
 * @version 1.0
 */


#ifndef __CSnakeDesignMatrix__
#define __CSnakeDesignMatrix__

#include <vector>
#include "SnakePars.h"

using namespace std;

class CFileName;
class CCharString;

/*!
 * \brief
 * Currently not in use
 */

//****************************************************************************

class CSnakeDesignMatrix : protected vector<float>
{
  protected:

	  unsigned int nPoints;
	
  
//============================================================================

  public:
   
	  CSnakeDesignMatrix(int npoints);

	  ~CSnakeDesignMatrix();

	  unsigned int getNPoints() const { return this->nPoints; };

	  unsigned int getLength() const { return this->size(); };

	  const float &operator [] (unsigned int index) const { return this->vector<float>::operator[](index); }

	  float &operator [] (unsigned int index) { return this->vector<float>::operator[](index); }

	  const float &operator () (unsigned int n, unsigned int m)  const
	  { return (*this)[PointsToIndex(n, m)]; }

	  float &operator () (unsigned int n, unsigned int m) 
	  { return (*this)[PointsToIndex(n, m)]; }

	  void incrementAdjacency(unsigned int n, unsigned int m)
	  { (*this)[PointsToIndex(n, m)] += 1; };
 
	  void decrementAdjacency(unsigned int n, unsigned int m)
	  { (*this)[PointsToIndex(n, m)] -= 1; };
 
	 //calculation of the Index by given regions			
	  unsigned int PointsToIndex(unsigned int n, unsigned int m) const
	  {
		  	if (n < m) return PointsToIndex(n, m);

			return ((n - 1) * this->nPoints - (n - 1) * n / 2 + m - 1 - n);
	  };

	  void initializeConfigurateDesignMatrix(eSnakeMode SnakeMode, bool open, float alpha, float beta, float gamma); 
				// Configurated because gamma is already added to the main digonal

//	  void dump(ofstream &oFile) const; 
//	  void dump(const CCharString &FileName) const; 


	
  protected: 

};

#endif

