#ifndef __CTransferFunction__
#define __CTransferFunction__

#include "LookupTable.h"
#include "Serializable.h"
#include "Filename.h"
#include <vector>


class CHistogram;
class CImageBuffer;

class CTransferFunction : public CLookupTable, public CSerializable
{
public:

	typedef enum {	eDirectTransfer,
					eLinearStretch,
					eColorIndexed,
					eLinearIHSStretch,
					eEqualization, 
					eIHSEqualization,
					eNormalisation,
					eIHSNormalisation,
					eWallisFilter} Type;

	CTransferFunction(int bpp, int bands);

	CTransferFunction(const CTransferFunction* function);

	CTransferFunction(const CTransferFunction& function);

	~CTransferFunction(void);

	virtual CTransferFunction* copy() const;

	Type getTransferFunctionType() const { return this->type;};

	bool isColorIndexed() const; 

	bool isInitialized() const { return this->bIsInitialized;};

	// reads the lookup table direct and initializes the object with 'eDirectTransfer'
	virtual bool read(const char* filename);

	// inits the table as eLinearStretch, scales linear between minValue and maxValue
	virtual bool initLinearStretch(const CFileName& imageFileName, int bpp, double minValue, double maxValue);

	// inits the table for direct transfer, no change of values
	virtual bool initDirectTransfer(const CFileName& imageFileName, int bpp, int bands);

	// inits with existing lut, we have to specify the type in case its not eColorIndexed
	virtual bool init(CTransferFunction::Type type,const CFileName& imageFileName,const CLookupTable& lut);

	virtual bool initEqualization(const CFileName& imageFileName,int bpp, const CHistogram* histogram,double minValue, double maxValue);
	
	virtual bool changeBrightness(const CHistogram* histogram, int brightValue);

	virtual bool initNormalisation(const CFileName& imageFileName,int bpp, const CHistogram* histogram, double minValue, double maxValue,double centre,double sigma);


	virtual bool updateTransferFunction(const CHistogram* histogram,const vector<double>& parameter);

	virtual bool applyTransferFunction(const CImageBuffer& srcBuf,CImageBuffer& destBuf,int actualHeight,int actualWidth,int zoomFactor,bool zoomUp) {return true;};

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified(){return this->bModified;};
	virtual void reconnectPointers();
	virtual void resetPointers();

	virtual bool isa(string& className) const
	{
	  if (className == "CTransferFunction")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CTransferFunction"); }; 

	static void addLUTExtension(const CFileName& imageFileName,CFileName& lookupTableName);

	double getParameter(unsigned int index) const;
	vector<double> getParameters() const {return this->parameter;};
	void setParameter(unsigned int index, double value);


	virtual bool getUnmappedLUT(CLookupTable& newLUT,const CHistogram* histogram);

protected:

	void addMinMaxToParameter(double minValue, double maxValue);

	CFileName lookupTableName;
	Type type;
	vector<double> parameter;

	int offsetParameters;

	bool bIsInitialized;

};
#endif
