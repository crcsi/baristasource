//
// C++ Interface: %{MODULE}
//
// Description: 
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef HISTOGRAMFLT_H
#define HISTOGRAMFLT_H

#include "CharString.h"
#include <vector>

class CHistogramFlt
{

 protected:
    
    unsigned long* histogram;

    unsigned long size;

    unsigned long nElements;
   
    float minEntry;

    float maxEntry;

    float binSize;
    
 public:

    CHistogramFlt();

    CHistogramFlt(float I_minEntry, float I_maxEntry, float I_binSize);

    ~CHistogramFlt();

    int minimum() const;
    int maximum() const;
    float getPercentile(double percent) const;

	unsigned long value(const unsigned long &u) const { return histogram[u]; };
    
	unsigned long getIndex(float value) const;
    
	float getPercentage(const unsigned long &u) const	 
	{ return float(histogram[u]) / float(nElements); }

    float getClassCentre(const unsigned long &u) const
	{ return minEntry + (float(u) + 0.5f) * binSize; }

    void addValue(float value);

    void addValue(long value);

    void increment(const unsigned long &u);

	unsigned long getSize() const { return size; };
    
    unsigned long getNElements() const { return nElements; };

	CCharString getDescriptorString(const CCharString &heading, int headingPrecision, const CCharString &separator, int &colWidth) const;

	CCharString getString(const CCharString &heading, int headingPrecision, const CCharString &separator) const;

//=============================================================================

    void init(float I_minEntry, float I_maxEntry, float I_binSize);
	void init(vector<float>& classCentres);

    void init();

 
//=============================================================================


};

#endif
