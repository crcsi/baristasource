#ifndef __CImageRegion__
#define __CImageRegion__


#include "ZObject.h"
#include "2DPoint.h"
#include "newmat.h"

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */
class CLabelImage;
class CImageBuffer;

//****************************************************************************

class CImageRegion : public ZObject
{
  protected:

	  unsigned short index;    /// Index of the region in the label image

	  unsigned long nPixels;   /// Number of pixels in the region
   
	  short bands;	           /// Number of bands of image
   
	  double *greyValues;      /// Average grey levels of all bands

	  SymmetricMatrix covarianceInv;    /// Inverse of the covariance matrix of the grey levels

	  SymmetricMatrix covariance;	   /// Covariance matrix of the grey levels

	  SymmetricMatrix sqrGreyValues;    /// Matrix of the soms of the squaresand mixed products of the grey levels
   	  
	  C2DPoint minPos;         /// Coordinates of the upper left corner of the bounding rectangle

	  C2DPoint maxPos;         /// Coordinates of the lower right corner of the bounding rectangle

	  C2DPoint cog;            /// Centre of gravity of the region

	  C2DPoint extents;        /// extents of the rectangle computed from the moments of the region

	  float angle;             /// direction angle of the rectangle computed from the moments of the region

	  double xx, xy, yy;       /// square sum of the coordinates of the region's pixels

	  int NNeigh;				//Number of neighbour labels
  
//============================================================================

  public:
   
	  CImageRegion();
	  CImageRegion(unsigned short I_index, int I_bands);
	  CImageRegion(const CImageRegion &region);

	  virtual ~CImageRegion();

	  string getClassName() const
	  {	
		  return string( "CImageRegion");
	  }

	  bool isa(string& className) const
	  {
		  if (className == "CImageRegion")
			  return true;
		  return false;
	  }
	
	  unsigned short getIndex() const { return index; };

	  unsigned long getNPixels() const { return nPixels; };

	  float greyLevel(int bnd) const { return (float) greyValues[bnd]; };

	  float getMahalanobis(float *I_greyValues) const;

	  float getDistanceMetric (const CImageRegion &reg) const;

	  float getSigma() const;

	  int getNNeigh() const { return this->NNeigh; };

	  short getBands() const { return this->bands; };

	  bool isConsistent(float *I_greyValues) const;

	  void addNNeigh(int value);

	  void setNNeigh(unsigned int new_value) { this->NNeigh = new_value; };
	  
	  float getAngle() const { return angle; };

	  const SymmetricMatrix &getCovarianceInv() const { return covarianceInv; };

	  const SymmetricMatrix &getCovariance() const { return covariance; };

	  C2DPoint getCOG() const { return cog; };
  
	  C2DPoint getExtents() const { return extents; };
 
	  C2DPoint getMinPos() const { return minPos; };

	  C2DPoint getMaxPos() const { return maxPos; };

	  C2DPoint getWinExtents() const { return C2DPoint(maxPos.x - minPos.x + 1, maxPos.y - minPos.y + 1); };

	  void copyData(unsigned short I_index, const CImageRegion &region);

	  void resetData(unsigned short I_index = -1);

	  bool isIdentical(const CImageRegion &region) const;

	  bool merge(CImageRegion &region);

	  void initParameters4Computation(int nbands);                 

	  void initParameters(const CImageBuffer &image,                 
						  const CLabelImage  &labelImage);

	  void initParameters(const CLabelImage  &labelImage);

	  void addColourVec(float *colourVec, long x, long y);

	  bool computeRegionPars();

	  void dump(ofstream &ofile) const;

	  void UpdateNNeigh(const unsigned short r) {this[r].addNNeigh(-1); };

  protected: 

};

#endif
