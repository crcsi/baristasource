#ifndef __CRAG__
#define __CRAG__


/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      Leibniz Universitšt Hannover, Institute of Photogrammetrie<p>
 * @author Franz Rottensteiner, Petra Helmholz
 * @version 1.0
 */

#include <vector>
using namespace std;
#include "SegmentNeighbourhood.h"
#include "SegmentNeighbourhoodVector.h"

class CCharString;
class CLabelImage;
class CImageRegion;
class CSegmentAMatrix;
class CImageBuffer;
class FeatureExtractionParameters;

/*!
 * \brief
 * CRAG is a container class describing a region adjacency graph (RAG) in a digital image.
 * 
  	 It is based on a label image representing the results of a segmentation algorithm.
	 Based on the label image, it contains a vector of image regions with their 
	 radiometric and geometric features, and it detects and models the neighbourhood relations
	 between these regions.
	
 * 
 * \see
 * CSegmentNeighbourhood | CSegmentNeighbourhoodVector | CImageRegion | CLabelImage | CSegmentAMatrix
 */

class CRAG
{
  protected:
	
	  //! The label image that is passed on to CRAG for analysis
	  CLabelImage *pLabelImage;

	  //! A vector of image regions. The entry at index i corresponds to label i in *pLabelImage
	  vector<CImageRegion *> *pRegionVector;

	  //! A vector containing the neighbourhood relations between the image regions in *pLabelImage
	  CSegmentNeighbourhoodVector *pNeighbourVector;

	  //! A matrix used to represent the adjacencies between the image regions in *pLabelImage
	  CSegmentAMatrix *pAdjacencyMatrix;
	
	
	  //! A digital image that shows the boundary pixels between image regions in *pLabelImage
	  int *pBoundaryPixels;

//============================================================================

  public:
   
	  CRAG();

	  CRAG(CLabelImage *plabelimage, const CImageBuffer &image,
		   const CImageBuffer *textureImage, int morphSize);

	  CRAG(const CRAG &rag);

	  virtual ~CRAG();
	
//============================================================================

	  bool isInitialised() const { return (this->pRegionVector != 0); };


	  unsigned int getNRegions() const { return this->pRegionVector->size(); }

	  CImageRegion *getRegion(int index) const { return (*this->pRegionVector)[index]; }

	  int getNNeighbours() const { return this->pNeighbourVector->getSize(); }

	  const CSegmentNeighbourhood &getNeighbour(int index) const { return (*this->pNeighbourVector)[index]; }
	  CSegmentNeighbourhood &getNeighbour(int index) { return (*this->pNeighbourVector)[index]; }


//============================================================================

	  bool initialise(CLabelImage *plabelimage, const CImageBuffer &image, 
		              const CImageBuffer *textureImage, int morphSize);

	  void clear();

	  	 
//============================================================================

	  bool merge(const CImageBuffer &img, const CImageBuffer *textureImage, 
		         const FeatureExtractionParameters &extractionPars, bool forAtkis, unsigned long ATKIS_threshold);

	  void renumber(const CImageBuffer &image, const CImageBuffer *textureImage,
		            int morphSize); 


//============================================================================

	  void exportAll(const CCharString &filenameBase, int offsetX, int offsetY); 

	  void exportRegions(const CCharString &filename) const; 

	  void exportRegionCentres(const CCharString &filename) const; 

	  void exportNeighbourhoods(const CCharString &filename) const; 

	  void exportBoundaryPixels(const CCharString &filename) const; 

	  void exportContoursToDXF(const CCharString &filename); 

	  void exportContoursToTIFF(const CCharString &filename, int offsetX, int offsetY); 

//============================================================================

  protected: 

//============================================================================

	  void exportLabelRegionNeighbours(int index);

	  void plausibilityCheck(unsigned long nPix,  const CImageBuffer &image,  
		                     const CImageBuffer *texture);

	  void plausibilityCheckATKIS(unsigned long nPix,  
		                          const CImageBuffer &image, 
								  const CImageBuffer *texture);

//============================================================================

};

#endif

