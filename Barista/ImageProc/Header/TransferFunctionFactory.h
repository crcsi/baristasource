#ifndef __CTransferFunctionFactory__
#define __CTransferFunctionFactory__


#include "TransferFunction.h"
#include <vector>
using namespace std;


class CFileName;
class CHistogram;
class doubles;

class CTransferFunctionFactory
{
protected:
	// don't allow creation of this object
	CTransferFunctionFactory(void){};
	~CTransferFunctionFactory(void){};

public:
	static CTransferFunction* create(const CTransferFunction& transferFunction);
	static CTransferFunction* create(const CTransferFunction* transferFunction);
	static CTransferFunction* createFromLUT(CTransferFunction::Type type,const CFileName& imageFileName,const CLookupTable& lut);
	static CTransferFunction* createDirectTransfer(const CFileName& imageFileName,int bands,int bpp);
	static CTransferFunction* createLinearStretch(const CFileName& imageFileName,int bpp,double minValue, double maxValue);
	static CTransferFunction* createLinearStretchIHS(const CFileName& imageFileName,int bpp,double minValue, double maxValue);
	static CTransferFunction* createEqualization(const CFileName& imageFileName,int bpp,const CHistogram* histogram,double minValue, double maxValue);
	static CTransferFunction* createEqualizationIHS(const CFileName& imageFileName,int bpp,const CHistogram* intensityHist,double minValue, double maxValue);
	static CTransferFunction* createNormalisation(const CFileName& imageFileName,int bpp,const CHistogram* histogram,double minValue, double maxValue,double centre,double sigma);
	static CTransferFunction* createNormalisationIHS(const CFileName& imageFileName,int bpp,const CHistogram* intensityHist,double minValue, double maxValue,double centre,double sigma);
	static CTransferFunction* createWallisFilterFunction(const CFileName& imageFileName,int bpp, double minValue, double maxValue);

};
#endif