#ifndef __CWallisFilter__
#define __CWallisFilter__

#include "TransferFunction.h"
#include "ImgBuf.h"
#include "doubles.h"

#define N_PARAMETERS 12 // update when adding more parameters!!!

#define INDEX_WINDOWSIZEX 0
#define INDEX_WINDOWSIZEY 1
#define INDEX_CONTRAST 2
#define INDEX_BRIGHTNESS 3
#define INDEX_MEAN 4
#define INDEX_STDDEV 5
#define INDEX_NPARAMETERSX 6
#define INDEX_NPARAMETERSY 7
#define INDEX_OFFSETX 8
#define INDEX_OFFSETY 9
#define INDEX_WIDTH 10
#define INDEX_HEIGHT 11

#define ROUND(x,max) (((x) < 0) ? 0 : ((x > max) ? max : x))

#define DEFAULT_WINDOWSIZE 40
#define DEFAULT_MEAN_8BIT 127
#define DEFAULT_MEAN_16BIT 32768
#define DEFAULT_STDDEV_8BIT 60
#define DEFAULT_STDDEV_16BIT 100
#define DEFAULT_CONTRAST 0.96
#define DEFAULT_BRIGHTNESS 0.95


class CWallisFilter : public CTransferFunction
{
public:
	CWallisFilter(int bpp,int bands);

	CWallisFilter(const CWallisFilter* function);

	CWallisFilter(const CWallisFilter& function);

	~CWallisFilter(void);

	virtual CTransferFunction* copy() const;

	bool init(const CFileName& imageFileName, double minValue, double maxValue);

	virtual bool updateTransferFunction(const CHistogram* histogram,const vector<double>& parameter);

	virtual bool applyTransferFunction(const CImageBuffer& srcBuf,CImageBuffer& destBuf,int actualHeight,int actualWidth,int zoomFactor,bool zoomUp);

	  virtual void getColor( int &r, int &g, int &b, int entry) const
	  {
		  r = (*this)[entry]; 
		  g = (*this)[entry]; 
		  b = (*this)[entry];
	  };

	  virtual void getColor( int &r, int &g, int &b, int red,int green, int blue) const
	  {

		  r = red;
		  g = green;
		  b = blue;
	  };

	  virtual void setColor(int r, int g, int b, int entry )
	  {		  	
		  (*this)[entry] = r;

	  };

	virtual bool isa(string& className) const
	{
	  if (className == "CWallisFilter")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CWallisFilter"); }; 

	doubles& getMeanVector() {return this->meanValues;};

	doubles& getStdDevVector(){return this->stdDevValues;};

	bool hasIdenticalParameters(int windowSizeX,
								int windowSizeY,
								double contrastParameter, 
								double brightnessParameter,
								double mean,
								double stdDev,
								int offsetX,
								int offsetY,
								int width,
								int height);

	bool setParameters(int windowSizeX,
				int windowSizeY,
				int nParametersX,
				int nParametersY,
				double contrastParameter, 
				double brightnessParameter,
				double mean,
				double stdDev,
				int offsetX,
				int offsetY,
				int width,
				int height);

	int getMeanValue()const { return (int)this->parameter[this->offsetParameters + INDEX_MEAN];}; 
	int getStdDevValue()const { return (int)this->parameter[this->offsetParameters + INDEX_STDDEV];}; 
	double getContrastParameter()const { return this->parameter[this->offsetParameters + INDEX_CONTRAST];}; 
	double getBrightnessParameter()const { return this->parameter[this->offsetParameters + INDEX_BRIGHTNESS];}; 
	int getWindowSizeX()const { return (int)this->parameter[this->offsetParameters + INDEX_WINDOWSIZEX];}; 
	int getWindowSizeY()const { return (int)this->parameter[this->offsetParameters + INDEX_WINDOWSIZEY];};
	int getOffsetX()const { return (int)this->parameter[this->offsetParameters + INDEX_OFFSETX];}; 
	int getOffsetY()const { return (int)this->parameter[this->offsetParameters + INDEX_OFFSETY];};
	int getWidth()const { return (int)this->parameter[this->offsetParameters + INDEX_WIDTH];}; 
	int getHeight()const { return (int)this->parameter[this->offsetParameters + INDEX_HEIGHT];};
	int getNParametersX()const {return (int)this->parameter[this->offsetParameters + INDEX_NPARAMETERSX];};
	int getNParametersY()const {return (int)this->parameter[this->offsetParameters + INDEX_NPARAMETERSY];};

	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	virtual bool getUnmappedLUT(CLookupTable& newLUT,const CHistogram* histogram);
	
	void writeWallisFilterFile();

protected:
	
	void initBilinearCoefs();
	void initParameters();

	void addWALExtension(const CFileName& lookupTableName,CFileName& wallisFileName);
	bool readWallisFilterFile();
	bool readwallisFilterFileHeader(FILE *in,vector<double>& parameters);
	

	void selectFunction(int bpp,int bands);

	void updateLUT();

	doubles bilinearCoefX_left;
	doubles bilinearCoefX_right;

	doubles bilinearCoefY_up;
	doubles bilinearCoefY_down;


	doubles R0;
	doubles R1;

	// these vectors are only here so we save copying all the values when updating
	doubles meanValues;
	doubles stdDevValues;

	CFileName wallisFileName;

	void (*transformPixel)(double& r0,double& r1,int index,const CImageBuffer& srcBuf,CImageBuffer& destBuf);

	static inline void transform8Bit_1Band(double& r0,double& r1,int index,const CImageBuffer& srcBuf,CImageBuffer& destBuf) 
	{
		destBuf.pixUChar(index) = (unsigned char)ROUND(r0 + r1 * srcBuf.pixUChar(index),255);
	};
	
	static inline void transform16Bit_1Band(double& r0,double& r1,int index,const CImageBuffer& srcBuf,CImageBuffer& destBuf) 
	{
		destBuf.pixUShort(index) = (unsigned short)ROUND(r0 + r1 * srcBuf.pixUShort(index),65535);
	};
	
	static inline void transform32Bit_1Band(double& r0,double& r1,int index,const CImageBuffer& srcBuf,CImageBuffer& destBuf)
	{
		destBuf.pixFlt(index) = (float)(r0 + r1 * srcBuf.pixFlt(index));
	};

	static inline void transform8Bit_MultiBand(double& r0,double& r1,int index,const CImageBuffer& srcBuf,CImageBuffer& destBuf)
	{
		  float i,h,s,rf,gf,bf;
		  CImageBuffer::RGB2IHS1(float(srcBuf.pixUChar(index)),float(srcBuf.pixUChar(index+1)),float(srcBuf.pixUChar(index+2)),i,h,s);
		  i =float(r0 + r1 * i);
		  CImageBuffer::IHS2RGB1(i,h,s,rf,gf,bf);
		  destBuf.pixUChar(index    ) = (unsigned char)ROUND(rf,255);
		  destBuf.pixUChar(index + 1) = (unsigned char)ROUND(gf,255);
		  destBuf.pixUChar(index + 2) = (unsigned char)ROUND(bf,255);
	};

	static inline void transform16Bit_MultiBand(double& r0,double& r1,int index,const CImageBuffer& srcBuf,CImageBuffer& destBuf)
	{
		  float i,h,s,rf,gf,bf;
		  CImageBuffer::RGB2IHS1(float(srcBuf.pixUShort(index)),float(srcBuf.pixUShort(index+1)),float(srcBuf.pixUShort(index+2)),i,h,s);
		  i =float(r0 + r1 * i);
		  CImageBuffer::IHS2RGB1(i,h,s,rf,gf,bf);
		  destBuf.pixUShort(index    ) = (unsigned short)ROUND(rf,65535);
		  destBuf.pixUShort(index + 1) = (unsigned short)ROUND(gf,65535);
		  destBuf.pixUShort(index + 2) = (unsigned short)ROUND(bf,65535);
	};

};


#endif