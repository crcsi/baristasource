#ifndef ___CMatchRecord_H___
#define ___CMatchRecord_H___

#include "2DPoint.h"
#include "XYPoint.h"
#include "ImgBuf.h"

class CMatchRecord
{
  protected:

	  CXYPoint P;

	  CImageBuffer greybuf;

	  vector<CMatchRecord *> hypotheses;
	
	  vector<C2DPoint> residuals;

	  vector<float> sigmaResVec;

	  vector<float> rhoVec;

	  void addHypothesis(CMatchRecord *hypothesis, float rho); 

	  void deleteHypothesis(CMatchRecord *hypothesis);

	  void swapHypotheses(int i, int j);

  public:

	  CMatchRecord() { };

	  CMatchRecord(const CXYPoint &p, const CImageBuffer &buf, int width, int height);

	  ~CMatchRecord();

	  int nHypotheses() const { return hypotheses.size(); };

	  CMatchRecord *getHypothesis(int index) const { return this->hypotheses[index]; };

	  CMatchRecord *getBestHypothesis(int &indexOfBest) const;

	  C2DPoint getPos() const { return P; };

	  C2DPoint &residual(int index) { return this->residuals[index]; };

	  const CXYPoint &getP() const { return P; };

	  float &sigmaRes(int index)  { return this->sigmaResVec[index]; };

	  float rho(int index) const { return this->rhoVec[index]; };

	  void init(const CXYPoint &p, const CImageBuffer &buf, int width, int height); 

	  float checkHypothesis(CMatchRecord *hypothesis, float threshold); 

	  void deleteHypothesis(int index);

	  void deleteAllHypotheses();

	  void clarifyHypotheses();

	  void setResidual(const C2DPoint &res, float sigmaRes, CMatchRecord * hypothesis);
	

};

#endif