//
// C++ Interface: %{MODULE}
//
// Description: 
//
//
// Author: %{AUTHOR} <%{EMAIL}>, (C) %{YEAR}
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CHISTOGRAM_H
#define CHISTOGRAM_H

#include "stdio.h"

class CHistogram
{
public:
    CHistogram(int BPP);
    ~CHistogram();

	CHistogram &operator = (const CHistogram &other);

    unsigned long value(int i) const;

	void setValue(int index,unsigned long value);

	int minimum() const;
    int maximum() const;
    int percentile(double percent) const;
	float mean() const;

	void increment(int i);
    int getSize() const;

	int getBpp() const { return this->bpp;};

	float getMin() const;

	float getMax() const;

	void resetFltMinMax();

	void checkFltMinMax(float val);

	void updateMinMax(double minPercentile,double maxPercentile);

	void reset();

/**
 * Write histogram data to binary file fp; 
 * fp must be open in write mode
 * @return true if writing was successful
 */
	bool writeHistogram(FILE *fp) const;

/**
 * Read histogram data from binary file fp; 
 * fp must be open in read mode 
 * @return true if reading was successful
 */
	bool readHistogram(FILE *fp);

/**
 * Direct access to histogram data
 * @return pointer to histogram data (unsigned long array)
 */
	const unsigned long *data() const { return this->histogram; }

protected:

    int bpp;
    int size;
    unsigned long* histogram;

	unsigned long minValue;
	unsigned long maxValue;
};

#endif
