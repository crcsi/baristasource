/**
 * Copyright:    Copyright (c) Petra Helmholz<p>
 * Company:      University of Melbourne<p>
 * @author Petra Helmholz
 * @version 1.0
 */

#pragma once

#include "XYPoint.h"
#include "ImgBuf.h"
#include "ProgressHandler.h"
#include "SnakePars.h"
#include "SnakeDesignMatrix.h"
#include "SnakePointsVec.h"

#include "newmat.h"

#define HOMOGENEOUS       250

class CLabelImage;
class CXYPointArray;
class C2DPoint;

/*!
 * \brief
 * Loop for snake calculation including the claculation of the 
 * Gaussian smooth image, derivations, design Matrices and addtional terms for the 
 * external energy for Balloon and GVF snakes.
 * 
 */

class CSnakeExtractor : protected CImageBuffer, public CProgressHandler
{
  protected:
		
	SnakeExtractionParameters extractionParameters;
	//CXYPointArray snakeStartingPoints;

	CImageBuffer *featRoi;					///< Local pointer to the image data buffer
	CImageBuffer GaussianImage;				///< Gaussian smooth images 
	CImageBuffer *pGradXImage;				///< Pointer to the derivation image in x direction
	CImageBuffer *pGradYImage;				///< Pointer to the derivation image in y direction

	//CSnakeDesignMatrix *pDesignMatrix;	///< Design matrix

	CSnakePointsVec *pSnakeVector;			///< Vector with all vertices and points
	
	CCharString listenerTitleString;

	int margin;

	
  public:

	CSnakeExtractor(const SnakeExtractionParameters &I_parameters);
	~CSnakeExtractor();

	void setRoi(CImageBuffer *troi);
	void resetRoi();

	bool computeSnake(CXYPointArray *snakeStartingPoints);

	float &gaussianImage(const unsigned long &index) const { return GaussianImage.pixFlt(index); };
		

private:
	bool SmoothImages();
	bool CalculateDerivations();

	bool calculateConfiguratedDesignMatrix(Matrix *DesignMatrix);
	bool calculateAdditionalExternalEnergyVec(vector<CXYPoint> *AdditionalExternalEnergy);

	void writeMatrix (Matrix *DesignMatrix, CFileName fn);
	void writeReport(int counter, double d_XY, vector<CXYPoint> *SnakeVec);
	void writeGrayValues(int counter, double d_XY, vector<CXYPoint> *SnakeVec);
	void writeCalculationValues(int counter, vector<CXYPoint> *SnakeVec, vector<CXYPoint> *AdditionalExternalEnergy);

	C2DPoint getGradValues(long x, long y);


};