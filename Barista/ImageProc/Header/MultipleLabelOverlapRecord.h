//============================================================================
//
// File        : MultipleLabelOverlapRecord.h
//
// Author      : FR
//
// Description : Implementation of class CMultipleLabelOverlapRecord,
//               a class showing multiple overlapping region for one region
//               of a label image
//
//============================================================================

#ifndef ____CMultipleLabelOverlapRecord____
#define ____CMultipleLabelOverlapRecord____

#include <vector>
#include "LabelOverlapIndicator.h"

class CMultipleLabelOverlapRecord 
{ 
  protected:

//============================================================================
//================================ Data ======================================

  
	  unsigned long index;


	  vector<CLabelOverlapIndicator> overlapRegions; 

	  unsigned long nPixels;

	  CLabelOverlapIndicator overlapBackground;

	  float area;

	  float totalCoverage;
   
	  eOVERLAP eClassification; 

	  int rmin, rmax, cmin, cmax;

  public:

//============================================================================

  
	  CMultipleLabelOverlapRecord();

	  CMultipleLabelOverlapRecord(unsigned long Index);

	  CMultipleLabelOverlapRecord(const CMultipleLabelOverlapRecord &record);
  
	  ~CMultipleLabelOverlapRecord () { };


//============================================================================
//================================ Data access ===============================

	  unsigned long getIndex() const { return index; };

	  unsigned long nOverlaps() const { return overlapRegions.size(); };

	  CLabelOverlapIndicator getRegion(unsigned long region) const 
	  { return overlapRegions[region]; };
   
	  unsigned long getRegionIndex(unsigned long region) const 
	  { return overlapRegions[region].getIndex(); };

	  unsigned long getNPixels(unsigned long region) const 
	  { return overlapRegions[region].getNPixels(); };

	  float getPercentage(unsigned long region) const 
	  { return overlapRegions[region].getPercentage(); };
   
	  eOVERLAP getClassification(unsigned long region) const 
	  { return overlapRegions[region].getClassification(); };

	  unsigned long getNPixelsBackground() const 
	  { return overlapBackground.getNPixels(); };

	  float getPercentageBackground() const 
	  { return overlapBackground.getPercentage(); };

	  eOVERLAP getClassificationBackground() const
	  { return overlapBackground.getClassification(); };

	  unsigned long getNPixels() const { return nPixels; };

	  int getOverlapIndicator(unsigned long region, 
                              CLabelOverlapIndicator &Indicator) const;


	  CCharString getString(bool showNone) const;

	  float getTotalCoverage() const { return totalCoverage; };

	  float getArea() const { return area; };

	  eOVERLAP getClassification() const { return eClassification; };

	  int getRmin() const { return rmin; }
	  int getRmax() const { return rmax; }
	  int getCmin() const { return cmin; }
	  int getCmax() const { return cmax; }

//============================================================================
//============================== Manipulate data =============================
 
	  void reset(unsigned long Index, float Area);

	  void clear();

	  void add(unsigned long Index, unsigned long NPixels);

	  void checkMinMax(int r, int c);

	  void finish();

	  void computeTotalOverlap();

	  void classify(float noneVsWeak, float weakVsPartial, 
                    float partialVsStrong);

	  void classifyTotal(float noneVsWeak, float weakVsPartial, 
                         float partialVsStrong);

	  void sort();

	  void reverseOrder();

	  void removeElement(unsigned long Ii_index);
    

//============================================================================
//=========================== Comparision operators ==========================

	  int operator <  (const CMultipleLabelOverlapRecord &record) const;

	  int operator >  (const CMultipleLabelOverlapRecord &record) const;

//============================================================================

  protected:
   
//============================================================================
  
	  void sortPart(int low, int high);

	  void swap(int i, int j);

   
//============================================================================

};

#endif
