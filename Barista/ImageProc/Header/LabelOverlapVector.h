//============================================================================
//
// File        : LabelOverlapVector.h
//
// Author      : FR
//
// Description : Interface of class CLabelOverlapVector, a vector of multiple 
//               overlap records
//
//============================================================================

#ifndef ____CLabelOverlapVector____
#define ____CLabelOverlapVector____


#include <vector>

#include "LabelOverlapIndicator.h"
#include "MultipleLabelOverlapRecord.h"

class CLabelOverlapVector 
{  
  protected:

	  vector<CMultipleLabelOverlapRecord> OverlapVector;

	  vector<unsigned long> SortedIndices;

  public:

//============================================================================


	  CLabelOverlapVector () { };

	  CLabelOverlapVector (unsigned long NElements);

	  CLabelOverlapVector (const CLabelOverlapVector &vec);

	  ~CLabelOverlapVector ();

//============================================================================
//================================ Data access ===============================

	  unsigned long getIndex( const unsigned long &region ) const
	  {
		  return SortedIndices[region];
	  };

	  CMultipleLabelOverlapRecord &getRecordByRegion ( const unsigned long &region ) 
	  {
		  return OverlapVector[getIndex(region)];
	  };

	  CMultipleLabelOverlapRecord &getRecordByIndex ( const unsigned long &index ) 
	  {
		  return OverlapVector[index];
	  };

	  unsigned long getLength() const { return OverlapVector.size(); };

	  unsigned long setLength(unsigned long NElements);


//============================================================================

	  void classifyPart(float noneVsWeak, float weakVsPartial, 
						float partialVsStrong);

	  void classifyTotal(float noneVsWeak, float weakVsPartial, 
                         float partialVsStrong);

	  void printHistogram(const CCharString &heading, float cellSize, float maxSize,
		                  bool printOverlapHisto) const;

	  void printSizeHistogram(const CCharString &heading, float cellSize, 
							  float maxSize) const;

	  void getSizeHistogram(vector <unsigned long> &histogram, float cellSize, 
                            float maxSize) const;

	  void printCoverageHistogram(const CCharString &heading) const;

//============================================================================

  
	  void writeData(CCharString heading = CCharString(""),
                     CCharString filename = CCharString("")) const;


	  void writeData(ostream &ost, CCharString heading = CCharString("")) const;


//============================================================================

	  void sortComponents();

	  void sort();

	  void sort(int low, int high);

//============================================================================

  protected:

//============================================================================

	  void reverseOrder();

	  void sortPart(int low, int high);
     
	  void swap(int i, int j);


//============================================================================

};

#endif
