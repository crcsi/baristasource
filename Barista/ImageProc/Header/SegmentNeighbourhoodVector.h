#ifndef __CSegmentNeighbourhoodVector__
#define __CSegmentNeighbourhoodVector__

#pragma once
#include "sortVector.h"
#include "SegmentNeighbourhood.h"
#include "SegmentAMatrix.h"

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

//****************************************************************************

class CSegmentNeighbourhoodVector : protected CSortVector<CSegmentNeighbourhood>
{
  protected:

	
  
//============================================================================

  public:
   
	  CSegmentNeighbourhoodVector();
	  CSegmentNeighbourhoodVector(const CSegmentNeighbourhoodVector &segmentVector);
	
	  virtual ~CSegmentNeighbourhoodVector();

	  int getSize() const { return this->size(); }

	  virtual void sortAscending() { CSortVector<CSegmentNeighbourhood>::sortAscending(); };

	  virtual void sortDescending() { CSortVector<CSegmentNeighbourhood>::sortDescending(); };
	  
	  CSegmentNeighbourhood &operator [] (unsigned int i) { return (*(CSortVector<CSegmentNeighbourhood> *) this)[i]; }

	  const CSegmentNeighbourhood &operator [] (unsigned int i) const { return (*(const CSortVector<CSegmentNeighbourhood> *) this)[i]; }

	  void addSegment(const CSegmentNeighbourhood &segment);

	  void addSegment(unsigned int Index, CImageRegion *pR1, CImageRegion *pR2);

	  void removeSegmentAt(unsigned int i);

	  void mergeSegments(unsigned int i, CLabelImage &labels, 
		                 int *boundary, CSegmentAMatrix &matrix, 
						 const CImageBuffer &img, vector<CImageRegion *> &regionVec, bool purge);

	  void purgeSegments(unsigned int i, CSegmentAMatrix &matrix);
	  
	  void pickSegmentRegion1(unsigned int i, CSegmentAMatrix &matrix);

	  void pickSegmentRegion2(unsigned int i, CSegmentAMatrix &matrix);

	  void update(CSegmentNeighbourhood &neigh, CSegmentAMatrix &matrix, int *boundary, const CImageBuffer &texture);

//	  void updateNewLabels(CLabelImage &newLabel);

	  void calculate(vector<CImageRegion *> &regionVec, CSegmentAMatrix &matrix, unsigned int dimension);

	  void dump(const CFileName &filename) const;

	  void purgeIndex();

	  void CalculateNNeigh(vector<CImageRegion *> &regionVec);

	  //void CSegmentNeighbourhoodVector::UpdateNNeigh(unsigned short k, vector<CImageRegion *> &regionVec);

	  CSortVector<unsigned short> FindNeighbours(int region);

	  CSortVector<unsigned short> MutalNeighbours(CSortVector<unsigned short> vec1, CSortVector<unsigned short> vec2);						 

  protected: 

};

#endif
