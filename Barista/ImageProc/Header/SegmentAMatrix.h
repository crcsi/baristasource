#ifndef __CSegmentAMatrix__
#define __CSegmentAMatrix__

#include <vector>
#include "LabelImage.h"

using namespace std;

class CFileName;
class CCharString;

class CSegmentAMatrix : protected vector<unsigned int>
{
  protected:

	  unsigned int nLabels;
	
  
//============================================================================

  public:
   
	  CSegmentAMatrix(int nlabels);
	  CSegmentAMatrix(const CSegmentAMatrix &mat);

	  ~CSegmentAMatrix();

	  unsigned int getNLabels() const { return this->nLabels; };

	  unsigned int getLength() const { return this->size(); };

	  //unsigned int getIndex() const { return this->index; };

	  const unsigned int &operator [] (unsigned int index) const { return this->vector<unsigned int>::operator[](index); }

	  unsigned int &operator [] (unsigned int index) { return this->vector<unsigned int>::operator[](index); }

	  const unsigned int &operator () (unsigned int label1, unsigned int label2)  const
	  { return (*this)[LabelsToIndex(label1, label2)]; }

	  unsigned int &operator () (unsigned int label1, unsigned int label2) 
	  { return (*this)[LabelsToIndex(label1, label2)]; }

	  void incrementAdjacency(unsigned int label1, unsigned int label2)
	  { (*this)[LabelsToIndex(label1, label2)] += 1; };
 
	  void decrementAdjacency(unsigned int label1, unsigned int label2)
	  { (*this)[LabelsToIndex(label1, label2)] -= 1; };
 
	 //calculation of the Index by given regions			
	  unsigned int LabelsToIndex(unsigned int label1, unsigned int label2) const
	  {
		  	if (label2 < label1) return LabelsToIndex(label2, label1);

			return ((label1 - 1) * this->nLabels - (label1 - 1) * label1 / 2 + label2 - 1 - label1);
	  };

	  void IndexToLabels(unsigned int index, unsigned int &label1, unsigned int &label2) const;
	
	  void dump(ofstream &oFile) const; 
	  void dump(const CCharString &FileName) const; 

	  void calculateFromEdgePixels(CLabelImage &labels,int *boundary); 

	
  protected: 

};

#endif

