#ifndef ___CLSM_H___
#define ___CLSM_H___

#include <vector>
#include "newmat.h"
#include "XYPoint.h"
#include "XYZPoint.h"
#include "MatchingParameters.h"

class CTrans2D;
class CImageBuffer;

class CLSM
{
  protected:

	  vector<CTrans2D *> templateToSearchTrafos;

	  vector<CImageBuffer *> searchBuffers;
	  vector<int> searchBufferFactors;

	  vector<CImageBuffer *> transformedSearchBuffers;
	  
	  CImageBuffer *templateBuf;

	  CImageBuffer *templateBufWeight;

	  CXYPoint refPoint;

	  vector<CXYPoint> searchPoints;

	  vector<float> rhoVec;

	  double sigma0;

	  SymmetricMatrix Qxx;

	  double templateVariance;

	  double templateMean;

	  CCharString projectDir;

	  CXYZPoint ObjPoint;

	  CMatchingParameters matchingPars;

	  double sigmaGreyValUsed;

  public:

	  CLSM(const CMatchingParameters &pars);

	  CLSM(const CMatchingParameters &pars, CImageBuffer *templatebuf, const CXYPoint &ref);

	  virtual ~CLSM();

	  bool init(const CMatchingParameters &pars, CImageBuffer *templatebuf, const CXYPoint &ref);

	  void addSearchImage(CImageBuffer *search, CTrans2D *trans, int searchFactor);

	  int nSearchImgs() const { return this->searchBuffers.size(); }

	  CXYPoint getSearchPoint(int i) const { return this->searchPoints[i]; };;

	  virtual bool LSM();

	  virtual bool hasPointObs() const { return false; };

	  bool isIntitalised() const { return this->templateBuf != 0; }

	  double getSigma0() const { return this->sigma0; };

	  void dump(const char *fn, bool temp, bool origSearch, bool transformedSearch, bool weights, float coordFactor) const;

	  void setProjectDir(const CCharString &path);

  protected:

	  void initLSMWeights();

	  int initLSMEquations(SymmetricMatrix &N, Matrix &t, double &pll);

	  int accumulateLSMEquations(SymmetricMatrix &N, Matrix &t, double &pll, int iSearch, int IndexN, 
		                         int r, int c, const C2DPoint &derivatives, double dg,
								 unsigned long uWeight);

	  virtual int addPointObservations(SymmetricMatrix &N, Matrix &t, double &pll, int IndexN) { return 0; };

	  bool iterateLSMOnce(double &pvv);

	  virtual void clear();

	  void changeSearchMeanVariance(CImageBuffer *searchBuf);

	  void initTemplateMeanVariance();

	  static void initMeanVariance(CImageBuffer *buf, double &mean, double &variance);

	  float crossCorr(int x, int y, int SearchIndex);

	  static void copyToColour(CImageBuffer &in, CImageBuffer &out, int r, int c);

};

#endif