/**
 * Copyright:    Copyright (c) Petra Helmholz<p>
 * Company:      University of Melbourne<p>
 * @author Petra Helmholz
 * @version 1.0
 */

#pragma once
#include "filter.h"

class CFileName;

enum eSnakeMode {eTraditional, eGVF, eBalloon};

/*!
 * \brief
 * Defines the set of parameters and their default values used for the snakes extraction.
 * 
 * Parameters for the General Settings are:\n
 * - buffer \n
 * - stopCriteria \n
 * - sigma \n
 * - gamma \n
 * - snakeSpace \n
 * - mo\n
 * 
 * Parameters for the Internal Energy are:\n
 * - alpha\n
 * - beta \n
 * 
 * Parameters for the External Energy are:\n
 * - kappa\n
 * - ka (Balloon Force).
 */


class SnakeExtractionParameters
{
protected:

   public:

	 SnakeExtractionParameters();
	 SnakeExtractionParameters(const SnakeExtractionParameters &pars);
	 ~SnakeExtractionParameters() {};

	 eSnakeMode Mode;

	 //Snake Parameters//////////////////////////////////////////////
	 bool openSnake;														///< Type of curve (open = 1, close = 0)

	 //Parameter for General Settings
	 unsigned int buffer;													///< Buffer around the object in pixels
	 float stopCriteria;													///< Stop criteria for snake calculation in pixel
	 float sigma;															///< Sigma for gaussian smoothnes of the image
	 float gamma;															///< Viscosity factor
	 unsigned int snakeSpace;												///< Approximated distance between two snake points
	 float mo;																///< Additional parameter for GVF snakes


	 eSnakeMode getMode() const {return this->Mode;};
	 bool getOpenSnakeMode () const {return this->openSnake;};
	 unsigned int getBuffer () const {return this->buffer;};
	 float getStopCriteria () const {return this->stopCriteria;};
	 float getSigma () const {return this->sigma;};
	 float getGamma () const {return this->gamma;};
	 unsigned int getSnakeSpace () const {return this->snakeSpace;};
	 float getMo () const {return this->mo;};

	//Parameters for Internal Energy
	 float alpha;															///< Elasticity Parameter
	 float beta;															///< Rigidity Parameter

	 float getAlpha () const {return this->alpha;};
	 float getBeta () const {return this->beta;};

	 //Parameters for External Energy
	 float kappa;															///< Strength of external energy 
	 float ka;																///< Balloon Force
	 
	 float getKappa () const {return this->kappa;};
	 float getKa () const {return this->ka;};


	 //functions//////////////////////////////////////////////
	 void writeParameters(const CFileName &fileName) const;
	 bool initParameters(const CFileName &fileName);
};

