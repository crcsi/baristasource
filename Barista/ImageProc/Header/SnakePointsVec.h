/**
 * Copyright:    Copyright (c) Petra Helmholz<p>
 * Company:      University of Melbourne<p>
 * @author PetraH
 * @version 1.0
 */

#ifndef __CSnakePointsVec__
#define __CSnakePointsVec__

#pragma once
#include <vector>
#include "SnakePoint.h"
#include "XYPointArray.h"
#include "SnakePars.h"
#include "ImgBuf.h"

using namespace std;

class CFileName;
class CCharString;


/*!
 * \brief
 * Defines a vector of CSnakePoint.
 */


//****************************************************************************

class CSnakePointsVec : protected vector<CSnakePoint>
{
  protected:

	
  
//============================================================================

  public:
   
	  CSnakePointsVec();
      virtual ~CSnakePointsVec();

	  int getSize() const { return  this->size(); }

	  CSnakePoint &operator [] (unsigned int i) { return (*(vector<CSnakePoint> *) this)[i]; }

	  const CSnakePoint &operator [] (unsigned int i) const { return (*(const vector<CSnakePoint> *) this)[i]; }

	  void initializeSnakeVector(CXYPointArray snakeStartingPoints, C2DPoint Offset, SnakeExtractionParameters extractionParameters, const CImageBuffer *gradX, const CImageBuffer *gradY);

	  bool updateSnakeVector(vector<CXYPoint> *NewSnakeVec, C2DPoint Offset, SnakeExtractionParameters extractionParameters, const CImageBuffer *gradX, const CImageBuffer *gradY);

	  bool addSegment(const CXYPoint point,const unsigned int index, const double phi, const unsigned int d, const CImageBuffer *gradX, const CImageBuffer *gradY, C2DPoint Offset, const SnakeExtractionParameters  *Parameters);	 
	  bool addSegment(const CXYPoint point,const unsigned int index, const CImageBuffer *gradX, const CImageBuffer *gradY, C2DPoint Offset, const SnakeExtractionParameters  *Parameters);
	  void addSegment(const CSnakePoint &segment);

	  //CSnakePoint getSegment(unsigned int i) {return (this->at(i)); };

	  void removeSegmentAt(unsigned int i);

	  void writeVector(int index) const;
	  void dump(const CFileName &filename) const;

  protected: 



};

#endif
