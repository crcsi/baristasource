#include <climits>
#include <float.h>
#include <math.h>
#include <strstream>
using namespace std;

#include "filter.h"
#include "ITUTM.h"
#include "ImgBuf.h"
#include "GenericStack.h"

CImageFilter::CImageFilter(eFilterType I_type, int I_sizeX, int I_sizeY, float I_parameter) : 
				sizeX (I_sizeX), sizeY(I_sizeY), type(I_type), filterValues(NULL),
				parameter(I_parameter)
{
	set(I_type, I_sizeX, I_sizeY, I_parameter);  
};
CImageFilter::CImageFilter(const CImageFilter &filter) : 
				sizeX (filter.sizeX), sizeY(filter.sizeY), type(filter.type), filterValues(NULL),
				parameter(filter.parameter)
{
	set(type, sizeX, sizeY, parameter);  
}

CImageFilter::~CImageFilter()
{
	if (filterValues) delete [] filterValues;
	filterValues = NULL;
};
	   
CImageFilter &CImageFilter::operator= ( const CImageFilter& filter )
{
	if (this != &filter) 
	{
		this->set(filter.type, filter.sizeX, filter.sizeY, filter.parameter);
	};
	return *this;
}

CCharString CImageFilter::getDescriptor() const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);

	switch(this->type)
	{
		case eAverage: 
			oss << "moving average (" << this->sizeX << " x " << this->sizeY << ")"; 
			break;
		case eSobelX:
		case eSobelY: 
			oss << "Sobel operator (3 x 3)"; 
			break; 
		case eLaplaceA:
		case eLaplaceB:
		case eLaplaceC: 
			oss << "Laplace filter (3 x 3)"; 
			break; 
		case eLoG: 
			oss << "Laplacian of Gaussian (" << this->sizeX <<" x " << this->sizeY << "); sigma: " << this->parameter; 
			break;
		case eGauss:
			oss << "Gaussian (" << this->sizeX <<" x " << this->sizeY << "); sigma: " << this->parameter; 
			break;
		case eDiffX:
		case eDiffY:
			oss << "simple first derivative"; 
			break;
		case eDiff45:
		case eDiff135:
			oss << "first derivative diagonal(3 x 3)";
			break;
		case eDiffXGauss:
		case eDiffYGauss:
			oss << "first derivative of a Gaussian (" << this->sizeX <<" x " << this->sizeY << "); sigma: " << this->parameter; 
			break;
		case eDiffXDeriche:
		case eDiffYDeriche:
			oss << "first derivative by the Deriche operator (" << this->sizeX <<" x " << this->sizeY << "); sigma: " << this->parameter; 
			break;
		case eBinom:
			oss << "Binomial filter (" << this->sizeX <<" x " << this->sizeY << ")"; 
			break;
		case eChamfer3:
			oss << "Chamfering (3 x 3)"; 
			break;
		case eChamfer5:
			oss << "Chamfering (5 x 5)"; 
			break;
		case eMorphological:
			oss << "morphological filter (" << this->sizeX <<" x " << this->sizeY << ")"; 
			break;
		case eMinimumFilter:
			oss << "minimum filter (" << this->sizeX <<" x " << this->sizeY << ")"; 
			break;
		case eMaximumFilter:
			oss << "maximum filter (" << this->sizeX <<" x " << this->sizeY << ")"; 
			break;
		default: 
			oss << "Unknown filter (" << this->sizeX <<" x " << this->sizeY << ")"; 
			break;
	}

	oss << ends;
	char *z = oss.str();
	CCharString st(z);
	delete [] z;

	return st;
};

bool CImageFilter::set(eFilterType I_type, int I_sizeX, int I_sizeY, float I_parameter)
{ 
	parameter = I_parameter;
	bool ret = true;	
	this->type = I_type;

	sizeX = I_sizeX; 
	sizeY = I_sizeY;
	long nPix = sizeX * sizeY;
	
	switch(type)
    {
		case  eAverage: 
			{ 
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float val = 1.0f / (float) nPix;
					for (int i = 0; i < nPix; ++i)               
					{
						filterValues[i] = val;        
					};
				};
			};                 
			break;
		
		case eSobelX:
			{
				sizeX =  sizeY = 3;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					filterValues[0] = -0.125f; filterValues[1] = 0.0f; filterValues[2] = 0.125f;
					filterValues[3] = -0.250f; filterValues[4] = 0.0f; filterValues[5] = 0.250f;
					filterValues[6] = -0.125f; filterValues[7] = 0.0f; filterValues[8] = 0.125f;
				}
			}
			break;

		case eSobelY:
			{
				sizeX =  sizeY = 3;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					filterValues[0] = -0.125f; filterValues[1] = -0.25f; filterValues[2] = -0.125f;
					filterValues[3] =  0.000f; filterValues[4] =  0.00f; filterValues[5] =  0.000f;
					filterValues[6] =  0.125f; filterValues[7] =  0.25f; filterValues[8] =  0.125f;
				}
			}
			break;

		case eLaplaceA:
			{
				sizeX =  sizeY = 3;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					filterValues[0] = 0.000f;   filterValues[1] =  0.125f;  filterValues[2] = 0.000f;
					filterValues[3] = 0.125f;   filterValues[4] = -0.500f;  filterValues[5] = 0.125f;
					filterValues[6] = 0.000f;   filterValues[7] =  0.125f;  filterValues[8] = 0.000f;
				};           
			};            
			break;

		case eLaplaceB:
			{
				sizeX =  sizeY = 3;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float val = 1.0f / 9.0f;
					filterValues[0] =  0.0f;   filterValues[1] =  -val;        filterValues[2] =  0.0f;
					filterValues[3] = -val;    filterValues[4] =  5.0f * val;  filterValues[5] = -val;
					filterValues[6] =  0.0f;   filterValues[7] =  -val;        filterValues[8] =  0.0f;
				};           
			};            
			break;

		case eLaplaceC:
			{
				sizeX =  sizeY = 3;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float val = 1.0f / 16.0f;
					filterValues[0] = val;   filterValues[1] =  val;   filterValues[2] = val;
					filterValues[3] = val;   filterValues[4] = -0.5f;  filterValues[5] = val;
					filterValues[6] = val;   filterValues[7] =  val;   filterValues[8] = val;
				};           
			};            
			break;

		case eDiffX:
			{
				sizeX = 3;
				sizeY = 1;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float val = 0.5f;
					filterValues[0] = -val;   filterValues[1] =  0.0f;   filterValues[2] = val;
				};           
			};            
			break;

		case eDiffY:
			{
				sizeX = 1;
				sizeY = 3;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float val = 0.5f;
					filterValues[0] = -val;   filterValues[1] =  0.0f;   filterValues[2] = val;
				};           
			};            
			break;

		case eDiff45:
			{
				sizeX = 3;
				sizeY = 3;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float val = 1.0f / sqrt(2.0f);
					filterValues[0] = -val;   filterValues[1] =  0.0f;   filterValues[2] = 0.0f;
					filterValues[3] = 0.0f;   filterValues[4] =  0.0f;   filterValues[5] = 0.0f;
					filterValues[6] = 0.0f;   filterValues[7] =  0.0f;   filterValues[8] =  val;
				};           
			};            
			break;

		case eDiff135:
			{
				sizeX = 3;
				sizeY = 3;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float val = 1.0f / sqrt(2.0f);
					filterValues[0] = 0.0f;   filterValues[1] =  0.0f;   filterValues[2] = -val;
					filterValues[3] = 0.0f;   filterValues[4] =  0.0f;   filterValues[5] = 0.0f;
					filterValues[6] =  val;   filterValues[7] =  0.0f;   filterValues[8] = 0.0f;
				};           
			};            
			break;

		case eBinom:
			{
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					long i;
					for (i = 0; i < sizeY; ++i)
						filterValues[i] = (float) Binom(sizeY - 1, i);

					long j = sizeY;
					for (i = 1; i  < sizeX; ++i, j += sizeY)
						filterValues[j] = (float) Binom(sizeX - 1, i);

					for(i = sizeY; i  < nPix; i += sizeY)   
						for (j = 1; j  < sizeY; ++j)
                            filterValues[i + j] = filterValues[i] * filterValues[j];

					float sum = 0.0f;
					for (i = 0; i < nPix; ++i)
						sum += filterValues[i];
					for (i = 0; i < nPix; ++i)
						filterValues[i] /= sum;
				};
			};
			break;

		case eGauss:
			{
				float sigma = I_parameter;
				if (sigma < 0.1f)
				{
					if (sizeX > sizeY) sigma = float(sizeX) / 5.0f;
					else sigma = float(sizeY) / 5.0f;
					parameter = sigma;
				}
				else
				{
					int width = 2 * (int) floor(3.0f * sigma + 0.5f) + 1;
					if (sizeX != 1) sizeX = width;
					if (sizeY != 1) sizeY = width;
					if ((sizeX == 1) && (sizeY == 1)) sizeX = sizeY = width;
				}
				nPix = sizeX * sizeY;	
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float x0 = float(sizeX - 1) * 0.5f;
					float y0 = float(sizeY - 1) * 0.5f;

					if (sizeX == 1)
					{
						for (int i = 0; i < sizeY; ++i)
						{
							float y = float(i) - y0;
							filterValues[i] = this->Gauss1D(y, sigma);
						}

					}
					else if (sizeY == 1)
					{
						for (int i = 0; i < sizeX; ++i)
						{
							float x = float(i) - x0;
							filterValues[i] = this->Gauss1D(x, sigma);
						}
					}
					else
					{
						for (int i = 0; i < sizeX; ++i)
						{
							float x = float(i) - x0;
							for (int j = 0; j < sizeY; ++j)
							{
								float y = float(j) - y0;
								filterValues[j * sizeX + i] = this->Gauss2D(x, y, sigma);
							}
						}
					}
				
					float sum = 0.0f;
					for (int i = 0; i < nPix; ++i)
						sum += filterValues[i];
					for (int i = 0; i < nPix; ++i)
						filterValues[i] /= sum;
				};
			};
			break;
      
		case eDiffXGauss:
			{
				float sigma = I_parameter;
				if (sigma < 0.1f)
				{
					if (sizeX > sizeY) sigma = float(sizeX) / 5.0f;
					else sigma = float(sizeY) / 5.0f;
					parameter = sigma;
				}
				else
				{
					int width = 2 * (int) floor(3.0f * sigma + 0.5f) + 1;
					if (width < 3) width = 3;
					sizeX = sizeY = width;
				}
				nPix = sizeX * sizeY;	
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float x0 = float(sizeX - 1) * 0.5f;
					float y0 = float(sizeY - 1) * 0.5f;

					float diffFact = 1.0f / (sigma * sigma);

					for (int i = 0; i < sizeX; ++i)
					{
						float x = float(i) - x0;
						for (int j = 0; j < sizeY; ++j)
						{
							float y = float(j) - y0;
							filterValues[j * sizeX + i] = x * diffFact * this->Gauss2D(x, y, sigma);
						}
					};
				
					float sum = 0.0f;
					for (int i = 0; i < nPix; ++i)
						sum += fabs(filterValues[i]);
					for (int i = 0; i < nPix; ++i)
						filterValues[i] /= sum;
				};
			};
			break;

		case eDiffYGauss:
			{
				float sigma = I_parameter;
				if (sigma < 0.1f)
				{
					if (sizeX > sizeY) sigma = float(sizeX) / 5.0f;
					else sigma = float(sizeY) / 5.0f;
					parameter = sigma;
				}
				else
				{
					int width = 2 * (int) floor(3.0f * sigma + 0.5f) + 1;
					if (width < 3) width = 3;
					sizeX = sizeY = width;
				}
				nPix = sizeX * sizeY;	
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float x0 = float(sizeX - 1) * 0.5f;
					float y0 = float(sizeY - 1) * 0.5f;

					float diffFact = 1.0f / (sigma * sigma);

					for (int i = 0; i < sizeX; ++i)
					{
						float x = float(i) - x0;
						for (int j = 0; j < sizeY; ++j)
						{
							float y = float(j) - y0;
							filterValues[j * sizeX + i] = y * diffFact * this->Gauss2D(x, y, sigma);
						}
					};
				
					float sum = 0.0f;
					for (int i = 0; i < nPix; ++i)
						sum += fabs(filterValues[i]);
					for (int i = 0; i < nPix; ++i)
						filterValues[i] /= sum;
				};
			};
			break;

		case eDiffXDeriche:
			{
				float sigma = I_parameter;
				if (sigma < 0.1f)
				{
					if (sizeX > sizeY) sigma = float(sizeX) / 7.0f;
					else sigma = float(sizeY) / 7.0f;
					parameter = sigma;
				}
				else
				{
					int width = 2 * (int) floor(5.0f * sigma + 0.5f) + 1;
					if (width < 3) width = 3;
					sizeX = sizeY = width;
				}

				nPix = sizeX * sizeY;	
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float x0 = float(sizeX - 1) * 0.5f;
					float y0 = float(sizeY - 1) * 0.5f;

					float diffFact = 1.0f / (sigma);

					for (int i = 0; i < sizeX; ++i)
					{
						float x = float(i) - x0;
						for (int j = 0; j < sizeY; ++j)
						{
							float y = float(j) - y0;
							filterValues[j * sizeX + i] = x * exp(-(fabs(x)+fabs(y)) * diffFact);
						}
					};
				
					float sum = 0.0f;
					for (int i = 0; i < nPix; ++i)
						sum += fabs(filterValues[i]);
					for (int i = 0; i < nPix; ++i)
						filterValues[i] /= sum;
				};
			}
			break;

		case eDiffYDeriche:
			{
				float sigma = I_parameter;
				if (sigma < 0.1f)
				{
					if (sizeX > sizeY) sigma = float(sizeX) / 5.0f;
					else sigma = float(sizeY) / 5.0f;
					parameter = sigma;
				}
				else
				{
					int width = 2 * (int) floor(5.0f * sigma + 0.5f) + 1;
					if (width < 3) width = 3;
					sizeX = sizeY = width;
				}

				nPix = sizeX * sizeY;	
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float y0 = float(sizeY - 1) * 0.5f;
					float x0 = float(sizeX - 1) * 0.5f;

					float diffFact = 1.0f / (sigma);

					for (int i = 0; i < sizeX; ++i)
					{
						float x = float(i) - x0;
						for (int j = 0; j < sizeY; ++j)
						{
							float y = float(j) - y0;
							filterValues[j * sizeX + i] = y * exp(-(fabs(x)+fabs(y)) * diffFact);
						}
					};
				
					float sum = 0.0f;
					for (int i = 0; i < nPix; ++i)
						sum += fabs(filterValues[i]);
					for (int i = 0; i < nPix; ++i)
						filterValues[i] /= sum;
				};
			}
			break;

		case eLoG:
			{
				float sigma = I_parameter;
				if (sigma <= 0.1f)
				{
					if (sizeX > sizeY) sigma = float(sizeX) / 7.0f;
					else sigma = float(sizeY) / 7.0f;					
					parameter = sigma;
				}
				else
				{
					sizeX = 2 * (int) floor(3.0f * sigma + 0.5f) + 1;
					sizeY = sizeX;
				}
				nPix = sizeX * sizeY;	
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float x0 = float(sizeX - 1) * 0.5f;
					float y0 = float(sizeY - 1) * 0.5f;

					for (int i = 0; i < sizeX; ++i)
					{
						float x = float(i) - x0;
						for (int j = 0; j < sizeY; ++j)
						{
							float y = float(j) - y0;
							filterValues[j * sizeX + i] = this->LoG(x, y, sigma);
						}
					};
				
					float sum = 0.0f;
					for (int i = 0; i < nPix; ++i)
						sum += fabs(filterValues[i]);
					for (int i = 0; i < nPix; ++i)
						filterValues[i] /= sum;
				};
			};
			break;
     		
		case eChamfer3:		
			{
				sizeX = 3;
				sizeY = 3;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					filterValues[0] = 7.0f;   filterValues[1] =  5.0f;   filterValues[2] = 7.0f;
					filterValues[3] = 5.0f;   filterValues[4] =  0.0f;   filterValues[5] = 5.0f;
					filterValues[6] = 7.0f;   filterValues[7] =  5.0f;   filterValues[8] = 7.0f;
				};           
			};            
			break;

		case eChamfer5:		
			{
				sizeX = 5;
				sizeY = 5;
				nPix = sizeX * sizeY;
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float vmax = float(USHRT_MAX);
					filterValues[ 0] =  vmax;  filterValues[ 1] = 11.0f;  filterValues[ 2] = vmax;  filterValues[ 3] = 11.0f;  filterValues[ 4] =  vmax; 
					filterValues[ 5] = 11.0f;  filterValues[ 6] =  7.0f;  filterValues[ 7] = 5.0f;  filterValues[ 8] =  7.0f;  filterValues[ 9] = 11.0f; 
					filterValues[10] =  vmax;  filterValues[11] =  5.0f;  filterValues[12] = 0.0f;  filterValues[13] =  5.0f;  filterValues[14] =  vmax; 
					filterValues[15] = 11.0f;  filterValues[16] =  7.0f;  filterValues[17] = 5.0f;  filterValues[18] =  7.0f;  filterValues[19] = 11.0f; 
					filterValues[20] =  vmax;  filterValues[21] = 11.0f;  filterValues[22] = vmax;  filterValues[23] = 11.0f;  filterValues[24] =  vmax; 
				};           
			};            
			break;
     
		case eMorphological: 
		case eMinimumFilter:
		case eMaximumFilter:
			{
				if ((type == eMinimumFilter) || (type == eMaximumFilter))
				{
					this->sizeX = 2 * this->sizeX + 1;
					this->sizeY = 2 * this->sizeY + 1;
					nPix = this->sizeX * this->sizeY;
				}
				filterValues = new float [nPix];
				if (!filterValues) ret = false;
				else
				{
					float val = 1.0f;
					for (int i = 0; i < nPix; ++i)               
					{
						filterValues[i] = val;        
					};
				};
			};

		default: 		
			{
				ret = false;
			};            
			break;
    };

	return ret;
};

float CImageFilter::LoG(float x, float y, float sigma) const
{
	x /= sigma;
	y /= sigma;

	float logOp = 0.5f * (x * x + y * y);
	logOp = (logOp - 1) * exp(-logOp);
   
	return logOp;
};

//============================================================================

float CImageFilter::Gauss2D(float x, float y, float sigma) const
{
	x /= sigma;
	y /= sigma;
	float gauss = float(1.0f / (2.0f * M_PI * sigma) * exp (-0.5f * (x * x + y * y)));
	return gauss;
};
//============================================================================

float CImageFilter::Gauss1D(float x, float sigma) const
{
	x /= sigma;
	float gauss = float(1.0f / (sqrt(2.0f * M_PI * sigma)) * exp (-0.5f * (x * x)));
	return gauss;
};

//============================================================================

long CImageFilter::Binom(int n, int k) const 
{
	double nOverK = 1;   
	int help = n - k;

	// calculate n! / (n-k)! 
	while (n > help) 
	{
		nOverK *= n ;
		--n; 
	};

	// calculate n over k
	while (k > 1) 
	{ 
		nOverK /= k; 
		--k; 
	};
   
	return (long) floor(nOverK + 0.5f);
};
	   
bool CImageFilter::convolve(const CImageBuffer &source, CImageBuffer& destination, bool resetMargin) const
{
	bool ret = true;
	if (isSeparable() && ((sizeX != 1) && (sizeY != 1)))
	{
		CImageFilter CseparateFilter(this->type,this->sizeX, 1, this->parameter);
		CImageBuffer intermediate(source);
		ret = CseparateFilter.convolve(source, intermediate, false);
		if (ret)
		{
			CseparateFilter.set(this->type, 1, this->sizeY, this->parameter);
			ret = CseparateFilter.convolve(intermediate, destination, resetMargin);
		};
	}
	else
	{
		int destBpp = destination.getBpp();
		int srcBpp  = source.getBpp();
		if (srcBpp > destBpp) destBpp = srcBpp;
		bool unsignedFlag = !isDerivative();
	
		if ((!unsignedFlag) && (destBpp < 16)) destBpp = 16;

		if ((destination.getBands()  != source.getBands())  || (destination.getWidth() != source.getWidth()) ||
			(destination.getHeight() != source.getHeight()) || (destination.getBpp() != destBpp) ||
			((destBpp == 16) && (unsignedFlag != destination.getUnsignedFlag())))
		{
			ret = destination.set(destination.getOffsetX(),destination.getOffsetY(), source.getWidth(),
				                  source.getHeight(), source.getBands(), destBpp, unsignedFlag);
		};

		if (ret)
		{
			if ((destination.getBpp() == 1) || (destination.getBpp() == 8))
			{
				if ((source.getBpp() == 1) || (source.getBpp() == 8)) convolveUcUc (source, destination);
				else ret = false;
			}
			else if (destination.getBpp() == 16)
			{		
				if (destination.getUnsignedFlag()) 
				{
					if ((source.getBpp() == 1) || (source.getBpp() == 8)) convolveUcUs (source, destination);
					else if (source.getBpp() == 16) 
					{			
						if (source.getUnsignedFlag()) convolveUsUs(source, destination);
						else  convolveShUs(source, destination);
					}
					else ret = false;
				}
				else
				{
					if ((source.getBpp() == 1) || (source.getBpp() == 8)) convolveUcSh (source, destination);
					else if (source.getBpp() == 16) 
					{			
						if (source.getUnsignedFlag()) convolveUsSh(source, destination);
						else  convolveShSh(source, destination);
					}
					else ret = false;
				};
			}
			else if (destination.getBpp() == 32)
			{
				if ((source.getBpp() == 1) || (source.getBpp() == 8)) convolveUcFl (source, destination);
				else if (source.getBpp() == 16) 			
				{			
					if (source.getUnsignedFlag()) convolveUsFl(source, destination);
					else  convolveShFl(source, destination);	
				}	
				else if (source.getBpp() == 32) convolveFlFl(source, destination);
				else ret = false;
			}
			else 
			{			
				ret = false;
			};

			if (resetMargin)
			{
				int margin = this->getOffsetX();
				if (this->getOffsetY() > margin) margin = this->getOffsetY();
				destination.setMargin(margin, 0);
			}
		};
	};
	return ret;
};
	   
bool CImageFilter::isDerivative() const
{
	if ((type == eSobelX)     || (type == eSobelY)     || (type == eDiffX)    || (type == eDiffY) ||
		(type == eLaplaceA)   || (type == eLaplaceB)   || (type == eLaplaceC) || (type == eLoG)   ||
		(type == eDiffXGauss) || (type == eDiffYGauss) || (type == eDiffXDeriche) || 
		(type == eDiffYDeriche))  return true;
	return false;
};


bool CImageFilter::isSeparable() const
{
	if ((type == eAverage) || (type == eGauss) || (type == eBinom)) return true;
	return false;
};
	   
float CImageFilter::getVarianceFactor() const
{
	float factor = 0.0f;
	int length = getLength();
	for (int i = 0; i < length; ++i) 
		factor += this->filterValues[i] * this->filterValues[i];
	
	return factor;
};

float CImageFilter::getSigmaFactor() const
{
	return sqrt(getVarianceFactor());
}
	   
void CImageFilter::dump(ostream &OutStream) const
{
	OutStream.setf(ios::floatfield, ios::fixed);
	OutStream.precision(9);
	OutStream << "Filter matrix: \n";
	for (int j = 0; j < sizeY; ++j)
	{
		for (int i = 0; i < sizeX; ++i)
		{
			OutStream.width(12);
			OutStream << this->filterValues[j * sizeX + i];
		}
		OutStream << "\n";
	}
	OutStream << endl;
};

void CImageFilter::convolveUcUc(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixUChar(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixUChar(u) = (unsigned char) sum;
			};
		}
	};	
};

void CImageFilter::convolveUcUs(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixUChar(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixUShort(u) = (unsigned short) sum;
			};
		}
	};
};

void CImageFilter::convolveUcSh(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixUChar(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixShort(u) = (short) sum;
			};
		}
	};
};

void CImageFilter::convolveUcFl(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixUChar(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixFlt(u) = sum;
			};
		}
	};
};

void CImageFilter::convolveUsUs(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixUShort(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixUShort(u) = (unsigned short) sum;
			};
		}
	};
};

void CImageFilter::convolveUsSh(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixUShort(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixShort(u) = (short) sum;
			};
		}
	};
};

void CImageFilter::convolveUsFl(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixUShort(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixFlt(u) = sum;
			};
		}
	};
};

void CImageFilter::convolveShUs(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixShort(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixUShort(u) = (unsigned short) sum;
			};
		}
	};
};

void CImageFilter::convolveShSh(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixShort(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixShort(u) = (short) sum;
			};
		}
	};
};

void CImageFilter::convolveShFl(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += float(source.pixShort(uFiltOld)) * filterValues[uFilt];
					};
				};

				destination.pixFlt(u) = sum;
			};
		}
	};
};

void CImageFilter::convolveFlFl(const CImageBuffer &source, CImageBuffer& destination) const
{
	int minFiltX = this->getOffsetX();
	int minFiltY = this->getOffsetY();
	int maxFiltX = sizeX - minFiltX - 1;
	int maxFiltY = sizeY - minFiltY - 1;

	long diffFirst = long(-minFiltY) * destination.getDiffY() - minFiltX * destination.getDiffX();
	long filterOffsetX = sizeX * destination.getDiffX();
	long filterOffset = (sizeY - 1) * destination.getDiffY() + filterOffsetX;
	unsigned long nPxRow = (destination.getWidth() + 1 - sizeX) * destination.getDiffX();
	for (int band = 0; band < destination.getBands(); ++band)
	{
		unsigned long uMin = destination.getIndex(minFiltY, minFiltX) + band;
		unsigned long uMax = destination.getIndex(destination.getHeight() - maxFiltY - 1, 
			                                      destination.getWidth()  - maxFiltX - 1);

		for (unsigned long uMaxRow = uMin + nPxRow; uMin < uMax; uMin += destination.getDiffY(), uMaxRow += destination.getDiffY())
		{
			for (unsigned long u = uMin; u < uMaxRow; u += destination.getDiffX())
			{
				float sum = 0.0f;
				unsigned long uFilt = 0;
				unsigned long uFiltOldMin = u + diffFirst;
				unsigned long uFiltOldMax = uFiltOldMin + filterOffset;

				for (; uFiltOldMin < uFiltOldMax; uFiltOldMin += destination.getDiffY())
				{
					unsigned long uFiltOldMaxRow = uFiltOldMin + filterOffsetX;
					for (unsigned long uFiltOld = uFiltOldMin; uFiltOld < uFiltOldMaxRow; ++uFilt, uFiltOld += destination.getDiffX())
					{
						sum += source.pixFlt(uFiltOld) * filterValues[uFilt];
					};
				};

				destination.pixFlt(u) = sum;
			};
		}
	};
};

bool CImageFilter::binaryMorphologicalOpen(const CImageBuffer &source,  CImageBuffer& destination, float value) const
{
	bool ret = true;
	if ((destination.getWidth() != source.getWidth()) || (destination.getHeight() != source.getHeight()) || 
		(destination.getBands() > 1) || (destination.getBpp() != 1))
	{
		ret = destination.set(source.getOffsetX(), source.getOffsetY(), source.getWidth(), source.getHeight(), 1,1,true);
	}
	else ret = destination.setOffset(source.getOffsetX(), source.getOffsetY());

	if (ret)
	{
		if (source.getBpp() != 1)
		{
			CImageBuffer binaryImg(source);
			ret = binaryImg.convertToBinary(value);
			if (ret) ret = binaryMorphologicalOpen(binaryImg,  destination, value);
		}
		else
		{
			destination.setAll(0);

			long diffIdxMin =  - (sizeX / 2) * source.getDiffX() - (sizeY / 2) * source.getDiffY();
			long diffIdxMax = (sizeX - sizeX / 2 - 1) * source.getDiffX() + (sizeY - sizeY / 2 - 1) * source.getDiffY();

			unsigned long uMin = source.getIndex(sizeY / 2, sizeX / 2);
			unsigned long uMax = source.getIndex(source.getHeight() - sizeY / 2 - 1, source.getWidth() - sizeX / 2);
			unsigned long lastInRow = source.getIndex(sizeY / 2, source.getWidth() - sizeX / 2);

			if (!(sizeY % 2))
			{
				uMax += source.getDiffY();
			}

			if (!(sizeX % 2))
			{
				lastInRow += source.getDiffX();
			}

			for (; uMin < uMax; lastInRow += source.getDiffY(), uMin += source.getDiffY()) 
			{
				for (unsigned long u = uMin; u  < lastInRow; u += source.getDiffX())
				{
					unsigned long firstInElement   = u + diffIdxMin;
					unsigned long lastInElementRow = firstInElement + sizeX;
					unsigned long lastInElement    = u + diffIdxMax;
					unsigned long strIdxMin = 0;

					bool isOK = true;
					for (;(firstInElement < lastInElement) && (isOK); strIdxMin += sizeY, 
						firstInElement += source.getDiffY(), lastInElementRow += source.getDiffY())
					{
						unsigned long strIdx = strIdxMin;
						unsigned long act    = firstInElement;

						for (; (act < lastInElementRow) && (isOK); strIdx++, act += source.getDiffX())
						{
							if (this->filterValues[strIdx] > 0.0f) isOK = isOK && (source.pixUChar(act) > 0);	
						};
					};

					strIdxMin = 0;
					firstInElement = u + diffIdxMin;
					lastInElementRow = firstInElement + sizeX;

					for (;(firstInElement < lastInElement) && (isOK); strIdxMin += sizeY, 
						firstInElement += source.getDiffY(), lastInElementRow += source.getDiffY())
					{
						unsigned long strIdx = strIdxMin;
						unsigned long act    = firstInElement;
      
						for (; act < lastInElementRow; strIdx++, act += source.getDiffX() )
						{
							if (this->filterValues[strIdx] > 0.0f) destination.pixUChar(act) = 1;
						};  
					}

				}
			}
		}
	}
	return ret;
};

bool CImageFilter::binaryMorphologicalClose(const CImageBuffer &source, CImageBuffer& destination, float value) const
{
	bool ret = true;
	if ((destination.getWidth() != source.getWidth()) || (destination.getHeight() != source.getHeight()) || 
		(destination.getBands() > 1) || (destination.getBpp() != 1))
	{
		ret = destination.set(source.getOffsetX(), source.getOffsetY(), source.getWidth(), source.getHeight(), 1,1,true);
	}
	else ret = destination.setOffset(source.getOffsetX(), source.getOffsetY());

	if (ret)
	{
		if (source.getBpp() != 1)
		{
			CImageBuffer binaryImg(source);
			ret = binaryImg.convertToBinary(value);
			if (ret) ret = binaryMorphologicalClose(binaryImg,  destination, value);
		}
		else
		{
			destination.setAll(0);

			long diffIdxMin =  - (sizeX / 2) * source.getDiffX() - (sizeY / 2) * source.getDiffY();
			long diffIdxMax = (sizeX - sizeX / 2 - 1) * source.getDiffX() + (sizeY - sizeY / 2 - 1) * source.getDiffY();

			unsigned long uMin = source.getIndex(sizeY / 2, sizeX / 2);
			unsigned long uMax = source.getIndex(source.getHeight() - sizeY / 2 - 1, source.getWidth() - sizeX / 2);
			unsigned long lastInRow = source.getIndex(sizeY / 2, source.getWidth() - sizeX / 2);

			if (!(sizeY % 2))
			{
				uMax += source.getDiffY();
			}

			if (!(sizeX % 2))
			{
				lastInRow += source.getDiffX();
			}

			for (; uMin < uMax; lastInRow += source.getDiffY(), uMin += source.getDiffY()) 
			{
				unsigned long u = uMin;

				for (; u  < lastInRow; u += source.getDiffX())
				{
					unsigned long firstInElement   = u + diffIdxMin;
					unsigned long lastInElementRow = firstInElement + sizeX;
					unsigned long lastInElement    = u + diffIdxMax;
					unsigned long strIdxMin = 0;

					bool isOK = false;
					for (;(firstInElement < lastInElement) && (!isOK); strIdxMin += sizeY, 
						firstInElement += source.getDiffY(), lastInElementRow += source.getDiffY())
					{
						unsigned long strIdx = strIdxMin;
						unsigned long act    = firstInElement;

						for (; (act < lastInElementRow) && (!isOK); strIdx++, act += source.getDiffX())
						{
							if (this->filterValues[strIdx] > 0.0f) isOK = (source.pixUChar(act) > 0.0f);
						};
					};

					strIdxMin = 0;
					firstInElement = u + diffIdxMin;
					lastInElementRow = firstInElement + sizeX;

					for (;(firstInElement < lastInElement) && (isOK); strIdxMin += sizeY, 
						firstInElement += source.getDiffY(), lastInElementRow += source.getDiffY())
					{
						unsigned long strIdx = strIdxMin;
						unsigned long act    = firstInElement;
      
						for (; act < lastInElementRow; strIdx++, act += source.getDiffX() )
						{
							if (this->filterValues[strIdx] > 0.0f) destination.pixUChar(act) = 1;
						};  
					}
				}
			}
		}
	}
	return ret;
}

bool CImageFilter::minimumFilter(const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const
{  
	if ((source.getBpp() != 32) || (source.getBands() > 1)) return false;

	//This implementation separates the filter
	CImageBuffer intermediate(source.getOffsetX(), source.getOffsetY(), source.getWidth(), 
							  source.getHeight(),  source.getBands(), source.getBpp(), 
							  source.getUnsignedFlag());
	int halfX = this->sizeX / 2;
	int halfY = this->sizeY / 2;

	// first, process columns

	unsigned long firstIndex = 0; 
	unsigned long lastIndex = source.getNGV();  
                  
	unsigned long lastColumnIndex = lastIndex -  source.getDiffY() + 1; 
	unsigned long index;

	int diff1 = this->sizeX * source.getDiffY();
	int diff2 = halfX * source.getDiffY();

	while (lastColumnIndex <= lastIndex)
	{
		index = firstIndex;

		unsigned long firstFullIndex = firstIndex + diff1; 
		unsigned long lastFullIndex  = lastColumnIndex - diff2; 
		unsigned long lastPartIndex  = firstIndex + diff2; 

		while (lastPartIndex < firstFullIndex)
		{
			float currentMin = FLT_MAX;

			for (unsigned long filterIndex = firstIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if ((value < currentMin) && (value != excludeValue)) currentMin = value;
			};

			if (currentMin < FLT_MAX) intermediate.pixFlt(index) = currentMin;
			else intermediate.pixFlt(index) = excludeValue;

			lastPartIndex += source.getDiffY();
			index += source.getDiffY();
		};

		unsigned long firstPartIndex = firstIndex; 

		while (index < lastFullIndex)
		{
			float currentMin = FLT_MAX;   
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if ((value < currentMin) && (value != excludeValue)) currentMin = value;
			};

			if (currentMin < FLT_MAX) intermediate.pixFlt(index) = currentMin;
			else intermediate.pixFlt(index) = excludeValue;
			firstPartIndex += source.getDiffY();
			lastPartIndex += source.getDiffY();
			index += source.getDiffY();
		};


		while (index < lastColumnIndex)
		{
			float currentMin = FLT_MAX;
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastColumnIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if ((value < currentMin) && (value != excludeValue)) currentMin = value;
			};

			if (currentMin < FLT_MAX) intermediate.pixFlt(index) = currentMin;
			else intermediate.pixFlt(index) = excludeValue;
			firstPartIndex += source.getDiffY();
			index += source.getDiffY();
		};

		firstIndex += source.getDiffX();
		lastColumnIndex += source.getDiffX();
	};

	// second,process rows

	firstIndex = 0;                         

	unsigned long lastRowIndex = firstIndex +  source.getDiffY(); 
 
	while (lastRowIndex <= lastIndex)
	{
		index = firstIndex;
		unsigned long firstFullIndex = firstIndex   + this->sizeY; 
		unsigned long lastFullIndex  = lastRowIndex -  halfY; 
		unsigned long lastPartIndex  = firstIndex   +  halfY; 

		while (lastPartIndex < firstFullIndex)
		{
			float currentMin = FLT_MAX;

			for (unsigned long filterIndex = firstIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if ((value < currentMin) && (value != excludeValue)) currentMin = value;
			};

			if (currentMin < FLT_MAX) destination.pixFlt(index) = currentMin;
			else destination.pixFlt(index) = excludeValue;

			lastPartIndex += source.getDiffX();
			index += source.getDiffX();
		};
  
		unsigned long firstPartIndex = firstIndex; 
  
		while (index < lastFullIndex)
		{
			float currentMin = FLT_MAX;
   
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if ((value < currentMin) && (value != excludeValue)) currentMin = value;
			};

			if (currentMin < FLT_MAX) destination.pixFlt(index) = currentMin;
			else destination.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffX();
			lastPartIndex += source.getDiffX();
			index += source.getDiffX();
		};

		while (index < lastRowIndex)
		{
			float currentMin = FLT_MAX;
    
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastRowIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if ((value < currentMin) && (value != excludeValue)) currentMin = value;
			};

			if (currentMin < FLT_MAX) destination.pixFlt(index) = currentMin;
			else destination.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffX();;
			index += source.getDiffX();;
		};

		firstIndex += source.getDiffY();
		lastRowIndex += source.getDiffY();
	};

	return true;
};

bool CImageFilter::maximumFilter(const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const
{  
	if ((source.getBpp() != 32) || (source.getBands() > 1)) return false;

	//This implementation separates the filter
	CImageBuffer intermediate(source.getOffsetX(), source.getOffsetY(), source.getWidth(), 
							  source.getHeight(),  source.getBands(), source.getBpp(), 
							  source.getUnsignedFlag());
	int halfX = this->sizeX / 2;
	int halfY = this->sizeY / 2;

	// first, process columns

	unsigned long firstIndex = 0; 
	unsigned long lastIndex = source.getNGV();  
                  
	unsigned long lastColumnIndex = lastIndex -  source.getDiffY() + 1; 
	unsigned long index;

	int diff1 = this->sizeX * source.getDiffY();
	int diff2 = halfX * source.getDiffY();

	while (lastColumnIndex <= lastIndex)
	{
		index = firstIndex;

		unsigned long firstFullIndex = firstIndex + diff1; 
		unsigned long lastFullIndex  = lastColumnIndex - diff2; 
		unsigned long lastPartIndex  = firstIndex + diff2; 

		while (lastPartIndex < firstFullIndex)
		{
			float currentMax = -FLT_MAX;

			for (unsigned long filterIndex = firstIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if ((value > currentMax) && (value != excludeValue)) currentMax = value;
			};

			if (currentMax > -FLT_MAX) intermediate.pixFlt(index) = currentMax;
			else intermediate.pixFlt(index) = excludeValue;

			lastPartIndex += source.getDiffY();
			index += source.getDiffY();
		};

		unsigned long firstPartIndex = firstIndex; 

		while (index < lastFullIndex)
		{
			float currentMax = -FLT_MAX;
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if ((value > currentMax) && (value != excludeValue)) currentMax = value;
			};

			if (currentMax > -FLT_MAX) intermediate.pixFlt(index) = currentMax;
			else intermediate.pixFlt(index) = excludeValue;
			firstPartIndex += source.getDiffY();
			lastPartIndex += source.getDiffY();
			index += source.getDiffY();
		};


		while (index < lastColumnIndex)
		{
			float currentMax = -FLT_MAX;
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastColumnIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if ((value > currentMax) && (value != excludeValue)) currentMax = value;
			};

			if (currentMax > -FLT_MAX) intermediate.pixFlt(index) = currentMax;
			else intermediate.pixFlt(index) = excludeValue;
			firstPartIndex += source.getDiffY();
			index += source.getDiffY();
		};

		firstIndex += source.getDiffX();
		lastColumnIndex += source.getDiffX();
	};

	// second,process rows

	firstIndex = 0;                         

	unsigned long lastRowIndex = firstIndex +  source.getDiffY(); 
 
	while (lastRowIndex <= lastIndex)
	{
		index = firstIndex;
		unsigned long firstFullIndex = firstIndex   + this->sizeY; 
		unsigned long lastFullIndex  = lastRowIndex -  halfY; 
		unsigned long lastPartIndex  = firstIndex   +  halfY; 

		while (lastPartIndex < firstFullIndex)
		{
			float currentMax = -FLT_MAX;

			for (unsigned long filterIndex = firstIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if ((value > currentMax) && (value != excludeValue)) currentMax = value;
			};

			if (currentMax > -FLT_MAX) destination.pixFlt(index) = currentMax;
			else destination.pixFlt(index) = excludeValue;

			lastPartIndex += source.getDiffX();
			index += source.getDiffX();
		};
  
		unsigned long firstPartIndex = firstIndex; 
  
		while (index < lastFullIndex)
		{
			float currentMax = -FLT_MAX;
   
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if ((value > currentMax) && (value != excludeValue)) currentMax = value;
			};

			if (currentMax > -FLT_MAX) destination.pixFlt(index) = currentMax;
			else destination.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffX();
			lastPartIndex += source.getDiffX();
			index += source.getDiffX();
		};

		while (index < lastRowIndex)
		{
			float currentMax = -FLT_MAX;
    
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastRowIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if ((value > currentMax) && (value != excludeValue)) currentMax = value;
			};

			if (currentMax > -FLT_MAX) destination.pixFlt(index) = currentMax;
			else destination.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffX();;
			index += source.getDiffX();;
		};

		firstIndex += source.getDiffY();
		lastRowIndex += source.getDiffY();
	};

	return true;
 };

bool CImageFilter::minimumRankFilter(int rank, const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const
{  
	if ((source.getBpp() != 32) || (source.getBands() > 1) || (rank >= this->sizeX * this->sizeY)) return false;

	CGenericStackAscending<float>stack(rank);

	//This implementation separates the filter
	CImageBuffer intermediate(source.getOffsetX(), source.getOffsetY(), source.getWidth(), 
							  source.getHeight(),  source.getBands(), source.getBpp(), 
							  source.getUnsignedFlag());
	int halfX = this->sizeX / 2;
	int halfY = this->sizeY / 2;

	// first, process columns

	unsigned long firstIndex = 0; 
	unsigned long lastIndex = source.getNGV();  
                  
	unsigned long lastColumnIndex = lastIndex -  source.getDiffY() + 1; 
	unsigned long index;

	int diff1 = this->sizeX * source.getDiffY();
	int diff2 = halfX * source.getDiffY();

	while (lastColumnIndex <= lastIndex)
	{
		index = firstIndex;

		unsigned long firstFullIndex = firstIndex + diff1; 
		unsigned long lastFullIndex  = lastColumnIndex - diff2; 
		unsigned long lastPartIndex  = firstIndex + diff2; 

		while (lastPartIndex < firstFullIndex)
		{
			stack.reset();

			for (unsigned long filterIndex = firstIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) intermediate.pixFlt(index) = stack[stackIdx];
			else intermediate.pixFlt(index) = excludeValue;

			lastPartIndex += source.getDiffY();
			index += source.getDiffY();
		};

		unsigned long firstPartIndex = firstIndex; 

		while (index < lastFullIndex)
		{
			stack.reset();
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) intermediate.pixFlt(index) = stack[stackIdx];
			else intermediate.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffY();
			lastPartIndex += source.getDiffY();
			index += source.getDiffY();
		};


		while (index < lastColumnIndex)
		{
			stack.reset();
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastColumnIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) intermediate.pixFlt(index) = stack[stackIdx];
			else intermediate.pixFlt(index) = excludeValue;
			firstPartIndex += source.getDiffY();
			index += source.getDiffY();
		};

		firstIndex += source.getDiffX();
		lastColumnIndex += source.getDiffX();
	};

	// second,process rows

	firstIndex = 0;                         

	unsigned long lastRowIndex = firstIndex +  source.getDiffY(); 
 
	while (lastRowIndex <= lastIndex)
	{
		index = firstIndex;
		unsigned long firstFullIndex = firstIndex   + this->sizeY; 
		unsigned long lastFullIndex  = lastRowIndex -  halfY; 
		unsigned long lastPartIndex  = firstIndex   +  halfY; 

		while (lastPartIndex < firstFullIndex)
		{
			stack.reset();
			
			for (unsigned long filterIndex = firstIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) destination.pixFlt(index) = stack[stackIdx];
			else destination.pixFlt(index) = excludeValue;

			lastPartIndex += source.getDiffX();
			index += source.getDiffX();
		};
  
		unsigned long firstPartIndex = firstIndex; 
  
		while (index < lastFullIndex)
		{
			stack.reset();
			   
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) destination.pixFlt(index) = stack[stackIdx];
			else destination.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffX();
			lastPartIndex += source.getDiffX();
			index += source.getDiffX();
		};

		while (index < lastRowIndex)
		{
			stack.reset();
			    
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastRowIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) destination.pixFlt(index) = stack[stackIdx];
			else destination.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffX();;
			index += source.getDiffX();;
		};

		firstIndex += source.getDiffY();
		lastRowIndex += source.getDiffY();
	};

	return true;
};

bool CImageFilter::maximumRankFilter(int rank, const CImageBuffer &source,  CImageBuffer& destination, float excludeValue) const
{  
	if ((source.getBpp() != 32) || (source.getBands() > 1) || (rank >= this->sizeX * this->sizeY)) return false;
	
	CGenericStackDescending<float>stack(rank);

	//This implementation separates the filter
	CImageBuffer intermediate(source.getOffsetX(), source.getOffsetY(), source.getWidth(), 
							  source.getHeight(),  source.getBands(), source.getBpp(), 
							  source.getUnsignedFlag());
	int halfX = this->sizeX / 2;
	int halfY = this->sizeY / 2;

	// first, process columns

	unsigned long firstIndex = 0; 
	unsigned long lastIndex = source.getNGV();  
                  
	unsigned long lastColumnIndex = lastIndex -  source.getDiffY() + 1; 
	unsigned long index;

	int diff1 = this->sizeX * source.getDiffY();
	int diff2 = halfX * source.getDiffY();

	while (lastColumnIndex <= lastIndex)
	{
		index = firstIndex;

		unsigned long firstFullIndex = firstIndex + diff1; 
		unsigned long lastFullIndex  = lastColumnIndex - diff2; 
		unsigned long lastPartIndex  = firstIndex + diff2; 

		while (lastPartIndex < firstFullIndex)
		{
			stack.reset();

			for (unsigned long filterIndex = firstIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) intermediate.pixFlt(index) = stack[stackIdx ];
			else intermediate.pixFlt(index) = excludeValue;

			lastPartIndex += source.getDiffY();
			index += source.getDiffY();
		};

		unsigned long firstPartIndex = firstIndex; 

		while (index < lastFullIndex)
		{
			stack.reset();
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) intermediate.pixFlt(index) = stack[stackIdx ];
			else intermediate.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffY();
			lastPartIndex += source.getDiffY();
			index += source.getDiffY();
		};


		while (index < lastColumnIndex)
		{
			stack.reset();
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastColumnIndex; filterIndex += source.getDiffY())
			{
				float value = source.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) intermediate.pixFlt(index) = stack[stackIdx ];
			else intermediate.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffY();
			index += source.getDiffY();
		};

		firstIndex += source.getDiffX();
		lastColumnIndex += source.getDiffX();
	};

	// second,process rows

	firstIndex = 0;                         

	unsigned long lastRowIndex = firstIndex +  source.getDiffY(); 
 
	while (lastRowIndex <= lastIndex)
	{
		index = firstIndex;
		unsigned long firstFullIndex = firstIndex   + this->sizeY; 
		unsigned long lastFullIndex  = lastRowIndex -  halfY; 
		unsigned long lastPartIndex  = firstIndex   +  halfY; 

		while (lastPartIndex < firstFullIndex)
		{
			stack.reset();

			for (unsigned long filterIndex = firstIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) destination.pixFlt(index) = stack[stackIdx ];
			else destination.pixFlt(index) = excludeValue;

			lastPartIndex += source.getDiffX();
			index += source.getDiffX();
		};
  
		unsigned long firstPartIndex = firstIndex; 
  
		while (index < lastFullIndex)
		{
			stack.reset();
   
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastPartIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) destination.pixFlt(index) = stack[stackIdx ];
			else destination.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffX();
			lastPartIndex += source.getDiffX();
			index += source.getDiffX();
		};

		while (index < lastRowIndex)
		{
			stack.reset();
    
			for (unsigned long filterIndex = firstPartIndex; filterIndex < lastRowIndex; filterIndex += source.getDiffX())
			{
				float value = intermediate.pixFlt(filterIndex);
				if (value != excludeValue) stack.checkInsertRecord(value);
			};

			int stackIdx = stack.CurrentSize() - 1;
			if (stackIdx >= 0) destination.pixFlt(index) = stack[stackIdx ];
			else destination.pixFlt(index) = excludeValue;

			firstPartIndex += source.getDiffX();;
			index += source.getDiffX();;
		};

		firstIndex += source.getDiffY();
		lastRowIndex += source.getDiffY();
	};

	return true;
 };

bool CImageFilter::greyMorphologicalOpen (const CImageBuffer &source,  
										  CImageBuffer& destination, 
										  float excludeValue) const
{
	CImageBuffer intermediate(source.getOffsetX(), source.getOffsetY(), source.getWidth(), 
							  source.getHeight(),  source.getBands(), source.getBpp(), 
							  source.getUnsignedFlag());
 
	bool ret = this->minimumFilter(source, intermediate, excludeValue);
	if (ret) 
		 ret = this->maximumFilter(intermediate, destination, excludeValue);

	return ret;
};


bool CImageFilter::greyMorphologicalClose (const CImageBuffer &source,  
										   CImageBuffer& destination, 
										   float excludeValue) const
{
	CImageBuffer intermediate(source.getOffsetX(), source.getOffsetY(), source.getWidth(), 
							  source.getHeight(),  source.getBands(), source.getBpp(), 
							  source.getUnsignedFlag());
 
	bool ret = this->maximumFilter(source, intermediate, excludeValue);
	if (ret) 
		 ret = this->minimumFilter(intermediate, destination, excludeValue);

	return ret;
};

bool CImageFilter::greyRankOpen (int rank, const CImageBuffer &source,  
								 CImageBuffer& destination, 
								 float excludeValue) const
{
	CImageBuffer intermediate(source.getOffsetX(), source.getOffsetY(), source.getWidth(), 
							  source.getHeight(),  source.getBands(), source.getBpp(), 
							  source.getUnsignedFlag());
 
	bool ret = this->minimumRankFilter(rank, source, intermediate, excludeValue);
	if (ret) 
		 ret = this->maximumRankFilter(rank, intermediate, destination, excludeValue);

	return ret;
};


bool CImageFilter::greyRankClose (int rank, const CImageBuffer &source,  
								  CImageBuffer& destination, 
								  float excludeValue) const
{
	CImageBuffer intermediate(source.getOffsetX(), source.getOffsetY(), source.getWidth(), 
							  source.getHeight(),  source.getBands(), source.getBpp(), 
							  source.getUnsignedFlag());
 
	bool ret = this->maximumRankFilter(rank, source, intermediate, excludeValue);
	if (ret) 
		 ret = this->minimumRankFilter(rank, intermediate, destination, excludeValue);

	return ret;
};


bool CImageFilter::binaryMinimumFilter (const CImageBuffer &source,  CImageBuffer& destination, float value)   const
{
	bool ret = true;
	if ((destination.getWidth() != source.getWidth()) || (destination.getHeight() != source.getHeight()) || 
		(destination.getBands() > 1) || (destination.getBpp() != 1))
	{
		ret = destination.set(source.getOffsetX(), source.getOffsetY(), source.getWidth(), source.getHeight(), 1,1,true);
	}
	else ret = destination.setOffset(source.getOffsetX(), source.getOffsetY());

	if (ret)
	{
		if (source.getBpp() != 1)
		{
			CImageBuffer binaryImg(source);
			ret = binaryImg.convertToBinary(value);
			if (ret) ret = binaryMinimumFilter(binaryImg,  destination, value);
		}
		else
		{
			int XC = (this->sizeX -1) / 2;
			int YC = (this->sizeY -1) / 2;

			unsigned long lastInpInRowFilter = this->sizeY * source.getDiffY();
			unsigned long firstResInRow = YC * destination.getDiffY() + XC * destination.getDiffX();

			unsigned long dIndexX = source.getDiffX() * this->sizeX;
			unsigned long dIndex  = dIndexX + source.getDiffY() * this->sizeY;

			for (; lastInpInRowFilter < source.getNGV(); lastInpInRowFilter += source.getDiffY(), 
				                                         firstResInRow += destination.getDiffY()) 
			{
				unsigned long firstInpInFilterMax  = lastInpInRowFilter - source.getDiffY() + dIndexX;
				unsigned long resIdx = firstResInRow;
				for (; firstInpInFilterMax < lastInpInRowFilter; firstInpInFilterMax += source.getDiffX(), 
					                                             resIdx += destination.getDiffX())
				{
					unsigned char isOK = 1;

					unsigned long firstCurrInFilter = firstInpInFilterMax - dIndex;
					int iFilterIndex = 0;
					for (; (firstCurrInFilter < firstInpInFilterMax) && (isOK > 0); firstCurrInFilter += source.getDiffY())
					{	
						unsigned long lastCurrInFilter = firstCurrInFilter + dIndexX;

						for (unsigned long curr = firstCurrInFilter; curr < lastCurrInFilter; curr += source.getDiffX(), iFilterIndex++)
						{
							if (!source.pixUChar(curr)) isOK = 0;
						}
					}

					destination.pixUChar(resIdx) = isOK;
				};
			}
		}	
	}
	
	return ret;
};

bool CImageFilter::binaryMaximumFilter(const CImageBuffer &source, CImageBuffer& destination, float value) const
{
	bool ret = true;
	if ((destination.getWidth() != source.getWidth()) || (destination.getHeight() != source.getHeight()) || 
		(destination.getBands() > 1) || (destination.getBpp() != 1))
	{
		ret = destination.set(source.getOffsetX(), source.getOffsetY(), source.getWidth(), source.getHeight(), 1,1,true);
	}
	else ret = destination.setOffset(source.getOffsetX(), source.getOffsetY());

	if (ret)
	{
		if (source.getBpp() != 1)
		{
			CImageBuffer binaryImg(source);
			ret = binaryImg.convertToBinary(value);
			if (ret) ret = binaryMaximumFilter(binaryImg,  destination, value);
		}
		else
		{
			int XC = (this->sizeX -1) / 2;
			int YC = (this->sizeY -1) / 2;

			unsigned long lastInpInRowFilter = this->sizeY * source.getDiffY();
			unsigned long firstResInRow = YC * destination.getDiffY() + XC * destination.getDiffX();

			unsigned long dIndexX = source.getDiffX() * this->sizeX;
			unsigned long dIndex  = dIndexX + source.getDiffY() * this->sizeY;

			for (; lastInpInRowFilter < source.getNGV(); lastInpInRowFilter += source.getDiffY(), 
				                                         firstResInRow += destination.getDiffY()) 
			{
				unsigned long firstInpInFilterMax  = lastInpInRowFilter - source.getDiffY() + dIndexX;
				unsigned long resIdx = firstResInRow;
				for (; firstInpInFilterMax < lastInpInRowFilter; firstInpInFilterMax+= source.getDiffX(), 
					                                             resIdx += destination.getDiffX())
				{
					unsigned char isOK = 0;

					unsigned long firstCurrInFilter = firstInpInFilterMax - dIndex;
					int iFilterIndex = 0;
					for (; (firstCurrInFilter < firstInpInFilterMax) && (isOK == 0); firstCurrInFilter += source.getDiffY())
					{
						unsigned long lastCurrInFilter = firstCurrInFilter + dIndexX;

						for (unsigned long curr = firstCurrInFilter; curr < lastCurrInFilter; curr += source.getDiffX(), iFilterIndex++)
						{
							if (source.pixUChar(curr)) isOK = 1;
						}
					}

					destination.pixUChar(resIdx) = isOK;
				}
			}
		}
	}

	return ret;
};
	   
float CImageFilter::getDericheFactor(float sigma)
{
	double sumsq = 0.0;
	CImageBuffer b(0,0,15,15,1,32,false,0.0);
	CImageBuffer dbx(b);
	CImageBuffer dby(b);
	b.pixFlt(b.getNGV() / 2) = 1.0;
	this->gradientDeriche(b, dbx, dby, sigma, false);

	for (unsigned int i = 0; i < b.getNGV(); ++i)
	{
		sumsq += dbx.pixFlt(i) *  dbx.pixFlt(i) +  dby.pixFlt(i) *  dby.pixFlt(i);
	}

	sumsq *= 0.5;

	return float(sumsq);
};

bool CImageFilter::gradientDeriche(const CImageBuffer &source, CImageBuffer& gradX, CImageBuffer& gradY, float sigma, bool resetMargin)
{
	bool ret = true;

	float alpha = 1.0f / sigma;

	float *InPic = 0;

	CImageBuffer fbuf;

	if ((source.getBands()) > 1 || source.getBpp() != 32)
	{
		fbuf.set(source);
		if (fbuf.getBands() > 1) fbuf.SetToIntensity();
		fbuf.convertToFloat();
		InPic = fbuf.getPixF();
	}
	else
	{
		InPic = source.getPixF();
	}


	int nElements = source.getWidth() * source.getHeight();
		
	float *Z1    = new float[source.getWidth()];
	float *Z2    = new float[source.getWidth()];
	float *Z3    = new float[source.getWidth()];
	float *B1    = new float[nElements];
	float *B2    = new float[nElements];
	float *B3    = new float[nElements];

	if ((gradX.getWidth() != source.getWidth())  || (gradX.getHeight() != source.getHeight()) || 
		(gradX.getBands() !=  1) || (gradX.getBpp() != 32))
	{
		ret = gradX.set(source.getOffsetX(), source.getOffsetY(), source.getWidth(), source.getHeight(), 1, 32, false, 0.0);
	}
	else 
	{
		gradX.setOffset(source.getOffsetX(), source.getOffsetY());
		gradX.setAll(0.0);
	}

	if (ret && ((gradY.getWidth() != source.getWidth())  || (gradY.getHeight() != source.getHeight()) || 
		(gradY.getBands() !=  1) || (gradY.getBpp() != 32)))
	{
		ret = gradY.set(source.getOffsetX(), source.getOffsetY(), source.getWidth(), source.getHeight(), 1, 32, false, 0.0);
	}
	else 
	{
		gradY.setOffset(source.getOffsetX(), source.getOffsetY());
		gradY.setAll(0.0);
	}

	if (!Z1 || !Z2 || !Z3 || !B1 || !B2 || !B3 || !InPic || !ret) 
	{
		ret = false;
	}
	else
	{
		int x, y, xmax;
		float    Z, h;

		float a  = -1.f * (1.f-exp(-alpha)) * (1.f-exp(-alpha));
		float b1 = -2.f * exp(-alpha);
		float b2 = exp(-2.f*alpha);
		float a0 = -a / (1.f - alpha*b1 - b2);
		float a1 = a0 * (alpha-1.f) * exp(-alpha);
		float a2 = a1 - a0*b1;
		float a3 = -a0 * b2;

		for (y=0; y < source.getHeight(); y++)
		{
			x = y * source.getWidth();
			xmax = x + source.getWidth();
			for (; x < xmax; x++) 
			{
				B1[x] = InPic[x];
				B2[x] = InPic[x];
			}
		}
		/* (i) */
		int idMinus, idPlus;
		for (y = 2; y < source.getHeight(); y++)
		{
			x = y * source.getWidth();
			xmax = x + source.getWidth();

			for (; x < xmax; x++)
			{
				idMinus = x - source.getWidth();
				B1[x] = InPic[idMinus] - b1 * B1[idMinus] - b2 * B1[idMinus - source.getWidth()];
			}
		}

		/* (ii) */
		for (y = source.getHeight() - 3; y >=0; y--)
		{
			x = y * source.getWidth();
			xmax = x + source.getWidth();

			for (; x<xmax; x++)
			{
				idPlus = x + source.getWidth();
				B2[x] = InPic[idPlus] - b1 * B2[idPlus] - b2 * B2[idPlus + source.getWidth()];
				B1[x] = a * (B1[x] - B2[x]);
			}
		}
		/* (iii), (iv) */
		for (y = 0; y < source.getHeight(); y++) 
		{
			int x0 = y * source.getWidth();
			for (x=0; x < source.getWidth(); x++) 
			{
				Z1[x] = B1[x+x0];
				Z2[x] = Z1[x];
				Z3[x] = Z1[x];
			}
			for (x=0+2; x < source.getWidth(); x++)
				Z2[x] = a0*Z1[x] + a1*Z1[x-1] - b1*Z2[x-1] - b2*Z2[x-2];
			for (x = source.getWidth()-3; x>=0; x--)
				Z3[x] = a2*Z1[x+1] + a3*Z1[x+2] - b1*Z3[x+1] - b2*Z3[x+2];
			for (x=0; x < source.getWidth(); x++)
				B3[x+x0] = Z2[x] + Z3[x];
		}

		for (y = 0; y < source.getHeight(); y++)
		{
			x = y * source.getWidth();
			xmax = x + source.getWidth();

			for (; x < xmax; x++)
			{
				B1[x] = InPic[x];
				B2[x] = InPic[x];
			}
		}

		/* (v), (vi) */
		for (y=0; y < source.getHeight(); y++)
		{
			int x0 = y * source.getWidth();

			for (x=0; x < source.getWidth(); x++)
			{
				Z2[x] = InPic[x+x0];
				Z3[x] = InPic[x+x0];
			}

			for (x=2; x < source.getWidth(); x++)
				Z2[x] = InPic[x-1+x0] - b1*Z2[x-1] - b2*Z2[x-2];

			for (x = source.getWidth()-3; x>=0; x--)
				Z3[x] = InPic[x+1+x0] - b1*Z3[x+1] - b2*Z3[x+2];

			for (x=0; x < source.getWidth(); x++)
				B1[x+x0] = a*(Z2[x]-Z3[x]);
		}

		/* (vii) */
		for (y = 2; y < source.getHeight(); y++)
		{
			x = y * source.getWidth();
			xmax = x + source.getWidth();

			for (; x < xmax; x++)
			{
				idMinus = x - source.getWidth();

				B2[x] = a0*B1[x] + a1*B1[x] - b1*B2[idMinus] - b2*B2[idMinus - source.getWidth()];
			}
		}

		/* viii */
		for (y = source.getHeight() - 3; y >= 0; y--) 
		{
			int x0 = y * source.getWidth();

			for (x=0; x < source.getWidth(); x++)  Z1[x] = B2[x+x0];

			x = y * source.getWidth();
			xmax = x + source.getWidth();

			for (; x < xmax; x++)
			{
				idPlus = x + source.getWidth();

				B2[x] = a2*B1[idPlus] + a3*B1[idPlus + source.getWidth()]
						   - b1*B2[idPlus] - b2*B2[idPlus + source.getWidth()];
			}

			for (x=0; x < source.getWidth(); x++) 
			{
				int id = x + x0;
				Z = B2[id] + Z1[x];
				h = B3[id];
				gradX.pixFlt(id) = Z;
				gradY.pixFlt(id) = h;
			}
		}
	}

	if (Z1) delete [] Z1;
	if (Z2) delete [] Z2;
	if (Z3) delete [] Z3;
	if (B1) delete [] B1;
	if (B2) delete [] B2;
	if (B3) delete [] B3;

	if (resetMargin) 
	{
		gradX.setMargin(2,0.0);
		gradY.setMargin(2,0.0);
	}

	return ret;
};

void CImageFilter::subtractFilter(CImageFilter F1)
{
	int w = F1.getOffsetX();
	int W = this->getOffsetX();
	int extra = W-w;
	for (int i = extra; i < (extra+F1.getLength()); i++)
	{
		this->filterValues[i] = this->filterValues[i] - F1.filterValues[i-extra];
	}
}