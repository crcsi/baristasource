#include "TransferFunctionFactory.h"
#include "Filename.h"
#include "IHSTransferFunction.h"
#include "WallisFilter.h"


CTransferFunction* CTransferFunctionFactory::create(const CTransferFunction& transferFunction)
{
	
	CTransferFunction* tF = transferFunction.copy();
	return tF;
}

CTransferFunction* CTransferFunctionFactory::create(const CTransferFunction* transferFunction)
{
	CTransferFunction* tF = NULL;
	if (transferFunction)
		tF = transferFunction->copy();
	
	return tF;
}


CTransferFunction* CTransferFunctionFactory::createFromLUT(CTransferFunction::Type type,const CFileName& imageFileName,const CLookupTable& lut)
{
	CTransferFunction* transferFunction = new CTransferFunction(0,0);

	if (!transferFunction->init(type,imageFileName,lut))
	{
		// make sure we init the transfer function always
		transferFunction->initDirectTransfer(imageFileName,lut.getBpp(),lut.getBands());
	}

	return transferFunction;	
}


CTransferFunction* CTransferFunctionFactory::createDirectTransfer(const CFileName& imageFileName,int bands,int bpp)
{
	CTransferFunction* transferFunction = new CTransferFunction(bpp,bands);

	if (!transferFunction->initDirectTransfer(imageFileName,bpp,bands))
	{
		delete transferFunction;
		transferFunction = NULL;
	}

	return transferFunction;
}


CTransferFunction* CTransferFunctionFactory::createLinearStretch(const CFileName& imageFileName,int bpp,double minValue, double maxValue)
{
	CTransferFunction* transferFunction = new CTransferFunction(bpp,1);

	if (!transferFunction->initLinearStretch(imageFileName,bpp,minValue,maxValue))
	{
		// make sure we init the transfer function always
		transferFunction->initDirectTransfer(imageFileName,bpp,1);
	}

	return transferFunction;
}

CTransferFunction* CTransferFunctionFactory::createEqualization(const CFileName& imageFileName,int bpp,const CHistogram* histogram,double minValue, double maxValue)
{
	CTransferFunction* transferFunction = new CTransferFunction(bpp,1);
	
	if (!transferFunction->initEqualization(imageFileName,bpp,histogram,minValue,maxValue))
	{
		// make sure we init the transfer function always
		transferFunction->initDirectTransfer(imageFileName,bpp,1);
	}

	return transferFunction;
}

CTransferFunction* CTransferFunctionFactory::createNormalisation(const CFileName& imageFileName,int bpp,const CHistogram* histogram,double minValue, double maxValue,double centre,double sigma)
{
	CTransferFunction* transferFunction = new CTransferFunction(bpp,1);
	
	if (!transferFunction->initNormalisation(imageFileName,bpp,histogram,minValue,maxValue,centre,sigma))
	{
		// make sure we init the transfer function always
		transferFunction->initDirectTransfer(imageFileName,bpp,1);
	}

	return transferFunction;
}


CTransferFunction* CTransferFunctionFactory::createLinearStretchIHS(const CFileName& imageFileName,int bpp,double minValue, double maxValue)
{
	CIHSTransferFunction* transferFunction = new CIHSTransferFunction(bpp);
	
	if (!transferFunction->initLinearStretchIHS(imageFileName,bpp,minValue,maxValue))
	{
		// make sure we init the transfer function always
		transferFunction->initDirectTransfer(imageFileName,bpp,3);
	}

	return transferFunction;
}


CTransferFunction* CTransferFunctionFactory::createEqualizationIHS(const CFileName& imageFileName,int bpp,const CHistogram* intensityHist,double minValue, double maxValue)
{
	CIHSTransferFunction* transferFunction = new CIHSTransferFunction(bpp);
	
	if (!transferFunction->initEqualizationIHS(imageFileName,bpp,intensityHist,minValue,maxValue))
	{
		// make sure we init the transfer function always
		transferFunction->initDirectTransfer(imageFileName,bpp,3);
	}

	return transferFunction;
}


CTransferFunction* CTransferFunctionFactory::createNormalisationIHS(const CFileName& imageFileName,int bpp,const CHistogram* intensityHist,double minValue, double maxValue,double centre,double sigma)
{
	CIHSTransferFunction* transferFunction = new CIHSTransferFunction(bpp);
	
	if (!transferFunction->initNormalisationIHS(imageFileName,bpp,intensityHist,minValue,maxValue,centre,sigma))
	{
		// make sure we init the transfer function always
		transferFunction->initDirectTransfer(imageFileName,bpp,3);
	}

	return transferFunction;
}

CTransferFunction* CTransferFunctionFactory::createWallisFilterFunction(const CFileName& imageFileName,int bpp, double minValue, double maxValue)
{
	CWallisFilter* transferFunction = new CWallisFilter(bpp,3);

	if (!transferFunction->init(imageFileName,minValue,maxValue))
	{
		// make sure we init the transfer function always
		transferFunction->initDirectTransfer(imageFileName,bpp,3);
	}

	return transferFunction;
}