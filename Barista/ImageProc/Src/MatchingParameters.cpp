#include "MatchingParameters.h"

#include "StructuredFile.h"

CMatchingParameters::CMatchingParameters()
{
	this->reset();
}

CMatchingParameters::~CMatchingParameters()
{
}

void CMatchingParameters::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("patchsize", this->patchsize);
    token = pStructuredFileSection->addToken();
	token->setValue("showMatchWindow", this->showMatchWindow);
    token = pStructuredFileSection->addToken();
	token->setValue("iterationSteps", this->iterationSteps);
    token = pStructuredFileSection->addToken();
	token->setValue("sigmaGrey8Bit", this->sigmaGrey8Bit);
    token = pStructuredFileSection->addToken();
	token->setValue("sigmaGrey16Bit", this->sigmaGrey16Bit);
    token = pStructuredFileSection->addToken();
	token->setValue("sigmaGeom", this->sigmaGeom);
    token = pStructuredFileSection->addToken();
	token->setValue("minZ", this->minZ);
    token = pStructuredFileSection->addToken();
	token->setValue("maxZ", this->maxZ);
    token = pStructuredFileSection->addToken();
	token->setValue("rhoMin", this->rhoMin);
    token = pStructuredFileSection->addToken();
    token->setValue("bandMode", this->getBandMode());
    token = pStructuredFileSection->addToken();
    token->setValue("bppMode", this->getBppMode());
    token = pStructuredFileSection->addToken();
    token->setValue("trafoMode", this->getTrafoMode());
    token = pStructuredFileSection->addToken();
    token->setValue("XYZFileName", this->XYZFileName);
    token = pStructuredFileSection->addToken();
    token->setValue("scaleMode", this->scalemode);
};

void CMatchingParameters::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("patchsize"))
			this->patchsize = token->getIntValue();
		else if(key.CompareNoCase("showMatchWindow"))
			this->showMatchWindow = token->getBoolValue();
		else if(key.CompareNoCase("iterationSteps"))
			this->iterationSteps = token->getIntValue();
		else if(key.CompareNoCase("sigmaGrey8Bit"))
			this->sigmaGrey8Bit = token->getDoubleValue();
		else if(key.CompareNoCase("sigmaGrey16Bit"))
			this->sigmaGrey16Bit = token->getDoubleValue();
		else if(key.CompareNoCase("sigmaGeom"))
			this->sigmaGeom = token->getDoubleValue();
		else if(key.CompareNoCase("minZ"))
			this->minZ = token->getDoubleValue();
		else if(key.CompareNoCase("maxZ"))
			this->maxZ = token->getDoubleValue();
		else if(key.CompareNoCase("rhoMin"))
			this->rhoMin = token->getDoubleValue();
		else if(key.CompareNoCase("bandMode"))
		{
			int typ = token->getIntValue();
			eLSMBANDMODE btyp;
			switch(typ)
			{
				case 0:  btyp = eIDENTICALBANDS; break;
				case 1:  btyp = eNONIDENTICALBANDS; break;
				default: btyp = eNONIDENTICALBANDS; break;
			}
			this->setBandMode(btyp);
		}
		else if(key.CompareNoCase("bppMode"))
		{
			int typ = token->getIntValue();
			eLSMBPPMODE btyp;
			switch(typ)
			{
				case 0:  btyp = eIDENTICALBPPS; break;
				case 1:  btyp = eALLBPPS; break;
				default: btyp = eALLBPPS; break;
			}
			this->setBppMode(btyp);
		}
		else if(key.CompareNoCase("trafoMode"))
		{
			int typ = token->getIntValue();
			eLSMTRAFOMODE btyp;
			switch(typ)
			{
				case 0:  btyp = eLSMNOTFW; break;
				case 1:  btyp = eLSMTFW; break;
				default: btyp = eLSMNOTFW; break;
			}
			this->setTrafoMode(btyp);
		}
		else if (key.CompareNoCase("XYZFileName"))
		{
			this->XYZFileName = token->getValue();
		}
		else if (key.CompareNoCase("scaleMode"))
		{
			int typ = token->getIntValue();
			eLSMSCALEMODE btyp;
			switch(typ)
			{
				case 0:  btyp = eLSMUSESEARCH; break;
				default: btyp = eLSMUSETEMP; break;
			}
			this->setScaleMode(btyp);
		}
	}
};


/**
 * Gets ClassName
 * returns a CCharString
 */
string CMatchingParameters::getClassName() const
{
    return string( "CMatchingParameters");
}

bool CMatchingParameters::isa(string& className) const
{
    if (className == "CMatchingParameters")
        return true;

    return false;
}


/**
 * reconnects pointers
 */
void CMatchingParameters::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CMatchingParameters::resetPointers()
{
    CSerializable::resetPointers();
}


/**
 * Flag if CMatchingParameters is modified
 * returns bool
 */
bool CMatchingParameters::isModified()
{
    return this->bModified;
}

void CMatchingParameters::reset()
{
	this->patchsize = 25;
	this->rhoMin = 0.7;
	this->sigmaGrey8Bit = 5.0;
	this->sigmaGrey16Bit = 40.0;
	this->sigmaGeom = 0.5;
	this->PyrLevel = -1;
	this->iterationSteps = 20;
	this->showMatchWindow = true;
	this->bandmode = eNONIDENTICALBANDS;
	this->bppmode = eALLBPPS;
	this->trafomode = eLSMNOTFW;
	this->XYZFileName = "MATCHED_POINTS";
	this->scalemode = eLSMUSETEMP;
}

CMatchingParameters &CMatchingParameters::operator = (const CMatchingParameters &parameter) 
{
	if (&parameter != this)
	{
		this->patchsize = parameter.patchsize;
		this->rhoMin = parameter.rhoMin;
		this->minZ = parameter.minZ;
		this->maxZ = parameter.maxZ;
		this->sigmaGrey8Bit = parameter.sigmaGrey8Bit;
		this->sigmaGrey16Bit = parameter.sigmaGrey16Bit;
		this->sigmaGeom = parameter.sigmaGeom;
		this->Z = parameter.Z;
		this->PyrLevel = parameter.PyrLevel;
		this->iterationSteps = parameter.iterationSteps;
		this->showMatchWindow = parameter.showMatchWindow;
		this->bandmode = parameter.bandmode;
		this->bppmode = parameter.bppmode;
		this->trafomode = parameter.trafomode;
		this->XYZFileName = parameter.XYZFileName;
		this->scalemode = parameter.scalemode;
	}

	return *this;
};


