
#include "LabelMatcher.h"

#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "filter.h"


CLabelMatcher::CLabelMatcher(float AreaThreshold):
           pLabelsOne(0), pLabelsTwo(0), pCounter(0), pLabelsCombined(0),
		   pMask(0), areaThreshold(AreaThreshold)
{
};

  
//============================================================================

CLabelMatcher::~CLabelMatcher ()
{
	this->clear();
};

//============================================================================

bool CLabelMatcher::init(CLabelImage *Labels1, CLabelImage *Labels2, int tolerance)
{
	bool ret = true;

	this->pLabelsOne = new CLabelImage(*Labels1);
	this->pLabelsTwo = new CLabelImage(*Labels2);

	if ((!this->pLabelsOne) || (!this->pLabelsTwo)) ret = false;
	else if ((this->pLabelsOne->getHeight() != this->pLabelsTwo->getHeight()) || 
		     (this->pLabelsOne->getWidth()  != this->pLabelsTwo->getWidth())) ret = false;
	else
	{
		this->pLabelsCombined = new CLabelImage(this->pLabelsOne->getOffsetX(), this->pLabelsOne->getOffsetY(), 
			                                    this->pLabelsOne->getWidth(),   this->pLabelsOne->getHeight(), 
											    this->pLabelsOne->getNeighbourhood());
		this->pMask = new CImageBuffer(this->pLabelsOne->getOffsetX(), this->pLabelsOne->getOffsetY(), 
			                           this->pLabelsOne->getWidth(),   this->pLabelsOne->getHeight(),1,1,true,0.0);

		if ((!this->pMask) || (!this->pLabelsCombined)) ret = false;
		else
		{
			if (this->listener) 
			{
				this->listener->setTitleString("Initialising data ...");
				this->listener->setProgressValue(0.0);
			}

			OverlapsDatasetOne.setLength(this->pLabelsOne->getNLabels() - 1);
			OverlapsDatasetTwo.setLength(this->pLabelsTwo->getNLabels() - 1);	
			morphologicalOpen(this->pLabelsOne, tolerance);
			morphologicalOpen(this->pLabelsTwo, tolerance);
			
			if (this->listener) 
			{
				this->listener->setProgressValue(100.0);
			}
		}
	}

	if (!ret) this->clear();

	return ret;
};
	  
//============================================================================

void CLabelMatcher::getCombinedLabelImage(CLabelImage &labels) const
{
	labels = *this->pLabelsCombined;
};

//============================================================================

bool CLabelMatcher::initCounter()
{
	if (pCounter) delete pCounter;
	long nLabelSizeOne = this->pLabelsOne->getNLabels() + 1;
	long nLabelSizeTwo = this->pLabelsTwo->getNLabels() + 1;

	pCounter = new CImageBuffer(0,0, nLabelSizeOne, nLabelSizeTwo,1,16,true,0);

	if (!pCounter) return false;
		
	for (unsigned long index = 0; index < this->pLabelsCombined->getNGV(); ++ index)
	{
		unsigned short label1 = this->pLabelsOne->getLabel(index);
		unsigned short label2 = this->pLabelsTwo->getLabel(index);
		if (label1 || label2)
		{
			unsigned long counterIndex = label2 * nLabelSizeOne + label1;
			++(pCounter->pixUShort(counterIndex));	
			this->pMask->pixUChar(index) = 1;
		}
		else this->pMask->pixUChar(index) = 0;
	};

	this->initBackgroundMask(0, 3);

	for (unsigned long index = 0; index < OverlapsDatasetOne.getLength(); ++ index)
	{
		this->distributionOne(index + 1, OverlapsDatasetOne.getRecordByRegion(index + 1));
	};

	for (unsigned long index = 0; index < OverlapsDatasetTwo.getLength(); ++ index)
	{
		this->distributionTwo(index + 1, OverlapsDatasetTwo.getRecordByRegion(index + 1));
	};

	for (int r = 0; r < this->pLabelsTwo->getHeight(); ++r)
	{
		unsigned long uMin = r * this->pLabelsTwo->getDiffY();
		for (int c = 0; c < this->pLabelsTwo->getWidth(); ++c)
		{
			unsigned short label1 = this->pLabelsOne->getLabel(uMin + c);
			unsigned short label2 = this->pLabelsTwo->getLabel(uMin + c);
			if (label1 > 0)
			{
				OverlapsDatasetOne.getRecordByRegion(label1).checkMinMax(r,c);
			}
			if (label2 > 0)
			{
				OverlapsDatasetTwo.getRecordByRegion(label2).checkMinMax(r,c);
			}
		}
	}


	return true;
};


//============================================================================

void CLabelMatcher::distributionOne(unsigned long indexOne,
									CMultipleLabelOverlapRecord &vectorOne)
{
	if (this->pCounter)
	{
		unsigned long u = indexOne;
		unsigned long u1 = 0;
		float area  = float(this->pLabelsOne->getNPixels(indexOne - 1));

		vectorOne.reset(indexOne, area);

		for(;u < pCounter->getNGV(); ++u1, u += pCounter->getWidth())
		{
			unsigned long npixels = pCounter->pixUShort(u);

			if (npixels > 0)
			{
				vectorOne.add(u1, npixels);
			};
		};

		vectorOne.finish();
	} 
	else
	{
		vectorOne.clear();
	};
};

//============================================================================

void CLabelMatcher::distributionTwo(unsigned long indexTwo, 
									CMultipleLabelOverlapRecord &vectorTwo)
{ 
	if (this->pCounter)
	{
		unsigned long u = indexTwo * pCounter->getWidth();
		unsigned long u1 = 0;
		unsigned long max = u + pCounter->getWidth();

		float area  = float(this->pLabelsTwo->getNPixels(indexTwo - 1));

		vectorTwo.reset(indexTwo, area);

		for(;u < max; ++u, ++u1)
		{
			unsigned long npixels = pCounter->pixUShort(u);

			if (npixels > 0)
			{
				vectorTwo.add(u1, npixels);
			};
		};

		vectorTwo.finish();
	} 
	else
	{
		vectorTwo.clear();
	};
};
//============================================================================

void CLabelMatcher::clear()
{
	if (this->pLabelsOne)      delete this->pLabelsOne;
	if (this->pLabelsTwo)      delete this->pLabelsTwo;
	if (this->pCounter)        delete this->pCounter;
	if (this->pLabelsCombined) delete this->pLabelsCombined;
	if (this->pMask)           delete this->pMask;

	this->pLabelsOne      = 0;
	this->pLabelsTwo      = 0;
	this->pCounter        = 0;
	this->pLabelsCombined = 0;
	this->pMask           = 0;

	this->combinedLabel1Vector.clear();
	this->combinedLabel2Vector.clear();
};


//============================================================================

void CLabelMatcher::writeData(CCharString filename) const
{
	if (filename.IsEmpty()) filename = protHandler.getDefaultDirectory() + "OverlapsAll.dat";
	writeData(OverlapsDatasetOne, OverlapsDatasetTwo, "", filename);
};

//============================================================================
 
void CLabelMatcher::writeData(const CLabelOverlapVector &Vector1, 
							  const CLabelOverlapVector &Vector2, 
							  CCharString heading, CCharString filename) const
{
	if (filename.IsEmpty()) protHandler.getDefaultDirectory()  + "OverlapsPercentage.dat";

	ofstream outfile (filename.GetChar());  
	outfile.setf(ios::fixed, ios::floatfield);
	outfile.precision(1);
	CCharString ActualHeading = CCharString("Distribution of labels (one) ") + heading + ":";

	Vector1.writeData(outfile, ActualHeading);

	ActualHeading = CCharString("Distribution of labels (two) ") + heading + ":";

	Vector2.writeData(outfile, ActualHeading); 
};
	  
//============================================================================
 
void CLabelMatcher:: morphologicalOpen(CLabelImage *pLabelImage, int structSize)
{
	CImageBuffer buf1(0,0, pLabelImage->getWidth(), pLabelImage->getHeight(),1,1,true);
	CImageFilter filter(eMorphological, structSize, structSize); 
	CImageBuffer bufCopy(buf1, false);

	for (unsigned long i = 0; i < pLabelImage->getNGV(); ++i)
	{
		if (pLabelImage->getLabel(i)) bufCopy.pixUChar(i) = 1;
		else bufCopy.pixUChar(i) = 0;
	}

	filter.binaryMorphologicalOpen(bufCopy, buf1, 1);

	for (unsigned long i = 0; i < pLabelImage->getNGV(); ++i)
	{
		if (!buf1.pixUChar(i)) pLabelImage->pixUShort(i) = 0;
	}
};
	  
//============================================================================
 
void CLabelMatcher::initBackgroundMask(CLabelImage *labelImg, int tolerance)
{
	CImageBuffer *pMask1 = new CImageBuffer(*this->pMask,  false);
	
	if (labelImg)
	{
		for (unsigned long index = 0; index < labelImg->getNGV(); ++ index)
		{
			unsigned short label = labelImg->getLabel(index);
			pMask1->pixUChar(index) = (label == 0) ?  1 : 0;
		};
	}
	else
	{
		for (unsigned long index = 0; index < this->pMask->getNGV(); ++ index)
		{
			pMask1->pixUChar(index) = !this->pMask->pixUChar(index);
		};
	}

	pMask1->setMargin(1,0);
	CImageFilter filter(eMorphological, tolerance, tolerance);
	filter.binaryMorphologicalOpen(*pMask1, *this->pMask, 1.0);
	delete pMask1;

	this->pMask->setMargin(tolerance, 0.0);

	CLabelImage *backG = new CLabelImage(*this->pMask, 1.0, 8);
	backG->computePixelNumbers();

	unsigned short maxLabel = 1;
	unsigned int maxPix = 0;
	for (unsigned int i = 1; i < backG->getNLabels(); ++i)
	{
		if (backG->getNPixels(i) > maxPix)
		{
			maxLabel = i;
			maxPix = backG->getNPixels(i);
		}
	}

	for (unsigned long index = 0; index < this->pMask->getNGV(); ++ index)
	{
		this->pMask->pixUChar(index) = (backG->getLabel(index) == maxLabel) ? 0 : 255;
	};

	this->pMask->setMargin(tolerance + 1,0);
	delete backG;

	if (outputMode >= eMAXIMUM)
	{
		CCharString f = protHandler.getDefaultDirectory() + "background.tif";
		this->pMask->exportToTiffFile(f.GetChar(),0,false);
	}
};
  
//============================================================================
 
void CLabelMatcher::openCombinedLabelImage(int structSize)
{
	CImageBuffer buf1(0,0, this->pLabelsCombined->getWidth(), this->pLabelsCombined->getHeight(),1,1,true);
	CImageFilter filter(eMorphological, structSize, structSize); 
	CImageBuffer bufCopy(buf1, false);

	for (unsigned long i = 0; i < this->pLabelsCombined->getNGV(); ++i)
	{
		if (this->pLabelsCombined->getLabel(i)) bufCopy.pixUChar(i) = 1;
		else bufCopy.pixUChar(i) = 0;
	}

	filter.binaryMorphologicalOpen(bufCopy, buf1, 1);

	this->pLabelsCombined->initFromMask(buf1,1.0);
	this->pLabelsCombined->updateVoronoi();
	this->combinedLabel1Vector.resize(this->pLabelsCombined->getNLabels(), 0);
	this->combinedLabel2Vector.resize(this->pLabelsCombined->getNLabels(), 0);

	for (unsigned long i = 0; i < this->pLabelsCombined->getNGV(); ++i)
	{
		unsigned short label = this->pLabelsCombined->getLabel(i);
		if (label)
		{
			unsigned short label1 = this->pLabelsOne->getLabel(i);
			unsigned short label2 = this->pLabelsTwo->getLabel(i);
			combinedLabel1Vector[label] = label1;
			combinedLabel2Vector[label] = label2;
		}
	}
};
	  
//============================================================================
 
void CLabelMatcher::compare(float noneVsWeak, float weakVsPartial, 
							float partialVsStrong, int tolerance)
{
	CCharString fileBasis = protHandler.getDefaultDirectory() + CSystemUtilities::dirDelimStr + "Overlap_";

	if (outputMode >= eMAXIMUM)
	{
		this->pLabelsOne->dump(protHandler.getDefaultDirectory().GetChar(), "One","",true);
		this->pLabelsTwo->dump(protHandler.getDefaultDirectory().GetChar(), "Two","",true);
	}

	this->OverlapsDatasetOne.classifyPart(noneVsWeak, weakVsPartial, partialVsStrong);
	this->OverlapsDatasetOne.classifyTotal(noneVsWeak, weakVsPartial, partialVsStrong);
	this->OverlapsDatasetTwo.classifyPart(noneVsWeak, weakVsPartial, partialVsStrong);
	this->OverlapsDatasetTwo.classifyTotal(noneVsWeak, weakVsPartial, partialVsStrong);

	this->pCounter->setAll(0);

	int newLabel = 0;
	
	CImageBuffer mask (*this->pMask, false);
	mask.setAll(0);

	for (unsigned int i = 0; i < this->pLabelsCombined->getNGV(); ++i)
	{
		unsigned short label1 = this->pLabelsOne->getLabel(i);
		unsigned short label2 = this->pLabelsTwo->getLabel(i);
		if (label1 ||label2)
		{
			if (label1 && label2)
			{
				unsigned long counterIndex = label2 * pCounter->getDiffY() + label1;
				int label = pCounter->pixUShort(counterIndex);
				if (!label)
				{
					++newLabel;
					label = newLabel;
					pCounter->pixUShort(counterIndex) = label;
				}

				this->pLabelsCombined->pixUShort(i) = label;
			}
			else 
			{
				mask.pixUChar(i) = 255;
			}
		}
	};

	CImageFilter filter(eMorphological, 10, 10); 
	CImageBuffer bufCopy(mask, true);
	filter.binaryMorphologicalOpen(bufCopy,mask,1.0);

	unsigned short firstNew = newLabel;

	for (unsigned int i = 0; i < this->pLabelsCombined->getNGV(); ++i)
	{
		if (mask.pixUChar(i))
		{
			unsigned short label1 = this->pLabelsOne->getLabel(i);
			unsigned short label2 = this->pLabelsTwo->getLabel(i);
			unsigned long counterIndex = label2 * pCounter->getDiffY() + label1;
			int label = pCounter->pixUShort(counterIndex);
			if (!label)
			{
				++newLabel;
				label = newLabel;
				pCounter->pixUShort(counterIndex) = label;
			}

			this->pLabelsCombined->pixUShort(i) = label;
		}
	};


	this->pLabelsCombined->setBoundariesToZero(false, true, true, 0);
}

//============================================================================
 
void CLabelMatcher::determineOverlaps(float noneVsWeak, float weakVsPartial, 
									  float partialVsStrong,  int tolerance)
{
	if (this->listener) 
	{
		this->listener->setTitleString("Determining Overlaps ...");
		this->listener->setProgressValue(0.0);
	}

	CCharString fileBasis = protHandler.getDefaultDirectory() + CSystemUtilities::dirDelimStr + "Overlap_";

	this->initCounter();

	this->compare(noneVsWeak, weakVsPartial, partialVsStrong, tolerance);

	this->openCombinedLabelImage(tolerance);

	if (outputMode >= eMAXIMUM)
	{
		this->pLabelsCombined->dump(protHandler.getDefaultDirectory().GetChar(), "","Combined_before", true, true);
	}
	
	this->initBackgroundMask(this->pLabelsCombined, 2 * tolerance);

	this->pLabelsCombined->makeGapsSmallByVoronoi(2 * tolerance, false, true, true);

	for (unsigned long i = 0; i < this->pLabelsCombined->getNGV(); ++i)
	{
		if (!this->pMask->pixUChar(i)) this->pLabelsCombined->pixUShort(i) = 0;
	}


	if (outputMode >= eMAXIMUM)
	{
		this->writeCorrespondences(fileBasis + "correspondences.txt");
		this->pLabelsCombined->dump(protHandler.getDefaultDirectory().GetChar(), "","Combined", true);
	}

	if (this->listener) 
	{
		this->listener->setProgressValue(100.0);
	}
};

//============================================================================
  
void CLabelMatcher::writeLabelsCombined() const
{
	this->pLabelsCombined->dump(protHandler.getDefaultDirectory(), "", "_combined");
};

//============================================================================
  
void CLabelMatcher::writeCorrespondences(const CCharString &filename) const
{
	ofstream f(filename.GetChar());
	if (f.good())
	{
		for (unsigned int i = 1; i < this->combinedLabel1Vector.size(); ++i)
		{
			f << "Label "; 
			f.width(8);
			f << i << " corresponds to ";
			f.width(8);
		    f << combinedLabel1Vector[i] << " in image 1 and to ";
			f.width(8);
		    f << combinedLabel2Vector[i] << " in image 2\n" ;
		}
	}
};

//============================================================================
  
void CLabelMatcher::writeTIFFOne(CLabelOverlapVector &record, 
                                            CCharString filename) const
{
	CImageBuffer localBuf(0, 0, this->getWidth(), this->getHeight(), 1, 8, true, 0.0);

	for (unsigned long index = 0; index < localBuf.getNGV(); ++index)
	{
		unsigned short newLabel = this->pLabelsOne->getLabel(index);
		unsigned short oldLabel = this->pLabelsTwo->getLabel(index);
		if (oldLabel)
		{
			if (!newLabel) localBuf.pixUChar(index) = 255;
			else if (record.getRecordByRegion(oldLabel).nOverlaps() == 0)
				localBuf.pixUChar(index) = 254;
			else localBuf.pixUChar(index) = (unsigned char) record.getRecordByRegion(oldLabel).nOverlaps();
		};
	};

	CLookupTable lut = splitMergeOverlapLUT();

	localBuf.exportToTiffFile(filename.GetChar(),&lut,false);

	for (unsigned long index = 0; index < localBuf.getNGV(); ++index)
	{
		unsigned short newLabel = this->pLabelsOne->getLabel(index);
		unsigned short oldLabel = this->pLabelsTwo->getLabel(index);
		if (oldLabel)
		{
			eOVERLAP eClass = record.getRecordByRegion(oldLabel).getClassification();
			switch ( eClass)
			{
				case eSTRONG:  localBuf.pixUChar(index) = 4; break;
				case ePARTIAL: localBuf.pixUChar(index) = 3; break;
				case eWEAK:    localBuf.pixUChar(index) = 2; break;
				case eNONE:    localBuf.pixUChar(index) = 1; break;
				default:       localBuf.pixUChar(index) = 0; break;
			};
		}
		else localBuf.pixUChar(index) = 0;
	};

	CFileName file(filename);
	file.SetFileName(file.GetFileName() + "_Class");
	localBuf.exportToTiffFile(file.GetChar(),&lut,false);
};
 
//============================================================================
  
void CLabelMatcher::writeTIFFTwo(CLabelOverlapVector &record, 
                                            CCharString filename) const
{
	CImageBuffer localBuf(0, 0, this->getWidth(), this->getHeight(), 1, 8, true, 0.0);

	for (unsigned long index = 0; index < localBuf.getNGV(); ++index)
	{
		unsigned short newLabel = this->pLabelsOne->getLabel(index);
		unsigned short oldLabel = this->pLabelsTwo->getLabel(index);
		if ((newLabel) && (newLabel < record.getLength()))
		{
			if (!oldLabel) localBuf.pixUChar(index) = 255;
			else if (record.getRecordByRegion(newLabel).nOverlaps() == 0)
				localBuf.pixUChar(index)= 254;   
			else localBuf.pixUChar(index) = (unsigned char) record.getRecordByRegion(newLabel).nOverlaps();
		};
	};

	CLookupTable lut = splitMergeOverlapLUT();

	localBuf.exportToTiffFile(filename.GetChar(),&lut,false);

	for (unsigned long index = 0; index < localBuf.getNGV(); ++index)
	{
		unsigned short newLabel = this->pLabelsOne->getLabel(index);
		unsigned short oldLabel = this->pLabelsTwo->getLabel(index);
		if ((newLabel) && (newLabel < record.getLength()))
		{
			eOVERLAP eClass = record.getRecordByRegion(newLabel).getClassification();
			switch ( eClass)
			{
				case eSTRONG:  localBuf.pixUChar(index) = 4; break;
				case ePARTIAL: localBuf.pixUChar(index) = 3; break;
				case eWEAK:    localBuf.pixUChar(index) = 2; break;
				case eNONE:    localBuf.pixUChar(index) = 1; break;
				default:       localBuf.pixUChar(index) = 0; break;
			};
		}
		else localBuf.pixUChar(index) = 0;
	};

	CFileName file(filename);
	file.SetFileName(file.GetFileName() + "_Class");
	localBuf.exportToTiffFile(file.GetChar(),&lut,false);
};

//============================================================================
  
CLookupTable CLabelMatcher::splitMergeOverlapLUT()
{
	CLookupTable lut(8, 3);
	lut.setColor(255, 255, 255, 0);
	lut.setColor(  0,   0, 255, 1);
	lut.setColor(  0, 255,   0, 2);
	lut.setColor(255,   0,   0, 3);
	lut.setColor(255, 255,   0, 4);
	lut.setColor(  0, 255, 255, 5);
	lut.setColor(255,   0, 255, 6);

	for (int i = 7; i < lut.getMaxEntries(); ++i)
	{
		lut.setColor(255,   0, 255, i);
	};

	lut.setColor(128,   0,   0, 254);
	lut.setColor(  0,   0,   0, 255);

	return lut;
};

//============================================================================
  