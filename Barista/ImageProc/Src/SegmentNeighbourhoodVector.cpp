#include "SegmentNeighbourhoodVector.h"
#include "m_FeatExtract.h"
#include "ProtocolHandler.h"

CSegmentNeighbourhoodVector::CSegmentNeighbourhoodVector()
{
};

//============================================================================
	
CSegmentNeighbourhoodVector::CSegmentNeighbourhoodVector(const CSegmentNeighbourhoodVector &segmentVector):CSortVector<CSegmentNeighbourhood>(segmentVector)
{
}

//============================================================================

CSegmentNeighbourhoodVector::~CSegmentNeighbourhoodVector()
{
};

//============================================================================
 
void CSegmentNeighbourhoodVector::addSegment(const CSegmentNeighbourhood &segment)
{
	this->push_back(segment);
};

//============================================================================
 
void CSegmentNeighbourhoodVector::addSegment(unsigned int Index, CImageRegion *pR1, CImageRegion *pR2)
{
	CSegmentNeighbourhood neigh(Index, pR1, pR2);
	this->addSegment(neigh);
};

//============================================================================
 
void CSegmentNeighbourhoodVector::removeSegmentAt(unsigned int i)
{
	if ((this->size() > 0) && ((int)i < this->size()))
	{
		for (int u = i + 1; u < this->size(); ++u)
		{
			(*this)[u - 1] = (*this)[u];
		}
		this->pop_back();
	}
};

//============================================================================
 
void CSegmentNeighbourhoodVector::mergeSegments(unsigned int i, CLabelImage &labels, 
												int *boundary, CSegmentAMatrix &matrix, 
												const CImageBuffer &img,vector<CImageRegion *> &regionVec, bool purge)
{
	CImageRegion *pR1 = (*this)[i].getRegion1();
	CImageRegion *pR2 = (*this)[i].getRegion2();
	unsigned short region1 = (*this)[i].getRegionIndex1();
	unsigned short region2 = (*this)[i].getRegionIndex2();
	
//update of regionVec
	CSortVector<unsigned short> NeighIdxOfRegion1, NeighIdxOfRegion2, MutalNeighs;

	NeighIdxOfRegion1 = FindNeighbours(region1);
	NeighIdxOfRegion2 = FindNeighbours(region2);
	MutalNeighs = MutalNeighbours(NeighIdxOfRegion1, NeighIdxOfRegion2);

	//reduce of NNeigh by mutal neighbours
	for (int j = 0; j < MutalNeighs.size(); j++) regionVec[MutalNeighs[j]]->addNNeigh(- 1);

	//reduce of NNeigh for region1
	int newNNeighRegion1 = (NeighIdxOfRegion1.size()-1) + (NeighIdxOfRegion2.size()-1) - MutalNeighs.size();
	regionVec[region1]->setNNeigh(newNNeighRegion1);

	//reduce of NNeigh for region2
	regionVec[region2]->setNNeigh(0);

//update adjacency matrix, border image, label image
	(*this)[i].merge(labels, boundary,matrix, img);
}

//============================================================================
 
void CSegmentNeighbourhoodVector::purgeSegments(unsigned int i, CSegmentAMatrix &matrix)
{
	unsigned short region1 = (*this)[i].getRegionIndex1();
	unsigned short region2 = (*this)[i].getRegionIndex2();

	//Vector with all regions which are to be deleted 
	CSortVector<unsigned long> RegionsToBeDelete_help;

	RegionsToBeDelete_help.push_back(region1);
	RegionsToBeDelete_help.push_back(region2);

	for (int j = 0; j < this->size(); ++j)
	{
		if ((*this)[j].getRegionIndex1() == region1 || 
			(*this)[j].getRegionIndex1() == region2)
		{	
			RegionsToBeDelete_help.push_back((*this)[j].getRegionIndex2());
		}
		else if ((*this)[j].getRegionIndex2() == region1 || 
			    (*this)[j].getRegionIndex2() == region2)

		{
			RegionsToBeDelete_help.push_back((*this)[j].getRegionIndex1());
		}
	}

	RegionsToBeDelete_help.sortAscending();

	//cut the RegionsToBeDelete_help Vector
	vector<unsigned int>  RegionsToBeDelete;

	int lastSeg = 0;
	for (int j = 0; j < RegionsToBeDelete_help.size(); j++)
	{
		if (RegionsToBeDelete_help[j] > (unsigned long) lastSeg)
		{
			lastSeg = RegionsToBeDelete_help[j];
			RegionsToBeDelete.push_back(RegionsToBeDelete_help[j]);
			}
	}

	//delete the elements
	for (unsigned int j = 0; j < RegionsToBeDelete.size(); j++)
	{
		int currIdx = 0; 
		unsigned int Region = RegionsToBeDelete[j];

		for (int k = 0; k < this->size(); ++k)
		{
			if ((*this)[k].getRegionIndex1() != Region && 
			    (*this)[k].getRegionIndex2() != Region)
			{			
				(*this)[currIdx] = (*this)[k];
				++currIdx;
			}
		}
		this->resize(currIdx);
	}	
};

//============================================================================
 
void CSegmentNeighbourhoodVector::pickSegmentRegion2(unsigned int i, CSegmentAMatrix &matrix)
{
	unsigned short region2 = (*this)[i].getRegionIndex2();
	int currIdx = 0; 


	for (int j = 0; j < this->size(); ++j)
	{
		if ((*this)[j].getRegionIndex1() == region2 || 
			(*this)[j].getRegionIndex2() == region2)
		{			
			(*this)[currIdx] = (*this)[j];
			++currIdx;
		}
	}
	this->resize(currIdx);
};
//============================================================================
 
void CSegmentNeighbourhoodVector::pickSegmentRegion1(unsigned int i, CSegmentAMatrix &matrix)
{
	unsigned short region1 = (*this)[i].getRegionIndex1();
	int currIdx = 0; 


	for (int j = 0; j < this->size(); ++j)
	{
		if ((*this)[j].getRegionIndex1() == region1 || 
			(*this)[j].getRegionIndex2() == region1)
		{			
			(*this)[currIdx] = (*this)[j];
			++currIdx;
		}
	}
	this->resize(currIdx);
};
//============================================================================
 
void CSegmentNeighbourhoodVector::update(CSegmentNeighbourhood &neigh, 
										 CSegmentAMatrix &matrix, 
										 int *boundary, const CImageBuffer &texture)
{
	unsigned short region1 = neigh.getRegionIndex1();
	unsigned short region2 = neigh.getRegionIndex2();

	for(int i=0; i< this->size();)
	{
		//delete the element of merged regions
		if((*this)[i].getRegionIndex1() == region1 && (*this)[i].getRegionIndex2() == region2) (*this).removeSegmentAt(i);
		
		else
		{
			//replace index of the region2 through index of the new region
			
			if((*this)[i].getRegionIndex1() == region2)
			{
				unsigned int index = matrix.LabelsToIndex(region1, (*this)[i].getRegionIndex2());
					
				if (region1 < (*this)[i].getRegionIndex2()) (*this).addSegment(index, neigh.getRegion1(), (*this)[i].getRegion2());
				else if (region1 > (*this)[i].getRegionIndex2()) (*this).addSegment(index, (*this)[i].getRegion2(), neigh.getRegion1());
				
				(*this)[this->size() - 1].initTexture(boundary, texture);
				(*this).removeSegmentAt(i);
			}	
			else if((*this)[i].getRegionIndex2() == region2)
			{
				unsigned int index = matrix.LabelsToIndex((*this)[i].getRegionIndex1(), region1);
			
				if (region1 <  (*this)[i].getRegionIndex1())(*this).addSegment(index, neigh.getRegion1(), (*this)[i].getRegion1());
				else if (region1 >  (*this)[i].getRegionIndex1()) (*this).addSegment(index, (*this)[i].getRegion1(), neigh.getRegion1());
				
				(*this)[this->size() - 1].initTexture(boundary, texture);
				(*this).removeSegmentAt(i);
				//(*this)[i].initDistance();
			}	
			else i++;
		}
	}

	//sort vector
	(*this).sortAscending();


	//delete double pairs 
	(*this).purgeIndex();
}

//============================================================================

void CSegmentNeighbourhoodVector::calculate(vector<CImageRegion *> &regionVec, CSegmentAMatrix &matrix, unsigned int dimension)
{
	unsigned int row_help = 0, col_help=0, index_help = 0;

	for (row_help = 1; row_help < dimension-2; row_help++) //check matrix row by row
	{	
		for (col_help = row_help+1; col_help < dimension-1; col_help++) //check every element in one row
		{
			index_help = matrix.LabelsToIndex(row_help, col_help);
			if (matrix[index_help]) (*this).addSegment(index_help,regionVec[row_help], regionVec[col_help]);
		}
	}
}


//============================================================================

void CSegmentNeighbourhoodVector::dump(const CFileName &filename) const
{
	ofstream outFile(filename.GetChar());
	if (outFile.good())
	{
		for (int i = 0; i < this->size(); ++i)
		{
			(*this)[i].dump(outFile);
		}
	}
};

//============================================================================

void CSegmentNeighbourhoodVector::purgeIndex()
{
	for (int i=0; i< (this->size()-1); i++)
	{
		for (int j=i+1; j< this->size(); j++)
		{
			if ((*this)[i].getIndex() == (*this)[j].getIndex()) (*this).removeSegmentAt(j);
		}


	}
};

//============================================================================

void CSegmentNeighbourhoodVector::CalculateNNeigh(vector<CImageRegion *> &regionVec)
{
	for (int j = 0; j < this->size(); ++j)
	{
		regionVec[(*this)[j].getRegionIndex1()]->addNNeigh(1);
		regionVec[(*this)[j].getRegionIndex2()]->addNNeigh(1);
	}
};

//============================================================================

CSortVector<unsigned short> CSegmentNeighbourhoodVector::FindNeighbours(int region)
{
	CSortVector<unsigned short> NeighIdxsOfRegion;

	for (int j = 0; j < this->size(); ++j)
	{
		if ((*this)[j].getRegionIndex1() == region || (*this)[j].getRegionIndex2() == region) 
		{
			if ((*this)[j].getRegionIndex1() == region) NeighIdxsOfRegion.push_back((*this)[j].getRegionIndex2());
			else NeighIdxsOfRegion.push_back((*this)[j].getRegionIndex1());
		}
	}

	return NeighIdxsOfRegion;
};

//============================================================================


CSortVector<unsigned short> CSegmentNeighbourhoodVector::MutalNeighbours(CSortVector<unsigned short> vec1, CSortVector<unsigned short> vec2)
{
	CSortVector<unsigned short> mutal;

	for (int k = 0; k < vec1.size(); k++)
	{
		for (int l = 0; l < vec2.size(); l++)
		{
			if (vec1[k] == vec2[l]) mutal.push_back(vec2[l]);
		}
	}

	return mutal;
};

//============================================================================
