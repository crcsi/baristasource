#include <float.h>
#include "LabelImage.h"
#include "m_FeatExtract.h"
#include "ProtocolHandler.h"
#include "HoughTransformation.h"


CHoughTransformation::CHoughTransformation(): rho_min (0), rho_max(0), delta_rho(1.0), delta_phi (1.0), puffer(0)
{
};

//============================================================================

CHoughTransformation::CHoughTransformation(float RhoMin, float RhoMax, float DeltaRho, float DeltaPhi): 
	 rho_min (RhoMin), rho_max(RhoMax), delta_rho(DeltaRho), delta_phi(DeltaPhi)
{
	CImageBuffer puffer(0,0,long (delta_rho)*360, long (rho_max - rho_min),1,1,true,0);
};

//============================================================================

CHoughTransformation::~CHoughTransformation()
{
	puffer = 0;
};