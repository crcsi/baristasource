//============================================================================
//
// File        : CMultipleLabelOverlapRecord.cpp
//
// Author      : FR
//
// Description : Implementation of class CCMultipleLabelOverlapRecord
//
//============================================================================
#include <climits>

#include <strstream>
using namespace std;

#include "MultipleLabelOverlapRecord.h"


CMultipleLabelOverlapRecord::CMultipleLabelOverlapRecord(): index(0), 
							 nPixels(0), totalCoverage(0.0f), 
							 eClassification(eNONE), area(0.0f),
							 rmin(INT_MAX), cmin(INT_MAX),
							 rmax(-1), cmax(-1)
{
};

//============================================================================

CMultipleLabelOverlapRecord::CMultipleLabelOverlapRecord(unsigned long Index):
							 index(Index), nPixels(0), totalCoverage(0.0f), 
						     eClassification(eNONE), area(0.0f),
							 rmin(INT_MAX), cmin(INT_MAX),
							 rmax(-1), cmax(-1)
{
};

//============================================================================

CMultipleLabelOverlapRecord::CMultipleLabelOverlapRecord(const CMultipleLabelOverlapRecord &record):
        index(record.index), nPixels(record.nPixels), 
		rmin(record.rmin), cmin(record.cmin), rmax(record.rmax), cmax(record.cmax),
        overlapRegions(record.overlapRegions), overlapBackground(record.overlapBackground), 
        totalCoverage(record.totalCoverage),   eClassification(record.eClassification),
        area(record.area)
{
};

//============================================================================

int CMultipleLabelOverlapRecord::getOverlapIndicator(unsigned long region, 
													 CLabelOverlapIndicator &Indicator) const
{
	for (unsigned long u = 0; u < nOverlaps(); ++u)
	{
		if (overlapRegions[u].getIndex() == region)
		{
			Indicator = overlapRegions[u];
			return u;
		};
	};

	return -1;
};

//============================================================================

CCharString CMultipleLabelOverlapRecord::getString(bool showNone) const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(1);
	oss << "\nRegion ";
	oss.width(5); 
	oss << index << "( ";
	oss.width(7);
	oss << area << " m^2 / ";
	oss.width(5);
	oss << totalCoverage * 100.0f << " % / ";
	oss.width(7);
	switch (eClassification)
	{
		case eWEAK:    oss<< "weak";    break;
		case ePARTIAL: oss<< "partial"; break;
		case eSTRONG:  oss<< "strong";  break;
		default:       oss<< "none";    break;
	};

	oss<< ") connects to ";
	if (nOverlaps() < 1) oss << "no region.";
	else if (nOverlaps() == 1) oss <<"region : ";
	else oss <<"regions: ";
	 
	oss << ends;

	char *zstr = oss.str();

	CCharString Str (zstr);
	delete[] zstr;

	for (unsigned long u = 0; u < nOverlaps(); ++u)
	{
		Str += overlapRegions[u].getString(showNone);
	};

	if (nOverlaps() > 0) Str += overlapBackground.getString(showNone);
	return Str;
};

//============================================================================

void CMultipleLabelOverlapRecord::clear()
{
	index = 0;
	nPixels = 0;
	this->overlapRegions.clear();
	overlapBackground.init(0,0);
	totalCoverage = 0;
	area = 0.0f;
	eClassification = eNONE;
	rmin = INT_MAX;
	cmin = INT_MAX;
	rmax = -1;
	cmax = -1;
};

//============================================================================

void CMultipleLabelOverlapRecord::reset(unsigned long Index, float Area)
{
	index = Index;
	area = Area;
	nPixels = 0;
	this->overlapRegions.clear();
	overlapBackground.init(0,0);
	totalCoverage = 0;
	eClassification = eNONE;
 
	rmin = INT_MAX;
	cmin = INT_MAX;
	rmax = -1;
	cmax = -1;
};

//============================================================================

void CMultipleLabelOverlapRecord::add(unsigned long Index, unsigned long NPixels)
{
	if (Index == 0)
	{
		overlapBackground.init(0, NPixels);
	}
	else
	{
		CLabelOverlapIndicator indicator;
		indicator.init(Index, NPixels);
		overlapRegions.push_back(indicator);
	};
	nPixels += NPixels;
};
	  
void CMultipleLabelOverlapRecord::checkMinMax(int r, int c)
{
	if (r < rmin) rmin = r;
	if (r > rmax) rmax = r;
	if (c < cmin) cmin = c;
	if (c > cmax) cmax = c;
};

//============================================================================

void CMultipleLabelOverlapRecord::computeTotalOverlap()
{
	totalCoverage = 0;
	for (unsigned long u = 0; u < nOverlaps(); ++u)
	{
		totalCoverage += overlapRegions[u].getPercentage();
	};
}

//============================================================================

void CMultipleLabelOverlapRecord::finish()
{
	overlapBackground.finish (nPixels);

	for (unsigned long u = 0; u < nOverlaps(); ++u)
	{
		overlapRegions[u].finish (nPixels);
	};

	computeTotalOverlap();
};

//============================================================================

void CMultipleLabelOverlapRecord::classify(float noneVsWeak, float weakVsPartial, 
										   float partialVsStrong)
{
	overlapBackground.classify(noneVsWeak, weakVsPartial, partialVsStrong);

	for (unsigned long u = 0; u < nOverlaps(); ++u)
	{
		overlapRegions[u].classify(noneVsWeak, weakVsPartial, partialVsStrong);
	};
};

//============================================================================

void CMultipleLabelOverlapRecord::classifyTotal(float noneVsWeak, 
												float weakVsPartial, 
												float partialVsStrong)
{
	if (totalCoverage < noneVsWeak) eClassification = eNONE;
	else if (totalCoverage < weakVsPartial) eClassification = eWEAK;
	else if (totalCoverage < partialVsStrong) eClassification = ePARTIAL;
	 else eClassification = eSTRONG;
};

//============================================================================

int CMultipleLabelOverlapRecord::operator <  (const CMultipleLabelOverlapRecord &record) const
{
	return getTotalCoverage() < record.getTotalCoverage();
};

//============================================================================

int CMultipleLabelOverlapRecord::operator >  (const CMultipleLabelOverlapRecord &record) const
{
	return getTotalCoverage() > record.getTotalCoverage();
};

//============================================================================

void CMultipleLabelOverlapRecord::sort()
{
	if (nOverlaps() > 1)
	{
		int maxI = nOverlaps() - 1;
		float max = overlapRegions[ maxI ].getPercentage();
		for ( int i = maxI - 1; i >= 0; --i )
		if ( overlapRegions[ i ].getPercentage() > max ) 
		{ 
			maxI = i; 
			max = overlapRegions[ maxI  ].getPercentage(); 
		};

		swap(maxI , nOverlaps()- 1 );
		sortPart(0, nOverlaps() - 2 );
		reverseOrder();
	};
};
  
//============================================================================

void CMultipleLabelOverlapRecord::reverseOrder()
{
	unsigned long min, max;

	for (min = 0, max = nOverlaps() - 1; min < max; ++min,--max)
	{
		swap(min, max);
	};
};

//============================================================================

void CMultipleLabelOverlapRecord::removeElement(unsigned long Index)
{
	if (nOverlaps())
	{
		totalCoverage -= overlapRegions[Index].getPercentage();

		for (unsigned long u = Index + 1; u < nOverlaps(); ++u)
		{
			overlapRegions[u - 1] = overlapRegions[u];
		};  
		overlapRegions.pop_back();
	};
};

//============================================================================

void CMultipleLabelOverlapRecord::sortPart(int low, int high)
{
	if ( low < high )   // stopping condition for recursion!
	{
		int lo = low;
		int hi = high + 1;

		float elem = overlapRegions[ low ].getPercentage();

		for ( ; ; )
		{
			while ( overlapRegions[ ++lo ].getPercentage() < elem );
			while ( overlapRegions[ --hi ].getPercentage() > elem );
			if ( lo < hi ) swap(lo, hi);
			 else break; 
		};

		sortPart(low,    hi - 1 );
		sortPart(hi + 1, high );
	}
};

//============================================================================
  
void CMultipleLabelOverlapRecord::swap(int i, int j)
{
	CLabelOverlapIndicator Ind = overlapRegions[i];
	overlapRegions[i] = overlapRegions[j];
	overlapRegions[j] = Ind;
};

//============================================================================

//****************************************************************************
//
// E n d   o f   c l a s s   P H _ m u l t i p l e O v e r l a p R e c o r d
//
//****************************************************************************
