#include <memory.h>
#include <math.h>
#include "ImageGraph.h"
#include "sortVector.h"
#include "XYEllipse.h"
//#include "BestFit2DEllipse.h"
//#include "LeastSquaresSolver.h"
#include "utilities.h"

AIM_Vertex::AIM_Vertex() : active(false), ord(0)
{
}
AIM_Vertex::AIM_Vertex(const AIM_Vertex &vertex) : active(vertex.active), 
					ord(vertex.ord), x(vertex.x), y(vertex.y),
					type(vertex.type)
{
	for (int i = 0; i < MAX_NEIGHBORS; ++i)
		c[i] = vertex.c[i];
}

int AIM_Vertex::getPositionOfContour(long contour)
{ 
	for(long i=0; i<this->ord;i++)
	{
		if (this->c[i].c_no == contour) return i;
	}
	return -1;
}

void AIM_Vertex::addNeighbour(long contour)
{
	if (this->active)
	{
		int pos = this->getPositionOfContour(contour);	
		if (pos >= 0) (this->c[pos].mul)++;
		else
		{ 
			this->c[this->ord].c_no = contour;
			this->c[this->ord].mul  = 1;
			(this->ord)++;
		}
	}
}

void AIM_Vertex::removeNeighbour(long contour, bool removeBoth)
{
	if (this->active)
	{ 
		long pos = this->getPositionOfContour(contour);
		if (pos >= 0)
		{
			if (removeBoth ||(this->c[pos].mul == 1))
			{
				for (long i=pos;i<(this->ord-1);i++)
					this->c[i] = this->c[i+1];
				(this->ord)--;
			}
			else (this->c[pos].mul)--;
		}
	}
}

AIM_Contour::AIM_Contour() : chain(0), attr(0), active(false), p_init(0), p_alloc(0)
{
	v_no[0] = 0;
	v_no[1] = 0;
}

AIM_Contour::AIM_Contour(const AIM_Contour &contour):
			active(contour.active), p_init(contour.p_init), p_alloc(contour.p_alloc),
			len(contour.len), attr(0)
{
	this->v_no[0] = contour.v_no[0];
	this->v_no[1] = contour.v_no[1];

	this->chain = new AIM_Pixel [contour.p_alloc];
    if (this->chain == 0) 
	{
		p_alloc = 0;
		len = 0;
	}
	else
	{
		memcpy(this->chain, contour.chain, this->p_alloc*sizeof(AIM_Pixel));
	};

	if (contour.attr)
	{
		attr = new C_attributes;
		*attr  = *contour.attr;
	};
};

AIM_Contour::~AIM_Contour()
{
	deleteAttributes();
	if (chain) delete [] chain;
	chain = 0;
	v_no[0] = 0;
	v_no[1] = 0;
}

void AIM_Contour::deleteAttributes()
{
	if (attr) delete attr;
	attr = 0;
};

void AIM_Contour::initAttributes(bool overwrite)
{
	if (!attr) attr = new C_attributes;
	if (overwrite) 
	{
		attr->pdir[0]  = attr->pdir[1] = 0;
		attr->strength = 0.0f;
		attr->length   = 0.0f;
		attr->type     = 0;
		attr->shape.area  = 0.0f;
		attr->shape.curv  = 0.0f;
		attr->shape.mdist = 0.0f;
		attr->shape.poly  = 0;
		attr->shape.type  = 0;
		attr->lineEndPoints[0].x = attr->lineEndPoints[0].y = 0.0;
		attr->lineEndPoints[1].x = attr->lineEndPoints[1].y = 0.0;
	}
};
	   
void AIM_Contour::cutOffChainAtEnd(unsigned long position)
{
	if (position < this->len) this->len = position;
};

	   
void AIM_Contour::cutOffChainAtBeginning(unsigned long position)
{
	for (unsigned long j = position; j < len; j++) 	  
		this->chain[j-position] = this->chain[j];

	this->len -= position;
};
	   
void AIM_Contour::setChain(AIM_Pixel *I_chain, unsigned long newLen, unsigned long newAlloc)
{
	this->chain = I_chain;
	this->len = newLen;
	this->p_alloc = newAlloc;
};

void AIM_Contour::appendChain(const AIM_Contour &contour)
{
	unsigned long newLen = this->len + contour.len - 1;

	if (newLen > this->p_alloc)
	{
		unsigned long newAlloc = newLen + this->p_init;

		AIM_Pixel *tmp = new AIM_Pixel[newAlloc];
		memcpy(tmp,chain,this->p_alloc * sizeof(AIM_Pixel));
		// handle and update string 
		deleteChain();
		p_alloc = newAlloc;
		chain = tmp;
	};
	
	for (unsigned long i = 1; i < contour.len;i++) 
		chain[i + len - 1] = contour.chain[i];

	len = newLen;
};
   
void AIM_Contour::appendChainReverted(const AIM_Contour &contour)
{
	unsigned long newLen = this->len + contour.len - 1;

	if (newLen > this->p_alloc)
	{
		unsigned long newAlloc = newLen + this->p_init;

		AIM_Pixel *tmp = new AIM_Pixel[newAlloc];
		memcpy(tmp,chain,this->p_alloc * sizeof(AIM_Pixel));
		// handle and update string 
		deleteChain();
		p_alloc = newAlloc;
		chain = tmp;
	};
	
	for (unsigned long i = 0; i < contour.len - 1; i++)
		chain[i+len] = contour.chain[contour.len - i - 2];
	len = newLen;
};

void AIM_Contour::insertChain(const AIM_Contour &contour)
{
	unsigned long newLen = this->len + contour.len - 1;

	if (newLen > this->p_alloc)
	{
		unsigned long newAlloc = newLen + this->p_init;

		AIM_Pixel *tmp = new AIM_Pixel[newAlloc];
		for (unsigned long i = newLen - 1; i >= contour.len; i--) 
			tmp[i] = chain[i - contour.len + 1];

		// handle and update string 
		deleteChain();
		p_alloc = newAlloc;
		chain = tmp;
	}
	else
	{
		for (unsigned long i = newLen - 1; i >= contour.len; i--) 
			chain[i] = chain[i - contour.len + 1];
	};
 

	for (unsigned long i = 0; i < contour.len; i++) 
		chain[i] = contour.chain[i];
	len = newLen;
};

void AIM_Contour::insertChainReverted(const AIM_Contour &contour)
{
	unsigned long newLen = this->len + contour.len - 1;

	if (newLen > this->p_alloc)
	{
		unsigned long newAlloc = newLen + this->p_init;

		AIM_Pixel *tmp = new AIM_Pixel[newAlloc];
		for (unsigned long i = newLen - 1; i >= contour.len; i--) 
			tmp[i] = chain[i - contour.len + 1];

		// handle and update string 
		deleteChain();
		p_alloc = newAlloc;
		chain = tmp;
	}
	else
	{
		for (unsigned long i = newLen - 1; i >= contour.len; i--) 
			chain[i] = chain[i - contour.len + 1];
	};
 
	for (unsigned long i = 0; i < contour.len; i++) chain[i] = 
		contour.chain[contour.len -i -1];	
	len = newLen;
};
      
AIM_Contour &AIM_Contour::operator = (const AIM_Contour &contour)
{
	if (this == &contour) return *this;
	deleteChain();

	this->active  = contour.active;
	this->p_init  = contour.p_init;
	this->p_alloc = contour.p_alloc;
	this->len     = contour.len;
	this->v_no[0] = contour.v_no[0];
	this->v_no[1] = contour.v_no[1];

	this->chain = new AIM_Pixel [contour.p_alloc];
    if (this->chain == 0) 
	{
		p_alloc = 0;
		len = 0;
	}
	else
	{
		memcpy(this->chain, contour.chain, this->p_alloc*sizeof(AIM_Pixel));
	};

	if (contour.attr)
	{
		if (!attr) attr = new C_attributes;
		*attr  = *contour.attr;
	}
	else deleteAttributes();

	return *this;
};

bool AIM_Contour::copyFrom(const AIM_Contour &contour)
{
	deleteAttributes();
	if (chain) delete[] chain;
	this->chain = new AIM_Pixel [contour.p_alloc];
    if (this->chain == 0) return false;
	this->active  = contour.active;
	this->p_init  = contour.p_init;
	this->p_alloc = contour.p_alloc;
	this->len     = contour.len;
	this->v_no[0] = contour.v_no[0];
	this->v_no[1] = contour.v_no[1];
	
	memcpy(this->chain, contour.chain, this->p_alloc*sizeof(AIM_Pixel));

	return true;
};

long AIM_Contour::addP(int firstLast, const AIM_Pixel &p)
{
	if (this->active)
	{
		if (this->len == this->p_alloc)
		{
			unsigned long newAlloc = this->p_init + this->p_alloc;
			AIM_Pixel *tmp = new AIM_Pixel [newAlloc];
      
			if (tmp == 0) return -1;

			memcpy(tmp, this->chain, this->p_alloc * sizeof(AIM_Pixel));
			delete [] this->chain;
			this->chain=tmp;
			this->p_alloc = newAlloc;
		}
    
	
		if (firstLast == POS_FIRST)
		{
			for (long i = this->len; i>0; i--) 
				this->chain[i]= this->chain[i-1];
			this->chain[0]=p;
		}   
		else if (firstLast == POS_LAST) this->chain[this->len]=p;
		else return -1; //fprintf(stderr,"add_p: error where=%d\n",where);
		this->len ++;
   
		return (firstLast == POS_FIRST) ? 0 : this->len-1;
	}
  
	return -1;
}

void AIM_Contour::initChain(unsigned long I_pinit)
{
	if (this->active && (this->chain == 0))
	{
		this->chain = new AIM_Pixel[I_pinit];
		this->p_alloc = I_pinit;
		this->p_init  = I_pinit;
		this->len = 0;
	}// 	else fprintf(stderr,"Error: init_chain, chain not activated\n");
}
	   
void AIM_Contour::deleteChain()
{
	if (chain) delete[] chain;
	chain = 0;
	this->len     = 0;
	this->p_alloc = 0;
};

long AIM_Contour::existsPinC(long x, long y)
{
  for (unsigned long i = 0; i < this->len; i++) 
  {
    if ((this->chain[i].x == x) && (this->chain[i].y == y)) 
		return (long) i;
  }

  return -1;
}

unsigned long AIM_Contour::delP(unsigned long p_no)
{
	for (unsigned long i=p_no; i<(this->len-1); i++) 
		this->chain[i] = this->chain[i+1];
	this->len--;
 
	return this->len;
}

void AIM_Contour::straightLineFit()
{
	if (this->attr)
	{
		C2DPoint *points = new C2DPoint[this->getLength()];

		for (unsigned long i=0; i < this->getLength(); i++)
		{
			points[i].x = (double) this->chain[i].x;
			points[i].y = (double) this->chain[i].y;
		}
  
		this->attr->line.initFromVector(points, this->getLength());

		this->attr->lineEndPoints[0] = this->attr->line.getBasePoint(points[0]);
		this->attr->lineEndPoints[1] = this->attr->line.getBasePoint(points[this->getLength() - 1]);

		delete [] points;
	}
}

float AIM_Contour::initContourLength()
{  
	float length=0.0;

	if (this->active)
	{
		unsigned long imax = this->getLength() - 1;
		for (unsigned long i = 0; i < imax;i++)
		{
			float dx = (float) this->chain[i].x - this->chain[i + 1].x;
			float dy = (float) this->chain[i].y - this->chain[i + 1].y;
			length += (float) sqrt(dx * dx + dy * dy);
		}
	}
	if (this->attr) this->attr->length = length;

    return length;
}


bool AIM_Contour::suppressPoint(AIM_Pixel *points, int start, int stop, float tolerance, float exponent)
{
	bool retVal = false;

	float maxdist = -1.0;

	int normx = points[stop].y  - points[start].y;
	int normy = points[start].x - points[stop].x;
	float normalize = (float) sqrt((float) (normx * normx + normy * normy));

    float tol = tolerance;

	if (tolerance == 0.0) 
	{
		if (normx == 0 && normy == 0) tol= getStraightLineTolerance((float) (stop-start), exponent);
		else tol = getStraightLineTolerance(normalize,exponent);
	};
  

	if (normx || normy)
	{  // open curve 
		for (int p=start+1;p<=stop;p++)
		{
			int dx = points[p].x - points[start].x;
			int dy = points[p].y - points[start].y;
			float dist = fabs((float)(dx*normx + dy*normy))/normalize;
			if (dist > maxdist) maxdist = dist;
		}
	}
	else 
	{  // closed curve    
		for (int p=start+1;p<=stop;p++)
		{
			int dx = points[p].x - points[start].x;
			int dy = points[p].y - points[start].y;
			float  dist = (float) sqrt((float) (dx*dx + dy*dy));
			if (dist > maxdist) maxdist=dist;
		}
	}
   
	if (maxdist < tol) retVal = true;
  
	return retVal;
}

void AIM_Contour::initShapeAttributes(float straightLength, float tolerance, float exponent)
{
	if ((this->active) && (this->attr))
	{   
		int normx = this->chain[this->getLength()-1].y - this->chain[0].y; 
		int normy = this->chain[0].x - this->chain[this->getLength()-1].x;
		float normalize= (float)1.0 / sqrt((float) (normx*normx+normy*normy));
		this->initContourLength();

		this->attr->shape.area  = 0.0f;
		this->attr->shape.mdist = -1.0;
			
		this->attr->shape.poly = this->getNrOfPolylines(tolerance, exponent);

		if (normx || normy)
		{  // open curve 
			float plus=0.0, minus=0.0;

			for (unsigned long p=0; p < this->getLength(); p++)
			{
				int dx = this->chain[p].x - this->chain[0].x;
				int dy = this->chain[p].y - this->chain[0].y;
				int dp = dx*normx + dy*normy;
				float dist= fabs((float)(dp)) * normalize;
				if (dist > this->attr->shape.mdist) this->attr->shape.mdist = dist; 
				if (dp > 0) plus += dist;
				if (dp < 0) minus += dist;
			}

			// attributes for open curves 
			if (fabs(plus)== fabs(minus)) this->attr->shape.curv = 0.0;
			else this->attr->shape.curv = (fabs(plus) - fabs(minus)) / (fabs(plus) + fabs(minus));
      
			if (this->attr->shape.poly == 1 && this->attr->length > straightLength)
			{
				this->attr->shape.type = STRAIGHT;
				this->straightLineFit();
				this->attr->shape.area = 1.0;	
			}
			else attr->shape.type = CURVED;
		}
		else 
		{  // closed curve 
			for (unsigned long p=0; p < this->getLength(); p++) 
			{
				int dx = this->chain[p].x - this->chain[0].x;
				int dy = this->chain[p].y - this->chain[0].y;
				float dist = (float) sqrt((float) (dx*dx + dy*dy));
				if (dist > this->attr->shape.mdist) this->attr->shape.mdist=dist;
			}
     
			for (unsigned long p=0; p < this->getLength()-1; p++) 
				this->attr->shape.area += (this->chain[p].x - this->chain[p+1].x) * (this->chain[p].y + this->chain[p+1].y);

			// attributes for a closed curve 
			this->attr->shape.curv = 1.0;      // incorrect but what the heck 
			this->attr->shape.area = (float) fabs(this->attr->shape.area / 2.0);
			this->attr->shape.type = CLOSED;    
		}	
	}
}

int AIM_Contour::getNrOfPolylines(float tolerance,float exponent)
{ 
	int n_lines = 0;
 
	if (this->active)
	{
		AIM_Pixel *points = new AIM_Pixel[this->getLength()];
		memcpy(points, this->chain, this->getLength() * sizeof(AIM_Pixel));
		n_lines = polygonPtsAdaptive(points, this->getLength(), tolerance, exponent) + 1;
		delete[] points;
	}
    
	return n_lines;
}

int AIM_Contour::polygonPtsAdaptive(AIM_Pixel *&points, float &tolerance, float &exponent)
{
	int n_lines = 0;
 
	if (this->active)
	{
		points = new AIM_Pixel[this->getLength()];
		memcpy(points, this->chain, this->getLength() * sizeof(AIM_Pixel));
		n_lines = polygonPtsAdaptive(points, this->getLength(), tolerance, exponent);
	};

	return n_lines;
}

int AIM_Contour::polygonPtsAdaptive(AIM_Pixel *points, int len, float &tolerance, float &exponent)
{  
	AIM_Pixel *curve = new AIM_Pixel[len];
	AIM_Pixel *line = new AIM_Pixel[len];
  
	memcpy(curve, points, len*sizeof(AIM_Pixel));
   
	int nPoints; 

	if (tolerance == 0.0)  nPoints =  polygonApprox (curve,len, line, tolerance, exponent) + 1;
	else nPoints = polygonApprox(curve,len,line, tolerance, 0.0) + 1;
 
	int *indexArray     = new int[nPoints];
	bool *removeIndices = new bool[nPoints];
	memset(removeIndices, 0, nPoints*sizeof(bool));

  
	// Get pixel number for all break-points 
	for (int i = 0, p_no=0; i < nPoints; i++)
	{
		for (int j = p_no; j < len; j++) 
			if (curve[j].x == line[i].x && curve[j].y == line[i].y)
			{
				indexArray[i]=j;
				p_no = j+1;
				break;
			}
	}
  
	int number = nPoints;
 
	bool ReallyReady = false;
	bool Ready = false;

	do
	{
		int maxind = 0;
		int maxlength=0;
    
		for (int i = 1; i < number-1; i++)
			if (removeIndices[i] && (indexArray[i+1]-indexArray[i-1]) > maxlength)
			{
				maxlength = indexArray[i+1] - indexArray[i-1];
				maxind    = i;
			}
  
			if (maxind)
			{    
				if (suppressPoint(points,indexArray[maxind-1], indexArray[maxind+1], tolerance,exponent))
				{
					for (int i=maxind; i<number-1; i++)
					{
						indexArray[i]=indexArray[i+1];
						removeIndices[i]=removeIndices[i+1];
					}

					number--;
					ReallyReady = Ready = false;     
				}
				else removeIndices[maxind] = false;
			} // if (maxind)
			else 
			{
				if (ReallyReady) Ready = true; // no more points to suppress 
				else 
				{
					ReallyReady = true;
					for (int i=0; i<number; i++) removeIndices[i] = true;
				}
			} 
	} while (!Ready || !ReallyReady);
  
  
	for (int i=1; i<number-1; i++)  points[i-1] = points[indexArray[i]];

	delete[] curve;
	delete[] line;
	delete[] indexArray;
	delete[] removeIndices;

	return number - 2;
}

float AIM_Contour::getStraightLineTolerance(float dist, float exponent)
{
	float tol = 0.0f;
 
	if (dist !=0.0)
	{
		tol = (float) log(10.0 * pow(dist, exponent));
	}

	return tol;
}

void AIM_Contour::initDirectionAttributes()
{
	if (this->active && this->attr)
	{
		this->attr->pdir[0] = 0.0f;
		this->attr->pdir[1] = 0.0f;

		AIM_Pixel line[2000]; 

		long nLines = polygonApprox(chain, this->getLength(),line, 0.0,1.0);
    
		if ((line[1].y != line[0].y) || (line[0].x != line[1].x))
			this->attr->pdir[0] =atan2((float)(line[1].y-line[0].y),(float)(line[0].x-line[1].x));
    
		if ((line[nLines-1].y != line[nLines].y) || (line[nLines].x != line[nLines-1].x))
			attr->pdir[1] = atan2((float)(line[nLines-1].y-line[nLines].y), (float)(line[nLines].x-line[nLines-1].x)); 
	}
}

int AIM_Contour::polygonApprox(AIM_Pixel  *curve, int npt, AIM_Pixel *polygon, float tolerance,float exponent)
// * An interface that makes sure the end of the last span, which is not the
// * start of any other span, will be written to _polygon_. It is the caller's
// * responsability to provide sufficient storage space in _polygon_.
{
  int	n_spans= polygonApproxR(curve, npt, polygon, tolerance, exponent);
  polygon[n_spans]= curve[npt-1];
  return n_spans;
}

int AIM_Contour::polygonApproxR(AIM_Pixel *curve, int npt, AIM_Pixel *polygon, float toleranz, float exponent)
// * Recursive procedure for subdividing the curve into sections that are well
// * enough (i.e., inside _toleranz_) represented by a straight line (a span of
// * a polygon. The starting points of all spans are written to _polygon_, and
// * the ending point is implicitly given as the starting point of the next
// * span. If some application (e.g., with closed curves) does not need the end
// * point of the last span, which is always the last point of _curve_, it may
// * call this function. Normally, "polygon_approx" should be used instead.
{
	AIM_Pixel *ende, *p_max, *p;
	int nx, ny, dq_max, d, dq, diff, teil1, index;
	float tol;
	ende= &curve[npt-1]; 
	nx= ende ->y - curve->y;
	ny= curve->x - ende ->x;

	if (toleranz == 0.0) 
	{
		if (nx == 0 && ny == 0) tol = getStraightLineTolerance((float) npt,exponent);
		else tol = getStraightLineTolerance(float(sqrt(float(nx*nx+ny*ny))),exponent);
	}
	else tol = toleranz;

	if (nx || ny)
	{  // open curve 
		dq_max=(int)( (nx*nx + ny*ny) * tol*tol);
		diff= ende->x*nx+ ende->y*ny;
		p_max= 0;

		for (p= curve; p != ende; p++)
		{
			d= p->x*nx + p->y*ny - diff;
			if ( (dq= d*d) > dq_max) 
			{ 
				dq_max= dq; p_max= p;
			}
		}
	}
	else 
	{ // closed curve or 1 point 
		int dx, dy;
		dq_max= (int)(tol * tol);
		p_max= 0;
		for (p= curve; p != ende; p++) 
		{
			dx= p->x - curve->x; dy= p->y - curve->y;
			dq= dx*dx + dy*dy;
			if (dq > dq_max) 
			{ 
				dq_max= dq; 
				p_max= p;
			}
		} 
	}
  
  
	if (p_max)
	{ // above tol -> split
		index = p_max - curve;
		teil1 = polygonApproxR(curve, index+1, polygon, toleranz, exponent);
		return teil1 + polygonApproxR(p_max, npt-index, polygon + teil1, toleranz, exponent);
	}
  
	// toleranz was observed
	*polygon= *curve;
	return 1;
}
/*
bool AIM_Contour::approxEllipse(CXYEllipse &ellipse, long cno)
{
	bool ret = true;

	CBestFit2DEllipse bestfitellipse;
	CCharStringArray Labels;
	CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();

	int nObs = this->getLength();
	Matrix obs(nObs, 2);

	CLabel ptLabel("0");
	for(int i = 0; i < nObs; i++)
	{
		ptLabel++;
		obs.element(i, 0) = this->chain[i].x;
		obs.element(i, 1) = this->chain[i].y;
		// Add covariance
		Matrix cov(2,2);
		cov = 0;	
		cov.element(0, 0) = 0.5f;
		cov.element(1, 1) = 0.5f;
		covariance->addSubMatrix(cov);
		// Add label
		CCharString* str;
		str = Labels.Add();
		*str = ptLabel.GetChar();
	}

	int nPara = 5;

	CLeastSquaresSolver lss(&bestfitellipse, &obs, &Labels, NULL, covariance);

	ret = lss.solve();

	if (ret)
	{
		Matrix result(nPara, 1);
		Matrix covmat(nPara, nPara);
		for(int i = 0; i < nPara; i++)
		{
			result.element(i, 0) = lss.getSolution()->element(i, 0);			
			for(int j = 0; j < nPara; j++)
			{
					covmat.element(i, j) = lss.getCovariance()->element(i, j);
				}
		}

		CLabel label = "Ellipse";
		label += abs(cno);
		ellipse.set(result, label);
		ellipse.setCovariance(covmat);
		if ((ellipse.getA() <= 0) || (ellipse.getB() <= 0)) ret = false;
		else if (cno > 0)
		{
			AIM_Contour cont;
			cont.active = true;
			cont.initChain(this->getLength() + 1);
			cont.len = 0;
			for (unsigned int i = 0; i < this->getLength(); ++i)
			{
				C2DPoint p(chain[i].x,chain[i].y);
				if (ellipse.getDistance(p) < 1.5f)
				{
					cont.chain[cont.len] = this->chain[i];
					++cont.len;
				};
			}
			return cont.approxEllipse(ellipse, -cno);
		}
	}	

	return ret;
};
*/
AIM_Graph::AIM_Graph(unsigned long Cinit, unsigned long Vinit, 
					 long I_width, long I_height):
					 c_alloc(Cinit), c_init(Cinit), 
					 v_alloc(Vinit), v_init(Vinit),
					 c_max(0), v_max(0), attr(0),
					 width(I_width), height(I_height)
{
	this->v = new AIM_Vertex[v_init];
	this->c = new AIM_Contour[c_init];
}

AIM_Graph::~AIM_Graph()
{
	delete [] v;
	delete [] c;
	if (attr) delete attr;
	attr = 0;
}

void AIM_Graph::initAttr()
{
	if (attr) delete attr;
	attr = new G_attributes;
	memset((void *) attr,0,sizeof(G_attributes));
	attr->BreakPts_def = true;
	attr->tolerance    = 0.0;
	attr->exponent     = 1.0;
	attr->Straight_def = true;
	attr->straight     = 10.0; 
};

long AIM_Graph::getNoOfV() const
{
	long number=0;

	for (unsigned long i=0; i < this->v_max; i++)
		if (this->v[i].active) number++;
  
	return number;
};


long AIM_Graph::getNoOfC() const
{
	long number=0;

	for (unsigned long i=0; i < this->c_max; i++)
		if (this->c[i].active) number++;
  
	return number;
};

long AIM_Graph::getnewV()
{
	long newPos = -1;

	for (unsigned long i=0; (i < this->v_alloc) && (newPos < 0); i++) 
	{
		if (!this->v[i].active) newPos = i;
	};

	if (newPos == -1)
	{
		unsigned long newAlloc = this->v_init + this->v_alloc;
		
		AIM_Vertex *tmp = new AIM_Vertex[newAlloc];

		if (tmp == 0) return -1;

		memcpy(tmp, this->v, this->v_alloc * sizeof(AIM_Vertex));
		delete [] this->v;
		this->v=tmp;

		newPos = this->v_alloc;
		this->v_alloc = newAlloc;
	}
	this->v[newPos].active = true;
	if (newPos + 1 > (long) this->v_max) this->v_max= newPos + 1;
	return newPos;
}


void AIM_Graph::deleteV(unsigned long vertex)
{
	this->v[vertex].active = false;
	this->v[vertex].ord    = 0;
}

long AIM_Graph::getnewC()
{  
	long newPos = -1;

	for (unsigned long i=0; (i < this->c_alloc) && (newPos < 0); i++) 
	{
		if (!this->c[i].active) newPos = i;
	}

	if (newPos == -1)
	{   // couldn't find a new empty position --> reallocate 

		unsigned long newAlloc = this->c_init + this->c_alloc;	  

		AIM_Contour *tmp = new  AIM_Contour[newAlloc];
    		
		if (tmp == 0) return -1;

		for (unsigned long u = 0; u < this->c_alloc; ++u)
		{
			tmp [u] = this->c[u];
		};

		delete [] this->c;

		this->c = tmp;

		newPos = this->c_alloc;
		this->c_alloc = newAlloc;
	}
	this->c[newPos].active = true;
	if (newPos + 1 > (long) this->c_max) this->c_max= (unsigned long) newPos +1;
  
	return newPos;
}

void AIM_Graph::deleteC(unsigned long contour)
{
	this->c[contour].deleteAttributes();

	this->c[contour].deleteChain();
	this->c[contour].active  = false;
}


long AIM_Graph::copyC(unsigned long source)
{
	long target = this->getnewC();

	if ((source < this->c_alloc) && (this->c[source].active))
	{
		if (!this->c[target].copyFrom(this->c[source])) return -1;
	}

	return target;
}

long AIM_Graph::getVertex(long x, long y)
{
	for (unsigned long i=0; i<this->v_max; i++)
	{
		if ((this->v[i].active)&&(this->v[i].x == x)&&(this->v[i].y == y))
			return i;
	}
  return -1;
}

int AIM_Graph::getPosOfVInC(long v_no, long c_no)
{
  int multiplicity=0,pos;

  if (this->v[v_no].active && this->c[c_no].active &&
      ((pos=this->v[v_no].getPositionOfContour(c_no)) >= 0)) 
    multiplicity=this->v[v_no].c[pos].mul;
  
  switch (multiplicity)
  {
  case 2: return POS_BOTH;
  case 1: 
    if (this->c[c_no].v_no[0] == v_no) return POS_FIRST;
    else if (this->c[c_no].v_no[1] == v_no) return POS_LAST;
    else return POS_NONE;
  default: return POS_NONE; 
  }
}


void AIM_Graph::mergeContoursAtVertex(long vertIndex, unsigned char type)
{
	if ((this->v[vertIndex].active) && (this->v[vertIndex].ord == 2))
	{
		unsigned long len[2], kno2[2], cno[2];
		int firstLast[2];  
		bool quit = false; 

		for (int i=0; i<2; i++)
		{
			cno[i]      = this->v[vertIndex].c[i].c_no;
			firstLast[i]= this->getPosOfVInC(vertIndex, cno[i]);
      
			if ((firstLast[i] != POS_NONE) && (firstLast[i] != POS_BOTH))
			{
				len[i]  = this->c[cno[i]].getLength();
				kno2[i] = this->c[cno[i]].v_no[1-firstLast[i]];
			}
			else quit = true;
		}
   
		if (!quit)
		{
			// allocate new memory for the merged string 
			unsigned long newLen    = this->c[cno[0]].getLength() + this->c[cno[1]].getLength()-1;
			unsigned long new_alloc = newLen + this->c[cno[0]].getInitLength();
			AIM_Pixel *tmp = new AIM_Pixel [new_alloc];
      
			// copy both original strings to the new 
			if (firstLast[0] == POS_FIRST)
			{
				this->c[cno[0]].getPixel(0).type = type;
				for (unsigned long i=0;i<len[0];i++) 
				{
					tmp[i+len[1]-1] = this->c[cno[0]].getPixel(i);
				}
				for (unsigned long i=0;i<(len[1]-1);i++)
					if (firstLast[1] == POS_FIRST) 
					{
						tmp[i] = this->c[cno[1]].getPixel(len[1]-1-i);
					}
					else 
					{
						tmp[i] = this->c[cno[1]].getPixel(i);	 
					}
			}
			else
			{
				this->c[cno[0]].getPixel(len[0]-1).type = type;
				for (unsigned long i=0;i<len[0];i++)
				{
					tmp[i] = this->c[cno[0]].getPixel(i);
				}
				for (unsigned long i=0;i<(len[1]-1);i++)
					if (firstLast[1] == POS_FIRST) 
					{
						tmp[i+len[0]] = this->c[cno[1]].getPixel(i+1);
					}
					else  
					{
						tmp[i+len[0]] = this->c[cno[1]].getPixel(len[1]-2-i);
					}
			}

			// handle and update string 
			this->c[cno[0]].deleteChain();
			this->c[cno[0]].setChain(tmp, newLen, new_alloc);

			// update cross references 
			this->c[cno[0]].v_no[firstLast[0]] = kno2[1];
			this->v[kno2[1]].removeNeighbour(cno[1],false);
			this->v[kno2[1]].addNeighbour(cno[0]);
			this->deleteC(cno[1]);
			this->deleteV(vertIndex);
			this->c[cno[0]].deleteAttributes();
		} //if (!quit)
	} // if ((this->v[vertIndex].active) && (this->v[vertIndex].ord == 2))
}

bool AIM_Graph::splitContourAt(long sourceContour, long x, long y, int type,
							   long &newContour, long &newVertex)
{
	if (this->c[sourceContour].active)
	{
		long fpos=this->c[sourceContour].existsPinC(x,y);
    
		if (fpos >= 0)
		{
			long lpos=this->c[sourceContour].getLength() - fpos -1;
			if ((fpos > 1) && (lpos > 1))
			{
				newContour = this->copyC(sourceContour);
				newVertex  = this->getnewV();
				long klast = this->c[sourceContour].v_no[1];
				this->v[newVertex].x=x;
				this->v[newVertex].y=y;
				this->v[newVertex].type=type;
				this->v[newVertex].addNeighbour(newContour);
				this->v[newVertex].addNeighbour(sourceContour);
				this->c[sourceContour].cutOffChainAtEnd(this->c[sourceContour].getLength() - lpos);
				this->c[sourceContour].getPixel(fpos).type=type;
				this->c[sourceContour].v_no[1]=newVertex;
				this->v[klast].removeNeighbour(sourceContour, false);
				this->c[newContour].cutOffChainAtBeginning(fpos);

				this->c[newContour].getPixel(0).type=type;
				this->c[newContour].v_no[0]=newVertex;
				this->v[klast].addNeighbour(newContour);
	
				this->c[sourceContour].deleteAttributes();
				
				return true;
			}
		}
	}
  
  return false;
}

int AIM_Graph::splitContours()
{
	unsigned int max = this->c_max;

	struct splitType{int len; AIM_Pixel *points; };

	splitType *tmp = new splitType[max];
	memset(tmp,0,max * sizeof(splitType));


	for (unsigned long cno = 0; cno < max; cno++)
	{
		if (this->c[cno].active)
		{
			float tol,e;

			if ((this->attr) && (this->attr->BreakPts_def)) 
			{
				tol = this->attr->tolerance;
				e   = this->attr->exponent;
			}
			else 
			{
				tol = 0.0;
				e   = 1.0;
			}


			AIM_Pixel *points = 0; 
			int nr = this->c[cno].polygonPtsAdaptive(points, tol, e);
			
			tmp[cno].len = nr;
			
			if (nr)
			{
				tmp[cno].points = new AIM_Pixel[nr];
				for(int i=0; i<nr; i++) tmp[cno].points[i] = points[i];
			}

			delete [] points;
			points = 0;
		}
	}
  
	for (unsigned long tno = 0; tno < max; tno++)
	{
		if (tmp[tno].len)
		{
			int source = tno;
			long target, newkpt;

			for(int i=0;i<tmp[tno].len;i++)
			{
				if (this->splitContourAt(source,tmp[tno].points[i].x,tmp[tno].points[i].y,5, target, newkpt))
					source=target;
			}
			delete [] tmp[tno].points;
		}
  
	}

	delete [] tmp;
 
	return 1;
}

int AIM_Graph::groupContours(bool onlyCurved)
{
 	float tolerance =  0.0f;
	float SlLength  = 10.0f;
	float exponent  =  1.0f;

	if (this->attr && (this->attr->BreakPts_def)) 

	{
		tolerance = this->attr->tolerance;
		exponent  = this->attr->exponent;
		SlLength  = this->attr->straight;
	};

	int c_no[2];
	C_attributes *Cattr[2];

	for (unsigned long cno=0; cno< this->c_max;cno++)
	{
		if (this->c[cno].active)
		{
			this->c[cno].initAttributes(false);
			Cattr[0] = this->c[cno].getAttr();
			this->c[cno].initShapeAttributes(SlLength, tolerance, exponent);
		}
	}
  
	unsigned long *v = new unsigned long [this->v_max];
	memset(v, 0, this->v_max*sizeof(unsigned long));
  
	int nr = 0;

	for (unsigned long vno= 0; vno < this->v_max;vno++)
	{
		if (this->v[vno].active && this->v[vno].ord == 2 && 
			this->v[vno].c[0].mul == 1 && this->v[vno].c[1].mul == 1)
		{
      
			if (onlyCurved)
			{
				for (int i=0;i<2;i++) c_no[i]= this->v[vno].c[i].c_no;	
				Cattr[0]= this->c[c_no[0]].getAttr();
				Cattr[1]= this->c[c_no[1]].getAttr();
				if (Cattr[0]->shape.type == CURVED && Cattr[1]->shape.type == CURVED) 
					v[nr++]=vno;
			}
			else v[nr++]=vno;
		}
	}
  
	// suppress all points in the v-array all together 
	for (int vno=0; vno<nr; vno++) this->mergeContoursAtVertex(v[vno],8);

	delete [] v;

	return nr;
}


int AIM_Graph::removeClosed(int lengthThreshold)
{ 
	int no_del = 0;

	for (unsigned long vno=0; vno<this->v_max; vno++)
	{ 
		if (this->v[vno].active && this->v[vno].ord == 1 && this->v[vno].c[0].mul == 2)
		{
			unsigned long cno=this->v[vno].c[0].c_no;
			if (this->c[cno].active && this->c[cno].getLength() <= (unsigned long) lengthThreshold)
			{
				// remove contour and vertex 
				this->deleteC(cno); 
				this->deleteV(vno);
				no_del++;
			}
		}
	}

  return no_del;
}


int AIM_Graph::iterateClean(float strengthThreshold, float lengthThreshold)
{ 
	int vno[2];
	// since we call this function iteratively we have to find all 
    // eligable contours for removal and then remove all together.

	int no_del=0;

	unsigned long *list = new unsigned long [this->c_max];

	for (unsigned long cno=0;cno < this->c_max;cno++)
	{ 
		if (this->c[cno].active)
		{
			for (long j=0;j<2;j++) vno[j]=this->c[cno].v_no[j];
			if ((this->v[vno[0]].ord == 1 && this->v[vno[0]].c[0].mul == 1) ||
				(this->v[vno[1]].ord == 1 && this->v[vno[1]].c[0].mul == 1))
			{
				C_attributes *Cattr = this->c[cno].getAttr();
				if ((Cattr) && (Cattr->length < lengthThreshold) && 
					(Cattr->strength < strengthThreshold) && Cattr->shape.type == CURVED)
					list[no_del++]=cno;
			}
		}
	}

	for (long i=0;i<no_del;i++) // remove vertex and chain 
	{ 
		unsigned long cno=list[i];
		if (this->c[cno].active)
		{
			for (long j=0;j<2;j++)
			{
				vno[j]= this->c[cno].v_no[j];
				if (this->v[vno[j]].ord == 1 && this->v[vno[j]].c[0].mul == 1)
					this->deleteV(vno[j]);
				else this->v[vno[j]].removeNeighbour(cno,false);
			}
			this->deleteC(cno); 
		}     
	}

	delete[] list;

	return no_del;
}
	   
int AIM_Graph::clean(float strengthThreshold, float lengthThreshold)
{
	int totalCleared = 0;
	int cleared      = 0;
	do
	{	
		cleared = iterateClean(strengthThreshold, lengthThreshold);
		totalCleared += cleared;
	} while (cleared);

	return totalCleared;
}


void AIM_Graph::dcelEdgeLinks()
{
	for (unsigned long kno=0;kno < this->v_alloc;kno++)
	{
		if (this->v[kno].active)
		{    
			for (int j=0; j < this->v[kno].ord; j++)
			{
				unsigned long cj = this->v[kno].c[j].c_no;
				C_attributes *Cattr = this->c[cj].getAttr();				
				float dj = (Cattr) ? Cattr->pdir[this->getPosOfVInC(kno,cj)] : (float)(-2.0*M_PI);

				for (int i=j+1; i < this->v[kno].ord; i++)
				{
					unsigned long ci = this->v[kno].c[i].c_no;
					Cattr = this->c[ci].getAttr();
					float di = (Cattr) ? Cattr->pdir[this->getPosOfVInC(kno,ci)] : (float)(-2.0*M_PI);

					if (di < dj)
					{
						AIM_C_in_Vertex tmp = this->v[kno].c[j];
						this->v[kno].c[j] = this->v[kno].c[i];
						this->v[kno].c[i] = tmp;
					}
				}
			}
		}
	}
}


void AIM_Graph::refineEndpoints()
{
	for (unsigned long i=0; i < this->v_max; i++)
	{
		if (this->v[i].active)
		{
			// compute refined vertex position 
			float xx = 0.0f;
			float yy = 0.0f;
			float no = 0.0f;

			for (int j=0; j<this->v[i].ord; j++)
			{
				unsigned long cno = this->v[i].c[j].c_no;
				C_attributes *Cattr =  this->c[cno].getAttr();

				if (this->c[cno].active && Cattr && Cattr->shape.type == STRAIGHT)
				{				
					int firstLast= this->getPosOfVInC(i,cno);
					xx += (float) Cattr->lineEndPoints[firstLast].x;
					yy += (float) Cattr->lineEndPoints[firstLast].y;
					no++;
				}
			}
			xx /= no;
			yy /= no;

			// assign the contours the new position 
      
			for (int j=0; j < this->v[i].ord; j++)
			{
				unsigned long cno = this->v[i].c[j].c_no;
				C_attributes *Cattr =  this->c[cno].getAttr();

				if (this->c[cno].active && Cattr && Cattr->shape.type == STRAIGHT)
				{
						int firstLast = this->getPosOfVInC(i,cno);
						Cattr->lineEndPoints[firstLast].x = xx;
						Cattr->lineEndPoints[firstLast].y = yy;
				}
			}
		}
	}
}




void AIM_Graph::getStatistics(float &avgStrenght, float &avgLength, float *medStrenght, float *medLength)
{
	CSortVector<float> *lenvec=0, *strvec=0;
 
	avgStrenght = avgLength =0.0f;
	if (medStrenght) 
	{
		*medStrenght = 0.0;
		strvec = new CSortVector<float>(this->c_alloc, 0.0f);
	}
	if (medLength) 
	{
		*medLength=0.0;
		lenvec = new CSortVector<float>(this->c_alloc, 0.0f);
	}
    
	unsigned long no = 0;

	for (unsigned long c_no = 0;c_no < this->c_alloc;c_no++)
	{
		C_attributes *Cattr;
		if (this->c[c_no].active && ((Cattr = this->c[c_no].getAttr())))
		{
			avgStrenght += Cattr->strength;
			if (medStrenght) (*strvec)[no] = Cattr->strength;
			avgLength += Cattr->length;
			if (medLength) (*lenvec)[no] = Cattr->length;
			no++;
		}
	}
 
	if (no)
	{
		avgLength   /= (float) no;
		avgStrenght /= (float) no;
		unsigned long n2  = no / 2;
		unsigned long n2p = n2 + 1;
    
		if (medStrenght)
		{
			strvec->resize((int) no, 0);
			strvec->sortAscending();
			*medStrenght=(float)(no % 2 ? (*strvec)[n2p] : 0.5*((*strvec)[n2] + (*strvec)[n2p]));
		}
    
		if (medLength)
		{
			lenvec->resize((int) no, 0);
			lenvec->sortAscending();
			*medLength=(float)(no % 2 ? (*lenvec)[n2p] : 0.5*((*lenvec)[n2] + (*lenvec)[n2p]));
		}
	}
 
	if (lenvec) delete lenvec;
	if (strvec) delete strvec;
}
