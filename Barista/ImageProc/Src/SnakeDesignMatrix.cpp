#include "SnakeDesignMatrix.h"




CSnakeDesignMatrix::CSnakeDesignMatrix(int npoints):nPoints(npoints)
{
	if (npoints <= 0) nPoints = 0;
	else
	{
		unsigned int length = nPoints *nPoints;
		this->resize(length, 0.0f);
	}
}

CSnakeDesignMatrix::~CSnakeDesignMatrix()
{
}

void CSnakeDesignMatrix::initializeConfigurateDesignMatrix(eSnakeMode SnakeMode, bool open, float alpha, float beta, float gamma)
{
	if(SnakeMode == eTraditional && open == true)
	{
		float temp1 = -alpha-4*beta;
		float temp2 = 2*alpha + 6*beta + gamma;
	
		unsigned int rowmax = this->getNPoints();
		unsigned int max = this->getLength()- 1;

		//first two lines of the design matrix
		(*this)[0] = temp2;
		(*this)[1] = temp1;
		(*this)[2] = beta;
		(*this)[rowmax] = temp1;
		(*this)[rowmax+1] = temp2;
		(*this)[rowmax+2] = temp1;
		(*this)[rowmax+3] = beta;

		for (unsigned int i = 2*rowmax+2; i < max-2*rowmax; i=i+rowmax+1)
		{	
			(*this)[i-2] = beta;
			(*this)[i-1] = temp1;
			(*this)[i] = temp2;
			(*this)[i+1] = temp1;
			(*this)[i+2] = beta;
		}

		//last two lines of the design matrix
		(*this)[max-rowmax-3] = beta;
		(*this)[max-rowmax-2] = temp1;
		(*this)[max-rowmax-1] = temp2;
		(*this)[max-rowmax] = temp1;
		(*this)[max-2] = beta;
		(*this)[max-1] = temp1;
		(*this)[max] = temp2;
	}
}

/*void CSnakeDesignMatrix::dump(const CCharString &FileName) const
{
	ofstream oFile(FileName.GetChar());
	if (oFile.good())
	{
		this->dump(oFile);
	}
}

void CSnakeDesignMatrix::dump(ofstream &oFile) const
{
	oFile.setf(ios::fixed, ios::floatfield);
	oFile.precision(3);

	if (this->nPoints < 1 )
	{
		oFile << "empty matrix";
	}
	else 
	{
		for (unsigned int i = 1; i < this->nPoints; i++)
		{
			oFile << "row: ";
			oFile.width(5);
			oFile << i;
			unsigned int j = 1;

			for (; j <= i; j++)
			{	
				oFile.width(5);
				oFile << " ";
			}

			for (;j < this->nPoints; j++)
			{	
				oFile.width(5);
				oFile << (*this)(i,j);
			}
		}
		oFile << endl;
	}
};*/

