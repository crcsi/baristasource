#include <limits.h>
#include <float.h>
#include <time.h>
#include <fstream>
#include <strstream>
using namespace std;

#include "tiffio.h"
#include "statistics.h"
#include "ProtocolHandler.h"
#include "m_match_def.h"
#include <vector>

#include "SnakeExtraction.h"

#define DebugLevel 0		///<Defined the level of data for output in files, ... \n
							///< 0-> no additional output, \n
							///< 1-> design matrix, images and change vector, \n
							///< 2-> output of everything\n

class CFileName;
class CCharString;


CSnakeExtractor::CSnakeExtractor(const SnakeExtractionParameters &I_parameters):
							extractionParameters(I_parameters), pGradXImage(0), pGradYImage(0)
{
};

//////////////////////////////////////////////////////////////////////////////////////////
CSnakeExtractor::~CSnakeExtractor(void)
{
	featRoi= NULL;
	if(pGradXImage) delete pGradXImage;
	if(pGradYImage) delete pGradYImage;
};

//////////////////////////////////////////////////////////////////////////////////////////
bool CSnakeExtractor::computeSnake(CXYPointArray *snakeStartingPoints)
{
	if (DebugLevel == 1 || DebugLevel == 2)
	{
		CFileName fnVecStart("StartSnakePoints.txt");
		fnVecStart.SetPath(protHandler.getDefaultDirectory());
		snakeStartingPoints->writeXYPointTextFile(fnVecStart, int (1));
	}

	bool ret = false;

	if (this->listener)
	{
		CCharString str("Start Snake Computation");
		if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
		this->listener->setTitleString(str.GetChar());
		this->listener->setProgressValue(5.0);
	}

	long offsetX = this->featRoi->getOffsetX();
	long offsetY = this->featRoi->getOffsetY();
	long width   = this->featRoi->getWidth();
	long height  = this->featRoi->getHeight();

	//pre processing
	this->pGradXImage = new CImageBuffer(offsetX, offsetY, width, height, this->featRoi->getBands(), 32, false, 0.0f);				//calculate derivations
	this->pGradYImage = new CImageBuffer(offsetX, offsetY, width, height, this->featRoi->getBands(), 32, false, 0.0f);
	
	ret = SmoothImages();							//calculate Gaussian Image
	ret = CalculateDerivations();					//calculate Derivations in X and Y
	
	//	this->pDesignMatrix = new CSnakeDesignMatrix(this->pSnakeVector->getSize());
//	this->pDesignMatrix->initializeConfigurateDesignMatrix(this->extractionParameters.Mode, this->extractionParameters.getOpenSnakeMode(), this->extractionParameters.getAlpha(), this->extractionParameters.getBeta(), this->extractionParameters.getGamma());

	//values which are need for the calculation
	Matrix DesignMatrix;							//Design Matrix
	Matrix InvertedDesignMatrix;					//Inverted Design Matrix
	
	vector<CXYPoint> NewSnakeVec;					//vector for the new points (X and Y)
	vector<CXYPoint> AdditionalExternalEnergy;		//additional energy term (values depends on used snake

	double temp_diffX, temp_diffY;
	double temp_diff, d_XY = 100.0f;		//values for the stop criteria
	unsigned int counter = 0;								//hard stop criteria: counter >10000
	unsigned int size = 0;									//size of the current pSnakeVector
	unsigned int i,j;										//indices
	
	//start the calculation
	do
	{
		//////////1. Preparation of the snake calculation//////////////////////////
		//create/ update the pSnakeVector
		if (counter == 0)
		{
			this->pSnakeVector = new CSnakePointsVec();
			this->pSnakeVector->initializeSnakeVector(snakeStartingPoints, this->featRoi->getOffset(), this->extractionParameters, this->pGradXImage, this->pGradYImage);
			size = this->pSnakeVector->getSize();
		}
		else
		{
			if (this->pSnakeVector->updateSnakeVector(&NewSnakeVec, this->featRoi->getOffset(), this->extractionParameters, this->pGradXImage, this->pGradYImage))
				size = this->pSnakeVector->getSize();
			else return false;
		}
			
		if (DebugLevel == 2) this->pSnakeVector->writeVector(counter);

		//create a new design matrix + invert
		DesignMatrix.ReSize(size, size);				
		DesignMatrix = 0.0;
		ret = calculateConfiguratedDesignMatrix(&DesignMatrix);	

		//double det = DesignMatrix.Determinant();

		if (ret && DesignMatrix.Determinant())
		{
			InvertedDesignMatrix.CleanUp();
			InvertedDesignMatrix = DesignMatrix.i();
		}
		else return false;

		if (counter == 0 && (DebugLevel == 1 || DebugLevel == 2)) writeMatrix(&DesignMatrix,CFileName ("ConfiguratedDesignMatrix.txt"));
		if (counter == 0 && DebugLevel == 2 && DesignMatrix.Determinant()) writeMatrix(&InvertedDesignMatrix,CFileName ("InvertedConfiguratedDesignMatrix.txt"));		

		//create a new AdditionalExternalEnergyVec
		AdditionalExternalEnergy.clear();
		AdditionalExternalEnergy.resize(size);
					
		//////////2. Calculation of the next iteration if all preprocessing steps were succefull//////////////////////////
		if(calculateAdditionalExternalEnergyVec(&AdditionalExternalEnergy))
		{
			temp_diff  = 0.0f;

			NewSnakeVec.clear();
			NewSnakeVec.resize(size);
			
			//2. calculate the new positions of the vertices
			for(i = 0; i < size; i++)
			{
				for(j = 0; j < size; j++)
				{		
					NewSnakeVec[i].x += (InvertedDesignMatrix.element(i,j) * ((*this->pSnakeVector)[j].getCoefficientsX() + AdditionalExternalEnergy[j].x));
					NewSnakeVec[i].y += (InvertedDesignMatrix.element(i,j) * ((*this->pSnakeVector)[j].getCoefficientsY() + AdditionalExternalEnergy[j].y));			
				}

				temp_diffX = (NewSnakeVec[i].x - (*this->pSnakeVector)[i].getX());
				temp_diffY = (NewSnakeVec[i].y - (*this->pSnakeVector)[i].getY());
				temp_diff  += sqrt((temp_diffX * temp_diffX) + (temp_diffY * temp_diffY));
			}

			//////////3.Calculate the stop criteria//////////////////////////
			d_XY = temp_diff/size;
			
			if (DebugLevel == 1 || DebugLevel == 2) writeReport(counter, d_XY, &NewSnakeVec);
			//if (DebugLevel == 2) writeGrayValues(counter, d_XY, &NewSnakeVec);
			if (DebugLevel == 2) writeCalculationValues(counter, &NewSnakeVec, &AdditionalExternalEnergy);

			counter++;
		}
		else return false;

		} while ( (d_XY > this->extractionParameters.getStopCriteria()) && (counter < 1000));// && ret == true);

	//analysis of the snake calculation
 	if(counter == 1000)
	{
		if (this->listener)
		{
			CCharString str("Error during the snake calculation - snakes does not converge");
			if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
			this->listener->setTitleString(str.GetChar());
			this->listener->setProgressValue(100.0);
		}
	}	
	
	/*else*//*if (!ret)
	{
		if (this->listener)
		{
			CCharString str("Error during the snake calculation");
			if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
			this->listener->setTitleString(str.GetChar());
			this->listener->setProgressValue(100.0);
		}
	}
	else
	{*/
		if (this->listener)
		{
			CCharString str("Snake calculation was successful");
			if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
			this->listener->setTitleString(str.GetChar());
			this->listener->setProgressValue(100.0);
	//	}

		snakeStartingPoints->RemoveAll();
	
		for(unsigned int k = 0; k < NewSnakeVec.size(); k++) snakeStartingPoints->Add(NewSnakeVec[k]);
		
		if (DebugLevel == 1 || DebugLevel == 2)
		{
			CFileName fnVecEnd("FinalSnakePoints.txt");
			fnVecEnd.SetPath(protHandler.getDefaultDirectory());
			snakeStartingPoints->writeXYPointTextFile(fnVecEnd, int (1));
		}
	}
	
	return(ret);

};


//////////////////////////////////////////////////////////////////////////////////////////
void CSnakeExtractor::setRoi(CImageBuffer *troi)
{
	featRoi= troi;
	this->set(featRoi->getOffsetX(), featRoi->getOffsetY(), featRoi->getWidth(), featRoi->getHeight(), 1, 8, true, HOMOGENEOUS);
	GaussianImage.set(offsetX, offsetY, width, height, 1, 32, true, 0.0f);
};

//////////////////////////////////////////////////////////////////////////////////////////
void CSnakeExtractor::resetRoi()
{
	this->featRoi=NULL;
};

//////////////////////////////////////////////////////////////////////////////////////////
bool CSnakeExtractor::SmoothImages()
{
	bool ret;

	if (DebugLevel == 1 || DebugLevel == 2)	
	{
		CFileName fnImg("img.tif");
		fnImg.SetPath(protHandler.getDefaultDirectory());
		this->featRoi->exportToTiffFile(fnImg.GetChar(),0, false);
	}

	//calculation of the Gaussian smooth image
	if(this->extractionParameters.getSigma() > 0.0f)
	{
		CImageFilter filter(eGauss, 3, 3, this->extractionParameters.sigma);		//define new filter
		margin = filter.getOffsetX();												//set new margin
		ret = filter.convolve(*featRoi, this->GaussianImage, false);				//filter the image
		this->GaussianImage.SetToIntensity();										//transform to intensity image
	}
	
	if (DebugLevel == 1 || DebugLevel == 2)	
	{
		CFileName fn("GaussianSmoothnessImage.tif");
		fn.SetPath(protHandler.getDefaultDirectory());
		this->GaussianImage.exportToTiffFile(fn.GetChar(),0, false);
	}

	return(ret);
}

//////////////////////////////////////////////////////////////////////////////////////////
bool CSnakeExtractor::CalculateDerivations()
{
	bool ret; 

	//calculation of the derivations in X and Y
	CImageFilter filterX(eDiffX, 3, 1, 0.0f);
	ret = filterX.convolve(this->GaussianImage, *(this->pGradXImage),false);

	CImageFilter filterY(eDiffY, 3, 1, 0.0f);
	ret = filterY.convolve(this->GaussianImage, *(this->pGradYImage),false);

	if (DebugLevel == 1 || DebugLevel == 2)	
	{
		CFileName fnX("gradXImage.tif");
		CFileName fnY("gradYImage.tif");
		fnX.SetPath(protHandler.getDefaultDirectory());
		fnY.SetPath(protHandler.getDefaultDirectory());
		pGradYImage->exportToTiffFile(fnX.GetChar(),0, true);
		pGradYImage->exportToTiffFile(fnY.GetChar(),0, true);
	}
	
	return (ret);

};

//////////////////////////////////////////////////////////////////////////////////////////
bool CSnakeExtractor::calculateConfiguratedDesignMatrix(Matrix *DesignMatrix)
{	
	bool ret = false;
	double beta = this->extractionParameters.getBeta();
	double alpha = this->extractionParameters.getAlpha();
	double gamma = this->extractionParameters.getGamma();

	int n = DesignMatrix->Nrows()-1;

	if(this->pSnakeVector->getSize() < 5)
	{
		if (this->listener)
		{
			CCharString str("Error during the snake calculation. Possible reason: selected distance between the vertices is too big.");
			if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
			this->listener->setTitleString(str.GetChar());
			this->listener->setProgressValue(100.0);
		}

		ret = false;				
	}
	else if(this->extractionParameters.openSnake == true)
	{
		double temp1 = -alpha - 4*beta;
		double temp2 = 2*alpha + 6*beta + gamma;
		
		//first two lines of the design matrix
		DesignMatrix->element(0,0) = alpha + beta + gamma;
		DesignMatrix->element(0,1) = - alpha - 2*beta;
		DesignMatrix->element(0,2) = beta;
		DesignMatrix->element(1,1) = -alpha - 2*beta + gamma;
		DesignMatrix->element(1,2) = 2*alpha + 5*beta;
		DesignMatrix->element(1,3) = temp1;
		DesignMatrix->element(1,4) = beta;

		for (int i = 2; i < n-1; i++)
		{
			DesignMatrix->element(i,i-2) = beta;
			DesignMatrix->element(i,i-1) = temp1;
			DesignMatrix->element(i,i) = temp2;
			DesignMatrix->element(i,i+1) = temp1;
			DesignMatrix->element(i,i+2) = beta;
		}

		//last two lines of the design matrix
		DesignMatrix->element(n-1,n-4) = beta;
		DesignMatrix->element(n-1,n-3) = temp1;
		DesignMatrix->element(n-1,n-2) = 2*alpha + 5*beta ;
		DesignMatrix->element(n-1,n-1) = -alpha -2*beta + gamma;
		DesignMatrix->element(n,n-2) = beta;
		DesignMatrix->element(n,n-1) = -alpha -2*beta;
		DesignMatrix->element(n,n) = alpha + beta + gamma;

		ret = true;
	}
	else if(this->extractionParameters.openSnake == false)
	{
		double temp1 = -alpha - 4*beta;
		double temp2 = 2*alpha + 6*beta + gamma;

		//first two lines of the design matrix
		DesignMatrix->element(0,0) = temp2;
		DesignMatrix->element(0,1) = temp1;
		DesignMatrix->element(0,2) = beta;
		DesignMatrix->element(0,n-1) = beta;
		DesignMatrix->element(0,n) = temp1;
		DesignMatrix->element(1,0) = temp1;
		DesignMatrix->element(1,1) = temp2;
		DesignMatrix->element(1,2) = temp1;
		DesignMatrix->element(1,3) = beta;
		DesignMatrix->element(1,n) = beta;

		for (int i = 2; i < n-1; i++)
		{
			DesignMatrix->element(i,i-2) = beta;
			DesignMatrix->element(i,i-1) = temp1;
			DesignMatrix->element(i,i)   = temp2;
			DesignMatrix->element(i,i+1) = temp1;
			DesignMatrix->element(i,i+2) = beta;
		}

		//last two lines of the design matrix
		DesignMatrix->element(n-1,0)   = beta;
		DesignMatrix->element(n-1,n-3) = beta;
		DesignMatrix->element(n-1,n-2) = temp1;
		DesignMatrix->element(n-1,n-1) = temp2 ;
		DesignMatrix->element(n-1,n)   = temp1;
		DesignMatrix->element(n,0)     = temp1;
		DesignMatrix->element(n,1)     = beta;
		DesignMatrix->element(n,n-2)   = beta;
		DesignMatrix->element(n,n-1)   = temp1;
		DesignMatrix->element(n,n)     = temp2;

		ret = true;
	}
	
	return ret;
};
//////////////////////////////////////////////////////////////////////////////////////////

bool CSnakeExtractor::calculateAdditionalExternalEnergyVec(vector<CXYPoint> *AdditionalExternalEnergy)
{
	bool ret = false;
	unsigned int size = AdditionalExternalEnergy->size();

	if(this->extractionParameters.getMode() == eTraditional) ret = true;	//no additional term for traditionel snakes

	else if (this->extractionParameters.getMode() == eBalloon)				//Balloon snakes
	{
		//1.BMatrix for the calculation of the additional energy
		Matrix B(size,size);
		B= 0.0;
		int n = B.Nrows()-1;

		if (this->extractionParameters.getOpenSnakeMode() == true)
		{
			//first line of the design matrix
			B.element(0,0) = -1.0f;
			B.element(0,1) = 1.0f;
			
			for (int i = 1; i < n; i++)
			{
				B.element(i,i-1) = -1.0f;
				B.element(i,i+1) = 1.0f;
			}

			//last line of the design matrix
			B.element(n,n-1) = -1.0f;
			B.element(n,n) = 1.0f;
		}
		else if (this->extractionParameters.getOpenSnakeMode() == false)
		{
			//first line of the design matrix
			B.element(0,1) = 1.0f;
			B.element(0,n) = -1.0f;
			
			for (int i = 1; i < n; i++)
			{
				B.element(i,i-1) = -1.0f;
				B.element(i,i+1) = 1.0f;
			}

			//last line of the design matrix
			B.element(n,0)   = 1.0f;
			B.element(n,n-1) = -1.0f;
		}

		//2.Calculation of the external energy
		vector<CXYPoint> q_help(size);					//vector for the new points (X and Y)
		vector<double> pmag(size, 0.0f);
		unsigned int i,j;
		float gamma = this->extractionParameters.getGamma();
		float BalloonForce = this->extractionParameters.getKa();

		for (i = 0; i < size; i++)
		{
			for (j = 0; j < size; j++)
			{
				q_help[i].x += (B.element(i,j) * (*this->pSnakeVector)[j].getX());
				q_help[i].y += (B.element(i,j) * (*this->pSnakeVector)[j].getY());
			}
	
			pmag[i] = sqrt ((q_help[i].x * q_help[i].x) + (q_help[i].y * q_help[i].y));
			
			(*AdditionalExternalEnergy)[i].x =  ((q_help[i].y) / pmag[i] * BalloonForce);
			(*AdditionalExternalEnergy)[i].y = -((q_help[i].x) / pmag[i] * BalloonForce);

			/*(*AdditionalExternalEnergy)[i].x = -((q_help[i].y) / pmag[i] * BalloonForce);
			(*AdditionalExternalEnergy)[i].y =  ((q_help[i].x) / pmag[i] * BalloonForce);*/
		}
		
		ret = true;
	}
	//else if (this->extractionParameters.getMode() == eGVF) ret = true;  //GVF snakes are still to come
	else ret = false;

	return(ret);
}

//////////////////////////////////////////////////////////////////////////////////////////
void CSnakeExtractor::writeMatrix(Matrix *DesignMatrix, CFileName fn)
{
	fn.SetPath(protHandler.getDefaultDirectory());
	ofstream oFile(fn.GetChar());

	oFile.setf(ios::fixed, ios::floatfield);
	oFile.precision(3);

	int n = this->pSnakeVector->getSize();

	if (n < 1 )
	{
		oFile << "empty matrix";
	}
	else 
	{
		for (int i = 0; i < n; i++)
		{
			oFile << "row: ";
			oFile.width(5);
			oFile << i;
			int j = 0;

			for (;j < n; j++)
			{	
				oFile.width(5);
				oFile << "  ";
				oFile <<  DesignMatrix->element(i,j);
			}
			oFile << endl;
		}
		oFile << endl;
	}

};

//////////////////////////////////////////////////////////////////////////////////////////
void CSnakeExtractor::writeReport(int counter, double d_XY, vector<CXYPoint> *SnakeVec)
{
	CCharString outputIndexStr;
	
	ostrstream oss;
	if (counter < 1000) oss << "0";
	if (counter < 100)  oss << "0";
	if (counter < 10)   oss << "0";
			
	oss << counter << ends;
	char *z = oss.str();
	outputIndexStr = z;
	delete [] z;	

	ofstream filename(protHandler.getDefaultDirectory() + "Report_" + outputIndexStr + "_Iteration" + ".txt");
	
	filename.setf(ios::fixed, ios::floatfield);
	filename.precision(3);

	for (unsigned int i = 0; i < SnakeVec->size(); ++i)
	{
		filename << "Point ";
		filename.width(5);
		filename << i;
		filename.width(5);
		filename << " x: " << (*this->pSnakeVector)[i].getX();
		filename.width(5);
		filename << " y: " << (*this->pSnakeVector)[i].getY();
		filename.width(5);
		filename << " diffX: " << (*SnakeVec)[i].x - (*this->pSnakeVector)[i].getX(); 
		filename.width(5);
		filename << " diffY: " << (*SnakeVec)[i].y - (*this->pSnakeVector)[i].getY() << endl;
	}

		filename << "\n\n d_XY: " << d_XY << endl;

};

//////////////////////////////////////////////////////////////////////////////////////////

void CSnakeExtractor::writeGrayValues(int counter, double d_XY, vector<CXYPoint> *SnakeVec)
{
	CCharString outputIndexStr;
	
	ostrstream oss;
	if (counter < 1000) oss << "0";
	if (counter < 100)  oss << "0";
	if (counter < 10)   oss << "0";
			
	oss << counter << ends;
	char *z = oss.str();
	outputIndexStr = z;
	delete [] z;	

	ofstream filename2(protHandler.getDefaultDirectory() + "GradValues_" + outputIndexStr + "_Iteration" + ".txt");
	
	C2DPoint GradValues;
	filename2.setf(ios::fixed, ios::floatfield);
	filename2.precision(3);

	for (unsigned int j = 0; j < SnakeVec->size(); ++j)
	{
		GradValues = getGradValues((long) (*this->pSnakeVector)[j].getX(), (long) (*this->pSnakeVector)[j].getY());

		filename2 << " x: " << (long) (*this->pSnakeVector)[j].getX();
		filename2.width(5);
		filename2 << " y: " << (long) (*this->pSnakeVector)[j].getY();
		filename2.width(5);
		filename2 << " gradX: " << GradValues.x;
		filename2.width(5);
		filename2 << " gradY: " << GradValues.y << endl ;
	}
};
//////////////////////////////////////////////////////////////////////////////////////////

void CSnakeExtractor::writeCalculationValues(int counter, vector<CXYPoint> *SnakeVec, vector<CXYPoint> *AdditionalExternalEnergy)
{
	CCharString outputIndexStr;
	
	ostrstream oss;
	if (counter < 1000) oss << "0";
	if (counter < 100)  oss << "0";
	if (counter < 10)   oss << "0";
			
	oss << counter << ends;
	char *z = oss.str();
	outputIndexStr = z;
	delete [] z;	

	ofstream filename2(protHandler.getDefaultDirectory() + "CalculationValues_" + outputIndexStr + "_Iteration" + ".txt");
	
	C2DPoint GradValues;
	filename2.setf(ios::fixed, ios::floatfield);
	filename2.precision(3);

	filename2 << " coeffX:    coeffY:    ExEnergieX:    ExEnergieY:" << endl ;

	for (unsigned int j = 0; j < SnakeVec->size(); ++j)
	{
		GradValues = getGradValues((long) (*this->pSnakeVector)[j].getX(), (long) (*this->pSnakeVector)[j].getY());

		filename2 << (*this->pSnakeVector)[j].getCoefficientsX();
		filename2.width(5);
		filename2 << "/t" << (*this->pSnakeVector)[j].getCoefficientsY();
		filename2.width(5);
		filename2 << "/t" << (*AdditionalExternalEnergy)[j].x;
		filename2.width(5);
		filename2 << "/t" << (*AdditionalExternalEnergy)[j].y << endl ;
	}
};
//////////////////////////////////////////////////////////////////////////////////////////
C2DPoint CSnakeExtractor::getGradValues(long x, long y)
{
	C2DPoint values;

	long IndexGradValues = this->pGradXImage->getIndex(y-this->offsetY, x-this->offsetX);
	values.x = this->pGradXImage->pixFlt(IndexGradValues);
	values.y = this->pGradYImage->pixFlt(IndexGradValues);

	return(values);
}

