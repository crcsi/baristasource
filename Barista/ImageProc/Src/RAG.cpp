//this comment is only for test reasons. I (Petra) want to try if the update of Barista in Linux is successful.

#include "RAG.h"

#include "ProtocolHandler.h"
#include "XYZPolyLine.h"

#include "FeatExtractPars.h"
#include "LabelImage.h"
#include "ImageRegion.h"
#include "SegmentAMatrix.h"

//==================================================================================

CRAG::CRAG() : pLabelImage(0), pRegionVector(0), pNeighbourVector(0), pAdjacencyMatrix(0), pBoundaryPixels(0)
{
};

//==================================================================================

CRAG::CRAG(CLabelImage *plabelimage, const CImageBuffer &image, 
		   const CImageBuffer *textureImage, int morphSize) : 
				pLabelImage(plabelimage), pRegionVector(0), 
				pNeighbourVector(0), pAdjacencyMatrix(0), pBoundaryPixels(0)
{
	this->initialise(plabelimage, image, textureImage, morphSize);
};

//==================================================================================

CRAG::CRAG(const CRAG &rag): pLabelImage(rag.pLabelImage),
						pRegionVector(0), pNeighbourVector(0), pAdjacencyMatrix(0), 
						pBoundaryPixels(0)
{
	if (rag.isInitialised())
	{
		this->pRegionVector = new vector<CImageRegion *>(rag.pRegionVector->size(), 0);

		for (unsigned int i = 0; i < rag.pRegionVector->size(); ++i)
		{
			if ((*rag.pRegionVector)[i]) (*(this->pRegionVector))[i] = new CImageRegion(*(*rag.pRegionVector)[i]);
		}
			
		this->pNeighbourVector = new CSegmentNeighbourhoodVector(*rag.pNeighbourVector);
		this->pAdjacencyMatrix = new CSegmentAMatrix(*rag.pAdjacencyMatrix);
		this->pBoundaryPixels = new int[this->pLabelImage->getNGV()];
		memcpy(this->pBoundaryPixels, rag.pBoundaryPixels, this->pLabelImage->getNGV() * sizeof(int)); 

		if ((!this->pNeighbourVector) || (!this->pAdjacencyMatrix) || (!this->pBoundaryPixels)) this->clear();
	}
};

//==================================================================================

CRAG::~CRAG()
{
	this->clear();
};
	
//==================================================================================
	  
bool CRAG::initialise(CLabelImage *plabelimage, const CImageBuffer &image, 
					  const CImageBuffer *textureImage, int morphSize)
{
	bool ret = true;		
	this->clear();

	if (!plabelimage) ret = false;
	else
	{
		if ((plabelimage->getWidth()  != image.getWidth()) || 
			(plabelimage->getHeight() != image.getHeight())) ret = false;
		else
		{
			this->pLabelImage   = plabelimage;
			this->pRegionVector = this->pLabelImage->initRegionVector(image, morphSize);

			if (!this->pRegionVector) ret = false;
			else
			{
				this->pAdjacencyMatrix = new CSegmentAMatrix(this->pLabelImage->getNLabels());
				this->pBoundaryPixels = new int [this->pLabelImage->getNGV()];
				if ((!this->pAdjacencyMatrix) || (!this->pBoundaryPixels)) ret = false;
				else
				{			
					memset(this->pBoundaryPixels, 0, this->pLabelImage->getNGV() * sizeof(int));
					this->pAdjacencyMatrix->calculateFromEdgePixels(*this->pLabelImage, this->pBoundaryPixels);
					this->pNeighbourVector = new CSegmentNeighbourhoodVector;	
					if (!this->pNeighbourVector) ret = false;
					else
					{
						this->pNeighbourVector->calculate(*this->pRegionVector, *this->pAdjacencyMatrix, 
							                              this->pLabelImage->getNLabels());
	
						if (textureImage) 
						{
							for (int k = 0; k < this->pNeighbourVector->getSize(); ++k) 
								(*this->pNeighbourVector)[k].initTexture(this->pBoundaryPixels, *textureImage);
						}

						this->pNeighbourVector->sortAscending(); 
						this->pNeighbourVector->CalculateNNeigh(*this->pRegionVector);


						for(int k = 0; k < this->pNeighbourVector->getSize(); k++) 
						{
							(*this->pNeighbourVector)[k].initContour(*this->pLabelImage, image, this->pBoundaryPixels);
						}
					}
				}

			}

			if (!ret) this->clear();
		}
	}

	return ret;
};

//==================================================================================

void CRAG::clear()
{
	if (this->pRegionVector) 
	{
		for (unsigned int i = 0; i < this->pRegionVector->size(); ++i)
		{
			delete (*(this->pRegionVector))[i];
			(*(this->pRegionVector))[i] = 0;
		}

		delete this->pRegionVector;
		this->pRegionVector = 0;
	}
	
	if (this->pNeighbourVector) delete this->pNeighbourVector;
	this->pNeighbourVector = 0;

	if (this->pAdjacencyMatrix) delete this->pAdjacencyMatrix;
	this->pAdjacencyMatrix = 0;
	if (this->pBoundaryPixels) delete[] this->pBoundaryPixels;
	this->pBoundaryPixels = 0;

	this->pLabelImage = 0;
};

	
//==================================================================================

bool CRAG::merge(const CImageBuffer &img, const CImageBuffer *textureImage, 
				 const FeatureExtractionParameters &extractionPars, bool forAtkis, unsigned long ATKIS_threshold)
{
	bool ret = this->isInitialised();

	if (ret)
	{
		if (outputMode >= eMAXIMUM)
		{
			// just export data for debugging purposes
			this->exportRegions(protHandler.getDefaultDirectory() + "labelsRGB.xyz");
			this->exportContoursToDXF(protHandler.getDefaultDirectory() + "Contours.dxf");
			this->exportAll("start_", img.getOffsetX(), img.getOffsetY());
	
			//print out image and texture 
			img.exportToTiffFile(protHandler.getDefaultDirectory() + "img.tif", 0, false);
			if (textureImage) textureImage->exportToTiffFile(protHandler.getDefaultDirectory() + "textureImage.tif",0,true);
		}

		CSegmentNeighbourhoodVector neighboursCopy;
		int removed = 0;
		int outputIndex = -1;

		do
		{
			removed = 0;
			neighboursCopy = *this->pNeighbourVector;
			outputIndex++;

			if (outputMode >= eMAXIMUM)
			{
				//print out of segment images after each iteration
				exportLabelRegionNeighbours(outputIndex);
			}

			unsigned short region1; 
			unsigned short region2; 

			//merging
			for (int k=0; k < neighboursCopy.getSize();k++) 
			{
				region1 = neighboursCopy[k].getRegionIndex1();
				region2 = neighboursCopy[k].getRegionIndex2();

				if ((neighboursCopy[k].getDistanceMetric() < extractionPars.RegionSimilarityThreshold) && 
					(neighboursCopy[k].getDistanceMetric() > 0) &&
					(neighboursCopy[k].getVarianceRatio() < extractionPars.RegionVarianceRatioThreshold) && 
					(neighboursCopy[k].getBoundaryTexture() <= extractionPars.RegionTextureThreshold) && 
					(neighboursCopy[k].getCountorRegion1DistanceMetric() < extractionPars.RegionSimilarityThreshold) && 
					(neighboursCopy[k].getCountorRegion1DistanceMetric() > 0) && 
					(neighboursCopy[k].getCountorRegion2DistanceMetric() < extractionPars.RegionSimilarityThreshold) && 
					(neighboursCopy[k].getCountorRegion2DistanceMetric() > 0 ))
				{
					neighboursCopy.mergeSegments(k, *this->pLabelImage, this->pBoundaryPixels, 
						                         *this->pAdjacencyMatrix, img, *this->pRegionVector, true);
					this->pNeighbourVector->update(neighboursCopy[k], *this->pAdjacencyMatrix, 
						                           this->pBoundaryPixels, *textureImage);
					neighboursCopy.purgeSegments(k, *this->pAdjacencyMatrix);

					--k;
					k = neighboursCopy.getSize();
					++removed;
				}
			}
		} while (removed > 0);
		

//postprocessing*********************************************************************

		// PlausibilityCheck (will merge labels with a size of 1 pixel if they 
		// are not significant different from their neigbour
		this->plausibilityCheck(3000, img, textureImage);

		//alternativ plausibility check for ATKIS
		if (forAtkis) this->plausibilityCheckATKIS(ATKIS_threshold, img, textureImage);

		this->renumber(img, textureImage, extractionPars.getMorphologicFilterSize());

		if (outputMode >= eMAXIMUM)
		{
			// just export data for debugging purposes
			this->exportAll("final_", img.getOffsetX(), img.getOffsetY());
		}
	}

	return ret;
};
	  
//==================================================================================

void CRAG::renumber(const CImageBuffer &image, const CImageBuffer *textureImage, 
					int morphSize)
{
	this->pLabelImage->renumberLabels();

	this->initialise(this->pLabelImage, image, textureImage, morphSize);
}; 
		  
//==================================================================================

void CRAG::plausibilityCheck(unsigned long nPix,  const CImageBuffer &image,  		   
							 const CImageBuffer *texture)
{
//mergen von 1Pixel grossen elementen, wenn signifikant nicht unterschiedlich von der umgebung
	int removed = 0;

	CSegmentNeighbourhoodVector neighbours = (*this->pNeighbourVector);
	CSegmentNeighbourhoodVector neighboursCopy;

	CSortVector<unsigned short> NeighIdxOfRegion;

	float *colourVector = new float[image.getBands()];

	do
	{
		removed = 0;
		neighboursCopy = neighbours;
				
		for(unsigned short k = 0; k < neighboursCopy.getSize(); k++)
		{
			if((neighboursCopy[k].getRegion1NPixels() == 1 || neighboursCopy[k].getRegion2NPixels() == 1))
			{

				unsigned short region1 = neighboursCopy[k].getRegionIndex1();
				unsigned short region2 = neighboursCopy[k].getRegionIndex2();
				bool merge = false;

				if (neighboursCopy[k].getRegion1NPixels() == 1)
				{
					image.getColourVec(region1, colourVector);
					merge = ((*this->pRegionVector)[region2]->isConsistent(colourVector));	
				}
				else
				{
					image.getColourVec(region2, colourVector);
					merge = ((*this->pRegionVector)[region1]->isConsistent(colourVector));	
				}
			

				if (merge)
				{
					neighboursCopy.mergeSegments(k, *this->pLabelImage, this->pBoundaryPixels, 
						                         *this->pAdjacencyMatrix, image, *this->pRegionVector, true);
					neighbours.update(neighboursCopy[k], *this->pAdjacencyMatrix, this->pBoundaryPixels, *texture);
					neighboursCopy.purgeSegments(k, *this->pAdjacencyMatrix);

					--k;
					k = neighboursCopy.getSize();
					++removed;
				}
			}
		}
	} while (removed > 0);

	delete [] colourVector;

	(*this->pNeighbourVector) = neighbours;
};

	  
//==================================================================================

void CRAG::plausibilityCheckATKIS(unsigned long nPix,  
		                          const CImageBuffer &image, 
								  const CImageBuffer *texture)
{
//mergen von 1Pixel grossen elementen zum element mit der geringsten DistanzMetric + von inselobjekten (nur ein nachbar)
	int removed = 0;

	CSegmentNeighbourhoodVector neighbours = (*this->pNeighbourVector);
	CSegmentNeighbourhoodVector neighboursCopy;

	unsigned short region1;
	unsigned short region2;

	do
	{
		removed = 0;
		neighboursCopy = neighbours;
				
		for(unsigned short k = 0; k < neighboursCopy.getSize(); k++)
		{
			region1 = neighboursCopy[k].getRegionIndex1();
			region2 = neighboursCopy[k].getRegionIndex2();

			int Region1NNeigh = this->getRegion(region1)->getNNeigh();
			int Region2NNeigh = this->getRegion(region2)->getNNeigh();

			if ((neighboursCopy[k].getRegion1NPixels() == 1) || (neighboursCopy[k].getRegion2NPixels() == 1) ||
				(Region1NNeigh == 1) || (Region2NNeigh == 1))
			{
				neighboursCopy.mergeSegments(k, *this->pLabelImage, this->pBoundaryPixels, 
					                         *this->pAdjacencyMatrix, image, *this->pRegionVector, true);
				neighbours.update(neighboursCopy[k], *this->pAdjacencyMatrix, this->pBoundaryPixels, *texture);
				neighboursCopy.purgeSegments(k, *this->pAdjacencyMatrix);

				--k;
				k = neighboursCopy.getSize();
				++removed;
			}
		}
	} while (removed > 0);

	(*this->pNeighbourVector) = neighbours;
};

//==================================================================================

void CRAG::exportAll(const CCharString &filenameBase, int offsetX, int offsetY) 
{
	CCharString pathBase = protHandler.getDefaultDirectory() + filenameBase;
	//print out adjacency matrix
	//matrix.dump(protHandler.getDefaultDirectory() + "start_matrix.txt");

#ifdef _DEBUG
	//print out regionVec
	this->exportRegions(pathBase + "regionVec.txt");

	//print out boundary image
	this->exportBoundaryPixels(pathBase + "Boundary.tif");

	//print our contour image
	exportContoursToTIFF(pathBase + "contourImage.tif", offsetX, offsetY); 
#endif

#ifndef FINAL_RELEASE		
	//print out label imag
	this->pLabelImage->dump(protHandler.getDefaultDirectory(), filenameBase);

	//print neighbourhood vector
	this->pNeighbourVector->dump(filenameBase + "neighbours.txt");
#endif

}; 
  
//============================================================================

void CRAG::exportContoursToDXF(const CCharString &filename) 
{
	if (this->isInitialised())
	{
		ofstream dxf(filename.GetChar());
		if (dxf.good())
		{		
			if (CXYZPolyLine::writeDXFHeader(dxf))
			{
				for (int k = 0; k < this->pNeighbourVector->getSize(); k++) 
				{
					(*this->pNeighbourVector)[k].exportContours(dxf);
				}

				CXYZPolyLine::writeDXFTrailer(dxf);
			}
		}
	}
}; 

//============================================================================

void CRAG::exportRegions(const CCharString &filename) const
{
	if (this->pRegionVector)
	{	
		ofstream regionFile(filename);

		int nReg = 0;
		for (unsigned int k = 1; k < this->pRegionVector->size(); ++k)
		{
			if (this->getRegion(k)) 
			{
				if (this->getRegion(k)->getNPixels() > 0)
				{
					this->getRegion(k)->dump(regionFile);
					++nReg;
				}
			}
		}

		regionFile << "\n\n Total: " << nReg << " non-empty regions";
	}
}; 

//============================================================================

void CRAG::exportLabelRegionNeighbours(int index)
{
	CCharString outputIndexStr;
	if (index != 0)
	
	{
		ostrstream oss;
		oss <<  "_merged_";
		if (index < 1000) oss << "0";
		if (index < 100)  oss << "0";
		if (index < 10)   oss << "0";
			
		oss << index << ends;
		char *z = oss.str();
		outputIndexStr = z;
		delete [] z;	
	}

	this->exportNeighbourhoods(protHandler.getDefaultDirectory() + "Neighbours" + outputIndexStr + ".txt");
	this->pLabelImage->dump(protHandler.getDefaultDirectory().GetChar(), "", outputIndexStr);
	this->exportRegions(protHandler.getDefaultDirectory() + "Regions" + outputIndexStr + ".txt");
};

//============================================================================

void CRAG::exportRegionCentres(const CCharString &filename) const
{
	if (this->pRegionVector)
	{
		ofstream oRgbFile(filename.GetChar());
		oRgbFile.setf(ios::fixed, ios::floatfield);
		oRgbFile.precision(2);

		for (unsigned int i = 1; i < this->pRegionVector->size(); ++i)
		{
			for (int j = 0; j < (*this->pRegionVector)[i]->getBands(); ++j)
			{
				float f = (*this->pRegionVector)[i]->greyLevel(j);
				oRgbFile.width(7);
				oRgbFile << f; 
			}

			oRgbFile << "\n"; 
		};
	}
}; 

//==================================================================================

void CRAG::exportNeighbourhoods(const CCharString &filename) const
{
	if (this->pNeighbourVector)
		this->pNeighbourVector->dump(filename);
}; 

//==================================================================================

void CRAG::exportBoundaryPixels(const CCharString &filename) const
{
	CImageBuffer buf1(0, 0, this->pLabelImage->getWidth(), this->pLabelImage->getHeight(),1,8,true,255);
	for (unsigned int i = 0; i< buf1.getNGV(); i++)
	{	
		if (this->pBoundaryPixels[i] > 0) buf1.pixUChar(i) = 0; 
		else if (this->pBoundaryPixels[i] == -1) buf1.pixUChar(i) = 100;
		else if (this->pBoundaryPixels[i] == -2) buf1.pixUChar(i) = 130;
	}
	
	buf1.exportToTiffFile(filename, 0, false);
}; 

//==================================================================================
	
void CRAG::exportContoursToTIFF(const CCharString &filename, int offsetX, int offsetY)
{
	if (this->pNeighbourVector)
	{
		CImageBuffer contourImage(offsetX, offsetY, this->pLabelImage->getWidth(), 
			                      this->pLabelImage->getHeight(), 1, 8, true, 0.0f);
		for (int i = 0; i < this->getNNeighbours(); ++i)
		{
			this->getNeighbour(i).exportContoursToBuffer(contourImage);
		}

		contourImage.exportToTiffFile(filename, 0, false);
	}
}
