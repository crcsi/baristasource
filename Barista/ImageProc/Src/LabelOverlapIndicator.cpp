//============================================================================
//
// File        : LabelOverlapIndicator.cpp
//
// Author      : FR
//
// Description : Implementation of class CLabelOverlapIndicator, a class
//               describing the overlap between two regions in a 
//               two label images
//
//============================================================================

#include <strstream>
using namespace std;

#include "LabelOverlapIndicator.h"

CLabelOverlapIndicator::CLabelOverlapIndicator(): index(0), nPixels(0), 
						percentage(0.0f), eClassification(eNONE)
{ 
};

//============================================================================

CLabelOverlapIndicator::CLabelOverlapIndicator(const CLabelOverlapIndicator &indicator):
						index(indicator.index), nPixels(indicator.nPixels), 
						percentage(indicator.percentage), 
						eClassification(indicator.eClassification)
{ 
};

//============================================================================

void CLabelOverlapIndicator::init(unsigned long Index, unsigned long NPixels)
{
	index   = Index;
	nPixels = NPixels;
};

//============================================================================

void CLabelOverlapIndicator::finish(unsigned long TotalPixels)
{
	percentage = float(nPixels) / float(TotalPixels);
};

//============================================================================

eOVERLAP CLabelOverlapIndicator::classify(float NoneVsWeak, float WeakVsPartial, 
			  		                      float PartialVsStrong)
{
	if (percentage < NoneVsWeak) eClassification = eNONE;
	else if (percentage < WeakVsPartial) eClassification = eWEAK;
	else if (percentage < PartialVsStrong) eClassification = ePARTIAL;
	else eClassification = eSTRONG;

	return eClassification;
};

//============================================================================

CCharString CLabelOverlapIndicator::getString(bool showNone) const
{
	CCharString str;
	if ((eClassification != eNONE) || showNone)
	{
		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(1);
		oss.width(5); 
		oss << index << " (";
		oss.width(7);
		oss << nPixels  << " pixels / ";
		oss.width(5);
		oss << percentage * 100.0f  << " % / ";

		switch (eClassification)
		{	
			case eNONE:    oss<< "none";    break;
			case eWEAK:    oss<< "weak";    break;
			case ePARTIAL: oss<< "partial"; break;
			case eSTRONG:  oss<< "strong";  break;
			default:       oss<< "none";    break;
		};

		oss<< ")" << ends;

		char *zStr = oss.str();

		str = zStr;
		delete[] zStr;
	};
	return str;
};
