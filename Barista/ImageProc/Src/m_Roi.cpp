#include <math.h>
#include "m_Roi.h"

m_Roi::m_Roi() : backupBuf(NULL)
{
	mod_y=0; // full image size in x 

	direc_flag=DIREC_OFF;
	pix_flag=CORR_GVALUES;
	
	filtered=FIL_NONE;	
    rad_mean=0.0;	
    rad_sdev=0.0;
	type = ROI_OWNMEM;
	m_errval=M_OK;
	_coeff[0]=0.0;	_coeff[1]=1.0; 	_coeff[2]=0.0;
	_coeff[3]=0.0;	_coeff[4]=0.0; 	_coeff[5]=1.0;
	_coeff_inv[0]=0.0;	_coeff_inv[1]=1.0; 	_coeff_inv[2]=0.0;
	_coeff_inv[3]=0.0;	_coeff_inv[4]=0.0; 	_coeff_inv[5]=1.0;
	_rot_angle=0.0;

	enable=true;
}

m_Roi::~m_Roi()
{
	deleteBackupBuffer();
}

void m_Roi::deleteBackupBuffer()
{
	if (backupBuf != NULL)
	{	
		delete [] backupBuf;
		backupBuf = NULL;
	}
}


int m_Roi::storeOriginalBuffer()
{
	int ret = M_OK;
	if (this->backupBuf != NULL)
		delete [] backupBuf;

	backupBuf = new CImageBuffer(*this);
	if (backupBuf == NULL) ret = M_ERROR;

	return ret;
}
int m_Roi::restoreOriginalBuffer()
{
	int ret = M_OK;

	if (backupBuf != NULL)
	{
		ret = this->set(*backupBuf);
	}

	return ret;
}

/*** FILTER INTERFACE ********************************************************/
/*** filter handler: lsm_filter ***/
/*
int	m_Roi::m_filter (int filt_fn,int size)

{
	//int		filt_fn;		// filter function to use	     
	//int		 size;			// size of filter (1=3x3, 2=5x5 etc) 

     int		 ret;			// value to return		     
    
    
    // immediately return if filter size is zero or already filtered 
    
    if (size == FIL_ZERO ||filtered == FIL_DONE) return M_OK;
    
    // in this implementation, we wish to restrict the practice of radiometric
    //   operations on true rois (subimages), and hence it is checked that the
    //   input roi is a complete image in itself 
    
    if (type == ROI_OWNMEM && mod_y == width)
        {
		//maria 04.09.01
		if(filt_fn==FIL_LOWPASS){
			if (m_pr_imp_lowpass(size) == M_OK)
            {
			   filtered = FIL_DONE;
				ret = M_OK;
			}
			else
			{
				m_errval = M_NOFILTER;
				ret = M_ERROR;
            }
		}
        }
    else
        {
        m_errval = M_SUBIMAGE;
        ret = M_ERROR;
        }
    
    return ret;
}

int	m_Roi::m_pr_imp_lowpass (int size)
{
    // imp_lowpass operates only on full images 
    int m_errval;
    if (mod_y != width)
        {
        m_errval = M_NOFILTER;
        return M_ERROR;
        }
    
    
    if (imp_lowpass (pixF,width,height,size) < 0)
        {
        m_errval = M_NOFILTER;
        return M_ERROR;
        }
    
    return M_OK;
}

int	m_Roi::imp_lowpass (float	*pix,int sx,int sy,int nf)
{
     float	*pix1, *pix2;	// pointers to pixel values	 
     short	*bp1, *bp2;	// pointers to 'buf' values	     
     short	 sum;		// running sum of grey values	     
     float	*rpix;		// pointer to row-start pixels	 
     short	*cbp;		// pointer to col-start 'buf' entries
     float	*end1, *end2;	// for end of loops thru pixels	 
     short	*bend;		// for end of loops thru buffer	     
    short		*buf;		// buffer of row summations	     
    short		 nd, nq;	// dim of filter; total pixels in it 
    int		 	 npix;		// no of pixels in image	     
    unsigned int	 nbyte;		// bytes to allocate memory for	     
    
    
    // allocate memory for buffer 
    
    npix = sx * sy;
    
    nbyte = (unsigned int) (npix * sizeof (short));
    
    if ((buf = (short*) malloc (nbyte)) == NULL) return (-1);
    
    // get size of filter 
    
    nd = nf * 2 + 1;
    nq = nd * nd;
    
    // sum along the rows 
    
    end1 = pix + npix;
    
    for (rpix = pix, bp1 = buf; rpix < end1; rpix += sx, bp1 += sx)
        {
        sum = 0;
        bp2 = bp1 + nf;
        end2 = rpix + nd;
        
        for (pix1 = rpix; pix1 < end2; pix1++) sum += (short) *pix1;
        
        *bp2++ = sum;
        end2 = rpix + sx;
        
        for (pix1 = rpix+nd, pix2 = rpix; pix1 < end2; pix1++, pix2++, bp2++)
            {
            sum += (short) (*pix1 - *pix2);
            *bp2 = sum;
            }
        }
    
    // sum the "summations along the rows" down through the columns 
    
    end1 = pix + sx - nf;
    
    for (pix1 = pix+nf, cbp = buf+nf; pix1 < end1; pix1++, cbp++)
        {
        sum = 0;
        pix2 = pix1 + nf*sx;
        bend = cbp + sx*nd;
        
        for (bp1 = cbp; bp1 < bend; bp1 += sx) sum += *bp1;
        
        *pix2 = (float) (sum/nq);
        bend = cbp + npix;
        
        for (bp1 = cbp+sx*nd, bp2 = cbp; bp1 < bend; bp1 += sx, bp2 += sx)
            {
            sum += (*bp1 - *bp2);
            pix2 += sx;
            *pix2 = (float) (sum/nq);
            }
        }
    
    free ((char*) buf);
    
    
    return (1);

}

*/
void m_Roi::m_enhance(float *buf_filt,WallisParam param)
{
	 /* Local variables */
    static int icxb, icyb, rang, idum;
    static int xlas, xfir, yfir, ylas;
    static double  av,std;
    static int idum1, i4dum, i4dum1;
    static double a, b, c, d;
    static int i, j, k, l, m, range;
    static double *xcoef_1,*xcoef_2,*xcoef_3,*xcoef_4;	/* was [3038][2][2] */;
    static double *ycoef_1,*ycoef_2,*ycoef_3,*ycoef_4;	/* was [3038][2][2] */;
	static double *par_r1,*par_r0;	/* was [3037][3037][2] */;
    static int ncoex, ncoey,ncoebx, ncoeby;
    static double incrx,incry;
    static int i1, j1, j2, k1, i2, m1, l1,grix,griy;
    static double x1, x2, y1, y2;
    static int gain_method,method;
    static double x11, x12, x21, x22;
    static double magain;
   // static int nx, ny;
    static double tr[2];
    static double gridbx, gridby, incrbx, incrby;
    static int id1,id2,id3,maxint, minint, sizebx, sizeby;
	
    static double dum1,dum,sum;
    static int npi, nxb, nyb;
    static double par1;
    static int nxb2, nyb2;
	
	int bins;

	
	
	if (param.sizex % 2 == 1) {
	    icxb = 1;
	} else {
	    icxb = 0;
	}
	if (param.sizey % 2 == 1) {
	    icyb = 1;
	} else {
	    icyb = 0;
	}

// 	maximum gain when std. dev. of block pixels = 0 
	magain = 0.;

//	magain = 0 means that 
// 	ii) for Wallis filter 
// 		gain =0, offset = bfc * desired average + (1-bfc) * average 
// 				 of block 
// 		bfc = brightness forcing constant

	if (param.gain_method != 0 && param.gain_method != 1) {
	    param.gain_method = 0;
	}
	
	bins=(int)pow(2.0,param.bits);
	
// 	for Wallis filter 
	if(param.method==0){
	    if (param.mean< 0. || param.mean> bins-1.) {
		 
		 fprintf(stderr,"Average outside range [0,%d] repeat!",bins-1);
		 return;
		
	    }

	    if(param.std<= 0. || param.std > (bins-1)/2) {
			
			 fprintf(stderr,"Standard deviation outside range (0,%d]",(bins-1)/2);
			
	    }
	}else{
		fprintf(stderr,"Not active method!\n");
		 return;
		
	   
	} 
/* 	constants to be used for the determination of offset,gain */

	    a = param.bfc * param.mean;
	    b = 1. - param.bfc;
	    c = param.cec * param.std;
	    d = (1. - param.cec) * param.std;

		
//start processing
//	nx= this->nx;
//	ny=in_lines;


	grix=param.sizex/2;
	griy=param.sizey/2;
	xcoef_1=(double *) calloc((grix), sizeof (double));
	xcoef_2=(double *) calloc((grix), sizeof (double));
	xcoef_3=(double *) calloc((grix), sizeof (double));
	xcoef_4=(double *) calloc((grix), sizeof (double));
	ycoef_1=(double *) calloc((griy), sizeof (double));
	ycoef_2=(double *) calloc((griy), sizeof (double));
	ycoef_3=(double *) calloc((griy), sizeof (double));
	ycoef_4=(double *) calloc((griy), sizeof (double));
	
// 	number of blocks in x and y 
// 	size of blocks and grid spacing for right, bottom border 
// nx  (image), sizex (block), distance (centers of blocks) 

	

	nxb =(int) (this->width - param.sizex) / grix;  // all integers, the result has decimal numbers but we get an int instead
    
	if (nxb * grix == this->width - param.sizex) {
		nxb++;
		sizebx = param.sizex;
		gridbx = (double) grix;
    } else {
		nxb += 2;
		sizebx = this->width - (nxb - 1) * grix; // compute the x size of the last tile
		idum = param.sizex - sizebx;		
		gridbx = grix - (double)(idum / 2.);  // distance from centers between the last pair of tiles
    }
    nyb = (int)(this->height - param.sizey) / griy;
    if (nyb * griy == this->height - param.sizey) {
		nyb++;
		sizeby = param.sizey;
		gridby = (double) griy;
    } else {
		nyb += 2;
		sizeby = this->height - (nyb - 1) * griy;
		idum = param.sizey - sizeby;
		gridby = griy - (double)(idum / 2.);
    }
    
	par_r0=(double *) calloc((nxb*nyb), sizeof (double));
	par_r1=(double *) calloc((nxb*nyb), sizeof (double));

		
/* 	find gain and offset for each block */

    j1 = -griy;  // y start of tile
    j2 = param.sizey - griy; //y end of tile
    for (k = 0; k < nyb; k++) { //number of tiles in y direction
		j1 += griy; // start from 0 for the first loop
		if (k != nyb-1) {
			j2 += griy;  
			id2 = param.sizey;
		}else {
			j2 = this->height;
			id2 = sizeby;
		}
		i1 =-grix;	//x start of tile
		i2 = param.sizex - grix; // x end of tile
		for (l = 0; l < nxb; l++) { // number of tiles in x direction
			
			i1 += grix;
			if (l != nxb-1) {
				i2 += grix;
				id3 = param.sizex;
			} else {
				i2 = width;
				id3 = sizebx;
			}
			

// 	offset, gain for Wallis filter 

//	first compute average, std. dev. of grey values 

			npi = id2 * id3;// number of pixels in the tile
			sum = 0.;
			dum1 = 0.;
			for (j = j1; j < j2; j++) {
				for (i = i1; i < i2; i++) {
					id1 = (int) pixF[i + j * width];
					sum += id1;
					dum1 += id1 * id1;
				}
			}
			av = sum / npi;  // average
// Computing 2nd power
			dum1 -= npi * (av * av);
			i4dum = npi - 1;
			if (i4dum != 0) {
				dum1 = sqrt(dum1 /(double) (npi - 1));
				std = dum1;
			}else {
				std = (float)0.;
			}
			if (std != 0.) {
				if (std != param.std) {
					dum = c / (param.cec * std + d);
					par_r1[l + k * nxb] = dum;
					par_r0[l + k * nxb] = a + (b - dum) * av;
				}else {

// 	violation of formula, but it is logical, i.e. gain = 1 
// 	when the two standard deviations are equal. 

					par_r1[l + k * nxb] = (float)1.;
					par_r0[l + k * nxb] = a - param.bfc * av;
				}
			}else {
				if (gain_method == 1) { // average gain
					if (l != 0) {
						if (k != 0) {

// 	not first row, not first column -> average
// of 3 neighbours 
// 	Rows and columns refer to the image blocks


							k1 = k - 1;
							l1 = l - 1;
							dum = (par_r1[l + k1 * nxb]+ par_r1[l1 + k1 * nxb]+ par_r1[l1 + k * nxb]) / 3.;
						}else {
//	not first row, first column 
							l1 = l - 1;
							if (nyb > 1) {
// 	average of 2 neighbours 
								dum = (par_r1[l1 + 1 * nxb] + par_r1[l1 + 2 * nxb]) / 2.;
							}else {
// 	equal to the top neighbour 
								dum = par_r1[l1 + 1 * nxb];
							}
						}
					}else {
							if (k != 0) {
// 	first row, not first column 
								dum = par_r1[l + (k-1) * nxb];
							}else {
// 	first row, first column 
								dum = magain;
							}
					}
				}else if (param.gain_method == 0) { // for zero gain
					dum = magain;
				}
				par_r1[l + k * nxb] = dum;
				par_r0[l + k * nxb] = a + (b - dum) * av;
			} // end if std =0
		}//end number of tiles in x direction
	}// end number of tiles in y direction


/*
	FILE* out= fopen("r1_m.txt","w+");

	for (int i=0; i < nyb; i++)
	{
		for (int j=0; j < nxb; j++)
		{
			fprintf(out,"%7.2lf",par_r1[i*nxb+j]);
		}
		fprintf(out,"\n");
	}

	fclose(out);
*/
// 	if only one block make point transformation for all pixels 

    if (nxb == 1 && nyb == 1) {
	 for (j = 0; j < height; j++) {
	    for (i = 0; i < width; i++) {
			if (par_r1[0] != (float)1.) {
				dum = par_r1[0] * pixF[i+j*width];
			}else {
				dum = (double) (pixF[i+j*width]);
			}
			if (par_r0[0] != (float)0.) {
				dum += par_r0[0];
			}
			i4dum = IROUND(dum);
			if (param.clipp == 1) { // for clipping
				if (i4dum < 0) {
					buf_filt[i + j *width] = 0;
				}else if (i4dum > bins-1) {
					buf_filt[i + j * width] = (float) (bins-1);
				}else {
					buf_filt[i + j * width] =(float) i4dum;
				}
			}else {
				buf_filt[i + j * width] = (float)i4dum;
			}
	    }
	 }
	
    }else{

// 	Compute linear interpolation coefficients in x and y direction 
//	and separately for the inner part and its right,bottom border 

//	coefficients in x, inner part
		

		incrx = 1. / (double)grix;
		ncoex = grix;
		if (icxb == 0) {
			dum = (double)grix - (float).5;// 	even x block size
		}else {
			dum = (double)grix;		// 	odd x block size 
		}
		xcoef_1[0] = dum / (double)grix;
		xcoef_2[0] = 1. - xcoef_1[0];
		
		for (i = 1; i < grix; i++) {
			i1 = i - 1;
			xcoef_1[i] = xcoef_1[i1] - incrx;// 	coefficients for left neighbour
			xcoef_2[i] = 1. - xcoef_1[i];//	coefficients for right neighbour 
		}

//	coefficients in y , inner part 
		incry = 1. / (double)griy;
		ncoey=griy;
		if (icyb == 0) {
			dum = (double)griy - (float).5;// 	even y block size 
		}else{
			dum = (double)griy;// 	odd y block size
		}
		ycoef_1[0] = dum / (double)griy;
		ycoef_2[0] = 1. - ycoef_1[0];

		for (i = 1; i < griy; i++) {
			i1 = i - 1;
			ycoef_1[i] = ycoef_1[i1] - incry;//	coefficients for top neighbour 
			ycoef_2[i] = 1. - ycoef_1[i];//	coefficients for bottom neighbour 
		}

// 	coefficients of x , right border of inner part 

		incrbx = 1. / gridbx;
		if (icxb == 0) {
			dum = gridbx - (float).5;
			ncoebx = IROUND(gridbx - (float).7) + 1;
		}else {
			dum = gridbx;
			ncoebx = IROUND(gridbx - (float).3) + 1;
		}
		xcoef_3[0] = dum / gridbx;
		for (i = 1; i <ncoebx;i++) {
			i1 = i - 1;
			xcoef_3[i] = xcoef_3[i1] - incrbx;
		}
		for (i = 0; i < ncoebx; i++) {
			xcoef_4[i] = 1. - xcoef_3[i];
		}

// 	coefficients of y, bottom border of inner part 

		incrby = 1. / gridby;
		if (icyb == 0) {
			dum = gridby - (float).5;
			ncoeby = IROUND(gridby - (float).7) + 1;
		} else {
			dum = gridby;
			ncoeby = IROUND(gridby - (float).3) + 1;
		}
		ycoef_3[0] = dum / gridby;
		for (i = 1; i < ncoeby; i++) {
			i1 = i - 1;
			ycoef_3[i] = ycoef_3[i1] - incrby;
		}

		for (i = 0; i < ncoeby; i++) {
			ycoef_4[i] = 1. - ycoef_3[i];
		}
//		printf("\nncoex,ncoey,ncoebx,ncoeby");
//		printf("\t%d %d %d %d",ncoex,ncoey,ncoebx,ncoeby);
    
// 	First process the corners. No extrapolation

// 	top left corner 

	    xfir =(int)(param.sizex / 2);
	    yfir =(int)(param.sizey / 2);
		dum = (double) param.sizex / 2. + (nxb - 2) * grix + gridbx + (float).3;
//		xlas = IROUND(dum) + 1;
		xlas = IROUND(dum);
	    dum = (double) param.sizey / 2. + (nyb - 2) * griy + gridby + (float).3;
//	    ylas = IROUND(dum) + 1;
	    ylas = IROUND(dum);

//	if(line==0){
		for (j = 0; j <yfir; j++) {
			for (i = 0; i < xfir; i++) {
				if (par_r1[0] != (float)1.) {
					dum = par_r1[0] * pixF[i + j * width];
				} else {
					dum = (double) (pixF[i + j * width]);
				}
				if (par_r0[0] != (float)0.) {
					dum += par_r0[0];
				}
				i4dum = IROUND(dum);
				if (param.clipp == 1) {
					if (i4dum < 0) {
						buf_filt[i + j * width] = 0;
					} else if (i4dum > bins-1) {
						buf_filt[i + j * width] = (float)bins-1;
					} else {
						buf_filt[i + j * width] =(float) i4dum;
					}
				}else {
					buf_filt[i + j * width] = (float)i4dum;
				}
			}
		}

// 	top right corner 

		for (i = xlas; i < width; i++) {
			for (j = 0; j < yfir; j++) {
				par1 = par_r1[(nxb-1)];
				if (par1 != (float)1.) {
					dum = par1 * pixF[i + j * width];
				} else {
					dum = (double) (pixF[i + j * width]);
				}
				par1 = par_r0[(nxb -1)];
				if (par1 != (float)0.) {
					dum += par1;
				}
				i4dum = IROUND(dum);
				if (param.clipp == 1) {
					if (i4dum < 0) {
						buf_filt[i + j * width] = 0;
					}else if (i4dum > bins-1) {
						buf_filt[i + j * width] = (float)(bins-1);
					}else {
						buf_filt[i + j * width] =(float) i4dum;
					}
				}else {
					buf_filt[i + j * width] = (float)i4dum;
				}
			}
		}
//	}//end if line==0
	
//	if(line<in_lines && line >in_lines-(3*param.sizey+tile)){
//	bottom left corner 

		for (i = 0; i < xfir; i++) {
			for (j = ylas; j < height; j++) {
				par1 = par_r1[(nyb-1)*nxb];
				if (par1 != (float)1.) {
					dum = par1 * pixF[i + j * width];
				}else {
					dum = (double) (pixF[i + j * width]);
				}
				par1 = par_r0[(nyb-1)*nxb];
				if (par1 != (float)0.) {
					dum += par1;
				}
				i4dum = IROUND(dum);
				if (param.clipp == 1) {
					if (i4dum < 0) {
						buf_filt[i + j * width] = 0;
					}else if (i4dum > bins-1) {
						buf_filt[i + j * width] = (float)(bins-1);
					}else {
						buf_filt[i + j * width] =(float) i4dum;
					}
				}else {
					buf_filt[i + j * width] =(float) i4dum;
				}
			}
		}

//	bottom right corner

			for (i = xlas; i < width; i++) {
				for (j = ylas; j < height;j++) {
					par1 = par_r1[(nxb-1) + (nyb -1) * nxb];
					if (par1 != (float)1.) {
						dum = par1 * pixF[i + j * width];
					}else{
						dum = (double) (pixF[i + j * width]);
					}
					par1 = par_r0[(nxb-1) + (nyb -1) * nxb];
					if (par1 != (float)0.) {
						dum += par1;
					}
					i4dum =IROUND(dum);
					if (param.clipp == 1) {
						if (i4dum < 0) {
							buf_filt[i + j * width] = 0;
						}else if (i4dum > bins-1) {
							buf_filt[i + j * width] = (float)(bins-1);
						} else {
							buf_filt[i + j * width] = (float)i4dum;
						}
					}else {
						buf_filt[i + j * width] = (float)i4dum;
					}
				}
			}
//	} // end if line

//	if(line==0){
//	process top border stripe, inner part 

    nxb2 = nxb - 2;
    i1 = xfir - ncoex;
    i2 = xfir;
    for (l = 0; l < nxb2; l++) {
		l1 = l + 1;
		i1 += ncoex;
		i2 += ncoex;
		k=-1;
		for (i = i1; i < i2; i++) {
			k++;
			x1 = xcoef_1[k];
			x2 = xcoef_2[k];
			tr[0] = x1 * par_r0[l] + x2 * par_r0[l1];
			tr[1] = x1 * par_r1[l] + x2 * par_r1[l1];
			for (j = 0; j < yfir; j++) {
				dum = tr[0] + tr[1] * pixF[i + j * width];
				i4dum = IROUND(dum);
				if (param.clipp == 1) {
					if (i4dum < 0) {
						buf_filt[i + j * width] = 0;
					} else if (i4dum > bins-1) {
						buf_filt[i + j * width] = (float)(bins-1);
					} else {
						buf_filt[i + j * width] =(float) i4dum;
					}
				}else {
					buf_filt[i + j * width] =(float)i4dum;
				}
			}
		}
    }

// 	top border stripe, right border of inner part 

    l = nxb - 2;
    l1 = l + 1;
    i1 += ncoex;
    i2 = i1 + ncoebx - 1;
    k = -1;
    for (i = i1; i <=i2; i++) {
		k++;
		x1 = xcoef_3[k];
		x2 = xcoef_4[k];
		tr[0] = x1 * par_r0[l] + x2 * par_r0[l1];
		tr[1] = x1 * par_r1[l] + x2 * par_r1[l1];
		for (j = 0; j < yfir; j++) {
			dum = tr[0] + tr[1] * pixF[i + j * width];
			i4dum = IROUND(dum);
			if (param.clipp == 1) {
				if (i4dum < 0) {
					buf_filt[i + j * width] = 0;
				}else if (i4dum > bins-1) {
					buf_filt[i + j * width] =(float)(bins-1);
				}else {
					buf_filt[i + j * width] =(float) i4dum;
				}
			}else {
				buf_filt[i + j * width] =(float)i4dum;
			}
		}
    }
//	}
//if(line<in_lines && line >in_lines-(3*param.sizey+tile)){
// 	bottom stripe, inner part 

    i1 = xfir - ncoex;
    i2 = xfir;
    for (l = 0; l < nxb2; l++) {
		l1 = l + 1;
		i1 += ncoex;
		i2 += ncoex;
		k = -1;
	 for (i = i1; i < i2; i++) {
	    k++;
	    x1 = xcoef_1[k];
	    x2 = xcoef_2[k];
	    tr[0] = x1 * par_r0[l+ (nyb-1)*nxb] + x2 * par_r0[l1 + (nyb-1)*nxb] ;
	    tr[1] = x1 * par_r1[l+ (nyb-1)*nxb] + x2 * par_r1[l1+ (nyb-1)*nxb];
	    
	    for (j = ylas; j < height; j++) {
			dum = tr[0] + tr[1] * pixF[i + j * width];
			i4dum = IROUND(dum);
			if (param.clipp == 1) {
				if (i4dum < 0) {
					buf_filt[i + j * width] = 0;
				} else if (i4dum > bins-1) {
					buf_filt[i + j * width] = (float)(bins-1);
				}else {
					buf_filt[i + j * width] =(float)i4dum;
				}
			}else {
				buf_filt[i + j * width] =(float) i4dum;
			}
		}
	 }
    }
	

// 	bottom stripe, right border of inner part 

    l = nxb - 2;
    l1 = l + 1;
    i1 += ncoex;
    i2 = i1 + ncoebx - 1;
    k = -1;
    for (i = i1; i <=i2; i++) {
		k++;
		x1 = xcoef_3[k];
		x2 = xcoef_4[k];
		tr[0] = x1 * par_r0[l+ (nyb-1)*nxb] + x2 * par_r0[l1+ (nyb-1)*nxb];
		tr[1] = x1 * par_r1[l+ (nyb-1)*nxb] + x2 * par_r1[l1+ (nyb-1)*nxb];
		
		for (j = ylas; j < height; j++) {
			dum = tr[0] + tr[1] * pixF[i + j * width];
			i4dum = IROUND(dum);
			if (param.clipp == 1) {
				if (i4dum < 0) {
					buf_filt[i + j * width] = 0;
				} else if (i4dum > bins-1) {
					buf_filt[i + j * width] = (float)(bins-1);
				} else {
					 buf_filt[i + j * width] = (float)i4dum;
				}
			} else {
				buf_filt[i + j * width] = (float)i4dum;
			}
		}
    }
//	}

// 	left border stripe, inner part 

    nyb2 = nyb - 2;
    j1 = yfir - ncoey;
    j2 = yfir;
    for (k = 0; k < nyb2; k++) {
		k1 = k + 1;
		j1 += ncoey;
		j2 += ncoey;
		l = -1;
		for (j = j1; j < j2; j++) {
			l++;
			y1 = ycoef_1[l];
			y2 = ycoef_2[l];
			tr[0] = y1 * par_r0[k*nxb] + y2 * par_r0[k1*nxb];
			tr[1] = y1 * par_r1[k*nxb] + y2 * par_r1[k1*nxb];
			for (i = 0; i < xfir; i++) {
				dum = tr[0] + tr[1] * pixF[i + j * width];
				i4dum = IROUND(dum);
				if (param.clipp == 1) {
					if (i4dum < 0) {
						buf_filt[i + j * width] = 0;
					} else if (i4dum > bins-1) {
						buf_filt[i + j * width] = (float)(bins-1);
					} else {
						buf_filt[i + j * width] = (float)i4dum;
					}
				} else {
					buf_filt[i + j * width] = (float)i4dum;
				}
			}
		}
    }

// 	left border stripe, bottom border of inner part 

    k = nyb - 2;
    k1 = k + 1;
    j1 += ncoey;
    j2 = j1 + ncoeby - 1;
    l = -1;
	for (j = j1; j <=j2; j++) {
		l++;
		y1 = ycoef_3[l];
		y2 = ycoef_4[l];
		tr[0] = y1 * par_r0[k*nxb] + y2 * par_r0[k1*nxb];
		tr[1] = y1 * par_r1[k*nxb] + y2 * par_r1[k1*nxb];
		for (i = 0; i < xfir; i++) {
			dum = tr[0] + tr[1] * pixF[i + j * width];
			i4dum = IROUND(dum);
			if (param.clipp == 1) {
				if (i4dum < 0) {
					buf_filt[i + j * width] = 0;
				} else if (i4dum > bins-1) {
					buf_filt[i + j * width] = (float)(bins-1);
				} else {
					buf_filt[i + j * width] = (float)i4dum;
				}
			} else {
				buf_filt[i + j * width] = (float)i4dum;
			}
		}
    }

//	right border stripe, inner part

    j1 = yfir - ncoey;
    j2 = yfir;
    for (k = 0; k < nyb2; k++) {
		k1 = k + 1;
		j1 += ncoey;
		j2 += ncoey;
		l = -1;
		for (j = j1; j < j2; j++) {
			l++;
			y1 = ycoef_1[l];
			y2 = ycoef_2[l];
			tr[0] = y1 * par_r0[(nxb-1)+ k *nxb] + y2 * par_r0[(nxb-1)+ k1 *nxb];
			tr[1] = y1 * par_r1[(nxb-1)+ k *nxb]+ y2 * par_r1[(nxb-1)+ k1 *nxb];
			for (i = xlas; i < width; i++) {
				dum = tr[0] + tr[1] * pixF[i + j * width];
				i4dum = IROUND(dum);
				if (param.clipp == 1) {
					if (i4dum < 0) {
						buf_filt[i + j * width] = 0;
					} else if (i4dum > bins-1) {
						buf_filt[i + j * width] = (float)(bins-1);
					} else {
						buf_filt[i + j * width] = (float)i4dum;
					}
				}else {
					buf_filt[i + j * width] = (float)i4dum;
				}
			}
		}
    }

//	right border stripe, bottom border of inner part 

    k = nyb - 2;
    k1 = k + 1;
    j1 += ncoey;
    j2 = j1 + ncoeby - 1;
    l = -1;
    for (j = j1; j <= j2; j++) {
		l++;
		y1 = ycoef_3[l];
		y2 = ycoef_4[l];
		tr[0] = y1 * par_r0[(nxb-1)+ k *nxb] + y2 * par_r0[(nxb-1)+ k1 *nxb];
		tr[1] = y1 * par_r1[(nxb-1)+ k *nxb] + y2 * par_r1[(nxb-1)+ k1 *nxb];
		for (i = xlas; i < width;i++) {
			dum = tr[0] + tr[1] * pixF[i + j * width];
			i4dum = IROUND(dum);
			if (param.clipp == 1) {
				if (i4dum < 0) {
					buf_filt[i + j * width] = 0;
				} else if (i4dum > bins-1) {
					buf_filt[i + j * width] = (float)(bins-1);
				} else {
					buf_filt[i + j * width] =(float)i4dum;
				}
			} else {
				buf_filt[i + j * width] = (float)i4dum;
			}
		}
    }
	
nxb2 = nxb - 2;
//	central region of inner part 

    i1 = xfir - ncoex;
    i2 = xfir;
    idum1 = yfir - ncoey;
    for (l = 0; l < nxb2; l++) {
		l1 = l + 1;
		i1 += ncoex;
		i2 += ncoex;
		j1 = idum1;
		j2 = yfir;
		for (m = 0; m < nyb2; m++) {
			m1 = m + 1;
			j1 += ncoey;
			j2 += ncoey;
			k = -1;
			for (i = i1; i < i2; i++) {
				k++;
				x1 = xcoef_1[k];
				x2 = xcoef_2[k];
				x11 = x1 * par_r0[l+m*nxb] + x2 * par_r0[l1+m*nxb];
				x12 = x1 * par_r0[l+m1*nxb] + x2 * par_r0[l1+m1*nxb];
				x21 = x1 * par_r1[l+m*nxb]+ x2 * par_r1[l1+m*nxb];
				x22 = x1 * par_r1[l+m1*nxb] + x2 * par_r1[l1+m1*nxb];
				k1 = -1;
				for (j = j1; j < j2; j++) {
					k1++;
					y1 = ycoef_1[k1];
					y2 = ycoef_2[k1];
					tr[0] = y1 * x11 + y2 * x12;
					tr[1] = y1 * x21 + y2 * x22;
					dum = tr[0] + tr[1] * pixF[i + j * width];
					i4dum = IROUND(dum);
					if (param.clipp == 1) {
						if (i4dum < 0) {
							buf_filt[i + j * width] = 0;
						} else if (i4dum > bins-1) {
							buf_filt[i + j * width] = (float)(bins-1);
						} else {
							buf_filt[i + j * width] = (float)i4dum;
						}
					} else {
						buf_filt[i + j * width] = (float)i4dum;
					}
				}	
			}
		}
    }

// 	inner part, right border 

   j1 = yfir - ncoey;
    j2 = yfir;
    l = nxb - 2;
    l1 = l + 1;
    i1 += ncoex;
    i2 = i1 + ncoebx - 1;
    for (m = 0; m < nyb2; m++) {
		m1 = m + 1;
		j1 += ncoey;
		j2 += ncoey;
		k =-1;
		for (i = i1; i <= i2; i++) {
			k++;
			x1 = xcoef_3[k];
			x2 = xcoef_4[k];
			x11 = x1 * par_r0[l+m*nxb] + x2 * par_r0[l1+m*nxb] ;
			x12 = x1 * par_r0[l+m1*nxb] + x2 * par_r0[l1+m1*nxb];
			x21 = x1 * par_r1[l+m*nxb] + x2 * par_r1[l1+m*nxb];
			x22 = x1 * par_r1[l+m1*nxb] + x2 * par_r1[l1+m1*nxb] ;
			k1 = -1;
			for (j = j1; j < j2; j++) {
				k1++;
				y1 = ycoef_1[k1];
				y2 = ycoef_2[k1];
				tr[0] = y1 * x11 + y2 * x12;
				tr[1] = y1 * x21 + y2 * x22;
				dum = tr[0] + tr[1] * pixF[i + j * width];
				i4dum = IROUND(dum);
				if (param.clipp == 1) {
					if (i4dum < 0) {
						buf_filt[i + j * width] = 0;
					} else if (i4dum > bins-1) {
						buf_filt[i + j * width] = (float)(bins-1);
					} else {
						buf_filt[i + j * width] =(float)i4dum;
					}
				} else {
					buf_filt[i + j * width] =(float)i4dum;
				}
			}
		}
    }
//if(line<in_lines && line >in_lines-(3*param.sizey+tile)){

// 	inner part, bottom border 

    i1 = xfir - ncoex;
    i2 = xfir;
    m = nyb - 2;
    m1 = m + 1;
    j1 += ncoey;
    j2 = j1 + ncoeby - 1;
    for (l = 0; l < nxb2; l++) {
		l1 = l + 1;
		i1 += ncoex;
		i2 += ncoex;
		k=-1;
		for (i = i1; i <= i2; i++) { // here
			k++;
			x1 = xcoef_1[k];
			x2 = xcoef_2[k];
			x11 = x1 * par_r0[l+m*nxb] + x2 * par_r0[l1+m*nxb] ;
			x12 = x1 * par_r0[l+m1*nxb] + x2 * par_r0[l1+m1*nxb];
			x21 = x1 * par_r1[l+m*nxb] + x2 * par_r1[l1+m*nxb];
			x22 = x1 * par_r1[l+m1*nxb] + x2 * par_r1[l1+m1*nxb];
			k1 = -1;
			for (j = j1; j <= j2; j++) {
				k1++;
				y1 = ycoef_3[k1];
				y2 = ycoef_4[k1];
				tr[0] = y1 * x11 + y2 * x12;
				tr[1] = y1 * x21 + y2 * x22;
				dum = tr[0] + tr[1] * pixF[i + j * width];
				i4dum = IROUND(dum);
				if (param.clipp == 1) {
					if (i4dum < 0) {
						buf_filt[i + j * width] = 0;
					} else if (i4dum > bins-1) {
						buf_filt[i + j * width] = (float)(bins-1);
					} else {
						buf_filt[i + j * width] =(float) i4dum;
					}
				} else {
					buf_filt[i + j * width] =(float) i4dum;
				}
			}
		}
    }


    m = nyb - 2;
    m1 = m + 1;
    l = nxb - 2;
    l1 = l + 1;
    i1 += ncoex;
    i2 = i1 + ncoebx - 1;
    k = -1;
	for (i = i1; i <= i2; i++) {
		k++;
		x1 = xcoef_3[k];
		x2 = xcoef_4[k];
		x11 = x1 * par_r0[l+m*nxb] + x2 * par_r0[l1+m*nxb];
		x12 = x1 * par_r0[l+m1*nxb] + x2 *par_r0[l1+m1*nxb];
		x21 = x1 * par_r1[l+m*nxb] + x2 * par_r1[l1+m*nxb];
		x22 = x1 * par_r1[l+m1*nxb] + x2 * par_r1[l1+m1*nxb];
		k1 = -1;
		for (j = j1; j <= j2; j++) {
			k1++;
			y1 = ycoef_3[k1];
			y2 = ycoef_4[k1];
			tr[0] = y1 * x11 + y2 * x12;
			tr[1] = y1 * x21 + y2 * x22;
			dum = tr[0] + tr[1] * pixF[i + j * width];
			i4dum = IROUND(dum);
			if (param.clipp == 1) {
				if (i4dum < 0) {
				  buf_filt[i + j * width] = 0;
				} else if (i4dum > bins-1) {
					buf_filt[i + j * width] = (float)(bins-1);
				} else {
					buf_filt[i + j * width] = (float)i4dum;
				}
			} else {
				buf_filt[i + j * width] =(float)i4dum;
			}
		}
	}
//} //end if
	}


	free(xcoef_1);
	free(xcoef_2);
	free(xcoef_3);
	free(xcoef_4);
	free(ycoef_1);
	free(ycoef_2);
	free(ycoef_3);
	free(ycoef_4);
	free(par_r0);
    free(par_r1);
} 


// ###########################################################################




int m_Roi::coeff_set_roi(double a[6],int flag)
{
	if(flag==0){
		_rot_angle=atan2(a[4],a[1]);
		
		_coeff[0]=a[0];
		_coeff[1]=a[1];
		_coeff[2]=a[2];
		_coeff[3]=a[3];
		_coeff[4]=a[4];
		_coeff[5]=a[5];
	}else{
		_coeff_inv[0]=a[0];
		_coeff_inv[1]=a[1];
		_coeff_inv[2]=a[2];
		_coeff_inv[3]=a[3];
		_coeff_inv[4]=a[4];
		_coeff_inv[5]=a[5];
	}

	return M_MATCH_OK;
}
