#include <climits>
#include <math.h>

#include "LabelImage.h"
#include "2DPoint.h"
#include "XYPolyLine.h"
#include "XYContour.h"
#include "XYContourArray.h"
#include "XYZPolyLine.h"
#include "filter.h"
#include "doubles.h"
#include "ImageRegion.h"
#include "ProtocolHandler.h"
#include "sortVector.h"


//****************************************************************************

CLookupTable CLabelImage::lookup(8, 3);

CLabelImage::CLabelImage(long I_offsetX, long I_offset_Y, long I_width, long I_height, int I_neighbourhood):
			CImageBuffer(I_offsetX, I_offset_Y, I_width, I_height, 1, 16, true, 0.0f), 
			nLabels (1), pVoronoi(0), pDistance(0), pMask(0), neighbourhood(I_neighbourhood)
{ 
	if (I_neighbourhood != 4) neighbourhood = 8;
	pVoronoi  = new CImageBuffer (I_offsetX, I_offset_Y, I_width, I_height, 1, 16, true, 0.0f);
	pDistance = new CImageBuffer (I_offsetX, I_offset_Y, I_width, I_height, 1, 16, true, 0.0f);
	this->numberOfPixels.push_back(0);
	initLookup();
};

//=============================================================================

CLabelImage::CLabelImage(const CImageBuffer &mask, float value, int I_neighbourhood):
			CImageBuffer(mask.getOffsetX(), mask.getOffsetY(), mask.getWidth(), mask.getHeight(), 1, 16, true, 0.0f), 
			nLabels (1), pVoronoi(0), pDistance(0), pMask(0), neighbourhood(I_neighbourhood)
 { 
	 if (I_neighbourhood != 4) neighbourhood = 8;
	 pVoronoi  = new CImageBuffer (offsetX, offsetY, width, height, 1, 16, true, 0.0f);
	 pDistance = new CImageBuffer (offsetX, offsetY, width, height, 1, 16, true, 0.0f);
	 this->numberOfPixels.push_back(0);

	 initFromMask(mask, value);
	
	 initLookup();
};

//=============================================================================

CLabelImage::CLabelImage(const CLabelImage &labelimage) : 
			CImageBuffer(labelimage), numberOfPixels(labelimage.numberOfPixels),
			nLabels (labelimage.nLabels), pVoronoi(0), pDistance(0), 
			pMask(0), neighbourhood(labelimage.neighbourhood)
{
	pVoronoi = new CImageBuffer(*labelimage.pVoronoi);
	pDistance = new CImageBuffer(*labelimage.pDistance);
	initLookup();
};

//=============================================================================

CLabelImage::~CLabelImage()
{
	if (pVoronoi)  delete pVoronoi;
	if (pDistance) delete pDistance;
	if (pMask)     delete pMask;
	pVoronoi  = 0;
	pDistance = 0;
	pMask     = 0;
};

//=============================================================================

bool CLabelImage::set(long I_offsetX, long I_offsetY, long I_width, long I_height, int I_bands, int I_bpp, bool I_unsignedFlag)
{
	if ((!I_unsignedFlag) || (I_bpp != 16) || (I_bands != 1)) return false;

	bool ret = CImageBuffer::set(I_offsetX, I_offsetY, I_width, I_height, 1, 16, true);
	
	if (ret && pVoronoi)  ret = pVoronoi->set(I_offsetX, I_offsetY, I_width, I_height, 1, 16, true);
	if (ret && pDistance) ret = pDistance->set(I_offsetX, I_offsetY, I_width, I_height, 1, 16, true);
	if (ret && pMask)     ret = pMask->set(I_offsetX, I_offsetY, I_width, I_height, 1, pMask->getBpp(), true);

	return ret;
};

//=============================================================================

bool CLabelImage::set(long I_offsetX, long I_offsetY, long I_width, long I_height, int I_bands, int I_bpp, bool I_unsignedFlag, float value)
{
	if ((!I_unsignedFlag) || (I_bpp != 16) || (I_bands != 1)) return false;

	bool ret = CImageBuffer::set(I_offsetX, I_offsetY, I_width, I_height, 1, 16, true, value);
	
	if (ret && pVoronoi)  ret = pVoronoi->set(I_offsetX, I_offsetY, I_width, I_height, 1, 16, true, value);
	if (ret && pDistance) ret = pDistance->set(I_offsetX, I_offsetY, I_width, I_height, 1, 16, true, 0);
	if (ret && pMask)     ret = pMask->set(I_offsetX, I_offsetY, I_width, I_height, 1, pMask->getBpp(), true);
	return ret;
};

//=============================================================================

bool CLabelImage::setOffset(long I_offsetX, long I_offsetY)
{
	bool ret = CImageBuffer::setOffset(I_offsetX, I_offsetY);
	
	if (ret && pVoronoi)  ret = pVoronoi->setOffset(I_offsetX, I_offsetY);
	if (ret && pDistance) ret = pDistance->setOffset(I_offsetX, I_offsetY);
	if (ret && pMask)     ret = pMask->setOffset(I_offsetX, I_offsetY);
	return ret;
};

//=============================================================================

bool CLabelImage::set(const CImageBuffer &buffer)
{
	bool ret = CImageBuffer::set(buffer.getOffsetX(), buffer.getOffsetY(), buffer.getWidth(), buffer.getHeight(), 1, 16, true);
	
	if (ret && pVoronoi)  ret = pVoronoi->set(offsetX, offsetY, width, height, 1, 16, true);
	if (ret && pDistance) ret = pDistance->set(offsetX, offsetY, width, height, 1, 16, true);
	if (ret && pMask)     ret = pMask->set(offsetX, offsetY, width, height, 1, pMask->getBpp(), true);
	return ret;
};

//=============================================================================

 void CLabelImage::clear()
 {
	 setAll(0.0f);

	 if (pVoronoi)  pVoronoi->setAll(0.0f);
	 if (pDistance) pDistance->setAll(0.0f);
	 if (pMask)     pMask->setAll(0.0f);
	 nLabels = 1;
	 numberOfPixels.clear();
	 numberOfPixels.push_back(0);
 };

//=============================================================================

void CLabelImage::initFromMask(const CImageBuffer &I_mask, float value)
{
	clear();
	if (this->pMask)
	{
		if ((this->pMask->getWidth() != width) || (this->pMask->getHeight() != height))		
			this->pMask->set(offsetX, offsetY, width, height, 1, 1, true, 0);
		else 
		{
			this->pMask->setOffset(offsetX, offsetY);
			this->pMask->setAll(0);
		}
	}
	else 
	{
		this->pMask = new CImageBuffer(offsetX, offsetY, width, height, 1, 1, true, 0);
	};

	if ((I_mask.getBpp() == 1) || (I_mask.getBpp() == 8))
	{
		for (unsigned long u = 0; u < nGV; ++u)
		{
			if (I_mask.pixUChar(u) >= value) this->pMask->pixUChar(u) = 1;
		}
	}
	else if ((I_mask.getBpp() == 16))
	{
		if (I_mask.getUnsignedFlag())
		{
			for (unsigned long u = 0; u < nGV; ++u)
			{
				if (I_mask.pixUShort(u) >= value) this->pMask->pixUChar(u) = 1;
			}
		}
		else
		{
			for (unsigned long u = 0; u < nGV; ++u)
			{
				if (I_mask.pixUShort(u) >= value) this->pMask->pixUChar(u) = 1;
			}
		}
	}
	else if ((I_mask.getBpp() == 32))
	{
		for (unsigned long u = 0; u < nGV; ++u)
		{
			if (I_mask.pixFlt(u) >= value) this->pMask->pixUChar(u) = 1;
		}	
	}

	pMask->setMargin(1,0);

	long nRegions = this->createLabels();

	if (nRegions > 1)
	{
		if ((long) numberOfPixels.size() < nRegions) numberOfPixels.resize(nRegions, 0);
		nLabels = nRegions; 
		computePixelNumbers();
	};
 };

//=============================================================================

CLabelImage &CLabelImage::operator= (const CLabelImage& labelImage)
{
	if (this != &labelImage) 
	{  
		bool bOK = CImageBuffer::set(labelImage);
		if (bOK)
		{
			if (labelImage.pVoronoi)
			{
				if (pVoronoi)  bOK = pVoronoi->set(*labelImage.pVoronoi);
				else pVoronoi = new CImageBuffer(*labelImage.pVoronoi);
				if (!pVoronoi) bOK = false;
			}
			else if (pVoronoi)
			{
				delete pVoronoi;
				pVoronoi = 0;
			}

			if (bOK)
			{
				if (labelImage.pDistance)
				{
					if (pDistance) bOK = pVoronoi->set(*labelImage.pDistance);
					else pDistance = new CImageBuffer(*labelImage.pDistance);
					if (!pDistance) bOK = false;
				}
				else if (pDistance)
				{
					delete pDistance;
					pDistance = 0;
				}

				if (bOK)
				{
					if (pMask) delete pMask;
					pMask = 0;
					numberOfPixels = labelImage.numberOfPixels;
					nLabels        = labelImage.nLabels;
				};
			};
		};
	};

	return *this;
};

//=============================================================================
 
void CLabelImage::initPixelNumbers()
{
	nLabels = 0;

	for (unsigned long u = 0; u < nGV; ++u)
	{
		unsigned short label = getLabel(u);
		if (label > nLabels) nLabels = label;
	};

	nLabels++;
	numberOfPixels.resize(nLabels);
	computePixelNumbers();
};

//=============================================================================

void CLabelImage::computePixelNumbers()
{
	numberOfPixels.resize(nLabels, 0);
	for (unsigned int i = 0; i < numberOfPixels.size(); ++i) numberOfPixels[i] = 0;
	for (unsigned long u = 0; u < nGV; ++u)
	{
		numberOfPixels[getLabel(u)]++;
	};
};

//=============================================================================
 
void CLabelImage::initMask()
{
	if (pMask) delete pMask;
	pMask = new CImageBuffer(offsetX, offsetY, width, height, 1, 1, true, 0);

	for (unsigned long u = 0; u < nGV; ++u)
	{
		if (getLabel(u) != 0) pMask->pixUChar(u) = 1;
	};
};

//=============================================================================

bool CLabelImage::getContours(CXYContourArray &contours, 
		                      unsigned short label, 
							  bool useVoronoi) const
{
	if (!useVoronoi) return this->getContours(*this, contours, label);

	return this->getContours(*this->pVoronoi, contours, label);
};
	  
//=============================================================================

unsigned short CLabelImage::getNeighbourLabel(unsigned short label, 
                                              const C2DPoint &pos) const
{
	long row = long(floor(0.5 + pos.y)) - this->getOffsetY();
	long col = long(floor(0.5 + pos.x)) - this->getOffsetX();	
	long index = this->getIndex(row, col);
	unsigned short maxDist = 10;

	unsigned short voronoiLabel = this->getVoronoi(index);
	unsigned short dist = this->getDistance(index);
	if (voronoiLabel != label)
	{
		if (dist < maxDist) return voronoiLabel;
		return 0;
	}


	index -= this->getDiffY();
	voronoiLabel = this->getVoronoi(index);
	dist = this->getDistance(index);
	if (voronoiLabel != label)
	{
		if (dist < maxDist) return voronoiLabel;
		return 0;
	}

	index -= this->getDiffX();
	voronoiLabel = this->getVoronoi(index);
	dist = this->getDistance(index);
	if (voronoiLabel != label)
	{
		if (dist < maxDist) return voronoiLabel;
		return 0;
	}

	index += this->getDiffY();
	voronoiLabel = this->getVoronoi(index);
	dist = this->getDistance(index);
	if (voronoiLabel != label)
	{
		if (dist < maxDist) return voronoiLabel;
		return 0;
	}

	index += this->getDiffY();
	voronoiLabel = this->getVoronoi(index);
	dist = this->getDistance(index);
	if (voronoiLabel != label)
	{
		if (dist < maxDist) return voronoiLabel;
		return 0;
	}

	index += this->getDiffX();
	voronoiLabel = this->getVoronoi(index);
	dist = this->getDistance(index);
	if (voronoiLabel != label)
	{
		if (dist < maxDist) return voronoiLabel;
		return 0;
	}

	index += this->getDiffX();
	voronoiLabel = this->getVoronoi(index);
	dist = this->getDistance(index);
	if (voronoiLabel != label)
	{
		if (dist < maxDist) return voronoiLabel;
		return 0;
	}

	index -= this->getDiffY();
	dist = this->getDistance(index);
	voronoiLabel = this->getVoronoi(index);
	if (voronoiLabel != label)
	{
		if (dist < maxDist) return voronoiLabel;
		return 0;
	}

	index -= this->getDiffY();
	dist = this->getDistance(index);
	voronoiLabel = this->getVoronoi(index);
	if (voronoiLabel != label)
	{
		if (dist < maxDist) return voronoiLabel;
		return 0;
	}

	return 0;
};
	  
//=============================================================================

void CLabelImage::resetVoronoiByDist(unsigned short minDist)
{
	for (unsigned int i = 0; i < this->nGV; ++i)
	{
		if (this->getDistance(i) >= minDist)
		{
			this->pVoronoi->pixUShort(i) = 0;
		}
	}
};

//=============================================================================

bool CLabelImage::getContours(vector<vector<CXYContour *> > &contourNeighbourVector, 
							  vector<vector<unsigned short> > &neighbours, 
							  unsigned short label, 
							  bool useVoronoi) const
{
	CXYContourArray contours;

	bool ret = true;

	if (!useVoronoi) ret = this->getContours(*this, contours, label);
	else ret = this->getContours(*this->pVoronoi, contours, label);

	if (ret)
	{
		contourNeighbourVector.resize(contours.GetSize());
		neighbours.resize(contours.GetSize());
		for (unsigned int i = 0; i < neighbours.size(); ++i)
		{
			CXYContour *cont = contours.GetAt(i);
			vector<unsigned short> neighbourlabels(cont->getPointCount());
			for (int j = 0; j < cont->getPointCount(); ++j)
			{
				C2DPoint *p = cont->getPoint(j);
				neighbourlabels[j] = this->getNeighbourLabel(label, *p); 
			}

			if (neighbourlabels[cont->getPointCount() - 1] == neighbourlabels[0])
			{
				int firstDiff = -1;
				for (int j = 0; (j < cont->getPointCount()) && (firstDiff < 0); ++j)
				{
					if (neighbourlabels[j] != neighbourlabels[0]) firstDiff = j;
				}

				if (firstDiff > 0)
				{
					vector<unsigned short> neighbourlabelsCopy(neighbourlabels);
					CXYContour contCopy(*cont);
					for (int j = firstDiff, newidx = 0; j < cont->getPointCount(); ++j, ++newidx)
					{
						C2DPoint *p = cont->getPoint(newidx);
						*p = *contCopy.getPoint(j);
						neighbourlabels [newidx] = neighbourlabelsCopy[j];
					}

					for (int j = 0, newidx = cont->getPointCount() - firstDiff; j < firstDiff; ++j, ++newidx)
					{
						C2DPoint *p = cont->getPoint(newidx);
						*p = *contCopy.getPoint(j);
						neighbourlabels [newidx] = neighbourlabelsCopy[j];
					}
				}
			}

			int j = 0;
			do
			{
				CXYContour *pNeighCont = new CXYContour;
				unsigned short neigh = neighbourlabels[j];
				while ((j < cont->getPointCount()) && (neighbourlabels[j] == neigh))
				{
					pNeighCont->addPoint(*cont->getPoint(j));
					++j;
				}
				if (pNeighCont->getPointCount() < 2)
				{
					delete pNeighCont;
				}
				else 
				{
					contourNeighbourVector[i].push_back(pNeighCont);
					neighbours[i].push_back(neigh);
				}


			} while (j < cont->getPointCount());
		}
	}

	return ret;
};

//=============================================================================

bool CLabelImage::getMaskForLabel(CImageBuffer &lMask, unsigned short label, 
								  int factor, bool useVoronoi) const
{
	if (!useVoronoi) return this->getMaskForLabel(lMask, *this, label, factor);

	return this->getMaskForLabel(lMask, *this->pVoronoi, label, factor);
};
	  
//=============================================================================

bool CLabelImage::getMaskForLabel(CImageBuffer &lMask, const CImageBuffer &labels, 
		                          unsigned short label, int factor) const
{
	bool ret = lMask.set(0, 0, labels.getWidth() * factor, labels.getHeight() * factor, 1, 1, true, 0.0f);
	if (ret)
	{
		int maskDiffX  = factor * lMask.getDiffX();
		int maskDiffY  = factor * lMask.getDiffY();
		int maskDiffXY = maskDiffX + maskDiffY;

		for (unsigned int uMinLabels = 0, uMinMask = 0; uMinLabels < labels.getNGV(); 
			 uMinLabels += labels.getDiffY(), uMinMask += maskDiffY) 
		{
			unsigned int uMaxLabelsInRow = uMinLabels + labels.getDiffY();
			for(unsigned int uLabel = uMinLabels, uMask = uMinMask; uLabel < uMaxLabelsInRow;
				uLabel += labels.getDiffX(), uMask += maskDiffX)
			{
				if (labels.pixUShort(uLabel) == label)
				{
					unsigned int maxMaskInner = uMask + maskDiffXY;
					for (unsigned int minMaskInner = uMask; minMaskInner < maxMaskInner; minMaskInner += lMask.getDiffY())
					{
						unsigned int maxRowMaskInner = minMaskInner + maskDiffX;
						for (unsigned int maskInner = minMaskInner; maskInner < maxRowMaskInner; ++maskInner)       
						{
							lMask.pixUChar(maskInner) = 255;
						};
					};
				};
			};
		};
	};

	return ret;
};

//=============================================================================

bool CLabelImage::getContours(const CImageBuffer &labels, 
							  CXYContourArray &contours, 
		                      unsigned short label) const
{
	int factor = 3;

	CImageBuffer contMask;
	bool ret = this->getMaskForLabel(contMask, labels, label, factor);

	if (ret)
	{
		contMask.setMargin(1, 0.0);
		CImageBuffer locMask(contMask, false);

		CImageFilter morphFil(eMorphological, factor, factor);
		morphFil[0] = morphFil[factor - 1] = 0;
		morphFil[factor * (factor - 1)] = morphFil[factor * factor - 1] = 0;

		morphFil.binaryMorphologicalOpen(contMask, locMask, 1.0);

		contMask.setAll(0.0);

		unsigned int uMin = locMask.getDiffX() + locMask.getDiffY(); 
		unsigned int uMax = locMask.getNGV() - locMask.getDiffY(); 
		unsigned int uMaxRow = locMask.getDiffY() + locMask.getDiffY() - locMask.getDiffX(); 
		for (; uMin < uMax; uMin += locMask.getDiffY(), uMaxRow += locMask.getDiffY())
		{
			for (unsigned int u = uMin; u < uMaxRow; u += locMask.getDiffX())
			{
				unsigned char cL0 = locMask.pixUChar(u);
				unsigned char cLX = locMask.pixUChar(u - locMask.getDiffX());
				unsigned char cLY = locMask.pixUChar(u - locMask.getDiffY());
				if ((cL0 != cLX) || (cL0 != cLY))
				{
					contMask.pixUChar(u) = 255;				 
				};
			};
		};
/*
		ostrstream oss;
		oss << protHandler.getDefaultDirectory().GetChar() << "cont_" << label << ".tif" << ends;
		char *z = oss.str();
		contMask.exportToTiffFile(z, 0, false);
		delete [] z;
*/
		ret = contMask.getContours(contours, 9, 0, false, true);
		if (ret)
		{
			for (int iC1 = 0; iC1 < contours.GetSize(); ++iC1)
			{
				CXYContour *pCont1 = contours.GetAt(iC1);
				C2DPoint *pP10 = pCont1->getPoint(0);
				C2DPoint *pP1L = pCont1->getPoint(pCont1->getPointCount() - 1);
				if (pP10->getDistance(*pP1L) > 3)
				{
					for (int iC2 = iC1 + 1; iC2 < contours.GetSize(); ++iC2)
					{
						CXYContour *pCont2 = contours.GetAt(iC2);
						C2DPoint *pP20 = pCont2->getPoint(0);
						C2DPoint *pP2L = pCont2->getPoint(pCont2->getPointCount() - 1);
						if (pP10->getDistance(*pP20) < 3)
						{
							pCont1->revert();
							pCont1->append(pCont2);
							contours.RemoveAt(iC2);
							iC2 = contours.GetSize();
							--iC1;
						}
						else if (pP10->getDistance(*pP2L) < 3)
						{
							pCont1->revert();
							pCont2->revert();
							pCont1->append(pCont2);
							contours.RemoveAt(iC2);
							iC2 = contours.GetSize();
							--iC1;
						}
						else if (pP1L->getDistance(*pP20) < 3)
						{
							pCont1->append(pCont2);
							contours.RemoveAt(iC2);
							iC2 = contours.GetSize();
							--iC1;
						}
						else if (pP1L->getDistance(*pP2L) < 3)
						{
							pCont2->revert();
							pCont1->append(pCont2);
							contours.RemoveAt(iC2);
							iC2 = contours.GetSize();
							--iC1;
						}
					}
				}
			}

			double oneByFact = 1.0 / (double) factor; 
			for (int iC = 0; iC < contours.GetSize(); ++iC)
			{
				CXYContour *pCont = contours.GetAt(iC);
				for (int iP = 0; iP < pCont->getPointCount(); ++iP)
				{
					C2DPoint *pP = pCont->getPoint(iP);
					pP->x = (pP->x - 1) * oneByFact;
					pP->y = (pP->y - 1) * oneByFact;
				}
			}
		}
	}

	return ret;
};

//=============================================================================
 
void CLabelImage::setBoundariesToZero(bool renumber, bool updateNumbers, 
									  bool recomputeVoronoi, unsigned short firstLabel)
{
	CImageBuffer copy(*this);

	for (unsigned int i = 0; i < this->getNGV(); ++i)
	{
		unsigned short label = copy.pixUShort(i);
		if (label > firstLabel)
		{ 
			unsigned int iBd = i + this->getDiffX();
			unsigned short bdL = copy.pixUShort(iBd);
			if (bdL && (bdL > label)) this->pixUShort(i) = 0;
			else
			{
				iBd -= this->getDiffY();
				bdL = copy.pixUShort(iBd);
				if (bdL && (bdL > label)) this->pixUShort(i) = 0;
				else
				{
					iBd -= this->getDiffX();
					bdL = copy.pixUShort(iBd);
					if (bdL && (bdL > label)) this->pixUShort(i) = 0;
					else
					{
						iBd -= this->getDiffX();
						bdL = copy.pixUShort(iBd);
						if (bdL && (bdL > label)) this->pixUShort(i) = 0;
						else
						{
							iBd += this->getDiffY();
							bdL = copy.pixUShort(iBd);
							if (bdL && (bdL > label)) this->pixUShort(i) = 0;
							else
							{
								iBd += this->getDiffY();
								bdL = copy.pixUShort(iBd);
								if (bdL && (bdL > label)) this->pixUShort(i) = 0;
								else
								{
									iBd += this->getDiffX();
									bdL = copy.pixUShort(iBd);
									if (bdL && (bdL > label)) this->pixUShort(i) = 0;
									else
									{
										iBd += this->getDiffX();
										bdL = copy.pixUShort(iBd);
										if (bdL && (bdL > label)) this->pixUShort(i) = 0;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if (renumber) this->renumberLabels();
	else if (updateNumbers) this->initPixelNumbers();
	
	if (recomputeVoronoi) this->updateVoronoi();
};

//=============================================================================
 
void CLabelImage::fill(const CXYPolyLine &poly, unsigned short label)
{
	this->fillByValue(poly, float(label));
};

//=============================================================================
 
void CLabelImage::fill(const CXYZPolyLine &poly, unsigned short label)
{
	long xmax = offsetX + width;
	long ymax = offsetY + height - 1;

	for (long xmin = offsetX; xmin < xmax; ++xmin)
	{
		doubles intersectionVec;
		unsigned long nIntersections = poly.getIntersections(xmin, intersectionVec);

		if (nIntersections)
		{
			for (unsigned long  u = 0; u < nIntersections; u += 2)
			{     
				assert(u + 1 < intersectionVec.size());
				long ymin = long(floor(intersectionVec[u] + 0.5f));
				long yupper = (intersectionVec[u + 1] < ymax)? (long) intersectionVec[u + 1] : ymax;
				if (ymin   < offsetY) ymin   = offsetY;
				if (ymin   > ymax)    ymin   = ymax;
				if (yupper < offsetY) yupper = offsetY;
				for (unsigned long j = this->getIndex(ymin, xmin); ymin <= yupper; ++ymin, j += diffY)
				{
					assert(j < nGV);
					//if (!this->pixUShort(j)) 
					this->pixUShort(j) = label;
					//else this->pixUShort(j) = 0;
				};
			};
		};
	};
};

//=============================================================================
 
void CLabelImage::fillVoronoi(const CXYPolyLine &poly, unsigned short label, unsigned short overwriteLabel)
{
	long xmax = offsetX + width;
	long ymax = offsetY + height - 1;

	for (long xmin = offsetX; xmin < xmax; ++xmin)
	{
		doubles intersectionVec;
		unsigned long nIntersections = poly.getIntersections(xmin, intersectionVec);

		if (nIntersections)
		{
			for (unsigned long  u = 0; u < nIntersections; u += 2)
			{     
				assert(u + 1 < intersectionVec.size());
				long ymin = long(floor(intersectionVec[u] + 0.5f));
				long yupper = (intersectionVec[u + 1] < ymax)? (long) intersectionVec[u + 1] : ymax;
				for (unsigned long j = this->getIndex(ymin, xmin); ymin <= yupper; ++ymin, j += diffY)
				{
					assert(j < nGV);
					if ((overwriteLabel < 0) || (pVoronoi->pixUShort(j) == overwriteLabel))
						pVoronoi->pixUShort(j) = label;
				};
			};
		};
	};
};

//=============================================================================
 
void CLabelImage::fillVoronoi(const CXYZPolyLine &poly, unsigned short label, unsigned short overwriteLabel)
{
	long xmax = offsetX + width;
	long ymax = offsetY + height - 1;

	for (long xmin = offsetX; xmin < xmax; ++xmin)
	{
		doubles intersectionVec;
		long nIntersections = poly.getIntersections(xmin, intersectionVec);

		if (nIntersections)
		{
			for (long  u = 0; u < nIntersections; u += 2)
			{     
				assert((long) u + 1 < (long) intersectionVec.size());
				long ymin = long(floor(intersectionVec[u] + 0.5f));
				long yupper = (intersectionVec[u + 1] < ymax)? (long) intersectionVec[u + 1] : ymax;
				for (unsigned long j = this->getIndex(ymin, xmin); ymin <= yupper; ++ymin, j += diffY)
				{
					assert(j < nGV);
					if ((overwriteLabel < 0) || (pVoronoi->pixUShort(j) == overwriteLabel))
						pVoronoi->pixUShort(j) = label;
				};
			};
		};
	};
};

//=============================================================================

void CLabelImage::changeContradicting()
{
	for (unsigned long u = 0; u < nGV; ++u)
	{
		unsigned short label = this->pixUShort(u);
		if ((label > 0) && (pVoronoi->pixUShort(u) != label)) this->pixUShort(u) = 0;
	};
};

//=============================================================================

void CLabelImage::resetVoronoi()
{
	if (pVoronoi) pVoronoi->setAll(0.0);
};

//=============================================================================

void CLabelImage::updateVoronoi()
{
	chamfer(3);
};

//=============================================================================

void CLabelImage::removeInVoronoi(const CImageBuffer &background, float value)
{ 
	if ((background.getWidth() == width) && (background.getHeight() == height))
	{
		if ((background.getBpp() == 1) || (background.getBpp() == 8))	
		{
			for (unsigned long u = 0; u < nGV; ++u) 
			{
				if (background.pixUChar(u) != value) pVoronoi->pixUShort(u) = 0;
			};
		}
		else if (background.getBpp() == 16) 
		{
			if (background.getUnsignedFlag())
			{
				for (unsigned long u = 0; u < nGV; ++u) 
				{
					if (background.pixUShort(u) != value) pVoronoi->pixUShort(u) = 0;
				};
			}
			else
			{
				for (unsigned long u = 0; u < nGV; ++u) 
				{
					if (background.pixShort(u) != value) pVoronoi->pixUShort(u) = 0;
				};
			}
		}
		else if (background.getBpp() == 32) 
		{
			for (unsigned long u = 0; u < nGV; ++u) 
			{
				if (background.pixFlt(u) != value) pVoronoi->pixUShort(u) = 0;
			};
		}
	};	
};

//=============================================================================

void CLabelImage::updateVoronoi(const CImageBuffer &background, float value)
{
	if ((background.getWidth() == width) && (background.getHeight() == height))
	{
		unsigned short backgroundLabel = (unsigned short) nLabels + 1;

		if ((background.getBpp() == 1) || (background.getBpp() == 8))	
		{
			for (unsigned long u = 0; u < nGV; ++u) 
			{
				if (background.pixUChar(u) != value) this->pixUShort(u) = backgroundLabel;
			};
		}
		else if (background.getBpp() == 16) 
		{
			if (background.getUnsignedFlag())
			{
				for (unsigned long u = 0; u < nGV; ++u) 
				{
					if (background.pixUShort(u) != value) this->pixUShort(u) = backgroundLabel;
				};
			}
			else
			{
				for (unsigned long u = 0; u < nGV; ++u) 
				{
					if (background.pixShort(u) != value) this->pixUShort(u) = backgroundLabel;
				};
			}
		}
		else if (background.getBpp() == 32) 
		{
			for (unsigned long u = 0; u < nGV; ++u) 
			{
				if (background.pixFlt(u) != value) this->pixUShort(u) = backgroundLabel;
			};
		}
		
		chamfer(3);

		for (unsigned long u = 0; u < nGV; ++u) 
		{
			if (this->pixUShort(u)     == backgroundLabel) this->pixUShort(u)     = 0;
			if (pVoronoi->pixUShort(u) == backgroundLabel) pVoronoi->pixUShort(u) = 0;
		};
	};
 };
   
//=============================================================================

void CLabelImage::replaceLabel(unsigned short oldLabel, unsigned short newLabel, bool renumber)
{
	for (unsigned long u = 0; u < nGV; ++u) 
	{
		if (this->pixUShort(u)     == oldLabel) this->pixUShort(u)     = newLabel;
		if (pVoronoi->pixUShort(u) == oldLabel) pVoronoi->pixUShort(u) = newLabel;
	};
  
	if (newLabel == 0) numberOfPixels[newLabel] += numberOfPixels[oldLabel];
	else numberOfPixels[newLabel] = numberOfPixels[oldLabel];
	numberOfPixels[oldLabel] = 0;
	if (renumber) removeEmptyLabels(renumber);
};

//=============================================================================

void CLabelImage::removeLabel(unsigned short label, bool renumber)
{
	replaceLabel(label, 0, renumber);
};

//=============================================================================

void CLabelImage::mergeLabels(unsigned short label1, unsigned short label2, bool renumber)
{
	if (label1 != label2)  
	{
		for (unsigned long u = 0; u < nGV; ++u) 
		{
			if (this->pixUShort(u)     == label1) this->pixUShort(u)     = label2;
			if (pVoronoi->pixUShort(u) == label1) pVoronoi->pixUShort(u) = label2;
		};

		numberOfPixels[label2] += numberOfPixels[label1];
		numberOfPixels[label1] = 0;
   
		if (renumber) 
		{
			removeEmptyLabels(renumber);
			updateVoronoi();
		};
	};
};

//=============================================================================

void CLabelImage::removeEmptyLabels(bool renumber)
{
	removeSmallLabels(0, renumber);
};

//=============================================================================

 void CLabelImage::importNewLabels(const CLabelImage& newLabels, bool renumber)
 {
	 if (newLabels.getNLabels() > 1)
	 {
		 unsigned short oldIndex = (unsigned short) getNLabels();
		 numberOfPixels.resize(getNLabels() + newLabels.getNLabels() - 1);

		 for (unsigned long u1 = 1; u1 < newLabels.getNLabels(); ++u1)
		 {
			 if (newLabels.numberOfPixels[u1] > 0)
			 {
				 for (unsigned long u = 0; u < nGV; ++u)
				 {
					 if (newLabels.pixUShort(u) == u1) this->pixUShort(u) = oldIndex;
				 };

				 numberOfPixels[oldIndex] = newLabels.numberOfPixels[u1];
				 ++oldIndex;
			 };
		 };

		 nLabels = (unsigned long) oldIndex;

		 if (renumber) 
		 {
			 removeEmptyLabels(renumber);
			 updateVoronoi();
		 };
	 };
 };

//=============================================================================

void CLabelImage::removeSmallLabels(unsigned long minimumSize, bool renumber)
{
	for (unsigned long u = 0; u < nGV; ++u)
	{
		unsigned short regionIndex = this->pixUShort(u);
		if (regionIndex > 0)
		{
			if (numberOfPixels[regionIndex] < minimumSize)
				removeLabel(regionIndex, 0);
		};
	};

	if (renumber)
	{
		renumberLabels();
	};
};

//=============================================================================

void CLabelImage::renumberLabels()
{
	unsigned short newIndex = 1;
  
	vector<unsigned short> newNumber(numberOfPixels.size());

	for (unsigned short u = 1; u < nLabels; ++u)
	{
		if (numberOfPixels[u] > 0) 
		{
//			if (newIndex != u) replaceLabel(u, newIndex, 0);
			newNumber[u] = newIndex;
			if (u != newIndex)
			{
				numberOfPixels[newIndex] = numberOfPixels[u];
				numberOfPixels[u] = 0;
			}
			++newIndex;
		}
		else 
		{
			newNumber[u] = 0;
			numberOfPixels[0] += numberOfPixels[u];
		}
	};

	for (unsigned long u = 0; u < nGV; ++u) 
	{
		this->pixUShort(u)     = newNumber[this->pixUShort(u)];
		unsigned short vLab = pVoronoi->pixUShort(u);
		if (vLab < newNumber.size())  pVoronoi->pixUShort(u) = newNumber[vLab];
	};

	nLabels = newIndex;
	numberOfPixels.resize(nLabels);
};
 
//=============================================================================

void CLabelImage::morphologicalOpen(int structureSize, unsigned short labelMin)
{
	if (labelMin <= 0) labelMin = 1;
	unsigned short labelNumbers = (unsigned short) nLabels;
	for (unsigned short u = labelMin; u < labelNumbers; ++u)
	{
		morphologicalOpenLabel(u, structureSize);
	};

	initPixelNumbers();
	updateVoronoi();
};

//=============================================================================

void CLabelImage::morphologicalOpenLabel(unsigned short label, int structureSize)
{
	CImageBuffer inputMask(offsetX, offsetY, width, height, 1, 1, true, 0.0f);

	for (unsigned long u = 0; u < nGV; ++u)
	{
		if (this->pixUShort(u) == label) inputMask.pixUChar(u) = 1;
	};

	CImageBuffer outputMask(inputMask);

	if (structureSize > 1)
	{
		CImageFilter filter (eMorphological, structureSize, structureSize);
		filter.binaryMorphologicalOpen(inputMask, outputMask, 1);
	};

	CLabelImage currentLabels(outputMask, 1, neighbourhood);

	if (currentLabels.nLabels > 1)
	{
		unsigned short first = 0;

		for (unsigned short u1  = 1; !first && (u1 < currentLabels.nLabels); ++u1)
		{
			if (currentLabels.getNPixels(u1) > 0) first = u1;
		};
   
		if (first > 0)
		{
			for (unsigned long u = 0; u < nGV; ++u)
			{
				if ((this->pixUShort(u) == label) && (currentLabels.pixUShort(u) == 0))
				{
					this->pixUShort(u) = 0;
					numberOfPixels[label]--;
				};
			};  

			currentLabels.removeLabel(first, 0);
			importNewLabels(currentLabels, 0);
		};
	}
	else
	{
		this->removeLabel(label,false);
	};
};

//=============================================================================

void CLabelImage::morphologicalClose(int structureSize)
{
	initMask();

	CImageBuffer inputMask(*pMask);

	CImageFilter filter (eMorphological, structureSize, structureSize);
	filter.binaryMorphologicalClose(inputMask, *pMask, 1);

	for (unsigned long u = 0; u < nGV; u++)
	{
		if (pMask->pixUChar(u) > 0) this->pixUShort(u) = pVoronoi->pixUShort(u);
		else this->pixUShort(u) = 0;
	}

	computePixelNumbers();

	delete pMask;
	pMask = 0;
};

//=============================================================================

void CLabelImage::eraseInVoronoi(const CImageBuffer &I_mask)
{
	for (unsigned long u = 0; u < nGV; ++u)
	{
		if (I_mask.pixUChar(u) != 0) pVoronoi->pixUShort(u) = 0;
	};
};
	  
unsigned short CLabelImage::initNewLabel(const CImageBuffer &image, 
										 const C2DPoint &centre, 
							             int winSize, 
										 CImageRegion &newRegion)
{
	unsigned short newLabel = (unsigned short) this->nLabels;
	long xmin = long(floor(centre.x + 0.5f)) - winSize / 2;
	long ymin = long(floor(centre.y + 0.5f)) - winSize / 2;
	long xmax = xmin + winSize;
	long ymax = ymin + winSize;

	if ((xmin < image.getOffsetX()) || (ymin < image.getOffsetY()) ||
		(xmax >= image.getOffsetX() + image.getWidth()) ||
		(ymax >= image.getOffsetY() + image.getHeight()) || (winSize < 3))
	{
	}
	else
	{
		newRegion.resetData(newLabel);
		float *colourVector = new float[image.getBands()];

		unsigned long uMin = image.getIndex(ymin-image.getOffsetY(), xmin-image.getOffsetX());
		unsigned long uLabelMin = this->getIndex(ymin - offsetY, xmin - offsetX);
		for (long y = ymin; y < ymax; ++y, uMin += image.getDiffY(), uLabelMin += this->diffY)
		{
			unsigned long u = uMin;
			unsigned long uLabel = uLabelMin;
			for (long x = xmin; x < xmax; ++x, u += image.getDiffX(), uLabel += this->diffX)
			{
				this->pixUS[uLabel] = newLabel;
				image.getColourVec(u, colourVector);
				newRegion.addColourVec(colourVector, x, y);
			}	
		}
 
		delete[] colourVector;
		newRegion.computeRegionPars();

		this->nLabels++;
		this->numberOfPixels.push_back(newRegion.getNPixels());
		this->updateVoronoi();
	};

	return newLabel;
};

//============================================================================
 
void CLabelImage::makeGapsSmallByVoronoi(int tolerance, bool renumber, bool updateNumbers,
		                                 bool recomputeVoronoi)
{
	this->updateVoronoi();
	tolerance *= 5;

	for (unsigned long i = 0; i < this->getNGV(); ++i)
	{
		if (!this->pixUShort(i))
		{
			if (this->getDistance(i) < tolerance) 
				this->pixUShort(i) = this->pVoronoi->pixUShort(i);
		}
	}

	this->setBoundariesToZero(renumber, updateNumbers, recomputeVoronoi, 0);
};
	 
//=============================================================================

unsigned char CLabelImage::convertLabelToUC(unsigned short label)
{
	if (label < 255) return (unsigned char) label;
	
	return (unsigned char) (label % 254 + 1);
};

//=============================================================================

void CLabelImage::dump(const char *path, const char *file, const char *suffix,
					   bool voronoiFlag, bool Ii_distFlag)
{
  
	char *filename  = new char[2048];
	char *filebase  = new char[2048];
	char *tifsuffix = new char[2048];

	strcpy(tifsuffix, suffix);
	strcat(tifsuffix, ".tif");
	strcpy(filebase, path);
	strcat(filebase, file);
	if (file[0] != 0) strcat (filebase, "_");

	strcpy(filename, filebase);
	strcat(filename, "labels");
	strcat(filename, tifsuffix);

	CImageBuffer exportBuffer(0,0,width,height,1,8,true,0);
	for (unsigned long u = 0; u < nGV; ++u) 
	{
		if (this->pixUShort(u) > 0) 
		{
			unsigned char c = convertLabelToUC(this->pixUShort(u));
			exportBuffer.pixUChar(u) = c;
		}
	}
	exportBuffer.exportToTiffFile(filename, &lookup, false);

	if (pVoronoi && voronoiFlag)
	{
		strcpy(filename, filebase);
		strcat(filename, "voronoi");
		strcat(filename, tifsuffix);  
		exportBuffer.setAll(0.0);
		for (unsigned long u = 0; u < nGV; ++u) 
			if (pVoronoi->pixUShort(u) > 0)			
			{
				unsigned short s = pVoronoi->pixUShort(u);
				unsigned char c;
				if (s < 255) c = (unsigned char) s;
				else c =  (unsigned char) s % 254 + 1;
				exportBuffer.pixUChar(u) = c;
			}
			exportBuffer.exportToTiffFile(filename, &lookup, false);
	};

	if (pDistance && Ii_distFlag)
	{
		strcpy(filename, filebase);
		strcat(filename, "distance");
		strcat(filename, tifsuffix);  
		exportBuffer.setAll(0.0);
		for (unsigned long u = 0; u < nGV; ++u) 
			if (pDistance->pixUShort(u) > 0) 
			{
				unsigned short s = pDistance->pixUShort(u);
				unsigned char c;
				if (s < 255) c = (unsigned char) s;
				else c =  (unsigned char) s % 254 + 1;
				exportBuffer.pixUChar(u) = c;
			}
		exportBuffer.exportToTiffFile(filename, 0, false);
	};

	if (pMask)
	{
		strcpy(filename, filebase);
		strcat(filename, "segments");
		strcat(filename, tifsuffix);  
		for (unsigned long u = 0; u < nGV; ++u) exportBuffer.pixUChar(u) = pMask->pixUChar(u);
		exportBuffer.exportToTiffFile(filename, 0, false);
	};
};

 //****************************************************************************
  
long CLabelImage::createLabels()
{
	long nRegions = 1;
	unsigned long pixelsInRegion = 0;
	this->setAll(0);

	if (neighbourhood == 8)
	{
		for (unsigned long u = 0; u < nGV; ++u)
		{
			if (this->pMask->pixUChar(u))
			{				
					growRegionN8(u, (unsigned short) nRegions, pixelsInRegion);
					numberOfPixels.push_back(pixelsInRegion);
					nRegions++;
					pixelsInRegion = 0;
			}
		}
	}
	else
	{  
		for (unsigned long u = 0; u < nGV; ++u)
		{
			if (this->pMask->pixUChar(u))
			{				
					growRegionN4(u, (unsigned short) nRegions, pixelsInRegion);
					numberOfPixels.push_back(pixelsInRegion);
					nRegions++;
					pixelsInRegion = 0;
			}
		}
	}

	mergeNeighbourRegions();
  
	delete this->pMask;
	this->pMask = 0;

	return nRegions;
};

//============================================================================

void CLabelImage::growRegionN8(unsigned long index, unsigned short region, unsigned long &PixelsInRegion)
{
#ifdef _DEBUG 
	unsigned long maxPix = 5000;
#else
	unsigned long maxPix = 8000;
#endif

	if ((this->pMask->pixUChar(index)) && (PixelsInRegion < maxPix))
	{ // Recursion must not be applied too often -> Stack overflow!!)
		this->pMask->pixUChar(index) = 0;
		PixelsInRegion++;
		this->pixUShort(index) = region;
		unsigned long neighbourIndex = index - diffY;
		growRegionN8(neighbourIndex, region, PixelsInRegion);
		neighbourIndex -= diffX;
		growRegionN8(neighbourIndex, region, PixelsInRegion);
		neighbourIndex += diffY;
		growRegionN8(neighbourIndex, region, PixelsInRegion);
		neighbourIndex += diffY;
		growRegionN8(neighbourIndex, region, PixelsInRegion);
 		neighbourIndex += diffX;
		growRegionN8(neighbourIndex, region, PixelsInRegion);
 		neighbourIndex += diffX;
		growRegionN8(neighbourIndex, region, PixelsInRegion);
 		neighbourIndex -= diffY;
		growRegionN8(neighbourIndex, region, PixelsInRegion);
 		neighbourIndex -= diffY;
		growRegionN8(neighbourIndex, region, PixelsInRegion);
 	};
};

//============================================================================

void CLabelImage::growRegionN4(unsigned long index, unsigned short region, unsigned long &PixelsInRegion)
{
#ifdef _DEBUG 
	unsigned long maxPix = 5000;
#else
	unsigned long maxPix = 8000;
#endif

	if ((this->pMask->pixUChar(index)) && (PixelsInRegion < maxPix))
	{ // Recursion must not be applied too often -> Stack overflow!!)
		this->pMask->pixUChar(index) = 0;
		PixelsInRegion++;
		this->pixUShort(index) = region;
		unsigned long neighbourIndex = index - diffY;
		growRegionN4(neighbourIndex, region, PixelsInRegion);
		neighbourIndex = index - diffX;
		growRegionN4(neighbourIndex, region, PixelsInRegion);
		neighbourIndex = index + diffY;
		growRegionN4(neighbourIndex, region, PixelsInRegion);
		neighbourIndex = index + diffX;
		growRegionN4(neighbourIndex, region, PixelsInRegion);
	};
};

//=========================================================================

long CLabelImage::growRegion(CImageRegion &region, const CImageBuffer &image)
{
	long nPix = 0;
	long nNewGrown = 0;

	do 
	{
		nNewGrown = growRegionIt(region, image);
		dump("E:\\projects\\","grow", "", false,true);
		nPix += nNewGrown;
	} while (nNewGrown);

	return nPix;
}

//=========================================================================

long CLabelImage::growRegionIt(CImageRegion &region, const CImageBuffer &image)
{
	unsigned short minDist = 7;
	unsigned short label = region.getIndex();

	long xmin = -1;//long(floor(region.getMinPos().x + 0.5)) - 3;
	long ymin = -1;//long(floor(region.getMinPos().y + 0.5)) - 3;
	long xmax = 50000; //long(floor(region.getMaxPos().x + 0.5)) + 3;
	long ymax = 50000;//long(floor(region.getMaxPos().y + 0.5)) + 3;
	if (xmin < image.getOffsetX() + 2) xmin = image.getOffsetX() + 2;
	if (ymin < image.getOffsetY() + 2) ymin = image.getOffsetY() + 2;
	if (xmax >= image.getOffsetX() + image.getWidth()  - 2) xmax = image.getOffsetX() + image.getWidth()  - 3;
	if (ymax >= image.getOffsetY() + image.getHeight() - 2) ymax = image.getOffsetY() + image.getHeight() - 3;

	float *colourVector = new float[image.getBands()];

	unsigned long uMin = image.getIndex(ymin-image.getOffsetY(), xmin - image.getOffsetX());
	unsigned long uMinLabel = this->getIndex(ymin- offsetY, xmin - offsetX);

	unsigned long newPixels = 0;

	CImageRegion oldregion(region);
	region.resetData(label);

	initMask();

	unsigned long labIdx = 0;
	for (long y = ymin; y < ymax; y++, uMin += image.getDiffY(), uMinLabel += diffY)
	{
		unsigned long u = uMin;
		unsigned long uLabel = uMinLabel;
		for (long x = xmin; x < xmax; x++, u += image.getDiffX(), uLabel += diffX)
		{
			if (this->getLabel(uLabel) == label) labIdx = uLabel;
			image.getColourVec(u, colourVector);
			if (oldregion.isConsistent(colourVector)) this->pMask->pixUChar(uLabel) = 1;
			else this->pMask->pixUChar(uLabel) = 0;
		}
	}

	delete [] colourVector;

	CImageBuffer inputMask(*pMask);
	CImageFilter filter (eMorphological, 3, 3);
	filter.binaryMorphologicalOpen(inputMask, *pMask, 1);
	
	
	this->setAll(0);
	this->pixUShort(labIdx) = label;
	growRegionN8(labIdx, region.getIndex(), newPixels);

	region.initParameters(image, *this);
	this->numberOfPixels[label] = region.getNPixels();

	newPixels -= oldregion.getNPixels();
	if (newPixels) this->updateVoronoi();

	return newPixels;
}

//==================================================================

long CLabelImage::growRegions(vector<CImageRegion *> &regionVector, 
							  const CImageBuffer &image,
							  unsigned short MinRegion, int maxIt)
{
	long nPix = 0;
	long nNewGrown = 0;

	int it = 0;
	do 
	{
		nNewGrown = growRegionsIt(regionVector, image, MinRegion);
		nPix += nNewGrown;
		++it;
	} while ((nNewGrown) && (it < maxIt));

	return nPix;
}

//===============================================================================================
	
vector<CImageRegion *> *CLabelImage::initRegionVector(const CImageBuffer &image)
{
	vector<CImageRegion *> *regVec = new vector<CImageRegion *>(this->numberOfPixels.size(), 0);

	ofstream outFile1((protHandler.getDefaultDirectory() + "initRegionVector_withoutOpening_oldInitalisation.txt"));
	
	for (unsigned short region = 1; region < regVec->size(); ++region)
	{
		(*regVec)[region] = new CImageRegion(region, image.getBands());
		(*regVec)[region]->initParameters(image,*this);
		this->numberOfPixels[region] = (*regVec)[region]->getNPixels();

		if (outputMode >= eMAXIMUM)
		{
			(*regVec)[region]->dump(outFile1);
		}
	}

	return regVec;
};

//===============================================================================================
	
vector<CImageRegion *> *CLabelImage::initRegionVector4Labels(const CImageBuffer &image)
{
	vector<CImageRegion *> *regVec = new vector<CImageRegion *>(this->numberOfPixels.size(), 0);

	ofstream *outFile = 0;

	if (outputMode >= eMAXIMUM)
	{
		outFile = new ofstream((protHandler.getDefaultDirectory() + "initRegionVector4Labels_step1.txt"));
	}

	for (unsigned short region = 1; region < regVec->size(); ++region)
	{
		(*regVec)[region] = new CImageRegion(region, image.getBands());
		(*regVec)[region]->initParameters4Computation(image.getBands());
		if (outFile) (*regVec)[region]->dump(*outFile);		
	}

	if (outFile) delete outFile;
	outFile = 0;
	
	long y = image.getOffsetY();
	long xmax = image.getOffsetX() + image.getWidth();
	long ymax = image.getOffsetY() + image.getHeight();
	unsigned short help = 0;

	float *colourVector = new float[image.getBands()];

	for (unsigned long uMin = 0, uLabelMin = 0; y < ymax; ++y, uMin += image.getDiffY(), uLabelMin += (*this).getDiffY())
	{
		unsigned long u = uMin;
		unsigned long uLabel = uLabelMin;

		for (long x = image.getOffsetX(); x < xmax; ++x, u += image.getDiffX(), uLabel += (*this).getDiffX())
		{
			help = (*this).getLabel(uLabel);
			if (help > 0) 
			{
				image.getColourVec(u, colourVector);
				(*regVec)[help]->addColourVec(colourVector, x, y);			
			}
		}
	}
	
	delete[] colourVector;

	if (outputMode >= eMAXIMUM)
	{
		outFile = new ofstream((protHandler.getDefaultDirectory() + "initRegionVector4Labels_step2.txt"));
	}

	for (unsigned short region = 1; region < regVec->size(); ++region)
	{
		(*regVec)[region]->computeRegionPars();
		this->numberOfPixels[region] = (*regVec)[region]->getNPixels();
		if (outFile) (*regVec)[region]->dump(*outFile);
	}

	if (outFile) delete outFile;
	outFile = 0;

	return regVec;

}

//===============================================================================================
	
vector<CImageRegion *> *CLabelImage::initRegionVector4Labels()
{
	vector<CImageRegion *> *regVec = new vector<CImageRegion *>(this->numberOfPixels.size(), 0);

	for (unsigned short region = 1; region < regVec->size(); ++region)
	{
		(*regVec)[region] = new CImageRegion(region, 1);
		(*regVec)[region]->initParameters4Computation(1);
	}
	
	long y = this->getOffsetY();
	long xmax = this->getOffsetX() + this->getWidth();
	long ymax = this->getOffsetY() + this->getHeight();
	unsigned short help = 0;

	float colourVector = 0;

	for (unsigned long uMin = 0, uLabelMin = 0; y < ymax; ++y, uMin += this->getDiffY(), uLabelMin += this->getDiffY())
	{
		unsigned long u = uMin;
		unsigned long uLabel = uLabelMin;

		for (long x = this->getOffsetX(); x < xmax; ++x, u += this->getDiffX(), uLabel += this->getDiffX())
		{
			help = this->getLabel(uLabel);
			if (help > 0) 
			{
				(*regVec)[help]->addColourVec(&colourVector, x, y);			
			}
		}
	}
	
	for (unsigned short region = 1; region < regVec->size(); ++region)
	{
		(*regVec)[region]->computeRegionPars();
		this->numberOfPixels[region] = (*regVec)[region]->getNPixels();
	}

	return regVec;

}
//===============================================================================================

vector<CImageRegion *> *CLabelImage::initRegionVector(const CImageBuffer &image, int morphSize)
{
	CLabelImage AllLabels(*this);


//regionVec with Opening
	CLabelImage OpLabels(*this);

	CImageBuffer source(0,0,image.getWidth(),image.getHeight(),1,8,true,1);
	CImageBuffer destination1(0,0,image.getWidth(),image.getHeight(),1,8,true,1);

	int margin = 1;
	
	unsigned long uMin = OpLabels.getIndex(margin, margin);
	unsigned long uMin2 = source.getIndex(margin, margin);
	unsigned long uMax = OpLabels.getIndex(OpLabels.getHeight() - margin, OpLabels.getWidth() - margin);
	unsigned long uMaxRow = OpLabels.getIndex(1, OpLabels.getWidth() - margin);

	for(; uMaxRow < uMax; uMin += OpLabels.getDiffY(), uMaxRow += OpLabels.getDiffY(), uMin2 += source.getDiffY())
	{
		for (unsigned long u = uMin,u2 = uMin2; u < uMaxRow; u += OpLabels.getDiffX(), u2 += source.getDiffX())
		{
			if (OpLabels.getLabel(u) == 0) source.pixUChar(u2) = 0; 
		}
	}

	if (outputMode >= eMAXIMUM)
	{
		//print out image of mask matrix
		OpLabels.exportToTiffFile(protHandler.getDefaultDirectory() + "OpLabelsMask.tif",0,false);
		source.exportToTiffFile(protHandler.getDefaultDirectory() + "OpLabelsSource.tif",0,false);
	}

	//create Image Filter for Morphological function and opining of non edge pixels of mask pic
	CImageFilter filter(eMorphological, morphSize, morphSize, 1.0f);
	
	filter.binaryMorphologicalOpen(source,destination1,1);

	if (outputMode >= eMAXIMUM)
	{
		//print out of opened mask pic
		destination1.exportToTiffFile(protHandler.getDefaultDirectory() + "OpLabelsMaskAfterOpening.tif",0,false);
	}

	//change of pixel values in copied label image (if pixel is edge pixel in mask change of value of this pixel in newLabel into 0) 
	unsigned long Index;
	unsigned long minIdx = 0;
	unsigned long maxIdxRow = minIdx + destination1.getWidth();
	unsigned long maxIdx = destination1.getNGV();

	for (; minIdx < maxIdx; minIdx += destination1.getDiffY(), maxIdxRow += destination1.getDiffY())
	{
 		for (Index = minIdx; Index < maxIdxRow; Index += destination1.getDiffX())
		{
			if (destination1.pixUChar(Index) == 0) OpLabels.pixUShort(Index) = 0;
		}
	}

	if (outputMode >= eMAXIMUM)
	{
		//print out of newLabel- image
		OpLabels.dump(protHandler.getDefaultDirectory()+ "OpLabelsFinal.tif");
	}

	vector<CImageRegion *> *regVec = OpLabels.initRegionVector4Labels(image);

	for (unsigned short region = 1; region < regVec->size(); ++region)
	{	
		if ((*regVec)[region]->getNPixels() < 3) (*regVec)[region]->initParameters(image,AllLabels);	
	}

	return regVec;
};


//===============================================================================================

vector<CImageRegion *> *CLabelImage::initRegionVector()
{
	vector<CImageRegion *> *regVec = this->initRegionVector4Labels();

	for (unsigned short region = 1; region < regVec->size(); ++region)
	{	
		if ((*regVec)[region]->getNPixels() < 3) (*regVec)[region]->initParameters(*this);	
	}

	return regVec;
};

//================================================================================================

long CLabelImage::growRegionsIt(vector<CImageRegion *> &regionVector, 
								const CImageBuffer &image,
								unsigned short MinRegion)
{
	unsigned short minDist = 7;

	long xmin = image.getOffsetX() + 1;
	long ymin = image.getOffsetY() + 1;
	long xmax = image.getOffsetX() + image.getWidth()  - 2;
	long ymax = image.getOffsetY() + image.getHeight() - 2;

	float *colourVector = new float[image.getBands()];

	unsigned long uMin = image.getIndex(1,1);
	unsigned long uMinThis = this->getIndex(1,1);
	unsigned long uMax = image.getIndex(image.getHeight() - 2, image.getWidth()  - 2);

	long newPixels = 0;

	for (; uMin < uMax; uMin += image.getDiffY(), uMinThis += this->getDiffY())
	{
		unsigned long uMaxR = uMin + image.getDiffY() - 3 * image.getDiffX();
		unsigned long uThis = uMinThis;
		for (unsigned long u = uMin; u < uMaxR; u += image.getDiffX(), uThis += this->getDiffX())
		{
			unsigned short label = this->pVoronoi->pixUShort(uThis);
			if ((label > MinRegion) && (!this->getLabel(uThis)) && (this->pDistance->pixUShort(uThis) < minDist))
			{
				image.getColourVec(u, colourVector);
				if (regionVector[label]->isConsistent(colourVector))
				{
					this->pixUShort(uThis) = label;
					++newPixels;
				}
			}
		}
	}


	delete [] colourVector;

	if (newPixels) 
	{
		this->updateVoronoi();
		for (unsigned short region = MinRegion; region < regionVector.size(); ++region)
		{	
			if (regionVector[region])
			{
				regionVector[region]->initParameters(image,*this);
				this->numberOfPixels[region] = regionVector[region]->getNPixels();
			}
		}
	}

	return newPixels;
}
	  
//============================================================================

void CLabelImage::exchangeLabels(unsigned short label1, unsigned short label2)
{
	for (unsigned int i = 0; i < this->nGV; ++i)
	{
		unsigned short l = this->getLabel(i);
		unsigned short lv = this->getVoronoi(i);

		if (l == label1) this->pixUShort(i) = label2;
		else if (l == label2) this->pixUShort(i) = label1;
		if (lv == label1) this->pVoronoi->pixUShort(i) = label2;
		else if (lv == label2) this->pVoronoi->pixUShort(i) = label1;
	}

	unsigned long dmy = this->numberOfPixels[label1];
	this->numberOfPixels[label1] = this->numberOfPixels[label2];
	this->numberOfPixels[label2] = dmy;
};

//============================================================================

void CLabelImage::replaceLabel(unsigned short oldLabel, unsigned short newLabel, 
							   vector<int> &rmin, vector<int> &rmax, 
							   vector<int> &cmin, vector<int> &cmax)
{
	unsigned long uMin    = (unsigned long) this->getIndex(rmin[oldLabel], cmin[oldLabel]);
	unsigned long uMaxRow = (unsigned long) this->getIndex(rmin[oldLabel], cmax[oldLabel]) + 1;
	unsigned long uMax    = (unsigned long) this->getIndex(rmax[oldLabel], cmax[oldLabel]) + 1;
	unsigned long u;

  	for (; uMin < uMax; uMin += this->diffY, uMaxRow += this->diffY)
	{
		for (u = uMin; u < uMaxRow; ++u)
		{
			if (this->pixUShort(u)     == oldLabel) this->pixUShort(u)     = newLabel;
			if (pVoronoi->pixUShort(u) == oldLabel) pVoronoi->pixUShort(u) = newLabel;
		}
	}

	if (newLabel == 0) numberOfPixels[newLabel] += numberOfPixels[oldLabel];
	else numberOfPixels[newLabel] = numberOfPixels[oldLabel];
	numberOfPixels[oldLabel] = 0;

	 if (rmin[newLabel] > rmin[oldLabel]) rmin[newLabel] = rmin[oldLabel];
	 if (rmax[newLabel] < rmax[oldLabel]) rmax[newLabel] = rmax[oldLabel];
	 if (cmin[newLabel] > cmin[oldLabel]) cmin[newLabel] = cmin[oldLabel];
	 if (cmax[newLabel] < cmax[oldLabel]) cmax[newLabel] = cmax[oldLabel];
};

//============================================================================

void CLabelImage::mergeNeighbourRegions()
{
	vector<int> rmin(this->numberOfPixels.size() + 1, this->height + 1);
	vector<int> rmax(this->numberOfPixels.size() + 1, -1);
	vector<int> cmin(this->numberOfPixels.size() + 1, this->width + 1);
	vector<int> cmax(this->numberOfPixels.size() + 1, -1);

	for (long r = 0; r < this->height; ++r)
	{
		unsigned long u = r * this->diffY;
		for (long c = 0; c < this->width; ++c, ++u)
		{
			short l = this->getLabel(u);
			if (l)
			{
				if (rmin[l] > r) rmin[l] = r;
				if (rmax[l] < r) rmax[l] = r;
				if (cmin[l] > c) cmin[l] = c;
				if (cmax[l] < c) cmax[l] = c;
			}
		}
	}

	if (neighbourhood == 8)
	{
  		for (unsigned long u = 0; u < nGV; ++u)  
		{			
			unsigned short label = this->getLabel(u);
			if (label > 0)
			{
				unsigned long  index1 = u + diffY;
				unsigned short label1 = this->getLabel(index1);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

				index1 += diffX;
				label1 = this->getLabel(index1);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

				index1 -= diffY;
				label1 = this->getLabel(index1);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

				index1 -= diffY;
				label1 = this->getLabel(index1);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

				index1 -= diffX;
				label1 = this->getLabel(index1);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

				index1 -= diffX;
				label1 = this->getLabel(index1);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

				index1 += diffY;
				label1 = this->getLabel(index1);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

				index1 += diffY;
				label1 = this->getLabel(index1);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);
			}
		}
	}
	else
	{
  		for (unsigned long u = 0; u < nGV; ++u)  
		{			
			unsigned short label = this->getLabel(u);
			if (label > 0)
			{
				unsigned short label1 = this->getLabel(u + diffY);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

				label1 = this->getLabel(u + diffX);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

				label1 = this->getLabel(u - diffX);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);

      			label1 = this->getLabel(u - diffY);
				if ((label1 != label) && (label1 > 0))
					replaceLabel(label1, label, rmin, rmax, cmin, cmax);
			}
		}  
	}
}

// Define a local class for chamfering: a mask element containing the pixel index and an index for resetting
class MElement
{
  protected:

	  unsigned long  resetIdx;

  public:

	  unsigned long  pixIdx;
	  unsigned short coeff;

	  // the class must have a default constructor for enabling creating arrays 
	  MElement(): pixIdx(0), resetIdx(0), coeff(0) { }

	  ~MElement(){ }

	  void init( unsigned long I_index, float Icoeff )
	  {
		  pixIdx = I_index;
		  resetIdx = I_index;
		  coeff = (unsigned short) Icoeff;
	  }

	  void reset() {  pixIdx = resetIdx; }

	  void setResetIdx() { resetIdx = pixIdx; }
}; // end of local class definition

//=============================================================================

MElement *getForwardHalfMask(const CImageFilter &mask, unsigned long uMin, long diffX, long diffY)
{
	int HMElements = (mask.getSizeX() == 3 ) ? 5 : 9; // 3x3 / 5x5 mask

    MElement *pHalfMask = new MElement [ HMElements ];

    if ( pHalfMask ) 
    { 
		if ( mask.getSizeX() == 3 )
		{
			// central element
			pHalfMask[0].init( uMin, mask[4] );
			// other elements from left to right and top to bottom
			pHalfMask[1].init( uMin - diffX - diffY, mask[0] );
			pHalfMask[2].init( uMin - diffY,         mask[1] );
			pHalfMask[3].init( uMin + diffX - diffY, mask[2] );
			pHalfMask[4].init( uMin - diffX,         mask[3] );
		}
		else // mask.getSizeX() == 5
		{
			// central element
			pHalfMask[0].init( uMin, mask[12] );
			// other elements from left to right and top to bottom
			// first row:
			pHalfMask[1].init( uMin -     diffX - 2 * diffY, mask[1] );
			pHalfMask[2].init( uMin +     diffX - 2 * diffY, mask[3] );
			// second row:
			pHalfMask[3].init( uMin - 2 * diffX -     diffY, mask[5] );
			pHalfMask[4].init( uMin -     diffX -     diffY, mask[6] );
			pHalfMask[5].init( uMin             -     diffY, mask[7] );
			pHalfMask[6].init( uMin +     diffX -     diffY, mask[8] );
			pHalfMask[7].init( uMin + 2 * diffX -     diffY, mask[9] );
			// third row:
			pHalfMask[8].init( uMin -     diffX            , mask[11] );
		}
     
		for (int maskEle = 1; maskEle < HMElements; ++maskEle )
			pHalfMask[ maskEle ].setResetIdx();
	};

	return pHalfMask;
};

//=============================================================================

MElement *getBackwardHalfMask(const CImageFilter &mask, unsigned long uMax, long diffX, long diffY)
{
	int HMElements = (mask.getSizeX() == 3 ) ? 5 : 9; // 3x3 / 5x5 mask

    MElement *pHalfMask = new MElement [ HMElements ];

    if ( pHalfMask ) 
    { 
		if ( mask.getSizeX() == 3 )
		{
			// central element
			pHalfMask[0].init( uMax, mask[4] );
			// other elements from left to right and top to bottom
			pHalfMask[1].init( uMax + diffX        , mask[5] );
			pHalfMask[2].init( uMax - diffX + diffY, mask[6] );
			pHalfMask[3].init( uMax         + diffY, mask[7] );
			pHalfMask[4].init( uMax + diffX + diffY, mask[8] );
		}
		else // mask.getSizeX() == 5
		{
			// central element
			pHalfMask[0].init( uMax, mask[12] );
			// other elements from left to right and top to bottom
			// third row:
			pHalfMask[1].init( uMax +     diffX            , mask[13] );
			// fourth row:
			pHalfMask[2].init( uMax - 2 * diffX +     diffY, mask[15] );
			pHalfMask[3].init( uMax -     diffX +     diffY, mask[16] );
			pHalfMask[4].init( uMax             +     diffY, mask[17] );
			pHalfMask[5].init( uMax +     diffX +     diffY, mask[18] );
			pHalfMask[6].init( uMax + 2 * diffX +     diffY, mask[19] );
			// fifth row:
			pHalfMask[7].init( uMax -     diffX + 2 * diffY, mask[21] );
			pHalfMask[8].init( uMax +     diffX + 2 * diffY, mask[23] );
			// move the rovers to the right positions
	  }

	  for ( int maskEle = 1; maskEle < HMElements; ++maskEle )
		  pHalfMask[ maskEle ].setResetIdx();
	};

	return pHalfMask;
}

//=============================================================================

bool CLabelImage::chamfer(int size)
{
	bool ret = true;
	eFilterType ft = (size == 3) ? eChamfer3 : eChamfer5;

	CImageFilter mask(ft, size, size);
	short margin = mask.getOffsetX(); 

	this->pVoronoi->set(*this);
	unsigned short sMax = USHRT_MAX / 2;
	for (unsigned long u = 0; u < nGV; ++u)
	{
		if (this->pixUShort(u)) this->pDistance->pixUShort(u) = 0; 
		else this->pDistance->pixUShort(u) = sMax; 
	};

	unsigned short minDist, dist;

    // prepare the top left and bottom right rover
    unsigned long uMin = this->getIndex(margin, margin);
    unsigned long uMax = this->getIndex(height - margin - 1, width - margin);

    int HMElements = (mask.getSizeX() == 3 ) ? 5 : 9; // 3x3 / 5x5 mask

    MElement *pHalfMask;
    pHalfMask = getForwardHalfMask(mask, uMin, diffX, diffY);

    if ( pHalfMask ) 
    { 

		// forward iteration:
		for (unsigned long uRowMax = uMin + width - margin; uRowMax <= uMax; uRowMax += diffY)
		{ // loop over rows
			for ( ; pHalfMask[0].pixIdx < uRowMax; )
			{// loop over columns
				// set minDist to the current value of the pixel
				minDist = this->pDistance->pixUShort(pHalfMask[0].pixIdx);
				unsigned short label = this->pVoronoi->pixUShort(pHalfMask[0].pixIdx);
				// try to find a lower distance by analysing the other half mask elements
				for ( int maskEle = 1; maskEle < HMElements; ++maskEle )
				{
					dist = this->pDistance->pixUShort(pHalfMask[ maskEle ].pixIdx) + pHalfMask[ maskEle ].coeff;
					if ( dist < minDist )
					{
						minDist = dist;
						label = this->pVoronoi->pixUShort(pHalfMask[ maskEle ].pixIdx);
					}

					pHalfMask[ maskEle ].pixIdx += diffX;
				}

				// set the current rover to the (possibly new) s_minDist
				this->pDistance->pixUShort(pHalfMask[0].pixIdx) = minDist;
				this->pVoronoi->pixUShort(pHalfMask[0].pixIdx)  = label;
				pHalfMask[0].pixIdx += diffX;
			}

			// reset to first column and goto next row
			for ( int maskEle = 0; maskEle < HMElements; ++maskEle )
			{
				pHalfMask[ maskEle ].reset();
				pHalfMask[ maskEle ].pixIdx += diffY;
				pHalfMask[ maskEle ].setResetIdx();
			}
		}	
		delete [] pHalfMask;
		pHalfMask = 0;
	} // end of forward iteration
	else ret = false;

	// prepare backward iteration

	if (ret) pHalfMask = getBackwardHalfMask(mask, uMax - 1, diffX, diffY);

	if(pHalfMask)	
	{ // backward iteration:
	  unsigned long firstInRow = this->getIndex(height - margin -1, margin);

	  for ( ; firstInRow >= uMin; firstInRow -= diffY )
	  { // loop over rows
		  for ( ; pHalfMask[0].pixIdx >= firstInRow; )
		  {// loop over columns
			  // set minDist to the current value of the pixel
			  minDist = this->pDistance->pixUShort(pHalfMask[0].pixIdx);
			  unsigned short label = this->pVoronoi->pixUShort(pHalfMask[0].pixIdx);	
			  // try to find a lower distance by analysing the other half mask elements
			  for ( int maskEle = 1; maskEle < HMElements; ++maskEle )
			  {
				  dist = this->pDistance->pixUShort(pHalfMask[ maskEle ].pixIdx) + pHalfMask[ maskEle ].coeff;
				  if ( dist < minDist )
				  {
					  minDist = dist;
					  label = this->pVoronoi->pixUShort(pHalfMask[ maskEle ].pixIdx);
				  };
				  pHalfMask[ maskEle ].pixIdx -= diffX;
			  }

			  // set the current rover to the (possibly new) minDist
			  this->pDistance->pixUShort(pHalfMask[0].pixIdx) = minDist;
			  this->pVoronoi->pixUShort(pHalfMask[0].pixIdx)  = label;
			  pHalfMask[0].pixIdx -= diffX;
		  }

		  // reset to largest column and goto previous row
		  for ( int maskEle = 0; maskEle < HMElements; ++maskEle )
		  {
			  pHalfMask[ maskEle ].reset();
			  pHalfMask[ maskEle ].pixIdx -= diffY;
			  pHalfMask[ maskEle ].setResetIdx();
		  }
	  }
	  delete [] pHalfMask;

	  this->pDistance->setMargin(margin, 0);
	  this->pVoronoi->setMargin(margin, 0);
	  // bug in last row
	  firstInRow = this->getIndex(height - margin -1, margin);
	  unsigned long lastInRow = this->getIndex(height - margin -1, width - margin);
	  for (unsigned long u =  firstInRow; u < lastInRow; ++u)
	  {
		  this->pVoronoi->pixUShort(u) = this->pVoronoi->pixUShort(u - this->diffY);
	  }
	  
	}
	else ret = false;


	return ret;
};

//=============================================================================

void CLabelImage::getColour(int region, int &r, int &g, int &b)
{
	r = 255;
	g = 255;
	b = 255;
  
	if (region > 0)
	{
		r = (37 * region) % 255;
		g = region;
		b = 255 - ((47 * region) % 255);
	};
};

//=============================================================================

void CLabelImage::initLookup()
 {
	 if (lookup[0] == 0) 
	 {
		 int r = 255, g = 255, b = 255;
		 lookup.setColor(r,g,b,0);

		 for (int i = 0; i < lookup.getMaxEntries(); ++i)
		 {
			 getColour(i, r, g, b);
			 lookup.setColor(r,g,b,i);
		 }
	 }
};

//=============================================================================

CLookupTable CLabelImage::get16BitLookup()
{
	CLookupTable lut(16,3);

	int r = 255, g = 255, b = 255;
	lut.setColor(r,g,b,0);

	for (int i = 1; i < lut.getMaxEntries(); ++i)
	{
		r = (37 * i) % 255;
		g = i % 200;
		b = 255 - ((47 * i) % 255);
	
		lut.setColor(r,g,b,i);	 
	}

	return lut;
};
