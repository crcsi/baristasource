#include <float.h>
#include "SnakePoint.h"
#include "SnakeExtraction.h"
#include "ProtocolHandler.h"
#include <vector>

//============================================================================	 

CSnakePoint &CSnakePoint::operator = (const CSnakePoint &p)
{
	if (this != &p)
	{
		index    = p.index;
		x		 = p.x;
		y		 = p.y;
	}

	return *this;
};
 
//============================================================================
void CSnakePoint::initGradientValues(const CImageBuffer *gradX, const CImageBuffer *gradY, C2DPoint Offset)
{
	float *colourVectorGradX = new float[gradX->getBands()];
	float *colourVectorGradY = new float[gradX->getBands()];

	gradX->getInterpolatedColourVecBilin(this->y - Offset.y, this->x - Offset.x, colourVectorGradX);
	gradY->getInterpolatedColourVecBilin(this->y - Offset.y, this->x - Offset.x, colourVectorGradY);

	this->gradX = colourVectorGradX[0];
	this->gradY = colourVectorGradY[0];
};

//============================================================================
bool CSnakePoint::initCoefficients(const SnakeExtractionParameters  *Parameters)
{
	bool ret;

	if(Parameters->getMode() == eTraditional)
	{
		this->coefficientsX = Parameters->getGamma() * this->x + Parameters->getKappa() * this->gradX;
		this->coefficientsY = Parameters->getGamma() * this->y + Parameters->getKappa() * this->gradY;
		ret = true;
	}
	else if (Parameters->getMode() == eBalloon)
	{
		this->coefficientsX = Parameters->getGamma() * this->x - Parameters->getKappa() * this->gradX;
		this->coefficientsY = Parameters->getGamma() * this->y - Parameters->getKappa() * this->gradY;
		ret = true;
	}
	else ret = false;

	return (ret);
};


//=======================================================================================	  
void CSnakePoint::dump(ofstream &oFile) const
{
	oFile.setf(ios::fixed, ios::floatfield);
	oFile.precision(3);


	oFile << "Point ";
	oFile.width(5);
	oFile << this->getIndex();
	oFile.width(5);
	oFile << "x: " << this->x;
	oFile.width(5);
	oFile << "  y: " << this->y;
	oFile.width(5);
	oFile << "  gradX: " << this->gradX; 
	oFile.width(5);
	oFile << "  gradY: " << this->gradY;
	oFile.width(5);
	oFile << " CoeffX: " << this->coefficientsX;
	oFile.width(5);
	oFile << " CoeffY: " << this->coefficientsY; 		
	oFile << endl;
};

