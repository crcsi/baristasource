#include <limits.h>
#include "LookupTable.h"
#include "histogram.h"

//============================================================================

CLookupTable::CLookupTable( int I_bpp, int I_bands ) : bpp( I_bpp ),
    bands(I_bands)
{
	set(I_bands, I_bpp, true);
};

CLookupTable::CLookupTable( const CLookupTable &lookup ): vector<int>(lookup),
     bpp( lookup.bpp ), bands( lookup.bands ), maxEntries(lookup.maxEntries)
{
};

CLookupTable::CLookupTable( const CLookupTable *pLookup ): vector<int>(*pLookup),
     bpp( pLookup->bpp ), bands( pLookup->bands ), maxEntries(pLookup->maxEntries)
{
};
	  
CLookupTable &CLookupTable::operator = (const CLookupTable &other)
{
	if (this != &other)
	{
		set(other.bands, other.bpp, false);
		for (unsigned int i = 0; i < this->size(); ++i)
		{
			(*this)[i] = other[i];
		}
	}

	return *this;
};

void CLookupTable::set(int Bands, int Bpp, bool initGrey)
{
	this->bpp = Bpp;
	this->bands = Bands;
	if (bpp == 8) maxEntries = 256;
	else if (bpp == 16) maxEntries = USHRT_MAX + 1;
	else if (bpp == 32) 
	{
		maxEntries = 256;
		this->bpp = 8;
	}

	else maxEntries = 0;

	unsigned long size = maxEntries * bands;
	this->resize(size);
	if (initGrey) initGreyscale();
}

bool CLookupTable::isGreyScale() const
{ 
	for ( int comp = 1; comp < bands; ++comp )
	{
		int entry0 = comp * maxEntries;
		for ( int entry = 0; entry < maxEntries; ++entry )     
			if ( (*this)[ entry0 + entry] != ( *this )[entry] ) return false;
	}

  return true;
}

bool CLookupTable::isIdentity() const
{	
	for ( int comp = 1; comp < bands; ++comp )
	{
		int entry0 = comp * maxEntries;
		for ( int entry = 0; entry < maxEntries; ++entry )         
			if ( (*this)[entry0 + entry] != entry ) return false;
	};

  return true;
}

bool CLookupTable::extractComponent( int component )
{
	bool ret = true;
	if ( component >= 0 && component < bands )
	{
		int entry0 = component * maxEntries;
		for ( int entry = 0; entry < maxEntries; ++entry )
  			(*this)[entry] = (*this)[entry0 + entry];
    
		bands = 1;
	}
	else ret = false;

	return ret;
};

bool CLookupTable::concatenateComponent( const CLookupTable &lookup, int component )
{
	bool ret = true;
	if ( component >= 0 && component < bands && lookup.bands == 1)
	{
		int entry0 = component * maxEntries;
		for ( int entry = 0; entry < maxEntries; ++entry )
		{
			int otherEntry = (*this)[entry0 + entry];
			if (otherEntry >= 0 && otherEntry < lookup.maxEntries)
				(*this)[entry0 + entry] = lookup[otherEntry];
			else ret = false;
     }
	}
	else ret = false;

	return ret;
};

bool CLookupTable::convertTo8Bit() 
{
	if ( bpp == 8 ) return false;

	for ( int i = 0; i < (int) this->size(); i++ )
	{
		(*this)[i] = ((*this)[i] * 255L) / ((1L << 16 ) - 1 );
	}
	return true;
};

bool CLookupTable::cutTo8Bit()
{
	unsigned short changed = 0;

	for ( int i = 0; i < (int) this->size(); i++  )
	{
		if ((*this)[i] < 0 )
		{ 
			(*this)[i] = 0; 
			++changed; 
		}
		else if ((*this)[i] > 255 )
		{ 
			(*this)[i] = 255; 
			++changed; 
		}
	}

	return (changed != 0);
};

//============================================================================

void CLookupTable::initGreyscale() 
{
	for ( int comp = 0; comp < bands; ++comp )
	{
		int entry0 = comp * maxEntries;
		for ( int entry = 0; entry < maxEntries; ++entry )    
		{
			(*this)[entry0 + entry ] = entry;
		}
	}
};

/**
 * reads look up table file 
 * @param  const char* filename
 * @returns true/false
 */
bool CLookupTable::read(const char* filename)
{  
	FILE* in = fopen(filename,"r");
	if (!in )
		return false;

	char dummy[1024];

	int depth = -1;


	fscanf(in, "%s", &dummy);
	fscanf(in, "%d", &depth);
	if ( depth == 100 ) depth = 8;
	else depth = 16;

	if ( depth != this->bpp)
	{
		fclose(in);
		return false;
	}

	fscanf(in, "%d", &this->maxEntries);

	this->set(3, this->bpp, true);

	int lr, lg, lb;

	for (int i = 0; i < this->maxEntries; i++)
	{
		fscanf(in, "%d %d %d", &lr, &lg, &lb);

		this->setColor(lr, lg, lb, i);
	}

	fclose(in);

    return true;
}



/**
 * writes look up table file
 * @param  const char* filename
 * @returns true/false
 */

bool CLookupTable::write(const char* filename) const
{
    if (this->maxEntries * this->bands != this->size())
		return false;


	FILE	*fout;
	fout = fopen(filename,"w");

	if ( !fout) return false;

    ::fprintf(fout,"JASC-PAL\n");

	if ( this->bpp == 8 )
		::fprintf(fout,"0100\n");
	else 
		::fprintf(fout,"10000\n");

	::fprintf(fout,"%d\n", this->maxEntries);

	int lg, lr, lb;

	for (int i = 0; i < this->maxEntries; i++)
	{
		this->getColor(lr, lg, lb, i);
		::fprintf(fout,"%d %d %d\n", lr, lg, lb);
	}


	fclose(fout);

	return true;
}
