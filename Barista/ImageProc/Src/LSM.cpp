

#include <climits>
#include <float.h>
#include <strstream>
using namespace std;

#include "LSM.h"
#include "ImgBuf.h"
#include "Trans2D.h"
#include "filter.h"
#include "histogram.h"
#include "ProtocolHandler.h"
#include "robustWeightFunction.h"
#include "SystemUtilities.h"

CLSM::CLSM(const CMatchingParameters &pars) : templateBuf(0), templateBufWeight(0), matchingPars(pars), sigmaGreyValUsed(10)
{ 

};

CLSM::CLSM(const CMatchingParameters &pars, CImageBuffer *templatebuf, const CXYPoint &ref): 
					matchingPars(pars), templateBuf(templatebuf), refPoint(ref)
{
	this->sigmaGreyValUsed = (this->templateBuf->getBpp() <= 8) ? 
		this->matchingPars.getSigmaGrey8Bit() : this->matchingPars.getSigmaGrey16Bit();
};

CLSM::~CLSM()
{
	clear();
};

void CLSM::setProjectDir(const CCharString &path)
{
	this->projectDir = path;
	if (!this->projectDir.IsEmpty()) 
	{
		if (this->projectDir[this->projectDir.GetLength() - 1] != CSystemUtilities::dirDelim) this->projectDir += CSystemUtilities::dirDelimStr;
	}
};
 
void CLSM::clear()
{
	for (unsigned int i = 0; i < this->transformedSearchBuffers.size(); ++i)
	{
		delete this->transformedSearchBuffers[i];
		this->transformedSearchBuffers[i] = 0;
	}
	this->transformedSearchBuffers.clear();

	for (unsigned int i = 0; i < this->templateToSearchTrafos.size(); ++i)
	{
		delete this->templateToSearchTrafos[i];
		this->templateToSearchTrafos[i] = 0;
	}	
	this->templateToSearchTrafos.clear();

	if (this->templateBufWeight) delete this->templateBufWeight;
	this->templateBufWeight = 0;
	this->searchPoints.clear();
	this->rhoVec.clear();
	this->searchBufferFactors.clear();
}; 

void CLSM::initLSMWeights()
{
	if (this->templateBufWeight) delete this->templateBufWeight;
	if (this->templateBuf)
	{
		this->templateBufWeight = new CImageBuffer(this->templateBuf->getOffsetX(), this->templateBuf->getOffsetY(), 
			                                       this->templateBuf->getWidth(), this->templateBuf->getHeight(), 1, 32, false);

		double hw = (this->templateBuf->getWidth() > this->templateBuf->getHeight()) ? this->templateBuf->getWidth() : this->templateBuf->getHeight();
		CRobustWeightFunction wf;
		wf.setParameters(hw * 0.5, hw  * 0.5, hw * 2.0);

		int nW = int(hw * 0.5) + 2;
		float *weights = new float[nW];
		for (int i = 0; i < nW; ++i) weights[i] = float(wf.getWeight(i));

		for (int imin = 0, y = -this->templateBufWeight->getHeight() / 2; imin < (int) this->templateBufWeight->getNGV(); imin += this->templateBufWeight->getDiffY(), y++)
		{
			int iMaxRow = imin + this->templateBufWeight->getDiffY();
			for (int i = imin, x = -this->templateBufWeight->getWidth() / 2; i < iMaxRow; i += this->templateBufWeight->getDiffX(), x++)
			{
				this->templateBufWeight->pixFlt(i) = weights[abs(x)] * weights[abs(y)];
			}
		}

		delete [] weights;
	}
	else this->templateBufWeight = 0;
};

bool CLSM::init(const CMatchingParameters &pars, CImageBuffer *templatebuf, const CXYPoint &ref)
{
	this->matchingPars = pars; 
	this->refPoint = ref;
	this->templateBuf = templatebuf;
	this->sigmaGreyValUsed = (this->templateBuf->getBpp() <= 8) ? 
		this->matchingPars.getSigmaGrey8Bit() : this->matchingPars.getSigmaGrey16Bit();
	this->initLSMWeights();
	return true;
};

void CLSM::addSearchImage(CImageBuffer *search, CTrans2D *trans, int searchFactor)
{
	this->searchBuffers.push_back(search);
	CTrans2D *localTrans = trans->clone();
	this->templateToSearchTrafos.push_back(localTrans);
	localTrans->changeP0(this->refPoint);
	CXYPoint searchPt;
	localTrans->transform(searchPt, this->refPoint);
	searchPt.setLabel(this->refPoint.getLabel());

	this->searchPoints.push_back(searchPt);

	CImageBuffer *transformedBuf = new CImageBuffer(this->templateBuf->getOffsetX(), 
		                                            this->templateBuf->getOffsetY(), 
													this->templateBuf->getWidth(),
													this->templateBuf->getHeight(),
													this->templateBuf->getBands(),
													this->templateBuf->getBpp(),
													this->templateBuf->getUnsignedFlag());

	this->transformedSearchBuffers.push_back(transformedBuf);
	this->searchBufferFactors.push_back(searchFactor);
};
	
int CLSM::accumulateLSMEquations(SymmetricMatrix &N, Matrix &t, double &pll, int iSearch, int IndexN, 
								 int r, int c, const C2DPoint &derivatives, double dg,
								 unsigned long uWeight)
{
	int ret = 1;

	if ((fabs(derivatives.x) < FLT_EPSILON) && (fabs(derivatives.y) < FLT_EPSILON))
		ret = 0;
	else
	{
		double w = 1.0;
		if (this->templateBufWeight) w = this->templateBufWeight->pixFlt(uWeight);

		int npars = this->templateToSearchTrafos[iSearch]->nParameters();
		Matrix Ax(npars, 1);
		Matrix Ay(npars, 1);
		this->templateToSearchTrafos[iSearch]->getDerivatives(c, r, Ax, Ay);
		for (int i = 0; i < npars; ++i)
		{
			Ax.element(i,0) = Ax.element(i,0) * derivatives.x + Ay.element(i,0) * derivatives.y ;
		}

		for (int i = 0; i < npars; ++i)
		{
			for (int j = i; j < npars; ++j)
			{
				N.element(IndexN + i, IndexN + j) += Ax.element(i, 0) * Ax.element(j, 0)* w;
			}

			t.element(IndexN + i, 0) += Ax.element(i,0) * dg * w;
		}
	
		pll += dg * dg * w;
	}

	return ret;
};

int CLSM::initLSMEquations(SymmetricMatrix &N, Matrix &t, double &pll)
{
	N = 0.0;
	t = 0.0;
	pll = 0.0;

	int nObs = 0;
	int IndexN = 0;

	for (int i = 0; i < this->nSearchImgs(); ++i)
	{
		CImageBuffer *searchBuf = this->transformedSearchBuffers[i];
		searchBuf->resample(*(this->searchBuffers[i]), *(this->templateToSearchTrafos[i]), eBicubic);

		int rmax = templateBuf->getOffsetY() + templateBuf->getHeight() - 1;
		int cmax = templateBuf->getOffsetX() + templateBuf->getWidth() - 1;
		C2DPoint derivatives;
		double dg;

		if ((templateBuf->getBpp() == 8) && (searchBuf->getBpp() == 8))
		{
			for (int r = templateBuf->getOffsetY() + 1; r < rmax; ++r)
			{
				unsigned long u = templateBuf->getIndex(r - templateBuf->getOffsetY(), 1);
				for (int c = templateBuf->getOffsetX() + 1; c < cmax; ++c, u += searchBuf->getDiffX())
				{
					derivatives.x = ((double) searchBuf->pixUChar(u + searchBuf->getDiffX()) - 
						             (double) searchBuf->pixUChar(u - searchBuf->getDiffX())) * 0.5;
					derivatives.y = ((double) searchBuf->pixUChar(u + searchBuf->getDiffY()) - 
						             (double) searchBuf->pixUChar(u - searchBuf->getDiffY())) * 0.5;
					dg  =  (double) templateBuf->pixUChar(u) - (double) searchBuf->pixUChar(u);
					nObs += accumulateLSMEquations(N, t, pll, i, IndexN, r, c, derivatives, dg, u);
				}
			}
		}
		else if ((templateBuf->getBpp() == 16) && (searchBuf->getBpp() == 16))
		{
			for (int r = templateBuf->getOffsetY() + 1; r < rmax; ++r)
			{
				unsigned long u = templateBuf->getIndex(r - templateBuf->getOffsetY(), 1);
				for (int c = templateBuf->getOffsetX() + 1; c < cmax; ++c, u += searchBuf->getDiffX())
				{
					derivatives.x = ((double) searchBuf->pixUShort(u + searchBuf->getDiffX()) - 
						             (double) searchBuf->pixUShort(u - searchBuf->getDiffX())) * 0.5;
					derivatives.y = ((double) searchBuf->pixUShort(u + searchBuf->getDiffY()) - 
						             (double) searchBuf->pixUShort(u - searchBuf->getDiffY())) * 0.5;
					dg  = (double) templateBuf->pixUShort(u) - (double) searchBuf->pixUShort(u);
					nObs += accumulateLSMEquations(N, t, pll, i, IndexN, r, c, derivatives, dg, u);
				}	
			}
		}
		else if ((templateBuf->getBpp() == 32) && (searchBuf->getBpp() == 32))
		{
			for (int r = templateBuf->getOffsetY() + 1; r < rmax; ++r)
			{
				unsigned long u = templateBuf->getIndex(r - templateBuf->getOffsetY(), 1);
				for (int c = templateBuf->getOffsetX() + 1; c < cmax; ++c, u += searchBuf->getDiffX())
				{
					derivatives.x = ((double) searchBuf->pixFlt(u + searchBuf->getDiffX()) - 
						                  searchBuf->pixFlt(u - searchBuf->getDiffX())) * 0.5;
					derivatives.y = ((double) searchBuf->pixFlt(u + searchBuf->getDiffY()) - 
						                  searchBuf->pixFlt(u - searchBuf->getDiffY())) * 0.5;
					dg  = (double) templateBuf->pixFlt(u) - (double) searchBuf->pixFlt(u);
					nObs += accumulateLSMEquations(N, t, pll, i, IndexN, r, c, derivatives, dg, u);
				}
			}
		}

		IndexN += this->templateToSearchTrafos[i]->nParameters();
	}

	if (this->hasPointObs()) 
	{
		nObs += addPointObservations(N, t, pll, IndexN);
	}

	return nObs;
};
	  
bool CLSM::iterateLSMOnce(double &pvv)
{
	bool ret = true;
	int nPars = 0;
	pvv = 0;
		
	double maxDistDirObs = (this->templateBuf->getWidth() > this->templateBuf->getHeight())? this->templateBuf->getWidth() / 2 : this->templateBuf->getHeight() / 2;
	double maxInflDirObs = 1.0;
	Matrix weightDirObs;

	for (int i = 0; i < this->nSearchImgs(); ++i)
	{
		nPars += this->templateToSearchTrafos[i]->nParameters();
	}

	if (this->hasPointObs()) nPars += 3;
	SymmetricMatrix N(nPars); 
	Matrix t(nPars, 1); 
	double pll;

	int nObs = initLSMEquations(N, t, pll);

	Try
	{
		for (int i = 0, IndexN = 0; i < this->nSearchImgs(); ++i)
		{
			this->templateToSearchTrafos[i]->getWeightForDirectObs(weightDirObs, this->sigmaGreyValUsed, maxInflDirObs, maxDistDirObs);
			for (int j = 0; j < weightDirObs.Nrows(); ++j)
			{
				int rc = IndexN + j;
				N.element(rc,rc) += weightDirObs.element(j,0);
			}

			IndexN += this->templateToSearchTrafos[i]->nParameters();
		}

		this->Qxx = N.i();
		Matrix dx = Qxx * t;
	
		int IndexN = 0;

		for (int i = 0; i < this->nSearchImgs(); ++i)
		{
			this->templateToSearchTrafos[i]->addVec(dx, IndexN);
			this->templateToSearchTrafos[i]->transform(searchPoints[i], this->refPoint);
			IndexN += this->templateToSearchTrafos[i]->nParameters();
		}
		if (this->hasPointObs())
		{
			this->ObjPoint.x += dx.element(IndexN, 0); IndexN++;
			this->ObjPoint.y += dx.element(IndexN, 0); IndexN++;
			this->ObjPoint.z += dx.element(IndexN, 0); 
		}

		t = t.t() * dx;
		pvv = pll - t.element(0,0);
		this->sigma0 = (pvv < 0) ? 0.0 : sqrt(pvv / (nObs - nPars));
	}
	CatchAll
	{
		ret = false;
	}

	return ret;
};

bool CLSM::LSM()
{
	bool ret = true;
	rhoVec.clear();
	this->sigma0 = 0.0f;
			
	if (outputMode > eMEDIUM)
	{
		this->dump("LSM", true, true, false, true, 1.0f);
	}

	if (templateBuf && this->nSearchImgs() > 0)
	{
		double pvv;
		double pvvOld;

		bool converged = false;
		int it = 1;

		ret = iterateLSMOnce(pvvOld);


		while (ret && (it < this->matchingPars.getIterationSteps()) && (!converged))
		{
			ret = iterateLSMOnce(pvv);

			if (outputMode > eMEDIUM)
			{
				ostrstream oss;
				if (it < 10) oss << "0";
				oss << it << ends;
				char *z = oss.str();
				this->dump(z, false, false, true, false, 1.0f);
				delete [] z;
			}

			++it;
			if (ret)
			{
				double dpvv = pvv + pvvOld;
				if (fabs(dpvv) < FLT_EPSILON) converged = true;
				else 
				{
					dpvv = (pvv - pvvOld) / dpvv;
					if (fabs(dpvv) < 0.001f) converged = true;
				}
				pvvOld = pvv;
			}
		}

		if (ret) 
		{		
			this->Qxx *= this->sigma0 * this->sigma0;
			int index0 = 0;

			for (int i = 0; i < this->nSearchImgs(); ++i)
			{
				CImageBuffer *searchBuf = this->transformedSearchBuffers[i];
				searchBuf->resample(*(this->searchBuffers[i]), *(this->templateToSearchTrafos[i]), eBicubic);
				float r = searchBuf->getCorrelation(*this->templateBuf);
				rhoVec.push_back(r);	

				this->templateToSearchTrafos[i]->setCovar(this->Qxx, index0);
				this->templateToSearchTrafos[i]->transform(this->searchPoints[i], this->refPoint);
				this->searchPoints[i] *= double(this->searchBufferFactors[i]);
				index0 += this->templateToSearchTrafos[i]->nParameters();
			}
		
			if (this->hasPointObs())
			{
				SymmetricMatrix cov(3);
				int idx = this->Qxx.Nrows() - 3;
				for (int i = 0; i < 3; ++i)
				{
					for (int j = i; j < 3; ++j)
					{
						cov.element(i,j) = this->Qxx.element(idx + i, idx + j);
					}
				}

				this->ObjPoint.setCovariance(cov);
			}
		}
	}
	else ret = false;

	return ret;
};
	  
void CLSM::copyToColour(CImageBuffer &in, CImageBuffer &out, int r, int c)
{
	if (in.getBpp() == 8)
	{
		out.set(in);
	}
	else if (in.getBpp() == 16) 
	{
		out.set(in);
		unsigned short min = USHRT_MAX, max = 0;
		CHistogram *histo = in.getHistogram(0);
		min = histo->percentile(0.05);
		max = histo->percentile(0.95);
		delete histo;
		for (int i = 1; i < in.getBands(); i++)
		{
			histo = in.getHistogram(i);
			unsigned short p = histo->percentile(0.05);
			if (p < min) min = p;
			p = histo->percentile(0.95);
			if (p > max) max = p;
			 delete histo;
		};

		if (max > 255)
		{
			float fact = float(255.0f) / float(max - min);
			for (unsigned long u = 0; u < out.getNGV(); ++u)
			{
				if (in.pixUShort(u) <= min) out.pixUShort(u) = 0;
				else if (in.pixUShort(u) >= max) out.pixUShort(u) = 255;
				else out.pixUShort(u) = (unsigned short )(float(in.pixUShort(u) - min) * fact);
			}
		}

		out.convertToUnsignedChar();
	}
	else 
	{
		out.set(in);
		out.convertToUnsignedChar();
	}

	if (out.getBands() == 1)
	{
		CImageBuffer cpy(out);
		out.set(0,0,cpy.getWidth(), cpy.getHeight(), 3, cpy.getBpp(), true);
		for (unsigned int i = 0, k = 0; i < cpy.getNGV(); ++i, k+= out.getDiffX())
		{
			unsigned char c = cpy.pixUChar(i);
			for (int j = 0; j < 3; ++j)
			{
				out.pixUChar(k+j) = c;
			}
		}
	}

	if ((r >= 0) && (c >= 0) && (r < out.getHeight()) && (c < out.getWidth()))
	{
		int index = out.getIndex(r,c);
		out.pixUChar(index) = 0;
		out.pixUChar(index+1) = 255;
		out.pixUChar(index+2) = 255;
	}
};

void CLSM::dump(const char *fn, bool temp, bool origSearch, bool transformedSearch, bool weights, float coordFactor) const
{
	CCharString filenameBase(fn);
	if (!filenameBase.IsEmpty()) filenameBase += "_";
	filenameBase = this->projectDir + filenameBase;
	 
	if (temp)
	{
		CCharString filename = filenameBase + "temp.tif";
		
		int r = (int) floor(this->refPoint.y / coordFactor+ 0.5 - this->templateBuf->getOffsetY());
		int c = (int) floor(this->refPoint.x / coordFactor + 0.5 - this->templateBuf->getOffsetX());
		CImageBuffer buf;
		copyToColour(*(this->templateBuf), buf, r, c);
		buf.exportToTiffFile(filename.GetChar(), 0, true);
	}

	for (unsigned int i = 0; (i < searchBuffers.size()) && origSearch; ++i)
	{
		ostrstream oss; oss << "search_" << i << ".tif" << ends;
		char *z = oss.str();
		CCharString filename = filenameBase + z;
		delete [] z;
		int r = (int) floor(this->searchPoints[i].y / double(this->searchBufferFactors[i]* coordFactor) + 0.5 - this->searchBuffers[i]->getOffsetY());
		int c = (int) floor(this->searchPoints[i].x / double(this->searchBufferFactors[i]* coordFactor) + 0.5 - this->searchBuffers[i]->getOffsetX());
		CImageBuffer buf;
		copyToColour(*(this->searchBuffers[i]), buf, r, c);
		buf.exportToTiffFile(filename.GetChar(), 0, true);
	}

	for (unsigned int i = 0; (i < searchBuffers.size()) && transformedSearch; ++i)
	{
		ostrstream oss; oss << "search_trans_" << i << ".tif" << ends;
		char *z = oss.str();
		CCharString filename = filenameBase + z;
		delete [] z;
		transformedSearchBuffers[i]->exportToTiffFile(filename.GetChar(), 0, true);
		int r = (int) floor(this->refPoint.y / coordFactor + 0.5 - this->templateBuf->getOffsetY());
		int c = (int) floor(this->refPoint.x / coordFactor + 0.5 - this->templateBuf->getOffsetX());
		CImageBuffer buf;
		copyToColour(*(this->transformedSearchBuffers[i]), buf, r, c);
		buf.exportToTiffFile(filename.GetChar(), 0, true);
	}

	if (weights && this->templateBufWeight && this->templateBuf)
	{
		CCharString filename = filenameBase + "weight.tif";
		CImageBuffer buf(0, 0, this->templateBuf->getWidth(), this->templateBuf->getHeight(), 1, 8, true);
		for (unsigned long i = 0; i < this->templateBuf->getNGV(); i++)
		{
			buf.pixUChar(i) = (unsigned char)(255.0f * this->templateBufWeight->pixFlt(i));
		}
		
		buf.exportToTiffFile(filename.GetChar(), 0, false);
	}
};

void CLSM::initMeanVariance(CImageBuffer *buf, double &mean, double &variance)
{
	double sum = 0.0f;
	double sumsq = 0.0f;
	double size = (double) (buf->getWidth() * buf->getHeight()) ;


	if ((buf->getBpp() == 1) || (buf->getBpp() == 8))
	{
		for (unsigned long uMin = 0, uMaxRow = buf->getDiffY(); uMin < buf->getNGV(); uMin += buf->getDiffY(), uMaxRow += buf->getDiffY())
		{		
			for (unsigned u = uMin; u < uMaxRow; u += buf->getDiffX())
			{
				double dc = double(buf->pixUChar(u));
				sum   += dc;	
				sumsq += dc * dc;
			};
		};
	}		
	else if (buf->getBpp() == 16)	
	{
		for (unsigned long uMin = 0, uMaxRow = buf->getDiffY(); uMin < buf->getNGV(); uMin += buf->getDiffY(), uMaxRow += buf->getDiffY())
		{		
			for (unsigned u = uMin; u < uMaxRow; u += buf->getDiffX())
			{			
				double dc = double(buf->pixUShort(u));
				sum   += dc;	
				sumsq += dc * dc;
			};
		};
	}
	else if (buf->getBpp() == 32)
	{
		for (unsigned long uMin = 0, uMaxRow = buf->getDiffY(); uMin < buf->getNGV(); uMin += buf->getDiffY(), uMaxRow += buf->getDiffY())
		{		
			for (unsigned u = uMin; u < uMaxRow; u += buf->getDiffX())
			{
				double dc = double(buf->pixFlt(u));
				sum   += dc;	
				sumsq += dc * dc;
			};
		};
	}

	mean = sum / size; 
	variance =  (sumsq - sum * sum / size) / (size - 1.0f);
};

void CLSM::initTemplateMeanVariance()
{
	initMeanVariance(this->templateBuf, this->templateMean, this->templateVariance);
};

void CLSM::changeSearchMeanVariance(CImageBuffer *searchBuf)
{
	double tempsv = sqrt(this->templateVariance);
	
	double searchMean, searchVariance;
	this->initMeanVariance(searchBuf, searchMean, searchVariance);

	float gFact = (float) (tempsv / sqrt(searchVariance));
					
	if (searchBuf->getBpp() == 8)
	{
		for (unsigned long u = 0; u < searchBuf->getNGV(); ++u)
		{
			float g = (float) searchBuf->pixUChar(u);
			g = float((g - searchMean) * gFact + this->templateMean);
			if (g < 0) g = 0;
			else if (g > 255) g = 255;
			searchBuf->pixUChar(u) = (unsigned char) g;
		}			
	}
	else if (searchBuf->getBpp() == 16)
	{
		for (unsigned long u = 0; u < searchBuf->getNGV(); ++u)
		{
			float g = (float) searchBuf->pixUShort(u);
			g = float((g - searchMean) * gFact + this->templateMean);
			if (g < 0) g = 0;
			else if (g > 16000) g = 16000;
			searchBuf->pixUShort(u) = (unsigned short) g;
		}
	}
	else if (searchBuf->getBpp() == 32)
	{
		for (unsigned long u = 0; u < searchBuf->getNGV(); ++u)
		{
			float g = searchBuf->pixFlt(u);
			g = float((g - searchMean) * gFact + this->templateMean);
			searchBuf->pixFlt(u) = g;
		}
	}
};
	  
float CLSM::crossCorr(int x, int y, int SearchIndex)
{
	double sumS     = 0.0f;
	double sumSsq   = 0.0f;
	double sumMixed = 0.0f;

	CImageBuffer *search = this->searchBuffers[SearchIndex];


	unsigned long minSearch = search->getIndex(y - search->getOffsetY(), x - search->getOffsetX());

	if ((search->getBpp() == 1) || (search->getBpp() == 8))
	{
		for (unsigned long uMin = 0, uMaxRow = templateBuf->getDiffY(); uMin < templateBuf->getNGV(); 
			 uMin += templateBuf->getDiffY(), uMaxRow += templateBuf->getDiffY(), minSearch += search->getDiffY())
		{		
			for (unsigned u = uMin, uSearch = minSearch; u < uMaxRow; 
				 u +=templateBuf->getDiffX(), uSearch += search->getDiffX())
			{
				double ds = double(search->pixUChar(uSearch));
				double dt = double(templateBuf->pixUChar(u));
				sumS     += ds;	
				sumSsq   += ds * ds;
				sumMixed += ds * dt;
			};
		};
	}		
	else if (search->getBpp() == 16)	
	{
		for (unsigned long uMin = 0, uMaxRow = templateBuf->getDiffY(); uMin < templateBuf->getNGV(); 
			 uMin += templateBuf->getDiffY(), uMaxRow += templateBuf->getDiffY(), minSearch += search->getDiffY())
		{		
			for (unsigned u = uMin, uSearch = minSearch; u < uMaxRow; 
				 u +=templateBuf->getDiffX(), uSearch += search->getDiffX())
			{
				double ds = double(search->pixUShort(uSearch));
				double dt = double(templateBuf->pixUShort(u));
				sumS     += ds;	
				sumSsq   += ds * ds;
				sumMixed += ds * dt;
			};
		};
	}
	else if (search->getBpp() == 32)
	{
		for (unsigned long uMin = 0, uMaxRow = templateBuf->getDiffY(); uMin < templateBuf->getNGV(); 
			 uMin += templateBuf->getDiffY(), uMaxRow += templateBuf->getDiffY(), minSearch += search->getDiffY())
		{		
			for (unsigned u = uMin, uSearch = minSearch; u < uMaxRow; 
				 u +=templateBuf->getDiffX(), uSearch += search->getDiffX())
			{
				double ds = double(search->pixFlt(uSearch));
				double dt = double(templateBuf->pixFlt(u));
				sumS     += ds;	
				sumSsq   += ds * ds;
				sumMixed += ds * dt;
			};
		};
	}

	double size = (double) (templateBuf->getWidth() * templateBuf->getHeight());
	double variance   =  (sumSsq   - sumS * sumS / size)  / (size - 1.0f);
	double covariance =  (sumMixed - sumS * templateMean) / (size - 1.0f);
	
	float rho = -1.0;
	if (variance > FLT_EPSILON && templateVariance > FLT_EPSILON)
	{
		rho = float(covariance/sqrt(variance * templateVariance));
	}

	return rho;
};
