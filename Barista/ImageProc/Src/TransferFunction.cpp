#include <math.h>

#include "TransferFunction.h"
#include "StructuredFile.h"
#include "SystemUtilities.h"
#include "LookupTableHelper.h"
#include "histogram.h"


CTransferFunction::CTransferFunction(int bpp, int bands) :
	CLookupTable(bpp,bands),type(CTransferFunction::eDirectTransfer),
	lookupTableName(""),bIsInitialized(false),offsetParameters(0)
{

}

CTransferFunction::CTransferFunction(const CTransferFunction* function) :
	CLookupTable(0,0)
{
	if (!function)
	{
		this->bIsInitialized = false;
		return;
	}

	this->set(function->bands,function->bpp,false);
	this->type = function->type;
	this->lookupTableName = function->lookupTableName;
	this->bIsInitialized = function->bIsInitialized;
	this->offsetParameters = function->offsetParameters;

	for (unsigned int i=0; i < this->size() && i < function->size(); i++)
	{
		(*this)[i] = (*function)[i];
	}

	this->parameter = function->parameter;


}

CTransferFunction::CTransferFunction(const CTransferFunction& function) :
	CLookupTable(function.bands,function.bpp),type(function.type),
	lookupTableName(function.lookupTableName),bIsInitialized(function.bIsInitialized),
	parameter(function.parameter),offsetParameters(function.offsetParameters)
{
	this->set(function.bands,function.bpp,false);
	for (unsigned int i=0; i < this->size() && i < function.size(); i++)
	{
		(*this)[i] = function[i];
	}


}

CTransferFunction* CTransferFunction::copy() const
{
	return new CTransferFunction(this);
}



CTransferFunction::~CTransferFunction(void)
{

}

bool CTransferFunction::isColorIndexed() const
{
	return (this->type == eColorIndexed);
}


bool CTransferFunction::read(const char* filename)
{
	this->type = CTransferFunction::eColorIndexed;
	this->bIsInitialized = CLookupTable::read(filename);
	if (this->bIsInitialized)
		this->lookupTableName = filename;
	return this->bIsInitialized;

}


void CTransferFunction::addMinMaxToParameter(double minValue, double maxValue)
{
	this->parameter.clear(); // Clear content 
	this->parameter.push_back(minValue);
	this->parameter.push_back(maxValue);
}

double CTransferFunction::getParameter(unsigned int index) const
{ 
	if (index < this->parameter.size())
		return this->parameter[index];
	else
		return 0.0;
}

void CTransferFunction::setParameter(unsigned int index, double value)
{
	if (index < this->parameter.size())
		this->parameter[index] = value;
}

bool CTransferFunction::updateTransferFunction(const CHistogram* histogram,const vector<double>& param)
{
	if (!histogram)
		return false;

	CLookupTableHelper helper;

	switch (this->type)
	{
	case eDirectTransfer:	this->set(3,bpp,true);
							this->parameter.clear();
							break;

	case eLinearStretch:	if (parameter.size() >= 2)
							{
								this->parameter.clear();
								this->parameter.push_back(param[0]);
								this->parameter.push_back(param[1]);
							}
							else
								this->addMinMaxToParameter(histogram->getMin(),histogram->getMax());

							this->bIsInitialized = helper.createLinearLUT(*this,this->bpp,this->parameter[0],this->parameter[1]);			
							break;

	case eEqualization:		if (parameter.size() >= 2)
							{
								this->parameter.clear();
								this->parameter.push_back(param[0]);
								this->parameter.push_back(param[1]);
							}
							else
								this->addMinMaxToParameter(histogram->getMin(),histogram->getMax());
						
							this->bIsInitialized = helper.createEqualization(*this,histogram,this->bpp,this->parameter[0],this->parameter[1]);					
							break;

	case eNormalisation:	if (parameter.size() >= 4)
							{
								this->parameter.clear();
								this->parameter.push_back(param[0]);
								this->parameter.push_back(param[1]);
								this->parameter.push_back(param[2]);
								this->parameter.push_back(param[3]);	
							}
							else
							{	
								this->addMinMaxToParameter(histogram->getMin(),histogram->getMax());
								this->parameter.push_back(pow(2.0f,this->bpp)/2.0);
								this->parameter.push_back(127.0/3.0);
							}	

							this->bIsInitialized = helper.createNormalisation(*this,histogram,bpp,this->parameter[0],this->parameter[1],this->parameter[2],this->parameter[3]);
							break;

	default :	return false;
	}

	return true;
}


bool CTransferFunction::initDirectTransfer(const CFileName& imageFileName, int bpp, int bands)
{
	this->set(3,bpp,true);

	for (int i=0; i < this->maxEntries; i++)
	{
		if ((*this)[i] > 255)
			this->setColor(255,255,255,i);
	}
	this->type = CTransferFunction::eDirectTransfer;
	this->bIsInitialized = true;
	this->addLUTExtension(imageFileName,this->lookupTableName);
	return true;
}



bool CTransferFunction::initLinearStretch(const CFileName& imageFileName, int bpp, double minValue, double maxValue)
{
	this->set(3,bpp,false);
	this->type = CTransferFunction::eLinearStretch;
	this->addLUTExtension(imageFileName,this->lookupTableName);
	
	this->addMinMaxToParameter(minValue,maxValue);
	this->offsetParameters = 2;

	CLookupTableHelper helper;
	this->bIsInitialized = helper.createLinearLUT(*this,bpp,minValue,maxValue);

	return this->bIsInitialized;
}


bool CTransferFunction::initEqualization(const CFileName& imageFileName,int bpp, const CHistogram* histogram, double minValue, double maxValue)
{
	this->set(3,bpp,false);
	this->type = CTransferFunction::eEqualization;
	this->addLUTExtension(imageFileName,this->lookupTableName);

	this->addMinMaxToParameter(minValue,maxValue);
	this->offsetParameters = 2;	

	CLookupTableHelper helper;
	this->bIsInitialized = helper.createEqualization(*this,histogram,bpp,minValue,maxValue);

	return this->bIsInitialized;
}


bool CTransferFunction::initNormalisation(const CFileName& imageFileName,int bpp, const CHistogram* histogram, double minValue, double maxValue,double centre,double sigma)
{
	this->set(3,bpp,false);
	this->type = CTransferFunction::eNormalisation;
	this->addLUTExtension(imageFileName,this->lookupTableName);

	this->addMinMaxToParameter(minValue,maxValue);
	this->parameter.push_back(centre);
	this->parameter.push_back(sigma);
	this->offsetParameters = 4;

	CLookupTableHelper helper;
	this->bIsInitialized = helper.createNormalisation(*this,histogram,bpp,minValue,maxValue,centre,sigma);

	return this->bIsInitialized;
}


bool CTransferFunction::init(CTransferFunction::Type type,const CFileName& imageFileName,const CLookupTable& lut)
{
	this->type = type;
	
	// = operator of CLookupTable class
	*((CLookupTable*)this) = lut;

	this->offsetParameters = 0;

	this->bIsInitialized = true;
	this->addLUTExtension(imageFileName,this->lookupTableName);

	return true;

}





void CTransferFunction::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName relPath;
	CFileName fileSaveName = this->lookupTableName;

	// create relative path
	if (!Path.IsEmpty() && !this->lookupTableName.IsEmpty())
	{
		relPath = CSystemUtilities::RelPath(this->lookupTableName, Path);
		fileSaveName.SetPath(relPath);
	}

	CToken* pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("bands",this->bands);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("bpp",this->bpp);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("type",this->type);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("maxEntries",this->maxEntries);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("offsetParameters",this->offsetParameters);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("lookupTableName",fileSaveName.GetChar());

	CCharString valueStr ="";
	CCharString elementStr;

	// save the parameter
	pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("size of parameter",int(this->parameter.size()));

	for (unsigned int i = 0; i < this->parameter.size(); i++)
		{
			double element = this->parameter[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}
	
	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("parameter", valueStr.GetChar());



	if (!this->lookupTableName.IsEmpty())
		this->write(this->lookupTableName.GetChar());

}

void CTransferFunction::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);
	
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("bands"))
			this->bands = token->getIntValue();
		else if(key.CompareNoCase("bpp"))
            this->bpp = token->getIntValue();
		else if(key.CompareNoCase("type"))
            this->type = (CTransferFunction::Type)token->getIntValue();
		else if(key.CompareNoCase("lookupTableName"))
            this->lookupTableName = token->getValue();
		else if (key.CompareNoCase("maxEntries"))
			this->maxEntries = token->getIntValue();
		else if (key.CompareNoCase("offsetParameters"))
			this->offsetParameters = token->getIntValue();
		else if (key.CompareNoCase("size of parameter"))
			this->parameter.resize(token->getIntValue());
		else if (key.CompareNoCase("parameter"))
		{
			int size = this->parameter.size();
			if (size)
			{
				double* tmpArray = new double [size];
				token->getDoubleValues(tmpArray,size);

				for (int i=0; i< size; i++)
				{
					this->parameter[i] = tmpArray[i];
				}

				if (tmpArray) delete [] tmpArray;
			}
		}



    }


	CFileName Path = pStructuredFileSection->getProjectPath();
	
	if (!Path.IsEmpty())
	{
		CFileName CWD = CSystemUtilities::getCwd();
		CSystemUtilities::chDir(Path);

		this->lookupTableName = CSystemUtilities::AbsPath(this->lookupTableName);
		
		if (CLookupTable::read(this->lookupTableName))
		{
			this->bIsInitialized = true;
		}
		else
		{
			this->bIsInitialized = false;
		}
		

		CSystemUtilities::chDir(CWD);
	}

}

 void CTransferFunction::reconnectPointers()
 {
	 CSerializable::reconnectPointers();
 }

 void CTransferFunction::resetPointers()
 {
	 CSerializable::resetPointers();
 }

 void CTransferFunction::addLUTExtension(const CFileName& imageFileName,CFileName& lookupTableName)
{
	lookupTableName.Empty();

	lookupTableName = imageFileName;

	if (lookupTableName.GetFileExt().CompareNoCase("hug"))
	{
		lookupTableName = lookupTableName.Left(lookupTableName.GetLength() -4);
	}

	lookupTableName += ".lut";

	
}

bool CTransferFunction::getUnmappedLUT(CLookupTable& newLUT,const CHistogram* histogram)
{
	CLookupTableHelper helper;

	// init with the original bpp
	int local_bpp = histogram ? histogram->getBpp() : this->bpp;

	if (this->type == CTransferFunction::eLinearStretch || this->type == CTransferFunction::eLinearIHSStretch)
	{
		int minVal = 0, maxVal = 255;

		if (local_bpp != 32)
		{
			minVal = int(this->parameter[0]);
			maxVal = int(this->parameter[1]);
		}
		helper.createLinearLUT(newLUT,local_bpp,this->parameter[0],this->parameter[1],minVal, maxVal);
	}
	else if (this->type == CTransferFunction::eEqualization || this->type == CTransferFunction::eIHSEqualization)
	{
		helper.createEqualization(newLUT,histogram,local_bpp,this->parameter[0],this->parameter[1],int(this->parameter[0]),int(this->parameter[1]));
	}
	else if (this->type == CTransferFunction::eNormalisation || this->type == CTransferFunction::eIHSNormalisation)
	{
		helper.createNormalisation(newLUT,histogram,local_bpp,this->parameter[0],this->parameter[1],this->parameter[2],this->parameter[3],int(this->parameter[0]),int(this->parameter[1]));
	}
	else //otherwise leave the newLUT as directTransfer
	{
		CLookupTable direct(local_bpp,3);
		newLUT = direct;
	}

	return true;
}


bool CTransferFunction::changeBrightness(const CHistogram* histogram, int brightValue)
{
	CLookupTableHelper helper;
	bool flag = false;

	if (this->type == CTransferFunction::eNormalisation && this->parameter.size() >= 4)
	{
		flag = helper.createNormalisation(*this,histogram,this->getBpp(),this->parameter[0],this->parameter[1],this->parameter[2],this->parameter[3]);
	}
	else if (this->type == CTransferFunction::eEqualization && this->parameter.size() >= 2)
	{
		flag = helper.createEqualization(*this,histogram,this->getBpp(),this->parameter[0],this->parameter[1]);
	}
	else if (this->type == CTransferFunction::eLinearStretch && this->parameter.size() >= 2)
	{
		flag = helper.createLinearLUT(*this,this->getBpp(),this->parameter[0],this->parameter[1]);

	}
	else if (this->type == CTransferFunction::eDirectTransfer)
	{
		CLookupTable lut(this->getBpp(),3);
		// = operator of CLookupTable class
		*((CLookupTable*)this) = &lut;
		flag = true;
	}

	if (flag)
	{
		flag = helper.changeBrightness(*this, this->getBpp(), brightValue);
	}

	
	return flag;

}