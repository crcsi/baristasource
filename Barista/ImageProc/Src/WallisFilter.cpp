#include "WallisFilter.h"

#include "LookupTableHelper.h"
#include "histogram.h"
#include "StructuredFile.h"
#include "SystemUtilities.h"


#include <float.h>




CWallisFilter::CWallisFilter(int bpp,int bands) :
	CTransferFunction(bpp,bands)
{
	// init for default display even without parameters being set
	double max = pow(2.0f,float(this->bpp));
	double mean =  max / 2.0;
	double stdDev =  bpp == 8 ? DEFAULT_STDDEV_8BIT : DEFAULT_STDDEV_16BIT;
	
	double minValue = ROUND(mean - stdDev*2.0,max);
	double maxValue = ROUND(mean + stdDev*2.0,max);
	
	CLookupTableHelper helper;
	helper.createLinearLUT(*this,this->bpp,minValue,maxValue);	
}


CWallisFilter::CWallisFilter(const CWallisFilter* function) : 
	CTransferFunction(function),bilinearCoefX_left(function->bilinearCoefX_left),
	bilinearCoefX_right(function->bilinearCoefX_right),
	bilinearCoefY_up(function->bilinearCoefY_up),
	bilinearCoefY_down(function->bilinearCoefY_down),
	R0(function->R0),R1(function->R1),wallisFileName(function->wallisFileName)
{

}

CWallisFilter::CWallisFilter(const CWallisFilter& function) :
	CTransferFunction(function),bilinearCoefX_left(function.bilinearCoefX_left),
	bilinearCoefX_right(function.bilinearCoefX_right),
	bilinearCoefY_up(function.bilinearCoefY_up),
	bilinearCoefY_down(function.bilinearCoefY_down),
	R0(function.R0),R1(function.R1),wallisFileName(function.wallisFileName)
{

}

CTransferFunction* CWallisFilter::copy() const
{
	return new CWallisFilter(this);
}


CWallisFilter::~CWallisFilter(void)
{
}



bool CWallisFilter::init(const CFileName& imageFileName, double minValue, double maxValue)
{
	
	this->initLinearStretch(imageFileName,this->bpp,minValue,maxValue);
	this->addWALExtension(this->lookupTableName,this->wallisFileName);

	this->type = CTransferFunction::eWallisFilter;

	
	// we init the filter via setParameters() 
	this->bIsInitialized = this->readWallisFilterFile();

	return true;
}

bool CWallisFilter::setParameters(	int windowSizeX,
									int windowSizeY,
									int nParametersX,
									int nParametersY,
									double contrastParameter, 
									double brightnessParameter,
									double mean,
									double stdDev,
									int offsetX,
									int offsetY,
									int width,
									int height)
{
	// total number of parameters, includes the parameters needed by
	// the transfer function
	unsigned int nParameter = N_PARAMETERS + this->offsetParameters;
	
	if (this->parameter.size() != nParameter)
		this->parameter.resize(nParameter);

	this->parameter[this->offsetParameters + INDEX_WINDOWSIZEX] = windowSizeX;
	this->parameter[this->offsetParameters + INDEX_WINDOWSIZEY] = windowSizeY;
	this->parameter[this->offsetParameters + INDEX_CONTRAST] = contrastParameter;
	this->parameter[this->offsetParameters + INDEX_BRIGHTNESS] = brightnessParameter;
	this->parameter[this->offsetParameters + INDEX_MEAN] = mean;
	this->parameter[this->offsetParameters + INDEX_STDDEV] = stdDev;
	this->parameter[this->offsetParameters + INDEX_NPARAMETERSX] = nParametersX;
	this->parameter[this->offsetParameters + INDEX_NPARAMETERSY] = nParametersY;
	this->parameter[this->offsetParameters + INDEX_OFFSETX] = offsetX;
	this->parameter[this->offsetParameters + INDEX_OFFSETY] = offsetY;
	this->parameter[this->offsetParameters + INDEX_WIDTH] = width;
	this->parameter[this->offsetParameters + INDEX_HEIGHT] = height;


	this->initBilinearCoefs();
	this->initParameters();
	this->updateLUT();
	this->bIsInitialized = true;

	return true;
}


bool CWallisFilter::hasIdenticalParameters(	int windowSizeX,
											int windowSizeY,
											double contrastParameter, 
											double brightnessParameter,
											double mean,
											double stdDev,
											int offsetX,
											int offsetY,
											int width,
											int height)
{

	bool identical = this->isInitialized();

	unsigned int nParameter = N_PARAMETERS + this->offsetParameters;
	
	if (nParameter != this->parameter.size() ||
		fabs(windowSizeY -			this->parameter[this->offsetParameters + INDEX_WINDOWSIZEX]) > FLT_EPSILON ||
		fabs(windowSizeY -			this->parameter[this->offsetParameters + INDEX_WINDOWSIZEY]) > FLT_EPSILON ||
		fabs(contrastParameter -	this->parameter[this->offsetParameters + INDEX_CONTRAST]) > FLT_EPSILON ||
		fabs(brightnessParameter -  this->parameter[this->offsetParameters + INDEX_BRIGHTNESS]) > FLT_EPSILON ||
		fabs(mean -					this->parameter[this->offsetParameters + INDEX_MEAN]) > FLT_EPSILON ||
		fabs(stdDev -				this->parameter[this->offsetParameters + INDEX_STDDEV]) > FLT_EPSILON ||
		fabs(offsetX -				this->parameter[this->offsetParameters + INDEX_OFFSETX]) > FLT_EPSILON ||
		fabs(offsetY -				this->parameter[this->offsetParameters + INDEX_OFFSETY]) > FLT_EPSILON ||
		fabs(width -				this->parameter[this->offsetParameters + INDEX_WIDTH]) > FLT_EPSILON ||
		fabs(height -				this->parameter[this->offsetParameters + INDEX_HEIGHT]) > FLT_EPSILON )
	{
		identical = false;
	}

	return identical;
}



bool CWallisFilter::applyTransferFunction(const CImageBuffer& srcBuf,CImageBuffer& destBuf,int actualHeight,int actualWidth,int zoomFactor,bool zoomUp)
{
	if (!this->bIsInitialized || this->parameter.size() != (N_PARAMETERS + this->offsetParameters))
		return false;



	// we can use the parameters directly when zooming up, so zoomFactor needs to be one
	// we have to consider the zoomfactor when zooming down, so keep it
	zoomFactor = zoomUp ? 1 : zoomFactor;

	// offset of image buffer in current pyramid level when zooming down!!
	// offset has to describe the row and col and not the offset in an one dimensional buffer!
	int offsetX = srcBuf.getOffsetX();
	int offsetY = srcBuf.getOffsetY();

	// if buffer covers area left of image, correct the offset and make window smaller
	// this is the case when interpolation for zoomUp and zoomFactor > 1 is used
	if (offsetX < 0)
	{		
		actualWidth += offsetX;
		offsetX = 0;
	}

	if (offsetY < 0)
	{
		actualHeight += offsetY;
		offsetY =0;
	}

	int nParametersX = (int)this->parameter[this->offsetParameters + INDEX_NPARAMETERSX];
	int nParametersY = (int)this->parameter[this->offsetParameters + INDEX_NPARAMETERSY];

	int halfWindowSizeX = (int)this->parameter[this->offsetParameters + INDEX_WINDOWSIZEX] / 2;
	int halfWindowSizeY = (int)this->parameter[this->offsetParameters + INDEX_WINDOWSIZEY] / 2;

	int indexParameterY,indexParameter;
	int neighbour[3] = {1,nParametersX,nParametersX+1};

	// dummy varibles
	double x1,x2,y11,y12,y21,y22,y1,y2,r0,r1;
	int rowOffset=0;
	
	// we have to transform every pixel to his original centre in level 0 when zoomFactor > 1 and zooming down
	float a0 = float(zoomFactor) / 2.0f - 0.5f;
	float a1 = float(zoomFactor);

	int rowWindow,colWindow;
	int rowParameter,colParameter;

	int indexCoeffX,indexCoeffY;

	// transfer offset in original image coordinates
	offsetX *= zoomFactor;
	offsetY *= zoomFactor;

	// upper left and lower right corner of window were to apply filter
	// coordinates refer to original image
	C2DPoint UL(this->parameter[this->offsetParameters + INDEX_OFFSETX],
				this->parameter[this->offsetParameters + INDEX_OFFSETY]);
	
	C2DPoint LR(UL.x + this->parameter[this->offsetParameters + INDEX_WIDTH],
				UL.y + this->parameter[this->offsetParameters + INDEX_HEIGHT]);

	int startRowBuf = 0;
	int startColBuf = 0;
	int lastRowBuf = actualHeight;
	int lastColBuf = actualWidth;

	if (offsetX < int(UL.x))
		startColBuf = int((UL.x - offsetX + 1.0) / zoomFactor); // go back to buffer coordinates

	if (offsetY < int(UL.y))
		startRowBuf = int((UL.y - offsetY + 1.0) / zoomFactor); // go back to buffer coordinates

	int extentBuf = offsetX + actualWidth * zoomFactor;
	
	if ( LR.x < extentBuf) // check in image coordinates
		lastColBuf = int(actualWidth - (extentBuf - LR.x - 1.0)/ zoomFactor); // go back to buffer coordinates

	extentBuf = offsetY +actualHeight * zoomFactor;

	if (LR.y < extentBuf)
		lastRowBuf = int(actualHeight - (extentBuf - LR.y - 1.0) / zoomFactor);


	// rowWindow and colWindow refer to the actual selected area, so we have to correct that
	offsetX -= int(this->parameter[this->offsetParameters + INDEX_OFFSETX]);
	offsetY -= int(this->parameter[this->offsetParameters + INDEX_OFFSETY]);

	// this selects the right function to transform the pixel
	this->selectFunction(srcBuf.getBpp(),srcBuf.getBands());

	if (!this->transformPixel)
		return false;

	int maxWindowSizeY = int(this->parameter[this->offsetParameters + INDEX_HEIGHT]) - 1;
	int maxWindowSizeX = int(this->parameter[this->offsetParameters + INDEX_WIDTH]) - 1;

	for (int rowBuf=startRowBuf; rowBuf < lastRowBuf; rowBuf++)
	{
		rowOffset = rowBuf * srcBuf.getWidth() * srcBuf.getDiffX(); // we use here the real buffer width to get the correct index!!!
		rowWindow = int(a0 + (rowBuf) * a1) + offsetY;  // compute the position of the pixel centre in original image with respect to the selected window
		rowWindow =	ROUND(rowWindow,maxWindowSizeY); // with big zoom factors we get problems with rounding to stay in the window, so just cut the values
		rowParameter = rowWindow / halfWindowSizeY; // compute the row of the parameter
		indexCoeffY = rowWindow - rowParameter * halfWindowSizeY;  // the coefficient, used for interpolation
		indexParameterY =  rowParameter * nParametersX; // compute the index in the parameters array 

		// bilinear coefficients in y direction
		y1 = this->bilinearCoefY_up[indexCoeffY];
		y2 = this->bilinearCoefY_down[indexCoeffY];

		for (int colBuf=startColBuf; colBuf < lastColBuf; colBuf++)
		{
			colWindow = int(a0 + (colBuf) * a1) + offsetX;
			colWindow = ROUND(colWindow,maxWindowSizeX);
			colParameter = colWindow / halfWindowSizeX;	
			indexCoeffX = colWindow - colParameter * halfWindowSizeX;
			indexParameter = indexParameterY + colParameter;

			x1 = this->bilinearCoefX_left[indexCoeffX];
			x2 = this->bilinearCoefX_right[indexCoeffX];
		
			y11 = y1 * this->R0[indexParameter] +                y2 * this->R0[indexParameter + neighbour[1]];
			y12 = y1 * this->R0[indexParameter + neighbour[0]] + y2 * this->R0[indexParameter + neighbour[2]];
			y21 = y1 * this->R1[indexParameter]+                 y2 * this->R1[indexParameter + neighbour[1]];
			y22 = y1 * this->R1[indexParameter + neighbour[0]] + y2 * this->R1[indexParameter + neighbour[2]];
			
			r0 = x1 * y11 + x2 * y12;
			r1 = x1 * y21 + x2 * y22;

			this->transformPixel(r0,r1,rowOffset + colBuf*srcBuf.getDiffX(),srcBuf,destBuf);			
		} // for colBuf
	} // for rowBuf
/*
	else if (srcBuf.getBpp() == 16)
	{
			
		unsigned short* srcUsBuf = srcBuf.getPixUS();
		unsigned short* destUsBuf = destBuf.getPixUS();			
		
		for (int rowBuf=startRowBuf; rowBuf < lastRowBuf; rowBuf++)
		{
			rowOffset = rowBuf * srcBuf.getWidth(); // we use here the real buffer width to get the correct index!!!
			rowWindow = int(a0 + (rowBuf) * a1) + offsetY;  // compute the position of the pixel centre in original image with respect to the selected window
			rowParameter = rowWindow / halfWindowSizeY; // compute the row of the parameter
			indexCoeffY = rowWindow - rowParameter * halfWindowSizeY;
			indexParameterY =  rowParameter * nParametersX; // compute the index in the parameters array 

			// bilinear coefficients in y direction
			y1 = this->bilinearCoefY_up[indexCoeffY];
			y2 = this->bilinearCoefY_down[indexCoeffY];

			for (int colBuf=startColBuf; colBuf < lastColBuf; colBuf++)
			{
				colWindow = int(a0 + (colBuf) * a1) + offsetX;
				colParameter = colWindow / halfWindowSizeX;
				indexCoeffX = colWindow - colParameter * halfWindowSizeX;
				indexParameter = indexParameterY + colParameter;

				x1 = this->bilinearCoefX_left[indexCoeffX];
				x2 = this->bilinearCoefX_right[indexCoeffX];
			
				y11 = y1 * this->R0[indexParameter] +                y2 * this->R0[indexParameter + neighbour[1]];
				y12 = y1 * this->R0[indexParameter + neighbour[0]] + y2 * this->R0[indexParameter + neighbour[2]];
				y21 = y1 * this->R1[indexParameter]+                 y2 * this->R1[indexParameter + neighbour[1]];
				y22 = y1 * this->R1[indexParameter + neighbour[0]] + y2 * this->R1[indexParameter + neighbour[2]];
				
				r0 = x1 * y11 + x2 * y12;
				r1 = x1 * y21 + x2 * y22;

				destUsBuf[rowOffset + colBuf] = (unsigned short)ROUND(r0 + r1 * srcUsBuf[rowOffset + colBuf],65535);

			} // for colBuf
		} // for rowBuf
	}
	else if (srcBuf.getBpp() == 32)
	{
		
		float* srcFBuf = srcBuf.getPixF();
		float* destFBuf = destBuf.getPixF();		
		
		for (int rowBuf=startRowBuf; rowBuf < lastRowBuf; rowBuf++)
		{
			rowOffset = rowBuf * srcBuf.getWidth(); // we use here the real buffer width to get the correct index!!!
			rowWindow = int(a0 + (rowBuf) * a1) + offsetY;  // compute the position of the pixel centre in original image with respect to the selected window
			rowParameter = rowWindow/ halfWindowSizeY; // compute the row of the parameter
			indexCoeffY = rowWindow - rowParameter * halfWindowSizeY;
			indexParameterY =  rowParameter * nParametersX; // compute the index in the parameters array 

			// bilinear coefficients in y direction
			y1 = this->bilinearCoefY_up[indexCoeffY];
			y2 = this->bilinearCoefY_down[indexCoeffY];

			for (int colBuf=startColBuf; colBuf < lastColBuf; colBuf++)
			{
				colWindow = int(a0 + (colBuf) * a1) + offsetX;
				colParameter = colWindow / halfWindowSizeX;
				indexCoeffX = colWindow - colParameter * halfWindowSizeX;
				indexParameter = indexParameterY + colParameter;

				x1 = this->bilinearCoefX_left[indexCoeffX];
				x2 = this->bilinearCoefX_right[indexCoeffX];
			
				y11 = y1 * this->R0[indexParameter] +                y2 * this->R0[indexParameter + neighbour[1]];
				y12 = y1 * this->R0[indexParameter + neighbour[0]] + y2 * this->R0[indexParameter + neighbour[2]];
				y21 = y1 * this->R1[indexParameter]+                 y2 * this->R1[indexParameter + neighbour[1]];
				y22 = y1 * this->R1[indexParameter + neighbour[0]] + y2 * this->R1[indexParameter + neighbour[2]];
				
				r0 = x1 * y11 + x2 * y12;
				r1 = x1 * y21 + x2 * y22;

				destFBuf[rowOffset + colBuf] = (float)(r0 + r1 * srcFBuf[rowOffset + colBuf]);
			} // for colBuf
		} // for rowBuf
	}
	else 
		return false;
*/

	return true;
}

void CWallisFilter::initBilinearCoefs()
{
	int windowSizeX = int(this->parameter[this->offsetParameters + INDEX_WINDOWSIZEX]);
	int windowSizeY = int(this->parameter[this->offsetParameters + INDEX_WINDOWSIZEY]);

	int halfWindowSizeX = windowSizeX/2;
	int halfWindowSizeY = windowSizeY/2;


	if (this->bilinearCoefX_left.size() != halfWindowSizeX)
	{
		this->bilinearCoefX_left.resize(halfWindowSizeX);
		this->bilinearCoefX_right.resize(halfWindowSizeX);
	}

	if (this->bilinearCoefY_up.size() != halfWindowSizeY)
	{
		this->bilinearCoefY_up.resize(halfWindowSizeY);
		this->bilinearCoefY_down.resize(halfWindowSizeY);
	}


	// compute the coefficients
	
	// for x direction
	float increment = 1.0f / (float)halfWindowSizeX;
	this->bilinearCoefX_left[0] = (halfWindowSizeX - 0.5) / double(halfWindowSizeX);
	this->bilinearCoefX_right[0] = 1.0 - this->bilinearCoefX_left[0];

	for (int i = 1; i < halfWindowSizeX; i++) 
	{
		this->bilinearCoefX_left[i] = this->bilinearCoefX_left[i-1] - increment;
		this->bilinearCoefX_right[i] = 1.0 - this->bilinearCoefX_left[i];
	}

	// for y direction
	increment = 1.0f / (float)halfWindowSizeY;
	this->bilinearCoefY_up[0] = (halfWindowSizeY - 0.5)  / (double)halfWindowSizeY;
	this->bilinearCoefY_down[0] = 1.0 - this->bilinearCoefY_up[0];

	for (int i = 1; i < halfWindowSizeY; i++) 
	{
		this->bilinearCoefY_up[i] = this->bilinearCoefY_up[i-1] - increment;
		this->bilinearCoefY_down[i] = 1.0 - this->bilinearCoefY_up[i];
	}

}


void CWallisFilter::initParameters()
{
	int size = this->meanValues.size();

	if (size == 0 || this->stdDevValues.size() == 0 || size != this->stdDevValues.size())
		return;

	if ( size != this->R0.size())
	{
		this->R0.resize(size);
		this->R1.resize(size);
	}


    double a = this->parameter[this->offsetParameters + INDEX_BRIGHTNESS] * this->parameter[this->offsetParameters + INDEX_MEAN];
    double b = 1. - this->parameter[this->offsetParameters + INDEX_BRIGHTNESS];
    double c = this->parameter[this->offsetParameters + INDEX_CONTRAST] * this->parameter[this->offsetParameters + INDEX_STDDEV];
    double d = (1. - this->parameter[this->offsetParameters + INDEX_CONTRAST]) * this->parameter[this->offsetParameters + INDEX_STDDEV];

	for (int i=0; i < size; i++)
	{
		this->R1[i] = c / (this->parameter[this->offsetParameters + INDEX_CONTRAST] * this->stdDevValues[i] + d);
		this->R0[i] = a + (b - this->R1[i] ) * this->meanValues[i];	
	}

/*
	int nParametersX =  this->parameter[6];
	int nParametersY = this->parameter[7];
/*
	FILE* out= fopen("r1_t.txt","w+");

	for (int i=1; i < nParametersY-1; i++)
	{
		for (int j=1; j < nParametersX-1; j++)
		{
			fprintf(out,"%7.2lf",this->R1[i*nParametersX+j]);
		}
		fprintf(out,"\n");
	}

	fclose(out);

*/

}


bool CWallisFilter::updateTransferFunction(const CHistogram* histogram,const vector<double>& param)
{
	if (parameter.size() >= 2)
	{
		this->parameter[0] = param[0];
		this->parameter[1] = param[1];
	}

	CLookupTableHelper helper;
	this->bIsInitialized = helper.createLinearLUT(*this,this->bpp,this->parameter[0],this->parameter[1]);			

	return this->bIsInitialized;
}


void CWallisFilter::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);
	
	CTransferFunction::serializeStore(pStructuredFileSection);


	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName relPath;
	CFileName fileSaveName = this->wallisFileName;

	// create relative path
	if (!Path.IsEmpty() && !this->wallisFileName.IsEmpty())
	{
		relPath = CSystemUtilities::RelPath(this->wallisFileName, Path);
		fileSaveName.SetPath(relPath);
	}

	CToken* pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("wallisFilterFileName",fileSaveName.GetChar());

	if (!this->wallisFileName.IsEmpty())
		this->writeWallisFilterFile();


}

void CWallisFilter::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);
	
	CTransferFunction::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		CCharString key = token->getKey();

		if (key.CompareNoCase("wallisFilterFileName"))
			this->wallisFileName = token->getValue();
	
	}

	CFileName Path = pStructuredFileSection->getProjectPath();

	if (!Path.IsEmpty())
	{
		CFileName CWD = CSystemUtilities::getCwd();
		CSystemUtilities::chDir(Path);

		this->wallisFileName = CSystemUtilities::AbsPath(this->wallisFileName);
		this->bIsInitialized = this->readWallisFilterFile();

		CSystemUtilities::chDir(CWD);
	}

}


void CWallisFilter::addWALExtension(const CFileName& lookupTableName,CFileName& wallisFileName)
{
	wallisFileName.Empty();

	wallisFileName = lookupTableName;

	if (wallisFileName.GetFileExt().CompareNoCase("lut"))
	{
		wallisFileName = wallisFileName.Left(wallisFileName.GetLength() -4);
	}

	wallisFileName += ".wal";

	
}



bool CWallisFilter::readWallisFilterFile()
{

	FILE *in = fopen(this->wallisFileName.GetChar(),"rb");

	bool ret = false;
	if (in)
	{
		
		// number of wallis parameters
		unsigned int size;
		unsigned int nRead; 
		
		// check to versions
		unsigned int version;
		nRead = fread(&version,sizeof(unsigned int),1,in);

		unsigned int nParameter = N_PARAMETERS + this->offsetParameters;

		if (version == 2)
		{
			unsigned int nParameters_Read = 0;
			nRead = fread(&nParameters_Read,sizeof(unsigned int),1,in);
			
			if (nRead == 1 && nParameter && nParameters_Read == nParameter)
			{
				this->parameter.resize(nParameter);
				nRead = fread(&this->parameter[0],sizeof(double),nParameter,in);
			}	
			
			if (nRead == nParameter)
				nRead = fread(&size,sizeof(unsigned int),1,in);

		}
		else // in older versions size will be first, so we read it already
			size = version;
		
		unsigned long sizeParameters = size + 1; // if (size == sizeParameters) will only be true when we compute sizeParameters
		
		if (this->parameter.size() == nParameter)
			sizeParameters = int(this->parameter[this->offsetParameters + INDEX_NPARAMETERSX]) * int(this->parameter[this->offsetParameters + INDEX_NPARAMETERSY]);
		
		if (nRead == 1 && size == sizeParameters )
		{
			this->R0.resize(size);
			this->R1.resize(size);
			fread(&this->R0[0],sizeof(double),size,in);
			fread(&this->R1[0],sizeof(double),size,in);
			this->initBilinearCoefs();
			this->updateLUT();
			ret = true;
		}

		fclose(in);
	}

	return ret;
}


void CWallisFilter::writeWallisFilterFile()
{

	if (this->R0.size())
	{
		FILE *out = fopen(this->wallisFileName.GetChar(),"wb");
		
		// write a version number
		unsigned int version = 2;
		fwrite(&version,sizeof(unsigned int),1,out);
		
		// write all parameters
		unsigned int nParameter = N_PARAMETERS + this->offsetParameters;
		fwrite(&nParameter,sizeof(unsigned int),1,out);
		if (nParameter)
			fwrite(&this->parameter[0],sizeof(double),nParameter, out);


		unsigned int size = this->R0.size();
		fwrite(&size,sizeof(unsigned int),1,out);
		fwrite(&this->R0[0],sizeof(double),size, out);
		fwrite(&this->R1[0],sizeof(double),size, out);
		fclose(out);
	}
}

void CWallisFilter::selectFunction(int bpp,int bands)
{
	if (bands == 1)
	{
		if (bpp == 8)
			this->transformPixel = &transform8Bit_1Band;
		else if (bpp == 16)
			this->transformPixel = &transform16Bit_1Band;
		else 
			this->transformPixel = &transform32Bit_1Band;

	}
	else if (bands == 3 || bands == 4)
	{
		if (bpp == 8)
			this->transformPixel = &transform8Bit_MultiBand;
		else if (bpp == 16)
			this->transformPixel = &transform16Bit_MultiBand;
		else 
			this->transformPixel = 0;
	}
	else
		this->transformPixel = 0;


}

void CWallisFilter::updateLUT()
{
	double mean =  this->parameter[this->offsetParameters + INDEX_MEAN];
	double stdDev =  this->parameter[this->offsetParameters + INDEX_STDDEV];
	double max = pow(2.0f,float(this->bpp));
	double minValue = ROUND(mean - stdDev*2.0,max);
	double maxValue = ROUND(mean + stdDev*2.0,max);
	
	CLookupTableHelper helper;
	this->bIsInitialized = helper.createLinearLUT(*this,this->bpp,minValue,maxValue);

	this->parameter[0] = minValue;
	this->parameter[1] = maxValue;

}

bool CWallisFilter::getUnmappedLUT(CLookupTable& newLUT,const CHistogram* histogram)
{

	return true;
}


