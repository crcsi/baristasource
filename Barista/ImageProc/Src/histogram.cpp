/**
@class CHistogram
@author Harry Hanley & Jochen Willneff
@date 2-feb-05
@version 1.0
*/
#include <float.h>
#include <math.h>

#include "histogram.h"
#include "memory.h"

CHistogram::CHistogram(int BPP) : bpp(BPP),maxValue(0),minValue(0)
{
    if (bpp == 8)
        this->size = 256;
    else 
        this->size = 65536;
    
    this->histogram = new unsigned long[this->size];
    memset(this->histogram, 0, this->size*sizeof(unsigned long));

	if (bpp == 32) resetFltMinMax();
}


CHistogram::~CHistogram()
{
    delete [] this->histogram;
}

CHistogram &CHistogram::operator = (const CHistogram &other)
{
	if (this != &other)
	{
		delete [] this->histogram;
		this->size = other.size;
		this->histogram = new unsigned long[this->size];
		memcpy(this->histogram, other.histogram, this->size*sizeof(unsigned long));
		this->minValue = other.minValue;
		this->maxValue = other.maxValue;
		this->bpp = other.bpp;
	}

	return *this;
};

/**
 * computes minimum non-zero histogram value
 * @return the min value
 */
int CHistogram::minimum() const
{
    int index = this->size-1;
    for (int i = this->size-1; i >= 0; i--)
    {
        if (this->value(i) > 0)
        {
            index = i;
        }
    }
    
    return index;
}

/**
 * computes maximum non-zero histogram value
 * @return the max value
 */
int CHistogram::maximum() const
{
    int index  = 0;
    for (int i = 0; i < this->size; i++)
    {
        if (this->value(i) > 0)
        {
            index = i;
        }
    }
    
    return index;
}

float CHistogram::mean() const
{
	double sum = 0.0;
	unsigned long n = 0;
    
	for (int i = 0; i < this->size; i++)
    {
		sum += (double)(this->histogram[i] *  i);
		n += this->histogram[i];
    }

	sum /= (double) n;

    return float(sum);
};

/**
 * gets the histogram entry at i
 * @param i the index 
 * @return the value
 */
unsigned long CHistogram::value(int i) const
{
    return this->histogram[i];
}

void CHistogram::setValue(int index,unsigned long value)
{
	this->histogram[index] = value;
}

/**
 * returns the size (length) of the histogram
 * @return the size
 */
int CHistogram::getSize() const
{
    return this->size;
}

/**
 * Used for building the histogram
 * increment the entry at index i
 * @param i the index of the pixel value
 */
void CHistogram::increment(int i)
{
    // sanity check
    if ((i >= 0) && ( i < this->size))  this->histogram[i]++;
}


void CHistogram::reset()
{
	memset(this->histogram, 0, this->size * sizeof(unsigned long));
	if (this->bpp == 32) resetFltMinMax();
};

int CHistogram::percentile(double percent) const
{

    unsigned long total = 0;
    unsigned long percentagevalue = 0;
    
    for (int i = 0; i < this->size; i++)
    {
        total += this->value(i);
    }

    unsigned long target = (unsigned long)(total*percent + 0.5);
    
    int val = 0;
    
    while (percentagevalue < target)
    {
        percentagevalue += this->value(val);
        val++;
    }    
       
    return val;
}

float CHistogram::getMin() const
{
	float val;
    
	if (this->bpp == 32)
		memcpy(&val, &(this->histogram[1]), sizeof(float));
	else
		val = (float)this->minValue;

	return val;
};

float CHistogram::getMax() const
{
	float val;

	if (this->bpp == 32)
		memcpy(&val, &(this->histogram[0]), sizeof(float));
	else
		val = (float)this->maxValue;

	return val;
};

void CHistogram::resetFltMinMax()
{
	float val = -FLT_MAX;
	memcpy(&(this->histogram[0]), &val, sizeof(float));
	val = FLT_MAX;
	memcpy(&(this->histogram[1]), &val, sizeof(float));
};

void CHistogram::checkFltMinMax(float val)
{
	float testMin;
	float testMax;
	memcpy(&testMax, &(this->histogram[0]), sizeof(float));
	memcpy(&testMin, &(this->histogram[1]), sizeof(float));

	if (val > testMax) memcpy(&(this->histogram[0]), &val, sizeof(float));
	if (val < testMin) memcpy(&(this->histogram[1]), &val, sizeof(float));
};
	
bool CHistogram::writeHistogram(FILE *fp) const
{
	if (!fp) return false;
	return (fwrite(this->histogram, sizeof(unsigned long), this->size, fp) == this->size);
};

bool CHistogram::readHistogram(FILE *fp)
{
	if (!fp) return false;
	
	bool ret  = (fread(this->histogram, sizeof(unsigned long), this->size, fp) == this->size);
	
	if (ret)
		this->updateMinMax(0.0,1.0);

	return ret;
};


void CHistogram::updateMinMax(double minPercentile,double maxPercentile)
{
	if (this->bpp != 32)
	{
		this->maxValue = this->percentile(maxPercentile);
		this->minValue = this->percentile(minPercentile);
	}
}