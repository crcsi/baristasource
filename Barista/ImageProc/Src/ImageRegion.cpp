#include <math.h>
#include <float.h>
#include "ImageRegion.h"
#include "statistics.h"
#include "utilities.h"
#include "ImgBuf.h"
#include "LabelImage.h"



CImageRegion::CImageRegion(): index (0), nPixels(0),
     greyValues(0), bands(1), cog(0.0f,0.0f), extents(0.0f,0.0f),
	 covarianceInv(1), angle(0.0f), sqrGreyValues(1),covariance(1),
	 minPos(FLT_MAX, FLT_MAX), maxPos(-FLT_MAX, -FLT_MAX), xx(0.0f), xy(0.0f), yy(0.0f), NNeigh(0)

{
	sqrGreyValues = 0.0f;
	covarianceInv = 0.0f;
	covariance  = 0.0f;
	greyValues = new double[bands];
	memset(greyValues, 0, bands * sizeof(double));
};

//============================================================================

CImageRegion::CImageRegion(unsigned short I_index, int I_bands): 
	 index (I_index), nPixels(0), greyValues(0), bands(I_bands),
     cog(0.0f,0.0f), extents(0.0f,0.0f), angle(0.0f), sqrGreyValues(I_bands),
	 covarianceInv(I_bands), minPos(FLT_MAX,FLT_MAX), covariance(I_bands),
	 maxPos(-FLT_MAX, -FLT_MAX), xx(0.0f), xy(0.0f), yy(0.0f), NNeigh(0)

{
	sqrGreyValues = 0.0f;
	covarianceInv = 0.0f;
	covariance  = 0.0f;
	greyValues = new double[bands];
	memset(greyValues, 0, bands * sizeof(double));
 };

//============================================================================

CImageRegion::CImageRegion(const CImageRegion &region):
	index(region.index), nPixels(region.nPixels), greyValues(0), 
	bands(region.bands), cog(region.cog), extents(region.extents),
	angle(region.angle), covarianceInv(region.covarianceInv), covariance(region.covariance),
	minPos(region.minPos), maxPos(region.maxPos), 
	xx(region.xx), xy(region.xy), yy(region.yy), sqrGreyValues(region.sqrGreyValues), NNeigh(0)
{
	greyValues = new double[bands];
	memcpy(greyValues, region.greyValues, bands * sizeof(double));
};

//============================================================================

CImageRegion::~CImageRegion()
{
	if (greyValues) delete [] greyValues;
	greyValues = 0;
};

//============================================================================

void CImageRegion::copyData(unsigned short I_index, const CImageRegion &region)
{
	index         = I_index; 
	nPixels       = region.nPixels;
	bands         = region.bands;
	cog           = region.cog;
	extents       = region.extents;
	angle         = region.angle;
	covariance    = region.covariance;
	covarianceInv = region.covarianceInv;
	minPos        = region.minPos;
	maxPos        = region.maxPos;
	xx            = region.xx;
	xy            = region.xy;
	yy            = region.yy;
	sqrGreyValues = region.sqrGreyValues;
	NNeigh		  = region.NNeigh;

	delete [] greyValues;
	greyValues = new double[bands];
	memcpy(greyValues, region.greyValues, bands * sizeof(double));
  };

//============================================================================

void CImageRegion::resetData(unsigned short I_index)
{
	index          = I_index; 
	nPixels       = 0;
	angle         = 0.0f;
	covarianceInv = 0.0f;
	covariance    = 0.0f;
	cog.x         = 0.0f;
	cog.y         = 0.0f;
	extents.x     = 0.0f;
	extents.y     = 0.0f;
	minPos.x      = FLT_MAX;
	minPos.y      = FLT_MAX;
	maxPos.x      = -FLT_MAX;
	maxPos.y      = -FLT_MAX;
	xx            = 0.0f;
	xy            = 0.0f;
	yy            = 0.0f;
	sqrGreyValues = 0.0f;
	NNeigh		  = 0;

	memset(greyValues, 0, bands * sizeof(double));
};

float CImageRegion::getMahalanobis(float *I_greyValues) const
{
	ColumnVector diff(bands);
	for (int i = 0; i < bands; ++i) diff(i + 1) = this->greyValues[i] - I_greyValues[i];
	Matrix mat = diff.t() * covarianceInv * diff;
	return float(mat(1,1));
};
//****************************************************************************
float CImageRegion::getDistanceMetric (const CImageRegion &region) const
{
	float dist = -1.0;

	if ((nPixels > 0) && (region.nPixels > 0))
	{
//		Matrix Q = covariance / double(this->nPixels) + region.covariance/ double(region.nPixels);
		SymmetricMatrix Q = covariance + region.covariance;
		if (Q.Determinant() > FLT_EPSILON) 
		{
			Q = Q.i();
			ColumnVector diff(bands);
			for (int i = 0; i < bands; ++i) diff.element(i) = this->greyValues[i] - region.greyValues[i];
			Matrix mat = diff.t() * Q * diff;

			dist = float(mat(1,1) / statistics::chiSquareQuantil(bands));
		};
	}

	return dist;
}
//********************************************************************************
bool CImageRegion::isConsistent(float *I_greyValues) const
{
	return (this->getMahalanobis(I_greyValues) < statistics::chiSquareQuantil(bands));
};
	  
float CImageRegion::getSigma() const
{
	double sum = FLT_MAX;
	if (this->nPixels > 1)
	{
		sum = 0.0;
		for (int i = 0; i < this->bands; ++i)
		{
			sum += this->covariance.element(i,i);
		}
		sum = sqrt (sum / double(this->bands));
	}

	return float(sum);
};
//****************************************************************************
bool CImageRegion::computeRegionPars()
{
	bool ret = true;

	if (nPixels > 0)
	{
		double dPix  = (double) nPixels;
		if (nPixels == 1)  
		{
			this->covariance = 0.0f;
			this->covarianceInv = 0.0f;
		}
		else
		{
			double dPix1 = dPix - 1.0f;
			for (int i = 0; i < bands; ++i)
			{
				covariance(i+1, i+1) = (sqrGreyValues(i+1, i+1) - (greyValues[i] * greyValues[i]) / dPix) / dPix1;
				for (int j = i+1; j < bands; ++j)
				{
					covariance(i+1, j+1) = (sqrGreyValues(i+1, j+1) - (greyValues[i] * greyValues[j]) / dPix) / dPix1;
				};
				this->greyValues[i] /= dPix;
			}	
		}

		if (fabs(covariance.Determinant())< FLT_EPSILON) ret = false;
		else covarianceInv = covariance.i();

		double cmXX = xx - cog.x * cog.x / dPix;
		double cmXY = xy - cog.x * cog.y / dPix;
		double cmYY = yy - cog.y * cog.y / dPix;
		cog /= dPix;  

		double dmy = cmXX - cmYY;

		angle = float(100.0f / M_PI * atan2(2 * cmXY , dmy));
		if (cmXX < cmYY) angle += 100.0f;
		dmy = sqrt(dmy * dmy + 4.0f * cmXY * cmXY) / 2.0f;

		extents.x = extents.y = (cmXX + cmYY) / 2.0f;
		extents.x += dmy;
		extents.y -= dmy;
		extents.x = sqrt(12.0f * extents.x / dPix);
		extents.y = sqrt(12.0f * extents.y / dPix);
	}
	else 
	{	
		ret           = false;
		angle         = 0.0f;
		covarianceInv = 0.0f;
		covariance    = 0.0f;
		cog.x         = 0.0f;
		cog.y         = 0.0f;
		extents.x     = 0.0f;
		extents.y     = 0.0f;
		minPos.x      = FLT_MAX;
		minPos.y      = FLT_MAX;
		maxPos.x      = -FLT_MAX;
		maxPos.y      = -FLT_MAX;
		xx            = 0.0f;
		xy            = 0.0f;
		yy            = 0.0f;
		sqrGreyValues = 0.0f;
		memset(greyValues, 0, bands * sizeof(double));
		NNeigh		  = 0;
	}

	return ret;
};

//============================================================================

bool CImageRegion::isIdentical(const CImageRegion &region)const
{
	float dist = getDistanceMetric (region);
	bool ret = ((dist < 0) || (dist > 1.0)) ? false : true;

	return ret;
};

//============================================================================

bool CImageRegion::merge(CImageRegion &region)
{
	bool ret = true;
	if (bands != region.bands) ret = false;
	else
	{
		this->sqrGreyValues += region.sqrGreyValues;

		for (int i = 0; i < bands; ++i)
		{
			greyValues[i] = double(nPixels) * greyValues[i] + double(region.nPixels) * region.greyValues[i];
		}
		cog = cog * double(nPixels) + region.cog * double(region.nPixels);
		nPixels += region.nPixels;
		xx      += region.xx;
		xy      += region.xy;
		yy      += region.yy;
		if (region.minPos.x < minPos.x) minPos.x = region.minPos.x;
		if (region.minPos.y < minPos.x) minPos.y = region.minPos.y;
		if (region.maxPos.x > maxPos.x) maxPos.x = region.maxPos.x;
		if (region.maxPos.y > maxPos.y) maxPos.y = region.maxPos.y;

        computeRegionPars();
		region.resetData(region.index);
	}

	return ret;
};

//****************************************************************************
	  
void CImageRegion::addColourVec(float *colourVec, long x, long y)
{
	for(int i = 0; i < bands; ++i)
	{
		greyValues[i] += colourVec[i];
		for (int j =  i; j < bands; ++j)
		{
			sqrGreyValues.element(i, j) += colourVec[i] * colourVec[j];
		}
	}

	if (x < minPos.x) minPos.x = x;
	if (y < minPos.y) minPos.y = y;
	if (x > maxPos.x) maxPos.x = x;
	if (y > maxPos.y) maxPos.y = y;
	cog.x += x;
	cog.y += y;
	xx += x * x;			
	xy += x * y;
	yy += y * y;
	this->nPixels++;
};

//==================================================================================

void CImageRegion::initParameters4Computation(int nbands)
{
	this->resetData(this->index);
  
	if (nbands != this->bands)
	{
		bands = nbands;
		sqrGreyValues = SymmetricMatrix(bands);
		covarianceInv = SymmetricMatrix(bands);
		covariance = SymmetricMatrix(bands);
		sqrGreyValues = 0.0f;
		covarianceInv = 0.0f;
		covariance = 0.0f;
		delete[]  greyValues;
		greyValues = new double[bands];
		memset(greyValues, 0, bands * sizeof(double));
	};
}

//==================================================================================

void CImageRegion::initParameters(const CImageBuffer &image,
                                  const CLabelImage  &labelImage)
{
	this->initParameters4Computation(image.getBands());
  
	long y = image.getOffsetY();
	long xmax = image.getOffsetX() + image.getWidth();
	long ymax = image.getOffsetY() + image.getHeight();

	float *colourVector = new float[bands];

	for (unsigned long uMin = 0, uLabelMin = 0; y < ymax; ++y, uMin += image.getDiffY(), uLabelMin += labelImage.getDiffY())
	{
		unsigned long u = uMin;
		unsigned long uLabel = uLabelMin;
		for (long x = image.getOffsetX(); x < xmax; ++x, u += image.getDiffX(), uLabel += labelImage.getDiffX())
		{
			if (labelImage.getLabel(uLabel) == this->index)
			{
				image.getColourVec(u, colourVector);
				addColourVec(colourVector, x, y);
			}
		}
	}
  
	delete[] colourVector;
	computeRegionPars();
};


//==================================================================================

void CImageRegion::initParameters(const CLabelImage  &labelImage)
{
	this->initParameters4Computation(1);
  
	long y = labelImage.getOffsetY();
	long xmax = labelImage.getOffsetX() + labelImage.getWidth();
	long ymax = labelImage.getOffsetY() + labelImage.getHeight();

	float colourVector = 0;

	for (unsigned long uMin = 0, uLabelMin = 0; y < ymax; ++y, uMin += labelImage.getDiffY(), uLabelMin += labelImage.getDiffY())
	{
		unsigned long u = uMin;
		unsigned long uLabel = uLabelMin;
		for (long x = labelImage.getOffsetX(); x < xmax; ++x, u += labelImage.getDiffX(), uLabel += labelImage.getDiffX())
		{
			if (labelImage.getLabel(uLabel) == this->index)
			{
				addColourVec(&colourVector, x, y);
			}
		}
	}
  
	computeRegionPars();
};

//==================================================================================	  

void CImageRegion::dump(ofstream &ofile) const
{
	ofile.setf(ios::fixed, ios::floatfield);
	ofile.precision(1);

	int r,g,b;

	unsigned short s = this->index;
	if (s >= 255)  s = s % 254 + 1;
	CLabelImage::getColour(s, r, g, b);

	ofile << "Region: " << this->index 
		  << "\n  Colour in label image: " << r << " " << g << " " << b
		  << "\n  COG: " << this->cog.x << " / " << this->cog.y
		  << "\n  Number of pixels: " << this->nPixels 
		  << "\n  Number of neighbours: " << this->NNeigh
		  << "\n  Grey levels:         ";
	ofile.precision(1);
	for (int i = 0; i < this->bands; ++i) 
	{
		ofile.width(7);
		ofile << this->greyValues[i] << " ";
	}

	ofile  << "\n  Standard deviations: ";
	ofile.precision(1);
	for (int i = 0; i < this->bands; ++i) 
	{
		ofile.width(7);
		ofile << sqrt(this->covariance.element(i,i)) << " ";
	}

	ofile.precision(5);
	
	ofile << "\n  Covariance Matrix: ";
	for (int i = 0; i < this->bands; ++i)
	{
		ofile << "\n     ";
		for (int j = 0; j < this->bands; ++j)
	    {
			ofile.width(10);
			ofile << this->covariance.element(i,j);
		}
	}

	ofile <<"\n\n================================================================================\n\n";

};
//==================================================================================

 void CImageRegion::addNNeigh(int value) 
 { 
	 if ((this->NNeigh + value)>= 0) this->NNeigh = this->NNeigh + value;
	 else this->NNeigh = 0;
 };
//==================================================================================