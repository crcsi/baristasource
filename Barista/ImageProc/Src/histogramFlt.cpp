/**
@class CHistogramFlt
@author Franz Rottensteiner
@date 08-Aug-2006
@version 1.0
*/
#include<math.h>
#include<strstream>
using namespace std;


#include "histogramFlt.h"

CHistogramFlt::CHistogramFlt(float I_minEntry, float I_maxEntry, float I_binSize):
            minEntry(I_minEntry), maxEntry(I_maxEntry), binSize(I_binSize), 
            nElements (0)
{
	this->size = (unsigned long) floor((maxEntry - minEntry) / I_binSize) + 1;
	histogram = new unsigned long [size];
	for (unsigned long u = 0; u < size; u++) histogram[u] = 0;
}

CHistogramFlt::CHistogramFlt(): minEntry(0), maxEntry(100), binSize(10), 
                                nElements (0)
{
	this->size = (unsigned long) floor((maxEntry - minEntry) / binSize) + 1;
	histogram = new unsigned long [size];
	for (unsigned long u = 0; u < size; u++) histogram[u] = 0;
};

CHistogramFlt::~CHistogramFlt()
{
    delete [] this->histogram;
}

/**
 * computes minimum non-zero histogram value
 * @return the min value
 */
int CHistogramFlt::minimum() const
{
    int index = this->size-1;
    for (int i = this->size-1; i >= 0; i--)
    {
        if (this->value(i) > 0)
        {
            index = i;
        }
    }
    
    return index;
}

/**
 * computes maximum non-zero histogram value
 * @return the max value
 */
int CHistogramFlt::maximum() const
{
    int index  = 0;
    for (unsigned long i = 0; i < this->size; i++)
    {
        if (this->value(i) > 0)
        {
            index = i;
        }
    }
    
    return index;
}

float CHistogramFlt::getPercentile(double percent) const
{
	float percentile;

	unsigned long target = (unsigned long)floor(double(this->nElements) * percent + 0.5);
	
	if (target == 0)
	{
		percentile = this->minEntry;
	}
	else
	{
		unsigned long percentagevalue = 0;
		unsigned long val = 0;
	    
		while (percentagevalue < target)
		{
			percentagevalue += this->value(val);
			val++;
		}    
	    

		if (val == 1) 
		{
			percentile = this->minEntry + float(target) / float(percentagevalue) * this->binSize;
		}
		else if (val == this->nElements)
		{
			percentile = this->maxEntry;
		}
		else
		{
			unsigned long curperc = this->value(val - 1);
			percentile = (float(val) - (float(percentagevalue - target) / float(curperc))) * this->binSize;
			percentile += this->minEntry;
		}
	}

    return percentile;
}
   
void CHistogramFlt::init(float I_minEntry, float I_maxEntry, float I_binSize)
{
	minEntry  = I_minEntry;
	maxEntry  = I_maxEntry;
 	binSize   = I_binSize;          
 	nElements = 0;
  	this->size = (unsigned long) floor((maxEntry - minEntry) / I_binSize) + 1;
	if (histogram) delete [] histogram;
	histogram = new unsigned long [size];
	for (unsigned long u = 0; u < size; u++) histogram[u] = 0;
};
    
void CHistogramFlt::init() 
{ 
	for (unsigned long u = 0; u < size; u++) histogram[u] = 0;
	nElements = 0; 
};
    
unsigned long CHistogramFlt::getIndex(float value) const
{
	unsigned long histIndex = 0;
	if (value >= minEntry) 
		histIndex = (unsigned long) floor(((value - minEntry) / binSize) + 0.5f);
	if (histIndex >= this->getSize()) histIndex = this->getSize() - 1;
	return histIndex;
};

void CHistogramFlt::addValue(float value)
{
	increment(getIndex(value));
}

void CHistogramFlt::addValue(long value)
{
	increment(getIndex(float(value)));
};

void CHistogramFlt::increment(const unsigned long &u)
{
	++histogram[u];
	++nElements;
};


CCharString CHistogramFlt::getDescriptorString(const CCharString &heading, int headingPrecision, const CCharString &separator, int &colWidth) const
{
	CCharString str;

	if (this->getSize() > 0)
	{
		int nheadDigits = heading.GetLength() + 1;	
		if (colWidth < 0)
		{
			colWidth = 1;
			unsigned long Max = maximum();
			if (Max > 9) colWidth = int (floor(log10(float(Max)))) + 1;
		}

		int maxEntryWidth = int(floor(log10(float(fabs(this->maxEntry))))) + 1;
		maxEntryWidth += headingPrecision + 2;
		if (maxEntryWidth > colWidth) colWidth = maxEntryWidth;

		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(headingPrecision);

		oss.width(nheadDigits);
		oss << heading << separator;

		for (unsigned int i = 0; i < (this->getSize() - 1); ++i)
		{
			oss.width(colWidth);
			oss << getClassCentre(i) << separator;
		}

		ostrstream oss1; 
		oss1.setf(ios::fixed, ios::floatfield);
		oss1.precision(headingPrecision);
		oss1 << "<" << this->maxEntry << ends;
		char *z = oss1.str();

		oss.width(colWidth);
		oss << z << "\n" << ends;
		delete [] z;
		z = oss.str();
		str = z;
		delete [] z;
	}

	return str;
};
	
CCharString CHistogramFlt::getString(const CCharString &heading, int headingPrecision, const CCharString &separator) const
{
	int nheadDigits = heading.GetLength() + 1;
	int colWidth = -1;
	CCharString str = getDescriptorString(heading, headingPrecision, separator, colWidth);

	if (this->getSize() > 0)
	{
		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(headingPrecision);

		oss.width(nheadDigits);
		oss << " " << separator;

		for (unsigned int i = 0; i < this->getSize(); ++i)
		{
			oss.width(colWidth);
			oss << this->histogram[i] << separator;
		}

		oss << "\n" << ends;
		char *z = oss.str();
		str += z;
		delete [] z;
	}

	return str;
};


void CHistogramFlt::init(vector<float>& classCentres)
{
	unsigned int size = classCentres.size();
	if (size < 2)
	{
		this->size = 0;
		if (this->histogram) delete [] this->histogram;
		this->histogram = 0;
		this->nElements = 0;
		return;
	}
		

	this->binSize   = classCentres[1] - classCentres[0];

	this->minEntry  = classCentres[0] - this->binSize / 2.0f;
	this->maxEntry  = classCentres[size -1] + this->binSize / 2.0f;
 	
 	this->nElements = 0;
  	this->size = size ;
	if (this->histogram) delete [] this->histogram;
	this->histogram = new unsigned long [size];
	for (unsigned long u = 0; u < size; u++) this->histogram[u] = 0;
}
