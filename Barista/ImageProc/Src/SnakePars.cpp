#include <math.h>
#include <strstream>
using namespace std;

#include "SnakePars.h"
#include "Filename.h"
#include "statistics.h"


SnakeExtractionParameters::SnakeExtractionParameters(): Mode(eTraditional), openSnake(true), buffer((unsigned int) 50), 
		stopCriteria((float) 0.5f), sigma((float) 1.0f), gamma((float) 0.1f), snakeSpace((unsigned int) 10),
		mo((float) 0.1f), alpha((float) 0.01f), beta((float) 3.0f), kappa((float) 0.01f), ka((float) 0.1f)
{
};

SnakeExtractionParameters::SnakeExtractionParameters(const SnakeExtractionParameters &pars): 
	   Mode (pars.Mode), openSnake(pars.openSnake), buffer(pars.buffer), 
	   stopCriteria(pars.stopCriteria), sigma(pars.sigma), gamma(pars.gamma),
	   snakeSpace(pars.snakeSpace), mo(pars.mo), 
	   alpha(pars.alpha), beta(pars.beta), 
	   kappa(pars.kappa), ka(pars.ka)
{
};

void SnakeExtractionParameters::writeParameters(const CFileName &fileName) const
{
	ofstream outFile(fileName.GetChar());
	if (outFile.good())
	{
		outFile.setf(ios::fixed, ios::floatfield);
		outFile.precision(3);

		outFile << "SnakeType:" << this->Mode
				<< "\n OpenSnake: " << this->openSnake 
				<< "\n Buffer: " << this->buffer 
				<< "\n StopCriteria: " << this->stopCriteria 
				<< "\n Sigma: " << this->sigma
				<< "\n Gamma: " << this->gamma 
				<< "\n SnakeSpace: " << this->snakeSpace 
				<< "\n Mo: " << this->mo
				<< "\n Alpha: " << this->alpha
				<< "\n Beta: " << this->beta 
				<< "\n Kappa: " << this->kappa
				<< "\n Ka: " << this->ka 
				<< "\n";
	}
};

bool SnakeExtractionParameters::initParameters(const CFileName &fileName)
{
	ifstream inFile(fileName.GetChar());
	int len = 2048;
	char *z = new char[len];
	bool ret = true;
	if (inFile.good())
	{
		inFile.getline(z, len);

		while (inFile.good())
		{
			CCharString arg(z);
			int pos1 = arg.Find("(");
			int pos2 = arg.Find(")");
			if ((pos1 > 0) && (pos2 > pos1))
			{
				CCharString key = arg.Left(pos1);
				arg = arg.Left(pos2);
				arg = arg.Right(arg.GetLength() - pos1 - 1);
				if (key == "Mode") 
				{
					istrstream iss(arg.GetChar());
					int m;
					iss >> m;
					this->Mode = (eSnakeMode) m;
				}
				else if (key == "OpenSnake")
				{
					istrstream iss(arg.GetChar());
					iss >> this->openSnake;
				}
				else if (key == "Buffer")
				{
					istrstream iss(arg.GetChar());
					iss >> this->buffer;
				}
				else if (key == "StopCriteria")
				{
					istrstream iss(arg.GetChar());
					iss >> this->stopCriteria;
				}
				else if (key == "Sigma")
				{
					istrstream iss(arg.GetChar());
					iss >> this->sigma;
				}
				else if (key == "Gamma")
				{
					istrstream iss(arg.GetChar());
					iss >> this->gamma;
				}
				else if (key == "SnakeSpace")
				{
					istrstream iss(arg.GetChar());
					iss >> this->snakeSpace;
				}
				else if (key == "Mo")
				{
					istrstream iss(arg.GetChar());
					iss >> this->mo;
				}
				else if (key == "Alpha")
				{
					istrstream iss(arg.GetChar());
					iss >> this->alpha;
				}
				else if (key == "Beta")
				{
					istrstream iss(arg.GetChar());
					iss >> this->beta;
				}
				else if (key == "Kappa")
				{
					istrstream iss(arg.GetChar());
					iss >> this->kappa;
				}
				else if (key == "Ka")
				{
					istrstream iss(arg.GetChar());
					iss >> this->ka;
				}
			}
			inFile.getline(z, len);
		}
	}
	else
		ret = false;
	delete [] z;

	return ret;
};