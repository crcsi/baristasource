#include "IHSTransferFunction.h"

#include "LookupTableHelper.h"
#include "histogram.h"

CIHSTransferFunction::CIHSTransferFunction(int bpp) :
	CTransferFunction(bpp,1)
{

}


CIHSTransferFunction::CIHSTransferFunction(const CIHSTransferFunction* function) : 
	CTransferFunction(function)
{

}

CIHSTransferFunction::CIHSTransferFunction(const CIHSTransferFunction& function) :
	CTransferFunction(function)
{

}

CIHSTransferFunction::CIHSTransferFunction(const CTransferFunction* function) :
	CTransferFunction(function)
{

}

CIHSTransferFunction::CIHSTransferFunction(const CTransferFunction& function):
	CTransferFunction(function)
{

}


CTransferFunction* CIHSTransferFunction::copy() const
{
	return new CIHSTransferFunction(this);
}


CIHSTransferFunction::~CIHSTransferFunction(void)
{
}


bool CIHSTransferFunction::initLinearStretchIHS(const CFileName& imageFileName,int bpp, double minValue, double maxValue)
{

	this->type = CTransferFunction::eLinearIHSStretch;
	this->set(3,bpp,false);
	this->addLUTExtension(imageFileName,this->lookupTableName);
	
	this->addMinMaxToParameter(minValue,maxValue);

	CLookupTableHelper helper;
	this->bIsInitialized = helper.createLinearLUT(*this,bpp,minValue,maxValue);

	return this->bIsInitialized;


}

bool CIHSTransferFunction::initEqualizationIHS(const CFileName& imageFileName,int bpp, const CHistogram* intensityHist, double minValue, double maxValue)
{
	if (!intensityHist)
		return false;

	this->type = CTransferFunction::eIHSEqualization;
	this->set(3,bpp,false);
	this->addLUTExtension(imageFileName,this->lookupTableName);
	
	this->addMinMaxToParameter(minValue,maxValue);

	CLookupTableHelper helper;
	this->bIsInitialized = helper.createEqualization(*this,intensityHist,bpp,minValue,maxValue);

	return this->bIsInitialized;	
}

bool CIHSTransferFunction::initNormalisationIHS(const CFileName& imageFileName,int bpp, const CHistogram* intensityHist, double minValue, double maxValue,double centre, double sigma)
{
	if (!intensityHist)
		return false;

	this->type = CTransferFunction::eIHSNormalisation;
	this->set(3,bpp,false);
	this->addLUTExtension(imageFileName,this->lookupTableName);
	
	this->addMinMaxToParameter(minValue,maxValue);
	this->parameter.push_back(centre);
	this->parameter.push_back(sigma);


	CLookupTableHelper helper;
	this->bIsInitialized = helper.createNormalisation(*this,intensityHist,bpp,minValue,maxValue,centre,sigma);

	return this->bIsInitialized;	
}



bool CIHSTransferFunction::updateTransferFunction(const CHistogram* histogram,const vector<double>& param)
{
	if (!histogram)
		return false;

	CLookupTableHelper helper;

	switch (this->type)
	{
	case eDirectTransfer:	CTransferFunction::updateTransferFunction(histogram,param);
							break;

	case eLinearIHSStretch:	if (parameter.size() == 2)
							{
								this->parameter.clear();
								this->parameter.push_back(param[0]);
								this->parameter.push_back(param[1]);
							}
							else
								this->addMinMaxToParameter(histogram->getMin(),histogram->getMax());

							this->bIsInitialized = helper.createLinearLUT(*this,this->bpp,this->parameter[0],this->parameter[1]);			
							break;

	case eIHSEqualization:		if (parameter.size() == 2)
							{
								this->parameter.clear();
								this->parameter.push_back(param[0]);
								this->parameter.push_back(param[1]);
							}
							else
								this->addMinMaxToParameter(histogram->getMin(),histogram->getMax());
						
							this->bIsInitialized = helper.createEqualization(*this,histogram,this->bpp,this->parameter[0],this->parameter[1]);					
							break;

	case eIHSNormalisation:	if (parameter.size() == 4)
							{
								this->parameter.clear();
								this->parameter.push_back(param[0]);
								this->parameter.push_back(param[1]);
								this->parameter.push_back(param[2]);
								this->parameter.push_back(param[3]);	
							}
							else
							{	
								this->addMinMaxToParameter(histogram->getMin(),histogram->getMax());
								this->parameter.push_back(pow(2.0f,this->bpp)/2.0);
								this->parameter.push_back(127.0/3.0);
							}	

							this->bIsInitialized = helper.createNormalisation(*this,histogram,bpp,this->parameter[0],this->parameter[1],this->parameter[2],this->parameter[3]);
							break;

	default :	CTransferFunction::updateTransferFunction(histogram,param);
	}

	return true;
}


bool CIHSTransferFunction::changeBrightness(const CHistogram* histogram, int brightValue)
{
	CLookupTableHelper helper;
	bool flag = false;

	if (this->type == CTransferFunction::eIHSNormalisation && this->parameter.size() >= 4)
	{
		flag = helper.createNormalisation(*this,histogram,this->getBpp(),this->parameter[0],this->parameter[1],this->parameter[2],this->parameter[3]);
	}
	else if (this->type == CTransferFunction::eIHSEqualization && this->parameter.size() >= 2)
	{
		flag = helper.createEqualization(*this,histogram,this->getBpp(),this->parameter[0],this->parameter[1]);
	}
	else if (this->type == CTransferFunction::eLinearIHSStretch && this->parameter.size() >= 2)
	{
		flag = helper.createLinearLUT(*this,this->getBpp(),this->parameter[0],this->parameter[1]);

	}
	else if (this->type == CTransferFunction::eDirectTransfer)
	{
		CLookupTable lut(this->getBpp(),3);
		// = operator of CLookupTable class
		*((CLookupTable*)this) = &lut;
		flag = true;
	}

	if (flag)
	{
		flag = helper.changeBrightness(*this, this->getBpp(), brightValue);
	}

	
	return flag;

}