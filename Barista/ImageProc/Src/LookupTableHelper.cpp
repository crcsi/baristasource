#include <math.h>

#include "LookupTableHelper.h"

#include "TransferFunction.h"
#include "LookupTable.h"
#include "histogram.h"
#include "CharString.h"
#include "utilities.h"
#include "ImgBuf.h"



CLookupTableHelper::CLookupTableHelper(void) : 
	MinGrey(0),MaxGrey(255),
	MinColour(0),MaxColour(255),
	MinNIR(0),MaxNIR(255),
	MinRed(0),MaxRed(255),
	MinGreen(0),MaxGreen(255),
	MinBlue(0),MaxBlue(255),
	colourFactor(1.0),greyFactor(1.0),
	clipValueMax(255),clipValueMin(0)
{

}

CLookupTableHelper::~CLookupTableHelper(void)
{

}

bool CLookupTableHelper::initMinMaxValues(double minValue, double maxValue)
{
    this->MaxGrey = (int)maxValue;
    this->MinGrey = (int)minValue;
	this->greyFactor = (float)(float(this->clipValueMax - this->clipValueMin) / (this->MaxGrey - this->MinGrey));
	
	return true;
}

bool CLookupTableHelper::initMinMaxValues(const vector<CHistogram*>& histograms,int bands, int bpp)
{
	if (!histograms.size())
		return false;
	
	if (!initMinMaxValues(histograms[0]->getMin(),histograms[0]->getMax()))
		return false;

    if (bpp == 8 || bpp == 16)
    {
        if (bands == 3 && histograms.size() == 3)
        {
            this->MinRed   = (int)histograms[0]->getMin();         
            this->MaxRed   = (int)histograms[0]->getMax();
            this->MinGreen = (int)histograms[1]->getMin();
            this->MaxGreen = (int)histograms[1]->getMax();
            this->MinBlue  = (int)histograms[2]->getMin();
            this->MaxBlue  = (int)histograms[2]->getMax();

			if ((this->MinRed < this->MinGreen) && (this->MinRed < this->MinBlue)) 
				this->MinColour = this->MinRed;
			else if (this->MinGreen < this->MinBlue) 
				this->MinColour = this->MinGreen;
			else
				this->MinColour = this->MinBlue;

			if ((this->MaxRed > this->MaxGreen) && (this->MaxRed > this->MaxBlue)) 
				this->MaxColour = this->MaxRed;
			else if (this->MaxGreen > this->MaxBlue) 
				this->MaxColour = this->MaxGreen;
			else 
				this->MaxColour = this->MaxBlue;

			this->colourFactor = (float)(float(this->clipValueMax - this->clipValueMin) / (this->MaxColour - this->MinColour));
        }
        else if (bands == 4 && histograms.size() == 4)
        {
            this->MinRed   = (int)histograms[0]->getMin();         
            this->MaxRed   = (int)histograms[0]->getMax();
            this->MinGreen = (int)histograms[0]->getMin();
            this->MaxGreen = (int)histograms[0]->getMax();
            this->MinBlue  = (int)histograms[0]->getMin();
            this->MaxBlue  = (int)histograms[0]->getMax();
            this->MinNIR   = (int)histograms[0]->getMin();
            this->MaxNIR   = (int)histograms[0]->getMax();

			if ((MinNIR < MinRed) && (MinNIR < MinGreen) && (MinNIR < MinBlue)) MinColour = MinNIR;
			else if ((MinRed < MinGreen) && (MinRed < MinBlue)) MinColour = MinRed;
			else if (MinGreen < MinBlue) MinColour = MinGreen;
			else MinColour = MinBlue;

			if ((MaxNIR > MaxRed) && (MaxNIR > MaxGreen) && (MaxNIR > MaxBlue)) MaxColour = MaxNIR;
			else if ((MaxRed > MaxGreen) && (MaxRed > MaxBlue)) MaxColour = MaxRed;
			else if (MaxGreen > MaxBlue) MaxColour = MaxGreen;
			else MaxColour = MaxBlue;

			colourFactor = (float)(float(this->clipValueMax - this->clipValueMin) / (MaxColour - MinColour));
        }
    }
	else if (bpp == 32)
	{
		if (bands == histograms.size())
		{
			for (int i = 1; i < bands; ++i)
			{
				float val = histograms[i]->getMax();
				if (val < MinGrey) MinGrey = (int)val;
				if (val > MaxGrey) MaxGrey = (int)val;
				val = histograms[i]->getMin();
				if (val < MinGrey) MinGrey = (int)val;
				if (val > MaxGrey) MaxGrey = (int)val;
			}

			greyFactor = (float)(float(this->clipValueMax - this->clipValueMin) / (MaxGrey - MinGrey));
		}
	}

	return true;
}


bool CLookupTableHelper::createLinearLUT(CLookupTable& newLUT,int bpp,double minValue, double maxValue,int clipValueMin,int clipValueMax) 
{
	if (newLUT.getBands() != 3 || newLUT.getBpp() != bpp)
		newLUT.set(3,bpp,false);
	
	this->setClipValues(clipValueMin,clipValueMax,bpp);

	if (!this->initMinMaxValues(minValue,maxValue))
		return false;

	int min =0;
	int max =0;


	if (bpp == 32)
	{
		min = scaleGrey(0,(float)this->MinGrey);
		max = scaleGrey(0,(float)this->MaxGrey);;
	}
	else
	{

		min = this->MinGrey;
		max = this->MaxGrey;
	}

	for (int i=0; i < min; i++)
		newLUT.setColor(this->clipValueMin,this->clipValueMin,this->clipValueMin,i);
	
	for (int i=max; i < newLUT.getMaxEntries(); i++)
		newLUT.setColor(this->clipValueMax,this->clipValueMax,this->clipValueMax,i);

	int value;
	for (int i= min; i < max; i++)
	{
		if (bpp == 32)
			value = i;
		else
			value = scaleGrey(0,(unsigned short)i);
		newLUT.setColor(value,value,value,i);
	}

	return true;
}


bool CLookupTableHelper::createDEMDiffLUT(CLookupTable& newLUT,
										  double minValue, 
										  double zeroValue, 
										  double maxValue)
{
	if (newLUT.getBands() != 3 || newLUT.getBpp() != 32)
		newLUT.set(3,32,false);

	if (!this->initMinMaxValues(minValue,maxValue))
		return false;


	int min = scaleGrey(0,(float)this->MinGrey);
	int max = scaleGrey(0,(float)this->MaxGrey);

	// fill with red and green when outside of interval
	for (int i=0; i < min; i++)
		newLUT.setColor(255,0,0,i);
	
	for (int i=max; i < newLUT.getMaxEntries()-1; i++)
		newLUT.setColor(0,255,0,i);

	// compute scale, always use the biger difference
	double scale = 1;
	int zero = scaleGrey(0,(float)zeroValue);
	int high = scaleGrey(0,(float)maxValue);
	int low = scaleGrey(0,(float)minValue);

	if ( (high - zero) > (zero - low))
		scale = 255.0 / double(high - zero);
	else
		scale = 255.0 / double(zero - low);

	// red
	for (int i= zero; i >= min; i--)
	{
		int r = 255,g = 255,b = 255;
		g -= int((zero - i) * scale);
		b -= int((zero - i) * scale);

		newLUT.setColor(r,g,b,i);
	}

	//green
	for (int i= zero; i <= max; i++)
	{
		int r = 255,g = 255,b = 255;
		r -= int((i - zero) * scale);
		b -= int((i - zero) * scale);
		newLUT.setColor(r,g,b,i);

	}

	return true;
}

/*
void CLookupTableHelper::changeBrightness(CLookupTable* lut,CHugeImage* himage,int value)
{
	this->initMinMaxValues(himage);
	this->createLinearLUT(lut,himage);

	for (unsigned int i=0; i< lut->size(); i++)
	{
		(*lut)[i] = brighten(value,(*lut)[i]);
	}
}

*/
bool CLookupTableHelper::createColorLegend(	const CTransferFunction* transferFunction,
											double minValue, 
											double zeroValue, 
											double maxValue,
											vector<int> &r,
											vector<int> &g,
											vector<int> &b,
											vector<CCharString> &text,
											int entries)
{
	if (transferFunction->getBands() != 3 )
		return false;

	this->initMinMaxValues(minValue,maxValue);

	int min  = scaleGrey(0,(float)minValue);
	int max  = scaleGrey(0,(float)maxValue);
	int zero = scaleGrey(0,(float)zeroValue);

	vector<float> classes;
	this->computeFloatClasses(classes,entries,(float)minValue,(float)zeroValue,(float)maxValue);

	
	r.clear();
	g.clear();
	b.clear();
	text.clear();

	int red,green,blue;
	for (unsigned int i=0; i< classes.size(); i++)
	{
		transferFunction->getColor(red,green,blue,scaleGrey(0,classes[i]));
		r.push_back(red);
		g.push_back(green);
		b.push_back(blue);

		CCharString t;
		t.Format(" %7.2lf",classes[i]);
		text.push_back(t);
	}

	return true;

}

void CLookupTableHelper::computeFloatClasses(vector<float>& classes,int nClasses,float minValue,float zeroValue,float maxValue)
{
	float scale = float(nClasses)  / (maxValue - minValue);
	float diff = (maxValue - minValue) / float(nClasses);
	int nSmallerThenZero = (int)(Round((zeroValue - minValue) *scale,0));
	int nBiggerThenZero = (int)(Round((maxValue - zeroValue) *scale,0));

	for (int i=nSmallerThenZero; i > 0; i--)
		classes.push_back(zeroValue - diff * i);

	for (int i=0; i <nBiggerThenZero; i++)
		classes.push_back(zeroValue + diff * i);

}


bool CLookupTableHelper::createEqualization(CLookupTable& newLUT,const CHistogram* histogram,int bpp, double minValue, double maxValue,int clipValueMin,int clipValueMax)
{
	if (newLUT.getBands() != 3 || newLUT.getBpp() != bpp)
		newLUT.set(3,bpp,false); // set parameters for new lookup table (re-doing!)

	this->setClipValues(clipValueMin,clipValueMax,bpp);

	if (!this->initMinMaxValues(minValue,maxValue))
		return false;	
	

	int min =0;
	int max =0;

	if (bpp == 32)
	{
		min = scaleGrey(0,(float)this->MinGrey);
		max = scaleGrey(0,(float)this->MaxGrey);;
	}
	else
	{

		min = this->MinGrey;
		max = this->MaxGrey;
	}

	double sum = 0.0;
	for (int i=min; i < max; i++)
	{
		sum += histogram->value(i);
	}

	for (int i=0; i < min; i++)
	{
		newLUT.setColor(this->clipValueMin,this->clipValueMin,this->clipValueMin,i);
	}
	

	double sumHist = 0.0;
	int value;

	for (int i = min; i < max; i ++)
	{
		// compute sum histogram (sumHist[max] close to 1.0)
		sumHist += histogram->value(i) / sum;
		
		value = int (sumHist * (this->clipValueMax - this->clipValueMin)) + this->clipValueMin ;  
		newLUT.setColor(value,value,value,i);
	}


	for (int i=max; i < newLUT.getMaxEntries(); i++)
		newLUT.setColor(this->clipValueMax,this->clipValueMax,this->clipValueMax,i);

	return true;
}

bool CLookupTableHelper::changeBrightness(CLookupTable& newLUT, int bpp, int value, int clipValueMin,int clipValueMax)
{
	if (newLUT.getBands() != 3 || newLUT.getBpp() != bpp)
		newLUT.set(3,bpp,false); // set parameters for new lookup table (re-doing!)


	for (unsigned int i=0; i < newLUT.size(); i++)
	{
		newLUT.at(i) += value;

		if (newLUT.at(i) < clipValueMin) newLUT.at(i) = clipValueMin;
		else if (newLUT.at(i) > clipValueMax) newLUT.at(i) = clipValueMax;
	}	

	return true;
}


bool CLookupTableHelper::createNormalisation(CLookupTable& newLUT,const CHistogram* histogram,int bpp, double minValue, double maxValue,double centre,double sigma,int clipValueMin,int clipValueMax)
{
	if (!histogram)
		return false;

	this->setClipValues(clipValueMin,clipValueMax,bpp);

	if (!this->initMinMaxValues(minValue,maxValue))
		return false;	
	
	int min =0;
	int max =0;

	if (bpp == 32)
	{
		min = scaleGrey(0,(float)this->MinGrey);
		max = scaleGrey(0,(float)this->MaxGrey);;
	}
	else
	{

		min = this->MinGrey;
		max = this->MaxGrey;
	}


	double realCentre = centre * (this->clipValueMax - this->clipValueMin) + this->clipValueMin;
	vector<double> aimedHist(this->clipValueMax - this->clipValueMin + 1);

	aimedHist[0] = 1.0/(sigma*sqrt(2*M_PI))*exp(-0.5*pow((this->clipValueMin-realCentre)/sigma,2));

	for (unsigned int i=  1; i < aimedHist.size(); i++)
		aimedHist[i] = aimedHist[i -1] + 1.0/(sigma*sqrt(2.0*M_PI))*exp(-0.5*pow((i + this->clipValueMin - realCentre)/sigma,2.0));

	
	this->matchHistogram(newLUT,histogram,aimedHist,bpp,min,max);


	return true;
}


bool CLookupTableHelper::matchHistogram(CLookupTable& newLUT,const CHistogram* hist, const vector<double>& sumHistToMatch,int bpp, int min, int max)
{
	if (!hist || bpp == 32)
		return false;

	int size = sumHistToMatch.size();
	int indexMatch = 0;
	double sum = 0;

	for (int i= min; i < max; i++)
		sum += hist->value(i);


	double sumHistOriginal = 0.0;

	for (int i=0; i < min; i++)
	{
		newLUT.setColor(this->clipValueMin,this->clipValueMin,this->clipValueMin,i);
	}

	for (int i= min; i < max; i++)
	{
		sumHistOriginal += double(hist->value(i)) / sum;
		for (int k=indexMatch; k < size; k++)
		{
			if (sumHistToMatch[k] > sumHistOriginal)
			{
				indexMatch = k;
				break;
			}
		}

		newLUT.setColor(indexMatch + this->clipValueMin,indexMatch + this->clipValueMin,indexMatch + this->clipValueMin,i);
		
	}

	for (int i= max ; i < hist->getSize(); i++)
		newLUT.setColor(this->clipValueMax,this->clipValueMax,this->clipValueMax,i);

	return true;
}

	
CLookupTable CLookupTableHelper::getDSMLookup()
{
	CLookupTable lut(8, 3);
	float i = 128.0, s = 1.0, h, r, g, b;

	float fact = (float) 2.0f / (3.0f * (float) lut.getMaxEntries());

	for (int index = 0; index < lut.getMaxEntries(); ++index)
	{
		h = float(lut.getMaxEntries() - (index + 1)) * fact;
		CImageBuffer::IHS2RGB(i, h, s, r, g, b);
		if (r <   0) r =   0;
		else if (r > 255) r = 255;
		if (g <   0) g =   0;
		else if (g > 255) g = 255;
		if (b <   0) b =   0;
		else if (b > 255) b = 255;
		lut.setColor(int(r), int(g),int(b), index);

	}
	return lut;
};

void CLookupTableHelper::setClipValues(int clipMin,int clipMax,int bpp)
{
	int max = int(pow(2.0f,bpp)) - 1;
	int min = 0;
	this->clipValueMax = clipMax;
	if (this->clipValueMax > max) 
		this->clipValueMax = max;
	else if (this->clipValueMax < min)
		this->clipValueMax  = min;

	this->clipValueMin = clipMin;
	if (this->clipValueMin > max) 
		this->clipValueMin = max;
	else if (this->clipValueMin < min)
		this->clipValueMin  = min;
}
