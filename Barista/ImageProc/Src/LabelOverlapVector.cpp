//============================================================================
//
// File        : LabelOverlapVector.c
//
// Author      : FR
//
// Description : Implementation of class CLabelOverlapVector, a vector of multiple 
//               overlap records
//
//============================================================================
#include <math.h>
#include <strstream>
using namespace std;

#include "ProtocolHandler.h"
#include "LabelOverlapVector.h"

CLabelOverlapVector::CLabelOverlapVector(unsigned long NElements):
                     OverlapVector(NElements), SortedIndices(NElements + 1)
{
	for (unsigned long u = 0; u < SortedIndices.size(); ++u) SortedIndices[u] = u - 1;
};

//============================================================================

CLabelOverlapVector::CLabelOverlapVector(const CLabelOverlapVector &vec):
                     OverlapVector(vec.OverlapVector),
                     SortedIndices(vec.SortedIndices)
{  
};

//============================================================================

CLabelOverlapVector::~CLabelOverlapVector ()
{
};

//============================================================================

unsigned long CLabelOverlapVector::setLength(unsigned long NElements)
{
	unsigned long oldSize_ = getLength();
	OverlapVector.resize(NElements);
	SortedIndices.resize(NElements + 1);
	for (unsigned long u = oldSize_; u < SortedIndices.size(); ++u) SortedIndices[u] = u - 1;
	return getLength();
};

//============================================================================

void CLabelOverlapVector::writeData(CCharString heading, CCharString filename) const
{
	if (filename.IsEmpty()) filename = "OverlapsAll.dat";
	ofstream outfile (filename.GetChar());  
	outfile.setf(ios::fixed, ios::floatfield);
	outfile.precision(1);

	writeData(outfile, heading);
};

//============================================================================

void CLabelOverlapVector::writeData(ostream &ostr, CCharString heading) const
{
	ostr << heading << "\n";
	for (long u = 0; u < heading.GetLength() - 1; ++u) ostr << "=";
	ostr<< "\n\n";

	for (unsigned long i = 0; i < getLength(); ++i)
	{
		ostr << OverlapVector[i].getString(true);
	};

	ostr << "\n\n\n";
};

//============================================================================

void CLabelOverlapVector::classifyTotal(float noneVsWeak, float weakVsPartial, 
										float partialVsStrong)
{
	for (unsigned long i = 0; i < getLength(); ++i)
	{
		OverlapVector[i].classifyTotal(noneVsWeak, weakVsPartial, partialVsStrong);
	};
};

//============================================================================

void CLabelOverlapVector::classifyPart(float noneVsWeak, float weakVsPartial, 
									   float partialVsStrong)
{
	for (unsigned long i = 0; i < getLength(); ++i)
	{
		OverlapVector[i].classify(noneVsWeak, weakVsPartial, partialVsStrong);
	};
};

//============================================================================

void CLabelOverlapVector::getSizeHistogram(vector <unsigned long> &histogram, 
										   float cellSize, float maxSize) const
{
	unsigned long length = (unsigned long) floor(maxSize / cellSize) + (unsigned long) 1;
	histogram.resize(length, 0);

	for (unsigned long u = 0; u < getLength(); ++u)
	{
		unsigned long index = (unsigned long) floor(OverlapVector[u].getArea() / cellSize); //+ 0.5f);
		if (index >= length) index = length - 1;
		++histogram[index];
	};
};

//============================================================================

void CLabelOverlapVector::printSizeHistogram(const CCharString &heading,                    
											 float cellSize, float maxSize) const
{
	vector<unsigned long> histogram;
	getSizeHistogram(histogram, cellSize, maxSize);

	ostrstream oss;
	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(1);
	oss<< "\n" << heading <<"\n";
	for (long i = 0; i < heading.GetLength() - 1; ++ i) oss<<"=";
	oss << "\n\n";

	if (histogram[0])
	{
		oss << "[";
		oss.width(7);
		oss<< 0.0f << " ... ";
		oss.width(7);
		oss << cellSize / 2.0f << "]: ";
		oss.width(5);
		oss << histogram[0] << "\n";
	};

	unsigned long length = histogram.size() - 1;

	unsigned long u = 1;
	for (; u < length; ++u)
	{
		if (histogram[u])
		{
			float border = (float (u) - 0.5f) * cellSize;
			oss << "[";
			oss.width(7);
			oss << border << " ... ";
			border += cellSize;
			oss.width(7);
			oss << border << "]: ";
			oss.width(5);
			oss << histogram[u] << "\n";
		};
	};

	float border = (float (u) - 0.5f) * cellSize;
	oss << "[";
	oss.width(7);
	oss << border << " ... ";
	oss.width(7);
	oss  << " " << "]: " ;
	oss.width(5);
	oss  << histogram[length] << "\n" << ends;

	protHandler.print(oss, CProtocolHandler::scr + CProtocolHandler::prt);
};

//============================================================================

void CLabelOverlapVector::printCoverageHistogram(const CCharString &heading) const
{
	vector<unsigned long> histogram(51,0);

	for (unsigned long u = 0; u < getLength(); ++u)
	{
		int index = (int) floor(OverlapVector[u].getTotalCoverage() * 50.0f);
		if (index > 50) index = 50;
		else if (index < 0) index = 0;
		++histogram[index];
	};

	float factor = 100.0f / float(getLength());

	ostrstream oss;

	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(1);
	oss<< "\n" << heading <<"\n";
	for (long i = 0; i < heading.GetLength() - 1; ++ i) oss<<"=";
	oss << "\n\n";

	for (unsigned long u = 0; u < histogram.size(); ++u)
	{
		if (histogram[u])
		{
			oss << "[";
			oss.width(3);
			oss << u * 2 << " ... ";
			oss.width(3);
			oss << u * 2 + 2 << "]: " ;
			oss.width(5);
			oss << histogram[u] << " / ";
			oss.width(5);
			oss << float(histogram[u]) * factor << "%\n";
		};
	};

	oss << "\n" << ends;

	protHandler.print(oss, CProtocolHandler::scr + CProtocolHandler::prt);
};

//============================================================================

void CLabelOverlapVector::printHistogram(const CCharString &heading, 
										 float cellSize, float maxSize,
										 bool printOverlapHisto) const
{
	vector<unsigned long> histogram(2000, 0);
	unsigned long maxSplit = 0;

	unsigned long nStrong = 0;
	unsigned long nPartial = 0;
	unsigned long nWeak = 0;
	unsigned long nNone = 0;

	float strongArea = 0;
	float partialArea = 0;
	float weakArea = 0;
	float noneArea = 0;

	for (unsigned long u = 0; u < getLength(); ++u)
	{
		unsigned long nOverlaps = OverlapVector[u].nOverlaps();

		++histogram[nOverlaps];
		if (nOverlaps > maxSplit) maxSplit = nOverlaps;

		eOVERLAP eClass = OverlapVector[u].getClassification();
		float area = OverlapVector[u].getArea();

		switch (eClass)
		{
			case eSTRONG:
			{
				++nStrong;
				strongArea += area;
			};
			break;

			case ePARTIAL:
			{
				++nPartial;
				partialArea += area;
			};
			break;

			case eWEAK:
			{
				++nWeak;
				weakArea += area;
			};
			break;

			case eNONE:
			default:
			{
				++nNone;
				noneArea += area;
			}
		};
	};

	if (nStrong > 0)  strongArea  /= nStrong;
	if (nPartial > 0) partialArea /= nPartial;
	if (nWeak > 0)    weakArea    /= nWeak;
	if (nNone > 0)    noneArea    /= nNone;

	maxSplit += 1;
	histogram.resize(maxSplit);

	float factor = 100.0f / float(getLength());
	ostrstream oss;

	oss.setf(ios::fixed,ios::floatfield);
	oss.precision(1);
	oss<< "\n" << heading <<"\n";
	for (long i = 0; i < heading.GetLength() - 1; ++ i) oss<<"=";
	oss << "\n\n";

	for (unsigned long u = 0; u < maxSplit; ++u)
	{
		if (histogram[u] > 0)
		{
			oss << "Overlaps: ";
			oss.width(5);
			oss << u << ", Buildings: ";
			oss.width(7);
			oss << histogram[u] << ", Percentage: ";
			oss.width(7);
			oss << float(histogram[u]) * factor << " %\n";
		};
	};

	oss << "\nStatistics of total Overlap: \n============================\n\n  STRONG  PARTIAL    WEAK     NONE     \n ";
	oss.width(7);
	oss << nStrong << " ";
	oss.width(7);
	oss << nPartial << " ";
	oss.width(7);
	oss << nWeak << " ";
	oss.width(7);
	oss << nNone << "\n ";
	oss.width(7);
	oss << float(nStrong)  * factor << " ";
	oss.width(7);
	oss << float(nPartial) * factor << " ";
	oss.width(7);
	oss << float(nWeak)    * factor << " ";
	oss.width(7);
	oss << float(nNone)    * factor << "\n ";
	oss.width(7);
	oss << strongArea << " ";
	oss.width(7);
	oss << partialArea << " " ;
	oss.width(7);
	oss << weakArea << " ";
	oss.width(7);
	oss << noneArea << "\n" << ends; 
	protHandler.print(oss, CProtocolHandler::scr + CProtocolHandler::prt);
	
	if (printOverlapHisto)
	{
		vector<unsigned long> areaHistogramStrong;
		vector<unsigned long> areaHistogramPartial;
		vector<unsigned long> areaHistogramWeak;
		vector<unsigned long> areaHistogramNone;
		vector<unsigned long> areaHistogramAll;

		unsigned long histLength = 0;

		if (nStrong)
		{
			CLabelOverlapVector vecCp(nStrong);
			unsigned long cp = 0;
			for (unsigned long u = 0; u < getLength(); ++u)
			{
				if (OverlapVector[u].getClassification() == eSTRONG)
				{
					vecCp.OverlapVector[cp] = OverlapVector[u];
					++cp;
				};
			};

			vecCp.getSizeHistogram(areaHistogramStrong, cellSize, maxSize);
			histLength = areaHistogramStrong.size();
			vecCp.printCoverageHistogram("\nCoverage histogram of strong overlap:");
		};


		if (nPartial)
		{
			CLabelOverlapVector vecCp(nPartial);
			unsigned long cp = 0;
			for (unsigned long u = 0; u < getLength(); ++u)
			{
				if (OverlapVector[u].getClassification() == ePARTIAL)
				{
					vecCp.OverlapVector[cp] = OverlapVector[u];
					++cp;
				};
			};

			vecCp.getSizeHistogram(areaHistogramPartial, cellSize, maxSize);
			histLength = areaHistogramPartial.size();
			vecCp.printCoverageHistogram("\nCoverage histogram of partial overlap:");
		};

		if (nWeak)
		{
			CLabelOverlapVector vecCp(nWeak);
			unsigned long cp = 0;
			for (unsigned long u = 0; u < getLength(); ++u)
			{
				if (OverlapVector[u].getClassification() == eWEAK)
				{
					vecCp.OverlapVector[cp] = OverlapVector[u];
					++cp;
				};
			};

			vecCp.getSizeHistogram(areaHistogramWeak, cellSize, maxSize);
			histLength = areaHistogramWeak.size();
			vecCp.printCoverageHistogram("\nCoverage histogram of weak overlap:");
		};

		if (nNone)
		{
			CLabelOverlapVector vecCp(nNone);
			unsigned long cp = 0;
			for (unsigned long u = 0; u < getLength(); ++u)
			{
				if (OverlapVector[u].getClassification() == eNONE)
				{
					vecCp.OverlapVector[cp] = OverlapVector[u];
					++cp;
				};
			};

			vecCp.getSizeHistogram(areaHistogramNone, cellSize, maxSize);
			histLength = areaHistogramNone.size();
			vecCp.printCoverageHistogram("\nCoverage histogram of no overlap:");
		};

		areaHistogramAll.resize(histLength, 0);
		if (areaHistogramStrong.size() < histLength)   areaHistogramStrong.resize(histLength, 0);
		if (areaHistogramPartial.size() < histLength)  areaHistogramPartial.resize(histLength, 0);
		if (areaHistogramWeak.size() < histLength)     areaHistogramWeak.resize(histLength, 0);
		if (areaHistogramNone.size() < histLength)     areaHistogramNone.resize(histLength, 0);

		protHandler.print("\nHistograms of overlap by area:\n==============================\n\n",
						  CProtocolHandler::scr + CProtocolHandler::prt);

		protHandler.print(CCharString("\n") + heading + "\n", CProtocolHandler::scr + CProtocolHandler::prt);

		ostrstream oss1;

		oss1.setf(ios::fixed, ios::floatfield);
		oss1.precision(0);
		float centre = cellSize * 0.5f;
		oss1 << "                 , < " << centre << ",";
		centre += cellSize;

		for (unsigned long u = 1; u <  histLength - 1; ++u, centre += cellSize)
		{
			oss1.width(5); 
			oss1 << centre << ",";
		};

		oss1 << " > "; 
		oss1.width(3);
		oss1 << centre - cellSize * 0.5f <<"\nStrong Overlap,   ";

		for (unsigned long u = 0; u <  histLength; ++u)
		{
			oss1.width(5); 
			oss1 << areaHistogramStrong[u]<< ",";
			areaHistogramAll[u] += areaHistogramStrong[u];
		};

		oss1<< "\nPartial Overlap,  ";

		for (unsigned long u = 0; u <  histLength; ++u)
		{
			oss1.width(5); 
			oss1 << areaHistogramPartial[u] << ",";
			areaHistogramAll[u] += areaHistogramPartial[u];
		};

		oss1<< "\nWeak Overlap,     ";

		for (unsigned long u = 0; u <  histLength; ++u)
		{
			oss1.width(5); 
			oss1 << areaHistogramWeak[u]<< ",";
			areaHistogramAll[u] += areaHistogramWeak[u];
		};

		oss1<< "\nNo Overlap,       ";

		for (unsigned long u = 0; u <  histLength; ++u)
		{
			oss1.width(5); 
			oss1  << areaHistogramNone[u]<< ",";
			areaHistogramAll[u] += areaHistogramNone[u];
		};

		oss1 << "\n\n" << ends;
		protHandler.print(oss1, CProtocolHandler::scr + CProtocolHandler::prt);
	}
};

//============================================================================

void CLabelOverlapVector::sortComponents()
{
	for (unsigned long u = 0; u < getLength(); ++u)
	{
		OverlapVector[u].sort();
	};
};

//============================================================================

void CLabelOverlapVector::sort()
{
	if (getLength() > 1)
	{
		int maxI = getLength() - 1;
		CMultipleLabelOverlapRecord max = OverlapVector[ maxI ];

		for ( int i = maxI - 1; i >= 0; --i )
		if ( OverlapVector [ i ] > max ) 
		{ 
			maxI = i; 
			max = OverlapVector[ maxI ]; 
		};

		swap(maxI, getLength()- 1 );
		sortPart(0, getLength() - 2 );
		reverseOrder();
	};
};

//============================================================================

void CLabelOverlapVector::sort(int low, int high)
{
	if (getLength() > 1)
	{
		int maxI = high - 1;
		CMultipleLabelOverlapRecord recMax = OverlapVector[ maxI ];

		for ( int i = maxI - 1; i >= low; --i )
		if ( OverlapVector [ i ] > recMax ) 
		{ 
			maxI = i; 
			recMax = OverlapVector[ maxI ]; 
		};

		swap(maxI, high - 1 );
		sortPart(low, high - 2 );

		unsigned long Min, Max;

		for (Min = low, Max = high - 1; Min < Max; ++Min,--Max)
		{
			swap(Min, Max);
		};
	};
};

//============================================================================

void CLabelOverlapVector::reverseOrder()
{
	unsigned long min, max;

	for (min = 0, max = getLength() - 1; min < max; ++min,--max)
	{
		swap(min, max);
	};
};

//============================================================================

void CLabelOverlapVector::sortPart(int low, int high)
{
	if ( low < high )   // stopping condition for recursion!
	{
		int lo = low;
		int hi = high + 1;

		CMultipleLabelOverlapRecord rec = OverlapVector[ low ];

		for ( ; ; )
		{
			while ( OverlapVector [ ++lo ] < rec );
			while ( OverlapVector [ --hi ] > rec );
			if ( lo < hi ) swap(lo, hi);
			else break; 
		};

		sortPart(low,    hi - 1 );
		sortPart(hi + 1, high );
	}
};

//============================================================================

void CLabelOverlapVector::swap(int i, int j)
{
	CMultipleLabelOverlapRecord rec = OverlapVector[i];
	this->OverlapVector[i] = this->OverlapVector[j];
	this->OverlapVector[j] = rec;
	unsigned long index = SortedIndices[i];
	SortedIndices[i] = SortedIndices[j];
	SortedIndices[j] = index;
};   
