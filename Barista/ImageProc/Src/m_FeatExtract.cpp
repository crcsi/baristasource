#include <limits.h>
#include <float.h>
#include <time.h>
#include <fstream>
#include <strstream>
using namespace std;

#include "tiffio.h"
#include "statistics.h"
#include "sortVector.h"
#include "ProtocolHandler.h"

#include "m_FeatExtract.h"
#include "XYPointArray.h"
#include "XYZPolyLine.h"
#include "XYPolyLineArray.h"
#include "XYContourArray.h"
#include "filter.h"
#include "m_match_def.h"
#include "LabelImage.h"
#include "ImageRegion.h"
#include "SegmentNeighbourhood.h"
#include "SegmentNeighbourhoodVector.h"
#include "SegmentAMatrix.h"
#include "HoughTransformation.h"
#include "RAG.h"

CFeatureExtractor::CFeatureExtractor(const FeatureExtractionParameters &I_parameters) : 
                                     margin(0), featRoi(0), graph(0), extractionParameters(I_parameters),
									 pointStrength(0), boundaryPolygon(0), validMask(0), varNoise(0)
{
}

CFeatureExtractor::~CFeatureExtractor(void)
{
	if (graph) delete graph;
	if (pointStrength) delete pointStrength;
	graph = 0;
	featRoi= NULL;
	this->boundaryPolygon = 0;
	this->validMask = 0;
}

void CFeatureExtractor::setRoi(CImageBuffer *troi, CXYPolyLine *BoundaryPoly)
{
	this->boundaryPolygon = BoundaryPoly;
	this->validMask = 0;
	featRoi= troi;
	this->set(featRoi->getOffsetX(), featRoi->getOffsetY(), featRoi->getWidth(), featRoi->getHeight(), 1, 8, false, HOMOGENEOUS);
	TextureStrength.set(offsetX, offsetY, width, height, 1, 32, false, 0.0f);
	TextureIsotropy.set(offsetX, offsetY, width, height, 1, 32, false, 0.0f);
	GradientDirection.set(offsetX, offsetY, width, height, 1, 32, false, 0.0f);
	margin = 0;
}

void CFeatureExtractor::setRoi(CImageBuffer *troi, CImageBuffer *pValidMask)
{
	this->boundaryPolygon = 0;
	this->validMask = pValidMask;
	featRoi= troi;
	this->set(featRoi->getOffsetX(), featRoi->getOffsetY(), featRoi->getWidth(), featRoi->getHeight(), 1, 8, false, HOMOGENEOUS);
	TextureStrength.set(offsetX, offsetY, width, height, 1, 32, false, 0.0f);
	TextureIsotropy.set(offsetX, offsetY, width, height, 1, 32, false, 0.0f);
	GradientDirection.set(offsetX, offsetY, width, height, 1, 32, false, 0.0f);
	margin = 0;

	if (this->validMask) 
	{
		if ((this->validMask->getWidth()  != this->getWidth()) || 
			(this->validMask->getHeight() != this->getHeight()) || 
			(this->validMask->getBands() > 1) || (this->validMask->getBpp() > 8))
		{
			this->validMask = 0;
		}
	}

}

/***********************************************************************/
	  
void CFeatureExtractor::computeDerivatives(CImageBuffer &gradX, CImageBuffer &gradY)
{
	CImageFilter filter = this->extractionParameters.getDerivativeFilterX();
	filter.convolve(*featRoi, gradX, false);
	filter = this->extractionParameters.getDerivativeFilterY();
	filter.convolve(*featRoi, gradY, false);
};

/***********************************************************************/

void CFeatureExtractor::exportClassification(const char *filename)
{
	CLookupTable lut(8, 3);
	lut.setColor(  0,   0, 255, POINT_RELMAX);
	lut.setColor(128, 128, 255, POINT_NOMAX);
	lut.setColor(  0, 255,   0, EDGE_RELMAX);
	lut.setColor(128, 255, 128, EDGE_NOMAX);
	lut.setColor(255,   0,   0, HOMOGENEOUS);
	lut.setColor(255, 255, 255, OUTSIDE_BOUNDARY);

	this->exportToTiffFile(filename, &lut, false);
};

/***********************************************************************/

bool CFeatureExtractor::edgesLink(float significance, CXYContourArray *xycont)
{
	float mode,dmax,dmin,thresh;
	float mmax=-1e+38F,scale=0.0,mmin=1e+38F;
	
	int	num_pts =0;
	float max=mmin;
	float min=mmax;

	if (this->featRoi==NULL)
        return false;
	

  /*================================================================
   * FEATURE EXTRACTION
   *================================================================*/    

	canny();

//	TiffDump(TextureStrength.getPixF(), 1, "E:\\projects\\gradient.tif");
//	TiffDump(GradientDirection.getPixF(), 1, "E:\\projects\\angle.tif");
	
	NoiseEstimation(significance,false,&mode,&dmax,&dmin,&thresh);
	
	for (unsigned long i=0; i < nGV; i++)
	{ 	
		if (this->textureStrength(i) < thresh)
		{
			gradientDirection(i)=0.0;
			this->pixUChar(i) = HOMOGENEOUS;
		}
		else if (!this->isEdgePixel(i))
		{
			gradientDirection(i)=0.0;
			this->pixUChar(i) = EDGE_NOMAX;
		};
	}

//	TiffDump(this->getPixUC(), 1, "E:\\projects\\significant.tif");

	return extractContours(xycont);
}

//*************************************************
// canny operator                                      
//*************************************************

void CFeatureExtractor::canny()
{
	CImageBuffer *filteredImage = this->featRoi;

	if (this->extractionParameters.getSigmaSmooth() > 0.0f)
	{
		CImageFilter filter(eGauss, 3, 3, this->extractionParameters.getSigmaSmooth());
		margin = filter.getOffsetX();
		filteredImage = new CImageBuffer(offsetX, offsetY, width, height, this->featRoi->getBands(), 32, false, 0.0f);
		filter.convolve(*featRoi, *filteredImage, false);
		filteredImage->SetToIntensity();
//		TiffDump(filteredImage->getPixF(), 1, "E:\\projects\\sigma.tif");
	};

	CImageBuffer tmp0  (offsetX, offsetY, width, height, this->featRoi->getBands(), 32, false, 0.0f);
	CImageBuffer tmp45 (offsetX, offsetY, width, height, this->featRoi->getBands(), 32, false, 0.0f);
	CImageBuffer tmp90 (offsetX, offsetY, width, height, this->featRoi->getBands(), 32, false, 0.0f);
	CImageBuffer tmp135(offsetX, offsetY, width, height, this->featRoi->getBands(), 32, false, 0.0f);
  
	// compute derivatives
	long di045 = 1 + width;
	long di135 = width - 1;
	float sq2 = float (1.0f / M_SQRT2_2);
	float pi2 = float (2.0f * M_PI);

	for (long y = margin; y < (height - margin); y++)
	{
		long iMin = y * width;
		for( long x = margin; x < (width - margin); x++)
		{
			long i = x + iMin;
			tmp0.pixFlt(i)   =  filteredImage->pixFlt(i - 1)     - filteredImage->pixFlt(i + 1);            // derivative in x
			tmp45.pixFlt(i)  = (filteredImage->pixFlt(i - di045) - filteredImage->pixFlt(i + di045)) * sq2; // derivative in 45 deg direction
			tmp90.pixFlt(i)  =  filteredImage->pixFlt(i - width) - filteredImage->pixFlt(i + width);           // derivative in y
			tmp135.pixFlt(i) = (filteredImage->pixFlt(i - di135) - filteredImage->pixFlt(i + di135)) * sq2; // derivative in 135 deg direction
		};
	};
    
  // ******** make non-max-suppression  ********
 
	margin += 1;

	for (long y = margin; y < (height - margin); y++)
	{  
		long iMin = y * width;

		for (long x = margin; x < (width - margin); x++)
		{
			long i = x + iMin;		
			this->pixUChar(i) = HOMOGENEOUS;
			float locGrad = fsqrt(tmp0.pixFlt(i) * tmp0.pixFlt(i)+ tmp90.pixFlt(i) * tmp90.pixFlt(i));
			textureStrength(i) = locGrad;
			float abstmp0   = fabs(tmp0.pixFlt(i) ); float abstmp45  = fabs(tmp45.pixFlt(i) );
			float abstmp90  = fabs(tmp90.pixFlt(i)); float abstmp135 = fabs(tmp135.pixFlt(i));
			if ((abstmp0 > abstmp45) && (abstmp0 > abstmp90) && (abstmp0 > abstmp135))
			{ // largest directional derivative is in x
				if ((abstmp0 > fabs(tmp0.pixFlt(i - 1))) && (abstmp0 >= fabs(tmp0.pixFlt(i + 1))))
					this->pixUChar(i) = EDGE_RELMAX;
			}
			else if ((abstmp45 > abstmp90) && (abstmp45 > abstmp135))
			{ // largest directional derivative is in 45 deg direction
				if ((abstmp45 >  fabs(tmp45.pixFlt(i - di045))) && 
					(abstmp45 >= fabs(tmp45.pixFlt(i + di045))))
					this->pixUChar(i) = EDGE_RELMAX;
			}
			else if (abstmp90 > abstmp135)
			{// largest directional derivative is in y
				if ((abstmp90 >  fabs(tmp90.pixFlt(i - width))) && (abstmp90 >= fabs(tmp90.pixFlt(i + width))))
					this->pixUChar(i) = EDGE_RELMAX;
			}
			else
			{	// largest directional derivative is in 135 deg direction		
				if ((abstmp135 >  fabs(tmp135.pixFlt(i - di135))) &&
					(abstmp135 >= fabs(tmp135.pixFlt(i + di135))) )
					this->pixUChar(i) = EDGE_RELMAX;
			}
    
      // ******** calc angle ********
      
			if ((fabs(tmp0.pixFlt(i)) < FLT_EPSILON) && (fabs(tmp90.pixFlt(i)) < FLT_EPSILON)) 
				gradientDirection(i) = 0.0;				
			else
			{
				gradientDirection(i) = float(atan2(tmp0.pixFlt(i), tmp90.pixFlt(i)));
				if (gradientDirection(i) <0.0) gradientDirection(i) += pi2;
			}
		} // end for (long x = margin; x < (height - margin); x++)  
	} // end for (long y = margin; y < (width - margin); y++) 

	if (filteredImage != this->featRoi) delete filteredImage;

//	TiffDump("_Canny");
}

void CFeatureExtractor::NoiseEstimation(float c,int Upper,float *mode,float *dmax,float *dmin,float *thresh)
{
  int j, J,JJ[NN];
  float *srt=NULL;
  float maxpx[NN],px[NN],m[NN],mode_avg,signif,sum,norm;
  
  J=(int)(nGV * 0.50);
  
  srt=(float *) malloc(nGV * sizeof(float));
  memcpy((void *) srt, (void *) TextureStrength.getPixF(), nGV * sizeof(float));
  sort(nGV, srt-1);
  
  for (j=0;j<NN;j++) 
  {
    maxpx[j] = (float) -1e38;
    /*    JJ[j]=(int) (J - j*J/(float) NN); */
   JJ[j]=(int)((0.25 + 0.05*j)* (float) nGV);
  }

  for (unsigned long i = 0; i < nGV - J; i++)
  {
	  if (srt[i] > 0.0)
	  { /* there might be a peak at 0 and we don't want it ! */
		  for (j = 0; j < NN; j++)
		  {
			  px[j] = float(1.0 / (srt[i + JJ[j]] - srt[i]));
			  if (px[j] > maxpx[j]) 
			  {
				  maxpx[j] = px[j];
				  m[j] = float(0.5 * (srt[i + JJ[j]] + srt[i])); 
			  }
		  }
	  }
  }

  for (j=0,mode_avg=sum=norm=0.0;j<NN;j++)  {
    mode_avg += m[j]; 
  }
  *mode = mode_avg / (float) NN; 
    
  signif=fsqrt(-2.0*log((100.0-c)/100.0));

  if (Upper)  *thresh=srt[nGV-1] - (srt[nGV-1] - *mode) * signif;
  else  *thresh= *mode * signif;
  
  *dmax=srt[nGV-1];
  *dmin=srt[0];

  free(srt);
}


void CFeatureExtractor::getStartPoints(CSortVector<AIM_Pixel> &startPoints, float P_th)
{
	int posL[12], posR[12];
	float bt[12];
	int path[12];
	float pval[12];
  
	for (int i=0; i<12; i++)
	{
		posL[i] = width * Ly[i] + Lx[i];
		posR[i] = width * Ry[i] + Rx[i];
		bt[i] = atan2((float)(Ly[i]-Ry[i]),(float)(Rx[i]-Lx[i]));
	}
  
	startPoints.clear();

	AIM_Pixel pixel;

	for (pixel.y = 0; pixel.y < height; pixel.y++)
	{
		unsigned long uMin = pixel.y * width;
		for (pixel.x = 0; pixel.x < width; pixel.x++)
		{
			unsigned long org = uMin + pixel.x;
			if (this->isEdgePixel(org))
			{ 
				for (int i = 0; i < 12; i++)
				{
					long L = org + posL[i];
					long R = org + posR[i];
					path[i] = 0;
					pval[i] = 0.0;
					if (this->isEdgePixel(L) && this->isEdgePixel(R))
					{
						 float oO = gradientDirection(org);
						 float oL = gradientDirection(L);
						 float oR = gradientDirection(R);
						 float P=(float)((diffori(bt[i],oL)+diffori(bt[i],oR)+diffori(bt[i],oO))*60.0/M_PI);

						 if (P < P_th)
						 {
							float mO = textureStrength(org);
							float mL = textureStrength(L);
							float mR = textureStrength(R);
							float C1  = (float)(mL + mR - 2.0 * mO); 
							float C2  = mL - mR;
							float C   = fabs(fsqrt(SQR(C1)+SQR(C2))/(float)(0.01+mO));

							float A = (float)((diffori(oO,oL)+diffori(oO,oR))*90.0/M_PI); 

							path[i]  = True;
							pval[i] += (float)((90.0-A)/90.0);
							pval[i] += (float)((2.0-C)/2.0); 
					 } // if ((P < P_th) && (!type || (type&&(type[L]==type[org])&&(type[R]==type[org]))))
					} // if (this->isEdgePixel(L) && this->isEdgePixel(R))
				} // for (int i = 0; i < 12; i++)

				float maxval = -FLT_MAX;
				int maxpos = -1; 
				for (int i = 0; i < 12; i++) 
					if (pval[i] > maxval)
					{
						maxval=pval[i];
						maxpos=i;
					};

				if (path[maxpos]) 
				{
					pixel.gradientStrength = pval[maxpos];
					startPoints.push_back(pixel);
				}
			} // if (nms[org])
		} //  for (unsigned long x = 0 ; x < width ; x++)
	}// for (unsigned long y = 0; y < height; y++ ) 

	startPoints.sortDescending();
}  

void CFeatureExtractor::aim_contour_aggregation(const CSortVector<AIM_Pixel> &startPoints)
{
	int c_nr[3],selfhit;

	aim_init_GlobalVar();
 
	for (long it = 0; it < startPoints.size(); it++)
	{   
		unsigned long index = startPoints[it].x + startPoints[it].y * width;
		if (!START[index]&&!KPTCODE[index]&&
			startPoints[it].x >=4 && startPoints[it].x < (width-4) && 
			startPoints[it].y >=4 && startPoints[it].y < (height-4)) 
		{
			c_nr[0] = graph->getnewC();
			c_nr[1]=c_nr[2]=-1;
			graph->c[c_nr[0]].initChain(MIN(MAX(width,height)/8,30));  

			float start_ori=gradientDirection(index);
			aim_get_ContourString(startPoints[it], start_ori, c_nr[0]);

			if (!aim_delete_string(c_nr[0]))
			{
				if ((selfhit=aim_treat_collision(c_nr[0], POS_FIRST)) >= 0)
				{
					c_nr[1]=c_nr[0];c_nr[0]=selfhit;
				}
				if ((selfhit=aim_treat_collision(c_nr[0], POS_LAST)) >= 0) c_nr[2]=selfhit;
			}
		} 
	} // for (long it = 0; it < nr_pts; it++)
  
	aim_free_GlobalVar();  
}

/***********************************************************************
  aim_init_GlobalVar(): To alleviate the transfer of parameters and 
  input maps (pointers) this function sets and initializes some useful
  global variables and pointers. 
  **********************************************************************/
void CFeatureExtractor::aim_init_GlobalVar()
{
  int x,y,m_no,p_no;
  float ang,ftmp,ftmp2;
  unsigned long i;

  for (y=-1,i=0;y<=1;y++) for (x=-1;x<=1;x++) MASK3X3[i++]=x+width*y;

  KPTCODE=(unsigned char *) malloc(nGV * sizeof(char));
  XYMAP=(XYmapType *) malloc(nGV*sizeof(XYmapType));
  START=(unsigned char *) malloc(nGV*sizeof(char));

  /* Initialize XYMAP, KPTCODE and START maps */
  for (i=0;i<nGV;i++) 
    XYMAP[i].c_no=XYMAP[i].type=START[i]= KPTCODE[i]=0;

  /* Initialize the searchmasks, weights etc. for GetContourString()*/
  for (m_no=0;m_no<8;m_no++){
    ftmp=(float)( m_no*M_PI/4.0);
    for (p_no=0;p_no<9;p_no++){
      SM2[m_no][p_no][0]=DIR1X[m_no][p_no]+width*DIR1Y[m_no][p_no];
      SM2[m_no][p_no][1]=DIR2X[m_no][p_no]+width*DIR2Y[m_no][p_no];
      ang=atan2((float)(-DIR1Y[m_no][p_no]),(float)(DIR1X[m_no][p_no]));
      NEXTMASK1[m_no][p_no]=aim_angle2discrete(ang,360);
      ang=atan2((float)(-DIR2Y[m_no][p_no]),(float)(DIR2X[m_no][p_no]));
      ftmp2=diffori(ang,ftmp);
      if (ftmp2 > M_PI/6.0) NEXTMASK2[m_no][p_no]=aim_angle2discrete(ang,360);
      else NEXTMASK2[m_no][p_no]=aim_angle2discrete(ftmp,360);
      MASKWEIGHT[m_no][p_no]=fpow(fcos(ftmp2),1.0);
    }
  }
}

/***********************************************************************
 FreeGlobalVariables: free all allocated memory.
 **********************************************************************/
void CFeatureExtractor::aim_free_GlobalVar(void)
{
  free(KPTCODE);free(XYMAP);free(START);
}
     
// *************************************
// *    Operations on graphs           *
// *************************************

// ***********************************************************************
//  GetContourString(): The actual function aggregating local
//  edge-evidences into coherent pieces of contour. The start-position
//  (stx,sty) and its local orientation defines the initalization.
 //**********************************************************************
void CFeatureExtractor::aim_get_ContourString(const AIM_Pixel &startPoint, float stori,int c_no)
{
	int tx,ty,where,ipos,m_no;
	int steps,stop,i,maxpr,collision,Coll_P1;
	int P1[9],P2[9],priority[9],nextpos;
	CSortVector<float> value(9, 0.0f);
	AIM_Pixel P;
 
	for (where= POS_FIRST;where<=POS_LAST;where++)
	{
    ty=startPoint.y;tx=startPoint.x;
    steps=stop=0;
    
    if (where == POS_FIRST) m_no=aim_angle2discrete(stori,360);
    else m_no=aim_angle2discrete((float)(M_PI+stori),360);
    
    while (!stop){
      
      
      ipos=tx+ty*graph->width;
      maxpr=collision=0;
      for (i=0;i<9;i++){
	P1[i]=ipos+SM2[m_no][i][0];   /* position level 1 */
	P2[i]=ipos+SM2[m_no][i][1];   /* position level 2 */
      }
      
      if (KPTCODE[ipos] == KEYPOINT) maxpr=4;
      else 
	if (KPTCODE[ipos]) maxpr=3;
	else {
	  for (i=0;i<9;i++){
	    priority[i]=1;
	    if (Coll_P1 = aim_collision(tx,ty,graph->width,m_no,i,1)) collision=True;
	    
	    if ( Coll_P1 || (KPTCODE[P1[i]] && !KPTCODE[P2[i]]) || 
		(!KPTCODE[P1[i]] && !KPTCODE[P2[i]] &&
		 !isEdgePixel(P1[i]) && !isEdgePixel(P2[i]))) priority[i]=0;
	    else if (KPTCODE[P2[i]] ||
		     (isEdgePixel(P2[i]) && (START[P1[i]]==0) && (START[P2[i]]==0) &&
		      (aim_collision(tx,ty,graph->width,m_no,i,2)==0))) priority[i]=2;
	  	    
	    if (priority[i] > maxpr) maxpr=priority[i];
	  }
	}
      
      if (maxpr == 1 && collision) maxpr=0; 
      
      /* evaluate max priority to choose next position */
      switch (maxpr){
      case 3: m_no=aim_kptCatch(ipos);nextpos=4;break;
      case 2:case 1: /* cases are treated equally !!! */
	for (i=0;i<9;i++){
	  if (priority[i]){
	    value[i]=(textureStrength(P1[i]) + textureStrength(P2[i]))*MASKWEIGHT[m_no][i]; 
	    
	    if (KPTCODE[P2[i]]) value[i] *= 2.0;
	    if (isEdgePixel(P1[i]) && isEdgePixel(P2[i])) value[i] *= 2.0;
	    	    
	  }else value[i]=0.0;
	  
	 
	}
	nextpos=value.getMaxIndex();break;
      default: /* priorities 4 and 0 */ stop=True;break;
      }
      
      P.x=tx;P.y=ty;
      if (stop){ /* aggregation terminating, set kpt-type */
	if (maxpr == 0)
	  if (collision) P.type=COLL_PIX;
	  else P.type=STOP_PIX;
	else P.type=aim_get_kptType(ipos);
	TDIR[where]=m_no;
      }
      else{
	/* set pixel type */
	if (steps == 0) P.type=START_PIX; else P.type=NORM_PIX;
	
	/* move (tx,ty) to new position */
	tx+=DIR1X[m_no][nextpos];
	ty+=DIR1Y[m_no][nextpos];
	
	/* update searchmasks according to chosen path and priority */
	switch(priority[nextpos]){
	case 3: break;
	case 2: m_no=NEXTMASK2[m_no][nextpos];break;
	case 1: m_no=NEXTMASK1[m_no][nextpos];break;
	}
      } 
          
      // update cross-ref map and set termination directions 
      XYMAP[ipos].type=P.type;XYMAP[ipos].c_no=c_no;
      
      // delete eligable start-points 
      for (i=0;i<9;i++) START[ipos+MASK3X3[i]]=True; 
      
      if (steps == 0 && where == POS_LAST){
	// correct the type of the start-point if termination at start-point.
	if (stop) graph->c[c_no].getPixel(graph->c[c_no].getLength()-1).type = P.type;
      }
      else graph->c[c_no].addP(where,P);   // add new pixel to contour string 
      
      steps++;
      
    } // while (!stop)
    
    // Add end-point to contour string and assign EP[where] the
    // key-point number for DeleteString() 
    EP[where]=aim_treat_endPoint(c_no,where); 
  } // for (where=FIRST;where<=LAST;where++)

}

int CFeatureExtractor::aim_delete_string(int c_no)
{
	int where,nx=graph->width,pos;

	if ((graph->c[c_no].active)&&(graph->c[c_no].getLength() < 4)&&
		(graph->v[EP[POS_FIRST]].ord == 1)&&(graph->v[EP[POS_LAST]].ord == 1))
	{
		for (where=POS_FIRST;where<=POS_LAST;where++)
		{
			graph->v[EP[where]].removeNeighbour(c_no, false);
			if (graph->v[EP[where]].active && (graph->v[EP[where]].ord == 0))
			{
				aim_delete_kptArea(EP[where]);
				graph->deleteV(EP[where]);
			}
		}
    
		for (unsigned long i=0;i<graph->c[c_no].getLength();i++)
		{
			pos=graph->c[c_no].getPixel(i).x+nx*graph->c[c_no].getPixel(i).y;
			if (!KPTCODE[pos]) XYMAP[pos].c_no=XYMAP[pos].type=0;
		}
    
		graph->deleteC(c_no);
		return True;
	}
	return False;
}


int CFeatureExtractor::aim_treat_collision(int c_no,int where)
{
  int nx=graph->width,m_no,ipos,i,ksrc;
  int xtar[3],ytar[3],ptarget,tx,ty,finished;
  long ctar, newc, ktar;

  int ret=-1;
  AIM_Pixel P;
 
  if (graph->c[c_no].active){
    
    ksrc=graph->c[c_no].v_no[where];
    if (graph->v[ksrc].active&&(graph->v[ksrc].type == COLL_PIX)&&
	(graph->v[ksrc].ord == 1)&&(graph->v[ksrc].c[0].mul == 1)){
     
      tx=graph->v[ksrc].x;ty=graph->v[ksrc].y;ipos=tx+ty*graph->width;
      m_no=TDIR[where]; // global variable set in GetContourString() 
      if ((m_no > 7)||(m_no < 0)) return 0;
      
      // Because of TDIR[], there has to be a collision point
	  // in one of the following three positions. 

      xtar[0]=tx+DIR1X[m_no][4];ytar[0]=ty+DIR1Y[m_no][4];
      xtar[1]=tx+DIR1X[m_no][0];ytar[1]=ty+DIR1Y[m_no][0];
      xtar[2]=tx+DIR1X[m_no][8];ytar[2]=ty+DIR1Y[m_no][8];
     
      for (i=finished=0;((i<3)&&!finished);i++){
	ptarget=xtar[i]+ytar[i]*nx;
	if ((XYMAP[ptarget].type == 1)||(XYMAP[ptarget].type == 4)){
	  ctar=XYMAP[ptarget].c_no;
	  
	  if (graph->splitContourAt(ctar, xtar[i], ytar[i], SPLIT_PIX, newc, ktar))
	  {
	    
	    aim_add_kptArea(KPTCODE,graph->v[ktar].x+graph->v[ktar].y*graph->width,SPLIT_PIX);
	    aim_updateXYMAP(newc);
	    
	    
	    if (ctar==c_no){c_no=graph->v[ksrc].c[0].c_no;ret=newc;} /* selfhit */
	    
	    if (where == POS_FIRST) graph->c[c_no].getPixel(0).type=NORM_PIX;
	    else graph->c[c_no].getPixel(graph->c[c_no].getLength()-1).type=NORM_PIX;
	    
	    P.x=graph->v[ktar].x;P.y=graph->v[ktar].y;P.type=graph->v[ktar].type;
	    graph->c[c_no].addP(where,P);
	    
	    graph->c[c_no].v_no[where]=ktar;
	    graph->v[ktar].addNeighbour(c_no);
	    aim_updateXYMAP(c_no);
	    
	    aim_delete_kptArea(ksrc);
	    graph->deleteV(ksrc);
	    
	    finished=True;
	  }
	}
      } 
    }
  }
  
  return ret;
} 

int CFeatureExtractor::aim_angle2discrete(float ang,int which)
{
  /* maps an arbitrary angle x to [0,1,2,3,4,5,6,7] or 
     [0,1,2,3] depending on which. */
  int no;
  float slice=(float)(M_PI/8.0);
  
  if ((ang > M_PI)||(ang < -M_PI)) ang=mapangle(ang,which);
  
  if ((ang > -slice)&&(ang < slice)) no=0;
  else if ((ang >= slice)&&(ang <= 3*slice))     no=1;
  else if ((ang > 3*slice)&&(ang < 5*slice))     no=2;
  else if ((ang >= 5*slice)&&(ang <= 7*slice))   no=3;
  else if ((ang >= -3*slice)&&(ang <= -slice))   no=7;
  else if ((ang > -5*slice)&&(ang < -3*slice))   no=6;
  else if ((ang >= -7*slice)&&(ang <= -5*slice)) no=5;
  else no=4;
  
  if ((which == 180)&&(no == 4)) no=0;

  return no;
}

int CFeatureExtractor::aim_collision(int tx,int ty,int nx,int m_no,int p_no,int elem)
{
  int sx,sy,dx,dy;
  
  if (elem == 1){
    sx=tx;sy=ty;
    dx=DIR1X[m_no][p_no];
    dy=DIR1Y[m_no][p_no];
  }
  else if (elem == 2){
    sx=tx+DIR1X[m_no][p_no];
    sy=ty+DIR1Y[m_no][p_no];
    dx=DIR2X[m_no][p_no]-DIR1X[m_no][p_no];
    dy=DIR2Y[m_no][p_no]-DIR1Y[m_no][p_no];
  }
  else return False;
  
  if ((XYMAP[sx+dx+(sy+dy)*nx].type&&!KPTCODE[sx+dx+(sy+dy)*nx]) ||
      (XYMAP[sx+(sy+dy)*nx].type&&!KPTCODE[sx+(sy+dy)*nx]&&
       XYMAP[sx+dx+sy*nx].type&&!KPTCODE[sx+dx+sy*nx])) return True;
  
  return False;
}


void CFeatureExtractor::aim_add_kptArea(unsigned char *kmap,int pos,char ktype)
{
  int j;
  
  for (j=0;j<9;j++) if (!kmap[pos+MASK3X3[j]]) kmap[pos+MASK3X3[j]]=ktype;
  kmap[pos]=KEYPOINT;
}

unsigned char CFeatureExtractor::aim_get_kptType(int pos)
{
  int j;
  CSortVector<float> tmp(256, 0.0f);
  for (j=0;j<9;j++) tmp[KPTCODE[pos+MASK3X3[j]]]++;
  return (unsigned char) tmp.getMaxIndex();    
}

void CFeatureExtractor::aim_delete_kptArea(int v_no)
{
	int no=0,list[100],d;
  
	for (unsigned long i=0; i<graph->v_alloc; i++) 
    if (graph->v[i].active) 
	{
		d=MAX(ABS(graph->v[v_no].x-graph->v[i].x),ABS(graph->v[v_no].y-graph->v[i].y));
		if ((d >= 1)&&(d <= 2)) list[no++]=i;
	}

	for (long j=0;j<9;j++) 
		KPTCODE[graph->v[v_no].x+graph->v[v_no].y*graph->width+MASK3X3[j]]=0;
  
	for (long j=0;j<no;j++)
		aim_add_kptArea(KPTCODE,graph->v[list[j]].x+graph->v[list[j]].y*graph->width,graph->v[list[j]].type);
}

// ***********************************************************************
//  UpdateXYMAP(): After complex operations on the graph-structure we
//  must update the XYMAP, the cross reference (x,y) map. We only have to 
//  update single contour strings (e.g. after split operations).
// **********************************************************************
void CFeatureExtractor::aim_updateXYMAP(int c_no)
{
	if (graph->c[c_no].active)
	{
		for (unsigned long j=0;j<graph->c[c_no].getLength();j++)
		{
			long pos = graph->c[c_no].getPixel(j).x + graph->c[c_no].getPixel(j).y * graph->width;
			XYMAP[pos].type = graph->c[c_no].getPixel(j).type;
			XYMAP[pos].c_no = c_no;
		}
	}
}

unsigned char CFeatureExtractor::aim_kptCatch(int pos)
{
  static unsigned char Direction[9]={3,2,1,4,255,0,5,6,7};
  int i;
  
  if ((KPTCODE[pos] != KEYPOINT)&&(KPTCODE[pos] != NO_PIX)){
    for (i=0;i<9;i++) 
      if (KPTCODE[pos+MASK3X3[i]] == KEYPOINT) return Direction[i];
  }
  fprintf(stderr,"Error: KptCatch() type=%d\n",KPTCODE[pos]);
  return -1;
}


int CFeatureExtractor::aim_treat_endPoint(int c_no,int where)
{
  int v_no;
  AIM_Pixel P;
   
  if (where == POS_FIRST) P=graph->c[c_no].getPixel(0);
  else if (where == POS_LAST) P=graph->c[c_no].getPixel(graph->c[c_no].getLength()-1);
  
  v_no = graph->getVertex(P.x,P.y);
  if (v_no < 0){   // new keypoint 
	  v_no = graph->getnewV();
    graph->v[v_no].x=P.x;graph->v[v_no].y=P.y;graph->v[v_no].type=P.type;
    aim_add_kptArea(KPTCODE,P.x+P.y*graph->width,P.type);
  }
  graph->v[v_no].addNeighbour(c_no); // add new chain to existing key-point
  graph->c[c_no].v_no[where]=v_no;
  
  return v_no;
}


///////////////////////////////

void CFeatureExtractor::Attribute_graph()
{
	unsigned char *xymap=NULL,*inliers=NULL;
	G_attributes *Gattr = ((G_attributes *) graph->attr);


	float tolerance =  0.0f;
	float SlLength  = 10.0f;
	float exponent  =  1.0f;

	if (Gattr && (Gattr->BreakPts_def)) 

	{
		tolerance = Gattr->tolerance;
		exponent  = Gattr->exponent;
		SlLength  = Gattr->straight;
	};

	for (unsigned long c_no=0;c_no< graph->c_max;c_no++)
	{
		if (graph->c[c_no].active)
		{
			graph->c[c_no].initAttributes(false);
			graph->c[c_no].initDirectionAttributes();
			graph->c[c_no].initShapeAttributes(SlLength, tolerance, exponent);
			Integrate_strength(c_no);
		}
	}
	graph->refineEndpoints();
}

float CFeatureExtractor::Integrate_strength(unsigned long c_no)
{
	C_attributes *attr;

	if (graph->c[c_no].active && (attr = graph->c[c_no].getAttr()))
	{
		attr->strength=0.0;
		for (unsigned long k=0;k<graph->c[c_no].getLength();k++) 
			attr->strength += textureStrength(graph->c[c_no].getPixel(k).x+graph->c[c_no].getPixel(k).y*graph->width);
		return attr->strength;
	}

	return 0.0;
}



void CFeatureExtractor::sort(int n,float *ra)
{
  int l,j,ir,i;
  float rra;
  
  l=(n >> 1)+1;
  ir=n;
  for (;;) {
    if (l > 1)
      rra=ra[--l];
    else {
      rra=ra[ir];
      ra[ir]=ra[1];
      if (--ir == 1) {
	ra[1]=rra;
	return;
      }
    }
    i=l;
    j=l << 1;
    while (j <= ir) {
      if (j < ir && ra[j] < ra[j+1]) ++j;
      if (rra < ra[j]) {
	ra[i]=ra[j];
	j += (i=j);
      }
      else j=ir+1;
    }
    ra[i]=rra;
  }
}



float CFeatureExtractor::diffori(float x,float y)
{
  float xx,yy,diff;

  xx=mapangle(x,180);
  yy=mapangle(y,180);
  
  diff=MAX(xx,yy)-MIN(xx,yy);
  
  return ((float)( (diff > M_PI/2.0) ? (M_PI-diff):(diff)));
}

float CFeatureExtractor::mapangle(float x,int which)
{
  /* maps an angle x to [-pi,pi] or [0,pi] depending on which */
  
  while (x < -M_PI) x +=(float)(2*M_PI);
  while (x >  M_PI) x -= (float)(2*M_PI);
  if ((which == 180)&&(x < 0)) x +=(float)(M_PI);

  return x; 
}

void CFeatureExtractor::initNoiseModelParameters()
{
	float avgGVal;
	float varFactor;
	if (this->extractionParameters.Mode == eFoerstner)
		varFactor = this->extractionParameters.getTotalVarianceFactorEdges();
	else varFactor = this->extractionParameters.getDerivativeFilterX().getVarianceFactor();

	this->VarNoiseConstant.resize(this->featRoi->getBands(), 1.0);
	this->VarNoiseLinear.resize(this->featRoi->getBands(), 0.0);
	this->VarNoiseFactor.resize(this->featRoi->getBands());

	if ((this->featRoi->getBpp() == 32)	&& (this->extractionParameters.NoiseModel == ePoisson))
		this->extractionParameters.NoiseModel = eGaussian;

	if (this->extractionParameters.NoiseModel == ePoisson)
		this->featRoi->getPoissonNoise(0, 1.0f, this->VarNoiseConstant, this->VarNoiseLinear, avgGVal);
	else  this->featRoi->getGaussianNoise(0, 1.0f, this->VarNoiseConstant);

	if (this->featRoi->getBpp() == 32)	
	{
		this->varNoise = 0.0f;

		for (unsigned int bnd = 0; bnd < this->VarNoiseConstant.size(); ++bnd)
		{
			this->varNoise += this->VarNoiseConstant[bnd];
			this->VarNoiseFactor[bnd].resize(1, this->VarNoiseConstant[bnd] * varFactor);

			if (this->VarNoiseFactor[bnd][0] < 0.001f) this->VarNoiseFactor[bnd][0] = 1000.0f;
			else this->VarNoiseFactor[bnd][0] = float(1.0) / this->VarNoiseFactor[bnd][0];

			if (this->extractionParameters.Mode != eFoerstner)
				this->VarNoiseFactor[bnd][0] = fsqrt(this->VarNoiseFactor[bnd][0]);
			
		}	

		this->varNoise /= (float) this->VarNoiseConstant.size();
	}
	else
	{
		this->varNoise = 0.0f;

		for (int bnd = 0; bnd < this->featRoi->getBands(); ++bnd) 
		{
			this->varNoise += (this->VarNoiseConstant[bnd] + avgGVal * this->VarNoiseLinear[bnd]);
			unsigned long maxGreyVals = (this->featRoi->getBpp() == 8) ? 256 : 256 * 256;
			this->VarNoiseFactor[bnd].resize(maxGreyVals);

			for (unsigned long u = 0; u < maxGreyVals; ++u)
			{
				this->VarNoiseFactor[bnd][u] = (this->VarNoiseConstant[bnd] + float(u) * this->VarNoiseLinear[bnd])* varFactor;
				if (this->VarNoiseFactor[bnd][u] < 0.01f) this->VarNoiseFactor[bnd][u] = 100.0f;
				else this->VarNoiseFactor[bnd][u] = float(1.0) / this->VarNoiseFactor[bnd][u];
				if (this->extractionParameters.Mode != eFoerstner)
					this->VarNoiseFactor[bnd][u] = fsqrt(this->VarNoiseFactor[bnd][u]);
			}
		}

		this->varNoise /= (float) this->VarNoiseConstant.size();
	}
};

void CFeatureExtractor::computeGradientMagnitudeAndDirectionCanny(CImageBuffer &gradX, CImageBuffer &gradY)
{
	if (this->listener)
	{
		CCharString str("Analysing image texture");
		if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
		this->listener->setTitleString(str.GetChar());
		this->listener->setProgressValue(28.0);
	}

	margin = this->extractionParameters.getDerivativeFilterX().getOffsetX();
	float pi2 = (float) M_PI / 2.0;
	
	unsigned long uMin = gradX.getIndex(margin,margin);
	unsigned long uMax = gradX.getIndex(height - margin - 1, width - margin);
	long diffRow = (width - 2 * margin) * gradX.getDiffX();
	unsigned long uTxMin = this->TextureStrength.getIndex(margin, margin);
	
	float bndInv = 1.0f / (float) gradX.getBands();

	if (this->featRoi->getBpp() == 32)	
	{
		for (; uMin < uMax; uMin += gradX.getDiffY(), uTxMin += TextureStrength.getDiffY())
		{  
			unsigned long uMaxRow = uMin + diffRow;
			unsigned long uTx = uTxMin;
			for (unsigned long u = uMin; u < uMaxRow; u += gradX.getDiffX(), uTx += TextureStrength.getDiffX())
			{
//				this->pixUChar(uTx) = EDGE_RELMAX;
				float avgGradX = 0.0f, avgGradY = 0.0f;
				for (int i = 0; i < gradX.getBands(); i += gradX.getDiffBnd())
				{
					unsigned long uBnd = u + i;
					avgGradX += gradX.pixFlt(uBnd) * this->VarNoiseFactor[i][0];					
					avgGradY += gradY.pixFlt(uBnd) * this->VarNoiseFactor[i][0];
				}

				avgGradX *= bndInv;	
				avgGradY *= bndInv;
				textureStrength(uTx) = fsqrt(avgGradX * avgGradX + avgGradY * avgGradY);

					// ******** calc angle ********
				if ((fabs(avgGradX) < FLT_EPSILON) && (fabs(avgGradY) < FLT_EPSILON)) 
					gradientDirection(uTx) = 0.0;					
				else
				{
					gradientDirection(uTx) = float(atan2(avgGradX, avgGradY));
					if (gradientDirection(uTx) <0.0) gradientDirection(uTx) += pi2;
				}
			} // end for (unsigned long u = uMin; u < uMaxRow; u += gradX->getDiffX(), uTx += TextureStrength.getDiffX())		
		} // end for (; uMin < uMax; uMin += gradX.getDiffY(), uTxMin+= TextureStrength.getDiffY())
	}
	else
	{
		if (this->featRoi->getBpp() == 8)
		{
			for (; uMin < uMax; uMin += gradX.getDiffY(), uTxMin += TextureStrength.getDiffY())
			{  
				unsigned long uMaxRow = uMin + diffRow;
				unsigned long uTx = uTxMin;
				for (unsigned long u = uMin; u < uMaxRow; u += gradX.getDiffX(), uTx += TextureStrength.getDiffX())
				{
//					this->pixUChar(uTx) = EDGE_RELMAX;
					float avgGradX = 0.0f, avgGradY = 0.0f;
					for (int i = 0; i < gradX.getBands(); i += gradX.getDiffBnd())
					{
						unsigned long uBnd = u + i;
						unsigned long gV = (unsigned long) this->featRoi->pixUChar(uBnd);
						float sigFact = this->VarNoiseFactor[i][gV];
						avgGradX += gradX.pixFlt(uBnd) * sigFact;					
						avgGradY += gradY.pixFlt(uBnd) * sigFact;
					}

					avgGradX *= bndInv;
					avgGradY *= bndInv;

					textureStrength(uTx) = fsqrt(avgGradX * avgGradX + avgGradY * avgGradY);

					// ******** calc angle ********
					if ((fabs(avgGradX) < FLT_EPSILON) && (fabs(avgGradY) < FLT_EPSILON)) 
						gradientDirection(uTx) = 0.0;					
					else
					{
						gradientDirection(uTx) = float(atan2(avgGradX, avgGradY));
						if (gradientDirection(uTx) <0.0) gradientDirection(uTx) += pi2;
					}
				} // end for (unsigned long u = uMin; u < uMaxRow; u += gradX->getDiffX(), uTx += TextureStrength.getDiffX())		
			} // end for (; uMin < uMax; uMin += gradX.getDiffY(), uTxMin+= TextureStrength.getDiffY())
		}
		else if (this->featRoi->getBpp() == 16)	
		{
			for (; uMin < uMax; uMin += gradX.getDiffY(), uTxMin += TextureStrength.getDiffY())
			{  
				unsigned long uMaxRow = uMin + diffRow;
				unsigned long uTx = uTxMin;
				for (unsigned long u = uMin; u < uMaxRow; u += gradX.getDiffX(), uTx += TextureStrength.getDiffX())
				{
//					this->pixUChar(uTx) = EDGE_RELMAX;
					float avgGradX = 0.0f, avgGradY = 0.0f;
					for (int i = 0; i < gradX.getBands(); i += gradX.getDiffBnd())
					{
						unsigned long uBnd = u + i;
						unsigned long gV = (unsigned long) this->featRoi->pixUShort(uBnd);
						float sigFact = this->VarNoiseFactor[i][gV];
						avgGradX += gradX.pixFlt(uBnd) * sigFact;					
						avgGradY += gradY.pixFlt(uBnd) * sigFact;
					}

					avgGradX *= bndInv;
					avgGradY *= bndInv;

					textureStrength(uTx) = fsqrt(avgGradX * avgGradX + avgGradY * avgGradY);

					// ******** calc angle *******
					if ((fabs(avgGradX) < FLT_EPSILON) && (fabs(avgGradY) < FLT_EPSILON)) 
						gradientDirection(uTx) = 0.0;					
					else
					{
						gradientDirection(uTx) = float(atan2(avgGradX, avgGradY));
						if (gradientDirection(uTx) <0.0) gradientDirection(uTx) += pi2;
					}
				} // end for (unsigned long u = uMin; u < uMaxRow; u += gradX->getDiffX(), uTx += TextureStrength.getDiffX())		
			} // end for (; uMin < uMax; uMin += gradX.getDiffY(), uTxMin+= TextureStrength.getDiffY())		
		};
	};
};

void CFeatureExtractor::computeGradientMagnitudeAndDirectionFoerstner(CImageBuffer &gradX, CImageBuffer &gradY)
{
	if (this->listener)
	{
		CCharString str("Analysing image texture");
		if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
		this->listener->setTitleString(str.GetChar());
		this->listener->setProgressValue(28.0);
	}

	CImageBuffer deltaXX(offsetX, offsetY, width, height, gradX.getBands(),32,true);
	CImageBuffer deltaYY(offsetX, offsetY, width, height, gradX.getBands(),32,true);
	CImageBuffer deltaXY(offsetX, offsetY, width, height, gradX.getBands(),32,true);

	CImageFilter filter = this->extractionParameters.getIntegrationFilterEdges();

	margin = extractionParameters.getTotalOffset();

	if ((extractionParameters.extractPoints) && 
		(extractionParameters.distNonmaxPts / 2 > margin))
		margin = extractionParameters.distNonmaxPts / 2;

	if (this->featRoi->getBpp() == 32)	
	{
		for (int bnd = 0; bnd < deltaXX.getBands(); ++bnd)
		{
			float VarFac = this->VarNoiseFactor[bnd][0];
			for (unsigned long u = bnd; u < deltaXX.getNGV(); u += deltaXX.getDiffX())
			{
				float g = this->featRoi->pixFlt(u);
				deltaXY.pixFlt(u) = gradX.pixFlt(u) * gradY.pixFlt(u) * VarFac;
				deltaXX.pixFlt(u) = gradX.pixFlt(u) * gradX.pixFlt(u) * VarFac;
				deltaYY.pixFlt(u) = gradY.pixFlt(u) * gradY.pixFlt(u) * VarFac;
			};
		};
	}
	else
	{
		if (this->featRoi->getBpp() == 8)
		{
			for (int bnd = 0; bnd < deltaXX.getBands(); ++bnd)
			{
				for (unsigned long u = bnd; u < deltaXX.getNGV(); u += deltaXX.getDiffX())
				{
					long g = this->featRoi->pixUChar(u);
					float varFact = this->VarNoiseFactor[bnd][g];
					deltaXY.pixFlt(u) = gradX.pixFlt(u) * gradY.pixFlt(u) * varFact;
					deltaXX.pixFlt(u) = gradX.pixFlt(u) * gradX.pixFlt(u) * varFact;
					deltaYY.pixFlt(u) = gradY.pixFlt(u) * gradY.pixFlt(u) * varFact;
				};
			};
		}
		else if (this->featRoi->getBpp() == 16)	
		{
			for (int bnd = 0; bnd < deltaXX.getBands(); ++bnd)
			{
				for (unsigned long u = bnd; u < deltaXX.getNGV(); u += deltaXX.getDiffX())
				{
					long g = this->featRoi->pixUShort(u);
					float varFact = this->VarNoiseFactor[bnd][g];
					deltaXY.pixFlt(u) = gradX.pixFlt(u) * gradY.pixFlt(u) * varFact;
					deltaXX.pixFlt(u) = gradX.pixFlt(u) * gradX.pixFlt(u) * varFact;
					deltaYY.pixFlt(u) = gradY.pixFlt(u) * gradY.pixFlt(u) * varFact;
				};
			};
		};
	};

	if (this->extractionParameters.extractEdges)
	{
		if (gradX.getBands() > 1)
		{
			CImageBuffer deltaX(gradX);
			CImageBuffer deltaY(gradY);

			deltaX.SetToIntensity();
			deltaY.SetToIntensity();
			float pi2 = float(2.0f * M_PI);

			for (unsigned long u = 0; u < deltaX.getNGV(); u += deltaX.getDiffX())		
			{
				if ((fabs(deltaX.pixFlt(u)) < FLT_EPSILON) && (fabs(deltaY.pixFlt(u)) < FLT_EPSILON)) 
					gradientDirection(u) = 0.0;				
				else
				{
					gradientDirection(u) = float(atan2(deltaX.pixFlt(u), deltaY.pixFlt(u)));
					if (gradientDirection(u) <0.0) gradientDirection(u) += pi2;
				}
			};
		}
		else
		{
			float pi2 = float(2.0f * M_PI);
			for (unsigned long u = 0; u < gradX.getNGV(); u += gradX.getDiffX())
			{
				if ((fabs(gradX.pixFlt(u)) < FLT_EPSILON) && (fabs(gradY.pixFlt(u)) < FLT_EPSILON)) 
					gradientDirection(u) = 0.0;				
				else
				{
					gradientDirection(u) = float(atan2(gradX.pixFlt(u), gradY.pixFlt(u)));
					if (gradientDirection(u) <0.0) gradientDirection(u) += pi2;
				}
			};		
		};
	};

	deltaXX.SetToIntensity();
	deltaXY.SetToIntensity();
	deltaYY.SetToIntensity();


	CImageFilter pointFilter = this->extractionParameters.getIntegrationFilterPoints();
		
	bool computePoints = ((this->extractionParameters.extractPoints) && (filter.getSizeX() == pointFilter.getSizeX()));
	
	if (this->pointStrength) delete this->pointStrength;
	this->pointStrength = 0;
	if (this->extractionParameters.extractPoints) 
		this->pointStrength = new CImageBuffer(this->offsetX, this->offsetY, this->width, this->height, 1, 32, false);

	if ((this->extractionParameters.extractPoints) && (filter.getSizeX() != pointFilter.getSizeX()))
	{
		float varFactorPt = this->extractionParameters.getTotalVarianceFactorPoints() / 
			                this->extractionParameters.getTotalVarianceFactorEdges();
	
		CImageBuffer nxxPt(deltaXX.getOffsetX(), deltaXX.getOffsetY(), deltaXX.getWidth(), deltaXX.getHeight(), 1, 32, false);
		CImageBuffer nxyPt(deltaXX.getOffsetX(), deltaXX.getOffsetY(), deltaXX.getWidth(), deltaXX.getHeight(), 1, 32, false);
		CImageBuffer nyyPt(deltaXX.getOffsetX(), deltaXX.getOffsetY(), deltaXX.getWidth(), deltaXX.getHeight(), 1, 32, false);

		pointFilter.convolve(deltaXX, nxxPt, false);
		pointFilter.convolve(deltaXY, nxyPt, false);
		pointFilter.convolve(deltaYY, nyyPt, false);
	
		for (unsigned long u = 0; u < nGV; u += diffX)	
		{
			float nxx   = nxxPt.pixFlt(u);
			float nxy   = nxyPt.pixFlt(u);
			float nyy   = nyyPt.pixFlt(u);
			float det   = (nxx* nyy - nxy * nxy) * varFactorPt;
			float trace = nxx + nyy;
			if (trace < FLT_EPSILON) this->pointStrength->pixFlt(u) = 0.0f;
			else this->pointStrength->pixFlt(u) = det / trace;
		};
	}

	CImageBuffer auxiliary(deltaXX);
	filter.convolve(auxiliary,deltaXX, false);

	auxiliary.set(deltaXY);
	filter.convolve(auxiliary,deltaXY,false);
	
	auxiliary.set(deltaYY);
	filter.convolve(auxiliary,deltaYY,false);

	for (unsigned long u = 0; u < nGV; u += diffX)	
	{
		float nxx   = deltaXX.pixFlt(u);
		float nxy   = deltaXY.pixFlt(u);
		float nyy   = deltaYY.pixFlt(u);
		float det   = nxx* nyy - nxy * nxy;
		float trace = nxx + nyy;
		textureStrength(u) = trace;

		if (trace < FLT_EPSILON) 
		{
			textureIsotropy(u) = 1.;
			if (computePoints) this->pointStrength->pixFlt(u) = 0.0f;
		}
		else 
		{
			textureIsotropy(u) = float(4. * det / (trace * trace));
			if (computePoints) this->pointStrength->pixFlt(u) = det / trace;
		}
	};
	
	TextureStrength.setMargin(margin, 0.0f);
	TextureIsotropy.setMargin(margin, 0.0f);
	if (this->pointStrength) this->pointStrength->setMargin(margin, 0.0f);
};

void CFeatureExtractor::classifyTexture()
{
	if (this->extractionParameters.Mode == eCanny)
	{
		for (unsigned long u = 0; u < nGV; u += diffX)	
		{
			if (textureStrength(u) > this->extractionParameters.getHomogeneousThreshold())
				this->pixUC[u] = EDGE_NOMAX;
			else this->pixUC[u] = HOMOGENEOUS;
		};

	}
	else if (this->extractionParameters.Mode == eFoerstner)
	{
		for (unsigned long u = 0; u < nGV; u += diffX)	
		{
			if (textureStrength(u) > this->extractionParameters.getHomogeneousThreshold())
			{
				if (textureIsotropy(u) >= this->extractionParameters.qMin) this->pixUC[u] = POINT_RELMAX;
				else this->pixUC[u] = EDGE_NOMAX;
			}
			else this->pixUC[u] = HOMOGENEOUS;;
		}
	}
	else if (this->extractionParameters.Mode == eWaterShed)
	{
		computeWatershed();
	}
};
	
void CFeatureExtractor::removeOutsideBoundary()
{
	if (this->boundaryPolygon)
	{
		CImageBuffer boundaryBuf(this->offsetX, this->offsetY, this->width, this->height, 1, 8, true, 0.0f);
		boundaryBuf.fillByValue(*(this->boundaryPolygon), 255);

		CImageBuffer bufCpy(boundaryBuf);
		int cutOff = this->extractionParameters.getMorphologicFilterRegion().getSizeX();
		if (cutOff > 1)
		{		
			CImageFilter f(eMinimumFilter, cutOff, cutOff);
			f.binaryMinimumFilter(bufCpy, boundaryBuf,1);
		}

		for (unsigned long u = 0; u < boundaryBuf.getNGV(); ++u)
		{
			if (boundaryBuf.pixUChar(u) < 1) this->pixUC[u] = OUTSIDE_BOUNDARY;
		}
	}
	else if (this->validMask)
	{
		CImageBuffer boundaryBuf(this->offsetX, this->offsetY, this->width, this->height, 1, 8, true, 0.0f);

		int cutOff = this->extractionParameters.getMorphologicFilterRegion().getSizeX();
		if (cutOff > 1)
		{		
			CImageFilter f(eMinimumFilter, cutOff, cutOff);
			f.binaryMinimumFilter(*(this->validMask), boundaryBuf,1);
		}
		else boundaryBuf.set(*(this->validMask));

		for (unsigned long u = 0; u < boundaryBuf.getNGV(); ++u)
		{
			if (boundaryBuf.pixUChar(u) < 1) this->pixUC[u] = OUTSIDE_BOUNDARY;
		}

	}
};


void CFeatureExtractor::queue_init(queue_str *qq)
{
  qq->first = (cell_str *) malloc(sizeof(cell_str));
  qq->last = qq->first;
  qq->first->next = ((cell_str *) 0);
}

int CFeatureExtractor::queue_empty(queue_str *qq)
{
  if (qq->first == qq->last)
    return(1);
  else
    return(0);
}

void CFeatureExtractor::queue_add(queue_str *qq, item_str *item_a)
{
  qq->last->next = (cell_str *) malloc(sizeof(cell_str));
  
  if (!qq->last->next)
  {
	  int blabla = 0;
  };
  
  qq->last = qq->last->next;
  qq->last->item = *item_a;
  qq->last->next = ((cell_str *) 0);
}

int CFeatureExtractor::queue_first(queue_str *qq, item_str *item_r)
{
  cell_str *cell;

  if (qq->first == qq->last)
    return(0);
  cell = qq->first;
  qq->first = qq->first->next;
  *item_r = qq->first->item;
  free(cell);
  return 0;
}

bool CFeatureExtractor::computeWatershed()
{
#define MASK -2
#define WSHED 0
#define INIT -1
#define INQUEUE -3
#define BORDER -4
#define FILL -5

	bool ret = true;
	unsigned long *ims = new unsigned long [this->getNGV()];
	CImageBuffer s1(this->offsetX,this->offsetY,this->width,this->height,1,16,false, INIT);
	s1.setMargin(1, BORDER);

	item_str p_num;
	queue_str q;
	
	//Wasserscheiden Transformation
	queue_init(&q);            // initialize pixels' queue 
	int cr_label = 0, flag = 0;

	unsigned long H[256], HC[257];
	// beginning of the tri-distributive function 
	for (int i = 0; i < 256; i++) 
	{   // initialize histogram 
		H[i] = 0;
		HC[i] = 0;	
	}

	// Histogramm berechnen, min und max bestimmen
	int hmax, hmin;
	hmax = hmin = (int) this->textureStrength(0);
	for (int r = 1; r < this->height - 1; r++)
	{
		int c = r * this->width + 1;
		int cmax = c + this->width - 2;
		for (; c < cmax; c++) 
		{
			int t = (int) this->textureStrength(c);
			 ++H[t];
			if (t < hmin) hmin = t;
			if (t > hmax) hmax = t;
		}
	}

		// Summenhistogramm berechnen
	 HC[hmin] = 0;
	for (int i = hmin + 1; i <= hmax + 1; i++)
		HC[i] = HC[i - 1] + H[i - 1];
	
	for (int r = 1; r < this->height - 1; r++)
	{
		int c = r * this->width + 1;
		int cmax = c + this->width - 2;
		for (; c < cmax; c++) 
		{
			int t = (int) this->textureStrength(c);
			ims[HC[t]] = c;
			HC[t] += 1;
		}
	}

	int lower_b;
    int l;

	int neighbourhood = 8;

	// Nachbarschaftspixel festlegen
	int nachbar_1D[8];
	int nachbar_zeile[8];
	int nachbar_spalte[8];

	nachbar_1D[7] = -1 - this->width;nachbar_1D[0] = -this->width; nachbar_1D[4] = 1 - this->width;
	nachbar_1D[3] = -1;							 nachbar_1D[1] = 1; 
	nachbar_1D[6] = this->width - 1;	nachbar_1D[2] =  this->width; nachbar_1D[5] = 1 + this->width;

	nachbar_zeile[7] = -1;	nachbar_zeile[0] = -1;	nachbar_zeile[4] = -1;
	nachbar_zeile[3] =  0;							nachbar_zeile[1] =  0; 
	nachbar_zeile[6] =  1;	nachbar_zeile[2] =  1;	nachbar_zeile[5] =  1;
	
	nachbar_spalte[7] = -1;	nachbar_spalte[0] = 0;	nachbar_spalte[4] = 1;
	nachbar_spalte[3] = -1;							nachbar_spalte[1] = 1; 
	nachbar_spalte[6] = -1;	nachbar_spalte[2] = 0;	nachbar_spalte[5] = 1;

	for (int i = hmin; i <= hmax; i++) 
	{
        if (i != 0)
          lower_b = (int)HC[i - 1];
        else
          lower_b = 0;

        for (unsigned int j = lower_b; j < HC[i]; j++) 
		{
          l = (int)ims[j];
		  s1.pixShort(l) = MASK;
          for (int r = 0; r < neighbourhood; r++) 
		  {
            int pos = l + nachbar_1D[r];
            if ((s1.pixShort(pos) > 0) || (s1.pixShort(pos) == WSHED)) 
			{
              p_num.address = l;
              queue_add(&q, &p_num);
              s1.pixShort(l) = INQUEUE;
            }
          }
        }

        while (!queue_empty(&q)) 
		{
          queue_first(&q, &p_num);
          l = p_num.address;
          for (int r = 0; r < neighbourhood; r++) 
		  {
            int pos = l + nachbar_1D[r];
            if (s1.pixShort(pos) > 0)
			{
              if ((s1.pixShort(l) == INQUEUE) || ((s1.pixShort(l) == WSHED) && flag))
                s1.pixShort(l) = s1.pixShort(pos);
              else
                if ((s1.pixShort(l) > 0) && (s1.pixShort(l) != s1.pixShort(pos)))
				{
                  s1.pixShort(l) = WSHED;
                  flag = 0;
                }
            }
            else
              if (s1.pixShort(pos) == WSHED) 
			  {
                if (s1.pixShort(l) == INQUEUE) 
				{
                  s1.pixShort(l) = WSHED;
                  flag = 1;
                }
              }
              else
                if (s1.pixShort(pos) == MASK) 
				{
                  s1.pixShort(pos) = INQUEUE;
                  p_num.address = pos;
                  queue_add(&q, &p_num);
                }
          }
        }

        for (unsigned int j = lower_b; j < HC[i]; j++) 
		{
          l = (int)ims[j];
          if (s1.pixShort(l) == MASK) 
		  {
            s1.pixShort(l) = ++cr_label;
            p_num.address = l;
            queue_add(&q, &p_num);
            while (!queue_empty(&q)) 
			{
              queue_first(&q, &p_num);
              int pos = p_num.address;
              for (int r = 0; r < neighbourhood; r++) 
			  {
                int t = pos + nachbar_1D[r];
                if (s1.pixShort(t) == MASK)
				{
                  p_num.address = t;
                  queue_add(&q, &p_num);
                  s1.pixShort(t) = cr_label;
                }
              }
            }
          }
        }
	}
	
	delete [] ims;
	s1.setMargin(1, BORDER);

	for (unsigned int i=0; i< this->getNGV(); i++) 
	{
		short s = s1.pixShort(i); 
		
		if (s == WSHED) 	
		{
			this->pixUC[i] = EDGE_RELMAX;
        } 
		else if (s == BORDER) 	
		{
              this->pixUC[i] = HOMOGENEOUS; // perhaps: later also border!
        }
		else 	
		{
              this->pixUC[i] = HOMOGENEOUS;
        }
      }

	this->setMargin(3, HOMOGENEOUS);

	return ret;
};

void CFeatureExtractor::thinOutEdges(CImageBuffer &gradX, CImageBuffer &gradY)
{
	gradX.SetToIntensity();
	gradY.SetToIntensity();
	CImageBuffer delta045(offsetX, offsetY, width, height, 1, 32, false);
	CImageBuffer delta135(offsetX, offsetY, width, height, 1, 32, false);
	float sqrt2 = float(1.0f / sqrt(2.0));

	for (unsigned long u = 0; u < nGV; u += diffX)	
	{
		delta045.pixFlt(u) =  gradX.pixFlt(u) * sqrt2 + gradY.pixFlt(u) * sqrt2;
		delta135.pixFlt(u) = -gradX.pixFlt(u) * sqrt2 + gradY.pixFlt(u) * sqrt2;
	};

	long diffIndDiag045 = width + 1;
	long diffIndDiag135 = width - 1;

	for (unsigned long u = 0; u < nGV; u += diffX)
	{
		if (this->isEdgeRegionPixel(u))
		{
			float abstmp0   = fabs(gradX.pixFlt(u) ); float abstmp45  = fabs(delta045.pixFlt(u) );
			float abstmp90  = fabs(gradY.pixFlt(u));  float abstmp135 = fabs(delta135.pixFlt(u));
			if ((abstmp0 > abstmp45) && (abstmp0 > abstmp90) && (abstmp0 > abstmp135))
			{ // largest directional derivative is in x
				if ((abstmp0 > fabs(gradX.pixFlt(u - 1))) && (abstmp0 >= fabs(gradX.pixFlt(u + 1))))
					this->pixUChar(u) = EDGE_RELMAX;
			}
			else if ((abstmp45 > abstmp90) && (abstmp45 > abstmp135))
			{ // largest directional derivative is in 45 deg direction
				if ((abstmp45 >  fabs(delta045.pixFlt(u - diffIndDiag045))) && 
					(abstmp45 >= fabs(delta045.pixFlt(u + diffIndDiag045))))
					this->pixUChar(u) = EDGE_RELMAX;
			}
			else if (abstmp90 > abstmp135)
			{// largest directional derivative is in y
				if ((abstmp90 >  fabs(gradY.pixFlt(u - width))) && (abstmp90 >= fabs(gradY.pixFlt(u + width))))
					this->pixUChar(u) = EDGE_RELMAX;
			}
			else
			{	// largest directional derivative is in 135 deg direction		
				if ((abstmp135 >  fabs(delta135.pixFlt(u - diffIndDiag135))) &&
					(abstmp135 >= fabs(delta135.pixFlt(u + diffIndDiag135))) )
					this->pixUChar(u) = EDGE_RELMAX;
			}
		}
	}
};

void CFeatureExtractor::thinOutPoints()
{
	int d = (this->extractionParameters.distNonmaxPts - 1) / 2;


	int *relMaxIndices = new int[(this->width - 2) * (this->height - 2)];
	int *relMaxR = new int[(this->width - 2) * (this->height - 2)];
	int *relMaxC = new int[(this->width - 2) * (this->height - 2)];
	int nRelMax = 0;

	int diffInd[8] = { this->diffX, this->diffX + this->diffY, this->diffY, 
		               -this->diffX + this->diffY, -this->diffX , -this->diffX - this->diffY,
					   -this->diffY, this->diffX - this->diffY };

	int rmax = height - 1;
	int dCmax = width  - 2;

	for (int r = 1; r < rmax; r++)
	{
		int ind = r * this->diffY + 1;
		int indmax = ind + dCmax;
		for (int c = 1; ind < indmax; ++ind, ++c)
		{
			if (this->isPointRegionPixel(ind)) 
			{
				float v = this->pointStrength->pixFlt(ind);
				bool isRel = true;
				for (int n = 0; (n < 8) && isRel; ++n)
				{
					float v1 = this->pointStrength->pixFlt(ind + diffInd[n]);
					isRel = v1 < v;	
				}
				if (!isRel) this->pixUChar(ind) = POINT_NOMAX;
				else
				{
					relMaxIndices[nRelMax] = ind;
					relMaxR[nRelMax] = r;
					relMaxC[nRelMax] = c;
					++nRelMax;
				}
			}
		}
	}
/*

	CFileName fn("fex_Class.tif");
	fn.SetPath(protHandler.getDefaultDirectory());
	this->exportToTiffFile(fn.GetChar(),0, false);
*/

	for (int i = 0; i < nRelMax; ++i)
	{
		int ind = relMaxIndices[i];
		int r0 = relMaxR[i];
		int c0 = relMaxC[i];
		int rMin = r0 - d;
		int rMax = r0 + d;
		int cMin = c0 - d;
		int cMax = c0 + d;
		float v0 = this->pointStrength->pixFlt(ind);

		if (this->isPointPixel(ind))
		{
			bool relMax = true; 
			for (int j = 0; (j < nRelMax) && relMax && (relMaxR[j] <= rMax); ++j)
			{
				if ((j != i) && (relMaxR[j] >= rMin) && (relMaxC[j] >= cMin) && (relMaxC[j] <= cMax))
				{
					float v1 = this->pointStrength->pixFlt(relMaxIndices[j]);
					if (v1 > v0) relMax = false;
					else this->pixUChar(relMaxIndices[j]) = POINT_NOMAX;
				}
			}

			if (!relMax) this->pixUChar(ind) = POINT_NOMAX;
		}
	}
/*
	fn = "fex_Class_noMax.tif";
	fn.SetPath(protHandler.getDefaultDirectory());
	this->exportToTiffFile(fn.GetChar(),0, false);
*/
	delete [] relMaxIndices;
	delete [] relMaxR;
	delete [] relMaxC;
/*
	float dd = (float) this->extractionParameters.distNonmaxPts * this->extractionParameters.distNonmaxPts;

	int dminMax = d * (width + 1);
	unsigned long uMin = (d + 1) * width + d + 1;
	unsigned long jmax = height - d - 1;
	for (unsigned long j = d + 1; j < jmax; ++j, uMin += diffY)
	{
		unsigned long imax = uMin + width - this->extractionParameters.distNonmaxPts - 1;
		for (unsigned long i = uMin; i < imax; ++i)
		{
			if (this->isPointRegionPixel(i)) // skip not selected windows 
			{
				unsigned long uaMin = i - dminMax;
				for (int k = 0; k < this->extractionParameters.distNonmaxPts; ++k, uaMin += diffY)
				{
					unsigned long uaMax = uaMin + this->extractionParameters.distNonmaxPts;
					for (unsigned long ua = uaMin; ua < uaMax; ++ua)
					{
						if (ua != i)
						{
							if (this->pointStrength->pixFlt(i) > this->pointStrength->pixFlt(ua)) 						
							{
								if (this->isPointPixel(ua)) this->pixUChar(ua) = POINT_NOMAX;
							}
							else if (this->pointStrength->pixFlt(i) < this->pointStrength->pixFlt(ua)) 
								this->pixUChar(i) = POINT_NOMAX;
							else if ((this->pixUChar(ua) == 0) && (this->pixUChar(i) == 0))
							{
								if (this->isPointPixel(ua)) this->pixUChar(ua) = POINT_NOMAX;
							}
						}
					};
				};
			};
		};
	};

	fn = "fex_Class_noMax1.tif";
	fn.SetPath(protHandler.getDefaultDirectory());
	this->exportToTiffFile(fn.GetChar(),0, false);
*/
};

bool CFeatureExtractor::GradientDeriche (CImageBuffer &gradX, CImageBuffer &gradY, float Sigma1, float Sigma2)
{
	float pi2 = float(M_PI) * 0.5f;

	CImageBuffer buf(*(this->featRoi), true);

	buf.RGBToIntensity();

	bool ret = buf.convertToFloat();

	float offset = this->extractionParameters.getKforWatershed() * fsqrt(this->varNoise);

	if (ret)
	{
		if (Sigma2 < Sigma1)
		{
			float f = Sigma1;
			Sigma1 = Sigma2;
			Sigma2 = f;
		} 

		CImageFilter filter(eDiffXDeriche,7,7,Sigma1);
		
		if (Sigma1 != Sigma2)
		{
			float gx1, gy1, gx2, gy2, g1, g2;


			ret = filter.gradientDeriche(buf, gradX, gradY, Sigma1, false); 
			
			CImageBuffer gradX1(gradX, false);
			CImageBuffer gradY1(gradY, false);

			if (ret) ret = filter.gradientDeriche(buf, gradX1, gradY1, Sigma2, false); 
			if (ret)
			{
				for (unsigned long u = 0; u < this->getNGV(); u++) 
				{
					gx1 = gradX.pixFlt(u);
					gy1 = gradY.pixFlt(u);
					gx2 = gradX1.pixFlt(u);
					gy2 = gradY1.pixFlt(u);
					g1 =  fsqrt(gx1 * gx1 + gy1 * gy1) - offset;
					g2 =  fsqrt(gx2 * gx2 + gy2 * gy2) - offset;
					this->textureStrength(u) = (g1 < g2) ? g1:g2;
					if ((fabs(gx1) < FLT_EPSILON) && (fabs(gy1) < FLT_EPSILON)) 
						gradientDirection(u) = 0.0;					
					else
					{
						gradientDirection(u) = float(atan2(gx1, gy1));
						if (gradientDirection(u) <0.0) gradientDirection(u) += pi2;
					}
				}
			}
		} 
		else 
		{
			ret = filter.gradientDeriche(buf, gradX, gradY, Sigma1, false); 
			if (ret)
			{
				float gx1, gy1;

				for (unsigned long u = 0; u < this->getNGV(); u++) 
				{
					gx1 = gradX.pixFlt(u);
					gy1 = gradY.pixFlt(u);
					this->textureStrength(u) =  fsqrt(gx1 * gx1 + gy1 * gy1)  - offset;
					if ((fabs(gx1) < FLT_EPSILON) && (fabs(gy1) < FLT_EPSILON)) 
						gradientDirection(u) = 0.0;					
					else
					{
						gradientDirection(u) = float(atan2(gx1, gy1));
						if (gradientDirection(u) <0.0) gradientDirection(u) += pi2;
					}
				}
			}
		}
	}

	return ret;
}


int CFeatureExtractor::TestBorder (unsigned char *map, int Width, int Height, int i, int j, int s)
{
    if ((i < s) || (j < s) || (i > Width-s-1) || (j > Height-s-1)) {
        return 1;
    }
    return 0;
}

bool CFeatureExtractor::Gauss()
{
	float *map2 = new float [this->width * this->height];
	if (!map2) return false;

	int rmax = this->height - 1;
	int dcmax = this->width - 2;

	float fact1 = 1.0f / 16.0f;
	float fact2 = 2.0f / 16.0f;
	float fact4 = 4.0f / 16.0f;

	for (int r = 1; r < rmax; r++)
	{

		int c = r * this->width + 1;
		int cmax = c + dcmax;

		for (; c < cmax;c++) 
		{
			map2[c] = floor(fact4 *  this->textureStrength (c)+
						    fact2 * (this->textureStrength(c - this->width) + 
							         this->textureStrength(c + this->width)  +
							         this->textureStrength(c - 1) + 
									 this->textureStrength(c + 1)) +
						    fact1 * (this->textureStrength(c - this->width - 1) + 
							         this->textureStrength(c - this->width + 1)  +
							         this->textureStrength(c - 2) +
							         this->textureStrength(c + 2)) + 0.5f);
		}
	}

	unsigned long uMin = 0;
	unsigned long uMax = this->getIndex(1, 0);
	unsigned long uMaxInRow;
	for (; uMin < uMax; uMin += this->diffY)
	{
		uMaxInRow = uMin + this->diffY;
		for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
		{
			map2[u] = this->textureStrength(u);
		}
	};

	// left margin
	uMin = this->getIndex(1, 0);
	uMax = this->getIndex(height - 1, 0);
	for (; uMin < uMax; uMin += this->diffY)
	{
		uMaxInRow = uMin + 1;
		for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
		{
			map2[u] = this->textureStrength(u);
		}
	};

	// right margin
	uMin = this->getIndex(1, width - 1) + 0;
	uMax = this->getIndex(height - 1, width);
	for (; uMin < uMax; uMin += this->diffY)
	{
		uMaxInRow = uMin + 1;
		for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
		{
			map2[u] = this->textureStrength(u);
		}
	}
		
				// bottom margin
	uMin = this->getIndex(height - 1, 0);
	uMax = this->getIndex(height, 0);
	for (; uMin < uMax; uMin += this->diffY)
	{
		uMaxInRow = uMin + this->diffY;
		for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
		{
			map2[u] = this->textureStrength(u);
		}
	}

	for (unsigned long u =0; u < this->getNGV(); u++) 
	{
		this->textureStrength(u) = map2[u];
//		if (this->textureStrength(u) < 0.0f) this->textureStrength(u) = 0.0f;
//		else if (this->textureStrength(u) > 255.0f) this->textureStrength(u) = 255.0f;
	}

	delete [] map2;

	return true;
}
bool CFeatureExtractor::textureClassification(CImageBuffer &gradX, CImageBuffer &gradY)
{
	bool ret = true;

	if (this->listener)
	{
		CCharString str("Analysing image noise");
		if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
		this->listener->setTitleString(str.GetChar());
		this->listener->setProgressValue(15.0);
	}

	if (this->extractionParameters.Mode != eWaterShed)
	{
		computeDerivatives(gradX, gradY);

		if (this->extractionParameters.Mode == eCanny)
			computeGradientMagnitudeAndDirectionCanny(gradX, gradY);
		else if (this->extractionParameters.Mode == eFoerstner)
			computeGradientMagnitudeAndDirectionFoerstner(gradX, gradY);
	}
	else
	{
		this->GradientDeriche(gradX,  gradY, 1.0f / 2.0f, 1.0f / 3.0f);

		//Gauss glaetten
	
		for (int i = 0; i < this->extractionParameters.getWatershedSmoothingIterations(); i++) 
		{
		   Gauss();
		}

		// Gradientenwerte auf Bereich [0 255] bringen

		for (unsigned long u = 0; u < this->getNGV(); u++) 
		{
			this->textureStrength(u) = floor (this->textureStrength(u) + 0.5f);
			if (this->textureStrength(u) < 0.0f) this->textureStrength(u) = 0.0f;
			else if (this->textureStrength(u) > 255.0f) this->textureStrength(u) = 255.0f;
		}

	}

	float percentil = 1.0f;

	if (this->extractionParameters.NoiseModel == eMedian)
	{
		percentil = TextureStrength.getPercentil(1.0f - this->extractionParameters.getAlpha(), margin);
	};

	if (this->extractionParameters.Mode == eWaterShed)
	{
		this->extractionParameters.setHomogeneousThreshold(percentil, 1);
	}
	else
	{
		this->extractionParameters.setHomogeneousThreshold(percentil, this->featRoi->getBands());
	}

	if (this->listener)
	{
		CCharString str("Classifying image texture");
		if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
		this->listener->setTitleString(str.GetChar());
		this->listener->setProgressValue(42.0);
	}
	
	classifyTexture();

	removeOutsideBoundary();

	if (this->extractionParameters.Mode == eWaterShed)
	{
		this->TextureStrength.setMargin(this->extractionParameters.getTotalOffset(), 0.0);

		if (outputMode >= eMAXIMUM)
		{
			CFileName fn = "fex_texture_WS.tif"; 
			fn.SetPath(protHandler.getDefaultDirectory());
			this->TextureStrength.exportToTiffFile(fn.GetChar(), 0, true);
		}

		computeDerivatives(gradX, gradY);
		computeGradientMagnitudeAndDirectionCanny(gradX, gradY);
		this->TextureStrength.setMargin(this->extractionParameters.getTotalOffset(), 0.0);
	}

	int margin = this->extractionParameters.getTotalOffset();
	if (this->boundaryPolygon || this->validMask) margin += this->extractionParameters.getMorphologicFilterRegion().getSizeX() - 1;
	this->setMargin(margin, OUTSIDE_BOUNDARY);

	if (this->listener)
	{
		CCharString str("Nonmaxima suppression");
		if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
		this->listener->setTitleString(str.GetChar());
		this->listener->setProgressValue(57.0);
	}

	if ((this->extractionParameters.extractEdges) && (this->extractionParameters.Mode != eWaterShed))
		thinOutEdges(gradX, gradY);

	if ((this->extractionParameters.Mode == eFoerstner) && 
		(this->extractionParameters.extractPoints))
		thinOutPoints();
	
	if (outputMode >= eMAXIMUM)
	{
		CFileName fn("fex_Class.tif");
		fn.SetPath(protHandler.getDefaultDirectory());
		this->exportToTiffFile(fn.GetChar(),0, false);
		fn = "fex_texture.tif"; 
		fn.SetPath(protHandler.getDefaultDirectory());
		this->TextureStrength.exportToTiffFile(fn.GetChar(), 0, true);
	}

	return ret;
};

bool CFeatureExtractor::extractFeatures(CXYPointArray *xypts, CXYContourArray *xycont, 
										CXYPolyLineArray *xyedges, CLabelImage *labels,
										CRAG *rag, bool merge)
{
	if (this->listener)
	{
		listenerTitleString = this->listener->getTitleString();
		CCharString str("Computing derivatives");
		if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
		this->listener->setTitleString(str.GetChar());
		this->listener->setProgressValue(5.0);
	}

	CImageBuffer deltaX(offsetX, offsetY, width, height, featRoi->getBands(), 32, false);
	CImageBuffer deltaY(offsetX, offsetY, width, height, featRoi->getBands(), 32, false);

	this->initNoiseModelParameters();

	bool ret = textureClassification(deltaX, deltaY);


	if ((this->extractionParameters.Mode == eFoerstner) && 
		(this->extractionParameters.extractPoints))
	{
		if (this->listener)
		{
			CCharString str("Extracting points");
			if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
			this->listener->setTitleString(str.GetChar());
			this->listener->setProgressValue(70.0);
		}

		extractPoints(xypts);
	}


	if (this->extractionParameters.extractEdges && xycont && xyedges) 
	{
		if (this->listener)
		{
			CCharString str("Extracting edges");
			if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
			this->listener->setTitleString(str.GetChar());
			this->listener->setProgressValue(80.0);
		}

		extractContours(xycont);

		if (this->listener)
		{
			CCharString str("Thinning edges");
			if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;

			this->listener->setTitleString(str.GetChar());
			this->listener->setProgressValue(85.0);
		}

		if (xyedges) thinOutContours(xycont, xyedges);
	}
	
	if (this->extractionParameters.extractRegions && labels) 
	{
		extractRegions(labels, rag, merge);
	}


	if (this->listener)
	{
		CCharString str("Feature extraction finished");
		if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
		this->listener->setTitleString(str.GetChar());
		this->listener->setProgressValue(100.0);
	}

	return ret;
};

//===============================================================================

bool CFeatureExtractor::extractRegions(CLabelImage *labels, CRAG *rag, bool merge)
{
	bool ret = true;
	if (this->listener)
	{
		CCharString str("Extracting regions");
		if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
		this->listener->setTitleString(str.GetChar());
		this->listener->setProgressValue(87.0);
	}


	CImageBuffer homogMask(this->getOffsetX(), this->getOffsetY(), this->getWidth(), this->getHeight(), 1, 1, true);

	if (this->extractionParameters.Mode != eWaterShed)
	{
		for (unsigned long u = 0; u < this->getNGV(); ++u)
		{
			unsigned char val = this->pixUChar(u);
			if ((val == HOMOGENEOUS) || (val == EDGE_NOMAX) || (val == POINT_NOMAX))
				homogMask.pixUChar(u) = 0;
			else homogMask.pixUChar(u) = 1;
		}

		homogMask.setMargin(this->extractionParameters.getTotalOffset(), 0);
		
		if (this->extractionParameters.getMorphologicFilterSize() > 1)
		{
			CImageFilter morpho(this->extractionParameters.getMorphologicFilterRegion());
			CImageBuffer homogMaskOpen(homogMask);
			morpho.binaryMorphologicalClose(homogMaskOpen, homogMask, 1);
		}

		for (unsigned long u = 0; u < this->getNGV(); ++u)
		{
			homogMask.pixUChar(u) = 1 - homogMask.pixUChar(u);
		}
	}
	else
	{
		for (unsigned long u = 0; u < this->getNGV(); ++u)
		{
			unsigned char val = this->pixUChar(u);
			if (val == HOMOGENEOUS) homogMask.pixUChar(u) = 1;
			else homogMask.pixUChar(u) = 0;
		}

	}
	
	homogMask.setMargin(this->extractionParameters.getTotalOffset(), 0);
	labels->initFromMask(homogMask, 1);	
	labels->updateVoronoi();


	// for testing only!		
	//extractParallelLines(*(this->featRoi));

	if (rag)
	{
		CImageBuffer *pTexCopy = 0;
		if (merge) 
		{	
			float homogFac = 1.0f / this->extractionParameters.getHomogeneousThreshold();

		   
			pTexCopy = new CImageBuffer(this->TextureStrength, false);
			for (unsigned int i = 0; i < this->TextureStrength.getNGV(); ++i)
			{
				pTexCopy->pixFlt(i) = this->textureStrength(i) * homogFac;
			}
		}

		if (this->listener)
		{
			CCharString str("Initialising RAG");
			if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
			this->listener->setTitleString(str.GetChar());
			this->listener->setProgressValue(90.0);
		}

		rag->initialise(labels, *this->featRoi, pTexCopy, 3);

		if (merge) 
		{
			if (this->listener)
			{
				CCharString str("Merging RAG");
				if (!listenerTitleString.IsEmpty()) str = listenerTitleString + ": " + str;
				this->listener->setTitleString(str.GetChar());
				this->listener->setProgressValue(95.0);
			}
		
			rag->merge(*this->featRoi, pTexCopy, this->extractionParameters, this->extractionParameters.getATKIS(), this->extractionParameters.getATKIS_threshold());
		}

		if (pTexCopy) delete pTexCopy;
	}

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
	}

	return ret;
};

//=================================================================================

void CFeatureExtractor::extractParallelLines(CImageBuffer &ROI)
{
	float RhoMin = 0.0, RhoMax = 200.0, DeltaRho = 1.0, DeltaPhi = 1.0;

	CHoughTransformation Hough_image(RhoMin, RhoMax, DeltaRho, DeltaPhi);

};
//=================================================================================


/*
bool CFeatureExtractor::extractEllipse(CXYEllipse &ellipse)
{
	float distThresh  = 1.5;
	float angleThresh = (float)M_PI / 6;
	float maxDim = float(sqrt(float(height * height + width * width)) / float(2.0));
	if (extractionParameters.minLineLength < 5) extractionParameters.minLineLength = 5;

	CImageBuffer deltaX(offsetX, offsetY, width, height, featRoi->getBands(), 32, false);
	CImageBuffer deltaY(offsetX, offsetY, width, height, featRoi->getBands(), 32, false);
	this->extractionParameters.extractPoints = 0;
	bool ret = textureClassification(deltaX, deltaY);

	CXYContourArray xycont;
	extractContours(&xycont);

	CXYPoint p;
	C2DPoint appx(this->width / 2,this->height / 2);
	long red;
	this->featRoi->getCentre(this->height/2, this->width / 2, this->width /2 - 2, this->height / 2 - 2, p, red);

	p.set(p.getX() - offsetX, p.getY() - offsetY);


//	CImageBuffer *pEdgePixels = new CImageBuffer(offsetX, offsetY, width, height, 1, 8, true, 0);


	CXYEllipseArray ells;

	for (unsigned int i = 0; i < graph->c_max; ++i)
	{
		CXYEllipse ell;
		if (graph->c[i].active && (graph->c[i].getLength() > extractionParameters.minLineLength))
		{
			if (graph->c[i].approxEllipse(ell,i)) 
			{
				if ((ell.getA() < maxDim) && (ell.getB() < maxDim) && 
					(ell.getX0() > 0)     && (ell.getY0() > 0) &&
					(ell.getX0() < width) && (ell.getY0() < height))
				ells.Add(ell);
			}
		}
	}

	unsigned long MaxInside = 0;
	for (int i = 0; i < ells.GetSize(); ++i)
	{
		AIM_Contour cont;
		cont.active = true;
		cont.initChain(200);
		CImageBuffer pix(offsetX, offsetY, width, height, 1, 8, true, 0);
		
		for (long y = 0; y < height; ++y)
		{
			long uMin = y * width;
			for (long x = 0; x < width; ++x)
			{
				if (this->isEdgePixel(uMin + x))
				{
					C2DPoint p(x,y);	
					C2DPoint normal;
					double dist = ells.GetAt(i)->getDistance(p, normal);			
					if (dist < distThresh)
					{
							pix.pixUChar(uMin + x) = 255;
							AIM_Pixel px;
							px.x = x; px.y = y;
							cont.addP(LAST, px);
					}
				}
			}
		}
		if (cont.getLength() > MaxInside)
		{
			CXYEllipse ell;
			if (cont.approxEllipse(ell, 1000)) 
			{
				if ((ell.getA() < maxDim) && (ell.getB() < maxDim) && 
					(ell.getX0() > 0)     && (ell.getY0() > 0) &&
					(ell.getX0() < width) && (ell.getY0() < height))
				{
					ellipse = CXYEllipse(ell.getA(), ell.getB(), ell.getTheta(), ell.getX0() + offsetX, ell.getY0() + offsetY, ell.getLabel());
					ellipse.setCovariance(*ell.getCovariance());
					MaxInside = cont.getLength();
					pix.exportToTiffFile("E:\\projects\\ellipse_max.tif", 0, true);
				}
				else pix.exportToTiffFile("E:\\projects\\ellipse.tif", 0, true);
			}
			else pix.exportToTiffFile("E:\\projects\\ellipse.tif", 0, true);
		}
		else pix.exportToTiffFile("E:\\projects\\ellipse.tif", 0, true);

	}

	*/
/*
    vector<C2DPoint> candVec;

	for (long y = 0; y < height; ++y)
	{
		long uMin = y * width;
		for (long x = 0; x < width; ++x)
		{
			if (pEdgePixels->pixUChar(uMin + x))
			{
				C2DPoint P(x,y);
				candVec.push_back(P);
			};
		}
	}

	pEdgePixels->exportToTiffFile("E:\\projects\\edges.tif", 0);

	delete pEdgePixels;

	if (candVec.size() < 5) ret = false;
	else
	{

		CXYEllipse cEll;
		long MaxInside = 0;
		bool found = false;
		int iter = 0;
		srand( (unsigned)time( NULL ) );

		while (!found && (iter < 500))
		{
			vector<C2DPoint> pointVector(5);
			long randInd = long((((double) rand() / (double) RAND_MAX) * (candVec.size() - 1)));
			pointVector[0] = candVec[randInd]; 
            randInd = long((((double) rand() / (double) RAND_MAX) * (candVec.size() - 1)));
			pointVector[1] = candVec[randInd]; 
            randInd = long((((double) rand() / (double) RAND_MAX) * (candVec.size() - 1)));
			pointVector[2] = candVec[randInd]; 
            randInd = long((((double) rand() / (double) RAND_MAX) * (candVec.size() - 1)));
			pointVector[3] = candVec[randInd]; 
            randInd = long((((double) rand() / (double) RAND_MAX) * (candVec.size() - 1)));
			pointVector[4] = candVec[randInd]; 

			
			if (cEll.init(pointVector))
			{
				if ((cEll.getA() < maxDim) && (cEll.getB() < maxDim) && 
					(cEll.getX0() > 0)     && (cEll.getY0() > 0) &&
					(cEll.getX0() < width) && (cEll.getY0() < height))
				{
					CImageBuffer pix(offsetX, offsetY, width, height, 1, 8, true, 0);

					long nInside = 0.0f; 
					for (int i = 0; i < candVec.size(); ++i)
					{
						C2DPoint p = candVec[i];
						C2DPoint normal;
						long index = long(floor(p.x + 0.5) + floor(p.y + 0.5) * width);
						double dist = cEll.getDistance(p, normal);	
						float angle = diffori(this->gradientDirection(index), atan2(normal.y, normal.x));

						if ((dist < distThresh) && (fabs(angle) < angleThresh))
						{
							++nInside;
							pix.pixUChar(index) = 255;
						}
					}
					if (nInside > MaxInside)
					{
						ellipse = cEll;
						MaxInside = nInside;
						pix.exportToTiffFile("E:\\projects\\ellipse_max.tif", 0);
					}
					else pix.exportToTiffFile("E:\\projects\\ellipse.tif", 0);
				}
			}

			if (MaxInside > 100) found = true;
			++iter;
		}

		if (MaxInside < 5) ret = false;
		else
		{			
			vector<C2DPoint> pointVector(MaxInside);
			long pointIdx = 0;
			CImageBuffer pix(offsetX, offsetY, width, height, 1, 8, true,0);
			for (long y = 0; y < height; ++y)
			{
				long uMin = y * width;
				for (long x = 0; x < width; ++x)
				{
					if (this->isEdgePixel(uMin + x))
					{
						C2DPoint p(x,y);
						double dist = cEll.getDistance(p);	

						if (dist < distThresh)
						{
							pix.pixUChar(uMin + x) = 255;
							pointVector[pointIdx].x = x + offsetX;
							pointVector[pointIdx].y = y + offsetY;
							pointIdx++;
						}
					}
				}
			}
			ellipse.init(pointVector);
			pix.exportToTiffFile("E:\\projects\\ellipse.tif", 0);
		}
	}
*/
/*
	return ret;
};
*/
void CFeatureExtractor::extractPoints(CXYPointArray *xyPoints)
{
	int        num_pts = 0;   // point counter 
	float      t;             // test value

	CXYPoint CircularPoint, CornerPoint;
	float CircularVtPV, CornerVtPV;

	int d=(this->extractionParameters.getOperSizePts()-1)/2;
  

//	CLabel CornerLabel("PCo");
//    CornerLabel += 1;
//	CLabel CircleLabel("PCi");
//    CircleLabel += 1;
//	CLabel TextureLabel("PTx");
//    TextureLabel += 1;

	//  loop over the selected windows (weights > 0.)  
	for (long j =0; j < height; j++)
	{
		unsigned long uMin = j * width;
		for (long i = 0; i < width; i++)
		{
			if (this->isPointPixel(i + uMin))
			{ 
				long redundancy = 0;

				this->featRoi->getCentreAndCorner(j, i, this->extractionParameters.getOperSizePts(), 
					                              this->extractionParameters.getOperSizePts(), 
					                              CircularPoint, CornerPoint, CircularVtPV, CornerVtPV, 
												  redundancy);

				if ((CircularVtPV > 0.0f) && (CornerVtPV > 0.0f))	
				{
					//  test value 				
					t = CircularVtPV / CornerVtPV;

					// classification and add offsets 
					if (t > this->extractionParameters.getCornerThreshold())
					{
//						CornerPoint.setLabel(CornerLabel);
						xyPoints->Add(CornerPoint);
//						CornerLabel++;
					}
					else if (1/t > this->extractionParameters.getCircleThreshold())
					{
//						CircularPoint.setLabel(CircleLabel);
						xyPoints->Add(CircularPoint);
//						CircleLabel++;
					}
					else
					{
//						CornerPoint.setLabel(TextureLabel);
						xyPoints->Add(CornerPoint);
//						TextureLabel++;
					}
				}
			}   //  end if (wt[i + uMin] > 0)
		}	// end for (i = 0; i < nx; i++)
	} // end for (j =0; j < ny; j++)
}
	  
void CFeatureExtractor::subpixelEdgePoint(C2DPoint &point) const
{
	unsigned long u = this->getIndex((long)point.y, (long)point.x);
	float angle = mapangle(this->GradientDirection.pixFlt(u) + float(M_PI * 0.5f),180);
    float f45 = (float) M_PI / 4;
	
	float g0 = this->textureStrength(u);
	float tanAngle;

    float dg11, dg12, dg21, dg22;

	if (angle < f45)
	{
		tanAngle = tan(angle);
		dg11 = textureStrength(u + diffX);
		dg12 = textureStrength(u + diffY + diffX);
		dg21 = textureStrength(u - diffX);
		dg22 = textureStrength(u - diffY - diffX);
	}
	else if (angle < 2.0f * f45)
	{
		tanAngle = (float) tan(M_PI * 0.5f - angle);
		dg11 = textureStrength(u + diffY);
		dg12 = textureStrength(u + diffY + diffX);
		dg21 = textureStrength(u - diffY);		
		dg22 = textureStrength(u - diffY - diffX);
	}
	else if (angle < 3.0f * f45)
	{
		tanAngle = (float) fabs(tan(M_PI * 0.5f - angle));
		dg11 = textureStrength(u + diffY);
		dg12 = textureStrength(u + diffY - diffX);
		dg21 = textureStrength(u - diffY);
		dg22 = textureStrength(u - diffY + diffX);
	}
	else 
	{
		tanAngle = fabs(tan(angle));
		dg11 = textureStrength(u - diffX);
		dg12 = textureStrength(u + diffY - diffX);
		dg21 = textureStrength(u + diffX);
		dg22 = textureStrength(u - diffY + diffX);
	}

	float g1 = dg11 + tanAngle * (dg12 - dg11);
	float g2 = dg21 + tanAngle * (dg22 - dg21);

	float d = 2.0f * (g2 + g1 - g0 * 2.0f);
	if (fabs(d) < FLT_EPSILON) d = 0.0f;
	else d = (g2 - g1) / (d * sqrt(1.0f + tanAngle * tanAngle));

	if (fabs(d) > 0.75f) d = 0.0f;

	point.x += cos(angle) * d + offsetX;
	point.y += sin(angle) * d + offsetY;
};

bool CFeatureExtractor::extractContours(CXYContourArray *xycont)
{
	bool ret = true;

	CSortVector<AIM_Pixel> *startPoints = new CSortVector<AIM_Pixel>;
	getStartPoints(*startPoints, 25.0);


	//================================================================
	// Generate Contour Graph
	//================================================================
  
	if (graph) delete graph;
	graph = new AIM_Graph(width + height, width + height, width, height);

	
	aim_contour_aggregation(*startPoints);
  

	delete startPoints;

	//================================================================
	// POSTPROCESSING
	//================================================================

	graph->initAttr();
	graph->groupContours(false);
	graph->splitContours();
 
	Attribute_graph();

	float avg_str,avg_len,med_str, strength = 1.0;

	graph->getStatistics(avg_str, avg_len, &med_str, NULL);
	int no_clear = graph->clean((float)(strength*med_str), graph->attr->straight +(float) 0.5);
//	no_clear = graph->removeClosed((int) graph->attr->straight);
	
	graph->groupContours(false);
	graph->splitContours();
	graph->groupContours(true);
 
	Attribute_graph();
	graph->dcelEdgeLinks();
	
	for (unsigned long c_no=0; c_no < graph->c_max ;c_no++)
	{	
		if (graph->c[c_no].active)
		{
			C_attributes *attr= graph->c[c_no].getAttr();
			
			if (graph->c[c_no].getLength() > this->extractionParameters.minLineLength)
			{			
				int n = xycont->GetSize();
				int contournumber = n;
				CLabel lineLabel("Contour");
				lineLabel += n;
				CXYContour *contour = xycont->Add();	
				contour->setLabel(lineLabel);
				for (unsigned long j = 0; j < graph->c[c_no].getLength(); j++)
				{	
					C2DPoint tmp ;

					tmp.x = graph->c[c_no].getPixel(j).x;
					tmp.y = graph->c[c_no].getPixel(j).y;
					this->subpixelEdgePoint(tmp);
					contour->addPoint(tmp);
				}

				contour->addVertex(0,graph->c[c_no].v_no[0]);
				contour->addVertex(1,graph->c[c_no].v_no[1]);
			}	
		}
	}	

	return ret;
};

void CFeatureExtractor::thinOutContours(CXYContourArray *xycont, CXYPolyLineArray *xyedges)
{
	if (xycont->GetSize())
	{
		xyedges->RemoveAll();

		CLabel EdgeLabel("Line");
		EdgeLabel += 0; 
		double sigma0 = 0.33f;
		float probability = 0.95f;

		statistics::setNormalQuantil (probability);
		statistics::setChiSquareQuantils(probability);

		CXYContourArray localContours(*xycont);

//		localContours.connectContours();

		for (long i = 0; i < localContours.GetSize(); ++i)
		{
			CXYContour *contour = localContours.GetAt(i);
			CXYPolyLine poly;
			poly.setLabel(EdgeLabel);
			++EdgeLabel;
			poly.initFromContour(contour, this->extractionParameters.lineThinningThreshold, sigma0);
			xyedges->add(poly);
		};
	};
}

void CFeatureExtractor::TiffDump(char *filePart) const
{
	char *filename = new char[2048];
	const char *path = "E:\\projects\\";
	const char *ext = ".tif";

	strcpy(filename, path);
	strcat(filename, "img");
    strcat(filename, filePart);
	strcat(filename, ext);

	if (featRoi->getPixUC())
		TiffDump(featRoi->getPixUC(), featRoi->getBands(), filename);
	else if (featRoi->getPixUS())
		TiffDump(featRoi->getPixUS(), featRoi->getBands(), filename);
	else 
		TiffDump(featRoi->getPixF(), featRoi->getBands(), filename);
	
	CImageBuffer auxiliary(TextureStrength);
	for (unsigned long u = 0; u < nGV; u += diffX)	
	{
		auxiliary.pixFlt(u) = sqrt(auxiliary.pixFlt(u));
	}

	strcpy(filename, path);
	strcat(filename, "W");
    strcat(filename, filePart);
	strcat(filename, ext);
	TiffDump(auxiliary.getPixF(), 1, filename);

	if (this->extractionParameters.Mode == eFoerstner)
	{
		strcpy(filename, path);
		strcat(filename, "Q");
		strcat(filename, filePart);
		strcat(filename, ext);
		TiffDump(TextureIsotropy.getPixF(), 1, filename);

		for (unsigned long u = 0; u < nGV; u += diffX)	
		{
			auxiliary.pixFlt(u) = atan(gradientDirection(u));
		}
	}
	else auxiliary.set(this->GradientDirection);

	strcpy(filename, path);
	strcat(filename, "D");
    strcat(filename, filePart);
	strcat(filename, ext);

	TiffDump(auxiliary.getPixF(), 1, filename);

	strcpy(filename, path);
	strcat(filename, "class");
    strcat(filename, filePart);
	strcat(filename, ext);

	TiffDump(this->getPixUC(), 1, filename);

	delete [] filename;
};

void CFeatureExtractor::TiffDump(short *img, int nbands, char *filename) const
{
	unsigned long nPix = width * height;
	if (nbands == 3) nPix *= 4;
	else nPix *= nbands;

	short max = SHRT_MIN;
	short min = SHRT_MAX;
	for (unsigned long u = 0; u < nPix; ++u)
	{
		if (img[u] < min) min = img[u];
		if (img[u] > max) max = img[u];
	};

	float factor = 0.0f;

	if (max > min) factor = 255.0f / (max - min);

	unsigned char* charBuffer = new unsigned char[nPix];

	for (unsigned long u = 0; u < nPix; ++u)
	{
		charBuffer[u] = (unsigned char)(float(float((img[u] - min)) * factor));
	};

	TiffDump(charBuffer, nbands, filename);

	delete[] charBuffer;
};

void CFeatureExtractor::TiffDump(unsigned short *img, int nbands, char *filename) const
{
	unsigned long nPix = width * height;
	if (nbands == 3) nPix *= 4;
	else nPix *= nbands;

	short max = 0;
	unsigned short min = USHRT_MAX;
	for (unsigned long u = 0; u < nPix; ++u)
	{
		if (img[u] < min) min = img[u];
		if (img[u] > max) max = img[u];
	};

	float factor = 0.0f;

	if (max > min) factor = 255.0f / (max - min);

	unsigned char* charBuffer = new unsigned char[nPix];

	for (unsigned long u = 0; u < nPix; ++u)
	{
		charBuffer[u] = (unsigned char)(float(float((img[u] - min)) * factor));
	};

	TiffDump(charBuffer, nbands, filename);

	delete[] charBuffer;
};

void CFeatureExtractor::TiffDump(float *img, int nbands, char *filename) const
{
	unsigned long nPix = width * height;
	float max = -FLT_MAX;
	float min = FLT_MAX;
	for (unsigned long u = 0; u < nPix; ++u)
	{
		if (img[u] < min) min = img[u];
		if (img[u] > max) max = img[u];
	};

	float factor = 0.0f;

	if (max > min) factor = 255.0f / (max - min);

	unsigned char* charBuffer = new unsigned char[nPix];

	for (unsigned long u = 0; u < nPix; ++u)
	{
		charBuffer[u] = (unsigned char)(float((img[u] - min) * factor));
	};

	TiffDump(charBuffer, nbands, filename);

	delete[] charBuffer;
};

void CFeatureExtractor::TiffDump(unsigned char *img, int nbands, char *filename) const
{
	TIFF * tif;
	tif = TIFFOpen(filename, "w");

	if (tif)
	{
		TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);
		TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, nbands);
		TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, width);
		TIFFSetField(tif, TIFFTAG_IMAGELENGTH, height);
		TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
		TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, 1);
		if (nbands == 1) 
		TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
		else TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);

		long diffx = (nbands != 3) ?  nbands : nbands + 1;
		diffx *= width;

		unsigned char* stripBuffer = new unsigned char[TIFFStripSize(tif)];
  
		unsigned long offset = 0;
        for (long row = 0; row < height; ++row, offset += diffx)
		{
			if (nbands != 3) memcpy(stripBuffer, img + offset, diffx);
			else
			{
				unsigned char *old = img + offset;
				for (long u = 0; u < width; ++u, ++old)
				{
					stripBuffer[u * 3 + 2] = *old; ++old;
					stripBuffer[u * 3 + 1] = *old; ++old;
					stripBuffer[u * 3    ] = *old; ++old;
				};
			};

			TIFFWriteEncodedStrip(tif, TIFFComputeStrip(tif, row, 0), stripBuffer, TIFFStripSize(tif));
		}

		delete [] stripBuffer;

		TIFFClose(tif);
		tif = NULL;
	};
};

