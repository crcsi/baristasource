#include "SegmentAMatrix.h"
#include "LabelImage.h"
#include "sortVector.h"




CSegmentAMatrix::CSegmentAMatrix(int nlabels):nLabels(nlabels)
{
	if (nlabels <= 0) nLabels = 0;
	else
	{
		unsigned int length = nLabels *(nLabels -1 ) / 2;
		this->resize(length, 0);
	}
}

CSegmentAMatrix::CSegmentAMatrix(const CSegmentAMatrix &mat): vector<unsigned int>(mat), nLabels(mat.nLabels)
{
}

CSegmentAMatrix::~CSegmentAMatrix()
{

}

void CSegmentAMatrix::IndexToLabels(unsigned int index, unsigned int &label1, unsigned int &label2) const
{

};
	
	
void CSegmentAMatrix::dump(const CCharString &FileName) const
{
	ofstream oFile(FileName.GetChar());
	if (oFile.good())
	{
		this->dump(oFile);
	}
}

void CSegmentAMatrix::dump(ofstream &oFile) const
{
	oFile.setf(ios::fixed, ios::floatfield);
	oFile.precision(3);

	if (this->nLabels < 1 )
	{
		oFile << "empty matrix";
	}
	else 
	{
		for (unsigned int i = 1; i < this->nLabels; i++)
		{
			oFile << "row: ";
			oFile.width(5);
			oFile << i;
			unsigned int j = 1;

			for (; j <= i; j++)
			{	
				oFile.width(5);
				oFile << " ";
			}

			for (;j < this->nLabels; j++)
			{	
				oFile.width(5);
				oFile << (*this)(i,j);
			}
		}
		oFile << endl;
	}
};

void CSegmentAMatrix::calculateFromEdgePixels(CLabelImage &labels,int *boundary)
{
	int margin = 1;
	unsigned long uMin = labels.getIndex(margin, margin);
	unsigned long uMax = labels.getIndex(labels.getHeight() - margin, labels.getWidth() - margin);
	unsigned long uMaxRow = labels.getIndex(1, labels.getWidth() - margin);

	unsigned long rv;
	CSortVector<unsigned short> eight_neighbourhood(8,0);	//to check the 8th-neighbourhood of a pixel


	for(; uMaxRow < uMax; uMin += labels.getDiffY(), uMaxRow += labels.getDiffY())
	{
		for (unsigned long u = uMin; u < uMaxRow; u += labels.getDiffX())
		{
			unsigned short label = labels.getLabel(u);
			if (!label)//this is an edge pixel!
			{
				//label of the neighbours
				rv = u + labels.getDiffX();
				eight_neighbourhood[0] = labels.getLabel(rv);
				rv += labels.getDiffY();
				eight_neighbourhood[1] = labels.getLabel(rv);
				rv -= labels.getDiffX();
				eight_neighbourhood[2] = labels.getLabel(rv);
				rv -= labels.getDiffX();
				eight_neighbourhood[3] = labels.getLabel(rv);
				rv -= labels.getDiffY();
				eight_neighbourhood[4] = labels.getLabel(rv);
				rv -= labels.getDiffY();
				eight_neighbourhood[5] = labels.getLabel(rv);
				rv += labels.getDiffX();
				eight_neighbourhood[6] = labels.getLabel(rv);
 				rv += labels.getDiffX();
				eight_neighbourhood[7] = labels.getLabel(rv);
				
				eight_neighbourhood.sortAscending();

				//detection of neighbour segments
				vector<unsigned int>  segmentid;
				int lastSeg = 0;
				for (int i=0; i<8; i++)
				{
					if (eight_neighbourhood[i] > lastSeg)
					{
						lastSeg = eight_neighbourhood[i];
						segmentid.push_back(eight_neighbourhood[i]);
					}
				}
				
				//calculation of adjacency and border matrix
				unsigned int segment_size = segmentid.size();	
				if (segment_size > 1)
				{
					unsigned long help;
					for (unsigned int i=0; i< segment_size - 1 ; i++) //border=index of adjacency matrix
					{
						for (unsigned int j=i+1; j< segment_size; j++)	
						{
							help = (*this).LabelsToIndex(segmentid[i],segmentid[j]);
							(*this)[help]++;
						}
					}
					if (segment_size == 2) boundary[u] = help;	//only two neighbours -> border=index of adjacency matrix
					else if (segment_size == 3) boundary[u] = -1;						//if a pixel part of more than two borders -> (--) -> number of negative number = number of neighbour segements
					else if (segment_size == 4) boundary[u] = -2;
				}
			}
		}
	}

};
