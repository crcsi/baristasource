#include <math.h>
#include <strstream>
using namespace std;

#include "FeatExtractPars.h"
#include "Filename.h"
#include "statistics.h"


FeatureExtractionParameters::FeatureExtractionParameters(): f_cor ((float)0.2),
        f_cir((float)0.8), Mode(eFoerstner), NoiseModel(ePoisson), 
		distNonmaxEdges(1), distNonmaxPts(5),  
		wFactor((float)1.5f), qMin((float)0.75f), 
		DerivativeFilterX(eDiffXGauss,3,3,0.5f), DerivativeFilterY(eDiffYGauss,3,3,0.5f),
		IntegrationFilterPoints(eGauss,5,5,1.0f), IntegrationFilterEdges(eGauss,5,5,0.7f),
		lineThinningThreshold((float) 0.5f), alpha((float)0.1f), extractPoints(true), extractEdges(true),
		minLineLength(3), extractRegions(true),MorphologicFilterRegions(eMorphological,3,3, 1),
		WatershedSmoothingIterations(1), RegionSimilarityThreshold(1.0f), RegionVarianceRatioThreshold(4.0f),
		RegionTextureThreshold(0.9f), kForWatershed(2.0f), ATKIS(false), ATKIS_threshold(3000)
{
};

FeatureExtractionParameters::FeatureExtractionParameters(const FeatureExtractionParameters &pars): 
	   HomogeneousThreshold(pars.HomogeneousThreshold), f_cor(pars.f_cor),
	   f_cir(pars.f_cir), DerivativeFilterX(pars.DerivativeFilterX), 
	   NoiseModel(pars.NoiseModel),
	   DerivativeFilterY(pars.DerivativeFilterY), IntegrationFilterEdges(pars.IntegrationFilterEdges), 
	   IntegrationFilterPoints(pars.IntegrationFilterPoints), Mode (pars.Mode), 
	   distNonmaxEdges(pars.distNonmaxEdges), distNonmaxPts(pars.distNonmaxPts), 
	   qMin(pars.qMin), wFactor(pars.wFactor), lineThinningThreshold(pars.lineThinningThreshold),
	   alpha(pars.alpha), extractPoints(pars.extractPoints), extractEdges(pars.extractEdges),
	   minLineLength(pars.minLineLength), extractRegions(pars.extractRegions),
	   MorphologicFilterRegions(pars.MorphologicFilterRegions), 
	   WatershedSmoothingIterations(pars.WatershedSmoothingIterations), 
	   RegionSimilarityThreshold(pars.RegionSimilarityThreshold), 
	   RegionVarianceRatioThreshold(pars.RegionVarianceRatioThreshold),
	   RegionTextureThreshold(pars.RegionTextureThreshold),
	   kForWatershed(pars.kForWatershed),
	   ATKIS(pars.ATKIS), 
	   ATKIS_threshold(pars.ATKIS_threshold)
{
}

void FeatureExtractionParameters::setWatershedSmoothingIterations(int it)
{
	if (it >= 0) this->WatershedSmoothingIterations = it;
};	 

void FeatureExtractionParameters::writeParameters(const CFileName &fileName) const
{
	ofstream outFile(fileName.GetChar());
	if (outFile.good())
	{
		outFile.setf(ios::fixed, ios::floatfield);
		outFile.precision(3);

		outFile << "NoiseModel(" << this->NoiseModel 
				<< ")\nDerivativeX(" << this->DerivativeFilterX.getType() 
				<< " " << this->DerivativeFilterX.getSizeX() << " " 
				<< this->DerivativeFilterX.getSizeY() << " " << this->DerivativeFilterX.getParameter()
				<< " )\nDerivativeY(" << this->DerivativeFilterY.getType() << " " 
				<< this->DerivativeFilterY.getSizeX() << " " << this->DerivativeFilterY.getSizeY() << " " 
				<< this->DerivativeFilterY.getParameter()
				<< " )\nIntegrationFilterEdges(" << this->IntegrationFilterEdges.getType() 
				<< " " << this->IntegrationFilterEdges.getSizeX() << " " 
				<< this->IntegrationFilterEdges.getSizeY() << " " << this->IntegrationFilterEdges.getParameter()
				<< " )\nIntegrationFilterPoints(" << this->IntegrationFilterPoints.getType() << " " 
				<< this->IntegrationFilterPoints.getSizeX() << " " 
				<< this->IntegrationFilterPoints.getSizeY() << " " << this->IntegrationFilterPoints.getParameter()
				<< " )\nMode(" << this->Mode << ")\nExtractPoints(" << this->extractPoints << ")\nExtractEdges(" << this->extractEdges
				<< ")\nExtractRegions(" << this->extractRegions << ")\nAlpha(" << this->alpha
				<< ")\nCOR(" << this->f_cor << ")\nCIR(" << this->f_cir 
				<< ")\nHomogeneousThreshold(" << this->HomogeneousThreshold 
				<< ")\nDistNonmaxEdges(" << this->distNonmaxEdges << ")\nDistNonmaxPts(" << this->distNonmaxPts
				<< ")\nQMin(" << this->qMin << ")\nWFactor(" << this->wFactor
				<< ")\nLineThinningThreshold(" << this->lineThinningThreshold 
				<< ")\nMinLineLength(" << this->minLineLength 
				<< ")\nMorphologicFilterRegions(" << this->MorphologicFilterRegions.getType() << " "
				<< this->MorphologicFilterRegions.getSizeX() << " " 
				<< this->MorphologicFilterRegions.getSizeY() << " " 
				<< this->MorphologicFilterRegions.getParameter() 
				<< ")\nWaterShedIt(" << this->WatershedSmoothingIterations 
				<< ")\nRegionSimilarityThreshold(" << this->RegionSimilarityThreshold
				<< ")\nRegionVarianceRatioThreshold(" << this->RegionVarianceRatioThreshold
				<< ")\nRegionTextureThreshold(" << this->RegionTextureThreshold
				<< ")\nkForWatershed(" << this->kForWatershed
				<< ")\nATKIS(" << this->ATKIS		
				<< ")\nATKIS_threshold(" << this->ATKIS_threshold
				<< ")\n";
	}
};

bool FeatureExtractionParameters::initParameters(const CFileName &fileName)
{
	ifstream inFile(fileName.GetChar());
	int len = 2048;
	char *z = new char[len];
	bool ret = true;
	if (inFile.good())
	{
		inFile.getline(z, len);

		while (inFile.good())
		{
			CCharString arg(z);
			int pos1 = arg.Find("(");
			int pos2 = arg.Find(")");
			if ((pos1 > 0) && (pos2 > pos1))
			{
				CCharString key = arg.Left(pos1);
				arg = arg.Left(pos2);
				arg = arg.Right(arg.GetLength() - pos1 - 1);
				if (key == "NoiseModel")
				{
					istrstream iss(arg.GetChar());
					int typ;

					iss >> typ;
					this->NoiseModel = (eNoiseModel) typ;
				}
				else if (key == "DerivativeX")
				{
					istrstream iss(arg.GetChar());
					eFilterType typ;
					int typi, sizex,sizey;
					float par;
					iss >> typi;
					typ = (eFilterType) typi;
					iss >> sizex;
					iss >> sizey;
					iss >> par;

					this->DerivativeFilterX.set(typ, sizex, sizey, par);
				}
				else if (key == "DerivativeY")
				{
					istrstream iss(arg.GetChar());
					eFilterType typ;
					int typi, sizex,sizey;
					float par;
					iss >> typi;
					typ = (eFilterType) typi;
					iss >> sizex;
					iss >> sizey;
					iss >> par;

					this->DerivativeFilterY.set(typ, sizex, sizey, par);
				}
				else if (key == "IntegrationFilterEdges")
				{
					istrstream iss(arg.GetChar());
					eFilterType typ;
					int typi, sizex,sizey;
					float par;
					iss >> typi;
					typ = (eFilterType) typi;
					iss >> sizex;
					iss >> sizey;
					iss >> par;

					this->IntegrationFilterEdges.set(typ, sizex, sizey, par);
				}
				else if (key == "IntegrationFilterPoints")
				{
					istrstream iss(arg.GetChar());
					eFilterType typ;
					int typi, sizex,sizey;
					float par;
					iss >> typi;
					typ = (eFilterType) typi;
					iss >> sizex;
					iss >> sizey;
					iss >> par;

					this->IntegrationFilterPoints.set(typ, sizex, sizey, par);
				}
				else if (key == "Mode") 
				{
					istrstream iss(arg.GetChar());
					int m;
					iss >> m;
					this->Mode = (eExtractionMode) m;
				}
				else if (key == "ExtractPoints")
				{
					istrstream iss(arg.GetChar());
					iss >> this->extractPoints;
				}
				else if (key == "ExtractEdges")
				{
					istrstream iss(arg.GetChar());
					iss >> this->extractEdges;
				}
				else if (key == "ExtractRegions")
				{
					istrstream iss(arg.GetChar());
					iss >> this->extractRegions;
				}
				else if (key == "Alpha")
				{
					istrstream iss(arg.GetChar());
					iss >> this->alpha;
				}
				else if (key == "COR")
				{
					istrstream iss(arg.GetChar());
					iss >> this->f_cor;
				}
				else if (key == "CIR")
				{
					istrstream iss(arg.GetChar());
					iss >> this->f_cir;
				}
				else if (key == "HomogeneousThreshold")
				{
					istrstream iss(arg.GetChar());
					iss >> this->HomogeneousThreshold;
				}
				else if (key == "DistNonmaxEdges")
				{
					istrstream iss(arg.GetChar());
					iss >> this->distNonmaxEdges;
				}
				else if (key == "DistNonmaxPts")
				{
					istrstream iss(arg.GetChar());
					iss >> this->distNonmaxPts;
				}
				else if (key == "QMin")
				{
					istrstream iss(arg.GetChar());
					iss >> this->qMin;
				}
				else if (key == "WFactor")
				{
					istrstream iss(arg.GetChar());
					iss >> this->wFactor;
				}
				else if (key == "LineThinningThreshold")
				{
					istrstream iss(arg.GetChar());
					iss >> this->lineThinningThreshold;
				}
				else if (key == "MinLineLength")
				{
					istrstream iss(arg.GetChar());
					iss >> this->minLineLength;
				}
				else if (key == "MorphologicFilterRegions")
				{
					istrstream iss(arg.GetChar());
					eFilterType typ;
					int typi, sizex,sizey;
					float par;
					iss >> typi;
					typ = (eFilterType) typi;
					iss >> sizex;
					iss >> sizey;
					iss >> par;

					this->MorphologicFilterRegions.set(typ, sizex, sizey, par);
				}
				else if (key == "WaterShedIt")
				{
					istrstream iss(arg.GetChar());
					iss >> this->WatershedSmoothingIterations;
				}
				else if (key == "RegionSimilarityThreshold")
				{
					istrstream iss(arg.GetChar());
					iss >> this->RegionSimilarityThreshold;
				}
				else if (key == "RegionVarianceRatioThreshold")
				{
					istrstream iss(arg.GetChar());
					iss >> this->RegionVarianceRatioThreshold;
				}
				else if (key == "RegionTextureThreshold")
				{
					istrstream iss(arg.GetChar());
					iss >> this->RegionTextureThreshold;
				}
				else if (key == "kForWatershed")
				{
					istrstream iss(arg.GetChar());
					iss >> this->kForWatershed;
				}
			}
			inFile.getline(z, len);
		}
	}
	else
		ret = false;
	delete [] z;

	return ret;
};

void FeatureExtractionParameters::setMorphologicFilterSize(int size)
{
	MorphologicFilterRegions.set(eMorphological, size, size, 1);	
};

float FeatureExtractionParameters::getSigmaSmooth() const
{
	eFilterType ft = DerivativeFilterX.getType();
	if ((ft != eDiffXGauss) && (ft != eDiffXDeriche)) return -1;
	return DerivativeFilterX.getParameter();
};

float FeatureExtractionParameters::getPointScale() const
{	
	if ((!extractEdges) && (!extractPoints)) return 1;
	if (!extractPoints) return getLineScale();

	if (IntegrationFilterPoints.getType() != eGauss) return (float) IntegrationFilterPoints.getSizeX();
	return IntegrationFilterPoints.getParameter();
};

float FeatureExtractionParameters::getLineScale() const
{	
	if ((!extractEdges) && (!extractPoints)) return 1;
	if (!extractEdges) return getPointScale();

	if (IntegrationFilterEdges.getType() != eGauss) return (float) IntegrationFilterEdges.getSizeX();
	return IntegrationFilterEdges.getParameter();
};

int FeatureExtractionParameters::getTotalOffset() const
{
	int offset;

	if (this->Mode != eWaterShed)
	{
		offset = (IntegrationFilterPoints.getOffsetX() > IntegrationFilterEdges.getOffsetX())?
	
			IntegrationFilterPoints.getOffsetX() : IntegrationFilterEdges.getOffsetX();
		offset += DerivativeFilterX.getOffsetX();
	}
	else
	{
		offset = DerivativeFilterX.getOffsetX() + this->WatershedSmoothingIterations;
	}

	return offset;
};
	   
float FeatureExtractionParameters::getTotalVarianceFactorEdges() const
{
	if ((!extractEdges) && (!extractPoints)) return 1;
	if (!extractEdges) return getTotalVarianceFactorPoints();

	return DerivativeFilterX.getVarianceFactor() * IntegrationFilterEdges.getVarianceFactor();
};

float FeatureExtractionParameters::getTotalVarianceFactorPoints() const
{
	if ((!extractEdges) && (!extractPoints)) return 1;
	if (!extractPoints) return getTotalVarianceFactorEdges();

	return DerivativeFilterX.getVarianceFactor() * IntegrationFilterPoints.getVarianceFactor();
};
		
bool FeatureExtractionParameters::setDerivativeFilter (eFilterType typ, float sigma)
{
	bool ret = true;

	if ((typ == eDiffX) || (typ == eDiffY))
	{
		DerivativeFilterX.set(eDiffX, 3, 1, 0.0f);
		DerivativeFilterY.set(eDiffY, 1, 3, 0.0f);
	}
	else if ((typ == eSobelX) || (typ == eSobelY))
	{
		DerivativeFilterX.set(eSobelX, 3, 3, 0.0f);
		DerivativeFilterY.set(eSobelY, 3, 3, 0.0f);
	}
	else if ((typ == eDiffXGauss) || (typ == eDiffYGauss))
	{
		DerivativeFilterX.set(eDiffXGauss, 3, 3, sigma);
		DerivativeFilterY.set(eDiffYGauss, 3, 3, sigma);
	}
	else if ((typ == eDiffXDeriche) || (typ == eDiffYDeriche))
	{
		DerivativeFilterX.set(eDiffXDeriche, 3, 3, sigma);
		DerivativeFilterY.set(eDiffYDeriche, 3, 3, sigma);
	}
	else ret = false;

	return ret;
};

bool FeatureExtractionParameters::setIntegrationFilter(eFilterType typ, int sizeEdges, int sizePoints, 
													   float sigmaEdges, float sigmaPoints)
{
	bool ret = true;

	if ((typ == eAverage) || (typ == eGauss) || (typ == eBinom))
	{
		IntegrationFilterEdges.set(typ, sizeEdges, sizeEdges, sigmaEdges);
		IntegrationFilterPoints.set(typ, sizePoints, sizePoints, sigmaPoints);
	}
	else ret = false;
		
	return ret;
};
	
void FeatureExtractionParameters::setAlpha(float I_alpha)
{
	alpha = I_alpha;
};

void FeatureExtractionParameters::setKforWatershed(float k)
{
	if (k >= 0.0) this->kForWatershed = k;
};

void  FeatureExtractionParameters::setATKIS(bool atkis_par)
{
	this->ATKIS = atkis_par;
};

void FeatureExtractionParameters::setATKIS_threshold (unsigned long ATKIS_threshold_par)
{
	this->ATKIS_threshold = ATKIS_threshold_par;
};
	
void FeatureExtractionParameters::setHomogeneousThreshold(float percentil, int bands)
{
	if (this->NoiseModel == eMedian)
		this->HomogeneousThreshold = percentil;
	else if (this->Mode == eFoerstner)
	{
		this->HomogeneousThreshold = (float) statistics::chiSquareFractil(1.0 - alpha, 
			2 * bands * this->IntegrationFilterEdges.getSizeX() *this->IntegrationFilterEdges.getSizeY());
	}
	else
	{
		this->HomogeneousThreshold = (float) sqrt(statistics::chiSquareFractil(1.0 - alpha, 2 * bands));
	};
};

