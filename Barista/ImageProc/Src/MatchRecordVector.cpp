#include <math.h>
#include <float.h>
#include <strstream>
#include "newmat.h"
#include "MatchRecordVector.h"
#include "ImgBuf.h"
#include "Trans2D.h"
#include "AdjustNormEqu.h"

CMatchRecordVector::CMatchRecordVector(const CXYPointArray &ptArray, const CImageBuffer &buf, 
									   int width, int height) : vector<CMatchRecord *>(ptArray.GetSize()),
									   cog(0,0), NHypotheses(0)
{
	for (unsigned int i = 0; i < this->size(); ++i)
	{
		CObsPoint *p = ptArray.GetAt(i);
		(*this)[i] = new CMatchRecord(*p, buf, width, height);
		this->cog.x += (*p)[0];
		this->cog.y += (*p)[1];
	}

	if (this->size() > 0) this->cog /= (double) this->size();
};

CMatchRecordVector::~CMatchRecordVector() 
{ 
	deleteAll();
};

void CMatchRecordVector::deleteAll()
{
	for (unsigned int i = 0; i < this->size(); ++i)
	{
		if ((*this)[i]) delete (*this)[i];
		(*this)[i] = 0;
	}

	this->cog.x = this->cog.y = 0.0;
	this->NHypotheses = 0;
	this->clear();
};

int CMatchRecordVector::getIndexByY(float Y) const
{
	if (this->nRecords() < 1) return -1;

	int upperIdx = this->nRecords() - 1;
	int lowerIdx = 0;

	if ((*((vector<CMatchRecord *> *)this))[upperIdx]->getPos().y < Y) return -1;
	if ((*((vector<CMatchRecord *> *)this))[lowerIdx]->getPos().y > Y) return lowerIdx;

	do
	{
		int idx = (upperIdx + lowerIdx) / 2;
		float y = (float) (*((vector<CMatchRecord *> *)this))[idx]->getPos().y;
		if (y < Y) lowerIdx = idx;
		else upperIdx = idx;

	} while ((upperIdx > lowerIdx) && (abs(upperIdx - lowerIdx) > 2));

	return (lowerIdx < upperIdx) ? lowerIdx : upperIdx;
};

bool CMatchRecordVector::computeTrafo(CTrans2D *trafo, Matrix &trafoCovar, 
									  const CMatchRecordVector &other, 
									  double &sigma0)
{
	CNormalEquationMatrix N(trafo->nParameters());
	CtVector m(trafo->nParameters());

	double vtpv; unsigned long Nobs;
	trafo->prepareAdjustment(N, m, vtpv, Nobs);
	trafo->setP0(this->cog);

	int nH = 0;

	for (unsigned int i = 0; i < this->size(); ++i)
	{
		C2DPoint p1 = (*this)[i]->getPos();
		for (int j = 0; j < (*this)[i]->nHypotheses(); j++)
		{
			trafo->accumulate(p1, (*this)[i]->getHypothesis(j)->getPos(), N, m, vtpv, Nobs);
			++nH;
		}
	}

	if (!trafo->solve(N, m, vtpv, Nobs, sigma0)) return false;
	else
	{	
		trafoCovar = *(trafo->getCovar());
		sigma0 = 0.0f;
	
		for (unsigned int i = 0; i < this->size(); ++i)
		{
			C2DPoint p1 = (*this)[i]->getPos();
			C2DPoint pTraf;
			trafo->transform(pTraf, p1);

			double qvv = trafo->getQvv(p1, trafoCovar);

			for (int j = 0; j < (*this)[i]->nHypotheses(); j++)
			{
				(*this)[i]->residual(j) = pTraf  - (*this)[i]->getHypothesis(j)->getPos();
				(*this)[i]->sigmaRes(j) = (float) qvv;
				sigma0 += (*this)[i]->residual(j).getSquareNorm();
			}
		};

		sigma0 /= (double) trafo->getRed(nH);

		
		trafoCovar *= sigma0;

		sigma0 = sqrt(sigma0);

		for (unsigned int i = 0; i < this->size(); ++i)
		{
			for (int j = 0; j < (*this)[i]->nHypotheses(); j++)
			{
				(*this)[i]->sigmaRes(j) *= (float) sigma0;
				(*this)[i]->getHypothesis(j)->setResidual((*this)[i]->residual(j), (*this)[i]->sigmaRes(j), (*this)[i]);
			}
		};
	};

	return true;
};


int CMatchRecordVector::removeLargeResiduals(float threshold)
{
	int nDeleted = 0;

	for (unsigned int i = 0; i < this->size(); ++i)
	{
		for (int j = 0; j < (*this)[i]->nHypotheses(); j++)
		{
			float sigmaVThresh = float((*this)[i]->sigmaRes(j)) * threshold;
			if (((fabs((*this)[i]->residual(j).x) > sigmaVThresh) || (fabs((*this)[i]->residual(j).y) > sigmaVThresh)) &&
				((fabs((*this)[i]->residual(j).x) > 0.5f) || (fabs((*this)[i]->residual(j).y) > 0.5f)))
			{
				(*this)[i]->deleteHypothesis(j);
				j--;
				++nDeleted;
			}
		}
	}

	return nDeleted;
};

bool CMatchRecordVector::computeRobustTrafo(CTrans2D *trafo, Matrix &trafoCovar, 
											CMatchRecordVector &other, float threshold,
											double &sigma0)
{
	bool ret;
	ret = this->computeTrafo(trafo, trafoCovar, other, sigma0);

	if (ret)
	{
		int nDeleted = 0;

		int maxIt = 30;
		int it = 0;

		do 
		{
			nDeleted = removeLargeResiduals(threshold);

			if (nDeleted) 
			{
				this->deleteNoMatches();
				other.deleteNoMatches();
				ret = this->computeTrafo(trafo, trafoCovar, other, sigma0);
			}

			it++;
		} while ((nDeleted) && (it < maxIt) && ret);

		this->clarifyMultipleMatches();
		other.deleteNoMatches();

		ret = this->computeTrafo(trafo, trafoCovar, other, sigma0);
	}

	return ret;
}

void CMatchRecordVector::deleteNoMatches()
{
	int nValidRecords = 0;
	this->NHypotheses = 0;
	this->cog.x = 0;
	this->cog.y = 0;

	for (unsigned int i = 0; i < this->size(); ++i)
	{
		if (!(*this)[i]->nHypotheses())
		{
			delete (*this)[i];
			(*this)[i] = 0;
		}
		else 
		{
			this->cog += double((*this)[i]->nHypotheses()) * (*this)[i]->getPos();

			this->NHypotheses += (*this)[i]->nHypotheses();
			(*this)[nValidRecords] = (*this)[i];
			nValidRecords++;
		}
	}
	
	this->cog /= double(this->NHypotheses);

	this->resize(nValidRecords);
};

void CMatchRecordVector::clarifyMultipleMatches()
{
	for (unsigned int i = 0; i < this->size(); ++i)
	{
		(*this)[i]->clarifyHypotheses();
	}
	this->deleteNoMatches();
};

void CMatchRecordVector::appendVectors(CMatchRecordVector &appendVec, 
									   CMatchRecordVector &otherVec, 
									   CMatchRecordVector &otherAppendVec)
{
	unsigned int oldLength = this->size();
	int nCOG1 = (!this->NHypotheses) ? this->size() : this->NHypotheses;
	int nCOG2 = (!appendVec.NHypotheses) ? appendVec.size() : appendVec.NHypotheses;

	this->cog *= nCOG1;
	appendVec.cog *= nCOG2;

	this->resize(this->size() + appendVec.size());
	
	for (unsigned int i = oldLength; i < this->size(); ++i)
	{
		(*this)[i] = appendVec[i - oldLength];
		appendVec[i - oldLength] = 0;
	}

	this->NHypotheses += appendVec.NHypotheses;
	this->cog += appendVec.cog;
	this->cog /= (nCOG1 + nCOG2);
	
	appendVec.resize(0);
	appendVec.cog.x = 0;
	appendVec.cog.y = 0;
	appendVec.NHypotheses = 0;

	oldLength = otherVec.size();
	nCOG1 = (!otherVec.NHypotheses) ? otherVec.size() : otherVec.NHypotheses;
	nCOG2 = (!otherAppendVec.NHypotheses) ? otherAppendVec.size() : otherAppendVec.NHypotheses;

	otherVec.cog *= nCOG1;
	otherAppendVec.cog *= nCOG2;

	otherVec.resize(otherVec.size() + otherAppendVec.size());
	
	for (unsigned int i = oldLength; i < otherVec.size(); ++i)
	{
		otherVec[i] = otherAppendVec[i - oldLength];
		otherAppendVec[i - oldLength] = 0;
	}

	otherVec.NHypotheses += otherAppendVec.NHypotheses;
	otherVec.cog += otherAppendVec.cog;
	otherVec.cog /= (nCOG1 + nCOG2);
	
	otherAppendVec.resize(0);
	otherAppendVec.cog.x = 0;
	otherAppendVec.cog.y = 0;
	otherAppendVec.NHypotheses = 0;
};
