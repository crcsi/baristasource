#include <math.h>
#include <limits.h>
#include <float.h>
#include <strstream>
using namespace std;

#include "ImgBuf.h"
#include "XYPoint.h"
#include "histogram.h"
#include "LookupTable.h"
#include "tiffio.h"
#include "Trans2D.h"
#include "XYContourArray.h"
#include "XYContour.h"
#include "XYPolyLine.h"
#include "ProtocolHandler.h"


//===========================================================================================

CImageBuffer::CImageBuffer(): offsetX(0), offsetY(0), width(0), 
                              height(0), bands(0), bpp(0), pixF(NULL),
							  diffX (1), diffY(1), diffBnd(1), bufferLength (0),
							  nGV(0), unsignedFlag(false)

{
}

//===========================================================================================

CImageBuffer::CImageBuffer (const CImageBuffer &buf, bool copyValues): offsetX(buf.offsetX), 
                                offsetY(buf.offsetY), width(buf.width), 
								height(buf.height), bands(buf.bands), 
								bpp(buf.bpp), diffX (buf.diffX), 
								diffY(buf.diffY), diffBnd(buf.diffBnd), 
								bufferLength (buf.bufferLength), nGV(buf.nGV),
								pixF(NULL), unsignedFlag(buf.unsignedFlag)
{
	if (allocateBuffer() && copyValues) memcpy(pixUC, buf.pixUC, bufferLength);
};

//===========================================================================================

CImageBuffer::CImageBuffer (long I_offsetX, long I_offsetY, long I_width, 
							long I_height, int I_bands, int I_bpp, 
							bool I_unsignedFlag, float value): 
                            offsetX(I_offsetX), offsetY(I_offsetY), width(I_width), 
                            height(I_height), bands(I_bands), bpp(I_bpp), 
							bufferLength (0), pixF(NULL), diffBnd(1),
							unsignedFlag(I_unsignedFlag)
{
	if (I_bands == 3) diffX = I_bands + 1;
	else diffX = I_bands;

	diffY = width * diffX;
	nGV = width * height * diffX;
	allocateBuffer(value);
};

//===========================================================================================

CImageBuffer::CImageBuffer (long I_offsetX, long I_offset_Y, long I_width, long I_height, 
							int I_bands, int I_bpp, bool I_unsignedFlag): 
                            offsetX(I_offsetX), offsetY(I_offset_Y), width(I_width), 
                            height(I_height), bands(I_bands), bpp(I_bpp), diffBnd(1),
							bufferLength (0), pixF(NULL), 
							unsignedFlag(I_unsignedFlag)
{
	if (I_bands == 3) diffX = I_bands + 1;
	else diffX = I_bands;

	diffY = width * diffX;
	nGV = width * height * diffX;
	allocateBuffer();
}

//===========================================================================================

CImageBuffer::~CImageBuffer()
{
	deleteBuffer();	
}

//===========================================================================================

void CImageBuffer::getRowColBand(long index, long &row, long &col, long &band) const
{
	band = index % this->diffX;
    index /= this->diffX;
    col = index % width;
	row = index / width;
};

//===========================================================================================

void CImageBuffer::deleteBuffer()
{
	if (pixF) delete [] pixF;
	pixF = NULL;
}

//===========================================================================================

bool CImageBuffer::allocateBuffer(float value)
{
	bool retVal = allocateBuffer();
	if (retVal) setAll(value);

	return retVal;
}

//===========================================================================================

bool CImageBuffer::allocateBuffer()
{
	bool retVal = true;

	deleteBuffer();

	if (bands == 3)
	{
		nGV = width * height * (bands + 1);
	}
	else nGV = width * height * bands;

	if ((this->bpp == 1) || (this->bpp == 8))
	{
		unsignedFlag = true;
		this->bufferLength = nGV;
		this->pixUC = new unsigned char [nGV];
		if (!this->pixUC) retVal = false;
	}
	else if (this->bpp == 16)
	{
		this->bufferLength = nGV * 2;
		if (unsignedFlag)
			this->pixUS = new unsigned short[nGV];
		else
			this->pixS = new short[nGV];

		if (!this->pixUS) retVal = false;
	}
	else if (this->bpp == 32)
	{
		unsignedFlag = false;
		this->bufferLength = nGV * 4;
		this->pixF = new float[nGV];
		if (!this->pixF) retVal = false;
	}
	else
	{
		retVal = false;
	};

	if (!retVal)
	{
		this->bufferLength = this->nGV = 0;
	};

	return retVal;
};
	
//===========================================================================================

vector<int> CImageBuffer::get4NeigbourIndexDiffs() const
{
	vector<int> dIndex(4);
	dIndex[0] =  this->diffX;
	dIndex[1] =  this->diffY;
	dIndex[2] = -this->diffX;
	dIndex[3] = -this->diffY;

	return dIndex;
}; 

//===========================================================================================

vector<int> CImageBuffer::get8NeigbourIndexDiffs() const
{
	vector<int> dIndex(8);
	dIndex[0] =  this->diffX;
	dIndex[1] =  dIndex[0] + this->diffY;
	dIndex[2] =  this->diffY;
	dIndex[3] =  dIndex[2] - this->diffX;
	dIndex[4] = -this->diffX;
	dIndex[5] = -dIndex[1];
	dIndex[6] = -this->diffY;
	dIndex[7] = -dIndex[3];

	return dIndex;
}; 

//===========================================================================================

float CImageBuffer::getPercentil(float percent, int margin) const
{
	CImageBuffer bufcpy; 
	extractSubWindow(offsetX + margin, offsetY + margin, width - 2 * margin, height - 2 * margin, bufcpy);
	float *buf = bufcpy.getPixF();
	unsigned long redSize = bufcpy.nGV;

	sort(redSize, buf - 1);
	
    return buf[(unsigned long)(percent * float(redSize))];
};

//===========================================================================================

float *CImageBuffer::getPixF() const
{
	if (this->bpp == 32) return pixF;

	return 0;
};

//===========================================================================================
	
unsigned char *CImageBuffer::getPixUC() const
{
	if ((this->bpp == 8) || (this->bpp == 1)) return pixUC;

	return 0;
};

//===========================================================================================

short *CImageBuffer::getPixS() const
{
	if ((this->bpp == 16) && (!unsignedFlag)) return pixS;

	return 0;
};

//===========================================================================================

unsigned short *CImageBuffer::getPixUS() const
{
	if ((this->bpp == 16) && (unsignedFlag)) return pixUS;

	return 0;
};

//===========================================================================================

void CImageBuffer::getColourVec(unsigned long index, float *colourVector) const
{
	if ((this->bpp == 1) || (this->bpp == 8))
	{
		for (int i = 0; i < bands; ++i)
		{
			colourVector[i] = this->pixUC[index + i];
		}
	}
	else if  (this->bpp == 16)
	{
		if (this->unsignedFlag)
		{
			for (int i = 0; i < bands; ++i)
			{
				colourVector[i] = this->pixUS[index + i];
			}
		}
		else
		{
			for (int i = 0; i < bands; ++i)
			{
				colourVector[i] = this->pixS[index + i];
			}
		}
	}
	else if (this->bpp == 32)
	{
		for (int i = 0; i < bands; ++i)
		{
			colourVector[i] = this->pixF[index + i];
		}
	}
};

//===========================================================================================

bool CImageBuffer::set(long I_offsetX, long I_offsetY, long I_width, long I_height, 
					   int I_bands, int I_bpp, bool I_unsignedFlag)
{
	offsetX      = I_offsetX;        offsetY      = I_offsetY;
	width        = I_width;          height       = I_height;
	bands        = I_bands;          bpp          = I_bpp;
    unsignedFlag = I_unsignedFlag;   bufferLength = 0;

	if (bands == 3) diffX = I_bands + 1;
	else diffX = I_bands;
	
	diffY   = diffX * I_width;
	diffBnd = 1;                	
	nGV          = I_width * I_height * diffX; 

	return allocateBuffer();
};

//===========================================================================================

bool CImageBuffer::set(long I_offsetX, long I_offsetY, long I_width, long I_height, 
					   int I_bands, int I_bpp, bool I_unsignedFlag, float value)
{
	offsetX      = I_offsetX;        offsetY      = I_offsetY;
	width        = I_width;          height       = I_height;
	bands        = I_bands;          bpp          = I_bpp;

	unsignedFlag = I_unsignedFlag;   bufferLength = 0;
	
	if (bands == 3) diffX = I_bands + 1;
	else diffX = I_bands;
	
	diffY   = diffX * I_width;
	diffBnd = 1;                	
	nGV     = I_width * I_height * diffX; 

	return allocateBuffer(value);
};

//===========================================================================================

bool CImageBuffer::set(const CImageBuffer &buffer)
{
	bool ret = set(buffer.offsetX, buffer.offsetY, buffer.width, buffer.height, 
		          buffer.bands, buffer.bpp, buffer.unsignedFlag);
	if (ret) memcpy(pixUC, buffer.pixUC, bufferLength);
	
	return ret;
};

//===========================================================================================

bool CImageBuffer::setOffset(long I_offsetX, long I_offsetY)
{
	offsetX      = I_offsetX;        
	offsetY      = I_offsetY;

	return true;
};

//===========================================================================================

void CImageBuffer::setAll(float value)
{
	if (this->nGV)
	{
		if ((this->bpp == 1) || (this->bpp == 8))
		{
			unsigned char valUC = (unsigned char) value;
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				pixUC [u] = valUC;
			}
		}
		else if (this->bpp == 16)
	    {
			if (unsignedFlag)
			{
				unsigned short valUS = (unsigned short) value;
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					pixUS [u] = valUS;
				}
			}
			else
			{
				 short valS = (short) value;
				for (unsigned long u = 0; u < this->nGV; ++u)
				{	
					pixS [u] = valS;
				}
			}
		}	
		else if (this->bpp == 32)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				pixF [u] = value;
			}
		}
	};
};

//===========================================================================================

void CImageBuffer::setMargin(int marginWidth, float value)
{
	if (this->nGV)
	{
		if ((this->bpp == 1) || (this->bpp == 8))
		{
			unsigned char valUC = (unsigned char) value;

			long marginLengthX = marginWidth * this->diffX;

			for (int band = 0; band < bands; ++band)
			{
				// upper margin
				unsigned long uMin = band;
				unsigned long uMax = this->getIndex(marginWidth, 0) + band;
				unsigned long uMaxInRow;
				for (; uMin < uMax; uMin += this->diffY)
				{
					uMaxInRow = uMin + this->diffY;
					for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
					{
						pixUC [u] = valUC;
					}
				};

				// left margin
				uMin = this->getIndex(marginWidth, 0) + band;
				uMax = this->getIndex(height - marginWidth, 0) + band;
				for (; uMin < uMax; uMin += this->diffY)
				{
					uMaxInRow = uMin + marginLengthX;
					for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
					{
						pixUC [u] = valUC;
					}
				};

				// right margin
				uMin = this->getIndex(marginWidth, width - marginWidth) + band;
				uMax = this->getIndex(height - marginWidth, width) + band;
				for (; uMin < uMax; uMin += this->diffY)
				{
					uMaxInRow = uMin + marginLengthX;
					for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
					{
						pixUC [u] = valUC;
					}
				}
				
				// bottom margin
				uMin = this->getIndex(height - marginWidth, 0) + band;
				uMax = this->getIndex(height, 0) + band;
				for (; uMin < uMax; uMin += this->diffY)
				{
					uMaxInRow = uMin + this->diffY;
					for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
					{
						pixUC [u] = valUC;
					}
				}
			}
		}
		else if (this->bpp == 16)
	    {
			if (unsignedFlag)
			{
				unsigned short valUS = (unsigned short) value;

				for (int band = 0; band < bands; ++band)
				{
					// upper margin
					unsigned long uMin = band;
					unsigned long uMax = this->getIndex(marginWidth, 0) + band;
					unsigned long uMaxInRow;
					for (; uMin < uMax; uMin += this->diffY)
					{
						uMaxInRow = uMin + this->diffY;
						for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
						{
							pixUS [u] = valUS;
						}
					};

 					// left margin
					uMin = this->getIndex(marginWidth, 0) + band;
					uMax = this->getIndex(height - marginWidth, 0) + band;
					for (; uMin < uMax; uMin += this->diffY)
					{
						uMaxInRow = uMin + marginWidth;
						for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
						{
							pixUS [u] = valUS;
						}
					};

					// right margin
					uMin = this->getIndex(marginWidth, width - marginWidth) + band;
					uMax = this->getIndex(height - marginWidth, width) + band;
					for (; uMin < uMax; uMin += this->diffY)
					{
						uMaxInRow = uMin + marginWidth;
						for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
						{
							pixUS [u] = valUS;
						}
					}
				
					// bottom margin
					uMin = this->getIndex(height - marginWidth, 0) + band;
					uMax = this->getIndex(height, 0) + band;
					for (; uMin < uMax; uMin += this->diffY)
					{
						uMaxInRow = uMin + this->diffY;
						for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
						{
							pixUS [u] = valUS;
						}
					}			
				}
			}
			else
			{
				short valS = (short) value;

				for (int band = 0; band < bands; ++band)
				{
					// upper margin
					unsigned long uMin = band;
					unsigned long uMax = this->getIndex(marginWidth, 0) + band;
					unsigned long uMaxInRow;
					for (; uMin < uMax; uMin += this->diffY)
					{
						uMaxInRow = uMin + this->diffY;
						for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
						{
							pixS [u] = valS;
						}
					};

 					// left margin
					uMin = this->getIndex(marginWidth, 0) + band;
					uMax = this->getIndex(height - marginWidth, 0) + band;
					for (; uMin < uMax; uMin += this->diffY)
					{
						uMaxInRow = uMin + marginWidth;
						for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
						{
							pixS [u] = valS;
						}
					};

					// right margin
					uMin = this->getIndex(marginWidth, width - marginWidth) + band;
					uMax = this->getIndex(height - marginWidth, width) + band;
					for (; uMin < uMax; uMin += this->diffY)
					{
						uMaxInRow = uMin + marginWidth;
						for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
						{
							pixS [u] = valS;
						}
					}
				
					// bottom margin
					uMin = this->getIndex(height - marginWidth, 0) + band;
					uMax = this->getIndex(height, 0) + band;
					for (; uMin < uMax; uMin += this->diffY)
					{
						uMaxInRow = uMin + this->diffY;
						for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
						{
							pixS [u] = valS;
						}
					}			
				}
			}
		}	
		else if (this->bpp == 32)
		{
			for (int band = 0; band < bands; ++band)
			{
				// upper margin
				unsigned long uMin = band;
				unsigned long uMax = this->getIndex(marginWidth, 0) + band;
				unsigned long uMaxInRow;
				for (; uMin < uMax; uMin += this->diffY)
				{
					uMaxInRow = uMin + this->diffY;
					for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
					{
						pixF [u] = value;
					}
				};

				// left margin
				uMin = this->getIndex(marginWidth, 0) + band;
				uMax = this->getIndex(height - marginWidth, 0) + band;
				for (; uMin < uMax; uMin += this->diffY)
				{
					uMaxInRow = uMin + marginWidth;
					for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
					{
						pixF [u] = value;
					}
				};

				// right margin
				uMin = this->getIndex(marginWidth, width - marginWidth) + band;
				uMax = this->getIndex(height - marginWidth, width) + band;
				for (; uMin < uMax; uMin += this->diffY)
				{
					uMaxInRow = uMin + marginWidth;
					for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
					{
						pixF [u] = value;
					}
				}
				
				// bottom margin
				uMin = this->getIndex(height - marginWidth, 0) + band;
				uMax = this->getIndex(height, 0) + band;
				for (; uMin < uMax; uMin += this->diffY)
				{
					uMaxInRow = uMin + this->diffY;
					for (unsigned long u = uMin; u < uMaxInRow; u += this->diffX)
					{
						pixF [u] = value;
					}
				}
			}
		}
	};
};

//===========================================================================================

bool CImageBuffer::convertToBinary(float value)
{
	bool ret = true;
	if (this->bands > 1) this->SetToIntensity();

	if (this->bpp == 8)
	{
		unsigned char *oldbuffer = this->pixUC;
		this->pixUC = 0;
		this->bpp = 1;
		ret = this->allocateBuffer();
		if (ret)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				if (oldbuffer[u] >= value) this->pixUC[u] = 1;
				else this->pixUC[u] = 0;
			}		
		}
		delete [] oldbuffer;
	}
	else if (this->bpp == 16)
	{
		if (this->unsignedFlag)
		{
			unsigned short *oldbuffer = this->pixUS;
			this->pixUS = 0;
			this->bpp = 1;
			ret = this->allocateBuffer();
			if (ret)
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					if (oldbuffer[u] >= value) this->pixUC[u] = 1;
					else this->pixUC[u] = 0;
				}
			}
			delete [] oldbuffer;
		}
		else
		{
			short *oldbuffer = this->pixS;
			this->pixUS = 0;
			this->bpp = 1;
			ret = this->allocateBuffer();
			if (ret)
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					if (oldbuffer[u] >= value) this->pixUC[u] = 1;
					else this->pixUC[u] = 0;
				}
			}
			delete [] oldbuffer;
		}
	}
	else if (this->bpp == 32)
	{
		float *oldbuffer = this->pixF;
		this->pixF = 0;
		this->bpp = 8;
		ret = this->allocateBuffer();
		if (ret)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				if (oldbuffer[u] >= value) this->pixUC[u] = 1;	
				else this->pixUC[u] = 0;
			}		
		}
		delete [] oldbuffer;
	};
	
	unsignedFlag = true;

	return ret;
};

//===========================================================================================

bool CImageBuffer::convertToUnsignedChar()
{
	bool ret = true;
	if (this->bpp == 16)
	{
		if (this->unsignedFlag)
		{
			unsigned short *oldbuffer = this->pixUS;
			this->pixUS = 0;
			this->bpp = 8;
			ret = this->allocateBuffer();
			if (ret)
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					this->pixUC[u] = (unsigned char) oldbuffer[u];
				}
			}
			delete [] oldbuffer;
		}
		else
		{
			short *oldbuffer = this->pixS;
			this->pixUS = 0;
			this->bpp = 8;
			ret = this->allocateBuffer();
			if (ret)
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					this->pixUC[u] = (unsigned char) oldbuffer[u];
				}
			}
			delete [] oldbuffer;
		}
	}
	else if (this->bpp == 32)
	{
		float *oldbuffer = this->pixF;
		this->pixF = 0;
		this->bpp = 8;
		ret = this->allocateBuffer();
		if (ret)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				this->pixUC[u] = (unsigned char) oldbuffer[u];
			}		
		}
		delete [] oldbuffer;
	}
	else if (this->bpp == 1) this->bpp = 8;
	
	unsignedFlag = true;

	return ret;
};

//===========================================================================================

bool CImageBuffer::convertToUnsignedChar(CLookupTable& lookupTable,unsigned char* outputBuffer)
{
	bool ret = true;
	bool useExternalBuffer = true;

	if (outputBuffer == NULL) 
		useExternalBuffer = false;

	if (this->bpp == 16)
	{
		if (this->unsignedFlag)
		{
			unsigned short *oldbuffer = this->pixUS;
			
			// change the datatype of the object
			if (!useExternalBuffer)
			{
				this->pixUS = 0;
				this->bpp = 8;
				ret = this->allocateBuffer();
				outputBuffer = this->pixUC;
			}
			else // use outputBuffer instead
				ret = true;


			if (ret)
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					outputBuffer[u] =  lookupTable[oldbuffer[u]];
				}
			}
			
			if (!useExternalBuffer)
				delete [] oldbuffer;
		}
		else
		{
		/*	short *oldbuffer = this->pixS;
			this->pixUS = 0;
			this->bpp = 8;
			ret = this->allocateBuffer();
			if (ret)
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					this->pixUC[u] = (unsigned char) oldbuffer[u];
				}
			}
			delete [] oldbuffer;
			*/
			return false;
		}
	}
	else if (this->bpp == 32)
	{
	/*	float *oldbuffer = this->pixF;
		this->pixF = 0;
		this->bpp = 8;
		ret = this->allocateBuffer();
		if (ret)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				this->pixUC[u] = (unsigned char) oldbuffer[u];
			}		
		}
		delete [] oldbuffer;
		*/
	}
	else if (this->bpp == 1) this->bpp = 8;
	
	unsignedFlag = true;

	return ret;
};

//===========================================================================================

bool CImageBuffer::convertToUnsignedShort()
{
	bool ret = true;
	if ((this->bpp == 1) || (this->bpp == 8))
	{
		unsigned char *oldbuffer = this->pixUC;
		this->pixUC = 0;
		this->bpp = 16;
		ret = this->allocateBuffer();
		unsignedFlag = true;
		if (ret)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				this->pixUS[u] = (unsigned short) oldbuffer[u];
			}
		}
		delete [] oldbuffer;
	}
	else if (this->bpp == 32)
	{
		float *oldbuffer = this->pixF;
		this->pixF = 0;
		this->bpp = 16;
		ret = this->allocateBuffer();
		if (ret)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				this->pixUS[u] = (unsigned short) oldbuffer[u];
			}
		}
		delete [] oldbuffer;
	}
	else if (!unsignedFlag)
	{
		short *oldbuffer = this->pixS;
		for (unsigned long u = 0; u < this->nGV; ++u)
		{
			this->pixUS[u] = (unsigned short) oldbuffer[u];
		}
		delete [] oldbuffer;
	};
	unsignedFlag = true;

	return ret;
};

//===========================================================================================

bool CImageBuffer::convertToShort()
{
	bool ret = true;
	if ((this->bpp == 1) || (this->bpp == 8))
	{
		unsigned char *oldbuffer = this->pixUC;
		this->pixUC = 0;
		this->bpp = 16;
		ret = this->allocateBuffer();
		unsignedFlag = true;
		if (ret)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				this->pixS[u] = (short) oldbuffer[u];
			}
		};
		delete [] oldbuffer;
	}
	else if (this->bpp == 32)
	{
		float *oldbuffer = this->pixF;
		this->pixF = 0;
		this->bpp = 16;
		ret = this->allocateBuffer();
		if (ret)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				this->pixS[u] = (short) oldbuffer[u];
			}
		}
		delete [] oldbuffer;
	}
	else if (unsignedFlag)
	{
		unsigned short *oldbuffer = this->pixUS;
		for (unsigned long u = 0; u < this->nGV; ++u)
		{
			this->pixS[u] = (short) oldbuffer[u];
		}
		delete [] oldbuffer;
	};
	unsignedFlag = false;

	return ret;
};

//===========================================================================================

bool CImageBuffer::convertToFloat()
{
	bool ret = true;

	if ((this->bpp == 1) || (this->bpp == 8))
	{
		unsigned char *oldbuffer = this->pixUC;
		this->pixUC = 0;
		this->bpp = 32;
		ret = this->allocateBuffer();
		if (ret)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				this->pixF[u] = (float) oldbuffer[u];
			}
		}
		delete [] oldbuffer;
	}
	else if (this->bpp == 16)
	{
		if (this->unsignedFlag)
		{
			unsigned short *oldbuffer = this->pixUS;
			this->pixUS = 0;
			this->bpp = 32;
			ret = this->allocateBuffer();
			if (ret)
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					this->pixF[u] = (float) oldbuffer[u];
				}
			}
			delete [] oldbuffer;
		}
		else
		{
			short *oldbuffer = this->pixS;
			this->pixS = 0;
			this->bpp = 32;
			ret = this->allocateBuffer();
			if (ret)
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					this->pixF[u] = (float)  oldbuffer[u];
				}
			}
			delete [] oldbuffer;
		}
	};

	unsignedFlag = false;
	return ret;
};

//===========================================================================================

bool CImageBuffer::extractSubWindow(long subOffsetX, long subOffsetY, 
	 							    long subWidth, long subHeight, 
								    CImageBuffer &buffer) const
{
	bool ret = true;

	if ((subOffsetX < offsetX) || (subOffsetY < offsetY) || 
		(subOffsetX + subWidth  > offsetX + width) || 
		(subOffsetY + subHeight > offsetY + height) || (subWidth < 1) || (subHeight < 1))
		ret = false;
	else
	{
		ret = buffer.set(subOffsetX, subOffsetY, subWidth, subHeight, this->bands, this->bpp, this->unsignedFlag);
		if (ret)
		{
			subOffsetX -= offsetX;
			subOffsetY -= offsetY;

			unsigned long uMin = this->getIndex(subOffsetY, subOffsetX);
			unsigned long uMax = this->getIndex(subOffsetY + subHeight - 1, subOffsetX + subWidth);
			unsigned long diff = subWidth * this->diffX;

			if ((this->bpp == 1) || (this->bpp == 8))
			{
				for (int bnd = 0; bnd < this->bands; bnd++)
				{
					unsigned long uMinNew = bnd;
					for (unsigned long uMinBnd = uMin + bnd; uMinBnd < uMax; uMinBnd += this->diffY, uMinNew += buffer.diffY)
					{
						unsigned long uMaxRow = uMinBnd + diff;
						unsigned long uNew = uMinNew;
						for (unsigned long u = uMinBnd; u < uMaxRow; u += this->diffX, uNew += buffer.diffX)
						{
							buffer.pixUC[uNew] = this->pixUC[u];
						};
					}
				};
			}
			else if (this->bpp == 16)	
			{
				if (this->unsignedFlag)
				{
					for (int bnd = 0; bnd < bands; bnd++)
					{
						unsigned long uMinNew = bnd;
						for (unsigned long uMinBnd = uMin + bnd; uMinBnd < uMax; uMinBnd += this->diffY, uMinNew += buffer.diffY)
						{
							unsigned long uMaxRow = uMinBnd + diff;
							unsigned long uNew = uMinNew;
							for (unsigned long u = uMinBnd; u < uMaxRow; u += this->diffX, uNew += buffer.diffX)
							{
								buffer.pixUS[uNew] = this->pixUS[u];
							}
						}
					}
				}
				else
				{
					for (int bnd = 0; bnd < bands; bnd++)
					{
						unsigned long uMinNew = bnd;
						for (unsigned long uMinBnd = uMin + bnd; uMinBnd < uMax; uMinBnd += this->diffY, uMinNew += buffer.diffY)
						{
							unsigned long uMaxRow = uMinBnd + diff;
							unsigned long uNew = uMinNew;
							for (unsigned long u = uMinBnd; u < uMaxRow; u += this->diffX, uNew += buffer.diffX)
							{
								buffer.pixS[uNew] = this->pixS[u];
							}
						}
					}
				}
			}
			else if (this->bpp == 32)
			{
				for (int bnd = 0; bnd < bands; bnd++)
				{
					unsigned long uMinNew = bnd;
					for (unsigned long uMinBnd = uMin + bnd; uMinBnd < uMax; uMinBnd += this->diffY, uMinNew += buffer.diffY)
					{
						unsigned long uMaxRow = uMinBnd + diff;
						unsigned long uNew = uMinBnd;
						for (unsigned long u = uMinBnd; u < uMaxRow; u += this->diffX, uNew += buffer.diffX)
						{
							buffer.pixF[uNew] = this->pixF[u];
						};
					}
				};			}
			else ret = false;
		};
	};
	return ret;
};

//===========================================================================================

bool CImageBuffer::extractBand(int band, CImageBuffer &buffer)
{
	bool ret = true;

	if (band > bands) ret = false;
	else
	{
		if ((buffer.bands != 1) || (buffer.height != height) ||  (buffer.width != width) || 
			(buffer.offsetX != offsetX) || (buffer.offsetY != offsetY) ||
			(buffer.bpp != bpp))
			ret = buffer.set(offsetX, offsetY, width, height, 1, this->bpp, this->unsignedFlag);

		if (ret)
		{
			unsigned long uMax = buffer.getIndex(height - 1, width);
			unsigned long uMinOld = band;

			if ((this->bpp == 1) || (this->bpp == 8))
			{							
				for (unsigned long uMin = 0; uMin < uMax; uMin += buffer.diffY, uMinOld += this->diffY)
				{
					unsigned long uMaxRow = uMin + buffer.diffY;					
					unsigned long uOld = uMinOld;
					for (unsigned long u = uMin; u < uMaxRow; u += buffer.diffX, uOld += this->diffX)
					{
							buffer.pixUC[u] = this->pixUC[uOld];
					};
				}
			}
			else if (this->bpp == 16)	
			{
				if (this->unsignedFlag)
				{
					for (unsigned long uMin = 0; uMin < uMax; uMin += buffer.diffY, uMinOld += this->diffY)
					{
						unsigned long uMaxRow = uMin + buffer.diffY;					
						unsigned long uOld = uMinOld;
						for (unsigned long u = uMin; u < uMaxRow; u += buffer.diffX, uOld += this->diffX)
						{
							buffer.pixUS[u] = this->pixUS[uOld];
						}
					}
				}
				else
				{
					for (unsigned long uMin = 0; uMin < uMax; uMin += buffer.diffY, uMinOld += this->diffY)
					{
						unsigned long uMaxRow = uMin + buffer.diffY;					
						unsigned long uOld = uMinOld;
						for (unsigned long u = uMin; u < uMaxRow; u += buffer.diffX, uOld += this->diffX)
						{
							buffer.pixS[u] = this->pixS[uOld];
						}
					}
				}
			}
			else if (this->bpp == 32)
			{
				for (unsigned long uMin = 0; uMin < uMax; uMin += buffer.diffY, uMinOld += this->diffY)
				{
					unsigned long uMaxRow = uMin + buffer.diffY;					
					unsigned long uOld = uMinOld;
					for (unsigned long u = uMin; u < uMaxRow; u += buffer.diffX, uOld += this->diffX)
					{
							buffer.pixF[u] = this->pixF[uOld];
					}
				}
			}
			else ret = false;
		};
	};
	return ret;
};

//===========================================================================================

float CImageBuffer::getIntensity (const float &R, const float &G, const float &B)
{
	return (R + G + B) / 3.0f;
};	

//===========================================================================================

void CImageBuffer::RGBToIntensity()
{
	if ((this->bands == 3) || (this->bands == 4))
	{
		int redBand   = 0;
		int greenBand = 1;
		int blueBand  = 2;
		CImageBuffer oldBuffer(*this);
		bands = 1;

		unsigned long uNew = 0;
		unsigned long uMin = 0;
		unsigned long uMax = this->getIndex(height - 1, width);

		if (this->bpp == 8)
		{
			allocateBuffer();
			for (; uMin < uMax; uMin += this->diffY)
			{
				unsigned long uMaxRow = uMin + this->diffY;
				for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ++uNew)
				{
					float R = (float) oldBuffer.pixUChar(u + redBand);
					float G = (float) oldBuffer.pixUChar(u + greenBand);
					float B = (float) oldBuffer.pixUChar(u + blueBand);
					this->pixUC[uNew] = (unsigned char) getIntensity(R,G,B);
				}
			};
		}
		else if (this->bpp == 16)
		{
			if (this->unsignedFlag)
			{
				allocateBuffer();
				for (; uMin < uMax; uMin += this->diffY)
				{
					unsigned long uMaxRow = uMin + this->diffY;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ++uNew)
					{
						float R = (float) oldBuffer.pixUShort(u + redBand);
						float G = (float) oldBuffer.pixUShort(u + greenBand);
						float B = (float) oldBuffer.pixUShort(u + blueBand);
						this->pixUS[uNew] = (unsigned short) getIntensity(R,G,B);
					}
				};
			}
			else
			{
				short *oldbuffer = this->pixS;
				allocateBuffer();
				for (; uMin < uMax; uMin += this->diffY)
				{
					unsigned long uMaxRow = uMin + this->diffY;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ++uNew)
					{
						float R = (float) oldBuffer.pixShort(u + redBand);
						float G = (float) oldBuffer.pixShort(u + greenBand);
						float B = (float) oldBuffer.pixShort(u + blueBand);
						this->pixS[uNew] = (short) getIntensity(R,G,B);
					}
				};
				delete [] oldbuffer;
			}
		}
		else if (this->bpp == 32)
		{
			float *oldbuffer = this->pixF;
			allocateBuffer();
			for (; uMin < uMax; uMin += this->diffY)
			{
				unsigned long uMaxRow = uMin + this->diffY;
				for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ++uNew)
				{
					float R = oldBuffer.pixFlt(u + redBand);
					float G = oldBuffer.pixFlt(u + greenBand);
					float B = oldBuffer.pixFlt(u + blueBand);
					this->pixF[uNew] = getIntensity(R,G,B);
				}
			};
		}

		diffX = 1;
		diffY = width;
		diffBnd = 1;
	}
};

//===========================================================================================

void CImageBuffer::SetToIntensity()
{
	if (this->bands > 1)
	{
		CImageBuffer oldBuffer(*this);
		bands = 1;
        float bandFact = (float) 1.0 / (float) oldBuffer.bands; 
		unsigned long uNew = 0;
		unsigned long uMin = 0;
		unsigned long uMax = this->getIndex(height - 1, width);

		if (this->bpp == 8)
		{
			allocateBuffer();
			for (; uMin < uMax; uMin += this->diffY)
			{
				unsigned long uMaxRow = uMin + this->diffY;
				for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ++uNew)
				{
					long intensity = 0;
					for (int bnd = 0; bnd < oldBuffer.bands; ++bnd)
					{
						intensity += oldBuffer.pixUChar(u + bnd);
					}
					this->pixUC[uNew] = (unsigned char) (float(intensity) * bandFact);
				}
			};
		}
		else if (this->bpp == 16)
		{
			if (this->unsignedFlag)
			{
				allocateBuffer();
				for (; uMin < uMax; uMin += this->diffY)
				{
					unsigned long uMaxRow = uMin + this->diffY;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ++uNew)
					{
						unsigned long intensity = 0;
						for (int bnd = 0; bnd < oldBuffer.bands; ++bnd)
						{
							intensity += oldBuffer.pixUShort(u + bnd);
						}

						this->pixUS[uNew] = (unsigned short) (float(intensity) * bandFact);
					}
				};
			}
			else
			{
				short *oldbuffer = this->pixS;
				allocateBuffer();
				for (; uMin < uMax; uMin += this->diffY)
				{
					unsigned long uMaxRow = uMin + this->diffY;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ++uNew)
					{
						unsigned long intensity = 0;
						for (int bnd = 0; bnd < oldBuffer.bands; ++bnd)
						{
							intensity += oldBuffer.pixShort(u + bnd);
						}

						this->pixS[uNew] = (short) (float(intensity) * bandFact);
					}
				};
				delete [] oldbuffer;
			}
		}
		else if (this->bpp == 32)
		{
			float *oldbuffer = this->pixF;
			allocateBuffer();
			for (; uMin < uMax; uMin += this->diffY)
			{
				unsigned long uMaxRow = uMin + this->diffY;
				for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ++uNew)
				{
					float intensity = 0.0f;	
					for (int bnd = 0; bnd < oldBuffer.bands; ++bnd)
					{
						intensity += oldBuffer.pixFlt(u + bnd);
					}
					this->pixF[uNew] = intensity * bandFact;
				}
			};
		}

		diffX = 1;
		diffY = width;
		diffBnd = 1;
	}
};

//===========================================================================================

bool CImageBuffer::setUpNormEquCornerCentre(long   approxRow,    long   approxCol, 												
										    int    opWidth,      int    opHeight,
										    double &NxxCorner,   double &NxyCorner,
										    double &NyyCorner, 
										    double &NxxCentre,   double &NxyCentre,
										    double &NyyCentre, 
										    double &Atl0Corner,  double &Atl1Corner, 
										    double &Atl0Centre,  double &Atl1Centre, 
										    double &omegaCorner, double &omegaCentre,
										    long   &redundancy)
  {
	  bool ret = true;
   
	  NxxCorner  = NxyCorner  = NyyCorner  = NxxCentre  = NxyCentre  = NyyCentre = 0.0f;
	  Atl0Corner = Atl1Corner = Atl0Centre = Atl1Centre = omegaCorner = omegaCentre = 0.0f;

	  long rowMin = approxRow - opHeight / 2;
	  long rowMax = approxRow + opHeight / 2;
	  long colMin = approxCol - opWidth  / 2;
	  long colMax = approxCol + opWidth  / 2;

	  float gradientX,  gradientY;
	  float gradientXX, gradientYY, gradientXY;

	  if ((rowMin < 1)                || (colMin < 1) || 
		  (rowMax > this->height - 2) || (colMax > this->width - 2)) ret = false;
	  else
	  {
		  unsigned long uMin = this->getIndex(rowMin, colMin);
		  for (long row = rowMin; row <= rowMax; row++, uMin += this->diffY)	
		  {
			  unsigned long u = uMin;
			  for (long col = colMin; col <= colMax; col++, u += this->diffX)
			  {
				  if (this->bpp < 16)
				  {
					  gradientX = float(this->pixUC[u + this->diffX]) - float(this->pixUC[u - this->diffX]);
					  gradientY = float(this->pixUC[u + this->diffY]) - float(this->pixUC[u - this->diffY]);
				  }
				  else if (this->bpp < 32)
				  {
					  if (this->unsignedFlag)
					  {
						  gradientX = float(this->pixUS[u + this->diffX]) - float(this->pixUS[u - this->diffX]);
						  gradientY = float(this->pixUS[u + this->diffY]) - float(this->pixUS[u - this->diffY]);
					  }
					  else
					  {
						  gradientX = float(this->pixS[u + this->diffX]) - float(this->pixS[u - this->diffX]);
						  gradientY = float(this->pixS[u + this->diffY]) - float(this->pixS[u - this->diffY]);
					  }
				  }
				  else
				  {
					  gradientX = this->pixF[u + this->diffX] - this->pixF[u - this->diffX];
					  gradientY = this->pixF[u + this->diffY] - this->pixF[u - this->diffY];
				  };

				  if ((fabs(gradientX) > FLT_EPSILON) || (fabs(gradientY) > FLT_EPSILON))
				  {
					  gradientXX = gradientX * gradientX;
					  gradientYY = gradientY * gradientY;
					  gradientXY = gradientX * gradientY;

					  NxxCorner   += gradientXX;
					  NyyCorner   += gradientYY;
					  NxyCorner   += gradientXY;
					  Atl0Corner  += col * gradientXX + row * gradientXY;
					  Atl1Corner  += col * gradientXY + row * gradientYY;
					  Atl0Centre  += col * gradientYY - row * gradientXY;
					  Atl1Centre  += row * gradientXX - col * gradientXY;
					  omegaCorner += col * col * gradientXX + row * row * gradientYY +
						             2.0f * row * col * gradientXY;
					  omegaCentre += row * row * gradientXX + col * col * gradientYY -
						             2.0f * row * col * gradientXY;
					  ++redundancy;
				  }
			  }
		  }

   		  NxxCentre =  NyyCorner; 
		  NyyCentre =  NxxCorner;
		  NxyCentre = -NxyCorner;
		  redundancy -= 2;
		  if (!redundancy) ret = false;
	  };
	  
	  return ret;
  };

//===========================================================================================

 float CImageBuffer::solveNormEq(const double &Nxx,   const double &Nxy,  const double &Nyy, 
	                             const double &Atl0,  const double &Atl1, const double &omega, 
								 long   redundancy,
								 CXYPoint &result)
  {
   
	  float m0sq = -1.0f;

	  double det = Nxx * Nyy - Nxy * Nxy;

	  if (fabs(det) > FLT_EPSILON)

	  {
		  // inversion of normal matrix
		  double qxx =  Nyy / det;
		  double qxy = -Nxy / det;
		  double qyy =  Nxx / det;
				
		  // solution of normal equation
		  
		  double x = qxx * Atl0 + qxy * Atl1;
		  double y = qxy * Atl0 + qyy * Atl1;
		  /* vtpv */
		  m0sq = float (omega  - (x * Atl0 + y * Atl1));
		  if ((m0sq < 0) && (fabs(m0sq) < 0.000001f)) m0sq = 0.000001f;

		  m0sq /= redundancy;

		  Matrix *covar = result.getCovariance();

		  (*covar)(1,1) = m0sq * qxx;    (*covar)(1,2) = m0sq * qxy;
		  (*covar)(2,1) = (*covar)(1,2); (*covar)(2,2) = m0sq * qyy;
		  result.setX(x + offsetX);
		  result.setY(y + offsetY);
	  };

	  return m0sq;
  };

//===========================================================================================

float CImageBuffer::getCorner(C2DPoint approx, int opWidth, int opHeight, CXYPoint &corner,
							  long &redundancy)
{
	long approxCol = long(floor(approx.x + 0.5f));
	long approxRow = long(floor(approx.y + 0.5f));
	return getCorner(approxRow, approxCol, opWidth, opHeight, corner, redundancy);
};

//===========================================================================================

float CImageBuffer::getCentre(C2DPoint approx, int opWidth, int opHeight, CXYPoint &centre,
							  long &redundancy)
{
	long approxCol = long(floor(approx.x + 0.5f));
	long approxRow = long(floor(approx.y + 0.5f));
	return getCentre(approxRow, approxCol, opWidth, opHeight, centre, redundancy);
};

//===========================================================================================

void CImageBuffer::getCentreAndCorner(C2DPoint approx, int opWidth, int opHeight, 
									  CXYPoint &centre, CXYPoint &corner, 
									  float &m0sqCentre, float &m0sqCorner,
									  long &redundancy)
{
	long approxCol = long(floor(approx.x + 0.5f));
	long approxRow = long(floor(approx.y + 0.5f));
	getCentreAndCorner(approxRow, approxCol, opWidth, opHeight, centre, corner, m0sqCentre, m0sqCorner, redundancy);
};

//===========================================================================================

float CImageBuffer::getCorner(long approxRow, long approxCol, int opWidth, int opHeight, CXYPoint &corner,
							  long &redundancy)
{
	float  m0;	
	double NxxCorner,  NxyCorner,  NyyCorner,  NxxCentre,  NxyCentre,   NyyCentre;
	double Atl0Corner, Atl1Corner, Atl0Centre, Atl1Centre, omegaCorner, omegaCentre;									   
		  
	if (this->bands > 1)
	{
		CImageBuffer subWindow;
		extractSubWindow(offsetX + approxCol - opWidth  / 2 - 1, 
			             offsetY + approxRow - opHeight / 2 - 1, 
						 opWidth + 2, opHeight + 2, subWindow);

		subWindow.SetToIntensity();
		approxRow += offsetY - subWindow.offsetY;
		approxCol += offsetX - subWindow.offsetX;
		return subWindow.getCorner(approxRow , approxCol, opWidth, opHeight, corner, redundancy);
	}
	else if (setUpNormEquCornerCentre(approxRow,   approxCol,   opWidth,    opHeight,
									  NxxCorner,   NxyCorner,   NyyCorner, 
									  NxxCentre,   NxyCentre,   NyyCentre, 
									  Atl0Corner,  Atl1Corner,  Atl0Centre, Atl1Centre, 
									  omegaCorner, omegaCentre, redundancy))
	{

		m0 = solveNormEq(NxxCorner,  NxyCorner, NyyCorner, Atl0Corner, Atl1Corner, omegaCorner, 
						 redundancy, corner);
	}
	else m0 = -1.0f;

	return m0;
};

//===========================================================================================

float CImageBuffer::getCentre(long approxRow, long approxCol, int opWidth, int opHeight, CXYPoint &centre,
							  long &redundancy)
{
	float  m0;	
	double NxxCorner,  NxyCorner,  NyyCorner,  NxxCentre,  NxyCentre,   NyyCentre;
	double Atl0Corner, Atl1Corner, Atl0Centre, Atl1Centre, omegaCorner, omegaCentre;									   
	
	if (this->bands > 1)
	{
		CImageBuffer subWindow;
		extractSubWindow(offsetX + approxCol - opWidth  / 2 - 1, 
			             offsetY + approxRow - opHeight / 2 - 1, 
						 opWidth + 2, opHeight + 2, subWindow);

		subWindow.SetToIntensity();
		approxRow += offsetY - subWindow.offsetY;
		approxCol += offsetX - subWindow.offsetX;
		return subWindow.getCentre(approxRow , approxCol, opWidth, opHeight, centre, redundancy);
	}
	else if (setUpNormEquCornerCentre(approxRow,   approxCol,   opWidth,    opHeight,
		                         NxxCorner,   NxyCorner,   NyyCorner, 
								 NxxCentre,   NxyCentre,   NyyCentre, 
								 Atl0Corner,  Atl1Corner,  Atl0Centre, Atl1Centre, 
								 omegaCorner, omegaCentre, redundancy))
	{
		m0 = solveNormEq(NxxCentre,  NxyCentre, NyyCentre, Atl0Centre, Atl1Centre, omegaCentre, 		 
						 redundancy, centre);
	}
	else m0 = -1.0f;

	return m0;			
};

//===========================================================================================

void CImageBuffer::getCentreAndCorner(long approxRow, long approxCol, int opWidth, int opHeight, 
									  CXYPoint &centre, CXYPoint &corner, 
									  float &m0sqCentre, float &m0sqCorner, long &redundancy)
{
	double NxxCorner,  NxyCorner,  NyyCorner,  NxxCentre,  NxyCentre,   NyyCentre;
	double Atl0Corner, Atl1Corner, Atl0Centre, Atl1Centre, omegaCorner, omegaCentre;									   
	
	if (this->bands > 1)
	{
		CImageBuffer subWindow;
		extractSubWindow(offsetX + approxCol - opWidth  / 2 - 1, 
			             offsetY + approxRow - opHeight / 2 - 1, 
						 opWidth + 2, opHeight + 2, subWindow);

		subWindow.SetToIntensity();
		approxRow += offsetY - subWindow.offsetY;
		approxCol += offsetX - subWindow.offsetX;
		subWindow.getCentreAndCorner(approxRow, approxCol, opWidth, opHeight, 
									 centre, corner, m0sqCentre, m0sqCorner, redundancy);
	}
	else 
	{
		if (setUpNormEquCornerCentre(approxRow,   approxCol,   opWidth,    opHeight,
								     NxxCorner,   NxyCorner,   NyyCorner, 
									 NxxCentre,   NxyCentre,   NyyCentre, 
									 Atl0Corner,  Atl1Corner,  Atl0Centre, Atl1Centre, 
									 omegaCorner, omegaCentre, redundancy))
		{
			m0sqCorner = solveNormEq(NxxCorner,  NxyCorner, NyyCorner, Atl0Corner, Atl1Corner, omegaCorner, 
									 redundancy, corner);
			m0sqCentre = solveNormEq(NxxCentre,  NxyCentre, NyyCentre, Atl0Centre, Atl1Centre, omegaCentre, 		 
									 redundancy, centre);
		}
		else m0sqCentre = m0sqCorner = -1.0f;	
	};
};

//===========================================================================================

bool CImageBuffer::getMinMax(int marginWidth, vector<float> &min, vector<float> &max) const
{
	bool ret = true;
	if (this->bufferLength < 1) ret = false;
	{
		min.resize(this->bands);
		max.resize(this->bands);
		unsigned long redSize = width * height - 2 * marginWidth * (width + height - 2* marginWidth);

		for (int bnd = 0; bnd < this->bands; ++bnd)
		{
			min[bnd] = FLT_MAX;
			max[bnd] = -FLT_MAX;

			unsigned long uMax = getIndex(height - marginWidth - 1, width - marginWidth);
			unsigned long pxRow = (width - 2 * marginWidth) * this->diffX;
			unsigned long ucpy = 0;

			if ((this->bpp == 1) || (this->bpp == 8))
			{
				for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
				{
					unsigned long uMaxRow = uMin + pxRow;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ucpy++)
					{
						assert(ucpy < redSize);
						assert(u < this->nGV);

						float fc = float(this->pixUC[u]);
						if (fc < min[bnd]) min[bnd] = float(fc);
						if (fc > max[bnd]) max[bnd] = float(fc);
					};
				};
			}
			else if (this->bpp == 16)
			{
				if (this->unsignedFlag)
				{
					for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
					{
						unsigned long uMaxRow = uMin + pxRow;
						for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ucpy++)
						{
							assert(ucpy < redSize);
							assert(u < this->nGV);
							float fc = float(this->pixUS[u]);
							if (fc < min[bnd]) min[bnd] = float(fc);
							if (fc > max[bnd]) max[bnd] = float(fc);
						};
					};
				}
				else
				{
					for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
					{
						unsigned long uMaxRow = uMin + pxRow;
						for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ucpy++)
						{
							assert(ucpy < redSize);
							assert(u < this->nGV);
							float fc = float(this->pixS[u]);
							if (fc < min[bnd]) min[bnd] = float(fc);
							if (fc > max[bnd]) max[bnd] = float(fc);
						};
					};
				}
			}
			else if (this->bpp == 32)
			{
				for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
				{
					unsigned long uMaxRow = uMin + pxRow;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ucpy++)
					{
						assert(ucpy < redSize);
						assert(u < this->nGV);
						float fc = float(this->pixF[u]);
						if (fc < min[bnd]) min[bnd] = float(fc);
						if (fc > max[bnd]) max[bnd] = float(fc);
					};
				};
			}
			else ret = false;
		};
	}

	return ret;
}

//===========================================================================================

bool CImageBuffer::getStatistics(int marginWidth, vector<float> &min, vector<float> &max, vector<float> &mean, 
		                         vector<float> &median, vector<float> &sdev, bool Flag, float NoDataValue) const
{
	bool ret = true;
	if (this->bufferLength < 1) ret = false;
	{
		min.resize(this->bands);
		max.resize(this->bands);
		mean.resize(this->bands);
		median.resize(this->bands);
		sdev.resize(this->bands);
		unsigned long redSize = width * height - 2 * marginWidth * (width + height - 2* marginWidth);
		float *bufcpy = new float [redSize];
		unsigned long excluded = 0;
		for (int bnd = 0; bnd < this->bands; ++bnd)
		{
			double sum = 0.0f;
			double sumsq = 0.0f;
			min[bnd] = FLT_MAX;
			max[bnd] = -FLT_MAX;

			unsigned long uMax = getIndex(height - marginWidth - 1, width - marginWidth);
			unsigned long pxRow = (width - 2 * marginWidth) * this->diffX;
			unsigned long ucpy = 0;

			if ((this->bpp == 1) || (this->bpp == 8))
			{
				for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
				{
					unsigned long uMaxRow = uMin + pxRow;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ucpy++)
					{
						assert(ucpy < redSize);
						assert(u < this->nGV);

						float fc = float(this->pixUC[u]);
						bufcpy[ucpy] = fc;
						if (fc < min[bnd]) min[bnd] = float(fc);
						if (fc > max[bnd]) max[bnd] = float(fc);
						sum   += double(fc);	
						sumsq += double(fc * fc);
					};
				};
			}
			else if (this->bpp == 16)
			{
				if (this->unsignedFlag)
				{
					for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
					{
						unsigned long uMaxRow = uMin + pxRow;
						for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ucpy++)
						{
							assert(ucpy < redSize);
							assert(u < this->nGV);
							float fc = float(this->pixUS[u]);
							bufcpy[ucpy] = fc;
							if (fc < min[bnd]) min[bnd] = float(fc);
							if (fc > max[bnd]) max[bnd] = float(fc);
							sum   += double(fc);	
							sumsq += double(fc * fc);
						};
					};
				}
				else
				{
					for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
					{
						unsigned long uMaxRow = uMin + pxRow;
						for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ucpy++)
						{
							assert(ucpy < redSize);
							assert(u < this->nGV);
							float fc = float(this->pixS[u]);
							bufcpy[ucpy] = fc;
							if (fc < min[bnd]) min[bnd] = float(fc);
							if (fc > max[bnd]) max[bnd] = float(fc);
							sum   += double(fc);	
							sumsq += double(fc * fc);
						};
					};
				}
			}
			else if (this->bpp == 32)
			{
				for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
				{
					unsigned long uMaxRow = uMin + pxRow;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX, ucpy++)
					{
						assert(ucpy < redSize);
						assert(u < this->nGV);
						float fc = float(this->pixF[u]);
						if (!Flag || fc != NoDataValue)
						{
							bufcpy[ucpy] = fc;
							if (fc < min[bnd]) min[bnd] = float(fc);
							if (fc > max[bnd]) max[bnd] = float(fc);
							sum   += double(fc);	
							sumsq += double(fc * fc);
						}
						else
						{
							excluded++;
						}
					};
				};
			}
			else ret = false;

			redSize -= excluded;

			sdev[bnd] = (float) sqrt((sumsq - sum * sum / float(redSize)) / (float(redSize) - 1.0f));
			mean[bnd] = (float) sum / float(redSize);
			sort(redSize, bufcpy - 1);
			median[bnd] = bufcpy[redSize/2];
		};

		delete [] bufcpy;
	}

	return ret;
}

//===========================================================================================

bool CImageBuffer::getGaussianNoise(int marginWidth, float alpha1, float alpha2, 
									float factor, vector<float> &sigmaN)
{
	bool ret = true;
	if (this->bufferLength < 1) ret = false;
	{
		sigmaN.resize(this->bands);
		float factorsq = factor * factor * 0.5f;
		for (int bnd = 0; bnd < this->bands; ++bnd)
		{
			CHistogram histogram(16);
			long nLrgr = 0;
			sigmaN[bnd] = 0.0f;
			unsigned long uMax = getIndex(height - marginWidth - 2, width - marginWidth);
			unsigned long pxRow = (width - 2 * marginWidth - 1) * this->diffX;
			unsigned long diff11 = this->diffX + this->diffY;

			for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
			{
					unsigned long uMaxRow = uMin + pxRow;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX)
					{
						float f00, f10, f01, f11;
						if ((this->bpp == 1) || (this->bpp == 8))
						{
							f00 = float(this->pixUC[u]);
							f10 = float(this->pixUC[u + this->diffX]);
							f01 = float(this->pixUC[u + this->diffY]);
							f11 = float(this->pixUC[u + diff11]);
						}
						else if (this->bpp == 16)
						{
							if (this->unsignedFlag)
							{
								f00 = float(this->pixUS[u]);
								f10 = float(this->pixUS[u + this->diffX]);
								f01 = float(this->pixUS[u + this->diffY]);
								f11 = float(this->pixUS[u + diff11]);
							}
							else
							{
								f00 = float(this->pixS[u]);
								f10 = float(this->pixS[u + this->diffX]);
								f01 = float(this->pixS[u + this->diffY]);
								f11 = float(this->pixS[u + diff11]);
							}
						}
						else if (this->bpp == 32)
						{
							f00 = this->pixF[u];
							f10 = this->pixF[u + this->diffX];
							f01 = this->pixF[u + this->diffY];
							f11 = this->pixF[u + diff11];
						}

						float d1 = f11 - f00;
						float d2 = f10 - f01;
    
						int index = (int) floor((d1 * d1 + d2 * d2) * factorsq);
						if (index >= histogram.getSize()) ++nLrgr;
						else histogram.increment(index);
  					}  
			}

			unsigned long redSize = width * height - 2 * marginWidth * (width + height - 2* marginWidth);
  
			sigmaN[bnd] = this->getSigma(histogram, redSize, alpha1, alpha2) / factor;
		}
	}

	return ret;
}

//===========================================================================================

CHistogram *CImageBuffer::getHistogram(int band)
{	
	CHistogram *imgHisto = new CHistogram(this->bpp);

	unsigned long uMin = band;
	unsigned long uMax = getIndex(height - 1, width) + band;
	unsigned long uMaxRow = uMin + diffY;

	if ((this->bpp == 8)  || (this->bpp == 1))
	{
		for (; uMin < uMax; uMin += diffY, uMaxRow += diffY)
		{
			for (unsigned long u = uMin; u < uMaxRow; u += diffX)
			{
				imgHisto->increment(this->pixUC[u]);
			};
		}
	}		
	else if (this->bpp == 16)  	
	{			
		if (this->unsignedFlag)	
		{
			for (; uMin < uMax; uMin += diffY, uMaxRow += diffY)
			{
				for (unsigned long u = uMin; u < uMaxRow; u += diffX)
				{
					imgHisto->increment(this->pixUS[u]);
				};
			}
		}			
		else		
		{
			for (; uMin < uMax; uMin += diffY, uMaxRow += diffY)
			{
				for (unsigned long u = uMin; u < uMaxRow; u += diffX)
				{
					imgHisto->increment(this->pixS[u]);
				};
			};
		}
	}
	else if (this->bpp == 32)  	
	{
		for (; uMin < uMax; uMin += diffY, uMaxRow += diffY)
		{
			for (unsigned long u = uMin; u < uMaxRow; u += diffX)
			{
				imgHisto->increment((int) this->pixF[u]);
			};
		}
	}
	else
	{
		delete imgHisto; imgHisto = 0;
	}

	if (imgHisto)
		imgHisto->updateMinMax(0.02,0.98);

	return imgHisto;
};

//===========================================================================================

bool CImageBuffer::getPoissonNoise(int marginWidth, float alpha1, float alpha2, float factor, 
								   vector<float> &sigmaNa, vector<float> &sigmaNb, float &avgGV)
{
	bool ret = true;
	if (this->bufferLength < 1) ret = false;
	{
		vector<float> avgGvals(this->bands);

		sigmaNa.resize(this->bands);
		sigmaNb.resize(this->bands);
		float factorsq = factor * factor * 0.5f;
		for (int bnd = 0; bnd < this->bands; ++bnd)
		{
			CHistogram *imgHisto = getHistogram(bnd);
			int nClasses = 6;
			vector<CHistogram *> HistogramVector(nClasses);
			vector<float> thresholdVector(nClasses);
			vector<float> classCentreVector(nClasses);
			vector<unsigned long> classNumberVector(nClasses);
			double alpha = 1.0f / float(nClasses);

			for (int i = 0; i < nClasses; ++i)
			{
				HistogramVector[i] = new CHistogram(16);
				thresholdVector[i] = (float) imgHisto->percentile(double(i+1) * alpha);
				classNumberVector[i] = 0;
				if (i == 0) 
				{
					classCentreVector[i] = float(thresholdVector[i] + imgHisto->minimum()) * 0.5f;
				}
				else
				{
					classCentreVector[i] = float(thresholdVector[i] + thresholdVector[i - 1]) * 0.5f;
				}
			};

			avgGvals[bnd] = imgHisto->percentile(0.5);
			
			unsigned long uMax = getIndex(height - marginWidth - 2, width - marginWidth);
			unsigned long pxRow = (width - 2 * marginWidth - 1) * this->diffX;
			unsigned long diff11 = this->diffX + this->diffY;

			for (unsigned long uMin = getIndex(marginWidth, marginWidth) + bnd;uMin < uMax; uMin += this->diffY)
			{
					unsigned long uMaxRow = uMin + pxRow;
					for (unsigned u = uMin; u < uMaxRow; u += this->diffX)
					{
						float f00, f10, f01, f11;
						if ((this->bpp == 1) || (this->bpp == 8))
						{
							f00 = float(this->pixUC[u]);
							f10 = float(this->pixUC[u + this->diffX]);
							f01 = float(this->pixUC[u + this->diffY]);
							f11 = float(this->pixUC[u + diff11]);
						}
						else if (this->bpp == 16)
						{
							if (this->unsignedFlag)
							{
								f00 = float(this->pixUS[u]);
								f10 = float(this->pixUS[u + this->diffX]);
								f01 = float(this->pixUS[u + this->diffY]);
								f11 = float(this->pixUS[u + diff11]);
							}
							else
							{
								f00 = float(this->pixS[u]);
								f10 = float(this->pixS[u + this->diffX]);
								f01 = float(this->pixS[u + this->diffY]);
								f11 = float(this->pixS[u + diff11]);
							}
						}
						else if (this->bpp == 32)
						{
							f00 = this->pixF[u];
							f10 = this->pixF[u + this->diffX];
							f01 = this->pixF[u + this->diffY];
							f11 = this->pixF[u + diff11];
						}

    
						float greyVal = f00;//0.25f * (f00 + f10 + f01 + f11);

						for (int i = 0; i < nClasses; ++i)
						{
							if (greyVal < thresholdVector[i]) 
							{
								float d1 = f11 - f00;
								float d2 = f10 - f01;
								HistogramVector[i]->increment((int) floor((d1 * d1 + d2 * d2) * factorsq));
								++classNumberVector[i];
								i = nClasses;
							};

						}
					}
			}

			float nxx = 0.0f, nxy = 0.0f, atpl0 = 0.0f, atpl1 = 0.0f;

			for (int i = 0; i < nClasses; ++i)
			{
				float sigma = this->getSigma(*(HistogramVector[i]), classNumberVector[i], alpha1, alpha2) / factor;
				sigma *= sigma;

				nxx += classCentreVector[i] * classCentreVector[i];
				nxy += classCentreVector[i];
				atpl0 += classCentreVector[i] * sigma;
				atpl1 += sigma;
			};
   
			float nyy = (float) nClasses;
			float det = nxx * nyy - nxy * nxy;


			sigmaNb[bnd] = ( nyy * atpl0 - nxy * atpl1) / det;
			sigmaNa[bnd] = (-nxy * atpl0 + nxx * atpl1) / det;

			// Make sure the variance is always larger than 0!
			float maxG = 255.0;
			if (this->bpp > 8)
			{
				if (this->bpp == 16)
				{
					if (this->unsignedFlag) maxG = 65535.0f;
					else maxG = 32767.0f;
				}
				else maxG = imgHisto->getMax();
			}
			float maxVal = sigmaNb[bnd] * maxG + sigmaNa[bnd];

			if ((sigmaNa[bnd] <= 0.0f)  && (maxVal <= 0.0f)) 
			{
				sigmaNa[bnd] = 1.0f;
				sigmaNb[bnd] = 0.0f;
			} 
			else if (sigmaNa[bnd] <= 0.0f)
			{
				sigmaNa[bnd] = 0.01f;
				sigmaNb[bnd] = (atpl0 - 0.01 * nxy) / nxx;
				maxVal = sigmaNb[bnd] * maxG + sigmaNa[bnd];
				if (maxVal <= 0.0f)
				{
					sigmaNa[bnd] = 1.0f;
					sigmaNb[bnd] = 0.0f;
				}
			} 
			else if (maxVal <= 0.0f)
			{
				nxx = nxx - 2.0 * maxG * nxy + float(nClasses) * maxG * maxG;
				atpl0 = atpl0 - maxG * atpl1 - 0.01f * nxy + float(nClasses) * 0.01f * maxG;
				sigmaNb[bnd] = atpl0 / nxx;
				sigmaNa[bnd] = -maxG * sigmaNb[bnd] + 0.01f;
			}

			
			delete imgHisto;

			for (int i = 0; i < nClasses; ++i)
			{
				delete HistogramVector[i];
				HistogramVector[i] = 0;
			};
		}

		avgGV = avgGvals[0];
		for (unsigned int i = 1; i < this->bands; ++i)
		{
			if (avgGvals[i] > avgGV) avgGV = avgGvals[i];
		}
	}

	return ret;
}

//===========================================================================================

void CImageBuffer::sort(long n, float *ra)
{
	int l,j,ir,i; 
	float rra;
	l=(n >> 1)+1;
	ir=n;
	for (;;) 
	{
		if (l > 1) rra=ra[--l];
		else 
		{    
			rra=ra[ir];
			ra[ir]=ra[1];
			if (--ir == 1) 		
			{
				ra[1]=rra;
				return;
			}
		}

		i=l;
		j=l << 1;
		while (j <= ir) 
		{
			if (j < ir && ra[j] < ra[j+1]) ++j;
			if (rra < ra[j]) 
			{
				ra[i]=ra[j];
				j += (i=j);
			}
			else j=ir+1;
		}
		ra[i]=rra;
	}
}

//===========================================================================================

float CImageBuffer::getSigma(const CHistogram &histogram, 
							 unsigned long numberOfPixels, 
							 float alpha1, float alpha2)
{		
	long alphaCount = long(float(numberOfPixels) * alpha1); 
	long count = 0;
	
	float sigma;
	float alphaPoint; 

	unsigned long i = 0;	
	for (; count < alphaCount; ++i)	 count += histogram.value(i);	

	alphaPoint = float(i) - (float(count - alphaCount) / float(histogram.value(i - 1)));

	float theoretical = float( - 2.0f * log(1.0f - alpha1));
	sigma = 2.0f * alphaPoint / theoretical;

	if (alpha2 > 0.0f)
	{
		if (sigma >= float(histogram.getSize())) sigma = float(histogram.getSize());

		count = 0;
		for (unsigned long u = 0; u < sigma; ++u) 
			count += histogram.value(u);
		alphaCount = (long) floor(float(count) * alpha2 + 0.5f);
		count = 0;

		unsigned long u;

		for (u = 0; count < alphaCount; ++u)	 count += histogram.value(u);		
		alphaPoint = float(u) - (float(count - alphaCount) / float(histogram.value(u - 1)));
		theoretical =  float(- 2.0f * log(1 - (1 - exp(float(-1))) * alpha2));
		sigma = 2.0f * alphaPoint / theoretical;		
	};

	return sqrt(sigma / 2.0f);
};

//===========================================================================================

bool CImageBuffer::exportToTiffFile(const char *filename, CLookupTable *lookup, bool stretch) const
{
	if (this->bpp != 8)
	{
		CImageBuffer exportBuffer(*this);
		if ((this->bpp == 16) && (this->unsignedFlag))
		{
			unsigned short min = USHRT_MAX, max = 0;
			CHistogram *histo = exportBuffer.getHistogram(0);
			min = histo->percentile(0.05);
			max = histo->percentile(0.95);
			delete histo;
			for (int i = 1; i < this->bands; i++)
			{
				histo = exportBuffer.getHistogram(i);
				unsigned short p = histo->percentile(0.05);
				if (p < min) min = p;
				p = histo->percentile(0.95);
				if (p > max) max = p;
			    delete histo;
			};

			if (max > 255)
			{
				float fact = float(255.0f) / float(max - min);
				for (unsigned long u = 0; u < nGV; ++u)
				{
					if (exportBuffer.pixUS[u] <= min) exportBuffer.pixUS[u] = 0;
					else if (exportBuffer.pixUS[u] >= max)exportBuffer.pixUS[u] = 255;
					else exportBuffer.pixUS[u] = (unsigned short )(float(exportBuffer.pixUS[u] - min) * fact);
				}
			}

			exportBuffer.convertToUnsignedChar();
		}
		else if (this->bpp == 1)
		{
			for (unsigned long u = 0; u < nGV; ++u)
			{
				if (exportBuffer.pixUC[u] > 0) exportBuffer.pixUC[u] = 255;
			}

			exportBuffer.convertToUnsignedChar();
		}
		else
		{
			exportBuffer.convertToUnsignedChar();
			if (stretch)
			{
				vector<float> mean, sdev, med, min, max, fact, diff;
			
				this->getStatistics(0, min, max, mean, med, sdev);
					
				for(int i = 0; i < bands; ++i)
				{
					diff.push_back(max[i] - min[i]);
					fact.push_back(255.0f / diff[i]);
				};

				float *colourVector = new float[bands];

				for (long row = 0, offset = 0; row < height; ++row, offset += diffY)
				{
					long maxCol = offset + diffY;
					for (long oldcol = offset; oldcol < maxCol; oldcol += diffX)
					{
						getColourVec(oldcol, colourVector);
						for (int bnd = 0; bnd < bands; ++bnd)
						{
							float delta = colourVector[bnd] - min[bnd];
							unsigned char v;
						
							if (delta <= 0) v = 0;
							else if (delta >= diff[bnd]) v = 255;
							else v = (unsigned char) (delta * fact[bnd]);
							exportBuffer.pixUChar(oldcol + bnd) = v;
						}
					};
				}

				delete [] colourVector;
				stretch = false;
			}
		}


		return exportBuffer.exportToTiffFile(filename, lookup, stretch);
	}
	else
	{
		TIFF * tif;
		tif = TIFFOpen(filename, "w");
		if (tif)
		{
			TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);
			TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, bands);
			TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, width);
			TIFFSetField(tif, TIFFTAG_IMAGELENGTH, height);
			TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
			TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, 1);

			if (bands == 1) 
			{
				if (!lookup) TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
				else
				{
					TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_PALETTE);	

					unsigned short *red   = new unsigned short [lookup->getMaxEntries()];
					unsigned short *green = new unsigned short [lookup->getMaxEntries()];
					unsigned short *blue  = new unsigned short [lookup->getMaxEntries()];

					short multiplication = (lookup->getBpp() == 8) ? 257 : 1;
       
					for (int rgb = 0; rgb < lookup->getMaxEntries(); rgb++)
					{
						int r,g,b;
						lookup->getColor(r,g,b,rgb);
						red  [rgb] = (unsigned short) r * multiplication;
						green[rgb] = (unsigned short) g * multiplication;
						blue [rgb] = (unsigned short) b * multiplication;
					}

					TIFFSetField(tif, TIFFTAG_COLORMAP, red, green, blue);

					delete [] red;
					delete [] green;
					delete [] blue;					
				}
			}
			else TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);

			unsigned char* stripBuffer = new unsigned char[TIFFStripSize(tif)];

			unsigned long offset = 0;

			for (long row = 0; row < height; ++row, offset += diffY)
			{
				if (bands != 3) memcpy(stripBuffer, this->pixUC + offset, diffY);
				else
				{
					unsigned char *old = this->pixUC + offset;
					for (long u = 0; u < width; ++u, ++old)
					{
						stripBuffer[u * 3    ] = *old; ++old;
						stripBuffer[u * 3 + 1] = *old; ++old;
						stripBuffer[u * 3 + 2] = *old; ++old;
					};
				};

				TIFFWriteEncodedStrip(tif, TIFFComputeStrip(tif, row, 0), stripBuffer, TIFFStripSize(tif));
			}
			delete [] stripBuffer;
			TIFFClose(tif);
			tif = NULL;
		}
		else return false;
	}

	return true;
}

//===========================================================================================

bool CImageBuffer::getInterpolatedColourVecBilin(double row, double col, unsigned char *colourVector) const
{
	int colIdx = (int) floor(col);
	int rowIdx = (int) floor(row);
	float dcol = float(col) - float(colIdx);
	float drow = float(row) - float(rowIdx);

	if ((rowIdx < 0) || (colIdx < 0) || (rowIdx + 1 >= this->getHeight()) || (colIdx + 1 >= this->getWidth())) 
	{
		for (int i = 0; i < this->bands; ++i)
		{
			colourVector[i] = 0;
		}
		return false;
	}

	long index = rowIdx * this->diffY + colIdx * this->diffX;
	for (int i = 0; i < this->bands; ++i, ++index)
	{
		unsigned char g00 = this->pixUC[index ];
		unsigned char g01 = this->pixUC[index + this->diffY];
		long index1 = index + this->diffX;
		unsigned char g10 = this->pixUC[index1];
		unsigned char g11 = this->pixUC[index1 + this->diffY];
		float a0 = float(g00);
		float a1 = float(g10) - a0;
		float a2 = float(g01) - a0;
		float a3 = float(g11) - a0 - a1 - a2;

		colourVector[i] = (unsigned char) (float(a0 + a1 * dcol + a2 * drow + a3 * drow * dcol));
		if (colourVector[i] == 0) colourVector[i] = 1;
	};

	return true;
};

//===========================================================================================

bool CImageBuffer::getInterpolatedColourVecBilin(double row, double col, unsigned short *colourVector) const
{
	int colIdx = (int) floor(col);
	int rowIdx = (int) floor(row);
	float dcol = float(col) - float(colIdx);
	float drow = float(row) - float(rowIdx);

	if ((rowIdx < 0) || (colIdx < 0) || (rowIdx + 1 >= this->getHeight()) || (colIdx + 1 >= this->getWidth())) 
	{
		for (int i = 0; i < this->bands; ++i)
		{
			colourVector[i] = 0;
		}
		return false;
	}

	long index = rowIdx * this->diffY + colIdx * this->diffX;
	for (int i = 0; i < this->bands; ++i, ++index)
	{
		unsigned short g00 = this->pixUS[index ];
		unsigned short g01 = this->pixUS[index + this->diffY];
		long index1 = index + this->diffX;
		unsigned short g10 = this->pixUS[index1];
		unsigned short g11 = this->pixUS[index1 + this->diffY];
		float a0 = float(g00);
		float a1 = float(g10) - a0;
		float a2 = float(g01) - a0;
		float a3 = float(g11) - a0 - a1 - a2;

		colourVector[i] = (unsigned short) (float(a0 + a1 * dcol + a2 * drow + a3 * drow * dcol));
		if (colourVector[i] == 0) colourVector[i] = 1;
	};
	return true;
};

//===========================================================================================

bool CImageBuffer::getInterpolatedColourVecBilin(double row, double col, short *colourVector) const
{
	int colIdx = (int) floor(col);
	int rowIdx = (int) floor(row);
	float dcol = float(col) - float(colIdx);
	float drow = float(row) - float(rowIdx);

	if ((rowIdx < 0) || (colIdx < 0) || (rowIdx + 1 >= this->getHeight()) || (colIdx + 1 >= this->getWidth())) 
	{
		for (int i = 0; i < this->bands; ++i)
		{
			colourVector[i] = 0;
		}
		return false;
	}

	long index = rowIdx * this->diffY + colIdx * this->diffX;
	for (int i = 0; i < this->bands; ++i, ++index)
	{
		short g00 = this->pixS[index ];
		short g01 = this->pixS[index + this->diffY];
		long index1 = index + this->diffX;
		short g10 = this->pixS[index1];
		short g11 = this->pixS[index1 + this->diffY];
		float a0 = float(g00);
		float a1 = float(g10) - a0;
		float a2 = float(g01) - a0;
		float a3 = float(g11) - a0 - a1 - a2;

		colourVector[i] = (short) (float(a0 + a1 * dcol + a2 * drow + a3 * drow * dcol));
		if (colourVector[i] == 0) colourVector[i] = 1;
	};
	return true;
};

//===========================================================================================

bool CImageBuffer::getInterpolatedColourVecBilin(double row, double col, float *colourVector) const
{
	int colIdx = (int) floor(col);
	int rowIdx = (int) floor(row);
	float dcol = float(col) - float(colIdx);
	float drow = float(row) - float(rowIdx);

	if ((rowIdx < 0) || (colIdx < 0) || (rowIdx + 1 >= this->getHeight()) || (colIdx + 1 >= this->getWidth())) 
	{
		for (int i = 0; i < this->bands; ++i)
		{
			colourVector[i] = 0;
		}
		return false;
	}

	long index = rowIdx * this->diffY + colIdx * this->diffX;
	for (int i = 0; i < this->bands; ++i, ++index)
	{
		float g00 = this->pixF[index ];
		float g01 = this->pixF[index + this->diffY];
		long index1 = index + this->diffX;
		float g10 = this->pixF[index1];
		float g11 = this->pixF[index1 + this->diffY];
		float a0 = g00;
		float a1 = g10 - a0;
		float a2 = g01 - a0;
		float a3 = g11 - a0 - a1 - a2;

		colourVector[i] = a0 + a1 * dcol + a2 * drow + a3 * drow * dcol;
		if (colourVector[i] == 0) colourVector[i] = 1;
	};
	return true;
};

//===========================================================================================

bool CImageBuffer::getInterpolatedColourVecBicub(const C2DPoint &p, unsigned char *colourVector) const
{
	float dcol = float(p.x - this->offsetX);
	float drow = float(p.y - this->offsetY);
	int col = (int) floor(dcol);
	int row = (int) floor(drow);
	dcol -= col;
	drow -= row;

	if ((row < 1) || (col < 1) || (row + 3 >= this->getHeight()) || (col + 3 >= this->getWidth()))
	{
		for (int i = 0; i < this->bands; ++i)
		{
			colourVector[i] = 0;
		}
		return false;
	}

	long index = (row - 1) * this->diffY + (col - 1) * this->diffX;
	for (int i = 0; i < this->bands; ++i, ++index)
	{
		long index1_0 = index;
		long index1   = index1_0;
		float gm1Y = this->pixUC[index1]; index1 += this->diffY;
		float g00Y = this->pixUC[index1]; index1 += this->diffY;
		float g01Y = this->pixUC[index1]; index1 += this->diffY;
		float g02Y = this->pixUC[index1];
		float gm1X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixUC[index1]; index1 += this->diffY;
		g00Y = this->pixUC[index1]; index1 += this->diffY;
		g01Y = this->pixUC[index1]; index1 += this->diffY;
		g02Y = this->pixUC[index1];
		float g00X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixUC[index1]; index1 += this->diffY;
		g00Y = this->pixUC[index1]; index1 += this->diffY;
		g01Y = this->pixUC[index1]; index1 += this->diffY;
		g02Y = this->pixUC[index1];
		float g01X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixUC[index1]; index1 += this->diffY;
		g00Y = this->pixUC[index1]; index1 += this->diffY;
		g01Y = this->pixUC[index1]; index1 += this->diffY;
		g02Y = this->pixUC[index1];
		float g02X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);

		float g = cubic(gm1X, g00X, g01X, g02X, dcol);
		if (g > 255) g = 255;
		else if (g <= 0) g = 1;
		colourVector[i] = (unsigned char) g;
	};
	return true;
};

//===========================================================================================

bool CImageBuffer::getInterpolatedColourVecBicub(const C2DPoint &p, short *colourVector) const
{
	float dcol = float(p.x - this->offsetX);
	float drow = float(p.y - this->offsetY);
	int col = (int) floor(dcol);
	int row = (int) floor(drow);
	dcol -= col;
	drow -= row;

	if ((row < 1) || (col < 1) || (row + 3 >= this->getHeight()) || (col + 3 >= this->getWidth()))
	{
		for (int i = 0; i < this->bands; ++i)
		{
			colourVector[i] = 0;
		}
		return false;
	}

	long index = (row - 1) * this->diffY + (col - 1) * this->diffX;
	for (int i = 0; i < this->bands; ++i, ++index)
	{
		long index1_0 = index;
		long index1   = index1_0;
		float gm1Y = this->pixS[index1]; index1 += this->diffY;
		float g00Y = this->pixS[index1]; index1 += this->diffY;
		float g01Y = this->pixS[index1]; index1 += this->diffY;
		float g02Y = this->pixS[index1];
		float gm1X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixS[index1]; index1 += this->diffY;
		g00Y = this->pixS[index1]; index1 += this->diffY;
		g01Y = this->pixS[index1]; index1 += this->diffY;
		g02Y = this->pixS[index1];
		float g00X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixS[index1]; index1 += this->diffY;
		g00Y = this->pixS[index1]; index1 += this->diffY;
		g01Y = this->pixS[index1]; index1 += this->diffY;
		g02Y = this->pixS[index1];
		float g01X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixS[index1]; index1 += this->diffY;
		g00Y = this->pixS[index1]; index1 += this->diffY;
		g01Y = this->pixS[index1]; index1 += this->diffY;
		g02Y = this->pixS[index1];
		float g02X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);

		float g = cubic(gm1X, g00X, g01X, g02X, dcol);
		colourVector[i] = (short) g;
		if (colourVector[i] == 0) colourVector[i] = 1;
	};
	return true;
};

//===========================================================================================

bool CImageBuffer::getInterpolatedColourVecBicub(const C2DPoint &p, unsigned short *colourVector) const
{
	float dcol = float(p.x - this->offsetX);
	float drow = float(p.y - this->offsetY);
	int col = (int) floor(dcol);
	int row = (int) floor(drow);
	dcol -= col;
	drow -= row;

	if ((row < 1) || (col < 1) || (row + 3 >= this->getHeight()) || (col + 3 >= this->getWidth()))
	{
		for (int i = 0; i < this->bands; ++i)
		{
			colourVector[i] = 0;
		}
		return false;
	}

	long index = (row - 1) * this->diffY + (col - 1) * this->diffX;
	for (int i = 0; i < this->bands; ++i, ++index)
	{
		long index1_0 = index;
		long index1   = index1_0;
		float gm1Y = this->pixUS[index1]; index1 += this->diffY;
		float g00Y = this->pixUS[index1]; index1 += this->diffY;
		float g01Y = this->pixUS[index1]; index1 += this->diffY;
		float g02Y = this->pixUS[index1];
		float gm1X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixUS[index1]; index1 += this->diffY;
		g00Y = this->pixUS[index1]; index1 += this->diffY;
		g01Y = this->pixUS[index1]; index1 += this->diffY;
		g02Y = this->pixUS[index1];
		float g00X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixUS[index1]; index1 += this->diffY;
		g00Y = this->pixUS[index1]; index1 += this->diffY;
		g01Y = this->pixUS[index1]; index1 += this->diffY;
		g02Y = this->pixUS[index1];
		float g01X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixUS[index1]; index1 += this->diffY;
		g00Y = this->pixUS[index1]; index1 += this->diffY;
		g01Y = this->pixUS[index1]; index1 += this->diffY;
		g02Y = this->pixUS[index1];
		float g02X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);

		float g = cubic(gm1X, g00X, g01X, g02X, dcol);
		if (g <= 0) g = 1;
		else if (g > 65535) g = 65535;
		colourVector[i] = (unsigned short) g;
	};
	return true;
};

//===========================================================================================

bool CImageBuffer::getInterpolatedColourVecBicub(const C2DPoint &p, float *colourVector) const
{
	float dcol = float(p.x - this->offsetX);
	float drow = float(p.y - this->offsetY);
	int col = (int) floor(dcol);
	int row = (int) floor(drow);
	dcol -= col;
	drow -= row;

	if ((row < 1) || (col < 1) || (row + 3 >= this->getHeight()) || (col + 3 >= this->getWidth()))
	{
		for (int i = 0; i < this->bands; ++i)
		{
			colourVector[i] = 0;
		}
		return false;
	}

	long index = (row - 1) * this->diffY + (col - 1) * this->diffX;
	for (int i = 0; i < this->bands; ++i, ++index)
	{
		long index1_0 = index;
		long index1   = index1_0;
		float gm1Y = this->pixF[index1]; index1 += this->diffY;
		float g00Y = this->pixF[index1]; index1 += this->diffY;
		float g01Y = this->pixF[index1]; index1 += this->diffY;
		float g02Y = this->pixF[index1];
		float gm1X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixF[index1]; index1 += this->diffY;
		g00Y = this->pixF[index1]; index1 += this->diffY;
		g01Y = this->pixF[index1]; index1 += this->diffY;
		g02Y = this->pixF[index1];
		float g00X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixF[index1]; index1 += this->diffY;
		g00Y = this->pixF[index1]; index1 += this->diffY;
		g01Y = this->pixF[index1]; index1 += this->diffY;
		g02Y = this->pixF[index1];
		float g01X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);
		index1_0 += this->diffX;
		index1   = index1_0;
		gm1Y = this->pixF[index1]; index1 += this->diffY;
		g00Y = this->pixF[index1]; index1 += this->diffY;
		g01Y = this->pixF[index1]; index1 += this->diffY;
		g02Y = this->pixF[index1];
		float g02X = cubic(gm1Y, g00Y, g01Y, g02Y, drow);

		colourVector[i] = cubic(gm1X, g00X, g01X, g02X, dcol);
		if (colourVector[i] == 0) colourVector[i] = 1;
	};
	return true;
};


//===========================================================================================

float CImageBuffer::cubic(float gm1, float g0, float g1, float g2, float dx)
{
	gm1 -= g0; g1 -= g0; g2 -= g0;
	float a2 = (g1 + gm1) * (float) 0.5;
	float a1 = (-g2 + (float) 6.0 * g1 - (float) 2.0 * gm1) * (float) 0.16666666666666666666666666666667;
	float a3 = ( g2 - (float) 3.0 * g1 -               gm1) * (float) 0.16666666666666666666666666666667;
	return ((((((a3 * dx) + a2) * dx) + a1) * dx) + g0);
}

//===========================================================================================

void CImageBuffer::setMeanStdDev(float mean, float standardDev)
{
	if (this->bands == 1)
	{
		vector<float> minVec, maxVec, meanVec, medianVec, sdevVec;
		this->getStatistics(0,minVec, maxVec, meanVec, medianVec, sdevVec);
		float currentMean = meanVec[0];
		float currentSdev = sdevVec[0];		

		float gFact = standardDev / currentSdev;
						
		if (this->bpp == 8)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				float g = (float) this->pixUC[u];
				g = float((g - currentMean) * gFact + mean);
				if (g < 0) g = 0;
				else if (g > 255) g = 255;
				this->pixUC[u] = (unsigned char) g;
			}			
		}
		else if (this->bpp == 16)
		{
			if (this->unsignedFlag)
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					float g = (float) this->pixUS[u];
					g = float((g - currentMean) * gFact + mean);
					if (g < 0) g = 0;
					else if (g > 32000) g = 32000;
					this->pixUS[u] = (unsigned short) g;
				}
			}
			else
			{
				for (unsigned long u = 0; u < this->nGV; ++u)
				{
					float g = (float) this->pixS[u];
					g = float((g - currentMean) * gFact + mean);
					if (g < -16000) g = -16000;
					else if (g > 16000) g = 16000;
					this->pixS[u] = (short) g;
				}
			}
		}
		else if (this->bpp == 32)
		{
			for (unsigned long u = 0; u < this->nGV; ++u)
			{
				float g = this->pixF[u];
				g = float((g - currentMean) * gFact + mean);
				this->pixF[u] = g;
			}
		}
	}
};

//===========================================================================================

void CImageBuffer::RGB2IHS(float r, float g, float b, float &i, float &h, float &s)
{
//	float is = r * r + g * g;
//	i = sqrt(is + b * b);
//	if (i <= 0.0) h = s = 0.0;
//	else
//	{
//		if (r == 0.0)
//		{
//			h = 0.0;
//		}
//		else h = atan(g/r);
//
//		if (is == 0) s = 0.0;
//		else s = atan(b / sqrt(is));
//	}

	i = 0.0;
	s = 0.0;
	h = 0.0;

	if ((r >= g) && (r >= b))
	{
		i = r;
		if (g >= b)		// max = r, med = g, min = b
		{ 
			float delta = r - b;
			if ((delta > 0.0) && (i > 0.0))
			{
				s = (float) (1.0 - b / i); 
				h = ((g - b) / delta) / float(6.0);
			}
		}
		else			// max = r, med = b, min = g
		{
			float delta = r - g;
			if ((delta > 0.0) && (i > 0.0))
			{
				s = (float) (1.0 - g / i); 
				h = (float(6.0) - (b - g) / delta) / float(6.0);
			}
		}
	}
	else if (g >= b) 
	{
		i = g;
		if (r >= b)		// max = g, med = r, min = b
		{ 
			float delta = g - b;
			if ((delta > 0.0) && (i > 0.0))
			{
				s = (float) (1.0 - b / i); 
				h = (float(2.0) - (r - b) / delta) / float(6.0);
			}
		}
		else			// max = g, med = b, min = r
		{
			float delta = g - r;
			if ((delta > 0.0) && (i > 0.0))
			{
				s = (float) (1.0 - r / i); 
				h = (float(2.0) + (b - r) / delta) / float(6.0);
			}
		}
	}
	else
	{
		i = b;
		if (r >= g)		// max = b, med = r, min = g
		{ 
			float delta = b - g;
			if ((delta > 0.0) && (i > 0.0))
			{
				s = (float) (1.0 - g / i); 
				h = (float(4.0) + (r - g) / delta) / float(6.0);
			}
		}
		else			// max = b, med = g, min = r
		{
			float delta = b - r;
			if ((delta > 0.0) && (i > 0.0))
			{
				s = (float) (1.0 - r / i); 
				h = (float(4.0) - (g - r) / delta) / float(6.0);
			}
		}
	}	        		    

/* /////afds
	i = (float)((r + g + b) * oneThird);
	s = 0.0;
	h = 0.0;

	if (i > 0.0)
	{
		if ((r >= g) && (r >= b))
		{
			if (g >= b)		// max = r, med = g, min = b
			{ 
				float delta = r - b;
				if (delta > 0.0)
				{
					s = (float) (1.0 - b / i); 
					h = ((g - b) / delta) * oneSixth;
				}
			}
			else			// max = r, med = b, min = g
			{
				float delta = r - g;
				if (delta > 0.0)
				{
					s = (float) (1.0 - g / i); 
					h = (6.0 - (b - g) / delta) * oneSixth;
				}
			}
		}
		else if (g >= b) 
		{
			if (r >= b)		// max = g, med = r, min = b
			{ 
				float delta = g - b;
				if (delta > 0.0)
				{
					s = (float) (1.0 - b / i); 
					h = (2.0 - (r - b) / delta) * oneSixth;
				}
			}
			else			// max = g, med = b, min = r
			{
				float delta = g - r;
				if (delta > 0.0)
				{
					s = (float) (1.0 - r / i); 
					h = (2.0 + (b - r) / delta) * oneSixth;
				}
			}
		}
		else
		{
			if (r >= g)		// max = b, med = r, min = g
			{ 
				float delta = b - g;
				if (delta > 0.0)
				{
					s = (float) (1.0 - g / i); 
					h = (4.0 + (r - g) / delta) * oneSixth;

				}
			}
			else			// max = b, med = g, min = r
			{
				float delta = b - r;
				if (delta > 0.0)
				{
					s = (float) (1.0 - r / i); 
					h = (4.0 - (g - r) / delta) * oneSixth;
				}
			}
		}	        		    
	} // if (i > 0.0)
	*/
}

//===========================================================================================

void CImageBuffer::IHS2RGB(float i, float h, float s, float &r, float &g, float &b)
{
	if (i == 0)
	{
		r = g = b = 0.0;
	}
	else if (s == 0)		// achromatic (grey)
	{		
		r = g = b = i;
	}
	else
	{
//		float cs = cos(s);
//		r = i * cos(h) * cs;
//		g = i * sin(h) * cs;
//		b = i * sin(s);
		h *= 6;

/*	adsfdsaf	if (h < 1)			// max = r, med = g, min = b
		{
			b = i * ((float) 1.0 - s);
			i *= 3;
			r = (i + b * (h - 2)) / (h + 1);
			g = i - b - r;
		}
		else if (h < 2)		// max = g, med = r, min = b
		{
			b = i * ((float) 1 - s);
			i *= 3;
			g = (i - b * h) / (3 - h);
			r = i - b - g;
		}
		else if (h < 3)		// max = g, med = b, min = r
		{
			r = i * ((float) 1 - s);
			i *= 3;
			g = (i + r * (h - 4)) / (h - 1);
			b = i - r - g;
		}
		else if (h < 4)		// max = b, med = b, min = r
		{
			r = i * (1 - s);
			i *= 3;
			b = (i + r * (2 - h)) / ( 5 - h);
			g = i - b - r;
		}
		else if (h < 5.0)
		{
			g = i * (1 - s);
			i *= 3;
			b = (i + g * (h - 6)) / (h - 3);
			r = i - b - g;
		}
		else // h >=5,		// max = r, med = b, min = g
		{
			g = i * (1 - s);
			i *= 3;
			r = (i + g * (4 - h)) / (7 - h);
			b = i - g - r;
		}
*/


		if (h < 1)			// max = r, med = g, min = b
		{
			r = i;
			b = i * ((float) 1.0 - s);
			g = b + (i - b) * h;
		}
		else if (h < 2)		// max = g, med = r, min = b
		{
			g = i;
			b = i * ((float) 1 - s);
			r = g + (i - b) * (1 - h);
		}
		else if (h < 3)		// max = g, med = b, min = r
		{
			g = i;
			r = i * ((float) 1 - s);
			b = r + (i - r) * (h - 2);
		}
		else if (h < 4)		// max = b, med = g, min = r
		{
			b = i;
			r = i * (1 - s);
			g = b + (i - r) * (3 - h);
		}
		else if (h < 5.0)	// max = b, med = r, min = g
		{
			b = i;
			g = i * (1 - s);
			r = g + (i - g) * (h - 4);
		}
		else				// max = r, med = b, min = g
		{
			r = i;
			g = i * (1 - s);
			b = r + (i - g) * (5 - h);
		}		
	}
};

//===========================================================================================

void CImageBuffer::RGB2IHS1(float r, float g, float b, float &i, float &h, float &s)
{
	i = (r + g + b) / 3.0f;

	if ((i <= 0) || ((r == g) && (r == b) && (g == b)))
	{
		s = 0.0;
		h = 0.0;
	}
	else
	{
		float dmy;

		if ((b <= g) && (b <= r)) 
		{
			dmy = (i - b) * 3.0f;
			h = (g - b) / dmy ;
			
		}
		else if (r <= g) 
		{
			dmy = (i - r) * 3.0f;
			h = float(1.0) + (b - r) / dmy;
		}
		else 
		{
			dmy = (i - g) * 3.0f;
			h = float(2.0) + (r - g) / dmy;
		}

		s = dmy  / i;
	}
}

//===========================================================================================

void CImageBuffer::IHS2RGB1(float i, float h, float s, float &r, float &g, float &b)
{
	if (h <= 1)
	{
		float threeSh = 3 * s * h;
		float oneMinS = 1 - s;
		r = i * (1 + 2 * s - threeSh);
		g = i * (oneMinS + threeSh);
		b = i * oneMinS;
	}
	else if (h <= 2)
	{
		float oneMinS = 1 - s;
		float oneMinH = h - 1;
		float threeSh = 3 * s * oneMinH;
		r = i * oneMinS;
		g = i * (1 + 2 * s - threeSh);
		b = i * (oneMinS + threeSh);
	}
	else
	{
		float oneMinS = 1 - s;
		float twoMinH = h - 2;
		float threeSh = 3 * s * twoMinH;
		r = i * (oneMinS + threeSh);
		g = i * oneMinS;
		b = i * (1 + 2 * s - threeSh);

	}

	if (r > 255) r = 255;
	else if (r < 0) r =0;

	if (b > 255) b = 255;
	else if (b < 0) r =0;

	if (g > 255) g = 255;
	else if (g < 0) g = 0;
};

//===========================================================================================

bool CImageBuffer::RGB2IHS()
{
	bool ret = true;
	if ((this->bands < 3) || (this->bands > 4)) ret = false;
	else
	{
		ret = this->convertToFloat();
		if (ret)
		{
			for (unsigned int idx = 0; idx < this->nGV; idx += this->diffX)
			{
				RGB2IHS(this->pixF[idx], this->pixF[idx + 1], this->pixF[idx + 2],
						this->pixF[idx], this->pixF[idx + 1], this->pixF[idx + 2]);
			}
		}
	}

	return ret;
};

//===========================================================================================

bool CImageBuffer::IHS2RGB(int I_bpp, bool I_unsignedFlag)
{
	bool ret = true;
	if ((this->bands < 3) || (this->bands > 4) || (this->bpp != 32)) ret = false;
	else
	{
		for (unsigned int idx = 0; idx < this->nGV; idx += this->diffX)
		{
			IHS2RGB(this->pixF[idx], this->pixF[idx + 1], this->pixF[idx + 2],
					this->pixF[idx], this->pixF[idx + 1], this->pixF[idx + 2]);
		}
		if (I_bpp == 8) ret = this->convertToUnsignedChar();
		else if (I_bpp == 16)
		{
			if (I_unsignedFlag) ret = this->convertToUnsignedShort();
			else ret = this->convertToShort();
		}
	}

	return ret;
};

//===========================================================================================

bool CImageBuffer::pansharpen(const CImageBuffer &pan, double *rotMat, float *mean, float meanPan)
{
	bool ret = true;
	if ((this->bands < 3) || (this->bands > 4)) ret = false;
	else
	{
		float *colourVec = new float[this->bands];
		float *colourVecTrans = new float[this->bands];

		if ((this->bpp == 16) && (pan.bpp == 16) && (this->unsignedFlag) && (pan.unsignedFlag))
		{
			for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
			{
				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{	
					colourVec[ibnd] = float(this->pixUS[idx + ibnd]) - mean[ibnd];
				}
				

				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{	
					colourVecTrans[ibnd] = 0.0;
					for (int jbnd = 0, matIdx = ibnd; jbnd < this->bands; ++jbnd, matIdx += this->bands)
					{
						colourVecTrans[ibnd] += float(rotMat[matIdx]) * colourVec[jbnd];
					}
				}

				colourVecTrans[this->bands - 1] = (float) pan.pixUShort(idxP) - meanPan;

				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{	
					colourVec[ibnd] = 0.0;
					for (int jbnd = 0, matIdx = ibnd * this->bands; jbnd < this->bands; ++jbnd, ++matIdx)
					{
						colourVec[ibnd] += float(rotMat[matIdx]) * colourVecTrans[jbnd];
					}
					colourVec[ibnd] += mean[ibnd];
					this->pixUS[idx + ibnd] = (unsigned short) colourVec[ibnd];
				}
			}
		}
		else if ((this->bpp == 8) && (pan.bpp == 8))
		{
			for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
			{
				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{	
					colourVec[ibnd] = float(this->pixUC[idx + ibnd]) - mean[ibnd];
				}
				

				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{	
					colourVecTrans[ibnd] = 0.0;
					for (int jbnd = 0, matIdx = ibnd; jbnd < this->bands; ++jbnd, matIdx += this->bands)
					{
						colourVecTrans[ibnd] += float(rotMat[matIdx]) * colourVec[jbnd];
					}
				}

				colourVecTrans[this->bands - 1] = (float) pan.pixUChar(idxP) - meanPan;

				for (int ibnd = 0; ibnd < this->bands; ++ibnd)
				{	
					colourVec[ibnd] = 0.0;
					for (int jbnd = 0, matIdx = ibnd * this->bands; jbnd < this->bands; ++jbnd, ++matIdx)
					{
						colourVec[ibnd] += float(rotMat[matIdx]) * colourVecTrans[jbnd];
					}
					colourVec[ibnd] += mean[ibnd];
					this->pixUC[idx + ibnd] = (unsigned char) colourVec[ibnd];
				}
			
			}
		}
		else ret = false;

		delete [] colourVec;
		delete [] colourVecTrans;
	}

	return ret;
};

//===========================================================================================

bool CImageBuffer::pansharpen(const CImageBuffer &pan, float corrVal, float panFactor)
{
	bool ret = true;
	if ((this->bands < 3) || (this->bands > 4)) ret = false;
	else
	{
		if (this->bands != 4) corrVal = -1;

		float *ihs = new float[this->nGV];

		if ((this->bpp == 16) && (pan.bpp == 16) && (this->unsignedFlag) && (pan.unsignedFlag))
		{
			if (corrVal <= 0.0)
			{
				for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
				{
					RGB2IHS(this->pixUS[idx], this->pixUS[idx + 1], this->pixUS[idx + 2],
							ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					ihs[idx] = panFactor * (float) pan.pixUShort(idxP);
					IHS2RGB(ihs[idx], ihs[idx + 1], ihs[idx + 2], ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					this->pixUS[idx]     = (unsigned short) ihs[idx]; 
					this->pixUS[idx + 1] = (unsigned short) ihs[idx + 1];
					this->pixUS[idx + 2] = (unsigned short) ihs[idx + 2];
				}
			}
			else
			{
				for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
				{
					RGB2IHS(this->pixUS[idx], this->pixUS[idx + 1], this->pixUS[idx + 2],
							ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					ihs[idx] = panFactor * ((float) pan.pixUShort(idxP) - corrVal * this->pixUS[idx + 3]);
					if (ihs[idx] < 0) ihs[idx] = 0;
					IHS2RGB(ihs[idx], ihs[idx + 1], ihs[idx + 2], ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					this->pixUS[idx]     = (unsigned short) ihs[idx]; 
					this->pixUS[idx + 1] = (unsigned short) ihs[idx + 1];
					this->pixUS[idx + 2] = (unsigned short) ihs[idx + 2];
				}
			}
		}
		else if ((this->bpp == 8) && (pan.bpp == 8))
		{
			if (corrVal <= 0.0)
			{
				for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
				{
					RGB2IHS(this->pixUC[idx], this->pixUC[idx + 1], this->pixUC[idx + 2],
							ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					ihs[idx] = panFactor * (float) pan.pixUChar(idxP);
					IHS2RGB(ihs[idx], ihs[idx + 1], ihs[idx + 2], ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					this->pixUC[idx]     = (unsigned char) ihs[idx]; 
					this->pixUC[idx + 1] = (unsigned char) ihs[idx + 1];
					this->pixUC[idx + 2] = (unsigned char) ihs[idx + 2];
				}
			}
			else
			{
				for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
				{
					RGB2IHS(this->pixUC[idx], this->pixUC[idx + 1], this->pixUC[idx + 2],
							ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					ihs[idx] = panFactor * ((float) pan.pixUChar(idxP) - corrVal * this->pixUC[idx + 3]);
					if (ihs[idx] < 0) ihs[idx] = 0;
					else if (ihs[idx] > 255) ihs[idx] = 255;
					IHS2RGB(ihs[idx], ihs[idx + 1], ihs[idx + 2], ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					this->pixUC[idx]     = (unsigned char) ihs[idx]; 
					this->pixUC[idx + 1] = (unsigned char) ihs[idx + 1];
					this->pixUC[idx + 2] = (unsigned char) ihs[idx + 2];
				}
			}
		}
		else ret = false;

		delete [] ihs;
	}

	return ret;
};

//===========================================================================================

bool CImageBuffer::pansharpen1(const CImageBuffer &pan, float corrVal, float panFactor)
{
	bool ret = true;
	if ((this->bands < 3) || (this->bands > 4)) ret = false;
	else
	{
		if (this->bands != 4) corrVal = -1;

		float *ihs = new float[this->nGV];

		if ((this->bpp == 16) && (pan.bpp == 16) && (this->unsignedFlag) && (pan.unsignedFlag))
		{
			if (corrVal <= 0.0)
			{
				for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
				{
					RGB2IHS(this->pixUS[idx], this->pixUS[idx + 1], this->pixUS[idx + 2],
							ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					ihs[idx] = panFactor * (float) pan.pixUShort(idxP);
					IHS2RGB(ihs[idx], ihs[idx + 1], ihs[idx + 2], ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					this->pixUS[idx]     = (unsigned short) ihs[idx]; 
					this->pixUS[idx + 1] = (unsigned short) ihs[idx + 1];
					this->pixUS[idx + 2] = (unsigned short) ihs[idx + 2];
				}
			}
			else
			{
				for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
				{
					RGB2IHS(this->pixUS[idx], this->pixUS[idx + 1], this->pixUS[idx + 2],
							ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					ihs[idx] = panFactor * ((float) pan.pixUShort(idxP) - corrVal * this->pixUS[idx + 3]);
					if (ihs[idx] < 0) ihs[idx] = 0;
					IHS2RGB(ihs[idx], ihs[idx + 1], ihs[idx + 2], ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					this->pixUS[idx]     = (unsigned short) ihs[idx]; 
					this->pixUS[idx + 1] = (unsigned short) ihs[idx + 1];
					this->pixUS[idx + 2] = (unsigned short) ihs[idx + 2];
				}
			}
		}
		else if ((this->bpp == 8) && (pan.bpp == 8))
		{
			if (corrVal <= 0.0)
			{
				for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
				{
					RGB2IHS(this->pixUC[idx], this->pixUC[idx + 1], this->pixUC[idx + 2],
							ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					ihs[idx] = panFactor * (float) pan.pixUChar(idxP);
					IHS2RGB(ihs[idx], ihs[idx + 1], ihs[idx + 2], ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					this->pixUC[idx]     = (unsigned char) ihs[idx]; 
					this->pixUC[idx + 1] = (unsigned char) ihs[idx + 1];
					this->pixUC[idx + 2] = (unsigned char) ihs[idx + 2];
				}
			}
			else
			{
				for (unsigned int idx = 0, idxP = 0; idx < this->nGV; idx += this->diffX, idxP += pan.diffX)
				{
					RGB2IHS(this->pixUC[idx], this->pixUC[idx + 1], this->pixUC[idx + 2],
							ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					ihs[idx] = panFactor * ((float) pan.pixUChar(idxP) - corrVal * this->pixUC[idx + 3]);
					if (ihs[idx] < 0) ihs[idx] = 0;
					else if (ihs[idx] > 255) ihs[idx] = 255;
					IHS2RGB(ihs[idx], ihs[idx + 1], ihs[idx + 2], ihs[idx], ihs[idx + 1], ihs[idx + 2]);
					this->pixUC[idx]     = (unsigned char) ihs[idx]; 
					this->pixUC[idx + 1] = (unsigned char) ihs[idx + 1];
					this->pixUC[idx + 2] = (unsigned char) ihs[idx + 2];
				}
			}
		}
		else ret = false;

		delete [] ihs;
	}

	return ret;
};

//===========================================================================================

bool CImageBuffer::resample(const CImageBuffer &input, const CTrans2D &trafo, eResamplingType resType)
{
	bool ret = true;

	C2DPoint pIn, pOut;
	unsigned long xmax = this->offsetX + this->width;
	unsigned long ymax = this->offsetY + this->height;
	unsigned long uMin = 0;

	if ((this->bpp == 8) && (input.bpp == 8) && (this->unsignedFlag) && (input.unsignedFlag))
	{
		if (resType == eNearestNeighbour)
		{
			for (unsigned int y = this->offsetY; y < ymax; ++y, uMin += this->diffY)
			{
				unsigned long u = uMin;
				for (unsigned int x = this->offsetX; x < xmax; ++x, u += this->diffX)
				{
					pIn.x = x; pIn.y = y;
					trafo.transform(pOut, pIn);
					pOut.x = floor(pOut.x + 0.5) - this->offsetX;
					pOut.y = floor(pOut.y + 0.5) - this->offsetY;

					if ((pOut.x < 0) || (pOut.x >= this->width) ||
						(pOut.y < 0) || (pOut.y >= this->height))
					{			
						for (int i = 0; i < bands; ++i)
						{
							this->pixUC[u + i] = 0;
						}
					}
					else
					{
						long index = this->getIndex(long(pOut.y),long(pOut.x));
						for (int i = 0; i < bands; ++i)
						{
							this->pixUC[u + i] = input.pixUC[index + i];
						}
					}
				}
			}
		}
		else if (resType == eBilinear)
		{
			for (unsigned int y = this->offsetY; y < ymax; ++y, uMin += this->diffY)
			{
				unsigned long u = uMin;
				for (unsigned int x = this->offsetX; x < xmax; ++x, u += this->diffX)
				{
					pIn.x = x; pIn.y = y;
					trafo.transform(pOut, pIn);
					input.getInterpolatedColourVecBilin(pOut, this->pixUC + u);
				}
			}
		}
		else // eBicubic
		{
			for (unsigned int y = this->offsetY; y < ymax; ++y, uMin += this->diffY)
			{
				unsigned long u = uMin;
				for (unsigned int x = this->offsetX; x < xmax; ++x, u += this->diffX)
				{
					pIn.x = x; pIn.y = y;
					trafo.transform(pOut, pIn);
					input.getInterpolatedColourVecBicub(pOut, this->pixUC + u);
				}
			}
		}
	}
	else if ((this->bpp == 16) && (input.bpp == 16) && (this->unsignedFlag)  && (input.unsignedFlag))	
	{
		if (resType == eNearestNeighbour)
		{
			for (unsigned int y = this->offsetY; y < ymax; ++y, uMin += this->diffY)
			{
				unsigned long u = uMin;
				for (unsigned int x = this->offsetX; x < xmax; ++x, u += this->diffX)
				{
					pIn.x = x; pIn.y = y;
					trafo.transform(pOut, pIn);
					pOut.x = floor(pOut.x + 0.5) - this->offsetX;
					pOut.y = floor(pOut.y + 0.5) - this->offsetY;

					if ((pOut.x < 0) || (pOut.x >= this->width) ||
						(pOut.y < 0) || (pOut.y >= this->height))
					{			
						for (int i = 0; i < bands; ++i)
						{
							this->pixUS[u + i] = 0;
						}
					}
					else
					{
						long index = this->getIndex(long(pOut.y), long(pOut.x));

						for (int i = 0; i < bands; ++i)
						{
							this->pixUS[u + i] = input.pixUS[index + i];
						}
					}
				}
			}
		}
		else if (resType == eBilinear)
		{
			for (unsigned int y = this->offsetY; y < ymax; ++y, uMin += this->diffY)
			{
				unsigned long u = uMin;
				for (unsigned int x = this->offsetX; x < xmax; ++x, u += this->diffX)
				{
					pIn.x = x; pIn.y = y;
					trafo.transform(pOut, pIn);
					input.getInterpolatedColourVecBilin(pOut, this->pixUS + u);
				}
			}
		}
		else // eBicubic
		{
			for (unsigned int y = this->offsetY; y < ymax; ++y, uMin += this->diffY)
			{
				unsigned long u = uMin;
				for (unsigned int x = this->offsetX; x < xmax; ++x, u += this->diffX)
				{
					pIn.x = x; pIn.y = y;
					trafo.transform(pOut, pIn);
					input.getInterpolatedColourVecBicub(pOut, this->pixUS + u);
				}
			}
		}
	}
	else if ((this->bpp == 32) && (input.bpp == 32))	
	{			
		if (resType == eNearestNeighbour)
		{
			for (unsigned int y = this->offsetY; y < ymax; ++y, uMin += this->diffY)
			{
				unsigned long u = uMin;
				for (unsigned int x = this->offsetX; x < xmax; ++x, u += this->diffX)
				{
					pIn.x = x; pIn.y = y;
					trafo.transform(pOut, pIn);
					pOut.x = floor(pOut.x + 0.5) - this->offsetX;
					pOut.y = floor(pOut.y + 0.5) - this->offsetY;

					if ((pOut.x < 0) || (pOut.x >= this->width) ||
						(pOut.y < 0) || (pOut.y >= this->height))
					{			
						for (int i = 0; i < bands; ++i)
						{
							this->pixF[u + i] = 0;
						}
					}
					else
					{
						long index = this->getIndex(long(pOut.y), long(pOut.x));

						for (int i = 0; i < bands; ++i)
						{
							this->pixF[u + i] = input.pixUS[index + i];
						}
					}
				}
			}
		}
		else if (resType == eBilinear)
		{
			for (unsigned int y = this->offsetY; y < ymax; ++y, uMin += this->diffY)
			{
				unsigned long u = uMin;
				for (unsigned int x = this->offsetX; x < xmax; ++x, u += this->diffX)
				{
					pIn.x = x; pIn.y = y;
					trafo.transform(pOut, pIn);
					input.getInterpolatedColourVecBilin(pOut, this->pixF + u);
				}
			}
		}
		else // eBicubic
		{
			for (unsigned int y = this->offsetY; y < ymax; ++y, uMin += this->diffY)
			{
				unsigned long u = uMin;
				for (unsigned int x = this->offsetX; x < xmax; ++x, u += this->diffX)
				{
					pIn.x = x; pIn.y = y;
					trafo.transform(pOut, pIn);
					input.getInterpolatedColourVecBicub(pOut, this->pixF + u);
				}
			}
		}
	}
	else ret = false;

	return ret;
};

//===========================================================================================

bool CImageBuffer::getCorrelation(const CImageBuffer &search, 
								  double xRef,    double yRef, 
								  double xSeaMin, double ySeaMin,
								  double dxSea,   double dySea, 
								  int dw, int dh, 
								  vector<float> &rhoVector, int nSteps) const
{
	bool ret = true;

	
	int nc = dw + dw + 1;
	int nr = dh + dh + 1;
	int cMinRef = int(floor(xRef + 0.5)) - this->offsetX - dw;
	int cMaxRef = cMinRef + nc;
	int rMinRef = int(floor(yRef + 0.5)) - this->offsetY - dh;
	int rMaxRef = rMinRef + nr;
	if ((cMinRef < 0) || (cMaxRef >= this->getWidth()) || 
		(rMinRef < 0) || (rMaxRef >= this->getHeight()) ||
		(this->bpp != search.bpp) || (this->unsignedFlag != search.unsignedFlag))
	{
		ret = false;
	}
	else
	{
		// start by computing the variance of the reference image
		int np = nc * nr;
		unsigned long Idx0Ref0   = this->getIndex(rMinRef, cMinRef);
		unsigned long IdxMaxRef  = this->getIndex(rMaxRef - 1, cMaxRef);
		unsigned long IdxRowMaxRef;
		unsigned long IdxRef;

		double sumTemp = 0.0f, sumsqTemp = 0.0f;
		double oneByN = 1.0 / ((double) np);

		if (this->bpp == 8)
		{
				for (unsigned long Idx0Ref = Idx0Ref0; Idx0Ref < IdxMaxRef;  Idx0Ref += this->diffY)
				{
					IdxRowMaxRef = Idx0Ref + nc;
					for (IdxRef = Idx0Ref; IdxRef < IdxRowMaxRef; IdxRef += this->diffX)
				{
					float g = (float) this->pixUChar(IdxRef);
					sumTemp   += g;
					sumsqTemp += g * g;
				}
			}
		}
		else if (this->bpp == 16)
		{
			if (this->unsignedFlag)
			{
				for (unsigned long Idx0Ref = Idx0Ref0; Idx0Ref < IdxMaxRef;  Idx0Ref += this->diffY)
				{
					IdxRowMaxRef = Idx0Ref + nc;
					for (IdxRef = Idx0Ref; IdxRef < IdxRowMaxRef; IdxRef += this->diffX)
					{
						float g = (float) this->pixUShort(IdxRef);
						sumTemp   += g;
						sumsqTemp += g * g;
					}
				};
			}
			else
			{
				for (unsigned long Idx0Ref = Idx0Ref0; Idx0Ref < IdxMaxRef;  Idx0Ref += this->diffY)
				{
					IdxRowMaxRef = Idx0Ref + nc;
					for (IdxRef = Idx0Ref; IdxRef < IdxRowMaxRef; IdxRef += this->diffX)
					{
						float g = (float) this->pixShort(IdxRef);
						sumTemp   += g;
						sumsqTemp += g * g;
					}
				}
			}
		}
		else if (this->bpp == 32)
		{
			for (unsigned long Idx0Ref = Idx0Ref0; Idx0Ref < IdxMaxRef;  Idx0Ref += this->diffY)
			{
				IdxRowMaxRef = Idx0Ref + nc;
				for (IdxRef = Idx0Ref; IdxRef < IdxRowMaxRef; IdxRef += this->diffX)
				{
					float g = this->pixFlt(IdxRef);
					sumTemp   += g;
					sumsqTemp += g * g;
				}
			};
		}
		else ret = false;

		double varTemp = sumsqTemp - sumTemp * sumTemp * oneByN;

		if (varTemp < FLT_EPSILON) ret = false;
		
		if (ret)
		{
			double sigTemp = sqrt(varTemp);
			// the standard deviation of the reference image has been computed, and it is larger than 0

			for (int i = 0; i < nSteps; ++i, xSeaMin += dxSea,  ySeaMin += dySea)
			{
				int cMinSea = int(floor(xSeaMin + 0.5)) - search.offsetX - dw;
				int cMaxSea = cMinSea + nc;

				int rMinSea = int(floor(ySeaMin + 0.5)) - search.offsetY - dh;
				int rMaxSea = rMinSea + nr;

				if ((cMinSea < 0) || (cMaxSea > search.getWidth()) ||
					(rMinSea < 0) || (rMaxSea > search.getHeight()))
				{
					rhoVector[i] = -1.0;
				}
				else
				{
					double sumSearch = 0.0f, sumsqSearch = 0.0f, sumMixed = 0.0f;
					unsigned long Idx0Sea = search.getIndex(rMinSea, cMinSea);
					unsigned long IdxSearch;

					if (this->bpp == 8)
					{
						for (unsigned long Idx0Ref = Idx0Ref0; Idx0Ref < IdxMaxRef;  Idx0Ref += this->diffY, Idx0Sea += search.diffY)
						{
							IdxRowMaxRef = Idx0Ref + nc;
							for (IdxRef = Idx0Ref, IdxSearch = Idx0Sea; IdxRef < IdxRowMaxRef; IdxRef += this->diffX, IdxSearch += search.diffX)
							{
								float gValT = (float) this->pixUChar(IdxRef);
								float gValS = (float) search.pixUChar(IdxSearch);
								sumSearch   += gValS;
								sumsqSearch += gValS * gValS;
								sumMixed    += gValT * gValS;
							}
						}
					}
					else if (this->bpp == 16)
					{
						if (this->unsignedFlag)
						{
							for (unsigned long Idx0Ref = Idx0Ref0; Idx0Ref < IdxMaxRef;  Idx0Ref += this->diffY, Idx0Sea += search.diffY)
							{
								IdxRowMaxRef = Idx0Ref + nc;
								for (IdxRef = Idx0Ref, IdxSearch = Idx0Sea; IdxRef < IdxRowMaxRef; IdxRef += this->diffX, IdxSearch += search.diffX)
								{
									float gValT = (float) this->pixUShort(IdxRef);
									float gValS = (float) search.pixUShort(IdxSearch);
									sumSearch   += gValS;
									sumsqSearch += gValS * gValS;
									sumMixed    += gValT * gValS;
								}
							};
						}
						else
						{
							for (unsigned long Idx0Ref = Idx0Ref0; Idx0Ref < IdxMaxRef;  Idx0Ref += this->diffY, Idx0Sea += search.diffY)
							{
								IdxRowMaxRef = Idx0Ref + nc;;
								for (IdxRef = Idx0Ref, IdxSearch = Idx0Sea; IdxRef < IdxRowMaxRef; IdxRef += this->diffX, IdxSearch += search.diffX)
								{
									float gValT = (float) this->pixShort(IdxRef);
									float gValS = (float) search.pixShort(IdxSearch);
									sumSearch   += gValS;
									sumsqSearch += gValS * gValS;
									sumMixed    += gValT * gValS;
								}
							}
						}
					}
					else if (this->bpp == 32)
					{
						for (unsigned long Idx0Ref = Idx0Ref0; Idx0Ref < IdxMaxRef;  Idx0Ref += this->diffY, Idx0Sea += search.diffY)
						{
							IdxRowMaxRef = Idx0Ref + nc;
							for (IdxRef = Idx0Ref, IdxSearch = Idx0Sea; IdxRef < IdxRowMaxRef; IdxRef += this->diffX, IdxSearch += search.diffX)
							{
								float gValT = this->pixFlt(IdxRef);
								float gValS = search.pixFlt(IdxSearch);
								sumSearch   += gValS;
								sumsqSearch += gValS * gValS;
								sumMixed    += gValT * gValS;
							}
						};
					}

					double varS = sumsqSearch - sumSearch * sumSearch * oneByN;

					if (varS < FLT_EPSILON)
					{
						rhoVector[i] = -1.0;
					}
					else
					{
						double covar = sumMixed - sumTemp * sumSearch * oneByN;
						rhoVector[i] = float(covar / (sigTemp * sqrt(varS)));
					}
				}
			}
		}

	}

	return ret;
};

//===========================================================================================

float *CImageBuffer::getTransformedTemplate(double xRef, double yRef, int w, int h, 
									        const C2DPoint &AxisX, const C2DPoint &AxisY,
											double &mean, double &sdev) const
{
#ifdef _DEBUG
	CImageBuffer buf1;
	this->extractSubWindow(long(floor(xRef + 0.5))- w/2, long(floor(yRef + 0.5)) - h / 2,w,h, buf1);
	buf1.exportToTiffFile("I:\\Projects\\CRC-SI\\tempOriginal.tif",0, false);
#endif

	float *tempImgBuf = 0;
	xRef -= this->offsetX;
	yRef -= this->offsetY;
	mean = 0.0;
	sdev = 0.0;

			
	double dw = double(w - 1) * 0.5;
	double dh = double(h - 1) * 0.5;

	double d1 = dw * AxisX.x, d2 = dh * AxisY.x;
	double d3 = dw * AxisX.y, d4 = dh * AxisY.y;
	double dwt = d1 + d2;
	double dht = d3 + d4;

	double dwt1 =  d1 - d2 + 0.5;
	double dht1 = -d3 + d4 + 0.5;
	
	C2DPoint pMin(xRef - dwt, yRef - dht);
	C2DPoint p;

	if ((pMin.x > 1.0) && (pMin.x + 1.0 < this->getWidth()) && 
		(pMin.y > 1.0) && (pMin.y + 1.0 < this->getHeight()) && 
		(xRef + dwt > 1.0) && (xRef + dwt + 1.0 < this->getWidth()) && 
		(yRef + dht > 1.0) && (yRef + dht + 1.0 < this->getHeight())&& 
		(xRef + dwt1 > 1.0) && (xRef + dwt1 + 1.0 < this->getWidth()) && 
		(yRef + dht1 > 1.0) && (yRef + dht1 + 1.0 < this->getHeight())&& 
		(xRef - dwt1 > 1.0) && (xRef + dwt1 + 1.0 < this->getWidth()) && 
		(yRef - dht1 > 1.0) && (yRef + dht1 + 1.0 < this->getHeight()))
	{
		// start by computing the variance of the reference image
		int np = w * h;
		tempImgBuf = new float[np];

		unsigned long Idx0Ref = 0;
		unsigned long IdxMaxRef = np;
		unsigned long IdxRowMaxRef;

		unsigned long IdxRef;

		double sumTemp = 0.0f, sumsqTemp = 0.0f;
		double oneByN = 1.0 / ((double) np);

		int row, col, index, index1;
		float a1, a2, a3, g00, g10, g01, g11, gInt, dx, dy;

		if (this->bpp == 8)
		{
			for (; Idx0Ref < IdxMaxRef;  Idx0Ref += w, pMin += AxisY)
			{
				IdxRowMaxRef = Idx0Ref + w;
				p.x = pMin.x; p.y = pMin.y;

				for (IdxRef = Idx0Ref; IdxRef < IdxRowMaxRef; IdxRef += 1, p += AxisX)
				{
					row = (int) floor(p.y);
					col = (int) floor(p.x);
					index = row * this->diffY + col * this->diffX;
					dx = float(p.x) - col;
					dy = float(p.y) - row;

					g00 = (float)this->pixUC[index ];		
					g01 = (float)this->pixUC[index + this->diffY];		
					index1 = index + this->diffX;
					g10 = (float)this->pixUC[index1];		
					g11 = (float)this->pixUC[index1 + this->diffY];
					a1 = g10 - g00;
					a2 = g01 - g00;
					a3 = g11 - g00 - a1 - a2;
					gInt = g00 + a1 * dx + a2 * dy + a3 * dy * dx;

					tempImgBuf[IdxRef] = gInt;
					mean += gInt;
					sdev += gInt * gInt;
				}
			}
		}
		else if (this->bpp == 16)
		{
			if (this->unsignedFlag)
			{
				for (; Idx0Ref < IdxMaxRef;  Idx0Ref += w, pMin += AxisY)
				{
					IdxRowMaxRef = Idx0Ref + w;
					p.x = pMin.x; p.y = pMin.y;

					for (IdxRef = Idx0Ref; IdxRef < IdxRowMaxRef; IdxRef += 1, p += AxisX)
					{
						row = (int) floor(p.y);
						col = (int) floor(p.x);
						index = row * this->diffY + col * this->diffX;
						dx = float(p.x) - col;
						dy = float(p.y) - row;

						g00 = (float)this->pixUS[index ];		
						g01 = (float)this->pixUS[index + this->diffY];		
						index1 = index + this->diffX;
						g10 = (float)this->pixUS[index1];		
						g11 = (float)this->pixUS[index1 + this->diffY];
						a1 = g10 - g00;
						a2 = g01 - g00;
						a3 = g11 - g00 - a1 - a2;
						gInt = g00 + a1 * dx + a2 * dy + a3 * dy * dx;

						tempImgBuf[IdxRef] = gInt;
						mean += gInt;
						sdev += gInt * gInt;
					}
				};
			}
			else
			{
				for (; Idx0Ref < IdxMaxRef;  Idx0Ref += w, pMin += AxisY)
				{
					IdxRowMaxRef = Idx0Ref + w;
					p.x = pMin.x; p.y = pMin.y;

					for (IdxRef = Idx0Ref; IdxRef < IdxRowMaxRef; IdxRef += 1, p += AxisX)
					{
						row = (int) floor(p.y);
						col = (int) floor(p.x);
						index = row * this->diffY + col * this->diffX;
						dx = float(p.x) - col;
						dy = float(p.y) - row;

						g00 = (float)this->pixS[index ];		
						g01 = (float)this->pixS[index + this->diffY];		
						index1 = index + this->diffX;
						g10 = (float)this->pixS[index1];		
						g11 = (float)this->pixS[index1 + this->diffY];
						a1 = g10 - g00;
						a2 = g01 - g00;
						a3 = g11 - g00 - a1 - a2;
						gInt = g00 + a1 * dx + a2 * dy + a3 * dy * dx;

						tempImgBuf[IdxRef] = gInt;
						mean += gInt;
						sdev += gInt * gInt;
					}
				}
			}
		}
		else if (this->bpp == 32)
		{
			for (; Idx0Ref < IdxMaxRef;  Idx0Ref += w, pMin += AxisY)
			{
				IdxRowMaxRef = Idx0Ref + w;
				p.x = pMin.x; p.y = pMin.y;

				for (IdxRef = Idx0Ref; IdxRef < IdxRowMaxRef; IdxRef += 1, p += AxisX)
				{
					row = (int) floor(p.y);
					col = (int) floor(p.x);
					index = row * this->diffY + col * this->diffX;
					dx = float(p.x) - col;
					dy = float(p.y) - row;

					g00 = this->pixF[index ];		
					g01 = this->pixF[index + this->diffY];		
					index1 = index + this->diffX;
					g10 = this->pixF[index1];		
					g11 = this->pixF[index1 + this->diffY];
					a1 = g10 - g00;
					a2 = g01 - g00;
					a3 = g11 - g00 - a1 - a2;
					gInt = g00 + a1 * dx + a2 * dy + a3 * dy * dx;

					tempImgBuf[IdxRef] = gInt;
					mean += gInt;
					sdev += gInt * gInt;
				}
			};
		}
		else
		{
			delete [] tempImgBuf;
			tempImgBuf = 0;
		}

		sdev = sdev - mean * mean * oneByN;
		mean *= oneByN;

		if (sdev < FLT_EPSILON)	
		{
			delete [] tempImgBuf;
			tempImgBuf = 0;
		};
		
		if (tempImgBuf)
		{
#ifdef _DEBUG
			CImageBuffer buf(0,0,w,h,1,this->bpp,true);
			if (this->bpp == 8)
			{
				for (int i = 0; i < w * h; ++i) 
				{
					float g = float(floor(tempImgBuf[i] + 0.5));
					if (g < 0) buf.pixUChar(i) = 0;
					else if (g > 255) buf.pixUChar(i) = 255;
					else buf.pixUChar(i) = (unsigned char) g;
				}
			}
			else if (this->bpp == 16)
			{
				for (int i = 0; i < w * h; ++i) 
				{
					float g = float(floor(tempImgBuf[i] + 0.5));
					if (g < 0) buf.pixUShort(i) = 0;
					else if (g > 16000) buf.pixUShort(i) = 16000;
					else buf.pixUShort(i) = (unsigned short) g;
				}
			}
			else
			{
				for (int i = 0; i < w * h; ++i) buf.pixFlt(i) = tempImgBuf[i];
			}

			buf.exportToTiffFile("I:\\Projects\\CRC-SI\\tempRot.tif",0, false);
#endif
			sdev = sqrt(sdev);
		}
	}


	return tempImgBuf;
};

//===========================================================================================

bool CImageBuffer::getCorrelation(const CImageBuffer &search, 
								  double xRef,    double yRef, 
								  double xSeaMin, double ySeaMin,
								  double dxSea,   double dySea, 
								  int dw, int dh, 
								  const C2DPoint &AxisX, const C2DPoint &AxisY,
								  vector<float> &rhoVector, int nSteps) const
{
	bool ret = true;

	int nc = dw + dw + 1;
	int nr = dh + dh + 1;

	double meanTemp, sdevTemp;

	float *tempBuf = getTransformedTemplate(xRef, yRef, nc, nr, AxisX, AxisY, meanTemp, sdevTemp);

	if ((!tempBuf) || (this->bpp != search.bpp) || (this->unsignedFlag != search.unsignedFlag))
	{
		if (tempBuf) delete [] tempBuf;
		ret = false;
	}
	else
	{
		// start by computing the variance of the reference image
		int np = nc * nr;
		double oneByN = 1.0 / ((double) np);
	
		// the standard deviation of the reference image has been computed, and it is larger than 0
#ifdef _DEBUG
		if (tempBuf)
		{
				CImageBuffer buf1;

				int xMinSea = int(floor(xSeaMin + 0.5));
				int yMinSea = int(floor(ySeaMin + 0.5));
				int xMaxSea = xMinSea;
				int yMaxSea = yMinSea;
				int xMinSea2 = int(floor(xSeaMin + nSteps * dxSea + 0.5));
				int yMinSea2 = int(floor(ySeaMin + nSteps * dySea + 0.5));
				int xMaxSea2 = xMinSea2;
				int yMaxSea2 = yMinSea2;

				if (xMinSea2 < xMinSea) xMinSea = xMinSea2;
				if (xMaxSea2 > xMaxSea) xMaxSea = xMaxSea2;
				if (yMinSea2 < yMinSea) yMinSea = yMinSea2;
				if (yMaxSea2 > yMaxSea) yMaxSea = yMaxSea2;
				xMinSea -= dw;
				yMinSea -= dh;
				xMaxSea += dw + 1;
				yMaxSea += dh + 1;
				if (xMinSea < search.getOffsetX()) xMinSea = search.getOffsetX();
				if (yMinSea < search.getOffsetY()) yMinSea = search.getOffsetY();
				if (xMaxSea > search.getOffsetX() + search.getWidth()  - 1) xMaxSea = search.getOffsetX() + search.getWidth()  - 1;
				if (yMaxSea > search.getOffsetY() + search.getHeight() - 1) yMaxSea = search.getOffsetY() + search.getHeight() - 1;

				if (search.extractSubWindow(xMinSea, yMinSea, xMaxSea - xMinSea + 1, yMaxSea - yMinSea + 1, buf1))
					buf1.exportToTiffFile("I:\\Projects\\CRC-SI\\search.tif",0, false);
		}
#endif
		for (int i = 0; i < nSteps; ++i, xSeaMin += dxSea,  ySeaMin += dySea)
		{
			int cMinSea = int(floor(xSeaMin + 0.5)) - search.offsetX - dw;
			int cMaxSea = cMinSea + nc;

			int rMinSea = int(floor(ySeaMin + 0.5)) - search.offsetY - dh;
			int rMaxSea = rMinSea + nr;

			if ((cMinSea < 0) || (cMaxSea > search.getWidth()) ||
				(rMinSea < 0) || (rMaxSea > search.getHeight()))
			{
				rhoVector[i] = -1.0;
			}
			else
			{
				double sumSearch = 0.0f, sumsqSearch = 0.0f, sumMixed = 0.0f;
				unsigned long IdxSea0   = search.getIndex(rMinSea, cMinSea);
				unsigned long IdxSeaMax = search.getIndex(rMinSea + nr - 1, cMinSea + nc);
				unsigned long IdxSearch, IdxRowMaxSea;
				unsigned long IdxRef = 0;

				if (this->bpp == 8)
				{
					for (; IdxSea0 < IdxSeaMax;  IdxSea0 += search.diffY)
					{
						IdxRowMaxSea = IdxSea0 + nc;
						for (IdxSearch = IdxSea0; IdxSearch < IdxRowMaxSea; IdxSearch += search.diffX, IdxRef++)
						{
							float gValT = tempBuf[IdxRef];
							float gValS = (float) search.pixUChar(IdxSearch);
							sumSearch   += gValS;
							sumsqSearch += gValS * gValS;
							sumMixed    += gValT * gValS;
						}
					}
				}
				else if (this->bpp == 16)
				{
					if (this->unsignedFlag)
					{
						for (; IdxSea0 < IdxSeaMax;  IdxSea0 += search.diffY)
						{
							IdxRowMaxSea = IdxSea0 + nc;
							for (IdxSearch = IdxSea0; IdxSearch < IdxRowMaxSea; IdxSearch += search.diffX, IdxRef++)
							{
								float gValT = tempBuf[IdxRef];
								float gValS = (float) search.pixUShort(IdxSearch);
								sumSearch   += gValS;
								sumsqSearch += gValS * gValS;
								sumMixed    += gValT * gValS;
							}
						};
					}
					else
					{
						for (; IdxSea0 < IdxSeaMax;  IdxSea0 += search.diffY)
						{
							IdxRowMaxSea = IdxSea0 + nc;
							for (IdxSearch = IdxSea0; IdxSearch < IdxRowMaxSea; IdxSearch += search.diffX, IdxRef++)
							{
								float gValT = tempBuf[IdxRef];
								float gValS = (float) search.pixShort(IdxSearch);
								sumSearch   += gValS;
								sumsqSearch += gValS * gValS;
								sumMixed    += gValT * gValS;
							}
						}
					}
				}
				else if (this->bpp == 32)
				{
					for (; IdxSea0 < IdxSeaMax;  IdxSea0 += search.diffY)
					{
						IdxRowMaxSea = IdxSea0 + nc;
						for (IdxSearch = IdxSea0; IdxSearch < IdxRowMaxSea; IdxSearch += search.diffX, IdxRef++)
						{
							float gValT = tempBuf[IdxRef];
							float gValS = search.pixFlt(IdxSearch);
							sumSearch   += gValS;
							sumsqSearch += gValS * gValS;
							sumMixed    += gValT * gValS;
						}
					};
				}

				double varS = sumsqSearch - sumSearch * sumSearch * oneByN;

				if (varS < FLT_EPSILON)
				{
					rhoVector[i] = -1.0;
				}
				else
				{
					double covar = sumMixed - meanTemp * sumSearch;
					rhoVector[i] = float(covar / (sdevTemp * sqrt(varS)));
				}
			}
		}

		delete [] tempBuf;
	}

	return ret;
};

//===========================================================================================

float CImageBuffer::getCorrelation(const CImageBuffer &buf) const
{
	double sum1 = 0.0f, sum2 = 0.0f, sumsq1 = 0.0f, sumsq2 = 0.0f, sum12 = 0.0f;
	int cmin = this->offsetX;
	if (buf.offsetX > cmin) cmin = buf.offsetX;
	int rmin = this->offsetY;
	if (buf.offsetY > rmin) rmin = buf.offsetY;
	int cmax = this->offsetX + this->width;
	int hlp = buf.offsetX + buf.width;
	if (hlp < cmax) cmax = hlp;
	int rmax = this->offsetY + this->height;
	hlp = buf.offsetY + buf.height;
	if (hlp < rmax) rmax = hlp;
	if ((rmax <= rmin) || (cmax <= cmin)) return -1;

	int nc = cmax - cmin;
	int nr = rmax - rmin;
	unsigned long thisIdx0   = this->getIndex(rmin - this->offsetY, cmin - this->offsetX);
	unsigned long bufIdx0    = buf.getIndex(rmin - buf.offsetY, cmin - buf.offsetX);
	unsigned long thisIdxMax = buf.getIndex(rmax - this->offsetY - 1, cmax - this->offsetX);

	if ((this->bpp == 8) && buf.bpp == 8)
	{
		for (; thisIdx0 < thisIdxMax;  thisIdx0 += this->diffY, bufIdx0 += buf.diffY)
		{
			unsigned long thisIdxRowMax = thisIdx0 + nc;
			for (unsigned long thisIdx = thisIdx0, bufIdx = bufIdx0; thisIdx < thisIdxRowMax; thisIdx += this->diffX, bufIdx += buf.diffX)
			{
				float gVal1 = (float) this->pixUChar(thisIdx);
				float gVal2 = (float) buf.pixUChar(bufIdx);
				sum1   += gVal1;
				sum2   += gVal2;
				sumsq1 += gVal1 * gVal1;
				sumsq2 += gVal2 * gVal2;
				sum12  += gVal1 * gVal2;
			}
		}
	}
	else if ((this->bpp == 16) && buf.bpp == 16)
	{
		if (this->unsignedFlag && buf.unsignedFlag)
		{
			for (; thisIdx0 < thisIdxMax;  thisIdx0 += this->diffY, bufIdx0 += buf.diffY)
			{
				unsigned long thisIdxRowMax = thisIdx0 + nc;
				for (unsigned long thisIdx = thisIdx0, bufIdx = bufIdx0; thisIdx < thisIdxRowMax; thisIdx += this->diffX, bufIdx += buf.diffX)
				{
					float gVal1 = (float) this->pixUShort(thisIdx);
					float gVal2 = (float) buf.pixUShort(bufIdx);
					sum1   += gVal1;
					sum2   += gVal2;
					sumsq1 += gVal1 * gVal1;
					sumsq2 += gVal2 * gVal2;
					sum12  += gVal1 * gVal2;
				}
			};
		}
		else if ((!this->unsignedFlag) && (!buf.unsignedFlag))
		{
			for (; thisIdx0 < thisIdxMax;  thisIdx0 += this->diffY, bufIdx0 += buf.diffY)
			{
				unsigned long thisIdxRowMax = thisIdx0 + nc;
				for (unsigned long thisIdx = thisIdx0, bufIdx = bufIdx0; thisIdx < thisIdxRowMax; thisIdx += this->diffX, bufIdx += buf.diffX)
				{
					float gVal1 = (float) this->pixShort(thisIdx);
					float gVal2 = (float) buf.pixShort(bufIdx);
					sum1   += gVal1;
					sum2   += gVal2;
					sumsq1 += gVal1 * gVal1;
					sumsq2 += gVal2 * gVal2;
					sum12  += gVal1 * gVal2;
				}
			}
		}
	}
	else if ((this->bpp == 32) && (buf.bpp == 32))
	{
		for (; thisIdx0 < thisIdxMax;  thisIdx0 += this->diffY, bufIdx0 += buf.diffY)
		{
			unsigned long thisIdxRowMax = thisIdx0 + nc;
			for (unsigned long thisIdx = thisIdx0, bufIdx = bufIdx0; thisIdx < thisIdxRowMax; thisIdx += this->diffX, bufIdx += buf.diffX)
			{
				float gVal1 = this->pixFlt(thisIdx);
				float gVal2 = buf.pixFlt(bufIdx);
				sum1   += gVal1;
				sum2   += gVal2;
				sumsq1 += gVal1 * gVal1;
				sumsq2 += gVal2 * gVal2;
				sum12  += gVal1 * gVal2;
			}
		};
	}
	else return -1;

	double oneByN = 1.0 / ((double) nc * nr);
	double var1 = sumsq1 - sum1 * sum1 * oneByN;
	double var2 = sumsq2 - sum2 * sum2 * oneByN;
	float rho = -1.0f;
	if ((var1 > FLT_EPSILON) && (var2 > FLT_EPSILON)) 
	{
		double covar = sum12 - sum1 * sum2 * oneByN;
		rho    = float(covar / sqrt(var1 * var2));
	}

	return rho;
};

//===========================================================================================

bool CImageBuffer::resample(const CImageBuffer &input, int factor)
{
	bool ret = this->set(input.offsetX * factor, input.offsetY * factor, 
		                 input.width * factor, input.height * factor, 
						 input.bands, input.bpp, input.unsignedFlag);
	if (ret)
	{
		unsigned long outDiffY = factor * this->diffY;
		unsigned long outDiffX = factor * this->diffX;
		for (int bnd = 0; bnd < this->bands; ++bnd)
		{
			unsigned long firstIn      = bnd;
			unsigned long LastInRowIn  = bnd + input.diffY;
			unsigned long lastIn       = input.getNGV() + bnd;
			unsigned long firstOut     = bnd;

			if ((this->bpp == 1) || (this->bpp == 8))
			{
				for(;LastInRowIn <= lastIn; firstIn += input.diffY, LastInRowIn += input.diffY, firstOut += outDiffY)
				{
					unsigned long in = firstIn;
					unsigned long out = firstOut;
					for (; in < LastInRowIn; in += input.diffX, out += outDiffX)
					{
						unsigned char val = input.pixUChar(in);
						unsigned long outFacMin = out;
						unsigned long outFacMax = out + outDiffY;
						unsigned long outFacRowMax = out + outDiffX;

						for (; outFacRowMax < outFacMax; outFacRowMax += this->diffY, outFacMin += this->diffY)
						{
							for (unsigned long Fac = outFacMin; Fac < outFacRowMax; Fac += this->diffX)       
							{
								this->pixUChar(Fac) = val;
							};
						};
					};
				};
			}
			else if (this->bpp == 16)
			{
				if (this->unsignedFlag)
				{
					for(;LastInRowIn < lastIn; firstIn += input.diffY, LastInRowIn += input.diffY, firstOut += outDiffY)
					{
						unsigned long in = firstIn;
						unsigned long out = firstOut;
						for (; in < LastInRowIn; in += input.diffX, out += outDiffX)
						{
							unsigned short val = input.pixUShort(in);
							unsigned long outFacMin = out;
							unsigned long outFacMax = out + outDiffY;
							unsigned long outFacRowMax = out + outDiffX;

							for (; outFacRowMax < outFacMax; outFacRowMax += this->diffY, outFacMin += this->diffY)
							{
								for (unsigned long Fac = outFacMin; Fac < outFacRowMax; Fac += this->diffX)       
								{
									this->pixUShort(Fac) = val;
								};
							};
						};
					};
				}
				else
				{
					for(;LastInRowIn < lastIn; firstIn += input.diffY, LastInRowIn += input.diffY, firstOut += outDiffY)
					{
						unsigned long in = firstIn;
						unsigned long out = firstOut;
						for (; in < LastInRowIn; in += input.diffX, out += outDiffX)
						{
							short val = input.pixShort(in);
							unsigned long outFacMin = out;
							unsigned long outFacMax = out + outDiffY;
							unsigned long outFacRowMax = out + outDiffX;

							for (; outFacRowMax < outFacMax; outFacRowMax += this->diffY, outFacMin += this->diffY)
							{
								for (unsigned long Fac = outFacMin; Fac < outFacRowMax; Fac += this->diffX)       
								{
									this->pixShort(Fac) = val;
								};
							};
						};
					};
				}
			}
			else if (this->bpp == 32)
			{
				for(;LastInRowIn < lastIn; firstIn += input.diffY, LastInRowIn += input.diffY, firstOut += outDiffY)
				{
					unsigned long in = firstIn;
					unsigned long out = firstOut;
					for (; in < LastInRowIn; in += input.diffX, out += outDiffX)
					{
						float val = input.pixFlt(in);
						unsigned long outFacMin = out;
						unsigned long outFacMax = out + outDiffY;
						unsigned long outFacRowMax = out + outDiffX;

						for (; outFacRowMax < outFacMax; outFacRowMax += this->diffY, outFacMin += this->diffY)
						{
							for (unsigned long Fac = outFacMin; Fac < outFacRowMax; Fac += this->diffX)       
							{
								this->pixFlt(Fac) = val;
							};
						};
					};
				};
			}
			else ret = false;
		};
	};

	return ret;
};

//===========================================================================================

bool CImageBuffer::markEdgePixels(CImageBuffer &edgeBuf) const
{
	bool ret = true;

	if ((this->bpp > 8) || (this->bands > 1)) ret = false;
	else ret = edgeBuf.set(this->offsetX, this->offsetY, this->width, this->height, 
		                   this->bands,   this->bpp,     this->unsignedFlag, 0);
	if (ret)
	{
		int delta = this->diffY - this->diffX;
		for(unsigned long indexMin = this->diffX + this->diffY; indexMin < this->nGV; indexMin += this->diffY)
		{
			unsigned long indexMaxRow = indexMin + delta;
			for (unsigned long index = indexMin; index < indexMaxRow; index += this->diffX)
			{
				if (this->pixUChar(index))
				{
					if ((!this->pixUChar(index - this->diffX)) || 
						(!this->pixUChar(index - this->diffY))) edgeBuf.pixUChar(index) = 1;
				}
				else
				{
					if ((this->pixUChar(index - this->diffX)) || 
						(this->pixUChar(index - this->diffY))) edgeBuf.pixUChar(index) = 1;
				}
			}
		}
	}

	return ret;
};

//===========================================================================================	

bool CImageBuffer::getContour(CXYContour &contour, bool useSecondDegree, bool checkFourFirst)
{
	if ((this->bpp > 8) || (this->bands > 1)) return false;

	unsigned long uStart; 
	contour.deleteContour();

	for (uStart = 0;  (uStart < this->nGV) && (this->pixUChar(uStart) < 1); ++uStart);

	if (uStart >= this->nGV) return false;

	// indices     0   1   2   3   4   5   6  7  8  9  10  11  12  13  14  15  16  17  18  19  20 21 22 23 24 
	long dr[25] = {0,  1,  1,  0, -1, -1, -1, 0, 1, 2,  2,  2,  1,  0, -1, -2, -2, -2, -2, -2, -1, 0, 1, 2, 2};
	long dc[25] = {0,  0, -1, -1, -1,  0,  1, 1, 1, 0, -1, -2, -2, -2, -2, -2, -1,  0,  1,  2,  2, 2, 2, 2, 1};
	long prevSecond[25] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 6, 7, 7, 7, 8, 1};
	long r, c, bnd;
    long delta[25];

	delta[0]  =  0;
	delta[1]  =  this->diffY;
	delta[8]  =  this->diffX + this->diffY;
	delta[9]  =  delta[1] + this->diffY;
	delta[10] =  delta[9]  - this->diffX;
	delta[11] =  delta[10] - this->diffX;
	delta[12] =  delta[11] - this->diffY;
	delta[13] =  delta[12] - this->diffY;
	delta[14] =  delta[13] - this->diffY;
	delta[15] =  delta[14] - this->diffY;
	delta[16] =  delta[15] + this->diffX;
	delta[17] =  delta[16] + this->diffX;
	delta[18] =  delta[17] + this->diffX;
	delta[19] =  delta[18] + this->diffX;
	delta[20] =  delta[19] + this->diffY;
	delta[21] =  delta[20] + this->diffY;
	delta[22] =  delta[21] + this->diffY;
	delta[23] =  delta[22] + this->diffY;
	delta[24] =  delta[23] - this->diffX;

	if (!checkFourFirst)
	{
		delta[2]  =  this->diffY - this->diffX;
		delta[3]  = -this->diffX;
		delta[4]  = -this->diffX - this->diffY;
		delta[5]  = -this->diffY;
		delta[6]  = -this->diffY + this->diffX;
		delta[7]  =  this->diffX;
		
		// local neighbourhood around central pixel:
		// ---------------------
		// |15 |16 |17 |18 |19 |
		// ---------------------
		// |14 | 4 | 5 | 6 |20 |
		// ---------------------
		// |13 | 3 | 0 | 7 |21 |  ---> c
		// ---------------------
		// |12 | 2 | 1 | 8 |22 |
		// ---------------------
		// |11 |10 | 9 |24 |23 |
		// ---------------------
		//           |
		//           V
		//           r
	}
	else
	{
		dr[2] =  0;   dc[2] = -1; 
		dr[3] = -1;   dc[3] =  0;
		dr[4] =  0;   dc[4] =  1;
		dr[5] =  1;   dc[5] = -1;
		dr[6] = -1;   dc[6] = -1;
		dr[7] = -1;   dc[7] =  1;
		prevSecond[ 5] = 2;  prevSecond[ 6] = 3;  prevSecond[ 7] = 4;
		prevSecond[ 8] = 1;  prevSecond[10] = 2;  prevSecond[11] = 2;
		prevSecond[12] = 2;  prevSecond[13] = 2;  prevSecond[14] = 3;
		prevSecond[15] = 3;  prevSecond[16] = 3;  prevSecond[17] = 3;
		prevSecond[18] = 4;  prevSecond[19] = 4;  prevSecond[20] = 4;
		prevSecond[21] = 4;  prevSecond[22] = 1;  prevSecond[23] = 1;
		prevSecond[24] = 1;
	
		delta[2]  = -this->diffX;
		delta[3]  = -this->diffY;
		delta[4]  =  this->diffX;
		delta[5]  = -this->diffX + this->diffY;
		delta[6]  = -this->diffX - this->diffY;
		delta[7]  =  this->diffX - this->diffY;
		
		// local neighbourhood around central pixel:
		// ---------------------
		// |15 |16 |17 |18 |19 |
		// ---------------------
		// |14 | 6 | 3 | 7 |20 |
		// ---------------------
		// |13 | 2 | 0 | 4 |21 |  ---> c
		// ---------------------
		// |12 | 5 | 1 | 8 |22 |
		// ---------------------
		// |11 |10 | 9 |24 |23 |
		// ---------------------
		//           |
		//           V
		//           r

	}

	int maxNeigh = useSecondDegree ? 25 : 9;
	this->getRowColBand(uStart, r,c,bnd);

	C2DPoint P(c, r);
	contour.addPoint(P);
	
	int prevI = 1;

	do
	{
		this->pixUC[uStart] = 0;

		bool found = false;

		for (int i = prevI; i < maxNeigh; ++i)
		{
			long r1 = r + dr[i];
			long c1 = c + dc[i];
			if ((r1 >= 0) && (r1 < this->height) && 
				(c1 >= 0) && (c1 < this->width))
			{
				long u = uStart + delta[i];
				if (this->pixUC[u])
				{
					r = r1;
					c = c1;
					uStart = u;
					P.x = c;
					P.y = r;
					contour.addPoint(P);
					found = true;
					prevI = i - 1;
					if (prevI < 1) prevI = maxNeigh - 1;
					prevI = prevSecond[prevI];
					i = maxNeigh;
				}
			}
		}
		if (!found)
		{
			for (int i = 1; i < prevI; ++i)
			{
				long r1 = r + dr[i];
				long c1 = c + dc[i];
				if ((r1 >= 0) && (r1 < this->height) && 
					(c1 >= 0) && (c1 < this->width))
				{
					long u = uStart + delta[i];
					if (this->pixUC[u])
					{
						r = r1;
						c = c1;
						uStart = u;
						P.x = c;
						P.y = r;
						contour.addPoint(P);
						found = true;
						prevI = i - 1;
						if (prevI < 1) prevI = maxNeigh - 1;
						prevI = prevSecond[prevI];
						i = maxNeigh;
					}
				}
			}
		}
	} while (this->pixUC[uStart] > 0);

	return (contour.getPointCount() > 0);
};

//===========================================================================================

bool CImageBuffer::getContour4Segmentation(CXYContour &contour, const CImageBuffer *overallImg)
{
	if ((this->bpp > 8) || (this->bands > 1)) return false;

	unsigned long uStart; 
	contour.deleteContour();

	for (uStart = 0;  (uStart < this->nGV) && (this->pixUChar(uStart) < 1); ++uStart);

	if (uStart >= this->nGV) return false;

	// local neighbourhood around central pixel:
	long dr[5] = {0,  1,   0, -1,  0};
	long dc[5] = {0,  0,  -1,  0,  1};
	long r, c, bnd;
    long delta[5];
	delta[0] =  0;
	delta[1] =  this->diffY;
	delta[2] = -this->diffX;
	delta[3] = -this->diffY;
	delta[4] =  this->diffX;
	    
	// -------------
	// |   | 3 |   |
	// |------------
	// | 2 | 0 | 4 | ---> c
	// |------------
	// |   | 1 |   |
	// |------------
	//       |
	//       V
	//       r

	this->getRowColBand(uStart, r,c,bnd);	//calculation of r,c and band by given index
	C2DPoint P(c + this->offsetX, r + this->offsetY);	//generate point P in image coordinate system
	if (overallImg) P.id = overallImg->getIndex(long(P.y - overallImg->offsetY), long(P.x - overallImg->offsetX));
					
	contour.addPoint(P);					//add point P to the contour
	
	int prevI = 1;

	//definition 4 features
	int sum = 0;
	int product = 0;
		

	do
	{
		this->pixUC[uStart] = 0;

		bool found = false;

		for (int i = prevI; i < 5; ++i)		//looking in 9er neighbourhood
		{
			long r1 = r + dr[i];			//translation frm local neigh coor. sys. in image coor. sys. (r)
			long c1 = c + dc[i];			//translation frm local neigh coor. sys. in image coor. sys. (c)

			if ((r1 >= 0) && (r1 < this->height) && //when r,c not out of the range of the image coor. sys.
				(c1 >= 0) && (c1 < this->width))
			{
				long u = uStart + delta[i];	

				if (this->pixUC[u])			//check neigh pixel = contour pixel?
				{							//yes:
					r = r1;						//add point to contour
					c = c1;
					uStart = u;
					P.x = c + this->offsetX;
					P.y = r + this->offsetY;
					if (overallImg) P.id = overallImg->getIndex(long(P.y - overallImg->offsetY), long(P.x - overallImg->offsetX));
					contour.addPoint(P);

					found = true;				//found = true
					prevI = i - 1;
					if (prevI < 1) prevI = 4;	//new prev (prev should provid that the same direction is to prefer):
					i = 5;						//no other neigh will be checked (interruption when a countor pixel was found)
				}
			}
		}
		if (!found)							//in case a contour pixel wasn't found (2 accelerate the process):
		{
			for (int i = 1; i < prevI; ++i)	//check of all previous neigh pixels
			{
				long r1 = r + dr[i];
				long c1 = c + dc[i];

				if ((r1 >= 0) && (r1 < this->height) && 
					(c1 >= 0) && (c1 < this->width))
				{
					long u = uStart + delta[i];

					if (this->pixUC[u])
					{
						r = r1;
						c = c1;
						uStart = u;
						P.x = c + this->offsetX;
						P.y = r + this->offsetY;
						if (overallImg) P.id = overallImg->getIndex(long(P.y - overallImg->offsetY), long(P.x - overallImg->offsetX));
						contour.addPoint(P);
						found = true;
						prevI = i - 1;
						if (prevI < 1) prevI = 4;
						i = 5;
					}
				}
			}
		}
	} while (this->pixUC[uStart] > 0);

	return (contour.getPointCount() > 0);
};

//***************************************************************************************************************

bool CImageBuffer::getContours(CXYContourArray &contours, int neighbourhood, const CImageBuffer *overallImg, 
							   bool useSecondDegree, bool checkFourFirst)
{
	contours.RemoveAll();

	CXYContour *contour = contours.Add();

	if (neighbourhood == 4)
	{
		while (getContour4Segmentation(*contour, overallImg))
		{
			if (contour->getPointCount() > 2) contour = contours.Add();
		}
	}
	else
	{
		while (getContour(*contour, useSecondDegree, checkFourFirst))
		{
			if (contour->getPointCount() > 2) contour = contours.Add();
		}
	}

	contours.RemoveAt(contours.GetSize() - 1);

	return (contours.GetSize() > 0);
};

//***************************************************************************************************************

void CImageBuffer::fillByValue(const CXYPolyLine &poly, float value)
{
	long xmax = offsetX + width;
	long ymax = offsetY + height - 1;

	if (this->bpp <= 8)
	{
		unsigned char val = (unsigned char) value;
		for (long xmin = offsetX; xmin < xmax; ++xmin)
		{
			doubles intersectionVec;
			long nIntersections = poly.getIntersections(xmin, intersectionVec);

			if (nIntersections)
			{
				for (long  u = 0; u < nIntersections; u += 2)
				{     
					assert((unsigned long)(u + 1) < intersectionVec.size());
					long ymin   = long(floor(intersectionVec[u] + 0.5f));
					long yupper = long(floor(intersectionVec[u + 1] + 0.5f));
					if (yupper > ymax)    yupper = ymax;
					if (ymin   < offsetY) ymin   = offsetY;
					if ((yupper >= offsetY)  && (ymin  <= ymax))
					{
						for (unsigned long j = this->getIndex(ymin-offsetY, xmin-offsetX); ymin <= yupper; ++ymin, j += diffY)
						{
							assert(j < nGV);
							for (int k = 0; k < this->bands; ++k)
								this->pixUC[j + k] = val;
						};
					}
				};
			};
		};
	}
	else if (this->bpp == 16)
	{
		if (this->unsignedFlag)
		{
			unsigned short val = (unsigned short) value;

			for (long xmin = offsetX; xmin < xmax; ++xmin)
			{
				doubles intersectionVec;
				long nIntersections = poly.getIntersections(xmin, intersectionVec);

				if (nIntersections)
				{
					for (long  u = 0; u < nIntersections; u += 2)
					{     
						assert((unsigned long)(u + 1) < intersectionVec.size());
						long ymin   = long(floor(intersectionVec[u] + 0.5f));
						long yupper = long(floor(intersectionVec[u + 1] + 0.5f));
						if (yupper > ymax)    yupper = ymax;
						if (ymin   < offsetY) ymin   = offsetY;
						if ((yupper >= offsetY)  && (ymin  <= ymax))
						{
							for (unsigned long j = this->getIndex(ymin-offsetY, xmin-offsetX); ymin <= yupper; ++ymin, j += diffY)
							{
								assert(j < nGV);
								for (int k = 0; k < this->bands; ++k)
									this->pixUS[j + k] = val;
							};
						};
					};
				};
			};
		}
		else
		{
			short val = (short) value;

			for (long xmin = offsetX; xmin < xmax; ++xmin)
			{
				doubles intersectionVec;
				long nIntersections = poly.getIntersections(xmin, intersectionVec);

				if (nIntersections)
				{
					for (long  u = 0; u < nIntersections; u += 2)
					{     
						assert((unsigned long)(u + 1) < intersectionVec.size());
						long ymin   = long(floor(intersectionVec[u] + 0.5f));
						long yupper = long(floor(intersectionVec[u + 1] + 0.5f));
						if (yupper > ymax)    yupper = ymax;
						if (ymin   < offsetY) ymin   = offsetY;
						if ((yupper >= offsetY)  && (ymin  <= ymax))
						{
							for (unsigned long j = this->getIndex(ymin-offsetY, xmin-offsetX); ymin <= yupper; ++ymin, j += diffY)
							{
								assert(j < nGV);
								for (int k = 0; k < this->bands; ++k)
									this->pixS[j + k] = val;
							};
						}
					};
				};
			};
		}
	}
	else if (this->bpp == 32)
	{
		for (long xmin = offsetX; xmin < xmax; ++xmin)
		{
			doubles intersectionVec;
			long nIntersections = poly.getIntersections(xmin, intersectionVec);

			if (nIntersections)
			{
				for (long  u = 0; u < nIntersections; u += 2)
				{     
					assert((unsigned long)(u + 1) < intersectionVec.size());
					assert((unsigned long)(u + 1) < intersectionVec.size());
					long ymin   = long(floor(intersectionVec[u] + 0.5f));
					long yupper = long(floor(intersectionVec[u + 1] + 0.5f));
					if (yupper > ymax)    yupper = ymax;
					if (ymin   < offsetY) ymin   = offsetY;
					if ((yupper >= offsetY)  && (ymin  <= ymax))
					{
						for (unsigned long j = this->getIndex(ymin-offsetY, xmin-offsetX); ymin <= yupper; ++ymin, j += diffY)
						{
							assert(j < nGV);
							for (int k = 0; k < this->bands; ++k)
								this->pixF[j + k] = value;
						};
					}
				};
			};
		};
	}
};