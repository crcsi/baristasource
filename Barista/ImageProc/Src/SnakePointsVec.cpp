#include "SnakePointsVec.h"
#include "ProtocolHandler.h"

CSnakePointsVec::CSnakePointsVec()
{
};

//============================================================================

CSnakePointsVec::~CSnakePointsVec()
{
};


//============================================================================

void CSnakePointsVec::initializeSnakeVector(CXYPointArray snakeStartingPoints, C2DPoint Offset, SnakeExtractionParameters extractionParameters, const CImageBuffer *gradX, const CImageBuffer *gradY)
{	
	/*  ------------------------->x
	    |		
		|				|\
		|		   phi /| \
        |			  /	x A|
        |			  -/|  /	
		|			  / | /
		|			 /  |/
		|		    /d  |
		|		   /    V
		|	    B x
		V
		y

			xB = xA + d sin(phi)
			yB = yA + d cos(phi)

			phi = atan2 ((yB-yA),(xB-xA)) = tmpDirection
			d = sqrt (dx^2 + dy^2) = tmpDistanz = distanz between A and B

		*/
	
	unsigned int s  = extractionParameters.getSnakeSpace();

	CXYPoint tmpStartPoint, tmpEndPoint;
	double tmpDirection;
	double tmpDistanz;
	unsigned int numberOfVertiecesBetweenPoints; //number of points (x in the drwaing below)
	unsigned int index = 0;

	/*         A o-----x-----x-----x-----x---o B
				    s     s	    s     s         

					where	A and B... points given from the operator
							x... new interpolated points (vertices)
							s... snake space (distanz between the vertices)     */

	unsigned int counter = snakeStartingPoints.GetSize()-1, i, j,k;

	if(snakeStartingPoints) 
	{

		for(i = 0; i < counter; i++, index++) //caclulation of vertices between two points
		{
			tmpStartPoint = snakeStartingPoints.getPoint(i);
			tmpEndPoint = snakeStartingPoints.getPoint(i+1);
			tmpDistanz = tmpStartPoint.getDistance(tmpEndPoint);
			tmpDirection = tmpStartPoint.getDirection(tmpEndPoint);
			
			addSegment(tmpStartPoint,index, gradX, gradY, Offset, &extractionParameters); //add the startpoint to the vector

			numberOfVertiecesBetweenPoints = unsigned int (tmpDistanz)/s;

			for(j = 0; j < numberOfVertiecesBetweenPoints; j++, ++index) // add the vertices between the points
			{
				addSegment(tmpStartPoint, index, tmpDirection, (j+1) * s, gradX, gradY, Offset, &extractionParameters);	
			}
		}
		if (!extractionParameters.getOpenSnakeMode()) //if the snake is closed, add points between the last and the first vertiece
		{
			tmpStartPoint = snakeStartingPoints.getPoint(counter);
			tmpEndPoint = snakeStartingPoints.getPoint(0);

			tmpDistanz = tmpStartPoint.getDistance(tmpEndPoint);
			tmpDirection = tmpStartPoint.getDirection(tmpEndPoint);
			numberOfVertiecesBetweenPoints = unsigned int (tmpDistanz)/s;

			for(k = 0; k < numberOfVertiecesBetweenPoints; k++, index++)
			{
				addSegment(&tmpStartPoint,index, tmpDirection, (k+1) * s, gradX, gradY, Offset, &extractionParameters);	
			}
		}
		else addSegment(snakeStartingPoints.getPoint(counter), index+1,gradX, gradY, Offset, &extractionParameters); //add the last point to the vector in case the snake is open

	}
};

//============================================================================
bool CSnakePointsVec::updateSnakeVector(vector<CXYPoint> *NewSnakeVec, C2DPoint Offset, SnakeExtractionParameters extractionParameters, const CImageBuffer *gradX, const CImageBuffer *gradY)
{
	eSnakeMode Mode = extractionParameters.getMode();
	unsigned int s  = extractionParameters.getSnakeSpace();
	float gamma     = extractionParameters.getGamma();
	float kappa     = extractionParameters.getKappa();
	float balloonForce = extractionParameters.getKa();

	unsigned int finishSize = NewSnakeVec->size();
	this->clear();
	//bool ret = false;
	
	for(unsigned int i = 0; i < NewSnakeVec->size(); i++)
	{
		if (((*NewSnakeVec)[i].x < 0) || ((*NewSnakeVec)[i].y < 0)				//is the new points anymore in the ROI?
			|| ((*NewSnakeVec)[i].y + 2 - Offset.y  >=  (double) gradX->getHeight())
			|| ((*NewSnakeVec)[i].x + 2 - Offset.x  >=  (double) gradX->getWidth()) 
			|| ((*NewSnakeVec)[i].y + 2 - Offset.y  >=  (double) gradY->getHeight()) 
			|| ((*NewSnakeVec)[i].x + 2 - Offset.x  >=  (double) gradY->getWidth())) 
		{
			
			return false;

		}else 
		{
			if (i == 0 || i == (NewSnakeVec->size()-1))	//first and last point will never deleted because of snake space
			{
				if(this->addSegment((*NewSnakeVec)[i], i, gradX, gradY, Offset, &extractionParameters))
				{
				}
				else return false;
			}
			else 	//check Snake Spacing for all other points
			{
				if(((*NewSnakeVec)[i].getDistance((*NewSnakeVec)[i+1]) < (double(s) / 2.0f)) && (finishSize > 5) && (i < (NewSnakeVec->size()- 2)))
				{
					i++;				//if the distance to small, skip this point but only if they number of points will not be smaller than 5
					finishSize -= 1;
				}
				
				if(this->addSegment((*NewSnakeVec)[i],i, gradX, gradY, Offset, &extractionParameters))		//create a new pSnakeVector with the results
				{
				}
				else return false;

				if((*NewSnakeVec)[i].getDistance((*NewSnakeVec)[i+1]) > double(2.0f * s))				//if the distance to big, add a new point
				{
					double direction = (*NewSnakeVec)[i].getDirection((*NewSnakeVec)[i+1]);

					if(this->addSegment((*NewSnakeVec)[i], i, direction, s, gradX, gradY, Offset, &extractionParameters)) finishSize += 1;
					else return false;
				}

				if(((*NewSnakeVec)[i].getDistance((*NewSnakeVec)[i+1]) < (double(s) / 2.0f)) && (finishSize == 5) && (i < (NewSnakeVec->size()- 2)))
					return false;

			}
		}	
	}

	return true;
}

//============================================================================
bool CSnakePointsVec::addSegment(const CXYPoint point,const unsigned int index, const double phi, const unsigned int d, const CImageBuffer *gradX, const CImageBuffer *gradY, C2DPoint Offset, const SnakeExtractionParameters  *Parameters)
{
	bool ret = false;
	CSnakePoint newPoint;

	newPoint.setIndex(index);
	newPoint.setX((point.x + d*sin(phi)));
	newPoint.setY((point.y + d*cos(phi)));
	newPoint.initGradientValues(gradX, gradY, Offset);
	if (newPoint.initCoefficients(Parameters)) ret = true;
	else return false;
	this->push_back(newPoint);

	return (ret);
};
//============================================================================

bool CSnakePointsVec::addSegment(const CXYPoint point,const unsigned int index, const CImageBuffer *gradX, const CImageBuffer *gradY, C2DPoint Offset, const SnakeExtractionParameters  *Parameters)
{
	bool ret = false;
	CSnakePoint newPoint;

	newPoint.setIndex(index);
	newPoint.setX(point.x);
	newPoint.setY(point.y);
	newPoint.initGradientValues(gradX, gradY, Offset);
	if (newPoint.initCoefficients(Parameters)) ret = true;
	else return false;
	this->push_back(newPoint);

	return(ret);
};
//============================================================================

void CSnakePointsVec::addSegment(const CSnakePoint &segment)
{
	this->push_back(segment);
};
//============================================================================
  
void CSnakePointsVec::removeSegmentAt(unsigned int i)
{
	if ((this->size() > 0) && ((int)i < this->size()))
	{
		for (unsigned int u = i + 1; u < this->size(); ++u)
		{
			(*this)[u - 1] = (*this)[u];
		}
		this->pop_back();
	}
};

//============================================================================
void CSnakePointsVec::dump(const CFileName &filename) const
{
	ofstream outFile(filename.GetChar());
	if (outFile.good())
	{
		for (unsigned int i = 0; i < this->size(); ++i)
		{
			(*this)[i].dump(outFile);
		}
	}
};

//============================================================================
void CSnakePointsVec::writeVector(int index) const
{
	CCharString outputIndexStr;
	
	ostrstream oss;
	if (index < 1000) oss << "0";
	if (index < 100)  oss << "0";
	if (index < 10)   oss << "0";
		
	oss << index << ends;
	char *z = oss.str();
	outputIndexStr = z;
	delete [] z;	
	

	ofstream filename(protHandler.getDefaultDirectory() + "NewVector_" + outputIndexStr + "_Iteration" + ".txt");

	int nReg = 0;
	for (unsigned int k = 0; k < this->size(); k++)
	{
		(*this)[k].dump(filename);
		++nReg;
	}
		filename << "\n\n Total: " << nReg;
};
