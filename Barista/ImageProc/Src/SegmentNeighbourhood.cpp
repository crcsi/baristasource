#include <float.h>
#include "SegmentNeighbourhood.h"
#include "LabelImage.h"
#include "m_FeatExtract.h"
#include "ProtocolHandler.h"
#include "statistics.h"
#include "2DPoint.h"



CSegmentNeighbourhood::CSegmentNeighbourhood(): index (0), pRegion1(0), pRegion2(0), distance(0.0),
	 countor_region1_distance(0.0), countor_region2_distance(0.0), boundaryTexture(0.0), varianceRatio(0.0)
{
};

//============================================================================

CSegmentNeighbourhood::CSegmentNeighbourhood(unsigned int Index, CImageRegion *pR1, CImageRegion *pR2): 
	 index (Index), pRegion1(pR1), pRegion2(pR2), distance(0.0), countor_region1_distance(0.0), 
	 countor_region2_distance(0.0), boundaryTexture(0.0), varianceRatio(0.0)
{
	this->initDistance();
	this->initVarianceRatio();
};

//============================================================================

CSegmentNeighbourhood::CSegmentNeighbourhood(const CSegmentNeighbourhood &neighbour):
	index(neighbour.index), pRegion1(neighbour.pRegion1), 
	boundaryTexture(neighbour.boundaryTexture), varianceRatio(neighbour.varianceRatio),
	pRegion2(neighbour.pRegion2), distance(neighbour.distance), countor_region1_distance(neighbour.countor_region1_distance), 
	countor_region2_distance(neighbour.countor_region2_distance)
{
};

//============================================================================

CSegmentNeighbourhood::~CSegmentNeighbourhood()
{
	pRegion1 = 0;
	pRegion2 = 0;
};

//============================================================================

void CSegmentNeighbourhood::init(unsigned int Index, CImageRegion *pR1, CImageRegion *pR2)
{
	this->index    = Index;
	this->pRegion1 = pR1;
	this->pRegion2 = pR2;
	this->boundaryTexture = 0.0;
	this->initDistance();
	this->initVarianceRatio();
	this->countor_region1_distance = 0.0;
	this->countor_region2_distance = 0.0;
};

//============================================================================
  
unsigned short CSegmentNeighbourhood::getRegionIndex1() const
{
	if (!this->pRegion1) return 0;

	return this->pRegion1->getIndex();
};

//============================================================================

unsigned short CSegmentNeighbourhood::getRegionIndex2() const
{
	if (!this->pRegion2) return 0;

	return this->pRegion2->getIndex();
};
	
//============================================================================	 

CSegmentNeighbourhood &CSegmentNeighbourhood::operator = (const CSegmentNeighbourhood &neighbour)
{
	if (this != &neighbour)
	{
		index    = neighbour.index;
		pRegion1 = neighbour.pRegion1;
		pRegion2 = neighbour.pRegion2;
		distance = neighbour.distance;
		boundaryTexture = neighbour.boundaryTexture; 
		varianceRatio = neighbour.varianceRatio;
	}

	return *this;
};
 
//============================================================================
 
bool CSegmentNeighbourhood::operator < (const CSegmentNeighbourhood &neighbour)
{
	return fabs(this->distance) < fabs(neighbour.distance);
};

bool CSegmentNeighbourhood::operator <= (const CSegmentNeighbourhood &neighbour)
{
	return fabs(this->distance) <= fabs(neighbour.distance);	
};

bool CSegmentNeighbourhood::operator >= (const CSegmentNeighbourhood &neighbour)
{
	return fabs(this->distance) >= fabs(neighbour.distance);	
};

bool CSegmentNeighbourhood::operator > (const CSegmentNeighbourhood &neighbour)
{
	return fabs(this->distance) > fabs(neighbour.distance);	
};

	
//============================================================================
   
void CSegmentNeighbourhood::initDistance()
{
	if (this->pRegion1 && this->pRegion2)
		this->distance = this->pRegion1->getDistanceMetric(*pRegion2);
	else this->distance = FLT_MAX;
};

//============================================================================
   
void CSegmentNeighbourhood::initTexture(const int *boundary, const CImageBuffer &texture)
{
	int Ntotal = 0;
	int Nlarge = 0;

	for (unsigned int i = 0; i < texture.getNGV(); ++i)
	{
		if (boundary[i] == this->index)
		{
			++Ntotal;
			if (texture.pixFlt(i) > 1.0) ++Nlarge;
		}
	}

	if (Ntotal < 1) this->boundaryTexture = 0.0;
	else this->boundaryTexture = float(Nlarge) / float(Ntotal);
};
	 
//============================================================================
   
void CSegmentNeighbourhood::initVarianceRatio()
{
	int n1 = this->pRegion1->getNPixels();
	int n2 = this->pRegion2->getNPixels();
	if (n1 <= 1 || n2 <= 1) this->varianceRatio = 0;
	else
	{
		float sig1 = this->pRegion1->getSigma();
		float sig2 = this->pRegion2->getSigma();
		int red1 = this->pRegion1->getBands() * (n1 - 1);
		int red2 = this->pRegion1->getBands() * (n2 - 1);
		int redSeparate = red1 + red2;
		float varSeparate = (sig1 * sig1 * float(red1) + sig2 * sig2 * float(red2)) / float(redSeparate);
		
		CImageRegion rMerged1(*this->pRegion1);
		CImageRegion rMerged2(*this->pRegion2);
		rMerged1.merge(rMerged2);
		float varMerged = rMerged1.getSigma();
		varMerged *= varMerged;
		int redMerged = rMerged1.getBands() * (rMerged1.getNPixels() - 1);
		if (varMerged < varSeparate) this->varianceRatio = 0.0;
		else
		{
			this->varianceRatio = varMerged / varSeparate;
			this->varianceRatio /= float(statistics::fisherFractil95(redMerged, redSeparate));
		}


/*	float omega1 = this->pRegion1->getSigma();
    omega1 *= omega1;
	float omega2 = this->pRegion2->getSigma();
    omega2 *= omega2;

		if (omega1 > omega2) this->varianceRatio = omega1 / omega2;
		else 
		{
			this->varianceRatio = omega2 / omega1;
			int n = n1; 
			n1 = n2;
			n2 = n;
		}
		this->varianceRatio /= float(statistics::fisherFractil95(n1, n2));
	*/
	}
};

//============================================================================
 
void CSegmentNeighbourhood::exportContours(ofstream &dxfFile) 
{
	this->contours.ContourArrayToDXF(dxfFile);
}
	  
//============================================================================
 
void CSegmentNeighbourhood::exportContoursToBuffer(CImageBuffer &contourBuffer)
{
	for (int j = 0; j < this->contours.GetSize(); ++j)			
	{
		CXYContour *cont = this->contours.GetAt(j);
		
		for (int k = 0; k < cont->getPointCount(); ++k)			
		{					
			C2DPoint *p = cont->getPoint(k);

			long col = long (floor(p->x + 0.5)) - contourBuffer.getOffsetX();
			long row = long (floor(p->y + 0.5)) - contourBuffer.getOffsetY();
			if ((col >= 0) && (col < contourBuffer.getWidth()) && 
				(row >= 0) && (row < contourBuffer.getHeight()))
			{
				unsigned long idx = contourBuffer.getIndex(row, col);
				contourBuffer.pixUChar(idx) = 255;
			}
		}
	}
};

//============================================================================
 
void CSegmentNeighbourhood::initContour(const CLabelImage &labels, const CImageBuffer &ROI, int *neighbourMat)
{

//generate image buffer around region1 and region2
	C2DPoint reg1_min = this->pRegion1->getMinPos();
	C2DPoint reg1_max = this->pRegion1->getMaxPos();
	C2DPoint reg2_min = this->pRegion2->getMinPos();
	C2DPoint reg2_max = this->pRegion2->getMaxPos();
	
	C2DPoint reg_min;
	C2DPoint reg_max;

	//vertix min
	if(reg1_min.x < reg2_min.x) reg_min.x = reg1_min.x - 2; else reg_min.x = reg2_min.x - 2;
	if(reg1_min.y < reg2_min.y) reg_min.y = reg1_min.y - 2; else reg_min.y = reg2_min.y - 2;

	//vertix max
	if(reg1_max.x > reg2_max.x) reg_max.x = reg1_max.x + 2; else reg_max.x = reg2_max.x + 2;
	if(reg1_max.y > reg2_max.y) reg_max.y = reg1_max.y + 2; else reg_max.y = reg2_max.y + 2;
	
	if (reg_min.x < 0.0) reg_min.x = 0.0;
	if (reg_min.y < 0.0) reg_min.y = 0.0;
	if (reg_max.x >= labels.getOffsetX() + labels.getWidth())  reg_max.x = labels.getOffsetX() + labels.getWidth()  - 1.0;
	if (reg_max.y >= labels.getOffsetY() + labels.getHeight()) reg_max.y = labels.getOffsetY() + labels.getHeight() - 1.0;

	//generate image buffer
	CImageBuffer SubWindow((long)reg_min.x,(long)reg_min.y, long(reg_max.x-reg_min.x) + 1, long(reg_max.y-reg_min.y) + 1, 1/*ROI.getBands()*/, 8, true);


//generate contours
	unsigned long minIdx = (unsigned long ) labels.getIndex((long)reg_min.y - labels.getOffsetY(),(long)reg_min.x  - labels.getOffsetX());
	unsigned long maxIdxRow = minIdx + SubWindow.getWidth();
	unsigned long maxIdx = (unsigned long ) labels.getIndex((long)reg_max.y - labels.getOffsetY(),(long)reg_max.x + 1 - labels.getOffsetX());

	unsigned long LIndex, subIndex0, subIndex;
	
	//generate black-white-contour image
		//label image coor.sys./ countor image coor.sys.(x,y): minIdx, maxIdxRow, maxIdx, LIndex
		//sub image coor. sys.(x,y in brackets): subIndex0, subIndex
		//	
		//  ------------------------------------------>x
		//  |
		//  |
		//  |		minIdx-------------maxIdxRow......> (x)
		//  |	  (subIndex0)				|
		//  |			|					|
		//  |			|	->LIndex->		|
		//  |			|	(subIndex)		|
		//  |			|					|
		//  |			|					|
		//  |			|					|
		//  |			----------------maxIdxRow
		//  |			.
		//  |			.
		//  V			V								
		//  y			(y)

	for (subIndex0 = 0; minIdx < maxIdx; minIdx += labels.getDiffY(), maxIdxRow += labels.getDiffY(), subIndex0 += SubWindow.getDiffY())
	{
		for (LIndex = minIdx, subIndex = subIndex0; LIndex < maxIdxRow; LIndex += labels.getDiffX(), subIndex += SubWindow.getDiffX())
		{
			if (neighbourMat[LIndex] == this->index && this->index !=0) 
			{
				SubWindow.pixUChar(subIndex) = 255;
			}
			else 
			{
				SubWindow.pixUChar(subIndex) = 0;
			}
		}
	}

	//generate contours
	SubWindow.getContours(this->contours, 4, &labels); //number of bands must be 1!

	//init features of the contours
	for (int j = 0; j < this->contours.GetSize(); ++j)			//selection of a contour
	{
		CXYContour *cont = this->contours.GetAt(j);	
		
		this->CountorFeatures.initParameters(ROI,labels);

		for (int k = 0; k < cont->getPointCount(); ++k)			//pixels analysis in this specific contour
		{					
			float *colourVector = new float[ROI.getBands()];
			memset(colourVector, 0, ROI.getBands()*sizeof(float));

			C2DPoint *p = cont->getPoint(k);
			long x = (long) p->x;
			long y = (long) p->y;
			unsigned long idx = ROI.getIndex(y -ROI.getOffsetY(), x - ROI.getOffsetX());

			ROI.getColourVec(idx, colourVector);
			
			this->CountorFeatures.addColourVec(colourVector, x, y);

			delete[] colourVector;
		}

		this->CountorFeatures.computeRegionPars();

		this->countor_region1_distance = this->getRegion1()->getDistanceMetric(this->getCountorFeatures());
		this->countor_region2_distance = this->getRegion2()->getDistanceMetric(this->getCountorFeatures());
	}
};


//============================================================================

void CSegmentNeighbourhood::merge(CLabelImage &labels, int *boundary, CSegmentAMatrix &matrix, const CImageBuffer &img)
{
	unsigned int nLabels = labels.getNLabels();
	unsigned short region1 = this->pRegion1->getIndex();
	unsigned short region2 = this->pRegion2->getIndex();
	unsigned int bordernumber = matrix.LabelsToIndex(region1, region2);


		
	//1.update of adjacency matrix
	vector<unsigned long> vecFrom, vecTo;

	//1.1. update part 1
	unsigned int startA = bordernumber+1;						//r= region1, c= region2, +1
	unsigned int endA = matrix.LabelsToIndex(region1, nLabels);

	unsigned int startB = matrix.LabelsToIndex(region2, region2) + 1;
	unsigned int endB = matrix.LabelsToIndex(region2, nLabels);
	
	for(unsigned int i = 0; i < endA - startA +1 ;i++) 
	{
		unsigned int iB = startB +i;
		unsigned int iA = startA +i;

		if (matrix[iB])
		{
			matrix[iA] += matrix[iB];
			
			matrix[iB] = 0;
			vecFrom.push_back(iB);
			vecTo.push_back(iA);
		}
	}
	
	//1.2. update part 2
	unsigned int startC = matrix.LabelsToIndex(region1, region1) +1;
	unsigned int endC;

	if(region1==1 && region2==2) endC=0;
	else endC = matrix.LabelsToIndex(region1, region2)-1;
	
	if(endC != startC-1 && region1 != region2)
	{
		for(unsigned int i = 0; i < (endC - startC +1) ;i++) 
		{
			unsigned help = matrix.LabelsToIndex(region1+1+i, region2);

			if (matrix[help])
			{
				unsigned int iC = startC+i;
				matrix[iC] += matrix[help];
				matrix[help] = 0;
				vecFrom.push_back(help);
				vecTo.push_back(iC);
			}
		}
	}

	//1.3. update part 3
	for(unsigned int i = 1; i < region1 ;i++) 
	{
		unsigned int help2 = matrix.LabelsToIndex(i,region2);
		
		if (matrix[help2])
		{
			unsigned int help1 = matrix.LabelsToIndex(i,region1);

			matrix[help1] += matrix[help2];
			matrix[help2] = 0;
			vecFrom.push_back(help2);
			vecTo.push_back(help1);
		}
	}

/*#ifdef _DEBUG	
	//print out for debug
	CFileName fn("update.txt");
	fn.SetPath(protHandler.getDefaultDirectory());

	ofstream update_matrix;
	
	update_matrix.open(fn.GetChar(), ios::app);
	update_matrix << "\n nLabels: " << nLabels << "  region1: " << region1 << "  region2: " << region2 << "  startA: " << startA << "  startB: " << startB << endl;
	update_matrix.close();
#endif*/
	
	
	//2.update boundary image (change the numbers of the edgepixel)
	int margin = 1;
	unsigned long uMax = labels.getIndex(labels.getHeight() - margin, labels.getWidth() - margin);

	CSortVector<short> eight_neighbourhood(8,0);	
	unsigned long rv;

	CImageRegion BoundaryReg(1, this->pRegion1->getBands());

	float *gv = new float[this->pRegion1->getBands()];

	long x, y, b;

	for (unsigned int k = 0, u = 0; k < uMax; k++, u += img.getDiffX())
	{
		if (boundary[k])
		{
			//border pixel between region1 and region2 (bordernumber -> 0)
			if (boundary[k] == bordernumber)	
			{	
				labels.pixUShort(k)=region1;	//change label image
				boundary[k] = 0;				//change border image

				labels.getRowColBand(k, y, x, b);

				img.getColourVec(u,gv);
				BoundaryReg.addColourVec(gv, x, y);

			}else
			
			//border pixel between more than two regions
			if (boundary[k] < 0)	
			{	
				//calculation of 8th neighbourhood
				rv = k + 1;
				eight_neighbourhood[0] = labels.getLabel(rv);
				rv += labels.getDiffY();
				eight_neighbourhood[1] = labels.getLabel(rv);
				rv -= labels.getDiffX();
				eight_neighbourhood[2] = labels.getLabel(rv);
				rv -= labels.getDiffX();
				eight_neighbourhood[3] = labels.getLabel(rv);
				rv -= labels.getDiffY();
				eight_neighbourhood[4] = labels.getLabel(rv);
				rv -= labels.getDiffY();
				eight_neighbourhood[5] = labels.getLabel(rv);
				rv += labels.getDiffX();
				eight_neighbourhood[6] = labels.getLabel(rv);
 				rv += labels.getDiffX();
				eight_neighbourhood[7] = labels.getLabel(rv);
				
				eight_neighbourhood.sortAscending();

				//detection of neighbour regions
				vector<unsigned int>  neighbour_id;
				int lastNeighbour = 0;
				for (int i=0; i<8; i++)
				{
					if (eight_neighbourhood[i] > lastNeighbour)
					{
						lastNeighbour = eight_neighbourhood[i];
						neighbour_id.push_back(eight_neighbourhood[i]);
					}
				}
				
				unsigned int neighbour_id_size = neighbour_id.size();

				for (unsigned int i = 0; i < neighbour_id.size(); i++)
				{
					if (neighbour_id[i] == region1)
					{
						for (unsigned int j=i; j<neighbour_id_size; j++)
						{

							//reduce border pixels
							if (neighbour_id[j] == region2)
							{
								if (neighbour_id_size == 4) boundary[k] = -1;

								if (neighbour_id_size == 3) 
								{
									unsigned int new_neighbour = 0;
									for (unsigned int l = 0; l < neighbour_id_size; ++l)
									{
										if (neighbour_id[l] != region1 && neighbour_id[l] != region2) new_neighbour = neighbour_id[l];
									}

									boundary[k] =  matrix.LabelsToIndex(region1,new_neighbour);
								}
								
							}
						}
					}
				}
				
			}else 
			
			//change border pixel between the new region and their neighbours
			{
				for (unsigned long l = 0; l < vecTo.size(); ++l)
				{
					if (boundary[k] == vecFrom[l]) boundary[k] = vecTo[l]; 
				}
			}
		}
	}
	//to add: change of edge pixel with a value smaller than 0

	

	//3.change the numbers of the labels in labelimage: replace region 2 by region 1;
	labels.mergeLabels(region2, region1, false);

	delete [] gv;

	BoundaryReg.computeRegionPars();
	this->pRegion1->merge(BoundaryReg);
	this->pRegion1->merge(*pRegion2);

	

}
//=======================================================================================	  
void CSegmentNeighbourhood::dump(ofstream &oFile) const
{
	oFile.setf(ios::fixed, ios::floatfield);
	oFile.precision(3);


	oFile << "Regions ";
	oFile.width(5);
	oFile << this->pRegion1->getIndex() << " and " ;
	oFile.width(5);
	oFile << this->pRegion2->getIndex() 
		<< ": Index: " << this->getIndex() << ",  Distance: " << this->distance << ", Texture: " << this->boundaryTexture 
		  << ", varianceRatio: " << this->varianceRatio << ", Region1 Countor: " << this->countor_region1_distance
		  << ", Region2 Countor: " << this->countor_region2_distance;;
	oFile << endl;
};

//============================================================================
   
void CSegmentNeighbourhood::setIndex(unsigned int dimension)
{
	this->index = (this->getRegionIndex1()-1)*dimension - (this->getRegionIndex1()-1)*this->getRegionIndex1()/2 + this->getRegionIndex2() -1 -this->getRegionIndex1();
};
