#include <math.h>
#include <float.h>
#include <strstream>
#include "MatchRecord.h"
#include "ImgBuf.h"

CMatchRecord::CMatchRecord(const CXYPoint &p, const CImageBuffer &buf, int width, int height)
{
	this->init(p, buf, width, height);
}

CMatchRecord::~CMatchRecord() 
{ 
};

void CMatchRecord::init(const CXYPoint &p, const CImageBuffer &buf, int width, int height)
{
	this->P = p;
	this->hypotheses.clear();
	this->residuals.clear();
	this->sigmaResVec.clear();
	this->rhoVec.clear();

	if (!(width  % 2)) width++;
	if (!(height % 2)) height++;
	int offsetX = int(floor(this->P.getX() + 0.5f)) - width / 2;
	int offsetY = int(floor(this->P.getY() + 0.5f)) - height / 2;
	buf.extractSubWindow(offsetX, offsetY, width, height, this->greybuf);
}; 

float CMatchRecord::checkHypothesis(CMatchRecord *hypothesis, float threshold)
{
	double sum1 = 0.0f, sum2 = 0.0f, sumsq1 = 0.0f, sumsq2 = 0.0f, sum12 = 0.0f;

	if ((this->greybuf.getBpp() == 8) && hypothesis->greybuf.getBpp() == 8)
	{
		for (unsigned long u1 = 0, u2 = 0; u1 < this->greybuf.getNGV();  u1 += this->greybuf.getDiffX(), u2 += hypothesis->greybuf.getDiffX())
		{
			float gVal1 = (float) this->greybuf.pixUChar(u1);
			float gVal2 = (float) hypothesis->greybuf.pixUChar(u2);
			sum1   += gVal1;
			sum2   += gVal2;
			sumsq1 += gVal1 * gVal1;
			sumsq2 += gVal2 * gVal2;
			sum12  += gVal1 * gVal2;
		}
	}
	else if ((this->greybuf.getBpp() == 16) && hypothesis->greybuf.getBpp() == 16)
	{
		if (this->greybuf.getUnsignedFlag() && hypothesis->greybuf.getUnsignedFlag())
		{
			for (unsigned long u1 = 0, u2 = 0; u1 < this->greybuf.getNGV();  u1 += this->greybuf.getDiffX(), u2 += hypothesis->greybuf.getDiffX())
			{
				float gVal1 = (float) this->greybuf.pixUShort(u1);
				float gVal2 = (float) hypothesis->greybuf.pixUShort(u2);
				sum1   += gVal1;
				sum2   += gVal2;
				sumsq1 += gVal1 * gVal1;
				sumsq2 += gVal2 * gVal2;
				sum12  += gVal1 * gVal2;
			};
		}
		else if ((!this->greybuf.getUnsignedFlag()) && (!hypothesis->greybuf.getUnsignedFlag()))
		{
			for (unsigned long u1 = 0, u2 = 0; u1 < this->greybuf.getNGV();  u1 += this->greybuf.getDiffX(), u2 += hypothesis->greybuf.getDiffX())
			{
				float gVal1 = (float) this->greybuf.pixShort(u1);
				float gVal2 = (float) hypothesis->greybuf.pixShort(u2);
				sum1   += gVal1;
				sum2   += gVal2;
				sumsq1 += gVal1 * gVal1;
				sumsq2 += gVal2 * gVal2;
				sum12  += gVal1 * gVal2;
			}
		}
	}
	else if ((this->greybuf.getBpp() == 32) && hypothesis->greybuf.getBpp() == 32)
	{
		for (unsigned long u1 = 0, u2 = 0; u1 < this->greybuf.getNGV();  u1 += this->greybuf.getDiffX(), u2 += hypothesis->greybuf.getDiffX())
		{
			float gVal1 = (float) this->greybuf.pixFlt(u1);
			float gVal2 = (float) hypothesis->greybuf.pixFlt(u2);
			sum1   += gVal1;
			sum2   += gVal2;
			sumsq1 += gVal1 * gVal1;
			sumsq2 += gVal2 * gVal2;
			sum12  += gVal1 * gVal2;
		};
	}

	double oneByN = 1.0 / ((double) this->greybuf.getWidth() * this->greybuf.getHeight());
	double var1 = sumsq1 - sum1 * sum1 * oneByN;
	double var2 = sumsq2 - sum2 * sum2 * oneByN;
	float rho = 0.0f;
	if ((var1 > FLT_EPSILON) && (var2 > FLT_EPSILON)) 
	{
		double covar = sum12 - sum1 * sum2 * oneByN;
		rho    = float(covar / sqrt(var1 * var2));
		if (rho > threshold)
		{
			this->addHypothesis(hypothesis, rho);
			hypothesis->addHypothesis(this, rho);
		}
	}

	return rho;
}; 

void CMatchRecord::addHypothesis(CMatchRecord *hypothesis, float rho)
{
	this->hypotheses.push_back(hypothesis);
	this->rhoVec.push_back(rho);
	C2DPoint res;
	this->residuals.push_back(res);
	this->sigmaResVec.push_back(0);
}; 

void CMatchRecord::deleteHypothesis(int index)
{
	CMatchRecord *toDelete = this->hypotheses[index];

	for (int i = index + 1; i < this->nHypotheses(); ++i)
	{
	  this->hypotheses [i - 1] = this->hypotheses[i]; 
	  this->residuals  [i - 1] = this->residuals[i];
	  this->sigmaResVec[i - 1] = this->sigmaResVec[i];
	  this->rhoVec     [i - 1] = this->rhoVec[i];
	}

	this->hypotheses.pop_back(); 
	this->residuals.pop_back();
	this->sigmaResVec.pop_back();
	this->rhoVec.pop_back();

	toDelete->deleteHypothesis(this);
};

void CMatchRecord::deleteAllHypotheses()
{
	for (int i = 0; i < this->nHypotheses(); ++i)
	{
		this->hypotheses[i]->deleteHypothesis(this);
	}

	this->hypotheses.clear(); 
	this->residuals.clear();
	this->sigmaResVec.clear();
	this->rhoVec.clear();
};

void CMatchRecord::deleteHypothesis(CMatchRecord *hypothesis)
{
	int toDel = -1;

	for (int i = 0; i < (this->nHypotheses()) && (toDel < 0); ++i)
	{
	  if (this->hypotheses [i] == hypothesis) toDel = i; 
	}

	if (toDel >= 0)
	{
		for (int i = toDel + 1; i < this->nHypotheses(); ++i)
		{
		  this->hypotheses [i - 1] = this->hypotheses[i]; 
		  this->residuals  [i - 1] = this->residuals[i];
		  this->sigmaResVec[i - 1] = this->sigmaResVec[i];
		  this->rhoVec     [i - 1] = this->rhoVec[i];
		}

		this->hypotheses.pop_back(); 
		this->residuals.pop_back();
		this->sigmaResVec.pop_back();
		this->rhoVec.pop_back();
	}
};

CMatchRecord *CMatchRecord::getBestHypothesis(int &indexOfBest) const
{
	CMatchRecord *recMin = 0;
	indexOfBest = -1;

	if (this->nHypotheses() > 0)
	{
		double resMin = FLT_MAX;

		for (int i = 0; i < this->nHypotheses(); ++i)
		{
			double res = this->residuals[i].getNorm() / this->sigmaResVec[i];
			if (res < resMin)
			{
				recMin = this->hypotheses[i];
				indexOfBest = i;
				resMin = res;
			}
		}
	}

	return recMin;
};
	  
void CMatchRecord::swapHypotheses(int i, int j)
{
	if (i != j)
	{
		CMatchRecord *hypo = this->hypotheses[i];
		this->hypotheses[i] = this->hypotheses[j];
		this->hypotheses[j] = hypo;

		C2DPoint resPt = this->residuals[i];
		this->residuals[i] = this->residuals[j];
		this->residuals[j] = resPt;
		
		double dmy = this->sigmaResVec[i];
		this->sigmaResVec[i] = this->sigmaResVec[j];
		this->sigmaResVec[j] = float(dmy);

		dmy = this->rhoVec[i];
		this->rhoVec[i] = this->rhoVec[j];
		this->rhoVec[j] = float(dmy);
	}
}

void CMatchRecord::clarifyHypotheses()
{
	if (this->nHypotheses() > 0)
	{
		int indexBest, otherIndexBest;
		CMatchRecord *bestHypothesis = this->getBestHypothesis(indexBest);
		CMatchRecord *otherBestHypothesis = bestHypothesis->getBestHypothesis(otherIndexBest);
		if (otherBestHypothesis != this)
		{
			otherBestHypothesis->deleteHypothesis(this);
			otherBestHypothesis->deleteAllHypotheses();
			this->deleteHypothesis(otherBestHypothesis);
			this->deleteAllHypotheses();
		}
		else
		{
			this->swapHypotheses(0, indexBest);
			bestHypothesis->swapHypotheses(0, otherIndexBest);

			while (this->nHypotheses() > 1)
			{
				this->deleteHypothesis(this->nHypotheses() - 1);
			};

			while (bestHypothesis->nHypotheses() > 1)
			{
				bestHypothesis->deleteHypothesis(bestHypothesis->nHypotheses() - 1);
			}
		}
	}
}

void CMatchRecord::setResidual(const C2DPoint &res, float sigmaRes, CMatchRecord * hypothesis)
{
	int index = -1;
	for (int i = 0; (i < this->nHypotheses()) && (index < 0); ++i)
	{
		if (this->hypotheses[i] == hypothesis) index = i;
	}

	if (index >= 0)
	{
		this->residuals[index] = res;
		this->sigmaResVec[index] = sigmaRes;
	}
};
