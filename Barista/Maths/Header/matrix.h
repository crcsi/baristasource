/////////////////////////////////////////////////////////////////////////////
// CMatrix:
// See matrix.cpp for the implementation of this class
//

#ifndef _INC_MATRIX                          
#define _INC_MATRIX

class CDoubleVector;
class CFileArchive;
class CMatrixListener;

class CMatrix
{
public:
	int m_rows,m_cols;		// rows and columns
	double **m_ppData;		// matrix
	CMatrixListener* listener; // should be an array but one for now
public:
	double*	GetDataPointer(int r, int c);
	double* __GetDataPointer(int r, int c);
public:
	void addListener(CMatrixListener* l)
	{
		this->listener = l;
	};

	void Transpose();
	~CMatrix();
	CMatrix();
	CMatrix(int rows, int cols);

	CMatrix(const CMatrix &rSrc);
	CMatrix(const CMatrix *pSrc);
	CMatrix( CDoubleVector &Vec);

	void Copy(const CMatrix &rSrc);
	void Copy(const CMatrix *pSrc);
	CMatrix& operator =(const CMatrix &rSrc) { Copy(rSrc); return *this; }
	CMatrix& operator =(CDoubleVector &Vec);
	CMatrix operator +(const CMatrix &rSrc);
	CMatrix operator -(const CMatrix &rSrc);
	CMatrix& operator +=(const CMatrix& rSrc);

	CMatrix operator *(const CMatrix &rSrc);
	CMatrix& operator *=(const CMatrix &rSrc);

	CMatrix operator *(const double &value);

	double	Get(int r, int c);
	void	Set(double d, int r, int c);

	bool	IsSymmetric() const {return (m_cols == -1);};

	bool	SetAsIdentity(double dValue=1.0);
	bool	Set(int rows, int cols);
	void	SetToZero();
	void	Resize(int nrows, int ncols);
	void	InsertColumns(int nCols, int nStartCol);

	void	GetDiagonalSubMatrix(CMatrix &Submatrix, int nRowStart, int nRowEnd);

	// addition & multiplication
	bool	AddSubmatrix(CMatrix &Submatrix, int SROW, int SCOL);
	bool	SubtractSubmatrix(CMatrix &Submatrix, int SROW, int SCOL);
	bool	MultiplyByScalar(double dScalar);
	bool	Mult_ATA(CMatrix& A, int AddMode);
	bool	Mult_ATUTA(CMatrix& A, CMatrix& UT);
	double	Mult_ATUTA(CDoubleVector& v, CMatrix& UT);
	bool	Mult_AAT(CMatrix& A, int AddMode=1);
	void	Mult_AB(CMatrix& A, CMatrix& B, int MultMode, int AddMode);
	bool	Mult_UTri_BT(CMatrix& UT,CMatrix& T);
	bool	Mult_A_B_UT(CMatrix& A, CMatrix& B);
	bool	Mult_A_B_UTM(CMatrix& A, CMatrix& B);
	bool	Mult_A_B_UTM3(CMatrix& A, CMatrix& B);
    bool    Mult_A_B_UTM_NX3(CMatrix& A, CMatrix& B);
    bool    Mult_A_B_UTM_NX7(CMatrix& A, CMatrix& B);

	bool	Mult_AUTAT(CMatrix& A,CMatrix& UT);
	bool	Mult_UTri_B(CMatrix& UT, CMatrix& B);
	bool	Mult_A_UT(CMatrix& A, CMatrix& UT);
	bool	Mult_AT_UT(CMatrix& A, CMatrix& UT);

	bool	Mult(CMatrix& A, CMatrix& B);
	bool	Mult(CMatrix& A);

	// decomposion and inversion
	bool CholeskyUT();
	bool CholeskyUTBsub(CDoubleVector& s);
	bool CholeskyUT_SQR_FREE();
	bool CholeskyUT_SQR_FREE_BKSUB(CDoubleVector& s);
	bool gwsr(CDoubleVector &x);
	bool uTdu_Inverse(bool bfactored);
	bool uTdu_Inverse();
	bool SRFCholeskyInverse();
	bool FullInverse();
	bool Invert_3By3();

	void SerializeMatrix(CFileArchive& ar);

	void Dump();
	void OutputMatrix(const char *cName);
	bool IsValidCovarianceMatrix();

	friend class CMappedMatrix;
};

#endif
