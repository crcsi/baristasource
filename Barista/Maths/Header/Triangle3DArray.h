// Triangle3DArray.h: interface for the CTriangle3DArray class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CTriangle3DArray__
#define __CTriangle3DArray__


#include "MUObjectArray.h"
#include "Triangle3DPtrArray.h"
#include "Triangle3D.h"

class CTriangle3DArray : public CMUObjectArray<CTriangle3D, CTriangle3DPtrArray>
{
public:
	CTriangle3DArray(int GrowBy = 0);

	virtual ~CTriangle3DArray();

};

#endif 
