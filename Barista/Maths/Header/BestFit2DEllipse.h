#ifndef __CBestFit2DEllipse__
#define __CBestFit2DEllipse__
// test comment for Mehdi
#include "LeastSquaresModel.h"

class CBestFit2DEllipse :	public CLeastSquaresModel
{
	double rho0;

public:
	CBestFit2DEllipse(void);
	~CBestFit2DEllipse(void);

	bool isa(string& className) const
	{
		if (className == "CBestFit2DEllipse")
			return true;

		return false;
	};
	string getClassName() const {return string("CBestFit2DEllipse");};
	
	bool approximate (Matrix* approx, Matrix* consts, Matrix* obs);
	bool Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob);
	bool Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob);
	bool Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
     */
     void getParameterNames(CCharStringArray* pNames);

    /**
     * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y"...
     */
     void getObservationNames(CCharStringArray* pNames);

     /**
      * @return a String cantainint the name of the best fit ex. "Best fit Ellipse"
      */
     void getName(CCharString& pName);

     /**
      * Sets the a priori parameters information along with a priori
      * covariance Matrix
      */
     void setParameters(Matrix* parms, CBlockDiagonalMatrix* Qxx);

    /**
     * Gets the apriori covariance Matrix (if there)
     */
    CBlockDiagonalMatrix* getQxx();
};
#endif
