// Triangle3dPtrArray.h: interface for the CTriangle3dPtrArray class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CTriangle3DPtrArray__
#define __CTriangle3DPtrArray__

#include "MUObjectPtrArray.h"
#include "Triangle3D.h"

class CTriangle3DPtrArray : public CMUObjectPtrArray<CTriangle3D>
{
public:
	CTriangle3DPtrArray(int nGrowBy = 0);
	virtual ~CTriangle3DPtrArray();

};

#endif 
