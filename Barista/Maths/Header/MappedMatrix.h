// MappedMatrix.h: interface for the CMappedMatrix class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CMappedMatrix__
#define __CMappedMatrix__

#include "matrix.h"

class CMappedMatrix : public CMatrix  
{
public:
	CMappedMatrix();
	virtual ~CMappedMatrix();

	bool MakeMap(CMatrix& src, int nFirstRow, int nFirstCol, 
								int nRows, int nCols);
	bool MakeSymmetricMap(CMatrix& src, int nFirstRow, int nFirstCol, 
								int nRows, int nCols);
	bool MakeRectangularMap(CMatrix& src, int nFirstRow, int nFirstCol, 
								int nRows, int nCols);
};

#endif 
