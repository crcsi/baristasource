#ifndef __CTransformationMatrix__
#define __CTransformationMatrix__
#include "newmat.h"

class CTransformationMatrix : public Matrix
{
public:
	CTransformationMatrix();
	CTransformationMatrix(Matrix M);
	~CTransformationMatrix();

	CTransformationMatrix(const CTransformationMatrix& src);
	CTransformationMatrix(double omega, double phi, double kappa, double Sx, double Sy, double Sz, double s =1);

	void Copy(const CTransformationMatrix& src);
	
	void form321(Matrix* pP1, Matrix* pP2, Matrix* pP3);
	void form321(Matrix* p321Alignment, Matrix* pP1, Matrix* pP2, Matrix* pP3);

	double getOmega();
	double getPhi();
	double getKappa();
};

#endif