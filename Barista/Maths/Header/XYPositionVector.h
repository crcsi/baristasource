#ifndef __CXYPositionVector__
#define __CXYPositionVector__

#include "2DPoint.h"

class CXYPositionVector
{
public:
	CXYPositionVector(C2DPoint* pStart, C2DPoint* pEnd);
    CXYPositionVector(C2DPoint* pStart, double theta, double length);
    CXYPositionVector();
	~CXYPositionVector();
	double getTheta();
	double getLength();
	void setLength(double length);
	C2DPoint* getStart();
	C2DPoint* getEnd();
	void setStart(C2DPoint* pStart);
	void setTheta(double theta);
	void plus(double angle);
	void minus(double angle);
	void getNormal(CXYPositionVector* pOutput, C2DPoint* pPoint, double length);

private:
	double theta;
	double length;
	C2DPoint start;
	C2DPoint end;
};

#endif
