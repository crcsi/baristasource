#ifndef __CBestFitPlane3D__
#define __CBestFitPlane3D__
#include "LeastSquaresModel.h"

class CBestFitPlane3D :	public CLeastSquaresModel
{

public:
	CBestFitPlane3D(void);
	~CBestFitPlane3D(void);

	bool isa(string& className) const
	{
		if (className == "CBestFitPlane3D")
			return true;

		return false;
	};
	string getClassName() const {return string("CBestFitPlane3D");};

    /**
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Column Matrix of approximations of the parameters of the least squares fit
     */
    bool approximate (Matrix* approx, Matrix* consts, Matrix* obs);

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Function evaluated at the given observation unsing the given parameters
     */
    bool Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return A Matrix of partials with respect to paramters evaluated at ob
     */
    bool Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @return Bi Matrix of patials with respect to observations for the ith observation
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     */
    bool Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
     */
     void getParameterNames(CCharStringArray* pNames);

    /**
     * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
     */
     void getObservationNames(CCharStringArray* pNames);

     /**
      * @return a String cantainint the name of the best fit ex. "Best fit Line"
      */
     void getName(CCharString& pName);

     /**
      * Sets the a priori parameters information along with a priori
      * covariance Matrix
      */
     void setParameters(Matrix* parms, CBlockDiagonalMatrix* Qxx);

    /**
     * Gets the apriori covariance Matrix (if there)
     */
    CBlockDiagonalMatrix* getQxx();

	// Testable 
	bool runTest(CCharStringArray& report);

	void accumulate(Matrix* N, Matrix* t, Matrix* parms, Matrix* constant,
                        Matrix* ob, Matrix* previousResid, Matrix* Q);

	 virtual void addConstraint(Matrix* N, Matrix* t, Matrix* parms);


};
#endif
