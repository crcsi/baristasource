///////////////////////////////////////////////////////////////////////////////////////////////
// SVector.h																						 //
// Implementation of Vector template class													 //
//																							 //
///////////////////////////////////////////////////////////////////////////////////////////////


#ifndef _SVECTOR_H__                          
#define _SVECTOR_H__

// Standard includes
#include <string.h>
#include <assert.h>

template <class VecType> class CSVector
{

protected:
	int m_nSize;				// dimension
	VecType *m_pData;          	// vector

public:

//construction
	CSVector();
	CSVector(int s);

//copy constructor
	CSVector(const CSVector &CV);

// overloaded operators
	CSVector<VecType> operator= (const CSVector op2);

	VecType& operator[](int nindex) 
	{ 
		assert(nindex >= 0);
		assert(nindex <= m_nSize);

		return (VecType&) m_pData[nindex]; 
	};

	CSVector<VecType> operator+ (CSVector op2);
	CSVector<VecType> operator- (CSVector op2);
	CSVector<VecType> operator+= (CSVector op2);
	bool			  operator== (CSVector op2);

//default destructor
	~CSVector();

//member functions
	void	SetAt(int nindex, VecType item);
	int		GetSize();
	void	SetSize(int s);
	void	Add(VecType NewData);
	int		GetUpperBound();
	void	CleanUp();
	void	SetAll(int s, VecType value);
	void	InsertAt(int loc, VecType toinsert);
	void	RemoveAt(int loc);
	VecType	GetAt(int nindex);

	VecType* GetData() {return m_pData; };

};

// CVector default constructor function
template <class VecType>
CSVector<VecType>::CSVector()
{
	m_nSize = 0;
	m_pData = 0;
}

template <class VecType>
CSVector<VecType>::CSVector(int s)
{
	m_nSize = s;

	m_pData = new VecType [s];

	//initialization to zero
	memset(m_pData,0,m_nSize*sizeof(VecType));
}

// copy constructor
template <class VecType>
CSVector<VecType>::CSVector(const CSVector &CV)
{
	m_nSize = CV.m_nSize;
	
	m_pData = new VecType [m_nSize];
	
	for(int i=0;i<m_nSize;i++)
		m_pData[i] = CV.m_pData[i];
}

// destructor
template <class VecType>
CSVector<VecType>::~CSVector()
{
	if ( m_pData != 0 )
		delete [] m_pData;
	
	m_pData = 0;
}

// overloaded = operator
template <class VecType>
CSVector<VecType> CSVector<VecType>::operator= (CSVector op2)
{
	m_nSize = op2.m_nSize;

	if(m_pData != 0)
		delete [] m_pData;
	m_pData = new VecType[m_nSize];

    for( int i = 0; i < m_nSize; i++ )
		m_pData[i] = op2.m_pData[i];
	
	return *this;
}

// overloaded + operator
template <class VecType>
CSVector<VecType> CSVector<VecType>::operator+ (CSVector op2)
{
	CSVector<VecType> temp(GetSize());

	for( int i = 0; i < GetSize(); i++ )
		temp[i] = m_pData[i] + op2[i];

	return (temp);
}   

// overloaded += operator
template <class VecType>
CSVector<VecType> CSVector<VecType>::operator+= (CSVector op2)
{
	for( int i = 0; i < GetSize(); i++ )
		m_pData[i] = m_pData[i] + op2[i];

	return *this;
}   

// overloaded - operator
template <class VecType>
CSVector<VecType> CSVector<VecType>::operator- (CSVector op2)
{
	CSVector<VecType> temp(GetSize());

	for( int i = 0; i < GetSize(); i++ )
		temp[i] = GetAt(i) - op2[i];

	return (temp);
}   

// overloaded == operator
template <class VecType>
bool CSVector<VecType>::operator== (CSVector op2)
{
	if (GetSize()!=op2.GetSize())
		return false;

	for( int i = 0; i < GetSize(); i++ )
	{
		if (m_pData[i] != op2[i])
			return false;
	}

	return true;
}



template <class VecType>
void CSVector<VecType>::SetAll(int s, VecType value)
{
	//if m exists, delete all of its elements before resetting
	if(m_pData != 0)
		delete [] m_pData;

	m_nSize = s;

	m_pData = new VecType[s];

	for( int i = 0; i < m_nSize; i++ )
		m_pData[i] = value;
}

template <class VecType>
void CSVector<VecType>::SetSize(int nNewSize)
{

	if (nNewSize == m_nSize)	//do nothing if size stays
	{
	}
	else if (nNewSize == 0)
	{
		// shrink to nothing
		delete[] m_pData;
		m_pData = 0;
		m_nSize = 0;
	}
	else if (m_pData == 0)
	{
		// create one with exact size
		m_pData = new VecType[nNewSize * sizeof(VecType)];

		memset(m_pData, 0, nNewSize * sizeof(VecType));  // zero fill

		m_nSize = nNewSize;
	}
	else if (nNewSize < m_nSize)
	{
		VecType* pNewData = new VecType[nNewSize * sizeof(VecType)];

		// copy new data from old ... vector has been truncated
		memcpy(pNewData, m_pData, nNewSize * sizeof(VecType));

		// get rid of old stuff (note: no destructors called)
		delete[] m_pData;
		m_pData = pNewData;
		m_nSize = nNewSize;
	}
	else
	{
		VecType* pNewData = new VecType[nNewSize * sizeof(VecType)];

		// NEW STUFF

		// copy new data from old
		for(int i = 0; i < m_nSize; i++)
			pNewData[i] = m_pData[i];

		// copy new data from old
		//memcpy(pNewData, m_pData, m_nSize * sizeof(VecType*));

		// construct any remaining elements at 0
		memset(&pNewData[m_nSize], 0, (nNewSize-m_nSize) * sizeof(VecType));

		// get rid of old stuff (note: no destructors called)
		delete[] m_pData;
		m_pData = pNewData;
		m_nSize = nNewSize;
	}
}

template <class VecType>
void CSVector<VecType>::Add(VecType NewData)
{
	SetSize(m_nSize+1);
	SetAt(m_nSize-1, NewData);
}

template <class VecType>
int CSVector<VecType>::GetSize()
{
	return (m_nSize);
}

template <class VecType>
int CSVector<VecType>::GetUpperBound()
{
	return (m_nSize-1);
}

template <class VecType>
void CSVector<VecType>::CleanUp()
{
	if (m_pData == 0)
		return;

	delete [] m_pData;
	m_pData = 0;
	m_nSize = 0;
}

template <class VecType>
void CSVector<VecType>::InsertAt(int nindex, VecType toinsert)
{
	SetSize(m_nSize+1);

	assert(nindex >= 0);
	assert(nindex <= m_nSize);

	memmove(&m_pData[nindex+1], &m_pData[nindex], (m_nSize-nindex)*sizeof(VecType));
	
	m_pData[nindex] = toinsert;
	
}

template <class VecType>
void CSVector<VecType>::RemoveAt(int nindex)
{
	assert(nindex >= 0);
	assert(nindex <= m_nSize);

	memmove(&m_pData[nindex], &m_pData[nindex+1], (m_nSize-nindex)*sizeof(VecType));

	SetSize(m_nSize-1);
}

template <class VecType>
void CSVector<VecType>::SetAt(int nindex, VecType item)
{
	assert(nindex >= 0);
	assert(nindex <= m_nSize);
	
	m_pData[nindex] = item;
}

template <class VecType>
VecType CSVector<VecType>::GetAt(int nindex)
{
	assert(nindex >= 0);
	assert(nindex <= m_nSize);

	return *(m_pData+nindex);
}

#endif

