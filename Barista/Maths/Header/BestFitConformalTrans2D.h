/**
 * Title:        CBestFitConformalTrans2D<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Harry Hanley<p>
 * Company:      Photometrix<p>
 * @author Harry Hanley & Simon Cronk
 * @version 1.0<p>
 *
 * CBestFitConformalTrans2D<p><p>
 *
 * This class defines the partials, the approximation and the function
 * for a 2D affine transformation. It is used in the least-squares computation
 * computation of the parameters a, b, c, d
 *
 * 2D conformal transformation<p>
 *
 *       x2 = a*x1 - b*y1 + c<p>
 *       y2 = b*x1 + a*y1 + d<p>
 *
 */

#ifndef __CBestFitConformalTrans2D__
#define __CBestFitConformalTrans2D__
#include "LeastSquaresModel.h"

class CBestFitConformalTrans2D : public CLeastSquaresModel
{
public:
	CBestFitConformalTrans2D(void);
	~CBestFitConformalTrans2D(void);

	bool isa(string& className) const
	{
		if (className == "CBestFitConformalTrans2D")
			return true;

		return false;
	};
	string getClassName() const {return string("CBestFitConformalTrans2D");};

    /**
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Column Matrix of approximations of the parameters of the least squares fit
     */
    bool approximate (Matrix* approx, Matrix* consts, Matrix* obs);

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Function evaluated at the given observation unsing the given parameters
     */
    bool Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return A Matrix of partials with respect to paramters evaluated at ob
     */
    bool Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @return Bi Matrix of patials with respect to observations for the ith observation
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     */
    bool Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
     */
     void getParameterNames(CCharStringArray* pNames);

    /**
     * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
     */
     void getObservationNames(CCharStringArray* pNames);

     /**
      * @return a String cantainint the name of the best fit ex. "Best fit Line"
      */
     void getName(CCharString& pName);

     /**
      * Sets the a priori parameters information along with a priori
      * covariance Matrix
      */
     void setParameters(Matrix* parms, CBlockDiagonalMatrix* Qxx);

    /**
     * Gets the apriori covariance Matrix (if there)
     */
    CBlockDiagonalMatrix* getQxx();

	// Testable 
	bool runTest(CCharStringArray& report);

};
#endif
