// DoubleVectorArray.h: interface for the CDoubleVectorArray class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CDoubleVectorArray__
#define __CDoubleVectorArray__

#include "MUObjectArray.h"

#include "DoubleVector.h"
#include "DoubleVectorPtrArray.h"

class CDoubleVectorArray : public CMUObjectArray<CDoubleVector, CDoubleVectorPtrArray> 
{
public:
	CDoubleVectorArray(int nGrowBy = 0);

	virtual ~CDoubleVectorArray();

};

#endif // __CDoubleVectorArray__
