#ifndef __CSerialMatrix__
#define __CSerialMatrix__
#include "Serializable.h"
#include "CharString.h"
#include "newmat.h"
#include "matrix.h"

class CSerialMatrix : public Matrix, public CSerializable
{
public:
	CSerialMatrix(void);
	CSerialMatrix(CMatrix* pOldStyle);
	CSerialMatrix(CSerialMatrix* pSrcMatrix);
	~CSerialMatrix(void);

	bool isa(string& className) const
	{
		if (className == "CSerialMatrix")
			return true;

		return false;
	};

	string getClassName() const {return string("CSerialMatrix");};

	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);
//	CCharString getClassName();
	bool isModified();
	void reconnectPointers();
	void resetPointers();
};
#endif
