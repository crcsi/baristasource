#ifndef __CMatrixListener__
#define __CMatrixListener__

class CMatrixListener
{
public:
	CMatrixListener(void);
public:
	~CMatrixListener(void);

	virtual void processingRow(int r) = 0;
	virtual void totalRows(int r) = 0;
};

#endif