/**
 * Title:        Barista
 * Description:
 * Copyright:    Copyright (c) Harry Hanley, Paul Dare
 * Company:      University of Melbourne
 * @author Harry Handley
 * @version 1.0
 *
 * This class extend CLeastSquaresModel and represents a generic
 * Least-Squares Bundle adjustment.
 * When forming the normal equation, this class expects that the station
 * parameters come first followed by the point parameters then the camera
 * parameters.
 *
 * It is assumed that the bundle has station and point parameters and may or may
 * not have camera parameters
 *
 */
#ifndef __CBundleModel__
#define __CBundleModel__

#include "LeastSquaresModel.h"
#include "newmat.h"
#include "BlockDiagonalMatrix.h"
#include "CharStringArray.h"
class CMUIntegerArray;

class CBundleModel : public CLeastSquaresModel
{
public:
    int nPoints;
    int nStations;
    int nCameras;

    // to be filled in by extended class
    int nCameraParameters;
    int nStationParameters;
    int nPointParameters;
    int nStationConstants;

    CCharStringArray* CameraNames;
    CCharStringArray* StationNames;
    CCharStringArray* PointNames;

    // This will be an array of size n
    // This first value is the station index for the current observation (xy)
    // THe second value is the (ground) point index for the current observation
    CMUIntegerArray* currentObservationIndices;

    CBundleModel(void);
    ~CBundleModel(void);

    bool approximate (Matrix* approx, Matrix* consts, Matrix* obs);

    void setParameters(Matrix* parms, CBlockDiagonalMatrix* Qxx);
    void setParameters(Matrix* parms, CBlockDiagonalMatrix* Qxx, int nStations, int nPoints);
    void setParamterSizes(int nStation, int nPoint, int nCamera);

    CBlockDiagonalMatrix* getQxx();

    void accumulate(Matrix* N, Matrix* t, Matrix* parms, Matrix* Constant,
                     Matrix* Observation, Matrix* previousResid, Matrix* Q);

    void calculateResiduals(Matrix* resids, Matrix* Delta, Matrix* parms, Matrix* Constant,
                     Matrix* Observation, Matrix* previousResid, Matrix* Q);

    void setObservationIndices(CMUIntegerArray* indices);
    void getObservationNames(CCharStringArray* names);
    int getStationCount();
    int getStationParameterCount();
    int getPointCount();
    int getPointParameterCount();
    int getCameraCount();
    int getCameraParameterCount();
    void getSubDelta(Matrix* subDelta, Matrix* delta);
    void setCameraNames(CCharStringArray* v);
    void setStationNames(CCharStringArray* v);
    void setPointNames(CCharStringArray* v);
    CCharStringArray* getCameraNames();
    CCharStringArray* getStationNames();
    CCharStringArray* getPointNames();
    void setControlPoint(int index, Matrix* xyz, Matrix* Covariance);
    void freePoint(int index, Matrix* Covariance);

    void setMatrix(int startRow, int endRow, int startCol, int endCol, Matrix* dest, Matrix* src);
};

#endif


