// Triangle3D.h: interface for the CTriangle3D class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CTriangle3D__
#define __CTriangle3D__

#include "newmat.h"

class CTriangle3DArray;
class CTriangle3D  
{
protected:

	// These are the vertices
	Matrix point1;
	Matrix point2;
	Matrix point3;

public:
	Matrix* getPoint1();
	Matrix* getPoint2();
	Matrix* getPoint3();

	CTriangle3DArray tesselate();
	CTriangle3D();
	virtual ~CTriangle3D();



};

#endif 
