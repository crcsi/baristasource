// DoubleVectorPtrArray.h: interface for the CDoubleVectorPtrArray class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CDoubleVectorPtrArray__
#define __CDoubleVectorPtrArray__

#include "MUObjectPtrArray.h"
#include "DoubleVector.h"

class CDoubleVectorPtrArray : public CMUObjectPtrArray<CDoubleVector>
{
public:
	CDoubleVectorPtrArray(int GrowBy = 0);
	virtual ~CDoubleVectorPtrArray();

};

#endif // __CDoubleVectorPtrArray__
