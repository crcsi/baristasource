#ifndef __CLeastSquaresStatusControl__
#define __CLeastSquaresStatusControl__

/**
 * Title:        
 * Description:
 * Copyright:    Copyright (c) Harry Hanley
 * Company:      Photometrix
 * @author Harry Hanley
 * @version 1.0
 */

class CLeastSquaresStatusControl
{

private:

    int status_iter;
    int status_rejectionCount;
    int status_dof;
    double status_rms;
    bool status_done;
    bool status_aborted;
    bool status_converged;
    bool status_maxItersReached;
    bool status_criticalError;
    bool control_do_reject; 
    double control_rejectionMultiplier; 
    double control_rejectionAdd; 
    int control_maxIterations; 
    bool control_abort; 
    double control_rejection_limit; 

public:
	CLeastSquaresStatusControl();

	void setDoReject(bool b);
	bool getDoReject();

	double getRejectionLimit();
	void setRejectionLimit(double d);

	void setRejectionMultiplier(double d);
	double getRejectionMultiplier();

	void setRejectionAdd(double d);
	double getRejectionAdd();

	void setRejectionCount(int n);
	int getRejectionCount();

	int getMaxIters();
	void setMaxIters(int i);

	void setDOF(int dof);
	int getDOF();

	void setIter(int iter);
	int getIter();

	void setAbort(bool b);
	bool getAbort();

	void setStatusAborted();   

	void setStatusDone();

	void setRMS(double d);
	double getRMS();

	void setStatusConverged();

	void setStatusMaxIterations();
};

#endif