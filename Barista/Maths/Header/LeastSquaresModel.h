#ifndef __CLeastSquaresModel__
#define __CLeastSquaresModel__
#include "ZObject.h"
class Matrix;
class CBlockDiagonalMatrix;
class CCharStringArray;
class CMUIntegerArray;
class CCharString;

class CLeastSquaresModel : public ZObject
{
public:
	CLeastSquaresModel();
	~CLeastSquaresModel();

    Matrix* parms0;
    CBlockDiagonalMatrix* Qxx;

	void deleteObjects();

    /**
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Column Matrix of approximations of the parameters of the least squares fit
     */
    virtual bool approximate (Matrix* approx, Matrix* consts, Matrix* obs) = 0;

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Function evaluated at the given observation unsing the given parameters
     */
    virtual bool Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob) = 0;

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return A Matrix of partials with respect to paramters evaluated at ob
     */
    virtual bool Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob) = 0;

    /**
     * @return Bi Matrix of patials with respect to observations for the ith observation
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     */
    virtual bool Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob) = 0;

    /**
     * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
     */
     virtual void getParameterNames(CCharStringArray* names) = 0;

    /**
     * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
     */
     virtual void getObservationNames(CCharStringArray* names) = 0;

     /**
      * @return a String cantainint the name of the best fit ex. "Best fit Line"
      */
     virtual void getName(CCharString& names) = 0;

     /**
      * Sets the a priori parameters information along with a priori
      * covariance Matrix
      */
     virtual void setParameters(Matrix* parms, CBlockDiagonalMatrix* Qxx) = 0;

    /**
     * Gets the apriori covariance Matrix (if there)
     */
    virtual CBlockDiagonalMatrix* getQxx() = 0;

    /**
     * Not emplemented
     */
     virtual void setObservationIndices(CMUIntegerArray* pIndices)
     {
     };
  

	 virtual void addConstraint(Matrix* N, Matrix* t, Matrix* parms){};
    /**
     *
     */
    virtual void accumulate(Matrix* pN, Matrix* pt, Matrix* parms, Matrix* pConstant,
                            Matrix* observation, Matrix* pPreviousResid, Matrix* pQ);

    virtual void calculateResiduals(Matrix* pResids, Matrix* pDelta, Matrix* parms, Matrix* pConstant,
                            Matrix* observation, Matrix* pPreviousResid, Matrix* pQ);

    /**
     * Generally 0, but can be overridden to reflect the number of "observations"
     * that are not really discreet. Useful in calculating the dof.
     */
    virtual int getNonDistinctObservationCount();

};
#endif
