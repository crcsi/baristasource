#ifndef __CLeastSquaresSolver__
#define __CLeastSquaresSolver__

/**
 * Title:        LeastSquaresSolver<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Harry Hanley<p>
 * Company:      University of Melbourne<p>
 * @author Harry Hanley
 * @version 1.0
 *
 * formatted with JxBeauty (c) johann.langhofer@nextra.at
 */

#include "newmat.h"
#include "BlockDiagonalMatrix.h"
#include "CharStringArray.h"
#include "LeastSquaresStatusControl.h"

class CLeastSquaresModel;
class CIntegerArrayArray;

/**
 * Solves the least squares fit defined by the given Model derived class
 */
class CLeastSquaresSolver
{

private:
	
	CLeastSquaresModel* LeastSquaresModel;

    Matrix					Obs;
    Matrix*					Constants;

    CCharStringArray		ObsLabels;

    CBlockDiagonalMatrix	Cofacter;

    Matrix              updatedObs;
    Matrix              Residuals;
    Matrix              previousResiduals;

    Matrix              Covariance ;
	Matrix				correlation;

    CCharStringArray    RejectedLabels;
    Matrix              RejectedResiduals; // cronk

    double              PreSigma0;
    double 		        PostSigma0;
    double 		        ConvergenceLimit;
    Matrix              Solution;
    CBlockDiagonalMatrix* Qxx;
    CBlockDiagonalMatrix* Wxx;

    CLeastSquaresStatusControl StatusControl;

    Matrix              parms0;  // Initital approximations
                                        //of parameters

    //Vector              EventListeners = new Vector();

    double              chiSquaredTest;
    int                 observationCount;
    int                 degreesOfFreedom;
    int                 iterationCount;
    bool				abort;
    double              currentRMS;
    CCharString         currentObsName;
    bool				debug;
    bool				pope;


    // The Vector observation indices (below) are not always used, but here is
    // their purpose...
    //
    // In a more complex adjustment (like a bundle) it is desired to take
    // advantage of the sparce nature of the Normal equations. To do so requires
    // some knowledge of where in the normal equations to fit a particular
    // observation's contribution. The observationIndices provide that
    // information.
    // The size of the Vector "observationIndices",  will be n where n is the
    // number of observations. Each observationIndex will be an array of
    // integers that indicate to which XYZ point, picture station and
    // For instance, for each (xy) observation it is required to know which
    // ground point it goes with, with camera was used (in the case of multiple
    // cameras) and from which station it was seen.
    // The LeastSquaresModel inherited class is responcible for interpreting
    // the observationIndices Vector
    
    CIntegerArrayArray* observationIndices; //

public:

    CLeastSquaresSolver(CLeastSquaresModel* LeastSquaresModel, Matrix* Observations);

	CLeastSquaresSolver(CLeastSquaresModel* pLeastSquaresModel,
                         Matrix* pObservations, CCharStringArray* pObsLabels,
                         Matrix* pConstants, Matrix* pSigmas);

	CLeastSquaresSolver(CLeastSquaresModel* pLeastSquaresModel,
                         Matrix* pObservations, CCharStringArray* pObsLabels,
                         Matrix* pConstants, CBlockDiagonalMatrix* pQ);

    CLeastSquaresSolver(CLeastSquaresModel* pLeastSquaresModel, Matrix* pConstants,
                         Matrix* pObservations, CCharStringArray* pObsLabels);

	~CLeastSquaresSolver();

    bool solve(); //(Matrix& solution);
	CLeastSquaresStatusControl* getStatus();
    Matrix* getSolution();
    Matrix* getResiduals();
    Matrix* getCovariance();
    double getPostSigma0();
    Matrix* getCorrelation();
	CLeastSquaresModel* getLeastSquaresModel();
    CCharStringArray* getObsLabels();
    void run();
    int getObservationCount();
    void setObservationIndices(CIntegerArrayArray* indices);
    void setConvergenceLimit(double c);
    CCharStringArray* getRejectedLabels();
    Matrix* getRejectedResiduals();
    bool getDoRejection();
    //void addEventListener(JLeastSquaresListener EventLister);
	void setDoRejection(bool b);
    CLeastSquaresStatusControl* getStatusControl();

	//synchronized
    void setAbort(bool b);
    double getCurrentRMS();
    void setCurrentRMS(double d);
    void getCurrentObsName(CCharString& label);
    void setCurrentObsName(CCharString s);
	void setCurrentObsName(int i);
    int getDOF();

	void plusEquals (Matrix* N, CBlockDiagonalMatrix* B);
	void times(Matrix* result, CBlockDiagonalMatrix* A, Matrix* B);

private:

//	void TriggerEvent(JLeastSquaresEvent Event);
	int getConstraintCount();
    Matrix* GetQ(int i);

//synchronized
	void setDOF(int i);

	void init();
};

#endif

