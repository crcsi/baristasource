// DoubleVector.h: interface for the CDoubleVector class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CDoubleVector__
#define __CDoubleVector__

#include "SVector.h"

class CMatrix;

class CDoubleVector : public CSVector<double>
{
public:
	void Normalize();
	double GetLength();
	void CrossProduct(CDoubleVector &a, CDoubleVector &b);
	CDoubleVector(int size) : CSVector<double>(size) {}

	CDoubleVector() : CSVector<double>() {}

	virtual ~CDoubleVector();

	bool	Mult_AT_V(CMatrix& A, CDoubleVector& V);
	bool	Mult_AT_UT_V(CMatrix& A, CMatrix& UT, CDoubleVector& V);
	bool	Mult_AT_VM(CMatrix& A, CDoubleVector &V);
	bool	Mult_UT_A_V(CMatrix& A, CDoubleVector& V);
	bool	Mult_A_V(CMatrix& A, CDoubleVector &V);
	bool	Mult_A_VM(CMatrix& A, CDoubleVector &V);

	bool	SetToZero();

	double  GetMinimum(long *pIndex=NULL);
	double  GetMaximum(long *pIndex=NULL);
	CDoubleVector GetDataInRange(int stIndex, int enIndex, double &sum);
	CDoubleVector& operator =(CMatrix &Mat);

	CDoubleVector operator *(double f) const;
	CDoubleVector& operator *=(double f);
	double operator *(const CDoubleVector &Vec) const;

	CDoubleVector operator -(const CDoubleVector &Vec) const;
	CDoubleVector& operator -=(const CDoubleVector &Vec);

	CDoubleVector operator +(const CDoubleVector &Vec) const;
	CDoubleVector& operator +=(const CDoubleVector &Vec);
 
	void	Dump();
};

#endif 
