#ifndef __CBestFit3DTrans__
#define __CBestFit3DTrans__

#include "LeastSquaresModel.h"
#include "Testable.h"

class CTransformationMatrix;
class CBestFit3DTrans : public CLeastSquaresModel, CTestable
{

private:
	void deleteObjects();

public:
	CBestFit3DTrans(void);
	~CBestFit3DTrans(void);

	bool isa(string& className) const
	{
		if (className == "CBestFit3DTrans")
			return true;

		return false;
	};
	string getClassName() const {return string("CBestFit3DTrans");};

	static bool equals(Matrix* A, Matrix* B);

    /**
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Column Matrix of approximations of the parameters of the least squares fit
     */
    bool approximate (Matrix* approx, Matrix* consts, Matrix* obs);

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Function evaluated at the given observation unsing the given parameters
     */
    bool Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return A Matrix of partials with respect to paramters evaluated at ob
     */
    bool Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @return Bi Matrix of patials with respect to observations for the ith observation
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     */
    bool Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob);

    /**
     * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
     */
     void getParameterNames(CCharStringArray* pNames);

    /**
     * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
     */
     void getObservationNames(CCharStringArray* pNames);

     /**
      * @return a String cantainint the name of the best fit ex. "Best fit Line"
      */
     void getName(CCharString& pName);

     /**
      * Sets the a priori parameters information along with a priori
      * covariance Matrix
      */
     void setParameters(Matrix* parms, CBlockDiagonalMatrix* Qxx);

    /**
     * Gets the apriori covariance Matrix (if there)
     */
    CBlockDiagonalMatrix* getQxx();

	bool runTest(CCharStringArray& report);
	//bool isa(const char* className);

	double approximateScale(Matrix* obs);
	void approximateShift(CTransformationMatrix& M, Matrix* obs, Matrix& shift);
	double computeRMSDiff(CTransformationMatrix& M, Matrix* obs);
};
#endif
