// constants.h : include file for math project include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#ifndef __Constants__
#define __Constants__

#define EPSILON 1e-30
#define SMALL 1.0e-16
#define PI 4.0*atan(1.0)
#define DTR (PI)/180.0
#define RTD 180.0/(PI)

#endif // __Constants__
