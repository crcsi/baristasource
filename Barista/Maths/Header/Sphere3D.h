// Sphere3D.h: interface for the CSphere3D class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __CSphere3D__
#define __CSphere3D__

#include <vector>
using namespace std;
#include"newmat.h"

class CTriangle3DArray;
class CTriangle3D;

class CSphere3D  
{
protected:
	double radius;
	Matrix origin;
public:
	Matrix* getOrigin();
	double getRadius();
	CSphere3D();
	CSphere3D(double x, double y, double z, double r);
	vector<Matrix *> tesselate(int levels);
	void fixToSphere(CTriangle3D* pTri);
	Matrix XYZatDegrees(double Asimuth, double Elevation);

	virtual ~CSphere3D();

};

#endif 
