#ifndef __CMatrixHelper__
#define __CMatrixHelper__

#include "Testable.h"

//#include <gl/gl.h>			// OpenGL includes
//#include <gl/glu.h>
//#include <gl/glaux.h>
//#include <afxdhtml.h>

typedef float GLfloat;

class Matrix;
class CMatrix;
class CCharStringArray;

class CMatrixHelper //: public CTestable
{
public:

	CMatrixHelper(void);
	virtual ~CMatrixHelper(void);

	static void convertMatrix(CMatrix* pOut, Matrix* pIn);
	static void convertMatrix(Matrix* pOut, CMatrix* pIn);
	static void convertMatrix(Matrix* out, GLfloat* in);
	static void convertMatrix(GLfloat* out,  Matrix* in);
	static bool isSymetrix(Matrix* pMatrix);
	static bool areEqual(Matrix* A, Matrix* B, double tolerance);
	static void normalize(Matrix& M);
	static void crossProductColumn(Matrix& result, Matrix& v1, Matrix& v2);
	static double dotProductColumn(Matrix& v1, Matrix& v2);
	static double angleColumn(Matrix& v1, Matrix& v2);

	//bool runTest(CCharStringArray& report);

};
#endif
