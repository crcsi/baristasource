#include "MatrixHelper.h"
#include "newmat.h"
#include "matrix.h"
#include "CharStringArray.h"
#include "math.h"

CMatrixHelper::CMatrixHelper(void)
{
}

CMatrixHelper::~CMatrixHelper(void)
{
}

/**
 * Computes the cross product of two column matrices
 */
void CMatrixHelper::crossProductColumn(Matrix& result, Matrix& v1, Matrix& v2)
{
	if(v1.Nrows() != 3 || v2.Nrows() != 3)
		return;

	if(v1.Ncols() != 1 || v2.Ncols() != 1)
		return;

	result.ReSize(3, 1);

	double ux = v1(1, 1);
	double uy = v1(2, 1);
	double uz = v1(3, 1);
	
	double vx = v2(1, 1);
	double vy = v2(2, 1);
	double vz = v2(3, 1);

	result(1, 1) = uy*vz - uz*vy;
	result(2, 1) = uz*vx - ux*vz;
	result(3, 1) = ux*vy - uy*vx;
}

/**
 * Normalizes a column or row matrix
 */
void CMatrixHelper::normalize(Matrix& M)
{
	if (M.Ncols() == 1)
	{
		double mag = 0;
		for (int r = 1; r <= M.Nrows(); r++)
		{
			mag += M(r, 1)*M(r, 1);
		}

		mag = sqrt(mag);

		if( fabs(mag) < 1.e-7)
			return;

		for (int r = 1; r <= M.Nrows(); r++)
		{
			M(r, 1) = M(r, 1) / mag;
		}
	}
	else if (M.Nrows() == 1)
	{
		double mag = 0;
		for (int c = 1; c <= M.Ncols(); c++)
		{
			mag += M(1, c)*M(1, c);
		}

		mag = sqrt(mag);

		if( fabs(mag) < 1.e-7)
			return;

		for (int c = 1; c <= M.Ncols(); c++)
		{
			M(1, c) = M(1, c) / mag;
		}
	}
}

double CMatrixHelper::dotProductColumn(Matrix& v1, Matrix& v2)
{
	if ( (v1.Nrows() != 3) || (v1.Ncols() != 1) || (v2.Nrows() != 3) || (v2.Ncols() != 1) )
		return 9999;

	double dot = v1(1, 1)*v2(1, 1)+v1(2, 1)*v2(2, 1)+v1(3, 1)*v2(3, 1);

	return dot;
}

/**
 * Computes the angle between column matrices
 * author: Axel
 */
double CMatrixHelper::angleColumn(Matrix& v1, Matrix& v2)
{
	if ( (v1.Nrows() != 3) || (v1.Ncols() != 1) || (v2.Nrows() != 3) || (v2.Ncols() != 1) )
		return 9999;

	double dot = v1(1, 1)*v2(1, 1)+v1(2, 1)*v2(2, 1)+v1(3, 1)*v2(3, 1);
	double a1 =  v1(1, 1)*v1(1, 1)+v1(2, 1)*v1(2, 1)+v1(3, 1)*v1(3, 1);
	double a2 =  v2(1, 1)*v2(1, 1)+v2(2, 1)*v2(2, 1)+v2(3, 1)*v2(3, 1);

	return acos(dot/(a1*a2));
}

void CMatrixHelper::convertMatrix(CMatrix* pOut, Matrix* pIn)
{
	if (CMatrixHelper::isSymetrix(pIn))
	{
		pOut->Resize(pIn->Nrows(), -1);

		for (int r = 0; r < pIn->Nrows(); r++)
		{
			for (int c = r; c < pIn->Ncols(); c++)
			{
				pOut->Set(pIn->element(r, c), r, c);
			}
		}
	}
	else
	{
		pOut->Resize(pIn->Nrows(), pIn->Ncols());

		for (int r = 0; r < pIn->Nrows(); r++)
		{
			for (int c = 0; c < pIn->Ncols(); c++)
			{
				pOut->Set(pIn->element(r, c), r, c);
			}
		}
	}
}

/**
 * THis assumes a 4x4 opengl matrix
 *
 */
void CMatrixHelper::convertMatrix(Matrix* out, GLfloat* in)
{
	out->ReSize(4, 4);

	(*out)(1, 1) = in[0];
	(*out)(2, 1) = in[1];
	(*out)(3, 1) = in[2];
	(*out)(4, 1) = in[3];
	 
	(*out)(1, 2) = in[4];
	(*out)(2, 2) = in[5];
	(*out)(3, 2) = in[6];
	(*out)(4, 2) = in[7];
	 
	(*out)(1, 3) = in[8];
	(*out)(2, 3) = in[9];
	(*out)(3, 3) = in[10];
	(*out)(4, 3) = in[11];
	 
	(*out)(1, 4) = in[12];
	(*out)(2, 4) = in[13];
	(*out)(3, 4) = in[14];
	(*out)(4, 4) = in[15];
}

/**
 * THis assumes a 4x4 opengl matrix
 *
 */
void CMatrixHelper::convertMatrix(GLfloat* out, Matrix* in)
{
	assert( (in->Nrows() == 4) && (in->Ncols() == 4) );

	out[0] = (GLfloat)(*in)(1, 1);
	out[1] = (GLfloat)(*in)(2, 1);
	out[2] = (GLfloat)(*in)(3, 1);
	out[3] = (GLfloat)(*in)(4, 1);
	   	  		 
	out[4] = (GLfloat)(*in)(1, 2);
	out[5] = (GLfloat)(*in)(2, 2);
	out[6] = (GLfloat)(*in)(3, 2);
	out[7] = (GLfloat)(*in)(4, 2);
	 	  		 
	out[8] = (GLfloat)(*in)(1, 3);
	out[9] = (GLfloat)(*in)(2, 3);
	out[10] =(GLfloat)(*in)(3, 3);
	out[11] =(GLfloat)(*in)(4, 3);
	 	  		 
	out[12] =(GLfloat)(*in)(1, 4);
	out[13] =(GLfloat)(*in)(2, 4);
	out[14] =(GLfloat)(*in)(3, 4);
	out[15] =(GLfloat)(*in)(4, 4);
}

bool CMatrixHelper::isSymetrix(Matrix* pMatrix)
{
	if (pMatrix->Ncols() != pMatrix->Nrows())
		return false;

	for (int r = 0; r < pMatrix->Nrows()-1; r++)
	{
		for (int c = r+1; c < pMatrix->Ncols(); c++)
		{
			if (pMatrix->element(r, c) != pMatrix->element(c, r))
				return false;
		}
	}

	return true;
}

void CMatrixHelper::convertMatrix(Matrix* pOut, CMatrix* pIn)
{
	if (pIn->IsSymmetric())
	{
		pOut->ReSize(pIn->m_rows, pIn->m_rows);
		
		for (int r = 0; r < pIn->m_rows; r++)
		{
			for (int c = 0; c < pIn->m_rows; c++)
			{
				pOut->element(r, c) = pIn->Get(r, c);
			}
		}
	}
	else
	{
		pOut->ReSize(pIn->m_rows, pIn->m_cols);

		for (int r = 0; r < pIn->m_rows; r++)
		{
			for (int c = 0; c < pIn->m_cols; c++)
			{
				pOut->element(r, c) = pIn->Get(r, c);
			}
		}
	}
}

bool CMatrixHelper::areEqual(Matrix* A, Matrix* B, double tolerance)
{
	if (A->Ncols() != B->Ncols())
		return false;

	if (A->Nrows() != B->Nrows())
		return false;

	for (int r = 1; r <= A->Nrows(); r++)
	{
		for (int c = 1; c <= A->Ncols(); c++)
		{
			double a = (*A)(r, c);
			double b = (*B)(r, c);
			double diff = ::fabs(a - b);

			if (diff > tolerance)
				return false;
		}
	}

	return true;
}
/*
bool CMatrixHelper::runTest(CCharStringArray& report)
{
	return true;
}

*/


