

#include "newmat.h"
#include "CharStringArray.h"
#include "BlockDiagonalMatrix.h"
#include "math.h"
#include "MatrixHelper.h"

#include "BestFit3DCircle.h"

CBestFit3DCircle::CBestFit3DCircle(void)
{
}

CBestFit3DCircle::~CBestFit3DCircle(void)
{
}

/**
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Column Matrix of approximations of the parameters of the least squares fit
  */
bool CBestFit3DCircle::approximate (Matrix* approx, Matrix* consts, Matrix* obs)
{
	approx->ReSize(8, 1);

	double pi = 4.0*atan(1.0);

	Matrix Norm(3,1);
	Matrix Compare(3,1);

	int count=0;
	int nRows =obs->Nrows();
	for(int i=0; i < nRows; i++)
	{
		Matrix master(3,1);
		master(1,1) = (*obs)(i+1, 1);
		master(2,1) = (*obs)(i+1, 2);
		master(3,1) = (*obs)(i+1, 3);

		for(int j=i+1; j < nRows; j++)
		{
			Matrix slave(3,1);
			slave(1,1) = (*obs)(j+1,1);
			slave(2,1) = (*obs)(j+1,2);
			slave(3,1) = (*obs)(j+1,3);

			Matrix tempNorm;
			CMatrixHelper::crossProductColumn(tempNorm, master, slave);
			CMatrixHelper::normalize(tempNorm);

			if(count == 0)
			{
				Compare = tempNorm;
				Norm = tempNorm;
				count++;

				continue;
			}
			else
			{
				double angle = CMatrixHelper::angleColumn(Compare, tempNorm);

				if(angle > pi/2.0)
				{
					tempNorm *= -1.0;
				}
			}

			Norm += tempNorm;
			count++;
		}
		
	}

	Norm /= count;
	CMatrixHelper::normalize(Norm);

	double a = Norm(1,1);
	double b = Norm(2,1);
	double c = Norm(3,1); 

	// c must allways be positive
	if (c < 0.0)
	{
		a = -a;
		b = -b;
		c = -c;
	}

	(*approx)(1, 1) = a;
	(*approx)(2, 1) = b;
	(*approx)(3, 1) = c;

	double d(0.0);

	// for d

	double x0 = 0;
	double y0 = 0;
	double z0 = 0;
	for(int i=0; i < nRows; i++)
	{
	//	Matrix pt(3,1);
		double x = (*obs)(i+1, 1);
		double y = (*obs)(i+1, 2);
		double z = (*obs)(i+1, 3);

		d += a*x + b*y + c*z;

		x0 += x;
		y0 += y;
		z0 += z;
	}

	x0 /= nRows;
	y0 /= nRows;
	z0 /= nRows;

	d /= nRows;

	(*approx)(4, 1) = d;
	(*approx)(5, 1) = x0;
	(*approx)(6, 1) = y0;
	(*approx)(7, 1) = z0;

	double r = 0;
	for(int i=0; i < nRows; i++)
	{
	//	Matrix pt(3,1);
		double x = (*obs)(i+1, 1);
		double y = (*obs)(i+1, 2);
		double z = (*obs)(i+1, 3);

		d += a*x + b*y + c*z;

		double dx = x - x0;
		double dy = y - y0;
		double dz = z - z0;

		r += sqrt(dx*dx + dy*dy + dz*dz);
	}

	r /= nRows;

	(*approx)(8, 1) = r;

	return true;
}

/**
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Function evaluated at the given observation unsing the given parameters
  */
bool CBestFit3DCircle::Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Fi->ReSize(1, 1);

    double a =  (*parms)(1, 1);
    double b =  (*parms)(2, 1);
	double c =  (*parms)(3, 1);
	double d =  (*parms)(4, 1);
	double x0 = (*parms)(5, 1);
	double y0 = (*parms)(6, 1);
	double z0 = (*parms)(7, 1);
	double r = (*parms)(8, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double z = (*ob)(1, 3);

	double t1 = x - a*a*x - a*b*y - a*c*z - a*d - x0;
	double t2 = y - a*b*x - b*b*y - b*c*z - b*d - y0;
	double t3 = z - a*c*x - b*c*y - c*c*z - c*d - z0;

    (*Fi)(1, 1) = t1*t1 + t2*t2 + t3*t3 - r*r;

    return  true;
}

/**
 * @param parms Column Matrix of approximations of the parameters
 * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
 * @return A Matrix of partials with respect to paramters evaluated at ob
 */
bool CBestFit3DCircle::Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Ai->ReSize(1, 8);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
	double c = (*parms)(3, 1);
	double d = (*parms)(4, 1);
	double x0 = (*parms)(5, 1);
	double y0 = (*parms)(6, 1);
	double z0 = (*parms)(7, 1);
	double r = (*parms)(8, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double z = (*ob)(1, 3);

	double t1 = x - a*a*x - a*b*y - a*c*z - a*d - x0;
	double t2 = y - a*b*x - b*b*y - b*c*z - b*d - y0;
	double t3 = z - a*c*x - b*c*y - c*c*z - c*d - z0;

	double p_f_a = 2*t1*(-2*a*x - b*y - c*z - d) - 2*t2*b*x - 2*t3*c*x;
	double p_f_b = -2*t1*a*y +2*t2*(-a*x - 2*b*y -c*z - d) - 2*t3*c*y;
	double p_f_c = -2*t1*a*z -2*t2*b*z + 2*t3*(-a*x -b*y -2*c*z -d);
	double p_f_d = -2*a*t1 -2*b*t2 - 2*c*t3;
	double p_f_x0 = -2*t1;
	double p_f_y0 = -2*t2;
	double p_f_z0 = -2*t3;
	double p_f_r = -2*r;

	(*Ai)(1, 1) = p_f_a;
	(*Ai)(1, 2) = p_f_b;
	(*Ai)(1, 3) = p_f_c;
	(*Ai)(1, 4) = p_f_d;
	(*Ai)(1, 5) = p_f_x0;
	(*Ai)(1, 6) = p_f_y0;
	(*Ai)(1, 7) = p_f_z0;
	(*Ai)(1, 8) = p_f_r;

	return true;
}

/**
  * @return Bi Matrix of patials with respect to observations for the ith observation
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  */
bool CBestFit3DCircle::Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Bi->ReSize(1, 3);

    double a =	(*parms)(1, 1);
    double b =	(*parms)(2, 1);
	double c =	(*parms)(3, 1);
	double d =	(*parms)(4, 1);
	double x0 = (*parms)(5, 1);
	double y0 = (*parms)(6, 1);
	double z0 = (*parms)(7, 1);
	double r = (*parms)(8, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double z = (*ob)(1, 3);

	double t1 = x - a*a*x - a*b*y - a*c*z - a*d - x0;
	double t2 = y - a*b*x - b*b*y - b*c*z - b*d - y0;
	double t3 = z - a*c*x - b*c*y - c*c*z - c*d - z0;

	double p_f_x = 2*t1*(1-a*a) -2*t2*a*b - 2*t3*a*c;
	double p_f_y = -2*t1*a*b + 2*t2*(1-b*b) - 2*t3*b*c;
	double p_f_z = -2*t1*a*c - 2*t2*b*c + 2*t3*(1-c*c);

    (*Bi)(1, 1) = p_f_x;
    (*Bi)(1, 2) = p_f_y;
    (*Bi)(1, 3) = p_f_z;

    return true;
}

/**
  * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
  */
void CBestFit3DCircle::getParameterNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "a";

	str = names->Add();
	*str = "b";

	str = names->Add();
	*str = "c";

	str = names->Add();
	*str = "d";

	str = names->Add();
	*str = "x0";

	str = names->Add();
	*str = "y0";

	str = names->Add();
	*str = "z0";

	str = names->Add();
	*str = "r";
}

/**
 * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
 */
void CBestFit3DCircle::getObservationNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "x";

	str = names->Add();
	*str = "y";
	
	str = names->Add();
	*str = "z";
}

/**
 * @return a String cantainint the name of the best fit ex. "Best fit Line"
 */
void CBestFit3DCircle::getName(CCharString& name)
{
	name = "Best-Fit 3D Circle";
}

/**
 * Sets the a priori parameters information along with a priori
 * covariance Matrix
 */
void CBestFit3DCircle::setParameters(Matrix* parms, CBlockDiagonalMatrix* qxx)
{
	// first delete object (if there)
	this->deleteObjects();

	this->parms0 = new Matrix;
	*this->parms0 = *parms;
	this->Qxx = new CBlockDiagonalMatrix(*qxx);
}

/**
  * Gets the apriori covariance Matrix (if there)
  */
CBlockDiagonalMatrix* CBestFit3DCircle::getQxx()
{
	return this->Qxx;
}

void CBestFit3DCircle::addConstraint(Matrix* N, Matrix* t, Matrix* parms)
{
    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
	double c = (*parms)(3, 1);
	double d = (*parms)(4, 1);
	double x0 = (*parms)(5, 1);
	double y0 = (*parms)(6, 1);
	double z0 = (*parms)(7, 1);
	double r = (*parms)(8, 1);

	Matrix A(1, 8);

	Matrix w(1, 1);

	A(1, 1) = 2*a;
	A(1, 2) = 2*b;
	A(1, 3) = 2*c;
	A(1, 4) = 0;
	A(1, 5) = 0;
	A(1, 6) = 0;
	A(1, 7) = 0;
	A(1, 8) = 0;

	w(1, 1) = a*a + b*b + c*c -1;

	Matrix P(1, 1);
	P = 0;

	P(1, 1) = 1.0e30;

	Matrix ATA = A.t()*P*A;

	*N += ATA;

	Matrix ATw = A.t()*P*w;

	*t += ATw;

	// second constraint
	A(1, 1) = x0;
	A(1, 2) = y0;
	A(1, 3) = z0;
	A(1, 4) = 1;
	A(1, 5) = a;
	A(1, 6) = b;
	A(1, 7) = c;
	A(1, 8) = 0;

	w(1, 1) = a*x0 + b*y0 + c*z0 + d;

	ATA = A.t()*P*A;

	*N += ATA;

	ATw = A.t()*P*w;

	*t += ATw;

	int hmm = 0;
}
