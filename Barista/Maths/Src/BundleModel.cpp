
/**
 * @class CBundleModel
 * Title:        Barista
 * Description:
 * Copyright:    Copyright (c) Harry Hanley
 * Company:      University of Melbourne
 * @author Harry Handley
 * @version 1.0
 *
 * This class extend CLeastSquaresModel and represents a generic
 * Least-Squares Bundle adjustment.
 * When forming the normal equation, this class expects that the station
 * parameters come first followed by the point parameters then the camera
 * parameters.
 *
 * It is assumed that the bundle has station and point parameters and may or may
 * not have camera parameters
 *
 */


#include "MUIntegerArray.h"
#include "BundleModel.h"

CBundleModel::CBundleModel()
{
    this->parms0 = NULL;
    this->Qxx = NULL;
    this->nPoints = 0;
    this->nStations = 0;
    this->nCameras = 0;

    // to be filled in by extended class
    this->nCameraParameters = 0;
    this->nStationParameters = 0;
    this->nPointParameters = 3;
    this->nStationConstants = 0;

    this->CameraNames = NULL;
    this->StationNames = NULL;
    this->PointNames = NULL;


    // This will be an array of size n
    // This first value is the station index for the current observation (xy)
    // THe second value is the (ground) point index for the current observation
    this->currentObservationIndices = NULL;
}

CBundleModel::~CBundleModel(void)
{
	this->deleteObjects();
}

bool CBundleModel::approximate (Matrix* approx, Matrix* consts, Matrix* obs)
{
    assert (parms0 != NULL);
   //     throw new java.lang.NullPointerException("setParameters needs to be called first");

//    return parms0;

    return true;
}

void CBundleModel::setParameters(Matrix* parms, CBlockDiagonalMatrix* Qxx)
{
    if (Qxx->getRowDimension() != Qxx->getColumnDimension())
        assert(false);

    if (parms->Nrows() != Qxx->getRowDimension())
        assert(false);

    if (this->parms0 != NULL)
        delete this->parms0;

    this->parms0 = new Matrix();

    *this->parms0 = *parms;

    if (this->Qxx != NULL)
        delete this->Qxx;

    this->Qxx = new CBlockDiagonalMatrix();

    *this->Qxx = *Qxx;
}

void CBundleModel::setParamterSizes(int nStation, int nPoint, int nCamera)
{
    this->nStationParameters = nStation;
    this->nPointParameters = nPoint;
    this->nCameraParameters = nCamera;
}

void CBundleModel::setParameters(Matrix* parms, CBlockDiagonalMatrix* Qxx, int nStations, int nPoints)
{
    this->nStations = nStations;
    this->nPoints = nPoints;
    this->setParameters(parms, Qxx);
}


CBlockDiagonalMatrix* CBundleModel::getQxx()
{
    return this->Qxx;
}

/**
*
*/
void CBundleModel::accumulate(Matrix* N, Matrix* t, Matrix* parms, Matrix* Constant,
                     Matrix* Observation, Matrix* previousResid, Matrix* Q)
{
    // Some convenient constants
    int nStaParms = this->nStationParameters;
    int nPntParms = this->nPointParameters;
    int nCamParms = this->nCameraParameters;

    Matrix f; 
    this->Fi(&f, parms, Constant, Observation);
    Matrix Ai; 
    this->Ai(&Ai, parms, Constant, Observation);
    Matrix Bi;
    this->Bi(&Bi, parms, Constant, Observation);

    // Form the equivalent weight matrix
 //  Matrix We = Bi.times(Q).times(Bi.transpose()).inverse();
    
     Matrix We = (Bi * *Q * Bi.t()).i();

    // accumulate normals matrix
 //   Matrix ATWeA = Ai.transpose().times(We).times(Ai);
    Matrix ATWeA = Ai.t() * We * Ai;

    // Get the various sub matrices out of ATWeA so they can go into
    // the corect spot in the Normals
    Matrix ATWeAStation = ATWeA.SubMatrix(1, nStaParms, 1, nStaParms);
   

    Matrix ATWeAPoint = ATWeA.SubMatrix(nStaParms+1, nStaParms+nPntParms, nStaParms+1, nStaParms+nPntParms);
    Matrix ATWeAStationPoint = ATWeA.SubMatrix(1, nStaParms, nStaParms+1, nStaParms+nPntParms);

    Matrix ATWeACamera;
    Matrix ATWeAStationCamera;
    Matrix ATWeAPointCamera;

    if (nCamParms > 0)
    {
        ATWeACamera = ATWeA.SubMatrix(nStaParms+nPntParms+1, nStaParms+nPntParms+nCamParms,
                                        nStaParms+nPntParms+1, nStaParms+nPntParms+nCamParms);

        ATWeAStationCamera = ATWeA.SubMatrix(1, nStaParms,
            nStaParms+nPntParms+1, nStaParms+nPntParms+nCamParms);

        ATWeAPointCamera = ATWeA.SubMatrix(nStaParms+1, nStaParms+nPntParms,
            nStaParms+nPntParms+1, nStaParms+nPntParms+nCamParms);

    }

    int i0 = *this->currentObservationIndices->GetAt(0);
    int i1 = *this->currentObservationIndices->GetAt(1);

    int stationIndex = i0 * nStaParms;
    int pointIndex = this->nStations* nStaParms + i1 * nPntParms;

    // Check if this is the type of bundle that has a camera model
    int cameraIndex = -1;
    if (nCamParms > 0)
    {
        int i2 = *this->currentObservationIndices->GetAt(2);
        cameraIndex = pointIndex + this->nPoints*nPntParms + i2 * nCamParms;
    }

    // Get sub matrix
    Matrix NStation = N->SubMatrix(stationIndex+1, stationIndex + nStaParms, stationIndex+1, stationIndex + nStaParms);
    // Accumulate
    NStation = NStation + ATWeAStation;
    // put it back
   
//    N->setMatrix(stationIndex, stationIndex + nStaParms-1, stationIndex, stationIndex + nStaParms-1, NStation);
    this->setMatrix(stationIndex+1, stationIndex + nStaParms, stationIndex+1, stationIndex + nStaParms, N, &NStation);

    // Get sub matrix
    Matrix NPoint = N->SubMatrix(pointIndex+1, pointIndex + nPntParms, pointIndex+1, pointIndex + nPntParms);
    // Accumulate
    NPoint = NPoint + ATWeAPoint;
    // put it back
//    N.setMatrix(pointIndex, pointIndex + nPntParms-1, pointIndex, pointIndex + nPntParms-1, NPoint);
    this->setMatrix(pointIndex+1, pointIndex + nPntParms, pointIndex+1, pointIndex + nPntParms, N, &NPoint);

    if (nCamParms > 0)
    {
        // Get sub matrix
        Matrix NCamera = N->SubMatrix(cameraIndex+1, cameraIndex + nCamParms,
                                        cameraIndex+1, cameraIndex + nCamParms);
        // Accumulate
        NCamera = NCamera + ATWeACamera;
        // put it back
 //      N.setMatrix(cameraIndex, cameraIndex + nCamParms-1, cameraIndex, cameraIndex + nCamParms-1, NCamera);
        this->setMatrix(cameraIndex+1, cameraIndex + nCamParms, cameraIndex+1, cameraIndex + nCamParms, N, &NCamera);
    }

    // Get sub matrix
    Matrix NStationPoint = N->SubMatrix(stationIndex+1, stationIndex+nStaParms, pointIndex+1, pointIndex+nPntParms);
    // Accumulate
    NStationPoint = NStationPoint + ATWeAStationPoint;
    // put it back
 //   N.setMatrix(stationIndex, stationIndex+nStaParms-1, pointIndex, pointIndex+nPntParms-1, ATWeAStationPoint);
    this->setMatrix(stationIndex+1, stationIndex+nStaParms, pointIndex+1, pointIndex+nPntParms, N, &ATWeAStationPoint);
    // and its symetrical part
//    N.setMatrix(pointIndex, pointIndex+nPntParms-1, stationIndex, stationIndex+nStaParms-1, NStationPoint.transpose());
    Matrix NStationPointT = NStationPoint.t();
    this->setMatrix(pointIndex+1, pointIndex+nPntParms, stationIndex+1, stationIndex+nStaParms, N, &NStationPointT);

    if (nCamParms > 0)
    {
        // Get sub matrix
        Matrix NStaionCamera = N->SubMatrix(stationIndex+1, stationIndex+nStaParms,
                                            cameraIndex+1, cameraIndex+nCamParms);
        // Accumulate
        NStaionCamera = NStaionCamera + ATWeAStationCamera;
        // put it back
//        N.setMatrix(stationIndex, stationIndex+nStaParms-1, cameraIndex, cameraIndex+nCamParms-1, NStaionCamera);
        this->setMatrix(stationIndex+1, stationIndex+nStaParms, cameraIndex+1, cameraIndex+nCamParms, N, &NStaionCamera);
        // and its symetrical part
//        N.setMatrix(cameraIndex, cameraIndex+nCamParms-1, stationIndex, stationIndex+nStaParms-1, NStaionCamera.transpose());
        Matrix NStaionCameraT = NStaionCamera.t();
        this->setMatrix(cameraIndex+1, cameraIndex+nCamParms, stationIndex+1, stationIndex+nStaParms, N, &NStaionCameraT);

        // Get sub matrix
        Matrix NPointCamera = N->SubMatrix(pointIndex+1, pointIndex+nPntParms,
                                            cameraIndex+1, cameraIndex+nCamParms);
        // Accumulate
        NPointCamera = NPointCamera + ATWeAPointCamera;
        // put it back
  //     N.setMatrix(pointIndex, pointIndex+nPntParms-1, cameraIndex, cameraIndex+nCamParms-1, NPointCamera);
        this->setMatrix(pointIndex+1, pointIndex+nPntParms, cameraIndex+1, cameraIndex+nCamParms, N, &NPointCamera);
        // and its symetrical part
   //     N.setMatrix(cameraIndex, cameraIndex+nCamParms-1, pointIndex, pointIndex+nPntParms-1, NPointCamera.transpose());
        Matrix NPointCameraT = NPointCamera.t();
        this->setMatrix(cameraIndex+1, cameraIndex+nCamParms, pointIndex+1, pointIndex+nPntParms, N, &NPointCameraT);
    }

    // accumulate right hand side
    Matrix ATWef = Ai.t() * We * f;
    Matrix ATWefStation = ATWef.SubMatrix(1, nStaParms, 1, 1);
    Matrix ATWefPoint = ATWef.SubMatrix(nStaParms+1, nStaParms+nPntParms, 1, 1);
    Matrix ATWefCamera;

    if (nCamParms > 0)
    {
        ATWefCamera = ATWef.SubMatrix(nStaParms+nPntParms+1, nStaParms+nPntParms+nCamParms, 1, 1);
    }

    // Get sub Matrix
    Matrix tStation = t->SubMatrix(stationIndex+1, stationIndex+nStaParms, 1, 1);
    // Acumulate
    tStation = tStation + ATWefStation;
    // put it back
 //   t.setMatrix(stationIndex, stationIndex+nStaParms-1, 0, 0, tStation);
    this->setMatrix(stationIndex+1, stationIndex+nStaParms, 1, 1, t, &tStation);

    // Get sub Matrix
    Matrix tPoint = t->SubMatrix(pointIndex+1, pointIndex+nPntParms, 1, 1);
    // Acumulate
    tPoint = tPoint + ATWefPoint;
    // put it back
 //   t.setMatrix(pointIndex, pointIndex+nPntParms-1, 0, 0, tPoint);
    this->setMatrix(pointIndex+1, pointIndex+nPntParms, 1, 1, t, &tPoint);

    if (nCamParms > 0)
    {
        // Get sub Matrix
        Matrix tCamera = t->SubMatrix(cameraIndex+1, cameraIndex+nCamParms, 1, 1);
        // Acumulate
        tCamera = tCamera + ATWefCamera;
        // put it back
  //      t.setMatrix(cameraIndex, cameraIndex+nCamParms-1, 0, 0, tCamera);
        this->setMatrix(cameraIndex+1, cameraIndex+nCamParms, 1, 1, t, &tCamera);
    }
}

void CBundleModel::calculateResiduals(Matrix* resids, Matrix* Delta, Matrix* parms, Matrix* Constant,
                     Matrix* Observation, Matrix* previousResid, Matrix* Q)
{
    Matrix f;
    this->Fi(&f, parms, Constant, Observation);
    
    Matrix Ai;
    this->Ai(&Ai, parms, Constant, Observation);
    
    Matrix Bi;
    this->Bi(&Bi, parms, Constant, Observation);

    // Form the equivalent weight matrix
    Matrix We = (Bi * *Q * Bi.t()).i();

    Matrix subDelta;
    this->getSubDelta(&subDelta, Delta);

    Matrix Ve = Ai * subDelta + f;

    *resids = *Q * Bi.t() * We * Ve;

    *resids = *resids * -1.;

//    return V;
}

void CBundleModel::setObservationIndices(CMUIntegerArray* Indices)
{
    this->currentObservationIndices = Indices;
}

void CBundleModel::getObservationNames(CCharStringArray* names)
{
    CCharString* name = names->Add();
    *name = "x";

    name = names->Add();
    *name = "y";

}

int CBundleModel::getStationCount()
{
    return this->nStations;
}

int CBundleModel::getStationParameterCount()
{
    return this->nStationParameters;
}

int CBundleModel::getPointCount()
{
    return this->nPoints;
}

int CBundleModel::getPointParameterCount()
{
    return this->nPointParameters;
}

int CBundleModel::getCameraCount()
{
    return this->nCameras;
}

int CBundleModel::getCameraParameterCount()
{
    return this->nCameraParameters;
}

/**
    * Gets a sub Matrix from the current iteration's Delta solution
    * This sub Matrix represents the parameters cooresponding to the
    * observation specified by the observationIndices
    * @param Delta the Delta Matrix (solution of parameters)
    */
void CBundleModel::getSubDelta(Matrix* subDelta, Matrix* Delta)
{
    // calculate the number of parameters appearing in the sub matrix
    int nParms = this->nStationParameters+this->nCameraParameters+this->nPointParameters;

    subDelta->ReSize(nParms, 1);

    int i0 = *this->currentObservationIndices->GetAt(0);
    int i1 = *this->currentObservationIndices->GetAt(1);

    int stationIndex = i0 * this->nStationParameters;
    int pointIndex = i1 * this->nPointParameters +
    this->nStationParameters * this->nStations;
    int cameraIndex = -1;

    if (this->nCameraParameters > 0)
    {
        int i2 = *this->currentObservationIndices->GetAt(2);

        cameraIndex = i2 * this->nCameraParameters +
        this->nStationParameters * this->nStations +
        this->nPointParameters * this->nPoints;
    }

    for (int i = 0; i < this->nStationParameters; i++)
        subDelta->element(i, 0) = Delta->element(stationIndex + i, 0);

    for (int i = 0; i < this->nPointParameters; i++)
        subDelta->element(i+this->nStationParameters, 0) = Delta->element(pointIndex + i, 0);

    for (int i = 0; i < this->nCameraParameters; i++)
        subDelta->element(i+this->nStationParameters+this->nPointParameters, 0) = Delta->element(cameraIndex + i, 0);

   // return subDelta;
}

void CBundleModel::setCameraNames(CCharStringArray* v)
{
    this->CameraNames = v;
}

void CBundleModel::setStationNames(CCharStringArray* v)
{
    this->StationNames = v;
}

void CBundleModel::setPointNames(CCharStringArray* v)
{
    this->PointNames = v;
}

CCharStringArray* CBundleModel::getCameraNames()
{
    return this->CameraNames;
}

CCharStringArray* CBundleModel::getStationNames()
{
    return this->StationNames;
}

CCharStringArray* CBundleModel::getPointNames()
{
    return this->PointNames;
}

/**
    * Sets a particular point as a "Control" point
    */
void CBundleModel::setControlPoint(int index, Matrix* xyz, Matrix* Covariance)
{
    int stationOffset = this->nStationParameters*this->nStations;
    int pointOffset = index * this->nPointParameters;

    for (int i = 0; i < this->nPointParameters; i++)
    {
        this->parms0->element(stationOffset + pointOffset + i, 0) = xyz->element(0, i);
    }

    Matrix* Q = this->Qxx->getSubMatrix(this->nStations+index);

    this->setMatrix(1, Q->Ncols(), 1, Q->Nrows(), Q, Covariance);
}

void CBundleModel::freePoint(int index, Matrix* Covariance)
{
    int stationOffset = this->nStationParameters*this->nStations;
    int pointOffset = index * this->nPointParameters;

    Matrix* Q = this->Qxx->getSubMatrix(this->nStations+index);
    this->setMatrix(1, Q->Ncols(), 1, Q->Nrows(), Q, Covariance);
}

/**
 * A helper function for setting a submatrix inside a larger matrix
 *
 * 
 */
void CBundleModel::setMatrix(int startRow, int endRow, int startCol, int endCol, Matrix* dest, Matrix* src)
{
    for (int r = 1, R = startRow; R <= endRow; r++, R++)
    {
        for (int c = 1, C = startCol; C <= endCol; c++, C++)
        {
            (*dest)(R, C) = (*src)(r, c);
        }
    }
}
