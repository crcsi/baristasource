////////////////////////////////////////////////////////////////////////////////
// File: Matrix.cpp		Implements CMatrix class

// Standard includes
#include <math.h>
#include <assert.h>
#include <stdio.h>

// External includes
#include "FileArchive.h"
#include "TextFile.h"

// Local includes
#include "DoubleVector.h"
#include "constants.h"

#include "matrix.h"
#include "MatrixListener.h"

////////////////////////////////////////////////////////////////////////////////
// CMatrix::CMatrix()
//
// constructor
//
CMatrix::CMatrix()
{
	m_rows = 0;
	m_cols = 0;
	m_ppData = NULL;
	this->listener = NULL;
}

////////////////////////////////////////////////////////////////////////////////
// void CMatrix::CMatrix(int rows, int cols)
//
// CMatrix Constructor given rows and columns
//
CMatrix::CMatrix(int rows, int cols)
{
	m_rows = 0;
	m_cols = 0;
	m_ppData = NULL;
	Set(rows,cols);
	this->listener = NULL;

}

CMatrix::CMatrix(const CMatrix &rSrc)
{ 
	m_rows = -1;
    m_cols = -1;
    m_ppData = NULL;
 	this->listener = NULL;

	Copy(rSrc); 
}

CMatrix::CMatrix(const CMatrix *pSrc)
{ 
	m_rows = -1;
    m_cols = -1;
    m_ppData = NULL;
 	this->listener = NULL;

	Copy(pSrc); 
}


CMatrix::CMatrix(CDoubleVector &Vec)
{
	m_rows = 0;
	m_cols = 0;
	m_ppData = NULL;
	this->listener = NULL;

	*this=Vec;
}

////////////////////////////////////////////////////////////////////////////////
// void CMatrix::Copy(const CMatrix &rSrc)
//
// copy constructor
//
void CMatrix::Copy(const CMatrix &rSrc)
{
	m_rows = rSrc.m_rows;
    m_cols = rSrc.m_cols;

	// TESTING .... clearing
	if ( m_ppData != NULL )
	{
		for( int i = 0; i < m_rows; i++)
			delete m_ppData[i];
   
		delete [] m_ppData;   

		m_ppData = NULL;
	}
    
    m_ppData = new double* [m_rows];

    // if c negative, matrix is upper triangular, symmetric
	if ( m_cols < 0 )
	{
		for( int i = 0; i < m_rows; i++)
			m_ppData[i] = new double[m_rows-i];

		for( int i = 0; i < m_rows; i++)
			memmove(m_ppData[i], rSrc.m_ppData[i], (m_rows-i) * sizeof(double));
	}
	else // matrix is rectangular
	{
		for( int i = 0; i < m_rows; i++)
			m_ppData[i]= new double[m_cols];

		for( int i = 0; i < m_rows; i++)
			memmove(m_ppData[i], rSrc.m_ppData[i], m_cols * sizeof(double));
	}
}

////////////////////////////////////////////////////////////////////////////////
// void CMatrix::Copy(const CMatrix *pSrc)
//
// copy constructor
//
void CMatrix::Copy(const CMatrix *pSrc)
{
	Copy(*pSrc);
}

////////////////////////////////////////////////////////////////////////////////
// CMatrix::~CMatrix()
//
// destructor
//
CMatrix::~CMatrix()
{
	if (m_ppData != NULL)
	{
		for( int i = 0; i < m_rows; i++ )
			delete m_ppData[i];
   
		delete [] m_ppData;   

		m_ppData = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////
// CMatrix& CMatrix::operator +=(const CMatrix& rSrc)
//
// += operator
//
CMatrix& CMatrix::operator +=(const CMatrix& rSrc)
{
	assert((m_rows == rSrc.m_rows) && (m_cols == rSrc.m_cols));
	
    // if cols is negative, matrices are upper triangular, symmetric
	if ( IsSymmetric() )
	{
		for( int i = 0; i < m_rows; i++ )
		{
			for( int j = 0; j < (m_rows-i); j++ )
				m_ppData[i][j] += rSrc.m_ppData[i][j];
		}
	}
	
	// matrices not upper triangular
	else
	{
		for( int i = 0; i < m_rows; i++ )
		{
			for( int j = 0; j < m_cols; j++ )
				m_ppData[i][j] += rSrc.m_ppData[i][j];
		}
	}
				
	return *this;
}

////////////////////////////////////////////////////////////////////////////////
// CMatrix CMatrix::operator -(CMatrix op2)
//
// - operator
//
CMatrix CMatrix::operator -(const CMatrix &rSrc)
{
	CMatrix Temp(rSrc.m_rows,rSrc.m_cols);
	
    // if cols is negative, matrices are upper triangular, symmetric
	if ( m_cols < 0 )
	{
		for( int i = 0; i < m_rows; i++ )
		{
			for( int j = 0; j < (m_rows-i); j++ )
				Temp.m_ppData[i][j] = m_ppData[i][j] - rSrc.m_ppData[i][j];
		}
	}
	
	// matrices not upper triangular
	else
	{
		for( int i = 0; i < m_rows; i++ )
		{
			for( int j = 0; j < m_cols; j++ )
				Temp.m_ppData[i][j] = m_ppData[i][j] - rSrc.m_ppData[i][j];
		}
	}
				
	return Temp;
}

////////////////////////////////////////////////////////////////////////////////
// CMatrix CMatrix::operator +(CMatrix op2)
//
// + operator
//
CMatrix CMatrix::operator +(const CMatrix &rSrc)
{
	CMatrix Temp(rSrc.m_rows,rSrc.m_cols);
	
    // if cols is negative, matrices are upper triangular, symmetric
	if ( m_cols < 0 )
	{
		for( int i = 0; i < m_rows; i++ )
		{
			for( int j = 0; j < (m_rows-i); j++ )
				Temp.m_ppData[i][j] = m_ppData[i][j] + rSrc.m_ppData[i][j];
		}
	}
	
	// matrices not upper triangular
	else
	{
		for( int i = 0; i < m_rows; i++ )
		{
			for( int j = 0; j < m_cols; j++ )
				Temp.m_ppData[i][j] = m_ppData[i][j] + rSrc.m_ppData[i][j];
		}
	}
				
	return Temp;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Set(int rows, int cols)
//
// initializer when constructor not used
//
bool CMatrix::Set(int rows, int cols)
{
	int i;

	if ( m_rows == rows && m_cols==cols)
	{
		SetToZero();
		return true;
	}

	// delete existing matrix elements
	if ( m_ppData != NULL )
	{
		for( i = 0; i < m_rows; i++ )
			delete [] m_ppData[i];
		delete [] m_ppData;
	}

	// reset number of rows and columns
	m_rows = rows;
    m_cols = cols;

	// if matrix is upper triangular or symmetric m_cols should be -1
	if (m_cols < 0)
		m_cols = -1;

	// if no rows, we're done, set m to NULL, get out and return ok
	if ( m_rows == 0 )
	{
		m_ppData = NULL;
		return true;
	}

	// if allocation doesn't work, return failure
	m_ppData = new double* [m_rows];
	if ( !m_ppData )
		return false;
    
    // if cols is negative, matrix is upper triangular, symmetric
	if ( m_cols < 0 )
	{
		for( i = 0; i < m_rows; i++ )
			m_ppData[i]= new double[m_rows-i];

		//initialization to zero
		for( i = 0; i < m_rows; i++ )
			memset(m_ppData[i],0,(m_rows-i)*sizeof(double));
	}

	// matrix is not upper triangular
	else
	{
		for( i = 0; i < m_rows; i++ )
			m_ppData[i]= new double[m_cols];

		//initialization to zero 

		for( i = 0; i < m_rows; i++ ) 
			memset(m_ppData[i],0,m_cols*sizeof(double));
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// void CMatrix::Resize(int newrows, int newcols)
//
// resizes matrix by creating new one and copying old into new
// all new matrix elements are set to zero
//
void CMatrix::Resize(int newrows, int newcols)
{
	if (m_rows==newrows && m_cols==newcols)	//nothing to do if matrix size is correct
		return;

	// temporary storage for existing CMatrix
	CMatrix temp(*this);
	
	// delete existing matrix and reset with new rows and columns
	for( int i = 0; i < m_rows; i++ )
		delete [] m_ppData[i];
	delete [] m_ppData;
	
	Set(newrows,newcols);
	
	// copy temp matrix into new 
	for( int i = 0; i < temp.m_rows; i++ )
		memmove(m_ppData[i], temp.m_ppData[i], (temp.m_rows - i)*sizeof(double));		
	
    m_rows = newrows;
    m_cols = newcols;
}

////////////////////////////////////////////////////////////////////////////////
// void CMatrix::InsertColumns(int nCols, int nStartCol)
//
// inserts nCols new columns into matrix starting at nStartCol
// all new matrix elements are set to zero
//
void CMatrix::InsertColumns(int nCols, int nStartCol)
{
	int i;
	int nNewRows;
	int nNewCols;
	double **mnew;
	int nColsToSkip = nStartCol + nCols;

	// compute new row and column dimensions
	if ( m_cols == -1 )
	{
		nNewRows = m_rows + nCols;
		nNewCols = -1;
	}

	else
	{
		nNewRows = m_rows;
		nNewCols = m_cols + nCols;
	}

	// Create rows of new matrix
    mnew = new double* [nNewRows];
    
    // matrix is upper triangular, symmetric
	// NOTE: in this case we are adding both rows and columns
	if ( m_cols < 0 )
	{
		// create columns and initialize elements to zero
		for( i = 0; i < nNewRows; i++)
		{
			mnew[i]= new double[nNewRows-i];
			memset(mnew[i],0,(nNewRows-i)*sizeof(double));
		}

		// do the shifting from old matrix to new up to start of new rows
		for( i = 0; i < nStartCol; i++)
		{
			// move all elements upto start of new columns
			memmove(mnew[i], m_ppData[i], (nStartCol - i)*sizeof(double));
			
			// move all elements after end of new columns
			//memmove(mnew[nColsToSkip - i], m_ppData[nStartCol - i], (r - nStartCol)*sizeof(double));		
			for( int j = nStartCol; j < m_rows; j++ )
				mnew[i][j+nCols-i] = m_ppData[i][j-i];
		}
	
		// do the shifting from old matrix after end of new rows
		for( i; i < m_rows; i++)
			memmove(mnew[i+nCols], m_ppData[i], (m_rows - i)*sizeof(double));

	}

    // matrix is rectangular
	else
		for( i = 0; i < nNewRows; i++)
		{
			mnew[i]= new double[nNewCols];
			//memset(mnew[i],0,(nNewCols)*sizeof(double));

			// move all elements upto start of new columns
			memmove(mnew[i], m_ppData[i], (nStartCol)*sizeof(double));
			
			// move all elements after end of new columns
			//memmove(mnew[nColsToSkip], m_ppData[nStartCol], (r - nStartCol)*sizeof(double));		
			for( int j = nStartCol; j < m_rows; j++ )
				mnew[i][j+nCols] = m_ppData[i][j];
		}

	// delete existing matrix
	for( i = 0; i < m_rows; i++ )
		delete [] m_ppData[i];
	delete [] m_ppData;   

	// set pointer to new matrix and reset rows and columns
	m_ppData = mnew;
	
    m_rows = nNewRows;
    m_cols = nNewCols;
}

////////////////////////////////////////////////////////////////////////////////
// void CMatrix::SetToZero()
//
// sets all elements to zero
//
void CMatrix::SetToZero()
{
	// matrix is upper triangular
	if ( m_cols < 0 )
		for( int i = 0; i < m_rows; i++ )
			memset(m_ppData[i],0,(m_rows-i)*sizeof(double));

	// matrix is not upper triangular
	else
		for( int i = 0; i < m_rows; i++ ) 
			memset(m_ppData[i],0,m_cols*sizeof(double));
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_ATA(CMatrix& A, int AddMode)
//
// multiplies transpose of A by A
//
bool CMatrix::Mult_ATA(CMatrix& A, int AddMode)
{
	assert(this->IsSymmetric());

	register int i,j,k;
	
	if ( AddMode == 1 )					// result will be added
	{
		for( i = 0; i < A.m_cols; i++ )
		{
			for( j = i; j < A.m_cols; j++ )
			{
				for( k = 0; k < A.m_rows; k++ )
					m_ppData[i][j-i] += A.m_ppData[k][i]*A.m_ppData[k][j]; 
			}
		}
	}

	else if ( AddMode == -1 )					// result will be subtracted
	{
		for( i = 0; i < A.m_cols; i++ )
		{
			for( j = i; j < A.m_cols; j++ )
			{
				for( k = 0; k < A.m_rows; k++ )
					m_ppData[i][j-i] -= A.m_ppData[k][i]*A.m_ppData[k][j]; 
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_ATUTA(CMatrix& A, CMatrix& UT)
//
// multiplies transpose of A by UT matrix by A
//
bool CMatrix::Mult_ATUTA(CMatrix& A, CMatrix& UT)
{
	assert(this->IsSymmetric());
	assert(UT.IsSymmetric());

	register int i,j,k,l;
	
	for( i = 0; i < A.m_cols; i++ )
	{
		for( j = i; j < A.m_cols; j++ )
		{
			for( k = 0; k < A.m_rows; k++ )
			{
				for( l = 0; l < k; l++ )
					m_ppData[i][j-i] += 
						A.m_ppData[k][i]*UT.m_ppData[l][k-l]*A.m_ppData[l][j];

				for( l = k; l < A.m_rows; l++ )
					m_ppData[i][j-i] += 
						A.m_ppData[k][i]*UT.m_ppData[k][l-k]*A.m_ppData[l][j];
			}
		}
	}

	return true;
}

double CMatrix::Mult_ATUTA(CDoubleVector& v, CMatrix& UT)
{
	assert(this->IsSymmetric());
	assert(UT.IsSymmetric());

	register int k,l;
	
	for( k = 0; k < v.GetSize(); k++ )
	{
		for( l = 0; l < k; l++ )
			m_ppData[0][0] += v[k]*UT.m_ppData[l][k-l]*v[l];

		for( l = k; l < v.GetSize(); l++ )
			m_ppData[0][0] += v[k]*UT.m_ppData[k][l-k]*v[l];
	}

	return m_ppData[0][0];
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_ATA(CMatrix& A, int AddMode/*=1*/)
//
// multiplies A x AT; stores result in UT matrix
//
bool CMatrix::Mult_AAT(CMatrix& A, int AddMode/*=1*/)
{
	register int i,j,k;
	
	for( i = 0; i < A.m_rows; i++ )
	{
		for( j = i; j < A.m_rows; j++ )
		{
			for( k = 0; k < A.m_cols; k++ )
				m_ppData[i][j-i] += AddMode*(A.m_ppData[i][k]*A.m_ppData[j][k]); 
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// void CMatrix::Mult_AB(CMatrix& A, CMatrix& B, int MultMode, int AddMode)
//
// multiplies two matrices considering mult and add modes
// MultMode =	0	 A * B 		AddMode <	-1	C = -R
// 				1	AT * B				=	-1	C = C - R
//				2	 A * BT				=	 0	C = R
//				3	AT * BT				=	 1	C = C + R
//
void CMatrix::Mult_AB(CMatrix& A, CMatrix& B, int MultMode, int AddMode)
{
	register int i,j,k;

	if ( MultMode == 0 )			// Multiplication is A x B
	{

		// matrix is rectangular
		if ( m_cols > 0 )
		{
			for( i = 0; i < A.m_rows; i++ )
			{
				for( j = 0; j < B.m_cols; j++ )
				{
					for( k = 0; k < B.m_rows; k++ )
					{
						m_ppData[i][j] += AddMode*(A.m_ppData[i][k] * B.m_ppData[k][j]);
					}
				}
			}
		}

		// matrix is upper triangular
		else
		{
			for( i = 0; i < A.m_rows; i++ )
			{
				for( j = i; j < B.m_rows; j++ )
				{
					for( k = 0; k < B.m_rows; k++ )
					{
						m_ppData[i][j-i] += AddMode*(A.m_ppData[i][k]*B.m_ppData[k][j]);
					}
				}
			}

		}
	}
		
	if ( MultMode == 1 )			// Multiplication is AT x B
	{
		for(i = 0; i < A.m_cols; i++)
		{
			for(j = 0; j < B.m_cols; j++)
			{
				for(k = 0; k < B.m_rows; k++)
				{
					m_ppData[i][j] += AddMode*(A.m_ppData[k][i] * B.m_ppData[k][j]);
				}

			}
		}
	}

	if ( MultMode == 2 )			// Multiplication is A x BT
	{
		for(i = 0; i < A.m_rows; i++)
			for(j = 0; j < B.m_rows; j++)
				for(k = 0; k < A.m_cols; k++)
					m_ppData[i][j] += AddMode*(A.m_ppData[i][k] * B.m_ppData[j][k]);
	}
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::SetAsIdentity(dValue)
//
// sets matrix as identity, must be square matrix
//
bool CMatrix::SetAsIdentity(double dValue)
{
	// verify square matrix
	if ( m_cols != -1 )
	{
		if ( m_rows != m_cols )
			return false;
	}

	int i;

	// matrix is upper triangular
	if ( m_cols == -1 )
	{
		for ( i = 0; i < m_rows; i++ )
			m_ppData[i][0] = dValue;
	}

	else
	{
		for ( i = 0; i < m_rows; i++ )
			m_ppData[i][i] = dValue;
	}

	return true;
}

/*///////////////////////////////////////////////////////////////////////////////////////////////
Author/Copyright:               Ken Edmundson

Address:                        Dept. of Geodetic Science and Surveying
                                1958 Neil Avenue
                                Columbus, OH 43210-1247

Date:                           02/04/91

Description:                    INPUT:  Upper-triangular matrix of the form

                                        u u u u u u b
                                          u u u u u b
                                            u u u u b
                                              u u u b
                                                u u b
                                                  u b
                                                    l

                                        where u upper-triangular, symmetric,
                                        and positive-definite; b is the
                                        right hand side vector; and l is
                                        the sum of squares of the "computed -
                                        observed" vector.

                                        Matrix dimension.

                                OUTPUT: Upper-triangular Choleski factor in
                                        the same memory location as the input
                                        matrix.
                                        NOTE: l has been reduced to the root
                                        sum of squares of the residuals.

                                This subroutine performs a Choleski decomp-
                                osition on the input upper-triangular symmetric
                                matrix and returns the upper triangular
                                Choleski factor which may be used to solve the
                                system Nx=b and/or for the	 of N.

                                Non-singularity of N is also verified.
///////////////////////////////////////////////////////////////////////////////////////////////*/
bool CMatrix::CholeskyUT()
{
	register double sum,det;
	register int i,j,k;

	// check diagonal elements for positive-definite requirement 

/*	  for(register int i=0;i<dimu-1;i++)
		if(u[i][0] < SMALL)
			{
			cout<<'\n'<<"Non-positive element found at position "<<i<<","<<i;
 			AfxMessageBox("ERROR:MATRIX NOT POSITIVE-DEFINITE");
			}
*/
	//assert((u[0][0]) > 0.0);
	if ( m_ppData[0][0] < 0.0 )
		return false;
	
	m_ppData[0][0] = sqrt(m_ppData[0][0]);

	for( i = 1; i < m_rows; i++ )
	{

		//assert(fabs(u[0][0]) > EPSILON);
		if ( fabs(m_ppData[0][0]) < EPSILON )
			return false;

		m_ppData[0][i] *= (1/m_ppData[0][0]);
	}

	for( i = 1; i < (m_rows - 1); i++ )
	{
		sum = 0.0;

		for( j = 0; j < i; j++ ) 
			sum += m_ppData[j][i-j] * m_ppData[j][i-j];

		//assert(m_ppData[i][0]-sum > 0.0);
		if ( m_ppData[i][0]-sum < 0.0 )
			return false;

		m_ppData[i][0] = sqrt(m_ppData[i][0] - sum);

		/*if(fabs(u[i][0]) <= SMALL){
		AfxMessageBox("At Element N[i][0]");
		AfxMessageBox("ERROR:CHOLESKI FACTORIZATION IMPOSSIBLE");
		return; 
		}*/

		//assert(fabs(m_ppData[i][0]) > EPSILON);
		if ( fabs(m_ppData[i][0]) < EPSILON )
			return false;
     
		for( j = (i+1); j < m_rows; j++ )
		{
			sum = 0.0;
      		for( k = 0; k < i; k++ )  
				sum += m_ppData[k][j-k] * m_ppData[k][i-k];

			m_ppData[i][j-i] = (m_ppData[i][j-i] - sum)/m_ppData[i][0];      
		}
	}

	sum = 0.0;                                 

	for( i = 0; i < (m_rows - 1); i++ )  
		sum += (m_ppData[i][m_rows-i-1] * m_ppData[i][m_rows-i-1]);

	//assert(m_ppData[u.r-1][0]-sum > 0.0);
	if ( m_ppData[m_rows-1][0]-sum < 0.0 )
		return false;


	m_ppData[m_rows-1][0] = sqrt(m_ppData[m_rows-1][0]-sum);

	// verify non-singularity of u 
	det = 1.0;
	for( i = 0; i < (m_rows-1); i++ )
   		det *= (m_ppData[i][0]*m_ppData[i][0]);

	if ( det <= SMALL )
		return false;

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::CholeskyUT_SQR_FREE()
//
// This Subroutine Same as Above Except Formulation Done W/O Square Roots
//
// slow!!! in large bundles
bool CMatrix::CholeskyUT_SQR_FREE()
{
	register int i,j,k;
	register double sum, divisor;
	double den;

	if (this->listener)
		this->listener->totalRows(this->m_rows);

	for( i = 0; i < m_rows; i++ )
	{
		if (this->listener)
			this->listener->processingRow(i);

		sum = 0.0;
		double* j_i_j;
		for( j = 0; j < i; j++ )
		{
			j_i_j = &m_ppData[j][i-j];
			sum += (*j_i_j) * (*j_i_j) * m_ppData[j][0];
			//sum += m_ppData[j][i-j] * m_ppData[j][i-j] * m_ppData[j][0];
		}

		m_ppData[i][0] -= sum;

		// Check for divide by zero
		den = m_ppData[i][0];
		if ( fabs(den) < EPSILON )
			continue;
			//return false;

		divisor = 1.0 / den;
   
		for( j = (i+1); j < m_rows; j++ )
		{
			sum = 0.0;
			for( k = 0; k < i; k++ )
				sum += m_ppData[k][j-k] * m_ppData[k][i-k] * m_ppData[k][0];

			m_ppData[i][j-i] = (m_ppData[i][j-i] - sum) * divisor;
		} //end j loop
	} //end i loop
	
	return true;
}

/*///////////////////////////////////////////////////////////////////////////////////////////////
Author/Copyright:               Ken Edmundson

Address:                        Dept. of Geodetic Science and Surveying
                                1958 Neil Avenue
                                Columbus, OH 43210-1247

Date:                           02/04/91

File:                           decomp.cpp

Description:                    INPUT: Upper-triangular Choleski factor of
                                       the form

                                        u u u u u u b
                                          u u u u u b
                                            u u u u b
                                              u u u b
                                                u u b
                                                  u b
                                                    v

                                        where u is reduced upper-triangular,
                                        symmetric matrix; b is the reduced
                                        right hand side vector; and v is
                                        the root sum of squares of the
                                        residuals.

                                        Matrix dimension.

                                OUTPUT:Solution vector to system Nx=b in
                                       the memory location of the input
                                       right-hand side vector.

                                This subroutine solves the system Nx=b by
                                back-substition.
************************************************************************************************/
bool CMatrix::CholeskyUTBsub(CDoubleVector &s)
{
	register int i,j;
	register double sum;

	// solve upper triangular system 
	s[m_rows-2] = m_ppData[m_rows-2][1]/m_ppData[m_rows-2][0];

	for( i = (m_rows-3); i >= 0; i-- )
	{
		sum = 0.0;

		for( j = (i+1); j < (m_rows-1); j++ )
			sum += m_ppData[i][j-i]*s[j];

		if (::fabs(m_ppData[i][0]) < EPSILON)
			return false;

		s[i] = (m_ppData[i][m_rows-1-i]-sum)/m_ppData[i][0];
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::CholeskyUT_SQR_FREE_BKSUB(CDoubleVector& s)
//
//  This Subroutine Same as Above Except Formulation Done W/O Square Roots
//
bool CMatrix::CholeskyUT_SQR_FREE_BKSUB(CDoubleVector& s)
{
	register int i,j;
	double sum;

	// solve upper triangular system 
	s[m_rows-2]= m_ppData[m_rows-2][1];   

	for( i = (m_rows-3); i >= 0; i-- )
	{
		sum = 0.0;
		for( j = (i+1); j < (m_rows-1); j++ )
			sum += m_ppData[i][j-i]*s[j];

		s[i] = m_ppData[i][m_rows-1-i]-sum;
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::uTdu_Inverse(bool bfactored)
//
// utdu inverse
//
bool CMatrix::uTdu_Inverse(bool bfactored)
{
	double sum, div;
	CDoubleVector s(m_rows);

	// Perform UTDU Decomposition if Necessary
	if ( !bfactored ) 
	{
		if ( !this->CholeskyUT_SQR_FREE() )
			return false;
	}															 
																 
	// create tmp from this matrix
	CMatrix tmp(m_rows + 1, -1);
	
	// Copy "this"
	for (int i = 0; i < m_rows; i++)
	{
		for (int j = 0; j < m_rows - i; j++)
			tmp.m_ppData[i][j] = m_ppData[i][j];	
	}

	// solve for inverse of tmp by columns and put back in u
	for( int i = 0; i < m_rows; i++ )
	{
		// place column of identity into last column of tmp
		for( int j = 0; j < m_rows; j++ )  
			tmp.m_ppData[j][m_rows-j] = 0.0;

		tmp.m_ppData[i][m_rows-i] = 1.0;
	
		// factor last column
		for( int j = 0; j < m_rows; j++ )
		{
//			assert( fabs(tmp.m_ppData[j][0]) > EPSILON); //Johannes 8.11.2000 for AutoResection
			if ( fabs(tmp.m_ppData[j][0]) < EPSILON )
				return false;

			div = 1.0/tmp.m_ppData[j][0];
			sum = 0.0;

			for(int k = 0; k < j; k++ ) 
				sum += tmp.m_ppData[k][m_rows-k]*tmp.m_ppData[k][j-k]*tmp.m_ppData[k][0];

			tmp.m_ppData[j][m_rows-j] = (tmp.m_ppData[j][m_rows-j] - sum)*div;
		}
   
		if ( !tmp.CholeskyUT_SQR_FREE_BKSUB(s) )
			return false;

		for( int j = 0; j <= i; j++ )
			m_ppData[j][i-j] = s[j];
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// bool CMatrix::uTdu_Inverse()
//
// utdu inverse
//
bool CMatrix::uTdu_Inverse()
{
	// compute inverse of upper triangular only
	if(!this->SRFCholeskyInverse())
		return false;

	// Now compute full inverse
	if(!this->FullInverse())
		return false;

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::SRFCholeskyInverse()
//
// Computes inverse of upper triangular matrix formed by square root free Cholesky
// not general: accounts for storage of right hand side in last column of InV
//
bool CMatrix::SRFCholeskyInverse()
{
	int i,j,l;
	double den;
	
	int rows = m_rows-1;			// we don't include the last row and
							// column in the inverse calculation
	// set diagonals
	for( i = 0; i < rows; i++ )
	{
		// Check for divide by zero
		den = m_ppData[i][0];
		if (fabs(den) < EPSILON)
			return false;

		m_ppData[i][0] = 1.0/den;
	}

	if (this->listener)
		this->listener->totalRows(this->m_rows);

	// compute remainder of inverse
	for( i = 0; i < rows; i++ )
	{
		if (this->listener)
			this->listener->processingRow(i);

		int cols = rows-i;
		for( j = 1; j < cols; j++ )
		{
			m_ppData[i][j] = -m_ppData[i][j];

			for( l = 1; l < j; l++ )
				m_ppData[i][j] -= m_ppData[i][l] * m_ppData[l+i][j-l];
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::FullInverse()
//
// Computes full UT inverse after call to "SRFCholeskyInverse(CMatrix &InV)"
// not general, takes right hand side into consideration
//
bool CMatrix::FullInverse()
{
	int i,j,l;
	int rows,cols;

	rows = m_rows-1;
	CMatrix tmp(rows,-1);

	// compute diagonals of inverse matrix
	for( i = 0; i < rows; i++ )
	{
		cols = rows-i;
		tmp.m_ppData[i][0] = m_ppData[i][0];
		
		for( j = 1; j < cols; j++ )
			tmp.m_ppData[i][0] += m_ppData[i][j] * m_ppData[i][j] * m_ppData[j+i][0];
	}

	// compute remainder of inverse
	for( i = 0; i < rows; i++ )
	{
		cols = rows-i;
				
		for( j = 1; j < cols; j++ )
		{
			tmp.m_ppData[i][j] = m_ppData[i][j]*m_ppData[j+i][0];

			for( l = j+1; l < cols; l++ )
				tmp.m_ppData[i][j] += m_ppData[i][l] * m_ppData[j+i][l-j] * m_ppData[l+i][0];
		}
	}

	// copy tmp back into T
	for( i = 0; i < rows; i++ )
		memmove(m_ppData[i], tmp.m_ppData[i], (rows-i) * sizeof(double));		


	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Invert_3By3()
//
// Inverts 3 by 3 matrx ... either rectangular or upper triangular
//
bool CMatrix::Invert_3By3()
{
	double Det;
	double den;
//	CMatrix A(this);
	CMatrix A;
	
	A.Copy(this);

	// matrix is rectangular
	if ( m_cols != -1 )
	{
		den = m_ppData[0][0] * (m_ppData[1][1] * m_ppData[2][2] - m_ppData[1][2] * m_ppData[2][1]) -
			  m_ppData[0][1] * (m_ppData[1][0] * m_ppData[2][2] - m_ppData[1][2] * m_ppData[2][0]) +
			  m_ppData[0][2] * (m_ppData[1][0] * m_ppData[2][1] - m_ppData[1][1] * m_ppData[2][0]);
    
		// Check for division by zero
		if ( fabs(den) < EPSILON )
			return false;

		Det = 1.0 / den;          

		m_ppData[0][0] = (A.m_ppData[1][1] * A.m_ppData[2][2] - A.m_ppData[1][2] * A.m_ppData[2][1]) * Det;
		m_ppData[0][1] = (A.m_ppData[0][2] * A.m_ppData[2][1] - A.m_ppData[0][1] * A.m_ppData[2][2]) * Det;           
		m_ppData[0][2] = (A.m_ppData[0][1] * A.m_ppData[1][2] - A.m_ppData[0][2] * A.m_ppData[1][1]) * Det;
		m_ppData[1][0] = (A.m_ppData[1][2] * A.m_ppData[2][0] - A.m_ppData[1][0] * A.m_ppData[2][2]) * Det;
		m_ppData[1][1] = (A.m_ppData[0][0] * A.m_ppData[2][2] - A.m_ppData[0][2] * A.m_ppData[2][0]) * Det;   
		m_ppData[1][2] = (A.m_ppData[0][2] * A.m_ppData[1][0] - A.m_ppData[0][0] * A.m_ppData[1][2]) * Det;
		m_ppData[2][0] = (A.m_ppData[1][0] * A.m_ppData[2][1] - A.m_ppData[1][1] * A.m_ppData[2][0]) * Det;
		m_ppData[2][1] = (A.m_ppData[0][1] * A.m_ppData[2][0] - A.m_ppData[0][0] * A.m_ppData[2][1]) * Det;
		m_ppData[2][2] = (A.m_ppData[0][0] * A.m_ppData[1][1] - A.m_ppData[0][1] * A.m_ppData[1][0]) * Det;
	}

	// matrix is upper triangular
	else
	{
		den = m_ppData[0][0] * (m_ppData[1][0] * m_ppData[2][0] - m_ppData[1][1] * m_ppData[1][1]) -
			  m_ppData[0][1] * (m_ppData[0][1] * m_ppData[2][0] - m_ppData[1][1] * m_ppData[0][2]) +
			  m_ppData[0][2] * (m_ppData[0][1] * m_ppData[1][1] - m_ppData[1][0] * m_ppData[0][2]);
          
		// Check for division by zero
		if (fabs(den) < EPSILON)
			return false;

		Det = 1.0 / den;          
       
		m_ppData[0][0] = (A.m_ppData[1][0] * A.m_ppData[2][0] - A.m_ppData[1][1] * A.m_ppData[1][1]) * Det;   
		m_ppData[0][1] = (A.m_ppData[0][2] * A.m_ppData[1][1] - A.m_ppData[0][1] * A.m_ppData[2][0]) * Det;           
		m_ppData[0][2] = (A.m_ppData[0][1] * A.m_ppData[1][1] - A.m_ppData[0][2] * A.m_ppData[1][0]) * Det;   
		m_ppData[1][0] = (A.m_ppData[0][0] * A.m_ppData[2][0] - A.m_ppData[0][2] * A.m_ppData[0][2]) * Det;   
		m_ppData[1][1] = (A.m_ppData[0][2] * A.m_ppData[0][1] - A.m_ppData[0][0] * A.m_ppData[1][1]) * Det;   
		m_ppData[2][0] = (A.m_ppData[0][0] * A.m_ppData[1][0] - A.m_ppData[0][1] * A.m_ppData[0][1]) * Det;   
	 }

	return true;
}    

/**********************************************************************************************
Author/Copyright:				Ken Edmundson
Address:						Dept. of Geodetic Science and Surveying
								1958 Neil Avenue
								Columbus, OH 43210-1247
Date:							02/12/91
Updated:						10/08/96 University of Melbourne

File:							matrix.cpp

Description:					INPUT: 
								
								1)	CMatrix containing ...
								
									a)	Upper-triangular matrix of the form

                                        d u u u u u b
                                          d u u u u b
                                            d u u u b
                                              d u u b
                                                d u b
                                                  d b
                                                    d
								
									where d is diagonal and u is a unit upper-triangular matrix
									such that [sqrt(d)]*u is equal to the same upper-triangular
									matrix obtained when the normal equations are solved by the
									Choleski (square-root) factorization.
									
									Note: The last element of d is the residual sum of squares.
									
									b) Matrix dimension.
									
								2)	Coefficient or Partial derivitive vector(x) for one 
									observation.
									
								OUTPUT:
								
								1)	Upper-triangular matrix in the same form as above which has 
									been re-triangularized with the current partial derivitive
									vector.
									
									This subroutine retriangularizes the input matrix by
									rotating the 1st,2nd,3rd rows, etc. with the new x vector
									until x has been transformed to zero.

Reference:						Gentleman,W.M.,"Least-Squares Computations Without Square Roots,"
								Journal Institute Maths Applications(1973) 12, 329-336.
/////////////////////////////////////////////////////////////////////////////////////////////*/

bool CMatrix::gwsr(CDoubleVector &x)
{
	register int i,r;
	double del,delp,d,c,s,xi,xk;

	del = 1.0; delp = 0.0; r = 0;

	while( delp >= 0.0 )
	{
		if ( r == m_rows )
			return false;		// return false????

		// skip rotation if new row element already zero
		if ( fabs(x[r]) <= SMALL )
		{
			r++;
			continue;
		}

		//else
		{	//testing optimizations

			d			= m_ppData[r][0];
			m_ppData[r][0]		= d+del*x[r]*x[r];
			c			= d/m_ppData[r][0];
			delp		= c*del;
			s			= del*x[r]/m_ppData[r][0];
			xi			= x[r];
/*
			d			= u.m_ppData[r][0];
			dp			= d+del*x[r]*x[r];
			c			= d/dp;
			delp		= c*del;
			s			= del*x[r]/dp;
			u.m_ppData[r][0]	= dp;
			xi			= x[r];
			x[r]		= 0.0;
*/
			for( i = (r+1); i < m_rows; i++ )
			{
				// if corresponding elements of rows are zero, nothing changes
				if( (fabs(x[i]) < SMALL) && (fabs(m_ppData[r][i-r]) < SMALL) ) 
					continue;

				xk = x[i];
				x[i] = x[i] - xi*m_ppData[r][i-r];
				m_ppData[r][i-r] = c*m_ppData[r][i-r] + s*xk;

			} //end for

			if( (delp <= SMALL) || ( r == (m_rows-1)))
				//return;
				break;

			del = delp;
			r++;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_UTri_BT(CMatrix& UT,CMatrix& T)
//
// multiplies an upper triangular matrix UT(rxr),by the transpose 
// of a rectangular matrix T(cxr),storing the result in a rectangular matrix R(rxc)
//
bool CMatrix::Mult_UTri_BT(CMatrix& UT,CMatrix& T)
{
	int rows = UT.m_rows;
	int cols = T.m_rows;

	// verify rows and cols
	if ( m_rows != rows || m_cols != cols )
		return false;

	for( int i = 0; i < rows; i++ )
	{
		for( int j = 0; j < cols; j++ )
		{
			for( int k = 0; k < i; k++ )
				m_ppData[i][j] += UT.m_ppData[k][i-k]*T.m_ppData[j][k];
			for( int k = i; k < m_rows; k++ )
				m_ppData[i][j] += UT.m_ppData[i][k-i]*T.m_ppData[j][k];
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_A_B_UTM(CMatrix& A, CMatrix& B)
//
// multiplies an rxc by a cxr and stores negated result in a UT matrix
//
bool CMatrix::Mult_A_B_UTM(CMatrix& A, CMatrix& B)
{
	int rows = A.m_rows;
	int cols = A.m_cols;

	// validate dimensions
	if ( A.m_cols != B.m_rows || A.m_rows != B.m_cols )
		return false;

	if ( m_rows != rows || m_cols != -1 )
		return false;

	for( int i = 0; i < rows; i++ )
	{
		for( int j = i; j < rows; j++ )
		{
			for( int k = 0; k < cols; k++ )
			{
				m_ppData[i][j-i] -= A.m_ppData[i][k]*B.m_ppData[k][j];
			}
		}
	}

	return true;
}



////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_A_B_UTM(CMatrix& A, CMatrix& B)
//
// multiplies an rxc by a cxr and stores negated result in a UT matrix
//
bool CMatrix::Mult_A_B_UTM3(CMatrix& A, CMatrix& B)
{
	int rows = A.m_rows;
	int cols = 3;

	// validate dimensions
	if ( A.m_cols != 3 || B.m_rows != 3 || A.m_rows != B.m_cols)
		return false;

	if ( this->m_rows != rows || this->m_cols != -1 )
		return false;
    
    int n = rows / 3;
    int spare = rows % 3;

    int i, j, I, J;
    
    double a11, a12, a13, a21, a22, a23, a31, a32, a33;
    double b11, b12, b13, b21, b22, b23, b31, b32, b33;
    double *A1, *A2, *A3;
    double *B1, *B2, *B3;
    double *C1, *C2, *C3;
    int bytes1 = sizeof(double);
    int bytes2 = 2*sizeof(double);
    int bytes3 = 3*sizeof(double);

    for (I = 0, i = 0; I < n; I++)
    {
        A1 = A.m_ppData[i];
        A2 = A.m_ppData[i+1];
        A3 = A.m_ppData[i+2];
        a11 = A1[0];
        a12 = A1[1];
        a13 = A1[2];
        a21 = A2[0];
        a22 = A2[1];
        a23 = A2[2];
        a31 = A3[0];
        a32 = A3[1];
        a33 = A3[2];

        B1 = B.m_ppData[0] + i;
        B2 = B.m_ppData[1] + i;
        B3 = B.m_ppData[2] + i;
        b11 = B1[0];
        b12 = B1[1];
        b13 = B1[2];
        b21 = B2[0];
        b22 = B2[1];
        b23 = B2[2];
        b31 = B3[0];
        b32 = B3[1];
        b33 = B3[2];
   
        C1 = this->m_ppData[i];
        C2 = this->m_ppData[i+1];
        C3 = this->m_ppData[i+2];

        // first the little ut bit
        C1[0] -= a11*b11 + a12*b21 + a13*b31;                    
        C1[1] -= a11*b12 + a12*b22 + a13*b32;                    
        C1[2] -= a11*b13 + a12*b23 + a13*b33;                    

        C2[0] -= a21*b12 + a22*b22 + a23*b32;                    
        C2[1] -= a21*b13 + a22*b23 + a23*b33;                    

        C3[0] -= a31*b13 + a32*b23 + a33*b33;                    

        C1 += 3;
        C2 += 2;
        C3 += 1;
        B1 += 3;
        B2 += 3;
        B3 += 3;
        for (J = I+1, j = i+3; J < n; J++)
        {
            b11 = B1[0];
            b12 = B1[1];
            b13 = B1[2];
            b21 = B2[0];
            b22 = B2[1];
            b23 = B2[2];
            b31 = B3[0];
            b32 = B3[1];
            b33 = B3[2]; 

            C1[0] -= a11*b11 + a12*b21 + a13*b31;                    
            C1[1] -= a11*b12 + a12*b22 + a13*b32;                    
            C1[2] -= a11*b13 + a12*b23 + a13*b33;                    

            C2[0] -= a21*b11 + a22*b21 + a23*b31;                    
            C2[1] -= a21*b12 + a22*b22 + a23*b32;                    
            C2[2] -= a21*b13 + a22*b23 + a23*b33;                    

            C3[0] -= a31*b11 + a32*b21 + a33*b31;                    
            C3[1] -= a31*b12 + a32*b22 + a33*b32;                    
            C3[2] -= a31*b13 + a32*b23 + a33*b33;                    

            C1 += 3;
            C2 += 3;
            C3 += 3;
            B1 += 3;
            B2 += 3;
            B3 += 3;

            j+=3;
        }

        // now for the spares
        if(spare == 0)
            continue;

        if (spare == 1)
        {
            b11 = B1[0];
            b21 = B2[0];
            b31 = B3[0];

            C1[0] -= a11*b11 + a12*b21 + a13*b31;                    
            C2[0] -= a21*b11 + a22*b21 + a23*b31;                    
            C3[0] -= a31*b11 + a32*b21 + a33*b31;                    
        }
        else if (spare == 2)
        {
            b11 = B1[0];
            b12 = B1[1];
            b21 = B2[0];
            b22 = B2[1];
            b31 = B3[0];
            b32 = B3[1];

            C1[0] -= a11*b11 + a12*b21 + a13*b31;                    
            C1[1] -= a11*b12 + a12*b22 + a13*b32;                    

            C2[0] -= a21*b11 + a22*b21 + a23*b31;                    
            C2[1] -= a21*b12 + a22*b22 + a23*b32;                    

            C3[0] -= a31*b11 + a32*b21 + a33*b31;                    
            C3[1] -= a31*b12 + a32*b22 + a33*b32;                    
        }

        i+=3;
    }


    // now the final spare
    if (spare == 1)
    {
        C1 = this->m_ppData[i];

        A1 = A.m_ppData[i];
        a11 = A1[0];
        a12 = A1[1];
        a13 = A1[2];

        b11 = B1[0];
        b21 = B2[0];
        b31 = B3[0];

        C1[0] -= a11*b11 + a12*b21 + a13*b31;                    
    }
    else if (spare == 2)
    {
        C1 = this->m_ppData[i];
        C2 = this->m_ppData[i+1];

        A1 = A.m_ppData[i];
        A2 = A.m_ppData[i+1];

        a11 = A1[0];
        a12 = A1[1];
        a13 = A1[2];
        a21 = A2[0];
        a22 = A2[1];
        a23 = A2[2];

        b11 = B1[0];
        b12 = B1[1];
        b21 = B2[0];
        b22 = B2[1];
        b31 = B3[0];
        b32 = B3[1];

        C1[0] -= a11*b11 + a12*b21 + a13*b31;                    
        C1[1] -= a11*b12 + a12*b22 + a13*b32;                    
        C2[0] -= a21*b11 + a22*b21 + a23*b31;                    
    }

/*
	for( int i = 0; i < rows; i++ )
	{
		for( int j = i; j < rows; j++ )
		{
			for( int k = 0; k < cols; k++ )
			{
				m_ppData[i][j-i] -= A.m_ppData[i][k]*B.m_ppData[k][j];
			}
		}
	}
*/
	return true;
}

/**
 * Multiplies an nx3 by a 3xn 
 *
 */
bool CMatrix::Mult_A_B_UTM_NX3(CMatrix& A, CMatrix& B)
{
	int rows = A.m_rows;
	int cols = 3;

	// validate dimensions
	if ( A.m_cols != 3 || B.m_rows != 3 || A.m_rows != B.m_cols)
		return false;

	if ( this->m_rows != rows || this->m_cols != -1 )
		return false;

    double* dest;
    double* src;
    double src1;
    double src2;
    double src3;
    double* B1;
    double* B2;
    double* B3;

	for( int i = 0; i < rows; i++ )
	{
        src = A.m_ppData[i];
        src1 = src[0];
        src2 = src[1];
        src3 = src[2];
		for( int j = i; j < rows; j+=3)
		{
            dest = &m_ppData[i][j-i];
            B1 = B.m_ppData[0];
            B2 = B.m_ppData[1];
            B3 = B.m_ppData[2];
            
            *dest -= src1*B1[j];
            *dest -= src2*B2[j];
            *dest -= src3*B3[j];

            if (j+1 < rows)
            {
                dest = &m_ppData[i][j+1-i];

                *dest -= src1*B1[j+1];
                *dest -= src2*B2[j+1];
                *dest -= src3*B3[j+1];
            }

            if (j+2 < rows)
            {
                dest = &m_ppData[i][j+2-i];

                *dest -= src1*B1[j+2];
                *dest -= src2*B2[j+2];
                *dest -= src3*B3[j+2];
            }
		}
	}

	return true;
}

bool CMatrix::Mult_A_B_UTM_NX7(CMatrix& A, CMatrix& B)
{
	int rows = A.m_rows;
	int cols = A.m_cols;

	// validate dimensions
	if ( A.m_cols != B.m_rows || A.m_rows != B.m_cols )
		return false;

	if ( m_rows != rows || m_cols != -1 )
		return false;

    double *dest;
    double* B1;
    double* B2;
    double* B3;
    double* B4;
    double* B5;
    double* B6;
    double* B7;
    double* src;
    double src1;
    double src2;
    double src3;
    double src4;
    double src5;
    double src6;
    double src7;

	for( int i = 0; i < rows; i++ )
	{
        src = A.m_ppData[i];
        src1 = src[0];
        src2 = src[1];
        src3 = src[2];
        src4 = src[3];
        src5 = src[4];
        src6 = src[5];
        src7 = src[6];

		for( int j = i; j < rows; j++ )
		{
            dest = &m_ppData[i][j-i];
            B1 = B.m_ppData[0];
            B2 = B.m_ppData[1];
            B3 = B.m_ppData[2];
            B4 = B.m_ppData[3];
            B5 = B.m_ppData[4];
            B6 = B.m_ppData[5];
            B7 = B.m_ppData[6];
            
            *dest -= src1*B1[j];
			*dest -= src2*B2[j];
			*dest -= src3*B3[j];
			*dest -= src4*B4[j];
			*dest -= src5*B5[j];
			*dest -= src6*B6[j];
			*dest -= src7*B7[j];
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_AUTAT(CMatrix& A,CMatrix& UT)
//
// multiplies A x UT x AT where UT is upper triangular stores the output
// in an UT matrix
//
//
bool CMatrix::Mult_AUTAT(CMatrix& A,CMatrix& UT)
{
	int rows = A.m_rows;
	int cols = A.m_cols;

	// validate dimensions
	if ( ((A.m_cols != UT.m_rows-1) && (A.m_cols != UT.m_rows)) || UT.m_cols != -1 )
		return false;

	if ( m_rows != rows || m_cols != -1 )
		return false;

	double Am_ppDatajk;
	double *m_ppDatai_jminusi;

	for( int i = 0; i < rows; i++ )
	{
		double* Am_ppDatai = A.m_ppData[i];

		for( int j = i; j < rows; j++ )
		{
			m_ppDatai_jminusi = &m_ppData[i][j-i];

			for( int k = 0; k < cols; k++ )
			{
				Am_ppDatajk = A.m_ppData[j][k];

				for( int l = 0; l < k; l++ )
				{
					*m_ppDatai_jminusi += Am_ppDatai[l] * UT.m_ppData[l][k-l] * Am_ppDatajk;
				}
				for( int l = k; l < cols; l++ )
				{
					*m_ppDatai_jminusi += Am_ppDatai[l] * UT.m_ppData[k][l-k] * Am_ppDatajk;
				}
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_A_UT(CMatrix& A, CMatrix& UT)
//
// multiplies A x UT (UT is upper triangular); calling matrix is rectangular
// with same dimensions as A
//
//
bool CMatrix::Mult_A_UT(CMatrix& A, CMatrix& UT)
{
	int rows = A.m_rows;
	int cols = A.m_cols;

	// validate dimensions
	if ( !UT.IsSymmetric() || A.m_cols != UT.m_rows )
		return false;

	if ( m_rows != rows || m_cols != cols )
		return false;

	for( int i = 0; i < rows; i++ )
	{
		for( int j = 0; j < cols; j++ )
		{
			for( int k = 0; k < j; k++ )
				m_ppData[i][j] += A.m_ppData[i][k] * UT.m_ppData[k][j];
			for( int k = j; k < cols; k++ )
				m_ppData[i][j] += A.m_ppData[i][k] * UT.m_ppData[j][k-j];
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_AT_UT(CMatrix& A, CMatrix& UT)
//
// multiplies AT x UT (UT is upper triangular); calling matrix is rectangular
// with same dimensions as A
//
//
bool CMatrix::Mult_AT_UT(CMatrix& A, CMatrix& UT)
{
	int rows = A.m_rows;
	int cols = A.m_cols;

	// validate dimensions
	if ( !UT.IsSymmetric() || A.m_rows != UT.m_rows )
		return false;

	if ( m_rows != cols || m_cols != rows )
		return false;

	for( int i = 0; i < cols; i++ )
	{
		for( int j = 0; j < rows; j++ )
		{
			for( int k = 0; k < j; k++ )
				m_ppData[i][j] += A.m_ppData[k][i] * UT.m_ppData[k][j-k];
			for( int k = j; k < rows; k++ )
				m_ppData[i][j] += A.m_ppData[k][i] * UT.m_ppData[j][k-j];
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_UTri_B(CMatrix& UT, CMatrix& B)
//
// multiplies an upper triangular matrix UT(rxr),by a rectangular 
// matrix B(rxc),storing the result in a rectangular matrix R(rxc)
//
bool CMatrix::Mult_UTri_B(CMatrix& UT, CMatrix& B)
{
	// validate dimensions
	int rows = UT.m_rows;
	int cols = B.m_cols;

	if ( UT.m_rows != B.m_rows || UT.m_cols != -1 )
		return false;

	if ( m_rows != rows || m_cols != B.m_cols )
		return false;

	for( int i = 0; i < rows; i++ )
	{
		for( int j = 0; j < cols; j++ )
		{
			for( int k = 0; k < i; k++ )
				m_ppData[i][j] += UT.m_ppData[k][i-k]*B.m_ppData[k][j];
			for( int k = i; k < m_rows; k++ )
				m_ppData[i][j] += UT.m_ppData[i][k-i]*B.m_ppData[k][j];
		}
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult_A_B_UT(CMatrix& A, CMatrix& B)
//
// multiplies an rxc by a cxr and stores summed result in a UT matrix
//
bool CMatrix::Mult_A_B_UT(CMatrix& A, CMatrix& B)
{
	int rows = A.m_rows;
	int cols = A.m_cols;

	// validate dimensions
	if ( A.m_cols != B.m_rows || A.m_rows != B.m_cols )
		return false;

	if ( m_rows != rows || m_cols != -1 )
		return false;

	for( int i = 0; i < rows; i++ )
	{
		for( int j = i; j < rows; j++ )
		{
			for( int k = 0; k < cols; k++ )
			{
				m_ppData[i][j-i] += A.m_ppData[i][k]*B.m_ppData[k][j];
			}
		}
	}

	return true;
}



/////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult(CMatrix& A, CMatrix& B)
//
// this = A * B (Johannes 20.10.2000)
// not for symmetric Matrices !!!
//
bool CMatrix::Mult(CMatrix& A, CMatrix& B)
{
	assert( !A.IsSymmetric() && !B.IsSymmetric() );

	if (A.m_cols !=  B.m_rows)
		return false;

	Set(A.m_rows,B.m_cols);
	
	SetToZero();
	Mult_AB(A,B,0,1);

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::Mult(CMatrix& A, CMatrix& B)
//
// this *= A (Johannes 20.10.2000)
// not for symmetric Matrices !!!
//
bool CMatrix::Mult(CMatrix& A)
{
	assert( !IsSymmetric() && !A.IsSymmetric() );

	if (m_cols !=  A.m_rows)
		return false;

	CMatrix temp=*this;

	Set(m_rows,A.m_cols);
	
	SetToZero();
	Mult_AB(temp,A,0,1);

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// CMatrix CMatrix::operator *(CMatrix& A, CMatrix& B)
//
// returns (A * B)
//
CMatrix CMatrix::operator *(const CMatrix &rSrc)
{
	CMatrix A=*this,B=rSrc;

	A.Mult(B);
	
	return A;
}


/////////////////////////////////////////////////////////////////////////////////
// CMatrix CMatrix::operator *=(const CMatrix &rSrc)
//
// returns reference to A=A*B
//
CMatrix& CMatrix::operator *=(const CMatrix &rSrc)
{
	CMatrix A=rSrc;

	Mult(A);
	
	return (CMatrix &)*this;
}


/////////////////////////////////////////////////////////////////////////////////
// CMatrix CMatrix::operator =(CDoubleVector &Vec)
//
CMatrix& CMatrix::operator =(CDoubleVector &Vec)
{
	Set(Vec.GetSize(),1);
	
	for (int i=0; i < Vec.GetSize(); i++)
		m_ppData[i][0]=Vec[i];

	return *this;
}

/////////////////////////////////////////////////////////////////////////////////
// CMatrix CMatrix::operator *(double &value)
//
CMatrix CMatrix::operator *(const double &value)
{
	double v=value;
	
	CMatrix temp(*this);
	
	temp.MultiplyByScalar(v);

	return temp;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::AddSubmatrix(CMatrix &Submatrix, int SROW, int SCOL)
//
// adds submatrix to calling matrix starting at SROW and SCOL
//
bool CMatrix::AddSubmatrix(CMatrix &Submatrix, int SROW, int SCOL)
{
	int i,j,sr,sc;

	// matrix to be added is upper triangular
	if ( Submatrix.m_cols == -1 )
		for( i = 0; i < Submatrix.m_rows; i++ )
		{
			sr = i+SROW;
			for(j = 0; j < (Submatrix.m_rows-i); j++)
				m_ppData[sr][j] += Submatrix.m_ppData[i][j];
		}

	// matrix to be added is rectangular
	else
		for( i = 0; i < Submatrix.m_rows; i++ )
		{
			sr = i+SROW;
			for(j = 0; j < Submatrix.m_cols; j++)
			{
				sc = j-i+SCOL;
				m_ppData[sr][sc] += Submatrix.m_ppData[i][j];
			}
		}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::SubtractSubmatrix(CMatrix &Submatrix, int SROW, int SCOL)
//
// subtracts submatrix from calling matrix starting at SROW and SCOL
//
bool CMatrix::SubtractSubmatrix(CMatrix &Submatrix, int SROW, int SCOL)
{
	int i,j,sr,sc;

	// matrix to be added is upper triangular
	if ( Submatrix.m_cols == -1 )
		for( i = 0; i < Submatrix.m_rows; i++ )
		{
			sr = i+SROW;
			for(j = 0; j < (Submatrix.m_rows-i); j++)
				m_ppData[sr][j] -= Submatrix.m_ppData[i][j];
		}

	// matrix to be added is rectangular
	else
		for( i = 0; i < Submatrix.m_rows; i++ )
		{
			sr = i+SROW;
			for(j = 0; j < Submatrix.m_cols; j++)
			{
				sc = j-i+SCOL;
				m_ppData[sr][sc] -= Submatrix.m_ppData[i][j];
			}
		}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// bool CMatrix::MultiplyByScalar(double dScalar)
//
// multiply matrix by scalar
//
bool CMatrix::MultiplyByScalar(double dScalar)
{
    // if cols is negative, matrix is upper triangular, symmetric
	if ( m_cols < 0 )
	{
		for( int i = 0; i < m_rows; i++ )
			for( int j = 0; j < m_rows-i; j++ )
				m_ppData[i][j] *= dScalar;

	}

	// matrix is rectangular
	else
	{
		for( int i = 0; i < m_rows; i++ )
			for( int j = 0; j < m_cols; j++ )
				m_ppData[i][j] *= dScalar;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
//
//
double CMatrix::Get(int r, int c)
{
	double* pData = GetDataPointer(r, c);

	return *pData;
}

///////////////////////////////////////////////////////////////////////////////
//
//
void CMatrix::Set(double d, int r, int c)
{
	double* pData = GetDataPointer(r, c);
	*pData = d;
}

///////////////////////////////////////////////////////////////////////////////
// CMatrix::GetDataPointer(int r, int c int nValidElements)
//
// Gets by normal matrix index 
// i.e. r and c refer to the "rectangular" indices
//
double* CMatrix::GetDataPointer(int r, int c)
{
	// Sanity check
	if ((r < 0) || (r > m_rows))
		assert(false);

	// Sanity check
	if ( !IsSymmetric() )
	{
		if ((c < 0) || (c > m_cols))
			assert(false);
	}

	if ( IsSymmetric() )
	{
		if ( r > c )
		{
			double *pData = m_ppData[c];
			return &pData[r - c];
		}			
		else
		{
			double *pData = m_ppData[r];
			return &pData[c - r];
		}
	}
	else
	{
		double *pData = m_ppData[r];
		return &pData[c];
	}
}

///////////////////////////////////////////////////////////////////////////////
// CMatrix::GetDataPointer(int r, int c int nValidElements)
//
// Gets by normal matrix index 
// i.e. r and c refer to the "rectangular" indices
//
// Do NOT USE this fuction. 
// Use "GetDataPointer"
//
//
// This is here in support of the Bundle only 
// and does not do what it says it does, but the bundle uses it
// and works with it the way it is.
//
//
double* CMatrix::__GetDataPointer(int r, int c)
{
	// Sanity check
	if ((r < 0) || (r > m_rows))
		assert(false);

	// Sanity check
	if ( !IsSymmetric() )
	{
		if ((c < 0) || (c > m_cols))
			assert(false);
	}

	if ( IsSymmetric() )
	{
		//if ( r > c )
		//{
		//	double *pData = m_ppData[c];
		//	return &pData[r];
		//}			
		//else
		//{
			double *pData = m_ppData[r];
			return &pData[c];
		//}
	}
	else
	{
		double *pData = m_ppData[r];
		return &pData[c];
	}
}

///////////////////////////////////////////////////////////////////////////////
//
//


void CMatrix::Dump()
{
	double* pData = 0;
	int nCols = 0;

	if (IsSymmetric())
		nCols = m_rows;
	else
		nCols = m_cols;

	printf("Matrix (%d x %d)\n", m_rows, nCols);

	for (int r = 0; r < m_rows; r++)
	{
		for (int c = 0; c < nCols; c++)
		{
			pData = GetDataPointer(r, c);
			printf(" %12.4f", *pData);
		}

		printf("\n");
	}

}


/*
///////////////////////////////////////////////////////////////////
// other matrix routines to be incorporated (or not) if/when/ever needed
///////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// multiplies the transpose of a rectangular matrix A(cxr), by a 
// rectangular matrix B(rxs) and stores the result in a rectangular matrix R(cxs)

void Mult_ATB(CMatrix* R, CMatrix* A, CMatrix* B)        
{
	assert(A->r == B->r);

	for( int i = 0; i < A->c; i++)
		for(int j = 0; j < B->c; j++ )
			for(int k = 0; k < B->r; k++ )
				R->m_ppData[i][j] += A->m_ppData[k][i]*B->m_ppData[k][j];
}
*/

void CMatrix::Transpose()
{
	if (IsSymmetric())
		return;

	CMatrix temp(*this);

	Set(temp.m_cols,temp.m_rows);

	for( int i = 0; i < temp.m_rows; i++)
		for(int j = 0; j < temp.m_cols; j++ )
			m_ppData[j][i]=temp.m_ppData[i][j];
}

void CMatrix::SerializeMatrix(CFileArchive& ar)
{
	if (ar.IsStoring())
	{
		ar << (long)m_rows;
		ar << (long)m_cols;

		if (m_cols < 0) // Upper triangular or symetric (i.e. half the memory)
		{
			for( int i = 0; i < m_rows; i++)
			{
				for(int j = 0; j < m_rows - i; j++ )
				{
					ar << m_ppData[i][j];
				}
			}
		}
		else // Full matrix
		{
			for( int i = 0; i < m_rows; i++)
			{
				for(int j = 0; j < m_cols; j++ )
				{
					ar << m_ppData[i][j];
				}
			}
		}
	}
	else
	{
		long lRow,lCol;
		
		ar >> lRow;
		ar >> lCol;

		Set(lRow, lCol);

		if (m_cols < 0)  // Upper triangular or symetric (i.e. half the memory)
		{
			for( int i = 0; i < m_rows; i++)
			{
				for(int j = 0; j < m_rows - i; j++ )
				{
					ar >> m_ppData[i][j];
				}
			}
		}
		else // Full matrix
		{
			for( int i = 0; i < m_rows; i++)
			{
				for(int j = 0; j < m_cols; j++ )
				{
					ar >> m_ppData[i][j];
				}
			}
		}

	}
}


///////////////////////////////////////////////////////////////////
// void CMatrix::GetDiagonalSubMatrix(CMatrix &Submatrix, int nRowStart, int nRowEnd)
//
// extract submatrix symmetrical to diagonal
//
void CMatrix::GetDiagonalSubMatrix(CMatrix &Submatrix, int nRowStart, int nRowEnd)
{
	int size=nRowEnd-nRowStart+1;

	assert(nRowEnd < m_rows);

	if (IsSymmetric())
	{
		Submatrix.Resize(size,-1);

		for (int r=0; r<size; r++)
			for (int c=r; c<size; c++)
				Submatrix.Set(Get(r+nRowStart,c+nRowStart), r, c);
	}
	else
	{
		Submatrix.Resize(size,size);

		for (int r=0; r<size; r++)
			for (int c=0; c<size; c++)
				Submatrix.Set(Get(r+nRowStart,c+nRowStart), r, c);
	}

}


void CMatrix::OutputMatrix(const char *cName)
{
	CCharString text,line;
	CTextFile file(cName,CTextFile::WRITE);

	long col=m_cols;
	if (col==-1)
		col=m_rows;

	for ( int i = 0; i < m_rows; i++ )
	{
		line="";
		for ( int j = 0; j < col; j++ )
		{
			text.Format("%.16e\t",Get(i,j));
			line+=text;
		}
		file.WriteLine(line);
	}
	file.close();
}

bool CMatrix::IsValidCovarianceMatrix()
{
	if ( !IsSymmetric() )
		return false;

	for( int i = 0; i < m_rows; i++ )
	{
		if (m_ppData[i][0] < 0)
			return false;
	}

	return true;
}
