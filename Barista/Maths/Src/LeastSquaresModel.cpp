/**
 * @class CLeastSquaresModel
 * BestFit Interface used for solving "combined-non linear" Least Squares
 * of the form Ad + Bv - w = 0
 *
 * Classes implementing this interface will provide the partial
 * derivatives for the particular modal being fit.
 * Once created the inherited class can be used to construct a
 * LeastSquares.LeastSquaresSolver that will compute the least squares solution
 * @see UniMelb.LeastSquares.JLeastSquaresSolver
 *
 */
#include "newmat.h"
#include "BlockDiagonalMatrix.h"
#include "CharStringArray.h"
#include "TextFile.h"
#include "LeastSquaresModel.h"


CLeastSquaresModel::CLeastSquaresModel()
{
    this->parms0 = NULL;
    this->Qxx = NULL;
}

CLeastSquaresModel::~CLeastSquaresModel()
{
	this->deleteObjects();
}

void CLeastSquaresModel::deleteObjects()
{
	if (this->parms0 != NULL)
	{
		delete this->parms0;
		this->parms0 = NULL;
	}

	if (this->Qxx != NULL)
	{
		delete this->Qxx;
		this->Qxx = NULL;
	}
}


/**
 *
 */
void CLeastSquaresModel::accumulate(Matrix* N, Matrix* t, Matrix* parms, Matrix* constant,
                        Matrix* ob, Matrix* previousResid, Matrix* Q)
{
	Matrix w;
    this->Fi(&w, parms, constant, ob);

    Matrix Ai;
	this->Ai(&Ai, parms, constant, ob);

	Matrix Bi;
	this->Bi(&Bi, parms, constant, ob);

	Matrix Z(1, 1);
	Z = 0;

    if (*previousResid != Z)
    {
//        w.minusEquals(Bi.times(previousResid));
		w = w - Bi * (*previousResid);
    }

    // Form the equivalent weight matrix
//    Matrix We = Bi.times(Q).times(Bi.transpose()).inverse();
    Matrix We = (Bi*(*Q)* Bi.t()).i();

    // accumulate normals matrix
//   Matrix ATWeA = Ai.transpose().times(We).times(Ai);
    Matrix ATWeA = Ai.t() * We * Ai;
//    N.plusEquals(ATWeA);
	*N = *N + ATWeA;

    // accumulate right hand side (ATw)
//    Matrix ATWew = Ai.transpose().times(We).times(w);
    Matrix ATWew = Ai.t() * We * w;
//    t.plusEquals(ATWew);
    *t = *t + ATWew;
}

void CLeastSquaresModel::calculateResiduals(Matrix* resids, Matrix* delta, Matrix* parms, Matrix* constant,
                            Matrix* ob, Matrix* previousResid, Matrix* Q)
{
    Matrix Ai;
	this->Ai(&Ai, parms, constant, ob);
    Matrix Bi;
	this->Bi(&Bi, parms, constant, ob);
    Matrix w;
	this->Fi(&w, parms, constant, ob);

	Matrix Z(1, 1);
	Z = 0;

    if (*previousResid != Z)
    {
//        w.minusEquals(Bi.times(previousResid));
		w = w - Bi * (*previousResid);
    }

    // Form the equivalent weight matrix
//    Matrix We = Bi.times(Q).times(Bi.transpose()).inverse();
    Matrix We = (Bi*(*Q)*Bi.t()).i();

//    Matrix Ve = Ai.times(Delta).plus(w);
	Matrix Ve = Ai * (*delta) + w;

//    Matrix V = Q.times(Bi.transpose()).times(We).times(Ve);
    *resids = (*Q) * Bi.t() * We * Ve;

//	V.timesEquals(-1.0);
	*resids = *resids * -1;

//    return V;
}

/**
* Generally 0, but can be overridden to reflect the number of "observations"
* that are not really discreet. Useful in calculating the dof.
*/
int CLeastSquaresModel::getNonDistinctObservationCount()
{
	return 0;
}

