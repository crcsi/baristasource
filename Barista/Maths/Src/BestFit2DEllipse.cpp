#include <float.h>

#include "newmat.h"
#include "CharStringArray.h"
#include "BlockDiagonalMatrix.h"
#include "math.h"
#include "MatrixHelper.h"

#include "BestFit2DEllipse.h"

CBestFit2DEllipse::CBestFit2DEllipse(void)
{
}

CBestFit2DEllipse::~CBestFit2DEllipse(void)
{
}


bool CBestFit2DEllipse::approximate (Matrix* approx, Matrix* consts, Matrix* obs)
{
	bool ret = true;
	double a;
	double b;
	double theta;
	double x0;
	double y0;
	int nObs = obs->Nrows();

	double X = 0;
	double Y = 0;

	double xmin = FLT_MAX;
	double xmax = -FLT_MAX;
	double ymin = FLT_MAX;
	double ymax = -FLT_MAX;
	for (long i = 0; i < nObs; ++i)
	{
		X += obs->element(i, 0);
		Y += obs->element(i, 1);
		if (obs->element(i, 0) < xmin) xmin = obs->element(i, 0);
		if (obs->element(i, 0) > xmax) xmax = obs->element(i, 0);
		if (obs->element(i, 1) < ymin) ymin = obs->element(i, 1);
		if (obs->element(i, 1) > ymax) ymax = obs->element(i, 1);
	};

	X /= (double) nObs;
	Y /= (double) nObs;

	xmax -= xmin;
	ymax -= ymin;
	double rho = xmax > ymax ? double(xmax) : double(ymax);

	Matrix x(6,1);
	x = 0.0;
	x(1,1) = x(3,1) = 1.0;
	int maxIt = 25;
	for (int it = 0; it < maxIt; ++it)
	{
		Matrix A(nObs, 6);
		Matrix l(nObs, 1);
		for (long i = 0; i < nObs; ++i)
		{
			double Xred = (obs->element(i, 0) - X) / rho;
			double Yred = (obs->element(i, 1) - Y) / rho;
			double bx = 2.0 * (x(1,1) * Xred + x(2,1) * Yred + x(4,1));
			double by = 2.0 * (x(3,1) * Yred + x(2,1) * Xred + x(5,1));
			double btb = sqrt(1.0 /(bx*bx + by*by));
			A(i + 1, 1) = Xred * Xred * btb; 
			A(i + 1, 2) = 2.0 * Xred * Yred* btb; 
			A(i + 1, 3) = Yred * Yred* btb; 
			A(i + 1, 4) = 2.0 * Xred * btb;  
			A(i + 1, 5) = 2.0 * Yred * btb;
			A(i + 1, 6) = 1.0 * btb;
			l(i + 1, 1) = -btb * (x(1,1) * Xred * Xred + 2.0 * x(2,1) * Xred * Yred + x(3,1) * Yred * Yred + 2.0 * x(4,1) * Xred + 2.0 * x(5,1) * Yred + x(6,1));
		};

		Matrix At = A.t();
		Matrix N(7,7);
		N = 0.0;
		N.SubMatrix(1,6,1,6) = At*A;
		N(1,7) = N(7,1) = x(3,1);
		N(3,7) = N(7,3) = x(1,1);
		N(2,7) = N(7,2) = -2.0 * x(2,1);
		Matrix n(7,1);
		n.SubMatrix(1,6,1,1) = At * l;
		n(7,1) = 1.0 - x(3,1) * x(1,1) + x(2,1) * x(2,1);

		Matrix Qxx = N.i();
		l = Qxx * n;

		x(1,1) += l(1,1);
		x(2,1) += l(2,1);
		x(3,1) += l(3,1);
		x(4,1) += l(4,1);
		x(5,1) += l(5,1);
		x(6,1) += l(6,1);
	}

	double subDet = x(2,1) * x(2,1) - x(3,1) * x(1,1);
	x0 = rho * (x(3,1) * x(4,1) - x(2,1) * x(5,1)) / subDet + X;
	y0 = rho * (x(1,1) * x(5,1) - x(2,1) * x(4,1)) / subDet + Y;
	double AminC      = x(1,1) - x(3,1);
		
	if ((fabs(x(2,1)) < DBL_EPSILON) && (fabs(AminC) < DBL_EPSILON)) theta = 0.0;
	else theta = atan2(2*x(2,1), -AminC) * 0.5f;

	if (theta < 0.0) theta += 3.1415927;

	double AplsC      = x(1,1) + x(3,1);
	double enumerator = 2.0 * (x(1,1) * x(5,1)* x(5,1) + x(3,1) * x(4,1)* x(4,1) + x(2,1)* x(2,1) * x(5,1) 
		                       - 2.0 * x(2,1) * x(4,1) * x(5,1) -  x(1,1) * x(3,1) * x(5,1));
	double aux = sqrt(1.0 + 4.0 * x(2,1) * x(2,1) / (AminC * AminC));
	a = enumerator / (subDet * ( AminC * aux - AplsC));
	b = enumerator / (subDet * (-AminC * aux - AplsC));
	if ((b > 0.0) && (a > 0.0))
	{
		a = rho * sqrt(a);
		b = rho * sqrt(b);
		if (a < b) 
		{
			aux = a;
			a = b;
			b = aux;
		}
	}
	else
	{
		x0 = X;
		y0 = Y;

	// Find largest and small distance from x0, y0 for a and b
		double small = 1e100;
		double big = 0;
		int ibig = -1;
		int ismall = -1;

		for (int i = 0; i < nObs; i++)
		{
			double dx = obs->element(i, 0) - x0;
			double dy = obs->element(i, 1) - y0;
			double d = sqrt(dx*dx + dy*dy);

			if (d > big)
			{
				ibig = i;
				big = d;
			}

			if (d < small)
				small = d;
		}

		a = big;
		b = small;

		// now guess a theta
		double dx = obs->element(ibig, 0) - x0;
		double dy = obs->element(ibig, 1) - y0;
		double sldf = atan(dy / dx);
		theta = 1.57 - atan(dy / dx);
	};

	approx->ReSize(5, 1);

	approx->element(0 , 0) = a; 
	approx->element(1 , 0) = b; 
	approx->element(2 , 0) = theta; 
	approx->element(3 , 0) = x0; 
	approx->element(4 , 0) = y0;

	return ret;
}

bool CBestFit2DEllipse::Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob)
{
	double a =		(*parms)(1, 1);
	double b =		(*parms)(2, 1);
	double theta =	(*parms)(3, 1);
	double x0 =		(*parms)(4, 1);
	double y0 =		(*parms)(5, 1);

	double x = ob->element(0, 0);
	double y = ob->element(0, 1);

	double sint = sin(theta);
	double cost = cos(theta);

    Fi->ReSize(1, 1);

	double f =	( cos(theta)*cos(theta) / (a*a) + sin(theta)*sin(theta) / (b*b) ) * (x - x0)*(x - x0) +
				sin(2 * theta) *(1/(a*a) - 1/(b*b)) * (x-x0)*(y-y0) +
				( sin(theta)*sin(theta) / (a*a) + cos(theta)*cos(theta) / (b*b) ) * (y - y0)*(y - y0) - 1;


	Fi->element(0,0) = f;

	return true;
}


bool CBestFit2DEllipse::Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob)
{
	Ai->ReSize(1, 5);

	double a =		(*parms)(1, 1);
	double b =		(*parms)(2, 1);
	double theta =	(*parms)(3, 1);
	double x0 =		(*parms)(4, 1);
	double y0 =		(*parms)(5, 1);

	double x = ob->element(0, 0);
	double y = ob->element(0, 1);

	double sint = sin(theta);
	double cost = cos(theta);

	double xbar = cost * (x - x0) + sint * (y - y0);
	double ybar = sint * (x - x0) - cost * (y - y0);

	double a0 = -2 * xbar * xbar / (a * a * a);
	double a1 =	-2 * ybar * ybar / (b * b * b);
	double a2 =	 2 * ( -1/(a*a) + 1/(b*b)) * xbar * ybar;
	double a3 =	-2 * ( xbar * cost / (a*a) + ybar * sint / (b*b));
	double a4 =	 2 * (-xbar * sint / (a*a) + ybar * cost / (b*b));

	Ai->element(0, 0) = a0;
	Ai->element(0, 1) = a1;
	Ai->element(0, 2) = a2;
	Ai->element(0, 3) = a3;
	Ai->element(0, 4) = a4;

	return true;
}


bool CBestFit2DEllipse::Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Bi->ReSize(1, 2);

	double a =		(*parms)(1, 1);
	double b =		(*parms)(2, 1);
	double theta =	(*parms)(3, 1);
	double x0 =		(*parms)(4, 1);
	double y0 =		(*parms)(5, 1);

    double x = ob->element(0, 0);
    double y = ob->element(0, 1);

	double b0 = 2 *(cos(theta)*cos(theta) / (a*a) + sin(theta)*sin(theta) / (b*b)) *
				(x - x0) + sin(2 * theta) * (1/(a*a) - 1/(b*b)) * (y - y0);

	double b1 = 2 *(sin(theta)*sin(theta) / (a*a) + cos(theta)*cos(theta) / (b*b) ) *
				(y - y0) + sin(2 * theta) * (1/(a*a) - 1/(b*b)) * (x - x0);

	Bi->element( 0, 0) = -b0;
	Bi->element( 0, 1) = -b1;

	return true;
}

/**
  * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
  */
void CBestFit2DEllipse::getParameterNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "a";
	str = names->Add();
	*str = "b";
	str = names->Add();
	*str = "theta";
	str = names->Add();
	*str = "x0";
	str = names->Add();
	*str = "y0";
}

/**
 * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y"...
 */
void CBestFit2DEllipse::getObservationNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "x";
	str = names->Add();
	*str = "y";
}


void CBestFit2DEllipse::getName(CCharString& name)
{
	name = "Best-Fit 2D Ellipse";
}

/**
 * Sets the a priori parameters information along with a priori
 * covariance Matrix
 */
void CBestFit2DEllipse::setParameters(Matrix* parms, CBlockDiagonalMatrix* qxx)
{
	// first delete object (if there)
	this->deleteObjects();

	this->parms0 = new Matrix;
	*this->parms0 = *parms;
	this->Qxx = new CBlockDiagonalMatrix(*qxx);
}

/**
  * Gets the apriori covariance Matrix (if there)
  */
CBlockDiagonalMatrix* CBestFit2DEllipse::getQxx()
{
	return this->Qxx;
}
