/**
 * @class CBestFitLine
 *
 * File class for best fit 2d line
 * @see CLeastSquaresModel
 * 
 * 
 * 
 * @author Harry Hanley
 * @version 1.0 
 * @date 20-jan-2004
 *
 */

#include "newmat.h"
#include "BlockDiagonalMatrix.h"
#include "LeastSquaresSolver.h"
#include "CharStringArray.h"
#include "MatrixHelper.h"
#include <math.h>

#include "BestFitLine.h"
CBestFitLine::CBestFitLine(void)
{
   this->parms0 = NULL;
   this->Qxx = NULL;
}

CBestFitLine::~CBestFitLine(void)
{
	this->deleteObjects();
}

/**
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Column Matrix of approximations of the parameters of the least squares fit
  */
bool CBestFitLine::approximate (Matrix* approx, Matrix* consts, Matrix* obs)
{
	approx->ReSize(2, 1);

	*approx = 0;
	
	return true;
}

/**
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Function evaluated at the given observation unsing the given parameters
  */
bool CBestFitLine::Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Fi->ReSize(1, 1);

    double a0 = parms->element(0, 0);
    double a1 = parms->element(1, 0);

    double x = ob->element(0, 0);
    double y = ob->element(0, 1);

    (*Fi)(1, 1) = y - a0 - a1*x;

    return  true;
}

/**
 * @param parms Column Matrix of approximations of the parameters
 * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
 * @return A Matrix of partials with respect to paramters evaluated at ob
 */
bool CBestFitLine::Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Ai->ReSize(1, 2);

    double x = ob->element(0, 0);

    Ai->element(0, 0) = -1;
    Ai->element(0, 1) = -x;

    return true;
}

/**
  * @return Bi Matrix of patials with respect to observations for the ith observation
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  */
bool CBestFitLine::Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Bi->ReSize(1, 2);

    double a0 = parms->element(0, 0);
    double a1 = parms->element(1, 0);

    (*Bi)(1, 1) = -a1;
    (*Bi)(1, 2) = 1;

    return true;
}

/**
  * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
  */
void CBestFitLine::getParameterNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "a0";

	str = names->Add();
	*str = "a1";
}

/**
 * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
 */
void CBestFitLine::getObservationNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "x";

	str = names->Add();
	*str = "y";
}

/**
 * @return a String cantainint the name of the best fit ex. "Best fit Line"
 */
void CBestFitLine::getName(CCharString& name)
{
	name = "Best-Fit 2D Line";
}

/**
 * Sets the a priori parameters information along with a priori
 * covariance Matrix
 */
void CBestFitLine::setParameters(Matrix* parms, CBlockDiagonalMatrix* qxx)
{
	// first delete object (if there)
	this->deleteObjects();

	this->parms0 = new Matrix;
	*this->parms0 = *parms;
	this->Qxx = new CBlockDiagonalMatrix(*qxx);

}

/**
  * Gets the apriori covariance Matrix (if there)
  */
CBlockDiagonalMatrix* CBestFitLine::getQxx()
{
	return this->Qxx;
}

// Testable 
bool CBestFitLine::runTest(CCharStringArray& report)
{
	CCharString* str = report.Add();

	*str = "-------------------------------------------------------------------------------";

	str = report.Add();
	*str = "CBestFitLine";

	Matrix obs(4, 2);

	obs(1, 1) = 3.00;
	obs(1, 2) = 4.50;
	obs(2, 1) = 4.25;
	obs(2, 2) = 4.25;
	obs(3, 1) = 5.50;
	obs(3, 2) = 5.50;
	obs(4, 1) = 8.00;
	obs(4, 2) = 5.50;

	Matrix sigma(4, 2);

	sigma(1, 1) = .020;//*.020;
	sigma(1, 2) = .015;//*.015;
	sigma(2, 1) = .023;//*.023;
	sigma(2, 2) = .036;//*.036;
	sigma(3, 1) = .033;//*.033;
	sigma(3, 2) = .028;//*.028;
	sigma(4, 1) = .016;//*.016;
	sigma(4, 2) = .019;//*.019;

	//CBestFitLine bfLine;

	CLeastSquaresSolver lss(this, &obs, NULL, NULL, &sigma);
	lss.solve();

	Matrix* solution = lss.getSolution();

	Matrix knownSolution(2, 1);

	knownSolution	<< 3.8543751434466 << .21421845179544;

	bool paramCheck = CMatrixHelper::areEqual(solution, &knownSolution, 1e-10);

	str = report.Add();
	if (paramCheck)
	{
		*str = "\t+ Parameter check successful";
	}
	else
	{
		*str = "\t*** Parameter check failed";
	}

	Matrix* V = lss.getResiduals();

	Matrix knownResids(4, 2);
	knownResids << 1.0455832907009E-003		<< -2.7455179333407E-003 
				<< -4.4186490141481E-002	<< 5.0533800206944E-001
				<< 1.3075033706552E-001		<< -4.3941423690237E-001
				<< -1.0022453798128E-002	<< 6.5975763274431E-002;

	bool residCheck = CMatrixHelper::areEqual(V, &knownResids, 1e-10);

	str = report.Add();
	if (residCheck)
	{
		*str = "\t+ Residual check successful";
	}
	else
	{
		*str = "\t*** Residual check failed";
	}


	Matrix knownC(2, 2);

	knownC	<< 5.6685149551591E-002		<< -9.5567150579038E-003
			<< -9.5567150579038E-003	<< 1.9174186091524E-003;

	Matrix* C = lss.getCovariance();

	bool covarianceCheck =CMatrixHelper::areEqual(C, &knownC, 1e-10);

	str = report.Add();
	if (covarianceCheck)
	{
		*str = "\t+ Covariance check successful";
	}
	else
	{
		*str = "\t*** Covariance check failed";
	}

	return paramCheck & residCheck & covarianceCheck;
}


