#include "TransformationMatrix.h"
#include "math.h"

CTransformationMatrix::CTransformationMatrix()
{
	// initialize as 4 x 4 identity matrix
	this->ReSize(4, 4);
	Real a[] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
	*this << a;
}

CTransformationMatrix::CTransformationMatrix(Matrix M)
{
	this->ReSize(4, 4);

	this->element(0, 0) = M.element(0, 0);
	this->element(1, 0) = M.element(1, 0);
	this->element(2, 0) = M.element(2, 0);
	this->element(3, 0) = M.element(3, 0);
	this->element(0, 1) = M.element(0, 1);
	this->element(1, 1) = M.element(1, 1);
	this->element(2, 1) = M.element(2, 1);
	this->element(3, 1) = M.element(3, 1);
	this->element(0, 2) = M.element(0, 2);
	this->element(1, 2) = M.element(1, 2);
	this->element(2, 2) = M.element(2, 2);
	this->element(3, 2) = M.element(3, 2);
	this->element(0, 3) = M.element(0, 3);
	this->element(1, 3) = M.element(1, 3);
	this->element(2, 3) = M.element(2, 3);
	this->element(3, 3) = M.element(3, 3);
}

CTransformationMatrix::CTransformationMatrix(double omega, double phi, double kappa, double Sx, double Sy, double Sz, double s /*=1*/)
{
	this->ReSize(4, 4);

	double m11 = cos((phi))*cos((kappa));
	double m12 = sin((omega))*sin((phi))*cos((kappa)) + cos((omega))*sin((kappa));
	double m13 = -cos((omega))*sin((phi))*cos((kappa)) + sin((omega))*sin((kappa));
	double m21 = -cos((phi))*sin((kappa));
	double m22 = -sin((omega))*sin((phi))*sin((kappa)) + cos((omega))*cos((kappa));
	double m23 = cos((omega))*sin((phi))*sin((kappa)) + sin((omega))*cos((kappa));
	double m31 = sin((phi));
	double m32 = -sin((omega))*cos((phi));
	double m33 = cos((omega))*cos((phi));

	m11 *= s;
	m12 *= s; 
	m13 *= s;
	m21 *= s;
	m22 *= s;
	m23 *= s;
	m31 *= s;
	m32 *= s;
	m33 *= s;

	(*this)(1, 1) = m11;
	(*this)(1, 2) = m12;
	(*this)(1, 3) = m13;
	(*this)(2, 1) = m21;
	(*this)(2, 2) = m22;
	(*this)(2, 3) = m23;
	(*this)(3, 1) = m31;
	(*this)(3, 2) = m32;
	(*this)(3, 3) = m33;

	(*this)(1, 4) = Sx;
	(*this)(2, 4) = Sy;
	(*this)(3, 4) = Sz;
	(*this)(4, 4) = 1.0;

	(*this)(4, 1) = 0;
	(*this)(4, 2) = 0;
	(*this)(4, 3) = 0;
}

CTransformationMatrix::CTransformationMatrix(const CTransformationMatrix& src)
{
	this->Copy(src);
}

void CTransformationMatrix::Copy(const CTransformationMatrix& src)
{
	*this = src;
}

CTransformationMatrix::~CTransformationMatrix()
{
}

void CTransformationMatrix::form321(Matrix* pP1, Matrix* pP2, Matrix* pP3)
{
	double omega = 0.0;
	double phi = 0.0;
	double kappa = 0.0;

	// calculate kappa (rotation about Z axis)
	kappa = atan2(pP2->element(1, 0) - pP1->element(1, 0), pP2->element(0, 0) - pP1->element(0, 0));

	// calculate phi (rotation about Y' -- which is the first rotated Y axis)
	phi = -atan2(pP2->element(2, 0) - pP1->element(2, 0),
		sqrt((pP2->element(0, 0) - pP1->element(0, 0)) * (pP2->element(0, 0) - pP1->element(0, 0)) + 
		              (pP2->element(1, 0) - pP1->element(1, 0)) * (pP2->element(1, 0) - pP1->element(1, 0)))
		); 

	// form rotation as per wolf's text (transposed), with omega = 0 
	this->element(0, 0) = cos(phi) * cos(kappa);
	this->element(0, 1) = cos(phi) * sin(kappa);
	this->element(0, 2) = -sin(phi);
	this->element(0, 3) = 0.0;
	this->element(1, 0) = sin(omega) * sin(phi) * cos(kappa) - cos(omega) * sin(kappa);
	this->element(1, 1) = sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
	this->element(1, 2) = sin(omega) * cos(phi);
	this->element(1, 3) = 0.0;
	this->element(2, 0) = cos(omega) * sin(phi) * cos(kappa) + sin(omega) * sin(kappa);
	this->element(2, 1) = cos(omega) * sin(phi) * sin(kappa) - sin(omega) * cos(kappa);
	this->element(2, 2) = cos(omega) * cos(phi);
	this->element(2, 3) = 0.0;
	this->element(3, 0) = 0.0;
	this->element(3, 1) = 0.0;
	this->element(3, 2) = 0.0;
	this->element(3, 3) = 1.0;

	// omega = firstRotation * (3rd point - 1st point)
	Matrix rotatedP3(4, 1);
	rotatedP3 = *this * (*pP3 - *pP1);
	
	omega = atan2(rotatedP3.element(2, 0), rotatedP3.element(1, 0));

	// reform the matrix with the newly calculated omega
	this->element(1, 0) = sin(omega) * sin(phi) * cos(kappa) - cos(omega) * sin(kappa);
	this->element(1, 1) = sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
	this->element(1, 2) = sin(omega) * cos(phi);
	this->element(2, 0) = cos(omega) * sin(phi) * cos(kappa) + sin(omega) * sin(kappa);
	this->element(2, 1) = cos(omega) * sin(phi) * sin(kappa) - sin(omega) * cos(kappa);
	this->element(2, 2) = cos(omega) * cos(phi);

	Matrix shift(4, 1);
	shift = -(*this * *pP1);

	double rP3X = shift.element(0, 0);
	double rP3Y = shift.element(1, 0);
	double rP3Z = shift.element(2, 0);

	// reform the matrix with the translation
	this->element(0, 3) = shift.element(0, 0);
	this->element(1, 3) = shift.element(1, 0);
	this->element(2, 3) = shift.element(2, 0);
}

double CTransformationMatrix::getOmega()
{
	// first get phi
	double phi	  = this->getPhi();
	double cosPhi = cos(phi);

	double sinOmega = -(*this)(3, 2) / cosPhi;
	double cosOmega =  (*this)(3, 3) / cosPhi;

	return atan2(sinOmega, cosOmega);
}

double CTransformationMatrix::getPhi()
{
	double sinPhi = (*this)(3, 1);
	double tmp = 1.0 - sinPhi*sinPhi;

	double cosPhi = sqrt(tmp);

	// return phi
	return atan2(sinPhi,cosPhi);
}


double CTransformationMatrix::getKappa()
{
	// first get phi
	double phi = getPhi();
	double cosPhi = cos(phi);

	double sinKappa = -(*this)(2, 1) / cosPhi;
	double cosKappa =  (*this)(1, 1) / cosPhi;

	return atan2(sinKappa,cosKappa);
}
