
#include "CharStringArray.h"
#include "newmat.h"
#include "BlockDiagonalMatrix.h"

#include "BestFitConformalTrans2D.h"
CBestFitConformalTrans2D::CBestFitConformalTrans2D(void)
{
}

CBestFitConformalTrans2D::~CBestFitConformalTrans2D(void)
{
}

/**
* @return a Vector containg the "familiar" names of each parameter.
*/
void CBestFitConformalTrans2D::getParameterNames(CCharStringArray* names)
{
	names->RemoveAll();
	
	CCharString* str = names->Add();
	*str = "a";

	str = names->Add();
	*str = "b";

	str = names->Add();
	*str = "c";

	str = names->Add();
	*str = "d";
}

/**
 * @param obs the observations
 * @return A double[6] all zeros
 */
bool CBestFitConformalTrans2D::approximate(Matrix* approx, Matrix* consts, Matrix* obs)
{
    // just make the array (zeros)
	approx->ReSize(4, 1);

	*approx = 0;

	return true;
}

/**
    * Evaluates the function at the given observation<p>
    *
    * F=<p>
    * |a*x - b*y + c - X|<p>
    * |b*x + a*y + d - Y|<p>
    *
    * @param parms Array of parameters a, b, c, d
    * @param ob Array contaning the observation x, y, X, Y (x,y = secondary; X,Y= primary)<p>
    * @return The Function evaluated
    */
bool CBestFitConformalTrans2D::Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Fi->ReSize(2, 1);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
    double c = (*parms)(3, 1);
    double d = (*parms)(4, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double X = (*ob)(1, 3);
    double Y = (*ob)(1, 4);

    double f00 = a*x - b*y + c - X;
    double f10 = b*x + a*y + d - Y;

    (*Fi)(1, 1) = f00;
    (*Fi)(2, 1) = f10;

	return true;
}

/**
    * Forms the "A" matrix from the general least squares form Ax + Bv + w = 0
    * A = partial(F)/partial(parms)
    * @param parms Array of parameters a, b, c, d
    * @param ob Array contaning the observation x, y, X, Y (x,y = secondary; X,Y= primary)<p>
    * @return The "A" Matrix
    */
bool CBestFitConformalTrans2D::Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Ai->ReSize(2, 4);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
    double c = (*parms)(3, 1);
    double d = (*parms)(4, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double X = (*ob)(1, 3);
    double Y = (*ob)(1, 4);

    double a00 = x;
    double a01 = -y;
    double a02 = 1;
    double a03 = 0;

    double a10 = y;
    double a11 = x;
    double a12 = 0;
    double a13 = 1;

    (*Ai)(1, 1) = a00;
    (*Ai)(1, 2) = a01;
    (*Ai)(1, 3) = a02;
    (*Ai)(1, 4) = a03;

    (*Ai)(2, 1) = a10;
    (*Ai)(2, 2) = a11;
    (*Ai)(2, 3) = a12;
    (*Ai)(2, 4) = a13;

	return true;
}

/**
* Forms the "B" matrix from the general least squares form Ax + Bv + w = 0
* B = partial(F)/partial(Observations)
* @param parms Array of parameters a, b, c, d
* @param ob Array contaning the observation x, y, X, Y (x,y = secondary; X,Y= primary)<p>
* @return The "B" Matrix
*/
bool CBestFitConformalTrans2D::Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Bi->ReSize(2, 4);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
    double c = (*parms)(3, 1);
    double d = (*parms)(4, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double X = (*ob)(1, 3);
    double Y = (*ob)(1, 4);

    double b00 = a;
    double b01 = -b;
    double b02 = -1;
    double b03 = 0;

    double b10 = b;
    double b11 = a;
    double b12 = 0;
    double b13 = -1;

    (*Bi)(1, 1) = b00;
    (*Bi)(1, 2) = b01;
    (*Bi)(1, 3) = b02;
    (*Bi)(1, 4) = b03;
		   
    (*Bi)(2, 1) = b10;
    (*Bi)(2, 2) = b11;
    (*Bi)(2, 3) = b12;
    (*Bi)(2, 4) = b13;

	return true;
}

/**
  * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
  */
void CBestFitConformalTrans2D::getObservationNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "x";

	str = names->Add();
	*str = "y";

	str = names->Add();
	*str = "X";

	str = names->Add();
	*str = "Y";
}

/**
 * @return a String cantainint the name of the best fit ex. "Best fit Line"
 */
void CBestFitConformalTrans2D::getName(CCharString& name)
{
	name = "Best-Fit 2D Conformal trans";
}

/**
 * Sets the a priori parameters information along with a priori
 * covariance Matrix
 */
void CBestFitConformalTrans2D::setParameters(Matrix* parms, CBlockDiagonalMatrix* qxx)
{
	// first delete object (if there)
	this->deleteObjects();

	this->parms0 = new Matrix;
	*this->parms0 = *parms;
	this->Qxx = new CBlockDiagonalMatrix(*qxx);

}

/**
  * Gets the apriori covariance Matrix (if there)
  */
CBlockDiagonalMatrix* CBestFitConformalTrans2D::getQxx()
{
	return this->Qxx;
}

