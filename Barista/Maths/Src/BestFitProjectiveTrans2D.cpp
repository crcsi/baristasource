

#include "LeastSquaresSolver.h"
#include "CharStringArray.h"
#include "newmat.h"
#include "BlockDiagonalMatrix.h"
#include "BestFitAffineTrans2D.h"
#include "BestFitProjectiveTrans2D.h"

 //       X = (a*x + b*y + c) / (dx + ey + 1)
 //       Y = (g*x + h*y + i) / (dx + ey + 1)

CBestFitProjectiveTrans2D::CBestFitProjectiveTrans2D(void)
{
}

CBestFitProjectiveTrans2D::~CBestFitProjectiveTrans2D(void)
{
}

/**
* @return a Vector containg the "familiar" names of each parameter.
*/
void CBestFitProjectiveTrans2D::getParameterNames(CCharStringArray* names)
{
	names->RemoveAll();
	
	CCharString* str = names->Add();
	*str = "a";

	str = names->Add();
	*str = "b";

	str = names->Add();
	*str = "c";

	str = names->Add();
	*str = "d";

	str = names->Add();
	*str = "e";

	str = names->Add();
	*str = "g";

	str = names->Add();
	*str = "h";

	str = names->Add();
	*str = "i";
}

/**
 * @param obs the observations
 * @return A double[9] all zeros
 */
bool CBestFitProjectiveTrans2D::approximate(Matrix* approx, Matrix* consts, Matrix* obs)
{
    // just make the array (zeros)
	approx->ReSize(8, 1);

	// use affine for an approximation
	CBestFitAffineTrans2D affine;

	CLeastSquaresSolver lss(&affine, obs);
	bool success = lss.solve();
	Matrix* pSolution = lss.getSolution();

	approx->element(0, 0) = pSolution->element(0, 0);
	approx->element(1, 0) = pSolution->element(1, 0);
	approx->element(2, 0) = pSolution->element(2, 0);
	approx->element(3, 0) = 0.0;
	approx->element(4, 0) = 0.0;
	approx->element(5, 0) = pSolution->element(3, 0);
	approx->element(6, 0) = pSolution->element(4, 0);
	approx->element(7, 0) = pSolution->element(5, 0);

	return true;
}

/**
 * Evaluates the function at the given observation<p>
 *
 * F=<p>
 //       0 = [(a*x + b*y + c) / (dx + ey + 1)] - X
 //       0 = [(g*x + h*y + i) / (dx + ey + 1)] - Y
 *
 * @param parms Array of parameters a, b, c, d, e, f
 * @param ob Array contaning the observation x, y, X, Y (x,y = secondary; X,Y= primary)<p>
 * @return The Function evaluated
 */
bool CBestFitProjectiveTrans2D::Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Fi->ReSize(2, 1);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
    double c = (*parms)(3, 1);
    double d = (*parms)(4, 1);
    double e = (*parms)(5, 1);
    double g = (*parms)(6, 1);
    double h = (*parms)(7, 1);
    double i = (*parms)(8, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double X = (*ob)(1, 3);
    double Y = (*ob)(1, 4);

	double f00 = ((a*x + b*y + c) / (d*x + e*y + 1)) - X;
	double f10 = ((g*x + h*y + i) / (d*x + e*y + 1)) - Y;

    (*Fi)(1, 1) = f00;
    (*Fi)(2, 1) = f10;

	return true;
}

/**
* Forms the "A" matrix from the general least squares form Ax + Bv + w = 0
* A = partial(F)/partial(parms)
* @param parms Array of parameters a, b, c, d, e, g, h, i
* @param ob Array contaning the observation x, y, X, Y (x,y = secondary; X,Y= primary)<p>
* @return The "A" Matrix
*/
bool CBestFitProjectiveTrans2D::Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Ai->ReSize(2, 8);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
    double c = (*parms)(3, 1);
    double d = (*parms)(4, 1);
    double e = (*parms)(5, 1);
    double g = (*parms)(6, 1);
    double h = (*parms)(7, 1);
    double i = (*parms)(8, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double X = (*ob)(1, 3);
    double Y = (*ob)(1, 4);

	double numX = a*x + b*y + c;
	double numY = g*x + h*y + i;
	double den =  d*x + e*y + 1;

    // Partials (first row)
    double a11 = x / den;
    double a12 = y / den;
    double a13 = 1 / den;
	double a14 = (-numX * x) / (den*den);
    double a15 = (-numX * y) / (den*den);
	double a16 = 0.0;
	double a17 = 0.0;
	double a18 = 0.0;

    // Partials (second row)
    double a21 = 0.0;
    double a22 = 0.0;
    double a23 = 0.0;
    double a24 = (-numY * x) / (den*den);
    double a25 = (-numY * y) / (den*den);
    double a26 = x / den;
    double a27 = y / den;
    double a28 = 1 / den;

    (*Ai)(1, 1) = a11;
    (*Ai)(1, 2) = a12;
    (*Ai)(1, 3) = a13;
    (*Ai)(1, 4) = a14;
    (*Ai)(1, 5) = a15;
    (*Ai)(1, 6) = a16;
    (*Ai)(1, 7) = a17;
    (*Ai)(1, 8) = a18;

    (*Ai)(2, 1) = a21;
    (*Ai)(2, 2) = a22;
    (*Ai)(2, 3) = a23;
    (*Ai)(2, 4) = a24;
    (*Ai)(2, 5) = a25;
    (*Ai)(2, 6) = a26;
    (*Ai)(2, 7) = a27;
    (*Ai)(2, 8) = a28;

	return true;
}

/**
 * Forms the "B" matrix from the general least squares form Ax + Bv + w = 0
 * B = partial(F)/partial(Observations)
 * @param parms Array of parameters a, b, c, e, g, h, i
 * @param ob Array contaning the observation x, y, X, Y (x,y = secondary; X,Y= primary)<p>
 * @return The "B" Matrix
 */
bool CBestFitProjectiveTrans2D::Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Bi->ReSize(2, 4);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
    double c = (*parms)(3, 1);
    double d = (*parms)(4, 1);
    double e = (*parms)(5, 1);
    double g = (*parms)(6, 1);
    double h = (*parms)(7, 1);
    double i = (*parms)(8, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double X = (*ob)(1, 3);
    double Y = (*ob)(1, 4);

	double numX = a*x + b*y + c;
	double numY = g*x + h*y + i;
	double den =  d*x + e*y + 1;

    double b11 = (a * den - (numX * d)) / (den*den);
    double b12 = (b * den - (numX * e)) / (den*den);
    double b13 = -1.0;
    double b14 = 0.0;

    double b21 = (g * den - (numY * d)) / (den*den);
    double b22 = (h * den - (numY * e)) / (den*den);
    double b23 = 0.0;
    double b24 = -1.0;

    (*Bi)(1, 1) = b11;
    (*Bi)(1, 2) = b12;
    (*Bi)(1, 3) = b13;
    (*Bi)(1, 4) = b14;
		   
    (*Bi)(2, 1) = b21;
    (*Bi)(2, 2) = b22;
    (*Bi)(2, 3) = b23;
    (*Bi)(2, 4) = b24;

	return true;
}

/**
  * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
  */
void CBestFitProjectiveTrans2D::getObservationNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "x";

	str = names->Add();
	*str = "y";

	str = names->Add();
	*str = "X";

	str = names->Add();
	*str = "Y";
}

/**
 * @return a String cantainint the name of the best fit ex. "Best fit Line"
 */
void CBestFitProjectiveTrans2D::getName(CCharString& name)
{
	name = "Best-Fit 2D Projective trans";
}

/**
 * Sets the a priori parameters information along with a priori
 * covariance Matrix
 */
void CBestFitProjectiveTrans2D::setParameters(Matrix* parms, CBlockDiagonalMatrix* qxx)
{
	// first delete object (if there)
	this->deleteObjects();

	this->parms0 = new Matrix;
	*this->parms0 = *parms;
	this->Qxx = new CBlockDiagonalMatrix(*qxx);
}

/**
  * Gets the apriori covariance Matrix (if there)
  */
CBlockDiagonalMatrix* CBestFitProjectiveTrans2D::getQxx()
{
	return this->Qxx;
}