// MappedMatrix.cpp: implementation of the CMappedMatrix class.
//
//////////////////////////////////////////////////////////////////////

#include "MappedMatrix.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
//#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
// 
CMappedMatrix::CMappedMatrix()
{
}

/////////////////////////////////////////////////////////////////////////////////
// CMappedMatrix::~CMappedMatrix()
//
// destructor
//
CMappedMatrix::~CMappedMatrix()
{
	delete [] m_ppData;

	m_ppData = 0;

}

/////////////////////////////////////////////////////////////////////////////////
// bool CMappedMatrix::MakeMap(CMatrix& src, int nFirstRow, int nFirstCol, 
//								int nRows, int nCols)
//
//
bool CMappedMatrix::MakeMap(CMatrix& src, int nFirstRow, int nFirstCol, 
								int nRows, int nCols)
{
	// first, validate the rows, they're treated the same,
	// whether the matrix is symmetric or rectangular
	if ( (nFirstRow + nRows - 1) > src.m_rows )
		return false;

	// Since the source is symmetric, we cannot map to any
	// portion below the diagonal (because it ain't there)
	//if (src.IsSymmetric() &&  (nFirstRow + nRows - 1) > nFirstCol)
	//	return false;

	// Check what kind of matrix we are mapping / making
	if ((nFirstRow == nFirstCol) && src.IsSymmetric())
	{
		m_rows = nRows;
		m_cols = -1;
	}
	else
	{
		// Since the source is symmetric, we cannot map to any
		// portion below the diagonal as it doesn't exist
		if (src.IsSymmetric() &&  (nFirstRow + nRows - 1) > nFirstCol)
			return false;

		m_rows = nRows;
		m_cols = nCols;
	}

	// Allocate pointer-pointer
	if (m_ppData != 0)
	{
		delete[] m_ppData;
		m_ppData = 0;
	}
	m_ppData = new double* [m_rows];

	// Map
	for (int i = 0; i < m_rows; i++)
	{
	//	m_ppData[i] = src.__GetDataPointer(nFirstRow + i, nFirstCol);
		m_ppData[i] = src.m_ppData[nFirstRow + i] + nFirstCol;
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// bool CMappedMatrix::MakeSymmetricMap(CMatrix& src, int nFirstRow, int nFirstCol, 
//								int nRows, int nCols)
//
//
bool CMappedMatrix::MakeSymmetricMap(CMatrix& src, int nFirstRow, int nFirstCol, 
								int nRows, int nCols)
{
	// first, validate the rows, they're treated the same,
	// whether the matrix is symmetric or rectangular
	if ( (nFirstRow + nRows - 1) > src.m_rows )
		return false;

	// if the source is symmetric, we cannot map to any
	// portion below the diagonal (because it ain't there)
	if ( src.IsSymmetric() )
	{
		if ( (nFirstCol + nRows - 1) > src.m_rows )
			return false;
	}
	else
	{
		if ( (nFirstCol + nCols - 1) > src.m_cols )
			return false;
	}

	// set m_rows, m_cols is -1, indicating symmetric
	m_rows = nRows;
	m_cols = -1;

	// Allocate pointer-pointer
	if (m_ppData != 0)
	{
		delete[] m_ppData;
		m_ppData = 0;
	}
	m_ppData = new double* [m_rows];

	// Map
	for (int i = 0; i < m_rows; i++)
	{
	//	m_ppData[i] = src.__GetDataPointer(nFirstRow + i, nFirstCol);
		m_ppData[i] = src.m_ppData[nFirstRow + i] + nFirstCol;
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// bool CMappedMatrix::MakeRectangularMap(CMatrix& src, int nFirstRow, int nFirstCol, 
//								int nRows, int nCols)
//
//
bool CMappedMatrix::MakeRectangularMap(CMatrix& src, int nFirstRow, int nFirstCol, 
								int nRows, int nCols)
{
	// first, validate the rows, they're treated the same,
	// whether the matrix is symmetric or rectangular
	if ( (nFirstRow + nRows - 1) > src.m_rows )
		return false;

	// if the source is symmetric, we cannot map to any
	// portion below the diagonal (because it ain't there)
	if ( src.IsSymmetric() )
	{
		if ( (nFirstCol + nCols - 1) > src.m_rows )
			return false;
	}
	else
	{
		if ( (nFirstCol + nCols - 1) > src.m_cols )
			return false;
	}

	m_rows = nRows;
	m_cols = nCols;

	// Allocate pointer-pointer
	if (m_ppData != 0)
	{
		delete[] m_ppData;
		m_ppData = 0;
	}
	m_ppData = new double* [m_rows];

	// Map
	if ( src.IsSymmetric() )
	{
		for (int i = 0; i < m_rows; i++)
		{
		//	m_ppData[i] = src.__GetDataPointer(nFirstRow + i, nFirstCol-i);
			m_ppData[i] = src.m_ppData[nFirstRow + i] + nFirstCol-i;
		}
	}
	else
	{
		for (int i = 0; i < m_rows; i++)
		{
			//m_ppData[i] = src.__GetDataPointer(nFirstRow + i, nFirstCol);
			m_ppData[i] = src.m_ppData[nFirstRow + i] + nFirstCol;
		}
	}

	return true;
}