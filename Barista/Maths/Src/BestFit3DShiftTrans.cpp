
#include "newmat.h"
#include "BlockDiagonalMatrix.h"
#include "LeastSquaresSolver.h"
#include "CharStringArray.h"
#include "MatrixHelper.h"
#include "TextFile.h"
#include "TransformationMatrix.h"
#include <math.h>

#include "BestFit3DShiftTrans.h"
CBestFit3DShiftTrans::CBestFit3DShiftTrans(void)
{
}

CBestFit3DShiftTrans::~CBestFit3DShiftTrans(void)
{
}

/**
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Column Matrix of approximations of the parameters of the least squares fit
  */
bool CBestFit3DShiftTrans::approximate (Matrix* approx, Matrix* consts, Matrix* obs)
{
	approx->ReSize(3, 1);

	if (this->parms0 != NULL)
	{
		*approx = *this->parms0;
		return true;
	}

	*approx = 0;
	return true;
}

/**
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Function evaluated at the given observation unsing the given parameters
  */
bool CBestFit3DShiftTrans::Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Fi->ReSize(3, 1);

	double Sx =	(*parms)(1, 1);
    double Sy =	(*parms)(2, 1);
    double Sz =	(*parms)(3, 1);
	
	double X = (*ob)(1, 1);
	double Y = (*ob)(1, 2);
	double Z = (*ob)(1, 3);
	double x = (*ob)(1, 4);
	double y = (*ob)(1, 5);
	double z = (*ob)(1, 6);

    (*Fi)(1, 1) = X - x - Sx;
    (*Fi)(2, 1) = Y - y - Sy;
    (*Fi)(3, 1) = Z - z - Sz;

    return  true;
}

/**
 * @param parms Column Matrix of approximations of the parameters
 * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
 * @return A Matrix of partials with respect to paramters evaluated at ob
 */
bool CBestFit3DShiftTrans::Ai(Matrix* A, Matrix* parms, Matrix* consts, Matrix* ob)
{
    A->ReSize(3, 3);

    double Sx =	(*parms)(1, 1);
    double Sy =	(*parms)(2, 1);
    double Sz =	(*parms)(3, 1);

	double X = (*ob)(1, 1);
	double Y = (*ob)(1, 2);
	double Z = (*ob)(1, 3);
	double x = (*ob)(1, 4);
	double y = (*ob)(1, 5);
	double z = (*ob)(1, 6);

	double p_f1_Sx = -1;   
	double p_f1_Sy = 0;
	double p_f1_Sz = 0;

	double p_f2_Sx = 0;
	double p_f2_Sy = -1;
	double p_f2_Sz = 0;

	double p_f3_Sx = 0;
	double p_f3_Sy = 0;
	double p_f3_Sz = -1;

	(*A)(1, 1) = p_f1_Sx; 
	(*A)(1, 2) = p_f1_Sy; 
	(*A)(1, 3) = p_f1_Sz; 

	(*A)(2, 1) = p_f2_Sx;
	(*A)(2, 2) = p_f2_Sy;
	(*A)(2, 3) = p_f2_Sz;

	(*A)(3, 1) = p_f3_Sx;
	(*A)(3, 2) = p_f3_Sy;
	(*A)(3, 3) = p_f3_Sz;

	return true;
}

/**
  * @return Bi Matrix of patials with respect to observations for the ith observation
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  */
bool CBestFit3DShiftTrans::Bi(Matrix* B, Matrix* parms, Matrix* consts, Matrix* ob)
{
    B->ReSize(3, 6);

    double Sx =	(*parms)(1, 1);
    double Sy =	(*parms)(2, 1);
    double Sz =	(*parms)(3, 1);

	double X = (*ob)(1, 1);
	double Y = (*ob)(1, 2);
	double Z = (*ob)(1, 3);
	double x = (*ob)(1, 4);
	double y = (*ob)(1, 5);
	double z = (*ob)(1, 6);

	double p_f1_x = -1;
	double p_f1_y = 0;
	double p_f1_z = 0;
	double p_f1_X = 1;
	double p_f1_Y = 0;
	double p_f1_Z = 0;

	double p_f2_x = 0;
	double p_f2_y = -1;
	double p_f2_z = 0;
	double p_f2_X = 0;
	double p_f2_Y = 1;
	double p_f2_Z = 0;

	double p_f3_x = 0;
	double p_f3_y = 0;
	double p_f3_z = -1;
	double p_f3_X = 0;
	double p_f3_Y = 0;
	double p_f3_Z = 1;

	(*B)(1, 1) = p_f1_x;
	(*B)(1, 2) = p_f1_y;
	(*B)(1, 3) = p_f1_z;
	(*B)(1, 4) = p_f1_X;
	(*B)(1, 5) = p_f1_Y;
	(*B)(1, 6) = p_f1_Z;
				 
	(*B)(2, 1) = p_f2_x;
	(*B)(2, 2) = p_f2_y;
	(*B)(2, 3) = p_f2_z;
	(*B)(2, 4) = p_f2_X;
	(*B)(2, 5) = p_f2_Y;
	(*B)(2, 6) = p_f2_Z;
				 
	(*B)(3, 1) = p_f3_x;
	(*B)(3, 2) = p_f3_y;
	(*B)(3, 3) = p_f3_z;
	(*B)(3, 4) = p_f3_X;
	(*B)(3, 5) = p_f3_Y;
	(*B)(3, 6) = p_f3_Z;

    return true;
}

/**
  * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
  */
void CBestFit3DShiftTrans::getParameterNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "a0";

	str = names->Add();
	*str = "a1";
}

/**
 * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
 */
void CBestFit3DShiftTrans::getObservationNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "x";

	str = names->Add();
	*str = "y";
}

/**
 * @return a String cantainint the name of the best fit ex. "Best fit Line"
 */
void CBestFit3DShiftTrans::getName(CCharString& name)
{
	name = "Best-Fit 3D Shift";
}

/**
 * Sets the a priori parameters information along with a priori
 * covariance Matrix
 */
void CBestFit3DShiftTrans::setParameters(Matrix* parms, CBlockDiagonalMatrix* qxx)
{
	this->parms0 = new Matrix;
	*this->parms0 = (*parms);
	this->Qxx = new CBlockDiagonalMatrix(*qxx);
}

/**
  * Gets the apriori covariance Matrix (if there)
  */
CBlockDiagonalMatrix* CBestFit3DShiftTrans::getQxx()
{
	return this->Qxx;
}



bool CBestFit3DShiftTrans::runTest(CCharStringArray& report)
{
	CCharString* str = report.Add();
	*str = "-------------------------------------------------------------------------------";
	str = report.Add();
	*str = "CBestFit3DShiftTrans";

	return true;
}
