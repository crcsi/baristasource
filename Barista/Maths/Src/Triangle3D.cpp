// Triangle3D.cpp: implementation of the CTriangle3D class.
//
//////////////////////////////////////////////////////////////////////

#include "Triangle3D.h"
#include "Triangle3DArray.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



CTriangle3D::CTriangle3D()
{
	this->point1.ReSize(3, 1);

	this->point1.Row(1) << 1;
	this->point1.Row(2) << 2;
	this->point1.Row(3) << 3;


	this->point2.ReSize(3, 1);
	this->point3.ReSize(3, 1);
}

CTriangle3D::~CTriangle3D()
{

}

Matrix* CTriangle3D::getPoint1()
{
	return &this->point1;
}

Matrix* CTriangle3D::getPoint2()
{
	return &this->point2;
}

Matrix* CTriangle3D::getPoint3()
{
	return &this->point3;
}

CTriangle3DArray CTriangle3D::tesselate()
{
	CTriangle3DArray New;

	// 1
	CTriangle3D* pTri = New.Add();
	pTri->getPoint1()->Row(1) << this->point1(1, 1);
	pTri->getPoint1()->Row(2) << this->point1(2, 1);
	pTri->getPoint1()->Row(3) << this->point1(3, 1);

	pTri->getPoint2()->Row(1) << (this->point1(1, 1) + this->point2(1, 1))/2.0;
	pTri->getPoint2()->Row(2) << (this->point1(2, 1) + this->point2(2, 1))/2.0;
	pTri->getPoint2()->Row(3) << (this->point1(3, 1) + this->point2(3, 1))/2.0;

	pTri->getPoint3()->Row(1) << (this->point1(1, 1) + this->point3(1, 1))/2.0;
	pTri->getPoint3()->Row(2) << (this->point1(2, 1) + this->point3(2, 1))/2.0;
	pTri->getPoint3()->Row(3) << (this->point1(3, 1) + this->point3(3, 1))/2.0;

	// 2
	pTri = New.Add();
	pTri->getPoint2()->Row(1) << this->point2(1, 1);
	pTri->getPoint2()->Row(2) << this->point2(2, 1);
	pTri->getPoint2()->Row(3) << this->point2(3, 1);

	pTri->getPoint1()->Row(1) << (this->point1(1, 1) + this->point2(1, 1))/2.0;
	pTri->getPoint1()->Row(2) << (this->point1(2, 1) + this->point2(2, 1))/2.0;
	pTri->getPoint1()->Row(3) << (this->point1(3, 1) + this->point2(3, 1))/2.0;

	pTri->getPoint3()->Row(1) << (this->point2(1, 1) + this->point3(1, 1))/2.0;
	pTri->getPoint3()->Row(2) << (this->point2(2, 1) + this->point3(2, 1))/2.0;
	pTri->getPoint3()->Row(3) << (this->point2(3, 1) + this->point3(3, 1))/2.0;

	// 3
	pTri = New.Add();
	pTri->getPoint3()->Row(1) << this->point3(1, 1);
	pTri->getPoint3()->Row(2) << this->point3(2, 1);
	pTri->getPoint3()->Row(3) << this->point3(3, 1);

	pTri->getPoint1()->Row(1) << (this->point1(1, 1) + this->point3(1, 1))/2.0;
	pTri->getPoint1()->Row(2) << (this->point1(2, 1) + this->point3(2, 1))/2.0;
	pTri->getPoint1()->Row(3) << (this->point1(3, 1) + this->point3(3, 1))/2.0;

	pTri->getPoint2()->Row(1) << (this->point2(1, 1) + this->point3(1, 1))/2.0;
	pTri->getPoint2()->Row(2) << (this->point2(2, 1) + this->point3(2, 1))/2.0;
	pTri->getPoint2()->Row(3) << (this->point2(3, 1) + this->point3(3, 1))/2.0;
	
	// 4
	pTri = New.Add();

	pTri->getPoint1()->Row(1) << (this->point1(1, 1) + this->point2(1, 1))/2.0;
	pTri->getPoint1()->Row(2) << (this->point1(2, 1) + this->point2(2, 1))/2.0;
	pTri->getPoint1()->Row(3) << (this->point1(3, 1) + this->point2(3, 1))/2.0;

	pTri->getPoint2()->Row(1) << (this->point1(1, 1) + this->point3(1, 1))/2.0;
	pTri->getPoint2()->Row(2) << (this->point1(2, 1) + this->point3(2, 1))/2.0;
	pTri->getPoint2()->Row(3) << (this->point1(3, 1) + this->point3(3, 1))/2.0;

	pTri->getPoint3()->Row(1) << (this->point2(1, 1) + this->point3(1, 1))/2.0;
	pTri->getPoint3()->Row(2) << (this->point2(2, 1) + this->point3(2, 1))/2.0;
	pTri->getPoint3()->Row(3) << (this->point2(3, 1) + this->point3(3, 1))/2.0;
	
	
	return New;

}



