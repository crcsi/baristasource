/**
 * @class CLeastSquaresSolver
 *  Title:        LeastSquaresSolver<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Harry Hanley<p>
 * Company:      University of Melbourne<p>
 * @author Harry Hanley
 * @version 1.0
 *
 */

#include "LeastSquaresModel.h"

#include "TextFile.h"
#include "IntegerArrayArray.h"
#include <math.h>

#include "LeastSquaresSolver.h"
/**
 * Solves the least squares fit defined by the given Model derived class
 */
void CLeastSquaresSolver::init()
{
    this->LeastSquaresModel = NULL;
    this->Obs = NULL;

//    Matrix              updatedObs = null;
//    Matrix              Residuals = null;
//    Matrix              previousResiduals = null;
    this->Constants = NULL;

//    this->Cofacter = NULL;

//    Matrix              Covariance = null;

//    Vector              RejectedLabels = new Vector();
//    Matrix              RejectedResiduals = null; // cronk

    this->PreSigma0 = 1.0;
    this->PostSigma0 = 0.0;
    this->ConvergenceLimit = .0000001;
//    Matrix              Solution = null;
    this->ObsLabels = NULL;

//    BlockDiagonalMatrix Qxx = null;
	this->Wxx = NULL;

//    JLeastSquaresStatusControl StatusControl = new JLeastSquaresStatusControl();
//    Matrix              parms0 = null;  // Initital approximations
                                        //of parameters

//    Vector              EventListeners = new Vector();

    this->chiSquaredTest = 0;
    this->observationCount = 0;
    this->degreesOfFreedom = 0;

    this->iterationCount = 0;
    this->abort = false;
    this->currentRMS = 0.0;
    this->currentObsName = "";
    this->debug = false;
    this->pope = false;

    this->observationIndices = NULL; //

// 	private int 		        Status = this->STATUS_NOT_RUN;

}

CLeastSquaresSolver::~CLeastSquaresSolver()
{

}

// minimum constructor
CLeastSquaresSolver::CLeastSquaresSolver (CLeastSquaresModel* LeastSquaresModel, Matrix* Observations)
{
	this->init();
    this->LeastSquaresModel = LeastSquaresModel;
    this->Obs = *Observations;
    this->Constants = Constants;

    int nObservations = Observations->Nrows();

    for (int i = 0; i < nObservations; i++)
    {
        CCharString* pStr = this->ObsLabels.Add();
		
		pStr->Format("%d", i);
    }

    int nParts = Observations->Ncols();
    this->observationCount = nObservations*nParts;

    // Create a cofacter matrix
    // In general this allows for the "parts" of each ovservation
    // to be coorelated i.e. an xyz point represents an observation of
    // 3 parts, each of which may have coorelation with the others.
    // This class does not support coorelation accross all ovservations
    // i.e. point to point coorelation
    this->Cofacter.resize(nParts, nParts, nObservations);

    // For this particular case there is no covariance, so just fill the
    // principle diagonal
    for (int i = 0; i < nObservations; i++)
    {
        Matrix* Qi = this->Cofacter.getSubMatrix(i);
        for (int r = 0; r < nParts; r++)
        {
            Qi->element(r, r) = 1e-3; // assume small values
        }
    }

    this->previousResiduals.ReSize(nObservations, nParts);
    this->Residuals.ReSize(nObservations, nParts);

    this->RejectedResiduals.ReSize(nObservations, nParts); // cronk
}


/**
    * Constructor
    * @param     Model Model - An object implementing the Model Interface
    * @param     Matrix Observations Matrix of Observations, were each observation may be severasl parts (like an XYZ point)
    * @see JModel
    */
CLeastSquaresSolver::CLeastSquaresSolver (CLeastSquaresModel* LeastSquaresModel,
                            Matrix* Observations, CCharStringArray* ObsLabels,
                            Matrix* Constants, Matrix* Sigmas)
{
	this->init();
    this->LeastSquaresModel = LeastSquaresModel;
    this->Obs = *Observations;
    this->Constants = Constants;

    int nObservations = Observations->Nrows();

    if (ObsLabels != NULL)
        this->ObsLabels = *ObsLabels;
    else
    {
        for (int i = 0; i < nObservations; i++)
        {
            CCharString* pStr = this->ObsLabels.Add();
			
			pStr->Format("%d", i);
        }
    }

    int nParts = Observations->Ncols();
    this->observationCount = nObservations*nParts;

    // Create a cofacter matrix
    // In general this allows for the "parts" of each ovservation
    // to be coorelated i.e. an xyz point represents an observation of
    // 3 parts, each of which may have coorelation with the others.
    // This class does not support coorelation accross all ovservations
    // i.e. point to point coorelation
    this->Cofacter.resize(nParts, nParts, nObservations);

    // For this particular case there is no covariance, so just fill the
    // principle diagonal
    for (int i = 0; i < nObservations; i++)
    {
        Matrix* Qi = this->Cofacter.getSubMatrix(i);
        for (int r = 0; r < nParts; r++)
        {
            Qi->element(r, r) = Sigmas->element(i, r)*Sigmas->element(i, r);
        }
    }

    this->previousResiduals.ReSize(nObservations, nParts);
    this->Residuals.ReSize(nObservations, nParts);

    this->RejectedResiduals.ReSize(nObservations, nParts); // cronk
}

/**
    * Constructor
    * @param     LeastSquaresModel LeastSquaresModel - An object implementing the LeastSquaresModel Interface
    * @param     double[][] Observations Array of Observations, each observation is an array of doubles
    * @see CLeastSquaresModel
    */
CLeastSquaresSolver::CLeastSquaresSolver (CLeastSquaresModel* LeastSquaresModel,
                            Matrix* Observations, CCharStringArray* ObsLabels,
                            Matrix* Constants, CBlockDiagonalMatrix* Q)
{
 	this->init();
    this->LeastSquaresModel = LeastSquaresModel;
    this->Obs = *Observations;
    this->Constants = Constants;

    int nObservations = Observations->Nrows();

    if (ObsLabels != NULL)
        this->ObsLabels = *ObsLabels;
    else
    {
		for (int i = 0; i < nObservations; i++)
        {
            CCharString* str = this->ObsLabels.Add();

			str->Format("%d", i);
        }
    }

    int nParts = Observations->Ncols();
    this->observationCount = nObservations*nParts;

    this->Cofacter = *Q;

    this->previousResiduals.ReSize(nObservations, nParts);
    this->Residuals.ReSize(nObservations, nParts);

    this->RejectedResiduals.ReSize(nObservations, nParts); // cronk
}

/**
    * Constructor
    * @param     LeastSquaresModel LeastSquaresModel - An object implementing the LeastSquaresModel Interface
    * @param     double[][] Observations Array of Observations, each observation is an array of doubles
    * @see CLeastSquaresModel
    */
CLeastSquaresSolver::CLeastSquaresSolver (CLeastSquaresModel* LeastSquaresModel, Matrix* Constants,
                            Matrix* Observations, CCharStringArray* ObsLabels)
{
	this->init();
    this->LeastSquaresModel = LeastSquaresModel;
    this->Obs = *Observations;
    this->Constants = Constants;

    int nObservations = Observations->Nrows();

    if (ObsLabels != NULL)
        this->ObsLabels = *ObsLabels;
    else
    {
		for (int i = 0; i < nObservations; i++)
        {
            CCharString* str = this->ObsLabels.Add();

			str->Format("%d", i);
        }
    }

    int nParts = Observations->Ncols();
    this->observationCount = nObservations*nParts;

	this->Cofacter.resize(nParts, nParts, nObservations);

    // For this particular case there is no covariance, so just fill the
    // principle diagonal with 1s
    for (int i = 0; i < nObservations; i++)
    {
        Matrix* Qi = this->Cofacter.getSubMatrix(i);

        for (int r = 0; r < nParts; r++)
        {
            Qi->element(r, r) = 1.0;
        }
    }

    this->previousResiduals.ReSize(nObservations, nParts);
    this->Residuals.ReSize(nObservations, nParts);

    this->RejectedResiduals.ReSize(nObservations, nParts); // cronk
}

/** A = A + B
@param B    a BlockDiagonalMatrix
@return     A + B
*/
void CLeastSquaresSolver::plusEquals (Matrix*N, CBlockDiagonalMatrix* B)
{
    //checkMatrixDimensions(B);

    int rowStart = 1;
    int colStart = 1;

    // process each submatrix
    for (int m = 0; m < B->getSubMatrixCount(); m++)
    {
        Matrix* S = B->getSubMatrix(m);
        int rowEnd = rowStart + S->Nrows() - 1;
        int colEnd = colStart + S->Ncols() - 1;

        for (int i = rowStart, ii = 1; i <= rowEnd; i++, ii++)
        {
            for (int j = colStart, jj = 1; j <= colEnd; j++, jj++)
            {
                double val = (*S)(ii, jj);
                (*N)(i, j) += val;
            }
        }

        rowStart = rowEnd + 1;
        colStart = colEnd + 1;

    }
}



/**
    * Solves the Least Squares fit
    * @return An array of parameters from the fit
    */
bool CLeastSquaresSolver::solve() // (Matrix& solution)
{
    Try
    {
        int nObs = this->Obs.Nrows();

        // sanity check
        if (nObs == 0)
		{
            return false;
		}

        if (!this->LeastSquaresModel->approximate(&this->Solution, this->Constants, &this->Obs))
			return false;

        this->parms0 = this->Solution;

        // Do we have an a priori covariance for the parameters?
        this->Qxx = this->LeastSquaresModel->getQxx();

        if (this->Qxx != NULL)
		{
			if (this->Wxx != NULL)
				delete this->Wxx;

			this->Wxx = new CBlockDiagonalMatrix(this->Qxx->getRowDimension(), 
				this->Qxx->getColumnDimension(), 
				this->Qxx->getSubMatrixCount());

			Qxx->inverse(*(this->Wxx));
		}

        int nParms = Solution.Nrows();
        int nParts = this->Obs.Ncols();
		int DOF = nObs * (nParts - this->LeastSquaresModel->getNonDistinctObservationCount()) - nParms + getConstraintCount();
        this->setDOF(DOF);
        this->StatusControl.setDOF(this->getDOF());

        this->PreSigma0 = 1;

        Matrix Delta(nParms, 1);       // Solution vector

        Matrix vTv(1, 1);

        double LastRMS = 0;
        bool bConverged = false;

        Matrix N; // The normals
		Matrix t; // the right hand side

        int rejectionCount = 0; // to keep spot in matrix - cronk

        for (int iter = 1; iter <= this->StatusControl.getMaxIters(); iter++)
        {
            if (this->debug)
            {
                printf("iter: %d", iter);
            }

            this->StatusControl.setIter(iter);
            //JLeastSquaresEvent Event = new JLeastSquaresEvent(this, this->StatusControl, JLeastSquaresEvent.GENERAL);
            //this->TriggerEvent(Event);

            if (this->StatusControl.getAbort())
            {
                this->StatusControl.setStatusAborted();
                this->StatusControl.setStatusDone();
                //JLeastSquaresEvent Event2 = new JLeastSquaresEvent(this, this->StatusControl, JLeastSquaresEvent.GENERAL);
                //this->TriggerEvent(Event2);
                return false;
            }

            // Zero out normals
            N.ReSize(nParms, nParms);
			N = 0;

            t.ReSize(nParms, 1);
			t = 0;

            vTv(1, 1) = 0.0;

            this->updatedObs = this->Obs + this->Residuals;

            for (int i = 0; i < nObs; i++)
            {
                if (this->observationIndices != NULL)
                {
                    CMUIntegerArray* indexes = this->observationIndices->GetAt(i);
//                    int[] indexes = (int[])this->observationIndices.get(i);
                    this->LeastSquaresModel->setObservationIndices(indexes);
                }

                this->setCurrentObsName(*(this->ObsLabels.GetAt(i)));

                Matrix w;// = null;
                Matrix Ai;// = null;
                Matrix Bi;// = null;

                // !!!new way
                Matrix Observation;// = null;
                Matrix previousResid(1, 1);
				previousResid = 0;

                if (this->pope)
                {
                    Observation = this->updatedObs.SubMatrix(i+1, i+1, 1, nParts);
                    previousResid = this->previousResiduals.SubMatrix(i+1, i+1, 1, nParts).t();
                }
                else
                {
                    Observation = this->Obs.SubMatrix(i+1, i+1, 1, nParts);
                }

                Matrix Constant;
                if (this->Constants != NULL)
                {
                    int nconstparts = this->Constants->Ncols();
                    Constant = this->Constants->SubMatrix(i+1, i+1, 1, nconstparts);
                }

                // Form the cofactor matrix for the ith observation
                Matrix* Qi = this->GetQ(i);

                this->LeastSquaresModel->accumulate(&N, &t, &this->Solution, &Constant, &Observation, &previousResid, Qi);
            }

            // solve normals
            if (this->Wxx != NULL)
            {
                this->plusEquals(&N, this->Wxx);

                Matrix fx = this->parms0 - this->Solution;
                Matrix hmm; 
				this->Wxx->times(&hmm, &fx);

                t = t - hmm;
            }

			this->LeastSquaresModel->addConstraint(&N, &t, &this->Solution);
			Matrix Ni = N.i();
			Delta = Ni * t;

			Delta = Delta * -1.0;

            // Calculate residuals
            for (int i = 0; i < nObs; i++)
            {
			    // now 
                if (this->observationIndices != NULL)
                {
                    CMUIntegerArray* indexes = this->observationIndices->GetAt(i);
//                    int[] indexes = (int[])this->observationIndices.get(i);
                    this->LeastSquaresModel->setObservationIndices(indexes);
                }

                // !!!new way
                Matrix Observation;
                Matrix previousResid(1, 1);
				previousResid = 0;

                if (this->pope)
                {
                    Observation = this->updatedObs.SubMatrix(i+1, i+1, 1, nParts);
                    previousResid = this->previousResiduals.SubMatrix(i+1, i+1, 1, nParts).t();
                }
                else
                {
                    Observation = this->Obs.SubMatrix(i+1, i+1, 1, nParts);
                }

                Matrix Constant;
                if (this->Constants != NULL)
                {
                    int nconstparts = this->Constants->Ncols();
                    Constant = this->Constants->SubMatrix(i+1, i+1, 1, nconstparts);
                }

                // Form the cofactor matrix for the ith observation
                Matrix* Qi = this->GetQ(i);

                Matrix V;
				
				this->LeastSquaresModel->calculateResiduals(&V, &Delta, &this->Solution, &Constant, &Observation, &previousResid, Qi);
                
 				for (int j = 1; j <= this->Residuals.Ncols(); j++)
                {
                    this->Residuals(i+1, j) = V(j, 1);
                }

                vTv = vTv + V.t() * Qi->i() * V;
            }

            this->previousResiduals = this->Residuals;

			double rms = 0;
            for (int i = 1; i <= this->Residuals.Nrows(); i++)
			{
                for (int j = 1; j <= this->Residuals.Ncols(); j++)
				{
                    rms += this->Residuals(i, j)*this->Residuals(i, j);
				}
			}

			this->StatusControl.setRMS(::sqrt(rms/this->observationCount));

            this->setCurrentRMS(this->StatusControl.getRMS());

//@@@            this->TriggerEvent(new JLeastSquaresEvent(this, this->StatusControl, JLeastSquaresEvent.GENERAL));

            // update parameters
            this->Solution = this->Solution + Delta;

			double diff = ::fabs(this->StatusControl.getRMS() - LastRMS);

            LastRMS = this->StatusControl.getRMS();

            if ( (iter > 0) && (diff < this->ConvergenceLimit))
            {
                if (this->StatusControl.getDoReject())
                {
                    double rms = 0;

                    for (int i = 1; i <= this->Residuals.Nrows(); i++)
					{
                        for (int j = 1; j <= this->Residuals.Ncols(); j++)
						{
                            rms += this->Residuals(i, j)*this->Residuals(i, j);
						}
					}

					rms = ::sqrt(rms/this->observationCount);

                    this->StatusControl.setRejectionLimit(rms * this->StatusControl.getRejectionMultiplier() + this->StatusControl.getRejectionAdd());

                    double buf = this->StatusControl.getRejectionLimit();

                    //First count the rejections
                    int count = 0;
                    for (int i = 1; i <= this->Residuals.Nrows(); i++)
                    {
                        for (int j = 1; j <= this->Residuals.Ncols(); j++)
                        {
                            double resid = this->Residuals(i, j);
							if (::fabs(resid) > this->StatusControl.getRejectionLimit())
                            {
                                count++;
                                break;
                            }
                        }
                    }

                    if (count == 0)
                    {
                        this->StatusControl.setStatusConverged();
                       // JLeastSquaresEvent Event2 = new JLeastSquaresEvent(this, this->StatusControl, JLeastSquaresEvent.GENERAL);
                       // this->TriggerEvent(Event2);
                        bConverged = true;
                        break;
                    }
                    else
                    {
                        this->StatusControl.setRejectionCount(this->StatusControl.getRejectionCount() + count);

//                        JLeastSquaresEvent Event2 = new JLeastSquaresEvent(this, this->StatusControl, JLeastSquaresEvent.GENERAL);
//                      this->TriggerEvent(Event2);

                        int nNewObs = nObs - count;
                        Matrix NewObs(nNewObs, nParts);

                        CBlockDiagonalMatrix NewCofactor;

                        CCharStringArray NewObsLabels;
                        //Vector NewObservationIndices = null;

                        //if (observationIndices != null)
                        //    NewObservationIndices = new Vector();

                        int index = 1;

                        // Loop over all residuals
                        for (int i = 1; i <= this->Residuals.Nrows(); i++)
                        {
                            bool skip = false;

                            // Loop over all parts of the joint observation
                            for (int j = 1; j <= nParts; j++)
                            {
								if (::fabs(this->Residuals(i, j)) > this->StatusControl.getRejectionLimit())
                                {
                                    skip = true;
                                    break;
                                }
                            }

                            if (skip)
                            {
                                this->RejectedLabels.Add(*(this->ObsLabels.GetAt(i)));

                                // here cronk
                                for(int j = 1; j <= nParts; j++)
                                    this->RejectedResiduals(rejectionCount, j) = this->Residuals(i,j);

                                rejectionCount++;
                                // to here cronk

                                continue;
                            }

                            for (int j = 1; j <= nParts; j++)
                                NewObs(index, j) = this->Obs(i, j);

                            NewObsLabels.Add(*(this->ObsLabels.GetAt(i)));

                            NewCofactor.addSubMatrix(*this->Cofacter.getSubMatrix(i));

//                            if (NewObservationIndices != null)
//                              NewObservationIndices.add(index, this->observationIndices.get(i));

                            index++;
                        }

                        nObs = nNewObs;
                        this->observationCount = nObs*nParts;

						// change to new obs
                        this->Obs = NewObs;

						// change to new cofacter
                        this->Cofacter = NewCofactor;

						// delete the old labels
                        this->ObsLabels = NewObsLabels;

                        //this->observationIndices = NewObservationIndices;

						// delete the old residuals
						this->Residuals.ReSize(nObs, nParts);
                    }
                }
                else
                {
                    this->StatusControl.setStatusConverged();
                   // JLeastSquaresEvent Event2 = new JLeastSquaresEvent(this, this->StatusControl, JLeastSquaresEvent.GENERAL);
                   // this->TriggerEvent(Event2);
                    bConverged = true;
                    break;
                }
            }

        } //for (int iter = 0; iter < MaxIters; iter++)

        if (!bConverged)
        {
            this->StatusControl.setStatusMaxIterations();
//            JLeastSquaresEvent Event2 = new JLeastSquaresEvent(this, this->StatusControl, JLeastSquaresEvent.GENERAL);
//            this->TriggerEvent(Event2);
        }

        // compute covariance matrix,
        // Note that this might be able to be extracted from
        // the line of code above "X = N.solve(t);"
        // in the future.

        //Matrix I;
		
		//I = Matrix::I= Matrix.identity(N.getRowDimension(), N.getRowDimension());
        this->Covariance = N.i();


//        double D = N.Determinant();

        // Calculation of the Post Sigma0
        this->setDOF(nObs * (nParts - this->LeastSquaresModel->getNonDistinctObservationCount()) - nParms + getConstraintCount());
        this->StatusControl.setDOF(this->getDOF());

		this->PostSigma0 = ::sqrt(vTv(1, 1) / this->getDOF());

        this->Covariance = this->Covariance * (this->PostSigma0 * this->PostSigma0);

        this->chiSquaredTest = (this->getDOF() * this->PostSigma0 * this->PostSigma0)/(this->PreSigma0 * this->PreSigma0);

        this->StatusControl.setStatusDone();
//        JLeastSquaresEvent Event = new JLeastSquaresEvent(this, this->StatusControl, JLeastSquaresEvent.GENERAL);
//        this->TriggerEvent(Event);

        return  true;
    }

	
    CatchAll //(char* error) 
    {

    //    e.printStackTrace();
     //   this->StatusControl.status_criticalError = true;
      //  JLeastSquaresEvent Event = new JLeastSquaresEvent(this, this->StatusControl, JLeastSquaresEvent.GENERAL);
      //  this->TriggerEvent(Event);
        return false;
    }
	
}

CLeastSquaresStatusControl* CLeastSquaresSolver::getStatus()
{
    return &this->StatusControl;
}

/**
    * Get the count of constrained parameters
    * @return number od constrained parameters
    */
int CLeastSquaresSolver::getConstraintCount()
{
	// Constraints are in the Wxx matrix
	if (this->Wxx == NULL)
		return 0;

	// By convention, a weight of 1 or greater will constitute a constraint
	// Look for "high" weights along the diagonal of Wxx
	int count = 0;
	for (int m = 0; m < this->Wxx->getSubMatrixCount(); m++)
	{
		Matrix* S = Wxx->getSubMatrix(m);

		for (int i = 1; i <= S->Nrows(); i++)
		{
			double val = (*S)(i, i);
			if (val > 1)
				count++;
		}
	}

	return count;
}

/**
    * Gets the cofactor matrix for the given observation
    * @param i index to Observation
    */
Matrix* CLeastSquaresSolver::GetQ(int i)
{
    Matrix* Q = this->Cofacter.getSubMatrix(i);
    return Q;
}


Matrix* CLeastSquaresSolver::getSolution()
{
    return &this->Solution;
}

/**
    * Gets the Residuals of the least square solution
    * @return the array of residuals (indices match the ingoing observations)
    */
Matrix* CLeastSquaresSolver::getResiduals()
{
    return &this->Residuals;
}

Matrix* CLeastSquaresSolver::getCovariance()
{
    return &this->Covariance;
}

double CLeastSquaresSolver::getPostSigma0()
{
    return this->PostSigma0;
}

/**
  * Gets the matrix of Correlation Coefficients
  */
Matrix* CLeastSquaresSolver::getCorrelation()
{
	int n = this->Covariance.Nrows();
	this->correlation.ReSize(n, n);

	for (int i = 1; i <= this->Covariance.Nrows(); i++)
	{
		for (int j = 1; j <= this->Covariance.Ncols(); j++)
		{
			double val = this->Covariance(i, j) / (::sqrt(this->Covariance(i, i) * this->Covariance(j, j)));
			correlation(i, j) = val;
		}
	}

	return &correlation;
}

CLeastSquaresModel* CLeastSquaresSolver::getLeastSquaresModel()
{
    return this->LeastSquaresModel;
}

CCharStringArray* CLeastSquaresSolver::getObsLabels()
{
    return &this->ObsLabels;
}

void CLeastSquaresSolver::run()
{
    this->solve();
}

 void CLeastSquaresSolver::setAbort(bool b)
{
    this->abort = b;
}

double CLeastSquaresSolver::getCurrentRMS()
{
    return this->currentRMS;
}

void CLeastSquaresSolver::setCurrentRMS(double d)
{
    this->currentRMS = d;
}

void CLeastSquaresSolver::getCurrentObsName(CCharString& label)
{
    label = this->currentObsName;
}

void CLeastSquaresSolver::setCurrentObsName(CCharString s)
{
    this->currentObsName = s;
}

void CLeastSquaresSolver::setCurrentObsName(int i)
{
    this->currentObsName.Format("%d", i);
}

int CLeastSquaresSolver::getObservationCount()
{
    return this->observationCount;
}

int CLeastSquaresSolver::getDOF()
{
    return this->degreesOfFreedom;
}

void CLeastSquaresSolver::setDOF(int i)
{
    this->degreesOfFreedom = i;
}

void CLeastSquaresSolver::setObservationIndices(CIntegerArrayArray* indices)
{
    this->observationIndices = indices;
}

void CLeastSquaresSolver::setConvergenceLimit(double c)
{
    this->ConvergenceLimit = c;
}

CCharStringArray* CLeastSquaresSolver::getRejectedLabels()
{
    return &this->RejectedLabels;
}

Matrix* CLeastSquaresSolver::getRejectedResiduals()
{
    return &this->RejectedResiduals;
}

bool CLeastSquaresSolver::getDoRejection()
{
    return this->StatusControl.getDoReject();
}

/*
void CLeastSquaresSolver::addEventListener(JLeastSquaresListener EventLister)
{
    this->EventListeners.add(EventLister);
}
*/

/*
void CLeastSquaresSolver::TriggerEvent(JLeastSquaresEvent Event)
{
    for (int i = 0; i < this->EventListeners.size(); i++)
    {
        JLeastSquaresListener EventLister = (JLeastSquaresListener)this->EventListeners.get(i);

        EventLister.LeastSquaresChanged(Event);
    }
}
*/

void CLeastSquaresSolver::setDoRejection(bool b)
{
    this->StatusControl.setDoReject(b);
}

CLeastSquaresStatusControl* CLeastSquaresSolver::getStatusControl()
{
    return &this->StatusControl;
}
