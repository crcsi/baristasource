
#include "newmat.h"
#include "BlockDiagonalMatrix.h"
#include "LeastSquaresSolver.h"
#include "CharStringArray.h"
#include "MatrixHelper.h"
#include "TextFile.h"
#include "TransformationMatrix.h"
#include <math.h>

#include "BestFit3DTrans.h"
#ifndef PI 
#define PI 4.0*atan(1.0)
#endif


CBestFit3DTrans::CBestFit3DTrans(void)
{
}

CBestFit3DTrans::~CBestFit3DTrans(void)
{
	this->deleteObjects();
}

void CBestFit3DTrans::deleteObjects()
{
	if (this->parms0 != NULL)
	{
		delete this->parms0;
		this->parms0 = NULL;
	}

	if (this->Qxx != NULL)
	{
		delete this->Qxx;
		this->Qxx = NULL;
	}
}

/**
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Column Matrix of approximations of the parameters of the least squares fit
  */
bool CBestFit3DTrans::approximate (Matrix* approx, Matrix* consts, Matrix* obs)
{
	approx->ReSize(7, 1);

	if (this->parms0 != NULL)
	{
		*approx = *this->parms0;
		return true;
	}

	*approx = 0;
	(*approx)(7, 1) = 1;

	double s = this->approximateScale(obs);

	double bestRMS = 1e10;
	Matrix shift;
	for (double omega = -PI+.02; omega < PI; omega += PI/2)
	{
		for (double phi = -PI/2+.02; phi < PI/2; phi += PI/2)
		{
			for (double kappa = -PI+.02; kappa < PI; kappa += PI/2)
			{
				CTransformationMatrix M1(omega, phi, kappa, 0, 0, 0, s);

				this->approximateShift(M1, obs, shift);
				double Sx = shift(1, 1);
				double Sy = shift(2, 1);
				double Sz = shift(3, 1);

				CTransformationMatrix M(omega, phi, kappa, Sx, Sy, Sz, s);
				
				double rms = this->computeRMSDiff(M, obs);
				if (rms < bestRMS) //bestRMS)
				{
					bestRMS = rms;
					(*approx)(1, 1) = omega;
					(*approx)(2, 1) = phi;
					(*approx)(3, 1) = kappa;
					(*approx)(4, 1) = Sx;
					(*approx)(5, 1) = Sy;
					(*approx)(6, 1) = Sz;
					(*approx)(7, 1) = s;
				}
			}
		}
	}
		
	return true;
}

/**
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Function evaluated at the given observation unsing the given parameters
  */
bool CBestFit3DTrans::Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Fi->ReSize(3, 1);

    double w =	(*parms)(1, 1);
    double p =	(*parms)(2, 1);
    double k =	(*parms)(3, 1);

	// fix angles that have "spun"
	while (w > 2*PI)
		w -= 2*PI;
	while (w < -2*PI)
		w += 2*PI;
	while (p > 2*PI)
		p -= 2*PI;
	while (p < -2*PI)
		p += 2*PI;
	while (k > 2*PI)
		k -= 2*PI;
	while (k < -2*PI)
		k += 2*PI;

	(*parms)(1, 1) = w;
	(*parms)(2, 1) = p;
    (*parms)(3, 1) = k;

    double Sx =	(*parms)(4, 1);
    double Sy =	(*parms)(5, 1);
    double Sz =	(*parms)(6, 1);
	double s =	(*parms)(7, 1);
	
	double X = (*ob)(1, 1);
	double Y = (*ob)(1, 2);
	double Z = (*ob)(1, 3);
	double x = (*ob)(1, 4);
	double y = (*ob)(1, 5);
	double z = (*ob)(1, 6);

	double m11 = cos(p)*cos(k);
	double m12 = sin(w)*sin(p)*cos(k) + cos(w)*sin(k);
	double m13 = -cos(w)*sin(p)*cos(k) + sin(w)*sin(k);
	double m21 = -cos(p)*sin(k);
	double m22 = -sin(w)*sin(p)*sin(k) + cos(w)*cos(k);
	double m23 = cos(w)*sin(p)*sin(k) + sin(w)*cos(k);
	double m31 = sin(p);
	double m32 = -sin(w)*cos(p);
	double m33 = cos(w)*cos(p);

    (*Fi)(1, 1) = X - s*(m11*x + m12*y + m13*z) - Sx;
    (*Fi)(2, 1) = Y - s*(m21*x + m22*y + m23*z) - Sy;
    (*Fi)(3, 1) = Z - s*(m31*x + m32*y + m33*z) - Sz;

    return  true;
}

/**
 * @param parms Column Matrix of approximations of the parameters
 * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
 * @return A Matrix of partials with respect to paramters evaluated at ob
 */
bool CBestFit3DTrans::Ai(Matrix* A, Matrix* parms, Matrix* consts, Matrix* ob)
{
    A->ReSize(3, 7);

    double w =	(*parms)(1, 1);
    double p =	(*parms)(2, 1);
    double k =	(*parms)(3, 1);
    double Sx =	(*parms)(4, 1);
    double Sy =	(*parms)(5, 1);
    double Sz =	(*parms)(6, 1);
	double s =	(*parms)(7, 1);

	double X = (*ob)(1, 1);
	double Y = (*ob)(1, 2);
	double Z = (*ob)(1, 3);
	double x = (*ob)(1, 4);
	double y = (*ob)(1, 5);
	double z = (*ob)(1, 6);

	double m11 = cos(p)*cos(k);
	double m12 = sin(w)*sin(p)*cos(k) + cos(w)*sin(k);
	double m13 = -cos(w)*sin(p)*cos(k) + sin(w)*sin(k);
	double m21 = -cos(p)*sin(k);
	double m22 = -sin(w)*sin(p)*sin(k) + cos(w)*cos(k);
	double m23 = cos(w)*sin(p)*sin(k) + sin(w)*cos(k);
	double m31 = sin(p);
	double m32 = -sin(w)*cos(p);
	double m33 = cos(w)*cos(p);

	double p_f1_w = -s*( (cos(w)*sin(p)*cos(k) - sin(w)*sin(k))*y +
					     (sin(w)*sin(p)*cos(k) + cos(w)*sin(k))*z );

	double p_f1_p = -s*( -sin(p)*cos(k)*x + sin(w)*cos(p)*cos(k)*y +
					      -cos(w)*cos(p)*cos(k)*z );

	double p_f1_k = -s*( (-cos(p)*sin(k))*x + (-sin(w)*sin(p)*sin(k) + cos(w)*cos(k))*y +
					     (cos(w)*sin(p)*sin(k) + sin(w)*cos(k))*z );

	double p_f1_Sx = -1;   
	double p_f1_Sy = 0;
	double p_f1_Sz = 0;

	double p_f1_s = -(m11*x + m12*y + m13*z);

	double p_f2_w = -s*( (-cos(w)*sin(p)*sin(k) - sin(w)*cos(k))*y + 
		                 (-sin(w)*sin(p)*sin(k) + cos(w)*cos(k))*z );

	double p_f2_p = -s*( (sin(p)*sin(k))*x + (-sin(w)*cos(p)*sin(k))*y +
		                 (cos(w)*cos(p)*sin(k))*z );

	double p_f2_k = -s*( (-cos(p)*cos(k))*x + (-sin(w)*sin(p)*cos(k) - cos(w)*sin(k))*y +
		                 (cos(w)*sin(p)*cos(k)-sin(w)*sin(k))*z );

	double p_f2_Sx = 0;
	double p_f2_Sy = -1;
	double p_f2_Sz = 0;

	double p_f2_s = -(m21*x + m22*y + m23*z);

	double p_f3_w = -s*( (-cos(w)*cos(p))*y + (-sin(w)*cos(p)*z) );

	double p_f3_p = -s*( cos(p)*x + (sin(w)*sin(p))*y + (-cos(w)*sin(p))*z );

	double p_f3_k = 0;

	double p_f3_Sx = 0;
	double p_f3_Sy = 0;
	double p_f3_Sz = -1;

	double p_f3_s = -(m31*x + m32*y + m33*z);

	(*A)(1, 1) = p_f1_w; 
	(*A)(1, 2) = p_f1_p; 
	(*A)(1, 3) = p_f1_k; 
	(*A)(1, 4) = p_f1_Sx; 
	(*A)(1, 5) = p_f1_Sy; 
	(*A)(1, 6) = p_f1_Sz; 
	(*A)(1, 7) = p_f1_s;

	(*A)(2, 1) = p_f2_w; 
	(*A)(2, 2) = p_f2_p; 
	(*A)(2, 3) = p_f2_k; 
	(*A)(2, 4) = p_f2_Sx;
	(*A)(2, 5) = p_f2_Sy;
	(*A)(2, 6) = p_f2_Sz;
	(*A)(2, 7) = p_f2_s;

	(*A)(3, 1) = p_f3_w; 
	(*A)(3, 2) = p_f3_p; 
	(*A)(3, 3) = p_f3_k; 
	(*A)(3, 4) = p_f3_Sx;
	(*A)(3, 5) = p_f3_Sy;
	(*A)(3, 6) = p_f3_Sz;
	(*A)(3, 7) = p_f3_s;

	return true;
}

/**
  * @return Bi Matrix of patials with respect to observations for the ith observation
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  */
bool CBestFit3DTrans::Bi(Matrix* B, Matrix* parms, Matrix* consts, Matrix* ob)
{
    B->ReSize(3, 6);

    double w =	(*parms)(1, 1);
    double p =	(*parms)(2, 1);
    double k =	(*parms)(3, 1);
    double Sx =	(*parms)(4, 1);
    double Sy =	(*parms)(5, 1);
    double Sz =	(*parms)(6, 1);
	double s =	(*parms)(7, 1);

	double X = (*ob)(1, 1);
	double Y = (*ob)(1, 2);
	double Z = (*ob)(1, 3);
	double x = (*ob)(1, 4);
	double y = (*ob)(1, 5);
	double z = (*ob)(1, 6);

	double m11 = cos(p)*cos(k);
	double m12 = sin(w)*sin(p)*cos(k) + cos(w)*sin(k);
	double m13 = -cos(w)*sin(p)*cos(k) + sin(w)*sin(k);
	double m21 = -cos(p)*sin(k);
	double m22 = -sin(w)*sin(p)*sin(k) + cos(w)*cos(k);
	double m23 = cos(w)*sin(p)*sin(k) + sin(w)*cos(k);
	double m31 = sin(p);
	double m32 = -sin(w)*cos(p);
	double m33 = cos(w)*cos(p);

	double p_f1_x = -s*m11;
	double p_f1_y = -s*m12;
	double p_f1_z = -s*m13;
	double p_f1_X = 1;
	double p_f1_Y = 0;
	double p_f1_Z = 0;

	double p_f2_x = -s*m21;
	double p_f2_y = -s*m22;
	double p_f2_z = -s*m23;
	double p_f2_X = 0;
	double p_f2_Y = 1;
	double p_f2_Z = 0;

	double p_f3_x = -s*m31;
	double p_f3_y = -s*m32;
	double p_f3_z = -s*m33;
	double p_f3_X = 0;
	double p_f3_Y = 0;
	double p_f3_Z = 1;

	(*B)(1, 1) = p_f1_X;
	(*B)(1, 2) = p_f1_Y;
	(*B)(1, 3) = p_f1_Z;
	(*B)(1, 4) = p_f1_x;
	(*B)(1, 5) = p_f1_y;
	(*B)(1, 6) = p_f1_z;
				 
	(*B)(2, 1) = p_f2_X;
	(*B)(2, 2) = p_f2_Y;
	(*B)(2, 3) = p_f2_Z;
	(*B)(2, 4) = p_f2_x;
	(*B)(2, 5) = p_f2_y;
	(*B)(2, 6) = p_f2_z;
				 
	(*B)(3, 1) = p_f3_X;
	(*B)(3, 2) = p_f3_Y;
	(*B)(3, 3) = p_f3_Z;
	(*B)(3, 4) = p_f3_x;
	(*B)(3, 5) = p_f3_y;
	(*B)(3, 6) = p_f3_z;

    return true;
}

/**
  * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
  */
void CBestFit3DTrans::getParameterNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "a0";

	str = names->Add();
	*str = "a1";
}

/**
 * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
 */
void CBestFit3DTrans::getObservationNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "X";

	str = names->Add();
	*str = "Y";

    str = names->Add();
	*str = "Z";

    str = names->Add();
	*str = "x";

    str = names->Add();
	*str = "y";

    str = names->Add();
	*str = "z";
}

/**
 * @return a String cantainint the name of the best fit ex. "Best fit Line"
 */
void CBestFit3DTrans::getName(CCharString& name)
{
	name = "CBestFit3DTrans";
}

/**
 * Sets the a priori parameters information along with a priori
 * covariance Matrix
 */
void CBestFit3DTrans::setParameters(Matrix* parms, CBlockDiagonalMatrix* qxx)
{
	this->parms0 = new Matrix;
	*this->parms0 = (*parms);
	this->Qxx = new CBlockDiagonalMatrix(*qxx);
}

/**
  * Gets the apriori covariance Matrix (if there)
  */
CBlockDiagonalMatrix* CBestFit3DTrans::getQxx()
{
	return this->Qxx;
}

bool CBestFit3DTrans::equals(Matrix* A, Matrix* B)
{
	if (A->Ncols() != B->Ncols())
		return false;

	if (A->Nrows() != B->Nrows())
		return false;

	for (int r = 1; r <= A->Nrows(); r++)
	{
		for (int c = 1; c <= A->Ncols(); c++)
		{
			double a = (*A)(r, c);
			double b = (*B)(r, c);
			double diff = ::fabs(a - b);

			if (diff > 1e-10)
				return false;
		}
	}

	return true;
}


double CBestFit3DTrans::approximateScale(Matrix* obs)
{
	int nPoints = obs->Nrows();

	
	// first compute an average point for both sets
	Matrix primary(1, 3);
	primary = 0;
	Matrix secondary(1, 3);
	secondary = 0;
	for (int i = 1; i <= nPoints; i++)
	{
		primary += obs->SubMatrix(i, i, 1, 3);
		secondary += obs->SubMatrix(i, i, 4, 6);
	}

	primary /= (double)nPoints;
	secondary /= (double)nPoints;
	
	// compute and average scale factor
	double sBar = 0;
	int count = 0;
	for (int i = 1; i <= nPoints; i++)
	{
		Matrix pi = obs->SubMatrix(i, i, 1, 3);
		Matrix si = obs->SubMatrix(i, i, 4, 6);

		pi = pi - primary;
		si = si - secondary;

		Matrix Dp = pi*pi.t();
		double distP = sqrt(Dp(1, 1));

		Matrix Ds = si*si.t();
		double distS = sqrt(Ds(1, 1));
		
		if (distS != 0)
		{
			sBar += distP/distS;
			count++;
		}
	}

	sBar /= (double)count;

	return sBar;
}

void CBestFit3DTrans::approximateShift(CTransformationMatrix& M, Matrix* obs, Matrix& shift)
{
	shift.ReSize(4, 1);
	shift = 0;
	Matrix transed;
	Matrix diff;
	Matrix primary(4, 1);
	Matrix secondary(4, 1);

	int nPoints = obs->Nrows();
	for (int i = 1; i <= nPoints; i++)
	{
		primary(1, 1) = (*obs)(i, 1);
		primary(2, 1) = (*obs)(i, 2);
		primary(3, 1) = (*obs)(i, 3);
		primary(4, 1) = 1;

		secondary(1, 1) = (*obs)(i, 4);
		secondary(2, 1) = (*obs)(i, 5);
		secondary(3, 1) = (*obs)(i, 6);
		secondary(4, 1) = 1;

		transed = M*secondary;

		diff = primary - transed;

		shift += diff;
	}

	shift /= (double)nPoints;
}

double CBestFit3DTrans::computeRMSDiff(CTransformationMatrix& M, Matrix* obs)
{
	Matrix primary(4, 1);
	Matrix secondary(4, 1);
	Matrix transed;
	Matrix diff;
	Matrix vTv;

	double rms = 0;

	int nPoints = obs->Nrows();
	for (int i = 1; i <= nPoints; i++)
	{
		primary(1, 1) = (*obs)(i, 1);
		primary(2, 1) = (*obs)(i, 2);
		primary(3, 1) = (*obs)(i, 3);
		primary(4, 1) = 1;

		secondary(1, 1) = (*obs)(i, 4);
		secondary(2, 1) = (*obs)(i, 5);
		secondary(3, 1) = (*obs)(i, 6);
		secondary(4, 1) = 1;

		transed = M*secondary;

		diff = primary - transed;

		vTv = diff.t()*diff;

		rms += vTv(1, 1);
	}

	rms /= ((double)nPoints*3);

	rms = sqrt(rms);
	return rms;
}

bool CBestFit3DTrans::runTest(CCharStringArray& report)
{
	CCharString* str = report.Add();
	*str = "-------------------------------------------------------------------------------";
	str = report.Add();
	*str = "CBestFit3DTrans";

	Matrix obs(6, 6);

	obs << 100 << 100 << 100 << 181.921993938436 <<  93.719047667935 << 111.146051249583  
		<< 150 << 150 << 150 << 204.922490215865 << 124.692559105411 << 161.762912216796  
		<< 200 << 100 << 100 << 251.041880461726 <<  89.397655099672 << 135.729866898868  
		<< 100 << 400 << 100 << 187.257220484872 << 312.862101264043 << 134.666928479553  
		<< 150 << 350 << 150 << 208.479307913489 << 270.787928169483 << 177.443497036776  
		<< 200 << 400 << 100 << 256.377107008162 << 308.540708695779 << 159.250744128838; 		

	Matrix sigmas(6, 6);

	sigmas = 1.0;
	double Sx = -175.92920620900900;
	double Sy = -48.8892514689854;  
	double Sz = 54.0198390768230;
	double s = 1.3607524001277; 

	if (false)
	{
		// complete sequence test
		int count = 0;
		for (double omega = -PI+.1; omega < PI+.1; omega += PI/4)
		{
			for (double phi = -PI+.1; phi < PI+.1; phi += PI/4)
			{
				for (double kappa = -PI+.1; kappa < PI+.1; kappa += PI/4)
				{
					CTransformationMatrix M1(omega, phi, kappa, -175.92920620900900, -48.8892514689854, 54.0198390768230, 1.3607524001277);

					Matrix newObs(6, 6);

					Matrix primary(4, 1);
					Matrix secondary(4, 1);

					int i;
					for (i = 1; i <=6; i++)
					{
						primary(1, 1) = obs(i, 1);
						primary(2, 1) = obs(i, 2);
						primary(3, 1) = obs(i, 3);
						primary(4, 1) = 1;

						Matrix result = M1*primary;

						newObs(i, 1) = result(1, 1);
						newObs(i, 2) = result(2, 1);
						newObs(i, 3) = result(3, 1);
						newObs(i, 4) = obs(i, 1);
						newObs(i, 5) = obs(i, 2);
						newObs(i, 6) = obs(i, 3);
					}
					
					// now see if we can get the same solution
					CLeastSquaresSolver lss(this, &newObs, NULL, NULL, &sigmas);
					lss.solve();
					Matrix* solution = lss.getSolution();

					double w =  (*solution)(1, 1);
					double p =  (*solution)(2, 1);
					double k =  (*solution)(3, 1);
					double Sx = (*solution)(4, 1);
					double Sy = (*solution)(5, 1);
					double Sz = (*solution)(6, 1);
					double s =  (*solution)(7, 1);					
					CTransformationMatrix M(w, p, k, Sx, Sy, Sz, s);	

					for (i = 1; i <=6; i++)
					{
						Matrix point(4, 1);
						Matrix testpointP(4, 1);

						testpointP(1, 1) = newObs(i, 1);
						testpointP(2, 1) = newObs(i, 2);
						testpointP(3, 1) = newObs(i, 3);
						testpointP(4, 1) = 1;

						point(1, 1) = newObs(i, 4);
						point(2, 1) = newObs(i, 5);
						point(3, 1) = newObs(i, 6);
						point(4, 1) = 1;

						Matrix pointP = M*point;

						if (!CMatrixHelper::areEqual(&pointP, &testpointP,  1e-10))
						{
							str = report.Add();
							*str = "\t*** Sequence test failed";
							break;
						}
					}
					if (i == 7)
					{
						count++;
					}
				}
			}
		}

		str = report.Add();
		str->Format("\t+ Performed %d random transfomations successfully", count);
	}

	// This will be a check of an imperfect trans
	obs		<< 100 << 100 << 100 << 181.9   <<    93.7   <<  111.1  
			<< 150 << 150 << 150 << 204.9   <<   124.6   <<  161.8   
			<< 200 << 100 << 100 << 251.1   <<    89.4   <<  135.7  
			<< 100 << 400 << 100 << 187.3   <<   312.9   <<  134.7   
			<< 150 << 350 << 150 << 208.4   <<   270.8   <<  177.5   
			<< 200 << 400 << 100 << 256.4   <<   308.5   <<  159.2;

	Matrix knownSolution(7, 1);
	knownSolution	<< 0.0983507829954 
					<< -0.3455491712318 
					<< -0.0257286560481 
					<< -175.8305249248606 
					<< -48.8209346548677 
					<< 54.0452313540797 
					<< 1.3604249674752;

	Matrix knownResids(6, 6);
	knownResids	<< 0 << 0  << 0 << -0.02730344687420 << 0.02049684354170  << -0.02827376725950
				<< 0 << 0  << 0 << -0.00324985559570 << -0.06941835325760 << 0.07260368570120
				<< 0 << 0  << 0 << 0.05679635251520  << 0.05984556890550  << -0.03870916773390
				<< 0 << 0  << 0 << 0.07073141917320  << 0.03894328217980  << -0.00492878630930
				<< 0 << 0  << 0 << -0.08635521173740 << 0.02489104594200  << 0.08340648877040
				<< 0 << 0  << 0 << -0.01061925748110 << -0.07475838731150 << -0.08409845316890;

	sigmas	<< 1e-10 << 1e-10 << 1e-10 << 1 << 1 << 1
			<< 1e-10 << 1e-10 << 1e-10 << 1 << 1 << 1
			<< 1e-10 << 1e-10 << 1e-10 << 1 << 1 << 1
			<< 1e-10 << 1e-10 << 1e-10 << 1 << 1 << 1
			<< 1e-10 << 1e-10 << 1e-10 << 1 << 1 << 1
			<< 1e-10 << 1e-10 << 1e-10 << 1 << 1 << 1;

	CLeastSquaresSolver lss(this, &obs, NULL, NULL, &sigmas);
	lss.solve();
	Matrix* solution = lss.getSolution();
	Matrix* resids = lss.getResiduals();
	if (!CMatrixHelper::areEqual(solution, &knownSolution,  1e-10))
	{
		str = report.Add();
		*str = "\t*** Perturbed parameter test failed";
	}
	else
	{
		str = report.Add();
		*str = "\t+ Perturbed parameter test successful";
	}
	if (!CMatrixHelper::areEqual(resids, &knownResids,  1e-10))
	{
		str = report.Add();
		*str = "\t*** Perturbed residual test failed";
	}
	else
	{
		str = report.Add();
		*str = "\t+ Perturbed residual test successful";
	}

	// Now run a fixed scale test
	this->approximate(solution, NULL, &obs);
	(*solution)(7, 1) = 1.0;
	CBlockDiagonalMatrix Qxx(7, 7, 1);
	Matrix* qxx = Qxx.getSubMatrix(0);
	*qxx = 0;
	(*qxx)(1, 1) = 100;
	(*qxx)(2, 2) = 100;
	(*qxx)(3, 3) = 100;
	(*qxx)(4, 4) = 100;
	(*qxx)(5, 5) = 100;
	(*qxx)(6, 6) = 100;
	(*qxx)(7, 7) = 1e-99;

	this->setParameters(solution, &Qxx);
	CLeastSquaresSolver lss2(this, &obs, NULL, NULL, &sigmas);
	lss2.solve();
	solution = lss2.getSolution();

	knownSolution	<< 0.0983478321716	
					<< -0.3455341352830
					<< -0.0257264841027
					<< -89.5065619595440
					<< 30.3481295207577
					<< 70.6314707383278
					<< 1.0000000000000;

	if (CMatrixHelper::areEqual(solution, &knownSolution, 1e-10))
	{
		str = report.Add();
		*str = "\t+ Fixed-scale parameter test successful";
	}
	else
	{
		str = report.Add();
		*str = "\t*** Fixed-scale parameter test failed";
	}

	resids = lss2.getResiduals();

	knownResids << 0.00000000000000	<< 0.00000000000000 <<0.00000000000000 << 13.22641711682110	 << 39.75542146920660	<< 4.39378218329370
				<< 0.00000000000000	<< 0.00000000000000 <<0.00000000000000 << -0.00311513498300	 << 26.44231631601620	<< -8.77817892121160
				<< 0.00000000000000	<< 0.00000000000000 <<0.00000000000000 << -13.20532114857890 << 39.78411202499770	<< 4.38721213129720
				<< 0.00000000000000	<< 0.00000000000000 <<0.00000000000000 << 13.29917819596410	 << -39.71169119381370	<< 4.41164071969680
				<< 0.00000000000000	<< 0.00000000000000 <<0.00000000000000 << -0.06373659152630	 << -26.47547449819690	<< -8.76977395928010
				<< 0.00000000000000	<< 0.00000000000000 <<0.00000000000000 << -13.25417642987180 << -39.79550199345130	<< 4.35454437843060;

	if (CMatrixHelper::areEqual(resids, &knownResids, 1e-10))
	{
		str = report.Add();
		*str = "\t+ Fixed-scale residual test successful";
	}
	else
	{
		str = report.Add();
		*str = "\t*** Fixed-scale residual test failed";
	}

	return true;
}
