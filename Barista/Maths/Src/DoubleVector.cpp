// DoubleVector.cpp: implementation of the CDoubleVector class.
//
//////////////////////////////////////////////////////////////////////

// Standard includes
#include <string.h>
#include <stdio.h> 
#include <math.h> 
#include <assert.h>

// Local includes
#include "matrix.h"

#include "DoubleVector.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDoubleVector::~CDoubleVector()
{
}

/////////////////////////////////////////////////////////////////////////////////
// bool CDoubleVector::SetToZero()
//
// set all elements to zero
//
bool CDoubleVector::SetToZero()
{
	memset(m_pData, 0, m_nSize * sizeof(double));

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// bool CDoubleVector::Mult_AV(CMatrix& A, CDoubleVector& V)
//
// AT x V; result summed in calling vector
//
bool CDoubleVector::Mult_AT_V(CMatrix& A, CDoubleVector& V)
{
	// validate dimensions

	// make sure A isn't upper triangular
	if ( A.m_cols == -1 )
		return false;

	// A rows must equal size of V
	if ( A.m_rows != V.GetSize() )
		return false;

	for( int i = 0; i < A.m_cols; i++ )
		for( int j = 0; j < A.m_rows; j++ )
			m_pData[i] += A.m_ppData[j][i] * V[j];

	return true;
}

/////////////////////////////////////////////////////////////////////////////////
// bool CDoubleVector::Mult_AT_UT_V(CMatrix& A, CMatrix& UT, CDoubleVector& V)
//
// AT x V; result summed in calling vector
//
bool CDoubleVector::Mult_AT_UT_V(CMatrix& A, CMatrix& UT, CDoubleVector& V)
{
	// validate dimensions

	// make sure A isn't upper triangular
	if ( A.m_cols == -1 )
		return false;

	// A rows must equal size of V
	if ( A.m_rows != V.GetSize() )
		return false;

	if ( !UT.IsSymmetric() || A.m_rows != UT.m_rows )
		return false;

	CMatrix temp(A.m_cols,A.m_rows);
	temp.Mult_AT_UT(A,UT);

	for( int i = 0; i < A.m_cols; i++ )
		for( int j = 0; j < A.m_rows; j++ )
			m_pData[i] += temp.m_ppData[i][j] * V[j];

	return true;
}


/////////////////////////////////////////////////////////////////////////////////
// bool CDoubleVector::Mult_AT_VM(CMatrix& A, CDoubleVector &V)
//
// AT x V; result subtracted from calling vector
//
bool CDoubleVector::Mult_AT_VM(CMatrix& A, CDoubleVector &V)
{
	// validate dimensions

	// make sure A isn't upper triangular
	if ( A.m_cols == -1 )
		return false;

	// A rows must equal size of V
	if ( A.m_rows != V.GetSize() )
		return false;

	for( int i = 0; i < A.m_cols; i++ )
		for( int j = 0; j < A.m_rows; j++ )
			m_pData[i] -= A.m_ppData[j][i]*V[j];

	return true;
}     

/////////////////////////////////////////////////////////////////////////////////
// bool CDoubleVector::Mult_UT_A_V(CMatrix& A, CDoubleVector& V)
//
// multiplies a UT matrix by a vector
//
bool CDoubleVector::Mult_UT_A_V(CMatrix& A, CDoubleVector& V)
{
	// validate dimensions
	
	// A must be upper triangular
	if ( A.m_cols != -1 )
		return false;

	// A rows must equal size of V
	if ( A.m_rows != V.GetSize() )
		return false;

	// this vector must be same size as V
	if ( m_nSize != V.GetSize() )
		return false;

	for( int i = 0; i < m_nSize; i++)
	{
		for( int j = 0; j < i; j++ )
			m_pData[i] += A.m_ppData[j][i-j]*V[j];
		for( int j = i; j < m_nSize; j++ )
			m_pData[i] += A.m_ppData[i][j-i]*V[j];
	}

	return true;
}     

////////////////////////////////////////////////////////////////////////////////
// bool CDoubleVector::Mult_A_V(CMatrix& A, CDoubleVector &V)
//
// matrix multiplied by a vector (A x V), result summed in calling vector
//
bool CDoubleVector::Mult_A_V(CMatrix& A, CDoubleVector &V)
{
	// validate dimensions

	// A must not be upper triangular
	if ( A.m_cols == -1 )
		return false;

	// A columns must equal size of V
	if ( A.m_cols != V.GetSize() )
		return false;

	// this vector size must equal A rows
	if ( m_nSize != A.m_rows )
		return false;

	for( int i = 0; i < m_nSize; i++ )
		for( int j = 0; j < A.m_cols; j++ )
			m_pData[i] += A.m_ppData[i][j]*V[j];

	return true;
}     

////////////////////////////////////////////////////////////////////////////////
// bool CDoubleVector::Mult_A_VM(CMatrix& A, CDoubleVector &V)        
//
// matrix multiplied by a vector (A x V), result subtracted from calling vector
//
bool CDoubleVector::Mult_A_VM(CMatrix& A, CDoubleVector &V)
{
	// validate dimensions

	// A must not be upper triangular
	if ( A.m_cols == -1 )
		return false;

	// A columns must equal size of V
	if ( A.m_cols != V.GetSize() )
		return false;

	// this vector size must equal A rows
	if ( m_nSize != A.m_rows )
		return false;

	for( int i = 0; i < m_nSize; i++ )
		for( int j = 0; j < A.m_cols; j++ )
			m_pData[i] -= A.m_ppData[i][j]*V[j];

	return true;
}

void CDoubleVector::Dump()
{
	printf("Vector (%d)\n", m_nSize);

	for (int i = 0; i < m_nSize; i++)
		printf("%12.6lf\n", m_pData[i]);

	printf("\n");

}

CDoubleVector& CDoubleVector::operator =(CMatrix &Mat)
{
	SetSize(Mat.m_rows);

	for( int i = 0; i < Mat.m_rows; i++ )
		m_pData[i] = Mat.m_ppData[i][0];

	return *this;
}

CDoubleVector CDoubleVector::operator *(double f) const
{
	CDoubleVector temp(*this);

	temp*=f;

	return temp;
}

CDoubleVector& CDoubleVector::operator *=(double f)
{

	for( int i = 0; i < GetSize(); i++ )
		m_pData[i] = f * m_pData[i];

	return *this;
}

CDoubleVector CDoubleVector::operator -(const CDoubleVector &Vec) const
{
	CDoubleVector temp(*this);
	temp-=Vec;
	
	return temp;
}

CDoubleVector&  CDoubleVector::operator -=(const CDoubleVector &Vec)
{
	assert( m_nSize==Vec.m_nSize );

	for( int i = 0; i < GetSize(); i++ )
		m_pData[i] -= Vec.m_pData[i];

	return *this;
}

CDoubleVector CDoubleVector::operator +(const CDoubleVector &Vec) const
{
	CDoubleVector temp(*this);
	temp+=Vec;

	return temp;
}

CDoubleVector &CDoubleVector::operator +=(const CDoubleVector &Vec)
{
	assert( m_nSize==Vec.m_nSize );

	for( int i = 0; i < GetSize(); i++ )
		m_pData[i] += Vec.m_pData[i];

	return *this;
}

void CDoubleVector::CrossProduct(CDoubleVector &a, CDoubleVector &b)
{
	assert(a.GetSize()==3 && b.GetSize()==3);

	SetSize(3);
	
	m_pData[0]= a.m_pData[1]*b.m_pData[2] - a.m_pData[2]*b.m_pData[1];
	m_pData[1]=-a.m_pData[0]*b.m_pData[2] + a.m_pData[2]*b.m_pData[0];
	m_pData[2]= a.m_pData[0]*b.m_pData[1] - a.m_pData[1]*b.m_pData[0];

}

double CDoubleVector::GetLength()
{
	double l=0;

	for( int i = 0; i < GetSize(); i++ )
		l+=m_pData[i]*m_pData[i];

	return sqrt(l);
}

void CDoubleVector::Normalize()
{
	double length=GetLength();
	
	for( int i = 0; i < GetSize(); i++ )
		m_pData[i]=m_pData[i]/length;

}

double CDoubleVector::operator *(const CDoubleVector &Vec) const
{
	assert( m_nSize==Vec.m_nSize );

	double sum=0.;

	for( int i=0; i<m_nSize; i++)
		sum += m_pData[i]*Vec.m_pData[i];
	
	return sum;
}


double CDoubleVector::GetMinimum(long *pIndex/*=NULL*/)
{
	assert( m_nSize > 0 );

	double min=m_pData[0];
	long minIndex=0;

	for( int i=1; i<m_nSize; i++)
	{
		if (m_pData[i]<min)
		{
			min = m_pData[i];
			minIndex = i;
		}
	}

	if (pIndex != NULL)
		*pIndex = minIndex;

	return min;
}

double CDoubleVector::GetMaximum(long *pIndex/*=NULL*/)
{
	assert( m_nSize > 0 );

	double max=m_pData[0];
	long maxIndex=0;

	for( int i=1; i<m_nSize; i++)
	{
		if (m_pData[i]>max)
		{
			max = m_pData[i];
			maxIndex = i;
		}
	}

	if (pIndex != NULL)
		*pIndex = maxIndex;
 
	return max;
}

CDoubleVector CDoubleVector::GetDataInRange(int stIndex, int enIndex, double &sum)
{
	CDoubleVector vect;

	vect.CleanUp();
	sum  = 0;
	int sz = this->GetSize();
	if (stIndex < 0)
		stIndex = 0;
	if (enIndex > sz-1)
		enIndex = sz-1;
	for (int i = stIndex; i <= enIndex; i++)
	{
		vect.Add(this->GetAt(i));
		sum += this->GetAt(i);
	}

	return vect;
}