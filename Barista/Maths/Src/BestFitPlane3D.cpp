
#include "newmat.h"
#include "CharStringArray.h"
#include "BlockDiagonalMatrix.h"
#include "math.h"
#include "MatrixHelper.h"

#include "BestFitPlane3D.h"
CBestFitPlane3D::CBestFitPlane3D(void)
{
}

CBestFitPlane3D::~CBestFitPlane3D(void)
{
}

/**
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Column Matrix of approximations of the parameters of the least squares fit
  */
bool CBestFitPlane3D::approximate (Matrix* approx, Matrix* consts, Matrix* obs)
{
	approx->ReSize(4, 1);

	double pi = 4.0*atan(1.0);

	Matrix Norm(3,1);
	Matrix Compare(3,1);

	int count=0;
	int nRows =obs->Nrows();

	double avex = 0;
	double avey = 0;
	double avez = 0;

	for (int i = 0; i < nRows; i++)
	{
		avex+=(*obs)(i+1, 1);
		avey+=(*obs)(i+1, 2);
		avez+=(*obs)(i+1, 3);
	}

	avex /= nRows;
	avey /= nRows;
	avez /= nRows;

	for(int i=0; i < nRows; i++)
	{
		Matrix master(3,1);
		master(1,1) = (*obs)(i+1, 1) - avex;
		master(2,1) = (*obs)(i+1, 2) - avey;
		master(3,1) = (*obs)(i+1, 3) - avez;
		CMatrixHelper::normalize(master);
		for(int j=i+1; j < nRows; j++)
		{
			Matrix slave(3,1);
			slave(1,1) = (*obs)(j+1,1) - avex;
			slave(2,1) = (*obs)(j+1,2) - avey;
			slave(3,1) = (*obs)(j+1,3) - avez;

			CMatrixHelper::normalize(slave);

			Matrix tempNorm;
			CMatrixHelper::crossProductColumn(tempNorm, master, slave);
			CMatrixHelper::normalize(tempNorm);

			double a = tempNorm(1, 1);
			double b = tempNorm(2, 1);
			double c = tempNorm(3, 1);

			if(count == 0)
			{
				Compare = tempNorm;
				Norm = tempNorm;
				count++;

				continue;
			}
			else
			{
				double angle = CMatrixHelper::angleColumn(Compare, tempNorm);

				if(angle > pi/2.0)
				{
					tempNorm *= -1.0;
				}
			}

			Norm += tempNorm;
			count++;
		}
		
	}

	Norm /= count;
	CMatrixHelper::normalize(Norm);

	double a = Norm(1,1);
	double b = Norm(2,1);
	double c = Norm(3,1); 

	// c must allways be positive
	if (c < 0.0)
	{
		a = -a;
		b = -b;
		c = -c;
	}

	(*approx)(1, 1) = a;
	(*approx)(2, 1) = b;
	(*approx)(3, 1) = c;

	double d(0.0);

	// for d

	for(int i=0; i < nRows; i++)
	{
		Matrix pt(3,1);
		pt(1,1) = (*obs)(i+1, 1);
		pt(2,1) = (*obs)(i+1, 2);
		pt(3,1) = (*obs)(i+1, 3);

		d -= a*pt(1,1) + b*pt(2,1) + c*pt(3,1);
	}

	d /= nRows;

	(*approx)(4, 1) = d;
	
	return true;
}

/**
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Function evaluated at the given observation unsing the given parameters
  */
bool CBestFitPlane3D::Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Fi->ReSize(1, 1);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
	double c = (*parms)(3, 1);//sqrt(1.0-a*a-b*b);
	double d = (*parms)(4, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double z = (*ob)(1, 3);

    (*Fi)(1, 1) = a*x + b*y + c*z + d;

    return  true;
}

/**
 * @param parms Column Matrix of approximations of the parameters
 * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
 * @return A Matrix of partials with respect to paramters evaluated at ob
 */
bool CBestFitPlane3D::Ai(Matrix* Ai, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Ai->ReSize(1, 4);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
	double c = (*parms)(3, 1);//sqrt(1.0-a*a-b*b);
	double d = (*parms)(4, 1);

    double x = (*ob)(1, 1);
    double y = (*ob)(1, 2);
    double z = (*ob)(1, 3);

	double root = sqrt(1.0-a*a-b*b);
	double den = 1.0/root;

	(*Ai)(1, 1) = x;// - a/root*z;
    (*Ai)(1, 2) = y;// - b/root*z;
    (*Ai)(1, 3) = z;
    (*Ai)(1, 4) = 1;

	return true;
}

/**
  * @return Bi Matrix of patials with respect to observations for the ith observation
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  */
bool CBestFitPlane3D::Bi(Matrix* Bi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Bi->ReSize(1, 3);

    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
	double c = (*parms)(3, 1);//sqrt(1.0-a*a-b*b);
	double d = (*parms)(4, 1);

    (*Bi)(1, 1) = 1;//a;
    (*Bi)(1, 2) = 1;//b;
    (*Bi)(1, 3) = 1;//c;

    return true;
}

/**
  * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
  */
void CBestFitPlane3D::getParameterNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "a";

	str = names->Add();
	*str = "b";

	str = names->Add();
	*str = "d";
}

/**
 * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
 */
void CBestFitPlane3D::getObservationNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "x";

	str = names->Add();
	*str = "y";
	
	str = names->Add();
	*str = "z";
}

/**
 * @return a String cantainint the name of the best fit ex. "Best fit Line"
 */
void CBestFitPlane3D::getName(CCharString& name)
{
	name = "Best-Fit 3D Plane";
}

/**
 * Sets the a priori parameters information along with a priori
 * covariance Matrix
 */
void CBestFitPlane3D::setParameters(Matrix* parms, CBlockDiagonalMatrix* qxx)
{
	// first delete object (if there)
	this->deleteObjects();

	this->parms0 = new Matrix;
	*this->parms0 = *parms;
	this->Qxx = new CBlockDiagonalMatrix(*qxx);

}

/**
  * Gets the apriori covariance Matrix (if there)
  */
CBlockDiagonalMatrix* CBestFitPlane3D::getQxx()
{
	return this->Qxx;
}

/**
 *
 */
void CBestFitPlane3D::accumulate(Matrix* N, Matrix* t, Matrix* parms, Matrix* constant,
                        Matrix* ob, Matrix* previousResid, Matrix* Q)
{
	
    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
	double c = (*parms)(3, 1);//sqrt(1.0-a*a-b*b);
	double d = (*parms)(4, 1);

	double mag = sqrt(a*a+b*b+c*c);
	
	a/=mag;
	b/=mag;
	c/=mag;

mag = sqrt(a*a+b*b+c*c);
//	(*parms)(1, 1) = a;
//	(*parms)(2, 1) = b;
//	(*parms)(3, 1) = c;

	CLeastSquaresModel::accumulate(N, t, parms, constant, ob, previousResid, Q);
}

void CBestFitPlane3D::addConstraint(Matrix* N, Matrix* t, Matrix* parms)
{
    double a = (*parms)(1, 1);
    double b = (*parms)(2, 1);
	double c = (*parms)(3, 1);//sqrt(1.0-a*a-b*b);
	double d = (*parms)(4, 1);

	Matrix A(1, 4);

	Matrix w(1, 1);

	A(1, 1) = 2*a;
	A(1, 2) = 2*b;
	A(1, 3) = 2*c;
	A(1, 4) = 0;

	w(1, 1) = a*a + b*b + c*c -1;

	Matrix P(1, 1);
	P = 0;

	P(1, 1) = 1.0e12;
//	P(2, 2) = 1.0e10;
//	P(3, 3) = 1.0e10;
//	P(4, 4) = 1.0e10;

	Matrix ATA = A.t()*P*A;

	*N += ATA;

	Matrix ATw = A.t()*P*w;

	*t += ATw;

	int hmm = 0;
}