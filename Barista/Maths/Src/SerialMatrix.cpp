#include "Token.h"
#include "StructuredFileSection.h"

#include "SerialMatrix.h"

CSerialMatrix::CSerialMatrix(void)
{
}

CSerialMatrix::CSerialMatrix(CSerialMatrix* pSrcMatrix) : Matrix(*pSrcMatrix)
{
}

CSerialMatrix::CSerialMatrix(CMatrix* pOldStyle)
{
	if (pOldStyle == NULL)
		return;

	int rows = pOldStyle->m_rows;
	int cols = pOldStyle->m_cols;

	if (cols == -1)
		cols = rows;

	this->ReSize(rows, cols);

	for (int r = 0; r < this->Nrows(); r++)
	{
		for (int c = 0; c < this->Ncols(); c++)
		{
			this->element(r, c) = pOldStyle->Get(r, c);
		}
	}
}

CSerialMatrix::~CSerialMatrix(void)
{
}

void CSerialMatrix::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CToken* pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("nrows", this->Nrows());

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("ncols", this->Ncols());

	for (int r = 0; r < this->Nrows(); r++)
	{
		pNewToken = pStructuredFileSection->addToken();

		CCharString keyStr;
		keyStr.Format("row%d", r);

		CCharString valueStr;

		for (int c = 0; c < this->Ncols(); c++) 
		{
			CCharString elementStr;
			double element = this->element(r, c);
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

		pNewToken->setValue(keyStr.GetChar(), valueStr.GetChar());
	}
}

void CSerialMatrix::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	int nrows, ncols;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* pToken = pStructuredFileSection->getTokens()->GetAt(i);
		
//		CCharString key = pToken->getKey();

		if(pToken->isKey("nrows"))
			nrows = pToken->getIntValue();
		else if(pToken->isKey("ncols"))
			ncols = pToken->getIntValue();
	}

	this->ReSize(nrows, ncols);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* pToken = pStructuredFileSection->getTokens()->GetAt(i);
		
		//CCharString key = pToken->getKey();

		double* pDoubleArray = new double[ncols];

		for (int r = 0; r < this->Nrows(); r++)
		{
			CCharString keyStr;
			keyStr.Format("row%d", r);

			if(pToken->isKey(keyStr.GetChar()))
			{
				// parse 'value' into each element
				pToken->getDoubleValues(pDoubleArray, ncols);

				for (int c = 0; c < this->Ncols(); c++)
				{
					this->element(r, c) = pDoubleArray[c];
				}
			}
		}

		delete [] pDoubleArray;
	}
}



bool CSerialMatrix::isModified()
{
	return this->bModified;
}

void CSerialMatrix::reconnectPointers()
{
	CSerializable::reconnectPointers();
}

void CSerialMatrix::resetPointers()
{
	CSerializable::resetPointers();
}