
#include "newmat.h"
#include "BlockDiagonalMatrix.h"
#include "LeastSquaresSolver.h"
#include "CharStringArray.h"
#include "MatrixHelper.h"
#include "TextFile.h"
#include "TransformationMatrix.h"
#include <math.h>

#include "BestFit2DTrans.h"

CBestFit2DTrans::CBestFit2DTrans(void)
{
}

CBestFit2DTrans::~CBestFit2DTrans(void)
{
}
/**
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Column Matrix of approximations of the parameters of the least squares fit
  */
bool CBestFit2DTrans::approximate (Matrix* approx, Matrix* consts, Matrix* obs)
{
	approx->ReSize(4, 1);

	*approx = 0;
	
	return true;
}

/**
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  * @return Function evaluated at the given observation unsing the given parameters
  */
bool CBestFit2DTrans::Fi(Matrix* Fi, Matrix* parms, Matrix* consts, Matrix* ob)
{
    Fi->ReSize(2, 1);

	double a = (*parms)(1, 1);
	double b = (*parms)(2, 1);
    double c = (*parms)(3, 1);
    double d = (*parms)(4, 1);

	double X = (*ob)(1, 1);
	double Y = (*ob)(1, 2);
	double x = (*ob)(1, 3);
	double y = (*ob)(1, 4);

    (*Fi)(1, 1) = X - a*x - b*y - c;
    (*Fi)(2, 1) = Y - a*y + b*x - d;

    return  true;
}

/**
 * @param parms Column Matrix of approximations of the parameters
 * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
 * @return A Matrix of partials with respect to paramters evaluated at ob
 */
bool CBestFit2DTrans::Ai(Matrix* A, Matrix* parms, Matrix* consts, Matrix* ob)
{
    A->ReSize(2, 4);

	double a = (*parms)(1, 1);
	double b = (*parms)(2, 1);
    double c = (*parms)(3, 1);
    double d = (*parms)(4, 1);

	double X = (*ob)(1, 1);
	double Y = (*ob)(1, 2);
	double x = (*ob)(1, 3);
	double y = (*ob)(1, 4);

	double p_f1_a = -x;
	double p_f1_b = -y;
	double p_f1_c = -1;
	double p_f1_d = 0;

	double p_f2_a = -y;
	double p_f2_b = x;
	double p_f2_c = 0;
	double p_f2_d = -1;

	(*A)(1, 1) = p_f1_a;
	(*A)(1, 2) = p_f1_b;
	(*A)(1, 3) = p_f1_c;
	(*A)(1, 4) = p_f1_d;

	(*A)(2, 1) = p_f2_a; 
	(*A)(2, 2) = p_f2_b; 
	(*A)(2, 3) = p_f2_c; 
	(*A)(2, 4) = p_f2_d;

	return true;
}

/**
  * @return Bi Matrix of patials with respect to observations for the ith observation
  * @param parms Column Matrix of approximations of the parameters
  * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
  */
bool CBestFit2DTrans::Bi(Matrix* B, Matrix* parms, Matrix* consts, Matrix* ob)
{
    B->ReSize(2, 4);

	double a = (*parms)(1, 1);
	double b = (*parms)(2, 1);
    double c = (*parms)(3, 1);
    double d = (*parms)(4, 1);

	double X = (*ob)(1, 1);
	double Y = (*ob)(1, 2);
	double x = (*ob)(1, 3);
	double y = (*ob)(1, 4);

	double p_f1_X = 1;
	double p_f1_Y = 0;
	double p_f1_x = -a;
	double p_f1_y = -b;

	double p_f2_X = 0;
	double p_f2_Y = 1;
	double p_f2_x = b;
	double p_f2_y = -a;

	(*B)(1, 1) = p_f1_X;
	(*B)(1, 2) = p_f1_Y;
	(*B)(1, 3) = p_f1_x;
	(*B)(1, 4) = p_f1_y;
				 
	(*B)(2, 1) = p_f2_X;
	(*B)(2, 2) = p_f2_Y;
	(*B)(2, 3) = p_f2_x;
	(*B)(2, 4) = p_f2_y;
				 
    return true;
}

/**
  * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
  */
void CBestFit2DTrans::getParameterNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "a";

	str = names->Add();
	*str = "b";

	str = names->Add();
	*str = "c";

	str = names->Add();
	*str = "d";
}

/**
 * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
 */
void CBestFit2DTrans::getObservationNames(CCharStringArray* names)
{
	names->RemoveAll();

	CCharString* str = names->Add();
	*str = "X";

	str = names->Add();
	*str = "Y";

	str = names->Add();
	*str = "x";

	str = names->Add();
	*str = "y";
}

/**
 * @return a String cantainint the name of the best fit ex. "Best fit Line"
 */
void CBestFit2DTrans::getName(CCharString& name)
{
	name = "2D conf trans";
}

/**
 * Sets the a priori parameters information along with a priori
 * covariance Matrix
 */
void CBestFit2DTrans::setParameters(Matrix* parms, CBlockDiagonalMatrix* qxx)
{
	this->parms0 = new Matrix;
	*this->parms0 = (*parms);
	this->Qxx = new CBlockDiagonalMatrix(*qxx);
}

/**
  * Gets the apriori covariance Matrix (if there)
  */
CBlockDiagonalMatrix* CBestFit2DTrans::getQxx()
{
	return this->Qxx;
}



bool CBestFit2DTrans::runTest(CCharStringArray& report)
{
	CCharString* str = report.Add();
	*str = "-------------------------------------------------------------------------------";
	str = report.Add();
	*str = "CBestFit2DTrans";

	return true;
}
