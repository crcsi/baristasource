// Sphere3D.cpp: implementation of the CSphere3D class.
//
//////////////////////////////////////////////////////////////////////

#include "math.h"

#include "Sphere3D.h"
#include "newmat.h"
#include "Triangle3DArray.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSphere3D::CSphere3D(double x, double y, double z, double r)
{
	this->origin.ReSize(3,1);

	this->origin.Row(1) << x;
	this->origin.Row(2) << y;
	this->origin.Row(3) << z;

	this->radius = r;
}

CSphere3D::CSphere3D()
{
	this->origin.ReSize(3,1);
}

CSphere3D::~CSphere3D()
{

}

Matrix* CSphere3D::getOrigin()
{
	return &this->origin;
}

double CSphere3D::getRadius()
{
	return this->radius;
}

///////////////////////////////////////////////////////////////////////////////
// Matrix XYZatDegrees(double Azimuth, double Elevation)
//
// return the XYZ (as a 3x1 matrix) of the point at the given polar angles 
//                                                               (in degrees)
//
//
Matrix CSphere3D::XYZatDegrees(double Azimuth, double Elevation)
{
	double PI = atan(1.0) * 4.0;

	double Az = Azimuth * PI / 180;
	double El = Elevation * PI / 180;

	Matrix XYZ(3, 1);
	XYZ.Row(1) << this->radius * (cos(Az) * cos(El));
	XYZ.Row(2) << this->radius * (sin(Az) * cos(El));
	XYZ.Row(3) << this->radius * (sin(El));

	XYZ += this->origin;

	return XYZ;
}

vector<Matrix *>  CSphere3D::tesselate(int levels)
{
	// first establish initial 6 triangles

	// 0, 0, +radius
	// 0, 0, -radius
	// 0, -radius, 0
	// 0, +radius, 0
	// +radius, 0, 0
	// -radius, 0, 0

	CTriangle3DArray Tess;


	// 1
	CTriangle3D* pTri = Tess.Add();
	*(pTri->getPoint1()) = this->XYZatDegrees(-90, 0);
	*(pTri->getPoint2()) = this->XYZatDegrees(0, 90);
	*(pTri->getPoint3()) = this->XYZatDegrees(0, 0);

	// 2
	pTri = Tess.Add();
	*(pTri->getPoint1()) = this->XYZatDegrees(180, 0);
	*(pTri->getPoint2()) = this->XYZatDegrees(0, 90);
	*(pTri->getPoint3()) = this->XYZatDegrees(-90, 0);

	// 3
	pTri = Tess.Add();
	*(pTri->getPoint1()) = this->XYZatDegrees(180, 0);
	*(pTri->getPoint2()) = this->XYZatDegrees(0, 90);
	*(pTri->getPoint3()) = this->XYZatDegrees(90, 0);

	// 4
	pTri = Tess.Add();
	*(pTri->getPoint1()) = this->XYZatDegrees(90, 0);
	*(pTri->getPoint2()) = this->XYZatDegrees(0, 90);
	*(pTri->getPoint3()) = this->XYZatDegrees(0, 0);

	// 5
	pTri = Tess.Add();
	*(pTri->getPoint1()) = this->XYZatDegrees(-90, 0);
	*(pTri->getPoint2()) = this->XYZatDegrees(0, -90);
	*(pTri->getPoint3()) = this->XYZatDegrees(0, 0);

	// 6
	pTri = Tess.Add();
	*(pTri->getPoint1()) = this->XYZatDegrees(180, 0);
	*(pTri->getPoint2()) = this->XYZatDegrees(0, -90);
	*(pTri->getPoint3()) = this->XYZatDegrees(-90, 0);

	// 7
	pTri = Tess.Add();
	*(pTri->getPoint1()) = this->XYZatDegrees(180, 0);
	*(pTri->getPoint2()) = this->XYZatDegrees(0, -90);
	*(pTri->getPoint3()) = this->XYZatDegrees(90, 0);

	// 8
	pTri = Tess.Add();
	*(pTri->getPoint1()) = this->XYZatDegrees(90, 0);
	*(pTri->getPoint2()) = this->XYZatDegrees(0, -90);
	*(pTri->getPoint3()) = this->XYZatDegrees(0, 0);

	for (int i = 0; i < levels; i++)
	{
		CTriangle3DArray newTess;
		for (int iTri = 0; iTri < Tess.GetSize(); iTri++)
		{
			CTriangle3D* pTri = Tess.GetAt(iTri);
			
			CTriangle3DArray tmp = pTri->tesselate();

			for (int j = 0; j < tmp.GetSize(); j++)
			{
				CTriangle3D* pTmp = tmp.GetAt(j);

				newTess.Add(*pTmp);
			}
		}

		Tess.RemoveAll();

		for (int j = 0; j < newTess.GetSize(); j++)
		{
			CTriangle3D* pTri = newTess.GetAt(j);
			this->fixToSphere(pTri);
			Tess.Add(*pTri);
		}

	}

	// filter points

	// return 3d Point array instead of triangles
	vector<Matrix *>  points;

	int nTriangles = Tess.GetSize();
	int counter = 0;

	for (int j = 0; j < nTriangles; j++)
	{
		
		CTriangle3D* pTri = Tess.GetAt(j);
		
		Matrix point1  = *(pTri->getPoint1());
		Matrix point2  = *(pTri->getPoint2());
		Matrix point3  = *(pTri->getPoint3());
		
		bool found = false;
		{for( unsigned int k = 0; k < points.size(); k++)
		{
			Matrix* pTest = points[k];
			
			if( (*pTest)(1,1) == point1(1,1) &&
				(*pTest)(2,1) == point1(2,1) &&
				(*pTest)(3,1) == point1(3,1) )
			{
				found = true;
				break;
			}
		}}

		if(!found)
			points.push_back(new Matrix(point1));

		found = false;
		{for(unsigned int k = 0; k < points.size(); k++)
		{
			Matrix* pTest = points[k];
			
			if( (*pTest)(1,1) == point2(1,1) &&
				(*pTest)(2,1) == point2(2,1) &&
				(*pTest)(3,1) == point2(3,1) )
			{
				found = true;
				break;
			}
		}}

		if(!found)
			points.push_back(new Matrix(point2));

		found = false;
		{for(unsigned int k = 0; k < points.size(); k++)
		{
			Matrix* pTest = points[k];
			
			if( (*pTest)(1,1) == point3(1,1) &&
				(*pTest)(2,1) == point3(2,1) &&
				(*pTest)(3,1) == point3(3,1) )
			{
				found = true;
				break;
			}
		}}

		if(!found)
			points.push_back(new Matrix(point3));

		counter++;
	}

	int bricktop = 0;

	return points;
}

void CSphere3D::fixToSphere(CTriangle3D* pTri)
{
	Matrix ijk1(3, 1);
	Matrix ijk2(3, 1);
	Matrix ijk3(3, 1);

	
	ijk1 = *(pTri->getPoint1()) - this->origin;
	double mag = sqrt( ijk1(1, 1)*ijk1(1, 1) +
					   ijk1(2, 1)*ijk1(2, 1) +
					   ijk1(3, 1)*ijk1(3, 1) );

	ijk1 /= mag;
	ijk1 *= this->radius;
	*(pTri->getPoint1()) = ijk1;

	ijk2 = *(pTri->getPoint2()) - this->origin;
	mag = sqrt( ijk2(1, 1)*ijk2(1, 1) +
	   		    ijk2(2, 1)*ijk2(2, 1) +
				ijk2(3, 1)*ijk2(3, 1) );

	ijk2 /= mag;
	ijk2 *= this->radius;
	*(pTri->getPoint2()) = ijk2;

	ijk3 = *(pTri->getPoint3()) - this->origin;
	mag = sqrt( ijk3(1, 1)*ijk3(1, 1) +
	   		    ijk3(2, 1)*ijk3(2, 1) +
				ijk3(3, 1)*ijk3(3, 1) );

	ijk3 /= mag;
	ijk3 *= this->radius;
	*(pTri->getPoint3()) = ijk3;


}