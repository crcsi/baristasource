#include "XYPositionVector.h"
#include "math.h"

#ifndef PI
#define PI atan(1.0) * 4.0
#endif

CXYPositionVector::CXYPositionVector(C2DPoint* pStart, C2DPoint* pEnd)
{
	this->start.x = pStart->x;
	this->start.y = pStart->y;

	this->end.x = pEnd->x;
	this->end.y = pEnd->y;

	double dx = this->end.x - this->start.x;
	double dy = this->end.y - this->start.y;

    this->theta = atan2(dy, dx);
    this->length = sqrt((dx*dx) + (dy*dy));
}

CXYPositionVector::CXYPositionVector(C2DPoint* pStart, double theta, double length)
{
	this->start.x = pStart->x;
	this->start.y = pStart->y;

	this->theta = theta;
	this->length = length;
}

CXYPositionVector::CXYPositionVector()
{
	this->theta = 0;
	this->length = 0;
}

CXYPositionVector::~CXYPositionVector()
{
}

double CXYPositionVector::getTheta()
{
	return this->theta;
}

double CXYPositionVector::getLength()
{
	return this->length;
}

void CXYPositionVector::setLength(double length)
{
	this->length = length;
}

C2DPoint* CXYPositionVector::getStart()
{
	return &this->start;
}

void CXYPositionVector::setStart(C2DPoint* pStart)
{
	this->start.x = pStart->x;
	this->start.y = pStart->y;
}

void CXYPositionVector::setTheta(double theta)
{
    this->theta = theta;

    while (this->theta > PI)
        this->theta -= (2*PI);

    while (this->theta < -PI)
        this->theta += (2*PI);
}

void CXYPositionVector::plus(double angle)
{
    this->setTheta(this->theta + angle);
}

void CXYPositionVector::minus(double angle)
{
    this->setTheta(this->theta - angle);
}

void CXYPositionVector::getNormal(CXYPositionVector* pOutput, C2DPoint* pPoint, double length)
{
	CXYPositionVector left(pPoint, this->theta, length);
    CXYPositionVector right(pPoint, this->theta, length);
	left.plus(PI/2.0);
	right.minus(PI/2.0);

    C2DPoint* pStart = right.getEnd();
    C2DPoint* pEnd = left.getEnd();

	CXYPositionVector normal(pStart, pEnd);
	pOutput->setStart(normal.getStart());
	pOutput->setTheta(normal.getTheta());
	pOutput->setLength(normal.getLength());
}