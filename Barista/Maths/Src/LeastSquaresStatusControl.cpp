/**
 * Title:        
 * Description:
 * Copyright:    Copyright (c) Harry Hanley
 * Company:      Photometrix
 * @author Harry Hanley
 * @version 1.0
 */

#include "LeastSquaresStatusControl.h"

CLeastSquaresStatusControl::CLeastSquaresStatusControl()
{
    this->status_iter = 0;
    this->status_rejectionCount = 0;
    this->status_dof = 0;
    this->status_rms = 0;
    this->status_done = false;
    this->status_aborted = false;
    this->status_converged = false;
    this->status_maxItersReached = false;
    this->status_criticalError = false;
    this->control_do_reject = false; 
    this->control_rejectionMultiplier = 3.0; 
    this->control_rejectionAdd = 0.0; 
    this->control_maxIterations = 100; 
    this->control_abort = false; 
    this->control_rejection_limit = 100.0; 
}

void CLeastSquaresStatusControl::setDoReject(bool b)
{
    this->control_do_reject = b;
}

bool CLeastSquaresStatusControl::getDoReject()
{
    return this->control_do_reject;
}

double CLeastSquaresStatusControl::getRejectionLimit()
{
    return this->control_rejection_limit;
}

void CLeastSquaresStatusControl::setRejectionLimit(double d)
{
    this->control_rejection_limit = d;
}

void CLeastSquaresStatusControl::setRejectionMultiplier(double d)
{
    this->control_rejectionMultiplier = d;
}

double CLeastSquaresStatusControl::getRejectionMultiplier()
{
    return this->control_rejectionMultiplier;
}

void CLeastSquaresStatusControl::setRejectionAdd(double d)
{
    this->control_rejectionAdd = d;
}

double CLeastSquaresStatusControl::getRejectionAdd()
{
    return this->control_rejectionAdd;
}

int CLeastSquaresStatusControl::getMaxIters()
{
    return this->control_maxIterations;
}

void CLeastSquaresStatusControl::setMaxIters(int i)
{
    this->control_maxIterations = i;
}

void CLeastSquaresStatusControl::setDOF(int dof)
{
	this->status_dof = dof;
}

int CLeastSquaresStatusControl::getDOF()
{
	return this->status_dof;
}

void CLeastSquaresStatusControl::setIter(int iter)
{
	this->status_iter = iter;
}

int CLeastSquaresStatusControl::getIter()
{
	return this->status_iter;
}

void CLeastSquaresStatusControl::setAbort(bool b)
{
	this->control_abort = b;
}

bool CLeastSquaresStatusControl::getAbort()
{
	return this->control_abort;
}

void CLeastSquaresStatusControl::setStatusAborted()
{
	this->status_aborted = true;
}

void CLeastSquaresStatusControl::setStatusDone()
{
	this->status_done = true;
}

void CLeastSquaresStatusControl::setRMS(double d)
{
	this->status_rms = d;
}

double CLeastSquaresStatusControl::getRMS()
{
	return this->status_rms;
}

void CLeastSquaresStatusControl::setStatusConverged()
{
	this->status_converged = true;
}

void CLeastSquaresStatusControl::setRejectionCount(int n)
{
	this->status_rejectionCount = n;
}

int CLeastSquaresStatusControl::getRejectionCount()
{
	return this->status_rejectionCount;
}

void CLeastSquaresStatusControl::setStatusMaxIterations()
{
	this->status_maxItersReached = true;
}
