#ifdef WIN32
#include <float.h> 

#include <QTableWidgetItem>
#include <QMessageBox> 
#include <QHeaderView>
#include <QFileDialog>

#include "Bui_BuildingReconstructionDialog.h"

#include "ProtocolHandler.h"
#include "SystemUtilities.h"
#include "BuildingReconstructor.h"
#include "BaristaProject.h"
#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "Bui_FeatureExtractionDialog.h"
#include "ProgressThread.h"
#include "BaristaProjectHelper.h"
#include "Bui_ProgressDlg.h"

#include "Bui_OutputModeHandler.h"

//=============================================================================

bui_BuildingReconstructionDialog::bui_BuildingReconstructionDialog (const vector<CBuilding *> &buildingList) : 
				ALSdata(0), ImageRes(-1.0), ALSRes(-1.0), SearchSize(5.0),
				sigmaZ(0.1), alpha(0.01), neighbours(5), obliqueness(75.0 / 180.0 * M_PI),
				reconstructor (0), success(false), BuildingList(buildingList),
				minBuildingHeight(1.0), pOutPutHandler(0)
{
	setupUi(this);

	connect(this->buttonOk,              SIGNAL(released()),               this, SLOT(OK()));
	connect(this->buttonCancel,          SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->HelpButton,            SIGNAL(released()),               this, SLOT(Help()));
	connect(this->ImageList,             SIGNAL(itemSelectionChanged()),   this, SLOT(onImageListChanged()));
	connect(this->ALSCombo,              SIGNAL(currentIndexChanged(int)), this, SLOT(onALSChanged()));
	connect(this->FeatureExtractionPars, SIGNAL(released()),               this, SLOT(onFeatureExtractionParsSelected()));
	connect(this->ImageResolution,       SIGNAL(editingFinished()),        this, SLOT(onImageResChanged()));
	connect(this->ALSResolution,         SIGNAL(editingFinished()),        this, SLOT(onALSResChanged()));
	connect(this->BrowseButton,          SIGNAL(released()),               this, SLOT(onBrowseSelected()));
	connect(this->BufferSize,            SIGNAL(editingFinished()),        this, SLOT(onBufferSizeChanged()));
	connect(this->AlphaEdit,             SIGNAL(editingFinished()),        this, SLOT(onAlphaChanged()));
	connect(this->neighboursEdit,        SIGNAL(editingFinished()),        this, SLOT(onNeighboursChanged()));
	connect(this->maxAngleEdit,          SIGNAL(editingFinished()),        this, SLOT(onObliquenessChanged()));
	connect(this->SigmaZALSEdit,         SIGNAL(editingFinished()),        this, SLOT(onSigmaALSChanged()));
	connect(this->MinHeightEdit,         SIGNAL(editingFinished()),        this, SLOT(onBuildingHeightChanged()));
 	connect(this->OutputLevelMin,        SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMed,        SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMax,        SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}

//=============================================================================

bui_BuildingReconstructionDialog::~bui_BuildingReconstructionDialog ()
{
	this->destroy();
}

//=============================================================================

void bui_BuildingReconstructionDialog::OutputLevelChanged()
{
	this->pOutPutHandler->reactOnButtonSelected();
}

//=============================================================================

void bui_BuildingReconstructionDialog::init()
{
	this->success = false;

	this->AvailableImageList.clear();

	for (int i = 0; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		if (img->getCurrentSensorModel())
		{
			this->AvailableImageList.push_back(img);
		}
	}

	this->setupImageSelector();

	this->ALSdata = 0;

	this->setupALS();
	CFileName parameterFile("BldRecPars.ini");
	parameterFile.SetPath(project.getFileName()->GetPath());
	this->initFromFile(parameterFile);

	CFileName featureFile("BldFEX.ini");
	featureFile.SetPath(project.getFileName()->GetPath());

	if (CSystemUtilities::fileExists(featureFile))
	{
		this->featureParameters.initParameters(featureFile);
	}
	else
	{
		this->featureParameters.extractPoints = false;
		this->featureParameters.Mode = eWaterShed;
		this->featureParameters.setDerivativeFilter(eDiffXDeriche, 3.0);
		this->featureParameters.writeParameters(featureFile);
	}

	this->outputPath = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "Reconstruct" + CSystemUtilities::dirDelimStr;

	this->ImageResolution->blockSignals(true);
	this->ALSResolution->blockSignals(true);
	this->BufferSize->blockSignals(true);
	this->OutputDirectory->blockSignals(true);
	this->OutputDirectory->setEnabled(false);
	this->neighboursEdit->blockSignals(true);
	this->AlphaEdit->blockSignals(true);
	this->maxAngleEdit->blockSignals(true);
	this->SigmaZALSEdit->blockSignals(true);

	QString ds=QString("%1").arg(this->ImageRes, 0, 'f', 3);
	this->ImageResolution->setText(ds);

	ds=QString("%1").arg(this->ALSRes, 0, 'f', 3);
	this->ALSResolution->setText(ds);

	ds=QString("%1").arg(this->SearchSize, 0, 'f', 2);
	this->BufferSize->setText(ds);
	ds=QString("%1").arg(this->alpha * 100.0, 0, 'f', 2);
	this->AlphaEdit->setText(ds);
	ds=QString("%1").arg(this->obliqueness / M_PI * 180, 0, 'f', 2);
	this->maxAngleEdit->setText(ds);
	ds=QString("%1").arg(this->sigmaZ, 0, 'f', 3);
	this->SigmaZALSEdit->setText(ds);
	ds=QString("%1").arg(this->minBuildingHeight, 0, 'f', 2);
	this->MinHeightEdit->setText(ds);
	ds=QString("%1").arg(this->neighbours);
	this->neighboursEdit->setText(ds);
	this->OutputDirectory->setText(this->outputPath.GetChar());

	this->ImageResolution->blockSignals(false);
	this->ALSResolution->blockSignals(false);
	this->BufferSize->blockSignals(false);
	this->neighboursEdit->blockSignals(false);
	this->AlphaEdit->blockSignals(false);
	this->maxAngleEdit->blockSignals(false);
	this->SigmaZALSEdit->blockSignals(false);
	this->OutputDirectory->blockSignals(false);

	this->buttonOk->setFocusPolicy(Qt::NoFocus);	
	this->buttonCancel->setFocusPolicy(Qt::NoFocus);

	if (this->pOutPutHandler) delete this->pOutPutHandler;
	this->pOutPutHandler = new bui_OutputModeHandler(this->OutputLevelMin, this->OutputLevelMed,
							                         this->OutputLevelMax, 0);

		
	this->checkSetup();
}
	  

//=============================================================================

void bui_BuildingReconstructionDialog::saveToFile(const CCharString &filename)
{
	ofstream parFile(filename.GetChar());
	if (parFile.good())
	{
		parFile.setf(ios::fixed, ios::floatfield);
		parFile.precision(5);

		parFile << "ImageResolution("      << this->ImageRes     
			    << ")\nALSResolution("     << this->ALSRes
				<< ")\nSearchSize("        << this->SearchSize  
				<< " )\nSigma("            << this->sigmaZ
				<< ")\nNeighbourhood("     << this->neighbours   
				<< " )\nAlpha("            << this->alpha            
				<< ")\nMinBuildingHeight(" << this->minBuildingHeight
				<< ")\nObliqueness("       << this->obliqueness * 180.0 / M_PI       
				<< ")\nOutputPath("        << this->outputPath     << ")\n";
	}
};

//=============================================================================

void bui_BuildingReconstructionDialog::initFromFile(const CCharString &filename)
{
	ifstream inFile(filename.GetChar());

	if (inFile.good())
	{
		int len = 2048;
		char *z = new char[len];
		inFile.getline(z, len);

		while (inFile.good())
		{
			CCharString arg(z);
			int pos1 = arg.Find("(");
			int pos2 = arg.Find(")");
			if ((pos1 > 0) && (pos2 > pos1))
			{
				CCharString key = arg.Left(pos1);
				arg = arg.Left(pos2);
				arg = arg.Right(arg.GetLength() - pos1 - 1);
				if (key == "ImageResolution")
				{
					istrstream iss(arg.GetChar());
					iss >> this->ImageRes;
				}
				else if (key == "ALSResolution") 
				{
					istrstream iss(arg.GetChar());
					iss >> this->ALSRes;
				}
				else if (key == "SearchSize")
				{
					istrstream iss(arg.GetChar());
					iss >> this->SearchSize;
				}
				else if (key == "Sigma")
				{
					istrstream iss(arg.GetChar());
					iss >> this->sigmaZ;
				}
				else if (key == "Neighbourhood")
				{
					istrstream iss(arg.GetChar());
					iss >> this->neighbours;
				}
				else if (key == "Alpha")
				{
					istrstream iss(arg.GetChar());
					iss >> this->alpha;
				}
				else if (key == "Obliqueness")
				{
					istrstream iss(arg.GetChar());
					iss >> this->obliqueness;
					this->obliqueness = this->obliqueness / 180.0 * M_PI;
				}
				else if (key == "MinBuildingHeight")
				{
					istrstream iss(arg.GetChar());
					iss >> this->minBuildingHeight;
				}
				else if (key == "OutputPath")
				{
					this->outputPath = arg;
				}
			}
			inFile.getline(z, len);
		}

		delete [] z;
	}
};

//=============================================================================

void bui_BuildingReconstructionDialog::destroy()
{
	if (this->pOutPutHandler) delete this->pOutPutHandler;
	this->pOutPutHandler = 0;

	if (this->reconstructor) delete this->reconstructor;
	this->reconstructor = 0;

	this->AvailableImageList.clear();
	this->SelectedImageList.clear();

	this->ALSdata = 0;
}
	 
//=============================================================================
 	 
void bui_BuildingReconstructionDialog::setupALS()
{
	int selectedIndex = 0;

	if (project.getNumberOfAlsDataSets())
	{
		this->ALSdata = project.getALSAt(0);
		selectedIndex = 1;
	}
		
	this->ALSCombo->blockSignals(true);
	this->ALSCombo->clear();
	this->ALSCombo->addItem("None");

	for (int i = 0; i < project.getNumberOfAlsDataSets(); ++i)
	{
		CVALS *als = project.getALSAt(i);
		this->ALSCombo->addItem(als->getFileNameChar());
	}

	this->ALSCombo->setCurrentIndex(selectedIndex);		

	this->ALSCombo->blockSignals(false);
	this->checkSetup();
};

//=============================================================================

void bui_BuildingReconstructionDialog::setupImageSelector()
{
	this->SelectedImageList.clear();
	this->ImageList->blockSignals(true);
	this->ImageList->setColumnCount(1);
	this->ImageList->setRowCount(this->AvailableImageList.size());
	this->ImageList->setHorizontalHeaderItem(0,new QTableWidgetItem("Select images"));
	this->ImageList->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

	for (unsigned int i = 0; i < this->AvailableImageList.size(); ++i)
	{
		QTableWidgetItem *it = new QTableWidgetItem(AvailableImageList[i]->getFileNameChar());
		//it->setData(Qt::EditRole,i);
		this->ImageList->setItem(i,0,it);
		this->SelectedImageList.push_back(AvailableImageList[i]);
	}

	this->ImageList->selectAll();

	this->ImageList->blockSignals(false);

	this->checkSetup();
};

//=============================================================================

void bui_BuildingReconstructionDialog::onImageListChanged()
{
	this->SelectedImageList.clear();
	this->ImageList->blockSignals(true);

	QList<QTableWidgetItem *> list = this->ImageList->selectedItems();
	for (int i = 0; i < list.count(); i++)
	{
		unsigned int index = list[i]->row();
		if ( index < this->AvailableImageList.size())
		{
			this->SelectedImageList.push_back(this->AvailableImageList[index]);
		}
	}

	this->ImageList->blockSignals(false);
	this->checkSetup();
}

//=============================================================================

void bui_BuildingReconstructionDialog::onALSChanged()
{
	this->ALSCombo->blockSignals(true);

	int indexSelected = (this->ALSCombo->currentIndex()) - 1;

	if ((indexSelected < 0) || (indexSelected >= project.getNumberOfAlsDataSets())) 
	{
		this->ALSdata = 0;
		this->ALSCombo->setCurrentIndex(0);	
		this->ALSResolution->setEnabled(false);
		this->roughnessGroup->setEnabled(false);
	}
	else
	{
		this->ALSdata = project.getALSAt(indexSelected);
		this->ALSResolution->setEnabled(true);
		this->roughnessGroup->setEnabled(true);
	}

	this->ALSCombo->blockSignals(false);
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingReconstructionDialog::onBrowseSelected()
{
	QString dirQ = QFileDialog::getExistingDirectory (this, "Choose Output Directory",
		                                              this->outputPath.GetChar());

	CFileName direc = ((const char*)dirQ.toLatin1());
		
	if (!direc.IsEmpty())
	{
		this->outputPath = direc;
		if (this->outputPath[this->outputPath.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->outputPath += CSystemUtilities::dirDelimStr;
	}

	this->OutputDirectory->setText(this->outputPath.GetChar());
}

//=============================================================================

void bui_BuildingReconstructionDialog::onBufferSizeChanged()
{
	this->BufferSize->blockSignals(true);

	bool OK;
	double hlp = this->BufferSize->text().toDouble(&OK);
	if (OK && (hlp > FLT_EPSILON)) this->SearchSize = hlp;
	
	QString ds=QString("%1").arg(this->SearchSize, 0, 'f', 2);
	this->BufferSize->setText(ds);
	this->BufferSize->blockSignals(false);

	checkSetup();
};
	  
//=============================================================================

void bui_BuildingReconstructionDialog::onNeighboursChanged()
{
	this->neighboursEdit->blockSignals(true);

	bool OK;
	int hlp = this->neighboursEdit->text().toInt(&OK);
	if (OK && (hlp > 3)) this->neighbours = hlp;
	
	QString ds=QString("%1").arg(this->neighbours);
	this->neighboursEdit->setText(ds);
	this->neighboursEdit->blockSignals(false);

	checkSetup();
};

//=============================================================================

void bui_BuildingReconstructionDialog::onObliquenessChanged()
{
	this->maxAngleEdit->blockSignals(true);

	bool OK;
	double hlp = this->maxAngleEdit->text().toDouble(&OK);
	if (OK && (hlp >= 0.0) && (hlp <= 90.0)) 
		this->obliqueness = hlp * M_PI / 180.0;
	
	QString ds=QString("%1").arg(this->obliqueness / M_PI * 180, 0, 'f', 2);
	this->maxAngleEdit->setText(ds);
	this->maxAngleEdit->blockSignals(false);

	checkSetup();
};

//=============================================================================

void bui_BuildingReconstructionDialog::onAlphaChanged()
{
	this->AlphaEdit->blockSignals(true);

	bool OK;
	double hlp = this->AlphaEdit->text().toDouble(&OK);
	if (OK && (hlp >= 0.01) && (hlp <= 99.99)) 
		this->alpha = hlp * 0.01;
	
	QString ds=QString("%1").arg(this->alpha * 100.0, 0, 'f', 2);
	this->AlphaEdit->setText(ds);
	this->AlphaEdit->blockSignals(false);

	checkSetup();
};

//=============================================================================

void bui_BuildingReconstructionDialog::onSigmaALSChanged()
{
	this->SigmaZALSEdit->blockSignals(true);

	bool OK;
	double hlp = this->SigmaZALSEdit->text().toDouble(&OK);
	if (OK && (hlp >= 0.001)) this->sigmaZ = hlp;
	
	QString ds=QString("%1").arg(this->sigmaZ, 0, 'f', 3);
	this->SigmaZALSEdit->setText(ds);
	this->SigmaZALSEdit->blockSignals(false);

	checkSetup();
};

//=============================================================================

void bui_BuildingReconstructionDialog::onBuildingHeightChanged()
{
	this->MinHeightEdit->blockSignals(true);

	bool OK;
	double hlp = this->MinHeightEdit->text().toDouble(&OK);
	if (OK && (hlp >= 0.01)) this->minBuildingHeight = hlp;
	
	QString ds=QString("%1").arg(this->minBuildingHeight, 0, 'f', 2);
	this->MinHeightEdit->setText(ds);
	this->MinHeightEdit->blockSignals(false);

	checkSetup();
};

//=============================================================================

void bui_BuildingReconstructionDialog::onImageResChanged()
{
	this->ImageResolution->blockSignals(true);

	bool OK;
	double hlp = this->ImageResolution->text().toDouble(&OK);
	if (OK) this->ImageRes = hlp;
	if (this->ImageRes < FLT_EPSILON) this->ImageRes = -1; 
	
	QString ds=QString("%1").arg(this->ImageRes, 0, 'f', 3);
	this->ImageResolution->setText(ds);
	this->ImageResolution->blockSignals(false);

	checkSetup();
};

//=============================================================================

void bui_BuildingReconstructionDialog::onALSResChanged()
{
	this->ALSResolution->blockSignals(true);
	bool OK;
	double hlp = this->ALSResolution->text().toDouble(&OK);
	if (OK) this->ALSRes = hlp;
	if (this->ALSRes < FLT_EPSILON) this->ALSRes = -1; 

	QString ds=QString("%1").arg(this->ALSRes, 0, 'f', 3);
	this->ALSResolution->setText(ds);
	this->ALSResolution->blockSignals(false);

	checkSetup();
};

//=============================================================================

void bui_BuildingReconstructionDialog::onFeatureExtractionParsSelected()
{
	CFileName featureFile("BldFEX.ini");
	featureFile.SetPath(project.getFileName()->GetPath());

	bui_FeatureExtractionDialog featureExtractor(this, this->featureParameters, false, true, true, featureFile);
	featureExtractor.exec();

	if (featureExtractor.parametersAccepted())
	{
		this->featureParameters = featureExtractor.getParameters();
		this->featureParameters.writeParameters(featureFile);
	}
};


//=============================================================================

void bui_BuildingReconstructionDialog::checkSetup()
{
	if (((this->SelectedImageList.size() > 1) || (this->ALSdata)) && (BuildingList.size() > 0))
	{
		this->buttonOk->setEnabled(true);
	}
	else
	{
		this->buttonOk->setEnabled(false);
	}
}


//=============================================================================

void bui_BuildingReconstructionDialog::OK()
{
	CCharString defDir = protHandler.getDefaultDirectory();
	CFileName parameterFile("BldRecPars.ini");
	parameterFile.SetPath(project.getFileName()->GetPath());
	this->saveToFile(parameterFile);

	this->buttonOk->setEnabled(false);
	this->buttonCancel->setEnabled(false);
	this->update();

	CALSDataSet *ALS = 0;
	if (this->ALSdata) ALS = this->ALSdata->getCALSData();

	vector<CObservationHandler *> imgVec;
	vector<CHugeImage *> hugImgVec; 

	for (unsigned int i = 0; i < this->SelectedImageList.size(); ++i)
	{
		CVImage *img = this->SelectedImageList[i];
		imgVec.push_back(img);
		hugImgVec.push_back(img->getHugeImage());
	}

	bool ret = true;

	unsigned int nOK = 0;
	this->success = true;
	CSystemUtilities::mkdir(this->outputPath);

	CCharString delAllF = this->outputPath + "_delAll.bat";
	ofstream delAllFile(delAllF.GetChar());

	for (unsigned int i = 0; i < this->BuildingList.size() && this->success; ++i)
	{	
#ifdef WIN32		
		CCharString bldPath = this->outputPath + this->BuildingList[i]->getLabel().GetChar() + CSystemUtilities::dirDelimStr;
		CFileName protfile("Reconstruction.prt");
		protfile.SetPath(bldPath);
		CSystemUtilities::mkdir(bldPath);

		delAllFile << "cd " << this->BuildingList[i]->getLabel().GetChar() << "\ncall _del.bat\ncd.."<< endl;
	
		CCharString delF = bldPath + "_del.bat";
		ofstream delFile(delF.GetChar());


		delFile << " del regions_merged_*.txt              \n del neighbours_merged_*.txt \n del labels_merged_*.tif "
			    << " del initRegionVector4Labels_step*.txt \n del fex_*.*                 \n del OpLabels*.tif "
				<< " del start*.*                          \n del final*.*                \n del img.tif";
		delFile.close();

		protHandler.open(protHandler.prt, protfile, false);
		protHandler.setDefaultDirectory(bldPath);

		ostrstream oss;
		oss <<  "Building Reconstruction (" << (i + 1) << " of " << this->BuildingList.size() << ")" << ends;
		char *z = oss.str();

		this->reconstructor = new CBuildingReconstructor(this->SearchSize, this->neighbours, this->obliqueness,
														 this->sigmaZ, this->alpha, this->minBuildingHeight,
														 this->featureParameters);
													     
		bui_ProgressDlg dlg(new CBuildingReconstructionThread(this->BuildingList[i], &imgVec, &hugImgVec, 
													          ALS, bldPath, this->ImageRes, this->ALSRes, 
															  this->reconstructor), this->reconstructor, z);
		dlg.exec();
		this->success = dlg.getSuccess();

		delete [] z;
#endif
		if (this->success) nOK++;
		delete this->reconstructor;
		this->reconstructor = 0;
	}


	delAllFile << "\n pause";
	delAllFile.close();

	if (nOK < this->BuildingList.size()) this->success = false;

	if (!this->success)
	{		
		QMessageBox::critical(this, "Reconstruction failed!", "Reconstruction failed!");
	}

	baristaProjectHelper.refreshTree();
	protHandler.close(protHandler.prt);
	protHandler.setDefaultDirectory(defDir);

	this->close();
};


//=============================================================================

void bui_BuildingReconstructionDialog::Cancel()
{
	this->close();
}


//=============================================================================

void bui_BuildingReconstructionDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/ImageRegistrationDlg.html");
};


//=============================================================================

#endif // WIN32
