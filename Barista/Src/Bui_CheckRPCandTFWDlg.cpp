#include "Bui_CheckRPCandTFWDlg.h"
#include "Icons.h"

bui_CheckRPCandTFWDlg::bui_CheckRPCandTFWDlg(void)
{
	setupUi(this);
	connect(this->okButton,SIGNAL(released()),this,SLOT(Ok()));
	connect(this->cancelButton,SIGNAL(released()),this,SLOT(Cancel()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
}

bui_CheckRPCandTFWDlg::~bui_CheckRPCandTFWDlg(void)
{
}


void bui_CheckRPCandTFWDlg::Ok()
{
}

void bui_CheckRPCandTFWDlg::Cancel()
{
}