#include "Bui_BandMergeDlg.h"

#include "Icons.h"
#include "BaristaProject.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QString>
#include "BaristaHtmlHelp.h"
#include "IniFile.h"
#include "BaristaProjectHelper.h"
#include "ImageExporter.h"
#include "Bui_ImageSensorSelector.h"

Bui_BandMergeDlg::Bui_BandMergeDlg(void):
	dlg(NULL),
	addTFW(false),
	success(false),
	imageRED(NULL),
	imageGREEN(NULL),
	imageBLUE(NULL),
	imageNIR(NULL),
	mergedImage(NULL),
	bands(-1),
	bpp(-1),
	width(-1),
	height(-1),
	mergedname(""),
	wRED(-1),
	hRED(-1),
	wGREEN(-1),
	hGREEN(-1),
	wBLUE(-1),
	hBLUE(-1),
	wNIR(-1),
	hNIR(-1),
	tfwRED(false),
	tfwGREEN(false),
	tfwBLUE(false),
	tfwNIR(false),
	bandsRED(-1),
	bppRED(-1),
	bandsGREEN(-1),
	bppGREEN(-1),
	bandsBLUE(-1),
	bppBLUE(-1),
	bandsNIR(-1),
	bppNIR(-1)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	// pushbutton connections
	this->connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));

	// output image connection
	connect(this->mergedOutputImage, SIGNAL(released()),this, SLOT(pBSelectOutputReleased()));

	// radio button connections
	this->connect(this->rbMergeTFW,SIGNAL(released()),this,SLOT(rbMergeTFWReleased()));
	this->connect(this->rbUseFromImageTFW,SIGNAL(released()),this,SLOT(rbUseFromImageTFWReleased()));

	// selector connections
	connect(this->comboRED  ,   SIGNAL(activated(int)), this, SLOT(REDImageChanged()));
	connect(this->comboGREEN,   SIGNAL(activated(int)), this, SLOT(GREENImageChanged()));
	connect(this->comboBLUE ,   SIGNAL(activated(int)), this, SLOT(BLUEImageChanged()));
	connect(this->comboNIR  ,   SIGNAL(activated(int)), this, SLOT(NIRImageChanged()));

	this->initDlg();
}

Bui_BandMergeDlg::~Bui_BandMergeDlg()
{
	if (this->dlg)
		delete this->dlg;

	this->dlg=NULL;

	if (!this->success)
	{
		if (this->mergedImage)
			project.deleteImage(this->mergedImage);
	}

}

void Bui_BandMergeDlg::initDlg()
{
	this->initImageSelectors();

	// RED
	this->le_widthRED->setDisabled(true);
	this->le_heightRED->setDisabled(true);
	this->le_bandsRED->setDisabled(true);
	this->le_bppRED->setDisabled(true);
	this->cbTFWRED->setDisabled(true);

	// GREEN
	this->le_widthGREEN->setDisabled(true);
	this->le_heightGREEN->setDisabled(true);
	this->le_bandsGREEN->setDisabled(true);
	this->le_bppGREEN->setDisabled(true);
	this->cbTFWGREEN->setDisabled(true);

	// BLUE
	this->le_widthBLUE->setDisabled(true);
	this->le_heightBLUE->setDisabled(true);
	this->le_bandsBLUE->setDisabled(true);
	this->le_bppBLUE->setDisabled(true);
	this->cbTFWBLUE->setDisabled(true);

	// NIR
	this->le_widthNIR->setDisabled(true);
	this->le_heightNIR->setDisabled(true);
	this->le_bandsNIR->setDisabled(true);
	this->le_bppNIR->setDisabled(true);
	this->cbTFWNIR->setDisabled(true);
}

void Bui_BandMergeDlg::applySettings()
{
	this->checkSetup();

}

void Bui_BandMergeDlg::pBOkReleased()
{
	if (this->mergedname.IsEmpty())
		return;

	this->setSuccess(true);
	this->close();
}

void Bui_BandMergeDlg::pBCancelReleased()
{
	this->setSuccess(false);
	this->close();
}

void Bui_BandMergeDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/BandMergeDlg.html");
}

void Bui_BandMergeDlg::pBSelectOutputReleased()
{
	QString filename = QFileDialog::getSaveFileName(this,
						"Choose File",
						inifile.getCurrDir(),
						//"Fileformats (*.tif *.jpg *.jp2 *.j2k *.ecw)",
						"",
						NULL);

	CFileName testname((const char*)filename.toLatin1());
	
	if ( testname.IsEmpty())
		return;

	this->mergedname = testname;

	this->mergedOutputFile->setText(this->mergedname.GetFullFileName().GetChar());

	this->checkSetup();
}

void Bui_BandMergeDlg::checkSetup()
{
	bool readyToGo = true;
	this->pBOk->setEnabled(false);

	int howmanyselected = this->getSelectedImageCount();

	if ( !this->checkBandCount()) 
	{
		readyToGo = false;
		if ( howmanyselected > 2 )
			QMessageBox::warning(this,"Image selection not valid",
			"One or more image have more than one band!",1,0,0);
	}
	if ( !this->checkHeights() && readyToGo) 
	{ 
		readyToGo = false;
		if ( howmanyselected > 1 )
			QMessageBox::warning(this,"Image selection not valid",
			"One or more image have a different height in pixel!",1,0,0);
	}
	if ( !this->checkWidths() && readyToGo)
	{
		readyToGo = false;
		if ( howmanyselected > 1 )
			QMessageBox::warning(this,"Image selection not valid",
			"One or more image have a different width in pixel!",1,0,0);
	}
	if ( !this->checkBpp() && readyToGo)
	{
		readyToGo = false;
		if ( howmanyselected > 1 )
			QMessageBox::warning(this,"Image selection not valid",
			"One or more image have a different image depth (Bits per pixel)!",1,0,0);
	}
	if ( !this->checkTFW() && readyToGo)
	{
		readyToGo = false;
		if ( howmanyselected > 1 )
			QMessageBox::warning(this,"Image selection not valid",
			"One or more image have a different TFW!",1,0,0);
	}

	// check filename is set
	if ( this->mergedname.IsEmpty() )
		readyToGo = false;

	if ( readyToGo ) this->pBOk->setEnabled(true);
}



void Bui_BandMergeDlg::cbTFWReleased()
{
	this->checkSetup();
}


void Bui_BandMergeDlg::rbMergeTFWReleased()
{
	this->checkSetup();
}

void Bui_BandMergeDlg::rbUseFromImageTFWReleased()
{
	this->checkSetup();
}


bool Bui_BandMergeDlg::createMergedImage()
{
	if (this->dlg)
		delete this->dlg;
	this->dlg = NULL;

	if (this->mergedImage)
	{
		project.deleteImage(this->mergedImage);
	}


	this->mergedImage = project.addImage(this->mergedname,false);
	this->mergedImage->getHugeImage()->bands = this->bands;
	this->mergedImage->getHugeImage()->bpp = this->bpp;
	this->mergedImage->setTFW(CTFW());
	
	if ( this->cbTFWMerged->isChecked() && this->tfw.gethasValues() )
	{
		this->mergedImage->initTFW(this->tfw);
	}
	

	this->dlg = new Bui_GDALExportDlg(this->mergedname,this->mergedImage,false,"Export Image");
	this->dlg->exec();

	if (!dlg->getSuccess())	// false if user has pressed "Cancel"
	{
		if (this->mergedImage)
			project.deleteImage(this->mergedImage);
		return false;
	}

	this->mergedImage->setName(this->dlg->getFilename().GetChar());
	CFileName hugname(this->mergedImage->getName().GetChar());
	hugname += ".hug";

	CHugeImage* hugRED   = this->imageRED->getHugeImage();
	CHugeImage* hugGREEN = this->imageGREEN->getHugeImage();
	CHugeImage* hugBLUE  = this->imageBLUE->getHugeImage();
	CHugeImage* hugNIR   = this->imageNIR ? this->imageNIR->getHugeImage() : 0;

	bool okay = false;

	baristaProjectHelper.setIsBusy(true);
	if ( !this->mergedImage->getHugeImage()->mergeImageBands(hugname, hugRED, hugGREEN, hugBLUE, hugNIR))
	{
		baristaProjectHelper.setIsBusy(false);
		QMessageBox::information(this,"Merging of image bands - Error",
			"Merge image wasn't created",1,0,0);
		return false;
	}
	else
	{
		// save image to disk
		okay = this->dlg->writeImage();
		baristaProjectHelper.setIsBusy(false);
	}

	return okay;
}


void Bui_BandMergeDlg::initImageSelectors()
{
	this->imageSelectorRED   = new bui_ImageSensorSelector(this->comboRED  , 0, ePANONLY, eALLSENSORS, true);
	this->imageSelectorGREEN = new bui_ImageSensorSelector(this->comboGREEN, 0, ePANONLY, eALLSENSORS, true);
	this->imageSelectorBLUE  = new bui_ImageSensorSelector(this->comboBLUE , 0, ePANONLY, eALLSENSORS, true);
	this->imageSelectorNIR   = new bui_ImageSensorSelector(this->comboNIR  , 0, ePANONLY, eALLSENSORS, true);
}

void Bui_BandMergeDlg::REDImageChanged()
{
	if (!this->imageSelectorRED->reactOnImageSelect()) 
	{
		QMessageBox::information(this, "No image selected", " No RED channel information set");
	};

	this->imageRED = this->imageSelectorRED->getSelectedImage();

	if (this->imageRED)
	{
		this->wRED = this->imageRED->getHugeImage()->getWidth();
		this->hRED = this->imageRED->getHugeImage()->getHeight();
		this->bandsRED = this->imageRED->getHugeImage()->getBands();
		this->bppRED = this->imageRED->getHugeImage()->getBpp();
		this->tfwRED = this->imageRED->getTFW()->gethasValues();

		this->le_widthRED->setText(QString::number(this->wRED));
		this->le_heightRED->setText(QString::number(this->hRED));
		this->le_bandsRED->setText(QString::number(this->bandsRED));
		this->le_bppRED->setText(QString::number(this->bppRED));

		this->le_widthRED->setEnabled(true);
		this->le_heightRED->setEnabled(true);
		this->le_bandsRED->setEnabled(true);
		this->le_bppRED->setEnabled(true);

		this->cbTFWRED->setEnabled(true);
		this->cbTFWRED->setChecked(this->tfwRED);
		this->cbTFWRED->setEnabled(false);
	}
	else
	{
		this->wRED = -1;
		this->hRED = -1;
		this->bandsRED = -1;
		this->bppRED = -1;
		this->tfwRED = false;

		this->le_widthRED->setText("");
		this->le_heightRED->setText("");
		this->le_bandsRED->setText("");
		this->le_bppRED->setText("");

		this->le_widthRED->setEnabled(false);
		this->le_heightRED->setEnabled(false);
		this->le_bandsRED->setEnabled(false);
		this->le_bppRED->setEnabled(false);

		this->cbTFWRED->setEnabled(true);
		this->cbTFWRED->setChecked(false);
		this->cbTFWRED->setEnabled(false);
	}

	this->updateTFWOptions();
	this->checkSetup();
};

void Bui_BandMergeDlg::GREENImageChanged()
{
	if (!this->imageSelectorGREEN->reactOnImageSelect()) 
	{
		QMessageBox::information(this, "No image selected", " No GREEN channel information set");
	};

	this->imageGREEN = this->imageSelectorGREEN->getSelectedImage();

	if (this->imageGREEN)
	{
		this->wGREEN = this->imageGREEN->getHugeImage()->getWidth();
		this->hGREEN = this->imageGREEN->getHugeImage()->getHeight();
		this->bandsGREEN = this->imageGREEN->getHugeImage()->getBands();
		this->bppGREEN = this->imageGREEN->getHugeImage()->getBpp();
		this->tfwGREEN = this->imageGREEN->getTFW()->gethasValues();

		this->le_widthGREEN->setText(QString::number(this->wGREEN));
		this->le_heightGREEN->setText(QString::number(this->hGREEN));
		this->le_bandsGREEN->setText(QString::number(this->bandsGREEN));
		this->le_bppGREEN->setText(QString::number(this->bppGREEN));

		this->le_widthGREEN->setEnabled(true);
		this->le_heightGREEN->setEnabled(true);
		this->le_bandsGREEN->setEnabled(true);
		this->le_bppGREEN->setEnabled(true);

		this->cbTFWGREEN->setEnabled(true);
		this->cbTFWGREEN->setChecked(this->tfwGREEN);
		this->cbTFWGREEN->setEnabled(false);
	}
	else
	{
		this->wGREEN = -1;
		this->hGREEN = -1;
		this->bandsGREEN = -1;
		this->bppGREEN = -1;
		this->tfwGREEN = false;

		this->le_widthGREEN->setText("");
		this->le_heightGREEN->setText("");
		this->le_bandsGREEN->setText("");
		this->le_bppGREEN->setText("");

		this->le_widthGREEN->setEnabled(false);
		this->le_heightGREEN->setEnabled(false);
		this->le_bandsGREEN->setEnabled(false);
		this->le_bppGREEN->setEnabled(false);

		this->cbTFWGREEN->setEnabled(true);
		this->cbTFWGREEN->setChecked(false);
		this->cbTFWGREEN->setEnabled(false);
	}

	this->updateTFWOptions();
	this->checkSetup();
};

void Bui_BandMergeDlg::BLUEImageChanged()
{
	if (!this->imageSelectorBLUE->reactOnImageSelect()) 
	{
		QMessageBox::information(this, "No image selected", " No BLUE channel information set");
	};

	this->imageBLUE = this->imageSelectorBLUE->getSelectedImage();

	if (this->imageBLUE)
	{
		this->wBLUE = this->imageBLUE->getHugeImage()->getWidth();
		this->hBLUE = this->imageBLUE->getHugeImage()->getHeight();
		this->bandsBLUE = this->imageBLUE->getHugeImage()->getBands();
		this->bppBLUE = this->imageBLUE->getHugeImage()->getBpp();
		this->tfwBLUE = this->imageBLUE->getTFW()->gethasValues();

		this->le_widthBLUE->setText(QString::number(this->wBLUE));
		this->le_heightBLUE->setText(QString::number(this->hBLUE));
		this->le_bandsBLUE->setText(QString::number(this->bandsBLUE));
		this->le_bppBLUE->setText(QString::number(this->bppBLUE));

		this->le_widthBLUE->setEnabled(true);
		this->le_heightBLUE->setEnabled(true);
		this->le_bandsBLUE->setEnabled(true);
		this->le_bppBLUE->setEnabled(true);

		this->cbTFWBLUE->setEnabled(true);
		this->cbTFWBLUE->setChecked(this->tfwBLUE);
		this->cbTFWBLUE->setEnabled(false);
	}
	else
	{
		this->wBLUE = -1;
		this->hBLUE = -1;
		this->bandsBLUE = -1;
		this->bppBLUE = -1;
		this->tfwBLUE = false;

		this->le_widthBLUE->setText("");
		this->le_heightBLUE->setText("");
		this->le_bandsBLUE->setText("");
		this->le_bppBLUE->setText("");

		this->le_widthBLUE->setEnabled(false);
		this->le_heightBLUE->setEnabled(false);
		this->le_bandsBLUE->setEnabled(false);
		this->le_bppBLUE->setEnabled(false);

		this->cbTFWBLUE->setEnabled(true);
		this->cbTFWBLUE->setChecked(false);
		this->cbTFWBLUE->setEnabled(false);
	}

	this->updateTFWOptions();
	this->checkSetup();
};

void Bui_BandMergeDlg::NIRImageChanged()
{
	if (!this->imageSelectorNIR->reactOnImageSelect()) 
	{
		QMessageBox::information(this, "No image selected", " No NIR channel information set");
	};

	this->imageNIR = this->imageSelectorNIR->getSelectedImage();

	if (this->imageNIR)
	{
		this->wNIR = this->imageNIR->getHugeImage()->getWidth();
		this->hNIR = this->imageNIR->getHugeImage()->getHeight();
		this->bandsNIR = this->imageNIR->getHugeImage()->getBands();
		this->bppNIR = this->imageNIR->getHugeImage()->getBpp();
		this->tfwNIR = this->imageNIR->getTFW()->gethasValues();

		this->le_widthNIR->setText(QString::number(this->wNIR));
		this->le_heightNIR->setText(QString::number(this->hNIR));
		this->le_bandsNIR->setText(QString::number(this->bandsNIR));
		this->le_bppNIR->setText(QString::number(this->bppNIR));

		this->le_widthNIR->setEnabled(true);
		this->le_heightNIR->setEnabled(true);
		this->le_bandsNIR->setEnabled(true);
		this->le_bppNIR->setEnabled(true);

		this->cbTFWNIR->setEnabled(true);
		this->cbTFWNIR->setChecked(this->tfwNIR);
		this->cbTFWNIR->setEnabled(false);
	}
	else
	{
		this->wNIR = -1;
		this->hNIR = -1;
		this->bandsNIR = -1;
		this->bppNIR = -1;
		this->tfwNIR = false;

		this->le_widthNIR->setText("");
		this->le_heightNIR->setText("");
		this->le_bandsNIR->setText("");
		this->le_bppNIR->setText("");

		this->le_widthNIR->setEnabled(false);
		this->le_heightNIR->setEnabled(false);
		this->le_bandsNIR->setEnabled(false);
		this->le_bppNIR->setEnabled(false);

		this->cbTFWNIR->setEnabled(true);
		this->cbTFWNIR->setChecked(false);
		this->cbTFWNIR->setEnabled(false);
	}

	this->updateTFWOptions();
	this->checkSetup();
};


bool Bui_BandMergeDlg::checkBandCount()
{
	if (!this->checkSingleBand() ) return false;

	// check that at least RED, GREEN, BLUE is set
	// RGB or RGBN case
	if ( this->imageRED && this->imageGREEN && this->imageBLUE )
	{
		this->bands = 3;
		if ( this->imageNIR )
		{
			this->bands = 4;
		}
		return true;
	}
	else
	{
		this->bands = -1;
		return false;
	}

};

bool Bui_BandMergeDlg::checkHeights()
{
	// check for consistent height
	if ( this->bands == 3 && (this->hRED == this->hGREEN) && (this->hRED == this->hBLUE) )
	{
		this->height = this->hRED;
		return true;
	}
	else if ( this->bands == 4 && (this->hRED == this->hGREEN) && ( this->hRED == this->hBLUE) && ( this->hRED == this->hNIR ))
	{
		this->height = this->hRED;
		return true;
	}
	else
	{
		this->height = -1;
		return false;
	}
};

bool Bui_BandMergeDlg::checkWidths()
{
	// check for consistent width
	if ( (this->bands == 3) && (this->wRED == this->wGREEN) && (this->wRED == this->wBLUE) )
	{
		this->width = this->wRED;
		return true;
	}
	else if ( (this->bands == 4) && (this->wRED == this->wGREEN) && ( this->wRED == this->wBLUE) && ( this->wRED == this->wNIR ))
	{
		this->width = this->wRED;
		return true;
	}
	else
	{
		this->width = -1;
		return false;
	}
};

bool Bui_BandMergeDlg::checkSingleBand()
{
	// check for 1 band only
	if ( this->imageRED && this->bandsRED != 1 ) return false;
	if ( this->imageGREEN && this->bandsGREEN != 1 ) return false;
	if ( this->imageBLUE && this->bandsBLUE != 1 ) return false;
	if ( this->imageNIR && this->bandsNIR != 1 ) return false;

	return true;

};

bool Bui_BandMergeDlg::checkBpp()
{
	// check for bpp consistency
	if ( (this->bands == 3) && (this->bppRED == this->bppGREEN) && (this->bppRED == this->bppBLUE) )
	{
		this->bpp = this->bppRED;
		return true;
	}
	else if ( (this->bands == 4) && (this->bppRED == this->bppGREEN) && ( this->bppRED == this->bppBLUE) && ( this->bppRED == this->bppNIR ))
	{
		this->bpp = this->bppRED;
		return true;
	}
	else
	{
		this->bpp = -1;
		return false;
	}
};

void Bui_BandMergeDlg::updateTFWOptions()
{
	// check if TFW is/are there
	this->cbTFWMerged->setEnabled(false);
	this->rbMergeTFW->setEnabled(false);
	this->rbUseFromImageTFW->setEnabled(false);	
	this->comboTFW->setEnabled(false);
	this->comboTFW->clear();

	if ( this->tfwRED || this->tfwGREEN || this->tfwBLUE || this->tfwNIR )
	{
		this->cbTFWMerged->setEnabled(true);
		this->cbTFWMerged->setChecked(true);
		this->rbMergeTFW->setEnabled(true);
		this->rbMergeTFW->setChecked(true);

		if ( this->tfwRED )
			this->comboTFW->addItem("RED");
		if ( this->tfwGREEN )
			this->comboTFW->addItem("GREEN");
		if ( this->tfwBLUE )
			this->comboTFW->addItem("BLUE");
		if ( this->tfwNIR )
			this->comboTFW->addItem("NIR");
		
		if ( this->comboTFW->count() > 0 )
		{
			if ( this->comboTFW->count() > 1 ) this->rbMergeTFW->setEnabled(true);
			this->rbUseFromImageTFW->setEnabled(true);
			this->comboTFW->setEnabled(true);
		}
	}
	else
	{
		this->cbTFWMerged->setEnabled(true);
		this->rbMergeTFW->setEnabled(true);
		this->rbUseFromImageTFW->setEnabled(true);	
		this->comboTFW->setEnabled(true);

		this->rbMergeTFW->setChecked(false);
		this->rbUseFromImageTFW->setChecked(false);
		this->cbTFWMerged->setChecked(false);

		this->cbTFWMerged->setEnabled(false);
		this->rbMergeTFW->setEnabled(false);
		this->rbUseFromImageTFW->setEnabled(false);	
		this->comboTFW->setEnabled(false);
	}
};


bool Bui_BandMergeDlg::checkTFW()
{
	if ( this->cbTFWMerged->isChecked())
	{
		// take TFW from specific image of combobox
		if (this->rbUseFromImageTFW->isChecked() )
		{
			QString qstr(this->comboTFW->currentText());
			if ( qstr=="RED")
				this->tfw = *this->imageRED->getTFW();
			else if ( qstr=="GREEN")
				this->tfw = *this->imageGREEN->getTFW();
			else if ( qstr=="BLUE")
				this->tfw = *this->imageBLUE->getTFW();
			else if ( qstr=="NIR")
				this->tfw = *this->imageNIR->getTFW();
			else 
				this->tfw.sethasValues(false);

			return true;
		}
		if ( this->rbMergeTFW->isChecked() )
		{
			bool allowmerge = this->mergeTFW();
			this->rbMergeTFW->setChecked(allowmerge);
			return allowmerge;
		}
		return false;
	}

	// okay, as no TFW will be added anyway
	else
		return true;
}

bool Bui_BandMergeDlg::mergeTFW()
{
	int tfwcount = 0;

	CTFW TFWRED;
	CTFW TFWGREEN;
	CTFW TFWBLUE;
	CTFW TFWNIR;

	if ( this->imageRED && this->imageRED->hasTFW())
	{
		TFWRED = *this->imageRED->getTFW();
		tfwcount++;
	}
	if ( this->imageGREEN && this->imageGREEN->hasTFW())
	{
		TFWGREEN = *this->imageGREEN->getTFW();
		tfwcount++;
	}
	if ( this->imageBLUE && this->imageBLUE->hasTFW())
	{
		TFWBLUE = *this->imageBLUE->getTFW();
		tfwcount++;
	}
	if ( this->imageNIR && this->imageNIR->hasTFW())
	{
		TFWNIR = *this->imageNIR->getTFW();
		tfwcount++;
	}

	if (TFWRED.gethasValues()) this->tfw = TFWRED;
	else if ( TFWGREEN.gethasValues() ) this->tfw = TFWGREEN;
	else if (TFWBLUE.gethasValues() ) this->tfw = TFWBLUE;
	else if (TFWNIR.gethasValues() ) this->tfw = TFWNIR;

	if ( tfwcount == 1 ) return true;

	bool sameValues = true;

	if ( this->tfw.gethasValues())
	{
		if (this->tfw!= TFWRED) sameValues = false;

		if ( sameValues )
			if (this->tfw!= TFWGREEN ) sameValues = false;

		if ( sameValues )
			if (this->tfw!=  TFWBLUE ) sameValues = false;

		if ( sameValues )
			if (this->tfw!= TFWNIR ) sameValues = false;
	}
	
	if ( !sameValues ) this->tfw.sethasValues(false);

	return sameValues;
}


int Bui_BandMergeDlg::getSelectedImageCount()
{
	int count = 0;

	if ( this->imageRED ) count++;
	if ( this->imageGREEN ) count++;
	if ( this->imageBLUE ) count++;
	if ( this->imageNIR ) count++;

	return count;
}






	
	




