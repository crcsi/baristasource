#include "Bui_RAWImportDlg.h"
#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "IniFile.h"
#include "BaristaProject.h"
#include "Bui_ReferenceSysDlg.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include "SystemUtilities.h"

#include <QString>
#include <QFileDialog>
#include <QMessageBox>



Bui_RAWImportDlg::Bui_RAWImportDlg(bool isDEM) : 
	success(false), FileSize(0),
	SizefromSettings(0),decimals(3),isDEM(isDEM),
	sizeMustFit(true),isASCII(false)

{
	this->setupUi(this);
	
	this->setWindowTitle("Import unformatted data");

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	if ( !this->isDEM )
	{
		this->FileFilter = "*.raw *.dat;; All Files (*.*)";
		this->setWindowTitle("Import unformatted data as Image");
		this->gbNoDataSigmaZ->setVisible(false);
		this->gbExtensionGridding->setEnabled(false);
	}
	else
	{
		this->setWindowTitle("Import unformatted data as DEM");
		this->FileFilter = "DEM (*.txt *.dat *.xyz *.grd *.hgt *.DEM *.raw);; All Files (*.*)";

		this->gbChannels->setEnabled(false);
		this->cbGeoreferencing->setVisible(false);
	}

	// pushbutton connections
	this->connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));

	this->connect(this->pB_File, SIGNAL(released()),this,SLOT(pBFileSelectReleased()));


	// spin box connections
	this->connect(this->spinBoxChannels, SIGNAL(valueChanged(int)), this, SLOT(spinBoxChannelsChanged(int)));
	this->connect(this->spinBoxCols, SIGNAL(valueChanged(int)), this, SLOT(spinBoxColsChanged(int)));
	this->connect(this->spinBoxRows, SIGNAL(valueChanged(int)), this, SLOT(spinBoxRowsChanged(int)));

	// lineEdits connections
	this->connect(this->leLLX,SIGNAL(textChanged(const QString &)),this,SLOT(leLLXChanged(const QString &)));
	this->connect(this->leLLY,SIGNAL(textChanged(const QString &)),this,SLOT(leLLYChanged(const QString &)));
	this->connect(this->leGridX,SIGNAL(textChanged(const QString &)),this,SLOT(leGridXChanged(const QString &)));
	this->connect(this->leGridY,SIGNAL(textChanged(const QString &)),this,SLOT(leGridYChanged(const QString &)));
	this->connect(this->leNoData,SIGNAL(textChanged(const QString &)),this,SLOT(NoDataChanged(const QString &)));
	this->connect(this->leZ0,SIGNAL(textChanged(const QString &)),this,SLOT(leZ0Changed(const QString &)));
	this->connect(this->leScale,SIGNAL(textChanged(const QString &)),this,SLOT(leScaleChanged(const QString &)));
	this->connect(this->leSigmaZ,SIGNAL(textChanged(const QString &)),this,SLOT(leSigmaZChanged(const QString &)));
	this->connect(this->leHeaderSize,SIGNAL(textChanged(const QString &)),this,SLOT(leHeaderSizeChanged(const QString &)));

	// radio button connections
	this->connect(this->rb8bit,SIGNAL(released()), this,SLOT(rb8bitReleased()));
	this->connect(this->rb16bit,SIGNAL(released()),this,SLOT(rb16bitReleased()));
	this->connect(this->rb32bit,SIGNAL(released()),this,SLOT(rb32bitReleased()));
	this->connect(this->rb64bit,SIGNAL(released()),this,SLOT(rb64bitReleased()));

	this->connect(this->rbSigned,SIGNAL(released()),this,SLOT(rbSignedReleased()));
	this->connect(this->rbUnsigned,SIGNAL(released()),this,SLOT(rbUnsignedReleased()));

	this->connect(this->rbBigEndian,SIGNAL(released()),this,SLOT(rbBigEndianReleased()));
	this->connect(this->rbLittleEndian,SIGNAL(released()),this,SLOT(rbLittleEndianReleased()));

	this->connect(this->rbTop,SIGNAL(released()),this,SLOT(rbTopReleased()));
	this->connect(this->rbBottom,SIGNAL(released()),this,SLOT(rbBottomReleased()));

	this->connect(this->rbASCII,SIGNAL(released()),this,SLOT(rbASCIIReleased()));
	this->connect(this->rbBinary,SIGNAL(released()),this,SLOT(rbBinaryReleased()));

	this->connect(this->rbInterleaved,SIGNAL(released()),this,SLOT(rbInterleavedReleased()));
	this->connect(this->rbNonInterleaved,SIGNAL(released()),this,SLOT(rbNonInterleavedReleased()));

	// check box connections
	this->connect(this->cb1arc,SIGNAL(released()), this,SLOT(cb1arcReleased()));
	this->connect(this->cb3arc,SIGNAL(released()), this,SLOT(cb3arcReleased()));
	this->connect(this->cb30arc,SIGNAL(released()), this,SLOT(cb30arcReleased()));
	this->connect(this->cbSquare,SIGNAL(released()), this,SLOT(cbSquareReleased()));
	this->connect(this->cbGeoreferencing,SIGNAL(released()), this,SLOT(cbGeoreferencingReleased()));

	// validators
	this->leLLX->setValidator(new QDoubleValidator(this));
	this->leLLY->setValidator(new QDoubleValidator(this));
	this->leGridX->setValidator(new QDoubleValidator(this));
	this->leGridY->setValidator(new QDoubleValidator(this));
	this->leNoData->setValidator(new QDoubleValidator(this));
	this->leZ0->setValidator(new QDoubleValidator(this));
	this->leScale->setValidator(new QDoubleValidator(this));

	this->leSigmaZ->setValidator(new QDoubleValidator(0.000001, 10000000, 3, this));
	this->leHeaderSize->setValidator(new QIntValidator(0,10000000.0,this));

	this->resize(0,0);
	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
	this->resize(0,0);
	this->init();
}

Bui_RAWImportDlg::~Bui_RAWImportDlg()
{
	this->data.deleteImageFile();
}


void Bui_RAWImportDlg::pBOkReleased()
{
	if ( this->cb_HugeFile->isChecked())
	{
		this->data.bDeleteHugeFile = true;
	}

	CImageImporter imageImporter;

	// in case we couldn't read the file the user has now provided information to read the file anyway
	// init it accordingly, we just need the imagefile ...
	if (!this->data.imagefile && this->isASCII)
	{
		
		imageImporter.initImportDataAsASCII(this->data,this->isDEM);
		
	}
	else if (!this->data.imagefile && !this->isASCII)
	{
			
		imageImporter.initImportDataAsBinary(this->data,this->isDEM);
	}


	
	bui_ProgressDlg dlg( new CImageLoadThread(imageImporter,this->data,this->isDEM),&imageImporter,"Importing data ...");
	dlg.exec();

	this->setSuccess(dlg.getSuccess());

	if (!this->success)
		QMessageBox::warning(this,"Import Error",imageImporter.getErrorMessage().GetChar());

	this->applyGeoReferencing();

	this->close();
	
	
}

void Bui_RAWImportDlg::pBCancelReleased()
{
	this->setSuccess(false);
	this->close();
}

void Bui_RAWImportDlg::pBHelpReleased()
{
	if ( this->isDEM)
		CHtmlHelp::callHelp("UserGuide/ImportDEM.html#UnformattedDEMImport");
	else
		CHtmlHelp::callHelp("UserGuide/ImportImages.html#UnformattedImageImport");
}


void Bui_RAWImportDlg::init()
{
	QString qstr;

	this->pBOk->setEnabled(false);

	this->spinBoxCols->setValue(this->data.cols);
	this->spinBoxRows->setValue(this->data.rows);

	this->leLLX->blockSignals(true);
	this->leLLY->blockSignals(true);
	this->leGridX->blockSignals(true);
	this->leGridY->blockSignals(true);
	this->leNoData->blockSignals(true);
	this->leZ0->blockSignals(true);
	this->leScale->blockSignals(true);
	this->leSigmaZ->blockSignals(true);

	this->leLLX->setText(QString( "%1" ).arg(this->data.lowerleft.x, 0, 'F', 3));
	this->leLLY->setText(QString( "%1" ).arg(this->data.lowerleft.y, 0, 'F', 3));
	this->leGridX->setText(QString( "%1" ).arg(this->data.gridding.x, 0, 'F', this->decimals));
	this->leGridY->setText(QString( "%1" ).arg(this->data.gridding.y, 0, 'F', this->decimals));

	if ( fabs(this->data.NoDataValue) > 10000000)
		this->leNoData->setText(QString( "%1" ).arg(this->data.NoDataValue, 0, 'E', 6));
	else
		this->leNoData->setText(QString( "%1" ).arg(this->data.NoDataValue, 0, 'F', 3));
	this->leZ0->setText(QString( "%1" ).arg(this->data.Z0, 0, 'F', 3));
	this->leScale->setText(QString( "%1" ).arg(this->data.scale, 0, 'F', 3));
	this->leSigmaZ->setText(QString( "%1" ).arg(this->data.sigmaZ, 0, 'F', 3));

	this->leLLX->blockSignals(false);
	this->leLLY->blockSignals(false);
	this->leGridX->blockSignals(false);
	this->leGridY->blockSignals(false);
	this->leNoData->blockSignals(false);
	this->leZ0->blockSignals(false);
	this->leScale->blockSignals(false);
	this->leSigmaZ->blockSignals(false);

	if ( this->data.filebpp == 8 )
		this->rb8bit->setChecked(true);
	else if ( this->data.filebpp == 16 )
		this->rb16bit->setChecked(true);
	else if ( this->data.filebpp == 32 )
		this->rb32bit->setChecked(true);
	else if ( this->data.filebpp == 64 )
		this->rb64bit->setChecked(true);
	else
		this->rb8bit->setChecked(true);

	this->rbBigEndian->setChecked(this->data.BigEndian);
	this->rbTop->setChecked(this->data.toptobottom);
	this->rbInterleaved->setChecked(this->data.interleaved);
	this->rbSigned->setChecked(this->data.interpretation);

	if ( this->cbSquare->isChecked() )
		this->leGridY->setEnabled(false);
	else
		this->leGridY->setEnabled(true);

	this->spinBoxChannels->setValue(this->data.bands);

	
	qstr.setNum(this->data.HeaderSize);
	this->leHeaderSize->setText(qstr);

	qstr.setNum(this->FileSize);
	this->leFileSize->setText(qstr);

	qstr.setNum(this->SizefromSettings);
	this->leFileSizeSettings->setText(qstr);


	qstr.setNum(this->FileSize);
	this->leFileSize->setText(qstr);

	if ( this->isASCII ) this->gbData->setEnabled(false);
	else  this->gbData->setEnabled(true);

	if ( this->sizeMustFit ) this->gbFileSize->setEnabled(true);
	else this->gbFileSize->setEnabled(false);


	this->le_filename->setText(this->data.filename.GetChar());

	this->checkSetup();
}

void Bui_RAWImportDlg::reset()
{
	this->data.reset();
	this->FileSize = 0;
	this->SizefromSettings = 0;
	this->decimals = 3;

	this->cbSquare->setChecked(false);
	this->cb1arc->setChecked(false);
	this->cb3arc->setChecked(false);
	this->cb30arc->setChecked(false);

	this->rbASCII->setChecked(false);
	this->rbBinary->setChecked(false);
	this->sizeMustFit = true;
	this->isASCII = false;
}


void Bui_RAWImportDlg::pBFileSelectReleased()
{
	this->reset();

	QString qname = QFileDialog::getOpenFileName(
		this,
		"Choose file",
		inifile.getCurrDir(),
		this->FileFilter);

    // make sure the file exists
    QFile test(qname);
    if (!test.exists())
        return;

	this->FileSize = test.size();
    test.close();

	CFileName filename = qname.toLatin1().constData();
	CFileName hugeFileNameDir;

	CImageImporter imageImporter;

	// check if we can actually write the huge file 
	if (!imageImporter.checkWritePemission(filename))
	{
		// ask for new directory to save the huge file
		QString newDir;
		do{
			newDir = QFileDialog::getExistingDirectory(this,"Select directory to save pyramide file",project.getFileName()->GetPath().GetChar());
			hugeFileNameDir = newDir.toLatin1().constData();
			hugeFileNameDir += CSystemUtilities::dirDelimStr;
		}while(newDir.isEmpty() ||  !imageImporter.checkWritePemission(CFileName(hugeFileNameDir + "dummy")));

	}

	// ues the tread, just in case we call XYZ -> txt convert function
	bui_ProgressDlg dlg(new CImageLoadThread(imageImporter,this->data,this->isDEM,filename,hugeFileNameDir),&imageImporter,"Init data import ...");
	dlg.exec();
	
	// if we go an imagefile then the reading process was ok 
	if (this->data.imagefile)
	{
		if (this->data.imagefile->getFileType() == CImageFile::eGDALFile || 
			this->data.imagefile->getFileType() == CImageFile::eRAWFile)
		{
			this->sizeMustFit = false;
			this->rbBinaryReleased();
		}
		else if (this->data.imagefile->getFileType() == CImageFile::eASCIIDEMFile)
		{
			this->rbASCIIReleased();
			this->sizeMustFit = false;
		}

	}
	
	// show error message
	else if (!imageImporter.getErrorMessage().IsEmpty())
	{
		CCharString title;
		if (this->isDEM)
			title = "Importing Unformatted DEM...";
		else
			title = "Importing Unformatted Image...";
		QMessageBox::information(this,title.GetChar(),imageImporter.getErrorMessage().GetChar());		
		imageImporter.setErrorMessage("");
	}

	else // if no imagefile, then the user has to supply all information
	{
		this->sizeMustFit = true;
		this->guessDimensions();
		
		if (this->data.filename.GetFileExt().CompareNoCase("HGT"))
		{
			this->data.NoDataValue = -32768;
			this->cb3arcReleased();
		}
		
	}
	
	this->init();
}

void Bui_RAWImportDlg::guessDimensions()
{
	// assumption of square dimensions and no header
	int testsize = this->FileSize - this->data.HeaderSize;

	int pixelcount = testsize/(this->data.bands*this->data.filebpp/8);

	this->data.cols = sqrt((double)pixelcount);
	this->data.rows = this->data.cols;

	if ( this->data.cols*this->data.rows == pixelcount ) return;
	else this->data.filebpp = 16;

	pixelcount = testsize/(this->data.bands*this->data.filebpp/8);
	this->data.cols = sqrt((double)pixelcount);
	this->data.rows = this->data.cols;

	if ( this->data.cols*this->data.rows == pixelcount ) return;
	else this->data.filebpp = 32;

	pixelcount = testsize/(this->data.bands*this->data.filebpp/8);
	this->data.cols = sqrt((double)pixelcount);
	this->data.rows = this->data.cols;

	if ( this->data.cols*this->data.rows != pixelcount ) 
	{
		this->data.cols = 0;
		this->data.rows = 0;
		this->data.filebpp = 8;
		return;
	}

}

void Bui_RAWImportDlg::checkSetup()
{
	bool readyToGo = true;
	this->pBOk->setEnabled(false);

	// check filesize
	bool sizeokay = this->checkSize();
	if ( this->sizeMustFit ) readyToGo = sizeokay;

	// check gridx, gridy
	if ( this->data.gridding.x == 0.0 || this->data.gridding.y ==0.0 )
		readyToGo = false;

	// ASCII/Binary selected
	if ( !this->rbASCII->isChecked() && !this->rbBinary->isChecked() )
		readyToGo = false;

	// check filename is set
	if ( this->data.filename.IsEmpty() )
		readyToGo = false;

	this->pBOk->setEnabled(readyToGo);

}

bool Bui_RAWImportDlg::checkSize()
{

	bool sizeokay = false;
	int pixelSize = 0;

	if ( this->data.filebpp == 8 )
		pixelSize = 1;
	else if ( this->data.filebpp == 16 )
		pixelSize = 2;
	else if ( this->data.filebpp == 32 )
		pixelSize = 4;
	else if ( this->data.filebpp == 64 )
		pixelSize = 8;

	this->SizefromSettings = this->data.HeaderSize + this->data.bands*pixelSize*this->data.cols*this->data.rows;

	QString qstr;
	qstr.setNum(this->SizefromSettings);
	this->leFileSizeSettings->setText(qstr);

	if ( this->SizefromSettings == this->FileSize ) sizeokay = true;
	return sizeokay;
}

void Bui_RAWImportDlg::leLLXChanged(const QString & text)
{
	if (this->leLLX->hasAcceptableInput())
		this->data.lowerleft.x = this->leLLX->text().toDouble();

	this->checkSetup();
}


void Bui_RAWImportDlg::leLLYChanged(const QString & text)
{
	if (this->leLLY->hasAcceptableInput())
		this->data.lowerleft.y = this->leLLY->text().toDouble();

	this->checkSetup();
}


void Bui_RAWImportDlg::leGridXChanged(const QString & text)
{
	if (this->leGridX->hasAcceptableInput())
		this->data.gridding.x = this->leGridX->text().toDouble();

	int precomma = text.indexOf(".") + 1;
	if ( precomma > 0)
		this->decimals = text.length() - precomma;

	if ( this->cbSquare->isChecked() )
	{
		this->data.gridding.y = this->data.gridding.x;
		this->leGridY->blockSignals(true);

		this->leGridY->setText(this->leGridX->text());

		//this->setDoubleText(this->leGridY, this->gridding.y, this->griddecimals);
		this->leGridY->blockSignals(false);
	}

	this->cb1arc->setChecked(false);
	this->cb3arc->setChecked(false);
	this->cb30arc->setChecked(false);

	this->checkSetup();
}
void Bui_RAWImportDlg::leGridYChanged(const QString & text)
{
	if (this->leGridY->hasAcceptableInput())
		this->data.gridding.y = this->leGridY->text().toDouble();

	int precomma = text.indexOf(".") + 1;
	if ( precomma > 0)
		this->decimals = text.length() - precomma;

	this->cb1arc->setChecked(false);
	this->cb3arc->setChecked(false);
	this->cb30arc->setChecked(false);

	this->checkSetup();
}


void Bui_RAWImportDlg::leHeaderSizeChanged(const QString & text)
{
	if (this->leHeaderSize->hasAcceptableInput() && !text.isEmpty())
		this->data.HeaderSize =  this->leHeaderSize->text().toInt();

	this->checkSetup();
}


void Bui_RAWImportDlg::NoDataChanged(const QString & text)
{
	if (this->leNoData->hasAcceptableInput() && !text.isEmpty())
		this->data.NoDataValue = this->leNoData->text().toDouble();
}
void Bui_RAWImportDlg::leZ0Changed(const QString & text)
{
	if (this->leZ0->hasAcceptableInput() && !text.isEmpty())
		this->data.Z0 = this->leZ0->text().toDouble();
}
void Bui_RAWImportDlg::leScaleChanged(const QString & text)
{
	if (this->leScale->hasAcceptableInput() && !text.isEmpty() )
		this->data.scale = this->leScale->text().toDouble();
}

void Bui_RAWImportDlg::leSigmaZChanged(const QString & text)
{
	if (this->leSigmaZ->hasAcceptableInput() && !text.isEmpty())
		this->data.sigmaZ = this->leSigmaZ->text().toDouble();

	if ( text.isEmpty() ) this->init();
}


void Bui_RAWImportDlg::spinBoxChannelsChanged( int bands)
{
	this->data.bands = bands;

	this->checkSetup();
}

void Bui_RAWImportDlg::spinBoxColsChanged(int newcols)
{
	this->data.cols = newcols;

	this->checkSetup();
}

void Bui_RAWImportDlg::spinBoxRowsChanged(int newrows)
{
	this->data.rows = newrows;

	this->checkSetup();
}

void Bui_RAWImportDlg::cbSquareReleased()
{
	if ( this->cbSquare->isChecked())
	{
		this->data.gridding.y = this->data.gridding.x;

		this->leGridY->blockSignals(true);
		this->leGridY->setText(this->leGridX->text());
		this->leGridY->blockSignals(false);
		this->leGridY->setEnabled(false);
	}
	else
	{
		this->leGridY->setEnabled(true);
		this->cb1arc->setChecked(false);
		this->cb3arc->setChecked(false);
		this->cb30arc->setChecked(false);
	}
}

void Bui_RAWImportDlg::rb8bitReleased()
{
	this->data.filebpp = 8;
	this->checkSetup();
}
void Bui_RAWImportDlg::rb16bitReleased()
{
	this->data.filebpp = 16;
	this->checkSetup();
}
void Bui_RAWImportDlg::rb32bitReleased()
{
	this->data.filebpp = 32;
	this->checkSetup();
}
void Bui_RAWImportDlg::rb64bitReleased()
{
	this->data.filebpp = 64;
	this->checkSetup();
}

void Bui_RAWImportDlg::cb1arcReleased()
{
	double grid = 1.0/3600.0;

	this->decimals = 10;

	this->data.gridding.x = grid;
	this->data.gridding.y = grid;
	this->cbSquare->setChecked(true);

	this->leGridX->setText(QString( "%1" ).arg(this->data.gridding.x, 0, 'F', this->decimals));
	this->leGridY->setText(QString( "%1" ).arg(this->data.gridding.y, 0, 'F', this->decimals));

	this->cb1arc->setChecked(true);
	this->cb3arc->setChecked(false);
	this->cb30arc->setChecked(false);

	this->checkSetup();
}

void Bui_RAWImportDlg::cb3arcReleased()
{
	double grid = 3.0/3600.0;

	this->decimals = 10;

	this->data.gridding.x = grid;
	this->data.gridding.y = grid;
	this->cbSquare->setChecked(true);

	this->leGridX->setText(QString( "%1" ).arg(this->data.gridding.x, 0, 'F', this->decimals));
	this->leGridY->setText(QString( "%1" ).arg(this->data.gridding.y, 0, 'F', this->decimals));

	this->cb1arc->setChecked(false);
	this->cb3arc->setChecked(true);
	this->cb30arc->setChecked(false);

	this->checkSetup();
}

void Bui_RAWImportDlg::cb30arcReleased()
{
	double grid = 30.0/3600.0;

	this->decimals = 10;

	this->data.gridding.x = grid;
	this->data.gridding.y = grid;
	this->cbSquare->setChecked(true);

	this->leGridX->setText(QString( "%1" ).arg(this->data.gridding.x, 0, 'F', this->decimals));
	this->leGridY->setText(QString( "%1" ).arg(this->data.gridding.y, 0, 'F', this->decimals));

	this->cb1arc->setChecked(false);
	this->cb3arc->setChecked(false);
	this->cb30arc->setChecked(true);

	this->checkSetup();
}


void Bui_RAWImportDlg::rbSignedReleased()
{
	this->data.interpretation = true;
	this->checkSetup();
}

void Bui_RAWImportDlg::rbUnsignedReleased()
{
	this->data.interpretation = false;
	this->checkSetup();
}

void Bui_RAWImportDlg::rbBigEndianReleased()
{
	this->data.BigEndian = true;
	this->checkSetup();
}

void Bui_RAWImportDlg::rbLittleEndianReleased()
{
	this->data.BigEndian = false;
	this->checkSetup();
}

void Bui_RAWImportDlg::rbTopReleased()
{
	this->data.toptobottom = true;
	this->checkSetup();
}

void Bui_RAWImportDlg::rbBottomReleased()
{
	this->data.toptobottom = false;
	this->checkSetup();
}

void Bui_RAWImportDlg::rbInterleavedReleased()
{
	this->data.interleaved = true;
	this->checkSetup();
}

void Bui_RAWImportDlg::rbNonInterleavedReleased()
{
	this->data.interleaved = false;
	this->checkSetup();
}

void Bui_RAWImportDlg::rbASCIIReleased()
{
	this->isASCII = true;
	this->rbASCII->setChecked(true);
	this->init();
}

void Bui_RAWImportDlg::rbBinaryReleased()
{
	this->isASCII = false;
	this->rbBinary->setChecked(true);
	this->init();
}


void Bui_RAWImportDlg::cbGeoreferencingReleased()
{
	if ( this->cbGeoreferencing->isChecked() )
	{
		this->gbExtensionGridding->setEnabled(true);
		this->data.bImportTFW = true;
		this->data.tfwSource = CImageImportData::eParameter;
	}
	else
	{
		this->gbExtensionGridding->setEnabled(false);
		this->data.bImportTFW = false;
	}
}



bool Bui_RAWImportDlg::applyGeoReferencing()
{
	if (this->data.vbase && (this->isDEM || this->cbGeoreferencing->isChecked()))
	{
		CReferenceSystem refsys = this->data.tfw.getRefSys();
		this->data.tfwFilename = this->data.filename + ".tfw";

		Bui_ReferenceSysDlg refdlg(&refsys,&this->data.tfwFilename, "Set TFW reference system information","UserGuide/ThumbNodeTFW.html",false,NULL,true);
		refdlg.exec();

		if (!refdlg.getSuccess()) 
			refsys.sethasValues(false);
		else
			refsys.sethasValues(true);
				
		CTFW localTFW;
		if ( refsys.getCoordinateType() == eGEOGRAPHIC )
		{
			Matrix parameters(6,1);
			parameters.element(0,0) = 0.0;
			parameters.element(1,0) = -this->data.gridding.y;
			parameters.element(2,0) = this->data.lowerleft.y + (this->data.rows - 1) * (this->data.gridding.y);
			parameters.element(3,0) = this->data.gridding.x;
			parameters.element(4,0) = 0.0;
			parameters.element(5,0) = this->data.lowerleft.x;
			refsys.setUTMZone(parameters.element(2,0), parameters.element(5,0));
	        
			localTFW.setAll(parameters);

			localTFW.setFileName(this->data.tfwFilename.GetChar());
			localTFW.setRefSys(refsys);
				
		}
		else
		{
			C2DPoint shift(this->data.lowerleft.x,  this->data.lowerleft.y + (this->data.rows - 1) * (this->data.gridding.y));
			localTFW.setFileName(this->data.tfwFilename.GetChar());
			localTFW.setRefSys(refsys);
			localTFW.setFromShiftAndPixelSize(shift, this->data.gridding);
		}

		this->data.vbase->setTFW(localTFW);
	}

	return true;
}

