#include <QFileDialog>

#include "Bui_SnakeDlg.h"

#include "BaristaProject.h"
#include "Icons.h"
#include "VImage.h"
#include "Bui_ProgressDlg.h"
#include "BaristaHtmlHelp.h"
#include "ProgressThread.h"
#include "LabelImage.h"
#include "ProtocolHandler.h"
#include "BaristaProjectHelper.h"


Bui_SnakeDlg::Bui_SnakeDlg(QWidget* parent, CFileName initialise, CVImage *I_image, bool open,  CXYPointArray *startPoints, bool *ok): 
			  QDialog(parent), iniFileName(initialise), image(I_image), OpenSnake(open), Points(startPoints), CaclulationOK(ok), parsAccepted(false)
{
	if (!initialise.IsEmpty() && this->parameters.initParameters(initialise))
	{
	}
	else
	{
		this->parameters.Mode = eTraditional;
	}

	this->parameters.openSnake = this->OpenSnake;
	
	this->setup();
	this->parsAccepted = false;
	this->ImageSelectorComboBox->blockSignals(true);

	if (I_image)
	{
		this->labelFileName = I_image->getFileNameChar();
		this->ImageSelectorComboBox->addItem(labelFileName.GetChar());
		this->labelFileName.SetPath(project.getFileName()->GetPath());
		this->labelFileName.SetFileName(this->labelFileName.GetFileName() + "_reg");
		if (this->labelFileName.GetFileExt() != CCharString("tif")) this->labelFileName.SetFileExt("tif");
	}
	else this->ImageSelectorComboBox->addItem("No data");

	this->ImageSelectorComboBox->blockSignals(false);
	this->ImageSelectorComboBox->setEnabled(false);

	this->initSnakeDlg();
};


Bui_SnakeDlg::~Bui_SnakeDlg()
{
	image = 0;
};

void Bui_SnakeDlg::setup()
{
	setupUi(this);
	
	//connect(this->ImageSelectorComboBox,SIGNAL(currentIndexChanged(int)),	this, SLOT(ImageChanged()));

	connect(this->OK_button,			SIGNAL(released()),				    this, SLOT(OK()));
	connect(this->Cancel_button,        SIGNAL(released()),				    this, SLOT(Cancel()));
	connect(this->Help_button,          SIGNAL(released()),				    this, SLOT(Help()));

	//connect(this->CurveClose,          SIGNAL(clicked(bool)),				this, SLOT(checkSnakeTypes()));
	//connect(this->CurveOpen,	        SIGNAL(clicked(bool)),				this, SLOT(checkSnakeTypes()));
	
	connect(this->Traditional,          SIGNAL(clicked(bool)),				this, SLOT(checkSnakeMode()));
	connect(this->GVF,					SIGNAL(clicked(bool)),				this, SLOT(checkSnakeMode()));
	connect(this->Balloon,				SIGNAL(clicked(bool)),				this, SLOT(checkSnakeMode()));

	connect(this->GS_Buffer,			SIGNAL(editingFinished()),			this, SLOT(checkValuesGeneralSettings()));
	connect(this->GS_StopCriteria,		SIGNAL(editingFinished()),			this, SLOT(checkValuesGeneralSettings()));
	connect(this->GS_Sigma,				SIGNAL(editingFinished()),			this, SLOT(checkValuesGeneralSettings()));
	connect(this->GS_Gamma,				SIGNAL(editingFinished()),			this, SLOT(checkValuesGeneralSettings()));
	connect(this->GS_SnakeSpace,		SIGNAL(editingFinished()),			this, SLOT(checkValuesGeneralSettings()));
	connect(this->GS_mo,				SIGNAL(editingFinished()),			this, SLOT(checkValuesGeneralSettings()));

	connect(this->lnternalE_alpha,		SIGNAL(editingFinished()),			this, SLOT(checkValuesInternalEnergy()));
	connect(this->lnternalE_beta,		SIGNAL(editingFinished()),			this, SLOT(checkValuesInternalEnergy()));

	connect(this->ExternalE_Kappa,		SIGNAL(editingFinished()),			this, SLOT(checkValuesExternalEnergy()));
	connect(this->ExternalE_kb,			SIGNAL(editingFinished()),			this, SLOT(checkValuesExternalEnergy()));
	
	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
};

void Bui_SnakeDlg::initImages()
{
	this->ImageSelectorComboBox->clear();

	this->ImageSelectorComboBox->blockSignals(true);
	if (project.getNumberOfImages() < 1)
	{
		this->ImageSelectorComboBox->addItem("No data");
	}
	else
	{
		for (int i = 0; i < project.getNumberOfImages(); ++i)
		{
			this->ImageSelectorComboBox->addItem(project.getImageAt(i)->getFileNameChar());
		}

		this->ImageSelectorComboBox->setCurrentIndex(0);
		this->image = project.getImageAt(0);
	}
	this->ImageSelectorComboBox->blockSignals(false);
};

void Bui_SnakeDlg::initSnakeDlg()
{
	this->parsAccepted = false;

	this->GVF->setEnabled(false);

	this->setParameters();

	this->OK_button->setFocusPolicy(Qt::NoFocus);	
	this->Cancel_button->setFocusPolicy(Qt::NoFocus);
};

void Bui_SnakeDlg::Cancel()
{
	this->parsAccepted = false;
	this->close();
};

void Bui_SnakeDlg::Help()
{
	CHtmlHelp::callHelp("UserGuide/FeatureExtractionDlg.html");
};


/*void Bui_SnakeDlg::checkSnakeTypes()
{
	this->CurveClose->blockSignals(true);
	this->CurveOpen->blockSignals(true);
	
	if (this->parameters.openSnake == true) 
	{
		this->CurveOpen->setChecked(true);
		//this->CurveClose->setEnabled(false); 
	}
	else 
	{
		this->CurveClose->setChecked(true);
		//this->CurveOpen->setEnabled(false); 
	}

	if (this->CurveClose->isChecked()) 
	{
		parameters.openSnake = false;
	}
	else 
	{
		parameters.openSnake = true;
	}

	if (parameters.openSnake == true) CurveOpen->setFocus();
	else CurveClose->setFocus();

	this->CurveClose->blockSignals(false);
	this->CurveOpen->blockSignals(false);
};*/

void Bui_SnakeDlg::checkSnakeMode()
{
	this->Traditional->blockSignals(true);
	this->GVF->blockSignals(true);
	this->Balloon->blockSignals(true);

	if (this->parameters.Mode == eTraditional) this->Traditional->setChecked(false);
	else if (this->parameters.Mode == eGVF) this->GVF->setChecked(false);
	else this->Balloon->setChecked(false);

	if (this->Traditional->isChecked())
	{
		this->GS_mo->setEnabled(false);
		this->ExternalE_kb->setEnabled(false);
		parameters.Mode = eTraditional;
	}
	else if (this->GVF->isChecked())
	{
		this->GS_mo->setEnabled(true);
		this->ExternalE_kb->setEnabled(false);
		parameters.Mode = eGVF;
	}
	else 
	{
		this->GS_mo->setEnabled(false);
		this->ExternalE_kb->setEnabled(true);
		parameters.Mode = eBalloon;
	}

	Traditional->setFocus();

	this->Traditional->blockSignals(false);
	this->GVF->blockSignals(false);
	this->Balloon->blockSignals(false);
};

void Bui_SnakeDlg::checkValuesGeneralSettings()
{
	this->GS_Buffer->blockSignals(true);
	this->GS_StopCriteria->blockSignals(true);
	this->GS_Sigma->blockSignals(true);
	this->GS_Gamma->blockSignals(true);
	this->GS_SnakeSpace->blockSignals(true);
	this->GS_mo->blockSignals(true);

	bool OK = false;

	unsigned int Buffer = GS_Buffer->text().toDouble(&OK);
	if ((!OK) || (Buffer < 0.0f) || (Buffer > 100.0f)) Buffer = this->parameters.getBuffer();
	else this->parameters.buffer = Buffer;
	
	QString ds=QString("%1").arg(Buffer);
	this->GS_Buffer->setText(ds);

	float Stop = GS_StopCriteria->text().toDouble(&OK);
	if((!OK) || (Stop < 0.1f) || (Stop > 30.0f)) Stop = this->parameters.getStopCriteria();
	else this->parameters.stopCriteria = Stop;

	ds=QString("%1").arg(Stop, 0, 'f', 2);
	this->GS_StopCriteria->setText(ds);

	float Sigma = GS_Sigma->text().toDouble(&OK);
	if((!OK) || (Sigma < 0.1f) || (Sigma > 50.0f)) Sigma = this->parameters.getSigma();
	else this->parameters.sigma = Sigma;

	ds=QString("%1").arg(Sigma, 0, 'f', 2);
	this->GS_Sigma->setText(ds);

	float Gamma = GS_Gamma->text().toDouble(&OK);
	if((!OK) || (Gamma < 0.0f) || (Gamma > 100.0f)) Gamma = this->parameters.getGamma();
	else this->parameters.gamma = Gamma;

	ds=QString("%1").arg(Gamma, 0, 'f', 3);
	this->GS_Gamma->setText(ds);
	
	unsigned int SSpace = GS_SnakeSpace->text().toDouble(&OK);
	if ((!OK) || (SSpace < 2.0f) || (SSpace > 50.0f)) SSpace = this->parameters.getSnakeSpace();
	else this->parameters.snakeSpace = SSpace;
	
	ds=QString("%1").arg(SSpace);
	this->GS_SnakeSpace->setText(ds);

	if (this->GVF->isChecked())
	{
		float mo = GS_mo->text().toDouble(&OK);
		if((!OK) || (mo < 0.0f) || (mo > 100.0f)) mo = this->parameters.getMo();
		else this->parameters.mo = mo;

		ds=QString("%1").arg(mo, 0, 'f', 3);
		this->GS_mo->setText(ds);
	}

	this->GS_Buffer->blockSignals(false);
	this->GS_StopCriteria->blockSignals(false);
	this->GS_Sigma->blockSignals(false);
	this->GS_Gamma->blockSignals(false);
	this->GS_SnakeSpace->blockSignals(false);
	this->GS_mo->blockSignals(false);
};

void Bui_SnakeDlg::checkValuesInternalEnergy()
{
	this->lnternalE_alpha->blockSignals(true);
	this->lnternalE_beta->blockSignals(true);

	bool OK = false;

	float Alpha = lnternalE_alpha->text().toDouble(&OK);
	if((!OK) || (Alpha < 0.0f) || (Alpha > 100.0f)) Alpha = this->parameters.getAlpha();
	else this->parameters.alpha = Alpha;

	QString ds=QString("%1").arg(Alpha, 0, 'f', 3);
	this->lnternalE_alpha->setText(ds);

	float Beta = lnternalE_beta->text().toDouble(&OK);
	if((!OK) || (Beta < 0.0f) || (Beta > 100.0f)) Beta = this->parameters.getBeta();
	else this->parameters.beta = Beta;

	ds=QString("%1").arg(Beta, 0, 'f', 3);
	this->lnternalE_beta->setText(ds);

	this->lnternalE_alpha->blockSignals(false);
	this->lnternalE_beta->blockSignals(false);
};

void Bui_SnakeDlg::checkValuesExternalEnergy()
{
	this->ExternalE_Kappa->blockSignals(true);
	this->ExternalE_kb->blockSignals(true);
	
	bool OK = false;

	float Kappa = ExternalE_Kappa->text().toDouble(&OK);
	if((!OK) || (Kappa < 0.0f) || (Kappa >= 100.0f)) Kappa = this->parameters.getKappa();
	else this->parameters.kappa = Kappa;

	QString ds=QString("%1").arg(Kappa, 0, 'f', 3);
	this->ExternalE_Kappa->setText(ds);

	if (this->Balloon->isChecked())
	{
		float KB = ExternalE_kb->text().toDouble(&OK);
		if((!OK) || (KB < 0.0f) || (KB > 100.0f)) KB = this->parameters.getKa();
		else this->parameters.ka = KB;

		ds=QString("%1").arg(KB, 0, 'f', 3);
		this->ExternalE_kb->setText(ds);
	}

	this->ExternalE_Kappa->blockSignals(false);
	this->ExternalE_kb->blockSignals(false);
};

void Bui_SnakeDlg::setParameters()
{
	this->Traditional->blockSignals(true);
	this->GVF->blockSignals(true);
	this->Balloon->blockSignals(true);

	this->CurveOpen->blockSignals(true);
	this->CurveClose->blockSignals(true);

	this->GS_Buffer->blockSignals(true);
	this->GS_Gamma->blockSignals(true);
	this->GS_mo->blockSignals(true);
	this->GS_Sigma->blockSignals(true);
	this->GS_SnakeSpace->blockSignals(true);
	this->GS_StopCriteria->blockSignals(true);
	
	this->lnternalE_alpha->blockSignals(true);
	this->lnternalE_beta->blockSignals(true);

	this->ExternalE_Kappa->blockSignals(true);
	this->ExternalE_kb->blockSignals(true);

	if(parameters.Mode == eTraditional)
	{
		Traditional->setChecked(true);
		GVF->setChecked(false);
		Balloon->setChecked(false);

		this->GS_mo->setEnabled(false);
		this->ExternalE_kb->setEnabled(false);
	}
	else if(parameters.Mode == eGVF)
	{
		Traditional->setChecked(false);
		GVF->setChecked(true);
		Balloon->setChecked(false);

		this->ExternalE_kb->setEnabled(false);
	}
	else if(parameters.Mode == eBalloon)
	{
		Traditional->setChecked(false);
		GVF->setChecked(false);
		Balloon->setChecked(true);

		this->GS_mo->setEnabled(false);
	}

	if(parameters.openSnake == true)
	{
		CurveOpen->setChecked(true);

		CurveClose->setChecked(false);
		CurveClose->setEnabled(false);
		
	}
	else
	{
		//CurveOpen->setChecked(false);
		CurveClose->setChecked(true);

		CurveOpen->setChecked(false);
		CurveOpen->setEnabled(false);
	}

	QString ds=QString("%1").arg(parameters.getBuffer());
	this->GS_Buffer->setText(ds);

	ds=QString("%1").arg(parameters.getStopCriteria(), 0, 'f', 2);
	this->GS_StopCriteria->setText(ds);

	ds=QString("%1").arg(parameters.getSigma(), 0, 'f', 2);
	this->GS_Sigma->setText(ds);

	ds=QString("%1").arg(parameters.getGamma(), 0, 'f', 3);
	this->GS_Gamma->setText(ds);
	
	ds=QString("%1").arg(parameters.getSnakeSpace());
	this->GS_SnakeSpace->setText(ds);

	ds=QString("%1").arg(parameters.getMo(), 0, 'f', 3);
	this->GS_mo->setText(ds);

	ds=QString("%1").arg(parameters.getAlpha(), 0, 'f', 3);
	this->lnternalE_alpha->setText(ds);

	ds=QString("%1").arg(parameters.getBeta(), 0, 'f', 3);
	this->lnternalE_beta->setText(ds);

	ds=QString("%1").arg(parameters.getKappa(), 0, 'f', 3);
	this->ExternalE_Kappa->setText(ds);

	ds=QString("%1").arg(parameters.getKa(), 0, 'f', 3);
	this->ExternalE_kb->setText(ds);

	this->Traditional->blockSignals(false);
	this->GVF->blockSignals(false);
	this->Balloon->blockSignals(false);

	this->CurveOpen->blockSignals(false);
	this->CurveClose->blockSignals(false);

	this->GS_Buffer->blockSignals(false);
	this->GS_Gamma->blockSignals(false);
	this->GS_mo->blockSignals(false);
	this->GS_Sigma->blockSignals(false);
	this->GS_SnakeSpace->blockSignals(false);
	this->GS_StopCriteria->blockSignals(false);
	
	this->lnternalE_alpha->blockSignals(false);
	this->lnternalE_beta->blockSignals(false);

	this->ExternalE_Kappa->blockSignals(false);
	this->ExternalE_kb->blockSignals(false);
};

void Bui_SnakeDlg::OK()
{
	if (image->getHugeImage()->getRoiPtr())
	{
		this->parameters.writeParameters(iniFileName);			//write parameters for snake calculation in a file

		//preproccesing: enlarge the image buffer buf (ROI) by adding the buffer from the dialog
		int bf = this->parameters.getBuffer();			//buffer bf from the snake dialog (entered by the operator)
		long buf_OffsetX = image->getHugeImage()->getRoiPtr()->getOffsetX();
		long buf_OffsetY = image->getHugeImage()->getRoiPtr()->getOffsetY();
		long buf_height	 = image->getHugeImage()->getRoiPtr()->getHeight();
		long buf_width	 = image->getHugeImage()->getRoiPtr()->getWidth();
		long img_height	 = this->image->getHugeImage()->getHeight();
		long img_width	 = this->image->getHugeImage()->getWidth();
		long tempBufLeft = bf, tmpBufRight = bf;
		int  bands   = image->getHugeImage()->getRoiPtr()->getBands();
		int  bpp     = image->getHugeImage()->getRoiPtr()->getBpp();

		//check if the the buffer bf is out of the image region
		if (bf > buf_OffsetX || bf > buf_OffsetY)												//check left side
		{
			if (buf_OffsetX <= buf_OffsetY) tempBufLeft = buf_OffsetX;
			else tempBufLeft = buf_OffsetY;
		}
		else if (bf > img_height - (buf_height + buf_OffsetY) || bf > img_width - (buf_width + buf_OffsetX))	//check right side
		{
			if (img_height - (buf_height + buf_OffsetY) <=  img_width - (buf_width + buf_OffsetX)) tmpBufRight = img_height - (buf_height + buf_OffsetY);
			else tmpBufRight = img_width - (buf_width + buf_OffsetX);
		}
		
		if (tmpBufRight < tempBufLeft) bf = (int) tmpBufRight;
		else if (tempBufLeft < tmpBufRight) bf = (int) tempBufLeft;

		//set the new buffer region
		buf_OffsetX -= bf;
		buf_OffsetY -= bf;
		buf_width += 2*bf;
		buf_height+= 2*bf;

		if(buf_width > 4000 || buf_height > 4000)
		{
			//image->getHugeImage()->getRoiPtr()  QDialog this->setq ->image->getHugeImage()->getRoiPtr()->get v->setHasLargeROI(true);
			//QMessageBox::critical( 0, "Barista"," ROI is too large (> 2000 pixels) for Snake Calculation!",
			//	" Please select smaller area for these operations");
			*(this->CaclulationOK) = false;
		}
		else
		{
			image->getHugeImage()->setRoi(buf_OffsetX, buf_OffsetY, buf_width, buf_height);		//set ROI

			CImageBuffer *buf = 0;
			buf = image->getHugeImage()->getRoiPtr();

			//start the calculation process
			CSnakeExtractionThread *thread = new CSnakeExtractionThread(buf, Points, this->parameters); //create a thread

			bui_ProgressDlg dlg(thread, thread);	// create a dialog
			dlg.exec();								// execute this dialog
			
			//return true/false if the calculation was successful or not
			if(dlg.getSuccess()) *(this->CaclulationOK) = true;
			else *(this->CaclulationOK) = false;

			//after the calsulations are done, update the refrech tree
			baristaProjectHelper.refreshTree();
		}		
	}

	this->parsAccepted = true;
	this->close();
}