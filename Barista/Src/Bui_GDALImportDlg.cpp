#include <float.h> 

#include "Bui_GDALImportDlg.h"
#include <vector>
#include <QFileDialog>
#include <QLayout>
#include <QMouseEvent>
#include <QMessageBox>

#include "IniFile.h"
#include "BaristaHtmlHelp.h"
#include "FileTable.h"
#include "Bui_ProgressDlg.h"
#include "SystemUtilities.h"
#include "BaristaProject.h"
#include "Icons.h"
#include "ProgressThread.h"
#include "Bui_ReferenceSysDlg.h"


#include "BaristaProjectHelper.h"



vector<CPredefinedBandOrders> predefinedBandOrders;


Bui_GDALImportDlg::Bui_GDALImportDlg(bool isDEM) : isDEM(isDEM), useGlobalRefSys(false), wasAskeduseGlobalRefSys(false)
{
	this->setupUi(this);
	
	this->setWindowTitle("Import Image(s)");

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));


	this->gbDEMValueSettings->setVisible(false);

	if ( this->isDEM )
	{
		this->setWindowTitle("Import DEM(s)");
		this->gb_Channels->setVisible(false);
		this->cB_PushBroom->setVisible(false);
		this->combo_PushBroom->setVisible(false);
		this->cb_RPC->setVisible(false);
		this->combo_RPC->setVisible(false);
		this->label_2->setVisible(false);
		this->label_5->setVisible(false);

		this->gbDEMValueSettings->setVisible(true);

	}

	this->combo_RPC->setEditable(true);
	this->combo_RPC->lineEdit()->setAlignment(Qt::AlignRight);
	this->combo_RPC->setEditable(false);

	this->tw_Files = new CFileTable(NULL);
	this->sW_Dummy->currentWidget()->layout()->addWidget(this->tw_Files);

	this->success = false;

	this->clearImportFiles();

	this->connect(this->pB_Files, SIGNAL(released()),this,SLOT(pbFilesReleased()));
	this->connect(this->pB_Import,SIGNAL(released()),this,SLOT(pbImportReleased()));
	this->connect(this->pB_Cancel,SIGNAL(released()),this,SLOT(pbCancelReleased()));
	this->connect(this->pB_Help,  SIGNAL(released()),this,SLOT(pbHelpReleased()));
	this->connect(this->cb_HugeFile, SIGNAL(released()),this,SLOT(cbHugeFileReleased()));
	this->connect(this->cb_RPC, SIGNAL(released()),this,SLOT(cbRPCReleased()));	
	this->connect(this->cb_TFW, SIGNAL(released()),this,SLOT(cbTFWReleased()));	
	this->connect(this->cB_PushBroom, SIGNAL(released()),this,SLOT(cBPushBroomReleased()));	
	this->connect(this->rb_ImageFile, SIGNAL(released()),this,SLOT(rbImageFileReleased()));	
	this->connect(this->rb_ExternalFile, SIGNAL(released()),this,SLOT(rbExternalFileReleased()));
	
	this->connect(this->tw_Files, SIGNAL(itemSelectionChanged ()),this, SLOT(twFilesItemSelectionChanged()));
	this->connect(this->combo_RPC, SIGNAL(currentIndexChanged(int)), this, SLOT(comboRPCCurrentIndexChanged(int)));
	this->connect(this->combo_TFW, SIGNAL(currentIndexChanged(int)), this, SLOT(comboTFWCurrentIndexChanged(int)));
	this->connect(this->combo_Bandorders, SIGNAL(currentIndexChanged(int)), this, SLOT(comboBandOrderCurrentIndexChanged(int)));
	this->connect(this->combo_PushBroom, SIGNAL(currentIndexChanged(int)), this, SLOT(comboPushBroomCurrentIndexChanged(int)));

	this->connect(this->lw_redchannels, SIGNAL(currentRowChanged(int)),this,SLOT(lwRedChannelCurrentRowChanged(int))); 
	this->connect(this->lw_greenchannels, SIGNAL(currentRowChanged(int)),this,SLOT(lwGreenChannelCurrentRowChanged (int))); 
	this->connect(this->lw_bluechannels, SIGNAL(currentRowChanged(int)),this,SLOT(lwBlueChannelCurrentRowChanged(int))); 
	this->connect(this->lw_alphachannels,SIGNAL(currentRowChanged(int)),this,SLOT(lwAlphaChannelCurrentRowChanged(int)));

	// DEMValue setting connections
	this->connect(this->leNoData,SIGNAL(textChanged(const QString &)),this,SLOT(NoDataChanged(const QString &)));
	this->connect(this->leZ0,SIGNAL(textChanged(const QString &)),this,SLOT(leZ0Changed(const QString &)));
	this->connect(this->leScale,SIGNAL(textChanged(const QString &)),this,SLOT(leScaleChanged(const QString &)));
	this->connect(this->leSigmaZ,SIGNAL(textChanged(const QString &)),this,SLOT(leSigmaZChanged(const QString &)));

	// validators
	this->leNoData->setValidator(new QDoubleValidator(this));
	this->leZ0->setValidator(new QDoubleValidator(this));
	this->leScale->setValidator(new QDoubleValidator(this));
	this->leSigmaZ->setValidator(new QDoubleValidator(this));

	predefinedBandOrders.clear();

	this->applySettings();

	predefinedBandOrders.push_back(CPredefinedBandOrders("User defined",1,0,0,0));

	predefinedBandOrders.push_back(CPredefinedBandOrders("User defined",3,0,1,2));
	predefinedBandOrders.push_back(CPredefinedBandOrders("RGB",3,0,1,2));
	predefinedBandOrders.push_back(CPredefinedBandOrders("BGR",3,2,1,0));	


	predefinedBandOrders.push_back(CPredefinedBandOrders("User defined",4,0,1,2,3));
	predefinedBandOrders.push_back(CPredefinedBandOrders("Quickbird original",4,2,1,0,3));
	predefinedBandOrders.push_back(CPredefinedBandOrders("Quickbird Pan",4,0,1,2,3));
	this->setModal(true);

	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
}

Bui_GDALImportDlg::~Bui_GDALImportDlg(void)
{
	
	for (unsigned int i=0; i < this->importFiles.size(); i++)	
		this->importFiles[i].d.deleteImageFile();
	
	delete this->tw_Files;
	this->clearImportFiles();
}


int Bui_GDALImportDlg::exec()
{
	this->show();
	this->pbFilesReleased();
	return QDialog::exec();
}




void Bui_GDALImportDlg::pbFilesReleased()
{
	this->clearImportFiles();

	// file dialog
	QString chooseFiles("Choose Image File(s)");
	QString filter("Images (*.tif *.tiff *.jpeg *.jpg *.gif *.ecw *.jp2 *.ceos IMG*__N IMG*__B IMG*__F IMG*__W IMG*_UN IMG*_UF IMG*_UB IMG*___ *.ntf)");

	if ( this->isDEM )
	{
		chooseFiles = "Choose DEM File(s)";
		filter = "DEM (*.txt *.dat *.dtm *.xyz *.grd *.DEM *.adf *.tif);; All Files (*.*)";
	}

	QStringList files = QFileDialog::getOpenFileNames(this,chooseFiles,inifile.getCurrDir(),filter);

	if (files.count() < 1)
	{
		this->applySettings();	
		return;
	}

	baristaProjectHelper.setIsBusy(true);

	this->tw_Files->clear();
	this->tw_Files->setHorizontalHeaderItem(0,this->tw_Files->createItem(QString("Files"),0,Qt::AlignCenter,QColor(0,0,0)));

	CFileName hugeFileNameDir;

	for (int i=0 ; i < files.count(); i++)
	{
		CFileName filename = files[i].toLatin1().constData();
		// create new object for import
		ImportFile element;

		// set default values for every file
		element.extRPC.clear();
		element.extTFW.clear();
		element.pushBroomFiles.clear();
		element.pushBroomDescription.clear();
		element.pushBroomType.clear();
		element.bandorder = 0;

		CImageImporter imageImporter;

		// check if we can actually write the huge file 
		if (i==0 && !imageImporter.checkWritePemission(filename))
		{
			// ask for new directory to save the huge file
			QString newDir;
			do{
				newDir = QFileDialog::getExistingDirectory(this,"Select directory to save pyramide file",project.getFileName()->GetPath().GetChar());
				hugeFileNameDir = newDir.toLatin1().constData();
				hugeFileNameDir += CSystemUtilities::dirDelimStr;
			}while(newDir.isEmpty() ||  !imageImporter.checkWritePemission(CFileName(hugeFileNameDir + "dummy")));

		}
		
		// ues the tread, just in case we call XYZ -> txt convert function
		bui_ProgressDlg dlg(new CImageLoadThread(imageImporter,element.d,this->isDEM,filename,hugeFileNameDir),&imageImporter,"Init data import ...");
		dlg.exec();

		// init data
		bool ret = dlg.getSuccess();
		
		// check for file information
		this->getFileInformation(element);

		// check if this is a ANVIR-2 file
		imageImporter.initImportDataForAVNIR2Image(element.d);


		if (ret && element.d.imagefile )
			this->importFiles.push_back(element);
		else
		{
			element.d.deleteImageFile();		
			QMessageBox::warning(this,"Import error!",imageImporter.getErrorMessage().GetChar());
		}
	}

	this->selectedFiles.clear();
	if ( (unsigned int)(files.count()) > this->importFiles.size() )
	{
		baristaProjectHelper.setIsBusy(false);
		QMessageBox::warning( this, "File import problem!"
		, "One or more of the selected files have an unrecognized format. Import of unrecognized files might be possible via unformatted import.");
		baristaProjectHelper.setIsBusy(true);
	}

	if ( this->importFiles.size() > 0 )
	{
		this->tw_Files->setRowCount(this->importFiles.size());	

		for (unsigned int i=0 ; i < this->importFiles.size(); i++)
		{
			QTableWidgetItem* item = this->tw_Files->createItem(QString(this->importFiles[i].d.filename.GetFullFileName().GetChar()),Qt::ItemIsSelectable,Qt::AlignLeft,QColor(0,0,0));
			this->tw_Files->setRowHeight(i,20);
			this->tw_Files->setItem(i,0,item);
		}

		this->selectedFiles.resize(1);
		this->selectedFiles[0] = 0;
		this->tw_Files->selectRow(this->selectedFiles[0]);
	}

	this->applySettings();
	baristaProjectHelper.setIsBusy(false);
}


void  Bui_GDALImportDlg::applySettings()
{
	this->lw_redchannels->blockSignals(true);
	this->lw_greenchannels->blockSignals(true);
	this->lw_bluechannels->blockSignals(true);
	this->lw_alphachannels->blockSignals(true);
	this->combo_Bandorders->blockSignals(true);

	this->clearDialog();

	this->checkAvailabilityOfSettings();

	if (this->importFiles.size() < 1)
		return;

	this->le_Directory->setText(QString(this->importFiles[0].d.filename.GetPath()));


	// first selected element
	ImportFile& element = this->importFiles[this->selectedFiles[0]];

	if ( !element.d.imagefile ) return;

	for (int i=0; i < element.d.imagefile->bands(); i++)
	{
		this->lw_redchannels->insertItem(i,QString("Channel ")+ QString().number(i+1));
		this->lw_greenchannels->insertItem(i,QString("Channel ")+ QString().number(i+1));
		this->lw_bluechannels->insertItem(i,QString("Channel ")+ QString().number(i+1));
		this->lw_alphachannels->insertItem(i,QString("Channel ")+ QString().number(i+1));
	}

	for (unsigned int i=0; i < predefinedBandOrders.size(); i++)
		if (predefinedBandOrders[i].bands == element.d.imagefile->bands())
			this->combo_Bandorders->addItem(QString(predefinedBandOrders[i].name.GetChar()));

	this->combo_Bandorders->setCurrentIndex(element.bandorder);

	int test = element.d.channel1;
	this->lw_redchannels->setCurrentRow(test);

	test = element.d.channel2;
	this->lw_greenchannels->setCurrentRow(test);

	test = element.d.channel3;
	this->lw_bluechannels->setCurrentRow(test);

	test = element.d.channel4;
	this->lw_alphachannels->setCurrentRow(test);


	for (int i=0; i <element.extRPC.count(); i++)
		this->combo_RPC->addItem(element.extRPC.at(i));

	for (int i=0; i < element.extTFW.count(); i++)
		this->combo_TFW->addItem(element.extTFW.at(i));

	for (int i=0; i < element.pushBroomDescription.count(); i++)
		this->combo_PushBroom->addItem(element.pushBroomDescription.at(i));	


	if (element.d.bHasTFWInImageFile && element.d.tfwSource == CImageImportData::eImageFile)
	{
		this->rb_ImageFile->setChecked(true);
		this->rb_ExternalFile->setChecked(false);
	}
	else if (element.d.bHasTFWInExternalFile && element.d.tfwSource == CImageImportData::eExternalFile && element.extTFW.size())
	{
		this->rb_ImageFile->setChecked(false);
		this->rb_ExternalFile->setChecked(true);
	}


	if (element.d.bDeleteHugeFile)
		this->cb_HugeFile->setChecked(true);
	else
		this->cb_HugeFile->setChecked(false);

	if (element.d.bImportRPC)
		this->cb_RPC->setChecked(true);
	else
		this->cb_RPC->setChecked(false);

	if (element.d.bImportTFW)
		this->cb_TFW->setChecked(true);
	else
		this->cb_TFW->setChecked(false);

	if (element.d.bImportPushBroom)
		this->cB_PushBroom->setChecked(true);
	else
		this->cB_PushBroom->setChecked(false);

	if (element.extRPC.size() > 0)
		this->combo_RPC->setCurrentIndex(element.selectedExtRPC);

	if (element.extTFW.size() > 0)
		this->combo_TFW->setCurrentIndex(element.selectedExtTFW);

	if (element.pushBroomFiles.size() > 0)
		this->combo_PushBroom->setCurrentIndex(element.selectedPushBroom);



	this->lw_redchannels->blockSignals(false);
	this->lw_greenchannels->blockSignals(false);
	this->lw_bluechannels->blockSignals(false);
	this->lw_alphachannels->blockSignals(false);
	this->combo_Bandorders->blockSignals(false);

	// DEM value settings
	this->leNoData->blockSignals(true);
	this->leZ0->blockSignals(true);
	this->leScale->blockSignals(true);
	this->leSigmaZ->blockSignals(true);

	if ( fabs(element.d.NoDataValue) > 10000000)
		this->leNoData->setText(QString( "%1" ).arg(element.d.NoDataValue, 0, 'E', 6));
	else
		this->leNoData->setText(QString( "%1" ).arg(element.d.NoDataValue, 0, 'F', 3));
	this->leZ0->setText(QString( "%1" ).arg(element.d.Z0, 0, 'F', 3));
	this->leScale->setText(QString( "%1" ).arg(element.d.scale, 0, 'F', 3));
	this->leSigmaZ->setText(QString( "%1" ).arg(element.d.sigmaZ, 0, 'F', 3));

	this->leNoData->blockSignals(false);
	this->leZ0->blockSignals(false);
	this->leScale->blockSignals(false);
	this->leSigmaZ->blockSignals(false);

}

void Bui_GDALImportDlg::clearDialog()
{
	this->lw_redchannels->clear();
	this->lw_greenchannels->clear();
	this->lw_bluechannels->clear();
	this->lw_alphachannels->clear();
	this->combo_RPC->clear();
	this->combo_TFW->clear();
	this->combo_PushBroom->clear();
	this->cb_RPC->setChecked(false);
	this->cb_TFW->setChecked(false);
	this->cB_PushBroom->setChecked(false);
	this->cb_HugeFile->setChecked(false);

	this->combo_Bandorders->clear();

}

void Bui_GDALImportDlg::cbHugeFileReleased()
{
	if ( this->selectedFiles.size() < 1)
		return;
	
	if (this->importFiles[this->selectedFiles[0]].d.bDeleteHugeFile)
		this->importFiles[this->selectedFiles[0]].d.bDeleteHugeFile = false;
	else
		this->importFiles[this->selectedFiles[0]].d.bDeleteHugeFile = true;
	
	// remember the clicked setting and apply to the rest
	bool clickedSetting = false;
	clickedSetting = this->importFiles[this->selectedFiles[0]].d.bDeleteHugeFile;

	// apply to the rest
	for (unsigned int i= 1; i < this->selectedFiles.size(); i++) 
		this->importFiles[this->selectedFiles[i]].d.bDeleteHugeFile = clickedSetting;
}

void Bui_GDALImportDlg::comboRPCCurrentIndexChanged(int index)
{
	if ( this->selectedFiles.size() != 1 || index == -1)
		return;

	this->importFiles[this->selectedFiles[0]].selectedExtRPC = index;
	this->importFiles[this->selectedFiles[0]].d.rpcFilename = this->importFiles[this->selectedFiles[0]].extRPC[index].toLatin1().constData();
}

void Bui_GDALImportDlg::comboTFWCurrentIndexChanged(int index)
{
	if ( this->selectedFiles.size() != 1 || index == -1)
		return;

	this->importFiles[this->selectedFiles[0]].selectedExtTFW = index;
	this->importFiles[this->selectedFiles[0]].d.tfwFilename = this->importFiles[this->selectedFiles[0]].extTFW[index].toLatin1().constData();
}


void Bui_GDALImportDlg::lwRedChannelCurrentRowChanged ( int index)
{
	if ( this->selectedFiles.size() < 1 || index == -1)
		return;	

	this->importFiles[this->selectedFiles[0]].d.channel1 = index;
	this->importFiles[this->selectedFiles[0]].bandorder = 0; // reset to User defined

	for (unsigned int i= 1; i < this->selectedFiles.size(); i++) 
		if (this->importFiles[this->selectedFiles[i]].d.imagefile->bands() == 
			this->importFiles[this->selectedFiles[0]].d.imagefile->bands())
		{
			this->importFiles[this->selectedFiles[0]].d.channel1 = index;
			this->importFiles[this->selectedFiles[0]].bandorder = 0; // reset to User defined
		}
	this->applySettings();

}

void Bui_GDALImportDlg::lwGreenChannelCurrentRowChanged ( int index)
{
	if ( this->selectedFiles.size() < 1 || index == -1)
		return;

	this->importFiles[this->selectedFiles[0]].d.channel2 = index;
	this->importFiles[this->selectedFiles[0]].bandorder = 0; // reset to User defined

	for (unsigned int i= 1; i < this->selectedFiles.size(); i++) 
		if (this->importFiles[this->selectedFiles[i]].d.imagefile->bands() == 
			this->importFiles[this->selectedFiles[0]].d.imagefile->bands())
		{
			this->importFiles[this->selectedFiles[0]].d.channel2 = index;
			this->importFiles[this->selectedFiles[0]].bandorder = 0; // reset to User defined
		}
	this->applySettings();
}
 
void Bui_GDALImportDlg::lwBlueChannelCurrentRowChanged ( int index)
{
	if ( this->selectedFiles.size() < 1 || index == -1)
		return;

	this->importFiles[this->selectedFiles[0]].d.channel3 = index;
	this->importFiles[this->selectedFiles[0]].bandorder = 0; // reset to User defined

	for (unsigned int i= 1; i < this->selectedFiles.size(); i++) 
		if (this->importFiles[this->selectedFiles[i]].d.imagefile->bands() == 
			this->importFiles[this->selectedFiles[0]].d.imagefile->bands())
		{
			this->importFiles[this->selectedFiles[0]].d.channel3 = index;
			this->importFiles[this->selectedFiles[0]].bandorder = 0; // reset to User defined
		}
	this->applySettings();
}
 
void Bui_GDALImportDlg::lwAlphaChannelCurrentRowChanged ( int index)
{
	if ( this->selectedFiles.size() < 1 || index == -1)
		return;

	this->importFiles[this->selectedFiles[0]].d.channel4 = index;
	this->importFiles[this->selectedFiles[0]].bandorder = 0; // reset to User defined

	for (unsigned int i= 1; i < this->selectedFiles.size(); i++) 
		if (this->importFiles[this->selectedFiles[i]].d.imagefile->bands() == 
			this->importFiles[this->selectedFiles[0]].d.imagefile->bands())
		{
			this->importFiles[this->selectedFiles[0]].d.channel4 = index;
			this->importFiles[this->selectedFiles[0]].bandorder = 0; // reset to User defined
		}
	this->applySettings();
}

void Bui_GDALImportDlg::comboBandOrderCurrentIndexChanged(int index)
{
	if (index < 0 || index >= int(predefinedBandOrders.size()))
		return;

	if ( this->selectedFiles.size() < 1 || index == -1)
		return;

	// translate combo index to vector index
	int preIndex= 0;
	for (unsigned int i=0; i <  predefinedBandOrders.size(); i++)
		if (predefinedBandOrders[i].name.CompareNoCase((const char*)this->combo_Bandorders->currentText().toLatin1()))
			preIndex = i;

	this->importFiles[this->selectedFiles[0]].bandorder = index;
	this->importFiles[this->selectedFiles[0]].d.channel1 = predefinedBandOrders[preIndex].channel1;
	this->importFiles[this->selectedFiles[0]].d.channel2 = predefinedBandOrders[preIndex].channel2;
	this->importFiles[this->selectedFiles[0]].d.channel3 = predefinedBandOrders[preIndex].channel3;
	this->importFiles[this->selectedFiles[0]].d.channel4 = predefinedBandOrders[preIndex].channel4;


	for (unsigned int i= 1; i < this->selectedFiles.size(); i++) 
		if (this->importFiles[this->selectedFiles[i]].d.imagefile->bands() == 
			this->importFiles[this->selectedFiles[0]].d.imagefile->bands())
		{
			this->importFiles[this->selectedFiles[i]].bandorder = index;
			this->importFiles[this->selectedFiles[i]].d.channel1 = predefinedBandOrders[preIndex].channel1;
			this->importFiles[this->selectedFiles[i]].d.channel2 = predefinedBandOrders[preIndex].channel2;
			this->importFiles[this->selectedFiles[i]].d.channel3 = predefinedBandOrders[preIndex].channel3;
			this->importFiles[this->selectedFiles[i]].d.channel4 = predefinedBandOrders[preIndex].channel4;

		}

	this->applySettings();
}

void Bui_GDALImportDlg::pbImportReleased()
{
	if (this->importFiles.size() < 1)
		return;

	baristaProjectHelper.setIsBusy(true);
	this->success = true;

	vector<CVBase*> vbaselist;

	for (unsigned int i=0; i < this->importFiles.size(); i++)
	{
		
		char title[100];

		sprintf(title,"Progress: file %d / %d ...",i+1,this->importFiles.size());
		
		CImageImporter imageImporter;

		bui_ProgressDlg dlg( new CImageLoadThread(imageImporter,this->importFiles[i].d,this->isDEM),&imageImporter,title);
		dlg.exec();

		// clean up
		this->importFiles[i].d.deleteImageFile();

		if (!dlg.getSuccess())
		{
			baristaProjectHelper.setIsBusy(false);
			QMessageBox::warning( this, "Import Error!", imageImporter.getErrorMessage().GetChar());
			baristaProjectHelper.setIsBusy(true);
		}
		else
		{
			// ask later if the tfw has no ref sys
			if (this->importFiles[i].d.bImportTFW && 
				this->importFiles[i].d.vbase && 
				!this->importFiles[i].d.tfw.getRefSys().gethasValues())
			{
				vbaselist.push_back(this->importFiles[i].d.vbase);
			}
			
			this->tw_Files->item(i,0)->setCheckState(Qt::Checked);
		}

	}

	for (unsigned int i=0; i < vbaselist.size(); i++)
	{
		if ( this->isDEM )
		{
			CVDEM* checkdem = (CVDEM*)vbaselist[i];
			if ( !this->useGlobalRefSys)
			{
				baristaProjectHelper.setIsBusy(false);
				CReferenceSystem refsys = checkdem->getTFW()->getRefSys();
				CFileName tfwname = checkdem->getTFW()->getFileNameChar();

				Bui_ReferenceSysDlg refDlg(&refsys,&tfwname ,"Choose Reference Parameters","");
	
				refDlg.exec();
					
				if (!refDlg.getSuccess())
				{
						QMessageBox::warning( this, "No valid Reference Parameters set!"
						, "Set Reference system via TFW popup menu!");
				}
				else
				{
					checkdem->setReferenceSystem(refsys);
					// ask if this refsys should be used for all following dems
					if ( (i <  vbaselist.size() -1) && !this->useGlobalRefSys && !this->wasAskeduseGlobalRefSys)
					{
						int yesno = QMessageBox::question(this, "Reference System settings",
						"Should this reference system be applied to all remaining DEMs with incomplete settings?",
						"&Yes", "&No",	0, 1  );

						if ( yesno == 1 ) 
						{
							this->useGlobalRefSys = false;
							this->wasAskeduseGlobalRefSys = true;
						}
						else 
						{
							this->globalRefSys = checkdem->getTFW()->getRefSys();
							this->useGlobalRefSys = true;
						}
					}
				}
				baristaProjectHelper.setIsBusy(true);
			}
			else
			{
				checkdem->setReferenceSystem(this->globalRefSys);
			}

/////////////////////
			if ( checkdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC )
			{
				double tgridx = checkdem->getTFW()->getA();
				double tgridy = checkdem->getTFW()->getE();
				double tshiftx = checkdem->getTFW()->getC();
				double tshifty = checkdem->getTFW()->getF();

				Matrix parameters(6,1);

				parameters.element(0,0) = 0.0;
				parameters.element(1,0) = tgridy;
				parameters.element(2,0) = tshifty;
				parameters.element(3,0) = tgridx;
				parameters.element(4,0) = 0.0;
				parameters.element(5,0) = tshiftx;
				
				CReferenceSystem swapprefsys(checkdem->getReferenceSystem());
				swapprefsys.setUTMZone(parameters.element(2,0), parameters.element(5,0));

				CTFW swappedTFW;
				swappedTFW.setAll(parameters);

				swappedTFW.setRefSys(swapprefsys);
				checkdem->setTFW(swappedTFW);
			}

/////////////////////


		}
		else
		{
			CVImage* checkimage = (CVImage*)vbaselist[i];
			if ( !this->useGlobalRefSys)
			{
				baristaProjectHelper.setIsBusy(false);
				CTFW tfw = *checkimage->getTFW();
				CReferenceSystem refsys = tfw.getRefSys();
				CFileName tfwname = tfw.getFileNameChar();

				Bui_ReferenceSysDlg refDlg(&refsys,&tfwname ,"Choose Reference Parameters","");
	
				refDlg.exec();
					
				if (!refDlg.getSuccess())
				{
						QMessageBox::warning( this, "No valid Reference Parameters set!"
						, "Set Reference system via TFW popup menu!");
				}
				else
				{
					tfw.setRefSys(refsys);
					checkimage->setTFW(tfw);
					// ask if this refsys should be used for all following dems
					if ( (i <  vbaselist.size() -1) && !this->useGlobalRefSys && !this->wasAskeduseGlobalRefSys)
					{
						int yesno = QMessageBox::question(this, "Reference System settings",
						"Should this reference system be applied to all remaining images with incomplete settings?",
						"&Yes", "&No",	0, 1  );

						if ( yesno == 1 ) 
						{
							this->useGlobalRefSys = false;
							this->wasAskeduseGlobalRefSys = true;
						}
						else 
						{
							this->globalRefSys = checkimage->getTFW()->getRefSys();
							this->useGlobalRefSys = true;
						}
					}
				}
				baristaProjectHelper.setIsBusy(true);
			}
			else
			{
				CTFW tfw = *checkimage->getTFW();
				tfw.setRefSys(this->globalRefSys);
				checkimage->setTFW(tfw);
			}
		}
		
	}


	inifile.setCurrDir(this->importFiles[0].d.filename.GetPath());
	inifile.writeIniFile();

	baristaProjectHelper.setIsBusy(false);
	this->close();
}

void Bui_GDALImportDlg::pbCancelReleased()
{
	this->success = false;
	this->close();
}

void Bui_GDALImportDlg::pbHelpReleased()
{
	if ( this->isDEM)
		CHtmlHelp::callHelp("UserGuide/ImportDEM.html#FormattedDEMImport");
	else
		CHtmlHelp::callHelp("UserGuide/ImportImages.html#FormattedImageImport");
}




void Bui_GDALImportDlg::cbRPCReleased()
{
	if ( this->selectedFiles.size() < 1)
		return;


	// remember the clicked setting and apply to the rest
	bool clickedSetting = false;

	if (this->cb_RPC->isChecked())
	{
		this->importFiles[this->selectedFiles[0]].d.bImportRPC = true;
		clickedSetting = true;
	}
	else
	{
		this->importFiles[this->selectedFiles[0]].d.bImportRPC = false;
		clickedSetting = false;
	}

	for (unsigned int i= 1; i < this->selectedFiles.size(); i++) 
		if (this->importFiles[this->selectedFiles[i]].d.bHasRPCInExternalFile ||
			this->importFiles[this->selectedFiles[i]].d.bHasRPCInImageFile) // only when available
			this->importFiles[this->selectedFiles[i]].d.bImportRPC = clickedSetting;

}

void Bui_GDALImportDlg::cbTFWReleased()
{
	if ( this->selectedFiles.size() < 1)
		return;


	// remember the clicked setting and apply to the rest
	bool clickedSetting = false;

	if (this->cb_TFW->isChecked())
	{
		this->importFiles[this->selectedFiles[0]].d.bImportTFW = true;
		clickedSetting = true;
	}
	else
	{
		this->importFiles[this->selectedFiles[0]].d.bImportTFW = false;
		clickedSetting = false;
	}

	for (unsigned int i= 1; i < this->selectedFiles.size(); i++) 
		if (this->importFiles[this->selectedFiles[i]].d.bHasTFWInExternalFile ||
			this->importFiles[this->selectedFiles[i]].d.bHasTFWInImageFile) // only when available
			this->importFiles[this->selectedFiles[i]].d.bImportTFW = clickedSetting;
}


void Bui_GDALImportDlg::rbImageFileReleased()
{
	if ( this->selectedFiles.size() < 1)
		return;

	if (this->importFiles[this->selectedFiles[0]].d.bHasTFWInImageFile)
	{
		this->importFiles[this->selectedFiles[0]].d.tfwSource = CImageImportData::eImageFile;

	}


	for (unsigned int i= 0; i < this->selectedFiles.size(); i++) 
	{
		if (this->importFiles[this->selectedFiles[i]].d.bHasTFWInImageFile)
		{
			this->importFiles[this->selectedFiles[i]].d.tfwSource = CImageImportData::eImageFile;
		}
	}


}
void Bui_GDALImportDlg::rbExternalFileReleased()
{
	if ( this->selectedFiles.size() < 1)
		return;

	if (this->importFiles[this->selectedFiles[0]].d.bHasTFWInExternalFile)
	{
		this->importFiles[this->selectedFiles[0]].d.tfwSource = CImageImportData::eExternalFile;

	}

	for (unsigned int i= 0; i < this->selectedFiles.size(); i++) 
	{
		if (this->importFiles[this->selectedFiles[i]].d.bHasTFWInExternalFile)
		{
			this->importFiles[this->selectedFiles[i]].d.tfwSource = CImageImportData::eExternalFile;
		}
	}
}



void Bui_GDALImportDlg::twFilesItemSelectionChanged()
{

	QList<QTableWidgetItem*> items = this->tw_Files->selectedItems();

	this->selectedFiles.clear();
	
	this->selectedFiles.resize(items.count());

	for (int i = 0; i < items.count(); i++)
	{
		this->selectedFiles[i] = this->tw_Files->getIndexOfItem(items[i]).row();
	}

	if (this->selectedFiles.size() > 0)
		this->applySettings();
}
	
	
void Bui_GDALImportDlg::getFileInformation(ImportFile& element)
{
	if (!element.d.imagefile)
		return;
	
	// rpc
	this->determineRPCFiles(element);

	// tfw
	this->determineTFWFiles(element);
	
	// pushBroom
	this->determinePushBroomFiles(element);
}




void Bui_GDALImportDlg::determineRPCFiles(ImportFile& element)
{
	CFileName filename_without_ext(element.d.filename.GetPath() + CSystemUtilities::dirDelimStr + 
					element.d.filename.GetFileName());

	vector<const char *> list;
	list.push_back(".RPB");
	list.push_back("_rpc.txt");
	list.push_back(".rpc");

	for (unsigned int i= 0; i < list.size(); i++)
	{
		CCharString name = filename_without_ext + list[i];
		ifstream in(name.GetChar());
		if (in.good())
			element.extRPC.append(QString(name.GetChar()));
		in.close();
	}

	// select the first
	if (element.extRPC.size() > 0)
	{
		element.selectedExtRPC = 0;
		element.d.rpcFilename = element.extRPC[0].toLatin1().constData();
		element.d.bHasRPCInExternalFile = true;
		element.d.rpcSource = CImageImportData::eExternalFile;
	}
}

void Bui_GDALImportDlg::determineTFWFiles(ImportFile& element)
{
	if (!element.d.imagefile ) return;

	CFileName filename_without_ext(element.d.filename.GetPath() + CSystemUtilities::dirDelimStr + 
					element.d.filename.GetFileName());

	vector<const char *> list;
	
	QStringList tfwList;
	
	if (element.d.filename.GetFileExt().CompareNoCase("tif"))
	{
		list.push_back(".tfw");
		list.push_back(".tiffw");
		list.push_back(".wld");
	}
	else if (element.d.filename.GetFileExt().CompareNoCase("jpg"))
	{
		list.push_back(".jgw");
		list.push_back(".wld");
	}
	else if (element.d.filename.GetFileExt().CompareNoCase("gif"))
	{
		list.push_back(".wld");
	}
	else if (element.d.filename.GetFileExt().CompareNoCase("ecw"))
	{
		list.push_back(".wld");
	}
	else if (element.d.filename.GetFileExt().CompareNoCase("jp2"))
	{
		list.push_back(".wld");
	}
	else if (element.d.filename.GetFileExt().CompareNoCase("ceos"))
	{
		list.push_back(".wld");
	}

	for (unsigned int i= 0; i < list.size(); i++)
	{
		CCharString name = filename_without_ext + list[i];
		ifstream in(name.GetChar());
		if (in.good())
			element.extTFW.append(QString(name.GetChar()));
		in.close();
	}

	// check for ALOS files
	if (element.d.imagefile->bands() == 1 && filename_without_ext.Find("IMG-") >= 0 && 
		(filename_without_ext.Find("1B2G_") >=0 || filename_without_ext.Find("1B2R_") >=0))
	{
		element.extTFW.append(filename_without_ext.GetChar());
	}
	
	// select the first
	if (element.extTFW.size() > 0)
	{
		element.selectedExtTFW = 0;
		element.d.bHasTFWInExternalFile = true;
		element.d.tfwFilename = element.extTFW[0].toLatin1().constData();
		element.d.tfwSource = CImageImportData::eExternalFile;
	}

	// we have tfw information in the imagefile
	if (element.d.bHasTFWInImageFile)
	{
		element.d.tfwSource = CImageImportData::eImageFile;
	}

}

void Bui_GDALImportDlg::determinePushBroomFiles(ImportFile& element)
{
	if (!element.d.imagefile ) return;

	CFileName localFileName = element.d.filename;
	// when the file is a huge file remove the .hug extension
	if (element.d.bHugeFile)
	{
		localFileName = element.d.filename.Left(element.d.filename.GetLength() -4);
	}



	// check for spot 5 files
	CCharString name = element.d.filename.GetPath().GetChar() + CSystemUtilities::dirDelimStr + "METADATA.DIM";
	ifstream in(name.GetChar());
	if (in.good())
	{
		element.pushBroomFiles.append(name.GetChar());
		if (element.d.imagefile->bands() > 1)
			element.pushBroomDescription.append(QString("Spot 5 - Multispectral"));
		else
			element.pushBroomDescription.append(QString("Spot 5 - Panchromatic"));

		element.pushBroomType.push_back(Spot5);
	}
	in.close();


	// check for quickbird files
	if (element.d.imagefile->bands() > 1)
	{
		CCharString name = element.d.filename.GetPath().GetChar() + CSystemUtilities::dirDelimStr + "MUL_README.TXT";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("QuickBird - Multispectral"));
			element.pushBroomType.push_back(Quickbird);
		}
		in.close();
	}
	else
	{	CCharString name = element.d.filename.GetPath().GetChar() + CSystemUtilities::dirDelimStr + "PAN_README.TXT";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("QuickBird - Panchromatic"));
			element.pushBroomType.push_back(Quickbird);
		}
		in.close();
	}

	// check for worldview files
	if (element.d.imagefile->bands() > 1) // leave that here for worldview-2 in the future
	{
		CCharString name = element.d.filename.GetPath().GetChar() + CSystemUtilities::dirDelimStr + localFileName.GetFileName() +  "_README.TXT";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("WorldView - Multispectral"));
			element.pushBroomType.push_back(Quickbird);
		}
		in.close();
	}
	else
	{	CCharString name = element.d.filename.GetPath().GetChar() + CSystemUtilities::dirDelimStr  + localFileName.GetFileName() + "_README.TXT";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("WorldView - Panchromatic"));
			element.pushBroomType.push_back(Quickbird);
		}
		in.close();
	}



	// check for ALOS files
	if (element.d.imagefile->bands() == 1 && // only single channels
		(localFileName.Find("IMG-") >= 0 || localFileName.GetFileExt().CompareNoCase("ntf")) && // files in CEOS or NITF format
		(localFileName.Find("O1A__") >=0 || localFileName.Find("O1B1__") >=0)) // level 1A or 1B1
	{
		element.pushBroomFiles.append(localFileName.GetChar());
		element.pushBroomType.push_back(Alos);
		
		// we are only looking for single channels of AVNIR 
		if (localFileName.Find("ALAV2") >=0)
			element.pushBroomDescription.append(QString("ALOS AVNIR-2 - Single Channel"));
		else if (localFileName.Find("ALPSM") >=0)
			element.pushBroomDescription.append(QString("ALOS PRISM - Panchromatic"));
		
	}

	// check for Formosat-2 files
	if (element.d.imagefile->bands() > 1) // leave that here for formosat-2 in the future
	{
		CCharString name = element.d.filename.GetPath().GetChar() + CSystemUtilities::dirDelimStr + localFileName.GetFileName() +  ".DIM";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("FORMOSAT-2 - Multispectral"));
			element.pushBroomType.push_back(Formosat2);
		}
		in.close();
	}
	else
	{	
		CCharString name = element.d.filename.GetPath().GetChar() + CSystemUtilities::dirDelimStr  + localFileName.GetFileName() + ".DIM";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("FORMOSAT-2 - Panchromatic"));
			element.pushBroomType.push_back(Formosat2);
		}
		in.close();
	}

	// check for THEOS files
	if (element.d.imagefile->bands() > 1) 
	{
		CCharString name = element.d.filename.GetPath().GetChar() + CSystemUtilities::dirDelimStr + "METADATA.XML";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("THEOS - Multispectral"));
			element.pushBroomType.push_back(Theos);
		}
		in.close();
	}
	else
	{	
		CCharString name = element.d.filename.GetPath().GetChar() + CSystemUtilities::dirDelimStr + "METADATA.XML";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("THEOS - Panchromatic"));
			element.pushBroomType.push_back(Theos);
		}
		in.close();
	}

	// check for GeoEye1 files
	CCharString rmPath = element.d.filename.GetPath();
	if (element.d.imagefile->bands() > 1) 
	{
		for (int i=0; i<2; i++)
		{
			int ii = rmPath.ReverseFind(CSystemUtilities::dirDelim);
			if (ii==-1) break;
			rmPath = rmPath.Left(ii);
		}
		
		CCharString name = rmPath.GetChar() + CSystemUtilities::dirDelimStr + localFileName.GetFileName()+ ".man";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("GeoEye1 - Multispectral"));
			element.pushBroomType.push_back(GeoEye1);
		}
		in.close();
	}
	else
	{	
		for (int i=0; i<2; i++)
		{
			int ii = rmPath.ReverseFind(CSystemUtilities::dirDelim);
			if (ii==-1) break;
			rmPath = rmPath.Left(ii);
		}

		CCharString name = rmPath.GetChar() + CSystemUtilities::dirDelimStr + localFileName.GetFileName()+ ".man";
		ifstream in(name.GetChar());
		if (in.good())
		{
			element.pushBroomFiles.append(name.GetChar());
			element.pushBroomDescription.append(QString("GeoEye1 - Panchromatic"));
			element.pushBroomType.push_back(GeoEye1);
		}
		in.close();
	}
	
	// select the first 
	if (element.pushBroomFiles.size() > 0)
	{
		element.selectedPushBroom = 0;
	}
}


void Bui_GDALImportDlg::checkAvailabilityOfSettings()
{
	
	if (this->importFiles.size() < 1)
	{
		this->pB_Import->setEnabled(false);
		return;
	}
	else 
		this->pB_Import->setEnabled(true);

	if ( !this->selectedFiles.size() || this->selectedFiles.size() > this->importFiles.size() ) return;

	ImportFile element = this->importFiles[this->selectedFiles[0]];

	if (element.d.bHugeFile)
	{
		this->gb_Channels->setEnabled(false);
		this->cb_HugeFile->setEnabled(false);
	}
	else
	{
		this->gb_Channels->setEnabled(true);
		this->cb_HugeFile->setEnabled(true);

		if (element.d.imagefile->bands() == 1)
			this->gb_Channels->setEnabled(false);
		else
			this->gb_Channels->setEnabled(true);

		if (element.d.imagefile->bands() == 3)
			this->lw_alphachannels->setEnabled(false);
		else
			this->lw_alphachannels->setEnabled(true);

	}

	// rpc
	this->combo_RPC->blockSignals(true);
	if (element.extRPC.count() == 0)
	{
		this->combo_RPC->addItem(QString("no RPC file found"));
		this->combo_RPC->setEnabled(false);
	}
	else
	{
		this->combo_RPC->setEnabled(true);
	}
	this->combo_RPC->blockSignals(false);


	if (element.d.bHasRPCInExternalFile || element.d.bHasRPCInImageFile)
		this->cb_RPC->setEnabled(true);
	else
		this->cb_RPC->setEnabled(false);


	// tfw
	this->combo_TFW->blockSignals(true);
	if (element.extTFW.count() == 0)
	{
		this->combo_TFW->addItem(QString("no TFW file found"));
		this->combo_TFW->setEnabled(false);
		this->rb_ExternalFile->setEnabled(false);
	}
	else
	{
		this->combo_TFW->setEnabled(true);
		this->rb_ExternalFile->setEnabled(true);
	}
	this->combo_TFW->blockSignals(false);

	if (element.d.bHasTFWInImageFile)
		this->rb_ImageFile->setEnabled(true);
	else
		this->rb_ImageFile->setEnabled(false);


	if (element.d.bHasTFWInImageFile ||element.d.bHasTFWInExternalFile)
		this->cb_TFW->setEnabled(true);
	else
		this->cb_TFW->setEnabled(false);


	// pushbroom
	if (element.pushBroomFiles.count() == 0)
	{
		this->combo_PushBroom->addItem(QString("no metadata found"));
		this->combo_PushBroom->setEnabled(false);
		this->cB_PushBroom->setEnabled(false);
	}
	else
	{
		this->combo_PushBroom->setEnabled(true);
		this->cB_PushBroom->setEnabled(true);
	}



	if (this->isDEM)
		this->cb_TFW->setEnabled(false);

}


void Bui_GDALImportDlg::cBPushBroomReleased()
{
	if ( this->selectedFiles.size() < 1)
		return;
	
	this->importFiles[this->selectedFiles[0]].d.bImportPushBroom = !this->importFiles[this->selectedFiles[0]].d.bImportPushBroom;
	
	unsigned int index = this->importFiles[this->selectedFiles[0]].selectedPushBroom;
	CFileName pushbroomName = this->importFiles[this->selectedFiles[0]].pushBroomFiles[index].toLatin1().constData();
	SatelliteType satType = this->importFiles[this->selectedFiles[0]].pushBroomType[index];
	
	if (this->importFiles[this->selectedFiles[0]].d.bImportPushBroom)
	{
		this->importFiles[this->selectedFiles[0]].d.pushBroomFilename = pushbroomName;
		this->importFiles[this->selectedFiles[0]].d.satType = satType;
	}

	// remember the clicked setting and apply to the rest
	bool clickedSetting = this->importFiles[this->selectedFiles[0]].d.bImportPushBroom;

	for (unsigned int i= 1; i < this->selectedFiles.size(); i++) 
		if (this->importFiles[this->selectedFiles[i]].pushBroomFiles.size() > 0)
		{
			this->importFiles[this->selectedFiles[i]].d.bImportPushBroom = clickedSetting;
			unsigned int index = this->importFiles[this->selectedFiles[i]].selectedPushBroom;
			CFileName pushbroomName = this->importFiles[this->selectedFiles[i]].pushBroomFiles[index].toLatin1().constData();
			SatelliteType satType = this->importFiles[this->selectedFiles[i]].pushBroomType[index];

			if (clickedSetting)
			{
				this->importFiles[this->selectedFiles[i]].d.pushBroomFilename = pushbroomName;
				this->importFiles[this->selectedFiles[i]].d.satType = satType;
			}

		}
}


void Bui_GDALImportDlg::comboPushBroomCurrentIndexChanged(int index)
{
	if ( this->selectedFiles.size() != 1 || index == -1)
		return;

	this->importFiles[this->selectedFiles[0]].selectedPushBroom = index;
}



void Bui_GDALImportDlg::clearImportFiles()
{
	for (unsigned  int i = 0; i < this->importFiles.size(); i++ )
	{
		if ( this->importFiles[i].d.imagefile )
		{
			delete this->importFiles[i].d.imagefile;
			this->importFiles[i].d.imagefile = NULL;
		}
	}

	this->importFiles.clear();
}



void Bui_GDALImportDlg::NoDataChanged(const QString & text)
{
	float fval = this->importFiles[this->selectedFiles[0]].d.NoDataValue;

	if (this->leNoData->hasAcceptableInput() && !text.isEmpty())
		fval = this->leNoData->text().toDouble();
	else
		this->applySettings();
	
	for (unsigned int i= 0; i < this->selectedFiles.size(); i++) 
	{
		this->importFiles[this->selectedFiles[i]].d.NoDataValue = fval;
	}

}
void Bui_GDALImportDlg::leZ0Changed(const QString & text)
{
	float fval = this->importFiles[this->selectedFiles[0]].d.Z0;

	if (this->leZ0->hasAcceptableInput() && !text.isEmpty())
		fval = this->leZ0->text().toDouble();
	else
		this->applySettings();

	for (unsigned int i= 0; i < this->selectedFiles.size(); i++) 
	{
		this->importFiles[this->selectedFiles[i]].d.Z0 = fval;
	}
}
void Bui_GDALImportDlg::leScaleChanged(const QString & text)
{
	float fval = this->importFiles[this->selectedFiles[0]].d.scale;

	if (this->leScale->hasAcceptableInput() && !text.isEmpty() )
		fval = this->leScale->text().toDouble();
	else
		this->applySettings();

	for (unsigned int i= 0; i < this->selectedFiles.size(); i++) 
	{
		this->importFiles[this->selectedFiles[i]].d.scale = fval;
	}
}

void Bui_GDALImportDlg::leSigmaZChanged(const QString & text)
{
	float fval = this->importFiles[this->selectedFiles[0]].d.sigmaZ;

	if (this->leSigmaZ->hasAcceptableInput() && !text.isEmpty() )
		fval = this->leSigmaZ->text().toDouble();
	else
		this->applySettings();

	
	for (unsigned int i= 0; i < this->selectedFiles.size(); i++) 
	{
		if ( fval > FLT_EPSILON )
			this->importFiles[this->selectedFiles[i]].d.sigmaZ = fval;
	}
}














CPredefinedBandOrders::CPredefinedBandOrders(CCharString name, int bands,int channel1, int channel2, int channel3, int channel4 )
{
	this->name = name;
	this->bands = bands;
	this->channel1 = channel1;
	this->channel2 = channel2;
	this->channel3 = channel3;
	this->channel4 = channel4;

}

CPredefinedBandOrders::CPredefinedBandOrders(const CPredefinedBandOrders& combination)
{
	this->name = combination.name;
	this->bands = combination.bands;
	this->channel1 = combination.channel1;
	this->channel2 = combination.channel2;
	this->channel3 = combination.channel3;
	this->channel4 = combination.channel4;
}

CPredefinedBandOrders&	CPredefinedBandOrders::operator=(const CPredefinedBandOrders& combination)
{
	this->name = combination.name;
	this->bands = combination.bands;
	this->channel1 = combination.channel1;
	this->channel2 = combination.channel2;
	this->channel3 = combination.channel3;
	this->channel4 = combination.channel4;
	return *this;
}
