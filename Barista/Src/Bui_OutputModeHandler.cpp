#include <QtWidgets\QRadioButton>


#include "Bui_OutputModeHandler.h"


//=============================================================================

bui_OutputModeHandler::bui_OutputModeHandler(QRadioButton *OutputLevelMinRadioButton,
		                                     QRadioButton *OutputLevelMedRadioButton,
							                 QRadioButton *OutputLevelMaxRadioButton,
											 QRadioButton *OutputLevelDebugRadioButton):
                       pOutputLevelMinRadioButton(OutputLevelMinRadioButton),
					   pOutputLevelMedRadioButton(OutputLevelMedRadioButton),
					   pOutputLevelMaxRadioButton(OutputLevelMaxRadioButton),
					   pOutputLevelDebugRadioButton(OutputLevelDebugRadioButton)
{
	this->init();
}

//=============================================================================

bui_OutputModeHandler::~bui_OutputModeHandler(void)
{
	this->pOutputLevelMinRadioButton   = 0;
	this->pOutputLevelMedRadioButton   = 0;
	this->pOutputLevelMaxRadioButton   = 0;
	this->pOutputLevelDebugRadioButton = 0;
}
	  
//=============================================================================

void bui_OutputModeHandler::init(eOUTPUTMODE mode)
{
	outputMode = mode;
	this->init();
};

//=============================================================================

void bui_OutputModeHandler::init()
{
	if (this->pOutputLevelMinRadioButton)   this->pOutputLevelMinRadioButton->blockSignals(true);
	if (this->pOutputLevelMedRadioButton)   this->pOutputLevelMedRadioButton->blockSignals(true);
	if (this->pOutputLevelMaxRadioButton)   this->pOutputLevelMaxRadioButton->blockSignals(true);
	if (this->pOutputLevelDebugRadioButton) this->pOutputLevelDebugRadioButton->blockSignals(true);

	if ((outputMode == eDEBUG)   && (!this->pOutputLevelDebugRadioButton)) outputMode = eMAXIMUM;
	if ((outputMode == eMAXIMUM) && (!this->pOutputLevelMaxRadioButton))   outputMode = eMEDIUM;
	if ((outputMode == eMEDIUM) &&  (!this->pOutputLevelMedRadioButton))   outputMode = eMINIMUM;

	if (outputMode == eMINIMUM)
	{
		if (this->pOutputLevelMinRadioButton)   this->pOutputLevelMinRadioButton->setChecked(true);	
		if (this->pOutputLevelMedRadioButton)   this->pOutputLevelMedRadioButton->setChecked(false);	
		if (this->pOutputLevelMaxRadioButton)   this->pOutputLevelMaxRadioButton->setChecked(false);	
		if (this->pOutputLevelDebugRadioButton) this->pOutputLevelDebugRadioButton->setChecked(false);	
	}
	else if (outputMode == eMEDIUM)
	{
		if (this->pOutputLevelMinRadioButton)   this->pOutputLevelMinRadioButton->setChecked(false);	
		if (this->pOutputLevelMedRadioButton)   this->pOutputLevelMedRadioButton->setChecked(true);	
		if (this->pOutputLevelMaxRadioButton)   this->pOutputLevelMaxRadioButton->setChecked(false);	
		if (this->pOutputLevelDebugRadioButton) this->pOutputLevelDebugRadioButton->setChecked(false);	
	}
	else if (outputMode == eMAXIMUM)
	{
		if (this->pOutputLevelMinRadioButton)   this->pOutputLevelMinRadioButton->setChecked(false);	
		if (this->pOutputLevelMedRadioButton)   this->pOutputLevelMedRadioButton->setChecked(false);	
		if (this->pOutputLevelMaxRadioButton)   this->pOutputLevelMaxRadioButton->setChecked(true);	
		if (this->pOutputLevelDebugRadioButton) this->pOutputLevelDebugRadioButton->setChecked(false);	
	}
	else // (outputMode == eDEBUG)
	{
		if (this->pOutputLevelMinRadioButton)   this->pOutputLevelMinRadioButton->setChecked(false);	
		if (this->pOutputLevelMedRadioButton)   this->pOutputLevelMedRadioButton->setChecked(false);	
		if (this->pOutputLevelMaxRadioButton)   this->pOutputLevelMaxRadioButton->setChecked(false);	
		if (this->pOutputLevelDebugRadioButton) this->pOutputLevelDebugRadioButton->setChecked(true);	
	}

	if (this->pOutputLevelMinRadioButton)   this->pOutputLevelMinRadioButton->blockSignals(false);
	if (this->pOutputLevelMedRadioButton)   this->pOutputLevelMedRadioButton->blockSignals(false);
	if (this->pOutputLevelMaxRadioButton)   this->pOutputLevelMaxRadioButton->blockSignals(false);
	if (this->pOutputLevelDebugRadioButton) this->pOutputLevelDebugRadioButton->blockSignals(false);
}

//=============================================================================

void bui_OutputModeHandler::reactOnButtonSelected()
{
	if (this->pOutputLevelMinRadioButton)   this->pOutputLevelMinRadioButton->blockSignals(true);
	if (this->pOutputLevelMedRadioButton)   this->pOutputLevelMedRadioButton->blockSignals(true);
	if (this->pOutputLevelMaxRadioButton)   this->pOutputLevelMaxRadioButton->blockSignals(true);
	if (this->pOutputLevelDebugRadioButton) this->pOutputLevelDebugRadioButton->blockSignals(true);

	if (outputMode == eMINIMUM)
	{
		if (this->pOutputLevelMedRadioButton)
		{
			if (this->pOutputLevelMedRadioButton->isChecked()) outputMode = eMEDIUM;
		}
		if (this->pOutputLevelMaxRadioButton)
		{
			if (this->pOutputLevelMaxRadioButton->isChecked()) outputMode = eMAXIMUM;
		}		
		if (this->pOutputLevelDebugRadioButton)
		{
			if (this->pOutputLevelDebugRadioButton->isChecked()) outputMode = eDEBUG;
		}
	}
	else if (outputMode == eMEDIUM)
	{
		if (pOutputLevelMinRadioButton)
		{
			if (this->pOutputLevelMinRadioButton->isChecked()) outputMode = eMINIMUM;
		}
		if (this->pOutputLevelMaxRadioButton)
		{
			if (this->pOutputLevelMaxRadioButton->isChecked()) outputMode = eMAXIMUM;
		}
		if (this->pOutputLevelDebugRadioButton)
		{
			if (this->pOutputLevelDebugRadioButton->isChecked()) outputMode = eDEBUG;
		}
	}
	else if (outputMode == eMAXIMUM)
	{
		if (pOutputLevelMinRadioButton)
		{
			if (this->pOutputLevelMinRadioButton->isChecked()) outputMode = eMINIMUM;
		}
		if (this->pOutputLevelMedRadioButton)
		{
			if (this->pOutputLevelMedRadioButton->isChecked()) outputMode = eMEDIUM;
		}
		if (this->pOutputLevelDebugRadioButton)
		{
			if (this->pOutputLevelDebugRadioButton->isChecked()) outputMode = eDEBUG;
		}
	}
	else // (outputMode == eDEBUG)
	{
		if (pOutputLevelMinRadioButton)
		{
			if (this->pOutputLevelMinRadioButton->isChecked()) outputMode = eMINIMUM;
		}
		if (this->pOutputLevelMedRadioButton)
		{
			if (this->pOutputLevelMedRadioButton->isChecked()) outputMode = eMEDIUM;
		}
		if (this->pOutputLevelMaxRadioButton)
		{
			if (this->pOutputLevelMaxRadioButton->isChecked()) outputMode = eMAXIMUM;
		}
	}

	this->init();

	if (this->pOutputLevelMinRadioButton)   this->pOutputLevelMinRadioButton->blockSignals(false);
	if (this->pOutputLevelMedRadioButton)   this->pOutputLevelMedRadioButton->blockSignals(false);
	if (this->pOutputLevelMaxRadioButton)   this->pOutputLevelMaxRadioButton->blockSignals(false);
	if (this->pOutputLevelDebugRadioButton) this->pOutputLevelDebugRadioButton->blockSignals(false);
}

//=============================================================================
	  