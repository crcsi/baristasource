#include <float.h> 

#include "Bui_Pansharpen.h"
#include <QFileDialog>
#include <QMessageBox> 
#include <QTimer> 
#include "newmatap.h"

#include "Trans3DFactory.h"
#include "ObjectSelector.h"
#include "GDALFile.h"
#include "IniFile.h"
#include "Icons.h"
#include "Bui_ImageSensorSelector.h"
#include "BaristaHtmlHelp.h"
#include "Bui_GDALExportDlg.h"
#include "ProgressThread.h"
#include "ProgressListener.h"
#include "Bui_ImageRegistrationDialog.h"
#include "Trans2DAffine.h"
#include "VDEM.h"
#include "histogram.h"

bui_PanDialog::bui_PanDialog (bool orthoMode) : multiSelector(0), panSelector(0), infraredCorrection(0.24f), 
								  exportDialog(0), PCA(false), isOrtho(orthoMode), 
								  panImage(0), multiImage(0), pansharpenedimage(0),
								  registerMode(eASPAN), trafo(0), resamplingType(eBicubic),
								  hasPansharpen(false), DEM(0)
{
	setupUi(this);

	connect(this->buttonOk,                SIGNAL(released()),               this, SLOT(ComputePansharpenImage()));
	connect(this->buttonCancel,            SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->HelpButton,              SIGNAL(released()),               this, SLOT(Help()));
	connect(this->PanSelectorCombo,        SIGNAL(currentIndexChanged(int)), this, SLOT(onPanImageChanged()));
	connect(this->MultiSelectorCombo,      SIGNAL(currentIndexChanged(int)), this, SLOT(onMultiImageChanged()));
	connect(this->DEMSelectorCombo,        SIGNAL(currentIndexChanged(int)), this, SLOT(DEMSelected()));

	connect(this->IRCheckbox,              SIGNAL(clicked(bool)),            this, SLOT(onIRbuttonClicked()));
	connect(this->IRpercentage,            SIGNAL(editingFinished()),        this, SLOT(onIRparChanged()));
	connect(this->outputimage,             SIGNAL(released()),               this, SLOT(openOutputImage()));
	connect(this->BicubicButton,           SIGNAL(clicked(bool)),            this, SLOT(interpolationMethodChanged()));
	connect(this->BilinearButton,          SIGNAL(clicked(bool)),            this, SLOT(interpolationMethodChanged()));
	connect(this->AutomaticRegisterButton, SIGNAL(released()),               this, SLOT(automaticRegistration()));
	connect(this->AsPanButton,             SIGNAL(clicked(bool)),            this, SLOT(asPanSelected()));
	connect(this->TFWButton,               SIGNAL(clicked(bool)),            this, SLOT(ownSelected()));
	connect(this->AutomaticButton,         SIGNAL(clicked(bool)),            this, SLOT(automaticSelected()));
	connect(this->orthoGResolution,        SIGNAL(editingFinished()),        this, SLOT(OrthoResChanged()));
	connect(this->orthoLLX,                SIGNAL(editingFinished()),        this, SLOT(OrthoLlcChanged()));
	connect(this->orthoLLY,                SIGNAL(editingFinished()),        this, SLOT(OrthoLlcChanged()));
	connect(this->orthoURX,                SIGNAL(editingFinished()),        this, SLOT(OrthoRucChanged()));
	connect(this->orthoURY,                SIGNAL(editingFinished()),        this, SLOT(OrthoRucChanged()));
	connect(this->PCAButton,               SIGNAL(clicked(bool)),            this, SLOT(algorithmChanged()));
	connect(this->IHSButton,               SIGNAL(clicked(bool)),            this, SLOT(algorithmChanged()));
	connect(this->maxExtentsButton,        SIGNAL(released()),               this, SLOT(initDEMBorders()));

  
	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}

bui_PanDialog ::~bui_PanDialog ()
{
	this->destroy();

	if (this->exportDialog) delete this->exportDialog;
	this->exportDialog = 0;
}

void bui_PanDialog::destroy()
{
    if (this->thread)
        delete this->thread;
    
    this->thread = NULL;

	if (this->pansharpenedimage)
	{
		this->pansharpenedimage->getHugeImage()->removeProgressListener();
		delete this->listener;
		this->listener = 0;
		this->pansharpenedimage = 0;
	}

	if (this->panSelector) delete panSelector;
	this->panSelector = 0;
	if (multiSelector) delete multiSelector;
	this->multiSelector = 0;
	this->panImage = 0;
	this->multiImage = 0;

	if (this->trafo) delete this->trafo;
	this->trafo = 0;

	if (this->timer) delete this->timer;
	this->timer = 0;

	this->DEM = 0;
}

void bui_PanDialog::init()
{
	this->hasPansharpen = false;
	if (this->isOrtho)
	{
		this->orthoParameterGroupBox->setVisible(false);
		this->DEMLabel->setVisible(false);
		this->DEMSelectorCombo->setVisible(false);
		this->panSelector = new bui_ImageSensorSelector(this->PanSelectorCombo, 0, ePANONLY, eTFWONLY);
		this->multiSelector = new bui_ImageSensorSelector(this->MultiSelectorCombo, 0, eMULTIBANDONLY, eTFWONLY);
	}
	else
	{
		this->TFWButton->setText ("Own");
		this->AsPanButton->setVisible(false);
		this->panSelector = new bui_ImageSensorSelector(this->PanSelectorCombo, 0, ePANONLY, eCURRENTNOTFW);
		this->multiSelector = new bui_ImageSensorSelector(this->MultiSelectorCombo, 0, eMULTIBANDONLY, eCURRENTNOTFW);

		CCharString oldDEM = inifile.getOrthoDEM();
	
		int selected = -1;
	
		for (int i = 0; i < project.getNumberOfDEMs(); ++i)
		{
			CVDEM *dem = project.getDEMAt(i);
			this->DEMSelectorCombo->addItem(dem->getFileName()->GetChar());
			if (oldDEM == *(dem->getFileName())) 
				selected = i;
		}

		CVDEM* activeDEM = project.getActiveDEM();
		if ( activeDEM)
		{
			selected = project.getDEMIndex(activeDEM);
		}

		if (project.getNumberOfDEMs() <= 0) this->DEM = 0;
		else
		{
			if (selected < 0) 
			{
				this->DEMSelectorCombo->setCurrentIndex(0);
				this->DEM = project.getDEMAt(0);
			}
			else
			{
				this->DEMSelectorCombo->setCurrentIndex(selected);
				this->DEM = project.getDEMAt(selected);			
			}
			this->DEMSelectorCombo->setToolTip(this->DEM->getFileName()->GetChar());
		}

		this->grid.x = inifile.getOrthoRes();
		if (this->grid.x <= 0) this->grid.x = 1.0f;
		this->grid.y = this->grid.x;

		this->llc.x = inifile.getOrthoMinX();
		this->llc.y = inifile.getOrthoMinY();
		this->ruc.x = inifile.getOrthoMaxX();
		this->ruc.y = inifile.getOrthoMaxY();

		this->setOrthoLimits(true);
	}

	this->Pansharpencreated = false;

    this->thread = NULL;
    this->timer = new QTimer(this);
    this->progressBar->setRange(0, 100);
    this->progressBar->setValue(0);

	this->listener = new CProgressListener();

    this->finished = false;


	this->panImage = this->panSelector->getSelectedImage();
	
	this->multiImage = this->multiSelector->getSelectedImage();

	this->IRCheckbox->blockSignals(true);
	this->IRpercentage->blockSignals(true);

	QString ds=QString("%1").arg(infraredCorrection, 0, 'f', 5);
	this->IRpercentage->setText(ds);

	this->IRCheckbox->setChecked(false);
	this->IRpercentage->setEnabled(false);

	if (this->multiImage)
	{
		if (this->multiImage->getHugeImage()->getBands() < 4) 
		{
			this->IRCheckbox->setEnabled(false);
		}
	}

	this->IRCheckbox->blockSignals(false);
	this->IRpercentage->blockSignals(false);

	this->PCAButton->blockSignals(true);
	this->IHSButton->blockSignals(true);
	
	this->PCAButton->setChecked(false);
	this->IHSButton->setChecked(true);

	this->PCAButton->blockSignals(false);
	this->IHSButton->blockSignals(false);

	this->BicubicButton->blockSignals(true);
	this->BilinearButton->blockSignals(true);

	if (this->resamplingType == eBicubic)
	{
		this->BicubicButton->setChecked(true);
		this->BilinearButton->setChecked(false);
	}
	else
	{
		this->BicubicButton->setChecked(false);
		this->BilinearButton->setChecked(true);
	}
	this->BicubicButton->blockSignals(false);
	this->BilinearButton->blockSignals(false);


    connect( this->timer, SIGNAL(timeout()), this, SLOT(slotTimeout()) );
	this->checkSetup(true);
}
	  
const char *bui_PanDialog::getPansharpFileName() const
{
	return this->pansharpenname.GetChar();
};

void bui_PanDialog::algorithmChanged()
{
	this->PCAButton->blockSignals(true);
	this->IHSButton->blockSignals(true);

	if (!this->PCA)
	{
		this->PCA = true;
		this->PCAButton->setChecked(true);
		this->IHSButton->setChecked(false);
		this->IRCorrectGroup->setEnabled(false);
	}
	else
	{
		this->PCA = false;
		this->PCAButton->setChecked(false);
		this->IHSButton->setChecked(true);
		this->IRCorrectGroup->setEnabled(true);
	}

	this->PCAButton->blockSignals(false);
	this->IHSButton->blockSignals(false);
}

void bui_PanDialog::Cancel()
{
	this->destroy();
	this->close();
}

void bui_PanDialog::onPanImageChanged()
{
	if (!this->panSelector->reactOnImageSelect()) 
	{
		QMessageBox::warning( this, "No image selected", " Pansharpening will be impossible");
	};

	if (this->panSelector->getSelectedImage()) 
	{
		this->panImage = this->panSelector->getSelectedImage();
	}
	else 
	{
		this->panImage = 0;
	}

	this->checkSetup(true);
}


void bui_PanDialog::onMultiImageChanged()
{
  	if (!this->multiSelector->reactOnImageSelect()) 
	{
		QMessageBox::warning( this, "No image selected", " Pansharpening will be impossible");
	};

	if (this->multiSelector->getSelectedImage()) 
	{
		this->multiImage = this->multiSelector->getSelectedImage();

		this->IRCheckbox->blockSignals(true);
		this->IRpercentage->blockSignals(true);

		if (this->multiImage->getHugeImage()->getBands() < 4) 
		{
			this->IRCheckbox->setEnabled(true);
		}

		this->IRCheckbox->blockSignals(false);
		this->IRpercentage->blockSignals(false);
	}
	else 
	{
		this->multiImage = 0;

		this->IRCheckbox->blockSignals(true);
		this->IRpercentage->blockSignals(true);


		this->IRCheckbox->setChecked(false);
		this->IRCheckbox->setEnabled(false);
		this->IRpercentage->setEnabled(false);

		this->IRCheckbox->blockSignals(false);
		this->IRpercentage->blockSignals(false);

	}
	
	this->checkSetup(true);
}
	
void bui_PanDialog::onIRbuttonClicked()
{
	if (this->IRCheckbox->isChecked()) this->IRpercentage->setEnabled(true);
	else this->IRpercentage->setEnabled(false);
};

void bui_PanDialog::onIRparChanged()
{
  	bool OK = false;
	float IR = this->IRpercentage->text().toDouble(&OK);
	if (OK)
	{
		infraredCorrection = IR;
	}

	QString ds=QString("%1").arg(infraredCorrection, 0, 'f', 5);
	this->IRpercentage->setText(ds);
};

void bui_PanDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/Pansharpen.html");
};

void bui_PanDialog::openOutputImage()
{
	if (this->exportDialog) delete this->exportDialog;
	this->exportDialog = NULL;

	CFileName outfile = this->pansharpenedimage->getName();
	this->pansharpenedimage->getHugeImage()->bpp   = this->multiImage->getHugeImage()->getBpp();
	this->pansharpenedimage->getHugeImage()->bands = this->multiImage->getHugeImage()->getBands();

	CTFW tfw;
	if (this->panSelector->getSelectedImage()->hasTFW())
		tfw = *this->panSelector->getSelectedImage()->getTFW();
	else if (this->multiSelector->getSelectedImage()->hasTFW())
		tfw = *this->multiSelector->getSelectedImage()->getTFW();

	if (tfw.gethasValues()) this->pansharpenedimage->setTFW(tfw);
	else if (!this->isOrtho && this->DEM)
	{
		C2DPoint shift(this->llc.x, this->ruc.y);
		tfw = *this->DEM->getTFW();
		tfw.setFromShiftAndPixelSize(shift, this->grid);
		this->pansharpenedimage->setTFW(tfw);
	}

	this->exportDialog = new Bui_GDALExportDlg(outfile,this->pansharpenedimage,false,"Select Output File");
	
	this->exportDialog->exec();
	if (this->exportDialog->getSuccess())
	{
		outfile = this->exportDialog->getFilename();
		this->outputname->setText(outfile.GetChar());
		this->pansharpenname = outfile.GetChar();
		this->pansharpenedimage->setName(outfile);

		this->hasPansharpen = true;
		this->checkSetup(false);
	}
}

void bui_PanDialog::ComputePansharpenImage()
{
	this->buttonOk->setEnabled(false);
	this->update();

	if ((this->multiImage->getHugeImage()->bands < 3)  || (this->multiImage->getHugeImage()->bands > 4))
	{
		QMessageBox::warning( this, " Pansharpening impossible", " The multispectral image must have 3 or 4 bands");
	}
	else if (this->panImage->getHugeImage()->bands != 1)
	{
		QMessageBox::warning( this, " Pansharpening impossible", " The panchromatic image must have 1 band");
	}
	else if (this->registerMode == eAUTOMATIC && !this->trafo)
	{
		QMessageBox::warning( this, " Pansharpening impossible", " You have to co-register the multispectral band first");
	}
	else if (this->multiImage->getHugeImage()->bpp != this->panImage->getHugeImage()->bpp)
	{
		QMessageBox::warning( this, " Pansharpening impossible", " The panchromatic and multispectral images must have the same image depth");
	}
	else
	{

		this->buttonCancel->setEnabled(false);

		this->pansharpenedimage->getHugeImage()->setProgressListener(this->listener);
		
		this->thread = new CPansharpenProcessingThread(this);

		thread->start();
		this->timer->start(500);
	}
}

void bui_PanDialog::setPANsharpenedImage(CVImage* outimage)
{
	this->pansharpenedimage = outimage;
}

void bui_PanDialog::exportImage()
{
	if (this->exportDialog)
	{
		this->exportDialog->writeSettings();
		this->exportDialog->writeImage();
	}
};

void bui_PanDialog::slotTimeout()
{
    if (this->finished)
    {
        this->timer->stop();
        return;
    }

    double val = this->listener->getProgress();
	this->progressBar->setValue((int)(100.0*val+0.5));

	if (this->thread != NULL && this->thread->isFinished())
    {
		this->update();
		this->finished = true;

		this->destroy();
		this->close();
	}
}

void bui_PanDialog::checkSetup(bool setRegistrationDefaults)
{
	if (this->panImage  && this->multiImage)
	{
		this->MSregistrationBox->setEnabled(true);

		if (setRegistrationDefaults)
		{
			if (this->isOrtho)
			{
				if ((this->panImage->getHugeImage()->getWidth() == this->multiImage->getHugeImage()->getWidth()) &&
					(this->panImage->getHugeImage()->getHeight() == this->multiImage->getHugeImage()->getHeight()))
					this->registerMode = eASPAN;
				else if (this->multiImage->hasTFW() && this->panImage->hasTFW()) this->registerMode = eOWN;
				else registerMode = eAUTOMATIC;
			}
			else 
			{
				if (this->multiImage->getCurrentSensorModel()) this->registerMode = eOWN;
				else registerMode = eAUTOMATIC;
			}
		}

		if (this->isOrtho)
		{
			if ((this->panImage->getHugeImage()->getWidth() == this->multiImage->getHugeImage()->getWidth()) &&
				(this->panImage->getHugeImage()->getHeight() == this->multiImage->getHugeImage()->getHeight()))
			{
				this->AsPanButton->setEnabled(true);
			}
			else
			{
				this->AsPanButton->setEnabled(false);
			}
		}

		if (this->isOrtho)
		{
			if (this->multiImage->hasTFW() && this->panImage->hasTFW()) this->TFWButton->setEnabled(true);
			else this->TFWButton->setEnabled(false);
		}
		else
		{
			if (this->multiImage->getCurrentSensorModel()) this->TFWButton->setEnabled(true);
			else this->TFWButton->setEnabled(false);
		}

		if (this->registerMode == eASPAN)
		{
			this->ResamplingMethod->setEnabled(false);
			this->AutomaticRegisterButton->setEnabled(false);
			this->AsPanButton->blockSignals(true);
			this->TFWButton->blockSignals(true);
			this->AutomaticButton->blockSignals(true);
			
			this->AsPanButton->setChecked(true);
			this->TFWButton->setChecked(false);
			this->AutomaticButton->setChecked(false);
			
			this->registerMode = eASPAN;

			this->AsPanButton->blockSignals(false);
			this->TFWButton->blockSignals(false);
			this->AutomaticButton->blockSignals(false);
		}
		else if (this->registerMode == eOWN)
		{
			this->ResamplingMethod->setEnabled(true);
			this->AutomaticRegisterButton->setEnabled(false);
			this->AsPanButton->blockSignals(true);
			this->TFWButton->blockSignals(true);
			this->AutomaticButton->blockSignals(true);
			
			this->AsPanButton->setChecked(false);
			this->TFWButton->setChecked(true);
			this->AutomaticButton->setChecked(false);
			
			this->AsPanButton->blockSignals(false);
			this->TFWButton->blockSignals(false);
			this->AutomaticButton->blockSignals(false);
		}
		else
		{
			this->ResamplingMethod->setEnabled(true);
			this->AutomaticRegisterButton->setEnabled(true);
			this->AsPanButton->blockSignals(true);
			this->TFWButton->blockSignals(true);
			this->AutomaticButton->blockSignals(true);
			
			this->AsPanButton->setChecked(false);
			this->TFWButton->setChecked(false);
			this->AutomaticButton->setChecked(true);
			
			this->AsPanButton->blockSignals(false);
			this->TFWButton->blockSignals(false);
			this->AutomaticButton->blockSignals(false);
		}

		if ((this->panImage  && this->multiImage && this->hasPansharpen) && 
			((this->registerMode != eAUTOMATIC) || (this->trafo)))
		{
			if ((!this->isOrtho) && (!this->DEM)) 
				this->buttonOk->setEnabled(false);
			else this->buttonOk->setEnabled(true);
		}
		else
		{
			this->buttonOk->setEnabled(false);
		}
	}
	else
	{
		this->ResamplingMethod->setEnabled(false);
		this->MSregistrationBox->setEnabled(false);
	}
}

void bui_PanDialog::interpolationMethodChanged()
{
	this->BicubicButton->blockSignals(true);
	this->BilinearButton->blockSignals(true);

	if (this->BilinearButton->isChecked())	
	{
		if (this->BicubicButton->isChecked()) this->BicubicButton->setChecked(false);	
		this->resamplingType = eBilinear;
	}
	else
	{
		if (!this->BicubicButton->isChecked()) this->BicubicButton->setChecked(true);
		this->resamplingType = eBicubic;
	};

	this->BilinearButton->setFocus();
	this->BicubicButton->blockSignals(false);
	this->BilinearButton->blockSignals(false);
};

void bui_PanDialog::asPanSelected()
{
	this->AsPanButton->blockSignals(true);
	this->TFWButton->blockSignals(true);
	this->AutomaticButton->blockSignals(true);
	
	this->AsPanButton->setChecked(true);
	this->TFWButton->setChecked(false);
	this->AutomaticButton->setChecked(false);
	
	this->registerMode = eASPAN;

	this->AsPanButton->blockSignals(false);
	this->TFWButton->blockSignals(false);
	this->AutomaticButton->blockSignals(false);

	checkSetup(false);
};

void bui_PanDialog::ownSelected()
{
	this->AsPanButton->blockSignals(true);
	this->TFWButton->blockSignals(true);
	this->AutomaticButton->blockSignals(true);
	
	this->AsPanButton->setChecked(false);
	this->TFWButton->setChecked(true);
	this->AutomaticButton->setChecked(false);
	
	this->registerMode = eOWN;

	this->AsPanButton->blockSignals(false);
	this->TFWButton->blockSignals(false);
	this->AutomaticButton->blockSignals(false);

	checkSetup(false);
};

void bui_PanDialog::automaticSelected()
{
	this->AsPanButton->blockSignals(true);
	this->TFWButton->blockSignals(true);
	this->AutomaticButton->blockSignals(true);
	
	this->AsPanButton->setChecked(false);
	this->TFWButton->setChecked(false);
	this->AutomaticButton->setChecked(true);
	
	this->registerMode = eAUTOMATIC;

	this->AsPanButton->blockSignals(false);
	this->TFWButton->blockSignals(false);
	this->AutomaticButton->blockSignals(false);


	checkSetup(false);
};

bool bui_PanDialog::automaticRegistration()
{
	bool ret = false;
	if (this->panImage && this->multiImage)
	{
		bui_ImageRegistrationDialog imageRegistration(false, this->multiImage, this->panImage);
		imageRegistration.show();
		imageRegistration.exec();
		if ( imageRegistration.isFinished() )
		{
			ret = imageRegistration.isFinished();
			if (ret) this->trafo = imageRegistration.getTrafoClone();
			if (!this->trafo) ret = false;
		}
	}
	return ret;
};

bool bui_PanDialog::panSharpen()
{
	bool ret;

	if (this->isOrtho)
	{
		if (this->registerMode == eOWN)
		{
			CTFW msTFW  = *(this->multiImage->getTFW());
			ret = msTFW.invert();
			if (ret)
			{
				CTFW panTFW = *(this->panImage->getTFW());
				msTFW.concatenate(panTFW);
				Matrix parms;
				msTFW.getAllParameters(parms);
				this->trafo = new CTrans2DAffine(parms);
			}
		}

		if (ret)
		{
			CHugeImage* pansharpen = this->pansharpenedimage->getHugeImage();

			CTFW tfw;
			if (this->panSelector->getSelectedImage()->hasTFW())
				tfw = *this->panSelector->getSelectedImage()->getTFW();
			else if (this->multiSelector->getSelectedImage()->hasTFW())
				tfw = *this->multiSelector->getSelectedImage()->getTFW();

			if (tfw.gethasValues()) this->pansharpenedimage->setTFW(tfw);

			CCharString hug = this->pansharpenname.GetChar();
			hug += ".hug";
			
			pansharpen->setFileName(hug.GetChar());

			if (this->IRCheckbox->isChecked())
				ret = pansharpen->panSharpen(this->multiImage->getHugeImage(), 
											 this->panImage->getHugeImage(), 				
											 this->infraredCorrection, this->PCA,						  	   
											 this->trafo, this->resamplingType);
			else 
				ret = pansharpen->panSharpen(this->multiImage->getHugeImage(), 
											 this->panImage->getHugeImage(), 	  
											 -1, this->PCA,	   
											 this->trafo, this->resamplingType);
			if (ret)
			{
				this->Pansharpencreated = true;

				CTFW tfw = *this->panImage->getTFW();
				if (tfw.gethasValues())
					this->pansharpenedimage->setTFW(tfw);
			}
			else this->Pansharpencreated = false;
		}

	}
	else
	{
		float maxVal = (float) this->multiImage->getHugeImage()->getHistogram(0)->percentile(0.95);

		for (int ibnd = 1; ibnd < 3; ++ibnd)
		{
			float val = (float) this->multiImage->getHugeImage()->getHistogram(ibnd)->percentile(0.95);
			if (val > maxVal) maxVal = val;
		}

		float panFactor = (float) 0.85;

		panFactor = maxVal / (panFactor * this->panImage->getHugeImage()->getHistogram()->percentile(0.95));

		if (panFactor > 1) panFactor = 1;
		else if (panFactor < 0.5f) panFactor = 0.5f;

		double *rotPCA = 0;
		float  *meanValPCA = 0;
		float  PanMean = 0;

		if (PCA)
		{
			SymmetricMatrix Covariance;
			this->multiImage->getHugeImage()->getCovar(Covariance);

			DiagonalMatrix  eigenVal(this->multiImage->getHugeImage()->getBands());   // eigen values
			Matrix          eigenVec(this->multiImage->getHugeImage()->getBands(), 
				                     this->multiImage->getHugeImage()->getBands()); // eigen vectors


			Jacobi(Covariance, eigenVal, eigenVec);     // compute eigen values and vectors

			meanValPCA = new float[this->pansharpenedimage->getHugeImage()->getBands()];

			for (int i = 0; i < this->pansharpenedimage->getHugeImage()->getBands(); ++i)
			{
				meanValPCA[i] = this->multiImage->getHugeImage()->getHistogram(i)->mean();
			}

			PanMean = this->panImage->getHugeImage()->getHistogram()->mean();

			rotPCA = new double [this->pansharpenedimage->getHugeImage()->getBands() * this->pansharpenedimage->getHugeImage()->getBands()];
			for (int ibnd = 0; ibnd < this->pansharpenedimage->getHugeImage()->getBands(); ++ibnd)
			{
				int iPCA = this->pansharpenedimage->getHugeImage()->getBands() * ibnd;
				for (int jbnd = 0; jbnd <  this->pansharpenedimage->getHugeImage()->getBands(); ++jbnd)
				{
					rotPCA[iPCA + jbnd] = eigenVec.element(ibnd, jbnd);
				}
			}
		};

		int orthoCols = (int)((this->ruc.x - this->llc.x)/this->grid.x + 1);
		int orthoRows = (int)((this->ruc.y - this->llc.y)/this->grid.y + 1);

		CCharString hugname(this->pansharpenedimage->getFileNameChar());
		hugname += ".hug";

		this->panImage->getHugeImage()->resetTiles();
		this->multiImage->getHugeImage()->resetTiles();

		ret = this->pansharpenedimage->getHugeImage()->prepareWriting(hugname.GetChar(), orthoCols, orthoRows, 
			                                                          this->multiImage->getHugeImage()->getBpp(), 
																	  this->multiImage->getHugeImage()->getBands());
		if (ret)
		{
			this->pansharpenedimage->getHugeImage()->computeTotalTileCount(orthoRows, orthoCols);

			CTrans3D *trans3DPan = CTrans3DFactory::initTransformation(this->DEM->getReferenceSystem(), this->panImage->getCurrentSensorModel()->getRefSys());
			CTrans3D *trans3DMulti = 0;
			if (this->registerMode == eOWN)
			{
				trans3DMulti = CTrans3DFactory::initTransformation(this->DEM->getReferenceSystem(), this->multiImage->getCurrentSensorModel()->getRefSys());
				if (!trans3DMulti && trans3DPan) 
				{
					delete trans3DPan; 
					trans3DPan = 0;
				}
			}

			if (trans3DPan)
			{
				int panlevel, panfactor, multilevel, multifactor;
				ret = this->panImage->getOptimalPyrLevel(project.getMeanHeight(), this->grid.x, panlevel, panfactor);
				if (ret) ret = this->multiImage->getOptimalPyrLevel(project.getMeanHeight(), this->grid.x, multilevel, multifactor);
				
				if (ret) ret = LoopTiles(trans3DPan, trans3DMulti, panFactor, rotPCA, meanValPCA, 
							             panlevel, multilevel, panfactor, multifactor);

				this->pansharpenedimage->getHugeImage()->resetTiles();
				this->multiImage->getHugeImage()->resetTiles();

				delete trans3DPan;
				if (trans3DMulti) delete trans3DMulti;

				ret = this->pansharpenedimage->getHugeImage()->finishWriting();
			}
			else ret = false;
		}
		if (rotPCA)     delete [] rotPCA;
		if (meanValPCA) delete [] meanValPCA;
		this->Pansharpencreated = ret;
	}
	return ret;
};

bool bui_PanDialog::LoopTiles(CTrans3D *transPan, CTrans3D *transMulti, float panFactor, double *rotPCA, float *meanValPCA, 
							  int panLevel, int multiLevel, int panFact, int multiFact)
{
	bool ret = true;
	C2DPoint demPt;

	float  PanMean = this->panImage->getHugeImage()->getHistogram()->mean();

	int nBands = this->pansharpenedimage->getHugeImage()->getBands();
	int Bpp = this->pansharpenedimage->getHugeImage()->getBpp();

	int tilewidth = this->pansharpenedimage->getHugeImage()->getTileWidth();
	int tileheight = this->pansharpenedimage->getHugeImage()->getTileHeight();

	int tilesdown = this->pansharpenedimage->getHugeImage()->tilesDown;
	int tilesacross = this->pansharpenedimage->getHugeImage()->tilesAcross;

	double nTiles = tilesdown*tilesacross;

	CImageBuffer orthoBufPan(0,0,tilewidth, tileheight, 1, Bpp, true);
	CImageBuffer orthoBufMS(0,0,tilewidth, tileheight, nBands, Bpp, true);

	unsigned char  *tilebufferUchrPan = 0;
	unsigned short *tilebufferShrtPan = 0;
	unsigned char  *tilebufferUchrMS = 0;
	unsigned short *tilebufferShrtMS = 0;

	if (Bpp == 8)  
	{
		tilebufferUchrPan = orthoBufPan.getPixUC();
		tilebufferUchrMS  = orthoBufMS.getPixUC();
	}
	else // (Bpp == 16) 
	{
		tilebufferShrtPan = orthoBufPan.getPixUS(); 	
		tilebufferShrtMS  = orthoBufMS.getPixUS(); 	
	}

	double *panX = new double[tilewidth*tileheight];
	double *panY = new double[tilewidth*tileheight];
	double *msX = new double[tilewidth*tileheight];
	double *msY = new double[tilewidth*tileheight];

	for ( int tr = 0; tr < tilesdown; tr++ )
	{
		for ( int tc = 0; tc < tilesacross; tc++ )
		{
			orthoBufPan.setOffset(tc * tilewidth, tr * tileheight);
			orthoBufMS.setOffset(tc * tilewidth, tr * tileheight);

			demPt.x = this->llc.x + tc * tilewidth * this->grid.x;
			demPt.y = this->ruc.y - tr * tileheight * this->grid.y;

			double PanXmin, PanXmax, PanYmin, PanYmax, MsXmin, MsXmax, MSYmin, MSYmax; 

			bool ok = computeImgCoordsForTile(demPt, panX, panY, msX, msY, panFact, multiFact,
										      transPan, transMulti, PanXmin, PanXmax, PanYmin, PanYmax,
											  MsXmin, MsXmax, MSYmin, MSYmax);

			if (ok)
			{
				CImageBuffer Panbuf(long(PanXmin), long(PanYmin), long(PanXmax - PanXmin), long(PanYmax - PanYmin), 1, Bpp, true);
				CImageBuffer MSbuf(long(MsXmin), long(MSYmin), long(MsXmax - MsXmin), long(MSYmax - MSYmin), nBands, Bpp, true);
				ok  = this->panImage->getHugeImage()->getRect(Panbuf, panLevel);
				ok &= this->multiImage->getHugeImage()->getRect(MSbuf, multiLevel);

				if (ok)
				{
					if (Bpp == 8)
					{
						if (this->resamplingType == eBicubic) 
						{
							Ortho8bitTileBicub(tilebufferUchrPan, panX, panY, Panbuf);	
							Ortho8bitTileBicub(tilebufferUchrMS,  msX,  msY,  MSbuf);	
						}
						else 
						{
							Ortho8bitTileBilin(tilebufferUchrPan, panX, panY, Panbuf);	
							Ortho8bitTileBilin(tilebufferUchrMS,  msX,  msY,  MSbuf);	
						}
					}
					else
					{
						if (this->resamplingType == eBicubic) 
						{
							Ortho16bitTileBicub(tilebufferShrtPan, panX, panY, Panbuf);	
							Ortho16bitTileBicub(tilebufferShrtMS,  msX,  msY,  MSbuf);	
						}
						else 
						{
							Ortho16bitTileBilin(tilebufferShrtPan, panX, panY, Panbuf);	
							Ortho16bitTileBilin(tilebufferShrtMS,  msX,  msY,  MSbuf);	
						}
					}

					if (PCA) ok = orthoBufMS.pansharpen(orthoBufPan, rotPCA, meanValPCA, PanMean);
					else ok = orthoBufMS.pansharpen1(orthoBufPan, this->infraredCorrection, panFactor);
				}
			};

			if (!ok) orthoBufMS.setAll(0);

			this->pansharpenedimage->getHugeImage()->dumpTile(orthoBufMS);

			// update value for progress bar
			if ( this->listener != NULL )
			{
				double done = tr * tilesacross + tc + 1;
				double pvalue = done/nTiles;
				this->listener->setProgressValue(pvalue);
			}

		}
	}

	this->listener->setProgressValue(1.0);

	return ret;
};

void bui_PanDialog::OrthoResChanged()
{
	bool OK = true; 
    double gsd = orthoGResolution->text().toDouble(&OK);

	if (OK && gsd > 0.0)
	{
		this->grid.x = this->grid.y = gsd;

		C2DPoint Delta = this->ruc - this->llc;
		int i = (int) floor (Delta.x / this->grid.x);
		int j = (int) floor (Delta.y / this->grid.y);
		if (fabs(Delta.x - i) > FLT_EPSILON)
			this->ruc.x = this->llc.x + double (i + 1) * this->grid.x;

		if (fabs(Delta.y - j) > FLT_EPSILON)
			this->ruc.y = this->llc.y + double (j + 1) * this->grid.y;
	}

	this->setOrthoLimits(false);
};

void bui_PanDialog::OrthoLlcChanged()
{
	bool OK = true; 
    double llcx, llcy;
	llcx = orthoLLX->text().toDouble(&OK);
	if (OK) llcy = orthoLLY->text().toDouble(&OK);

	if (OK)
	{
		this->llc.x = llcx;
		this->llc.y = llcy;
		C2DPoint Delta = this->ruc - this->llc;
		int i = (int) floor (Delta.x / this->grid.x);
		int j = (int) floor (Delta.y / this->grid.y);
		if (fabs(Delta.x - i) > FLT_EPSILON)
			this->ruc.x = this->llc.x + double (i + 1) * this->grid.x;

		if (fabs(Delta.y - j) > FLT_EPSILON)
			this->ruc.y = this->llc.y + double (j + 1) * this->grid.y;

		if (this->ruc.x <= this->llc.x) this->ruc.x = this->llc.x + this->grid.x;
		if (this->ruc.y <= this->llc.y) this->ruc.y = this->llc.y + this->grid.y;
	}

	this->setOrthoLimits(false);
};

void bui_PanDialog::OrthoRucChanged()
{
	bool OK = true; 
    double urcx, urcy;
	urcx = orthoURX->text().toDouble(&OK);
	if (OK) urcy = orthoURY->text().toDouble(&OK);

	if (OK)
	{
		this->ruc.x = urcx;
		this->ruc.y = urcy;
		C2DPoint Delta = this->ruc - this->llc;
		int i = (int) floor (Delta.x / this->grid.x);
		int j = (int) floor (Delta.y / this->grid.y);
		if (fabs(Delta.x - i) > FLT_EPSILON)
			this->llc.x = this->ruc.x - double (i + 1) * this->grid.x;

		if (fabs(Delta.y - j) > FLT_EPSILON)
			this->llc.y = this->ruc.y - double (j + 1) * this->grid.y;
	}

	if (this->ruc.x <= this->llc.x) this->llc.x = this->ruc.x - this->grid.x;
	if (this->ruc.y <= this->llc.y) this->llc.y = this->ruc.y - this->grid.y;

	this->setOrthoLimits(false);
};

void bui_PanDialog::setOrthoLimits(bool checkDEM)
{
	if (checkDEM && this->DEM)
	{
		if (this->llc.x < this->DEM->getDEM()->getminX())
			this->llc.x = this->DEM->getDEM()->getminX();

		if (this->llc.x >= this->DEM->getDEM()->getmaxX())
			this->llc.x = this->DEM->getDEM()->getmaxX() - this->grid.x;

		if (this->llc.y < this->DEM->getDEM()->getminY())
			this->llc.y = this->DEM->getDEM()->getminY();

		if (this->llc.y >= this->DEM->getDEM()->getmaxY())
			this->llc.y = this->DEM->getDEM()->getmaxY() - this->grid.y;

		if (this->ruc.x <= this->DEM->getDEM()->getminX())
			this->ruc.x = this->DEM->getDEM()->getminX() + this->grid.x;

		if (this->ruc.x > this->DEM->getDEM()->getmaxX())
			this->ruc.x = this->DEM->getDEM()->getmaxX();

		if (this->ruc.y <= this->DEM->getDEM()->getminY())
			this->ruc.y = this->DEM->getDEM()->getminY() + this->grid.y;

		if (this->ruc.y > this->DEM->getDEM()->getmaxY())
			this->ruc.y = this->DEM->getDEM()->getmaxY();

		C2DPoint Delta = this->ruc - this->llc;
		int i = (int) floor (Delta.x / this->grid.x);
		int j = (int) floor (Delta.y / this->grid.y);
		if (fabs(Delta.x - i) > FLT_EPSILON)
			this->ruc.x = this->llc.x + double (i + 1) * this->grid.x;

		if (fabs(Delta.y - j) > FLT_EPSILON)
			this->ruc.y = this->llc.y + double (j + 1) * this->grid.y;

		if (this->ruc.x <= this->llc.x) this->ruc.x = this->llc.x + this->grid.x;
		if (this->ruc.y <= this->llc.y) this->ruc.y = this->llc.y + this->grid.y;
	}

	this->orthoLLX->blockSignals(true);
	this->orthoLLY->blockSignals(true);
	this->orthoURX->blockSignals(true);
	this->orthoURY->blockSignals(true);
	this->orthoGResolution->blockSignals(true);

	QString ds=QString("%1").arg( this->llc.x, 0, 'f', 3);
	this->orthoLLX->setText(ds);
	ds=QString("%1").arg( this->ruc.x, 0, 'f', 3);
	this->orthoURX->setText(ds);
	ds=QString("%1").arg( this->llc.y, 0, 'f', 3);
	this->orthoLLY->setText(ds);
	ds=QString("%1").arg( this->ruc.y, 0, 'f', 3);
	this->orthoURY->setText(ds);
	ds=QString("%1").arg(this->grid.x, 0, 'f', 3);
	this->orthoGResolution->setText(ds);

	this->orthoLLX->blockSignals(false);
	this->orthoLLY->blockSignals(false);
	this->orthoURX->blockSignals(false);
	this->orthoURY->blockSignals(false);
	this->orthoGResolution->blockSignals(false);
}

void bui_PanDialog::DEMSelected()
{
	int indexSelected = (this->DEMSelectorCombo->currentIndex());

	if ((indexSelected >= 0) && (indexSelected < project.getNumberOfDEMs()))
	{
		this->DEM = project.getDEMAt(indexSelected);

		if (inifile.getOrthoDEM() == *(this->DEM->getFileName()))
		{
			this->llc.x = inifile.getOrthoMinX();
			this->llc.y = inifile.getOrthoMinY();
			this->ruc.x = inifile.getOrthoMaxX();
			this->ruc.y = inifile.getOrthoMaxY();
            this->setOrthoLimits(true);
		}
		else this->setOrthoLimits(false);

		this->DEMSelectorCombo->setToolTip(this->DEM->getFileName()->GetChar());
	}
	else this->DEM = 0;

	this->checkSetup(false);
}


void bui_PanDialog::initDEMBorders()
{    
	this->llc.x = this->DEM->getDEM()->getminX();
	this->llc.y = this->DEM->getDEM()->getminY();

	this->ruc.x = this->DEM->getDEM()->getmaxX();
	this->ruc.y = this->DEM->getDEM()->getmaxY();
	
	C2DPoint Delta = this->ruc - this->llc;
	int i = (int) floor (Delta.x / this->grid.x);
	int j = (int) floor (Delta.y / this->grid.y);
	if (fabs(Delta.x - i) > FLT_EPSILON)
		this->ruc.x = this->llc.x + double (i + 1) * this->grid.x;

	if (fabs(Delta.y - j) > FLT_EPSILON)
		this->ruc.y = this->llc.y + double (j + 1) * this->grid.y;

	setOrthoLimits(false);
}

bool bui_PanDialog::computeImgCoordsForTile(C2DPoint &firstDEMPoint, double *xPanArray, double *yPanArray, 
											double *xMSArray, double *yMSArray, int PanFact, int MsFact,
											CTrans3D *Pantrafo, CTrans3D *MStrafo,
											double &xminPan, double &xmaxPan, double &yminPan, double &ymaxPan,
											double &xminMS, double &xmaxMS,double &yminMS, double &ymaxMS)
{
	xminPan = yminPan = xminMS = yminMS =  FLT_MAX;
	xmaxPan = ymaxPan = xmaxMS = ymaxMS = -FLT_MAX;

	double coordFactorPan = 1.0 / double(PanFact);
	double coordFactorMS  = 1.0 / double(MsFact);

	int tilewidth = this->pansharpenedimage->getHugeImage()->getTileWidth();
	int tileheight = this->pansharpenedimage->getHugeImage()->getTileHeight();
	long indexMax = tilewidth * tileheight;

	C2DPoint imgPointPan, imgPointMS;
	C3DPoint demPoint, 	geoPoint;
	demPoint.y = firstDEMPoint.y;
    int iOK = 0;

	if (this->registerMode == eOWN)
	{
		// process the current tile for orthoimage	
		for ( long index0 = 0; index0 < indexMax; index0 += tilewidth, demPoint.y -= this->grid.y)
		{		
			demPoint.x  = firstDEMPoint.x;
			int indexRowMax = index0 + tilewidth;
			for ( int index = index0; index < indexRowMax; ++index, demPoint.x += this->grid.x)
			{
				// get XYZ of full ortho image pixel
				demPoint.z = this->DEM->getDEM()->getBLInterpolatedHeight(demPoint.x, demPoint.y);

				if ( demPoint.z != -9999.0)
				{
					// transform dempoint to sensor reference frame			
					Pantrafo->transform(geoPoint, demPoint);
					
					// reproject to sensor frame
					this->panImage->getCurrentSensorModel()->transformObj2Obs(imgPointPan, geoPoint);
					MStrafo->transform(geoPoint, demPoint);
					this->multiImage->getCurrentSensorModel()->transformObj2Obs(imgPointMS, geoPoint);

					
					if ((imgPointPan.y > 0) && (imgPointPan.y < this->panImage->getHugeImage()->getHeight()) &&	
						(imgPointPan.x > 0) && (imgPointPan.x < this->panImage->getHugeImage()->getWidth())  &&
						(imgPointMS.y > 0)  && (imgPointMS.y < this->multiImage->getHugeImage()->getHeight()) &&	
						(imgPointMS.x > 0)  && (imgPointMS.x < this->multiImage->getHugeImage()->getWidth()))
					{  // interpolate pixel value for ortho image
						imgPointPan *= coordFactorPan;
						imgPointMS  *= coordFactorMS;
						xPanArray[index] = imgPointPan.x;
						yPanArray[index] = imgPointPan.y;
						xMSArray[index]  = imgPointMS.x;
						yMSArray[index]  = imgPointMS.y;
						if (imgPointPan.x > xmaxPan) xmaxPan = imgPointPan.x;
						if (imgPointPan.x < xminPan) xminPan = imgPointPan.x;
						if (imgPointPan.y > ymaxPan) ymaxPan = imgPointPan.y;
						if (imgPointPan.y < yminPan) yminPan = imgPointPan.y;
						if (imgPointMS.x  > xmaxMS)  xmaxMS  = imgPointMS.x;
						if (imgPointMS.x  < xminMS)  xminMS  = imgPointMS.x;
						if (imgPointMS.y  > ymaxMS)  ymaxMS  = imgPointMS.y;
						if (imgPointMS.y  < yminMS)  yminMS  = imgPointMS.y;
						iOK++;
					}
					else 
					{  // re-projected point is outside the image
						xPanArray[index] = -1;
						yPanArray[index] = -1;
						xMSArray[index]  = -1;
						yMSArray[index]  = -1;
					}
				}
				else 
				{ // no DEM heigth available
					xPanArray[index] = -1;
					yPanArray[index] = -1;
					xMSArray[index]  = -1;
					yMSArray[index]  = -1;
				}	
			}
		}
	}
	else
	{
		// process the current tile for orthoimage	
		for ( long index0 = 0; index0 < indexMax; index0 += tilewidth, demPoint.y -= this->grid.y)
		{		
			demPoint.x  = firstDEMPoint.x;
			int indexRowMax = index0 + tilewidth;
			for ( int index = index0; index < indexRowMax; ++index, demPoint.x += this->grid.x)
			{
				// get XYZ of full ortho image pixel
				demPoint.z = this->DEM->getDEM()->getBLInterpolatedHeight(demPoint.x, demPoint.y);

				if ( demPoint.z != -9999.0)
				{
					Pantrafo->transform(geoPoint, demPoint);
					
					// reproject to sensor frame
					this->panImage->getCurrentSensorModel()->transformObj2Obs(imgPointPan, geoPoint);

					this->trafo->transform(imgPointMS,imgPointPan);

					if ((imgPointPan.y > 0) && (imgPointPan.y < this->panImage->getHugeImage()->getHeight()) &&	
						(imgPointPan.x > 0) && (imgPointPan.x < this->panImage->getHugeImage()->getWidth())  &&
						(imgPointMS.y > 0)  && (imgPointMS.y < this->multiImage->getHugeImage()->getHeight()) &&	
						(imgPointMS.x > 0)  && (imgPointMS.x < this->multiImage->getHugeImage()->getWidth()))
					{  // interpolate pixel value for ortho image
						imgPointPan *= coordFactorPan;
						imgPointMS  *= coordFactorMS;
						xPanArray[index] = imgPointPan.x;
						yPanArray[index] = imgPointPan.y;
						xMSArray[index]  = imgPointMS.x;
						yMSArray[index]  = imgPointMS.y;
						if (imgPointPan.x > xmaxPan) xmaxPan = imgPointPan.x;
						if (imgPointPan.x < xminPan) xminPan = imgPointPan.x;
						if (imgPointPan.y > ymaxPan) ymaxPan = imgPointPan.y;
						if (imgPointPan.y < yminPan) yminPan = imgPointPan.y;
						if (imgPointMS.x  > xmaxMS)  xmaxMS  = imgPointMS.x;
						if (imgPointMS.x  < xminMS)  xminMS  = imgPointMS.x;
						if (imgPointMS.y  > ymaxMS)  ymaxMS  = imgPointMS.y;
						if (imgPointMS.y  < yminMS)  yminMS  = imgPointMS.y;
						iOK++;				
					}
					else
					{  // re-projected point is outside the image
						xPanArray[index] = -1;
						yPanArray[index] = -1;
						xMSArray[index]  = -1;
						yMSArray[index]  = -1;
					}
				}
				else 
				{ // no DEM heigth available
					xPanArray[index] = -1;
					yPanArray[index] = -1;
					xMSArray[index]  = -1;
					yMSArray[index]  = -1;
				}	
			}
		}
	}

	xminPan = floor(xminPan) - 7.0;
	xmaxPan = floor(xmaxPan) + 7.0;
	yminPan = floor(yminPan) - 7.0;
	ymaxPan = floor(ymaxPan) + 7.0;
	if (xminPan < 0) xminPan = 0.0;
	if (yminPan < 0) yminPan = 0.0;
	if (xmaxPan >= this->panImage->getHugeImage()->getWidth()) 
		xmaxPan = this->panImage->getHugeImage()->getWidth() - 1;
	if (ymaxPan >= this->panImage->getHugeImage()->getHeight()) 
		ymaxPan = this->panImage->getHugeImage()->getHeight() - 1;		

	xminMS = floor(xminMS) - 7.0;
	xmaxMS = floor(xmaxMS) + 7.0;
	yminMS = floor(yminMS) - 7.0;
	ymaxMS = floor(ymaxMS) + 7.0;
	if (xminMS < 0) xminMS = 0.0;
	if (yminMS < 0) yminMS = 0.0;
	if (xmaxMS >= this->multiImage->getHugeImage()->getWidth()) 
		xmaxMS = this->multiImage->getHugeImage()->getWidth() - 1;
	if (ymaxMS >= this->multiImage->getHugeImage()->getHeight()) 
		ymaxMS = this->multiImage->getHugeImage()->getHeight() - 1;		


	long dxMS  = (long) (xmaxMS  - xminMS);
	long dyMS  = (long) (ymaxMS  - yminMS);
	long dxPan = (long) (xmaxPan - xminPan);
	long dyPan = (long) (ymaxPan - yminPan);
	bool ret = ((iOK > 1) && (dxMS > 0) && (dyMS > 0) && (dxPan > 0) && (dyPan > 0));
	return ret;
};


void bui_PanDialog::Ortho8bitTileBilin(unsigned char *orthoBuf, 
									   double *xArray, double *yArray, 
									   const CImageBuffer &imgBuf)
{
	C2DPoint p;

	long coordIdx = 0;
	int diffX = imgBuf.getDiffX();

	int tilewidth = this->pansharpenedimage->getHugeImage()->getTileWidth();
	int tileheight = this->pansharpenedimage->getHugeImage()->getTileHeight();

	long iMax = tilewidth * tileheight * diffX;

	memset(orthoBuf, 0, iMax * sizeof(unsigned char));

	for (long imgIdx = 0; imgIdx < iMax; imgIdx += diffX, ++coordIdx)
	{
		if (xArray[coordIdx] >= 0)
		{
			p.x = xArray[coordIdx];
			p.y = yArray[coordIdx];
			imgBuf.getInterpolatedColourVecBilin(p, orthoBuf + imgIdx);
		};
	}
}

void bui_PanDialog::Ortho16bitTileBilin(unsigned short *orthoBuf, 
										double *xArray, double *yArray, 
										const CImageBuffer &imgBuf)
{
	C2DPoint p;

	long coordIdx = 0;
	int diffX = imgBuf.getDiffX();

	int tilewidth = this->pansharpenedimage->getHugeImage()->getTileWidth();
	int tileheight = this->pansharpenedimage->getHugeImage()->getTileHeight();

	long iMax = tilewidth * tileheight * diffX;

	memset(orthoBuf, 0, iMax * sizeof(unsigned short));

	for (long imgIdx = 0; imgIdx < iMax; imgIdx += diffX, ++coordIdx)
	{
		if (xArray[coordIdx] >= 0)
		{
			p.x = xArray[coordIdx];
			p.y = yArray[coordIdx];
			imgBuf.getInterpolatedColourVecBilin(p, orthoBuf + imgIdx);
		};
	}
}


void bui_PanDialog::Ortho8bitTileBicub(unsigned char *orthoBuf, 
									   double *xArray, double *yArray, 
									   const CImageBuffer &imgBuf)
{
	C2DPoint p;

	long coordIdx = 0;
	int diffX = imgBuf.getDiffX();

	int tilewidth = this->pansharpenedimage->getHugeImage()->getTileWidth();
	int tileheight = this->pansharpenedimage->getHugeImage()->getTileHeight();

	long iMax = tilewidth * tileheight * diffX;

	memset(orthoBuf, 0, iMax * sizeof(unsigned char));

	for (long imgIdx = 0; imgIdx < iMax; imgIdx += diffX, ++coordIdx)
	{
		if (xArray[coordIdx] >= 0)
		{
			p.x = xArray[coordIdx];
			p.y = yArray[coordIdx];
			imgBuf.getInterpolatedColourVecBicub(p, orthoBuf + imgIdx);
		};
	}
}

void bui_PanDialog::Ortho16bitTileBicub(unsigned short *orthoBuf, 
										double *xArray, double *yArray, 
										const CImageBuffer &imgBuf)
{
	C2DPoint p;

	long coordIdx = 0;
	int diffX = imgBuf.getDiffX();

	int tilewidth = this->pansharpenedimage->getHugeImage()->getTileWidth();
	int tileheight = this->pansharpenedimage->getHugeImage()->getTileHeight();

	long iMax = tilewidth * tileheight * diffX;

	memset(orthoBuf, 0, iMax * sizeof(unsigned short));

	for (long imgIdx = 0; imgIdx < iMax; imgIdx += diffX, ++coordIdx)
	{
		if (xArray[coordIdx] >= 0)
		{
			p.x = xArray[coordIdx];
			p.y = yArray[coordIdx];
			imgBuf.getInterpolatedColourVecBicub(p, orthoBuf + imgIdx);
		};
	}
}
