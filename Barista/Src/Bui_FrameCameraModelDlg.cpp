#include "Bui_FrameCameraModelDlg.h"

#include "Bui_FrameCameraDlg.h"
#include "Bui_CameraMountingDlg.h"
#include "Bui_FrameCameraDlg.h"

#include "Icons.h"
#include "BaristaProject.h"
#include "FrameCameraAnalogue.h"
#include "RotationBaseFactory.h"
#include "CameraFactory.h"


Bui_FrameCameraModelDlg::Bui_FrameCameraModelDlg(CFrameCameraModel* frameCameraModel,bool viewOnly,QWidget* parent) 
	: CAdjustmentDlg(parent),cameraPos(0,0,0),viewOnly(viewOnly),cameraMountingDlg(0),oldTab(-1),
	  selectedCameraMountingIndex(-1),selectedCameraIndex(-1),cameraDlg(0),
	  existingFrameModel(frameCameraModel)

{
	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->existingFrameModel->addListener(this);

	CCamera* existingCamera = NULL;
	CCameraMounting* existingCameraMounting = NULL;

	if (frameCameraModel->gethasValues())
	{
		existingCamera = frameCameraModel->getCamera();	
		existingCameraMounting = frameCameraModel->getCameraMounting();

		this->cameraPos = frameCameraModel->getCameraPosition();
		if (frameCameraModel->getCameraRotation()->getType() == RPY)
		{
			this->rollPitchYaw = frameCameraModel->getCameraRotation();
			this->rB_RollPitchYaw->setChecked(true);
			this->lB_Angle1->setText("Roll");
			this->lB_Angle2->setText("Pitch");
			this->lB_Angle3->setText("Yaw");
		}
		else 	
		{
			this->omegaPhiKappa = frameCameraModel->getCameraRotation();
			this->rB_OmegaPhiKappa->setChecked(true);
			this->lB_Angle1->setText("Omega");
			this->lB_Angle2->setText("Phi");
			this->lB_Angle3->setText("Kappa");
		}

	}
	else
	{
		this->rB_RollPitchYaw->setChecked(true);
		this->lB_Angle1->setText("Roll");
		this->lB_Angle2->setText("Pitch");
		this->lB_Angle3->setText("Yaw");
	}

	// init the cameras and camera mountings
	int nCameras = project.getNumberOfCameras();
	int nCameraMountings = project.getNumberOfCameraMountings();


	// make a local copy, so that we can change values but still be able to drive back
	for (int i=0; i < nCameras; i++)
	{
		CCamera* c = project.getCameraAt(i);
		if (c->getCameraType() == eDigitalFrame || c->getCameraType() == eAnalogueFrame)
		{
			this->cameras.push_back(CCameraFactory::createCamera(c)); // make local copy
			this->existingCameras.push_back(c); // store reference
			
			if (c == existingCamera) 
				selectedCameraIndex = this->existingCameras.size() -1; // remember the index, so we can select this camera		
		}
	}

	for (int i=0; i<nCameraMountings; i++)
	{
		CCameraMounting* cm = project.getCameraMountingAt(i);
		this->cameraMountings.push_back(*cm);  // local copy
		this->existingCameraMountings.push_back(cm); // reference

		if (cm == existingCameraMounting)
			selectedCameraMountingIndex = this->existingCameraMountings.size()-1; // remember the index, so we can select this camera mounting

	}

	// add a default mounting
	if (this->cameraMountings.size() == 0)
	{
		this->cameraMountings.push_back(CCameraMounting("Default camera mouning"));
		selectedCameraMountingIndex = 0;
	}

	// fill the comboboxes 
	this->updateCameras();
	this->updateMountings();

	// select 'new camera' or the existing camera and camera mounting
	this->selectCamera(selectedCameraIndex+1);
	this->selectCameraMounting(selectedCameraMountingIndex+1);

	
	if (this->viewOnly)
	{
		this->gB_Interior->setVisible(false);
	}


	this->connect(this->pB_Camera, SIGNAL(released()),this,SLOT(pbCameraReleased()));
	this->connect(this->pB_CameraMounting, SIGNAL(released()),this,SLOT(pbCamaraMountingReleased()));

	this->connect(this->lE_X,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Y,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Z,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Angle1,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Angle2,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Angle3,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));

	this->connect(this->lE_X,SIGNAL(editingFinished()),this,SLOT(leXEditingFinished()));
	this->connect(this->lE_Y,SIGNAL(editingFinished()),this,SLOT(leYEditingFinished()));
	this->connect(this->lE_Z,SIGNAL(editingFinished()),this,SLOT(leZEditingFinished()));
	this->connect(this->lE_Angle1,SIGNAL(editingFinished()),this,SLOT(leAngle1EditingFinished()));
	this->connect(this->lE_Angle2,SIGNAL(editingFinished()),this,SLOT(leAngle2EditingFinished()));
	this->connect(this->lE_Angle3,SIGNAL(editingFinished()),this,SLOT(leAngle3EditingFinished()));

	this->connect(this->rB_RollPitchYaw,SIGNAL(released()),this,SLOT(rbRollPitchYawReleased()));
	this->connect(this->rB_OmegaPhiKappa,SIGNAL(released()),this,SLOT(rbOmegaPhiKappaReleased()));	

	this->connect(this->cB_Camera,SIGNAL(currentIndexChanged(int)),this,SLOT(cBCameraCurrentIndexChanged(int)));
	this->connect(this->cB_CameraMounting,SIGNAL(currentIndexChanged(int)),this,SLOT(cBCameraMountingCurrentIndexChanged(int)));

	this->lE_X->setValidator(new QDoubleValidator(this));	
	this->lE_Y->setValidator(new QDoubleValidator(this));	
	this->lE_Z->setValidator(new QDoubleValidator(this));	
	this->lE_Angle1->setValidator(new QDoubleValidator(this));	
	this->lE_Angle2->setValidator(new QDoubleValidator(this));	
	this->lE_Angle3->setValidator(new QDoubleValidator(this));

	this->connect(this->tWFrameCameraModel,SIGNAL(currentChanged (int)),this, SLOT(tWCurrentChanged( int)));

	this->applySettings();

}





Bui_FrameCameraModelDlg::~Bui_FrameCameraModelDlg(void)
{
	this->cleanUp();
}


void Bui_FrameCameraModelDlg::cleanUp()
{
	if (this->cameraDlg)
		delete this->cameraDlg;
	this->cameraDlg = NULL;

	if (this->cameraMountingDlg)
		delete this->cameraMountingDlg;
	this->cameraMountingDlg = NULL;

	if (this->existingFrameModel)
		this->existingFrameModel->removeListener(this);
	this->existingFrameModel = NULL;

	if (this->selectedCameraIndex >=0 && 
		this->selectedCameraIndex < int(this->existingCameras.size()) && 
		this->existingCameras[this->selectedCameraIndex])
	{
		this->existingCameras[this->selectedCameraIndex]->removeListener(this);
		this->existingCameras[this->selectedCameraIndex] = NULL;
	}

	if (this->selectedCameraMountingIndex >=0 &&
		this->selectedCameraMountingIndex < int(this->existingCameraMountings.size()) &&
		this->existingCameraMountings[this->selectedCameraMountingIndex])
	{
		this->existingCameraMountings[this->selectedCameraMountingIndex]->removeListener(this);
		this->existingCameraMountings[this->selectedCameraMountingIndex] = NULL;
	}
	
	for (unsigned int i=0; i< this->cameras.size(); i++)
		delete this->cameras[i];

	this->cameras.clear();
}


bool Bui_FrameCameraModelDlg::cancelButtonReleased()
{
	this->success = false;
	return true;
}

CCharString Bui_FrameCameraModelDlg::helpButtonReleased()
{
	return "UserGuide/ThumbNodeFrameCameraModel.html";
}

CCharString Bui_FrameCameraModelDlg::getTitleString()
{
	return "FrameCamera model";
}


bool Bui_FrameCameraModelDlg::okButtonReleased()
{
	// force copy from sub dialogs
	// just change a tab so that we copy the information back from the current dialog
	this->tWCurrentChanged(this->oldTab + 1);

	this->listenToSignal = false;


	// handle camera

	CCamera* cameraToSave = NULL;
	if (this->selectedCameraIndex >= int(this->existingCameras.size()))
	{
		// user has added a new camera and has selected the camera to save 
		// add the new camera to the project
		cameraToSave = project.addCamera(this->cameras[this->selectedCameraIndex]);
	}
	else if(this->selectedCameraIndex >=0 && this->selectedCameraIndex < int(this->existingCameras.size()))
	{
		// existing project camera selected
		// update the project camera
		CCameraFactory::copyCamera(this->existingCameras[this->selectedCameraIndex],this->cameras[this->selectedCameraIndex]);
		cameraToSave = this->existingCameras[this->selectedCameraIndex];
	}


	// handle camera mounting

	CCameraMounting* cameraMountingToSave = NULL;
	if (this->selectedCameraMountingIndex >= int(this->existingCameraMountings.size()))
	{
		// user has added a new camera mounting and has selected it to save 
		// add the new camera mounting to the project
		CCameraMounting* newCameraMounting = project.addCameraMounting();
		*newCameraMounting = this->cameraMountings[this->selectedCameraMountingIndex];
		cameraMountingToSave = newCameraMounting;
	}
	else if (this->selectedCameraMountingIndex >= 0 && this->selectedCameraMountingIndex < int(this->existingCameraMountings.size()))
	{
		*this->existingCameraMountings[this->selectedCameraMountingIndex] = this->cameraMountings[this->selectedCameraMountingIndex];
		cameraMountingToSave= this->existingCameraMountings[this->selectedCameraMountingIndex];
	}


	if (this->viewOnly)
	{
		// update existing frame camera model
		existingFrameModel->setCameraPosition(this->cameraPos);
		if (this->rB_RollPitchYaw->isChecked())
			this->existingFrameModel->setCameraRotation(CRotationBaseFactory::initRotation(this->rollPitchYaw.getRotMat(),RPY));
		else
			this->existingFrameModel->setCameraRotation(CRotationBaseFactory::initRotation(this->omegaPhiKappa.getRotMat(),OPK));

	}
	else
	{
		// init new frame camera model
		CFileName fileName;
		if (this->existingFrameModel->gethasValues()) // keep the old name
			fileName =  *this->existingFrameModel->getFileName();
		else
			fileName = "Frame Camera";

		if (this->rB_RollPitchYaw->isChecked())
			this->existingFrameModel->init(fileName,this->cameraPos,CRotationBaseFactory::initRotation(this->rollPitchYaw.getRotMat(),RPY),cameraMountingToSave,(CFrameCamera*)cameraToSave);
		else
			this->existingFrameModel->init(fileName,this->cameraPos,CRotationBaseFactory::initRotation(this->omegaPhiKappa.getRotMat(),OPK),cameraMountingToSave,(CFrameCamera*)cameraToSave);
	}

	this->listenToSignal = true;

	this->success = true;
	return true;
}



void Bui_FrameCameraModelDlg::pbCameraReleased()
{
	this->initCameraDlg(NULL);
	this->tWFrameCameraModel->setTabEnabled(2,true);
	this->tWFrameCameraModel->setCurrentIndex(2);
	
}



bool Bui_FrameCameraModelDlg::selectCamera(int cbCameraIndex)
{
	this->cB_Camera->blockSignals(true);

	// select the row
	this->cB_Camera->setCurrentIndex(cbCameraIndex);


	if (this->selectedCameraIndex >=0 && this->selectedCameraIndex < int(this->existingCameras.size()))
		this->existingCameras[this->selectedCameraIndex]->removeListener(this);
	
	this->selectedCameraIndex = cbCameraIndex -1; // save the vector index 

	if (this->selectedCameraIndex >=0 && this->selectedCameraIndex < int(this->existingCameras.size()))	
		this->existingCameras[this->selectedCameraIndex]->addListener(this);


	if (cbCameraIndex > 0)
	{	
		this->pB_Camera->setEnabled(false);
		this->initCameraDlg((CFrameCamera*)this->cameras[this->selectedCameraIndex]);
		this->tWFrameCameraModel->setTabEnabled(2,true);
	}
	else
	{	
		this->pB_Camera->setEnabled(true);
		this->initCameraDlg(NULL);
		this->tWFrameCameraModel->setTabEnabled(2,false);
	}

	this->cB_Camera->blockSignals(false);
	this->checkAvailabilityOfSettings();
	return true;
}

bool Bui_FrameCameraModelDlg::selectCameraMounting(int cbCameraMountingIndex)
{
	this->cB_CameraMounting->blockSignals(true);

	// select the row
	this->cB_CameraMounting->setCurrentIndex(cbCameraMountingIndex);

	if (this->selectedCameraMountingIndex >=0 && this->selectedCameraMountingIndex < int(this->existingCameraMountings.size()))
		this->existingCameraMountings[this->selectedCameraMountingIndex]->removeListener(this);

	this->selectedCameraMountingIndex = cbCameraMountingIndex -1;
	
	if (this->selectedCameraMountingIndex >=0 && this->selectedCameraMountingIndex < int(this->existingCameraMountings.size()))
		this->existingCameraMountings[this->selectedCameraMountingIndex]->addListener(this);

	if (cbCameraMountingIndex > 0)
	{
		this->pB_CameraMounting->setEnabled(false);
		this->initCameraMountingDlg(&this->cameraMountings[this->selectedCameraMountingIndex]);
		this->tWFrameCameraModel->setTabEnabled(1,true);
	}
	else
	{
		this->pB_CameraMounting->setEnabled(true);
		this->initCameraMountingDlg(NULL);
		this->tWFrameCameraModel->setTabEnabled(1,false);
	}

	this->cB_CameraMounting->blockSignals(false);
	this->checkAvailabilityOfSettings();
	return true;
}

void Bui_FrameCameraModelDlg::updateCameras()
{
	this->cB_Camera->blockSignals(true);
	this->cB_Camera->clear();

	// always add the possibility to create a new camera
	this->cB_Camera->addItem("Define new camera",0);

	for (unsigned int i=0; i< this->cameras.size(); i++)
	{
		CCamera *c= this->cameras[i];

		if (c->getCameraType() == eDigitalFrame)
			// User data is never accessed, not adding camera data to CB
			//this->cB_Camera->addItem(QString("%1 (digital)").arg(c->getName().GetChar()), c);
			this->cB_Camera->addItem(QString("%1 (digital)").arg(c->getName().GetChar()));
		else if (c->getCameraType() == eAnalogueFrame)
			// User data is never accessed, not adding camera data to CB
			//this->cB_Camera->addItem(QString("%1 (analogue)").arg(c->getName().GetChar()), c);
			this->cB_Camera->addItem(QString("%1 (analogue)").arg(c->getName().GetChar()));
	}
	
	this->cB_Camera->blockSignals(false);


}

void Bui_FrameCameraModelDlg::updateMountings()
{
	this->cB_CameraMounting->blockSignals(true);
	this->cB_CameraMounting->clear();

	// always add the possibility to create a new camera mounting
	this->cB_CameraMounting->addItem("Define new camera mounting",0);

	for (unsigned int i=0; i< this->cameraMountings.size(); i++)
		this->cB_CameraMounting->addItem(QString("%1").arg(this->cameraMountings[i].getName().GetChar()));

	this->cB_CameraMounting->blockSignals(false);
}


void Bui_FrameCameraModelDlg::leTextChanged(const QString & text)
{
	this->checkAvailabilityOfSettings();
}

void Bui_FrameCameraModelDlg::rbRollPitchYawReleased()
{
	this->rollPitchYaw.updateRotMat(this->omegaPhiKappa.getRotMat());
	this->lB_Angle1->setText("Roll");
	this->lB_Angle2->setText("Pitch");
	this->lB_Angle3->setText("Yaw");

	this->applySettings();


}

void Bui_FrameCameraModelDlg::rbOmegaPhiKappaReleased()
{
	this->omegaPhiKappa.updateRotMat(this->rollPitchYaw.getRotMat());
	this->lB_Angle1->setText("Omega");
	this->lB_Angle2->setText("Phi");
	this->lB_Angle3->setText("Kappa");
	this->applySettings();
}

void Bui_FrameCameraModelDlg::cBCameraCurrentIndexChanged(int index)
{
	this->selectCamera(index);	
}

void Bui_FrameCameraModelDlg::cBCameraMountingCurrentIndexChanged(int index)
{
		this->selectCameraMounting(index);	
}

void Bui_FrameCameraModelDlg::pbCamaraMountingReleased()
{
	this->initCameraMountingDlg(NULL);
	this->tWFrameCameraModel->setTabEnabled(1,true);
	this->tWFrameCameraModel->setCurrentIndex(1);

	
}

void Bui_FrameCameraModelDlg::initCameraMountingDlg(CCameraMounting* cameraMounting)
{
	if (this->cameraMountingDlg)
	{
		this->tab_CameraMounting->layout()->removeWidget(this->cameraMountingDlg);
		QObject::disconnect(this->cameraMountingDlg, SIGNAL(enableOkButton(bool)),this,SLOT(cameraMountingStatusChanged(bool)));
		delete this->cameraMountingDlg;
		this->cameraMountingDlg = NULL;
	}


	this->cameraMountingDlg = new Bui_CameraMountingDlg(cameraMounting,!this->viewOnly,this);
	this->tab_CameraMounting->layout()->addWidget(this->cameraMountingDlg);

	QObject::connect(this->cameraMountingDlg, SIGNAL(enableOkButton(bool)),this,SLOT(cameraMountingStatusChanged(bool)));


}


void Bui_FrameCameraModelDlg::initCameraDlg(CFrameCamera* camera)
{
	if (this->cameraDlg)
	{
		this->tab_Camera->layout()->removeWidget(this->cameraDlg);
		QObject::disconnect(this->cameraDlg, SIGNAL(enableOkButton(bool)),this,SLOT(cameraStatusChanged(bool)));
		delete this->cameraDlg;
		this->cameraDlg = NULL;
	}


	this->cameraDlg = new Bui_FrameCameraDlg(camera,this);
	this->tab_Camera->layout()->addWidget(this->cameraDlg);

	QObject::connect(this->cameraDlg, SIGNAL(enableOkButton(bool)),this,SLOT(cameraStatusChanged(bool)));


}


void Bui_FrameCameraModelDlg::leXEditingFinished()
{
	this->cameraPos.x = this->lE_X->text().toDouble();
	this->applySettings();
}

void Bui_FrameCameraModelDlg::leYEditingFinished()
{
	this->cameraPos.y = this->lE_Y->text().toDouble();
	this->applySettings();
}

void Bui_FrameCameraModelDlg::leZEditingFinished()
{
	this->cameraPos.z = this->lE_Z->text().toDouble();
	this->applySettings();
}

void Bui_FrameCameraModelDlg::leAngle1EditingFinished()
{
	if (this->rB_RollPitchYaw->isChecked())
	{
		this->rollPitchYaw[0] =  this->lE_Angle1->text().toDouble()*M_PI/180.0;
		this->rollPitchYaw.computeRotMatFromParameter();
	}
	else
	{
		this->omegaPhiKappa[0] = this->lE_Angle1->text().toDouble()*M_PI/180.0;
		this->omegaPhiKappa.computeRotMatFromParameter();
	}

	this->applySettings();
}

void Bui_FrameCameraModelDlg::leAngle2EditingFinished()
{
	if (this->rB_RollPitchYaw->isChecked())
	{
		this->rollPitchYaw[1] =  this->lE_Angle2->text().toDouble()*M_PI/180.0;
		this->rollPitchYaw.computeRotMatFromParameter();
	}
	else
	{
		this->omegaPhiKappa[1] = this->lE_Angle2->text().toDouble()*M_PI/180.0;
		this->omegaPhiKappa.computeRotMatFromParameter();
	}
	this->applySettings();
}

void Bui_FrameCameraModelDlg::leAngle3EditingFinished()
{
	if (this->rB_RollPitchYaw->isChecked())
	{
		this->rollPitchYaw[2] =  this->lE_Angle3->text().toDouble()*M_PI/180.0;
		this->rollPitchYaw.computeRotMatFromParameter();
	}
	else
	{
		this->omegaPhiKappa[2] = this->lE_Angle3->text().toDouble()*M_PI/180.0;
		this->omegaPhiKappa.computeRotMatFromParameter();
	}
	this->applySettings();
}



void Bui_FrameCameraModelDlg::applySettings()
{
	int digits1 = 4;
	int digits2 = 5;

	this->lE_X->setText(QString("%1").arg(this->cameraPos.x,0,'f',digits1));
	this->lE_Y->setText(QString("%1").arg(this->cameraPos.y,0,'f',digits1));
	this->lE_Z->setText(QString("%1").arg(this->cameraPos.z,0,'f',digits1));

	if (this->rB_RollPitchYaw->isChecked())
	{
		this->lE_Angle1->setText(QString("%1").arg(this->rollPitchYaw[0]*180.0/M_PI,0,'f',digits2));
		this->lE_Angle2->setText(QString("%1").arg(this->rollPitchYaw[1]*180.0/M_PI,0,'f',digits2));
		this->lE_Angle3->setText(QString("%1").arg(this->rollPitchYaw[2]*180.0/M_PI,0,'f',digits2));
	}
	else
	{
		this->lE_Angle1->setText(QString("%1").arg(this->omegaPhiKappa[0]*180.0/M_PI,0,'f',digits2));
		this->lE_Angle2->setText(QString("%1").arg(this->omegaPhiKappa[1]*180.0/M_PI,0,'f',digits2));
		this->lE_Angle3->setText(QString("%1").arg(this->omegaPhiKappa[2]*180.0/M_PI,0,'f',digits2));
	}

	this->checkAvailabilityOfSettings();
}

void Bui_FrameCameraModelDlg::checkAvailabilityOfSettings()
{
	if (this->cB_Camera->currentIndex() == 0 || this->cB_CameraMounting->currentIndex() == 0 ||
		!this->lE_X->hasAcceptableInput() || 
		!this->lE_Y->hasAcceptableInput() || !this->lE_Z->hasAcceptableInput() ||
		!this->lE_Angle1->hasAcceptableInput() || !this->lE_Angle2->hasAcceptableInput() ||
		!this->lE_Angle3->hasAcceptableInput())
	{
		if (this->cB_Camera->currentIndex() == 0 && this->tWFrameCameraModel->currentIndex() !=2)
			this->tWFrameCameraModel->setTabEnabled(2,false);
		else if (this->cB_CameraMounting->currentIndex() == 0 &&this->tWFrameCameraModel->currentIndex() !=1 )
			this->tWFrameCameraModel->setTabEnabled(1,false);
		else if (!this->lE_X->hasAcceptableInput() || 
			!this->lE_Y->hasAcceptableInput() || !this->lE_Z->hasAcceptableInput() ||
			!this->lE_Angle1->hasAcceptableInput() || !this->lE_Angle2->hasAcceptableInput() ||
			!this->lE_Angle3->hasAcceptableInput())
		{
			this->tWFrameCameraModel->setTabEnabled(2,false);
			this->tWFrameCameraModel->setTabEnabled(1,false);
		}


		emit enableOkButton(false);
	}
	else
	{
		this->tWFrameCameraModel->setTabEnabled(2,true);
		this->tWFrameCameraModel->setTabEnabled(1,true);

		emit enableOkButton(true);
	}

}


void Bui_FrameCameraModelDlg::cameraMountingStatusChanged(bool b)
{
	if (b)
	{
		this->tWFrameCameraModel->setTabEnabled(0,true);
		this->tWFrameCameraModel->setTabEnabled(2,true);

		this->checkAvailabilityOfSettings();
	}
	else
	{
		this->tWFrameCameraModel->setTabEnabled(0,false);
		this->tWFrameCameraModel->setTabEnabled(2,false);
		
		emit enableOkButton(b);
	}



}

void Bui_FrameCameraModelDlg::cameraStatusChanged(bool b)
{
	if (b)
	{
		this->tWFrameCameraModel->setTabEnabled(0,true);
		this->tWFrameCameraModel->setTabEnabled(1,true);

		this->checkAvailabilityOfSettings();
	}
	else
	{
		this->tWFrameCameraModel->setTabEnabled(0,false);
		this->tWFrameCameraModel->setTabEnabled(1,false);

		emit enableOkButton(b);
	}

	

}

void Bui_FrameCameraModelDlg::tWCurrentChanged( int index )
{
	if (oldTab != -1)
	{
		if (oldTab == 1)
		{
			
			if (this->cameraMountingDlg && this->cameraMountingDlg->okButtonReleased())
			{
				if (!this->viewOnly)	
				{
					int currentIndex = this->cB_CameraMounting->currentIndex();

					if (currentIndex == 0) // user defines a new camera mounting
						this->cameraMountings.push_back(this->cameraMountingDlg->getCameraMounting());
					
					this->updateMountings();
						
					// select the new created camera mounting
					if (currentIndex == 0)
						this->selectCameraMounting(this->cameraMountings.size()); // the last one is the new one
					else
						this->selectCameraMounting(currentIndex); // keep the selected one
				}
			}
			
		}
		else if (oldTab == 2)
		{
			if (this->cameraDlg && this->cameraDlg->okButtonReleased())
			{
				if (!this->viewOnly)
				{
					int currentIndex = this->cB_Camera->currentIndex();

					if (currentIndex == 0) // user defines a new camera 
							this->cameras.push_back(CCameraFactory::createCamera(&this->cameraDlg->getCamera()));
			
					this->updateCameras();
							
					// select the new created camera
					if (currentIndex == 0)
						this->selectCamera(this->cameras.size()); // the last one is the new one
					else
						this->selectCamera(currentIndex); // keep the selected one
				}

			}
			
		}
		this->checkAvailabilityOfSettings();
	}

	oldTab = index;

}


void Bui_FrameCameraModelDlg::ckeckDlgContent()
{
	this->checkAvailabilityOfSettings();
}

void Bui_FrameCameraModelDlg::elementsChangedSlot(CLabel element)
{
	if (!this->listenToSignal)
		return;

	if  (element  == "CFRAMECAMERA" || element  == "CFRAMECAMERAANALOGUE")
		CCameraFactory::copyCamera(this->cameras[this->selectedCameraIndex],this->existingCameras[this->selectedCameraIndex]);
	else if  (element  == "CCAMERAMOUNTING")
		this->cameraMountings[this->selectedCameraMountingIndex] = *this->existingCameraMountings[this->selectedCameraMountingIndex];
	else if (element == "CFRAMECAMERAMODEL")
	{
		this->frameCameraModel = *this->existingFrameModel;
		this->cameraPos = this->frameCameraModel.getCameraPosition();
		if (this->existingFrameModel->getCameraRotation()->getType() == RPY)
		{
			this->rollPitchYaw = *(CRollPitchYawRotation*)this->existingFrameModel->getCameraRotation();
			this->rB_RollPitchYaw->blockSignals(true);
			this->rB_RollPitchYaw->setChecked(true);
			this->rB_RollPitchYaw->blockSignals(false);
		}
		else
		{		
			this->omegaPhiKappa = *(COmegaPhiKappaRotation*)this->existingFrameModel->getCameraRotation();
			this->rB_OmegaPhiKappa->blockSignals(true);
			this->rB_OmegaPhiKappa->setChecked(true);
			this->rB_OmegaPhiKappa->blockSignals(false);
		}
	}

	this->applySettings();
}