#include "PanHandler.h"
#include "Bui_MainWindow.h"

CPanHandler::CPanHandler(void)
{
	this->statusBarText = "PANMODE ";
}

CPanHandler::~CPanHandler(void)
{
}


bool CPanHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{

	if (v->getLeftButtonDown() || v->getMiddleButtonDown())	
	{
		int xDiff = v->getXPressed() - e->x();
		int yDiff = v->getYPressed() - e->y();
        
		int xPos = v->getXPosPressed();
		int yPos = v->getYPosPressed();
		xPos += xDiff;
		yPos += yDiff;
		v->setScrollX(xPos);
		v->setScrollY(yPos);
		this->setCurrentCursor(v);

		// when dragging, NearestNeighbour is good enough
		v->changeResamplingType(eNearestNeighbour);
		v->update();

		if (this->inGeoLinkMode)
			this->doGeoLink(v,true);

		return true;
	}


	return false;
};

bool CPanHandler::keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
{
	return CBaristaViewEventHandler::keyReleaseEvent(e, v);
};

bool CPanHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{ 
	return false;
};

bool CPanHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{

	return false;
};


bool CPanHandler::redraw(CBaristaView *v)
{
	return true;
};

void CPanHandler::initialize(CBaristaView *v)
{
};

void CPanHandler::open()
{
	CBaristaViewEventHandler::open();
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->PanAction->setChecked(true);
	mainwindow->blockSignals(false);
};

void CPanHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->PanAction->setChecked(false);
	mainwindow->blockSignals(false);
};

bool CPanHandler::handleMiddleMousePressEvent(QMouseEvent *e, CBaristaView *v) 
{ 
		this->setCurrentCursor(v);
		return true;
		
}

bool CPanHandler::handleMiddleMouseReleaseEvent(QMouseEvent *e, CBaristaView *v) 
{
	// reset the original resampling mode of the view
	v->resetResamplingType();
	v->update();

	if (this->inGeoLinkMode)
		this->doGeoLink(v,false);
	return true;
}