#include "Bui_ExtensionsGroupBox.h"

Bui_ExtensionsGroupBox::Bui_ExtensionsGroupBox(QWidget *parent, bool mode) : SwitchMode(mode)
{
	setupUi(this);

	// spin box connections
	this->connect(this->spinBoxULX, SIGNAL(valueChanged(double)), this, SLOT(spinBoxULXChanged(double)));
	this->connect(this->spinBoxULY, SIGNAL(valueChanged(double)), this, SLOT(spinBoxULYChanged(double)));
	this->connect(this->spinBoxLRX, SIGNAL(valueChanged(double)), this, SLOT(spinBoxLRXChanged(double)));
	this->connect(this->spinBoxLRY, SIGNAL(valueChanged(double)), this, SLOT(spinBoxLRYChanged(double)));

	this->connect(this->spinBoxHeight, SIGNAL(valueChanged(double)), this, SLOT(spinBoxHeightChanged(double)));
	this->connect(this->spinBoxWidth, SIGNAL(valueChanged(double)), this, SLOT(spinBoxWidthChanged(double)));

	this->connect(this->spinBoxGrid, SIGNAL(valueChanged(double)), this, SLOT(spinBoxGridChanged(double)));
	this->connect(this->spinBoxCols, SIGNAL(valueChanged(double)), this, SLOT(spinBoxColsChanged(double)));
	this->connect(this->spinBoxRows, SIGNAL(valueChanged(double)), this, SLOT(spinBoxRowChanged(double)));

	// radio button connections
	if (this->SwitchMode)
	{
		this->connect(this->rbPixel,SIGNAL(released()),this,SLOT(rbPixelReleased()));
		this->connect(this->rbMetric,SIGNAL(released()),this,SLOT(rbMetricReleased()));
	}
	else
	{
		this->rbPixel->setVisible(false);
		this->rbMetric->setVisible(false);
		this->widgetswitch->setVisible(false);
	}


	this->ndigits = 2;

	this->resize(0,0);

}

Bui_ExtensionsGroupBox::~Bui_ExtensionsGroupBox()
{

}

void Bui_ExtensionsGroupBox::blockSpinBoxSignals(bool block)
{
	this->spinBoxULX->blockSignals(block);
	this->spinBoxULY->blockSignals(block);
	this->spinBoxLRX->blockSignals(block);
	this->spinBoxLRY->blockSignals(block);

	this->spinBoxHeight->blockSignals(block);
	this->spinBoxWidth->blockSignals(block);

	this->spinBoxCols->blockSignals(block);
	this->spinBoxRows->blockSignals(block);
	this->spinBoxGrid->blockSignals(block);
};


void Bui_ExtensionsGroupBox::setSpinBoxDecimals(int decimals)
{
	this->ndigits = decimals;
	this->spinBoxULX->setDecimals(this->ndigits);
	this->spinBoxULY->setDecimals(this->ndigits);
	this->spinBoxLRX->setDecimals(this->ndigits);
	this->spinBoxLRY->setDecimals(this->ndigits);

	this->spinBoxGrid->setDecimals(this->ndigits);

	this->spinBoxHeight->setDecimals(this->ndigits);
	this->spinBoxWidth->setDecimals(this->ndigits);


};

void Bui_ExtensionsGroupBox::setSpinBoxStep(double step)
{
	this->spinBoxULX->setSingleStep(step);
	this->spinBoxULY->setSingleStep(step);
	this->spinBoxLRX->setSingleStep(step);
	this->spinBoxLRY->setSingleStep(step);
	this->spinBoxHeight->setSingleStep(step);
	this->spinBoxWidth->setSingleStep(step);
};

void Bui_ExtensionsGroupBox::spinBoxULXChanged(double newulx)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriculx = newulx;
		this->getROIfromMetrictoPixel();
	}
	else
		this->ulx = (int)newulx;

	this->setSpinBoxes();
}

void Bui_ExtensionsGroupBox::spinBoxULYChanged(double newuly)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriculy = newuly;
		this->getROIfromMetrictoPixel();
	}
	else
	{
		this->uly = (int)newuly;
	}

	this->setSpinBoxes();
}

void Bui_ExtensionsGroupBox::spinBoxLRXChanged(double newlrx)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriclrx = newlrx;
		this->getROIfromMetrictoPixel();
	}
	else
		this->lrx = (int)newlrx;

	this->setSpinBoxes();
}

void Bui_ExtensionsGroupBox::spinBoxLRYChanged(double newlry)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriclry = newlry;
		this->getROIfromMetrictoPixel();
	}

	else
		this->lry = (int)newlry;

	this->setSpinBoxes();
}

void Bui_ExtensionsGroupBox::spinBoxHeightChanged(double newheight)
{
	if ( this->rbMetric->isChecked() ) 
	{
		this->metriclry = this->metriculy - newheight;
		this->getROIfromMetrictoPixel();
	}
	else
	{
		this->lry = this->uly + newheight;
	}

	this->setSpinBoxes();
}

void Bui_ExtensionsGroupBox::spinBoxWidthChanged(double newwidth)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriclrx = this->metriculx + newwidth;
		this->getROIfromMetrictoPixel();
	}
	else
	{
		this->lrx = this->ulx + newwidth;
	}

	this->setSpinBoxes();
}

void Bui_ExtensionsGroupBox::setSpinBoxes()
{
	if ( this->rbMetric->isChecked() ) this->setMetricSpinBoxes();
	else this->setPixelSpinBoxes();
}

void Bui_ExtensionsGroupBox::getROIfromMetrictoPixel()
{

	if (!this->tfw.gethasValues())
		return;
	
	C3DPoint metricUL(this->metriculx, this->metriculy, 0);
	C3DPoint metricLR(this->metriclrx, this->metriclry, 0);

	C2DPoint UL;
	C2DPoint LR;

	tfw.transformObj2Obs(UL, metricUL);
	tfw.transformObj2Obs(LR, metricLR);

	this->ulx = UL.x;
	this->uly = UL.y;

	this->lrx = LR.x;
	this->lry = LR.y;

	this->ncols = this->lrx - this->ulx;
	this->nrows = this->lry - this->uly;
}


void Bui_ExtensionsGroupBox::setPixelSpinBoxes()
{
	this->setPixelValues();
	this->blockSpinBoxSignals(true);
	this->setSpinBoxDecimals(this->ndigits);
	this->setSpinBoxStep(1);

	// set ranges of the spin boxes
	this->spinBoxULX->setRange(0, this->maxx);
	this->spinBoxULY->setRange(0, this->maxy);
	this->spinBoxLRX->setRange(this->ulx, this->maxx);
	this->spinBoxLRY->setRange(this->uly, this->maxy);

	// set them to size of ul of ROI to lr image corner
	this->spinBoxWidth->setRange(0, this->maxcols);
	this->spinBoxHeight->setRange(0, this->maxrows);
	
	// set current ROI values
	this->spinBoxULX->setValue(this->ulx);
	this->spinBoxULY->setValue(this->uly);
	this->spinBoxLRX->setValue(this->lrx);
	this->spinBoxLRY->setValue(this->lry);
	this->spinBoxHeight->setValue(this->nrows);
	this->spinBoxWidth->setValue(this->ncols);

	this->blockSpinBoxSignals(false);
}

void Bui_ExtensionsGroupBox::setMetricSpinBoxes()
{
	if ( this->tfw.gethasValues() )
	{		
		if ( this->tfw.getRefSys().getCoordinateType() == eGEOGRAPHIC )
			this->setSpinBoxStep(0.0001);
		else
			this->setSpinBoxStep(1);


		// calculate ROI in metric and reproject back to image
		this->setMetricValues();
		this->blockSpinBoxSignals(true);
		this->setSpinBoxDecimals(this->ndigits);

		// set ranges of the spin boxes
		this->spinBoxULX->setRange(this->metricminx, this->metricmaxx);
		this->spinBoxULY->setRange(this->metricminy, this->metricmaxy);
		this->spinBoxLRX->setRange(this->metriculx, this->metricmaxx);
		this->spinBoxLRY->setRange(this->metricminy, this->metriculy);

		// set them to size of ul of ROI to lr image corner
		this->spinBoxWidth->setRange(0, this->metricmaxwidth);
		this->spinBoxHeight->setRange(0, this->metricmaxheight);

		this->spinBoxGrid->setRange(0, this->metricmaxwidth);
		this->spinBoxCols->setRange(0, this->maxcols);
		this->spinBoxRows->setRange(0, this->maxrows);
		
		// set current ROI values
		this->spinBoxULX->setValue(this->metriculx);
		this->spinBoxULY->setValue(this->metriculy);
		this->spinBoxLRX->setValue(this->metriclrx);
		this->spinBoxLRY->setValue(this->metriclry);
		this->spinBoxHeight->setValue(this->metricheight);
		this->spinBoxWidth->setValue(this->metricwidth);

		this->spinBoxGrid->setValue(this->gridsize);
		this->spinBoxCols->setValue(this->ncols);
		this->spinBoxRows->setValue(this->nrows);

		this->blockSpinBoxSignals(false);

	}
}

void Bui_ExtensionsGroupBox::setPixelValues()
{
	this->ncols = this->lrx - this->ulx;
	this->nrows = this->lry - this->uly;

	this->maxcols = this->maxx  - this->ulx;
	this->maxrows = this->maxy - this->uly;

	this->ndigits = 0;
}



void Bui_ExtensionsGroupBox::setMetricValues()
{
	if (!this->tfw.gethasValues())
		return;

	C2DPoint ULImg(0, 0);
	C2DPoint UL(this->ulx, this->uly);
	C2DPoint LR(this->lrx, this->lry);
	C2DPoint LRImg(this->maxx, this->maxy);

	C3DPoint metricULImg;
	C3DPoint metricUL;
	C3DPoint metricLR;
	C3DPoint metricLRImg;

	this->tfw.transformObs2Obj(metricULImg, ULImg);
	this->tfw.transformObs2Obj(metricUL, UL);
	this->tfw.transformObs2Obj(metricLR, LR);
	this->tfw.transformObs2Obj(metricLRImg, LRImg);

	if ( this->tfw.getRefSys().getCoordinateType() == eGRID )
	{
		this->ndigits = 2;
		this->metriculx = metricUL.x;
		this->metriculy = metricUL.y;

		this->metriclrx = metricLR.x;
		this->metriclry = metricLR.y;

		this->metricwidth = this->metriclrx - this->metriculx;
		this->metricheight = this->metriculy - this->metriclry;

		this->ncols = this->metricwidth/this->gridsize;
		this->nrows = this->metricheight/this->gridsize;

		this->metricminx = metricULImg.x;
		this->metricmaxx = metricLRImg.x;

		this->metricminy = metricLRImg.y;
		this->metricmaxy = metricULImg.y;

		this->metricmaxwidth = this->metricmaxx - this->metriculx;
		this->metricmaxheight = this->metriculy - this->metricminy;
	}
	else if ( this->tfw.getRefSys().getCoordinateType() == eGEOGRAPHIC )
	{
		this->ndigits = 8;

		this->metriculx = metricUL.x;
		this->metriculy = metricUL.y;

		this->metriclrx = metricLR.x;
		this->metriclry = metricLR.y;

		this->metricwidth = this->metriclrx - this->metriculx;
		this->metricheight = this->metriculy - this->metriclry;

		this->metricminx = metricULImg.x;
		this->metricmaxx = metricLRImg.x;

		this->metricminy = metricLRImg.y;
		this->metricmaxy = metricULImg.y;

		this->metricmaxwidth = this->metricmaxx - this->metriculx;
		this->metricmaxheight = this->metriculy - this->metricminy;
	}
}

void Bui_ExtensionsGroupBox::resetSpinBoxes()
{
	// set ranges of the spin boxes
	this->blockSpinBoxSignals(true);

	this->spinBoxULX->setRange(0, 0);
	this->spinBoxULY->setRange(0, 0);
	this->spinBoxLRX->setRange(0, 0);
	this->spinBoxLRY->setRange(0, 0);

	this->spinBoxWidth->setRange(0, 0);
	this->spinBoxHeight->setRange(0, 0);

	this->spinBoxCols->setRange(0, 0);
	this->spinBoxRows->setRange(0, 0);
	this->spinBoxGrid->setRange(0, 0);
	
	// set current ROI values
	this->spinBoxULX->setValue(0);
	this->spinBoxULY->setValue(0);
	this->spinBoxLRX->setValue(0);
	this->spinBoxLRY->setValue(0);

	this->spinBoxHeight->setValue(0);
	this->spinBoxWidth->setValue(0);

	this->spinBoxCols->setValue(0);
	this->spinBoxRows->setValue(0);
	this->spinBoxGrid->setValue(0);

	this->blockSpinBoxSignals(false);

}

void Bui_ExtensionsGroupBox::setValues(const C2DPoint &shift, const C2DPoint &gridSize, const int cols, const int rows, const CReferenceSystem &refsys)
{
	this->rbMetric->setChecked(true);
	this->ncols = cols;
	this->nrows = rows;
	
	this->tfw.setRefSys(refsys);


	if ( refsys.getCoordinateType() == eGEOGRAPHIC )
	{
		Matrix parameters(6,1);
		parameters.element(0,0) = 0.0;
		parameters.element(1,0) = -gridSize.y;
		parameters.element(2,0) = shift.x;
		parameters.element(3,0) = gridSize.x;
		parameters.element(4,0) = 0.0;
		parameters.element(5,0) = shift.y;

		this->tfw.setAll(parameters);

		this->metricmaxx = this->tfw.getC();
		this->metricminy = this->tfw.getF();

		this->gridsize = fabs(this->tfw.getB());

		this->metricwidth = this->ncols*this->gridsize;
		this->metricheight = this->nrows*gridSize.x;
		this->metricminx = this->metricmaxx - this->metricheight;
		this->metricmaxy = this->metricminy + this->metricwidth;
	}
	else
	{
		this->tfw.setFromShiftAndPixelSize(shift, gridSize);
		this->metricminx = this->tfw.getC();
		this->metricmaxy = this->tfw.getF();
		this->metricwidth = this->ncols*this->tfw.getA();
		this->metricheight = this->nrows*this->tfw.getA();
		
		this->gridsize = this->tfw.getA();


		this->metricmaxx = this->metricminx + this->metricwidth;
		this->metricminy = this->metricmaxy - this->metricheight;
	}


	this->metriculx = this->metricminx;
	this->metriculy = this->metricmaxy;
	this->metriclrx = this->metricmaxx;
	this->metriclry = this->metricminy;

	this->metricmaxwidth = this->metricwidth;
	this->metricmaxheight = this->metricheight;

	this->maxx = this->ncols;
	this->maxy = this->nrows;

	this->blockSpinBoxSignals(true);

	
	this->setSpinBoxDecimals(this->ndigits);

	// set ranges of the spin boxes
	this->spinBoxULX->setRange(this->metricminx, this->metricmaxx);
	this->spinBoxULY->setRange(this->metricminy, this->metricmaxy);
	this->spinBoxLRX->setRange(this->metriculx, this->metricmaxx);
	this->spinBoxLRY->setRange(this->metricminy, this->metriculy);

	this->spinBoxWidth->setRange(0, this->metricmaxwidth);
	this->spinBoxHeight->setRange(0, this->metricmaxheight);

	this->spinBoxGrid->setRange(0, 100000);

	this->maxcols = this->metricmaxwidth/ this->gridsize +1;
	this->maxrows = this->metricmaxheight/ this->gridsize +1;

	this->spinBoxCols->setRange(0, this->maxcols);
	this->spinBoxRows->setRange(0, this->maxrows);
	
	// set current ROI values
	this->spinBoxULX->setValue(this->metriculx);
	this->spinBoxULY->setValue(this->metriculy);
	this->spinBoxLRX->setValue(this->metriclrx);
	this->spinBoxLRY->setValue(this->metriclry);
	this->spinBoxHeight->setValue(this->metricheight);
	this->spinBoxWidth->setValue(this->metricwidth);

	this->spinBoxGrid->setValue(this->gridsize);
	this->spinBoxCols->setValue(this->ncols);
	this->spinBoxRows->setValue(this->nrows);

	this->blockSpinBoxSignals(false);
}


void Bui_ExtensionsGroupBox::rbMetricReleased()
{
	// set spinbox display to metric
	this->setMetricSpinBoxes();
}

void Bui_ExtensionsGroupBox::rbPixelReleased()
{
	// set spinbox display to pixel
	this->setPixelSpinBoxes();
}

void Bui_ExtensionsGroupBox::spinBoxGridChanged(double newgrid)
{
	if ( this->rbMetric->isChecked() )
	{
		this->gridsize = newgrid;
		this->getROIfromMetrictoPixel();
	}

	else
		this->gridsize = newgrid;

	this->setSpinBoxes();
}

void Bui_ExtensionsGroupBox::spinBoxColsChanged(double newcols)
{
	if ( this->rbMetric->isChecked() )
	{
		this->ncols = newcols;
		this->getROIfromMetrictoPixel();
	}

	else
		this->ncols = (int)newcols;

	this->setSpinBoxes();
}

void Bui_ExtensionsGroupBox::spinBoxRowChanged(double newrows)
{
	if ( this->rbMetric->isChecked() )
	{
		this->nrows = newrows;
		this->getROIfromMetrictoPixel();
	}

	else
		this->nrows = (int)newrows;

	this->setSpinBoxes();
}

