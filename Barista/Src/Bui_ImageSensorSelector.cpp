#include <QtWidgets\QComboBox>
#include "Bui_ImageSensorSelector.h"

#include "BaristaProject.h"
#include "VImage.h"

extern CBaristaProject project;

bui_ImageSensorSelector::bui_ImageSensorSelector(QComboBox *I_imageSelector, 
												 QComboBox *I_sensorModelSelector, 
												 eImageSelectorBandConfig bandconfig,
												 eImageSelectorSensorConfig sensorconfig,
												 CVImage *I_selectedImage , 
												 CSensorModel *I_selectedSensorModel) : 
			imageSelector(I_imageSelector), sensorModelSelector(I_sensorModelSelector), 
			selectedImage(I_selectedImage), selectedSensorModel(I_selectedSensorModel),
			bandConfig(bandconfig), deltaNone(0), sensorConfig(sensorconfig)
{
	init(selectedImage, selectedSensorModel, false);
}

bui_ImageSensorSelector::bui_ImageSensorSelector(QComboBox *I_imageSelector, 
												 QComboBox *I_sensorModelSelector, 
												 eImageSelectorBandConfig bandconfig,
												 eImageSelectorSensorConfig sensorconfig,
												 bool addNone,
												 CVImage *I_selectedImage , 
												 CSensorModel *I_selectedSensorModel) : 
			imageSelector(I_imageSelector), sensorModelSelector(I_sensorModelSelector), 
			selectedImage(I_selectedImage), selectedSensorModel(I_selectedSensorModel),
			bandConfig(bandconfig), deltaNone(0), sensorConfig(sensorconfig)
{
	init(selectedImage, selectedSensorModel, addNone);
}

bui_ImageSensorSelector::~bui_ImageSensorSelector(void)
{
	imageSelector       = 0;
	sensorModelSelector = 0;
	selectedImage       = 0;
	selectedSensorModel = 0;
}

void bui_ImageSensorSelector::init(CVImage *I_selectedImage, CSensorModel *I_selectedSensorModel, bool addNone)
{
	if (addNone) deltaNone = 1;

	if (project.getNumberOfImages())
	{
		int selectedIndex = 0;
		this->imageSelector->blockSignals(true);
		this->imageSelector->clear();
		if (addNone) this->imageSelector->addItem("None");
				
		for (int i = 0; i < project.getNumberOfImages(); ++i)
		{
			CVImage *img = project.getImageAt(i);
			if ((this->bandConfig == eALLBANDS) || 
				((this->bandConfig == ePANONLY) && (img->getHugeImage()->getBands() == 1)) ||
				((this->bandConfig == eMULTIBANDONLY) && (img->getHugeImage()->getBands() >= 3)))
			{
				if ((this->sensorConfig == eALLSENSORS) || 
					((this->sensorConfig == eTFWONLY) && (img->hasTFW())) ||
					((this->sensorConfig == eTFWORNONE) && (img->hasTFW()) && !img->getCurrentSensorModel()) ||
					((this->sensorConfig == eCURRENTNOTFW) && img->getCurrentSensorModel() && img->getCurrentSensorModelType() != eTFW) ||
					((this->sensorConfig == eCURRENTALL) && img->getCurrentSensorModel()) ||
					((this->sensorConfig == eFRAMECAMONLY) && img->getFrameCamera()->gethasValues()))
				{
					this->imageSelector->addItem(img->getFileNameChar());
					if (img == I_selectedImage) 
						selectedIndex = i + deltaNone;
				}
			}
		}

		this->imageSelector->setCurrentIndex(selectedIndex);

		this->imageSelector->blockSignals(false);

		if (!I_selectedImage && !addNone)
		{
			if (this->bandConfig != eALLBANDS) 
			{
				CCharString name((const char *) this->imageSelector->itemText(selectedIndex).toLatin1());
				selectedIndex = project.getImageIndex(&name);
			}
			
			this->selectedImage = project.getImageAt(selectedIndex);
		}
		else this->selectedImage = I_selectedImage;
		initSensorModel(I_selectedSensorModel);
	}
}
	  
bool bui_ImageSensorSelector::initSensorModel(CSensorModel *I_selectedSensorModel)
{
	bool ret = true;
	if (this->sensorModelSelector)
	{
		this->RPCIndex    = -1;  
		this->affineIndex = -1;
		this->pushbroomIndex = -1;
		this->frameCameraIndex = -1;

		this->sensorModelSelector->blockSignals(true);
		if (!this->selectedImage) 
		{
			this->sensorModelSelector->setEnabled(false);
			this->sensorModelSelector->clear();
			this->selectedSensorModel = 0;
			ret = false;
		}
		else
		{
			int indexSelect = -1;
			int indexCurrent = -1;
			int index = 0;
			this->sensorModelSelector->setEnabled(true);
			this->sensorModelSelector->clear();
			if (this->selectedImage->hasRPC())
			{
				this->sensorModelSelector->addItem("RPC");
				if (this->selectedImage->getRPC() == I_selectedSensorModel) indexSelect = index;
				if (this->selectedImage->getRPC() == this->selectedImage->getCurrentSensorModel()) indexCurrent = index;
				this->RPCIndex = index;
				index++;
			}
			if (this->selectedImage->hasAffine())
			{
				this->sensorModelSelector->addItem("Affine");
				if (this->selectedImage->getAffine() == I_selectedSensorModel) indexSelect = index;
				if (this->selectedImage->getAffine() == this->selectedImage->getCurrentSensorModel()) indexCurrent = index;
				this->affineIndex = index;
				index++;
			}
			if (this->selectedImage->hasPushbroom())
			{
				this->sensorModelSelector->addItem("Pushbroom");
				if (this->selectedImage->getPushbroom() == I_selectedSensorModel) indexSelect = index;
				if (this->selectedImage->getPushbroom() == this->selectedImage->getCurrentSensorModel()) indexCurrent = index;
				this->pushbroomIndex = index;
				index++;
			}
			if (this->selectedImage->hasFrameCamera())
			{
				this->sensorModelSelector->addItem("Frame Camera");
				if (this->selectedImage->getFrameCamera() == I_selectedSensorModel) indexSelect = index;
				if (this->selectedImage->getFrameCamera() == this->selectedImage->getCurrentSensorModel()) indexCurrent = index;
				this->frameCameraIndex = index;
				index++;
			}
			if (indexSelect < 0)  indexSelect = indexCurrent;
			if ((indexSelect < 0) && (index > 0)) indexSelect = 0;
			if (indexSelect < 0)
			{
				this->sensorModelSelector->setEnabled(false);
				this->sensorModelSelector->clear();
				this->selectedSensorModel = 0;
				ret = false;
			}
			else
			{
				this->sensorModelSelector->setCurrentIndex(indexSelect);

				if (indexSelect == this->RPCIndex) this->selectedSensorModel = this->selectedImage->getRPC();
				else if (indexSelect == this->affineIndex) this->selectedSensorModel = this->selectedImage->getAffine();
				else if (indexSelect == this->pushbroomIndex) this->selectedSensorModel = this->selectedImage->getPushbroom();
				else if (indexSelect == frameCameraIndex)  this->selectedSensorModel = this->selectedImage->getFrameCamera();
			}
		}

		this->sensorModelSelector->blockSignals(false);
	}
	else ret = false;

	return ret;
};

bool bui_ImageSensorSelector::reactOnImageSelect()
{
	bool ret = true;
	int indexSelected = (this->imageSelector->currentIndex()) - deltaNone;
	if ((indexSelected < 0) || (indexSelected >= project.getNumberOfImages())) 
	{
		ret = false;
		this->selectedImage = 0;
		if (this->sensorModelSelector) ret &= initSensorModel();
	}
	else
	{
		if (this->bandConfig != eALLBANDS) 
		{
			CCharString name((const char *) this->imageSelector->itemText(indexSelected +deltaNone).toLatin1());
			indexSelected = project.getImageIndex(&name);
		}
		this->selectedImage = project.getImageAt(indexSelected);
		
		if (this->sensorModelSelector) ret = initSensorModel();
	}

	return ret;
}

bool bui_ImageSensorSelector::reactOnSensorModelSelect()
{
	bool ret = true;

	if (!this->selectedImage) ret = false;
	else
	{
		int selectedIndex = this->sensorModelSelector->currentIndex();
		if (selectedIndex == this->affineIndex) 
			this->selectedSensorModel = this->selectedImage->getAffine();
		else if (selectedIndex == this->RPCIndex) 
			this->selectedSensorModel = this->selectedImage->getRPC();
		else if (selectedIndex == this->pushbroomIndex) 
			this->selectedSensorModel = this->selectedImage->getPushbroom();
		else if (selectedIndex == this->frameCameraIndex) 
			this->selectedSensorModel = this->selectedImage->getFrameCamera();
		
		else ret = false;
	}

	return ret;
}
