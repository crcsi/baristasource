#include "Bui_BuildingDXF.h"

#include <QFileDialog>
#include "IniFile.h"
#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "SystemUtilities.h"


Bui_BuildingDXF::Bui_BuildingDXF(const CCharString &filenameBase, QWidget *parent)	: QDialog(parent),
		success(false), iFileName(""), relativeHeights(false), FilenameBase(filenameBase)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	connect(this->pBCancel,         SIGNAL(released()),    this, SLOT(CancelReleased()));
	connect(this->pBBrowse,         SIGNAL(released()),    this, SLOT(BrowseReleased()));
	connect(this->pBOk,             SIGNAL(released()),    this, SLOT(OkReleased()));
	connect(this->pBHelp,           SIGNAL(released()),    this, SLOT(HelpReleased()));
	connect(this->pAbsoluteHeights, SIGNAL(clicked(bool)), this, SLOT(ModeButtonChanged()));
	connect(this->pRelativeHeights, SIGNAL(clicked(bool)), this, SLOT(ModeButtonChanged()));
	this->applySettings();

}
Bui_BuildingDXF::~Bui_BuildingDXF()
{
}


void Bui_BuildingDXF::CancelReleased()
{
	this->close();
}

void Bui_BuildingDXF::OkReleased()
{
	this->success= true;
	this->close();
}

void Bui_BuildingDXF::HelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/ThumbNodeBuilding.html#BuildingDXF");
}

void Bui_BuildingDXF::ModeButtonChanged()
{
	this->relativeHeights = !this->relativeHeights;

	this->applySettings();
}

void Bui_BuildingDXF::BrowseReleased()
{
	CCharString str = inifile.getCurrDir();
	str += CSystemUtilities::dirDelimStr;
    str += FilenameBase + ".dxf";

    QString qstr( str.GetChar());

	// file dialog
	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a DXF File Name",
		qstr,
		"DXF-File (*.dxf)"); 

	if (s.isEmpty())
		return;

	if (!s.endsWith(".dxf"))
		s += ".dxf";  


    CFileName cfile = (const char*)s.toLatin1();

	this->iFileName = cfile;

	this->applySettings();
}

void Bui_BuildingDXF::applySettings()
{
	this->checkAvailabilityOfSettings();

	this->pFileName->setText(this->iFileName.GetPath().GetChar());

	this->pAbsoluteHeights->blockSignals(true);
	this->pRelativeHeights->blockSignals(true);

	if (this->relativeHeights)
	{
		this->pAbsoluteHeights->setChecked(false);
		this->pRelativeHeights->setChecked(true);
	}
	else
	{
		this->pAbsoluteHeights->setChecked(true);
		this->pRelativeHeights->setChecked(false);
	}

	this->pAbsoluteHeights->blockSignals(false);
	this->pRelativeHeights->blockSignals(false);
}

void Bui_BuildingDXF::checkAvailabilityOfSettings()
{
	if (this->iFileName.IsEmpty())
		this->pBOk->setEnabled(false);
	else
		this->pBOk->setEnabled(true);
}
