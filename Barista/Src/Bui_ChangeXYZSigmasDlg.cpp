#include "Bui_ChangeXYZSigmasDlg.h"
#include "Icons.h"

bui_ChangeXYZSigmasDlg::bui_ChangeXYZSigmasDlg(void)
{
	setupUi(this);
	connect(this->Cancel,SIGNAL(released()),this,SLOT(Cancel_released()));
	connect(this->OKButton,SIGNAL(released()),this,SLOT(OKButton_released()));
	
	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
}

bui_ChangeXYZSigmasDlg::~bui_ChangeXYZSigmasDlg(void)
{
}


void bui_ChangeXYZSigmasDlg::setSX( double sx )
{
	this->sx = sx;
	this->SXInput->setText(QString( "%1" ).arg(this->sx , 0, 'F', 8 ));
}

void bui_ChangeXYZSigmasDlg::setSY( double sy )
{
	this->sy = sy;
	this->SYInput->setText(QString( "%1" ).arg(this->sy, 0, 'F', 8 ));
}

void bui_ChangeXYZSigmasDlg::setSZ( double sz )
{
	this->sz = sz;
	this->SZInput->setText(QString( "%1" ).arg(this->sz , 0, 'F', 8 ));
}

double bui_ChangeXYZSigmasDlg::getSX()
{
	return this->sx;
}

double bui_ChangeXYZSigmasDlg::getSY()
{
 return this->sy;
}

double bui_ChangeXYZSigmasDlg::getSZ()
{
 return this->sz;
}


void bui_ChangeXYZSigmasDlg::Cancel_released()
{
    this->makechanges = false;
 this->close();
}

void bui_ChangeXYZSigmasDlg::OKButton_released()
{
    this->makechanges = true;
 this->sx = this->SXInput->text().toDouble();
 this->sy = this->SYInput->text().toDouble();
 this->sz = this->SZInput->text().toDouble();
 this->close();
}


bool bui_ChangeXYZSigmasDlg::getmakeChanges()
{
    return this->makechanges;
}
