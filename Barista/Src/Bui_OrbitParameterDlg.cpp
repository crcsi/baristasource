#include "Bui_OrbitParameterDlg.h"
#include "utilities.h"
#include "BaristaHtmlHelp.h"
#include "BaristaProject.h"
#include <QMessageBox>
#include <QDoubleValidator>
#include "Bui_SplineModelDlg.h"
#include "Bui_OrbitSettings.h"



Bui_OrbitParameterDlg::Bui_OrbitParameterDlg(CSplineModel* splineModel,SplineModelType splineType,QWidget *parent)
	: CAdjustmentDlg(parent),additionalParameterDlg(0),splineParameterDlg(0),oldTab(-1),splineModel(splineModel)
{
	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->defaultTab = 1; // the additional parameter
	this->tabWidget->setCurrentIndex(this->defaultTab);

	this->initSplineModelDlg(splineModel,splineType);
	this->initAdditionalParameterDlg(splineModel,splineType);

	if (this->splineModel)
		splineModel->addListener(this);
}

Bui_OrbitParameterDlg::~Bui_OrbitParameterDlg()
{
	this->cleanUp();
}

void Bui_OrbitParameterDlg::cleanUp()
{
	if (this->splineModel)
		this->splineModel->removeListener(this);
	this->splineModel = NULL;

	if (this->additionalParameterDlg)
		delete this->additionalParameterDlg;
	this->additionalParameterDlg = NULL;

	if (this->splineParameterDlg)
		delete this->splineParameterDlg;
	this->splineParameterDlg = NULL;

}

bool Bui_OrbitParameterDlg::okButtonReleased()
{
	this->success = false;

	if (this->additionalParameterDlg && this->splineParameterDlg)
	{
	
		this->additionalParameterDlg->setListenToSignal(false);
		this->splineParameterDlg->setListenToSignal(false);

		if (this->additionalParameterDlg->okButtonReleased())
			this->success = true;

		if (this->splineParameterDlg->okButtonReleased())
			this->success = true;

		this->additionalParameterDlg->setListenToSignal(true);
		this->splineParameterDlg->setListenToSignal(true);

	}

	return this->success;
}

bool Bui_OrbitParameterDlg::cancelButtonReleased()
{
	return true;
}

CCharString Bui_OrbitParameterDlg::helpButtonReleased()
{
	return "";
}


void Bui_OrbitParameterDlg::splineModelDlgStatusChanged(bool b)
{
	if (b)
	{
		this->tabWidget->setTabEnabled(1,true);
	}
	else
	{
		this->tabWidget->setTabEnabled(1,false);
	}

	emit enableOkButton(b);	
}


void Bui_OrbitParameterDlg::additionalParameterDlgStatusChanged(bool b)
{
	if (b)
	{
		this->tabWidget->setTabEnabled(0,true);
	}
	else
	{
		this->tabWidget->setTabEnabled(0,false);
	}

	emit enableOkButton(b);	
}

void Bui_OrbitParameterDlg::tWCurrentChanged( int index )
{
	if (oldTab != -1)
	{
		if (oldTab == 0 && this->splineParameterDlg)
		{
			this->splineParameterDlg->okButtonReleased();
		}
		else if (oldTab == 1 && this->additionalParameterDlg)
		{
			this->additionalParameterDlg->okButtonReleased();
		}
	}

	oldTab = index;

}

void Bui_OrbitParameterDlg::initSplineModelDlg(CSplineModel* sensorModel,SplineModelType splineType)
{
	this->splineParameterDlg = new Bui_SplineModelDlg(sensorModel,splineType,this);
	this->connect(this->splineParameterDlg,SIGNAL(enableOkButton(bool)),this,SLOT(splineModelDlgStatusChanged(bool)));
	this->tab_Spline->layout()->addWidget(this->splineParameterDlg);
}


void Bui_OrbitParameterDlg::initAdditionalParameterDlg(CSplineModel* sensorModel,SplineModelType splineType)
{

	this->additionalParameterDlg = new Bui_OrbitSettings(sensorModel,splineType,this);
	this->connect(this->additionalParameterDlg,SIGNAL(enableOkButton(bool)),this,SLOT(additionalParameterDlgStatusChanged(bool)));
	this->tab_Add->layout()->addWidget(this->additionalParameterDlg);


}

void Bui_OrbitParameterDlg::elementsChangedSlot(CLabel element)
{
	if (!this->listenToSignal)
		return;

	if (element == "CORBITPATHMODEL" || element == "CORBITATTIUDEMODEL" || element == "CSPLINEMODEL")
	{
		
	}
}