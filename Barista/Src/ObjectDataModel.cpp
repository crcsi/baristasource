#include "ObjectDataModel.h"

#include <QMenu>


CObjectDataModel::CObjectDataModel(ViewParameter viewParameter): vp(viewParameter),
	dataModelType(eModelNotInitialized)
{
	
}

CObjectDataModel::~CObjectDataModel(void)
{

}


void CObjectDataModel::setShowSelector(bool show) 
{
	this->beginResetModel();
	this->vp.showSelector = show;
	this->setHeader(this->guessHeader());
	this->endResetModel();
}
void CObjectDataModel::setShowSigma(bool show)
{
	this->beginResetModel();
	this->vp.showSigma = show;
	this->endResetModel();
}

void CObjectDataModel::setShowCoordinates(bool show)
{
	this->beginResetModel();
	this->vp.showCoordinates = show;
	this->endResetModel();
}

void CObjectDataModel::setAllowSigmaChange(bool allow)
{
	this->vp.allowSigmaChange = allow;
}

void CObjectDataModel::setHeader(const QStringList& headerList)
{
	this->headerList = headerList;
	if (this->headerList.count() && this->headerList.count() <= this->columnCount(QModelIndex()))
		emit this->headerDataChanged(Qt::Horizontal,0,this->headerList.count()-1);
}

void CObjectDataModel::setFormatString(const QStringList& format)
{
	this->beginResetModel();
	this->formatList = format;
	this->endResetModel();
}

void CObjectDataModel::setAllowDeletePoints(bool allow)
{
	this->vp.allowDeletePoint = allow;
}

void CObjectDataModel::setAllowChangeLabels(bool allow)
{
	this->vp.allowLabelChange = allow;
}

void CObjectDataModel::setAllowBestFitEllipse(bool allow)
{
	this->vp.allowBestFitEllipse = allow;
}

void CObjectDataModel::setAllowXYZAveragePoints(bool allow)
{
	this->vp.allowXYZAveragePoints = allow;
}



void CObjectDataModel::elementAdded(CLabel element) 
{
	if (!this->listenToSignal)
		return;

	this->beginResetModel();
	vector<QString> param;
	param.push_back(element.GetChar());
	emit  dataModelChanged(eObjectAdded,param);
	this->endResetModel(); // force the view to update
}

void CObjectDataModel::elementDeleted(CLabel element)
{
	if (!this->listenToSignal)
		return;	

	this->beginResetModel();
	vector<QString> param;
	param.push_back(element.GetChar());
	emit  dataModelChanged(eObjectDeleted,param);
	this->endResetModel(); // force the view to update
}

void CObjectDataModel::elementsChanged(CLabel element)
{
	if (!this->listenToSignal)
		return;

	this->beginResetModel();
	vector<QString> param;
	emit  dataModelChanged(eObjectName,param);
	this->endResetModel(); // force the view to update
}


void CObjectDataModel::rightMouseButtonReleased(const QModelIndexList &mappedList,vector<QAction*>& actions)
{
	if (actions.size())
	{
		QMenu* contextMenu = new QMenu(0);
		Q_CHECK_PTR( contextMenu );
 
		for (unsigned int i=0; i< actions.size(); i++)
			contextMenu->addAction(actions[i]);
		
		contextMenu->exec( QCursor::pos() );

		for (unsigned int i=0; i< actions.size(); i++)
			delete actions[i];

		actions.clear();

		delete contextMenu;
	}
}