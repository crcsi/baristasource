#include <QFileDialog>

#include "Bui_FeatureExtractionDialog.h"

#include "BaristaProject.h"
#include "Icons.h"
#include "VImage.h"
#include "BaristaHtmlHelp.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include "LabelImage.h"
#include "ProtocolHandler.h"
#include "BaristaProjectHelper.h"

bui_FeatureExtractionDialog::bui_FeatureExtractionDialog(QWidget* parent, FeatureExtractionParameters &pars, 
														 bool points, bool lines, bool regions, 
														 CFileName initialise) : 
               QDialog(parent), image(0), enablePoints (points), enableLines(lines), enableRegions (regions), 
			   parsAccepted(false), parameters(pars), labelimage(0), labelFileName("Regions.tif"),
			   iniFileName(initialise), boundaryPoly(0), boundaryPolyXYZ(0)
{
	if (!initialise.IsEmpty()) this->parameters.initParameters(initialise);

	this->setup();
	if (!enableLines && !enableRegions) parameters.Mode = eFoerstner;

	this->ImageSelectorComboBox->addItem("No data");
	this->ImageSelectorComboBox->setEnabled(false);

	this->init();
};

bui_FeatureExtractionDialog::bui_FeatureExtractionDialog(QWidget* parent, CVImage *I_image, CFileName initialise) : 
               QDialog(parent), image(I_image), enablePoints (true), enableLines(true), 
			   enableRegions (true), parsAccepted(false), labelimage(0),
			   iniFileName(initialise), boundaryPoly(0), boundaryPolyXYZ(0)
{
	if (!initialise.IsEmpty() && this->parameters.initParameters(initialise))
	{
	}
	else
	{
		this->parameters.setDerivativeFilter(eDiffXGauss,1.0f);
		this->parameters.NoiseModel = ePoisson;
		this->parameters.setIntegrationFilter(eGauss, 5, 5, 0.7f, 1.0f);	
		this->parameters.Mode = eFoerstner;
	}

	this->setup();

	this->parsAccepted = false;
	this->ImageSelectorComboBox->blockSignals(true);

	if (I_image) 
	{
		this->labelFileName = I_image->getFileNameChar();
		this->ImageSelectorComboBox->addItem(labelFileName.GetChar());
		this->labelFileName.SetPath(project.getFileName()->GetPath());
		this->labelFileName.SetFileName(this->labelFileName.GetFileName() + "_reg");
		if (this->labelFileName.GetFileExt() != CCharString("tif")) this->labelFileName.SetFileExt("tif");
	}
	else this->ImageSelectorComboBox->addItem("No data");

	this->ImageSelectorComboBox->blockSignals(false);

	this->ImageSelectorComboBox->setEnabled(false);

	this->init();
}
	
bui_FeatureExtractionDialog::bui_FeatureExtractionDialog(QWidget* parent, CXYZPolyLine *poly, CFileName initialise) : 
               QDialog(parent), image(0), enablePoints (true), enableLines(true), 
			   enableRegions (true), parsAccepted(false), labelimage(0),
			   iniFileName(initialise), boundaryPoly(0), boundaryPolyXYZ(poly)
{
	if (!initialise.IsEmpty()) this->parameters.initParameters(initialise);
	else
	{
		this->parameters.setDerivativeFilter(eDiffXGauss,1.0f);
		this->parameters.NoiseModel = ePoisson;
		this->parameters.setIntegrationFilter(eGauss, 5, 5, 0.7f, 1.0f);	
		this->parameters.Mode = eFoerstner;
	}

	this->setup();

	this->parsAccepted = false;


	this->ImageSelectorComboBox->setEnabled(true);
	this->initImages();

	if (this->image) 
	{
		this->labelFileName = this->image->getFileNameChar();
		this->labelFileName.SetPath(project.getFileName()->GetPath());
		CCharString fn = this->labelFileName.GetFileName();
		if (poly) fn += poly->getLabel().GetChar();
		fn += "_reg";
		this->labelFileName.SetFileName(fn);
		if (this->labelFileName.GetFileExt() != CCharString("tif")) this->labelFileName.SetFileExt("tif");
	}

	this->init();
};

bui_FeatureExtractionDialog::~bui_FeatureExtractionDialog()
{
	image = 0;
	if (labelimage) delete labelimage;

	labelimage = 0;

	if (boundaryPoly) delete boundaryPoly;

	this->boundaryPolyXYZ = 0;
}
	
void bui_FeatureExtractionDialog::setup()
{
	setupUi(this);
	
	connect(this->buttonOK,               SIGNAL(released()),               this, SLOT(OK()));
	connect(this->buttonCancel,           SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->buttonHelp,             SIGNAL(released()),               this, SLOT(Help()));
	connect(this->DerivativeKernel,       SIGNAL(currentIndexChanged(int)), this, SLOT(checkValuesDerivative()));
	connect(this->FoerstnerKernel,        SIGNAL(currentIndexChanged(int)), this, SLOT(checkValuesFoerstnerKernel()));

	connect(this->CannyRadio,             SIGNAL(clicked(bool)),            this, SLOT(checkValuesMode()));
	connect(this->FoerstnerRadioButton,   SIGNAL(clicked(bool)),            this, SLOT(checkValuesMode()));
	connect(this->WatershedModel,         SIGNAL(clicked(bool)),            this, SLOT(checkValuesMode()));

	connect(this->extractEdges,           SIGNAL(stateChanged(int)),        this, SLOT(checkValuesFeatureTypes()));
	connect(this->extractPoints,          SIGNAL(stateChanged(int)),        this, SLOT(checkValuesFeatureTypes()));
	connect(this->extractRegions,         SIGNAL(stateChanged(int)),        this, SLOT(checkValuesFeatureTypes()));

	connect(this->SigmaSmooth,            SIGNAL(editingFinished()),        this, SLOT(checkValuesDerivative()));
	connect(this->WatershedSmoothing,     SIGNAL(editingFinished()),        this, SLOT(checkValuesDerivative()));
	connect(this->NoiseModel,             SIGNAL(currentIndexChanged(int)), this, SLOT(checkValuesGeneral()));
	connect(this->SignificanceLevel,      SIGNAL(editingFinished()),        this, SLOT(checkValuesGeneral()));	
	connect(this->EdgeThinning,           SIGNAL(editingFinished()),        this, SLOT(checkValuesGeneral()));
	connect(this->minLineLength,          SIGNAL(editingFinished()),        this, SLOT(checkValuesGeneral()));
	connect(this->IntegrationScaleEdges,  SIGNAL(editingFinished()),        this, SLOT(checkValuesFoerstnerKernel()));
	connect(this->IntegrationScalePoints, SIGNAL(editingFinished()),        this, SLOT(checkValuesFoerstnerKernel()));
	connect(this->Qmin,                   SIGNAL(editingFinished()),        this, SLOT(checkValuesFoerstner()));
	connect(this->NonMaxPoints,           SIGNAL(editingFinished()),        this, SLOT(checkValuesFoerstner()));


	connect(this->ImageSelectorComboBox,  SIGNAL(currentIndexChanged(int)), this, SLOT(ImageChanged()));
	connect(this->morphologicSize,        SIGNAL(editingFinished()),        this, SLOT(checkValuesRegion()));
	connect(this->PercentageLineEdit,     SIGNAL(editingFinished()),        this, SLOT(checkValuesRegion()));
	connect(this->VarianceRatioEdit,      SIGNAL(editingFinished()),        this, SLOT(checkValuesRegion()));
	connect(this->SimilarityEdit,         SIGNAL(editingFinished()),        this, SLOT(checkValuesRegion()));
	connect(this->SimilarityEdit,         SIGNAL(editingFinished()),        this, SLOT(checkValuesRegion()));
	connect(this->kWatershed,             SIGNAL(editingFinished()),        this, SLOT(kWatershedChanged()));
	
	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
}


void bui_FeatureExtractionDialog::init()
{
	this->parsAccepted = false;

	if (!this->enableLines)   this->parameters.extractEdges = false;
	if (!this->enablePoints)  this->parameters.extractPoints = false;
	if (!this->enableRegions) this->parameters.extractRegions = false;

	this->DerivativeKernel->blockSignals(true);
	this->NoiseModel->blockSignals(true);		
	this->FoerstnerKernel->blockSignals(true);	

	this->DerivativeKernel->addItem("Simple Derivative");
	this->DerivativeKernel->addItem("Sobel Operator");
	this->DerivativeKernel->addItem("Derivative of Gaussian");
	this->DerivativeKernel->addItem("Deriche Operator");

	this->NoiseModel->addItem("Percentile");
	this->NoiseModel->addItem("Gaussian");
	this->NoiseModel->addItem("Poisson");

	this->FoerstnerKernel->addItem("Gaussian Filter");
	this->FoerstnerKernel->addItem("Binomial Filter");
	this->FoerstnerKernel->addItem("Moving Average");

	this->DerivativeKernel->blockSignals(false);	
	this->NoiseModel->blockSignals(false);	
	this->FoerstnerKernel->blockSignals(false);	

	this->setParameters();

	this->buttonOK->setFocusPolicy(Qt::NoFocus);	
	this->buttonCancel->setFocusPolicy(Qt::NoFocus);
}

void bui_FeatureExtractionDialog::checkValuesGeneral()
{
	eNoiseModel Model = ePoisson;  

	if (this->NoiseModel->currentIndex() == 0) Model = eMedian;
	else if (this->NoiseModel->currentIndex() == 1) Model = eGaussian;

	this->parameters.NoiseModel = Model;

	bool OK = false;
	float alpha = SignificanceLevel->text().toDouble(&OK);
	if ((!OK) || (alpha <= 0.0f) || (alpha >= 1.0f))
		alpha = this->parameters.getAlpha();
	this->parameters.setAlpha(alpha);

	QString ds=QString("%1").arg(alpha, 0, 'f', 3);
	this->SignificanceLevel->setText(ds);

	float edgeThinning = EdgeThinning->text().toDouble(&OK); 
	if ((!OK) || (edgeThinning <= 0.0f) || (edgeThinning >= 25.0f))
		edgeThinning = this->parameters.lineThinningThreshold;
	else this->parameters.lineThinningThreshold = edgeThinning;

	ds=QString("%1").arg(edgeThinning, 0, 'f', 1);
	this->EdgeThinning->setText(ds);
	
	float lineLength = minLineLength->text().toDouble(&OK); 
	if ((!OK) || (lineLength <= 0.0f))
		lineLength = this->parameters.minLineLength;
	else this->parameters.minLineLength = lineLength;

	ds=QString("%1").arg(lineLength, 0, 'f', 1);
	this->minLineLength->setText(ds);
};


void bui_FeatureExtractionDialog::kWatershedChanged()
{
	bool OK;
	this->kWatershed->blockSignals(true); 

	float k = (float) this->kWatershed->text().toDouble(&OK);

	if ((!OK) || (k < 0.0f)) k = this->parameters.getKforWatershed();
	
	this->parameters.setKforWatershed(k);

	QString ds=QString("%1").arg(k, 0, 'f', 2);
	this->kWatershed->setText(ds);

	
	this->kWatershed->blockSignals(false); 
};

void bui_FeatureExtractionDialog::checkValuesDerivative()
{
	eFilterType typ = eDiffX;

	if (DerivativeKernel->currentIndex() == 2)
	{
		SigmaSmooth->setEnabled(true);
		typ = eDiffXGauss;
	}
	else if (DerivativeKernel->currentIndex() == 3)
	{
		SigmaSmooth->setEnabled(true);
		typ = eDiffXDeriche;
	}
	else
	{
		SigmaSmooth->setEnabled(false);
		if (DerivativeKernel->currentIndex() == 1) typ = eSobelX;
	};

	bool OK = false;
	float sigma = SigmaSmooth->text().toDouble(&OK);
	if ((!OK) || (sigma < 0.1f) || (sigma > 20.0f))
	{
		sigma = this->parameters.getSigmaSmooth();
		if (sigma < 0) sigma = 0.5f;

		parameters.setDerivativeFilter(typ,sigma);
		QString ds=QString("%1").arg(sigma, 0, 'f', 2);
		this->SigmaSmooth->setText(ds);
	}
	else 
	{
		if ((typ == eDiffXGauss) || (typ == eDiffXDeriche))
		{
			QString ds=QString("%1").arg(sigma, 0, 'f', 2);
			this->SigmaSmooth->setText(ds);
		};
		parameters.setDerivativeFilter(typ,sigma);
	}
	this->DerivativeKernel->setFocus();

	this->WatershedSmoothing->blockSignals(true);
	int it = this->WatershedSmoothing->text().toInt(&OK);
	if (OK && it >= 0)
	{	
		this->parameters.setWatershedSmoothingIterations(it);
		QString ds=QString("%1").arg(it);
		this->WatershedSmoothing->setText(ds);
	}
	this->WatershedSmoothing->blockSignals(false);
}

void bui_FeatureExtractionDialog::labelBrowsePushed()
{
	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a file name",
		labelFileName.GetChar(),
		"TIFF files (*.tif)");


	this->labelFileName = s.toLatin1();

	this->LabelFile->blockSignals(true);
	this->LabelFile->setText(this->labelFileName.GetChar());
	this->LabelFile->blockSignals(false);
};

void bui_FeatureExtractionDialog::checkValuesMode()
{
	this->CannyRadio->blockSignals(true);
	this->FoerstnerRadioButton->blockSignals(true);
	this->WatershedModel->blockSignals(true);

	if (this->parameters.Mode == eCanny)
	{
		this->CannyRadio->setChecked(false);
	}
	else if (this->parameters.Mode == eFoerstner)
	{
		this->FoerstnerRadioButton->setChecked(false);
	}
	else
	{
		this->WatershedModel->setChecked(false);
	}

	if (this->CannyRadio->isChecked()) 
	{
		disableFoerstnerPars();
		this->DerivativeKernel->setEnabled(true);
		this->extractPoints->setEnabled(false);
		this->WatershedSmoothing->setEnabled(false);
		parameters.Mode = eCanny;
	}
	else if (this->FoerstnerRadioButton->isChecked()) 
	{
		enableFoerstnerPars();
		this->DerivativeKernel->setEnabled(true);
		this->extractPoints->setEnabled(true);
		this->WatershedSmoothing->setEnabled(false);
		parameters.Mode = eFoerstner;
	}
	else
	{
		disableFoerstnerPars();
		this->DerivativeKernel->setEnabled(true);
		this->extractPoints->setEnabled(false);
		this->extractRegions->setEnabled(true);
		this->WatershedSmoothing->setEnabled(true);
		parameters.Mode = eWaterShed;
	};

	FoerstnerRadioButton->setFocus();

	this->CannyRadio->blockSignals(false);
	this->FoerstnerRadioButton->blockSignals(false);
	this->WatershedModel->blockSignals(false);
};


void bui_FeatureExtractionDialog::checkValuesFeatureTypes()
{

	if (extractEdges->isChecked()) 
	{
		IntegrationScaleEdges->setEnabled(true);
		this->parameters.extractEdges = true;
	}
	else
	{
		IntegrationScaleEdges->setEnabled(false);
		this->parameters.extractEdges = false;
	};

	if (extractPoints->isChecked()) 
	{
		IntegrationScalePoints->setEnabled(true);
		NonMaxPoints->setEnabled(true);
		this->parameters.extractPoints = true;
	}
	else
	{
		IntegrationScalePoints->setEnabled(false);
		NonMaxPoints->setEnabled(false);
		this->parameters.extractPoints = false;
	};

	if (this->enableRegions && extractRegions->isChecked()) 
	{
		this->morphologicSize->setEnabled(true);
		this->LabelBrowse->setEnabled(true);
		this->parameters.extractRegions = true;
	}
	else
	{
		this->morphologicSize->setEnabled(false);
		this->LabelBrowse->setEnabled(false);
		this->parameters.extractRegions = false;
	};
};
	
int bui_FeatureExtractionDialog::getMaxNonMax()
{
	int maxNonMax = 1000;
	
	if (this->image)
	{
		maxNonMax = this->image->getHugeImage()->getWidth() > this->image->getHugeImage()->getHeight()? 
			this->image->getHugeImage()->getHeight() / 2 : this->image->getHugeImage()->getWidth() / 2;

		if (this->image->getHugeImage()->getRoiPtr())
		{
			maxNonMax = this->image->getHugeImage()->getRoiPtr()->getWidth() > this->image->getHugeImage()->getRoiPtr()->getHeight()? 
				this->image->getHugeImage()->getRoiPtr()->getHeight() / 2 : this->image->getHugeImage()->getRoiPtr()->getWidth() / 2;
		}
	}
	return maxNonMax;
};
	
void bui_FeatureExtractionDialog::checkValuesFoerstner()
{
	bool OK = false;
	float qmin = Qmin->text().toDouble(&OK);
	if ((!OK) || (qmin <= 0.0f) || (qmin >= 1.0f))
		qmin = this->parameters.qMin;
	else this->parameters.qMin = qmin;
	QString ds=QString("%1").arg(qmin, 0, 'f', 2);
	this->Qmin->setText(ds);

	int maxNonMax = getMaxNonMax();

	int nonMax = int(floor(NonMaxPoints->text().toDouble(&OK) + 0.5f));
	if ((!OK) || (nonMax < 1) || (nonMax > maxNonMax))
		nonMax = this->parameters.distNonmaxPts;
	
	if (nonMax > maxNonMax) nonMax = maxNonMax;

	this->parameters.distNonmaxPts = nonMax;

	ds=QString("%1").arg(float(nonMax), 0, 'f', 0);
	this->NonMaxPoints->setText(ds);
};
	
void bui_FeatureExtractionDialog::checkValuesFoerstnerKernel()
{
	eFilterType typ = eGauss;
	if (FoerstnerKernel->currentIndex() == 1) typ = eBinom;
	else if (FoerstnerKernel->currentIndex() == 2) typ = eAverage;

	bool OK = false;
	float scaleEdges = IntegrationScaleEdges->text().toDouble(&OK);
	if ((!OK) || (scaleEdges <= 0.0f) || (scaleEdges >= 10.0f))
		scaleEdges = this->parameters.getLineScale();

	float scalePoints = IntegrationScalePoints->text().toDouble(&OK);
	if ((!OK) || (scalePoints <= 0.0f) || (scalePoints >= 10.0f))
		scalePoints = this->parameters.getPointScale();
	if (scalePoints < scaleEdges) 
	{
		if (parameters.extractEdges) scalePoints = scaleEdges;
		else scaleEdges = scalePoints;
	}

	eFilterType oldTyp = this->parameters.getIntegrationFilterEdges().getType();
	int digits = (typ == eGauss) ? 2 : 0;

	if (typ != oldTyp)
	{
		if (typ == eGauss)
		{
			scalePoints = (scalePoints - 1.0f) / 6.0f;
			scaleEdges  = (scaleEdges  - 1.0f) / 6.0f;
		}
		else if (oldTyp == eGauss)
		{
			scalePoints = this->parameters.getIntegrationFilterPoints().getSizeX();
			scaleEdges  = this->parameters.getIntegrationFilterEdges().getSizeX();
		}
	};

	this->parameters.setIntegrationFilter(typ,scaleEdges,scalePoints,scaleEdges,scalePoints);

	QString ds=QString("%1").arg(scalePoints, 0, 'f', digits);
	this->IntegrationScalePoints->setText(ds);

	ds=QString("%1").arg(scaleEdges, 0, 'f', digits);
	this->IntegrationScaleEdges->setText(ds);
};
	
void bui_FeatureExtractionDialog::initImages()
{
	this->ImageSelectorComboBox->clear();

	this->ImageSelectorComboBox->blockSignals(true);
	if (project.getNumberOfImages() < 1)
	{
		this->ImageSelectorComboBox->addItem("No data");
	}
	else
	{
		for (int i = 0; i < project.getNumberOfImages(); ++i)
		{
			this->ImageSelectorComboBox->addItem(project.getImageAt(i)->getFileNameChar());
		}

		this->ImageSelectorComboBox->setCurrentIndex(0);
		this->image = project.getImageAt(0);
	}
	this->ImageSelectorComboBox->blockSignals(false);
};

void bui_FeatureExtractionDialog::ImageChanged()
{
	int index = this->ImageSelectorComboBox->currentIndex();
	if (index < project.getNumberOfImages())
	{
		this->image = project.getImageAt(index);
	}
	else 
	{
		this->ImageSelectorComboBox->setCurrentIndex(0);
		if (project.getNumberOfImages() < 1)  this->image = project.getImageAt(0);
		else this->image = 0;
	}

};
	
void bui_FeatureExtractionDialog::setBoundaryPoly(CXYZPolyLine *poly)
{
	if (this->boundaryPoly) delete this->boundaryPoly;
	this->boundaryPoly = 0;

	CCharString path = this->labelFileName.GetPath();
	if (path.IsEmpty()) this->labelFileName.SetPath(project.getFileName()->GetPath());
	this->labelFileName.SetFileExt("tif");
	CCharString fn = this->labelFileName.GetFileName();
	
	CCharString polyLabel(poly->getLabel().GetChar());

	if (!fn.IsEmpty())
	{
		if (this->boundaryPolyXYZ)
		{
			if (fn.Find(this->boundaryPolyXYZ->getLabel().GetChar()))
			{
				fn.replace(this->boundaryPolyXYZ->getLabel().GetChar(), polyLabel.GetChar());
			}
			else fn += polyLabel + "_reg";
		}
		else fn += polyLabel + "_reg";
	}
	else
	{
		fn = polyLabel + "_reg";
	}


	this->labelFileName.SetFileName(fn);
	this->boundaryPolyXYZ = poly;
};

void bui_FeatureExtractionDialog::initBoundaryPoly()
{
	if (this->boundaryPoly) delete this->boundaryPoly;
	this->boundaryPoly = 0;

	if (this->boundaryPolyXYZ && this->image)
	{
		const CSensorModel *model = this->image->getCurrentSensorModel();
		if (!model && this->image->hasTFW()) model =  this->image->getTFW();
		if (model)
		{
			this->boundaryPoly = new CXYPolyLine();

			int xmin = this->image->getHugeImage()->getWidth()  + this->parameters.getTotalOffset();
			int ymin = this->image->getHugeImage()->getHeight() + this->parameters.getTotalOffset();
			int xmax = -this->parameters.getTotalOffset();
			int ymax = this->parameters.getTotalOffset();

			CXYPoint p;
			for (int i = 0; i < this->boundaryPolyXYZ->getPointCount(); ++i)
			{
				CXYZPoint *P = this->boundaryPolyXYZ->getPoint(i);
				model->transformObj2Obs(p, *P);
				this->boundaryPoly->addPoint(p);
				if (p.x > xmax) xmax = int(floor(p.x)) + 1;
				if (p.y > ymax) ymax = int(floor(p.y)) + 1;
				if (p.x < xmin) xmin = int(floor(p.x));
				if (p.y < ymin) ymin = int(floor(p.y));
			}

			if (this->boundaryPoly->getPointCount() > 1) 
			{
				CXYPoint p0 = this->boundaryPoly->getPoint(0);
				CXYPoint pN = this->boundaryPoly->getPoint(this->boundaryPoly->getPointCount() - 1);
				if (p0.getDistance(pN) < 0.01) this->boundaryPoly->removePoint(this->boundaryPoly->getPointCount() - 1);
			}
			xmin -= this->parameters.getTotalOffset();
			ymin -= this->parameters.getTotalOffset();
			xmax += this->parameters.getTotalOffset();
			ymax += this->parameters.getTotalOffset();
			if (xmin < 0) xmin = 0;
			if (ymin < 0) ymin = 0;
			if (xmax >= this->image->getHugeImage()->getWidth())  xmax = this->image->getHugeImage()->getWidth()  - 1;
			if (ymax >= this->image->getHugeImage()->getHeight()) ymax = this->image->getHugeImage()->getHeight() - 1;

			int w = xmax - xmin + 1;
			int h = ymax - ymin + 1;

			this->boundaryPoly->closeLine();
			if ((this->boundaryPoly->getPointCount() < 3) || (xmin >= this->image->getHugeImage()->getWidth()) || 
				(ymin >= this->image->getHugeImage()->getHeight()) || (xmax <=0) || (ymax <=0) 
				|| (w < this->parameters.getTotalOffset()) || (h < this->parameters.getTotalOffset()))
			{
				delete this->boundaryPoly;
				this->boundaryPoly = 0;
			}
			else
			{
				this->image->getHugeImage()->setRoi(xmin, ymin, w, h);
			}
		}
	}
};

//===============================================
void bui_FeatureExtractionDialog::Cancel()
{
	this->parsAccepted = false;

	this->close();
}

//===============================================
void bui_FeatureExtractionDialog::OK()
{
	CImageBuffer *buf = 0;

	this->initBoundaryPoly();

	if (image) buf = image->getHugeImage()->getRoiPtr();
	if (buf) 
	{
		this->parameters.writeParameters(iniFileName);


		CLabelImage labels(buf->getOffsetX(), buf->getOffsetY(), buf->getWidth(), buf->getHeight(), 8);

		CFeatureExtractionThread *thread = new CFeatureExtractionThread(buf,this->boundaryPoly, image->getXYPoints(), 
			                                                            image->getXYContours(), image->getXYEdges(),
														                &labels, this->parameters);

		bui_ProgressDlg dlg(thread, thread);
		dlg.exec();

		if (dlg.getSuccess() && (this->parameters.extractRegions))
		{
			CVImage *IMG = project.addImage(this->labelFileName, false);
			CCharString str = IMG->getName() + ".hug";
	
			CLookupTable lut = labels.get16BitLookup();
			if (!IMG->getHugeImage()->dumpImage(str.GetChar(), labels, &lut))
				project.deleteImage(IMG);
		}

		baristaProjectHelper.refreshTree();
	}

	this->parsAccepted = true;

/*	CFeatureExtractor extractor (this->parameters);
	extractor.setRoi(image->getHugeImage()->getRoiPtr());

	CXYEllipse ellipse;
	if (extractor.extractEllipse(ellipse))
	{
		image->getEllipses()->Add(ellipse);
	}
	
	extractor.resetRoi();
*/


/*
	for (int i = 0; i < image->getXYEdges()->GetSize(); ++i)
	{
		CXYPolyLine *cont = image->getXYEdges()->GetAt(i);
		vector<C2DPoint> contVec;
		for (int j = 0; j < cont->getPointCount(); ++j)
		{
			CXYPoint p = cont->getPoint(j);
			contVec.push_back(p.get2DPoint());
		}

		CXYEllipse ellipse;
		if (ellipse.init(contVec)) image->getEllipses()->Add(ellipse);
	};

	for (int i = 0; i < image->getXYContours()->GetSize(); ++i)
	{
		CXYContour *cont = image->getXYContours()->GetAt(i);
		vector<C2DPoint> contVec;
		for (int j = 0; j < cont->getPointCount(); ++j)
		{
			C2DPoint *p = cont->getPoint(j);
			contVec.push_back(*p);
		}

		CBestFit2DEllipse bestfitellipse;
		CCharStringArray Labels;
		CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();

		Matrix obs;

		int nObs = contVec.size();
		obs.ReSize(nObs, 2);
     
		CLabel ptLabel("0");
		for(int i = 0; i < nObs; i++)
		{
			ptLabel++;
			obs.element(i, 0) = contVec[i].x;
			obs.element(i, 1) = contVec[i].y;
			// Add covariance
			Matrix cov(2,2);
			cov = 0;	
			cov.element(0, 0) = 0.5f;
			cov.element(1, 1) = 0.5f;
			covariance->addSubMatrix(cov);
			// Add label
			CCharString* str;
			str = Labels.Add();
			*str = ptLabel.GetChar();
		}

		int nPara = 5;

		CLeastSquaresSolver lss(&bestfitellipse, &obs, &Labels, NULL, covariance);

		bool maybe = lss.solve();

		if ( maybe )
		{
			Matrix result;
			result.ReSize(nPara, 1);
			result = 0;
			Matrix covmat;
			covmat.ReSize(nPara, nPara);
			covmat = 0;
			for(int i = 0; i < nPara; i++)
			{
				result.element(i, 0) = lss.getSolution()->element(i, 0);			
				for(int j = 0; j < nPara; j++)
				{
					covmat.element(i, j) = lss.getCovariance()->element(i, j);
				}
			}

			CXYEllipse ellipse;
			CLabel label = "Ellipse";
			label += image->getEllipses()->GetSize();
			ellipse.set(result, label);
			ellipse.setCovariance(covmat);
		//CXYPoint center = ellipse.getCenter();
		//image->getXYPoints()->Add(center);

			image->getEllipses()->Add(ellipse);
		}	
	}
*/
	this->close();
}

void bui_FeatureExtractionDialog::checkValuesRegion()
{
	this->morphologicSize->blockSignals(true);
	this->SimilarityEdit->blockSignals(true);
	this->PercentageLineEdit->blockSignals(true);
	this->VarianceRatioEdit->blockSignals(true);

	bool OK = false;
	int size = this->morphologicSize->text().toInt(&OK);
	if (OK &&  (size >= 0.0f))
	{
		this->parameters.setMorphologicFilterSize(size);
	}

	QString	ds = QString("%1").arg(parameters.getMorphologicFilterSize(), 0);
	this->morphologicSize->setText(ds);

	double val = this->SimilarityEdit->text().toDouble(&OK);
	if (OK &&  (val > 0.0))
	{
		this->parameters.RegionSimilarityThreshold = (float) val;
	}
	ds = QString("%1").arg(parameters.RegionSimilarityThreshold, 0, 'f', 2);
	this->SimilarityEdit->setText(ds);

	val = this->VarianceRatioEdit->text().toDouble(&OK);
	if (OK &&  (val > 0.0))
	{
		this->parameters.RegionVarianceRatioThreshold = (float) val;
	}
	ds = QString("%1").arg(parameters.RegionVarianceRatioThreshold, 0, 'f', 2);
	this->VarianceRatioEdit->setText(ds);

	val = this->PercentageLineEdit->text().toDouble(&OK) * 0.01;
	if (OK &&  (val >= 0.0) &&  (val <= 1.0))
	{
		this->parameters.RegionTextureThreshold = (float) val;
	}
	ds = QString("%1").arg(parameters.RegionTextureThreshold * 100.0, 0, 'f', 1);
	this->PercentageLineEdit->setText(ds);

	this->morphologicSize->blockSignals(false);
	this->SimilarityEdit->blockSignals(false);
	this->PercentageLineEdit->blockSignals(false);
	this->VarianceRatioEdit->blockSignals(false);
}

void bui_FeatureExtractionDialog::enableFoerstnerPars()
{
	this->FoerstnerPars->setEnabled(true);
	FoerstnerKernel->setEnabled(true);

	Qmin->setEnabled(true);
	if (extractEdges->isChecked()) IntegrationScaleEdges->setEnabled(true);
	else IntegrationScaleEdges->setEnabled(false);
	if (extractPoints->isChecked())
	{
		IntegrationScalePoints->setEnabled(true);
		NonMaxPoints->setEnabled(true);
	}
	else 
	{
		IntegrationScalePoints->setEnabled(false);
		NonMaxPoints->setEnabled(false);
	}
	
	if (!extractRegions->isChecked() || (!this->enableRegions))
	{
		this->morphologicSize->setEnabled(false);
		this->SimilarityEdit->setEnabled(false);
		this->PercentageLineEdit->setEnabled(false);
		this->VarianceRatioEdit->setEnabled(false);
	}
	else
	{
		this->morphologicSize->setEnabled(true);
		this->SimilarityEdit->setEnabled(true);
		this->PercentageLineEdit->setEnabled(true);
		this->VarianceRatioEdit->setEnabled(true);
	}
};

	
void bui_FeatureExtractionDialog::disableFoerstnerPars()
{
	this->FoerstnerPars->setEnabled(false);
	FoerstnerKernel->setEnabled(false);
	Qmin->setEnabled(false);
	IntegrationScaleEdges->setEnabled(false);
	IntegrationScalePoints->setEnabled(false);
	NonMaxPoints->setEnabled(false);
};

void bui_FeatureExtractionDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/FeatureExtractionDlg.html");
};


void bui_FeatureExtractionDialog::setParameters()
{
	this->SigmaSmooth->blockSignals(true);
	this->DerivativeKernel->blockSignals(true);	
	this->NoiseModel->blockSignals(true);		
	this->FoerstnerKernel->blockSignals(true);	
	this->IntegrationScalePoints->blockSignals(true);	
	this->IntegrationScaleEdges->blockSignals(true);	
	this->SignificanceLevel->blockSignals(true);	
	this->CannyRadio->blockSignals(true);	
	this->FoerstnerRadioButton->blockSignals(true);
	this->WatershedModel->blockSignals(true);	
	this->extractPoints->blockSignals(true);
	this->extractEdges->blockSignals(true);
	this->extractRegions->blockSignals(true);
	this->morphologicSize->blockSignals(true);
	this->WatershedSmoothing->blockSignals(true);
	this->SimilarityEdit->blockSignals(true);
	this->PercentageLineEdit->blockSignals(true);
	this->VarianceRatioEdit->blockSignals(true);
	this->kWatershed->blockSignals(true);

	if (!this->enableLines && !this->enableRegions) 
	{
		parameters.Mode = eFoerstner;
		CannyRadio->setEnabled(false);
		this->WatershedModel->setEnabled(false);
		FoerstnerRadioButton->setEnabled(false);
	}
	if (this->enablePoints) this->extractPoints->setEnabled(true);
	else this->extractPoints->setEnabled(false);

	if (!this->enableLines)
	{
		this->extractEdges->setEnabled(false);
		this->EdgeThinning->setEnabled(false);
		this->minLineLength->setEnabled(false);
	}

	if (this->enableRegions && this->parameters.extractRegions) 
	{
		this->morphologicSize->setEnabled(true);
		this->SimilarityEdit->setEnabled(true);
		this->PercentageLineEdit->setEnabled(true);
		this->VarianceRatioEdit->setEnabled(true);
	}
	else 
	{
		this->morphologicSize->setEnabled(false);		
		this->extractRegions->setEnabled(false);
		this->SimilarityEdit->setEnabled(false);
		this->PercentageLineEdit->setEnabled(false);
		this->VarianceRatioEdit->setEnabled(false);
	}

	eFilterType filterTyp  = this->parameters.getDerivativeFilterX().getType();

	if (filterTyp == eDiffX) 
	{
		this->DerivativeKernel->setCurrentIndex(0);
		this->SigmaSmooth->setEnabled(false);
	}
	else if (filterTyp == eSobelX) 
	{
		this->SigmaSmooth->setEnabled(false);
		this->DerivativeKernel->setCurrentIndex(1);
	}
	else if (filterTyp == eDiffXGauss)
	{
		this->DerivativeKernel->setCurrentIndex(2);
		this->SigmaSmooth->setEnabled(true);
	}
	else if (filterTyp == eDiffXDeriche)
	{
		this->DerivativeKernel->setCurrentIndex(3);
		this->SigmaSmooth->setEnabled(true);
	}

	this->DerivativeKernel->setFocus();


	if (this->parameters.NoiseModel == eMedian)
		this->NoiseModel->setCurrentIndex(0);
	else if (this->parameters.NoiseModel == eGaussian)
		this->NoiseModel->setCurrentIndex(1);
	else this->NoiseModel->setCurrentIndex(2);

	QString ds=QString("%1").arg(parameters.getSigmaSmooth(), 0, 'f', 2);
	this->SigmaSmooth->setText(ds);

	ds=QString("%1").arg(parameters.getAlpha(), 0, 'f', 3);
	this->SignificanceLevel->setText(ds);

	ds=QString("%1").arg(parameters.getWatershedSmoothingIterations());
	this->WatershedSmoothing->setText(ds);

	filterTyp = this->parameters.getIntegrationFilterPoints().getType();

	if (filterTyp == eGauss) 
	{
		this->FoerstnerKernel->setCurrentIndex(0);
		ds=QString("%1").arg(this->parameters.getPointScale(), 0, 'f', 2);
		this->IntegrationScalePoints->setText(ds);
		ds=QString("%1").arg(this->parameters.getLineScale(), 0, 'f', 2);
		this->IntegrationScaleEdges->setText(ds);
	}
	else 
	{
		float size = (float) this->parameters.getIntegrationFilterPoints().getSizeX();
		ds=QString("%1").arg(size, 0, 'f', 0);
		this->IntegrationScalePoints->setText(ds);
		size = this->parameters.getIntegrationFilterEdges().getSizeX();
		ds=QString("%1").arg(size, 0, 'f', 0);
		this->IntegrationScaleEdges->setText(ds);

		if (filterTyp == eBinom) this->FoerstnerKernel->setCurrentIndex(1);
		else this->FoerstnerKernel->setCurrentIndex(2);
	}



	if (parameters.Mode == eFoerstner)
	{
		CannyRadio->setChecked(false);
		WatershedModel->setChecked(false);
		FoerstnerRadioButton->setChecked(true);
		if (this->parameters.extractPoints) this->extractPoints->setChecked(true);
		else this->extractPoints->setChecked(false);
		if (this->parameters.extractRegions) this->extractRegions->setChecked(true);
		else this->extractRegions->setChecked(false);
		if (this->parameters.extractEdges) this->extractEdges->setChecked(true);
		else this->extractEdges->setChecked(false);
		this->DerivativeKernel->setEnabled(true);
		this->WatershedSmoothing->setEnabled(false);		
	}
	else if (parameters.Mode == eCanny)
	{
		CannyRadio->setChecked(true);
		WatershedModel->setChecked(false);
		FoerstnerRadioButton->setChecked(false);
		if (this->parameters.extractEdges) extractEdges->setChecked(true);
		else extractEdges->setChecked(false);
		if (this->parameters.extractRegions) this->extractRegions->setChecked(true);
		else this->extractRegions->setChecked(false);
		this->WatershedSmoothing->setEnabled(false);
		this->extractPoints->setChecked(false);
		this->extractPoints->setEnabled(false);
		this->DerivativeKernel->setEnabled(true);
		disableFoerstnerPars();
	}
	else if (parameters.Mode == eWaterShed)
	{
		CannyRadio->setChecked(false);
		WatershedModel->setChecked(true);
		FoerstnerRadioButton->setChecked(false);
		if (this->parameters.extractEdges) extractEdges->setChecked(true);
		else extractEdges->setChecked(false);
		if (this->parameters.extractRegions) this->extractRegions->setChecked(true);
		else this->extractRegions->setChecked(false);
		this->extractPoints->setChecked(false);
		this->extractPoints->setEnabled(false);
		this->extractRegions->setEnabled(true);
		this->WatershedSmoothing->setEnabled(true);
		disableFoerstnerPars();
	}

	if (parameters.Mode == eFoerstner) enableFoerstnerPars();
	else disableFoerstnerPars();

	if (this->extractRegions)
	{
		this->LabelFile->setEnabled(false);
		this->LabelBrowse->setEnabled(true);
		this->LabelFile->setText(this->labelFileName.GetChar());
	}
	else
	{
		this->LabelFile->setEnabled(false);
		this->LabelBrowse->setEnabled(false);
		this->LabelFile->setText("");		
	}

	ds=QString("%1").arg(parameters.lineThinningThreshold, 0, 'f', 1);
	this->EdgeThinning->setText(ds);
	ds=QString("%1").arg(parameters.minLineLength, 0, 'f', 1);
	this->minLineLength->setText(ds);

	ds=QString("%1").arg(parameters.qMin, 0, 'f', 2);
	this->Qmin->setText(ds);

	int maxNonMax = this->getMaxNonMax();
	if (parameters.distNonmaxPts > maxNonMax) parameters.distNonmaxPts = maxNonMax;

	ds=QString("%1").arg((float) parameters.distNonmaxPts, 0, 'f', 0);
	this->NonMaxPoints->setText(ds);

	ds = QString("%1").arg(parameters.getMorphologicFilterSize(), 0);
	this->morphologicSize->setText(ds);

	ds = QString("%1").arg(parameters.RegionSimilarityThreshold, 0, 'f', 2);
	this->SimilarityEdit->setText(ds);
	ds = QString("%1").arg(parameters.RegionTextureThreshold * 100.0f, 0, 'f', 1);
	this->PercentageLineEdit->setText(ds);
	ds = QString("%1").arg(parameters.RegionVarianceRatioThreshold, 0, 'f', 2);
	this->VarianceRatioEdit->setText(ds);

	ds = QString("%1").arg(parameters.getKforWatershed(), 0, 'f', 2);
	this->kWatershed->setText(ds);

	this->SigmaSmooth->blockSignals(false);
	this->DerivativeKernel->blockSignals(false);	
	this->NoiseModel->blockSignals(false);	
	this->FoerstnerKernel->blockSignals(false);	
	this->IntegrationScalePoints->blockSignals(false);	
	this->IntegrationScaleEdges->blockSignals(false);	
	this->SignificanceLevel->blockSignals(false);	
	this->CannyRadio->blockSignals(false);	
	this->FoerstnerRadioButton->blockSignals(false);
	this->extractPoints->blockSignals(false);
	this->extractEdges->blockSignals(false);
	this->extractRegions->blockSignals(false);
	this->WatershedModel->blockSignals(false);	
	this->morphologicSize->blockSignals(false);
	this->WatershedSmoothing->blockSignals(false);
	this->SimilarityEdit->blockSignals(false);
	this->PercentageLineEdit->blockSignals(false);
	this->VarianceRatioEdit->blockSignals(false);
	this->kWatershed->blockSignals(false);
};
