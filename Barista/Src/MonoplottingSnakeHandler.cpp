#include "MonoplottingSnakeHandler.h"
#include "Bui_MainWindow.h"
#include "XYPolyLine.h"
#include <QColor>
#include "Bui_LabelChangeDlg.h"
#include <QMessageBox>
#include "BaristaProjectHelper.h"
#include "ImageView.h"
#include "Bui_SnakeDlg.h"
#include "Bui_CropROIDlg.h"

#include "ProtocolHandler.h"


CMonoplottingSnakeHandler::CMonoplottingSnakeHandler(void)
{
	this->statusBarText = "MonoplottingSnakeLineMODE ";
}

CMonoplottingSnakeHandler::~CMonoplottingSnakeHandler(void)
{
}

bool CMonoplottingSnakeHandler::keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
{
	CBaristaViewEventHandler::keyReleaseEvent(e, v);

	if (v->getVBase())
	{
		//key events
		if ( e->key() == Qt::Key_F  || e->key() == Qt::Key_C)
		{
			if (v->getVBase() != this->currentVBase || this->line.getPointCount() < 2)
				return false;

			//get snake points and calculate ROI
			C2DPoint snakeRoiUL(v->getHugeImage()->getHeight(), v->getHugeImage()->getWidth());
			C2DPoint snakeRoiLR(0.0f, 0.0f);

			int numberOfPoints = this->line.getPointCount();
			CXYPointArray *points = new CXYPointArray();

			for (int i=0 ; i < numberOfPoints; i++) 
			{
				CXYPoint* p = this->current2DPoints.getAt(i);
				points->add(p);
				
				if (p->y > snakeRoiLR.y) snakeRoiLR.y = p->y;
				if (p->x > snakeRoiLR.x) snakeRoiLR.x = p->x;
				if (p->y < snakeRoiUL.y) snakeRoiUL.y = p->y;
				if (p->x < snakeRoiUL.x) snakeRoiUL.x = p->x;
			}
			
			//define ROI
			v->setROILR(snakeRoiLR); 
			v->setROIUL(snakeRoiUL);

			//if not too big, fill ROI with image data
			if( snakeRoiLR.x - snakeRoiUL.x > 4000 || snakeRoiLR.y - snakeRoiUL.y > 4000)
			{
				v->setHasLargeROI(true);
				/*QMessageBox::critical( 0, "Barista"," ROI is too large (> 2000 pixels) for Snake Calculation!",
					" Please select smaller area for these operations");
				return false;*/
			}
			else
			{
				v->setHasLargeROI(false);
			}

				if (v->getHugeImage())
					v->getHugeImage()->setRoi(snakeRoiUL.x,snakeRoiUL.y, snakeRoiLR.x-snakeRoiUL.x, snakeRoiLR.y-snakeRoiUL.y);
					//v->setHasLargeROI(false);

					v->setShowROI(false);
					v->setHasROI(true);
			//}
		
			//close/ open line
			bool openSnake = true;

			if ( e->key() == Qt::Key_C ) //close line
			{
				if (this->line.getPointCount())
				{
					this->closeDrawings(this->currentLineLabel);
				}
				
				this->line.closeLine();
				openSnake = false;
			}

			// has the user has changed the label while drawing the line ?
			if (this->currentLineLabel == this->changedLineLabel)
			{
			}
			else // yes
			{
				// rename the 3D polyline
				this->line.setLabel(this->changedLineLabel);
				
				// rename the dataset because we don't draw the line again
				v->renameDataSet(this->currentLineLabel,this->changedLineLabel); 
			
				// rename the points of the 2D/3D polyline
				
				// build the point label
				CCharString lineString (this->changedLineLabel.GetChar());
				CCharString pointString(lineString + "_P");

				// add to 2D monoplotted points of the image
				for (int i=0 ; i < this->current2DPoints.GetSize(); i++)
				{
					CXYPoint* p = this->current2DPoints.getAt(i);
					this->currentPointLabel = pointString.GetChar();
					this->currentPointLabel += i;
					p->setLabel(this->currentPointLabel); // 2d
					this->line.getPoint(i)->setLabel(this->currentPointLabel); //3d
				}

				// ok the label is changed
				this->currentLineLabel = this->changedLineLabel;

			}

			CLabel LineLabel1 = this->currentLineLabel;

			//monoplotting is finished
			this->initPolyline();
			v->resetFirstSecondClick();			

			//calculation of snake
			bool* SnakeCalculationOK = new bool;
			*SnakeCalculationOK = false;

			CFileName fn("snake.ini");
			fn.SetPath(project.getFileName()->GetPath());

			Bui_SnakeDlg dlg(v, fn, v->getVImage(), openSnake, points, SnakeCalculationOK); 
			dlg.show();
			dlg.exec();	

			//clean up
			v->unselectROI();
			this->removeDrawingDataSet(LineLabel1);
			this->current2DPoints.RemoveAll();
			this->line.deleteLine();

			if (*SnakeCalculationOK)
			{
				//prepare to draw snake point as monoplotted line
				this->currentVBase = v->getVBase();
				this->currentLineLabel.Set(LineLabel1);				//change back to old label
				this->addDrawingDataSets(this->currentLineLabel);	//add the datasets in all open image views
				
				this->line.setLabel(this->currentLineLabel);		//rename the 3D polyline

				// don't show the selected points labels in the current image view
				v->setShowPoints(this->currentLineLabel,true);
				v->setShowPointLabels(this->currentLineLabel,false);

				// build the point label
				CCharString lineString (this->currentLineLabel.GetChar());
				CCharString pointString(lineString + "_P");
				this->currentPointLabel = pointString.GetChar();

				//prepare for loop to draws the points
				int size = points->GetSize();
				CXYPoint p;
				CXYZPoint linepoint;
					
				// add to 2D monoplotted points of the image
				for(int i = 0; i < size - 1;  i++) 
				{
					this->currentPointLabel += i;							//increase the point label
					p = points->getAt(i);									//get a point (which has no label)
					p.setLabel(this->currentPointLabel);					//change label of the point
		
					this->current2DPoints.Add(p);							//add 2d point
					this->monoPlotter.getPoint(linepoint, p);				//calculate 3d point
					this->line.addPoint(linepoint);							//add 3d point
					
					this->addToDrawings(this->currentLineLabel, linepoint);	//add to drawing datasets
				}

				//close line
				if(openSnake)this->line.openLine();
				else //				
				{
					if (this->line.getPointCount()) this->closeDrawings(this->currentLineLabel);

					this->line.closeLine();
				}
				
				// add to project
				if (project.addMonoplottedLine(this->line))
					baristaProjectHelper.refreshTree();

				// clean up
				this->initPolyline();
				v->resetFirstSecondClick();
			}
			else
			{
				QMessageBox::critical( 0, "Barista"," Error during the calculation");
				return false;
			}
		}
		else if ( e->key() == Qt::Key_Escape)
		{
			if (v->getVBase() != this->currentVBase)
				return false;

			this->removeDrawingDataSet(this->currentLineLabel);
			v->resetFirstSecondClick();
			this->initPolyline();
		}
		else if (e->key() == Qt::Key_Backspace)
		{
			if (v->getVBase() != this->currentVBase)
				return false;

			if (this->current2DPoints.GetSize() && this->line.getPointCount())
			{
				// reset the first click 
				if (this->current2DPoints.GetSize() > 1)
				{
					CObsPoint* p =this->current2DPoints.GetAt(this->current2DPoints.GetSize()-2);
					C2DPoint lastClickedPoint((*p)[0],(*p)[1]);			
					v->setFirstClick(lastClickedPoint);
				}
				else
					v->setFirstClick(C2DPoint(-1,-1));

				this->current2DPoints.RemoveAt(this->current2DPoints.GetSize()-1,1);
				this->removePointFromDataSet(this->currentLineLabel,this->line.getPoint(this->line.getPointCount()-1)->getLabel());
				this->line.removePoint(this->line.getPointCount()-1);
			}
		}

		else if ( e->key() == Qt::Key_L )
		{
			//change current line label
			if (this->currentLineLabel.GetLength() == 0 )
			{
				this->currentLineLabel = project.getCurrentLineLabel();
			}

			bui_LabelChangeDlg labeldlg(this->currentLineLabel.GetChar(), "New Line Label");

			labeldlg.setModal(true);
			labeldlg.exec();

			CLabel newlabel = labeldlg.getLabelText().GetChar();

			if ( newlabel.GetLength() == 0 )
			{
				labeldlg.setLabelText(this->currentLineLabel.GetChar());
				labeldlg.exec();
			}
			else
			{
				this->changedLineLabel = newlabel;
				project.setCurrentLineLabel(newlabel );
			}

			labeldlg.close();
		}
	}
	return true;
};






