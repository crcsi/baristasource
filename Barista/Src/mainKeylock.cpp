#include <QtGui/QApplication>
#include "keylock.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	KeyLock w;
	w.show();
	a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
	return a.exec();
}
