#include "Bui_ReferenceSysDlg.h"
#include "Spheroid.h"
#include "IniFile.h"
#include <math.h>
#include "ReferenceSystem.h"
#include "Filename.h"
#include <QFileDialog>
#include "BaristaHtmlHelp.h"
#include "Icons.h"


Bui_ReferenceSysDlg::Bui_ReferenceSysDlg(CReferenceSystem* refSystem,CFileName* filename, const char *title, 
										 const char* helpfile, bool getFileName, const char* filter,
										 bool setRefValues, bool fileHasToExist): QDialog(NULL),
		helpFile(helpfile), getFileName(getFileName),setRefValues(setRefValues), zonetyped(-1),
		FileHasToExist(fileHasToExist)
{
	setupUi(this);
	this->setWindowTitle(QString(title));

	// do we have to ask for the filename?
	if (getFileName)
	{
		this->pB_File->setEnabled(true);
	}
	else
	{
		this->pB_File->setEnabled(false);
		this->le_filename->setText(QString(filename->GetChar()));
	}
	
	// save parameter
	this->filename = filename;
	this->refSys = refSystem;
	this->filter = filter;

	this->initDlg();

	this->le_CentralMeridianInput->setValidator(new QDoubleValidator(this));
	this->le_LatitudeOriginInput->setValidator(new QDoubleValidator(this));
	this->le_Scale->setValidator(new QDoubleValidator(this));
	this->le_easting->setValidator(new QDoubleValidator(this));
	this->le_northing->setValidator(new QDoubleValidator(this));


	this->connect(this->rB_Geocentric, SIGNAL(released()),this,SLOT(rbGeocentricReleased()));
	this->connect(this->rB_Geographic, SIGNAL(released()),this,SLOT(rbGeographicReleased()));
	this->connect(this->rB_Grid,       SIGNAL(released()),this,SLOT(rbGridReleased()));
	this->connect(this->rB_UTM,        SIGNAL(released()),this,SLOT(rbUTMReleased()));
	this->connect(this->rB_TM,         SIGNAL(released()),this,SLOT(rbTMReleased()));
	this->connect(this->rB_WGS84,      SIGNAL(released()),this,SLOT(rbWGS84Released()));
	this->connect(this->rB_OtherRefSys,SIGNAL(released()),this,SLOT(rbOtherRefSysReleased()));
	this->connect(this->pB_Apply,	   SIGNAL(released()),this,SLOT(pBApplySettings()));
	this->connect(this->rB_OtherCS,    SIGNAL(released()),this,SLOT(rbOtherCSReleased()));
	this->connect(this->pB_cancel,     SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pB_File,       SIGNAL(released()),this,SLOT(pBFileSelectReleased()));
	this->connect(this->rB_northernH,  SIGNAL(released()),this,SLOT(checkValues()));
	this->connect(this->rB_southernH,  SIGNAL(released()),this,SLOT(checkValues()));
	this->connect(this->le_CentralMeridianInput,SIGNAL(editingFinished()),this,SLOT(checkValues()));
	this->connect(this->le_LatitudeOriginInput,SIGNAL(editingFinished()),this,SLOT(checkValues()));
	this->connect(this->le_Scale,      SIGNAL(editingFinished()),this,SLOT(checkValues()));
	this->connect(this->le_easting,    SIGNAL(editingFinished()),this,SLOT(checkValues()));
	this->connect(this->le_northing,   SIGNAL(editingFinished()),this,SLOT(checkValues()));

	this->connect(this->le_CentralMeridianInput,SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));
	this->connect(this->le_LatitudeOriginInput,SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));
	this->connect(this->le_Scale,      SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));
	this->connect(this->le_easting,    SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));
	this->connect(this->le_northing,   SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));


	this->connect(this->pB_Help,       SIGNAL(released()),this,SLOT(callHelp()));


	this->connect(this->cB_Zone,       SIGNAL(editTextChanged(const QString &)),this,SLOT(zoneEdited(const QString &)));
	this->connect(this->cB_Zone,       SIGNAL(highlighted ( int )),this,SLOT(zonehighlighted ( int )));
	this->connect(this->cB_Zone,       SIGNAL(activated ( int )),this,SLOT(zonehighlighted ( int )));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
	this->setModal(true);
}


int Bui_ReferenceSysDlg::exec()
{
	this->show();
	if (this->getFileName)
		this->pBFileSelectReleased();
	return QDialog::exec();
}



void Bui_ReferenceSysDlg::initDlg()
{

	this->setGridSettingsEnabled(false);
	this->setUTMSettingsEnabled(false);
	this->setTMSettingsEnabled(false);
	this->setSuccess(false);

	// utm zones
	int i = 0;
    while( ++i <= 60 )
    {
		this->cB_Zone->insertItem( i, QString::fromLatin1( "Zone " ) + QString::number( i ) );      
    }

	// spheroides
	this->cB_Spheroid->insertItem(0,QString::fromLatin1( WGS84Spheroid.getName()));
	this->cB_Spheroid->insertItem(1,QString::fromLatin1( GRS1980Spheroid.getName()));
	this->cB_Spheroid->insertItem(1,QString::fromLatin1( GRS1967Spheroid.getName()));
	this->cB_Spheroid->insertItem(2,QString::fromLatin1( BesselSpheroid.getName()));
	this->cB_Spheroid->insertItem(3,QString::fromLatin1( AustralianNationalSpheroid.getName()));
	this->cB_Spheroid->insertItem(4,QString::fromLatin1( Clarke1866Spheroid.getName()));
	this->cB_Spheroid->insertItem(5,QString::fromLatin1( Clarke1880Spheroid.getName()));
	this->cB_Spheroid->insertItem(6,QString::fromLatin1( HayfordSpheroid.getName()));
	this->cB_Spheroid->insertItem(7,QString::fromLatin1( IERSSpheroid.getName()));

	// get values from inifile to start with
	int lastZone = inifile.getZone();
	double lastLongitude = inifile.getCentralMeridian();
	double lastLatitude = inifile.getLatitudeOrigin();
	int lastHemisphere = inifile.getHemisphere();
	double lastScale = inifile.getTMScale();
	double lastNorthing = inifile.getTMNorthing();
	double lastEasting = inifile.getTMEasting();
	bool lastUTM = inifile.getIsUTM();
	bool lastWGS = inifile.getIsWGS84();
	CCharString lastEl = inifile.getEllipsoidName();

	// get values from existing Refsys
	
	if ( this->setRefValues )
	{
		lastZone = this->refSys->getZone();
		lastLongitude = this->refSys->getCentralMeridian();
		lastLatitude = this->refSys->getLatitudeOrigin();
		lastHemisphere = this->refSys->getHemisphere();
		lastScale = this->refSys->getScale();
		lastNorthing = this->refSys->getNorthingConstant();
		lastEasting = this->refSys->getEastingConstant();
		lastUTM = this->refSys->isUTM();
		lastWGS = this->refSys->isWGS84();
		lastEl = this->refSys->getSpheroid().getName();
	}



	// set default values
	
	this->cB_Spheroid->blockSignals(true);
	this->rB_WGS84->blockSignals(true);
	this->rB_OtherCS->blockSignals(true);
	this->rB_UTM->blockSignals(true);
	this->rB_TM->blockSignals(true);

	int ellItem = -1;
    if (lastEl == WGS84Spheroid.getName()) ellItem = 0;
	else if (lastEl == GRS1980Spheroid.getName()) ellItem = 1;
	else if (lastEl == GRS1967Spheroid.getName()) ellItem = 2;
	else if (lastEl == BesselSpheroid.getName()) ellItem = 3;
	else if (lastEl == AustralianNationalSpheroid.getName()) ellItem = 4;
	else if (lastEl == Clarke1866Spheroid.getName()) ellItem = 5;
	else if (lastEl == Clarke1880Spheroid.getName()) ellItem = 6;
	else if (lastEl == HayfordSpheroid.getName()) ellItem = 7;
	else if (lastEl == IERSSpheroid.getName()) ellItem = 8;
	else ellItem = 0;
	this->cB_Spheroid->setCurrentIndex(ellItem);	
	
	if (lastWGS) 
	{
		this->rB_WGS84->setChecked(true);
		this->rB_OtherRefSys->setChecked(false);
		this->cB_Spheroid->setEnabled(false);	
	}
	else 
	{
		this->rB_WGS84->setChecked(false);
		this->rB_OtherRefSys->setChecked(true);
		this->cB_Spheroid->setEnabled(true);
	}

	if (lastUTM) 
	{
		this->rB_UTM->setChecked(true);
		this->rB_TM->setChecked(false);
		this->gB_UTMSettings->setEnabled(true);
	}
	else
	{
		this->rB_UTM->setChecked(false);
		this->rB_TM->setChecked(true);
		this->gB_TMSettings->setEnabled(true);
	}

	this->cB_Spheroid->blockSignals(false);
	this->rB_WGS84->blockSignals(false);
	this->rB_OtherCS->blockSignals(false);
	this->rB_UTM->blockSignals(false);
	this->rB_TM->blockSignals(false);

	if (lastZone > 0 && lastZone < 61)
	{
		this->cB_Zone->setCurrentIndex(lastZone-1);
	}
	else
		this->cB_Zone->setCurrentIndex(0);

	if (fabs(lastLongitude + 1.0) > 0.000001)
		this->le_CentralMeridianInput->setText(QString( "%1" ).arg(lastLongitude, 0, 'F', 8 ));
	else
		this->le_CentralMeridianInput->setText(QString("0.0"));

	if (fabs(lastLatitude + 1.0) > 0.000001)
		this->le_LatitudeOriginInput->setText(QString( "%1" ).arg(lastLatitude, 0, 'F', 8 ));
	else
		this->le_LatitudeOriginInput->setText(QString("0.0"));

	if (lastHemisphere == NORTHERN)
		this->rB_northernH->setChecked(true);
	else if (lastHemisphere == SOUTHERN)
		this->rB_southernH->setChecked(true);
	
	this->le_Scale->setText(QString( "%1" ).arg(lastScale, 0, 'F', 8 ));
	this->le_easting->setText(QString( "%1" ).arg(lastEasting, 0, 'F', 4 ));
	this->le_northing->setText(QString( "%1" ).arg(lastNorthing, 0, 'F', 4 ));

	if (this->refSys && this->setRefValues)
	{
		if (this->refSys->getCoordinateType() == eGEOGRAPHIC)
			this->rB_Geographic->setChecked(true);
		else if (this->refSys->getCoordinateType() == eGEOCENTRIC)
			this->rB_Geocentric->setChecked(true);
		else if (this->refSys->getCoordinateType() == eGRID)
		{
			if (this->refSys->isUTM())
			{
				this->rB_UTM->setChecked(true);

				int zone = this->refSys->getZone();
				if (zone >=0 && zone < this->cB_Zone->count())
				{
					this->cB_Zone->setCurrentIndex(lastZone-1);
				}
				else 
					this->cB_Zone->setCurrentIndex(0);

				int hemisphere = this->refSys->getHemisphere();
				if (hemisphere == NORTHERN)
					this->rB_northernH->setChecked(true);
				else if (hemisphere == SOUTHERN)
					this->rB_southernH->setChecked(true);
			}
			else
			{
				this->rB_TM->setChecked(true);

				double longitude = this->refSys->getCentralMeridian();
				double latitude = this->refSys->getLatitudeOrigin();
				if (fabs(longitude + 1.0) > 0.000001)
					this->le_CentralMeridianInput->setText(QString().sprintf("%.8lf",longitude));
				else
					this->le_CentralMeridianInput->setText(QString("0.0"));

				if (fabs(latitude + 1.0) > 0.000001)
					this->le_LatitudeOriginInput->setText(QString().sprintf("%.8lf",latitude));
				else
					this->le_LatitudeOriginInput->setText(QString("0.0"));

			}
			this->setGridSettingsEnabled(true);
			this->rB_Grid->setChecked(true);

		}
		else
			this->rB_OtherRefSys->setChecked(true);

		if (this->refSys->isWGS84())
			this->rB_WGS84->setChecked(true);
		else
		{
			CCharString spName = this->refSys->getSpheroid().getName();

			int ellItem = -1;
			if (spName == WGS84Spheroid.getName()) ellItem = 0;
			else if (spName == GRS1980Spheroid.getName()) ellItem = 1;
			else if (spName == GRS1967Spheroid.getName()) ellItem = 2;
			else if (spName == BesselSpheroid.getName()) ellItem = 3;
			else if (spName == AustralianNationalSpheroid.getName()) ellItem = 4;
			else if (spName == Clarke1866Spheroid.getName()) ellItem = 5;
			else if (spName == Clarke1880Spheroid.getName()) ellItem = 6;
			else if (spName == HayfordSpheroid.getName()) ellItem = 7;
			else if (spName == IERSSpheroid.getName()) ellItem = 8;
			else ellItem = 0;
			this->cB_Spheroid->setCurrentIndex(ellItem);	
		}

	}

	this->checkValues();

}

Bui_ReferenceSysDlg::~Bui_ReferenceSysDlg(void)
{
}

void Bui_ReferenceSysDlg::setUTMSettingsEnabled(bool b)
{
	this->gB_UTMSettings->setEnabled(b);
}

void Bui_ReferenceSysDlg::setTMSettingsEnabled(bool b)
{
	this->gB_TMSettings->setEnabled(b);
}

void Bui_ReferenceSysDlg::setGridSettingsEnabled(bool b)
{
	this->gB_GridSettings->setEnabled(b);
	if (b)
	{
		if (this->rB_UTM->isChecked()) 
		{
			this->setUTMSettingsEnabled(true);
			this->setTMSettingsEnabled(false);
		}
		else 
		{
			this->setUTMSettingsEnabled(false);
			
			if (this->rB_TM->isChecked())
				this->setTMSettingsEnabled(true);
			else this->setTMSettingsEnabled(false);
	
		}	
	}
}




void Bui_ReferenceSysDlg::rbGeocentricReleased()
{
	this->setGridSettingsEnabled(false);
	this->checkValues();	
}

void Bui_ReferenceSysDlg::rbGeographicReleased()
{
	this->setGridSettingsEnabled(false);
	this->checkValues();
}

void Bui_ReferenceSysDlg::rbGridReleased()
{
	this->setGridSettingsEnabled(true);
	this->checkValues();
}

void Bui_ReferenceSysDlg::rbOtherCSReleased()
{
	this->setGridSettingsEnabled(false);
	this->checkValues();
}


void Bui_ReferenceSysDlg::rbUTMReleased()
{
	this->setTMSettingsEnabled(false);
	this->setUTMSettingsEnabled(true);
	this->checkValues();
}

void Bui_ReferenceSysDlg::rbTMReleased()
{
	this->setUTMSettingsEnabled(false);
	this->setTMSettingsEnabled(true);
	this->checkValues();
}

void Bui_ReferenceSysDlg::rbWGS84Released()
{
	this->cB_Spheroid->setEnabled(false);
	this->cB_Spheroid->setCurrentIndex(0);
	this->checkValues();
}

void Bui_ReferenceSysDlg::rbOtherRefSysReleased()
{
	this->cB_Spheroid->setEnabled(true);
	this->checkValues();
}

void Bui_ReferenceSysDlg::pBCancelReleased()
{
	this->setSuccess(false);
	this->close();
}

void Bui_ReferenceSysDlg::pBFileSelectReleased()
{
	QString filename;
	
	bool OK = true;

	if (FileHasToExist)
	{
		filename = QFileDialog::getOpenFileName(
		this,
		"Choose file",
		inifile.getCurrDir(),
		filter);
 
		QFile test(filename);
    
		if (!test.exists()) OK = false;
		else test.close();
	}
	else 
	{
		filename = QFileDialog::getSaveFileName(this, "Choose file", inifile.getCurrDir(), filter);
	}
   
    // make sure the file exists
   
	if (OK)
	{
		*(this->filename) = (const char*)filename.toLatin1();
		this->le_filename->setText(filename);
	}

	this->checkValues();
}

void Bui_ReferenceSysDlg::pBApplySettings()
{
	// set Coordinate Type
	if (this->rB_Geographic->isChecked())
		this->refSys->setCoordinateType(eGEOGRAPHIC);
	else if (this->rB_Geocentric->isChecked())
		this->refSys->setCoordinateType(eGEOCENTRIC);
	else if  (this->rB_Grid->isChecked())
		this->refSys->setCoordinateType(eGRID);
	else  this->refSys->setCoordinateType(eUndefinedCoordinate);  // just in case


	// set Spheroid
	if (this->rB_WGS84->isChecked())
	{
		this->refSys->setToWGS1984();
	}
	else
	{
		QString spheroid =	this->cB_Spheroid->currentText();

		if (spheroid.compare(QString(WGS84Spheroid.getName())) == 0)
			this->refSys->setSpheroid(WGS84Spheroid);

		if (spheroid.compare(QString(GRS1980Spheroid.getName())) == 0)
			this->refSys->setSpheroid(GRS1980Spheroid);

		if (spheroid.compare(QString(GRS1967Spheroid.getName())) == 0)
			this->refSys->setSpheroid(GRS1967Spheroid);

		else if (spheroid.compare(QString(AustralianNationalSpheroid.getName()))== 0)
			this->refSys->setSpheroid(AustralianNationalSpheroid);
		
		else if (spheroid.compare(QString(BesselSpheroid.getName()))== 0)
			this->refSys->setSpheroid(BesselSpheroid);	
		
		else if (spheroid.compare(QString(Clarke1866Spheroid.getName()))== 0)
			this->refSys->setSpheroid(Clarke1866Spheroid);
		
		else if (spheroid.compare(QString(Clarke1880Spheroid.getName()))== 0)
			this->refSys->setSpheroid(Clarke1880Spheroid);
		
		else if (spheroid.compare(QString(HayfordSpheroid.getName()))== 0)
			this->refSys->setSpheroid(HayfordSpheroid);
		
		else if (spheroid.compare(QString(IERSSpheroid.getName()))== 0)
			this->refSys->setSpheroid(IERSSpheroid);
	}

	// set Grid
	if (this->rB_Grid->isChecked())
	{
		if (this->rB_UTM->isChecked())
		{
			int zone = this->cB_Zone->currentIndex()+1;
			inifile.setZone(zone);

			int hemi;
			if (this->rB_northernH->isChecked()) hemi = NORTHERN;
			else hemi = SOUTHERN;
			inifile.setHemisphere(hemi);

			this->refSys->setUTMZone(zone, hemi);
		}
		else if (this->rB_TM->isChecked())
		{
			double longitude = this->le_CentralMeridianInput->text().toDouble();
			double latitude =  this->le_LatitudeOriginInput->text().toDouble();

			double FEasting  = this->le_easting->text().toDouble(); 
			double FNorthing = this->le_northing->text().toDouble(); 
			double scale     = this->le_Scale->text().toDouble();

			this->refSys->setTMParameters(longitude, FEasting, FNorthing, scale, latitude);

			inifile.setCentralMeridian(longitude);
			inifile.setLatitudeOrigin(latitude);
			inifile.setTMScale(scale);
			inifile.setTMEasting(FEasting);
			inifile.setTMNorthing(FNorthing);
		}
	}

	inifile.setEllipsoidName(this->refSys->getSpheroid().getName());
	inifile.setIsUTM(this->refSys->isUTM());
	inifile.setIsWGS84(this->refSys->isWGS84());
	inifile.setCoordinateType(this->refSys->getCoordinateType());
	inifile.writeIniFile();

	this->setSuccess(true);
	this->close();
}

void Bui_ReferenceSysDlg::checkValues()
{
	if (this->le_filename->text().compare("")== 0)
	{
		this->pB_Apply->setEnabled(false);
		return;
	}
	
	if (!this->rB_Geocentric->isChecked() &&
		!this->rB_Geographic->isChecked() &&
		!this->rB_Grid->isChecked()       &&
		!this->rB_OtherCS->isChecked())
	{
		this->pB_Apply->setEnabled(false);
		return;
	}
	
	if (this->rB_Grid->isChecked() &&
		!this->rB_UTM->isChecked() &&
		!this->rB_TM->isChecked())
	{
		this->pB_Apply->setEnabled(false);	
		return;
	}

	if (this->rB_UTM->isChecked() && 
		this->rB_Grid->isChecked() &&
		!this->rB_northernH->isChecked() &&
		!this->rB_southernH->isChecked())
	{
		this->pB_Apply->setEnabled(false);	
		return;
	}

	if (!this->rB_OtherCS->isChecked() &&
		!this->rB_WGS84->isChecked() && 
		!this->rB_OtherRefSys->isChecked())
	{
		this->pB_Apply->setEnabled(false);	
		return;
	}

	if (this->rB_TM->isChecked() && this->rB_Grid->isChecked())
	{
		if (!this->le_CentralMeridianInput->hasAcceptableInput() ||
			!this->le_LatitudeOriginInput->hasAcceptableInput() ||
			!this->le_Scale->hasAcceptableInput() ||
			!this->le_easting->hasAcceptableInput() ||
			!this->le_northing->hasAcceptableInput() )
		{
			this->pB_Apply->setEnabled(false);
			return;
		}
		else
		{
			this->pB_Apply->setEnabled(true);
		}
	}

	this->pB_Apply->setEnabled(true);
}

void Bui_ReferenceSysDlg::checkValues(const QString& text)
{
	this->checkValues();
}


void Bui_ReferenceSysDlg::callHelp()
{
	CHtmlHelp::callHelp(helpFile);
}

void Bui_ReferenceSysDlg::zoneEdited(const QString & text)
{
	int wason = this->cB_Zone->currentIndex();
	QString itext = "Zone " + QString::number( wason);
	int length = itext.length();

	QString typed(text);
		QString qzone = typed.remove(0, length);

	bool isint = true;
	int ityped = typed.toInt(&isint);

	if ( !isint )
	{
		this->cB_Zone->setCurrentIndex(0);
		this->zonetyped = -1;
		return;
	}

	if ( this->zonetyped == -1 )
	{
		this->cB_Zone->blockSignals ( true );
		this->cB_Zone->setCurrentIndex(ityped -1);
		this->cB_Zone->blockSignals ( false );
		this->zonetyped = ityped;
	}
	else
	{
		int newzone = this->zonetyped*10 + ityped -1;
		if ( newzone > 1 && newzone < 61 )
		{
			this->cB_Zone->blockSignals ( true );
			this->cB_Zone->setCurrentIndex(newzone);
			this->cB_Zone->blockSignals ( false );
		}
		else
		{
			this->cB_Zone->blockSignals ( true );
			this->cB_Zone->setCurrentIndex(this->zonetyped -1);
			this->cB_Zone->blockSignals ( false );
		}
		this->zonetyped = -1;
	}
}

void Bui_ReferenceSysDlg::zonehighlighted(int index)
{
	this->zonetyped = -1;
	this->cB_Zone->blockSignals ( true );
	this->cB_Zone->setCurrentIndex(index);
	this->cB_Zone->blockSignals ( false );

}
