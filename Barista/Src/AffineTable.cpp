#include "AffineTable.h"
#include <QHeaderView>
#include <QCloseEvent>
#include "Affine.h"


#include "Label.h"
#include "math.h"


CAffineTable::CAffineTable(CAffine* affine,QWidget* parent) : CBaristaTable(parent),affine(affine)
{

	this->initTable();
    affine->addListener(this);
}

CAffineTable::~CAffineTable(void)
{
}

void CAffineTable::closeEvent ( QCloseEvent * e )
{
    this->affine->removeListener(this);
    e->accept();
}


void CAffineTable::initTable()
{
	CCharString str("Affine Parameters:   ");
	str = str + this->affine->getFileName()->GetFullFileName();
	this->setWindowTitle(str.GetChar());

	this->setAccessibleName(this->affine->getFileName()->GetChar());

	this->setColumnCount(3);

	// parameters plus coordinate system
	this->setRowCount(this->affine->getNumberOfStationPars()+1);
	this->setColumnWidth(0, 110);
	this->setColumnWidth(1, 190);
	this->setColumnWidth(3, 40);

	for (int i=0; i < this->affine->getNumberOfStationPars()+1; i++)
		this->setRowHeight(i,20);

	QStringList headerList;
	headerList.append(QString("Parameters"));
	headerList.append(QString("Values"));
	headerList.append(QString("Sigma"));
	this->setHorizontalHeaderLabels(headerList);

	QString sys = this->affine->getRefSys().getCoordinateSystemName().GetChar();

	QTableWidgetItem* item;
	
	////// Affine node reference system info
	item = this->createItem("Coordinate System",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,0,item);
	item = this->createItem(sys,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,1,item);
	item = this->createItem("NA",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,2,item);

	// Affine parameters
    double value;
    double sigma;

    Matrix parms;
    Matrix covmat;

    bool load = false;

    this->getAffine()->getAllParameters(parms);
    covmat = *(this->getAffine()->getCovariance());
    
    for (int i = 0; i < affine->getNumberOfStationPars(); ++i)
    {
        CLabel clabel = "A";         
        clabel += i+1;
        QString str = clabel.GetChar();

		item = this->createItem(str, 0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i+1,0,item);

        value = parms.element(i, 0);
		QString valuestr = QString( "%1" ).arg(value, 0, 'F', 6 );

		item = this->createItem(valuestr, 0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i+1,1,item);

		QString sigmastr("not known");

        if (covmat.Nrows() != 0)
        {
            sigma = sqrt(covmat.element(i, i));
			if ( sigma > 0.0)
				sigmastr = QString( "%1" ).arg(sigma, 0, 'g', 6 );
        }

		item = this->createItem(sigmastr, 0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i+1,2,item);
    }

}

void CAffineTable::setAffine(CAffine* affine)
{
    this->affine = affine;
    affine->addListener(this);
}
