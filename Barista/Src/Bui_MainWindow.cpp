#include "Bui_MainWindow.h"
#include <QFileDialog>
#include <QMessageBox>

#include "BaristaIconViewItem.h"
#include "HugeImage.h"
#include "BestFitLine.h"
#include "LeastSquaresSolver.h"

#include "BaristaHtmlHelp.h"
#include "SystemUtilities.h"
#include "Bui_DEMMatchingDlg.h"
#include "BaristaProjectHelper.h"


#include "ImageTable.h"
#include "XYZPolyLineTable.h"
#include "BuildingTable.h"
#include "XYEllipseTable.h"
#include "RPCTable.h"
#include "AffineTable.h"
#include "TFWTable.h"

#include "Bui_ReferenceSysDlg.h"
#include "Bui_Pansharpen.h"
#include "Bui_ProgressDlg.h"
#include "Bui_orthodialog.h"
#include "Bui_ImageRegistrationDialog.h"
#include "Bui_GDALImportDlg.h"
#include "Bui_NewProject.h"

#include "Bui_OrbitParameterDlg.h"
#include "Bui_BandMergeDlg.h"
#include "Bui_ProjectSettingsDlg.h"
#include "Bui_DEMMergeDlg.h"
#include "Bui_DEMDifferenceDlg.h"
#include "Bui_FrameCameraModelDlg.h"
#include "Bui_FrameCameraDlg.h"
#include "Bui_CameraMountingDlg.h"
#include "Bui_RAWImportDlg.h"
#include "Bui_DialogContainer.h"
#include "Bui_PushbroomDlg.h"
#include "Bui_CameraDlg.h"
#include "Bui_AdjustDialog.h"
#include "Bui_MatchingParametersDlg.h"
#include "Bui_BuildingsDetector.h"

//#include "Bui_ChannelSwitchBox.h"
#include "Bui_ImgShowDlg.h"
//#include "Bui_ImageChipsDlg.h"
#include "ImageChipsMatchingDlg.h"

#include "ObjectSelector.h"

#include "OrthoImageGenerator.h"
#include "MonoPlotter.h"
#include "tetrixwindow.h"
#include <QShortcut>
#include "ImageView.h"

#include "Icons.h"
#include "BundleHelper.h"
#include "ProgressThread.h"

#include "Bui_ImportALS.h"
#include "Bui_ImportGIS.h"
#include "ALSTable.h"
#include "PushBroomScannerTable.h"

#include "DEMView.h"

#include "PointDrawing.h"
//#include "HardwareDongle.h"
#include "ObjectSelectorTable.h"
#include "ObsPointDataModel.h"
#include "PolyLineDataModel.h"
#include "3DView.h"

#include "Bui_ImageMergeDlg.h"
#include "Bui_GCPMatcher.h"
#include "Bui_AVNIRBandMergeDlg.h"
//#include "GCPDataBasePoint.h"
#include "Trans3DFactory.h"
#include "HeightOptions.h"

QTreeWidgetItem* clickedItem = NULL;



bui_MainWindow::bui_MainWindow(QWidget *parent,const char* name, Qt::WindowFlags flags)
    : QMainWindow(parent, flags),saved(false),xPressed(true),
	dongleTimer(NULL),DigitizeAction(NULL),MeasureAction(NULL),digitizeMenu(NULL),
	LSMAction(NULL),SCPcollectionAction(NULL),monoplotLinesMenu(0), isCollectGCPActionActive(false)
{

	setupUi(this);
	// change the font size
	QFont currentFont = QApplication::font();
	currentFont.setPointSize(11);
	
	this->Window->setFont(currentFont);
	this->menuImage_Show->setFont(currentFont);
	this->menuFeature_Extraction->setFont(currentFont);
	this->menuHelp->setFont(currentFont);
	this->menuSettings->setFont(currentFont);
	this->Monoplotting->setFont(currentFont);
	this->menuMonoplot_Lines->setFont(currentFont);
	this->menuDEM_Processing->setFont(currentFont);
	this->fileMenu->setFont(currentFont);
	this->menuImport_DEM->setFont(currentFont);
	this->menuImport_Image_s->setFont(currentFont);
	this->Adjustments->setFont(currentFont);
	this->menuExport_RPCs->setFont(currentFont);
	this->menuImage_Processing->setFont(currentFont);
	this->menuPansharpening->setFont(currentFont);
	this->menuALOS_tools->setFont(currentFont);
	
	this->qt_dead_widget_MenuBar->setFont(currentFont);
	

	this->DigitizeAction = new QAction(this);
	this->DigitizeAction->setCheckable(true);
	this->DigitizeAction->setText(QString::fromUtf8("Digitize"));

    this->MeasureAction = new QAction(this);
    this->MeasureAction->setText(QString::fromUtf8("Measure"));
    this->MeasureAction->setCheckable(true);

    this->LSMAction = new QAction(this);
    this->LSMAction->setText(QString::fromUtf8("Matching"));
    this->LSMAction->setCheckable(true);

	// for SCP chips collection //
	this->SCPcollectionAction = new QAction(this);
    this->SCPcollectionAction->setText(QString::fromUtf8("Chip collection"));
    this->SCPcollectionAction->setCheckable(true);
	//to inactivate Collect chip action at Monoplotting menu we set this->CollectChipsAction->setVisible(false);
	//in order to activate delete the following line
	//to permanently unuse this option remove all info related to CollectChipsAction from all files and also from Monoplotting menu at MainWindow.ui
	//this->CollectChipsAction->setVisible(false);

	this->MonoplottingSnakeLinesAction = new QAction(this);
	this->MonoplottingSnakeLinesAction->setText(QString::fromUtf8("Monoplot Snake Lines"));
	this->MonoplottingSnakeLinesAction->setCheckable(true);

	this->MonoplottingLinesAction = new QAction(this);
	this->MonoplottingLinesAction->setText(QString::fromUtf8("Monoplot Lines"));
	this->MonoplottingLinesAction->setCheckable(true);

	//file
	connect(this->fileNewAction,							SIGNAL( triggered()),this,SLOT(fileNew()));						//BaristaEventHandler switches
	connect(this->fileOpenAction,							SIGNAL( triggered()),this,SLOT(fileOpen()));					//BaristaEventHandler switches
	connect(this->fileSaveAction,							SIGNAL( triggered()),this,SLOT(fileSave()));					//BaristaEventHandler switches
	connect(this->fileSaveAsAction,							SIGNAL( triggered()),this,SLOT(fileSaveAs()));
	connect(this->actionClose,								SIGNAL( triggered()),this,SLOT(fileClose()));

	connect(this->actionFormattedImageImport,				SIGNAL( triggered()),this,SLOT(importImages()));
	connect(this->actionUnformattedImageImport,				SIGNAL( triggered()),this,SLOT(importImage()));

	connect(this->fileImport_3D_PointsAction,				SIGNAL( triggered()),this,SLOT(import3DPoints()));

	connect(this->actionFormattedDEMImport,					SIGNAL( triggered()),this,SLOT(importDEMs()));
	connect(this->actionUnformattedDEMImport,				SIGNAL( triggered()),this,SLOT(importDEM()));

	connect(this->actionImport_ALS_data,					SIGNAL( triggered()),this,SLOT(importALSData()));
	
	connect(this->actionImport_GIS_data,					SIGNAL( triggered()),this,SLOT(importGISData()));

	connect(this->fileExitAction,							SIGNAL( triggered()),this,SLOT(fileExit()));
	
	
	//adjustment
	connect(this->actionAdjustment,							SIGNAL( triggered()),this,SLOT(AdjustmentBundleClicked()));
	connect(this->functionsRPCRegenerateBiasCorrectedRPCs,	SIGNAL( triggered()),this,SLOT(RPCRegenerateBiasCorrected()));

	connect(this->functionsExportupdatedRPCsIkonos,			SIGNAL( triggered()),this,SLOT(exportAllupdatedIkonosRPCs()));
	connect(this->functionsExportupdatedRPCsQuickBird,		SIGNAL( triggered()),this,SLOT(exportAllupdatedQuickBirdRPCs()));


	//image processing
	connect(this->functionsOrthoimageAction,				SIGNAL( triggered()),this,SLOT(OrthoImageGeneration()));
	connect(this->actionOrthorectified_Images,				SIGNAL( triggered()),this,SLOT(PanSharpenOrtho()));
	connect(this->actionOriginal_Images,					SIGNAL( triggered()),this,SLOT(PanSharpenOriginal()));

	connect(this->actionImage_Registration,					SIGNAL( triggered()),this,SLOT(ImageRegistration()));
	connect(this->actionRegisterImagesByOrthophotos,		SIGNAL( triggered()),this,SLOT(RegisterImagesByOrthophotos()));
	connect(this->actionRegister_Images_Using_Image_Chips,	SIGNAL(triggered()) ,this,SLOT(loadGCPdatabase()));

	connect(this->MergeImageBandsAction,					SIGNAL( triggered()),this,SLOT(MergeImageBands()));
	connect(this->actionMergeALOSImages,					SIGNAL( triggered()),this,SLOT(mergeALOSImages()));
	connect(this->actionMergeALOSBands,						SIGNAL( triggered()),this,SLOT(mergeALOSBands()));
	
	connect(this->actionGenerate_DEM_by_Image_Matching,		SIGNAL( triggered()),this,SLOT(MatchDEM()));
	connect(this->actionCompute_staggered_ADS40_Image,		SIGNAL( triggered()),this,SLOT(computeADS40StaggeredImage()));
	
	// DEM processing
	connect(this->MergeDEMAction,							SIGNAL( triggered()),this,SLOT(MergeDEM()));
	connect(this->DifferenceDEMAction,						SIGNAL( triggered()),this,SLOT(DifferenceDEM()));

	
	//monoplotting
	connect(this->MonoplotPointsAction,						SIGNAL(toggled(bool)),this,SLOT(MonoplotPoints(bool)));		//BaristaEventHandler switches
	
	connect(this->MonoplotLinesAction,						SIGNAL(triggered()),this,SLOT(MonoplottingLines()));			
	connect(this->MonoplottingLinesAction,					SIGNAL(toggled(bool)),this,SLOT(MonoplottingLines(bool)));		//BaristaEventHandler switches

	connect(this->MonoplotSnakeLinesAction,					SIGNAL(triggered()),this,SLOT(MonoplottingSnakeLines()));
	connect(this->MonoplottingSnakeLinesAction,				SIGNAL(toggled(bool)),this,SLOT(MonoplottingSnakeLines(bool)));	//BaristaEventHandler switches			

	connect(this->MeasureHeightsAction,						SIGNAL(toggled(bool)),this,SLOT(MonoplotHeights(bool)));	//BaristaEventHandler switches
	
	connect(this->MonoplotBuildingsAction,					SIGNAL(toggled(bool)),this,SLOT(MonoplotBuildings(bool)));	//BaristaEventHandler switches

	//feature extraction
	connect(this->RegionGrowingAction,						SIGNAL(toggled(bool)),this,SLOT(regionGrowing(bool)));		//BaristaEventHandler switches
	connect(this->actionBuilding_Detection,	    			SIGNAL(triggered()),this,SLOT(BuildingsDetection()));
	//connect(this->actionBuilding_Detection,	    			SIGNAL(triggered()),this,SLOT(testline()));

	//window
	//connect(this->actionChannel_Content ,SIGNAL( triggered()),this,SLOT(contentShow()));
	connect(this->actionPoint_Residual,						SIGNAL( triggered()),this,SLOT(imageShow()));
	connect(this->windowClose_all_WindowsAction,			SIGNAL( triggered()),this,SLOT(closeAllWindows()));

	//settings
	connect(this->ProjectSettingsAction,					SIGNAL( triggered()),this,SLOT(setProjectSettings()));
	connect(this->MatchingParametersAction,					SIGNAL( triggered()),this,SLOT(setMatchingParameters()));

	//help
	connect(this->actionGet_Started,						SIGNAL( triggered()),this,SLOT(acHelpGetStarted()));
	connect(this->actionContents,							SIGNAL( triggered()),this,SLOT(acHelpContens()));
	connect(this->actionSearch,								SIGNAL( triggered()),this,SLOT(acHelpSearch()));

	connect(this->actionAbout,								SIGNAL( triggered()),this,SLOT(acHelpAbout()));

	//???
	connect(this->AffineBundleAction,						SIGNAL( triggered()),this,SLOT(AffineBundleClicked()));
	
	//further BaristaEventHandler switches
	connect(this->PanAction,								SIGNAL(toggled(bool)),this,SLOT(setPanMove(bool)));
	connect(this->ZoomOutAction,							SIGNAL(toggled(bool)),this,SLOT(setZoomOut(bool)));
	connect(this->ZoomInAction,								SIGNAL(toggled(bool)),this,SLOT(setZoomIn(bool)));

	connect(this->DigitizeAction,							SIGNAL(toggled(bool)),this,SLOT(digitizeButtonToggle(bool)));
	connect(this->MeasureAction,							SIGNAL(toggled(bool)),this,SLOT(measureButtonToggle(bool)));
	connect(this->LSMAction,								SIGNAL(toggled(bool)),this,SLOT(lsmButtonToggle(bool)));
	
	connect(this->geoLinkAction,							SIGNAL(toggled(bool)),this,SLOT(geoLinkModeToggle(bool)));
	connect(this->Barista3DViewAction,						SIGNAL(toggled(bool)),this,SLOT(barista3DViewToggle(bool)));

	// GCP chip database
	connect(this->CreateSCPDatabaseAction,					SIGNAL(triggered()),this,SLOT(chipDbaseNew()));
	connect(this->SCPcollectionAction,						SIGNAL(toggled(bool)),this,SLOT(scpCollectionButtonToggle(bool)));
	//connect(this->CollectChipsAction,						SIGNAL(triggered()),this,SLOT(scpCollectionButtonToggle()));	
	connect(this->SaveSCPDatabaseAction,					SIGNAL(triggered()),this,SLOT(chipDbFileSave()));
	connect(this->LoadSCPDatabaseAction,					SIGNAL(triggered()),this,SLOT(chipDbFileOpen()));
	connect(this->CloseSCPDatabaseAction,					SIGNAL(triggered()),this,SLOT(chipDbClose()));
	

	// load and set icons
	QPixmap iconBar, iconNew, iconOpn, iconSav, iconDig, iconM24, iconMat, iconGCN, iconGCO, iconGCS, iconGCP;
	QPixmap iconZmi, iconZmo, iconZha, iconMPt, iconMLi, iconMSn, iconMBl, iconMHe, iconRGr, iconBld, iconGeo, icon3DV; 

	iconBar.loadFromData(Barista,		sizeof(Barista),		 "PNG"); 			this->setWindowIcon(QIcon(iconBar));
	iconNew.loadFromData(filenew,		sizeof(filenew),		"PNG");				this->fileNewAction->setIcon(QIcon(iconNew));
	iconOpn.loadFromData(fileopen,		sizeof(fileopen),		"PNG");				this->fileOpenAction->setIcon(QIcon(iconOpn));
	iconSav.loadFromData(filesave,		sizeof(filesave),		"PNG");				this->fileSaveAction->setIcon(QIcon(iconSav));
		
	iconDig.loadFromData(DigitizeIcon, sizeof(DigitizeIcon),	"PNG");				this->DigitizeAction->setIcon(QIcon(iconDig));
	iconM24.loadFromData(Measure24,		sizeof(Measure24),		"PNG");				this->MeasureAction->setIcon(QIcon(iconM24));
	iconMat.loadFromData(Matching,		sizeof(Matching),		"PNG");				this->LSMAction->setIcon(QIcon(iconMat));

	//GCP chips	
	iconGCN.loadFromData(filenew,		sizeof(filenew),		"PNG");				this->CreateSCPDatabaseAction->setIcon(QIcon(iconGCN));
	iconGCO.loadFromData(fileopen,		sizeof(fileopen),		"PNG");				this->LoadSCPDatabaseAction->setIcon(QIcon(iconGCO));
	iconGCS.loadFromData(filesave,		sizeof(filesave),		"PNG");				this->SaveSCPDatabaseAction->setIcon(QIcon(iconGCS));
	iconGCP.loadFromData(SCP32,			sizeof(SCP32),			"PNG");				this->SCPcollectionAction->setIcon(QIcon(iconGCP));
	//icon.loadFromData(SCP32,		sizeof(SCP32),			"PNG");					this->CollectChipsAction->setIcon(QIcon(icon));

	iconZmi.loadFromData(zoomin22,		sizeof(zoomin22),		"PNG");				this->ZoomInAction->setIcon(QIcon(iconZmi));
	iconZmo.loadFromData(zoomout2,		sizeof(zoomout2),		"PNG");				this->ZoomOutAction->setIcon(QIcon(iconZmo));
	iconZha.loadFromData(hand2,			sizeof(hand2),			"PNG");				this->PanAction->setIcon(QIcon(iconZha));

	iconMPt.loadFromData(MonoPoints,	sizeof(MonoPoints),		"PNG");				this->MonoplotPointsAction->setIcon(QIcon(iconMPt));
	iconMLi.loadFromData(MonoLines,		sizeof(MonoLines),		"PNG");				this->MonoplotLinesAction->setIcon(QIcon(iconMLi));
	iconMSn.loadFromData(MonoplottingSnakeLine,sizeof(MonoplottingSnakeLine),	"PNG");	this->MonoplotSnakeLinesAction->setIcon(QIcon(iconMSn));
	
	iconMLi.loadFromData(MonoLines,	sizeof(MonoLines),		"PNG");					this->MonoplottingLinesAction->setIcon(QIcon(iconMLi));
	iconMSn.loadFromData(MonoplottingSnakeLine, sizeof(MonoplottingSnakeLine), "PNG");	this->MonoplottingSnakeLinesAction->setIcon(QIcon(iconMSn));

	iconMBl.loadFromData(MonoBuildings, sizeof(MonoBuildings), "PNG");				this->MonoplotBuildingsAction->setIcon(QIcon(iconMBl));
	iconMHe.loadFromData(MeasureHeights,sizeof(MeasureHeights),"PNG");				this->MeasureHeightsAction->setIcon(QIcon(iconMHe));
	//icon.loadFromData(MonoBuildings, sizeof(MonoBuildings), "PNG");				this->MonoplotBuildingsAction->setIcon(QIcon(icon));

	iconRGr.loadFromData(regionGrowingIcon,sizeof(regionGrowingIcon), "PNG");		this->RegionGrowingAction->setIcon(QIcon(iconRGr));
	iconBld.loadFromData(buildingIcon,	sizeof(buildingIcon), "TIF");				this->actionBuilding_Detection->setIcon(QIcon(iconBld));

	iconGeo.loadFromData(GeoLink,		sizeof(GeoLink),		"PNG");				this->geoLinkAction->setIcon(iconGeo);

	icon3DV.loadFromData(_3D,			sizeof(_3D),			"PNG");				this->Barista3DViewAction->setIcon(icon3DV);



	// EventHandler for BaristaViews
	currentBaristaViewHandler = &this->viewingHandler;

	this->viewingHandler.setParent(this);
	this->digitizingHandler.setParent(this);
	this->zoomInHandler.setParent(this);
	this->zoomOutHandler.setParent(this);
	this->panHandler.setParent(this);
	this->seedHandler.setParent(this);
	this->monoplottingPointHandler.setParent(this);
	this->monoplottingLineHandler.setParent(this);
	this->monoplottingSnakeHandler.setParent(this);
#ifdef WIN32	
	this->monoplottingBuildingHandler.setParent(this);
#endif	
	this->monoplottingHeightHandler.setParent(this);
	this->cameraMountingHandler.setParent(this);
	this->lsmHandler.setParent(this);
	this->measureHandler.setParent(this);
	//addition>>
	this->gcpchiphandler.setParent(this);
	//<<addition
	QShortcut* shortcut = new QShortcut(this);
	shortcut->setKey(Qt::CTRL + Qt::ALT + Qt::Key_J);
	this->connect(shortcut, SIGNAL(activated()),this, SLOT(shortCutActivated()));

#ifdef FINAL_RELEASE
	this->RegionGrowingAction->setEnabled( false);
	this->Barista3DViewAction->setEnabled( false);
	this->actionGenerate_DEM_by_Image_Matching->setEnabled(false);
	this->actionImport_GIS_data->setEnabled(false);
	this->actionCompute_staggered_ADS40_Image->setEnabled(false);
	//this->actionRegister_Images_Using_Image_Chips->setEnabled(false);
#endif

	//pull down menu: digitize menu
	this->DigitizeButtons = new QToolButton(this->toolBar);
	this->toolBar->insertWidget(this->ZoomInAction,this->DigitizeButtons);
	this->toolBar->insertSeparator(this->ZoomInAction);
	QMenu* menu = new QMenu();
	QList<QAction*> list;
	list.append(this->DigitizeAction);
	list.append(this->MeasureAction);
	list.append(this->LSMAction);
	list.append(this->SCPcollectionAction);
	menu->addActions(list);
	this->DigitizeButtons->setMenu(menu);
	this->DigitizeButtons->setPopupMode(QToolButton::MenuButtonPopup);
	this->DigitizeButtons->setDefaultAction(this->DigitizeAction);

	//pull down menu: monoplotting line menu
	this->MonoplotLinesButton = new QToolButton(this->toolBar);
	this->toolBar->insertWidget(this->MonoplotBuildingsAction, this->MonoplotLinesButton);
	QMenu* menu_lines = new QMenu();
	QList<QAction*> list_lines;
	list_lines.append(this->MonoplottingLinesAction);
	list_lines.append(this->MonoplottingSnakeLinesAction);
	menu_lines->addActions(list_lines);
	this->MonoplotLinesButton->setMenu(menu_lines);
	this->MonoplotLinesButton->setPopupMode(QToolButton::MenuButtonPopup);
	this->MonoplotLinesButton->setDefaultAction(this->MonoplottingLinesAction);

	this->toolBar->insertAction(this->RegionGrowingAction,this->actionBuilding_Detection);
	//this->toolBar->insertSeparator(this->geoLinkAction);

	this->initBaristaEmpty();
}

///////////////////////////////////////////////////////////////
bui_MainWindow::~bui_MainWindow()
{
	if (this->dongleTimer != NULL)
	{
		delete this->dongleTimer;
		this->dongleTimer = NULL;
	}

	//pull down menu: digitize menu
	if (this->DigitizeAction)
		delete this->DigitizeAction;
	this->DigitizeAction = NULL;

	if (this->MeasureAction)
		delete this->MeasureAction;
	this->MeasureAction =  NULL;

	if (this->LSMAction)
		delete this->LSMAction;
	this->LSMAction =  NULL;

	if (this->SCPcollectionAction)
		delete this->SCPcollectionAction;
	this->SCPcollectionAction =  NULL;

	if (this->digitizeMenu)
		delete this->digitizeMenu;
	this->digitizeMenu = NULL;

	if (this->DigitizeButtons)
		delete this->DigitizeButtons;
	this->DigitizeButtons = NULL;

	//pull down menu: monoplotting line menu
	if(this->MonoplottingLinesAction)
		delete this->MonoplottingLinesAction;
	this->MonoplottingLinesAction = NULL;

	if(this->MonoplottingSnakeLinesAction)
		delete this->MonoplottingSnakeLinesAction;
	this->MonoplottingSnakeLinesAction = NULL;
	
	if (this->monoplotLinesMenu)
		delete this->monoplotLinesMenu;
	this->monoplotLinesMenu = NULL;

	if (this->MonoplotLinesButton)
		delete this->MonoplotLinesButton;
	this->MonoplotLinesButton = NULL;

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::closeEvent(QCloseEvent* e)
{
	if (this->xPressed)
	{
		this->fileExit();
		if (!this->saved)
			e->ignore();
	}
	return ;
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::fileNew()
{
	if (!this->closeProject())
		return;

	Bui_NewProject dlg;
	dlg.exec();

	if (dlg.getSuccess())
	{
		if (!this->closeProject())
			return;
		project.setFileName(dlg.getFileName());
		this->initBarista();
		baristaProjectHelper.refreshTree();

	  inifile.setCurrDir(dlg.getFileName().GetPath());
	  inifile.writeIniFile();
	}

	return;
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::chipDbaseNew()
{
		if (!this->closeChipDbase())
			return;

		Bui_NewProject dlg(0,true);
		dlg.exec();

		if (dlg.getSuccess())
		{
			if (!this->closeChipDbase())
				return;
			project.setChipDbFileName(dlg.getFileName());
			project.setDefaultGCPvalues();
			this->SaveSCPDatabaseAction->setEnabled(true);
			this->CloseSCPDatabaseAction->setEnabled(true);
			//CGCPDataBasePoint* gcpPoint = project.getGCPpoint();

			//this->initBarista();
			//baristaProjectHelper.refreshTree();

		  //inifile.setCurrDir(dlg.getFileName().GetPath());
		  //inifile.writeIniFile();
		}
		return;
}

///////////////////////////////////////////////////////////////
bool bui_MainWindow::closeProject()
{

	if (!project.getFileName()->GetLength())
		return true;


	this->saved = false;

	CCharString text("Do you want to save project\n \"");
	text += project.getFileName()->GetFullFileName();
	text += "\" before closing?";
	switch(QMessageBox::information(this, "  Barista  ",
							text.GetChar(),
							"&Yes", "&No", "&Cancel",
							0, // Enter == button 0
							2))

	{ // Escape == button 2
		case 0:	 // Save clicked or Alt+S pressed or Enter pressed.
				this->fileSave();
				if (this->saved)
					this->xPressed = false;
				break;
		case 1: // Discard clicked or Alt+D pressed
				this->xPressed = false;
				break;
		case 2: // Cancel clicked or Escape pressed
				// don't exit
				return false;
	}

	currentBaristaViewHandler->close();

	this->closeAllWindows();
	this->initBaristaEmpty();
	baristaProjectHelper.refreshTree();

	return true;
}

////////////////////////
void bui_MainWindow::chipDbClose()
{
	closeChipDbase();
}

///////////////////////////////////////////////////////////////
bool bui_MainWindow::closeChipDbase()
{

	if (!project.getChipDbFileName()->GetLength())
		return true;


	//this->saved = false;

	CCharString text("Do you want to save chip database\n \"");
	text += project.getChipDbFileName()->GetFullFileName();
	text += "\" before closing?";
	switch(QMessageBox::information(this, "  Barista chip database ",
							text.GetChar(),
							"&Yes", "&No", "&Cancel",
							0, // Enter == button 0
							2))

	{ // Escape == button 2
		case 0:	 // Save clicked or Alt+S pressed or Enter pressed.
				this->chipDbFileSave();
				//if (this->saved)
				//	this->xPressed = false;
				break;
		case 1: // Discard/No clicked or Alt+D pressed
				//this->xPressed = false;
				break;
		case 2: // Cancel clicked or Escape pressed
				// don't exit
				return false;
	}

	//currentBaristaViewHandler->close();

	//this->closeAllWindows();
	//this->initBaristaEmpty();
	//baristaProjectHelper.refreshTree();
	project.setChipDbFileName("");	
	project.deleteGCPchipPoints(project.getGCPchipPoints());
	this->CloseSCPDatabaseAction->setEnabled(false);
	this->LoadSCPDatabaseAction->setEnabled(true);
	this->CreateSCPDatabaseAction->setEnabled(true);
	this->SaveSCPDatabaseAction->setEnabled(false);
	return true;
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::setTitle(CFileName title)
{
	CCharString t("Barista: ");
	if (title.GetLength() > 0)
		t += title.GetFullFileName();
	else
		t+="untitled";

	this->setWindowTitle(t.GetChar());
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::fileClose()
{
	// EventHandler for BaristaViews
	currentBaristaViewHandler->close();
	currentBaristaViewHandler = &this->viewingHandler;
	currentBaristaViewHandler->open();

	if (!this->closeProject())
		return;

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::fileOpen()
{

    // file dialog
	QString s = QFileDialog::getOpenFileName(
		this,
		"Choose a Barista Project file",
		inifile.getCurrDir(),
		"Barista Projects (*.bar)");

    QFile test(s);

    if (!test.exists())
    return;

	if (!this->closeProject())
		return;

    this->openFile((const char*)s.toLatin1());
}

///////////////////////////////////////////////////////////////
bool bui_MainWindow::openFile(CFileName projectName)
{
 	bui_ProgressDlg dlg(new CBaristaProjectReaderThread(&project,projectName),&project,"Reading Project ...");

	dlg.exec();

	if (!dlg.getSuccess())
	{
		QString message;
		message.sprintf("Error while reading the project:\n%s",projectName.GetChar());
		QMessageBox::critical(this,"Read error",message);
		return false;
	}

    baristaProjectHelper.restoreTree();
    inifile.setCurrDir(projectName.GetPath());

    inifile.writeIniFile();

	this->setTitle(*project.getFileName());
	this->initBarista();

	return true;
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::fileSave()
{

    if (project.getFileName()->GetLength() > 0)
    {
		baristaProjectHelper.setIsBusy(true);
		this->setCursor(Qt::BusyCursor);
		QString s(project.getFileName()->GetChar());

        QFile test(s);

		CFileName cfile((const char*)s.toLatin1());
		project.writeProject(cfile);

		inifile.setCurrDir(cfile.GetPath());
		inifile.writeIniFile();
		this->saved = true;
		this->setCursor(Qt::ArrowCursor);
		baristaProjectHelper.setIsBusy(false);
    }
    else
    {
        this->fileSaveAs();
    }
}


// GCP chips database file open
///////////////////////////////////////////////////////////////
void bui_MainWindow::chipDbFileOpen()
{

    if (bui_MainWindow::closeChipDbase())
    {
		baristaProjectHelper.setIsBusy(true);
		this->setCursor(Qt::BusyCursor);
		
		CFileName *projectFileName = project.getFileName();
		CCharString projectPath = projectFileName->GetPath();

		//select a file
		QString title("Choose a GCP database file");
	    // file dialog
		QString filename = QFileDialog::getOpenFileName(
			this,
			title,
			//"Choose a 2D Point file",
			(QString)projectPath,
			"Chip datavase (*.cdb)");

		// make sure the file exists
		QFile test(filename);
		if (!test.exists())
			return;
		test.close();
 
		CFileName cfilename = (const char*)filename.toLatin1();

		bool readSuccess = project.openGCPDbase(cfilename);

		if (readSuccess)
		{
			project.setChipDbFileName(cfilename);			
			this->SaveSCPDatabaseAction->setEnabled(true);
			this->CloseSCPDatabaseAction->setEnabled(true);
			baristaProjectHelper.refreshTree();	
		}
		else
		{
			QMessageBox::warning(this, "Chip database!"	 , "Openning fails!");
		}
		this->setCursor(Qt::ArrowCursor);
		baristaProjectHelper.setIsBusy(false);
    }
    //else
    //{
        //this->fileSaveAs();
		//QMessageBox::warning(this, "No chip database is currenly open!"	 , "Open or create a new chip database first!");
    //}
		
}

// GCP chips database file save
///////////////////////////////////////////////////////////////
void bui_MainWindow::chipDbFileSave()
{

    if (project.getChipDbFileName()->GetLength() > 0)
    {
		baristaProjectHelper.setIsBusy(true);
		this->setCursor(Qt::BusyCursor);
		//QString s(project.getChipDbFileName()->GetChar());

        //QFile test(s);
		
		//CFileName cfile((const char*)s.toLatin1());
		//project.writeProject(cfile);

		//inifile.setCurrDir(cfile.GetPath());
		//inifile.writeIniFile();
		//this->saved = true;

		project.saveGCPDbase();

		this->setCursor(Qt::ArrowCursor);
		baristaProjectHelper.setIsBusy(false);
    }
    else
    {
        //this->fileSaveAs();
		//QMessageBox::warning(this, "No chip database is currenly open!"	 , "Open or create a new chip database first!");
    }
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::fileSaveAs()
{

   	CCharString str = *project.getFileName();

    QString qstr( str.GetChar());

	// file dialog
	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Barista Project File Name",
		qstr,
		"Barista Projects (*.bar)");

	baristaProjectHelper.setIsBusy(true);
	if (s.isEmpty())
	{
		this->saved = false;
		baristaProjectHelper.setIsBusy(false);
		return;
	}
	else
		this->saved = true;

	if (!s.endsWith(".bar"))
        s += ".bar";

    CFileName cfile = (const char*)s.toLatin1();

	project.setFileName(cfile.GetChar());
    project.writeProject(cfile.GetChar());

    inifile.setCurrDir(cfile.GetPath());
    inifile.writeIniFile();

	this->setTitle(*project.getFileName());
	baristaProjectHelper.setIsBusy(false);
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::fileExit()
{
	if (this->closeProject())
		exit(0);

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::shortCutActivated()
{
	TetrixWindow* window= new TetrixWindow(this) ;
	window->setAttribute(Qt::WA_DeleteOnClose,true);
	window->show();
}

///////////////////////////////////////////////////////////////
/**
 * import images to project and add thumbnail to tree
 *
 */
void bui_MainWindow::importImages()
{

	Bui_GDALImportDlg dlg;
	dlg.exec();
	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
/**
 * user has double clicked an image thumbnail, open the image
 *
 */
void bui_MainWindow::thumbnailDblClick(QTreeWidgetItem* item, int i)
{
	if ( item == NULL) // if doubleclicked on empty space
		return;

	if ( this->toolBar->isHidden() )
		this->toolBar->show();

    CBaristaIconViewItem* bitem = (CBaristaIconViewItem*)item;

    const ZObject* object = bitem->getObject();

	if (!object)
		return;

    if (object->isa("CVImage"))
    {
		CImageView* imageView = 0;
		CVImage* image = (CVImage*)object;
		imageView = new CImageView(image, workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(imageView);
		imageView->setAttribute(Qt::WA_DeleteOnClose);
        imageView->setUpdatesEnabled(true);
		if (!imageView->isVisible()) imageView->show();
		else imageView->activateWindow();
    }
    else if (object->isa("CXYPointArray"))
    {
		CXYPointArray* points = (CXYPointArray*)object;

		CObjectSelectorTable* pointTable = NULL;

		ViewParameter viewParameter;

		viewParameter.showSelector = false;
		viewParameter.showSigma = false;
		viewParameter.showCoordinates = true;
		viewParameter.allowSigmaChange = false;
		viewParameter.allowLabelChange = false;
		viewParameter.allowDeletePoint = false;
		viewParameter.allowBestFitEllipse = false;
		viewParameter.allowXYZAveragePoints = false;

		if (points->getPointType() == MEASURED)
		{
			viewParameter.showSigma = true;
			viewParameter.allowSigmaChange = true;
			viewParameter.allowLabelChange = true;
			viewParameter.allowDeletePoint = true;
			viewParameter.allowBestFitEllipse = true;
		}
		else if (points->getPointType() == RESIDUALS)
		{
			viewParameter.showSigma = true;
		}

		// needs parent (image) for ellipse from Table
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)item->parent();
		const ZObject* imageobject = imageitem->getObject();
		CVImage* image = (CVImage*)imageobject;

		CObsPointDataModel* dataModel = new CObsPointDataModel(points,viewParameter,image); // creats a datamodel and sets the view for the given data and view parameters
		pointTable = new CObjectSelectorTable(dataModel,false,workspace); // creates an object selector to select objects in the table

		QMdiSubWindow *widget = workspace->addSubWindow(pointTable);
		pointTable->setAttribute(Qt::WA_DeleteOnClose);
		if (!pointTable->isVisible()) pointTable->show();
		else pointTable->activateWindow();
    }
    else if (object->isa("CXYPolyLineArray"))
    {
		CXYPolyLineArray* lineArray = (CXYPolyLineArray*)object;

		CObjectSelectorTable* polyLineTable = NULL;

		ViewParameter viewParameter;

		viewParameter.showSelector = false;
		viewParameter.showSigma = false;
		viewParameter.showCoordinates = false;
		viewParameter.allowSigmaChange = false;
		viewParameter.allowLabelChange = false;
		viewParameter.allowDeletePoint = true;
		viewParameter.allowBestFitEllipse = false;
		viewParameter.allowXYZAveragePoints = false;


		// needs parent (image) for ellipse from Table
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)item->parent();
		const ZObject* imageobject = imageitem->getObject();
		CVImage* image = (CVImage*)imageobject;

		CPolyLineDataModel* dataModel = new CPolyLineDataModel(lineArray,viewParameter);
		polyLineTable = new CObjectSelectorTable(dataModel,false,workspace);

		QMdiSubWindow *widget = workspace->addSubWindow(polyLineTable);
		polyLineTable->setAttribute(Qt::WA_DeleteOnClose);
		if (!polyLineTable->isVisible()) polyLineTable->show();
		else polyLineTable->activateWindow();
    }
    else if (object->isa("CXYZPointArray"))
    {
		CXYZPointArray* points = (CXYZPointArray*) object;
		CObjectSelectorTable* pointTable = NULL;

		ViewParameter viewParameter;

		viewParameter.showSelector = false;
		viewParameter.showSigma = false;
		viewParameter.showCoordinates = true;
		viewParameter.allowSigmaChange = false;
		viewParameter.allowLabelChange = false;
		viewParameter.allowDeletePoint = false;
		viewParameter.allowBestFitEllipse = false;
		viewParameter.allowXYZAveragePoints = false;

		if (points->getPointType() == eBUNDLERESULT || points->getPointType() == eNOTYPE ||  points->getPointType() ==eGEOREFERENCED)
		{
			viewParameter.showSigma = true;
			viewParameter.allowSigmaChange = true;
			viewParameter.allowLabelChange = true;
			viewParameter.allowDeletePoint = true;
			viewParameter.allowBestFitEllipse = true;
		}
		else if (points->getPointType() == eRESIDUAL)
		{
			viewParameter.showSigma = true;
		}
		else if (points->getPointType() == eMONOPLOTTED)
		{
			viewParameter.showSigma = true;
			viewParameter.allowDeletePoint = true;
		}

		CObsPointDataModel* dataModel = new CObsPointDataModel(points,viewParameter);
		pointTable = new CObjectSelectorTable(dataModel,false,workspace);

		QMdiSubWindow *widget = workspace->addSubWindow(pointTable);
		pointTable->setAttribute(Qt::WA_DeleteOnClose);
        if (!pointTable->isVisible()) pointTable->show();
		else pointTable->activateWindow();
    }
    else if (object->isa("CRPC"))
    {
		CRPC* rpc = (CRPC*)object;
        CRPCTable* rpcTable = new CRPCTable(rpc, workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(rpcTable);
		rpcTable->setAttribute(Qt::WA_DeleteOnClose);
		if (!rpcTable->isVisible()) rpcTable->show();
		else rpcTable->activateWindow();
	}
    else if (object->isa("CAffine"))
    {
		CAffine* affine = (CAffine*) object;
        CAffineTable* affineTable = new CAffineTable(affine,workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(affineTable);
		affineTable->setAttribute(Qt::WA_DeleteOnClose);
        if (!affineTable->isVisible()) affineTable->show();
		else affineTable->activateWindow();
    }
    else if (object->isa("CPushBroomScanner"))
    {

		CPushBroomScanner* pbs = (CPushBroomScanner*)object;
		Bui_PushbroomDlg* dlg = new Bui_PushbroomDlg(pbs);

		// QT takes care of the memory!
		Bui_DialogContainer* dlgContainer = new Bui_DialogContainer(dlg,this,550,450);
		dlgContainer->show();

	}
    else if (object->isa("CTFW"))
    {

        CTFW* tfw = (CTFW*)object;
		CTFWTable* tfwTable = new CTFWTable(tfw,workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(tfwTable);
		tfwTable->setAttribute(Qt::WA_DeleteOnClose);
		if (!tfwTable->isVisible()) tfwTable->show();
		else tfwTable->activateWindow();
    }
    else if (object->isa("CVDEM"))
    {
		CVDEM* vdem = (CVDEM*)object;
		CDEMView* demView = new CDEMView(vdem, workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(demView);
		demView->setAttribute(Qt::WA_DeleteOnClose);
        demView->setUpdatesEnabled(true);
		if (!demView->isVisible()) demView->show();
		else demView->activateWindow();
    }
	else if (object->isa("CVALS"))
    {
		CVALS* als = (CVALS*)object;
		CALSTable* alsTable = new CALSTable(als, workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(alsTable);
		alsTable->setAttribute(Qt::WA_DeleteOnClose);

        if (!alsTable->isVisible()) alsTable->show();
		else alsTable->activateWindow();
    }
    else if (object->isa("CXYZPolyLineArray"))
    {
        CXYZPolyLineArray* linearray = (CXYZPolyLineArray*)object;
		CObjectSelectorTable* polylineTable = NULL;

		ViewParameter viewParameter;

		viewParameter.showSelector = false;
		viewParameter.showSigma = false;
		viewParameter.showCoordinates = true;
		viewParameter.allowSigmaChange = false;
		viewParameter.allowLabelChange = true;
		viewParameter.allowDeletePoint = true;
		viewParameter.allowBestFitEllipse = false;
		viewParameter.allowXYZAveragePoints = false;

		CPolyLineDataModel* dataModel = new CPolyLineDataModel(linearray,viewParameter);
		polylineTable = new CObjectSelectorTable(dataModel,false,workspace);


	//	CXYZPolyLineTable* polylineTable = new CXYZPolyLineTable(linearray, workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(polylineTable);
		polylineTable->setAttribute(Qt::WA_DeleteOnClose);
        if (!polylineTable->isVisible()) polylineTable->show();
		else polylineTable->activateWindow();
    }
#ifdef WIN32    
    else if (object->isa("CBuildingArray"))
    {
        CBuildingArray* buildings = (CBuildingArray*)object;
		CBuildingTable* buildingTable = new CBuildingTable(buildings, workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(buildingTable);
		buildingTable->setAttribute(Qt::WA_DeleteOnClose);
		if (!buildingTable->isVisible()) buildingTable->show();
		else buildingTable->activateWindow();
    }
#endif    
    else if (object->isa("CXYEllipseArray"))
    {
        CXYEllipseArray* ellipses = (CXYEllipseArray*)object;
		CXYEllipseTable* ellipseTable = new CXYEllipseTable(ellipses,workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(ellipseTable);
		ellipseTable->setAttribute(Qt::WA_DeleteOnClose);
        if (!ellipseTable->isVisible()) ellipseTable->show();
		else ellipseTable->activateWindow();
    }
	else if (object->isa("COrbitObservations"))
	{
		COrbitObservations* observation = (COrbitObservations*) object;
		Bui_OrbitParameterDlg* dlg = new Bui_OrbitParameterDlg((CSplineModel*)observation->getCurrentSensorModel(),observation->getSplineType());

		// QT takes care of the memory!
		Bui_DialogContainer* dlgContainer = new Bui_DialogContainer(dlg,this,550,400);
		dlgContainer->show();

	}
	else if (object->isa("CCCDLine"))
	{
		CCCDLine* camera = (CCCDLine*) object;
		Bui_CameraDlg* dlg = new Bui_CameraDlg(camera);

		// QT takes care of the memory!
		Bui_DialogContainer* dlgContainer = new Bui_DialogContainer(dlg,this,550,500);
		dlgContainer->show();


	}
	else if (object->isa("CFrameCameraModel"))
	{
		CFrameCameraModel* frameCamera = (CFrameCameraModel*) object;
		Bui_FrameCameraModelDlg* dlg = new Bui_FrameCameraModelDlg(frameCamera,true,this);

		// QT takes care of the memory!
		Bui_DialogContainer* dlgContainer =  new Bui_DialogContainer(dlg,this,650,500);
		dlgContainer->show();

	}
	else if (object->isa("CFrameCamera"))
	{
		CFrameCamera* frameCamera = (CFrameCamera*) object;
		Bui_FrameCameraDlg* dlg = new Bui_FrameCameraDlg(frameCamera,this);

		// QT takes care of the memory!
		Bui_DialogContainer* dlgContainer = new Bui_DialogContainer(dlg,this,650,500);
		dlgContainer->show();


	}
	else if (object->isa("CFrameCameraAnalogue"))
	{
		CFrameCameraAnalogue* frameCamera = (CFrameCameraAnalogue*) object;
		Bui_FrameCameraDlg* dlg = new Bui_FrameCameraDlg(frameCamera,this);

		// QT takes care of the memory!
		Bui_DialogContainer* dlgContainer = new Bui_DialogContainer(dlg,this,650,500);
		dlgContainer->show();

	}
	else if (object->isa("CCameraMounting"))
	{
		CCameraMounting* cameraMounting = (CCameraMounting*) object;
		Bui_CameraMountingDlg* dlg = new Bui_CameraMountingDlg(cameraMounting,false);

		// QT takes care of the memory!
		Bui_DialogContainer* dlgContainer= new Bui_DialogContainer(dlg,this,400,300);
		dlgContainer->show();

	}

	workspace->tileSubWindows();
}



///////////////////////////////////////////////////////////////
void bui_MainWindow::OrthoImageGeneration()
{
	if ( project.getNumberOfDEMs() == 0 )
	{
		QMessageBox::warning( this, "No DEM available in this project!",
								 "No orthophoto generation possible!");
	}
	else
	{	
		project.resetSelectedCVImage();
		
		bui_OrthoDialog orthoDlg(0, 0, 0, inifile.getCurrDir());

		orthoDlg.show();
		orthoDlg.exec();

		if (orthoDlg.getOrthoCreated()) baristaProjectHelper.refreshTree();
	}
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::PanSharpenOrtho()
{
	PanSharpening(true);
};

void bui_MainWindow::PanSharpenOriginal()
{
	PanSharpening(false);
};

///////////////////////////////////////////////////////////////
void bui_MainWindow::PanSharpening(bool orthorectified)
{
	int imagecount = project.getNumberOfImages();
	CVImage* pansharpenimage = project.addImage("", false);
    bui_PanDialog panDlg(orthorectified);

	panDlg.setPANsharpenedImage( pansharpenimage);
    panDlg.show();
    panDlg.exec();

	if ( panDlg.panSharpIsCreated() )
		panDlg.exportImage();
	else
		project.deleteImageAt(imagecount);

	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::testline()
{
    CBestFitLine bfl;

    Matrix obs(3, 2);

    /* *
    obs(1, 1)=1.05;
    obs(1, 2)=1.05;
    obs(2, 1)=1.95;
    obs(2, 2)=2.08;
    obs(3, 1)=2.92;
    obs(3, 2)=2.90;
    * */

    obs(1, 1)=1.;
    obs(1, 2)=1.;
    obs(2, 1)=2.;
    obs(2, 2)=2.;
    obs(3, 1)=3.;
    obs(3, 2)=3.;


    CBlockDiagonalMatrix Q(2, 2, 3);

    for (int i = 0; i < 3; i++)
    {
        Matrix* qi = Q.getSubMatrix(i);

        if (i < 2)
        {
            (*qi)(1, 1) = .1;
            (*qi)(2, 2) = .1;
        }
        else
        {
            (*qi)(1, 1) = .1;
            (*qi)(2, 2) = .1;
        }
    }

    CLeastSquaresSolver lss(&bfl, &obs, NULL, NULL, &Q);
    lss.solve();

    Matrix* parms = lss.getSolution();

    double a0 = (*parms)(1, 1);
    double a1 = (*parms)(2, 1);
    int hmm = 0;


    Matrix a(4, 4);

    for (int i = 1; i <=4; i++)
    {
        for (int j = 1; j <=4; j++)
        {
            a(i, j) = i*j;
        }
    }

    Matrix b = a.SubMatrix(1, 2, 1, 3);

    b(1, 1) = 999;
     hmm = 0;
}


///////////////////////////////////////////////////////////////
void bui_MainWindow::import3DPoints()
{

	CFileName filename;
	CReferenceSystem refSys;

	Bui_ReferenceSysDlg dlg(&refSys,&filename, "Import 3D Points", "UserGuide/Import3DPoints.html", true,"3D Points (*.txt *.csv *.gcp)");
	dlg.exec();

	if ( !dlg.getSuccess())	return;

	project.addXYZPoints(filename, refSys, false, true,eNOTYPE);

	inifile.setCurrDir(filename.GetPath());
	inifile.writeIniFile();

	baristaProjectHelper.refreshTree();
}


///////////////////////////////////////////////////////////////
void bui_MainWindow::importALSData()
{

	bui_ImportALSDialog dlg;
	dlg.exec();

	if (dlg.getSuccess())
	{
		vector<CFileName> filenames = dlg.getInputFilenames();
		for (unsigned int i = 0; i < filenames.size(); ++i)
		{

			CVALS *pALS = 0;
			if (dlg.getHasStrips())
			{
				pALS = project.addALSStrips(filenames[i], dlg.getFormat(), dlg.getPulseMode(), dlg.getRefsys(), dlg.getOffset());
			}
			else
			{
				pALS = project.addALSTiles(filenames[i], dlg.getFormat(), dlg.getPulseMode(), dlg.getRefsys(), dlg.getOffset(), dlg.getDelta());
			}

			if (pALS)
			{
				CProgressThread *thread = new CALSDataReaderThread(pALS->getCALSData(), filenames[i], dlg.getFormat(), dlg.getPulseMode(), dlg.getOffset());
				ostrstream oss; 
				oss << "Importing ALS data (" << (i + 1) << "/" << filenames.size() << ")" << ends;
				char *z = oss.str();

				bui_ProgressDlg dlg(thread, pALS->getCALSData(), z);

				delete [] z;

				dlg.exec();

				bool ret = dlg.getSuccess();

				if (!ret) project.deleteALS(pALS);
			}
		}

		if (filenames.size() > 0) inifile.setCurrDir(filenames[0].GetPath());
		inifile.writeIniFile();

		baristaProjectHelper.refreshTree();
	}
}


///////////////////////////////////////////////////////////////
void bui_MainWindow::RPCBundleClicked()
{
    for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		project.getImageAt(i)->setCurrentSensorModel(eRPC);
    }
	CVControl* Control = project.getControlPoints();

    if (Control == NULL)
	{
		QMessageBox::warning( this, "  No Control Points found!", "Set Control Points first!");
	}
	else
	{
		CReferenceSystem refsys = Control->getCurrentSensorModel()->getRefSys();

		if ((refsys.getCoordinateType() != eGEOGRAPHIC) || (!refsys.isWGS84()))
		{
			QMessageBox::warning( this, "  Control Points are not in WGS84Geographic!",
									"Use WGS84Geographic control points for RPC Bundle!");
		}
		else
		{
			CBaristaBundleHelper helper;
			CCharString path = project.getFileName()->GetPath();
			CFileName protfile = path + CSystemUtilities::dirDelimStr + "iteration.prt";
			helper.initProtocol(protfile.GetChar());
			helper.bundleInit();

			if (helper.getNStations() < 2)
			{
				QMessageBox::warning( this, "  RPC Bundle not possible!",
						"RPC Bundle requires at least one station with RPCs and measured image points.");
			}
			else
			{
				bool maybe = helper.solve(false, false, false);

				if (!maybe)
				{
					QString caption("Adjustment failure!"), text(" Adjustment did not converge");
					QMessageBox::warning( this, caption, text);
				}
				else
				{
					helper.closeProtocol();
					protfile = path + CSystemUtilities::dirDelimStr + "adjust.prt";
					helper.initProtocol(protfile.GetChar());
					
					helper.printResults();

					CXYZPointArray* points = project.addXYZPoints("RPCBundle", refsys, true, false,eBUNDLERESULT);

					helper.getObjectPoints(*points);

					helper.updateResiduals();
					helper.setSensorFilenames();
					baristaProjectHelper.refreshTree();

					QString caption("Adjustment successful!"), text(helper.rmsString(" Adjustment was successful; s0: ").GetChar());
					QMessageBox::information( this, caption, text);
				}
			}
			helper.closeProtocol();
		}
	}

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::RPCBiasCorrectionNone()
{
    project.RPCBiasCorrectionNone();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::RPCBiasCorrectionShift()
{
    project.RPCBiasCorrectionShift();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::RPCBiasCorrectionShiftDrift()
{
    project.RPCBiasCorrectionShiftDrift();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::RPCBiasCorrectionAffine()
{
    project.RPCBiasCorrectionAffine();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::RPCRegenerateBiasCorrected()
{
    project.RPCRegenerateBiasCorrected();
	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::resetRPCtoOriginal()
{
    project.resetRPCtoOriginal();

	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::exportAllupdatedIkonosRPCs()
{
    QString qstr( inifile.getCurrDir());

	QString s = QFileDialog::getExistingDirectory(
		this,
		"Choose a directory for exported RPC (IKONOS format)",
		qstr,
		QFileDialog::ShowDirsOnly);

    if ( s.isNull())
        return;

	CFileName dirname = (const char*)s.toLatin1();

	inifile.setCurrDir(dirname.GetChar());

	for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		if ( project.getImageAt(i)->hasRPC())
        {
			CCharString str = inifile.getCurrDir();
			str += project.getImageAt(i)->getRPC()->getItemText().GetChar();
            str += "Ikonos.txt";
            project.getImageAt(i)->getRPC()->writeIKONOSFormat(str.GetChar());
        }
    }
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::exportAllupdatedQuickBirdRPCs()
{
    QString qstr( inifile.getCurrDir());

	QString s = QFileDialog::getExistingDirectory(
		this,
		"Choose a directory for exported RPC (QuickBird format)",
		qstr,
		QFileDialog::ShowDirsOnly);

    if ( s.isNull())
        return;

	CFileName dirname = (const char*)s.toLatin1();

	inifile.setCurrDir(dirname.GetChar());

    for (int i = 0; i < project.getNumberOfImages(); i++)
    {
        if ( project.getImageAt(i)->hasRPC())
        {
            CCharString str = inifile.getCurrDir();
            str += project.getImageAt(i)->getRPC()->getItemText().GetChar();
            str += "QB.txt";
            project.getImageAt(i)->getRPC()->writeQuickBirdFormat(str.GetChar());
        }
    }
}


///////////////////////////////////////////////////////////////
bool bui_MainWindow::setActiveDEM()
{
	if (!project.getActiveDEM())
	{
		CObjectSelector selector;
        selector.makeDEMSelector();

        ZObject* object = selector.getSelection();
        CVDEM* vdem = (CVDEM*)object;

        if ( !vdem )
		{
			//QMessageBox::warning( this, "No DEM available!",
			//					  "Set active DEM not possible!");
            return false;
		}

		vdem->getDEM()->load();
		project.setActiveDEM(vdem);
	}

	return true;
}


///////////////////////////////////////////////////////////////
void bui_MainWindow::AffineBundleClicked()
{
	int withAffine = 0;
	for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		project.getImageAt(i)->setCurrentSensorModel(eAFFINE);
		if ( project.getImageAt(i)->getAffine()->gethasValues())
		{
			withAffine++;
		}
    }

	if ( withAffine == 0 )
	{
		QMessageBox::warning( this, "No Image has Affine parameters!",
			"Compute Forward Affine for at least one image before Affine Bundle!");
		return;
	}

    CVControl* Control = project.getControlPoints();

    if (Control == NULL)
	{
		 QMessageBox::warning( this, "No Control Points found!"	 , "Set Control Points first!");
	}
	else
	{
		CBaristaBundleHelper helper;
		CFileName protfile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "iteration.prt";

		helper.initProtocol(protfile.GetChar());
		helper.bundleInit();

		if (helper.getNStations() < 2)
		{
			 QMessageBox::warning( this, "  Affine Bundle not possible!",
			   "Affine Bundle requires at least one station with Affine parameters and measured image points.");
		}
		else
		{
			bool maybe = helper.solve(false, false, false);

			if (!maybe)
			{
				QMessageBox::warning( this, "Adjustment did not converge!", " ");
			}
			else
			{
				QMessageBox::information( this, "Adjustment successful!", helper.rmsString(" Adjustment was successful; s0: ").GetChar());
				helper.closeProtocol();

				protfile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "adjust.prt";
				helper.initProtocol(protfile.GetChar());
				helper.printResults();

				CXYZPointArray* points = project.addXYZPoints("AffineBundle", Control->getCurrentSensorModel()->getRefSys(), true, false,eBUNDLERESULT);

				helper.getObjectPoints(*points);

				// to update Affine sets
				helper.updateResiduals();
				helper.setSensorFilenames();
			}

			helper.closeProtocol();
			baristaProjectHelper.refreshTree();
		}
	}
}


///////////////////////////////////////////////////////////////
void bui_MainWindow::closeAllWindows()
{
    workspace->closeAllSubWindows();
	/*QWidgetList list =workspace->subWindowList();
	for (int i=0; i<list.count(); i++)
	{
		QWidget* w = list.at(i);
		w->close();
		delete w;
	}*/

	// catch all object selector table dialogs and delete as well
	QList<QDialog*> dialogs = workspace->findChildren<QDialog*>();
	for (int i=0; i < dialogs.size(); i++)
	{
		QDialog* dlg = dialogs.at(i);
		delete dlg;
	}

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::importDEMs()
{

	Bui_GDALImportDlg dlg(true);
	dlg.exec();
	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::digitizeButtonToggle( bool b )
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		currentBaristaViewHandler = &this->digitizingHandler;

		// change the icon
		if (this->DigitizeButtons->defaultAction() != this->DigitizeAction)
			this->DigitizeButtons->setDefaultAction(this->DigitizeAction);

	}
	else if ( !b && currentBaristaViewHandler == &this->digitizingHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
}




///////////////////////////////////////////////////////////////
void bui_MainWindow::measureButtonToggle( bool b )
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		currentBaristaViewHandler = &this->measureHandler;

		// change the icon
		if (this->DigitizeButtons->defaultAction() != this->MeasureAction)
			this->DigitizeButtons->setDefaultAction(this->MeasureAction);

	}
	else if ( !b && currentBaristaViewHandler == &this->measureHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::lsmButtonToggle( bool b )
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		currentBaristaViewHandler = &this->lsmHandler;

		// change the icon
		if (this->DigitizeButtons->defaultAction() != this->LSMAction)
			this->DigitizeButtons->setDefaultAction(this->LSMAction);

	}
	else if ( !b && currentBaristaViewHandler == &this->lsmHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::scpCollectionButtonToggle()
{
	if (this->getCollectGCPActionActive())
	{
//		this->setCollectGCPActionActive(false);
		scpCollectionButtonToggle(false);
	}
	else
	{
		//this->setCollectGCPActionActive(true);
		scpCollectionButtonToggle(true);
	}
}
///////////////////////////////////////////////////////////////
void bui_MainWindow::scpCollectionButtonToggle( bool b )
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		//if (!project.getChipDbFileName()->GetLength())
		//{	//give warning and close current barista view handler
		//	QMessageBox::warning(this, "No chip database is currently open!"	 , "Open or create a new chip database first!");
			//CGCPchipHandler::close();
		//}
		//else // chip file exists, so allow collecting chips
		//{
			currentBaristaViewHandler = &this->gcpchiphandler;
			this->setCollectGCPActionActive(true);

			// change the icon
			if (this->DigitizeButtons->defaultAction() != this->SCPcollectionAction)
			this->DigitizeButtons->setDefaultAction(this->SCPcollectionAction);
		//}

	}
	else if ( !b && currentBaristaViewHandler == &this->gcpchiphandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
		this->setCollectGCPActionActive(false);
	}

	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::setZoomIn(bool b )
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		currentBaristaViewHandler = &this->zoomInHandler;
	}
	else if ( !b && currentBaristaViewHandler == &this->zoomInHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::setZoomOut(bool b )
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		currentBaristaViewHandler = &this->zoomOutHandler;
	}
	else if ( !b && currentBaristaViewHandler == &this->zoomOutHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::setPanMove(bool b )
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		currentBaristaViewHandler = &this->panHandler;
	}
	else if ( !b && currentBaristaViewHandler == &this->panHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::regionGrowing(bool b)
{
	currentBaristaViewHandler->close();
	if ( b )
	{
		currentBaristaViewHandler = &this->seedHandler;
	}
	else if ( !b && currentBaristaViewHandler == &this->seedHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
};

////////////////////////////////////////////////////////////////
void bui_MainWindow::BuildingsDetection()
{
		bui_BuildingsDetector buildings;
		buildings.show();
		buildings.exec();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::MatchDEM()
{
	if ( project.getNumberOfImages() < 2 )
	{
		QMessageBox::warning( this, "You need at least two images in your project!",
								 "DEM matching is not possible!");
	}
	else
	{
		Bui_DEMMatchingDlg dlg;

		dlg.exec();

		if (dlg.getSuccess())
		{
			baristaProjectHelper.refreshTree();
		}
	}
};

///////////////////////////////////////////////////////////////
void bui_MainWindow::RegisterImagesByOrthophotos()
{
	bui_GCPMatchingDialog dlg;
	if (!dlg.isInitialised())
	{
		QMessageBox::warning(0, "Matching of GCPs not possible!", 
			                 "Matching of GCPs requires an orthophoto with TFW node");
	}
	else
	{
		dlg.exec();
		if (dlg.getSuccess()) baristaProjectHelper.refreshTree();
	}
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::MonoplotPoints(bool b)
{
  if ( b )
	{
	currentBaristaViewHandler=&this->viewingHandler;
	currentBaristaViewHandler->close(); // close the current event handler, one event handler at any time, now new handler will be activated
    HeightOptions dlg;
	dlg.exec();
	
		if (project.getMeanHeightMonoplotting() && dlg.getsuccess())
		{
		  this->AskForHeightMonoplotting();
 		currentBaristaViewHandler = &this->monoplottingPointHandler;
		}
		else if (project.getHeightPointMonoplotting() && dlg.getsuccess())
		{
 		project.setMeanHeightMonoplotting(true);
		currentBaristaViewHandler = &this->monoplottingPointHandler;
		}
		else if (project.getHeightDEMMonoplotting() && dlg.getsuccess())
		{
			if (!this->setActiveDEM())
			{
				QMessageBox::warning(this, "No DEM is available!","DEM cannot be set active!");
			    currentBaristaViewHandler =false;
			}
		currentBaristaViewHandler = &this->monoplottingPointHandler;
		}
		else if(dlg.getsuccess())
		{
		    currentBaristaViewHandler =false;
		}

		currentBaristaViewHandler->open();
	}
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::MonoplottingLines()
{
	MonoplottingLines(true);
}

void bui_MainWindow::MonoplottingLines(bool b)
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		if ( !this->setActiveDEM() && !project.getMeanHeightMonoplotting() )
			this->AskForHeightMonoplotting();

		if ( this->setActiveDEM() || project.getMeanHeightMonoplotting() )
			currentBaristaViewHandler = &this->monoplottingLineHandler;

		// change the icon
		if (this->MonoplotLinesButton->defaultAction() != this->MonoplottingLinesAction)
			this->MonoplotLinesButton->setDefaultAction(this->MonoplottingLinesAction);

	}
	else if ( !b && currentBaristaViewHandler == &this->monoplottingLineHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::MonoplottingSnakeLines()
{
	MonoplottingSnakeLines(true);
}

void bui_MainWindow::MonoplottingSnakeLines(bool b)
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		if ( !this->setActiveDEM() && !project.getMeanHeightMonoplotting() )
			this->AskForHeightMonoplotting();

		if ( this->setActiveDEM() || project.getMeanHeightMonoplotting() )
			currentBaristaViewHandler = &this->monoplottingSnakeHandler;;

		// change the icon
		if (this->MonoplotLinesButton->defaultAction() != this->MonoplottingSnakeLinesAction)
			this->MonoplotLinesButton->setDefaultAction(this->MonoplottingSnakeLinesAction);
	}
	else if ( !b && currentBaristaViewHandler == &this->monoplottingSnakeHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::MonoplotHeights(bool b)
{
	currentBaristaViewHandler->close();

	if ( b )
	{
		if ( !this->setActiveDEM() && !project.getMeanHeightMonoplotting() )
			this->AskForHeightMonoplotting();

		if ( this->setActiveDEM() || project.getMeanHeightMonoplotting() )
			currentBaristaViewHandler = &this->monoplottingHeightHandler;
	}
	else if ( !b && currentBaristaViewHandler == &this->monoplottingHeightHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::MonoplotBuildings(bool b)
{
#ifdef WIN32
	currentBaristaViewHandler->close();


	if ( b )
	{
		if ( !this->setActiveDEM() && !project.getMeanHeightMonoplotting() )
			this->AskForHeightMonoplotting();

		if ( this->setActiveDEM() || project.getMeanHeightMonoplotting() )
			currentBaristaViewHandler = &this->monoplottingBuildingHandler;
	}
	else if ( !b && currentBaristaViewHandler == &this->monoplottingBuildingHandler )
	{
		currentBaristaViewHandler = &this->viewingHandler;
	}

	currentBaristaViewHandler->open();
#endif	
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::ImageRegistration()
{
	if (project.getNumberOfImages() < 2)
		QMessageBox::warning( this, "Image registration cannot be started", " To start image registration you need at least two images ");
	else
	{
		bui_ImageRegistrationDialog imageRegistration(true, 0, 0);
		imageRegistration.show();
		imageRegistration.exec();
		if ( imageRegistration.isFinished() )
		{
			int hmmmm = 5;
		}
	}
};

///////////////////////////////////////////////////////////////
void bui_MainWindow::acHelpGetStarted()
{
	CHtmlHelp::callHelp("Introduction.html");
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::acHelpContens()
{
	CHtmlHelp::callHelp("UserGuide/MainWindow.html");
}

void bui_MainWindow::acHelpSearch()
{
	CHtmlHelp::callSearch();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::acHelpAbout()
{
	CCharString text = "<H2>Barista ";

	CCharString link_splash = QApplication::applicationDirPath().toLatin1().constData();
	link_splash += "/splash.png";


	CCharString link_crcsi = QApplication::applicationDirPath().toLatin1().constData();
	link_crcsi += "/CRCSILogo.png";
//
//
//#ifdef FINAL_RELEASE
//	CCharString version = "Final Release";
//#elif _DEBUG
//	CCharString version = "Debug Version";
//#else
//	CCharString version = "Release Version (Testing)";
//#endif
//
//	text += BARISTA_VERSION;
//	text += "</H2>";
//	text += "<p><img src=\"";
//	text += link_splash;
//	text += "\" alt=\"Barista Logo\"></p>";
//	text += "<P>Date: ";
//	text += __DATE__;
//	text += "</P><P>Version: ";
//	text += version;
//
//#ifdef WIN32
//	if (CHardwareDongle::getProtectionType() == eDongleOK)
//	{
//#ifdef FINAL_VERSION
//#else
//		text += "</P><P>Protection: Dongle protected";
//#endif
//	}
//	else if (CHardwareDongle::getProtectionType() == eTrialOK)
//	{
//		text += "</P><P>Protection: Trial Version";
//	}
//	else
//	{
//#ifdef FINAL_VERSION
//	text += "</P><P>Protection: CRC-SI Project Partner";
//#else
//	text += "</P><P>Protection: None";
//#endif
//	}
//
//#else // WIN32
//	text += "</P><P>Protection: None";
//
//#endif // WIN32
//


	text += "</P><P>Copyright &copy; 2008 Spatial Information Systems Ltd (SISL)</P>";
	text += "<H4>Contact:</H4>";
	text += "<P><A href=\"www.baristasoftware.com.au\">www.baristasoftware.com.au</A></P>";
	text += "<P><A href=\"mailto:contact@baristasoftware.com.au\">contact@baristasoftware.com.au</A></P>";
	text += "<p><img src=\"";
	text += link_crcsi;
	text += "\" alt=\"CRCSI Logo\">&nbsp;<A href=\"www.crcsi.com.au\">Cooperative Research Centre for Spatial Information</A></P>";

	QMessageBox::about(this,"About Barista",
							text.GetChar()
							);

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::PushbroomBundleClicked()
{
	 for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		CVImage* image = project.getImageAt(i);
		if (image->getCurrentSensorModelType() != ePUSHBROOM)
		{
			QMessageBox::warning( this, "  No Pushbroom Sensor Model found!" , "Load Satellite Metatdata for every Image!");
			return;
		}
    }
	CVControl* Control = project.getControlPoints();

    if (Control == NULL)
	{
		QMessageBox::warning( this, "  No Control Points found!", "Set Control Points first!");
	}
	else
	{
		CReferenceSystem refsys = Control->getCurrentSensorModel()->getRefSys();

		if ((refsys.getCoordinateType() != eGEOCENTRIC) || (!refsys.isWGS84()))
		{
			QMessageBox::warning( this, "  Control Points are not in WGS84Geocentic!",
									"Use WGS84Geocentric control points for Pushbroom Bundle!");
		}
		else
		{
			CBaristaBundleHelper helper;
			CCharString path = project.getFileName()->GetPath();
			CFileName protfile = path + CSystemUtilities::dirDelimStr + "iteration.prt";
			helper.initProtocol(protfile.GetChar());
		
			helper.bundleInit();

			if (helper.getNStations() < 1)
			{
				QMessageBox::warning( this, "  Pushbroom Bundle not possible!",
						"Pushbroom Bundle requires at least one station with Satellite Data and measured image points.");
			}
			else
			{
				bui_ProgressDlg dlg(new CRunBundleThread(&helper,false, false, false),&helper);
				dlg.exec();
				helper.closeProtocol();
		
				if (!dlg.getSuccess())
				{
					QString caption("Adjustment failure!"), text(" Adjustment did not converge");
					QMessageBox::warning( this, caption, text);

				}
				else
				{
					protfile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "adjust.prt";
					helper.initProtocol(protfile.GetChar());
		
					helper.printResults();

					CXYZPointArray* points = project.addXYZPoints("PushBroomBundle", refsys, true, false,eBUNDLERESULT);

					helper.getObjectPoints(*points);

					helper.updateResiduals();
					helper.setSensorFilenames();
					baristaProjectHelper.refreshTree();

					QString caption("Adjustment successful!"), text(helper.rmsString(" Adjustment was successful; s0: ").GetChar());
					QMessageBox::information( this, caption, text);

				}
			}

			helper.closeProtocol();
		}
	}

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::FrameCameraBundleClicked()
{
/*
	 for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		CVImage* image = project.getImageAt(i);
		if (image->getCurrentSensorModelType() != eFRAMECAMERA)
		{
			QMessageBox::warning( this, "  No Frame camera Model found!" , "Load camera data for every Image!");
			return;
		}
    }
*/
	CVControl* Control = project.getControlPoints();

    if (Control == NULL)
	{
		QMessageBox::warning( this, "  No Control Points found!", "Set Control Points first!");
	}
	else
	{
		CReferenceSystem refsys = Control->getCurrentSensorModel()->getRefSys();

		if (refsys.getCoordinateType() == eGEOGRAPHIC)
		{
			QMessageBox::warning( this, "  Control Points are in geographic coordinates!",
									"Use grid or geocentric control points for Camera Bundle!");
		}
		else
		{
			CBaristaBundleHelper helper;
			CCharString path = project.getFileName()->GetPath();
			CFileName protfile = path + CSystemUtilities::dirDelimStr + "iteration.prt";
			helper.initProtocol(protfile.GetChar());

			helper.bundleInit();

			if (helper.getNStations() < 1)
			{
				QMessageBox::warning( this, "  Frame Camera Bundle not possible!",
						"Frame Camera Bundle requires at least one station with Camera data and measured image points.");
			}
			else
			{
				bui_ProgressDlg dlg(new CRunBundleThread(&helper, false,false, false),&helper);
				dlg.exec();


				if (!dlg.getSuccess())
				{
					QString caption("Adjustment failure!"), text(" Adjustment did not converge");
					QMessageBox::warning( this, caption, text);
				}
				else
				{
					for (int j = 0; j < helper.getNStations(); ++j)
					{
						const CObservationHandler *oh = helper.getObservationHandler(j);
						if (oh != Control)
						{
							CSensorModel *mod = oh->getCurrentSensorModel();
							mod->setRefSys(refsys);
						}
					}

					helper.closeProtocol();
					protfile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "adjust.prt";
					helper.initProtocol(protfile.GetChar());

					helper.printResults();

					CXYZPointArray* points = project.addXYZPoints("FrameCameraBundle", refsys, true, false,eBUNDLERESULT);

					helper.getObjectPoints(*points);

					helper.updateResiduals();
					helper.setSensorFilenames();
					baristaProjectHelper.refreshTree();

					QString caption("Adjustment successful!"), text(helper.rmsString(" Adjustment was successful; s0: ").GetChar());
					QMessageBox::information( this, caption, text);

				}
			}
		
			helper.closeProtocol();		
		}
	}

}


///////////////////////////////////////////////////////////////
void bui_MainWindow::initBaristaEmpty()
{
	project.close();
	baristaProjectHelper.closeGUI();
	project.initProject();
	this->setTitle(*project.getFileName());

	this->Adjustments->setEnabled(false);
	this->Monoplotting->setEnabled(false);
	this->fileSaveAction->setEnabled(false);
	this->fileSaveAsAction->setEnabled(false);
	this->actionClose->setEnabled(false);
	this->actionImport_ALS_data->setEnabled(false);
	this->actionImport_GIS_data->setEnabled(false);
	this->fileImport_3D_PointsAction->setEnabled(false);
	this->menuImport_Image_s->setEnabled(false);
	this->menuImport_DEM->setEnabled(false);
	this->menuSCP_DB->setEnabled(false);
	this->MonoplotPointsAction->setEnabled(false);

	this->MonoplotLinesButton->setEnabled(false);
	this->MonoplottingLinesAction->setEnabled(false);
	this->MonoplottingSnakeLinesAction->setEnabled(false);
	
	this->MonoplotBuildingsAction->setEnabled(false);
	this->MeasureHeightsAction->setEnabled(false);
	
	this->RegionGrowingAction->setEnabled(false);
	this->actionBuilding_Detection->setEnabled(false);

	this->Window->setEnabled(false);
	this->ZoomOutAction->setEnabled(false);
	this->ZoomInAction->setEnabled(false);
	this->PanAction->setEnabled(false);
	this->menuImage_Processing->setEnabled(false);
	this->menuDEM_Processing->setEnabled(false);
	this->menuFeature_Extraction->setEnabled(false);

	this->DigitizeButtons->setEnabled(false);
	this->DigitizeAction->setEnabled(false);
	this->MeasureAction->setEnabled(false);
	this->LSMAction->setEnabled(false);

	//GCP chips
	this->SCPcollectionAction->setEnabled(false);
	this->SaveSCPDatabaseAction->setEnabled(false);
	this->CloseSCPDatabaseAction->setEnabled(false);

	this->menuSettings->setEnabled(false);
	this->ProjectSettingsAction->setEnabled(false);
	this->MatchingParametersAction->setEnabled(false);

	this->geoLinkAction->setEnabled(false);
	this->Barista3DViewAction->setEnabled(false);
	this->actionMergeALOSImages->setEnabled(false);


	// EventHandler for BaristaViews
	currentBaristaViewHandler = &this->viewingHandler;
	currentBaristaViewHandler->deactivateGeoLinkMode();
	currentBaristaViewHandler->open();

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::initBarista()
{
	this->setTitle(*project.getFileName());

	this->Adjustments->setEnabled(true);
	this->Monoplotting->setEnabled(true);
	this->fileSaveAction->setEnabled(true);
	this->fileSaveAsAction->setEnabled(true);
	this->actionClose->setEnabled(true);
	this->actionImport_ALS_data->setEnabled(true);
	this->actionImport_GIS_data->setEnabled(true);
	this->menuSCP_DB->setEnabled(true);

	this->fileImport_3D_PointsAction->setEnabled(true);
	this->menuImport_Image_s->setEnabled(true);
	this->menuImport_DEM->setEnabled(true);

	this->MonoplotPointsAction->setEnabled(true);

	this->MonoplotLinesButton->setEnabled(true);
	this->MonoplottingLinesAction->setEnabled(true);
	this->MonoplottingSnakeLinesAction->setEnabled(true);

	this->MonoplotBuildingsAction->setEnabled(true);
	this->MeasureHeightsAction->setEnabled(true);

#ifndef FINAL_RELEASE
	this->RegionGrowingAction->setEnabled(true);
#endif
	this->actionBuilding_Detection->setEnabled(true);	

	this->Window->setEnabled(true);
	this->ZoomOutAction->setEnabled(true);
	this->ZoomInAction->setEnabled(true);
	this->PanAction->setEnabled(true);
	this->menuImage_Processing->setEnabled(true);
	this->menuDEM_Processing->setEnabled(true);
	this->menuFeature_Extraction->setEnabled(true);

	this->DigitizeButtons->setEnabled(true);
	this->DigitizeAction->setEnabled(true);
	this->MeasureAction->setEnabled(true);
	this->LSMAction->setEnabled(true);
	this->SCPcollectionAction->setEnabled(true);

	this->menuSettings->setEnabled(true);
	this->ProjectSettingsAction->setEnabled(true);
	this->MatchingParametersAction->setEnabled(true);

	this->geoLinkAction->setEnabled(true);
	this->Barista3DViewAction->setEnabled(true);
	this->actionMergeALOSImages->setEnabled(true);

#ifdef FINAL_RELEASE
	this->RegionGrowingAction->setEnabled( false);
	this->actionImport_GIS_data->setEnabled(false);
	this->Barista3DViewAction->setEnabled(false);
	this->actionGenerate_DEM_by_Image_Matching->setEnabled(false);
#endif

	// EventHandler for BaristaViews
	currentBaristaViewHandler = &this->viewingHandler;
	currentBaristaViewHandler->open();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::validateBarista()
{
//#ifdef WIN32
//	CHardwareDongle dongle;
//
//	ValidateType valid = dongle.validateBarista();
//
//	if (valid == eDongleOK || valid == eDongleFailure)
//	{
//		if (this->dongleTimer)
//			this->dongleTimer->stop();
//
//
//		while (dongle.validateBarista() != eDongleOK)
//		{
//			int button = QMessageBox::critical(this,"Barista Dongle Version",dongle.getErrorMessage(),QMessageBox::Retry , QMessageBox::Cancel);
//
//			if (button == QMessageBox::Cancel)
//				exit(0);
//		}
//
//
//		if (this->dongleTimer)
//			this->dongleTimer->start(60000);
//		else
//			this->initTimer();
//	}
//	else if (valid == eTrialOK)
//	{
//		QMessageBox::information(this,"Barista Trial Version",dongle.getErrorMessage());
//	}
//	else if (valid == eTrialFailure)
//	{
//		QMessageBox::critical(this,"Barista Trial Version",dongle.getErrorMessage());
//		exit(0);
//	}
//	else if (valid == eFullVersion)
//	{
//	}
//	else
//	{
//		QMessageBox::critical(this,"Barista Version Error","Unknown Barista Version");
//		exit(0);
//	}
//#endif
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::initTimer()
{
	if (this->dongleTimer)
		delete this->dongleTimer;

	this->dongleTimer = new QTimer(this);
	this->connect( this->dongleTimer, SIGNAL(timeout()), this, SLOT(dongleCheck()) );
	this->dongleTimer->start(60000);
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::ForwardIntersection()
{
	CBundleManager helper;
	CCharString path = project.getFileName()->GetPath();
	CFileName protfile = path + CSystemUtilities::dirDelimStr + "forward.prt";
	helper.initProtocol(protfile.GetChar());
		
	bool isOK = true;
	eImageSensorModel sensorModel = eUNDEFINEDSENSOR;

	for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		CVImage *pImg = project.getImageAt(i);
		eImageSensorModel currSensorModel = pImg-> getCurrentSensorModelType();
		if ((pImg->getXYPoints()->GetSize() > 0) && (currSensorModel != eUNDEFINEDSENSOR) &&
			(currSensorModel != eTFW))
		{
			if (helper.getNStations() < 1) sensorModel = currSensorModel;
			else if (sensorModel != currSensorModel) isOK = false;

			helper.addObservations(pImg);
		}
    }


	if ((helper.getNStations() > 1) && (isOK)) helper.bundleInit(true,false);

	if ((helper.getNStations() < 2) || (!isOK))
	{
		if (helper.getNStations() < 2)
			QMessageBox::warning( this, "  Forward Intersection not possible!",
						"Forward Intersection requires at least two station with a current sensor model and measured image points.");
		else
			QMessageBox::warning( this, "  Forward Intersection not possible!",
						"Forward Intersection requires all images to have the same type of their current sensor model.");
	}
	else
	{
		CReferenceSystem refsys = helper.getObservationHandler(0)->getCurrentSensorModel()->getRefSys();

		bool maybe = helper.solve(true, false, false);

		if (!maybe)
		{
			QString caption("Adjustment failure!"), text(" Adjustment did not converge");
			QMessageBox::warning( this, caption, text);
		}
		else
		{
			helper.printResults();

			CXYZPointArray* points = project.addXYZPoints("Forward Intersection", refsys, true, false,eBUNDLERESULT);

			helper.getObjectPoints(*points);

			helper.updateResiduals(true);
			baristaProjectHelper.refreshTree();

			QString caption("Adjustment successful!"), text(helper.rmsString(" Adjustment was successful; s0: ").GetChar());
			QMessageBox::information( this, caption, text);
		}
	}

	helper.closeProtocol();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::MergeImageBands()
{
	if ( project.getNumberOfImages() == 0 )
	{
		QMessageBox::critical(this,"No Images in current project","Merging of images not possible!",1,0,0);
		return;
	}

	Bui_BandMergeDlg dlg;
	dlg.exec();

	if (!dlg.getSuccess())	// false if user has pressed "Cancel"
		return;

	if (!dlg.createMergedImage())	// actual merging failed
		QMessageBox::critical(this,"Merging of images - Error","Barista couldn't merge images!",1,0,0);

	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::setProjectSettings()
{
	Bui_ProjectSettingsDlg dlg;

	dlg.exec();
}

///////////////////////////////////////////////////////////////
bool  bui_MainWindow::AskForHeightMonoplotting()
{
	int yesno = QMessageBox::question(this,
	"Project has no DEM available",
	"Use mean project height for Monoplotting?",
	"&Yes", "&No",	0, 1  );
    
	if ( yesno == 0 )
	{
		Bui_ProjectSettingsDlg dlg;
		dlg.exec();

		if ( project.getMeanHeight() != -9999.0) project.setMeanHeightMonoplotting(true);
		else
			project.setMeanHeightMonoplotting(false);
	}
	else
		project.setMeanHeightMonoplotting(false);

	return project.getMeanHeightMonoplotting();

};

///////////////////////////////////////////////////////////////
void bui_MainWindow::MergeDEM()
{
	if ( project.getNumberOfDEMs() == 0 )
	{
		QMessageBox::warning( this, "No DEM available in this project!",
								 "DEM merging is not possible!");
		return;
	}
	else if ( project.getNumberOfDEMs() == 1 )
	{
		QMessageBox::warning( this, "Project contains only one DEM",
								 "DEM merging requires at least two DEMs!");
		return;
	}
	else
	{
		Bui_DEMMergeDlg dlg;

		dlg.exec();

		if (!dlg.getSuccess())	// false if user has pressed "Cancel"
			return;

		if (!dlg.createMergedDEM())	// actual merging failed
			QMessageBox::critical(this,"Merging of DEMs - Error","Barista couldn't merge DEMs!",1,0,0);

		baristaProjectHelper.refreshTree();
	}

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::DifferenceDEM()
{
	if ( project.getNumberOfDEMs() == 0 )
	{
		QMessageBox::warning( this, "No DEM available in this project!",
								 "DEM differencing is not possible!");
		return;
	}
	else if ( project.getNumberOfDEMs() == 1 )
	{
		QMessageBox::warning( this, "Project contains only one DEM",
								 "DEM differencing requires at least two DEMs!");
		return;
	}
	else
	{
		Bui_DEMDifferenceDlg dlg;

		dlg.exec();

		if (!dlg.getSuccess())	// false if user has pressed "Cancel"
			return;

		if (!dlg.createDifferenceDEM())	// actual differencing failed
			QMessageBox::critical(this,"Differencing of DEMs - Error","Barista couldn't difference DEMs!",1,0,0);

		baristaProjectHelper.refreshTree();
	}

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::importDEM()
{

	Bui_RAWImportDlg dlg(true);
	dlg.exec();
	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::AdjustmentBundleClicked()
{
	Bui_AdjustDialog dlg(this);
	dlg.exec();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::importImage()
{

	Bui_RAWImportDlg dlg(false);
	dlg.exec();
	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::setMatchingParameters()
{
	Bui_MatchingParametersDlg dlg;

	dlg.exec();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::geoLinkModeToggle(bool b)
{
	if (b)
		currentBaristaViewHandler->activateGeoLinkMode();
	else
		currentBaristaViewHandler->deactivateGeoLinkMode();

}

///////////////////////////////////////////////////////////////
void  bui_MainWindow::barista3DViewToggle(bool b)
{
	if (b)
	{
		C3DView* view3D = new C3DView(workspace);

		QMdiSubWindow *widget = workspace->addSubWindow(view3D);
		view3D->setAttribute(Qt::WA_DeleteOnClose);
		view3D->setUpdatesEnabled(true);
		if (!view3D->isVisible()) view3D->show();
		else view3D->activateWindow();

		workspace->tileSubWindows();
	}

}

///////////////////////////////////////////////////////////////
void bui_MainWindow::importGISData()
{

	bui_ImportGISDialog dlg;
	dlg.exec();

	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::mergeALOSImages()
{

	Bui_ImageMergeDlg dlg(this);
	dlg.exec();


	if (dlg.getSuccess())
		baristaProjectHelper.refreshTree();

	return;
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::startCameraMountingHandler(CVImage* vimage)
{
	if (this->cameraMountingHandler.setVImage(vimage))
	{
		currentBaristaViewHandler->close();
		currentBaristaViewHandler = &this->cameraMountingHandler;
		currentBaristaViewHandler->open();
		
	}
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::endCameraMountingHandler()
{
	if (currentBaristaViewHandler == &this->cameraMountingHandler)
	{	
		currentBaristaViewHandler->close();
		currentBaristaViewHandler = &this->viewingHandler;
		currentBaristaViewHandler->open();
	}
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::computeADS40StaggeredImage()
{
	CHugeImage* img1 = project.getImageAt(0)->getHugeImage();
	CHugeImage* img2 = project.getImageAt(1)->getHugeImage();

	CFileName newFileName = img1->getFileName()->GetPath();
	newFileName += CSystemUtilities::dirDelimStr + "staggeredImage.hug";
	CVImage* newImage = project.addImage(newFileName.GetChar(), false);
	
	CHugeImage* hImage = newImage->getHugeImage();

	bool ret = hImage->prepareWriting(newFileName.GetChar(),img1->getWidth()*2,img1->getHeight(),img1->getBpp(),img1->getBands());

	unsigned short pix1,pix2,pix3,pix4;
	unsigned int newPix;
	

	int offset = 3;

	for (int rowImage=offset; rowImage < hImage->getHeight()-1; rowImage+=hImage->getTileHeight())
	{
		for (int colImage=offset; colImage < hImage->getWidth()-1; colImage+=hImage->getTileWidth())
		{
			CImageBuffer srcBuf1(colImage/2-offset,rowImage-offset,hImage->getTileWidth()/2+ 2*offset,hImage->getTileHeight() + 2*offset,hImage->getBands(),hImage->getBpp(),true);			
			CImageBuffer srcBuf2(colImage/2-offset,rowImage-offset,hImage->getTileWidth()/2+ 2*offset,hImage->getTileHeight() + 2*offset,hImage->getBands(),hImage->getBpp(),true);						
			
			img1->getRect(srcBuf1);
			img2->getRect(srcBuf2);
			
			CImageBuffer destBuf(0,0,hImage->getTileWidth(),hImage->getTileHeight(),hImage->getBands(),hImage->getBpp(),true);
			
			
			for (int rowTile = 0; rowTile < hImage->getTileHeight(); rowTile++)
			{
				for (int colTile = 0; colTile < hImage->getTileWidth(); colTile++)
				{
					int r = offset + rowTile;
					int c = offset + colTile/2;
					C2DPoint p(r + 0.25,c + 0.25);
					srcBuf1.getInterpolatedColourVecBilin(p,&pix1);
					
					p.x = c + 0.25;
					p.y = r + 0.75;
					srcBuf1.getInterpolatedColourVecBilin(p,&pix2);

					p.x = c - 0.25;
					p.y = r + 0.25;
					srcBuf2.getInterpolatedColourVecBilin(p,&pix3);

					p.x = c - 0.25;
					p.y = r + 0.75;
					srcBuf2.getInterpolatedColourVecBilin(p,&pix4);

					newPix = (pix1 + pix2 + pix3 + pix4)/4.0;
					destBuf.pixShort(rowTile*destBuf.getWidth()+colTile) = (unsigned short) newPix;
				}
			}
			
			hImage->dumpTile(destBuf);

		}
	}



/*
	for (int rowImage=0; rowImage < hImage->getHeight()-1; rowImage+=hImage->getTileHeight())
	{
		for (int colImage=0; colImage < hImage->getWidth()-1; colImage+=hImage->getTileWidth())
		{
			CImageBuffer destBuf(0,0,hImage->getTileWidth(),hImage->getTileHeight(),hImage->getBands(),hImage->getBpp(),true);
			for (int rowTile = 0; rowTile < hImage->getTileHeight(); rowTile++)
			{
				for (int colTile = 0; colTile < hImage->getTileWidth(); colTile++)
				{
					int r = rowImage+rowTile;
					int c = (colImage+colTile)/2;

					img1->getRect((unsigned char*)&pix1,r  ,1,c,1);
					img1->getRect((unsigned char*)&pix2,r+1,1,c,1);
					img2->getRect((unsigned char*)&pix3,r  ,1,c,1);
					img2->getRect((unsigned char*)&pix4,r+1,1,c,1);
					newPix = (pix1 + pix2 + pix3 + pix4)/4.0;
					destBuf.pixShort(rowTile*destBuf.getWidth()+colTile) = (unsigned short) newPix;
				}
			}
			
			hImage->dumpTile(destBuf);

		}
	}
*/


	ret = hImage->finishWriting();
	
	baristaProjectHelper.refreshTree();
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::mergeALOSBands()
{
	
	Bui_AVNIRBandMergeDlg dlg;

	CBandMerger merger;
/*
	vector<vector<CVImage*>> inputImages;
	vector<CCharString> names;
	merger.findImageGroups(inputImages,names);

	CFileName fileName = inputImages[0][0]->getHugeImage()->getFileName()->GetPath() + "/mergedImage";
	CVImage* newImage = project.addImage(fileName, false);

	merger.init(inputImages[0],names[0],CBandMerger::eBiLinear,newImage);

 	bui_ProgressDlg dlg(new CBandMergeThread(merger),&merger,"Merging AVNIR-2 bands ...");
	*/
	dlg.exec();

	if (dlg.getSuccess())
		baristaProjectHelper.refreshTree();
}


///////////////////////////////////////////////////////////////
void bui_MainWindow::loadGCPdatabase()
{	// open and read the ASCII database files containing a list of GCP and associated information
	// for each GCP (3D points), there would be a list of 2D observed points, each with an image chip
	// that means for each 2D observed point, we need to load associated image chips 

	ImageChipsMatchingDlg mdlg;
	mdlg.exec();

	/*
	vector <CGCPDataBasePoint> gcpDB;
	CGCPDataBasePoint gcpPoint;
	CGCPData2DPoint gcp2DPoint;
	//ANNidxArray nnIdx; // array of indices of selected GCP Points from gcpDB;

	// for test purposes we set the following GCP list
	
	//GCP 1
	gcpPoint.setLabel("GCP1");
	gcpPoint.x = 713006.2500;
	gcpPoint.y = 6174173.1250;
	gcpPoint.z = 792.7322;
	gcpPoint.setCoordinateType(eGRID);
	gcpPoint.setDate("12 Sep. 2008");
	gcpPoint.setFromGPS(true);
	gcpPoint.setSX(1.0);
	gcpPoint.setSY(1.0);
	gcpPoint.setSZ(1.0);
	// 2D observed point of GCP1
	gcp2DPoint.x = 103.25;
	gcp2DPoint.y = 94;
	gcp2DPoint.setDate("12 Sep. 2008");
	gcp2DPoint.setOrthoImageFileName("I:\\ALOS-GA\\chip001.tif");
	gcp2DPoint.setLabel("GCP1");
	gcp2DPoint.setSX(0.5);
	gcp2DPoint.setSY(0.5);
	// add above 2D point to gcpPoint
	gcpPoint.set2DPoint(gcp2DPoint);
	// add above GCP to the Database
	gcpDB.push_back(gcpPoint);


	//GCP 2
	gcpPoint.setLabel("GCP2");
	gcpPoint.x = 726523.7500;
	gcpPoint.y = 6199703.7500;
	gcpPoint.z = 823.3038;
	gcpPoint.setCoordinateType(eGRID);
	gcpPoint.setDate("12 Sep. 2008");
	gcpPoint.setFromGPS(true);
	gcpPoint.setSX(1.0);
	gcpPoint.setSY(1.0);
	gcpPoint.setSZ(1.0);
	// 2D observed point of GCP1
	gcp2DPoint.x = 49.00;
	gcp2DPoint.y = 42.25;
	gcp2DPoint.setDate("12 Sep. 2008");
	gcp2DPoint.setOrthoImageFileName("I:\\ALOS-GA\\chip002.tif");
	gcp2DPoint.setLabel("GCP2");
	gcp2DPoint.setSX(0.5);
	gcp2DPoint.setSY(0.5);
	// add above 2D point to gcpPoint
	gcpPoint.set2DPoint(gcp2DPoint);
	// add above GCP to the Database
	gcpDB.push_back(gcpPoint);

	//GCP 3
	gcpPoint.setLabel("GCP3");
	gcpPoint.x = 726243.1250;
	gcpPoint.y = 6198926.2500;
	gcpPoint.z = 813.1219;
	gcpPoint.setCoordinateType(eGRID);
	gcpPoint.setDate("12 Sep. 2008");
	gcpPoint.setFromGPS(true);
	gcpPoint.setSX(1.0);
	gcpPoint.setSY(1.0);
	gcpPoint.setSZ(1.0);
	// 2D observed point of GCP1
	gcp2DPoint.x = 29.25;
	gcp2DPoint.y = 25.50;
	gcp2DPoint.setDate("12 Sep. 2008");
	gcp2DPoint.setOrthoImageFileName("I:\\ALOS-GA\\chip003.tif");
	gcp2DPoint.setLabel("GCP3");
	gcp2DPoint.setSX(0.5);
	gcp2DPoint.setSY(0.5);
	// add above 2D point to gcpPoint
	gcpPoint.set2DPoint(gcp2DPoint);
	// add above GCP to the Database
	gcpDB.push_back(gcpPoint);

*/
	// select GCP/check Points
	//CXYZPointArray* ctrlPoints = project.getCheckPoints();
	
	//selectGCPPoints(gcpDB, nnIdx);
	//int *index = project.selectGCPPoints(ctrlPoints);  // select GCP within the large quadrangular strip
	// index has 1 for selected points, 0 for unselected points

	if (mdlg.getSuccess())
	{
		CMatchingParameters pars = project.getMatchingParameter();

		CReferenceSystem refSys;
		if (project.getActiveDEM())
			refSys = project.getActiveDEM()->getReferenceSystem();
		//else // user defined Reference System
		//	CReferenceSystem refSys;
		refSys.setCoordinateType(eGEOCENTRIC);

		//bui_ProgressDlg dlg(new CGCPPointMatchingThread(project, gcpDB, pars, refSys), &project, "Matching GCP Database");
		bui_ProgressDlg dlg(new CGCPPointMatchingThread(project, *(project.getGCPdb()), pars, refSys), &project, "Matching GCP Database");
		//project.matchGCPDataBasePoints(gcpDB,project.getMatchingParameter());
		dlg.exec();
	
		if (!dlg.getSuccess())
		{
			QMessageBox::warning(this,"Matching GCP Database", "GCP Point matching failed...!");
		}
	}
	baristaProjectHelper.refreshTree();
	//int here = 1;
}

///////////////////////////////////////////////////////////////
void bui_MainWindow::imageShow()
{
	Bui_ImgShowDlg dlg;
	dlg.exec();	
}

//void bui_MainWindow::contentShow()
//{
//	bui_ChannelSwitchBox dlg(this);
//	dlg.exec();	
//}

