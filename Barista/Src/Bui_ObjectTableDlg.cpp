#include "Bui_ObjectTableDlg.h"
#include "ObjectSelectorTable.h"
#include <QGridLayout>
#include "IniFile.h"

Bui_ObjectTableDlg::Bui_ObjectTableDlg(CObjectSelectorTable* table,QWidget *parent)
	: QDialog(parent),table(table)
{
	this->setupUi(this);
	this->setAttribute(Qt::WA_DeleteOnClose,true);
	this->setAccessibleName(table->accessibleName());
	QGridLayout* layout = new QGridLayout();
	layout->addWidget(table);
	this->setLayout(layout);
	this->setWindowTitle("Object Selector");
	this->connect(table,SIGNAL(nameChanged(QString)),this,SLOT(nameChanged(QString)));

	int width, height;
	inifile.getSizeOfObjectSelectorDlg(width,height);

	if (width != -1 && height != -1)
	{
		this->resize(width,height);
	}
}

Bui_ObjectTableDlg::~Bui_ObjectTableDlg()
{
	QSize size = this->size();

	inifile.setSizeOfObjectSelectorDlg(size.width(),size.height());
	inifile.writeIniFile();
}

void Bui_ObjectTableDlg::nameChanged(QString name)
{
	this->setAccessibleName(name);
}