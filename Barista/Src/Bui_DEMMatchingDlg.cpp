#include <float.h> 

#include <QDoubleValidator>
#include <QTableWidgetItem>
#include <QMessageBox> 
#include <QHeaderView>
#include <QFileDialog>

#include "Bui_DEMMatchingDlg.h"
#include "Bui_FeatureExtractionDialog.h"
#include "ProgressThread.h"

#include "SystemUtilities.h"
#include "BaristaProject.h"
#include "Icons.h"
#include "DEMMatcher.h"
#include "Bui_ProgressDlg.h"
#include "Bui_ReferenceSysDlg.h"
#include "Trans3DFactory.h"
#include "ProtocolHandler.h"
#include "XYZPolyLineArray.h"

Bui_DEMMatchingDlg::Bui_DEMMatchingDlg(QWidget *parent): QDialog(parent), 
    decimals(3), llPoint(0,0),urPoint(1,1), gridSize(1,1), success (false),
	matcher (0), pRefSys(0), InterpolationMethod(eMovingHzPlane),
	maxDistInterpol(-1.0), neighbourhoodSize(9), pOuterPolyLine(0)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->AvailableImageList.clear();

	for (int i = 0; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		if (img->getCurrentSensorModel())
		{
			this->AvailableImageList.push_back(img);
		}
	}

	this->setupImageSelector();
	this->setDEMExtents();
	this->minZ = project.getMatchingParameter().getMinZ();
	this->maxZ = project.getMatchingParameter().getMaxZ();

	CFileName parameterFile("SGMPars.ini");
	parameterFile.SetPath(project.getFileName()->GetPath());
	this->SGMpars.initFromFile(parameterFile);
	
	parameterFile = "DEMMatchPars.ini"; 
	parameterFile.SetPath(project.getFileName()->GetPath());
	bool isLoaded = this->initFromFile(parameterFile);

	this->setParsToDlg();


	//this->resize(0,0);
	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
	this->resize(0,0);

	// pushbutton connections
	this->connect(this->buttonCancel, SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->buttonOk,     SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->HelpButton,   SIGNAL(released()),this,SLOT(pBHelpReleased()));

	// slider connections
	this->connect(this->preFilterCapSlider,      SIGNAL(valueChanged(int)),this,SLOT(preFilterCapSliderChanged(int)));
	this->connect(this->speckleWindowSizeSlider, SIGNAL(valueChanged(int)),this,SLOT(speckleWindowSizeChanged(int)));
	this->connect(this->speckleRangeSlider,      SIGNAL(valueChanged(int)),this,SLOT(speckleRangeChanged(int)));

	// lineEdits connections
	this->connect(this->P1Edit,             SIGNAL(editingFinished ()),this,SLOT(P1EditingFinished()));
	this->connect(this->P2Edit,             SIGNAL(editingFinished ()),this,SLOT(P2EditingFinished()));
	this->connect(this->le_disp12MaxDiff,   SIGNAL(editingFinished ()),this,SLOT(disp12MaxDiffEditingFinished()));
	this->connect(this->le_UnitquenessEdit, SIGNAL(editingFinished ()),this,SLOT(onUnitquenessEditingFinished()));
	this->connect(this->SADWindowSizeEdit,  SIGNAL(editingFinished ()),this,SLOT(onSADWindowEditingFinished()));
	this->connect(this->le_maxZ,            SIGNAL(editingFinished ()),this,SLOT(leMaxZEditingFinished()));
	this->connect(this->le_minZ,            SIGNAL(editingFinished ()),this,SLOT(leMinZEditingFinished()));

	// validators
	this->P1Edit->setValidator(new QDoubleValidator(this));
	this->P2Edit->setValidator(new QDoubleValidator(this));
	this->le_disp12MaxDiff->setValidator(new QDoubleValidator(this));
	this->le_UnitquenessEdit->setValidator(new QDoubleValidator(this));
	this->SADWindowSizeEdit->setValidator(new QDoubleValidator(this));
	this->le_maxZ->setValidator(new QDoubleValidator(this));
	this->le_minZ->setValidator(new QDoubleValidator(this));

	this->connect(orthoLLX,                SIGNAL(editingFinished()),        this, SLOT(onLLChanged()));
	this->connect(orthoLLY,                SIGNAL(editingFinished()),        this, SLOT(onLLChanged()));
	this->connect(orthoURX,                SIGNAL(editingFinished()),        this, SLOT(onURChanged()));
	this->connect(orthoURY,                SIGNAL(editingFinished()),        this, SLOT(onURChanged()));
	this->connect(orthoGResolution,        SIGNAL(editingFinished()),        this, SLOT(onGridChanged()));
	this->connect(Neighbourhood,           SIGNAL(editingFinished()),        this, SLOT(onNeighbourhoodChanged()));
	this->connect(MaximumDistance,         SIGNAL(editingFinished()),        this, SLOT(onMaxDistInterpolChanged()));
	this->connect(OuterBorderLine,         SIGNAL(editingFinished()),        this, SLOT(onOuterBorderlineChanged()));
	this->connect(InnerBorderLine,         SIGNAL(editingFinished()),        this, SLOT(onInnerBorderlineChanged()));
	this->connect(MovingTiltedPlaneButton, SIGNAL(clicked(bool)),            this, SLOT(onInterpolationMethodChanged()));
	this->connect(MovingHZButton,          SIGNAL(clicked(bool)),            this, SLOT(onInterpolationMethodChanged()));
	this->connect(BrowseButton,            SIGNAL(released()),               this, SLOT(onOutputFileChanged()));
	this->connect(this->ImageList,         SIGNAL(itemSelectionChanged()),   this, SLOT(onImageListChanged()));
	this->connect(this->SetToMaxButton,    SIGNAL(released()),               this, SLOT(onSetToMax()));
	this->connect(this->fullDPCheckBox,    SIGNAL(stateChanged(int)),        this, SLOT(onFullDPChanged()));

	this->OutputDirectory->setEnabled(false);

	this->onOutputFileChanged();
	if (!isLoaded) this->onSetToMax();
	else setDEMExtents();

	this->checkSetup();
}

//================================================================================

Bui_DEMMatchingDlg::~Bui_DEMMatchingDlg()
{
	if (this->pRefSys) delete this->pRefSys;
	this->pRefSys = 0;
	if (this->matcher) delete this->matcher;
	this->matcher = 0;

	if (this->pOuterPolyLine) delete  this->pOuterPolyLine;
	this->pOuterPolyLine = 0;

	for (unsigned int i = 0; i < this->InnerPolylineList.size(); ++i)
	{
		delete this->InnerPolylineList[i];
		this->InnerPolylineList[i] = 0;
	}
	this->InnerPolylineList.clear();
}
	  
//================================================================================

void Bui_DEMMatchingDlg::saveToFile(const CCharString &fn) const
{
	ofstream parFile(fn.GetChar());
	if (parFile.good())
	{
		parFile.setf(ios::fixed, ios::floatfield);
		parFile.precision(5);

		parFile << "llPoint("             << this->llPoint.getString(" ", 10, 3)  << ")\n"
			    << "urPoint("             << this->urPoint.getString(" ", 10, 3)  << ")\n"
			    << "gridSize("            << this->gridSize.getString(" ", 10, 3) << ")\n"
			    << "DEMFileName("         << this->DEMFileName                    << ")\n"
			    << "InterpolationMethod(" << int(InterpolationMethod)             << ")\n"
			    << "maxDistInterpol("     << this->maxDistInterpol                << ")\n"
			    << "neighbourhoodSize("   << this->neighbourhoodSize              << ")\n"
			    << "minZ("                << this->minZ                           << ")\n"
			    << "maxZ("                << this->maxZ                           << ")\n";
	};
};

//================================================================================

bool Bui_DEMMatchingDlg::initFromFile(const CCharString &fn)
{
	ifstream inFile(fn.GetChar());

	bool ret = true;

	if (inFile.good())
	{
		int len = 2048;
		char *z = new char[len];
		inFile.getline(z, len);

		while (inFile.good())
		{
			CCharString arg(z);
			int pos1 = arg.Find("(");
			int pos2 = arg.Find(")");
			if ((pos1 > 0) && (pos2 > pos1))
			{
				CCharString key = arg.Left(pos1);
				arg = arg.Left(pos2);
				arg = arg.Right(arg.GetLength() - pos1 - 1);
				if (key == "llPoint")
				{
					istrstream iss(arg.GetChar());
					iss >> this->llPoint.x;
					iss >> this->llPoint.y;
				}
				else if (key == "urPoint") 
				{
					istrstream iss(arg.GetChar());
					iss >> this->urPoint.x;
					iss >> this->urPoint.y;
				}
				else if (key == "gridSize")
				{
					istrstream iss(arg.GetChar());
					iss >> this->gridSize.x;
					iss >> this->gridSize.y;
				}
				else if (key == "DEMFileName")
				{
					this->DEMFileName = arg;
				}
				else if (key == "InterpolationMethod")
				{
					int intArg;
					istrstream iss(arg.GetChar());
					iss >> intArg;
					this->InterpolationMethod = (eInterpolationMethod) intArg;
				}
				else if (key == "maxDistInterpol")
				{
					istrstream iss(arg.GetChar());
					iss >> this->maxDistInterpol;
				}
				else if (key == "neighbourhoodSize")
				{
					istrstream iss(arg.GetChar());
					iss >> this->neighbourhoodSize;
				}
				else if (key == "minZ")
				{
					istrstream iss(arg.GetChar());
					iss >> this->minZ;
				}
				else if (key == "maxZ")
				{
					istrstream iss(arg.GetChar());
					iss >> this->maxZ;
				}
			}
			inFile.getline(z, len);
		}

		delete [] z;
	}
	else ret = false;

	return ret;
};

//================================================================================

void Bui_DEMMatchingDlg::setParsToDlg()
{
	this->preFilterCapSlider->setValue(this->SGMpars.preFilterCap);
	this->le_preFilterCap->setText(QString( "%1" ).arg(this->SGMpars.preFilterCap));

	this->speckleWindowSizeSlider->setValue(this->SGMpars.speckleWindowSize);
	this->le_speckleWindowSize->setText(QString( "%1" ).arg(this->SGMpars.speckleWindowSize));

	this->speckleRangeSlider->setValue(this->SGMpars.speckleRange);
	this->le_speckleRange->setText(QString( "%1" ).arg(this->SGMpars.speckleRange));
	
	this->P1Edit->blockSignals(true);
	this->P2Edit->blockSignals(true);
	this->le_disp12MaxDiff->blockSignals(true);
	this->le_UnitquenessEdit->blockSignals(true);
	this->SADWindowSizeEdit->blockSignals(true);
	this->le_maxZ->blockSignals(true);
	this->le_minZ->blockSignals(true);

	this->P1Edit->setText(QString( "%1" ).arg(this->SGMpars.P1));
	this->P2Edit->setText(QString( "%1" ).arg(this->SGMpars.P2));
	this->le_disp12MaxDiff->setText(QString( "%1" ).arg(this->SGMpars.disp12MaxDiff));
	this->le_UnitquenessEdit->setText(QString( "%1" ).arg(this->SGMpars.uniquenessRatio));
	this->SADWindowSizeEdit->setText(QString( "%1" ).arg(this->SGMpars.SADWindowSize));

	this->le_maxZ->setText(QString( "%1" ).arg(this->maxZ, 0, 'F', this->decimals));
	this->le_minZ->setText(QString( "%1" ).arg(this->minZ, 0, 'F', this->decimals));

	this->P1Edit->blockSignals(false);
	this->P2Edit->blockSignals(false);
	this->le_disp12MaxDiff->blockSignals(false);
	this->le_UnitquenessEdit->blockSignals(false);
	this->SADWindowSizeEdit->blockSignals(false);
	this->le_maxZ->blockSignals(false);
	this->le_minZ->blockSignals(false);

	this->MovingTiltedPlaneButton->blockSignals(true);
	this->MovingHZButton->blockSignals(true);

	if (this->InterpolationMethod == eMovingHzPlane) 
	{
		this->MovingHZButton->setChecked(true);
		this->MovingTiltedPlaneButton->setChecked(false);
	}
	else
	{
		this->MovingHZButton->setChecked(false);
		this->MovingTiltedPlaneButton->setChecked(true);
	}

	this->MovingTiltedPlaneButton->blockSignals(false);
	this->MovingHZButton->blockSignals(false);

	this->Neighbourhood->blockSignals(true);
	this->MaximumDistance->blockSignals(true);

	QString dt=QString("%1").arg(neighbourhoodSize);
	this->Neighbourhood->setText(dt);
	dt=QString("%1").arg(maxDistInterpol, 0, 'f', 1);
	this->MaximumDistance->setText(dt);
	  
	this->Neighbourhood->blockSignals(false);
	this->MaximumDistance->blockSignals(false);

	this->fullDPCheckBox->blockSignals(true);
	this->fullDPCheckBox->setChecked(this->SGMpars.fullDP);
	this->fullDPCheckBox->blockSignals(false);
}

//================================================================================

void Bui_DEMMatchingDlg::setupImageSelector()
{
	this->SelectedImageList.clear();

	this->ImageList->blockSignals(true);
	this->ImageList->setColumnCount(1);
	this->ImageList->setRowCount(this->AvailableImageList.size());
	this->ImageList->setHorizontalHeaderItem(0,new QTableWidgetItem("Select images"));
	this->ImageList->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

	for (unsigned int i = 0; i < this->AvailableImageList.size(); ++i)
	{
		QTableWidgetItem *it = new QTableWidgetItem(AvailableImageList[i]->getFileNameChar());
		//it->setData(Qt::EditRole,i);
		this->ImageList->setItem(i,0,it);					
		this->SelectedImageList.push_back(AvailableImageList[i]);
	}

	this->ImageList->selectAll();

	this->ImageList->blockSignals(false);

	this->checkSetup();
};

//================================================================================

void Bui_DEMMatchingDlg::pBCancelReleased()
{
	this->close();
}

//================================================================================

void Bui_DEMMatchingDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/MenuSettings.html");
}

//================================================================================

void Bui_DEMMatchingDlg::pBOkReleased()
{
	CFileName parameterFile("SGMPars.ini");
	parameterFile.SetPath(project.getFileName()->GetPath());
	this->SGMpars.saveToFile(parameterFile);

	parameterFile = "DEMMatchPars.ini"; 
	parameterFile.SetPath(project.getFileName()->GetPath());
	this->saveToFile(parameterFile);
	
	CFileName fn(DEMFileName);

	fn += ".hug";

	this->DEM = project.addDEM(this->DEMFileName);
	if (this->DEM)
	{
		this->DEM->getDEM()->initialize(fn.GetChar(), this->llPoint, this->urPoint, this->gridSize);
		this->DEM->setReferenceSystem(*pRefSys);
		vector<CXYZPolyLine *> *pvec = 0;
		if (this->InnerPolylineList.size() > 0) pvec = &this->InnerPolylineList;

		this->matcher = new CDEMMatcher(this->SelectedImageList, this->DEM,  this->SGMpars, 
			                            this->minZ, this->maxZ,  this->InterpolationMethod, 
										this->maxDistInterpol,   this->neighbourhoodSize, 
										this->pOuterPolyLine,    pvec);

		bui_ProgressDlg dlg(new CDEMProcessingThread(this), this->matcher, "DEM Matching");
		dlg.exec();
		this->success = dlg.getSuccess();

		delete this->matcher;
		this->matcher = 0;
	}

	if (!this->success && this->DEM) project.deleteDEM(this->DEM);

	this->close();
}

//================================================================================

void Bui_DEMMatchingDlg::preFilterCapSliderChanged(int preFilterCap )
{
	if (preFilterCap <= 0) preFilterCap = 1;
	this->SGMpars.preFilterCap = preFilterCap; 

	this->setParsToDlg();
}

//================================================================================
	

void Bui_DEMMatchingDlg::speckleWindowSizeChanged(int size )
{	
	if (size < 0) size = 0;
	this->SGMpars.speckleWindowSize = size;
	if (this->SGMpars.speckleRange < 1) this->SGMpars.speckleRange  = 1;

	this->setParsToDlg();
}

//================================================================================

void Bui_DEMMatchingDlg::speckleRangeChanged(int range )
{
	if (range < this->SGMpars.speckleRange) this->SGMpars.speckleRange -= 16;
	else if (range > this->SGMpars.speckleRange) 
	{
		if (this->SGMpars.speckleRange  == 1) this->SGMpars.speckleRange  = 16;
		else this->SGMpars.speckleRange += 16;
	}

	if (this->SGMpars.speckleRange < 16) this->SGMpars.speckleRange  = 1;
	
	
	this->setParsToDlg();
}

//================================================================================

void Bui_DEMMatchingDlg::onSADWindowEditingFinished()
{
	if ( this->SADWindowSizeEdit->hasAcceptableInput() )
	{
		int val = this->SADWindowSizeEdit->text().toInt();

		if (val < this->SGMpars.SADWindowSize) 
		{
			if (!(val % 2)) val--;
			if (val < 1) val = 1;
		}
		else if (val > this->SGMpars.SADWindowSize) 
		{
			if (!(val % 2)) val++;
		}

		this->SGMpars.SADWindowSize = val;
	}

	this->setParsToDlg();
}

//================================================================================

void Bui_DEMMatchingDlg::P1EditingFinished()
{
	if ( this->P1Edit->hasAcceptableInput() )
	{
		int val = this->P1Edit->text().toInt();

		if (val < 0) val = 0;
		
		this->SGMpars.P1 = val;

		if (this->SGMpars.P2 < this->SGMpars.P1) this->SGMpars.P2 = this->SGMpars.P1 + 1;
	}

	this->setParsToDlg();
}

//================================================================================

void Bui_DEMMatchingDlg::P2EditingFinished()
{
	if ( this->P2Edit->hasAcceptableInput() )
	{
		int val = this->P2Edit->text().toInt();

		if (val < 1) val = 1;
		
		this->SGMpars.P2 = val;

		if (this->SGMpars.P2 < this->SGMpars.P1) this->SGMpars.P1 = this->SGMpars.P2 - 1;
	}

	this->setParsToDlg();
};
		
//================================================================================

void Bui_DEMMatchingDlg::disp12MaxDiffEditingFinished()
{
	if ( this->le_disp12MaxDiff->hasAcceptableInput() )
	{
		int val = this->le_disp12MaxDiff->text().toInt();

		if ( val < 0 ) val = -1;

		this->SGMpars.disp12MaxDiff = val;
	}

	this->setParsToDlg();
}

//================================================================================

void Bui_DEMMatchingDlg::onUnitquenessEditingFinished()
{
	if (this->le_UnitquenessEdit->hasAcceptableInput() )
	{
		int val = this->le_UnitquenessEdit->text().toInt();

		if ( val <   0 ) val =   0;
		if ( val > 100 ) val = 100;

		this->SGMpars.uniquenessRatio = val;
	}
	this->setParsToDlg();
}

//================================================================================

void Bui_DEMMatchingDlg::leMaxZEditingFinished()
{
	if ( this->le_maxZ->hasAcceptableInput() )
	{
		this->maxZ = this->le_maxZ->text().toDouble();
	}

	this->setParsToDlg();
}

//================================================================================

void Bui_DEMMatchingDlg::leMinZEditingFinished()
{
	if ( this->le_minZ->hasAcceptableInput() )
	{
		this->minZ =this->le_minZ->text().toDouble();
	}

	this->setParsToDlg();
}

//================================================================================

void Bui_DEMMatchingDlg::onImageListChanged()
{
	this->SelectedImageList.clear();
	this->ImageList->blockSignals(true);

	QList<QTableWidgetItem *> list = this->ImageList->selectedItems();
	for (int i = 0; i < list.count(); i++)
	{
		unsigned int index = list[i]->row();
		if ( index < this->AvailableImageList.size())
		{
			CVImage *img = this->AvailableImageList[index];
			this->SelectedImageList.push_back(img);
		}
	}


	this->ImageList->blockSignals(false);

	this->checkSetup();
};

//================================================================================
	 
void Bui_DEMMatchingDlg::setDEMExtents()
{
	this->orthoLLX->blockSignals(true);
	this->orthoLLY->blockSignals(true);
	this->orthoURX->blockSignals(true);
	this->orthoURY->blockSignals(true);
	this->orthoGResolution->blockSignals(true);

	QString ds = QString("%1").arg(this->llPoint.x, 0, 'f', 3);
	this->orthoLLX->setText(ds);
	ds = QString("%1").arg(this->llPoint.y, 0, 'f', 3);
	this->orthoLLY->setText(ds);

	ds = QString("%1").arg(this->urPoint.x, 0, 'f', 3);
	this->orthoURX->setText(ds);
	ds = QString("%1").arg(this->urPoint.y, 0, 'f', 3);
	this->orthoURY->setText(ds);

	ds = QString("%1").arg(this->gridSize.x, 0, 'f', 3);
	this->orthoGResolution->setText(ds);

	this->orthoLLX->blockSignals(false);
	this->orthoLLY->blockSignals(false);
	this->orthoURX->blockSignals(false);
	this->orthoURY->blockSignals(false);
	this->orthoGResolution->blockSignals(false);
};

//================================================================================
	 
void Bui_DEMMatchingDlg::onInterpolationMethodChanged()
{
	this->MovingHZButton->blockSignals(true);
	this->MovingTiltedPlaneButton->blockSignals(true);

	if (this->InterpolationMethod == eMovingTiltedPlane) 
	{
		this->MovingTiltedPlaneButton->setChecked(false);
		this->MovingHZButton->setChecked(true);
		this->InterpolationMethod = eMovingHzPlane;
	}
	else
	{
		this->MovingTiltedPlaneButton->setChecked(true);
		this->MovingHZButton->setChecked(false);
		this->InterpolationMethod = eMovingTiltedPlane;
	}

	this->MovingHZButton->blockSignals(false);
	this->MovingTiltedPlaneButton->blockSignals(false);
}

//================================================================================
	 
void Bui_DEMMatchingDlg::onMaxDistInterpolChanged()
{
	bool OK;
	this->MaximumDistance->blockSignals(true);

	double hlp = this->MaximumDistance->text().toDouble(&OK);
	if (OK)
	{
		if (hlp < FLT_EPSILON) this->maxDistInterpol = -1.0;
		else this->maxDistInterpol = hlp;
	}

	QString dt=QString("%1").arg(maxDistInterpol, 0, 'f', 1);
	this->MaximumDistance->setText(dt);

	this->MaximumDistance->blockSignals(false);
};

//================================================================================

void Bui_DEMMatchingDlg::onNeighbourhoodChanged()
{
	bool OK;
	this->Neighbourhood->blockSignals(true);

	int hlp = this->Neighbourhood->text().toInt(&OK);
	if (OK && (hlp > 0)) this->neighbourhoodSize = hlp;

	QString dt=QString("%1").arg(this->neighbourhoodSize);
	this->Neighbourhood->setText(dt);

	this->Neighbourhood->blockSignals(false);
};

//================================================================================
	 
void Bui_DEMMatchingDlg::onOuterBorderlineChanged()
{
	if (this->pOuterPolyLine) delete  this->pOuterPolyLine;
	this->pOuterPolyLine = 0;
		
	CCharString str;

	if (this->pRefSys)
	{
		str = (const char*) this->OuterBorderLine->text().toLatin1();
		str.MakeUpper();

		if (!str.IsEmpty())
		{
			CXYZPolyLineArray *polyLines = project.getXYZPolyLines();

			for (int i = 0; i < polyLines->GetSize() && !this->pOuterPolyLine; ++i)
			{
				CXYZPolyLine *p = polyLines->getAt(i);
				CCharString polyLabel = p->getLabel().GetChar();
				if (polyLabel.Find(str) == 0) 
				{
					if (p->isClosed()) 
					{
						CTrans3D *pTrafo = CTrans3DFactory::initTransformation(*(polyLines->getReferenceSystem()), *(this->pRefSys));
						if (pTrafo)
						{
							this->pOuterPolyLine = new CXYZPolyLine(*p);
							for (int i = 0; i < this->pOuterPolyLine->getPointCount(); ++i)
							{
								CXYZPoint *pP = this->pOuterPolyLine->getPoint(i);
								pTrafo->transform(*pP, *pP);
							}

							delete pTrafo;
						}
					}
				}
			}
		}
	}
	
	if (!this->pOuterPolyLine)
	{
		this->OuterBorderLine->blockSignals(true);
		this->OuterBorderLine->setText("");
		this->OuterBorderLine->blockSignals(false);
	}
	else 
	{
		this->OuterBorderLine->setText(str.GetChar());
	}
}

//================================================================================
	 
void Bui_DEMMatchingDlg::onInnerBorderlineChanged()
{
	for (unsigned int i = 0; i < this->InnerPolylineList.size(); ++i)
	{
		delete this->InnerPolylineList[i];
		this->InnerPolylineList[i] = 0;
	}
	this->InnerPolylineList.clear();

	if (this->pRefSys)
	{
		CCharString str = (const char*) this->InnerBorderLine->text().toLatin1();

		if (!str.IsEmpty())
		{
			CXYZPolyLineArray *polyLines = project.getXYZPolyLines();
			CTrans3D *pTrafo = CTrans3DFactory::initTransformation(*(polyLines->getReferenceSystem()), *(this->pRefSys));
			if (pTrafo)
			{				
				for (int i = 0; i < polyLines->GetSize(); ++i)
				{
					CXYZPolyLine *p = polyLines->getAt(i);
					CCharString polyLabel = p->getLabel().GetChar();
					if ((polyLabel.Find(str) == 0)  && p->isClosed())
					{
						CXYZPolyLine *pNew = new CXYZPolyLine(*p);
						this->InnerPolylineList.push_back(pNew);

						for (int j = 0; j < pNew->getPointCount(); ++j)
						{
							CXYZPoint *pP = pNew->getPoint(j);
							pTrafo->transform(*pP, *pP);
						}
					}
				}

				delete pTrafo;
			}
		}
	}
	if (!this->InnerPolylineList.size())
	{
		this->InnerBorderLine->blockSignals(true);
		this->InnerBorderLine->setText("");
		this->InnerBorderLine->blockSignals(false);
	}
}

//================================================================================
	 
void Bui_DEMMatchingDlg::onLLChanged()
{
	this->orthoLLX->blockSignals(true);
	this->orthoLLY->blockSignals(true);
	this->orthoURX->blockSignals(true);
	this->orthoURY->blockSignals(true);
	bool OK;
	double hlp = this->orthoLLX->text().toDouble(&OK);
	if (OK) this->llPoint.x = hlp;
	hlp = this->orthoLLY->text().toDouble(&OK);
	if (OK) this->llPoint.y = hlp;

	if (this->urPoint.x < this->llPoint.x) this->urPoint.x = this->llPoint.x + this->gridSize.x;
	else 
	{
		double f_w = (this->urPoint.x - this->llPoint.x) / this->gridSize.x;
		int w = floor(f_w);
		if (fabs(f_w - double(w)) > FLT_EPSILON) this->urPoint.x = this->llPoint.x + double (w + 1) * this->gridSize.x;
	}

	if (this->urPoint.y < this->llPoint.y) this->urPoint.y = this->llPoint.y + this->gridSize.y;
	else 
	{
		double f_h = (this->urPoint.y - this->llPoint.y) / this->gridSize.y;
		int h = floor(f_h);
		if (fabs(f_h - double(h)) > FLT_EPSILON) this->urPoint.y = this->llPoint.y + double (h + 1) * this->gridSize.y;
	}

	this->orthoLLX->blockSignals(false);
	this->orthoLLY->blockSignals(false);
	this->orthoURX->blockSignals(false);
	this->orthoURY->blockSignals(false);

	setDEMExtents();
};

//================================================================================
	 
void Bui_DEMMatchingDlg::onURChanged()
{
	this->orthoLLX->blockSignals(true);
	this->orthoLLY->blockSignals(true);
	this->orthoURX->blockSignals(true);
	this->orthoURY->blockSignals(true);

	bool OK;
	double hlp = this->orthoURX->text().toDouble(&OK);
	if (OK) this->urPoint.x = hlp;
	hlp = this->orthoURY->text().toDouble(&OK);
	if (OK) this->urPoint.y = hlp;

	if (this->urPoint.x < this->llPoint.x) this->llPoint.x = this->urPoint.x - this->gridSize.x;
	else 
	{
		double f_w = (this->urPoint.x - this->llPoint.x) / this->gridSize.x;
		int w = floor(f_w);
		if (fabs(f_w - double(w)) > FLT_EPSILON) this->llPoint.x = this->urPoint.x - double (w + 1) * this->gridSize.x;
	}

	if (this->urPoint.y < this->llPoint.y) this->llPoint.y = this->urPoint.y - this->gridSize.y;
	else 
	{
		double f_h = (this->urPoint.y - this->llPoint.y) / this->gridSize.y;
		int h = floor(f_h);
		if (fabs(f_h - double(h)) > FLT_EPSILON) this->llPoint.y = this->urPoint.y - double (h + 1) * this->gridSize.y;
	}

	this->orthoLLX->blockSignals(false);
	this->orthoLLY->blockSignals(false);
	this->orthoURX->blockSignals(false);
	this->orthoURY->blockSignals(false);

	setDEMExtents();
};

//================================================================================
	 
void Bui_DEMMatchingDlg::onGridChanged()
{
	bool OK;
	double hlp = this->orthoGResolution->text().toDouble(&OK);
	if (OK && (hlp > 0.01)) this->gridSize.x = this->gridSize.y = hlp;
	
	double f_w = (this->urPoint.x - this->llPoint.x) / this->gridSize.x;
	int w = floor(f_w);
	if (fabs(f_w - double(w)) > FLT_EPSILON) this->urPoint.x = this->llPoint.x + double (w + 1) * this->gridSize.x;

	double f_h = (this->urPoint.y - this->llPoint.y) / this->gridSize.y;
	int h = floor(f_h);
	if (fabs(f_h - double(h)) > FLT_EPSILON) this->urPoint.y = this->llPoint.y + double (h + 1) * this->gridSize.y;

	setDEMExtents();
};

//================================================================================

void Bui_DEMMatchingDlg::onOutputFileChanged()
{
	bool wasInit = true;

	if (!this->pRefSys) 
	{
		this->pRefSys = new CReferenceSystem;
		wasInit = false;
	}

	if (this->pRefSys)
	{
		CFileName fn(this->DEMFileName);

		Bui_ReferenceSysDlg dlg(this->pRefSys, &fn, "DEM Matching", "CoordinateSysDlg.html", true, "*.dem", false, false);
		dlg.exec();
		if (!dlg.getSuccess())
		{
			if (!wasInit) 
			{
				delete this->pRefSys;
				this->pRefSys = 0;
			}

		}
		else
		{
			this->DEMFileName = fn;
			this->OutputDirectory->setText(this->DEMFileName.GetChar());
		}
	}

	this->checkSetup();
};
	  
//================================================================================
	 
void Bui_DEMMatchingDlg::onFullDPChanged()
{
	this->SGMpars.fullDP = !this->SGMpars.fullDP;
	this->fullDPCheckBox->blockSignals(true);
	this->fullDPCheckBox->setChecked(this->SGMpars.fullDP);
	this->fullDPCheckBox->blockSignals(false);
};

//================================================================================
	 
void Bui_DEMMatchingDlg::getPminPmax(C2DPoint &pmin, C2DPoint &pmax, CVImage *img)
{
	C3DPoint PSensor;
	C3DPoint PDEM;
	C2DPoint p;
	CSensorModel *pSensor = img->getCurrentSensorModel();
	CTrans3D *pTrafo = CTrans3DFactory::initTransformation(pSensor->getRefSys(), *(this->pRefSys));
	int w = img->getHugeImage()->getWidth();
	int h = img->getHugeImage()->getHeight();
	p.x = 0;
	p.y = 0;
	pSensor->transformObs2Obj(PSensor, p,project.getMeanHeight());
	pTrafo->transform(PDEM, PSensor);

	pmin.x = pmax.x = PDEM.x;
	pmin.y = pmax.y = PDEM.y;

	p.x = w - 1;
	pSensor->transformObs2Obj(PSensor, p,project.getMeanHeight());
	pTrafo->transform(PDEM, PSensor);

	if (PDEM.x < pmin.x) pmin.x = PDEM.x;
	if (PDEM.x > pmax.x) pmax.x = PDEM.x;
	if (PDEM.y < pmin.y) pmin.y = PDEM.y;
	if (PDEM.y > pmax.y) pmax.y = PDEM.y;

	p.y = h - 1;
	pSensor->transformObs2Obj(PSensor, p,project.getMeanHeight());
	pTrafo->transform(PDEM, PSensor);

	if (PDEM.x < pmin.x) pmin.x = PDEM.x;
	if (PDEM.x > pmax.x) pmax.x = PDEM.x;
	if (PDEM.y < pmin.y) pmin.y = PDEM.y;
	if (PDEM.y > pmax.y) pmax.y = PDEM.y;

	p.x = 0;
	pSensor->transformObs2Obj(PSensor, p,project.getMeanHeight());
	pTrafo->transform(PDEM, PSensor);

	if (PDEM.x < pmin.x) pmin.x = PDEM.x;
	if (PDEM.x > pmax.x) pmax.x = PDEM.x;
	if (PDEM.y < pmin.y) pmin.y = PDEM.y;
	if (PDEM.y > pmax.y) pmax.y = PDEM.y;

	delete pTrafo;
};

//================================================================================
	 
void Bui_DEMMatchingDlg::onSetToMax()
{
	if (!this->pRefSys) this->onOutputFileChanged();
	if (this->pRefSys)
	{
		bool found = false;
		if (this->pOuterPolyLine)
		{
			if (this->pOuterPolyLine->getPointCount() > 2)
			{
				found = true;
				
				CXYZPoint *p = this->pOuterPolyLine->getPoint(0);

				this->llPoint.x = this->urPoint.x = p->x;
				this->llPoint.y = this->urPoint.y = p->y;

				for (int i = 1; i < this->pOuterPolyLine->getPointCount(); ++i)
				{
					p = this->pOuterPolyLine->getPoint(i);
					if (p->x < this->llPoint.x) this->llPoint.x = p->x;
					if (p->y < this->llPoint.y) this->llPoint.y = p->y;
					if (p->x > this->urPoint.x) this->urPoint.x = p->x;
					if (p->y > this->urPoint.y) this->urPoint.y = p->y;
				}

				this->llPoint.x -= this->gridSize.x;
				this->llPoint.y -= this->gridSize.y;
				this->urPoint.x += this->gridSize.x;
				this->urPoint.y += this->gridSize.y;
			}
		}
		
		if (!found)
		{
			this->getPminPmax(this->llPoint, this->urPoint, this->SelectedImageList[0]);
			C2DPoint pmin, pmax;
			for (unsigned int i = 1; i < this->SelectedImageList.size(); ++i)
			{
				this->getPminPmax(pmin, pmax, this->SelectedImageList[i]);

				if (pmin.x > this->llPoint.x) this->llPoint.x = pmin.x;
				if (pmin.y > this->llPoint.y) this->llPoint.y = pmin.y;
				if (pmax.x < this->urPoint.x) this->urPoint.x = pmax.x;
				if (pmax.y < this->urPoint.y) this->urPoint.y = pmax.y;
			}
		}

		int w = int(floor((this->urPoint.x - this->llPoint.x) / this->gridSize.x));
		int h = int(floor((this->urPoint.y - this->llPoint.y) / this->gridSize.y));

		if (w < 1) w = 1;
		if (h < 1) h = 1;

		this->llPoint.x = floor(this->llPoint.x / this->gridSize.x + 0.5) * this->gridSize.x;
		this->llPoint.y = floor(this->llPoint.y / this->gridSize.y + 0.5) * this->gridSize.y;
		this->urPoint.x = this->llPoint.x + double(w) * this->gridSize.x;
		this->urPoint.y = this->llPoint.y + double(h) * this->gridSize.y;

		setDEMExtents();
	}
}

//================================================================================
	 
void Bui_DEMMatchingDlg::checkSetup()
{
	if (this->SelectedImageList.size() > 1)
	{
		this->SetToMaxButton->setEnabled(true);
		if (this->DEMFileName.IsEmpty()) this->buttonOk->setEnabled(false);
		else this->buttonOk->setEnabled(true);
	}
	else
	{
		this->SetToMaxButton->setEnabled(false);
		this->buttonOk->setEnabled(false);
	}
};

//================================================================================
	 
bool Bui_DEMMatchingDlg::compute()
{
	CCharString oldPath = project.getFileName()->GetPath();
	CCharString newPath = this->DEMFileName.GetPath();

	CFileName prot("DEMMatch.prt");
	prot.SetPath(newPath);
	protHandler.setDefaultDirectory(newPath); 

	protHandler.open(protHandler.prt, prot, false);

	this->success = this->matcher->match();

	protHandler.close(protHandler.prt);
	protHandler.setDefaultDirectory(oldPath); 

	return this->success;
}

//================================================================================
	 