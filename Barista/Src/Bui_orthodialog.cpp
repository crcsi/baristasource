#include <float.h>
#include "Bui_orthodialog.h"

#include <qfiledialog.h>
#include <QMessageBox>

#include "ObjectSelector.h"
#include "ProgressThread.h"
#include "OrthoImageGenerator.h"
#include "BaristaProject.h"
#include "Icons.h"

#include "VImage.h"
#include "BaristaHtmlHelp.h"
#include "IniFile.h"
#include "Bui_GDALExportDlg.h"

#include "SystemUtilities.h"
#include "GDALFile.h"
#include "Bui_ProgressDlg.h"
#include "ImageImporter.h"


bui_OrthoDialog::bui_OrthoDialog(CVImage *image, C2DPoint *lrc, C2DPoint *ulc, const char *currentDir)
{
	setupUi(this);
	connect(this->buttonOk,             SIGNAL(released()),                      this, SLOT(ComputeOrthoimage()));
	connect(this->buttonCancel,         SIGNAL(released()),                      this, SLOT(Cancel()));
	connect(this->ImageSelector,        SIGNAL(currentRowChanged (int)),         this, SLOT(ImageSelected(int)));
	connect(this->ImageSelector,        SIGNAL(itemClicked (QListWidgetItem *)), this, SLOT(ImageClicked(QListWidgetItem *)));
	connect(this->DEM,                  SIGNAL(currentIndexChanged(int)),        this, SLOT(DEMSelected()));
	connect(this->orthoLLX,             SIGNAL(editingFinished()),               this, SLOT(OrthoLLCChanged()));
	connect(this->orthoLLY,             SIGNAL(editingFinished()),               this, SLOT(OrthoLLCChanged()));
	connect(this->orthoURX,             SIGNAL(editingFinished()),               this, SLOT(OrthoRUCChanged()));
	connect(this->orthoURY,             SIGNAL(editingFinished()),               this, SLOT(OrthoRUCChanged()));
	connect(this->orthoResolution,      SIGNAL(editingFinished()),               this, SLOT(OrthoGSDChanged()));
	connect(this->BicubicButton,        SIGNAL(clicked(bool)),                   this, SLOT(InterpolationMethodSelected()));
	connect(this->BilinearButton,       SIGNAL(clicked(bool)),                   this, SLOT(InterpolationMethodSelected()));
	connect(this->UseAnchor,            SIGNAL(stateChanged(int)),               this, SLOT(AnchorSwitchChanged()));
	connect(this->AnchorWidth,          SIGNAL(editingFinished()),               this, SLOT(AnchorWidthChanged()));
	connect(this->orthoOutputImage,     SIGNAL(released()),                      this, SLOT(OpenOutImage()));
	connect(this->applyFileTypeToAll,   SIGNAL(released()),                      this, SLOT(applyFormatToAllSelected()));
	connect(this->maxExtentsAllButton,  SIGNAL(released()),                      this, SLOT(setMaxExtentsForAllSelected()));
	connect(this->ApplyResolutionToAll, SIGNAL(released()),                      this, SLOT(applyGSDToAllSelected()));
	connect(this->ApplyToAllButton,     SIGNAL(released()),                      this, SLOT(applyExtentsToAllSelected()));
	connect(this->buttonHelp,           SIGNAL(released()),                      this, SLOT(Help()));
	connect(this->maxExtentsButton,     SIGNAL(released()),                      this, SLOT(initDEMBorders()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init(image, lrc, ulc);
	this->pCurrentDir = currentDir;
	this->checkSetup();
}

//================================================================================

bui_OrthoDialog::~bui_OrthoDialog()
{

}
//================================================================================

void bui_OrthoDialog::init(CVImage *img, C2DPoint *lrc, C2DPoint *ulc)
{
	this->Orthocreated = false;

	// init the vector
	this->imageVector.init(img,lrc,ulc);

	// init GUI with available images
	if (img) this->initImg(img);
	else this->initImgList();

	this->DEM->blockSignals(true);

	// add all available DEMs to GUI
	for (int i = 0; i < project.getNumberOfDEMs(); ++i)
	{
		CVDEM *dem = project.getDEMAt(i);
		this->DEM->addItem(dem->getFileName()->GetChar());

	}

	// select the one which has been selected automatically
	if (this->imageVector.getDEM())
	{
		int selected = project.getDEMIndex(this->imageVector.getDEM());

		this->DEM->setCurrentIndex(selected);
		this->DEM->setToolTip(this->imageVector.getDEM()->getFileName()->GetChar());
	}

	this->DEM->blockSignals(false);

	setInterpolationGUI();
	setAnchorGUI();
}

//================================================================================

void bui_OrthoDialog::initImgList()
{
	this->selectedImage = 0;
	if (this->imageVector.size() > 0)
	{
		for (unsigned int i=0; i < imageVector.size(); i++)
		{
			CCharString name = imageVector[i].getInputImg()->getFileNameChar();

			QListWidgetItem* item = new QListWidgetItem(name.GetChar());
			item->setData(Qt::UserRole,i); // remember index into array
			item->setCheckState(Qt::Unchecked);
			item->setToolTip(name.GetChar());

			this->ImageSelector->addItem(item);
			if (i == 0)
			{
				item->setCheckState(Qt::Checked);
				imageVector[i].setIsSelected(true);
			}
		}

		this->ImageSelector->setCurrentItem(this->ImageSelector->item(0));

		setCurrentImage(this->selectedImage);
	}
}

//================================================================================

void bui_OrthoDialog::initImg(CVImage *image)
{
	if (this->imageVector.size() == 1)
	{
		this->selectedImage = 0;
		CCharString name = this->imageVector[0].getInputImg()->getFileNameChar();

		QListWidgetItem* item = new QListWidgetItem(name.GetChar());
		item->setFlags(Qt::ItemFlags(item->flags() - Qt::ItemIsUserCheckable - Qt::ItemIsSelectable));

		item->setData(Qt::UserRole,0); // remember index into array
		item->setToolTip(name.GetChar());
		this->ImageSelector->addItem(item);
		this->imageVector[0].setIsSelected(true);
		this->ImageSelector->setCurrentItem(this->ImageSelector->item(0));

		this->setCurrentImage(this->selectedImage);
	}

}

//================================================================================

void bui_OrthoDialog::setCurrentImage(int index)
{
	this->selectedImage = index;

	this->orthoResolution->blockSignals(true);
	this->orthoLLX->blockSignals(true);
	this->orthoLLY->blockSignals(true);
	this->orthoURX->blockSignals(true);
	this->orthoURY->blockSignals(true);

	this->orthoOutputFile->setText(this->imageVector[this->selectedImage].getOrthoFileNameChar());
	this->orthoOutputFile->setToolTip(this->imageVector[this->selectedImage].getOrthoFileNameChar());

	QString ds = QString("%1").arg(this->imageVector[this->selectedImage].getGSD().x, 0, 'f', 3);
	this->orthoResolution->setText(ds);

	ds=QString("%1").arg(this->imageVector[this->selectedImage].getLLC().x, 0, 'f', 3);
	this->orthoLLX->setText(ds);
	ds=QString("%1").arg(this->imageVector[this->selectedImage].getLLC().y, 0, 'f', 3);
	this->orthoLLY->setText(ds);
	ds=QString("%1").arg(this->imageVector[this->selectedImage].getRUC().x, 0, 'f', 3);
	this->orthoURX->setText(ds);
	ds=QString("%1").arg(this->imageVector[this->selectedImage].getRUC().y, 0, 'f', 3);
	this->orthoURY->setText(ds);

	this->orthoResolution->blockSignals(false);
	this->orthoLLX->blockSignals(false);
	this->orthoLLY->blockSignals(false);
	this->orthoURX->blockSignals(false);
	this->orthoURY->blockSignals(false);
}

//================================================================================

void bui_OrthoDialog::setInterpolationGUI()
{
	this->BilinearButton->blockSignals(true);
	this->BicubicButton->blockSignals(true);
	this->BicubicButton->setChecked(this->imageVector.useBicubicInterpolation());
	this->BilinearButton->setChecked(!this->imageVector.useBicubicInterpolation());
	this->BilinearButton->blockSignals(false);
	this->BicubicButton->blockSignals(false);
};

//================================================================================

void bui_OrthoDialog::setAnchorGUI()
{
	this->UseAnchor->blockSignals(true);
	this->AnchorWidth->blockSignals(true);
	this->UseAnchor->setChecked(this->imageVector.useAnchorPoints());

	QString ds=QString("%1").arg(this->imageVector.getAnchorWidth(), 0);
	this->AnchorWidth->setText(ds);

	if (this->imageVector.useAnchorPoints()) this->AnchorWidth->setEnabled(true);
	else this->AnchorWidth->setEnabled(false);

	this->UseAnchor->blockSignals(false);
	this->AnchorWidth->blockSignals(false);
};


//================================================================================

void bui_OrthoDialog::InterpolationMethodSelected()
{
	if (this->BilinearButton->isChecked())
	{
		if (this->BicubicButton->isChecked()) this->BicubicButton->setChecked(false);
		this->imageVector.setToBicubicInterpolation(false);
	}
	else
	{
		if (!this->BicubicButton->isChecked()) this->BicubicButton->setChecked(true);
		this->imageVector.setToBicubicInterpolation(true);
	};

	this->BilinearButton->setFocus();
};

//================================================================================

void bui_OrthoDialog::AnchorSwitchChanged()
{
	this->imageVector.setUseAnchorPoints(!this->imageVector.useAnchorPoints());
	setAnchorGUI();
};

//================================================================================

void bui_OrthoDialog::AnchorWidthChanged()
{
	bool isOK;
	int newWidth = this->AnchorWidth->text().toInt(&isOK);
	if (isOK && (newWidth > 0))
	{
		this->imageVector.setAnchorWidth(newWidth);
	}

	setAnchorGUI();
};

//================================================================================

void bui_OrthoDialog::ImageSelected(int row)
{
	if (!this->imageVector.hasOneImageOnly())
	{
		this->selectedImage = this->ImageSelector->item(row)->data(Qt::UserRole).toInt();

		this->setCurrentImage(this->selectedImage);

		this->checkSetup();
	}
}

//================================================================================

void bui_OrthoDialog::ImageClicked(QListWidgetItem *item)
{
	if (!this->imageVector.hasOneImageOnly())
	{
		int index = item->data(Qt::UserRole).toInt();
		this->imageVector[index].setIsSelected(item->checkState() == Qt::Checked);
		this->checkSetup();
	}
};

//================================================================================

void bui_OrthoDialog::DEMSelected()
{
	int indexSelected = (this->DEM->currentIndex());

	if ((indexSelected >= 0) && (indexSelected < project.getNumberOfDEMs()))
	{
		this->imageVector.setDEM(project.getDEMAt(indexSelected));

		if (this->imageVector.getDEM())
			this->DEM->setToolTip(this->imageVector.getDEM()->getFileName()->GetChar());
	}

	this->checkSetup();
}

//================================================================================

void bui_OrthoDialog::OrthoLLCChanged()
{
	if (this->imageVector.isInitialised())
	{
		bool isDblX,isDblY;

		C2DPoint newLLC = this->imageVector[this->selectedImage].getLLC();

		newLLC.x = this->orthoLLX->text().toDouble(&isDblX);
		newLLC.y = this->orthoLLY->text().toDouble(&isDblY);

		if (isDblX && isDblY)
		{
			this->imageVector[this->selectedImage].setLLC(newLLC);
		}

		this->setCurrentImage(this->selectedImage);
	}
}

//================================================================================

void bui_OrthoDialog::OrthoRUCChanged()
{
	if (this->imageVector.isInitialised())
	{
		bool isDblX,isDblY;

		C2DPoint newRUC = this->imageVector[this->selectedImage].getRUC();

		newRUC.x = this->orthoURX->text().toDouble(&isDblX);
		newRUC.y = this->orthoURY->text().toDouble(&isDblY);

		if (isDblX && isDblY)
		{
			this->imageVector[this->selectedImage].setRUC(newRUC);
		}
		this->setCurrentImage(this->selectedImage);
	}
}

//================================================================================

void bui_OrthoDialog::OrthoGSDChanged()
{
	if (this->imageVector.isInitialised())
	{
		bool isDbl;
		double gsd = this->orthoResolution->text().toDouble(&isDbl);
		if (isDbl && (gsd > FLT_EPSILON))
		{
			this->imageVector[this->selectedImage].setGSD(gsd,gsd);

			this->setCurrentImage(this->selectedImage);
		}
	}
}

//================================================================================

void bui_OrthoDialog::initDEMBorders()
{

	if (!this->imageVector.initDEMBorders(this->selectedImage))
	{
		QMessageBox::warning( this,this->imageVector.getErrorTitleMessage().GetChar(),this->imageVector.getErrorMessage().GetChar());
	}
	else
		this->setCurrentImage(this->selectedImage);

}

//================================================================================

void bui_OrthoDialog::applyFormatToAllSelected()
{
	if (this->imageVector.isInitialised())
	{
		CImageExporter& exporter = this->imageVector[this->selectedImage].getImageExporter();


		for (unsigned int i = 0; i < this->imageVector.size(); ++i)
		{
			if ((i != this->selectedImage))
			{
				this->imageVector[i].getImageExporter().applySettings(exporter);
			}
		}
	}
}

//================================================================================

void bui_OrthoDialog::setMaxExtentsForAllSelected()
{
	if (this->imageVector.isInitialised())
	{
		this->imageVector.setMaxExtentsForAll();
		this->setCurrentImage(this->selectedImage);
	}

}

//================================================================================

void bui_OrthoDialog::applyGSDToAllSelected()
{
	if (this->imageVector.isInitialised())
	{
		bool isDbl;
		double gsd = this->orthoResolution->text().toDouble(&isDbl);
		if (isDbl && (gsd > FLT_EPSILON))
		{
			this->imageVector.setGSDForAll(gsd);

			this->setCurrentImage(this->selectedImage);
		}
	}
}


//================================================================================

void bui_OrthoDialog::applyExtentsToAllSelected()
{
	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		if (i != this->selectedImage)
		{
			this->imageVector[i].setExtents(this->imageVector[this->selectedImage].getLLC(),
				                            this->imageVector[this->selectedImage].getRUC());
		}
	}
}


//================================================================================

void bui_OrthoDialog::OpenOutImage()
{
	if (this->imageVector.isInitialised())
	{
		// use the exporter from the record and init the dlg
		CImageExporter& imageExporter = this->imageVector[this->selectedImage].getImageExporter();
		Bui_GDALExportDlg dlg(imageExporter,"Select Output File",true,true,true);

		dlg.exec();

		if (dlg.getSuccess())
		{
			this->imageVector[this->selectedImage].setOrthoPhotoName(imageExporter.getFilename());
			this->orthoOutputFile->setText(this->imageVector[this->selectedImage].getOrthoFileNameChar());
			this->orthoOutputFile->setToolTip(this->imageVector[this->selectedImage].getOrthoFileNameChar());
		}
		else
		{
			this->imageVector[this->selectedImage].clearExporter();
		}

		this->checkSetup();
	}
}

//================================================================================

void bui_OrthoDialog::checkSetup()
{
	int iSelected = 0;
	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		if (this->imageVector[i].getIsSelected()) ++iSelected;
	}
	if (iSelected && this->imageVector.getDEM())
	{
		this->buttonOk->setEnabled(true);
	}
	else
	{
		this->buttonOk->setEnabled(false);
	}
}

//================================================================================

void bui_OrthoDialog::ComputeOrthoimage()
{
	int nImgs = 0;
	int iComputed = 0;

	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		if (this->imageVector[i].getIsSelected())
		{
			++nImgs;
		}
	};

	if (this->imageVector.getDEM() && nImgs)
	{
		this->buttonOk->setEnabled(false);

		for (unsigned int i = 0; i < this->imageVector.size(); ++i)
		{
			if (this->imageVector[i].getIsSelected())
			{
				COrthoImageGenerator orthogenerator;
				orthogenerator.setLLX(this->imageVector[i].getLLC().x);
				orthogenerator.setLLY(this->imageVector[i].getLLC().y);
				orthogenerator.setURX(this->imageVector[i].getRUC().x);
				orthogenerator.setURY(this->imageVector[i].getRUC().y);
				orthogenerator.setGsd(this->imageVector[i].getGSD().x);
				orthogenerator.setImage(this->imageVector[i].getInputImg());
				orthogenerator.setDEM(this->imageVector.getDEM());
				orthogenerator.setBicubic(this->imageVector.useBicubicInterpolation());
				orthogenerator.setUseAnchor(this->imageVector.useAnchorPoints());
				orthogenerator.setAnchorWidth(this->imageVector.getAnchorWidth());
				orthogenerator.orthoimage = this->imageVector[i].getOrtho();

				inifile.setOrthoRes(orthogenerator.getGsd());
				inifile.setOrthoMinX(orthogenerator.getLLX());
				inifile.setOrthoMinY(orthogenerator.getLLY());
				inifile.setOrthoMaxX(orthogenerator.getURX());
				inifile.setOrthoMaxY(orthogenerator.getURY());
				inifile.setOrthoDEM(*(this->imageVector.getDEM()->getFileName()));
				inifile.writeIniFile();

				ostrstream oss;
				oss << "(" << (iComputed + 1) << "/" << nImgs <<") ..." << ends;
				char *z  = oss.str();
				CCharString titleApp(z);
				delete [] z;

				CCharString title = "Orthophoto generation " + titleApp;

				// create the ortho image
				bui_ProgressDlg orthoDlg(new COrthoProcessingThread(&orthogenerator), &orthogenerator,title.GetChar());
				orthoDlg.exec();

				if (orthoDlg.getSuccess())
				{
					++iComputed;

					// export ortho, use dlg to show progress bar
					CImageExporter& imageExporter = this->imageVector[i].getImageExporter();
					imageExporter.writeSettings();
					bui_ProgressDlg exportDlg(new CGDALExportThread(&imageExporter),&imageExporter,"Exporting file...");
					exportDlg.exec();

					if (exportDlg.getSuccess())
					{
						// reimport into project, we don't need a progress bar here as we know that the
						// huge file is there
						CImageImporter imageImporter;
						CImageImportData importData;

						imageImporter.initImportData(importData,false,this->imageVector[i].getOrthoFileName(),"");
						imageImporter.importImage(importData,false);

					}
					else
					{
						// something went wrong when exporting the image, maybe not supported export settings??
						QMessageBox::warning(NULL,"Export Error","Barista couldn't export the image!\nOnly the .hug file will be loaded!",1,0,0);

						// so we just import the huge file and leave the export to the user
						CImageImporter imageImporter;
						CImageImportData importData;

						imageImporter.initImportData(importData,false,this->imageVector[i].getOrthoHugeFileName(),"");
						imageImporter.importImage(importData,false);
					}

				}
				else
				{
					QMessageBox::warning(this,"Orthogeneration",orthogenerator.getErrorMessage().GetChar());
				}
			}
		}
	}

	this->close();

	this->Orthocreated = (iComputed > 0);
}


//================================================================================

void bui_OrthoDialog::Cancel()
{
	this->close();
}

//================================================================================

void bui_OrthoDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/Orthoimage.html");
};

//================================================================================
