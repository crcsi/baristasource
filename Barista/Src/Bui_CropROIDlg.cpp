#include "Bui_CropROIDlg.h"
#include "Icons.h"

#include <QMessageBox>
#include <QFileDialog>

#include "BaristaHtmlHelp.h"
#include "BaristaView.h"
#include "GDALFile.h"
#include "Bui_GDALExportDlg.h"
#include "IniFile.h"
#include "BaristaProject.h"
#include "VImage.h"
#include "VDEM.h"


Bui_CropROIDlg::Bui_CropROIDlg(CBaristaView *view, const char *title, const char* helpfile,
							   bool hasRPCs,bool hasPushBroom,bool hasTFW,bool hasPoints,bool hasAffine): 
		view(view),title(title),helpfile(helpfile),hasRPCs(hasRPCs),hasPushBroom(hasPushBroom),hasTFW(hasTFW),
		hasPoints(hasPoints),hasAffine(hasAffine),dlg(NULL),oldVImage(view->getVImage()),oldVDem(view->getVDEM()),
		newVImage(NULL),newVDem(NULL),success(false)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
	this->setWindowTitle(QString(title));

	// pushbutton connections
	this->connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));

	// radio button connections
	this->connect(this->rbPixel,SIGNAL(released()),this,SLOT(rbPixelReleased()));
	this->connect(this->rbMetric,SIGNAL(released()),this,SLOT(rbMetricReleased()));

	// spin box connections
	this->connect(this->spinBoxULX, SIGNAL(valueChanged(double)), this, SLOT(spinBoxULXChanged(double)));
	this->connect(this->spinBoxULY, SIGNAL(valueChanged(double)), this, SLOT(spinBoxULYChanged(double)));
	this->connect(this->spinBoxLRX, SIGNAL(valueChanged(double)), this, SLOT(spinBoxLRXChanged(double)));
	this->connect(this->spinBoxLRY, SIGNAL(valueChanged(double)), this, SLOT(spinBoxLRYChanged(double)));

	this->connect(this->spinBoxHeight, SIGNAL(valueChanged(double)), this, SLOT(spinBoxHeightChanged(double)));
	this->connect(this->spinBoxWidth, SIGNAL(valueChanged(double)), this, SLOT(spinBoxWidthChanged(double)));

	// output image connection
	connect(this->cropOutputImage, SIGNAL(released()),this, SLOT(OpenOutImage()));

	this->initDlg();

	this->ndigits = 2;
}

Bui_CropROIDlg::~Bui_CropROIDlg()
{
	if (this->dlg)
		delete this->dlg;

	this->dlg=NULL;

	if (!this->success)
	{
		if (this->newVImage)
			project.deleteImage(this->newVImage);

		if (this->newVDem)
			project.deleteDEM(this->newVDem);
	}

}


void Bui_CropROIDlg::initDlg()
{
	if ( this->view->getVImage() )
	{
		// enable/set radio button for pixel/metric (only when TFW available
		this->rbPixel->setChecked(true);
		if ( !this->hasTFW ) this->rbMetric->setVisible(false);
		else
		{
			this->rbMetric->setVisible(true);
		}
		
		this->gbCrop->setVisible(false);

		// enable what might all be cropped
		if ( !this->hasRPCs ) this->cbRPC->setVisible(false);
		if ( !this->hasTFW ) this->cbTFW->setVisible(false);
		if ( !this->hasPushBroom ) this->cbPushBroom->setVisible(false);
		if ( !this->hasPoints ) this->cbPoints->setVisible(false);
		if ( !this->hasAffine) this->cbAffine->setVisible(false);

		if ( this->hasRPCs || this->hasTFW || this->hasPushBroom || this->hasPoints || this->hasAffine)
			this->gbCrop->setVisible(true);

		this->pBOk->setEnabled(false);

		this->setSpinBoxes();
	}

	if ( this->view->getVDEM() )
	{
		// enable/set radio button for pixel/metric (only when TFW available
		this->rbPixel->setChecked(true);

		// DEM has TFW
		this->rbMetric->setVisible(true);

		// don't show tick option in DEM case
		this->gbCrop->setVisible(false);

		this->cropOutputImage->setText("Output DEM");

		this->pBOk->setEnabled(false);

		this->setSpinBoxes();
	}

	// to make it as small as possible, minimumpolicy takes care of required size
	this->resize(0,0);
}


void Bui_CropROIDlg::pBCancelReleased()
{

	this->close();
}

void Bui_CropROIDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp(this->helpfile);
}


void Bui_CropROIDlg::rbMetricReleased()
{
	// set spinbox display to metric
	this->setMetricSpinBoxes();
}

void Bui_CropROIDlg::rbPixelReleased()
{
	// set spinbox display to pixel
	this->setPixelSpinBoxes();
}

void Bui_CropROIDlg::spinBoxULXChanged(double newulx)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriculx = newulx;
		this->getROIfromMetrictoPixel();
	}
	else
		this->ulx = (int)newulx;

	this->updateROI();
	this->setSpinBoxes();
}

void Bui_CropROIDlg::spinBoxULYChanged(double newuly)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriculy = newuly;
		this->getROIfromMetrictoPixel();
	}
	else
	{
		this->uly = (int)newuly;
	}

	this->updateROI();
	this->setSpinBoxes();
}

void Bui_CropROIDlg::spinBoxLRXChanged(double newlrx)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriclrx = newlrx;
		this->getROIfromMetrictoPixel();
	}
	else
		this->lrx = (int)newlrx;

	this->updateROI();
	this->setSpinBoxes();
}

void Bui_CropROIDlg::spinBoxLRYChanged(double newlry)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriclry = newlry;
		this->getROIfromMetrictoPixel();
	}

	else
		this->lry = (int)newlry;

	this->updateROI();
	this->setSpinBoxes();
}

void Bui_CropROIDlg::spinBoxHeightChanged(double newheight)
{
	if ( this->rbMetric->isChecked() ) 
	{
		this->metriclry = this->metriculy - newheight;
		this->getROIfromMetrictoPixel();
	}
	else
	{
		this->lry = this->uly + newheight;
	}

	this->updateROI();
	this->setSpinBoxes();
}

void Bui_CropROIDlg::spinBoxWidthChanged(double newwidth)
{
	if ( this->rbMetric->isChecked() )
	{
		this->metriclrx = this->metriculx + newwidth;
		this->getROIfromMetrictoPixel();
	}
	else
	{
		this->lrx = this->ulx + newwidth;
	}

	this->updateROI();
	this->setSpinBoxes();
}

void Bui_CropROIDlg::setSpinBoxes()
{
	if ( this->rbMetric->isChecked() ) this->setMetricSpinBoxes();
	else this->setPixelSpinBoxes();
}


void Bui_CropROIDlg::setPixelSpinBoxes()
{
	this->setPixelValues();
	this->blockSpinBoxSignals(true);
	this->setSpinBoxDecimals(this->ndigits);
	this->setSpinBoxStep(1);

	// set ranges of the spin boxes
	this->spinBoxULX->setRange(0, this->maxx);
	this->spinBoxULY->setRange(0, this->maxy);
	this->spinBoxLRX->setRange(this->ulx, this->maxx);
	this->spinBoxLRY->setRange(this->uly, this->maxy);

	// set them to size of ul of ROI to lr image corner
	this->spinBoxWidth->setRange(0, this->maxWidth);
	this->spinBoxHeight->setRange(0, this->maxHeight);
	
	// set current ROI values
	this->spinBoxULX->setValue(this->ulx);
	this->spinBoxULY->setValue(this->uly);
	this->spinBoxLRX->setValue(this->lrx);
	this->spinBoxLRY->setValue(this->lry);
	this->spinBoxHeight->setValue(this->height);
	this->spinBoxWidth->setValue(this->width);

	this->blockSpinBoxSignals(false);
}

void Bui_CropROIDlg::setMetricSpinBoxes()
{
	const CTFW* tfw =  this->view->getTFW();
	if ( tfw && tfw->gethasValues() )
	{		
		if ( tfw->getRefSys().getCoordinateType() == eGEOGRAPHIC )
			this->setSpinBoxStep(0.0001);
		else
			this->setSpinBoxStep(1);


		// calculate ROI in metric and reproject back to image
		this->setMetricValues();
		this->blockSpinBoxSignals(true);
		this->setSpinBoxDecimals(this->ndigits);

		// set ranges of the spin boxes
		this->spinBoxULX->setRange(this->metricminx, this->metricmaxx);
		this->spinBoxULY->setRange(this->metricminy, this->metricmaxy);
		this->spinBoxLRX->setRange(this->metriculx, this->metricmaxx);
		this->spinBoxLRY->setRange(this->metricminy, this->metriculy);

		// set them to size of ul of ROI to lr image corner
		this->spinBoxWidth->setRange(0, this->metricmaxwidth);
		this->spinBoxHeight->setRange(0, this->metricmaxheight);
		
		// set current ROI values
		this->spinBoxULX->setValue(this->metriculx);
		this->spinBoxULY->setValue(this->metriculy);
		this->spinBoxLRX->setValue(this->metriclrx);
		this->spinBoxLRY->setValue(this->metriclry);
		this->spinBoxHeight->setValue(this->metricheight);
		this->spinBoxWidth->setValue(this->metricwidth);

		this->blockSpinBoxSignals(false);

	}
}

void Bui_CropROIDlg::OpenOutImage()
{

	if (this->dlg)
		delete this->dlg;
	this->dlg = NULL;


	if (this->oldVImage)
	{
		if (this->newVImage)
			project.deleteImage(this->newVImage);

		this->newVImage = project.addImage(this->cropName,false);
		this->newVImage->getHugeImage()->bands = this->oldVImage->getHugeImage()->getBands();
		this->newVImage->getHugeImage()->bpp = this->oldVImage->getHugeImage()->getBpp();
		if ( this->oldVImage->hasTFW() && this->cbTFW->isChecked() )
			this->newVImage->setTFW(*this->oldVImage->getTFW());
		else 
		{
			CTFW tfw;		
			this->newVImage->setTFW(tfw);
		}

		this->newVImage->getHugeImage()->setTransferFunction(this->oldVImage->getHugeImage()->getTransferFunction(),true,true);

		this->dlg = new Bui_GDALExportDlg(this->cropName,this->newVImage,false,"Export Image");
	}
	else if (this->oldVDem)
	{
		if (this->newVDem)
			project.deleteDEM(this->newVDem);

		this->newVDem = project.addDEM(this->cropName);
		this->newVDem->getDEM()->bands = this->oldVDem->getDEM()->getBands();
		this->newVDem->getDEM()->bpp = this->oldVDem->getDEM()->getBpp();
		this->newVDem->getDEM()->setTFW(*this->oldVDem->getDEM()->getTFW());
		this->dlg = new Bui_GDALExportDlg(this->cropName,this->newVDem,true,"Export DEM");
	}
	else 
		return;

	dlg->exec();


	if (!dlg->getSuccess())	// false if user has pressed "Cancel"
	{
		if (this->oldVImage && this->newVImage)
			project.deleteImage(this->newVImage);
		else if (this->oldVDem && this->newVDem)
			project.deleteDEM(this->newVDem);

		return;
	}
	
	this->cropName = this->dlg->getFilename();
	this->cropOutputFile->setText(this->cropName.GetFullFileName().GetChar());

	this->checkSetup();

}


void Bui_CropROIDlg::checkSetup()
{
	bool okay = true;
	if ( this->ulx < 0 || this->ulx > this->maxx ) okay = false;
	if ( this->uly < 0 || this->uly > this->maxy ) okay = false;
	if ( this->cropName.GetLength() < 0 ) okay = false;

	this->pBOk->setEnabled(okay);
}

void Bui_CropROIDlg::pBOkReleased()
{
	this->setMetricValues();
	bool okay = true;

	this->close();

	// check box values
	bool cropPoints = false;
	bool cropRPC = false;
	bool cropTFW  = false;
	bool cropPushBroom  = false;
	bool cropAffine = false;

	// crop according to selected check boxes
	if ( this->cbPoints->isChecked() )  cropPoints = true;
	if ( this->cbTFW->isChecked() ) cropTFW = true;
	if ( this->cbRPC->isChecked() ) cropRPC = true;
	if ( this->cbPushBroom->isChecked() ) cropPushBroom  = true;
	if ( this->cbAffine->isChecked() ) cropAffine  = true;

	// do cropping of VImage (including points and sensor models)
	if (this->oldVImage && this->newVImage)
	{
		this->newVImage->setName(this->cropName.GetChar());
		okay = this->newVImage->cropfromImage(this->oldVImage,this->ulx, this->uly, this->width, this->height,
		cropPoints ,cropRPC,cropTFW || this->dlg->getExportGeo(),cropPushBroom,cropAffine);
	}
	else if (this->oldVDem && this->newVDem)
	{
		this->newVDem->setFileName(this->cropName.GetChar());
		okay = this->newVDem->cropfromDEM(this->oldVDem,this->ulx, this->uly, this->width, this->height);
	}

	if (this->dlg && okay)
	{
		if (!cropTFW && this->newVImage)
				dlg->setExportGeo(false);

		dlg->writeSettings();
		okay = dlg->writeImage();
	}

	if ( okay )
	{
		if ( this->view->getVImage() )
		{
			QMessageBox::information( this, "Crop image successful!",
			"Image was cropped, saved and added to current project");
		}
		if ( this->view->getVDEM() )
		{
			QMessageBox::information( this, "Crop DEM successful!",
			"DEM was cropped, saved and added to current project");
		}
		this->success = true;
	}
	else
		QMessageBox::information( this, "Cropping failed!",
		"Could not crop Data");


}

void Bui_CropROIDlg::setPixelValues()
{
	// collect information for crop dialog
	//this->ulx = v->getROIUL().x;
	//this->uly = v->getROIUL().y;
	//this->lrx = v->getROILR().x;
	//this->lry = v->getROILR().y;
	
	this->ulx = floor(this->view->getROIUL().x + 0.5);
	this->uly = floor(this->view->getROIUL().y + 0.5);
	this->lrx = floor(this->view->getROILR().x + 0.5);
	this->lry = floor(this->view->getROILR().y + 0.5);

	this->width = this->lrx - this->ulx;
	this->height = this->lry - this->uly;

	this->maxx = this->view->getDataWidth(false);
	this->maxy = this->view->getDataHeight(false);

	this->maxWidth = this->maxx  - this->ulx;
	this->maxHeight = this->maxy - this->uly;

	this->ndigits = 0;

	this->updateROI();
}



void Bui_CropROIDlg::setMetricValues()
{
	const CTFW* tfw = view->getTFW();

	if (!tfw)
		return;
	


	// collect information for crop dialog
	this->ulx = this->view->getROIUL().x;
	this->uly = this->view->getROIUL().y;
	this->lrx = this->view->getROILR().x;
	this->lry = this->view->getROILR().y;

	CXYPoint ULImg(0, 0);
	CXYPoint UL(this->ulx, this->uly);
	CXYPoint LR(this->lrx, this->lry);
	CXYPoint LRImg(this->maxx, this->maxy);

	CXYZPoint metricULImg;
	CXYZPoint metricUL;
	CXYZPoint metricLR;
	CXYZPoint metricLRImg;

	tfw->transformObs2Obj(metricULImg, ULImg);
	tfw->transformObs2Obj(metricUL, UL);
	tfw->transformObs2Obj(metricLR, LR);
	tfw->transformObs2Obj(metricLRImg, LRImg);

	if ( tfw->getRefSys().getCoordinateType() == eGRID )
	{
		this->ndigits = 2;
		this->metriculx = metricUL.x;
		this->metriculy = metricUL.y;

		this->metriclrx = metricLR.x;
		this->metriclry = metricLR.y;

		this->metricwidth = this->metriclrx - this->metriculx;
		this->metricheight = this->metriculy - this->metriclry;

		this->metricminx = metricULImg.x;
		this->metricmaxx = metricLRImg.x;

		this->metricminy = metricLRImg.y;
		this->metricmaxy = metricULImg.y;

		this->metricmaxwidth = this->metricmaxx - this->metriculx;
		this->metricmaxheight = this->metriculy - this->metricminy;
	}
	else if ( tfw->getRefSys().getCoordinateType() == eGEOGRAPHIC )
	{
		this->ndigits = 7;

		this->metriculx = metricUL.y;
		this->metriculy = metricUL.x;

		this->metriclrx = metricLR.y;
		this->metriclry = metricLR.x;

		this->metricwidth = this->metriclrx - this->metriculx;
		this->metricheight = this->metriculy - this->metriclry;

		this->metricminx = metricULImg.y;
		this->metricmaxx = metricLRImg.y;

		this->metricminy = metricLRImg.x;
		this->metricmaxy = metricULImg.x;

		this->metricmaxwidth = this->metricmaxx - this->metriculx;
		this->metricmaxheight = this->metriculy - this->metricminy;
	}
}

void Bui_CropROIDlg::updateROI()
{
	
	C2DPoint firstclick(this->ulx, this->uly);

	double newsecondx = this->lrx;
	if ( newsecondx > this->maxx ) newsecondx =  this->maxx;

	double newsecondy = this->lry;
	if ( newsecondy > this->maxy ) newsecondy =  this->maxy;

	C2DPoint secondclick(newsecondx, newsecondy);

	this->view->setFirstClick(firstclick);
	this->view->setSecondClick(secondclick);

	this->view->selectROI();
	this->view->update();
}

void Bui_CropROIDlg::getROIfromMetrictoPixel()
{
	const CTFW* tfw = view->getTFW();

	if (!tfw)
		return;
	
	CXYZPoint metricUL(this->metriculx, this->metriculy, 0);
	CXYZPoint metricLR(this->metriclrx, this->metriclry, 0);

	if ( tfw->getRefSys().getCoordinateType() == eGEOGRAPHIC )
	{
		metricUL.x = this->metriculy;
		metricUL.y = this->metriculx;

		metricLR.x = this->metriclry;
		metricLR.y = this->metriclrx;
	}

	CXYPoint UL;
	CXYPoint LR;

	tfw->transformObj2Obs(UL, metricUL);
	tfw->transformObj2Obs(LR, metricLR);

	this->ulx = UL.x;
	this->uly = UL.y;

	this->lrx = LR.x;
	this->lry = LR.y;

	this->width = this->lrx - this->ulx;
	this->height = this->lry - this->uly;

	this->updateROI();
}

void Bui_CropROIDlg::blockSpinBoxSignals(bool block)
{
	this->spinBoxULX->blockSignals(block);
	this->spinBoxULY->blockSignals(block);
	this->spinBoxLRX->blockSignals(block);
	this->spinBoxLRY->blockSignals(block);
	this->spinBoxHeight->blockSignals(block);
	this->spinBoxWidth->blockSignals(block);
};


void Bui_CropROIDlg::setSpinBoxDecimals(int decimals)
{
	this->spinBoxULX->setDecimals(decimals);
	this->spinBoxULY->setDecimals(decimals);
	this->spinBoxLRX->setDecimals(decimals);
	this->spinBoxLRY->setDecimals(decimals);
	this->spinBoxHeight->setDecimals(decimals);
	this->spinBoxWidth->setDecimals(decimals);
};

void Bui_CropROIDlg::setSpinBoxStep(double step)
{
	this->spinBoxULX->setSingleStep(step);
	this->spinBoxULY->setSingleStep(step);
	this->spinBoxLRX->setSingleStep(step);
	this->spinBoxLRY->setSingleStep(step);
	this->spinBoxHeight->setSingleStep(step);
	this->spinBoxWidth->setSingleStep(step);
};
