#include "XYEllipseTable.h"


#include "XYEllipseArray.h"
#include <QMessageBox>
#include <QMenu>
#include <QCloseEvent>


CXYEllipseTable::CXYEllipseTable(CXYEllipseArray* ellipses,QWidget* parent) : CBaristaTable(parent),ellipses(ellipses)
{
	this->initTable();
    ellipses->addListener(this);

	bool b = this->connect(this, SIGNAL( cellChanged(int, int)), this, SLOT(changedValue(int, int)));
}

CXYEllipseTable::~CXYEllipseTable(void)
{
}

void CXYEllipseTable::closeEvent ( QCloseEvent * e )
{
    this->ellipses->removeListener(this);
    e->accept();
}


void CXYEllipseTable::initTable()
{
	CCharString str("XYEllipses:   ");
	str = str + this->ellipses->getFileName()->GetFullFileName();
	this->setWindowTitle(str.GetChar());
	this->setAccessibleName(this->ellipses->getFileName()->GetChar());

	this->setColumnCount(6);
	this->setRowCount(this->ellipses->GetSize());

	for (int i=0; i < this->ellipses->GetSize(); i++)
		this->setRowHeight(i,20);

	this->setColumnWidth(0, 100);
	this->setColumnWidth(1, 80);
	this->setColumnWidth(2, 80);
	this->setColumnWidth(3, 80);
	this->setColumnWidth(4, 80);
	this->setColumnWidth(5, 80);

	QStringList headerList;

	headerList.append(QString("Label" ));
	headerList.append(QString("a" ));
	headerList.append(QString("b" ));
	headerList.append(QString("theta" ));
	headerList.append(QString("X0" ));
	headerList.append(QString("Y0" ));

	this->setHorizontalHeaderLabels(headerList);
   
    CLabel label = "";
    double a;
    double b;
    double theta;
	double x0;
    double y0;

	QTableWidgetItem* item;
 
    for ( int i = 0; i < this->ellipses->GetSize(); ++i )
    {
		CXYEllipse* ellipse = this->ellipses->GetAt(i);

		QString label = ellipse->getLabel().GetChar();

		a = ellipse->getA();
        b = ellipse->getB();
        theta = ellipse->getTheta();
        x0 = ellipse->getX0();
        y0 = ellipse->getY0();

		item = this->createItem(label,(Qt::ItemFlags)35,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(i,0,item);

		item = this->createItem(QString( "%1" ).arg(a, 0, 'f', 3 ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,1,item);

		item = this->createItem(QString( "%1" ).arg(b, 0, 'f', 3 ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,2,item);

		item = this->createItem(QString( "%1" ).arg(theta, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,3,item);

		item = this->createItem(QString( "%1" ).arg(x0, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,4,item);

		item = this->createItem(QString( "%1" ).arg(y0, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,5,item);
    }
}


void CXYEllipseTable::setEllipses(CXYEllipseArray* ellipses)
{
    this->ellipses = ellipses;
    ellipses->addListener(this);
}


void CXYEllipseTable::elementAdded(CLabel element)
{
	this->blockSignals(true);

	int size = this->ellipses->GetSize();

    if (size < 0)
        return;

	this->setRowCount(size);

	int index = size -1;
	this->setRowHeight(index,20);

	QTableWidgetItem* item;

	CXYEllipse* ellipse = this->ellipses->GetAt(index);

	QString label = ellipse->getLabel().GetChar();
	double a = ellipse->getA();
	double b = ellipse->getB();
	double theta = ellipse->getTheta();
	double x0 = ellipse->getX0();
	double y0 = ellipse->getY0();

	item = this->createItem(label,(Qt::ItemFlags)35,Qt::AlignLeft,QColor(0,0,0));
	this->setItem(index,0,item);

	item = this->createItem(QString( "%1" ).arg(a, 0, 'f', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,1,item);

	item = this->createItem(QString( "%1" ).arg(b, 0, 'f', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,2,item);

	item = this->createItem(QString( "%1" ).arg(theta, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,3,item);

	item = this->createItem(QString( "%1" ).arg(x0, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,4,item);

	item = this->createItem(QString( "%1" ).arg(y0, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,5,item);
	    
	this->blockSignals(false);	    
}

void CXYEllipseTable::elementDeleted(CLabel element)
{
	this->updateContents();
}


void CXYEllipseTable::changedValue( int row, int col )
{
	if ( col == 0)
	{
		CLabel clabel = this->getItemAsLabel(row, col);

		// if label is empty or alreday exists keep old label
		if (clabel.GetLength() == 0 || this->ellipses->getEllipse(clabel.GetChar()))
		{
			this->setLabelAsItemText(this->ellipses->GetAt(row)->getLabel(), row, col);
			
			QMessageBox::warning( this, "Change Label not possible!"
			, "This label already exist. Choose different Label!");
		}
		// if label is not empty and not existing yet accept new label
		else
		{
			this->ellipses->GetAt(row)->setLabel( clabel);
			this->setLabelAsItemText(clabel, row, col);
		}
	}
}

void CXYEllipseTable::removefromTable()
{
	this->blockSignals(true);
	int size = this->rowCount();

	int count=0;
	for ( int i =size -1; i >=0; i--)
	{
		if ( this->item(i, 0)->isSelected())
		{
			count++;
		}
		else
		{
			if (count) 
				this->ellipses->RemoveAt(i+1,count);
			count =0;
		}
	}  
	if (count) 
		this->ellipses->RemoveAt(0,count);
    
  
	this->updateContents();
	this->blockSignals(false);
}

void CXYEllipseTable::contextMenuEvent(QContextMenuEvent* e)
{
    QAction *deleteAction = new QAction(tr("&Delete Ellipse(s)"), this);
    connect(deleteAction, SIGNAL(triggered()), this, SLOT(removefromTable()));

	QMenu* contextMenu = new QMenu( this );
	Q_CHECK_PTR( contextMenu );
     
	contextMenu->addAction(deleteAction);

	contextMenu->exec( QCursor::pos() );

	delete deleteAction;
	delete contextMenu;
}