#include <float.h> 

#include "Bui_GenerateRPC.h"

#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "SystemUtilities.h"

#include "BaristaProject.h"

Bui_GenerateRPC::Bui_GenerateRPC(QWidget *parent)	: QDialog(parent),
					success(false), zMin(0), zMax(100)
{
	setupUi(this);
	this->success = false;

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	connect(this->buttonOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	connect(this->buttonCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	connect(this->buttonHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));
	connect(this->ZMinSpinBox,SIGNAL(valueChanged ( double ) ),this,SLOT(pBZMinChanged()));
	connect(this->ZMaxSpinBox,SIGNAL(valueChanged ( double ) ),this,SLOT(pBZMaxChanged()));

	this->setDefaults();
}

Bui_GenerateRPC::~Bui_GenerateRPC()
{
}


void Bui_GenerateRPC::setDefaults()
{
	 this->zMin =  FLT_MAX;
	 this->zMax = -FLT_MAX;

	 for (int i = 0; i < project.getNumberOfDEMs(); ++i)
	{
		CVDEM *dem = project.getDEMAt(i);
		double z = dem->getDEM()->getminZ();
		if (z < this->zMin) this->zMin = z;
		if (z > this->zMax) this->zMax = z;
		z = dem->getDEM()->getmaxZ();
		if (z < this->zMin) this->zMin = z;
		if (z > this->zMax) this->zMax = z;
	}

	if (this->zMin > this->zMax)
	{
		this->zMin = project.getMeanHeight() - 250.0;
		if (this->zMin < -9999.0) this->zMin = 0.0;
		this->zMax = this->zMin + 500.0;
	}

	this->applySettings();
};

void Bui_GenerateRPC::applySettings()
{
	if (this->zMin <= -9999.0)  this->zMin = -9999.0;
	if (this->zMax >= 10000.0)  this->zMax =  9999.0;

	if (this->zMax < this->zMin)
	{
		if (this->zMax <= -9000.0)  this->zMax = -9000.0;
		this->zMin = this->zMax - 100.0;
	}

	if((this->zMax - this->zMin) < 100)
	{
		this->zMin = (this->zMax + this->zMin) * 0.5f - 50.0;
		this->zMax = this->zMin + 100.0;
	}
	
	this->ZMinSpinBox->setValue(this->zMin);
	this->ZMaxSpinBox->setValue(this->zMax);
}

void Bui_GenerateRPC::pBOkReleased()
{
	this->success= true;
	this->close();
}

void Bui_GenerateRPC::pBCancelReleased()
{
	this->success = false;
	this->close();
}

void Bui_GenerateRPC::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/ThumbNodePushBroomScanner.html#GenerateRPCs");
}

void Bui_GenerateRPC::pBZMinChanged()
{
	this->zMin = this->ZMinSpinBox->value();
	this->applySettings();
}


void Bui_GenerateRPC::pBZMaxChanged()
{
	this->zMax = this->ZMaxSpinBox->value();
	this->applySettings();
}
