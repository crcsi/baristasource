#include "Bui_ProtocolViewer.h"
#include <QFileDialog>
#include <QFile>
#include <QScrollBar>
#include <QTextStream>
#include "BaristaProject.h"
#include "BaristaProjectHelper.h"

Bui_ProtocolViewer::Bui_ProtocolViewer(CFileName protocolFile, QWidget *parent)	: QDialog(parent),
	filename(protocolFile)
{
	baristaProjectHelper.setIsBusy(true);
	this->setupUi(this);
	this->setAttribute(Qt::WA_DeleteOnClose,true);

	this->textBrowser->setReadOnly(true);
	this->textBrowser->setFont(QFont("Courier"));
	this->textBrowser->setLineWrapMode(QTextEdit::NoWrap);

     QFile file(this->filename.GetChar());
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	 {
		 baristaProjectHelper.setIsBusy(false);
		 return;
	 }

	QTextStream in(&file);
	QString text = in.readAll();
	this->textBrowser->setText(text);

	this->lE_File->setText(this->filename.GetChar());
	this->lE_File->setToolTip(this->filename.GetChar());

	this->connect(this->pB_Save,SIGNAL(released()),this,SLOT(pBSaveAsreleased()));
	QTextCursor cursor = this->textBrowser->textCursor();
	cursor.movePosition(QTextCursor::Start);
	this->textBrowser->setTextCursor(cursor);
	baristaProjectHelper.setIsBusy(false);
}


Bui_ProtocolViewer::~Bui_ProtocolViewer()
{
}

void Bui_ProtocolViewer::pBSaveAsreleased()
{
	QString newProtocolName = QFileDialog::getSaveFileName(this,"Save Protocol as..",this->filename.GetPath().GetChar(),"*.*");

	if (!newProtocolName.isEmpty())
	{
		if (QFile::exists(newProtocolName))
			QFile::remove(newProtocolName);

		if (QFile::copy(this->filename.GetChar(),newProtocolName))
		{
			this->filename = newProtocolName.toLatin1().constData();
			this->lE_File->setText(this->filename.GetChar());
			this->lE_File->setToolTip(this->filename.GetChar());

			this->informListeners_elementsChanged();
		}
		
	}
}



void Bui_ProtocolViewer::reloadProtocolFile()
{
	baristaProjectHelper.setIsBusy(true);

	this->lE_File->setText(this->filename.GetChar());
	this->lE_File->setToolTip(this->filename.GetChar());

     QFile file(this->filename.GetChar());
     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	 {
		 baristaProjectHelper.setIsBusy(false);
		 return;
	 }

	QTextStream in(&file);
	QString text = in.readAll();
	this->textBrowser->setText(text);
	baristaProjectHelper.setIsBusy(false);

	int bottom = this->textBrowser->verticalScrollBar()->maximum();

	this->textBrowser->verticalScrollBar()->setValue(bottom);

	this->raise();
}

void Bui_ProtocolViewer::closeEvent(QCloseEvent *event)
{
	this->informListeners_elementDeleted();
}
