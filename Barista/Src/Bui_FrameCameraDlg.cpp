#include "Bui_FrameCameraDlg.h"

#include <QLineEdit>
#include <QHeaderView>

#include "FrameCamera.h"
#include "FrameCameraAnalogue.h"
#include "utilities.h"
#include "CameraFactory.h"
#include "EditorDelegate.h"
#define ROW_HEIGHT 20


Bui_FrameCameraDlg::Bui_FrameCameraDlg(CFrameCamera* existingCamera,QWidget *parent): 
	CAdjustmentDlg(parent), success(false),selectedDistortionModel(-1),radialModel(0),
	fiducialModel(0),currentCamera(0),existingCamera(existingCamera)
{
	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->connect(this->pB_AddDistortion, SIGNAL(released()),this,SLOT(pbAddDistortionReleased()));
	this->connect(this->pB_DeleteDistortion, SIGNAL(released()),this,SLOT(pbDeleteDistortionReleased()));
	this->connect(this->pB_AddFiducial, SIGNAL(released()),this,SLOT(pbAddFiducialReleased()));
	this->connect(this->pB_DeleteFiducial, SIGNAL(released()),this,SLOT(pbDeleteFiducialReleased()));
	this->connect(this->rB_Digtal, SIGNAL(released()),this,SLOT(rbDigitalReleased()));
	this->connect(this->rB_Analoge, SIGNAL(released()),this,SLOT(rbAnalogueReleased()));
	


	this->connect(this->lE_XAuto,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_YAuto,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_XSymmetry,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_YSymmetry,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));

	this->connect(this->lE_FocalLength,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_PixelSize,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_name,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_ID,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));

	this->connect(this->dE_Date,SIGNAL(dateChanged(const QDate &)),this,SLOT(deDateDateChanged(const QDate &)));
	this->connect(this->lW_Models,SIGNAL(itemSelectionChanged()),this,SLOT(modelSelectionChanged()));

	this->connect(this->lE_XAuto,SIGNAL(editingFinished()),this,SLOT(leXAutoEditingFinished()));
	this->connect(this->lE_YAuto,SIGNAL(editingFinished()),this,SLOT(leYAutoEditingFinished()));
	this->connect(this->lE_XSymmetry,SIGNAL(editingFinished()),this,SLOT(leXSymmetryEditingFinished()));
	this->connect(this->lE_YSymmetry,SIGNAL(editingFinished()),this,SLOT(leYSymmetryEditingFinished()));

	this->connect(this->lE_FocalLength,SIGNAL(editingFinished()),this,SLOT(leFocalLengthEditingFinished()));
	this->connect(this->lE_PixelSize,SIGNAL(editingFinished()),this,SLOT(lePixelSizeEditingFinished()));
	this->connect(this->lE_ID,SIGNAL(editingFinished()),this,SLOT(leIDEditingFinished()));
	this->connect(this->lE_name,SIGNAL(editingFinished()),this,SLOT(leNameEditingFinished()));
	this->connect(this->dE_Date,SIGNAL(editingFinished()),this,SLOT(deDateEditingFinished()));


	this->lE_FocalLength->setValidator(new QDoubleValidator(this));
	this->lE_XAuto->setValidator(new QDoubleValidator(this));
	this->lE_YAuto->setValidator(new QDoubleValidator(this));	
	this->lE_XSymmetry->setValidator(new QDoubleValidator(this));
	this->lE_YSymmetry->setValidator(new QDoubleValidator(this));	
	
	this->lE_PixelSize->setValidator(new QDoubleValidator(this));	

	this->fiducialModel = new CFiducialDataModel();

	this->tV_FiducialMarks->setModel(this->fiducialModel);
	this->tV_FiducialMarks->horizontalHeader()->setStretchLastSection(true);
	this->tV_FiducialMarks->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	this->tV_FiducialMarks->setItemDelegateForColumn(0,new CEditorDelegate(this,CEditorDelegate::eNoValidator));
	this->tV_FiducialMarks->setItemDelegateForColumn(1,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
	this->tV_FiducialMarks->setItemDelegateForColumn(2,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));



	QListWidgetItem* item = new QListWidgetItem("No Distortion",0,0);
	this->lW_Models->addItem(item);

	item = new QListWidgetItem("Radial Distortion",0,1);
	this->lW_Models->addItem(item);

	this->lW_Models->itemAt(0,0)->setSelected(true);
	
	// init the digital camera
	if (this->existingCamera == NULL)
	{
		this->rB_Digtal->setChecked(true);
		this->digitalCamera.setName("New Camera");
		this->rbDigitalReleased();
	}
	else
	{
		if (this->existingCamera->getCameraType() == eDigitalFrame)
		{
			this->digitalCamera = *this->existingCamera;
			this->currentCamera = &this->digitalCamera;
			this->rB_Digtal->setChecked(true);
			this->rB_Analoge->setEnabled(false);
			this->rbDigitalReleased();
		}
		else if (this->existingCamera->getCameraType() == eAnalogueFrame)
		{
			this->analogueCamera = *(CFrameCameraAnalogue*)this->existingCamera;
			this->currentCamera = (CFrameCamera*)(&this->analogueCamera);
			this->rB_Analoge->setChecked(true);
			this->rB_Digtal->setEnabled(false);
			this->rbAnalogueReleased();
		}

		this->existingCamera->addListener(this);
	}

	this->connect(this->tV_FiducialMarks->verticalHeader(),SIGNAL(sectionClicked(int)),this,SLOT(tVFiducialMarksHeaderClicked(int )));
	this->connect(this->tV_FiducialMarks,SIGNAL(clicked(const QModelIndex&)),this,SLOT(tVFiducialMarksClicked(const QModelIndex&)));

	this->applySettings();
}



Bui_FrameCameraDlg::~Bui_FrameCameraDlg(void)
{
	this->cleanUp();
}

void Bui_FrameCameraDlg::cleanUp()
{
	if (this->existingCamera)
		this->existingCamera->removeListener(this);
	this->existingCamera = NULL;

	if (this->fiducialModel)
		delete this->fiducialModel;
	this->fiducialModel = NULL;
}


void  Bui_FrameCameraDlg::applySettings()
{
	int digits1 = 4;
	int digits2 = 4;
	int digits3 = 7;

	if (this->currentCamera)
	{
		this->lE_XAuto->setText(QString("%1").arg(this->currentCamera->getIRP().x,0,'f',digits1));
		this->lE_YAuto->setText(QString("%1").arg(this->currentCamera->getIRP().y,0,'f',digits1));
		this->lE_XSymmetry->setText(QString("%1").arg(this->currentCamera->getIRPSymmetry().x,0,'f',digits1));
		this->lE_YSymmetry->setText(QString("%1").arg(this->currentCamera->getIRPSymmetry().y,0,'f',digits1));
		this->lE_FocalLength->setText(QString("%1").arg(this->currentCamera->getIRP().z,0,'f',digits1));  // we use the negative focal length

		this->lE_name->setText(QString(this->currentCamera->getName().GetChar()));
		this->dE_Date->setDate(QDate(this->currentCamera->getCalibrationYear(),
									 this->currentCamera->getCalibrationMonth(),
									 this->currentCamera->getCalibrationDay()));
		this->lE_PixelSize->setText(QString("%1").arg(this->currentCamera->getScale(),0,'f',digits1));
		this->lE_ID->setText(QString(this->currentCamera->getObjectiveName().GetChar()));

	}

	this->checkAvailabilityOfSettings();




}

void  Bui_FrameCameraDlg::checkAvailabilityOfSettings()
{


	if (this->rB_Analoge->isChecked())
	{
		this->tV_FiducialMarks->setEnabled(true);
		this->pB_AddFiducial->setEnabled(true);
		this->pB_DeleteFiducial->setEnabled(true);
	}
	else
	{
		this->tV_FiducialMarks->setEnabled(false);
		this->pB_AddFiducial->setEnabled(false);
		this->pB_DeleteFiducial->setEnabled(false);
	}

	if (this->rB_Digtal->isChecked())
	{
		this->lE_PixelSize->setEnabled(true);
		this->tV_FiducialMarks->clearSelection();
	}
	else
		this->lE_PixelSize->setEnabled(false);

	if (this->selectedDistortionModel == 0)
	{
		this->pB_AddDistortion->setEnabled(false);
		this->pB_DeleteDistortion->setEnabled(false);
		this->tV_Distortion->setEnabled(false);
	}
	else
	{
		this->pB_AddDistortion->setEnabled(true);
		this->pB_DeleteDistortion->setEnabled(true);
		this->tV_Distortion->setEnabled(true);
	}

	QItemSelectionModel* selectionModel =this->tV_Distortion->verticalHeader()->selectionModel();
	if (selectionModel && selectionModel->selectedRows().count())
		this->pB_DeleteDistortion->setEnabled(true);
	else
		this->pB_DeleteDistortion->setEnabled(false);


	selectionModel =this->tV_FiducialMarks->verticalHeader()->selectionModel();
	if (selectionModel && selectionModel->selectedRows().count())
		this->pB_DeleteFiducial->setEnabled(true);
	else
		this->pB_DeleteFiducial->setEnabled(false);


	if (this->lE_name->text().isEmpty() ||
		!this->lE_XAuto->hasAcceptableInput() || !this->lE_YAuto->hasAcceptableInput() ||
		!this->lE_FocalLength->hasAcceptableInput() || !this->lE_PixelSize->hasAcceptableInput())
		emit enableOkButton(false);
	else
		emit enableOkButton(true);

}


bool Bui_FrameCameraDlg::cancelButtonReleased()
{
	this->success = false;
	return true;
}

CCharString Bui_FrameCameraDlg::helpButtonReleased()
{
	return "UserGuide/ThumbNodeFrameCamera.html";
}

bool Bui_FrameCameraDlg::okButtonReleased()
{
	if (this->existingCamera)
		CCameraFactory::copyCamera(this->existingCamera,this->currentCamera);

	this->success = true;
	return true;
}

void Bui_FrameCameraDlg::pbAddDistortionReleased()
{
	this->tV_Distortion->blockSignals(true);

	QStandardItemModel* model = (QStandardItemModel*)this->tV_Distortion->model();

	int row = model->rowCount();
	model->insertRow(row);
	this->tV_Distortion->setRowHeight(row,ROW_HEIGHT);
	this->tV_Distortion->blockSignals(false);
	
}

void Bui_FrameCameraDlg::pbDeleteDistortionReleased()
{

	QItemSelectionModel* selectionModel =this->tV_Distortion->selectionModel();

	if (selectionModel && selectionModel->selectedRows().count())
	{
		QModelIndexList list = selectionModel->selectedRows();
		for (int i = list.count() -1; i >= 0; i--)
		{
			this->tV_Distortion->model()->removeRow(list.at(i).row());
		}

	}


}

void Bui_FrameCameraDlg::pbAddFiducialReleased()
{
	int row = this->fiducialModel->rowCount(QModelIndex());
	this->fiducialModel->insertRow(row);
	this->tV_FiducialMarks->setRowHeight(row,ROW_HEIGHT);



}

void Bui_FrameCameraDlg::pbDeleteFiducialReleased()
{
	QItemSelectionModel* selectionModel =this->tV_FiducialMarks->selectionModel();

	if (selectionModel && selectionModel->selectedRows().count())
	{
		QModelIndexList list = selectionModel->selectedRows();
		for (int i = list.count() -1; i >= 0; i--)
		{
			this->fiducialModel->removeRow(list.at(i).row());
		}

	}


}

void Bui_FrameCameraDlg::leTextChanged(const QString & text)
{
	this->checkAvailabilityOfSettings();
}


void Bui_FrameCameraDlg::deDateDateChanged ( const QDate & date )
{
	this->checkAvailabilityOfSettings();
}

void Bui_FrameCameraDlg::rbDigitalReleased()
{
	
	this->fiducialModel->setCamera(0);
	
	this->currentCamera = &this->digitalCamera;	
	this->checkAvailabilityOfSettings();
}

void Bui_FrameCameraDlg::rbAnalogueReleased()
{
	
	this->currentCamera = (CFrameCameraAnalogue*)(&this->analogueCamera);
		
	this->fiducialModel->setCamera(&this->analogueCamera);

	for (int i=0; i<this->fiducialModel->rowCount(QModelIndex()); i++)
		this->tV_FiducialMarks->setRowHeight(i,ROW_HEIGHT);

	this->checkAvailabilityOfSettings();
}


void Bui_FrameCameraDlg::leXAutoEditingFinished()
{
	if (this->currentCamera)
	{
		CXYZPoint oldIRP = this->currentCamera->getIRP();
		CXYZPoint irp(this->lE_XAuto->text().toDouble(),
					  oldIRP.y,
					  oldIRP.z);
		
		this->digitalCamera.setIRP(irp);
		this->analogueCamera.setIRP(irp);
	}
	
	this->applySettings();
}

void Bui_FrameCameraDlg::leYAutoEditingFinished()
{
	if (this->currentCamera)
	{
		CXYZPoint oldIRP = this->currentCamera->getIRP();
		CXYZPoint irp(oldIRP.x,
					  this->lE_YAuto->text().toDouble(),
					  oldIRP.z);
		
		this->digitalCamera.setIRP(irp);
		this->analogueCamera.setIRP(irp);
	}
	
	this->applySettings();
}

void Bui_FrameCameraDlg::leXSymmetryEditingFinished()
{
	if (this->currentCamera)
	{
		CXYPoint oldIRP = this->currentCamera->getIRPSymmetry();
		CXYPoint irp(this->lE_XSymmetry->text().toDouble(),
					  oldIRP.y);
		
		this->digitalCamera.setIRPSymmetry(irp);
		this->analogueCamera.setIRPSymmetry(irp);
	}
	
	this->applySettings();
}

void Bui_FrameCameraDlg::leYSymmetryEditingFinished()
{
	if (this->currentCamera)
	{
		CXYPoint oldIRP = this->currentCamera->getIRPSymmetry();
		CXYPoint irp(oldIRP.x,
					  this->lE_YSymmetry->text().toDouble());
		
		this->digitalCamera.setIRPSymmetry(irp);
		this->analogueCamera.setIRPSymmetry(irp);
	}
	
	this->applySettings();
}

void Bui_FrameCameraDlg::leFocalLengthEditingFinished()
{
	if (this->currentCamera)
	{
		CXYZPoint oldIRP = this->currentCamera->getIRP();
		CXYZPoint irp(oldIRP.x,
					  oldIRP.y,
					  this->lE_FocalLength->text().toDouble()); // we use the negative focal length
		
		this->digitalCamera.setIRP(irp);
		this->analogueCamera.setIRP(irp);
	}
	
	this->applySettings();
}

void Bui_FrameCameraDlg::leNameEditingFinished()
{
	this->digitalCamera.setName(this->lE_name->text().toLatin1().constData());
	this->analogueCamera.setName(this->lE_name->text().toLatin1().constData());
	this->applySettings();
}

void Bui_FrameCameraDlg::leIDEditingFinished()
{
	this->digitalCamera.setObjectiveName(this->lE_ID->text().toLatin1().constData());
	this->analogueCamera.setObjectiveName(this->lE_ID->text().toLatin1().constData());
	this->applySettings();
}

void Bui_FrameCameraDlg::deDateEditingFinished()
{
	QDate date = this->dE_Date->date();
	this->digitalCamera.setCalibrationDay(date.day());
	this->digitalCamera.setCalibrationMonth(date.month());
	this->digitalCamera.setCalibrationYear(date.year());
	
	this->analogueCamera.setCalibrationDay(date.day());
	this->analogueCamera.setCalibrationMonth(date.month());
	this->analogueCamera.setCalibrationYear(date.year());
	this->applySettings();
}

void Bui_FrameCameraDlg::lePixelSizeEditingFinished()
{
	this->digitalCamera.setScale(this->lE_PixelSize->text().toDouble());
	
	this->applySettings();
}




void Bui_FrameCameraDlg::modelSelectionChanged()
{

	QList<QListWidgetItem*> list = this->lW_Models->selectedItems();
	
	if (list.size() != 1)
		return;

	this->selectedDistortionModel = list.at(0)->type();
	

	if (this->selectedDistortionModel == 0)
		this->setNoDistortionModel();
	else if (this->selectedDistortionModel == 1)
		this->setRadialDistortionModel();

	this->checkAvailabilityOfSettings();
}

void Bui_FrameCameraDlg::setNoDistortionModel()
{
	this->tV_Distortion->setModel(0);
	this->checkAvailabilityOfSettings();
}

void Bui_FrameCameraDlg::setRadialDistortionModel()
{

	// init the model
	if (!this->radialModel)
	{
		this->radialModel = new QStandardItemModel(this);
		this->radialModel->setColumnCount(5);

		this->radialModel->setHorizontalHeaderItem(0,new QStandardItem("r"));
		this->radialModel->setHorizontalHeaderItem(1,new QStandardItem("I"));
		this->radialModel->setHorizontalHeaderItem(2,new QStandardItem("II"));
		this->radialModel->setHorizontalHeaderItem(3,new QStandardItem("III"));
		this->radialModel->setHorizontalHeaderItem(4,new QStandardItem("IV"));

		this->tV_Distortion->horizontalHeader()->setStretchLastSection(true);
		this->tV_Distortion->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
		this->tV_Distortion->setItemDelegateForColumn(0,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
		this->tV_Distortion->setItemDelegateForColumn(1,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
		this->tV_Distortion->setItemDelegateForColumn(2,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
		this->tV_Distortion->setItemDelegateForColumn(3,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
		this->tV_Distortion->setItemDelegateForColumn(4,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
		
		this->connect(this->tV_Distortion->verticalHeader(),SIGNAL(sectionClicked(int)),this,SLOT(tVDistortionHeaderClicked(int )));
		this->connect(this->tV_Distortion,SIGNAL(clicked(const QModelIndex&)),this,SLOT(tVDistortionHeaderClicked(const QModelIndex&)));


	}

	this->tV_Distortion->setModel(this->radialModel);

	int rows = this->radialModel->rowCount();
	for (int i=0 ; i < rows; i++)
		this->tV_Distortion->setRowHeight(i,ROW_HEIGHT);


	
}


void Bui_FrameCameraDlg::tVDistortionHeaderClicked(int logicalIndex  )
{
	QItemSelectionModel* selectionModel =this->tV_Distortion->verticalHeader()->selectionModel();
	if (selectionModel && selectionModel->selectedRows().count())
		this->pB_DeleteDistortion->setEnabled(true);
	else
		this->pB_DeleteDistortion->setEnabled(false);
}

void Bui_FrameCameraDlg::tVDistortionHeaderClicked(const QModelIndex& index )
{
	this->pB_DeleteDistortion->setEnabled(false);
}

void Bui_FrameCameraDlg::tVFiducialMarksHeaderClicked(int logicalIndex  )
{
	QItemSelectionModel* selectionModel =this->tV_FiducialMarks->verticalHeader()->selectionModel();
	if (selectionModel && selectionModel->selectedRows().count())
		this->pB_DeleteFiducial->setEnabled(true);
	else
		this->pB_DeleteFiducial->setEnabled(false);
}

void Bui_FrameCameraDlg::tVFiducialMarksClicked(const QModelIndex& index )
{
	this->pB_DeleteFiducial->setEnabled(false);
}


void Bui_FrameCameraDlg::elementsChangedSlot(CLabel element)
{
	if (this->existingCamera)
	{
		CCameraFactory::copyCamera(this->currentCamera,this->existingCamera);
		this->applySettings();
	}
}





//###########################################################################################

void CFiducialDataModel::setCamera(CFrameCameraAnalogue* camera)
{
	this->beginResetModel();
	this->camera = camera;
	this->endResetModel();
}

int CFiducialDataModel::rowCount(const QModelIndex& parent) const
{
	if (this->camera)
		return this->camera->getFiducialMarks()->GetSize();
	else
		return 0;
}

int CFiducialDataModel::columnCount(const QModelIndex& parent) const
{
	return 3;
}

QVariant CFiducialDataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role == Qt::TextAlignmentRole)
		return int(Qt::AlignRight | Qt::AlignVCenter);
	else if (role == Qt::DisplayRole || role == Qt::EditRole)
	{
		const CXYPoint* point = this->camera->getFiducialMarks()->getAt(index.row());
		if (index.column() == 0)
		{
			return QString("%1").arg(point->getLabel().GetChar());
		}
		else if (index.column() == 1)
		{
			if (role == Qt::DisplayRole)		
				return QString().sprintf("%.4lf",point->x);
			else
				return QString().sprintf("%lf",point->x);

		}
		else if (index.column() == 2)
		{
			if (role == Qt::DisplayRole)		
				return QString().sprintf("%.4lf",point->y);
			else
				return QString().sprintf("%lf",point->y);
		}
	}
	return QVariant();

	
}

QVariant CFiducialDataModel::headerData(int section,Qt::Orientation orientation, int role) const
{
	
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
	{
		if (section == 0)
			return "ID";
		else if (section == 1)
			return "x";
		else if (section == 2)
			return "y";
	}
	else if (role == Qt::DisplayRole && orientation == Qt::Vertical)
	{
		return QString("%1 %2").arg("Mark").arg(section+1);
	}

	return QVariant();

}


Qt::ItemFlags CFiducialDataModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
	 return Qt::ItemIsEnabled;

	return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool CFiducialDataModel::setData(const QModelIndex &index,const QVariant &value, int role)
{
	if (index.isValid() && role == Qt::EditRole) 
	{
		bool ret = false;
		if (index.column() == 0 && !value.isNull())
			ret =  this->camera->updateFiducialMarkLabel(value.toByteArray().constData(),index.row());
		else if (index.column() == 1 && !value.isNull())
			ret =  this->camera->updateFiducialMarkX(value.toDouble(),index.row());
		else if (index.column() == 2 && !value.isNull())
			ret =  this->camera->updateFiducialMarkY(value.toDouble(),index.row());

		if (ret)
			emit dataChanged(index, index);
		return ret;
	}

	return false;
}

bool CFiducialDataModel::insertRows(int position, int rows, const QModelIndex &parent)
{
	beginInsertRows(QModelIndex(), position, position+rows-1);

	int offset = 0;
	for (int i = 0; i < rows; i++) 
	{
		bool ret;
		do 
		{
			offset++;
			CLabel label = QString("%1").arg(position + i + offset).toLatin1().constData();
			CXYPoint point (0,0,label);
			ret = this->camera->addFiducialMark(point);
		}while (!ret);
	}

	endInsertRows();
	return true;
}

bool CFiducialDataModel::removeRows(int position, int rows, const QModelIndex &parent)
{
	beginRemoveRows(QModelIndex(), position, position+rows-1);

	for (int i = 0; i < rows; i++) 
	{
		this->camera->deleteFiducialMark(position + i);
	}

	endRemoveRows();
	return true;
}

