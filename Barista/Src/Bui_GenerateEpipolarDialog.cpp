//#include "newmat.h"

#include "Bui_GenerateEpipolarDialog.h"
#include <QFileDialog>
#include <QMessageBox> 

#include "BaristaProject.h"
#include "SystemUtilities.h"

#include "Bui_ImageSensorSelector.h"
#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include "GenerateEpipolarProject.h"

bui_GenerateEpipolarDialog::bui_GenerateEpipolarDialog (CVImage *left) : 
				leftImageSelector(0), rightImageSelector(0), leftImage(left), success(false)
{
	setupUi(this);

	connect(this->buttonOk,               SIGNAL(released()),               this, SLOT(OK()));
	connect(this->buttonCancel,           SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->HelpButton,             SIGNAL(released()),               this, SLOT(Help()));
	connect(this->LeftImageCombo,         SIGNAL(currentIndexChanged(int)), this, SLOT(onLeftImageChanged()));
	connect(this->RightImageCombo,        SIGNAL(currentIndexChanged(int)), this, SLOT(onRightImageChanged()));
	connect(this->BrowseButton,           SIGNAL(released()),               this, SLOT(onOutputPathPushed()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}

//================================================================================

bui_GenerateEpipolarDialog::~bui_GenerateEpipolarDialog ()
{
	this->destroy();
}

//================================================================================

void bui_GenerateEpipolarDialog::init()
{
    this->success = false;

	if (!this->leftImage) 
		this->leftImageSelector = new bui_ImageSensorSelector(this->LeftImageCombo,    0, eALLBANDS, eFRAMECAMONLY, project.getImageAt(0));
	else
		this->leftImageSelector = new bui_ImageSensorSelector(this->LeftImageCombo,    0, eALLBANDS, eFRAMECAMONLY, this->leftImage);

	this->leftImage = this->leftImageSelector->getSelectedImage();

	this->rightImage = 0;
	
	for (int i = 0; !this->rightImage && (i < project.getNumberOfImages()); ++i)
	{
		CVImage *img = project.getImageAt(i);
		if ((img->getFrameCamera()->gethasValues()) && (img != this->leftImage))
		{
			this->rightImage = img;
		}
	}

	this->rightImageSelector = new bui_ImageSensorSelector(this->RightImageCombo, 0, eALLBANDS, eALLSENSORS, this->rightImage);
	
	this->rightImage = this->rightImageSelector->getSelectedImage();

	this->OutputPath->blockSignals(true);

	if (this->leftImage) 
	{
		this->outputPath = project.getFileName()->GetPath();
		if (!this->outputPath.IsEmpty())
		{
			if (this->outputPath[this->outputPath.GetLength() - 1] != CSystemUtilities::dirDelim)
				this->outputPath += CSystemUtilities::dirDelimStr;
		}
	}

	this->OutputPath->setText(this->outputPath.GetChar());

	this->checkSetup();
}

//================================================================================
	  
void bui_GenerateEpipolarDialog::destroy()
{
	if (this->leftImageSelector) delete this->leftImageSelector;
	this->leftImageSelector = 0;

	if (this->rightImageSelector) delete this->rightImageSelector;
	this->rightImageSelector = 0;
}
	  
//================================================================================

void bui_GenerateEpipolarDialog::checkSetup()
{
	if ((this->leftImage) && (this->rightImage) && (this->leftImage != this->rightImage))
	{
		this->buttonOk->setEnabled(true);
	}
	else
	{
		this->buttonOk->setEnabled(false);
	}
}
	
//================================================================================
	  
void bui_GenerateEpipolarDialog::onLeftImageChanged()
{
	if (!this->leftImageSelector->reactOnImageSelect()) 
	{
		QMessageBox::warning( this, "No left image selected", " Generation of epipolar images will be impossible");
	};

	if (this->leftImageSelector->getSelectedImage()) 
	{
		this->leftImage = this->leftImageSelector->getSelectedImage();
	}
	else 
	{
		this->leftImage = 0;
	}

	this->checkSetup();
}

//================================================================================
	  
void bui_GenerateEpipolarDialog::onRightImageChanged()
{
  	if (!this->rightImageSelector->reactOnImageSelect()) 
	{
		QMessageBox::warning( this, "No right image selected", " Generation of epipolar images will be impossible");
	};

	if (this->rightImageSelector->getSelectedImage()) 
	{
		this->rightImage = this->rightImageSelector->getSelectedImage();
	}
	else 
	{
		this->rightImage = 0;
	}
	
	this->checkSetup();
}

//================================================================================

void bui_GenerateEpipolarDialog::onOutputPathPushed()
{
	QString dirQ = QFileDialog::getExistingDirectory (this, "Choose Output Directory",
		                                              this->outputPath.GetChar());

	CFileName direc = ((const char*)dirQ.toLatin1());
		
	if (!direc.IsEmpty())
	{
		this->outputPath = direc;
		if (this->outputPath[this->outputPath.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->outputPath += CSystemUtilities::dirDelimStr;

		this->OutputPath->setText(this->outputPath.GetChar());
	}
};

//================================================================================

void bui_GenerateEpipolarDialog::OK()
{
	this->buttonOk->setEnabled(false);
	this->buttonCancel->setEnabled(false);
	this->update();

	this->success = false;
	CGenerateEpipolarProject *pEpi = new CGenerateEpipolarProject(this->leftImage, this->rightImage, this->outputPath);
	
	if (pEpi) 
	{
		bui_ProgressDlg dlg(new GenerateEpipolarThread(pEpi), pEpi, "Generation of epipolar images");
		dlg.exec();
		this->success = dlg.getSuccess();
		delete pEpi;
	}

	if (!this->success)
	{		
		QMessageBox::critical(this, "Generation of epipolar images failed!", "Generation of epipolar images failed!");
	}

	this->close();
};

//================================================================================

void bui_GenerateEpipolarDialog::Cancel()
{
	this->success = false;
	this->close();
}

//================================================================================

void bui_GenerateEpipolarDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/GenerateEpipolarDlg.html");
};

//================================================================================
