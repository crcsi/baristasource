#include "EditorDelegate.h"
#include <QModelIndex>
#include <QLineEdit>
#include <QDoubleValidator>
#include <QIntValidator>

CEditorDelegate::CEditorDelegate(QObject* parent,CEditorDelegate::ValidatorType type,double minValue, double maxValue) : 
	QItemDelegate(parent),setMinMax(true),min(minValue),max(maxValue),type(type)
{

}

CEditorDelegate::CEditorDelegate(QObject* parent,CEditorDelegate::ValidatorType type) : 
	QItemDelegate(parent),setMinMax(false),min(0.0),max(0.0),type(type)
{

}

void CEditorDelegate::paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const
{
	QString text;
	QStyleOptionViewItem localOptions = option;
	localOptions.displayAlignment = Qt::AlignRight | Qt::AlignVCenter;

	text = QString(index.model()->data(index,Qt::DisplayRole).toString());

	drawDisplay(painter,localOptions,localOptions.rect,text);
	drawFocus(painter,localOptions,localOptions.rect);

}

QWidget *CEditorDelegate::createEditor(QWidget *parent,const QStyleOptionViewItem & option, const QModelIndex &index) const
{

	QLineEdit* edit = new QLineEdit(parent);
	
	if (type == CEditorDelegate::eDoubleValidator)
	{
		QDoubleValidator *v = NULL;
		
		if (this->setMinMax)
			v = new QDoubleValidator(min,max,20,parent);
		else 
			v = new QDoubleValidator(parent);

		edit->setValidator(v);

	}
	else if (type == CEditorDelegate::eIntValidator)
	{
		QIntValidator *v = NULL;
		
		if (this->setMinMax)
			v = new QIntValidator(int(min),int(max),parent);
		else 
			v = new QIntValidator(parent);

		edit->setValidator(v);
	}
	

	return edit;
}

void CEditorDelegate::setEditorData(QWidget * editor, const QModelIndex & index )const
{
	QLineEdit* edit = qobject_cast<QLineEdit*>(editor);
	edit->setText(index.model()->data(index,Qt::EditRole).toString());
}

void CEditorDelegate::setModelData ( QWidget * editor, QAbstractItemModel * model, const QModelIndex & index ) const
{
	QLineEdit* edit = qobject_cast<QLineEdit*>(editor);
	if (edit->hasAcceptableInput())
		 QItemDelegate::setModelData(editor,model,index);

}