#include "XYZPolyLineTable.h"


#include "XYZPolyLineArray.h"
#include <QMessageBox>
#include <QMenu>
#include <QCloseEvent>
#include "BaristaProject.h"
#include "BaristaProjectHelper.h"
#include "Bui_FeatureExtractionDialog.h"

CXYZPolyLineTable::CXYZPolyLineTable(CXYZPolyLineArray* lines,QWidget* parent) : CBaristaTable(parent),
	lines(lines),xdezimals(4),ydezimals(4)
{

	this->initTable();
    lines->addListener(this);

	bool b = this->connect(this, SIGNAL( cellChanged(int, int)), this, SLOT(changedValue(int, int)));
	this->connect(this, SIGNAL( itemSelectionChanged()), this, SLOT(itemSelectionChanged()));
}

CXYZPolyLineTable::~CXYZPolyLineTable(void)
{
}


void CXYZPolyLineTable::itemSelectionChanged ()
{
	QList<QTableWidgetItem *> list = this->selectedItems ();

	this->lines->unselectAll();

	for (int i=0; i < list.count(); i++)
		{
			CLabel l(lines->GetAt(list.at(i)->row())->getLabel());
			lines->selectElement(l,true);
		}

}


void CXYZPolyLineTable::closeEvent ( QCloseEvent * e )
{
    this->lines->removeListener(this);
    e->accept();
}


void CXYZPolyLineTable::initTable()
{
	CCharString str("XYZPolyLines:   ");
	str = str + this->lines->getFileName()->GetFullFileName();
	this->setWindowTitle(str.GetChar());
	this->setAccessibleName(this->lines->getFileName()->GetChar());


	this->setColumnCount(8);
	this->setRowCount(this->lines->GetSize());

	for (int i=0; i < this->lines->GetSize(); i++)
		this->setRowHeight(i,20);

	this->setColumnWidth(0, 150);
	this->setColumnWidth(1, 100);
	this->setColumnWidth(2, 80);
	this->setColumnWidth(3, 110);
	this->setColumnWidth(4, 110);
	this->setColumnWidth(5, 80);
	this->setColumnWidth(6, 70);
	this->setColumnWidth(7, 80);

	QStringList headerList;

    if ( this->lines->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)
    {
		this->xdezimals = 10;
		this->ydezimals = 10;

		headerList.append(QString("Label"));
		headerList.append(QString("Number of Points"));
		headerList.append(QString("Start Point" ));
        headerList.append(QString("Latitude" ));
        headerList.append(QString("Longitude" ));
        headerList.append(QString("Height" ));
		headerList.append(QString("Length" ));
		headerList.append(QString("Open/Closed" ));
    }
    else if ( this->lines->getReferenceSystem()->getCoordinateType() == eGEOCENTRIC)
    {
		headerList.append(QString("Label"));
		headerList.append(QString("Number of Points"));
		headerList.append(QString("Start Point" ));
        headerList.append(QString("X" ));
        headerList.append(QString("Y" ));
        headerList.append(QString("Z" ));
		headerList.append(QString("Length" ));
		headerList.append(QString("Open/Closed" ));
    }
    else if( this->lines->getReferenceSystem()->getCoordinateType() == eGRID)
    {
        CCharString p1("Label");

		if (this->lines->getReferenceSystem()->isUTM())
		{
			p1 += "(Zone ";
        
			CLabel p2;
			p2 += this->lines->getReferenceSystem()->getZone();

			p1 = p1 + p2.GetChar();

			if ( this->lines->getReferenceSystem()->getHemisphere() == SOUTHERN )
			{
				p1 = p1 + ", Southern)";   
			}
			else if ( this->lines->getReferenceSystem()->getHemisphere() == NORTHERN )
			{
				p1 = p1 + ", Northern)";
			}
		}
		else
		{
			p1 += "(Lat:";
			double lon = this->lines->getReferenceSystem()->getCentralMeridian();
			double lat = this->lines->getReferenceSystem()->getLatitudeOrigin();

			char buf[256];
			sprintf(buf, "%5.2f Lon:%5.2f)", lat, lon); 

			p1 = p1 + buf;
		}

		headerList.append(QString(p1.GetChar() ));
		headerList.append(QString("Number of Points"));
		headerList.append(QString("Start Point" ));
        headerList.append(QString("Easting" ));
        headerList.append(QString("Northing" ));
        headerList.append(QString("Height" ));
		headerList.append(QString("Length" ));
		headerList.append(QString("Open/Closed" ));
    }
	else
	{ // undefined coordinate type
		headerList.append(QString("Label"));
		headerList.append(QString("Number of Points"));
		headerList.append(QString("Start Point" ));
        headerList.append(QString("LocalX" ));
        headerList.append(QString("LocalY" ));
        headerList.append(QString("LocalZ" ));
		headerList.append(QString("Length" ));
		headerList.append(QString("Open/Closed" ));
	}

	this->setHorizontalHeaderLabels(headerList);

    CLabel label = "";
    double x;
    double y;
    double z;

	QTableWidgetItem* item;
    
    for ( int i = 0; i < this->lines->GetSize(); ++i )
    {
        QString llabel = this->lines->GetAt(i)->getLabel().GetChar();
		CXYZPolyLine* line = this->lines->getAt(i);

		double length = line->getLength();

		CObsPoint* point = line->getPoint(0);       
        x = (*point)[0];
        y = (*point)[1];
        z = (*point)[2];
		QString plabel = point->getLabel().GetChar();
		
		item = this->createItem(llabel,(Qt::ItemFlags)35,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(i,0,item);

		item = this->createItem(QString( "%1" ).arg(line->getPointCount()),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,1,item);

		item = this->createItem(plabel,0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,2,item);

		item = this->createItem(QString( "%1" ).arg(x, 0, 'f', this->xdezimals ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,3,item);

		item = this->createItem(QString( "%1" ).arg(y, 0, 'f', this->ydezimals ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,4,item);

		item = this->createItem(QString( "%1" ).arg(z, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,5,item);

		item = this->createItem(QString( "%1" ).arg(length, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,6,item);

		if (line->isClosed())
			item = this->createItem("Closed",(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		else
			item = this->createItem( "Open",(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));

		this->setItem(i,7,item);
    }
}


void CXYZPolyLineTable::setLines(CXYZPolyLineArray* lines)
{
    this->lines = lines;
    lines->addListener(this);
}


void CXYZPolyLineTable::elementAdded(CLabel element)
{
	this->blockSignals(true);

	int size = this->lines->GetSize();

    if (size < 0)
        return;

	this->setRowCount(size);

	int index = size -1;
	this->setRowHeight(index,20);

    CLabel label = "";
    double x;
    double y;
    double z;
	double length;

	QTableWidgetItem* item;

	QString llabel = this->lines->GetAt(index)->getLabel().GetChar();
	CXYZPolyLine* line = this->lines->getAt(index);

	if (line->getPointCount() == 0)
		return;

	length = line->getLength();

	CObsPoint* point = line->getPoint(0);       
	x = (*point)[0];
	y = (*point)[1];
	z = (*point)[2];
	QString plabel = point->getLabel().GetChar();

	item = this->createItem(llabel,(Qt::ItemFlags)35,Qt::AlignLeft,QColor(0,0,0));
	this->setItem(index,0,item);

	item = this->createItem(QString( "%1" ).arg(line->getPointCount()),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,1,item);

	item = this->createItem(plabel,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,2,item);

	item = this->createItem(QString( "%1" ).arg(x, 0, 'f', this->xdezimals ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,3,item);

	item = this->createItem(QString( "%1" ).arg(y, 0, 'f', this->ydezimals ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,4,item);

	item = this->createItem(QString( "%1" ).arg(z, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,5,item);

	item = this->createItem(QString( "%1" ).arg(length, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(index,6,item);

	if (line->isClosed())
		item = this->createItem("Closed",(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
	else
		item = this->createItem( "Open",(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));

	this->setItem(index,7,item);

	this->blockSignals(false);
	    
}

void CXYZPolyLineTable::elementDeleted(CLabel element)
{
	this->updateContents();
}



void CXYZPolyLineTable::changedValue( int row, int col )
{
	if ( col == 0)
	{
		CLabel clabel = this->getItemAsLabel(row, col);

		// if label is empty or alreday exists keep old label
		if (clabel.GetLength() == 0 || this->lines->getLine(clabel.GetChar()))
		{
			this->setLabelAsItemText(this->lines->GetAt(row)->getLabel(), row, col);
			
			QMessageBox::warning( this, "Change Label not possible!"
			, "This label already exist. Choose different Label!");
			
		}
		// if label is not empty and not existing yet accept new label
		else
		{
			this->lines->GetAt(row)->setLabel( clabel);
			this->setLabelAsItemText(clabel, row, col);
		}
	}
}

void CXYZPolyLineTable::removefromTable()
{
	this->blockSignals(true);
	int size = this->rowCount();

	int count=0;
	for ( int i =size -1; i >=0; i--)
	{
		if ( this->item(i, 0)->isSelected())
		{
			count++;
		}
		else
		{
			if (count) 
				if (project.removeMonoplottedLine(i+1,count))
					baristaProjectHelper.refreshTree();

			count =0;
		}
	}  
	if (count) 
		if (project.removeMonoplottedLine(0,count))
			baristaProjectHelper.refreshTree();
  
	this->updateContents();
	this->blockSignals(false);
}

int CXYZPolyLineTable::getNumberOfSelectedRows()
{
	int count = 0;

	for ( int i = 0; i < this->rowCount(); i++)
	{
		if ( this->item(i, 0)->isSelected()) count++;
	}  

	return count;
}

void CXYZPolyLineTable::contextMenuEvent(QContextMenuEvent* e)
{
    QAction *deleteAction = new QAction(tr("&Delete Lines(s)"), this);
    connect(deleteAction, SIGNAL(triggered()), this, SLOT(removefromTable()));

	QMenu* contextMenu = new QMenu( this );
	Q_CHECK_PTR( contextMenu );

	QAction *XYZPolyLinesExtractFeaturesAction = 0;

	if ((project.getNumberOfImages() > 0) && (getNumberOfSelectedRows() == 1))
	{
		XYZPolyLinesExtractFeaturesAction = new QAction(tr("&Extract Features"), this);
		connect(XYZPolyLinesExtractFeaturesAction, SIGNAL(triggered()), this, SLOT(XYZPolyLinesExtractFeatures()));
		this->addAction(XYZPolyLinesExtractFeaturesAction);
	}

	contextMenu->addAction(deleteAction);

	contextMenu->exec( QCursor::pos() );

	delete deleteAction;
	if (XYZPolyLinesExtractFeaturesAction) delete XYZPolyLinesExtractFeaturesAction;
	delete contextMenu;
}


void CXYZPolyLineTable::XYZPolyLinesExtractFeatures()
{
	CXYZPolyLine *poly = 0;

	for ( int i = 0; (i < this->rowCount()) && (!poly); i++)
	{
		if ( this->item(i, 0)->isSelected()) poly = project.getXYZPolyLines()->getAt(i);
	}  

	if (poly) 
	{	
		bui_FeatureExtractionDialog dlg(this, poly, "");
		dlg.exec();
		if (dlg.parametersAccepted())
		{
			baristaProjectHelper.refreshTree();
			this->updateContents();
		}
	}
}