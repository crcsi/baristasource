#include "Bui_SatelliteDataImportDlg.h"
#include "Filename.h"
#include "IniFile.h"
#include "Filename.h"
#include <QFileDialog>
#include "BaristaHtmlHelp.h"
#include "BaristaProject.h"
#include "OrbitObservations.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include "SensorModel.h"
#include "Icons.h"

#include <QDoubleValidator>
#include <QMessageBox>


Bui_SatelliteDataImportDlg::Bui_SatelliteDataImportDlg(CFileName& filename, const char* helpfile):
	filename(filename), helpFile(helpfile),importError(false),success(false),accuracyChanged(false),
	adjustPath(true),adjustAttitudes(true)

{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	// create the importer for each satellite
	this->dataImporter.push_back(new CSatelliteDataImporter(Spot5));
	this->dataImporter.push_back(new CSatelliteDataImporter(Quickbird));
	this->dataImporter.push_back(new CSatelliteDataImporter(Alos));
	this->dataImporter.push_back(new CSatelliteDataImporter(Formosat2));
	this->dataImporter.push_back(new CSatelliteDataImporter(Theos));
	this->dataImporter.push_back(new CSatelliteDataImporter(GeoEye1));

	// get the last imported satellite or select default one
	this->satellite= (SatelliteType) inifile.getSatellite();
	if (this->satellite >= int(this->dataImporter.size()))
		this->satellite = Spot5;

	this->sBAttitudes->setMinimum(0.01);
	this->sBAttitudes->setMaximum(10.0);
	this->sBAttitudes->setDecimals(2);
	this->sBAttitudes->setSingleStep(0.01);

	this->sBPath->setMinimum(0.1);
	this->sBPath->setMaximum(10.0);
	this->sBPath->setDecimals(1);
	this->sBPath->setSingleStep(0.1);

	this->connect(this->rB_Theos,SIGNAL(released()),this,SLOT(rB_TheosReleased()));
	this->connect(this->pB_File,SIGNAL(released()),this,SLOT(pBFileReleased()));
	this->connect(this->pB_Import,SIGNAL(released()),this,SLOT(pBImportReleased()));
	this->connect(this->pB_Cancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pB_Help,SIGNAL(released()),this,SLOT(pBHelpReleased()));
	this->connect(this->rB_Spot5,SIGNAL(released()),this,SLOT(rB_Spot5Released()));
	this->connect(this->rB_Quickbird,SIGNAL(released()),this,SLOT(rB_QuickbirdReleased()));
	this->connect(this->rB_Alos,SIGNAL(released()),this,SLOT(rB_AlosReleased()));
	this->connect(this->rB_Formosat2,SIGNAL(released()),this,SLOT(rB_Formosat2Released()));	
	this->connect(this->rB_GeoEye1,SIGNAL(released()),this,SLOT(rB_GeoEye1Released()));	
	this->connect(this->leSatelliteHeight,SIGNAL(textChanged(const QString &)),this,SLOT(leSatelliteHeightTextChanged(const QString &)));
	this->connect(this->leGroundHeight,SIGNAL(textChanged(const QString &)),this,SLOT(leGroundHeightTextChanged(const QString &)));
	this->connect(this->leTemperature,SIGNAL(textChanged(const QString &)),this,SLOT(leGroundTemperatureTextChanged(const QString &)));
	this->connect(this->sBAttitudes,SIGNAL(valueChanged(double )),this,SLOT(sBAttitudesValueChanged(double)));
	this->connect(this->sBPath,SIGNAL(valueChanged(double)),this,SLOT(sBPathValueChanged(double)));
	this->connect(this->pBAccuracy,SIGNAL(released()),this,SLOT(pBAccuracyReleased()));
	this->connect(this->cBPath,SIGNAL(currentIndexChanged(int)),this,SLOT(currentIndexChangedPath(int))); 
	this->connect(this->cBAttitudes,SIGNAL(currentIndexChanged(int)),this,SLOT(currentIndexChangedAttitudes(int)));
	this->connect(this->cBMounting,SIGNAL(currentIndexChanged(int)),this,SLOT(currentIndexChangedMounting(int)));
	this->connect(this->cBCamera,SIGNAL(currentIndexChanged(int)),this,SLOT(currentIndexChangedCamera(int)));

	this->filterQuickbird = "Quickbird (*.TXT)";
	this->filterSpot5 = "Spot 5 (*.xml *.DIM)"; 
	this->filterAlos = "Alos (IMG*__N IMG*__B IMG*__F IMG*__W IMG*___)";
	this->filterFormosat2 = "Formosat-2 (*.xml *.DIM)";
	this->filterTheos= "Theos (*.xml *.txt)";
	this->filterGeoEye1= "GeoEye1 (*.man)";
   

	this->leSatelliteHeight->setValidator(new QDoubleValidator(0.0,10000.0,1,this));
	this->leGroundHeight->setValidator(new QDoubleValidator(-10.0,5000.0,1,this));
	this->leTemperature->setValidator(new QDoubleValidator(-50.0,100.0,1,this));

	this->applySettings();
}

Bui_SatelliteDataImportDlg::~Bui_SatelliteDataImportDlg(void)
{
	// just in case we forgot to call the function directly
	this->finishImport();
}

void Bui_SatelliteDataImportDlg::pBFileReleased()
{

	const char* filter;

	this->importError = false;

	if (this->satellite == Spot5)
		filter = this->filterSpot5.GetChar();
	else if (this->satellite == Quickbird)
		filter = this->filterQuickbird.GetChar();
	else if (this->satellite == Alos)
		filter = this->filterAlos.GetChar();
	else if (this->satellite == Formosat2)
		filter = this->filterFormosat2.GetChar();
	else if (this->satellite == Theos)
		filter = this->filterTheos.GetChar();
	else if (this->satellite == GeoEye1)
		filter = this->filterGeoEye1.GetChar();

	QString filename = QFileDialog::getOpenFileName(
		this,
		"Choose file",
		inifile.getCurrDir(),
		filter);

    // make sure the file exists
    QFile test(filename);
    if (!test.exists())
        return;
	test.close();

	this->importMetadata(filename.toLatin1().constData(),this->satellite);
}

bool Bui_SatelliteDataImportDlg::importMetadata(CFileName filename,SatelliteType sat)
{
	if (sat != Spot5 && sat != Quickbird && sat != Alos && sat != Formosat2 && 
		sat!=Theos && sat!=GeoEye1)		
		return false;

	this->satellite = sat;
	CSatelliteDataImporter& importer = *this->dataImporter[this->satellite];
	
	bui_ProgressDlg dlg(new CSatelliteDataReaderThread(&importer,filename),&importer);
	dlg.exec();

	this->importError = !dlg.getSuccess();

	if (this->importError)
	{
		QMessageBox::warning( this, "Import Error!", importer.getErrorMessage().GetChar());
	}
	else
	{
		this->accuracyChanged = false;
		this->adjustPath = true;
		this->adjustAttitudes = true;		
	}

	this->applySettings();
	return !this->importError;
}



void Bui_SatelliteDataImportDlg::pBImportReleased()
{

	this->setSuccess(true);

	CSatelliteDataImporter& importer = *this->dataImporter[this->satellite];

	inifile.setSatelliteHeight(importer.getSatelliteHeight(),this->satellite);
	inifile.setSceneHeight(importer.getGroundHeight(),this->satellite);
	inifile.setSceneTemperature(importer.getGroundTemperature(),this->satellite);
	inifile.setRMSPath(importer.getAimedRMSPath(),this->satellite);
	inifile.setRMSAttitudes(importer.getAimedRMSAttitudes(),this->satellite);


	inifile.setSatellite(this->satellite);
	this->filename = importer.getFileName();
	inifile.setCurrDir(this->filename.GetPath());
	inifile.writeIniFile();

	// delete all the importers we don't need anymore
	// the remaining one will be deleted in the destructor
	for (unsigned int i=0; i < this->dataImporter.size(); i++)
	{
		if (i != this->satellite)
		{
			this->dataImporter[i]->cancelImport();
			delete this->dataImporter[i];
			this->dataImporter[i] = NULL;
		}
	}

	this->close();
}

void Bui_SatelliteDataImportDlg::pBCancelReleased()
{
	// delete all importers
	for (unsigned int i=0; i < this->dataImporter.size(); i++)
	{
		this->dataImporter[i]->cancelImport();
		delete this->dataImporter[i];
		this->dataImporter[i] = NULL;
	}
	
	this->dataImporter.clear();

	this->setSuccess(false);
	this->close();
}

void Bui_SatelliteDataImportDlg::pBAccuracyReleased()
{

	CSatelliteDataImporter& importer = *this->dataImporter[this->satellite];

	bui_ProgressDlg dlg(new COrbitAdjustmentThread(&importer,this->adjustPath,this->adjustAttitudes),&importer);
	dlg.exec();
	
	if (!dlg.getSuccess())
	{
		if (importer.getCurrentRMSPath() < 0.0 || importer.getCurrentRMSAttitudes() < 0.0)
		{
			QMessageBox::warning( this, "Error while changing accuracy level!", "Restoring of old accuracy level also failed!\nReload the whole orbit!");
			importer.cancelImport();
			this->importError = true;
		}
		else
			QMessageBox::warning( this, "Error while changing accuracy level!", "The old accuracy level will be used!");
		
	}

	this->accuracyChanged = false;
	this->applySettings();
}




void Bui_SatelliteDataImportDlg::applySettings()
{
	this->checkAvailabilityOfSettings();

	if (this->satellite == Spot5)
		this->rB_Spot5->setChecked(true);
	else if (this->satellite == Quickbird)
		this->rB_Quickbird->setChecked(true);
	else if (this->satellite == Alos)
		this->rB_Alos->setChecked(true);
	else if (this->satellite == Formosat2)
		this->rB_Formosat2->setChecked(true);
	else if (this->satellite == Theos)
		this->rB_Theos->setChecked(true);
	else if (this->satellite == GeoEye1)
		this->rB_GeoEye1->setChecked(true);


	CSatelliteDataImporter &importer = *this->dataImporter[this->satellite];

	this->le_File->setText(importer.getFileName().GetChar());
	this->le_File->setToolTip(importer.getFileName().GetChar());

	this->blockSignals(true);
	this->leSatelliteHeight->setText(QString().sprintf("%.1lf",importer.getSatelliteHeight() / 1000.0));
	this->leGroundHeight->setText(QString().sprintf("%.1lf",importer.getGroundHeight()));
	this->leTemperature->setText(QString().sprintf("%.1lf",importer.getGroundTemperature()));

	if (importer.getCurrentRMSPath() < 0.0)
		this->lePathAccuracy->setText(QString("no data"));
	else if (importer.getCurrentRMSPath() > 0.1)
		this->lePathAccuracy->setText(QString().sprintf("%.2lf",importer.getCurrentRMSPath()));
	else
		this->lePathAccuracy->setText(QString(" < 0.1"));


	if (importer.getCurrentRMSAttitudes() < 0.0)
		this->leAttitudeAccuracy->setText(QString("no data"));
	else if (importer.getCurrentRMSAttitudes() > 0.01)
		this->leAttitudeAccuracy->setText(QString().sprintf("%.3lf",importer.getCurrentRMSAttitudes()));
	else
		this->leAttitudeAccuracy->setText(QString(" < 0.01"));



	this->sBAttitudes->setValue(importer.getAimedRMSAttitudes());
	this->sBPath->setValue(importer.getAimedRMSPath());


	this->blockSignals(false);


}

void Bui_SatelliteDataImportDlg::checkAvailabilityOfSettings()
{

	this->blockSignals(true);
	this->cBPath->blockSignals(true);
	this->cBAttitudes->blockSignals(true);
	this->cBMounting->blockSignals(true);
	this->cBCamera->blockSignals(true);

	this->cBPath->clear();
	this->cBAttitudes->clear();
	this->cBMounting->clear();
	this->cBCamera->clear();

	CSatelliteDataImporter &importer = *this->dataImporter[this->satellite];

	if (this->importError || importer.getFileName().IsEmpty())
	{
		this->pB_Import->setEnabled(false);
		this->cBAttitudes->setEnabled(false);
		this->cBPath->setEnabled(false);
		this->cBCamera->setEnabled(false);
		this->cBMounting->setEnabled(false);
	}
	else
	{
		this->pB_Import->setEnabled(true);
		this->cBAttitudes->setEnabled(true);
		this->cBPath->setEnabled(true);
		this->cBCamera->setEnabled(true);
		this->cBMounting->setEnabled(true);

		vector<CCharString> names;
		unsigned int selected=0;

		importer.getKnownOrbitPaths(names,selected);
		for (unsigned int i=0; i < names.size(); i++)
			this->cBPath->addItem(names[i].GetChar());
		this->cBPath->setCurrentIndex(selected);

		importer.getKnownOrbitAttitudes(names,selected);
		for (unsigned int i=0; i < names.size(); i++)
			this->cBAttitudes->addItem(names[i].GetChar());
		this->cBAttitudes->setCurrentIndex(selected);

		importer.getKnownCameraMountings(names,selected);
		for (unsigned int i=0; i < names.size(); i++)
			this->cBMounting->addItem(names[i].GetChar());
		this->cBMounting->setCurrentIndex(selected);

		importer.getKnownCameras(names,selected);
		for (unsigned int i=0; i < names.size(); i++)
			this->cBCamera->addItem(names[i].GetChar());
		this->cBCamera->setCurrentIndex(selected);
		
		
	}

	if (this->satellite == Spot5 || this->satellite == Quickbird || this->satellite == Alos || this->satellite == Formosat2 ||
		this->satellite==Theos || this->satellite == GeoEye1)
		this->pB_File->setEnabled(true);
	else
		this->pB_File->setEnabled(false);


	if (this->accuracyChanged && importer.getCurrentRMSPath() > -1.0 &&
		importer.getCurrentRMSAttitudes() > -1.0)
		this->pBAccuracy->setEnabled(true);
	else
		this->pBAccuracy->setEnabled(false);



	if (this->pB_Import->isEnabled())
	{
		if (!this->leGroundHeight->hasAcceptableInput() ||
			!this->leSatelliteHeight->hasAcceptableInput()||
			!this->leTemperature->hasAcceptableInput())
		{
			this->pB_Import->setEnabled(false);
		}
	}

	if (this->cBAttitudes->currentIndex() > 0)
	{
		this->sBAttitudes->setEnabled(false);
		this->leAttitudeAccuracy->setEnabled(false);
	}
	else
	{
		this->sBAttitudes->setEnabled(true);
		this->leAttitudeAccuracy->setEnabled(true);
	}

	if (this->cBPath->currentIndex() > 0 )
	{
		this->lePathAccuracy->setEnabled(false);	
		this->sBPath->setEnabled(false);
	}
	else
	{
		this->sBPath->setEnabled(true);
		this->lePathAccuracy->setEnabled(true);
	}

	this->cBPath->blockSignals(false);
	this->cBAttitudes->blockSignals(false);
	this->cBMounting->blockSignals(false);
	this->cBCamera->blockSignals(false);
	this->blockSignals(false);


}


void Bui_SatelliteDataImportDlg::rB_Spot5Released()
{
	this->satellite = Spot5;
	this->applySettings();
}

void Bui_SatelliteDataImportDlg::rB_QuickbirdReleased()
{

	this->satellite = Quickbird;
	this->applySettings();
}

void Bui_SatelliteDataImportDlg::rB_AlosReleased()
{
	this->satellite = Alos;
	this->applySettings();
}

void Bui_SatelliteDataImportDlg::rB_Formosat2Released()
{
	this->satellite = Formosat2;
	this->applySettings();
}

void Bui_SatelliteDataImportDlg::rB_TheosReleased()
{
	this->satellite = Theos;
	this->applySettings();
}

void Bui_SatelliteDataImportDlg::rB_GeoEye1Released()
{
	this->satellite = GeoEye1;
	this->applySettings();
}


void Bui_SatelliteDataImportDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp(this->helpFile);
}

void Bui_SatelliteDataImportDlg::leSatelliteHeightTextChanged(const QString & text)
{
	if (this->leSatelliteHeight->hasAcceptableInput())
		this->dataImporter[this->satellite]->setSatelliteHeight(this->leSatelliteHeight->text().toDouble() * 1000.0);
	this->checkAvailabilityOfSettings();
}
	
void Bui_SatelliteDataImportDlg::leGroundHeightTextChanged(const QString & text)
{
	if (this->leGroundHeight->hasAcceptableInput())
		this->dataImporter[this->satellite]->setGroundHeight(this->leGroundHeight->text().toDouble());
	this->checkAvailabilityOfSettings();
}
	
void Bui_SatelliteDataImportDlg::leGroundTemperatureTextChanged(const QString & text)
{
	if (this->leTemperature->hasAcceptableInput())
		this->dataImporter[this->satellite]->setGroundTemperature(this->leTemperature->text().toDouble());
	this->checkAvailabilityOfSettings();
}

void Bui_SatelliteDataImportDlg::sBAttitudesValueChanged ( double value )
{
	this->accuracyChanged = true;
	this->dataImporter[this->satellite]->setAimedRMSAttitudes(value);
	this->applySettings();
}

void Bui_SatelliteDataImportDlg::sBPathValueChanged (double value )
{
	this->accuracyChanged = true;
	this->dataImporter[this->satellite]->setAimedRMSPath(value);
	this->applySettings();
}


void Bui_SatelliteDataImportDlg::currentIndexChangedPath( int index )
{
	if (index >= 0)
	{
		if (index > 0)
			this->adjustPath = false;
		else
			this->adjustPath = true;
		
		this->dataImporter[this->satellite]->selectPath(index);
	}

	this->applySettings();
}

void Bui_SatelliteDataImportDlg::currentIndexChangedAttitudes( int index )
{
	if (index >= 0)
	{
		if (index > 0)
			this->adjustAttitudes = false;
		else
			this->adjustAttitudes = true;
		
		this->dataImporter[this->satellite]->selectAttitudes(index);
	}
	this->applySettings();
}

void Bui_SatelliteDataImportDlg::currentIndexChangedMounting( int index )
{
	if (index >= 0)
		this->dataImporter[this->satellite]->selectCameraMounting(index);
	this->applySettings();
}

void Bui_SatelliteDataImportDlg::currentIndexChangedCamera( int index )
{
	if (index >= 0)
		this->dataImporter[this->satellite]->selectCamera(index);
	this->applySettings();
}

/*
bool Bui_SatelliteDataImportDlg::hasTFW() const
{
	if (!this->satelliteReader) return false;

	return this->satelliteReader->hasTFW();
};
		

CTFW *Bui_SatelliteDataImportDlg::getTFW()
{
	if (!hasTFW()) return 0;

	return this->satelliteReader->getTFW();
};
*/


double Bui_SatelliteDataImportDlg::getSatelliteHeight()const 
{
	return this->dataImporter[this->satellite]->getSatelliteHeight();
}

double Bui_SatelliteDataImportDlg::getGroundHeight()const 
{
	return this->dataImporter[this->satellite]->getGroundHeight();
}

double Bui_SatelliteDataImportDlg::getGroundTemperature() const 
{
	return this->dataImporter[this->satellite]->getGroundTemperature();
}

double Bui_SatelliteDataImportDlg::getFirstLineTime() const 
{
	return this->dataImporter[this->satellite]->getFirstLineTime();
}

double Bui_SatelliteDataImportDlg::getTimePerLine() const 
{
	return this->dataImporter[this->satellite]->getTimePerLine();
}


int Bui_SatelliteDataImportDlg::getActiveAdditionalParameter()const
{
	return this->dataImporter[this->satellite]->getActiveAdditionalParameter();
}

int Bui_SatelliteDataImportDlg::getActiveInteriorParameter() const
{
	return this->dataImporter[this->satellite]->getActiveInteriorParameter();
}

int Bui_SatelliteDataImportDlg::getActiveMountingRotationParameter() const
{
	return this->dataImporter[this->satellite]->getActiveMountingRotationParameter();
}

int Bui_SatelliteDataImportDlg::getActiveMountingShiftParameter()const
{
	return this->dataImporter[this->satellite]->getActiveMountingShiftParameter();
}


CSensorModel* Bui_SatelliteDataImportDlg::getOrbitPathModel()
{
	return this->dataImporter[this->satellite]->getOrbitPath();
}

CSensorModel* Bui_SatelliteDataImportDlg::getOrbitAttitudeModel()
{
	return this->dataImporter[this->satellite]->getOrbitAttitudes();
}

CCamera* Bui_SatelliteDataImportDlg::getCamera() 
{
	return this->dataImporter[this->satellite]->getCamera();
}

CCameraMounting* Bui_SatelliteDataImportDlg::getCameraMounting() 
{
	return this->dataImporter[this->satellite]->getCameraMounting();
}


void Bui_SatelliteDataImportDlg::cancelImport()
{
	this->dataImporter[this->satellite]->cancelImport();
}

void Bui_SatelliteDataImportDlg::finishImport()
{
	// clean up
	for (unsigned int i=0; i < this->dataImporter.size(); i++)
	{
		if (this->dataImporter[i])
		{
			delete this->dataImporter[i];
			this->dataImporter[i] = NULL;
		}
	}
}