#include "RPCTable.h"

#include <QCloseEvent>
#include "RPC.h"
#include "Label.h"
#include "math.h"

CRPCTable::CRPCTable(CRPC* rpc,QWidget* parent) : CBaristaTable(parent),rpc(rpc)
{

	this->initTable();
    rpc->addListener(this);
}

CRPCTable::~CRPCTable(void)
{
}

void CRPCTable::closeEvent ( QCloseEvent * e )
{
    this->rpc->removeListener(this);
    e->accept();
}


void CRPCTable::setRPC(CRPC* rpc)
{
    this->rpc = rpc;
    rpc->addListener(this);
}


void CRPCTable::initTable()
{
    CCharString str("RPC:   ");
	str = str + this->getRPC()->getFileName()->GetFullFileName();
    this->setWindowTitle(str.GetChar()); 
	this->setAccessibleName(this->getRPC()->getFileName()->GetChar());

	// 92 from provided RPCs and 7 additional parameters
	int paracount = 99;

	this->setColumnCount(3);
	this->setRowCount(paracount);

	for (int i=0; i < paracount; i++)
		this->setRowHeight(i,20);

	this->setColumnWidth(0, 100);
	this->setColumnWidth(1, 100);
	this->setColumnWidth(2, 100);

	QStringList headerList;

	headerList.append(QString("Parameter" ));
	headerList.append(QString("Value" ));
	headerList.append(QString("Sigma" ));

	this->setHorizontalHeaderLabels(headerList);

    double value;
    Matrix parms;
    bool load = false;

    this->getRPC()->getAllParameters(parms);

	QTableWidgetItem* item;
    
    // Line_Num_Coefficients
    for (int i = 0; i < 20; i++)
    {
        CLabel clabel = "Line_Num";         
        clabel += i+1;

        QString str = clabel.GetChar();

        value = parms.element(i, 0);

		item = this->createItem(str,0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,0,item);

		item = this->createItem(QString( "%1" ).arg(value, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,1,item);

		item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,2,item);
    }

    // Line_Den_Coefficients
    for (int i = 0; i < 20; i++)
    {
        CLabel clabel = "Line_Den";         
        clabel += i+1;

        QString str = clabel.GetChar();

        value = parms.element(20+i, 0);

		item = this->createItem(str,0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(20+i,0,item);

		item = this->createItem(QString( "%1" ).arg(value, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(20+i,1,item);

		item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(20+i,2,item);
    }

    // Samp_Num_Coefficients
    for (int i = 0; i < 20; i++)
    {
        CLabel clabel = "Samp_Num";         
        clabel += i+1;

        QString str = clabel.GetChar();

        value = parms.element(40+i, 0);

		item = this->createItem(str,0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(40+i,0,item);

		item = this->createItem(QString( "%1" ).arg(value, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(40+i,1,item);

		item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(40+i,2,item);
    }

    // Samp_Den_Coefficients
    for (int i = 0; i < 20; i++)
    {
        CLabel clabel = "Samp_Den";         
        clabel += i+1;

        QString str = clabel.GetChar();

        value = parms.element(60+i, 0);

		item = this->createItem(str,0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(60+i,0,item);

		item = this->createItem(QString( "%1" ).arg(value, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(60+i,1,item);

		item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(60+i,2,item);
    }

	item = this->createItem("LINE_OFF",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(80,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(80, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(80,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(80,2,item);

	item = this->createItem("SAMP_OFF",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(81,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(81, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(81,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(81,2,item);

	item = this->createItem("LAT_OFF",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(82,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(82, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(82,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(82,2,item);

	item = this->createItem("LONG_OFF",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(83,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(83, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(83,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(83,2,item);

	item = this->createItem("HEIGHT_OFF",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(84,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(84, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(84,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(84,2,item);

	item = this->createItem("LINE_SCALE",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(85,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(85, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(85,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(85,2,item);

	item = this->createItem("SAMP_SCALE",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(86,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(86, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(86,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(86,2,item);

	item = this->createItem("LAT_SCALE",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(87,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(87, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(87,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(87,2,item);

	item = this->createItem("LONG_SCALE",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(88,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(88, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(88,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(88,2,item);

	item = this->createItem("HEIGHT_SCALE",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(89,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(89, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(89,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(89,2,item);

	item = this->createItem("Width",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(90,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(90, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(90,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(90,2,item);

	item = this->createItem("Height",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(91,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(91, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(91,1,item);
	item = this->createItem("fixed",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(91,2,item);

    // the following values have sigmas

	Matrix *covar = this->getRPC()->getCovariance();
    double A0sigma = sqrt(covar->element(0, 0));
    double A1sigma = sqrt(covar->element(1, 1));
    double A2sigma = sqrt(covar->element(2, 2));
    double B0sigma = sqrt(covar->element(3, 3));
    double B1sigma = sqrt(covar->element(4, 4));
    double B2sigma = sqrt(covar->element(5, 5));
    double B3sigma = sqrt(covar->element(6, 6));

	item = this->createItem("A0",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(92,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(92, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(92,1,item);
	item = this->createItem(QString( "%1" ).arg(A0sigma, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(92,2,item);

	item = this->createItem("A1",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(93,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(93, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(93,1,item);
	item = this->createItem(QString( "%1" ).arg(A1sigma, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(93,2,item);

	item = this->createItem("A2",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(94,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(94, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(94,1,item);
	item = this->createItem(QString( "%1" ).arg(A2sigma, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(94,2,item);

	item = this->createItem("B0",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(95,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(95, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(95,1,item);
	item = this->createItem(QString( "%1" ).arg(B0sigma, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(95,2,item);

	item = this->createItem("B1",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(96,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(96, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(96,1,item);
	item = this->createItem(QString( "%1" ).arg(B1sigma, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(96,2,item);

	item = this->createItem("B2",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(97,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(97, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(97,1,item);
	item = this->createItem(QString( "%1" ).arg(B2sigma, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(97,2,item);

	item = this->createItem("B3",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(98,0,item);
	item = this->createItem(QString( "%1" ).arg(parms.element(98, 0), 0, 'f', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(98,1,item);
	item = this->createItem(QString( "%1" ).arg(B3sigma, 0, 'E', 6 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(98,2,item);

	this->setCurrentItem(item);
}

