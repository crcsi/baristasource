#include <float.h> 

#include "Bui_BuildingChangeDialog.h"

#include <qfiledialog.h> 
#include <QMessageBox> 

#include "ObjectSelector.h"
#include "ProgressThread.h"
#include "BaristaProject.h"
#include "Icons.h"
#include "VImage.h"
#include "VDEM.h"
#include "DEM.h"
#include "BaristaHtmlHelp.h"
#include "IniFile.h"
#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "ProgressThread.h"
#include "Bui_ProgressDlg.h"
#include "LabelImage.h"
#include "dxfFile.h"

#include "BuildingChangeEvaluator.h"
#include "BaristaProjectHelper.h"

#include "Bui_OutputModeHandler.h"

//=============================================================================

bui_BuildingChangeDialog::bui_BuildingChangeDialog(CVImage *pExisting, bool isChangeDetector) : 
		pExistingLabels(pExisting), pNewLabels(0), MinBuildingArea(10.0), 
		noneVsWeak(10.0), weakVsPartial(50.0), partialVsStrong(80.0), computationOK (false),
		detector(0), borderSize(5), toleranceSplit(4.0), toleranceChanges(3.0),
		importExisting(false), changeDetector(isChangeDetector), pDSM(0),
		cellsizeHeight(1.0f), cellsizeArea(20.0f), maxsizeHeight(25.0), maxsizeArea(250.0), 
		pOutPutHandler(0)
{

	setupUi(this);
	connect(this->buttonOk,              SIGNAL(released()),               this, SLOT(OK()));
	connect(this->buttonCancel,          SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->buttonHelp,            SIGNAL(released()),               this, SLOT(Help()));
	connect(this->DataBaseLabelSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(ExistingChanged()));
	connect(this->NewLabelSelector,      SIGNAL(currentIndexChanged(int)), this, SLOT(NewChanged()));
	connect(this->DSMSelector,           SIGNAL(currentIndexChanged(int)), this, SLOT(DSMChanged()));
	connect(this->DTMSelector,           SIGNAL(currentIndexChanged(int)), this, SLOT(DTMChanged()));
	connect(this->PartialStrong,         SIGNAL(editingFinished()),        this, SLOT(PartialVsStrongChanged()));
	connect(this->WeakPartial,           SIGNAL(editingFinished()),        this, SLOT(WeakVsPartialChanged()));
    connect(this->NonWeak,               SIGNAL(editingFinished()),        this, SLOT(NoneVsWeakChanged()));
    connect(this->MinArea,               SIGNAL(editingFinished()),        this, SLOT(MinAreaChanged()));
    connect(this->BorderSize,            SIGNAL(editingFinished()),        this, SLOT(BordersizeChanged()));
    connect(this->ToleranceSplit,        SIGNAL(editingFinished()),        this, SLOT(ToleranceSplitChanged()));
    connect(this->ToleranceChanges,      SIGNAL(editingFinished()),        this, SLOT(ToleranceChangesChanged()));
	connect(this->CellSizeArea,          SIGNAL(editingFinished()),        this, SLOT(CellSizeAreaChanged()));
	connect(this->CellSizeHeight,        SIGNAL(editingFinished()),        this, SLOT(CellSizeHeightChanged()));
	connect(this->MaxSizeArea,           SIGNAL(editingFinished()),        this, SLOT(MaxSizeAreaChanged()));
	connect(this->MaxSizeHeight,         SIGNAL(editingFinished()),        this, SLOT(MaxSizeHeightChanged()));
 	connect(this->importButton,          SIGNAL(released()),               this, SLOT(dxfImport()));
	connect(this->OutputLevelMin,        SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMed,        SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMax,        SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputBrowse,          SIGNAL(released()),               this, SLOT(explorePathSelected()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}

//=============================================================================

bui_BuildingChangeDialog::~bui_BuildingChangeDialog(void)
{
	if (this->pOutPutHandler) delete this->pOutPutHandler;
	this->pOutPutHandler = 0;

	if (this->detector) delete this->detector;
	this->detector = 0;

	this->pExistingLabels = 0;
	this->pNewLabels      = 0;
	this->pDSM = 0;
}

//=============================================================================

void bui_BuildingChangeDialog::init()
{
	if (!changeDetector)
	{
  
		this->setWindowTitle("Evaluation of Building Detection");
		this->ExistingLabel->setText("Reference Label Image");
		this->setupDSMSelector(this->DSMSelector, this->pDSM);
		this->setupDSMSelector(this->DTMSelector, this->pDTM);
	}
	else
	{
		this->DSMSelector->setVisible(false);
		this->DSMlabel->setVisible(false);
		this->DTMSelector->setVisible(false);
		this->DTMlabel->setVisible(false);
		this->EvalParameters->setVisible(false);
	}

	currentDir = inifile.getCurrDir();
	if (!currentDir.IsEmpty())
	{
		if (this->currentDir[this->currentDir.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->currentDir += CSystemUtilities::dirDelimStr;
	}
	this->outputDir = this->currentDir;
	this->OutputDirectory->setText(this->outputDir.GetChar());

	setupImageSelector(DataBaseLabelSelector, this->pExistingLabels);
	setupImageSelector(NewLabelSelector,      this->pNewLabels);

	this->PartialStrong->blockSignals(true);
    this->WeakPartial->blockSignals(true);
	this->NonWeak->blockSignals(true);
	this->MinArea->blockSignals(true);
	this->BorderSize->blockSignals(true);
	this->ToleranceSplit->blockSignals(true);
	this->ToleranceChanges->blockSignals(true);
	this->CellSizeArea->blockSignals(true);
	this->CellSizeHeight->blockSignals(true);
	this->MaxSizeArea->blockSignals(true);
	this->MaxSizeHeight->blockSignals(true);

	QString dt=QString("%1").arg(this->partialVsStrong, 0, 'f', 1);
	this->PartialStrong->setText(dt);

	dt=QString("%1").arg(this->weakVsPartial, 0, 'f', 1);
    this->WeakPartial->setText(dt);

	dt=QString("%1").arg(this->noneVsWeak, 0, 'f', 1);
    this->NonWeak->setText(dt);

	dt=QString("%1").arg(this->MinBuildingArea, 0, 'f', 1);
    this->MinArea->setText(dt);

	dt=QString("%1").arg(this->borderSize);
    this->BorderSize->setText(dt);

	dt=QString("%1").arg(this->toleranceSplit);
    this->ToleranceSplit->setText(dt);

	dt=QString("%1").arg(this->toleranceChanges);
    this->ToleranceChanges->setText(dt);

	if (!changeDetector)
	{
		dt=QString("%1").arg(this->cellsizeArea, 0, 'f', 1);
		this->CellSizeArea->setText(dt);
		dt=QString("%1").arg(this->cellsizeHeight, 0, 'f', 1);
		this->CellSizeHeight->setText(dt);
		dt=QString("%1").arg(this->maxsizeArea, 0, 'f', 1);
		this->MaxSizeArea->setText(dt);
		dt=QString("%1").arg(this->maxsizeHeight, 0, 'f', 1);
		this->MaxSizeHeight->setText(dt);
	}

	this->MinArea->blockSignals(false); 
	this->PartialStrong->blockSignals(false);
    this->WeakPartial->blockSignals(false);
	this->NonWeak->blockSignals(false);
	this->BorderSize->blockSignals(false);
	this->ToleranceSplit->blockSignals(false);
	this->ToleranceChanges->blockSignals(false);
	this->CellSizeArea->blockSignals(false);
	this->CellSizeHeight->blockSignals(false);
	this->MaxSizeArea->blockSignals(false);
	this->MaxSizeHeight->blockSignals(false);

	if (this->pOutPutHandler) delete this->pOutPutHandler;
	this->pOutPutHandler = new bui_OutputModeHandler(this->OutputLevelMin, this->OutputLevelMed,
							                         this->OutputLevelMax, 0);

	this->NewLabelSelector->setFocus();
	this->checkSetup();
}

//=============================================================================

void bui_BuildingChangeDialog::explorePathSelected()
{
	QString dirQ = QFileDialog::getExistingDirectory (this, "Choose Output Directory",
		                                              this->outputDir.GetChar());

	CFileName direc = ((const char*)dirQ.toLatin1());
		
	if (!direc.IsEmpty())
	{
		this->outputDir = direc;
		if (this->outputDir[this->outputDir.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->outputDir += CSystemUtilities::dirDelimStr;

		this->OutputDirectory->setText(this->outputDir.GetChar());
	}
};

//=============================================================================

void bui_BuildingChangeDialog::OutputLevelChanged()
{
	this->pOutPutHandler->reactOnButtonSelected();
}
	
//=============================================================================

void bui_BuildingChangeDialog::setupImageSelector(QComboBox *box, CVImage *selectedImage)
{
	box->blockSignals(true);

	box->clear();

	box->addItem("Not Selected");

	int indexSelected = 0;

	for (int i = 0, index = 1; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		if (img->getHugeImage()->getBpp() == 16)
		{
			box->addItem(img->getFileNameChar());
			if (img == selectedImage) indexSelected = index;
			++index;
		}
	}

	box->setCurrentIndex(indexSelected);

	box->blockSignals(false);
};

//=============================================================================

void bui_BuildingChangeDialog::setupDSMSelector(QComboBox *box, CVDEM *selectedDSM)
{
	box->blockSignals(true);

	box->clear();

	box->addItem("Not Selected");

	int indexSelected = 0;

	for (int i = 0, index = 1; i < project.getNumberOfDEMs(); ++i)
	{
		CVDEM *dem = project.getDEMAt(i);
		box->addItem(dem->getFileName()->GetChar());
		if (dem == selectedDSM) indexSelected = index;
		++index;
	}

	box->setCurrentIndex(indexSelected);

	box->blockSignals(false);
};

//=============================================================================

void bui_BuildingChangeDialog::ExistingChanged()
{
	QString strng = this->DataBaseLabelSelector->currentText();
	CCharString Name = (const char *) strng.toLatin1();

	this->pExistingLabels = project.getImageByName(&Name);

	if (!this->pExistingLabels)
	{
		setupImageSelector(DataBaseLabelSelector, this->pExistingLabels);
	}
	else
	{
		if (this->pNewLabels == this->pExistingLabels) 
		{
			this->pNewLabels = 0;
			setupImageSelector(NewLabelSelector,  this->pNewLabels);
		}
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingChangeDialog::NewChanged()
{
	QString strng = this->NewLabelSelector->currentText();
	CCharString Name = (const char *) strng.toLatin1();

	this->pNewLabels = project.getImageByName(&Name);

	if (!pNewLabels)
	{
		setupImageSelector(NewLabelSelector,  this->pNewLabels);
	}
	else
	{
		if (this->pNewLabels == this->pExistingLabels) 
		{
			this->pExistingLabels = 0;
			setupImageSelector(DataBaseLabelSelector,  this->pExistingLabels);
		}
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingChangeDialog::DSMChanged()
{
	QString strng = this->DSMSelector->currentText();
	CCharString Name = (const char *) strng.toLatin1();

	this->pDSM = project.getDEMByName(Name);

	if (!this->pDSM)
	{
		setupDSMSelector(this->DSMSelector, this->pDSM);
	}
	else
	{
		if (this->pDSM == this->pDTM) 
		{
			this->pDTM = 0;
			setupDSMSelector(this->DTMSelector, this->pDTM);
		}
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingChangeDialog::DTMChanged()
{
	QString strng = this->DTMSelector->currentText();
	CCharString Name = (const char *) strng.toLatin1();

	this->pDTM = project.getDEMByName(Name);

	if (!this->pDTM)
	{
		setupDSMSelector(this->DTMSelector, this->pDTM);
	}
	else
	{
		if (this->pDSM == this->pDTM) 
		{
			this->pDSM = 0;
			setupDSMSelector(this->DSMSelector, this->pDSM);
		}
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingChangeDialog::MinAreaChanged()
{
	this->MinArea->blockSignals(true);
	
	bool isDouble;

	double hlp = this->MinArea->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)) this->MinBuildingArea = hlp;
	
	QString dt=QString("%1").arg(this->MinBuildingArea, 0, 'f', 1);
    this->MinArea->setText(dt);
	
	this->MinArea->blockSignals(false); 
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::BordersizeChanged()
{
	this->BorderSize->blockSignals(true);
	
	bool isDouble;

	double hlp = floor(this->BorderSize->text().toDouble(&isDouble));
	if ((isDouble) && (hlp >= 1.0)) this->borderSize = (int) hlp;
	
	QString dt=QString("%1").arg(this->borderSize);
    this->BorderSize->setText(dt);
	
	this->BorderSize->blockSignals(false); 
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::ToleranceSplitChanged()
{
	this->ToleranceSplit->blockSignals(true);
	
	bool isDouble;

	double hlp = this->ToleranceSplit->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 1.0)) this->toleranceSplit = hlp;
	
	QString dt=QString("%1").arg(this->toleranceSplit);
    this->ToleranceSplit->setText(dt);
	
	this->ToleranceSplit->blockSignals(false); 
	this->ToleranceChanges->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::ToleranceChangesChanged()
{
	this->ToleranceChanges->blockSignals(true);
	
	bool isDouble;

	double hlp = this->ToleranceChanges->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 1.0)) this->toleranceChanges = hlp;
	
	QString dt=QString("%1").arg(this->toleranceChanges);
    this->ToleranceChanges->setText(dt);
	
	this->ToleranceChanges->blockSignals(false); 
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::CellSizeAreaChanged()
{
	this->CellSizeArea->blockSignals(true);
	
	bool isDouble;

	double hlp = this->CellSizeArea->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0)) this->cellsizeArea = hlp;
	
	QString dt=QString("%1").arg(this->cellsizeArea, 0, 'f', 1);
    this->CellSizeArea->setText(dt);
	
	this->CellSizeArea->blockSignals(false);
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::CellSizeHeightChanged()
{
	this->CellSizeHeight->blockSignals(true);
	
	bool isDouble;

	double hlp = this->CellSizeHeight->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0)) this->cellsizeHeight = hlp;
	
	QString dt=QString("%1").arg(this->cellsizeHeight, 0, 'f', 1);
    this->CellSizeHeight->setText(dt);
	
	this->CellSizeHeight->blockSignals(false);
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::MaxSizeAreaChanged()
{
	this->MaxSizeArea->blockSignals(true);
	
	bool isDouble;

	double hlp = this->MaxSizeArea->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0)) this->maxsizeArea = hlp;
	
	QString dt=QString("%1").arg(this->maxsizeArea, 0, 'f', 1);
    this->MaxSizeArea->setText(dt);
	
	this->MaxSizeArea->blockSignals(false);
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::MaxSizeHeightChanged()
{
	this->MaxSizeHeight->blockSignals(true);
	
	bool isDouble;

	double hlp = this->MaxSizeHeight->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0)) this->maxsizeHeight = hlp;
	
	QString dt=QString("%1").arg(this->maxsizeHeight, 0, 'f', 1);
    this->MaxSizeHeight->setText(dt);
	
	this->MaxSizeHeight->blockSignals(false);
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::NoneVsWeakChanged()
{
	this->NonWeak->blockSignals(true);
	
	bool isDouble;

	double hlp = this->NonWeak->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0) && (hlp <= 100.0)) this->noneVsWeak = hlp;
	
	QString dt=QString("%1").arg(this->noneVsWeak, 0, 'f', 1);
    this->NonWeak->setText(dt);
	
	this->NonWeak->blockSignals(false);
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::WeakVsPartialChanged()
{
	this->WeakPartial->blockSignals(true);
	
	bool isDouble;

	double hlp = this->WeakPartial->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0) && (hlp <= 100.0)) this->weakVsPartial = hlp;
	
	QString dt=QString("%1").arg(this->weakVsPartial, 0, 'f', 1);
    this->WeakPartial->setText(dt);
	
	this->WeakPartial->blockSignals(false);
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::PartialVsStrongChanged()
{
	this->PartialStrong->blockSignals(true);
	
	bool isDouble;

	double hlp = this->PartialStrong->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0) && (hlp <= 100.0)) this->partialVsStrong = hlp;
	
	QString dt=QString("%1").arg(this->partialVsStrong, 0, 'f', 1);
    this->PartialStrong->setText(dt);
	
	this->PartialStrong->blockSignals(false);
	this->NewLabelSelector->setFocus();
};

//=============================================================================

void bui_BuildingChangeDialog::setCurrentDir(const CCharString &cdir)
{
	this->currentDir = cdir;
}

//=============================================================================

void bui_BuildingChangeDialog::checkSetup()
{
	if ((this->pExistingLabels || this->importExisting) && this->pNewLabels)
	{
		this->buttonOk->setEnabled(true);
		this->NewLabelSelector->setFocus();
	}
	else
	{
		this->buttonOk->setEnabled(false);
	}
}

//=============================================================================

void bui_BuildingChangeDialog::dxfImport()
{

	QString s = QFileDialog::getOpenFileName(
	this,
	"Choose a DXF file",
	inifile.getCurrDir(),
	"DXF files (*.dxf)");

    QFile test(s);

    if (test.exists())
	{
		this->existingDXF = (const char*)s.toLatin1();
		this->importExisting = true;
		setupImageSelector(DataBaseLabelSelector, 0);
		this->pExistingLabels = 0;
	}
}

//=============================================================================

bool bui_BuildingChangeDialog::setupExtents()
{
	bool ret = true;

	CHugeImage *oldHug = this->pExistingLabels->getHugeImage();
	CHugeImage *newHug = this->pNewLabels->getHugeImage();
	const CTFW *tfwOld = this->pExistingLabels->getTFW();
	const CTFW *tfwNew = this->pNewLabels->getTFW();

	C3DPoint p1(0,0,0), p2(oldHug->getWidth() - 1,oldHug->getHeight() - 1,0), 
		     p3(newHug->getWidth() - 1, newHug->getHeight() - 1,0),
			 P1old, P2old, P1new, P2new;
	tfwOld->transformObs2Obj(P1old, p1);
	tfwOld->transformObs2Obj(P2old, p2);
	tfwNew->transformObs2Obj(P1new, p1);
	tfwNew->transformObs2Obj(P2new, p3);

	C2DPoint Plu, Prl;
	Plu.x = (P1new.x > P1old.x) ? P1new.x : P1old.x;
	Plu.y = (P1new.y < P1old.y) ? P1new.y : P1old.y;

	Prl.x = (P2new.x < P2old.x) ? P2new.x : P2old.x;
	Prl.y = (P2new.y > P2old.y) ? P2new.y : P2old.y;

	C2DPoint delta (fabs(tfwNew->getA()), fabs(tfwNew->getE()));

	this->trafo.setFromShiftAndPixelSize(Plu, delta);

	this->width  = (int) floor((Prl.x - Plu.x) / delta.x) + 1;
	this->height = (int) floor((Plu.y - Prl.y) / delta.y) + 1;

	if ((this->width < 1) || (this->height < 1)) ret = false;

	return ret;
};

//=============================================================================

bool bui_BuildingChangeDialog::readLabelImages(CLabelImage &imgOld, CLabelImage &imgNew)
{
	bool ret = true;

	CHugeImage *newHug = this->pNewLabels->getHugeImage();
	CHugeImage *oldHug = this->pExistingLabels->getHugeImage();

	ret = imgOld.set(0, 0, this->width, this->height, 1, 16, true);
	if (ret) ret = imgNew.set(0, 0, this->width, this->height, 1, 16, true);
	if (ret)
	{
		CLabelImage imgOldRaw(0, 0, oldHug->getWidth(), oldHug->getHeight(), 8);
		CLabelImage imgNewRaw(0, 0, newHug->getWidth(), newHug->getHeight(), 8);
		ret = oldHug->getRect(imgOldRaw) && newHug->getRect(imgNewRaw);
		if (ret)
		{
			CTFW labelToNew(*this->pNewLabels->getTFW());
			CTFW labelToOld(*this->pExistingLabels->getTFW());
			ret = labelToNew.invert() && labelToOld.invert();
			if (ret)
			{
				labelToNew.concatenate(this->trafo);
				labelToOld.concatenate(this->trafo);
				C2DPoint PminNew  = labelToNew.getShift();
				C2DPoint DeltaNew(fabs(labelToNew.getA()), fabs(labelToNew.getE()));
				C2DPoint PminOld  = labelToOld.getShift();
				C2DPoint DeltaOld(fabs(labelToOld.getA()), fabs(labelToOld.getE()));

				for(unsigned long uMin = 0, uMax = imgOld.getDiffY(); uMin < imgOld.getNGV(); 
					uMin += imgOld.getDiffY(), uMax += imgOld.getDiffY(),
					PminOld.y += DeltaOld.y, PminNew.y += DeltaNew.y)
				{
					long uMinOld = long (floor(PminOld.y + 0.5f)) * imgOldRaw.getDiffY();
					long uMinNew = long (floor(PminNew.y + 0.5f)) * imgNewRaw.getDiffY();
					double xOld = PminOld.x;
					double xNew = PminNew.x;
					
					for (unsigned long u = uMin; u < uMax; ++u, xOld += DeltaOld.x, xNew += DeltaNew.x)
					{
						long uOld = uMinOld + long(floor(xOld + 0.5f)) *  imgOldRaw.getDiffX();
						long uNew = uMinNew + long(floor(xNew + 0.5f)) *  imgNewRaw.getDiffX();

						imgOld.pixUShort(u) = imgOldRaw.pixUShort(uOld);
						imgNew.pixUShort(u) = imgNewRaw.pixUShort(uNew);
					}
				}
			}
		}
	}
	return ret;
};

//=============================================================================

void bui_BuildingChangeDialog::OK()
{
	if (this->pNewLabels)
	{
		if (this->importExisting)
		{
			baristaProjectHelper.setIsBusy(true);
			initLabelFromDXF();
			baristaProjectHelper.setIsBusy(false);

			this->importExisting = false;
			setupImageSelector(DataBaseLabelSelector, this->pExistingLabels);
		}
		else if (this->pExistingLabels)
		{
			if (this->detector) delete this->detector;
			this->detector = 0;

			const CTFW *tfwOld = (this->pExistingLabels->hasTFW()) ? this->pExistingLabels->getTFW() : 0;
			const CTFW *tfwNew = (this->pNewLabels->hasTFW()) ? this->pNewLabels->getTFW() : 0;
			if (!tfwOld || !tfwNew)
			{
					QMessageBox::warning( this, "Change detection not possible!"
					, "No TFW node available!");
			}
			else if (!setupExtents())
			{
					QMessageBox::warning( this, "Change detection not possible!"
					, "Windows do not overlap!");
			}
			else
			{
				CCharString heading;
				if (this->changeDetector)
				{
					this->detector = new CBuildingChangeEvaluator (trafo, this->MinBuildingArea, this->outputDir);
					heading = "Building Change Detection ...";	
				}
				else
				{
					this->detector = new CBuildingChangeEvaluator (trafo, this->MinBuildingArea, this->outputDir);
					heading = "Evaluation of Building Detection ...";	
				}

#ifdef WIN32
				bui_ProgressDlg dlg(new CBuildingChangeThread(this),this->detector, heading.GetChar());

				dlg.exec();

				this->computationOK = dlg.getSuccess();
				baristaProjectHelper.refreshTree();
#endif				
			}
		}

		this->close();
	}
}

//=============================================================================

void bui_BuildingChangeDialog::Cancel()
{
	this->close();
}

//=============================================================================

void bui_BuildingChangeDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/BuildingDetector.html");
};

//=============================================================================

bool bui_BuildingChangeDialog::detectChanges()
{
	bool ret = true;
	if (!this->detector) ret = false;
	else
	{
		CHugeImage *newHug = this->pNewLabels->getHugeImage();
		CHugeImage *oldHug = this->pExistingLabels->getHugeImage();

		CLabelImage imgOld(0, 0, 1, 1, 8);
		CLabelImage imgNew(0, 0, 1, 1, 8);

		if (!readLabelImages(imgOld, imgNew))
		{
			QMessageBox::warning( this, " Cannot detect changes!", " Error initialising data!");
		}
		else
		{
			imgOld.setMargin(this->borderSize, 0);
			imgOld.initPixelNumbers();
			imgOld.removeEmptyLabels(true);
			imgOld.updateVoronoi();

			imgNew.setMargin(this->borderSize, 0);
			imgNew.initPixelNumbers();
			imgNew.removeEmptyLabels(true);
			imgNew.updateVoronoi();

			CCharString prot = this->changeDetector ? "ChangeDetection.prt" : "evaluate.prt";
			this->detector->init(&imgNew, &imgOld, prot.GetChar());

			CFileName fn;
			if (this->changeDetector)
			{
				detector->detectChanges(this->noneVsWeak * 0.01, this->weakVsPartial * 0.01, 
									this->partialVsStrong * 0.01, this->toleranceSplit, 							  
									this->toleranceChanges, this->cellsizeArea, this->maxsizeArea);
				
				fn = this->outputDir + "changes.tif";
			}
			else
			{
				CDEM *dsm = 0;
				CDEM *dtm = 0;
				if (this->pDSM && this->pDTM)
				{
					dsm = this->pDSM->getDEM();
					dtm = this->pDTM->getDEM();
				}
				detector->evaluateResults(dsm, dtm, 
					                this->noneVsWeak * 0.01, this->weakVsPartial * 0.01, 
									this->partialVsStrong * 0.01, this->toleranceSplit, 							  
									this->toleranceChanges, this->cellsizeArea, this->maxsizeArea,
									this->cellsizeHeight, this->maxsizeHeight);

				fn = this->outputDir + "evaluate.tif";
			}

			CVImage *img = project.addImage(fn, false);
			CCharString str = img->getName() + ".hug";
	
			if (!detector->dumpPixResults(str.GetChar(), img->getHugeImage()))
				project.deleteImage(img);
			else
			{
				img->setTFW(detector->getTrafo());
			}


			if (this->changeDetector)
			{			
				fn = this->outputDir + "newState.tif";
				img = project.addImage(fn, false);
				str = img->getName() + ".hug";
				if (!detector->dumpNewState(str.GetChar(), img->getHugeImage()))
				project.deleteImage(img);
				else
				{
					img->setTFW(detector->getTrafo());
				}
			}
			baristaProjectHelper.refreshTree();
		}
	}

	return ret;
};

//=============================================================================

void bui_BuildingChangeDialog::initLabelFromDXF()
{
	baristaProjectHelper.setIsBusy(true);

	CLabel l("DXF0");
	CDXFReader reader(l, existingDXF);

	unsigned long nVerts, nLines, nPolys, n3dFaces;
	bool OK = reader.isOpen();

	CFileName FN1 (existingDXF);
	FN1.SetFileName(existingDXF.GetFileName() + "_red");
			
	if (OK) reader.readDXF(nVerts, nLines, nPolys, n3dFaces, false);
	if (OK) OK = nPolys > 0;
	
	 if (OK) 
	 {
		CFileName FN (existingDXF);
		FN.SetFileExt("tif");

		this->pExistingLabels = project.addImage(FN, false);		
		FN += ".hug";

		this->pExistingLabels->setTFW(*pNewLabels->getTFW());	
		
		CHugeImage *newHug = pNewLabels->getHugeImage();

		CLabelImage labels(0, 0, newHug->getWidth(), newHug->getHeight(), 4);

		unsigned short L = 1;

		ofstream newDXF(FN1.GetChar());
		CXYZPolyLine::writeDXFHeader(newDXF);	

		CXYZPolyLineArray innerPolyLines;

		for (int i = 0; i < reader.polylines.GetSize(); ++i)
		{
			CXYZPolyLine *poly1 = reader.polylines.getAt(i);
			poly1->closeLine();
			bool isOuter = true;

			for (int j = i; j < reader.polylines.GetSize(); ++j)
			{
				CXYZPolyLine *poly2 = reader.polylines.getAt(j);
				poly2->closeLine();
				if (poly1->contains(*poly2)) 
				{
					innerPolyLines.Add(*poly2);
					reader.polylines.RemoveAt(j);
					--j;
				}
				else if (poly2->contains(*poly1))
				{
					innerPolyLines.Add(*poly1);
					j = reader.polylines.GetSize() + 1;
					reader.polylines.RemoveAt(i);
					isOuter = false;
					--i;
				}
			}
		}

		for (int i = 0; i < reader.polylines.GetSize(); ++i)
		{
			C2DPoint pmin(FLT_MAX, FLT_MAX), pmax(-FLT_MAX, -FLT_MAX);
			CXYZPolyLine *poly = reader.polylines.getAt(i);
			for (int j = 0; j < poly->getPointCount(); ++j)
			{
				CXYZPoint *p = poly->getPoint(j);
				this->pExistingLabels->getTFW()->transformObj2Obs(*p, *p);
				if (p->x < pmin.x) pmin.x = p->x;
				if (p->y < pmin.y) pmin.y = p->y;
				if (p->x > pmax.x) pmax.x = p->x;
				if (p->y > pmax.y) pmax.y = p->y;
			}
			poly->closeLine();
			if ((pmax.x > 0) && (pmax.y > 0) && (pmin.x < newHug->getWidth()) && (pmin.y < newHug->getHeight())) 
			{
				labels.fill(*poly, L);
				++L;
				if (!L) ++L;

				for (int j = 0; j < poly->getPointCount(); ++j)
				{
					CXYZPoint *p = poly->getPoint(j);
					this->pExistingLabels->getTFW()->transformObs2Obj(*p, (C3DPoint)*p);
				}

				poly->writeDXF(newDXF, "database", 1, 3);

			}
	
		}
	
		for (int i = 0; i < innerPolyLines.GetSize(); ++i)
		{
			C2DPoint pmin(FLT_MAX, FLT_MAX), pmax(-FLT_MAX, -FLT_MAX);
			CXYZPolyLine *poly = innerPolyLines.getAt(i);
			for (int j = 0; j < poly->getPointCount(); ++j)
			{
				CXYZPoint *p = poly->getPoint(j);
				this->pExistingLabels->getTFW()->transformObj2Obs(*p, *p);
				if (p->x < pmin.x) pmin.x = p->x;
				if (p->y < pmin.y) pmin.y = p->y;
				if (p->x > pmax.x) pmax.x = p->x;
				if (p->y > pmax.y) pmax.y = p->y;
			}
			poly->closeLine();
			if ((pmax.x > 0) && (pmax.y > 0) && (pmin.x < newHug->getWidth()) && (pmin.y < newHug->getHeight())) 
			{
				labels.fill(*poly, 0);
				
				for (int j = 0; j < poly->getPointCount(); ++j)
				{
					CXYZPoint *p = poly->getPoint(j);
					this->pExistingLabels->getTFW()->transformObs2Obj(*p, (C3DPoint)*p);
				}

				poly->writeDXF(newDXF, "database", 1, 3);

			}
		}


		CXYZPolyLine::writeDXFTrailer(newDXF);	

		labels.setMargin(1, 0);
		labels.initPixelNumbers();
		labels.removeEmptyLabels(true);

		OK = this->pExistingLabels->getHugeImage()->writeLabelImage(FN.GetChar(), labels);

		if (!OK)
		{
			project.deleteImage(this->pExistingLabels);
			QMessageBox::warning( this, "Generation of label image failed!", "DXF Import was not successful!");
			this->pExistingLabels = 0;
		}
		else 
		{
			baristaProjectHelper.refreshTree();		
		}
	 }

	baristaProjectHelper.setIsBusy(false);
}

//=============================================================================
