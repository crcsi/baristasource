	/**
 * @class CBaristaView
 *
 * Class for viewing geometric entities
 *
 * @author Franz Rottensteiner
 * @version 1.0
 * @date 8-Feb-2007
 *
 */
#include <qpainter.h>
#include <qcolor.h>
#include <QMouseEvent>
#include <QAction>

#include "BaristaView.h"
#include "CharString.h"
#include "Icons.h"
#include "XYContour.h"
#include "XYPolyLine.h"
#include "QTImageHelper.h"
#include "BaristaViewEventHandler.h"
#include "BaristaProject.h"
#include "Bui_CropROIDlg.h"
#include "Bui_MainWindow.h"
#include <qmenu.h>

#include "Bui_ChannelSwitchBox.h"
#include "TransferFunctionFactory.h"
#include "LookupTableHelper.h"
#include "BaristaProjectHelper.h"
#include "Bui_WallisFilterDlg.h"
#include "Bui_ImgShowDlg.h"

////
#include "histogram.h"
////

#include "LabelImage.h"

CBaristaView::CBaristaView(const CCharString &title,const CTransferFunction& transferFunction,
						   double xmin, double xmax, double ymin, double ymax,
						   double xoffset, double yoffset,
						   QWidget * parent) :
        QWidget(parent), pmin (xmin, ymin), pmax(xmax, ymax), offset (xoffset, yoffset),
		zoomFactor (1.0), virtualWidth(0), virtualHeight(0), scrollX(0), scrollY(0),
		firstPaint(true), roiLR(-1.0, -1.0), roiUL(-1.0, -1.0), firstClick(-1.0, -1.0), 
		secondClick(-1.0, -1.0),showROI(false),showRubberLine(false),contextMenu(NULL), 
		CtrlDown(false), TFWMonoplotting(false), HeightMonoplotting(false),
		showMatchWindow(false),inGeoLinkMode(false),resamplingReset(false),
		currentResamplingType(eNearestNeighbour),geoLinkThread(this),dataResolution(-1.0)
{
	this->setWindowTitle(title.GetChar());
	this->setAccessibleName(title.GetChar());

	this->setAttribute(Qt::WA_DeleteOnClose);

    // listen for mouse events
    this->setMouseTracking(true);

    this->lButtonDown = false;
    this->mButtonDown = false;
    this->rButtonDown = false;

    this->xPressed = 0;
    this->yPressed = 0;






    // scroll barstuff
    this->hChannel.loadFromData( hchannel, sizeof( hchannel ), "BMP" );
    this->vChannel.loadFromData( vchannel, sizeof( vchannel ), "BMP" );

    this->upArrow.loadFromData( up_arrow, sizeof( up_arrow ), "BMP" );
    this->downArrow.loadFromData( down_arrow, sizeof( down_arrow ), "BMP" );
    this->leftArrow.loadFromData( left_arrow, sizeof( left_arrow ), "BMP" );
    this->rightArrow.loadFromData( right_arrow, sizeof( right_arrow ), "BMP" );
    this->hThumbNib.loadFromData( hthumb, sizeof( hthumb ), "BMP" );
    this->vThumbNib.loadFromData( vthumb, sizeof( vthumb ), "BMP" );
    this->leftThumb.loadFromData( left_thumb, sizeof( left_thumb ), "BMP" );
    this->rightThumb.loadFromData( right_thumb, sizeof( right_thumb ), "BMP" );
    this->topThumb.loadFromData( top_thumb, sizeof( top_thumb ), "BMP" );
    this->bottomThumb.loadFromData( bottom_thumb, sizeof( bottom_thumb ), "BMP" );
    this->corner.loadFromData( corner1, sizeof( corner1 ), "BMP" );

	QPixmap zoomInCursor;
	QPixmap zoomOutCursor;
	QPixmap panCursor;
	QPixmap crossHairCursor;
	QPixmap crossHairSCursor;
	QPixmap crossHairCCursor;

	zoomInCursor.loadFromData(zoomin22, sizeof(zoomin22), "PNG");
	zoomOutCursor.loadFromData(zoomout2, sizeof(zoomout2), "PNG");
	panCursor.loadFromData(hand2, sizeof(hand2), "PNG");
	crossHairCursor.loadFromData(crossHair, sizeof(crossHair), "PNG");
	crossHairSCursor.loadFromData(crossHairS, sizeof(crossHairS), "PNG");
	crossHairCCursor.loadFromData(crossHairC, sizeof(crossHairC), "PNG");

	this->zoomin_cur = new QCursor(zoomInCursor);
	this->zoomout_cur= new QCursor(zoomOutCursor);
	this->pan_cur= new QCursor(panCursor);
	this->cross_cur = new QCursor(crossHairCursor);
	this->crossS_cur = new QCursor(crossHairSCursor);
	this->crossC_cur = new QCursor(crossHairCCursor);

    this->rightArrowPressed = false;
    this->leftArrowPressed = false;
    this->upArrowPressed = false;
    this->downArrowPressed = false;

	this->scrollbarSize = this->vChannel.width();
    this->lButtonDown = false;
    this->rButtonDown = false;
    this->vThumbPressed = false;
    this->hThumbPressed = false;
    // end scroll bar stuff

	this->hasROI=false;

	this->residualFactor = 1.0;

	
	//this->Res3DHtFactor = 1.0;
	//this->Res3DXYFactor = 1.0;
	

	this->setFocusPolicy(Qt::StrongFocus);
}


void CBaristaView::initEventHandler()
{
	CBaristaViewEventHandler::addBaristaView(this);
}


CBaristaView::~CBaristaView()
{

	if (this->zoomin_cur  != NULL)
		delete this->zoomin_cur;
	this->zoomin_cur  = NULL;

	if (this->zoomout_cur  != NULL)
		delete this->zoomout_cur;
	this->zoomout_cur =NULL;

	if (this->pan_cur    != NULL)
		delete this->pan_cur;
	this->pan_cur = NULL;

	if (this->cross_cur)
		delete cross_cur;
	this->cross_cur = NULL;

	if (this->crossS_cur)
		delete crossS_cur;
	this->crossS_cur = NULL;

	if (this->crossC_cur)
		delete crossC_cur;
	this->crossC_cur = NULL;

	currentBaristaViewHandler->setStatusBarText("");
	CBaristaViewEventHandler::deleteBaristaView(this);
}

// this function gets called by QT when QWidget::update() has been called or
// the window needs to redraw after occlusion
void CBaristaView::paintEvent(QPaintEvent* e)
{
	
	QPainter painter(this);
	this->drawContents(painter);

}

void CBaristaView::updateFromThread()
{
	this->clearImages();
	this->update();
}


// this function only gets called when a child class has not implemented it
void CBaristaView::drawContents (QPainter &p)
{
	this->drawContentsPre(p);
	this->drawContentsPost(p);
}


// child classes should call this function first to draw the actual image data
void CBaristaView::drawContentsPre(QPainter &p)
{
	this->setVirtualWidth();
	this->setVirtualHeight();

	this->doFirstDraw(this->virtualHeight,this->virtualWidth);

	this->refreshImages();

	for (int i = 0; i < this->qimages.count(); i++)
    {
        QImage* qimage = (QImage*)this->qimages.at(i);
        QPoint* offset = (QPoint*)this->offsets.at(i);

		p.drawImage(offset->x()-this->getScrollX(), offset->y()-this->getScrollY(), *qimage);
    }
}


//child classes should call this function after they have draw their private stuff
void CBaristaView::drawContentsPost(QPainter &p)
{
	// draw monoplotted points
	CVBase* vbase = this->getVBase();
	CXYPointArray* monoPlottedPoints = vbase ? vbase->getMonoPlottedPoints() : 0;

	if (monoPlottedPoints && monoPlottedPoints->GetSize() && monoPlottedPoints->getActive())
		this->drawXYPointArray(monoPlottedPoints,p);


	// draw unselected ROI
	if(this->showROI && this->lButtonDown)
	{
		double xscreen,yscreen;
		toScreen(cpos.x, cpos.y, xscreen, yscreen);

		int rectLeft = xPressed;
		int rectHeight = yscreen - yPressed;

		int rectTop = yPressed;
		int rectWidth = xscreen - xPressed;
		p.setPen(QPen(Qt::blue, 2,Qt::DotLine));

		p.drawRect(rectLeft, rectTop, rectWidth, rectHeight );
	}


	int nDrawing = this->drawings.getNDataSets();
	for (int i=0; i< nDrawing; i++)
	{
		CDrawingDataSet* dataSet = this->drawings.getDataSet(i);

		// handle points
		if (dataSet->getShowPoints())
		{
			int *col = dataSet->getPointColor();
			QColor c(col[0], col[1], col[2]);
			int nPoints = dataSet->getNPoints();
			if (dataSet->getShowPointLabels())
			{
				for (int k= 0; k < nPoints; k++)
					this->drawPoint(p,dataSet->getPoint(k),c,1,4);
			}
			else
			{
				for (int k= 0; k < nPoints; k++)
					this->drawCross(p,dataSet->getPoint(k),c,1,4);
			}
		}

		// handle topology
		int nTopo = dataSet->getNTopology();
		for (int k=0; k < nTopo;k++)
		{
			CTopology* topo = dataSet->getTopology(k);
			if (topo->from && topo->to)
			{
				QColor c(topo->color[0], topo->color[1], topo->color[2]);
				this->drawLine(p,*topo->from,*topo->to, c,topo->lineWidth);
			}
		}
	}


	// draw selected ROI
	if (this->hasROI)
	{
		double xscreen1, yscreen1, xscreen2, yscreen2;

		toScreen(this->roiLR.x, this->roiLR.y, xscreen1, yscreen1);
		toScreen(this->roiUL.x, this->roiUL.y, xscreen2, yscreen2);
		p.setPen(QPen(Qt::blue,2));
		p.drawRect(xscreen1, yscreen1, xscreen2 - xscreen1, yscreen2 - yscreen1);
	}

	// draw rubber line
	if ( this->showRubberLine && this->firstClick.x != -1 && this->secondClick.x == -1)
	{
		this->drawLine(p, this->firstClick, this->cpos, Qt::magenta, 2);
	}

	// draw match window
	int size =  project.getMatchingParameter().getPatchSize();

	if ( this->showMatchWindow && size > 2 )
	{
		C2DPoint ul(this->cpos.x - size, this->cpos.y - size); 
		C2DPoint ll(this->cpos.x - size, this->cpos.y + size); 
		C2DPoint ur(this->cpos.x + size, this->cpos.y - size); 
		C2DPoint lr(this->cpos.x + size, this->cpos.y + size); 

		this->drawLine(p, ul, ll, Qt::magenta, 1);
		this->drawLine(p, ul, ur, Qt::magenta, 1);
		this->drawLine(p, ur, lr, Qt::magenta, 1);
		this->drawLine(p, lr, ll, Qt::magenta, 1);
	}

    this->paintBorders(p);
}


void CBaristaView::refreshHugeImages(CHugeImage* himage)
{
	int tfilt=0;

    int xPos;
    int yPos;
    xPos = this->getScrollX();
    yPos = this->getScrollY();

    int w = this->getViewWidth();
    int h = this->getViewHeight();

    int imageWidth = this->getDataWidth();
    int imageHeight = this->getDataHeight();

	m_Roi *troi=himage->getRoiPtr();
	if(troi!=NULL)
	{
		tfilt=troi->filtered;
	}
	if ( tfilt!=FIL_NONE)
		this->clearImages();

    if ( /*tfilt!=FIL_NONE ||*/ (imageWidth < w) || (imageHeight < h)) // we will do a fit to visible rect
    {
		QImage* qimage =  QTImageHelper::createQImage(himage, 0, himage->getHeight(), 0, himage->getWidth(), imageHeight, imageWidth);
		this->clearImages();
        this->qimages.append((QObject*)qimage);
        QPoint* offset = new QPoint(0, 0);
        this->offsets.append((QObject*)offset);

        return;
    }

    int leftTile = xPos/himage->getTileWidth();
    int rightTile = (xPos+w)/himage->getTileWidth();

    int tilesAcross = rightTile-leftTile+1;

    int topTile = yPos/himage->getTileHeight();
    int bottomTile = (yPos+h)/himage->getTileHeight();

    int tilesDown = bottomTile-topTile+1;

    int nTiles = tilesAcross*tilesDown;

    // check for tiles not needed
	for ( int i = 0; i <  this->qimages.count();)
    {
        QImage* qimage = (QImage*)this->qimages.at(i);
        QPoint* offset = (QPoint*)this->offsets.at(i);

        if ( (offset->x() < leftTile*himage->getTileWidth())  ||
             (offset->x() > rightTile*himage->getTileWidth())  ||
             (offset->y() < topTile*himage->getTileHeight())  ||
             (offset->y() > bottomTile*himage->getTileHeight()) )
        {
			this->clearImage(i);
        }
        else
        {
            i++;
        }
    }

    // make sure we have enough tiles
    while (this->qimages.count() < nTiles)
    {
        QImage* qimage = new QImage();
        this->qimages.append((QObject*)qimage);
        QPoint* offset = new QPoint();
        offset->setX(-1);
        offset->setY(-1);

        this->offsets.append((QObject*)offset);
    }

    int topR = topTile*himage->getTileHeight();
    int bottomR = bottomTile*himage->getTileHeight();
    int leftC = leftTile*himage->getTileWidth();
    int rightC = rightTile*himage->getTileWidth();

    for (int r = topR; r <= bottomR; r+= himage->getTileHeight())
    {
        for (int c = leftC; c <= rightC; c+= himage->getTileWidth())
        {
            if (this->isImageThere(r, c))
                continue;

            this->fetchTile(r, c,himage);
        }
    }
}

void CBaristaView::refreshImages()
{
	CHugeImage *himage = this->getHugeImage();

	if (himage)
		this->refreshHugeImages(himage);

}




bool CBaristaView::fetchTile(int r, int c,CHugeImage* himage)
{

    for (int i = 0; i < this->qimages.count(); i++)
    {
        QPoint* offset = (QPoint*)this->offsets.at(i);
        if ( (offset->x() != -1) && (offset->y() != -1) )
            continue;

		QImage* qimage = QTImageHelper::createTileQImage(himage,r,c,this->zoomFactor);
		this->clearImage(i);
		this->qimages.append((QObject*)qimage);

        offset = new QPoint(0, 0);
        this->offsets.append((QObject*)offset);

        offset->setX(c);
        offset->setY(r);
        return true;
    }


    return false;
}

bool CBaristaView::isImageThere(int r, int c)
{
    for (int i = 0; i < this->qimages.count(); i++)
    {
        QPoint* offset = (QPoint*)this->offsets.at(i);
        if ( (offset->x() == c) && (offset->y() == r) )
            return true;
    }

    return false;
}




void CBaristaView::zoomToPoint(const C2DPoint& point,double zoomFactor,bool updateView)
{

	this->setZoomFactor(zoomFactor);
	this->setVirtualWidth();
	this->setVirtualHeight();

	C2DPoint pos(point.x* this->zoomFactor - this->getViewWidth()  / 2,
				 point.y* this->zoomFactor - this->getViewHeight() /2);

    this->setScrollX(pos.x);
    this->setScrollY(pos.y);

	

	// when called from a different thread then the gui thread, do not call update
	// updates from different threads have to come via signals !!!
	if (updateView)
	{
		this->clearImages();
		this->update();
	}

}

void CBaristaView::setZoomFactor(double zoomFactor)
{
	this->zoomFactor = zoomFactor;
}





void CBaristaView::setScrollX(int x)
{
    this->scrollX = min(max(0, x), this->virtualWidth - this->width());
}

void CBaristaView::setScrollY(int y)
{
    this->scrollY = min(max(0, y), this->virtualHeight - this->height());
}




bool CBaristaView::toScreen(double x, double y, double &xscreen, double &yscreen)
{
	bool ret = true;
	if ((x < pmin.x) || (x > pmax.x)  || (y < pmin.y) || (y > pmax.y)) ret = false;
	xscreen = (x + this->offset.x) * this->zoomFactor - this->getScrollX();
	yscreen = (y + this->offset.y) * this->zoomFactor - this->getScrollY();

	return ret;
};

bool CBaristaView::fromScreen(double xscreen, double yscreen, double &x, double &y)
{
   x = (xscreen + this->getScrollX()) / this->zoomFactor - this->offset.x;
   y = (yscreen + this->getScrollY()) / this->zoomFactor - this->offset.y;

   bool ret = true;
   if ((x < pmin.x) || (x > pmax.x)  || (y < pmin.y) || (y > pmax.y)) ret = false;
   return ret;
};

bool CBaristaView::getMouseEventCoords(QMouseEvent * e, double &x, double &y)
{
	return this->fromScreen(e->x(), e->y(), x, y);
};


/**
 * Draws cross at image position to imageview
 * @param point
 */
void CBaristaView::drawCross(QPainter &p, const CObsPoint &point, QColor color, int width, int size)
{
	double xscreen, yscreen;

    if (toScreen(point[0], point[1], xscreen, yscreen))
	{
		QPen pen(color, width);
		p.setPen(pen);

		p.drawLine(xscreen - size, yscreen, xscreen + size + 1 , yscreen);
		p.drawLine(xscreen, yscreen - size, xscreen, yscreen + size + 1);
	}
}

/** >> addition
 * Draws circle at image position to imageview
 * @param point
 */
void CBaristaView::drawCircle(QPainter &p, const CObsPoint &point, QColor color, int width, int size)
{
	double xscreen, yscreen;	
	/*int rect_size;
	if (color == Qt::white)
		rect_size = 2;
	else if (color == Qt::black)
		rect_size = 2;
	else
		rect_size = 4;
		*/
    if (toScreen(point[0], point[1], xscreen, yscreen))
	{
		QPen pen(color, width, Qt::SolidLine);
		p.setPen(pen);
		
		
     QRectF rectangle(xscreen-width, yscreen-width, 2*width, 2*width);
     int startAngle = 0 * 16;
     int spanAngle = 360 * 16;     
     p.drawPie(rectangle, startAngle, spanAngle); 
	 p.fillRect(rectangle, color);
	}
}
/**
 * Draws line at image position to imageview
 * @param start,  end
 */
void CBaristaView::drawLine(QPainter &p, const CObsPoint &start, const CObsPoint &end,
							QColor color, int width)
{
	double xscreen1, yscreen1, xscreen2, yscreen2;

	bool isInside = toScreen(start[0], start[1], xscreen1, yscreen1);
	isInside |= toScreen(end[0], end[1], xscreen2, yscreen2);

	if (isInside)
	{
		// screen position
		QPen pen(color, width);
		p.setPen(pen);
    	p.drawLine(xscreen1, yscreen1, xscreen2, yscreen2);
	}
}


void CBaristaView::drawPoint(QPainter &painter, const CObsPoint &point,QColor color, int width, int size)
{
	this->drawCross(painter, point,color,width,size);
	this->drawLabel(painter, point,color,width);
}

/**
 * Draws line at image position to imageview from 2DPoints
 * @param start,  end
 */
void CBaristaView::drawLine(QPainter &p, const C2DPoint &start, const C2DPoint &end,
							QColor color, int width)
{
	double xscreen1, yscreen1, xscreen2, yscreen2;

	bool isInside = toScreen(start[0], start[1], xscreen1, yscreen1);
	isInside |= toScreen(end[0], end[1], xscreen2, yscreen2);

	if (isInside)
	{
		// screen position
		QPen pen(color, width);
		p.setPen(pen);
    	p.drawLine(xscreen1, yscreen1, xscreen2, yscreen2);
	}
}

/**
 * Draws label at image position to imageview
 * @param point
 */
void CBaristaView::drawLabel(QPainter &p, const CObsPoint &point, QColor color, int width)
{
	double xscreen, yscreen;

    if (toScreen(point[0], point[1], xscreen, yscreen))
	{
		QPen pen(color, width);

		//p.setFont(QFont("Helvetica", 10, 75));

		p.setPen(pen);

		QString qstring = point.getLabel().GetChar();
		p.drawText(xscreen + 5, yscreen +3, qstring);
	}
}
/**
 * Draws line at image position to imageview
 * @param start,  end
 */
void CBaristaView::drawEdge(QPainter &p, const CXYContour &contour, QColor color,
							QColor endcolor, int width)
{
	QPen pen;
	QBrush brush;
	p.setBackgroundMode(Qt::OpaqueMode);
	double xscreen, yscreen;
	pen.setWidth(width);
	QColor brushEndCol(255, 255, 0, 127);
	QColor brushCol(255, 0, 0, 127);

	for(int j=0; j < contour.getPointCount(); j++)
	{
		C2DPoint *point = contour.getPoint(j);

		if (toScreen((*point)[0], (*point)[1], xscreen, yscreen))
		{
			if (j== 0 || j == contour.getPointCount()-1)
			{
				brush.setColor(brushEndCol);
				pen.setColor(endcolor);
			}
			else
			{
				brush.setColor(brushCol);
				pen.setColor(color);
			}

			p.setPen(pen);
			p.setBrush(brush);

			p.drawRect(xscreen, yscreen, zoomFactor, zoomFactor);
			p.fillRect(xscreen, yscreen, zoomFactor, zoomFactor, brush);
		}
	}

}

/**
 * Draws line at image position to imageview
 * @param p,  poly
 */
void CBaristaView::drawThinnedEdge(QPainter &p,  const CXYPolyLine &poly,
		                          QColor color, int width)
{
	Qt::BGMode mode = p.backgroundMode();
	p.setBackgroundMode(Qt::OpaqueMode);

	C2DPoint point1 = poly.getPoint(0);
	C2DPoint point2;

	for(int j=1; j < poly.getPointCount(); j++)
	{
		point2 = poly.getPoint(j);
		drawLine(p, point1, point2, color, width);
		point1 = point2;
	};

	if (poly.isClosed())
	{
		point1 = poly.getPoint(poly.getPointCount() - 1);
		point2 = poly.getPoint(0);
		drawLine(p, point1, point2, color, width);
	};

	p.setBackgroundMode(mode);
};

void CBaristaView::paintBorders(QPainter &dc)
{
    this->paintHBorder(dc);
    this->paintVBorder(dc);
    this->paintCorner(dc);
    this->paintLeftArrow(dc);
    this->paintRightArrow(dc);
    this->paintUpArrow(dc);
    this->paintDownArrow(dc);
    this->paintHThumb(dc);
    this->paintVThumb(dc);
}

void CBaristaView::paintHBorder(QPainter &dc)
{
    int l = 0;
    int t = this->getViewHeight();

    int w = this->getViewWidth();
    int h = this->scrollbarSize;

    if (w <= 0)
        return;

	dc.drawImage(QRect(l,t,w,h),this->hChannel);
}

void CBaristaView::paintVBorder(QPainter &dc)
{
    if (this->scrollbarSize < 0)
        this->scrollbarSize = this->vChannel.width();

    int l = this->width();
    int t = 0;

    int w = this->scrollbarSize;
    int h = this->height();

    if (h <= 0)
        return;

	dc.drawImage(QRect(l,t,w,h),this->vChannel);
}

void CBaristaView::paintCorner(QPainter &dc)
{
    int l = this->width();
    int t = this->height();

	this->corner.x = l;
	this->corner.y = t;

	dc.drawPixmap(l,t,this->corner);
}

void CBaristaView::paintLeftArrow(QPainter &dc)
{
	int l = 0;
    int t = this->height();

	this->leftArrow.x = l;
	this->leftArrow.y = t;

	dc.drawPixmap(l,t,this->leftArrow);
}

void CBaristaView::paintRightArrow(QPainter &dc)
{
    int l = this->width() - this->rightArrow.width();
    int t = this->height();

	this->rightArrow.x = l;
	this->rightArrow.y = t;

	dc.drawPixmap(l,t,this->rightArrow);
}

void CBaristaView::paintUpArrow(QPainter &dc)
{
    int l = this->width();
    int t = 0;

	this->upArrow.x = l;
	this->upArrow.y = t;

	dc.drawPixmap(l,t,this->upArrow);
}

void CBaristaView::paintDownArrow(QPainter &dc)
{
    int l = this->width();
    int t = this->height() - this->downArrow.height();

	this->downArrow.x = l;
	this->downArrow.y = t;

	dc.drawPixmap(l,t,this->downArrow);
}

void CBaristaView::paintVThumb(QPainter &dc)
{
    int range = this->downArrow.y - this->upArrow.height();
    double ratio = (double)this->height()/(double)this->virtualHeight;

    if (ratio >= 1.0)
    {
        return;
    }

    int thumbHeight = (int)(ratio*range + 0.5);

	if (thumbHeight < (this->topThumb.height() + this->bottomThumb.height()) )
		thumbHeight = this->topThumb.height() + this->bottomThumb.height();

    range -= thumbHeight;

    int pos = (int)((double)this->scrollY*(double)range/(double)(this->virtualHeight-this->height()) + 0.5);

    pos += this->upArrow.height();

    this->topThumb.x = this->width();
    this->bottomThumb.x = this->width();
    int vThumbX = this->width();

    this->topThumb.y = pos;
    this->bottomThumb.y = pos + thumbHeight - this->bottomThumb.height() ;
    int vThumbY = pos +  this->topThumb.height();


	this->topThumb.draw(&dc);
	this->bottomThumb.draw(&dc);

	thumbHeight -= this->topThumb.height() + this->bottomThumb.height();
	dc.drawImage(QRect(vThumbX,vThumbY,this->vThumbNib.width(),thumbHeight),this->vThumbNib);

}

void CBaristaView::paintHThumb(QPainter &dc)
{
    int l = this->width();

    // from right to left arrow
    int range = this->rightArrow.x - this->leftArrow.width();
    double ratio = (double)this->width()/(double)this->virtualWidth;

    if (ratio >= 1.0)
    {
        return;
    }

    int thumbWidth = (int)(ratio*range + 0.5) ;

	if (thumbWidth < (this->leftThumb.width() + this->rightThumb.width()) )
		thumbWidth = this->leftThumb.width() + this->rightThumb.width();

    range -= thumbWidth;
	int pos = (int)((double)this->scrollX*(double)range/(double)(this->virtualWidth-this->width()) + 0.5);
    pos += this->leftArrow.width();

    this->leftThumb.y = this->height();
    this->rightThumb.y = this->height();
    int hThumbY = this->height();

    this->leftThumb.x = pos;
    this->rightThumb.x = pos + thumbWidth - this->rightThumb.width();
    int hThumbX = pos + this->leftThumb.width();

	this->leftThumb.draw(&dc);
	this->rightThumb.draw(&dc);

	thumbWidth -= this->leftThumb.width() + this->rightThumb.width();
	dc.drawImage(QRect(hThumbX,hThumbY,thumbWidth,this->hThumbNib.height()),this->hThumbNib);
}


bool CBaristaView::inVBar(const C2DPoint &point)
{
    return false;
}

bool CBaristaView::inHBar(const C2DPoint &point)
{
    return false;
}

bool CBaristaView::onVThumb(const C2DPoint &point)
{
    if ((point.x < this->topThumb.x) || (point.x > (this->topThumb.x + this->topThumb.width())) ||
		(point.y < this->topThumb.y) || (point.y > (this->bottomThumb.y + this->bottomThumb.height())))
		return false;
	
	return true;

}

bool CBaristaView::onHThumb(const C2DPoint &point)
{
    if ((point.x < this->leftThumb.x) || (point.x > (this->rightThumb.x + this->rightThumb.width())) ||
		(point.y < this->leftThumb.y) || (point.y > (this->leftThumb.y + this->leftThumb.height())))
		return false;

	return true;
}

bool CBaristaView::onDownArrow(const C2DPoint &point)
{
    if (point.x < this->downArrow.x)
        return false;

    if (point.x >= this->downArrow.x + this->downArrow.width())
        return false;

    if (point.y < this->downArrow.y)
        return false;

    if (point.y >= this->downArrow.y + this->downArrow.height())
        return false;
    return true;
}

bool CBaristaView::onUpArrow(const C2DPoint &point)
{
    if (point.x < this->upArrow.x)
        return false;

    if (point.x >= this->upArrow.x + this->upArrow.width())
        return false;

    if (point.y < this->upArrow.y)
        return false;

    if (point.y >= this->upArrow.y + this->upArrow.height())
        return false;
    return true;
}

bool CBaristaView::onLeftArrow(const C2DPoint &point)
{
    if (point.x < this->leftArrow.x)
        return false;

    if (point.x >= this->leftArrow.x + this->leftArrow.width())
        return false;

    if (point.y < this->leftArrow.y)
        return false;

    if (point.y >= this->leftArrow.y + this->leftArrow.height())
        return false;
    return true;
}

bool CBaristaView::onRightArrow(const C2DPoint &point)
{
    if (point.x < this->rightArrow.x)
        return false;

    if (point.x >= this->rightArrow.x + this->rightArrow.width())
        return false;

    if (point.y < this->rightArrow.y)
        return false;

    if (point.y >= this->rightArrow.y + this->rightArrow.height())
        return false;
    return true;
}

bool CBaristaView::onVBarArea(const C2DPoint &point)
{
	if (point.x < this->width())
		return false;

	if (point.x >= this->width() + this->scrollbarSize)
		return false;

	if (point.y < this->upArrow.y + this->upArrow.height())
		return false;

	if (point.y > this->downArrow.y)
		return false;

	if (this->onDownArrow(point))
		return false;

	if (this->onUpArrow(point))
		return false;

	if (this->onVThumb(point))
		return false;
	return true;
}

bool CBaristaView::onHBarArea(const C2DPoint &point)
{
	if (point.y < this->height())
		return false;

	if (point.y >= this->height() + this->scrollbarSize)
		return false;

	if (point.x < this->leftArrow.x + this->leftArrow.width())
		return false;

	if (point.x > this->rightArrow.x)
		return false;

	if (this->onLeftArrow(point))
		return false;

	if (this->onRightArrow(point))
		return false;

	if (this->onHThumb(point))
		return false;
	return true;
}
 bool CBaristaView::onScrollBarArea(const C2DPoint &point)
    {
        return this->onVThumb(point) ||
        this->onHThumb(point) ||
        this->onDownArrow(point) ||
        this->onUpArrow(point) ||
        this->onLeftArrow(point) ||
        this->onRightArrow(point) ||
        this->onVBarArea(point) ||
        this->onHBarArea(point);
    };

/**
 * handle mouse wheel event
 * @param e
 */
void CBaristaView::wheelEvent ( QWheelEvent * e )
{
	int dx,dy;
    int x = this->getScrollX();
    int y = this->getScrollY();

    // performs a zoom in or out
    if (e->delta() < 0)
    {
        this->zoomUp();
        dx = e->x();
        dy = e->y();

        this->setScrollX(x*2 + dx);
        this->setScrollY(y*2 + dy);
    }
    else
    {
        this->zoomDown();
        dx = e->x();
        dy = e->y();
        this->setScrollX(x/2 - dx/2);
        this->setScrollY(y/2 - dy/2);
    }

	if (this->inGeoLinkMode && currentBaristaViewHandler)
		currentBaristaViewHandler->doGeoLink(this);

	this->updateStatusBarInfo();
}

void CBaristaView::keyReleaseEvent ( QKeyEvent * e )
{

	
	if (currentBaristaViewHandler)
		currentBaristaViewHandler->keyReleaseEvent(e, this);

	this->updateStatusBarInfo();

	bool changeQImages = false;
	// to increase/decrease brightness
	if ( e->key() == Qt::Key_Up )
	{
		this->changeBrightness(1.0);
		changeQImages = true;
	}
	else if ( e->key() == Qt::Key_Down)
	{
		this->changeBrightness(-1.0);
		changeQImages = true;
	}

	if (changeQImages)
	{
		this->clearImages();
		this->update();
	}


/*	else if (e->key() == Qt::Key_W)
	{
		baristaProjectHelper.setIsBusy(true);
		CWaterShed watershed;
		CHugeImage* himage = this->getHugeImage();
		CImageBuffer buffer(0,0,himage->getWidth(),himage->getHeight(),1,8,true);
		himage->getRect(buffer);	

		if (watershed.compute(buffer))
		{
			CFileName fn("testCombi2.tif");
			fn.SetPath(project.getFileName()->GetPath());
			CVImage *IMG = project.addImage(fn, false);
			CLookupTable lut(himage->getTransferFunction());
			CCharString str = IMG->getName() + ".hug";
			if (!IMG->getHugeImage()->dumpImage(str.GetChar(), buffer, &lut))
				project.deleteImage(IMG);
				
		}

		baristaProjectHelper.setIsBusy(false);
		baristaProjectHelper.refreshTree();
	}
*/
}


void CBaristaView::keyPressEvent ( QKeyEvent * e )
{
	if (currentBaristaViewHandler)
		currentBaristaViewHandler->keyPressEvent(e, this);

	this->updateStatusBarInfo();
	this->update();
}

void CBaristaView::resetFirstSecondClick()
{
	this->firstClick.x = this->firstClick.y = -1;
	this->secondClick.x = this->secondClick.y = -1;
}


void CBaristaView::enterEvent ( QEvent * event )
{
	if (event->type() == QEvent::Enter)
	{
		this->CtrlDown = currentBaristaViewHandler->getCTRLisDown();
		currentBaristaViewHandler->handleEnterEvent(event,this);

		this->setFocus(Qt::MouseFocusReason);

	}

}

void CBaristaView::leaveEvent ( QEvent * event )
{
	if (event->type() == QEvent::Leave)
	{
		this->CtrlDown = false;
		currentBaristaViewHandler->handleLeaveEvent(event,this);
		this->update();
	}
}


/**
 * Handle mouse button press
 * @param e
 */
void CBaristaView::mousePressEvent ( QMouseEvent * e )
{
    C2DPoint point(e->x(), e->y()); // e->x() returns the x position of the mouse cursor, relative to the widget that received the event. 

	// following if-else statements check the appropriate action to be taken by checking the current mouse position where mouse was pressed
    if (this->onVThumb(point))
    {
        this->hThumbPressed = false;
        this->vThumbPressed = true;
    }
    else if (this->onHThumb(point))
    {
        this->hThumbPressed = true;
        this->vThumbPressed = false;
    }
    else if (this->onLeftArrow(point))
    {
        this->leftArrowPressed = true;
    }
    else if (this->onRightArrow(point))
    {
        this->rightArrowPressed = true;
    }
    else if (this->onUpArrow(point))
    {
        this->upArrowPressed = true;
    }
    else if (this->onDownArrow(point))
    {
        this->downArrowPressed = true;
    }
	else
	{
		if (e->button() == Qt::LeftButton) this->lButtonDown  = true;
		else if (e->button() == Qt::RightButton) this->rButtonDown  = true;
		else if (e->button() == Qt::MidButton) this->mButtonDown  = true;

		currentBaristaViewHandler->handleMouseButtonEvent(e, this); // call mouse button event handler
	}


// general use for ZOOMIN & ZOOMOUT
    this->xPressed = e->x();
    this->yPressed = e->y();

    this->xPosPressed = this->getScrollX();
    this->yPosPressed = this->getScrollY();

	this->updateStatusBarInfo();
}


/**
 * Handle mouse button release
 * @param e
 */
void CBaristaView::mouseReleaseEvent ( QMouseEvent * e )
{
    C2DPoint point(e->x(), e->y()); // e->x() returns the x position of the mouse cursor, relative to the widget that received the event. 


	if (this->vThumbPressed || this->hThumbPressed || this->leftArrowPressed ||
		this->rightArrowPressed ||  this->upArrowPressed || this->downArrowPressed )
	{
		// do nothing!
	}
    else if (this->onLeftArrow(point))// && this->leftArrowPressed )
    {
        double ratio = (double)this->width()/(double)this->virtualWidth;
        ratio = Max(1, 1/ratio);

        this->setScrollX(this->scrollX - 10*ratio);
    }
    else if (this->onRightArrow(point))// && this->rightArrowPressed)
    {
        double ratio = (double)this->width()/(double)this->virtualWidth;
        ratio = Max(1, 1/ratio);

        this->setScrollX(this->scrollX + 10*ratio);
    }
    else if (this->onUpArrow(point))// && this->upArrowPressed)
    {
        double ratio = (double)this->height()/(double)this->virtualHeight;
        ratio = Max(1, 1/ratio);

        this->setScrollY(this->scrollY - 10*ratio);
    }
    else if (this->onDownArrow(point))// && this->downArrowPressed)
    {
        double ratio = (double)this->height()/(double)this->virtualHeight;
        ratio = Max(1, 1/ratio);

        this->setScrollY(this->scrollY + 10*ratio);
    }
	else if (this->onVBarArea(point))
	{
        double ratio = (double)this->height()/(double)this->virtualHeight;
        ratio = Max(1, 1/ratio);
		if (point.y < this->topThumb.y)
		{
			this->setScrollY(this->scrollY - 100*ratio);
		}
		else
		{
			this->setScrollY(this->scrollY + 100*ratio);
		}
	}
	else if (this->onHBarArea(point))
	{
        double ratio = (double)this->height()/(double)this->virtualHeight;
        ratio = Max(1, 1/ratio);

		if (point.x < this->leftThumb.x)
		{
			this->setScrollX(this->scrollX - 100*ratio);
		}
		else
		{
			this->setScrollX(this->scrollX + 100*ratio);
		}
	}
	else if (this->onHThumb(point))
	{
	}
	else if (this->onVThumb(point))
	{
		
	}
	else
	{
		 if (e->button() == Qt::LeftButton)
		{
			this->lButtonDown  = false;
		}
		else if (e->button() == Qt::RightButton)
		{
			this->rButtonDown = false;
		}
		else if (e->button() == Qt::MidButton)
		{
			this->mButtonDown = false;
		}

		currentBaristaViewHandler->handleMouseButtonEvent(e, this);

	}

    this->vThumbPressed = false;
    this->hThumbPressed = false;

    this->rightArrowPressed = false;
    this->leftArrowPressed = false;
    this->upArrowPressed = false;
    this->downArrowPressed = false;

	this->update(); // update the View
	this->updateStatusBarInfo();

	baristaProjectHelper.refreshTree();
}

void CBaristaView::mouseMoveEvent ( QMouseEvent * e )
{
	C2DPoint point(e->x(), e->y());

	// set cursor
	if (this->onScrollBarArea(point) || this->vThumbPressed || this->hThumbPressed || this->leftArrowPressed ||
		this->rightArrowPressed ||  this->upArrowPressed || this->downArrowPressed )
    {
		this->setArrowCursor();
	}
	else
	{
		currentBaristaViewHandler->setCurrentCursor(this);
	}


	if (this->vThumbPressed)
    {
        double ratio = (double)this->height()/(double)this->virtualHeight;

		int yDiff = this->yPressed - point.y;

        int yPos = this->yPosPressed;
        yPos -= (int)(yDiff/ratio+0.5);
        this->setScrollY(yPos);

		if (this->inGeoLinkMode && currentBaristaViewHandler)
			currentBaristaViewHandler->doGeoLink(this);

		return;
    }
    else if (this->hThumbPressed)
    {
        double ratio = (double)this->width()/(double)this->virtualWidth;

        int xDiff = this->xPressed - point.x;

        int xPos = this->xPosPressed;
        xPos -= (int)(xDiff/ratio+0.5);
        this->setScrollX(xPos);

		if (this->inGeoLinkMode && currentBaristaViewHandler)
			currentBaristaViewHandler->doGeoLink(this);

		return;
    }
	else
	{
		currentBaristaViewHandler->handleMouseMoveEvent(e, this);
	}

	this->fromScreen(point.x, point.y, cpos.x, cpos.y);

	this->updateStatusBarInfo();
}


void CBaristaView::getPixelInfoHugeImage(QString& pixelInfo,
										 QString& geoInfo,
										 QString& colorInfo,
										 QString& zoomInfo,
										 QString& residualInfo,
										 QString& brightnessInfo,
										 CHugeImage* himage)
{


	// make string for pixel coordinates
	double x = this->cpos.x;
	double y = this->cpos.y;
	pixelInfo.sprintf(" [pixel]: %10.2lf, %10.2lf",x,y);

	// make string for color info
	this->retrieveGreyValues(cpos.x,cpos.y,himage);

	int bands = himage->getBands();

	if (bands == 1)
	{
		if (himage->getBpp() != 32)
			colorInfo.sprintf(" [grey]: %4ld", (int)this->red);
		else
			colorInfo.sprintf(" [grey]: %5.3lf", this->red);
	}
	else if (bands == 3)
	{
		if (himage->getBpp() != 32)
			colorInfo.sprintf(" [rgb]: %4ld, %4ld, %4ld",(int)this->red, (int)this->green, (int)this->blue);
		else
			colorInfo.sprintf(" [rgb]: %5.3lf, %5.3lf, %5.3lf",this->red, this->green, this->blue);
	}
	else
	{
		if (himage->getBpp() != 32)
			colorInfo.sprintf(" [rgbn]: %4ld, %4ld, %4ld, %4ld",(int)this->red, (int)this->green,(int) this->blue,(int) this->nir);
		else
			colorInfo.sprintf(" [rgbn]: %5.3lf, %5.3lf, %5.3lf, %5.3lf",this->red, this->green, this->blue, this->nir);
	}

	// make string for zoom, residualfactor and brightness
	zoomInfo.sprintf(" [zoom]: %.2lf", this->zoomFactor);
	residualInfo.sprintf(" [residual]: %.2lf", this->residualFactor);
	brightnessInfo.sprintf(" [brightness]: %d", himage->getBrightness());
}



bool CBaristaView::addPointDrawing(const CLabel& dataSetName,CObsPoint* p)
{
	if (this->drawings.addPoint(dataSetName,p))
		this->update();

	return true;
}

bool CBaristaView::removePointDrawing(const CLabel& dataSetName,const CLabel& p)
{
	if (this->drawings.removePoint(dataSetName,p))
		this->update();

	return true;
}


bool CBaristaView::queryPointDrawing(const CLabel& dataSetName,const CLabel & pointLabel)
{
	return this->drawings.queryPoint(dataSetName,pointLabel);
}

bool CBaristaView::addPolyLineDrawing(const CLabel& dataSetName,CXYPolyLine* p,const QColor& color,unsigned int lineWidth)
{
	int c[3];
	c[0] = color.red();
	c[1] = color.green();
	c[2] = color.blue();

	if (this->drawings.addPolyLine(dataSetName,p,c,lineWidth))
		this->update();

	return true;
}

bool CBaristaView::removePolyLineDrawing(const CLabel& dataSetName,CXYZPolyLine* p)
{
	if (this->drawings.removePolyLine(dataSetName,p))
		this->update();

	return true;
}


bool CBaristaView::addDrawingDataSet(CDrawingDataSet& dataSet)
{
	if (this->drawings.addDataSet(dataSet))
		this->update();

	return true;
}
	
bool CBaristaView::addDrawingDataSet(const CLabel& dataSetName, int dim, bool showPoints, bool showPointLabels, QColor pointColor)
{
	int c[3];
	c[0] = pointColor.red();
	c[1] = pointColor.green();
	c[2] = pointColor.blue();

	return this->drawings.addDataSet(dataSetName,dim,showPoints,showPointLabels,c);
};
	
QColor CBaristaView::getPointColor(const CLabel& dataSetName)
{
	int *c = this->drawings.getPointColor(dataSetName);
	QColor col(c[0], c[1], c[2]);
	return col;
};

bool CBaristaView::removeDrawingDataSet(const CLabel& dataSetName)
{
	if (this->drawings.removeDataSet(dataSetName))
		this->update();

	return true;
}



void CBaristaView::removeAllDrawingDataSets()
{
	this->drawings.removeAllDataSets();
	this->update();
}

bool CBaristaView::addTopology(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2,QColor& color,unsigned int lineWidth)
{
	int c[3];
	c[0] = color.red();
	c[1] = color.green();
	c[2] = color.blue();

	if (this->drawings.addTopology(dataSetName,p1,p2,c,lineWidth))
		this->update();

	return true;
}

bool CBaristaView::removeTopology(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2)
{
	if (this->drawings.removeTopology(dataSetName,p1,p2))
		this->update();

	return true;
}


// change color
void CBaristaView::changeTopologyColor(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2,QColor& color)
{
	int c[3];
	c[0] = color.red();
	c[1] = color.green();
	c[2] = color.blue();

	this->drawings.changeTopologyColor(dataSetName,p1,p2,c);
}

void CBaristaView::changeAllTopologyColors(const CLabel& dataSetName,QColor& color)
{
	int c[3];
	c[0] = color.red();
	c[1] = color.green();
	c[2] = color.blue();

	this->drawings.changeAllTopologyColors(dataSetName,c);
	this->update();
}


// change line width
void CBaristaView::changeTopologyLineWidth(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2,unsigned int lineWidth)
{
	this->drawings.changeTopologyLineWidth(dataSetName,p1,p2,lineWidth);
}

void CBaristaView::changeAllTopologyLineWidth(const CLabel& dataSetName,unsigned int lineWidth)
{
	this->drawings.changeAllTopologyLineWidth(dataSetName,lineWidth);
	this->update();
}


void CBaristaView::doFirstDraw(int virtualHeight, int virtualWidth)
{

	if (this->firstPaint)
	{
		this->firstPaint = false;

		if (currentBaristaViewHandler)
			currentBaristaViewHandler->initialize(this);

		this->zoomToFit();
	}
}

bool CBaristaView::getShowPoints(const CLabel& dataSetName)
{
	return this->drawings.getShowPoints(dataSetName);
};

void CBaristaView::setShowPoints(const CLabel& dataSetName,bool b)
{
	this->drawings.setShowPoints(dataSetName,b);
	this->update();
};

bool CBaristaView::getShowPointLabels(const CLabel& dataSetName)
{
	return this->drawings.getShowPointLabels(dataSetName);
};

void CBaristaView::setShowPointLabels(const CLabel& dataSetName,bool b)
{
	this->drawings.setShowPointLabels(dataSetName,b);
	this->update();
};


void CBaristaView::setPointColor(const CLabel& dataSetName,QColor& color)
{
	int c[3];
	c[0] = color.red();
	c[1] = color.green();
	c[2] = color.blue();

	this->drawings.setPointColor(dataSetName,c);
	this->update();
};

bool CBaristaView::renameDataSet(const CLabel& oldName,const CLabel& newName)
{
	return this->drawings.renameDataSet(oldName,newName);
}

void CBaristaView::addPopUpAction(QAction* action, bool addSeparator)
{
	if (this->contextMenu)
	{
		this->contextMenu->addAction(action);

		if (addSeparator)
			this->contextMenu->addSeparator();
	}
}

void CBaristaView::addContextMenuSeparator()
{
	if (this->contextMenu)
	{
		this->contextMenu->addSeparator();
	}
}

void CBaristaView::createPopUpMenu()
{
	if (this->contextMenu)
		delete this->contextMenu;

	this->contextMenu = new QMenu(this);

	if (this->getHugeImage())
	{
		// adds action for all image views
		QAction* setChannelDisplayAction= new QAction(tr("Set Channel Content"), this);
		connect(setChannelDisplayAction, SIGNAL(triggered()), this, SLOT(setChannelDisplay()));

		
		this->addPopUpAction(setChannelDisplayAction, true);
/*
		if (!(this->getHugeImage()->getBpp() == 32))
		{
			QAction* wallisFilterAction= new QAction(tr("&Wallis Filter"), this);
			connect(wallisFilterAction, SIGNAL(triggered()), this, SLOT(callWallisFilterDlg()));

			this->addPopUpAction(wallisFilterAction,true);
		}
*/
		QAction* setPRDisplayAction= new QAction(tr("Set Point/Residual Plot"), this);
		connect(setPRDisplayAction, SIGNAL(triggered()), this, SLOT(setPRDisplay()));

		
		this->addPopUpAction(setPRDisplayAction, true);	
	}


}

void CBaristaView::deletePopUpMenu()
{
	if (this->contextMenu)
	{
		this->contextMenu->clear();

		delete this->contextMenu;
		this->contextMenu = NULL;
	}

}

void CBaristaView::showPopUpMenu()
{
	if (this->contextMenu)
		this->contextMenu->exec(QCursor::pos());
}

void CBaristaView::unselectROI()
{
	this->hasROI = false;
	this->haslargeROI = false;
	this->showROI = false;
	this->resetFirstSecondClick();
}

void CBaristaView::drawXYPointArray(const CXYPointArray* xyPointArray,
									QPainter &p, shwLabel LablelType, shwPoint PointType,
									QColor LabelColor, QColor PointColor,
									int Width, 
									int Size,								
									int LabelWidth)
{
	int pointCount = xyPointArray->GetSize();

	for (int i = 0; i < pointCount; i++)
	{
		CObsPoint* point = xyPointArray->GetAt(i);

		if (PointType == pCross && LablelType == lOn)
		{
			this->drawCross(p, point,PointColor,Width,Size);
			this->drawLabel(p, point,LabelColor,LabelWidth);
		}
		else if (PointType == pCross && LablelType == lOff)
		{
			this->drawCross(p, point,PointColor,Width,Size);
		}
		else if (PointType == pDot && LablelType == lOn)
		{
			this->drawCircle(p, point,PointColor,Width,Size);
			this->drawLabel(p, point,LabelColor,LabelWidth);
		}
		else if (PointType == pDot  && LablelType == lOff)
		{
			this->drawCircle(p, point,PointColor,Width,Size);
		}		
		else if (PointType == pNoPoint  && LablelType == lOn)
		{
			this->drawLabel(p, point,LabelColor,LabelWidth);
		}
		else
		{//do nothing
		}
			
	}

}

void CBaristaView::drawXYContours(CXYContourArray* contours,
									QPainter &p,
									QColor color,
									QColor endcolor,
									int width)
{
	for (int i = 0; i < contours->GetSize(); i++)
	{
		CXYContour* contour = contours->getLine(i);

		this->drawEdge(p,*contour,color,endcolor,width);

	//	this->drawLabel(&p2, point);

		}
}

void CBaristaView::drawXYPolyLines(	CXYPolyLineArray* polyLines,
									QPainter &p,
									QColor color,
									int width)
{
		for (int i = 0; i < polyLines->GetSize(); i++)
		{
			CXYPolyLine* poly = polyLines->getLine(i);

			this->drawThinnedEdge(p, *poly,color,width);
		}
}

void CBaristaView::drawEllipses(CXYEllipseArray* ellipses,
								QPainter &p,
								QColor color,
								int width)
{
	for (int i = 0; i < ellipses->GetSize(); i++)
	{
		CXYEllipse* ellipse = ellipses->getEllipse(i);

		C2DPointArray boarder;
		ellipse->getBoarderPoints(&boarder);

		for (int j = 0; j < boarder.GetSize()-1; j++)
		{
			this->drawLine(p, *boarder.GetAt(j), *boarder.GetAt(j+1) );
		}

		// close ellipse boarder
		if (boarder.GetSize() > 1)
		{
			int last = boarder.GetSize();
			this->drawLine(p, *boarder.GetAt(last-1),*boarder.GetAt(0) );
		}
	}
}



/**
 * Select ROI for processing
 * @param
 */
void CBaristaView::selectROI()
{
	double xmin = min(this->firstClick.x,this->secondClick.x);
	double xmax = max(this->firstClick.x,this->secondClick.x);
	double ymin = min(this->firstClick.y,this->secondClick.y);
	double ymax = max(this->firstClick.y,this->secondClick.y);

	if ((xmax - xmin) < 2.0 || (ymax - ymin) < 2.0)
	{
		this->resetFirstSecondClick();
		return;
	}

	this->roiUL.x = xmin;
	this->roiUL.y = ymin;
	this->roiLR.x = xmax;
	this->roiLR.y = ymax;

	// if not too big, fill ROI with image data
	if( xmax-xmin > 4000 || ymax-ymin > 4000)
	{
		this->haslargeROI = true;
	}
	else
	{
		if (this->getHugeImage())
			this->getHugeImage()->setRoi(xmin,ymin,xmax-xmin,ymax-ymin);
		this->haslargeROI = false;
	}

	this->showROI = false;
	this->hasROI = true;
	this->resetFirstSecondClick();
}

void CBaristaView::zoomUp()
{
    this->setZoomFactor(this->zoomFactor*2.0f);
	this->clearImages();
	this->setVirtualHeight();
	this->setVirtualWidth();
	this->update();
}

void CBaristaView::zoomDown()
{
    this->setZoomFactor(this->zoomFactor /2.0f);
	this->clearImages();
	this->setVirtualHeight();
	this->setVirtualWidth();
	this->update();
}

void CBaristaView::setVirtualWidth()
{
    int imageWidth = this->getDataWidth();
    int w = this->width();
    this->virtualWidth = max(w, imageWidth);
}

void CBaristaView::setVirtualHeight()
{
    int imageHeight = this->getDataHeight();
    int h = this->height();
    this->virtualHeight = max(h, imageHeight);
}


void CBaristaView::zoomToFit()
{

    int imageWidth = this->getDataWidth();

    int w = this->getViewWidth();

	while ( imageWidth > w )
	{
		this->setZoomFactor(this->zoomFactor / 2.0f);
		imageWidth = this->getDataWidth();
	}

	this->clearImages();
	this->update();
}

void CBaristaView::elementAdded(CLabel element)
{
	this->update();
}

void CBaristaView::elementDeleted(CLabel element)
{
	this->update();
}

void CBaristaView::closeEvent ( QCloseEvent * e )
{
	this->clearImages();

    e->accept();
	QWidget::close();

}


void CBaristaView::setChannelDisplay()
{
	bui_ChannelSwitchBox channelswitchbox(this);
	channelswitchbox.exec();
}


void CBaristaView::setPRDisplay()
{
	Bui_ImgShowDlg PRdisplay(this);
	PRdisplay.exec();
}


/**
 * retrieve grey values for display
 * with zoomer
 * @param e
 */
void CBaristaView::retrieveGreyValues(int x,int y,CHugeImage* himage)
{

	unsigned char* buf = new unsigned char[himage->pixelSizeBytes()];
	unsigned short* bufUS = (unsigned short *) buf;
	float* bufF = (float *) buf;

	if ((x >= 0) && (y >= 0) && (x < himage->getWidth()) && (y < himage->getHeight()))
	{
		himage->getRect(buf, y, 1, x, 1);

		if ( (himage->getBands() == 3) || (himage->getBands() == 4))
		{
			if (himage->getBpp() == 8)
			{
				this->red = buf[0];
				this->green = buf[1];
				this->blue = buf[2];

				if (himage->getBands() == 4)
					this->nir = buf[3];
			}
			else if (himage->getBpp() == 16)
			{
				this->red = bufUS[0];
				this->green = bufUS[1];
				this->blue = bufUS[2];

				if (himage->getBands() == 4)
					this->nir = bufUS[3];
			}
		}
		else if (himage->getBands() == 1)
		{
			if (himage->getBpp() == 8)
			{
				this->red = buf[0];
				this->green = buf[0];
				this->blue = buf[0];
			}
			else if (himage->getBpp() == 16)
			{
				this->red = bufUS[0];
				this->green = bufUS[0];
				this->blue = bufUS[0];
			}
			else if (himage->getBpp() == 32)
			{
				this->red = bufF[0];
				this->green = bufF[0];
				this->blue = bufF[0];
			}
		}
	}

	delete [] buf;
}

void CBaristaView::clearImages()
{
    while (this->qimages.count() > 0)
		this->clearImage(0);
}

void CBaristaView::clearImage(int index)
{
     QImage* qimage = (QImage*)this->qimages.at(index);
     QPoint* offset = (QPoint*)this->offsets.at(index);

     delete qimage;
     delete offset;

     this->qimages.removeAt(index);
     this->offsets.removeAt(index);
}

void CBaristaView::changeBrightness(double factor)
{
	CHugeImage* himage = this->getHugeImage();	
	if (himage)
	{	
		himage->changeBrightness(factor*10); // change in hugefile
	}


}

int CBaristaView::getDataWidth(bool considerZoom) const
{
	CHugeImage* himage = this->getHugeImage();
	if (himage)
		return himage->getWidth()* (considerZoom ? this->zoomFactor : 1.0);
	else
		return 0;
}

int CBaristaView::getDataHeight(bool considerZoom) const
{
	CHugeImage* himage = this->getHugeImage();
	if (himage)
		return himage->getHeight()*(considerZoom ? this->zoomFactor : 1.0);
	else
		return 0;
}

void CBaristaView::CropROI()
{
}

void CBaristaView::ResetRoi()
{
}

void CBaristaView::updateStatusBarInfo()
{
	// show information in mainwindow statusbar
	if (this->cpos.x != -1)
	{
		QString pixelInfo("");
		QString geoInfo("");
		QString colorInfo("");
		QString zoomInfo("");
		QString residualInfo("");
		QString brightnessInfo("");

		CHugeImage* himage = this->getHugeImage();
		if (himage)
			this->getPixelInfoHugeImage(pixelInfo,geoInfo,colorInfo,zoomInfo,residualInfo,brightnessInfo,himage);

		currentBaristaViewHandler->setStatusBarText(pixelInfo);

	}
}



void CBaristaView::activateGeoLinkMode() 
{
	this->inGeoLinkMode = true;
	
}

void CBaristaView::deactivateGeoLinkMode()
{
	this->inGeoLinkMode = false;
	
}

bool CBaristaView::computeViewCentre(C2DPoint& centre)
{
	this->fromScreen(this->getViewWidth()/2,this->getViewHeight()/2,centre.x,centre.y);
	
	if (centre.x > this->getDataWidth(false) || centre.y > this->getDataHeight(false))
		return false;
	else
		return true;
}

float CBaristaView::getDataResolution() 
{
	
	if (this->dataResolution < 0.0) // has not yet been computed
	{
		// we monoplot two points and compute the 3D difference to get the reslution
		CMonoPlotter& mp = this->getMonoPlotter();
		
		// use the view centre
		C2DPoint p1_2D,p2_2D;
		if (!this->computeViewCentre(p1_2D))
		{
			// ok then just a point in the centre
			p1_2D.x = this->getDataWidth(false) / 2;
			p1_2D.y = this->getDataHeight(false) / 2;
		}

		// point next to first point
		p2_2D.x = p1_2D.x + 1;
		p2_2D.y = p1_2D.y;
		
		CXYZPoint p1_3D,p2_3D;

		if (mp.getPoint(p1_3D,p1_2D,1.0) && mp.getPoint(p2_3D,p2_2D,1.0))
		{
			// we need a metrical value, so transform if needed
			if (mp.getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
			{
				mp.getReferenceSystem().geographicToGeocentric(p1_3D,p1_3D);
				mp.getReferenceSystem().geographicToGeocentric(p2_3D,p2_3D);
			}
			
			C3DPoint diff(p1_3D.x - p2_3D.x,p1_3D.y - p2_3D.y,p1_3D.z - p2_3D.z);
			this->dataResolution = float(diff.getNorm());
		}
		
	}

	return this->dataResolution;
}



CMonoPlotter& CBaristaView::getMonoPlotter()
{
	if ((this->monoPlotter.getVDEM() != project.getActiveDEM())  ||
		(this->getVBase() && (this->monoPlotter.getModel() != this->getVBase()->getModel(eLSMTFW))) ||
		 (! this->getVBase() && (this->monoPlotter.getModel() != this->getCurrentSensorModel())))
	{
		this->monoPlotter.setVDEM(project.getActiveDEM(),false);

		if (this->getVBase())
			this->monoPlotter.setModel(this->getVBase()->getModel(eLSMTFW));
		else
			this->monoPlotter.setModel(this->getCurrentSensorModel());		
	}

	return this->monoPlotter;
}

void CBaristaView::doGeoLink(const CXYZPoint& point3D,const CReferenceSystem &refSysOf3DPoint,double zoomFactor,float pixelResolution,bool changeResampling)
{
	// tell the thread to start
	this->geoLinkThread.doGeoLink(point3D,refSysOf3DPoint,zoomFactor,pixelResolution,changeResampling);	
}

void CBaristaView::changeResamplingType(eResamplingType newType)
{
	if (this->getHugeImage() && !this->resamplingReset)
	{
		this->resamplingReset = true;
		this->currentResamplingType = this->getHugeImage()->getResamplingType();
		this->clearImages();
		this->getHugeImage()->setResamplingType(newType);
		
	}
}

void CBaristaView::resetResamplingType()
{
	if (this->getHugeImage() && this->resamplingReset )
	{
		this->resamplingReset = false;

		if (this->getHugeImage()->getResamplingType() != this->currentResamplingType)
		{
			this->clearImages();	// force view to recreate the pixmaps	
			this->getHugeImage()->setResamplingType(this->currentResamplingType);
		}
	}

}


void CBaristaView::callWallisFilterDlg()
{
	Bui_WallisFilterDlg dlg(this);

	dlg.exec();

}

/**
 * Draws scale bar label at image position to imageview
 * @param point
 */
void CBaristaView::drawScaleLabel(QPainter &p, const CObsPoint &point, QColor color, int width, QString lbl)
{
	double xscreen, yscreen;

    if (toScreen(point[0], point[1], xscreen, yscreen))
	{
		QPen pen(color, width);

		p.setFont(QFont("Helvetica", 14));

		p.setPen(pen);

		//QString qstring = point.getLabel().GetChar();
		p.drawText(xscreen, yscreen, lbl);
	}
}
