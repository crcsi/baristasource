
#include "HeightOptions.h"
#include "BaristaProject.h"
#include "Icons.h"


extern CBaristaProject project;

HeightOptions::HeightOptions(QWidget *parent):QDialog(parent)
{
	setupUi(this);
	this->DefaultSet();

	connect(this->DEM,SIGNAL(released()),this,SLOT(DEMButtonclicked()));
	connect(this->ProjectHeight,SIGNAL(released()),this,SLOT(ProjectHeightButtonclicked()));
	connect(this->IndividualPoint,SIGNAL(released()),this,SLOT(SinglePointButtonclicked()));
	connect(this->Ok,SIGNAL(released()),this,SLOT(Okclicked()));
	connect(this->Cancel,SIGNAL(released()),this,SLOT(Cancelclicked()));

	project.setMeanHeightMonoplotting(false);
	project.setHeightPointMonoplotting(false);
	//project.setHeightDEMMonoplotting(false);
	this->success=false;

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

};

HeightOptions::~HeightOptions()
{
};

void HeightOptions::DefaultSet()
{
    this->DEM->setChecked(true);
	project.setMeanHeightMonoplotting(false);
	project.setHeightPointMonoplotting(false);
	project.setHeightDEMMonoplotting(true);
 }

void HeightOptions::DEMButtonclicked()
{
	project.setMeanHeightMonoplotting(false);
	project.setHeightPointMonoplotting(false);
	project.setHeightDEMMonoplotting(true);

};

void HeightOptions::SinglePointButtonclicked()
{
	project.setMeanHeightMonoplotting(false);
	project.setHeightPointMonoplotting(true);
	project.setHeightDEMMonoplotting(false);
}

void HeightOptions::ProjectHeightButtonclicked()
{
	project.setMeanHeightMonoplotting(true);
	project.setHeightPointMonoplotting(false);
	project.setHeightDEMMonoplotting(false);

}

void HeightOptions::Okclicked()
{
	this->close();
	this->success=true;
}
void HeightOptions::Cancelclicked()
{
	this->close();
}