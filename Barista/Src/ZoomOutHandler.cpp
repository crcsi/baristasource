#include "ZoomOutHandler.h"
#include "Bui_MainWindow.h"

CZoomOutHandler::CZoomOutHandler(void)
{
	this->statusBarText = "ZOOMOUTMODE ";
}

CZoomOutHandler::~CZoomOutHandler(void)
{
}


bool CZoomOutHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{
	return CBaristaViewEventHandler::handleMouseMoveEvent(e, v);
};


bool CZoomOutHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	return false;
};

bool CZoomOutHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	int x = v->getXPosPressed(); 
	int y = v->getYPosPressed(); 

	v->zoomDown();

	int dx = v->getXPressed();
	int dy = v->getYPressed();

	v->setScrollX(x/2 - dx/2);
	v->setScrollY(y/2 - dy/2);

	if (this->inGeoLinkMode)
		this->doGeoLink(v);

	return true;
};

bool CZoomOutHandler::handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	return true;
};

bool CZoomOutHandler::redraw(CBaristaView *v)
{
	return true;
};

void CZoomOutHandler::initialize(CBaristaView *v)
{
};

void CZoomOutHandler::open()
{
	CBaristaViewEventHandler::open();
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->ZoomOutAction->setChecked(true);
	mainwindow->blockSignals(false);
};

void CZoomOutHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->ZoomOutAction->setChecked(false);
	mainwindow->blockSignals(false);
};
