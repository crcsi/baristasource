#include "Bui_CooccurrenceDialog.h"

#include <qfiledialog.h> 
#include <QMessageBox> 

#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "IniFile.h"
#include "Icons.h"
#include "BaristaProject.h"
#include "VImage.h"
#include "BaristaHtmlHelp.h"
#include "BuildingChangeEvaluator.h"
#include "BaristaProjectHelper.h"


bui_CooccurrenceDialog::bui_CooccurrenceDialog(CVImage *pReference) : 
		pReferenceClassification(pReference), pAutomaticClassification(0),
		computationOK (false)
{

	setupUi(this);
	connect(this->buttonOk,               SIGNAL(released()),               this, SLOT(OK()));
	connect(this->buttonCancel,           SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->buttonHelp,             SIGNAL(released()),               this, SLOT(Help()));
	connect(this->AutomaticClassSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(AutomaticChanged()));
	connect(this->ReferenceClassSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(ReferenceChanged()));
	connect(this->OutputLevelMin,         SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMed,         SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMax,         SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputBrowse,           SIGNAL(released()),               this, SLOT(explorePathSelected()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}

bui_CooccurrenceDialog::~bui_CooccurrenceDialog(void)
{
	this->pReferenceClassification = 0;
	this->pAutomaticClassification = 0;
}

void bui_CooccurrenceDialog::init()
{
	currentDir = inifile.getCurrDir();
	if (!currentDir.IsEmpty())
	{
		if (this->currentDir[this->currentDir.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->currentDir += CSystemUtilities::dirDelimStr;
	}
	this->outputDir = this->currentDir;
	this->OutputDirectory->setText(this->outputDir.GetChar());

	setupImageSelector(ReferenceClassSelector, this->pReferenceClassification);
	setupImageSelector(AutomaticClassSelector, this->pAutomaticClassification);

	this->OutputLevelMin->blockSignals(true);
	this->OutputLevelMed->blockSignals(true);
	this->OutputLevelMax->blockSignals(true);

	if (outputMode == eMINIMUM)
	{
		this->OutputLevelMin->setChecked(true);	
		this->OutputLevelMed->setChecked(false);	
		this->OutputLevelMax->setChecked(false);	
	}
	else if (outputMode == eMEDIUM)
	{
		this->OutputLevelMin->setChecked(false);	
		this->OutputLevelMed->setChecked(true);	
		this->OutputLevelMax->setChecked(false);	
	}
	else if (outputMode == eMAXIMUM)
	{
		this->OutputLevelMin->setChecked(false);	
		this->OutputLevelMed->setChecked(false);	
		this->OutputLevelMax->setChecked(true);	
	}
	else // (outputMode == eDEBUG)
	{
		this->OutputLevelMin->setChecked(false);	
		this->OutputLevelMed->setChecked(false);	
		this->OutputLevelMax->setChecked(true);	
	}
	this->OutputLevelMin->blockSignals(false);
	this->OutputLevelMed->blockSignals(false);
	this->OutputLevelMax->blockSignals(false);

	this->AutomaticClassSelector->setFocus();
	this->checkSetup();
}

void bui_CooccurrenceDialog::explorePathSelected()
{
	QString dirQ = QFileDialog::getExistingDirectory (this, "Choose Output Directory",
		                                              this->outputDir.GetChar());

	CFileName direc = ((const char*)dirQ.toLatin1());
		
	if (!direc.IsEmpty())
	{
		this->outputDir = direc;
		if (this->outputDir[this->outputDir.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->outputDir += CSystemUtilities::dirDelimStr;

		this->OutputDirectory->setText(this->outputDir.GetChar());
	}
};

void bui_CooccurrenceDialog::OutputLevelChanged()
{
	this->OutputLevelMin->blockSignals(true);
	this->OutputLevelMed->blockSignals(true);
	this->OutputLevelMax->blockSignals(true);

	if (outputMode == eMINIMUM)
	{
		if (this->OutputLevelMed->isChecked()) outputMode = eMEDIUM;
		else if (this->OutputLevelMax->isChecked()) outputMode = eMAXIMUM;
	}
	else if (outputMode == eMEDIUM)
	{
		if (this->OutputLevelMin->isChecked()) outputMode = eMINIMUM;
		else if (this->OutputLevelMax->isChecked()) outputMode = eMAXIMUM;
	}
	else if (outputMode == eMAXIMUM)
	{
		if (this->OutputLevelMin->isChecked()) outputMode = eMINIMUM;
		else if (this->OutputLevelMed->isChecked()) outputMode = eMEDIUM;
	}
	else // (outputMode == eDEBUG)
	{
		if (this->OutputLevelMin->isChecked()) outputMode = eMINIMUM;
		else if (this->OutputLevelMed->isChecked()) outputMode = eMEDIUM;
	}

	if (outputMode == eMINIMUM)
	{
		this->OutputLevelMin->setChecked(true);	
		this->OutputLevelMed->setChecked(false);	
		this->OutputLevelMax->setChecked(false);	
	}
	else if (outputMode == eMEDIUM)
	{
		this->OutputLevelMin->setChecked(false);	
		this->OutputLevelMed->setChecked(true);	
		this->OutputLevelMax->setChecked(false);	
	}
	else if (outputMode == eMAXIMUM)
	{
		this->OutputLevelMin->setChecked(false);	
		this->OutputLevelMed->setChecked(false);	
		this->OutputLevelMax->setChecked(true);	
	}
	else // (outputMode == eDEBUG)
	{
		this->OutputLevelMin->setChecked(false);	
		this->OutputLevelMed->setChecked(false);	
		this->OutputLevelMax->setChecked(true);	
	}

	this->OutputLevelMin->blockSignals(false);
	this->OutputLevelMed->blockSignals(false);
	this->OutputLevelMax->blockSignals(false);
}
	
void bui_CooccurrenceDialog::setupImageSelector(QComboBox *box, CVImage *selectedImage)
{
	box->blockSignals(true);

	box->clear();

	box->addItem("Not Selected");

	int indexSelected = 0;

	for (int i = 0, index = 1; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		if ((img->getHugeImage()->getBpp() == 8) && (img->getHugeImage()->getBands() < 2))
		{
			box->addItem(img->getFileNameChar());
			if (img == selectedImage) indexSelected = index;
			++index;
		}
	}

	box->setCurrentIndex(indexSelected);

	box->blockSignals(false);
};

void bui_CooccurrenceDialog::ReferenceChanged()
{
	QString strng = this->ReferenceClassSelector->currentText();
	CCharString Name = (const char *) strng.toLatin1();

	this->pReferenceClassification = project.getImageByName(&Name);

	if (!this->pReferenceClassification)
	{
		setupImageSelector(ReferenceClassSelector, this->pReferenceClassification);
	}
	else
	{
		if (this->pAutomaticClassification == this->pReferenceClassification) 
		{
			this->pAutomaticClassification = 0;
			setupImageSelector(AutomaticClassSelector,  this->pAutomaticClassification);
		}
	}
	
	this->checkSetup();
}

void bui_CooccurrenceDialog::AutomaticChanged()
{
	QString strng = this->AutomaticClassSelector->currentText();
	CCharString Name = (const char *) strng.toLatin1();

	this->pAutomaticClassification = project.getImageByName(&Name);

	if (!pAutomaticClassification)
	{
		setupImageSelector(AutomaticClassSelector,  this->pAutomaticClassification);
	}
	else
	{
		if (this->pAutomaticClassification == this->pReferenceClassification) 
		{
			this->pReferenceClassification = 0;
			setupImageSelector(ReferenceClassSelector,  this->pReferenceClassification);
		}
	}
	
	this->checkSetup();
}

void bui_CooccurrenceDialog::setCurrentDir(const CCharString &cdir)
{
	this->currentDir = cdir;
}

void bui_CooccurrenceDialog::checkSetup()
{
	if (this->pReferenceClassification && this->pAutomaticClassification)
	{
		this->buttonOk->setEnabled(true);
		this->AutomaticClassSelector->setFocus();
	}
	else
	{
		this->buttonOk->setEnabled(false);
	}
}

void bui_CooccurrenceDialog::OK()
{
	CHugeImage *pAutomaticHug = this->pAutomaticClassification->getHugeImage();
	CHugeImage *pReferenceHug = this->pReferenceClassification->getHugeImage();
	if ((pAutomaticHug->getWidth()  != pReferenceHug->getWidth()) || 
		(pAutomaticHug->getHeight() != pReferenceHug->getHeight()))
	{
		QMessageBox::warning( this, "Computation of cooccurrence matrix not possible!", 
			                 "Images have different extents!");
		this->computationOK = false;
	}	
	else
	{
		baristaProjectHelper.setIsBusy(true);

		CFileName protFileName = "Cooccurrence.csv";
		protFileName.SetPath(this->outputDir);
		protHandler.open(protHandler.prt,protFileName, false);

		ostrstream oss; 
		oss.setf(ios::fixed, ios::floatfield);
		oss << "Cooccurrence matrix:\n====================\n\nAutomatic classification:,Filename,Width,Height\n," 
			<< this->pAutomaticClassification->getFileNameChar() << ", " << pAutomaticHug->getWidth() << ","
			<< pAutomaticHug->getHeight() << "\n,"
			<< this->pReferenceClassification->getFileNameChar() << ", " << pReferenceHug->getWidth() << ","
			<< pReferenceHug->getHeight() << "\n\n";

		protHandler.print(oss,protHandler.prt);

		CFileName fn("Cooccurrence.tif");
		fn.SetPath(this->outputDir);
		CVImage *img = project.addImage(fn, false);
		CCharString str = img->getName() + ".hug";
		img->getHugeImage()->setFileName(str.GetChar());

		this->computationOK = CBuildingChangeEvaluator::computeConfusionMatrix(img->getHugeImage(), pAutomaticHug, pReferenceHug);

		protHandler.close(protHandler.prt);

		if (this->computationOK)  
		{
			CTFW tfw;
			bool hasTFW = false;

			if (this->pAutomaticClassification->hasTFW())
			{
				tfw = *(this->pAutomaticClassification->getTFW());
				hasTFW = true;
			}
			else if (this->pReferenceClassification->hasTFW())
			{
				tfw = *(this->pReferenceClassification->getTFW());
				hasTFW = true;
			}
	
			if (hasTFW)
			{
				img->setTFW(tfw);
			}

			baristaProjectHelper.refreshTree();	
		}
		else
		{
			project.deleteImage(img);
		}

		baristaProjectHelper.setIsBusy(false);
	}

	this->close();
}

void bui_CooccurrenceDialog::Cancel()
{
	this->close();
}

void bui_CooccurrenceDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/CooccurrenceMatrix.html");
};
