#include "Bui_ImgShowDlg.h"
#include "Icons.h"
#include <QColorDialog>
#include "Bui_MainWindow.h"
#include <qmdisubwindow.h>

Bui_ImgShowDlg::Bui_ImgShowDlg(QWidget *parent)
	: QDialog(parent)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));	

	this->defineDefaultData();
	this->initImgShowDlg(false);
	
	// pushbutton connections
	this->connect(this->pB_Cancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pB_Close,SIGNAL(released()),this,SLOT(pBCloseReleased()));
	this->connect(this->pB_LblColor,SIGNAL(released()),this,SLOT(pBLabelColorReleased()));
	this->connect(this->pB_PntColor,SIGNAL(released()),this,SLOT(pBPointColorReleased()));
	this->connect(this->pB_ResColor,SIGNAL(released()),this,SLOT(pBResColorReleased()));
	this->connect(this->pB_OK,SIGNAL(released()),this,SLOT(pBOKReleased()));
	this->connect(this->pB_Apply,SIGNAL(released()),this,SLOT(pBApplyReleased()));

	// radiobutton connections
	this->connect(this->rB_On,SIGNAL(released()),this,SLOT(rBLablelOnReleased()));
	this->connect(this->rB_Off,SIGNAL(released()),this,SLOT(rBLablelOffReleased()));
	this->connect(this->rB_Cross,SIGNAL(released()),this,SLOT(rBCrossReleased()));
	this->connect(this->rB_Dot,SIGNAL(released()),this,SLOT(rBDotReleased()));
	this->connect(this->rB_NoPnt,SIGNAL(released()),this,SLOT(rBNoPntReleased()));
	this->connect(this->rB_Imgxy,SIGNAL(released()),this,SLOT(rBImagexyReleased()));
	this->connect(this->rB_ImgDiff,SIGNAL(released()),this,SLOT(rBImageDiffReleased()));
	this->connect(this->rB_ObjXY,SIGNAL(released()),this,SLOT(rBObjXYReleased()));
	this->connect(this->rB_ObjHt,SIGNAL(released()),this,SLOT(rBObjHeightReleased()));
	this->connect(this->rB_NoPlt,SIGNAL(released()),this,SLOT(rBNoPlotReleased()));	
	this->connect(this->rB_ProjOn,SIGNAL(released()),this,SLOT(rbProjectedPointOnReleased()));	
	this->connect(this->rB_ProjOff,SIGNAL(released()),this,SLOT(rbProjectedPointOffReleased()));	
	this->connect(this->rB_Custom,SIGNAL(released()),this,SLOT(rBCustomReleased()));	
	this->connect(this->rB_Default,SIGNAL(released()),this,SLOT(rBDefaultReleased()));		
	this->connect(this->cB_Scale,SIGNAL(released()),this,SLOT(cBScaleReleased()));		
}

Bui_ImgShowDlg::~Bui_ImgShowDlg()
{

}

void Bui_ImgShowDlg::initImgShowDlg(bool defaultSetup)
{
	QPalette palette;

	//if (!defaultSetup)
	//{

	//setting for Setup
	this->pB_Cancel->setEnabled(false);
	if (project.isShowProjectedPoint())
	{
		this->rB_ProjOn->setChecked(true);
		this->rB_ProjOff->setChecked(false);
	}
	else
	{
		this->rB_ProjOn->setChecked(false);
		this->rB_ProjOff->setChecked(true);	
	}


		// settings for Label
		if (project.getLblShowType() == lOn)
		{
			this->rB_On->setChecked(true);
			this->rB_Off->setChecked(false);
			this->pB_LblColor->setDisabled(false);
		}
		else if (project.getLblShowType() == lOff)
		{
			this->rB_On->setChecked(false);
			this->rB_Off->setChecked(true);
			this->pB_LblColor->setDisabled(true);
		}

		int *lC = project.getLblColor();
		int *pC = project.getPntColor();
		int *rC = project.getResColor();
		QColor lCQ(lC[0], lC[1], lC[2]);
		QColor pCQ(pC[0], pC[1], pC[2]);
		QColor rCQ(rC[0], rC[1], rC[2]);

		palette = this->lE_LblColor->palette();
		palette.setColor(QPalette::Base, lCQ);
		this->lE_LblColor->setPalette(palette);

		//settings for Point
		if (project.getPntShowType() == pCross)
		{
			this->rB_Cross->setChecked(true);
			this->rB_Dot->setChecked(false);
			this->rB_NoPnt->setChecked(false);
			this->pB_PntColor->setDisabled(false);
			this->sB_DotWidth->setEnabled(false);
		}
		else if (project.getPntShowType() == pDot)
		{
			this->rB_Cross->setChecked(false);
			this->rB_Dot->setChecked(true);
			this->rB_NoPnt->setChecked(false);
			this->pB_PntColor->setDisabled(false);
			this->sB_DotWidth->setEnabled(true);
		}
		else if (project.getPntShowType() == pNoPoint)
		{
			this->rB_Cross->setChecked(false);
			this->rB_Dot->setChecked(false);
			this->rB_NoPnt->setChecked(false);
			this->pB_PntColor->setDisabled(true);
			this->sB_DotWidth->setEnabled(false);
		}

		palette = this->lE_PntColor->palette();
		palette.setColor(QPalette::Base,pCQ);
		this->lE_PntColor->setPalette(palette);

		this->sB_DotWidth->setRange(1,10);
		this->sB_DotWidth->setSingleStep(1);
		this->sB_DotWidth->setValue(project.getDotWidth());

		//settings for Residual	
		if (project.getResShowType() == rImgxy)
		{
			this->rB_Imgxy->setChecked(true);
			this->rB_ImgDiff->setChecked(false);
			this->rB_ObjXY->setChecked(false);
			this->rB_ObjHt->setChecked(false);
			this->rB_NoPlt->setChecked(false);
			this->pB_ResColor->setDisabled(false);
			this->sB_LnWidth->setEnabled(true);
		}
		else if (project.getResShowType() == rImgDiff)
		{
			this->rB_Imgxy->setChecked(false);
			this->rB_ImgDiff->setChecked(true);
			this->rB_ObjXY->setChecked(false);
			this->rB_ObjHt->setChecked(false);
			this->rB_NoPlt->setChecked(false);
			this->pB_ResColor->setDisabled(false);
			this->sB_LnWidth->setEnabled(true);
		}
		else if (project.getResShowType() == rObXY)
		{
			this->rB_Imgxy->setChecked(false);
			this->rB_ImgDiff->setChecked(false);
			this->rB_ObjXY->setChecked(true);
			this->rB_ObjHt->setChecked(false);
			this->rB_NoPlt->setChecked(false);
			this->pB_ResColor->setDisabled(false);
			this->sB_LnWidth->setEnabled(true);
		}
		else if (project.getResShowType() == rObHt)
		{
			this->rB_Imgxy->setChecked(false);
			this->rB_ImgDiff->setChecked(false);
			this->rB_ObjXY->setChecked(false);
			this->rB_ObjHt->setChecked(true);
			this->rB_NoPlt->setChecked(false);
			this->pB_ResColor->setDisabled(false);
			this->sB_LnWidth->setEnabled(true);
		}
		else if (project.getResShowType() == rNoResidual)
		{
			this->rB_Imgxy->setChecked(false);
			this->rB_ImgDiff->setChecked(false);
			this->rB_ObjXY->setChecked(false);
			this->rB_ObjHt->setChecked(false);
			this->rB_NoPlt->setChecked(true);
			this->pB_ResColor->setDisabled(true);
			this->sB_LnWidth->setEnabled(false);
		}

		palette = this->lE_ResColor->palette();
		palette.setColor(QPalette::Base,rCQ);
		this->lE_ResColor->setPalette(palette);

		this->sB_LnWidth->setRange(1,10);
		this->sB_LnWidth->setSingleStep(1);
		this->sB_LnWidth->setValue(project.getLnWidth());

		this->sB_Scale->setRange(0.5,10.0);
		this->sB_Scale->setSingleStep(0.5);
		this->sB_Scale->setValue(project.getScaleValue());

		this->sB_GSD->setRange(0.0,5.0);
		this->sB_GSD->setSingleStep(0.01);
		if (project.getGSDValue() == 0)
			this->sB_GSD->setValue(project.getProjectGSDValue());
		else
			this->sB_GSD->setValue(project.getGSDValue());

		if (project.isShowScale())
		{
			this->cB_Scale->setChecked(true);
			this->sB_Scale->setEnabled(true);
			this->sB_GSD->setEnabled(true);
		}
		else
		{
			this->cB_Scale->setChecked(false);
			this->sB_Scale->setEnabled(false);
			this->sB_GSD->setEnabled(false);
		}
	//}
	//else
	//{
		//getDefaultData();
		//this->initImgShowDlg(false); // set the values on the dialog
		//this->initImgShowDlg(true); // disable dialog items

	//}
		if (defaultSetup)
		{
			this->rB_Custom->setChecked(false);
			this->rB_Default->setChecked(true);		
			this->rB_ProjOn->setEnabled(false);
			this->rB_ProjOff->setEnabled(false);
			this->gB_Label->setEnabled(false);
			this->gB_Point->setEnabled(false);
			this->gB_Residual->setEnabled(false);
			this->cB_Scale->setChecked(true);
			this->sB_Scale->setEnabled(true);
			this->sB_GSD->setEnabled(true);
		}
		else
		{
			this->rB_Custom->setChecked(true);
			this->rB_Default->setChecked(false);
			this->rB_ProjOn->setEnabled(true);
			this->rB_ProjOff->setEnabled(true);
			this->gB_Label->setEnabled(true);
			this->gB_Point->setEnabled(true);
			this->gB_Residual->setEnabled(true);
		}
}

void Bui_ImgShowDlg::rBCustomReleased()
{
	this->initImgShowDlg(false);
}

void Bui_ImgShowDlg::cBScaleReleased()
{
	if (this->cB_Scale->isChecked())
	{
		this->sB_Scale->setEnabled(true);
		this->sB_GSD->setEnabled(true);
	}
	else
	{
		this->sB_Scale->setEnabled(false);
		this->sB_GSD->setEnabled(false);
	}
}

void Bui_ImgShowDlg::rBDefaultReleased()
{
	getDefaultData();
	this->initImgShowDlg(true);
}
void Bui_ImgShowDlg::rbProjectedPointOnReleased()
{
	this->showProjPoint = true;
	//project.setShowProjectedPoint(this->showProjPoint);
	this->rB_ProjOn->setChecked(true);
	this->rB_ProjOff->setChecked(false);	
}

void Bui_ImgShowDlg::rbProjectedPointOffReleased()
{
	this->showProjPoint = false;
	//project.setShowProjectedPoint(this->showProjPoint);
	this->rB_ProjOn->setChecked(false);
	this->rB_ProjOff->setChecked(true);	
}

void Bui_ImgShowDlg::defineDefaultData()
{
	flag_label_def = lOn; //0 for off, 1 for on
	flag_point_def = pCross; // 0 (cross), 1 (dot), 2 (none)
	flag_res_def = rImgxy; // 0-4
	color_label_def = Qt::green;
	color_point_def = Qt::green;
	color_res_def = Qt::magenta;
	dot_width_def = 2;
	line_width_def = 1;
	showProjPoint_def = true;
	showScale_def = true;
	scaleValue_def = 1.0;
	gsdValue_def = project.getProjectGSDValue();
}
void Bui_ImgShowDlg::getDefaultData()
{
	int rC[3], lC[3], pC[3];
	pC[0] = color_point_def.red();
	pC[1] = color_point_def.green();
	pC[2] = color_point_def.blue();
	lC[0] = color_label_def.red();
	lC[1] = color_label_def.green();
	lC[2] = color_label_def.blue();
	rC[0] = color_res_def.red();
	rC[1] = color_res_def.green();
	rC[2] = color_res_def.blue();

	project.setLblShowType(flag_label_def);
	project.setPntShowType(flag_point_def);
	project.setResShowType(flag_res_def);
	project.setLblColor(lC);
	project.setPntColor(pC);
	project.setResColor(rC);
	project.setDotWidth(dot_width_def);
	project.setLnWidth(line_width_def);
	project.setShowProjectedPoint(showProjPoint_def);
	project.setShowScale(showScale_def);
	project.setScaleValue(scaleValue_def);
	project.setGSDValue(gsdValue_def);
}

void Bui_ImgShowDlg::pBApplyReleased()
{
	saveOldData();
	getNewData();
	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}
	this->pB_Cancel->setEnabled(true);
}

void Bui_ImgShowDlg::pBOKReleased()
{
	//saveOldData();
	getNewData();
	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}
	this->close();
}
void Bui_ImgShowDlg::saveOldData()
{
	flag_label = project.getLblShowType();
	flag_point = project.getPntShowType();
	flag_res = project.getResShowType();

	int *lC = project.getLblColor();
	int *pC = project.getPntColor();
	int *rC = project.getResColor();
	QColor lCQ(lC[0], lC[1], lC[2]);
	QColor pCQ(pC[0], pC[1], pC[2]);
	QColor rCQ(rC[0], rC[1], rC[2]);

	color_label = lCQ;
	color_point = pCQ;
	color_res = rCQ;
	dot_width = project.getDotWidth();
	line_width = project.getLnWidth();
	showProjPoint = project.isShowProjectedPoint();
	showScale = project.isShowScale();
	scaleValue = project.getScaleValue();
	gsdValue = project.getGSDValue();
}
void Bui_ImgShowDlg::retrieveOldData()
{
	project.setLblShowType(flag_label);
	project.setPntShowType(flag_point);
	project.setResShowType(flag_res);
	int rC[3], lC[3], pC[3];
	pC[0] = color_point_def.red();
	pC[1] = color_point_def.green();
	pC[2] = color_point_def.blue();
	lC[0] = color_label_def.red();
	lC[1] = color_label_def.green();
	lC[2] = color_label_def.blue();
	rC[0] = color_res_def.red();
	rC[1] = color_res_def.green();
	rC[2] = color_res_def.blue();

	project.setLblColor(lC);
	project.setPntColor(pC);
	project.setResColor(rC);
	project.setDotWidth(dot_width);
	project.setLnWidth(line_width);
	project.setShowProjectedPoint(showProjPoint);
	project.setShowScale(showScale);
	project.setScaleValue(scaleValue);
	project.setGSDValue(gsdValue);
}

void Bui_ImgShowDlg::getNewData()
{
	QColor color;
	QPalette palette;

	if (this->rB_ProjOn->isChecked())
		project.setShowProjectedPoint(true);
	else
		project.setShowProjectedPoint(false);

	//Label
	if (this->rB_On->isChecked())
		project.setLblShowType(lOn);
	else
		project.setLblShowType(lOff);
	palette = this->lE_LblColor->palette();
	color = palette.color(QPalette::Base);
	int C[3];
	C[0] = color.red();
	C[1] = color.green();
	C[2] = color.blue();

	project.setLblColor(C);

	//Point
	if (this->rB_Cross->isChecked())
	{
		project.setPntShowType(pCross);
		project.setDotWidth(1); // default width of cross
	}	
	else if (this->rB_Dot->isChecked())
	{
		project.setPntShowType(pDot);
		project.setDotWidth(this->sB_DotWidth->value());
	}
	else
		project.setPntShowType(pNoPoint);
	palette = this->lE_PntColor->palette();
	color = palette.color(QPalette::Base);
	C[0] = color.red();
	C[1] = color.green();
	C[2] = color.blue();

	project.setPntColor(C);
	
	//Residual
	if (this->rB_Imgxy->isChecked())
		project.setResShowType(rImgxy);
	else if (this->rB_ImgDiff->isChecked())
		project.setResShowType(rImgDiff);
	else if (this->rB_ObjXY->isChecked())
		project.setResShowType(rObXY);
	else if (this->rB_ObjHt->isChecked())
		project.setResShowType(rObHt);
	else
		project.setResShowType(rNoResidual);
	palette = this->lE_ResColor->palette();
	color = palette.color(QPalette::Base);
	C[0] = color.red();
	C[1] = color.green();
	C[2] = color.blue();

	project.setResColor(C);	
	project.setLnWidth(this->sB_LnWidth->value());

	if (this->cB_Scale->isChecked())
	{
		project.setShowScale(true);
		project.setScaleValue(this->sB_Scale->value());
		project.setGSDValue(this->sB_GSD->value());
	}
	else
	{
		project.setShowScale(false);
		project.setScaleValue(this->sB_Scale->value());
		project.setGSDValue(this->sB_GSD->value());
	}
}

	//Residual
void Bui_ImgShowDlg::rBImagexyReleased()
{
	this->pB_ResColor->setDisabled(false);
	this->sB_LnWidth->setEnabled(true);
	this->cB_Scale->setText("Show scale bar: value [pixel]");
	this->cB_Scale->setEnabled(true);
	this->sB_Scale->setEnabled(true);
	this->sB_GSD->setEnabled(true);
	/*
	this->rB_Imgxy->setChecked(true);
	this->rB_ImgDiff->setChecked(false);
	this->rB_ObjXY->setChecked(false);
	this->rB_ObjHt->setChecked(false);
	this->rB_NoPlt->setChecked(false);
	*/
}

void Bui_ImgShowDlg::rBImageDiffReleased()
{
	this->pB_ResColor->setDisabled(false);
	this->sB_LnWidth->setEnabled(true);
	this->cB_Scale->setText("Show scale bar: value [pixel]");
	this->cB_Scale->setEnabled(true);
	this->sB_Scale->setEnabled(true);
	this->sB_GSD->setEnabled(true);
	/*
	this->rB_Imgxy->setChecked(false);
	this->rB_ImgDiff->setChecked(true);
	this->rB_ObjXY->setChecked(false);
	this->rB_ObjHt->setChecked(false);
	this->rB_NoPlt->setChecked(false);
	*/
}

void Bui_ImgShowDlg::rBObjXYReleased()
{
	this->pB_ResColor->setDisabled(false);
	this->sB_LnWidth->setEnabled(true);
	this->cB_Scale->setText("Show scale bar: value [metre]");
	this->cB_Scale->setEnabled(true);
	this->sB_Scale->setEnabled(true);
	this->sB_GSD->setEnabled(true);
	/*
	this->rB_Imgxy->setChecked(false);
	this->rB_ImgDiff->setChecked(false);
	this->rB_ObjXY->setChecked(true);
	this->rB_ObjHt->setChecked(false);
	this->rB_NoPlt->setChecked(false);
	*/
}

void Bui_ImgShowDlg::rBObjHeightReleased()
{
	this->pB_ResColor->setDisabled(false);
	this->sB_LnWidth->setEnabled(true);
	this->cB_Scale->setText("Show scale bar: value [metre]");
	this->cB_Scale->setEnabled(true);
	this->sB_Scale->setEnabled(true);
	this->sB_GSD->setEnabled(true);
	/*
	this->rB_Imgxy->setChecked(false);
	this->rB_ImgDiff->setChecked(false);
	this->rB_ObjXY->setChecked(false);
	this->rB_ObjHt->setChecked(true);
	this->rB_NoPlt->setChecked(false);
	*/
}

void Bui_ImgShowDlg::rBNoPlotReleased()
{
	this->pB_ResColor->setDisabled(true);
	this->sB_LnWidth->setEnabled(false);
	this->cB_Scale->setEnabled(false);
	this->sB_Scale->setEnabled(false);
	this->sB_GSD->setEnabled(false);
	/*
	this->rB_Imgxy->setChecked(false);
	this->rB_ImgDiff->setChecked(false);
	this->rB_ObjXY->setChecked(false);
	this->rB_ObjHt->setChecked(false);
	this->rB_NoPlt->setChecked(true);
	*/
}

void Bui_ImgShowDlg::pBResColorReleased()
{
	int *c = project.getResColor();
	QColor col(c[0], c[1], c[2]);
	QColor color = QColorDialog::getColor(col,this);
	QPalette palette = this->lE_ResColor->palette();
	palette.setColor(QPalette::Base,color);
	this->lE_ResColor->setPalette(palette);
	//project.setResColor(color);	
}


//Point
void Bui_ImgShowDlg::rBCrossReleased()
{
	this->pB_PntColor->setDisabled(false);	
	this->sB_DotWidth->setEnabled(false);
	/*
	this->rB_Cross->setChecked(true);
	this->rB_Dot->setChecked(false);
	this->rB_NoPnt->setChecked(false);
	*/
}

void Bui_ImgShowDlg::rBDotReleased()
{
	this->pB_PntColor->setDisabled(false);
	this->sB_DotWidth->setEnabled(true);
	/*
	this->rB_Cross->setChecked(false);
	this->rB_Dot->setChecked(true);
	this->rB_NoPnt->setChecked(false);
	*/
}

void Bui_ImgShowDlg::rBNoPntReleased()
{
	this->pB_PntColor->setDisabled(true);
	this->sB_DotWidth->setEnabled(false);
	/*
	this->rB_Cross->setChecked(false);
	this->rB_Dot->setChecked(false);
	this->rB_NoPnt->setChecked(true);
	*/
}

void Bui_ImgShowDlg::pBPointColorReleased()
{
	int *c = project.getPntColor();
	QColor col(c[0], c[1], c[2]);
	QColor color = QColorDialog::getColor(col,this);
	QPalette palette = this->lE_PntColor->palette();
	palette.setColor(QPalette::Base,color);
	this->lE_PntColor->setPalette(palette);
	//project.setPntColor(color);	
}

// Label
void Bui_ImgShowDlg::rBLablelOffReleased()
{
	this->pB_LblColor->setDisabled(true);
}

void Bui_ImgShowDlg::rBLablelOnReleased()
{
	this->pB_LblColor->setDisabled(false);
}

void Bui_ImgShowDlg::pBLabelColorReleased()
{
	int *c = project.getLblColor();
	QColor col(c[0], c[1], c[2]);
	QColor color = QColorDialog::getColor(col,this);
	QPalette palette = this->lE_LblColor->palette();
	palette.setColor(QPalette::Base,color);
	this->lE_LblColor->setPalette(palette);
	//project.setLblColor(color);	
}

void Bui_ImgShowDlg::pBCancelReleased()
{
	retrieveOldData();
	if (this->rB_Default->isChecked())
		this->initImgShowDlg(true);
	else
		this->initImgShowDlg(false);

	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}
	this->pB_Cancel->setEnabled(false);
}

void Bui_ImgShowDlg::pBCloseReleased()
{
	this->close();
}