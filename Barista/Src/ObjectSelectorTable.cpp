#include "ObjectSelectorTable.h"

#include <QSortFilterProxyModel>
#include <QAbstractButton>
#include <QAction>
#include "BaristaViewEventHandler.h"
#include "VImage.h"
#include "ObsPointDataModel.h"
#include "PolyLineDataModel.h"
#include "SensorModel.h"
#include "Trans3DFactory.h"
#include "GenericPolyLine.h"
#include "BaristaProject.h"

CLabel DRAWING_NAME("SELECTION_DATASET");
CLabel PROJECTION_NAME("PROJECTION_DATASET");
#define ZOOM_WINDOW_SIZE 300


CObjectSelectorTable::CObjectSelectorTable(CObjectDataModel* dataModel,bool isObjectSelectorOnly,QWidget* parent):
	CObjectTable(dataModel,parent),isObjectSelectorOnly(isObjectSelectorOnly)
{
	if (!dataModel)
		return;

	// the button in the corner of the table
	QAbstractButton* btn = this->findChild<QAbstractButton*>();
	if (btn)
		this->connect(btn,SIGNAL(released()),this,SLOT(selectAllObjects()));
	
	// now we get informed when a view is opened or closed
	currentBaristaViewHandler->addListenerToOpenViewList(this);

	
	int nViews = currentBaristaViewHandler->getNumberOfViews();
	for (int i=0; i < nViews; i++)
		this->initView(currentBaristaViewHandler->getView(i));
}


CObjectSelectorTable::~CObjectSelectorTable(void)
{

	for (int i=this->views.size() - 1; i >=0 ; i--)
	{
		this->deleteDataSet(i);
	}

	currentBaristaViewHandler->removeListenerFromOpenViewList(this);
	
}


void CObjectSelectorTable::initView(CBaristaView* view)
{

	ObjectData dataset;
	dataset.view = view;
	dataset.trafo = NULL;
	dataset.sensorModel = NULL;

	// the ref sys of this object
	CReferenceSystem* refSys = this->proxyModel->getReferenceSystem();

	// try to get the ref sys of the current view
	dataset.sensorModel = dataset.view->getCurrentSensorModel();
	if (!dataset.sensorModel) // then try TFW
		dataset.sensorModel = dataset.view->getTFW();
	if (dataset.sensorModel) // do we have a sensormodel
	{
		// ok, we found a sensor model, now create the trafo
		if (refSys)
			dataset.trafo = CTrans3DFactory::initTransformation(*refSys,dataset.sensorModel->getRefSys());
	}



	switch (this->proxyModel->getDataModelType())
	{
	case eObsPointModel:	{
								this->initObsPointDataSet(dataset,view);
								break;
							}
	case ePolyLineModel:	{
								this->initPolyLineDataSet(dataset,view);
								break;
							}
	default:				{
								break;
							}
			
	}

	this->views.push_back(dataset);
}

void CObjectSelectorTable::initObsPointDataSet(ObjectData& dataSet,CBaristaView* view)
{

	ViewParameter viewParameter;
	viewParameter.showSelector = false;
	viewParameter.showSigma = false;
	viewParameter.showCoordinates = false;
	viewParameter.allowSigmaChange = false;
	viewParameter.allowLabelChange = false;
	viewParameter.allowDeletePoint = false;
	viewParameter.allowBestFitEllipse = false;
	viewParameter.allowXYZAveragePoints = false;


	if (dataSet.view->getVImage()) // it's an image view
	{
		CObsPointDataModel* dataModel = new CObsPointDataModel(dataSet.view->getVImage()->getXYPoints(),viewParameter,dataSet.view->getVImage());
		this->connect(dataModel,SIGNAL(dataModelChanged(DataModelChanges,vector<QString>)),this,SLOT(dataModelChanged(DataModelChanges,vector<QString>)));
		CObjectSortFilterModel * proxy = new CObjectSortFilterModel(dataModel);
		dataSet.arrays.push_back(proxy);

		dataModel = new CObsPointDataModel(dataSet.view->getVImage()->getProjectedXYPoints(),viewParameter,dataSet.view->getVImage());
		this->connect(dataModel,SIGNAL(dataModelChanged(DataModelChanges,vector<QString>)),this,SLOT(dataModelChanged(DataModelChanges,vector<QString>)));
		proxy = new CObjectSortFilterModel(dataModel);
		dataSet.arrays.push_back(proxy);


		dataModel = new CObsPointDataModel(dataSet.view->getVBase()->getMonoPlottedPoints(),viewParameter);
		this->connect(dataModel,SIGNAL(dataModelChanged(DataModelChanges,vector<QString>)),this,SLOT(dataModelChanged(DataModelChanges,vector<QString>)));
		proxy = new CObjectSortFilterModel(dataModel);
		dataSet.arrays.push_back(proxy);

	}
	else if (dataSet.view->getVDEM()) // it's a dem view
	{
		CObsPointDataModel* dataModel = new CObsPointDataModel(dataSet.view->getVBase()->getMonoPlottedPoints(),viewParameter);
		this->connect(dataModel,SIGNAL(dataModelChanged(DataModelChanges,vector<QString>)),this,SLOT(dataModelChanged(DataModelChanges,vector<QString>)));
		CObjectSortFilterModel * proxy = new CObjectSortFilterModel(dataModel);
		dataSet.arrays.push_back(proxy);
	}


}

void CObjectSelectorTable::initPolyLineDataSet(ObjectData& dataSet,CBaristaView* view)
{
	ViewParameter viewParameter;
	viewParameter.showSelector = false;
	viewParameter.showSigma = false;
	viewParameter.showCoordinates = false;
	viewParameter.allowSigmaChange = false;
	viewParameter.allowLabelChange = false;
	viewParameter.allowDeletePoint = false;
	viewParameter.allowBestFitEllipse = false;
	viewParameter.allowXYZAveragePoints = false;

	if (dataSet.view->getVImage()) // it's an image view
	{
		CPolyLineDataModel* dataModel = new CPolyLineDataModel(dataSet.view->getVImage()->getXYEdges(),viewParameter);
		this->connect(dataModel,SIGNAL(dataModelChanged(DataModelChanges,vector<QString>)),this,SLOT(dataModelChanged(DataModelChanges,vector<QString>)));
		CObjectSortFilterModel * proxy = new CObjectSortFilterModel(dataModel);
		dataSet.arrays.push_back(proxy);
	}

}


void CObjectSelectorTable::deleteDataSet(int index)
{
	ObjectData dataset = this->views[index];

	dataset.view->removeDrawingDataSet(DRAWING_NAME);
	dataset.view->removeDrawingDataSet(PROJECTION_NAME);
	for (unsigned int k=0; k < dataset.arrays.size(); k++)
		delete dataset.arrays[k];

	if (dataset.trafo)
		delete dataset.trafo;

	dataset.trafo = NULL;
	dataset.sensorModel = NULL;
	dataset.view = NULL;

	this->views.erase(this->views.begin()+ index);
}


void CObjectSelectorTable::elementAdded(CLabel element)
{
	int nViews = currentBaristaViewHandler->getNumberOfViews();
	for (int i=0; i < nViews; i++)
	{
		CBaristaView * view = currentBaristaViewHandler->getView(i);
		bool found = false;
		for (unsigned int k=0; k < this->views.size(); k++)	
		{
			if (view == this->views[k].view)
			{
				found = true;			
				break;
			}
		}
		
		if (!found)
			this->initView(view);
	}
}

void CObjectSelectorTable::elementDeleted(CLabel element)
{
	
	int nViews = currentBaristaViewHandler->getNumberOfViews();

	for (int k=this->views.size() - 1; k >=0 ; k--)	
	{
		bool found = false;
		for (int i=0; i < nViews; i++)
		{
			CBaristaView * view = currentBaristaViewHandler->getView(i);
			if (view == this->views[k].view)
			{
				found = true;			
				break;
			}
		}

		if (!found)
			this->deleteDataSet(k);
	}



}

void CObjectSelectorTable::mouseReleaseEvent ( QMouseEvent * mouseEvent )
{

	if (mouseEvent->button() == Qt::RightButton && !this->isObjectSelectorOnly)
	{
		int selectedRows = this->selectedIndexes().size() / this->model()->columnCount();
		if (selectedRows == 1)
		{
			QAction* zoomToPointAction = new QAction("Zoom to object",this);
			connect(zoomToPointAction, SIGNAL(triggered()), this, SLOT(zoomToObject()));
			this->actionList.push_back(zoomToPointAction);

			if (this->proxyModel->getDataModelType() == eObsPointModel && 
				this->objectDimension == 3 && 
				currentBaristaViewHandler->allowSetCurrentImageLabel())
			{
				QAction* a = new QAction("Set as current image label",this);
				connect(a, SIGNAL(triggered()), this, SLOT(setCurrentImageLabel()));
				this->actionList.push_back(a);
			}
		}
		
	}
	else if (mouseEvent->button() == Qt::LeftButton)
	{
		this->selectObjects(false,false);
	}

	CObjectTable::mouseReleaseEvent(mouseEvent);
}

void CObjectSelectorTable::selectAllObjects()
{
	this->selectAll();
	this->selectObjects(false,false);
}


void CObjectSelectorTable::selectObjects(bool zoom,bool renameObject)
{
	QModelIndexList listTmp = this->selectedIndexes();
	QModelIndexList list;
	// compute the index of every selected row in original array
	for (int i=0; i<listTmp.count(); i++)
	{
		if (listTmp.at(i).column() == this->proxyModel->getNameColumn()) // we only want a row ones
		{	
			list.append(listTmp.at(i));
		}
	}

	// get label of every row to compute the regular expression
	QString regExp("^(");
	for (int i=0; i < list.size(); i++)
	{
		if (i >0)
			regExp += "|";

		QModelIndex index = this->proxyModel->index(list.at(i).row(),this->proxyModel->getNameColumn());
		regExp += this->proxyModel->data(index,Qt::DisplayRole).toString();
		
	}

	regExp += ")$";

	CObsPoint* point = NULL;
	GenericPolyLine* polyLine = NULL;

	
	// set the regExp for every open view and highlight the remaining objects
	for (unsigned int i=0; i< this->views.size(); i++)
	{
		ObjectData dataSet = this->views[i]; 
		dataSet.view->removeDrawingDataSet(DRAWING_NAME);
		dataSet.view->addDrawingDataSet(DRAWING_NAME,2,true,true,QColor(Qt::yellow));
		dataSet.view->removeDrawingDataSet(PROJECTION_NAME);
		dataSet.view->addDrawingDataSet(PROJECTION_NAME,2,true,true,QColor(Qt::red));

		if (!list.size())
			return;


		int nRemaining = 0;
		for (unsigned int k=0; k< dataSet.arrays.size() && !nRemaining; k++)
		{
			CObjectSortFilterModel * proxy = dataSet.arrays[k];
			// set regExp
			proxy->setFilterRegExp(regExp);

			// draw all objects remaining in the filtered model
			nRemaining = proxy->rowCount();
			for (int j=0; j < nRemaining; j++)
			{	
				switch (proxy->getDataModelType())
				{
				case eObsPointModel: {
										point = proxy->getObsPoint(proxy->index(j,0));
										if (point)
										{
											dataSet.view->addPointDrawing(DRAWING_NAME,point);
											if (zoom)
												this->zoomToPoint(dataSet.view,*point); // shows the view (image) Zoomed at the selected point
										}

										break;
									 }
				case ePolyLineModel: {
										polyLine = proxy->getPolyLine(proxy->index(j,0));
										if (polyLine && polyLine->getClassName().compare("CXYPolyLine") == 0)
										{
											dataSet.view->addPolyLineDrawing(DRAWING_NAME,(CXYPolyLine*)polyLine,QColor(Qt::yellow),(unsigned int)project.getMonoplottedLineWidth());
											if (zoom && polyLine->getPointCount())
												this->zoomToPoint(dataSet.view,polyLine->getPoint(0));
										}

										break;
									 }
				}
			}
		}

		// check if all 3d objects were found in the views, if not try to project the object
		if (this->objectDimension == 3)
		{
			
			for (int i=0; i < list.size(); i++)
			{
				switch (this->proxyModel->getDataModelType())
				{
				case eObsPointModel: {				
										CObsPoint* obsPoint = this->proxyModel->getObsPoint(list.at(i));
										if (obsPoint && dataSet.trafo && dataSet.sensorModel && 
											!dataSet.view->queryPointDrawing(DRAWING_NAME,obsPoint->getLabel()))
										{	
											C3DPoint point3D((*obsPoint)[0],(*obsPoint)[1],(*obsPoint)[2]);
											CObsPoint p(2);
											dataSet.trafo->transform(point3D,point3D);
											dataSet.sensorModel->transformObj2Obs(p,point3D);
											p.setLabel(obsPoint->getLabel());
											dataSet.view->addPointDrawing(PROJECTION_NAME,&p);
											if (zoom)
												this->zoomToPoint(dataSet.view,p);	

										}

										if (renameObject && list.size() == 1 && obsPoint)
										{
											currentBaristaViewHandler->setCurrentImageLabel(obsPoint->getLabel());
										}

										break;
									  }
				case ePolyLineModel:{
										GenericPolyLine * polyLine = this->proxyModel->getPolyLine(list.at(i));
										if (polyLine && dataSet.trafo && dataSet.sensorModel)
										{
											CXYPolyLine new2DPolyLine;
											for (int i=0; i< polyLine->getPointCount(); i++)
											{
												C3DPoint point3D(polyLine->getPoint(i)[0],polyLine->getPoint(i)[1],polyLine->getPoint(i)[2]);
												CObsPoint p(2);
												dataSet.trafo->transform(point3D,point3D);
												dataSet.sensorModel->transformObj2Obs(p,point3D);
												p.setLabel(polyLine->getPoint(i).getLabel());
												new2DPolyLine.addPoint(p);
											}

											if(polyLine->isClosed())
												new2DPolyLine.closeLine();

											dataSet.view->addPolyLineDrawing(PROJECTION_NAME,&new2DPolyLine,QColor(Qt::red),(unsigned int)project.getMonoplottedLineWidth());
											if (zoom && new2DPolyLine.getPointCount())
												this->zoomToPoint(dataSet.view,new2DPolyLine.getPoint(0));
										}
										break;
									}
						
				} // switch
			}
		}
	} // end of views


}

void CObjectSelectorTable::dataModelChanged(DataModelChanges changeType,vector<QString> parameter)
{
	switch (changeType)
	{
	case eObjectName:	{		
								emit nameChanged(parameter[2]);
								break;
								}	
	
	case eObjectDeleted:{
								this->selectObjects(false,false);
								break;
							}
	case eObjectAdded: {		this->selectObjects(false,false);
								break;
							}

		default: {} // do nothing
	}

}

void CObjectSelectorTable::zoomToObject()
{
	QModelIndexList list = this->selectedIndexes();
	int rows = list.size() / this->model()->columnCount();

	if (rows != 1)
		return;

	this->selectObjects(true,true);

}

void CObjectSelectorTable::mouseDoubleClickEvent ( QMouseEvent * event )
{
	if (this->isObjectSelectorOnly && event->button() == Qt::LeftButton)
	{
		QModelIndexList listTmp = this->selectedIndexes();
		
		if (( listTmp.size() / this->model()->columnCount()) == 1)
		{
			this->selectObjects(true,true);
			return;
		}
	}

	CObjectTable::mouseDoubleClickEvent(event);
	
}

void CObjectSelectorTable::zoomToPoint(CBaristaView* view, CObsPoint& point)
{
	C2DPoint zoomPoint(point[0],point[1]);
	double zoomFactor = view->getZoomFactor();
	double size = double(view->width())/zoomFactor;
	while (size > ZOOM_WINDOW_SIZE)
	{
		zoomFactor *= 2;												
		size = double(view->width())/zoomFactor;
	}

	view->zoomToPoint(zoomPoint,zoomFactor,true);	
}

void CObjectSelectorTable::setCurrentImageLabel()
{

}