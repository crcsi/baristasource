#include "FileTable.h"
#include <QVBoxLayout>
#include <QHeaderView>
CFileTable::CFileTable(QWidget* parent): CBaristaTable(parent)
{
	this->initTable();
}


CFileTable::CFileTable(QWidget* parent, int top, int left, int width, int height, int ncols, int colwidth): CBaristaTable(parent)
{
	QSizePolicy polciy = this->sizePolicy();
	Layout = new QVBoxLayout();
	this->setLayout(Layout);
	this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    this->setObjectName(QString::fromUtf8("lw_Files"));
    this->setGeometry(QRect(top, left, width, height));
	this->setColumnCount(1);
	this->setColumnWidth(0,colwidth);
	this->setHorizontalHeaderItem(0,this->createItem(QString("DEMs"),0,Qt::AlignCenter,QColor(0,0,0)));

	polciy = this->sizePolicy();
}


CFileTable::~CFileTable(void)
{
}

void CFileTable::initTable()
{
    this->setObjectName(QString::fromUtf8("lw_Files"));
    this->setGeometry(QRect(20, 100, 451, 201));
	this->setColumnCount(1);
	this->setColumnWidth(0,415);
	this->horizontalHeader()->setStretchLastSection(true);
	this->setHorizontalHeaderItem(0,this->createItem(QString("Files"),0,Qt::AlignCenter,QColor(0,0,0)));

}