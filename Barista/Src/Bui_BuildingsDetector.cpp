#include "bui_BuildingsDetector.h"
#include "Icons.h"
#include "BaristaProject.h"
#include "IniFile.h"
#include <qfiledialog.h>
#include "SystemUtilities.h"
#include "BaristaHtmlHelp.h"
#include "BuildingsDetector.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include "BaristaProjectHelper.h"
#include "RAG.h"
#include <QMessageBox>

const char *bui_BuildingsDetector::noImageTxt        = "None";
const char *bui_BuildingsDetector::noSigmaNDVITxt    = "None";

bui_BuildingsDetector::bui_BuildingsDetector(CVALS * als): ALSfile(als), colorImage(0), pSigmaNDVI(0), pDEM(0), Building(0),imgTFW(0), demTFW(0),
    														resolution(0.0,0.0), extentsInit(false),success(false), boundaryPoly(0), maskMode(true), 
															orientationImage(0), edgeImage(0), entropyImage(0), detectionMode(true), useAdjustment(true)
{
	setupUi(this);
	this->move(150,120);
	
	//connect(this->okButton,                   SIGNAL(released()),                      this, SLOT(ComputeBuilding()));
	//connect(this->cancelButton,               SIGNAL(released()),                      this, SLOT(Cancel()));
	
	connect(this->MultispectralImageSelector, SIGNAL(currentIndexChanged(int)),       this, SLOT(ImageSelected()));
	connect(this->ALSselector,                SIGNAL(currentIndexChanged(int)),       this, SLOT(ALSSelected()));
	connect(this->SigmaNDVISelector,          SIGNAL(currentIndexChanged(int)),		  this, SLOT(NDVISigmaSelected()));
	connect(this->DEMSelector,				  SIGNAL(currentIndexChanged(int)),       this, SLOT(DEMSelected()));
	connect(this->OrientationSelector,		  SIGNAL(currentIndexChanged(int)),       this, SLOT(OrientationSelected()));
	connect(this->EdgeImageSelector,		  SIGNAL(currentIndexChanged(int)),       this, SLOT(EdgeImageSelected()));
	connect(this->EntropySelector,			  SIGNAL(currentIndexChanged(int)),       this, SLOT(EntropySelected()));

	connect(this->maxExtentButton,            SIGNAL(released()),                      this, SLOT(maxExtentsSelected()));

	connect(this->im_LLX,                     SIGNAL(editingFinished()),               this, SLOT(LLChanged()));
	connect(this->im_LLY,                     SIGNAL(editingFinished()),               this, SLOT(LLChanged()));
	connect(this->im_UPX,                     SIGNAL(editingFinished()),               this, SLOT(RUChanged()));
	connect(this->im_UPY,                     SIGNAL(editingFinished()),               this, SLOT(RUChanged()));

	connect(this->browseButton,               SIGNAL(released()),                      this, SLOT(explorePathSelected()));

	connect(this->okButton,                   SIGNAL(released()),                      this, SLOT(Ok()));
	connect(this->cancelButton,               SIGNAL(released()),                      this, SLOT(Cancel()));
	connect(this->helpButton,                 SIGNAL(released()),                      this, SLOT(Help()));

	connect(this->rBDEM,					  SIGNAL(clicked(bool)),				   this, SLOT(DEMmodeSelected()));
	connect(this->rBHistogram,				  SIGNAL(clicked(bool)),			       this, SLOT(HistogramModeSelected()));

	connect(this->cBentropy,				  SIGNAL (clicked(bool)),             this, SLOT(useEntropyClicked())); 
	connect(this->cBhistogram,				  SIGNAL (clicked(bool)),             this, SLOT(useHistogramClicked())); 

	connect(this->rBnewDetection,			  SIGNAL(clicked(bool)),				   this, SLOT(newDetectionModeSelected()));
	connect(this->rBoldRefine,				  SIGNAL(clicked(bool)),			       this, SLOT(oldRefineModeSelected()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
    
	/*if (ALSfile)
	{
		this->llPoint.x = floor (this->ALSfile->getCALSData()->getPMin().x);
		this->llPoint.y = floor (this->ALSfile->getCALSData()->getPMin().y);			
		this->urPoint.x = floor (this->ALSfile->getCALSData()->getPMax().x + 0.5);		
		this->urPoint.y = floor (this->ALSfile->getCALSData()->getPMax().y + 0.5);		
    	this->extentsInit = true;
		
	}*/

	this->llPoint.x = FLT_MIN;
	this->llPoint.y = FLT_MIN;
	this->urPoint.x = FLT_MAX;
	this->urPoint.y = FLT_MAX;

	this->MAXllPoint.x = FLT_MIN;
	this->MAXllPoint.y = FLT_MIN;
	this->MAXurPoint.x = FLT_MAX;
	this->MAXurPoint.y = FLT_MAX;

	this->setupExtents(1);
	this->init();
}
/////////////////////////////////////
bui_BuildingsDetector::~bui_BuildingsDetector(void)
{
  this->colorImage		= 0;
  this->ALSfile			= 0;
  this->Building		= 0;
  this->pSigmaNDVI      = 0;
  this->pDEM			= 0;
  this->orientationImage = 0;
  this->edgeImage        = 0;
  this->entropyImage     = 0;	
}

void bui_BuildingsDetector::useEntropyClicked()
{	
	if (this->cBentropy->isChecked())
	{
		this->cBhistogram->setChecked(true);
		this->EntropySelector->setEnabled(true);
		this->OrientationSelector->setEnabled(true);
		this->EdgeImageSelector->setEnabled(true);

		this->useEntropy = true;
		this->useHistogram = true;
	}	
	else
	{
		this->cBhistogram->setChecked(false);
		this->EntropySelector->setEnabled(false);
		this->OrientationSelector->setEnabled(false);
		this->EdgeImageSelector->setEnabled(false);

		this->useEntropy = false;
		this->useHistogram = false;
	}
	
	
	
	checkSetup();
}

void bui_BuildingsDetector::useHistogramClicked()
{	
	if (this->cBentropy->isChecked())
	{
		this->cBhistogram->setChecked(true);		
		
		//this->useEntropy = true;
		//this->useHistogram = true;
	}
	else if (this->cBhistogram->isChecked())
	{	
		this->OrientationSelector->setEnabled(true);
		this->EdgeImageSelector->setEnabled(true);

		this->useEntropy = false;
		this->useHistogram = true;
	}
	else
	{
		this->OrientationSelector->setEnabled(false);
		this->EdgeImageSelector->setEnabled(false);

		this->useEntropy = false;
		this->useHistogram = false;
	}
	
	this->useHistogram = this->cBhistogram->isChecked();

	checkSetup();
}

void bui_BuildingsDetector::DEMmodeSelected()
{	
	this->rBDEM->setChecked(true);
	this->rBHistogram->setChecked(false);
	this->maskMode = true; //use DEM
	this->DEMSelector->setEnabled(true);
	checkSetup();
}

void bui_BuildingsDetector::HistogramModeSelected()
{	
	this->rBDEM->setChecked(false);
	this->rBHistogram->setChecked(true);
	this->maskMode = false; //use histogram
	this->DEMSelector->setEnabled(false);
	checkSetup();
}

void bui_BuildingsDetector::newDetectionModeSelected()
{	
	this->rBDEM->setEnabled(true);
	//this->rBnewDetection->setChecked(true);
	//this->rBoldRefine->setChecked(false);
	this->detectionMode = true; //detect new buildings
	
	this->MultispectralImageSelector->setEnabled(true);
	this->ALSselector->setEnabled(true);
	this->SigmaNDVISelector->setEnabled(true);
	if (this->maskMode)
		this->DEMSelector->setEnabled(true);
	else
		this->DEMSelector->setEnabled(false);
	if (this->useEntropy)
	{
		this->EntropySelector->setEnabled(true);
		this->OrientationSelector->setEnabled(true);
		this->EdgeImageSelector->setEnabled(true);
	}
	else if (this->useHistogram)
	{
		this->EntropySelector->setEnabled(false);
		this->OrientationSelector->setEnabled(true);
		this->EdgeImageSelector->setEnabled(true);
	}
	else
	{
		this->EntropySelector->setEnabled(false);
		this->OrientationSelector->setEnabled(false);
		this->EdgeImageSelector->setEnabled(false);
	}
	this->cBuseAdjusment->setEnabled(true);
	this->cBNDVIExtension->setEnabled(true);
	this->cBentropy->setEnabled(true);
	this->cBhistogram->setEnabled(true);

	this->im_LLX->setEnabled(true);
	this->im_LLY->setEnabled(true);
	this->im_UPX->setEnabled(true);
	this->im_UPY->setEnabled(true);
	this->maxExtentButton->setEnabled(true);

	checkSetup();
}

void bui_BuildingsDetector::oldRefineModeSelected()
{	
	this->rBDEM->setEnabled(false);
	//this->rBHistogram->setEnabled(false);
	this->detectionMode = false; //refine old buildings

	this->MultispectralImageSelector->setEnabled(true);
	this->ALSselector->setEnabled(false);
	this->SigmaNDVISelector->setEnabled(false);	
	this->DEMSelector->setEnabled(false);
	this->EntropySelector->setEnabled(false);
	this->OrientationSelector->setEnabled(true);
	this->EdgeImageSelector->setEnabled(true);

	this->cBuseAdjusment->setEnabled(false);
	this->cBNDVIExtension->setEnabled(false);
	this->cBentropy->setEnabled(false);
	this->cBhistogram->setEnabled(false);

	this->im_LLX->setEnabled(false);
	this->im_LLY->setEnabled(false);
	this->im_UPX->setEnabled(false);
	this->im_UPY->setEnabled(false);
	this->maxExtentButton->setEnabled(false);

	checkSetup();
}

// methods difinision (initialisation)
 ///////////////////////////////////////////
void bui_BuildingsDetector::init()
{
	if (this->extentsInit) 
	{
		setCurrentExtentsAndResolution();
		this->im_LLX->setEnabled(true);
		this->im_LLY->setEnabled(true);
		this->im_UPX->setEnabled(true);
		this->im_UPY->setEnabled(true);
		this->maxExtentButton->setEnabled(true);
	}
	
	currentDir = inifile.getCurrDir();
	this->okButton->setEnabled(false);

	this->outputDir = this->currentDir;
	if (!this->outputDir.IsEmpty())
	{
		if (this->outputDir[this->outputDir.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->outputDir += CSystemUtilities::dirDelimStr;
	}

	this->outputDirectory->setText(this->outputDir.GetChar());

	setupImageSelector(this->MultispectralImageSelector, this->colorImage, this->noImageTxt);
	setupALSSelector(this->ALSselector, this->ALSfile, this->noImageTxt);
	setupImageSelector(this->SigmaNDVISelector, this->pSigmaNDVI, this->noSigmaNDVITxt);
	setupDEMSelector(this->DEMSelector, this->pDEM, this->noImageTxt);
	setupImageSelector(this->OrientationSelector, this->orientationImage, this->noImageTxt);
	setupImageSelector(this->EdgeImageSelector, this->edgeImage, this->noImageTxt);
	setupImageSelector(this->EntropySelector, this->entropyImage, this->noImageTxt);
	//
	if (this->boundaryPoly) delete this->boundaryPoly;
	this->boundaryPoly = 0;
	this->rBDEM->setChecked(true);
	this->rBHistogram->setChecked(false);
	this->rBHistogram->setEnabled(false); //comment this line not to allow histogram mode for mask generation

	this->useNDVIinExtension = false;
	this->useEntropy = false;
	this->useHistogram = false;

	this->EntropySelector->setEnabled(false);
	this->OrientationSelector->setEnabled(false);
	this->EdgeImageSelector->setEnabled(false);

	this->rBnewDetection->setChecked(true);
	this->rBoldRefine->setChecked(false);	

	this->cBuseAdjusment->setChecked(true);
}

/////////////////////////////////////

void bui_BuildingsDetector::setupDEMSelector(QComboBox *box, CVDEM *selectedDEM, const char *descriptor)
{
	box->blockSignals(true);

	box->clear();

	box->addItem(descriptor);

	int indexSelected = 0;

	for (int i = 0; i < project.getNumberOfDEMs(); ++i)
	{
		CVDEM *dem = project.getDEMAt(i);
		box->addItem(dem->getFileName()->GetChar());
		if (dem == selectedDEM) indexSelected = i + 1;
	}

	box->setCurrentIndex(indexSelected);
	
	box->blockSignals(false);
};
/////////////////////////////////////

/////////////////////////////////////

void bui_BuildingsDetector::setupImageSelector(QComboBox *box, CVImage *selectedImage, const char *descriptor)
{
	box->blockSignals(true);

	box->clear();

	box->addItem(descriptor);

	int indexSelected = 0;

	for (int i = 0; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		box->addItem(img->getFileNameChar());
		if (img == selectedImage) indexSelected = i + 1;
	}

	box->setCurrentIndex(indexSelected);
	
	box->blockSignals(false);
};
/////////////////////////////////////

void bui_BuildingsDetector::setupALSSelector(QComboBox *box, CVALS *selectedALS, const char *descriptor)
{
	box->blockSignals(true);

	box->clear();

	box->addItem(descriptor);

	int indexSelected = 0;

	for (int i = 0; i < project.getNumberOfAlsDataSets(); ++i)
	{
		CVALS *laserData = project.getALSAt(i);
		box->addItem(laserData->getFileNameChar());
		if (laserData == selectedALS) indexSelected = i + 1;
	}

	box->setCurrentIndex(indexSelected);
	
	box->blockSignals(false);
};
////////////////////////////////////////////

bool bui_BuildingsDetector::detectBuildings()
{
	bool ret = true;

	if (this->detectionMode)
	{
		//remove previously removed buildings
		this->colorImage->getXYEdges()->RemoveAll();

	//addition on 11 Oct 2011>>
	//if (this->useEntropy)
	//	this->useHistogram = true;
	CHugeImage *inputImage = this->colorImage->getHugeImage();	

	//Building->rectifyFinalBuildings(this->orientationImage->getHugeImage(), this->edgeImage->getHugeImage(), *this->colorImage->getXYEdges());
	//<< addition
	
	CFileName upMaskName("upMask");
    CFileName gMaskName("gMask");
	
	CVImage *imUpMask = project.addImage(upMaskName, false);
	imUpMask->getHugeImage()->setFileName(upMaskName);
	
	CVImage *imGMask = project.addImage(gMaskName, false);
	imGMask->getHugeImage()->setFileName(gMaskName);

	CVImage *cropImg = project.addImage("croppedNDVIsigma", false);
	cropImg->getHugeImage()->setFileName("croppedNDVIsigma");

	// test for YIQ images
	//CVImage *Y = project.addImage("Intensity",false);
	//CVImage *I = project.addImage("Hue",false);
	//CVImage *Q = project.addImage("Saturation",false);
	//CHugeImage *Iy = Y->getHugeImage(), *Hi = I->getHugeImage(), *Sq = Q->getHugeImage();
	CHugeImage *Iy = new CHugeImage, *Hi = new CHugeImage, *Sq = new CHugeImage;
	//Y->setTFW(*this->colorImage->getTFW()); I->setTFW(*this->colorImage->getTFW()); Q->setTFW(*this->colorImage->getTFW());
	CCharString appDir = CSystemUtilities::getEnvVar("APPDATA") + CSystemUtilities::dirDelimStr + "Barista" + CSystemUtilities::dirDelimStr;
	Iy->prepareWriting(appDir + "Intensity", inputImage->getWidth(), inputImage->getHeight(), 32, 1);
	Hi->prepareWriting(appDir + "Hue", inputImage->getWidth(), inputImage->getHeight(), 32, 1);
	Sq->prepareWriting(appDir + "Saturation", inputImage->getWidth(), inputImage->getHeight(), 32, 1);
	CLookupTable lut(8,3); Iy->setLookupTable(&lut); Hi->setLookupTable(&lut); Sq->setLookupTable(&lut);
	inputImage->makeYIQImages(Iy,Hi,Sq);

	//Building->extract(imUpMask->getHugeImage(), imGMask->getHugeImage(), imGMask->getXYPoints(), imGMask->getXYContours(), imGMask->getXYEdges());
	//Building->extractMasks(imUpMask->getHugeImage(), imGMask->getHugeImage(), this->colorImage->getHugeImage()->getWidth(), this->colorImage->getHugeImage()->getHeight());
	//this->maskTFW = imGMask->getTFW();
	CTFW tfw(*imGMask->getTFW());
	ret = Building->extractMasks(imUpMask->getHugeImage(), imGMask->getHugeImage(), &tfw);

	if (ret)
	{
	Building->extractContours(*imGMask->getXYContours());
	
	Building->detectCornersAndLines(*imGMask->getXYPoints(), *imGMask->getXYContours(), *imGMask->getXYEdges());
	
	//Building->applyNDVIandEdgeThresh(imGMask->getHugeImage(), this->pSigmaNDVI->getHugeImage(), cropImg->getHugeImage(), *imGMask->getXYPoints(), *imGMask->getXYEdges());
	//Building->applyNDVIandEdgeThresh(imGMask->getHugeImage(), this->pSigmaNDVI->getHugeImage(), cropImg->getHugeImage(), *this->pSigmaNDVI->getXYPoints(), *this->pSigmaNDVI->getXYEdges());
	if (!this->useEntropy)
		Building->applyNDVIandEdgeThresh(imGMask->getHugeImage(), this->pSigmaNDVI->getHugeImage(), cropImg->getHugeImage(), *this->pSigmaNDVI->getXYPoints(), *imGMask->getXYEdges(), *this->pSigmaNDVI->getXYEdges());
	else
		Building->applyNDVIandEdgeThresh(imGMask->getHugeImage(), this->pSigmaNDVI->getHugeImage(), cropImg->getHugeImage(), *this->pSigmaNDVI->getXYPoints(), *imGMask->getXYEdges(), *this->pSigmaNDVI->getXYEdges(),this->entropyImage->getHugeImage());
	
	//Building->adjustLines(imGMask->getHugeImage(), cropImg->getHugeImage(), *imGMask->getXYPoints(), *imGMask->getXYEdges());
	if (this->useAdjustment)
		Building->adjustLines(imGMask->getHugeImage(), cropImg->getHugeImage(), *imGMask->getXYPoints(), *imGMask->getXYEdges(), *imUpMask->getXYEdges());
		//Building->adjustLinesWithImage(imGMask->getHugeImage(), cropImg->getHugeImage(), *this->colorImage->getXYPoints(), *this->colorImage->getXYContours(), *this->colorImage->getXYEdges(), *imGMask->getXYEdges());
	
	//Building->extendLines(imGMask->getHugeImage(), cropImg->getHugeImage(), *imGMask->getXYPoints(), *imGMask->getXYEdges());
	if (!this->useEntropy)
		Building->extendLines(imGMask->getHugeImage(), cropImg->getHugeImage(), *this->pSigmaNDVI->getXYPoints(), *imGMask->getXYEdges(), NULL);
	else
		Building->extendLines(imGMask->getHugeImage(), cropImg->getHugeImage(), *this->pSigmaNDVI->getXYPoints(), *imGMask->getXYEdges(), this->entropyImage->getHugeImage());

	//Building->formInitialBuildings(imGMask->getHugeImage(), cropImg->getHugeImage(), *imGMask->getXYPoints(), *imGMask->getXYEdges());
	if (!this->useEntropy)
		Building->formInitialBuildings(imGMask->getHugeImage(), cropImg->getHugeImage(), *this->pSigmaNDVI->getXYPoints(), *imGMask->getXYEdges(), NULL);
	else
		Building->formInitialBuildings(imGMask->getHugeImage(), cropImg->getHugeImage(), *this->pSigmaNDVI->getXYPoints(), *imGMask->getXYEdges(), this->entropyImage->getHugeImage());
	//Building->findFinalBuildings(imGMask->getHugeImage(), imUpMask->getHugeImage(), cropImg->getHugeImage(), Iy, Hi, Sq, *imGMask->getXYPoints(), *imGMask->getXYEdges());
	//Building->findFinalBuildings(imGMask->getHugeImage(), imUpMask->getHugeImage(), cropImg->getHugeImage(), Iy, Hi, Sq, *imGMask->getXYPoints(), *this->colorImage->getXYEdges());
	if (!this->useEntropy)
		Building->findFinalBuildings(imGMask->getHugeImage(), imUpMask->getHugeImage(), cropImg->getHugeImage(), Iy, Hi, Sq, *imGMask->getXYPoints(), *imUpMask->getXYEdges(), *this->colorImage->getXYEdges(), NULL);
	else
		Building->findFinalBuildings(imGMask->getHugeImage(), imUpMask->getHugeImage(), cropImg->getHugeImage(), Iy, Hi, Sq, *imGMask->getXYPoints(), *imUpMask->getXYEdges(), *this->colorImage->getXYEdges(), this->entropyImage->getHugeImage());

	//addition on 11 Oct 2011>>
	if (this->useHistogram)
	{
		/* the following code is for: when we compute texture info within detection algorithm 
		//
		//CXYPolyLineArray *brect = this->colorImage->getXYEdges();

		//first creat grey-scale image of the colour orthoimage

		//CVImage *GreyImage = project.addImage("Grey Scale",false);
		//CHugeImage *Grey = GreyImage->getHugeImage();
		// if we uncomment above 2 lines & 1 line after four lines from here; and comment the folloing line, then we can see the grey image in barista project 
		CHugeImage *Grey = new CHugeImage;
		
		Grey->prepareWriting("Grey Scale",inputImage->getWidth(), inputImage->getHeight(),8,1);
		CLookupTable lut(8,3); Grey->setLookupTable(&lut);
		inputImage->RGBtoGrey(Grey);
		//baristaProjectHelper.refreshTree();

		CHugeImage *entropy = new CHugeImage;
		CHugeImage *edgeOrientation = new CHugeImage;
		CHugeImage *edgeHug = new CHugeImage;
		entropy->setFileName("Entropy Image");
		edgeOrientation->setFileName("Edge Orientation Histogram");
		edgeHug->setFileName("Edge Numbers");

		baristaProjectHelper.setIsBusy(true);
		bool OK = Grey->computeEntropyEdgeAndOrientation(entropy, edgeOrientation,edgeHug);
		baristaProjectHelper.setIsBusy(false);
		//unsigned char *bufe = new unsigned char, *bufo = new unsigned char;
		//entropy->getRect(bufe,1,1,1,1);
		//edgeOrientation->getRect(bufo,1,1,1,1);

		*/

		//we assume that the computation of texture info was done before and user selects the corresponding files 
		//from building detection window

		ret = Building->rectifyFinalBuildings(this->orientationImage->getHugeImage(), this->edgeImage->getHugeImage(), *this->colorImage->getXYEdges());
	}
	//<< addition

	/*CXYPolyLineArray *polysFrom = imGMask->getXYEdges();
	CXYPolyLineArray *polysTo = this->colorImage->getXYEdges();
	for (int i = 0; i < polysFrom->GetSize(); i++)
	{		
		polysTo->add(*polysFrom->getAt(i));
	}

	CXYPointArray *pointsFrom = imGMask->getXYPoints();
	CXYPointArray *pointsTo = project.getImageAt(0)->getXYPoints();
	for (int i = 0; i < pointsFrom->GetSize(); i++)
	{		
		pointsTo->add(*pointsFrom->getAt(i));
	}

	pointsFrom = imGMask->getXYPoints();
	pointsTo = project.getImageAt(3)->getXYPoints();
	for (int i = 0; i < pointsFrom->GetSize(); i++)
	{		
		pointsTo->add(*pointsFrom->getAt(i));
	}*/

/*
	int fact = 3;
	CImageBuffer edgePixelBuffer;
	edgePixelBuffer.resample(*Building->getGroundMask(), fact);

	CImageBuffer multipliedWin;
	multipliedWin.set(edgePixelBuffer.getOffsetX(), edgePixelBuffer.getOffsetY(), edgePixelBuffer.getWidth(), edgePixelBuffer.getHeight(),1,1, true);

	CImageFilter morpho(eMorphological, fact, fact, 1.0);

	morpho[0] = 0; morpho[1] = 1; morpho[2] = 0;
	morpho[3] = 1; morpho[4] = 1; morpho[5] = 1;
	morpho[6] = 0; morpho[7] = 1; morpho[8] = 0;

	morpho.binaryMorphologicalOpen(edgePixelBuffer, multipliedWin, 1);
	multipliedWin.markEdgePixels(edgePixelBuffer);
	edgePixelBuffer.getContours(*imGMask->getXYContours());

	CImageBuffer edgePixelBufferFrom(*Building->getGroundMask());
	CImageBuffer edgePixelBufferTo;
	edgePixelBufferFrom.markEdgePixels(edgePixelBufferTo);
	edgePixelBufferTo.getContours(*imGMask->getXYContours());
*/
	//Building->getGroundMask()->getContours(*imGMask->getXYContours(),4);
/*
	CFeatureExtractor extractor (Building->getParameters());
	extractor.setRoi(Building->getGroundMask(), boundaryPoly);
	CRAG rag;

	CLabelImage labels(Building->getGroundMask()->getOffsetX(), Building->getGroundMask()->getOffsetY(), Building->getGroundMask()->getWidth(), Building->getGroundMask()->getHeight(), 8);
	extractor.extractFeatures(imGMask->getXYPoints(), imGMask->getXYContours(), imGMask->getXYEdges(), &labels, &rag, true);
	extractor.resetRoi();
	*/
	//CLabelImage labels(Building->getGroundMask()->getOffsetX(), Building->getGroundMask()->getOffsetY(), Building->getGroundMask()->getWidth(), Building->getGroundMask()->getHeight(), 8);
	//CFeatureExtractionThread *thread = new CFeatureExtractionThread(Building->getGroundMask(), this->boundaryPoly, imGMask->getXYPoints(), imGMask->getXYContours(), imGMask->getXYEdges(), &labels, Building->getParameters());
	//bui_ProgressDlg dlg(new CBuildingsDetectThread(this), thread);
	//bui_ProgressDlg dlg(thread, thread);
	//dlg.exec();
	
	imGMask->getXYContours()->setActive(true);
	//imGMask->getXYEdges()->setActive(true);
	// for visulising purpose
	//imUpMask->setTFW(*this->maskTFW);
    //imGMask->setTFW(*this->maskTFW);
	imUpMask->setTFW(tfw);
    imGMask->setTFW(tfw);

	//remove cropped tile
	project.deleteImage(cropImg);
	}
	else
	{
		//give warning and delete empty masks from project
		//QMessageBox::warning(NULL,"Mask creation error!","Mask generation failed!");
		project.deleteImage(imUpMask);
		project.deleteImage(imGMask);
		project.deleteImage(cropImg);

	}

	//delete the YIQ colour components
	delete Iy; 
	delete Hi; 
	delete Sq;
	}
	else//refine old buildings
	{
		CXYPolyLineArray *oldBuldings = this->colorImage->getXYEdges();
		bool rectify = false;

		for (int i = 0; i < oldBuldings->GetSize(); i++)
		{
			CXYPolyLine *ln = oldBuldings->getAt(i);
			if (ln->getPointCount() == 5)
				rectify = true;
			else
			{
				rectify = false;
				break;
			}
		}

		if (rectify)
			ret = Building->rectifyFinalBuildings(this->orientationImage->getHugeImage(), this->edgeImage->getHugeImage(), *this->colorImage->getXYEdges());
		else
			ret = false;
		//	QMessageBox::warning(this,"Rectifying detected buildings!","Empty set of buildings or some buildings do not have four corners!");
	}

   	return ret;		
}

/////////////////////////////////////////////
void bui_BuildingsDetector::Ok()
{
	this->success = true;
	this->close();
	CALSDataSet * alsdata;
	if (this->ALSfile)
		alsdata=this->ALSfile->getCALSData();
	else
		alsdata = 0;
	//CHugeImage *sigmandvi = 0;

	this->useNDVIinExtension = this->cBNDVIExtension->isChecked();
	this->useAdjustment = this->cBuseAdjusment->isChecked();
	//this->useEntropy = this->cBentropy->isChecked();
	//this->useHistogram = this->cBhistogram->isChecked();
	/*if (this->colorImage->hasTFW())
	{
		C3DPoint p1(0.0, colorImage->getHugeImage()->getHeight() - 1.0, 0.0);
		C3DPoint p2(colorImage->getHugeImage()->getWidth() - 1.0, 0.0, 0.0);
		C3DPoint P1,P2;
		colorImage->getTFW()->transformObs2Obj(P1, p1);
		colorImage->getTFW()->transformObs2Obj(P2, p2);
		this->resolution.x = fabs(colorImage->getTFW()->getA());
		this->resolution.y = fabs(colorImage->getTFW()->getE());
		this->llPoint.x    = P1.x;
		this->llPoint.y    = P1.y;			
		this->urPoint.x    = P2.x;			
		this->urPoint.y    = P2.y;
	}*/
	this->imgTFW= this->colorImage->getTFW();
	if (this->pDEM)
		this->demTFW = this->pDEM->getTFW();
	else
		this->demTFW = 0;

	//if (this->pSigmaNDVI) sigmandvi = this->pSigmaNDVI->getHugeImage();

	if (this->detectionMode)//detect buildings
		if (this->maskMode)
			Building = new BuildingsDetector (ALSfile->getCALSData(), colorImage->getHugeImage(), pDEM->getHugeImage(), 
			imgTFW, demTFW, llPoint, urPoint, resolution, demResolution, outputDir,this->useNDVIinExtension,
			this->useEntropy,this->useHistogram, detectionMode);
		else
			Building = new BuildingsDetector (ALSfile->getCALSData(), colorImage->getHugeImage(),
			imgTFW,         llPoint, urPoint, resolution,                outputDir,this->useNDVIinExtension,
			this->useEntropy,this->useHistogram, detectionMode);
	else//refine old buildings
		Building = new BuildingsDetector (colorImage->getHugeImage(), imgTFW, outputDir, detectionMode);

	//this->entropyImage->getHugeImage()->;
	//CALSDataSet *ALS, CHugeImage *colorImage, CHugeImage *dem, const CTFW *imgTFW, const CTFW *demTFW,
	//								  const C2DPoint &min, const C2DPoint &max, const C2DPoint &grid,  const C2DPoint &gridDEM,
	//								  const CCharString &directory

	bui_ProgressDlg *dlg = new bui_ProgressDlg(new CBuildingsDetectThread(this),this->Building, "Building Detection ...");
	dlg->exec();
	
	bool ret = dlg->getSuccess();
	delete dlg;
	
	baristaProjectHelper.refreshTree();
}
///////////////////////////////////////////

void bui_BuildingsDetector::Cancel()
{
	this->close();
}
///////////////////////////////////////////
void bui_BuildingsDetector::Help()
{
	CHtmlHelp::callHelp("UserGuide/BuildingDetector.html");
};
/////////////////////////////////

void bui_BuildingsDetector::ImageSelected()
{
	int indexSelected = this->MultispectralImageSelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfImages()))
	{
		this->colorImage = 0;
		setupImageSelector(this->MultispectralImageSelector,  this->colorImage,  this->noImageTxt);
	}
	else  
	{
		this->colorImage = project.getImageAt(indexSelected - 1);
		//this->resolution.x=this->colorImage->getTFW()->getA();
		//this->resolution.y=this->resolution.x;
	}

	this->setupExtents(3);
	this->checkSetup();
}

/////////////////////////////////

void bui_BuildingsDetector::DEMSelected()
{
	int indexSelected = this->DEMSelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfDEMs()))
	{
		this->pDEM = 0;
		setupDEMSelector(this->DEMSelector,  this->pDEM,  this->noImageTxt);
	}
	else  
	{
		this->pDEM = project.getDEMAt(indexSelected - 1);
		this->demResolution.x = this->pDEM->getTFW()->getA();
		this->demResolution.y = this->demResolution.x;
	}

	this->setupExtents(5);
	this->checkSetup();
}
/////////////////////////////////////////

void bui_BuildingsDetector::ALSSelected()
{
	int indexSelected = this->ALSselector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfAlsDataSets()))
	{
		this->ALSfile = 0;
		setupALSSelector(this->ALSselector,  this->ALSfile,  this->noImageTxt);
	}
	else  
	{
		this->ALSfile = project.getALSAt(indexSelected - 1);		
	}
	
	this->setupExtents(2);
	this->checkSetup();
}
///////////////////////////////////
void bui_BuildingsDetector::setupExtents(int code)
{
	// code = 1 for checking all, 2 for ALS only, 3 for colour image only, 4 for ndvi image only, 5 for DEM only
	if (this->ALSfile && (code == 1 || code == 2))
	{
		this->resolution.x = floor (this->ALSfile->getCALSData()->getPointDist().x + this->ALSfile->getCALSData()->getPointDist().y + 0.5) * 0.5;
		if (this->resolution.x < 0.5) this->resolution.x = 0.5;
		this->resolution.y = this->resolution.x;

		C2DPoint P1,P2;

		P1.x = floor (this->ALSfile->getCALSData()->getPMin().x);
		P1.y = floor (this->ALSfile->getCALSData()->getPMin().y);			
		P2.x = floor (this->ALSfile->getCALSData()->getPMax().x + 0.5);		
		P2.y = floor (this->ALSfile->getCALSData()->getPMax().y + 0.5);		

		if (this->llPoint.x < P1.x)
			this->llPoint.x = P1.x;
		if (this->llPoint.y < P1.y)
			this->llPoint.y = P1.y;
		if (this->urPoint.x > P2.x)
			this->urPoint.x = P2.x;
		if (this->urPoint.y > P2.y)
			this->urPoint.y = P2.y;
	}
		//addition>>
	if (this->colorImage && (code == 1 || code == 3))
	{
		if (this->colorImage->hasTFW())
		{
			C3DPoint p1(0.0, colorImage->getHugeImage()->getHeight() - 1.0, 0.0);
			C3DPoint p2(colorImage->getHugeImage()->getWidth() - 1.0, 0.0, 0.0);
			C3DPoint P1,P2;
			colorImage->getTFW()->transformObs2Obj(P1, p1);
			colorImage->getTFW()->transformObs2Obj(P2, p2);
			//this->resolution.x = fabs(colorImage->getTFW()->getA());
			//this->resolution.y = fabs(colorImage->getTFW()->getE());
			if (this->llPoint.x < P1.x)
				this->llPoint.x = P1.x;
			if (this->llPoint.y < P1.y)
				this->llPoint.y = P1.y;
			if (this->urPoint.x > P2.x)
				this->urPoint.x = P2.x;
			if (this->urPoint.y > P2.y)
				this->urPoint.y = P2.y;
		}
	}

	if (this->pSigmaNDVI && (code == 1 || code == 4))
	{
		if (this->pSigmaNDVI->hasTFW())
		{
			C3DPoint p1(0.0, pSigmaNDVI->getHugeImage()->getHeight() - 1.0, 0.0);
			C3DPoint p2(pSigmaNDVI->getHugeImage()->getWidth() - 1.0, 0.0, 0.0);
			C3DPoint P1,P2;
			pSigmaNDVI->getTFW()->transformObs2Obj(P1, p1);
			pSigmaNDVI->getTFW()->transformObs2Obj(P2, p2);
			//this->resolution.x = fabs(colorImage->getTFW()->getA());
			//this->resolution.y = fabs(colorImage->getTFW()->getE());
			if (this->llPoint.x < P1.x)
				this->llPoint.x = P1.x;
			if (this->llPoint.y < P1.y)
				this->llPoint.y = P1.y;
			if (this->urPoint.x > P2.x)
				this->urPoint.x = P2.x;
			if (this->urPoint.y > P2.y)
				this->urPoint.y = P2.y;
		}
	}

	if (this->pDEM && (code == 1 || code == 5))
	{
		if (this->pDEM->hasTFW())
		{
			C3DPoint p1(0.0, pDEM->getHugeImage()->getHeight() - 1.0, 0.0);
			C3DPoint p2(pDEM->getHugeImage()->getWidth() - 1.0, 0.0, 0.0);
			C3DPoint P1,P2;
			pDEM->getTFW()->transformObs2Obj(P1, p1);
			pDEM->getTFW()->transformObs2Obj(P2, p2);
			//this->resolution.x = fabs(colorImage->getTFW()->getA());
			//this->resolution.y = fabs(colorImage->getTFW()->getE());
			if (this->llPoint.x < P1.x)
				this->llPoint.x = P1.x;
			if (this->llPoint.y < P1.y)
				this->llPoint.y = P1.y;
			if (this->urPoint.x > P2.x)
				this->urPoint.x = P2.x;
			if (this->urPoint.y > P2.y)
				this->urPoint.y = P2.y;
		}
	}

	if (this->orientationImage && (code == 1 || code == 6))
	{
		if (this->orientationImage->hasTFW())
		{
			C3DPoint p1(0.0, orientationImage->getHugeImage()->getHeight() - 1.0, 0.0);
			C3DPoint p2(orientationImage->getHugeImage()->getWidth() - 1.0, 0.0, 0.0);
			C3DPoint P1,P2;
			orientationImage->getTFW()->transformObs2Obj(P1, p1);
			orientationImage->getTFW()->transformObs2Obj(P2, p2);
			//this->resolution.x = fabs(colorImage->getTFW()->getA());
			//this->resolution.y = fabs(colorImage->getTFW()->getE());
			if (this->llPoint.x < P1.x)
				this->llPoint.x = P1.x;
			if (this->llPoint.y < P1.y)
				this->llPoint.y = P1.y;
			if (this->urPoint.x > P2.x)
				this->urPoint.x = P2.x;
			if (this->urPoint.y > P2.y)
				this->urPoint.y = P2.y;
		}
	}

	if (this->edgeImage && (code == 1 || code == 7))
	{
		if (this->edgeImage->hasTFW())
		{
			C3DPoint p1(0.0, edgeImage->getHugeImage()->getHeight() - 1.0, 0.0);
			C3DPoint p2(edgeImage->getHugeImage()->getWidth() - 1.0, 0.0, 0.0);
			C3DPoint P1,P2;
			edgeImage->getTFW()->transformObs2Obj(P1, p1);
			edgeImage->getTFW()->transformObs2Obj(P2, p2);
			//this->resolution.x = fabs(colorImage->getTFW()->getA());
			//this->resolution.y = fabs(colorImage->getTFW()->getE());
			if (this->llPoint.x < P1.x)
				this->llPoint.x = P1.x;
			if (this->llPoint.y < P1.y)
				this->llPoint.y = P1.y;
			if (this->urPoint.x > P2.x)
				this->urPoint.x = P2.x;
			if (this->urPoint.y > P2.y)
				this->urPoint.y = P2.y;
		}
	}

	if (this->entropyImage && (code == 1 || code == 8))
	{
		if (this->entropyImage->hasTFW())
		{
			C3DPoint p1(0.0, entropyImage->getHugeImage()->getHeight() - 1.0, 0.0);
			C3DPoint p2(entropyImage->getHugeImage()->getWidth() - 1.0, 0.0, 0.0);
			C3DPoint P1,P2;
			entropyImage->getTFW()->transformObs2Obj(P1, p1);
			entropyImage->getTFW()->transformObs2Obj(P2, p2);
			//this->resolution.x = fabs(colorImage->getTFW()->getA());
			//this->resolution.y = fabs(colorImage->getTFW()->getE());
			if (this->llPoint.x < P1.x)
				this->llPoint.x = P1.x;
			if (this->llPoint.y < P1.y)
				this->llPoint.y = P1.y;
			if (this->urPoint.x > P2.x)
				this->urPoint.x = P2.x;
			if (this->urPoint.y > P2.y)
				this->urPoint.y = P2.y;
		}
	}
		//<<addition

	if (!this->ALSfile && !this->colorImage && !this->pDEM && !this->pSigmaNDVI)
	{
		this->llPoint.x = FLT_MIN;
		this->llPoint.y = FLT_MIN;
		this->urPoint.x = FLT_MAX;
		this->urPoint.y = FLT_MAX;

		this->MAXllPoint.x = FLT_MIN;
		this->MAXllPoint.y = FLT_MIN;
		this->MAXurPoint.x = FLT_MAX;
		this->MAXurPoint.y = FLT_MAX;

		this->extentsInit = false;

		this->im_LLX->setEnabled(false);
		this->im_LLY->setEnabled(false);
		this->im_UPX->setEnabled(false);
		this->im_UPY->setEnabled(false);
		this->maxExtentButton->setEnabled(false);
	}
	else
	{
		this->extentsInit = true;
		this->MAXllPoint.x = this->llPoint.x;
		this->MAXllPoint.y = this->llPoint.y;
		this->MAXurPoint.x = this->urPoint.x;
		this->MAXurPoint.y = this->urPoint.y;

		this->im_LLX->setEnabled(true);
		this->im_LLY->setEnabled(true);
		this->im_UPX->setEnabled(true);
		this->im_UPY->setEnabled(true);
		this->maxExtentButton->setEnabled(true);

		this->setCurrentExtentsAndResolution();
	}
};

///////////////////////////////////
void bui_BuildingsDetector::maxExtentsSelected()
{
	if (this->extentsInit)
	{
		this->llPoint.x = this->MAXllPoint.x;
		this->llPoint.y = this->MAXllPoint.y;
		this->urPoint.x = this->MAXurPoint.x;
		this->urPoint.y = this->MAXurPoint.y;
		this->setCurrentExtentsAndResolution();
	}
}
///////////////////////////////////
void bui_BuildingsDetector::setCurrentExtentsAndResolution()
{

	QString dt=QString("%1").arg(this->resolution.x, 0, 'f', 3);

	this->im_LLY->blockSignals(true);
	this->im_LLX->blockSignals(true);
	this->im_UPX->blockSignals(true);
	this->im_UPY->blockSignals(true);

	dt=QString("%1").arg(this->llPoint.x, 0, 'f', 3);
	this->im_LLX->setText(dt);

	dt=QString("%1").arg(this->llPoint.y, 0, 'f', 3);
	this->im_LLY->setText(dt);

	dt=QString("%1").arg(this->urPoint.x, 0, 'f', 3);
	this->im_UPX->setText(dt);

	dt=QString("%1").arg(this->urPoint.y, 0, 'f', 3);
	this->im_UPY->setText(dt);
	
	
	this->im_LLY->blockSignals(false);
	this->im_LLX->blockSignals(false);
	this->im_UPY->blockSignals(false);
	this->im_UPX->blockSignals(false);   
};
////////////////////////////////////////////

void bui_BuildingsDetector::LLChanged()
{
	bool isDouble;
	C2DPoint Res(0.25,0.25); //mask resolution

	double hlp = this->im_LLX->text().toDouble(&isDouble);
	if (isDouble && fabs(llPoint.x - hlp) > FLT_EPSILON && hlp >= MAXllPoint.x)
	{
		llPoint.x = hlp;
		if (urPoint.x <= llPoint.x) urPoint.x = llPoint.x + Res.x;
		urPoint.x = llPoint.x + (floor((urPoint.x - llPoint.x) / Res.x)) * Res.x;
	}
 
	hlp = this->im_LLY->text().toDouble(&isDouble);
	if (isDouble && fabs(llPoint.y - hlp) > FLT_EPSILON && hlp >= MAXllPoint.y) 
	{
		llPoint.y = hlp;
		if (urPoint.y <= llPoint.y) urPoint.y = llPoint.y + Res.y;
		urPoint.y = llPoint.y + (floor((urPoint.y - llPoint.y) / Res.y)) * Res.y;
	}
	
	this->setCurrentExtentsAndResolution();
};
	  
void bui_BuildingsDetector::RUChanged()
{
	bool isDouble;
	C2DPoint Res(0.25,0.25); //mask resolution
	double hlp = this->im_UPX->text().toDouble(&isDouble);
	if (isDouble && fabs(urPoint.x - hlp) > FLT_EPSILON && hlp <= MAXurPoint.x) 
	{
		urPoint.x = hlp;
		if (urPoint.x <= llPoint.x) llPoint.x = urPoint.x - Res.x;
		llPoint.x = urPoint.x - (floor((urPoint.x - llPoint.x) / Res.x)) * Res.x;
	}

	hlp = this->im_UPY->text().toDouble(&isDouble);
	if (isDouble && fabs(urPoint.y - hlp) > FLT_EPSILON && hlp <= MAXurPoint.y)
	{
		urPoint.y = hlp;
		if (urPoint.y <= llPoint.y) llPoint.y = urPoint.y - Res.y;
		llPoint.y = urPoint.y - (floor((urPoint.y - llPoint.y) / Res.y)) * Res.y;
	}
	
	this->setCurrentExtentsAndResolution();
};
/////////////////////////////////////////////

void bui_BuildingsDetector::explorePathSelected()
{
	QString dirQ = QFileDialog::getExistingDirectory (this, "Choose Output Directory",
		                                              this->outputDir.GetChar());

	CFileName direc = ((const char*)dirQ.toLatin1());
		
	if (!direc.IsEmpty())
	{
		this->outputDir = direc;
		if (this->outputDir[this->outputDir.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->outputDir += CSystemUtilities::dirDelimStr;

		this->outputDirectory->setText(this->outputDir.GetChar());
	}
};
//////////////////////////////////////////////

void bui_BuildingsDetector::checkSetup()
{
	bool enable = false, enable1=false, enable2=false, enable3=false, enable4=false;

	if (this->detectionMode)//detect new buildings
	{
		enable1 = this->ALSfile && this->colorImage && this->pSigmaNDVI && this->pDEM;
		enable2 = this->ALSfile && this->colorImage && this->pSigmaNDVI && this->maskMode == false;
		if (this->useEntropy)//ie, if (this->useEntropy && this->useHistogram)
		{
			enable3 = this->entropyImage && this->orientationImage && this->edgeImage;
			//enable4 = this->orientationImage && this->edgeImage;
		}
		else if (this->useHistogram)
			enable4 = this->orientationImage && this->edgeImage;

		if (this->useEntropy)
		{
			enable = (enable1 && enable3) || (enable2 && enable3);
		}
		else if (this->useHistogram)
		{
			enable = (enable1 && enable4) || (enable2 && enable4);
		}
		else
			enable = enable1 || enable2;		
	}
	else//refine old detected buildings
	{
		enable = this->colorImage && this->orientationImage && this->edgeImage;
	}

	if (enable)
	{
		this->okButton->setEnabled(true);
	}
	else
	{
		this->okButton->setEnabled(false);
	}
}
//////////////////////////////////////////

bool bui_BuildingsDetector::getSuccess()
{
 return success;
}

/////////////////////////////////////////

void bui_BuildingsDetector::NDVISigmaSelected()
{
	int indexSelected = this->SigmaNDVISelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfImages()))
	{
		this->pSigmaNDVI = 0;
		setupImageSelector(SigmaNDVISelector,  this->pSigmaNDVI,  this->noSigmaNDVITxt);
	}
	else
	{
		this->pSigmaNDVI = project.getImageAt(indexSelected - 1);

		
//		if (this->pNDVI == this->pSigmaNDVI) 
//		{
//			this->pNDVI = 0;
//			setupImageSelector(NDVISelector,  this->pNDVI,  this->noNDVITxt);
//		}
	}
	
	this->setupExtents(4);
	this->checkSetup();
}

/////////////////////////////////////////

void bui_BuildingsDetector::OrientationSelected()
{
	int indexSelected = this->OrientationSelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfImages()))
	{
		this->orientationImage = 0;
		setupImageSelector(this->OrientationSelector,  this->orientationImage,  this->noImageTxt);
	}
	else  
	{
		this->orientationImage = project.getImageAt(indexSelected - 1);
		//this->resolution.x=this->colorImage->getTFW()->getA();
		//this->resolution.y=this->resolution.x;
	}

	this->setupExtents(6);
	this->checkSetup();
}
/////////////////////////////////////////

void bui_BuildingsDetector::EdgeImageSelected()
{
	int indexSelected = this->EdgeImageSelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfImages()))
	{
		this->edgeImage = 0;
		setupImageSelector(this->EdgeImageSelector,  this->edgeImage,  this->noImageTxt);
	}
	else  
	{
		this->edgeImage = project.getImageAt(indexSelected - 1);
		//this->resolution.x=this->colorImage->getTFW()->getA();
		//this->resolution.y=this->resolution.x;
	}

	this->setupExtents(7);
	this->checkSetup();
}
/////////////////////////////////////////

void bui_BuildingsDetector::EntropySelected()
{
	int indexSelected = this->EntropySelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfImages()))
	{
		this->entropyImage = 0;
		setupImageSelector(this->EntropySelector,  this->entropyImage,  this->noImageTxt);
	}
	else  
	{
		this->entropyImage = project.getImageAt(indexSelected - 1);
		//this->resolution.x=this->colorImage->getTFW()->getA();
		//this->resolution.y=this->resolution.x;
	}

	this->setupExtents(8);
	this->checkSetup();
}
/////////////////////////////////////////