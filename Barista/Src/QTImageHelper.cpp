/** 
 * @class QTImageHelper
 * helps create Qimages through various static functions
 *
 * @author Thomas Weser
 * @version 1.0 
 */

#include "QTImageHelper.h"
#include <QPainter>
#include <QImage>
#include <QIcon>
#include <QPen>


#include "Icons.h"
#include "HugeImage.h"
#include "VImage.h"
#include "histogram.h"
#include "BaristaProject.h"
#include "SystemUtilities.h"



int QTImageHelper::MinNIR    =   0;
int QTImageHelper::MaxNIR    = 255;
int QTImageHelper::MinRed    =   0;
int QTImageHelper::MaxRed    = 255;
int QTImageHelper::MinGreen  =   0;
int QTImageHelper::MaxGreen  = 255;
int QTImageHelper::MinBlue   =   0;
int QTImageHelper::MaxBlue   = 255;

int QTImageHelper::MinGrey   =   0;
int QTImageHelper::MaxGrey   = 255;
int QTImageHelper::MinColour =   0;
int QTImageHelper::MaxColour = 255;

float QTImageHelper::colourFactor = 1.0;
float QTImageHelper::greyFactor   = 1.0;

QImage* QTImageHelper::createTileQImage(CHugeImage* image,int tileR, int tileC, float zoomFactor)
{
	CImageBuffer imgBuf;

	if (image->getImageTile(imgBuf,tileR,tileC,zoomFactor))
		return QTImageHelper::createCommonQImage(imgBuf,image);
	else
		return new QImage();
}


QImage* QTImageHelper::createQImage(CHugeImage* image,int rectTop, int rectHeight, int rectLeft, int rectWidth, int destHeight, int destWidth)
{
	CImageBuffer imgBuf;

	if (image->getRect(imgBuf,rectTop,rectHeight,rectLeft,rectWidth,destHeight,destWidth))
		return QTImageHelper::createCommonQImage(imgBuf,image);
	else
		return new QImage();
}


QImage* QTImageHelper::createCommonQImage(const CImageBuffer& imgBuf,CHugeImage* image)
{
	QImage* newQImage = new QImage(imgBuf.getWidth(),imgBuf.getHeight(),QImage::Format_RGB32);
	QTImageHelper::fillQImage(newQImage, imgBuf,image);
	return  newQImage; 
}


bool QTImageHelper::createQIcon(QIcon* qicon, CHugeImage* image, int rectTop, int rectHeight, int rectLeft, int rectWidth, int destHeight, int destWidth)
{
	QImage *im = QTImageHelper::createQImage(image,rectTop,rectHeight,rectLeft,rectWidth,destHeight,destWidth);
	QPixmap p = QPixmap::fromImage(*im);
	qicon->addPixmap(p);
	delete im;
	return true;
}



bool QTImageHelper::fillQImage(QImage* qimage,const CImageBuffer& imgBuf, CHugeImage* image)
{
	int red = image->getRChannel();
	int green = image->getGChannel();
	int blue = image->getBChannel();

	const CTransferFunction* transferFunction = image->getTransferFunction();
	
	if (!transferFunction)
		return false;

	int w = qimage->width();
	int h = qimage->height();
	int cr, cg, cb;
	int lineOffset;
	long cMax = 4 * w;

    // 16 bit grey scale image
	if ( (imgBuf.getBands() == 1) && (imgBuf.getBpp()== 16) )
    {
		unsigned short *shortBuf = imgBuf.getPixUS();

		for (int r = 0; r < h; r++)
       {
            uchar* line =  qimage->scanLine ( r );
            lineOffset = r * w; 

            for (int c = 0; c < w; c++)
            {
				unsigned short entry =shortBuf[lineOffset+c];   
				transferFunction->getColor(cr, cg, cb, entry);
				line[c*4] = cb;
				line[c*4 + 1] = cg;
				line[c*4 + 2] = cr;
            }
       }
    }
    else if ( (imgBuf.getBands() == 3 || imgBuf.getBands() == 4) && (imgBuf.getBpp() == 16) ) //3 or 4 bands, 16-bit rgb image
    {
		unsigned short *shortBuf = imgBuf.getPixUS();

		for (int r = 0; r < h; r++)
		{
            uchar* line =  qimage->scanLine ( r );
            lineOffset = r * cMax;
                
            for (int c = 0; c < cMax; c += 4)
            {
				transferFunction->getColor(cr, // get blue
								   cg, // get green
								   cb, // get red
								   shortBuf[lineOffset + c + red],
								   shortBuf[lineOffset + c + green],
								   shortBuf[lineOffset + c + blue]);
				line[c    ] = cb;
				line[c + 1] = cg;
				line[c + 2] = cr;

            }
       }
    }
	else if ((imgBuf.getBands() == 1) && (imgBuf.getBpp() == 8) )//1 bands, 8-bit grey image
    {
		for (int r = 0; r < h; r++)
		{
            uchar* line =  qimage->scanLine ( r );
            lineOffset = r * w;
                
			unsigned char* buf = imgBuf.getPixUC();

            for (int c = 0; c < w; c ++)
            {
				transferFunction->getColor(cr, cg, cb,buf[lineOffset+c]);
				line[c*4] = cb;
				line[c*4 + 1] = cg;
				line[c*4 + 2] = cr;
			}
		}
    }
    else if ( (imgBuf.getBands() == 3 || imgBuf.getBands() == 4) && (imgBuf.getBpp() == 8) ) //3 and 4 bands, 8-bit rgb image
    {
		for (int r = 0; r < h; r++)
		{
            uchar* line =  qimage->scanLine ( r );
            lineOffset = r * cMax;
               
			unsigned char* buf = imgBuf.getPixUC();

            for (int c = 0; c < cMax; c += 4)
            {
				transferFunction->getColor(cr,
							   cg, 
							   cb, 
							   buf[lineOffset + c + red],
							   buf[lineOffset + c + green],
							   buf[lineOffset + c + blue]);
				line[c    ] = cb;
				line[c + 1] = cg;
				line[c + 2] = cr;
            }
       }
    }
	else if (imgBuf.getBpp() == 32)
	{
		float *fbuf = (float *) imgBuf.getPixF();

		if (imgBuf.getBands() == 1)//1 bands, 32-bit (float) grey image
		{
		
			QTImageHelper::computeHistograms(image);
			
			for (int r = 0; r < h; r++)
			{
				uchar* line =  qimage->scanLine ( r );
				int lineOffset = r * w;
	                
				for (int c = 0; c < w; c ++)
				{
					float val = fbuf[lineOffset+c];
					
					if (val == -9999) 
					{
						cr = 0;
						cg = 0;
						cb = 0;
					}
					else 
						transferFunction->getColor(cr,cg,cb,scaleGrey(val));

					line[c*4] = cb;
					line[c*4 + 1] = cg;
					line[c*4 + 2] = cr;
				}
			}
		}
	}
    
    return true;
}



void QTImageHelper::computeHistograms(CHugeImage* image)
{
    // we only need the scaling when it's 16-bits
    if (image->getBpp() == 16)
    {
        //CHistogram* histogram = image->getHistogram();
        CHistogram* histogram = image->getHistogram(0);
        MaxGrey = histogram->percentile(0.98);
        MinGrey = histogram->percentile(0.02);
        greyFactor = 255.0 / (MaxGrey - MinGrey);

        if (image->getBands() == 3)
        {
            MinRed   = image->getHistogram(0)->percentile(0.1);         
            MaxRed   = image->getHistogram(0)->percentile(0.999);
            MinGreen = image->getHistogram(1)->percentile(0.08);
            MaxGreen = image->getHistogram(1)->percentile(0.997);
            MinBlue  = image->getHistogram(2)->percentile(0.1);
            MaxBlue  = image->getHistogram(2)->percentile(0.999);

			if ((MinRed < MinGreen) && (MinRed < MinBlue)) MinColour = MinRed;
			else if (MinGreen < MinBlue) MinColour = MinGreen;
			else MinColour = MinBlue;

			if ((MaxRed > MaxGreen) && (MaxRed > MaxBlue)) MaxColour = MaxRed;
			else if (MaxGreen > MaxBlue) MaxColour = MaxGreen;
			else MaxColour = MaxBlue;

			colourFactor = 255.0 / (MaxColour - MinColour);
        }
        else if (image->getBands() == 4)
        {
            MinRed   = image->getHistogram(0)->percentile(0.02);
            MaxRed   = image->getHistogram(0)->percentile(0.95);
            MinGreen = image->getHistogram(1)->percentile(0.05);
            MaxGreen = image->getHistogram(1)->percentile(0.985);
            MinBlue  = image->getHistogram(2)->percentile(0.02);
            MaxBlue  = image->getHistogram(2)->percentile(0.95);
            MinNIR   = image->getHistogram(3)->percentile(0.02);
            MaxNIR   = image->getHistogram(3)->percentile(0.90);

			if ((MinNIR < MinRed) && (MinNIR < MinGreen) && (MinNIR < MinBlue)) MinColour = MinNIR;
			else if ((MinRed < MinGreen) && (MinRed < MinBlue)) MinColour = MinRed;
			else if (MinGreen < MinBlue) MinColour = MinGreen;
			else MinColour = MinBlue;

			if ((MaxNIR > MaxRed) && (MaxNIR > MaxGreen) && (MaxNIR > MaxBlue)) MaxColour = MaxNIR;
			else if ((MaxRed > MaxGreen) && (MaxRed > MaxBlue)) MaxColour = MaxRed;
			else if (MaxGreen > MaxBlue) MaxColour = MaxGreen;
			else MaxColour = MaxBlue;

			colourFactor = 255.0 / (MaxColour - MinColour);
        }
    }
	else if (image->getBpp() == 32)
	{
        CHistogram* histogram = image->getHistogram(0);

		if (image->getTransferFunction()->isColorIndexed())
		{
			MaxGrey = histogram->getMax();
			MinGrey = histogram->getMin();
		}
		else
		{
			MaxGrey = image->getTransferFunction()->getParameter(1);
			MinGrey = image->getTransferFunction()->getParameter(0);
		}
		for (int i = 1; i < image->getBands(); ++i)
		{
			float val = image->getHistogram(i)->getMax();
			if (val < MinGrey) MinGrey = val;
			if (val > MaxGrey) MaxGrey = val;
			val = image->getHistogram(i)->getMin();
			if (val < MinGrey) MinGrey = val;
			if (val > MaxGrey) MaxGrey = val;
		}

        greyFactor = 255.0 / (MaxGrey - MinGrey);
	}
}




QImage QTImageHelper::makeIcon(const ZObject* object)
{
    QImage tmp;

    if (object->isa("CXYPointArray"))
    {
        CXYPointArray* points = (CXYPointArray*)object;

		if (points->getPointType() == MEASURED || points->getPointType() == PROJECTED)
		{
			if (points->getActive())
				tmp.loadFromData( ActivePoints16, sizeof( ActivePoints16 ), "PNG" );
			else
				tmp.loadFromData( Points16, sizeof( Points16 ), "PNG" );

		}
		//else if (points->getPointType() == RESIDUALS)
		else if (points->getPointType() == RESIDUALS || points->getPointType() == OBJRESIDUALS)
		{
			tmp.loadFromData( Residuals, sizeof( Residuals ), "PNG" );
		}
		else if (points->getPointType() == MONOPLOTTED)
		{
			tmp.loadFromData( MonoPoints, sizeof( MonoPoints ), "PNG" );
		}

    }
    else if (object->isa("CXYZPointArray"))
    {
        CXYZPointArray* points = (CXYZPointArray*)object;

		if (points->getPointType() == eRESIDUAL)
		{
			tmp.loadFromData( Residuals, sizeof( Residuals ), "PNG" );
		}
        else if (points->getReferenceSystem()->getCoordinateType() == eUndefinedCoordinate)
        {
            if (points->isControl())
            {
                tmp.loadFromData( localxyzControl, sizeof( localxyzControl ), "PNG" ); 
            }
            else
            {
                tmp.loadFromData( localxyz, sizeof( localxyz ), "PNG" ); 
            }
        }
        else if (points->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)
        {
            if (points->isControl())
            {
                tmp.loadFromData( geographiccontrol16, sizeof( geographiccontrol16 ), "PNG" ); 
            }
            else
            {
                tmp.loadFromData( geographic16, sizeof( geographic16 ), "PNG" ); 
            }
        }
        else if (points->getReferenceSystem()->getCoordinateType() == eGEOCENTRIC)
        {
            if (points->isControl())
            {
                tmp.loadFromData( geocentriccontrol16, sizeof( geocentriccontrol16 ), "PNG" ); 
            }
            else
            {
                tmp.loadFromData( geocentric16, sizeof( geocentric16 ), "PNG" ); 
            }
        }
        else if (points->getReferenceSystem()->getCoordinateType() == eGRID)
        {
			if (points->getReferenceSystem()->isUTM())
			{
				if (points->isControl())
				{
					tmp.loadFromData( utmcontrol16, sizeof( utmcontrol16 ), "PNG" );
				}
				else
				{
					tmp.loadFromData( utm16, sizeof( utm16 ), "PNG" );
				}
			}
			else
			{
				if (points->isControl())
				{
					tmp.loadFromData( TM16control, sizeof( TM16control ), "PNG" );
				}
				else
				{
					tmp.loadFromData( TM16, sizeof( TM16 ), "PNG" );
				}
			}
        }    
    }
    else if (object->isa("CRPC"))
    {      
		CSensorModel* model = (CSensorModel*)object;
		if (model->isCurrentSensorModel())
			tmp.loadFromData( RPCcurrent, sizeof( RPCcurrent ), "PNG" );
		else		
			tmp.loadFromData( RPC, sizeof( RPC ), "PNG" );
	}
    else if (object->isa("CAffine"))
    {
		CSensorModel* model = (CSensorModel*)object;
		if (model->isCurrentSensorModel())
			tmp.loadFromData( Affinecurrent, sizeof( Affinecurrent ), "PNG" );
		else		
			tmp.loadFromData( Affine, sizeof( Affine ), "PNG" ); 
    }
    else if (object->isa("CTFW"))
    { 
        tmp.loadFromData( tfw, sizeof( tfw ), "PNG" );
	}
    else if (object->isa("CVDEM"))
    {
		CVDEM* dem = (CVDEM*)object;

		if (dem->getIsActiveDEM())
		{
			tmp.loadFromData( ActiveDEM, sizeof( ActiveDEM ), "PNG" );
		}
		else
		{
			tmp.loadFromData( DEM16, sizeof( DEM16 ), "PNG" );
		}
    }
    else if (object->isa("CXYZPolyLineArray"))
    {
        tmp.loadFromData( Lines, sizeof( Lines ), "PNG" );
    }
    else if (object->isa("CBuildingArray"))
    {
        tmp.loadFromData( Buildings, sizeof( Buildings ), "PNG" );
    }
	else if (object->isa("CXYContourArray"))
    {
        CXYContourArray* contours = (CXYContourArray*)object;

		if (contours->GetSize()>0)
		{
			tmp.loadFromData( Contours, sizeof( Contours ), "PNG" );
		}
		
    }
	else if (object->isa("CXYPolyLineArray"))
    {
        CXYPolyLineArray* polygons = (CXYPolyLineArray*)object;

		if (polygons->GetSize()>0)
		{
			tmp.loadFromData( XYPolylines, sizeof( XYPolylines ), "PNG" );
		}
		
    }
	else if (object->isa("CXYEllipseArray"))
    {
        CXYEllipseArray* ellipses = (CXYEllipseArray*)object;

		if (ellipses->GetSize()>0)
		{
			tmp.loadFromData( EllipseIcon, sizeof( EllipseIcon ), "PNG" );
		}
		
    }
	else if (object->isa("CVALS"))
    {
        CVALS* als = (CVALS*) object;
		tmp.loadFromData( ALSIcon, sizeof( ALSIcon ), "PNG" );		
    }
	else if (object->isa("CPushBroomScanner"))
    {
		CSensorModel* model = (CSensorModel*)object;
		if (model->isCurrentSensorModel())
			tmp.loadFromData( PushBroomcurrent, sizeof( PushBroomcurrent ), "PNG" );
		else		
			tmp.loadFromData( PushBroom, sizeof( PushBroom ), "PNG" );
	
    }
	else if (object->isa("CCameraMounting"))
    {
		tmp.loadFromData( MountingIcon, sizeof( MountingIcon ), "PNG" );		
    }
	else if (object->isa("CCCDLine"))
    {
		tmp.loadFromData( CameraIcon, sizeof( CameraIcon ), "PNG" );		
    }
	else if (object->isa("CFrameCamera"))
    {
		tmp.loadFromData( CameraIcon, sizeof( CameraIcon ), "PNG" );		
    }
	else if (object->isa("CFrameCameraAnalogue"))
    {
		tmp.loadFromData( CameraIcon, sizeof( CameraIcon ), "PNG" );		
    }
	else if (object->isa("COrbitObservations"))
    {
		COrbitObservations* orbit = (COrbitObservations*)object;

		if ( orbit->getSplineType() == Path )
			tmp.loadFromData( PathIcon, sizeof( PathIcon ), "PNG" );
			
		if ( orbit->getSplineType() == Attitude )
			tmp.loadFromData( AttitudeIcon, sizeof( AttitudeIcon ), "PNG" );
    }


    return tmp;
}


bool QTImageHelper::writeImageChips(CVImage* vimage, CFileName dir, int width, int height, bool withHTML, bool withControl)
{
	if (!vimage || !vimage->getHugeImage() || !vimage->getHugeImage()->getTransferFunction())
		return false;
	
	bool success = false;

	if (!dir.IsEmpty())
	{
		if (dir[dir.GetLength() - 1] != CSystemUtilities::dirDelim) dir += CSystemUtilities::dirDelimStr;
	}

	CXYPointArray* xypoints = vimage->getXYPoints();
	CFileName filename;
	CFileName imagetype(".png");

	CFileName imagename(vimage->getName());

	CHugeImage* himage = vimage->getHugeImage();

	int w = width;
	int h = height;

	QPoint qp(10, 20);
		
	QPen whitepen(Qt::white);
	QPen blackpen(Qt::black);
	whitepen.setWidth(3);
	blackpen.setWidth(3);

	CVControl* control = project.getControlPoints();
	CXYZPointArray* controlpoints;

	bool hasControl = false;

	if ( control && withControl)
	{
		controlpoints = project.getControlPoints()->getXYZPointArray();
		if ( controlpoints ) hasControl = true;
	}

	// open a file for HTML output and write header
	FILE	*fp;
	CFileName html(dir + CSystemUtilities::dirDelimStr + "chips.html");

	if ( withHTML )
	{
		fp = fopen(html.GetChar(),"w");

		fprintf(fp,"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd\">\n");
		fprintf(fp,"<HTML>\n<BODY>\n");
		fprintf(fp,"<DIV CLASS=\"doc\">\n");
		fprintf(fp,"<H3 CLASS=\"doc\">Points measured in image: %s</H3>\n", imagename.GetFullFileName().GetChar());

		fprintf(fp,"<DIV CLASS=\"table\">\n");
		fprintf(fp,"<TABLE CLASS=\"doc\" border=\"2px\">\n");
		fprintf(fp,"\t<TR CLASS=\"doc\">\n");
		fprintf(fp,"\t\t<TH CLASS=\"doc\">Image chip</TH>\n");
		fprintf(fp,"\t\t<TH CLASS=\"doc\">2D Information</TH>\n");

		// add 3D info column if control points are available
		if ( hasControl) fprintf(fp,"\t\t<TH CLASS=\"doc\">3D Information</TH>\n");
	}

	int npoints = xypoints->GetSize();
	for (int i= 0; i< npoints; i++)
	{
		CXYPoint *p = xypoints->getAt(i);

		CFileName jpgname(p->getLabel().GetChar());
		jpgname += imagetype;

		filename = dir + jpgname;
		
		QString title;
		title.sprintf("Chip %s (%d from %d)", p->getLabel().GetChar(), i+1, npoints);

		int ulx = p->x -width/2;
		int uly = p->y -height/2;

		QImage* qimage = QTImageHelper::createQImage(himage,uly,h,ulx,w,h,w);

		QPainter qpainter;
		
		qpainter.begin(qimage);

		QString qlabel(p->getLabel().GetChar());
		QString xtext = QString(" %1,").arg(p->x, 0, 'F', 2);
		QString ytext = QString(" %1").arg(p->y, 0, 'F', 2);

		qlabel.append(xtext);
		qlabel.append(ytext);

		qpainter.setFont(QFont("Helvetica", 10, 75));
		qpainter.setPen(whitepen);
		qpainter.drawText(qp, qlabel);
		qpainter.drawEllipse(w/2-10, h/2-10, 20, 20);

		qpainter.setPen(blackpen);
		qpainter.drawEllipse(w/2-13, h/2-13, 26, 26);

		qpainter.end();

		success = qimage->save(filename.GetChar(),"PNG");
		
		delete qimage;
		qimage = 0;

		// create entry in html table
		if ( withHTML )
		{
			fprintf(fp,"\t\t<TR CLASS=\"doc\">\n");
			fprintf(fp,"\t\t<TD CLASS=\"center\"><IMG CLASS=\"inline\" SRC=\"%s\" ALT=\"Inline Graphik\"></TD>\n", jpgname.GetChar());
			fprintf(fp,"\t\t<TD CLASS=\"center\"> Label: %s<BR> Sample: %.2f<BR> Line: %.2f</TD>\n", p->getLabel().GetChar(), p->x, p->y );


			if ( hasControl)
			{
				CXYZPoint* p3d = (CXYZPoint*)controlpoints->getPoint(p->getLabel().GetChar());

				if ( p3d )
				{	
					if ( controlpoints->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)
						fprintf(fp,"\t\t<TD CLASS=\"center\"> X: %.8f<BR> Y: %.8f<BR> Z: %.3f</TD>\n", p3d->x, p3d->y, p3d->z );
					else
						fprintf(fp,"\t\t<TD CLASS=\"center\"> X: %.3f<BR> Y: %.3f<BR> Z: %.3f</TD>\n", p3d->x, p3d->y, p3d->z );
					
				}
				else
					fprintf(fp,"\t\t<TD CLASS=\"center\"> not a control point</TD>\n");
			}

			fprintf(fp,"\t\t</TR>\n");
		}
	}

	// finish html file and close it
	if ( withHTML )
	{
		fprintf(fp,"\t</TR>\n");
		fprintf(fp,"</TABLE>\n");
		fprintf(fp,"</DIV>\n");
		fprintf(fp,"</DIV>\n");
		fprintf(fp,"</BODY>\n");
		fprintf(fp,"</HTML>\n");
		fclose(fp);
	}

	return success;
}


QPixmap QTImageHelper::createNoFileFoundPixmap()
{
	QPixmap pixmap(70,70);
	
	QPainter painter;
	painter.begin(&pixmap);

	QColor c(Qt::red);
	QBrush brush(c);
	painter.fillRect(0,0,70,70,brush);
	QPen whitepen(Qt::white);
	whitepen.setWidth(2);
	painter.setPen(whitepen);
	painter.setFont(QFont("Helvetica", 12, 75));
	painter.drawText(10,20, QString("File"));
	painter.drawText(10,40, QString("not"));
	painter.drawText(10,60, QString("Found!"));
	painter.end();
	
	return pixmap;
}