#include "keylock.h"
#include "HardwareDongle.h"
#include "Encryption.h"


KeyLock::KeyLock(QWidget *parent, Qt::WindowFlags flags)	: QDialog(parent, flags)
{
	this->setupUi(this);
	this->connect(this->pBClose,SIGNAL(released()),this,SLOT(pBCloseReleased()));
	this->connect(this->pBRead,SIGNAL(released()),this,SLOT(pBReadReleased()));
	this->connect(this->pBWrite,SIGNAL(released()),this,SLOT(pBWriteReleased()));
}

KeyLock::~KeyLock()
{

}

void KeyLock::pBReadReleased()
{
#ifdef WIN32	
	CHardwareDongle dongle;
	CEncryption e;

	if (!dongle.checkDongelPresents())
	{
		this->leSerial->setText("Not Present");
		return;
	}


	unsigned int sn = dongle.readSerialNunmber();
	unsigned long id = dongle.getUniqueID();
	

	unsigned int v = dongle.getVersionNumber();

	unsigned int maxUser = dongle.getMaxUsers();

	QString serial;
	QString idString;
	QString version;
	QString users;

	serial.sprintf("%u",sn);
	if (e.isValidCode(sn,id))
		idString.sprintf("%u",id);
	else
		idString.sprintf("wrong encryption!");
	version.sprintf("%u",v);

	users.sprintf("%u",maxUser);

	this->leSerial->setText(serial);
	this->leID->setText(idString);
	this->leVersion->setText(version);
	this->le_MaxUser->setText(users);
#endif

}

void KeyLock::pBWriteReleased()
{
#ifdef WIN32	
	CHardwareDongle dongle;
	CEncryption e;

	unsigned int sn = dongle.readSerialNunmber();
	unsigned long id = e.encryptBaristaSerialNumber(sn);





	QString version( this->leVersion->text());

	bool ok;
	int v =	version.toInt(&ok);

	if (!ok)
	{
		this->leSerial->setText("Write error");
		return;
	}

	if (!dongle.writeUniqueID(id))
	{
		this->leSerial->setText("Write error");
		return;
	}

	if (!dongle.writeVersionNumber(v))
	{
		this->leSerial->setText("Write error");
		return;
	}
	
	this->pBReadReleased();
#endif 	
}

void KeyLock::pBCloseReleased()
{
	this->close();
}