#include "ObjectSortFilterModel.h"
#include "ObjectDataModel.h"


CObjectSortFilterModel::CObjectSortFilterModel(CObjectDataModel* dataModel):
	dataModel(dataModel)
{
	if (!dataModel)
		return;

	this->setSourceModel(this->dataModel);
}

CObjectSortFilterModel::~CObjectSortFilterModel(void)
{
	if (this->dataModel)
		delete this->dataModel;
	this->dataModel = NULL;
}




void CObjectSortFilterModel::rightMouseButtonReleased(const QModelIndexList &list,vector<QAction*>& actions)
{
	QModelIndexList mappedList;
	for (int i=0; i< list.count();i++)
		if (list.at(i).column() == this->getNameColumn()) // we only want a row ones
			mappedList.append(this->mapToSource(list.at(i)));

	this->dataModel->rightMouseButtonReleased(mappedList,actions);
}

CObsPoint* CObjectSortFilterModel::getObsPoint(const QModelIndex& modelIndex)
{
	if (modelIndex.isValid())
		return this->dataModel->getObsPoint(this->mapToSource(modelIndex).row());
	else
		return NULL;
}

GenericPolyLine* CObjectSortFilterModel::getPolyLine(const QModelIndex& modelIndex)
{ 
	if (modelIndex.isValid())
		return this->dataModel->getPolyLine(this->mapToSource(modelIndex).row());
	else
		return NULL;
}