/**
 * @class COpenBaristaViewList
 *
 * 
 * @author Franz Rottensteiner
 * @version 1.0 
 * @date 8-Feb-2007
 *
 */

#include "OpenBaristaViewList.h"
  
void COpenBaristaViewList::addView(CBaristaView *view, bool inGeoLinkMode)
{
	if (this->getIndexOfView(view) < 0) // if view is not alreday in the list
	{
		this->list.push_back(view); // add to the list
		if (inGeoLinkMode)
			view->activateGeoLinkMode();
		else
			view->deactivateGeoLinkMode();

		this->informListeners_elementAdded(); // inform all current listener that a view is added (in Parent class CUpdateHandler)

	}

	

};

void COpenBaristaViewList::deleteView(CBaristaView *view)
{
	int index = this->getIndexOfView(view);
	if (index >= 0) 
	{
		this->deleteView(index);
	}
};

int COpenBaristaViewList::getIndexOfView(CBaristaView *view)
{
	for (int i = 0; i < this->getNumberOfViews(); ++i)
	{
		if (this->list[i] == view) return i;
	}

	return -1;
};

void COpenBaristaViewList::deleteView(int index)
{
	for (int i = index + 1; i < this->getNumberOfViews(); ++i)
	{
		this->list[i - 1] = this->list[i];
	}

	this->list.pop_back();
	this->informListeners_elementDeleted();
};

void COpenBaristaViewList::activateGeoLinkMode()
{
	for (int i = 0; i < this->getNumberOfViews(); ++i)
	{
		this->list[i] ->activateGeoLinkMode();
	}	
}

void COpenBaristaViewList::deactivateGeoLinkMode()
{
	for (int i = 0; i < this->getNumberOfViews(); ++i)
	{
		this->list[i] ->deactivateGeoLinkMode();
	}
}