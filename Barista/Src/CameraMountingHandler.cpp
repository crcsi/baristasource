#include "CameraMountingHandler.h"

#include "Bui_PushbroomImportDlg.h"
#include "VImage.h"
#include <QMessageBox>
#include "Bui_MainWindow.h"
#include "BaristaProjectHelper.h"

#define FLOORLABEL "Floor Point"
#define ROOFLABEL "Roof Point"
#define DATASETNAME "MountingHandler"


CCameraMountingHandler::CCameraMountingHandler(): importDlg(0),image(0),floorPoint(-1,-1),
	roofPoint(-1,-1)
{ 
	this->statusBarText = "MEASURE VERTICAL POLE ";
}

CCameraMountingHandler::~CCameraMountingHandler()
{
	this->cleanUp();
}


void CCameraMountingHandler::open()
{
	CBaristaViewEventHandler::open();

	if (this->importDlg)
		delete this->importDlg;
	this->importDlg = NULL;

	if (!this->image)
		return;

	this->roofPoint.x = -1;
	this->roofPoint.y = -1;
	this->floorPoint.x = -1;
	this->floorPoint.y = -1;

	this->importDlg = new Bui_PushbroomImportDlg(this->image,this->parent);
	this->importDlg->addListener(this);

	this->importDlg->setModal(true);
	this->importDlg->show();
}

void CCameraMountingHandler::close()
{
	if (this->importDlg && this->importDlg->getSuccess() && this->image)
	{
		CFileName filename = "PushBroomScanner";

		if (!this->image->initGenericPushBroomScanner(filename,
										(CCCDLine*)this->importDlg->getCamera(),
										this->importDlg->getSatelliteHeight(),
										this->importDlg->getGroundHeight(),
										this->importDlg->getGroundTemperature(),
										this->importDlg->getInTrackViewAngle(),
										this->importDlg->getCrossTrackViewAngle(),
										this->importDlg->getUL(),
										this->importDlg->getUR(),
										this->importDlg->getLL(),
										this->importDlg->getLR()))
		{
			QMessageBox::warning( this->parent, "No Orbit imported!", "Could not initialize the Pushbroom Scanner with the read data!");
		}
		else
		{
			CBaristaProjectHelper helper;
			helper.refreshTree();
		}

	}

	for (int i=0; i < this->baristaViews.getNumberOfViews(); i++)
	{
		CBaristaView* bview = this->baristaViews.getView(i);

		if (bview->getVImage() && bview->getVImage() == this->image)
		{
			CLabel ds(DATASETNAME);
			bview->removeDrawingDataSet(ds);
		}
	}


	this->cleanUp();
}

void CCameraMountingHandler::cleanUp()
{
	if (this->importDlg)
	{
		this->importDlg->removeListener(this);
		this->importDlg->close();
		delete this->importDlg;
	}
	this->importDlg = NULL;

	if (this->image)
		this->image->removeListener(this);

	this->image = NULL;

	this->roofPoint.x = -1;
	this->roofPoint.y = -1;
	this->floorPoint.x = -1;
	this->floorPoint.y = -1;
}


bool CCameraMountingHandler::keyReleaseEvent(QKeyEvent *e, CBaristaView *v)
{
	CBaristaViewEventHandler::keyReleaseEvent(e,v);
	

	if ( e->key() == Qt::Key_Escape || e->key() == Qt::Key_Delete || e->key() == Qt::Key_Backspace)
	{
		// roof point already measured
		if (this->roofPoint.x > -1.0)
		{
			CLabel ds(DATASETNAME);
			CLabel p(ROOFLABEL);
			v->removePointDrawing(ds,p);
			this->roofPoint.x = -1;
			this->roofPoint.y = -1;
		}
		else if (this->floorPoint.x > -1.0)
		{
			CLabel ds(DATASETNAME);
			CLabel p(FLOORLABEL);
			v->removePointDrawing(ds,p);
			this->floorPoint.x = -1;
			this->floorPoint.y = -1;
		}
	}

	v->updateStatusBarInfo();

	return true;
}

bool CCameraMountingHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
  
	CHugeImage* himage = v->getHugeImage();
	
	if (!himage || !this->image || this->image->getHugeImage() != himage )
		return false;


	if (v->getVImage())
	{	
		CXYPoint tmp;

		v->getMouseEventCoords(e, tmp.x, tmp.y);
	    
		// no points to be measured outside image range
		if (tmp.getX() < -0.5 || tmp.getX() > himage->getWidth() || 
			tmp.getY() < -0.5 || tmp.getY() > himage->getHeight() )
			return false;
		
		if (this->floorPoint.x == -1.0)
		{
			this->floorPoint = tmp;

			CLabel ds(DATASETNAME);
			CLabel l(FLOORLABEL);
			tmp.setLabel(l);
			v->addPointDrawing(ds,&tmp);
		}
		else if (this->roofPoint.x == -1.0)
		{
			this->roofPoint = tmp;
			CLabel ds(DATASETNAME);
			CLabel l(ROOFLABEL);
			tmp.setLabel(l);
			v->addPointDrawing(ds,&tmp);

			// compute the mounting angles
			this->computeViewingAngle();

		}
		else
		{
			// ignore click
		}
	}

	return true;
}




bool CCameraMountingHandler::setVImage(CVImage* vimage)
{
	if (this->image)
		return false;

	this->image = vimage;
	this->image->addListener(this);

	return true;
}

void CCameraMountingHandler::elementDeleted(CLabel element)
{
	bui_MainWindow* mw = (bui_MainWindow*)this->getParent();
	mw->endCameraMountingHandler();
}

void CCameraMountingHandler::computeViewingAngle()
{
	if (this->floorPoint.x == -1 || this->floorPoint.y == -1 || 
		this->roofPoint.x == -1 || this->roofPoint.y == -1)
		return;

	if (!this->importDlg || !this->image)
		return;

	double epsilon = 0.000001;
	double di = 1000;
	double dc = 1000;
	
	// first determine the signs of the look angles
	double dx = roofPoint.x - floorPoint.x;
	double dy = roofPoint.y - floorPoint.y;

	C2DPoint fp(this->floorPoint.y,this->floorPoint.y,0.0);
	C2DPoint rp(this->roofPoint.x,this->roofPoint.y,0.0);
	double signInTrack = dy < 0.0? -1.0 : 1.0;
	double signCrossTrack = dx < 0.0? 1.0 : -1.0;

	double dydx = (dy)/(dx);

	double offNadir = this->importDlg->getOffNadirAngle() * M_PI/ 180.0;
	double inTrack= signInTrack * offNadir/sqrt(2.0);
	double crossTrack= signCrossTrack * offNadir/sqrt(2.0);
	offNadir = tan(offNadir);
	offNadir *= offNadir;

	bool success = false;

	if (fabs(dx) < 0.5)
	{
		inTrack = signInTrack * offNadir;
		crossTrack = 0.0;
		success = true;
	}
	else if (fabs(dy) < 0.5)
	{
		crossTrack = signCrossTrack * offNadir;
		inTrack = 0.0;
		success = true;
	}
	else
	{
		C3DPoint irp = this->importDlg->getIRP();
		C2DPoint pp0(fp.x - irp.x, fp.y - irp.y);
		pp0 /= irp.z;
		
		for (int k=0; k < 2; k++)
		{
			di = 1000;
			dc = 1000;
			success = false;
			for (int i=0; i < 10 && (fabs(di) > epsilon || fabs(dc) > epsilon) ;i++)
			{
				double f0,g0,dfdi,dfdc,dgdi,dgdc;
				this->evalF(inTrack,crossTrack,pp0.y,pp0.x,f0,dfdi,dfdc);
				this->evalG(inTrack,crossTrack,g0,dgdi,dgdc);
				g0 = offNadir - g0;
				f0 = dydx - f0;
				double det = 1.0 / (dfdi * dgdc - dfdc * dgdi);
				double a11 =  dgdc * det, a12 = -dfdc * det;
				double a21 = -dgdi * det, a22 =  dfdi * det;
			
				di = a11 * f0 + a12 * g0;
				dc = a21 * f0 + a22 * g0;

				inTrack += di;
				crossTrack += dc;
			}

			if ((fabs(di) < epsilon) && (fabs(dc) < epsilon))
				success = true;
			else
				break;

			dydx /= cos(inTrack);
		
		}

	}

	if (success)
	{
		this->importDlg->setViewingAngles(inTrack,crossTrack);
	}
	else
	{
		QMessageBox::warning( this->parent, "Error", "Determination of looking angles failed!");
	}
	return;
}



bool CCameraMountingHandler::evalF(double i,double c,double a,double b,double& value,double& dfdi,double& dfdc) const
{
	double cos_C2 = cos(c)*cos(c);

	double cos_I = cos(i);
	double sin_I = sin(i);
	double tan_C = tan(c);

	value = (sin_I - a*cos_I)/(-tan_C -b*cos_I); 
	double dev = (b*cos_I + tan_C);
	dev *=dev;
	dfdi = (-b-cos_I*tan_C-a*sin_I*tan_C)/dev;
	dfdc = ((sin_I-a*cos_I)/cos_C2)/dev;

	return true;
}

bool CCameraMountingHandler::evalG(double i,double c,double& value,double& dgdi,double& dgdc) const
{
	value = tan(i)*tan(i)+tan(c)*tan(c);
	dgdi = 2 * tan(i)/(cos(i)*cos(i));
	dgdc = 2 * tan(c)/(cos(c)*cos(c));

	return true;
}

