#include "MeasureHandler.h"
#include "Bui_MainWindow.h"
#include "Label.h"
#include "Bui_LabelChangeDlg.h"
#include <QMessageBox>
#include "Bui_ProtocolViewer.h"

#include "SystemUtilities.h"


CMeasureHandler::CMeasureHandler(void): firstXYZ(NULL), secondXYZ(NULL), protocolstream(NULL), protocolViewer(NULL)
{
	this->statusBarText = "MEASUREMODE ";
}

CMeasureHandler::~CMeasureHandler(void)
{
	if (this->protocolViewer)
	{
		this->protocolViewer->close();
		this->protocolViewer = NULL;
	}
	this->closeProtocol();

	if (this->firstXYZ )
		delete this->firstXYZ;
	this->firstXYZ = NULL;

	if (this->secondXYZ )
		delete this->secondXYZ;
	this->secondXYZ = NULL;
}

bool CMeasureHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	CDigitizingHandler::handleLeftMouseReleaseEvent(e,v);

	if ( this->geoXYZ )
	{
		if ( !this->firstXYZ)
		{
			this->firstXYZ = new CXYZPoint(this->geoXYZ);
		}
		else
		{
			this->secondXYZ =  new CXYZPoint(this->geoXYZ);

			this->printResults();
			this->viewProtocol();

			this->firstXYZ = NULL;
			this->secondXYZ = NULL;
		}
		return true;
	}
	else
	{
		if ( v->getVImage() )
		{
			QMessageBox::warning( this->parent, "No georeferenced point was determined!"
				, "Measure mode requires TFW parameters for image.");
		}
	}

	return false;
};

bool CMeasureHandler::keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
{
	if ( e->key() == Qt::Key_Delete || e->key() == Qt::Key_Backspace)
	{
		if ( this->firstXYZ ) 
			this->firstXYZ = NULL;
	}
	
	return CDigitizingHandler::keyReleaseEvent (e, v);
}


void CMeasureHandler::open()
{
	CBaristaViewEventHandler::open();
	
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->MeasureAction->setChecked(true);
	mainwindow->blockSignals(false);

	if (this->firstXYZ )
		delete this->firstXYZ;
	this->firstXYZ = NULL;

	if (this->secondXYZ )
		delete this->secondXYZ;
	this->secondXYZ = NULL;

	// make new protocol file or do append
	this->protocolFile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "measure.prt";

	this->viewProtocol();




};

void CMeasureHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->MeasureAction->setChecked(false);
	mainwindow->blockSignals(false);

	if ( this->protocolViewer )
	{
		this->protocolViewer->removeListener(this);
		this->protocolViewer->close();
		this->protocolViewer = NULL;
	}
	this->closeProtocol();

	if (this->firstXYZ )
		delete this->firstXYZ;
	this->firstXYZ = NULL;

	if (this->secondXYZ )
		delete this->secondXYZ;
	this->secondXYZ = NULL;
};

void CMeasureHandler::viewProtocol()
{
	if ( !this->protocolViewer )
	{
		this->initProtocol(this->protocolFile.GetChar());

		this->protocolViewer = new Bui_ProtocolViewer(this->protocolFile, this->parent);
		this->protocolViewer->setWindowTitle("Coordinate Differences");
		this->protocolViewer->addListener(this);

		this->protocolViewer->move(100,50);
		this->protocolViewer->resize(800, 150);
		this->protocolViewer->show();
	}
	else
	{
		this->protocolViewer->reloadProtocolFile();
	}
}

void CMeasureHandler::initProtocol(const char *filename)
{
	if (!this->protocolstream)
	{
		this->protocolstream = new ofstream(filename);
		if (!protocolstream->good()) 
			this->closeProtocol();
	}
}

void CMeasureHandler::closeProtocol()
{
	if (this->protocolstream) 
	{
		this->protocolstream->close();
		delete this->protocolstream;
		this->protocolstream = 0;	
	}

}

void CMeasureHandler::printResults()
{
	if (!this->protocolstream)
		this->initProtocol(this->protocolFile.GetChar());

	if (this->protocolstream)
	{
		int widthDIFF = 12;
		int widthCOORD = 12;
		int widthLABEL = 10;
		int digits = 3;
		
		protocolstream->setf(ios::fixed, ios::floatfield);
		protocolstream->precision(digits);

		protocolstream->width(widthLABEL);
		*protocolstream << this->firstXYZ->getLabel().GetChar();
		protocolstream->width(widthCOORD);
		*protocolstream << this->firstXYZ->x;
		protocolstream->width(widthCOORD);
		*protocolstream << this->firstXYZ->y;


		protocolstream->width(widthLABEL);
		*protocolstream << this->secondXYZ->getLabel().GetChar();
		protocolstream->width(widthCOORD);
		*protocolstream << this->secondXYZ->x;
		protocolstream->width(widthCOORD);
		*protocolstream << this->secondXYZ->y;

		double dx = this->firstXYZ->x - this->secondXYZ->x;
		double dy = this->firstXYZ->y - this->secondXYZ->y;

		double distance = sqrt(dx*dx + dy*dy);

		*protocolstream << "  dx: "; 
		protocolstream->width(widthDIFF);
		*protocolstream	<< dx;
		*protocolstream << "  dy: "; 
		protocolstream->width(widthDIFF);
		*protocolstream << dy;


		*protocolstream << "  dist: "; 
		protocolstream->width(widthDIFF);
		*protocolstream << distance;

		*protocolstream  << endl;
	}
};

void CMeasureHandler::elementDeleted(CLabel element)
{
	this->protocolViewer= NULL;
}

void CMeasureHandler::elementsChanged(CLabel element)
{
	this->closeProtocol();
	this->protocolFile = this->protocolViewer->getFileName();

	this->protocolstream = new ofstream(this->protocolFile.GetChar(), ios_base::app);
	if (!protocolstream->good())
		this->closeProtocol();
}