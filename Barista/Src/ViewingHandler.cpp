#include "ViewingHandler.h"
#include "Bui_MainWindow.h"
#include "ImageView.h"

CViewingHandler::CViewingHandler()
{
	this->statusBarText = "VIEWMODE ";
}

CViewingHandler::~CViewingHandler()
{
}


bool CViewingHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{	
	return CBaristaViewEventHandler::handleMouseMoveEvent(e, v);
};


bool CViewingHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	C2DPoint xy;
	if (!v->getMouseEventCoords(e, xy.x, xy.y))
		return false;
	
	if ( v->firstClick.x == -1 ) v->firstClick = xy;
	
	v->showROI = true;
	v->hasROI = false;
	
	return true;
};

bool CViewingHandler::handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{

	// populate popupmenu depending what's needed
	if(v->getHasROI())
	{	
		QAction* CropROIAction = new QAction("&Crop ROI", v);
		v->connect(CropROIAction, SIGNAL(triggered()), v, SLOT(CropROI()));
		v->addPopUpAction(CropROIAction, true);

		if (v->getVImage())
		{
			QAction* OpenFeaturesDialogAction = new QAction("&Extract Features", v);
			v->connect(OpenFeaturesDialogAction, SIGNAL(triggered()), v, SLOT(OpenFeaturesDialog()));

//			QAction* enhanceAreaROIAction = new QAction("&Enhance Area", v);
//			v->connect(enhanceAreaROIAction, SIGNAL(triggered()), v, SLOT(enhanceAreaROI()));

			QAction* OrthofromROIAction = 0;
			if (v->getVImage()->getCurrentSensorModel() && (project.getNumberOfDEMs() > 0))
			{
				OrthofromROIAction = new QAction("&Generate Orthoimage from ROI", v);
				v->connect(OrthofromROIAction, SIGNAL(triggered()), v, SLOT(OrthofromROI()));
			}

			QAction* fitEllipsePointsROIAction = new QAction("&Ellipse Fitting", v);
			v->connect(fitEllipsePointsROIAction, SIGNAL(triggered()), v, SLOT(fitEllipsePointsROI()));
			
			//QAction* fitSnakeAction = new QAction("&Snake Calculation", v);
			//v->connect(fitSnakeAction, SIGNAL(triggered()), v, SLOT(openSnakeDialog()));
			
			QAction* deletePointsROIAction= new QAction("&Delete 2D Points", v);
			v->connect(deletePointsROIAction, SIGNAL(triggered()), v, SLOT(deletePointsROI()));	

			v->addContextMenuSeparator();
			v->addPopUpAction(OpenFeaturesDialogAction);
	//		v->addPopUpAction(enhanceAreaROIAction,true);
			if (OrthofromROIAction) v->addPopUpAction(OrthofromROIAction, true);
			v->addPopUpAction(fitEllipsePointsROIAction, true);

			//v->addPopUpAction(fitSnakeAction, true);
		//	v->addPopUpAction(fitSnakeAction);
			
			v->addPopUpAction(deletePointsROIAction);
		}
		else if (v->getVDEM())
		{
			QAction* SetDEMRoiTo9999Action = new QAction("&Set heights in ROI to -9999", v);
			v->connect(SetDEMRoiTo9999Action, SIGNAL(triggered()), v, SLOT(ResetRoi()));
			v->addPopUpAction(SetDEMRoiTo9999Action);
		}
	
	}

	return true;
};

bool CViewingHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{	
	C2DPoint xy;
	if (!v->getMouseEventCoords(e, xy.x, xy.y))
	{
		if ( v->getHugeImage() )
		{
			int width = v->getHugeImage()->getWidth();
			int height = v->getHugeImage()->getHeight();

			if ( xy.x < 0 ) xy.x = 0;
			if ( xy.x > width ) xy.x = width;
			if ( xy.y < 0 ) xy.y = 0;
			if ( xy.y > height ) xy.y = height;
		}
		else 
		{
			v->resetFirstSecondClick();
			return false;
		}
	}
	
	if ( v->firstClick.x != -1 ) 
	{
		v->secondClick = xy;
		v->selectROI();

	}
	v->showROI = false;
	return true;
};



bool CViewingHandler::redraw(CBaristaView *v)
{
	return true;
};

void CViewingHandler::initialize(CBaristaView *v)
{
};


void CViewingHandler::close()
{
	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i < n; i++)
	{
		CBaristaView* v = this->baristaViews.getView(i);
		v->setShowROI(false);
		v->setHasROI(false);
		v->update();
	}
};

