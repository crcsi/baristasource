#include "PolyLineDataModel.h"
#include <QMessageBox>
#include <QAction>
#include "Bui_FeatureExtractionDialog.h"
#include "BaristaProjectHelper.h"
#include "BaristaProject.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"

CPolyLineDataModel::CPolyLineDataModel(CGenericPolyLineArray* polyLineArray,ViewParameter viewParameter):
	CObjectDataModel(viewParameter),polyLineArray(polyLineArray)
{
	this->polyLineArray->addListener(this);
	this->dataModelType = ePolyLineModel;

	this->setHeader(this->guessHeader());
	this->setFormatString(this->guessDecimals());	

}

CPolyLineDataModel::~CPolyLineDataModel(void)
{
	this->polyLineArray->removeListener(this);
}


void CPolyLineDataModel::elementsChanged(CLabel element)
{
	if (!this->listenToSignal)
		return;

	
	this->beginResetModel();
	vector<QString> param;
	param.push_back(this->titlePrefix);
	param.push_back(this->polyLineArray->getFileName()->GetFullFileName().GetChar());
	param.push_back(this->polyLineArray->getFileName()->GetChar());
	emit  dataModelChanged(eObjectName,param);	
	this->endResetModel(); // force the view to update
}

int CPolyLineDataModel::rowCount(const QModelIndex& parent) const
{

	return this->polyLineArray->GetSize();
}

int CPolyLineDataModel::columnCount(const QModelIndex& parent) const
{

	return 4;
}

QVariant CPolyLineDataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();	

	if (role == Qt::TextAlignmentRole)
	{
		if (index.column() == this->getNameColumn())
			return int(Qt::AlignRight | Qt::AlignVCenter);			
		else
			return int(Qt::AlignCenter | Qt::AlignVCenter);
	}
	else if ( role == Qt::DisplayRole || 
			  role == Qt::EditRole || 
			  role == SortingRole || 
			  role == Qt::ToolTipRole )
	{
		
		GenericPolyLine* pl = this->polyLineArray->GetAt(index.row());
		if (index.column() ==  this->getNameColumn()) // the label
		{
			return QString().sprintf(this->formatList.at(0).toLatin1().constData(),pl->getLabel().GetChar());
		}
		else if (index.column() == 1) // number of points
		{
			if (role == SortingRole)
				return pl->getPointCount();
			else
				return QString().sprintf(this->formatList.at(1).toLatin1().constData(),pl->getPointCount());
		}
		else if (index.column() == 2) // the length
		{
			if (role == SortingRole)
				return pl->getLength();
			else
				return QString().sprintf(this->formatList.at(2).toLatin1().constData(),pl->getLength());
		}
		else if (index.column() == 3) // open /closed
		{
			return pl->isClosed() ? QString("closed") : QString("open");
		}

	}
	else if (role == Qt::BackgroundRole)
	{
		return QColor(Qt::white);
	}
		
	return QVariant();
}

QVariant CPolyLineDataModel::headerData(int section,Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal && section < this->headerList.count())
	{
		return this->headerList.at(section);
	}
	

	return QAbstractTableModel::headerData(section,orientation,role);
}

Qt::ItemFlags CPolyLineDataModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return false;

	if (index.column() == this->getNameColumn() && this->vp.allowLabelChange)
		return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
	else
		return QAbstractItemModel::flags(index);
}

bool CPolyLineDataModel::setData(const QModelIndex &index, const QVariant &value,int role )
{
	if (!index.isValid())
		return false;
	
	if (role == Qt::EditRole)
	{
		
		if (this->vp.allowLabelChange && index.column() == this->getNameColumn())
		{
			CLabel clabel = value.toString().toLatin1().constData();

			// if label is empty or alreday exists keep old label
			GenericPolyLine* existingLine = this->polyLineArray->getLine(clabel.GetChar());
			GenericPolyLine* thisLine = this->polyLineArray->getLine(index.row());
			if (clabel.GetLength() == 0 || (existingLine && existingLine != thisLine))
			{
				QMessageBox::warning( 0, "Change Label not possible!"
				, "This label already exist. Choose different Label!");
			}
			// if label is not empty and not existing yet accept new label
			else
			{
				this->polyLineArray->GetAt(index.row())->setLabel(clabel);
				emit dataChanged(index,index);
				return true;
			}
		}

	}


	return false;

	return false;
}


bool CPolyLineDataModel::removeRows (int row,int count,const QModelIndex & parent)
{
    beginRemoveRows(QModelIndex(), row, row+count-1);
	this->listenToSignal = false;

	this->polyLineArray->RemoveAt(row,count);

	this->listenToSignal = true;
    endRemoveRows();
    return true;
}



QStringList CPolyLineDataModel::guessHeader()
{
	QStringList localHeaderList;

	if (!this->polyLineArray)
		return localHeaderList;
	
	
	CCharString label("Label");

	if (this->polyLineArray->getReferenceSystem()->getCoordinateType() != eGRID)
	{
		localHeaderList << label.GetChar();
	}
	else 
	{
		if (this->polyLineArray->getReferenceSystem()->isUTM())
		{
			label += "(Zone ";

			CLabel p2;
			p2 += this->polyLineArray->getReferenceSystem()->getZone();

			label = label + p2.GetChar();

			if ( this->polyLineArray->getReferenceSystem()->getHemisphere() == SOUTHERN )
			{
				label = label + ", Southern)";   
			}
			else if ( this->polyLineArray->getReferenceSystem()->getHemisphere() == NORTHERN )
			{
				label = label + ", Northern)";
			}
		}
		else 
		{
			label += "(Lat:";
			double lon = this->polyLineArray->getReferenceSystem()->getCentralMeridian();
			double lat = this->polyLineArray->getReferenceSystem()->getLatitudeOrigin();

			char buf[256];
			sprintf(buf, "%5.2f Lon:%5.2f)", lat, lon); 

			label = label + buf;
		}

		localHeaderList << label.GetChar();

	}


	localHeaderList << "Number of Points";
	localHeaderList << "Height/Length";
	localHeaderList << "Open/Closed";



	return localHeaderList;
}


QStringList CPolyLineDataModel::guessDecimals()
{
	QStringList localFormatList;
	
	if (!this->polyLineArray)
		return localFormatList;
	
	
	
	localFormatList.append("%s");	// the label
	localFormatList.append("%d");	// number of points
	
	if (this->polyLineArray->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)
		localFormatList.append("%.10lf");
	else if (this->polyLineArray->getReferenceSystem()->getCoordinateType() == eGEOCENTRIC)
		localFormatList.append("%.4lf");
	else if (this->polyLineArray->getReferenceSystem()->getCoordinateType() == eGRID)
		localFormatList.append("%.4lf");
	else
		localFormatList.append("%.3lf");

	return localFormatList;

}


void CPolyLineDataModel::removefromTable()
{

	for ( int i =this->list.size() -1; i >=0; i--)
	{
		this->removeRows(this->list.at(i).row(),1);
	}  
}


GenericPolyLine* CPolyLineDataModel::getPolyLine(int index)
{
	return this->polyLineArray->GetAt(index);
}

void CPolyLineDataModel::requestWindowTitle()
{
	this->elementsChanged();
}

int CPolyLineDataModel::getDimension() const
{
	if (this->polyLineArray->getClassName().compare("CXYZPolyLineArray") == 0)
		return 3;
	else
		return 0;
}

void CPolyLineDataModel::XYZPolyLinesSnakeAction()
{
	CXYZPolyLine *poly = (CXYZPolyLine *) this->polyLineArray->GetAt(this->list.at(0).row());

	if (poly) 
	{
		unsigned int numberOfPoints = poly->getPointCount();

		//get snake points
		C2DPoint snakeRoiUL(poly->getPoint(numberOfPoints)->getY(),  poly->getPoint(numberOfPoints)->getX());
		C2DPoint snakeRoiLR(0.0f, 0.0f);

		//int numberOfPoints = this->line.getPointCount();
		CXYPointArray *points = new CXYPointArray();

		for (unsigned int i=0 ; i < numberOfPoints; i++) 
		{
/*			CXYPoint* p = this->current2DPoints.getAt(i);
			points->add(p);
				
			if (p->y > snakeRoiLR.y) snakeRoiLR.y = p->y;
			if (p->x > snakeRoiLR.x) snakeRoiLR.x = p->x;
			if (p->y < snakeRoiUL.y) snakeRoiUL.y = p->y;
			if (p->x < snakeRoiUL.x) snakeRoiUL.x = p->x;*/
		}

		if (poly->isClosed()) //close contour
		{
		}
		else //open contour
		{
		}

		//GenericPolyLine* pl = this->polyLineArray->GetAt(index.row());
		//pl->isClosed()


		//if (v->getVBase() != this->currentVBase || this->line.getPointCount() < 2)
		//		return false;


		//if open
		//if close

		/* ////////////////////
		if ( e->key() == Qt::Key_F  || e->key() == Qt::Key_C)
		{
			if (v->getVBase() != this->currentVBase || this->line.getPointCount() < 2)
				return false;

			//get snake points and calculate ROI
			C2DPoint snakeRoiUL(v->getHugeImage()->getHeight(), v->getHugeImage()->getWidth());
			C2DPoint snakeRoiLR(0.0f, 0.0f);

			int numberOfPoints = this->line.getPointCount();
			CXYPointArray *points = new CXYPointArray();

			for (int i=0 ; i < numberOfPoints; i++) 
			{
				CXYPoint* p = this->current2DPoints.getAt(i);
				points->add(p);
				
				if (p->y > snakeRoiLR.y) snakeRoiLR.y = p->y;
				if (p->x > snakeRoiLR.x) snakeRoiLR.x = p->x;
				if (p->y < snakeRoiUL.y) snakeRoiUL.y = p->y;
				if (p->x < snakeRoiUL.x) snakeRoiUL.x = p->x;
			}
			
			//define ROI
			v->setROILR(snakeRoiLR); 
			v->setROIUL(snakeRoiUL);

			//if not too big, fill ROI with image data
			if( snakeRoiLR.x - snakeRoiUL.x > 4000 || snakeRoiLR.y - snakeRoiUL.y > 4000)
			{
				v->setHasLargeROI(true);
			}
			else
			{
				v->setHasLargeROI(false);
			}

				if (v->getHugeImage())
					v->getHugeImage()->setRoi(snakeRoiUL.x,snakeRoiUL.y, snakeRoiLR.x-snakeRoiUL.x, snakeRoiLR.y-snakeRoiUL.y);
					//v->setHasLargeROI(false);

					v->setShowROI(false);
					v->setHasROI(true);
			//}
		
			//close/ open line
			bool openSnake = true;

			if ( e->key() == Qt::Key_C ) //close line
			{
				if (this->line.getPointCount())
				{
					this->closeDrawings(this->currentLineLabel);
				}
				
				this->line.closeLine();
				openSnake = false;
			}

			// has the user has changed the label while drawing the line ?
			if (this->currentLineLabel == this->changedLineLabel)
			{
			}
			else // yes
			{
				// rename the 3D polyline
				this->line.setLabel(this->changedLineLabel);
				
				// rename the dataset because we don't draw the line again
				v->renameDataSet(this->currentLineLabel,this->changedLineLabel); 
			
				// rename the points of the 2D/3D polyline
				
				// build the point label
				CCharString lineString (this->changedLineLabel.GetChar());
				CCharString pointString(lineString + "_P");

				// add to 2D monoplotted points of the image
				for (int i=0 ; i < this->current2DPoints.GetSize(); i++)
				{
					CXYPoint* p = this->current2DPoints.getAt(i);
					this->currentPointLabel = pointString.GetChar();
					this->currentPointLabel += i;
					p->setLabel(this->currentPointLabel); // 2d
					this->line.getPoint(i)->setLabel(this->currentPointLabel); //3d
				}

				// ok the label is changed
				this->currentLineLabel = this->changedLineLabel;

			}

			CLabel LineLabel1 = this->currentLineLabel;

			//monoplotting is finished
			this->initPolyline();
			v->resetFirstSecondClick();			

			//calculation of snake
			bool* SnakeCalculationOK = new bool;
			*SnakeCalculationOK = false;

			CFileName fn("snake.ini");
			fn.SetPath(project.getFileName()->GetPath());

			Bui_SnakeDlg dlg(v, fn, v->getVImage(), openSnake, points, SnakeCalculationOK); 
			dlg.show();
			dlg.exec();	

			//clean up
			v->unselectROI();
			this->removeDrawingDataSet(LineLabel1);
			this->current2DPoints.RemoveAll();
			this->line.deleteLine();

			if (*SnakeCalculationOK)
			{
				//prepare to draw snake point as monoplotted line
				this->currentVBase = v->getVBase();
				this->currentLineLabel.Set(LineLabel1);				//change back to old label
				this->addDrawingDataSets(this->currentLineLabel);	//add the datasets in all open image views
				
				this->line.setLabel(this->currentLineLabel);		//rename the 3D polyline

				// don't show the selected points labels in the current image view
				v->setShowPoints(this->currentLineLabel,true);
				v->setShowPointLabels(this->currentLineLabel,false);

				// build the point label
				CCharString lineString (this->currentLineLabel.GetChar());
				CCharString pointString(lineString + "_P");
				this->currentPointLabel = pointString.GetChar();

				//prepare for loop to draws the points
				int size = points->GetSize();
				CXYPoint p;
				CXYZPoint linepoint;
					
				// add to 2D monoplotted points of the image
				for(int i = 0; i < size - 1;  i++) 
				{
					this->currentPointLabel += i;							//increase the point label
					p = points->getAt(i);									//get a point (which has no label)
					p.setLabel(this->currentPointLabel);					//change label of the point
		
					this->current2DPoints.Add(p);							//add 2d point
					this->monoPlotter.getPoint(linepoint, p);				//calculate 3d point
					this->line.addPoint(linepoint);							//add 3d point
					
					this->addToDrawings(this->currentLineLabel, linepoint);	//add to drawing datasets
				}

				//close line
				if(openSnake)this->line.openLine();
				else //				
				{
					if (this->line.getPointCount()) this->closeDrawings(this->currentLineLabel);

					this->line.closeLine();
				}
				
				// add to project
				if (project.addMonoplottedLine(this->line))
					baristaProjectHelper.refreshTree();

				// clean up
				this->initPolyline();
				v->resetFirstSecondClick();
			}
			else
			{
				QMessageBox::critical( 0, "Barista"," Error during the calculation");
				return false;
			}
		}
		else if ( e->key() == Qt::Key_Escape)
		{
			if (v->getVBase() != this->currentVBase)
				return false;

			this->removeDrawingDataSet(this->currentLineLabel);
			v->resetFirstSecondClick();
			this->initPolyline();
		}
		else if (e->key() == Qt::Key_Backspace)
		{
			if (v->getVBase() != this->currentVBase)
				return false;

			if (this->current2DPoints.GetSize() && this->line.getPointCount())
			{
				// reset the first click 
				if (this->current2DPoints.GetSize() > 1)
				{
					CObsPoint* p =this->current2DPoints.GetAt(this->current2DPoints.GetSize()-2);
					C2DPoint lastClickedPoint((*p)[0],(*p)[1]);			
					v->setFirstClick(lastClickedPoint);
				}
				else
					v->setFirstClick(C2DPoint(-1,-1));

				this->current2DPoints.RemoveAt(this->current2DPoints.GetSize()-1,1);
				this->removePointFromDataSet(this->currentLineLabel,this->line.getPoint(this->line.getPointCount()-1)->getLabel());
				this->line.removePoint(this->line.getPointCount()-1);
			}
		}

		else if ( e->key() == Qt::Key_L )
		{
			//change current line label
			if (this->currentLineLabel.GetLength() == 0 )
			{
				this->currentLineLabel = project.getCurrentLineLabel();
			}

			bui_LabelChangeDlg labeldlg(this->currentLineLabel.GetChar(), "New Line Label");

			labeldlg.setModal(true);
			labeldlg.exec();

			CLabel newlabel = labeldlg.getLabelText().GetChar();

			if ( newlabel.GetLength() == 0 )
			{
				labeldlg.setLabelText(this->currentLineLabel.GetChar());
				labeldlg.exec();
			}
			else
			{
				this->changedLineLabel = newlabel;
				project.setCurrentLineLabel(newlabel );
			}

			labeldlg.close();
		}
		*///////////

	}

}

void CPolyLineDataModel::XYZPolyLinesExtractFeatures()
{

	CXYZPolyLine *poly = (CXYZPolyLine *) this->polyLineArray->GetAt(this->list.at(0).row());

	if (poly) 
	{	
		CCharString dir = protHandler.getDefaultDirectory();

		CFileName fn("FEX.ini");
		fn.SetPath(project.getFileName()->GetPath());

		CCharString curDir = dir + "poly_0";
		CSystemUtilities::mkdir(curDir);
		protHandler.setDefaultDirectory(curDir);

		bui_FeatureExtractionDialog dlg(0, poly, fn);
		dlg.exec();
		if (dlg.parametersAccepted())
		{
			for (int i = 1; i < this->list.size(); ++i)
			{
				ostrstream oss;
				oss << "poly_" << i << ends;
				char *z = oss.str();
				curDir = dir + z;
				CSystemUtilities::mkdir(curDir);
				protHandler.setDefaultDirectory(curDir);
				delete [] z;

				poly = (CXYZPolyLine *) this->polyLineArray->GetAt(this->list.at(i).row());
				dlg.setBoundaryPoly(poly);
				dlg.OK();
			}

			baristaProjectHelper.refreshTree();
		}

		protHandler.setDefaultDirectory(dir);
	}
}

void CPolyLineDataModel::rightMouseButtonReleased(const QModelIndexList& List,vector<QAction*>& actions)
{

	// we need to order the list for deleting
	this->list.clear();
	for (int i=0; i< List.size(); i++)
	{
		bool inserted = false;
		for (int k=0; k< this->list.count(); k++)
		{	
			if (List.at(i).row() <= this->list.at(k).row())
			{
				this->list.insert(k,List.at(i));
				inserted = true;
				break;
			}
		}
		
		if (!inserted)
			this->list.append(List.at(i));

	}

	if ((project.getNumberOfImages() > 0) && this->polyLineArray->getDimension() == 3)
	{
		QAction *XYZPolyLinesExtractFeaturesAction = new QAction(tr("&Extract Features"), this);
		connect(XYZPolyLinesExtractFeaturesAction, SIGNAL(triggered()), this, SLOT(XYZPolyLinesExtractFeatures()));
		actions.push_back(XYZPolyLinesExtractFeaturesAction);

		/*QAction *XYZPolyLinesSnakeAction = new QAction(tr("&Snake Optimization"), this);
		connect(XYZPolyLinesSnakeAction, SIGNAL(triggered()), this, SLOT(XYZPolyLinesSnakeAction()));
		actions.push_back(XYZPolyLinesSnakeAction);*/
	}

	if (this->vp.allowDeletePoint && this->list.size())
	{
		QAction *deleteAction = NULL;

		deleteAction = new QAction(tr("&Delete Line(s)"), this);
		connect(deleteAction, SIGNAL(triggered()), this, SLOT(removefromTable()));

		if (deleteAction)      actions.push_back(deleteAction);
	}
	

	CObjectDataModel::rightMouseButtonReleased(this->list,actions);
}