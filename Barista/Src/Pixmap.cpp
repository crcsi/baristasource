#include <QImage>
#include "Pixmap.h"
#include "QTImageHelper.h"
#include <QPainter>

CPixmap::CPixmap(void)
{
    this->x = 0;
    this->y = 0;
}

CPixmap::~CPixmap(void)
{
}

void CPixmap::draw(QPainter* dc)
{
    dc->drawPixmap(this->x, this->y, *this);
}

void CPixmap::draw(QPainter* dc, int x, int y)
{
    dc->drawPixmap(this->x + x, this->y + y, *this);
}
