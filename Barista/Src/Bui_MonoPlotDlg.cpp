#include "Bui_MonoPlotDlg.h"
#include "ReferenceSystem.h"
#include "XYZPoint.h"
#include "IniFile.h"
#include "Icons.h"
#include "Bui_TMPropertiesDlg.h"
#include "BaristaProject.h"
#include "Bui_ImageChipsDlg.h"

bui_MonoPlotDlg::bui_MonoPlotDlg():success(false)
{
	setupUi(this);
	this->move(150,120);
	connect(this->GridButton,SIGNAL(released()),this,SLOT(GridButtonclicked()));
	connect(this->localxyzButton,SIGNAL(released()),this,SLOT(localxyzButtonclicked()));
	connect(this->geocentricButton,SIGNAL(released()),this,SLOT(geocentricButtonclicked()));
	connect(this->geographicButton,SIGNAL(released()),this,SLOT(geographicButtonclicked()));
	connect(this->Labelvalue,SIGNAL(editingFinished()),this,SLOT(changeLabel_clicked()));
//	connect(this->changeHeight,SIGNAL(released()),this,SLOT(changeHeight_clicked()));
	connect(this->Zvalue,SIGNAL(editingFinished()),this,SLOT(changeHeight_clicked()));
	connect(this->Ok,SIGNAL(released()),this,SLOT(Ok_clicked()));
	connect(this->Cancel,SIGNAL(released()),this,SLOT(Cancel_clicked()));

	this->labeledited = false;
	this->heightedited = false;
	this->gotTMvalues = false;

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	// addition>>
	this->lblchipID->setVisible(false);
	this->lEchipID->setVisible(false);
	this->pBChangeChipInfo->setVisible(false);
	connect(this->pBChangeChipInfo,SIGNAL(released()),this,SLOT(ChangeChipInfo_clicked()));
	//<<addition
}

bui_MonoPlotDlg::~bui_MonoPlotDlg(void)
{
}

void bui_MonoPlotDlg::setRefSys(const CReferenceSystem &I_refsys)
{
	this->refsys = I_refsys;
};

void bui_MonoPlotDlg::gridsetchecked()
{
	this->refsys.setCoordinateType(eGRID);
    this->GridButton->setChecked(true);
}

void bui_MonoPlotDlg::localxyzsetchecked()
{
	this->refsys.setCoordinateType(eUndefinedCoordinate);
    this->localxyzButton->setChecked(true);   
}

void bui_MonoPlotDlg::geographicsetchecked()
{
    this->refsys.setCoordinateType(eGEOGRAPHIC);
    this->geographicButton->setChecked(true);
}

void bui_MonoPlotDlg::geocentricsetchecked()
{
    this->refsys.setCoordinateType(eGEOCENTRIC);
    this->geocentricButton->setChecked(true);
}


void bui_MonoPlotDlg::LabelValueSetText(const char* label)
{
    this->Labelvalue->setText( label);
}

void bui_MonoPlotDlg::setXValue( double X, int digits)
{
    this->Xvalue->setText(QString( "%1" ).arg(X, 0, 'F', digits ));
}

void bui_MonoPlotDlg::setYValue(double Y, int digits)
{
    this->Yvalue->setText(QString( "%1" ).arg(Y, 0, 'F', digits ));
}

void bui_MonoPlotDlg::setZValue( double Z, int digits)
{
    this->Zvalue->setText(QString( "%1" ).arg(Z, 0, 'F', digits ));
}
	 
void bui_MonoPlotDlg::initTMparameters()
{
	Bui_TMPropertiesDlg tmdlg(&this->refsys, "TM Settings",  "UserGuide/Import3DPoints.html");

	//tmdlg.setCentralMeridian(inifile.getCentralMeridian());
	//tmdlg.setLatitudeofOrigin(inifile.getLatitudeOrigin());

	tmdlg.setModal(true);
	tmdlg.exec();

	//centralMeridian = tmdlg.getCentralMeridianValue();
	//latitudeOrigin = tmdlg.getLatitudeofOriginValue();
	this->gotTMvalues = true;
};

void bui_MonoPlotDlg::setDlgValues( double X, double Y, double Z, const char * label, bool setchecked)
{
    this->X = X;
    this->Y = Y;
    this->Z = Z;
    this->plabel = label;
    int xyCoordDigits = 4;
    int zCoordDigits = 4;

    //  get coordinate system information
	if (this->refsys.getCoordinateType() == eGEOCENTRIC)
	{
		xyCoordDigits = 4;
		zCoordDigits  = 4;
		if (setchecked) this->geocentricsetchecked();
	}
    else if (this->refsys.getCoordinateType() == eGEOGRAPHIC)
    {
		xyCoordDigits = 8;
		zCoordDigits  = 4;
        if (setchecked) this->geographicsetchecked();
    }
    else if( this->refsys.getCoordinateType() == eGRID)
    {
		xyCoordDigits = 4;
		zCoordDigits  = 4;
		if (setchecked) this->gridsetchecked();
    }
    else
    {
		xyCoordDigits = 4;
		zCoordDigits  = 4;
        if (setchecked) this->localxyzsetchecked();
    }
    
	this->setXValue(this->X, xyCoordDigits);    
	this->setYValue(this->Y, xyCoordDigits);    
	this->setZValue(this->Z, zCoordDigits);    
	this->Labelvalue->setText( this->plabel);

    this->repaint();
}

void bui_MonoPlotDlg::setChipDlgValues( double Xc, double Yc, double Zc, const char * label, const char *chipID, bool setchecked)
{
	this->X = Xc;
    this->Y = Yc;
    this->Z = Zc;
    this->plabel = label;
    int xyCoordDigits = 4;
    int zCoordDigits = 4;

	this->chipID = chipID;
    //  get coordinate system information
	if (this->refsys.getCoordinateType() == eGEOCENTRIC)
	{
		xyCoordDigits = 4;
		zCoordDigits  = 4;
		if (setchecked) this->geocentricsetchecked();
	}
    else if (this->refsys.getCoordinateType() == eGEOGRAPHIC)
    {
		xyCoordDigits = 8;
		zCoordDigits  = 4;
        if (setchecked) this->geographicsetchecked();
    }
    else if( this->refsys.getCoordinateType() == eGRID)
    {
		xyCoordDigits = 4;
		zCoordDigits  = 4;
		if (setchecked) this->gridsetchecked();
    }
    else
    {
		xyCoordDigits = 4;
		zCoordDigits  = 4;
        if (setchecked) this->localxyzsetchecked();
    }
    
	this->setXValue(this->X, xyCoordDigits);    
	this->setYValue(this->Y, xyCoordDigits);    
	this->setZValue(this->Z, zCoordDigits);    
	this->Labelvalue->setText( this->plabel);
	this->lEchipID->setText(this->chipID);
    this->repaint();

	this->lblchipID->setVisible(true);
	this->lEchipID->setVisible(true);
	this->pBChangeChipInfo->setVisible(true);
}

void bui_MonoPlotDlg::geographicButtonclicked()
{
    C3DPoint inpoint(this->X, this->Y, this->Z);
    C3DPoint outpoint;

	this->plabel = this->getLabelValueAsLabel().GetChar();

    // from GEOCENTRIC to GEOGRAPHIC
    if (this->refsys.getCoordinateType() == eGEOCENTRIC)
    {
		this->refsys.geocentricToGeographic(outpoint, inpoint);
		
		this->setXValue(outpoint.x, 8);    
		this->setYValue(outpoint.y, 8);    
		this->setZValue(outpoint.z, 4);    
        this->repaint();   
        
    }
    else if(this->refsys.getCoordinateType() == eGEOGRAPHIC)
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel);        
    }
    // from Grid to GEOGRAPHIC
    else if (this->refsys.getCoordinateType() == eGRID)
    {       
		if (!this->refsys.isUTM())
		{
			if (!this->gotTMvalues) initTMparameters();
			this->refsys.setTMParameters(this->centralMeridian, 0.0, 0.0, 1.0, this->latitudeOrigin);
		};

		this->refsys.gridToGeographic(outpoint, inpoint);

   		this->setXValue(outpoint.x, 8);    
		this->setYValue(outpoint.y, 8);    
		this->setZValue(outpoint.z, 4);        
        this->repaint();       
    }
    else 
    {
		this->setDlgValues(this->X, this->Y,this->Z, this->plabel);         
    }
}

void bui_MonoPlotDlg::geocentricButtonclicked()
{
    C3DPoint inpoint(this->X, this->Y, this->Z);
    C3DPoint outpoint;

	this->plabel = this->getLabelValueAsLabel().GetChar();

    if(this->refsys.getCoordinateType() == eGEOCENTRIC)
    {       
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel);       
    }
	else if(this->refsys.getCoordinateType() == eGEOGRAPHIC)
    {     
		this->refsys.geographicToGeocentric(outpoint, inpoint);
		this->setXValue(outpoint.x, 4);    
		this->setYValue(outpoint.y, 4);    
		this->setZValue(outpoint.z, 4);    
        this->repaint();
    }
	else if(this->refsys.getCoordinateType() == eGRID)
    {
		if (!this->refsys.isUTM()) 
		{			
			if (!this->gotTMvalues) initTMparameters();
			this->refsys.setTMParameters(this->centralMeridian, 0.0, 0.0, 1.0, this->latitudeOrigin);
		}

		this->refsys.gridToGeocentric(outpoint, inpoint);
		this->setXValue(outpoint.x, 4);    
		this->setYValue(outpoint.y, 4);    
		this->setZValue(outpoint.z, 4);    
        this->repaint();
    }
	else
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel);       
    }
}

void bui_MonoPlotDlg::GridButtonclicked()
{
	C3DPoint inpoint(this->X, this->Y, this->Z);
    C3DPoint outpoint(inpoint);

	this->plabel = this->getLabelValueAsLabel().GetChar();

	if (!this->refsys.isUTM())
	{
		if (!this->gotTMvalues) initTMparameters();
		this->refsys.setTMParameters(this->centralMeridian, 0.0, 0.0, 1.0, this->latitudeOrigin);
	}

    // from GEOCENTRIC to Grid
    if (this->refsys.getCoordinateType() == eGEOCENTRIC)
    {
		this->refsys.geocentricToGrid(outpoint, inpoint);
		this->setXValue(outpoint.x, 4);    
		this->setYValue(outpoint.y, 4);    
		this->setZValue(outpoint.z, 4);    
        this->repaint();
    }
    else if(this->refsys.getCoordinateType() == eGEOGRAPHIC)
    {
		this->refsys.setUTMZone(this->X, this->Y);
        this->refsys.geographicToGrid(outpoint, inpoint);
		this->setXValue(outpoint.x, 4);    
		this->setYValue(outpoint.y, 4);    
		this->setZValue(outpoint.z, 4);    
        this->repaint();
    }
    else if(this->refsys.getCoordinateType() == eGRID)
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel);
    }
	else 
    {
        this->setDlgValues(this->X, this->Y,this->Z, this->plabel);
    }
}

void bui_MonoPlotDlg::localxyzButtonclicked()
{
    // do nothing to point, but reset original radiobutton
    if(this->refsys.getCoordinateType() == eGEOCENTRIC)
    {
        this->geocentricsetchecked();
    }
    else if(this->refsys.getCoordinateType() == eGEOGRAPHIC)
    {
        this->geographicsetchecked();
    }
    else if(this->refsys.getCoordinateType() == eGRID)
    {
        this->gridsetchecked();
    }
    
    this->repaint();
}

void bui_MonoPlotDlg::changeLabel_clicked()
{
	this->labeledited = true;
	//QString nl(this->Labelvalue->text());
	//this->LabelValueSetText(nl);
	this->LabelValueSetText(this->getLabelValueAsLabel().GetChar());
	this->plabel = this->getLabelValueAsLabel().GetChar();
    return;
}

void bui_MonoPlotDlg::changeHeight_clicked()
{
	this->Z = this->Zvalue->text().toDouble();
	this->heightedited = true;
	this->singleHeight=this->Z;
}


CLabel bui_MonoPlotDlg::getLabelValueAsLabel()
{
	QByteArray qba = this->Labelvalue->text().toLatin1();
	CLabel label(qba.constData());
	if (project.getHeightPointMonoplotting()) project.setCurrentPointLabel(label);
	return label;
}

void bui_MonoPlotDlg::Ok_clicked()
{
	this->success = true;
	this->close();
}

double bui_MonoPlotDlg::getsingleHeight()
{
	return this->singleHeight;
}

void bui_MonoPlotDlg:: Cancel_clicked()
{	
	this->success = false;
	this->close();
}

void bui_MonoPlotDlg::ChangeChipInfo_clicked()
{
	Bui_ImageChipsDlg dlg;
	dlg.exec();
}