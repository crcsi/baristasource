#include "ObjectSelector.h"

#include <QRect>

QSize CObjectSelector::dlgSize(0,0);


CObjectSelector::CObjectSelector(void) 
{
    this->selector = -1;
	this->itemcount = 0;

	if (CObjectSelector::dlgSize.width() != 0)
	{
		
		this->dlg.resize(CObjectSelector::dlgSize);
	}
}

CObjectSelector::~CObjectSelector(void)
{
	 CObjectSelector::dlgSize = this->dlg.size();
}

void CObjectSelector::setSelector(int selector)
{
    this->selector = selector;

	if (this->itemcount == 1) 
	{
		this->dlg.ListWidget->setCurrentRow(0);
	}
	else if ( this->itemcount > 1 )
	{
		this->setModal(true);
		this->setExec();
	}
}

int CObjectSelector::getSelector()
{
    return this->selector;
}

vector<ZObject*> CObjectSelector::getMultiSelection()
{
	return this->dlg.getSelectedObjects();
}

int CObjectSelector::getItemCount()
{
	return this->itemcount;
}

void CObjectSelector::setTitle(const char* title)
{
    this->dlg.setTitleText(title);
	this->dlg.setWindowTitle(title);
}

void CObjectSelector::setButtonText(const char* buttontext)
{
    this->dlg.setButtonText(buttontext);
}

void CObjectSelector::setModal(bool modal)
{
    this->dlg.setModal(modal);
}

void CObjectSelector::show()
{
    this->dlg.show();
}

void CObjectSelector::setMultiSelection(bool b)
{
	this->dlg.setMultiSelection(b);
}

void CObjectSelector::makeDEMSelector()
{

    this->dlg.setWindowTitle("Object Chooser: DEMs");
    this->dlg.setButtonText("Select DEM");

	for (int i = 0; i < project.getNumberOfDEMs(); i++)
    {
        const char* name = project.getDEMAt(i)->getFileName()->GetChar();
        this->insertItem( name, project.getDEMAt(i) );
    }

	if (CObjectSelector::dlgSize.width() ==0)
		this->dlg.adjustLineCount(this->itemcount);

    this->setSelector(DEMSELECTOR);
}

void CObjectSelector::makeALSSelector()
{

    this->dlg.setWindowTitle("Object Chooser: ALS data set");
    this->dlg.setButtonText("Select ALS");

	for (int i = 0; i < project.getNumberOfAlsDataSets(); i++)
    {
		const char* name = project.getALSAt(i)->getFileNameChar();
        this->insertItem( name, project.getALSAt(i) );
    }

	if (CObjectSelector::dlgSize.width() ==0)
		this->dlg.adjustLineCount(this->itemcount);

    this->setSelector(ALSSELECTOR);
}

void CObjectSelector::makeImageSelector(eImageSensorModel sensorType)
{
    this->dlg.setWindowTitle("Object Chooser: Images");
    this->dlg.setButtonText("Select Image");

	int count = 0;
    for (int i = 0; i < project.getNumberOfImages(); i++)
    {
        const char* name = project.getImageAt(i)->getFileNameChar();
        if (sensorType == eUNDEFINEDSENSOR) // always add 
			this->insertItem( name, project.getImageAt(i));
		else if (sensorType == ePUSHBROOM && project.getImageAt(i)->getPushbroom()->gethasValues())
			this->insertItem( name, project.getImageAt(i));
		else if (sensorType == eRPC && project.getImageAt(i)->getRPC()->gethasValues())
			this->insertItem( name, project.getImageAt(i));
		else if (sensorType == eTFW && project.getImageAt(i)->getTFW()->gethasValues())
			this->insertItem( name, project.getImageAt(i));
		else if (sensorType == eAFFINE && project.getImageAt(i)->getAffine()->gethasValues())
			this->insertItem( name, project.getImageAt(i));
		else if (sensorType == eFRAMECAMERA && project.getImageAt(i)->getFrameCamera()->gethasValues())
			this->insertItem( name, project.getImageAt(i));
    }

	if (CObjectSelector::dlgSize.width() ==0)
		this->dlg.adjustLineCount(this->itemcount);

	//condition commented by M. Awrangjeb on 19 March 2009 (comment removed)
	//	if (sensorType == ePUSHBROOM || sensorType == eRPC || sensorType == eTFW ||
	//		sensorType == eAFFINE || sensorType == eFRAMECAMERA )	
			this->setMultiSelection(true);

    this->setSelector(IMAGESELECTOR);
}

void CObjectSelector::makeXYPointSelector()
{
    this->dlg.setWindowTitle("Object Chooser: XYPoints");
    this->dlg.setButtonText("Select XYPoints");

	this->xyPointArray.clear();

	int itemcount =0;

    for (int i = 0; i < project.getNumberOfImages(); i++)
    {
        if (project.getImageAt(i)->getXYPoints()->GetSize() > 0)
        {
            const char* name = project.getImageAt(i)->getXYPoints()->getFileName()->GetChar();
            this->insertItem( name, project.getImageAt(i)->getXYPoints() );
			this->xyPointArray.push_back(project.getImageAt(i)->getXYPoints());
			itemcount++;
        }
		if (project.getImageAt(i)->getProjectedXYPoints()->GetSize() > 0)
        {
            const char* name = project.getImageAt(i)->getProjectedXYPoints()->getFileName()->GetChar();
            this->insertItem( name, project.getImageAt(i)->getProjectedXYPoints() );
			this->xyPointArray.push_back(project.getImageAt(i)->getProjectedXYPoints());
			itemcount++;
        }
		if (project.getImageAt(i)->getDifferencedXYPoints()->GetSize() > 0)
        {
            const char* name = project.getImageAt(i)->getDifferencedXYPoints()->getFileName()->GetChar();
            this->insertItem( name, project.getImageAt(i)->getDifferencedXYPoints() );
			this->xyPointArray.push_back(project.getImageAt(i)->getDifferencedXYPoints());
			itemcount++;
        }
		if (project.getImageAt(i)->getResiduals()->GetSize() > 0)
        {
            const char* name = project.getImageAt(i)->getResiduals()->getFileName()->GetChar();
			this->xyPointArray.push_back(project.getImageAt(i)->getResiduals());
            this->insertItem( name, project.getImageAt(i)->getResiduals() );
			itemcount++;
        } 
    }

	if (CObjectSelector::dlgSize.width() ==0)
		this->dlg.adjustLineCount(this->itemcount);

    this->setSelector(XYPOINTSELECTOR);

}

void CObjectSelector::makeXYZPointSelector()
{
    this->dlg.setWindowTitle("Object Chooser: XYZPoints");
    this->dlg.setButtonText("Select XYZPoints");

    for (int i = 0; i < project.getNumberOfXYZPointArrays(); i++)
    {
        const char* name = project.getPointArrayAt(i)->getFileName()->GetChar();
        this->insertItem( name, project.getPointArrayAt(i) );
    }

	if (CObjectSelector::dlgSize.width() ==0)
		this->dlg.adjustLineCount(this->itemcount);

    this->setSelector(XYZPOINTSELECTOR);

}

void CObjectSelector::makeRPCSelector()
{
    this->dlg.setWindowTitle("Object Chooser: RPCs");
    this->dlg.setButtonText("Select RPC");

    for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		if (project.getImageAt(i)->hasRPC())
        {
            const char* name = project.getImageAt(i)->rpcFileName();
            this->insertItem( name,project.getImageAt(i) );
        }
    }

	if (CObjectSelector::dlgSize.width() ==0)
		this->dlg.adjustLineCount(this->itemcount);

    this->setSelector(RPCSELECTOR);

}

void CObjectSelector::makeAffineSelector()
{
    this->dlg.setWindowTitle("Object Chooser: Affines");
    this->dlg.setButtonText("Select Affine");

    for (int i = 0; i < project.getNumberOfImages(); i++)
    {
        if (project.getImageAt(i)->hasAffine())
        {
            const char* name = project.getImageAt(i)->affineFileName();
            this->insertItem( name, project.getImageAt(i) );
        }
    }

	if (CObjectSelector::dlgSize.width() ==0)
		this->dlg.adjustLineCount(this->itemcount);

    this->setSelector(AFFINESELECTOR);

}


void CObjectSelector::makeOrbitMergeSelector(CPushBroomScanner* selectedScanner)
{

    this->dlg.setWindowTitle("Choose Images for Orbit Merging ");
    this->dlg.setButtonText("Select Image(s)");

	if (!selectedScanner)
		return;

	vector<COrbitPathModel*> knowPaths;
	vector<COrbitAttitudeModel*> knowAttitudes;

	knowPaths.push_back(selectedScanner->getOrbitPath());
	knowAttitudes.push_back(selectedScanner->getOrbitAttitudes());

	for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		CVImage* vimage = project.getImageAt(i);
		CPushBroomScanner* currentScanner = vimage->getPushbroom();
		bool newScanner = true;

		// check if we got the path already
		for (unsigned int i=0; newScanner && i < knowPaths.size(); i++)
		{
			if (currentScanner && currentScanner->getOrbitPath() == knowPaths[i] )
				newScanner = false;
		}

		// check if we got the attitudes already
		for (unsigned int i=0; newScanner && i < knowAttitudes.size(); i++)
		{
			if (currentScanner && currentScanner->getOrbitAttitudes() == knowAttitudes[i] )
				newScanner = false;
		}

		if (newScanner)
		{
			const char* name = vimage->getFileNameChar();
			this->insertItem( name, vimage);
			knowPaths.push_back(currentScanner->getOrbitPath());
			knowAttitudes.push_back(currentScanner->getOrbitAttitudes());
		}
    }

	if (CObjectSelector::dlgSize.width() ==0)
		this->dlg.adjustLineCount(this->itemcount);

	this->setMultiSelection(true);
    this->setSelector(ORBITMERGESELECTOR);
}

void CObjectSelector::insertItem(const char* name, ZObject* object)
{
    this->dlg.ItemtoListbox(name, object);
	this->itemcount++;
}

void CObjectSelector::setExec()
{
    this->dlg.exec();
}

ZObject* CObjectSelector::getSelection()
{

    vector<ZObject*> selectedObjects = this->dlg.getSelectedObjects();

    if (selectedObjects.size() == 0)
        return NULL;
	else
		return selectedObjects[0];
/*
	if (this->itemcount == 1) selection = 0;

    int count =0;

    if (this->getSelector() == AFFINESELECTOR )
    {
        for (int i = 0; i < project.getNumberOfImages(); i++)
        {
            if (project.getImageAt(i)->hasAffine())
            {
                if (selection == count)
                {
                    object = project.getImageAt(i)->getAffine();
                }   
                count++;
            }
        }
    }
    else if (this->getSelector() == IMAGESELECTOR )
    {
        object = project.getImageAt(selection);   
    }
    else if (this->getSelector() == DEMSELECTOR )
    {
		object = project.getDEMAt(selection);   
    }
    else if (this->getSelector() == RPCSELECTOR )
    {
        for (int i = 0; i < project.getNumberOfImages(); i++)
        {
			if (project.getImageAt(i)->hasRPC())
            {
                if (selection == count)
                {
                    object = project.getImageAt(i)->getRPC();
                }
                count++;
            }
        }
    }
    else if (this->getSelector() == XYPOINTSELECTOR )
    {
		object = (ZObject*) this->xyPointArray[selection];
    }
    else if (this->getSelector() == XYZPOINTSELECTOR )
    {
        object = project.getPointArrayAt(selection);   
    }
	else assert(false);

    return object;
*/
}

