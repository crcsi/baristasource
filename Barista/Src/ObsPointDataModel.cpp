#include "ObsPointDataModel.h"
#include "ObsPointArray.h"
#include "EditorDelegate.h"
#include <QAction>
#include "Bui_ChangeXYSigmasDlg.h"
#include "Bui_ChangeXYZSigmasDlg.h"
#include "XYPointArray.h"
#include "XYZPointArray.h"
#include "utilities.h"
#include "BaristaProject.h"
#include "BestFit2DEllipse.h"
#include "LeastSquaresSolver.h"
#include <QMessageBox>
#include "newmatap.h"
#include "Bui_LabelChangeDlg.h"
#include "VImage.h"
#include "BaristaProjectHelper.h"


CObsPointDataModel::CObsPointDataModel(CObsPointArray* pointArray,ViewParameter viewParameter,CVImage* vimage) : 
	CObjectDataModel(viewParameter),points(pointArray),image(vimage),
	isXYZMonoplotted(false) // CObjectDataModel sets the view; points is an array pointer of CObsPointArray class
{
	this->points->addListener(this); // add this model as a listener for points
	this->setHeader(this->guessHeader()); // sets column headers for the view
	this->setFormatString(this->guessDecimals());	// sets data format for the view
	this->titlePrefix = this->guessTitlePrefix(); // sets a title for the view
	this->dataModelType = eObsPointModel; // selects a model type

}



CObsPointDataModel::~CObsPointDataModel()
{
	this->points->removeListener(this);
}


int CObsPointDataModel::rowCount(const QModelIndex& parent) const
{
	return this->points->GetSize();
}

int CObsPointDataModel::columnCount(const QModelIndex& parent) const
{
	int c = this->points->getDimension();
	
	if (this->vp.showCoordinates)
	{
		if (this->vp.showSigma) // we only allow the sigma when the points are shown as well
			c += this->points->getDimension();
	}
	else
		c -= this->points->getDimension();

	if (this->vp.showSelector)
		c +=1;

	// the label 
	c +=1;

	return  c;
}

QVariant CObsPointDataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();	

	if (role == Qt::TextAlignmentRole)
	{
		if (index.column() == 0 && this->vp.showSelector)
			return int(Qt::AlignCenter | Qt::AlignVCenter);
		else
			return int(Qt::AlignRight | Qt::AlignVCenter);
	}
	else if (role ==Qt::CheckStateRole && index.column() == 0 && this->vp.showSelector)
	{
		CObsPoint*p = this->points->GetAt(index.row());
		if (p->getStatusBit(0))
			return true;
		else
			return false;
	}
	else if ( role == Qt::DisplayRole || 
			  role == Qt::EditRole || 
			  role == SortingRole || 
			  role == Qt::ToolTipRole )
	{
		
		CObsPoint*p = this->points->GetAt(index.row());
		if (index.column() < this->getNameColumn()) // selector, no data to display
		{
			if (role == SortingRole)
				return p->getStatusBit(0); // sort by status
			else
				return QVariant();
		}
		else if (index.column() == this->getNameColumn()) // the label
			return QString().sprintf(this->formatList.at(0).toLatin1().constData(),p->getLabel().GetChar());
		
		QString format;
		int column = index.column() - this->getNameColumn(); 

		if (role == Qt::DisplayRole &&  column < this->formatList.count())
			format = this->formatList.at(column) ;
		else 
			format = "%lf";
		
		if (index.column() >=  this->getFirstCoordinateColumn() && 
			     index.column() < this->getFirstSigmaColumn())
		{
			double value = (*p)[index.column()- this->getFirstCoordinateColumn()];
			if (role == SortingRole)
				return value;
			else
				return QString().sprintf(format.toLatin1().constData(),value);
		}
		else if (index.column() >= this->getFirstSigmaColumn())
		{
			double value;
			value = this->facSigma[index.column() - this->getFirstSigmaColumn()] * p->getSigma(index.column()- this->getFirstSigmaColumn());
			if (role == SortingRole)
				return value;
			else
				return QString().sprintf(format.toLatin1().constData(),value);
		}


	}
	else if (role == Qt::BackgroundRole)
	{
		CObsPoint*p = this->points->GetAt(index.row());
		if (p->getStatusBit(0) &&
			index.column() >= this->getFirstCoordinateColumn() && 
			index.column() < this->getFirstSigmaColumn())
		{
			
			if (p->getStatusBit(ROBUSTBITBASE + index.column() - this->getFirstCoordinateColumn()))
				return QColor(Qt::red);
		}
		return QColor(Qt::white);
	}
		
	return QVariant();
}


void CObsPointDataModel::elementsChanged(CLabel element)
{
	if (!this->listenToSignal)
		return;

	
	this->beginResetModel();
	vector<QString> param;
	param.push_back(this->titlePrefix);
	param.push_back(this->points->getFileName()->GetFullFileName().GetChar());
	param.push_back(this->points->getFileName()->GetChar());
	emit  dataModelChanged(eObjectName,param);
	this->endResetModel(); // force the view to update
}

QVariant CObsPointDataModel::headerData(int section,Qt::Orientation orientation, int role) const
{
	
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal && section < this->headerList.count())
		return this->headerList.at(section);

	return QAbstractTableModel::headerData(section,orientation,role);

}
Qt::ItemFlags CObsPointDataModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags f = QAbstractItemModel::flags(index);

	CObsPoint*p = this->points->GetAt(index.row());

	if (index.column() == 0 && this->vp.showSelector)
	{
		f |= Qt::ItemIsUserCheckable;
		return (Qt::ItemFlags)(f - Qt::ItemIsSelectable);
	}
	else if (!p->getStatusBit(0))
	{
		return (Qt::ItemFlags)(QAbstractItemModel::flags(index) - Qt::ItemIsEnabled);
	}
	else if (this->vp.showSigma && this->vp.allowSigmaChange || this->vp.allowLabelChange)
	{
		if (index.column() >= this->getFirstSigmaColumn() || (index.column() == this->getNameColumn())) 
			return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
	}


	return QAbstractItemModel::flags(index);
}

bool CObsPointDataModel::setData(const QModelIndex &index, const QVariant &value,int role)
{
	if (!index.isValid())
		return false;
	
	if (role == Qt::CheckStateRole)
	{
		if (index.column() == 0)
		{
			
			CObsPoint*p = this->points->GetAt(index.row());
			int d = p->getDimension();
			bool isSelected = index.model()->data(index,Qt::CheckStateRole).toBool();

			if (isSelected)
				p->deactivateStatusBit(0);
			else
				p->activateStatusBit(0);

			
			QModelIndex i2 = index.model()->index(index.row(),index.model()->columnCount(QModelIndex())-1);
			emit dataChanged(index,i2);
			return true;
		}
	}
	else if (role == Qt::EditRole)
	{
		
		if ( this->vp.showSigma && this->vp.allowSigmaChange && index.column() >= this->getFirstSigmaColumn())
		{
			CObsPoint* p = this->points->GetAt(index.row());
			int i = index.column() - this->getFirstSigmaColumn();
			double sigma = value.toDouble() / this->facSigma[i];
			p->setCovarElement(i,i,sigma);
			emit dataChanged(index,index);
			return true;
		}
		else if (this->vp.allowLabelChange && index.column() == this->getNameColumn())
		{
		
			CLabel clabel = value.toString().toLatin1().constData();

			// if label is empty or alreday exists keep old label
			CObsPoint* existingPoint = this->points->getPoint(clabel.GetChar());
			CObsPoint* thisPoint = this->points->getPoint(index.row());
			if (clabel.GetLength() == 0 || (existingPoint && existingPoint != thisPoint))
			{
				QMessageBox::warning( 0, "Change Label not possible!"
				, "This label already exist. Choose different Label!");
			}
			// if label is not empty and not existing yet accept new label
			else
			{
				this->points->GetAt(index.row())->setLabel(clabel);
				emit dataChanged(index,index);
				return true;
			}
		}

	}


	return false;
}


int CObsPointDataModel::getFirstCoordinateColumn() const 
{
		return  (getNameColumn() + 1);
}


int CObsPointDataModel::getFirstSigmaColumn()const
{ 
	return points->getDimension() + getFirstCoordinateColumn();
}

int CObsPointDataModel::getDimension() const 
{
	return this->points->getDimension();
}

void CObsPointDataModel::rightMouseButtonReleased(const QModelIndexList& list,vector<QAction*>& actions)
{
	if ((this->vp.allowSigmaChange || this->vp.allowDeletePoint || this->vp.allowBestFitEllipse || this->vp.allowXYZAveragePoints) && list.size())
	{
		
		QAction *changeSigmaAction = NULL;
		QAction *deleteAction = NULL;
		QAction *ellipseAction = NULL;
		QAction *averageAction = NULL;
		QAction *invertSelectionAction = NULL;

		this->list.clear();

		if (!this->vp.allowDeletePoint)
			this->list = list; // just save the list	

		if (this->vp.showSelector)
		{
			invertSelectionAction = new QAction("Invert Selection",this);
			connect(invertSelectionAction, SIGNAL(triggered()), this, SLOT(invertSelection()));
		}

		if (this->vp.allowSigmaChange)
		{
			changeSigmaAction = new QAction(tr("&Change Sigmas"), this);
			connect(changeSigmaAction, SIGNAL(triggered()), this, SLOT(changeSigmas()));
		}

		if (this->vp.allowBestFitEllipse)
		{
			 ellipseAction = new QAction(tr("&Best Fit Ellipse"), this);
			 if (this->getDimension() == 2 && this->image)
				connect(ellipseAction, SIGNAL(triggered()), this, SLOT(bestFit2DEllipse()));		
			 else if (this->getDimension() == 3)
				connect(ellipseAction, SIGNAL(triggered()), this, SLOT(bestFit3DEllipse()));		
		}



		if (this->vp.allowXYZAveragePoints && list.size() > 1)
		{
			averageAction = new QAction(tr("&Average selected Points"), this);
			connect(averageAction, SIGNAL(triggered()), this, SLOT(averagePoints()));
		}

		if (this->vp.allowDeletePoint)
		{
			deleteAction = new QAction(tr("&Delete Point(s)"), this);
			connect(deleteAction, SIGNAL(triggered()), this, SLOT(removefromTable()));

			// we need to order the list for deleting
			for (int i=0; i< list.size(); i++)
			{
				bool inserted = false;
				for (int k=0; k< this->list.count(); k++)
				{	
					if (list.at(i).row() <= this->list.at(k).row())
					{
						this->list.insert(k,list.at(i));
						inserted = true;
						break;
					}
				}
				
				if (!inserted)
					this->list.append(list.at(i));
			}

		}

		// add all active actions to the vector, the parent class will finally create the menu
		if (invertSelectionAction) actions.push_back(invertSelectionAction);
		if (changeSigmaAction) actions.push_back(changeSigmaAction);
		if (deleteAction)      actions.push_back(deleteAction);
		if (ellipseAction)	   actions.push_back(ellipseAction);
		if (averageAction)	   actions.push_back(averageAction);

	}

	CObjectDataModel::rightMouseButtonReleased(this->list,actions);
}

void CObsPointDataModel::invertSelection()
{
	for (int i=0; i< this->list.size(); i++)
	{
		const QModelIndex& index = list.at(i);
		QModelIndex indexSelector = this->createIndex(index.row(),0);
		this->setData(indexSelector,0,Qt::CheckStateRole);
	}
}

void CObsPointDataModel::changeSigmas()
{
	if (this->getDimension() == 2)
	{
		bui_ChangeXYSigmasDlg sigmadlg;

		sigmadlg.setSX(0.5);
		sigmadlg.setSY(0.5);

		sigmadlg.setModal(true);
		sigmadlg.exec();
		if ( sigmadlg.getmakeChanges())
		{
			for (int i=0; i< this->list.size(); i++)
			{
				const QModelIndex& index = list.at(i);
				int sigmaColumn = this->getFirstSigmaColumn();
				QModelIndex sxIndex = this->createIndex(index.row(),sigmaColumn);
				QModelIndex syIndex = this->createIndex(index.row(),sigmaColumn + 1);

				this->setData(sxIndex,sigmadlg.getSX());
				this->setData(syIndex,sigmadlg.getSY());
			}
		}
	}
	else if (this->getDimension() == 3)
	{
		bui_ChangeXYZSigmasDlg sigmadlg;

		sigmadlg.setSX(0.5);
		sigmadlg.setSY(0.5);
		sigmadlg.setSZ(0.5);

		sigmadlg.setModal(true);
		sigmadlg.exec();
		if ( sigmadlg.getmakeChanges())
		{
			for (int i=0; i< this->list.size(); i++)
			{
				const QModelIndex& index = list.at(i);
				int sigmaColumn = this->getFirstSigmaColumn();
				QModelIndex sxIndex = this->createIndex(index.row(),sigmaColumn);
				QModelIndex syIndex = this->createIndex(index.row(),sigmaColumn + 1);
				QModelIndex szIndex = this->createIndex(index.row(),sigmaColumn + 2);

				this->setData(sxIndex,sigmadlg.getSX());
				this->setData(syIndex,sigmadlg.getSY());
				this->setData(szIndex,sigmadlg.getSZ());
			}
		}
	}
	

}

void CObsPointDataModel::removefromTable()
{

	for ( int i =this->list.size() -1; i >=0; i--)
	{
		this->removeRows(this->list.at(i).row(),1);
	}  
}


QStringList CObsPointDataModel::guessHeader()
{
	QStringList localHeaderList;
	if (this->vp.showSelector)
		localHeaderList.append("use"); // Inserts 'use' at the end of the list

	if (this->points && this->points->getClassName().compare("CXYPointArray") == 0)
	{
		CXYPointArray* pA = (CXYPointArray*)this->points;
		if (pA->getPointType() == RESIDUALS)
		{
			localHeaderList << "Label";
			localHeaderList << "vx";
			localHeaderList << "vy";
			localHeaderList << "vx/sx";
			localHeaderList << "vy/sy";
		}
		else
		{
			localHeaderList << "Label";
			localHeaderList << "x";
			localHeaderList << "y";
			localHeaderList << "sx";
			localHeaderList << "sy";	
		}


	}
	else if (this->points && this->points->getClassName().compare("CXYZPointArray") == 0)
	{

		CXYZPointArray* pA = (CXYZPointArray*) this->points;
		
		if (pA->getPointType() == eRESIDUAL)
		{
			localHeaderList.append(QString("Label" ));
			localHeaderList.append(QString("VX" ));
			localHeaderList.append(QString("VY" ));
			localHeaderList.append(QString("VZ" ));
			localHeaderList.append(QString("VX/SX" ));
			localHeaderList.append(QString("VY/SY" ));
			localHeaderList.append(QString("VZ/SZ" ));		
		}

		else if ( pA->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)
		{
			localHeaderList.append(QString("Label"));
			localHeaderList.append(QString("Latitude"));
			localHeaderList.append(QString("Longitude" ));
			localHeaderList.append(QString("Height" ));
			localHeaderList.append(QString("SLat[m]" ));
			localHeaderList.append(QString("SLon[m]" ));
			localHeaderList.append(QString("SHeight[m]" ));
		}
		else if ( pA->getReferenceSystem()->getCoordinateType() == eGEOCENTRIC)
		{
			localHeaderList.append(QString("Label" ));
			localHeaderList.append(QString("X" ));
			localHeaderList.append(QString("Y" ));
			localHeaderList.append(QString("Z" ));
			localHeaderList.append(QString("SX" ));
			localHeaderList.append(QString("SY" ));
			localHeaderList.append(QString("SZ" ));
		}
		else if ( pA->getReferenceSystem()->getCoordinateType() == eGRID)
		{   
			CCharString p1("Label");

			if (pA->getReferenceSystem()->isUTM())
			{
				p1 += "(Zone ";

				CLabel p2;
				p2 += pA->getReferenceSystem()->getZone();

				p1 = p1 + p2.GetChar();

				if ( pA->getReferenceSystem()->getHemisphere() == SOUTHERN )
				{
					p1 = p1 + ", Southern)";   
				}
				else if ( pA->getReferenceSystem()->getHemisphere() == NORTHERN )
				{
					p1 = p1 + ", Northern)";
				}
			}
			else
			{
				p1 += "(Lat:";
				double lon = pA->getReferenceSystem()->getCentralMeridian();
				double lat = pA->getReferenceSystem()->getLatitudeOrigin();

				char buf[256];
				sprintf(buf, "%5.2f Lon:%5.2f)", lat, lon); 

				p1 = p1 + buf;
			}

			localHeaderList.append(QString(p1.GetChar() ));
			localHeaderList.append(QString("Easting" ));
			localHeaderList.append(QString("Northing" ));
			localHeaderList.append(QString("Height" ));
			localHeaderList.append(QString("SEasting" ));
			localHeaderList.append(QString("SNorthing" ));
			localHeaderList.append(QString("SHeight" ));
		}
		else
		{ // undefined coordinate type
			localHeaderList.append(QString("Label"));
			localHeaderList.append(QString("LocalX"));
			localHeaderList.append(QString("LocalY"));
			localHeaderList.append(QString("LocalZ"));
			localHeaderList.append(QString("SX"));
			localHeaderList.append(QString("SY"));
			localHeaderList.append(QString("SZ"));
		}
	}

	return localHeaderList;
}


QStringList CObsPointDataModel::guessDecimals()
{
	QStringList localFormatList;
	
	// the label
	localFormatList.append("%s");	

	if (this->points)
	{
		this->facSigma.resize(this->points->getDimension());
		for (unsigned int i=0; i< this->facSigma.size(); i++)
		{
			this->facSigma[i] = 1.0;
		}
	}

	if (this->points && this->points->getClassName().compare("CXYPointArray") == 0)
	{
		localFormatList.append("%.2lf");
		localFormatList.append("%.2lf");
		localFormatList.append("%.2lf");
		localFormatList.append("%.2lf");
	}
	else if (this->points && this->points->getClassName().compare("CXYZPointArray") == 0)
	{

		CXYZPointArray* pA = (CXYZPointArray*) this->points;
		if ( pA->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)
		{
			localFormatList.append("%.10lf");
			localFormatList.append("%.10lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");

			
			this->facSigma[0]  =  (M_PI * pA->getReferenceSystem()->getSpheroid().getSemiAverage()) / 180.0;
			this->facSigma[1]  =  (M_PI * pA->getReferenceSystem()->getSpheroid().getSemiAverage()) / 180.0;
			this->facSigma[2]  =  1.0;

		}
		else if ( pA->getReferenceSystem()->getCoordinateType() == eGEOCENTRIC)
		{
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
		}
		else if ( pA->getReferenceSystem()->getCoordinateType() == eGRID)
		{   
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
			localFormatList.append("%.4lf");
		}
		else
		{ // undefined coordinate type
			localFormatList.append("%.3lf");
			localFormatList.append("%.3lf");
			localFormatList.append("%.3lf");
			localFormatList.append("%.3lf");
			localFormatList.append("%.3lf");
			localFormatList.append("%.3lf");
		}


	}

	return localFormatList;

}


QString CObsPointDataModel::guessTitlePrefix()
{
	if (this->points && this->points->getClassName().compare("CXYPointArray") == 0)
	{
		CXYPointArray* pA = (CXYPointArray*)this->points;
		if (pA->getPointType() == RESIDUALS)
		{
			return QString("");
		}
		else
		{
			return QString("xy Points: ");
		}
	}
	else if (this->points && this->points->getClassName().compare("CXYZPointArray") == 0)	
	{
		CXYZPointArray* pA = (CXYZPointArray*)this->points;
		if (pA->getPointType() == eRESIDUAL)
		{
			return QString("");
		}
		else 
		{
			if (pA->getPointType() == eMONOPLOTTED)
				this->isXYZMonoplotted = true;

			return QString("XYZ Points: ");
		}
	}
	else
		return QString();
}

void  CObsPointDataModel::requestWindowTitle()
{
	this->elementsChanged();
}

bool CObsPointDataModel::removeRows (int row,int count,const QModelIndex & parent)
{
    beginRemoveRows(QModelIndex(), row, row+count-1);
	this->listenToSignal = false;

	if (this->isXYZMonoplotted && (project.getMonoplottedPoints() == this->points))
	{
		if (project.removeMonoplottedPoint(row,count))
			baristaProjectHelper.refreshTree();
	}
	else
		this->points->RemoveAt(row,count);

	this->listenToSignal = true;
    endRemoveRows();
    return true;
}




void CObsPointDataModel::bestFit2DEllipse()
{

	CXYPointArray ellipsepoints;

	CXYPointArray* xyPointAray = (CXYPointArray*)this->points;

	for ( int i = 0; i < this->list.size(); i++)
	{
		ellipsepoints.add(xyPointAray->getAt(this->list.at(i).row()));

	}

    CBestFit2DEllipse bestfitellipse;
    CCharStringArray Labels;
    CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();

	Matrix obs;
	
	int nObs = ellipsepoints.GetSize();
	obs.ReSize(nObs, 2);

    for(int i = 0; i < nObs; i++)
    {
		CXYPoint* point = ellipsepoints.getAt(i);

		obs.element(i, 0) = (*point)[0];
		obs.element(i, 1) = (*point)[1];

        // Add covariance
        Matrix cov;

        cov.ReSize(2,2);
        cov = 0;
        cov.element(0, 0) = point->getSigma(0);
        cov.element(1, 1) = point->getSigma(1);
        cov.element(0, 1) = point->getCovarElement(0,1);
        cov.element(1, 0) = point->getCovarElement(1,0);
        covariance->addSubMatrix(cov);

        // Add label
    	CCharString* str;
        str = Labels.Add();
        *str = point->getLabel().GetChar();
    }

	int nPara = 5;

    CLeastSquaresSolver lss(&bestfitellipse, &obs, &Labels, NULL, covariance);

    bool maybe = lss.solve();

	if ( maybe )
	{
		Matrix result;
		result.ReSize(nPara, 1);
		result = 0;

		Matrix covmat;
		covmat.ReSize(nPara, nPara);
		covmat = 0;

		for(int i = 0; i < nPara; i++)
		{
			result.element(i, 0) = lss.getSolution()->element(i, 0);
			
			for(int j = 0; j < nPara; j++)
			{
				covmat.element(i, j) = lss.getCovariance()->element(i, j);
			}
		}

		CXYEllipse ellipse;
		CLabel label = "Ellipse";

		label += this->image->getEllipses()->GetSize();

		ellipse.set(result, label);
		ellipse.setCovariance(covmat);

		this->image->getEllipses()->Add(ellipse);
		
	}

	delete covariance;
	baristaProjectHelper.refreshTree();
}




void CObsPointDataModel::bestFit3DEllipse()
{
	vector<CXYZPoint *> ellipsePoints3D;

	CXYZPointArray* xyzPointArray = (CXYZPointArray*)this->points;

	CXYPointArray ellipsepoints;

	CXYPoint p;
	CXYZPoint COG(0.0, 0.0, 0.0);

	for ( int i = 0; i < this->list.size(); i++)
	{
		CXYZPoint *P = xyzPointArray->getAt(this->list.at(i).row());
		ellipsePoints3D.push_back(P);
		COG[0] += (*P)[0];
		COG[1] += (*P)[1];
		COG[2] += (*P)[2];
		p.x = (*P)[0];
		p.y = (*P)[1];
		p.setSX(P->getSigma(0));
		p.setSY(P->getSigma(1));
		p.setSXY(P->getCovarElement(0,1));
		p.setLabel(P->getLabel());
		ellipsepoints.Add(p);

	}

	if (ellipsePoints3D.size() < 5)
	{
		QMessageBox::warning( 0, "Less than 5 points selected!", 
			                 "3D Ellipse fitting requires at least 5 points!");
	}
	else
	{
		double s = (double) ellipsePoints3D.size();
		COG[0] /= s;
		COG[1] /= s;
		COG[2] /= s;
		

		CBestFit2DEllipse bestfitellipse;
		CCharStringArray Labels;
		CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();

		Matrix obs;
		SymmetricMatrix N(3);     // matrix of eigen value system
		N = 0;

		int nObs = ellipsepoints.GetSize();
		obs.ReSize(nObs, 2);

		for(int i = 0; i < nObs; i++)
		{
			CObsPoint* point = ellipsepoints.GetAt(i);

			obs.element(i, 0) = (*point)[0];
			obs.element(i, 1) = (*point)[1];

			// Add covariance
			Matrix cov;

			cov.ReSize(2,2);
			cov = 0;
			cov.element(0, 0) = point->getSigma(0);
			cov.element(1, 1) = point->getSigma(1);
			cov.element(0, 1) = point->getCovarElement(0,1);
			cov.element(1, 0) = point->getCovarElement(1,0);
			covariance->addSubMatrix(cov);

			// Add label
    		CCharString* str;
			str = Labels.Add();
			*str = point->getLabel().GetChar();

			double x = (*ellipsePoints3D[i])[0] - COG.x;
			double y = (*ellipsePoints3D[i])[1] - COG.y;
			double z = (*ellipsePoints3D[i])[2] - COG.z;
			N.element(0,0) += x * x;  N.element(0,1) += x * y; N.element(0,2) += x * z;
									  N.element(1,1) += y * y; N.element(1,2) += y * z;
															   N.element(2,2) += z * z;
		}
	  
		DiagonalMatrix  eigenValues(3);   // eigen values
		Matrix eigenVectors(3,3);
	  
		Try
		{
			Jacobi(N, eigenValues, eigenVectors);     // compute eigen values and vectors
			
			double sigma0 = sqrt(fabs(eigenValues.element(0)) / double(ellipsePoints3D.size() - 3));   

			C3DPoint normal(eigenVectors.element(0,0), eigenVectors.element(1, 0), eigenVectors.element(2, 0)); 

			int nPara = 5;

			CLeastSquaresSolver lss(&bestfitellipse, &obs, &Labels, NULL, covariance);

			bool maybe = lss.solve();

			if ( maybe )
			{
				Matrix result;
				result.ReSize(nPara, 1);
				result = 0;

				Matrix covmat;
				covmat.ReSize(nPara, nPara);
				covmat = 0;

				for(int i = 0; i < nPara; i++)
				{
					result.element(i, 0) = lss.getSolution()->element(i, 0);

					for(int j = 0; j < nPara; j++)
					{
						covmat.element(i, j) = lss.getCovariance()->element(i, j);
					}
				}

				CXYEllipse ellipse;
				CLabel label = "Ellipse";
				label += this->points->GetSize();

				bui_LabelChangeDlg labeldlg(label.GetChar());
				labeldlg.setModal(true);
				labeldlg.exec();

				label.Set(labeldlg.getLabelText().GetChar());

				ellipse.set(result, label);
				ellipse.setCovariance(covmat);

				CXYPoint center = ellipse.getCenter();
				CXYZPoint Center3D(center.x, center.y,0);
				Center3D.setLabel(label);
				Center3D.setSX(center.getSX());
				Center3D.setSXY(center.getSXY());
				Center3D.setSY(center.getSY());
				Center3D.setSZ(sigma0 / sqrt(double(ellipsePoints3D.size())));
				
				Center3D.z = COG.z -(normal.x * (Center3D.x - COG.x) + normal.y * (Center3D.y - COG.y)) / normal.z;

				this->points->Add(Center3D);
			}

			delete covariance;
			baristaProjectHelper.refreshTree();
		}
		CatchAll
		{
			QMessageBox::warning( 0, "Inappropriate configuration!", 
			                     "The 3D Points for ellipse fitting must not be situated on a straight line!");
		}
	}
}

void CObsPointDataModel::averagePoints()
{

	vector<CXYZPoint *> selection;

	CXYZPointArray* xyzPointArray = (CXYZPointArray*)this->points;

	CXYZPoint Average(0.0, 0.0, 0.0);

	for ( int i = 0; i < this->list.size(); i++)
	{
		CXYZPoint *P = xyzPointArray->getAt(this->list.at(i).row());
		selection.push_back(P);
		Average.x += P->x;
		Average.y += P->y;
		Average.z += P->z;

	}
	
	if (selection.size() < 2)
	{
		QMessageBox::warning( 0, "Less than 2 points selected!", 
			                 "Averaging requires at least 2 points!");
	}
	else
	{
		double s = (double) selection.size();
		Average[0] /= s;
		Average[1] /= s;
		Average[2] /= s;

		double sigmaX = 0;
		double sigmaY = 0;
		double sigmaZ = 0;
		for (unsigned  int i = 0; i < selection.size(); i++)
		{
			sigmaX += (Average.x - selection[i]->x)*(Average.x - selection[i]->x);
			sigmaY += (Average.y - selection[i]->y)*(Average.y - selection[i]->y);
			sigmaZ += (Average.z - selection[i]->z)*(Average.z - selection[i]->z);
		}

		sigmaX /= (double) selection.size() - 1.0;
		sigmaY /= (double) selection.size() - 1.0;
		sigmaZ /= (double) selection.size() - 1.0;

		Average.setSX(sqrt(sigmaX));
		Average.setSY(sqrt(sigmaY));
		Average.setSZ(sqrt(sigmaZ));

		CLabel label = selection[selection.size()-1]->getLabel();
		label += "M";
		
		while (this->points->getPoint(label.GetChar()))
			label++;
		
		bui_LabelChangeDlg labeldlg(label.GetChar());
		labeldlg.setModal(true);
		labeldlg.exec();

		while ( labeldlg.getSuccess() && this->points->getPoint(labeldlg.getLabelText().GetChar()) )
		{
			QMessageBox::warning( 0, "Point wasn't added to point list!", 
				"Choosen point label exists already");

			CLabel testlabel(labeldlg.getLabelText().GetChar());
			testlabel++;	
			labeldlg.setLabelText(testlabel.GetChar());
			labeldlg.setSuccess(false);
			labeldlg.exec();
		}

		if ( labeldlg.getSuccess() )
		{
			label.Set(labeldlg.getLabelText().GetChar());
			Average.setLabel(label);
			this->points->Add(Average);
		}
	}
}

CObsPoint* CObsPointDataModel::getObsPoint(int index)
{
	return this->points->GetAt(index);
}

vector<int> CObsPointDataModel::getSigmaColumns() const
{
	int firstC = this->getFirstSigmaColumn();
	vector<int> sigmaColumns(this->getDimension());	
	for (int i=0; i < this->getDimension(); i++)
		sigmaColumns[i] = firstC + i;

	return sigmaColumns;
}

CReferenceSystem* CObsPointDataModel::getReferenceSystem()
{
	if (this->points->getClassName().compare("CXYZPointArray")== 0)
	{
		CXYZPointArray* xyzArray =(CXYZPointArray*)this->points;
		return xyzArray->getReferenceSystem();
	}
	else
		return NULL;
}