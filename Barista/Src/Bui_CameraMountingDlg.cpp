#include "Bui_CameraMountingDlg.h"

#include <QDoubleValidator>
#include "utilities.h"



Bui_CameraMountingDlg::Bui_CameraMountingDlg(CCameraMounting* existingCameraMounting,bool allowEdit,QWidget *parent):
	CAdjustmentDlg(parent),existingCameraMounting(existingCameraMounting)
{
	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->connect(this->rB_RollPitchYaw,SIGNAL(released()),this,SLOT(rbRollPitchYawReleased()));
	this->connect(this->rB_OmegaPhiKappa,SIGNAL(released()),this,SLOT(rbOmegaPhiKappaReleased()));
	this->connect(this->lE_XShift,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_YShift,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_ZShift,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Angle1,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Angle2,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Angle3,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Name,  SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	
	this->connect(this->lE_XShift,SIGNAL(editingFinished()),this,SLOT(leXShiftEditingFinished()));
	this->connect(this->lE_YShift,SIGNAL(editingFinished()),this,SLOT(leYShiftEditingFinished()));
	this->connect(this->lE_ZShift,SIGNAL(editingFinished()),this,SLOT(leZShiftEditingFinished()));
	this->connect(this->lE_Angle1,SIGNAL(editingFinished()),this,SLOT(leAngle1EditingFinished()));
	this->connect(this->lE_Angle2,SIGNAL(editingFinished()),this,SLOT(leAngle2EditingFinished()));
	this->connect(this->lE_Angle3,SIGNAL(editingFinished()),this,SLOT(leAngle3EditingFinished()));
	this->connect(this->lE_Name,  SIGNAL(editingFinished()),this,SLOT(leNameEditingFinished()));

	this->lE_XShift->setValidator(new QDoubleValidator(this));	
	this->lE_YShift->setValidator(new QDoubleValidator(this));	
	this->lE_ZShift->setValidator(new QDoubleValidator(this));	
	this->lE_Angle1->setValidator(new QDoubleValidator(this));	
	this->lE_Angle2->setValidator(new QDoubleValidator(this));	
	this->lE_Angle3->setValidator(new QDoubleValidator(this));

	if (existingCameraMounting != NULL)
	{
		// make local copy
		this->cameraMounting = *existingCameraMounting;
		this->existingCameraMounting->addListener(this);
	}
	else
		this->cameraMounting.setName("New Camera Mounting");

	this->rB_RollPitchYaw->setChecked(true);
	
	this->lE_XShift->setEnabled(allowEdit);
	this->lE_YShift->setEnabled(allowEdit);
	this->lE_ZShift->setEnabled(allowEdit);
	this->lE_Angle1->setEnabled(allowEdit);
	this->lE_Angle2->setEnabled(allowEdit);
	this->lE_Angle3->setEnabled(allowEdit);


	this->applySettings();

}

Bui_CameraMountingDlg::~Bui_CameraMountingDlg(void)
{
	this->cleanUp();
}


void Bui_CameraMountingDlg::cleanUp()
{
	if (this->existingCameraMounting)
		this->existingCameraMounting->removeListener(this);
	
	this->existingCameraMounting = NULL;
}


void  Bui_CameraMountingDlg::applySettings()
{
	int digits2 = 4;
	int digits3 = 4;
	
	this->lE_Name->setText(QString("%1").arg(this->cameraMounting.getName().GetChar())); 

	this->lE_XShift->setText(QString("%1").arg(this->cameraMounting.getShift().x,0,'f',digits2));
	this->lE_YShift->setText(QString("%1").arg(this->cameraMounting.getShift().y,0,'f',digits2));
	this->lE_ZShift->setText(QString("%1").arg(this->cameraMounting.getShift().z,0,'f',digits2));

	if (this->rB_RollPitchYaw->isChecked())
	{
		this->lE_Angle1->setText(QString("%1").arg(this->cameraMounting.getRotation()[0]*180.0/M_PI,0,'f',digits3));
		this->lE_Angle2->setText(QString("%1").arg(this->cameraMounting.getRotation()[1]*180.0/M_PI,0,'f',digits3));
		this->lE_Angle3->setText(QString("%1").arg(this->cameraMounting.getRotation()[2]*180.0/M_PI,0,'f',digits3));
	}
	else
	{
		this->lE_Angle1->setText(QString("%1").arg(this->omegaPhiKappa[0]*180.0/M_PI,0,'f',digits3));
		this->lE_Angle2->setText(QString("%1").arg(this->omegaPhiKappa[1]*180.0/M_PI,0,'f',digits3));
		this->lE_Angle3->setText(QString("%1").arg(this->omegaPhiKappa[2]*180.0/M_PI,0,'f',digits3));
	}

	this->checkAvailabilityOfSettings();




}


void  Bui_CameraMountingDlg::checkAvailabilityOfSettings()
{
	if (this->lE_Name->text().isEmpty() || !this->lE_XShift->hasAcceptableInput() ||
		!this->lE_YShift->hasAcceptableInput() || !this->lE_ZShift->hasAcceptableInput() ||
		!this->lE_Angle1->hasAcceptableInput() || !this->lE_Angle2->hasAcceptableInput() ||
		!this->lE_Angle3->hasAcceptableInput())
		emit enableOkButton(false);
	else
		emit enableOkButton(true);


}


void Bui_CameraMountingDlg::leTextChanged(const QString & text)
{
	this->checkAvailabilityOfSettings();
}


void Bui_CameraMountingDlg::rbRollPitchYawReleased()
{
	this->cameraMounting.getRotation().updateRotMat(this->omegaPhiKappa.getRotMat());
	this->lB_Angle1->setText("Roll");
	this->lB_Angle2->setText("Pitch");
	this->lB_Angle3->setText("Yaw");

	this->applySettings();


}

void Bui_CameraMountingDlg::rbOmegaPhiKappaReleased()
{
	this->omegaPhiKappa.updateRotMat(this->cameraMounting.getRotation().getRotMat());
	this->lB_Angle1->setText("Omega");
	this->lB_Angle2->setText("Phi");
	this->lB_Angle3->setText("Kappa");
	this->applySettings();
}



void Bui_CameraMountingDlg::leNameEditingFinished()
{
	this->cameraMounting.setName(this->lE_Name->text().toLatin1().constData());
	
	this->applySettings();
}

void Bui_CameraMountingDlg::leXShiftEditingFinished()
{
	CXYZPoint oldShift = this->cameraMounting.getShift();

	CXYZPoint shift(this->lE_XShift->text().toDouble(),
				  oldShift.y,
				  oldShift.z);
		
	this->cameraMounting.setShift(shift);
	
	this->applySettings();
}

void Bui_CameraMountingDlg::leYShiftEditingFinished()
{
	CXYZPoint oldShift = this->cameraMounting.getShift();

	CXYZPoint shift(oldShift.x,
				  this->lE_YShift->text().toDouble(),
				  oldShift.z);
		
	this->cameraMounting.setShift(shift);
	
	this->applySettings();
}

void Bui_CameraMountingDlg::leZShiftEditingFinished()
{
	CXYZPoint oldShift = this->cameraMounting.getShift();

	CXYZPoint shift(oldShift.x,
				  oldShift.y,
				  this->lE_ZShift->text().toDouble());
		
	this->cameraMounting.setShift(shift);
	
	this->applySettings();
}

void Bui_CameraMountingDlg::leAngle1EditingFinished()
{
	C3DPoint oldAngles; 
	if (this->rB_RollPitchYaw->isChecked())
		oldAngles = this->cameraMounting.getRotation();
	else
		oldAngles = this->omegaPhiKappa;
	
	C3DPoint angles_rho(this->lE_Angle1->text().toDouble()*M_PI/180.0,
					  oldAngles.y,
					  oldAngles.z);

	if (this->rB_RollPitchYaw->isChecked())
		this->cameraMounting.getRotation().updateAngles_Rho(&angles_rho);
	else
	{
		this->omegaPhiKappa.updateAngles_Rho(&angles_rho);
		this->cameraMounting.getRotation().updateRotMat(this->omegaPhiKappa.getRotMat());
	}

	this->applySettings();
}

void Bui_CameraMountingDlg::leAngle2EditingFinished()
{
	C3DPoint oldAngles; 
	if (this->rB_RollPitchYaw->isChecked())
		oldAngles = this->cameraMounting.getRotation();
	else
		oldAngles = this->omegaPhiKappa;

	C3DPoint angles_rho(oldAngles.x,
					  this->lE_Angle2->text().toDouble()*M_PI/180.0,
					  oldAngles.z);

	if (this->rB_RollPitchYaw->isChecked())
		this->cameraMounting.getRotation().updateAngles_Rho(&angles_rho);
	else
	{
		this->omegaPhiKappa.updateAngles_Rho(&angles_rho);
		this->cameraMounting.getRotation().updateRotMat(this->omegaPhiKappa.getRotMat());
	}
	
	this->applySettings();
}

void Bui_CameraMountingDlg::leAngle3EditingFinished()
{
	C3DPoint oldAngles; 
	if (this->rB_RollPitchYaw->isChecked())
		oldAngles = this->cameraMounting.getRotation();
	else
		oldAngles = this->omegaPhiKappa;
	
	C3DPoint angles_rho(oldAngles.x,
					  oldAngles.y,
					  this->lE_Angle3->text().toDouble()*M_PI/180.0);

	if (this->rB_RollPitchYaw->isChecked())
		this->cameraMounting.getRotation().updateAngles_Rho(&angles_rho);
	else
	{
		this->omegaPhiKappa.updateAngles_Rho(&angles_rho);
		this->cameraMounting.getRotation().updateRotMat(this->omegaPhiKappa.getRotMat());
	}

	this->applySettings();
}

bool Bui_CameraMountingDlg::okButtonReleased()
{
	this->listenToSignal = false;

	this->leNameEditingFinished();
	this->leAngle1EditingFinished();
	this->leAngle2EditingFinished();
	this->leAngle3EditingFinished();
	this->leXShiftEditingFinished();
	this->leYShiftEditingFinished();
	this->leZShiftEditingFinished();

	if (this->existingCameraMounting)
		*this->existingCameraMounting= this->cameraMounting;

	this->listenToSignal = true;

	this->success = true;
	return true;
}

bool Bui_CameraMountingDlg::cancelButtonReleased()
{
	this->success = false;
	return true;
}

CCharString Bui_CameraMountingDlg::helpButtonReleased()
{
	return "UserGuide/ThumbNodeCameraMounting.html";
}

void Bui_CameraMountingDlg::elementsChangedSlot(CLabel element)
{
	if (this->listenToSignal && this->existingCameraMounting)
	{
		this->cameraMounting = *this->existingCameraMounting;
		this->applySettings();
	}	
}