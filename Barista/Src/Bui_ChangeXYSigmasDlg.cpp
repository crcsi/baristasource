#include "Bui_ChangeXYSigmasDlg.h"
#include "Icons.h"

bui_ChangeXYSigmasDlg::bui_ChangeXYSigmasDlg(void)
{	setupUi(this);

   // signals and slots connections
    connect( this->Cancel, SIGNAL( released() ), this, SLOT( Cancel_released() ) );
    connect( this->OKButton, SIGNAL( released() ), this, SLOT( OKButton_released() ) );

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

}

bui_ChangeXYSigmasDlg::~bui_ChangeXYSigmasDlg(void)
{
}


void bui_ChangeXYSigmasDlg::setSX( double sx )
{
	this->sx = sx;
	this->SXInput->setText(QString( "%1" ).arg(this->sx , 0, 'F', 8 ));
}

void bui_ChangeXYSigmasDlg::setSY( double sy )
{
	this->sy = sy;
	this->SYInput->setText(QString( "%1" ).arg(this->sy , 0, 'F', 8 ));
}

double bui_ChangeXYSigmasDlg::getSX()
{
	return this->sx;
}

double bui_ChangeXYSigmasDlg::getSY()
{
 return this->sy;
}

void bui_ChangeXYSigmasDlg::Cancel_released()
{
    this->makechanges = false;
	this->close();
}

void bui_ChangeXYSigmasDlg::OKButton_released()
{
	this->makechanges = true;
	this->sx = this->SXInput->text().toDouble();
	this->sy = this->SYInput->text().toDouble();
	this->close();
}


bool bui_ChangeXYSigmasDlg::getmakeChanges()
{
    return this->makechanges;

}