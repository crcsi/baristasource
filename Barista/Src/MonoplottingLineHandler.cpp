#include "MonoplottingLineHandler.h"
#include "Bui_MainWindow.h"
#include "XYPolyLine.h"
#include <QColor>
#include "Bui_LabelChangeDlg.h"
#include <QMessageBox>
#include "BaristaProjectHelper.h"


CMonoplottingLineHandler::CMonoplottingLineHandler(void):currentVBase(NULL),currentLineLabel("")
{
	this->statusBarText = "MonoplottingLineMODE ";
}

CMonoplottingLineHandler::~CMonoplottingLineHandler(void)
{
}


bool CMonoplottingLineHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{
	return CBaristaViewEventHandler::handleMouseMoveEvent(e, v);
	v->setShowRubberLine(true);
};




bool CMonoplottingLineHandler::keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
{
	CBaristaViewEventHandler::keyReleaseEvent(e, v);

	if (v->getVBase())
	{
		if ( e->key() == Qt::Key_F  || e->key() == Qt::Key_C)
		{

			if (v->getVBase() != this->currentVBase || this->line.getPointCount() < 2)
				return false;

			if ( e->key() == Qt::Key_C ) //close line
			{
				if (this->line.getPointCount())
				{
					this->closeDrawings(this->currentLineLabel);
				}
				this->line.closeLine();
			}

			// has the user has changed the label while drawing the line ?
			if (this->currentLineLabel == this->changedLineLabel)
			{
				
			}
			else // yes
			{
				// rename the 3D polyline
				this->line.setLabel(this->changedLineLabel);
				
				// rename the dataset because we don't draw the line again
				v->renameDataSet(this->currentLineLabel,this->changedLineLabel); 
			
				// rename the points of the 2D/3D polyline
				
				// build the point label
				CCharString lineString (this->changedLineLabel.GetChar());
				CCharString pointString(lineString + "_P");

				// add to 2D monoplotted points of the image
				for (int i=0 ; i < this->current2DPoints.GetSize(); i++)
				{
					CXYPoint* p = this->current2DPoints.getAt(i);
					this->currentPointLabel = pointString.GetChar();
					this->currentPointLabel += i;
					p->setLabel(this->currentPointLabel); // 2d
					this->line.getPoint(i)->setLabel(this->currentPointLabel); //3d
				}

				// ok the label is changed
				this->currentLineLabel = this->changedLineLabel;

			}
			
			// add to the image
			for (int i=0 ; i < this->current2DPoints.GetSize()-1; i++)
				v->getVBase()->addMonoPlottedPoint(*this->current2DPoints.getAt(i),false);
			// last point will force the array to be sorted
			v->getVBase()->addMonoPlottedPoint(*this->current2DPoints.getAt(this->current2DPoints.GetSize()-1));

			// change to finished line
			QColor color(project.getMonoplottedLineColor().x,project.getMonoplottedLineColor().y, project.getMonoplottedLineColor().z);
			this->changeColor(this->currentLineLabel,color);
			v->setShowPoints(this->currentLineLabel, false);
			
			// add to project
			if (project.addMonoplottedLine(this->line))
				baristaProjectHelper.refreshTree();


			// clean up
			this->initPolyline();
			v->resetFirstSecondClick();
		}
		else if ( e->key() == Qt::Key_Escape)
		{
			if (v->getVBase() != this->currentVBase)
				return false;

			this->removeDrawingDataSet(this->currentLineLabel);
			v->resetFirstSecondClick();
			this->initPolyline();
		}
		else if (e->key() == Qt::Key_Backspace)
		{
			if (v->getVBase() != this->currentVBase)
				return false;

			if (this->current2DPoints.GetSize() && this->line.getPointCount())
			{
				// reset the first click 
				if (this->current2DPoints.GetSize() > 1)
				{
					CObsPoint* p =this->current2DPoints.GetAt(this->current2DPoints.GetSize()-2);
					C2DPoint lastClickedPoint((*p)[0],(*p)[1]);			
					v->setFirstClick(lastClickedPoint);
				}
				else
					v->setFirstClick(C2DPoint(-1,-1));

				this->current2DPoints.RemoveAt(this->current2DPoints.GetSize()-1,1);
				this->removePointFromDataSet(this->currentLineLabel,this->line.getPoint(this->line.getPointCount()-1)->getLabel());
				this->line.removePoint(this->line.getPointCount()-1);
			}
		}

		else if ( e->key() == Qt::Key_L )
		{
			//change current line label
			if (this->currentLineLabel.GetLength() == 0 )
			{
				this->currentLineLabel = project.getCurrentLineLabel();
			}

			bui_LabelChangeDlg labeldlg(this->currentLineLabel.GetChar(), "New Line Label");

			labeldlg.setModal(true);
			labeldlg.exec();

			CLabel newlabel = labeldlg.getLabelText().GetChar();

			if ( newlabel.GetLength() == 0 )
			{
				labeldlg.setLabelText(this->currentLineLabel.GetChar());
				labeldlg.exec();
			}
			else
			{
				this->changedLineLabel = newlabel;
				project.setCurrentLineLabel(newlabel );
			}

			labeldlg.close();
		}
	}

	return true;
};


bool CMonoplottingLineHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	v->setShowRubberLine(true);
	return false;
};

bool CMonoplottingLineHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	if (v->getVBase())
	{
		if (!CMonoplottingHandler::handleLeftMouseReleaseEvent(e,v))
			return false;

		CXYPoint xy;
		if (!v->getMouseEventCoords(e, xy.x, xy.y))
			return false;



		//// give a warning if reference sys of line and dem don't match
		if ( project.getNumberOfMonoplottedLines() > 0 )
		{
			// check if coordinates are in the same coordinate system
			// and show at least a warning message
			eCoordinateType demtype = this->monoPlotter.getReferenceSystem().getCoordinateType();
			eCoordinateType monotype = project.getXYZPolyLines()->getReferenceSystem()->getCoordinateType();

			if (demtype != monotype)
			{
				QMessageBox::warning( v, "DEM and monoplotted lines are defined in different coordinate systems!"
				, "Coordinate definitions of monoplotted lines will be set to DEM parameters");
			}
		}
		////


		project.setMonoplottedLineReferenceSystem(this->monoPlotter.getReferenceSystem());

		CXYZPoint linepoint;

		if (this->line.getPointCount() && v->getVBase() != this->currentVBase)
			return false;

		v->setFirstClick(C2DPoint(xy.x, xy.y));

		if (!this->monoPlotter.getPoint(linepoint, xy))
		{
			QMessageBox::warning( v, "Monoplotting did not converge!"
			, "No DEM information available for this image point!");

			// handle rubberline behavior if no point
			if (this->current2DPoints.GetSize() == 0 && this->line.getPointCount() == 0)
			{
				v->resetFirstSecondClick();
			}
			else if (this->current2DPoints.GetSize() > 1)
			{
				CObsPoint* p =this->current2DPoints.GetAt(this->current2DPoints.GetSize()-1);
				C2DPoint lastClickedPoint((*p)[0],(*p)[1]);			
				v->setFirstClick(lastClickedPoint);
			}

			return false;
		}

		// create new linelabel and pointlabel
		int pointNr = this->line.getPointCount();
		CCharString lineLabel(this->currentLineLabel.GetChar());

		if (pointNr == 0)	// get the label from the project for the first point
		{
			// unselect every other line
			project.getXYZPolyLines()->unselectAll();
			
			if (this->changedLineLabel.GetLength())
			{
				this->currentLineLabel.Set(this->changedLineLabel);
			}
			else
			{
				this->currentLineLabel.Set(project.getCurrentLineLabel());
				this->changedLineLabel.Set(this->currentLineLabel); // init with default
			}

			this->currentVBase = v->getVBase();
			this->line.setLabel(this->currentLineLabel);

			// add the datasets in all open image views
			this->addDrawingDataSets(this->currentLineLabel);
			
			// don't show the selected points labels in the current image view
			v->setShowPoints(this->currentLineLabel,true);
			v->setShowPointLabels(this->currentLineLabel,false);	

			// buil the point label
			CCharString lineString (this->currentLineLabel.GetChar());
			CCharString pointString(lineString + "_P");
			this->currentPointLabel = pointString.GetChar();
		}
	 


		CLabel pointLabel(this->currentPointLabel);
		pointLabel += pointNr;

		// save the label
		linepoint.setLabel(pointLabel); // set label for 3D point
		xy.setLabel(pointLabel);	// set label for 2D point

		line.addPoint(linepoint);	// save in 3D line
		this->current2DPoints.Add(xy);	// save in 2D array

		// add to drawing datasets
		this->addToDrawings(this->currentLineLabel,linepoint);
	}
 
    return true;

};




void CMonoplottingLineHandler::initPolyline()
{
	this->line.deleteLine();
	this->line.setLabel("");
	this->currentLineLabel = "";
	this->currentPointLabel = "";
	this->currentVBase = 0;
	this->current2DPoints.RemoveAll();
	this->changedLineLabel = "";
}

bool CMonoplottingLineHandler::redraw(CBaristaView *v)
{
	if (v->getVBase())
	{
		if (!CMonoplottingHandler::redraw(v))
			return false;	
		
		v->setShowRubberLine(true);

		if (this->line.getPointCount())
		{
			if (v->getVBase() == this->currentVBase)
				v->addDrawingDataSet(this->line.getLabel(),2,true,false);	
			else
				v->addDrawingDataSet(this->line.getLabel(),2,false);	

			this->drawMonoplottedLine(v,&this->line);
		}

		CXYZPolyLineArray* monoPlottedLines = project.getXYZPolyLines();
		int nLines = monoPlottedLines->GetSize();

		for (int k=0; k< nLines; k++)
		{
			CXYZPolyLine* pLine = monoPlottedLines->getAt(k);
			v->addDrawingDataSet(pLine->getLabel(),2,false);
			this->drawMonoplottedLine(v,pLine);
		}
	}

	return true;

};


void CMonoplottingLineHandler::open()
{
	baristaProjectHelper.setIsBusy(true);
	CBaristaViewEventHandler::open();
	CMonoplottingHandler::open();

	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->MonoplotLinesAction->setChecked(true);
	mainwindow->blockSignals(false);

	this->initPolyline();

	project.getXYZPolyLines()->addListener(this);

	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i < n; i++)
	{
		redraw(this->baristaViews.getView(i));
	}
	baristaProjectHelper.setIsBusy(false);
};

void CMonoplottingLineHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;


	mainwindow->blockSignals(true);
	mainwindow->MonoplotLinesAction->setChecked(false);
	mainwindow->blockSignals(false);

	CBaristaViewEventHandler::close();
	CMonoplottingHandler::close();

	project.getXYZPolyLines()->removeListener(this);
};



void CMonoplottingLineHandler::drawMonoplottedLine(CBaristaView * v,CXYZPolyLine * pNewPolyline)
{
	
	CXYPolyLine new2DPolyLine;
	for (int i=0; i< pNewPolyline->getPointCount(); i++)
	{
		CXYPoint p;
		if (this->backprojectPoint(v,p,pNewPolyline->getPoint(i)))
			new2DPolyLine.addPoint(p);
	}

	if(pNewPolyline->isClosed())
		new2DPolyLine.closeLine();
		
	QColor color(project.getMonoplottedLineColor().x,project.getMonoplottedLineColor().y, project.getMonoplottedLineColor().z);
	v->addPolyLineDrawing(pNewPolyline->getLabel(),&new2DPolyLine,color,project.getMonoplottedLineWidth());


}

void  CMonoplottingLineHandler::addDrawingDataSets(CLabel& label)
{
	int n = this->baristaViews.getNumberOfViews();
	for (int i=0; i< n ; i++)
	{
		CBaristaView* v = this->baristaViews.getView(i);
		v->addDrawingDataSet(label,2,false);
	}
}


void CMonoplottingLineHandler::addToDrawings(CLabel& dataSetName,CXYZPoint& p)
{
	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i< n ; i++)
	{
		CBaristaView* v = this->baristaViews.getView(i);

		CXYPoint newPoint;
		if (this->backprojectPoint(v,newPoint,p))
		{
			v->addPointDrawing(dataSetName,&newPoint);

			if (this->line.getPointCount() < 2)
				continue;

			QColor color(project.getMonoplottedLineColor().x,project.getMonoplottedLineColor().y, project.getMonoplottedLineColor().z);
			v->addTopology(dataSetName,this->line.getPoint(this->line.getPointCount()-2)->getLabel(),p.getLabel(),color,project.getMonoplottedLineWidth());
		}
	}
}


void CMonoplottingLineHandler::closeDrawings(CLabel& dataSetName)
{
	int n = this->baristaViews.getNumberOfViews();

	CLabel p1(this->line.getPoint(0)->getLabel());
	CLabel p2(this->line.getPoint(this->line.getPointCount()-1)->getLabel());
	QColor color(project.getMonoplottedLineColor().x,project.getMonoplottedLineColor().y, project.getMonoplottedLineColor().z);
	for (int i=0; i< n ; i++)
	{
		CBaristaView* v = this->baristaViews.getView(i);
		v->addTopology(dataSetName,p1,p2,color,project.getMonoplottedLineWidth());
		
	}
}

void CMonoplottingLineHandler::elementAdded(CLabel element)
{

}

void CMonoplottingLineHandler::elementDeleted(CLabel element)
{
	this->removeDrawingDataSet(element);		
}

void CMonoplottingLineHandler::elementsChanged(CLabel element)
{

}


void CMonoplottingLineHandler::elementSelected(bool selected,CLabel element,int elementIndex)
{
	if (selected)
	{
		QColor color = Qt::yellow;
		this->changeColor(element,color);
	}
	else
	{
		QColor color(project.getMonoplottedLineColor().x,project.getMonoplottedLineColor().y, project.getMonoplottedLineColor().z);
		this->changeColor(element,color);
	}
}

void CMonoplottingLineHandler::drawingSettingsChanged(QColor& newColor,unsigned int newLineWidth)
{

	CXYZPolyLineArray* monoPlottedLines = project.getXYZPolyLines();
	int nLines = monoPlottedLines->GetSize();

	for (int k=0; k< nLines; k++)
	{
		CXYZPolyLine* pLine = monoPlottedLines->getAt(k);
		for (int i=0; i < baristaViews.getNumberOfViews(); i++)
		{
			CBaristaView* bview = baristaViews.getView(i);
			bview->changeAllTopologyLineWidth(pLine->getLabel(),newLineWidth);
			bview->changeAllTopologyColors(pLine->getLabel(),newColor);		
		}		
	}
}