#include "MonoplottingPointHandler.h"
#include "Bui_MainWindow.h"
#include <QMessageBox>
#include "Bui_LabelChangeDlg.h"
#include "BaristaProjectHelper.h"
#include "Bui_MonoPlotDlg.h"
CLabel point_dataset_name("MonoPlottingPoints");


CMonoplottingPointHandler::CMonoplottingPointHandler(void):currentPointLabel("")
{
	this->statusBarText = "MonoplottingPointMODE "; // shows mode at the bottom of the MainWindow
	this->monoplotdlg.move(150, 120);
}

bool CMonoplottingPointHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{
	return CBaristaViewEventHandler::handleMouseMoveEvent(e, v);
};

bool CMonoplottingPointHandler::keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
{
	CBaristaViewEventHandler::keyReleaseEvent(e, v);

	if ( e->key() == Qt::Key_L )
	{
		//change current point label
		if (this->currentPointLabel.GetLength() == 0 )
		{
			this->currentPointLabel = project.getCurrentPointLabel();
		}

		bui_LabelChangeDlg labeldlg(this->currentPointLabel.GetChar(), "New Point Label");

		labeldlg.setModal(true);
		labeldlg.exec();

		CLabel newlabel = labeldlg.getLabelText().GetChar();

		if ( newlabel.GetLength() == 0 )
		{
			labeldlg.setLabelText(this->currentPointLabel.GetChar());
			labeldlg.exec();
		}
		else
		{
			this->currentPointLabel = newlabel;
			project.setCurrentPointLabel(newlabel );
		}

		labeldlg.close();
	}

	return true;
};

bool CMonoplottingPointHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	return true;
};


bool CMonoplottingPointHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	//this->monoplotdlg.exec();

	if (v->getVBase())
	{
		if (!CMonoplottingHandler::handleLeftMouseReleaseEvent(e,v))
			return false;

		CXYPoint tmp;
		if (!v->getMouseEventCoords(e, tmp.x, tmp.y))
			return false;

		this->monoplotdlg.setRefSys(this->monoPlotter.getReferenceSystem());

		//// give a warning if reference sys of point and dem don't match
		if ( project.getNumberOfMonoplottedPoints() > 0 )
		{
			// check if coordinates are in the same coordinate system
			// and show at least a warning message
			eCoordinateType demtype = this->monoPlotter.getReferenceSystem().getCoordinateType();
			eCoordinateType monotype = project.getMonoplottedPoints()->getReferenceSystem()->getCoordinateType();

			if (demtype != monotype)
			{
				QMessageBox::warning( v, "DEM and monoplotted points are defined in different coordinate systems!"
				, "Coordinate definitions of monoplotted points will be set to DEM parameters");
			}
		}
		////

		project.setMonoplottedPointReferenceSystem(this->monoPlotter.getReferenceSystem());

		// Enter heigh for individual points
       if (project.getHeightPointMonoplotting())
	   {
     	   	 bui_MonoPlotDlg dlg;
	         dlg.exec();
	         project.setMeanHeight(dlg.getsingleHeight());
	   }

		CXYZPoint newxyz;

		if(this->monoPlotter.getPoint(newxyz, tmp))
		{
			if ( this->currentPointLabel.GetLength() == 0)
			{
				this->currentPointLabel = project.getCurrentPointLabel();
			}

			tmp.setLabel(this->currentPointLabel);
			newxyz.setLabel(this->currentPointLabel);

			v->getVBase()->addMonoPlottedPoint(tmp);

			if (project.addMonoplottedPoint(newxyz))
				baristaProjectHelper.refreshTree();

			this->addMonoPlottedPointDrawings(v,&newxyz);

			this->monoplotdlg.setDlgValues(newxyz.getX(),newxyz.getY(),newxyz.getZ(), newxyz.getLabel().GetChar());

			this->monoplotdlg.show();
			this->monoplotdlg.raise();

			this->initPoint();

			
		}
		else
		{
			QMessageBox::warning( v, "Monoplotting did not converge!"
			, "No DEM information available for this image point!");
			return false;
		}
	}

	return true;
};


bool CMonoplottingPointHandler::redraw(CBaristaView *v)
{
	if (v->getVBase())
	{
		if (!CMonoplottingHandler::redraw(v))
			return false;

		v->addDrawingDataSet(point_dataset_name,2,true,true,QColor(Qt::magenta));

		CObsPointArray* monoPlotted3DPoints = project.getMonoplottedPoints();
		int nPoints = monoPlotted3DPoints->GetSize();

		for (int k=0; k< nPoints; k++)
		{
			CObsPoint* p = monoPlotted3DPoints->GetAt(k);
			CVBase* vbase = v->getVBase();
			
			// don't plot the point when in image point list
			if (vbase)
			{
				CLabel l(p->getLabel());
				if (vbase->getMonoPlottedPoint(l))
					continue;
			}
			CXYPoint point;
			if (this->backprojectPoint(v,point,*p))
				v->addPointDrawing(point_dataset_name,&point);
		}
	}

	return true;
};


void CMonoplottingPointHandler::open()
{
	baristaProjectHelper.setIsBusy(true);
	CBaristaViewEventHandler::open();
	CMonoplottingHandler::open();

	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->MonoplotPointsAction->setChecked(true);
	mainwindow->blockSignals(false);

	project.getMonoplottedPoints()->addListener(this);

	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i < n; i++)
	{
		redraw(this->baristaViews.getView(i));
	}

	this->initPoint();
	baristaProjectHelper.setIsBusy(false);
};

void CMonoplottingPointHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->MonoplotPointsAction->setChecked(false);
	mainwindow->blockSignals(false);

	CBaristaViewEventHandler::close();
	CMonoplottingHandler::close();
	
	project.getMonoplottedPoints()->removeListener(this);

	this->monoplotdlg.close();
}


void CMonoplottingPointHandler::addMonoPlottedPointDrawings(CBaristaView * v,CXYZPoint * pNewPoint)
{
	int n = this->baristaViews.getNumberOfViews();
	
	for (int i=0; i< n ; i++)
	{

		CBaristaView* view = this->baristaViews.getView(i);
		if (view != v) 
		{
			this->redraw(view);
			CXYPoint point;
			if (this->backprojectPoint(view,point,*pNewPoint))
				view->addPointDrawing(point_dataset_name,&point);
		}
		else
			view->update();

	}
}

void CMonoplottingPointHandler::elementDeleted(CLabel element)
{
	if (element.GetLength())
		this->removePointFromDataSet(point_dataset_name,element);
	else
		this->removeDrawingDataSet(point_dataset_name);
}

void CMonoplottingPointHandler::initPoint()
{
	this->currentPointLabel = "";
}

		