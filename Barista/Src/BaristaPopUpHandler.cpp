#include "BaristaPopUpHandler.h"
#include "ZObject.h"

#include <QTreeWidgetItem>
#include <QFileDialog>
#include <QMessageBox>
#include <qmdisubwindow.h>

#include "SystemUtilities.h"
#include "BaristaIconViewItem.h"
#include "BundleHelper.h"
#include "Trans2DAffine.h"
#include "Trans2DConformal.h"
#include "BestFitAffineTrans2D.h"
#include "BestFitConformalTrans2D.h"
#include "LeastSquaresSolver.h"
#include "Bui_orthodialog.h"
#include "OrthoImageGenerator.h"
#include "TiffFile.h"
#include "Bui_ProgressDlg.h"
#include "ImageTable.h"
#include "LabelImage.h"
#include "Bui_BuildingChangeDialog.h"
#include "Bui_CooccurrenceDialog.h"
#include "Bui_GenerateEpipolarDialog.h"
#include "Bui_LabelChangeDlg.h"
#include "Bui_ChangeXYSigmasDlg.h"
#include "Bui_ReferenceSysDlg.h"
#include "Bui_InterpolateDEMHeightDlg.h"
#include "Bui_ChangeXYZSigmasDlg.h"
#include "Bui_DifferenceUnitDlg.h"
#include "Bui_GDALExportDlg.h"
#include "Bui_TMPropertiesDlg.h"
#include "Bui_SatelliteDataImportDlg.h"
#include "Bui_PushbroomImportDlg.h"
//#include "Bui_ImageChipsDlg.h"
#include "Bui_FrameCameraModelDlg.h"
#include "Bui_DialogContainer.h"
#include "Bui_DEMTransformDlg.h"
#include "Bui_DEMOperationsDlg.h"
#include "Bui_DEMStatistics.h"
#include "Bui_ObjectTableDlg.h"
#include "Bui_GenerateRPC.h"
#include "BaristaProjectHelper.h"
#include "LookupTableHelper.h"
#include "histogram.h"

#include "Bui_ALSMergeDialog.h"

#include "GDALFile.h"
#include "ImgBuf.h"
#include "Trans3DFactory.h"

#include "RotationBaseFactory.h"
#include "Trans3DFactory.h"
#include "TransGeographicToGrid.h"

#include "ObjectSelector.h"
#include "MonoPlotter.h"
#include "ProgressThread.h"

#include "BundleHelper.h"
#include "Bui_BuildingDXF.h"

#include "VALS.h"
#include "Bui_ALSInterpolationDialog.h"
#include "Bui_BuildingDialog.h"
#include "Bui_ALSFilter.h"
#include "DEMTable.h"
#include "Icons.h"
#include "ObsPointDataModel.h"
#include "ObjectSelectorTable.h"
#include "BaristaTreeWidget.h"
#include "Bui_ALSExportDialog.h"
#include "BaristaViewEventHandler.h"
#include "Bui_MainWindow.h"
#include "AlosReader.h"
#include "Bui_GCPMatcher.h"
#include "ImageExporter.h"
#include "Bui_BuildingsDetector.h"

//addition >>
#include "ogrsf_frmts.h"
//#include <QMessageBox>
//<< addition

extern CBaristaTreeWidget* thumbnailView;

CBaristaPopUpHandler::CBaristaPopUpHandler(QTreeWidgetItem* item) : interpolateDEMfromALSAction(0), interpolateIntensityFromALSAction(0),
			generateLabelImageAction (0), changeDetectionAction(0), evaluateDetectionAction(0), cooccurrenceAction(0), mergeALSAction(0),
			exportALSAction(0), filterALSAction(0), ExportFrameCameraProjectionMatrixAction(0), ImportFrameCameraOrientationAction1(0), 
			ImportFrameCameraOrientationAction2(0), FrameCameraSetRefsysAction(0), ExportFrameCameraAction(0),
			generateEpipolarImagesAction(0)

{

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(icon);

	this->nullActions();
	this->treewidgetitem = item;

    CBaristaIconViewItem* bitem = (CBaristaIconViewItem*)item;

    this->object = bitem->getObject();

    if (this->object->isa("CVImage"))
    {
		project.setSelectedCVImage ((CVImage *) this->object);
		this->createImagePopUpMenu();
	}
    else if (object->isa("CXYPointArray"))
    {
		CXYPointArray* pa = (CXYPointArray*)  this->object;
		this->createXYPointArrayPopUpMenu(pa->getPointType());
    }
    else if (object->isa("CRPC"))
    {
		this->createRPCPopUpMenu();
    }
    else if (object->isa("CAffine"))
    {
		this->createAffinePopUpMenu();
    }
    else if (object->isa("CTFW"))
    {
		this->createTFWPopUpMenu();
    }
    else if (object->isa("CVDEM"))
    {
		this->createVDEMPopUpMenu();
    }
	else if (object->isa("CVALS"))
    {
		this->createVALSPopUpMenu();
    }
	else if (object->isa("CXYZPolyLineArray"))
    {
		this->createXYZPolyLineArrayPopUpMenu();
    }
    else if (object->isa("CBuildingArray"))
    {
		this->createBuildingArrayPopUpMenu();
    }
	else if (object->isa("CXYContourArray"))
    {
		this->createXYContourArrayPopUpMenu();
	}
	else if (object->isa("CXYPolyLineArray"))
    {
		this->createXYPolyLineArrayPopUpMenu();
	}
    else if (object->isa("CXYZPointArray"))
    {
		this->createXYZPointArrayPopUpMenu();
    }
    else if (object->isa("CXYEllipseArray"))
    {
		this->createXYEllipseArrayPopUpMenu();
    }
    else if (object->isa("CPushBroomScanner"))
    {
		CPushBroomScanner* scanner = (CPushBroomScanner*)  this->object;
		this->createPushBroomScannerPopUpMenu(scanner);
    }
    else if (object->isa("CFrameCameraModel"))
    {
		CFrameCameraModel* frameCamera = (CFrameCameraModel*)  this->object;
		this->createFrameCameraPopUpMenu(frameCamera);
    }
	else if (object->isa("COrbitObservations"))
	{
		COrbitObservations* orbit = (COrbitObservations*) this->object;
		if (orbit->getSplineType() == Path)
		{
			this->createOrbitPathPopUpMenu(orbit);
		}
		else if (orbit->getSplineType() == Attitude)
		{
			this->createOrbitAttitudePopUpMenu(orbit);
		}
	}

	this->addSeparator();
    RenameObjectAction = new QAction(tr("&Rename"), this);
    connect(RenameObjectAction, SIGNAL(triggered()), this, SLOT(RenameObject()));
	this->addAction(RenameObjectAction);

	bool allowDelete = true;

	if (object->isa("COrbitObservations")
		|| object->isa("CCamera")
		|| object->isa("CCameraMounting")
		|| object->isa("CCCDLine")
		|| object->isa("CFrameCamera")
		|| object->isa("CFrameCameraAnalogue")) allowDelete = false;

	// TFW of DEM node must not be deleted
	if ( object->isa("CTFW") )
	{
		CBaristaIconViewItem* parentitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		if ( parentitem->getObject()->isa("CVDEM") ) allowDelete = false;
	}


	if (allowDelete)
	{
		this->addSeparator();
		DeleteObjectAction = new QAction(tr("&Delete"), this);
		connect(DeleteObjectAction, SIGNAL(triggered()), this, SLOT(DeleteObject()));
		this->addAction(DeleteObjectAction);
	}

    this->exec( QCursor::pos() );
}

CBaristaPopUpHandler::~CBaristaPopUpHandler(void)
{
	// CVImage actions
	if ( import2DPointsAction ) delete import2DPointsAction;
	if ( importRPCAction ) delete importRPCAction;
	if ( importAffineAction ) delete importAffineAction;
	if ( importTFWAction ) delete importTFWAction;
	if ( importMetadataAction) delete importMetadataAction;
	if ( importPushbroomAction) delete importPushbroomAction;
	if ( ForwardAffineClickedAction ) delete ForwardAffineClickedAction;
	if ( SingleImageRPCBiasCorrectionAction ) delete SingleImageRPCBiasCorrectionAction;
	if ( createTFWAction ) delete createTFWAction;
	if ( create2DConformalAction ) delete create2DConformalAction;
	if ( OrthoImageGenerationAction ) delete OrthoImageGenerationAction;
	if ( DisplayImagePropertiesAction ) delete DisplayImagePropertiesAction;
	if ( refreshImageHistogramsAction ) delete refreshImageHistogramsAction;
	if ( applyLookupTableAction ) delete applyLookupTableAction;
	if ( exportTiledTIFFAction ) delete exportTiledTIFFAction;
	if ( generateNDVIAction ) delete generateNDVIAction;
    if ( generateLabelImageAction ) delete generateLabelImageAction;
	if (changeDetectionAction) delete changeDetectionAction;
	if (evaluateDetectionAction) delete evaluateDetectionAction;
	if (cooccurrenceAction) delete cooccurrenceAction;
	if (importCameraAction) delete importCameraAction;
	if (innerOrientationAction) delete innerOrientationAction;
	if (orbitPathAction) delete orbitPathAction;
	if (importPreciseALOSData) delete importPreciseALOSData;
	if (MergeOrbitsAction) delete MergeOrbitsAction;	
	if (setPyramidFileAction) delete setPyramidFileAction;
    if (BuildingsDetectionAction) delete BuildingsDetectionAction;
    if (generateEpipolarImagesAction) delete generateEpipolarImagesAction;

	// CXYPointArray actions
	if ( makeImagePointsActiveAction ) delete makeImagePointsActiveAction;
	if ( ChangeSigmaXYAction ) delete		  ChangeSigmaXYAction;
	if ( DifferencebyLabelXYAction ) delete	  DifferencebyLabelXYAction;
	if ( DifferencebyIndexXYAction ) delete	  DifferencebyIndexXYAction;
	if ( monoplotxyRPCAction ) delete		  monoplotxyRPCAction;
	if ( monoplotxyTFWAction ) delete		  monoplotxyTFWAction;
	if ( monoplotxyAffineAction ) delete	  monoplotxyAffineAction;
	if ( monoplotxyPushBroomAction ) delete	  monoplotxyPushBroomAction;
	if ( monoplotxyCurrentSensorModelAction ) delete	  monoplotxyCurrentSensorModelAction;
	if ( export2DSpacedTextFileAction ) delete export2DSpacedTextFileAction;
	if ( export2DCSVTextFileAction ) delete	  export2DCSVTextFileAction;
	if ( georeferencingTFWAction ) delete	  georeferencingTFWAction;
	if ( georeferencing2DTrafoAction ) delete georeferencing2DTrafoAction;
	if ( writeImagePointChipsAction ) delete writeImagePointChipsAction;
	if (xyPointArraySelectorAction) delete xyPointArraySelectorAction;

	// CRPC actions
	if ( SetRPCCurrentModelAction ) delete SetRPCCurrentModelAction;
	if ( SingleRPCBiasCorrectionNoneAction ) delete SingleRPCBiasCorrectionNoneAction;
	if ( SingleRPCBiasCorrectionShiftAction ) delete SingleRPCBiasCorrectionShiftAction;
	if ( SingleRPCBiasCorrectionShiftDriftAction ) delete SingleRPCBiasCorrectionShiftDriftAction;
	if ( SingleRPCBiasCorrectionAffineAction ) delete SingleRPCBiasCorrectionAffineAction;
	if ( SingleRPCRegenerateBiasCorrectedAction ) delete SingleRPCRegenerateBiasCorrectedAction;
	if ( exportIkonosRPCsAction ) delete exportIkonosRPCsAction;
	if ( exportQuickBirdRPCsAction ) delete exportQuickBirdRPCsAction;
	if ( exportSocetRPCsAction) delete exportSocetRPCsAction;
	if ( exportCSVRPCsAction ) delete exportCSVRPCsAction;

	// CAffine actions
	if ( SetAffineCurrentModelAction ) delete SetAffineCurrentModelAction;
	if ( exportAffineTextFileAction) delete exportAffineTextFileAction;

	if (ExportPushBroomScannerRPCAction) delete ExportPushBroomScannerRPCAction;


	if (SetFrameCameraCurrentModelAction) delete SetFrameCameraCurrentModelAction;
	if (ExportFrameCameraProjectionMatrixAction) delete ExportFrameCameraProjectionMatrixAction;
	if (ExportFrameCameraAction) delete ExportFrameCameraAction;
	if (ImportFrameCameraOrientationAction1) delete ImportFrameCameraOrientationAction1;
	if (ImportFrameCameraOrientationAction2) delete ImportFrameCameraOrientationAction2;
	if (FrameCameraSetRefsysAction) delete FrameCameraSetRefsysAction;
	
	// CTFW actions
	if ( exportTFWTextFileAction) delete exportTFWTextFileAction;

	// CVDEM actions
	if ( interpolateHeightfromDEMAction )   delete interpolateHeightfromDEMAction;
	if ( interpolateHeightsfromDEMAction )  delete interpolateHeightsfromDEMAction;
	if ( displayDEMPropertiesAction )       delete displayDEMPropertiesAction;
	if ( displayDEMStatisticsAction)		delete displayDEMStatisticsAction;
	if ( deleteDEMStatisticsAction)			delete deleteDEMStatisticsAction;
	if ( exportImageDEMAction )             delete exportImageDEMAction;
	if ( exportASCIIDEMAction )              delete exportASCIIDEMAction;
	if ( exportASCIIDEMVerticalFlipAction ) delete exportASCIIDEMVerticalFlipAction;
	if ( detectBuildingsAction )            delete detectBuildingsAction;
	if ( setDEMActiveAction )				delete setDEMActiveAction;
	if ( unsetDEMActiveAction )             delete unsetDEMActiveAction;
	if ( transformDEMAction )				delete transformDEMAction;

	// CVALS actions
	if (interpolateDEMfromALSAction) delete interpolateDEMfromALSAction;
	if (interpolateIntensityFromALSAction) delete interpolateIntensityFromALSAction;
	if (mergeALSAction) delete mergeALSAction;
	if (exportALSAction) delete exportALSAction;
	if (filterALSAction) delete filterALSAction;

	// CXYZPolyLineArray  actions
	if ( exportXYZPolyLinesTextFileAction ) delete exportXYZPolyLinesTextFileAction;
	if ( exportXYZPolyLinesVRMLAction )     delete exportXYZPolyLinesVRMLAction;
	if ( exportXYZPolyLinesDXFAction )      delete exportXYZPolyLinesDXFAction;
	if ( TransformXYZPolyLines3DAction )    delete TransformXYZPolyLines3DAction;
	//addiiton >>
	if ( exportXYZPolyLinesShapefileAction )    delete exportXYZPolyLinesShapefileAction;
	//<< addiiton

	// CBuildingArray  actions
	if ( exportBuildingsTextFileAction ) delete  exportBuildingsTextFileAction;
	if ( exportBuildingsVRMLAction ) delete	    exportBuildingsVRMLAction;
	if ( exportBuildingsDXFAction ) delete	    exportBuildingsDXFAction;

	// CXYContourArray  actions
	if ( thinoutXYContoursAction ) delete  thinoutXYContoursAction;

	// CXYPolyLineArray  QActions
	if ( monoplotxyPolyLinesRPCAction ) delete		monoplotxyPolyLinesRPCAction;
	if ( monoplotxyPolyLinesAffineAction ) delete	monoplotxyPolyLinesAffineAction;
	if ( geoTFWPolyLinesAction ) delete				geoTFWPolyLinesAction;
	if ( geo2DConformalPolyLinesAction ) delete		geo2DConformalPolyLinesAction;


	// CXYZPointArray QAction
	if ( ChangeSigmaXYZAction )			delete ChangeSigmaXYZAction;
	if ( DifferencebyLabelXYZAction )	delete DifferencebyLabelXYZAction;
	if ( DifferencebyIndexXYZAction )	delete DifferencebyIndexXYZAction;
	if ( export3DSpacedTextFileAction ) delete export3DSpacedTextFileAction;
	if ( export3DCSVTextFileAction )	delete export3DCSVTextFileAction;
	if ( exportDDMMSSsssFileAction )	delete exportDDMMSSsssFileAction;
	if ( xyz_to_xyRPCAction )			delete xyz_to_xyRPCAction;
	if ( xyz_to_xyAffineAction )		delete xyz_to_xyAffineAction;
	if ( xyz_to_xyTFWAction )			delete xyz_to_xyTFWAction;
	if ( xyz_to_xyPushbroomAction )		delete xyz_to_xyPushbroomAction;
	if (xyz_to_xyFrameCameraAction)		delete xyz_to_xyFrameCameraAction;
	if ( setControlAction )				delete setControlAction;
	if ( unsetControlAction )			delete unsetControlAction;
	if ( XYZtoGeographicAction )		delete XYZtoGeographicAction;
	if ( XYZtoGeocentricAction )		delete XYZtoGeocentricAction;
	if ( XYZtoUTMAction )				delete XYZtoUTMAction;
	if ( XYZtoTMAction )				delete XYZtoTMAction;
	if ( InterpolateDEMAction )         delete InterpolateDEMAction;
	if ( MatchGCPsAction)               delete MatchGCPsAction;
	//addition>>
	if ( Get3DResidualAction )			delete Get3DResidualAction;		
	if ( export3DShapefileAction )			delete export3DShapefileAction;		
	//<<addition
	if ( CopyToOtherListAction )        delete CopyToOtherListAction;

    // CXYEllipseArray  QAction
	if ( getEllipseCentersAction) delete			getEllipseCentersAction;
	if ( exportEllipsestoFileAction) delete			exportEllipsestoFileAction;
	if ( exportEllipseCenterstoFileAction) delete	exportEllipseCenterstoFileAction;


	// general QAction
	if ( DeleteObjectAction ) delete DeleteObjectAction;
	if ( RenameObjectAction ) delete RenameObjectAction;
	if ( OnOffAction ) delete OnOffAction;
	if ( setReferenceInformationAction) delete setReferenceInformationAction;


	if ( this->submenuOne ) delete this->submenuOne;
	if ( this->submenuTwo ) delete this->submenuTwo;
	if ( this->submenuThree ) delete this->submenuThree;	
}

void CBaristaPopUpHandler::nullActions()
{
	// CVImage actions
	this->import2DPointsAction = NULL;
	this->importRPCAction = NULL;
	this->importAffineAction =  NULL;
	this->importTFWAction =  NULL;
	this->importMetadataAction = NULL;
	this->importPushbroomAction = NULL;
	this->ForwardAffineClickedAction =  NULL;
	this->SingleImageRPCBiasCorrectionAction =  NULL;
	this->createTFWAction =  NULL;
	this->create2DConformalAction =  NULL;
	this->OrthoImageGenerationAction =  NULL;
	this->DisplayImagePropertiesAction =  NULL;
	this->refreshImageHistogramsAction =  NULL;
	this->applyLookupTableAction =  NULL;
	this->exportTiledTIFFAction =  NULL;
	this->generateNDVIAction = NULL;
	this->generateLabelImageAction = NULL;
	this->changeDetectionAction = NULL;
	this->evaluateDetectionAction = NULL;
	this->cooccurrenceAction = NULL;
	this->importCameraAction = NULL;
	this->innerOrientationAction = NULL;	
	this->orbitPathAction = NULL;
	this->importPreciseALOSData = NULL;
	this->MergeOrbitsAction = NULL;
	this->setPyramidFileAction = NULL;
	this->BuildingsDetectionAction = NULL;
	this->generateEpipolarImagesAction = NULL;

	// CXYPointArray actions
	this->makeImagePointsActiveAction = NULL;
	this->ChangeSigmaXYAction = NULL;
	this->DifferencebyLabelXYAction = NULL;
	this->DifferencebyIndexXYAction = NULL;
	this->monoplotxyTFWAction = 0;
	this->monoplotxyRPCAction = NULL;
	this->monoplotxyAffineAction = NULL;
	this->monoplotxyPushBroomAction = NULL;
	this->monoplotxyCurrentSensorModelAction = NULL;
	this->export2DSpacedTextFileAction = NULL;
	this->export2DCSVTextFileAction = NULL;
	this->georeferencingTFWAction = NULL;
	this->georeferencing2DTrafoAction = NULL;
	this->writeImagePointChipsAction = NULL;
	this->xyPointArraySelectorAction = NULL;
	this->orbitPathAction = NULL;
	this->importPreciseALOSData = NULL;
	this->MergeOrbitsAction = NULL;
	this->setPyramidFileAction = NULL;

	// CRPC actions
	this->SetRPCCurrentModelAction = NULL;
	this->SingleRPCBiasCorrectionNoneAction = NULL;
	this->SingleRPCBiasCorrectionShiftAction = NULL;
	this->SingleRPCBiasCorrectionShiftDriftAction = NULL;
	this->SingleRPCBiasCorrectionAffineAction = NULL;
	this->SingleRPCRegenerateBiasCorrectedAction = NULL;
	this->exportIkonosRPCsAction = NULL;
	this->exportQuickBirdRPCsAction = NULL;
	this->exportSocetRPCsAction = NULL;
	this->exportCSVRPCsAction = NULL;

	// CAffine actions
	this->SetAffineCurrentModelAction = NULL;
	this->exportAffineTextFileAction = NULL;

	// CPushBroomScanner actions
	this->ExportPushBroomScannerRPCAction = 0;

	this->SetFrameCameraCurrentModelAction = NULL;
	this->ExportFrameCameraProjectionMatrixAction = NULL;
	this->ExportFrameCameraAction = NULL;
	this->ImportFrameCameraOrientationAction1 = NULL;
	this->ImportFrameCameraOrientationAction2 = NULL;
	this->FrameCameraSetRefsysAction = NULL;
	//CTFW action
	this->exportTFWTextFileAction = NULL;

	// CVDEM actions
	this->interpolateHeightfromDEMAction = NULL;
	this->interpolateHeightsfromDEMAction = NULL;
	this->displayDEMPropertiesAction = NULL;
	this->displayDEMStatisticsAction = NULL;
	this->deleteDEMStatisticsAction = NULL;
	this->exportASCIIDEMAction = NULL;
	this->exportImageDEMAction = NULL;
	this->exportASCIIDEMVerticalFlipAction = NULL;
	this->detectBuildingsAction = 0;
	this->setDEMActiveAction = 0;
	this->unsetDEMActiveAction = 0;
	this->transformDEMAction = 0;

	// CVALS actions
	this->interpolateDEMfromALSAction = 0;
	this->interpolateIntensityFromALSAction = 0;
	this->filterALSAction;
	this->mergeALSAction = 0;
	this->exportALSAction = 0;

	// CXYZPolyLineArray  actions
	this->exportXYZPolyLinesTextFileAction = NULL;
	this->exportXYZPolyLinesVRMLAction = NULL;
	this->exportXYZPolyLinesDXFAction = NULL;
	this->TransformXYZPolyLines3DAction = 0;
	//addition >>
	this->exportXYZPolyLinesShapefileAction = NULL;
	//<< addition

	// CBuildingArray  actions
	this->exportBuildingsTextFileAction = NULL;
	this->exportBuildingsVRMLAction = NULL;
	this->exportBuildingsDXFAction = NULL;

	// CXYContourArray  actions
	this->thinoutXYContoursAction = NULL;

	// CXYPolyLineArray
	this->monoplotxyPolyLinesRPCAction = NULL;
	this->monoplotxyPolyLinesAffineAction = NULL;
	this->geoTFWPolyLinesAction = NULL;
	this->geo2DConformalPolyLinesAction = NULL;

	// CXYZPointArray Qaction
	this->ChangeSigmaXYZAction = NULL;
	this->DifferencebyLabelXYZAction = NULL;
	this->DifferencebyIndexXYZAction = NULL;
	this->export3DSpacedTextFileAction = NULL;
	this->export3DCSVTextFileAction = NULL;
	this->exportDDMMSSsssFileAction = NULL;
	this->xyz_to_xyRPCAction = NULL;
	this->xyz_to_xyAffineAction = NULL;
	this->xyz_to_xyTFWAction = NULL;
	this->xyz_to_xyPushbroomAction = NULL;
	this->xyz_to_xyFrameCameraAction = NULL;
	this->setControlAction = NULL;
	this->unsetControlAction = NULL;
	this->XYZtoGeographicAction = NULL;
	this->XYZtoGeocentricAction = NULL;
	this->XYZtoUTMAction = NULL;
	this->XYZtoTMAction = NULL;
	this->InterpolateDEMAction = 0;
	this->MatchGCPsAction = 0;
	//addition>>
	this->Get3DResidualAction = NULL;		
	this->export3DShapefileAction = NULL;		
	//<<addition
	this->CopyToOtherListAction = 0;

    // CXYEllipseArray  QAction
	this->getEllipseCentersAction = NULL;
	this->exportEllipsestoFileAction = NULL;
	this->exportEllipseCenterstoFileAction = NULL;

	// general QAction
	this->DeleteObjectAction = NULL;
	this->RenameObjectAction = NULL;
	this->OnOffAction = NULL;
	this->setReferenceInformationAction = NULL;

	this->submenuOne = NULL;
	this->submenuTwo = NULL;
	this->submenuThree = NULL;
}


void CBaristaPopUpHandler::createImagePopUpMenu()
{
	CVImage *image = 0;

	if (this->object->isa("CVImage")) 
		image = (CVImage*)object;
    
	if (!image)
		return;

	if (image->hasValidHugeImage())
	{
		import2DPointsAction = new QAction(tr("&Import 2D Points"), this);
		connect(import2DPointsAction, SIGNAL(triggered()), this, SLOT(import2DPoints()));
		this->addAction(import2DPointsAction);

		importRPCAction = new QAction(tr("&Import RPCs"), this);
		connect(importRPCAction, SIGNAL(triggered()), this, SLOT(importRPC()));
		this->addAction(importRPCAction);

		importAffineAction = new QAction(tr("&Import Affine"), this);
		connect(importAffineAction, SIGNAL(triggered()), this, SLOT(importAffine()));
		this->addAction(importAffineAction);

		importTFWAction = new QAction(tr("&Import TFW"), this);
		connect(importTFWAction, SIGNAL(triggered()), this, SLOT(importTFW()));
		this->addAction(importTFWAction);


		importMetadataAction = new QAction(tr("&Import Orbit"), this);
		connect(importMetadataAction, SIGNAL(triggered()), this, SLOT(importMetadata()));
		this->addAction(importMetadataAction);

		importPushbroomAction = new QAction(tr("&Init Generic Pushbroom Sensor"), this);
		connect(importPushbroomAction, SIGNAL(triggered()), this, SLOT(importPushbroomData()));
		this->addAction(importPushbroomAction);

#ifndef FINAL_RELEASE

		importCameraAction = new QAction(tr("&Init FrameCamera"), this);
		connect(importCameraAction, SIGNAL(triggered()), this, SLOT(importCamera()));
		this->addAction(importCameraAction);
#endif

		this->addSeparator();

		ForwardAffineClickedAction = new QAction(tr("&Compute Forward Affine"), this);
		connect(ForwardAffineClickedAction, SIGNAL(triggered()), this, SLOT(ForwardAffineClicked()));
		this->addAction(ForwardAffineClickedAction);

		/*
		SingleImageRPCBiasCorrectionAction = new QAction(tr("&Single Image RPC Bias Correction"), this);
		connect(SingleImageRPCBiasCorrectionAction, SIGNAL(triggered()), this, SLOT(SingleImageRPCBiasCorrection()));
		this->addAction(SingleImageRPCBiasCorrectionAction);
	*/
		createTFWAction = new QAction(tr("&Create TFW"), this);
		connect(createTFWAction, SIGNAL(triggered()), this, SLOT(createTFW()));
		this->addAction(createTFWAction);

		create2DConformalAction = new QAction(tr("&Create 2D Conformal"), this);
		connect(create2DConformalAction, SIGNAL(triggered()), this, SLOT(create2DConformal()));
		this->addAction(create2DConformalAction);

		//addition >>
		this->addSeparator();
		writeRPCsToFileAction = new QAction(tr("&Write back RPC (NITF format only)"), this);
		connect(writeRPCsToFileAction, SIGNAL(triggered()), this, SLOT(writeRPCtoImageFile()));
		this->addAction(writeRPCsToFileAction);
		CFileName fname(image->getFileNameChar());
		if (image->hasRPC() && fname.GetFileExt().CompareNoCase("ntf"))
			this->writeRPCsToFileAction->setEnabled(true);
		else
			this->writeRPCsToFileAction->setEnabled(false);
		//<< addition

#ifndef FINAL_RELEASE
		if (image && image->hasFrameCamera() && image->getFrameCamera()->getCamera()->getCameraType() == eAnalogueFrame)
		{
			innerOrientationAction = new QAction(tr("&Compute inner orientation"), this);
			connect(innerOrientationAction, SIGNAL(triggered()), this, SLOT(computeInnerOrientation()));
			this->addAction(innerOrientationAction);
		}

		if (image && image->hasFrameCamera())
		{
			if (project.getNumberOfImagesWithFrameCam() > 1)
			{
				generateEpipolarImagesAction = new QAction(tr("&Generate Epipolar Image"), this);
				connect(generateEpipolarImagesAction, SIGNAL(triggered()), this, SLOT(generateEpipolarImages()));
				this->addAction(generateEpipolarImagesAction);
			}
		}
#endif

		this->addSeparator();

		if (image && image->getCurrentSensorModel() && (project.getNumberOfDEMs() > 0))
		{
			OrthoImageGenerationAction = new QAction(tr("&Orthoimage Generation"), this);
			connect(OrthoImageGenerationAction, SIGNAL(triggered()), this, SLOT(OrthoImageGeneration()));
			this->addAction(OrthoImageGenerationAction);
		}

		refreshImageHistogramsAction = new QAction(tr("&Refresh Image Histogram(s)"), this);
		connect(refreshImageHistogramsAction, SIGNAL(triggered()), this, SLOT(refreshImageHistograms()));
		this->addAction(refreshImageHistogramsAction);

		applyLookupTableAction = new QAction(tr("&Apply Transfer Function permanently"), this);
		connect(applyLookupTableAction, SIGNAL(triggered()), this, SLOT(applyLookupTable()));
		this->addAction(applyLookupTableAction);

		exportTiledTIFFAction = new QAction(tr("&Export Image"), this);
		connect(exportTiledTIFFAction, SIGNAL(triggered()), this, SLOT(exportTiledTIFF()));
		this->addAction(exportTiledTIFFAction);

		if (image)
		{		
			generateNDVIAction  = new QAction(tr("&Generate NDVI"), this);
			connect(generateNDVIAction, SIGNAL(triggered()), this, SLOT(generateNDVI()));
			this->addAction(generateNDVIAction);

			//addition >>
			//if (image->getHugeImage()->getBands() >= 3)
			//{
				this->addSeparator();
				generateTextureInfoAction  = new QAction(tr("&Generate Entropy Mask and Edge Orientation"), this);
				connect(generateTextureInfoAction, SIGNAL(triggered()), this, SLOT(generateTextureInfo()));
				this->addAction(generateTextureInfoAction);

				generateEdgeOrientationAction  = new QAction(tr("&Generate Edge Orientation Only"), this);
				connect(generateEdgeOrientationAction, SIGNAL(triggered()), this, SLOT(generateEdgeOrientation()));
				this->addAction(generateEdgeOrientationAction);

				generateEntropyAction  = new QAction(tr("&Generate Entropy Mask Only"), this);
				connect(generateEntropyAction, SIGNAL(triggered()), this, SLOT(generateEntropy()));
				this->addAction(generateEntropyAction);
			//}
			//<< addition

			if ((image->getHugeImage()->getBpp() <= 8) && (image->getHugeImage()->getBands() == 1))
			{
				generateLabelImageAction  = new QAction(tr("&Generate Label image"), this);
				connect(generateLabelImageAction, SIGNAL(triggered()), this, SLOT(generateLabelImage()));
				this->addAction(generateLabelImageAction);
			}
#ifndef FINAL_RELEASE
			if ((image->getHugeImage()->getBpp() == 16) && (image->getHugeImage()->getBands() == 1))
			{
				this->addSeparator();
				changeDetectionAction  = new QAction(tr("&Building Change Detection"), this);
				connect(changeDetectionAction, SIGNAL(triggered()), this, SLOT(changeDetection()));
				this->addAction(changeDetectionAction);
				evaluateDetectionAction = new QAction(tr("&Evaluation of Building Detection"), this);
				connect(evaluateDetectionAction, SIGNAL(triggered()), this, SLOT(evaluateDetection()));
				this->addAction(evaluateDetectionAction);
			}
			if ((image->getHugeImage()->getBpp() == 8) && (image->getHugeImage()->getBands() == 1))
			{
				this->addSeparator();
				this->cooccurrenceAction  = new QAction(tr("&Compute cooccurrence matrix"), this);
				connect(cooccurrenceAction, SIGNAL(triggered()), this, SLOT(cooccurrence()));
				this->addAction(cooccurrenceAction);
			}

			this->addSeparator();

		 	BuildingsDetectionAction = new QAction(tr("&Building Detection"), this);
    		connect(BuildingsDetectionAction, SIGNAL(triggered()), this, SLOT(BuildingsDetection()));
	    	this->addAction(BuildingsDetectionAction);
         
#endif
		}

		this->addSeparator();

		DisplayImagePropertiesAction = new QAction(tr("&Display Image Properties"), this);
		connect(DisplayImagePropertiesAction, SIGNAL(triggered()), this, SLOT(DisplayImageProperties()));
		this->addAction(DisplayImagePropertiesAction);
	}
	else
	{
		setPyramidFileAction = new QAction(tr("&Select pyramid file"), this);
		connect(setPyramidFileAction, SIGNAL(triggered()), this, SLOT(setPyramidFile()));
		this->addAction(setPyramidFileAction);
	}	
}

/**
 * import 2DPoints to image and add thumbnail to tree
 *
 */
void CBaristaPopUpHandler::import2DPoints()
{
	CVImage* image = (CVImage*)this->object;
	CFileName ifn(image->getName().GetChar());
	QString title("Choose a 2D Point file for image ");
	title.append(ifn.GetFileName().GetChar());

    // file dialog
	QString filename = QFileDialog::getOpenFileName(
		this,
		title,
		//"Choose a 2D Point file",
		inifile.getCurrDir(),
		"2D Points (*.txt *.csv *.ipf)");

    // make sure the file exists
    QFile test(filename);
    if (!test.exists())
        return;
    test.close();

	CFileName cfilename = (const char*)filename.toLatin1();


    CXYPointArray* xypoints = image->getXYPoints();
	if ( xypoints->GetSize() > 0)
		xypoints->RemoveAll();

    xypoints->readXYPointTextFile(cfilename);

	xypoints->setPointType(MEASURED);

	xypoints->setActive(true);

	inifile.setCurrDir(cfilename.GetPath());
	inifile.writeIniFile();

	baristaProjectHelper.refreshTree();
}



/**
 * import RPCs to image and add thumbnail to tree
 *
 */
void CBaristaPopUpHandler::importRPC()
{
	CVImage* image = (CVImage*)this->object;
	CFileName ifn(image->getName().GetChar());
	QString title("Choose a RPC parameter file for image ");
	title.append(ifn.GetFileName().GetChar());

	// file dialog
	QString filename = QFileDialog::getOpenFileName(
		this,
		title,
		inifile.getCurrDir(),
		"RPC file (*_rpc.txt *.RPB *.txt *.rpc)");

    // make sure the file exists
    QFile test(filename);
    if (!test.exists())
        return;
    test.close();

	CFileName cname = (const char*)filename.toLatin1();
	image->initRPCs(cname);

	inifile.setCurrDir(cname.GetPath());
	inifile.writeIniFile();

	baristaProjectHelper.refreshTree();

}

void CBaristaPopUpHandler::importAffine()
{
	CFileName filename;
	CReferenceSystem refSys;

	Bui_ReferenceSysDlg dlg(&refSys,&filename, "Import Affine Parameters", "CoordinateSysDlg.html",  true,"Affine Parameter file (*.txt)");
	dlg.exec();
	if (!dlg.getSuccess()) return;

	CVImage* image = (CVImage*)this->object;

    image->initAffine(filename, refSys);

	inifile.setCurrDir(filename.GetPath());
	inifile.writeIniFile();

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::importTFW()
{
	CFileName filename;
	CReferenceSystem refSys;

	Bui_ReferenceSysDlg dlg(&refSys,&filename, "Import TFW file", "CoordinateSysDlg.html", true, "TFW Parameter file (*.txt *.tfw)");
	dlg.exec();
	if (!dlg.getSuccess()) return;

	CVImage* image = (CVImage*)this->object;
    image->initTFW(filename, refSys);

	inifile.setCurrDir(filename.GetPath());
	inifile.writeIniFile();

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::importMetadata()
{
	
	CFileName filename;
	Bui_SatelliteDataImportDlg dlg(filename,"UserGuide/ImportMetadata.html");
	dlg.exec();

	if (!dlg.getSuccess())
		return;

	CVImage* image = (CVImage*)this->object;

	if (!image->initPushBroomScanner(filename,
									(CCCDLine*)dlg.getCamera(),
									dlg.getCameraMounting(),
									dlg.getOrbitPathModel(),
									dlg.getOrbitAttitudeModel(),
									dlg.getSatelliteHeight(),
									dlg.getGroundHeight(),
									dlg.getGroundTemperature(),
									dlg.getFirstLineTime(),
									dlg.getTimePerLine(),
									dlg.getActiveMountingShiftParameter(),
									dlg.getActiveMountingRotationParameter(),
									dlg.getActiveInteriorParameter(),
									dlg.getActiveAdditionalParameter()))
	{
		dlg.cancelImport();
		QMessageBox::warning( this, "No Orbit imported!", "Could not initialize the Pushbroom Scanner with the read data!");
	}
	else
		dlg.finishImport();

	baristaProjectHelper.refreshTree();
}


void CBaristaPopUpHandler::importPushbroomData()
{

	CVImage* image = (CVImage*)this->object;

	bui_MainWindow* mw = (bui_MainWindow*)currentBaristaViewHandler->getParent();
	mw->startCameraMountingHandler(image);
	
}


void CBaristaPopUpHandler::ForwardAffineClicked()
{
	CVImage* image = (CVImage*)this->object;

    CXYPointArray* xypoints = image->getXYPoints();
	if (xypoints->GetSize() <= 0 )
	{
		QMessageBox::warning( this, "No Image Points measured!"
		, "No Forward Affine possible!");
        return;
	}

    CVControl* controlpoints = project.getControlPoints();
    if (controlpoints == NULL)
	{
		QMessageBox::warning( this, "No Control Points found!"
			, "Set Control Points first!");
		return;
	}

    CCharStringArray Labels;
    CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();


	Matrix obs = controlpoints->getXYZPointArray()->getMergedPoints(&Labels, covariance, xypoints);

    // to have at least 4 img/control-points to determine the 8 parameters!
    if ( obs.Nrows() < 4)
	{
		QMessageBox::warning( this, "ForwardAffine not possible!"
			, "At least 4 control points must be measured to determine Affine parameters");
		return;
	}

	image->getAffine()->sethasValues(true);
	image->setCurrentSensorModel(eAFFINE);

	image->getAffine()->setRefSys(controlpoints->getCurrentSensorModel()->getRefSys());
	image->getAffine()->resectParameters(*(image->getXYPoints()), *(controlpoints->getXYZPointArray()));

	CBaristaBundleHelper helper;
	CFileName protfile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "iteration.prt";

	helper.initProtocol(protfile.GetChar());
	helper.bundleInit(image);

    bool maybe = helper.solve(false, false, false);

	if (!maybe)
	{
		QMessageBox::warning(this, "Adjustment did not converge!", "");
		image->resetSensorModel(image->getAffine());
	}
	else
	{
		helper.closeProtocol();
		protfile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "adjust.prt";
		helper.initProtocol(protfile.GetChar());
	
		helper.printResults();

		QMessageBox::information( this, "Adjustment successful!", helper.rmsString(" Adjustment was successful; s0: ").GetChar());

		image->getAffine()->setFileName("ForwardAffine");
		image->getAffine()->setRefSys(controlpoints->getCurrentSensorModel()->getRefSys());
	}


	baristaProjectHelper.refreshTree();
    delete covariance;

	helper.closeProtocol();
}

void CBaristaPopUpHandler::SingleImageRPCBiasCorrection()
{
	CVImage* image = (CVImage*)this->object;

    if (!image->hasRPC())
	{
		QMessageBox::warning( this, "Image has no RPC parameters!"
		, "No bias correction possible!");
        return;
	}

    CXYPointArray* xypoints = image->getXYPoints();
	if (xypoints->GetSize() <= 0 )
	{
		QMessageBox::warning( this, "No Image Points measured!"
		, "No bias correction possible!");
        return;
	}

    CVControl* controlpoints = project.getControlPoints();
    if (controlpoints == NULL)
	{
		QMessageBox::warning( this, "No Control Points found!"
		, "Set Control Points first!");
        return;
	}

	CReferenceSystem refsys = controlpoints->getCurrentSensorModel()->getRefSys();
	if ((!refsys.isWGS84()) || ( refsys.getCoordinateType() != eGEOGRAPHIC))
	{
		QMessageBox::warning( this, "Single Image Bias Correction not possible!"
			, "Control points have to be provided in WGS84 Geographic Coordinates.");
		return;

	};

    CCharStringArray Labels;
    CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();

    Matrix obs = controlpoints->getXYZPointArray()->getMergedPoints(&Labels, covariance, xypoints);

    // to have at least 1 img/control-points to determine the shift parameters!

    if ( obs.Nrows() == 0)
	{
		QMessageBox::warning( this, "SingleImageBiasCorrection not possible!"
			, "No control point was measured.");
		return;
	}
    if ( obs.Nrows() == 1)
	{
		QMessageBox::warning( this, "SingleImageBiasCorrection might not be possible!"
		, "Only 1 control point was measured. Only shift can be determined");
	}
    if ( obs.Nrows() == 2)
	{
		QMessageBox::warning( this, "SingleImageBiasCorrection might not be possible!"
		, "Only 2 control point was measured. Adjustment might fail, only shift component should be calculated");
	}
    if ( obs.Nrows() == 3)
	{
		QMessageBox::warning( this, "SingleImageBiasCorrection might not be possible!"
		, "Only 3 control point was measured. Adjustment might fail, only shift component should be calculated");
	}


	image->getRPC()->sethasValues(true);
	image->setCurrentSensorModel(eRPC);

	image->getRPC()->setRefSys(controlpoints->getCurrentSensorModel()->getRefSys());

	CBaristaBundleHelper helper;
	CFileName protfile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "iteration.prt";

	helper.initProtocol(protfile.GetChar());
	helper.bundleInit(image);

    bool maybe = helper.solve(false, false, false);

	if (!maybe)
	{
		QMessageBox::warning( this, "Adjustment did not converge!", " ");
	}
	else
	{
		helper.closeProtocol();
	
		protfile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "adjust.prt";
		helper.initProtocol(protfile.GetChar());
	
		helper.printResults();

		QMessageBox::information( this, "Adjustment successful!", helper.rmsString(" Adjustment was successful; s0: ").GetChar());
	}


	delete covariance;
    image->getRPC()->setFileName("SingleImageBiasCorrected");
	helper.closeProtocol();
	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::createTFW()
{
	CVImage* image = (CVImage*)this->object;

    CXYPointArray* xypoints = image->getXYPoints();
    if (xypoints->GetSize() == 0)
	{
		QMessageBox::warning( this, "No Image Points measured!"
		, "No create TFW possible!");
        return;
	}
    CVControl* controlpoints = project.getControlPoints();
    if (controlpoints == NULL)
	{
		QMessageBox::warning( this, "No Control Points found!"
			, "Set Control Points first!");
		return;
	}

    CCharStringArray Labels;
    CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();

	Matrix obs = controlpoints->getXYZPointArray()->getMergedPointsforGeoreferencing(&Labels, covariance, xypoints);

    // to have at least 3 img/control-points to determine the 6 parameters!
    if ( obs.Nrows() < 3)
	{
		QMessageBox::warning( this, "Create TFW not possible!"
			, "At least 3 control points must be measured to determine TFW parameters");
		return;
	}

	CTFW tfw = *image->getTFW();

	tfw.setRefSys(controlpoints->getCurrentSensorModel()->getRefSys());

    CBestFitAffineTrans2D createdtfw;
    CLeastSquaresSolver lss(&createdtfw, &obs, &Labels, NULL, covariance);


    lss.solve();

	Matrix* fromAffine = lss.getSolution();
	Matrix forTFW;

	forTFW.ReSize(6,1);

	// TFW values stored in a different order, but Trans2DAffine and BestFitAffineTrans2D can be
	// used without other restrictions
	forTFW.element(0,0) = fromAffine->element(0,0);
	forTFW.element(1,0) = fromAffine->element(1,0);
	forTFW.element(2,0) = fromAffine->element(2,0);
	forTFW.element(3,0) = fromAffine->element(3,0);
	forTFW.element(4,0) = fromAffine->element(4,0);
	forTFW.element(5,0) = fromAffine->element(5,0);

	tfw.setParameterCount(CSensorModel::addPar,6);
    tfw.setFileName("TFW");
	tfw.setRefSys(controlpoints->getCurrentSensorModel()->getRefSys());
	tfw.setAll(forTFW);
	image->setTFW(tfw);
	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::create2DConformal()
{
	CVImage* image = (CVImage*)this->object;

    CXYPointArray* xypoints = image->getXYPoints();
    if (xypoints->GetSize() == 0)
	{
		QMessageBox::warning( this, "No Image Points measured!"
		, "No create 2D Conformal possible!");
        return;
	}

    CVControl* controlpoints = project.getControlPoints();
    if (controlpoints == NULL)
	{
		QMessageBox::warning( this, "No Control Points found!"
			, "Set Control Points first!");
		return;
	}

    CCharStringArray Labels;
    CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();

	Matrix obs = controlpoints->getXYZPointArray()->getMergedPointsforGeoreferencing(&Labels, covariance, xypoints);

    // to have at least 2 img/control-points to determine the 4 parameters!
    if ( obs.Nrows() < 2)
	{
		QMessageBox::warning( this, "Create 2D Conformal not possible!"
			, "At least 2 control points must be measured to determine 2D conformal parameters");
		return;
	}

    CBestFitConformalTrans2D conformal;

	if (controlpoints->getCurrentSensorModel()->getRefSys().getCoordinateType() == eGRID)
	{
		for (int i = 0; i < obs.Nrows(); i++)
		{
			obs.element(i, 1) *= -1.0;
		}
	}

    CLeastSquaresSolver lss(&conformal, &obs, &Labels, NULL, covariance);

    lss.solve();

    CTFW tfw = *image->getTFW();

	Matrix* fromConformal = lss.getSolution();
	Matrix forTFW;

	forTFW.ReSize(6,1);

	// use TFW to store 2DConformal results
	forTFW.element(0,0) = fromConformal->element(0,0);
	forTFW.element(1,0) = fromConformal->element(1,0);
	forTFW.element(2,0) = fromConformal->element(2,0);
	forTFW.element(3,0) = fromConformal->element(3,0);
	forTFW.element(4,0) = 0;
	forTFW.element(5,0) = 0;

	tfw.setParameterCount(CSensorModel::addPar,4);
    tfw.setFileName("2D Conformal");
    tfw.setRefSys(controlpoints->getCurrentSensorModel()->getRefSys());
	tfw.setAll(forTFW);
	image->setTFW(tfw);
	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::OrthoImageGeneration()
{
	CVImage* image = project.getSelectedCVImage();

	if (!image || !image->getCurrentSensorModel())
	{
		if (!image) QMessageBox::warning(this, "No image selected!",
								 "No orthophoto generation possible!");
		else QMessageBox::warning(this, "Selected image does not have a current sensor model!",
								 "No orthophoto generation possible!");

		project.resetSelectedCVImage();
	}
	else
	{
		bui_OrthoDialog orthoDlg(image, 0, 0, inifile.getCurrDir());
		orthoDlg.show();
		orthoDlg.exec();

		if (orthoDlg.getOrthoCreated()) baristaProjectHelper.refreshTree();
	}
}

void CBaristaPopUpHandler::DisplayImageProperties()
{
	CVImage* image = (CVImage*)this->object;
	CImageTable* table = new CImageTable(image, workspace);
	workspace->addSubWindow(table);
	table->show();

	workspace->tileSubWindows();
}

void CBaristaPopUpHandler::refreshImageHistograms()
{
	CVImage* image = (CVImage*)this->object;
	image->getHugeImage()->refreshHistograms();
}

void CBaristaPopUpHandler::exportTiledTIFF()
{
	CVImage* image = (CVImage*)this->object;
	
	Bui_GDALExportDlg dlg("",image,false,"Export Image");
	dlg.exec();

	if (!dlg.getSuccess())	// false if user has pressed "Cancel"
		return;

	if (!dlg.writeImage())	// actual writing failed
		QMessageBox::critical(NULL,"Export Error","Barista couldn't export the image!",1,0,0);
}

//addition >>
void CBaristaPopUpHandler::generateEdgeOrientation()
{
	bool OK;

	//image
	CVImage* image = (CVImage*)this->object;
	CHugeImage *imgFile = image->getHugeImage();
	float resImage = image->getTFW()->getA();
	//get grey scale image
	CHugeImage *Grey;
	//if (Grey == NULL)
	//{
		if (imgFile->getBands() >= 3)
		{
			//CVImage *GreyImage = project.addImage("Grey Scale",false);
			//CHugeImage *Grey = GreyImage->getHugeImage();
			// if we uncomment above 2 lines & 1 line after four lines from here; and comment the folloing line, then we can see the grey image in barista project 
			Grey = new CHugeImage;
			Grey->prepareWriting("Grey Scale",imgFile->getWidth(), imgFile->getHeight(),8,1);
			CLookupTable lut(8,3); Grey->setLookupTable(&lut);
			OK = imgFile->RGBtoGrey(Grey);
			//baristaProjectHelper.refreshTree();

			if (!OK)
			{			
				QMessageBox::warning( this, "Edge orientation estimation was not successful!", "Conversion to grey scale image failed!");
				return;
			}
		}//if we want to calculate texture for greyscale image then uncomment following codes
		else if (imgFile->getBands() == 1 && imgFile->getBpp() == 8)
			Grey = imgFile;
		else
		{
			QMessageBox::warning( this, "Edge orientation estimation was not successful!", "A grey scale image was not found!");
			return;
		}
	//}
	//image file name
	CFileName FileBase (*(image->getHugeImage()->getFileName()));
	CCharString Path = FileBase.GetPath();
	CFileName FN1 = FileBase.GetFileName();
	CCharString FN2 = FN1.GetFileName();
	CCharString Ext = FN1.GetFileExt();
	CCharString del(" ");
	del[0] = CSystemUtilities::dirDelim;

	/*
	//entropy image
	FileBase = Path + del + FN2 + "_Entropy";
	CFileName FN = FileBase + "." + Ext;
    CVImage *entropyImg = project.addImage(FN, false);
	FN += ".hug";
	CHugeImage *entropy = entropyImg->getHugeImage();
	entropy->setFileName(FN.GetChar());
*/
	
	/*
	//CVImage *GreyImage = project.addImage("Grey Scale",false);
	//CHugeImage *Grey = GreyImage->getHugeImage();
	// if we uncomment above 2 lines & 1 line after four lines from here; and comment the folloing line, then we can see the grey image in barista project 
	CHugeImage *Grey = new CHugeImage;
	Grey->prepareWriting("Grey Scale",imgFile->getWidth(), imgFile->getHeight(),8,1);
	CLookupTable lut(8,3); Grey->setLookupTable(&lut);
	imgFile->RGBtoGrey(Grey);
	//baristaProjectHelper.refreshTree();
*/

	//Edge orientation file
	FileBase = Path + del + FN2 + "_Edge_Orientation";
	CFileName FN = FileBase + "." + Ext;
    CVImage *edgeOrientationImg = project.addImage(FN, false);
	FN += ".hug";
	CHugeImage *edgeOrientation = edgeOrientationImg->getHugeImage();
	edgeOrientation->setFileName(FN.GetChar());

	
	//edge image: contains edge ID numbers
	FileBase = Path + del + FN2 + "_EdgeImage";
	FN = FileBase + "." + Ext;
	CVImage *edgeImg = project.addImage(FN, false);
	FN += ".hug";
	CHugeImage *edgeHug = edgeImg->getHugeImage();
	edgeHug->setFileName(FN.GetChar());	


	baristaProjectHelper.setIsBusy(true);
	//OK = Grey->computeEdgeOrientation(edgeOrientation,edgeHug);

	//the above line works fine but does not show a progress bar
	CProgressThread *thread = new CHugeImageEdgeOrientationThread(Grey,edgeOrientation,edgeHug,resImage);
	bui_ProgressDlg *dlg = new bui_ProgressDlg(thread,Grey, "Computing Edge Orientation ...");
	dlg->exec();	
	OK = dlg->getSuccess();
	delete dlg;
	//the above lines is to show a progress bar

	baristaProjectHelper.setIsBusy(false);
	//baristaProjectHelper.refreshTree();
	
	if (!OK)
	{
		//project.deleteImage(entropyImg);
		project.deleteImage(edgeOrientationImg);
		project.deleteImage(edgeImg);
		QMessageBox::warning( this, "Texture estimation was not successful!", "Texture estimation was not successful!");
	}
	else
	{
		if (image->hasTFW())
		{
			//entropyImg->setTFW(*(image->getTFW()));
			edgeOrientationImg->setTFW(*(image->getTFW()));
			edgeImg->setTFW(*(image->getTFW()));
		}

		baristaProjectHelper.refreshTree();
	}

	//return OK;
}

/*
void CBaristaPopUpHandler::generateEdgeOrientation()
{
	CHugeImage *Grey = NULL;
	generateEdgeOrientation(Grey);
}
*/
void CBaristaPopUpHandler::generateEntropy()
{
	bool OK;

	//image
	CVImage* image = (CVImage*)this->object;
	CHugeImage *imgFile = image->getHugeImage();

	//get grey scale image
	CHugeImage *Grey;
	//if (Grey == NULL)
	//{
		if (imgFile->getBands() >= 3)
		{
			//CVImage *GreyImage = project.addImage("Grey Scale",false);
			//CHugeImage *Grey = GreyImage->getHugeImage();
			// if we uncomment above 2 lines & 1 line after four lines from here; and comment the folloing line, then we can see the grey image in barista project 
			Grey = new CHugeImage;
			Grey->prepareWriting("Grey Scale",imgFile->getWidth(), imgFile->getHeight(),8,1);
			CLookupTable lut(8,3); Grey->setLookupTable(&lut);
			OK = imgFile->RGBtoGrey(Grey);
			//baristaProjectHelper.refreshTree();

			if (!OK)
			{			
				QMessageBox::warning( this, "Entropy estimation was not successful!", "Conversion to grey scale image failed!");
				return;
			}
		}//if we want to calculate texture for greyscale image then uncomment following codes
		else if (imgFile->getBands() == 1 && imgFile->getBpp() == 8)
			Grey = imgFile;
		else
		{
			QMessageBox::warning( this, "Entropy estimation was not successful!", "A grey scale image was not found!");
			return;
		}
	//}
	//image file name
	CFileName FileBase (*(image->getHugeImage()->getFileName()));
	CCharString Path = FileBase.GetPath();
	CFileName FN1 = FileBase.GetFileName();
	CCharString FN2 = FN1.GetFileName();
	CCharString Ext = FN1.GetFileExt();
	CCharString del(" ");
	del[0] = CSystemUtilities::dirDelim;

	//entropy image
	FileBase = Path + del + FN2 + "_EntropyMask";
	CFileName FN = FileBase + "." + Ext;
    CVImage *entropyImg = project.addImage(FN, false);
	FN += ".hug";
	CHugeImage *entropy = entropyImg->getHugeImage();
	entropy->setFileName(FN.GetChar());

	baristaProjectHelper.setIsBusy(true);
	//OK = Grey->computeEntropy(entropy);
	//the above line works fine but does not show a progress bar
	CProgressThread *thread = new CHugeImageEntropyThread(Grey,entropy);
	bui_ProgressDlg *dlg = new bui_ProgressDlg(thread,Grey, "Computing Entropy ...");
	dlg->exec();	
	OK = dlg->getSuccess();
	delete dlg;
	//the above lines is to show a progress bar

	baristaProjectHelper.setIsBusy(false);
	//baristaProjectHelper.refreshTree();
	
	if (!OK)
	{
		project.deleteImage(entropyImg);
		//project.deleteImage(edgeOrientationImg);
		//project.deleteImage(edgeImg);
		QMessageBox::warning( this, "Entropy estimation was not successful!", "Texture estimation was not successful!");
	}
	else
	{
		if (image->hasTFW())
		{
			entropyImg->setTFW(*(image->getTFW()));
			//edgeOrientationImg->setTFW(*(image->getTFW()));
			//edgeImg->setTFW(*(image->getTFW()));
		}

		baristaProjectHelper.refreshTree();
	}

	//return OK;
}

/*
void CBaristaPopUpHandler::generateEntropy()
{
	CHugeImage *Grey = NULL;
	generateEntropy(Grey);
}
*/
void CBaristaPopUpHandler::generateTextureInfo()
{	
	/*
	bool OK;

	//image
	CVImage* image = (CVImage*)this->object;
	CHugeImage *imgFile = image->getHugeImage();

	//get grey scale image
	CHugeImage *Grey;
	if (imgFile->getBands() >= 3)
	{
		//CVImage *GreyImage = project.addImage("Grey Scale",false);
		//CHugeImage *Grey = GreyImage->getHugeImage();
		// if we uncomment above 2 lines & 1 line after four lines from here; and comment the folloing line, then we can see the grey image in barista project 
		Grey = new CHugeImage;
		Grey->prepareWriting("Grey Scale",imgFile->getWidth(), imgFile->getHeight(),8,1);
		CLookupTable lut(8,3); Grey->setLookupTable(&lut);
		OK = imgFile->RGBtoGrey(Grey);
		//baristaProjectHelper.refreshTree();

		if (!OK)
		{			
			QMessageBox::warning( this, "Texture estimation was not successful!", "Conversion to grey scale image failed!");
			return;
		}
	}//if we want to calculate texture for greyscale image then uncomment following codes
	else if (imgFile->getBands() == 1 && imgFile->getBpp() == 8)
		Grey = imgFile;
	else
	{
		QMessageBox::warning( this, "Texture estimation was not successful!", "A grey scale image was not found!");
		return;
	}
	*/

	generateEntropy();
	generateEdgeOrientation();
	

	/*
	//image
	CVImage* image = (CVImage*)this->object;
	CHugeImage *imgFile = image->getHugeImage();

	//get grey scale image
	CHugeImage *Grey;
	if (imgFile->getBands() >= 3)
	{
		//CVImage *GreyImage = project.addImage("Grey Scale",false);
		//CHugeImage *Grey = GreyImage->getHugeImage();
		// if we uncomment above 2 lines & 1 line after four lines from here; and comment the folloing line, then we can see the grey image in barista project 
		Grey = new CHugeImage;
		Grey->prepareWriting("Grey Scale",imgFile->getWidth(), imgFile->getHeight(),8,1);
		CLookupTable lut(8,3); Grey->setLookupTable(&lut);
		OK = imgFile->RGBtoGrey(Grey);
		//baristaProjectHelper.refreshTree();

		if (!OK)
		{			
			QMessageBox::warning( this, "Texture estimation was not successful!", "Conversion to grey scale image failed!");
			return;
		}
	}//if we want to calculate texture for greyscale image then uncomment following codes
	else if (imgFile->getBands() == 1 && imgFile->getBpp() == 8)
		Grey = imgFile;
	else
	{
		QMessageBox::warning( this, "Texture estimation was not successful!", "A grey scale image was not found!");
		return;
	}

	//image file name
	CFileName FileBase (*(image->getHugeImage()->getFileName()));
	CCharString Path = FileBase.GetPath();
	CFileName FN1 = FileBase.GetFileName();
	CCharString FN2 = FN1.GetFileName();
	CCharString Ext = FN1.GetFileExt();
	CCharString del(" ");
	del[0] = CSystemUtilities::dirDelim;

	//entropy image
	FileBase = Path + del + FN2 + "_Entropy";
	CFileName FN = FileBase + "." + Ext;
    CVImage *entropyImg = project.addImage(FN, false);
	FN += ".hug";
	CHugeImage *entropy = entropyImg->getHugeImage();
	entropy->setFileName(FN.GetChar());

	

	//CVImage *GreyImage = project.addImage("Grey Scale",false);
	//CHugeImage *Grey = GreyImage->getHugeImage();
	// if we uncomment above 2 lines & 1 line after four lines from here; and comment the folloing line, then we can see the grey image in barista project 
	CHugeImage *Grey = new CHugeImage;
	Grey->prepareWriting("Grey Scale",imgFile->getWidth(), imgFile->getHeight(),8,1);
	CLookupTable lut(8,3); Grey->setLookupTable(&lut);
	imgFile->RGBtoGrey(Grey);
	//baristaProjectHelper.refreshTree();


	//Edge orientation file
	FileBase = Path + del + FN2 + "_Edge_Orientation";
	FN = FileBase + "." + Ext;
    CVImage *edgeOrientationImg = project.addImage(FN, false);
	FN += ".hug";
	CHugeImage *edgeOrientation = edgeOrientationImg->getHugeImage();
	edgeOrientation->setFileName(FN.GetChar());

	
	//edge image: contains edge ID numbers
	FileBase = Path + del + FN2 + "_EdgeImage";
	FN = FileBase + "." + Ext;
	CVImage *edgeImg = project.addImage(FN, false);
	FN += ".hug";
	CHugeImage *edgeHug = edgeImg->getHugeImage();
	edgeHug->setFileName(FN.GetChar());	


	baristaProjectHelper.setIsBusy(true);
	OK = Grey->computeEntropyAndEdgeOrientation(entropy, edgeOrientation,edgeHug);
	baristaProjectHelper.setIsBusy(false);
	//baristaProjectHelper.refreshTree();
	
	if (!OK)
	{
		project.deleteImage(entropyImg);
		project.deleteImage(edgeOrientationImg);
		project.deleteImage(edgeImg);
		QMessageBox::warning( this, "Texture estimation was not successful!", "Texture estimation was not successful!");
	}
	else
	{
		if (image->hasTFW())
		{
			entropyImg->setTFW(*(image->getTFW()));
			edgeOrientationImg->setTFW(*(image->getTFW()));
			edgeImg->setTFW(*(image->getTFW()));
		}

		baristaProjectHelper.refreshTree();
	}
	*/
}
//<< addition
void CBaristaPopUpHandler::generateNDVI()
{
	CVImage* image = (CVImage*)this->object;

	CFileName FileBase (*(image->getHugeImage()->getFileName()));
	CCharString Path = FileBase.GetPath();
	CFileName FN1 = FileBase.GetFileName();
	CCharString FN2 = FN1.GetFileName();
	CCharString Ext = FN1.GetFileExt();
	CCharString del(" ");
	del[0] = CSystemUtilities::dirDelim;
	FileBase = Path + del + FN2 + "_NDVI";

	CFileName FN = FileBase + "." + Ext;

    CVImage *ndviImg = project.addImage(FN, false);

	FN += ".hug";
	CHugeImage *NDVI = ndviImg->getHugeImage();
	NDVI->setFileName(FN.GetChar());

	CHugeImage *imgFile = image->getHugeImage();
	if ((imgFile->getBpp() == 8) && (imgFile->getBands() == 1))
	{
		baristaProjectHelper.setIsBusy(true);

		bool OK = NDVI->prepareWriting(FN.GetChar(), imgFile->getWidth(), imgFile->getHeight(), 32, 1);

		if (OK)
		{
			CImageBuffer inputbuffer (0, 0, imgFile->getWidth(), imgFile->getHeight(), 1, 8, true);
			OK = imgFile->getRect(inputbuffer);
			if (OK)
			{
				CImageBuffer tile(0, 0, NDVI->getTileWidth(), NDVI->getTileHeight(), 1, 32, true);

				float fact = 200.0f / 255.0f;
				float factSig = 50.0f / 250.0f;

				for(int TR = 0; TR < imgFile->getHeight(); TR += imgFile->getTileHeight())
				{
					for (int TC = 0; TC < imgFile->getWidth(); TC += imgFile->getTileWidth())
					{
						unsigned long idxM0 = inputbuffer.getIndex(TR,TC);
						long cMax = tile.getWidth();
						if (TC + cMax > imgFile->getWidth()) cMax = imgFile->getWidth() - TC;

						for (unsigned long idxM1 = 0; ((idxM0 < inputbuffer.getNGV()) && (idxM1 < tile.getNGV())); idxM0  += inputbuffer.getDiffY(), idxM1 += tile.getDiffY())
						{
							unsigned long idxMMx = idxM1 + tile.getDiffY();
							for (unsigned long idxM = idxM1, c = 0; (c < (unsigned long)(cMax)) && (idxM < idxMMx); idxM  += tile.getDiffX(), c++)
							{
								unsigned char ndviChar = inputbuffer.pixUChar(idxM0 + c);
								float ndviFLT = (ndviChar < 1) ? -101.0 : float(ndviChar) * fact - 100.0;

//								float ndviSIG = 50.0f - float(ndviChar) * factSig; // 5.0f = 250/50
//								tile.pixFlt(idxM) = ndviSIG;
								tile.pixFlt(idxM) = ndviFLT;
							}
						}

						OK &= NDVI->dumpTile(tile);
					}
				}

				OK &= NDVI->finishWriting();
			}
		}

		baristaProjectHelper.setIsBusy(false);
		return;
	}

	FN = FileBase + "_sig." + Ext;

    CVImage *ndviSigmaImg = project.addImage(FN, false);
	FN += ".hug";
	CHugeImage *SigmaNDVI = ndviSigmaImg->getHugeImage();
	SigmaNDVI->setFileName(FN.GetChar());

	baristaProjectHelper.setIsBusy(true);
	bool OK = NDVI->computeVegetationIndex(image->getHugeImage(), SigmaNDVI, true);
	baristaProjectHelper.setIsBusy(false);

	if (!OK)
	{
		project.deleteImage(ndviImg);
		project.deleteImage(ndviSigmaImg);
		QMessageBox::warning( this, "NDVI was not successful!", "NDVI was not successful!");
	}
	else
	{
		if (image->hasTFW())
		{
			ndviImg->setTFW(*(image->getTFW()));
			ndviSigmaImg->setTFW(*(image->getTFW()));
		}

		baristaProjectHelper.refreshTree();
	}
/*
	QMessageBox::warning( this, "NDVI processing not yet implemented!", "NDVI processing not yet implemented!");


	CHugeImage R, G, B, I, RGBI;
	R.open("F:\\Projects\\EuroSDR\\Lyngby\\OrthoR_lyngby.tif.hug");
	G.open("F:\\Projects\\EuroSDR\\Lyngby\\OrthoG_lyngby.tif.hug");
	B.open("F:\\Projects\\EuroSDR\\Lyngby\\OrthoB_lyngby.tif.hug");
	I.open("F:\\Projects\\EuroSDR\\Lyngby\\OrthoIR_lyngby.tif.hug");

	const char *fn = "F:\\Projects\\EuroSDR\\Lyngby\\OrthoLyngby.tif.hug";

	RGBI.mergeImageBands(fn, &R, &G, &B, &I);
*/
};
void CBaristaPopUpHandler::changeDetection()
{
    if (this->object->isa("CVImage"))
    {
		CVImage* image = (CVImage*)object;
		bui_BuildingChangeDialog dlg(image, true);
		dlg.exec();
	}
}
void CBaristaPopUpHandler::evaluateDetection()
{
    if (this->object->isa("CVImage"))
    {
		CVImage* image = (CVImage*)object;
		bui_BuildingChangeDialog dlg(image, false);
		dlg.exec();
	}
}

void CBaristaPopUpHandler::cooccurrence()
{
    if (this->object->isa("CVImage"))
    {
		CVImage* image = (CVImage*)object;
		bui_CooccurrenceDialog dlg(image);
		dlg.exec();
	}
}

void CBaristaPopUpHandler::BuildingsDetection()
{
        CVALS* als = (CVALS*)object;
		CALSDataSet *alsdata = als->getCALSData();
		bui_BuildingsDetector buildings(als);
		buildings.exec();
}

void CBaristaPopUpHandler::generateLabelImage()
{
    if (this->object->isa("CVImage"))
    {
		CVImage* image = (CVImage*)object;
		CHugeImage *imgFile = image->getHugeImage();
		if (imgFile && (imgFile->getBpp() <= 8) && (imgFile->getBands() == 1))
		{
			CFileName FileBase (*(imgFile->getFileName()));
			CCharString Path = FileBase.GetPath();
			CFileName FN1 = FileBase.GetFileName();
			CCharString FN2 = FN1.GetFileName();
			CCharString Ext = FN1.GetFileExt();
			CCharString del(" ");
			del[0] = CSystemUtilities::dirDelim;
			FileBase = Path + del + FN2 + "_LABELS";
			CFileName FN = FileBase + "." + Ext;

			CVImage *labelImg = project.addImage(FN, false);
			FN += ".hug";
			CHugeImage *labelImgHug = labelImg->getHugeImage();

			baristaProjectHelper.setIsBusy(true);

			CImageBuffer inputbuffer (0, 0, imgFile->getWidth(), imgFile->getHeight(), 1, imgFile->getBpp(), true);

			bool OK = imgFile->getRect(inputbuffer);

			if (OK)
			{
				inputbuffer.setMargin(1,0);
				CLabelImage img(inputbuffer, 1.0, 4);
				OK = labelImgHug->writeLabelImage(FN.GetChar(), img);
			}

			baristaProjectHelper.setIsBusy(false);

			if (!OK)
			{
				project.deleteImage(labelImg);
				QMessageBox::warning( this, "Generation of label image failed!", "NDVI was not successful!");
			}
			else
			{
				if (image->hasTFW())
				{
					labelImg->setTFW(*(image->getTFW()));
				}

				baristaProjectHelper.refreshTree();
			}
		}
	}
}

void CBaristaPopUpHandler::RenameObject()
{
    if (this->object->isa("CVImage"))
    {
		CVImage* rename = (CVImage*)object;
		bui_LabelChangeDlg labeldlg(rename->getName(), "New Image Name");
		labeldlg.setModal(true);
		labeldlg.exec();

		CCharString newname = labeldlg.getLabelText();
		project.getUniqueImageName(newname);
		if (!project.renameImage(rename, newname.GetChar()))
			return;
    }
    else if (this->object->isa("CXYPointArray"))
    {
		CXYPointArray* rename = (CXYPointArray*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New XYPoints Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
	}
	else if (this->object->isa("CRPC"))
    {
		CRPC* rename = (CRPC*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New RPC Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
    }
	else if (this->object->isa("CPushBroomScanner"))
    {
		CPushBroomScanner* rename = (CPushBroomScanner*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New PushBroom Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
    }
    else if (this->object->isa("CAffine"))
    {
		CAffine* rename = (CAffine*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New Affine Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
	}
    else if (this->object->isa("CTFW"))
    {
		CTFW* rename = (CTFW*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New TFW Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
	}
	else if (this->object->isa("CXYContourArray"))
    {
		CXYContourArray* rename = (CXYContourArray*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New XYContours Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
	}
	else if (this->object->isa("CXYPolyLineArray"))
    {
		CXYPolyLineArray* rename = (CXYPolyLineArray*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New XYPolyLines Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
	}
    else if (this->object->isa("CVDEM"))
    {
		CVDEM* rename = (CVDEM*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New DEM Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();

		if (!project.renameDEM(rename, newname.GetChar()))
			return;
	}
    else if (this->object->isa("CVALS"))
    {
		CVALS* rename = (CVALS*) object;
		bui_LabelChangeDlg labeldlg(rename->getFileName().GetFullFileName().GetChar(), "New ALS Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();

		if (!project.renameALS(rename, newname.GetChar())) return;
	}
    else if (this->object->isa("CXYZPointArray"))
    {
		CXYZPointArray* rename = (CXYZPointArray*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New XYZPoints Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();

		if (!project.renameXYZPointArray(rename, newname.GetChar()))
			return;
	}
    else if (this->object->isa("CXYZPolyLineArray"))
    {
		CXYZPolyLineArray* rename = (CXYZPolyLineArray*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New XYZPolyLines Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
	}
    else if (this->object->isa("CBuildingArray"))
    {
#ifdef WIN32    	
		CBuildingArray* rename = (CBuildingArray*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New Buildings Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
#endif		
	}
    else if (this->object->isa("CXYEllipseArray"))
    {
		CXYEllipseArray* rename = (CXYEllipseArray*)object;
		bui_LabelChangeDlg labeldlg(rename->getFileName()->GetFullFileName().GetChar(), "New XYEllipses Name");
		labeldlg.setModal(true);
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		rename->setFileName(newname.GetChar());
	}
    else if (this->object->isa("COrbitObservations"))
    {
		COrbitObservations* rename = (COrbitObservations*)object;

		if (rename->getSplineType() == Path)
		{
			bui_LabelChangeDlg labeldlg(rename->getName());
			labeldlg.exec();
			CCharString newname = labeldlg.getLabelText();
			project.getUniqueOrbitPathName(newname);
			if (!project.renameOrbitObservation(rename, newname.GetChar()))
				return;
		}
		else if (rename->getSplineType() == Attitude)
		{
			bui_LabelChangeDlg labeldlg(rename->getName());
			labeldlg.exec();
			CCharString newname = labeldlg.getLabelText();
			project.getUniqueOrbitAttitudeName(newname);
			if (!project.renameOrbitObservation(rename, newname.GetChar()))
				return;
		}

	}
    else if ((this->object->isa("CCamera"))      || (this->object->isa("CCCDLine")) || 
	         (this->object->isa("CFrameCamera")) || (this->object->isa("CFrameCameraAnalogue")))
    {
		CCamera* rename = (CCamera*)object;

		bui_LabelChangeDlg labeldlg(rename->getName());
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		project.getUniqueCameraName(newname);

		if (!project.renameCamera(rename, newname.GetChar()))
			return;


	}
    else if (this->object->isa("CCameraMounting"))
    {
		CCameraMounting* rename = (CCameraMounting*)object;

		bui_LabelChangeDlg labeldlg(rename->getName());
		labeldlg.exec();
		CCharString newname = labeldlg.getLabelText();
		project.getUniqueCameraMountingName(newname);
		if (!project.renameCameraMounting(rename, newname.GetChar()))
			return;


	}

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::DeleteObject()
{
	CCharStringArray maybeonworkspacelist;

	CCharString maybeonworkspace("");

	CVImage *ImageToDelete = 0;

	CXYZPointArray* xyztokill = 0;

	CVDEM* demToDelete =0;

    if (this->object->isa("CVImage"))
    {
		CVImage* image = (CVImage*)object;

		ImageToDelete = image;

		maybeonworkspace = image->getFileNameChar();
		maybeonworkspacelist.Add(maybeonworkspace);

		image->getXYContours()->RemoveAll();

		// we have to loop over the children of the image, which might
		// have an open window on the workspace
        //CXYPointArray xypoints;
        //CXYPointArray projectedxypoints;
        //CXYPointArray differencedxypoints;
        //CXYPointArray residuals;
        //CXYZPointArray xyzpoints;
        //CRPC rpc;
        //CAffine affine;
        //CTFW tfw;
		//CXYContours xycontours;
		//CXYEllipses ellipses;

	    CCharString childwindowname("");
	    childwindowname = *(image->getXYPoints()->getFileName());
		maybeonworkspacelist.Add(childwindowname);

	    childwindowname = *(image->getProjectedXYPoints()->getFileName());
		maybeonworkspacelist.Add(childwindowname);

	    childwindowname = *(image->getDifferencedXYPoints()->getFileName());
		maybeonworkspacelist.Add(childwindowname);

		childwindowname = *(image->getResiduals()->getFileName());
		maybeonworkspacelist.Add(childwindowname);


	    childwindowname = image->rpcFileName();
		maybeonworkspacelist.Add(childwindowname);

	    childwindowname = image->affineFileName();
		maybeonworkspacelist.Add(childwindowname);

	    childwindowname = image->tfwFileName();
		maybeonworkspacelist.Add(childwindowname);

		childwindowname = image->pushBroomScannerFileName();
		maybeonworkspacelist.Add(childwindowname);

		childwindowname = *(image->getXYContours()->getFileName());
		maybeonworkspacelist.Add(childwindowname);

		childwindowname = *(image->getEllipses()->getFileName());
		maybeonworkspacelist.Add(childwindowname);
    }
    else if (object->isa("CXYPointArray"))
    {
		CBaristaIconViewItem* item = (CBaristaIconViewItem*)this->treewidgetitem->parent();

		if ( item->getObject()->isa("CVImage"))
		{
			CVImage* image = (CVImage*)item->getObject();

			CXYPointArray* xypoints = (CXYPointArray*)object;
			maybeonworkspace = *(xypoints->getFileName());
			maybeonworkspacelist.Add(maybeonworkspace);

			xypoints->RemoveAll();

			if ( image->getXYPoints()->GetSize() == 0 && image->getResiduals()->GetSize() != 0)
			{
				maybeonworkspace = *(image->getResiduals()->getFileName());
				maybeonworkspacelist.Add(maybeonworkspace);

				image->getResiduals()->RemoveAll();
			}
		}
		else if ( item->getObject()->isa("CXYPointArray"))
		{
			CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)item->parent();
			if ( imageitem->getObject()->isa("CVImage"))
			{
				CVImage* image = (CVImage*)imageitem->getObject();

				CXYPointArray* xypoints = image->getResiduals();
				maybeonworkspace =  *(xypoints->getFileName());
				maybeonworkspacelist.Add(maybeonworkspace);

				xypoints->RemoveAll();
			}
		}
		else if ( item->getObject()->isa("CVDEM"))
		{
			CVDEM* vdem = (CVDEM*)item->getObject();

			CXYPointArray* xypoints = vdem->getMonoPlottedPoints();
			maybeonworkspace =  *(xypoints->getFileName());
			maybeonworkspacelist.Add(maybeonworkspace);
			xypoints->RemoveAll();
		}


	}
	else if (object->isa("CRPC"))
    {
		CRPC* rpc = (CRPC*)object;
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		CVImage* image = (CVImage*)imageitem->getObject();

		project.deleteSensorModelofImage(image, rpc);

		maybeonworkspace = image->rpcFileName();
		maybeonworkspacelist.Add(maybeonworkspace);
    }
	else if (object->isa("CPushBroomScanner"))
    {
		CPushBroomScanner* pushbroomscanner = (CPushBroomScanner*)object;
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		CVImage* image = (CVImage*)imageitem->getObject();


		project.deleteSensorModelofImage(image, pushbroomscanner);

		maybeonworkspace = image->pushBroomScannerFileName();
		maybeonworkspacelist.Add(maybeonworkspace);
    }
    else if (object->isa("CAffine"))
    {
		CAffine* affine = (CAffine*)object;
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		CVImage* image = (CVImage*)imageitem->getObject();

		project.deleteSensorModelofImage(image, affine);

		maybeonworkspace = image->affineFileName();
		maybeonworkspacelist.Add(maybeonworkspace);
	}
    else if (object->isa("CTFW"))
    {
		CTFW* tfw = (CTFW*)object;
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		if (imageitem->type() == eImageCaption)
		{
			CVImage* image = (CVImage*)imageitem->getObject();
			project.deleteSensorModelofImage(image, tfw);
			maybeonworkspace = image->tfwFileName();
			maybeonworkspacelist.Add(maybeonworkspace);
		}
		else if (imageitem->type() == eDEM)
		{
			CVDEM* vdem = (CVDEM*)imageitem->getObject();
			maybeonworkspace = tfw->getFileNameChar();
			maybeonworkspacelist.Add(maybeonworkspace);
			project.deleteSensorModelofDEM(vdem, tfw);

		}


	}
	else if (object->isa("CXYContourArray"))
    {
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		CVImage* image = (CVImage*)imageitem->getObject();

		CXYContourArray* xycontours = (CXYContourArray*)object;
		maybeonworkspace = *(xycontours->getFileName());
		maybeonworkspacelist.Add(maybeonworkspace);
		xycontours->RemoveAll();
	}
	else if (object->isa("CXYPolyLineArray"))
    {
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		CVImage* image = (CVImage*)imageitem->getObject();

		CXYPolyLineArray* xyedges = (CXYPolyLineArray*) object;
		maybeonworkspace = *(xyedges->getFileName());
		maybeonworkspacelist.Add(maybeonworkspace);
		xyedges->RemoveAll();
	}
    else if (object->isa("CVDEM"))
    {

		CVDEM* vdem = (CVDEM*)object;
		demToDelete = vdem;

		// the view
		maybeonworkspace = vdem->getFileName()->GetFullFileName();
		maybeonworkspacelist.Add(maybeonworkspace);

		// the demtable
		maybeonworkspace = *(vdem->getFileName());
		maybeonworkspacelist.Add(maybeonworkspace);

		// the tfw node
		maybeonworkspace = vdem->getDEM()->getTFW()->getFileNameChar();
		maybeonworkspacelist.Add(maybeonworkspace);

    }
    else if (object->isa("CVALS"))
    {
		CVALS* als = (CVALS*) object;
		maybeonworkspace = als->getFileNameChar();
		maybeonworkspacelist.Add(maybeonworkspace);
		project.deleteALS(als);
    }
    else if (object->isa("CXYZPointArray"))
    {
		CBaristaIconViewItem* parentItem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		if (parentItem && parentItem->getObject() && parentItem->getObject()->isa("CXYZPointArray"))
		{
			CXYZPointArray* parentArray = (CXYZPointArray*)parentItem->getObject();
			xyztokill = project.getPointResidualArrayOfXYZPointArray(parentArray);
			if (xyztokill)
			{
				maybeonworkspace = *(xyztokill->getFileName());
				maybeonworkspacelist.Add(maybeonworkspace);
				xyztokill->RemoveAll();
			}
		}
		else
		{
			CXYZPointArray* points = (CXYZPointArray*)object;
			maybeonworkspace = *(points->getFileName());
			maybeonworkspacelist.Add(maybeonworkspace);
			xyztokill = points;
		}
	}
    else if (object->isa("CXYZPolyLineArray"))
    {
		CXYZPolyLineArray* lines = (CXYZPolyLineArray*)object;
		maybeonworkspace = *(lines->getFileName());
		maybeonworkspacelist.Add(maybeonworkspace);
		project.deleteXYZPolyLines();
	}
    else if (object->isa("CBuildingArray"))
    {
#ifdef WIN32    	
		CBuildingArray* buildings = (CBuildingArray*)object;
		maybeonworkspace = *(buildings->getFileName());
		maybeonworkspacelist.Add(maybeonworkspace);
		project.deleteBuildings();
#endif		
	}
    else if (object->isa("CXYEllipseArray"))
    {
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		CVImage* image = (CVImage*)imageitem->getObject();
		maybeonworkspace = *(image->getEllipses()->getFileName());
		maybeonworkspacelist.Add(maybeonworkspace);
		image->getEllipses()->RemoveAll();
	}
	else if (object->isa("CFrameCameraModel"))
    {
		CFrameCameraModel* frameCameraModel = (CFrameCameraModel*)object;
		CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
		CVImage* image = (CVImage*)imageitem->getObject();


		project.deleteSensorModelofImage(image, frameCameraModel);

		maybeonworkspace = image->pushBroomScannerFileName();
		maybeonworkspacelist.Add(maybeonworkspace);
    }


	int onworkspace = workspace->subWindowList().count();
	int howmany = maybeonworkspacelist.GetSize();

	// close open window(s) on workspace of deleted object
	for ( int j = 0; j < maybeonworkspacelist.GetSize(); j++)
	{
		CCharString* tosearch = maybeonworkspacelist.GetAt(j);

		for ( int i = workspace->subWindowList().count()-1; i >= 0; i--)
		{
			//const char* name = workspace->subWindowList().at(i)->name();
			QString qname = workspace->subWindowList().at(i)->accessibleName();

			QByteArray qba = qname.toLatin1();
			const char* name = qba.constData();

			if (tosearch->CompareNoCase(name))
			{
				QWidget* widget = workspace->subWindowList().at(i);
				widget->close();
			}
		}

		// catch all object selector table dialogs and delete as well
		QList<Bui_ObjectTableDlg*> dialogs = workspace->findChildren<Bui_ObjectTableDlg*>();
		for (int i=0; i < dialogs.size(); i++)
		{
			Bui_ObjectTableDlg* dlg = dialogs.at(i);
			QString name = dlg->accessibleName();
			if (tosearch->CompareNoCase(name.toLatin1().constData()))
				delete dlg;
		}

		
	}





	// update the open windows on the workspace
	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}

	if (ImageToDelete)
	{
		project.deleteImage(ImageToDelete);
	}
	if (xyztokill)
	{
		project.deleteXYZPoints(xyztokill);
		project.deleteMonoplottedPoints(xyztokill);
		project.deleteGeoreferencedPoints(xyztokill);
	}

	if 	(demToDelete)
	{
		project.deleteDEM(demToDelete);
	}

	workspace->tileSubWindows();
	baristaProjectHelper.refreshTree();
}


void CBaristaPopUpHandler::createXYPointArrayPopUpMenu(xypointtype type)
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;
	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
    CVImage* image = (CVImage*) imageitem->getObject();



    makeImagePointsActiveAction = new QAction(tr("&Set/Unset Active"), this);
    connect(makeImagePointsActiveAction, SIGNAL(triggered()), this, SLOT(makeImagePointsActive()));
	this->addAction(makeImagePointsActiveAction);


	if (type == MEASURED)
	{
		ChangeSigmaXYAction = new QAction(tr("&Change Sigma"), this);
		connect(ChangeSigmaXYAction, SIGNAL(triggered()), this, SLOT(ChangeSigmaXY()));
		this->addAction(ChangeSigmaXYAction);
	}

    DifferencebyLabelXYAction = new QAction(tr("&Difference by Label"), this);
    connect(DifferencebyLabelXYAction, SIGNAL(triggered()), this, SLOT(DifferencebyLabelXY()));
	this->addAction(DifferencebyLabelXYAction);

    DifferencebyIndexXYAction = new QAction(tr("&Difference by Index"), this);
    connect(DifferencebyIndexXYAction, SIGNAL(triggered()), this, SLOT(DifferencebyIndexXY()));
	this->addAction(DifferencebyIndexXYAction);


	if (type == MEASURED || type ==  MONOPLOTTED  )
	{
		this->addSeparator();
		xyPointArraySelectorAction = new QAction("Open Point Selecor",this);
		this->connect(xyPointArraySelectorAction,SIGNAL(triggered()),this,SLOT(pointArraySelector()));
		this->addAction(xyPointArraySelectorAction);

		this->addSeparator();
	}
	if ((type == MEASURED || type == MONOPLOTTED) && image)
	{
		if (image->hasSensorModel(true))
		{
			this->addSeparator();

			submenuOne = new QMenu(this );

			if (image->hasRPC())
			{
				monoplotxyRPCAction = new QAction(tr("&with RPC"), this);
				connect(monoplotxyRPCAction, SIGNAL(triggered()), this, SLOT(monoplotxyRPC()));
				submenuOne->addAction(monoplotxyRPCAction);
			}

			if (image->hasAffine())
			{
				monoplotxyAffineAction = new QAction(tr("&with Affine"), this);
				connect(monoplotxyAffineAction, SIGNAL(triggered()), this, SLOT(monoplotxyAffine()));
				submenuOne->addAction(monoplotxyAffineAction);
			}

			if (image->hasPushbroom())
			{
				monoplotxyPushBroomAction = new QAction(tr("&with PushBroomScanner"), this);
				connect(monoplotxyPushBroomAction, SIGNAL(triggered()), this, SLOT(monoplotxyPushBroom()));
				submenuOne->addAction(monoplotxyPushBroomAction);
			}

			if (image->hasTFW())
			{
				monoplotxyTFWAction = new QAction(tr("&with TFW"), this);
				connect(monoplotxyTFWAction, SIGNAL(triggered()), this, SLOT(monoplotxyTFW()));
				submenuOne->addAction(monoplotxyTFWAction);
			}

			if (image->getCurrentSensorModel())
			{
				monoplotxyCurrentSensorModelAction = new QAction(tr("&with Current Sensor Model"), this);
				connect(monoplotxyCurrentSensorModelAction, SIGNAL(triggered()), this, SLOT(monoplotxyCurrentSensorModel()));
				submenuOne->addAction(monoplotxyCurrentSensorModelAction);
			}

			submenuOne->setTitle("&Monoplot 2D Points");
			this->addMenu(submenuOne);
		}
	}



	if (type == MEASURED || type == MONOPLOTTED)
	{

		this->addSeparator();

		submenuTwo = new QMenu(this );


		georeferencingTFWAction = new QAction(tr("&with TFW file"), this);
		connect(georeferencingTFWAction, SIGNAL(triggered()), this, SLOT(georeferencingTFW()));
		submenuTwo->addAction(georeferencingTFWAction);

		georeferencing2DTrafoAction = new QAction(tr("&with 2D Trafo"), this);
		connect(georeferencing2DTrafoAction, SIGNAL(triggered()), this, SLOT(georeferencing2DTrafo()));
		submenuTwo->addAction(georeferencing2DTrafoAction);

		submenuTwo->setTitle("&Georeference 2D Points");

		this->addMenu(submenuTwo);
	}

	this->addSeparator();

#ifndef FINAL_RELEASE
    writeImagePointChipsAction = new QAction(tr("&Write Image Point Chips"), this);
    connect(writeImagePointChipsAction, SIGNAL(triggered()), this, SLOT(writeImagePointChips()));
	this->addAction(writeImagePointChipsAction);

	this->addSeparator();
#endif
    export2DSpacedTextFileAction = new QAction(tr("&Export Space Separated TextFile"), this);
    connect(export2DSpacedTextFileAction, SIGNAL(triggered()), this, SLOT(export2DSpacedTextFile()));
	this->addAction(export2DSpacedTextFileAction);

    export2DCSVTextFileAction = new QAction(tr("&Export Comma Separated TextFile"), this);
    connect(export2DCSVTextFileAction, SIGNAL(triggered()), this, SLOT(export2DCSVTextFile()));
	this->addAction(export2DCSVTextFileAction);


}

void CBaristaPopUpHandler::createRPCPopUpMenu()
{
	submenuOne = new QMenu(this );

    SetRPCCurrentModelAction = new QAction(tr("&Set RPCs to current sensor model"), this);
    connect(SetRPCCurrentModelAction, SIGNAL(triggered()), this, SLOT(SetRPCCurrentModel()));
	this->addAction(SetRPCCurrentModelAction);

	this->addSeparator();

    SingleRPCBiasCorrectionNoneAction = new QAction(tr("&None"), this);
    connect(SingleRPCBiasCorrectionNoneAction, SIGNAL(triggered()), this, SLOT(SingleRPCBiasCorrectionNone()));
	submenuOne->addAction(SingleRPCBiasCorrectionNoneAction);

    SingleRPCBiasCorrectionShiftAction = new QAction(tr("&Shift"), this);
    connect(SingleRPCBiasCorrectionShiftAction, SIGNAL(triggered()), this, SLOT(SingleRPCBiasCorrectionShift()));
	submenuOne->addAction(SingleRPCBiasCorrectionShiftAction);

    SingleRPCBiasCorrectionShiftDriftAction = new QAction(tr("&Shift + drift"), this);
    connect(SingleRPCBiasCorrectionShiftDriftAction, SIGNAL(triggered()), this, SLOT(SingleRPCBiasCorrectionShiftDrift()));
	submenuOne->addAction(SingleRPCBiasCorrectionShiftDriftAction);

    SingleRPCBiasCorrectionAffineAction = new QAction(tr("&Affine"), this);
    connect(SingleRPCBiasCorrectionAffineAction, SIGNAL(triggered()), this, SLOT(SingleRPCBiasCorrectionAffine()));
	submenuOne->addAction(SingleRPCBiasCorrectionAffineAction);

	submenuOne->setTitle("&Set Bias Correction Parameters");

	this->addMenu(submenuOne);

	this->addSeparator();

	SingleRPCRegenerateBiasCorrectedAction = new QAction(tr("&Regenerate bias-corrected RPCs"), this);
    connect(SingleRPCRegenerateBiasCorrectedAction, SIGNAL(triggered()), this, SLOT(SingleRPCRegenerateBiasCorrected()));
	this->addAction(SingleRPCRegenerateBiasCorrectedAction);

	this->addSeparator();
    exportIkonosRPCsAction = new QAction(tr("&Export Ikonos Format"), this);
    connect(exportIkonosRPCsAction, SIGNAL(triggered()), this, SLOT(exportIkonosRPCs()));
	this->addAction(exportIkonosRPCsAction);

    exportQuickBirdRPCsAction = new QAction(tr("&Export QuickBird Format"), this);
    connect(exportQuickBirdRPCsAction, SIGNAL(triggered()), this, SLOT(exportQuickBirdRPCs()));
	this->addAction(exportQuickBirdRPCsAction);

    exportSocetRPCsAction = new QAction(tr("&Export Socet Set Format"), this);
    connect(exportSocetRPCsAction, SIGNAL(triggered()), this, SLOT(exportSocetRPCs()));
	this->addAction(exportSocetRPCsAction);

    exportCSVRPCsAction = new QAction(tr("&Export CSV"), this);
    connect(exportCSVRPCsAction, SIGNAL(triggered()), this, SLOT(exportCSVRPCs()));
	this->addAction(exportCSVRPCsAction);	
}

void CBaristaPopUpHandler::createAffinePopUpMenu()
{
    SetAffineCurrentModelAction = new QAction(tr("&Set Affine to current sensor model"), this);
    connect(SetAffineCurrentModelAction, SIGNAL(triggered()), this, SLOT(SetAffineCurrentModel()));
	this->addAction(SetAffineCurrentModelAction);

	if (this->setReferenceInformationAction) delete this->setReferenceInformationAction;
    setReferenceInformationAction = new QAction(tr("&Set Affine Reference System"), this);
    connect(setReferenceInformationAction, SIGNAL(triggered()), this, SLOT(setReferenceInformation()));
	this->addAction(setReferenceInformationAction);

	this->addSeparator();

    exportAffineTextFileAction = new QAction(tr("&Export Affine Parameters"), this);
    connect(exportAffineTextFileAction, SIGNAL(triggered()), this, SLOT(exportAffineTextFile()));
	this->addAction(exportAffineTextFileAction);
}

void CBaristaPopUpHandler::createPushBroomScannerPopUpMenu(CPushBroomScanner* scanner)
{
    SetPushBroomScannerCurrentModelAction = new QAction(tr("&Set PushBroomScanner to current sensor model"), this);
    connect(SetPushBroomScannerCurrentModelAction, SIGNAL(triggered()), this, SLOT(SetPushBroomScannerCurrentModel()));
	this->addAction(SetPushBroomScannerCurrentModelAction);

	this->addSeparator();

	ExportPushBroomScannerRPCAction = new QAction(tr("Generate RPCs"), this);
    connect(ExportPushBroomScannerRPCAction, SIGNAL(triggered()), this, SLOT(ExportPushBroomScannerRPC()));
	this->addAction(ExportPushBroomScannerRPCAction);

	MergeOrbitsAction = new QAction(tr("Merge Orbits"), this);
    connect(MergeOrbitsAction, SIGNAL(triggered()), this, SLOT(mergeOrbits()));
	this->addAction(MergeOrbitsAction);

}



void CBaristaPopUpHandler::createFrameCameraPopUpMenu(CFrameCameraModel* frameCamera)
{
    SetFrameCameraCurrentModelAction = new QAction(tr("&Set Frame Camera to current sensor model"), this);
    connect(SetFrameCameraCurrentModelAction, SIGNAL(triggered()), this, SLOT(SetFrameCameraCurrentModel()));
	this->addAction(SetFrameCameraCurrentModelAction);

    ExportFrameCameraProjectionMatrixAction = new QAction(tr("&Export Projection Matrix"), this);
    connect(ExportFrameCameraProjectionMatrixAction, SIGNAL(triggered()), this, SLOT(ExportFrameCameraProjectionMatrix()));
	this->addAction(ExportFrameCameraProjectionMatrixAction);

    ExportFrameCameraAction = new QAction(tr("&Export Frame Camera"), this);
    connect(ExportFrameCameraAction, SIGNAL(triggered()), this, SLOT(ExportFrameCamera()));
	this->addAction(ExportFrameCameraAction);

#ifndef FINAL_RELEASE
	this->submenuOne = new QMenu(this );
			
	ImportFrameCameraOrientationAction1 = new QAction(tr("&Do not rotate by 180 deg"), this);
	connect(ImportFrameCameraOrientationAction1, SIGNAL(triggered()), this, SLOT(ImportFrameCameraOrientation1()));
	submenuOne->addAction(ImportFrameCameraOrientationAction1);

	ImportFrameCameraOrientationAction2 = new QAction(tr("&Rotate by 180 deg"), this);
	connect(ImportFrameCameraOrientationAction2, SIGNAL(triggered()), this, SLOT(ImportFrameCameraOrientation2()));
	submenuOne->addAction(ImportFrameCameraOrientationAction2);
			
	submenuOne->setTitle("&Import Exterior Orientation");
	this->addMenu(submenuOne);
#endif

	FrameCameraSetRefsysAction = new QAction(tr("&Set Reference System"), this);
    connect(FrameCameraSetRefsysAction, SIGNAL(triggered()), this, SLOT(FrameCameraSetRefsys()));
	this->addAction(FrameCameraSetRefsysAction);

}


void CBaristaPopUpHandler::createTFWPopUpMenu()
{
	if (this->setReferenceInformationAction) delete this->setReferenceInformationAction;
    setReferenceInformationAction = new QAction(tr("&Set TFW Reference System"), this);
    connect(setReferenceInformationAction, SIGNAL(triggered()), this, SLOT(setReferenceInformation()));
	this->addAction(setReferenceInformationAction);

    exportTFWTextFileAction = new QAction(tr("&Export TFW Parameters"), this);
    connect(exportTFWTextFileAction, SIGNAL(triggered()), this, SLOT(exportTFWTextFile()));
	this->addAction(exportTFWTextFileAction);

}

void CBaristaPopUpHandler::createVDEMPopUpMenu()
{
	if (this->setReferenceInformationAction) delete this->setReferenceInformationAction;
    setReferenceInformationAction = new QAction(tr("&Set DEM Reference System"), this);
    connect(setReferenceInformationAction, SIGNAL(triggered()), this, SLOT(setReferenceInformation()));
	this->addAction(setReferenceInformationAction);

    setDEMActiveAction = new QAction(tr("&Set Active DEM"), this);
    connect(setDEMActiveAction, SIGNAL(triggered()), this, SLOT(setDEMActive()));
	this->addAction(setDEMActiveAction);

    unsetDEMActiveAction = new QAction(tr("&Unset Active DEM"), this);
    connect(unsetDEMActiveAction, SIGNAL(triggered()), this, SLOT(unsetDEMActive()));
	this->addAction(unsetDEMActiveAction);

	this->addSeparator();

    interpolateHeightfromDEMAction = new QAction(tr("&Get interpolated height"), this);
    connect(interpolateHeightfromDEMAction, SIGNAL(triggered()), this, SLOT(interpolateHeightfromDEM()));
	this->addAction(interpolateHeightfromDEMAction);

    interpolateHeightsfromDEMAction = new QAction(tr("&Get multiple interpolated heights"), this);
    connect(interpolateHeightsfromDEMAction, SIGNAL(triggered()), this, SLOT(interpolateHeightsfromDEM()));
	this->addAction(interpolateHeightsfromDEMAction);

	this->addSeparator();

    displayDEMPropertiesAction = new QAction(tr("&Display DEM Properties"), this);
    connect(displayDEMPropertiesAction, SIGNAL(triggered()), this, SLOT(DisplayDEMProperties()));
	this->addAction(displayDEMPropertiesAction);


    displayDEMStatisticsAction = new QAction(tr("&Compute DEM Statistics ..."), this);
    connect(displayDEMStatisticsAction, SIGNAL(triggered()), this, SLOT(DisplayDEMStatistcs()));
	this->addAction(displayDEMStatisticsAction);

	CVDEM* vdem = (CVDEM*) this->object;
	if (vdem->getDEMStatisticts().GetSize())
	{
		deleteDEMStatisticsAction = new QAction(tr("&Delete DEM Statistics ..."), this);
		connect(deleteDEMStatisticsAction, SIGNAL(triggered()), this, SLOT(DeleteDEMStatistcs()));
		this->addAction(deleteDEMStatisticsAction);
	}


	this->addSeparator();
    transformDEMAction = new QAction(tr("&Transform DEM"), this);
    connect(transformDEMAction, SIGNAL(triggered()), this, SLOT(transformDEM()));
	this->addAction(transformDEMAction);

	DEMoperationAction = new QAction(tr("&DEM Operation"), this);
	connect(DEMoperationAction, SIGNAL(triggered()), this, SLOT(demOperation()));
	this->addAction(DEMoperationAction);

	this->addSeparator();
    exportASCIIDEMAction = new QAction(tr("&Write ASCII DEM"), this);
    connect(exportASCIIDEMAction, SIGNAL(triggered()), this, SLOT(exportASCIIDEM()));
	this->addAction(exportASCIIDEMAction);

    exportImageDEMAction = new QAction(tr("&Write Image DEM"), this);
    connect(exportImageDEMAction, SIGNAL(triggered()), this, SLOT(exportImageDEM()));
	this->addAction(exportImageDEMAction);

    exportASCIIDEMVerticalFlipAction = new QAction(tr("&Write vertically flipped ASCII DEM"), this);
    connect(exportASCIIDEMVerticalFlipAction, SIGNAL(triggered()), this, SLOT(exportASCIIDEMVerticalFlip()));
	this->addAction(exportASCIIDEMVerticalFlipAction);

#ifndef FINAL_RELEASE
	this->addSeparator();

    detectBuildingsAction = new QAction(tr("&Detect Buildings"), this);
    connect(detectBuildingsAction, SIGNAL(triggered()), this, SLOT(detectBuildings()));
	this->addAction(detectBuildingsAction);
#endif


}

void CBaristaPopUpHandler::createVALSPopUpMenu()
{
	if (this->setReferenceInformationAction) delete this->setReferenceInformationAction;
    setReferenceInformationAction = new QAction(tr("&Set ALS Reference System"), this);
    connect(setReferenceInformationAction, SIGNAL(triggered()), this, SLOT(setReferenceInformation()));
	this->addAction(setReferenceInformationAction);
	this->addSeparator();

    interpolateDEMfromALSAction = new QAction(tr("Interpolate &DEM"), this);
    connect(interpolateDEMfromALSAction, SIGNAL(triggered()), this, SLOT(interpolateDEMfromALS()));
	this->addAction(interpolateDEMfromALSAction);

    interpolateIntensityFromALSAction = new QAction(tr("Interpolate &Intensity"), this);
    connect(interpolateIntensityFromALSAction, SIGNAL(triggered()), this, SLOT(interpolateIntensityFromALS()));
	this->addAction(interpolateIntensityFromALSAction);

	this->mergeALSAction = new QAction(tr("Merge ALS data sets"), this);
    connect(mergeALSAction, SIGNAL(triggered()), this, SLOT(mergeALS()));
	this->addAction(mergeALSAction);

#ifndef FINAL_RELEASE
	this->filterALSAction  = new QAction(tr("Filter ALS data"), this);
	connect(this->filterALSAction, SIGNAL(triggered()), this, SLOT(filterALS()));
	this->addAction(this->filterALSAction);
#endif
	this->addSeparator();

	this->mergeALSAction = new QAction(tr("Buildings Detection"), this);
    connect(mergeALSAction, SIGNAL(triggered()), this, SLOT(BuildingsDetection()));
	this->addAction(mergeALSAction);

	this->addSeparator();

	this->exportALSAction = new QAction(tr("Export ALS"), this);
    connect(exportALSAction, SIGNAL(triggered()), this, SLOT(exportALS()));
	this->addAction(exportALSAction);
	
}

void CBaristaPopUpHandler::interpolateDEMfromALS()
{
	CVALS* als = (CVALS*)object;
	CALSDataSet *alsdata = als->getCALSData();

	bui_ALSInterpolationDialog dlg(als, "Interpolate DEM from ALS Data", "UserGuide/ALSDEMInterpolate.html", true);
	dlg.exec();

	if (dlg.getSuccess())
	{
		CFileName fn = dlg.getFileName();
		CVDEM *vdem = project.addDEM(fn);
		int pulse = dlg.getPulse();
		int level = dlg.getLevel();
		int neighbourhood = dlg.getNeighbourhood();

		eInterpolationMethod method = dlg.getMethod();

		C2DPoint pmin = dlg.getMin();
		C2DPoint pmax = dlg.getMax();
		C2DPoint grid = dlg.getRes();

		double maxDist = dlg.getMaxDist();

		CProgressThread *thread = new CALSDataInterpolatorThread(alsdata, fn, vdem, pulse, level,
			                                                     neighbourhood, method, pmin,
																 pmax, grid, maxDist);

		bui_ProgressDlg *pdlg = new bui_ProgressDlg(thread, vdem->getDEM(), "DEM Interpolation");
		pdlg->exec();

		bool ret = pdlg->getSuccess();
		delete pdlg;

		baristaProjectHelper.refreshTree();
	}
};

void CBaristaPopUpHandler::filterALS()
{
	CVALS* als = (CVALS*)object;
	
	bui_ALSFilterDialog dlg(als);
	dlg.exec();
};

void CBaristaPopUpHandler::mergeALS()
{
	bui_ALSMergeDialog dlg;
	dlg.exec();
	if (dlg.getSuccess())
	{
		CVALS *als = dlg.getALS();

		CProgressThread *thread = new CALSDataMergeThread(&dlg);
		bui_ProgressDlg *pdlg = new bui_ProgressDlg(thread, als->getCALSData(), "Merging ALS data sets");

		pdlg->exec();
	}
};

void CBaristaPopUpHandler::exportALS()
{
	CVALS* als = (CVALS*)object;
	bui_ALSExportDialog dlg(als);
	dlg.exec();
}

void CBaristaPopUpHandler::interpolateIntensityFromALS()
{
	CVALS* als = (CVALS*)object;
	CALSDataSet *alsdata = als->getCALSData();

	bui_ALSInterpolationDialog dlg(als, "Interpolate Intensity Image from ALS Data", "UserGuide/ALSIntensityInterpolate.html", false);
	dlg.exec();

	if (dlg.getSuccess())
	{
		CFileName fn = dlg.getFileName();

		CVImage *vimage = project.addImage(fn, false);

		int pulse = dlg.getPulse();
		int level = dlg.getLevel();
		int neighbourhood = dlg.getNeighbourhood();

		eInterpolationMethod method = dlg.getMethod();

		C2DPoint pmin = dlg.getMin();
		C2DPoint pmax = dlg.getMax();
		C2DPoint grid = dlg.getRes();

		double maxDist = dlg.getMaxDist();

		CProgressThread *thread = new CALSDataInterpolatorThread(als, fn, vimage, pulse, level,
			                                                     neighbourhood, method, pmin,
																 pmax, grid, maxDist);

		bui_ProgressDlg *pdlg = new bui_ProgressDlg(thread, vimage->getHugeImage(), "Intensity Interpolation");
		pdlg->exec();
		bool ret = pdlg->getSuccess();
		delete pdlg;

		baristaProjectHelper.refreshTree();
	}
};

void CBaristaPopUpHandler::createXYZPolyLineArrayPopUpMenu()
{
    exportXYZPolyLinesTextFileAction = new QAction(tr("&Export XYZPolyLines to TextFile"), this);
    connect(exportXYZPolyLinesTextFileAction, SIGNAL(triggered()), this, SLOT(exportXYZPolyLinesTextFile()));
	this->addAction(exportXYZPolyLinesTextFileAction);

    exportXYZPolyLinesVRMLAction = new QAction(tr("&Export XYZPolyLines to VRML"), this);
    connect(exportXYZPolyLinesVRMLAction, SIGNAL(triggered()), this, SLOT(exportXYZPolyLinesVRML()));
	this->addAction(exportXYZPolyLinesVRMLAction);

    exportXYZPolyLinesDXFAction = new QAction(tr("&Export XYZPolyLines to DXF"), this);
    connect(exportXYZPolyLinesDXFAction, SIGNAL(triggered()), this, SLOT(exportXYZPolyLinesDXF()));
	this->addAction(exportXYZPolyLinesDXFAction);

	//addition >>
	//on 15-3-2011
	exportXYZPolyLinesShapefileAction = new QAction(tr("&Export XYZPolyLines to Shapefile"), this);
    connect(exportXYZPolyLinesShapefileAction, SIGNAL(triggered()), this, SLOT(exportXYZPolyLinesShapefile()));
	this->addAction(exportXYZPolyLinesShapefileAction);
	//<< addition

    TransformXYZPolyLines3DAction = new QAction(tr("&Transform XYZPolyline"), this);
    connect(TransformXYZPolyLines3DAction, SIGNAL(triggered()), this, SLOT(TransformXYZPolyLines3D()));
	this->addAction(TransformXYZPolyLines3DAction);
}

void CBaristaPopUpHandler::createBuildingArrayPopUpMenu()
{
    exportBuildingsTextFileAction = new QAction(tr("&Export Building to TextFile"), this);
    connect(exportBuildingsTextFileAction, SIGNAL(triggered()), this, SLOT(exportBuildingsTextFile()));
	this->addAction(exportBuildingsTextFileAction);

    exportBuildingsVRMLAction = new QAction(tr("&Export Building to VRML"), this);
    connect(exportBuildingsVRMLAction, SIGNAL(triggered()), this, SLOT(exportBuildingsVRML()));
	this->addAction(exportBuildingsVRMLAction);

    exportBuildingsDXFAction = new QAction(tr("&Export Building to DXF"), this);
    connect(exportBuildingsDXFAction, SIGNAL(triggered()), this, SLOT(exportBuildingsDXF()));
	this->addAction(exportBuildingsDXFAction);	
}

void CBaristaPopUpHandler::createXYContourArrayPopUpMenu()
{
    thinoutXYContoursAction = new QAction(tr("&Thin Out XYContours"), this);
    connect(thinoutXYContoursAction, SIGNAL(triggered()), this, SLOT(thinoutXYContours()));
	this->addAction(thinoutXYContoursAction);

	this->addSeparator();

    OnOffAction = new QAction(tr("&Display On/Off"), this);
    connect(OnOffAction, SIGNAL(triggered()), this, SLOT(OnOff()));
	this->addAction(OnOffAction);


}

void CBaristaPopUpHandler::createXYPolyLineArrayPopUpMenu()
{
	submenuOne = new QMenu(this );

    monoplotxyPolyLinesRPCAction = new QAction(tr("&with RPC"), this);
    connect(monoplotxyPolyLinesRPCAction, SIGNAL(triggered()), this, SLOT(monoplotxyPolyLinesRPC()));
	submenuOne->addAction(monoplotxyPolyLinesRPCAction);

    monoplotxyPolyLinesAffineAction = new QAction(tr("&with Affine"), this);
    connect(monoplotxyPolyLinesAffineAction, SIGNAL(triggered()), this, SLOT(monoplotxyPolyLinesAffine()));
	submenuOne->addAction(monoplotxyPolyLinesAffineAction);

	submenuOne->setTitle("&Monoplot XYPolyLines");

	this->addMenu(submenuOne);
	this->addSeparator();

	submenuTwo = new QMenu(this );

    geoTFWPolyLinesAction = new QAction(tr("&with TFW file"), this);
    connect(geoTFWPolyLinesAction, SIGNAL(triggered()), this, SLOT(geoTFWPolyLines()));
	submenuTwo->addAction(geoTFWPolyLinesAction);

    geo2DConformalPolyLinesAction = new QAction(tr("&with 2D Trafo"), this);
    connect(geo2DConformalPolyLinesAction, SIGNAL(triggered()), this, SLOT(geo2DConformalPolyLines()));
	submenuTwo->addAction(geo2DConformalPolyLinesAction);

	submenuTwo->setTitle("&Georeference XYPolyLines");

	this->addMenu(submenuTwo);

	this->addSeparator();

    OnOffAction = new QAction(tr("&Display On/Off"), this);
    connect(OnOffAction, SIGNAL(triggered()), this, SLOT(OnOff()));
	this->addAction(OnOffAction);
}

void CBaristaPopUpHandler::createXYZPointArrayPopUpMenu()
{

	submenuOne = new QMenu(this );

    xyz_to_xyRPCAction = new QAction(tr("&with RPC"), this);
    connect(xyz_to_xyRPCAction, SIGNAL(triggered()), this, SLOT(xyz_to_xyRPC()));
	submenuOne->addAction(xyz_to_xyRPCAction);

    xyz_to_xyAffineAction = new QAction(tr("&with Affine"), this);
    connect(xyz_to_xyAffineAction, SIGNAL(triggered()), this, SLOT(xyz_to_xyAffine()));
	submenuOne->addAction(xyz_to_xyAffineAction);

    xyz_to_xyTFWAction = new QAction(tr("&with TFW"), this);
    connect(xyz_to_xyTFWAction, SIGNAL(triggered()), this, SLOT(xyz_to_xyTFW()));
	submenuOne->addAction(xyz_to_xyTFWAction);

    xyz_to_xyPushbroomAction = new QAction(tr("&with Pushbroom Model"), this);
    connect(xyz_to_xyPushbroomAction, SIGNAL(triggered()), this, SLOT(xyz_to_xyPushbroom()));
	submenuOne->addAction(xyz_to_xyPushbroomAction);

#ifndef FINAL_RELEASE
    xyz_to_xyFrameCameraAction = new QAction(tr("&with Frame Camera Model"), this);
    connect(xyz_to_xyFrameCameraAction, SIGNAL(triggered()), this, SLOT(xyz_to_xyFrameCamera()));
	submenuOne->addAction(xyz_to_xyFrameCameraAction);
#endif
	submenuOne->setTitle("&3D to 2D");

	this->addMenu(submenuOne);
	this->addSeparator();

	submenuTwo = new QMenu(this );

    setControlAction = new QAction(tr("&set Control"), this);
    connect(setControlAction, SIGNAL(triggered()), this, SLOT(setControl()));
	submenuTwo->addAction(setControlAction);

    unsetControlAction = new QAction(tr("&unset Control"), this);
    connect(unsetControlAction, SIGNAL(triggered()), this, SLOT(unsetControl()));
	submenuTwo->addAction(unsetControlAction);

	submenuTwo->setTitle("&Control");
	this->addMenu(submenuTwo);

	this->addSeparator();

	if (this->setReferenceInformationAction) delete this->setReferenceInformationAction;
    setReferenceInformationAction = new QAction(tr("&Set XYZPoints Reference System"), this);
    connect(setReferenceInformationAction, SIGNAL(triggered()), this, SLOT(setReferenceInformation()));
	this->addAction(setReferenceInformationAction);

	this->addSeparator();
	xyPointArraySelectorAction = new QAction("Open Point Selector",this);
	this->connect(xyPointArraySelectorAction,SIGNAL(triggered()),this,SLOT(pointArraySelector()));
	this->addAction(xyPointArraySelectorAction);

	this->addSeparator();


    ChangeSigmaXYZAction = new QAction(tr("&Change Sigma"), this);
    connect(ChangeSigmaXYZAction, SIGNAL(triggered()), this, SLOT(ChangeSigmaXYZ()));
	this->addAction(ChangeSigmaXYZAction);

    DifferencebyLabelXYZAction = new QAction(tr("&Difference by Label"), this);
    connect(DifferencebyLabelXYZAction, SIGNAL(triggered()), this, SLOT(DifferencebyLabelXYZ()));
	this->addAction(DifferencebyLabelXYZAction);

    DifferencebyIndexXYZAction = new QAction(tr("&Difference by Index"), this);
    connect(DifferencebyIndexXYZAction, SIGNAL(triggered()), this, SLOT(DifferencebyIndexXYZ()));
	this->addAction(DifferencebyIndexXYZAction);

    CopyToOtherListAction = new QAction(tr("&Copy to other XYZPoint list"), this);
    connect(CopyToOtherListAction, SIGNAL(triggered()), this, SLOT(CopyToOtherList()));
	this->addAction(CopyToOtherListAction);

	this->addSeparator();
	submenuThree = new QMenu(this );

    XYZtoGeographicAction = new QAction(tr("&to Geographic"), this);
    connect(XYZtoGeographicAction, SIGNAL(triggered()), this, SLOT(XYZtoGeographic()));
	submenuThree->addAction(XYZtoGeographicAction);

    XYZtoGeocentricAction = new QAction(tr("&to Geocentric"), this);
    connect(XYZtoGeocentricAction, SIGNAL(triggered()), this, SLOT(XYZtoGeocentric()));
	submenuThree->addAction(XYZtoGeocentricAction);

    XYZtoUTMAction = new QAction(tr("&to UTM"), this);
    connect(XYZtoUTMAction, SIGNAL(triggered()), this, SLOT(XYZtoUTM()));
	submenuThree->addAction(XYZtoUTMAction);

    XYZtoTMAction = new QAction(tr("&to TM"), this);
    connect(XYZtoTMAction, SIGNAL(triggered()), this, SLOT(XYZtoTM()));
	submenuThree->addAction(XYZtoTMAction);

	submenuThree->setTitle("&Transform 3D Points");

	this->addMenu(submenuThree);

	this->addSeparator();

    export3DSpacedTextFileAction = new QAction(tr("&Export Space Separated TextFile"), this);
    connect(export3DSpacedTextFileAction, SIGNAL(triggered()), this, SLOT(export3DSpacedTextFile()));
	this->addAction(export3DSpacedTextFileAction);

    export3DCSVTextFileAction = new QAction(tr("&Export Comma Separated TextFile"), this);
    connect(export3DCSVTextFileAction, SIGNAL(triggered()), this, SLOT(export3DCSVTextFile()));
	this->addAction(export3DCSVTextFileAction);

    exportDDMMSSsssFileAction = new QAction(tr("&Export DD.MMSSsss TextFile"), this);
    connect(exportDDMMSSsssFileAction, SIGNAL(triggered()), this, SLOT(exportDDMMSSsssFile()));
	this->addAction(exportDDMMSSsssFileAction);

	//addition >>
	export3DShapefileAction = new QAction(tr("&Export as Shapefile"), this);
    connect(export3DShapefileAction, SIGNAL(triggered()), this, SLOT(export3DShapefile()));
	this->addAction(export3DShapefileAction);
	//<< addition

	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;
    if (xyzpoints->getReferenceSystem()->getCoordinateType() == eGRID)
	{
		this->addSeparator();
		InterpolateDEMAction = new QAction(tr("&Interpolate DEM"), this);
		connect(InterpolateDEMAction, SIGNAL(triggered()), this, SLOT(InterpolateDEM()));
		this->addAction(InterpolateDEMAction);
	}

	if (((xyzpoints->getReferenceSystem()->getCoordinateType() == eGRID) || 
		 (xyzpoints->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)) && 
		(project.getNumberOfImages() > 0))
	{
		this->addSeparator();
		MatchGCPsAction = new QAction(tr("&Match GCPs"), this);
		connect(MatchGCPsAction, SIGNAL(triggered()), this, SLOT(MatchGCPs()));
		this->addAction(MatchGCPsAction);
	}
	
	this->addSeparator();
	Get3DResidualAction = new QAction(tr("&Vector Plot"), this);
	connect(Get3DResidualAction, SIGNAL(triggered()), this, SLOT(Get3DResidual()));
	this->addAction(Get3DResidualAction);	
}

void CBaristaPopUpHandler::createXYEllipseArrayPopUpMenu()
{
    getEllipseCentersAction = new QAction(tr("&Add Centers to Image Points"), this);
    connect(getEllipseCentersAction, SIGNAL(triggered()), this, SLOT(getEllipseCenters()));
	this->addAction(getEllipseCentersAction);

    exportEllipsestoFileAction = new QAction(tr("&Export Ellipses to TextFile"), this);
    connect(exportEllipsestoFileAction, SIGNAL(triggered()), this, SLOT(exportEllipsestoFile()));
	this->addAction(exportEllipsestoFileAction);

    exportEllipseCenterstoFileAction = new QAction(tr("&Export Ellipse Centers to TextFile"), this);
    connect(exportEllipseCenterstoFileAction, SIGNAL(triggered()), this, SLOT(exportEllipseCenterstoFile()));
	this->addAction(exportEllipseCenterstoFileAction);

	this->addSeparator();

    OnOffAction = new QAction(tr("&Display On/Off"), this);
    connect(OnOffAction, SIGNAL(triggered()), this, SLOT(OnOff()));
	this->addAction(OnOffAction);
}

void CBaristaPopUpHandler::makeImagePointsActive()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

	xypoints->setActive(!xypoints->getActive());

	baristaProjectHelper.refreshTree();

	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
	    workspace->subWindowList().at(i)->update();
	}

}

void CBaristaPopUpHandler::ChangeSigmaXY()
{
	CXYPointArray* points = (CXYPointArray*)this->object;
	bui_ChangeXYSigmasDlg sigmadlg;
	sigmadlg.setSX(0.5);
	sigmadlg.setSY(0.5);

	sigmadlg.setModal(true);
	sigmadlg.exec();

	if ( sigmadlg.getmakeChanges())
	{
		points->setSXSYforAll(sigmadlg.getSX(), sigmadlg.getSY());
	}

}

void CBaristaPopUpHandler::DifferencebyLabelXY()
{
	CXYPointArray* points = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
	CVImage* image = (CVImage*)imageitem->getObject();

	CObjectSelector selector;
	selector.makeXYPointSelector();

    ZObject* object = selector.getSelection();
    CXYPointArray* comparepoints = (CXYPointArray*)object;

    if(!comparepoints)
        return;

	CXYPointArray* differences = image->getDifferencedXYPoints();

	if ( differences->GetSize() != 0 )
		differences->RemoveAll();

	points->differenceByLabel(differences, comparepoints);
	differences->setFileName("XYdiff");

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::DifferencebyIndexXY()
{
	CXYPointArray* points = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
	CVImage* image = (CVImage*)imageitem->getObject();

	CObjectSelector selector;
	selector.makeXYPointSelector();

    ZObject* object = selector.getSelection();
    CXYPointArray* comparepoints = (CXYPointArray*)object;

    if(!comparepoints)
        return;

	CXYPointArray* differences = image->getDifferencedXYPoints();

	points->differenceByIndex(differences, comparepoints);
	differences->setFileName("XYdiff");

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::monoplotxyRPC()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

    CVImage* image = (CVImage*)imageitem->getObject();

	if ( image )
	{
		CRPC* rpc = image->getRPC();

		if ( !rpc->gethasValues())
		{
			QMessageBox::warning( this, "Image has no RPC parameters!"
				, "No RPC Monoplotting possible!");
		}
		else
		{
			CObjectSelector selector;
			selector.makeDEMSelector();

			ZObject* object = selector.getSelection();
			CVDEM* vdem = (CVDEM*)object;

			if (vdem )
			{
				image->setCurrentSensorModel(eRPC);

				CMonoPlotter monoplotter;

				monoplotter.setModel(image->getCurrentSensorModel());
				monoplotter.setVDEM(vdem, false);

				if ((!monoplotter.getDEMToSensor()) || (!monoplotter.getSensorToDEM()))
				{
					QMessageBox::warning( this, "Coordinate systems of RPCs and DEM are incompatible!"
						, "No RPC Monoplotting possible!");
				}
				else
				{
					CXYZPointArray* xyzpoints = project.addXYZPoints("RPCMonoplottedPoints", vdem->getReferenceSystem(), true, false,eMONOPLOTTED);

					for ( int i = 0; i < xypoints->GetSize(); i++)
					{
						CXYZPoint xyzpoint;
						CXYPoint* xypoint = xypoints->getAt(i);
						if (monoplotter.getPoint(xyzpoint, *xypoint))
						{
							xyzpoint.setLabel(xypoint->getLabel());
							xyzpoints->Add(xyzpoint);
						}
					}

					baristaProjectHelper.refreshTree();
				}
			}
		}
	}
}

void CBaristaPopUpHandler::monoplotxyTFW()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

    CVImage* image = (CVImage*)imageitem->getObject();

	if (image )
	{
		CTFW tfw = *image->getTFW();

		CObjectSelector selector;
		selector.makeDEMSelector();

		ZObject* object = selector.getSelection();
		CVDEM* vdem = (CVDEM*)object;

		if (vdem )
		{
			CMonoPlotter monoplotter;

			monoplotter.setModel(&tfw);
			monoplotter.setVDEM(vdem, false);
	
			if ((!monoplotter.getDEMToSensor()) || (!monoplotter.getSensorToDEM()))
			{
				QMessageBox::warning( this, "Coordinate systems of TFW and DEM are incompatible!"
					, "No TFW Monoplotting possible!");
			}
			else
			{					
				CXYZPointArray* xyzpoints = project.addXYZPoints("TFWMonoplottedPoints", vdem->getReferenceSystem(), true, false,eMONOPLOTTED);

				for ( int i = 0; i < xypoints->GetSize(); i++)
				{
					CXYZPoint xyzpoint;
					CXYPoint* xypoint = xypoints->getAt(i);
					if (monoplotter.getPoint(xyzpoint, *xypoint))
					{
						xyzpoint.setLabel(xypoint->getLabel());
						xyzpoints->Add(xyzpoint);
					}
				}

				baristaProjectHelper.refreshTree();
			}
		}
	}
}
void CBaristaPopUpHandler::monoplotxyAffine()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

    CVImage* image = (CVImage*)imageitem->getObject();

	if ( !image )
		return;

    CAffine* affine = image->getAffine();

	if ( !affine->gethasValues())
	{
		QMessageBox::warning( this, "Image has no Affine parameters!"
			, "No Affine Monoplotting possible!");
		return;
	}

	CObjectSelector selector;
	selector.makeDEMSelector();

	ZObject* object = selector.getSelection();
	CVDEM* vdem = (CVDEM*)object;

	if ( !vdem )
		return;

    CXYZPointArray* xyzpoints = project.addXYZPoints("AffineMonoplottedPoints", vdem->getReferenceSystem(), true, false,eMONOPLOTTED);

	image->setCurrentSensorModel(eAFFINE);

    CMonoPlotter monoplotter;

    monoplotter.setModel(image->getCurrentSensorModel());
    monoplotter.setVDEM(vdem, false);

	if ((!monoplotter.getDEMToSensor()) || (!monoplotter.getSensorToDEM()))
	{
		QMessageBox::warning( this, "Coordinate systems of Affine parameters and DEM are incompatible!"
			, "No Affine Monoplotting possible!");
		return;
	}

    for ( int i = 0; i < xypoints->GetSize(); i++)
    {
        CXYZPoint xyzpoint;
        CXYPoint* xypoint = xypoints->getAt(i);
        if (monoplotter.getPoint(xyzpoint, *xypoint))
		{
			xyzpoint.setLabel(xypoint->getLabel());
			xyzpoints->Add(xyzpoint);
		}
    }

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::monoplotxyPushBroom()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

    CVImage* image = (CVImage*)imageitem->getObject();

	if ( !image )
		return;

	CPushBroomScanner* push = image->getPushbroom();

	if ( !push->gethasValues())
	{
		QMessageBox::warning( this, "Image has no PushbroomScanner parameters!"
			, "No PushbroomScanner Monoplotting possible!");
		return;
	}

	CObjectSelector selector;
	selector.makeDEMSelector();

	ZObject* object = selector.getSelection();
	CVDEM* vdem = (CVDEM*)object;

	if ( !vdem )
		return;

	vdem->getDEM()->load();

    CXYZPointArray* xyzpoints = project.addXYZPoints("PushbroomScannerMonoplottedPoints", vdem->getReferenceSystem(), true, false,eMONOPLOTTED);

	image->setCurrentSensorModel(ePUSHBROOM);

    CMonoPlotter monoplotter;

    monoplotter.setModel(image->getCurrentSensorModel());
    monoplotter.setVDEM(vdem);

	if ((!monoplotter.getDEMToSensor()) || (!monoplotter.getSensorToDEM()))
	{
		QMessageBox::warning( this, "Coordinate systems of PushbroomScanner parameters and DEM are incompatible!"
			, "No PushbroomScanner Monoplotting possible!");
		return;
	}

    for ( int i = 0; i < xypoints->GetSize(); i++)
    {
        CXYZPoint xyzpoint;
        CXYPoint* xypoint = xypoints->getAt(i);
        if (monoplotter.getPoint(xyzpoint, *xypoint))
		{
			xyzpoint.setLabel(xypoint->getLabel());
			xyzpoints->Add(xyzpoint);
		}
    }

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::monoplotxyCurrentSensorModel()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

    CVImage* image = (CVImage*)imageitem->getObject();

	if ( !image )
		return;

	CSensorModel* smodel = image->getCurrentSensorModel();

	if ( !smodel->gethasValues())
	{
		QMessageBox::warning( this, "Image has no current sensor model!"
			, "No Monoplotting possible!");
		return;
	}

	CObjectSelector selector;
	selector.makeDEMSelector();

	ZObject* object = selector.getSelection();
	CVDEM* vdem = (CVDEM*)object;

	if ( !vdem )
		return;

	vdem->getDEM()->load();

    CXYZPointArray* xyzpoints = project.addXYZPoints("CSMMonoplottedPoints", vdem->getReferenceSystem(), true, false,eMONOPLOTTED);

    CMonoPlotter monoplotter;

    monoplotter.setModel(image->getCurrentSensorModel());
    monoplotter.setVDEM(vdem);

	if ((!monoplotter.getDEMToSensor()) || (!monoplotter.getSensorToDEM()))
	{
		QMessageBox::warning( this, "Coordinate systems of current sensor model and DEM are incompatible!"
			, "No Monoplotting possible!");
		return;
	}

    for ( int i = 0; i < xypoints->GetSize(); i++)
    {
        CXYZPoint xyzpoint;
        CXYPoint* xypoint = xypoints->getAt(i);
        if (monoplotter.getPoint(xyzpoint, *xypoint))
		{
			xyzpoint.setLabel(xypoint->getLabel());
			xyzpoints->Add(xyzpoint);
		}
    }

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::export2DSpacedTextFile()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += xypoints->getFileName()->GetFileName().GetChar();
    str += ".txt";
    QString qstr( str.GetChar());

	// file dialog
	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Space Separated Textfile Name",
		qstr,
		"Space Separated Textfile (*.txt)");

    if ( s.isNull())
        return;

	CFileName cfilename = (const char*)s.toLatin1();

    int type = 1; // switch for space/csv separation, space = 1

    xypoints->writeXYPointTextFile(cfilename, type);
}

void CBaristaPopUpHandler::export2DCSVTextFile()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += xypoints->getFileName()->GetFileName().GetChar();
    str += ".csv";
    QString qstr( str.GetChar());

	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Comma Separated Textfile Name",
		qstr,
		"Space Comma Separated Textfile (*.csv)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

    int type = 0; // switch for space/csv separation, csv = 0

    xypoints->writeXYPointTextFile(cfilename, type);
}

void CBaristaPopUpHandler::georeferencingTFW()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;
	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

    CVImage* image = (CVImage*)imageitem->getObject();
    CTFW tfw = *image->getTFW();

	if ( !tfw.gethasValues())
	{
		QMessageBox::warning( this, "Image has no TFW parameters!"
			, "No TFW Georeferencing possible!");
		return;
	}

	if ( tfw.getNumberOfStationPars() != 6)
	{
		QMessageBox::warning( this, "Parameter set is not TFW!"
			, "No TFW Georeferencing possible!");
		return;
	}

    CXYZPointArray* xyzpoints = project.addXYZPoints("TFWGeoReferencedPoints", tfw.getRefSys(), true, false,eGEOREFERENCED);

	Matrix parms;

	tfw.getAllParameters(parms);

    CTrans2DAffine trans2d(parms);

    for ( int i = 0; i < xypoints->GetSize(); i++)
    {
        CXYZPoint xyzpoint;
        CXYPoint* xypoint = xypoints->getAt(i);

		trans2d.transform(xyzpoint, *xypoint);
		xyzpoints->Add(xyzpoint);
    }

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::georeferencing2DTrafo()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

    CVImage* image = (CVImage*)imageitem->getObject();

    CTFW tfw = *image->getTFW();

	if ( !tfw.gethasValues())
	{
		QMessageBox::warning( this, "Image has no 2D Conformal parameters!"
			, "No 2D Conformal Georeferencing possible!");
		return;
	}

	if ( tfw.getNumberOfStationPars() != 4)
	{
		QMessageBox::warning( this, "Parameter set is not 2D Conformal!"
			, "No 2D Conformal Georeferencing possible!");
		return;
	}

    CXYZPointArray* xyzpoints = project.addXYZPoints("2DTrafoGeoReferencedPoints", tfw.getRefSys(), true, false,eGEOREFERENCED);

	Matrix parms;

	tfw.getAllParameters(parms);

	Matrix conformal;
	conformal.ReSize(4,1);

	conformal.element(0,0) = parms.element(0,0);
	conformal.element(1,0) = parms.element(1,0);
	conformal.element(2,0) = parms.element(2,0);
	conformal.element(3,0) = parms.element(3,0);

	CTrans2DConformal trans2d(conformal);

	for ( int i = 0; i < xypoints->GetSize(); i++)
	{
		CXYZPoint xyzpoint;
		CXYPoint* xypoint = xypoints->getAt(i);

		if (tfw.getRefSys().getCoordinateType() == eGRID)
		{
			(*xypoint)[1] *= -1.0;
		}

		trans2d.transform(xyzpoint, xypoint);
		xyzpoints->Add(xyzpoint);
	}

	baristaProjectHelper.refreshTree();
}


void CBaristaPopUpHandler::SingleRPCBiasCorrectionNone()
{
	CRPC* rpc = (CRPC*)this->object;
	rpc->fixAll();
}

void CBaristaPopUpHandler::SingleRPCBiasCorrectionShift()
{
    CRPC* rpc = (CRPC*)this->object;
	rpc->freeShift();
}

void CBaristaPopUpHandler::SingleRPCBiasCorrectionShiftDrift()
{
    CRPC* rpc = (CRPC*)this->object;
	rpc->freeShiftDrift();
}

void CBaristaPopUpHandler::SingleRPCBiasCorrectionAffine()
{
    CRPC* rpc = (CRPC*)this->object;
	rpc->freeAffine();
}

void CBaristaPopUpHandler::SingleRPCRegenerateBiasCorrected()
{
	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
	CVImage* vimage = (CVImage*)imageitem->getObject();

	vimage->getRPC()->reformulate();

	// get original name, attach "_bc" and set it
	CFileName oldname = vimage->oriRPCFileName();
	CFileName bcname = oldname.GetFileName().GetChar();
	bcname += "_bc";
	vimage->getRPC()->setFileName(bcname.GetChar());
	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::exportIkonosRPCs()
{
	CRPC* rpc = (CRPC*)this->object;

    CCharString str = inifile.getCurrDir();
    str += rpc->getFileName()->GetFileName().GetChar();
    str += "Ikonos.RPB";
    QString qstr( str.GetChar());

	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose an Ikonos Format RPC File Name",
		qstr,
		"Ikonos Format (*.RPB )");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

    rpc->writeIKONOSFormat(cfilename.GetChar());
}

void CBaristaPopUpHandler::exportQuickBirdRPCs()
{
	CRPC* rpc = (CRPC*)this->object;

    CCharString str = inifile.getCurrDir();
    str += rpc->getFileName()->GetFileName().GetChar();
    str += "QB.RPB";
    QString qstr( str.GetChar());

	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a QuickBird Format RPC File Name",
		qstr,
		"Ikonos QuickBird (*.RPB )");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

    rpc->writeQuickBirdFormat(cfilename.GetChar());
}

void CBaristaPopUpHandler::exportSocetRPCs()
{

	CRPC* rpc = (CRPC*)this->object;

    CCharString str = inifile.getCurrDir();
    str += rpc->getFileName()->GetFileName().GetChar();

    QString qstr( str.GetChar());

	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Socet Set Format RPC File Name",
		qstr,
		"All Files  (*.*)");

    if ( s.isNull())
        return;

    CFileName cfilename = s.toLatin1().constData();

    rpc->writeSocetFormat(cfilename.GetChar());

}


void CBaristaPopUpHandler::exportCSVRPCs()
{
	CRPC* rpc = (CRPC*)this->object;

    CCharString str = inifile.getCurrDir();
    str += rpc->getFileName()->GetFileName().GetChar();
    str += ".csv";
    QString qstr( str.GetChar());

	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Comma Separated RPC File Name",
		qstr,
		"Comma Separated RPC File (*.csv)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	rpc->writeTextFile(cfilename.GetChar());
}


void CBaristaPopUpHandler::exportAffineTextFile()
{
	CAffine* affine = (CAffine*)this->object;

    CCharString str = inifile.getCurrDir();
    str += affine->getFileName()->GetFileName().GetChar();
    str += ".csv";
    QString qstr( str.GetChar());

	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Comma Separated Affine File Name",
		qstr,
		"Comma Separated Affine File (*.csv)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

    affine->writeTextFile(cfilename);
}


void CBaristaPopUpHandler::setReferenceInformation()
{

	if (this->object->isa("CTFW"))
	{
		CTFW* tfw = (CTFW*)this->object;

		CReferenceSystem refsys(tfw->getRefSys());

		Bui_ReferenceSysDlg dlg(&refsys,tfw->getFileName(), "Set TFW reference system information","UserGuide/ThumbNodeTFW.html",false,NULL,true);

		dlg.exec();

		if (!dlg.getSuccess()) return;

		tfw->setRefSys(refsys);

	}
    else if (this->object->isa("CXYZPointArray"))
	{
		CXYZPointArray* points = (CXYZPointArray*)this->object;

		CReferenceSystem refsys(*points->getReferenceSystem());

		Bui_ReferenceSysDlg dlg(&refsys,points->getFileName(), "Set XYZPoints reference system information","UserGuide/Import3DPoints.html",false,NULL,true);

		dlg.exec();

		if (!dlg.getSuccess()) return;

		points->setReferenceSystem(refsys);
	}
    else if (this->object->isa("CVDEM"))
	{
		CVDEM* dem = (CVDEM*)this->object;

		CReferenceSystem refsys(dem->getReferenceSystem());

		Bui_ReferenceSysDlg dlg(&refsys,dem->getFileName(), "Set DEM reference system information","UserGuide/ImportDEM.html",false,NULL,true);

		dlg.exec();

		if (!dlg.getSuccess()) return;

		dem->setReferenceSystem(refsys);
	}
	else if (this->object->isa("CVALS"))
	{
		CVALS* als = (CVALS*) this->object;

		CReferenceSystem refsys(als->getReferenceSystem());

		CFileName fn = als->getFileName();
		Bui_ReferenceSysDlg dlg(&refsys, &fn, "Set ALS reference system information","UserGuide/ImportALS.html",false,NULL,true);

		dlg.exec();

		if (!dlg.getSuccess()) return;

		als->setReferenceSystem(refsys);
	}
    else if (this->object->isa("CAffine"))
	{
		CAffine* affine = (CAffine*)this->object;

		CReferenceSystem refsys(affine->getRefSys());

		Bui_ReferenceSysDlg dlg(&refsys,affine->getFileName(), "Set Affine reference system information","UserGuide/ThumbNodeAffine.html",false,NULL,true);

		dlg.exec();

		if (!dlg.getSuccess()) return;

		affine->setRefSys(refsys);
	}

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::exportTFWTextFile()
{
	CTFW* tfw = (CTFW*)this->object;

    CCharString str = inifile.getCurrDir();
    str += tfw->getFileName()->GetFileName().GetChar();
    str += ".tfw";
    QString qstr( str.GetChar());

	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a TFW File Name",
		qstr,
		"TFW File Name (*.tfw)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	tfw->writeTextFile(cfilename);
}

void CBaristaPopUpHandler::interpolateHeightfromDEM()
{
	CVDEM* dem = (CVDEM*)this->object;

	bui_InterpolateDEMHeightDlg dlg;

	dlg.setDEM(dem);

	double centerx = (dem->getDEM()->getmaxX() + dem->getDEM()->getminX())/2.0;
	double centery = (dem->getDEM()->getmaxY() + dem->getDEM()->getminY())/2.0;

	dlg.setX(centerx);
	dlg.setY(centery);

	dlg.setModal(true);
	dlg.exec();
}

void CBaristaPopUpHandler::interpolateHeightsfromDEM()
{
	CVDEM* dem = (CVDEM*)this->object;
	CObjectSelector selector;
	selector.makeXYZPointSelector();

    ZObject* object = selector.getSelection();
    CXYZPointArray* points = (CXYZPointArray*)object;

    if(!points)
        return;

	if ((dem->getReferenceSystem().getCoordinateType() != points->getReferenceSystem()->getCoordinateType()) ||
		(dem->getReferenceSystem().isWGS84() != points->getReferenceSystem()->isWGS84()) ||
		((dem->getReferenceSystem().isUTM()  != points->getReferenceSystem()->isUTM()) &&
		 (dem->getReferenceSystem().getCoordinateType() == eGRID)))
	{
		QMessageBox::warning( this, "DEM Interpolation of Points not possible!"
			, "DEM and Points have different Reference system!");
		return;
	}

	CXYZPointArray* interpolatedpoints = project.addXYZPoints("InterpolatedHeights", dem->getReferenceSystem(), true, false,eNOTYPE);
	dem->getDEM()->interpolateHeight(interpolatedpoints, points);

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::detectBuildings()
{
	CVDEM* vdem = (CVDEM*)this->object;

#ifdef WIN32
	bui_BuildingDialog buildingDialog(vdem);

	buildingDialog.exec();

	buildingDialog.close();
	
#endif	
}


void CBaristaPopUpHandler::exportASCIIDEM()
{
	CVDEM* vdem = (CVDEM*)this->object;

    CCharString str = inifile.getCurrDir();
    str += vdem->getFileName()->GetFileName().GetChar();
    str += "_exported.txt";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Text File Name",
		qstr,
		"Text File Name (*.txt)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	baristaProjectHelper.setIsBusy(true);
	vdem->getDEM()->writeASCII(cfilename.GetChar());
	baristaProjectHelper.setIsBusy(false);
}

void CBaristaPopUpHandler::exportImageDEM()
{
	CVDEM* vdem = (CVDEM*)this->object;
/*
    CCharString str = inifile.getCurrDir();
    str += vdem->getFileName()->GetFileName().GetChar();
    str += "_exported.tif";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a TIFF File Name",
		qstr,
		"TIFF File Name (*.tif)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	vdem->getDEM()->writeTIFF(cfilename.GetChar());
*/

	Bui_GDALExportDlg dlg("",vdem,true,"Export DEM as Image");
	dlg.exec();

	if (!dlg.getSuccess())	// false if user has pressed "Cancel"
		return;

	if (!dlg.writeImage())	// actual writing failed
		QMessageBox::critical(NULL,"Export Error","Barista couldn't export the DEM image!",1,0,0);


}

void CBaristaPopUpHandler::exportASCIIDEMVerticalFlip()
{
	CVDEM* vdem = (CVDEM*)this->object;

    CCharString str = inifile.getCurrDir();
    str += vdem->getFileName()->GetFileName().GetChar();
    str += "_Vflipped.txt";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Text File Name",
		qstr,
		"Text File Name (*.txt)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	baristaProjectHelper.setIsBusy(true);
	vdem->getDEM()->writeASCIIVerticalFlip(cfilename.GetChar());
	baristaProjectHelper.setIsBusy(false);
}

void CBaristaPopUpHandler::exportXYZPolyLinesTextFile()
{
	CXYZPolyLineArray* xyzlines = (CXYZPolyLineArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += xyzlines->getFileName()->GetFileName().GetChar();
    str += ".txt";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Text File Name",
		qstr,
		"Text File Name (*.txt)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	xyzlines->writeXYZPolyLineTextFile(cfilename);
}

void CBaristaPopUpHandler::exportXYZPolyLinesVRML()
{
	CXYZPolyLineArray* xyzlines = (CXYZPolyLineArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += xyzlines->getFileName()->GetFileName().GetChar();
    str += ".wrl";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a VRML 1.0 ASCII-File Name",
		qstr,
		"VRML 1.0 ASCII-File (*.wrl)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	xyzlines->writeXYZPolyLineVRMLFile(cfilename);
}



void CBaristaPopUpHandler::exportBuildingsVRML()
{
#ifdef WIN32	
	CBuildingArray* buildings = (CBuildingArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += buildings->getFileName()->GetFileName().GetChar();
    str += ".wrl";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a VRML 2.0 ASCII-File Name",
		qstr,
		"VRML 2.0 ASCII-File (*.wrl)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	buildings->writeBuildingVRMLFile(cfilename);
#endif
}


void CBaristaPopUpHandler::exportBuildingsTextFile()
{
#ifdef WIN32	
	CBuildingArray* buildings = (CBuildingArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += buildings->getFileName()->GetFileName().GetChar();
    str += ".txt";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Text File Name",
		qstr,
		"Text File Name (*.txt)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	buildings->writeBuildingTextFile(cfilename);
#endif	
}

void CBaristaPopUpHandler::OnOff()
{
	if (this->object->isa("CXYContourArray"))
	{
		CXYContourArray* contours = (CXYContourArray*)this->object;
		if(contours->getActive())
		{
			contours->setActive(false);
		}else
		{
			contours->setActive(true);
		}
	}
	if (this->object->isa("CXYPolyLineArray"))
	{
		CXYPolyLineArray* edges = (CXYPolyLineArray*)this->object;
		if(edges->getActive())
		{
			edges->setActive(false);
		}else
		{
			edges->setActive(true);
		}
	}
	if (this->object->isa("CXYEllipseArray"))
	{
		CXYEllipseArray* ellipses = (CXYEllipseArray*)this->object;
		if(ellipses->getActive())
		{
			ellipses->setActive(false);
		}else
		{
			ellipses->setActive(true);
		}
	}

	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}
}

void CBaristaPopUpHandler::thinoutXYContours()
{
	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
	CVImage* image = (CVImage*)imageitem->getObject();
	baristaProjectHelper.setIsBusy(true);
	image->thinOutXYContours();
	baristaProjectHelper.setIsBusy(false);
	baristaProjectHelper.refreshTree();
}


void CBaristaPopUpHandler::monoplotxyPolyLinesRPC()
{
	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

    CVImage* image = (CVImage*)imageitem->getObject();

	if (image)
	{
		CRPC* rpc = image->getRPC();

		if (!rpc->gethasValues())
		{
			QMessageBox::warning( this, "Image has no RPC parameters!",
										"No RPC Monoplotting possible!");
		}
		else
		{
			image->setCurrentSensorModel(eRPC);
			if(monoplotxyPolyLines(image))
			{
				baristaProjectHelper.refreshTree();
			}
		}
	}
}

void CBaristaPopUpHandler::monoplotxyPolyLinesAffine()
{
    CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
    CVImage* image = (CVImage*)imageitem->getObject();
	if (image)
	{
		CAffine* affine = image->getAffine();

		if (!affine->gethasValues())
		{
			QMessageBox::warning( this, "Image has no Affine parameters!",
										"No Affine Monoplotting possible!");
		}
		else
		{
			image->setCurrentSensorModel(eAFFINE);
			if(monoplotxyPolyLines(image))
			{
				baristaProjectHelper.refreshTree();
			}
		}
	}
}

bool CBaristaPopUpHandler::monoplotxyPolyLines(CVImage *image)
{
	bool retVal = true;
	CSensorModel *sensorModel = image->getCurrentSensorModel();

	if (!sensorModel) retVal = false;
	else
	{
		CXYPolyLineArray* xylines = (CXYPolyLineArray *) this->object;

		CObjectSelector selector;
		selector.makeDEMSelector();

		ZObject* object = selector.getSelection();
		CVDEM* vdem = (CVDEM*)object;

		if (!vdem ) retVal =  false;
		else
		{
			vdem->getDEM()->load();

			CXYZPolyLineArray* xyzlines = project.getXYZPolyLines();

			xyzlines->setReferenceSystem(vdem->getReferenceSystem());

			string str = sensorModel->getClassName();
			CCharString name(str);
			name = name.Right(name.GetLength() - 1) + "MonoplottedPolyLines";
			xyzlines->setFileName(project.getNewXYZPointArrayName(name));

			CMonoPlotter plotter;

			plotter.setModel(sensorModel);
			plotter.setVDEM(vdem);

			if ((!plotter.getDEMToSensor()) || (!plotter.getSensorToDEM()))
			{
				QMessageBox::warning( this, "Coordinate systems of sensor model and DEM are incompatible!",
									 "No Monoplotting possible!");
				retVal =   false;
			}
			else
			{
				baristaProjectHelper.setIsBusy(true);

				int linecount = xyzlines->GetSize();

				for ( int i = 0; i < xylines->GetSize(); i++)
				{
					CXYPolyLine* xyline = xylines->getAt(i);

					CXYZPolyLine* xyzline = xyzlines->add();

					xyzline->setLabel(xyline->getLabel());
					

					for ( int j = 0; j < xyline->getPointCount(); j++)
					{
						CXYZPoint xyzpoint;

						plotter.getPoint(xyzpoint, xyline->getPoint(j));
						xyzpoint.setLabel(xyline->getPoint(j).getLabel());
						xyzline->addPoint(xyzpoint);
					}
				}

				baristaProjectHelper.setIsBusy(false);
			}
		}
	}
	return retVal;
}


void CBaristaPopUpHandler::geoTFWPolyLines()
{
	CXYPolyLineArray* xylines = (CXYPolyLineArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
    CVImage* image = (CVImage*)imageitem->getObject();

	if ( !image)
		return;

	CTFW tfw = *image->getTFW();

	if ( !tfw.gethasValues())
	{
		QMessageBox::warning( this, "Image has no TFW parameters!"
			, "No Georeferencing of PolyLines possible!");
		return;
	}

	if ( tfw.getNumberOfStationPars() != 6)
	{
		QMessageBox::warning( this, "Parameter set is not TFW!"
			, "No TFW Georeferencing possible!");
		return;
	}

	CObjectSelector selector;
	selector.makeDEMSelector();

	ZObject* object = selector.getSelection();
	CVDEM* vdem = (CVDEM*)object;

	if ( !vdem )
		return;

	vdem->getDEM()->load();

	CDEM* dem = vdem->getDEM();

	CXYZPolyLineArray* xyzlines = project.getXYZPolyLines();

    xyzlines->setReferenceSystem(vdem->getReferenceSystem());
    xyzlines->setFileName(project.getNewXYZPointArrayName("GeoTFWPolyLines"));

	int linecount = xyzlines->GetSize();

	CLabel label("Line");
	label += linecount;

	Matrix parms;

	tfw.getAllParameters(parms);

    CTrans2DAffine trans2d(parms);

    for ( int i = 0; i < xylines->GetSize(); i++)
    {
        CXYZPolyLine* xyzline = xyzlines->add();

		xyzline->setLabel(label);
		label++;

        CXYPolyLine* xyline = xylines->getAt(i);

		for ( int j = 0; j < xyline->getPointCount(); j++)
		{
			CXYZPoint xyzpoint;
			xyzpoint.setLabel(xyline->getPoint(j).getLabel());
			CXYPoint xypoint = xyline->getPoint(j);

			trans2d.transform(xyzpoint, xypoint);

			double Z;

			Z = dem->getBLInterpolatedHeight(xyzpoint.getX(), xyzpoint.getY());

			xyzpoint.setZ(Z);
			xyzline->addPoint(xyzpoint);
		}
    }

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::geo2DConformalPolyLines()
{
	CXYPolyLineArray* xylines = (CXYPolyLineArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

    CVImage* image = (CVImage*)imageitem->getObject();

	if ( !image)
		return;

	CTFW tfw = *image->getTFW();

	if ( !tfw.gethasValues())
	{
		QMessageBox::warning( this, "Image has no TFW parameters!"
			, "No Georeferencing of PolyLines possible!");
		return;
	}

	if ( tfw.getNumberOfStationPars() != 4)
	{
		QMessageBox::warning( this, "Parameter set is not 2D Conformal!"
			, "No 2D Conformal Georeferencing possible!");
		return;
	}

	CObjectSelector selector;
	selector.makeDEMSelector();

	ZObject* object = selector.getSelection();
	CVDEM* vdem = (CVDEM*)object;

	if ( !vdem )
		return;

	vdem->getDEM()->load();

	CDEM* dem = vdem->getDEM();

	CXYZPolyLineArray* xyzlines = project.getXYZPolyLines();

    xyzlines->setReferenceSystem(vdem->getReferenceSystem());
    xyzlines->setFileName(project.getNewXYZPointArrayName("Geo2DConformalPolyLines"));

	int linecount = xyzlines->GetSize();

	CLabel label("Line");
	label += linecount;

	Matrix parms;

	tfw.getAllParameters(parms);

	Matrix conformal;
	conformal.ReSize(4,1);

	conformal.element(0,0) = parms.element(0,0);
	conformal.element(1,0) = parms.element(1,0);
	conformal.element(2,0) = parms.element(2,0);
	conformal.element(3,0) = parms.element(3,0);

	CTrans2DConformal trans2d(conformal);

	eCoordinateType coordType = tfw.getRefSys().getCoordinateType();

    for ( int i = 0; i < xylines->GetSize(); i++)
    {
        CXYZPolyLine* xyzline = xyzlines->add();

		xyzline->setLabel(label);
		label++;

        CXYPolyLine* xyline = xylines->getAt(i);

		for ( int j = 0; j < xyline->getPointCount(); j++)
		{
			CXYZPoint xyzpoint;

			xyzpoint.setLabel(xyline->getPoint(j).getLabel());

			CXYPoint xypoint = xyline->getPoint(j);

			if (coordType == eGRID)
			{
				xypoint.setY(-1.0*xypoint.getY());
			}

			trans2d.transform(xyzpoint, xypoint);

			double Z;

			Z = dem->getBLInterpolatedHeight(xyzpoint.getX(), xyzpoint.getY());

			xyzpoint.setZ(Z);
			xyzline->addPoint(xyzpoint);
		}
    }

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::ChangeSigmaXYZ()
{
	CXYZPointArray* points = (CXYZPointArray*)this->object;
	bui_ChangeXYZSigmasDlg sigmadlg;

	sigmadlg.setSX(0.1);
	sigmadlg.setSY(0.1);
	sigmadlg.setSZ(0.1);

	sigmadlg.setModal(true);
	sigmadlg.exec();

	if ( sigmadlg.getmakeChanges())
    {
	    points->setSXSYSZforAll(sigmadlg.getSX(), sigmadlg.getSY(), sigmadlg.getSZ());
	}
}

void CBaristaPopUpHandler::DifferencebyLabelXYZ()
{
	CXYZPointArray* points = (CXYZPointArray*)this->object;

    CObjectSelector selector;
	selector.makeXYZPointSelector();

    ZObject* object = selector.getSelection();
    CXYZPointArray* comparepoints = (CXYZPointArray*)object;

	if ((!comparepoints) || (points->GetSize() < 1))
        return;

	// check if coordinates are in the same coordinate system
	// and show at least a warning message
	eCoordinateType typclicked = points->getReferenceSystem()->getCoordinateType();
	eCoordinateType typselected = comparepoints->getReferenceSystem()->getCoordinateType();

	if (typclicked != typselected)
	{
		QMessageBox::warning( this, " XYZ Difference by Label not possible!"
		, "XYZ Points must not be defined in different coordinate systems! ");
		return;
	}

	CXYZPointArray* differences = project.addXYZPoints("XYZdiff", *(points->getReferenceSystem()), true, false,eDIFFERENCED);

	
	bool metrical = true;
	if (typclicked == eGEOGRAPHIC)
	{
		bui_DifferenceUnitDlg dlg;
		dlg.setMeterChecked();
		dlg.exec();

		metrical = dlg.meterChecked();
	}

	project.differenceByLabelXYZ(differences,points,comparepoints,metrical);

	baristaProjectHelper.refreshTree();
}


void CBaristaPopUpHandler::DifferencebyIndexXYZ()
{
	CXYZPointArray* points = (CXYZPointArray*)this->object;

    CObjectSelector selector;
	selector.makeXYZPointSelector();

    ZObject* object = selector.getSelection();
    CXYZPointArray* comparepoints = (CXYZPointArray*)object;

    if ((!comparepoints) || (points->GetSize() < 1))
        return;

	// check if coordinates are in the same coordinate system
	// and show at least a warning message
	eCoordinateType typclicked = points->getReferenceSystem()->getCoordinateType();
	eCoordinateType typselected = comparepoints->getReferenceSystem()->getCoordinateType();

	if (typclicked != typselected)
	{
		QMessageBox::warning( this, " XYZ Difference by Index not possible!"
		, "XYZ Points must not be defined in different coordinate systems! ");
		return;
	}

	CXYZPointArray* differences = project.addXYZPoints("XYZdiff",*(points->getReferenceSystem()), true, false,eDIFFERENCED);

	if (typclicked == eGEOGRAPHIC)
	{
		bui_DifferenceUnitDlg dlg;

		dlg.setMeterChecked();
		dlg.setModal(true);
		dlg.exec();

		if ( dlg.meterChecked())
		{
			CXYZPointArray utmselected;
			CXYZPointArray utmcomparepoints;
			CXYZPoint *p = points->getAt(0);
			utmselected.getReferenceSystem()->setToWGS1984();
			utmselected.getReferenceSystem()->setUTMZone(p->getX(), p->getY());
			CTransGeographicToGrid trans(*(points->getReferenceSystem()), *(utmselected.getReferenceSystem()));


			trans.transformArray(&utmselected, points);
			trans.transformArray(&utmcomparepoints, comparepoints);

			utmselected.differenceByIndex(differences, &utmcomparepoints);
			differences->getReferenceSystem()->setCoordinateType(eUndefinedCoordinate);
		}
		else if ( dlg.degreeChecked())
		{
			points->differenceByIndex(differences, comparepoints);
			differences->getReferenceSystem()->setCoordinateType(typclicked);
		}
	}

	if (typclicked != eGEOGRAPHIC)
	{
		points->differenceByIndex(differences, comparepoints);
		differences->getReferenceSystem()->setCoordinateType(typclicked);
	}

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::export3DShapefile()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += xyzpoints->getFileName()->GetFileName().GetChar();
    str += ".shp";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Shapefile Name",
		qstr,
		"Shapefile Name (*.shp)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

    int type = 1; // switch for space/csv separation, space = 1

    //xyzpoints->writeXYZPointShapefile(cfilename);

	// above method is not working for Final Release due to inclusion of ogrsf_frmts.h file, 2 errors found in BaristaBatch project 
	
	// source: http://gdal.org/ogr/ogr_apitut.html
	// we start by registering all the drivers, 
	// and then fetch the Shapefile driver as we will need it to create our output file. 
	const char *pszDriverName = "ESRI Shapefile";

	/*			DEPRECEATED Classes
    OGRSFDriver *poDriver;

    OGRRegisterAll();

    poDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName(
                pszDriverName );
    if( poDriver == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Driver not found!");
		return;
    }

	// Next we create the datasource.
	OGRDataSource *poDS;

    poDS = poDriver->CreateDataSource((const char *) cfilename, NULL );
    if( poDS == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Datasource creation failed!");
		return;
    }

	// Now we create the output layer.
	 OGRLayer *poLayer;

    poLayer = poDS->CreateLayer("XYZPointArray", NULL, wkbPoint25D, NULL );
    if( poLayer == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Layer creation failed!");
		return;
    }
	*/

	GDALDriver *poDriver;

	GDALAllRegister();

	poDriver = GetGDALDriverManager()->GetDriverByName(
		pszDriverName);
	if (poDriver == NULL)
	{
		//QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Driver not found!");
		return;
	}

	// Next we create the datasource.
	GDALDataset *poDS;

	poDS = poDriver->Create((const char *)cfilename, 0, 0, 0, GDT_Unknown, NULL);
	if (poDS == NULL)
	{
		//QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Datasource creation failed!");
		return;
	}

	// Now we create the output layer.
	OGRLayer *poLayer;

	poLayer = poDS->CreateLayer("XYZPointArray", NULL, wkbPoint25D, NULL);
	if (poLayer == NULL)
	{
		//QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Layer creation failed!");
		return;
	}

	 //we need to create any attribute fields that should appear on the layer.
	OGRFieldDefn oField( "Name", OFTString );
    oField.SetWidth(32);


    if( poLayer->CreateField( &oField ) != OGRERR_NONE )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Data field creation failed!");
		return;
    }
	

	//double x, y;
    //char szName[33];
	const char* buf;
    double x, y, z;//, tx[2], ty[2];
	//double ht=0;
	//OGRLineString ls;

	for (int i = 0; i < xyzpoints->GetSize(); i++)    
    //while( !feof(stdin) 
    //       && fscanf( stdin, "%lf,%lf,%32s", &x, &y, szName ) == 3 )
    {
		

        CObsPoint* point = xyzpoints->GetAt(i);

        buf = point->getLabel().GetChar();
        x = (*point)[0];
        y = (*point)[1];
        z = (*point)[2];


        OGRFeature *poFeature;

        poFeature = OGRFeature::CreateFeature( poLayer->GetLayerDefn() );
        poFeature->SetField( "Name", buf );
	
        //poFeature->SetField("BHeight", z);

        OGRPoint pt;
		//if(i==0)
		//{
        pt.setX( x );
        pt.setY( y );
		pt.setZ( z );
		//ht = z-ht;
		//tx = x; ty = y;

		//}else
		//{
		//      pt.setX( tx );
        //pt.setY( ty );
		//pt.setZ( z );
		//}

		//ls.addPoint(x,y,z);

        poFeature->SetGeometry( &pt ); 

        if( poLayer->CreateFeature( poFeature ) != OGRERR_NONE )
        {
           //QMessageBox::warning(NULL, " Writing to shapefile!"
			//, "Feature creation failed!");
			return;
        }

		// Now we create a feature in the file. The OGRLayer::CreateFeature() 
		// does not take ownership of our feature so we clean it up when done with it.
         OGRFeature::DestroyFeature( poFeature );
    }
	
}

void CBaristaPopUpHandler::export3DSpacedTextFile()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += xyzpoints->getFileName()->GetFileName().GetChar();
    str += ".txt";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Space Separated Text File Name",
		qstr,
		"Text File Name (*.txt)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

    int type = 1; // switch for space/csv separation, space = 1

    xyzpoints->writeXYZPointTextFile(cfilename, type);

}


void CBaristaPopUpHandler::export3DCSVTextFile()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += xyzpoints->getFileName()->GetFileName().GetChar();
    str += ".csv";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Comma Separated Text File Name",
		qstr,
		"Text File Name (*.csv)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

    int type = 0; // switch for space/csv separation, csv = 0

    xyzpoints->writeXYZPointTextFile(cfilename, type);

}

void CBaristaPopUpHandler::exportDDMMSSsssFile()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;
	if (xyzpoints->getReferenceSystem()->getCoordinateType() != eGEOGRAPHIC)
	{
		QMessageBox::warning( this, "  Export to DD.MMSSssss impossible!",
		 "Transform the points to geographic coordinates first.");
	}
	else
	{
		CCharString str = inifile.getCurrDir();
		str += xyzpoints->getFileName()->GetFileName().GetChar();
		str += ".txt";
		QString qstr( str.GetChar());

 		QString s = QFileDialog::getSaveFileName(
			this,
			"Choose a DD.MMSSsss Text File Name",
			qstr,
			"Text File Name (*.txt)");

		if ( s.isNull())
			return;

		CFileName cfilename = (const char*)s.toLatin1();

		xyzpoints->writeXYZPointDDMMSSsssTextFile(cfilename);
	}
}

void CBaristaPopUpHandler::CopyToOtherList()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CObjectSelector selector;
    selector.makeXYZPointSelector();
    ZObject *o = selector.getSelection();
    CXYZPointArray *pCopyPts = (CXYZPointArray*)o;

    if(pCopyPts)
	{
		CTrans3D *trans = CTrans3DFactory::initTransformation(*(xyzpoints->getReferenceSystem()), *(pCopyPts->getReferenceSystem()));

		if (!trans)
		{
			QMessageBox::warning( this, "  Copy action is impossible!", "The reference systems are not compatible.");
		}
		else
		{
			CXYZPointArray ptsCpy;
			trans->transformArray(&ptsCpy, xyzpoints);
			delete trans;

			if (!pCopyPts->getIsSorted()) pCopyPts->forceSortPoints();

			for (int i = 0; i < ptsCpy.GetSize(); ++i)
			{
				CXYZPoint *pToBeAdded = ptsCpy.getAt(i);
				CXYZPoint *pExist;
				int indExist = pCopyPts->getPointIndexBinarySearch(pToBeAdded->getLabel());
				if (indExist < 0) pExist = pCopyPts->add();
				else pExist = pCopyPts->getAt(indExist);
				
				*pExist = *pToBeAdded;
			}
		}
	}
}

void CBaristaPopUpHandler::xyz_to_xyRPC()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CObjectSelector selector;
    selector.makeImageSelector(eRPC);

    vector<ZObject*> objects = selector.getMultiSelection();
	for (unsigned int i=0; i<objects.size(); i++)
	{
		CVImage* image = (CVImage*)objects[i];

		if (!image)
			continue;

		if (image->hasRPC())
		{
			eCoordinateType xyzsystem = xyzpoints->getReferenceSystem()->getCoordinateType();

			if ( xyzsystem != eGEOGRAPHIC )
			{
				QMessageBox::warning( this, "  3D to 2D not possible!"
				, "For RPC backprojection 3D points have to be defined in WGS84Geographic.");
				continue;
			}

			CXYPointArray* xy = image->getProjectedXYPoints();
			image->getRPC()->transformArrayObj2Obs(*xy, *xyzpoints);
			xy->setFileName("3Dto2DwithRPC");
		}
		else
		{
			QMessageBox::warning( this, "  3D to 2D with RPC not possible!"
			, "The selected image has no RPC parameters.");
			continue;
		}

	}
	baristaProjectHelper.refreshTree();

	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}
}


void CBaristaPopUpHandler::xyz_to_xyAffine()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CObjectSelector selector;
    selector.makeImageSelector();

    vector<ZObject*> objects = selector.getMultiSelection();
	for (unsigned int i=0; i<  objects.size(); i++)  
	{
		CVImage* image = (CVImage*)objects[i];

		if (!image)
			continue;

		if (image->hasAffine())
		{
			eCoordinateType affineType = image->getAffine()->getRefSys().getCoordinateType();
			eCoordinateType xyzType = xyzpoints->getReferenceSystem()->getCoordinateType();

			if ( affineType != xyzType)
			{
				QMessageBox::warning( this, "  3D to 2D not possible!"
				, "Affine parameters and 3D points are defined in different coordinate systems");
				continue;
			}

			CXYPointArray* xy = image->getProjectedXYPoints();
			image->getAffine()->transformArrayObj2Obs(*xy, *xyzpoints);
			xy->setFileName("3Dto2DwithAffine");
		}
		else
		{
			QMessageBox::warning( this, "  3D to 2D with Affine not possible!"
			, "The selected image has no Affine parameters.");
			continue;
		}

	}

	baristaProjectHelper.refreshTree();

	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}

}

void CBaristaPopUpHandler::xyz_to_xyTFW()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CObjectSelector selector;
    selector.makeImageSelector(eTFW);

    vector<ZObject*> objects = selector.getMultiSelection();
	for (unsigned int i=0; i < objects.size(); i++)
	{
		CVImage* image = (CVImage*)objects[i];

		if (!image)
			continue;

		if (image->getTFW()->gethasValues())
		{
			eCoordinateType tfwType = image->getTFW()->getRefSys().getCoordinateType();
			eCoordinateType xyzType = xyzpoints->getReferenceSystem()->getCoordinateType();

			if ( tfwType != xyzType)
			{
				QMessageBox::warning( this, "  3D to 2D not possible!"
				, "TFW parameters and 3D points are defined in different coordinate systems");
				continue;
			}

			if ( image->getTFW()->getNumberOfStationPars() == 4)
			{
				QMessageBox::warning( this, " 3D to 2D not possible!"
				, "Conformal Transformation to image coordinates not implemented");
				continue;
			}

			CXYPointArray* xy = image->getProjectedXYPoints();
			image->getTFW()->transformArrayObj2Obs(*xy, *xyzpoints);
			xy->setFileName("3Dto2DwithTFW");

		}
		else
		{
			QMessageBox::warning( this, "  3D to 2D with TFW not possible!"
			, "The selected image has no TFW parameters.");
			return;
		}
	}
	baristaProjectHelper.refreshTree();

	for (int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}

}


void CBaristaPopUpHandler::setControl()
{
	CXYZPointArray* points = (CXYZPointArray*)this->object;

	project.setControlPoints(points);
	baristaProjectHelper.refreshTree();

}


void CBaristaPopUpHandler::unsetControl()
{
	CXYZPointArray* points = (CXYZPointArray*)this->object;

	project.unsetControlPoints(points);
	baristaProjectHelper.refreshTree();
}



void CBaristaPopUpHandler::XYZtoGeographic()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

	CReferenceSystem refsys(*(xyzpoints->getReferenceSystem()));
	refsys.setCoordinateType(eGEOGRAPHIC);

    CXYZPointArray* Geographicpoints = project.addXYZPoints("3DtoGeographic", refsys, true, false,eNOTYPE);


    CTrans3D* trans = CTrans3DFactory::initTransformation(*(xyzpoints->getReferenceSystem()), *(Geographicpoints->getReferenceSystem()));

    if (trans)
	{
		trans->transformArray(Geographicpoints, xyzpoints);
		baristaProjectHelper.refreshTree();
		delete trans;
	}
}


void CBaristaPopUpHandler::XYZtoGeocentric()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

	CReferenceSystem refsys(*(xyzpoints->getReferenceSystem()));
	refsys.setCoordinateType(eGEOCENTRIC);
    CXYZPointArray* Geocentricpoints = project.addXYZPoints("3DtoGeocentric", refsys, true, false,eNOTYPE);

	CTrans3D* trans = CTrans3DFactory::initTransformation(*(xyzpoints->getReferenceSystem()), *(Geocentricpoints->getReferenceSystem()));

    if (trans)
	{
		trans->transformArray(Geocentricpoints, xyzpoints);
		baristaProjectHelper.refreshTree();
		delete trans;
	}
}


void CBaristaPopUpHandler::XYZtoUTM()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

  	CReferenceSystem refsys(*(xyzpoints->getReferenceSystem()));
	refsys.setCoordinateType(eGRID);

    CXYZPointArray *UTMpoints = project.addXYZPoints("3DtoUTM", refsys, true, false,eNOTYPE);

	if (xyzpoints->GetSize() > 0)
	{
		CXYZPoint p3D = *(xyzpoints->getAt(0));

		eCoordinateType type = xyzpoints->getReferenceSystem()->getCoordinateType();

		if (type == eGEOCENTRIC)
			xyzpoints->getReferenceSystem()->geocentricToGeographic(p3D, p3D);
		else if (type == eGRID) xyzpoints->getReferenceSystem()->gridToGeographic(p3D, p3D);

		UTMpoints->getReferenceSystem()->setUTMZone(p3D.x, p3D.y);
		if ((type == eGEOCENTRIC) || (type == eGEOGRAPHIC))
		{
			xyzpoints->getReferenceSystem()->setUTMZone(p3D.x, p3D.y);
		}

		CTrans3D* trans = CTrans3DFactory::initTransformation(*(xyzpoints->getReferenceSystem()), *(UTMpoints->getReferenceSystem()));

		if (trans)
		{
			trans->transformArray(UTMpoints, xyzpoints);
			baristaProjectHelper.refreshTree();
			delete trans;
		}
		else
		{
			project.deleteXYZPoints(UTMpoints);
			QMessageBox::critical(NULL,"Transformation from TM to UTM not possible!",
				"Use intermediate transformation via Geographic or Geocentric",1,0,0);
		}
	}
}


void CBaristaPopUpHandler::XYZtoTM()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

	if ( xyzpoints->getReferenceSystem()->getCoordinateType() == eGRID
		&& xyzpoints->getReferenceSystem()->isUTM())
	{
		QMessageBox::critical(NULL,"Transformation from UTM to TM not possible!",
			"Use intermediate transformation via Geographic or Geocentric.",1,0,0);
		return;
	}

	CReferenceSystem refsys;
    CXYZPointArray *TMpoints = project.addXYZPoints("3DtoTM", refsys, true, false,eNOTYPE);

	Bui_TMPropertiesDlg tmdlg(TMpoints->getReferenceSystem(), "TM Settings",  "UserGuide/Import3DPoints.html");;

	tmdlg.setModal(true);
	tmdlg.exec();

	double cm = TMpoints->getReferenceSystem()->getCentralMeridian();
	double fe = TMpoints->getReferenceSystem()->getEastingConstant();
	double fn = TMpoints->getReferenceSystem()->getNorthingConstant();
	double sc = TMpoints->getReferenceSystem()->getScale();
	double lo = TMpoints->getReferenceSystem()->getLatitudeOrigin();

	TMpoints->getReferenceSystem()->setTMParameters(cm, fe, fn, sc, lo);

	CTrans3D* trans = CTrans3DFactory::initTransformation(*(xyzpoints->getReferenceSystem()), *(TMpoints->getReferenceSystem()));

    if (trans)
	{
		trans->transformArray(TMpoints, xyzpoints);
		baristaProjectHelper.refreshTree();
		delete trans;
	}
	else
	{
		project.deleteXYZPoints(TMpoints);
	}
}


void CBaristaPopUpHandler::InterpolateDEM()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

	bui_ALSInterpolationDialog dlg(xyzpoints, "Interpolate DEM from XYZ Points", "UserGuide/ALSDEMInterpolate.html", true);
	dlg.exec();

	if (dlg.getSuccess())
	{
		CFileName fn = dlg.getFileName();
		CVDEM *vdem = project.addDEM(fn);
		int neighbourhood = dlg.getNeighbourhood();

		eInterpolationMethod method = dlg.getMethod();

		C2DPoint pmin = dlg.getMin();
		C2DPoint pmax = dlg.getMax();
		C2DPoint grid = dlg.getRes();

		double maxDist = dlg.getMaxDist();

		CProgressThread *thread = new CALSDataInterpolatorThread(xyzpoints, fn, vdem, neighbourhood,
			                                                     method, pmin, pmax, grid, maxDist);

		bui_ProgressDlg *pdlg = new bui_ProgressDlg(thread, vdem->getDEM(), "DEM Interpolation");
		pdlg->exec();

		bool ret = pdlg->getSuccess();
		delete pdlg;

		baristaProjectHelper.refreshTree();
	}
}

void CBaristaPopUpHandler::MatchGCPs()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*) this->object;

	bui_GCPMatchingDialog dlg(xyzpoints);
	if (!dlg.isInitialised())
	{
		QMessageBox::warning(0, "Matching of GCPs not possible!", 
			                 "Matching of GCPs requires an orthophoto with TFW node");
	}
	else
	{
		dlg.exec();
		if (dlg.getSuccess()) baristaProjectHelper.refreshTree();
	}
}

void CBaristaPopUpHandler::getEllipseCenters()
{
	CXYEllipseArray* ellipses = (CXYEllipseArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

	if ( imageitem->getObject()->isa("CVImage"))
	{
		CVImage* image = (CVImage*)imageitem->getObject();

		CXYPointArray* xypoints = image->getXYPoints();

		for ( int i = 0; i < ellipses->GetSize(); i++)
		{
			CXYPoint center = ellipses->GetAt(i)->getCenter();
			xypoints->Add(center);
		}
	}

}
void CBaristaPopUpHandler::exportEllipsestoFile()
{
	CXYEllipseArray* ellipses = (CXYEllipseArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += ellipses->getFileName()->GetFileName().GetChar();
    str += ".txt";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Text File Name",
		qstr,
		"Text File Name (*.txt)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	ellipses->writeEllipseTextFile(cfilename.GetChar());
}

void CBaristaPopUpHandler::exportEllipseCenterstoFile()
{
	CXYEllipseArray* ellipses = (CXYEllipseArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += ellipses->getFileName()->GetFileName().GetChar();
    str += ".txt";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Text File Name",
		qstr,
		"Text File Name (*.txt)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	ellipses->writeEllipseCentersTextFile(cfilename.GetChar());
}

void CBaristaPopUpHandler::writeImagePointChips()
{
	CXYPointArray* xypoints = (CXYPointArray*)this->object;

	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();
    CVImage* image = (CVImage*)imageitem->getObject();

	if ( !image )
		return;

	if ( xypoints->GetSize() <= 0 )
		return;

	bool hascontrol = false;
	if ( project.getControlPoints()) hascontrol = true;

	//Bui_ImageChipsDlg chipdlg(image, inifile.getCurrDir(),Bui_ImageChipsDlg::eUserDefinedVersion);

	//chipdlg.exec();

	//if (!chipdlg.getSuccess())	// false if user has pressed "Cancel"
	//	return;

	//if (!chipdlg.writeImageChips())	// actual writing failed
	//	QMessageBox::critical(NULL,"Write Image Chips - Error","Barista couldn't write image chips!",1,0,0);

}

void CBaristaPopUpHandler::xyz_to_xyPushbroom()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CObjectSelector selector;

    selector.makeImageSelector(ePUSHBROOM);

    vector<ZObject*> objects = selector.getMultiSelection();
	vector<CVImage*> images;
    for (unsigned int i=0; i< objects.size(); i++)
	{
		CVImage* image = (CVImage*)objects[i];

		if (!image)
			continue;

		images.push_back(image);
	}
	
	if (images.size() && xyzpoints)
	{
		CCharString erorMessage;
		bool ret = project.backProjectXYZPoints(images,*xyzpoints,erorMessage,false,false);

		if (!ret)
			QMessageBox::warning( this, "  3D to 2D not possible for image(s)!",erorMessage.GetChar());


		baristaProjectHelper.refreshTree();

		for ( int i = 0; i < workspace->subWindowList().count(); i++)
		{
			workspace->subWindowList().at(i)->update();
		}

			
	}


}


void CBaristaPopUpHandler::SetRPCCurrentModel()
{
	CRPC* rpc = (CRPC*)this->object;
	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

	if ( imageitem )
	{
		CVImage* image = (CVImage*)imageitem->getObject();
		project.setCurrentSensorModelofImage(image, eRPC);
	}
	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::SetAffineCurrentModel()
{
	CAffine* affine = (CAffine*)this->object;
	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

	if ( imageitem )
	{
		CVImage* image = (CVImage*)imageitem->getObject();
		project.setCurrentSensorModelofImage(image, eAFFINE);
	}
	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::SetPushBroomScannerCurrentModel()
{
	CPushBroomScanner* pushbroomscanner = (CPushBroomScanner*)this->object;
	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

	if ( imageitem )
	{
		CVImage* image = (CVImage*)imageitem->getObject();
		project.setCurrentSensorModelofImage(image, ePUSHBROOM);
	}

	baristaProjectHelper.refreshTree();
}


void CBaristaPopUpHandler::exportBuildingsDXF()
{
#ifdef WIN32	
	CBuildingArray* buildings = (CBuildingArray*)this->object;

    Bui_BuildingDXF DdxfExport(buildings->getFileName()->GetFileName());

	DdxfExport.exec();

	if (DdxfExport.getSuccess())
	{
		buildings->writeBuildingDXFFile(DdxfExport.getFileName(), DdxfExport.getRelativeHeights());
	};
#endif	
}

//addition >>
void CBaristaPopUpHandler::exportXYZPolyLinesShapefile()
{
	CXYZPolyLineArray* xyzlines = (CXYZPolyLineArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += xyzlines->getFileName()->GetFileName().GetChar();
    str += ".shp";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a Shapefile Name",
		qstr,
		"Shapefile (*.shp)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	//xyzlines->writeXYZPolyLineShapefile(cfilename);

	// above method is not working for Final Release due to inclusion of ogrsf_frmts.h file, 2 errors found in BaristaBatch project 

	// source: http://gdal.org/ogr/ogr_apitut.html
	// we start by registering all the drivers, 
	// and then fetch the Shapefile driver as we will need it to create our output file. 
	const char *pszDriverName = "ESRI Shapefile";

	/* DEPRECEATED
	
    OGRSFDriver *poDriver;

    OGRRegisterAll();

    poDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName(
                pszDriverName );
    if( poDriver == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Driver not found!");
		return;
    }

	// Next we create the datasource.
	OGRDataSource *poDS;

    poDS = poDriver->CreateDataSource((const char *) cfilename, NULL );
    if( poDS == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Datasource creation failed!");
		return;
    }

	// Now we create the output layer.
	 OGRLayer *poLayer;

    poLayer = poDS->CreateLayer("XYZPolylines", NULL, wkbLineString25D, NULL );
    if( poLayer == NULL )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Layer creation failed!");
		return;
    }
	*/

	GDALDriver *poDriver;

	GDALAllRegister();

	poDriver = GetGDALDriverManager()->GetDriverByName(
		pszDriverName);
	if (poDriver == NULL)
	{
		//QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Driver not found!");
		return;
	}

	// Next we create the datasource.
	GDALDataset *poDS;

	poDS = poDriver->Create((const char *)cfilename, 0, 0, 0, GDT_Unknown, NULL);
	if (poDS == NULL)
	{
		//QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Datasource creation failed!");
		return;
	}

	// Now we create the output layer.
	OGRLayer *poLayer;

	poLayer = poDS->CreateLayer("XYZPolylines", NULL, wkbLineString25D, NULL);
	if (poLayer == NULL)
	{
		//QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Layer creation failed!");
		return;
	}

	OGRFieldDefn lField( "Name", OFTString);
	lField.SetWidth(32);

	if( poLayer->CreateField( &lField ) != OGRERR_NONE )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Data field creation failed!");
		return;
    }

	OGRFieldDefn hField( "Height", OFTReal);
    hField.SetPrecision(3);


    if( poLayer->CreateField( &hField ) != OGRERR_NONE )
    {
        //QMessageBox::warning(NULL, " Writing to shapefile!"
		//, "Data field creation failed!");
		return;
    }


	//double x, y;
    //char szName[33];
	//const char* buf;
    double x, y, z;//, tx[2], ty[2];
	double ht, fH, lH;
	

	for (int i = 0; i < xyzlines->GetSize(); i++)
	{
		OGRLineString ls;

		CXYZPolyLine* line = xyzlines->getAt(i);
		int nPoint = line->getPointCount();
		for (int j = 0; j < nPoint; j++)
		{
			CObsPoint* point = line->getPoint(j);
			x = (*point)[0];
			y = (*point)[1];
			z = (*point)[2];

			ls.addPoint(x,y,z);
			if (j == 0)
				fH = z;
			else if (j == nPoint-1)
				lH = z;
		}

		OGRFeature *poFeature;
        poFeature = OGRFeature::CreateFeature( poLayer->GetLayerDefn() );
		poFeature->SetField("Name", line->getLabel().GetChar());
		ht = lH - fH;
        poFeature->SetField("Height", ht);
		

		poFeature->SetGeometry( &ls ); 
		if( poLayer->CreateFeature( poFeature ) != OGRERR_NONE )
        {
           //QMessageBox::warning(NULL, " Writing to shapefile!"
			//, "Feature creation failed!");
			return;
        }
		// Now we create a feature in the file. The OGRLayer::CreateFeature() 
		// does not take ownership of our feature so we clean it up when done with it.
		OGRFeature::DestroyFeature( poFeature );
	}
	// Finally we need to close down the datasource in order to ensure headers are 
	// written out in an orderly way and all resources are recovered.
    // OGRDataSource::DestroyDataSource( poDS ); // DEPRECEATED
	GDALClose(poDS);

}
//<< addition

void CBaristaPopUpHandler::exportXYZPolyLinesDXF()
{
	CXYZPolyLineArray* xyzlines = (CXYZPolyLineArray*)this->object;

    CCharString str = inifile.getCurrDir();
    str += xyzlines->getFileName()->GetFileName().GetChar();
    str += ".dxf";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a DXF-File Name",
		qstr,
		"DXF-File (*.dxf)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	xyzlines->writeXYZPolyLineDXFFile(cfilename);
}

void CBaristaPopUpHandler::TransformXYZPolyLines3D()
{
	CXYZPolyLineArray* xyzlines = (CXYZPolyLineArray*)this->object;

	CReferenceSystem oldRefSys(*(xyzlines->getReferenceSystem()));

	CReferenceSystem newRefSys(oldRefSys);

	Bui_ReferenceSysDlg dlg(&newRefSys, xyzlines->getFileName(), "Transform XYZPolylines", "CoordinateSysDlg.html",  false,0,true,false);
	dlg.exec();
	if (dlg.getSuccess()) 
	{
		CTrans3D *pTrafo = CTrans3DFactory::initTransformation(oldRefSys,newRefSys);
		if (!pTrafo)
		{
			QMessageBox::warning( this, "XYZPolylines cannot be transformed", "Incompatible definition of reference system parameters.");
		}
		else
		{
			for (int i = 0; i < xyzlines->GetSize(); ++i)
			{
				CXYZPolyLine *pPoly = xyzlines->getAt(i);

				for (int j = 0; j < pPoly->getPointCount(); ++j)
				{
					CXYZPoint *pP = pPoly->getPoint(j);
					pTrafo->transform(*pP, *pP);
				}
			}
			xyzlines->setReferenceSystem(newRefSys);
			delete pTrafo;
		}
	}
}

void CBaristaPopUpHandler::setDEMActive()
{
	CVDEM* dem = (CVDEM*)this->object;

	project.setActiveDEM(dem);
	baristaProjectHelper.refreshTree();

	//addition >>
	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}
	//<< addition
}

void CBaristaPopUpHandler::unsetDEMActive()
{
	project.unsetActiveDEM();
	baristaProjectHelper.refreshTree();

	//addition >>
	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}
	//<< addition
}


void CBaristaPopUpHandler::ExportPushBroomScannerRPC()
{
	CPushBroomScanner* scanner = (CPushBroomScanner*)  this->object;

	CVImage *pbImg = 0;

	for (int i = 0; i < project.getNumberOfImages() && !pbImg; ++i)
	{
		CVImage *img = project.getImageAt(i);
		if (img->getPushbroom() == scanner) pbImg = img;
	}

	if (pbImg)
	{
		Bui_GenerateRPC dlg(this);
		dlg.exec();


		if (dlg.getSuccess())
		{
			double rms = pbImg->initRPCs(*scanner, dlg.getZMin(), dlg.getZMax());


			if (rms < 0.0f)
			{
				QMessageBox::warning( this, "RPC Generation was not successful",
											"RPC Generation was not successful.");
			}
			else
			{
				ostrstream oss;
				oss.setf(ios::fixed, ios::floatfield);
				oss.precision(3);
				oss << "RPC Generation was successful; s0: " << rms << ends;
				char *z = oss.str();
				CCharString str(z);
				delete [] z;
				QMessageBox::information( this, "RPC Generation successful!", str.GetChar());
				baristaProjectHelper.refreshTree();
			}
		}
	}
	else
	{
		QMessageBox::warning( this, "RPC Generation was not successful",
									"No corresponding image was found for Pushbroom Scanner.");
	}
};

void CBaristaPopUpHandler::DisplayDEMProperties()
{
	CVDEM* vdem = (CVDEM*)object;
	CDEMTable* demTable = new CDEMTable(vdem,workspace);
	QWidget *widget = workspace->addSubWindow(demTable);
	widget->setAttribute(Qt::WA_DeleteOnClose);
	demTable->show();
/*
    if (!demTable->isVisible()) demTable->show();
	else demTable->activateWindow();
*/
	workspace->tileSubWindows();
}

void CBaristaPopUpHandler::applyLookupTable()
{
	CVImage* image = (CVImage*)this->object;
	CHugeImage* himage = image->getHugeImage();
	
	if ( !himage ) return;

	QString fn = QFileDialog::getSaveFileName(this,"Select new filename for huge image",inifile.getCurrDir(),"*.hug",0);

	if (fn.isEmpty())
		return;


	CVImage* newImage = project.addImage(fn.toLatin1().constData(),false);
	CHugeImage* newHImage = newImage->getHugeImage();
	newHImage->setFileName(fn.toLatin1().constData());

	bui_ProgressDlg dlg(new CApplyTransferFunctionThread(himage,newHImage,true),newHImage,"Applying transfer function ..");

	dlg.exec();
	
	if (dlg.getSuccess())
		baristaProjectHelper.refreshTree();

}

void CBaristaPopUpHandler::importCamera()
{
	CVImage* image = (CVImage*)this->object;

	if (!image)	return;

	Bui_FrameCameraModelDlg* dlg = new Bui_FrameCameraModelDlg(image->getFrameCamera(),false,0);

	// QT takes care of the memory!
	Bui_DialogContainer* dlgContainer = new Bui_DialogContainer(dlg,0,600,400);
	
	dlgContainer->exec();

	if (dlg->getSuccess())
	{
		image->setCurrentSensorModel(eFRAMECAMERA);
		if (project.getImageAt(0))
		{
			if (project.getImageAt(0)->hasFrameCamera())
			{
				image->getFrameCamera()->setRefSys(project.getImageAt(0)->getFrameCamera()->getRefSys());
			}
		}

		baristaProjectHelper.refreshTree();
	}
}

void CBaristaPopUpHandler::generateEpipolarImages()
{
	CVImage* image = (CVImage*)this->object;
	if (!image)	return;

	bui_GenerateEpipolarDialog dlg(image);
	dlg.exec();
	if (dlg.getSuccess()) 
	{
		baristaProjectHelper.refreshTree();
	}
}

void CBaristaPopUpHandler::writeRPCtoImageFile()
{
	CVImage* image = (CVImage*)this->object;
	if (!image)	return;
	CFileName fname(image->getFileNameChar());	
	CFileName ext(fname.GetFileExt());
	if (ext.CompareNoCase("ntf"))
	{
		CFileName fullname = fname.GetFileName();
		CFileName *hfname = image->getHugeImage()->getFileName();
		CFileName path = hfname->GetPath();
		CFileName fullfname = path + CSystemUtilities::dirDelimStr + fullname + "." + ext;

		CRPC *rpc = image->getRPC();
		//char *rpcs = findRPCstring(rpc);

		//////////////////////////
		//char *rpcs = new char[1026];//, *temp;
		double dvals;
		long lvals;
		//bool sign; // true for + & false for -
		CCharString ch(""), tempc;

		Matrix parms;
		rpc->getAllParameters(parms);
		
		//line offset
		lvals = (long) parms.element(80, 0); 
		CCharString ch1(lvals,6,true);
		ch = ch+ch1;

		//sample offset
		lvals = (long) parms.element(81, 0);
		CCharString ch2(lvals,5,true);
		ch = ch+ch2;

		//latitude offset
		dvals = parms.element(82, 0);
		CCharString ch3(dvals,8,4,true,true);	
		ch = ch+ch3;

		//longitude offset
		dvals = parms.element(83, 0);	
		CCharString ch4(dvals,9,4,true,true);	
		ch = ch+ch4;

		//height offset
		lvals = (long) parms.element(84, 0);	
		CCharString ch5(lvals,5,true,true);
		ch = ch+ch5;

		//line sacle
		lvals = (long) parms.element(85, 0);	
		CCharString ch6(lvals,6,true);
		ch = ch+ch6;

		//sample sacle
		lvals = (long) parms.element(86, 0);	
		CCharString ch7(lvals,5,true);
		ch = ch+ch7;

		//latitude scale
		dvals = parms.element(87, 0);
		CCharString ch8(dvals,8,4,true,true);	
		ch = ch+ch8;

		//longitude scale
		dvals = parms.element(88, 0);	
		CCharString ch9(dvals,9,4,true,true);	
		ch = ch+ch9;

		//height scale
		lvals = (long) parms.element(89, 0);	
		CCharString ch10(lvals,5,true,true);
		ch = ch+ch10;

		//each of 80 line & sample numerator & denominator is 12 bytes long: format +x.xxxxxxE-x
		CCharString ch0;
		CCharString LineNum("");
		for (int i = 0; i < 20; i++)
		{
			dvals = parms.element(i, 0);	
			ch0.setScientificNumber(dvals,6);
			LineNum = LineNum+ch0;
		}

		CCharString LineDen("");
		for (int i = 0; i < 20; i++)
		{
			dvals = parms.element(20+i, 0);	
			ch0.setScientificNumber(dvals,6);
			LineDen = LineDen+ch0;
		}

		CCharString SamNum("");
		for (int i = 0; i < 20; i++)
		{
			dvals = parms.element(40+i, 0);	
			ch0.setScientificNumber(dvals,6);
			SamNum = SamNum+ch0;
		}

		CCharString SamDen("");
		for (int i = 0; i < 20; i++)
		{
			dvals = parms.element(60+i, 0);	
			ch0.setScientificNumber(dvals,6);
			SamDen = SamDen+ch0;
		}

		//rpcs = (char *)ch.GetChar();
		//strcat(rpcs,LineNum.GetChar());
		//strcat(rpcs,LineDen.GetChar());
		//strcat(rpcs,SamNum.GetChar());
		//strcat(rpcs,SamDen.GetChar());

	//rpcs[1026] = '\0';
	//return rpcs;

	/////////////////////////
		//char *rd = new char[7];
		FILE * pFile;
		pFile = fopen (fullfname , "r+" );
		//fseek ( pFile , 1012 , SEEK_SET );
		//fgets(rd,6,pFile);
		fseek ( pFile , 1012 , SEEK_SET );		
		fputs (ch.GetChar(), pFile );
		fputs (LineNum.GetChar(), pFile );
		fputs (LineDen.GetChar(), pFile );
		fputs (SamNum.GetChar(), pFile );
		fputs (SamDen.GetChar(), pFile );
		fclose ( pFile );

		//delete []rd;
		//delete []rpcs;

		//int here = 1;
	}
	else
		QMessageBox::warning(this,"Write error!","This option is only for NITF file!");
}

//the following function probably has not been used
char *CBaristaPopUpHandler::findRPCstring(CRPC *rpc)
{
	char *rpcs = new char[1026];//, *temp;
	double dvals;
	long lvals;
	//bool sign; // true for + & false for -
	CCharString ch(""), tempc;

	Matrix parms;
	rpc->getAllParameters(parms);
	
	//line offset
	lvals = (long) parms.element(80, 0); 
	CCharString ch1(lvals,6,true);
	ch = ch+ch1;

	//sample offset
	lvals = (long) parms.element(81, 0);
	CCharString ch2(lvals,5,true);
	ch = ch+ch2;

	//latitude offset
	dvals = parms.element(82, 0);
	CCharString ch3(dvals,8,4,true,true);	
	ch = ch+ch3;

	//longitude offset
	dvals = parms.element(83, 0);	
	CCharString ch4(dvals,9,4,true,true);	
	ch = ch+ch4;

	//height offset
	lvals = (long) parms.element(84, 0);	
	CCharString ch5(lvals,5,true,true);
	ch = ch+ch5;

	//line sacle
	lvals = (long) parms.element(85, 0);	
	CCharString ch6(lvals,6,true);
	ch = ch+ch6;

	//sample sacle
	lvals = (long) parms.element(86, 0);	
	CCharString ch7(lvals,5,true);
	ch = ch+ch7;

	//latitude scale
	dvals = parms.element(87, 0);
	CCharString ch8(dvals,8,4,true,true);	
	ch = ch+ch8;

	//longitude scale
	dvals = parms.element(88, 0);	
	CCharString ch9(dvals,9,4,true,true);	
	ch = ch+ch9;

	//height scale
	lvals = (long) parms.element(89, 0);	
	CCharString ch10(lvals,5,true,true);
	ch = ch+ch10;

	//each of 80 line & sample numerator & denominator is 12 bytes long: format +x.xxxxxxE-x
	CCharString ch0;
	CCharString LineNum("");
	for (int i = 0; i < 20; i++)
	{
		dvals = parms.element(i, 0);	
		ch0.setScientificNumber(dvals,6);
		LineNum = LineNum+ch0;
	}

	CCharString LineDen("");
	for (int i = 0; i < 20; i++)
	{
		dvals = parms.element(20+i, 0);	
		ch0.setScientificNumber(dvals,6);
		LineDen = LineDen+ch0;
	}

	CCharString SamNum("");
	for (int i = 0; i < 20; i++)
	{
		dvals = parms.element(40+i, 0);	
		ch0.setScientificNumber(dvals,6);
		SamNum = SamNum+ch0;
	}

	CCharString SamDen("");
	for (int i = 0; i < 20; i++)
	{
		dvals = parms.element(60+i, 0);	
		ch0.setScientificNumber(dvals,6);
		SamDen = SamDen+ch0;
	}

	rpcs = (char *)ch.GetChar();
	strcat(rpcs,LineNum.GetChar());
	strcat(rpcs,LineDen.GetChar());
	strcat(rpcs,SamNum.GetChar());
	strcat(rpcs,SamDen.GetChar());

	//rpcs[1026] = '\0';
	return rpcs;
}

void CBaristaPopUpHandler::computeInnerOrientation()
{
	CVImage* image = (CVImage*)this->object;
	if (!image)	return;

	if (image->computeInnerOrientation())
		baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::xyz_to_xyFrameCamera()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CObjectSelector selector;
    selector.makeImageSelector(eFRAMECAMERA);

    vector<ZObject*> objects = selector.getMultiSelection();
    for (unsigned int i=0; i< objects.size(); i++)
	{
		CVImage* image = (CVImage*)objects[i];

		if (!image)
			continue;

		if (image->hasFrameCamera())
		{
	/*		eCoordinateType xyzsystem = xyzpoints->getReferenceSystem()->getCoordinateType();

			if ( xyzsystem != eGEOCENTRIC || !xyzpoints->getReferenceSystem()->isWGS84() )
			{
				QMessageBox::warning( this, "  3D to 2D not possible!"
				, "Only geocentric coordinates in WGS 84 possible at the moment!");
				return;
			}
	*/
			CXYPointArray* xy = image->getProjectedXYPoints();
			image->getFrameCamera()->transformArrayObj2Obs(*xy, *xyzpoints);
			xy->setFileName("3Dto2DwithFrameCamera");
		}
		else
		{
			QMessageBox::warning( this, "  3D to 2D with Frame camera not possible!"
			, "The selected image has no Frame camera Sensor Model.");
			continue;
		}
	}

	baristaProjectHelper.refreshTree();

	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}
}

void CBaristaPopUpHandler::SetFrameCameraCurrentModel()
{
	CFrameCameraModel* frameCamera = (CFrameCameraModel*)this->object;
	CBaristaIconViewItem* imageitem = (CBaristaIconViewItem*)this->treewidgetitem->parent();

	if ( imageitem )
	{
		CVImage* image = (CVImage*)imageitem->getObject();
		project.setCurrentSensorModelofImage(image, eFRAMECAMERA);
	}

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::ExportFrameCameraProjectionMatrix()
{
	CFrameCameraModel* frameCamera = (CFrameCameraModel*)this->object;
	
	CCharString str = inifile.getCurrDir();
    str += frameCamera->getFileName()->GetFileName().GetChar();
    str += "_ProjectionMatrix.dat";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a File Name",
		qstr,
		"data file (*.dat)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	Matrix P = frameCamera->getProjectionMatrix();

	ofstream outFile(cfilename.GetChar());
	outFile.setf(ios::fixed, ios::floatfield);
	outFile.precision(20);

	int w = fabs(log(P.MaximumAbsoluteValue())) + 22;

	for (int r = 0; r < P.Nrows(); ++r)
	{
		for (int c = 0; c < P.Ncols(); ++c)
		{
			outFile.width(w);
			outFile << P.element(r,c) << " ";
		}
		outFile << "\n";
	}
}

void CBaristaPopUpHandler::ExportFrameCamera()
{
	CFrameCameraModel* frameCamera = (CFrameCameraModel*)this->object;
	
	CCharString str = inifile.getCurrDir();
    str += frameCamera->getFileName()->GetFileName().GetChar();
    str += "_ori.dat";
    QString qstr( str.GetChar());

 	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a File Name",
		qstr,
		"data file (*.dat)");

    if ( s.isNull())
        return;

    CFileName cfilename = (const char*)s.toLatin1();

	ofstream outFile(cfilename.GetChar());
	outFile << frameCamera->getDescriptorString();
}

void CBaristaPopUpHandler::ImportFrameCameraOrientation1()
{
	CFrameCameraModel* frameCamera = (CFrameCameraModel*)this->object;


	QString s = QFileDialog::getOpenFileName(this, "Select File Containing the Exterior Orientation ", inifile.getCurrDir());

 	
    if (!s.isNull())
	{
		CFileName cfilename = (const char*)s.toLatin1();
		project.initExteriorOrientationOfFrameCameras(cfilename.GetChar(), false);
		baristaProjectHelper.refreshTree();
	}
}

void CBaristaPopUpHandler::ImportFrameCameraOrientation2()
{
	CFrameCameraModel* frameCamera = (CFrameCameraModel*)this->object;


	QString s = QFileDialog::getOpenFileName(this, "Select File Containing the Exterior Orientation ", inifile.getCurrDir());

 	
    if (!s.isNull())
	{
		CFileName cfilename = (const char*)s.toLatin1();
		project.initExteriorOrientationOfFrameCameras(cfilename.GetChar(), true);
		baristaProjectHelper.refreshTree();
	}
}


void CBaristaPopUpHandler::FrameCameraSetRefsys()
{
	CFrameCameraModel* frameCamera = (CFrameCameraModel*)this->object;

	CFileName filename(*frameCamera->getFileName());
	CReferenceSystem refSys = frameCamera->getRefSys();

	Bui_ReferenceSysDlg dlg(&refSys,&filename, "Set Reference System", "CoordinateSysDlg.html", false, 0, true, false);
	dlg.exec();
	if (dlg.getSuccess()) 
	{
		frameCamera->setRefSys(refSys);
	};
}

void CBaristaPopUpHandler::transformDEM()
{
	CVDEM* vdem = (CVDEM*)this->object;

	Bui_DEMTransformDlg dlg(vdem);
	dlg.exec();

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::demOperation()
{
	CVDEM* vdem = (CVDEM*)this->object;

	Bui_DEMOperationsDlg dlg(vdem);
	dlg.exec();

	baristaProjectHelper.refreshTree();
}

void CBaristaPopUpHandler::createDEMLegend(QWidget *Parent, CVDEM* vdem)
{
	QDialog *legend = vdem->getLegend();
	if (legend)
	{
		legend->close();
		delete legend;
		legend = NULL;
		vdem->setLegend(legend);
	}
	
	vector<int> red,green,blue;
	vector<CCharString> text;

	CLookupTableHelper helper;
	
	if (helper.createColorLegend(vdem->getDEM()->getTransferFunction(),
								 vdem->getDEM()->getHistogram()->getMin(),
								 vdem->getZeroValue(),
								 vdem->getDEM()->getHistogram()->getMax(),
								 red,
								 green,
								 blue,
								 text,
								 vdem->getNLegendeClasses()))
	{
		
		legend = new QDialog(Parent);

		// load and set window icon
		QPixmap icon;
		icon.loadFromData(Barista, sizeof(Barista), "PNG");
		legend->setWindowIcon(QIcon(icon));

		QGridLayout* layout = new QGridLayout;		
		
		
		for (int i=red.size() -1, k=0; i >=0 && k < red.size() ; i--,k++)
		{
			QLineEdit *lE = new QLineEdit;
			QPalette p = lE->palette();
			QColor color(red[i],green[i],blue[i]);
			p.setColor(QPalette::Base,color);
			lE->setPalette(p);
			lE->setReadOnly(true);

			lE->setAlignment(Qt::AlignCenter);
			if (vdem->getStatHistogram().getSize())
			{
				lE->setMinimumWidth(120);
				lE->setMaximumWidth(120);
				lE->setText(QString().sprintf("%7ld (%5.2lf %%)",vdem->getStatHistogram().value(i),vdem->getStatHistogram().getPercentage(i)*100));
			}
			else
			{
				lE->setMinimumWidth(40);
				lE->setMaximumWidth(40);
			}
			QLabel* lB = new QLabel(QString(text[i].GetChar()));
			

			lB->setFont(QFont("Courier", 10, 75));
			layout->addWidget(lE,k,0);
			layout->addWidget(lB,k,1);
			
		}
		legend->setLayout(layout);
		legend->setWindowTitle("Legend");
		QPalette p = legend->palette();
		QColor bg(Qt::white);
		p.setColor(QPalette::Base,bg);
		legend->setPalette(p);

		legend->show();
		vdem->setLegend(legend);
	}

	
}
void CBaristaPopUpHandler::DisplayDEMStatistcs()
{

	CVDEM* vdem = (CVDEM*) this->object;

	Bui_DEMStatistics dlg(vdem,this);
	dlg.exec();

	if (dlg.getSuccess())
	{
		baristaProjectHelper.setIsBusy(true);
		int txtCol[3], linCol[3];
		txtCol[0] = dlg.getStatTextColor().red();
		txtCol[1] = dlg.getStatTextColor().green();
		txtCol[2] = dlg.getStatTextColor().blue();
		linCol[0] = dlg.getStatLineColor().red();
		linCol[1] = dlg.getStatLineColor().green();
		linCol[2] = dlg.getStatLineColor().blue();
		vdem->computeDEMStatistics(	dlg.getNStatisticRows(),
									dlg.getNStatisticCols(),
									dlg.getShowAvg(),
									dlg.getShowDevAvg(),
									dlg.getShowMin(),
									dlg.getShowMax(),
									dlg.getShowDev(),
									txtCol, linCol,
									dlg.getStatFontSize(),
									dlg.getNLegendeClasses(),
									dlg.getComputeHist());

		createDEMLegend(thumbnailView, vdem);
		baristaProjectHelper.setIsBusy(false);
	}
}

void CBaristaPopUpHandler::DeleteDEMStatistcs()
{
	CVDEM* vdem = (CVDEM*) this->object;
	vdem->deleteDEMStatistics();
}


void CBaristaPopUpHandler::pointArraySelector()
{
	CObsPointArray* a = (CObsPointArray*)this->object;
	ViewParameter viewParameter;
	viewParameter.showSelector = false;
	viewParameter.showSigma = false;
	viewParameter.showCoordinates = false;
	viewParameter.allowSigmaChange = false;
	viewParameter.allowLabelChange = false;
	viewParameter.allowDeletePoint = false;
	viewParameter.allowBestFitEllipse = false;
	viewParameter.allowXYZAveragePoints = false;


	CObsPointDataModel* dataModel = new CObsPointDataModel(a,viewParameter);
	CObjectSelectorTable* table = new CObjectSelectorTable(dataModel,true,NULL);
	Bui_ObjectTableDlg* dlg = new Bui_ObjectTableDlg(table,workspace);
	dlg->show();

	return;
}

void CBaristaPopUpHandler::createOrbitPathPopUpMenu(COrbitObservations* orbitPath)
{
	this->orbitPathAction = new QAction(tr("&Compute Path points"), this);
    connect(orbitPathAction, SIGNAL(triggered()), this, SLOT(computeOrbitPathPoints()));
	this->addAction(orbitPathAction);
}





void CBaristaPopUpHandler::computeOrbitPathPoints()
{
	if (this->object->isa("COrbitObservations"))
	{
		COrbitObservations* orbit = (COrbitObservations*) this->object;
		
		
		if (orbit->getSplineType() == Path && orbit->getCurrentSensorModel())
		{
			CFileName arrayName("Orbit Path Points");	
			CXYZPointArray* points = project.addXYZPoints(arrayName,orbit->getCurrentSensorModel()->getRefSys(),true,false,eDIFFERENCED);

			
			if (points)
			{
				COrbitPathModel* path = (COrbitPathModel*)orbit->getCurrentSensorModel();
				CXYZPoint location;
				int nPoints = 9;
				double dt = (path->getLastTime() - path->getFirstTime()) / (nPoints);
				double time = path->getFirstTime();
				CLabel pointLabel = "OrbitPoint_";
				for (int i=0; i < nPoints; i++)
				{
					
					if (path->evaluateLocation(location,time))
					{
						pointLabel++;
						location.setLabel(pointLabel);					
						points->add(location);
					}
					
					time += dt;
				}

			}
			
			baristaProjectHelper.refreshTree();

		}
	}
}


void CBaristaPopUpHandler::createOrbitAttitudePopUpMenu(COrbitObservations* orbitAttitudes)
{
#ifndef FINAL_RELEASE 
	this->importPreciseALOSData = new QAction(tr("&Import ALOS precise orbit data"), this);
    connect(importPreciseALOSData, SIGNAL(triggered()), this, SLOT(importPreciseALOSOrbitData()));
	this->addAction(importPreciseALOSData);
#endif
}

void CBaristaPopUpHandler::importPreciseALOSOrbitData()
{
	QString imageFile = QFileDialog::getOpenFileName(this,"Select image file using the selected attitudes",inifile.getCurrDir());
	
	if (imageFile.isEmpty())
		return;

	QString dirAtt = QFileDialog::getExistingDirectory(this,"Select directory with ALOS orbit attitude precision data",inifile.getCurrDir());
	
	if (dirAtt.isEmpty())
		return;


	QString dirPath = QFileDialog::getExistingDirectory(this,"Select directory with ALOS orbit path precision data",inifile.getCurrDir());
	
	if (dirPath.isEmpty())
		return;

	COrbitObservations* attitudes = (COrbitObservations*) this->object;
	CPushBroomScanner* scanner = project.getPushBroomScannerForOrbit(attitudes);
	
	CAlosReader reader(true);
	CFileName directoryAttitudes(dirAtt.toLatin1().constData());
	CFileName directoryPath(dirPath.toLatin1().constData());
	CFileName file(imageFile.toLatin1().constData());
	
	if (!scanner || !reader.readPrecisionData(file,directoryAttitudes,directoryPath,scanner))
	{
		QMessageBox::warning(0,"Import Error",reader.getErrorMessage().GetChar());
	}

}

void CBaristaPopUpHandler::mergeOrbits()
{
	CPushBroomScanner* scanner = (CPushBroomScanner*)this->object;
	
	CObjectSelector selector;

	selector.makeOrbitMergeSelector(scanner);

	vector<ZObject*> data= selector.getMultiSelection();
	
	vector<CPushBroomScanner*> toMerge;
	toMerge.push_back(scanner);

	for (unsigned int i=0; i < data.size(); i++)
	{
		CVImage* vimage = (CVImage*)data[i];
		toMerge.push_back(vimage->getPushbroom());
	}

	if (toMerge.size() > 1 && project.mergeOrbits(toMerge))
	{
		baristaProjectHelper.refreshTree();
	}


}

//addition>> M. Awrangjeb, 19 Mar 2009
void CBaristaPopUpHandler::Get3DResidual()
{
	CXYZPointArray* xyzpoints = (CXYZPointArray*)this->object;

    CObjectSelector selector;
	selector.makeImageSelector(eUNDEFINEDSENSOR); // eRPC for GeoEye project, ePUSHBROOM for ALOS and QuickBird, set eUNDEFINEDSENSOR for any sensor type select 1 image at a time
	//eRPC: for selecting more than one images, eUNDEFINEDSENSOR: for selecting one image at a time only
    vector<ZObject*> objects = selector.getMultiSelection();
	for (unsigned int i=0; i<objects.size(); i++)
	{
		CVImage* image = (CVImage*)objects[i];

		if (!image)
			continue;

			CXYPointArray* xy3DResArray = image->getXY3DResiduals();
			CXYPointArray* xyPointArray = image->getXYPoints();
			xy3DResArray->setFileName("3DResidualsDiff");
			xy3DResArray->RemoveAll();
			xy3DResArray->setPointType(OBJRESIDUALS);
			for (int j = 0; j<xyPointArray->GetSize(); j++)
			{
				
				CObsPoint* XYPoint1 = xyPointArray->GetAt(j);
				CObsPoint* XYZPoint = (CObsPoint*)xyzpoints->getPoint(XYPoint1->getLabel().GetChar());
				
				if (XYPoint1 && XYZPoint)
				{
					CObsPoint* XYPoint = xy3DResArray->Add();
					(*XYPoint)[0] = (*XYZPoint)[0];
					(*XYPoint)[1] = (*XYZPoint)[1];
					XYPoint->setDot((*XYZPoint)[2]);

					XYPoint->setLabel(XYPoint1->getLabel());
				}
				
				int here = 0;
			}	
	}
	baristaProjectHelper.refreshTree();

	for ( int i = 0; i < workspace->subWindowList().count(); i++)
	{
		workspace->subWindowList().at(i)->update();
	}
}
//<<addition

void CBaristaPopUpHandler::setPyramidFile()
{
	QString filename = QFileDialog::getOpenFileName(
						this,
						"Select Barista Pyramid File",
						inifile.getCurrDir(),
						"Barista pyramid file (*.hug)");

    // make sure the file exists
    QFile test(filename);
    if (!test.exists())
        return;
	test.close();

	CVImage* vimage = (CVImage*)this->object;

	vimage->getHugeImage()->open(filename.toLatin1().constData(),true);
	
	baristaProjectHelper.refreshTree();	
}