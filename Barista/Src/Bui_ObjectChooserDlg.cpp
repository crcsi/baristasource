#include "Bui_ObjectChooserDlg.h"
#include "Icons.h"
#include <QListWidgetItem>
#include "ZObject.h"




bui_ObjectChooserDlg::bui_ObjectChooserDlg(void)
{
	setupUi(this);
	    connect(this->Ok,SIGNAL(released()),this,SLOT(Ok_clicked()));
		connect(this->ListWidget,SIGNAL(itemDoubleClicked(QListWidgetItem*)), this,SLOT(Ok_clicked(QListWidgetItem*)));
		connect(this->Cancel,SIGNAL(released()),this,SLOT(Cancel_clicked()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));



}

bui_ObjectChooserDlg::~bui_ObjectChooserDlg(void)
{

}


vector<ZObject*> bui_ObjectChooserDlg::getSelectedObjects()
{
	QList<QListWidgetItem*> list = this->ListWidget->selectedItems();
	vector<ZObject*> zList;

	for (int i=0; i < list.size(); i++) 
		zList.push_back((ZObject*)list.at(i)->data(Qt::UserRole).toUInt());

	return zList;
}


void bui_ObjectChooserDlg::setTitleText( const char * title )
{
	this->setWindowTitle(title);
}


void bui_ObjectChooserDlg::setButtonText( const char * buttontext )
{
    this->Ok->setText(buttontext);
}

void bui_ObjectChooserDlg::ItemtoListbox( const char * name,ZObject* object)
{
	QListWidgetItem* newItem = new QListWidgetItem (QString(name));
	newItem->setData(Qt::UserRole,(unsigned int)object);

	this->ListWidget->addItem(newItem);
    this->ListWidget->setSelectionMode(QAbstractItemView::SingleSelection );

}

void bui_ObjectChooserDlg::setMultiSelection(bool b)
{
	this->ListWidget->setSelectionMode(QAbstractItemView::MultiSelection);
}

void bui_ObjectChooserDlg::Ok_clicked()
{
    this->close();
}

void bui_ObjectChooserDlg::Ok_clicked(QListWidgetItem * item)
{
	this->ListWidget->setCurrentItem(item);
	this->Ok_clicked();
}

void bui_ObjectChooserDlg::Cancel_clicked()
{
    this->ListWidget->clear();
    this->close();
}

void bui_ObjectChooserDlg::adjustLineCount(int nlines)
{
	if ( nlines > 20 ) nlines = 20;
	int lineheight = this->ListWidget->sizeHintForRow(0);
	this->resize(420, nlines*lineheight + 20);
	
}


