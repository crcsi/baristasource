#include "Bui_ChannelSwitchBox.h"
#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "BaristaView.h"
#include "LookupTable.h"
#include "LookupTableHelper.h"
#include "TransferFunction.h"
#include "TransferFunctionFactory.h"
#include "EditorDelegate.h"
#include "IniFile.h"
#include "float.h"
#include "Bui_WallisFilterDlg.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"


#include <QPainter>
#include <QMap>
#include <QDoubleSpinBox>
#include <QToolButton>
#include <QStylePainter>
#include <QMouseEvent>
#include <QHeaderView>
#include <QScrollBar>
#include <QVariant>
#include <QColorDialog>
#include <QMessageBox>
#include <QFileDialog>

QColor curveColor[5] = {QColor(Qt::red),
						QColor(Qt::green),
						QColor(Qt::blue),
						QColor(Qt::magenta),
						QColor(Qt::black)};


typedef struct {
	QString name;
	CTransferFunction::Type type;
}TransferData;


QMap<int,TransferData> functions1Band;
QMap<int,TransferData> functions4Band;

bui_ChannelSwitchBox::bui_ChannelSwitchBox(CBaristaView* view) : bview(view), himage(view->getHugeImage()),
	transferFunction_save(0),plot(0),Nchannels(-1),red(-1),green(-1),
	blue(-1),lutModel(0),proxyModel(0),tV_LUT(0),selectedTransferFunction(CTransferFunction::eDirectTransfer),
	localIntensityHistogram(view->getHugeImage()->getBpp()),success(false),wallisDlg(0),
	minValue(0),maxValue(255),centreValue(0.5),sigmaValue(60),Brightness_save(0)
{

	// we need a huge image and a transfer function
	if (!this->himage || !this->himage->getTransferFunction())
		return;

	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->initTransferBox();

	this->init(this->himage);
	
    // signals and slots connections
	connect( this->HelpButton,SIGNAL( released() ),this, SLOT( HelpReleased() ) );
	connect( this->Cancel,SIGNAL( released() ),this, SLOT( Cancel_released() ) );
	connect( this->SetImageChannels,SIGNAL( released() ),this, SLOT( SetImageChannels_released() ) );
	connect( this->NearestNeighbourRadioButton,SIGNAL( released() ), this, SLOT( NearestNeighbourChanged()));
	connect( this->BilinearRadioButton,SIGNAL( released() ), this, SLOT( BilinearChanged()));
	connect( this->BicubicRadioButton,SIGNAL( released() ), this, SLOT( BicubicChanged()));
	connect( this->rB_Grey,SIGNAL(released()),this,SLOT(rB_GreyValueReleased()));
	connect( this->rB_LUT,SIGNAL(released()),this,SLOT(rB_LUTReleased()));
	connect( this->redchannels,SIGNAL (currentRowChanged(int)), this, SLOT(lW_RedRowChanged(int)));
	connect( this->greenchannels,SIGNAL (currentRowChanged(int)), this, SLOT(lW_GreenRowChanged(int)));
	connect( this->bluechannels,SIGNAL (currentRowChanged(int)), this, SLOT(lW_BlueRowChanged(int)));
	connect( this->cB_Transfer,SIGNAL (currentIndexChanged(int)), this, SLOT(cB_TransferCurrentIndexChanged(int)));
	connect( this->pB_LoadLUT,SIGNAL( released() ), this, SLOT( loadLUTReleased()));

	connect( this->dsB_MinValue, SIGNAL(valueChanged(double)),this,SLOT(sBMinValueChanged(double)));
	connect( this->dsB_MaxValue, SIGNAL(valueChanged(double)),this,SLOT(sBMaxValueChanged(double)));
	connect(this->dsB_MaxValue,SIGNAL(valueChanged(double)),this,SLOT(setMax(double)));
	connect(this->dsB_MinValue,SIGNAL(valueChanged(double)),this,SLOT(setMin(double)));
	
	connect( this->dsB_Centre, SIGNAL(valueChanged(double)),this,SLOT(sBCentreValueChanged(double)));
	connect( this->dsB_Sigma, SIGNAL(valueChanged(double)),this,SLOT(sBSigmaValueChanged(double)));
	connect ( this->pB_CreateLUT ,SIGNAL( released()),this,SLOT(pBDefaultLUTReleased()));


	if (this->himage->getBands() > 1)
		this->rB_LUT->setEnabled(false);
	
	if (this->selectedTransferFunction == CTransferFunction::eColorIndexed && this->himage->getBands() == 1 )
	{
		this->rB_LUT->setChecked(true);
		this->rB_LUTReleased();	
	}
	else 
	{
		this->rB_Grey->setChecked(true);
		this->rB_GreyValueReleased();
	}
	
	
	this->layout()->setSizeConstraint(QLayout::SetFixedSize);

}



bui_ChannelSwitchBox::~bui_ChannelSwitchBox(void)
{
	if (this->plot)
		delete this->plot;
	this->plot = NULL;

	if (this->lutModel)
	{
		this->disconnect(this->lutModel,SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&)),this,SLOT(lutDataChanged(const QModelIndex&, const QModelIndex&)));		
		delete this->lutModel;
	}
	this->lutModel = NULL;

	if (this->proxyModel)
		delete this->proxyModel;
	this->proxyModel = NULL;

	if (this->wallisDlg)
		delete this->wallisDlg;
	this->wallisDlg = NULL;

	if (this->tV_LUT)
		delete this->tV_LUT;
	this->tV_LUT = NULL;

	for (unsigned int i =0; i < this->localHistograms.size(); i++)
		delete this->localHistograms[i];

	this->localHistograms.clear();

	if (this->transferFunction_save)
		delete this->transferFunction_save;
	this->transferFunction_save = NULL;

	
	if (this->success)
	{
		// the image is now using the selected transfer function
		// delete all other functions
		QMapIterator<CTransferFunction::Type, CTransferFunction*> i(this->transferFunctions);
		while (i.hasNext()) 
		{
			i.next();
			CTransferFunction::Type type = i.key();

			if (type != this->selectedTransferFunction)
			{
				delete this->transferFunctions[type];
				this->transferFunctions[type] = NULL;
			}
		}
	
		this->transferFunctions.clear();

	}
	else
	{
		// the image is using it's old transfer function, delete all newly created
		QMapIterator<CTransferFunction::Type, CTransferFunction*> i(this->transferFunctions);
		while (i.hasNext()) 
		{
			i.next();
			CTransferFunction::Type type = i.key();
			delete this->transferFunctions[type];
			this->transferFunctions[type] = NULL;
		}
	
		this->transferFunctions.clear();		
	}
}

void bui_ChannelSwitchBox::Cancel_released()
{
	this->success = false;
	this->close();
}

void bui_ChannelSwitchBox::closeEvent(QCloseEvent* event)
{
	if (!this->success) // cancel or cross have been clicked
	{
		// restor old status
		if (this->transferFunction_save)
			this->himage->setTransferFunction(this->transferFunction_save,true,false);
		
		if (this->Brightness_save)
			this->himage->changeBrightness(this->Brightness_save);

		this->himage->setChannels(this->old_red,this->old_green,this->old_blue);
		this->himage->setResamplingType(this->old_resamplingType);
		
		this->bview->clearImages();
		this->bview->update();
	}
	
	QWidget::closeEvent(event);
}

void bui_ChannelSwitchBox::cB_TransferCurrentIndexChanged(int index )
{
	if (index != -1)
	{
		if (this->himage->getBands() == 1)
			this->selectTransferFunction(functions1Band[index].type);
		else
			this->selectTransferFunction(functions4Band[index].type);
	}
}



void bui_ChannelSwitchBox::selectTransferFunction(CTransferFunction::Type type)
{
	if (type == CTransferFunction::eColorIndexed)
		return;
	
	// check if we have the requested transfer function already
	if (!this->transferFunctions.contains(type))
	{
		//no, create a new one
		switch (type)
		{
		case CTransferFunction::eDirectTransfer:	
								this->transferFunctions.insert(type,CTransferFunctionFactory::createDirectTransfer(this->himage->getFileNameChar(),
																										this->himage->getBands(),
																										this->himage->getBpp()));
								break;

		case CTransferFunction::eLinearStretch:	
								this->transferFunctions.insert(type,CTransferFunctionFactory::createLinearStretch(this->himage->getFileNameChar(),
																									   this->himage->getBpp(),
																									   this->minValue,
																									   this->maxValue));

								break;

		case CTransferFunction::eEqualization:
								this->transferFunctions.insert(type,CTransferFunctionFactory::createEqualization(this->himage->getFileNameChar(),
																									  this->himage->getBpp(),
																									  this->localHistograms[0],
																									  this->minValue,
																									  this->maxValue));

								break;

		case CTransferFunction::eNormalisation:
								this->transferFunctions.insert(type,CTransferFunctionFactory::createNormalisation(this->himage->getFileNameChar(),
																									   this->himage->getBpp(),
																									   this->localHistograms[0],
																							 		   this->minValue,
																									   this->maxValue,
																									   this->centreValue,
																									   this->sigmaValue));
								break;

		case CTransferFunction::eLinearIHSStretch:
								this->transferFunctions.insert(type,CTransferFunctionFactory::createLinearStretchIHS(this->himage->getFileNameChar(),
																											  this->himage->getBpp(),
																											   this->minValue,
																												this->maxValue));
								break;


		case CTransferFunction::eIHSEqualization:
								this->transferFunctions.insert(type,CTransferFunctionFactory::createEqualizationIHS(this->himage->getFileNameChar(),
																										 this->himage->getBpp(),
																										 &this->localIntensityHistogram,
																										  this->minValue,
																										  this->maxValue));
								break;


		case CTransferFunction::eIHSNormalisation:
								this->transferFunctions.insert(type,CTransferFunctionFactory::createNormalisationIHS(this->himage->getFileNameChar(),
																									   this->himage->getBpp(),
																									   &this->localIntensityHistogram,
																							 		    this->minValue,
																									   this->maxValue,
																									   this->centreValue,
																									   this->sigmaValue));							
								break;

		case CTransferFunction::eWallisFilter:
								if (this->himage->getBands() == 1)
									this->transferFunctions.insert(type,CTransferFunctionFactory::createWallisFilterFunction(this->himage->getFileNameChar(),himage->getBpp(),this->localHistograms[0]->getMin(),this->localHistograms[0]->getMax()));
								else
									this->transferFunctions.insert(type,CTransferFunctionFactory::createWallisFilterFunction(this->himage->getFileNameChar(),himage->getBpp(),this->localIntensityHistogram.getMin(),this->localIntensityHistogram.getMax()));
			
								break;

		}
	}

	// just set the new function, no additional copy
	this->himage->setTransferFunction(this->transferFunctions[type],false,false);	


	// copy parameters in local parameters and change gui
	if (type == CTransferFunction::eLinearStretch || type == CTransferFunction::eLinearIHSStretch ||
		type == CTransferFunction::eEqualization  || type == CTransferFunction::eIHSEqualization ||
		type == CTransferFunction::eNormalisation || type == CTransferFunction::eIHSNormalisation)
	{
		this->dsB_MinValue->blockSignals(true);
		this->dsB_MaxValue->blockSignals(true);
		this->dsB_Sigma->blockSignals(true);
		this->dsB_Centre->blockSignals(true);

		this->minValue = this->transferFunctions[type]->getParameter(0);
		this->maxValue = this->transferFunctions[type]->getParameter(1);
		this->dsB_MinValue->setValue(this->minValue);
		this->dsB_MaxValue->setValue(this->maxValue);
		
		if (type == CTransferFunction::eNormalisation || type == CTransferFunction::eIHSNormalisation)
		{
			this->centreValue = this->transferFunctions[type]->getParameter(2);
			this->sigmaValue = this->transferFunctions[type]->getParameter(3);	
			this->dsB_Centre->setValue(this->centreValue);
			this->dsB_Sigma->setValue(this->sigmaValue);
			this->dsB_Centre->setEnabled(true);
			this->dsB_Sigma->setEnabled(true);
		}
		else
		{
			this->dsB_Centre->setEnabled(false);
			this->dsB_Sigma->setEnabled(false);
		}

		this->dsB_MaxValue->blockSignals(false);
		this->dsB_MinValue->blockSignals(false);
		this->dsB_Sigma->blockSignals(false);
		this->dsB_Centre->blockSignals(false);
	}

	if (type == CTransferFunction::eDirectTransfer)
	{
		this->dsB_MinValue->setEnabled(false);
		this->dsB_MaxValue->setEnabled(false);

	}
	else
	{
		this->dsB_MinValue->setEnabled(true);
		this->dsB_MaxValue->setEnabled(true);
	}

	this->selectedTransferFunction = type;


	if (type == CTransferFunction::eWallisFilter)
		this->initWallisFilterView();
	else
		this->initHistogramView();
	
	this->updateView();

}

int bui_ChannelSwitchBox::getIndexOfTransferFunction(CTransferFunction::Type type,int bands)
{
	int ret = -1;
	
	if (bands == 1)
	{
		for (int i=0; i < functions1Band.size(); i++)
		{
			if (functions1Band[i].type == type)
			{
				ret = i;
				break;
			}
		}
	}
	else
	{
		for (int i=0; i < functions4Band.size(); i++)
		{
			if (functions4Band[i].type == type)
			{
				ret = i;
				break;
			}
		}
	}



	return ret;
}

void bui_ChannelSwitchBox::NearestNeighbourChanged()
{
	if (this->NearestNeighbourRadioButton->isChecked())
	{
		this->BicubicRadioButton->setChecked(false);
		this->BilinearRadioButton->setChecked(false);
	}
	else this->NearestNeighbourRadioButton->setChecked(true);
	this->resamplingType = eNearestNeighbour;
	this->himage->setResamplingType(this->resamplingType);
	this->updateView();

};

void bui_ChannelSwitchBox::BilinearChanged()
{
	if (this->BilinearRadioButton->isChecked())
	{
		this->BicubicRadioButton->setChecked(false);
		this->NearestNeighbourRadioButton->setChecked(false);
	}
	else this->BilinearRadioButton->setChecked(true);
	this->resamplingType = eBilinear;
	this->himage->setResamplingType(this->resamplingType);
	this->updateView();

};

void bui_ChannelSwitchBox::BicubicChanged()
{
	if (this->BicubicRadioButton->isChecked())
	{
		this->BilinearRadioButton->setChecked(false);
		this->NearestNeighbourRadioButton->setChecked(false);
	}
	else this->BicubicRadioButton->setChecked(true);
	this->resamplingType = eBicubic;
	this->himage->setResamplingType(this->resamplingType);
	this->updateView();
};

void bui_ChannelSwitchBox::HelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/ImageViewDisplay.html");
};


void bui_ChannelSwitchBox::SetImageChannels_released()
{
	this->success = true;
	
	if (this->selectedTransferFunction == CTransferFunction::eWallisFilter && this->wallisDlg)
		this->wallisDlg->okPressed();

	this->close();
}

void bui_ChannelSwitchBox::init(CHugeImage *himage)
{
	this->Nchannels = this->himage->getBands();

	this->red = this->himage->getRChannel();
	this->green = this->himage->getGChannel();
	this->blue = this->himage->getBChannel();
	this->resamplingType = this->himage->getResamplingType();
	
	this->transferFunctions.clear();
	this->selectedTransferFunction = this->himage->getTransferFunction()->getTransferFunctionType();
	this->transferFunctions.insert(this->selectedTransferFunction,this->himage->getTransferFunction());
	
	// make a backup
	this->old_red = this->red;
	this->old_green = this->green;
	this->old_blue = this->blue;
	this->old_resamplingType = this->resamplingType;
	this->transferFunction_save = CTransferFunctionFactory::create(this->himage->getTransferFunction());
	this->Brightness_save = this->himage->getBrightness();

	if (this->bview->getHasROI())
	{
		int rectLeft = this->bview->getROIUL().x;
		int rectTop = this->bview->getROIUL().y;
		int width =  this->bview->getROILR().x - this->bview->getROIUL().x;
		int height = this->bview->getROILR().y -this->bview->getROIUL().y;		
		this->localHistograms.resize(this->himage->getBands());
		for (int i = 0; i < this->himage->getBands(); i++)
			this->localHistograms[i] = new CHistogram(this->himage->getBpp());

		bui_ProgressDlg dlg(new CComputeHistogramThread(this->himage,rectTop,height,rectLeft,width,this->localHistograms,this->localIntensityHistogram),this->himage,"Computing histograms for ROI...");
		dlg.exec();
				
		
	}
	else
	{
		// copy the current histograms
		this->localHistograms.resize(this->himage->getBands());
		for (unsigned int i=0; i < this->localHistograms.size(); i++)
		{
			this->localHistograms[i] = new CHistogram(this->himage->getBpp());
			(*this->localHistograms[i]) = *this->himage->getHistogram(i);
		}

		if (this->himage->getBands() > 1)
			this->localIntensityHistogram = *this->himage->getIntensityHistogram();
	}
	
	// maybe the histogram has changed, so we better update the transfer function
	this->updateTransferFunctionParameter();

	if (this->himage->getBands() == 1)
	{
		this->minValue = this->localHistograms[0]->getMin();
		this->maxValue = this->localHistograms[0]->getMax();
	}
	else
	{
		this->minValue = this->localIntensityHistogram.getMin();
		this->maxValue = this->localIntensityHistogram.getMax();
	}
		
	this->centreValue = 0.5;
	this->sigmaValue = 60;

	if (this->himage->getBpp() == 32)
	{
		this->dsB_MinValue->setRange(-10000.0,10000.0);
		this->dsB_MinValue->setDecimals(2);
		this->dsB_MaxValue->setRange(-10000.0,10000.0);
		this->dsB_MaxValue->setDecimals(2);
	}
	else
	{
		this->dsB_MinValue->setMinimum(0.0);
		this->dsB_MinValue->setMaximum(pow(2,(float)this->himage->getBpp()));
		this->dsB_MinValue->setDecimals(0);
		this->dsB_MaxValue->setMinimum(0.0);
		this->dsB_MaxValue->setMaximum(pow(2,(float)this->himage->getBpp()));
		this->dsB_MaxValue->setDecimals(0);

	}


	this->initChannelBoxes();
	this->initResampleButtons();

}


void bui_ChannelSwitchBox::initHistogramView()
{
	if (!this->plot && this->selectedTransferFunction != CTransferFunction::eWallisFilter)
	{

		this->plot = new CHistogramPlot(this);

	//	if (this->himage->getBpp() != 32)
		{
			if (this->himage->getBands() == 1)
				this->plot->setCurveData(CHistogramPlot::Grey,QColor(Qt::black),this->localHistograms[0]);
			else if (this->himage->getBands() >= 3)
			{
				this->plot->setCurveData(CHistogramPlot::Red,QColor(Qt::red),this->localHistograms[0]);
				this->plot->setCurveData(CHistogramPlot::Green,QColor(Qt::green),this->localHistograms[1]);
				this->plot->setCurveData(CHistogramPlot::Blue,QColor(Qt::blue),this->localHistograms[2]);

				if (this->himage->getBands() == 4)
					this->plot->setCurveData(CHistogramPlot::Alpha,QColor(Qt::magenta),this->localHistograms[3]);
			}

			if (this->himage->getIntensityHistogram())
				this->plot->setCurveData(CHistogramPlot::Intensity,QColor(Qt::black),&this->localIntensityHistogram);
		}

		QGridLayout* l = new QGridLayout(this->gB_SumHistogram);
		l->setMargin(4);
		

		QSpinBox* sB_Smooth = new QSpinBox(this->gB_SumHistogram);
		sB_Smooth->setRange(1,30);
		
		QLabel* label = new QLabel("smooth histogram by:",this->gB_SumHistogram);

		QHBoxLayout *hl = new QHBoxLayout();
		hl->addStretch();
		hl->addWidget(label);
		hl->addWidget(sB_Smooth);

		connect(sB_Smooth,SIGNAL(valueChanged(int)),this->plot,SLOT(sB_SmoothChanged(int)));
		
		l->addLayout(hl,0,0);
		l->addWidget(this->plot,1,0);
		this->gB_SumHistogram->setLayout(l);

	}

	if (this->plot)
	{
		if (this->himage->getBands() == 1)
			this->plot->setTransferFunction(this->transferFunctions[this->selectedTransferFunction],true,this->localHistograms[0]);
		else
			this->plot->setTransferFunction(this->transferFunctions[this->selectedTransferFunction],true,&this->localIntensityHistogram);
		
		this->sW_Details->setCurrentIndex(0);
	}

}

void bui_ChannelSwitchBox::initLUTView()
{
	if (this->lutModel)
	{
		this->disconnect(this->lutModel,SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&)),this,SLOT(lutDataChanged(const QModelIndex&, const QModelIndex&)));	
		delete this->lutModel;
	}
	this->lutModel = NULL;

	if (!this->tV_LUT)
	{
		this->tV_LUT = new CLUTTableView(this,this->himage->getBpp());
		QGridLayout* l = new QGridLayout();
		l->addWidget(this->tV_LUT,0,0);
		l->setMargin(0);
		this->gB_LUT->setLayout(l);

	}

	this->tV_LUT->verticalHeader()->setDefaultSectionSize(20);
	
	
	this->lutModel = new CLUTDataModel(this->transferFunctions[this->selectedTransferFunction]);
	this->connect(this->lutModel,SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&)),this,SLOT(lutDataChanged(const QModelIndex&, const QModelIndex&)));	
	this->proxyModel = new QSortFilterProxyModel();
	this->proxyModel->setSourceModel(this->lutModel);
	this->tV_LUT->setModel(this->proxyModel);
	this->tV_LUT->verticalHeader()->hide();
	this->tV_LUT->horizontalScrollBar()->hide();
	this->tV_LUT->horizontalScrollBar()->setDisabled(true);


	this->tV_LUT->horizontalHeader()->setSortIndicator(1,Qt::AscendingOrder);
	this->tV_LUT->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
	this->tV_LUT->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
	this->tV_LUT->setColumnWidth(0,60);

	this->proxyModel->setSortRole(Qt::DisplayRole);
	this->proxyModel->setDynamicSortFilter(true);
	this->tV_LUT->setSortingEnabled(true);
	this->tV_LUT->sortByColumn(1);

}

void bui_ChannelSwitchBox::initWallisFilterView()
{
	if (!this->wallisDlg)
	{
		this->wallisDlg = new Bui_WallisFilterDlg(this->bview,false,this);
		QGridLayout* gl =  new QGridLayout(this->pageWallisFilter);
		gl->addWidget(this->wallisDlg);
	}


	this->sW_Details->setCurrentIndex(2);
}

void bui_ChannelSwitchBox::initResampleButtons()
{
	switch (this->resamplingType)
	{
		case eBicubic:
			{
				this->BicubicRadioButton->setChecked(true);
				this->BilinearRadioButton->setChecked(false);
				this->NearestNeighbourRadioButton->setChecked(false);
			}
			break;

		case eBilinear:
			{
				this->BicubicRadioButton->setChecked(false);
				this->BilinearRadioButton->setChecked(true);
				this->NearestNeighbourRadioButton->setChecked(false);
			}
			break;

		case eNearestNeighbour:
		default:
			{
				this->BicubicRadioButton->setChecked(false);
				this->BilinearRadioButton->setChecked(false);
				this->NearestNeighbourRadioButton->setChecked(true);
			}
			break;
	}
};


void bui_ChannelSwitchBox::initChannelBoxes()
{
	for( int i = 0; i < this->Nchannels; i++ )
    {
		this->redchannels->insertItem( i, QString::fromLatin1( "Channel " ) + QString::number(i + 1) );      
		this->greenchannels->insertItem( i, QString::fromLatin1( "Channel " ) + QString::number(i + 1) );      
		this->bluechannels->insertItem( i, QString::fromLatin1( "Channel " ) + QString::number(i + 1) );      
    }
    
	if (this->red == -1 || this->Nchannels < 3)
    {
		this->red = 0;
	}
	
	QListWidgetItem* redpreselected = this->redchannels->item(this->red);
	this->redchannels->setItemSelected(redpreselected , true);
	this->redchannels->setCurrentRow(this->red);
	this->redchannels->scrollToItem(redpreselected );

	if (this->green == -1 || this->Nchannels < 3)
    {
		this->green = 0;
	}
	
	QListWidgetItem* greenpreselected = this->greenchannels->item(this->green);

	this->greenchannels->setItemSelected(greenpreselected , true);
	this->greenchannels->setCurrentRow(this->green);
	this->greenchannels->scrollToItem(greenpreselected );

	if (this->blue == -1 || this->Nchannels < 3)
    {
		this->blue = 0;
	}

	QListWidgetItem* bluepreselected = this->bluechannels->item(this->blue);

	this->bluechannels->setItemSelected(bluepreselected , true);
	this->bluechannels->setCurrentRow(this->blue);
	this->bluechannels->scrollToItem(bluepreselected );
	
    this->redchannels->setSelectionMode(QAbstractItemView::SingleSelection );
    this->greenchannels->setSelectionMode(QAbstractItemView::SingleSelection );
    this->bluechannels->setSelectionMode(QAbstractItemView::SingleSelection );
}


void bui_ChannelSwitchBox::lW_RedRowChanged(int currentRow)
{
	this->red = currentRow;
	this->himage->setRChannel(this->red);
	this->plot->updateColor(CHistogramPlot::Red,curveColor[this->red]);
	this->updateView();
}

void bui_ChannelSwitchBox::lW_GreenRowChanged(int currentRow)
{
	this->green = currentRow;
	this->himage->setGChannel(this->green);
	this->plot->updateColor(CHistogramPlot::Green,curveColor[this->green]);
	this->updateView();
}

void bui_ChannelSwitchBox::lW_BlueRowChanged(int currentRow)
{

	this->blue = currentRow;
	this->himage->setBChannel(this->blue);
	this->plot->updateColor(CHistogramPlot::Blue,curveColor[this->blue]);
	this->updateView();

}

void bui_ChannelSwitchBox::updateView()
{
	this->bview->clearImages();
	this->bview->update();
}


void bui_ChannelSwitchBox::pBDefaultLUTReleased()
{

	// replace the old color indexed lut in the array and the huge image
	if (this->selectedTransferFunction == CTransferFunction::eColorIndexed)	
	{
		this->transferFunctions[CTransferFunction::eColorIndexed] = CTransferFunctionFactory::createFromLUT( CTransferFunction::eColorIndexed,"default LUT",CLookupTableHelper::getDSMLookup());		
		
		// delete the old lut, no pointer will reference to it
		this->himage->setTransferFunction(this->transferFunctions[CTransferFunction::eColorIndexed],false,true);
	}
	else
	{
		this->transferFunctions[CTransferFunction::eColorIndexed] = CTransferFunctionFactory::createFromLUT( CTransferFunction::eColorIndexed,"default LUT",CLookupTableHelper::getDSMLookup());		
		
		// keep the old transfer function, the pointer is still in the array
		this->himage->setTransferFunction(this->transferFunctions[CTransferFunction::eColorIndexed],false,false);
	}		


	if (this->lutModel)
		this->lutModel->setLUT(this->himage->getTransferFunction());

	this->updateView();
	
}



void bui_ChannelSwitchBox::rB_GreyValueReleased()
{
	int index = -1;
	this->cB_Transfer->setEnabled(true);
	bool triggerUpdate = false; 

	// check which function to select, last selected was color indexed
	if (this->selectedTransferFunction == CTransferFunction::eColorIndexed)
	{	
		index = this->cB_Transfer->currentIndex();
		triggerUpdate = true; 
	}
	else
		index = this->getIndexOfTransferFunction(this->selectedTransferFunction,this->himage->getBands());


	if (!triggerUpdate && this->cB_Transfer->currentIndex() == index)
		triggerUpdate = true;

	if (index > -1 && index < this->cB_Transfer->count())
		this->cB_Transfer->setCurrentIndex(index);

	if (triggerUpdate) // force the update
		this->cB_TransferCurrentIndexChanged(index);

}

void bui_ChannelSwitchBox::rB_LUTReleased()
{
	this->selectedTransferFunction = CTransferFunction::eColorIndexed;
	
	// we need a color indexed transfer function, create if not in array already
	if (!this->transferFunctions.contains(CTransferFunction::eColorIndexed))
	{
		CFileName name = "default LUT";
		this->transferFunctions[this->selectedTransferFunction] = CTransferFunctionFactory::createFromLUT(CTransferFunction::eColorIndexed,name,CLookupTableHelper::getDSMLookup());
	}

	if (!this->lutModel)
		this->initLUTView();

	this->himage->setTransferFunction(this->transferFunctions[this->selectedTransferFunction],false,false);

	this->cB_Transfer->setEnabled(false);
	this->sW_Details->setCurrentIndex(1);
	this->updateView();
}

void bui_ChannelSwitchBox::setMax(double max)
{
	this->dsB_MinValue->setMaximum(max);
}

void bui_ChannelSwitchBox::setMin(double min)
{
	this->dsB_MaxValue->setMinimum(min);
}

void bui_ChannelSwitchBox::sBMinValueChanged(double value)
{
	this->transferFunctions[this->selectedTransferFunction]->setParameter(0,value);
	this->minValue = value;
	this->updateTransferFunctionParameter();
	this->updateView();

}

void bui_ChannelSwitchBox::sBMaxValueChanged(double value)
{
	this->transferFunctions[this->selectedTransferFunction]->setParameter(1,value);
	this->maxValue = value;
	this->updateTransferFunctionParameter();
	this->updateView();
}

void bui_ChannelSwitchBox::sBCentreValueChanged(double value)
{
	this->transferFunctions[this->selectedTransferFunction]->setParameter(2,value);
	this->updateTransferFunctionParameter();
	this->centreValue = value;
	this->updateView();
}

void bui_ChannelSwitchBox::sBSigmaValueChanged(double value)
{
	this->transferFunctions[this->selectedTransferFunction]->setParameter(3,value);
	this->sigmaValue = value;
	this->updateTransferFunctionParameter();
	this->updateView();
}


void bui_ChannelSwitchBox::initTransferBox()
{
	TransferData d;
	functions1Band.clear();
	d.name = "Original"; d.type = CTransferFunction::eDirectTransfer;
	functions1Band[0] = d;
	d.name = "Linear Stretch"; d.type = CTransferFunction::eLinearStretch;
	functions1Band[1] = d;
	d.name = "Equalisation"; d.type = CTransferFunction::eEqualization;
	functions1Band[2] = d;
	d.name = "Normalisation"; d.type = CTransferFunction::eNormalisation;
	functions1Band[3] = d;
	d.name = "Wallis Filter"; d.type = CTransferFunction::eWallisFilter;
	functions1Band[4] = d;

	functions4Band.clear();
	d.name = "Original"; d.type = CTransferFunction::eDirectTransfer;
	functions4Band[0] = d;
	d.name = "Linear Stretch (IHS)"; d.type = CTransferFunction::eLinearIHSStretch;
	functions4Band[1] = d;
	d.name = "Equalisation (IHS)"; d.type = CTransferFunction::eIHSEqualization;
	functions4Band[2] = d;
	d.name = "Normalisation (IHS)"; d.type = CTransferFunction::eIHSNormalisation;
	functions4Band[3] = d;
	d.name = "Wallis Filter"; d.type = CTransferFunction::eWallisFilter;
	functions4Band[4] = d;


	if (this->himage->getBands() == 1)
	{
		int length = functions1Band.size();
		if (this->himage->getBpp() == 32)
			length = 2; // only Original and Linear Stretch
		for (int i=0; i < length ;i++)
			this->cB_Transfer->insertItem( i, functions1Band[i].name ); 
	}
	else
		for (int i=0; i < functions4Band.size();i++)
			this->cB_Transfer->insertItem( i, functions4Band[i].name ); 

}


void bui_ChannelSwitchBox::updateTransferFunctionParameter()
{
	if (this->himage->getBpp() != 32)
	{
		CTransferFunction* current = this->transferFunctions[this->selectedTransferFunction];
		
		if (this->himage->getBands() == 1)
			current->updateTransferFunction(this->localHistograms[0],current->getParameters());
		else
			current->updateTransferFunction(&this->localIntensityHistogram,current->getParameters());

		if (this->plot)
		{
			if (this->himage->getBands() == 1)
				this->plot->setTransferFunction(current,false,this->localHistograms[0]);
			else
				this->plot->setTransferFunction(current,false,&this->localIntensityHistogram);
		}
	}
}


void bui_ChannelSwitchBox::lutDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight)
{
	this->updateView();
}


void bui_ChannelSwitchBox::loadLUTReleased()
{
	if (this->selectedTransferFunction != CTransferFunction::eColorIndexed)
		return;

	if ( this->himage->getBands() > 1 )
	{
		QMessageBox::warning( this, "Image has more than one band!"
		, "Lookup Table can only be assigned to single band images");
        return;
	}

    // file dialog
	QString filename = QFileDialog::getOpenFileName(
		this,
		"Choose a Lookup Table file",
		inifile.getCurrDir(),
		"Lookup Table (*.lut *.pal)");

    // make sure the file exists
    QFile test(filename);
    if (!test.exists())
        return;
    test.close();

	CFileName cfilename = (const char*)filename.toLatin1();

	CLookupTable lut(this->himage->getBpp(), this->himage->getBands());
	lut.read(cfilename.GetChar());

	if ( !lut.read(cfilename.GetChar()))
	{
		QMessageBox::warning( this, "Lookup Table can not be applied!"
		, "Lookup Table and image have different image depths (bpp)");
        return;
	}

	int r,g,b;

	for (int i=0; i < lut.getMaxEntries(); i++)
	{
		lut.getColor(r,g,b,i);
		this->transferFunctions[this->selectedTransferFunction]->setColor(r,g,b,i);
	}
	
	if (this->lutModel)
		this->tV_LUT->reset();

	this->updateView();


}


//##########################################################################################
//##########################################################################################


CHistogramPlot::CHistogramPlot(QWidget *parent)
    : QWidget(parent),transferFunction(0),mouseDrag(false),smooth(1)
{
    setBackgroundRole(QPalette::Dark);
    setAutoFillBackground(true);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setFocusPolicy(Qt::StrongFocus);
    rubberBandIsShown = false;
    setPlotSettings(PlotSettings());
}

CHistogramPlot::~CHistogramPlot()
{

}

void CHistogramPlot::setPlotSettings(const PlotSettings &settings)
{
    zoomStack.clear();
    zoomStack.append(settings);
    curZoom = 0;
    refreshPixmap();
}

void CHistogramPlot::zoomOut()
{
    if (curZoom > 0) 
	{
        --curZoom;
        refreshPixmap();
    }
}

void CHistogramPlot::zoomIn()
{
    if (curZoom < zoomStack.count() - 1)
	{
        ++curZoom;
        refreshPixmap();
    }
}

void CHistogramPlot::setCurveData(int id,QColor color,const CHistogram* histogram)
{
	if (!histogram)
		return;

	// set min and max for x scaling properly
	PlotSettings s = this->zoomStack[0];
	if (histogram->getMin() < s.minX) s.minX = histogram->getMin();
	if (histogram->getMax() > s.maxX) s.maxX = histogram->getMax();

	if (histogram->getBpp() != 32)
	{
		// save the histogram 
		vector<double> data(histogram->getSize());

		double sum =0;
		double min = FLT_MAX;
		double max = FLT_MIN;

		unsigned long value =0;
		for (int k=0; k < histogram->getSize(); k++)
		{
			data[k] = histogram->value(k);
			sum += data[k];

			// determine scale in y
			if (data[k] < min )
				min =  data[k];
			else if (data[k] > max)
				max = data[k];
			
		}

		sum /= 100.0;
		
		if (fabs(sum) > 1)
		{
			min /= sum;
			max /= sum;
		}

		for (int k=0; k < histogram->getSize(); k++)
			data[k] /= sum;  // in percent now

		max += max * 0.02;
		min -= min * 0.02;

		// determine absolute min and max
		if (min  < s.minY )
			s.minY = min;
		if (max > s.maxY )
			s.maxY = max;

		this->curveData[id] = data;
		this->curveColor[id] = color;

	}
	else
	{
		s.minY = 0;
		s.maxY = 1.0;
	}

	this->originalMin = s.minY;
	this->originalMax = s.maxY;




	this->setPlotSettings(s);
	
    refreshPixmap();
}

void CHistogramPlot::setTransferFunction(CTransferFunction* transferFunction,bool resetView,const CHistogram* histogram)
{
	this->transferFunction = CTransferFunctionFactory::create(transferFunction);

	transferFunction->getUnmappedLUT(*this->transferFunction,histogram);

	if (transferFunction && transferFunction->getBpp() == 32)
		return;

	this->computeCurves();

	if (resetView)
		this->clearView();

	this->refreshPixmap();
}

void CHistogramPlot::computeCurves()
{

	this->zoomStack[0].minY = FLT_MAX;
	this->zoomStack[0].maxY = FLT_MIN;


	if (this->curveData.contains(Red) && this->curveData.contains(Green) && this->curveData.contains(Blue))
		this->computeCurrentRGBData();


	QMapIterator<int, vector<double> > i(this->curveData);
	while (i.hasNext()) 
	{
		i.next();
		int id = i.key();

		if (id == Red || id == Green || id == Blue)
			continue;
	
		this->computeCurve(id);
	}
}

void CHistogramPlot::clearCurve(int id)
{
    curveData.remove(id);
    refreshPixmap();
}


void CHistogramPlot::clearView()
{
	PlotSettings s = this->zoomStack[0];
	this->zoomStack.clear();
	this->zoomStack.push_back(s);
	this->curZoom = 0;
}


void CHistogramPlot::paintEvent(QPaintEvent *event)
{
    QStylePainter painter(this);
    painter.drawPixmap(0, 0, pixmap);

    if (rubberBandIsShown) 
	{
        painter.setPen(palette().light().color());
        painter.drawRect(rubberBandRect.normalized()
                                       .adjusted(0, 0, -1, -1));
    }

    if (hasFocus()) 
	{
        QStyleOptionFocusRect option;
        option.initFrom(this);
        option.backgroundColor = palette().dark().color();
        painter.drawPrimitive(QStyle::PE_FrameFocusRect, option);
    }
}

void CHistogramPlot::resizeEvent(QResizeEvent * event)
{
	this->refreshPixmap();
}

void CHistogramPlot::mousePressEvent(QMouseEvent *event)
{
    QRect rect = this->computeRect();

    if (event->button() == Qt::LeftButton) 
	{
        if (rect.contains(event->pos())) 
		{
            rubberBandIsShown = true;
            rubberBandRect.setTopLeft(event->pos());
            rubberBandRect.setBottomRight(event->pos());
            updateRubberBandRegion();
            setCursor(Qt::CrossCursor);
        }
    }
	else if (event->button() == Qt::MidButton)
	{
		this->mouseDrag = true;
		this->lastPos = event->pos();
	}
}

void CHistogramPlot::mouseMoveEvent(QMouseEvent *event)
{
    if (rubberBandIsShown) 
	{
        updateRubberBandRegion();
        rubberBandRect.setBottomRight(event->pos());
        updateRubberBandRegion();
    }
	else if (this->mouseDrag)
	{
		QRect rect = this->computeRect();
		double scaleX =  double(this->zoomStack[this->curZoom].numXTicks) /  double(rect.width());
		double scaleY =   double(this->zoomStack[this->curZoom].numYTicks)/ double(rect.height());
		double xShift = double(event->pos().x() - this->lastPos.x()) * scaleX;
		double yShift = double(event->pos().y() - this->lastPos.y()) * scaleY;

		double scrollX =0,scrollY =0;
		if (xShift < -0.25) 
			scrollX = 1;
		else if (xShift > 0.25) 
			scrollX = -1;

		if (yShift < -0.25) scrollY = -1;
		else if (yShift > 0.25) scrollY = 1;

		this->lastPos = event->pos();
		this->zoomStack[curZoom].scroll(scrollX,scrollY);

		refreshPixmap();
		
	}
}

void CHistogramPlot::mouseReleaseEvent(QMouseEvent *event)
{
    if ((event->button() == Qt::LeftButton) && rubberBandIsShown) {
        rubberBandIsShown = false;
        updateRubberBandRegion();
        unsetCursor();

        QRect rect = rubberBandRect.normalized();
        if (rect.width() < 4 || rect.height() < 4)
            return;
        rect.translate(-MarginLeft, -MarginTop);

        PlotSettings prevSettings = zoomStack[curZoom];
        PlotSettings settings;
        double dx = prevSettings.spanX() / (width() - MarginLeft - MarginRight);
        double dy = prevSettings.spanY() / (height() - MarginTop - MarginBottom);
        settings.minX = prevSettings.minX + dx * rect.left();
        settings.maxX = prevSettings.minX + dx * rect.right();
        settings.minY = prevSettings.maxY - dy * rect.bottom();
        settings.maxY = prevSettings.maxY - dy * rect.top();
        settings.adjust();

        zoomStack.resize(curZoom + 1);
        zoomStack.append(settings);
        zoomIn();
    }
	else if (event->button() == Qt::MidButton)
		this->mouseDrag = false;

}

void CHistogramPlot::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) 
	{
    case Qt::Key_Left:		zoomStack[curZoom].scroll(-1, 0);
							refreshPixmap();
							break;

    case Qt::Key_Right:		zoomStack[curZoom].scroll(+1, 0);
							refreshPixmap();
							break;

    case Qt::Key_Down:		zoomStack[curZoom].scroll(0, -1);
							refreshPixmap();
							break;

    case Qt::Key_Up:		zoomStack[curZoom].scroll(0, +1);
							refreshPixmap();
							break;

    default:				QWidget::keyPressEvent(event);
    }
}

void CHistogramPlot::wheelEvent(QWheelEvent *event)
{
	if (event->delta() > 0)
	{
		this->zoomOut();
	}
	else 
	{
		QRect rect = this->computeRect();
		QPoint pos = event->pos();

		if (rect.contains(event->pos())) 
		{
		
			PlotSettings prevSettings = zoomStack[curZoom];
			PlotSettings settings;
			
			QRect newRect(pos.x() - rect.width() / 4,pos.y() - rect.height() /4,rect.width() /2,rect.height() /2);

			newRect.translate(-MarginLeft, -MarginTop);

			double dx = prevSettings.spanX() / (width() - MarginLeft - MarginRight);
			double dy = prevSettings.spanY() / (height() - MarginTop - MarginBottom);
			settings.minX = prevSettings.minX + dx * newRect.left();
			settings.maxX = prevSettings.minX + dx * newRect.right();
			settings.minY = prevSettings.maxY - dy * newRect.bottom();
			settings.maxY = prevSettings.maxY - dy * newRect.top();
			settings.adjust();

			zoomStack.resize(curZoom + 1);
			zoomStack.append(settings);
			zoomIn();

		}
	}
 }

void CHistogramPlot::updateRubberBandRegion()
{
    QRect rect = rubberBandRect.normalized();
    update(rect.left(), rect.top(), rect.width(), 1);
    update(rect.left(), rect.top(), 1, rect.height());
    update(rect.left(), rect.bottom(), rect.width(), 1);
    update(rect.right(), rect.top(), 1, rect.height());
}

void CHistogramPlot::refreshPixmap()
{
    pixmap = QPixmap(size());
    pixmap.fill(this, 0, 0);

    QPainter painter(&pixmap);
    painter.initFrom(this);

	if (this->transferFunction && this->transferFunction->getTransferFunctionType() == CTransferFunction::eDirectTransfer)
	{
		this->zoomStack[0].minY = this->originalMin;
		this->zoomStack[0].maxY = this->originalMax;
	}

    drawGrid(&painter);
    drawCurves(&painter);
    update();
}

void CHistogramPlot::drawGrid(QPainter *painter)
{
    
	QRect rect = this->computeRect();

    if (!rect.isValid())
        return;

    PlotSettings settings = zoomStack[curZoom];
    QPen quiteDark = palette().dark().color().light();
    QPen light = palette().light().color();

	double oldLabel= -10000;
    for (int i = 0; i <= settings.numXTicks; ++i) 
	{
        int x = rect.left() + (i * (rect.width() - 1)/ settings.numXTicks);
        double label = settings.minX + (i * settings.spanX()/ settings.numXTicks);
		QString number;
		if (fabs(oldLabel - label) > 1)
			number.sprintf("%.0lf",label);       
		else
			number.sprintf("%.1lf",label); 

		oldLabel = label;

		painter->setPen(quiteDark);
        painter->drawLine(x, rect.top(), x, rect.bottom());
        painter->setPen(light);
        painter->drawLine(x, rect.bottom(), x, rect.bottom() + 5);
        painter->drawText(x - 50, rect.bottom() + 5, 100, 15, Qt::AlignHCenter | Qt::AlignTop,number);
    }
    for (int j = 0; j <= settings.numYTicks; ++j) 
	{
        int y = rect.bottom() - (j * (rect.height() - 1) / settings.numYTicks);
        double label = settings.minY + (j * settings.spanY() / settings.numYTicks);
		QString number;
		number.sprintf("%.2lf %%",label);
        painter->setPen(quiteDark);
        painter->drawLine(rect.left(), y, rect.right(), y);
        painter->setPen(light);
        painter->drawLine(rect.left() - 5, y, rect.left(), y);
        painter->drawText(rect.left() - MarginLeft, y - 10, MarginLeft - 5, 20, Qt::AlignRight | Qt::AlignVCenter,number);
    }
    painter->drawRect(rect.adjusted(0, 0, -1, -1));
}

void CHistogramPlot::drawCurves(QPainter *painter)
{
 	QRect rect = this->computeRect();

    if (!rect.isValid() || !this->transferFunction)
        return;

    painter->setClipRect(rect.adjusted(+1, +1, -1, -1));

	if (this->transferFunction->getTransferFunctionType() == CTransferFunction::eDirectTransfer)
	{
		QMapIterator<int, vector<double> > i(this->curveData);
		while (i.hasNext()) 
		{
			i.next();
			int id = i.key();

			const vector<double> &data = i.value();

			QPolygonF polyline(data.size());
			this->createDirectTransferCurve(data,polyline,rect);

			painter->setPen(this->curveColor[id]);
			painter->drawPolyline(polyline);

		}
	}
	else // use the transfer function
	{

		QMapIterator<int, vector<double> > i(this->currentData);
		while (i.hasNext()) 
		{
			i.next();
			int id = i.key();

			QPolygonF polyline(this->currentData[id].size());
			this->createCurve(id,polyline,rect);

			painter->setPen(this->curveColor[id]);
			painter->drawPolyline(polyline);
		}
	}
}




void CHistogramPlot::createCurve(int id,QPolygonF& polyline,QRect& rect)
{
	PlotSettings settings = zoomStack[curZoom];
   
	vector<double>& newHist = this->currentData[id];

	double dx, dy, x ,y;

	// map histogram to view
    for (unsigned int j = 0; j < newHist.size(); ++j) 
	{
		dx = j - settings.minX;
        dy = newHist[j] - settings.minY;
        x = rect.left() + (dx * (rect.width() - 1)/ settings.spanX());
        y = rect.bottom() - (dy * (rect.height() - 1) / settings.spanY());
        polyline[j] = QPointF(x, y);
    }

}


void CHistogramPlot::createDirectTransferCurve(const vector<double> &data,QPolygonF& polyline,QRect& rect)
{
	PlotSettings settings = zoomStack[curZoom];
 	
	double dx, dy, x ,y;

	// map histogram to view
    for (unsigned int j = 0; j < data.size(); ++j) 
	{
		dx = j - settings.minX;
        dy = data[j] - settings.minY;
        x = rect.left() + (dx * (rect.width() - 1)/ settings.spanX());
        y = rect.bottom() - (dy * (rect.height() - 1) / settings.spanY());
        polyline[j] = QPointF(x, y);
    }	
}



void CHistogramPlot::computeCurrentRGBData()
{
	const vector<double> &dataRed = this->curveData[Red];
	const vector<double> &dataGreen = this->curveData[Green];
	const vector<double> &dataBlue = this->curveData[Blue];


	// compute RGB curves
	if (dataRed.size() == dataGreen.size() &&
		dataRed.size() == dataBlue.size())
	{

		
		if (!this->currentData.contains(Red))
		{
			vector<double> newRedHist(dataRed.size());
			this->currentData[Red] = newRedHist;
		}

		if (!this->currentData.contains(Green))
		{
			vector<double> newGreenHist(dataRed.size());
			this->currentData[Green] = newGreenHist;
		}

		if (!this->currentData.contains(Blue))
		{
			vector<double> newBlueHist(dataRed.size());
			this->currentData[Blue] = newBlueHist;
		}

		vector<double>& newRedHist = this->currentData[Red];
		vector<double>& newGreenHist = this->currentData[Green];
		vector<double>& newBlueHist = this->currentData[Blue];

		for (unsigned int i=0; i < newRedHist.size(); i++)
		{
			newRedHist[i] =0;
			newGreenHist[i] =0;
			newBlueHist[i] =0;
		}


		vector<int> color(3);

		// apply transfer function to original histogram
		for (unsigned int j = 0; j < dataRed.size(); j++) 
		{
			this->transferFunction->getColor(color[0],color[1],color[2],j,j,j);
			newRedHist[color[0]] += dataRed[j];
			newGreenHist[color[1]] += dataGreen[j];
			newBlueHist[color[2]] += dataBlue[j];
		}


		// smooth the current histogram if requested
		if (this->smooth > 1)
		{
			int size = (newRedHist.size() / this->smooth) * this->smooth;
			int add = newRedHist.size() % this->smooth;
			for (int j = 0; j < size; j+= this->smooth) 
			{
				double valueRed =0;
				double valueGreen =0;
				double valueBlue =0;
				int index;

				// compute first the average value
				for (int k= 0; k < this->smooth; k++)
				{
					index = j+k;
					valueRed += newRedHist[index];
					valueGreen += newGreenHist[index];
					valueBlue += newBlueHist[index];

				}

				valueRed /= double(this->smooth);
				valueGreen /= double(this->smooth);
				valueBlue /= double(this->smooth);


				// save average value in the coresponding entries
				for (int k= 0; k < this->smooth; k++)
				{
					index = j+k;
					newRedHist[index] = valueRed;
					newGreenHist[index] = valueGreen;
					newBlueHist[index] = valueBlue;
				}
			}
		}

		int size = newRedHist.size() -1; // skip first and last 
		for (int i=1; i < size; i++)
		{
			if (newRedHist[i] < this->zoomStack[0].minY) this->zoomStack[0].minY = newRedHist[i];
			else if (newRedHist[i] > this->zoomStack[0].maxY) this->zoomStack[0].maxY = newRedHist[i];

			if (newGreenHist[i] < this->zoomStack[0].minY) this->zoomStack[0].minY = newGreenHist[i];
			else if (newGreenHist[i] > this->zoomStack[0].maxY) this->zoomStack[0].maxY = newGreenHist[i];

			if (newBlueHist[i] < this->zoomStack[0].minY) this->zoomStack[0].minY = newBlueHist[i];
			else if (newBlueHist[i] > this->zoomStack[0].maxY) this->zoomStack[0].maxY = newBlueHist[i];
		}


	} // end RGB
}


void CHistogramPlot::computeCurve(int id_curveData)
{
	const vector<double> &data = this->curveData[id_curveData];

	if (!this->currentData.contains(id_curveData))
	{
		vector<double> newHist(data.size());
		this->currentData[id_curveData] = newHist;
	}



	vector<double>& newHist = this->currentData[id_curveData];

	for (unsigned int i=0; i < newHist.size(); i++)
			newHist[i] =0;

	// apply transfer function to original histogram
    for (unsigned int j = 0; j < data.size(); ++j) 
	{
		newHist[(*this->transferFunction)[j]] += data[j];
    }

	// smooth the current histogram if requested
	if (this->smooth > 1)
	{
		
		int size = (newHist.size() / this->smooth) * this->smooth;
		int add = newHist.size() % this->smooth;
		for (int j = 0; j < size; j+= this->smooth) 
		{
			double value =0;

			// compute first the average value
			for (int k= 0; k < this->smooth; k++)
				value += newHist[j+k];
			value /= double(this->smooth);

			// save average value in the coresponding entries
			for (int k= 0; k < this->smooth; k++)
				newHist[j+k] = value;
		}
	}

	this->zoomStack[0];

	int size = newHist.size() -1;
	for (int i=1; i < size; i++)
	{
		if (newHist[i] < this->zoomStack[0].minY) this->zoomStack[0].minY = newHist[i];
		else if (newHist[i] > this->zoomStack[0].maxY) this->zoomStack[0].maxY = newHist[i];
	}


}




QRect CHistogramPlot::computeRect()
{
  return QRect (MarginLeft, MarginTop,
               width() - MarginLeft - MarginRight, height() -  MarginTop - MarginBottom);
}


void CHistogramPlot::sB_SmoothChanged(int value)
{
	this->smooth = value;
	
	this->computeCurves();

	this->refreshPixmap();
}


QSize CHistogramPlot::minimumSizeHint() const
{
    return QSize(250, 200);
}

QSize CHistogramPlot::sizeHint() const
{
    return QSize(400, 200);
}


void CHistogramPlot::updateColor(int id,QColor color)
{
	if (this->curveColor.contains(id))
	{
		this->curveColor[id] = color;
		this->refreshPixmap();
	}
}


PlotSettings::PlotSettings()
{
    minX = 1000000000.0;
    maxX = -100000000.0;
    numXTicks = 5;

    minY = FLT_MAX;
    maxY = FLT_MIN;
    numYTicks = 5;
}

void PlotSettings::scroll(double dx, double dy)
{
    double stepX = spanX() / numXTicks;
    minX += dx * stepX;
    maxX += dx * stepX;

    double stepY = spanY() / numYTicks;
    minY += dy * stepY;
    maxY += dy * stepY;
}

void PlotSettings::adjust()
{
    adjustAxis(minX, maxX, numXTicks);
    adjustAxis(minY, maxY, numYTicks);
}

void PlotSettings::adjustAxis(double &min, double &max,
                              int &numTicks)
{
    const int MinTicks = 4;
    double grossStep = (max - min) / MinTicks;
    double step = pow(10.0, floor(log10(grossStep)));

    if (5 * step < grossStep) {
        step *= 5;
    } else if (2 * step < grossStep) {
        step *= 2;
    }

    numTicks = int(ceil(max / step) - floor(min / step));
    if (numTicks < MinTicks)
        numTicks = MinTicks;
    min = floor(min / step) * step;
    max = ceil(max / step) * step;
}


//##########################################################################################
//##########################################################################################

CLUTDataModel::CLUTDataModel(CLookupTable* lut) : lut(lut)
{
	this->setLUT(lut);
}


void CLUTDataModel::setLUT(CLookupTable* lut)
{
	if (lut)
	{
		this->beginResetModel();
		this->lut = lut;
		this->lutData.clear();

		int nClasses =0;
		int lastR= -1,lastG = -1, lastB,r,g,b;
		for (int i=0; i < this->lut->getMaxEntries(); i++)
		{
			this->lut->getColor(r,g,b,i);
			
			if ( r != lastR || g != lastG || b != lastB)
			{
				this->lutData.push_back(i);
				lastR = r;
				lastG = g;
				lastB = b;
			}

		}
		this->endResetModel();
	}
}

int CLUTDataModel::rowCount(const QModelIndex& parent) const
{
	return this->lutData.size();
}

int CLUTDataModel::columnCount(const QModelIndex& parent) const
{
	return 2;
}

QVariant CLUTDataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();	

	if (role == Qt::TextAlignmentRole && index.column() == 1)
	{
		return int(Qt::AlignRight | Qt::AlignVCenter);
	}
	else if ( role == Qt::DisplayRole || role == Qt::ToolTipRole || role == Qt::EditRole)
	{
		if (index.column() == 1)
		{
			return this->lutData[index.row()];
		}
	}

	else if (role == Qt::BackgroundRole)
	{
		if (index.column() == 0)
		{
			int r,g,b;
			this->lut->getColor(r,g,b,this->lutData[index.row()]);
			return QColor(r,g,b);
		}
		else
			return QColor(Qt::white);
	}
	return QVariant();
}

QVariant CLUTDataModel::headerData(int section,Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal )
	{
		if (section == 0)
			return  "Colour";
		else if (section == 1)
			return "Value";
	}
	
	return QAbstractTableModel::headerData(section,orientation,role);
}

Qt::ItemFlags CLUTDataModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return QAbstractItemModel::flags(index);
	else if (index.column() == 0)
		return (Qt::ItemFlags)(QAbstractItemModel::flags(index)   - Qt::ItemIsSelectable);
	else if (index.column() == 1)
		return (Qt::ItemFlags)(QAbstractItemModel::flags(index) + Qt::ItemIsEditable);
	else
		return QAbstractItemModel::flags(index) ;
}

bool CLUTDataModel::setData(const QModelIndex &index, const QVariant &value,int role )
{
	if (!index.isValid())
		return false;
	
	if (role == Qt::EditRole && index.column() == 1)
	{
		int v = value.toInt();
		if (v > 0 && v < this->lut->getMaxEntries())
		{
			this->lutData[index.row()] = value.toInt();
			emit dataChanged(index,index);
			return true;
		}
	}
	else if (role == Qt::BackgroundRole && index.column() == 0)
	{
		if (value.canConvert<QColor>())
		{
			QColor newColor = value.value<QColor>();
			this->lut->setColor(newColor.red(),newColor.green(),newColor.blue(),this->lutData[index.row()]);
			emit dataChanged(index,index);
			return true;
		}
	}



	return false;
}

bool CLUTDataModel::removeRows (int row,int count,const QModelIndex & parent)
{
	return false;
}


//##########################################################################################
//##########################################################################################



CLUTTableView::CLUTTableView(QWidget* parent,int bpp) : QTableView(parent)
{
	this->setMinimumWidth(140);
	this->setMaximumWidth(140);
	this->setSelectionMode(QAbstractItemView::SingleSelection);
	this->setItemDelegateForColumn(1,new CEditorDelegate(this,CEditorDelegate::eIntValidator,0,pow(2.0,(double(bpp)))-1));
}

void CLUTTableView:: mouseDoubleClickEvent ( QMouseEvent * event )
{
	QModelIndex index = this->indexAt(event->pos());
	
	if (!index.isValid())
		return;

	if (index.column() == 0)
	{
		QVariant value = this->model()->data(index,Qt::BackgroundRole);
		QColor oldColor;
		if (value.canConvert<QColor>())
			oldColor = value.value<QColor>();
		else
			oldColor = QColor(Qt::white);

		QColor newColor = QColorDialog::getColor(oldColor,this);
		if (newColor.isValid())
			this->model()->setData(index,newColor,Qt::BackgroundRole);

	}
	else
		QTableView::mouseDoubleClickEvent(event);



}

void CLUTTableView::mouseReleaseEvent ( QMouseEvent * mouseEvent )
{
	QTableView::mouseReleaseEvent(mouseEvent);
}