#include "Bui_ObsPointDlg.h"
#include "ObsPointDataModel.h"
#include "CheckBoxDelegate.h"
#include "ObjectTable.h"


Bui_ObsPointDlg::Bui_ObsPointDlg(CObsPointArray *pointArray,
					bool showSelector,
					bool showSigma,
					bool allowSigmaChange,
					bool allowDeletePoint,
					bool allowLabelChange,
					QWidget *parent)
	: CAdjustmentDlg(parent), tableView(0)
{
	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	ViewParameter viewParameter;
	viewParameter.showSelector = showSelector;
	viewParameter.showSigma = showSigma;
	viewParameter.showCoordinates = true;
	viewParameter.allowSigmaChange = allowSigmaChange;
	viewParameter.allowLabelChange = allowLabelChange;
	viewParameter.allowDeletePoint = allowDeletePoint;
	viewParameter.allowBestFitEllipse = false;
	viewParameter.allowXYZAveragePoints = false;

	CObsPointDataModel* dataModel = new CObsPointDataModel(pointArray,viewParameter);
	this->tableView = new CObjectTable(dataModel,parent);
	this->layout()->addWidget(this->tableView);

}

Bui_ObsPointDlg::~Bui_ObsPointDlg()
{
	
}

void Bui_ObsPointDlg::cleanUp()
{

}

void Bui_ObsPointDlg::elementDeleted(CLabel element)
{
}

bool Bui_ObsPointDlg::okButtonReleased()
{

	return true;
}

bool Bui_ObsPointDlg::cancelButtonReleased()
{
	return true;
}

CCharString Bui_ObsPointDlg::helpButtonReleased()
{
	return "";
}


void Bui_ObsPointDlg::elementsChangedSlot(CLabel element)
{

}
