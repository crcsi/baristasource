#include "Bui_DifferenceUnitDlg.h"
#include "Icons.h"

bui_DifferenceUnitDlg::bui_DifferenceUnitDlg(void)
{	
	setupUi(this);
	connect(this->OK,SIGNAL(released()),this,SLOT(OK_released()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
}

bui_DifferenceUnitDlg::~bui_DifferenceUnitDlg(void)
{
}

void bui_DifferenceUnitDlg::OK_released()
{
    this->close();
}


void bui_DifferenceUnitDlg::setDegreeChecked()
{
    this->degreeButton->setChecked(true);
}


void bui_DifferenceUnitDlg::setMeterChecked()
{
    this->meterButton->setChecked(true);
}


bool bui_DifferenceUnitDlg::meterChecked()
{
    return this->meterButton->isChecked();
}


bool bui_DifferenceUnitDlg::degreeChecked()
{
    return this->degreeButton->isChecked();
}


