#include "Bui_MeasureHeightDlg.h"
#include "Icons.h"

bui_MeasureHeightDlg::bui_MeasureHeightDlg(void)
{	
	setupUi(this);
	connect(this->Ok,SIGNAL(released()),this,SLOT(Ok_clicked()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
}

bui_MeasureHeightDlg::~bui_MeasureHeightDlg(void)
{
}

void bui_MeasureHeightDlg::setHeightValue( double height )
{
    this->HeightValue->setText(QString( "%1" ).arg(height, 0, 'F', 4 ));

}


void bui_MeasureHeightDlg::Ok_clicked()
{
    this->close();
}

