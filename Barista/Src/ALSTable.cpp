#include "ALSTable.h"

#include <QCloseEvent>
#include "VALS.h"

CALSTable::CALSTable(CVALS* ALS, QWidget* parent) : CBaristaTable(parent), als(ALS)
{
	this->initTable();
}

CALSTable::~CALSTable()
{
	this->als = 0;
}

void CALSTable::closeEvent ( QCloseEvent * e )
{
	e->accept();
}


void CALSTable::setALS(CVALS* ALS)
{
    this->als = ALS;
}


void CALSTable::initTable()
{
    CCharString str("ALS Parameters:   ");
	str = str + this->als->getFileName().GetFullFileName();
	this->setWindowTitle(str.GetChar());

	this->setAccessibleName(this->als->getFileNameChar());
	this->setColumnCount(2);
	int nrows = 16;
	this->setRowCount(nrows);

	for (int i=0; i < nrows; i++)
		this->setRowHeight(i,20);

	QString yesStr("Yes");
	QString noStr("No");

	this->setColumnWidth(0, 230);
	this->setColumnWidth(1, 100);
	QStringList headerList;

	headerList.append(QString("Parameter" ));
	headerList.append(QString("Value" ));

	this->setHorizontalHeaderLabels(headerList);
     
    double value = 1233;

    bool load = false;

	CALSDataSet *alsdata = this->als->getCALSData();

	QString sys      = alsdata->getReferenceSystem().getCoordinateSystemName().GetChar();
	C3DPoint min     = alsdata->getPMin();
	C3DPoint max     = alsdata->getPMax();
	C2DPoint pdist   = alsdata->getPointDist();
	QString fileDisk = alsdata->getFileNameChar();
	int nstrips      = alsdata->nStrips();
	int npts         = alsdata->getNPoints();
	
	eALSPulseModeType pmode = alsdata->getPulsMode();
	QString pmodeStr;

	if (pmode == eALSFirst) pmodeStr = "First pulse";
	else if (pmode == eALSLast) pmodeStr = "Last pulse";
	else pmodeStr = "First and last pulse";

	QTableWidgetItem* item;
	
	////// ALS parameters
	item = this->createItem("Coordinate System",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,0,item);
	item = this->createItem(sys,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,1,item);

	item = this->createItem("Min X",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(1,0,item);
	item = this->createItem(QString( "%1" ).arg(min.x, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(1,1,item);
	item = this->createItem("Min Y",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(2,0,item);
	item = this->createItem(QString( "%1" ).arg(min.y, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(2,1,item);

	item = this->createItem("Max X",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(3,0,item);
	item = this->createItem(QString( "%1" ).arg(max.x, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(3,1,item);
	item = this->createItem("Max Y",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(4,0,item);
	item = this->createItem(QString( "%1" ).arg(max.y, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(4, 1,item);

	item = this->createItem("Min Height",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(5,0,item);
	item = this->createItem(QString( "%1" ).arg(min.z, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(5,1,item);
	item = this->createItem("Max Height",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(6,0,item);
	item = this->createItem(QString( "%1" ).arg(max.z, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(6,1,item);

	item = this->createItem("Point spacing in flight direction",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(7,0,item);
	item = this->createItem(QString( "%1" ).arg(pdist.x, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(7,1,item);
	item = this->createItem("Point spacing across flight direction",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(8,0,item);
	item = this->createItem(QString( "%1" ).arg(pdist.y, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(8,1,item);
	
	item = this->createItem("Pulse Mode",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(9,0,item);
	item = this->createItem(pmodeStr,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(9,1,item);

	item = this->createItem("Time recorded",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(10,0,item);
	if (alsdata->getHasTime()) item = this->createItem(yesStr,0,Qt::AlignRight,QColor(0,0,0));
	else item = this->createItem(noStr,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(10,1,item);

	item = this->createItem("Intensity recorded",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(11,0,item);
	if (alsdata->getHasIntensity()) item = this->createItem(yesStr,0,Qt::AlignRight,QColor(0,0,0));
	else item = this->createItem(noStr,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(11,1,item);

	item = this->createItem("Number of Strips",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(12,0,item);
	item = this->createItem(QString( "%1" ).arg(nstrips),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(12,1,item);
	
	item = this->createItem("Number of points",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(13,0,item);
	item = this->createItem(QString( "%1" ).arg(npts),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(13,1,item);

	////////////////////

	item = this->createItem("ALS project name",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(14,0,item);

	QString projname(this->als->getFileName().GetFullFileName());

	item = this->createItem(projname,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(14,1,item);

	item = this->createItem("File name on disk",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(15,0,item);

	QString filename(this->als->getDiskFileNameChar());

	int col1width = 6*filename.length();
	this->setColumnWidth(1, col1width);

	item = this->createItem(filename,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(15,1,item);
}
