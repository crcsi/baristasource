#include "Bui_CameraDlg.h"
#include "CameraFactory.h"
#include "DistortionFactory.h"
#include "DistortionALOS.h"

#include "EditorDelegate.h"
#include "CheckBoxDelegate.h"
#include <QHeaderView>
#include "newmat.h"
#include <QMouseEvent>
#include "PushbroomScanner.h"
#include "BaristaProject.h"

Bui_CameraDlg::Bui_CameraDlg(CCamera* existingCamera,CPushBroomScanner* scanner,QWidget *parent): CAdjustmentDlg(parent),
	existingCamera(existingCamera),dataModel(0),existingScanner(scanner),useAlosParameter(true)
{

	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->existingCamera->addListener(this);

	if (this->existingScanner)
	{
		this->existingScanner->addListener(this);
	}
	else
		this->cB_Alos->setVisible(false);

	this->connect(this->lE_Name,  SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Name,  SIGNAL(editingFinished()),this,SLOT(leNameEditingFinished()));



	this->localCopyOfCamera = CCameraFactory::createCamera(this->existingCamera);
	if (this->localCopyOfCamera->getDistortion() && 
		this->localCopyOfCamera->getDistortion()->getDistortionType() == eDistortionALOS) 
	{
		this->distortion = CDistortionFactory::createDistortion(this->localCopyOfCamera->getDistortion());
		this->initActiveParameter();

		this->dataModel = new CDistortionDataModel(this->distortion,activeParameter);

		this->tV_Distortion->setItemDelegateForColumn(1,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
		this->tV_Distortion->setItemDelegateForColumn(2,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
		this->tV_Distortion->setItemDelegateForColumn(3,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
		this->tV_Distortion->setItemDelegateForColumn(4,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
		this->tV_Distortion->setItemDelegateForColumn(5,new CCheckBoxDelegate(this));

		this->tV_Distortion->verticalHeader()->setDefaultSectionSize(20);
		this->tV_Distortion->verticalHeader()->hide();
		this->tV_Distortion->setSelectionMode(QTableView::SingleSelection);

		this->tV_Distortion->setModel(this->dataModel);

		this->tV_Distortion->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
		this->tV_Distortion->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
		this->tV_Distortion->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);
		this->tV_Distortion->horizontalHeader()->setSectionResizeMode(3,QHeaderView::Stretch);
		this->tV_Distortion->horizontalHeader()->setSectionResizeMode(4,QHeaderView::Stretch);
		if (this->dataModel->columnCount(QModelIndex()) == 6)
		{
			this->tV_Distortion->horizontalHeader()->setSectionResizeMode(5,QHeaderView::Fixed);
			this->tV_Distortion->setColumnWidth(5,25);
		}

		this->tV_Distortion->setColumnWidth(0,170);

		this->connect(this->tV_Distortion,SIGNAL(clicked(const QModelIndex&)),this->dataModel,SLOT(itemClicked(const QModelIndex&)));
		this->connect(this->cB_Alos,SIGNAL(stateChanged(int)),this,SLOT(cBAlosParameterStateChanged(int)));
		this->connect(this->pB_Reset,SIGNAL(released()),this,SLOT(pBResetReleased()));
		this->connect(this->pB_ResetAll,SIGNAL(released()),this,SLOT(pBResetAllReleased()));
		this->connect(this->pB_Apply,SIGNAL(released()),this,SLOT(pBApplyReleased()));



		if (this->existingScanner)
			this->useAlosParameter = this->existingScanner->getParameterCount(CSensorModel::addPar);

	}
	else if(this->localCopyOfCamera->getDistortion() && 
		this->localCopyOfCamera->getDistortion()->getDistortionType() == eDistortionTHEOS)  //shijie liu
	{
		this->distortion = CDistortionFactory::createDistortion(this->localCopyOfCamera->getDistortion());
		
		this->gB_AlosDistortion->setVisible(false);
	}
	else 
	{
		this->distortion = NULL;
		this->gB_AlosDistortion->setVisible(false);
	}


	this->applySettings();
}

Bui_CameraDlg::~Bui_CameraDlg()
{
	this->cleanUp();
}

void Bui_CameraDlg::cleanUp()
{
	if (this->localCopyOfCamera)
		delete this->localCopyOfCamera;
	this->localCopyOfCamera = NULL;

	if (this->existingCamera)
		this->existingCamera->removeListener(this);
	this->existingCamera = NULL;
	
	if (this->distortion)
		delete this->distortion;
	this->distortion = NULL;

	if (this->existingScanner)
		this->existingScanner->removeListener(this);

	this->existingScanner = NULL;
}


bool Bui_CameraDlg::okButtonReleased()
{
	this->listenToSignal=false;
	CDistortionFactory::copyDistortion(this->localCopyOfCamera->getDistortion(),this->distortion);
	CCameraFactory::copyCamera(this->existingCamera,this->localCopyOfCamera);
	this->saveActiveParameter(this->existingScanner);

// To keep sensor models sharing a camera consistent
	for (int i = 0; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		if (img->hasPushbroom() && this->existingScanner)
		{
			CPushBroomScanner *push = img->getPushbroom();
			if ((push != this->existingScanner) && (push->getGenericCamera() == this->existingScanner->getGenericCamera()))
			{
				int nAddPar = this->existingScanner->getParameterCount(CSensorModel::addPar);
				int nInteriorPar = this->existingScanner->getParameterCount(CSensorModel::interiorPar);
				push->setActiveParameter(CSensorModel::addPar,this->existingScanner->getActiveParameter(CSensorModel::addPar));
				push->setActiveParameter(CSensorModel::interiorPar, this->existingScanner->getActiveParameter(CSensorModel::interiorPar));

				push->setParameterCount(CSensorModel::addPar,nAddPar);
				push->setParameterCount(CSensorModel::interiorPar,nInteriorPar);
		
			}
		}
	}

	this->listenToSignal =true;

	return true;
}

bool Bui_CameraDlg::cancelButtonReleased()
{
	return true;
}

CCharString Bui_CameraDlg::helpButtonReleased()
{
	return "UserGuide/ThumbNodeCCDCamera.html";
}


void Bui_CameraDlg::initActiveParameter()
{
	if (this->existingScanner)
	{
		if (this->existingScanner->getParameterCount(CSensorModel::addPar))
			this->useAlosParameter = true;
		else
			this->useAlosParameter = false;

		this->activeParameter.clear();
		this->activeParameter.push_back(!this->existingScanner->getActiveParameterStatus(CSensorModel::addPar,PARAMETER_1));
		this->activeParameter.push_back(!this->existingScanner->getActiveParameterStatus(CSensorModel::addPar,PARAMETER_2));
		this->activeParameter.push_back(!this->existingScanner->getActiveParameterStatus(CSensorModel::addPar,PARAMETER_3));
		this->activeParameter.push_back(!this->existingScanner->getActiveParameterStatus(CSensorModel::addPar,PARAMETER_4));
		this->activeParameter.push_back(!this->existingScanner->getActiveParameterStatus(CSensorModel::addPar,PARAMETER_5));
		this->activeParameter.push_back(!this->existingScanner->getActiveParameterStatus(CSensorModel::addPar,PARAMETER_6));
		/*
		if(this->localCopyOfCamera->getDistortion())//shijie liu
		{
			if(this->localCopyOfCamera->getDistortion()->getDistortionType() == eDistortionTHEOS)
			{
				this->activeParameter.push_back(!this->existingScanner->getActiveParameterStatus(CSensorModel::addPar,PARAMETER_7));
				this->activeParameter.push_back(!this->existingScanner->getActiveParameterStatus(CSensorModel::addPar,PARAMETER_8));
			}
		}//*/
	}
}

void Bui_CameraDlg::saveActiveParameter(CPushBroomScanner* scanner)
{
	if (scanner && this->activeParameter.size())
	{

		if (this->activeParameter[0])
			scanner->deactivateParameter(CSensorModel::addPar,PARAMETER_1);
		else
			scanner->activateParameter(CSensorModel::addPar,PARAMETER_1);

		if (this->activeParameter[1])
			scanner->deactivateParameter(CSensorModel::addPar,PARAMETER_2);
		else
			scanner->activateParameter(CSensorModel::addPar,PARAMETER_2);

		if (this->activeParameter[2])
			scanner->deactivateParameter(CSensorModel::addPar,PARAMETER_3);
		else
			scanner->activateParameter(CSensorModel::addPar,PARAMETER_3);

		if (this->activeParameter[3])
			scanner->deactivateParameter(CSensorModel::addPar,PARAMETER_4);
		else
			scanner->activateParameter(CSensorModel::addPar,PARAMETER_4);

		if (this->activeParameter[4])
			scanner->deactivateParameter(CSensorModel::addPar,PARAMETER_5);
		else
			scanner->activateParameter(CSensorModel::addPar,PARAMETER_5);

		if (this->activeParameter[5])
			scanner->deactivateParameter(CSensorModel::addPar,PARAMETER_6);
		else
			scanner->activateParameter(CSensorModel::addPar,PARAMETER_6);

		if (!this->useAlosParameter)
			scanner->deactivateParameterGroup(CSensorModel::addPar);

	}	
}


void Bui_CameraDlg::applySettings()
{
	int digits1 = 1;
	int digits2 = 3;

	this->lE_Name->setText(QString("%1").arg(this->localCopyOfCamera->getName().GetChar())); 

	this->lE_X->setText(QString("%1").arg(this->localCopyOfCamera->getIRP().x,0,'f',digits1));
	this->lE_Y->setText(QString("%1").arg(this->localCopyOfCamera->getIRP().y,0,'f',digits1));
	this->lE_C->setText(QString("%1").arg(this->localCopyOfCamera->getIRP().z,0,'f',digits1));

	this->lE_Scale->setText(QString("%1").arg(this->localCopyOfCamera->getScale()*1000.0,0,'f',digits2));

	if (this->dataModel)
		this->dataModel->update();

	this->cB_Alos->blockSignals(true);
	this->cB_Alos->setChecked(this->useAlosParameter);
	this->cB_Alos->blockSignals(false);

	checkAvailabilityOfSettings();
}

void Bui_CameraDlg::checkAvailabilityOfSettings()
{
	if (this->lE_Name->text().isEmpty())
	{
		emit enableOkButton(false);
	}
	else
		emit enableOkButton(true);

	if (this->activeParameter.size() && !this->useAlosParameter)
	{
		this->pB_Reset->setEnabled(false);
		this->pB_ResetAll->setEnabled(false);
		this->tV_Distortion->setEnabled(false);
	}
	else if (this->activeParameter.size() && this->useAlosParameter)
	{
		this->pB_Reset->setEnabled(true);
		this->pB_ResetAll->setEnabled(true);
		this->tV_Distortion->setEnabled(true);
	}
}

void Bui_CameraDlg::leTextChanged(const QString & text)
{
	this->checkAvailabilityOfSettings();
}

void Bui_CameraDlg::leNameEditingFinished()
{
	this->localCopyOfCamera->setName(this->lE_Name->text().toLatin1().constData());
	
	this->applySettings();
}


void Bui_CameraDlg::cBAlosParameterStateChanged(int state)
{
	if (state == Qt::Checked)
		this->useAlosParameter = true;
	else 
		this->useAlosParameter = false;

	this->applySettings();

}



void Bui_CameraDlg::pBResetReleased()
{
	if (this->distortion)
	{
		this->distortion->resetParameter();
		this->applySettings();
	}

}

void Bui_CameraDlg::pBResetAllReleased()
{
	if (this->distortion)
	{
		for (int i=0; i < project.getNumberOfImages(); i++)
		{
			CVImage* image = project.getImageAt(i);
			if (!image->hasPushbroom())
				continue;

			CCamera* c = image->getPushbroom()->getCamera();
			
			if (! c || !c->getDistortion() || c->getDistortion()->getDistortionType() != eDistortionALOS)
				continue;

			c->resetADPParameter();

		}
	}

}

void Bui_CameraDlg::pBApplyReleased()
{
	this->listenToSignal = false;
	if (this->distortion)
	{
		for (int i=0; i < project.getNumberOfImages(); i++)
		{
			CVImage* image = project.getImageAt(i);
			if (!image->hasPushbroom())
				continue;
			CPushBroomScanner* p = image->getPushbroom();
			CCamera* c = p->getCamera();
			
			if (! c || !c->getDistortion() || c->getDistortion()->getDistortionType() != eDistortionALOS)
				continue;

			CDistortion* d= c->getDistortion();
			d->setSigmaDirectObservation(this->distortion->getSigmaDirectObservation(PARAMETER_1),PARAMETER_1);
			d->setSigmaDirectObservation(this->distortion->getSigmaDirectObservation(PARAMETER_2),PARAMETER_2);
			d->setSigmaDirectObservation(this->distortion->getSigmaDirectObservation(PARAMETER_3),PARAMETER_3);
			d->setSigmaDirectObservation(this->distortion->getSigmaDirectObservation(PARAMETER_4),PARAMETER_4);
			d->setSigmaDirectObservation(this->distortion->getSigmaDirectObservation(PARAMETER_5),PARAMETER_5);
			d->setSigmaDirectObservation(this->distortion->getSigmaDirectObservation(PARAMETER_6),PARAMETER_6);

			this->saveActiveParameter(p);
			c->informListener();
		}
	}

	this->listenToSignal = true;
}



void Bui_CameraDlg::elementsChangedSlot(CLabel element)
{
	if (!this->listenToSignal)
		return;

	CCameraFactory::copyCamera(this->localCopyOfCamera,this->existingCamera);
	CDistortionFactory::copyDistortion(this->distortion,this->localCopyOfCamera->getDistortion());
	this->initActiveParameter();
	this->applySettings();

}





// ###############################################################################################
// ###############################################################################################
// ###############################################################################################

CDistortionDataModel::CDistortionDataModel(CDistortion* distortion,vector<bool> &activeParameter,QObject* parent ) 
	: QAbstractTableModel(parent), distortion(distortion),activeParameter(activeParameter)
{

}


int CDistortionDataModel::rowCount(const QModelIndex& parent) const
{
	if (this->distortion)
		return 6;
	else
		return 0;
}

int CDistortionDataModel::columnCount(const QModelIndex& parent) const
{
	if (this->activeParameter.size())
		return 6;
	else
		return 5;
}

QVariant CDistortionDataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role == Qt::TextAlignmentRole)
		return int(Qt::AlignRight | Qt::AlignVCenter);
	else if (role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole)
	{
		int parameter = 0;
		double returnValue=0.0;

		if (index.row() == 0)		parameter = PARAMETER_1;	
		else if (index.row() == 1)	parameter = PARAMETER_2;	
		else if (index.row() == 2)	parameter = PARAMETER_3;		
		else if (index.row() == 3)	parameter = PARAMETER_4;	
		else if (index.row() == 4)	parameter = PARAMETER_5;	
		else if (index.row() == 5)	parameter = PARAMETER_6;	
	

		if (index.column() == 0)
		{
			CCharString name;
			this->distortion->getParameterName(name,parameter);
			return QString("%1").arg(name.GetChar());
		}
		else if (index.column() == 1)
		{
			double value = this->distortion->getParameter(parameter);
			if (parameter == PARAMETER_2 ) 
			{				
				value -= 1.0;
				value *= 1e6;
			}
			else if (parameter == PARAMETER_5)
			{
				value *= 1e6;
			}


			if (role == Qt::DisplayRole)
				return QString().sprintf("%.2lf",value);
			else
				return QString("%1").arg(value);
		}
		else if (index.column() == 2)
		{
			double value = this->distortion->getSigmaParameter(parameter);
			if (parameter == PARAMETER_2 || parameter == PARAMETER_5)
			{
				value *= 1e6;
			}

			if (role == Qt::DisplayRole)
				return QString().sprintf("%.3lf",value);
			else
				return QString("%1").arg(value);
		}
		else if (index.column() == 3)
		{
			double value = this->distortion->getDirectObservation(parameter);
			if (parameter == PARAMETER_2 )
			{
				value -=1.0;
				value *= 1e6;
			}
			else if (parameter == PARAMETER_5)
			{
				value *= 1e6;
			}

			if (role == Qt::DisplayRole)
				return QString().sprintf("%.2lf",value);
			else
				return QString("%1").arg(value);
		}
		else if (index.column() == 4)
		{
			double value = this->distortion->getSigmaDirectObservation(parameter);
			if (parameter == PARAMETER_2 || parameter == PARAMETER_5)
			{
				value *= 1e6;
			}

			if (role == Qt::DisplayRole)
				return QString().sprintf("%.3lf",value);
			else
				return QString("%1").arg(value);
		}
		else if (index.column() == 5)
		{
			if (role == Qt::ToolTipRole)
				return this->activeParameter[index.row()] ? "parameter fixed" : "parameter free";
			else
				return (bool)this->activeParameter[index.row()];
		}
	}
	return QVariant();

	
}

QVariant CDistortionDataModel::headerData(int section,Qt::Orientation orientation, int role) const
{
	
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
	{
		if (section == 0)
			return "Parameter";
		else if (section == 1)
			return "Value";
		else if (section == 2)
			return "Precision";
		else if (section == 3)
			return "Observation";
		else if (section == 4)
			return "Sigma";
		else if (section == 5)
			return "fix";
	}

	return QVariant();

}


Qt::ItemFlags CDistortionDataModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
	 return Qt::ItemIsEnabled;

	if (index.column() == 4 )
		return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
	else if (index.column() == 5 )
		return QAbstractItemModel::flags(index) | Qt::ItemIsUserCheckable;
	else
		return (Qt::ItemFlags)(QAbstractItemModel::flags(index) - Qt::ItemIsSelectable);
}

bool CDistortionDataModel::setData(const QModelIndex &index,const QVariant &value, int role)
{
	if (index.isValid() && role == Qt::EditRole) 
	{
		int parameter = 0;

		if (index.row() == 0) parameter = PARAMETER_1;	
		else if (index.row() == 1)	parameter = PARAMETER_2;	
		else if (index.row() == 2)	parameter = PARAMETER_3;		
		else if (index.row() == 3)	parameter = PARAMETER_4;	
		else if (index.row() == 4)	parameter = PARAMETER_5;	
		else if (index.row() == 5)	parameter = PARAMETER_6;
		

		if (index.column() == 4 && !value.isNull())
		{
			double tmp = value.toDouble();
			if (parameter == PARAMETER_2 || parameter == PARAMETER_5)
				tmp /= 1e6;

			this->distortion->setSigmaDirectObservation(tmp,parameter);
		}
		else if (index.column() == 5 && !value.isNull())
			this->activeParameter[index.row()] = value.toBool();
		else
			return false;

		emit dataChanged(index, index);
		return true;
		
	}

	return false;
}


void CDistortionDataModel::update()
{
	QModelIndex indexTopLeft = this->index(0,0);
	QModelIndex indexBottomRight = this->index(5,5);

	emit dataChanged(indexTopLeft, indexBottomRight);
}



void CDistortionDataModel::itemClicked( const QModelIndex& index)
{
	if (index.column() == 5)
	{
		this->setData(index,!this->data(index,Qt::DisplayRole).toBool());
	}

}

