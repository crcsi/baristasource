#include <fstream>

#include "Bui_ImportGIS.h"
#include <QFileDialog>
#include <QMessageBox>

#include "Icons.h"
#include "IniFile.h"
#include "BaristaHtmlHelp.h"
#include "Bui_ReferenceSysDlg.h"
#include "BaristaProject.h"
#include "BaristaProjectHelper.h"
#include "3DPoint.h"

#include "ogrsf_frmts.h"

extern CBaristaProjectHelper baristaProjectHelper;

bui_ImportGISDialog::bui_ImportGISDialog () : filenameSelected(false), inputFilename(""), success(false)

{
	setupUi(this);

	connect(this->Inputfile,            SIGNAL(released()),        this, SLOT(onFileSelected()));
	connect(this->inputname,            SIGNAL(editingFinished()), this, SLOT(onFileChanged()));

	connect(this->HelpButton,           SIGNAL(released()),        this, SLOT(Help()));
	connect(this->buttonCancel,         SIGNAL(released()),        this, SLOT(Cancel()));
	connect(this->buttonOk,             SIGNAL(released()),        this, SLOT(OK()));


	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}

bui_ImportGISDialog ::~bui_ImportGISDialog ()
{
	this->destroy();
}

void bui_ImportGISDialog::destroy()
{

}

void bui_ImportGISDialog::init()
{
	this->filenameSelected = false;

	this->inputname->setEnabled(false);
	this->buttonOk->setEnabled(false);

}

void bui_ImportGISDialog::onFileSelected()
{
    // file dialog
	QString s = QFileDialog::getOpenFileName(
		this,
		"Choose GIS Data file",
		inifile.getCurrDir(),
		"GIS data (*.shp *.shx *.dbf *.dgn *.kml)");

    QFile test(s);
	CFileName fn = (const char*)s.toLatin1();
	
	if (!test.exists())
	{
		QMessageBox::warning( this, "Input file does not exist", " Input file does not exist");
		return;
	}
	else
	{
		this->inputname->setEnabled(true);
		ifstream istr(fn.GetChar());
		if (istr.is_open())
		{
			istr.close();
			this->inputFilename = fn.GetChar();
			this->inputname->setText(s);
			this->buttonOk->setEnabled(true);
			this->filenameSelected = true;
		}
	}
}

void bui_ImportGISDialog::onFileChanged()
{
  	QString n = this->inputname->text();

	CFileName fn(n.toLatin1().constData());

	ifstream istr(fn.GetChar());

	if (istr.is_open())
	{
		istr.close();
		this->inputFilename = fn;
		this->buttonOk->setEnabled(true);
		this->filenameSelected = true;
	}
	else
	{
		QMessageBox::warning( this, "Input file does not exist", " Input file does not exist");
		this->buttonOk->setEnabled(false);
		this->filenameSelected = false;
	}

}

void bui_ImportGISDialog::OK()
{
	/* DEPRECEATED
	
    OGRRegisterAll();

	CReferenceSystem refSys;
	refSys.sethasValues(true);

    OGRDataSource       *poDS;

	poDS = OGRSFDriverRegistrar::Open( this->inputFilename.GetChar(), false );
    if( poDS == NULL )
    {
        return;
    }
	*/

	GDALAllRegister();

	CReferenceSystem refSys;
	refSys.sethasValues(true);

	GDALDataset       *poDS;
	poDS = (GDALDataset*)GDALOpenEx(this->inputFilename.GetChar(), GDAL_OF_VECTOR, NULL, NULL, NULL);
	if (poDS == NULL)
	{
		return;
	}

	this->buttonOk->setEnabled(false);
	baristaProjectHelper.setIsBusy(true);

    OGRLayer  *poLayer = 0;

	
	int layercount = poDS->GetLayerCount();

    OGRFeature *poFeature;

	for ( int i = 0; i < layercount; i++)
	{
		poLayer = poDS->GetLayer(i);

		if ( !poLayer) continue;

///////////////////////////////////////////////////////////////////////////////////////////
		OGRSpatialReference* refInfo = poLayer->GetSpatialRef();

		if ( refInfo)
		{
			// set reference system values
			Matrix parMat(6,1);

			// check spheriod
			OGRErr errorCodeSemiMajor;
			OGRErr errorCodeInvFlattening;

			double semiMajor = refInfo->GetSemiMajor(&errorCodeSemiMajor);
			double invFlattening = refInfo->GetInvFlattening(&errorCodeInvFlattening);

			if (errorCodeSemiMajor != 0 || errorCodeInvFlattening != 0)
				refSys.sethasValues(false);
			else
			{
				// get information from string
				CSpheroid spheroid("", semiMajor,invFlattening);

				// compare with known spheroides
				if (spheroid == WGS84Spheroid) refSys.setToWGS1984();
				else if ( spheroid == GRS1980Spheroid ) refSys.setSpheroid(GRS1980Spheroid);
				else if ( spheroid == GRS1967Spheroid ) refSys.setSpheroid(GRS1967Spheroid);
				else if ( spheroid == BesselSpheroid ) refSys.setSpheroid(BesselSpheroid);
				else if ( spheroid == AustralianNationalSpheroid ) refSys.setSpheroid(AustralianNationalSpheroid);
				else if ( spheroid == Clarke1866Spheroid ) refSys.setSpheroid(Clarke1866Spheroid);
				else if ( spheroid == Clarke1880Spheroid ) refSys.setSpheroid(Clarke1880Spheroid);
				else if ( spheroid == HayfordSpheroid ) refSys.setSpheroid(HayfordSpheroid);
				else if ( spheroid == IERSSpheroid ) refSys.setSpheroid(IERSSpheroid);
				else // not known 
				{
					refSys.sethasValues(false);
				}
			}

			// check coordinate type
			if (refInfo->IsGeographic())
			{
				refSys.setCoordinateType(eGEOGRAPHIC);

				// switch longitude and latitude
				double swap = parMat.element(2,0); 
				parMat.element(2,0) = parMat.element(5,0); 
				parMat.element(5,0) = swap;
				swap = parMat.element(0,0); 
				parMat.element(0,0) = parMat.element(3,0); 
				parMat.element(3,0) = swap;
				swap = parMat.element(1,0); 
				parMat.element(1,0) = parMat.element(4,0); 
				parMat.element(4,0) = swap;
				refSys.setUTMZone(parMat.element(2,0), parMat.element(5,0));
			}

			else if( refInfo->IsProjected())
			{
				refSys.setCoordinateType(eGRID);
				CCharString pszProjection(refInfo->GetAttrValue( "PROJECTION" ));
			    
				if (!pszProjection.CompareNoCase(SRS_PT_TRANSVERSE_MERCATOR))
				{
					refSys.sethasValues(false);
				}
				else
				{
					int isNorth;
					int zone = refInfo->GetUTMZone(&isNorth);
					if (zone > 0) refSys.setUTMZone(zone, isNorth != 0 ? NORTHERN : SOUTHERN);
					else
					{
						refSys.setTMParameters(refInfo->GetNormProjParm( SRS_PP_CENTRAL_MERIDIAN, 0.0), 
											   refInfo->GetNormProjParm( SRS_PP_FALSE_EASTING, 0.0 ), 
											   refInfo->GetNormProjParm( SRS_PP_FALSE_NORTHING, 0.0), 
											   refInfo->GetProjParm( SRS_PP_SCALE_FACTOR, 1.0 ), 
											   refInfo->GetNormProjParm( SRS_PP_LATITUDE_OF_ORIGIN, 0.0 ) );		
					}
				}
			}
			else
				refSys.sethasValues(false);
		}
		else
			refSys.sethasValues(false);


////////////////////////////////////////////////////////////////////////////////////////////

		long featurecount = poLayer->GetFeatureCount();

		CLabel defaultplabel = "P";
		CLabel defaultllabel = "L";

		CLabel plabel = "";
		CLabel llabel = "";

		int mpointcount = project.getNumberOfMonoplottedPoints();
		if ( mpointcount > 0)
		{
			defaultplabel = project.getMonoplottedPoints()->getAt(mpointcount -1 )->getLabel();
			defaultplabel++;
		}


		int mlinecount = project.getNumberOfMonoplottedLines();
		if ( mlinecount > 0)
		{
			defaultllabel = project.getXYZPolyLines()->getAt(mlinecount -1 )->getLabel();
			defaultllabel++;
		}

		for ( long j = 0; j < featurecount; j++)
		{
			poFeature = poLayer->GetFeature(j);

			if (!poFeature) continue;

			OGRGeometry* geometry = poFeature->GetGeometryRef();

			if (!geometry) continue;

			OGRwkbGeometryType geotype = geometry->getGeometryType();

			if ( geotype == wkbLineString || geotype == wkbLineString25D)
			{
				int fieldCount = poFeature->GetFieldCount();

				if ( fieldCount == 3 )
				{
					const char* field0 = poFeature->GetFieldAsString(0);
					const char* field1 = poFeature->GetFieldAsString(1);
					const char* field2 = poFeature->GetFieldAsString(2);

					QString qstr(field0);
					qstr.append(field1);
					qstr.append(field2);

					if ( qstr.isEmpty() ) llabel = defaultllabel;
					else
					{
						llabel = (const char*)qstr.toLatin1();
						llabel +=  j;
					}
				}
				else
				{
					llabel = defaultllabel;
					llabel +=  j;
				}

				OGRLineString* linestring = (OGRLineString*)geometry;
				
				int pointcount = linestring->getNumPoints();

				OGRPoint ogrpoint;
				CXYZPoint point;
				CXYZPolyLine line;

				plabel = llabel;
				plabel += "P";
				
				for ( int n = 0; n < pointcount; n++)
				{
					linestring->getPoint(n, &ogrpoint);

					point.x = ogrpoint.getX();
					point.y = ogrpoint.getY();
					point.z = ogrpoint.getZ();

					// fits for Ikonos at south end
					//point.x = ogrpoint.getX() -1947138;
					//point.y = ogrpoint.getY() +1963935;

					// fits for SPOT
					//point.x = ogrpoint.getX() -1947052;
					//point.y = ogrpoint.getY() +1963712;

					// fits for orthos (IKONOS/SPOT)
					//point.x = ogrpoint.getX() -1947075;
					//point.y = ogrpoint.getY() +1963717;

					// fits for the UTM 45N coordinate data set from ARCGIS
					if ( refSys.gethasValues() )
					{
						if ( refSys.getZone() == 45 && refSys.getHemisphere() == NORTHERN)
						{
							point.x += -260;
							point.y +=  +66;
						}
					}


					point.setLabel(plabel++);

					line.addPoint(point);
					project.addMonoplottedPoint(point);
				}

				line.setLabel(llabel++);
				project.addMonoplottedLine(line);
				

			}
			else if ( geotype == wkbPoint || geotype == wkbPoint25D)
			{
				int fieldCount = poFeature->GetFieldCount();

				if ( fieldCount == 3 )
				{
					const char* field0 = poFeature->GetFieldAsString(0);
					const char* field1 = poFeature->GetFieldAsString(1);
					const char* field2 = poFeature->GetFieldAsString(2);

					QString qstr(field0);
					qstr.append(field1);
					qstr.append(field2);

					if ( qstr.isEmpty() ) plabel = defaultplabel;
					else
					{
						plabel = (const char*)qstr.toLatin1();
						plabel +=  j;
					}
				}
				else
					plabel = defaultplabel;


				OGRPoint* ogrpoint = (OGRPoint*)geometry;
				CXYZPoint point;
				point.x = ogrpoint->getX();
				point.y = ogrpoint->getY();
				point.z = ogrpoint->getZ();

				if ( refSys.gethasValues() )
				{
					if ( refSys.getZone() == 45 && refSys.getHemisphere() == NORTHERN)
					{
						point.x += -260;
						point.y +=  +66;
					}
				}

				point.setLabel(plabel++);
				project.addMonoplottedPoint(point);
			}
			else if ( geotype == wkbPolygon || geotype == wkbPolygon25D)
			{	
				OGRPolygon* polygon = (OGRPolygon*)geometry;
				OGRLinearRing* extRing = polygon->getExteriorRing();
				
				int pointcount = extRing->getNumPoints();

				OGRPoint ogrpoint;
				CXYZPoint point;
				CXYZPolyLine line;
				
				llabel = defaultllabel;
				llabel +=  j;

				plabel = llabel;
				plabel += "P";
				
				for ( int n = 0; n < pointcount; n++)
				{
					extRing->getPoint(n, &ogrpoint);

					point.x = ogrpoint.getX();
					point.y = ogrpoint.getY();
					point.z = ogrpoint.getZ();

					point.setLabel(plabel++);

					line.addPoint(point);

					project.addMonoplottedPoint(point);
				}

				line.setLabel(llabel++);
				project.addMonoplottedLine(line);
				

				int nRings = polygon->getNumInteriorRings();

				CLabel iRingllabel = llabel;

				iRingllabel += "_";

				for ( int k = 0; k < nRings; k++)
				{
					OGRLinearRing* intRing = polygon->getInteriorRing(k);

					pointcount = intRing->getNumPoints();

					iRingllabel += k;

					for ( int n = 0; n < pointcount; n++)
					{
						intRing->getPoint(n, &ogrpoint);

						CLabel iRingplabel = iRingllabel;
						iRingplabel += "_";
						iRingplabel += n;

						point.x = ogrpoint.getX();
						point.y = ogrpoint.getY();
						point.z = ogrpoint.getZ();

						point.setLabel(iRingplabel);

						line.addPoint(point);

						project.addMonoplottedPoint(point);
					}

					line.setLabel(iRingllabel);
					project.addMonoplottedLine(line);
				}
			}
			else
			{
				int stop = 9;
			}
			
		}

	}

	OGRFeature::DestroyFeature( poFeature );
    //OGRDataSource::DestroyDataSource( poDS );		// DEPRECEATED
	GDALClose(poDS);

	baristaProjectHelper.setIsBusy(false);
/// check RefSys

	if ( !refSys.gethasValues() )
	{
		Bui_ReferenceSysDlg dlg(&refSys,&this->inputFilename, "Set Reference system information","UserGuide/Import3DPoints.html",false,NULL,true);

		dlg.exec();

		if (!dlg.getSuccess()) return;
	}

	project.getXYZPolyLines()->setReferenceSystem(refSys);
	project.getMonoplottedPoints()->setReferenceSystem(refSys);

	this->buttonOk->setEnabled(true);
	this->success = true;
	this->destroy();
	this->close();
};

void bui_ImportGISDialog::Cancel()
{
	this->success = false;
	this->destroy();
	this->close();
}

void bui_ImportGISDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/ImportGIS.html");
};
