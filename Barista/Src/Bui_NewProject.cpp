#include "Bui_NewProject.h"

#include <QFileDialog>
#include "IniFile.h"
#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "SystemUtilities.h"

#include "BaristaProject.h"


Bui_NewProject::Bui_NewProject(QWidget *parent, bool isnewChipDb)	: QDialog(parent),success(false),iFileName(""),isnewChipDB(isnewChipDb)
{
	setupUi(this);
	this->success = false;

	// load and set window icon
	QPixmap icon; //  off-screen image representation that can be used as a paint device. 
	icon.loadFromData(Barista, sizeof(Barista), "PNG"); // Loads the Barista incon. Returns true if the pixmap was loaded successfully; otherwise returns false. 
	this->setWindowIcon(QIcon(icon));


	//addition on 15 Feb 2010 >>
	if (isnewChipDB) // settings for chip database
	{
		this->labelMeanHeight->setVisible(false);
		this->spinboxMeanHeight->setVisible(false);
		this->setWindowTitle("Start chip database");
		this->gBProjectName->setTitle("Database settings");
		this->label->setText("Database directory:");
		this->label_2->setText("Database name:");
	}
	//<< addition on 15 Feb 2010

	connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	connect(this->pBFilename,SIGNAL(released()),this,SLOT(pBFileNameReleased()));
	connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));
	connect(this->leProjectName,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->applySettings();

}
Bui_NewProject::~Bui_NewProject()
{
}


void Bui_NewProject::pBCancelReleased()
{

	this->close();
}

void Bui_NewProject::pBOkReleased()
{
	if (this->isnewChipDB == false)
	{
		double height = this->spinboxMeanHeight->value();
		project.setMeanHeight(height);
	}

	this->success= true;
	this->close();
}


int Bui_NewProject::exec()
{
	this->show();
	this->pBFileNameReleased();
	return QDialog::exec();
}


void Bui_NewProject::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/MenuFile.html#NewProject");
}

void Bui_NewProject::pBFileNameReleased()
{
   CCharString str = inifile.getCurrDir();
	QString s;
   if (this->isnewChipDB == false)
   {
	   str += CSystemUtilities::dirDelimStr + "NewProject";

		QString qstr( str.GetChar());

		// file dialog
		s = QFileDialog::getSaveFileName(
			this,
			"Choose a Barista Project File Name",
			qstr,
			"Barista Projects (*.bar)"); 

		if (s.isEmpty())
			return;

		if (!s.endsWith(".bar"))
			s += ".bar";    		
	}
   else
   {
		str += CSystemUtilities::dirDelimStr + "NewChipDatabase";

		QString qstr( str.GetChar());

		// file dialog
		s = QFileDialog::getSaveFileName(
			this,
			"Choose a Barista Chip Database File Name",
			qstr,
			"Barista Chip Databases (*.cdb)"); 

		if (s.isEmpty())
			return;

		if (!s.endsWith(".cdb"))
			s += ".cdb";
   }

	CFileName cfile = (const char*)s.toLatin1();
	this->iFileName = cfile.GetChar();
	this->applySettings(); // show the settings on the New Project Dialog
}

void Bui_NewProject::applySettings()
{ // show the settings on the New Project Dialog

	if (this->isnewChipDB == false)
	{
		this->spinboxMeanHeight->setRange(-100, 9000);
		this->spinboxMeanHeight->setSingleStep(50);
		
		if ( project.getMeanHeight() == -9999.0) project.setMeanHeight(0.0);
		this->setspinboxMeanHeight(project.getMeanHeight());
	}

	this->checkAvailabilityOfSettings();

	this->leProjectDirectory->setText(this->iFileName.GetPath().GetChar());
	this->leProjectName->setText(this->iFileName.GetFileName().GetChar());
}

void Bui_NewProject::checkAvailabilityOfSettings()
{
	if (this->iFileName.IsEmpty())
		this->pBOk->setEnabled(false);
	else
		this->pBOk->setEnabled(true);
}

void Bui_NewProject::leTextChanged( const QString & text )
{
	CFileName tmp = this->iFileName.GetPath() + CSystemUtilities::dirDelimStr;

	QString newText(text);

	if (this->isnewChipDB == false)
	{
		if (!newText.isEmpty() && !newText.endsWith(".bar"))
			newText += ".bar";  
	}
	else
	{
		if (!newText.isEmpty() && !newText.endsWith(".cdb"))
			newText += ".cdb";  
	}
	
	CCharString qText(newText.toLatin1());
	this->iFileName = tmp + qText;
	this->applySettings();
}


void Bui_NewProject::setspinboxMeanHeight(double height)
{
	this->spinboxMeanHeight->setValue(height);

}