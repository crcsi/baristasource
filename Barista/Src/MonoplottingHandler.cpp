#include "MonoplottingHandler.h"

#include <QMessageBox>
#include "BaristaProject.h"
#include "Bui_MainWindow.h"
#include "XYZPolyLineArray.h"


bool CMonoplottingHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	if (!project.getActiveDEM() && !project.getMeanHeightMonoplotting())
	{	
		bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

		if (!mainwindow->setActiveDEM())
			return false;
	}

	CVBase* vbase = v->getVBase();
	
	if (!vbase)
		return false;
	
	this->monoPlotter.setVDEM(project.getActiveDEM());

	eLSMTRAFOMODE trafomode = eLSMNOTFW;

	if ( v->getTFWMonoplotting() ) trafomode = eLSMTFW;

	const CSensorModel* smodel = vbase->getModel(trafomode);

	if (!smodel && v->getVImage() && vbase->getModel(eLSMTFW) )
	{
		int yesno = QMessageBox::question(v,
		"Active View has only TFW",
		"Use TFW for Monoplotting?",
		"&Yes", "&No",	0, 1  );

		if ( yesno == 1 ) { v->setTFWMonoplotting(false); smodel = NULL; return false; }
		if ( yesno == 0 ) 
		{
			v->setTFWMonoplotting(true);
			smodel = vbase->getModel(eLSMTFW);
		}
	}
			
	if ( !smodel || !smodel->gethasValues())
	{
		QMessageBox::warning( v, "No monoplotting possible!",
		"Active View has no SensorModel or TFW parameters!" );
		return false;
	}		

	this->monoPlotter.setModel(smodel);

	if ((!this->monoPlotter.getDEMToSensor()) || (!this->monoPlotter.getSensorToDEM()))
	{
		QMessageBox::warning( NULL, "Coordinate systems of sensor model and DEM are incompatible!",
								  "No monoplotting possible!");
		return false;
	}

	return true;
}

void CMonoplottingHandler::open()
{
	this->monoPlotter.init();

	if (!project.getActiveDEM() && !project.getMeanHeightMonoplotting())
	{	
		bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

		if (!mainwindow->setActiveDEM())
			return;
	}
	
	this->monoPlotter.setVDEM(project.getActiveDEM());
	
}

void CMonoplottingHandler::close()
{
	this->monoPlotter.init();	
	this->removeDrawingDataSets();
}

bool CMonoplottingHandler::redraw(CBaristaView *v)
{
	CVBase* vbase = v->getVBase();

	if (vbase)
	{	

		const CSensorModel* sm = vbase->getModel(eLSMTFW);

		// try TFW if nothing else there as sm
		if (!sm || !sm->gethasValues())
			return false;

		this->monoPlotter.setModel(sm);

		if ((!this->monoPlotter.getDEMToSensor()) || (!this->monoPlotter.getSensorToDEM()))
			return false;

		this->drawDEMBorder(v);
	}

	return true;
}


void CMonoplottingHandler::initialize(CBaristaView *v)
{
	redraw(v);
};

void CMonoplottingHandler::removeDrawingDataSets()
{
	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i< n ; i++)
	{
		this->baristaViews.getView(i)->removeAllDrawingDataSets();
	}
}

void CMonoplottingHandler::removeDrawingDataSet(const CLabel& dataSetName)
{
	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i< n ; i++)
	{
		this->baristaViews.getView(i)->removeDrawingDataSet(dataSetName);
	}
}

void CMonoplottingHandler::removePointFromDataSet(const CLabel& dataSetName,const CLabel& point)
{
	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i< n ; i++)
	{
		this->baristaViews.getView(i)->removePointDrawing(dataSetName,point);
	}
}


bool CMonoplottingHandler::backprojectPoint(CBaristaView *v,CXYPoint& p2DPoint,const CXYZPoint& p3DPoint)
{
	CVBase* vbase = v->getVBase();
	
	if (!vbase)
		return false;

	// try TFW if nothing else there as model
	const CSensorModel* model = vbase->getModel(eLSMTFW);
	
	if (!model || !model->gethasValues())
		return false;	

	CXYZPoint transformed;
	this->monoPlotter.setModel(model);
	CTrans3D* trans = this->monoPlotter.getDEMToSensor();
	trans->transform(transformed,p3DPoint);
	model->transformObj2Obs(p2DPoint,transformed);

	return true;
}


void CMonoplottingHandler::drawDEMBorder(CBaristaView *v)
{
	
	CXYZPolyLineArray* borderlines = this->monoPlotter.getDEMBorderLines();

	if (borderlines)
	{
		for ( int i =0; i < borderlines->GetSize(); i++ )
		{
			CXYZPolyLine* line = borderlines->getLine(i);
			CLabel label = "border";
			label += i;
			line->setLabel(label);

			CXYPolyLine new2DPolyLine;
			for (int j=0; j< line->getPointCount(); j++)
			{
				CXYPoint p;
				if (this->backprojectPoint(v,p, line->getPoint(j)))
				{
					CLabel plabel("P");
					plabel += j;
					p.setLabel(plabel);
					new2DPolyLine.addPoint(p);
				}
			}

			if(line->isClosed())
				new2DPolyLine.closeLine();
				

			QColor color(Qt::yellow);
			v->addDrawingDataSet(line->getLabel(),2,false);
			v->addPolyLineDrawing(line->getLabel(),&new2DPolyLine,color,1);
		}
	}

}

void CMonoplottingHandler::changeColor(const CLabel& dataSetName,QColor& color)
{
	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i< n ; i++)
	{
		this->baristaViews.getView(i)->changeAllTopologyColors(dataSetName,color);
	}
}