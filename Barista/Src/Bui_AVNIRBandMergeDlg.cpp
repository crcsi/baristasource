#include "Bui_AVNIRBandMergeDlg.h"
#include <QHeaderView>

#include "SystemUtilities.h"
#include "BaristaHtmlHelp.h"
#include "BaristaProject.h"
#include "Bui_ProgressDlg.h"
#include <QMessageBox>
#include <QModelIndex>
#include <QComboBox>
#include <QTreeWidgetItem>
#include "Icons.h"
#include "GDALFile.h"
#include "Bui_GDALExportDlg.h"
#include "ProgressThread.h"

class CCustomDelegate;

Bui_AVNIRBandMergeDlg::Bui_AVNIRBandMergeDlg(QWidget *parent) : success(false),selectedCamera(-1)
{
	this->setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	QStringList headerList;
	headerList << "Available Files";
	headerList << "Band";

	QStringList colorNames;
	colorNames << "red";
	colorNames << "green";
	colorNames << "blue";
	colorNames << "infrared";

	QList<QColor> colors;
	colors << QColor(Qt::red);
	colors << QColor(Qt::green);
	colors << QColor(Qt::blue);
	colors << QColor(Qt::magenta);

	this->tW_Files->setHeaderLabels(headerList);
	this->tW_Files->header()->setDefaultSectionSize(250);
	this->tW_Files->setItemDelegate(new CCustomDelegate(this->tW_Files,colorNames,colors));
	

	this->connect(this->pB_Merge,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pB_Cancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pB_Help,SIGNAL(released()),this,SLOT(pBHelpReleased()));
	this->connect(this->pB_FileName,SIGNAL(released()),this,SLOT(pBFileReleased()));

	this->connect(this->tW_Files,SIGNAL(itemSelectionChanged()),this,SLOT(tWFilesItemSelectionChanged()));
	this->connect(this->tW_Files,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(tWFilesItemChanged(QTreeWidgetItem*,int)));
	this->connect(this->tW_Files,SIGNAL(itemClicked(QTreeWidgetItem*,int)),this,SLOT(tWFilesItemClicked(QTreeWidgetItem*,int)));

	this->loadImages();
	this->checkSetup();
	this->success = true;
}

Bui_AVNIRBandMergeDlg::~Bui_AVNIRBandMergeDlg()
{
}


void Bui_AVNIRBandMergeDlg::pBOkReleased()
{

	// determine how many we are importing, just to show the user a number
	unsigned int n=0;
	unsigned int count=0;
	for (unsigned int group=0 ; group < this->data.size(); group++)
	{
		CGroupDataBandMerger& d = this->data[group];
		if (d.readyToImport && d.import)
			n++;
	}

	
	for (unsigned int group=0 ; group < this->data.size(); group++)
	{
		CGroupDataBandMerger& d = this->data[group];
		
		if (!d.readyToImport || ( d.readyToImport && !d.import) || !this->checkBandSelection(d.item))
		{
			if (d.mergedImage)
				project.deleteImage(d.mergedImage);
			d.mergedImage = NULL;
		}
		else
		{
			count++;
			
			this->changeBandOrder(d);
			
			CBandMerger merger;
			unsigned int startCol = CBandMerger::findBoundary(d.inputImages[0]->getHugeImage());
			bool ret = merger.init(d.inputImages,startCol,d.cameraName,d.method,d.mergedImage);
		
			CCharString progressText,saveText;
			
			progressText.Format("Merging AVNIR-2 bands %u/%u...",count,n);
			saveText.Format("Saving file on disk %u/%u...",count,n);

			if (ret)
			{
				bui_ProgressDlg dlg(new CBandMergeThread(merger),&merger,progressText.GetChar());
				dlg.exec();

				this->success = dlg.getSuccess();

				if (!this->success)
				{
					QMessageBox::warning(this,"Error while merging images",merger.getErrorMessage().GetChar());
				}
			
				// write the file on disk
				d.exportDialog->writeSettings();
				d.exportDialog->writeImage(saveText.GetChar());

				d.mergedImage->setCurrentSensorModel(ePUSHBROOM);

			}
			else
				QMessageBox::warning(this,"Error while merging images",merger.getErrorMessage().GetChar());
		}
	}

	this->close();
}

void Bui_AVNIRBandMergeDlg::pBCancelReleased()
{
	this->success = false;

	for (unsigned int group=0; group < this->data.size(); group++)
	{
		CGroupDataBandMerger& d = this->data[group];

		if (d.mergedImage)
			project.deleteImage(d.mergedImage);
		d.mergedImage = NULL;
	}
	this->close();
}

void Bui_AVNIRBandMergeDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/AlosMergeDlg.html");
}

void Bui_AVNIRBandMergeDlg::pBFileReleased()
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
		return;
	
	CGroupDataBandMerger& d = this->data[this->selectedCamera];

	d.exportDialog->exec();
	if (d.exportDialog->getSuccess())
	{
		d.filename = d.exportDialog->getFilename();
		this->lE_FileName->setText(d.filename.GetChar());
		this->lE_FileName->setToolTip(d.filename.GetChar());
		d.mergedImage->setName(d.filename);
	}	

	this->checkSetup();

}


void Bui_AVNIRBandMergeDlg::changeBandOrder(CGroupDataBandMerger& d)
{
	CVImage* R = NULL;
	CVImage* G = NULL;
	CVImage* B = NULL;
	CVImage* A = NULL;


	if (d.item)
	{
		for (int i=0; i< d.item->childCount() ; i++)
		{
			if (d.item->child(i)->data(1,Qt::UserRole).toInt() == 0)
				R = d.inputImages[i];
			else if (d.item->child(i)->data(1,Qt::UserRole).toInt() == 1)
				G = d.inputImages[i];
			else if (d.item->child(i)->data(1,Qt::UserRole).toInt() == 2)
				B = d.inputImages[i];
			else if (d.item->child(i)->data(1,Qt::UserRole).toInt() == 3)
				A = d.inputImages[i];
		}

		// change the order
		if (R)
			d.inputImages[0] = R;
		
		if (G)
			d.inputImages[1] = G;

		if (B)
			d.inputImages[2] = B;

		if (d.inputImages.size() == 4 && A)
			d.inputImages[3] = A;
	}


}


void Bui_AVNIRBandMergeDlg::checkSetup()
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
	{
		this->pB_Merge->setEnabled(false);
	}
	else
	{
		this->pB_Merge->setEnabled(false);

		CGroupDataBandMerger& d = this->data[this->selectedCamera];

		// ckeck if we can import this group
		if (d.filename.IsEmpty() || !this->checkBandSelection(d.item))
		{
			d.readyToImport = false;			
		}
		else
		{
			d.readyToImport = true;	
		}

		// adjust check state
		this->tW_Files->blockSignals(true);
		if (d.readyToImport && d.import)
			d.item->setCheckState(0,Qt::Checked);
		else
			d.item->setCheckState(0,Qt::Unchecked);
		this->tW_Files->blockSignals(false);

		// set flags for all items
		for (unsigned int group=0; group < this->data.size(); group++)
		{
			CGroupDataBandMerger& d = this->data[group];
			
			if (d.readyToImport)
			{
				if (d.import)
					this->pB_Merge->setEnabled(true);
				
				if (!(d.item->flags() & Qt::ItemIsUserCheckable))
					d.item->setFlags(Qt::ItemFlag(d.item->flags() + Qt::ItemIsUserCheckable));
			}
			else
			{
				if (d.item->flags() & Qt::ItemIsUserCheckable)
					d.item->setFlags(Qt::ItemFlags(d.item->flags() - Qt::ItemIsUserCheckable));
			}
		}
	}
}

bool Bui_AVNIRBandMergeDlg::checkBandSelection(QTreeWidgetItem* parent)
{
	bool hasRed = false;
	bool hasGreen = false;
	bool hasBlue = false;
	bool hasAlpha = false;

	for (int i=0; i< parent->childCount() ; i++)
	{
		int index = parent->child(i)->data(1,Qt::UserRole).toInt();
		if (index == 0)
			hasRed = true;
		else if (index == 1)
			hasGreen = true;
		else if (index == 2)
			hasBlue = true;
		else if (index == 3)
			hasAlpha = true;
	}

	bool ret = false;
	if (hasRed && hasGreen && hasBlue)
	{
		ret = true;
		if (parent->childCount() == 4 && !hasAlpha)
			ret = false;
	}

	return ret;
}

void Bui_AVNIRBandMergeDlg::loadImages()
{
	vector<vector<CVImage*> > possibleImages;
	vector<CCharString> cameraNames;
	this->data.clear();

	CBandMerger::findImageGroups(possibleImages,cameraNames);

	QStringList colorNames;
	colorNames << "red";
	colorNames << "green";
	colorNames << "blue";
	colorNames << "infrared";
	
	QList<QColor> colors;
	colors << QColor(Qt::red);
	colors << QColor(Qt::green);
	colors << QColor(Qt::blue);
	colors << QColor(Qt::magenta);



	QList<QTreeWidgetItem *> items;
	this->data.resize(possibleImages.size());

	for (unsigned int i=0; i < possibleImages.size(); i++)
	{
		CGroupDataBandMerger& d = this->data[i];

		d.cameraName = cameraNames[i];
		d.inputImages = possibleImages[i];

		// set default file name
		d.filename = possibleImages[i][0]->getHugeImage()->getFileName()->GetPath() + CSystemUtilities::dirDelimStr + d.cameraName + ".tif";

		// init everything for import
		
		// create new image in project
		d.mergedImage = project.addImage(d.filename.GetFileName(), false);
		
		// set defaults
		d.mergedImage->getHugeImage()->bpp   = 8;
		d.mergedImage->getHugeImage()->bands = possibleImages[i].size();

		d.mergedImage->setName(d.filename);

		d.exportDialog = new Bui_GDALExportDlg(d.filename,d.mergedImage,false,"Select Output File",true,false,true);
		d.exportDialog->rBBandReleased();
		

		QTreeWidgetItem* parent = new QTreeWidgetItem((QTreeWidget*)NULL,QStringList(QString("%1").arg(d.cameraName.GetChar())));
		parent->setData(0,Qt::UserRole,i); // remember index into array
		parent->setData(1,Qt::UserRole,-1); // set invalid index
		items.append(parent);
		parent->setCheckState(0,Qt::Unchecked);
		parent->setFlags(Qt::ItemFlag(parent->flags() + Qt::ItemIsEditable));

		d.item = parent;

		for (unsigned int k= 0; k < d.inputImages.size(); k++)
		{
			QTreeWidgetItem* item = new QTreeWidgetItem(parent,QStringList(QString().sprintf("%s",d.inputImages[k]->getFileNameChar())));
			item->setData(0,Qt::UserRole,-1); // no index to remember
			
			item->setData(1,Qt::DisplayRole,colorNames.at(k));
			item->setTextColor(1,colors.at(k));
			item->setData(1,Qt::UserRole,k);
			item->setFlags(Qt::ItemFlag(item->flags() + Qt::ItemIsEditable));

			items.append(item);
			
		}

		
		
	}
	
	this->tW_Files->setColumnCount(2);
	this->tW_Files->insertTopLevelItems(0, items); 

	if (this->data.size() > 0)
	{
		this->tW_Files->setCurrentItem(items.at(0));
	}


}

void Bui_AVNIRBandMergeDlg::tWFilesItemSelectionChanged()
{
	QList<QTreeWidgetItem *> list = this->tW_Files->selectedItems();

	if (list.size() != 1)
		return;

	int index = list.at(0)->data(0,Qt::UserRole).toInt();

	if (index != -1 && index < int(this->data.size()) && index != this->selectedCamera)
	{
		this->selectedCamera = index;
	
		if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
			return;
	
		// change the gui
		this->selectImageGroup(this->selectedCamera);
	}
	else
	{
		QTreeWidgetItem * parent = list.at(0)->parent();
		
		if (parent)
		{
			this->tW_Files->setCurrentItem(parent);
		}
	}

	this->checkSetup();
}

void Bui_AVNIRBandMergeDlg::selectImageGroup(unsigned int group)
{
	if (group < 0 || group >= this->data.size())
		return;

	CGroupDataBandMerger& d = this->data[group];
	
	// change file name
	this->lE_FileName->blockSignals(true);
	this->lE_FileName->setText(d.filename.GetChar());
	this->lE_FileName->setToolTip(d.filename.GetChar());
	this->lE_FileName->blockSignals(false);


}


void Bui_AVNIRBandMergeDlg::tWFilesItemChanged(QTreeWidgetItem* item,int col)
{
	if (!item)
		return;
	
	int index = item->data(col,Qt::UserRole).toInt();

	if (col == 0 && index != -1 && index < int(this->data.size()) && index == this->selectedCamera)
	{
		CGroupDataBandMerger& d = this->data[this->selectedCamera];

		bool chekSetup = false;
		// has the check state changed?
		if (d.readyToImport)
		{
			if (d.import && d.item->checkState(0) == Qt::Unchecked)
				d.import = false;
			else if (!d.import && d.item->checkState(0) == Qt::Checked)
				d.import = true;

			chekSetup = true;
			
		}

		// has the camera name changed?
		CCharString newName(d.item->text(0).toLatin1().constData());
		if (!d.cameraName.CompareNoCase(newName))
		{
			
			if (!this->checkCameraName(newName,d.cameraName))
			{
				// change display 
				this->tW_Files->blockSignals(true);
				d.item->setText(0,newName.GetChar());
				this->tW_Files->blockSignals(true);
			}

			d.cameraName = newName;
			chekSetup = true;
		}
		
		if (chekSetup)
			this->checkSetup();

	}
	else if (col == 1)
	{
		this->checkSetup();
	}
}

void Bui_AVNIRBandMergeDlg::tWFilesItemClicked(QTreeWidgetItem * item,int column)
{
	if (column == 1 && item->data(1,Qt::UserRole).toInt() >= 0) 
		this->tW_Files->editItem(item,column);


}


bool Bui_AVNIRBandMergeDlg::checkCameraName(CCharString& newName,const CCharString& oldName)
{
	// check with local cameras
	for (unsigned int group=0; group < this->data.size(); group++)
	{
		if (this->data[group].cameraName.CompareNoCase(newName))
		{
			newName = oldName; // use the old name
			return false;
		}
	}
	
	// check if camera with same name is already in project
	return project.getUniqueCameraName(newName);
}



// ###############################################################################################
// ###############################################################################################

CGroupDataBandMerger::~CGroupDataBandMerger() 
{
	if (!this->exportDialog)
		delete this->exportDialog;
	this->exportDialog = NULL;

}


// ###############################################################################################
// ###############################################################################################


CCustomDelegate::CCustomDelegate(QObject* parent,const QStringList& items,const QList<QColor>& colors) :
		items(items),colors(colors)
{

}



QWidget *CCustomDelegate::createEditor(QWidget *parent,const QStyleOptionViewItem & option, const QModelIndex &index) const
{
	QWidget*  editor= NULL;
	
	if (index.isValid())
	{
		if (index.column() == 1 && index.model()->data(index,Qt::UserRole).toInt() >= 0)
		{
			QComboBox * cb = new QComboBox(parent);
			cb->addItems(this->items);
			editor = cb;
	
		}
		else if (index.column() == 0 && index.model()->data(index,Qt::UserRole).toInt() >= 0)
		{
			editor = QItemDelegate::createEditor(parent,option,index);
		}
	}


	return editor;
}

void CCustomDelegate::setEditorData(QWidget * editor, const QModelIndex & index )const
{
	if (index.isValid() && index.column() == 1 && index.model()->data(index,Qt::UserRole).toInt() >=0)
	{
		QComboBox* cb = qobject_cast<QComboBox*>(editor);
		cb->setCurrentIndex(index.model()->data(index,Qt::UserRole).toInt());
		// connect with the signal after we have set the current index
		this->connect(cb,SIGNAL(currentIndexChanged(int)),this,SLOT(CBBoxCurrentIndexChanged(int)));
	}
	else
		QItemDelegate::setEditorData(editor,index);
}

void CCustomDelegate::setModelData ( QWidget * editor, QAbstractItemModel * model, const QModelIndex & index ) const
{
	if (index.isValid() && model && index.column() == 1 && index.model()->data(index,Qt::UserRole).toInt() >=0)
	{
		QComboBox* cb = qobject_cast<QComboBox*>(editor);
		int newIndex = cb->currentIndex();
		model->setData(index,newIndex,Qt::UserRole);
		model->setData(index,this->items[newIndex],Qt::DisplayRole);
		model->setData(index,this->colors[newIndex],Qt::TextColorRole);
	}
	else
		QItemDelegate::setModelData(editor,model,index);
}

void CCustomDelegate::CBBoxCurrentIndexChanged(int index)
{
	QComboBox *cb = qobject_cast<QComboBox*>(sender());

	if (cb)
	{
		this->disconnect(cb,SIGNAL(currentIndexChanged(int)),this,SLOT(CBBoxCurrentIndexChanged(int)));
		emit commitData(cb);
		emit closeEditor(cb);
	}


}