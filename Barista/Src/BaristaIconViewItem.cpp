#include "BaristaIconViewItem.h"

#include "VImage.h"
#include "QTImageHelper.h"

#include <QPainter>
#include <QBrush>
#include <QColor>

CBaristaIconViewItem::CBaristaIconViewItem(QTreeWidgetItem* parent,IconViewType type,CVImage* image) :
	QTreeWidgetItem(parent,(int) type),object((ZObject*)image)
{
	

	QString imageText = image->getItemText().GetChar();
	this->setToolTip(0,imageText);
	

	switch (type)
	{
	case eImage:		{
							QIcon icon;

							if (image->hasValidHugeImage())
							{
								CHugeImage* himage = image->getHugeImage();
								QTImageHelper::createQIcon(&icon, himage, 0, himage->getHeight(), 0, himage->getWidth(), -1, 80);
							}
							else
							{
					
								icon.addPixmap(QTImageHelper::createNoFileFoundPixmap());
							}			

							this->setIcon(0,icon);

							break;
						}

	case eImageCaption: {
							this->setTextAlignment(0,Qt::AlignTop | Qt::AlignLeft);
							this->setText(0,imageText);
							this->setTextColor(0,Qt::blue);
							break;
						}
	default: return;
	}

	

	

}


CBaristaIconViewItem::CBaristaIconViewItem(QTreeWidgetItem* parent,IconViewType type,const ZObject* object,QString caption):
	QTreeWidgetItem(parent,(int) type),object(object)
{
	if (this->object)
		this->setIcon(0, QPixmap::fromImage(QTImageHelper::makeIcon(this->object)));
	this->setText(0, caption );
	this->setToolTip(0,caption);
}



CBaristaIconViewItem::CBaristaIconViewItem(QTreeWidget* parent,IconViewType type,ZObject* object,QString caption):
	QTreeWidgetItem(parent,(int) type),object(object)
{

	if (this->object)
		this->setIcon(0, QPixmap::fromImage(QTImageHelper::makeIcon(this->object)));
	this->setText(0, caption );	
	this->setToolTip(0,caption);
}




CBaristaIconViewItem::~CBaristaIconViewItem(void)
{
}

void CBaristaIconViewItem::setObject(ZObject* object)
{
    this->object = object;
}

const ZObject* CBaristaIconViewItem::getObject()
{
	return this->object;
}
