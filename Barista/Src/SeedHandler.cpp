#include "SeedHandler.h"
#include "Bui_MainWindow.h"

CSeedHandler::CSeedHandler(void)
{
	this->statusBarText = "SEEDMODE ";
}

CSeedHandler::~CSeedHandler(void)
{
}


bool CSeedHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{
	return false;
};


bool CSeedHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	return false;
};

bool CSeedHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	return false;
};


bool CSeedHandler::redraw(CBaristaView *v)
{
	return true;
};

void CSeedHandler::initialize(CBaristaView *v)
{
};

void CSeedHandler::open()
{
	CBaristaViewEventHandler::open();	
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->RegionGrowingAction->setChecked(true);
	mainwindow->blockSignals(false);
};

void CSeedHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->RegionGrowingAction->setChecked(false);
	mainwindow->blockSignals(false);
};
