#include "Bui_MatchingParametersDlg.h"
#include "ObjectSelector.h"

#include "BaristaProject.h"
#include "Icons.h"
#include <QDoubleValidator>

extern CBaristaProject project;

Bui_MatchingParametersDlg::Bui_MatchingParametersDlg(QWidget *parent, CMatchingParameters *pars)
	: QDialog(parent), decimals(3), parsChanged(false)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	if (pars) this->matchingParameter = *pars;
	else this->matchingParameter = project.getMatchingParameter();
	this->initDlg();

	//this->resize(0,0);
	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));
	this->resize(0,0);

	// pushbutton connections
	this->connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));
	this->connect(this->pbDefault,SIGNAL(released()),this,SLOT(pBDefaultReleased()));
	this->connect(this->SelectXYZPointButton,SIGNAL(released()),this,SLOT(bXYZPointsReleased()));

	// slider connections
	this->connect(this->rhoSlider, SIGNAL(valueChanged(int)),this,SLOT(rholSliderChanged(int)));
	this->connect(this->patchsizeSlider, SIGNAL(valueChanged(int)),this,SLOT(patchsizeSliderChanged(int)));
	this->connect(this->iterationSlider, SIGNAL(valueChanged(int)),this,SLOT(iterationSliderChanged(int)));

	// check box connections
	this->connect(this->cBMatchWindow,SIGNAL(released()), this,SLOT(cBMatchWindowReleased()));

	// lineEdits connections
	this->connect(this->le_sigma8bit,SIGNAL(editingFinished ()),this,SLOT(leSigma8BitEditingFinished()));
	this->connect(this->le_sigma16bit,SIGNAL(editingFinished ()),this,SLOT(leSigma16BitEditingFinished()));
	this->connect(this->le_sigmaGeom,SIGNAL(editingFinished ()),this,SLOT(leSigmaGeomEditingFinished()));
	this->connect(this->le_maxZ,SIGNAL(editingFinished ()),this,SLOT(leMaxZEditingFinished()));
	this->connect(this->le_minZ,SIGNAL(editingFinished ()),this,SLOT(leMinZEditingFinished()));
	this->connect(this->XYZPointName,SIGNAL(editingFinished ()),this,SLOT(leBXYZEditingFinished()));


	this->connect(this->useTemplateScaleButton, SIGNAL(clicked(bool)), this, SLOT(rBScaleModeChanged()));
	this->connect(this->useSearchScaleButton,   SIGNAL(clicked(bool)), this, SLOT(rBScaleModeChanged()));

	// validators
	this->le_sigma8bit->setValidator(new QDoubleValidator(this));
	this->le_sigma16bit->setValidator(new QDoubleValidator(this));
	this->le_sigmaGeom->setValidator(new QDoubleValidator(this));
	this->le_maxZ->setValidator(new QDoubleValidator(this));
	this->le_minZ->setValidator(new QDoubleValidator(this));

	this->cbTFW->setEnabled(false);
}

Bui_MatchingParametersDlg::~Bui_MatchingParametersDlg()
{

}



void Bui_MatchingParametersDlg::initDlg()
{
	this->patchsizeSlider->setValue(this->matchingParameter.getPatchSize());
	this->le_patchsize->setText(QString( "%1" ).arg(this->matchingParameter.getPatchSize()));

	this->iterationSlider->setValue(this->matchingParameter.getIterationSteps());
	this->le_iterationsteps->setText(QString( "%1" ).arg(this->matchingParameter.getIterationSteps()));

	int rhopercent = (int)(this->matchingParameter.getRhoMin()*100);
	this->rhoSlider->setValue(rhopercent);
	this->le_minRho->setText(QString( "%1" ).arg(rhopercent));

	this->le_sigma8bit->blockSignals(true);
	this->le_sigma16bit->blockSignals(true);
	this->le_sigmaGeom->blockSignals(true);
	this->le_maxZ->blockSignals(true);
	this->le_minZ->blockSignals(true);

	this->le_sigma8bit->setText(QString( "%1" ).arg(this->matchingParameter.getSigmaGrey8Bit(), 0, 'F', this->decimals));
	this->le_sigma16bit->setText(QString( "%1" ).arg(this->matchingParameter.getSigmaGrey16Bit(), 0, 'F', this->decimals));
	this->le_sigmaGeom->setText(QString( "%1" ).arg(this->matchingParameter.getSigmaGeom(), 0, 'F', this->decimals));

	this->le_maxZ->setText(QString( "%1" ).arg(this->matchingParameter.getMaxZ(), 0, 'F', this->decimals));
	this->le_minZ->setText(QString( "%1" ).arg(this->matchingParameter.getMinZ(), 0, 'F', this->decimals));

	this->le_sigma8bit->blockSignals(false);
	this->le_sigma16bit->blockSignals(false);
	this->le_sigmaGeom->blockSignals(false);
	this->le_maxZ->blockSignals(false);
	this->le_minZ->blockSignals(false);

	if ( this->matchingParameter.getBandMode() == eIDENTICALBANDS )
		this->rbSameNBands->setChecked(true);
	else
		this->rbAllNBands->setChecked(true);

	if ( this->matchingParameter.getBppMode() == eIDENTICALBPPS )
		this->rbSameBpp->setChecked(true);
	else
		this->rbAllBpp->setChecked(true);

	if (this->matchingParameter.getTrafoMode() == eLSMTFW)
		this->cbTFW->setChecked(true);
	else
		this->cbTFW->setChecked(false);

	this->cBMatchWindow->setChecked(this->matchingParameter.getShowMatchWindow());


	this->XYZPointName->blockSignals(true);
	CCharString s = this->matchingParameter.getXYZFileName();
	if (s.IsEmpty()) 
	{
		s = "MATCHED_POINTS";
		this->matchingParameter.setXYZFileName(s);
	}

	this->XYZPointName->setText(s.GetChar());
	this->XYZPointName->blockSignals(false);


	this->useTemplateScaleButton->blockSignals(true);
	this->useSearchScaleButton->blockSignals(true);

	if (this->matchingParameter.getScaleMode() == eLSMUSESEARCH)
	{
		this->useTemplateScaleButton->setChecked(false);
		this->useSearchScaleButton->setChecked(true);
	}
	else
	{
		this->useTemplateScaleButton->setChecked(true);
		this->useSearchScaleButton->setChecked(false);
	}

	this->useTemplateScaleButton->blockSignals(false);
	this->useSearchScaleButton->blockSignals(false);
}


void Bui_MatchingParametersDlg::rBScaleModeChanged()
{
	this->useTemplateScaleButton->blockSignals(true);
	this->useSearchScaleButton->blockSignals(true);

	if (this->matchingParameter.getScaleMode() == eLSMUSESEARCH)
	{
		this->matchingParameter.setScaleMode(eLSMUSETEMP);
		this->useTemplateScaleButton->setChecked(true);
		this->useSearchScaleButton->setChecked(false);
	}
	else
	{
		this->matchingParameter.setScaleMode(eLSMUSESEARCH);
		this->useTemplateScaleButton->setChecked(false);
		this->useSearchScaleButton->setChecked(true);
	}

	this->useTemplateScaleButton->blockSignals(true);
	this->useSearchScaleButton->blockSignals(true);
};

void Bui_MatchingParametersDlg::pBCancelReleased()
{
	this->parsChanged = false;
	this->close();
}

void Bui_MatchingParametersDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/MenuSettings.html");
}

void Bui_MatchingParametersDlg::pBOkReleased()
{
	this->matchingParameter.setPatchSize(this->patchsizeSlider->value());
	this->matchingParameter.setIterationSteps(this->iterationSlider->value());

	double rho = (double)(this->rhoSlider->value())/100.0;
	this->matchingParameter.setRhoMin(rho);
	this->matchingParameter.setMinZ(this->le_minZ->text().toDouble());
	this->matchingParameter.setMaxZ(this->le_maxZ->text().toDouble());
	this->matchingParameter.setSigmaGrey8Bit(this->le_sigma8bit->text().toDouble());
	this->matchingParameter.setSigmaGrey16Bit(this->le_sigma16bit->text().toDouble());
	this->matchingParameter.setSigmaGeom(this->le_sigmaGeom->text().toDouble());
	this->matchingParameter.setShowMatchWindow(this->cBMatchWindow->isChecked());

	if ( this->rbSameNBands->isChecked() )
		this->matchingParameter.setBandMode(eIDENTICALBANDS);
	else
		this->matchingParameter.setBandMode(eNONIDENTICALBANDS);

	if ( this->rbAllBpp->isChecked() )
		this->matchingParameter.setBppMode(eALLBPPS);
	else
		this->matchingParameter.setBppMode(eIDENTICALBPPS);


	if ( this->cbTFW->isChecked() )
		this->matchingParameter.setTrafoMode(eLSMTFW);
	else
		this->matchingParameter.setTrafoMode(eLSMNOTFW);


	project.setMatchingParameter(this->matchingParameter);

	this->parsChanged = true;

	this->close();
}

void Bui_MatchingParametersDlg::pBDefaultReleased()
{
	this->resetToDefault();
	this->initDlg();
}


void Bui_MatchingParametersDlg::resetToDefault()
{
	double minZ = project.getMeanHeight() - 500.0;
	double maxZ = project.getMeanHeight() + 500.0;

	this->matchingParameter.setMinZ(minZ);
	this->matchingParameter.setMaxZ(maxZ);

	this->matchingParameter.reset();

	this->initDlg();

}

void Bui_MatchingParametersDlg::rholSliderChanged(int rhoint )
{
	this->matchingParameter.setRhoMin((double)(rhoint)/100.0);
	this->initDlg();
}

void Bui_MatchingParametersDlg::patchsizeSliderChanged(int size )
{
	this->matchingParameter.setPatchSize(size);
	this->initDlg();
}

void Bui_MatchingParametersDlg::iterationSliderChanged(int steps )
{
	this->matchingParameter.setIterationSteps(steps);
	this->initDlg();
}

void Bui_MatchingParametersDlg::leSigma8BitEditingFinished()
{
	if ( this->le_sigma8bit->hasAcceptableInput() )
	{
		double dval = this->le_sigma8bit->text().toDouble();

		if ( dval > 0.0 )
		{
			this->matchingParameter.setSigmaGrey8Bit(this->le_sigma8bit->text().toDouble());
		}
	}
	this->initDlg();
}


void Bui_MatchingParametersDlg::leSigma16BitEditingFinished()
{
	if ( this->le_sigma16bit->hasAcceptableInput() )
	{
		double dval = this->le_sigma16bit->text().toDouble();

		if ( dval > 0.0 )
		{
			this->matchingParameter.setSigmaGrey16Bit(this->le_sigma16bit->text().toDouble());
		}
	}

	this->initDlg();
}

void Bui_MatchingParametersDlg::leSigmaGeomEditingFinished()
{
	if ( this->le_sigmaGeom->hasAcceptableInput() )
	{
		double dval = this->le_sigmaGeom->text().toDouble();

		if ( dval > 0.0 )
		{
			this->matchingParameter.setSigmaGeom(this->le_sigmaGeom->text().toDouble());
		}
	}
	this->initDlg();
}

void Bui_MatchingParametersDlg::leMaxZEditingFinished()
{
	if ( this->le_maxZ->hasAcceptableInput() )
	{
		this->matchingParameter.setMaxZ(this->le_maxZ->text().toDouble());
	}

	this->initDlg();
}

void Bui_MatchingParametersDlg::leMinZEditingFinished()
{
	if ( this->le_minZ->hasAcceptableInput() )
	{
		this->matchingParameter.setMinZ(this->le_minZ->text().toDouble());
	}

	this->initDlg();
}
	
void Bui_MatchingParametersDlg::bXYZPointsReleased()
{
	CObjectSelector selector;
    selector.makeXYZPointSelector();
    ZObject *object = selector.getSelection();
    CXYZPointArray *pXYZPts = (CXYZPointArray*)object;

    if(pXYZPts)
	{
		this->matchingParameter.setXYZFileName(pXYZPts->getItemText());
		this->XYZPointName->blockSignals(true);
		this->XYZPointName->setText(this->matchingParameter.getXYZFileName().GetChar());
		this->XYZPointName->blockSignals(false);
	}
};

void Bui_MatchingParametersDlg::leBXYZEditingFinished()
{
	QString qstr = this->XYZPointName->text();

    CCharString s (qstr.toLatin1().constData());
	if (s.IsEmpty()) 
	{
		s = "MATCHED_POINTS";
	}

	this->matchingParameter.setXYZFileName(s);

	this->XYZPointName->blockSignals(true);
	this->XYZPointName->setText(this->matchingParameter.getXYZFileName().GetChar());
	this->XYZPointName->blockSignals(false);
};

/*
void Bui_MatchingParametersDlg::rbAllNBandsclicked()
{
	//this->matchingParameter.setSameNBands(false);

}

void Bui_MatchingParametersDlg::rbSameNBandsclicked()
{
	//this->matchingParameter.setSameNBands(true);
}

void Bui_MatchingParametersDlg::cbTFWReleased()
{
	this->matchingParameter.setUseTFW(this->cbTFW->isChecked());
}
*/
void Bui_MatchingParametersDlg::cBMatchWindowReleased()
{
	this->matchingParameter.setShowMatchWindow(this->cBMatchWindow->isChecked());
}
