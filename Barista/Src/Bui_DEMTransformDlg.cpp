#include "Bui_DEMTransformDlg.h"

#include <QMessageBox>

#include "Icons.h"
#include "BaristaProject.h"
#include "BaristaHtmlHelp.h"
#include "GDALFile.h"
#include "Bui_GDALExportDlg.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include "TFW.h"
#include "BaristaProjectHelper.h"

Bui_DEMTransformDlg::Bui_DEMTransformDlg(CVDEM* vdem) : trafoname(""), success(false),srcdem(vdem),trafodem(NULL),dlg(NULL)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->decimals = 3;

	if ( srcdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC )
	{
		this->decimals = 10;
		this->rbGeographicButton->setChecked(true);
		this->geographicselected = true;
		this->utmselected = false;
	}
	else if ( srcdem->getReferenceSystem().isUTM())
	{
		this->rbUTMButton->setChecked(true);
		this->geographicselected =false;
		this->utmselected = true;
	}
		
	
	this->reset();

	this->init();

	// pushbutton connections
	this->connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));
	this->connect(this->pbTrafoDEM,SIGNAL(released()),this,SLOT(pBSelectOutputReleased()));

	// radio button connections
	this->connect(this->rbUTMButton,SIGNAL(released()),this,SLOT(rbUTMButtonclicked()));
	this->connect(this->rbGeographicButton,SIGNAL(released()),this,SLOT(rbGeographicButtonclicked()));

	// check box connections
	this->connect(this->cb1arc,SIGNAL(released()), this,SLOT(cb1arcReleased()));
	this->connect(this->cb3arc,SIGNAL(released()), this,SLOT(cb3arcReleased()));
	this->connect(this->cb30arc,SIGNAL(released()), this,SLOT(cb30arcReleased()));

	// spin box connections
	this->connect(this->spinBoxCols, SIGNAL(valueChanged(int)), this, SLOT(spinBoxColsChanged(int)));
	this->connect(this->spinBoxRows, SIGNAL(valueChanged(int)), this, SLOT(spinBoxRowsChanged(int)));

	// lineEdits connections
	this->connect(this->leLLX,SIGNAL(editingFinished ()),this,SLOT(leLLXEditingFinished()));
	this->connect(this->leLLY,SIGNAL(editingFinished ()),this,SLOT(leLLYEditingFinished()));
	this->connect(this->leGridX,SIGNAL(editingFinished ()),this,SLOT(leGridXEditingFinished()));
	this->connect(this->leGridY,SIGNAL(editingFinished ()),this,SLOT(leGridYEditingFinished()));

	// validators
	this->leLLX->setValidator(new QDoubleValidator(this));
	this->leLLY->setValidator(new QDoubleValidator(this));
	this->leGridX->setValidator(new QDoubleValidator(this));
	this->leGridY->setValidator(new QDoubleValidator(this));

}

Bui_DEMTransformDlg::~Bui_DEMTransformDlg()
{
	if (this->dlg)
		delete this->dlg;

	this->dlg=NULL;

}

void Bui_DEMTransformDlg::init()
{
	QString qstr;

	this->spinBoxCols->blockSignals(true);
	this->spinBoxRows->blockSignals(true);
	this->leLLX->blockSignals(true);
	this->leLLY->blockSignals(true);
	this->leGridX->blockSignals(true);
	this->leGridY->blockSignals(true);


	this->spinBoxCols->setValue(this->cols);
	this->spinBoxRows->setValue(this->rows);

	this->leLLX->setText(QString( "%1" ).arg(this->xmin, 0, 'F', this->decimals));
	this->leLLY->setText(QString( "%1" ).arg(this->ymin, 0, 'F', this->decimals));
	this->leGridX->setText(QString( "%1" ).arg(this->gridx, 0, 'F', this->decimals));
	this->leGridY->setText(QString( "%1" ).arg(this->gridy, 0, 'F', this->decimals));

	this->leLLX->blockSignals(false);
	this->leLLY->blockSignals(false);
	this->leGridX->blockSignals(false);
	this->leGridY->blockSignals(false);
	this->spinBoxCols->blockSignals(false);
	this->spinBoxRows->blockSignals(false);

	if ( this->geographicselected )
	{
		this->cb1arc->setEnabled(true);
		this->cb3arc->setEnabled(true);
		this->cb30arc->setEnabled(true);
	}
	else
	{
		this->cb1arc->setEnabled(false);
		this->cb3arc->setEnabled(false);
		this->cb30arc->setEnabled(false);
	}

	this->checkSetup();

}


void Bui_DEMTransformDlg::reset()
{
	this->gridx = this->srcdem->getDEM()->getgridX();
	this->gridy = this->srcdem->getDEM()->getgridY();

	double zmin = this->srcdem->getDEM()->getminZ();

	
	if ( srcdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC )
	{
		this->ymin = this->srcdem->getDEM()->getminX();
		this->ymax = this->srcdem->getDEM()->getmaxX();
		this->xmin = this->srcdem->getDEM()->getminY();
		this->xmax = this->srcdem->getDEM()->getmaxY();
	}
	else if ( srcdem->getReferenceSystem().getCoordinateType() == eGRID )
	{
		this->xmin = this->srcdem->getDEM()->getminX();
		this->xmax = this->srcdem->getDEM()->getmaxX();
		this->ymin = this->srcdem->getDEM()->getminY();
		this->ymax = this->srcdem->getDEM()->getmaxY();
	}

	this->ULin.x = this->srcdem->getDEM()->getminX();
	this->ULin.y = this->srcdem->getDEM()->getmaxY();
	this->ULin.z = zmin;

	this->URin.x = this->srcdem->getDEM()->getmaxX();
	this->URin.y = this->srcdem->getDEM()->getmaxY();
	this->URin.z = zmin;
	
	this->LLin.x = this->srcdem->getDEM()->getminX();
	this->LLin.y = this->srcdem->getDEM()->getminY();
	this->LLin.z = zmin;

	this->LRin.x = this->srcdem->getDEM()->getmaxX();
	this->LRin.y = this->srcdem->getDEM()->getminY();
	this->LRin.z = zmin;

	this->cols = this->srcdem->getDEM()->getWidth();
	this->rows = this->srcdem->getDEM()->getHeight();

	this->cb1arc->setChecked(false);
	this->cb3arc->setChecked(false);
	this->cb30arc->setChecked(false);

}


void Bui_DEMTransformDlg::pBOkReleased()
{
	if (this->trafoname.IsEmpty())
		return;

	if ( !this->srcdem ) return;

	if ( !this->trafodem ) return;

	this->trafodem->getDEM()->width = this->cols;
	this->trafodem->getDEM()->height = this->rows;
	
	/*
	CTFW tfw;
	CReferenceSystem refsys;
	if ( this->geographicselected )
	{ 
		C2DPoint shift(this->ymax, this->xmin);
		C2DPoint gridding(this->gridx,  this->gridy);

		refsys.setUTMZone(this->ymax, this->xmin);
		refsys.setCoordinateType(eGEOGRAPHIC);

		tfw.setRefSys(refsys);
		tfw.setFromShiftAndPixelSize(shift, gridding);			
	}
	else if ( this->utmselected )
	{	
		C2DPoint shift(this->xmin,  this->ymax);
		C2DPoint gridding(this->gridx,  this->gridy);
		refsys.setCoordinateType(eGRID);
		refsys.setUTMZone(this->ULin.x, this->ULin.y);

		tfw.setRefSys(refsys);
		tfw.setFromShiftAndPixelSize(shift, gridding);
	}	
	}
	else
		return;

	tfw.setFileName("TrafoDEM.tfw");

	this->trafodem->setTFW(tfw);

	*/

	//addition >>
	if ( this->geographicselected )
	{ //transformed from utm to geographic or geographic to geographic
		CTFW tfw;
		CReferenceSystem refsys;
		C2DPoint shift(this->ymax, this->xmin);
		C2DPoint gridding(this->gridx,  this->gridy);

		refsys.setUTMZone(this->ymax, this->xmin);
		refsys.setCoordinateType(eGEOGRAPHIC);

		tfw.setRefSys(refsys);
		tfw.setFromShiftAndPixelSize(shift, gridding);	
		tfw.setFileName("TrafoDEM.tfw");
		this->trafodem->setTFW(tfw);
	}
	else if ( this->utmselected )
	{	
		if (this->srcdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
		{ // transform from geographic to utm
			CTFW tfw;
			CReferenceSystem refsys;
			C2DPoint shift(this->xmin,  this->ymax);
			C2DPoint gridding(this->gridx,  this->gridy);
			refsys.setCoordinateType(eGRID);

			refsys.setUTMZone(this->ULin.x, this->ULin.y);

			tfw.setRefSys(refsys);
			tfw.setFromShiftAndPixelSize(shift, gridding);
			tfw.setFileName("TrafoDEM.tfw");
			this->trafodem->setTFW(tfw);
		}
		else if (this->srcdem->getReferenceSystem().getCoordinateType() == eGRID)
		{	// transform from utm to utm
			Matrix parms;
			parms.ReSize(6,1);

			parms.element(0,0) = this->gridx;
			parms.element(1,0) = 0.0;
			parms.element(2,0) = this->xmin;
			parms.element(3,0) = 0.0;
			parms.element(4,0) = -1.0*this->gridy;
			parms.element(5,0) = this->ymax;


			CTFW tfw (*this->trafodem->getTFW());
			tfw.setAll(parms);
			tfw.setRefSys(this->srcdem->getReferenceSystem());
			tfw.setFileName("TrafoDEM.tfw");
			this->trafodem->setTFW(tfw);
		}
		else 
			return;
	}
	else
		return;
	
	//<< addition


	this->trafodem->getDEM()->setFileName( this->trafoname.GetChar());

	bui_ProgressDlg progressdlg(new CDEMTransformThread(this->trafodem->getDEM(),this->srcdem->getDEM()),this->trafodem->getDEM(),"Transforming DEM ...");
	
	progressdlg.exec();
	this->success = progressdlg.getSuccess();


	if ( this->success && this->dlg)
	{
		if ( this->dlg->writeSettings() )
			this->dlg->writeImage("Writing DEM as image file");

		if (!this->dlg->getSuccess())
		{
			this->success = false;
		}
	}

	this->close();
}

void Bui_DEMTransformDlg::pBCancelReleased()
{
	this->setSuccess(false);
	this->close();
}

void Bui_DEMTransformDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/TransformDEM.html");
}

void Bui_DEMTransformDlg::pBSelectOutputReleased()
{

	if (!this->dlg)
		delete this->dlg;
	this->dlg =  NULL;

	this->trafoname = "";
	this->transformedDEMFile->setText(this->trafoname.GetFullFileName().GetChar());

	if (this->srcdem)
	{
		if (this->trafodem)
			project.deleteDEM(this->trafodem);

		this->trafodem = project.addDEM(this->trafoname);
		this->trafodem->getDEM()->bands = this->srcdem->getDEM()->getBands();
		this->trafodem->getDEM()->bpp = this->srcdem->getDEM()->getBpp();
		this->trafodem->getDEM()->setTFW(*this->srcdem->getDEM()->getTFW());
		this->trafodem->getDEM()->width = this->cols;
		this->trafodem->getDEM()->height = this->rows;

		this->dlg = new Bui_GDALExportDlg(this->trafoname,this->trafodem,true,"Transform DEM");
	}
	else 
		return;

	dlg->exec();

	if (!this->dlg->getSuccess())	// false if user has pressed "Cancel"
	{
		if (this->trafodem)
			project.deleteDEM(this->trafodem);

		this->trafodem = NULL;
		this->trafoname = "";
	}
	else
	{
		this->trafoname = this->dlg->getFilename();
		this->transformedDEMFile->setText(this->trafoname.GetFullFileName().GetChar());
		this->trafodem->setFileName(this->trafoname.GetChar());
	}

	this->checkSetup();

}



void Bui_DEMTransformDlg::rbGeographicButtonclicked()
{
	this->utmselected = false;
	this->geographicselected = true;

	this->decimals = 10;

	//double zmin = this->srcdem->getDEM()->getminZ();

    // from GEOCENTRIC to GEOGRAPHIC
	if (this->srcdem->getReferenceSystem().getCoordinateType() == eGEOCENTRIC)
    {
		QMessageBox::warning( this, "Source DEM is defined in geocentric coordinates!"
			, "DEM Transformation to Geographic is not implemented.");
		return;
    }
	else if (this->srcdem->getReferenceSystem().getCoordinateType() == eGRID)
    {       
		if (!this->srcdem->getReferenceSystem().isUTM())
		{
			QMessageBox::warning( this, "Source DEM is defined in TM coordinates!"
				, "DEM Transformation to Geographic is not implemented.");
		}
		else
		{
			this->srcdem->getReferenceSystem().gridToGeographic(ULout, ULin);
			this->srcdem->getReferenceSystem().gridToGeographic(URout, URin);
			this->srcdem->getReferenceSystem().gridToGeographic(LLout, LLin);
			this->srcdem->getReferenceSystem().gridToGeographic(LRout, LRin);

			this->getExtensions();
		}
    }
	else if (this->srcdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
    {
		this->reset();
	}
	
	this->init();
}



void Bui_DEMTransformDlg::rbUTMButtonclicked()
{
	this->utmselected = true;
	this->geographicselected = false;

	this->decimals = 3;

	//C3DPoint inpoint(this->xmin, this->ymin, 0);
    //C3DPoint outpoint(inpoint);

    // from GEOCENTRIC to UTM
    if (this->srcdem->getReferenceSystem().getCoordinateType() == eGEOCENTRIC)
    {
		QMessageBox::warning( this, "Source DEM is defined in geocentric coordinates!"
			, "Transformation not implemented.");
		return;
    }
    else if(this->srcdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
    {
		this->srcdem->getReferenceSystem().geographicToGrid(ULout, ULin);
		this->srcdem->getReferenceSystem().geographicToGrid(URout, URin);
		this->srcdem->getReferenceSystem().geographicToGrid(LLout, LLin);
		this->srcdem->getReferenceSystem().geographicToGrid(LRout, LRin);

		this->getExtensions();
    }
	else if (this->srcdem->getReferenceSystem().isUTM())
    {
		this->reset();
	}

	this->init();
}

bool Bui_DEMTransformDlg::createTransformedDEM()
{
	this->success = this->transform();
	return this->getSuccess();
}

void Bui_DEMTransformDlg::checkSetup()
{
	bool readyToGo = true;
	this->pBOk->setEnabled(false);

	// check filename is set
	if ( this->trafoname.IsEmpty() )
		readyToGo = false;

	// check gridx, gridy
	if ( this->gridx <= 0.0 || this->gridy <= 0.0 )
		readyToGo = false;

	// check cols, rows
	if ( this->cols <= 0.0 || this->rows <= 0.0 )
		readyToGo = false;

	if ( readyToGo ) this->pBOk->setEnabled(true);
}


bool Bui_DEMTransformDlg::transform()
{
	
	this->trafodem->getDEM()->setFileName(this->trafoname.GetChar());

	
	bui_ProgressDlg progressdlg(new CDEMTransformThread(this->trafodem->getDEM(),this->srcdem->getDEM()),this->trafodem->getDEM(),"Tansforming DEMs ...");
	
	progressdlg.exec();

	if (!progressdlg.getSuccess())
	{
		this->success = false;
	}

	if ( this->success && this->dlg)
	{
		if ( this->dlg->writeSettings() )
			this->dlg->writeImage("Writing DEM as image file");

		if (!this->dlg->getSuccess())
		{
			this->success = false;
		}
	}
	
	baristaProjectHelper.refreshTree();

	

	return this->success;
}

void Bui_DEMTransformDlg::getExtensions()
{

	double newxmin = ULout.x;
	double newymin = ULout.y;

	double newxmax = ULout.x;
	double newymax = ULout.y;


	// determine xmin
	if ( URout.x < newxmin ) newxmin = URout.x;
	if ( LLout.x < newxmin ) newxmin = LLout.x;
	if ( LRout.x < newxmin ) newxmin = LRout.x;

	// determine xmax
	if ( URout.x > newxmax ) newxmax = URout.x;
	if ( LLout.x > newxmax ) newxmax = LLout.x;
	if ( LRout.x > newxmax ) newxmax = LRout.x;

	// determine ymin
	if ( URout.y < newymin ) newymin = URout.y;
	if ( LLout.y < newymin ) newymin = LLout.y;
	if ( LRout.y < newymin ) newymin = LRout.y;

	// determine ymax
	if ( URout.y > newymax ) newymax = URout.y;
	if ( LLout.y > newymax ) newymax = LLout.y;
	if ( LRout.y > newymax ) newymax = LRout.y;



	if (this->srcdem->getReferenceSystem().getCoordinateType() == eGRID)
	{
		this->ymin = newxmin;
		this->xmin = newymin;

		this->ymax = newxmax;
		this->xmax = newymax;

		this->gridx = (newxmax - newxmin)/this->rows;
		this->gridy = (newymax - newymin)/this->cols;
	}
	else if (this->srcdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
	{

		this->xmin = newxmin;
		this->ymin = newymin;

		this->xmax = newxmax;
		this->ymax = newymax;

		this->gridx = (newxmax - newxmin)/this->cols;
		this->gridy = (newymax - newymin)/this->rows;
	}
	else
	{

	}

}


void Bui_DEMTransformDlg::cb1arcReleased()
{
	double grid = 1.0/3600.0;

	this->decimals = 10;

	this->gridx = grid;
	this->gridy = grid;

	this->getColsRows();

	this->cb1arc->setChecked(true);
	this->cb3arc->setChecked(false);
	this->cb30arc->setChecked(false);

	this->init();
}

void Bui_DEMTransformDlg::cb3arcReleased()
{
	double grid = 3.0/3600.0;

	this->decimals = 10;

	this->gridx = grid;
	this->gridy = grid;

	this->getColsRows();

	this->cb1arc->setChecked(false);
	this->cb3arc->setChecked(true);
	this->cb30arc->setChecked(false);

	this->init();
}

void Bui_DEMTransformDlg::cb30arcReleased()
{
	double grid = 30.0/3600.0;

	this->decimals = 10;

	this->gridx = grid;
	this->gridy = grid;

	this->getColsRows();

	this->cb1arc->setChecked(false);
	this->cb3arc->setChecked(false);
	this->cb30arc->setChecked(true);

	this->init();
}

void Bui_DEMTransformDlg::getColsRows()
{
	this->cols =(this->xmax - this->xmin)/this->gridx;
	this->rows =(this->ymax - this->ymin)/this->gridy;

	int stop =1;
}

void Bui_DEMTransformDlg::spinBoxColsChanged(int newcols)
{
	this->cols = newcols;
	this->getGridxGridy();

	this->init();
}

void Bui_DEMTransformDlg::spinBoxRowsChanged(int newrows)
{
	this->rows = newrows;
	this->getGridxGridy();

	this->init();
}

void Bui_DEMTransformDlg::getGridxGridy()
{
	this->gridx = (this->xmax - this->xmin)/this->cols;
	this->gridy = (this->ymax - this->ymin)/this->rows;
}

void Bui_DEMTransformDlg::leLLXEditingFinished()
{
	if ( this->leLLX->hasAcceptableInput() )
		this->xmin = this->leLLX->text().toDouble();
}

void Bui_DEMTransformDlg::leLLYEditingFinished()
{
	if ( this->leLLY->hasAcceptableInput() )
		this->ymin = this->leLLY->text().toDouble();
}

void Bui_DEMTransformDlg::leGridXEditingFinished()
{
	if ( this->leGridX->hasAcceptableInput() )
	{
		double temp = this->leGridX->text().toDouble();

		if ( temp > 0.0 )
		{
			this->gridx =temp;
			this->getColsRows();

			this->cb1arc->setChecked(false);
			this->cb3arc->setChecked(false);
			this->cb30arc->setChecked(false);
		}
	}
	
	this->init();
}

void Bui_DEMTransformDlg::leGridYEditingFinished()
{
	if ( this->leGridY->hasAcceptableInput() )
	{
		double temp = this->leGridY->text().toDouble();

		if ( temp > 0.0 )
		{
			this->gridy =temp;
			this->getColsRows();

			this->cb1arc->setChecked(false);
			this->cb3arc->setChecked(false);
			this->cb30arc->setChecked(false);
		}
	}
	this->init();
}




