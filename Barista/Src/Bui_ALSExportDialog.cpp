#include "Bui_ALSExportDialog.h"

#include <qfiledialog.h> 
#include <QMessageBox> 

#include "IniFile.h"
#include "ObjectSelector.h"
#include "ProgressThread.h"
#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "BaristaProjectHelper.h"

#include "VALS.h"


bui_ALSExportDialog::bui_ALSExportDialog(CVALS *als) : 
		ALS(als), pulse(0), success(false), exportTime(false), exportIntensity(false)
{
	setupUi(this);
	
	connect(this->XYZFormatButton,  SIGNAL(clicked(bool)), this, SLOT(FormatSelected()));
	connect(this->XYZIButton,       SIGNAL(clicked(bool)), this, SLOT(FormatSelected()));
	connect(this->TXYZIButton,      SIGNAL(clicked(bool)), this, SLOT(FormatSelected()));
	connect(this->FirstPulseButton, SIGNAL(clicked(bool)), this, SLOT(PulseSelected()));
	connect(this->LastPulseButton,  SIGNAL(clicked(bool)), this, SLOT(PulseSelected()));
	connect(this->OutputFile,       SIGNAL(released()),    this, SLOT(OutputFileSelected()));
	connect(this->buttonOk,         SIGNAL(released()),    this, SLOT(OK()));
	connect(this->buttonCancel,     SIGNAL(released()),    this, SLOT(Cancel()));
	connect(this->buttonHelp,       SIGNAL(released()),    this, SLOT(Help()));
   
	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}

bui_ALSExportDialog::~bui_ALSExportDialog(void)
{
	this->ALS = 0;
}

void bui_ALSExportDialog::init()
{
	this->setPulseMode();
	this->setFormat();

	this->filename = this->ALS->getFileName();
	this->filename += ".xyz";
	
	this->OutputFileName->setText(QString(this->filename.GetChar()));

	this->buttonOk->setEnabled(true);
}


void bui_ALSExportDialog::destroy()
{
}

void bui_ALSExportDialog::PulseSelected()
{
	this->FirstPulseButton->blockSignals(true);
	this->LastPulseButton->blockSignals(true);

	if (this->pulse == 0) 
	{
		this->FirstPulseButton->setChecked(false);
		this->LastPulseButton->setChecked(true);
		this->pulse = 1;
	}
	else
	{
		this->FirstPulseButton->setChecked(true);
		this->LastPulseButton->setChecked(false);
		this->pulse = 0;
	}

	this->FirstPulseButton->blockSignals(false);
	this->LastPulseButton->blockSignals(false);
};

void bui_ALSExportDialog::FormatSelected()
{
	if (this->exportTime) 
	{
		this->exportTime = false;
		this->exportIntensity = this->XYZIButton->isChecked();
	}
	else if (this->exportIntensity)
	{
		this->exportIntensity = false;
		this->exportTime = this->TXYZIButton->isChecked();
	}
	else
	{
		this->exportIntensity = this->XYZIButton->isChecked();
		this->exportTime = this->TXYZIButton->isChecked();
	}

	this->setFormat();
}

void bui_ALSExportDialog::OutputFileSelected()
{
	QString fn = QFileDialog::getSaveFileName(this, "Choose File",
											  inifile.getCurrDir(),	
											  "Fileformats (*.xyz)",	
											  NULL);

	CFileName testname((const char*)fn.toLatin1());

	if (!testname.IsEmpty())
	{
		this->filename = testname;
		this->OutputFileName->setText(QString(this->filename.GetChar()));
		this->buttonOk->setEnabled(true);
	}
}
	  
void bui_ALSExportDialog::setFormat()
{
	if (this->ALS->getCALSData()->getHasTime())
	{
		this->TXYZIButton->setEnabled(true);
	}
	else
	{
		this->TXYZIButton->setEnabled(false);
		this->exportTime = false;
	}

	if (this->ALS->getCALSData()->getHasIntensity())
	{
		this->TXYZIButton->setEnabled(true);
		this->XYZIButton->setEnabled(true);
	}
	else
	{
		this->TXYZIButton->setEnabled(false);
		this->XYZIButton->setEnabled(false);
		this->exportTime = false;
		this->exportIntensity = false;
	}
	
	this->TXYZIButton->blockSignals(true);
	this->XYZIButton->blockSignals(true);
	this->XYZFormatButton->blockSignals(true);

	if (this->exportTime) 
	{
		this->TXYZIButton->setChecked(true);
		this->XYZIButton->setChecked(false);
		this->XYZFormatButton->setChecked(false);
	}
	else if (this->exportIntensity)
	{
		this->TXYZIButton->setChecked(false);
		this->XYZIButton->setChecked(true);
		this->XYZFormatButton->setChecked(false);
	}
	else 
	{
		this->TXYZIButton->setChecked(false);
		this->XYZIButton->setChecked(false);
		this->XYZFormatButton->setChecked(true);
	}

	this->TXYZIButton->blockSignals(false);
	this->XYZIButton->blockSignals(false);
	this->XYZFormatButton->blockSignals(false);
};

void bui_ALSExportDialog::setPulseMode()
{
	this->FirstPulseButton->blockSignals(true);
	this->LastPulseButton->blockSignals(true);

	if (this->ALS->getCALSData()->getPulsMode() == eALSFirst)
	{
		this->pulse = 0;
		this->LastPulseButton->setEnabled(false);
		this->FirstPulseButton->setEnabled(true);
	}
	else if (this->ALS->getCALSData()->getPulsMode() == eALSLast)
	{
		this->pulse = 1;
		this->FirstPulseButton->setEnabled(false);
		this->LastPulseButton->setEnabled(true);
	}
	else if (eALSFirstLast)
	{
		this->FirstPulseButton->setEnabled(true);
		this->LastPulseButton->setEnabled(true);
	}

	if (this->pulse == 0) 
	{
		this->FirstPulseButton->setChecked(true);
		this->LastPulseButton->setChecked(false);
	}
	else
	{
		this->FirstPulseButton->setChecked(false);
		this->LastPulseButton->setChecked(true);
	}

	this->FirstPulseButton->blockSignals(false);
	this->LastPulseButton->blockSignals(false);
};

void bui_ALSExportDialog::OK()
{
	this->success = true;
	baristaProjectHelper.setIsBusy(true);
	this->ALS->exportASCII(this->filename.GetChar(), this->pulse, this->exportTime, this->exportIntensity);
	baristaProjectHelper.setIsBusy(false);
	this->destroy();
	this->close();
}

void bui_ALSExportDialog::Cancel()
{
	this->destroy();
	this->close();
}

void bui_ALSExportDialog::Help()
{
	CHtmlHelp::callHelp("ALSEXPORT"); 
};
