#include "Bui_ProgressDlg.h"

#include "BaristaProject.h"
#include "Icons.h"
#include "ProgressThread.h"
#include "ProgressListener.h"
#include "ProgressHandler.h"
#include <QDialog>
#include "StructuredFile.h"
#include "BaristaProjectHelper.h"


bui_ProgressDlg::bui_ProgressDlg(CProgressThread* thread, CProgressHandler* handler, const char* title) 
	: QDialog(NULL,Qt::SubWindow), listener(0), thread(thread), handler(handler), timer(0), success(false), firstPaint(true), finished(false)
{
	setupUi(this);

	this->setWindowTitle(title);
	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG"); // reads Barista Icon
	this->setWindowIcon(QIcon(icon)); // sets the icon on the progress dialog box
    this->progressBar->setRange(0, 100); // range of the progress 
    this->progressBar->setValue(0); // sets intitial value

	this->timer = new QTimer(this); // provides repetitive and single-shot timers. 
    this->listener = new CProgressListener();
	this->listener->setTitleString(title);

	connect( this->timer, SIGNAL(timeout()), this, SLOT(slotTimeout()) ); // if times out for a single-shot the slot is called
}


void bui_ProgressDlg::setTitleString(const char* title)
{
	this->setWindowTitle(title);
}



bui_ProgressDlg::~bui_ProgressDlg(void)
{
    if (this->thread)
        delete this->thread;
    
    this->thread = NULL;

    delete this->listener;
	this->listener = NULL;

    delete this->timer;
	this->timer = NULL;


}


void bui_ProgressDlg::paintEvent ( QPaintEvent * e) 
{
    if (this->firstPaint)
    {
        this->firstPaint = false;

		if (this->handler != NULL)
		{
			baristaProjectHelper.setIsBusy(true);
			this->handler->setProgressListener(this->listener);
			if (this->thread != NULL)
				this->thread->start(); // automatically calls run() function in the thread (CRunBundleThread:: run() ProgressThread.cpp file in the case of Adjustments)
			this->timer->start(500);
		}
		return;
    }
    
    QDialog::paintEvent (e);
}


void bui_ProgressDlg::slotTimeout()
{
    if (this->finished)
    {
        this->timer->stop();
        this->done(0);
		baristaProjectHelper.setIsBusy(false);
        return;
    }

    double val = this->listener->getProgress();
	int progress;
	
	if (val < 0.0) // workaround for reading the project
	{
		int percentComplete = 0;
		int restoreComplete = 0;
		int readComplete = 0;

		if( (CStructuredFile::byteCount != 0) && (CStructuredFile::sectionCount != 0) )
		{
			readComplete = 100*CStructuredFile::currentByte / CStructuredFile::byteCount;
			restoreComplete = 100*CSerializable::restoreCount / CStructuredFile::sectionCount;
		}

		progress = (readComplete + restoreComplete)/2;
	}
	else
		progress = (int)(val + 0.5);

    this->progressBar->setValue(progress);
	this->setWindowTitle(this->listener->getTitleString());

	if (this->thread != NULL && this->thread->isFinished())
	{
		this->success = this->thread->getSuccess();	
		this->finished = true;
		this->handler->setProgressListener(NULL);
	}

}
