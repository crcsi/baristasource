
#include "3DView.h"


#include "BaristaProject.h"

#include <QGridLayout>
#include "OpenGLObject.h"
#include <QAction>
#include <QPushButton>

#include "ObjectSelector.h"


C3DView::C3DView(QWidget *parent) :  CBaristaView("Barista 3D View",NULL,0,0,0,0,0,0,parent), openGL(0)
{

	QGridLayout* gridLayout = new QGridLayout(this);
	
	this->pbContextMenuButton = new QPushButton(tr("&Right here for Context Menu"), this);
	gridLayout->addWidget(this->pbContextMenuButton);

	this->openGL = new COpenGLObject(this);
	gridLayout->addWidget(this->openGL);
	this->setLayout(gridLayout);

}

C3DView::~C3DView()
{

	if (this->openGL)
		delete this->openGL;
	this->openGL = NULL;
	
}

void C3DView::paintEvent(QPaintEvent* e)
{
	


	QWidget::paintEvent(e);
}


void C3DView::keyReleaseEvent ( QKeyEvent * e )
{ 
	if ( this->openGL ) 
		this->openGL->keyReleaseEvent(e );

}


void C3DView::createPopUpMenu()
{
	CBaristaView::createPopUpMenu();

	
	// adds actions for 3D view
	if ( project.getNumberOfXYZPointArrays() > 0 )
	{
		QAction* loadXYZPointsAction = new QAction(tr("&Show/Hide XYZ Points"), this);
		connect(loadXYZPointsAction, SIGNAL(triggered()), this, SLOT(loadXYZPoints()));
		this->addPopUpAction(loadXYZPointsAction, true);
	}

	// switch on different stuff
	if ( project.getGeoreferencedPoints()->GetSize() > 0 )
	{
		bool ticked = project.getGeoreferencedPoints()->getDisplaySettings()->getShowEntities3D();
		QAction* showGeoPointsAction = new QAction(tr("&Show Georeferenced Points"), this);
		showGeoPointsAction->setCheckable(true);
		showGeoPointsAction->setChecked(ticked);
		connect(showGeoPointsAction, SIGNAL(triggered()), this, SLOT(setShowGeoPoints()));
		this->addPopUpAction(showGeoPointsAction, true);
	}

	if ( project.getMonoplottedPoints()->GetSize() > 0 )
	{
		bool ticked = project.getMonoplottedPoints()->getDisplaySettings()->getShowEntities3D();
		QAction* showMonoplottedPointsAction = new QAction(tr("&Show Monoplotted Points"), this);
		showMonoplottedPointsAction->setCheckable(true);
		showMonoplottedPointsAction->setChecked(ticked);
		connect(showMonoplottedPointsAction, SIGNAL(triggered()), this, SLOT(setShowMonoplottedPoints()));
		this->addPopUpAction(showMonoplottedPointsAction, false);
	}

	if ( project.getNumberOfMonoplottedLines() > 0 )
	{
		bool ticked = project.getXYZPolyLines()->getDisplaySettings()->getShowEntities3D();
		QAction* showMonoplottedLinesAction = new QAction(tr("&Show Monoplotted Lines"), this);
		showMonoplottedLinesAction->setCheckable(true);
		showMonoplottedLinesAction->setChecked(ticked);
		connect(showMonoplottedLinesAction, SIGNAL(triggered()), this, SLOT(setShowMonoplottedLines()));
		this->addPopUpAction(showMonoplottedLinesAction, false);
	}

	if ( project.getNumberOfMonoplottedBuildings() > 0 )
	{
#ifdef WIN32
		bool ticked = project.getBuildings()->getDisplaySettings()->getShowEntities3D();
		QAction* showMonoplottedBuildingsAction = new QAction(tr("&Show Monoplotted Buildings"), this);
		showMonoplottedBuildingsAction->setCheckable(true);
		showMonoplottedBuildingsAction->setChecked(ticked);
		connect(showMonoplottedBuildingsAction, SIGNAL(triggered()), this, SLOT(setShowMonoplottedBuildings()));
		this->addPopUpAction(showMonoplottedBuildingsAction, false);
#endif		
	}

	if ( project.getNumberOfDEMs() > 0 )
	{
		QAction* loadDEMsAction = new QAction(tr("&Show/Hide DEMs"), this);
		connect(loadDEMsAction, SIGNAL(triggered()), this, SLOT(loadDEMs()));
		this->addPopUpAction(loadDEMsAction, true);
	}

	if ( project.getNumberOfAlsDataSets() > 0 )
	{
		QAction* loadALSAction = new QAction(tr("&Show/Hide ALS data sets"), this);
		connect(loadALSAction, SIGNAL(triggered()), this, SLOT(loadALS()));
		this->addPopUpAction(loadALSAction, true);
	}


	QAction* resetViewAction = new QAction(tr("&Reset View"), this);
	connect(resetViewAction, SIGNAL(triggered()), this, SLOT(resetView()));
	this->addPopUpAction(resetViewAction, true);
}

void C3DView::loadXYZPoints()
{
    CObjectSelector selector;
	selector.makeXYZPointSelector();

    ZObject* object = selector.getSelection();
    CXYZPointArray* xyz = (CXYZPointArray*)object;

	bool onoff = !xyz->getDisplaySettings()->getShowEntities3D();

	xyz->getDisplaySettings()->setShowEntities3D(onoff);
	xyz->getDisplaySettings()->setShowLabels3D(true);

	if ( this->openGL ) this->openGL->updateGL();
}

void C3DView::loadDEMs()
{
    CObjectSelector selector;
	selector.makeDEMSelector();

    ZObject* object = selector.getSelection();
    CVDEM* dem = (CVDEM*)object;

	bool onoff = !dem->getDisplaySettings()->getShowEntities3D();

	dem->getDisplaySettings()->setShowEntities3D(onoff);

	if ( this->openGL ) this->openGL->updateGL();
}

void C3DView::loadALS()
{
    CObjectSelector selector;
	selector.makeALSSelector();

    ZObject* object = selector.getSelection();
    CVALS* als = (CVALS*)object;

	bool onoff = !als->getDisplaySettings()->getShowEntities3D();

	als->getDisplaySettings()->setShowEntities3D(onoff);

	if ( this->openGL ) this->openGL->updateGL();
}



void C3DView::setShowGeoPoints()
{
	if ( project.getGeoreferencedPoints()->GetSize() > 0)
	{
		bool newshow = !project.getGeoreferencedPoints()->getDisplaySettings()->getShowEntities3D();
		project.getGeoreferencedPoints()->getDisplaySettings()->setShowEntities3D(newshow);
	}
	if ( this->openGL ) this->openGL->updateGL();

}

void C3DView::setShowMonoplottedPoints()
{
	if (project.getMonoplottedPoints()->GetSize() > 0)
	{
		bool newshow = !project.getMonoplottedPoints()->getDisplaySettings()->getShowEntities3D();
		project.getMonoplottedPoints()->getDisplaySettings()->setShowEntities3D(newshow);
	}
	if ( this->openGL ) this->openGL->updateGL();
}

void C3DView::setShowMonoplottedLines()
{
	if (project.getXYZPolyLines()->GetSize() > 0)
	{
		bool newshow = !project.getXYZPolyLines()->getDisplaySettings()->getShowEntities3D();
		project.getXYZPolyLines()->getDisplaySettings()->setShowEntities3D(newshow);
	}
	if ( this->openGL ) this->openGL->updateGL();
}

void C3DView::setShowMonoplottedBuildings()
{
#ifdef WIN32
	if (project.getBuildings()->GetSize() > 0)
	{
		bool newshow = !project.getBuildings()->getDisplaySettings()->getShowEntities3D();
		project.getBuildings()->getDisplaySettings()->setShowEntities3D(newshow);
	}
	if ( this->openGL ) this->openGL->updateGL();
#endif
}

void C3DView::resetView()
{
	if ( this->openGL)
		this->openGL->resetView();
}



