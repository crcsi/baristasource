/**
 * @class CImageView
 *
 * Class for viewing CHugeImage's @see CHugeImage
 * 
 * 
 * 
 * 
 * @author Harry Hanley
 * @version 1.0 
 * @date 20-jan-2005
 *
 */

#include "ImageView.h"

// qt includes
#include <QPainter>
#include <QFile>
#include <QMessageBox>
#include <QTime>

#include "Bui_LabelChangeDlg.h"
#include "Bui_MonoPlotDlg.h"
#include "Bui_MainWindow.h"
#include "Bui_FeatureExtractionDialog.h"
#include "Bui_orthodialog.h"
#include "Bui_ProgressDlg.h"



#include "Icons.h"
#include "ProgressThread.h"
#include "GDALFile.h"
#include "OrthoImageGenerator.h"
#include "XYPolyLine.h"
#include "LeastSquaresSolver.h"
#include "BestFit2DEllipse.h"
#include "BaristaProject.h"
#include "Bui_CropROIDlg.h"

#include "QTImageHelper.h"
#include "BaristaProjectHelper.h"

#include "Bui_SnakeDlg.h"

 
CImageView::CImageView(CVImage* img, QWidget * parent) :
        CBaristaView(img->getName(),*img->getHugeImage()->getTransferFunction(),
		             -0.5f, double(img->getHugeImage()->getWidth()), 
					 -0.5f, double(img->getHugeImage()->getHeight()), 
					 0.5f, 0.5f, parent), image(img)
{       
	// we have to do that after the constructor of the BaristaView is finished,
	// so that this is really an image view!!
	this->initEventHandler(); // calls parent class CBaristaView that adds a handler using CBaristaViewEventHandler for this CImageView

	this->image->addListener(this); // adds 'this' view as a listener for 'image' (using the parent Class CUpdateHandler)
	this->image->getXYPoints()->addListener(this); // adds 'this' view as a listener for '2D image points' (using the parent Class CUpdateHandler )
	// whenever 'image' or '2D image points' get changed they inform the listener 'this' (view) to redraw the information
	// for example, when elements of 2D points are changed or a new point is added (say, using digitize to add a point)
}


CImageView::~CImageView(void)
{

}



CHugeImage *CImageView::getHugeImage() const
{
	return this->image->getHugeImage(); 
}


/** draw the picture
 * @param p 
 */
void CImageView::drawContents ( QPainter &p)
{
	if (this->image == NULL)
		return;
	bool mode; // 0 for image residuals, 1 for object residuals
	this->drawContentsPre(p);
	CXYPointArray* measuredPoints = this->image->getXYPoints();
	float gsd = project.getGSDValue(); // any input gives same gsd!
	if (project.getPntShowType() != pNoPoint)
	{ // beginning of point show if statement
	// draw measured xy points
	int *prjLblC= project.getLblColor();
	int *prjPntC= project.getPntColor();

	QColor lC(prjLblC[0], prjLblC[1], prjLblC[2]);
	QColor pC(prjPntC[0], prjPntC[1], prjPntC[2]);

	if (measuredPoints && measuredPoints->GetSize() && measuredPoints->getActive())
		this->drawXYPointArray(measuredPoints,p,project.getLblShowType(),project.getPntShowType(), lC, pC, project.getDotWidth());

	if (project.isShowProjectedPoint())// draw projected points
	{
		CXYPointArray* projectedPoints = this->image->getProjectedXYPoints();

		if (projectedPoints && projectedPoints->GetSize() && projectedPoints->getActive() )
			this->drawXYPointArray(projectedPoints,p,project.getLblShowType(),project.getPntShowType(),Qt::yellow,Qt::yellow, project.getDotWidth());
	}

	} // end of point show if statement

	// may be included into imageshow dialog under Window menu
	CXYPointArray* fiducialMarks = this->image->getFiducialMarkObs();

	if (fiducialMarks && fiducialMarks->GetSize()&& fiducialMarks->getActive())
		this->drawXYPointArray(fiducialMarks,p,project.getLblShowType(),project.getPntShowType(),Qt::blue,1,4,Qt::blue);
	

	if (project.getResShowType() != rNoResidual)
	{// beginning of residual show if statement			
	// draw residual vector
		
	CXYPointArray* residualPoints = this->image->getResiduals();
	
	int *prjResC= project.getResColor();
	QColor rC(prjResC[0], prjResC[1], prjResC[2]);
	
	if (residualPoints && measuredPoints && residualPoints->GetSize() && 
		//measuredPoints->GetSize() && residualPoints->getActive())
		measuredPoints->GetSize() && residualPoints->getActive() && project.getResShowType() == rImgxy) // addition
	{
		mode = false;
		int residualCount = residualPoints->GetSize();
		int measuredPointCount = measuredPoints->GetSize();
			
		if ( measuredPointCount  && residualCount )
		{
			for (int i = 0; i < residualCount; i++)
			{
				CObsPoint* respoint = residualPoints->GetAt(i);

				CCharString label = respoint->getLabel().GetChar();
					
				CObsPoint* measuredpoint = measuredPoints->getPoint( label);

				if (measuredpoint)
				{
					double xm = (*measuredpoint)[0];
					double ym = (*measuredpoint)[1];
					double xr = (*respoint)[0];
					double yr = (*respoint)[1];

					double f = this->residualFactor;

					double endx = xm + f*xr;
					double endy = ym + f*yr;

					CObsPoint endpoint(2);
					endpoint[0] = endx;
					endpoint[1] = endy;
	
					this->drawLine(p, measuredpoint, endpoint, rC,project.getLnWidth());
				}
			}
		}

	}

	// draw 3DresidualXY vector
	CXYPointArray* ObjRes = this->image->getXY3DResiduals();
	
	
	if (ObjRes && measuredPoints && ObjRes->GetSize() && 
		//measuredPoints->GetSize() && ObjRes->getActive())
		measuredPoints->GetSize() && ObjRes->getActive() && project.getResShowType() == rObXY) // addition
	{
		mode = true;
		int residualCount = ObjRes->GetSize();
		int measuredPointCount = measuredPoints->GetSize();
			
		if ( measuredPointCount  && residualCount )
		{
			for (int i = 0; i < residualCount; i++)
			{
				CObsPoint* respoint = ObjRes->GetAt(i);

				CCharString label = respoint->getLabel().GetChar();
					
				CObsPoint* measuredpoint = measuredPoints->getPoint( label);

				if (measuredpoint)
				{
					double xm = (*measuredpoint)[0];
					double ym = (*measuredpoint)[1];
					double xr = (*respoint)[0]/gsd;
					double yr = (*respoint)[1]/gsd;

					double f = this->residualFactor;

					double endx = xm + f*xr;
					double endy = ym + f*yr;

					CObsPoint endpoint(2);
					endpoint[0] = endx;
					endpoint[1] = endy;

					this->drawLine(p, measuredpoint, endpoint,  rC,project.getLnWidth());
				}
			}
		}

	}

	// draw 3Dresidual Height
	//CXYPointArray* ObjRes = this->image->getXY3DResiduals();
	
	
	if (ObjRes && measuredPoints && ObjRes->GetSize() && 
		//measuredPoints->GetSize() && ObjRes->getActive())
		measuredPoints->GetSize() && ObjRes->getActive() && project.getResShowType() == rObHt) // addition
	{
		mode = true;		
		int residualCount = ObjRes->GetSize();
		int measuredPointCount = measuredPoints->GetSize();
			
		if ( measuredPointCount  && residualCount )
		{
			for (int i = 0; i < residualCount; i++)
			{
				CObsPoint* respoint = ObjRes->GetAt(i);

				CCharString label = respoint->getLabel().GetChar();
					
				CObsPoint* measuredpoint = measuredPoints->getPoint( label);

				if (measuredpoint)
				{
					double xm = (*measuredpoint)[0];
					double ym = (*measuredpoint)[1];
					double xr = xm;
					double yr = respoint->getDot()/gsd;

					double f = this->residualFactor;

					double endx = xm;// + f*xr;
					double endy = ym + f*yr;

					CObsPoint endpoint(2);
					endpoint[0] = endx;
					endpoint[1] = endy;

					this->drawLine(p, measuredpoint, endpoint, rC,project.getLnWidth());
				}
			}
		}

	}

	// draw diff vector
	CXYPointArray* diffPoints = this->image->getDifferencedXYPoints();
	
	//if (diffPoints && measuredPoints  && diffPoints->getActive())
	if (diffPoints && measuredPoints  && diffPoints->getActive() && project.getResShowType() == rImgDiff) // addition
	{
		mode = false;		
		int diffCount = diffPoints->GetSize();
		int measuredPointCount = measuredPoints->GetSize();
			
		if ( measuredPointCount  && diffCount )
		{
			for (int i = 0; i < diffCount; i++)
			{
				CObsPoint* diffpoint = diffPoints->GetAt(i);

				CCharString label = diffpoint->getLabel().GetChar();
					
				CObsPoint* measuredpoint = measuredPoints->getPoint( label);

				if (measuredpoint)
				{
					double xm = (*measuredpoint)[0];
					double ym = (*measuredpoint)[1];
					double xr = (*diffpoint)[0];
					double yr = (*diffpoint)[1];

					double f = this->residualFactor;

					double endx = xm + f*xr;
					double endy = ym + f*yr;

					CObsPoint endpoint(2);
					endpoint[0] = endx;
					endpoint[1] = endy;

					this->drawLine(p, measuredpoint, endpoint,  rC,project.getLnWidth());
				}
			}
		}

	}

		if (project.isShowScale()) // show scale bar
		{			
			drawScaleBar(p, mode, gsd);
		}
	} // end of residual show if statement

	
	if (this->image->getXYContours()->getActive() && this->image->getXYContours()->GetSize()>0)
		this->drawXYContours(this->image->getXYContours(),p);


	if (this->image->getXYEdges()->getActive() && this->image->getXYEdges()->GetSize()>0)
		this->drawXYPolyLines(this->image->getXYEdges(),p);


	if (this->image->getEllipses()->getActive() && this->image->getEllipses()->GetSize() > 0)
		this->drawEllipses(this->image->getEllipses(),p);


	if(this->CtrlDown)
	{
		QString crd;
		QString gv;
		crd.sprintf("(%lf, %lf)",cpos.x,cpos.y);
		gv.sprintf("(%d, %d, %d)",red,green,blue);

		double xscreen,yscreen;
   		toScreen(cpos.x, cpos.y, xscreen, yscreen);

		p.setPen(QPen(Qt::green,1));
		p.drawText (xscreen,yscreen -10,crd);
		p.drawText (xscreen,yscreen +20,gv);

		//this->drawZoomer(p,cpos);
	}	

	this->drawContentsPost(p);
}




/**
 * handle mouse move event
 * @param e 
 */
void CImageView::mouseMoveEvent ( QMouseEvent * e )
{    
	CBaristaView::mouseMoveEvent(e);
	this->update();
}

void CImageView::getPixelInfoHugeImage(QString& pixelInfo,
										 QString& geoInfo,
										 QString& colorInfo,
										 QString& zoomInfo,
										 QString& residualInfo,
										 QString& brightnessInfo,
										 CHugeImage* himage)
{

	CBaristaView::getPixelInfoHugeImage(pixelInfo,geoInfo,colorInfo,zoomInfo,residualInfo,brightnessInfo,himage);

	// if tfw then show georeferenced position
	if ( this->image->hasTFW() )
	{
		C3DPoint geo;
		const CSensorModel *sm = this->image->getTFW();

		if ( sm->transformObs2Obj(geo, cpos) )
		{
			if ( sm->getRefSys().getCoordinateType() == eGEOGRAPHIC )
				geoInfo.sprintf(" [geo]: %.8f, %.8lf",geo.x,geo.y);			
			else
				geoInfo.sprintf(" [geo]: %.3lf, %.3lf",geo.x,geo.y);
		}

	}

	pixelInfo.append(colorInfo);
	pixelInfo.append(geoInfo);
	pixelInfo.append(zoomInfo);
	pixelInfo.append(residualInfo);
	pixelInfo.append(brightnessInfo);
}



void CImageView::deletePointsROI()
{
	this->image->getXYPoints()->deletebyArea(this->roiUL.x, this->roiLR.x, this->roiUL.y, this->roiLR.y);
	this->unselectROI();
}

void CImageView::fitEllipsePointsROI()
{
	CXYPointArray points;

	this->image->getXYPoints()->getbyArea(this->roiUL.x, this->roiLR.x, this->roiUL.y, this->roiLR.y, &points);

	if ( points.GetSize() < 5 )
	{
		QMessageBox::warning( this, "Less than 5 points selected!"
		, "2D Ellipse fitting requires at least 5 points!");

		this->unselectROI();
		return;
	}

    CBestFit2DEllipse bestfitellipse;
    CCharStringArray Labels;
    CBlockDiagonalMatrix* covariance = new CBlockDiagonalMatrix();

	Matrix obs;
	
	int nObs = points.GetSize();
	obs.ReSize(nObs, 2);

    for(int i = 0; i < nObs; i++)
    {
		CObsPoint* point = points.GetAt(i);

		obs.element(i, 0) = (*point)[0];
		obs.element(i, 1) = (*point)[1];

        // Add covariance
        Matrix cov;

        cov.ReSize(2,2);
        cov = 0;
        cov.element(0, 0) = point->getSigma(0);
        cov.element(1, 1) = point->getSigma(1);
        cov.element(0, 1) = point->getCovarElement(0,1);
        cov.element(1, 0) = point->getCovarElement(1,0);
        covariance->addSubMatrix(cov);

        // Add label
    	CCharString* str;
        str = Labels.Add();
        *str = point->getLabel().GetChar();
    }

	int nPara = 5;

    CLeastSquaresSolver lss(&bestfitellipse, &obs, &Labels, NULL, covariance);

    bool maybe = lss.solve();

	if ( maybe )
	{
		Matrix result;
		result.ReSize(nPara, 1);
		result = 0;

		Matrix covmat;
		covmat.ReSize(nPara, nPara);
		covmat = 0;

		for(int i = 0; i < nPara; i++)
		{
			result.element(i, 0) = lss.getSolution()->element(i, 0);
			
			for(int j = 0; j < nPara; j++)
			{
				covmat.element(i, j) = lss.getCovariance()->element(i, j);
			}
		}

		CXYEllipse ellipse;
		CLabel label = "Ellipse";
		label += image->getEllipses()->GetSize();

		bui_LabelChangeDlg labeldlg(label.GetChar());
		labeldlg.setModal(true);
		labeldlg.exec();

		CLabel newlabel(labeldlg.getLabelText().GetChar());

		// if label is empty or alreday exists keep old label
		while (image->getEllipses()->getEllipse(newlabel.GetChar()))
		{
			QMessageBox::warning( this, "Change Label not possible!"
			, "This label already exist. Choose different Label!");
			labeldlg.setLabelText(label.GetChar());
			labeldlg.exec();

			newlabel.Set(labeldlg.getLabelText().GetChar());
		}
		
		label.Set(newlabel);

		ellipse.set(result, label);
		ellipse.setCovariance(covmat);

		CXYPoint center = ellipse.getCenter();
		image->getXYPoints()->Add(center);
		image->getEllipses()->Add(ellipse);

		image->getEllipses()->setActive(true);

	}
	else
	{
		QMessageBox::warning( this, "Ellipse fitting did not converge!"
			, "No 2D Ellipse was created");
	}


	delete covariance;

	this->unselectROI();

	this->update();
	baristaProjectHelper.refreshTree();
}

/**
 * Extract features 
 * @param  
 */
void CImageView::OpenFeaturesDialog()
{
	if ( this->haslargeROI )
	{
		QMessageBox::critical( 0, "Barista"," ROI is too large (> 2000 pixels) for Feature Extraction! Please select smaller area for these operations");
		return;
	}

	CFileName fn("FEX.ini");
	fn.SetPath(project.getFileName()->GetPath());

	bui_FeatureExtractionDialog featureExtractor(this, this->image, fn);

	featureExtractor.show();
	featureExtractor.exec();
}

/*

void CImageView::enhanceAreaROI()
{
	if ( this->haslargeROI )
	{
		QMessageBox::critical( 0, "Barista"," ROI is too large (> 2000 pixels) for enhancement! Please select smaller area for these operations");
		this->unselectROI();
		return;
	}

	if(this->getVImage()->getHugeImage()->getBands() !=1 || this->getVImage()->getHugeImage()->getBpp() != 8)
	{
		QMessageBox::critical( 0, "Barista","Enhancement of ROI is only implemented for 1 band images with 8 bit");
		this->unselectROI();
		return;
	}

	WallisParam par;
		
	if(this->getVImage()->getHugeImage()->getRoiPtr()==NULL)
	{
		//ROI is not defined!!! - Select ROI
		return;
	}

	par.gain_method=1;
	par.method=0;
	 par.flag_roi=0;
			 par.sizex=40;
	         par.sizey=40;
	         par.bfc=0.95;
	         par.cec=0.9;
	         par.clipp=1;
	     
	if(this->getVImage()->getHugeImage()->getBpp()==8)
	{
			 par.mean=127;
			 par.std=60;
			 par.bits=8;
	}
	else
	{
		 par.mean=1024;
			 par.std=600;
			 par.bits=11;
	
	}
	
	this->getVImage()->getHugeImage()->RadiometricEnhancement(par);

	this->update();
	return;

}
*/

void CImageView::closeEvent ( QCloseEvent * e )
{
	this->image->removeListener(this);
	this->image->getXYPoints()->removeListener(this);
	this->image->getHugeImage()->resetTiles();
     
	CBaristaView::closeEvent(e);	
}

void CImageView::keyReleaseEvent ( QKeyEvent * e )
{

	CBaristaView::keyReleaseEvent(e);


	// to increase/decrease residual scale
	if ( e->key() == Qt::Key_Plus )
	{
		this->changeResidualFactor(1.5);
	}
	else if ( e->key() == Qt::Key_Minus)
	{
		this->changeResidualFactor(0.5);
	}

	this->update();

}

/*
void CImageView::drawZoomer(QPainter &p, CObsPoint point)
{

	QPoint sp; // screen position
	int ox,oy;
	
	// x size frame
    int cx_size = 101;

	// y size frame
    int cy_size =75;
    
	// zoom of the frame, typically set to 2 times the zoom we have on the ImageView
	//
	double t_zoom=this->zoomFactor * 2.0;

	//size of the pixmap that we allocate from the image and active tile  1:1 (without zoom)
	//Attention we don't set this to the size of the frame...
	int sx=cx_size/t_zoom;
	int sy=cy_size/t_zoom;

	//offset to grap pixmap
	int tx=point[0]-((sx-1)/2.0);
	int ty=point[1]-((sy-1)/2.0);

	//get the screen position from the global coordinates
	sp.setX((point[0]+0.5)*this->zoomFactor - this->getScrollX());
    sp.setY((point[1]+0.5)*this->zoomFactor - this->getScrollY());

	//get the huge image
	CHugeImage* himage = image->getHugeImage();

	int pixelSize=himage->pixelSizeBytes();
	// in order to discriminate between grey scale and color images
	

	if (tx < 0.5 || tx+sx > himage->getWidth() || 
        ty < 0.5 || ty+sy > himage->getHeight() )
        return;

	// now we allocate the buffer to hold the pixel info and we brighten the pixel by factor 2.0
	unsigned char *temp = new unsigned char[sx*sy*pixelSize];

	if ( himage->getColorIndexed() )
		himage->getRect(temp, ty , sy, tx, sx);
	else
		himage->getBrightenedRect(temp, ty , sy, tx, sx, 2.0);

	// we create the pixmap from the buffer

	int r = himage->getRChannel();
	int g = himage->getGChannel();
	int b = himage->getBChannel();

	QPixmap* pixmap = QTImageHelper::createQPixmap(temp,sx,sy,pixelSize,himage->getBands(),r,g,b, himage->getLookupTable());

    delete [] temp;

	// check the offsets to position the zoomer next to the cursor, default left from cursor
	ox=sp.x()-cx_size - 10;
	oy=sp.y()+ 10;

	if(ox<0.5){
		ox=sp.x()+10;
	}
	if(ox + cx_size >=himage->getWidth()){
		return;
	}
	if(oy+cy_size >=himage->getHeight()){
		oy=sp.y()-cy_size-10;
	}
	if(oy<0.5){
		return;
	}

	// scale the pixmap
	QMatrix m,sh;
	m.scale(t_zoom,t_zoom);
	
	QPixmap pixScaled;
	pixScaled = pixmap->transformed(m);

	// draw on the painter
	p.drawPixmap( ox, oy, pixScaled);

	//draw a cross in the middle
	p.setPen(QPen(Qt::black,1));

	p.drawLine(ox + (int)(((cx_size-1)/2.0)+0.5) - 4, oy+(int)(((cy_size-1)/2.0)+0.5) , ox + (int)(((cx_size-1)/2.0)+0.5) + 4 , oy+(int)(((cy_size-1)/2.0)+0.5));
    p.drawLine(ox + (int)(((cx_size-1)/2.0)+0.5), oy+(int)(((cy_size-1)/2.0)+0.5) - 4, ox + (int)(((cx_size-1)/2.0)+0.5) , oy+(int)(((cy_size-1)/2.0)+0.5) + 4);
	
	//QPen pen2(Qt::darkCyan,2);
	p.setPen(QPen(Qt::green,2));
	p.drawRect(ox,oy,cx_size,cy_size);		
	
	delete pixmap;
}

*/


void CImageView::changeResidualFactor(double factor)
{
	this->residualFactor *= factor;
	this->update();
}

void CImageView::OrthofromROI()
{
	if (!this->image->getCurrentSensorModel() || (project.getNumberOfDEMs() < 1))
	{
		if (!this->image->getCurrentSensorModel()) 
			QMessageBox::warning( this, "Image not available or without georeferencing information!",
							 "No orthophoto generation possible!");
		else QMessageBox::warning( this, "No DEM is available!",
								 "No orthophoto generation possible!");
	}
	else
	{
		C2DPoint lr(this->roiLR);
		C2DPoint ul(this->roiUL); 

		bui_OrthoDialog orthoDlg(this->image, &lr, &ul, inifile.getCurrDir());

		orthoDlg.show();
		orthoDlg.exec();

		if (orthoDlg.getOrthoCreated()) baristaProjectHelper.refreshTree();
	}
}

void CImageView::regionGrowing(int xSeed, int ySeed)
{
	C2DPoint tmp((xSeed + this->getScrollX())/this->zoomFactor - 0.5,    
		         (ySeed + this->getScrollY())/this->zoomFactor - 0.5);

    // no points to be digitized outside image range
	if (tmp.x < 0.5 || tmp.x > this->image->getHugeImage()->getWidth() - 1 || 
        tmp.y < 0.5 || tmp.y > this->image->getHugeImage()->getHeight() - 1 )
        return;

	this->image->regionGrowing(tmp);
}

void CImageView::createPopUpMenu()
{
	CBaristaView::createPopUpMenu();
}

void CImageView::elementsChanged(CLabel element)
{

	CBaristaView::elementsChanged();

	this->setWindowTitle(this->image->getFileNameChar());
	this->setAccessibleName(this->image->getFileNameChar());
	this->update();

}

const CSensorModel* CImageView::getCurrentSensorModel()
{
	if (this->image)
		return this->image->getCurrentSensorModel();
	else
		return NULL;
}





const CTFW* CImageView::getTFW() const
{
	return this->image->getTFW();
}

void CImageView::CropROI()
{
	bool hasPushBroom = this->image->getPushbroom()->gethasValues();
	bool hasTFW = this->image->getTFW()->gethasValues();
	bool hasRPCs = this->image->getRPC()->gethasValues();
	bool hasPoints = this->image->getXYPoints()->GetSize() > 0;
	bool hasAffine = this->image->getAffine()->gethasValues();

	Bui_CropROIDlg cropdlg(this,"Crop from Image", "UserGuide/ImageView.html#CropROI",hasRPCs,hasPushBroom,hasTFW,hasPoints,hasAffine);
	cropdlg.exec();
}

void CImageView::drawScaleBar(QPainter &p, bool mode, double gsd)
{
	CObsPoint stpoint(2), enpoint(2), lbpoint(2), midpoint(2); // start, end of scale bar and its label
	//float gsd = this->image->getApproxGSD(100); // any input gives same gsd!		
		
	midpoint[0] = this->image->getHugeImage()->getWidth()/2;
	midpoint[1] = this->image->getHugeImage()->getHeight()-1000;
	lbpoint[1] = midpoint[1]-500;
/*
	stpoint[0] = 1000;
	
	
	enpoint[0] = stpoint[0] + project.getScaleValue()*this->residualFactor;
	enpoint[1] = stpoint[1];

	midpoint[0] = (stpoint[0] + enpoint[0])/2.0;
	midpoint[1] = stpoint[1];
	
	
*/	
	int d = (int) project.getScaleValue();
	double rest = 10*(project.getScaleValue() - (double)d);
	int dec = (int)rest;
	CLabel lbl = "";
	lbl += d;
	lbl += ".";
	lbl += dec;

	CLabel lbl_ini = "0";
	lbl_ini += ".";
	lbl_ini += "0";

	CLabel lbl_fin = "";
	lbl_fin += d;
	lbl_fin += ".";
	lbl_fin += dec;

	if (!mode)
	{
		lbpoint[0] = midpoint[0] -765;
		lbl += " pl";		
	}
	else
	{
		lbpoint[0] = midpoint[0] - 1750;
		lbl += " m";
		double gsd_rel = project.getScaleValue()/gsd;
		d = (int) gsd_rel;
		rest = 10*(gsd_rel - (double)d);
		dec = (int)rest;
		lbl += " (";
		lbl += d;
		lbl += ".";
		lbl += dec;
		lbl += " pl)";
	}
	int *prjResC= project.getResColor();
	QColor rC(prjResC[0], prjResC[1], prjResC[2]);

	this->drawScaleLabel(p, lbpoint, rC, 1, lbl.GetChar());

	double scale_length = project.getScaleValue()*this->residualFactor;
	stpoint[0] = midpoint[0]-scale_length/2.0;
	stpoint[1] = midpoint[1];
	
	enpoint[0] = midpoint[0]+scale_length/2.0;
	enpoint[1] = midpoint[1];

	this->drawLine(p, stpoint, enpoint, rC,project.getLnWidth());

	CObsPoint vpoint1(2), vpoint2(2);
	vpoint1[0] = enpoint[0];
	vpoint1[1] = enpoint[1]-150;
	vpoint2[0] = enpoint[0];
	vpoint2[1] = enpoint[1]+150;
	this->drawLine(p, vpoint1, vpoint2, rC,project.getLnWidth());
	//lbpoint[0] = enpoint[0]-240;
	//lbpoint[1] = enpoint[1]+500;
	//this->drawScaleLabel(p, lbpoint, project.getResColor(), 1, lbl_fin.GetChar());
	
	vpoint1[0] = stpoint[0];
	vpoint1[1] = stpoint[1]-150;
	vpoint2[0] = stpoint[0];
	vpoint2[1] = stpoint[1]+150;
	this->drawLine(p, vpoint1, vpoint2, rC,project.getLnWidth());
	//lbpoint[0] = stpoint[0]-240;
	//lbpoint[1] = stpoint[1]+500;
	//this->drawScaleLabel(p, lbpoint, project.getResColor(), 1, lbl_ini.GetChar());

	/*
	int parts = project.getScaleValue()/0.5;
	double scale_part = scale_length/parts;
	double len;
	int i = 1; double label = 0.5;
	vpoint1[1] = stpoint[1]-75;
	vpoint2[1] = stpoint[1]+75;
	while (i<parts)
	{
		len = scale_part*i;
		vpoint1[0] += scale_part;
		vpoint2[0] += scale_part;
		this->drawLine(p, vpoint1, vpoint2, project.getResColor(),project.getLnWidth());
		d = (int) label;
		rest = 10*(label - (double)d);
		dec = (int)rest;
		lbl = "";
		lbl += d;
		lbl += ".";
		lbl += dec;
		lbpoint[0] = stpoint[0]-240+len;
		lbpoint[1] = stpoint[1]+500;
		this->drawScaleLabel(p, lbpoint, project.getResColor(), 1, lbl.GetChar());

		i++;		
		label += 0.5;
	}	
	*/
}

/*this function were needed when the snake calculation was no monoplot function

void CImageView::openSnakeDialog()
{
	CFileName fn("snake.ini");
	fn.SetPath(project.getFileName()->GetPath());
	
	//get snake points
	CXYPointArray points;
	this->image->getXYPoints()->getbyArea(this->roiUL.x, this->roiLR.x, this->roiUL.y, this->roiLR.y, &points);
	
	if ( points.GetSize() < 2 )
	{
		QMessageBox::warning( this, "Less than 2 points selected!"
		, "Snake algorithm requires at least 2 points!");

		this->unselectROI();
		return;
	}

	//crop image of snake points
	C2DPoint snakeRoiUL((this->roiUL.x + this->roiLR.x)/2, (this->roiUL.y + this->roiLR.y)/2);
	C2DPoint snakeRoiLR((this->roiUL.x + this->roiLR.x)/2, (this->roiUL.y + this->roiLR.y)/2);
	CXYPoint snakePoint = new CXYPoint;
		
	for(int i =0; i < points.GetSize(); i++)
	{
		snakePoint = points.getPoint(i);
		if (snakePoint.y > snakeRoiLR.y) snakeRoiLR.y = snakePoint.y;
		if (snakePoint.x > snakeRoiLR.x) snakeRoiLR.x = snakePoint.x;
		if (snakePoint.y < snakeRoiUL.y) snakeRoiUL.y = snakePoint.y;
		if (snakePoint.x < snakeRoiUL.x) snakeRoiUL.x = snakePoint.x;
	}

	this->image->getHugeImage()->setRoi(snakeRoiUL.x, snakeRoiUL.y, snakeRoiLR.x-snakeRoiUL.x, snakeRoiLR.y-snakeRoiUL.y);

	this->roiLR = snakeRoiLR;
	this->roiUL = snakeRoiUL;
	
	if (this->haslargeROI )
	{
		QMessageBox::critical( 0, "Barista"," ROI is too large (> 2000 pixels) for Feature Extraction!"," Please select smaller area for these operations");
		return;
	}
	
	//calculation
	Bui_SnakeDlg dlg(this, fn, this->image, false);
	dlg.show();
	dlg.exec();	
}*/
