#include "DigitizingHandler.h"
#include "Bui_MainWindow.h"
#include "Label.h"
#include "Bui_LabelChangeDlg.h"
#include <QMessageBox>
#include "BaristaProjectHelper.h"
#include "Trans3DFactory.h"


CDigitizingHandler::CDigitizingHandler(void): geoXYZ(NULL), imgPoint(NULL)
{
	this->statusBarText = "DIGITIZEMODE ";
}

CDigitizingHandler::~CDigitizingHandler(void)
{
	//if (this->imgPoint )
		//delete this->imgPoint;
	this->imgPoint = NULL;

	if (this->geoXYZ )
		delete this->geoXYZ;
	this->geoXYZ = NULL;
}

bool CDigitizingHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{
	CBaristaViewEventHandler::handleMouseMoveEvent(e, v);
	return true;
};

bool CDigitizingHandler::keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
{
	CBaristaViewEventHandler::keyReleaseEvent(e, v);

	if ( e->key() == Qt::Key_L)
	{
		CLabel clabel;

		if (v->getVImage())
		{
			if (v->getVImage()->getCurrentLabel().GetLength() == 0)
			{
				clabel.Set("POINT");         
				clabel += v->getVImage()->getXYPoints()->GetSize();
				v->getVImage()->setCurrentLabel(clabel);
			}
			else clabel.Set(v->getVImage()->getCurrentLabel());

			bui_LabelChangeDlg labeldlg(v->getVImage()->getCurrentLabel().GetChar(), "New Point Label", true);

			labeldlg.setModal(true);
			labeldlg.exec();

			CLabel newlabel = labeldlg.getLabelText().GetChar();

			bool applyToAllImages = labeldlg.checkBoxAllViews->isChecked();
			bool replacePoint = labeldlg.cB_ReplaceExistingPoint->isChecked();

			if ( !applyToAllImages )
			{
				
				// single image check
				// if label is empty or alreday exists keep old label
				while (!replacePoint && v->getVImage()->getXYPoints()->getPoint(newlabel.GetChar()))
				{
					QMessageBox::warning( 0, "Change Label not possible!"
					, "This label already exist for this image. Choose different Label!");
					labeldlg.setLabelText(clabel.GetChar());
					labeldlg.exec();

					newlabel.Set(labeldlg.getLabelText().GetChar());
				}

				clabel.Set(newlabel);
				v->getVImage()->setCurrentLabel(clabel);
			}
			else
			{
				// multiple image check
				while (!replacePoint && !project.checkforUniquePointLabel(newlabel))
				{
					QMessageBox::warning( this->parent, "Change Label not possible!"
					, "This label already exist in one or more image(s). Choose different Label!");
					labeldlg.setLabelText(clabel.GetChar());
					labeldlg.exec();

					newlabel.Set(labeldlg.getLabelText().GetChar());
				}

				clabel.Set(newlabel);
				project.setCurrentXYPointLabelForAllImages(clabel);
			}

			labeldlg.close();
		}
		else if (v->getVDEM())
		{
		}
	}
	else if ( e->key() == Qt::Key_Delete || e->key() == Qt::Key_Backspace)
	{
		if ( v->getVImage() )
		{
			// retain label of measured point before deleting it
			int tokill = v->getVImage()->getXYPoints()->GetSize() - 1;

			if ( tokill >= 0 )
			{
				CLabel label = v->getVImage()->getXYPoints()->GetAt(tokill)->getLabel();
				v->getVImage()->getXYPoints()->RemoveAt(tokill);
				v->getVImage()->setCurrentLabel(label);

				this->imgPoint = NULL;

				if ( this->geoXYZ )
				{
					int geotokill = project.getGeoreferencedPoints()->GetSize() - 1;

					CXYZPoint* geopoint = project.getGeoreferencedPoints()->getAt(geotokill);
					if ( geopoint->getLabel() == label)
						project.getGeoreferencedPoints()->RemoveAt(geotokill);
					else
						project.getGeoreferencedPoints()->deletebyLabel(label.GetChar());


					//this->geoXYZ = NULL;

					if ( project.getGeoreferencedPoints()->GetSize() == 0 ) baristaProjectHelper.refreshTree();
				}
			}
			if ( v->getVImage()->getXYPoints()->GetSize() == 0 ) baristaProjectHelper.refreshTree();
		}
	}

	return true;
};

bool CDigitizingHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	return false;
};

bool CDigitizingHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	this->imgPoint = NULL; // initialize 2D image point pointer to null

	if (this->geoXYZ) delete this->geoXYZ;
	this->geoXYZ = NULL; // initialize 3D geo point pointer to null

    CHugeImage* himage = v->getHugeImage();
	
	if (!himage) // if himage not found exit with true
		return true;

	if (v->getVImage())
	{	
		CXYPoint tmp;

		v->getMouseEventCoords(e, tmp.x, tmp.y);
	    
		// no points to be digitized outside image range. Pixel position usually refers to the center of a square unit and therefore the offset value is set at 0.5
		if (tmp.getX() < -0.5 || tmp.getX() > himage->getWidth() || 
			tmp.getY() < -0.5 || tmp.getY() > himage->getHeight() )
			return false;
		
		CLabel l = v->getVImage()->getCurrentLabel();

		tmp.setLabel(l);
		
		// set default sigma SX and SY to 0.5 pixel
		tmp.setSX(0.5);
		tmp.setSY(0.5);

		CObsPoint* oldPoint = v->getVImage()->getXYPoints()->getPoint(l.GetChar());

		if (oldPoint)
			v->getVImage()->getXYPoints()->Remove(oldPoint);

		this->imgPoint = v->getVImage()->getXYPoints()->add(tmp); // add the newly digitized point to the existing 2D point array. 
		// This add function also informs all the listners (for exmaple, the current view) that the 2D point array a new element is added
		// On getting the information, the listenres take proper action, for exmaple, the current image view updates its view (redraw its content)

		if (v->getVImage()->getXYPoints()->GetSize() < 2 ) baristaProjectHelper.refreshTree(); // if 2D point array size is less than 2, 
		// the first element is just addedd, and therefore needs refreshing the tree view

		// create georeferenced XYZPoints (with Z = 0) if image has TFW
		if ( v->getVImage() )
		{
			if ( v->getVImage()->hasTFW() )
			{
				const CSensorModel *sm = v->getVImage()->getTFW();

				CXYZPointArray* geopoints = project.getGeoreferencedPoints();

				if ( project.getNumberOfGeoreferencedPoints() < 1)
					project.setGeoreferencedPointReferenceSystem(sm->getRefSys());

				eCoordinateType geotype = geopoints->getReferenceSystem()->getCoordinateType();
				eCoordinateType smtype = sm->getRefSys().getCoordinateType();

				CXYZPoint geo;

				if ( sm->transformObs2Obj(geo, tmp) )
				{

					CTrans3D *trafo = CTrans3DFactory::initTransformation(sm->getRefSys(), *(geopoints->getReferenceSystem()));
					if ((smtype != geotype) && !trafo)
					{
						QMessageBox::warning( v, "TFW parameters and georeferenced points are defined in different coordinate systems!"
						, "Coordinate transformation cannot be carried out");
					}

					geo.z = project.getMeanHeight();
					
					if (trafo) trafo->transform(geo,geo);

					project.addGeoreferencedPoint(geo);
					this->geoXYZ = new CXYZPoint(geo);
				}

				if ( project.getNumberOfGeoreferencedPoints() == 1 ) baristaProjectHelper.refreshTree();
			}
		}

		v->getVImage()->incrementCurrentLabel();
		v->update();
	}
	else if (v->getVDEM())
	{
	}

	return false;
};


bool CDigitizingHandler::redraw(CBaristaView *v)
{
	return true;
};

void CDigitizingHandler::initialize(CBaristaView *v)
{
};

void CDigitizingHandler::open()
{
	CBaristaViewEventHandler::open();
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;
	
	mainwindow->DigitizeAction->blockSignals(true);
	mainwindow->DigitizeAction->setChecked(true);
	mainwindow->DigitizeAction->blockSignals(false);

	//if (this->imgPoint )
		//delete this->imgPoint;
	this->imgPoint = NULL;

	if (this->geoXYZ )
		delete this->geoXYZ;
	this->geoXYZ = NULL;
};

void CDigitizingHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->DigitizeAction->blockSignals(true);
	mainwindow->DigitizeAction->setChecked(false);
	mainwindow->DigitizeAction->blockSignals(false);

	//if (this->imgPoint )
		//delete this->imgPoint;
	this->imgPoint = NULL;

	if (this->geoXYZ )
		delete this->geoXYZ;
	this->geoXYZ = NULL;
};


void CDigitizingHandler::setCurrentImageLabel(const CLabel& l)
{
	project.setCurrentXYPointLabelForAllImages(l);
}