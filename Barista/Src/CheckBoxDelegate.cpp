#include "CheckBoxDelegate.h"
#include <QCheckBox>
#include <QModelIndex>
#include <QEvent>

CCheckBoxDelegate::CCheckBoxDelegate(QObject *parent)
	: QItemDelegate(parent)
{

}


void CCheckBoxDelegate::paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const
{
	QString text;
	QStyleOptionViewItem localOptions = option;
	localOptions.displayAlignment = Qt::AlignRight | Qt::AlignVCenter;

	if (index.model()->data(index,Qt::DisplayRole).toBool())
		this->drawCheck(painter,option,option.rect,Qt::Checked);
	else
		this->drawCheck(painter,option,option.rect,Qt::Unchecked);

	drawFocus(painter,localOptions,localOptions.rect);


}


