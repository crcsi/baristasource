#include "LSMHandler.h"

#include "ImageView.h"
#include "HugeImage.h"
#include "VImage.h"
#include "BaristaProject.h"
#include "Bui_MainWindow.h"
#include "Bui_LabelChangeDlg.h"
#include "ProtocolHandler.h"

#include <QMessageBox>


CLSMHandler::CLSMHandler()
{
	this->statusBarText = "LSMMODE ";
	this->matchdlg.move(150, 120);

	this->matchdlg.setWindowTitle("Match Dialog");
}


CLSMHandler::~CLSMHandler()
{
};


bool CLSMHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	CMatchingParameters mpar = project.getMatchingParameter();

	double testmin = mpar.getMinZ();
	double testmax = mpar.getMaxZ();
	double range = testmax - testmin;

	if ( range <= 0.0 )
	{
		QMessageBox::warning( v, "Invalid height range!"
			, "Review matching parameters");
		this->openMatchingParameterDlg();
		return false;
	}

	this->imgPoint = NULL;
	CDigitizingHandler::handleLeftMouseReleaseEvent(e, v);

	bool ret = true;

	if ( this->imgPoint )
	{
		// get unique label

		CLabel l = this->imgPoint->getLabel();

		project.setCurrentXYPointLabelForAllImages(l);

		lsm.setProjectDir(project.getFileName()->GetPath());
		
		double z = mpar.getZ();
		float uncertaintyZ = mpar.getDeltaZ();
		bool ismonoplotted = false;
		
		if (fabs(mpar.getSigmaGeom()) > 2.0) 
		{
			mpar.setSigmaGeom(-fabs(mpar.getSigmaGeom()));
			mpar.setPyrLevel(0);
		}

		if (v->getVImage()->hasTFW() && project.getActiveDEM())
		{
			CXYZPoint objPt;
			mpar.setSigmaGeom(-fabs(mpar.getSigmaGeom()));

			CMonoPlotter monoplotter;
			monoplotter.setModel(v->getVImage()->getTFW());
			monoplotter.setVDEM(project.getActiveDEM(), false);

			if ((monoplotter.getDEMToSensor()) && (monoplotter.getSensorToDEM()))
			{
				if (monoplotter.getPoint(objPt, *(this->imgPoint))) 
				{
					z = objPt.z;
					uncertaintyZ = project.getActiveDEM()->getDEM()->getSigmaZ();
					ismonoplotted = true;
					mpar.setPyrLevel(0);
				}
			}
		}

		double sig = (mpar.getSigmaGeom() > 0) ? mpar.getSigmaGeom() : 0.5;

		// set default sigma SX and SY to 0.5 pixel
		this->imgPoint->setSX(sig);
		this->imgPoint->setSY(sig);

#ifdef _DEBUG
		outputMode = eMAXIMUM;
#endif

		ret = lsm.initialize(v->getVImage(), this->imgPoint, z, uncertaintyZ, mpar, 0,
							 mpar.getXYZFileName(), 0);

		if (ret) ret = lsm.MatchWithLSM();

		if (lsm.insertObjectPoint())
		{
			// check if point was matched and XYZ was calculated
			CXYZPointArray* matchedxyz = project.getPointArrayByName(mpar.getXYZFileName());

			if ( matchedxyz)
			{
				CObsPoint *matchpoint = matchedxyz->getPoint(this->imgPoint->getLabel().GetChar());
				if ( !matchpoint)
				{
					int yesno = QMessageBox::question(this->parent, "Matching was unsuccessful!",
					"No XYZ point could be calculated. Keep image point measurement?",
					"&Yes", "&No",	0, 1  );

					if ( yesno == 1 )
					{
						int lastadded = v->getVImage()->getXYPoints()->GetSize() -1;
						v->getVImage()->getXYPoints()->RemoveAt(lastadded);
					}
				}
				else
				{

					if (ismonoplotted)
					{
						CXYZPoint p;
						v->getVImage()->getTFW()->transformObs2Obj(p, *(this->imgPoint), (*matchpoint)[2], uncertaintyZ);
						matchpoint->setCovariance(p.getCovar());
					}

					CReferenceSystem refsys(matchedxyz->getRefSys());

					this->matchdlg.setRefSys(refsys);
					this->matchdlg.setDlgValues((*matchpoint)[0],(*matchpoint)[1],(*matchpoint)[2], matchpoint->getLabel().GetChar());
					this->matchdlg.show();
					this->matchdlg.raise();
				}
			}
			else
			{
				int lastadded = v->getVImage()->getXYPoints()->GetSize() -1;
				v->getVImage()->getXYPoints()->RemoveAt(lastadded);
			}
		}

		while (!project.checkforUniquePointLabel(l)) l++;

		project.setCurrentXYPointLabelForAllImages(l);
	}
	else return false;

	return ret;
};


bool CLSMHandler::keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
{
	CBaristaViewEventHandler::keyReleaseEvent(e, v);

	if ( e->key() == Qt::Key_L)
	{
		CLabel clabel;

		if (v->getVImage())
		{
			if (v->getVImage()->getCurrentLabel().GetLength() == 0)
			{
				clabel.Set("POINT");         
				clabel += v->getVImage()->getXYPoints()->GetSize();
				v->getVImage()->setCurrentLabel(clabel);
			}
			else clabel.Set(v->getVImage()->getCurrentLabel());

			bui_LabelChangeDlg labeldlg(v->getVImage()->getCurrentLabel().GetChar(), "New Point Label", true);
			labeldlg.forceLabelforAllViews();

			labeldlg.setModal(true);
			labeldlg.exec();

			CLabel newlabel = labeldlg.getLabelText().GetChar();

			// multiple image check
			while (!project.checkforUniquePointLabel(newlabel))
			{
				QMessageBox::warning( 0, "Change Label not possible!"
				, "This label already exist in one or more image(s). Choose different Label!");
				labeldlg.setLabelText(clabel.GetChar());
				labeldlg.exec();

				newlabel.Set(labeldlg.getLabelText().GetChar());
			}

			clabel.Set(newlabel);
			project.setCurrentXYPointLabelForAllImages(clabel);

			labeldlg.close();
		}
	}
	else if ( e->key() == Qt::Key_O)
	{
		bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

		mainwindow->setMatchingParameters();
	}
	else if ( e->key() == Qt::Key_M)
	{
		CMatchingParameters mpar = project.getMatchingParameter();
		bool onoff = !mpar.getShowMatchWindow();
		mpar.setShowMatchWindow(onoff);
		project.setMatchingParameter(mpar);
		v->setShowMatchWindow(onoff);
	}
	else if ( e->key() == Qt::Key_Delete || e->key() == Qt::Key_Backspace)
	{
		if ( this->imgPoint )
		{
			CLabel deleteLabel = this->imgPoint->getLabel();
			
			// delete image points in all images
			for ( int i = 0; i < project.getNumberOfImages(); i++)
			{
				CVImage* image = project.getImageAt(i);
				image->getXYPoints()->deletebyLabel(deleteLabel.GetChar());
			}

			// delete point from Matched 3D point list
			CXYZPointArray* matchedxyz = project.getPointArrayByName(project.getMatchingParameter().getXYZFileName());

			if ( matchedxyz )
				matchedxyz->deletebyLabel(deleteLabel.GetChar());

			this->imgPoint = NULL;
		}
	}

	return true;
};

void CLSMHandler::handleLeaveEvent ( QEvent * event, CBaristaView *v)
{
	CBaristaViewEventHandler::handleLeaveEvent(event,v);
	v->setShowMatchWindow(false);
}


void CLSMHandler::handleEnterEvent ( QEvent * event, CBaristaView *v )
{
	v->setShowMatchWindow(project.getMatchingParameter().getShowMatchWindow());	
}

void CLSMHandler::open()
{
	CBaristaViewEventHandler::open();
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->LSMAction->setChecked(true);
	mainwindow->blockSignals(false);

	// check matching parameter of project
	double testmin = project.getMatchingParameter().getMinZ();
	double testmax = project.getMatchingParameter().getMaxZ();

	double range = testmax - testmin;

	if ( project.getMatchingParameter().getPatchSize() < 3 || range <= 0.0 )
	{
		CMatchingParameters parameters;
		parameters.reset();
		project.setMatchingParameter(parameters);
		this->openMatchingParameterDlg();
	}
	lsm.clear();

	CLabel label("MATCH1");

	while ( !project.checkforUniquePointLabel(label))
		label++;
	
	project.setCurrentXYPointLabelForAllImages(label);

	this->imgPoint = NULL;

};

void CLSMHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->LSMAction->setChecked(false);
	mainwindow->blockSignals(false);
	lsm.clear();

	this->matchdlg.close();
	this->imgPoint = NULL;


	int n = this->baristaViews.getNumberOfViews();
	for (int i=0; i< n ; i++)
	{
		CBaristaView* v = this->baristaViews.getView(i);
		v->setShowMatchWindow(false);
	}
		

};

void CLSMHandler::openMatchingParameterDlg()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;
	mainwindow->setMatchingParameters();
}

			 