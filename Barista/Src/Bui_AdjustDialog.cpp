#include "Bui_AdjustDialog.h"
#include "BaristaProject.h"
#include "AdjustmentDlg.h"
#include "Bui_VImageDlg.h"
#include "Bui_XYZPointDlg.h"
#include <QFile>
#include <QMessageBox>
#include "CheckBoxDelegate.h"
#include <QHeaderView>
#include <QPushButton>
#include "QTImageHelper.h"
#include <QPainter>
#include <QListWidget>
#include <QScrollBar>
#include "Bui_ProtocolViewer.h"
#include "BundleHelper.h"
#include "SystemUtilities.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include <QMdiArea>
#include "VImage.h"
#include <QSortFilterProxyModel>
#include <QKeyEvent>
#include "BaristaHtmlHelp.h"
#include <QMenu>
#include "LookupTable.h"
#include "BaristaProjectHelper.h"
#include "Trans3DFactory.h"
#include "IniFile.h"

extern CIniFile inifile;

Bui_AdjustDialog::Bui_AdjustDialog(QWidget *parent)
	: QDialog(parent),vimageDlg(0),lW_ImageFilter(0),
	showImageFilter(false),	showPushBroom(Qt::Checked),showRPC(Qt::Checked),
	showAffine(Qt::Checked),showFrame(Qt::Checked),currentProtocolFile(&protocolFile),
	robust(false),maxRobustPercentage(0.03),minRobFac(4.0),obsPointDlg(0),obsPointDataModel(0),
	oldTab(0),lW_PointFilter(0),showGeographic(Qt::Checked), showGeocentric(Qt::Checked),
	showGrid(Qt::Checked),forward(false),excludeTie(false),pointView(0),bundleResult(0),infoExportStatus(true)
{
	baristaProjectHelper.setIsBusy(true);
	this->setupUi(this);

	inifile.getAdjustmentSettings(this->forward,this->excludeTie,this->maxRobustPercentage,this->minRobFac);

	// save the project names
	this->originalFileName = *project.getFileName();
	this->backupFileName = this->originalFileName + "_backup";
	// make a full backup
	this->saveProject(this->backupFileName);

	this->setWindowTitle("Bundle Adjustment");
	// connect all buttons
	this->connect(this->pB_Ok,SIGNAL(released()),this,SLOT(pbOkReleased()));
	this->connect(this->pB_Apply,SIGNAL(released()),this,SLOT(pbApplyReleased()));
	this->connect(this->pB_Cancel,SIGNAL(released()),this,SLOT(pbCancelReleased()));
	this->connect(this->pB_Help,SIGNAL(released()),this,SLOT(pbHelpReleased()));
	this->connect(this->pB_Protocol,SIGNAL(released()),this,SLOT(viewProtocol()));
	this->connect(this->pB_Run,SIGNAL(released()),this,SLOT(runAdjustment()));
	this->connect(this->pB_3DPoints,SIGNAL(released()),this,SLOT(view3DPoints()));

	this->sW_Dialogs->setCurrentIndex(0);

	//-------------------------
	// image view
	// create the datamodel and the filter proxy
	this->imageDataModel = new CImageDataModel(this);
	this->imageFilterModel = new QSortFilterProxyModel(this);

	// init the filter proxy
	this->imageFilterModel->setFilterRole(FilterRole);
	this->imageFilterModel->setDynamicSortFilter(true);
	this->imageFilterModel->setSourceModel(this->imageDataModel);
	this->imageFilterModel->setFilterRegExp(QRegExp("show", Qt::CaseInsensitive,QRegExp::FixedString));

	// init the image view
	// some settings need to be set before the view is connected to the model!
	this->imageView->verticalHeader()->setDefaultSectionSize(ROW_HEIGHT);
	this->imageView->verticalHeader()->hide();
	this->imageView->setSelectionMode(QTableView::SingleSelection);
	this->imageView->setItemDelegate(new CImageTableDelegate());
	this->imageView->setShowGrid(false);
	QSize iconSize(80,ROW_HEIGHT * N_SENSORMODELS);
	this->imageView->setIconSize(iconSize);
	this->imageView->setModel(this->imageFilterModel);
	// applt settings after the model as been set
	this->initView();

	// connect signals
	qRegisterMetaType<QModelIndex>("QModelIndex");
	qRegisterMetaType<CVImage>("CVImage");
	this->connect(this->imageDataModel,SIGNAL(selectedVImageChanged(CVImage*)),this,SLOT(initVImageDlg(CVImage*)));
	this->connect(this->imageDataModel,SIGNAL(selectionChaged()),this,SLOT(checkAdjustmentSettings()));
	this->connect(this->imageView,SIGNAL(clicked( const QModelIndex& )),this,SLOT(filteredImageClicked( const QModelIndex& )));
	this->connect(this->imageDataModel,SIGNAL(forceToSave()),this,SLOT(forceCurrentDlgToSave()));
	// init the image filter
	this->connect(this->tB_ImageFilter,SIGNAL(released()),this,SLOT(pBImageFilterReleased()));
	// just to init the line edit window
	this->showImageFilter = true;
	this->pBImageFilterReleased();

	// select the first image
	this->imageDataModel->selectCurrentImage();


	//-------------------------
	// point view
	// create the view and add to tab
	this->pointView = new CPointTableView();
	this->tab_3DPoints->layout()->addWidget(this->pointView);
	// create data model and filter proxy
	this->obsPointDataModel = new CObsPointArrayDataModel(this);
	this->obsPointFilterModel = new QSortFilterProxyModel(this);
	// init the filter proxy
	this->obsPointFilterModel->setFilterRole(FilterRole);
	this->obsPointFilterModel->setDynamicSortFilter(true);
	this->obsPointFilterModel->setSourceModel(this->obsPointDataModel);
	this->obsPointFilterModel->setFilterRegExp(QRegExp("show", Qt::CaseInsensitive,QRegExp::FixedString));

	// init the view
	this->pointView->verticalHeader()->hide();
	this->pointView->setSelectionMode(QTableView::SingleSelection);
	this->pointView->setShowGrid(false);
	this->pointView->setModel(this->obsPointFilterModel);
	this->pointView->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);

	// connect signals
	this->connect(this->obsPointDataModel,SIGNAL(selectedObsPointArrayChanged(CObsPointArray*,CObsPointArray*)),this,SLOT(init3DPointDlg(CObsPointArray*,CObsPointArray*)));
	this->connect(this->pointView,SIGNAL(clicked( const QModelIndex& )),this,SLOT(filteredPointArrayClicked( const QModelIndex& )));
	this->connect(this->pointView,SIGNAL(contextMenuCalled( const QModelIndex& )),this,SLOT(filteredPointArrayRightClicked( const QModelIndex& )));
	// init the image filter
	this->connect(this->tB_PointFilter,SIGNAL(released()),this,SLOT(pBPointFilterReleased()));


	// select point array
	this->obsPointDataModel->selectCurrentPointArray();
	// just to init the line edit window
	this->showPointFilter = true;
	this->pBPointFilterReleased();

	//-------------------------
	// robust settings
	this->lE_Threshold->setValidator(new QDoubleValidator(this));
	this->lE_maxErrors->setValidator(new QDoubleValidator(this));
	// robust signals
	this->connect(this->cB_Robust,SIGNAL(stateChanged(int)),this,SLOT(cBRobustStateChanged(int)));
	this->connect(this->lE_maxErrors,SIGNAL(textChanged(const QString &)),this,SLOT(textChanged(const QString&)));
	this->connect(this->lE_Threshold,SIGNAL(textChanged(const QString &)),this,SLOT(textChanged(const QString&)));
	this->connect(this->lE_maxErrors,SIGNAL(editingFinished()),this,SLOT(editingFinishedMaxRobust()));
	this->connect(this->lE_Threshold,SIGNAL(editingFinished()),this,SLOT(editingFinishedMinRobFac()));
	this->connect(this->pB_ResetRobust,SIGNAL(released()),this,SLOT(resetRobust()));

	this->connect(this->cB_Forward,SIGNAL(stateChanged(int)),this,SLOT(cBForwardStateChanged(int)));
	this->connect(this->cB_ExcludeTie,SIGNAL(stateChanged(int)),this,SLOT(cBExcludeTieStateChanged(int)));

	//-------------------------
	// protocol stuff

	this->iterationFile =project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "iteration.prt";
	this->protocolFile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "adjust.prt";
	this->infoFileName = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "info.txt";

	this->lE_ProtocolFile->setText(this->currentProtocolFile->GetChar());
	this->lE_ProtocolFile->setToolTip(this->currentProtocolFile->GetChar());
	this->writeAdjustmentResults("-","-","-","-","-");

	this->lE_3DPoints_File->setText(this->infoFileName.GetChar());
	this->lE_ProtocolFile->setToolTip(this->infoFileName.GetChar());

	//-------------------------
	// tab stuff
	this->connect(this->tW_Objects,SIGNAL(currentChanged (int)),this, SLOT(tWCurrentChanged( int)));

	this->applySettings();
	baristaProjectHelper.setIsBusy(false);
}

void Bui_AdjustDialog::keyPressEvent( QKeyEvent * keyEvent)
{
	if (keyEvent->key() == Qt::Key_Escape)
	{
		keyEvent->accept();
		int button =QMessageBox::question(this,"Close Adjustment","Closing of the adjustment will restore old project status!","Close","Save and Close","Cancel");
		if (button == 0)
			this->pbCancelReleased();
		else if (button == 1)
			this->pbOkReleased();

	}
}

void Bui_AdjustDialog::pBImageFilterReleased( )
{

	if (this->showImageFilter)
	{
		if (this->lW_ImageFilter)
		{
			this->lW_ImageFilter->close();
			this->disconnect(this->lW_ImageFilter,SIGNAL( itemClicked ( QListWidgetItem *)),this,SLOT(imageFilterItemClicked( QListWidgetItem *)));
			delete this->lW_ImageFilter;
		}
		this->lW_ImageFilter = NULL;
		this->tB_ImageFilter->setArrowType(Qt::DownArrow);
		if (this->showPushBroom && this->showRPC && this->showAffine && this->showFrame)
			this->lE_ImageFilter->setText("all images shown");
		else
			this->lE_ImageFilter->setText("images filtered");
	}
	else
	{
		this->lW_ImageFilter = new QListWidget(this->tab_Images);
		this->lW_ImageFilter->setGeometry(23,20,136,65);
		this->lW_ImageFilter->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		this->lW_ImageFilter->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		this->lW_ImageFilter->addItem("show PushBroom");
		this->lW_ImageFilter->item(0)->setCheckState(this->showPushBroom);
		this->lW_ImageFilter->item(0)->setData(Qt::UserRole,ePUSHBROOM);
		this->lW_ImageFilter->addItem("show RPC");
		this->lW_ImageFilter->item(1)->setCheckState(this->showRPC);
		this->lW_ImageFilter->item(1)->setData(Qt::UserRole,eRPC);
		this->lW_ImageFilter->addItem("show Affine");
		this->lW_ImageFilter->item(2)->setCheckState(this->showAffine);
		this->lW_ImageFilter->item(2)->setData(Qt::UserRole,eAFFINE);
		this->lW_ImageFilter->addItem("show Frame");
		this->lW_ImageFilter->item(3)->setCheckState(this->showFrame);
		this->lW_ImageFilter->item(3)->setData(Qt::UserRole,eFRAMECAMERA);
		this->lW_ImageFilter->setWindowFlags(Qt::SubWindow);
		this->tB_ImageFilter->setArrowType(Qt::UpArrow);
		this->lE_ImageFilter->setText("choose filter");
		this->connect(this->lW_ImageFilter,SIGNAL( itemClicked ( QListWidgetItem *)),this,SLOT(imageFilterItemClicked( QListWidgetItem *)));


		this->lW_ImageFilter->show();
	}

	this->showImageFilter = !this->showImageFilter;

}

void Bui_AdjustDialog::pBPointFilterReleased()
{
	if (this->showPointFilter)
	{
		if (this->lW_PointFilter)
		{
			this->lW_PointFilter->close();
			this->disconnect(this->lW_PointFilter,SIGNAL( itemClicked ( QListWidgetItem *)),this,SLOT(pointFilterItemClicked( QListWidgetItem *)));
			delete this->lW_PointFilter;
		}
		this->lW_PointFilter = NULL;
		this->tB_PointFilter->setArrowType(Qt::DownArrow);
		if (this->showGeographic && this->showGeocentric && this->showGrid)
			this->lE_PointFilter->setText("all shown");
		else
			this->lE_PointFilter->setText("filtered");
	}
	else
	{
		this->lW_PointFilter = new QListWidget(this->tab_3DPoints);
		this->lW_PointFilter->setGeometry(23,20,136,50);
		this->lW_PointFilter->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		this->lW_PointFilter->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		this->lW_PointFilter->addItem("show Geographic");
		this->lW_PointFilter->item(0)->setCheckState(this->showGeographic);
		this->lW_PointFilter->item(0)->setData(Qt::UserRole,eGEOGRAPHIC);
		this->lW_PointFilter->addItem("show Geocentric");
		this->lW_PointFilter->item(1)->setCheckState(this->showGeocentric);
		this->lW_PointFilter->item(1)->setData(Qt::UserRole,eGEOCENTRIC);
		this->lW_PointFilter->addItem("show Grid");
		this->lW_PointFilter->item(2)->setCheckState(this->showGrid);
		this->lW_PointFilter->item(2)->setData(Qt::UserRole,eGRID);
		this->lW_PointFilter->setWindowFlags(Qt::SubWindow);
		this->tB_PointFilter->setArrowType(Qt::UpArrow);
		this->lE_PointFilter->setText("choose filter");
		this->connect(this->lW_PointFilter,SIGNAL( itemClicked ( QListWidgetItem *)),this,SLOT(pointFilterItemClicked( QListWidgetItem *)));


		this->lW_PointFilter->show();
	}

	this->showPointFilter = !this->showPointFilter;
}


void Bui_AdjustDialog::imageFilterItemClicked( QListWidgetItem * item )
{
	if (item->data(Qt::UserRole).toInt() == ePUSHBROOM)
	{
		this->showPushBroom = (this->showPushBroom == Qt::Checked )? Qt::Unchecked : Qt::Checked;
		this->lW_ImageFilter->item(0)->setCheckState(this->showPushBroom);

	}
	else if (item->data(Qt::UserRole).toInt() ==  eRPC)
	{
		this->showRPC = (this->showRPC == Qt::Checked )? Qt::Unchecked : Qt::Checked;
		this->lW_ImageFilter->item(1)->setCheckState(this->showRPC);
	}
	else if (item->data(Qt::UserRole).toInt() ==  eAFFINE)
	{
		this->showAffine = (this->showAffine == Qt::Checked )? Qt::Unchecked : Qt::Checked;
		this->lW_ImageFilter->item(2)->setCheckState(this->showAffine);
	}
	else if (item->data(Qt::UserRole).toInt() ==  eFRAMECAMERA)
	{
		this->showFrame = (this->showFrame == Qt::Checked )? Qt::Unchecked : Qt::Checked;
		this->lW_ImageFilter->item(3)->setCheckState(this->showFrame);
	}

	this->imageDataModel->applyImageFilter(this->showPushBroom,this->showRPC,this->showAffine,this->showFrame);
	this->checkAdjustmentSettings();
}


void Bui_AdjustDialog::pointFilterItemClicked( QListWidgetItem * item)
{
	if (item->data(Qt::UserRole).toInt() == eGEOGRAPHIC)
	{
		this->showGeographic = (this->showGeographic == Qt::Checked )? Qt::Unchecked : Qt::Checked;
		this->lW_PointFilter->item(0)->setCheckState(this->showGeographic);

	}
	else if (item->data(Qt::UserRole).toInt() ==  eGEOCENTRIC)
	{
		this->showGeocentric = (this->showGeocentric == Qt::Checked )? Qt::Unchecked : Qt::Checked;
		this->lW_PointFilter->item(1)->setCheckState(this->showGeocentric);
	}
	else if (item->data(Qt::UserRole).toInt() ==  eGRID)
	{
		this->showGrid = (this->showGrid == Qt::Checked )? Qt::Unchecked : Qt::Checked;
		this->lW_PointFilter->item(2)->setCheckState(this->showGrid);
	}

	this->obsPointDataModel->applyPointFilter(this->showGeographic,this->showGeocentric,this->showGrid);
	this->checkAdjustmentSettings();
}


Bui_AdjustDialog::~Bui_AdjustDialog()
{

	this->deleteMemory();
}



void Bui_AdjustDialog::deleteMemory()
{
	if (this->vimageDlg)
		delete this->vimageDlg;
	this->vimageDlg = NULL;

	if (this->obsPointDlg)
		delete this->obsPointDlg;
	this->obsPointDlg = NULL;

	if (this->imageDataModel)
		delete this->imageDataModel;
	this->imageDataModel = NULL;


	if (this->imageFilterModel)
		delete this->imageFilterModel;
	this->imageFilterModel = NULL;

	if (this->obsPointDataModel)
		delete this->obsPointDataModel;
	this->obsPointDataModel = NULL;

	if (this->obsPointFilterModel)
		delete this->obsPointFilterModel;
	this->obsPointFilterModel = NULL;

	if (this->lW_ImageFilter)
		delete this->lW_ImageFilter;
	this->lW_ImageFilter = NULL;

	if (this->lW_PointFilter)
		delete this->lW_PointFilter;
	this->lW_PointFilter = NULL;

	if (this->pointView )
		delete this->pointView;
	this->pointView = NULL;
}



void Bui_AdjustDialog::initVImageDlg(CVImage* image)
{

	// create the dialog for the first time
	if (this->vimageDlg == NULL)  // show dialog for the first time
	{
		this->vimageDlg = new Bui_VImageDlg(image,this);
		this->connect(this->vimageDlg,SIGNAL(enableOkButton(bool)),this,SLOT(okButtonStatusChanged(bool)));
		this->vimageDlg->setMinimumSize(QSize(550,450));
		this->tab_VImage->layout()->addWidget(this->vimageDlg);
	}
	else // just change image
		this->vimageDlg->setVImage(image);

}

void Bui_AdjustDialog::init3DPointDlg(CObsPointArray* pointArray,CObsPointArray* residualArray)
{

	if (this->obsPointDlg == NULL)
	{
		this->obsPointDlg = new Bui_XYZPointDlg((CXYZPointArray*)pointArray,(CXYZPointArray*)residualArray);
		this->connect(this->obsPointDlg,SIGNAL(enableOkButton(bool)),this,SLOT(okButtonStatusChanged(bool)));
		this->obsPointDlg->setMinimumSize(QSize(550,450));
		this->tab_ObsPoint->layout()->addWidget(this->obsPointDlg);
	}
	else
		this->obsPointDlg->setPoints((CXYZPointArray*)pointArray,(CXYZPointArray*)residualArray);

}


void Bui_AdjustDialog::forceCurrentDlgToSave()
{
	// force first the old dlg to save it's content
	if (this->sW_Dialogs->currentIndex() == 0 && this->vimageDlg)
		this->vimageDlg->okButtonReleased();
	else if  (this->sW_Dialogs->currentIndex() == 1 && this->obsPointDlg)
		this->obsPointDlg->okButtonReleased();

	this->checkAdjustmentSettings();
}

void Bui_AdjustDialog::writeAdjustmentResults(QString s0,QString nObs,QString nPar,QString redundancy,QString grossErrors)
{
	this->lE_S0->setText(s0);
	this->lE_NObs->setText(nObs);
	this->lE_NPar->setText(nPar);
	this->lE_Redundancy->setText(redundancy);
	this->lE_GrossErrors->setText(grossErrors);
}


void Bui_AdjustDialog::saveProject(CFileName& filename)
{
	project.writeProject(filename.GetChar());
}

void Bui_AdjustDialog::loadProject(CFileName& filename)
{
	baristaProjectHelper.closeGUI();
	project.close();
	project.readProject(filename.GetChar());
	project.setFileName(this->originalFileName.GetChar());

}

void Bui_AdjustDialog::deleteBackup()
{
	QFile::remove(this->backupFileName.GetChar());
}

void Bui_AdjustDialog::pbOkReleased()
{
	if ((this->vimageDlg && !this->vimageDlg->okButtonReleased()) ||
		(this->obsPointDlg && !this->obsPointDlg->okButtonReleased()))
	{
		// keep the backup, just in case
		this->close();
	}


	inifile.setAdjustmentSettings(this->forward,this->excludeTie,this->maxRobustPercentage,this->minRobFac);
	inifile.writeIniFile();

	this->deleteBackup();
	baristaProjectHelper.refreshTree();
	this->close();
}

void Bui_AdjustDialog::pbApplyReleased()
{
	baristaProjectHelper.setIsBusy(true);
	if (this->vimageDlg && this->vimageDlg->okButtonReleased() &&
		this->obsPointDlg && this->obsPointDlg->okButtonReleased())
	{
		this->saveProject(this->backupFileName);
		baristaProjectHelper.refreshTree();
	}
	baristaProjectHelper.setIsBusy(false);
}

void Bui_AdjustDialog::pbCancelReleased()
{
	baristaProjectHelper.setIsBusy(true);
	this->deleteMemory();
	this->loadProject(this->backupFileName);
	this->deleteBackup();
	baristaProjectHelper.refreshTree();
	baristaProjectHelper.setIsBusy(false);
	this->close();

}


void Bui_AdjustDialog::pbHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/MenuAdjustments.html");
}

void Bui_AdjustDialog::okButtonStatusChanged(bool b)
{
	if (b)
	{
		this->gB_Adjustment->setEnabled(true);
		this->pB_Apply->setEnabled(true);
		this->pB_Ok->setEnabled(true);
		this->tW_Objects->setEnabled(true);
	}
	else
	{
		this->gB_Adjustment->setEnabled(false);
		this->pB_Apply->setEnabled(false);
		this->pB_Ok->setEnabled(false);
		this->tW_Objects->setEnabled(false);
	}

}

void Bui_AdjustDialog::initView()
{
	int n = this->imageView->model()->rowCount() / IMAGE_SPAN;
	for (int i=0; i < n; i++)
	{
		this->imageView->setSpan(i * IMAGE_SPAN,0,N_SENSORMODELS,1);
		this->imageView->setSpan(i * IMAGE_SPAN,1,N_SENSORMODELS,1);
		this->imageView->setSpan((i+1)* IMAGE_SPAN -1,0,1,3);
	}

	this->imageView->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
	this->imageView->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
	this->imageView->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Fixed);
	this->imageView->setColumnWidth(0,25);
	this->imageView->setColumnWidth(2,60);


}



void Bui_AdjustDialog::tWCurrentChanged( int index )
{

	if (oldTab == 0 && this->vimageDlg)
	{
		this->vimageDlg->okButtonReleased();
	}
	else if (oldTab == 1 && this->obsPointDlg)
	{
		this->obsPointDlg->okButtonReleased();

	}
	this->sW_Dialogs->setCurrentIndex(index);
	oldTab = index;

}


void Bui_AdjustDialog::viewProtocol()
{
	Bui_ProtocolViewer* p = new Bui_ProtocolViewer(*this->currentProtocolFile,this);
	p->show();
}

void Bui_AdjustDialog::view3DPoints()
{
	Bui_ProtocolViewer* q = new Bui_ProtocolViewer(this->infoFileName,this);
	q->show();
}

void Bui_AdjustDialog::runAdjustment()
{
	// first force current changes to be saved
	if (this->vimageDlg )
		this->vimageDlg->okButtonReleased();

	if (this->obsPointDlg )
		this->obsPointDlg->okButtonReleased();


	CBaristaBundleHelper helper;
	helper.initProtocol(this->iterationFile);
	// add images to the Observation handler
	CObservationHandlerPtrArray activeImages;
	CObservationHandlerPtrArray activeOrbits;
	this->imageDataModel->getActiveObservationHandler(&activeImages,&activeOrbits);


	// add check points to control point array
	CXYZPointArray* checkPoints = project.getCheckPoints();
	project.addCheckPointsToControlPoints(checkPoints); // project->controlpoints contains all points (checkpoints only, controlpoints only, mix)

	if (!this->forward)
		helper.bundleInit(&activeImages,&activeOrbits,this->forward,this->excludeTie);
	else
		helper.bundleInit(&activeImages,NULL,this->forward,this->excludeTie);

	int r = helper.getNObs() + helper.getNConstraints() - helper.getNUnknowns();
	int nObs = helper.getNObs();
	int nPar = helper.getNUnknowns();


	CVControl* Control = project.getControlPoints();
    if (Control == NULL && !this->forward)
	{
		QMessageBox::warning( this, "  No Control Points found!", "Set Control Points first!");
	}
	else
	{
		CReferenceSystem refsys;
		if (this->forward)
			refsys = activeImages.GetAt(0)->getCurrentSensorModel()->getRefSys();
		else
			refsys = Control->getCurrentSensorModel()->getRefSys();

		helper.setMaxRobustPercentage(this->maxRobustPercentage);
		helper.setMinRobFac(this->minRobFac);
		bui_ProgressDlg dlg(new CRunBundleThread(&helper,this->forward, this->robust, true),&helper);
		dlg.exec();
		helper.closeProtocol();

		if (!dlg.getSuccess())
		{

			QString caption("Adjustment failure!"), text(" Adjustment did not converge");
			QMessageBox::warning( this, caption, text);
			this->currentProtocolFile = &this->iterationFile;


			this->writeAdjustmentResults(	"not converged!",
											QString("%1").arg(nObs),
											QString("%1").arg(nPar),
											QString("%1").arg(r),
											QString("%1").arg(0));


		}
		else
		{
			this->currentProtocolFile = &this->protocolFile;
			helper.initProtocol(this->protocolFile);



			if (!this->bundleResult)
				this->bundleResult = project.addXYZPoints("AdjustmentResult", refsys, true, false,eBUNDLERESULT);

			helper.getObjectPoints(*this->bundleResult);


			if (checkPoints)
			{

				this->doQualityAssessment(helper, &activeImages);
			}

			helper.printResults();



			if (!this->forward)
			{
				helper.updateResiduals();
			}

			for (int i = 0; i < activeImages.GetSize(); ++i)
			{
				activeImages.GetAt(i)->getCurrentSensorModel()->setRefSys(refsys);
			}


			QString caption("Adjustment successful!"), text(helper.rmsString(" Adjustment was successful; s0: ").GetChar());
			QMessageBox::information( this, caption, text);

			double s0 = helper.getRMS();
			int grossErrors = helper.getNGrossErrors();
			this->writeAdjustmentResults(	QString().sprintf("%.2lf",s0),
											QString("%1").arg(nObs),
											QString("%1").arg(nPar),
											QString("%1").arg(r),
											QString("%1").arg(grossErrors));



			this->obsPointDataModel->initModel();

		}

		this->lE_ProtocolFile->setText(this->currentProtocolFile->GetChar());
		this->lE_ProtocolFile->setToolTip(this->currentProtocolFile->GetChar());

		if (this->infoExportStatus)
		{
			CVControl *control = project.getControlPoints();
			if (control && (control->nObs() > 0))
			{
				//helper.initProtocol(this->infoFileName);
				this->exportAdjustmentInfo(control,this->infoFileName);
			}
		}
	}

	// remove added check points
	project.removeChekPointsFromControlPoints(checkPoints);
	helper.closeProtocol();
}





void Bui_AdjustDialog::exportAdjustmentInfo(CVControl *control, CFileName infoFileName)
{
	CCharStringArray colHeads;
	double ratio; // multiplicative factor to transfer x and y residuals (for Geographic coordinate system only and x == Longitude, y == Latitude) into meter unit
	int prec1, prec2; // for decimal point precisions of points and residuals
	int wd1, wd2, wd3, wd4, wd5, wd6, wd7, wd8; // for 8 column widths
	CCharString ch;
	CCharString width = "     ";
	CXYZPointArray* points = control->getXYZPointArray();
	CXYZPointArray* residuals = control->getXYZResidualPointArray();

	if (points && residuals)
	{
		ofstream os(infoFileName);
		if (os.is_open())
		{
			this->guessHeaderFormat(points, colHeads, prec1, prec2, ratio);
			//os << header;
			os.setf(ios::fixed, ios::floatfield);
			//strncpy(

			// accumulate column headings of the table
			//os.precision(3);
			wd1 = max(colHeads.GetAt(0)->GetLength(),8); // assuming maximum Label length is 8 characters
			os.width(wd1);
			os << colHeads.GetAt(0)->GetChar() << width;

			//os.precision(3);
			wd2 = max(colHeads.GetAt(1)->GetLength(),5); // assuming maximum Type length is 5 characters
			os.width(wd2);
			os << colHeads.GetAt(1)->GetChar() <<  width;

			//os.precision(prec1);
			wd3 = max(colHeads.GetAt(2)->GetLength(),prec1+10); // assuming values of this column are maximum prec1 + 5 chars long
			os.width(wd3);
			os << colHeads.GetAt(2)->GetChar() << width;

			//os.precision(prec1);
			wd4 = max(colHeads.GetAt(3)->GetLength(),prec1+10); // assuming values of this column are maximum prec1 + 5 chars long
			os.width(wd4);
			os << colHeads.GetAt(3)->GetChar() << width;

			//os.precision(prec2);
			wd5 = max(colHeads.GetAt(4)->GetLength(),prec2+10); // assuming values of this column are maximum prec2 + 5 chars long
			os.width(wd5);
			os << colHeads.GetAt(4)->GetChar() << width;

			//os.precision(prec2);
			wd6 = max(colHeads.GetAt(5)->GetLength(),prec2+5); // assuming values of this column are maximum prec2 + 5 chars long
			os.width(wd6);
			os << colHeads.GetAt(5)->GetChar() << width;

			//os.precision(prec2);
			wd7 = max(colHeads.GetAt(6)->GetLength(),prec2+5); // assuming values of this column are maximum prec2 + 5 chars long
			os.width(wd7);
			os << colHeads.GetAt(6)->GetChar() << width;

			//os.precision(prec2);
			wd8 = max(colHeads.GetAt(7)->GetLength(),prec2+5); // assuming values of this column are maximum prec2 + 5 chars long
			os.width(wd8);
			os << colHeads.GetAt(7)->GetChar() << "\n";
			os << "=================================================================================================================================" << "\n";

			for (int i=0; i < points->GetSize(); i++)
			{
				CXYZPoint *p = points->getAt(i);
				if (p->getStatusBit(0))
				{
					CXYZPoint* r = (CXYZPoint*)residuals->getPoint(p->getLabel().GetChar());
					if (r)
					{
						if (p->getStatusBit(ADDED_CHECKPOINT) || p->getStatusBit(CHANGED_CONTROLPOINT))
						{
							ch = "Check";
						}
						else
						{
							ch = "GCP";
						}
						os.precision(3);
						os.width(wd1);
						os << p->getLabel().GetChar() << width;

						os.precision(3);
						os.width(wd2);
						os << ch << width;

						os.precision(prec1);
						os.width(wd3);
						os << p->x << width;

						os.precision(prec1);
						os.width(wd4);
						os << p->y << width;

						os.precision(prec2);
						os.width(wd5);
						os << p->z << width;

						os.precision(prec2);
						os.width(wd6);
						os << r->x*ratio << width;

						os.precision(prec2);
						os.width(wd7);
						os << r->y*ratio << width;

						os.precision(prec2);
						os.width(wd8);
						os << r->z << "\n";
					}
				}
			}
			os.close();
		}
	}
}



void Bui_AdjustDialog::guessHeaderFormat(const CXYZPointArray* points, CCharStringArray& colHeading, int &precision1, int &precision2, double &ratio)
{
	//CCharString width = "     ";
	if (points->getRefSys().getCoordinateType() == eGEOGRAPHIC)
	{
		colHeading.Add("Label");
		colHeading.Add("Type");
		colHeading.Add("Latitude");
		colHeading.Add("Longitude");
		colHeading.Add("Height");
		colHeading.Add("Res_X (m)");
		colHeading.Add("Res_Y (m)");
		colHeading.Add("Res_Z (m)");

		precision1 = 10;
		precision2 = 4;
		ratio = (M_PI * points->getRefSys().getSpheroid().getSemiAverage()) / 180.0;
		//pA->getReferenceSystem()->getSpheroid().getSemiAverage()
	}
	else if ( points->getRefSys().getCoordinateType() == eGEOCENTRIC)
	{
		colHeading.Add("Label");
		colHeading.Add("Type");
		colHeading.Add("X");
		colHeading.Add("Y");
		colHeading.Add("Z");
		colHeading.Add("Res_X (m)");
		colHeading.Add("Res_Y (m)");
		colHeading.Add("Res_Z (m)");

		precision1 = 4;
		precision2 = 4;

		ratio = 1;
	}
	else if ( points->getRefSys().getCoordinateType() == eGRID)
	{
		CCharString p1("Label");
		if (points->getRefSys().isUTM())
		{
			p1 += "(Zone ";

			//char p2[3];
			CCharString p2;
			p2.Format("%d",points->getRefSys().getZone());
			//itoa(points->getRefSys().getZone(),p2,10);
			p1 = p1 + p2;
			if ( points->getRefSys().getHemisphere() == SOUTHERN )
			{
				p1 = p1 + ", Southern)";
			}
			else if ( points->getRefSys().getHemisphere() == NORTHERN )
			{
				p1 = p1 + ", Northern)";
			}
		}
		else
		{
			p1 += "(Lat:";
			double lon = points->getRefSys().getCentralMeridian();
			double lat = points->getRefSys().getLatitudeOrigin();

			char buf[256];
			sprintf(buf, "%5.2f Lon:%5.2f)", lat, lon);

			p1 = p1 + buf;
		}
		colHeading.Add(p1);
		colHeading.Add("Type");
		colHeading.Add("Easting");
		colHeading.Add("Northing");
		colHeading.Add("Height");
		colHeading.Add("Res_X (m)");
		colHeading.Add("Res_Y (m)");
		colHeading.Add("Res_Z (m)");

		precision1 = 4;
		precision2 = 4;
		ratio = 1;
	}
	else
	{ // undefined coordinate type
		colHeading.Add("Label");
		colHeading.Add("Type");
		colHeading.Add("Local_X");
		colHeading.Add("Local_Y");
		colHeading.Add("Local_Z");
		colHeading.Add("Res_X (m)");
		colHeading.Add("Res_Y (m)");
		colHeading.Add("Res_Z (m)");

		precision1 = 3;
		precision2 = 3;
		ratio = 1;
	}
}

void Bui_AdjustDialog::checkAdjustmentSettings()
{
	if (!this->imageDataModel->activeImages() || !this->lE_maxErrors->hasAcceptableInput() ||
		!this->lE_Threshold->hasAcceptableInput() || !this->obsPointDataModel->activePointArrays())
	{
		this->pB_Run->setEnabled(false);
	}
	else
	{
		this->pB_Run->setEnabled(true);
	}

	this->cB_Robust->blockSignals(true);
	if (this->robust)
	{
		this->cB_Robust->setChecked(true);
		this->lE_maxErrors->setEnabled(true);
		this->lE_Threshold->setEnabled(true);
	}
	else
	{
		this->cB_Robust->setChecked(false);
		this->lE_maxErrors->setEnabled(false);
		this->lE_Threshold->setEnabled(false);
	}
	this->cB_Robust->blockSignals(false);


	this->cB_Forward->blockSignals(true);
	this->cB_ExcludeTie->blockSignals(true);

	if (this->forward && !this->excludeTie)
	{
		this->cB_ExcludeTie->setEnabled(false);
		this->cB_Forward->setEnabled(true);
	}
	else if (!this->forward && this->excludeTie)
	{
		this->cB_ExcludeTie->setEnabled(true);
		this->cB_Forward->setEnabled(false);
	}
	else if (!this->forward && !this->excludeTie)
	{
		this->cB_ExcludeTie->setEnabled(true);
		this->cB_Forward->setEnabled(true);
	}
	else // this case is not allowed, only forward or exclude tie!!
	{
		// just change one of the two bool variables
		this->excludeTie = false;

		this->cB_Forward->blockSignals(false);
		this->cB_ExcludeTie->blockSignals(false);
		this->checkAdjustmentSettings();
	}


	this->cB_Forward->blockSignals(false);
	this->cB_ExcludeTie->blockSignals(false);


}



void Bui_AdjustDialog::applySettings()
{
	this->checkAdjustmentSettings();

	this->lE_maxErrors->setText(QString("%1").arg(this->maxRobustPercentage*100.0));
	this->lE_Threshold->setText(QString().sprintf("%.1lf",this->minRobFac));

	if (this->excludeTie && !this->cB_ExcludeTie->isChecked())
	{
		this->cB_ExcludeTie->blockSignals(true);
		this->cB_ExcludeTie->setChecked(true);
		this->cB_ExcludeTie->blockSignals(false);

	}

	if (this->forward && !this->cB_Forward->isChecked())
	{
		this->cB_Forward->blockSignals(true);
		this->cB_Forward->setChecked(true);
		this->cB_Forward->blockSignals(false);

	}

}

void Bui_AdjustDialog::cBRobustStateChanged(int state)
{
	this->robust = state;
	this->checkAdjustmentSettings();
}

void Bui_AdjustDialog::cBForwardStateChanged(int state)
{
	this->forward = state;

	if (this->forward && this->excludeTie)
		this->excludeTie = false;

	this->checkAdjustmentSettings();
}

void Bui_AdjustDialog::cBExcludeTieStateChanged(int state)
{
	this->excludeTie = state;

	if (this->excludeTie && this->forward)
		this->forward = false;

	this->checkAdjustmentSettings();
}

void Bui_AdjustDialog::textChanged(const QString &text )
{
	this->checkAdjustmentSettings();
}



void Bui_AdjustDialog::editingFinishedMaxRobust()
{
	if (this->lE_maxErrors->hasAcceptableInput())
		this->maxRobustPercentage = this->lE_maxErrors->text().toDouble()/100.0;

	this->applySettings();
}


void Bui_AdjustDialog::editingFinishedMinRobFac()
{
	if (this->lE_Threshold->hasAcceptableInput())
		this->minRobFac = this->lE_Threshold->text().toDouble();

	this->applySettings();
}

void Bui_AdjustDialog::filteredImageClicked(const QModelIndex& index)
{
	this->imageDataModel->itemClicked(this->imageFilterModel->mapToSource(index));
}



void Bui_AdjustDialog::filteredPointArrayClicked(const QModelIndex& index)
{
	this->obsPointDataModel->itemClicked(this->obsPointFilterModel->mapToSource(index));
}

void Bui_AdjustDialog::filteredPointArrayRightClicked(const QModelIndex& index)
{
	this->obsPointDataModel->itemRightClicked(this->obsPointFilterModel->mapToSource(index));
}



void Bui_AdjustDialog::resetRobust()
{
	project.resetRobustErrors();

}


bool Bui_AdjustDialog::doQualityAssessment(CBaristaBundleHelper &helper, CObservationHandlerPtrArray *images)
{
	bool ret = true;

	int nCheckPointsUsed = 0;
	vector<C3DPoint> maxResCheckPoints;
	vector<C3DPoint> rmsCheckPoints;
	vector<int> nStationCheckPoints;
	CXYZPointArray* checkPoints = project.getCheckPoints();
	CXYZPointArray* controlPoints = 0;
	if (project.getControlPoints()) controlPoints = project.getControlPoints()->getXYZPointArray();

	CXYZPointArray* qualityResult = project.getPointResidualArrayOfXYZPointArray(checkPoints);

	if (!checkPoints || !controlPoints || !qualityResult || !this->bundleResult || !images)
		ret = false;
	else
	{
		qualityResult->RemoveAll();

		for (int i=0; i < controlPoints->GetSize(); i++)
		{
			CObsPoint* p_control = controlPoints->GetAt(i);

			// only if active control point
			if (!p_control->getStatusBit(0))
				continue;

			CObsPoint* p_check = checkPoints->getPoint(p_control->getLabel().GetChar());


			if (p_check)
			{
				// disable all check points which are also control points and
				// have not been added to the control point array as check point
				if (!p_control->getStatusBit(ADDED_CHECKPOINT) && !p_control->getStatusBit(CHANGED_CONTROLPOINT))
					p_check->deactivateStatusBit(0);
				else
					nCheckPointsUsed++;
			}
		}

		// do 2D assessment for every active image
		for (int i=0, j=0; i < images->GetSize(); i++)
		{
			CVImage* image = (CVImage*)images->GetAt(i);
			if (helper.getIndexOfObsHandler(image) >= 0)
			{
				maxResCheckPoints.push_back(C3DPoint());
				rmsCheckPoints.push_back(C3DPoint());
				nStationCheckPoints.push_back(0);

				CSensorModel* smodel = image->getCurrentSensorModel();

				if (smodel)
				{
					CTrans3D* trans = CTrans3DFactory::initTransformation(checkPoints->getRefSys(),smodel->getRefSys());

					if (trans)
					{
						CXYZPointArray tmpCheck;
						trans->transformArray(&tmpCheck,checkPoints);

						CXYPointArray* xy = image->getProjectedXYPoints();
						smodel->transformArrayObj2Obs(*xy, tmpCheck);

						// fix the status bit
						for (int k=0; k < xy->GetSize() && k < checkPoints->GetSize(); k++)
						{
							xy->GetAt(k)->setStatus(checkPoints->GetAt(k)->getStatus());
						}

						xy->setFileName("3Dto2D");

						CXYPointArray* differences = image->getDifferencedXYPoints();
						differences->RemoveAll();
						xy->differenceByLabel(differences,image->getXYPoints());
						differences->setFileName("XYdiff");
						this->getQualityValues(differences,nStationCheckPoints[j],rmsCheckPoints[j],maxResCheckPoints[j]);
					}
				}

				++j;
			}
		}

		// do 3D assessment
		project.differenceByLabelXYZ(qualityResult,checkPoints,this->bundleResult,true);
		maxResCheckPoints.push_back(C3DPoint());
		rmsCheckPoints.push_back(C3DPoint());
		nStationCheckPoints.push_back(0);

		int index = nStationCheckPoints.size() - 1;
		this->getQualityValues(qualityResult,nStationCheckPoints[index],rmsCheckPoints[index],maxResCheckPoints[index]);
	}

	CReferenceSystem refSysCheckPoints = checkPoints->getRefSys();
	helper.setQualityAssessmentData(nCheckPointsUsed,maxResCheckPoints,rmsCheckPoints,nStationCheckPoints,refSysCheckPoints);;

	return ret;
}

void Bui_AdjustDialog::getQualityValues(CObsPointArray* diffArray,int& nPoints,C3DPoint& rmsValues,C3DPoint& maxResValues)
{
	if (diffArray && diffArray->GetSize())
	{
		nPoints = diffArray->GetSize() - 1;
		CObsPoint* rmsPoint = diffArray->getPoint("RMS");

		if (rmsPoint)
		{
			rmsValues.x = (*rmsPoint)[0];
			rmsValues.y = (*rmsPoint)[1];

			if (diffArray->getDimension() == 3)
				rmsValues.z = (*rmsPoint)[2];

			maxResValues.x = -1.0;
			maxResValues.y = -1.0;
			maxResValues.z = -1.0;



			for (int i=0; i < diffArray->GetSize(); i++)
			{
				CObsPoint* p = diffArray->GetAt(i);

				if (p == rmsPoint)
					continue;

				if (fabs((*p)[0]) > maxResValues.x)
					maxResValues.x = fabs((*p)[0]);

				if (fabs((*p)[1]) > maxResValues.y)
					maxResValues.y = fabs((*p)[1]);

				if (diffArray->getDimension() == 3 && fabs((*p)[2]) > maxResValues.z)
					maxResValues.z = fabs((*p)[2]);
			}


		}
	}
}




// ###############################################################################################
// ###############################################################################################
// ###############################################################################################


CImageDataModel::CImageDataModel(QObject* parent)
	:QAbstractTableModel(parent), currentImage(-1)
{

	// init the local data;
	ImageDataElements element;
	for (int i=0; i< project.getNumberOfImages(); i++)
	{
		CVImage* image =project.getImageAt(i);
		if (!image->getCurrentSensorModel())
			continue;

		// save the name
		element.name = image->getName();

		// create the thumbnail
		if (image->hasValidHugeImage())
		{
			CHugeImage* himage =project.getImageAt(i)->getHugeImage();
			QImage *tmp = QTImageHelper::createQImage(himage, 0,himage->getHeight(), 0, himage->getWidth(),ROW_HEIGHT * N_SENSORMODELS , -1);
			element.thumbnail = QPixmap::fromImage(*tmp);
			delete tmp;
		}
		else
		{
			element.thumbnail = QTImageHelper::createNoFileFoundPixmap();
		}


		// show all images with a sensor model
		element.show = true;
		element.isSelected = true;

		// select always the first one
		if (this->currentImage == -1)
		{
			this->currentImage = 0;
			element.isCurrent = true;
		}
		else
			element.isCurrent= false;

		element.hasPushBroom = image->hasPushbroom();
		element.hasRPC = image->hasRPC();
		element.hasAffine = image->hasAffine();
		element.hasFrame = image->hasFrameCamera();

		// save the active sensor model type
		if (image->getCurrentSensorModelType() == ePUSHBROOM)
			element.activeModelType = 0;
		else if (image->getCurrentSensorModelType() == eRPC)
			element.activeModelType = 1;
		else if (image->getCurrentSensorModelType() == eAFFINE)
			element.activeModelType = 2;
		else if (image->getCurrentSensorModelType() == eFRAMECAMERA)
			element.activeModelType = 3;
		else
			element.activeModelType = 0;

		// and the image
		element.image = image;

		this->localData.push_back(element);

	}



}

int CImageDataModel::rowCount(const QModelIndex& parent) const
{
	return this->localData.size() * IMAGE_SPAN;
}

int CImageDataModel::activeImages() const
{
	int count =0;
	for (unsigned int i=0; i< this->localData.size(); i++)
	{
		if (this->localData[i].show && this->localData[i].isSelected)
			count ++;
	}
	return count;
}

void CImageDataModel::getActiveObservationHandler(CObservationHandlerPtrArray *images,CObservationHandlerPtrArray *orbits)
{

	bool hasPushBroom = false;
	vector<COrbitObservations*> neededOrbits;
	int nOrbits = project.getNumberOfOrbits();

	images->RemoveAll(); //make the image list empty first


	// add selected and shown images only
	for (unsigned int i=0; i< this->localData.size(); i++)
	{
		if (this->localData[i].show && this->localData[i].isSelected)
		{
			images->Add(this->localData[i].image);
			if (this->localData[i].activeModelType == 0) // pushbroom
			{
				hasPushBroom = true;

				// save all orbits we need
				for (int k=0; k< nOrbits; k++)
				{
					COrbitObservations* orbit = project.getOrbitObservationAt(k);
					if (orbit->getCurrentSensorModel() == this->localData[i].image->getPushbroom()->getOrbitPath() ||
						orbit->getCurrentSensorModel() == this->localData[i].image->getPushbroom()->getOrbitAttitudes())
						neededOrbits.push_back(orbit);
				}
			}
		}
	}


	// we only want the same orbit once
	if (hasPushBroom)
	{
		for (int i=0;i<nOrbits; i++)
		{
			COrbitObservations* orbit = project.getOrbitObservationAt(i);
			for (unsigned int k=0; k<neededOrbits.size(); k++)
			{
				if (orbit == neededOrbits[k])
				{
					orbits->Add(orbit);
					break;
				}
			}

		}

	}


}


int CImageDataModel::columnCount(const QModelIndex& parent) const
{
	return 3;
}

QVariant CImageDataModel::headerData(int section,Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
	{
		if (section == 0)
			return "use";
		else if (section == 1)
			return "Image";
		else if (section == 2)
			return "Sensor";
	}
	else if (role == Qt::SizeHintRole && orientation == Qt::Horizontal)
	{
		if (section == 0)
			return QSize(10,20);
		else if (section == 1)
			return QSize(10,20);
		else if (section == 2)
			return QSize(10,20);
	}
	return QVariant();
}

QVariant CImageDataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	int row =index.row() % IMAGE_SPAN;
	int element = index.row() / IMAGE_SPAN;

	if (role == FilterRole)
	{

		return this->localData[element].show ? "show" : "-";
	}
	else if (role == Qt::DecorationRole)
	{

		if (index.column() ==1)
			return  this->localData[element].thumbnail;

	}
	else if (role == Qt::TextAlignmentRole)
	{
		if (row == N_SENSORMODELS)
			return Qt::AlignCenter;
	}
	else if (role == Qt::DisplayRole)
	{
		if (row == N_SENSORMODELS)
			return this->localData[element].name;

		if (index.column() == 0)
			return this->localData[element].isSelected;

		if (index.column() == 2)
		{
			if (row == 0 && this->localData[element].hasPushBroom)
				return QString("PushBroom");
			else if (row == 1 && this->localData[element].hasRPC)
				return QString("RPC");
			else if ( row == 2 && this->localData[element].hasAffine)
				return QString("Affine");
			else if (row ==3 && this->localData[element].hasFrame)
				return QString("Frame");
		}
	}
	else if (role == Qt::BackgroundRole)
	{
		if ( index.column() == 2 && row == this->localData[element].activeModelType)
			return QColor(Qt::red);
		else if (this->localData[element].isCurrent)
			return QColor(230,230,230);
		else
			return QColor(Qt::white);
	}
	else if (role == Qt::ToolTipRole)
	{
		return this->localData[element].name;
	}


	return QVariant();
}

Qt::ItemFlags CImageDataModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
	 return Qt::ItemIsEnabled;

	return Qt::ItemFlags(QAbstractItemModel::flags(index) - Qt::ItemIsSelectable);
}


void CImageDataModel::itemClicked( const QModelIndex& index)
{
	if (!index.isValid())
		return;

	int row = index.row() % IMAGE_SPAN;
	int element = index.row() / IMAGE_SPAN;

	bool elementsChanged = false;

	// swap selection
	if (index.column() == 0 && row != N_SENSORMODELS)
	{
		this->localData[element].isSelected = !this->localData[element].isSelected;
		elementsChanged = true;
		emit this->selectionChaged();
	}
	else if (index.column() == 2) // change sensor model
	{
		if ( (!this->localData[element].hasPushBroom && row ==0) ||
			 (!this->localData[element].hasRPC && row == 1) ||
			 (!this->localData[element].hasAffine && row == 2) ||
			 (!this->localData[element].hasFrame && row == 3) ||
			 (row == this->localData[element].activeModelType))
		{
			// ignore click on already active sensor model
		}
		else
		{

			// force the dialog to save its data first so we can see the changes in other dialogs
			emit forceToSave();

			this->localData[element].activeModelType = row;

			if (row == 0)
				this->localData[element].image->setCurrentSensorModel(ePUSHBROOM);
			else if (row == 1)
				this->localData[element].image->setCurrentSensorModel(eRPC);
			else if (row == 2)
				this->localData[element].image->setCurrentSensorModel(eAFFINE);
			else if (row == 3)
				this->localData[element].image->setCurrentSensorModel(eFRAMECAMERA);

			elementsChanged = true;
		}
	}

	// change current image
	if (!this->localData[element].isCurrent && !(index.column() == 0 && row != N_SENSORMODELS) )
	{
		// force the dialog to save its data first so we can see the changes in other dialogs
		emit forceToSave();

		this->localData[this->currentImage].isCurrent = false;
		this->localData[element].isCurrent = true;
		this->currentImage = element;

		emit selectedVImageChanged(this->localData[element].image);
		elementsChanged = true;
	}


	if (elementsChanged) // force view to update
	{
		QModelIndex i1 = this->index(0,0);
		QModelIndex i2 = this->index((element+1)*IMAGE_SPAN -1 ,2);
		emit this->dataChanged(i1,i2);
	}


}

void CImageDataModel::selectCurrentImage()
{
	if (this->currentImage >=0 && this->currentImage < int(this->localData.size()))
		emit selectedVImageChanged(this->localData[this->currentImage].image);
}


void CImageDataModel::applyImageFilter(bool showPushBroom,bool showRpc,bool showAffine,bool showFrame)
{

	bool show = false;
	this->beginResetModel();
	for (unsigned int i=0; i< this->localData.size(); i++)
	{
		if (this->localData[i].hasPushBroom &&  showPushBroom)
		{
			this->localData[i].show = true;
			continue;
		}
		else if (this->localData[i].hasRPC && showRpc)
		{
			this->localData[i].show = true;
			continue;
		}
		else if (this->localData[i].hasAffine && showAffine)
		{
			this->localData[i].show = true;
			continue;
		}
		else if (this->localData[i].hasFrame && showFrame)
		{
			this->localData[i].show = true;
			continue;
		}
		else
		{
			this->localData[i].show = false;
		}

	}

	this->endResetModel();
}

// ###############################################################################################
// ###############################################################################################
// ###############################################################################################

CObsPointArrayDataModel::CObsPointArrayDataModel(QObject* parent): currentArray(-1),
	currentControlArray(-1),currentCheckPointArray(-1)
{
	this->initModel();
}

void CObsPointArrayDataModel::initModel()
{
	this->beginResetModel();
	this->currentArray = -1;
	this->currentControlArray = -1;
	this->currentCheckPointArray = -1;

	this->localData.clear();

	ObsPointDataElements element;

	for (int i=0; i<project.getNumberOfXYZPointArrays(); i++)
	{
		CXYZPointArray* pA = project.getPointArrayAt(i);
		CXYZPointArray* rA = project.getPointResidualArrayAt(i);

		element.name = pA->getItemText().GetChar();
		element.show = true;
		element.isControl = pA->isControl();
		element.isCheckPoint = pA->isCheckPoint();
		element.pointArray = pA;
		element.residualArray = rA;

		int type = pA->getRefSys().getCoordinateType();
		if (type == eGEOGRAPHIC || type == eGEOCENTRIC || type == eGRID)
			element.type =type;
		else
			continue;

		if (element.isControl)
		{
			this->currentArray = this->localData.size();
			this->currentControlArray = this->localData.size();
		}

		if (element.isCheckPoint)
			this->currentCheckPointArray = this->localData.size();

		this->localData.push_back(element);
	}
	this->endResetModel();
}

int CObsPointArrayDataModel::rowCount(const QModelIndex& parent) const
{

	return this->localData.size();
}

int CObsPointArrayDataModel::columnCount(const QModelIndex& parent) const
{
	return 1;
}

QVariant CObsPointArrayDataModel::headerData(int section,Qt::Orientation orientation, int role) const
{
	return QString("XYZ Points");
}

QVariant CObsPointArrayDataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role == FilterRole)
	{
		return this->localData[index.row()].show ? "show" : "-";
	}
	else if (role == Qt::DecorationRole)
	{
		return  QPixmap::fromImage(QTImageHelper::makeIcon(this->localData[index.row()].pointArray));
	}
	else if (role == Qt::TextAlignmentRole)
	{
		return int(Qt::AlignRight | Qt::AlignVCenter);
	}
	else if (role == Qt::DisplayRole)
	{
		return this->localData[index.row()].name;
	}
	else if (role == Qt::TextColorRole)
	{
		if (this->localData[index.row()].isControl)
			return QColor(Qt::red);
		else if (this->localData[index.row()].isCheckPoint)
			return QColor(Qt::blue);
		else
			return QColor(Qt::black);
	}
	else if (role == Qt::ToolTipRole)
	{
		return this->localData[index.row()].name;
	}
	else if (role == Qt::BackgroundRole)
	{
		if (this->currentArray >=0 && index.row() == this->currentArray)
			return QColor(230,230,230);
		else
			return QColor(Qt::white);
	}

	return QVariant();
}

Qt::ItemFlags CObsPointArrayDataModel::flags(const QModelIndex &index) const
{
	return Qt::ItemFlags(QAbstractItemModel::flags(index)- Qt::ItemIsSelectable);
}

bool CObsPointArrayDataModel::setData(const QModelIndex &index, const QVariant &value,int role)
{
	return false;
}


void CObsPointArrayDataModel::itemClicked( const QModelIndex& index)
{
	if (!index.isValid())
		return;

	// already selected
	if (this->currentArray >=0 && this->currentArray  == index.row())
		return;
	else
	{
		this->beginResetModel();
		this->currentArray = index.row();
		emit selectedObsPointArrayChanged(this->localData[index.row()].pointArray,
										  this->localData[index.row()].residualArray);
		this->endResetModel();
	}
}

void CObsPointArrayDataModel::itemRightClicked(const QModelIndex& index)
{
	if (!index.isValid())
		return;


	QMenu* contextMenu = new QMenu(0);
	Q_CHECK_PTR( contextMenu );

	QAction* control = NULL;
	QAction* check = NULL;

	if (this->localData[index.row()].isControl)
	{
		control = new QAction("unset Control",this);
		connect(control, SIGNAL(triggered()), this, SLOT(unsetControl()));
	}
	else if (this->localData[index.row()].isCheckPoint)
	{
		check = new QAction("unset CheckPoints",this);
		connect(check, SIGNAL(triggered()), this, SLOT(unsetCheckPoint()));
	}
	else
	{
		control = new QAction("set Control",this);
		connect(control, SIGNAL(triggered()), this, SLOT(setControl()));

		check = new QAction("set CheckPoints",this);
		connect(check, SIGNAL(triggered()), this, SLOT(setCheckPoint()));
	}


	this->selectedIndex = index;

	if (control)
		contextMenu->addAction(control);
	if (check)
		contextMenu->addAction(check);

	contextMenu->exec( QCursor::pos() );

	delete control;
	delete check;
	delete contextMenu;

}

void CObsPointArrayDataModel::unsetControl()
{

	if (this->currentArray >=0 && !this->localData[this->currentArray].isControl)
		return;
	else
	{
		this->beginResetModel();
		if (this->currentControlArray >=0 )
			this->localData[this->currentControlArray].isControl = false;


		this->currentControlArray = -1;

		// tell the project
		project.unsetControlPoints((CXYZPointArray*)this->localData[this->selectedIndex.row()].pointArray);

		emit selectedObsPointArrayChanged(this->localData[this->selectedIndex.row()].pointArray,
										  this->localData[this->selectedIndex.row()].residualArray);
		this->endResetModel();
	}
}

void CObsPointArrayDataModel::setControl()
{

	// already the control file
	if (this->currentArray >=0 && this->localData[this->currentArray].isControl)
		return;
	else
	{
		this->beginResetModel();
		if (this->currentControlArray >=0 )
			this->localData[this->currentControlArray].isControl = false;

		this->localData[this->selectedIndex.row()].isControl = true;
		this->currentControlArray = this->selectedIndex.row();

		// tell the project
		project.setControlPoints((CXYZPointArray*)this->localData[this->selectedIndex.row()].pointArray);

		emit selectedObsPointArrayChanged(this->localData[this->selectedIndex.row()].pointArray,
										  this->localData[this->selectedIndex.row()].residualArray);
		this->endResetModel();
	}

}


void CObsPointArrayDataModel::unsetCheckPoint()
{

	if (this->currentArray >=0 && !this->localData[this->currentArray].isCheckPoint)
		return;
	else
	{
		this->beginResetModel();
		if (this->currentCheckPointArray >=0 )
			this->localData[this->currentCheckPointArray].isCheckPoint = false;


		this->currentCheckPointArray = -1;

		// tell the project
		project.unsetCheckPoints((CXYZPointArray*)this->localData[this->selectedIndex.row()].pointArray);

		emit selectedObsPointArrayChanged(this->localData[this->selectedIndex.row()].pointArray,
										  this->localData[this->selectedIndex.row()].residualArray);
		this->endResetModel();
	}
}

void CObsPointArrayDataModel::setCheckPoint()
{
	// already the check Point file
	if (this->currentArray >=0 && this->localData[this->currentArray].isCheckPoint)
		return;
	else
	{
		this->beginResetModel();
		if (this->currentCheckPointArray >=0 )
			this->localData[this->currentCheckPointArray].isCheckPoint = false;

		this->localData[this->selectedIndex.row()].isCheckPoint = true;
		this->currentCheckPointArray = this->selectedIndex.row();

		// tell the project
		project.setCheckPoints((CXYZPointArray*)this->localData[this->selectedIndex.row()].pointArray);

		emit selectedObsPointArrayChanged(this->localData[this->selectedIndex.row()].pointArray,
										  this->localData[this->selectedIndex.row()].residualArray);
		this->endResetModel();
	}
}

void CObsPointArrayDataModel::selectCurrentPointArray()
{
	if (this->currentArray >=0 && this->currentArray < int(this->localData.size()))
		emit selectedObsPointArrayChanged(this->localData[this->currentArray].pointArray,
											this->localData[this->currentArray].residualArray);
}


void CObsPointArrayDataModel::applyPointFilter(bool showGeographic,bool showGeocentric,bool showGrid)
{
	bool show = false;
	this->beginResetModel();
	for (unsigned int i=0; i< this->localData.size(); i++)
	{
		if (this->localData[i].type == eGEOGRAPHIC && showGeographic)
		{
			this->localData[i].show = true;
			continue;
		}
		else if (this->localData[i].type == eGEOCENTRIC && showGeocentric)
		{
			this->localData[i].show = true;
			continue;
		}
		else if (this->localData[i].type == eGRID && showGrid)
		{
			this->localData[i].show = true;
			continue;
		}
		else
		{
			this->localData[i].show = false;
		}

	}

	this->endResetModel();
}

int CObsPointArrayDataModel::activePointArrays() const
{
	int count =0;
	for (unsigned int i=0; i< this->localData.size(); i++)
	{
		if (this->localData[i].show)
			count ++;
	}
	return count;
}













// ###############################################################################################
// ###############################################################################################
// ###############################################################################################

void CImageTableDelegate::paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const
{
	QString text;
	QStyleOptionViewItem localOptions = option;

	localOptions.displayAlignment = Qt::AlignLeft | Qt::AlignVCenter;

	QColor bgColor =index.model()->data(index,Qt::BackgroundRole).value<QColor>();
	localOptions.palette.setBrush(QPalette::Window,bgColor);

	painter->fillRect(localOptions.rect, localOptions.palette.background());

	int row = index.row() % IMAGE_SPAN;

	if (row == N_SENSORMODELS)
	{

		localOptions.displayAlignment =  option.displayAlignment | Qt::AlignCenter;
		QString text = index.model()->data(index,Qt::DisplayRole).toString();

		this->drawDisplay(painter,localOptions,localOptions.rect,text);
		//drawFocus(painter,localOptions,localOptions.rect);

	}
	else  if (index.column() == 0)
	{
		if	(index.model()->data(index,Qt::DisplayRole).toBool())
			this->drawCheck(painter,localOptions,localOptions.rect,Qt::Checked);
		else
			this->drawCheck(painter,localOptions,localOptions.rect,Qt::Unchecked);
	}
	else if (index.column() == 1)
	{
		QVariant v = index.model()->data(index,Qt::DecorationRole);
		if (v.canConvert<QPixmap>())
		{
			QPixmap p = v.value<QPixmap>();
			this->drawDecoration(painter,localOptions,localOptions.rect,p);
		}
	}
	else if (index.column() == 2)
	{
		QString text = index.model()->data(index,Qt::DisplayRole).toString();
		this->drawDisplay(painter,localOptions,localOptions.rect,text);
	}

}



// ###############################################################################################
// ###############################################################################################
// ###############################################################################################

CCheckBoxWithTextDelegate::CCheckBoxWithTextDelegate(QObject *parent)
	: QItemDelegate(parent)
{

}


void CCheckBoxWithTextDelegate::paint(QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex &index) const
{

	QStyleOptionViewItem localOptions = option;
	localOptions.displayAlignment = Qt::AlignRight | Qt::AlignVCenter;

	bool selected = index.model()->data(index,Qt::UserRole).toBool();
	QString text = index.model()->data(index,Qt::DisplayRole).toString();

	if (selected)
	{
		this->drawCheck(painter,option,option.rect,Qt::Checked);
		this->drawDisplay(painter,option,option.rect,text);
	}
	else
	{
		this->drawCheck(painter,option,option.rect,Qt::Unchecked);
		this->drawDisplay(painter,option,option.rect,text);

	}

	drawFocus(painter,localOptions,localOptions.rect);


}



// ###############################################################################################
// ###############################################################################################
// ###############################################################################################

void CPointTableView::contextMenuEvent(QContextMenuEvent * e)
{
	QModelIndex index = this->indexAt(e->pos());
	emit contextMenuCalled(index);
}
