#include <strstream>
using namespace std;


#ifdef WIN32
#include "vraniml.h"
#endif


#include "BuildingBundleManager.h"
#include "Building.h"
#include "BaristaProject.h"
#include "VImage.h"
#include "GenericObservationHandler.h"
#include "3DAdjustingPlane.h"
#include "PlaneObservationHandler.h"

CBuildingBundleManager::CBuildingBundleManager(CBuilding *building, float I_convergenceLimit, 
											   float MaxRobustPercentage, float MinRobFac, 
											   float havg, const CCharString &protfile) : 
			CBundleManager(I_convergenceLimit, MaxRobustPercentage, MinRobFac, havg, protfile),
			pBuilding(building), ERP(0.0,0.0,0.0)
{
}

CBuildingBundleManager::~CBuildingBundleManager()
{
	for (int i = 0; i < obsVec.size(); ++i) 
		delete obsVec[i];
}

void CBuildingBundleManager::bundleInit(bool forwardOnly)
{
	// initialise observation handlers first, then use CBuildingBundleManager::bundleInit
#ifdef WIN32
	this->nPointPars = 0;
	this->ObjectPoints.RemoveAll();
	ERP.x = 0.0;
	ERP.y = 0.0;
	ERP.z = FLT_MAX;
	vrSolid *solid = 0; 

	if (this->pBuilding) solid = this->pBuilding->getSolid();

	if (solid)
	{
		LISTPOS vPos = solid->verts.GetHeadPosition();
		while (vPos)
		{
			vrVertex *v = solid->verts.GetNext(vPos);
			CObsPoint *p = this->pBuilding->getVertices().getAt(v->GetId());
			CObsPoint *ObjectPointsPoint = this->ObjectPoints.Add(*p);
			this->nPointPars += 3;
			ERP.x += (*p)[0];
			ERP.y += (*p)[1];
			if ((*p)[2] < ERP.z) ERP.z = (*p)[2];
		}

		ERP.x /= double(this->ObjectPoints.GetSize());
		ERP.y /= double(this->ObjectPoints.GetSize());

		if (ObjectPoints.GetSize() > 3)
		{
			this->ObjectPoints.sortByLabel();

			for (int i = 0; i < project.getNumberOfImages() ; ++i)
			{
				CVImage *img = project.getImageAt(i);
				CSensorModel *sensorMod = img->getCurrentSensorModel();
				CXYPointArray *pImgPoints = img->getMonoPlottedPoints();

				if (sensorMod && (pImgPoints->GetSize() > 0))
				{
					CGenericObservationHandler *oh = 0;

					for (int j = 0; j < pImgPoints->GetSize() && !oh; ++j)
					{
						if (this->ObjectPoints.getPoint_binarySearch(pImgPoints->GetAt(j)->getLabel()))
						{
							oh = new CGenericObservationHandler(pImgPoints, sensorMod, img->getFileNameChar(), false);
							obsVec.push_back(oh);
							this->addObservations(oh);
							sensorMod->setParameterCount(CSensorModel::shiftPar, 0);
							sensorMod->setParameterCount(CSensorModel::rotPar, 0);
							sensorMod->setParameterCount(CSensorModel::scalePar, 0);
							sensorMod->setParameterCount(CSensorModel::mountShiftPar, 0);
							sensorMod->setParameterCount(CSensorModel::mountRotPar, 0);
							sensorMod->setParameterCount(CSensorModel::interiorPar, 0);
							sensorMod->setParameterCount(CSensorModel::addPar, 0);
							sensorMod->setParameterCount(CSensorModel::constraints, 0);
						}
					}
				}
			}

			LISTPOS fPos = solid->faces.GetHeadPosition();
			while (fPos)
			{
				vrFace *f = solid->faces.GetNext(fPos);

				ostrstream oss;
				ePlaneEquationType type;
				int Parameterisation = C3DAdjustingPlane::constCoeff;
				double Sigma = 0.01f;
				int MirrorType = 0;
				SFColor color = f->GetColor(vrWhite);
				if (color == vrCornflowerBlue) 
				{ // floor
					type = eWPlaneEquation;
					Parameterisation = 0;
					oss << "FLOOR_";
				}
				else if (color == vrFirebrick)
				{ // roof
					type = eWPlaneEquation;
					oss << "ROOF_";
				}
				else
				{ // wall;
					oss << "WALL_";
					if (fabs(f->GetNormal().x) > fabs(f->GetNormal().y))
					{
						type = eUPlaneEquation;
						Parameterisation += C3DAdjustingPlane::linVCoeff;
					}
					else
					{
						type = eVPlaneEquation;
						Parameterisation += C3DAdjustingPlane::linUCoeff;
					}
					
				}
				oss << f->GetId() << ends;
				char *fn = oss.str();
				CCharString Filename(fn);
				delete [] fn;

				CPlaneObservationHandler *oh = new CPlaneObservationHandler(Filename);
				C3DAdjustingPlane *plane = oh->getPlane();
				plane->init(&ERP, &ROT, type, Parameterisation, Sigma, MirrorType);
				CObsPointArray *obsPoints = oh->getObsPoints();

				LISTPOS lPos = f->loops.GetHeadPosition();
				while (lPos)
				{
					vrLoop *l = f->loops.GetNext(lPos);
					LISTPOS hePos = l->halfedges.GetHeadPosition();
					while(hePos)
					{
						vrHalfEdge *he = l->halfedges.GetNext(hePos);
						CObsPoint *p = this->pBuilding->getVertices().getAt(he->GetVertex()->GetId());
						CObsPoint *pPlane = obsPoints->Add();
						pPlane->setLabel(p->getLabel());
						(*pPlane)[0] = 0;
					}
				}

				if (obsPoints->GetSize() < 1)
				{
					delete oh;
				}
				else
				{
					obsVec.push_back(oh);
					this->addObservations(oh);
					plane->setParameterCount(CSensorModel::shiftPar, 0);
					plane->setParameterCount(CSensorModel::rotPar, 0);
					plane->setParameterCount(CSensorModel::scalePar, 0);
					plane->setParameterCount(CSensorModel::mountShiftPar, 0);
					plane->setParameterCount(CSensorModel::mountRotPar, 0); 
					plane->setParameterCount(CSensorModel::interiorPar, 0);
				}
			}

			CBundleManager::bundleInit(false,false);
		}
	}
#endif	
}

/**
* Builds the Observation Matrix and Covariance Matrix "Q", ObsLabels are
* the String labels of the observations. They coorespond by index to the
* observations.
*/
void CBuildingBundleManager::initObservations()
{
	maxResX.resize(this->getNStations());
	maxResY.resize(this->getNStations());
	maxResZ.resize(this->getNStations());
	maxRobX.resize(this->getNStations());
	maxRobY.resize(this->getNStations());
	maxRobZ.resize(this->getNStations());
	maxResLabelX.resize(this->getNStations());
	maxResLabelY.resize(this->getNStations());
	maxResLabelZ.resize(this->getNStations());
	maxRobLabelX.resize(this->getNStations());
	maxRobLabelY.resize(this->getNStations());
	maxRobLabelZ.resize(this->getNStations());

	RMS.resize(this->getNStations());
	NPointsX.resize(this->getNStations());;
	NPointsY.resize(this->getNStations());;
	NPointsZ.resize(this->getNStations());;

	this->nGrossErrors = 0;
	this->nPointObs = 0;
	this->nConstraints = 0;
	this->nDirectObs = 0;
	

	if (this->getNStations() > 0)
	{
		for (int i = 0; i < this->getNStations(); ++i)
		{
			CObservationHandler* oh = ObservationHandlers.GetAt(i);
			oh->resetIndices();
			this->nConstraints += oh->getNConstraints();
			CObsPointArray *obsPoints = oh->getObsPoints();
			for (int j = 0; j < obsPoints->GetSize(); ++j)
			{
				CObsPoint *p = obsPoints->GetAt(j);
				p->id = -1;
				for (int k = 0; k < this->ObjectPoints.GetSize() && p->id < 0; ++k)
				{
					CObsPoint pObj = this->ObjectPoints.GetAt(k);
					if (pObj.getLabel() == p->getLabel())
					{
						p->id = k;
						this->nPointObs += oh->getDimension();
					}
				}
			}
		}
	}
}


void CBuildingBundleManager::initParameters(bool forwardOnly)
{
	nDirectObs = 0;
	// to have at least one station
	if (this->getNStations() > 0)
	{
		// Set up parameter approximations
		this->solution.ReSize(this->getNUnknowns() + this->getNConstraints(), 1);
        this->solution = 0.0;

		int offset = 0;
		int offsetConstraint = this->getNUnknowns();

		if (!forwardOnly)
		{
			for (int i = 0; i < this->getNStations(); i++)
			{
				CObservationHandler* oh = ObservationHandlers.GetAt(i);
				CSensorModel *sensorMod = oh->getCurrentSensorModel();

				sensorMod->setParameterIndex(CSensorModel::shiftPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::shiftPar);
				sensorMod->setParameterIndex(CSensorModel::rotPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::rotPar);
				sensorMod->setParameterIndex(CSensorModel::scalePar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::scalePar);
				sensorMod->setParameterIndex(CSensorModel::mountShiftPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::mountShiftPar);
				sensorMod->setParameterIndex(CSensorModel::mountRotPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::mountRotPar);
				sensorMod->setParameterIndex(CSensorModel::interiorPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::interiorPar);
				sensorMod->setParameterIndex(CSensorModel::addPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::addPar);

				sensorMod->setParameterIndex(CSensorModel::constraints,offsetConstraint);
				offsetConstraint+=sensorMod->getParameterCount(CSensorModel::constraints);

				if (oh->getNstationParameters() > 0)
					sensorMod->insertParametersForAdjustment(this->solution);
					
				nDirectObs += sensorMod->getDirectObsCount();
			}
		}

		for (int i = 0; i < this->getNObjectPoints(); i++)
		{
			CObsPoint* xyzPoint = this->ObjectPoints.GetAt(i);
			xyzPoint->id = offset;
			this->solution.element(offset, 0) = (*xyzPoint)[0]; ++offset;
			this->solution.element(offset, 0) = (*xyzPoint)[1]; ++offset;
			this->solution.element(offset, 0) = (*xyzPoint)[2]; ++offset;
		}
	}
}

bool CBuildingBundleManager::solve(bool forwardOnly, bool robust, bool omitMarked)
{
	bool ret = CBundleManager::solve(forwardOnly, robust, omitMarked);

	if (ret)
	{
		// set object coordinates of building
	}

	return ret;
};
