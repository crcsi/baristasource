#include "TFWTable.h"

#include <QHeaderView>
#include <QCloseEvent>
#include "TFW.h"

CTFWTable::CTFWTable(CTFW* tfw,QWidget* parent) : CBaristaTable(parent),offsetdezimals(4),
	scaledezimals(4),rotationdezimals(4),tfw(tfw)
{
	this->initTable();
    tfw->addListener(this);
}

CTFWTable::~CTFWTable(void)
{

}

void CTFWTable::closeEvent ( QCloseEvent * e )
{
    this->tfw->removeListener(this);
    e->accept();
}


void CTFWTable::initTable()
{
	CCharString str("TFW Parameters:   ");
	str = str + this->tfw->getFileName()->GetFullFileName();
	this->setWindowTitle(str.GetChar());
	this->setAccessibleName(this->tfw->getFileName()->GetChar());


	this->setColumnCount(2);

	// parameters plus coordinate system
	this->setRowCount(this->tfw->getNumberOfStationPars()+1);
	this->setColumnWidth(0, 110);
	this->setColumnWidth(1, 190);

	for (int i=0; i < tfw->getNumberOfStationPars()+1; i++)
		this->setRowHeight(i,20);

	QStringList headerList;
	headerList.append(QString("Parameters"));
	headerList.append(QString("Values"));
	this->setHorizontalHeaderLabels(headerList);

	if ( this->tfw->getRefSys().getCoordinateType() == eGEOGRAPHIC)
    {
		this->offsetdezimals = 10;
		this->scaledezimals = 10;
		this->rotationdezimals = 10;
    }
    else
    {
		this->offsetdezimals = 4;
		this->scaledezimals = 4;
		this->rotationdezimals = 4;
	}

	QString sys = this->tfw->getRefSys().getCoordinateSystemName().GetChar();

	QTableWidgetItem* item;
	
	////// TFW node reference system info
	item = this->createItem("Coordinate System",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,0,item);
	item = this->createItem(sys,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,1,item);


	// TFW parameters

	if(this->tfw->getParameterCount(CSensorModel::addPar) == 6)
	{
		// Scale X
		item = this->createItem("Scale X",0,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(1,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getA(), 0, 'F', this->scaledezimals ),0,Qt::AlignRight,QColor(0,0,0) );
		this->setItem(1,1,item);

		// Rotation row
		item = this->createItem("Rotation row",0,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(2,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getD(), 0, 'F', this->rotationdezimals ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(2,1,item);

		// Rotation col
		item = this->createItem("Rotation col",0,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(3,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getB(), 0, 'F', this->rotationdezimals ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(3,1,item);

		// Scale Y
		item = this->createItem("Scale Y",0,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(4,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getE(), 0, 'F', this->scaledezimals ) ,0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(4,1,item);
		
		// Offset X 
		item = this->createItem("Offset X",0,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(5,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getC(), 0, 'F', this->offsetdezimals ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(5,1,item);

		// Offset Y
		item = this->createItem("Offset Y",0,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(6,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getF(), 0, 'F', this->offsetdezimals ) ,0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(6,1,item);

	}
	else if(this->tfw->getParameterCount(CSensorModel::addPar) == 4)
	{
		// Scale
		item = this->createItem("Scale",Qt::ItemIsSelectable,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(1,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getA(), 0, 'F', this->scaledezimals ),Qt::ItemIsSelectable,Qt::AlignRight,QColor(0,0,0));
		this->setItem(1,1,item);

		// Rotation
		item = this->createItem("Rotation",Qt::ItemIsSelectable,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(2,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getB(), 0, 'F', this->rotationdezimals ),Qt::ItemIsSelectable,Qt::AlignRight,QColor(0,0,0));
		this->setItem(2,1,item);

		// Offset X
		item = this->createItem("Offset X",Qt::ItemIsSelectable,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(3,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getC(), 0, 'F', this->offsetdezimals ),Qt::ItemIsSelectable,Qt::AlignRight,QColor(0,0,0));
		this->setItem(3,1,item);

		// Offset Y
		item = this->createItem("Offset Y",Qt::ItemIsSelectable,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(4,0,item);
		item = this->createItem(QString( "%1" ).arg(this->tfw->getD(), 0, 'F', this->offsetdezimals ) ,Qt::ItemIsSelectable,Qt::AlignRight,QColor(0,0,0));
		this->setItem(4,1,item);
	}
}
