#include "Bui_RpcDlg.h"
#include "VImage.h"
#include "BaristaProject.h"

#define FIXED 1e-18

Bui_RpcDlg::Bui_RpcDlg(CRPC* existingRPC,CRPC* originalRPC,QWidget *parent)	: CAdjustmentDlg(parent),
	existingRPC(existingRPC),originalRPC(originalRPC)
{
	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->localRPC.copy(*this->existingRPC);
	existingRPC->addListener(this);

	this->connect(this->rB_None,SIGNAL(released()),this,SLOT(rBNoneReleased()));
	this->connect(this->rB_Shift,SIGNAL(released()),this,SLOT(rBShiftReleased()));
	this->connect(this->rB_ShiftDrift,SIGNAL(released()),this,SLOT(rBShiftDriftReleased()));
	this->connect(this->rB_Affine,SIGNAL(released()),this,SLOT(rBAffineReleased()));

	this->connect(this->pB_Reset,SIGNAL(released()),this,SLOT(pBResetReleased()));
	this->connect(this->pB_ResetAll,SIGNAL(released()),this,SLOT(pBResetAllReleased()));
	this->connect(this->pB_Apply,SIGNAL(released()),this,SLOT(pBApplyReleased()));


	this->applySettings();
}

Bui_RpcDlg::~Bui_RpcDlg()
{
	this->cleanUp();	
}

void Bui_RpcDlg::cleanUp()
{
	if (this->existingRPC)
		this->existingRPC->removeListener(this);
	this->existingRPC = NULL;
}

bool Bui_RpcDlg::okButtonReleased()
{
	this->listenToSignal=false;
	this->existingRPC->copy(this->localRPC);
	this->listenToSignal =true;
	return true;
}

bool Bui_RpcDlg::cancelButtonReleased()
{
	return true;
}

CCharString Bui_RpcDlg::helpButtonReleased()
{
	return "";
}



void Bui_RpcDlg::applySettings()
{
	this->rB_None->blockSignals(true);
	this->rB_Shift->blockSignals(true);
	this->rB_ShiftDrift->blockSignals(true);
	this->rB_Affine->blockSignals(true);

	
	Matrix* covar = this->localRPC.getCovariance();
	Matrix par;
	this->localRPC.getAdjustableParameters(par);

	QString formatPAR = "%.2e";
	QString formatSIG = "%.2e";

	if (this->localRPC.getConfigType() == eRPCAffine)
	{
		this->gB_Parameter->setVisible(true);
		this->rB_Affine->setChecked(true);
		this->lB_A1->setVisible(true);
		this->lB_A2->setVisible(true);
		this->lB_B1->setVisible(true);
		this->lB_B2->setVisible(true);

		this->lE_A1->setVisible(true);
		this->lE_A2->setVisible(true);
		this->lE_B1->setVisible(true);
		this->lE_B2->setVisible(true);

		this->lE_SA1->setVisible(true);
		this->lE_SA2->setVisible(true);
		this->lE_SB1->setVisible(true);
		this->lE_SB2->setVisible(true);

		this->lB_A0->setText("A0");
		this->lB_A1->setText("A1");
		this->lB_A2->setText("A2");
		this->lB_B0->setText("B0");
		this->lB_B1->setText("B1");
		this->lB_B2->setText("B2");


		this->lE_A0->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(0,0)));
		this->lE_A1->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(1,0)));
		this->lE_A2->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(2,0)));
		this->lE_B0->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(3,0)));
		this->lE_B1->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(4,0)));
		this->lE_B2->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(5,0)));

		this->lE_SA0->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(0,0))));
		this->lE_SA1->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(1,1))));
		this->lE_SA2->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(2,2))));
		this->lE_SB0->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(3,3))));
		this->lE_SB1->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(4,4))));
		this->lE_SB2->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(5,5))));


	}
	else if (this->localRPC.getConfigType() == eRPCShiftDrift)
	{
		this->gB_Parameter->setVisible(true);
		this->rB_ShiftDrift->setChecked(true);

		this->lB_A1->setVisible(false);
		this->lB_A2->setVisible(true);
		this->lB_B1->setVisible(false);
		this->lB_B2->setVisible(true);

		this->lE_A1->setVisible(false);
		this->lE_A2->setVisible(true);
		this->lE_B1->setVisible(false);
		this->lE_B2->setVisible(true);

		this->lE_SA1->setVisible(false);
		this->lE_SA2->setVisible(true);
		this->lE_SB1->setVisible(false);
		this->lE_SB2->setVisible(true);

		this->lB_A0->setText("shift x");
		this->lB_A2->setText("drift x");
		this->lB_B0->setText("shift y");
		this->lB_B2->setText("drift y");


		this->lE_A0->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(0,0)));
		this->lE_A2->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(2,0)));
		this->lE_B0->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(3,0)));
		this->lE_B2->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(5,0)));

		this->lE_SA0->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(0,0))));
		this->lE_SA2->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(2,2))));
		this->lE_SB0->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(3,3))));
		this->lE_SB2->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(5,5))));



	}
	else if (this->localRPC.getConfigType() == eRPCShift)
	{
		this->gB_Parameter->setVisible(true);
		this->rB_Shift->setChecked(true);
		this->lB_A1->setVisible(false);
		this->lB_A2->setVisible(false);
		this->lB_B1->setVisible(false);
		this->lB_B2->setVisible(false);

		this->lE_A1->setVisible(false);
		this->lE_A2->setVisible(false);
		this->lE_B1->setVisible(false);
		this->lE_B2->setVisible(false);

		this->lE_SA1->setVisible(false);
		this->lE_SA2->setVisible(false);
		this->lE_SB1->setVisible(false);
		this->lE_SB2->setVisible(false);

		this->lB_A0->setText("shift x");
		this->lB_B0->setText("shift y");

		this->lE_A0->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(0,0)));
		this->lE_B0->setText(QString().sprintf(formatPAR.toLatin1().constData(),par.element(3,0)));

		this->lE_SA0->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(0,0))));
		this->lE_SB0->setText(QString().sprintf(formatPAR.toLatin1().constData(),sqrt(covar->element(3,3))));

	}
	else
	{
		this->rB_None->setChecked(true);
		this->gB_Parameter->setVisible(false);
	}


	this->rB_None->blockSignals(false);
	this->rB_Shift->blockSignals(false);
	this->rB_ShiftDrift->blockSignals(false);
	this->rB_Affine->blockSignals(false);

}

void Bui_RpcDlg::rBNoneReleased()
{
	this->localRPC.fixAll();
	this->applySettings();
}

void Bui_RpcDlg::rBShiftReleased()
{
	this->localRPC.freeShift();
	this->applySettings();
}

void Bui_RpcDlg::rBShiftDriftReleased()
{
	this->localRPC.freeShiftDrift();
	this->applySettings();
}

void Bui_RpcDlg::rBAffineReleased()
{
	this->localRPC.freeAffine();
	this->applySettings();
}

void Bui_RpcDlg::pBResetReleased()
{
	this->localRPC.copy(*this->originalRPC);
	this->applySettings();
}

void Bui_RpcDlg::pBResetAllReleased()
{
	for (int i=0; i < project.getNumberOfImages(); i++)
	{
		CVImage* image = project.getImageAt(i);
		if (!image->hasRPC())
			continue;
		
		image->resetRPC();
		
	}	
	this->applySettings();
}

void Bui_RpcDlg::pBApplyReleased()
{
	this->listenToSignal = false;
	
	for (int i=0; i < project.getNumberOfImages(); i++)
	{
		CVImage* image = project.getImageAt(i);
		if (!image->hasRPC())
			continue;

		if (this->localRPC.getConfigType() == eRPCNone)
		{
			image->getRPC()->fixAll();

		}
		else if (this->localRPC.getConfigType() == eRPCShift)
		{
			image->getRPC()->freeShift();

		}
		else if (this->localRPC.getConfigType() == eRPCShiftDrift)
		{
			image->getRPC()->freeShiftDrift();

		}
		else if (this->localRPC.getConfigType() == eRPCAffine)
		{
			image->getRPC()->freeAffine();

		}
		
	}		

	this->listenToSignal = true;
}

void Bui_RpcDlg::elementsChangedSlot(CLabel element)
{
	if (!this->listenToSignal)
		return;
	
	this->localRPC.copy(*this->existingRPC);

	this->applySettings();
}