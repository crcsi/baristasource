#include "Bui_LabelChangeDlg.h"
#include "Icons.h"
#include <QValidator>

bui_LabelChangeDlg::bui_LabelChangeDlg(void):success(false)
{
	setupUi(this);

	connect(this->Ok,SIGNAL(released()),this,SLOT(Ok_released()));
	connect(this->LabelInput,SIGNAL(returnPressed()),this,SLOT(Ok_released()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->LabelInput->setFocus( Qt::ActiveWindowFocusReason);
}

bui_LabelChangeDlg::bui_LabelChangeDlg(const char * text, const char* title, bool extended):success(false)
{
	setupUi(this);

	this->LabelInput->setFocus( Qt::ActiveWindowFocusReason);

	this->setWindowTitle("New name");

	if ( title ) this->setWindowTitle(title);

	this->setLabelText(text);

	connect(this->Ok,SIGNAL(released()),this,SLOT(Ok_released()));
	connect(this->LabelInput,SIGNAL(returnPressed()),this,SLOT(Ok_released()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
	
	if ( extended )
	{
		this->window()->resize(250, 55);
		this->checkBoxAllViews->setVisible(true);
	}
	else
	{
		this->window()->resize(250, 25);
		this->checkBoxAllViews->setVisible(false);
	}

}

bui_LabelChangeDlg::~bui_LabelChangeDlg(void)
{
}

CCharString bui_LabelChangeDlg::getLabelText()
{
	QByteArray qba = this->LabelInput->text().toLatin1();

	CCharString string = (const char*)qba;

	return string;
}

void bui_LabelChangeDlg::Ok_released()
{
    if ( !this->LabelInput->text().isEmpty())
	{
        this->close();
		this->setSuccess(true);
	}
}

void bui_LabelChangeDlg::setLabelText(const char* text)
{
    this->LabelInput->setText(text);
}

void bui_LabelChangeDlg::forceLabelforAllViews()
{
	this->checkBoxAllViews->setChecked(true);
	this->checkBoxAllViews->setEnabled(false);
}

