#include "ProgressThread.h"

#include "HugeImage.h"
#include "OrthoImageGenerator.h"
#include "Bui_ImageRegistrationDialog.h"
#include "GDALFile.h"
#include "Filename.h"
#include "SatelliteDataImporter.h"
#include "SatelliteDataReader.h"

#include "Bui_ALSMergeDialog.h"

#include "VDEM.h"
#include "VImage.h"
#include "VALS.h"
#include "BaristaProject.h"
#include "BundleHelper.h"
#include "ASCIIDEMFile.h"
#include "Bui_BuildingDialog.h"
#include "Bui_BuildingChangeDialog.h"
#include "Bui_Pansharpen.h"
#include "Bui_DEMMatchingDlg.h"
#include "ImageMerger.h" 
#include "Bui_ALSFilter.h"
#include "Bui_GCPMatcher.h"
#include "ImageImporter.h"
#include "BandMerger.h"
#include "ImageExporter.h"
#include "RAG.h"
#include "Bui_LabelChangeDlg.h"
#include <QMessageBox>
#include "BuildingReconstructor.h"
#include "Bui_BuildingsDetector.h"
#include "GenerateEpipolarProject.h"


CProgressThread::CProgressThread(void) : success(false)
{

}

CProgressThread::~CProgressThread(void)
{

}


/* #####################################################################*/
/**
 * Constructor
 * @param OrthoImageGenerator
 */
COrthoProcessingThread::COrthoProcessingThread(COrthoImageGenerator* I_orthogenerator) :
			orthogenerator(I_orthogenerator)
{
}

COrthoProcessingThread::~COrthoProcessingThread()
{
	this->orthogenerator =  0;
}

/**
 * implementation of the run function 
 * @see QThread
 */
void COrthoProcessingThread::run()
{
	this->success = this->orthogenerator->makeOrthoImage();
}



/* #####################################################################*/

/**
 * Constructor
 * @param image the image to pansharpen
 */

CPansharpenProcessingThread::CPansharpenProcessingThread(bui_PanDialog* panDLG):
			panDialog(panDLG)
{
}


CPansharpenProcessingThread::~CPansharpenProcessingThread()
{
	panDialog = 0;
}

/**
 * implementation of the run function 
 * @see QThread
 */
void CPansharpenProcessingThread::run()
{
	this->success = this->panDialog->panSharpen();
}


/* #####################################################################*/

CImgRegistrationProcessingThread::CImgRegistrationProcessingThread(bui_ImageRegistrationDialog* registrationmatcher) :
			registrationMatcher(registrationmatcher)
{
}

CImgRegistrationProcessingThread::~CImgRegistrationProcessingThread()
{
	this->registrationMatcher =  0;
}

/**
 * implementation of the run function 
 * @see QThread
 */
void CImgRegistrationProcessingThread::run()
{
	this->success = this->registrationMatcher->compute();
}


/* #####################################################################*/

CDEMProcessingThread::CDEMProcessingThread(Bui_DEMMatchingDlg* demmatcher) :
			demMatcher(demmatcher)
{
}

CDEMProcessingThread::~CDEMProcessingThread()
{
	this->demMatcher =  0;
}

/**
 * implementation of the run function 
 * @see QThread
 */
void CDEMProcessingThread::run()
{
	this->success = this->demMatcher->compute();
}


/* #####################################################################*/

void CGCPMatchingThread::run()
{
	this->success = (this->gcpMatcher->MatchPoints() > 0);
}

/* #####################################################################*/

CGDALExportThread::CGDALExportThread(CImageExporter* imageExporter) :
		imageExporter(imageExporter)

{
}

CGDALExportThread::~CGDALExportThread(void)
{
}

void CGDALExportThread::run()
{
	this->success = this->imageExporter->writeImage();
}


/* #####################################################################*/

CSatelliteDataReaderThread::CSatelliteDataReaderThread(CSatelliteDataImporter* importer,CFileName filename):
	importer(importer),filename(filename)
{
}

void CSatelliteDataReaderThread:: run()
{
	this->success = this->importer->importMetadata(this->filename);
}



/* #####################################################################*/

COrbitAdjustmentThread::COrbitAdjustmentThread(CSatelliteDataImporter* importer,bool adjustPath,bool adjustAttitudes):
								importer(importer),adjustPath(adjustPath),adjustAttitudes(adjustAttitudes)
								
{
}

void COrbitAdjustmentThread:: run()
{
	this->success = this->importer->recomputeOrbit(this->adjustPath,this->adjustAttitudes);
}


/* #####################################################################*/

CALSDataReaderThread::CALSDataReaderThread(CALSDataSet *ALS, const char *Filename, eALSFormatType eformat, eALSPulseModeType epulseType, const C2DPoint &Offset):
		als(ALS), filename(Filename), eFormat(eformat), ePulseType(epulseType), offset(Offset)
{
}

void CALSDataReaderThread:: run()
{
	this->success = this->als->read(filename.GetChar(), eFormat, ePulseType, offset);
}


/* #####################################################################*/

CALSDataInterpolatorThread::CALSDataInterpolatorThread(CXYZPointArray *Points, 
													   const char *fileName, CVDEM *DEM, 
													   int Neighbourhood, eInterpolationMethod Method,
													   const C2DPoint &Pmin, 
													   const C2DPoint &Pmax, const C2DPoint &grid,
							                           double maxdist) :
		als(0), filename(fileName), vdem(DEM), vimage(0), pulse(0),
		level(0), neighbourhood(Neighbourhood), method(Method), pmin(Pmin),
		pmax(Pmax), gridS(grid), vals(0), points(Points), maxDist(maxdist)
{
}

CALSDataInterpolatorThread::CALSDataInterpolatorThread(CALSDataSet *ALS, const char *fileName, 
													   CVDEM *DEM, int Pulse,
													   int Level, int Neighbourhood, 
													   eInterpolationMethod Method,
													   const C2DPoint &Pmin, const C2DPoint &Pmax, 
													   const C2DPoint &grid,
							                           double maxdist) :
		als(ALS), filename(fileName), vdem(DEM), vimage(0), pulse(Pulse),
		level(Level), neighbourhood(Neighbourhood), method(Method), pmin(Pmin),
		pmax(Pmax), gridS(grid), vals(0), points(0), maxDist(maxdist)
{
}

CALSDataInterpolatorThread::CALSDataInterpolatorThread(CVALS* VALS, const char *fileName, 
													   CVImage *IMG, int Pulse,
													   int Level, int Neighbourhood, 
													   eInterpolationMethod Method,
													   const C2DPoint &Pmin, const C2DPoint &Pmax, 
													   const C2DPoint &grid,
							                           double maxdist) :
		als(0), filename(fileName), vdem(0), vimage(IMG), pulse(Pulse),
		level(Level), neighbourhood(Neighbourhood), method(Method), pmin(Pmin),
		pmax(Pmax), gridS(grid), vals(VALS), points(0), maxDist(maxdist)
{
	this->als = this->vals->getCALSData();
}

void CALSDataInterpolatorThread::run()
{
	if (vdem) 
	{
		double minSqrDistWeight = 0.01;
		if (this->als) this->success = this->vdem->interpolate(*(this->als), this->filename.GetChar(), 
															   this->pmin, this->pmax, this->gridS, 
															   this->neighbourhood, this->pulse, 
															   this->level, this->method, maxDist, minSqrDistWeight);
		else  this->success = this->vdem->interpolate(*(this->points), this->filename.GetChar(), 
															   this->pmin, this->pmax, this->gridS, 
															   this->neighbourhood, this->method, this->maxDist, minSqrDistWeight);
	}
	else if (vimage) 
	{
		CFileName fn = this->filename + ".hug";

		this->success = vals->createIntensityImage(this->vimage->getHugeImage(), fn.GetChar(),
												   this->pmin, this->pmax, this->gridS, 
												   this->neighbourhood, this->pulse, 
												   this->level, this->method, this->maxDist);

		CTFW tfw(*this->vimage->getTFW());

		Matrix parms(6,1);
		parms.element(0, 0) =  this->gridS.x;
		parms.element(1, 0) =  0.0;
		parms.element(2, 0) =  this->pmin.x;
		parms.element(3, 0) =  0.0;
		parms.element(4, 0) = -this->gridS.y;
		parms.element(5, 0) =  this->pmax.y;
		
		tfw.setAll(parms);
		this->vimage->setTFW(tfw);
		
	}
	else this->success = false;

	if (this->als) this->als->clear();
}

/* #####################################################################*/

CALSDataMergeThread::CALSDataMergeThread(bui_ALSMergeDialog *ALSMrgDlg):
					  ALSMergeDlg(ALSMrgDlg)
{
};

void CALSDataMergeThread::run()
{
	this->success = ALSMergeDlg->compute();
}; 

/* #####################################################################*/

CBaristaProjectReaderThread::CBaristaProjectReaderThread(CBaristaProject *project, const char *Filename): project(project), filename(Filename)
{
}

void CBaristaProjectReaderThread:: run()
{
	this->project->readProject(this->filename);
	this->success = true;
}

/* #####################################################################*/

CRunBundleThread::CRunBundleThread(CBaristaBundleHelper *bundleHelper,bool forwardOnly, 
								   bool Robust, bool OmitMarked) :bundleHelper(bundleHelper),
								   robust(Robust), omitMarked(OmitMarked),forwardOnly(forwardOnly)
{
}

void CRunBundleThread:: run()
{
	this->success = this->bundleHelper->solve(forwardOnly, robust, omitMarked);
}


/* #####################################################################*/
CASCIIConvertFromXYZThread::CASCIIConvertFromXYZThread(CASCIIDEMFile *asciiDEM,const char *Filename):asciiDEM(asciiDEM),filename(Filename)
{
}

void CASCIIConvertFromXYZThread::run()
{
	this->success = asciiDEM->convertFromXYZ(filename.GetChar());
}

/* #####################################################################*/
CFeatureExtractionThread::CFeatureExtractionThread(CImageBuffer *imgbuf, CXYPolyLine *boundary, CXYPointArray *xypts, 
		                                           CXYContourArray *xycont, CXYPolyLineArray *xyedges, 
							                       CLabelImage *Labels,
							                       FeatureExtractionParameters Parameter):
         img(imgbuf), parameter(Parameter), points(xypts), contours(xycont), edges(xyedges), 
		 labels(Labels), boundaryPoly(boundary)
{
}

void CFeatureExtractionThread::run()
{
	if (img)
	{
		CFeatureExtractor extractor (this->parameter);
		if (this->listener)
		{
			this->listener->setTitleString("");
			extractor.setProgressListener(this->listener);
		}
		extractor.setRoi(img, boundaryPoly);

		int index = 0;
		if (points) index = points->GetSize();

		CRAG rag;

		extractor.extractFeatures(points, contours, edges, labels, &rag, true);

		if (points)
		{
			CLabel label = "PF";
			label += index;
			for (; index < points->GetSize(); ++index, ++label)
			{
				CXYPoint *pP = points->getAt(index);
				pP->setLabel(label);
			}
		};

		extractor.resetRoi();

		this->success = true;
	}
	else this->success = false;
}
/* #####################################################################*/
CSnakeExtractionThread::CSnakeExtractionThread(CImageBuffer *imgbuf, CXYPointArray *Start, SnakeExtractionParameters Parameter):
							image(imgbuf), startPoints(Start), parameter(Parameter)
{
}

void CSnakeExtractionThread::run()
{
	bool ret = false;

	if (image)
	{
		CSnakeExtractor extractor(this->parameter);
			
		if (this->listener)
		{
			this->listener->setTitleString("");
			extractor.setProgressListener(this->listener);
		}

		extractor.setRoi(image);
 		ret = extractor.computeSnake(startPoints);
			
		extractor.resetRoi();

		if (ret) this->success = true;
		else this->success = false;
	}
	else this->success = false;
}


#ifdef WIN32
/* #####################################################################*/

CBuildingDetectionThread::CBuildingDetectionThread(bui_BuildingDialog *DLG):dlg(DLG)
{
}

void CBuildingDetectionThread::run()
{
	if (dlg)
	{
		this->success = dlg->extract();
	}
	else this->success = false;
}


/* #####################################################################*/

CBuildingChangeThread::CBuildingChangeThread(bui_BuildingChangeDialog *DLG):dlg(DLG)
{
}

void CBuildingChangeThread::run()
{
	if (dlg)
	{
		this->success = dlg->detectChanges();
	}
	else this->success = false;
}

#endif

/* #####################################################################*/

CALSFilterThread::CALSFilterThread(bui_ALSFilterDialog *DLG):dlg(DLG)
{
}

void CALSFilterThread::run()
{
	if (dlg)
	{
		this->success = dlg->filter();
	}
	else this->success = false;
}

/* #####################################################################*/

CBuildingsDetectThread::CBuildingsDetectThread(bui_BuildingsDetector *DLG):dlg(DLG)
{
}

void CBuildingsDetectThread::run()
{
	if (dlg)
	{
		this->success = dlg->detectBuildings();
	}
	else this->success = false;
}

/* #####################################################################*/

CBuildingReconstructionThread::CBuildingReconstructionThread(CBuilding *Building, 
		                     vector<CObservationHandler *> *imgVec, 
							 vector<CHugeImage *> *hugImgVec, 
							 CALSDataSet *ALS, const CCharString &path,
							 double PixImg, double PixDSM,
							 CBuildingReconstructor *reconstructor):
			  pBuilding(Building), pImgVec(imgVec), pHugeImgVec(hugImgVec), 
			  pALS(ALS), outputPath(path), pixImg(PixImg), pixDSM(PixDSM), 
			  pReconstructor(reconstructor)
{
};

void CBuildingReconstructionThread::run()
{
	this->success = true;

#ifdef WIN32
	
	if (this->pReconstructor)
	{
		this->success = pReconstructor->initDataForBuilding(this->pBuilding, this->pImgVec, 
															this->pHugeImgVec, this->pALS, 
															this->outputPath, this->pixImg, 
															this->pixDSM);

		if (this->success)
		{
			this->success = this->pReconstructor->reconstruct();
		}
	}

#endif
}; 


/* #####################################################################*/

CDEMMergeThread::CDEMMergeThread(CDEM* newDEM,vector<CDEM*> mergeDEMList):
	newDEM(newDEM),mergeDEMList(mergeDEMList)
{
}

void CDEMMergeThread::run()
{
	if (this->newDEM)
	{
		this->success = newDEM->mergefromDEMs(this->mergeDEMList);
	}
	else this->success = false;
}

/* #####################################################################*/

CDEMDifferenceThread::CDEMDifferenceThread(CDEM* newDEM,CDEM* masterDEM, CDEM* compareDEM):
	newDEM(newDEM),masterDEM(masterDEM),compareDEM(compareDEM)
{
}

void CDEMDifferenceThread::run()
{
	if (this->newDEM)
	{
		this->success = newDEM->differenceDEMs(masterDEM, compareDEM);
	}
	else this->success = false;
}

/* #####################################################################*/

CHugeImageFlipThread::CHugeImageFlipThread(CHugeImage* himage, bool vertical, bool horizontal):
	himage(himage), vertical(vertical), horizontal(horizontal)
{
}

void CHugeImageFlipThread::run()
{
	if (this->himage)
	{
		this->success = this->himage->flip(vertical, horizontal);
	}
	else this->success = false;
}

/* #####################################################################*/

/* #####################################################################*/

CHugeImageEntropyThread::CHugeImageEntropyThread(CHugeImage* himage, CHugeImage* entropy):
	himage(himage), entropy(entropy)
{
}

void CHugeImageEntropyThread::run()
{
	if (this->himage)
	{
		this->success = this->himage->computeEntropy(entropy);
	}
	else this->success = false;
}

/* #####################################################################*/

/* #####################################################################*/

CHugeImageEdgeOrientationThread::CHugeImageEdgeOrientationThread(CHugeImage* himage, CHugeImage* edgeOrientation, CHugeImage* edgeImg, float resImage):
	himage(himage), edgeOrientation(edgeOrientation), edgeImg(edgeImg), resImage(resImage)
{
}

void CHugeImageEdgeOrientationThread::run()
{
	if (this->himage)
	{
		this->success = this->himage->computeEdgeOrientation(edgeOrientation,edgeImg,resImage);
	}
	else this->success = false;
}

/* #####################################################################*/

CDEMTransformThread::CDEMTransformThread(CDEM* trafoDEM,CDEM* srcDEM):
	trafoDEM(trafoDEM),srcDEM(srcDEM)
{
}

void CDEMTransformThread::run()
{
	if (this->trafoDEM)
	{
		this->success = this->trafoDEM->transformDEM(srcDEM);
	}
	else this->success = false;
}

/* #####################################################################*/

CDEMOperationsThread::CDEMOperationsThread(double nullval, double constVall, string LogicOP, CDEM* trafoDEM,CDEM* srcDEM):
constvalue (constVall), operation (LogicOP),nullval (nullval), trafoDEM(trafoDEM),srcDEM(srcDEM)
{
}

void CDEMOperationsThread::run()
{
	if (this->trafoDEM)
	{
		this->success = this->trafoDEM->Operation(srcDEM, constvalue, nullval, operation);
	}
	else this->success = false;
}


/* #####################################################################*/

CMeanAndStdDevThread::CMeanAndStdDevThread(CHugeImage* himage,
										  doubles& mean,
										  doubles& stdDev,
										  int& nParametersX,
										  int& nParametersY,
										  int windowSizeX,
										  int windowSizeY,
										  int rectTop,
										  int rectHeight,
										  int rectLeft,
										  int rectWidth) :
	himage(himage),mean(mean),stdDev(stdDev),nParametersX(nParametersX),
	nParametersY(nParametersY),windowSizeX(windowSizeX),windowSizeY(windowSizeY),
	rectTop(rectTop),rectHeight(rectHeight),rectLeft(rectLeft),rectWidth(rectWidth)
{
}

void CMeanAndStdDevThread::run()
{
	if (this->himage)
	{
		this->success = this->himage->computeMeanAndStdDev(this->mean,this->stdDev,
															this->nParametersX,this->nParametersY,
															this->windowSizeX,this->windowSizeY,
															this->rectTop,this->rectHeight,
															this->rectLeft,this->rectWidth);
	}
	else this->success = false;
}


/* #####################################################################*/

CImageMergeThread::CImageMergeThread(CImageMerger* merger) :
	merger(merger)
{
}

void CImageMergeThread::run()
{
	if (this->merger)
	{
		this->success = this->merger->mergeImages();
	}
	else this->success = false;
}

/* #####################################################################*/

CApplyTransferFunctionThread::CApplyTransferFunctionThread(CHugeImage* oldHugeImage,CHugeImage* newHugeImage,bool applyTransferFunction):
	oldHugeImage(oldHugeImage),applyTransferFunction(applyTransferFunction),newHugeImage(newHugeImage)
{
}

void CApplyTransferFunctionThread::run()
{
	if (this->newHugeImage && this->oldHugeImage)
	{
		this->success = this->newHugeImage->createFromHugeImage(this->oldHugeImage,this->applyTransferFunction);
	}
	else this->success = false;
}

/* #####################################################################*/

CComputeHistogramThread::CComputeHistogramThread(CHugeImage* himage,int rectTop,int rectHeight,int rectLeft,int rectWidth,vector<CHistogram*>& histograms,CHistogram& intensityHistogram):
	himage(himage),rectTop(rectTop),rectLeft(rectLeft),width(rectWidth),height(rectHeight),histograms(histograms),intensityHistogram(intensityHistogram)
{
}

void CComputeHistogramThread::run()
{
	if (this->himage)
	{
		this->success = this->himage->computeHistogramsForROI(	this->rectTop,
																this->height,
																this->rectLeft,
																this->width,
																this->histograms,
																this->intensityHistogram);
		
	}
	else this->success = false;
}

/* #####################################################################*/
void CImageLoadThread::run()
{
	if (this->callInit)
		this->success =this->imageImporter.initImportData(this->importData,this->isDEM,this->filename,this->hugeFileNameDir);
	else
		this->success = this->imageImporter.importImage(this->importData,this->isDEM);
}


/* #####################################################################*/

CBandMergeThread::CBandMergeThread(CBandMerger& bandMerger) : bandMerger(bandMerger) 
{
}
	  
void CBandMergeThread::run()
{
	this->success = this->bandMerger.mergeBands();
}

/* #####################################################################*/

CGCPPointMatchingThread::CGCPPointMatchingThread(CBaristaProject& proj, const vector<CGCPDataBasePoint>& db, const CMatchingParameters &par,CReferenceSystem refSys) : 
                                                project(proj), dataBase(db), pars(par),referenceSystem(refSys)
{
}

void CGCPPointMatchingThread::run()
{
	this->success = this->project.matchGCPDataBasePoints(this->dataBase, this->pars,referenceSystem);
}


/* #####################################################################*/

GenerateEpipolarThread::GenerateEpipolarThread(CGenerateEpipolarProject *pEPI) : pEpi(pEPI)
{
}

void GenerateEpipolarThread::run()
{
	this->success = pEpi->generateEpipolar();
}


