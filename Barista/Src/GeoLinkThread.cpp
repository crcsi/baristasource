#include "GeoLinkThread.h"
#include "BaristaView.h"
#include "Trans3DFactory.h"


CGeoLinkThread::CGeoLinkThread(CBaristaView* view) : QThread(view),
	view(view), zoomFactor(1.0),changeResampling(false),trafo(0),
	resolutionSource(-1.0)

{
	this->connect(this,SIGNAL(finished()),this->view,SLOT(updateFromThread()));
}

CGeoLinkThread::~CGeoLinkThread(void)
{
	// wait if a thread is still running before deleting object
	this->wait();

	if (this->trafo)
		delete this->trafo;
	this->trafo = NULL;

}

void CGeoLinkThread::doGeoLink(const C3DPoint& centrePoint,const CReferenceSystem &refSysOf3DPoint, double zoomFactor,float resolutionSource,bool changeResampling)
{
	QMutexLocker(&this->mutex);
	
	// save the data
	this->centrePoint = centrePoint; // we need to copy the point now
	this->zoomFactor = zoomFactor;
	this->changeResampling = changeResampling;
	this->refSysOf3DPoint = refSysOf3DPoint;
	this->resolutionSource = resolutionSource;
	// and start thread
	this->start();
}

void CGeoLinkThread::run()
{
	this->mutex.lock();
	// only start doing something when not in use	
	if (this->view)
	{
		
		// first, we have to transform the centre point into the reference system of the 
		// current sensor model of the image 
		C3DPoint result;

		CMonoPlotter& mp = this->view->getMonoPlotter();
		const CSensorModel* smImage = mp.getModel();

		// we can only do geo linking with this view when the image has a sensor model
		if (smImage)
		{
			// update transformation if needed
			if (!this->trafo || //check if we got a transformation 
				this->trafo->getSystemFrom() != this->refSysOf3DPoint || // check if refsys are still ok
				this->trafo->getSystemTo() != smImage->getRefSys()) // check if sensor model of image is still the same 
			{
				// something has change so we need to update the transformation
				
				if (this->trafo)
					delete this->trafo;

				this->trafo =  CTrans3DFactory::initTransformation(this->refSysOf3DPoint,smImage->getRefSys());
			}

			if (this->trafo)
			{
				
				// transform centre point into reference system of image sensor model
				this->trafo->transform(result,this->centrePoint);
				
				// back project the 3D point using the current sensor model of image
				C2DPoint point2D;
				smImage->transformObj2Obs(point2D,result);

				// is resulting point inside image -> zoom to the point
				if (point2D.x >= 0 && point2D.x < this->view->getDataWidth(false) &&
					point2D.y >= 0 && point2D.y < this->view->getDataHeight(false))
				{
					
					// we want to adjust the zoom factor so that both images show roughly the same
					float resolutionTarget = this->view->getDataResolution();
			
					// only compute if we got a valid resolution for both images, we keep the original zoom otherwise
					if (resolutionTarget > 0.0 && this->resolutionSource > 0.0)
					{
						if (fabs (resolutionTarget - this->resolutionSource) > 0.5)
						{
							float power = log(resolutionTarget * zoomFactor / this->resolutionSource) / log(2.0);
							zoomFactor = pow(2.0,int(power + 0.5));
						}

					}

					// change resampling to save time
					if (changeResampling)
						this->view->changeResamplingType(eNearestNeighbour);
					else
						this->view->resetResamplingType();	

					this->view->zoomToPoint(point2D,zoomFactor,false);
				}

			}
		}

	}
	
	this->mutex.unlock();
}