#include "newmat.h"

#include "Bui_ImageRegistrationDialog.h"
#include <QFileDialog>
#include <QMessageBox> 

#include "SystemUtilities.h"
#include "IniFile.h"
#include "ProgressThread.h"
#include "Icons.h"
#include "Bui_ImageSensorSelector.h"
#include "BaristaHtmlHelp.h"
#include "Bui_FeatureExtractionDialog.h"
#include "RegistrationMatcher.h"
#include "Trans2D.h"
#include "Trans2DAffine.h"
#include "Bui_ProgressDlg.h"
#include "BaristaProject.h"
#include "BaristaProjectHelper.h"


bui_ImageRegistrationDialog::bui_ImageRegistrationDialog (bool showResample, CVImage *search, CVImage *temp) : 
				searchImageSelector(0), referenceImageSelector(0), 
				model(eAFFINEREGISTRATION), RefImageRes(1.0), SearchImageRes(1.0), keepPoints(false),
				searchImage(search), referenceImage(temp), trafo(0), matcher(0), resampleImage(false)
{
	setupUi(this);

	connect(this->buttonOk,               SIGNAL(released()),               this, SLOT(OK()));
	connect(this->buttonCancel,           SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->HelpButton,             SIGNAL(released()),               this, SLOT(Help()));
	connect(this->RefImageCombo,          SIGNAL(currentIndexChanged(int)), this, SLOT(onRefImageChanged()));
	connect(this->SearchImageCombo,       SIGNAL(currentIndexChanged(int)), this, SLOT(onSearchImageChanged()));
	connect(this->FeatureExtractionPars,  SIGNAL(released()),               this, SLOT(onFeatureExtractionParsChanged()));
	connect(this->CorrCoeffSelector,      SIGNAL(editingFinished()),        this, SLOT(onCorrCoeffChanged()));
	connect(this->SensorModelComboBox,    SIGNAL(currentIndexChanged(int)), this, SLOT(onModelChanged()));
	connect(this->RefImageResolution,     SIGNAL(editingFinished()),        this, SLOT(onRefResChanged()));
	connect(this->SearchImageResolution,  SIGNAL(editingFinished()),        this, SLOT(onSearchResChanged()));
	connect(this->MaxParalax,             SIGNAL(editingFinished()),        this, SLOT(onParallaxChanged()));
	connect(this->AddPoints,              SIGNAL(stateChanged(int)),        this, SLOT(keepPointsChanged()));
	connect(this->transferCheckBox,       SIGNAL(stateChanged(int)),        this, SLOT(onTransferGCPsChanged()));
	connect(this->sigma0,                 SIGNAL(editingFinished()),        this, SLOT(maxSigma0Changed()));
	connect(this->TransformImageCheckBox, SIGNAL(stateChanged(int)),        this, SLOT(TransformImageChanged()));
	connect(this->BrowseButton,           SIGNAL(released()),               this, SLOT(FileNamePushed()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->resampledImageOutputBox->setVisible(showResample);

	this->minCorrCoeff = inifile.getRegistrationCrossCorr();
	this->maxparallax  = inifile.getRegistrationParallax();
	this->maxsigma0    = inifile.getRegistrationSigma0();
	this->transferGCPs = inifile.getRegistrationAddPoints();

	this->init();
}

bui_ImageRegistrationDialog::~bui_ImageRegistrationDialog ()
{
	this->destroy();
}

void bui_ImageRegistrationDialog::init()
{
	this->hasRefImage = false;
	this->hasSearchImage = false;
    this->finished = false;

	if (!this->referenceImage) 
		this->referenceImageSelector = new bui_ImageSensorSelector(this->RefImageCombo,    0, eALLBANDS, eALLSENSORS, project.getImageAt(0));
	else
		this->referenceImageSelector = new bui_ImageSensorSelector(this->RefImageCombo,    0, eALLBANDS, eALLSENSORS, this->referenceImage);

	this->referenceImage = this->referenceImageSelector->getSelectedImage();
	if (this->referenceImage) 
	{
		this->hasRefImage = true;
		this->RefImageRes = this->referenceImage->getApproxGSD(project.getMeanHeight());
		if (this->RefImageRes > 0) this->RefImageRes = floor(this->RefImageRes * 100.0 + 0.5) * 0.01;
	}

	if (!this->searchImage)
		this->searchImageSelector = new bui_ImageSensorSelector(this->SearchImageCombo, 0, eALLBANDS, eALLSENSORS, project.getImageAt(1));
	else 
		this->searchImageSelector = new bui_ImageSensorSelector(this->SearchImageCombo, 0, eALLBANDS, eALLSENSORS, searchImage);
	this->searchImage = this->searchImageSelector->getSelectedImage();
	if (this->searchImage) 
	{
		this->SearchImageRes = this->searchImage->getApproxGSD(project.getMeanHeight());
		this->hasSearchImage = true;
		if (this->SearchImageRes > 0) this->SearchImageRes = floor(this->SearchImageRes * 100.0 + 0.5) * 0.01;
	}


	this->RefImageResolution->blockSignals(true);
	this->SearchImageResolution->blockSignals(true);
	this->CorrCoeffSelector->blockSignals(true);
	this->sigma0->blockSignals(true);

	QString ds=QString("%1").arg(this->minCorrCoeff, 0, 'f', 5);
	this->CorrCoeffSelector->setText(ds);

	ds=QString("%1").arg(this->RefImageRes, 0, 'f', 2);
	this->RefImageResolution->setText(ds);
	ds=QString("%1").arg(this->SearchImageRes, 0, 'f', 2);
	this->SearchImageResolution->setText(ds);
	ds=QString("%1").arg(this->maxparallax, 0, 'f', 1);
	this->MaxParalax->setText(ds);
	ds=QString("%1").arg(this->maxsigma0, 0, 'f', 2);
	this->sigma0->setText(ds);

	this->RefImageResolution->blockSignals(false);
	this->SearchImageResolution->blockSignals(false);
	this->CorrCoeffSelector->blockSignals(false);
	this->sigma0->blockSignals(false);

	this->AddPoints->blockSignals(true);
	this->AddPoints->setChecked(this->keepPoints);
	this->AddPoints->blockSignals(false);

	this->transferCheckBox->blockSignals(true);
	this->transferCheckBox->setChecked(this->transferGCPs);
	this->transferCheckBox->blockSignals(false);

	this->featureParameters.setDerivativeFilter(eSobelX,1.0f);
	this->featureParameters.NoiseModel = ePoisson;
	this->featureParameters.Mode = eFoerstner;
	this->featureParameters.setIntegrationFilter(eBinom, 5, 5, 0.7f, 1.0f);	
	this->featureParameters.distNonmaxPts = 15;
	this->featureParameters.extractPoints = 1;
	this->featureParameters.extractEdges  = 0;

	CFileName fn("RegFEX.ini");
	fn.SetPath(project.getFileName()->GetPath());
	if (CSystemUtilities::fileExists(fn))  this->featureParameters.initParameters(fn);

	this->SensorModelComboBox->blockSignals(true);
	this->SensorModelComboBox->addItem("2D Affine");
	this->SensorModelComboBox->setCurrentIndex(0);
	this->SensorModelComboBox->blockSignals(false);

	this->buttonOk->setFocusPolicy(Qt::NoFocus);	
	this->buttonCancel->setFocusPolicy(Qt::NoFocus);

	this->TransformImageCheckBox->blockSignals(true);
	this->transferCheckBox->setChecked(this->resampleImage);
	this->TransformImageCheckBox->blockSignals(false);

	if (this->searchImage)
	{
		this->initDefaultFilename(this->searchImage->getHugeImage()->getFileNameChar());
		this->ResampledImageLineEdit->blockSignals(true);
		this->ResampledImageLineEdit->setText(this->resampleFileName.GetChar());
		this->ResampledImageLineEdit->blockSignals(false);
	}

	this->checkSetup();
}
	  
void bui_ImageRegistrationDialog::initDefaultFilename(const char *hugname)
{
	this->resampleFileName = hugname;
	this->resampleFileName.SetFileName(this->resampleFileName.GetFileName() + "_resampled");
	int e = this->resampleFileName.ReverseFind('.');
	if (e >= 0)
	{
		if (this->resampleFileName.Right(this->resampleFileName.GetLength() - e - 1) == "hug")
		{
			this->resampleFileName = this->resampleFileName.Left(this->resampleFileName.GetLength() - 4);
		};
	}
};

void bui_ImageRegistrationDialog::destroy()
{
	if (this->matcher) delete this->matcher;
	this->matcher = 0;

	if (this->referenceImageSelector) delete this->referenceImageSelector;
	this->referenceImageSelector = 0;

	if (this->searchImageSelector) delete this->searchImageSelector;
	this->searchImageSelector = 0;

	if (trafo) delete trafo;
	trafo = 0;
}
	  
CTrans2D *bui_ImageRegistrationDialog::getTrafoClone() const 
{ 
	CTrans2D *tmp = 0;
	if (this->trafo) tmp = trafo->clone(); 

	return tmp;
};

	
void bui_ImageRegistrationDialog::onRefImageChanged()
{
	if (!this->referenceImageSelector->reactOnImageSelect()) 
	{
		QMessageBox::warning( this, "No image selected", " Image registration will be impossible");
	};

	if (this->referenceImageSelector->getSelectedImage()) 
	{
		this->referenceImage = this->referenceImageSelector->getSelectedImage();
		this->hasRefImage = true;	
		this->RefImageRes = this->referenceImage->getApproxGSD(project.getMeanHeight());
		if (this->RefImageRes > 0) this->RefImageRes = floor(this->RefImageRes * 100.0 + 0.5) * 0.01;

		this->RefImageResolution->blockSignals(true);
		QString ds=QString("%1").arg(this->RefImageRes, 0, 'f', 2);
		this->RefImageResolution->setText(ds);
		this->RefImageResolution->blockSignals(false);	
	}
	else 
	{
		this->hasRefImage = false;
		this->referenceImage = 0;
	}

	this->checkSetup();
}

void bui_ImageRegistrationDialog::onSearchImageChanged()
{
  	if (!this->searchImageSelector->reactOnImageSelect()) 
	{
		QMessageBox::warning( this, "No search image selected", " Image registration will be impossible");
	};

	if (this->searchImageSelector->getSelectedImage()) 
	{
		this->searchImage = this->searchImageSelector->getSelectedImage();
		this->hasSearchImage = true;
		this->SearchImageRes = this->searchImage->getApproxGSD(project.getMeanHeight());
		if (this->SearchImageRes > 0) this->SearchImageRes = floor(this->SearchImageRes * 100.0 + 0.5) * 0.01;
		this->SearchImageResolution->blockSignals(true);
		QString ds=QString("%1").arg(this->SearchImageRes, 0, 'f', 2);
		this->SearchImageResolution->setText(ds);
		this->SearchImageResolution->blockSignals(false);

		this->initDefaultFilename(this->searchImage->getHugeImage()->getFileNameChar());
		this->ResampledImageLineEdit->blockSignals(true);
		this->ResampledImageLineEdit->setText(this->resampleFileName.GetChar());
		this->ResampledImageLineEdit->blockSignals(false);
	}
	else 
	{
		this->hasSearchImage = false;
		this->searchImage = 0;
	}
	
	this->checkSetup();
}

void bui_ImageRegistrationDialog::onRefResChanged()
{
	this->RefImageResolution->blockSignals(true);

	bool OK;
	double hlp = this->RefImageResolution->text().toDouble(&OK);
	if (OK && (hlp > 0.0)) this->RefImageRes = hlp;
	
	QString ds=QString("%1").arg(this->RefImageRes, 0, 'f', 2);
	this->RefImageResolution->setText(ds);
	
	this->RefImageResolution->blockSignals(false);
	checkSetup();
};

void bui_ImageRegistrationDialog::onSearchResChanged()
{
	this->SearchImageResolution->blockSignals(true);
	bool OK;
	double hlp = this->SearchImageResolution->text().toDouble(&OK);
	if (OK && (hlp > 0.0)) this->SearchImageRes = hlp;

	QString ds=QString("%1").arg(this->SearchImageRes, 0, 'f', 2);
	this->SearchImageResolution->setText(ds);

	this->SearchImageResolution->blockSignals(false);
	checkSetup();
};

void bui_ImageRegistrationDialog::onCorrCoeffChanged()
{
  	bool OK = false;
	float corrcoeff = this->CorrCoeffSelector->text().toDouble(&OK);
	if (OK)
	{
		if ((corrcoeff > 0.0) && (corrcoeff < 1.0))
			this->minCorrCoeff = corrcoeff;
	}

	QString ds=QString("%1").arg(this->minCorrCoeff, 0, 'f', 5);
	this->CorrCoeffSelector->setText(ds);
};

void bui_ImageRegistrationDialog::onParallaxChanged()
{
  	bool OK = false;
	float pl = this->MaxParalax->text().toDouble(&OK);
	if (OK)
	{
		if (pl > 0.0) this->maxparallax = pl;
	}

	QString ds=QString("%1").arg(this->maxparallax, 0, 'f', 1);
	this->MaxParalax->setText(ds);
};
		  
void bui_ImageRegistrationDialog::onModelChanged()
{
	int indexSelected = (this->SensorModelComboBox->currentIndex());
	CCharString name((const char *) this->SensorModelComboBox->itemText(indexSelected).toLatin1());

	if (indexSelected == 0) this->model = eAFFINEREGISTRATION;

};


void bui_ImageRegistrationDialog::onFeatureExtractionParsChanged()
{
	CFileName fn("RegFEX.ini");
	fn.SetPath(project.getFileName()->GetPath());

	bui_FeatureExtractionDialog featureExtractor(this, this->featureParameters, true, false, false, fn);
	featureExtractor.show();
	featureExtractor.exec();
	if (featureExtractor.parametersAccepted())
		this->featureParameters = featureExtractor.getParameters();
};

void bui_ImageRegistrationDialog::keepPointsChanged()
{
	this->AddPoints->blockSignals(true);
	this->keepPoints = !this->keepPoints;
	this->AddPoints->setChecked(this->keepPoints);
	this->AddPoints->blockSignals(false);
}

void bui_ImageRegistrationDialog::TransformImageChanged()
{
	this->TransformImageCheckBox->blockSignals(true);
	this->resampleImage = !this->resampleImage;
	this->TransformImageCheckBox->setChecked(this->resampleImage);
	this->TransformImageCheckBox->blockSignals(false);
}

void bui_ImageRegistrationDialog::onTransferGCPsChanged()
{
	this->transferCheckBox->blockSignals(true);
	this->transferGCPs = !this->transferGCPs;
	this->transferCheckBox->setChecked(this->transferGCPs);
	this->transferCheckBox->blockSignals(false);
}

void bui_ImageRegistrationDialog::maxSigma0Changed()
{
  	bool OK = false;
	this->sigma0->blockSignals(true);
	float pl = this->sigma0->text().toDouble(&OK);
	if (OK)
	{
		if (pl > 0.0) this->maxsigma0 = pl;
	}

	QString ds=QString("%1").arg(this->maxsigma0, 0, 'f', 1);
	this->sigma0->setText(ds);
	this->sigma0->blockSignals(false);
};

void bui_ImageRegistrationDialog::OK()
{
	inifile.setRegistrationCrossCorr(this->minCorrCoeff);
	inifile.setRegistrationParallax(this->maxparallax);
	inifile.setRegistrationSigma0(this->maxsigma0);
	inifile.setRegistrationAddPoints(this->transferGCPs);
	inifile.writeIniFile();

	CFileName fn("RegFEX.ini");
	fn.SetPath(project.getFileName()->GetPath());
	this->featureParameters.writeParameters(fn);

	this->matcher = new CRegistrationMatcher (this->referenceImage, this->searchImage, this->RefImageRes, this->SearchImageRes);
	this->matcher->setMinCrossCorr(this->minCorrCoeff);
	this->matcher->setFeatureExtractionPars(this->featureParameters);
	this->matcher->setSensorModel(this->model);
	this->matcher->setMaxParallaxDifference(this->maxparallax);
	CFileName protfile("registration.prt");
	protfile.SetPath(project.getFileName()->GetPath());
	this->matcher->initProtocol(protfile);

	this->buttonOk->setEnabled(false);
	this->buttonCancel->setEnabled(false);
	this->update();

	bui_ProgressDlg dlg(new CImgRegistrationProcessingThread(this), this->matcher,"Image registration");
	dlg.exec();

	this->finished = dlg.getSuccess();

	delete this->matcher;
	this->matcher = 0;

	if (!this->finished)
	{		
		QMessageBox::critical(this, "Registration failed!", "Registration failed!");
	}

	this->close();
};

void bui_ImageRegistrationDialog::Cancel()
{
	this->close();
}

void bui_ImageRegistrationDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/ImageRegistrationDlg.html");
};

bool bui_ImageRegistrationDialog::compute()
{
	if (!this->matcher) return false;

	if (this->trafo) delete this->trafo;
	this->trafo = 0;


	int maxlevel1 = this->referenceImage->getHugeImage()->getNumberOfPyramidLevels() - 1;
	int maxlevel2 = this->searchImage->getHugeImage()->getNumberOfPyramidLevels() - 1;

	if (maxlevel1 > 5) maxlevel1 = 5;
	double startGSD1 = this->RefImageRes * pow(2.0, maxlevel1);
	double startGSD2 = this->SearchImageRes * pow(2.0, maxlevel2);
	if (startGSD2 < startGSD1) startGSD1 = startGSD2;
	
	bool ret = this->matcher->match(startGSD1, this->keepPoints);

	if (ret)
	{	
		if (this->maxsigma0 < matcher->getSigma0()) ret = false;
		else 
		{
			bool refresh = false;
			if (this->transferGCPs) 
			{
				refresh = matcher->MoveMeasuredPoints();	
			}

			if (this->resampleImage)
			{
				CVImage *img = project.addImage(this->resampleFileName, false);

				CHugeImage *hug = this->searchImage->getHugeImage();
				CCharString fn = this->resampleFileName + ".hug";

				hug->setProgressListener(this->matcher->getListener());

				if(!img->getHugeImage()->resample(fn.GetChar(), hug->getWidth(), hug->getHeight(), hug, *(this->matcher->getTrafo()), eBicubic))
				{
					project.deleteImage(img);
				}
				else refresh = true;

				hug->removeProgressListener();
			}

			if (refresh) baristaProjectHelper.refreshTree();
		}
	}
	
	return ret;
}

void bui_ImageRegistrationDialog::checkSetup()
{
	if ((this->hasRefImage)     && (this->hasSearchImage) && 
		(this->RefImageRes > 0) && (this->SearchImageRes > 0))
	{
		this->buttonOk->setEnabled(true);
	}
	else
	{
		this->buttonOk->setEnabled(false);
	}
}
	
void bui_ImageRegistrationDialog::FileNamePushed()
{
	QString s = QFileDialog::getSaveFileName(
		this,
		"Choose a file name",
		resampleFileName.GetChar(),
		"TIFF files (*.tif)");


	this->resampleFileName = s.toLatin1();

	this->ResampledImageLineEdit->blockSignals(true);
	this->ResampledImageLineEdit->setText(this->resampleFileName.GetChar());
	this->ResampledImageLineEdit->blockSignals(false);
};
