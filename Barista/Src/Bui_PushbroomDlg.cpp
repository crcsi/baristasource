#include "Bui_PushbroomDlg.h"


#include "Bui_CameraMountingDlg.h"
#include "Bui_OrbitParameterDlg.h"
#include "Bui_CameraDlg.h"
#include "SplineModelFactory.h"
#include "BaristaProject.h"

Bui_PushbroomDlg::Bui_PushbroomDlg(CPushBroomScanner* existingScanner,QWidget *parent) 
	: CAdjustmentDlg(parent),cameraMountingDlg(0),pbs(*existingScanner),
	path(*existingScanner->getOrbitPath()),
	attitudes(*existingScanner->getOrbitAttitudes()),
	cameraMounting(*existingScanner->getCameraMounting()),
	camera(*existingScanner->getCamera()),existingScanner(existingScanner),
	existingPath(existingScanner->getOrbitPath()), exstingAttitudes(existingScanner->getOrbitAttitudes()),
	existingCameraMounting(existingScanner->getCameraMounting()), existingCamera(existingScanner->getCamera())
	
{
	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->existingPath->addListener(this);
	this->exstingAttitudes->addListener(this);
	this->existingScanner->addListener(this);
	this->existingCameraMounting->addListener(this);
	this->existingCamera->addListener(this);

	this->orbitPathDlg = new Bui_OrbitParameterDlg(&this->path,Path,this);
	this->gridLayout1->addWidget(this->orbitPathDlg);

	this->orbitAttitudeDlg  = new Bui_OrbitParameterDlg(&this->attitudes,Attitude,this);
	this->gridLayout3->addWidget(this->orbitAttitudeDlg);

	this->cameraMountingDlg = new Bui_CameraMountingDlg(&this->cameraMounting,false,this);
	this->gridLayout5->addWidget(this->cameraMountingDlg);

	this->cameraDlg = new Bui_CameraDlg(&this->camera,&this->pbs,this);
	this->gridLayout7->addWidget(this->cameraDlg);

	QObject::connect(this->cameraMountingDlg, SIGNAL(enableOkButton(bool)),this,SLOT(cameraMountingStatusChanged(bool)));
	QObject::connect(this->orbitPathDlg, SIGNAL(enableOkButton(bool)),this,SLOT(pathStatusChanged(bool)));
	QObject::connect(this->orbitAttitudeDlg, SIGNAL(enableOkButton(bool)),this,SLOT(attitudeStatusChanged(bool)));
	QObject::connect(this->cameraDlg, SIGNAL(enableOkButton(bool)),this,SLOT(cameraStatusChanged(bool)));
	
	// set the default tab
	this->setDefaultTab(1); 
	this->connect(this->tWPushBroom,SIGNAL(currentChanged (int)),this, SLOT(tWCurrentChanged( int)));
	
}



Bui_PushbroomDlg::~Bui_PushbroomDlg()
{
	this->cleanUp();
}

void Bui_PushbroomDlg::cleanUp()
{
	if (this->existingPath)
		this->existingPath->removeListener(this);
	this->existingPath = NULL;
	
	if (this->exstingAttitudes)
		this->exstingAttitudes->removeListener(this);
	this->exstingAttitudes = NULL;

	if (this->existingScanner)
		this->existingScanner->removeListener(this);
	this->existingScanner = NULL;	

	if (this->existingCameraMounting)
		this->existingCameraMounting->removeListener(this);
	this->existingCameraMounting = NULL;


	if (this->existingCamera)
		this->existingCamera->removeListener(this);
	this->existingCamera = NULL;
	

	if (this->cameraMountingDlg)
		delete this->cameraMountingDlg;
	this->cameraMountingDlg = NULL;

	if (this->orbitPathDlg)
		delete this->orbitPathDlg;
	this->orbitPathDlg = NULL;

	if (this->orbitAttitudeDlg)
		delete this->orbitAttitudeDlg;
	this->orbitAttitudeDlg = NULL;

	if (this->cameraDlg)
		delete this->cameraDlg;
	this->cameraDlg = NULL;
}

void Bui_PushbroomDlg::setDefaultTab(int defaultTab) 
{
	this->defaultTab = defaultTab;

	if (this->defaultTab >=0 && this->defaultTab < this->tWPushBroom->count())
		this->tWPushBroom->setCurrentIndex(this->defaultTab);
}


bool Bui_PushbroomDlg::okButtonReleased()
{

	if (!this->cameraMountingDlg->okButtonReleased())
		return false;


	if (!this->orbitPathDlg->okButtonReleased())
		return false;

	if (!this->orbitAttitudeDlg->okButtonReleased())
		return false;

	if (!this->cameraDlg->okButtonReleased())
		return false;


	if (this->existingScanner)
	{
		this->listenToSignal = false;
		(*this->existingScanner) = this->pbs;
		(*this->existingScanner->getCameraMounting()) = this->cameraMounting;
		(*this->existingScanner->getOrbitPath()) = this->path;
		(*this->existingScanner->getOrbitAttitudes()) = this->attitudes;
		(*this->existingScanner->getCamera()) = this->camera;

//		project.setUseOrbitOvsevationHandler(this->existingPath,this->orbitPathDlg->getUseOrbitObservationHandler());
//		project.setUseOrbitOvsevationHandler(this->exstingAttitudes,this->orbitAttitudeDlg->getUseOrbitObservationHandler());
		this->listenToSignal = true;
	}

	this->success = true;
	return true;
}

bool Bui_PushbroomDlg::cancelButtonReleased()
{
	this->success = false;
	return true;
}

CCharString Bui_PushbroomDlg::helpButtonReleased()
{
	return "UserGuide/ThumbNodePushBroomScanner.html";
}


void Bui_PushbroomDlg::cameraMountingStatusChanged(bool b)
{
	if (b)
	{
		this->tWPushBroom->setTabEnabled(0,true);
		this->tWPushBroom->setTabEnabled(1,true);
		this->tWPushBroom->setTabEnabled(3,true);
	}
	else
	{
		this->tWPushBroom->setTabEnabled(0,false);
		this->tWPushBroom->setTabEnabled(1,false);
		this->tWPushBroom->setTabEnabled(3,false);
	}

	emit enableOkButton(b);
}

void Bui_PushbroomDlg::cameraStatusChanged(bool b)
{
	if (b)
	{
		this->tWPushBroom->setTabEnabled(0,true);
		this->tWPushBroom->setTabEnabled(1,true);
		this->tWPushBroom->setTabEnabled(2,true);
	}
	else
	{
		this->tWPushBroom->setTabEnabled(0,false);
		this->tWPushBroom->setTabEnabled(1,false);
		this->tWPushBroom->setTabEnabled(2,false);
	}

	emit enableOkButton(b);
}

void Bui_PushbroomDlg::pathStatusChanged(bool b)
{
	if (b)
	{
		this->tWPushBroom->setTabEnabled(1,true);
		this->tWPushBroom->setTabEnabled(2,true);
		this->tWPushBroom->setTabEnabled(3,true);
	}
	else
	{
		this->tWPushBroom->setTabEnabled(1,false);
		this->tWPushBroom->setTabEnabled(2,false);
		this->tWPushBroom->setTabEnabled(3,false);
	}

	emit enableOkButton(b);
}

void Bui_PushbroomDlg::attitudeStatusChanged(bool b)
{
	if (b)
	{
		this->tWPushBroom->setTabEnabled(0,true);
		this->tWPushBroom->setTabEnabled(2,true);
		this->tWPushBroom->setTabEnabled(3,true);
	}
	else
	{
		this->tWPushBroom->setTabEnabled(0,false);
		this->tWPushBroom->setTabEnabled(2,false);
		this->tWPushBroom->setTabEnabled(3,false);
	}

	emit enableOkButton(b);
}


void Bui_PushbroomDlg::tWCurrentChanged( int index )
{
	if (this->lastTab != -1)
	{
		if (this->lastTab == 0)
		{
			this->orbitPathDlg->okButtonReleased();		
		}
		else if (this->lastTab == 1)
		{
			this->orbitAttitudeDlg->okButtonReleased();
		}
		else if (this->lastTab == 2)
		{
			this->cameraMountingDlg->okButtonReleased();
		}
		else if (this->lastTab == 3)
		{
			this->cameraDlg->okButtonReleased();
		}
	}

	this->lastTab = index;

}


void Bui_PushbroomDlg::elementsChangedSlot(CLabel element)
{
if (!this->listenToSignal)
		return;

	if (element  == "CORBITPATHMODEL")
		this->path = *this->existingPath;
	else if (element  == "CORBITATTITUDEMODEL")
		this->attitudes = *this->exstingAttitudes;
	else if  (element  == "CCCDLINE")
		this->camera = *this->existingCamera;
	else if  (element  == "CCAMERAMOUNTING")
		this->cameraMounting = *this->existingCameraMounting;
	else if (element == "CPUSHBROOMSCANNER")
		this->pbs = *this->existingScanner;
}
