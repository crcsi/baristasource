#include "Bui_OrbitSettings.h"
#include "SplineModelFactory.h"
#include "OrbitPathModel.h"
#include "utilities.h"
#include "BaristaHtmlHelp.h"
#include "BaristaProject.h"
#include <QMessageBox>
#include <QDoubleValidator>




Bui_OrbitSettings::Bui_OrbitSettings(CSensorModel* sensorModel,SplineModelType splineType,QWidget *parent)
	: CAdjustmentDlg(parent),splineType(splineType),
	bPathShiftSelected(false),bPathRotationSelected(false),bAttitudeRotationSelected(false),
	bAttitudeTimeRotationSelected(false),showInOrbitalSystem(true),useOrbitObservations(true),
	fixedMatrix1(3,3)
{
	setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	if (this->splineType == 1)
	{
		// make local copy but keep reference
		this->path = *(COrbitPathModel*)sensorModel;
		this->existingPath = (COrbitPathModel*)sensorModel;
		this->existingPath->addListener(this);
		CCharString pathName = *this->path.getFileName();
		
		if (!project.getFixedRotationForOrbit(this->fixedMatrix2,pathName))
		{
			this->rB_Orbit->setEnabled(false);
			this->rB_Global->setChecked(true);
			this->showInOrbitalSystem = false;
		}
		else
		{
			this->rB_Orbit->setEnabled(true);
			this->rB_Orbit->setChecked(true);
			for (int i=0; i<3;i++)
				for (int k=0;k<3;k++)
					this->fixedMatrix1.element(i,k) = this->fixedMatrix2(i,k);

		}
		this->initPath();
	}
	else if (this->splineType == 2)
	{
		this->showInOrbitalSystem= false ;
		this->rB_Global->setEnabled(false);
		this->rB_Orbit->setEnabled(false);
		// make local copy but keep reference
		this->attitudes =  *(COrbitAttitudeModel*)sensorModel;
		this->existingAttitudes = (COrbitAttitudeModel*)sensorModel;
		this->existingAttitudes->addListener(this);
		CCharString attitudeName= *this->attitudes.getFileName();
		
/*
		if (!project.getFixedRotationForOrbit(this->fixedMatrix,attitudeName))
		{
			this->rB_Orbit->setEnabled(false);
			this->rB_Global->setChecked(true);
			this->showInOrbitalSystem = false;
		}
		else
		{
			this->rB_Orbit->setEnabled(true);
			this->rB_Orbit->setChecked(true);
			
		}
		*/
		this->initAttitudes();
	}
	else
		return;

	this->connect(this->pB_ApplyAll,SIGNAL(released()),this,SLOT(pBAllReleased()));
	this->connect(this->rB_Global,SIGNAL(released()),this,SLOT(rBGlobalReleased()));
	this->connect(this->rB_Orbit,SIGNAL(released()),this,SLOT(rBOrbitReleased()));

	this->applySettings();
	this->checkAvailabilityOfSettingsPath();
	this->checkAvailabilityOfSettingsAttitudes();
}

CCharString Bui_OrbitSettings::getTitleString()
{
	if (this->splineType == 1)
	{
		return "Orbit Path settings";
	}
	else if (this->splineType == 2)
	{
		return "Orbit Attitude settings";
	}
	else 
		return "";
	

}

void Bui_OrbitSettings::initPath()
{
	this->sW->setCurrentIndex(0);

	this->copyDataInLocalPathVariables();

	this->connect(this->cB_PathOffset,SIGNAL(stateChanged(int)),this,SLOT(cBPathShiftStateChanged(int)));
	this->connect(this->cB_PathRotation,SIGNAL(stateChanged(int)),this,SLOT(cBPathRotationStateChanged(int)));
	this->connect(this->lE_PathXAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEPathXShiftAccuracyTextChanged(const QString &)));
	this->connect(this->lE_PathYAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEPathYShiftAccuracyTextChanged(const QString &)));
	this->connect(this->lE_PathZAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEPathZShiftAccuracyTextChanged(const QString &)));
	this->connect(this->lE_PathRollAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEPathRollAccuracyTextChanged(const QString &)));
	this->connect(this->lE_PathPitchAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEPathPitchAccuracyTextChanged(const QString &)));
	this->connect(this->lE_PathYawAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEPathYawAccuracyTextChanged(const QString &)));
	this->connect(this->pB_PathShiftReset,SIGNAL(released()),this,SLOT(pBPathShiftResetReleased()));
	this->connect(this->pB_PathShiftAll,SIGNAL(released()),this,SLOT(pBPathShiftAllReleased()));
	this->connect(this->pB_PathRotationReset,SIGNAL(released()),this,SLOT(pBPathRotationResetReleased()));
	this->connect(this->pB_PathRotationAll,SIGNAL(released()),this,SLOT(pBPathRotationAllReleased()));


	this->lE_PathXAccuracy->setValidator(new QDoubleValidator(0.0,100000.0,8,this));
	this->lE_PathYAccuracy->setValidator(new QDoubleValidator(0.0,100000.0,8,this));
	this->lE_PathZAccuracy->setValidator(new QDoubleValidator(0.0,100000.0,8,this));


	this->lE_PathRollAccuracy->setValidator(new QDoubleValidator(0.0,180.0,10,this));
	this->lE_PathPitchAccuracy->setValidator(new QDoubleValidator(0.0,180.0,10,this));
	this->lE_PathYawAccuracy->setValidator(new QDoubleValidator(0.0,180.0,10,this));


}


void Bui_OrbitSettings::copyDataInLocalPathVariables()
{
	CCharString name(*this->path.getFileName());
	this->lE_Name->setText(name.GetChar());
	this->lE_Name->setToolTip(name.GetChar());

	this->pathShiftParameter = this->path.getShift();
	this->pathRotationParameter = this->path.getRotation();

	this->pathShiftObservation = this->path.getDirectObservationForShift();
	this->pathRotationObservation = this->path.getDirectObservationForAngles();

	this->bPathShiftSelected = this->path.getParameterCount(CSensorModel::shiftPar) == 0 ? false : true;
	this->bPathRotationSelected = this->path.getParameterCount(CSensorModel::rotPar) == 0 ? false : true;

	if (this->showInOrbitalSystem)
		this->transformToOrbitalSystem();
}


void Bui_OrbitSettings::transform(CXYZPoint& parameter,bool inverse)
{
		C3DPoint tmpResult;
		const Matrix& Qll = parameter.getCovar();
		Matrix Qxx;

		if (inverse)
		{
			Qxx = this->fixedMatrix1 * Qll *this->fixedMatrix1.t();
			this->fixedMatrix2.R_times_x(tmpResult,parameter);
		}
		else
		{
			Qxx = this->fixedMatrix1.t() * Qll *this->fixedMatrix1;
			this->fixedMatrix2.RT_times_x(tmpResult,parameter);
		}

		for (int i=0; i<3;i++)
			parameter[i] = tmpResult[i];

		parameter.setCovariance(Qxx);

}


void Bui_OrbitSettings::transformToOrbitalSystem(bool inverse)
{
	if (this->splineType == 1)
	{

		// shift parameter
		this->transform(this->pathShiftParameter,inverse);

		// shift observation
		this->transform(this->pathShiftObservation,inverse);

		// rotation parameter
		this->transform(this->pathRotationParameter,inverse);

		// rotation observation
		this->transform(this->pathRotationObservation,inverse);

	}
	else if (this->splineType == 2)
	{
		
		// constant rot parameter
		this->transform(this->attitudeRotationParameter,inverse);

		// constant rot observation
		this->transform(this->attitudeRotationObservation,inverse);

		// time rot parameter
		this->transform(this->attitudeTimeRotationParameter,inverse);

		// time rot observation
		this->transform(this->attitudeTimeRotationObservation,inverse);
	}

}

void Bui_OrbitSettings::initAttitudes()
{
	this->sW->setCurrentIndex(1);

	this->copyDataInLocalAttitudeVariables();


	this->connect(this->cB_AttitudeOffset,SIGNAL(stateChanged(int)),this,SLOT(cBAttitudeShiftStateChanged(int)));
	this->connect(this->cB_AttitudeTimeRotation,SIGNAL(stateChanged(int)),this,SLOT(cBAttitudeRotationStateChanged(int)));
	this->connect(this->lE_AttitudeXAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEAttitudeXShiftAccuracyTextChanged(const QString &)));
	this->connect(this->lE_AttitudeYAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEAttitudeYShiftAccuracyTextChanged(const QString &)));
	this->connect(this->lE_AttitudeZAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEAttitudeZShiftAccuracyTextChanged(const QString &)));
	this->connect(this->lE_AttitudeTimeRollAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEAttitudeTimeRollAccuracyTextChanged(const QString &)));
	this->connect(this->lE_AttitudeTimePitchAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEAttitudeTimePitchAccuracyTextChanged(const QString &)));
	this->connect(this->lE_AttitudeTimeYawAccuracy,SIGNAL(textChanged(const QString &)),this,SLOT(lEAttitudeTimeYawAccuracyTextChanged(const QString &)));
	this->connect(this->pB_AttitudeShiftReset,SIGNAL(released()),this,SLOT(pBAttitudeShiftResetReleased()));
	this->connect(this->pB_AttitudeShiftAll,SIGNAL(released()),this,SLOT(pBAttitudeShiftAllReleased()));
	this->connect(this->pB_AttitudeTimeRotationReset,SIGNAL(released()),this,SLOT(pBAttitudeTimeRotationResetReleased()));
	this->connect(this->pB_AttitudeTimeRotationAll,SIGNAL(released()),this,SLOT(pBAttitudeTimeRotationAllReleased()));



	this->lE_AttitudeXAccuracy->setValidator(new QDoubleValidator(0.0,1000.0,10,this));
	this->lE_AttitudeYAccuracy->setValidator(new QDoubleValidator(0.0,1000.0,10,this));
	this->lE_AttitudeZAccuracy->setValidator(new QDoubleValidator(0.0,1000.0,10,this));


	this->lE_AttitudeTimeRollAccuracy->setValidator(new QDoubleValidator(0.0,1000.0,10,this));
	this->lE_AttitudeTimePitchAccuracy->setValidator(new QDoubleValidator(0.0,1000.0,10,this));
	this->lE_AttitudeTimeYawAccuracy->setValidator(new QDoubleValidator(0.0,1000.0,10,this));




}

void Bui_OrbitSettings::copyDataInLocalAttitudeVariables()
{
	CCharString name(*this->attitudes.getFileName());

	this->lE_Name->setText(name.GetChar());
	this->lE_Name->setToolTip(name.GetChar());

	this->attitudeRotationParameter = this->attitudes.getRotParameter();
	this->attitudeTimeRotationParameter = this->attitudes.getScaleParameter();

	this->attitudeRotationObservation = this->attitudes.getDirectObservationForAngles();
	this->attitudeTimeRotationObservation = this->attitudes.getDirectObservationForScale();

	this->bAttitudeRotationSelected = this->attitudes.getParameterCount(CSensorModel::rotPar) == 0 ? false : true;
	this->bAttitudeTimeRotationSelected = this->attitudes.getParameterCount(CSensorModel::scalePar) == 0 ? false : true;

	if (this->showInOrbitalSystem)
		this->transformToOrbitalSystem();
}


Bui_OrbitSettings::~Bui_OrbitSettings()
{
	this->cleanUp();
}

void Bui_OrbitSettings::cleanUp()
{
	if (this->splineType == 1 && this->existingPath)
		this->existingPath->removeListener(this);
	else if (this->splineType == 2 && this->existingAttitudes)
		this->existingAttitudes->removeListener(this);

	this->existingPath = NULL;
	this->existingAttitudes = NULL;
}


void Bui_OrbitSettings::applySettings()
{
	if (this->splineType == 1)
		this->applySettingsPath();
	else if (this->splineType == 2)
		this->applySettingsAttitudes();
}

void Bui_OrbitSettings::checkAvailabilityOfSettingsPath()
{
	if (!this->lE_PathXAccuracy->hasAcceptableInput() ||
		!this->lE_PathYAccuracy->hasAcceptableInput() ||
		!this->lE_PathZAccuracy->hasAcceptableInput() ||
		!this->lE_PathRollAccuracy->hasAcceptableInput() ||
		!this->lE_PathPitchAccuracy->hasAcceptableInput() ||
		!this->lE_PathYawAccuracy->hasAcceptableInput() )
	{
		emit enableOkButton(false);
		this->pB_ApplyAll->setEnabled(false);
	}
	else
	{
		emit enableOkButton(true);
		this->pB_ApplyAll->setEnabled(true);
	}



	if (this->bPathShiftSelected)
	{
		this->cB_PathOffset->setChecked(true);
		this->gB_PathOffset->setEnabled(true);
		this->pB_PathShiftReset->setEnabled(true);
		this->pB_PathShiftAll->setEnabled(true);
	}
	else
	{
		this->cB_PathOffset->setChecked(false);
		this->gB_PathOffset->setEnabled(false);
		this->pB_PathShiftReset->setEnabled(false);
		this->pB_PathShiftAll->setEnabled(false);
	}

	if (this->bPathRotationSelected)
	{
		this->cB_PathRotation->setChecked(true);
		this->gB_PathRotation->setEnabled(true);
		this->pB_PathRotationReset->setEnabled(true);
		this->pB_PathRotationAll->setEnabled(true);
	}
	else
	{
		this->cB_PathRotation->setChecked(false);
		this->gB_PathRotation->setEnabled(false);
		this->pB_PathRotationReset->setEnabled(false);
		this->pB_PathRotationAll->setEnabled(false);
	}

}

void Bui_OrbitSettings::checkAvailabilityOfSettingsAttitudes()
{
	if (!this->lE_AttitudeXAccuracy->hasAcceptableInput() ||
		!this->lE_AttitudeYAccuracy->hasAcceptableInput() ||
		!this->lE_AttitudeZAccuracy->hasAcceptableInput() ||
		!this->lE_AttitudeTimeRollAccuracy->hasAcceptableInput() ||
		!this->lE_AttitudeTimePitchAccuracy->hasAcceptableInput() ||
		!this->lE_AttitudeTimeYawAccuracy->hasAcceptableInput() )
	{
		emit enableOkButton(false);
		this->pB_ApplyAll->setEnabled(false);
	}
	else
	{
		emit enableOkButton(true);
		this->pB_ApplyAll->setEnabled(true);
	}



	if (this->bAttitudeRotationSelected)
	{
		this->cB_AttitudeOffset->setChecked(true);
		this->gB_AttitudeOffset->setEnabled(true);
		this->pB_AttitudeShiftReset->setEnabled(true);
		this->pB_AttitudeShiftAll->setEnabled(true);
	}
	else
	{
		this->cB_AttitudeOffset->setChecked(false);
		this->gB_AttitudeOffset->setEnabled(false);
		this->pB_AttitudeShiftReset->setEnabled(false);
		this->pB_AttitudeShiftAll->setEnabled(false);
	}

	if (this->bAttitudeTimeRotationSelected)
	{
		this->cB_AttitudeTimeRotation->setChecked(true);
		this->gB_AttitudeTimeRotation->setEnabled(true);
		this->pB_AttitudeTimeRotationReset->setEnabled(true);
		this->pB_AttitudeTimeRotationAll->setEnabled(true);
	}
	else
	{
		this->cB_AttitudeTimeRotation->setChecked(false);
		this->gB_AttitudeTimeRotation->setEnabled(false);
		this->pB_AttitudeTimeRotationReset->setEnabled(false);
		this->pB_AttitudeTimeRotationAll->setEnabled(false);
	}
}

void Bui_OrbitSettings::applySettingsPath()
{
	this->cB_PathOffset->blockSignals(true);
	this->cB_PathRotation->blockSignals(true);
	this->lE_PathXAccuracy->blockSignals(true);
	this->lE_PathYAccuracy->blockSignals(true);
	this->lE_PathZAccuracy->blockSignals(true);
	this->lE_PathRollAccuracy->blockSignals(true);
	this->lE_PathPitchAccuracy->blockSignals(true);
	this->lE_PathYawAccuracy->blockSignals(true);


	this->checkAvailabilityOfSettingsPath();

	QString tooltip("adjusted Parameter: ");
	QString shiftString = this->setDoubleText(this->pathShiftParameter.getCovarElement(0,0),3);
	this->setDoubleText(this->lE_PathXParameter,this->pathShiftParameter.x,3);
	this->lE_PathXParameter->setText(this->lE_PathXParameter->text() + "  +- " + shiftString);
	this->lE_PathXParameter->setToolTip(tooltip + this->lE_PathXParameter->text());
	
	shiftString = this->setDoubleText(this->pathShiftParameter.getCovarElement(1,1),3);
	this->setDoubleText(this->lE_PathYParameter,this->pathShiftParameter.y,3);
	this->lE_PathYParameter->setText(this->lE_PathYParameter->text() + "  +- " + shiftString);
	this->lE_PathYParameter->setToolTip(tooltip + this->lE_PathYParameter->text());

	shiftString = this->setDoubleText(this->pathShiftParameter.getCovarElement(2,2),3);
	this->setDoubleText(this->lE_PathZParameter,this->pathShiftParameter.z,3);
	this->lE_PathZParameter->setText(this->lE_PathZParameter->text() + "  +- " + shiftString);
	this->lE_PathZParameter->setToolTip(tooltip + this->lE_PathZParameter->text());



	shiftString = this->setDoubleText(this->pathRotationParameter.getCovarElement(0,0)*180.0/M_PI,3);
	this->setDoubleText(this->lE_PathRollParameter,this->pathRotationParameter.x*180.0/M_PI,3);
	this->lE_PathRollParameter->setText(this->lE_PathRollParameter->text() + "  +- " + shiftString);
	this->lE_PathRollParameter->setToolTip(tooltip + this->lE_PathRollParameter->text());

	shiftString = this->setDoubleText(this->pathRotationParameter.getCovarElement(1,1)*180.0/M_PI,3);
	this->setDoubleText(this->lE_PathPitchParameter,this->pathRotationParameter.y*180.0/M_PI,3);
	this->lE_PathPitchParameter->setText(this->lE_PathPitchParameter->text() + "  +- " + shiftString);
	this->lE_PathPitchParameter->setToolTip(tooltip + this->lE_PathPitchParameter->text());
	
	shiftString = this->setDoubleText(this->pathRotationParameter.getCovarElement(2,2)*180.0/M_PI,3);
	this->setDoubleText(this->lE_PathYawParameter,this->pathRotationParameter.z*180.0/M_PI,3);
	this->lE_PathYawParameter->setText(this->lE_PathYawParameter->text() + "  +- " + shiftString);
	this->lE_PathYawParameter->setToolTip(tooltip + this->lE_PathYawParameter->text());



	this->setDoubleText(this->lE_PathXObservation,this->pathShiftObservation.x,3);
	this->setDoubleText(this->lE_PathYObservation,this->pathShiftObservation.y,3);
	this->setDoubleText(this->lE_PathZObservation,this->pathShiftObservation.z,3);

	this->setDoubleText(this->lE_PathXAccuracy,this->pathShiftObservation.getCovarElement(0,0),3);
	this->setDoubleText(this->lE_PathYAccuracy,this->pathShiftObservation.getCovarElement(1,1),3);
	this->setDoubleText(this->lE_PathZAccuracy,this->pathShiftObservation.getCovarElement(2,2),3);

	this->setDoubleText(this->lE_PathRollObservation,this->pathRotationObservation.x*180/M_PI,4);
	this->setDoubleText(this->lE_PathPitchObservation,this->pathRotationObservation.y*180/M_PI,4);
	this->setDoubleText(this->lE_PathYawObservation,this->pathRotationObservation.z*180/M_PI,4);

	this->setDoubleText(this->lE_PathRollAccuracy,this->pathRotationObservation.getCovarElement(0,0)*180/M_PI,4);
	this->setDoubleText(this->lE_PathPitchAccuracy,this->pathRotationObservation.getCovarElement(1,1)*180/M_PI,4);
	this->setDoubleText(this->lE_PathYawAccuracy,this->pathRotationObservation.getCovarElement(2,2)*180/M_PI,4);


	this->cB_PathOffset->blockSignals(false);
	this->cB_PathRotation->blockSignals(false);
	this->lE_PathXAccuracy->blockSignals(false);
	this->lE_PathYAccuracy->blockSignals(false);
	this->lE_PathZAccuracy->blockSignals(false);
	this->lE_PathRollAccuracy->blockSignals(false);
	this->lE_PathPitchAccuracy->blockSignals(false);
	this->lE_PathYawAccuracy->blockSignals(false);

}


void Bui_OrbitSettings::applySettingsAttitudes()
{
	this->cB_AttitudeOffset->blockSignals(true);
	this->cB_AttitudeTimeRotation->blockSignals(true);
	this->lE_AttitudeXAccuracy->blockSignals(true);
	this->lE_AttitudeYAccuracy->blockSignals(true);
	this->lE_AttitudeZAccuracy->blockSignals(true);
	this->lE_AttitudeTimeRollAccuracy->blockSignals(true);
	this->lE_AttitudeTimePitchAccuracy->blockSignals(true);
	this->lE_AttitudeTimeYawAccuracy->blockSignals(true);


	this->checkAvailabilityOfSettingsAttitudes();

	QString tooltip("adjusted Parameter: ");
	QString rotString = this->setDoubleText(this->attitudeRotationParameter.getCovarElement(0,0)*180.0/M_PI,3);
	this->setDoubleText(this->lE_AttitudeXParameter,this->attitudeRotationParameter.x*180.0/M_PI,3);
	this->lE_AttitudeXParameter->setText(this->lE_AttitudeXParameter->text() + "  +- " + rotString);
	this->lE_AttitudeXParameter->setToolTip(tooltip +this->lE_AttitudeXParameter->text());
	
	rotString = this->setDoubleText(this->attitudeRotationParameter.getCovarElement(1,1)*180.0/M_PI,3);
	this->setDoubleText(this->lE_AttitudeYParameter,this->attitudeRotationParameter.y*180.0/M_PI,3);
	this->lE_AttitudeYParameter->setText(this->lE_AttitudeYParameter->text() + "  +- " + rotString);
	this->lE_AttitudeYParameter->setToolTip(tooltip+this->lE_AttitudeYParameter->text());

	rotString = this->setDoubleText(this->attitudeRotationParameter.getCovarElement(2,2)*180.0/M_PI,3);
	this->setDoubleText(this->lE_AttitudeZParameter,this->attitudeRotationParameter.z*180.0/M_PI,3);
	this->lE_AttitudeZParameter->setText(this->lE_AttitudeZParameter->text() + "  +- " + rotString);
	this->lE_AttitudeZParameter->setToolTip(tooltip+this->lE_AttitudeZParameter->text());



	rotString = this->setDoubleText(this->attitudeTimeRotationParameter.getCovarElement(0,0),3);
	this->setDoubleText(this->lE_AttitudeTimeRollParameter,this->attitudeTimeRotationParameter.x,3);
	this->lE_AttitudeTimeRollParameter->setText(this->lE_AttitudeTimeRollParameter->text() + "  +- " + rotString);
	this->lE_AttitudeTimeRollParameter->setToolTip(tooltip+this->lE_AttitudeTimeRollParameter->text());

	rotString = this->setDoubleText(this->attitudeTimeRotationParameter.getCovarElement(1,1),3);
	this->setDoubleText(this->lE_AttitudeTimePitchParameter,this->attitudeTimeRotationParameter.y,3);
	this->lE_AttitudeTimePitchParameter->setText(this->lE_AttitudeTimePitchParameter->text() + "  +- " + rotString);
	this->lE_AttitudeTimePitchParameter->setToolTip(tooltip+this->lE_AttitudeTimePitchParameter->text());
	
	rotString = this->setDoubleText(this->attitudeTimeRotationParameter.getCovarElement(2,2),3);
	this->setDoubleText(this->lE_AttitudeTimeYawParameter,this->attitudeTimeRotationParameter.z,3);
	this->lE_AttitudeTimeYawParameter->setText(this->lE_AttitudeTimeYawParameter->text() + "  +- " + rotString);
	this->lE_AttitudeTimeYawParameter->setToolTip(tooltip+this->lE_AttitudeTimeYawParameter->text());



	this->setDoubleText(this->lE_AttitudeXObservation,this->attitudeRotationObservation.x*180/M_PI,3);
	this->setDoubleText(this->lE_AttitudeYObservation,this->attitudeRotationObservation.y*180/M_PI,3);
	this->setDoubleText(this->lE_AttitudeZObservation,this->attitudeRotationObservation.z*180/M_PI,3);

	this->setDoubleText(this->lE_AttitudeXAccuracy,this->attitudeRotationObservation.getCovarElement(0,0)*180/M_PI,3);
	this->setDoubleText(this->lE_AttitudeYAccuracy,this->attitudeRotationObservation.getCovarElement(1,1)*180/M_PI,3);
	this->setDoubleText(this->lE_AttitudeZAccuracy,this->attitudeRotationObservation.getCovarElement(2,2)*180/M_PI,3);

	this->setDoubleText(this->lE_AttitudeTimeRollObservation,this->attitudeTimeRotationObservation.x,4);
	this->setDoubleText(this->lE_AttitudeTimePitchObservation,this->attitudeTimeRotationObservation.y,4);
	this->setDoubleText(this->lE_AttitudeTimeYawObservation,this->attitudeTimeRotationObservation.z,4);

	this->setDoubleText(this->lE_AttitudeTimeRollAccuracy,this->attitudeTimeRotationObservation.getCovarElement(0,0),4);
	this->setDoubleText(this->lE_AttitudeTimePitchAccuracy,this->attitudeTimeRotationObservation.getCovarElement(1,1),4);
	this->setDoubleText(this->lE_AttitudeTimeYawAccuracy,this->attitudeTimeRotationObservation.getCovarElement(2,2),4);


	this->cB_AttitudeOffset->blockSignals(false);
	this->cB_AttitudeTimeRotation->blockSignals(false);
	this->lE_AttitudeXAccuracy->blockSignals(false);
	this->lE_AttitudeYAccuracy->blockSignals(false);
	this->lE_AttitudeZAccuracy->blockSignals(false);
	this->lE_AttitudeTimeRollAccuracy->blockSignals(false);
	this->lE_AttitudeTimePitchAccuracy->blockSignals(false);
	this->lE_AttitudeTimeYawAccuracy->blockSignals(false);

	
}


void Bui_OrbitSettings::setDoubleText(QLineEdit* le,double value, int decimals)
{

	QString text = this->setDoubleText(value,decimals);
	le->setText(text);
	le->setToolTip(text);

}

QString Bui_OrbitSettings::setDoubleText(double value, int decimals)
{
	QString text;
	double t = 1.0/pow(10.0,decimals);
	if (fabs(value) < t)
		text.sprintf("%5.2e",value);
	else
	{
		QString format;
		format.sprintf("%c5.%dlf",'%',decimals);
		text.sprintf(format.toLatin1(),value);
	}
	return text;
}

void Bui_OrbitSettings::cBPathShiftStateChanged(int state)
{
	if (state == Qt::Checked)
	{
		this->bPathShiftSelected = true;
		this->path.activateParameterGroup(CSensorModel::shiftPar);
	}
	else 
	{
		this->bPathShiftSelected = false;
		this->path.deactivateParameterGroup(CSensorModel::shiftPar);
	}

	this->applySettings();
}

void Bui_OrbitSettings::cBPathRotationStateChanged(int state)
{
	if (state == Qt::Checked)
	{
		this->bPathRotationSelected = true;
		this->path.activateParameterGroup(CSensorModel::rotPar);
	}
	else 
	{
		this->bPathRotationSelected = false;
		this->path.activateParameterGroup(CSensorModel::rotPar);
	}

	this->applySettings();
}


void Bui_OrbitSettings::lEPathXShiftAccuracyTextChanged(const QString & text)
{
	if (this->lE_PathXAccuracy->hasAcceptableInput())
		this->pathShiftObservation.setCovarElement(0,0,this->lE_PathXAccuracy->text().toDouble());


	this->checkAvailabilityOfSettingsPath();
}

void Bui_OrbitSettings::lEPathYShiftAccuracyTextChanged(const QString & text)
{
	if (this->lE_PathYAccuracy->hasAcceptableInput())
		this->pathShiftObservation.setCovarElement(1,1,this->lE_PathYAccuracy->text().toDouble());

	this->checkAvailabilityOfSettingsPath();
}

void Bui_OrbitSettings::lEPathZShiftAccuracyTextChanged(const QString & text)
{
	if (this->lE_PathZAccuracy->hasAcceptableInput())
		this->pathShiftObservation.setCovarElement(2,2,this->lE_PathZAccuracy->text().toDouble());

	this->checkAvailabilityOfSettingsPath();
}




void Bui_OrbitSettings::lEPathRollAccuracyTextChanged(const QString & text)
{
	if (this->lE_PathRollAccuracy->hasAcceptableInput())
		this->pathRotationObservation.setCovarElement(0,0, this->lE_PathRollAccuracy->text().toDouble() * M_PI / 180.0);

	this->checkAvailabilityOfSettingsPath();
}


void Bui_OrbitSettings::lEPathPitchAccuracyTextChanged(const QString & text)
{
	if (this->lE_PathPitchAccuracy->hasAcceptableInput())
		this->pathRotationObservation.setCovarElement(1,1, this->lE_PathPitchAccuracy->text().toDouble() * M_PI / 180.0);

	this->checkAvailabilityOfSettingsPath();
}

void Bui_OrbitSettings::lEPathYawAccuracyTextChanged(const QString & text)
{
	if (this->lE_PathYawAccuracy->hasAcceptableInput())
		this->pathRotationObservation.setCovarElement(2,2,this->lE_PathYawAccuracy->text().toDouble() * M_PI / 180.0);

	this->checkAvailabilityOfSettingsPath();
}


void Bui_OrbitSettings::pBPathShiftResetReleased()
{

	CXYZPoint tmp(0,0,0);
	if (this->showInOrbitalSystem)
	{
		this->transform(tmp);
	}

	this->pathShiftParameter = tmp;
	this->setParameter(0,true);

	this->applySettings();
	this->checkAvailabilityOfSettingsPath();
	this->checkAvailabilityOfSettingsAttitudes();

}

void Bui_OrbitSettings::pBPathShiftAllReleased()
{
	this->listenToSignal = false;
	this->pBPathShiftResetReleased();
	this->setAllParameter(true);
	this->listenToSignal = true;
}
void Bui_OrbitSettings::pBPathRotationResetReleased()
{
	CXYZPoint tmp(0,0,0);
	if (this->showInOrbitalSystem)
	{
		this->transform(tmp);
	}

	this->pathRotationParameter = tmp;
	this->setParameter(0,true);



	this->applySettings();
	this->checkAvailabilityOfSettingsPath();
	this->checkAvailabilityOfSettingsAttitudes();
}
void Bui_OrbitSettings::pBPathRotationAllReleased()
{
	this->listenToSignal = false;
	this->pBPathRotationResetReleased();
	this->setAllParameter(true);
	this->listenToSignal = true;
}








void Bui_OrbitSettings::cBAttitudeShiftStateChanged(int state)
{
	if (state == Qt::Checked)
	{
		this->bAttitudeRotationSelected = true;
	}
	else 
	{
		this->bAttitudeRotationSelected = false;
	}

	this->applySettings();
}

void Bui_OrbitSettings::cBAttitudeRotationStateChanged(int state)
{
	if (state == Qt::Checked)
		this->bAttitudeTimeRotationSelected = true;
	else 
		this->bAttitudeTimeRotationSelected = false;

	this->applySettings();
}


void Bui_OrbitSettings::lEAttitudeXShiftAccuracyTextChanged(const QString & text)
{
	if (this->lE_AttitudeXAccuracy->hasAcceptableInput())
		this->attitudeRotationObservation.setCovarElement(0,0,this->lE_AttitudeXAccuracy->text().toDouble()* M_PI / 180.0);

	this->checkAvailabilityOfSettingsAttitudes();
}

void Bui_OrbitSettings::lEAttitudeYShiftAccuracyTextChanged(const QString & text)
{
	if (this->lE_AttitudeYAccuracy->hasAcceptableInput())
		this->attitudeRotationObservation.setCovarElement(1,1,this->lE_AttitudeYAccuracy->text().toDouble()* M_PI / 180.0);

	this->checkAvailabilityOfSettingsAttitudes();
}

void Bui_OrbitSettings::lEAttitudeZShiftAccuracyTextChanged(const QString & text)
{
	if (this->lE_AttitudeZAccuracy->hasAcceptableInput())
		this->attitudeRotationObservation.setCovarElement(2,2,this->lE_AttitudeZAccuracy->text().toDouble()* M_PI / 180.0);

	this->checkAvailabilityOfSettingsAttitudes();
}




void Bui_OrbitSettings::lEAttitudeTimeRollAccuracyTextChanged(const QString & text)
{
	if (this->lE_AttitudeTimeRollAccuracy->hasAcceptableInput())
		this->attitudeTimeRotationObservation.setCovarElement(0,0, this->lE_AttitudeTimeRollAccuracy->text().toDouble());

	this->checkAvailabilityOfSettingsAttitudes();
}


void Bui_OrbitSettings::lEAttitudeTimePitchAccuracyTextChanged(const QString & text)
{
	if (this->lE_AttitudeTimePitchAccuracy->hasAcceptableInput())
		this->attitudeTimeRotationObservation.setCovarElement(1,1, this->lE_AttitudeTimePitchAccuracy->text().toDouble());

	this->checkAvailabilityOfSettingsAttitudes();
}

void Bui_OrbitSettings::lEAttitudeTimeYawAccuracyTextChanged(const QString & text)
{
	if (this->lE_AttitudeTimeYawAccuracy->hasAcceptableInput())
		this->attitudeTimeRotationObservation.setCovarElement(2,2,this->lE_AttitudeTimeYawAccuracy->text().toDouble());

	this->checkAvailabilityOfSettingsAttitudes();
}

void Bui_OrbitSettings::pBAttitudeShiftResetReleased()
{
	CXYZPoint tmp(0,0,0);
	if (this->showInOrbitalSystem)
	{
		this->transform(tmp);
	}
	
	this->attitudeRotationParameter = tmp;
	this->setParameter(0,true);

	this->applySettings();
	this->checkAvailabilityOfSettingsPath();
	this->checkAvailabilityOfSettingsAttitudes();
}

void Bui_OrbitSettings::pBAttitudeShiftAllReleased()
{
	this->listenToSignal = false;
	this->pBAttitudeShiftResetReleased();
	this->setAllParameter(true);
	this->listenToSignal = true;
}
void Bui_OrbitSettings::pBAttitudeTimeRotationResetReleased()
{

	CXYZPoint tmp(0,0,0);
	if (this->showInOrbitalSystem)
	{
		this->transform(tmp);
	}

	this->attitudeTimeRotationParameter = tmp;
	this->setParameter(0,true);

	this->applySettings();
	this->checkAvailabilityOfSettingsPath();
	this->checkAvailabilityOfSettingsAttitudes();
}
void Bui_OrbitSettings::pBAttitudeTimeRotationAllReleased()
{
	this->listenToSignal = false;
	this->pBAttitudeTimeRotationResetReleased();
	this->setAllParameter(true);
	this->listenToSignal = true;
}



bool Bui_OrbitSettings::okButtonReleased()
{
	

	if (this->splineType == 1)
	{
		this->setParameter(this->existingPath,true);
		// the 'existingPath' is shared with the SplineParameterDlg, so only update the parameter
		// which can be changed by this dlg
		this->existingPath->setParameterCount(CSensorModel::shiftPar,this->path.getParameterCount(CSensorModel::shiftPar));
		this->existingPath->setParameterCount(CSensorModel::rotPar,this->path.getParameterCount(CSensorModel::rotPar));

		project.setUseOrbitOvsevationHandler(this->existingPath,this->useOrbitObservations);
	}
	else if (this->splineType == 2)
	{
		this->setParameter(this->existingAttitudes,true);
		// the 'existingAttitudes' is shared with the SplineParameterDlg, so only update the parameter
		// which can be changed by this dlg
		this->existingAttitudes->setParameterCount(CSensorModel::scalePar,this->attitudes.getParameterCount(CSensorModel::scalePar));
		this->existingAttitudes->setParameterCount(CSensorModel::rotPar,this->attitudes.getParameterCount(CSensorModel::rotPar));

		project.setUseOrbitOvsevationHandler(this->existingAttitudes,this->useOrbitObservations);
	}


	
	this->success = true;

	return true;
}

bool Bui_OrbitSettings::cancelButtonReleased()
{
	this->success = false;
	return true;
}

CCharString Bui_OrbitSettings::helpButtonReleased()
{
	if (this->splineType == 1)
		return"UserGuide/ThumbNodeOrbitPath.html";
	else return "UserGuide/ThumbNodeOrbitAttitudes.html";
}



void Bui_OrbitSettings::pBAllReleased()
{
	// first set the local one 
	this->listenToSignal =false;
	this->okButtonReleased();
	this->setAllParameter(false);
	this->listenToSignal =true;
	
}

void Bui_OrbitSettings::setAllParameter(bool parameter)
{
	for (int i=0; i < project.getNumberOfOrbits(); i++)
	{
		COrbitObservations* orbit = project.getOrbitObservationAt(i);
		if (orbit->getSplineType() == this->splineType)
		{
			this->setParameter(orbit->getCurrentSensorModel(),parameter);
		}
	}
}

void Bui_OrbitSettings::setParameter(const CSensorModel* sensorModel,bool parameter)
{
	bool wasShownInOrbit = false;
	if (this->showInOrbitalSystem)
	{
		wasShownInOrbit = true;
		this->showInOrbitalSystem = false;
		this->transformToOrbitalSystem(true);
	}


	if (this->splineType == 1)
	{
		COrbitPathModel* p;
		if (sensorModel)
			p = (COrbitPathModel*)sensorModel;
		else
			p = & this->path;

		p->setDirectObservationForShift(this->pathShiftObservation);
		p->setDirectObservationForAngles(this->pathRotationObservation);
		if (parameter)
		{
			p->setShift(this->pathShiftParameter);
			p->setRotation(this->pathRotationParameter);
		}

		if (this->bPathShiftSelected)
			p->activateParameterGroup(CSensorModel::shiftPar);
		else
			p->deactivateParameterGroup(CSensorModel::shiftPar);

		if (this->bPathRotationSelected)
			p->activateParameterGroup(CSensorModel::rotPar);
		else
			p->deactivateParameterGroup(CSensorModel::rotPar);
		
	}
	else if (this->splineType == 2)
	{
		COrbitAttitudeModel* a;
		if (sensorModel)
			a = (COrbitAttitudeModel*)sensorModel;
		else
			a = & this->attitudes;		

		a->setDirectObservationForAngles(this->attitudeRotationObservation);
		a->setDirectObservationForScale(this->attitudeTimeRotationObservation);

		if (parameter)
		{
			a->setRotParameter(this->attitudeRotationParameter);
			a->setScaleParameter(this->attitudeTimeRotationParameter);
		}

		if (this->bAttitudeRotationSelected)
			a->activateParameterGroup(CSensorModel::rotPar);
		else
			a->deactivateParameterGroup(CSensorModel::rotPar);

		if (this->bAttitudeTimeRotationSelected)
			a->activateParameterGroup(CSensorModel::scalePar);
		else
			a->deactivateParameterGroup(CSensorModel::scalePar);
	}


	if (wasShownInOrbit)
	{
		this->showInOrbitalSystem = true;
		this->transformToOrbitalSystem();
	}

}



void Bui_OrbitSettings::elementsChangedSlot(CLabel element)
{

	if (!this->listenToSignal) 
		return;


	if (this->splineType == 1)
	{
		this->path = *this->existingPath;
		this->copyDataInLocalPathVariables();
	}
	else if (this->splineType == 2)
	{
		this->attitudes =  *this->existingAttitudes;
		this->copyDataInLocalAttitudeVariables();
	}

	this->applySettings();
	this->checkAvailabilityOfSettingsPath();
	this->checkAvailabilityOfSettingsAttitudes();
}



void Bui_OrbitSettings::rBGlobalReleased()
{
	if (this->showInOrbitalSystem)
	{
		this->showInOrbitalSystem = false;
		this->transformToOrbitalSystem(true);
		this->applySettings();
	}
}

void Bui_OrbitSettings::rBOrbitReleased()
{
	if (!this->showInOrbitalSystem)
	{
		this->showInOrbitalSystem = true;
		this->transformToOrbitalSystem();
		this->applySettings();
	}
}
