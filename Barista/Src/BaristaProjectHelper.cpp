#include <QWidgetList>

#include "BaristaProjectHelper.h"
#include "SystemUtilities.h"
#include "Bui_MainWindow.h"
#include "BaristaIconViewItem.h"
#include "BaristaTreeWidget.h"
#include "ASCIIDEMFile.h"
#include "G3DFile.h"
#include "BaristaViewEventHandler.h"
#include "Bui_MainWindow.h"
#include "ImageView.h"


CBaristaProjectHelper baristaProjectHelper;

extern CBaristaTreeWidget* thumbnailView;


CBaristaProjectHelper::CBaristaProjectHelper(void)
{
	
}

CBaristaProjectHelper::~CBaristaProjectHelper(void)
{
	
}



void CBaristaProjectHelper::setIsBusy(bool b)
{
	if (b)
		QApplication::setOverrideCursor(Qt::WaitCursor);
	else
		QApplication::restoreOverrideCursor();
	
}



/** 
 * restores the tree view after a readproject
 */
void CBaristaProjectHelper::restoreTree()
{
    QString itemtext;

	CBaristaIconViewItem* imageItem = NULL;
	
	
	// create image node
	if (project.getNumberOfImages())
		imageItem = new CBaristaIconViewItem(thumbnailView,eImages,NULL,"Images");

    // restoring the image nodes and attached stuff
    for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		CVImage* image = project.getImageAt(i);
        CBaristaIconViewItem* bitemIcon = new CBaristaIconViewItem(imageItem,eImage,image);
		CBaristaIconViewItem* bitemCaption = new CBaristaIconViewItem(imageItem,eImageCaption,image);


        if ( image->getXYPoints()->GetSize() > 0)
        {
            CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitemCaption,
																	eXYPoints,
																	image->getXYPoints(),
																	image->getXYPoints()->getItemText().GetChar());
			
			// add residual node to measured XYPoints
			if ( image->getResiduals()->GetSize() > 0)
				CBaristaIconViewItem* residuals = new CBaristaIconViewItem( item,
																			eResiduals,
																			image->getResiduals(),
																			image->getResiduals()->getItemText().GetChar());

        }

		//addition>> //showing 3D residuals
		if ( image->getXY3DResiduals()->GetSize() > 0)
        {
            CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitemCaption,
																	ObjResiduals,
																	image->getXY3DResiduals(),
																	image->getXY3DResiduals()->getItemText().GetChar());
        }
		//<<addition

		if (image->getNMonoPlottedPoints())
			CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitemCaption,
																	eXYMonoPlottedPoints,
																	image->getMonoPlottedPoints(),
																	image->getMonoPlottedPoints()->getItemText().GetChar());


		if (image->getNFiducialmarkObs())
			CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitemCaption,
																	eXYFiducialMarkObservations,
																	image->getFiducialMarkObs(),
																	image->getFiducialMarkObs()->getItemText().GetChar());


		if ( image->getProjectedXYPoints()->GetSize())
            CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitemCaption,
																	eProjectedPoints,
																	image->getProjectedXYPoints(),
																	image->getProjectedXYPoints()->getItemText().GetChar());

		if ( image->getDifferencedXYPoints()->GetSize() > 0)
            CBaristaIconViewItem* item = new CBaristaIconViewItem(  bitemCaption ,
																	eDifferencedPoints,
																	image->getDifferencedXYPoints(),
																	image->getDifferencedXYPoints()->getItemText().GetChar());		

         if ( image->hasRPC())
            CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitemCaption,
																	eRPCs,
																	image->getRPC(),
																	image->getRPC()->getItemText().GetChar());
		
		if (image->hasPushbroom())
			CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitemCaption,
																	ePushBroom,
																	image->getPushbroom(),
																	image->getPushbroom()->getItemText().GetChar());

		if (image->hasFrameCamera())
			CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitemCaption,
																	eFrameCamera,
																	image->getFrameCamera(),
																	image->getFrameCamera()->getItemText().GetChar());

        if (image->hasAffine())
            CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitemCaption,
																	eAffine,
																	image->getAffine(),
																	image->getAffine()->getItemText().GetChar());

        if ( image->hasTFW())
            CBaristaIconViewItem* item = new CBaristaIconViewItem(  bitemCaption,
																	eTFWs,
																	image->getTFW(),
																	image->getTFW()->getItemText().GetChar());

		if ( image->getXYContours()->GetSize())
            CBaristaIconViewItem* item = new CBaristaIconViewItem(  bitemCaption,
																	eXYContours,
																	image->getXYContours(),
																	image->getXYContours()->getItemText().GetChar());
		if ( image->getXYEdges()->GetSize())
            CBaristaIconViewItem* item = new CBaristaIconViewItem(  bitemCaption,
																	eEdges,
																	image->getXYEdges(),
																	image->getXYEdges()->getItemText().GetChar());

		if ( image->getEllipses()->GetSize() > 0 )
            CBaristaIconViewItem* item = new CBaristaIconViewItem(  bitemCaption,
																	eEllipses,
																	image->getEllipses(),
																	image->getEllipses()->getItemText().GetChar());


		thumbnailView->setItemExpanded(bitemCaption,true);
    }
	
	if (imageItem)
		thumbnailView->expandItem(imageItem);


	// restoring xyzpoints 
	CBaristaIconViewItem* xyzArrayItem = NULL;
	if (project.getNumberOfXYZPointArrays())
		xyzArrayItem = new CBaristaIconViewItem(thumbnailView,eXYZArrays,NULL,"XYZ Points");


    for (int i = 0; i < project.getNumberOfXYZPointArrays(); i++)
    {
		CXYZPointArray* xyzpoints = project.getPointArrayAt(i);
		CXYZPointArray* xyzResPoints = project.getPointResidualArrayAt(i);

		if (xyzpoints->isControl())
			project.setControlPoints(xyzpoints);
		
        CBaristaIconViewItem* bitem = new CBaristaIconViewItem( xyzArrayItem,
																eXYZPoints,
																xyzpoints,
																xyzpoints->getItemText().GetChar());
		if (xyzResPoints->GetSize())
            CBaristaIconViewItem* item = new CBaristaIconViewItem(  bitem,
																	eXYZPoints,
																	xyzResPoints,
																	xyzResPoints->getItemText().GetChar());


    }

	if (xyzArrayItem)
		thumbnailView->expandItem(xyzArrayItem);



    // restoring dems 
	CBaristaIconViewItem* demItem = NULL;
	if (project.getNumberOfDEMs())
		demItem = new CBaristaIconViewItem(thumbnailView,eDEMs,NULL,"DEMs");


	for (int i = 0; i < project.getNumberOfDEMs(); i++)
    {
		CVDEM* vdem = project.getDEMAt(i);
		if (vdem->getIsActiveDEM())
			project.setActiveDEM(vdem);


        CBaristaIconViewItem* bitem = new CBaristaIconViewItem( demItem,
																eDEM,
																vdem,
																vdem->getItemText().GetChar());

/////////////
		if (!vdem->getDEM()->getisloaded())
		{
			CHugeImage* humage = vdem->getDEM();
			CFileName filename = *(humage->getFileName());
			
			if (!humage->open(filename.GetChar(), false))
			{
				CFileName originalFilename(filename.GetPath() + CSystemUtilities::dirDelimStr + filename.GetFileName()) ;
				CCharString ext = originalFilename.GetFileExt();

				if (ext.CompareNoCase("TXT") || ext.CompareNoCase("DEM") )
				{
					CASCIIDEMFile asciidem(originalFilename.GetChar());
					humage->open(&asciidem);
				}
				else if (ext.CompareNoCase("G3D") )
				{
					CG3DFile g3ddem(originalFilename.GetChar());
					humage->open(&g3ddem);
				}
			}
		}

///////////


        if ( vdem->getDEM()->getTFW()->gethasValues())
            CBaristaIconViewItem* item = new CBaristaIconViewItem(bitem,
																	eTFWs,
																	vdem->getDEM()->getTFW(),
																	vdem->getDEM()->getTFW()->getItemText().GetChar());

		if (vdem->getNMonoPlottedPoints())
			CBaristaIconViewItem* item = new CBaristaIconViewItem(	bitem,
																	eXYMonoPlottedPoints,
																	vdem->getMonoPlottedPoints(),
																	vdem->getMonoPlottedPoints()->getItemText().GetChar());

    }

	if (demItem)
		thumbnailView->expandItem(demItem);



    // restoring ALS data sets 
	CBaristaIconViewItem* alsItem = NULL;
	if (project.getNumberOfAlsDataSets())
		alsItem = new CBaristaIconViewItem(thumbnailView,eALSDataSets,NULL,"ALS data sets");


	for (int i = 0; i < project.getNumberOfAlsDataSets(); i++)
    {
		CVALS* als = project.getALSAt(i);
        CBaristaIconViewItem* bitem = new CBaristaIconViewItem( alsItem,
																eALS,
																als,
																als->getItemText().GetChar());

    }

	if (alsItem)
		thumbnailView->expandItem(alsItem);



	if ( project.getNumberOfGeoreferencedPoints() > 0)
        CBaristaIconViewItem* item = new CBaristaIconViewItem(	thumbnailView,
																eXYZPoints,
																project.getGeoreferencedPoints(),
																project.getGeoreferencedPoints()->getItemText().GetChar());

	//addition>>
	if ( project.getNumberOfGCPchipPoints() > 0)
        CBaristaIconViewItem* item = new CBaristaIconViewItem(	thumbnailView,
																eXYZPoints,
																project.getGCPchipPoints(),
																project.getGCPchipPoints()->getItemText().GetChar());
	//<<addition

	if ( project.getNumberOfMonoplottedPoints() > 0)
	   CBaristaIconViewItem* item = new CBaristaIconViewItem(	thumbnailView,
																eXYZPoints,
																project.getMonoplottedPoints(),
																project.getMonoplottedPoints()->getItemText().GetChar());





    // restoring XYZPolyLines 
	if ( project.getNumberOfMonoplottedLines() > 0 )
		CBaristaIconViewItem* bitem = new CBaristaIconViewItem( thumbnailView,
																ePolyLines,
																project.getXYZPolyLines(),
																project.getXYZPolyLines()->getItemText().GetChar());

	//addition >>
	// restoring XYZ height lines 
	if ( project.getNumberOfMonoplottedHeightLines() > 0 )
		CBaristaIconViewItem* bitem = new CBaristaIconViewItem( thumbnailView,
																ePolyLines,
																project.getXYZHeightLines(),
																//project.getXYZHeightLines()->getItemText().GetChar());
																"HeightLines");
	//project.getXYZHeightLines()->getItemText().GetChar());
	//<< addition

#ifdef WIN32
    // restoring Buildings
	if ( project.getNumberOfMonoplottedBuildings() > 0 )
		CBaristaIconViewItem* bitem = new CBaristaIconViewItem( thumbnailView,
																eBuildings,
																project.getBuildings(),	
																project.getBuildings()->getItemText().GetChar());
#endif
    // restoring orbit observations
	CBaristaIconViewItem* orbitPathItem = NULL;
	CBaristaIconViewItem* orbitAttitudeItem = NULL;
	
	for (int i = 0; i < project.getNumberOfOrbits(); i++)
    {
		COrbitObservations* orbit = project.getOrbitObservationAt(i);
        
		if (orbit->getSplineType() == Path)
		{
			if (!orbitPathItem)
			{
				orbitPathItem = new CBaristaIconViewItem(thumbnailView,eOrbitPaths,NULL,"Orbit Paths");
			}
			CBaristaIconViewItem* bitem = new CBaristaIconViewItem( orbitPathItem,
																ePath,
																orbit,
																orbit->getName().GetChar());
		}
		else if (orbit->getSplineType() == Attitude)
		{
			if (!orbitAttitudeItem)
			{
				orbitAttitudeItem = new CBaristaIconViewItem(thumbnailView,eOrbitAttitudes,NULL,"Orbit Attitudes");
			}
			CBaristaIconViewItem* bitem = new CBaristaIconViewItem( orbitAttitudeItem,
																eAttitudes,
																orbit,
																orbit->getName().GetChar());
		}
    }

	if (orbitPathItem)
		thumbnailView->expandItem(orbitPathItem);
	if (orbitAttitudeItem)
		thumbnailView->expandItem(orbitAttitudeItem);
	

    // restoring cameras
	CBaristaIconViewItem* cameraItem = NULL;

	for (int i = 0; i < project.getNumberOfCameras(); i++)
    {
		CCamera* camera = project.getCameraAt(i);
        
		if (!cameraItem)
		{
			cameraItem = new CBaristaIconViewItem(thumbnailView,eCameras,NULL,"Cameras");
		}

		CBaristaIconViewItem* bitem = new CBaristaIconViewItem( cameraItem,
																eCCDCamera,
																camera,
																camera->getName().GetChar());
    }

	if (cameraItem)
		thumbnailView->expandItem(cameraItem);


    // restoring cameraMountings
	CBaristaIconViewItem* cameraMountingItem = NULL;

	for (int i = 0; i < project.getNumberOfCameraMountings(); i++)
    {
		CCameraMounting* cameraMounting = project.getCameraMountingAt(i);
        
		if (!cameraMountingItem)
		{
			cameraMountingItem = new CBaristaIconViewItem(thumbnailView,eCameras,NULL,"CameraMountings");
		}

		CBaristaIconViewItem* bitem = new CBaristaIconViewItem( cameraMountingItem,
																eCCDCamera,
																cameraMounting,
																cameraMounting->getName().GetChar());
    }

	if (cameraMountingItem)
		thumbnailView->expandItem(cameraMountingItem);


}


/** 
 * refreshes the tree view after an action
 */
void CBaristaProjectHelper::refreshTree()
{
	thumbnailView->clear();
	this->restoreTree();
}


void CBaristaProjectHelper::closeGUI()
{
	if (workspace)
	{
		workspace->closeAllSubWindows();
		/*QWidgetList list =workspace->subWindowList();
		for (int i=0; i<list.count(); i++)
		{
			QWidget* w = list.at(i);
			w->close();
			delete w;
		}*/
	}

	currentBaristaViewHandler->close();  
}

void CBaristaProjectHelper::setText(const char *text) const
{
	currentBaristaViewHandler->setStatusBarText(text);
}

void CBaristaProjectHelper::openBaristaView(CVImage* image)
{

	if (!currentBaristaViewHandler->isOpen(image) && image)
	{
		CImageView*imageView = new CImageView(image, workspace);
		QMdiSubWindow *widget = workspace->addSubWindow(imageView);
		imageView->setAttribute(Qt::WA_DeleteOnClose);
        imageView->setUpdatesEnabled(true);
		if (!imageView->isVisible()) imageView->show(); 
		else imageView->activateWindow();
		workspace->tileSubWindows();
	}
}