#ifdef WIN32
#include <float.h> 

#include "MonoplottingBuildingHandler.h"
#include "Bui_MainWindow.h"
#include <QMessageBox>
#include <QColor>
#include "Bui_LabelChangeDlg.h"
#include "BuildingBundleManager.h"
#include "BaristaProjectHelper.h"

CMonoplottingBuildingHandler::CMonoplottingBuildingHandler(void) :currentVImage(NULL),
TEMP_BUILDING_LABEL("Temp")
{
	this->statusBarText = "MonoplottingBuildingMODE ";
}

CMonoplottingBuildingHandler::~CMonoplottingBuildingHandler(void)
{
}

bool CMonoplottingBuildingHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{
	return CBaristaViewEventHandler::handleMouseMoveEvent(e, v);
};

bool CMonoplottingBuildingHandler::keyPressEvent (QKeyEvent *e, CBaristaView *v)
{
	return CBaristaViewEventHandler::keyPressEvent(e, v);
}

bool CMonoplottingBuildingHandler::keyReleaseEvent (QKeyEvent *e, CBaristaView *v)
{
	CBaristaViewEventHandler::keyReleaseEvent(e, v);
	if (v->getVImage())
	{

		if ( e->key() == Qt::Key_F)
		{
			if (v->getVImage() != this->currentVImage)
				return false;

			if (this->roof.getPointCount() < 3)
			{
				QMessageBox::warning( v, "Building not saved!"
				, "At least three roof points are required!");
				return false;
			}

			CBuilding* pBuilding;
			CBuilding building;

			project.getBuildings()->unselectAll();
			
			int currentBuildingIndex = project.getCurrentBuildingIndex();
			
			if (this->courtYardMode)
			{
				
				// add court yard or superstructure
				if ( this->roof.getPointCount() == this->floor.getPointCount() && currentBuildingIndex >= 0)
				{
					pBuilding = (CBuilding*)project.getBuildings()->GetAt(currentBuildingIndex);
					pBuilding->addInnerCourtyard(this->roof, this->floor);
				}
				else
					return false;

				this->removeDrawingDataSet(pBuilding->getLabel());
			}
			else if (this->superStructureMode)
			{

				if ( this->roof.getPointCount() == this->floor.getPointCount() && currentBuildingIndex >= 0)
				{
					pBuilding = (CBuilding*)project.getBuildings()->GetAt(currentBuildingIndex);
					if (this->superStructureIsInterior())
						pBuilding->addRoofPlane(this->roof, this->floor);
					else
						pBuilding->booleanUnion(this->roof, this->floor);
				}
				else
					return false;

				this->removeDrawingDataSet(pBuilding->getLabel());
			}
			else
			{
				pBuilding = &building;
				this->setBuildingLabels(pBuilding,this->newBuildingLabel);

				//finish building
				pBuilding->addRoofPlane(this->roof, this->floor);
				project.setCurrentBuildingIndex(project.addMonoplottedBuilding(*pBuilding,&baristaProjectHelper));
				currentBuildingIndex = project.getCurrentBuildingIndex();
				
				if (currentBuildingIndex == 1)
					baristaProjectHelper.refreshTree();

			}

			// add to 2D monoplotted points of the image
			for (int i=0 ; i < this->current2DRoof.GetSize(); i++)
				v->getVImage()->addMonoPlottedPoint(*this->current2DRoof.getAt(i),false);

			if (!this->courtYardMode)
				// last point will force the array to be sorted
				v->getVImage()->addMonoPlottedPoint(this->xyfloor);
			else
				v->getVImage()->getMonoPlottedPoints()->sortByLabel();

			this->removeDrawingDataSet(TEMP_BUILDING_LABEL);

			int n = this->baristaViews.getNumberOfViews();

			for (int i=0; i < n; i++)
			{
				CDrawingDataSet dataSet(2);
				CBaristaView *view= this->baristaViews.getView(i);
				this->createBuildingDataSet(dataSet,view,pBuilding);
				view->addDrawingDataSet(dataSet);
				this->redraw(view);
			}

			if ( currentBuildingIndex >= 0 )
			{
				pBuilding = project.getBuildings()->GetAt(currentBuildingIndex);
				project.getBuildings()->selectElement(pBuilding->getLabel(),true);
				
			}

			this->initBuilding();

			v->resetFirstSecondClick();
			this->setCurrentCursor(v);
			v->update();
		}
		else if ( e->key() == Qt::Key_Backspace )
		{
			if (v->getVImage() != this->currentVImage)
				return false;

			//delete last building point
			if (this->current2DRoof.GetSize() && this->roof.getPointCount())
			{
				
				// reset the first click 
				if (this->current2DRoof.GetSize() > 1)
				{
					CObsPoint* p =this->current2DRoof.GetAt(this->current2DRoof.GetSize()-2);
					C2DPoint lastClickedPoint((*p)[0],(*p)[1]);			
					v->setFirstClick(lastClickedPoint);
				}
				else
				{
					v->setFirstClick(C2DPoint(-1,-1));
					this->removeDrawingDataSet(TEMP_BUILDING_LABEL);
				}
				
				int lastRoofP = this->roof.getPointCount()-1;
				int lastFloorP = this->floor.getPointCount()-1;
				
				this->removePoint(this->roof.getPoint(lastRoofP)->getLabel());
				this->removePoint(this->floor.getPoint(lastFloorP)->getLabel());
				this->roof.removePoint(lastRoofP);
				this->floor.removePoint(lastFloorP);
				this->current2DRoof.RemoveAt(this->current2DRoof.GetSize()-1,1);
			}
		}
		else if ( e->key() == Qt::Key_Escape )
		{
			if (!this->courtYardMode && !this->superStructureMode)
			{
				project.getBuildings()->unselectAll();
				project.setCurrentBuildingIndex(-1);
			}

			//delete current building
			this->removeDrawingDataSet(TEMP_BUILDING_LABEL);
			this->initBuilding();
			v->resetFirstSecondClick();
			this->setCurrentCursor(v);
			v->update();
		}
		else if ( e->key() == Qt::Key_C )
		{
			if (this->floor.getPointCount() > 0)
			{
				QMessageBox::warning( v, "Building or Building part not finalized yet!"
				, "Press F to finalize building!");
				return false;
			}
			
			this->setCourtYardMode();
			this->setCurrentCursor(v);
			v->update();

			//add interior court yard to building
			v->resetFirstSecondClick();
		}
		else if ( e->key() == Qt::Key_S )
		{
			if (this->floor.getPointCount() > 0)
			{
				QMessageBox::warning( v, "Building or Building part not finalized yet!"
				, "Press F to finalize building!");
				return false;
			}
			this->setSuperStructureMode();
			this->setCurrentCursor(v);
			v->update();
			//add super structure to building
			v->resetFirstSecondClick();
		}
		else if ( e->key() == Qt::Key_L )
		{
			if (this->courtYardMode || this->superStructureMode)
				return true;

			if (this->newBuildingLabel.GetLength() == 0 )
				this->newBuildingLabel.Set(project.getCurrentBuildingLabel());

			bui_LabelChangeDlg labeldlg(this->newBuildingLabel.GetChar(), "New Building Label");

			labeldlg.setModal(true);
			labeldlg.exec();

			CLabel newlabel = labeldlg.getLabelText();

			// if label is empty or alreday exists keep old label
			while (!project.checkforUniqueBuildingLabel(newlabel))
			{
				QMessageBox::warning( 0, "Change Label not possible!"
				, "This building label already exist. Choose different Label!");
				labeldlg.setLabelText(this->newBuildingLabel.GetChar());
				labeldlg.exec();

				newlabel.Set(labeldlg.getLabelText().GetChar());
			}
			
			this->newBuildingLabel = newlabel;
			project.setCurrentBuildingLabel(newlabel );

			labeldlg.close();
		}
		else if (e->key() == Qt::Key_Delete)
		{
			if (project.getCurrentBuildingIndex() >=0)
				if (project.removeMonoplottedBuilding(project.getCurrentBuildingIndex(),1))
					baristaProjectHelper.refreshTree();
		}
	}
	
	return true;
};

bool CMonoplottingBuildingHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	v->setShowRubberLine(true);	
	return false;
};

bool CMonoplottingBuildingHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	if (v->getVImage())
	{
		if (!CMonoplottingHandler::handleLeftMouseReleaseEvent(e,v))
			return false;

		CXYPoint xy;
		if (!v->getMouseEventCoords(e, xy.x, xy.y))
			return false;
		
		v->setFirstClick(C2DPoint(xy.x, xy.y));

		//// give a warning if reference sys of buildings and dem don't match
		if ( project.getNumberOfMonoplottedBuildings() > 0 )
		{
			// check if coordinates are in the same coordinate system
			// and show at least a warning message
			eCoordinateType demtype = this->monoPlotter.getReferenceSystem().getCoordinateType();
			eCoordinateType monotype = project.getBuildings()->getReferenceSystem()->getCoordinateType();

			if (demtype != monotype)
			{
				QMessageBox::warning( v, "DEM and monoplotted buildings are defined in different coordinate systems!"
				, "Coordinate definitions of monoplotted buildings will be set to DEM parameters");
			}
		}
		////

		project.setMonoplottedBuildingReferenceSystem(this->monoPlotter.getReferenceSystem());

		// monoplotting first building point on the ground
		if ( this->shiftdown )
		{
			if (this->selectBuilding(xy,v))
			{
				project.selectCurrentBuilding();
		

			}
			v->resetFirstSecondClick();
		}
		else if ( this->floor.getPointCount() == 0)
		{	
			this->currentVImage = v->getVImage();
			if (!this->firstPointClicked(xy,v))
			{
				v->resetFirstSecondClick();
				return false;
			}
		}
		// monoplotting first roof point
		else if ( this->floor.getPointCount() == 1 && this->roof.getPointCount() == 0)
		{
			if (v->getVImage() != this->currentVImage)
				return false;

			if (!this->secondPointClicked(xy))
			{
				v->resetFirstSecondClick();
				return false;
			}

		}
		else if ( this->floor.getPointCount() >= 1 && this->roof.getPointCount() >= 1 )
		{
			
			if (v->getVImage() != this->currentVImage)
				return false;

			if (!this->nextPointClicked(xy))
			{
						// reset the first click 
				if (this->current2DRoof.GetSize() >= 1)
				{
					CObsPoint* p =this->current2DRoof.GetAt(this->current2DRoof.GetSize()-1);
					C2DPoint lastClickedPoint((*p)[0],(*p)[1]);			
					v->setFirstClick(lastClickedPoint);
				}
				return false;
			}
			else
			{
//				CBuilding building;
//				building.addRoofPlane(this->roof, this->floor);
//				CBuildingBundleManager handle(&building);
//				handle.bundleInit(false);
//				if (handle.getNStations() > 0)
//				{
//					handle.solve(false, false, false);
//				}
			}
		}
	}

	return true;
};


bool CMonoplottingBuildingHandler::redraw(CBaristaView *v)
{
	if (v->getVImage() || v->getVDEM())
	{
		if (!CMonoplottingHandler::redraw(v))
			return false;

		CBuildingArray* monoPlottedBuildings = project.getBuildings();
		int nBuildings = monoPlottedBuildings->GetSize();

		for (int k=0; k< nBuildings; k++)
		{
			CBuilding* pBuilding = monoPlottedBuildings->GetAt(k);
			CDrawingDataSet dataSet(2);
			this->createBuildingDataSet(dataSet,v,pBuilding);
			v->addDrawingDataSet(dataSet);
		}
	}

	return true;
};

void CMonoplottingBuildingHandler::initBuilding()
{
	this->current2DRoof.RemoveAll();
	this->currentFloorPointLabel.Set("");
	this->currentRoofPointLabel.Set("");
	this->roof.deleteLine();
	this->floor.deleteLine();
	this->currentVImage = NULL;
	this->newBuildingLabel = "";
	this->setBaseMode();
}

void CMonoplottingBuildingHandler::open()
{
	baristaProjectHelper.setIsBusy(true);
	CBaristaViewEventHandler::open();
	CMonoplottingHandler::open();

	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->MonoplotBuildingsAction->setChecked(true);
	mainwindow->blockSignals(false);

	this->initBuilding();

	project.getBuildings()->addListener(this);

	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i < n; i++)
	{
		redraw(this->baristaViews.getView(i));
	}
	baristaProjectHelper.setIsBusy(false);
};

void CMonoplottingBuildingHandler::close()
{
	CBaristaViewEventHandler::close();
	CMonoplottingHandler::close();

	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->MonoplotBuildingsAction->setChecked(false);
	mainwindow->blockSignals(false);

	project.getBuildings()->removeListener(this);
};

bool CMonoplottingBuildingHandler::createBuildingDataSet(CDrawingDataSet& dataSet,CBaristaView* v,CBuilding *pBuilding)
{
	CObsPointArray buildingPoints(3);
	vector<int> from;
	vector<int> to;
	QColor color = pBuilding->getIsSelected() ? Qt::yellow : QColor(project.getBuildingLineColor().x,
																	project.getBuildingLineColor().y,
																	project.getBuildingLineColor().z);

	pBuilding->getAllVerticesAndEdges(buildingPoints,from,to);
	
	for (int i=0; i < buildingPoints.GetSize(); i++)
	{
		CXYPoint xyPoint;
		this->backprojectPoint(v,xyPoint,*buildingPoints.GetAt(i));
		dataSet.addPoint(&xyPoint);
	}

	if (from.size() != to.size())
		return false;

	dataSet.setName(pBuilding->getLabel());
	dataSet.setShowPoints(false);

	int c[3];
	c[0] = color.red();
	c[1] = color.green();
	c[2] = color.blue();


	for (unsigned int i=0; i < from.size(); i++)
	{
		dataSet.addTopology(buildingPoints.GetAt(from[i])->getLabel(),buildingPoints.GetAt(to[i])->getLabel(),c,project.getBuildingLineWidth());
	}

	return true;
}

void CMonoplottingBuildingHandler::addDrawingDataSets()
{
	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i < n; i++)
	{
		CBaristaView * v = this->baristaViews.getView(i);
		CDrawingDataSet dataSet(2);
		dataSet.setName(TEMP_BUILDING_LABEL);

		CXYPoint newPoint;
		if (this->floor.getPointCount())
		{
			this->backprojectPoint(v,newPoint,this->floor.getPoint(0));
			dataSet.addPoint(&newPoint);
		}

		if (v->getVImage() == this->currentVImage)
		{
			dataSet.setShowPoints(true);
			dataSet.setShowPointLabels(false);
		}
		else
		{
			dataSet.setShowPoints(false);
		}
		
		v->addDrawingDataSet(dataSet);
	}	
}

void CMonoplottingBuildingHandler::addFirstRoofPoint()
{
	int n = this->baristaViews.getNumberOfViews();
	QColor color = Qt::magenta;
	for (int i=0; i < n; i++)
	{
		CBaristaView * v = this->baristaViews.getView(i);
		
		CXYPoint newPoint;
		this->backprojectPoint(v,newPoint,this->roof.getPoint(0));
		v->addPointDrawing(TEMP_BUILDING_LABEL,&newPoint);
		v->addTopology(TEMP_BUILDING_LABEL,this->roof.getPoint(0)->getLabel(),this->floor.getPoint(0)->getLabel(),color,project.getBuildingLineWidth());
	}	
}

void CMonoplottingBuildingHandler::addNextRoofPoint()
{
	int n = this->baristaViews.getNumberOfViews();
	

	int index = this->roof.getPointCount() -1;

	CXYZPoint* roofP = this->roof.getPoint(index);
	CXYZPoint* floorP = this->floor.getPoint(index);

	if (index < 1)
		return;

	CXYZPoint* roofP_prev = this->roof.getPoint(index - 1);
	CXYZPoint* floorP_prev = this->floor.getPoint(index - 1);


	QColor color = Qt::magenta;
	for (int i=0; i < n; i++)
	{
		CBaristaView * v = this->baristaViews.getView(i);
		
		CXYPoint newPoint;
		this->backprojectPoint(v,newPoint,roofP);
		v->addPointDrawing(TEMP_BUILDING_LABEL,&newPoint);

		this->backprojectPoint(v,newPoint,floorP);
		v->addPointDrawing(TEMP_BUILDING_LABEL,&newPoint);


		v->addTopology(TEMP_BUILDING_LABEL,roofP->getLabel(),floorP->getLabel(),color,project.getBuildingLineWidth());
		v->addTopology(TEMP_BUILDING_LABEL,roofP_prev->getLabel(),roofP->getLabel(),color,project.getBuildingLineWidth());
		v->addTopology(TEMP_BUILDING_LABEL,floorP_prev->getLabel(),floorP->getLabel(),color,project.getBuildingLineWidth());
	}	
}

void CMonoplottingBuildingHandler::removePoint(CLabel& label)
{
	int n = this->baristaViews.getNumberOfViews();
	

	for (int i=0; i < n; i++)
	{
		CBaristaView * v = this->baristaViews.getView(i);
		v->removePointDrawing(TEMP_BUILDING_LABEL,label);
	}
}

void CMonoplottingBuildingHandler::elementAdded(CLabel element)
{
	
}

void CMonoplottingBuildingHandler::elementDeleted(CLabel element)
{
	this->removeDrawingDataSet(element);	
}

void CMonoplottingBuildingHandler::elementsChanged(CLabel element)
{

}

void CMonoplottingBuildingHandler::elementSelected(bool selected,CLabel element,int elementIndex)
{
	if (selected)
	{
		QColor color = Qt::yellow;
		this->changeColor(element,color);
	}
	else
	{
		QColor color = QColor(project.getBuildingLineColor().x,project.getBuildingLineColor().y,project.getBuildingLineColor().z);
		this->changeColor(element,color);
	}
	

}


bool CMonoplottingBuildingHandler::intersectWithBuilding(const CXYPoint& xy,C3DPoint& intersection,const int& buildingIndex)
{
	double zmin = this->monoPlotter.getMinZ();
	double zmax = this->monoPlotter.getMaxZ();

	if (buildingIndex < 0)
		return false;

	C3DPoint p0;
	C3DPoint dir;
	
	C2DPoint imgpoint(xy.x, xy.y);
	if ( this->monoPlotter.getProjectionRay(p0, dir, imgpoint, zmin, zmax +500))
	{
		CBuilding* building = project.getBuildings()->GetAt(buildingIndex);
		return building->intersectWithCurrentFace(p0, dir, intersection);
	}
	else
		return false;

}


bool CMonoplottingBuildingHandler::firstPointClicked(CXYPoint& xy,CBaristaView* v)
{
	if ( this->courtYardMode || this->superStructureMode)
	{
		int currentBuildingIndex = project.getCurrentBuildingIndex();
		if (currentBuildingIndex < 0)
			return false;

		CBuilding* pBuilding = project.getBuildings()->GetAt(currentBuildingIndex);
		pBuilding->resetCurrentFace();

		CXYZPoint intersection;
		if (!this->intersectWithBuilding(xy,intersection,currentBuildingIndex))
			return false;

		if (this->superStructureMode)
		{
			this->addDrawingDataSets();
			int scount = pBuilding->getSuperStructureCount();

			CCharString pointStringFloor(pBuilding->getLabel().GetChar());
			CCharString dummy(pointStringFloor + "_S%dF");
			pointStringFloor.Format(dummy.GetChar(),scount);

			this->currentFloorPointLabel.Set(pointStringFloor.GetChar());
			CLabel pointLabel(this->currentFloorPointLabel);
			pointLabel += this->floor.getPointCount();
			
			xy.setLabel(pointLabel);
			intersection.setLabel(pointLabel);
			this->xyfloor = xy;


			this->floor.addPoint(intersection);
			this->addFirstSuperStructureDrawing(intersection);

		}
		else
		{
			double sigmaZ;
			CXYZPoint courtbottom(intersection);
			courtbottom.z = this->monoPlotter.getInterpolatedHeight(courtbottom.x, courtbottom.y, sigmaZ);
			courtbottom.setSZ(sigmaZ);

			if (courtbottom.z == DEM_NOHEIGHT)
			{
				QMessageBox::warning( 0, "No DEM height could be interpolated!"
				, "No point could be measured!");
				return false;
			}

			this->addDrawingDataSets();
			int ccount = pBuilding->getCourtYardCount();

			CCharString pointStringRoof(pBuilding->getLabel().GetChar());
			CCharString dummy(pointStringRoof + "_C%dR");
			pointStringRoof.Format(dummy.GetChar(),ccount);

			CCharString pointStringFloor(pBuilding->getLabel().GetChar());
			dummy = pointStringFloor + "_C%dF";
			pointStringFloor.Format(dummy.GetChar(),ccount);


			this->currentRoofPointLabel.Set(pointStringRoof.GetChar());
			CLabel pointLabel(this->currentRoofPointLabel);
			pointLabel += this->roof.getPointCount();
			
			xy.setLabel(pointLabel);
			this->current2DRoof.Add(xy);

			intersection.setLabel(pointLabel);

			this->currentFloorPointLabel.Set(pointStringFloor.GetChar());
			pointLabel.Set(this->currentFloorPointLabel);
			pointLabel += this->floor.getPointCount();

			courtbottom.setLabel(pointLabel);
	
		
			this->roof.addPoint(intersection);
			this->floor.addPoint(courtbottom);
			this->addFirstCourtYardDrawing(courtbottom);
			this->addFirstRoofPoint();
		}
	}
	else
	{
	
		CXYZPoint groundpoint;
		this->xyfloor = xy;

		if (!this->monoPlotter.getPoint(groundpoint, xy))
		{
			QMessageBox::warning( 0, "Monoplotting did not converge!"
			, "No groundpoint could be measured!");
			return false;
		}

		// unselect every other building
		project.getBuildings()->unselectAll();
		project.setCurrentBuildingIndex(-1);
		this->newBuildingLabel.Set(project.getCurrentBuildingLabel());

		CCharString pointString(TEMP_BUILDING_LABEL.GetChar());
		pointString = pointString + "_F";
	
		this->currentFloorPointLabel.Set(pointString.GetChar());
		CLabel pointLabel(this->currentFloorPointLabel);
		pointLabel += this->floor.getPointCount();
		

		groundpoint.setLabel(pointLabel);
		this->xyfloor.setLabel(pointLabel);
		this->floor.addPoint(groundpoint);

		this->addDrawingDataSets();
	}

	return true;
}


bool CMonoplottingBuildingHandler::secondPointClicked(CXYPoint& xy)
{
	bool ret = true;
	if ( !this->courtYardMode)
	{
		CXYZPoint roofpoint;
		int currentBuildingIndex = project.getCurrentBuildingIndex();
		if (this->superStructureMode)
		{
			
			if (currentBuildingIndex < 0) ret = false;
			else
			{
				CBuilding* building = project.getBuildings()->GetAt(currentBuildingIndex);
		
				C3DPoint p01, p02, dir1, dir2;
				double zmin = this->monoPlotter.getMinZ();
				double zmax = this->monoPlotter.getMaxZ();

				ret  = this->monoPlotter.getProjectionRay(p01, dir1, this->xyfloor, zmin, zmax +500);
				if (ret) ret = this->monoPlotter.getProjectionRay(p02, dir2, xy, zmin, zmax +500);

				if (ret) 
					ret = building->verticalPoleAtCurrentFace(p01, dir1, p02, dir2, 
										                     *this->floor.getPoint(0), roofpoint);

				// check if changed floor point is still in current roof
				if (!building->isPointInCurrentFace(*this->floor.getPoint(0)))
				{
					QMessageBox::warning( 0, "Superstructure not on roof", "No point measured!");				
					return false;
				}

			}
		}
		else
		{
			ret = this->monoPlotter.getVerticalPole(*this->floor.getPoint(0), roofpoint, this->xyfloor, xy);
		}

		if (ret)
		{
			CCharString pointString(TEMP_BUILDING_LABEL.GetChar());
			if (this->superStructureMode)
			{
				CBuilding* building = project.getBuildings()->GetAt(currentBuildingIndex);
				int scount = building->getSuperStructureCount();

				pointString = building->getLabel().GetChar();
				CCharString dummy(pointString + "_S%dR");
				pointString.Format(dummy.GetChar(),scount);


			}
			else
				pointString = pointString + "_R";
		
			this->currentRoofPointLabel.Set(pointString.GetChar());
			CLabel pointLabel(this->currentRoofPointLabel);
			pointLabel += this->roof.getPointCount();

			roofpoint.setLabel(pointLabel);
			xy.setLabel(pointLabel);
			
			this->roof.addPoint(roofpoint);
			this->current2DRoof.Add(xy);
			this->addFirstRoofPoint();
		}
	}
	else
	{
		ret = this->nextPointClicked(xy);

	}

	if (!ret) QMessageBox::warning( 0, "Monoplotting did not converge!", "No roofpoint could be measured!");

	return ret;
}

bool CMonoplottingBuildingHandler::nextPointClicked(CXYPoint& xy)
{
	CBuilding* building = NULL;
	int currentBuildingIndex = project.getCurrentBuildingIndex();
	if (currentBuildingIndex >= 0)
		building = project.getBuildings()->GetAt(currentBuildingIndex);


	CXYZPoint roofpoint;
	double Z = this->roof.getPoint(0)->z;
	
	if (!this->monoPlotter.getXYfromImgPointHeight(roofpoint,xy, Z))
	{
		QMessageBox::warning( 0, "Monoplotting did not converge!"
		, "No roof point could be measured!");
		return false;
	}

	CXYZPoint groundpoint(roofpoint.x, roofpoint.y,0);
	if (this->superStructureMode && building && building->isPointInCurrentFace(roofpoint) )
		groundpoint.z = this->floor.getPoint(0)->z;
	else
	{
		if (this->courtYardMode && building && !building->isPointInCurrentFace(roofpoint))
		{
			QMessageBox::warning( 0, "CourtYard Point outside roof polygon", "No point measured!");				
			return false;
		}
		double sigmaZ;
		groundpoint.z = this->monoPlotter.getInterpolatedHeight(roofpoint.x, roofpoint.y, sigmaZ);
		groundpoint.setSZ(sigmaZ);

		if (groundpoint.z == DEM_NOHEIGHT)
		{
			QMessageBox::warning( 0, "No DEM height could be interpolated!"
			, "No point could be measured!");

			return false;	
		}
	}

	this->currentRoofPointLabel++;
	roofpoint.setLabel(this->currentRoofPointLabel);
	xy.setLabel(this->currentRoofPointLabel);

	this->current2DRoof.Add(xy);
	this->roof.addPoint(roofpoint);

	this->currentFloorPointLabel++;
	groundpoint.setLabel(this->currentFloorPointLabel);
	this->floor.addPoint(groundpoint);
	this->addNextRoofPoint();

	return true;
}

void CMonoplottingBuildingHandler::addFirstCourtYardDrawing(CXYZPoint& pNewPoint)
{
	int n = this->baristaViews.getNumberOfViews();
	QColor color = Qt::magenta;

	for (int i=0; i< n ; i++)
	{
		CBaristaView* view = this->baristaViews.getView(i);
		CXYPoint point;
		if (this->backprojectPoint(view,point,pNewPoint))
		{
			view->addPointDrawing(TEMP_BUILDING_LABEL,&point);
			view->addTopology(TEMP_BUILDING_LABEL,this->roof.getPoint(0)->getLabel(),pNewPoint.getLabel(),color,project.getBuildingLineWidth());
		}

	}
}

void CMonoplottingBuildingHandler::addFirstSuperStructureDrawing(CXYZPoint& pNewPoint)
{
	int n = this->baristaViews.getNumberOfViews();
	QColor color = Qt::magenta;

	for (int i=0; i< n ; i++)
	{
		CBaristaView* view = this->baristaViews.getView(i);
		CXYPoint point;
		if (this->backprojectPoint(view,point,pNewPoint))
		{
			view->addPointDrawing(TEMP_BUILDING_LABEL,&point);
		}

	}
}

bool CMonoplottingBuildingHandler::selectBuilding(CXYPoint &xy, CBaristaView *v)
{
	double distance = 100000000;
	int nbuildings = project.getNumberOfMonoplottedBuildings();

	int currentBuildingIndex = -1;

	CXYZPoint pClicked;

	for (int i = 0; i< nbuildings ; i++)
	{
		CBuilding* pBuilding = (CBuilding*)project.getBuildings()->GetAt(i);
		double Z = pBuilding->getRoofHeight();

		CXYZPoint pB(pBuilding->getCentre().x, pBuilding->getCentre().y, Z);
		
		if (this->monoPlotter.getXYfromImgPointHeight(pClicked,xy, Z))
		{
			double tmpdistance = pClicked.distanceFrom(&pB);
			if ( tmpdistance < distance )
			{
				currentBuildingIndex = i;
				distance = tmpdistance;
			}		
		}
	}

	project.setCurrentBuildingIndex(currentBuildingIndex);
	return true;
}


void CMonoplottingBuildingHandler::setBuildingLabels(CBuilding* pBuilding,CLabel label)
{


	// rename the points of the 2D/3D building
	// build the point label
	CCharString buildingString(label.GetChar());
	CCharString pointString(buildingString + "_R");

	// change label of roof
	for (int i=0 ; i < this->current2DRoof.GetSize(); i++)
	{
		CLabel currentPointLabel = pointString.GetChar();
		currentPointLabel += i;
		this->current2DRoof.getAt(i)->setLabel(currentPointLabel); //2D
		this->roof.getPoint(i)->setLabel(currentPointLabel); //3D
	}

	// change the 2d floor point
	pointString = buildingString + "_F";
	CLabel currentPointLabel = pointString.GetChar();
	currentPointLabel += 0;
	this->xyfloor.setLabel(currentPointLabel);

	for (int i=0 ; i < this->floor.getPointCount(); i++)
	{
		CLabel currentPointLabel = pointString.GetChar();
		currentPointLabel += i;
		this->floor.getPoint(i)->setLabel(currentPointLabel); //3D
	}

	// rename the 3D building
	pBuilding->setLabel(label);
}

void CMonoplottingBuildingHandler::setCourtYardMode()
{
	this->courtYardMode = true;

	this->superStructureMode = false;
}

void CMonoplottingBuildingHandler::setSuperStructureMode()
{
		this->courtYardMode = false;

	this->superStructureMode = true;
}

void CMonoplottingBuildingHandler::setBaseMode()
{
	this->courtYardMode = false;
	this->superStructureMode = false;
}


bool CMonoplottingBuildingHandler::superStructureIsInterior()const
{
	if (this->superStructureMode && !this->floor.getPointCount())
		return false;

	for (int i=1; i< this->floor.getPointCount(); i++)	
	{
		if (fabs(this->floor.getPoint(i)->z - this->floor.getPoint(0)->z) > FLT_EPSILON)
			return false;
	}
		

	return true;
}

void CMonoplottingBuildingHandler::drawingSettingsChanged(QColor& newColor,unsigned int newLineWidth)
{

	CBuildingArray* buildings = project.getBuildings();
	int nBuilding = buildings->GetSize();

	for (int k=0; k< nBuilding; k++)
	{
		CBuilding* pBuilding = buildings->GetAt(k);

		for (int i=0; i < baristaViews.getNumberOfViews(); i++)
		{
			CBaristaView* bview = baristaViews.getView(i);
			bview->changeAllTopologyLineWidth(pBuilding->getLabel(),newLineWidth);
			
			if (!pBuilding->getIsSelected())
				bview->changeAllTopologyColors(pBuilding->getLabel(),newColor);		
		}		
	}


}
#endif

