#include "Bui_DialogContainer.h"
#include "AdjustmentDlg.h"
#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include <QSizePolicy>

Bui_DialogContainer::Bui_DialogContainer(CAdjustmentDlg	*dialog,QWidget *parent,int width,int height): QDialog(parent),dialog(dialog)
{
	this->setupUi(this); // init the three buttons
	this->setAttribute(Qt::WA_DeleteOnClose,true);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));




	this->hboxLayout->addWidget(this->dialog); // add the dialog

	this->setWindowTitle(this->dialog->getTitleString().GetChar());

	this->connect(this->pB_Ok, SIGNAL(released()),this,SLOT(pbOkReleased()));
	this->connect(this->pB_Cancel, SIGNAL(released()),this,SLOT(pbCancelReleased()));
	this->connect(this->pB_Help, SIGNAL(released()),this,SLOT(pbHelpReleased()));	

	QObject::connect(this->dialog, SIGNAL(enableOkButton(bool)),this,SLOT(okButtonStatusChanged(bool)));
	QObject::connect(this->dialog, SIGNAL(closeDlg(bool)),this,SLOT(closeDlg(bool)));
//	QSize size(width, height);
//	this->resize(size);

	this->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum));

	this->dialog->ckeckDlgContent();

}

Bui_DialogContainer::~Bui_DialogContainer()
{
}


void Bui_DialogContainer::pbOkReleased()
{
	if (this->dialog->okButtonReleased())
		this->close();
}

void Bui_DialogContainer::pbCancelReleased()
{
	if (this->dialog->cancelButtonReleased())
		this->close();
}

void Bui_DialogContainer::pbHelpReleased()
{
	CHtmlHelp::callHelp(this->dialog->helpButtonReleased().GetChar());
}

void Bui_DialogContainer::okButtonStatusChanged(bool b)
{
	this->pB_Ok->setEnabled(b);
}

void Bui_DialogContainer::closeDlg(bool b)
{
	this->close();
}