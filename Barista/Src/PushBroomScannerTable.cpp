#include "PushBroomScannerTable.h"

#include <QCloseEvent>
#include "PushbroomScanner.h"

#include <math.h>
#include "utilities.h"

CPushBroomScannerTable::CPushBroomScannerTable(CPushBroomScanner* pushBroomScanner, QWidget* parent) : CBaristaTable(parent) ,pbs(pushBroomScanner)
{
	this->initTable();
}

CPushBroomScannerTable::~CPushBroomScannerTable(void)
{
	this->pbs = NULL;
}



void CPushBroomScannerTable::closeEvent ( QCloseEvent * e )
{
    e->accept();
}


void CPushBroomScannerTable::setPushBroomScanner(CPushBroomScanner* pushBroomScanner)
{
    this->pbs = pushBroomScanner;
}



void CPushBroomScannerTable::initTable()
{
    CCharString str("PushBroom Scanner Parameters:   ");
	str = str + this->pbs->getFileName()->GetFullFileName();
	this->setWindowTitle(str.GetChar());

	this->setAccessibleName(this->pbs->getFileName()->GetChar());

	this->setColumnCount(2);
	int nrows = 16;
	this->setRowCount(nrows);

	for (int i=0; i < nrows; i++)
		this->setRowHeight(i,20);

	QString yesStr("Yes");
	QString noStr("No");

	this->setColumnWidth(0, 230);
	this->setColumnWidth(1, 100);
	QStringList headerList;

	headerList.append(QString("Parameter" ));
	headerList.append(QString("Value" ));

	this->setHorizontalHeaderLabels(headerList);
     
    double value = 1233;

	int rowCamera = 0;
	int rowMounting = 7;


//	QString sys      = this->pbs->getRefer\enceSystem().getCoordinateSystemName().GetChar();
	CCCDLine *camera   = this->pbs->getCamera();
	CCameraMounting* cameraMounting = this->pbs->getCameraMounting();

	const CXYZPoint& shift   = cameraMounting->getShift();
	const CRollPitchYawRotation& rot = cameraMounting->getRotation();

	QTableWidgetItem* item;
	
//	item = this->createItem("Coordinate System",0,Qt::AlignRight,QColor(0,0,0));
//	this->setItem(0,0,item);
//	item = this->createItem(sys,0,Qt::AlignRight,QColor(0,0,0));
//	this->setItem(0,1,item);


	// camera
	item = this->createItem("Camera Parameter",0,Qt::AlignLeft,QColor(0,0,0));
	this->setItem(0 + rowCamera,0,item);
	item = this->createItem("",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0 + rowCamera,1,item);


	item = this->createItem("Principal Distance [pixel]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(1 + rowCamera,0,item);
	item = this->createItem(QString( "%1" ).arg(camera->getPrincipalDistance(), 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(1 + rowCamera,1,item);

	item = this->createItem("Principal Point x [pixel]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(2 + rowCamera,0,item);
	item = this->createItem(QString( "%1" ).arg(camera->getIRP().x, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(2 + rowCamera,1,item);

	item = this->createItem("Principal Point y [pixel]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(3 + rowCamera,0,item);
	item = this->createItem(QString( "%1" ).arg(camera->getIRP().y, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(3 + rowCamera,1,item);


	item = this->createItem("Pixel Size [m]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(5 + rowCamera,0,item);
	item = this->createItem(QString( "%1" ).arg(camera->getScale(), 0, 'E', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(5 + rowCamera,1,item);


	// mounting
	item = this->createItem("Mounting Parameter",0,Qt::AlignLeft,QColor(0,0,0));
	this->setItem( 0 + rowMounting,0,item);
	item = this->createItem("",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 0  + rowMounting,1,item);

	item = this->createItem("Shift X [m]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 1 + rowMounting,0,item);
	item = this->createItem(QString( "%1" ).arg(shift.x, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 1  + rowMounting,1,item);

	item = this->createItem("Shift Y [m]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 2 + rowMounting,0,item);
	item = this->createItem(QString( "%1" ).arg(shift.y, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 2  + rowMounting,1,item);

	item = this->createItem("Shift Z [m]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 3 + rowMounting,0,item);
	item = this->createItem(QString( "%1" ).arg(shift.z, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 3  + rowMounting,1,item);

	double angle = rot[0] * 180/M_PI;
	if (angle < 0) angle += 360;

	item = this->createItem("Roll [�]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 4 + rowMounting,0,item);
	item = this->createItem(QString( "%1" ).arg(angle, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 4  + rowMounting,1,item);

	angle = rot[1] * 180/M_PI;
	if (angle < 0) angle += 360;

	item = this->createItem("Pitch [�]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 5 + rowMounting,0,item);
	item = this->createItem(QString( "%1" ).arg(angle, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 5  + rowMounting,1,item);

	angle = rot[2] * 180/M_PI;
	if (angle < 0) angle += 360;

	item = this->createItem("Yaw [�]",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 6 + rowMounting,0,item);
	item = this->createItem(QString( "%1" ).arg(angle, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem( 6  + rowMounting,1,item);
	


}
