#include "ImageTable.h"

#include <QCloseEvent>


CImageTable::CImageTable(CVImage* image,QWidget* parent) : CBaristaTable(parent),vimage(image),imageTableRowCount(8)
{
	this->initTable();
	this->vimage->addListener(this);
}

CImageTable::~CImageTable(void)
{
}

void CImageTable::closeEvent ( QCloseEvent * e )
{
    this->vimage->removeListener(this);
    e->accept();
}


void CImageTable::setImage(CVImage* image)
{
    this->vimage = image;
}





void CImageTable::elementAdded(CLabel element)
{
	this->updateContents();
}

void CImageTable::elementDeleted(CLabel element)
{
	this->updateContents();
}


void CImageTable::initTable()
{
    CCharString str("Image Parameters:   ");
	str = str + this->vimage->getName();
	this->setAccessibleName(this->vimage->getFileNameChar());
	this->setWindowTitle(str.GetChar());


	this->setColumnCount(2);
	this->setRowCount(this->imageTableRowCount);

	for (int i=0; i < this->imageTableRowCount; i++)
		this->setRowHeight(i,20);

	this->setColumnWidth(0, 120);
	this->setColumnWidth(1, 100);
	QStringList headerList;

	headerList.append(QString("Parameter" ));
	headerList.append(QString("Value" ));

	this->setHorizontalHeaderLabels(headerList);
     
	int width = this->vimage->getHugeImage()->getWidth();
	int height = this->vimage->getHugeImage()->getHeight();
	int bands = this->vimage->getHugeImage()->getBands();
	int bpp = this->vimage->getHugeImage()->getBpp();

	QTableWidgetItem* item;
	
	////// Image parameters
	item = this->createItem("Width",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,0,item);
	item = this->createItem(QString( "%1" ).arg(width),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,1,item);

	item = this->createItem("Height",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(1,0,item);
	item = this->createItem(QString( "%1" ).arg(height),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(1,1,item);

	item = this->createItem("Bands",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(2,0,item);
	item = this->createItem(QString( "%1" ).arg(bands),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(2,1,item);

	item = this->createItem("Bpp",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(3,0,item);
	item = this->createItem(QString( "%1" ).arg(bpp),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(3,1,item);

	item = this->createItem("Number of pixels",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(4,0,item);
	item = this->createItem(QString( "%1" ).arg(width*height),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(4,1,item);

	QString imgprojname(this->vimage->getFileNameChar());

	item = this->createItem("Image project name",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(5,0,item);
	item = this->createItem(imgprojname,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(5,1,item);


	QString filename(this->getImage()->getHugeImage()->getFileNameChar());

	int col1width = 6*filename.length();
	this->setColumnWidth(1, col1width);
	item = this->createItem("File name on disk",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(6,0,item);
	item = this->createItem(filename,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(6,1,item);
	
	item = this->createItem("Current SensorModel",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(7,0,item);
	item = this->createItem(this->getSensorModelName(),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(7,1,item);
}


QString CImageTable::getSensorModelName()
{
	QString qstr("");

	if( this->getImage()->getCurrentSensorModel() != NULL)
	{
		CCharString guiname = this->getImage()->getCurrentSensorModel()->getGUIName();
		qstr.append(guiname.GetChar());
	}
	else qstr.append("no SensorModel set");

	return qstr;
}