#include "Bui_GDALExportDlg.h"

#include "ProgressThread.h"
#include "Bui_ProgressDlg.h"
#include "BaristaHtmlHelp.h"
#include "SystemUtilities.h"
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include "IniFile.h"
#include "Icons.h"


Bui_GDALExportDlg::Bui_GDALExportDlg(CFileName exportFileName,
									 CVBase* srcImage,
									 bool isDEM,
									 const char* title,
									 bool MakeBandsAccessible ,
									 bool geoCodedDefault,
									 bool makeFileAccessibleByDefaultFlag):

		internalImageExporter(exportFileName,srcImage,isDEM),
		success(false),exportToDirectory(false),
		currentImageExporter(internalImageExporter)

{
	this->initDlg(title,MakeBandsAccessible,geoCodedDefault,makeFileAccessibleByDefaultFlag);
}

Bui_GDALExportDlg::Bui_GDALExportDlg(CImageExporter& externalImageExporter,
									 const char* title,
									 bool MakeBandsAccessible,
									 bool geoCodedDefault,
									 bool makeFileAccessibleByDefaultFlag) :
	currentImageExporter(externalImageExporter),internalImageExporter("",NULL,false),
	success(false),exportToDirectory(false)
{
	this->initDlg(title,MakeBandsAccessible,geoCodedDefault,makeFileAccessibleByDefaultFlag);
}


void Bui_GDALExportDlg::initDlg(const char* title,bool MakeBandsAccessible,bool geoCodedDefault,bool makeFileAccessibleByDefaultFlag)
{
	this->setupUi(this);
	this->setWindowTitle(QString(title));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	QStringList filetypes;
	filetypes << "TIFF (*.tif)"
			 << "JPEG (*.jpg)"
			 << "JPEG 2000 (*.jp2)"
			 << "ECW  (*.ecw)";

	this->cB_Filetype->addItems(filetypes);


	const CFileName& fn = this->currentImageExporter.getFilename();
	if (  (!fn.IsEmpty() && !fn.GetFileName().IsEmpty()) &&	 !makeFileAccessibleByDefaultFlag)
	{
		this->pB_FileExport->setEnabled(false);
	}
	else if (!fn.IsEmpty() && fn.GetFileName().IsEmpty())
	{
		this->exportToDirectory = true;
		this->pB_FileExport->setText("Select Directory");
	}
	else
		this->pB_FileExport->setEnabled(true);


	if (this->currentImageExporter.hasGeoInformation() && geoCodedDefault)
		this->currentImageExporter.setExportGeo(true);


	if (this->currentImageExporter.getDepth() == 32 && this->currentImageExporter.getBands() > 1)
	{
		QMessageBox::critical(this,"Export error!","Only 1 channel 32 Bit images are supported!");
		this->close();
	}

	this->connect(this->pB_FileExport,   SIGNAL(released()),this,SLOT(pBSelectFileReleased()));
	this->connect(this->pB_Export,       SIGNAL(released()),this,SLOT(pBExportReleased()));
	this->connect(this->pB_Cancel,       SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pB_Help,         SIGNAL(released()),this,SLOT(pBHelpReleased()));

	this->connect(this->rB_Jpeg,         SIGNAL(toggled(bool)),this,SLOT(rBJpegToggled(bool)));
	this->connect(this->rB_LZW,          SIGNAL(released()),this,SLOT(rBLZWReleased()));
	this->connect(this->rB_None,         SIGNAL(released()),this,SLOT(rBNoneReleased()));
	this->connect(this->hS_Jpeg,         SIGNAL(valueChanged(int)),this,SLOT(hSJpegSliderChanged(int)));

	this->connect(this->rB_8Bit,         SIGNAL(released()),this,SLOT(rB8BitReleased()));
	this->connect(this->rB_16Bit,        SIGNAL(released()),this,SLOT(rB16BitReleased()));
	this->connect(this->rB_32Bit,        SIGNAL(released()),this,SLOT(rB32BitReleased()));

	this->connect(this->rB_1Channel,     SIGNAL(released()),this,SLOT(rB1ChannelReleased()));
	this->connect(this->rB_3Channels,    SIGNAL(released()),this,SLOT(rB3ChannelsReleased()));
	this->connect(this->rB_4Channels,    SIGNAL(released()),this,SLOT(rB4ChannelsReleased()));

	this->connect(this->rB_Red,          SIGNAL(released()),this,SLOT(rBRedReleased()));
	this->connect(this->rB_Green,        SIGNAL(released()),this,SLOT(rBGreenReleased()));
	this->connect(this->rB_Blue,         SIGNAL(released()),this,SLOT(rBBlueReleased()));
	this->connect(this->rB_Alpha,        SIGNAL(released()),this,SLOT(rBAlphaReleased()));
	this->connect(this->rB_Intensity,    SIGNAL(released()),this,SLOT(rBIntensityReleased()));
	this->connect(this->rB_RGB,          SIGNAL(released()),this,SLOT(rBRGBReleased()));
	this->connect(this->rB_CIR,          SIGNAL(released()),this,SLOT(rBCIRReleased()));

	this->connect(this->cB_ExportGeo,    SIGNAL(released()),this,SLOT(cBExportGeoReleased()));
	this->connect(this->cb_ExportExtern, SIGNAL(released()),this,SLOT(cBExportExternReleased()));

	this->connect(this->cB_Filetype,     SIGNAL(currentIndexChanged(int)),this,SLOT(cBFileTypeCurrentIndexChanged(int)));

	this->connect(this->cb_TiledTiff,    SIGNAL(released()),this,SLOT(cbTiledTiffReleased()));
	this->connect(this->rB_Pixel,        SIGNAL(released()),this,SLOT(rBPixelReleased()));
	this->connect(this->rB_Band,         SIGNAL(released()),this,SLOT(rBBandReleased()));


	this->applySettings();
	if (!MakeBandsAccessible)
	{
		this->gB_BandSelector->setVisible(false);
		this->gb_Settings->setVisible(false);
	}
}


Bui_GDALExportDlg::~Bui_GDALExportDlg(void)
{


}


bool Bui_GDALExportDlg::writeImage(const char* title)
{
	this->currentImageExporter.writeSettings();
	bui_ProgressDlg dlg(new CGDALExportThread(&this->currentImageExporter),&this->currentImageExporter,title);
	dlg.exec();
	return dlg.getSuccess();
}

bool Bui_GDALExportDlg::writeSettings()
{
	return this->currentImageExporter.writeSettings();
}

/* Slots Start +++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void Bui_GDALExportDlg::pBSelectFileReleased()
{
	if ( !this->exportToDirectory )
	{
		QString filename = QFileDialog::getSaveFileName(this,
							"Choose File",
							inifile.getCurrDir(),
							"Fileformats (*.tif *.jpg *.jp2 *.j2k *.ecw)",
							NULL);

		CFileName testname((const char*)filename.toLatin1());

		if (!testname.IsEmpty())
			this->currentImageExporter.setFilename(testname);
	}
	else
	{
		QString dirname = QFileDialog::getExistingDirectory(this,
							"Choose Directory",
							this->currentImageExporter.getFilename().GetChar(),
							QFileDialog::ShowDirsOnly);

		CFileName testname((const char*)dirname.toLatin1());

		if (!testname.IsEmpty())
				this->currentImageExporter.setFilename(testname + CSystemUtilities::dirDelimStr);
	}

	this->applySettings();
}


void Bui_GDALExportDlg::pBExportReleased()
{
	this->setSuccess(this->currentImageExporter.writeSettings());
	this->close();
}

void Bui_GDALExportDlg::pBCancelReleased()
{
	this->setSuccess(false);
	this->close();
}

void Bui_GDALExportDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/ExportImage.html");
}


void Bui_GDALExportDlg::rBJpegToggled(bool b)
{
	if (b)
	{
		this->currentImageExporter.setCompression("Jpeg");
	}
	this->applySettings();
}


void Bui_GDALExportDlg::rBLZWReleased()
{
	this->currentImageExporter.setCompression("LZW");
	this->applySettings();
}

void Bui_GDALExportDlg::rBNoneReleased()
{
	this->currentImageExporter.setCompression("None");
	this->applySettings();
}

void Bui_GDALExportDlg::cBFileTypeCurrentIndexChanged(int index)
{
	if (index == 0)
		this->currentImageExporter.setFileType("tif");
	else if (index == 1)
	{
		this->currentImageExporter.setFileType("jpg");
	}
	else if (index == 2)
	{
		this->currentImageExporter.setFileType("jp2");
	}
	else if (index == 3)
	{
		this->currentImageExporter.setFileType("ecw");
	}
	else
		this->currentImageExporter.setFileType("unknown");

	this->applySettings();
}

void Bui_GDALExportDlg::hSJpegSliderChanged(int index )
{
	this->currentImageExporter.setQuality(index);
	this->applySettings();
}


void Bui_GDALExportDlg::rB8BitReleased()
{
	this->currentImageExporter.setDepth(8);
	this->applySettings();
}

void Bui_GDALExportDlg::rB16BitReleased()
{
	this->currentImageExporter.setDepth(16);
	this->applySettings();
}

void Bui_GDALExportDlg::rB32BitReleased()
{
	this->currentImageExporter.setDepth(32);
	this->applySettings();
}

void Bui_GDALExportDlg::rB1ChannelReleased()
{
	this->currentImageExporter.setBands(1);
	this->applySettings();
}

void Bui_GDALExportDlg:: rB3ChannelsReleased()
{
	this->currentImageExporter.setBands(3);
	this->applySettings();
}

void Bui_GDALExportDlg::rB4ChannelsReleased()
{
	this->currentImageExporter.setBands(4);
	this->applySettings();
}


void Bui_GDALExportDlg::rBRedReleased()
{
	this->currentImageExporter.setChannelCombination("Red");
	this->applySettings();
}


void Bui_GDALExportDlg::rBGreenReleased()
{
	this->currentImageExporter.setChannelCombination("Green");
	this->applySettings();
}

void Bui_GDALExportDlg::rBBlueReleased()
{
	this->currentImageExporter.setChannelCombination("Blue");
	this->applySettings();
}

void Bui_GDALExportDlg::rBAlphaReleased()
{
	this->currentImageExporter.setChannelCombination("Alpha");
	this->applySettings();
}

void Bui_GDALExportDlg::rBIntensityReleased()
{
	this->currentImageExporter.setChannelCombination("Intensity");
	this->applySettings();
}

void Bui_GDALExportDlg::rBRGBReleased()
{
	this->currentImageExporter.setChannelCombination("RGB");
	this->applySettings();
}

void Bui_GDALExportDlg::rBCIRReleased()
{
	this->currentImageExporter.setChannelCombination("CIR");
	this->applySettings();
}

void Bui_GDALExportDlg::cBExportGeoReleased()
{

	if (this->currentImageExporter.getExportGeo())
		this->currentImageExporter.setExportGeo(false);
	else
		this->currentImageExporter.setExportGeo(true);

	this->applySettings();
}

void Bui_GDALExportDlg::cBExportExternReleased()
{
	if (this->currentImageExporter.getExportExtern())
		this->currentImageExporter.setExportExtern(false);
	else
		this->currentImageExporter.setExportExtern(true);

	this->applySettings();
}

void Bui_GDALExportDlg::cbTiledTiffReleased()
{
	if (this->currentImageExporter.getTiledTiff())
		this->currentImageExporter.setTiledTiff(false);
	else
		this->currentImageExporter.setTiledTiff(true);

	this->applySettings();
}

void Bui_GDALExportDlg::pBExportExternReleased()
{
        bool ok;
		QString filename = QInputDialog::getText(this,
											QString("Change Filename           "),
											QString("Filename"),
											QLineEdit::Normal,
											QString(this->currentImageExporter.getGeoFileName().GetFileName()),&ok);
        if (ok && !filename.isEmpty())
			this->currentImageExporter.setGeoFileName((const char*)filename.toLatin1());

}

void Bui_GDALExportDlg::rBPixelReleased()
{
	this->currentImageExporter.setBandInterlave(false);
	this->applySettings();
}

void Bui_GDALExportDlg::rBBandReleased()
{
	this->currentImageExporter.setBandInterlave(true);
	this->applySettings();
}


/* Slots End+++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void Bui_GDALExportDlg::applySettings()
{

	this->cB_Filetype->blockSignals(true);
	this->cb_TiledTiff->blockSignals(true);
	this->cb_ExportExtern->blockSignals(true);
	this->cB_ExportGeo->blockSignals(true);
	this->rB_16Bit->blockSignals(true);
	this->rB_8Bit->blockSignals(true);
	this->rB_32Bit->blockSignals(true);
	this->rB_1Channel->blockSignals(true);
	this->rB_3Channels->blockSignals(true);
	this->rB_4Channels->blockSignals(true);
	this->rB_Alpha->blockSignals(true);
	this->rB_Blue->blockSignals(true);
	this->rB_CIR->blockSignals(true);
	this->rB_Intensity->blockSignals(true);
	this->rB_Jpeg->blockSignals(true);
	this->rB_LZW->blockSignals(true);
	this->rB_None->blockSignals(true);
	this->rB_Red->blockSignals(true);
	this->rB_RGB->blockSignals(true);
	this->rB_Pixel->blockSignals(true);
	this->rB_Band->blockSignals(true);

	// check first all available Settings
	this->checkAvailabilityOfSettings();


	// write filename
	this->le_Dir->setText(QString(this->currentImageExporter.getFilename().GetPath()));

	if ( !this->exportToDirectory)
	{
		if (!this->currentImageExporter.getFilename().IsEmpty())
		{
			this->le_FileExport->setText(QString(this->currentImageExporter.getFilename().GetFullFileName()));

			// this->setGeoFileName() was never called when used from BandMerger
			if ( this->currentImageExporter.getGeoFileName().IsEmpty() ) this->currentImageExporter.setGeoFileName();

			// write geo filename
			if (this->currentImageExporter.getExportExtern())
				this->le_FileGeo->setText(this->currentImageExporter.getGeoFileName().GetChar());
		}
	}
	else
	{
		if (!this->currentImageExporter.getFilename().IsEmpty())
		{
			this->le_FileExport->setText("");
			this->le_FileExport->setEnabled(false);
			this->le_Dir->setText(QString(this->currentImageExporter.getFilename().GetChar()));
		}
	}


	// select filetype
	if (this->currentImageExporter.getFileType().CompareNoCase("tif") )
		this->cB_Filetype->setCurrentIndex(0);
	else if (this->currentImageExporter.getFileType().CompareNoCase("jpg"))
		this->cB_Filetype->setCurrentIndex(1);
	else if (this->currentImageExporter.getFileType().CompareNoCase("jp2"))
		this->cB_Filetype->setCurrentIndex(2);
	else if (this->currentImageExporter.getFileType().CompareNoCase("ecw"))
		this->cB_Filetype->setCurrentIndex(3);


	// compression settings
	this->le_Quality->setText(QString().number(this->currentImageExporter.getQuality()));
	this->hS_Jpeg->setSliderPosition(this->currentImageExporter.getQuality());

	if (this->currentImageExporter.getCompression().CompareNoCase("None"))
		this->rB_None->setChecked(true);
	else if (this->currentImageExporter.getCompression().CompareNoCase("LZW"))
		this->rB_LZW->setChecked(true);
	else if (this->currentImageExporter.getCompression().CompareNoCase("Jpeg"))
		this->rB_Jpeg->setChecked(true);
	if (this->currentImageExporter.getFileType().CompareNoCase("jpg") || this->currentImageExporter.getFileType().CompareNoCase("jp2"))
		this->rB_Jpeg->setChecked(true);


	// depth settings
	if (this->currentImageExporter.getDepth() == 8)
		this->rB_8Bit->setChecked(true);
	else if (this->currentImageExporter.getDepth() == 16)
		this->rB_16Bit->setChecked(true);
	else if (this->currentImageExporter.getDepth() == 32)
		this->rB_32Bit->setChecked(true);


	// channel settings
	if (this->currentImageExporter.getBands() == 1)
		this->rB_1Channel->setChecked(true);
	else
		this->rB_1Channel->setChecked(false);

	if (this->currentImageExporter.getBands() == 3)
		this->rB_3Channels->setChecked(true);
	else
		this->rB_3Channels->setChecked(false);
	if (this->currentImageExporter.getBands() == 4)
		this->rB_4Channels->setChecked(true);
	else
		this->rB_4Channels->setChecked(false);

	if (this->currentImageExporter.getChannelCombination().CompareNoCase("Red"))
		this->rB_Red->setChecked(true);
	else if (this->currentImageExporter.getChannelCombination().CompareNoCase("Green"))
		this->rB_Green->setChecked(true);
	else if (this->currentImageExporter.getChannelCombination().CompareNoCase("Blue"))
		this->rB_Blue->setChecked(true);
	else if (this->currentImageExporter.getChannelCombination().CompareNoCase("Alpha"))
		this->rB_Alpha->setChecked(true);
	else if (this->currentImageExporter.getChannelCombination().CompareNoCase("Intensity"))
		this->rB_Intensity->setChecked(true);
	else if (this->currentImageExporter.getChannelCombination().CompareNoCase("RGB"))
		this->rB_RGB->setChecked(true);
	else if (this->currentImageExporter.getChannelCombination().CompareNoCase("CIR"))
		this->rB_CIR->setChecked(true);

	// geo setting
	if (this->currentImageExporter.getExportGeo())
		this->cB_ExportGeo->setChecked(true);

	if (this->currentImageExporter.getExportExtern())
		this->cb_ExportExtern->setChecked(true);

	if (this->currentImageExporter.getTiledTiff())
		this->cb_TiledTiff->setChecked(true);
	else
		this->cb_TiledTiff->setChecked(false);

	if (this->currentImageExporter.getBandInterlave())
		this->rB_Band->setChecked(true);
	else
		this->rB_Pixel->setChecked(true);

	this->cB_Filetype->blockSignals(false);
	this->cb_TiledTiff->blockSignals(false);
	this->cb_ExportExtern->blockSignals(false);
	this->cB_ExportGeo->blockSignals(false);
	this->rB_16Bit->blockSignals(false);
	this->rB_8Bit->blockSignals(false);
	this->rB_32Bit->blockSignals(false);
	this->rB_1Channel->blockSignals(false);
	this->rB_3Channels->blockSignals(false);
	this->rB_4Channels->blockSignals(false);
	this->rB_Alpha->blockSignals(false);
	this->rB_Blue->blockSignals(false);
	this->rB_CIR->blockSignals(false);
	this->rB_Intensity->blockSignals(false);
	this->rB_Jpeg->blockSignals(false);
	this->rB_LZW->blockSignals(false);
	this->rB_None->blockSignals(false);
	this->rB_Red->blockSignals(false);
	this->rB_RGB->blockSignals(false);
	this->rB_Pixel->blockSignals(false);
	this->rB_Band->blockSignals(false);
}

void Bui_GDALExportDlg::checkAvailabilityOfSettings()
{
	// do we have a filename yet?
	if (this->currentImageExporter.getFilename().IsEmpty())
		this->pB_Export->setEnabled(false);
	else
		this->pB_Export->setEnabled(true);

	this->availabilityChannelSettings();
	this->availabilityCompressionSettings();
	this->availabilityDepthSettings();
	this->availabilityGeoSettings();
	this->availabilityTIFFSettings();
}

void Bui_GDALExportDlg::availabilityCompressionSettings()
{

	if (this->currentImageExporter.getDepth() != 8 && !this->currentImageExporter.getFileType().CompareNoCase("jpg"))
	{
		this->rB_Jpeg->setEnabled(false);
		this->hS_Jpeg->setEnabled(false);
	}
	else
	{
		this->rB_Jpeg->setEnabled(true);
		this->hS_Jpeg->setEnabled(true);
	}

	if (this->currentImageExporter.getFileType().CompareNoCase("jpg") || this->currentImageExporter.getFileType().CompareNoCase("jp2") )
	{
		this->rB_LZW->setEnabled(false);
		this->rB_None->setEnabled(false);
		this->rB_Jpeg->setEnabled(true);
	}
	else
	{
		this->rB_LZW->setEnabled(true);
		this->rB_None->setEnabled(true);
	}

	if ( !this->currentImageExporter.getFileType().CompareNoCase("jpg") &&
		 !this->currentImageExporter.getFileType().CompareNoCase("tif") &&
		 !this->currentImageExporter.getFileType().CompareNoCase("jp2"))
		this->gb_Compression->setEnabled(false);
	else
		this->gb_Compression->setEnabled(true);


	if (this->currentImageExporter.getCompression().CompareNoCase("Jpeg"))
		this->hS_Jpeg->setEnabled(true);
	else
		this->hS_Jpeg->setEnabled(false);
}


void Bui_GDALExportDlg::availabilityDepthSettings()
{
	if (this->currentImageExporter.getDataDepth() == 8 ||
		this->currentImageExporter.getFileType().CompareNoCase("jpg") ||
		this->currentImageExporter.getFileType().CompareNoCase("ecw") ||
		(this->currentImageExporter.getCompression().CompareNoCase("Jpeg") &&
		 this->currentImageExporter.getFileType().CompareNoCase("jpg")))
	{
		this->rB_16Bit->setEnabled(false);
		this->rB_32Bit->setEnabled(false);
	}
	else if (this->currentImageExporter.getDataDepth() == 16)
	{
		this->rB_8Bit->setEnabled(true);
		this->rB_16Bit->setEnabled(true);
		this->rB_32Bit->setEnabled(false);
	}
	else if (this->currentImageExporter.getDataDepth() == 32)
	{
		this->rB_8Bit->setEnabled(true);
		this->rB_16Bit->setEnabled(true);
		this->rB_32Bit->setEnabled(true);
	}

}

void Bui_GDALExportDlg::availabilityChannelSettings()
{
	if (this->currentImageExporter.getDataBands() == 1)
	{
		this->rB_3Channels->setEnabled(false);
		this->rB_4Channels->setEnabled(false);
		this->gB_3Channels->setEnabled(false);

		this->rB_Red->setEnabled(false);
		this->rB_Green->setEnabled(false);
		this->rB_Blue->setEnabled(false);
		this->rB_Alpha->setEnabled(false);
		this->rB_Intensity->setEnabled(true);
	}
	else if (currentImageExporter.getDataBands() == 3)
	{
		this->rB_3Channels->setEnabled(true);
		this->rB_4Channels->setEnabled(false);
		this->gB_3Channels->setEnabled(false);

		this->rB_Red->setEnabled(true);
		this->rB_Green->setEnabled(true);
		this->rB_Blue->setEnabled(true);
		this->rB_Alpha->setEnabled(false);
		this->rB_Intensity->setEnabled(true);

	}

	else if (this->currentImageExporter.getDataBands() == 4)
	{
		this->rB_3Channels->setEnabled(true);
		this->rB_4Channels->setEnabled(true);
		this->gB_3Channels->setEnabled(true);

		this->rB_Red->setEnabled(true);
		this->rB_Green->setEnabled(true);
		this->rB_Blue->setEnabled(true);
		this->rB_Alpha->setEnabled(true);
		this->rB_Intensity->setEnabled(true);
	}


	if (this->currentImageExporter.getBands() == 1 )
	{
		this->gB_1Channel->setEnabled(true);
		this->gB_3Channels->setEnabled(false);

	}
	else if (this->currentImageExporter.getBands() == 3)
	{
		this->gB_1Channel->setEnabled(false);
		if (this->currentImageExporter.getDataBands() == 4)
			this->gB_3Channels->setEnabled(true);
		else
			this->gB_3Channels->setEnabled(false);
	}
	else if (this->currentImageExporter.getBands() == 4)
	{
		this->gB_1Channel->setEnabled(false);
		this->gB_3Channels->setEnabled(false);
	}


}


void Bui_GDALExportDlg::availabilityGeoSettings()
{

	if (!this->currentImageExporter.hasGeoInformation())
		this->gB_Geo->setEnabled(false);

	if (this->currentImageExporter.getExportGeo())
		this->cb_ExportExtern->setEnabled(true);
	else
		this->cb_ExportExtern->setEnabled(false);

}

void Bui_GDALExportDlg::availabilityTIFFSettings()
{
	if (this->currentImageExporter.getFileType().CompareNoCase("tif"))
	{
		this->gb_TiledTIFF->setEnabled(true);

	}
	else
	{
		this->gb_TiledTIFF->setEnabled(false);
	}
}

