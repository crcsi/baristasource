#include "Bui_DEMStatistics.h"
#include "VDEM.h"
#include <QColorDialog>
#include <QPalette>
#include "IniFile.h"
#include "BaristaProject.h"


Bui_DEMStatistics::Bui_DEMStatistics(CVDEM* vdem,QWidget *parent)
	: QDialog(parent),success(false),nStatisticRows(4),
		nStatisticCols(4),showAvg(true),showDevAvg(true),
		showMin(true),showMax(true),showDev(false),statTextColor(Qt::blue),
		statLineColor(Qt::blue),vdem(vdem),statFontSize(10),nLegendeClasses(10),
		computeLegende(false)
{
	this->setupUi(this);
	this->sB_Rows->setMinimum(1);
	this->sB_Cols->setMinimum(1);
	this->sB_FontSize->setMinimum(1);
	this->sB_Legende->setMinimum(2);
	

	// init from dem
	if (this->vdem->getNStatisticRows() >= 1)
	{
		this->nStatisticRows = this->vdem->getNStatisticRows();
		this->nStatisticCols = this->vdem->getNStatisticCols();
		this->showAvg = this->vdem->getShowAvg();
		this->showDevAvg = this->vdem->getShowDevAvg();
		this->showMin = this->vdem->getShowMin();
		this->showMax = this->vdem->getShowMax();
		this->showDev = this->vdem->getShowDev();
		const int *stc = this->vdem->getStatTextColor();
		const int *slc = this->vdem->getStatLineColor();
		QColor qstc(stc[0], stc[1], stc[2]);
		QColor qslc(slc[0], slc[1], slc[2]);
		this->statTextColor = qstc;
		this->statLineColor = qslc;
		this->statFontSize = this->vdem->getStatFontSize();
		this->nLegendeClasses = this->vdem->getNLegendeClasses();

	}
	else if (inifile.getDEM_NStatisticRows() >= 1) // use data from ini file
	{
		this->nStatisticRows = inifile.getDEM_NStatisticRows();
		this->nStatisticCols = inifile.getDEM_NStatisticCols();
		this->showAvg = inifile.getDEM_ShowAvg();
		this->showDevAvg = inifile.getDEM_ShowDevAvg();
		this->showMin = inifile.getDEM_ShowMin();
		this->showMax = inifile.getDEM_ShowMax();
		this->showDev = inifile.getDEM_ShowDev();
		this->statFontSize = inifile.getDEMStatFontSize();
		this->nLegendeClasses = inifile.getDEMNLegendeClasses();
		
		vector<int> c = inifile.getDEM_StatTextColor();
		QColor colorText(c[0],c[1],c[2]);
		this->statTextColor =colorText;
		c =inifile.getDEM_StatLineColor();
		QColor colorLine(c[0],c[1],c[2]);
		this->statLineColor = colorLine;
	}
	// else use default info from dialog




	this->connect(this->sB_Rows,SIGNAL(valueChanged(int)),this,SLOT(valueChangedRows(int)));
	this->connect(this->sB_Cols,SIGNAL(valueChanged(int)),this,SLOT(valueChangedCols(int)));
	this->connect(this->sB_FontSize,SIGNAL(valueChanged(int)),this,SLOT(valueChangedFontSize(int)));
	this->connect(this->sB_Legende,SIGNAL(valueChanged(int)),this,SLOT(valueChangedLegende(int)));

	this->cB_Avg->setChecked(this->showAvg);
	this->cB_DevAvg->setChecked(this->showDevAvg);
	this->cB_Min->setChecked(this->showMin);
	this->cB_Max->setChecked(this->showMax);
	this->cB_Dev->setChecked(this->showDev);
	this->sB_Rows->setValue(this->nStatisticRows);
	this->sB_Cols->setValue(this->nStatisticCols);
	this->sB_FontSize->setValue(this->statFontSize);
	this->sB_Legende->setValue(this->nLegendeClasses);

	this->connect(this->cB_Avg,   SIGNAL(stateChanged(int)),this,SLOT(stateChangedAvg(int)));
	this->connect(this->cB_DevAvg,SIGNAL(stateChanged(int)),this,SLOT(stateChangedDevAvg(int)));
	this->connect(this->cB_Min,   SIGNAL(stateChanged(int)),this,SLOT(stateChangedMin(int)));
	this->connect(this->cB_Max,   SIGNAL(stateChanged(int)),this,SLOT(stateChangedMax(int)));
	this->connect(this->cB_Dev,   SIGNAL(stateChanged(int)),this,SLOT(stateChangedDev(int)));
	this->connect(this->cB_ShowStatInLegend,SIGNAL(stateChanged(int)),this,SLOT(stateChangedLegende(int)));


	this->connect(this->pB_Ok,    SIGNAL(released()),this,SLOT(pBOk()));
	this->connect(this->pB_Cancel,SIGNAL(released()),this,SLOT(pBCancel()));
	this->connect(this->pB_Help,  SIGNAL(released()),this,SLOT(pBHelp()));


	this->connect(this->pB_TextColor,SIGNAL(released()),this,SLOT(pBTextColor()));
	this->connect(this->pB_LineColor,SIGNAL(released()),this,SLOT(pBLineColor()));

	QPalette palette = this->lE_TextColor->palette();
	palette.setColor(QPalette::Base,this->statTextColor);
	this->lE_TextColor->setPalette(palette);

	palette = this->lE_LineColor->palette();
	palette.setColor(QPalette::Base,this->statLineColor);
	this->lE_LineColor->setPalette(palette);
	this->cB_ShowStatInLegend->setChecked(this->computeLegende);

}

Bui_DEMStatistics::~Bui_DEMStatistics()
{
	
}


void Bui_DEMStatistics::stateChangedAvg(int state)
{
	if (state == Qt::Checked)
		this->showAvg = true;
	else
		this->showAvg = false;

}

void Bui_DEMStatistics::stateChangedDevAvg(int state)
{
	if (state == Qt::Checked)
		this->showDevAvg = true;
	else
		this->showDevAvg = false;
}

void Bui_DEMStatistics::stateChangedMin(int state)
{
	if (state == Qt::Checked)
		this->showMin = true;
	else
		this->showMin = false;
}

void Bui_DEMStatistics::stateChangedMax(int state)
{
	if (state == Qt::Checked)
		this->showMax = true;
	else
		this->showMax = false;
}

void Bui_DEMStatistics::stateChangedDev(int state)
{
	if (state == Qt::Checked)
		this->showDev = true;
	else
		this->showDev = false;
}

void Bui_DEMStatistics::valueChangedRows(int nRows)
{
	this->nStatisticRows = nRows;
	this->lE_Rows->setText(QString("%1").arg(this->vdem->getDEM()->getHeight() / this->nStatisticRows));
	
}

void Bui_DEMStatistics::valueChangedCols(int nCols)
{
	this->nStatisticCols = nCols;
	this->lE_Cols->setText(QString("%1").arg(this->vdem->getDEM()->getWidth() / this->nStatisticCols));
}

void Bui_DEMStatistics::pBOk()
{
	this->success = true;

	vector<int> colorText(3);
	colorText[0] = this->statTextColor.red();
	colorText[1] = this->statTextColor.green();
	colorText[2] = this->statTextColor.blue();

	vector<int> colorLine(3);
	colorLine[0] = this->statLineColor.red();
	colorLine[1] = this->statLineColor.green();
	colorLine[2] = this->statLineColor.blue();

	inifile.setDEMStatistics(	this->nStatisticRows,
								this->nStatisticCols,
								this->showAvg,
								this->showDevAvg,
								this->showMin,
								this->showMax,
								this->showDev,
								colorText,
								colorLine,
								this->statFontSize,
								this->nLegendeClasses);
	inifile.writeIniFile();

	this->close();
}

void Bui_DEMStatistics::pBCancel()
{
	this->success = false;
	this->close();

}
void Bui_DEMStatistics::pBHelp()
{
	
}

void Bui_DEMStatistics::pBTextColor()
{
	this->statTextColor = QColorDialog::getColor(this->statTextColor,this);
	QPalette palette = this->lE_TextColor->palette();
	palette.setColor(QPalette::Base,this->statTextColor);
	this->lE_TextColor->setPalette(palette);
}

void Bui_DEMStatistics::pBLineColor()
{
	this->statLineColor = QColorDialog::getColor(this->statLineColor,this);
	QPalette palette = this->lE_LineColor->palette();
	palette.setColor(QPalette::Base,this->statLineColor);
	this->lE_LineColor->setPalette(palette);
}

void Bui_DEMStatistics::valueChangedFontSize(int fontSize)
{
	this->statFontSize = fontSize;
}

void Bui_DEMStatistics::valueChangedLegende(int value)
{
	this->nLegendeClasses = value;
}

void Bui_DEMStatistics::stateChangedLegende(int state)
{
	if (state == Qt::Checked)
	{
		this->computeLegende = true;
	}
	else
	{
		this->computeLegende = false;
	}
}


