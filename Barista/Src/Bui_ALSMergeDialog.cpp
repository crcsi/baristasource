#include <float.h> 

#include <QTableWidgetItem>
#include <QMessageBox> 
#include <QHeaderView>
#include <QFileDialog>

#include "Bui_ALSMergeDialog.h"

#include "ProtocolHandler.h"
#include "SystemUtilities.h"
#include "BaristaProject.h"
#include "BaristaHtmlHelp.h"
#include "Icons.h"
#include "BaristaProjectHelper.h"
#include "ALSDataWithTiles.h"

bui_ALSMergeDialog::bui_ALSMergeDialog () : success(false), als(0), alsTiles(0)
{
	setupUi(this);

	connect(this->buttonOk,              SIGNAL(released()),               this, SLOT(OK()));
	connect(this->buttonCancel,          SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->HelpButton,            SIGNAL(released()),               this, SLOT(Help()));
	connect(this->ALSList,               SIGNAL(itemSelectionChanged()),   this, SLOT(onALSListChanged()));
	connect(this->BrowseButton,          SIGNAL(released()),               this, SLOT(onBrowseSelected()));
	
	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}

bui_ALSMergeDialog::~bui_ALSMergeDialog ()
{
	this->als = 0;
	this->alsTiles = 0;
	this->destroy();
}

void bui_ALSMergeDialog::init()
{
	this->success = false;

	this->AvailableALSList.clear();

	for (int i = 0; i < project.getNumberOfAlsDataSets(); ++i)
	{
		CVALS *als = project.getALSAt(i);
		AvailableALSList.push_back(project.getALSAt(i));
	}

	this->setupALSSelector();

	this->outputFile = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "ALS.ALS";

	this->OutputFile->blockSignals(true);
	this->OutputFile->setEnabled(false);

	this->OutputFile->setText(this->outputFile.GetChar());

	this->OutputFile->blockSignals(false);

	this->buttonOk->setFocusPolicy(Qt::NoFocus);	
	this->buttonCancel->setFocusPolicy(Qt::NoFocus);

	this->checkSetup();
}

void bui_ALSMergeDialog::destroy()
{
	this->AvailableALSList.clear();
	this->SelectedALSList.clear();
}

void bui_ALSMergeDialog::setupALSSelector()
{
	this->SelectedALSList.clear();
	this->ALSList->blockSignals(true);
	this->ALSList->setColumnCount(1);
	this->ALSList->setRowCount(this->AvailableALSList.size());
	this->ALSList->setHorizontalHeaderItem(0,new QTableWidgetItem("Select images"));
	this->ALSList->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

	for (unsigned int i = 0; i < this->AvailableALSList.size(); ++i)
	{
		QTableWidgetItem *it = new QTableWidgetItem(AvailableALSList[i]->getFileNameChar());
		//it->setData(Qt::EditRole,i);
		this->ALSList->setItem(i,0,it);
		this->SelectedALSList.push_back(AvailableALSList[i]);
	}

	this->ALSList->selectAll();

	this->ALSList->blockSignals(false);

	this->checkSetup();
};

void bui_ALSMergeDialog::onALSListChanged()
{
	this->SelectedALSList.clear();
	this->ALSList->blockSignals(true);

	QList<QTableWidgetItem *> list = this->ALSList->selectedItems();
	for (int i = 0; i < list.count(); i++)
	{
		unsigned int index = list[i]->row();
		if ( index < this->AvailableALSList.size())
		{
			this->SelectedALSList.push_back(this->AvailableALSList[index]);
		}
	}

	this->ALSList->blockSignals(false);
	this->checkSetup();
}

void bui_ALSMergeDialog::onBrowseSelected()
{
	QString fnQ = QFileDialog::getSaveFileName(this, "Choose Output File", 
		                                        this->outputFile.GetPath().GetChar(), "*.als");

	CFileName fn = ((const char*)fnQ.toLatin1());
		
	if (!fn.IsEmpty()) this->outputFile = fn;

	this->OutputFile->setText(this->outputFile.GetChar());
}

void bui_ALSMergeDialog::checkSetup()
{
	if (this->SelectedALSList.size() > 1)
	{
		this->buttonOk->setEnabled(true);
	}
	else
	{
		this->buttonOk->setEnabled(false);
	}
}


void bui_ALSMergeDialog::OK()
{
	this->success = true;
	this->alsTiles = 0;

	this->als = project.addALS();
	
	vector<CALSDataSet *> alsVec;

	for (unsigned int i = 0; i < this->SelectedALSList.size(); ++i)
	{
		CVALS *als = this->SelectedALSList[i];
		alsVec.push_back(als->getCALSData());
	}

	if (!this->als) success = false;
	else 
	{
		this->alsTiles = this->als->initALSTile(this->outputFile.GetChar(), alsVec, false);
		if (!this->alsTiles)
		{
			project.deleteALS(this->als);
			this->als = 0;
			this->alsTiles = 0;
			this->success = false;
		}
	}

	if (!this->success)
	{		
		QMessageBox::critical(this, "Merging of ALS data sets failed!", "Merging of ALS data sets failed!");
	}

	this->close();
};

void bui_ALSMergeDialog::Cancel()
{
	this->success = false;
	this->close();
}

void bui_ALSMergeDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/ImageRegistrationDlg.html");
};

bool bui_ALSMergeDialog::compute()
{
	this->buttonOk->setEnabled(false);
	this->buttonCancel->setEnabled(false);
	this->update();

	if (!this->als) return false;

	baristaProjectHelper.setIsBusy(true);
	this->success = true;

	vector<CALSDataSet *> alsVec;

	for (unsigned int i = 0; i < this->SelectedALSList.size(); ++i)
	{
		CVALS *als = this->SelectedALSList[i];
		alsVec.push_back(als->getCALSData());
	}
	

	if (!this->alsTiles->init(alsVec))
	{
		project.deleteALS(this->als);
		this->als = 0;
		this->alsTiles = 0;
		this->success = false;
	}
	else baristaProjectHelper.refreshTree();

	baristaProjectHelper.setIsBusy(false);
	return this->success;
}
