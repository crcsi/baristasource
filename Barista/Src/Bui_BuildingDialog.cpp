#ifdef WIN32

#include <float.h> 

#include "Bui_BuildingDialog.h"

#include <qfiledialog.h> 
#include <QMessageBox> 

#include "ObjectSelector.h"
#include "ProgressThread.h"
#include "BaristaProject.h"
#include "Icons.h"
#include "VImage.h"
#include "VDEM.h"
#include "DEM.h"
#include "BaristaHtmlHelp.h"
#include "IniFile.h"
#include "SystemUtilities.h"
#include "ProtocolHandler.h"
#include "ProgressThread.h"
#include "Bui_ProgressDlg.h"

#include "BuildingDetector.h"
#include "DempsterShaferBuildings.h"
#include "BaristaProjectHelper.h"

#include "Bui_OutputModeHandler.h"


const char *bui_BuildingDialog::noDSMLastTxt        = "Do not use DSM Last Pulse";
const char *bui_BuildingDialog::noDSMFirstTxt       = "Do not use DSM First Pulse";
const char *bui_BuildingDialog::noDTMTxt            = "Automatically generate DTM";
const char *bui_BuildingDialog::noNDVITxt           = "Do not use NDVI";
const char *bui_BuildingDialog::noSigmaNDVITxt      = "Do not use sigma(NDVI)";
const char *bui_BuildingDialog::ExistingDatabaseTxt = "Do not use existing database";

//=============================================================================

bui_BuildingDialog::bui_BuildingDialog(CVDEM *pDSM) : pDSMLast(0), pDSMFirst(0), 
                    pDTM(0), pNDVI(0), pSigmaNDVI(0), MinBuildingHeight(2.5f), MaxBuildingSize(50),
					computationOK(false), resolution(1.0,1.0), llPoint(0.0, 0.0),
					urPoint(1.0, 1.0), extentsInit(false), progressvalue(0),
					MinBuildingSize(25.0f), MinBuildingArea(20), NDVIThresholdBuilding(0.0f),
					useFirstpulseForRoughness(false), useRoughnessStrengthBuilding(true),
					useRoughnessIsotropyBuilding(true), structureForOpen(3.0f), 
					structureForClose (3.0f), thinning(1.0f), buildingPercentage(75),
					percentageTreesBuilding(20.0f), Qmin(0.7f), kernelsizeRoughness(3),
					percentagePoints0Building(30), percentageHomog0Building(40),
					detector(0), MinBuildingHeightDTM(3.5f), NDVIThresholdBuildingDTM(0.0),
					pBuildingLabels(0), maxPostClassIterations(25), useWaterModel(false),
					useRank(false), rankPercentage(0.05f),
					FirstLastThresholdValue(2.5f), PointPercentageThresholdValue(0.5f),
					assignInverseFLToTheta(true), pOutPutHandler(0)
			 
{
	setupUi(this);
	connect(this->buttonOk,                    SIGNAL(released()),               this, SLOT(OK()));
	connect(this->buttonCancel,                SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->buttonHelp,                  SIGNAL(released()),               this, SLOT(Help()));
	connect(this->DSMLastSelector,             SIGNAL(currentIndexChanged(int)), this, SLOT(DSMLastSelected()));
	connect(this->DSMFirstSelector,            SIGNAL(currentIndexChanged(int)), this, SLOT(DSMFirstSelected()));
	connect(this->DTMSelector,                 SIGNAL(currentIndexChanged(int)), this, SLOT(DTMSelected()));
	connect(this->NDVISelector,                SIGNAL(currentIndexChanged(int)), this, SLOT(NDVISelected()));
	connect(this->SigmaNDVISelector,           SIGNAL(currentIndexChanged(int)), this, SLOT(NDVISigmaSelected()));
	connect(this->ExistingDataBaseSelector,    SIGNAL(currentIndexChanged(int)), this, SLOT(ExistingDatabaseSelected()));
	connect(this->maxExtent,                   SIGNAL(editingFinished()),        this, SLOT(maxExtentChanged()));
	connect(this->minExtent,                   SIGNAL(editingFinished()),        this, SLOT(minExtentChanged()));
	connect(this->minArea,                     SIGNAL(editingFinished()),        this, SLOT(minAreaChanged()));
	connect(this->minHeight,                   SIGNAL(editingFinished()),        this, SLOT(minHeightChanged()));
	connect(this->minHeightDTM,                SIGNAL(editingFinished()),        this, SLOT(minHeightDTMChanged()));
	connect(this->buildingLLX,                 SIGNAL(editingFinished()),        this, SLOT(LLChanged()));
	connect(this->buildingLLY,                 SIGNAL(editingFinished()),        this, SLOT(LLChanged()));
	connect(this->buildingURX,                 SIGNAL(editingFinished()),        this, SLOT(RUChanged()));
	connect(this->buildingURY,                 SIGNAL(editingFinished()),        this, SLOT(RUChanged()));
	connect(this->buildingGResolution,         SIGNAL(editingFinished()),        this, SLOT(resolutionChanged()));
    connect(this->percentageTrees,             SIGNAL(editingFinished()),        this, SLOT(roughnessParametersChanged()));
    connect(this->filterSize,                  SIGNAL(editingFinished()),        this, SLOT(roughnessParametersChanged()));
    connect(this->NDVIThreshold,               SIGNAL(editingFinished()),        this, SLOT(NDVIThresholdChanged()));
    connect(this->NDVIDTMThreshold,            SIGNAL(editingFinished()),        this, SLOT(NDVIThresholdDTMChanged()));
	connect(this->HomogeneousThresholdFirstIt, SIGNAL(editingFinished()),        this, SLOT(DTMParametersChanged()));
    connect(this->PointThresholdFirstIt,       SIGNAL(editingFinished()),        this, SLOT(DTMParametersChanged()));
	connect(this->maxExtentsButton,            SIGNAL(released()),               this, SLOT(maxExtentsSelected()));
	connect(this->roughnessLastPulseButton,    SIGNAL(clicked(bool)),            this, SLOT(roughnessModeChanged()));
	connect(this->roughnessFirstPulseButton,   SIGNAL(clicked(bool)),            this, SLOT(roughnessModeChanged()));
	connect(this->useRoughnessStrength,        SIGNAL(stateChanged(int)),        this, SLOT(useRoughnessStrengthChanged()));
	connect(this->useRoughnessIso,             SIGNAL(stateChanged(int)),        this, SLOT(useRoughnessIsoChanged()));
	connect(this->StructOpen,                  SIGNAL(editingFinished()),        this, SLOT(structureForOpenChanged()));
	connect(this->StructClose,                 SIGNAL(editingFinished()),        this, SLOT(structureForCloseChanged()));
	connect(this->Thin,                        SIGNAL(editingFinished()),        this, SLOT(ThinningChanged()));
	connect(this->ExistingDatabaseProb,        SIGNAL(editingFinished()),        this, SLOT(BuildingPercentageChanged()));
	connect(this->MaxItPostProc,               SIGNAL(editingFinished()),        this, SLOT(MaxIterationChanged()));
	connect(this->OutputLevelMin,              SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMed,              SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMax,              SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->UseWaterModel,               SIGNAL(stateChanged(int)),        this, SLOT(UseWaterModelChanged()));
	connect(this->Explore,                     SIGNAL(released()),               this, SLOT(explorePathSelected()));
	connect(this->FirstLastThreshold,          SIGNAL(editingFinished()),        this, SLOT(FirstLastThresholdChanged()));
	connect(this->PointPercentage,             SIGNAL(editingFinished()),        this, SLOT(PointPercentageChanged()));
	connect(this->AssignToNotT,                SIGNAL(clicked(bool)),            this, SLOT(FLThetaModeChanged()));
	connect(this->AssignToTheta,               SIGNAL(clicked(bool)),            this, SLOT(FLThetaModeChanged()));

	connect(this->MorphologicFilter,           SIGNAL(clicked(bool)),            this, SLOT(DTMModeChanged()));
	connect(this->RankFilter,                  SIGNAL(clicked(bool)),            this, SLOT(DTMModeChanged()));
	connect(this->RankPercentage,              SIGNAL(editingFinished()),        this, SLOT(DTMRankChanged()));
	
	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));


	if (pDSM)
	{
		this->pDSMLast = pDSM;
		this->resolution.x = this->pDSMLast->getDEM()->getgridX();
		this->resolution.y = this->pDSMLast->getDEM()->getgridY();
		this->llPoint.x = this->pDSMLast->getDEM()->getminX();
		this->llPoint.y = this->pDSMLast->getDEM()->getminY();			
		this->urPoint.x = this->pDSMLast->getDEM()->getmaxX();			
		this->urPoint.y = this->pDSMLast->getDEM()->getmaxY();
		this->extentsInit = true;
	}

	this->init();
}

//=============================================================================

bui_BuildingDialog::~bui_BuildingDialog(void)
{
	if (this->pOutPutHandler) delete this->pOutPutHandler;
	this->pOutPutHandler = 0;

	if (this->detector) delete this->detector;
	this->detector = 0;

	this->pDSMLast        = 0;
	this->pDSMFirst       = 0;
	this->pDTM            = 0;
	this->pNDVI           = 0;
	this->pSigmaNDVI      = 0;
	this->pBuildingLabels = 0;
}

//=============================================================================

void bui_BuildingDialog::init()
{
	currentDir = inifile.getCurrDir();

	this->outputDir = this->currentDir;
	if (!this->outputDir.IsEmpty())
	{
		if (this->outputDir[this->outputDir.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->outputDir += CSystemUtilities::dirDelimStr;
	}

	this->initParameters(this->currentDir + CSystemUtilities::dirDelimStr + "Buildings.ini");

	this->outputdir->setText(this->outputDir.GetChar());

	setupDEMSelector  (DSMLastSelector,          this->pDSMLast,        this->noDSMLastTxt);
	setupDEMSelector  (DSMFirstSelector,         this->pDSMFirst,       this->noDSMFirstTxt);
	setupDEMSelector  (DTMSelector,              this->pDTM,            this->noDTMTxt);
	setupImageSelector(NDVISelector,             this->pNDVI,           this->noNDVITxt);
	setupImageSelector(SigmaNDVISelector,        this->pSigmaNDVI,      this->noSigmaNDVITxt);
	setupImageSelector(ExistingDataBaseSelector, this->pBuildingLabels, this->ExistingDatabaseTxt);

	this->setCurrentExtentsAndResolution();
    
	this->maxExtent->blockSignals(true);
    this->minHeight->blockSignals(true);
    this->minHeightDTM->blockSignals(true);
	this->minExtent->blockSignals(true);
	this->roughnessLastPulseButton->blockSignals(true);
    this->roughnessFirstPulseButton->blockSignals(true);
	this->percentageTrees->blockSignals(true);
	this->filterSize->blockSignals(true);
	this->minArea->blockSignals(true);
	this->NDVIThreshold->blockSignals(true);
	this->NDVIDTMThreshold->blockSignals(true);
    this->PointThresholdFirstIt->blockSignals(true);
	this->HomogeneousThresholdFirstIt->blockSignals(true);
	this->useRoughnessStrength->blockSignals(true);
	this->useRoughnessIso->blockSignals(true);
	this->StructOpen->blockSignals(true);
	this->StructClose->blockSignals(true);
	this->Thin->blockSignals(true);
	this->ExistingDatabaseProb->blockSignals(true);
	this->MaxItPostProc->blockSignals(true);
	this->UseWaterModel->blockSignals(true);
	this->FirstLastThreshold->blockSignals(true);
	this->PointPercentage->blockSignals(true);
	this->AssignToNotT->blockSignals(true);
	this->AssignToTheta->blockSignals(true);

	QString dt=QString("%1").arg(this->MaxBuildingSize, 0, 'f', 2);
	this->maxExtent->setText(dt);

	dt=QString("%1").arg(this->MinBuildingHeight, 0, 'f', 2);
    this->minHeight->setText(dt);
	dt=QString("%1").arg(this->MinBuildingHeightDTM, 0, 'f', 2);
    this->minHeightDTM->setText(dt);

	dt=QString("%1").arg(this->MinBuildingSize, 0, 'f', 2);
    this->minExtent->setText(dt);
	
	dt=QString("%1").arg(this->MinBuildingArea, 0, 'f', 1);
    this->minArea->setText(dt);

	dt=QString("%1").arg(this->NDVIThresholdBuilding, 0, 'f', 1);
    this->NDVIThreshold->setText(dt);

	dt=QString("%1").arg(this->NDVIThresholdBuildingDTM, 0, 'f', 1);
    this->NDVIDTMThreshold->setText(dt);

	dt=QString("%1").arg(this->percentageTreesBuilding, 0, 'f', 1);
    this->percentageTrees->setText(dt);
	
	dt=QString("%1").arg((float) this->kernelsizeRoughness, 0, 'f', 0);
    this->filterSize->setText(dt);

	dt=QString("%1").arg((float) this->percentagePoints0Building, 0, 'f', 1);
    this->PointThresholdFirstIt->setText(dt);
	dt=QString("%1").arg((float) this->percentageHomog0Building, 0, 'f', 1);
    this->HomogeneousThresholdFirstIt->setText(dt);

	dt=QString("%1").arg(this->structureForOpen, 0, 'f', 2);
	this->StructOpen->setText(dt);
	dt=QString("%1").arg(this->structureForClose, 0, 'f', 2);
	this->StructClose->setText(dt);

	dt=QString("%1").arg(this->thinning, 0, 'f', 2);
	this->Thin->setText(dt);

	dt=QString("%1").arg(this->buildingPercentage, 0, 'f', 1);
	this->ExistingDatabaseProb->setText(dt);

	dt=QString("%1").arg(this->maxPostClassIterations, 0);
	this->MaxItPostProc->setText(dt);

	dt=QString("%1").arg(this->FirstLastThresholdValue, 0, 'f', 2);
	this->FirstLastThreshold->setText(dt);

	dt=QString("%1").arg(this->PointPercentageThresholdValue * 100.00, 0, 'f', 1);;
	this->PointPercentage->setText(dt);

	
	this->useRoughnessStrength->setChecked(this->useRoughnessStrengthBuilding);
	this->useRoughnessIso->setChecked(this->useRoughnessIsotropyBuilding);

	if (this->useFirstpulseForRoughness)
	{	
		roughnessLastPulseButton->setChecked(false);	
		roughnessFirstPulseButton->setChecked(true);	
	}
	else
	{
		roughnessLastPulseButton->setChecked(true);	
		roughnessFirstPulseButton->setChecked(false);	
	}	

	if (this->useWaterModel)
	{
		UseWaterModel->setChecked(true);	
	}
	else
	{
		UseWaterModel->setChecked(false);	
	}

	if (this->assignInverseFLToTheta) 
	{
		this->AssignToTheta->setChecked(true);
		this->AssignToNotT->setChecked(false);
	}
	else
	{
		this->AssignToTheta->setChecked(false);
		this->AssignToNotT->setChecked(true);
	}

	setDTMmorphMode();

	this->minArea->blockSignals(false); 
	this->roughnessLastPulseButton->blockSignals(false);
    this->roughnessFirstPulseButton->blockSignals(false);
	this->percentageTrees->blockSignals(false);
	this->filterSize->blockSignals(false);
	this->minExtent->blockSignals(false); 
	this->maxExtent->blockSignals(false);
    this->minHeight->blockSignals(false);
    this->minHeightDTM->blockSignals(false);
	this->NDVIDTMThreshold->blockSignals(false);
	this->NDVIThreshold->blockSignals(false);
    this->PointThresholdFirstIt->blockSignals(false);
	this->HomogeneousThresholdFirstIt->blockSignals(false);
	this->useRoughnessStrength->blockSignals(false);
	this->useRoughnessIso->blockSignals(false);
	this->StructOpen->blockSignals(false);
	this->StructClose->blockSignals(false);
	this->Thin->blockSignals(false);
	this->ExistingDatabaseProb->blockSignals(false);
	this->MaxItPostProc->blockSignals(false);
	this->UseWaterModel->blockSignals(false);
	this->FirstLastThreshold->blockSignals(false);
	this->PointPercentage->blockSignals(false);
	this->AssignToNotT->blockSignals(false);
	this->AssignToTheta->blockSignals(false);

	if (this->pOutPutHandler) delete this->pOutPutHandler;
	this->pOutPutHandler = new bui_OutputModeHandler(this->OutputLevelMin, this->OutputLevelMed,
							                         this->OutputLevelMax, 0);

	this->checkSetup();
}

//=============================================================================

void bui_BuildingDialog::explorePathSelected()
{
	QString dirQ = QFileDialog::getExistingDirectory (this, "Choose Output Directory",
		                                              this->outputDir.GetChar());

	CFileName direc = ((const char*)dirQ.toLatin1());
		
	if (!direc.IsEmpty())
	{
		this->outputDir = direc;
		if (this->outputDir[this->outputDir.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->outputDir += CSystemUtilities::dirDelimStr;

		this->outputdir->setText(this->outputDir.GetChar());
	}
};

//=============================================================================
  
void bui_BuildingDialog::setCurrentExtentsAndResolution()
{
	this->buildingGResolution->blockSignals(true);

	QString dt=QString("%1").arg(this->resolution.x, 0, 'f', 3);
	this->buildingGResolution->setText(dt);

	this->buildingGResolution->blockSignals(false);

	this->buildingLLY->blockSignals(true);
	this->buildingLLX->blockSignals(true);
	this->buildingURX->blockSignals(true);
	this->buildingURY->blockSignals(true);

	dt=QString("%1").arg(this->llPoint.x, 0, 'f', 3);
	this->buildingLLX->setText(dt);

	dt=QString("%1").arg(this->llPoint.y, 0, 'f', 3);
	this->buildingLLY->setText(dt);

	dt=QString("%1").arg(this->urPoint.x, 0, 'f', 3);
	this->buildingURX->setText(dt);

	dt=QString("%1").arg(this->urPoint.y, 0, 'f', 3);
	this->buildingURY->setText(dt);

	this->buildingLLY->blockSignals(false);
	this->buildingLLX->blockSignals(false);
	this->buildingURX->blockSignals(false);
	this->buildingURY->blockSignals(false);   
};

//=============================================================================

void bui_BuildingDialog::setDTMmorphMode()
{
	this->MorphologicFilter->blockSignals(true);
	this->RankFilter->blockSignals(true);
	this->RankPercentage->blockSignals(true);

	QString dt=QString("%1").arg(this->rankPercentage * 100.0, 0, 'f', 1);
	this->RankPercentage->setText(dt);

	if (this->useRank) 
	{
		this->MorphologicFilter->setChecked(false);
		this->RankFilter->setChecked(true);
		this->RankPercentage->setEnabled(true);
	}
	else
	{
		this->MorphologicFilter->setChecked(true);
		this->RankFilter->setChecked(false);
		this->RankPercentage->setEnabled(false);
	}

	this->MorphologicFilter->blockSignals(false);
	this->RankFilter->blockSignals(false);
	this->RankPercentage->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::setupDEMSelector(QComboBox *box, CVDEM *selectedDEM, const char *descriptor)
{
	box->blockSignals(true);

	box->clear();

	box->addItem(descriptor);

	int indexSelected = 0;

	for (int i = 0; i < project.getNumberOfDEMs(); ++i)
	{
		CVDEM *dem = project.getDEMAt(i);
		box->addItem(dem->getFileName()->GetChar());
		if (dem == selectedDEM) indexSelected = i + 1;
	}

	box->setCurrentIndex(indexSelected);
	
	if ((box == this->NDVISelector) && (indexSelected == 0))
	{
		this->SigmaNDVISelector->setDisabled(true);
	}

	box->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::setupImageSelector(QComboBox *box, CVImage *selectedImage, const char *descriptor)
{
	box->blockSignals(true);

	box->clear();

	box->addItem(descriptor);

	int indexSelected = 0;

	for (int i = 0; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		box->addItem(img->getFileNameChar());
		if (img == selectedImage) indexSelected = i + 1;
	}

	box->setCurrentIndex(indexSelected);
	
	if ((box == this->NDVISelector) && (indexSelected == 0))
	{
		this->SigmaNDVISelector->setDisabled(true);
	}

	box->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::setupExtents(CDEM *dem)
{
	if (dem)
	{
		this->resolution.x = dem->getgridX();
		this->resolution.y = dem->getgridY();
		this->llPoint.x    = dem->getminX();
		this->llPoint.y    = dem->getminY();			
		this->urPoint.x    = dem->getmaxX();			
		this->urPoint.y    = dem->getmaxY();

		this->setCurrentExtentsAndResolution();
  
		this->extentsInit = true;
	}
};
	
//=============================================================================
  
void bui_BuildingDialog::DTMModeChanged()
{
	this->useRank = !this->useRank;
	setDTMmorphMode();
};

//=============================================================================

void bui_BuildingDialog::DTMRankChanged()
{
	this->RankPercentage->blockSignals(true);
	
	bool isDouble;

	double hlp = this->RankPercentage->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0)&& (hlp <= 100.0)) this->rankPercentage = float(hlp / 100.0);
	
	QString dt=QString("%1").arg(this->rankPercentage * 100.0, 0, 'f', 1);
    this->RankPercentage->setText(dt);
	
	this->RankPercentage->blockSignals(false);   
};

//=============================================================================

void bui_BuildingDialog::setupExtents(CVImage *img)
{
	if (img)
	{
		if (img->hasTFW())
		{
			C3DPoint p1(0.0, img->getHugeImage()->getHeight() - 1.0, 0.0);
			C3DPoint p2(img->getHugeImage()->getWidth() - 1.0, 0.0, 0.0);
			C3DPoint P1,P2;
			img->getTFW()->transformObs2Obj(P1, p1);
			img->getTFW()->transformObs2Obj(P2, p2);
			this->resolution.x = fabs(img->getTFW()->getA());
			this->resolution.y = fabs(img->getTFW()->getE());
			this->llPoint.x    = P1.x;
			this->llPoint.y    = P1.y;			
			this->urPoint.x    = P2.x;			
			this->urPoint.y    = P2.y;

			this->setCurrentExtentsAndResolution();
	  
			this->extentsInit = true;
		}
	}
};

//=============================================================================

void bui_BuildingDialog::Cancel()
{
	this->close();
}

//=============================================================================

void bui_BuildingDialog::DSMLastSelected()
{
	int indexSelected = this->DSMLastSelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfDEMs()))
	{
		this->pDSMLast = 0;
		setupDEMSelector(DSMLastSelector, pDSMLast, this->noDSMLastTxt);
	}
	else
	{
		this->pDSMLast = project.getDEMAt(indexSelected - 1);

		if (!extentsInit) setupExtents(pDSMLast->getDEM());

		if (this->pDSMFirst == this->pDSMLast) 
		{
			this->pDSMFirst = 0;
			setupDEMSelector(DSMFirstSelector, this->pDSMFirst, this->noDSMFirstTxt);
		}

		if (this->pDTM == this->pDSMLast)  
		{
			this->pDTM = 0;
			setupDEMSelector(DTMSelector, this->pDTM, this->noDTMTxt);
		}
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingDialog::DSMFirstSelected()
{
	int indexSelected = this->DSMFirstSelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfDEMs()))
	{
		this->pDSMFirst = 0;
		setupDEMSelector(DSMFirstSelector, pDSMFirst, this->noDSMFirstTxt);
	}
	else
	{
		this->pDSMFirst = project.getDEMAt(indexSelected - 1);

		if (!extentsInit) setupExtents(pDSMFirst->getDEM());

		if (this->pDSMLast  == this->pDSMFirst) 
		{
			this->pDSMLast = 0;
			setupDEMSelector(DSMLastSelector,  this->pDSMLast,  this->noDSMLastTxt);
		}

		if (this->pDTM == this->pDSMFirst)  
		{
			this->pDTM = 0;
			setupDEMSelector(DTMSelector, this->pDTM, this->noDTMTxt);
		}
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingDialog::DTMSelected()
{
	int indexSelected = this->DTMSelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfDEMs()))
	{
		this->pDTM = 0;
		setupDEMSelector(DTMSelector, this->pDTM, this->noDTMTxt);
	}
	else
	{
		this->pDTM = project.getDEMAt(indexSelected - 1);

		if (!extentsInit) setupExtents(pDTM->getDEM());

		if (this->pDSMLast == this->pDTM) 
		{
			this->pDSMLast = 0;
			setupDEMSelector(DSMLastSelector,  this->pDSMLast,  this->noDSMLastTxt);
		}

		if (this->pDSMFirst == this->pDTM)  
		{
			this->pDSMFirst = 0;
			setupDEMSelector(DSMFirstSelector, pDSMFirst, this->noDSMFirstTxt);
		}
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingDialog::NDVISelected()
{
	int indexSelected = this->NDVISelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfImages()))
	{
		this->pNDVI = 0;
		setupImageSelector(NDVISelector,  this->pNDVI,  this->noNDVITxt);
	}
	else
	{
		this->pNDVI = project.getImageAt(indexSelected - 1);
		this->SigmaNDVISelector->setDisabled(false);

		if (!extentsInit) setupExtents(pNDVI);

		if (this->pSigmaNDVI == this->pNDVI) 
		{
			this->pSigmaNDVI = 0;
			setupImageSelector(SigmaNDVISelector,  this->pSigmaNDVI,  this->noSigmaNDVITxt);
		}
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingDialog::NDVISigmaSelected()
{
	int indexSelected = this->SigmaNDVISelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfImages()))
	{
		this->pSigmaNDVI = 0;
		setupImageSelector(SigmaNDVISelector,  this->pSigmaNDVI,  this->noSigmaNDVITxt);
	}
	else
	{
		this->pSigmaNDVI = project.getImageAt(indexSelected - 1);

		
		if (this->pNDVI == this->pSigmaNDVI) 
		{
			this->pNDVI = 0;
			setupImageSelector(NDVISelector,  this->pNDVI,  this->noNDVITxt);
		}
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingDialog::ExistingDatabaseSelected()
{
	int indexSelected = this->ExistingDataBaseSelector->currentIndex();

	if ((indexSelected == 0) || (indexSelected > project.getNumberOfImages()))
	{
		this->pBuildingLabels = 0;
		setupImageSelector(ExistingDataBaseSelector, this->pBuildingLabels, this->ExistingDatabaseTxt);
	}
	else
	{
		this->pBuildingLabels = project.getImageAt(indexSelected - 1);
	}
	
	this->checkSetup();
}

//=============================================================================

void bui_BuildingDialog::maxExtentsSelected()
{    
	CDEM *dem = 0;
	if (this->pDSMLast)          dem = this->pDSMLast->getDEM();
	if (!dem && this->pDSMFirst) dem = this->pDSMFirst->getDEM();;
	if (dem) this->setupExtents(dem);
}

//=============================================================================

void bui_BuildingDialog::LLChanged()
{
	bool isDouble;

	double hlp = buildingLLX->text().toDouble(&isDouble);
	if (isDouble && fabs(llPoint.x - hlp) > FLT_EPSILON)
	{
		llPoint.x = hlp;
		if (urPoint.x <= llPoint.x) urPoint.x = llPoint.x + resolution.x;
		urPoint.x = llPoint.x + (floor((urPoint.x - llPoint.x) / resolution.x)) * resolution.x;
	}
 
	hlp = buildingLLY->text().toDouble(&isDouble);
	if (isDouble && fabs(llPoint.y - hlp) > FLT_EPSILON) 
	{
		llPoint.y = hlp;
		if (urPoint.y <= llPoint.y) urPoint.y = llPoint.y + resolution.y;
		urPoint.y = llPoint.y + (floor((urPoint.y - llPoint.y) / resolution.y)) * resolution.y;
	}
	
	this->setCurrentExtentsAndResolution();
};

//=============================================================================
	  
void bui_BuildingDialog::RUChanged()
{
	bool isDouble;

	double hlp = this->buildingURX->text().toDouble(&isDouble);
	if (isDouble && fabs(urPoint.x - hlp) > FLT_EPSILON) 
	{
		urPoint.x = hlp;
		if (urPoint.x <= llPoint.x) llPoint.x = urPoint.x - resolution.x;
		llPoint.x = urPoint.x - (floor((urPoint.x - llPoint.x) / resolution.x)) * resolution.x;
	}

	hlp = buildingURY->text().toDouble(&isDouble);
	if (isDouble && fabs(urPoint.y - hlp) > FLT_EPSILON)
	{
		urPoint.y = hlp;
		if (urPoint.y <= llPoint.y) llPoint.y = urPoint.y - resolution.y;
		llPoint.y = urPoint.y - (floor((urPoint.y - llPoint.y) / resolution.y)) * resolution.y;
	}
	
	this->setCurrentExtentsAndResolution();
};

//=============================================================================

void bui_BuildingDialog::resolutionChanged()
{
	bool isDouble;

	double hlp = this->buildingGResolution->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0) && (fabs(this->resolution.x - hlp) > FLT_EPSILON))
	{
		this->resolution.x = hlp;
		this->resolution.y = hlp;
		urPoint.x = llPoint.x + (floor((urPoint.x - llPoint.x) / resolution.x)) * resolution.x;
		urPoint.y = llPoint.y + (floor((urPoint.y - llPoint.y) / resolution.y)) * resolution.y;
	}
  
	this->setCurrentExtentsAndResolution();
};

//=============================================================================

void bui_BuildingDialog::minHeightChanged()
{
	this->minHeight->blockSignals(true);

	bool isDouble;

	double hlp = this->minHeight->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)) this->MinBuildingHeight = hlp;
	
	QString dt=QString("%1").arg(this->MinBuildingHeight, 0, 'f', 2);
    this->minHeight->setText(dt);
	
	this->minHeight->blockSignals(false);   
};

//=============================================================================

void bui_BuildingDialog::minHeightDTMChanged()
{
	this->minHeightDTM->blockSignals(true);

	bool isDouble;

	double hlp = this->minHeightDTM->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)) this->MinBuildingHeightDTM = hlp;
	
	QString dt=QString("%1").arg(this->MinBuildingHeightDTM, 0, 'f', 2);
    this->minHeightDTM->setText(dt);
	
	this->minHeightDTM->blockSignals(false);   
};
  
//=============================================================================

void bui_BuildingDialog::FLThetaModeChanged()
{
	AssignToTheta->blockSignals(true);
    AssignToNotT->blockSignals(true);
	
	if (this->assignInverseFLToTheta)
	{	
		AssignToNotT->setChecked(true);	
		AssignToTheta->setChecked(false);	
		this->assignInverseFLToTheta = false;
	}
	else
	{
		AssignToNotT->setChecked(false);	
		AssignToTheta->setChecked(true);	
		this->assignInverseFLToTheta = true;
	}

	AssignToTheta->blockSignals(false);
    AssignToNotT->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::FirstLastThresholdChanged()
{
	this->FirstLastThreshold->blockSignals(true);

	bool isDouble;

	double hlp = this->FirstLastThreshold->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)) this->FirstLastThresholdValue = hlp;
	
	QString dt=QString("%1").arg(this->FirstLastThresholdValue, 0, 'f', 2);
    this->FirstLastThreshold->setText(dt);
	
	this->FirstLastThreshold->blockSignals(false);   
};

//=============================================================================

void bui_BuildingDialog::PointPercentageChanged()
{
	this->PointPercentage->blockSignals(true);

	bool isDouble;

	double hlp = this->PointPercentage->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0) && hlp <= 100.0) this->PointPercentageThresholdValue = hlp * 0.01;
	
	QString dt=QString("%1").arg(this->PointPercentageThresholdValue * 100.0, 0, 'f', 1);
    this->PointPercentage->setText(dt);
	
	this->PointPercentage->blockSignals(false);   
};

//=============================================================================

void bui_BuildingDialog::maxExtentChanged()
{
	this->maxExtent->blockSignals(true);
	
	bool isDouble;

	double hlp = this->maxExtent->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)) this->MaxBuildingSize = hlp;
	
	QString dt=QString("%1").arg(this->MaxBuildingSize, 0, 'f', 2);
    this->maxExtent->setText(dt);
	
	this->maxExtent->blockSignals(false);   

	if (this->MinBuildingSize > this->MaxBuildingSize)
	{
		this->minExtent->blockSignals(true);

		this->MinBuildingSize = this->MaxBuildingSize;
		dt=QString("%1").arg(this->MinBuildingSize, 0, 'f', 2);
		this->minExtent->setText(dt);
		this->minExtent->blockSignals(false);
	}
};

//=============================================================================

void bui_BuildingDialog::minExtentChanged()
{
	this->minExtent->blockSignals(true);
	
	bool isDouble;

	double hlp = this->minExtent->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)) this->MinBuildingSize = hlp;
	
	QString dt=QString("%1").arg(this->MinBuildingSize, 0, 'f', 2);
    this->minExtent->setText(dt);
	
	this->minExtent->blockSignals(false); 

	if (this->MinBuildingSize > this->MaxBuildingSize)
	{
		this->maxExtent->blockSignals(true);

		this->MaxBuildingSize = this->MinBuildingSize;
		dt=QString("%1").arg(this->MaxBuildingSize, 0, 'f', 2);
		this->maxExtent->setText(dt);
		this->maxExtent->blockSignals(false);
	}
}

//=============================================================================

void bui_BuildingDialog::minAreaChanged()
{
	this->minArea->blockSignals(true);
	
	bool isDouble;

	double hlp = this->minArea->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)) this->MinBuildingArea = hlp;
	
	QString dt=QString("%1").arg(this->MinBuildingArea, 0, 'f', 1);
    this->minArea->setText(dt);
	
	this->minArea->blockSignals(false); 
};

//=============================================================================

void bui_BuildingDialog::NDVIThresholdChanged()
{
	this->NDVIThreshold->blockSignals(true);
	
	bool isDouble;

	double hlp = this->NDVIThreshold->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= -100.0) && (hlp <= 100.0)) this->NDVIThresholdBuilding = hlp;
	
	QString dt=QString("%1").arg(this->NDVIThresholdBuilding, 0, 'f', 1);
    this->NDVIThreshold->setText(dt);
	
	this->NDVIThreshold->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::NDVIThresholdDTMChanged()
{
	this->NDVIDTMThreshold->blockSignals(true);
	
	bool isDouble;

	double hlp = this->NDVIDTMThreshold->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= -100.0) && (hlp <= 100.0)) this->NDVIThresholdBuildingDTM = hlp;
	
	QString dt=QString("%1").arg(this->NDVIThresholdBuildingDTM, 0, 'f', 1);
    this->NDVIDTMThreshold->setText(dt);
	
	this->NDVIDTMThreshold->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::structureForOpenChanged()
{
	this->StructOpen->blockSignals(true);
	
	bool isDouble;

	double hlp = this->StructOpen->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0) && (hlp <= 100.0)) this->structureForOpen = hlp;
	
	QString dt=QString("%1").arg(this->structureForOpen, 0, 'f', 2);
    this->StructOpen->setText(dt);
	
	this->StructOpen->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::structureForCloseChanged()
{
	this->StructClose->blockSignals(true);
	
	bool isDouble;

	double hlp = this->StructClose->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0) && (hlp <= 100.0)) this->structureForClose = hlp;
	
	QString dt=QString("%1").arg(this->structureForClose, 0, 'f', 2);
    this->StructClose->setText(dt);
	
	this->StructClose->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::ThinningChanged()
{
	this->Thin->blockSignals(true);
	
	bool isDouble;

	double hlp = this->Thin->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0) && (hlp <= 100.0)) this->thinning = hlp;
	
	QString dt=QString("%1").arg(this->thinning, 0, 'f', 2);
    this->Thin->setText(dt);
	
	this->Thin->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::BuildingPercentageChanged()
{
	this->ExistingDatabaseProb->blockSignals(true);
	
	bool isDouble;

	double hlp = this->ExistingDatabaseProb->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0) && (hlp <= 100.0)) this->buildingPercentage = hlp;
	
	QString dt=QString("%1").arg(this->buildingPercentage, 0, 'f', 1);
    this->ExistingDatabaseProb->setText(dt);
	
	this->ExistingDatabaseProb->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::MaxIterationChanged()
{
	this->MaxItPostProc->blockSignals(true);
	
	bool isDouble;

	double hlp = this->MaxItPostProc->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0) && (hlp <= 100.0)) this->maxPostClassIterations = floor(hlp);
	
	QString dt=QString("%1").arg(this->maxPostClassIterations, 0);
    this->MaxItPostProc->setText(dt);
	
	this->MaxItPostProc->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::useRoughnessStrengthChanged()
{
	this->useRoughnessStrengthBuilding = useRoughnessStrength->isChecked();
};

//=============================================================================

void bui_BuildingDialog::useRoughnessIsoChanged()
{
	this->useRoughnessIsotropyBuilding = useRoughnessIso->isChecked();
};

//=============================================================================

void bui_BuildingDialog::UseWaterModelChanged()
{
	this->useWaterModel = UseWaterModel->isChecked();
};

//=============================================================================

void bui_BuildingDialog::roughnessModeChanged()
{
	roughnessLastPulseButton->blockSignals(true);
    roughnessFirstPulseButton->blockSignals(true);
	
	if (this->useFirstpulseForRoughness)
	{	
		roughnessLastPulseButton->setChecked(true);	
		roughnessFirstPulseButton->setChecked(false);	
		this->useFirstpulseForRoughness = false;
	}
	else
	{
		roughnessLastPulseButton->setChecked(false);	
		roughnessFirstPulseButton->setChecked(true);	
		this->useFirstpulseForRoughness = true;
	}

	roughnessLastPulseButton->blockSignals(false);
    roughnessFirstPulseButton->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::roughnessParametersChanged()
{
	this->percentageTrees->blockSignals(true);
	this->filterSize->blockSignals(true);
	
	bool isDouble;

	double hlp = this->percentageTrees->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0) && (hlp <= 100.0)) this->percentageTreesBuilding = hlp;
	
	QString dt=QString("%1").arg(this->percentageTreesBuilding, 0, 'f', 1);
    this->percentageTrees->setText(dt);
	
	hlp = this->filterSize->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 3.0)) this->kernelsizeRoughness = hlp;
	
	if (kernelsizeRoughness < 3) kernelsizeRoughness = 3;
	else if (!(kernelsizeRoughness % 2)) kernelsizeRoughness++;

	dt=QString("%1").arg((float) this->kernelsizeRoughness, 0, 'f', 0);
    this->filterSize->setText(dt);

	this->percentageTrees->blockSignals(false);
	this->filterSize->blockSignals(false);
};

//=============================================================================

void bui_BuildingDialog::setCurrentDir(const CCharString &cdir)
{
	this->currentDir = cdir;
}

//=============================================================================

void bui_BuildingDialog::checkSetup()
{
	if (!this->pNDVI)
	{
		this->NDVIDTMThreshold->setEnabled(false);
		this->NDVIThreshold->setEnabled(false);
		this->UseWaterModel->setEnabled(false);
	}
	else
	{
		this->NDVIDTMThreshold->setEnabled(true);
		this->NDVIThreshold->setEnabled(true);
		this->UseWaterModel->setEnabled(true);
	}

	if (this->pDSMLast || this->pDSMFirst)
	{
		this->buttonOk->setEnabled(true);
		this->SurfaceRoughnessTab->setEnabled(true);
		this->DempsterShaferGroup->setEnabled(true);
		if (!this->pDSMLast || !this->pDSMFirst)
			this->RoughnessFirstLastGroup->setEnabled(false);
		else
			this->RoughnessFirstLastGroup->setEnabled(true);

		if (this->pDSMLast && this->pDSMFirst)
		{
			this->FirstLastGroupBox->setEnabled(true);
			this->FirstLastThreshold->setEnabled(true);
		}
		else
		{
			this->FirstLastGroupBox->setEnabled(false);
			this->FirstLastThreshold->setEnabled(false);
		}

		if (!this->pDTM)
		{
			this->DTMParameters->setEnabled(true);
			if (this->MaxBuildingSize > this->MinBuildingSize)
			{
				if (this->pNDVI) this->NDVIDTMThreshold->setEnabled(true);
				this->minHeightDTM->setEnabled(true);
				this->SurfaceRoughnessGroup->setEnabled(true);
			}
			else
			{
				this->NDVIDTMThreshold->setEnabled(false);
				this->minHeightDTM->setEnabled(false);
				this->SurfaceRoughnessGroup->setEnabled(false);
			}
		}
		else 
		{
			this->DTMParameters->setEnabled(false);
		}
	}
	else
	{
		this->RoughnessFirstLastGroup->setEnabled(false);
		this->buttonOk->setEnabled(false);
		this->SurfaceRoughnessTab->setEnabled(false);
		this->DempsterShaferGroup->setEnabled(false);
	}
}

//=============================================================================

void bui_BuildingDialog::OutputLevelChanged()
{
	this->pOutPutHandler->reactOnButtonSelected();
}
	
//=============================================================================
  
void bui_BuildingDialog::DTMParametersChanged()
{
    this->PointThresholdFirstIt->blockSignals(true);
	this->HomogeneousThresholdFirstIt->blockSignals(true);

	bool isDouble;

	double hlp = this->PointThresholdFirstIt->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0) && (hlp <= 100.0)) this->percentagePoints0Building = hlp;
	
	QString dt=QString("%1").arg((float) this->percentagePoints0Building, 0, 'f', 1);
    this->PointThresholdFirstIt->setText(dt);

	hlp = this->HomogeneousThresholdFirstIt->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0) && (hlp <= 100.0)) this->percentageHomog0Building = hlp;
	dt=QString("%1").arg((float) this->percentageHomog0Building, 0, 'f', 1);
    this->HomogeneousThresholdFirstIt->setText(dt);

    this->PointThresholdFirstIt->blockSignals(false);
	this->HomogeneousThresholdFirstIt->blockSignals(false);
};

//=============================================================================
		  
void bui_BuildingDialog::writeDSRecord (ofstream &DSFile, const char *key, 
										float Pl, float Pu, float xl, float xu, 
										int DSWidth)
{
	DSFile << key << "(";
	DSFile.width(DSWidth);
	DSFile << Pl << " ";
	DSFile.width(DSWidth);
	DSFile << Pu << " ";
	DSFile.width(DSWidth);
	DSFile << xl << " ";
	DSFile.width(DSWidth);
	DSFile << xu << ")\n";
};
  
//=============================================================================

bool bui_BuildingDialog::writeDSFile(const CCharString &filename) const
{
	ofstream DempShafFile(filename.GetChar());
	if (!DempShafFile.good()) return false;

	DempShafFile.setf(ios::fixed, ios::floatfield);
	DempShafFile.precision(2);
	float Pl = 0.05f, Pu = 0.95f;
	float xu, xl, intervalWidth;

	int DSWidth = 7;

    DempShafFile << "DEMPSTER_SHAFER\n";

	intervalWidth = 1.5f;
	if (this->MinBuildingHeight < intervalWidth) intervalWidth = this->MinBuildingHeight;
	
	xl = this->MinBuildingHeight - intervalWidth;
	xu = this->MinBuildingHeight + intervalWidth;
	writeDSRecord(DempShafFile, "DZ_PIXEL            ", Pl, Pu, xl, xu, DSWidth);

	Pl = 0.05f; 
	Pu = 0.95f;
	xl = 1.0 - 2.0 * this->percentageTreesBuilding / 100.0;
	xu = 1.0;
	writeDSRecord(DempShafFile, "HOMOGENEITY_PIXEL   ", Pl, Pu, xl, xu, DSWidth);

	Pl = 0.05f; 
	Pu = 0.8f;
	xl = 0.0f;
	xu = 1.0f;
	writeDSRecord(DempShafFile, "ISOTROPY_PIXEL      ", Pl, Pu, xl, xu, DSWidth);

	Pl = 0.05f; 
	Pu = 0.95f;
	xl = this->FirstLastThresholdValue - intervalWidth;
	xu = this->FirstLastThresholdValue + intervalWidth;
	writeDSRecord(DempShafFile, "FIRSTLAST_PIXEL     ", Pl, Pu, xl, xu, DSWidth);

	intervalWidth = 30.0;

	Pl = 0.10f; 
	Pu = 0.90f;
	xl = this->NDVIThresholdBuilding - intervalWidth;
	xu = this->NDVIThresholdBuilding + intervalWidth;
	writeDSRecord(DempShafFile, "NDVI_PIXEL          ", Pl, Pu, xl, xu, DSWidth);

	intervalWidth = 1.5f;
	if (this->MinBuildingHeight < intervalWidth) intervalWidth = this->MinBuildingHeight;
	Pl = 0.05f; 
	Pu = 0.95f;
	xl = this->MinBuildingHeight - intervalWidth;
	xu = this->MinBuildingHeight + intervalWidth;
	writeDSRecord(DempShafFile, "DZ_REGION           ", Pl, Pu, xl, xu, DSWidth);

	Pl = 0.00f; 
	Pu = 0.00f;
	xl = 0;
	xu = 0.6f;
	writeDSRecord(DempShafFile, "HOMOGENEOUS_REGION  ", Pl, Pu, xl, xu, DSWidth);

	Pl = 0.05f; 
	Pu = 0.95f;

	intervalWidth = 0.25;
	
	xl = PointPercentageThresholdValue - intervalWidth;
	xu = PointPercentageThresholdValue + intervalWidth;
	writeDSRecord(DempShafFile, "POINT_REGION        ", Pl, Pu, xl, xu, DSWidth);

	intervalWidth = 15.0;
	Pl = 0.10f; 
	Pu = 0.90f;
	xl = this->NDVIThresholdBuilding - intervalWidth;
	xu = this->NDVIThresholdBuilding + intervalWidth;	
	writeDSRecord(DempShafFile, "NDVI_REGION         ", Pl, Pu, xl, xu, DSWidth);

	return true;
};

//=============================================================================

int bui_BuildingDialog::getSensorOption() const
{
	int sensorOption = 0;

	if (this->pDSMLast || this->pDSMFirst) sensorOption +=  CDempsterShaferBuildingClassifier::UseDz;

	if (this->useRoughnessStrengthBuilding) 
	{
		sensorOption += CDempsterShaferBuildingClassifier::UseRoughStr;
	}

	if (this->useRoughnessIsotropyBuilding) 
	{
		sensorOption += CDempsterShaferBuildingClassifier::UseRoughIso;
	}


	return sensorOption;
};

//=============================================================================
	  
int bui_BuildingDialog::getStruct(float size)
{
	size /= this->resolution.x;

	int structSize = floor(size);

	return structSize;
};

//=============================================================================

void bui_BuildingDialog::OK()
{
	if (this->pDSMLast || this->pDSMFirst)
	{
		if (this->detector) delete this->detector;

		CDEM *dsmlast = 0, *dsmfirst = 0, *dtm  = 0;
		if (this->pDSMLast)  dsmlast = this->pDSMLast->getDEM();
		if (this->pDSMFirst) dsmfirst = this->pDSMFirst->getDEM();
		if (this->pDTM)      dtm = this->pDTM->getDEM();
		CHugeImage *ndvi = 0, *sigmandvi = 0, *existing = 0;
		const CTFW *pTfwNDVI = 0;
		const CTFW *pTfwExist = 0;

		if (this->pNDVI)     
		{
			ndvi     = this->pNDVI->getHugeImage();
			pTfwNDVI = this->pNDVI->getTFW();
		}
		if (this->pSigmaNDVI) sigmandvi = this->pSigmaNDVI->getHugeImage();
		if (this->pBuildingLabels) 
		{
			existing  = this->pBuildingLabels->getHugeImage();
			pTfwExist = this->pBuildingLabels->getTFW();
		}

		this->detector = new CBuildingDetector(this->MaxBuildingSize, dsmlast, dsmfirst, dtm,  
								               ndvi, sigmandvi, pTfwNDVI,
											   existing, pTfwExist,
											   this->MinBuildingHeightDTM,
											   this->resolution, 
											   this->llPoint, this->urPoint,
											   this->outputDir);
		if (this->useRank)
		{
			this->detector->setDTMmethodToRank(this->rankPercentage);
		}
		else
		{
			this->detector->setDTMmethodToMorpho();
		}

		bui_ProgressDlg dlg(new CBuildingDetectionThread(this),this->detector,"Building Detection ...");
		dlg.exec();
		this->computationOK = dlg.getSuccess();
		baristaProjectHelper.refreshTree();
	
	}

	this->close();
}

//=============================================================================

bool bui_BuildingDialog::extract()
{
	bool ret = true;
	if (!this->detector) ret = false;
	else
	{
		CFileName probabilityModelFile = this->outputDir + "DempsterShafer.txt";
		this->writeDSFile(probabilityModelFile);

		this->writeParameters(this->currentDir + CSystemUtilities::dirDelimStr + "Buildings.ini");

		protHandler.open(CProtocolHandler::prt, this->outputDir + "Buildings.prt", false);
		
		ostrstream oss; oss.setf(ios::fixed, ios::floatfield); oss.precision(2);
		oss << "\nCommencing building detection\n=============================\n\n   Time: " 
			<< CSystemUtilities::timeString().GetChar() << "\n";

		protHandler.print(oss, CProtocolHandler::prt);

		this->detector->initWindows();

		if (!detector->isInitialised())
		{
			QMessageBox::warning( this, " Cannot detect buildings!", " Error initialising data!");
			ret = false;

		}
		else
		{
			bool useNDVI = this->pNDVI != 0;
			bool firstForRough = this->useFirstpulseForRoughness && (this->pDSMFirst != 0);
			float minArea0 = this->MaxBuildingSize * this->MaxBuildingSize * 0.25f;

			float pointThr = 0.5f;
			float homThr = 0.2f;
			float sigmaNDVIMultiplicator = 2.0f;
			eDECISIONRULE decisionRule = eMAXSUP;
			bool classifyRegions = true;
			int  minimumFilter = 0;
			bool createVegetationLayer = false;
			
			float minRoughnessQuantil = 1.0 - this->percentageTreesBuilding / 100.0;

			int  sensorOption = getSensorOption();

			float probExisting = this->buildingPercentage / 100.0;

			detector->detectBuildings(firstForRough, this->kernelsizeRoughness,  
									 this->percentageTreesBuilding / 100.0, this->Qmin, 
									 this->MinBuildingArea, getStruct(structureForOpen),
									 pointThr,  homThr, 
									 this->NDVIThresholdBuildingDTM,  useNDVI,
									 minArea0, this->percentagePoints0Building / 100.0, 
									 this->percentageHomog0Building / 100.0, 
									 this->MinBuildingSize, sensorOption, 
									 probabilityModelFile, minRoughnessQuantil, sigmaNDVIMultiplicator,
									 decisionRule, maxPostClassIterations, classifyRegions, 
									 getStruct(structureForClose), minimumFilter, createVegetationLayer, 
									 this->assignInverseFLToTheta, probExisting, useWaterModel);

			CVImage *img = project.addImage(this->outputDir + "labels.tif", false);
			CCharString str = img->getName() + ".hug";
			img->getHugeImage()->setFileName(str.GetChar());

			CTFW tfw(*img->getTFW());
			if (!detector->makePersistent(img->getHugeImage(), tfw, this->outputDir + "buildings.dat")) 
				project.deleteImage(img);
			else
			{
				img->setTFW(tfw);
			}

			CVDEM *newNormDSM = project.addDEM(this->outputDir + "DSM_Norm.tif");
				
			str = *(newNormDSM->getFileName()) + ".hug";
			newNormDSM->getDEM()->setFileName(str.GetChar());

			if (!detector->dumpNormDSM(str, newNormDSM->getDEM()))
				project.deleteDEM(newNormDSM);


			if (!this->pDTM) 
			{
				CVDEM *newDTM = project.addDEM(this->outputDir + "ApproxDTM.tif");
				str = *(newDTM->getFileName()) + ".hug";
				newDTM->getDEM()->setFileName(str.GetChar());

				if (!detector->dumpDTM(str, newDTM->getDEM()))
					project.deleteDEM(newDTM);
			}
			detector->exportBuildingLabelImage(this->outputDir + "labels.tif");
			detector->getBuildings(this->thinning, *project.getBuildings());
		}
	
	}

	return ret;
};

//=============================================================================

void bui_BuildingDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/BuildingDetector.html");
};

//=============================================================================

void bui_BuildingDialog::writeParameters(const CCharString &filename) const
{
	ofstream outFile(filename.GetChar());
	if (outFile.good())
	{
		outFile.setf(ios::fixed, ios::floatfield);
		outFile.precision(3);

		outFile << "DSMLast(";
		if (pDSMLast) outFile << pDSMLast->getFileName()->GetChar();
		else outFile << "None";
		
		outFile << ")\nDSMFirst(";
		if (pDSMFirst) outFile << pDSMFirst->getFileName()->GetChar();
		else outFile << "None";
		outFile << ")\nDTM(";
		if (pDTM) outFile << pDTM->getFileName()->GetChar();
		else outFile << "None";
		outFile << ")\nNDVI(";
		if (pNDVI) outFile << pNDVI->getFileNameChar();
		else outFile << "None";
		outFile << ")\nSigNDVI(";
		if (pSigmaNDVI) outFile << pSigmaNDVI->getFileNameChar();
		else outFile << "None";
		outFile << ")\nDataBase(";
		if (pBuildingLabels) outFile << pBuildingLabels->getFileNameChar();
		else outFile << "None";
		outFile << ")\nMinBuildingHeight("             << MinBuildingHeight
			    << ")\nMinBuildingHeightDTM("          << MinBuildingHeightDTM
			    << ")\nMaxBuildingSize("               << MaxBuildingSize
			    << ")\nMinBuildingSize("               << MinBuildingSize
			    << ")\nMinBuildingArea("               << MinBuildingArea
			    << ")\nNDVIThresholdBuilding("         << NDVIThresholdBuilding
			    << ")\nNDVIThresholdBuildingDTM("      << NDVIThresholdBuildingDTM
			    << ")\nuseFirstpulseForRoughness("     << useFirstpulseForRoughness
			    << ")\nuseRoughnessStrenghBuilding("   << useRoughnessStrengthBuilding
			    << ")\nuseRoughnessIsoBuilding("       << useRoughnessIsotropyBuilding
			    << ")\npercentageTreesBuilding("       << percentageTreesBuilding
			    << ")\nQmin("                          << Qmin
			    << ")\nkernelsizeRoughness("           << kernelsizeRoughness
				<< ")\nLLC("                           << llPoint.x    << " " << llPoint.y
				<< ")\nRUC("                           << urPoint.x    << " " << urPoint.y
				<< ")\nResolution("                    << resolution.x << " " << resolution.y
				<< ")\nHomog0("                        << percentageHomog0Building 
				<< ")\nPoint0("                        << percentagePoints0Building 
				<< ")\nOpen("                          << structureForOpen 
				<< ")\nClose("                         << structureForClose 
				<< ")\nThinning("                      << thinning 
				<< ")\nPBuild("                        << buildingPercentage 
				<< ")\nMaxItPost("                     << maxPostClassIterations 
				<< ")\nOutputLevel("                   << outputMode 
				<< ")\nUseWater("                      << useWaterModel 
				<< ")\nUseRank("                       << useRank 
				<< ")\nRankPercentage("                << rankPercentage
				<< ")\nFirstLastThresholdValue("       << FirstLastThresholdValue
				<< ")\nAssignInverseFLToTheta("        << assignInverseFLToTheta
				<< ")\nPointPercentageThresholdValue(" << PointPercentageThresholdValue
				<< ")\nOutputDir("                     << outputDir.GetChar()
				<< ")\n";
	}
};

//=============================================================================

void bui_BuildingDialog::initParameters(const CCharString &filename)
{
	ifstream inFile(filename.GetChar());
	int len = 2048;
	char *z = new char[len];
	if (inFile.good())
	{
		inFile.getline(z, len);

		while (inFile.good())
		{
			CCharString arg(z);
			int pos1 = arg.Find("(");
			int pos2 = arg.Find(")");
			if ((pos1 > 0) && (pos2 > pos1))
			{
				CCharString key = arg.Left(pos1);
				arg = arg.Left(pos2);
				arg = arg.Right(arg.GetLength() - pos1 - 1);
				if (key == "DSMLast")
				{
					this->pDSMLast = project.getDEMByName(arg);
				}
				else if (key == "DSMFirst")
				{
					this->pDSMFirst = project.getDEMByName(arg);
				}
				else if (key == "DTM")
				{
					this->pDTM = project.getDEMByName(arg);
				}
				else if (key == "NDVI")
				{
					this->pNDVI = project.getImageByName(&arg);
				}
				else if (key == "SigNDVI")
				{
					this->pSigmaNDVI = project.getImageByName(&arg);
				}
				else if (key == "DataBase") 
				{
					this->pBuildingLabels  = project.getImageByName(&arg);
				}
				else if (key == "MinBuildingHeight")
				{
					istrstream iss(arg.GetChar());
					iss >> MinBuildingHeight;
				}
				else if (key == "MinBuildingHeightDTM")
				{
					istrstream iss(arg.GetChar());
					iss >> MinBuildingHeightDTM;
				}
				else if (key == "MaxBuildingSize")
				{
					istrstream iss(arg.GetChar());
					iss >> MaxBuildingSize;
				}
				else if (key == "MinBuildingSize")
				{
					istrstream iss(arg.GetChar());
					iss >> MinBuildingSize;
				}
				else if (key == "MinBuildingArea")
				{
					istrstream iss(arg.GetChar());
					iss >> MinBuildingArea;
				}
				else if (key == "NDVIThresholdBuilding")
				{
					istrstream iss(arg.GetChar());
					iss >> NDVIThresholdBuilding;
				}
				else if (key == "NDVIThresholdBuildingDTM")
				{
					istrstream iss(arg.GetChar());
					iss >> NDVIThresholdBuildingDTM;
				}
				else if (key == "useFirstpulseForRoughness")
				{
					istrstream iss(arg.GetChar());
					iss >> useFirstpulseForRoughness;
				}
				else if (key == "useRoughnessStrenghBuilding")
				{
					istrstream iss(arg.GetChar());
					iss >> useRoughnessStrengthBuilding;
				}
				else if (key == "useRoughnessIsoBuilding")
				{
					istrstream iss(arg.GetChar());
					iss >> useRoughnessIsotropyBuilding;
				}
				else if (key == "Qmin")
				{
					istrstream iss(arg.GetChar());
					iss >> Qmin;
				}
				else if (key == "kernelsizeRoughness")
				{
					istrstream iss(arg.GetChar());
					iss >> kernelsizeRoughness;
				}
				else if (key == "LLC")
				{
					istrstream iss(arg.GetChar());
					iss >> llPoint.x   >> llPoint.y;
					extentsInit = true;
				}
				else if (key == "RUC")
				{
					istrstream iss(arg.GetChar());
					iss >> urPoint.x >> urPoint.y;
					extentsInit = true;
				}
				else if (key == "Resolution")
				{
					istrstream iss(arg.GetChar());
					iss >> resolution.x >> resolution.y;
				}
				else if (key == "percentageTreesBuilding")
				{
					istrstream iss(arg.GetChar());
					iss >> percentageTreesBuilding;
				}
				else if (key == "Homog0")
				{
					istrstream iss(arg.GetChar());
					iss >> percentageHomog0Building;
				}
				else if (key == "Point0")
				{
					istrstream iss(arg.GetChar());
					iss >> percentagePoints0Building;
				}
				else if (key == "Open")
				{
					istrstream iss(arg.GetChar());
					iss >> structureForOpen;
				}
				else if (key == "Thinning")
				{
					istrstream iss(arg.GetChar());
					iss >> thinning;
				}
				else if (key == "PBuild")
				{
					istrstream iss(arg.GetChar());
					iss >> buildingPercentage;
				}
				else if (key == "Close")
				{
					istrstream iss(arg.GetChar());
					iss >> structureForClose;
				}				
				else if (key == "MaxItPost")
				{
					istrstream iss(arg.GetChar());
					iss >> maxPostClassIterations;
				}
				else if (key == "OutputLevel")
				{
					int mod;
					istrstream iss(arg.GetChar());
					iss >> mod;
					outputMode = (eOUTPUTMODE) mod;
				}
				else if (key == "UseWater")
				{
					int usemod;
					istrstream iss(arg.GetChar());
					iss >> usemod;
					useWaterModel = usemod != 0;
				}
				else if (key == "UseRank" )
				{
					int usemod;
					istrstream iss(arg.GetChar());
					iss >> usemod;
					useRank = usemod != 0;
				}
				else if (key == "RankPercentage")
				{
					istrstream iss(arg.GetChar());
					iss >> this->rankPercentage;
				}
				else if (key == "FirstLastThresholdValue")
				{
					istrstream iss(arg.GetChar());
					iss >> this->FirstLastThresholdValue;
				}
				else if (key == "AssignInverseFLToTheta")
				{
					istrstream iss(arg.GetChar());
					iss >> this->assignInverseFLToTheta;
				}
				else if (key == "PointPercentageThresholdValue")
				{
					istrstream iss(arg.GetChar());
					iss >> this->PointPercentageThresholdValue;
				}
				else if (key == "OutputDir")
				{
					outputDir = arg;
				}
			}
			inFile.getline(z, len);
		}
	}
	delete [] z;
};

#endif // WIN32

//=============================================================================
