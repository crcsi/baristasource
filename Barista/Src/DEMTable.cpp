#include "DEMTable.h"

#include <QCloseEvent>
#include "VDEM.h"

CDEMTable::CDEMTable(CVDEM* dem,QWidget* parent) : CBaristaTable(parent),vdem(dem)
{
	this->decimals = 3;
	this->initTable();
    this->vdem->addListener(this);
}

CDEMTable::~CDEMTable(void)
{
}

void CDEMTable::closeEvent ( QCloseEvent * e )
{
    this->vdem->removeListener(this);
    e->accept();
}


void CDEMTable::setDEM(CVDEM* dem)
{
    this->vdem = dem;
	dem->addListener(this);
}



void CDEMTable::initTable()
{
	if ( this->vdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
		this->decimals = 8;
     else
   		this->decimals = 3;
	
    CCharString str("DEM Parameters:   ");
	str = str + this->vdem->getFileName()->GetFullFileName();

	int nrows = 16;
	this->setAccessibleName(this->vdem->getFileName()->GetChar());
	this->setWindowTitle(str.GetChar());
	this->setColumnCount(2);
	this->setRowCount(nrows);

	for (int i=0; i < nrows; i++)
		this->setRowHeight(i,20);

	this->setColumnWidth(0, 100);
	this->setColumnWidth(1, 100);
	QStringList headerList;

	headerList.append(QString("Parameter" ));
	headerList.append(QString("Value" ));

	this->setHorizontalHeaderLabels(headerList);
     
    double value = 1233;

    bool load = false;
    double maxz = this->vdem->getDEM()->getmaxZ();
    double minz = this->vdem->getDEM()->getminZ();
    double maxx = this->vdem->getDEM()->getmaxX();
    double minx = this->vdem->getDEM()->getminX();
    double maxy = this->vdem->getDEM()->getmaxY();
    double miny = this->vdem->getDEM()->getminY();
    bool gridded = this->vdem->getDEM()->getisgridded();

	double gridx = this->vdem->getDEM()->getgridX();
	double gridy = this->vdem->getDEM()->getgridY();

	QString sys = this->vdem->getReferenceSystem().getCoordinateSystemName().GetChar();

	QTableWidgetItem* item;
	
	////// DEM parameters
	item = this->createItem("Coordinate System",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,0,item);
	item = this->createItem(sys,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(0,1,item);

	item = this->createItem("Min X",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(1,0,item);
	item = this->createItem(QString( "%1" ).arg(minx, 0, 'F', this->decimals ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(1,1,item);
	item = this->createItem("Min Y",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(2,0,item);
	item = this->createItem(QString( "%1" ).arg(miny, 0, 'F', this->decimals ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(2,1,item);

	item = this->createItem("Max X",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(3,0,item);
	item = this->createItem(QString( "%1" ).arg(maxx, 0, 'F', this->decimals ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(3,1,item);
	item = this->createItem("Max Y",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(4,0,item);
	item = this->createItem(QString( "%1" ).arg(maxy, 0, 'F', this->decimals ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(4, 1,item);

	item = this->createItem("Min Height",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(5,0,item);
	item = this->createItem(QString( "%1" ).arg(minz, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(5,1,item);
	item = this->createItem("Max Height",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(6,0,item);
	item = this->createItem(QString( "%1" ).arg(maxz, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(6,1,item);

	item = this->createItem("Gridded",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(7,0,item);
	item = this->createItem("False",0,Qt::AlignRight,QColor(0,0,0));
	if (this->vdem->getDEM()->getisgridded())
		item = this->createItem("True",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(7,1,item);

	item = this->createItem("Grid spacing X",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(8,0,item);
	item = this->createItem(QString( "%1" ).arg(gridx, 0, 'F', this->decimals ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(8,1,item);
	item = this->createItem("Grid spacing Y",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(9,0,item);
	item = this->createItem(QString( "%1" ).arg(gridy, 0, 'F', this->decimals ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(9,1,item);
  
	item = this->createItem("Width",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(10,0,item);
	item = this->createItem(QString( "%1" ).arg(this->vdem->getDEM()->getWidth()),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(10,1,item);
	item = this->createItem("Height",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(11,0,item);
	item = this->createItem(QString( "%1" ).arg(this->vdem->getDEM()->getHeight()),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(11,1,item);

	item = this->createItem("Number of points",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(12,0,item);
	item = this->createItem(QString( "%1" ).arg(this->vdem->getDEM()->getNumPoints()),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(12,1,item);
	item = this->createItem("Sigma Z",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(13,0,item);
	float sigmaZ = this->vdem->getDEM()->getSigmaZ();
	item = this->createItem(QString( "%1" ).arg(sigmaZ, 0, 'F', 3 ),0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(13,1,item);

	////////////////////

	item = this->createItem("DEM project name",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(14,0,item);

	QString projname(this->vdem->getFileName()->GetFullFileName());

	item = this->createItem(projname,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(14,1,item);

	item = this->createItem("File name on disk",0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(15,0,item);

	QString filename(this->vdem->getDEM()->getFileNameChar());

	int col1width = 6*filename.length();
	this->setColumnWidth(1, col1width);

	item = this->createItem(filename,0,Qt::AlignRight,QColor(0,0,0));
	this->setItem(15,1,item);
}

void CDEMTable::elementsChanged(CLabel element)
{
	this->updateContents();
}
