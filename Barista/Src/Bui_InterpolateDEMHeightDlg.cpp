#include "Bui_InterpolateDEMHeightDlg.h"
#include "VDEM.h"
#include "Icons.h"

bui_InterpolateDEMHeightDlg::bui_InterpolateDEMHeightDlg(void)
{
	setupUi(this);

   // signals and slots connections
	connect( this->GetHeight, SIGNAL( released() ), this, SLOT( getHeight_released() ) );
	connect( this->Close, SIGNAL( released() ), this, SLOT( Close_released() ) );

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
}

bui_InterpolateDEMHeightDlg::~bui_InterpolateDEMHeightDlg(void)
{
}

void bui_InterpolateDEMHeightDlg::setX( double x )
{
	this->x = x;
	this->XInput->setText(QString( "%1" ).arg(this->x , 0, 'F', 4 ));
}

double bui_InterpolateDEMHeightDlg::getX()
{
	return this->x;
}

void bui_InterpolateDEMHeightDlg::setY( double y )
{
	this->y = y;
	this->YInput->setText(QString( "%1" ).arg(this->y , 0, 'F', 4 ));
}

double bui_InterpolateDEMHeightDlg::getY()
{
	return this->y;
}

void bui_InterpolateDEMHeightDlg::setZ( double z )
{
	this->z = z;
	this->ZOutput->setText(QString( "%1" ).arg(this->z , 0, 'F', 4 ));
}

double bui_InterpolateDEMHeightDlg::getZ()
{
	return this->z;
}

void bui_InterpolateDEMHeightDlg::getHeight_released()
{

	this->x = this->XInput->text().toDouble();
	this->y = this->YInput->text().toDouble();
	this->setZ(this->dem->getDEM()->getBLInterpolatedHeight(this->getX(), this->getY()));
}

void bui_InterpolateDEMHeightDlg::Close_released()
{
	this->close();
}

void bui_InterpolateDEMHeightDlg::setDEM(CVDEM* dem)
{
	this->dem = dem;
}