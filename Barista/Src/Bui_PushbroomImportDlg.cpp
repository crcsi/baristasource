#include <QFile>
#include <QHeaderView>
#include <QFileDialog>


#include "Bui_PushbroomImportDlg.h"

#include "BaristaHtmlHelp.h"
#include "StructuredFile.h"
#include "BaristaProject.h"
#include "IniFile.h"
#include "BaristaProject.h"
#include "EditorDelegate.h"
#include "Bui_ReferenceSysDlg.h"
#include "BaristaViewEventHandler.h"
#include "Bui_MainWindow.h"
#include "BaristaProjectHelper.h"
#include "SystemUtilities.h"
#include <QTreeView>
#include <QMessageBox>


Bui_PushbroomImportDlg::Bui_PushbroomImportDlg(CVImage* vimage,QWidget *parent)
	: QDialog(parent),success(false),selectedCamera(0),groundTemp(15.0),
	  projectH(project.getMeanHeight()),inTrackViewAngle(0.0),crossTrackViewAngle(0.0),
	  selectedCoordinateType(eGEOCENTRIC),hasMeasuredSuccessfully(false),offNadirAngle(0.0),
	  image(vimage),offNadirEntered(false),hasEnteredRefInfo(false)
{
	this->setupUi(this);

	this->lE_Xc->setValidator(new QDoubleValidator(this));
	this->lE_Yc->setValidator(new QDoubleValidator(this));
	this->lE_F->setValidator(new QDoubleValidator(this));
	this->lE_SatH->setValidator(new QDoubleValidator(0.0,5000.0,1,this));
	this->lE_InTrackAngle->setValidator(new QDoubleValidator(-90.0,90.0,5,this));
	this->lE_CrossTrackAngle->setValidator(new QDoubleValidator(-90.0,90.0,5,this));
	this->lE_OffNadir->setValidator(new QDoubleValidator(-90.0,90.0,5,this));
	this->lE_GroundHeight->setValidator(new QDoubleValidator(-1000.0,8000.0,5,this));
	this->lE_GroundTemp->setValidator(new QDoubleValidator(-100.0,100.0,5,this));


	this->connect(this->pB_Ok,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pB_Cancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pB_Help,SIGNAL(released()),this,SLOT(pBHelpReleased()));
	this->connect(this->pB_SaveCamera,SIGNAL(released()),this,SLOT(pBSaveCameraReleased()));
	this->connect(this->pB_LoadSceneInfo,SIGNAL(released()),this,SLOT(pBLoadSceneInfoReleased()));
	this->connect(this->pB_SaveSceneInfo,SIGNAL(released()),this,SLOT(pBSaveSceneInfoReleased()));
	this->connect(this->pB_Measure,SIGNAL(released()),this,SLOT(pBMeasureReleased()));

	this->connect(this->cB_Edit,SIGNAL(stateChanged(int)),this,SLOT(cBEditCameraChanged(int)));

	this->connect(this->rB_Geocentric,SIGNAL(released()),this,SLOT(rBGeocentricReleased()));
	this->connect(this->rB_Geographic,SIGNAL(released()),this,SLOT(rBGeographicReleased()));
	this->connect(this->rB_Grid,SIGNAL(released()),this,SLOT(rBGridReleased()));

	this->connect(this->rB_Direct,SIGNAL(released()),this,SLOT(rBDirectReleased()));
	this->connect(this->rB_Measure,SIGNAL(released()),this,SLOT(rBMeasureReleased()));

	this->connect(this->lE_Xc,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_Yc,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_F,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_SatH,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_InTrackAngle,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_CrossTrackAngle,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_OffNadir,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_GroundHeight,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_GroundTemp,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_CameraName,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));


	this->connect(this->lE_Xc,SIGNAL(editingFinished()),this,SLOT(editingFinishedXc()));
	this->connect(this->lE_Yc,SIGNAL(editingFinished()),this,SLOT(editingFinishedYc()));
	this->connect(this->lE_F,SIGNAL(editingFinished()),this,SLOT(editingFinishedF()));
	this->connect(this->lE_SatH,SIGNAL(editingFinished()),this,SLOT(editingFinishedSatH()));
	this->connect(this->lE_InTrackAngle,SIGNAL(editingFinished()),this,SLOT(editingFinishedInTrackViewAngle()));
	this->connect(this->lE_CrossTrackAngle,SIGNAL(editingFinished()),this,SLOT(editingFinishedCrossTrackViewAngle()));
	this->connect(this->lE_OffNadir,SIGNAL(editingFinished()),this,SLOT(editingFinishedOffNadirAngle()));
	this->connect(this->lE_GroundHeight,SIGNAL(editingFinished()),this,SLOT(editingFinishedGroundHeight()));
	this->connect(this->lE_GroundTemp,SIGNAL(editingFinished()),this,SLOT(editingFinishedGroundTemp()));
	this->connect(this->lE_CameraName,SIGNAL(editingFinished()),this,SLOT(editingFinishedCameraName()));

	this->connect(this->cB_Camera,SIGNAL(currentIndexChanged(int)),this,SLOT(currentCameraChanged(int)));
	
	this->connect(this->tW_Scene,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(scenePointChanged(QTableWidgetItem*)));

	inifile.getLastViewAngle(this->inTrackViewAngle,this->crossTrackViewAngle);

	this->lE_InTrackAngle->setText(QString().sprintf("%.4lf",this->inTrackViewAngle));
	this->lE_CrossTrackAngle->setText(QString().sprintf("%.4lf",this->crossTrackViewAngle));
	this->lE_GroundHeight->setText(QString().sprintf("%.1lf",this->projectH));
	this->lE_GroundTemp->setText(QString().sprintf("%.1lf",this->groundTemp));
	this->lE_OffNadir->setText(QString().sprintf("%.1lf",this->offNadirAngle));

	if (this->selectedCoordinateType == eGEOCENTRIC)
	{
		this->rB_Geocentric->setChecked(true);
		this->rBGeocentricReleased();
	}
	else if (this->selectedCoordinateType == eGEOGRAPHIC)
	{
		this->rB_Geographic->setChecked(true);
		this->rBGeographicReleased();
	}
	else
	{
		this->rB_Grid->setChecked(true);
		this->rBGridReleased();
	}


	this->rB_Direct->setChecked(true);


	this->readCameras();
	this->checkSettings();





/*

	//test

	QDialog* testDlg = new QDialog(parent);
	QGridLayout* l = new QGridLayout(testDlg);
	QTreeView* tv = new QTreeView();
	CBaristaThumbnailModel* m = new CBaristaThumbnailModel(tv);
	
	tv->setModel(m);
	l->addWidget(tv);
	testDlg->setLayout(l);

	testDlg->show();
*/


}

Bui_PushbroomImportDlg::~Bui_PushbroomImportDlg()
{

}


void Bui_PushbroomImportDlg::pBOkReleased()
{

	this->success = true;


	// we are only asking when grid is selected, so let the user confirm the refsys for geocentric and 
	//geographic here
	if (!this->hasEnteredRefInfo)
	{
		CFileName dummy = "Reference system";
		Bui_ReferenceSysDlg dlg(&this->refSys,&dummy,"Select Grid Settings","",false,"",true);	
	
		dlg.exec();
		this->success = dlg.getSuccess();
	}

	if (this->success)
	{
		inifile.setSelectedCamera(this->selectedCamera);
		inifile.setLastViewAngle(this->inTrackViewAngle,this->crossTrackViewAngle);
		inifile.writeIniFile();
	}

	this->close();
}

void Bui_PushbroomImportDlg::pBCancelReleased()
{
	this->success = false;
	this->close(); 
}

void Bui_PushbroomImportDlg::closeEvent(QCloseEvent* e)
{
	this->informListeners_elementDeleted();
}


void Bui_PushbroomImportDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/ImportGenericPushBroom.html");

}


void Bui_PushbroomImportDlg::pBSaveCameraReleased()
{
	CStructuredFile structuredFile;
	CStructuredFileSection* pMainSection = structuredFile.getMainSection();

	CFileName cameraFileName = inifile.getIniFileName();
	cameraFileName = cameraFileName.GetPath();
	cameraFileName += CSystemUtilities::dirDelimStr + "cameraFile.ini";
	
	bool saveNewCamera = false;
	if (this->cameras[0].getName().CompareNoCase("User defined camera"))
		saveNewCamera = false;
	else
		saveNewCamera = true;

	CameraInfos readHelper(this->cameras,this->satH,saveNewCamera);
	readHelper.serializeStore(pMainSection);
	structuredFile.writeFile(cameraFileName.GetChar());
}


void Bui_PushbroomImportDlg::leTextChanged(const QString & text)
{
	this->checkSettings();
}

void Bui_PushbroomImportDlg::checkSettings()
{
	if (!this->lE_F->hasAcceptableInput() || 
		!this->lE_Xc->hasAcceptableInput() || 
		!this->lE_Yc->hasAcceptableInput() ||
		!this->lE_SatH->hasAcceptableInput() ||
		!this->lE_InTrackAngle->hasAcceptableInput() ||
		!this->lE_CrossTrackAngle->hasAcceptableInput() ||
		!this->lE_GroundHeight->hasAcceptableInput() ||
		!this->lE_GroundTemp->hasAcceptableInput() ||
		this->lE_CameraName->text().isEmpty() ||
		(this->rB_Measure->isChecked() && !this->lE_OffNadir->hasAcceptableInput()) ||
		(this->rB_Measure->isChecked() && !this->hasMeasuredSuccessfully)
		)
	{
		this->pB_Ok->setEnabled(false);
	}
	else
	{
		if (this->checkTable())
			this->pB_Ok->setEnabled(true);
		else
			this->pB_Ok->setEnabled(false);
	}

	if (this->selectedCamera == 0 || this->cB_Edit->isChecked())
	{
		if (this->lE_Xc->hasAcceptableInput() && 
			this->lE_Yc->hasAcceptableInput() &&
			this->lE_F->hasAcceptableInput() &&
			!this->lE_CameraName->text().isEmpty())
		{
			this->pB_SaveCamera->setEnabled(true);
		}
		else
			this->pB_SaveCamera->setEnabled(false);

		this->lE_F->setEnabled(true);
		this->lE_Xc->setEnabled(true);
		this->lE_Yc->setEnabled(true);
		this->lE_CameraName->setEnabled(true);		
		this->lE_SatH->setEnabled(true);
		
	}
	else
	{
		this->lE_F->setEnabled(false);
		this->lE_Xc->setEnabled(false);
		this->lE_Yc->setEnabled(false);
		this->lE_CameraName->setEnabled(false);		
		this->pB_SaveCamera->setEnabled(false);
		this->lE_SatH->setEnabled(false);
	}

	if (this->rB_Measure->isChecked())
	{
		this->pB_Measure->setEnabled(true);
	}
	else
		this->pB_Measure->setEnabled(false);

}

bool Bui_PushbroomImportDlg::checkTable()
{
	for (int r=0; r < this->tW_Scene->rowCount(); r++)
		for (int c=0; c < this->tW_Scene->columnCount(); c++)
			if (this->tW_Scene->item(r,c)->text().isEmpty())
				return false;
	
	return true;		
}


bool Bui_PushbroomImportDlg::readCameras()
{
	CameraInfos readHelper(this->cameras,this->satH,false);
	
	this->selectedCamera = inifile.getSelectedCamera();

	// first camera is always a user defined camera
	CCCDLine ccdLine;
	ccdLine.setName("User defined camera");
	this->cameras.Add(ccdLine);

	CFileName cameraFileName = inifile.getIniFileName();
	cameraFileName = cameraFileName.GetPath();
	cameraFileName += CSystemUtilities::dirDelimStr + "cameraFile.ini";

	// first check if user has it's own camera file
	if (!QFile::exists(cameraFileName.GetChar()))
	{
		// if not, use the global one
		cameraFileName = QApplication::applicationDirPath().toLatin1();
		cameraFileName += CSystemUtilities::dirDelimStr + "cameraFile.ini";
		if (!QFile::exists(cameraFileName.GetChar()))
			return false;
	}

	CStructuredFile structuredFile;
	CStructuredFileSection* pMainSection = structuredFile.getMainSection();
	structuredFile.readFile(cameraFileName);
	
	readHelper.serializeRestore(pMainSection);
	


	this->cB_Camera->blockSignals(true);

	this->cB_Camera->clear();
	for (int i=0; i < this->cameras.GetSize(); i++)
		this->cB_Camera->addItem(this->cameras.GetAt(i)->getName().GetChar());
	
	

	if (this->selectedCamera >= this->cB_Camera->count())
		this->selectedCamera = 0; // we always have one camera

	this->cB_Camera->setCurrentIndex(this->selectedCamera);
	
	this->cB_Camera->blockSignals(false);


	this->currentCameraChanged(this->selectedCamera);

	return true;
}

void Bui_PushbroomImportDlg::currentCameraChanged( int index )
{
	if ( index >= this->cameras.GetSize())
		index = 0;

	this->selectedCamera = index;
	CCamera *cam = this->cameras.GetAt(index);
	CXYZPoint irp = cam->getIRP();

	this->lE_F->setText(QString().sprintf("%.5lf",irp.z));
	this->lE_Xc->setText(QString().sprintf("%.5lf",irp.x));
	this->lE_Yc->setText(QString().sprintf("%.5lf",irp.y));
	this->lE_CameraName->setText(cam->getName().GetChar());

	this->lE_SatH->setText(QString().sprintf("%.1lf",this->satH[index]));

	if (index == 0)
		this->cB_Edit->setChecked(true);

	this->checkSettings();
}


void Bui_PushbroomImportDlg::editingFinishedXc()
{
	if (this->selectedCamera < this->cameras.GetSize())
	{
		CXYZPoint irp = this->cameras.GetAt(this->selectedCamera)->getIRP();
		irp.x = this->lE_Xc->text().toDouble();
		this->cameras.GetAt(this->selectedCamera)->setIRP(irp);

	}
}

void Bui_PushbroomImportDlg::editingFinishedYc()
{
	if (this->selectedCamera < this->cameras.GetSize())
	{
		CXYZPoint irp = this->cameras.GetAt(this->selectedCamera)->getIRP();
		irp.y = this->lE_Yc->text().toDouble();
		this->cameras.GetAt(this->selectedCamera)->setIRP(irp);

	}
}

void Bui_PushbroomImportDlg::editingFinishedF()
{
	if (this->selectedCamera < this->cameras.GetSize())
	{
		CXYZPoint irp = this->cameras.GetAt(this->selectedCamera)->getIRP();
		irp.z = this->lE_F->text().toDouble();
		this->cameras.GetAt(this->selectedCamera)->setIRP(irp);

	}
}

void Bui_PushbroomImportDlg::editingFinishedCameraName()
{
	if (this->selectedCamera < this->cameras.GetSize())
	{
		this->cameras.GetAt(this->selectedCamera)->setName(this->lE_CameraName->text().toLatin1().constData());
	}
}

void Bui_PushbroomImportDlg::editingFinishedSatH()
{
	if (this->selectedCamera < int(this->satH.size()))
	{
		this->satH[this->selectedCamera] =  this->lE_SatH->text().toDouble();
	}
}

void Bui_PushbroomImportDlg::editingFinishedGroundTemp()
{
	this->groundTemp = this->lE_GroundTemp->text().toDouble();
}

void Bui_PushbroomImportDlg::editingFinishedGroundHeight()
{
	this->projectH = this->lE_GroundHeight->text().toDouble();
}

void Bui_PushbroomImportDlg::editingFinishedInTrackViewAngle()
{
	this->inTrackViewAngle = this->lE_InTrackAngle->text().toDouble();
}

void Bui_PushbroomImportDlg::editingFinishedCrossTrackViewAngle()
{
	this->crossTrackViewAngle = this->lE_CrossTrackAngle->text().toDouble();
}



void Bui_PushbroomImportDlg::initSceneTable(int nCols)
{
	this->tW_Scene->blockSignals(true);

	if (nCols != 2 && nCols != 3)
		return;

	this->tW_Scene->clear();

	this->tW_Scene->setRowCount(4);
	this->tW_Scene->setColumnCount(nCols);

	this->tW_Scene->setItem(0,0,new QTableWidgetItem(""));
	this->tW_Scene->setItem(0,1,new QTableWidgetItem(""));
	this->tW_Scene->setItem(1,0,new QTableWidgetItem(""));
	this->tW_Scene->setItem(1,1,new QTableWidgetItem(""));
	this->tW_Scene->setItem(2,0,new QTableWidgetItem(""));
	this->tW_Scene->setItem(2,1,new QTableWidgetItem(""));
	this->tW_Scene->setItem(3,0,new QTableWidgetItem(""));
	this->tW_Scene->setItem(3,1,new QTableWidgetItem(""));



	QStringList vLabels;
	vLabels << "UL";
	vLabels << "UR";
	vLabels << "LL";
	vLabels << "LR";


	this->tW_Scene->setVerticalHeaderLabels(vLabels);

	this->tW_Scene->setItemDelegate(new CEditorDelegate(this->tW_Scene,CEditorDelegate::eDoubleValidator));

	double h = 20.0;
	this->tW_Scene->setRowHeight(0,h);
	this->tW_Scene->setRowHeight(1,h);
	this->tW_Scene->setRowHeight(2,h);
	this->tW_Scene->setRowHeight(3,h);

	this->tW_Scene->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
	this->tW_Scene->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);

	if (nCols == 3)
	{
		this->tW_Scene->setItem(0,2,new QTableWidgetItem(""));
		this->tW_Scene->setItem(1,2,new QTableWidgetItem(""));
		this->tW_Scene->setItem(2,2,new QTableWidgetItem(""));
		this->tW_Scene->setItem(3,2,new QTableWidgetItem(""));

		this->tW_Scene->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);
	}

	this->tW_Scene->blockSignals(false);
}




void Bui_PushbroomImportDlg::rBGeocentricReleased()
{
	this->initSceneTable(3);

	this->selectedCoordinateType = eGEOCENTRIC;
	this->refSys.setCoordinateType(eGEOCENTRIC);

	QStringList hLabels;
	hLabels << "X [m]";
	hLabels << "Y [m]";
	hLabels << "Z [m]";
	this->tW_Scene->setHorizontalHeaderLabels(hLabels);
	this->hasEnteredRefInfo = false;
	this->checkSettings();

}

void Bui_PushbroomImportDlg::rBGeographicReleased()
{
	this->initSceneTable(2);

	this->selectedCoordinateType = eGEOGRAPHIC;
	this->refSys.setCoordinateType(eGEOGRAPHIC);

	QStringList hLabels;
	hLabels << "Latitude [deg]";
	hLabels << "Longitude [deg]";
	this->tW_Scene->setHorizontalHeaderLabels(hLabels);
	this->hasEnteredRefInfo = false;
	this->checkSettings();


}

void Bui_PushbroomImportDlg::rBGridReleased()
{
	this->initSceneTable(2);
	
	this->selectedCoordinateType = eGRID;
	this->refSys.setCoordinateType(eGRID);
	
	QStringList hLabels;
	hLabels << "Easting [m]";
	hLabels << "Northing [m]";
	this->tW_Scene->setHorizontalHeaderLabels(hLabels);

	if (inifile.getIsUTM())
		this->refSys.setUTMZone(inifile.getZone(),inifile.getHemisphere());
	else
		this->refSys.setTMParameters(inifile.getCentralMeridian(),
									 inifile.getTMEasting(),
									 inifile.getTMNorthing(),
									 inifile.getTMScale(),
									 inifile.getLatitudeOrigin());

	CFileName dummy = "Reference system";
	Bui_ReferenceSysDlg dlg(&this->refSys,&dummy,"Select Grid Settings","",false,"",true);
	
	dlg.exec();

	if (dlg.getSuccess())
		this->hasEnteredRefInfo = true;

	// just in case user has change the type
	if (this->refSys.getCoordinateType() == eGEOCENTRIC)
	{
		this->rB_Geocentric->setChecked(true);
		this->rBGeocentricReleased();
	}
	else if (refSys.getCoordinateType() == eGEOGRAPHIC)
	{
		this->rB_Geographic->setChecked(true);
		this->rBGeographicReleased();
	}


	this->checkSettings();
}



void Bui_PushbroomImportDlg::scenePointChanged(QTableWidgetItem * item)
{
	int c = item->column();
	int r = item->row();

	if (r == 0)
	{
		if (c == 0)
			this->UL.x = item->text().toDouble();
		else if (c == 1)
			this->UL.y = item->text().toDouble();

		if (this->selectedCoordinateType == eGEOCENTRIC)
			this->UL.z = item->text().toDouble();
		else
			this->UL.z = this->projectH;
	}
	else if (r == 1)
	{
		if (c == 0)
			this->UR.x = item->text().toDouble();
		else if (c == 1)
			this->UR.y = item->text().toDouble();

		if (this->selectedCoordinateType == eGEOCENTRIC)
			this->UL.z = item->text().toDouble();
		else
			this->UL.z = this->projectH;
	}
	else if (r == 2)
	{
		if (c == 0)
			this->LL.x = item->text().toDouble();
		else if (c == 1)
			this->LL.y = item->text().toDouble();

		if (this->selectedCoordinateType == eGEOCENTRIC)
			this->UL.z = item->text().toDouble();
		else
			this->UL.z = this->projectH;
	}
	else if (r == 3)
	{
		if (c == 0)
			this->LR.x = item->text().toDouble();
		else if (c == 1)
			this->LR.y = item->text().toDouble();

		if (this->selectedCoordinateType == eGEOCENTRIC)
			this->UL.z = item->text().toDouble();
		else
			this->UL.z = this->projectH;
	}

	this->checkSettings();
}



CCamera* Bui_PushbroomImportDlg::getCamera()
{
	if (this->selectedCamera <= this->cameras.GetSize())
		return project.addCamera(this->cameras.GetAt(this->selectedCamera));
	else
		return NULL;
}

double Bui_PushbroomImportDlg::getSatelliteHeight() const
{
	if (this->selectedCamera <= int(this->satH.size()))
		return this->satH[this->selectedCamera] * 1000.0;
	else
		return 0.0;
}

void Bui_PushbroomImportDlg::pBSaveSceneInfoReleased()
{

	QString saveFile = QFileDialog::getSaveFileName(this,"Save scene information",inifile.getCurrDir());

	if (!saveFile.isEmpty())
	{
		CStructuredFile structuredFile;
		CStructuredFileSection* pMainSection = structuredFile.getMainSection();

		SceneInfo readHelper(this->LL,this->LR,this->UL,this->UR,this->refSys,
							this->groundTemp,this->projectH,this->inTrackViewAngle,
							this->crossTrackViewAngle);

		readHelper.serializeStore(pMainSection);
		structuredFile.writeFile(saveFile.toLatin1().constData());
	}
}

void Bui_PushbroomImportDlg::pBLoadSceneInfoReleased()
{
	QString loadFile = QFileDialog::getOpenFileName(this,"Load scene information",inifile.getCurrDir());

	if (!loadFile.isEmpty())
	{
		SceneInfo readHelper(this->LL,this->LR,this->UL,this->UR,this->refSys,
							this->groundTemp,this->projectH,this->inTrackViewAngle,
							this->crossTrackViewAngle);
	
	
		CStructuredFile structuredFile;
		CStructuredFileSection* pMainSection = structuredFile.getMainSection();
		structuredFile.readFile(loadFile.toLatin1().constData());
	
		readHelper.serializeRestore(pMainSection);

		if (this->refSys.getCoordinateType() == eGEOCENTRIC)
		{
			this->rB_Geocentric->setChecked(true);
			this->rBGeocentricReleased();
			this->fillSceneTable(3);
			
		}
		else if (this->refSys.getCoordinateType() == eGEOGRAPHIC)
		{
			this->rB_Geographic->setChecked(true);
			this->rBGeographicReleased();
			this->fillSceneTable(2);
		}
		else
		{
			this->rB_Grid->setChecked(true);
			this->rBGridReleased();
			this->fillSceneTable(2);
		}	
	
		this->lE_GroundTemp->setText(QString().sprintf("%.5lf",this->groundTemp));
		this->lE_GroundHeight->setText(QString().sprintf("%.5lf",this->projectH));
		this->lE_InTrackAngle->setText(QString().sprintf("%.5lf",this->inTrackViewAngle));
		this->lE_CrossTrackAngle->setText(QString().sprintf("%.5lf",this->crossTrackViewAngle));
		this->hasEnteredRefInfo = true;

		this->checkSettings();
	}

}

void Bui_PushbroomImportDlg::fillSceneTable(int nCols)
{
	if (this->tW_Scene->rowCount() != 4 ||
		this->tW_Scene->columnCount() != nCols)
		return;

	CCharString format;
	
	if (this->selectedCoordinateType == eGEOGRAPHIC)
		format = "%.7lf";
	else
		format = "%.2lf";

	for (int c=0; c < nCols; c++)
	{
		this->tW_Scene->item(0,c)->setText(QString().sprintf(format.GetChar(),this->UL[c]));
		this->tW_Scene->item(1,c)->setText(QString().sprintf(format.GetChar(),this->UR[c]));
		this->tW_Scene->item(2,c)->setText(QString().sprintf(format.GetChar(),this->LL[c]));
		this->tW_Scene->item(3,c)->setText(QString().sprintf(format.GetChar(),this->LR[c]));

	}

}

void Bui_PushbroomImportDlg::cBEditCameraChanged(int state)
{
	this->checkSettings();
}


C3DPoint Bui_PushbroomImportDlg::getLL() const 
{
	C3DPoint ret = this->LL;
	
	// we always return geographic
	this->transformPoint(ret);
	return ret;
}

C3DPoint Bui_PushbroomImportDlg::getUL() const
{
	C3DPoint ret = this->UL;
	
	// we always return geographic
	this->transformPoint(ret);
	return ret;
}


C3DPoint Bui_PushbroomImportDlg::getLR() const 
{
	C3DPoint ret = this->LR;
	
	// we always return geographic
	this->transformPoint(ret);
	return ret;
}


C3DPoint Bui_PushbroomImportDlg::getUR() const 
{
	C3DPoint ret = this->UR;
	
	// we always return geographic
	this->transformPoint(ret);
	return ret;
}

void Bui_PushbroomImportDlg::transformPoint(C3DPoint& point)const
{
	if (this->refSys.getCoordinateType() == eGRID)
		this->refSys.gridToGeographic(point,point);
	else if (this->refSys.getCoordinateType() == eGEOCENTRIC)
		this->refSys.geocentricToGeographic(point,point);
}

void Bui_PushbroomImportDlg::editingFinishedOffNadirAngle()
{
	this->offNadirAngle = this->lE_OffNadir->text().toDouble();
	this->offNadirEntered = true;
}

void Bui_PushbroomImportDlg::rBDirectReleased()
{
	this->setModal(true);
	this->activateWindow();
	this->checkSettings();
}

void Bui_PushbroomImportDlg::rBMeasureReleased()
{
	this->setModal(true);
	this->show();
	this->checkSettings();
}


void Bui_PushbroomImportDlg::pBMeasureReleased()
{

	if (!this->offNadirEntered)
	{
		QMessageBox::warning( this, "Enter off nadir angle first", "Enter off nadir angle before measuring the vertical plole !");
	}
	else
	{
		// reset 
		this->hasMeasuredSuccessfully = false;
		this->setModal(false);

		CBaristaProjectHelper helper;
		helper.openBaristaView(this->image);
	}


}


void Bui_PushbroomImportDlg::setViewingAngles(double inTrack,double crossTrack)
{
	this->inTrackViewAngle = inTrack * 180 / M_PI;
	this->crossTrackViewAngle = crossTrack * 180 / M_PI;
	
	this->lE_InTrackAngle->setText(QString().sprintf("%.4lf",this->inTrackViewAngle));
	this->lE_CrossTrackAngle->setText(QString().sprintf("%.4lf",this->crossTrackViewAngle));

	this->hasMeasuredSuccessfully = true;
	this->checkSettings();
}




C3DPoint Bui_PushbroomImportDlg::getIRP()
{
	return this->cameras[this->selectedCamera].getIRP();
}














CameraInfos::CameraInfos(CCameraArray& cameras,doubles& satH,bool saveNewCamera) :
	cameras(cameras),satH(satH),saveNewCamera(saveNewCamera)
{

}

void CameraInfos::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);
	
	CToken* token = pStructuredFileSection->addToken();
    token->setValue("number of cameras", int(this->satH.size()));

	CCharString valueStr;
	CCharString elementStr;

	// satellite heights 
	token = pStructuredFileSection->addToken();
	valueStr = "";

	// skip first height, belongs to default camera,
	int start = this->saveNewCamera ? 0 : 1;
	
	for (unsigned int i = start; i < this->satH.size(); i++)
		{
			double element = this->satH[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	token->setValue("satellite heights", valueStr.GetChar());

	CStructuredFileSection* child = pStructuredFileSection->addChild();
	
	if (this->saveNewCamera)
	{
		this->cameras.serializeStore(child);
	}
	else // don't save the default camera
	{ 
		CCameraArray tmpCameraArray(this->cameras);
		tmpCameraArray.RemoveAt(0);
		tmpCameraArray.serializeStore(child);
	}
}

void CameraInfos::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);
	
	int size = 0;
	doubles tmpHeights;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("number of cameras"))
            tmpHeights.resize(token->getIntValue());
		else if (key.CompareNoCase("satellite heights"))
			token->getDoublesClass(tmpHeights);

	}


	for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
	{
		CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
		if (child->getName().CompareNoCase("CCameraArray"))
		{
			this->cameras.serializeRestore(child);
		}
	}

	// make sure we have the same number of entries
	this->satH.resize(this->cameras.GetSize());
	
	for (unsigned int i=1; i < this->satH.size() && (i-1) < tmpHeights.size() ; i++)
		this->satH[i] = tmpHeights[i-1];


}
	


SceneInfo::SceneInfo(C3DPoint &LL,	C3DPoint &LR,C3DPoint &UL,C3DPoint &UR,
				CReferenceSystem &refSys,double &groundTemp,
				double &projectH,double &inTrackViewAngle,
				double &crossTrackViewAngle) :
	LL(LL),LR(LR),UR(UR),UL(UL),refSys(refSys),groundTemp(groundTemp),
	projectH(projectH),inTrackViewAngle(inTrackViewAngle),crossTrackViewAngle(crossTrackViewAngle)
{

}


void SceneInfo::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);
	
	CToken* token = pStructuredFileSection->addToken();
    token->setValue("groundTemp", this->groundTemp);

	token = pStructuredFileSection->addToken();
    token->setValue("projectH", this->projectH);

	token = pStructuredFileSection->addToken();
    token->setValue("inTrackViewAngle", this->inTrackViewAngle);

	token = pStructuredFileSection->addToken();
    token->setValue("crossTrackViewAngle", this->crossTrackViewAngle);


	CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->LL.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->LR.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->UL.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->UR.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->refSys.serializeStore(child);

}

void SceneInfo::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);
	
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("groundTemp"))
			this->groundTemp = token->getDoubleValue();
		else if (key.CompareNoCase("projectH"))
			this->projectH = token->getDoubleValue();
		else if (key.CompareNoCase("inTrackViewAngle"))
			this->inTrackViewAngle = token->getDoubleValue();
		else if (key.CompareNoCase("crossTrackViewAngle"))
			this->crossTrackViewAngle = token->getDoubleValue();

	}

	bool readLL = false;
	bool readLR = false;
	bool readUL = false;
	bool readUR = false;


	for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
	{
		CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
		if (child->getName().CompareNoCase("C3DPoint") && !readLL && !readLR && !readUL && !readUR)
		{
			this->LL.serializeRestore(child);
			readLL = true;
		}
		else if (child->getName().CompareNoCase("C3DPoint") && readLL && !readLR && !readUL && !readUR)
		{
			this->LR.serializeRestore(child);
			readLR = true;
		}
		else if (child->getName().CompareNoCase("C3DPoint") && readLL && readLR && !readUL && !readUR)
		{
			this->UL.serializeRestore(child);
			readUL = true;
		}
		else if (child->getName().CompareNoCase("C3DPoint") && readLL && readLR && readUL && !readUR)
		{
			this->UR.serializeRestore(child);
			readUR = true;
		}
		else if (child->getName().CompareNoCase("CReferenceSystem"))
		{
			this->refSys.serializeRestore(child);
		}
	}

}
	










CBaristaThumbnailModel::CBaristaThumbnailModel(QObject* parent) : 
	QAbstractItemModel(parent)
{
	
}
CBaristaThumbnailModel::~CBaristaThumbnailModel(void)
{

}

QModelIndex CBaristaThumbnailModel::index(int,int,const QModelIndex &parent) const
{
	return QModelIndex();
}

QModelIndex CBaristaThumbnailModel::parent(const QModelIndex &child) const
{
	return QModelIndex();
}

int CBaristaThumbnailModel::rowCount(const QModelIndex &parent) const
{
	
	return 0;
}

int CBaristaThumbnailModel::columnCount(const QModelIndex &parent) const
{
	return 1;
}

QVariant CBaristaThumbnailModel::data(const QModelIndex &index,int role) const
{
	return QVariant();
}

QVariant CBaristaThumbnailModel::headerData(int section,Qt::Orientation orientation,int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
	{
		if (section == 0)
			return QString("Items");

	}

	return QVariant();
}