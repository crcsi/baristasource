#include "Bui_ALSInterpolationDialog.h"

#include <qfiledialog.h> 
#include <QMessageBox> 

#include "IniFile.h"
#include "ObjectSelector.h"
#include "ProgressThread.h"
#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "BaristaProject.h"

#include "VALS.h"
#include "ALSPointCloud.h"


bui_ALSInterpolationDialog::bui_ALSInterpolationDialog(CVALS *als, const char *title, 
													   const char *helptext, bool DEM) : 
				helpText(helptext), ALS(als), pmin(0,0), pmax(1,1), resolution(1,1), pulse(0), 
				neighbourhood(4), level(0), method(eMovingTiltedPlane), success(false),
				DEMflag(DEM), points(0), maxDist(-1.0)
{
	setupUi(this);
	this->setWindowTitle(QString(title));
	
	connect(this->minX,                     SIGNAL(editingFinished()), this, SLOT(LimitsMinSelected()));
	connect(this->minY,                     SIGNAL(editingFinished()), this, SLOT(LimitsMinSelected()));
	connect(this->maxX,                     SIGNAL(editingFinished()), this, SLOT(LimitsMaxSelected()));
	connect(this->maxY,                     SIGNAL(editingFinished()), this, SLOT(LimitsMaxSelected()));
	connect(this->maxExtentsButton,         SIGNAL(released()),        this, SLOT(SetToMax()));
	connect(this->GResolution,              SIGNAL(editingFinished()), this, SLOT(GridSelected()));
	connect(this->MovingHZButton,           SIGNAL(clicked(bool)),     this, SLOT(InterpolationMethodSelected()));
	connect(this->MovingTiltedPlaneButton,  SIGNAL(clicked(bool)),     this, SLOT(InterpolationMethodSelected()));
	connect(this->TriangulationRadioButton, SIGNAL(clicked(bool)),     this, SLOT(InterpolationMethodSelected()));
	connect(this->FirstPulseButton,         SIGNAL(clicked(bool)),     this, SLOT(PulseSelected()));
	connect(this->LastPulseButton,          SIGNAL(clicked(bool)),     this, SLOT(PulseSelected()));
	connect(this->OutputFile,               SIGNAL(released()),        this, SLOT(OutSelected()));
	connect(this->ResolutionLevel,          SIGNAL(editingFinished()), this, SLOT(LevelSelected()));
	connect(this->Neighbourhood,            SIGNAL(editingFinished()), this, SLOT(NeighbourhoodSelected()));
	connect(this->MaximumDistance,          SIGNAL(editingFinished()), this, SLOT(MaxDistChanged()));
	connect(this->buttonOk,                 SIGNAL(released()),        this, SLOT(OK()));
	connect(this->buttonCancel,             SIGNAL(released()),        this, SLOT(Cancel()));
	connect(this->buttonHelp,               SIGNAL(released()),        this, SLOT(Help()));
   
	if (!this->DEMflag) 
	{
		this->method = eMovingHzPlane;
		this->TriangulationRadioButton->setVisible(false);
	}

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}
//================================================================================

bui_ALSInterpolationDialog::bui_ALSInterpolationDialog(CXYZPointArray *Points, const char *title, 
													   const char *helptext, bool DEM) : 
				helpText(helptext), ALS(0), pmin(0,0), pmax(1,1), resolution(1,1), pulse(0), 
				neighbourhood(4), level(0), method(eMovingTiltedPlane), success(false),
				DEMflag(DEM), points(Points), maxDist(-1.0)
{
	setupUi(this);
	this->setWindowTitle(QString(title));
	
	connect(this->minX,                     SIGNAL(editingFinished()), this, SLOT(LimitsMinSelected()));
	connect(this->minY,                     SIGNAL(editingFinished()), this, SLOT(LimitsMinSelected()));
	connect(this->maxX,                     SIGNAL(editingFinished()), this, SLOT(LimitsMaxSelected()));
	connect(this->maxY,                     SIGNAL(editingFinished()), this, SLOT(LimitsMaxSelected()));
	connect(this->maxExtentsButton,         SIGNAL(released()),        this, SLOT(SetToMax()));
	connect(this->GResolution,              SIGNAL(editingFinished()), this, SLOT(GridSelected()));
	connect(this->MovingHZButton,           SIGNAL(clicked(bool)),     this, SLOT(InterpolationMethodSelected()));
	connect(this->MovingTiltedPlaneButton,  SIGNAL(clicked(bool)),     this, SLOT(InterpolationMethodSelected()));
	connect(this->TriangulationRadioButton, SIGNAL(clicked(bool)),     this, SLOT(InterpolationMethodSelected()));
	connect(this->FirstPulseButton,        SIGNAL(clicked(bool)),     this, SLOT(PulseSelected()));
	connect(this->LastPulseButton,         SIGNAL(clicked(bool)),     this, SLOT(PulseSelected()));
	connect(this->OutputFile,              SIGNAL(released()),        this, SLOT(OutSelected()));
	connect(this->ResolutionLevel,         SIGNAL(editingFinished()), this, SLOT(LevelSelected()));
	connect(this->Neighbourhood,           SIGNAL(editingFinished()), this, SLOT(NeighbourhoodSelected()));
	connect(this->MaximumDistance,         SIGNAL(editingFinished()), this, SLOT(MaxDistChanged()));
	connect(this->buttonOk,                SIGNAL(released()),        this, SLOT(OK()));
	connect(this->buttonCancel,            SIGNAL(released()),        this, SLOT(Cancel()));
	connect(this->buttonHelp,              SIGNAL(released()),        this, SLOT(Help()));
   
	if (!this->DEMflag) this->method = eMovingHzPlane;

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->init();
}

//================================================================================

bui_ALSInterpolationDialog::~bui_ALSInterpolationDialog(void)
{
	this->ALS = 0;
	this->points = 0;
}

//================================================================================

void bui_ALSInterpolationDialog::init()
{
	QString dt=QString("%1").arg(resolution.x, 0, 'f', 3);
	this->GResolution->setText(dt);
	this->SetToMax();

	if (this->ALS)
	{
		this->FirstPulseButton->blockSignals(true);
		this->LastPulseButton->blockSignals(true);
		this->ResolutionLevel->blockSignals(true);

		if (this->pulse == 0) 
		{
			this->FirstPulseButton->setChecked(true);
			this->LastPulseButton->setChecked(false);
		}
		else
		{
			this->FirstPulseButton->setChecked(false);
			this->LastPulseButton->setChecked(true);
		}

		dt=QString("%1").arg(double(this->level), 0, 'f', 0);
		this->ResolutionLevel->setText(dt);

		this->FirstPulseButton->blockSignals(false);
		this->LastPulseButton->blockSignals(false);
		this->ResolutionLevel->blockSignals(false);
	}
	else 
	{
		this->FirstLastGroupBox->setVisible(false);
		this->ResolutionLevel->setVisible(false);
		this->Resolution_LevelLabel->setVisible(false);
	}


	this->MovingHZButton->blockSignals(true);
	this->MovingTiltedPlaneButton->blockSignals(true);

	if (this->method == eMovingHzPlane) 
	{
		this->MovingHZButton->setChecked(true);
		this->MovingTiltedPlaneButton->setChecked(false);
	}
	else
	{
		this->MovingHZButton->setChecked(false);
		this->MovingTiltedPlaneButton->setChecked(true);
	}

	if (!this->DEMflag) 
	{
		this->MovingHZButton->setEnabled(false);
		this->MovingTiltedPlaneButton->setEnabled(false);
	}

	this->MovingHZButton->blockSignals(false);
	this->MovingTiltedPlaneButton->blockSignals(false);

	dt=QString("%1").arg(double(this->neighbourhood), 0, 'f', 0);
	this->Neighbourhood->setText(dt);

	this->MaximumDistance->blockSignals(true);
	dt=QString("%1").arg( double(this->maxDist), 0, 'f', 2);
	this->MaximumDistance->setText(dt);
	this->MaximumDistance->blockSignals(false);

	if (this->ALS) this->filename = this->ALS->getFileName();
	else if (this->points) this->filename = *this->points->getFileName();
	if (this->DEMflag) this->filename += ".dem";
	else this->filename += ".tif";

	this->OutputFileName->setText(QString(this->filename.GetChar()));

	this->buttonOk->setEnabled(true);
}

//================================================================================

void bui_ALSInterpolationDialog::destroy()
{
}

//================================================================================

void bui_ALSInterpolationDialog::SetToMax()
{
	C3DPoint p1(FLT_MAX, FLT_MAX, 0), p2(-FLT_MAX, -FLT_MAX, 0);
	if (this->ALS)
	{
		p1 = this->ALS->getCALSData()->getPMin();
		p2 = this->ALS->getCALSData()->getPMax();
	}
	else 
	{
		for (int i = 0; i < this->points->GetSize(); ++i)
		{
			CObsPoint *p = this->points->GetAt(i);
			double x = (*p)[0], y = (*p)[1];
			if (x < p1.x) p1.x = x;
			if (y < p1.y) p1.y = y;
			if (x > p2.x) p2.x = x;
			if (y > p2.y) p2.y = y;
		}
	}

	this->pmin.x = floor(p1.x / this->resolution.x) * this->resolution.x;
	this->pmin.y = floor(p1.y / this->resolution.y) * this->resolution.y;
	this->pmax.x = floor(p2.x / this->resolution.x + 1.0) * this->resolution.x;
	this->pmax.y = floor(p2.y / this->resolution.y + 1.0) * this->resolution.y;

	this->minX->blockSignals(true);
	this->minY->blockSignals(true);
	this->maxX->blockSignals(true);
	this->maxY->blockSignals(true);
	
	QString ds=QString("%1").arg( this->pmin.x, 0, 'f', 3);
	this->minX->setText(ds);
	ds=QString("%1").arg( this->pmin.y, 0, 'f', 3);
	this->minY->setText(ds);
	ds=QString("%1").arg( this->pmax.x, 0, 'f', 3);
	this->maxX->setText(ds);
	ds=QString("%1").arg( this->pmax.y, 0, 'f', 3);
	this->maxY->setText(ds);

	this->minX->blockSignals(false);
	this->minY->blockSignals(false);
	this->maxX->blockSignals(false);
	this->maxY->blockSignals(false);
};

//================================================================================

void bui_ALSInterpolationDialog::LimitsMinSelected()
{
	this->minX->blockSignals(true);
	this->minY->blockSignals(true);
	this->maxX->blockSignals(true);
	this->maxY->blockSignals(true);

	bool OK;
	double d = this->minX->text().toDouble(&OK);
	if (OK) this->pmin.x = d;
	QString ds=QString("%1").arg( this->pmin.x, 0, 'f', 3);
	this->minX->setText(ds);

	d = this->minY->text().toDouble(&OK);
	if (OK) this->pmin.y = d;
	ds=QString("%1").arg( this->pmin.y, 0, 'f', 3);
	this->minY->setText(ds);

	if (this->pmin.x >= this->pmax.x)
	{
		this->pmax.x = this->pmin.x + this->resolution.x;
		ds=QString("%1").arg( this->pmax.x, 0, 'f', 3);
		this->maxX->setText(ds);
	}

	if (this->pmin.y >= this->pmax.y)
	{
		this->pmax.y = this->pmin.y + this->resolution.y;
		ds=QString("%1").arg( this->pmax.y, 0, 'f', 3);
		this->maxY->setText(ds);
	}

	this->minX->blockSignals(false);
	this->minY->blockSignals(false);
	this->maxX->blockSignals(false);
	this->maxY->blockSignals(false);
};

//================================================================================

void bui_ALSInterpolationDialog::LimitsMaxSelected()
{
	this->minX->blockSignals(true);
	this->minY->blockSignals(true);
	this->maxX->blockSignals(true);
	this->maxY->blockSignals(true);

	bool OK;
	
	double d = this->maxX->text().toDouble(&OK);
	if (OK) this->pmax.x = d;
	QString ds=QString("%1").arg( this->pmax.x, 0, 'f', 3);
	this->maxX->setText(ds);

	d = this->maxY->text().toDouble(&OK);
	if (OK) this->pmax.y = d;
	ds=QString("%1").arg( this->pmax.y, 0, 'f', 3);
	this->maxY->setText(ds);

	if (this->pmin.x >= this->pmax.x)
	{
		this->pmin.x = this->pmax.x - this->resolution.x;
		ds=QString("%1").arg( this->pmin.x, 0, 'f', 3);
		this->minX->setText(ds);
	}

	if (this->pmin.y >= this->pmax.y)
	{
		this->pmin.y = this->pmin.y - this->resolution.y;
		ds=QString("%1").arg( this->pmin.y, 0, 'f', 3);
		this->minY->setText(ds);
	}

	this->minX->blockSignals(false);
	this->minY->blockSignals(false);
	this->maxX->blockSignals(false);
	this->maxY->blockSignals(false);
};

//================================================================================

void bui_ALSInterpolationDialog::GridSelected()
{
	this->GResolution->blockSignals(true);
	bool OK;
	double d = this->GResolution->text().toDouble(&OK);
	if (OK) 
	{
		if (d > 0.0) this->resolution.x = d;
		this->resolution.y = this->resolution.x;

	}

	QString ds=QString("%1").arg( this->resolution.x, 0, 'f', 3);
	this->GResolution->setText(ds);

	this->GResolution->blockSignals(false);
}

//================================================================================

void bui_ALSInterpolationDialog::LevelSelected()
{
	this->Neighbourhood->blockSignals(true);
	bool OK;
	int d = this->Neighbourhood->text().toInt(&OK);
	if (OK) 
	{
		if (d > 0) this->neighbourhood = d;
	}
	QString ds=QString("%1").arg( double(this->neighbourhood), 0, 'f', 0);
	this->Neighbourhood->setText(ds);

	this->Neighbourhood->blockSignals(false);
}

//================================================================================

void bui_ALSInterpolationDialog::MaxDistChanged()
{
	this->MaximumDistance->blockSignals(true);
	bool OK;
	int d = this->MaximumDistance->text().toInt(&OK);
	if (OK) 
	{
		if (d > FLT_EPSILON) this->maxDist = d;
		else this->maxDist = -1.0;
	}
	QString ds=QString("%1").arg( double(this->maxDist), 0, 'f', 2);
	this->MaximumDistance->setText(ds);

	this->MaximumDistance->blockSignals(false);
}

//================================================================================

void bui_ALSInterpolationDialog::NeighbourhoodSelected()
{
	this->ResolutionLevel->blockSignals(false);
	bool OK;
	int d = this->ResolutionLevel->text().toInt(&OK);
	if (OK) 
	{
		if (d >= 0) this->level = d;
	}
	QString ds=QString("%1").arg( double(this->level), 0, 'f', 0);
	this->ResolutionLevel->setText(ds);

	this->ResolutionLevel->blockSignals(false);
}

//================================================================================

void bui_ALSInterpolationDialog::PulseSelected()
{
	this->FirstPulseButton->blockSignals(true);
	this->LastPulseButton->blockSignals(true);

	if (this->pulse == 0) 
	{
		this->FirstPulseButton->setChecked(false);
		this->LastPulseButton->setChecked(true);
		this->pulse = 1;
	}
	else
	{
		this->FirstPulseButton->setChecked(true);
		this->LastPulseButton->setChecked(false);
		this->pulse = 0;
	}

	this->FirstPulseButton->blockSignals(false);
	this->LastPulseButton->blockSignals(false);
};

//================================================================================

void bui_ALSInterpolationDialog::InterpolationMethodSelected()
{
	if (this->method == eMovingTiltedPlane) 
	{
		if (this->MovingHZButton->isChecked()) this->method = eMovingHzPlane;
		else this->method = eTriangulation;
	}
	else if (this->method == eMovingHzPlane) 
	{
		if (this->MovingTiltedPlaneButton->isChecked()) this->method = eMovingTiltedPlane;
		else this->method = eTriangulation;
	}
	else
	{
		if (this->MovingHZButton->isChecked()) this->method = eMovingHzPlane;
		else this->method = eMovingTiltedPlane;
	}

	this->setInterpolationTypeGUI();
}

//================================================================================

void bui_ALSInterpolationDialog::setInterpolationTypeGUI()
{
	this->MovingHZButton->blockSignals(true);
	this->MovingTiltedPlaneButton->blockSignals(true);
	this->TriangulationRadioButton->blockSignals(true);

	if (this->method == eMovingTiltedPlane) 
	{
		this->MovingHZButton->setChecked(false);
		this->MovingTiltedPlaneButton->setChecked(true);
		this->TriangulationRadioButton->setChecked(false);
		this->MaximumDistance->setEnabled(true);
	}
	else if (this->method == eMovingHzPlane) 
	{
		this->MovingHZButton->setChecked(true);
		this->MovingTiltedPlaneButton->setChecked(false);
		this->TriangulationRadioButton->setChecked(false);
		this->MaximumDistance->setEnabled(true);
	}
	else
	{
		this->MovingHZButton->setChecked(false);
		this->MovingTiltedPlaneButton->setChecked(false);
		this->TriangulationRadioButton->setChecked(true);
		this->MaximumDistance->setEnabled(false);
	}

	this->MovingHZButton->blockSignals(false);
	this->MovingTiltedPlaneButton->blockSignals(false);
	this->TriangulationRadioButton->blockSignals(false);
};

//================================================================================

void bui_ALSInterpolationDialog::OutSelected()
{
	QString fn;
	
	if (this->DEMflag) fn = QFileDialog::getSaveFileName(this, "Choose File",
														 inifile.getCurrDir(),	
														 "Fileformats (*.dem)",	
														 NULL);
	else  fn = QFileDialog::getSaveFileName(this, "Choose File",
											inifile.getCurrDir(),	
											"Fileformats (*.tif)",	
											NULL);

	CFileName testname((const char*)fn.toLatin1());

	if (!testname.IsEmpty())
	{
		this->filename = testname;
		this->OutputFileName->setText(QString(this->filename.GetChar()));
		this->buttonOk->setEnabled(true);
	}
}

//================================================================================

void bui_ALSInterpolationDialog::OK()
{
	this->success = true;
	this->destroy();
	this->close();
}

//================================================================================

void bui_ALSInterpolationDialog::Cancel()
{
	this->destroy();
	this->close();
}

//================================================================================

void bui_ALSInterpolationDialog::Help()
{
	CHtmlHelp::callHelp(helpText); 
};

//================================================================================
