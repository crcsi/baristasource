#include <float.h> 

#include "Bui_ALSFilter.h"

#include <qfiledialog.h> 
#include <QMessageBox> 

#include "Icons.h"
#include "VDEM.h"
#include "VALS.h"
#include "VImage.h"
#include "BaristaProject.h"

#include "ProgressThread.h"
#include "ALSFilter.h"

#include "SystemUtilities.h"
#include "BaristaHtmlHelp.h"
#include "ProtocolHandler.h"
#include "IniFile.h"

#include "BaristaProjectHelper.h"
#include "Bui_ProgressDlg.h"

#include "Bui_OutputModeHandler.h"

//=============================================================================

bui_ALSFilterDialog::bui_ALSFilterDialog(CVALS *ALS) : pALS(ALS), 
                    MinHeight(0.15f), structureForOpen(45.0f),
					computationOK(false), resolution(1.0,1.0), llPoint(0.0, 0.0),
					urPoint(1.0, 1.0), extentsInit(false), ALSfilter(0), sigmaALS(0.15),
					numberIterations(7), heightThresholdFilter(0.5f), structureForOpenDetail(15.0),
					numberPoints(16), MinimumBuildingHeight(2.0f), pOutPutHandler(0),
					alpha(0.01), maxObliqueness (75 * M_PI / 180.0)

{
	setupUi(this);
	connect(this->buttonOk,               SIGNAL(released()),               this, SLOT(OK()));
	connect(this->buttonCancel,           SIGNAL(released()),               this, SLOT(Cancel()));
	connect(this->buttonHelp,             SIGNAL(released()),               this, SLOT(Help()));
	connect(this->ALSSelector,            SIGNAL(currentIndexChanged(int)), this, SLOT(DSMLastSALSSelectedelected()));
	connect(this->minHeightDifference,    SIGNAL(editingFinished()),        this, SLOT(minHeightChanged()));
	connect(this->filterSize,             SIGNAL(editingFinished()),        this, SLOT(structureForOpenChanged()));
	connect(this->NumberOfIterations,     SIGNAL(editingFinished()),        this, SLOT(NumberIterationsChanged()));
	connect(this->FilterSizeDetail,       SIGNAL(editingFinished()),        this, SLOT(FilterDetailChanged()));
	connect(this->NumberOfPoints,         SIGNAL(editingFinished()),        this, SLOT(NumberPointsChanged()));
	connect(this->SigmaALS,               SIGNAL(editingFinished()),        this, SLOT(SigmaALSChanged()));
	connect(this->alphaLineEdit,          SIGNAL(editingFinished()),        this, SLOT(AlphaChanged()));
	connect(this->MaxObliquenessLineEdit, SIGNAL(editingFinished()),        this, SLOT(MaxObliquenessChanged()));
	connect(this->HeightThresholdFilter,  SIGNAL(editingFinished()),        this, SLOT(HeightThresholdFilterChanged()));
	connect(this->MinBuildHeight,         SIGNAL(editingFinished()),        this, SLOT(MinBuildHeightChanged()));
	connect(this->buildingLLX,            SIGNAL(editingFinished()),        this, SLOT(LLChanged()));
	connect(this->buildingLLY,            SIGNAL(editingFinished()),        this, SLOT(LLChanged()));
	connect(this->buildingURX,            SIGNAL(editingFinished()),        this, SLOT(RUChanged()));
	connect(this->buildingURY,            SIGNAL(editingFinished()),        this, SLOT(RUChanged()));
	connect(this->buildingGResolution,    SIGNAL(editingFinished()),        this, SLOT(resolutionChanged()));
	connect(this->maxExtentsButton,       SIGNAL(released()),               this, SLOT(maxExtentsSelected()));
	connect(this->OutputLevelMin,         SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMed,         SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->OutputLevelMax,         SIGNAL(clicked(bool)),            this, SLOT(OutputLevelChanged()));
	connect(this->Explore,                SIGNAL(released()),               this, SLOT(explorePathSelected()));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));


	if (pALS)
	{
		this->resolution.x = floor (this->pALS->getCALSData()->getPointDist().x + this->pALS->getCALSData()->getPointDist().y + 0.5) * 0.5;
		if (this->resolution.x < 0.5) this->resolution.x = 0.5;
		this->resolution.y = this->resolution.x;
		this->llPoint.x = floor (this->pALS->getCALSData()->getPMin().x);
		this->llPoint.y = floor (this->pALS->getCALSData()->getPMin().y);			
		this->urPoint.x = floor (this->pALS->getCALSData()->getPMax().x + 0.5);		
		this->urPoint.y = floor (this->pALS->getCALSData()->getPMax().y + 0.5);		
		this->extentsInit = true;
	}

	this->init();
}

//=============================================================================

bui_ALSFilterDialog::~bui_ALSFilterDialog(void)
{
	if (this->pOutPutHandler) delete this->pOutPutHandler;
	this->pOutPutHandler = 0;

	if (this->ALSfilter) delete this->ALSfilter;
	this->ALSfilter = 0;
	this->pALS            = 0;
}

//=============================================================================

void bui_ALSFilterDialog::init()
{
	currentDir = inifile.getCurrDir();

	this->outputDir = this->currentDir;
	if (!this->outputDir.IsEmpty())
	{
		if (this->outputDir[this->outputDir.GetLength() - 1] != CSystemUtilities::dirDelim)
			this->outputDir += CSystemUtilities::dirDelimStr;
	}
	this->initParameters(project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "ALSFilter.ini");

	this->outputDir += "DEM" + CSystemUtilities::dirDelimStr;

	this->outputFileName = this->outputDir + "DTM.tif";

	this->outputFile->setText(this->outputFileName.GetChar());

	
	setupALSSelector(this->pALS);

	this->setCurrentExtentsAndResolution();
   
	this->minHeightDifference->blockSignals(true);
    this->filterSize->blockSignals(true);

	this->NumberOfIterations->blockSignals(true);
	this->SigmaALS->blockSignals(true);
	this->alphaLineEdit->blockSignals(true);
	this->MaxObliquenessLineEdit->blockSignals(true);

	this->HeightThresholdFilter->blockSignals(true);
	this->FilterSizeDetail->blockSignals(true);
	this->NumberOfPoints->blockSignals(true);
	this->MinBuildHeight->blockSignals(true);
	QString dt=QString("%1").arg(this->MinHeight, 0, 'f', 2);
    this->minHeightDifference->setText(dt);
	dt=QString("%1").arg(this->structureForOpen, 0, 'f', 2);
    this->filterSize->setText(dt);
	dt=QString("%1").arg(this->sigmaALS, 0, 'f', 3);
    this->SigmaALS->setText(dt);
	dt=QString("%1").arg(this->alpha * 100.0, 0, 'f', 2);
    this->alphaLineEdit->setText(dt);
	dt=QString("%1").arg(this->maxObliqueness / M_PI * 180.0, 0, 'f', 3);
    this->MaxObliquenessLineEdit->setText(dt);
	dt=QString("%1").arg(this->numberIterations);
    this->NumberOfIterations->setText(dt);
	dt=QString("%1").arg(this->heightThresholdFilter, 0, 'f', 3);
    this->HeightThresholdFilter->setText(dt);
	dt=QString("%1").arg(this->numberPoints);
    this->NumberOfPoints->setText(dt);
	dt=QString("%1").arg(this->structureForOpenDetail, 0, 'f', 2);
    this->FilterSizeDetail->setText(dt);
	dt=QString("%1").arg(this->MinimumBuildingHeight, 0, 'f', 2);
    this->MinBuildHeight->setText(dt);
	
	this->minHeightDifference->blockSignals(false);
    this->filterSize->blockSignals(false);
	this->NumberOfIterations->blockSignals(false);
	this->SigmaALS->blockSignals(false);
	this->HeightThresholdFilter->blockSignals(false);
	this->FilterSizeDetail->blockSignals(false);
	this->NumberOfPoints->blockSignals(false);
	this->MinBuildHeight->blockSignals(false);
	this->SigmaALS->blockSignals(false);
	this->alphaLineEdit->blockSignals(false);
	this->MaxObliquenessLineEdit->blockSignals(false);

	if (this->pOutPutHandler) delete this->pOutPutHandler;
	this->pOutPutHandler = new bui_OutputModeHandler(this->OutputLevelMin, this->OutputLevelMed,
							                         this->OutputLevelMax, 0);
	this->checkSetup();
}

//=============================================================================

void bui_ALSFilterDialog::explorePathSelected()
{
	QString fnQ = QFileDialog::getSaveFileName(this, "Choose a DEM file Name", this->outputDir.GetChar(), "DEM file (*.dem)");
    
	CFileName fn = ((const char*)fnQ.toLatin1());
		
	if (!fn.IsEmpty())
	{
		this->outputFileName = fn;
		this->outputDir = fn.GetPath();
		if (!this->outputDir.IsEmpty())
		{
			if (this->outputDir[this->outputDir.GetLength() - 1] != CSystemUtilities::dirDelim)
				this->outputDir += CSystemUtilities::dirDelimStr;

			this->outputFile->setText(this->outputFileName.GetChar());
		}
	}
};
  
//=============================================================================

void bui_ALSFilterDialog::setCurrentExtentsAndResolution()
{
	this->buildingGResolution->blockSignals(true);

	QString dt=QString("%1").arg(this->resolution.x, 0, 'f', 3);
	this->buildingGResolution->setText(dt);

	this->buildingGResolution->blockSignals(false);

	this->buildingLLY->blockSignals(true);
	this->buildingLLX->blockSignals(true);
	this->buildingURX->blockSignals(true);
	this->buildingURY->blockSignals(true);

	dt=QString("%1").arg(this->llPoint.x, 0, 'f', 3);
	this->buildingLLX->setText(dt);

	dt=QString("%1").arg(this->llPoint.y, 0, 'f', 3);
	this->buildingLLY->setText(dt);

	dt=QString("%1").arg(this->urPoint.x, 0, 'f', 3);
	this->buildingURX->setText(dt);

	dt=QString("%1").arg(this->urPoint.y, 0, 'f', 3);
	this->buildingURY->setText(dt);

	this->buildingLLY->blockSignals(false);
	this->buildingLLX->blockSignals(false);
	this->buildingURX->blockSignals(false);
	this->buildingURY->blockSignals(false);   
};

//=============================================================================

void bui_ALSFilterDialog::setupALSSelector(CVALS *ALS)
{
	this->ALSSelector->blockSignals(true);

	this->ALSSelector->clear();

	int indexSelected = 0;

	for (int i = 0; i < project.getNumberOfAlsDataSets(); ++i)
	{
		CVALS *als = project.getALSAt(i);
		this->ALSSelector->addItem(als->getFileName().GetChar());
		if (als == ALS) indexSelected = i;
	}

	this->ALSSelector->setCurrentIndex(indexSelected);
	this->ALSSelector->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::Cancel()
{
	this->close();
}

//=============================================================================

void bui_ALSFilterDialog::ALSSelected()
{
	int indexSelected = this->ALSSelector->currentIndex();

	this->pALS = project.getALSAt(indexSelected);
	
	this->checkSetup();
}

//=============================================================================

void bui_ALSFilterDialog::maxExtentsSelected()
{    
	if (this->pALS)
	{
		this->resolution.x = floor (this->pALS->getCALSData()->getPointDist().x * 100 + 0.5) * 0.01;
		this->resolution.y = this->resolution.x;
		this->llPoint.x = floor (this->pALS->getCALSData()->getPMin().x);
		this->llPoint.y = floor (this->pALS->getCALSData()->getPMin().y);			
		this->urPoint.x = floor (this->pALS->getCALSData()->getPMax().x) + 1.0;		
		this->urPoint.y = floor (this->pALS->getCALSData()->getPMax().y) + 1.0;		
		this->extentsInit = true;
	}
}

//=============================================================================
  
void bui_ALSFilterDialog::LLChanged()
{
	bool isDouble;

	double hlp = buildingLLX->text().toDouble(&isDouble);
	if (isDouble && fabs(llPoint.x - hlp) > FLT_EPSILON)
	{
		llPoint.x = hlp;
		if (urPoint.x <= llPoint.x) urPoint.x = llPoint.x + resolution.x;
		urPoint.x = llPoint.x + (floor((urPoint.x - llPoint.x) / resolution.x)) * resolution.x;
	}
 
	hlp = buildingLLY->text().toDouble(&isDouble);
	if (isDouble && fabs(llPoint.y - hlp) > FLT_EPSILON) 
	{
		llPoint.y = hlp;
		if (urPoint.y <= llPoint.y) urPoint.y = llPoint.y + resolution.y;
		urPoint.y = llPoint.y + (floor((urPoint.y - llPoint.y) / resolution.y)) * resolution.y;
	}
	
	this->setCurrentExtentsAndResolution();
};

//=============================================================================

void bui_ALSFilterDialog::RUChanged()
{
	bool isDouble;

	double hlp = this->buildingURX->text().toDouble(&isDouble);
	if (isDouble && fabs(urPoint.x - hlp) > FLT_EPSILON) 
	{
		urPoint.x = hlp;
		if (urPoint.x <= llPoint.x) llPoint.x = urPoint.x - resolution.x;
		llPoint.x = urPoint.x - (floor((urPoint.x - llPoint.x) / resolution.x)) * resolution.x;
	}

	hlp = buildingURY->text().toDouble(&isDouble);
	if (isDouble && fabs(urPoint.y - hlp) > FLT_EPSILON)
	{
		urPoint.y = hlp;
		if (urPoint.y <= llPoint.y) llPoint.y = urPoint.y - resolution.y;
		llPoint.y = urPoint.y - (floor((urPoint.y - llPoint.y) / resolution.y)) * resolution.y;
	}
	
	this->setCurrentExtentsAndResolution();
};

//=============================================================================

void bui_ALSFilterDialog::resolutionChanged()
{
	bool isDouble;

	double hlp = this->buildingGResolution->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0) && (fabs(this->resolution.x - hlp) > FLT_EPSILON))
	{
		this->resolution.x = hlp;
		this->resolution.y = hlp;
		urPoint.x = llPoint.x + (floor((urPoint.x - llPoint.x) / resolution.x)) * resolution.x;
		urPoint.y = llPoint.y + (floor((urPoint.y - llPoint.y) / resolution.y)) * resolution.y;
	}
  
	this->setCurrentExtentsAndResolution();
};

//=============================================================================

void bui_ALSFilterDialog::minHeightChanged()
{
	this->minHeightDifference->blockSignals(true);

	bool isDouble;

	double hlp = this->minHeightDifference->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)) this->MinHeight = hlp;
	
	QString dt=QString("%1").arg(this->MinHeight, 0, 'f', 2);
    this->minHeightDifference->setText(dt);
	
	this->minHeightDifference->blockSignals(false);   
};

//=============================================================================

void bui_ALSFilterDialog::structureForOpenChanged()
{
	this->filterSize->blockSignals(true);
	
	bool isDouble;

	double hlp = this->filterSize->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0)) this->structureForOpen = hlp;
	
	QString dt=QString("%1").arg(this->structureForOpen, 0, 'f', 2);
    this->filterSize->setText(dt);
	
	this->filterSize->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::SigmaALSChanged()
{
	this->SigmaALS->blockSignals(true);
	
	bool isDouble;

	double hlp = this->SigmaALS->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0)) this->sigmaALS = hlp;
	
	QString dt=QString("%1").arg(this->sigmaALS, 0, 'f', 3);
    this->SigmaALS->setText(dt);
	
	this->SigmaALS->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::AlphaChanged()
{
	this->alphaLineEdit->blockSignals(true);
	
	bool isDouble;

	double hlp = this->alphaLineEdit->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)&& (hlp < 100.0)) this->alpha = hlp / 100.0;
	
	QString dt=QString("%1").arg(this->alpha * 100, 0, 'f', 2);
    this->alphaLineEdit->setText(dt);
	
	this->alphaLineEdit->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::MaxObliquenessChanged()
{
	this->MaxObliquenessLineEdit->blockSignals(true);
	
	bool isDouble;

	double hlp = this->MaxObliquenessLineEdit->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)&& (hlp < 90.0)) this->maxObliqueness = hlp / 180.0 * M_PI;
	
	QString dt=QString("%1").arg(this->maxObliqueness * 180 / M_PI, 0, 'f', 2);
    this->MaxObliquenessLineEdit->setText(dt);
	
	this->MaxObliquenessLineEdit->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::MinBuildHeightChanged()
{
	this->MinBuildHeight->blockSignals(true);
	
	bool isDouble;

	double hlp = this->MinBuildHeight->text().toDouble(&isDouble);
	if ((isDouble) && (hlp >= 0.0)) this->MinimumBuildingHeight = hlp;
	
	QString dt=QString("%1").arg(this->MinimumBuildingHeight, 0, 'f', 2);
    this->MinBuildHeight->setText(dt);
	
	this->MinBuildHeight->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::HeightThresholdFilterChanged()
{
	this->HeightThresholdFilter->blockSignals(true);
	
	bool isDouble;

	double hlp = this->HeightThresholdFilter->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.0)) this->heightThresholdFilter = hlp;
	
	QString dt=QString("%1").arg(this->heightThresholdFilter, 0, 'f', 3);
    this->HeightThresholdFilter->setText(dt);
	
	this->HeightThresholdFilter->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::NumberIterationsChanged()
{
	this->NumberOfIterations->blockSignals(true);
	
	bool isInt;

	int hlp = this->NumberOfIterations->text().toInt(&isInt);
	if ((isInt) && (hlp > 0)) this->numberIterations = hlp;
	
	QString dt=QString("%1").arg(this->numberIterations);
    this->NumberOfIterations->setText(dt);
	
	this->NumberOfIterations->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::FilterDetailChanged()
{
	this->FilterSizeDetail->blockSignals(true);
	
	bool isDouble;

	double hlp = this->FilterSizeDetail->text().toDouble(&isDouble);
	if ((isDouble) && (hlp > 0.01)) this->structureForOpenDetail = hlp;
	
	QString dt=QString("%1").arg(this->structureForOpenDetail, 0, 'f', 2);
    this->FilterSizeDetail->setText(dt);
	
	this->FilterSizeDetail->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::NumberPointsChanged()
{
	this->NumberOfPoints->blockSignals(true);
	
	bool isInt;

	int hlp = this->NumberOfPoints->text().toInt(&isInt);
	if ((isInt) && (hlp > 0)) this->numberPoints = hlp;
	
	QString dt=QString("%1").arg(this->numberPoints);
    this->NumberOfPoints->setText(dt);
	
	this->NumberOfPoints->blockSignals(false);
};

//=============================================================================

void bui_ALSFilterDialog::setCurrentDir(const CCharString &cdir)
{
	this->currentDir = cdir;
}

//=============================================================================

void bui_ALSFilterDialog::checkSetup()
{
	if (this->pALS)
	{
		this->buttonOk->setEnabled(true);
	}
	else
	{
		this->buttonOk->setEnabled(false);
	}
}

//=============================================================================

void bui_ALSFilterDialog::OutputLevelChanged()
{
	this->pOutPutHandler->reactOnButtonSelected();
}
	  
//=============================================================================

int bui_ALSFilterDialog::getStruct(float size)
{
	size /= this->resolution.x;

	int structSize = floor(size);

	return structSize;
};

//=============================================================================

void bui_ALSFilterDialog::OK()
{
	if (this->ALSfilter) delete this->ALSfilter;
	CFileName fn(this->outputFileName);
	CCharString path = fn.GetPath() + CSystemUtilities::dirDelimStr;
	CSystemUtilities::createDirectoryTree(path);

	protHandler.open(CProtocolHandler::prt, this->outputDir + "ALSFilter.prt", false);
		
	ostrstream oss; oss.setf(ios::fixed, ios::floatfield); oss.precision(2);
	oss << "\nCommencing Filtering of ALS data\n================================\n\n   Time: " 
		<< CSystemUtilities::timeString().GetChar() << "\n";

	protHandler.print(oss, CProtocolHandler::prt);


	CVDEM *newDEM = project.addDEM(this->outputFileName);

	CCharString str = *newDEM->getFileName() + ".hug";
	newDEM->getDEM()->setFileName(str.GetChar());
	C2DPoint shift(this->llPoint.x, this->urPoint.y);
	newDEM->getDEM()->setTFW(shift, this->resolution);
	newDEM->setReferenceSystem(this->pALS->getReferenceSystem());

	this->ALSfilter = new CALSFilter(this->pALS->getCALSData(), newDEM->getDEM(), this->MinimumBuildingHeight,
		                             this->MinHeight, this->sigmaALS, this->structureForOpen, 
									 this->structureForOpenDetail, 									 
									 this->heightThresholdFilter, this->alpha, this->maxObliqueness, 
									 this->numberIterations, this->numberPoints,
									 this->llPoint, this->urPoint, 
									 this->resolution, this->outputDir);

	this->writeParameters(project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + "ALSFilter.ini");

		
	bui_ProgressDlg dlg(new CALSFilterThread(this), this->ALSfilter, "ALS Filtering ...");
	dlg.exec();
	this->computationOK = dlg.getSuccess();
	newDEM->getDEM()->setTFW(shift, this->resolution);
	newDEM->setReferenceSystem(this->pALS->getReferenceSystem());

	if (!this->computationOK) project.deleteDEM(newDEM);

	baristaProjectHelper.refreshTree();

	protHandler.close(CProtocolHandler::prt);

	this->close();
}

//=============================================================================

bool bui_ALSFilterDialog::filter()
{
	bool ret = true;
	if (!this->ALSfilter) ret = false;
	else
	{
		if (!this->ALSfilter->isInitialised())
		{
			QMessageBox::warning( this, " Cannot filter ALS data!", " Error initialising data!");
			ret = false;

		}
		else
		{
			ret = this->ALSfilter->filter();
		}
	}

	return ret;
};

//=============================================================================

void bui_ALSFilterDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/BuildingDetector.html");
};


//=============================================================================

void bui_ALSFilterDialog::writeParameters(const CCharString &filename) const
{
	ofstream outFile(filename.GetChar());
	if (outFile.good())
	{
	
		outFile.setf(ios::fixed, ios::floatfield);
		outFile.precision(5);
	
		outFile << "MinHeight("                 << this->MinHeight
			    << ")\nMinimumBuildingHeight("  << this->MinimumBuildingHeight
				<< ")\nstructureForOpen("       << this->structureForOpen
				<< ")\nsigmaALS("               << this->sigmaALS
				<< ")\nalpha("                  << this->alpha
				<< ")\nmaxObliqueness("         << this->maxObliqueness
				<< ")\nheightThresholdFilter("  << this->heightThresholdFilter
				<< ")\nnumberIterations("       << this->numberIterations
				<< ")\nstructureForOpenDetail(" << this->structureForOpenDetail
			    << ")\nnumberPoints("           << this->numberPoints
				<< ")\n";
	}
};

//=============================================================================

void bui_ALSFilterDialog::initParameters(const CCharString &filename)
{
	ifstream inFile(filename.GetChar());
	int len = 2048;
	char *z = new char[len];
	if (inFile.good())
	{
		inFile.getline(z, len);

		while (inFile.good())
		{
			CCharString arg(z);
			int pos1 = arg.Find("(");
			int pos2 = arg.Find(")");
			if ((pos1 > 0) && (pos2 > pos1))
			{
				CCharString key = arg.Left(pos1);
				arg = arg.Left(pos2);
				arg = arg.Right(arg.GetLength() - pos1 - 1);
				if (key == "MinHeight")
				{
					istrstream iss(arg.GetChar());
					iss >> this->MinHeight;
				}
				else if (key == "MinimumBuildingHeight")
				{
					istrstream iss(arg.GetChar());
					iss >> this->MinimumBuildingHeight;
				}
				else if (key == "structureForOpen")
				{
					istrstream iss(arg.GetChar());
					iss >> this->structureForOpen;
				}
				else if (key == "sigmaALS")
				{
					istrstream iss(arg.GetChar());
					iss >> this->sigmaALS;
				}
				else if (key == "alpha")
				{
					istrstream iss(arg.GetChar());
					iss >> this->alpha;
				}
				else if (key == "maxObliqueness")
				{
					istrstream iss(arg.GetChar());
					iss >> this->maxObliqueness;
				}
				else if (key == "heightThresholdFilter")
				{
					istrstream iss(arg.GetChar());
					iss >> this->heightThresholdFilter;
				}
				else if (key == "numberIterations")
				{
					istrstream iss(arg.GetChar());
					iss >> this->numberIterations;
				}
				else if (key == "structureForOpenDetail")
				{
					istrstream iss(arg.GetChar());
					iss >> this->structureForOpenDetail;
				}
				else if (key == "numberPoints")
				{
					istrstream iss(arg.GetChar());
					iss >> this->numberPoints;
				}
			
			}
			inFile.getline(z, len);
		}
	}
	delete [] z;
};

//=============================================================================

