#include "DEMView.h"

#include <QKeyEvent>
#include <QMessageBox>
#include <QString>
#include <qpainter.h>

#include "VDEM.h"
#include "BaristaProject.h"
#include "Bui_CropROIDlg.h"
#include "QTImageHelper.h"
#include "LookupTable.h"

CDEMView::CDEMView(CVDEM* vdem, QWidget * parent) :
        CBaristaView(vdem->getItemText(),*vdem->getDEM()->getTransferFunction(),
		             -0.5f, double(vdem->getDEM()->getWidth()), 
					 -0.5f, double(vdem->getDEM()->getHeight()), 
				      0.5f, 0.5f, parent), vdem(vdem),startRow(2),startCol(2),endRow(2),endCol(2),
					  centre(2)
{
	// we have to do that after the constructor of the BaristaView is finished,
	// so that this is really an dem view!!
	this->initEventHandler();

	this->vdem->addListener(this);
	
	this->createDEMStatistics(this->vdem->getDEMStatisticts(),
							  this->vdem->getNStatisticRows(),
							  this->vdem->getNStatisticCols());

}

CDEMView::~CDEMView(void)
{
}


CVDEM* CDEMView::getVDEM() const
{
	return this->vdem;
}

CHugeImage * CDEMView::getHugeImage() const
{ 
	return this->vdem->getDEM(); 
}

const CSensorModel* CDEMView::getCurrentSensorModel()
{
	if (this->vdem)
		return this->vdem->getDEM()->getTFW();
	else
		return NULL;
}

void CDEMView::drawContents ( QPainter &p)
{
	if (this->vdem == NULL)
		return;

	this->drawContentsPre(p);

	int nRows = this->vdem->getNStatisticRows();
	int nCols = this->vdem->getNStatisticCols();
	int fontSize = this->vdem->getStatFontSize();
	int textSpace = 12;
	

	if (nRows != 0 && nCols != 0 )
	{
		double xscreen, yscreen;

		for (int r=0; r< nRows; r++)
		{
			
			for (int c=0; c < nCols; c++)
			{
				CObsPoint* cen = this->centre.GetAt(r*nCols + c);
				
				int textStartPos = -24;
				if (toScreen((*cen)[0], (*cen)[1], xscreen, yscreen))
				{
					const int *demC = this->vdem->getStatTextColor();
					QColor col(demC[0], demC[1], demC[2]);
					QPen pen(col, 3);

					p.setFont(QFont("Courier",fontSize , 75));

					p.setPen(pen);

					if (this->vdem->getShowAvg())
					{
						p.drawText(xscreen -50,yscreen + textStartPos, this->avgText.at(r*nCols + c));
						textStartPos +=textSpace;
					}
					
					if (this->vdem->getShowDevAvg())
					{
						p.drawText(xscreen -50,yscreen + textStartPos, this->rmsText.at(r*nCols + c));
						textStartPos +=textSpace;
					}

					if (this->vdem->getShowMin())
					{
						p.drawText(xscreen -50,yscreen + textStartPos, this->minText.at(r*nCols + c));
						textStartPos +=textSpace;
					}

					if (this->vdem->getShowMax())
					{
						p.drawText(xscreen -50,yscreen +textStartPos, this->maxText.at(r*nCols + c));
						textStartPos +=textSpace;
					}

					if (this->vdem->getShowDev())
					{
						p.drawText(xscreen -50,yscreen +textStartPos, this->devText.at(r*nCols + c));
						textStartPos +=textSpace;
					}

				}
			}
		}
	

		const int *slc = this->vdem->getStatLineColor();
		QColor qc(slc[0],slc[1],slc[2]);
		for (int i=0; i < this->startCol.GetSize(); i++)
		{
			CObsPoint* pS = this->startCol.GetAt(i);
			CObsPoint* pE = this->endCol.GetAt(i);
			this->drawLine(p,pS,pE,qc);
		}

		for (int i=0; i < this->startRow.GetSize(); i++)
		{
			CObsPoint* pS = this->startRow.GetAt(i);
			CObsPoint* pE = this->endRow.GetAt(i);
			this->drawLine(p,pS,pE,qc);
		}	
	}

	this->drawContentsPost(p);
}


/**
 * handle mouse move event
 * @param e 
 */
void CDEMView::mouseMoveEvent ( QMouseEvent * e )
{    
	CBaristaView::mouseMoveEvent(e);
	this->update();
}



void CDEMView::keyReleaseEvent ( QKeyEvent * e )
{

	CBaristaView::keyReleaseEvent(e);
	this->update();
}



void CDEMView::createPopUpMenu()
{
	CBaristaView::createPopUpMenu();
}



void CDEMView::getPixelInfoHugeImage(QString& pixelInfo,
										 QString& geoInfo,
										 QString& colorInfo,
										 QString& zoomInfo,
										 QString& residualInfo,
										 QString& brightnessInfo,
										 CHugeImage* himage)
{

	CBaristaView::getPixelInfoHugeImage(pixelInfo,geoInfo,colorInfo,zoomInfo,residualInfo,brightnessInfo,himage);

	// if tfw then show georeferenced position
	CTFW* tfw = this->vdem->getDEM()->getTFW();
	CXYZPoint geo;
	
	if ( tfw->transformObs2Obj(geo,cpos))
	{
		if ( tfw->getRefSys().getCoordinateType() == eGEOGRAPHIC )
			geoInfo.sprintf(" [geo]: %.8f, %.8lf",geo.x,geo.y);			
		else
			geoInfo.sprintf(" [geo]: %.3lf, %.3lf",geo.x,geo.y);
	}

	pixelInfo.append(colorInfo);
	pixelInfo.append(geoInfo);
	pixelInfo.append(zoomInfo);
	pixelInfo.append(residualInfo);
	pixelInfo.append(brightnessInfo);
}


void CDEMView::closeEvent ( QCloseEvent * e )
{
	this->vdem->removeListener(this);
	this->vdem->getDEM()->resetTiles();
     
	CBaristaView::closeEvent(e);	
}

void CDEMView::elementDeleted(CLabel element)
{
	if (element == "DEMSTATISTICS")
		this->deleteDEMStatistics();

	this->update();
}


void CDEMView::elementsChanged(CLabel element)
{

	CBaristaView::elementsChanged();

	if (element == "DEMSTATISTICS")
		this->createDEMStatistics(this->vdem->getDEMStatisticts(),
								  this->vdem->getNStatisticRows(),
								  this->vdem->getNStatisticCols());


	this->setWindowTitle(this->vdem->getFileName()->GetChar());
	this->setAccessibleName(this->vdem->getFileName()->GetChar());
	this->update();

}

const CTFW* CDEMView::getTFW() const 
{
	return this->vdem->getDEM()->getTFW();
}


void CDEMView::CropROI()
{
	Bui_CropROIDlg cropdlg(this,"Crop from DEM", "UserGuide/DEMView.html#DEMROI",false,false,true,false,false);
	cropdlg.exec();
}

void CDEMView::ResetRoi()
{
	if (this->vdem)
	{
		int w = abs(this->roiLR.x - this->roiUL.x + 1);
		int h = abs(this->roiUL.y - this->roiLR.y + 1);

		if (this->roiUL.x + w > this->vdem->getDEM()->getWidth())  w = this->vdem->getDEM()->getWidth()  - this->roiUL.x;
		if (this->roiUL.y + h > this->vdem->getDEM()->getHeight()) h = this->vdem->getDEM()->getHeight() - this->roiUL.y;
		
		CImageBuffer buf(this->roiUL.x, this->roiUL.y, w, h, 1, 32, false, -9999);
		
		this->vdem->getDEM()->setRect(buf);
	}
};


bool CDEMView::createDEMStatistics(const CObsPointArray& demStatistics,int nRows,int nCols)
{
	
	if (nRows == 0 || nCols == 0 || demStatistics.GetSize() == 0)
		return false;

	int height = this->vdem->getDEM()->getHeight();
	int width = this->vdem->getDEM()->getWidth();

	int rowHeight = height / nRows;
	int colWidth = width / nCols;

	for (int i=1; i < nCols; i++)
	{
		CObsPoint* pS = this->startCol.Add();
		CObsPoint* pE = this->endCol.Add();
		(*pS)[0] = colWidth * i;
		(*pS)[1] = 0;
		(*pE)[0] = colWidth * i;
		(*pE)[1] = height;
	}

	for (int i=1; i < nRows; i++)
	{
		CObsPoint* pS = this->startRow.Add();
		CObsPoint* pE = this->endRow.Add();
		(*pS)[0] = 0;
		(*pS)[1] = rowHeight * i;
		(*pE)[0] = width;
		(*pE)[1] = rowHeight * i;
	}


	//const CObsPointArray& demStatistics = this->vdem->getDEMStatisticts();

	int centerC;
	int centerR;

	for (int r=0; r< nRows; r++)
	{
		centerR = rowHeight / 2 + rowHeight * r;
		for (int c=0; c < nCols; c++)
		{
			centerC = colWidth / 2 + colWidth * c;
			const CObsPoint* stat = demStatistics.GetAt(r*nCols + c);
			QString text;
			text.sprintf("AVG: %7.2lf",(*stat)[0]);
			this->avgText.append(text);
			text.sprintf("RMS: %7.2lf",(*stat)[1]);
			this->rmsText.append(text);
			text.sprintf("MIN: %7.2lf",(*stat)[2]);
			this->minText.append(text);			
			text.sprintf("MAX: %7.2lf",(*stat)[3]);
			this->maxText.append(text);
			text.sprintf("DEV: %7.2lf",(*stat)[4]);
			this->devText.append(text);
			CObsPoint p(2);
			p[0] = centerC;
			p[1] = centerR;
			this->centre.Add(p);
		}
		
		

		
	}

	return true;
}


bool CDEMView::deleteDEMStatistics()
{
	this->startCol.RemoveAll();
	this->startRow.RemoveAll();
	this->endCol.RemoveAll();
	this->endRow.RemoveAll();
	this->centre.RemoveAll();

	this->avgText.clear();
	this->rmsText.clear();
	this->minText.clear();
	this->maxText.clear();
	

	return true;
}