#include <QApplication>
#include <QFile>
#include <QMessageBox>
#include <qmdiarea.h>
#include <qsplitter.h>


#include <ANN/ANN.h>

#include "BaristaTreeWidget.h"
#include "BaristaHtmlHelp.h"

#include "IniFile.h"
#include "Bui_MainWindow.h"


CBaristaTreeWidget* thumbnailView = NULL;
QFrame* tmpFrame = NULL;
QMdiArea* workspace = NULL;

int main( int argc, char ** argv )
{
	CFileName exePath(argv[0]);

	CHtmlHelp::fileNameSearch.SetPath(exePath.GetPath());
    if (QFile(inifile.getIniFileName()).exists())
    {
        inifile.readIniFile();
    }


    QApplication a( argc, argv );

	QFont currentFont = QApplication::font();
	currentFont.setFamily("Tahoma");
#ifdef WIN32
	currentFont.setPointSize(8);
#else
	currentFont.setPointSize(10);
#endif
	QApplication::setFont(currentFont);

	bui_MainWindow mw;

	thumbnailView = new CBaristaTreeWidget();
	thumbnailView->setHeaderLabels(QStringList("Items"));
	thumbnailView->setIndentation(25);
	thumbnailView->setIconSize(QSize(50,53));


	//thumbnailView->setRootIsDecorated(false);
	// adds plus/minus for expand/collpase
	thumbnailView->setRootIsDecorated(true);

	//thumbnailView->setUniformRowHeights(false);

	workspace = new QMdiArea();

	QGridLayout* gridLayout = new QGridLayout();
	gridLayout->addWidget(thumbnailView);
	gridLayout->addWidget(workspace);
	mw.splitter->setLayout(gridLayout);

	QList<int> sizeList;
	QSize size = mw.size();
	sizeList.append(size.width()*0.1);

	sizeList.append(size.width()*0.9);
	mw.splitter->setSizes(sizeList);

	mw.connect(thumbnailView, SIGNAL( itemDoubleClicked( QTreeWidgetItem*, int ) ), &mw, SLOT( thumbnailDblClick( QTreeWidgetItem*, int ) ) );
	mw.connect(thumbnailView, SIGNAL( itemClicked( QTreeWidgetItem*, int )),thumbnailView, SLOT( itemClickedSlot( QTreeWidgetItem*, int ) ) );
	mw.connect(thumbnailView, SIGNAL( itemDoubleClicked( QTreeWidgetItem*, int )),thumbnailView, SLOT( itemDoubleClickedSlot( QTreeWidgetItem*, int ) ) );
    mw.showMaximized();



	mw.validateBarista();


    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );

	if (argc >= 2 )
		mw.openFile(argv[1]);


	int rv = a.exec();

	annClose();

    return rv;
}

