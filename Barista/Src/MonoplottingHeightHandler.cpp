#include "MonoplottingHeightHandler.h"
#include "Bui_MainWindow.h"
#include <QMessageBox>
#include "XYZPoint.h"
#include "Bui_MeasureHeightDlg.h"
#include "BaristaProjectHelper.h"

CMonoplottingHeightHandler::CMonoplottingHeightHandler(void)
{
	this->statusBarText = "MonoplottingHeightMODE ";
}

CMonoplottingHeightHandler::~CMonoplottingHeightHandler(void)
{
}


bool CMonoplottingHeightHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{
	return CBaristaViewEventHandler::handleMouseMoveEvent(e, v);
};



bool CMonoplottingHeightHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	v->setShowRubberLine(true);	
	return false;
};

bool CMonoplottingHeightHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	if (v->getVImage())
	{
		if (!CMonoplottingHandler::handleLeftMouseReleaseEvent(e,v))
			return false;

		CXYPoint xy;
		if (!v->getMouseEventCoords(e, xy.x, xy.y))
			return false;

		//addition >>
		//// give a warning if reference sys of line and dem don't match
		if ( project.getNumberOfMonoplottedHeightLines() > 0 )
		{
			// check if coordinates are in the same coordinate system
			// and show at least a warning message
			eCoordinateType demtype = this->monoPlotter.getReferenceSystem().getCoordinateType();
			eCoordinateType monotype = project.getXYZHeightLines()->getReferenceSystem()->getCoordinateType();

			if (demtype != monotype)
			{
				QMessageBox::warning( v, "DEM and height lines are defined in different coordinate systems!"
				, "Coordinate definitions of height lines will be set to DEM parameters");
			}
		}
		////
		//set monoplotter & height lines to the same ref system
		project.setBheightLineReferenceSystem(this->monoPlotter.getReferenceSystem());
		//<< addition
	
		if ( v->getFirstClick().x == -1 )
		{
			v->setFirstClick(C2DPoint(xy.x, xy.y));
		}
		else if ( v->getFirstClick().x != -1 && v->getSecondClick().x == -1 )
		{
			v->setSecondClick(C2DPoint(xy.x, xy.y));
		}
		
		if ( v->getFirstClick().x != -1 && v->getSecondClick().x != -1 )
		{
			CXYPoint p1(v->getFirstClick()), p2(v->getSecondClick());
			p1.setSX(0.5); p1.setSY(0.5); 
			p2.setSX(0.5); p2.setSY(0.5);

			CXYZPoint bottom;
			CXYZPoint top;
		
			bool heightmeasured = this->monoPlotter.getVerticalPole(bottom, top, p1, p2);

			if (!heightmeasured)
			{
				QMessageBox::warning( v, "Monoplotting did not converge!"
				, "No height difference could be measured!");
				v->resetFirstSecondClick();

				return false;
			}

			// addition >>
			// create new linelabel and pointlabel
			//int pointNr = this->line.getPointCount();
			//CCharString lineLabel(this->currentLineLabel.GetChar());

			//if (pointNr == 0)	// get the label from the project for the first point
			//{
				// unselect every other line
				//project.getXYZHeightLines->unselectAll();
				
				//if (this->changedLineLabel.GetLength())
				//{
				//	this->currentLineLabel.Set(this->changedLineLabel);
				//}
				//else
				//{
					//this->currentLineLabel.Set(project.getCurrentBheightLabel());
					//this->changedLineLabel.Set(this->currentLineLabel); // init with default
				//}

				//this->currentVBase = v->getVBase();
				//this->line.setLabel(this->currentLineLabel);

				// add the datasets in all open image views
				//this->addDrawingDataSets(this->currentLineLabel);
				
				// don't show the selected points labels in the current image view
				//v->setShowPoints(this->currentLineLabel,true);
				//v->setShowPointLabels(this->currentLineLabel,false);	

				// buil the point label
				//CCharString lineString (this->currentLineLabel.GetChar());
				//CCharString pointString(lineString + "_P");
				//this->currentPointLabel = pointString.GetChar();
			//}

			CXYZPolyLine line;						
			CLabel lineLabel, botPLabel, upPLabel;
			lineLabel = project.getCurrentBheightLabel();
			line.setLabel(lineLabel);
			CCharString lineString(lineLabel.GetChar());
			CCharString botString(lineString + "_BPoint");
			botPLabel = botString.GetChar();
			CCharString upString(lineString + "_UPoint");
			upPLabel = upString.GetChar();
			if (bottom.z < top.z)
			{
				bottom.setLabel(botPLabel);
				top.setLabel(upPLabel);
			}
			else
			{
				bottom.setLabel(upPLabel);
				top.setLabel(botPLabel);
			}

			line.addPoint(bottom);
			line.addPoint(top);
			// add to project
			if (project.addBheightLine(line))
				baristaProjectHelper.refreshTree();
			//<< addition

			double height = top.z -  bottom.z;
			bui_MeasureHeightDlg heightdlg;
			heightdlg.setHeightValue( height);

			heightdlg.setModal(true);
			heightdlg.show();

			heightdlg.exec();

			v->resetFirstSecondClick();
		}
	}

	//addition >>
	//show changes in openned views
	baristaProjectHelper.setIsBusy(true);
	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i < n; i++)
	{
		redraw(this->baristaViews.getView(i));
	}
	baristaProjectHelper.setIsBusy(false);
	//<< addition

	return true;
};


bool CMonoplottingHeightHandler::redraw(CBaristaView *v)
{
	if (v->getVImage())
	{
		if (!CMonoplottingHandler::redraw(v))
			return false;
		//addition >>
		CXYZPolyLineArray* heightLines = project.getXYZHeightLines();
		int nLines = heightLines->GetSize();

		for (int k=0; k< nLines; k++)
		{
			CXYZPolyLine* pLine = heightLines->getAt(k);
			v->addDrawingDataSet(pLine->getLabel(),2,true,true);
			this->drawHeightLine(v,pLine);
		}
		//<< addition
	}
	return true;
};

//addition >>
void CMonoplottingHeightHandler::drawHeightLine(CBaristaView * v,CXYZPolyLine * pNewPolyline)
{
	
	CXYPolyLine new2DPolyLine;
	for (int i=0; i< pNewPolyline->getPointCount(); i++)
	{
		CXYPoint p;
		if (this->backprojectPoint(v,p,pNewPolyline->getPoint(i)))
			new2DPolyLine.addPoint(p);
	}

	if(pNewPolyline->isClosed())
		new2DPolyLine.closeLine();
		
	QColor color(project.getMonoplottedLineColor().x,project.getMonoplottedLineColor().y, project.getMonoplottedLineColor().z);
	v->addPolyLineDrawing(pNewPolyline->getLabel(),&new2DPolyLine,color,project.getMonoplottedLineWidth());


}
//<< addition

void CMonoplottingHeightHandler::open()
{
	baristaProjectHelper.setIsBusy(true);
	CBaristaViewEventHandler::open();
	CMonoplottingHandler::open();

	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->MeasureHeightsAction->setChecked(true);
	mainwindow->blockSignals(false);

	//addition >>
	this->initPolyline();
	project.getXYZHeightLines()->addListener(this);
	//<< addition

	int n = this->baristaViews.getNumberOfViews();

	for (int i=0; i < n; i++)
	{
		redraw(this->baristaViews.getView(i));
	}
	baristaProjectHelper.setIsBusy(false);
};

void CMonoplottingHeightHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->MeasureHeightsAction->setChecked(false);
	mainwindow->blockSignals(false);

	CBaristaViewEventHandler::close();
	CMonoplottingHandler::close();

	//addition >>
	project.getXYZHeightLines()->removeListener(this);
	//<< addition
};


//addition >>
void CMonoplottingHeightHandler::initPolyline()
{
	//this->line.deleteLine();
	//this->line.setLabel("");
	this->currentLineLabel = "";
	//this->currentPointLabel = "";
	//this->currentVBase = 0;
	//this->current2DPoints.RemoveAll();
	//this->changedLineLabel = "";
}

//<< addition