#include "Bui_ImageMergeDlg.h"

#include "SystemUtilities.h"
#include "BaristaHtmlHelp.h"
#include "BaristaProject.h"
#include "Bui_ProgressDlg.h"
#include <QMessageBox>
#include <QIntValidator>
#include <QTreeWidgetItem>
#include "Icons.h"
#include "GDALFile.h"
#include "Bui_GDALExportDlg.h"
#include "ProgressThread.h"

Bui_ImageMergeDlg::Bui_ImageMergeDlg(QWidget *parent) : success(false),selectedCamera(-1)
{
	this->setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->lE_FirstCol->setValidator(new QIntValidator(0,4000,this));
	this->lE_LastCol->setValidator(new QIntValidator(0,4000,this));

	this->tW_Files->setHeaderLabel("Available Files");

	this->connect(this->pB_Merge,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pB_Cancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pB_Help,SIGNAL(released()),this,SLOT(pBHelpReleased()));
	this->connect(this->pB_FileName,SIGNAL(released()),this,SLOT(pBFileReleased()));
	this->connect(this->cB_AutoAdjust,SIGNAL(stateChanged(int)),this,SLOT(cBAutoAdjustStateChanged(int)));
	
	this->connect(this->lE_FirstCol,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_LastCol,SIGNAL(textChanged(const QString &)),this,SLOT(leTextChanged(const QString &)));
	this->connect(this->lE_FirstCol,SIGNAL(editingFinished()),this,SLOT(editingFinishedFirstCol()));
	this->connect(this->lE_LastCol,SIGNAL(editingFinished()),this,SLOT(editingFinishedLastCol()));

	this->connect(this->rB_NearestNeighbour,SIGNAL(released()),this,SLOT(rBNearestNeighbourReleased()));
	this->connect(this->rB_BiLinear,SIGNAL(released()),this,SLOT(rBBiLinearReleased()));
	this->connect(this->rB_BiCubic,SIGNAL(released()),this,SLOT(rBBiCubicReleased()));

	this->connect(this->tW_Files,SIGNAL(itemSelectionChanged()),this,SLOT(tWFilesItemSelectionChanged()));
	this->connect(this->tW_Files,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(tWFilesItemChanged(QTreeWidgetItem*,int)));

	this->loadImages();
	this->checkSetup();
	this->success = true;
}

Bui_ImageMergeDlg::~Bui_ImageMergeDlg()
{
}


void Bui_ImageMergeDlg::pBOkReleased()
{

	// determine how many we are importing, just to show the user a number
	unsigned int n=0;
	unsigned int count=0;
	for (unsigned int group=0 ; group < this->data.size(); group++)
	{
		CGroupData& d = this->data[group];
		if (d.readyToImport && d.import)
			n++;
	}

	
	for (unsigned int group=0 ; group < this->data.size(); group++)
	{
		CGroupData& d = this->data[group];
		
		if (!d.readyToImport || ( d.readyToImport && !d.import))
		{
			if (d.mergedImage)
				project.deleteImage(d.mergedImage);
			d.mergedImage = NULL;
		}
		else
		{
			count++;
			CImageMerger merger;
			bool ret = merger.init(d.inputImages,d.firstCol,d.lastCol,d.cameraName,d.method,d.mergedImage);
		
			CCharString progressText,saveText;
			
			progressText.Format("Merging images %u/%u...",count,n);
			saveText.Format("Saving file on disk %u/%u...",count,n);

			if (ret)
			{
				bui_ProgressDlg dlg(new CImageMergeThread(&merger),&merger,progressText.GetChar());
				dlg.exec();

				this->success = dlg.getSuccess();

				if (!this->success)
				{
					QMessageBox::warning(this,"Error while merging images",merger.getErrorMessage().GetChar());
				}
			
				// write the file on disk
				d.exportDialog->writeSettings();
				d.exportDialog->writeImage(saveText.GetChar());

				d.mergedImage->setCurrentSensorModel(ePUSHBROOM);

			}
			else
				QMessageBox::warning(this,"Error while merging images",merger.getErrorMessage().GetChar());
		}
	}

	this->close();
}

void Bui_ImageMergeDlg::pBCancelReleased()
{
	this->success = false;

	for (unsigned int group=0; group < this->data.size(); group++)
	{
		CGroupData& d = this->data[group];

		if (d.mergedImage)
			project.deleteImage(d.mergedImage);
		d.mergedImage = NULL;
	}
	this->close();
}

void Bui_ImageMergeDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/AlosMergeDlg.html");
}

void Bui_ImageMergeDlg::pBFileReleased()
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
		return;
	
	CGroupData& d = this->data[this->selectedCamera];

	d.exportDialog->exec();
	if (d.exportDialog->getSuccess())
	{
		d.filename = d.exportDialog->getFilename();
		this->lE_FileName->setText(d.filename.GetChar());
		this->lE_FileName->setToolTip(d.filename.GetChar());
		d.mergedImage->setName(d.filename);
	}	

	this->checkSetup();

}

void Bui_ImageMergeDlg::cBAutoAdjustStateChanged(int state)
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
		return;
	
	CGroupData& d = this->data[this->selectedCamera];

	if (state == Qt::Checked)
	{
		d.autoAdjustBoundary = true;
		this->lE_FirstCol->setEnabled(false);
		this->lE_LastCol->setEnabled(false);
	}
	else
	{
		d.autoAdjustBoundary = false;
		this->lE_FirstCol->setEnabled(true);
		this->lE_LastCol->setEnabled(true);		
	}

	this->checkSetup();
}

void Bui_ImageMergeDlg::checkSetup()
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
	{
		this->pB_Merge->setEnabled(false);
	}
	else
	{
		this->pB_Merge->setEnabled(false);

		CGroupData& d = this->data[this->selectedCamera];

		// ckeck if we can import this group
		if (!this->lE_FirstCol->hasAcceptableInput() ||
			!this->lE_LastCol->hasAcceptableInput() ||
			d.filename.IsEmpty())
		{
			d.readyToImport = false;			
		}
		else
		{
			d.readyToImport = true;	
		}

		// adjust check state
		this->tW_Files->blockSignals(true);
		if (d.readyToImport && d.import)
			d.item->setCheckState(0,Qt::Checked);
		else
			d.item->setCheckState(0,Qt::Unchecked);
		this->tW_Files->blockSignals(false);

		
		if (d.autoAdjustBoundary)
		{
			this->lE_FirstCol->setEnabled(false);
			this->lE_LastCol->setEnabled(false);
		}
		else
		{
			this->lE_FirstCol->setEnabled(true);
			this->lE_LastCol->setEnabled(true);
		}



		// set flags for all items
		for (unsigned int group=0; group < this->data.size(); group++)
		{
			CGroupData& d = this->data[group];
			
			if (d.readyToImport)
			{
				if (d.import)
					this->pB_Merge->setEnabled(true);
				
				if (!(d.item->flags() & Qt::ItemIsUserCheckable))
					d.item->setFlags(Qt::ItemFlag(d.item->flags() + Qt::ItemIsUserCheckable));
			}
			else
			{
				if (d.item->flags() & Qt::ItemIsUserCheckable)
					d.item->setFlags(Qt::ItemFlags(d.item->flags() - Qt::ItemIsUserCheckable));
			}
		}
		

		

	}
}

void Bui_ImageMergeDlg::leTextChanged(const QString &text)
{
	this->checkSetup();
}

void Bui_ImageMergeDlg::editingFinishedFirstCol()
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
		return;
	
	CGroupData& d = this->data[this->selectedCamera];

	if (this->lE_FirstCol->hasAcceptableInput())
	{
		d.firstCol = this->lE_FirstCol->text().toInt();
	}
}

void Bui_ImageMergeDlg::editingFinishedLastCol()
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
		return;
	
	CGroupData& d = this->data[this->selectedCamera];

	if (this->lE_LastCol->hasAcceptableInput())
	{
		d.lastCol = this->lE_LastCol->text().toInt();
	}
}

void Bui_ImageMergeDlg::rBNearestNeighbourReleased()
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
		return;
	
	CGroupData& d = this->data[this->selectedCamera];

	d.method = CImageMerger::eNearestNeighbour;
}

void Bui_ImageMergeDlg::rBBiLinearReleased()
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
		return;
	
	CGroupData& d = this->data[this->selectedCamera];

	d.method = CImageMerger::eBiLinear;
}

void Bui_ImageMergeDlg::rBBiCubicReleased()
{
	if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
		return;
	
	CGroupData& d = this->data[this->selectedCamera];

	d.method = CImageMerger::eBiCubic;
}

void Bui_ImageMergeDlg::loadImages()
{
	vector<vector<CVImage*> > possibleImages;
	vector<CCharString> cameraNames;
	this->data.clear();

	project.findImageGroups(possibleImages, cameraNames, 2);

	QList<QTreeWidgetItem *> items;
	this->data.resize(possibleImages.size());
	for (unsigned int i=0; i < possibleImages.size(); i++)
	{
		CGroupData& d = this->data[i];

		d.cameraName = cameraNames[i];
		d.inputImages = possibleImages[i];

		// set default file name
		d.filename = possibleImages[i][0]->getHugeImage()->getFileName()->GetPath() + CSystemUtilities::dirDelimStr + d.cameraName + ".tif";

		// init everything for import
		
		// create new image in project
		d.mergedImage = project.addImage(d.filename.GetFileName(), false);
		
		// set defaults
		d.mergedImage->getHugeImage()->bpp   = 8;
		d.mergedImage->getHugeImage()->bands = 1;
		d.mergedImage->setName(d.filename);

		d.exportDialog = new Bui_GDALExportDlg(d.filename, d.mergedImage,false, "Select Output File",true,false,true);
		
		QTreeWidgetItem* parent = new QTreeWidgetItem((QTreeWidget*)NULL,QStringList(QString("%1").arg(d.cameraName.GetChar())));
		parent->setData(0,Qt::UserRole,i); // remember index into array
		items.append(parent);
		parent->setCheckState(0,Qt::Unchecked);
		parent->setFlags(Qt::ItemFlag(parent->flags() + Qt::ItemIsEditable));

		d.item = parent;

		for (unsigned int k= 0; k < d.inputImages.size(); k++)
		{
			QTreeWidgetItem* item = new QTreeWidgetItem(parent,QStringList(QString().sprintf("%s",d.inputImages[k]->getFileNameChar())));
			item->setData(0,Qt::UserRole,-1); // no index to remember
			item->setFlags(Qt::ItemFlags(item->flags() - Qt::ItemIsUserCheckable));
			items.append(item);
			
		}
	
	}
	
	this->tW_Files->insertTopLevelItems(0, items); 

	if (this->data.size() > 0)
	{
		this->tW_Files->setCurrentItem(items.at(0));
	}


}

void Bui_ImageMergeDlg::tWFilesItemSelectionChanged()
{
	QList<QTreeWidgetItem *> list = this->tW_Files->selectedItems();

	if (list.size() != 1)
		return;

	int index = list.at(0)->data(0,Qt::UserRole).toInt();

	if (index != -1 && index < int(this->data.size()) && index != this->selectedCamera)
	{
		this->selectedCamera = index;
	
		if (this->selectedCamera < 0 || this->selectedCamera >= int(this->data.size()))
			return;
	
		CGroupData& d = this->data[this->selectedCamera];

		CVImage* firstSelectedImage = d.inputImages[0];
		CVImage* lastSelectedImage = d.inputImages[d.inputImages.size()-1];

		// check if already initialized
		if (d.firstCol == -1)
			d.firstCol = CImageMerger::findBoundary(firstSelectedImage->getHugeImage(),false);

		if (d.lastCol == -1)
			d.lastCol = CImageMerger::findBoundary(lastSelectedImage->getHugeImage(),true);

		// change the gui
		this->selectImageGroup(this->selectedCamera);
	}
	else
	{
		QTreeWidgetItem * parent = list.at(0)->parent();
		if (parent)
		{
			this->tW_Files->setCurrentItem(parent);
		}
	}

	this->checkSetup();
}

void Bui_ImageMergeDlg::selectImageGroup(unsigned int group)
{
	if (group < 0 || group >= this->data.size())
		return;

	CGroupData& d = this->data[group];
	
	// select the interpolation method
	this->rB_NearestNeighbour->blockSignals(true);
	this->rB_BiLinear->blockSignals(true);
	this->rB_BiCubic->blockSignals(true);

	if (d.method == CImageMerger::eNearestNeighbour)
		this->rB_NearestNeighbour->setChecked(true);
	else if (d.method == CImageMerger::eBiLinear)
		this->rB_BiLinear->setChecked(true);
	else if (d.method == CImageMerger::eBiCubic)
		this->rB_BiCubic->setChecked(true);

	this->rB_NearestNeighbour->blockSignals(false);
	this->rB_BiLinear->blockSignals(false);
	this->rB_BiCubic->blockSignals(false);

	// tick box if selected
	this->cB_AutoAdjust->blockSignals(true);
	this->cB_AutoAdjust->setChecked(d.autoAdjustBoundary ? Qt::Checked : Qt::Unchecked);
	this->cB_AutoAdjust->blockSignals(false);

	// change first and last col settings
	this->lE_FirstCol->blockSignals(true);
	this->lE_LastCol->blockSignals(true);
	CVImage* firstSelectedImage = d.inputImages[0];
	CVImage* lastSelectedImage = d.inputImages[d.inputImages.size()-1];
	
	// change the validator
	this->lE_FirstCol->setValidator(new QIntValidator(0,firstSelectedImage->getHugeImage()->getWidth() - 1,this));
	this->lE_LastCol->setValidator(new QIntValidator(0,lastSelectedImage->getHugeImage()->getWidth() - 1,this));

	this->lE_FirstCol->setText(QString().sprintf("%d",d.firstCol));
	this->lE_LastCol->setText(QString().sprintf("%d",d.lastCol));

	this->lE_FirstCol->blockSignals(false);
	this->lE_LastCol->blockSignals(false);

	// change file name
	this->lE_FileName->blockSignals(true);
	this->lE_FileName->setText(d.filename.GetChar());
	this->lE_FileName->setToolTip(d.filename.GetChar());
	this->lE_FileName->blockSignals(false);


}


void Bui_ImageMergeDlg::tWFilesItemChanged(QTreeWidgetItem* item,int col)
{
	if (!item)
		return;
	
	int index = item->data(0,Qt::UserRole).toInt();

	if (index != -1 && index < int(this->data.size()) && index == this->selectedCamera && col == 0)
	{
		CGroupData& d = this->data[this->selectedCamera];

		bool chekSetup = false;
		// has the check state changed?
		if (d.readyToImport)
		{
			if (d.import && d.item->checkState(0) == Qt::Unchecked)
				d.import = false;
			else if (!d.import && d.item->checkState(0) == Qt::Checked)
				d.import = true;

			chekSetup = true;
			
		}

		// has the camera name changed?
		CCharString newName(d.item->text(0).toLatin1().constData());
		if (!d.cameraName.CompareNoCase(newName))
		{
			
			if (!this->checkCameraName(newName,d.cameraName))
			{
				// change display 
				this->tW_Files->blockSignals(true);
				d.item->setText(0,newName.GetChar());
				this->tW_Files->blockSignals(true);
			}

			d.cameraName = newName;
			chekSetup = true;
		}
		
		if (chekSetup)
			this->checkSetup();

	}
}

bool Bui_ImageMergeDlg::checkCameraName(CCharString& newName,const CCharString& oldName)
{
	// check with local cameras
	for (unsigned int group=0; group < this->data.size(); group++)
	{
		if (this->data[group].cameraName.CompareNoCase(newName))
		{
			newName = oldName; // use the old name
			return false;
		}
	}
	
	// check if camera with same name is already in project
	return project.getUniqueCameraName(newName);
}


CGroupData::~CGroupData() 
{
	if (!this->exportDialog)
		delete this->exportDialog;
	this->exportDialog = NULL;

}