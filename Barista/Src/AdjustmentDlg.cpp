#include "AdjustmentDlg.h"
 #include <QKeyEvent>

CAdjustmentDlg::CAdjustmentDlg(QWidget *parent) : QDialog(parent),
	success(false),listenToSignal(true),lastTab(-1),defaultTab(0)
{
	// this is needed when sending custom objects via signals and thread boundaries
	qRegisterMetaType<CLabel>("CLabel");
	this->setAttribute(Qt::WA_DeleteOnClose,true);
}

CAdjustmentDlg::~CAdjustmentDlg(void)
{
}
void CAdjustmentDlg::elementAdded(CLabel element)
{
}

void CAdjustmentDlg::elementDeleted(CLabel element)
{
	this->cancelButtonReleased();
	this->cleanUp();
	emit closeDlg(true);
}

void CAdjustmentDlg::elementsChanged(CLabel element)
{
	// when emitting the signal we can listen to updates from different threads
	emit elementsChangedSignal(element);
}

void CAdjustmentDlg::ckeckDlgContent()
{
}

void CAdjustmentDlg::keyPressEvent( QKeyEvent * keyEvent)
{
	keyEvent->ignore();
}
