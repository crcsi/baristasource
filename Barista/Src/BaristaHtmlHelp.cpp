#ifdef WIN32
#include <windows.h>
#include <htmlhelp.h>
#endif

#include <QApplication>
#include <QString>
#include <QMessageBox>

#include "BaristaHtmlHelp.h"
#include "SystemUtilities.h"


CFileName CHtmlHelp::fileNameSearch =  "Barista.chm";
#ifdef WIN32
CFileName CHtmlHelp::fileNameContens =  CHtmlHelp::fileNameSearch + "::/";	
#else
CFileName CHtmlHelp::fileNameContens =  CHtmlHelp::fileNameSearch + "#xchm:/";
#endif

CHtmlHelp::CHtmlHelp(void)
{
}

CHtmlHelp::~CHtmlHelp(void)
{
}

void CHtmlHelp::callHelp(const char* page)
{

#ifdef WIN32
	CCharString helpFile = CCharString(QApplication::applicationDirPath().toLatin1().constData()) + "/" + fileNameContens;
	helpFile += page;
	HWND hwnd = HtmlHelpA(GetDesktopWindow(),helpFile.GetChar(), HH_DISPLAY_TOC,NULL);
#else
	CCharString appPath = QApplication::applicationDirPath().toLatin1().constData();
	appPath +=  + "/";
	CCharString command = appPath + "./xchm  file:" + appPath +fileNameContens;
	command +=page;
	command +="&";
	CSystemUtilities::systemCommand(command.GetChar());
	
#endif	
}

void CHtmlHelp::callSearch()
{
#ifdef WIN32
	HH_FTS_QUERY q ;

	q.cbStruct         = sizeof(HH_FTS_QUERY) ;
	q.fUniCodeStrings  = FALSE ;
	q.pszSearchQuery   = (LPCTSTR)"";
	q.iProximity       = HH_FTS_DEFAULT_PROXIMITY ;
	q.fStemmedSearch   = FALSE ;
	q.fTitleOnly       = FALSE ;
	q.fExecute         = FALSE ;
	q.pszWindow        = NULL ;

	CCharString helpFile = CCharString(QApplication::applicationDirPath().toLatin1().constData()) + "/" + fileNameSearch;
	HWND hwnd = HtmlHelpA(GetDesktopWindow(),helpFile.GetChar(), HH_DISPLAY_SEARCH,(DWORD)&q);
#endif
}
