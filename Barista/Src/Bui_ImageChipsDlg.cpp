#include "Bui_ImageChipsDlg.h"
#include "GCPDataBasePoint.h"
#include "BaristaProject.h"
#include <QDate>
#include "Icons.h"

Bui_ImageChipsDlg::Bui_ImageChipsDlg(QWidget *parent)
	: QDialog(parent)
{
	setupUi(this);
	
	initDialog();

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	connect(this->pBOK,SIGNAL(released()),this,SLOT(OK_clicked()));
	connect(this->pBCancel,SIGNAL(released()),this,SLOT(Cancel_clicked()));
}

Bui_ImageChipsDlg::~Bui_ImageChipsDlg()
{

}

void Bui_ImageChipsDlg::initDialog()
{
	CGCPDataBasePoint P3d = project.getGCPpoint();
	CGCPData2DPoint P2d = P3d.get2DPointAt(0);			

	this->lePlatform->setText((QString)P2d.getPlatform());

	//sensor
	QStringList sensorList;
	sensorList << "2xHRG" << "2xHRVIR" << "AVNIR-2"  << "Ikonos-2" << "HRS" << "PRISM" << "QuickBird-2";
	this->cBSensor->insertItems(0,sensorList);
	//this->leSensor->setText((QString)P2d.getSensor());
	CCharString currentSensor = P2d.getSensor();
	int i;
	for (i = 0; i < 7; i++)
	{
		CCharString thisSensor = sensorList[i].toLatin1();		
		if (currentSensor.CompareNoCase(thisSensor))
			break;
	}
	this->cBSensor->setCurrentIndex(i);


	this->leSourceOfHeight->setText((QString)P2d.getSourceOfHeight());
	this->leHDatum->setText((QString)P2d.gethDatum());
	this->leVDatum->setText((QString)P2d.getvDatum());
	this->leProjection->setText((QString)P2d.getProjection());
	this->leEPSGCode->setText((QString)P2d.getepsgCode());
	this->leOriginalDataProvider->setText((QString)P2d.getdataProvider());
	this->leLicenseID->setText((QString)P2d.getLicense());
	this->leComments->setText((QString)P2d.getComments());

	QString aDateStr = (QString)P2d.getAcquiredDate();
	QString cDateStr = (QString)P2d.getDateCreation();
	QDate AcquiredDate = QDate::fromString(aDateStr,"dd/MM/yyyy");
	QDate DateCreation = QDate::fromString(cDateStr,"dd/MM/yyyy");
	//CCharString AcquiredDatestr = (CCharString)AcquiredDate.toString("dd/MM/yyyy").toLatin1();
	//CCharString DateCreationstr = (CCharString)DateCreation.toString("dd/MM/yyyy").toLatin1();
	this->deAcquisitionDate->setDate(AcquiredDate);
	this->deDateCreation->setDate(DateCreation);

	this->dsbResolution->setRange(0.1,20.0);
	this->dsbResolution->setSingleStep(0.1);
	this->dsbResolution->setValue(P2d.getQualityOfDefinition());

	this->dsbViewAngle->setRange(-90,+90);
	this->dsbViewAngle->setSingleStep(0.1);
	this->dsbViewAngle->setValue(P2d.getViewAngle());

	this->dsbAccuracy->setRange(0.1,20.0);
	this->dsbAccuracy->setSingleStep(0.1);
	this->dsbAccuracy->setValue(P2d.getAccuracy());

	this->dsbAccuracyZ->setRange(0.1,20.0);
	this->dsbAccuracyZ->setSingleStep(0.1);
	this->dsbAccuracyZ->setValue(P2d.getAccuracyZ());

	this->cbUnit->addItem("metre");
	this->cbUnit->addItem("degree");
	if (P2d.getUnit() == "metre")
		this->cbUnit->setCurrentIndex(0);
	else
		this->cbUnit->setCurrentIndex(1);
	
	this->sbZone->setRange(1,60);
	this->sbZone->setSingleStep(1);
	this->sbZone->setValue(P2d.getZone());

	this->sbChipSize->setRange(1,512);
	this->sbChipSize->setSingleStep(1);
	this->sbChipSize->setValue(P2d.getchipSize());

	this->cbIsMonoplotted->addItem("true");
	this->cbIsMonoplotted->addItem("false");
	if (P2d.getisMonoplotted() == true)
		this->cbIsMonoplotted->setCurrentIndex(0);
	else
		this->cbIsMonoplotted->setCurrentIndex(1);

	this->sbNumberOfBands->setRange(1,4);
	this->sbNumberOfBands->setSingleStep(1);
	this->sbNumberOfBands->setValue(P2d.getnBands());
}

void Bui_ImageChipsDlg::OK_clicked()
{
	CGCPData2DPoint P2d;

	P2d.setPlatform((CCharString)this->lePlatform->text().toLatin1());
	//P2d.setSensor((CCharString)this->leSensor->text().toLatin1());
	P2d.setSensor((CCharString)this->cBSensor->currentText().toLatin1());

	P2d.setSourceOfHeight((CCharString)this->leSourceOfHeight->text().toLatin1());
	P2d.sethDatum((CCharString)this->leHDatum->text().toLatin1());
	P2d.setvDatum((CCharString)this->leVDatum->text().toLatin1());
	P2d.setProjection((CCharString)this->leProjection->text().toLatin1());
	P2d.setepsgCode((CCharString)this->leEPSGCode->text().toLatin1());
	P2d.setdataProvider((CCharString)this->leOriginalDataProvider->text().toLatin1());
	P2d.setLicense((CCharString)this->leLicenseID->text().toLatin1());
	P2d.setComments((CCharString)this->leComments->text().toLatin1());
	
	P2d.setDate((CCharString)this->deAcquisitionDate->date().toString("dd/MM/yyyy").toLatin1());
	P2d.setDateCreation((CCharString)this->deDateCreation->date().toString("dd/MM/yyyy").toLatin1());

	P2d.setQualityOfDefinition(this->dsbResolution->value());
	P2d.setViewAngle(this->dsbViewAngle->value());
	P2d.setAccuracy(this->dsbAccuracy->value());
	P2d.setAccuracyZ(this->dsbAccuracyZ->value());

	P2d.setUnit((CCharString)this->cbUnit->currentText().toLatin1());

	P2d.setZone(this->sbZone->value());
	P2d.setchipSize(this->sbChipSize->value());

	P2d.setisMonoplotted((CCharString)this->cbIsMonoplotted->currentText().toLatin1());
	
	P2d.setnBands(this->sbNumberOfBands->value());

	P2d.setX(this->sbChipSize->value()/2);
	P2d.setY(this->sbChipSize->value()/2);

	project.getGCPpoint().setGCPPoint2DArrayClear();
	project.getGCPpoint().set2DPoint(P2d);
	//
	CGCPDataBasePoint P3d = project.getGCPpoint();
	P3d.setGCPPoint2DArrayClear();
	P3d.set2DPoint(P2d);
	P3d.setCoordinateType(eGEOCENTRIC);
	//P3d.setDate(CSystemUtilities::dateString().GetChar());
	P3d.setDate((CCharString)this->deDateCreation->date().toString("dd/MM/yyyy").toLatin1());
	P3d.setFromGPS(true);
	project.setGCPpoint(P3d);

	this->close();
}

void Bui_ImageChipsDlg::Cancel_clicked()
{
	this->close();
}