#include "Bui_DEMOperationsDlg.h"

#include <QMessageBox>

#include "Icons.h"
#include "BaristaProject.h"
#include "BaristaHtmlHelp.h"
#include "GDALFile.h"
#include "Bui_GDALExportDlg.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include "TFW.h"
#include "BaristaProjectHelper.h"


Bui_DEMOperationsDlg::Bui_DEMOperationsDlg(CVDEM* vdem): trafoname(""), success(false),dlg(NULL)
,srcdem(vdem), trafodem(NULL), nullVALL(-9999),consVall(5),condition(">")
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	// pushbutton connections
	this->connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));

	// output connection
	connect(this->pbTrafoDEM, SIGNAL(released()),this, SLOT(pBSelectOutputReleased()));

    // selection of control parameters
	this->connect(this->operationsLineEdit,SIGNAL(editingFinished ()),this,SLOT(operationEditingFinished()));
	this->connect(this->ConsValLineEdit,SIGNAL(editingFinished ()),this,SLOT(constantEditingFinished()));
	this->connect(this->NullValueLineEdit,SIGNAL(editingFinished ()),this,SLOT(nullValEditingFinished()));
	
	QString conditions(condition.c_str());
	this->operationsLineEdit->setText(QString( "%1" ).arg(conditions ));
	this->ConsValLineEdit->setText(QString( "%1" ).arg(this->consVall) );
	this->NullValueLineEdit->setText(QString( "%1" ).arg(this->nullVALL) );
	
	this->checkSetup();
}

Bui_DEMOperationsDlg::~Bui_DEMOperationsDlg()
{
	if (this->dlg)
		delete this->dlg;

	this->dlg=NULL;

	if (!this->success)
	{
		if ( this->trafodem )
			project.deleteDEM(trafodem);
		this->trafodem = NULL;
	}
}

void Bui_DEMOperationsDlg::pBOkReleased()
{
	if (!this->trafodem) return;
	if ( !this->srcdem ) return;

	this->trafodem->getDEM()->width = this->srcdem->getDEM()->getWidth();
	this->trafodem->getDEM()->height = this->srcdem->getDEM()->getHeight();

	CTFW tfw (*this->trafodem->getTFW());
	tfw.setRefSys(this->srcdem->getReferenceSystem());
	tfw.setFileName("TrafoDEM.tfw");
	this->trafodem->setTFW(tfw);

	this->trafodem->getDEM()->setFileName( this->trafoname.GetChar());

	bui_ProgressDlg progressdlg(new CDEMOperationsThread ( nullVALL, consVall, condition ,this->trafodem->getDEM(),this->srcdem->getDEM()),this->trafodem->getDEM(),"Logic operation on DEM ...");

	progressdlg.exec();
	this->success = progressdlg.getSuccess();


	if ( this->success && this->dlg)
	{
		if ( this->dlg->writeSettings() )
			this->dlg->writeImage("Writing DEM as image file");

		if (!this->dlg->getSuccess())
		{
			this->success = false;
		}
	}

	this->setSuccess(true);
	this->close();
}

void Bui_DEMOperationsDlg::pBCancelReleased()
{
	this->setSuccess(false);
	this->close();
}

void Bui_DEMOperationsDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/DifferenceDEMs.html");
}

void Bui_DEMOperationsDlg::pBSelectOutputReleased()
{
	if (!this->dlg)
		delete this->dlg;
	this->dlg =  NULL;

	this->trafoname = "";
	this->OutputDEMFile->setText(this->trafoname.GetFullFileName().GetChar());

	if (this->srcdem)
	{
		if (this->trafodem)
			project.deleteDEM(this->trafodem);

		this->trafodem = project.addDEM(this->trafoname);
		this->trafodem->getDEM()->bands = this->srcdem->getDEM()->getBands();
		this->trafodem->getDEM()->bpp = this->srcdem->getDEM()->getBpp();
		this->trafodem->getDEM()->setTFW(*this->srcdem->getDEM()->getTFW());
		this->trafodem->getDEM()->width = this->srcdem->getDEM()->getWidth();
		this->trafodem->getDEM()->height = this->srcdem->getDEM()->getHeight();

		this->dlg = new Bui_GDALExportDlg(this->trafoname,this->trafodem,true,"DEM Operation");
	}
	else 
		return;

	dlg->exec();

	if (!this->dlg->getSuccess())	// false if user has pressed "Cancel"
	{
		if (this->trafodem)
			project.deleteDEM(this->trafodem);

		this->trafodem = NULL;
		this->trafoname = "";
	}
	else
	{
		this->trafoname = this->dlg->getFilename();
		this->OutputDEMFile->setText(this->trafoname.GetFullFileName().GetChar());
		this->trafodem->setFileName(this->trafoname.GetChar());
	}

	this->checkSetup();
}

void Bui_DEMOperationsDlg::operationEditingFinished()
{
		if ( this->operationsLineEdit->hasAcceptableInput() )
		{
			//string temp;
    		condition= this->operationsLineEdit->text().toStdString();
			//strcpy (condition.GetChar(), temp.c_str());
			/*CCharString tempstr =temp.c_str();
			const char temchar =*(tempstr.GetChar());
			condition= &temchar;*/
		}
}


void Bui_DEMOperationsDlg::constantEditingFinished()
{
	if ( this->ConsValLineEdit->hasAcceptableInput() )
		this->consVall = this->ConsValLineEdit->text().toDouble();
}

void Bui_DEMOperationsDlg::nullValEditingFinished()
{
	if ( this->NullValueLineEdit->hasAcceptableInput() )
		this->nullVALL = this->NullValueLineEdit->text().toDouble();
}

void Bui_DEMOperationsDlg::checkSetup()
{
	bool readyToGo = true;
	this->pBOk->setEnabled(false);
	this->OutputDEMFile->setEnabled(true);

	// check filename is set
	if ( this->trafoname.IsEmpty() )
		readyToGo = false;

	if ( readyToGo ) this->pBOk->setEnabled(true);
}