#include "Bui_XYZPointDlg.h"
#include "XYZPointArray.h"
#include "Bui_ObsPointDlg.h"


Bui_XYZPointDlg::Bui_XYZPointDlg(CXYZPointArray* points,CXYZPointArray* residuals, QWidget *parent)
	: CAdjustmentDlg(parent),points(0),residuals(0),oldTab(-1),pointObsDlg(0),pointResDlg(0)
{
	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->setPoints(points,residuals);
}

Bui_XYZPointDlg::~Bui_XYZPointDlg()
{
	this->cleanUp();
}

void Bui_XYZPointDlg::elementDeleted(CLabel element)
{
}


void Bui_XYZPointDlg::cleanUp()
{
	if (this->points)
		this->points->removeListener(this);
	this->points = NULL;

	if (this->residuals)
		this->residuals->removeListener(this);
	this->residuals = NULL;

	if (this->pointObsDlg)
		delete this->pointObsDlg;
	this->pointObsDlg = NULL;

	if (this->pointResDlg)
		delete this->pointResDlg;
	this->pointResDlg = NULL;
}

bool Bui_XYZPointDlg::okButtonReleased()
{
	if (this->pointObsDlg && !this->pointObsDlg->okButtonReleased())
		return false;

	if (this->pointResDlg && !this->pointResDlg->okButtonReleased())
		return false;

	return true;
}

bool Bui_XYZPointDlg::cancelButtonReleased()
{
	return true;
}
CCharString Bui_XYZPointDlg::helpButtonReleased()
{
	return "";
}


void Bui_XYZPointDlg::setPoints(CXYZPointArray* newPoints,CXYZPointArray* newResiduals)
{
	// clean up first
	if (this->points)
		this->points->removeListener(this);

	if (this->residuals)
		this->residuals->removeListener(this);

	if (!newPoints || !newResiduals)
		return;

	this->points = newPoints;
	this->residuals = newResiduals;

	this->points->addListener(this);
	this->residuals->addListener(this);

	this->init3DObsPointDlg();
	this->init3DResPointDlg();
}

void Bui_XYZPointDlg::pointObsDlgStatusChanged(bool b)
{
	if (b)
	{
		this->tabWidget->setTabEnabled(1,true);
	}
	else
	{
		this->tabWidget->setTabEnabled(1,false);
	}

	emit enableOkButton(b);	
}

void Bui_XYZPointDlg::pointResDlgStatusChanged(bool b)
{
	if (b)
	{
		this->tabWidget->setTabEnabled(0,true);
	}
	else
	{
		this->tabWidget->setTabEnabled(0,false);
	}

	emit enableOkButton(b);	
}

void Bui_XYZPointDlg::tWCurrentChanged( int index )
{
	if (oldTab != -1)
	{
		if (oldTab == 0 && this->pointObsDlg)
		{
			this->pointObsDlg->okButtonReleased();
		}
		else if (oldTab == 1 && this->pointResDlg)
		{
			this->pointResDlg->okButtonReleased();
		}
	}

	oldTab = index;
}




void Bui_XYZPointDlg::init3DObsPointDlg()
{
	if (this->pointObsDlg)
	{
		this->tabXYZPoints->layout()->removeWidget(this->pointObsDlg);
		this->disconnect(this->pointObsDlg,SIGNAL(enableOkButton(bool)),this,SLOT(pointObsDlgStatusChanged(bool)));
		delete this->pointObsDlg;
		this->pointObsDlg = NULL;
	}




	this->pointObsDlg = new Bui_ObsPointDlg(this->points);
	this->connect(this->pointObsDlg,SIGNAL(enableOkButton(bool)),this,SLOT(pointObsDlgStatusChanged(bool)));
	this->tabXYZPoints->layout()->addWidget(this->pointObsDlg);


}

void Bui_XYZPointDlg::init3DResPointDlg()
{
	if (this->pointResDlg)
	{
		this->tabXYZResiduals->layout()->removeWidget(this->pointResDlg);
		this->disconnect(this->pointResDlg,SIGNAL(enableOkButton(bool)),this,SLOT(pointResDlgStatusChanged(bool)));
		delete this->pointResDlg;
		this->pointResDlg = NULL;
	}

	if (this->residuals->GetSize())
	{


		this->tabWidget->setTabEnabled(1,true);
		this->pointResDlg = new Bui_ObsPointDlg(this->residuals,false,true,false);
		this->connect(this->pointResDlg,SIGNAL(enableOkButton(bool)),this,SLOT(pointResDlgStatusChanged(bool)));
		this->tabXYZResiduals->layout()->addWidget(this->pointResDlg);
	}
	else
		this->tabWidget->setTabEnabled(1,false);


}

void Bui_XYZPointDlg::elementsChangedSlot(CLabel element)
{
	if (!this->listenToSignal)
		return;

	if (element == "CXYZPOINTARRAY" )
		this->init3DResPointDlg();
}