#ifdef WIN32

#include "BuildingTable.h"

#include "BuildingArray.h"
#include <QMessageBox>
#include <QMenu>

#include <QCloseEvent>

#include "BaristaProject.h"
#include "BaristaProjectHelper.h"
#include "Bui_BuildingReconstructionDialog.h"


CBuildingTable::CBuildingTable(CBuildingArray* buildings,QWidget* parent) : CBaristaTable(parent),
	buildings(buildings) ,xdezimals(4),ydezimals(4)
{

	this->initTable();
    buildings->addListener(this);


	bool b = this->connect(this, SIGNAL( cellChanged(int, int)), this, SLOT(changedValue(int, int)));
	this->connect(this, SIGNAL( itemSelectionChanged()), this, SLOT(itemSelectionChanged()));


	for (int i=0; i < buildings->GetSize();i++)
	{
		if (buildings->GetAt(i)->getIsSelected())
		{
			this->elementSelected(true,"",i);
		}
	}
}

CBuildingTable::~CBuildingTable(void)
{
}



void CBuildingTable::closeEvent ( QCloseEvent * e )
{
    this->buildings->removeListener(this);
    e->accept();
}


void CBuildingTable::initTable()
{
	CCharString str("Buildings:   ");
	str = str + this->buildings->getFileName()->GetFullFileName();
	this->setWindowTitle(str.GetChar());

	this->setAccessibleName(this->buildings->getFileName()->GetChar());

	this->setColumnCount(8);
	this->setRowCount(this->buildings->GetSize());

	for (int i=0; i < this->buildings->GetSize(); i++)
		this->setRowHeight(i,20);

	this->setColumnWidth(0, 150);
	this->setColumnWidth(1, 90);
	this->setColumnWidth(2, 95);
	this->setColumnWidth(3, 100);
	this->setColumnWidth(4, 100);
	this->setColumnWidth(5, 80);
	this->setColumnWidth(6, 100);
	this->setColumnWidth(7, 100);

	QStringList headerList;

    if ( this->buildings->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC)
    {
		this->xdezimals = 10;
		this->ydezimals = 10;

		headerList.append(QString("Label"));
		headerList.append(QString("Number of Points"));
		headerList.append(QString("Start Point" ));
        headerList.append(QString("Latitude" ));
        headerList.append(QString("Longitude" ));
        headerList.append(QString("Height" ));
    }
    else if ( this->buildings->getReferenceSystem()->getCoordinateType() == eGEOCENTRIC)
    {
        headerList.append(QString("Label" ));
        headerList.append(QString("Number of Points" ));
        headerList.append(QString("Start Point" ));
        headerList.append(QString("X" ));
        headerList.append(QString("Y" ));
        headerList.append(QString("Z" ));
    }
    else if ( this->buildings->getReferenceSystem()->getCoordinateType() == eGRID)
    {
		CCharString p1("Label");

		if (this->buildings->getReferenceSystem()->isUTM())
		{
			p1 += "(Zone ";
        
			CLabel p2;
			p2 += this->buildings->getReferenceSystem()->getZone();

			p1 = p1 + p2.GetChar();

			if ( this->buildings->getReferenceSystem()->getHemisphere() == SOUTHERN )
			{
				p1 = p1 + ", Southern)";   
			}
			else if ( this->buildings->getReferenceSystem()->getHemisphere() == NORTHERN )
			{
				p1 = p1 + ", Northern)";
			}
		}
		else
		{
			p1 += "(Lat:";
			double lon = this->buildings->getReferenceSystem()->getCentralMeridian();
			double lat = this->buildings->getReferenceSystem()->getLatitudeOrigin();

			char buf[256];
			sprintf(buf, "%5.2f Lon:%5.2f)", lat, lon); 

			p1 = p1 + buf;
		}       

		headerList.append(QString(p1.GetChar() ));
        headerList.append(QString("Number of Points" ));
        headerList.append(QString("Start Point" ));
        headerList.append(QString("Easting" ));
        headerList.append(QString("Northing" ));
        headerList.append(QString("Height" ));
    }
    else 
    { // undefined coordinate type
		headerList.append(QString("Label"));
        headerList.append(QString("Number of Points" ));
        headerList.append(QString("Start Point" ));
        headerList.append(QString("LocalX" ));
        headerList.append(QString("LocalY" ));
        headerList.append(QString("LocalZ" ));
    }

	headerList.append(QString("Building Height" ));
    headerList.append(QString("Building Area" ));

	this->setHorizontalHeaderLabels(headerList);

    CLabel label = "";
    
	QTableWidgetItem* item;
    
    for ( int i = 0; i < this->buildings->GetSize(); ++i )
    {
		CBuilding* building = this->buildings->GetAt(i);

		QString blabel = building->getLabel().GetChar();

		C2DPoint p = building->getCentre();

		double z = building->getFloorHeight();
		double height = building->getHeight();
		double area = building->getArea();

		QString plabel = building->getFirstPointLabel().GetChar();

		
		item = this->createItem(blabel,(Qt::ItemFlags)35,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(i,0,item);

		item = this->createItem(QString( "%1" ).arg(building->getPointCount()),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,1,item);

		item = this->createItem(plabel,0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,2,item);

		item = this->createItem(QString( "%1" ).arg(p.x, 0, 'f', this->xdezimals ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,3,item);

		item = this->createItem(QString( "%1" ).arg(p.y, 0, 'f', this->ydezimals ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,4,item);

		item = this->createItem(QString( "%1" ).arg(z, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,5,item);

		item = this->createItem(QString( "%1" ).arg(height, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,6,item);

		item = this->createItem(QString( "%1" ).arg(area, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(i,7,item);
    }
}


void CBuildingTable::setBuildings(CBuildingArray* buildings)
{
    this->buildings = buildings;
    buildings->addListener(this);
}


void CBuildingTable::elementAdded(CLabel element)
{
	this->blockSignals(true);

	int size = this->buildings->GetSize();

    if (size < 0)
        return;

	this->setRowCount(size);

	int index = size -1;
	this->setRowHeight(index,20);

    CLabel label = "";

	QTableWidgetItem* item;
    
	CBuilding* building = this->buildings->GetAt(index);

	QString blabel = building->getLabel().GetChar();

	if (building->getPointCount() > 0)
	{
		C2DPoint p = building->getCentre();

		double z = building->getFloorHeight();
		double height = building->getHeight();
		QString plabel = building->getFirstPointLabel().GetChar();
		double area = building->getArea();

		item = this->createItem(blabel,(Qt::ItemFlags)35,Qt::AlignLeft,QColor(0,0,0));
		this->setItem(index,0,item);

		item = this->createItem(QString( "%1" ).arg(building->getPointCount()),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(index,1,item);

		item = this->createItem(plabel,0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(index,2,item);

		item = this->createItem(QString( "%1" ).arg(p.x, 0, 'f', this->xdezimals ),0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(index,3,item);

		item = this->createItem(QString( "%1" ).arg(p.y, 0, 'f', this->ydezimals ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(index,4,item);

		item = this->createItem(QString( "%1" ).arg(z, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(index,5,item);

		item = this->createItem(QString( "%1" ).arg(height, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(index,6,item);

		item = this->createItem(QString( "%1" ).arg(area, 0, 'f', 3 ),(Qt::ItemFlags)0,Qt::AlignRight,QColor(0,0,0));
		this->setItem(index,7,item);

		this->blockSignals(false);
	}
}

void CBuildingTable::elementDeleted(CLabel element)
{
	this->updateContents();
}


void CBuildingTable::changedValue( int row, int col )
{
	if ( col == 0)
	{
		CLabel clabel = this->getItemAsLabel(row, col);

		// if label is empty or alreday exists keep old label
		if (clabel.GetLength() == 0 || this->buildings->getBuilding(clabel.GetChar()))
		{
			this->setLabelAsItemText(this->buildings->GetAt(row)->getLabel(), row, col);
			
			QMessageBox::warning( this, "Change Label not possible!"
			, "This label already exist. Choose different Label!");
		}
		// if label is not empty and not existing yet accept new label
		else
		{
			this->buildings->GetAt(row)->setLabel( clabel);
			this->setLabelAsItemText(clabel, row, col);
		}
	}
}

void CBuildingTable::removefromTable()
{
	this->blockSignals(true);
	int size = this->rowCount();

	int count=0;
	for ( int i =size -1; i >=0; i--)
	{
		if ( this->item(i, 0)->isSelected())
		{
			count++;
		}
		else
		{
			if (count) 
				if (project.removeMonoplottedBuilding(i+1,count))
					baristaProjectHelper.refreshTree();

			count =0;
		}
	}  
	if (count) 
		if (project.removeMonoplottedBuilding(0,count))
			baristaProjectHelper.refreshTree();
    
  
	project.setCurrentBuildingIndex(-1);

	this->updateContents();
	this->blockSignals(false);
}

void CBuildingTable::reconstructBuildings()
{
	#ifdef WIN32
		int size = this->rowCount();

		vector<CBuilding *> buildingList;

		for ( int i =size -1; i >=0; i--)
		{
			if ( this->item(i, 0)->isSelected())
			{
				buildingList.push_back(project.getBuildings()->GetAt(i));
			}
		}  

		bui_BuildingReconstructionDialog dlg(buildingList);
		dlg.exec();
	#else
	// code for Linux and other systems
	#endif
}

void CBuildingTable::contextMenuEvent(QContextMenuEvent* e)
{
	QMenu* contextMenu = new QMenu( this );
	Q_CHECK_PTR( contextMenu );

	QAction *deleteAction = new QAction(tr("&Delete Building(s)"), this);
    connect(deleteAction, SIGNAL(triggered()), this, SLOT(removefromTable()));
	
    
#ifndef FINAL_RELEASE
	QAction *ReconstructBuildingAction = new QAction(tr("&Reconstruct"), this);
	connect(ReconstructBuildingAction, SIGNAL(triggered()), this, SLOT(reconstructBuildings()));
	contextMenu->addAction(ReconstructBuildingAction);
	contextMenu->addSeparator();
#endif


 
	contextMenu->addAction(deleteAction);

	contextMenu->exec( QCursor::pos() );

#ifndef FINAL_RELEASE
	delete ReconstructBuildingAction;
#endif

	delete deleteAction;
	delete contextMenu;
}

void CBuildingTable::elementSelected(bool selected,CLabel element,int elementIndex)
{
	this->blockSignals(true);

	if (elementIndex >=0 && elementIndex < this->rowCount())
		this->item(elementIndex,0)->setSelected(selected);	

	this->blockSignals(false);
}


void CBuildingTable::itemSelectionChanged ()
{
	this->blockSignals(true);

	QList<QTableWidgetItem *> list = this->selectedItems ();

	buildings->unselectAll(false);

	for (int i=0; i < list.count(); i++)
	{
		buildings->selectElement(buildings->GetAt(list.at(i)->row())->getLabel(),true,false);
	}

	if (list.count()== 1)
		project.setCurrentBuildingIndex(list.at(0)->row());
	else
		project.setCurrentBuildingIndex(-1);

	this->blockSignals(false);
}

#endif // WIN32