#include "Bui_SplineModelDlg.h"


#include "utilities.h"
#include "BaristaHtmlHelp.h"
#include "BaristaProject.h"
#include <QMessageBox>
#include <QDoubleValidator>
#include <QHeaderView>
#include "EditorDelegate.h"
#include "CheckBoxDelegate.h"



Bui_SplineModelDlg::Bui_SplineModelDlg(CSplineModel* splineModel,SplineModelType splineType,QWidget *parent)
	: CAdjustmentDlg(parent),splineType(splineType),existingSpline(splineModel),spline(0)
	
{
	if (!this->existingSpline)
		return;
	
	setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->existingSpline->addListener(this);

	if (this->splineType == Path)
	{
		this->lB_X->setText("X [m]");
		this->lB_Y->setText("Y [m]");
		this->lB_Z->setText("Z [m]");

		CObsPoint p = this->existingSpline->getOffsetSpline();
		this->lE_X->setText(QString().sprintf("%.2lf",p[0]));
		this->lE_Y->setText(QString().sprintf("%.2lf",p[1]));
		this->lE_Z->setText(QString().sprintf("%.2lf",p[2]));


	}
	else if (this->splineType == Attitude)
	{
		this->lB_X->setText("Roll [m�]");
		this->lB_Y->setText("Pitch [m�]");
		this->lB_Z->setText("Yaw [m�]");

		CObsPoint p = this->existingSpline->getOffsetSpline();
		this->existingSpline->convertToDifferentialUnits(p,p);

		this->lE_X->setText(QString().sprintf("%.2lf",p[0]));
		this->lE_Y->setText(QString().sprintf("%.2lf",p[1]));
		this->lE_Z->setText(QString().sprintf("%.2lf",p[2]));

	}


	this->spline = CSplineModelFactory::initSplineModel(splineModel,splineType);


	this->dataModel = new CSplineDataModel(this->spline,splineType,this);


	this->tV_SplineParameter->setItemDelegateForColumn(3,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
	this->tV_SplineParameter->setItemDelegateForColumn(4,new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
	this->tV_SplineParameter->setItemDelegateForColumn(5,new CCheckBoxDelegate(this));

	this->tV_SplineParameter->verticalHeader()->setDefaultSectionSize(20);
	this->tV_SplineParameter->verticalHeader()->hide();
	this->tV_SplineParameter->setSelectionMode(QTableView::SingleSelection);

	this->tV_SplineParameter->setModel(this->dataModel);

	this->tV_SplineParameter->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
	this->tV_SplineParameter->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
	this->tV_SplineParameter->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
	
	if (this->existingSpline->getUseDirectObservationsForSpline())
	{
		this->tV_SplineParameter->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
		this->tV_SplineParameter->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
		this->tV_SplineParameter->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Fixed);
		this->tV_SplineParameter->setColumnWidth(5,25);
	}
	this->tV_SplineParameter->setColumnWidth(0,120);

	this->connect(this->tV_SplineParameter,SIGNAL(clicked(const QModelIndex&)),this->dataModel,SLOT(itemClicked(const QModelIndex&)));

}

CCharString Bui_SplineModelDlg::getTitleString()
{
	if (this->splineType == 1)
	{
		return "Orbit Path settings";
	}
	else if (this->splineType == 2)
	{
		return "Orbit Attitude settings";
	}
	else 
		return "";
	

}
Bui_SplineModelDlg::~Bui_SplineModelDlg()
{
	this->cleanUp();
}

void Bui_SplineModelDlg::cleanUp()
{
	
	if (this->dataModel)
		delete this->dataModel;
	this->dataModel;
	
	if (this->spline)
		delete this->spline;
	this->spline = NULL;
	
	


	this->existingSpline->removeListener(this);
	this->existingSpline = NULL;
}


bool Bui_SplineModelDlg::okButtonReleased()
{
	//this->setParameter(0,true);

	if (this->spline)
	{
		this->existingSpline->setDirectObservationsForSpline(this->spline->getDirectObservationsForSpline());
		this->success = true;
	}
	return this->success;
}

bool Bui_SplineModelDlg::cancelButtonReleased()
{
	this->success = false;
	return true;
}

CCharString Bui_SplineModelDlg::helpButtonReleased()
{
	if (this->splineType == 1)
		return"UserGuide/ThumbNodeOrbitPath.html";
	else return "UserGuide/ThumbNodeOrbitAttitudes.html";
}


void Bui_SplineModelDlg::elementsChangedSlot(CLabel element)
{
	if (!this->listenToSignal) 
		return;

	*(this->spline) = *(this->existingSpline);
	this->dataModel->update();
}




// ###############################################################################################
// ###############################################################################################
// ###############################################################################################

CSplineDataModel::CSplineDataModel(CSplineModel* splineModel,SplineModelType splineType,QObject* parent ) 
	: QAbstractTableModel(parent), splineModel(splineModel), splineType(splineType)
{

}


int CSplineDataModel::rowCount(const QModelIndex& parent) const
{
	if (this->splineModel)
	{
		return this->splineModel->getParameterCount(CSensorModel::addPar);
	}
	else
		return 0;
}

int CSplineDataModel::columnCount(const QModelIndex& parent) const
{
	if (this->splineModel && this->splineModel->getUseDirectObservationsForSpline())
		return 6;
	else
		return 3;
}

QVariant CSplineDataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	double value;

	if (role == Qt::TextAlignmentRole)
		return int(Qt::AlignRight | Qt::AlignVCenter);
	else if (( role == Qt::DisplayRole || role == Qt::EditRole || role == Qt::ToolTipRole) && this->splineModel)
	{
		int nCoeffs =  this->splineModel->getDegree() + 1;
		int size = this->splineModel->getNSegments() * nCoeffs ;
		int coordinate = index.row() / size;
		int dummy = index.row() % size ;
		int segment = dummy /  nCoeffs;
		int parameter =  dummy - segment * nCoeffs; 


		if (index.column() == 0)
		{
			CCharStringArray names;
			this->splineModel->getObservationNames(names);
			return QString().sprintf("Segment %d (%s): a%d  ",segment + 1,names.GetAt(coordinate)->GetChar(),parameter);
		}
		else if (index.column() == 1) // value
		{
			double X,Y,Z;
			this->splineModel->getSplineParameter(segment,parameter,X,Y,Z);
			CObsPoint p(3);
			p[0] = X;
			p[1] = Y;
			p[2] = Z;

			this->splineModel->convertToDifferentialUnits(p,p);

			if (coordinate == 0)
				value = p[0];
			else if (coordinate == 1)
				value = p[1];
			else if (coordinate == 2)
				value = p[2];

			if (role == Qt::DisplayRole)
				return QString().sprintf("%.2lf",value);
			else
				return QString("%1").arg(value);
				
		}
		else if (index.column() == 2) // precision
		{
			CObsPoint p(1);
			p[0] = this->splineModel->getSigmaSplineParameter(index.row());

			this->splineModel->convertToDifferentialUnits(p,p);

			if (role == Qt::DisplayRole)
				return QString().sprintf("%.2lf",p[0]);
			else
				return QString("%1").arg(p[0]);
			
		}
		else if (index.column() == 3) // observation
		{
			CObsPoint p(1);
			p[0] = this->splineModel->getDirectObservationsForSpline(parameter,coordinate,segment);
			this->splineModel->convertToDifferentialUnits(p,p);


			if (role == Qt::DisplayRole)
				return QString().sprintf("%.2lf",p[0]);
			else
				return QString("%1").arg(p[0]);
			
			
		}
		else if (index.column() == 4) // sigma
		{
			CObsPoint p(1);
			p[0] = this->splineModel->getSigmaDirectObservationsForSpline(parameter,coordinate,segment);
			this->splineModel->convertToDifferentialUnits(p,p);

			if (role == Qt::DisplayRole)
				return QString().sprintf("%.2lf",p[0]);
			else
				return QString("%1").arg(p[0]);
		}
		else if (index.column() == 5) // use observation
		{
			bool useObs = this->splineModel->getUseDirectObservationForSpline(parameter,coordinate);

			if (role == Qt::ToolTipRole)
				return useObs ? "use direct observation in adjustment" : "don't use direct observation in adjustment";
			else
				return (bool)useObs;

		}

	}

	
	return QVariant();	
}

QVariant CSplineDataModel::headerData(int section,Qt::Orientation orientation, int role) const
{
	
	if (role == Qt::DisplayRole && orientation == Qt::Horizontal && this->splineModel)
	{
		if (section == 0)
			return "Parameter";
		else if (section == 1)
			return (CCharString("Value ") + this->splineModel->diffUnitsString()).GetChar();
		else if (section == 2)
			return (CCharString("Precision ") + this->splineModel->diffUnitsString()).GetChar();
		else if (section == 3)
			return (CCharString("Observation ") + this->splineModel->diffUnitsString()).GetChar();
		else if (section == 4)
			return (CCharString("Sigma ") + this->splineModel->diffUnitsString()).GetChar();
		else if (section == 5)
			return "use";
			
	}

	return QVariant();
}


Qt::ItemFlags CSplineDataModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
	 return Qt::ItemIsEnabled;
	
	bool areActive = false;

	if (this->splineModel->getUseDirectObservationsForSpline())
		areActive = this->data(this->index(index.row(),5),Qt::DisplayRole).toBool();

	if (areActive && (index.column() == 3 || index.column() == 4))
		return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
	else if (!areActive && (index.column() == 3 || index.column() == 4))
		return (Qt::ItemFlags)(QAbstractItemModel::flags(index) - Qt::ItemIsEnabled);
	else if (index.column() == 5 )
		return QAbstractItemModel::flags(index) | Qt::ItemIsUserCheckable;
	else
		return (Qt::ItemFlags)(QAbstractItemModel::flags(index) - Qt::ItemIsSelectable);
}

bool CSplineDataModel::setData(const QModelIndex &index,const QVariant &value, int role)
{
	if (index.isValid() && role == Qt::EditRole && this->splineModel) 
	{
		int nCoeffs =  this->splineModel->getDegree() + 1;
		int size = this->splineModel->getNSegments() * nCoeffs ;
		int coordinate = index.row() / size;
		int dummy = index.row() % size ;
		int segment = dummy /  nCoeffs;
		int parameter =  dummy - segment * nCoeffs; 


		if (index.column() == 3)
		{
			double v = value.toDouble();
			if (this->splineType == Attitude)
				v *= M_PI / 180.0;

			this->splineModel->setDirectObservationsForSpline(parameter,coordinate,segment,v);
		}
		else if (index.column() == 4)
		{
			double v = value.toDouble();
			if (this->splineType == Attitude)
				v *= M_PI / 180.0;
			
			this->splineModel->setSigmaDirectObservationsForSpline(parameter,coordinate,segment,v);
		}
		else if (index.column() == 5)
		{
			this->splineModel->setUseDirectObservationForSpline(parameter,coordinate,value.toBool());
			this->update(); // force full update
			return true;
			
		}
		else 
			return false;

		emit dataChanged(index, index);
		return true;

	}

	return false;
}


void CSplineDataModel::update()
{
	QModelIndex indexTopLeft = this->index(0,0);
	QModelIndex indexBottomRight = this->index(this->rowCount(QModelIndex()) -1,5);

	emit dataChanged(indexTopLeft, indexBottomRight);
}



void CSplineDataModel::itemClicked( const QModelIndex& index)
{
	if (index.column() == 5)
	{
		this->setData(index,!this->data(index,Qt::DisplayRole).toBool());
	}

}

