#include "BaristaViewEventHandler.h"
#include "Bui_MainWindow.h"
#include "MonoPlotter.h"
#include "Trans3DFactory.h"


COpenBaristaViewList CBaristaViewEventHandler::baristaViews;
bool CBaristaViewEventHandler::inGeoLinkMode = false;

bool CBaristaViewEventHandler::handleMiddleMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;
	CPanHandler& panHandler = mainwindow->getPanHandler();

	panHandler.handleMiddleMousePressEvent(e,v);
	
	return true;
}

bool CBaristaViewEventHandler::handleMiddleMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;
	CPanHandler& panHandler = mainwindow->getPanHandler();

	panHandler.handleMiddleMouseReleaseEvent(e,v);

	this->setCurrentCursor(v); 


	return true;
}

bool CBaristaViewEventHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{
	if ( v->getMiddleButtonDown())
	{
		bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;
		CPanHandler& panHandler = mainwindow->getPanHandler();

		panHandler.handleMouseMoveEvent(e,v);


	}
	return true;
}
 
void CBaristaViewEventHandler::handleLeaveEvent ( QEvent * event, CBaristaView *v ) 
{
	this->setCTRLPressed(false);
	this->setStatusBarText("");
}

void CBaristaViewEventHandler::open()
{
	resetFirstSecondClickForAllViews();
	this->setStatusBarText();
};

void CBaristaViewEventHandler::close()
{
	resetFirstSecondClickForAllViews();
};

void CBaristaViewEventHandler::resetFirstSecondClickForAllViews()
{
	for (int i = 0; i < this->baristaViews.getNumberOfViews(); ++i)
	{
		CBaristaView* v= this->baristaViews.getView(i);
		v->resetFirstSecondClick();
		v->setHasROI(false);
		v->setShowRubberLine(false);
		v->setShowROI(false);
	}
};

void CBaristaViewEventHandler::setStatusBarText(QString additionalText)
{
	if (!parent)
		return;

	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;


	QString labelstr(this->statusBarText.GetChar() +additionalText);

	mainwindow->statusBar()->showMessage(labelstr);
}

bool CBaristaViewEventHandler::handleRightMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	C2DPoint xy;
	if (!v->getMouseEventCoords(e, xy.x, xy.y))
		return false;

	v->createPopUpMenu();
	return true;
}

bool CBaristaViewEventHandler::keyPressEvent(QKeyEvent *e, CBaristaView *v)
{
	if ( e->key() == Qt::Key_Shift )
	{
		this->shiftdown = true;
	}
	else if ( e->key() == Qt::Key_Control )
	{
		this->ctrldown = true;
		v->setCTRLPressed(true);
	}

	return true;
}

bool CBaristaViewEventHandler::keyReleaseEvent(QKeyEvent *e, CBaristaView *v)
{
	if ( e->key() == Qt::Key_Shift )
	{
		this->shiftdown = false;
	}
	else if ( e->key() == Qt::Key_Control )
	{
		this->ctrldown = false;
		v->setCTRLPressed(false);
	}

	v->updateStatusBarInfo();

	return true;
}

void CBaristaViewEventHandler::addListenerToOpenViewList(CUpdateListener* listener)
{
	baristaViews.addListener(listener);
}

void CBaristaViewEventHandler::removeListenerFromOpenViewList(CUpdateListener* listener)
{
	baristaViews.removeListener(listener);
}


void CBaristaViewEventHandler::activateGeoLinkMode()
{
	CBaristaViewEventHandler::inGeoLinkMode = true;

	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;
	mainwindow->blockSignals(true);
	mainwindow->geoLinkAction->setChecked(true);
	mainwindow->blockSignals(false);
	baristaViews.activateGeoLinkMode();
}

void CBaristaViewEventHandler::deactivateGeoLinkMode()
{
	CBaristaViewEventHandler::inGeoLinkMode = false;
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->geoLinkAction->setChecked(false);
	mainwindow->blockSignals(false);

	baristaViews.deactivateGeoLinkMode();
}


void CBaristaViewEventHandler::doGeoLink(CBaristaView* view,bool changeResampling)
{
	C2DPoint centre;
	if (!view || !view->computeViewCentre(centre))
		return;

	// try to set an active dem
	if (project.getActiveDEM() == NULL && project.getNumberOfDEMs())
	{
		bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;
		mainwindow->setActiveDEM();
	}	
	
	
	// try mono plotting
	// that only works when the image has a sensor model!
	// if no DEM is available then we will use the mean project height
	// the reference system of the computed 3D point:
	//			with DEM: 3D point is in reference system of DEM
	//			without DEM: 3D point is in reference system of sensor model
	CXYZPoint point3D;
	CMonoPlotter& monoPlotter = view->getMonoPlotter();
	if (monoPlotter.getPoint(point3D, centre, 1.0))
	{
		// get the resolution of a pixel to adjust the zoom accordingly
		float resolution = view->getDataResolution();
		
		// now inform all open views 
		for (int i=0; i < baristaViews.getNumberOfViews(); i++)
		{
			
			CBaristaView* bview = baristaViews.getView(i);
			if (bview != view)
			{
				bview->doGeoLink(point3D,monoPlotter.getReferenceSystem(),view->getZoomFactor(),resolution,changeResampling);
			}
		}
	}
}



bui_MainWindow* CBaristaViewEventHandler::getBaristaMainWindow()
{
	return (bui_MainWindow*)this->parent;
}


bool CBaristaViewEventHandler::isOpen(CVImage* image)const
{
	for (int i=0; i < baristaViews.getNumberOfViews(); i++)
	{
		CBaristaView* bview = baristaViews.getView(i);
		if (bview && bview->getVImage() == image)
			return true;
	}

	return false;
}

CBaristaViewEventHandler *currentBaristaViewHandler = 0;