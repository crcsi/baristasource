#include "ObjectTable.h"

#include "ObsPointDataModel.h"
#include "ObsPointArray.h"
#include <QHeaderView>
#include <qcommonstyle.h>
#include <QMouseEvent>

#include "EditorDelegate.h"


CObjectTable::CObjectTable(CObjectDataModel* dataModel,QWidget* parent):
	QTableView(parent),proxyModel(0),objectDimension(0) // QTableView class provides a default model/view implementation of a table view.
{// proxyModel is a CObjectSortFilterModel (QSortFilterProxyModel) class that works between model and view/delegate to work certain works on data, for exmaple, sorting, filtering 
	if (!dataModel)
		return;

	this->setAttribute(Qt::WA_DeleteOnClose);
	this->verticalHeader()->setDefaultSectionSize(ROW_HEIGHT_TABLE);
	this->setStyle(new QCommonStyle());

	this->objectDimension = dataModel->getDimension();

	this->proxyModel = new CObjectSortFilterModel(dataModel);
 	this->setModel(this->proxyModel); // table model is set as the proxymodel (QSortFilterProxyModel)

	this->setShowSelector(dataModel->getViewParameter().showSelector);
	this->setShowSigma(dataModel->getViewParameter().showSigma);
	this->setShowCoordinates(dataModel->getViewParameter().showCoordinates);
	this->setAllowSigmaChange(dataModel->getViewParameter().allowSigmaChange);
	this->setAllowDeletePoints(dataModel->getViewParameter().allowDeletePoint);
	this->setAllowChageLabels(dataModel->getViewParameter().allowLabelChange);
	this->proxyModel->setAllowBestFitEllipse(dataModel->getViewParameter().allowBestFitEllipse);
	this->proxyModel->setAllowXYZAveragePoints(dataModel->getViewParameter().allowXYZAveragePoints);


	this->setSelectionBehavior(QAbstractItemView::SelectRows);
	this->connect(dataModel,SIGNAL(dataModelChanged(DataModelChanges,vector<QString>)),this,SLOT(dataModelChanged(DataModelChanges,vector<QString>)));
	this->proxyModel->requestWindowTitle();

	// sorting stuff
	this->proxyModel->setSortRole(SortingRole);
	this->proxyModel->setDynamicSortFilter(true);
	this->setSortingEnabled(true); 
	this->sortByColumn(this->proxyModel->getNameColumn(),Qt::AscendingOrder);
}

CObjectTable::~CObjectTable()
{
	this->deleteMemory();
}


void CObjectTable::setFormatString(const QStringList& formats)
{
	this->proxyModel->setFormatString(formats);
}

void CObjectTable::setShowSelector(bool show)
{
	this->proxyModel->setShowSelector(show);

	if (show)
	{
		this->setColumnWidth(0,27);
		this->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);
	}
	else
		this->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);


	for (int i=1; i< this->proxyModel->columnCount(QModelIndex()); i++)
		this->horizontalHeader()->setSectionResizeMode(i,QHeaderView::Stretch);

	
}

void CObjectTable::setShowSigma(bool show)
{
	this->proxyModel->setShowSigma(show);
}


void CObjectTable::setShowCoordinates(bool show)
{
	this->proxyModel->setShowCoordinates(show);
}

void CObjectTable::setAllowSigmaChange(bool allow)
{
	if (allow)
	{
		vector<int> sigmaColumns = this->proxyModel->getSigmaColumns();
		for (unsigned int i=0; i < sigmaColumns.size(); i++)
			this->setItemDelegateForColumn(sigmaColumns[i],new CEditorDelegate(this,CEditorDelegate::eDoubleValidator));
	}

	this->proxyModel->setAllowSigmaChange(allow);
}

void CObjectTable::mouseDoubleClickEvent ( QMouseEvent * event )
{
	QTableView::mouseDoubleClickEvent(event);
}; 

void CObjectTable::mouseReleaseEvent(QMouseEvent * mouseEvent)
{
	if (mouseEvent->button() == Qt::RightButton)
	{
		this->proxyModel->rightMouseButtonReleased(this->selectedIndexes(),this->actionList);
	}

	return QTableView::mouseReleaseEvent(mouseEvent);
}

void CObjectTable::setHeader(const QStringList& headerList)
{
	this->proxyModel->setHeader(headerList);
}

void CObjectTable::dataModelChanged(DataModelChanges changeType,vector<QString> parameter)
{
	switch (changeType)
	{
		case eObjectName:	{ 
							if (parameter.size() != 3)
								break;

							QString title(parameter[0] + parameter[1]) ;
							this->setWindowTitle(title);
							this->setAccessibleName(parameter[2]);
							
							break;
						}
		default: {} // do nothing
	}
	
}

void CObjectTable::setAllowDeletePoints(bool allow)
{
	this->proxyModel->setAllowDeletePoints(allow);
}

void CObjectTable::setAllowChageLabels(bool allow)
{
	this->proxyModel->setAllowChangeLabels(allow);
}

void CObjectTable::closeEvent(QCloseEvent *event)
{
	this->deleteMemory();
}

void CObjectTable::deleteMemory()
{
	if (this->proxyModel)
		delete this->proxyModel;
	this->proxyModel = NULL;
}

void CObjectTable::setFilterRegExp(QString regExp)
{
	this->proxyModel->setFilterRegExp(regExp);
}

CObsPoint*  CObjectTable::getObsPoint(const QModelIndex& index)
{
	if (index.isValid())
		return this->proxyModel->getObsPoint(index);
	else
		return NULL;
}