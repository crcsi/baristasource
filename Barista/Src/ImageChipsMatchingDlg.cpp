#include "ImageChipsMatchingDlg.h"
#include "Icons.h"
#include "BaristaProject.h"
#include "IniFile.h"
#include "SystemUtilities.h"
#include <QFileDialog>
#include <QMessageBox>
#include "BaristaProjectHelper.h"
#include "Trans3DFactory.h"
#include <time.h>
#include "Bui_MatchingParametersDlg.h"

ImageChipsMatchingDlg::ImageChipsMatchingDlg(QWidget *parent)
	: QDialog(parent)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->originalChipSelectionArray.clear();
	this->currentChipSelectionArray.clear();
	this->isImageSelected.clear();

	//this->possibleOriginalImages.clear();
	this->nImages = 0;
	this->nSelection = 0;
	this->success = false;

	intiImages();
	initChipSelection();
	this->currentChipSelectionArray = this->originalChipSelectionArray;
	setProjectChipSelectionArray();
	//initDEMs();

	this->lEdbFilename->setText(project.getChipDbFileName()->GetChar());
	this->lEdbFilename->setEnabled(false);
	this->cBSensor->setEnabled(false);
	this->dSBminRes->setEnabled(false);
	this->dSBmaxRes->setEnabled(false);
	this->dEFrom->setEnabled(false);
	this->dETo->setEnabled(false);
	this->sBnGCPs->setEnabled(false);
	this->tWchips->setEditTriggers(0);//no edit possible
	this->tWchips->setSelectionBehavior(QAbstractItemView::SelectRows); //only rows are selected
	this->tWchips->setSelectionMode(QAbstractItemView::SingleSelection); // single item at a time
	this->tWchips->setAlternatingRowColors(true);

	this->minRes = 1000;
	this->maxRes = -1000;
	this->dateFrom = QDate::currentDate();
	this->dateTo.fromString("01/01/2000","dd/MM/yyyy");
	this->nGCPs = 10;

	initChips();
	initSelectionCriteria();
	initMatchingParameters();
	//images
	//connect(this->lWImages,					SIGNAL(currentRowChanged (int)),         this, SLOT(ImageSelected(int)));
	connect(this->lWImages,						SIGNAL(itemClicked (QListWidgetItem *)),	this, SLOT(ImageClicked(QListWidgetItem *)));
	
	//chip database
	connect(this->pBdbFilename,					SIGNAL(released()),							this,SLOT(pBFileNameReleased()));
	connect(this->ckBSensor,					SIGNAL(stateChanged(int)),					this,SLOT(ckBSensorChanged()));         //signal: currentIndexChanged ( int index )
	connect(this->ckBResolution,				SIGNAL(stateChanged(int)),					this,SLOT(ckBResolutionChanged())); //signal: editingFinished () 
	connect(this->ckBDate,						SIGNAL(stateChanged(int)),					this,SLOT(ckBDateChanged()));             //signal: void dateChanged ( const QDate & date ) 
	connect(this->ckBnGCPs,						SIGNAL(stateChanged(int)),					this,SLOT(ckBnGCPsChanged())); //signal: editingFinished () 
	connect(this->dSBminRes,					SIGNAL(valueChanged(double)),				this,SLOT(minResChanged()));
	connect(this->dSBmaxRes,					SIGNAL(valueChanged(double)),				this,SLOT(maxResChanged()));
	connect(this->pBapplySettings,				SIGNAL(released()),							this,SLOT(pBapplySettingsReleased()));
	connect(this->sBnGCPs,						SIGNAL(valueChanged(int)),					this,SLOT(nGCPsChanged()));

	//matching parameters
	connect(this->ckBWallisFilter,				SIGNAL(stateChanged(int)),					this, SLOT(WallisFilterClicked()));
	connect(this->lEpatchSize,					SIGNAL(editingFinished()),					this, SLOT(PatchSizeChanged()));
	connect(this->lEminCorrelation,				SIGNAL(editingFinished()),					this, SLOT(RhoMinChanged()));
	connect(this->lEsizeOfSearchWndow,			SIGNAL(editingFinished()),					this, SLOT(SearchAreaChanged()));
	connect(this->pBMatchPar,					SIGNAL(released()),							this, SLOT(MatchParsSelected()));

	//control
	connect(this->pBOK,							SIGNAL(released()),							this, SLOT(pBOKclicked()));
	connect(this->pBCancel,						SIGNAL(released()),							this, SLOT(pBCancelclicked()));
}

ImageChipsMatchingDlg::~ImageChipsMatchingDlg()
{

}

void ImageChipsMatchingDlg::intiImages()
{
	int count = 0;
	for (int i = 0; i < project.getNumberOfImages(); ++i)
	{		
		CVImage *img = project.getImageAt(i);
		if (img->getCurrentSensorModel()) 
		{	//this image is by default selected 
			//this->possibleOriginalImages.push_back(img);
			this->nImages++;

			//To show this image in the image selector box: QListWidget
			CCharString name = img->getFileNameChar();
			
			//create an QList item
			QListWidgetItem* item = new QListWidgetItem(name.GetChar());
			item->setData(Qt::UserRole,i); // remember index into array
			item->setCheckState(Qt::Checked);
			item->setToolTip(name.GetChar());

			this->lWImages->addItem(item);
			item->setCheckState(Qt::Checked);

			this->isImageSelected.push_back(true);
			this->ImgSelected[count++] = true;
		}
	}
}

/*void ImageChipsMatchingDlg::initDEMs()
{
	if (project.getNumberOfDEMs() <= 0) this->pDEM = 0;
	else
	{
		this->cBoxDEM->blockSignals(true);
		CVDEM *pActiveDEM = project.getActiveDEM();

		int selected = 0;
		
		for (int i = 0; i < project.getNumberOfDEMs(); ++i)
		{
			CVDEM *dem = project.getDEMAt(i);
			this->cBoxDEM->addItem(dem->getFileName()->GetChar());
			if (pActiveDEM == dem) selected = i;
		}

		this->cBoxDEM->setCurrentIndex(selected);
		this->pDEM = project.getDEMAt(selected);
		
		this->cBoxDEM->setToolTip(this->pDEM->getFileName()->GetChar());
		
		this->cBoxDEM->blockSignals(false);
	}
}*/
void ImageChipsMatchingDlg::initChipSelection()
{
	vector <bool> sel;
	//dummy.clear();
	CXYZPointArray* gcpDBPointsFrom = project.getGCPchipPoints();
	CXYZPointArray gcpDBPointsTo;
	//CXYPointArray gcpDB2dPoints;
	//select chips based on the selected images
	for (int i = 0; i < this->nImages; ++i)
	{
		//if (this->lWImages->item(i)->checkState() == Qt::Checked) 
		//{
			sel.clear();
			QListWidgetItem *itm = this->lWImages->item(i);
			QString itemName = itm->text();
			CCharString itemNameStr((CCharString)itemName.toLatin1());
			CVImage *image = project.getImageByName(&itemNameStr);
			//selectedImgs.push_back(image);
			CReferenceSystem refSys = image->getCurrentSensorModel()->getRefSys();
			CSensorModel *model = image->getCurrentSensorModel();
			//refSys.getCoordinateType();
			//refSys.gri
			CTrans3D *trafo = CTrans3DFactory::initTransformation(gcpDBPointsFrom->getRefSys(), model->getRefSys()); //convert GCP DB point type (eGEOCENTRIC) to image point type

			if (trafo)
			{
				trafo->transformArray(&gcpDBPointsTo,gcpDBPointsFrom);
				if (gcpDBPointsTo)
				{
					CXYPoint tmp;
					for (int j = 0; j < gcpDBPointsTo.GetSize(); j++)
					{
						model->transformObj2Obs(tmp,gcpDBPointsTo.getAt(j));
						//gcpDB2dPoints.add(tmp);
						if (tmp.x >= 0 && tmp.x < image->getHugeImage()->getWidth() && tmp.y >= 0 && tmp.y < image->getHugeImage()->getHeight())
							sel.push_back(true);
						else
							sel.push_back(false);
					}

				}
			}
			this->originalChipSelectionArray.push_back(sel);
			//refSys.
		}
	//}
}

void ImageChipsMatchingDlg::initChips()
{
	vector <bool> *selection = project.getChipSelectionArray();
	this->tWchips->clear();	
	if (project.getChipDbFileName()->GetLength() > 0)
    {
		this->tWchips->setColumnCount(4);
		this->tWchips->setRowCount(this->nSelection);
		//this->tWchips->setEnabled(false);
		QStringList colHeaders;
		colHeaders << "Chip ID" << "Sensor" << "Date" << "Resolution";// << "Size" << "Resolution" << "Bands"; 
		this->tWchips->setHorizontalHeaderLabels(colHeaders);
		//this->tWchips->setColumnWidth(1,15);
		//this->tWchips->setColumnWidth(2,10);
		//this->tWchips->setColumnWidth(3,5);

		vector <CGCPDataBasePoint> *gcpDB = project.getGCPdb();
		vector<CGCPDataBasePoint>::iterator it3d;
		int i = 0, j = 0; 
		for (it3d = gcpDB->begin() ; it3d < gcpDB->end(); it3d++ )
		{
			//this->tWchips->insertRow(i);

			//access 2d point array from 3d vector, that means from gcpDB
			vector<CGCPData2DPoint> P2ds = it3d->getGCPPoint2DArray();

			//now go through all 2d elements
			vector<CGCPData2DPoint>::iterator it;
			for (it = P2ds.begin() ; it < P2ds.end(); it++) // if this was set true to be selected
			{				
				if (selection->at(j))
				{
					//this->tWchips->insertRow(i);
					QTableWidgetItem * chipID = new QTableWidgetItem(it->getChipID().GetChar());
					QTableWidgetItem * sensor = new QTableWidgetItem(it->getSensor().GetChar());
					QTableWidgetItem * date = new QTableWidgetItem(it->getAcquiredDate().GetChar());
					
					QString ds = QString("%1").arg(it->getQualityOfDefinition(), 0, 'f', 1);
					QTableWidgetItem * resol = new QTableWidgetItem(ds);

					//QTableWidgetItem * size = new QTableWidgetItem(csize);
					//QTableWidgetItem * resolution = new QTableWidgetItem(cres);
					//QTableWidgetItem * nband = new QTableWidgetItem(cnband);				

					this->tWchips->setItem(i,0,chipID);
					this->tWchips->setItem(i,1,sensor);
					this->tWchips->setItem(i,2,date);
					this->tWchips->setItem(i,3,resol);
					//this->tWchips->setItem(i,3,size);
					//this->tWchips->setItem(i,4,resolution);
					//this->tWchips->setItem(i,5,nband);

					// find min and max resolutions among all chips
					if (it->getQualityOfDefinition() < this->minRes)
						this->minRes = it->getQualityOfDefinition();
					if (it->getQualityOfDefinition() > this->maxRes)
						this->maxRes = it->getQualityOfDefinition();

					// find earliest and latest acquire dates among all chips
					QDate acquireDate = QDate::fromString(it->getAcquiredDate().GetChar(),"dd/MM/yyyy");
					if (acquireDate < this->dateFrom)
						this->dateFrom = acquireDate;
					if (acquireDate > this->dateTo)
						this->dateTo = acquireDate;

					//fprintf(fp,"%s,%f,%f,%f,%f,%f,%f,%s,%s,%s,%s,%f,%f,%f,%f,%s,%s,%s,%s,%s,%d,%s,%d,%s,%s,%s,%d,%s\n",it->getChipID().GetChar(),it3d->getX(),it3d->getY(),it3d->getZ(),it3d->getSX(),it3d->getSY(),it3d->getSZ(),it->getPlatform().GetChar(),it->getSensor().GetChar(),it->getAcquiredDate().GetChar(),it->getDateCreation().GetChar(),it->getQualityOfDefinition(),it->getViewAngle(),it->getAccuracy(),it->getAccuracyZ(),it->getUnit().GetChar(),it->getSourceOfHeight().GetChar(),it->gethDatum().GetChar(),it->getvDatum().GetChar(),it->getProjection().GetChar(),it->getZone(),it->getepsgCode().GetChar(),it->getchipSize(),it->getdataProvider().GetChar(),it->getLicense().GetChar(),(const char*)it->getOrthoImageFileName(),it->getnBands(),it->getComments().GetChar());
					i++;					
					//selection.push_back(true);
				}
				j++;
			}
			//j++;
		}//for it3 ends

    }
    else
    {        
		QMessageBox::warning(this, "No chip database is currenly open!"	 , "Open or create a new chip database first!");
		this->pBOK->setEnabled(false);
    }
	//project.setChipSelectionArray(&selection);
}

void ImageChipsMatchingDlg::initSelectionCriteria()
{
	//sensor
	QStringList sensorList;
	sensorList << "(All)" << "2xHRG" << "2xHRVIR" << "AVNIR-2"  << "Ikonos-2" << "HRS" << "PRISM" << "QuickBird-2";
	this->cBSensor->insertItems(0,sensorList);
	this->cBSensor->setCurrentIndex(0);

	//acquisition dates
	this->dEFrom->setDate(this->dateFrom);
	this->dETo->setDate(this->dateTo);

	//resolution
	//char minResBuf[5], maxResBuf[5];
	//sprintf(minResBuf,"%2.2f",this->minRes);
	//sprintf(maxResBuf,"%2.2f",this->maxRes);
	this->dSBminRes->setRange(0.0,99.9);
	this->dSBminRes->setSingleStep(0.1);
	this->dSBminRes->setDecimals(1);
	this->dSBminRes->setValue(this->minRes);
	this->dSBmaxRes->setRange(0.0,99.9);
	this->dSBmaxRes->setSingleStep(0.1);
	this->dSBmaxRes->setDecimals(1);
	this->dSBmaxRes->setValue(this->maxRes);
	this->dSBminRes->setMaximum(this->dSBmaxRes->value()-0.1);
	this->dSBmaxRes->setMinimum(this->dSBminRes->value()+0.1);

	//number of GCPs per image
	//char nGCPsBuf[4];
	//sprintf(nGCPsBuf,"%d",this->nGCPs);
	this->sBnGCPs->setRange(0,999);
	this->sBnGCPs->setSingleStep(1);
	this->sBnGCPs->setValue(this->nGCPs);
}

void ImageChipsMatchingDlg::ImageClicked(QListWidgetItem *item)
{	
 	int index = item->data(Qt::UserRole).toInt();

	if (item->checkState() == Qt::Checked && !this->ImgSelected[index]) //!this->isImageSelected.at(index)) 
	{
		this->ImgSelected[index] = true;
		this->setProjectChipSelectionArray();
		this->initChips();
	}
	else if (item->checkState() != Qt::Checked && this->ImgSelected[index]) //this->isImageSelected.at(index)) 
	{
		this->ImgSelected[index] = false;
		this->setProjectChipSelectionArray();
		this->initChips();
	}

	if (index < this->nImages)
		{		
			vector<CVImage *> selectedImgs;

			/*QList <QListWidgetItem *> list = lWImages->selectedItems();

			for (int i = 0; i < list.size(); i++)
			{
				QListWidgetItem *itm = list.at(i);
				QString itemName = itm->text();
				CCharString itemNameStr((CCharString)itemName.toLatin1());
				CVImage *image = project.getImageByName(&itemNameStr);
				selectedImgs.push_back(image);
			}*/
			for (int i = 0; i < this->nImages; ++i)
			{
				if (this->lWImages->item(i)->checkState() == Qt::Checked) 
				{
					QListWidgetItem *itm = this->lWImages->item(i);
					QString itemName = itm->text();
					CCharString itemNameStr((CCharString)itemName.toLatin1());
					CVImage *image = project.getImageByName(&itemNameStr);
					selectedImgs.push_back(image);
				}
			}

			if (selectedImgs.size() > 0)
			{
		//		this->pGCPmatcher->setImageVector(selectedImgs, this->pDEM, this->ScenesBridged, this->sceneSizePercentage);
		//		this->initPossibleOrthos();
		//		this->setFEXParGUI();
			}
			else 
			{
			}
		}

		//this->checkSetup();
}

void ImageChipsMatchingDlg::pBFileNameReleased()
{
	if (ImageChipsMatchingDlg::closeChipDbase())
    {
		baristaProjectHelper.setIsBusy(true);
		this->setCursor(Qt::BusyCursor);
		
		CFileName *projectFileName = project.getFileName();
		CCharString projectPath = projectFileName->GetPath();

		//select a file
		QString title("Choose a GCP database file");
	    // file dialog
		QString filename = QFileDialog::getOpenFileName(
			this,
			title,
			//"Choose a 2D Point file",
			(QString)projectPath,
			"Chip datavase (*.cdb)");

		// make sure the file exists
		QFile test(filename);
		if (!test.exists())
			return;
		test.close();
 
		CFileName cfilename = (const char*)filename.toLatin1();

		bool readSuccess = project.openGCPDbase(cfilename);

		if (readSuccess)
		{
			project.setChipDbFileName(cfilename);			
			//this->SaveSCPDatabaseAction->setEnabled(true);
			//this->CloseSCPDatabaseAction->setEnabled(true);
			baristaProjectHelper.refreshTree();	
		}
		else
		{
			QMessageBox::warning(this, "Chip database!"	 , "Openning fails!");
		}
		this->setCursor(Qt::ArrowCursor);
		baristaProjectHelper.setIsBusy(false);

		this->lEdbFilename->setText(project.getChipDbFileName()->GetChar());

		initChips();
    }
}


///////////////////////////////////////////////////////////////
bool ImageChipsMatchingDlg::closeChipDbase()
{

	if (!project.getChipDbFileName()->GetLength())
		return true;


	//this->saved = false;

	CCharString text("Do you want to save chip database\n \"");
	text += project.getChipDbFileName()->GetFullFileName();
	text += "\" before closing?";
	switch(QMessageBox::information(this, "  Barista chip database ",
							text.GetChar(),
							"&Yes", "&No", "&Cancel",
							0, // Enter == button 0
							2))

	{ // Escape == button 2
		case 0:	 // Save clicked or Alt+S pressed or Enter pressed.
				this->chipDbFileSave();
				//if (this->saved)
				//	this->xPressed = false;
				break;
		case 1: // Discard/No clicked or Alt+D pressed
				//this->xPressed = false;
				break;
		case 2: // Cancel clicked or Escape pressed
				// don't exit
				return false;
	}

	//currentBaristaViewHandler->close();

	//this->closeAllWindows();
	//this->initBaristaEmpty();
	//baristaProjectHelper.refreshTree();
	project.setChipDbFileName("");	
	project.deleteGCPchipPoints(project.getGCPchipPoints());
	return true;
}

void ImageChipsMatchingDlg::chipDbFileSave()
{

    if (project.getChipDbFileName()->GetLength() > 0)
    {
		baristaProjectHelper.setIsBusy(true);
		this->setCursor(Qt::BusyCursor);
		//QString s(project.getChipDbFileName()->GetChar());

        //QFile test(s);
		
		//CFileName cfile((const char*)s.toLatin1());
		//project.writeProject(cfile);

		//inifile.setCurrDir(cfile.GetPath());
		//inifile.writeIniFile();
		//this->saved = true;

		project.saveGCPDbase();

		this->setCursor(Qt::ArrowCursor);
		baristaProjectHelper.setIsBusy(false);
    }
    else
    {
        //this->fileSaveAs();
		//QMessageBox::warning(this, "No chip database is currenly open!"	 , "Open or create a new chip database first!");
    }
}

void ImageChipsMatchingDlg::ckBSensorChanged()
{
	if (this->ckBSensor->isChecked())
	{
		this->cBSensor->setEnabled(true);
	}
	else
	{
		this->cBSensor->setEnabled(false);
	}

}

void ImageChipsMatchingDlg::ckBResolutionChanged()
{
	if (this->ckBResolution->isChecked())
	{
		this->dSBminRes->setEnabled(true);
		this->dSBmaxRes->setEnabled(true);
	}
	else
	{
		this->dSBminRes->setEnabled(false);
		this->dSBmaxRes->setEnabled(false);
	}

}

void ImageChipsMatchingDlg::ckBDateChanged()
{
	if (this->ckBDate->isChecked())
	{
		this->dEFrom->setEnabled(true);
		this->dETo->setEnabled(true);
	}
	else
	{
		this->dEFrom->setEnabled(false);
		this->dETo->setEnabled(false);
	}

}

void ImageChipsMatchingDlg::ckBnGCPsChanged()
{
	if (this->ckBnGCPs->isChecked())
	{
		this->sBnGCPs->setEnabled(true);
		
	}
	else
	{
		this->sBnGCPs->setEnabled(false);
	}
}


void ImageChipsMatchingDlg::setProjectChipSelectionArray()
{
	//vector <bool> *selection = project.getChipSelectionArray();
	//selection->clear();
	vector <bool> newSelection;
	newSelection.clear();
	this->nSelection = 0;

	unsigned int nImages = this->currentChipSelectionArray.size();
	unsigned int nChips;
	if (nImages > 0) 
		nChips = this->currentChipSelectionArray.at(0).size();
	else
		nChips = 0;

	for (unsigned int j = 0; j < nChips; j++)
	{
		bool flag = false;
		for (unsigned int i = 0; i < nImages; i++)
		{
			if (this->ImgSelected[i])
			{
				flag |= this->currentChipSelectionArray.at(i).at(j);
				if (flag)
				{
					this->nSelection++;
					break;
				}
			}
		}
		newSelection.push_back(flag);
	}
	
	project.setChipSelectionArray(&newSelection);
}

void ImageChipsMatchingDlg::minResChanged()
{	
	this->dSBmaxRes->setMinimum(this->dSBminRes->value()+0.1);
}

void ImageChipsMatchingDlg::maxResChanged()
{
	this->dSBminRes->setMaximum(this->dSBmaxRes->value()-0.1);
}

void ImageChipsMatchingDlg::pBapplySettingsReleased()
{
	//unsigned int **sel = 0;
	//sel = new unsigned int*[this->nImages];
	//for( int i = 0 ; i < this->nImages; i++ )
	//	sel[i] = new unsigned int[this->nGCPs+1]; // first col contains total and then others contain indices for selected chips;	

	vector <vector<unsigned int>> selInt;
	selInt.clear();

	this->currentChipSelectionArray.clear();
	//unsigned int sel[10][10];
	vector <bool> dummy;
	if (this->ckBSensor->isChecked() || this->ckBResolution->isChecked() || this->ckBDate->isChecked() || this->ckBnGCPs->isChecked())
	{		
		for (int i = 0; i < this->nImages; i++) // for all images
		{
			vector<unsigned int> selIn;
			selIn.clear();
			vector <CGCPDataBasePoint> *gcpDB = project.getGCPdb();
			vector<CGCPDataBasePoint>::iterator it3d;
			int j = 0; vector <bool> selection;
			selection.clear(); //sel[i][0] = 0; 
			dummy.clear(); selIn.push_back(0);
			for (it3d = gcpDB->begin() ; it3d < gcpDB->end(); it3d++ )
			{
				//this->tWchips->insertRow(i);

				//access 2d point array from 3d vector, that means from gcpDB
				vector<CGCPData2DPoint> P2ds = it3d->getGCPPoint2DArray();

				//now go through all 2d elements
				vector<CGCPData2DPoint>::iterator it;
				for (it = P2ds.begin() ; it < P2ds.end(); it++) // if this was set true to be selected
				{	
					bool flag, flagSensor = false, flagResolution = false, flagDate = false;
					if (this->originalChipSelectionArray.at(i).at(j)) // if this chip is within this image
					{
						//now check individual selection criteria
						//check sensor
						if (this->ckBSensor->isChecked())
						{
							if (this->cBSensor->currentIndex() == 0)
								flagSensor = true;
							else
							{
								CCharString sensorSelected = (CCharString)this->cBSensor->currentText().toLatin1();
								CCharString sensorOfChip = it->getSensor();
								if (sensorSelected.CompareNoCase(sensorOfChip))
									flagSensor = true;
							}
						}
						else
							flagSensor = true;


						//check resolution
						if (this->ckBResolution->isChecked())
						{
							double res = it->getQualityOfDefinition();
							double minReso = this->dSBminRes->value();
							double maxReso = this->dSBmaxRes->value();

							if (res >= minReso && res <= maxReso)
								flagResolution = true;
						}
						else
							flagResolution = true;

						//check acquire date
						if (this->ckBDate->isChecked())
						{
							QDate date = QDate::fromString(it->getAcquiredDate().GetChar(),"dd/MM/yyyy");
							QDate startDate = this->dEFrom->date();
							QDate endDate = this->dETo->date();
							if (date >= startDate && date <= endDate)
								flagDate = true;

						}
						else
							flagDate = true;
					}					
					flag = flagSensor & flagResolution & flagDate;
					selection.push_back(flag);
					if (flag)
					{
						//sel[i][0] += 1;
						//sel[i][sel[i][0]] = j;

						selIn.at(0) += 1;
						selIn.push_back(j);						
					}
					j++;
					dummy.push_back(false);
				}
				//j++;
			}//for it3 ends
			this->currentChipSelectionArray.push_back(selection);
			selInt.push_back(selIn);
		}//number of images loop
	}
	else
		this->currentChipSelectionArray = this->originalChipSelectionArray;

	// select max nGCPs
	vector <bool> newSelection;
	if (this->ckBnGCPs->isChecked())
	{			
		//vector <bool> dummy(this->originalChipSelectionArray.at(0).size(),false);	
		for( int i = 0 ; i < this->nImages; i++ )
		{
			//if (sel[i][0] > this->nGCPs)
			if (selInt.at(i).at(0) > this->nGCPs)
			{
				//this->currentChipSelectionArray.at(i).clear(); //clear it
				newSelection = dummy;
				srand(time(NULL));
				int randomNum, index;
				for (unsigned int j = 0; j < this->nGCPs; j++) // make selection
				{
					//randomNum = rand() % sel[i][0] + 1; // generate ranom number in range [1,sel[i][0]]
					randomNum = rand() % selInt.at(i).at(0) + 1; // generate ranom number in range [1,sel[i][0]]
					//index = sel[i][randomNum];
					index = selInt.at(i).at(randomNum);
					newSelection.at(index) = true;
					//this->currentChipSelectionArray.at(i).at(index) = false;
				}
				this->currentChipSelectionArray.at(i) = newSelection;
				//newSelection.clear();
			}
		}		
		//dummy.clear();
	}	
	
	//if (sel)
	//{
	//	for( int i = 0 ; i < this->nImages; i++ )
	//		delete[] sel[i] ;
	//	delete[] sel;
	//	sel = NULL;
	//}
	this->setProjectChipSelectionArray();
	this->initChips();	
}

//================================================================================

void ImageChipsMatchingDlg::WallisFilterClicked()
{
	this->ckBWallisFilter->blockSignals(true);

	this->useWallis = !this->useWallis;
	this->ckBWallisFilter->setChecked(this->useWallis);
	project.setUseWallis4ChipMatching(this->useWallis);
	this->ckBWallisFilter->blockSignals(false);
}

void ImageChipsMatchingDlg::PatchSizeChanged()
{
	//if (this->pGCPmatcher)
	//{
		bool isOK;
	
		int newSize = this->lEpatchSize->text().toInt(&isOK);

		if (isOK && (newSize > 0))
		{
			CMatchingParameters pars = project.getMatchingParameter();
			pars.setPatchSize(newSize);
			//this->pGCPmatcher->setMatchPars(pars);
			this->setMatchParGUI(pars);
		}
	//}
}

//================================================================================

void ImageChipsMatchingDlg::setMatchParGUI(const CMatchingParameters &pars)
{
	this->lEpatchSize->blockSignals(true);
	this->lEminCorrelation->blockSignals(true);
	this->lEsizeOfSearchWndow->blockSignals(true);
	this->ckBWallisFilter->blockSignals(true);

	QString ds;
	
	ds=QString("%1").arg(pars.getPatchSize());                 this->lEpatchSize->setText(ds); 		
	ds=QString("%1").arg(pars.getRhoMin() * 100.0, 0, 'f', 1); this->lEminCorrelation->setText(ds); 
	ds=QString("%1").arg(pars.getSigmaGeom(),      0, 'f', 0); this->lEsizeOfSearchWndow->setText(ds); 

	this->ckBWallisFilter->setChecked(this->useWallis);
	this->lEpatchSize->blockSignals(false);
	this->lEminCorrelation->blockSignals(false);
	this->lEsizeOfSearchWndow->blockSignals(false);
	this->ckBWallisFilter->blockSignals(false);
}

//================================================================================

void ImageChipsMatchingDlg::RhoMinChanged()
{
	//if (this->pGCPmatcher)
	//{
		bool isOK;
	
		double newRho = this->lEminCorrelation->text().toDouble(&isOK) * 0.01;

		if (isOK && (newRho > 0) && (newRho <= 1.0))
		{
			CMatchingParameters pars = project.getMatchingParameter();
			pars.setRhoMin(newRho);
			//this->pGCPmatcher->setMatchPars(pars);
			this->setMatchParGUI(pars);
		}
	//}
}

//================================================================================

void ImageChipsMatchingDlg::SearchAreaChanged()
{
	//if (this->pGCPmatcher)
	//{
		bool isOK;

		double newSA = this->lEsizeOfSearchWndow->text().toDouble(&isOK);

		if (isOK && (newSA > 0.0))
		{
			CMatchingParameters pars = project.getMatchingParameter();
			pars.setSigmaGeom(newSA);
			//this->pGCPmatcher->setMatchPars(pars);
			this->setMatchParGUI(pars);
		}
	//}
}

//================================================================================

void ImageChipsMatchingDlg::MatchParsSelected()
{
	//if (this->pGCPmatcher)
	//{	
		CMatchingParameters pars = project.getMatchingParameter();
		Bui_MatchingParametersDlg dlg(this, &(pars));

		dlg.exec();

		if (dlg.getParsChanged())
		{
			//pars = project.getMatchingParameter();
			//this->pGCPmatcher->setMatchPars(pars);
			this->setMatchParGUI(pars);
		}
	//}
}

void ImageChipsMatchingDlg::pBOKclicked()
{
	this->success = true;
	this->close();
}

void ImageChipsMatchingDlg::pBCancelclicked()
{
	this->success = false;
	this->close();
}

void ImageChipsMatchingDlg::initMatchingParameters()
{
	CMatchingParameters pars = project.getMatchingParameter();
	this->useWallis = false;
	this->ckBWallisFilter->setChecked(this->useWallis);

	QString ds;
	
	ds=QString("%1").arg(pars.getPatchSize());                 this->lEpatchSize->setText(ds); 		
	ds=QString("%1").arg(pars.getRhoMin() * 100.0, 0, 'f', 1); this->lEminCorrelation->setText(ds); 
	ds=QString("%1").arg(pars.getSigmaGeom(),      0, 'f', 0); this->lEsizeOfSearchWndow->setText(ds); 

}

void ImageChipsMatchingDlg::nGCPsChanged()
{
	this->nGCPs = this->sBnGCPs->value();
}