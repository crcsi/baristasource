#include "ZoomInHandler.h"
#include "Bui_MainWindow.h"
#include "ImageView.h"

CZoomInHandler::CZoomInHandler(void)
{
	this->statusBarText = "ZOOMINMODE ";
}

CZoomInHandler::~CZoomInHandler(void)
{
}


bool CZoomInHandler::handleMouseMoveEvent(QMouseEvent *e, CBaristaView *v)
{
	return CBaristaViewEventHandler::handleMouseMoveEvent(e, v);
};


bool CZoomInHandler::handleLeftMousePressEvent(QMouseEvent *e, CBaristaView *v)
{
	return false;
};

bool CZoomInHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	int x = v->getXPosPressed(); 
	int y = v->getYPosPressed(); 

	v->zoomUp();

	int dx = v->getXPressed();
	int dy = v->getYPressed();

	v->setScrollX(x*2 + dx);
	v->setScrollY(y*2 + dy);

	if (this->inGeoLinkMode)
		this->doGeoLink(v);

	return true;
};

bool CZoomInHandler::handleRightMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	return true;
};


bool CZoomInHandler::redraw(CBaristaView *v)
{
	return true;
};

void CZoomInHandler::initialize(CBaristaView *v)
{
};

void CZoomInHandler::open()
{
	CBaristaViewEventHandler::open();
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->ZoomInAction->setChecked(true);
	mainwindow->blockSignals(false);

};

void CZoomInHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->ZoomInAction->setChecked(false);
	mainwindow->blockSignals(false);
};
