#include "Bui_VImageDlg.h"
#include "VImage.h"
#include "Bui_PushbroomDlg.h"
#include "Bui_FrameCameraModelDlg.h"
#include "Bui_ObsPointDlg.h"
#include "Bui_RpcDlg.h"


Bui_VImageDlg::Bui_VImageDlg(CVImage* image,QWidget *parent)
	: CAdjustmentDlg(parent),sensorModelDlg(0),image(0),pointObsDlg(0),
	oldTab(-1),pointResDlg(0)
{
	this->setupUi(this);

	// this connection is needed to listen to updated from different threads!!
	connect(this,SIGNAL(elementsChangedSignal(CLabel)),this,SLOT(elementsChangedSlot(CLabel)));

	this->setVImage(image);
}

Bui_VImageDlg::~Bui_VImageDlg()
{

	this->cleanUp();

}

void Bui_VImageDlg::cleanUp()
{
	if (this->image)
	{
		this->image->removeListener(this);
		this->image->getResiduals()->removeListener(this);
	}
	this->image = NULL;

	if (this->sensorModelDlg)
		delete this->sensorModelDlg;
	this->sensorModelDlg = NULL;

	if (this->pointObsDlg)
		delete this->pointObsDlg;
	this->pointObsDlg = NULL;

	if (this->pointResDlg)
		delete this->pointResDlg;
	this->pointResDlg = NULL;
}

void Bui_VImageDlg::elementDeleted(CLabel element)
{
}

void Bui_VImageDlg::setVImage(CVImage* newVImage)
{
	// clean up first
	if (this->image)
	{
		this->image->removeListener(this);
		this->image->getResiduals()->removeListener(this);
	}

	this->image = newVImage;
	this->initSensorModelDlg();
	this->image->addListener(this);
	this->image->getResiduals()->addListener(this);
	this->init2DObsPointDlg();
	this->init2DResPointDlg();

}


bool Bui_VImageDlg::okButtonReleased()
{
	if (this->sensorModelDlg && !this->sensorModelDlg->okButtonReleased())
		return false;

	if (this->pointObsDlg && !this->pointObsDlg->okButtonReleased())
		return false;

	if (this->pointResDlg && !this->pointResDlg->okButtonReleased())
		return false;

	return true;
}

bool Bui_VImageDlg::cancelButtonReleased()
{
	return true;
}

CCharString Bui_VImageDlg::helpButtonReleased()
{
	return "";
}


void Bui_VImageDlg::sensorModelStatusChanged(bool b)
{
	if (b)
	{
		this->tabWidget->setTabEnabled(0,true);
		this->tabWidget->setTabEnabled(2,true);
	}
	else
	{
		this->tabWidget->setTabEnabled(0,false);
		this->tabWidget->setTabEnabled(2,false);
	}

	emit enableOkButton(b);	
}


void Bui_VImageDlg::pointObsDlgStatusChanged(bool b)
{
	if (b)
	{
		this->tabWidget->setTabEnabled(1,true);
		this->tabWidget->setTabEnabled(2,true);
	}
	else
	{
		this->tabWidget->setTabEnabled(1,false);
		this->tabWidget->setTabEnabled(2,false);

	}

	emit enableOkButton(b);	
}

void Bui_VImageDlg::pointResDlgStatusChanged(bool b)
{
	if (b)
	{
		this->tabWidget->setTabEnabled(0,true);
		this->tabWidget->setTabEnabled(1,true);
	}
	else
	{
		this->tabWidget->setTabEnabled(0,false);
		this->tabWidget->setTabEnabled(1,false);

	}

	emit enableOkButton(b);	
}

void Bui_VImageDlg::tWCurrentChanged( int index )
{
	if (oldTab != -1)
	{
		if (oldTab == 0 && this->pointObsDlg)
		{
			this->pointObsDlg->okButtonReleased();
		}
		else if (oldTab == 1 && this->sensorModelDlg)
		{
			this->sensorModelDlg->okButtonReleased();
		}
		else if (oldTab == 2 && this->pointResDlg)
		{
			this->pointResDlg->okButtonReleased();
		}
	}

	oldTab = index;

}

void Bui_VImageDlg::initSensorModelDlg()
{

	bool firstDlg = true;

	if (this->sensorModelDlg)
	{
		this->setDefaultTab(this->sensorModelDlg->getCurrentTab());
		this->tab_SensorModel->layout()->removeWidget(this->sensorModelDlg);
		this->disconnect(this->sensorModelDlg,SIGNAL(enableOkButton(bool)),this,SLOT(sensorModelStatusChanged(bool)));
		delete this->sensorModelDlg;
		this->sensorModelDlg = NULL;
		firstDlg = false;
	}
	
	if (this->image->getCurrentSensorModelType() == ePUSHBROOM)
	{
		this->sensorModelDlg = new Bui_PushbroomDlg(this->image->getPushbroom(),this);
		this->connect(this->sensorModelDlg,SIGNAL(enableOkButton(bool)),this,SLOT(sensorModelStatusChanged(bool)));
		this->tab_SensorModel->layout()->addWidget(this->sensorModelDlg);
		if (!firstDlg)
			this->sensorModelDlg->setDefaultTab(this->defaultTab);
	}
	else if (this->image->getCurrentSensorModelType() == eFRAMECAMERA)
	{
		this->sensorModelDlg = new Bui_FrameCameraModelDlg(this->image->getFrameCamera(),this);
		this->connect(this->sensorModelDlg,SIGNAL(enableOkButton(bool)),this,SLOT(sensorModelStatusChanged(bool)));
		this->tab_SensorModel->layout()->addWidget(this->sensorModelDlg);
	}
	else if (this->image->getCurrentSensorModelType() == eRPC)
	{
		this->sensorModelDlg = new Bui_RpcDlg(this->image->getRPC(),this->image->getOriginalRPC(),this);
		this->connect(this->sensorModelDlg,SIGNAL(enableOkButton(bool)),this,SLOT(sensorModelStatusChanged(bool)));
		this->tab_SensorModel->layout()->addWidget(this->sensorModelDlg);
	}

}


void Bui_VImageDlg::init2DObsPointDlg()
{
	if (this->pointObsDlg)
	{
		this->tab_2DPoints->layout()->removeWidget(this->pointObsDlg);
		this->disconnect(this->pointObsDlg,SIGNAL(enableOkButton(bool)),this,SLOT(pointObsDlgStatusChanged(bool)));
		delete this->pointObsDlg;
		this->pointObsDlg = NULL;
	}




	this->pointObsDlg = new Bui_ObsPointDlg(this->image->getXYPoints());
	this->connect(this->pointObsDlg,SIGNAL(enableOkButton(bool)),this,SLOT(pointObsDlgStatusChanged(bool)));
	this->tab_2DPoints->layout()->addWidget(this->pointObsDlg);


}


void Bui_VImageDlg::init2DResPointDlg()
{
	if (this->pointResDlg)
	{
		this->tab_Residuals->layout()->removeWidget(this->pointResDlg);
		this->disconnect(this->pointResDlg,SIGNAL(enableOkButton(bool)),this,SLOT(pointResDlgStatusChanged(bool)));
		delete this->pointResDlg;
		this->pointResDlg = NULL;
	}

	if (this->image->getResiduals()->GetSize())
	{


		this->tabWidget->setTabEnabled(2,true);
		this->pointResDlg = new Bui_ObsPointDlg(this->image->getResiduals(),false,true,false);
		this->connect(this->pointResDlg,SIGNAL(enableOkButton(bool)),this,SLOT(pointResDlgStatusChanged(bool)));
		this->tab_Residuals->layout()->addWidget(this->pointResDlg);
	}
	else
		this->tabWidget->setTabEnabled(2,false);


}

void Bui_VImageDlg::elementsChangedSlot(CLabel element)
{
	if (!this->listenToSignal)
		return;

	if (element == "CVIMAGE")
		this->initSensorModelDlg();
	else if (element == "CXYPOINTARRAY" || element == "CXYZPOINTARRAY" )
		this->init2DResPointDlg();
}