#include "Bui_DEMMergeDlg.h"
#include "Icons.h"

#include "FileTable.h"
#include "BaristaProject.h"
#include "BaristaHtmlHelp.h"
#include <QFileDialog>
#include "IniFile.h"
#include <QMessageBox>
#include "ProgressThread.h"
#include "Bui_ExtensionsGroupBox.h"
#include "Bui_ProgressDlg.h"
#include "GDALFile.h"
#include "Bui_GDALExportDlg.h"
#include "BaristaProjectHelper.h"


Bui_DEMMergeDlg::Bui_DEMMergeDlg() : mergedname(""), success(false),dlg(NULL),newdem(NULL)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
	
	// create DEM List and put it in widget
	DEMListLayout = new QVBoxLayout(this->widget);
    DEMListLayout->setSpacing(6);
    DEMListLayout->setMargin(9);
	DEMListLayout->setObjectName(QString::fromUtf8("widgetLayout"));

	this->projectdems = new CFileTable(this->widget, 20, 20, 500, 140, 0,460 );
	DEMListLayout->addWidget(this->projectdems );

	this->projectdems->clear();
	this->projectdems->setHorizontalHeaderItem(0,this->projectdems->createItem(QString("Select DEMs for merging"),0,Qt::AlignCenter,QColor(0,0,0)));
	this->projectdems->setRowCount(project.getNumberOfDEMs());


	// create extension groupbox and put it in widget
	ExtensionLayout = new QVBoxLayout(this->widgetExtensions);
	ExtensionLayout->setObjectName(QString::fromUtf8("widgetLayout"));

	this->gbextensions = new Bui_ExtensionsGroupBox(this->widgetExtensions, false);
	ExtensionLayout->addWidget(this->gbextensions );

	// pushbutton connections
	this->connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));

	// output connection
	connect(this->pbMergedDEM, SIGNAL(released()),this, SLOT(pBSelectOutputReleased()));

	// selection of DEM from project
	this->connect(this->projectdems, SIGNAL(itemSelectionChanged ()),this, SLOT(DEMSelectionChanged()));
	this->connect(this->projectdems, SIGNAL(cellChanged(int, int)),this, SLOT(DEMSelectionChanged(int, int)));

	// selection of Master DEM from comboMaster
	this->connect(this->comboMaster, SIGNAL(currentIndexChanged(int)),this, SLOT(MasterSelectionChanged(int)));

	// fill FileTable with DEMs
	for (int i=0 ; i < project.getNumberOfDEMs(); i++)
	{
		CVDEM* vdem = project.getDEMAt(i);

		QString demname(vdem->getFileName()->GetChar());

		// write dem names in table
		this->projectdems->blockSignals(true);
		QTableWidgetItem* item = this->projectdems->createItem(demname,Qt::ItemIsSelectable,Qt::AlignLeft,QColor(0,0,0));
		item->setCheckState(Qt::Unchecked);
		this->projectdems->setRowHeight(i,20);
		this->projectdems->setItem(i,0,item);
		this->projectdems->blockSignals(false);
	}

	this->checkSetup();
}

Bui_DEMMergeDlg::~Bui_DEMMergeDlg()
{
	delete this->projectdems;

	if (this->dlg)
		delete this->dlg;

	this->dlg=NULL;

	if (!this->success)
	{
		if ( this->newdem )
			project.deleteDEM(newdem);
		this->newdem = NULL;
	}
}

void Bui_DEMMergeDlg::pBOkReleased()
{
	if (this->mergedname.IsEmpty())
		return;

	this->setSuccess(true);
	this->close();
}

void Bui_DEMMergeDlg::pBCancelReleased()
{
	this->setSuccess(false);
	this->close();
}

void Bui_DEMMergeDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/MergeDEMs.html");
}

bool Bui_DEMMergeDlg::createMergedDEM()
{
	this->success = this->merge();
	return this->getSuccess();
}

void Bui_DEMMergeDlg::pBSelectOutputReleased()
{
	if (this->dlg)
		delete this->dlg;

	if (this->newdem)
	{
		project.deleteDEM(newdem);
		this->mergedname = "";
		this->mergedDEMFile->setText("");
	}

	this->newdem = project.addDEM("MergeDEM");

	this->newdem->getDEM()->bands = 1;
	this->newdem->getDEM()->bpp = 32;

	this->newdem->getDEM()->setTFW(this->gbextensions->getTFW());

	this->dlg = new Bui_GDALExportDlg(this->mergedname,this->newdem,true,"Export DEM");


	/// make sure that geoinfo is written to file
	this->dlg->cB_ExportGeo->setChecked(true);
	
	this->dlg->exec();


	if (!this->dlg->getSuccess())	// false if user has pressed "Cancel"
	{
		if (this->newdem)
			project.deleteDEM(this->newdem);

		this->newdem = NULL;

		this->mergedname = "";

		return;
	}

	this->mergedname = this->dlg->getFilename();
	this->mergedDEMFile->setText(this->mergedname.GetFullFileName().GetChar());

	this->newdem->setFileName(this->mergedname.GetChar());
	this->checkSetup();
}

void Bui_DEMMergeDlg::DEMSelectionChanged()
{
	for (int i = 0; i < this->projectdems->rowCount(); i++)
	{
		if ( this->projectdems->item(i,0)->isSelected() ) 
		{
			this->projectdems->item(i,0)->setCheckState(Qt::Checked);
		}
		else
		{
			this->projectdems->item(i,0)->setCheckState(Qt::Unchecked);
		}
	}

	this->updateDEMSelection();
	this->updateMasterOptions();
	this->checkSetup();
}

void Bui_DEMMergeDlg::DEMSelectionChanged(int i, int j)
{
	this->DEMSelectionChanged();
}



void Bui_DEMMergeDlg::checkSetup()
{
	bool readyToGo = true;
	this->pBOk->setEnabled(false);
	this->pbMergedDEM->setEnabled(false);

	// check for TFW info
	if ( this->gbextensions->getTFW().gethasValues() )
	{
		this->pbMergedDEM->setEnabled(true);
	}

	// check filename is set
	if ( this->mergedname.IsEmpty() )
		readyToGo = false;

	// check ReferenceSystems
	if ( !this->checkReferenceSystems() )
	{
		QMessageBox::warning(this,"DEM selection not valid",
		"DEMs are defined in different Reference systems!",1,0,0);
		readyToGo = false;
	}

	if ( readyToGo ) this->pBOk->setEnabled(true);
}

void Bui_DEMMergeDlg::updateMasterOptions()
{
	this->comboMaster->blockSignals(true);
	this->comboMaster->setEnabled(false);
	this->comboMaster->clear();

	this->mergeDEMList.resize(0);
	for (int i = 0; i < this->projectdems->rowCount(); i++)
	{
		if ( this->projectdems->item(i,0)->isSelected() )
		{
			CVDEM* vdem = project.getDEMAt(i);

			unsigned int k;
			for ( k=0; k < this->mergeDEMList.size(); k++)
				if (this->mergeDEMList[k]->getDEM()->getgridX() > vdem->getDEM()->getgridX())
					break;
			
			this->mergeDEMList.insert(this->mergeDEMList.begin() + k,vdem);
		}
	}

	for (unsigned int i=0; i< this->mergeDEMList.size(); i++)
	{
		QString demname(this->mergeDEMList[i]->getFileName()->GetChar());
	
		// write dem name in comboMaster
		this->comboMaster->addItem(demname);
	}

	if ( this->comboMaster->count() > 0 )
		this->comboMaster->setEnabled(true);

	this->comboMaster->blockSignals(false);
}

void Bui_DEMMergeDlg::MasterSelectionChanged(int i)
{
	this->getMasterSelection();
	this->updateExtensionValues();
}

void Bui_DEMMergeDlg::getMasterSelection()
{
	QString mName =  this->comboMaster->currentText();
    CCharString searchname = (const char*)mName.toLatin1();

	this->masterIndex = -1;

	CVDEM* sDEM = project.getDEMByName(searchname);
	if ( sDEM != NULL ) this->masterIndex = project.getDEMIndex(sDEM);
}

bool Bui_DEMMergeDlg::checkReferenceSystems()
{
	bool RefSysOkay = true;
	this->getMasterSelection();

	if ( this->masterIndex == -1 ) return true;

	CVDEM* masterDem = project.getDEMAt(this->masterIndex);

	CReferenceSystem refsys = masterDem->getReferenceSystem();

	for ( int i = 0; i < this->comboMaster->count(); i++)
	{
		QString testname =  this->comboMaster->itemText(i);
		CCharString searchname = (const char*)testname.toLatin1();

		CVDEM* testDEM = project.getDEMByName(searchname);

		if ( testDEM->getReferenceSystem() != refsys )
			return false;
	}

	return RefSysOkay;
}

void Bui_DEMMergeDlg::updateExtensionValues()
{
	if ( this->demselection.size() < 1 ) 
	{
		this->gbextensions->resetSpinBoxes();
		return;
	}

	int firstindex = -1; 
	int i = 0;

	while ( firstindex == -1 && i < int(this->demselection.size()))
	{
		if ( !this->demselection[i] )
			i++;
		else
			firstindex = i;
	}

	if ( firstindex == -1 ) 
	{
		this->gbextensions->resetSpinBoxes();
		return;
	}
	if ( this->masterIndex == -1 ) this->masterIndex = firstindex;

	if ( this->masterIndex == -1 ) return;

	CDEM* masterDEM = project.getDEMAt(this->masterIndex)->getDEM();

	this->gridx = masterDEM->getgridX();
	this->gridy = masterDEM->getgridY();
	this->xmin = masterDEM->getminX();
	this->xmax = masterDEM->getmaxX();
	this->ymin = masterDEM->getminY();
	this->ymax = masterDEM->getmaxY();

	for ( int i = firstindex; i < this->projectdems->rowCount(); i++ )
	{
		if ( this->demselection[i])
		{
			CDEM* currentDEM = project.getDEMAt(i)->getDEM();
			
			double cxmin = currentDEM->getminX();
			double cxmax = currentDEM->getmaxX();
			double cymin = currentDEM->getminY();
			double cymax = currentDEM->getmaxY();

			if ( cxmin < this->xmin ) this->xmin = cxmin;
			if ( cxmax > this->xmax ) this->xmax = cxmax;
			if ( cymin < this->ymin ) this->ymin = cymin;
			if ( cymax > this->ymax ) this->ymax = cymax;
		}
	}

	C2DPoint gridS;
	C2DPoint shift;

	CReferenceSystem refsys = masterDEM->getTFW()->getRefSys();
	if ( refsys.getCoordinateType() == eGEOGRAPHIC )
	{
		this->height = this->xmax - this->xmin;
		this->width = this->ymax - this->ymin;
		this->cols = this->width/this->gridy +1;
		this->rows = this->height/this->gridx +1;

		gridS.x = this->gridx;
		gridS.y = this->gridy;

		shift.x = this->xmax;
		shift.y = this->ymin;
	}
	else
	{
		this->width = this->xmax - this->xmin;
		this->cols = this->width/this->gridx +1;
		this->height = this->ymax - this->ymin;
		this->rows = this->height/this->gridy +1;

		gridS.x = this->gridx;
		gridS.y = this->gridy;

		shift.x = this->xmin;
		shift.y = this->ymin + (this->rows - 1) * gridS.y;
	}



	if ( refsys.getCoordinateType() == eGEOGRAPHIC )
		this->gbextensions->setSpinBoxDecimals(10);
	else
		this->gbextensions->setSpinBoxDecimals(2);
	this->gbextensions->setValues(shift, gridS, this->cols, this->rows, refsys);
}

void Bui_DEMMergeDlg::updateDEMSelection()
{
	this->demselection.resize(this->projectdems->rowCount());

	for (int i = 0; i < this->projectdems->rowCount(); i++)
	{
		this->demselection[i] = false;
		if ( this->projectdems->item(i,0)->isSelected() ) 
			this->demselection[i] = true;
	}

	this->updateExtensionValues();
}


bool Bui_DEMMergeDlg::merge()
{
	CVDEM* masterDEM = project.getDEMAt(this->masterIndex);

	this->newdem->getDEM()->width = (int)this->gbextensions->spinBoxCols->value();
	this->newdem->getDEM()->height = (int)this->gbextensions->spinBoxRows->value();
	this->newdem->getDEM()->setTFW(this->gbextensions->getTFW());

	vector<CDEM*> dems;

	for ( unsigned int i = 0; i < this->mergeDEMList.size(); i++)
	{
		dems.push_back(this->mergeDEMList[i]->getDEM());
	}
	
	newdem->getDEM()->setFileName(this->mergedname.GetChar());
	
	bui_ProgressDlg progressdlg(new CDEMMergeThread(this->newdem->getDEM(),dems),this->newdem->getDEM(),"Merging DEMs ...");
	
	progressdlg.exec();

	if (!progressdlg.getSuccess())
	{
		this->success = false;
	}

	if ( this->success && this->dlg)
	{
		if ( this->dlg->writeSettings() )
			this->dlg->writeImage("Writing DEM as image file");

		if (!this->dlg->getSuccess())
		{
			this->success = false;
		}
	}

	baristaProjectHelper.refreshTree();
	return this->success;
}

bool Bui_DEMMergeDlg::getTFWInfo()
{
	if ( this->newdem)
	{
		this->updateExtensionValues();

		return true;
	}
	else
		return false;
}


