#include "GCPchipHandler.h"
#include "ImageExporter.h"
#include "ImageView.h"
#include "HugeImage.h"
#include "VImage.h"
#include "BaristaProject.h"
#include "Bui_MainWindow.h"
#include "Bui_LabelChangeDlg.h"
#include "ProtocolHandler.h"
#include "BaristaProjectHelper.h"
#include <QMessageBox>
#include "Bui_ProgressDlg.h"
#include "Bui_MonoPlotDlg.h"
#include "GDALFile.h"
#include "Bui_GDALExportDlg.h"

CGCPchipHandler::CGCPchipHandler()
{
	this->statusBarText = "CHIPMODE ";
	this->dlg = NULL;
	this->newVImage = NULL;
	//this->matchdlg.move(150, 120);

	//this->matchdlg.setWindowTitle("Match Dialog");
}


CGCPchipHandler::~CGCPchipHandler()
{
	if (this->dlg)
		delete this->dlg;
	this->dlg=NULL;

	//if (this->newVImage)
	//	project.deleteImage(this->newVImage);		
};

void CGCPchipHandler::open()
{
	CBaristaViewEventHandler::open();
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->SCPcollectionAction->setChecked(true);
	mainwindow->blockSignals(false);
/*
	// check matching parameter of project
	double testmin = project.getMatchingParameter().getMinZ();
	double testmax = project.getMatchingParameter().getMaxZ();

	double range = testmax - testmin;

	if ( project.getMatchingParameter().getPatchSize() < 3 || range <= 0.0 )
	{
		CMatchingParameters parameters;
		parameters.reset();
		project.setMatchingParameter(parameters);
		this->openMatchingParameterDlg();
	}
	lsm.clear();
*/
	//CLabel label("CHIP_1");

	//while ( !project.checkforUniquePointLabel(label))
	//	label++;	

	this->imgPoint = NULL;
	this->newVImage = NULL;
	this->dlg = NULL;
	//int ps = project.getMatchingParameter().getPatchSize();
	//project.getMatchingParameter().setPatchSize(128);
};

void CGCPchipHandler::close()
{
	bui_MainWindow* mainwindow = (bui_MainWindow*)this->parent;

	mainwindow->blockSignals(true);
	mainwindow->SCPcollectionAction->setChecked(false);
	mainwindow->blockSignals(false);
	/*
	lsm.clear();

	this->matchdlg.close();
	*/
	this->imgPoint = NULL;
	if (this->newVImage)
	{
		delete this->newVImage;
		this->newVImage = NULL;
	}

	if (this->dlg)
	{
		delete this->dlg;
		this->dlg = NULL;
	}
/*
	int n = this->baristaViews.getNumberOfViews();
	for (int i=0; i< n ; i++)
	{
		CBaristaView* v = this->baristaViews.getView(i);
		v->setShowMatchWindow(false);
	}
*/	

};


bool CGCPchipHandler::handleLeftMouseReleaseEvent(QMouseEvent *e, CBaristaView *v)
{
	if (project.getNumberOfDEMs() <= 0)
	{
		//give warning and close current barista view handler
		QMessageBox::warning(0, "No DEM is currently present!"	 , "Import a corresponding DEM to the project first!");
	}
	else if (!project.getChipDbFileName()->GetLength())
	{	//give warning and close current barista view handler
		QMessageBox::warning(0, "No chip database is currently open!"	 , "Open or create a new chip database first!");
			//CGCPchipHandler::close();
	}	
	else // chip file exists, so allow collecting chips
	{
		//generate Chip Label
		this->chipID = project.getCurrentChipID();
		this->chipName = CCharString(this->chipID,12,true); //CCharString(long number, int digits, bool leadingZeroes);	
		char buf[20], buf1[12];
		sprintf(buf1, "%ld", this->chipID);
		strcpy(buf,"CHIP_");
		strcat(buf,buf1);
		int len = strlen(buf);
		buf[len] = '\0';
		CLabel label(buf);
		project.setCurrentXYPointLabelForAllImages(label);


		this->imgPoint = NULL; // initialize 2D image point, exportDialog and newVImage pointers to null	  
		this->newVImage = NULL;
		this->dlg = NULL;

		CHugeImage* himage = v->getHugeImage();	
		if (!himage) // if himage not found exit with true
			return false;
		if (v->getVImage() && v->getVImage()->hasTFW())
		{	
			//find 2d point at mouse click point
			CXYPoint tmp;
			v->getMouseEventCoords(e, tmp.x, tmp.y);	    
			// no points to be digitized outside image range. Pixel position usually refers to the center of a square unit and therefore the offset value is set at 0.5
			if (tmp.getX() < -0.5 || tmp.getX() > himage->getWidth() || 
				tmp.getY() < -0.5 || tmp.getY() > himage->getHeight() )
				return false;
			CLabel l = v->getVImage()->getCurrentLabel();
			tmp.setLabel(l);
			// set default sigma SX and SY to 0.5 pixel
			tmp.setSX(0.5);
			tmp.setSY(0.5);
			CObsPoint* oldPoint = v->getVImage()->getXYPoints()->getPoint(l.GetChar());
			if (oldPoint)
				v->getVImage()->getXYPoints()->Remove(oldPoint);
			this->imgPoint = v->getVImage()->getXYPoints()->add(tmp); // add the newly digitized point to the existing 2D point array. 
			// This add function also informs all the listners (for exmaple, the current view) that the 2D point array a new element is added
			// On getting the information, the listenres take proper action, for exmaple, the current image view updates its view (redraw its content)
			
			//convert this 2d point to 3d point using image's sensor model & project's active DEM
			if  (project.getActiveDEM())
			{
				CXYZPoint objPt;
				CMonoPlotter monoplotter;
				monoplotter.setModel(v->getVImage()->getTFW());
				monoplotter.setVDEM(project.getActiveDEM(), false);
				if ((monoplotter.getDEMToSensor()) && (monoplotter.getSensorToDEM()))
				{
					monoplotter.getPoint(objPt, *(this->imgPoint));

					//v->getVImage()->getTFW()->transformObs2Obj(objPt,*(this->imgPoint),0);
					CVDEM *activeDEM = project.getActiveDEM();
					C2DPoint demObs;
					activeDEM->getTFW()->transformObj2Obs(demObs, objPt);
					if (demObs.x < 0 || demObs.y < 0 || demObs.x > activeDEM->getDEM()->getWidth()-1 || demObs.y > activeDEM->getDEM()->getHeight()-1)
					{
						// Secondary control point is out of the active DEM
						QMessageBox::warning(0, "Outside the DEM coverage!"	 , "Chosen control point is outside the active DEM!");
						//remove added 2d image point
						this->remove_imagePoint(v,l);
						return false;
					}

					bui_MonoPlotDlg scpdlg;
					scpdlg.setWindowTitle("Chip Information");
					scpdlg.setRefSys(monoplotter.getReferenceSystem());
					scpdlg.setChipDlgValues(objPt.getX(), objPt.getY(), objPt.getZ(), (const char *)l.GetChar(), this->chipName, true);
					scpdlg.exec();

					if (scpdlg.getSuccess())				{				

						CCharString path = project.getChipDbFileName()->GetPath();
						CFileName fname = path + "/" + "Chip_" + this->chipName + ".tif";

						this->oldVImage = v->getVImage();
						//this->newVImage = project.addImage(fname,false);
						this->newVImage = new CVImage;

						this->newVImage->setName(fname.GetChar());
						this->newVImage->getHugeImage()->bands = this->oldVImage->getHugeImage()->getBands();
						this->newVImage->getHugeImage()->bpp = this->oldVImage->getHugeImage()->getBpp();
						this->newVImage->setTFW(*this->oldVImage->getTFW());
						this->newVImage->getHugeImage()->setTransferFunction(this->oldVImage->getHugeImage()->getTransferFunction(),true,true);				

						//set chip width & height
						CGCPDataBasePoint P3d = project.getGCPpoint();						
						if (P3d.get2DPointArraySize()>0)
						{
							CGCPData2DPoint P2 = P3d.get2DPointAt(0);
							this->chipWidth = P2.getchipSize();
							this->chipHeight = this->chipWidth;
						}
						else
						{
							this->chipWidth = 256;
							this->chipHeight = 256;
						}
						this->ulx = this->imgPoint->getX() - this->chipWidth/2;
						this->uly = this->imgPoint->getY() - this->chipHeight/2;			

						//crop chip
						bool okay = this->newVImage->cropfromImage(this->oldVImage,this->ulx, this->uly, this->chipWidth, this->chipHeight, false, false, true, false, false);
						
						if (this->dlg)
						{
							delete this->dlg;
							this->dlg = NULL;
						}
						this->dlg = new Bui_GDALExportDlg(fname,this->newVImage,false,"Export Image");
						if (this->dlg && okay)
						{
							this->dlg->setExportGeo(true);
							this->dlg->writeSettings();
							okay = this->dlg->writeImage();
							
							//add this chip to the project's chip database
							//convert to geocentric coordinate system
							C3DPoint inpoint(objPt.getX(), objPt.getY(), objPt.getZ());
							C3DPoint outpoint;
							if (monoplotter.getReferenceSystem().getCoordinateType() == eGRID)
							{
								monoplotter.getReferenceSystem().gridToGeocentric(outpoint, inpoint);
							}
							else if (monoplotter.getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
							{
								monoplotter.getReferenceSystem().geographicToGeocentric(outpoint, inpoint);
							}
							
							//set 2d chip info
							CGCPData2DPoint P2d = P3d.get2DPointAt(0);
							P2d.setLabel(l);
							P2d.setOrthoImageFileName(fname);
							P2d.setnBands(this->newVImage->getHugeImage()->bands);
							P2d.setChipID(this->chipName);
							//set this 2d info to 3d info
							//project.getGCPpoint().setGCPPoint2DArrayClear();
							//project.getGCPpoint().set2DPoint(P2d);
							//now set 3d info
							//P3d = project.getGCPpoint();
							P3d.x = outpoint.x;
							P3d.y = outpoint.y;
							P3d.z = outpoint.z;
							P3d.setSX(0.1);
							P3d.setSY(0.1);
							P3d.setSZ(0.1);
							P3d.setLabel(l);	
							//set all this chip info to database							
							project.addToChipDB(P2d,P3d);
							project.addToChipSelectionArray(true);
							
							//add to project's xyzGCPchipPoints list
							CXYZPoint objPoint;
							objPoint.setX(outpoint.x);
							objPoint.setY(outpoint.y);
							objPoint.setZ(outpoint.z);
							objPoint.setSX(0.1);
							objPoint.setSY(0.1);
							objPoint.setSZ(0.1);
							objPoint.setLabel(l);
							project.addGCPchipPoint(objPoint);


							//chip collection is successful, so set next chip ID
							this->chipID++;
							project.setCurrentChipID(this->chipID);

							
						}
						//if (this->newVImage)
						//	project.deleteImage(this->newVImage);
						
					}
					else
					{// chip export cancelled
						//remove added 2d image point
						this->remove_imagePoint(v,l);
						return false;
					}
				}
				else
				{//could not convert to 3d object point
					QMessageBox::warning(0, "2D to 3D conversion!"	 , "Conversion failed!");
					//remove added 2d image point
					this->remove_imagePoint(v,l);
					return false;
				}
			}
			else
			{// no active DEM in project
				QMessageBox::warning(0, "No active DEM!"	 , "Project should have an active DEM!");
				//remove added 2d image point
				this->remove_imagePoint(v,l);
				return false;
			}

		} 
		else
		{//not an image with TFW node
			QMessageBox::warning(0, "No TFW node!"	 , "Image should be orthorectified with a TFW node!");
			return false;
		}		
	}

	 // if 2D point array size is less than 2 or if number of chip points is 1 
	// the first chip is just addedd, and therefore needs refreshing the tree view
	if (v->getVImage()->getXYPoints()->GetSize() < 2 || project.getNumberOfGCPchipPoints() == 1) 
		baristaProjectHelper.refreshTree();

	if (this->newVImage)
	{
		delete this->newVImage;
		this->newVImage = NULL;
	}

	if (this->dlg)
	{
		delete this->dlg;
		this->dlg = NULL;
	}
	return true;
		
}

void CGCPchipHandler::remove_imagePoint(CBaristaView *v, CLabel l)
{
	//remove added 2d image point
	CObsPoint* oldPoint = v->getVImage()->getXYPoints()->getPoint(l.GetChar());
	if (oldPoint)
		v->getVImage()->getXYPoints()->Remove(oldPoint);					
	// This remove function also informs all the listners (for exmaple, the current view) that the 2D point array an element is removed
	// On getting the information, the listenres take proper action, for exmaple, the current image view updates its view (redraw its content)
	if (v->getVImage()->getXYPoints()->GetSize() < 1 ) 
		baristaProjectHelper.refreshTree(); // if 2D point array size is less than 2, 
	// the first element is just addedd, and therefore needs refreshing the tree view
}

void CGCPchipHandler::handleLeaveEvent ( QEvent * event, CBaristaView *v)
{
	CBaristaViewEventHandler::handleLeaveEvent(event,v);
	v->setShowMatchWindow(false);
}


void CGCPchipHandler::handleEnterEvent ( QEvent * event, CBaristaView *v )
{
	v->setShowMatchWindow(true);	
}