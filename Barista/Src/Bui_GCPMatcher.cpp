#include <float.h> 

#include "Bui_GCPMatcher.h"
#include <QFileDialog>
#include <QMessageBox> 
#include <QTimer> 

#include "Icons.h"
#include "BaristaHtmlHelp.h"
#include "BaristaProject.h"
#include "Bui_MatchingParametersDlg.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include "GCPMatcher.h"
#include "FeatExtractPars.h"
#include "Bui_FeatureExtractionDialog.h"

//================================================================================

bui_GCPMatchingDialog::bui_GCPMatchingDialog () : 
			pGCPmatcher(0), sceneSizePercentage(1.0 / 3.0), ScenesBridged(5), 
			nPoints(100), pDEM(0), useWallis(true), setGCP(true), GCPType (eGEOCENTRIC)
{
	setupUi(this);
	this->init();
}


//================================================================================

bui_GCPMatchingDialog::bui_GCPMatchingDialog (CXYZPointArray *GCPs) : 
			pGCPmatcher(0), sceneSizePercentage(1.0 / 3.0), ScenesBridged(5),
			nPoints(100), pDEM(0), useWallis(true)
{
	setupUi(this);
	this->init(GCPs);
}

//================================================================================

bui_GCPMatchingDialog::~bui_GCPMatchingDialog()
{
	if (this->pGCPmatcher) delete this->pGCPmatcher;
	this->pGCPmatcher = 0;
}

//================================================================================

void bui_GCPMatchingDialog::init()
{
	connect(this->buttonOk,                 SIGNAL(released()),                      this, SLOT(OK()));
	connect(this->buttonCancel,             SIGNAL(released()),                      this, SLOT(Cancel()));
	connect(this->HelpButton,               SIGNAL(released()),                      this, SLOT(Help()));
	connect(this->ImageSelector,            SIGNAL(currentRowChanged (int)),         this, SLOT(ImageSelected(int)));
	connect(this->ImageSelector,            SIGNAL(itemClicked (QListWidgetItem *)), this, SLOT(ImageClicked(QListWidgetItem *)));
	connect(this->OrthoImages,              SIGNAL(currentRowChanged (int)),         this, SLOT(OrthoImageSelected(int)));
	connect(this->OrthoImages,              SIGNAL(itemClicked (QListWidgetItem *)), this, SLOT(OrthoImageClicked(QListWidgetItem *)));
	connect(this->DEMcombo,                 SIGNAL(currentIndexChanged(int)),        this, SLOT(DEMChanged()));
	connect(this->WallisFilterCheckBox,     SIGNAL(stateChanged(int)),               this, SLOT(WallisFilterClicked()));

	connect(this->MatchingParametersButton, SIGNAL(released()),                      this, SLOT(MatchParsSelected()));
	connect(this->PatchSize,                SIGNAL(editingFinished()),               this, SLOT(PatchSizeChanged()));
	connect(this->RhoMin,                   SIGNAL(editingFinished()),               this, SLOT(RhoMinChanged()));
	connect(this->SearchArea,               SIGNAL(editingFinished()),               this, SLOT(SearchAreaChanged()));
	connect(this->NPoints,                  SIGNAL(editingFinished()),               this, SLOT(NPointsChanged()));
	connect(this->SceneSizePercentage,      SIGNAL(editingFinished()),               this, SLOT(SceneSizePercentageChanged()));
	connect(this->NScenesBridged,           SIGNAL(editingFinished()),               this, SLOT(NScenesChanged()));
	connect(this->FeatureExtractionButton,  SIGNAL(released()),                      this, SLOT(FEXParsSelected()));
	connect(this->GeographicButton,         SIGNAL(clicked(bool)),                   this, SLOT(CoordSysChanged()));
	connect(this->GeocentricButton,         SIGNAL(clicked(bool)),                   this, SLOT(CoordSysChanged()));
	connect(this->GridButton,               SIGNAL(clicked(bool)),                   this, SLOT(CoordSysChanged()));
	connect(this->setGCPcheckBox,           SIGNAL(stateChanged(int)),               this, SLOT(GCPchanged()));
	
	this->CoordinateTypeGroupBox->setVisible(true);
	this->setGCPcheckBox->setVisible(true);
	this->FeatureParsgroupBox->setVisible(true);
 	this->DEMGroupBox->setVisible(true);

	this->possibleOriginalImages.clear();
	this->possibleOrthoImages.clear();

	this->extractFeatures = true;

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	CMatchingParameters pars(project.getMatchingParameter());
	FeatureExtractionParameters extractionPars;

	for (int i = 0; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		if (img->getCurrentSensorModel()) this->possibleOriginalImages.push_back(img);
	}

	this->initDEMs();
		
	if (this->pDEM && (this->possibleOriginalImages.size() > 0))
	{

		CFileName featureFile("GCPFEX.ini");
		featureFile.SetPath(project.getFileName()->GetPath());

		if (CSystemUtilities::fileExists(featureFile))
		{
			extractionPars.initParameters(featureFile);
			this->nPoints = extractionPars.distNonmaxPts;
		}
		else
		{
			extractionPars.setDerivativeFilter(eSobelX,1.0f);
			extractionPars.NoiseModel = ePoisson;
			extractionPars.Mode = eFoerstner;
			extractionPars.setIntegrationFilter(eBinom, 5, 5, 0.7f, 1.0f);	
			extractionPars.distNonmaxPts = this->nPoints;
			extractionPars.extractPoints = 1;
			extractionPars.extractEdges  = 0;
		}

	
		this->pGCPmatcher = new CGCPMatcher(this->possibleOriginalImages, this->pDEM, this->ScenesBridged, 
			                                float(this->sceneSizePercentage), pars, extractionPars);

		this->GCPType = this->pDEM->getReferenceSystem().getCoordinateType();
	}

	if (this->isInitialised())
	{
		for (unsigned int i=0; i < this->possibleOriginalImages.size(); i++)
		{
			CCharString name = this->possibleOriginalImages[i]->getFileNameChar();

			QListWidgetItem* item = new QListWidgetItem(name.GetChar());
			item->setData(Qt::UserRole,i); // remember index into array
			item->setCheckState(Qt::Checked);
			item->setToolTip(name.GetChar());

			this->ImageSelector->addItem(item);
			item->setCheckState(Qt::Checked);
		}	

		this->initPossibleOrthos();
	}
	else
	{
		this->setCurrentOrtho(1);
	}	


	this->setGCPcheckBox->blockSignals(true);
	this->setGCPcheckBox->setChecked(this->setGCP);
	this->setGCPcheckBox->blockSignals(false);
	setCoordinateTypeGUI();
	setMatchParGUI(pars);
	setFEXParGUI();

	this->checkSetup();
}
	  
//================================================================================

void bui_GCPMatchingDialog::init(CXYZPointArray *GCPs)
{
	connect(this->buttonOk,                 SIGNAL(released()),                      this, SLOT(OK()));
	connect(this->buttonCancel,             SIGNAL(released()),                      this, SLOT(Cancel()));
	connect(this->HelpButton,               SIGNAL(released()),                      this, SLOT(Help()));
	connect(this->MatchingParametersButton, SIGNAL(released()),                      this, SLOT(MatchParsSelected()));
	connect(this->PatchSize,                SIGNAL(editingFinished()),               this, SLOT(PatchSizeChanged()));
	connect(this->RhoMin,                   SIGNAL(editingFinished()),               this, SLOT(RhoMinChanged()));
	connect(this->SearchArea,               SIGNAL(editingFinished()),               this, SLOT(SearchAreaChanged()));
	connect(this->OrthoImages,              SIGNAL(currentRowChanged (int)),         this, SLOT(OrthoImageSelected(int)));
	connect(this->OrthoImages,              SIGNAL(itemClicked (QListWidgetItem *)), this, SLOT(OrthoImageClicked(QListWidgetItem *)));
	connect(this->WallisFilterCheckBox,     SIGNAL(stateChanged(int)),               this, SLOT(WallisFilterClicked()));
	
	this->CoordinateTypeGroupBox->setVisible(false);
	this->setGCPcheckBox->setVisible(false);
	this->FeatureParsgroupBox->setVisible(false);
	this->InputImagesGroupBox->setVisible(false);
	this->DEMGroupBox->setVisible(false);

	this->extractFeatures = false;

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
	this->possibleOriginalImages.clear();
	this->possibleOrthoImages.clear();

	CMatchingParameters pars(project.getMatchingParameter());


	if (GCPs) 
	{
		if (!project.getControlPoints()) this->setGCP = false;
		else
		{
			this->setGCP = (GCPs == project.getControlPoints()->getXYZPointArray());
		}
		this->GCPType = GCPs->getReferenceSystem()->getCoordinateType();
		pars.setXYZFileName(*(GCPs->getFileName()));
		this->pGCPmatcher = new CGCPMatcher(GCPs, pars);
	}

	if (this->isInitialised())
	{
		this->initPossibleOrthos();
	}
	else
	{
		this->setCurrentOrtho(1);
	}	

	setMatchParGUI(pars);
	this->checkSetup();
}
	 	  
//================================================================================

void bui_GCPMatchingDialog::initDEMs()
{
	if (project.getNumberOfDEMs() <= 0) this->pDEM = 0;
	else
	{
		this->DEMcombo->blockSignals(true);
		CVDEM *pActiveDEM = project.getActiveDEM();

		int selected = 0;
		
		for (int i = 0; i < project.getNumberOfDEMs(); ++i)
		{
			CVDEM *dem = project.getDEMAt(i);
			this->DEMcombo->addItem(dem->getFileName()->GetChar());
			if (pActiveDEM == dem) selected = i;
		}

		this->DEMcombo->setCurrentIndex(selected);
		this->pDEM = project.getDEMAt(selected);
		
		this->DEMcombo->setToolTip(this->pDEM->getFileName()->GetChar());
		
		this->DEMcombo->blockSignals(false);
	}
};

//================================================================================

void bui_GCPMatchingDialog::initPossibleOrthos()
{
	vector <CVImage *> oldOrthos = this->possibleOrthoImages;
	vector<bool> oldOrthoSelected(this->possibleOrthoImages.size());

	for (unsigned int i = 0; i < this->possibleOrthoImages.size(); ++i)
	{
		oldOrthoSelected[i] = (this->OrthoImages->item(i)->checkState() == Qt::Checked);
	}

	this->possibleOrthoImages.clear();
	this->OrthoImages->clear();

	for (int i = 0; i < this->pGCPmatcher->getNumberOfPossibleOrthophotos(); ++i)
	{
		CVImage *ortho = this->pGCPmatcher->getOrthoImageAt(i);
		this->possibleOrthoImages.push_back(ortho);
		CCharString name = ortho->getFileNameChar();
		QListWidgetItem* item = new QListWidgetItem(name.GetChar());
		item->setData(Qt::UserRole,i); // remember index into array
		if ((oldOrthoSelected.size() < 1) || (i == 0)) item->setCheckState(Qt::Checked);
		else 
		{
			bool isChecked = false; 
			for (unsigned int j = 0; (j < oldOrthoSelected.size()) && !isChecked; ++j)
			{
				if (oldOrthos[j] == ortho) isChecked = oldOrthoSelected[j];
			}
			if (isChecked) item->setCheckState(Qt::Checked);
			else item->setCheckState(Qt::Unchecked);
		}
		item->setToolTip(name.GetChar());
		this->OrthoImages->addItem(item);
	}

	if (this->possibleOrthoImages.size() < 1) this->setCurrentOrtho(1);
};

//================================================================================

void bui_GCPMatchingDialog::setMatchParGUI(const CMatchingParameters &pars)
{
	this->PatchSize->blockSignals(true);
	this->RhoMin->blockSignals(true);
	this->SearchArea->blockSignals(true);
	this->WallisFilterCheckBox->blockSignals(true);

	QString ds;
	
	ds=QString("%1").arg(pars.getPatchSize());                 this->PatchSize->setText(ds); 		
	ds=QString("%1").arg(pars.getRhoMin() * 100.0, 0, 'f', 1); this->RhoMin->setText(ds); 
	ds=QString("%1").arg(pars.getSigmaGeom(),      0, 'f', 0); this->SearchArea->setText(ds); 

	this->WallisFilterCheckBox->setChecked(this->useWallis);
	this->PatchSize->blockSignals(false);
	this->RhoMin->blockSignals(false);
	this->SearchArea->blockSignals(false);
	this->WallisFilterCheckBox->blockSignals(false);
};

//================================================================================

void bui_GCPMatchingDialog::setFEXParGUI()
{
	this->NPoints->blockSignals(true);
	this->SceneSizePercentage->blockSignals(true);
	this->NScenesBridged->blockSignals(true);
	
	QString ds;
	
	ds=QString("%1").arg(this->nPoints);                                this->NPoints->setText(ds); 		
	ds=QString("%1").arg(this->sceneSizePercentage * 100.0, 0, 'f', 1); this->SceneSizePercentage->setText(ds); 
	ds=QString("%1").arg(this->ScenesBridged);                          this->NScenesBridged->setText(ds); 

	if (this->pGCPmatcher) this->NScenesBridged->setEnabled(this->pGCPmatcher->imageVectorIsStrip());

	this->NPoints->blockSignals(false);
	this->SceneSizePercentage->blockSignals(false);
	this->NScenesBridged->blockSignals(false);
};
	  
//================================================================================

void bui_GCPMatchingDialog::setCoordinateTypeGUI()
{
	this->GeographicButton->blockSignals(true);
	this->GeocentricButton->blockSignals(true);
	this->GridButton->blockSignals(true);

	bool isGeog = this->GCPType == eGEOGRAPHIC;
	bool isGeoc = this->GCPType == eGEOCENTRIC;
	bool isGrid = this->GCPType == eGRID;

	this->GeographicButton->setChecked(isGeog);
	this->GeocentricButton->setChecked(isGeoc);
	this->GridButton->setChecked(isGrid);

	this->GeographicButton->blockSignals(false);
	this->GeocentricButton->blockSignals(false);
	this->GridButton->blockSignals(false);
};

//================================================================================

void bui_GCPMatchingDialog::CoordSysChanged()
{
	if (this->GCPType == eGEOGRAPHIC)
	{
		if (this->GeocentricButton->isChecked()) this->GCPType = eGEOCENTRIC;
		else if (this->GridButton->isChecked()) this->GCPType = eGRID;
	}
	else if (this->GCPType == eGEOCENTRIC)
	{
		if (this->GeographicButton->isChecked()) this->GCPType = eGEOGRAPHIC;
		else if (this->GridButton->isChecked()) this->GCPType = eGRID;
	}
	else if(this->GCPType == eGRID)
	{
		if (this->GeographicButton->isChecked()) this->GCPType = eGEOGRAPHIC;
		else if (this->GeocentricButton->isChecked()) this->GCPType = eGEOCENTRIC;
	}
	else
	{
		if (this->GeographicButton->isChecked()) this->GCPType = eGEOGRAPHIC;
		else if (this->GeocentricButton->isChecked()) this->GCPType = eGEOCENTRIC;
		else this->GCPType = eGRID;
	}

	this->setCoordinateTypeGUI();
};

//================================================================================

void bui_GCPMatchingDialog::GCPchanged()
{
	this->setGCP = !this->setGCP;
	this->setGCPcheckBox->blockSignals(true);
	this->setGCPcheckBox->setChecked(this->setGCP);
	this->setGCPcheckBox->blockSignals(false);
};

//================================================================================

bool bui_GCPMatchingDialog::isInitialised() const 
{ 
	if (!this->pGCPmatcher) return false;

	if (this->GCPType == eUndefinedCoordinate) return false;
	
	return this->pGCPmatcher->isInitialised();
}

//================================================================================
	  
void bui_GCPMatchingDialog::setCurrentOrtho(int index)
{
	int nImgs = 0;
	if (this->pGCPmatcher)
	{
		nImgs = this->pGCPmatcher->getNumberOfPossibleOrthophotos();
		this->pGCPmatcher->setCurrentOrthoImage(index);
	}

	this->OrthoImages->blockSignals(true);

	bool enableOK = true;

	if (index >= nImgs)
	{
		enableOK = false;
		this->OrthoImages->clear();
		this->OrthoImages->addItem("No image available");
		index = 0;
	}

	this->buttonOk->setEnabled(enableOK);

	this->OrthoImages->blockSignals(false);
}
	  
//================================================================================

void bui_GCPMatchingDialog::ImageSelected(int row)
{
	QListWidgetItem *item = this->ImageSelector->item(row);
	int selIdx = item->data(Qt::UserRole).toInt();

	if (this->pGCPmatcher)
	{
		if (selIdx < (int) this->possibleOriginalImages.size())
		{


		}
	}

	this->checkSetup();
};
	
	  
//================================================================================

void bui_GCPMatchingDialog::ImageClicked(QListWidgetItem *item)
{
	int index = item->data(Qt::UserRole).toInt();
	if (this->pGCPmatcher && this->pDEM)
	{
		if (index < (int) this->possibleOriginalImages.size())
		{		
			vector<CVImage *> selectedImgs;
			for (unsigned int i = 0; i < this->possibleOriginalImages.size(); ++i)
			{
				if (this->ImageSelector->item(i)->checkState() == Qt::Checked) selectedImgs.push_back(this->possibleOriginalImages[i]);
			}

			if (selectedImgs.size() > 0)
			{
				this->pGCPmatcher->setImageVector(selectedImgs, this->pDEM, this->ScenesBridged, this->sceneSizePercentage);
				this->initPossibleOrthos();
				this->setFEXParGUI();
			}
			else 
			{
			}
		}

		this->checkSetup();
	}
};

  	  
//================================================================================

void bui_GCPMatchingDialog::DEMChanged()
{
	int indexSelected = (this->DEMcombo->currentIndex());

	if ((indexSelected >= 0) && (indexSelected < project.getNumberOfDEMs()))
	{
		this->pDEM = project.getDEMAt(indexSelected);
		
		if (this->pGCPmatcher) 
		{
			this->pGCPmatcher->setDEM(this->pDEM, this->ScenesBridged, this->sceneSizePercentage);
		}

		this->initPossibleOrthos();
		this->setFEXParGUI();

		this->DEMcombo->setToolTip(this->pDEM->getFileName()->GetChar());
	}
	else this->pDEM = 0;

	if (this->pDEM)
	{
		this->GCPType = this->pDEM->getReferenceSystem().getCoordinateType();
		this->setCoordinateTypeGUI();
	}

	this->checkSetup();
};

	  
//================================================================================

void bui_GCPMatchingDialog::WallisFilterClicked()
{
	this->WallisFilterCheckBox->blockSignals(true);

	this->useWallis = !this->useWallis;
	this->WallisFilterCheckBox->setChecked(this->useWallis);

	this->WallisFilterCheckBox->blockSignals(false);
};

//================================================================================

void bui_GCPMatchingDialog::OrthoImageSelected(int row)
{
	QListWidgetItem *item = this->OrthoImages->item(row);
	int selIdx = item->data(Qt::UserRole).toInt();

	if (this->pGCPmatcher)
	{
		if (!extractFeatures)
		{
			CVImage *oldImg = this->pGCPmatcher->getOrthoImage();
			if (oldImg != this->pGCPmatcher->getOrthoImageAt(selIdx))
			{
				this->pGCPmatcher->setCurrentOrthoImage(selIdx);
				item->setCheckState(Qt::Checked);
 
				int oldIndex = this->pGCPmatcher->getIndexOfOrthophoto(oldImg);
				item = this->OrthoImages->item(oldIndex);
				item->setCheckState(Qt::Unchecked);
			}
			else
			{
				item->setCheckState(Qt::Checked);
			}
		}
	}

	this->checkSetup();
};
	
	  
//================================================================================

void bui_GCPMatchingDialog::OrthoImageClicked(QListWidgetItem *item)
{
	int index = item->data(Qt::UserRole).toInt();
	if (this->pGCPmatcher)
	{
		if (extractFeatures)
		{		
			vector<CVImage *> selectedImgs;
			for (unsigned int i = 0; i < this->possibleOrthoImages.size(); ++i)
			{
				if (this->OrthoImages->item(i)->checkState() == Qt::Checked) selectedImgs.push_back(this->possibleOrthoImages[i]);
			}

			if ((selectedImgs.size() < 1) && (this->possibleOrthoImages.size() > 1))
			{
				this->OrthoImages->item(0)->setCheckState(Qt::Checked);
				selectedImgs.push_back(this->possibleOrthoImages[0]);
			}

			if (selectedImgs.size() > 0)
			{
				this->pGCPmatcher->setPossibleOrthoVector(selectedImgs);
				this->setFEXParGUI();
			}
		}
		else this->OrthoImageSelected(index);

		this->checkSetup();
	}
};
	  
//================================================================================

void bui_GCPMatchingDialog::PatchSizeChanged()
{
	if (this->pGCPmatcher)
	{
		bool isOK;
	
		int newSize = this->PatchSize->text().toInt(&isOK);

		if (isOK && (newSize > 0))
		{
			CMatchingParameters pars = this->pGCPmatcher->getMatchPars();
			pars.setPatchSize(newSize);
			this->pGCPmatcher->setMatchPars(pars);
			this->setMatchParGUI(pars);
		}
	}
};
	  
//================================================================================

void bui_GCPMatchingDialog::RhoMinChanged()
{
	if (this->pGCPmatcher)
	{
		bool isOK;
	
		double newRho = this->RhoMin->text().toDouble(&isOK) * 0.01;

		if (isOK && (newRho > 0) && (newRho <= 1.0))
		{
			CMatchingParameters pars = this->pGCPmatcher->getMatchPars();
			pars.setRhoMin(newRho);
			this->pGCPmatcher->setMatchPars(pars);
			this->setMatchParGUI(pars);
		}
	}
};
	  
//================================================================================

void bui_GCPMatchingDialog::SearchAreaChanged()
{
	if (this->pGCPmatcher)
	{
		bool isOK;

		double newSA = this->SearchArea->text().toDouble(&isOK);

		if (isOK && (newSA > 0.0))
		{
			CMatchingParameters pars = this->pGCPmatcher->getMatchPars();
			pars.setSigmaGeom(newSA);
			this->pGCPmatcher->setMatchPars(pars);
			this->setMatchParGUI(pars);
		}
	}
};

//================================================================================

void bui_GCPMatchingDialog::SceneSizePercentageChanged()
{
	if (this->pGCPmatcher && this->pDEM)
	{
		bool isOK;
		vector<CVImage *> selectedImgs;
	
		for (int i = 0; i < this->pGCPmatcher->getNumberOfPossibleImages(); ++i)
		{
			selectedImgs.push_back(this->pGCPmatcher->getImageAt(i));
		}


		double newPercentage = this->SceneSizePercentage->text().toDouble(&isOK) * 0.01;

		if (isOK && (newPercentage > 0) && (newPercentage <= 1.0))
		{
			this->sceneSizePercentage = newPercentage;

			if (selectedImgs.size() > 0)
			{
				this->pGCPmatcher->setImageVector(selectedImgs, this->pDEM, this->ScenesBridged, this->sceneSizePercentage);
			}

			this->setFEXParGUI();
			this->checkSetup();
		}
	}
};
	
//================================================================================

void bui_GCPMatchingDialog::NScenesChanged()
{
	if (this->pGCPmatcher && this->pDEM)
	{
		bool isOK;
	
		vector<CVImage *> selectedImgs;

		for (int i = 0; i < this->pGCPmatcher->getNumberOfPossibleImages(); ++i)
		{
			selectedImgs.push_back(this->pGCPmatcher->getImageAt(i));
		}


		int newscenes = this->NScenesBridged->text().toInt(&isOK);

		if (isOK && (newscenes >= 0))
		{
			this->ScenesBridged = newscenes;

			if (selectedImgs.size() > 0)
			{
				this->pGCPmatcher->setImageVector(selectedImgs, this->pDEM, this->ScenesBridged, this->sceneSizePercentage);
			}

			this->setFEXParGUI();
			this->checkSetup();
		}
	}
};
	
//================================================================================

void bui_GCPMatchingDialog::NPointsChanged()
{
	if (this->pGCPmatcher)
	{
		bool isOK;

		int newPts = this->NPoints->text().toInt(&isOK);

		if (isOK && (newPts >= 0))
		{
			FeatureExtractionParameters extractionPars = this->pGCPmatcher->getExtractionPars();
	
			this->nPoints = newPts;
			extractionPars.distNonmaxPts = newPts;

			this->pGCPmatcher->setFeatureExtractionPars(extractionPars);
			this->setFEXParGUI();
		}
	};
}

//================================================================================

void bui_GCPMatchingDialog::MatchParsSelected()
{
	if (this->pGCPmatcher)
	{	
		CMatchingParameters pars = this->pGCPmatcher->getMatchPars();
		Bui_MatchingParametersDlg dlg(this, &(pars));

		dlg.exec();

		if (dlg.getParsChanged())
		{
			pars = project.getMatchingParameter();
			this->pGCPmatcher->setMatchPars(pars);
			this->setMatchParGUI(pars);
		}
	}
}

//================================================================================

void bui_GCPMatchingDialog::FEXParsSelected()
{
	if (this->pGCPmatcher)
	{	
		FeatureExtractionParameters pars =  this->pGCPmatcher->getExtractionPars();

		bui_FeatureExtractionDialog dlg(this, pars, true, false, false, "");

		dlg.exec();

		if (dlg.parametersAccepted())
		{
			pars = dlg.getParameters();

			this->nPoints = pars.distNonmaxPts;
	
			this->pGCPmatcher->setFeatureExtractionPars(pars);
			this->setFEXParGUI();
		}
	}
}

//================================================================================

void bui_GCPMatchingDialog::OK()
{
	CFileName featureFile("GCPFEX.ini");
	featureFile.SetPath(project.getFileName()->GetPath());
	if (this->pGCPmatcher) this->pGCPmatcher->getExtractionPars().writeParameters(featureFile);

	bui_ProgressDlg dlg(new CGCPMatchingThread(this), this->pGCPmatcher, "Matching of GCPs");
	dlg.exec();

	this->success = dlg.getSuccess();

	this->close();
}

//================================================================================

int bui_GCPMatchingDialog::MatchPoints()
{
	int iMatched = 0;

	if (this->pGCPmatcher)
	{
		if (this->pGCPmatcher->getNumberOfPossibleImages() > 0) 
			iMatched =  this->pGCPmatcher->ExtractAndMatchPoints(this->useWallis, this->GCPType, this->setGCP);
		else 
			iMatched = this->pGCPmatcher->MatchPoints(this->useWallis);
	}
	return iMatched;
};
	  
//================================================================================

void bui_GCPMatchingDialog::checkSetup()
{
	this->buttonOk->setEnabled(this->isInitialised());
};

//================================================================================

void bui_GCPMatchingDialog::Cancel()
{
	this->success = false;
	this->close();
}

//================================================================================

void bui_GCPMatchingDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/GCPMatching.html");
};

//================================================================================
