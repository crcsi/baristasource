#include "Bui_TMPropertiesDlg.h"
#include "Spheroid.h"
#include "IniFile.h"
#include <math.h>
#include "ReferenceSystem.h"
#include <QFileDialog>
#include <QDoubleValidator>
#include "BaristaHtmlHelp.h"
#include "Icons.h"

Bui_TMPropertiesDlg::Bui_TMPropertiesDlg(CReferenceSystem* refSystem, const char *title, const char* helpfile): QDialog(NULL),
		helpFile(helpfile)
{
	setupUi(this);
	this->setWindowTitle(QString(title));

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
	this->setModal(true);


	// save parameter
	this->refSys = refSystem;

	this->initDlg();

	this->le_CentralMeridianInput->setValidator(new QDoubleValidator(this));
	this->le_LatitudeOriginInput->setValidator(new QDoubleValidator(this));
	this->le_Scale->setValidator(new QDoubleValidator(this));
	this->le_easting->setValidator(new QDoubleValidator(this));
	this->le_northing->setValidator(new QDoubleValidator(this));

	this->connect(this->le_CentralMeridianInput,SIGNAL(editingFinished()),this,SLOT(checkValues()));
	this->connect(this->le_LatitudeOriginInput,SIGNAL(editingFinished()),this,SLOT(checkValues()));
	this->connect(this->le_Scale,      SIGNAL(editingFinished()),this,SLOT(checkValues()));
	this->connect(this->le_easting,    SIGNAL(editingFinished()),this,SLOT(checkValues()));
	this->connect(this->le_northing,   SIGNAL(editingFinished()),this,SLOT(checkValues()));
	this->connect(this->le_CentralMeridianInput,SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));
	this->connect(this->le_LatitudeOriginInput,SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));
	this->connect(this->le_Scale,      SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));
	this->connect(this->le_easting,    SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));
	this->connect(this->le_northing,   SIGNAL(textChanged(const QString &)),this,SLOT(checkValues(const QString &)));

	this->connect(this->pB_Help,       SIGNAL(released()),this,SLOT(callHelp()));
	this->connect(this->pB_Apply,       SIGNAL(released()),this,SLOT(pBApplySettings()));
	this->connect(this->pB_cancel,       SIGNAL(released()),this,SLOT(pBCancelReleased()));




}



void Bui_TMPropertiesDlg::initDlg()
{
	this->setSuccess(false);

	double lastLongitude = inifile.getCentralMeridian();
	double lastLatitude = inifile.getLatitudeOrigin();
	double lastScale = inifile.getTMScale();
	double lastNorthing = inifile.getTMNorthing();
	double lastEasting = inifile.getTMEasting();
	// set default values

	if (fabs(lastLongitude + 1.0) > 0.000001)
		this->le_CentralMeridianInput->setText(QString().sprintf("%.8lf",lastLongitude));
	else
		this->le_CentralMeridianInput->setText(QString("0.0"));

	if (fabs(lastLatitude + 1.0) > 0.000001)
		this->le_LatitudeOriginInput->setText(QString().sprintf("%.8lf",lastLatitude));
	else
		this->le_LatitudeOriginInput->setText(QString("0.0"));
	
	
	this->le_Scale->setText(QString().sprintf("%.8lf",lastScale));
	this->le_easting->setText(QString().sprintf("%.4lf",lastEasting));
	this->le_northing->setText(QString().sprintf("%.4lf",lastNorthing));

	this->checkValues();

}





Bui_TMPropertiesDlg::~Bui_TMPropertiesDlg(void)
{
}




void Bui_TMPropertiesDlg::pBCancelReleased()
{
	this->setSuccess(false);
	this->close();
}



void Bui_TMPropertiesDlg::pBApplySettings()
{
	this->refSys->setCoordinateType(eGRID);
	this->refSys->setToWGS1984();


	double longitude = this->le_CentralMeridianInput->text().toDouble();
	double latitude =  this->le_LatitudeOriginInput->text().toDouble();

	double FEasting  = this->le_easting->text().toDouble(); 
	double FNorthing = this->le_northing->text().toDouble(); 
	double scale     = this->le_Scale->text().toDouble();

	this->refSys->setTMParameters(longitude, FEasting, FNorthing, scale, latitude);

	inifile.setCentralMeridian(longitude);
	inifile.setLatitudeOrigin(latitude);
	inifile.setTMScale(scale);
	inifile.setTMEasting(FEasting);
	inifile.setTMNorthing(FNorthing);

	inifile.setEllipsoidName(this->refSys->getSpheroid().getName());
	inifile.setIsUTM(this->refSys->isUTM());
	inifile.setIsWGS84(this->refSys->isWGS84());
	inifile.setCoordinateType(this->refSys->getCoordinateType());
	inifile.writeIniFile();

	this->setSuccess(true);
	this->close();
}

void Bui_TMPropertiesDlg::checkValues()
{

	if (!this->le_CentralMeridianInput->hasAcceptableInput() ||
		!this->le_LatitudeOriginInput->hasAcceptableInput() ||
		!this->le_Scale->hasAcceptableInput() ||
		!this->le_easting->hasAcceptableInput() ||
		!this->le_northing->hasAcceptableInput() )
	{
		this->pB_Apply->setEnabled(false);
	}
	else
	{
		this->pB_Apply->setEnabled(true);
	}

}

void Bui_TMPropertiesDlg::checkValues(const QString& text)
{
	this->checkValues();
}

void Bui_TMPropertiesDlg::callHelp()
{
	CHtmlHelp::callHelp(helpFile);
}