#include "Bui_ProjectSettingsDlg.h"

#include "BaristaProject.h"
#include "Icons.h"
#include <QColorDialog>
#include "BaristaViewEventHandler.h"

extern CBaristaProject project;

Bui_ProjectSettingsDlg::Bui_ProjectSettingsDlg(QWidget *parent)
	: QDialog(parent),oldMonoplottedLineWidth(project.getMonoplottedLineWidth()),oldBuildingLineWidth(project.getBuildingLineWidth())
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));
	this->setWindowTitle("Project Settings Dialog");

	this->initDlg();

	// pushbutton connections
	this->connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));

	this->connect(this->pB_MonoplottedLineColor,SIGNAL(released()),this,SLOT(pBMonoplottedLineColor()));
	this->connect(this->pB_BuildingLineColor,SIGNAL(released()),this,SLOT(pBBuildingLineColor()));


	// spin box connections
	//this->connect(this->spinboxMeanHeight, SIGNAL(valueChanged(double)), this, SLOT(setspinboxMeanHeight(double)));

	this->connect(this->sB_MonoplottedLineWidth, SIGNAL(valueChanged(int)), this, SLOT(setMonoplottedLineWidth(int)));
	this->connect(this->sB_BuildingLineWidth, SIGNAL(valueChanged(int)), this, SLOT(setBuildingLineWidth(int)));
 
	QColor monoplottedLineColor(project.getMonoplottedLineColor().x,project.getMonoplottedLineColor().y,project.getMonoplottedLineColor().z);
	this->oldMonoplottedLineColor = monoplottedLineColor;

	QColor buildingLineColor(project.getBuildingLineColor().x,project.getBuildingLineColor().y,project.getBuildingLineColor().z);
	this->oldBuildingLineColor = buildingLineColor;

}

Bui_ProjectSettingsDlg::~Bui_ProjectSettingsDlg()
{

}



void Bui_ProjectSettingsDlg::initDlg()
{
	this->spinboxMeanHeight->setRange(-100, 9000);
	this->spinboxMeanHeight->setSingleStep(50);
	
	if ( project.getMeanHeight() == -9999.0) project.setMeanHeight(0.0);
	this->setspinboxMeanHeight(project.getMeanHeight());

	// Monoplotted Line settings
	if (project.getMonoplottedLineColor().id < 0 )
	{
		QColor cyan = Qt::cyan;
		C3DPoint lcolor(cyan.red(), cyan.green(), cyan.blue());
		lcolor.setID(1);
		project.setMonoplottedLineColor(lcolor);
	}

	this->statMonoplottedLineColor.setRgb(
		(int)project.getMonoplottedLineColor().x,
		(int)project.getMonoplottedLineColor().y, 
		(int)project.getMonoplottedLineColor().z);

	QPalette palette = this->lE_MonoplottedLineColor->palette();
	palette.setColor(QPalette::Base,this->statMonoplottedLineColor);
	this->lE_MonoplottedLineColor->setPalette(palette);
	this->sB_MonoplottedLineWidth->setRange(1, 30);

	if (project.getMonoplottedLineWidth() < 1) project.setMonoplottedLineWidth(1);
	this->sB_MonoplottedLineWidth->setValue(project.getMonoplottedLineWidth());

	// Building settings
	if (project.getBuildingLineColor().id < 0 )
	{
		QColor cyan = Qt::cyan;
		C3DPoint lcolor(cyan.red(), cyan.green(), cyan.blue());
		lcolor.setID(1);
		project.setBuildingLineColor(lcolor);
	}

	this->statBuildingLineColor.setRgb(
		(int)project.getBuildingLineColor().x,
		(int)project.getBuildingLineColor().y, 
		(int)project.getBuildingLineColor().z);

	palette = this->lE_BuildingLineColor->palette();
	palette.setColor(QPalette::Base,this->statBuildingLineColor);
	this->lE_BuildingLineColor->setPalette(palette);
	this->sB_BuildingLineWidth->setRange(1, 30);

	if (project.getBuildingLineWidth() < 1) project.setBuildingLineWidth(1);
	this->sB_BuildingLineWidth->setValue(project.getBuildingLineWidth());

}


void Bui_ProjectSettingsDlg::pBCancelReleased()
{
	this->close();
}

void Bui_ProjectSettingsDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/MenuSettings.html");
}

void Bui_ProjectSettingsDlg::pBOkReleased()
{

	double height = this->spinboxMeanHeight->value();

	project.setMeanHeight(height);

	QColor newMonoplottedLineColor(project.getMonoplottedLineColor().x,project.getMonoplottedLineColor().y,project.getMonoplottedLineColor().z);
	if (this->oldMonoplottedLineWidth != project.getMonoplottedLineWidth() || newMonoplottedLineColor != this->oldMonoplottedLineColor)
	{
		currentBaristaViewHandler->drawingSettingsChanged(newMonoplottedLineColor,project.getMonoplottedLineWidth());
	}

	QColor newBuildingLineColor(project.getBuildingLineColor().x,project.getBuildingLineColor().y,project.getBuildingLineColor().z);
	if (this->oldBuildingLineWidth != project.getBuildingLineWidth() || newBuildingLineColor != this->oldBuildingLineColor)
	{
		currentBaristaViewHandler->drawingSettingsChanged(newBuildingLineColor,project.getBuildingLineWidth());
	}

	this->close();
}

void Bui_ProjectSettingsDlg::setspinboxMeanHeight(double height)
{
	this->spinboxMeanHeight->setValue(height);

}

void Bui_ProjectSettingsDlg::pBMonoplottedLineColor()
{
	this->statMonoplottedLineColor = QColorDialog::getColor(this->statMonoplottedLineColor,this);
	QPalette palette = this->lE_MonoplottedLineColor->palette();
	palette.setColor(QPalette::Base,this->statMonoplottedLineColor);
	this->lE_MonoplottedLineColor->setPalette(palette);

	C3DPoint lcolor(1, this->statMonoplottedLineColor.red(), this->statMonoplottedLineColor.green(), this->statMonoplottedLineColor.blue());
	project.setMonoplottedLineColor(lcolor);
}

void Bui_ProjectSettingsDlg::setMonoplottedLineWidth(int width)
{
	project.setMonoplottedLineWidth(width);
}

void Bui_ProjectSettingsDlg::pBBuildingLineColor()
{
	this->statBuildingLineColor = QColorDialog::getColor(this->statBuildingLineColor,this);
	QPalette palette = this->lE_BuildingLineColor->palette();
	palette.setColor(QPalette::Base,this->statBuildingLineColor);
	this->lE_BuildingLineColor->setPalette(palette);

	C3DPoint lcolor(1, this->statBuildingLineColor.red(), this->statBuildingLineColor.green(), this->statBuildingLineColor.blue());
	project.setBuildingLineColor(lcolor);
}

void Bui_ProjectSettingsDlg::setBuildingLineWidth(int width)
{
	project.setBuildingLineWidth(width);
}