#include "Bui_DEMDifferenceDlg.h"
#include "Icons.h"
#include "BaristaProject.h"
#include "BaristaHtmlHelp.h"
#include "GDALFile.h"
#include "Bui_GDALExportDlg.h"
#include "IniFile.h"
#include <QMessageBox>
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"
#include "LookupTableHelper.h"
#include "histogram.h"
#include "BaristaProjectHelper.h"


Bui_DEMDifferenceDlg::Bui_DEMDifferenceDlg(QWidget* parent) : outname(""), success(false),dlg(NULL)
	,newdem(NULL), masterIndex(-1), compareIndex(-1), useColor(true),zero(0),QDialog(parent)
{
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	// pushbutton connections
	this->connect(this->pBCancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pBOk,SIGNAL(released()),this,SLOT(pBOkReleased()));
	this->connect(this->pBHelp,SIGNAL(released()),this,SLOT(pBHelpReleased()));

	// output connection
	connect(this->pbOutDEM, SIGNAL(released()),this, SLOT(pBSelectOutputReleased()));

	// selection of DEM from comboMaster and comboDiff
	this->connect(this->comboMaster, SIGNAL(currentIndexChanged(int)),this, SLOT(MasterSelectionChanged(int)));
	this->connect(this->comboDiff, SIGNAL(currentIndexChanged(int)),this, SLOT(DiffSelectionChanged(int)));
	this->connect(this->cB_Create, SIGNAL(stateChanged(int)),this,SLOT(cBCreateColor(int)));
	this->connect(this->lE_Zero,SIGNAL(textChanged ( const QString &)),this,SLOT(textChanged ( const QString &)));
	this->connect(this->lE_Zero,SIGNAL(editingFinished ()),this,SLOT(lEZero()));

	this->lE_Zero->setValidator(new QDoubleValidator(this));

	this->fillComboMaster();
	this->fillComboDiff();
	this->rbMasterArea->setChecked(true);
	this->cB_Create->setChecked(useColor);
	this->lE_Zero->setText(QString("%1").arg(this->zero));
	this->checkSetup();


}

Bui_DEMDifferenceDlg::~Bui_DEMDifferenceDlg()
{
	if (this->dlg)
		delete this->dlg;

	this->dlg=NULL;

	if (!this->success)
	{
		if ( this->newdem )
			project.deleteDEM(newdem);
		this->newdem = NULL;
	}
}

void Bui_DEMDifferenceDlg::pBOkReleased()
{
	if (this->outname.IsEmpty())
		return;

	this->setSuccess(true);
	this->close();
}

void Bui_DEMDifferenceDlg::pBCancelReleased()
{
	this->setSuccess(false);
	this->close();
}

void Bui_DEMDifferenceDlg::pBHelpReleased()
{
	CHtmlHelp::callHelp("UserGuide/DifferenceDEMs.html");
}

void Bui_DEMDifferenceDlg::cBCreateColor(int state)
{
	if (state == Qt::Checked)
	{
		this->lE_Zero->setEnabled(true);
		this->useColor = true;
	}
	else
	{
		this->lE_Zero->setEnabled(false);
		
		this->useColor = false;
	}
	
	this->checkSetup();
}

void Bui_DEMDifferenceDlg::textChanged( const QString & text)
{
	this->checkSetup();

	
}

void Bui_DEMDifferenceDlg::lEZero()
{
	if (this->lE_Zero->hasAcceptableInput())
		this->zero = this->lE_Zero->text().toFloat();

	this->checkSetup();
}

void Bui_DEMDifferenceDlg::pBSelectOutputReleased()
{
	if (this->dlg)
		delete this->dlg;

	if (this->newdem)
		project.deleteDEM(newdem);
	
	this->outname = "";
	this->outputDEMFile->setText("");

	this->newdem = project.addDEM("DiffDEM");
	this->newdem->getDEM()->bands = 1;
	this->newdem->getDEM()->bpp = 32;

	this->dlg = new Bui_GDALExportDlg(this->outname,this->newdem,true,"Export DEM");
	
	this->dlg->cB_ExportGeo->setChecked(true);

	this->dlg->exec();

	if (!this->dlg->getSuccess())	// false if user has pressed "Cancel"
	{
		if (this->newdem)
			project.deleteDEM(this->newdem);

		this->newdem = NULL;
		this->outname = "";
	}
	else
	{
		this->outname = this->dlg->getFilename();
		this->outputDEMFile->setText(this->outname.GetFullFileName().GetChar());
		this->newdem->setFileName(this->outname.GetChar());
	}

	this->checkSetup();
}

bool Bui_DEMDifferenceDlg::createDifferenceDEM()
{
	this->success = this->difference();
	return this->getSuccess();
}

void Bui_DEMDifferenceDlg::checkSetup()
{
	bool readyToGo = true;
	this->pBOk->setEnabled(false);
	this->pbOutDEM->setEnabled(false);

	// selection
	if ( this->masterIndex == -1 || this->compareIndex == -1 )
	{
		readyToGo = false;
		return;
	}

	// check ReferenceSystems
	if ( !this->checkReferenceSystems() )
	{
		QMessageBox::warning(this,"DEM selection not valid",
		"DEMs are defined in different Reference systems!",1,0,0);
		readyToGo = false;
		return;
	}

	this->pbOutDEM->setEnabled(true);

	// check filename is set
	if ( this->outname.IsEmpty() )
		readyToGo = false;

	if (this->useColor && !this->lE_Zero->hasAcceptableInput())
	{
		readyToGo = false;
	}
	
	if ( readyToGo ) this->pBOk->setEnabled(true);
}


bool Bui_DEMDifferenceDlg::difference()
{
	CVDEM* masterVDEM = project.getDEMAt(this->masterIndex);
	if ( !masterVDEM ) return false;
	CDEM* master = masterVDEM->getDEM();

	CReferenceSystem refsys = master->getTFW()->getRefSys();

	CVDEM* compareVDEM = project.getDEMAt(this->compareIndex);
	if ( !compareVDEM ) return false;
	CDEM* compare = compareVDEM->getDEM();
	
	this->gridx = master->getgridX();
	this->gridy = master->getgridY();

	this->xmin = master->getminX();
	this->xmax = master->getmaxX();
	this->ymin = master->getminY();
	this->ymax = master->getmaxY();

	if ( this->rbMasterArea->isChecked() )
	{
		// get dimensions from master
		this->gridx = master->getgridX();
		this->gridy = master->getgridY();

		this->xmin = master->getminX();
		this->xmax = master->getmaxX();
		this->ymin = master->getminY();
		this->ymax = master->getmaxY();

		this->cols = master->getWidth();
		this->rows = master->getHeight();
	}
	else if ( this->rbOverlap->isChecked() )
	{
		// get dimensions from overlap
		this->gridx = master->getgridX();
		this->gridy = master->getgridY();

		this->xmin = master->getminX();
		this->xmax = master->getmaxX();
		this->ymin = master->getminY();
		this->ymax = master->getmaxY();

		double cxmin = compare->getminX();
		double cxmax = compare->getmaxX();
		double cymin = compare->getminY();
		double cymax = compare->getmaxY();

		bool overlap = true;

		// no overlap cases
		if ( cxmax < this->xmin ) overlap = false;
		if ( cxmin > this->xmax ) overlap =false;

		if ( cymax < this->ymin ) overlap = false;
		if ( cymin > this->ymax ) overlap =false;

		if ( !overlap ) return overlap;
		
		if (  cxmin > this->xmin ) this->xmin =  cxmin;
		if (  cxmax < this->xmax ) this->xmax =  cxmax;
	
		if (  cymin > this->ymin ) this->ymin =  cymin;
		if (  cymax < this->ymax ) this->ymax =  cymax;

		if ( overlap )
		{
			this->cols = (this->xmax - this->xmin ) /this->gridx;
			this->rows = (this->ymax - this->ymin ) /this->gridy;
		}
		else
			return false;

		
	}
	else
		return false;
	

	this->newdem->getDEM()->width = this->cols;
	this->newdem->getDEM()->height = this->rows;

/////////////////////
	if ( refsys.getCoordinateType() == eGEOGRAPHIC )
	{
		Matrix parameters(6,1);
		parameters.element(0,0) = 0.0;
		parameters.element(1,0) = -this->gridy;
		parameters.element(2,0) = this->xmax;
		parameters.element(3,0) = this->gridx;
		parameters.element(4,0) = 0.0;
		parameters.element(5,0) = this->ymin;

		CTFW tfw(parameters);
		tfw.setRefSys(refsys);

		this->newdem->getDEM()->setTFW(tfw);
	}
	else
	{
		Matrix parameters(6,1);
		parameters.element(0,0) = this->gridx;
		parameters.element(1,0) = 0.0;
		parameters.element(2,0) = this->xmin;
		parameters.element(3,0) = 0.0;
		parameters.element(4,0) = -this->gridy;
		parameters.element(5,0) = this->ymax;

		CTFW tfw(parameters);
		tfw.setRefSys(refsys);

		this->newdem->getDEM()->setTFW(tfw);
	}




/////////////////////
	newdem->getDEM()->setFileName(this->outname.GetChar());

	
	bui_ProgressDlg progressdlg(new CDEMDifferenceThread(this->newdem->getDEM(),master, compare),this->newdem->getDEM(),"Differencing DEMs ...");
	
	progressdlg.exec();

	if (!progressdlg.getSuccess())
	{
		this->success = false;
	}

	if ( this->success && this->dlg)
	{
		
		if (this->useColor)
		{
			vector<double> parameter;
			parameter.resize(3);
			newdem->getHugeImage()->getHistogram()->updateMinMax(0.0,1.0);
			
			CLookupTableHelper helper;
			CLookupTable lut(newdem->getDEM()->getBpp(),3);
			
			helper.createDEMDiffLUT(lut,
									newdem->getHugeImage()->getHistogram()->getMin(),
									this->zero,
									newdem->getHugeImage()->getHistogram()->getMax());

			newdem->getDEM()->setLookupTable(lut);
			newdem->setZeroValue(this->zero);
		}
		if ( this->dlg->writeSettings() )
			this->dlg->writeImage("Writing DEM as image file");

		if (!this->dlg->getSuccess())
		{
			this->success = false;
		}
	}
	
	baristaProjectHelper.refreshTree();
	return this->success;
}


bool Bui_DEMDifferenceDlg::checkReferenceSystems()
{
	this->getMasterSelection();
	this->getCompareSelection();
	
	if ( (this->masterIndex == -1) || (this->compareIndex == -1) ) return false;

	CVDEM* masterDem = project.getDEMAt(this->masterIndex);
	CVDEM* compareDem = project.getDEMAt(this->compareIndex);

	if ( !masterDem || !compareDem ) return false;

	CReferenceSystem mrefsys = masterDem->getReferenceSystem();
	CReferenceSystem crefsys = compareDem->getReferenceSystem();

	if ( mrefsys != crefsys )
			return false;

	return true;
}

void Bui_DEMDifferenceDlg::fillComboMaster()
{
	this->comboMaster->blockSignals(true);
	this->comboMaster->setEnabled(false);
	this->comboMaster->clear();

	for (int i = 0; i < project.getNumberOfDEMs(); i++)
	{
		CVDEM* vdem = project.getDEMAt(i);

		QString demname(vdem->getFileName()->GetChar());
	
		// write dem name in comboMaster
		this->comboMaster->addItem(demname);
	}

	if ( this->comboMaster->count() > 0 )
	{
		this->comboMaster->setEnabled(true);
		this->comboMaster->setCurrentIndex(0);
		this->getMasterSelection();
	}

	this->comboMaster->blockSignals(false);
}

void Bui_DEMDifferenceDlg::fillComboDiff()
{
	this->comboDiff->blockSignals(true);
	this->comboDiff->setEnabled(false);
	this->comboDiff->clear();

	for (int i = 0; i < project.getNumberOfDEMs(); i++)
	{
		CVDEM* vdem = project.getDEMAt(i);

		QString demname(vdem->getFileName()->GetChar());
	
		// write dem name in comboDiff
		this->comboDiff->addItem(demname);
	}

	if ( this->comboDiff->count() > 1 )
	{
		this->comboDiff->setEnabled(true);
		this->comboDiff->setCurrentIndex(1);
		this->getCompareSelection();
	}

	this->comboDiff->blockSignals(false);
}

void Bui_DEMDifferenceDlg::getMasterSelection()
{
	int mindex = this->comboMaster->currentIndex();
	QString mName =  this->comboMaster->itemText(mindex);
    CCharString searchname = (const char*)mName.toLatin1();

	this->masterIndex = -1;

	CVDEM* sDEM = project.getDEMByName(searchname);
	if ( sDEM != NULL ) this->masterIndex = project.getDEMIndex(sDEM);

	if (  this->masterIndex > -1 ) this->comboMaster->setCurrentIndex(this->masterIndex);
}

void Bui_DEMDifferenceDlg::getCompareSelection()
{
	int cindex = this->comboDiff->currentIndex();
	QString mName =  this->comboDiff->itemText(cindex);
    CCharString searchname = (const char*)mName.toLatin1();

	this->compareIndex = -1;

	CVDEM* sDEM = project.getDEMByName(searchname);
	if ( sDEM != NULL ) this->compareIndex = project.getDEMIndex(sDEM);

	if (   this->compareIndex > -1 ) this->comboDiff->setCurrentIndex( this->compareIndex);
}

void Bui_DEMDifferenceDlg::MasterSelectionChanged(int i)
{
	this->getMasterSelection();
	this->checkSetup();
}

void Bui_DEMDifferenceDlg::DiffSelectionChanged(int i)
{
	this->getCompareSelection();
	this->checkSetup();
}

