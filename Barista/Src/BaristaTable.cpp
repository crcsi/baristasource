#include "BaristaTable.h"
#include <QHeaderView>

CBaristaTable::CBaristaTable(QWidget* parent) : QTableWidget(parent),firstDraw(true)
{
	this->setAttribute(Qt::WA_DeleteOnClose);
	//this->setSelectionMode(QAbstractItemView::ContiguousSelection);
	this->setSelectionBehavior(QAbstractItemView::SelectRows);
}

CBaristaTable::~CBaristaTable(void)
{
}

/*
Qt::ItemIsSelectable    1  It can be selected.
Qt::ItemIsEditable      2  It can be edited.
Qt::ItemIsDragEnabled   4  It can be dragged.
Qt::ItemIsDropEnabled   8  It can be used as a drop target.
Qt::ItemIsUserCheckable 16 It can be checked or unchecked by the user.
Qt::ItemIsEnabled       32 The user can interact with the item.
Qt::ItemIsTristate      64 The item is checkable with three separate states.

The ItemFlags type is a typedef for QFlags<ItemFlag>. It stores an OR combination of ItemFlag values.
*/

QTableWidgetItem* CBaristaTable::createItem(QString value,Qt::ItemFlags flags,Qt::Alignment alignment, QColor textColor)
{
	QTableWidgetItem* item= new QTableWidgetItem(value);
	item->setTextColor(textColor);
	item->setBackgroundColor(QColor(255, 255, 255));
	item->setTextAlignment(alignment);
	item->setFlags(flags);
	return item;
}


CLabel CBaristaTable::getItemAsLabel(int row, int col)
{
	QByteArray qba = this->item(row, col)->text().toLatin1();
	CLabel label(qba.constData());
	return label;
}

void CBaristaTable::setLabelAsItemText(CLabel label, int row, int col)
{
	QString str = label.GetChar();
	
	this->blockSignals(true);
	this->item(row,col)->setText(str);
	this->blockSignals(false);
}


QModelIndex CBaristaTable::getIndexOfItem(QTableWidgetItem* item)
{
	return this->indexFromItem(item);
}

void CBaristaTable::updateContents()
{	
	this->blockSignals(true);
	this->setRowCount(0);
	this->initTable();
	this->blockSignals(false);
}

void CBaristaTable::elementsChanged(CLabel element)
{
    this->updateContents();
}
