#include <fstream>

#include "Bui_ImportALS.h"
#include <QFileDialog>
#include <QMessageBox> 
#include <QHeaderView>

#include "Icons.h"
#include "IniFile.h"
#include "systemUtilities.h"
#include "BaristaHtmlHelp.h"
#include "Bui_ReferenceSysDlg.h"
#include "BaristaProject.h"


bui_ImportALSDialog::bui_ImportALSDialog () : filenameSelected(false), inputFilenames(0), 
											  eFormat(eALSTXYZI), 
											  ePulseMode (eALSFirstLast),
											  success(false), offset(0,0), delta (1,1),
											  hasStrips(true)

{
	setupUi(this);

	connect(this->Inputfile,            SIGNAL(released()),             this, SLOT(onFileSelected()));

	connect(this->XYZButton,            SIGNAL(clicked(bool)),          this, SLOT(onFormatChanged()));
	connect(this->XYZIButton,           SIGNAL(clicked(bool)),          this, SLOT(onFormatChanged()));
	connect(this->XYZITButton,          SIGNAL(clicked(bool)),          this, SLOT(onFormatChanged()));
	connect(this->TXYZButton,           SIGNAL(clicked(bool)),          this, SLOT(onFormatChanged()));
	connect(this->TXYZIButton,          SIGNAL(clicked(bool)),          this, SLOT(onFormatChanged()));
	connect(this->TopoSysButton,        SIGNAL(clicked(bool)),          this, SLOT(onFormatChanged()));
	connect(this->LASButton,            SIGNAL(clicked(bool)),          this, SLOT(onFormatChanged()));

	connect(this->LPButton,             SIGNAL(clicked(bool)),          this, SLOT(onPulseModeChanged()));
	connect(this->FPButton,             SIGNAL(clicked(bool)),          this, SLOT(onPulseModeChanged()));
	connect(this->FPLPButton,           SIGNAL(clicked(bool)),          this, SLOT(onPulseModeChanged()));

	connect(this->XOffset,              SIGNAL(editingFinished()),      this, SLOT(onOffsetChanged()));
	connect(this->YOffset,              SIGNAL(editingFinished()),      this, SLOT(onOffsetChanged()));

	connect(this->ImportStripsButton,   SIGNAL(clicked(bool)),          this, SLOT(onImportModeChanged()));
	connect(this->ImportTileButton,     SIGNAL(clicked(bool)),          this, SLOT(onImportModeChanged()));

	connect(this->PtSpacingInFlight,    SIGNAL(editingFinished()),      this, SLOT(onDeltaChanged()));
	connect(this->PtSpacingCrossFlight, SIGNAL(editingFinished()),      this, SLOT(onDeltaChanged()));

	connect(this->HelpButton,           SIGNAL(released()),             this, SLOT(Help()));
	connect(this->buttonCancel,         SIGNAL(released()),             this, SLOT(Cancel()));
	connect(this->buttonOk,             SIGNAL(released()),             this, SLOT(OK()));

	  
	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->offset.x  = inifile.getALSoffsetX();
	this->offset.y  = inifile.getALSoffsetY();
	this->delta.x   = inifile.getALSdeltaX();
	this->delta.y   = inifile.getALSdeltaY();
	this->hasStrips = inifile.getALShasStrips();
	int i = inifile.getALSFormat();

	switch(i)
	{
	  case 0:  this->eFormat = eALSXYZ;     break; 
	  case 1:  this->eFormat = eALSXYZI;    break; 
	  case 2:  this->eFormat = eALSXYZIT;   break; 
	  case 3:  this->eFormat = eALSTXYZ;    break; 
	  case 4:  this->eFormat = eALSTXYZI;   break; 
	  case 5:  this->eFormat = eALSTopoSys; break; 
	  case 6:  this->eFormat = eALSLAS;     break; 
	  default: this->eFormat = eALSTXYZI;   break;
	}
	
	i = inifile.getALSPulseMode();

	switch(i)
	{
	  case 0:  this->ePulseMode = eALSFirst;     break; 
	  case 1:  this->ePulseMode = eALSLast;      break; 
	  case 2:  this->ePulseMode = eALSFirstLast; break; 
	  default: this->ePulseMode = eALSFirstLast; break;
	}

	this->init();
}

bui_ImportALSDialog ::~bui_ImportALSDialog ()
{
	this->destroy();
}

void bui_ImportALSDialog::destroy()
{

}

void bui_ImportALSDialog::init()
{
	this->filenameSelected = false;
	
	this->InputFiles->setColumnCount(1);
	this->InputFiles->setRowCount(1);
	this->InputFiles->setHorizontalHeaderItem(0,new QTableWidgetItem("ALS Files"));
	this->InputFiles->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

	this->setDataToGUI();

	this->InputFiles->setEnabled(false);
	this->InputFiles->blockSignals(true);

	this->buttonOk->setEnabled(false);

}
	  
void bui_ImportALSDialog::setDataToGUI()
{
	this->XYZButton->blockSignals(true);
	this->XYZIButton->blockSignals(true);
	this->XYZITButton->blockSignals(true);
	this->TXYZButton->blockSignals(true);
	this->TXYZIButton->blockSignals(true);
	this->TopoSysButton->blockSignals(true);
	this->LASButton->blockSignals(true);

	this->XYZButton->setChecked(false);
	this->XYZIButton->setChecked(false);
	this->XYZITButton->setChecked(false);
	this->TXYZButton->setChecked(false);
	this->TXYZIButton->setChecked(false);
	this->TopoSysButton->setChecked(false);
	this->LASButton->setChecked(false);

	if (eFormat == eALSLAS)
	{ 	
		LASButton->setChecked(true);    	
							  
		this->ePulseMode = eALSFirstLast;		
		this->hasStrips = false;
		
		this->FPButton->setEnabled(false);
		this->LPButton->setEnabled(false);
		this->FPLPButton->setEnabled(true);

		this->ImportStripsButton->setEnabled(false);
		this->ImportTileButton->setEnabled(false);		
	}
	else 
	{
		this->FPButton->setEnabled(true);
		this->LPButton->setEnabled(true);					  
		this->ImportStripsButton->setEnabled(true);					  
		this->ImportTileButton->setEnabled(true);

		if (eFormat == eALSTopoSys) 
		{
			this->TopoSysButton->setChecked(true);
			if (ePulseMode == eALSFirstLast)
			{
				ePulseMode = eALSLast;
			}
			this->FPLPButton->setEnabled(false);
		}
		else
		{
			this->FPLPButton->setEnabled(true);
			if (eFormat == eALSXYZ) XYZButton->setChecked(true);
			else if (eFormat == eALSXYZI) XYZIButton->setChecked(true);
			else if (eFormat == eALSXYZIT) XYZITButton->setChecked(true);
			else if (eFormat == eALSTXYZ) TXYZButton->setChecked(true);
			else if (eFormat == eALSTXYZI) TXYZIButton->setChecked(true);
		}
	}

	this->XYZButton->blockSignals(false);
	this->XYZIButton->blockSignals(false);
	this->XYZITButton->blockSignals(false);
	this->TXYZButton->blockSignals(false);
	this->TXYZIButton->blockSignals(false);
	this->TopoSysButton->blockSignals(false);
	this->LASButton->blockSignals(false);
		
	this->LPButton->blockSignals(true);
	this->FPButton->blockSignals(true);
	this->FPLPButton->blockSignals(true);

	switch(ePulseMode)
	{
		case eALSFirst    : { 
								FPButton->setChecked(true);    
								LPButton->setChecked(false);    
								FPLPButton->setChecked(false);    
							}
							break; 

		case eALSLast     : { 
								FPButton->setChecked(false);    
								LPButton->setChecked(true);    
								FPLPButton->setChecked(false);    
							}
							break;

		case eALSFirstLast: { 
								FPButton->setChecked(false);    
								LPButton->setChecked(false);    
								FPLPButton->setChecked(true);    
							}
							break;
		default: break; 
	}

	this->LPButton->blockSignals(false);
	this->FPButton->blockSignals(false);
	this->FPLPButton->blockSignals(false);

	this->ImportStripsButton->blockSignals(true);
	this->ImportTileButton->blockSignals(true);
	this->ImportStripsButton->setChecked(this->hasStrips);
	this->ImportTileButton->setChecked(!this->hasStrips);
	this->ImportStripsButton->blockSignals(false);
	this->ImportTileButton->blockSignals(false);

	this->PtSpacingInFlight->blockSignals(true);
	this->PtSpacingCrossFlight->blockSignals(true);

	QString ds=QString("%1").arg(delta.x, 0, 'f', 2);
	this->PtSpacingInFlight->setText(ds);
	ds=QString("%1").arg(delta.y, 0, 'f', 2);
	this->PtSpacingCrossFlight->setText(ds);
	this->PtSpacingInFlight->blockSignals(false);
	this->PtSpacingCrossFlight->blockSignals(false);
	
	if (this->hasStrips) this->DeltaGroup->setEnabled(!this->hasStrips);


	ds=QString("%1").arg(offset.x, 0, 'f', 2);
	this->XOffset->setText(ds);

	ds=QString("%1").arg(offset.y, 0, 'f', 2);
	this->YOffset->setText(ds);
};

void bui_ImportALSDialog::onFileSelected()
{
	CFileName fn("ALS file");
	CReferenceSystem ref;
	
	QStringList files = QFileDialog::getOpenFileNames (this, "Choose ALS File(s)", inifile.getCurrDir(), "ALS data (*.txt *.xyz *.las *.fst)");
	if (files.count() >= 1)
	{
		Bui_ReferenceSysDlg dlg(&ref, &fn, "Import ALS Data", "UserGuide/ImportALS.html", false, 0, false, false);
		dlg.exec();	

		if (dlg.getSuccess())	
		{
			this->refsys = ref;			
			CFileName path = project.getFileName()->GetPath();

			this->InputFiles->setEnabled(false);
			this->InputFiles->blockSignals(true);
			this->InputFiles->setColumnCount(1);
			this->InputFiles->setRowCount(files.count());
			this->InputFiles->setHorizontalHeaderItem(0,new QTableWidgetItem("ALS Files"));
			this->InputFiles->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

			this->inputFilenames.resize(files.count());
		
			for (int i = 0; i < files.count(); ++i)
			{
				this->inputFilenames[i] = files[i].toLatin1().constData();	
				fn = CSystemUtilities::RelPath(this->inputFilenames[i], path);
				QTableWidgetItem *it = new QTableWidgetItem(fn.GetChar());
				this->InputFiles->setItem(i,0,it);
			}

			this->InputFiles->selectAll();

			this->buttonOk->setEnabled(true);
			this->filenameSelected = true;
		}
		else
		{
			QMessageBox::warning( this, "Input file does not exist", " Input file does not exist");
		}
	}	
}

//================================================================================

void bui_ImportALSDialog::onFormatChanged()
{
	this->XYZButton->blockSignals(true);
	this->XYZIButton->blockSignals(true);
	this->XYZITButton->blockSignals(true);
	this->TXYZButton->blockSignals(true);
	this->TXYZIButton->blockSignals(true);
	this->TopoSysButton->blockSignals(true);
	this->LASButton->blockSignals(true);
	this->FPLPButton->blockSignals(true);
	this->FPButton->blockSignals(true);
	this->LPButton->blockSignals(true);
	this->ImportStripsButton->blockSignals(true);
	this->ImportTileButton->blockSignals(true);

	switch(eFormat)
	{
		case eALSXYZ    : { XYZButton->setChecked(false);     break; }
		case eALSXYZI   : { XYZIButton->setChecked(false);    break; }
		case eALSXYZIT  : { XYZITButton->setChecked(false);   break; }
		case eALSTXYZ   : { TXYZButton->setChecked(false);    break; }
		case eALSTXYZI  : { TXYZIButton->setChecked(false);   break; }
		case eALSTopoSys: 
						  { 
							  TopoSysButton->setChecked(false); 
							  this->FPLPButton->setEnabled(true);
							  break; 
						  }
		case eALSLAS    : 
						  { 
							  LASButton->setChecked(false);    
							  this->FPButton->setEnabled(true);
							  this->LPButton->setEnabled(true);
							  this->ImportStripsButton->setEnabled(true);
							  this->ImportTileButton->setEnabled(true);
							  break;
						  } 

		default: break; 
	}

	if (XYZButton->isChecked()) eFormat = eALSXYZ;
	else if (XYZIButton->isChecked()) eFormat = eALSXYZI;
	else if (XYZITButton->isChecked()) eFormat = eALSXYZIT;
	else if (TXYZButton->isChecked()) eFormat = eALSTXYZ;
	else if (TXYZIButton->isChecked()) eFormat = eALSTXYZI;
	else if (TopoSysButton->isChecked()) 
	{
		eFormat = eALSTopoSys;
		if (ePulseMode == eALSFirstLast)
		{
			FPLPButton->setChecked(false); 
			LPButton->setChecked(true); 
			ePulseMode = eALSLast;
		}
		this->FPLPButton->setEnabled(false);
	}
	else
	{
		this->eFormat = eALSLAS;
		this->ePulseMode = eALSFirstLast;
		this->hasStrips = false;
		this->DeltaGroup->setEnabled(true);

		this->FPButton->setEnabled(false);
		this->LPButton->setEnabled(false);
		this->ImportStripsButton->setEnabled(false);
		this->ImportTileButton->setEnabled(false);
		this->ImportStripsButton->setChecked(false);
		this->ImportTileButton->setChecked(true);

		this->FPButton->setChecked(false);
		this->LPButton->setChecked(false);
		this->FPLPButton->setChecked(true);
	}

	this->FPLPButton->blockSignals(false);

	this->XYZButton->blockSignals(false);
	this->XYZIButton->blockSignals(false);
	this->XYZITButton->blockSignals(false);
	this->TXYZButton->blockSignals(false);
	this->TXYZIButton->blockSignals(false);
	this->TopoSysButton->blockSignals(false);
	this->LASButton->blockSignals(false);
	this->FPButton->blockSignals(false);
	this->LPButton->blockSignals(false);
	this->ImportStripsButton->blockSignals(false);
	this->ImportTileButton->blockSignals(false);
}

void bui_ImportALSDialog::onPulseModeChanged()
{
	this->LPButton->blockSignals(true);
	this->FPButton->blockSignals(true);
	this->FPLPButton->blockSignals(true);

	switch(ePulseMode)
	{
		case eALSFirst    : { FPButton->setChecked(false);    break; }
		case eALSLast     : { LPButton->setChecked(false);   break; }
		case eALSFirstLast: { FPLPButton->setChecked(false); break; }
		default: break; 
	}

	if (FPButton->isChecked()) ePulseMode = eALSFirst;
	else if (LPButton->isChecked()) ePulseMode = eALSLast;
	else ePulseMode = eALSFirstLast;

	this->LPButton->blockSignals(false);
	this->FPButton->blockSignals(false);
	this->FPLPButton->blockSignals(false);
}

void bui_ImportALSDialog::onImportModeChanged()
{
	this->ImportStripsButton->blockSignals(true);
	this->ImportTileButton->blockSignals(true);

	this->hasStrips = !this->hasStrips;
	this->ImportStripsButton->setChecked(this->hasStrips);
	this->ImportTileButton->setChecked(!this->hasStrips);
	if (this->hasStrips) this->DeltaGroup->setEnabled(false);
	else this->DeltaGroup->setEnabled(true);
	this->ImportStripsButton->blockSignals(false);
	this->ImportTileButton->blockSignals(false);
}

void bui_ImportALSDialog::onOffsetChanged()
{
	this->XOffset->blockSignals(true);
	this->YOffset->blockSignals(true);

	bool OK;
	double d = this->XOffset->text().toDouble(&OK);
	if (OK) this->offset.x = d;

	d = this->YOffset->text().toDouble(&OK);
	if (OK) this->offset.y = d;

	QString ds=QString("%1").arg(offset.x, 0, 'f', 2);
	this->XOffset->setText(ds);

	ds=QString("%1").arg(offset.y, 0, 'f', 2);
	this->YOffset->setText(ds);

	this->XOffset->blockSignals(false);
	this->YOffset->blockSignals(false);
};

void bui_ImportALSDialog::onDeltaChanged()
{
	this->PtSpacingInFlight->blockSignals(true);
	this->PtSpacingCrossFlight->blockSignals(true);
	bool OK;

	double d = this->PtSpacingInFlight->text().toDouble(&OK);
	if (OK && (d > 0.005)) this->delta.x = d;

	d = this->PtSpacingCrossFlight->text().toDouble(&OK);
	if (OK && (d > 0.005)) this->delta.y = d;

	QString ds=QString("%1").arg(delta.x, 0, 'f', 2);
	this->PtSpacingInFlight->setText(ds);

	ds=QString("%1").arg(delta.y, 0, 'f', 2);
	this->PtSpacingCrossFlight->setText(ds);

	this->PtSpacingInFlight->blockSignals(false);
	this->PtSpacingCrossFlight->blockSignals(false);
};

void bui_ImportALSDialog::OK()
{
	this->success = true;
	
	inifile.setALSoffsetX(this->offset.x);
	inifile.setALSoffsetY(this->offset.y);   
	inifile.setALSdeltaX(this->delta.x);
	inifile.setALSdeltaY(this->delta.y);
	inifile.setALShasStrips(this->hasStrips);
	inifile.setALSFormat((int) this->eFormat);
	inifile.setALSPulseMode((int) this->ePulseMode);
	
	this->destroy();
	this->close();
};

void bui_ImportALSDialog::Cancel()
{
	this->success = false;
	this->destroy();
	this->close();
}
	
void bui_ImportALSDialog::Help()
{
	CHtmlHelp::callHelp("UserGuide/ImportALS.html");
};
