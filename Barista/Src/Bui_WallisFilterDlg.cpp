#include "Bui_WallisFilterDlg.h"

#include "IniFile.h"
#include "HugeImage.h"
#include "histogram.h"
#include "BaristaView.h"
#include "WallisFilter.h"
#include "TransferFunctionFactory.h"
#include "BaristaProjectHelper.h"
#include "Icons.h"
#include "Bui_ProgressDlg.h"
#include "ProgressThread.h"


Bui_WallisFilterDlg::Bui_WallisFilterDlg(CBaristaView* view,bool showButtons,QWidget* parent) :
	QDialog(parent), backUpFunction(0), himage(view->getHugeImage()),wallisFilter(0),
	success(false),contrast(DEFAULT_CONTRAST),brightness(DEFAULT_BRIGHTNESS),mean(DEFAULT_MEAN_8BIT),
	stdDev(DEFAULT_STDDEV_8BIT),meanAndStdDevInitialized(false),windowSizeX(DEFAULT_WINDOWSIZE),
	windowSizeY(DEFAULT_WINDOWSIZE),instantPreView(true),view(view)
{
	
	if (!this->himage || !this->himage->getTransferFunction() || this->himage->getBpp() > 16)
		return;

	this->offsetX = 0;
	this->offsetY = 0;
	this->width = this->himage->getWidth();
	this->height = this->himage->getHeight();

	if (this->view->getHasROI())
	{
		this->offsetX = this->view->getROIUL().x;
		this->offsetY = this->view->getROIUL().y;
		this->width =  this->view->getROILR().x - this->view->getROIUL().x;
		this->height = this->view->getROILR().y -this->view->getROIUL().y;
	}

	if( this->offsetX < 0 || this->offsetY < 0 || 
	   (this->offsetX + this->width ) > this->himage->getWidth() ||
	   (this->offsetY + this->height) > this->himage->getHeight() || this->width < 10 ||
		this->height < 10)
		return;

	// make a backup of the current transfer function
	this->backUpFunction = CTransferFunctionFactory::create(himage->getTransferFunction());

	
	setupUi(this);

	// load and set window icon
	QPixmap icon;
	icon.loadFromData(Barista, sizeof(Barista), "PNG");
	this->setWindowIcon(QIcon(icon));

	this->connect(this->pB_Ok,SIGNAL(released()),this,SLOT(pBOKReleased()));
	this->connect(this->pB_Cancel,SIGNAL(released()),this,SLOT(pBCancelReleased()));
	this->connect(this->pB_HelpButton,SIGNAL(released()),this,SLOT(pBHelpReleased()));
	this->connect(this->pB_Update,SIGNAL(released()),this,SLOT(pBUpdateReleased()));

	this->connect(this->hS_MeanValue,SIGNAL(valueChanged(int)),this->sB_MeanValue,SLOT(setValue(int)));
	this->connect(this->hS_StdDev,SIGNAL(valueChanged(int)),this->sB_StdDev,SLOT(setValue(int)));
	this->connect(this->hS_Contrast,SIGNAL(valueChanged(int)),this->sB_Contrast,SLOT(setValue(int)));
	this->connect(this->hS_Brightness,SIGNAL(valueChanged(int)),this->sB_Brightness,SLOT(setValue(int)));

	this->connect(this->sB_MeanValue,SIGNAL(valueChanged(int)),this->hS_MeanValue,SLOT(setValue(int)));
	this->connect(this->sB_StdDev,SIGNAL(valueChanged(int)),this->hS_StdDev,SLOT(setValue(int)));
	this->connect(this->sB_Contrast,SIGNAL(valueChanged(int)),this->hS_Contrast,SLOT(setValue(int)));
	this->connect(this->sB_Brightness,SIGNAL(valueChanged(int)),this->hS_Brightness,SLOT(setValue(int)));

	if (this->instantPreView)
		this->cB_PreView->setChecked(true);
	else
		this->cB_PreView->setChecked(false);

	this->connect(this->cB_PreView,SIGNAL(released()),this,SLOT(cBPreviewReleased()));

	if (this->himage->getTransferFunction()->getTransferFunctionType() == CTransferFunction::eWallisFilter)
	{
		this->wallisFilter = (CWallisFilter*) this->himage->getTransferFunction();
		
		if (this->wallisFilter->isInitialized())
		{
			this->windowSizeX = this->wallisFilter->getWindowSizeX();
			this->windowSizeY = this->wallisFilter->getWindowSizeY();
			this->mean = this->wallisFilter->getMeanValue();
			this->stdDev = this->wallisFilter->getStdDevValue();
			this->contrast = this->wallisFilter->getContrastParameter();
			this->brightness = this->wallisFilter->getBrightnessParameter();
			this->nParametersX = this->wallisFilter->getNParametersX();
			this->nParametersY = this->wallisFilter->getNParametersY();
		}

	}
	else 
	{
		this->mean =  (this->himage->getHistogram()->getMax() - this->himage->getHistogram()->getMin())/2.0f;
	}

	if (this->himage->getBpp() == 8)
	{
		this->sB_MeanValue->setMaximum(255);
		this->hS_MeanValue->setMaximum(255);

		this->sB_StdDev->setMaximum(255);
		this->hS_StdDev->setMaximum(255);

		this->lB_MeanValueMax->setText("255");
		this->lB_StdDevMax->setText("255");


	}
	else 
	{
		this->sB_MeanValue->setMaximum(65535);
		this->hS_MeanValue->setMaximum(65535);

		this->sB_StdDev->setMaximum(65535);
		this->hS_StdDev->setMaximum(65535);

		this->lB_MeanValueMax->setText("65535");
		this->lB_StdDevMax->setText("65535");
	}


	this->sB_MeanValue->setValue(this->mean);
	this->sB_StdDev->setValue(this->stdDev);
	this->sB_Contrast->setValue(int(this->contrast * 100.0));
	this->sB_Brightness->setValue(int(this->brightness * 100.0));
	this->sB_WindowSizeX->setValue(this->windowSizeX);
	this->sB_WindowSizeY->setValue(this->windowSizeY);


	this->connect(this->sB_MeanValue,SIGNAL(valueChanged(int)),this,SLOT(sBMeanValueChanged(int)));
	this->connect(this->sB_StdDev,SIGNAL(valueChanged(int)),this,SLOT(sBStdDevValueChanged(int)));
	this->connect(this->sB_Contrast,SIGNAL(valueChanged(int)),this,SLOT(sBContrastValueChanged(int)));
	this->connect(this->sB_Brightness,SIGNAL(valueChanged(int)),this,SLOT(sBBrightnessValueChanged(int)));

	this->connect(this->sB_WindowSizeX,SIGNAL(valueChanged(int)),this,SLOT(sBWindowSizeXValueChanged(int)));
	this->connect(this->sB_WindowSizeY,SIGNAL(valueChanged(int)),this,SLOT(sBWindowSizeYValueChanged(int)));

	if (!showButtons)
	{
		this->pB_Ok->setVisible(false);
		this->pB_Cancel->setVisible(false);
		this->pB_HelpButton->setVisible(false);
		this->layout()->setContentsMargins(0,0,0,0);
	}

	this->checkSetup();
	
}



Bui_WallisFilterDlg::~Bui_WallisFilterDlg(void)
{
	if (this->backUpFunction)
		delete this->backUpFunction;
	this->backUpFunction = NULL;
}


void Bui_WallisFilterDlg::pBOKReleased()
{
	this->success = true;
	this->okPressed();
	this->close();
}

void Bui_WallisFilterDlg::okPressed()
{
	this->pBUpdateReleased();
	this->wallisFilter->getMeanVector().clear();
	this->wallisFilter->getStdDevVector().clear();
	this->wallisFilter->writeWallisFilterFile();
}

void Bui_WallisFilterDlg::pBCancelReleased()
{
	this->success = false;
	
	if (this->himage && this->backUpFunction)
	{
		// the huge image will delete the memory allocated for this->wallisFilter,
		// so no need to do that here
		this->himage->setTransferFunction(this->backUpFunction,true,true);
	}

	
	this->close();
}


void Bui_WallisFilterDlg::pBHelpReleased()
{

}


void Bui_WallisFilterDlg::pBUpdateReleased()
{
	bool ret = true;
	
	CBaristaProjectHelper helper;
	helper.setIsBusy(true);

	if (!this->wallisFilter)
	{
		this->wallisFilter = (CWallisFilter*)CTransferFunctionFactory::createWallisFilterFunction(this->himage->getFileNameChar(),himage->getBpp(),himage->getHistogram()->getMin(),himage->getHistogram()->getMax());
		himage->setTransferFunction(this->wallisFilter,false,true);
	}

	if (!this->meanAndStdDevInitialized)
	{
			if (!this->wallisFilter->hasIdenticalParameters(this->windowSizeX,
														   this->windowSizeY,
														   this->contrast,
														   this->brightness,
														   this->mean,
														   this->stdDev,
														   this->offsetX,
														   this->offsetY,
														   this->width,
														   this->height))
			{
				CCharString title("Computing parameters  ... ");
				bui_ProgressDlg dlg(new CMeanAndStdDevThread(this->himage,	this->wallisFilter->getMeanVector(),
																			this->wallisFilter->getStdDevVector(),
																			this->nParametersX,
																			this->nParametersY,
																			this->windowSizeX,
																			this->windowSizeY,
																			this->offsetY,
																			this->height,
																			this->offsetX,
																			this->width),
															 this->himage,title.GetChar());

				dlg.exec();
				
				this->meanAndStdDevInitialized = dlg.getSuccess();
			}
			else
				this->meanAndStdDevInitialized = true;
	}									


	this->checkSetup();
		
	helper.setIsBusy(false);	
	
	if (this->meanAndStdDevInitialized)
	{
		this->updateWallisFilter();
	}
}



void Bui_WallisFilterDlg::sBMeanValueChanged(int value)
{
	this->mean = value;

	if (this->instantPreView && this->meanAndStdDevInitialized)
		this->updateWallisFilter();
}


void Bui_WallisFilterDlg::sBStdDevValueChanged(int value)
{
	this->stdDev = value;
	
	if (this->instantPreView && this->meanAndStdDevInitialized)
		this->updateWallisFilter();
}


void Bui_WallisFilterDlg::sBContrastValueChanged(int value)
{
	this->contrast = double(value) / 100.0;

	if (this->instantPreView && this->meanAndStdDevInitialized)
		this->updateWallisFilter();
}


void Bui_WallisFilterDlg::sBBrightnessValueChanged(int value)
{
	this->brightness = double(value) / 100.0;

	if (this->instantPreView && this->meanAndStdDevInitialized)
		this->updateWallisFilter();
}

void Bui_WallisFilterDlg::sBWindowSizeXValueChanged(int value)
{
	this->meanAndStdDevInitialized = false;
	this->windowSizeX = value;

	this->checkSetup();
}

void Bui_WallisFilterDlg::sBWindowSizeYValueChanged(int value)
{
	this->meanAndStdDevInitialized = false;
	this->windowSizeY = value;

	this->checkSetup();
}

void Bui_WallisFilterDlg::checkSetup()
{
	if (this->meanAndStdDevInitialized)
	{
		this->cB_PreView->setEnabled(true);
	}
	else 
	{
		this->cB_PreView->setEnabled(false);
	}

}

void Bui_WallisFilterDlg::cBPreviewReleased()
{
	this->instantPreView = !this->instantPreView;
	this->checkSetup();
}

void Bui_WallisFilterDlg::updateWallisFilter()
{
	if (this->wallisFilter)
	{
	
		CBaristaProjectHelper helper;
		helper.setIsBusy(true);
		this->wallisFilter->setParameters(	this->windowSizeX,
											this->windowSizeY,
											this->nParametersX,
											this->nParametersY,
											this->contrast,
											this->brightness,
											this->mean,
											this->stdDev,
											this->offsetX,
											this->offsetY,
											this->width,
											this->height);
		if (this->view)
		{
			this->view->clearImages();
			this->view->update();
		}
		helper.setIsBusy(false);
	}
	
}