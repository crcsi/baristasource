#include "BaristaTreeWidget.h"
#include <QContextMenuEvent>
#include <QTreeWidgetItem>
#include "BaristaPopUpHandler.h"
#include "BaristaIconViewItem.h"

CBaristaTreeWidget::CBaristaTreeWidget(QWidget* parent) : QTreeWidget(parent)
{
}

CBaristaTreeWidget::CBaristaTreeWidget() 
{
}


CBaristaTreeWidget::~CBaristaTreeWidget(void)
{
}

void CBaristaTreeWidget::contextMenuEvent(QContextMenuEvent * e)
{	
	QPoint p = e->pos();
	QModelIndex i = this->indexAt(p);
	if (i.isValid())
	{
		QTreeWidgetItem* item = this->itemFromIndex(i);
		if( ((CBaristaIconViewItem*)item)->getObject())
			CBaristaPopUpHandler popupmenu(item);
	}
}
void  CBaristaTreeWidget::itemClickedSlot(QTreeWidgetItem * item,int column)
{
	if (item->type() == eImage)
	{
		QModelIndex index = this->indexFromItem(item);
		QModelIndex below = this->indexBelow(index);
		QTreeWidgetItem* belowItem = this->itemFromIndex(below);
		this->setItemSelected(belowItem,true);
			
	}
	else if (item->type() == eImageCaption)
	{
		QModelIndex index = this->indexFromItem(item);
		QModelIndex above = this->indexAbove(index);
		QTreeWidgetItem* aboveItem = this->itemFromIndex(above);
		this->setItemSelected(aboveItem,true);
	}
	
}

void  CBaristaTreeWidget::itemDoubleClickedSlot(QTreeWidgetItem * item,int column)
{
	if (item->type() == eImage || item->type() == eImageCaption)
	{
		bool b = this->isItemExpanded(item);
		this->setItemExpanded(item,true);
		b = this->isItemExpanded(item);
		int i = 9;
			
	}

}