#include "OpenGLObject.h"
#include <QtOpenGL>
#include <string.h>
#include <gl\GLU.h>

#include "BaristaProject.h"
#include "VDEM.h"
#include "ALSPoint.h"
#include "BaristaProjectHelper.h"

#include "LookupTableHelper.h"

COpenGLObject::COpenGLObject(QWidget * parent ) : QGLWidget(parent), rot(0,0,0),pos(0,0,0),
	FOV(45.0),aspect(1.0), xyzListcount(0), onButton(false),scale(1.0), nPaints(0),points(512*512),alspoints(0),
	capacity(0), lut(0,0)
{
	this->resetView();

	this->min.x = 99999999999999999.0;
	this->min.y = 99999999999999999.0;
	this->min.z = 99999999999999999.0;

	this->max.x = -99999999999999999.0;
	this->max.y = -99999999999999999.0;
	this->max.z = -99999999999999999.0;


	this->bRotationChanged = false;
	this->bDrawLabels = true;

	this->resizeGL(this->width(), this->height());

	this->dlg = new QDialog();
	QGridLayout* gl = new QGridLayout(this->dlg);
	this->textBrowser = new QTextEdit(this->dlg);
	gl->addWidget(this->textBrowser);
	this->dlg->setLayout(gl);

	this->dlg->resize(600, 80);
	this->dlg->move(100, 100);
	this->dlg->show();

}

COpenGLObject::~COpenGLObject(void)
{
    this->makeCurrent();

	for ( unsigned int i = 0; i < this->xyzLists.size(); i++)
		glDeleteLists(this->xyzLists[i], 1);

	glDeleteLists(this->geoList, 1);
	glDeleteLists(this->monoList, 1);
	glDeleteLists(this->axesList, 1);

	if (!this->alspoints)
	{
		for (int i=0; i < this->capacity; i++)
			delete this->alspoints[i];

		delete [] this->alspoints;
		this->alspoints = 0;
		this->capacity =0;
	}

}

void COpenGLObject::initializeGL()
{
	this->qglClearColor(Qt::black);
	glClearDepth(1.0f);
    glShadeModel(GL_FLAT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	this->makeObjects();
}


void COpenGLObject::paintGL()
{
	::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	this->resizeGL(this->width(), this->height());

	// now we go to perspective projection for 3d stuff
	::glMatrixMode(GL_PROJECTION);
	::glLoadIdentity();

	this->zNearClip = this->zEye / 1000.;
	this->zFarClip = this->zEye * 100.;
	::gluPerspective(this->FOV, this->aspect, this->zNearClip, this->zFarClip);
	// switch back to the modelview matrix and clear it
	::glMatrixMode(GL_MODELVIEW);
	::glLoadIdentity();

	this->setupViewingTransform();

	glTranslated(this->pos.x, this->pos.y, this->pos.z);

	baristaProjectHelper.setIsBusy(true);

	this->nElements = 0;
	this->nTiles = 0;

	this->sum0 = 0;
	this->sum1 = 0;
	this->sum2 = 0;
	this->sum3 = 0;
	this->sum4 = 0;
	this->sum5 = 0;



	// draw XYZ points
	for (unsigned int i = 0; i < this->xyzLists.size(); i++)
	{
		if ( project.getPointArrayAt(i)->getDisplaySettings()->getShowEntities3D())
			glCallList(this->xyzLists[i]);
	}


	QTime qtotal;
	qtotal.start();

	// draw DEMs
	for (unsigned int i = 0; i < (unsigned int)project.getNumberOfDEMs(); i++)
	{
		CVDEM* vdem = project.getDEMAt(i);

		if ( vdem->getDisplaySettings()->getShowEntities3D())
		{
			unsigned int ncols = this->demList[i].tilesacross;
			unsigned int nrows = this->demList[i].tilesdown;

			C2DPoint dxy(this->demList[i].pointdist);

			for ( unsigned int row = 0; row < nrows; row++)
			{
				for ( unsigned int col = 0; col < ncols; col++)
				{
					QTime qlevelcheck;
					qlevelcheck.start();
					unsigned int tileindex = row*ncols + col;
					
					C3DPoint ul = this->demList[i].tiles[tileindex].ul;
					C3DPoint ll = this->demList[i].tiles[tileindex].ll;
					C3DPoint ur = this->demList[i].tiles[tileindex].ur;
					C3DPoint lr = this->demList[i].tiles[tileindex].lr;
					C3DPoint tmiddle(this->demList[i].tiles[tileindex].pmid);

					int level = this->demList[i].tiles[tileindex].level;
					int oldlevel = level;
					int lowlevel = this->demList[i].tiles[tileindex].lowLevel;

					// needs display
					bool showTile = this->checkTileLevel(ul, ll, lr, ur, tmiddle, dxy, level, lowlevel, 50.0);

					this->sum4 += qlevelcheck.elapsed();

					if ( !showTile)
					{
						if ( this->demList.at(i).tiles[tileindex].hasList )
						{
							glDeleteLists(this->demList.at(i).tiles[tileindex].glList,1);
							this->demList.at(i).tiles[tileindex].hasList = false;
							this->demList[i].tiles[tileindex].level = this->demList[i].tiles[tileindex].lowLevel;
						}
					}
					else
					{
						this->nTiles++;
						
						bool levelchange = false;
						if ( oldlevel != level) levelchange = true;

						bool hasList = this->demList.at(i).tiles[tileindex].hasList;

						if ( !hasList )
						{
							this->demList.at(i).tiles[tileindex].glList = glGenLists(1);
							glNewList(this->demList.at(i).tiles[tileindex].glList, GL_COMPILE);

							this->demList[i].tiles[tileindex].level = level;
							this->demList[i].tiles[tileindex].elementcount = this->refreshDEMTile(i,row,col,this->demList[i].tiles[tileindex].glList,level);

							this->demList.at(i).tiles[tileindex].hasList = true;
						}
						
						else if ( levelchange )
						{
							glDeleteLists(this->demList.at(i).tiles[tileindex].glList,1);
							
							this->demList.at(i).tiles[tileindex].hasList = false;

							this->demList.at(i).tiles[tileindex].glList = glGenLists(1);
							glNewList(this->demList.at(i).tiles[tileindex].glList, GL_COMPILE);

							this->demList[i].tiles[tileindex].level = level;
							this->demList[i].tiles[tileindex].elementcount = this->refreshDEMTile(i,row,col,this->demList[i].tiles[tileindex].glList,level);

							this->demList.at(i).tiles[tileindex].hasList = true;

						}
						
						this->nElements += this->demList[i].tiles[tileindex].elementcount;
						
						glCallList(this->demList[i].tiles[tileindex].glList);
		
					}	
				}
			}
		}
	}


	// draw ALS
	for (unsigned int i = 0; i < (unsigned int)project.getNumberOfAlsDataSets(); i++)
	{
		CVALS* vals = project.getALSAt(i);

		if ( vals->getDisplaySettings()->getShowEntities3D())
		{
			unsigned int ncols = this->alsList[i].tilesacross;
			unsigned int nrows = this->alsList[i].tilesdown;

			C2DPoint dxy(this->alsList[i].pointdist);

			for ( unsigned int row = 0; row < nrows; row++)
			{
				for ( unsigned int col = 0; col < ncols; col++)
				{
					QTime qlevelcheck;
					qlevelcheck.start();
					unsigned int tileindex = row*ncols + col;
					
					C3DPoint ul = this->alsList[i].tiles[tileindex].ul;
					C3DPoint ll = this->alsList[i].tiles[tileindex].ll;
					C3DPoint ur = this->alsList[i].tiles[tileindex].ur;
					C3DPoint lr = this->alsList[i].tiles[tileindex].lr;
					C3DPoint tmiddle(this->alsList[i].tiles[tileindex].pmid);

					int level = this->alsList[i].tiles[tileindex].level;
					int oldlevel = level;
					int lowlevel = this->alsList[i].tiles[tileindex].lowLevel;

					// needs display
					bool showTile = this->checkTileLevel(ul, ll, lr, ur, tmiddle, dxy, level, lowlevel, 300.0);

					this->sum4 += qlevelcheck.elapsed();

					if ( !showTile)
					{
						if ( this->alsList.at(i).tiles[tileindex].hasList )
						{
							glDeleteLists(this->alsList.at(i).tiles[tileindex].glList,1);
							this->alsList.at(i).tiles[tileindex].hasList = false;
							this->alsList[i].tiles[tileindex].level = this->alsList[i].tiles[tileindex].lowLevel;
						}
					}
					else
					{	
						bool levelchange = false;
						if ( oldlevel != level) levelchange = true;

						bool hasList = this->alsList.at(i).tiles[tileindex].hasList;

						if ( !hasList )
						{
							this->alsList.at(i).tiles[tileindex].glList = glGenLists(1);
							glNewList(this->alsList.at(i).tiles[tileindex].glList, GL_COMPILE);

							this->alsList[i].tiles[tileindex].level = level;
							this->alsList[i].tiles[tileindex].elementcount = this->refreshALSTile(i,row,col,this->alsList[i].tiles[tileindex].glList,level);

							this->alsList.at(i).tiles[tileindex].hasList = true;
						}
						
						else if ( levelchange )
						{
							glDeleteLists(this->alsList.at(i).tiles[tileindex].glList,1);
							
							this->alsList.at(i).tiles[tileindex].hasList = false;

							this->alsList.at(i).tiles[tileindex].glList = glGenLists(1);
							glNewList(this->alsList.at(i).tiles[tileindex].glList, GL_COMPILE);

							this->alsList[i].tiles[tileindex].level = level;
							this->alsList[i].tiles[tileindex].elementcount = this->refreshALSTile(i,row,col,this->alsList[i].tiles[tileindex].glList,level);

							this->alsList.at(i).tiles[tileindex].hasList = true;

						}
						
						this->nElements += this->alsList[i].tiles[tileindex].elementcount;
						
						glCallList(this->alsList[i].tiles[tileindex].glList);
						this->nTiles++;
					}	
				}
			}
		}
	}



	// draw monoplotted points
	if ( project.getMonoplottedPoints()->getDisplaySettings()->getShowEntities3D())
		glCallList(this->monoList);

	// draw monoplotted lines
	if ( project.getXYZPolyLines()->getDisplaySettings()->getShowEntities3D())
		glCallList(this->lineList);

#ifdef WIN32
	// draw monoplotted buildings
	if ( project.getBuildings()->getDisplaySettings()->getShowEntities3D())
		glCallList(this->buildingList);
#endif

	// draw georeferenced points
	if ( project.getGeoreferencedPoints()->getDisplaySettings()->getShowEntities3D())
		glCallList(this->geoList);

	// draw axes
	glCallList(this->axesList);

	//draw labels, no drawing while mouse button is down
	if ( !this->onButton )
		this->drawLabels();

	this->sum0 = qtotal.elapsed();

//	this->sum3 = this->sum0 - this->sum1 - this->sum2 - this->sum4 - this->sum5;


	QString text("Paints: ");
	text.append(QString("%1").arg(this->nPaints));
	text.append(QString(", NTiles: %1").arg(this->nTiles));

	text.append(QString(", FOV: %1").arg(this->FOV));
	text.append(QString(", leaningleft: %1").arg(this->testvalue1));
	text.append(QString(", leanx: %1").arg(this->testvalue2));
	text.append(QString(", leany: %1").arg(this->testvalue3));

	//text.append(QString(", NElements: %1").arg(this->nElements));
	//text.append(QString(", total: %1").arg(this->sum0));
	//text.append(QString(", levelcheck: %1").arg(this->sum4));
	//text.append(QString(", load: %1").arg(this->sum1));
	//text.append(QString(", compile %1").arg(this->sum2));
	//text.append(QString(", finish: %1").arg(this->sum3));
	//text.append(QString(", diff: %1").arg(this->sum3));

	this->textBrowser->append(text);

	this->nPaints++;

	this->dlg->raise();


	baristaProjectHelper.setIsBusy(false);


}


void COpenGLObject::resizeGL(int width, int height)
{
    int side = qMax(width, height);
	if (width && height)
		this->aspect = width/height;
	else
		this->aspect = 1.0;
	
	glViewport((width - side) / 2, (height - side) / 2, side, side);
}

void COpenGLObject::mousePressEvent(QMouseEvent *event)
{
	this->lastPos = event->pos();
}

void  COpenGLObject::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->buttons()) 
		this->onButton = true;
	else
		 this->onButton = false;

	this->updateGL();
}


void COpenGLObject::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - this->lastPos.x();
    int dy = event->y() - this->lastPos.y();
	
	
	if (event->buttons()) 
		this->onButton = true;
	else
		 this->onButton = false;


    if (event->buttons() & Qt::LeftButton) 
	{
		this->bRotationChanged = true;
        this->setXRotation(this->rot.x + 8 * dy*(this->FOV/45));
        this->setYRotation(this->rot.y + 8 * dx*(this->FOV/45));
    } 
	else if (event->buttons() & Qt::RightButton)
	{
		this->bRotationChanged = true;
        this->setXRotation(this->rot.x + 8 * dy*(this->FOV/45));
        this->setZRotation(this->rot.z + 8 * dx*(this->FOV/45));
    }
	else if (event->buttons() & Qt::MidButton)
	{
		GLfloat tmpRotMat[16];
		glGetFloatv(GL_MODELVIEW_MATRIX, tmpRotMat);

		Matrix M(4, 4);

		for (int r = 1; r <=4; r++)
		{
			for (int c = 1; c <= 4; c++)
			{
				int index = (r-1)*4+c-1;
                M(r, c) = tmpRotMat[index];
			}
		}

		Matrix S(4, 1);
		//S(1, 1) = +.01*dx;
		//S(2, 1) = -.01*dy;
		// consider FOV on rotation
		S(1, 1) = +.01*dx*(this->FOV/45);
		S(2, 1) = -.01*dy*(this->FOV/45);
		S(3, 1) = 0;
		S(4, 1) = 0;

		Matrix Sp = M*S;

		this->pos.x += Sp(1, 1);
		this->pos.y += Sp(2, 1);
		this->pos.z += Sp(3, 1);

		this->updateGL();
	}

    this->lastPos = event->pos();
}


void COpenGLObject::wheelEvent ( QWheelEvent * e )
{
	int nClicks = e->delta() / 120;

	if (nClicks > 0)
		this->FOV *= 1.3;
	else
		this->FOV /= 1.3;

/*	
	if (nClicks > 0)
		this->zEye *= 1.3;
	else
		this->zEye /= 1.3;
*/
	this->bRotationChanged = true;

	this->updateGL();
	this->drawLabels();
}


void COpenGLObject::keyReleaseEvent ( QKeyEvent * e )
{
	if ( e->key() == Qt::Key_R)
	{
		this->resetView();
	}
	// change pos
	else if ( e->key() == Qt::Key_Up )
	{
		this->pos.y -= 0.05*this->FOV/45;
	}
	else if ( e->key() == Qt::Key_Down)
	{
		this->pos.y += 0.05*this->FOV/45;
	}
	if ( e->key() == Qt::Key_Right )
	{
		this->pos.x -= 0.05*this->FOV/45;
	}
	else if ( e->key() == Qt::Key_Left)
	{
		this->pos.x += 0.05*this->FOV/45;
	}
	
	this->updateGL();
}


void COpenGLObject::setXRotation(double angle)
{
    normalizeAngle(&angle);
    if (angle != this->rot.x) 
	{
         this->rot.x = angle;
         this->updateGL();
    }
}

void COpenGLObject::setYRotation(double angle)
{
    normalizeAngle(&angle);
    if (angle != this->rot.y) 
	{
        this->rot.y = angle;
        this->updateGL();
    }
}

void COpenGLObject::setZRotation(double angle)
{
    normalizeAngle(&angle);
    if (angle != this->rot.z) 
	{
        this->rot.z = angle;
        this->updateGL();
    }
}

void COpenGLObject::normalizeAngle(double *angle)
{
    while (*angle < 0)
        *angle += 360 * 16;
    while (*angle > 360 * 16)
        *angle -= 360 * 16;
}






void COpenGLObject::makePoints(CXYZPointArray* pointArray, QColor qcolor)
{
	GLdouble x1;
	GLdouble y1;
	GLdouble z1;

	this->qglColor(qcolor);
	
	glPointSize(this->pointsize);
	for (int i = 0; i < pointArray->GetSize(); i++)
	{
		CObsPoint* Point = pointArray->GetAt(i);
		this->toModel(*Point, x1, y1, z1);
		glBegin(GL_POINTS);
		glVertex3d(x1, y1, z1);
		glEnd();
	}
	
}


void COpenGLObject::setupViewingTransform()
{
	::glTranslated(0, 0, -this->zEye);

	if (this->bRotationChanged)
	{
  	    glMultMatrixf(this->RotMat);

		glRotatef((GLfloat)(.5*this->rot.x), this->X_SCREEN[0], this->X_SCREEN[1], this->X_SCREEN[2]);
		glRotatef((GLfloat)(.5*this->rot.y), this->Y_SCREEN[0], this->Y_SCREEN[1], this->Y_SCREEN[2]);
		glRotatef((GLfloat)(.5*this->rot.z), this->Z_SCREEN[0], this->Z_SCREEN[1], this->Z_SCREEN[2]);

		this->rot.x = 0;
		this->rot.y = 0;
		this->rot.z = 0;

		glGetFloatv(GL_MODELVIEW_MATRIX, this->RotMat);

		/*----------------------------------------------------------------------
		| ... and force it to be a rotation (i.e. get rid of translation above).
		| Unfortunately, this is very specific.
		*---------------------------------------*/
		this->RotMat[14] = 0.0;

		/*--------------------------------------------------------------------
		| The transposed (i.e. inverse) of the rotation matrix applied to the
		| canonical axes ({1, 0, 0}, {0, 1, 0}) of the object frame of refer-
		| ence give the new coordinates, in this frame, of the screen axes, so
		| we are ready for the next turn...
		*-----------------------------------*/

		this->X_SCREEN[0] = this->RotMat[0];
		this->X_SCREEN[1] = this->RotMat[4];
		this->X_SCREEN[2] = this->RotMat[8];

		this->Y_SCREEN[0] = this->RotMat[1];
		this->Y_SCREEN[1] = this->RotMat[5];
		this->Y_SCREEN[2] = this->RotMat[9];

		this->Z_SCREEN[0] = this->RotMat[2];
		this->Z_SCREEN[1] = this->RotMat[6];
		this->Z_SCREEN[2] = this->RotMat[10];

		this->bRotationChanged = false;
	}
	else
	{
		glMultMatrixf(this->RotMat);
		glRotatef((GLfloat)(.5*this->rot.x), X_SCREEN[0], X_SCREEN[1], X_SCREEN[2]);
		glRotatef((GLfloat)(.5*this->rot.y), Y_SCREEN[0], Y_SCREEN[1], Y_SCREEN[2]);
		glRotatef((GLfloat)(.5*this->rot.z), Z_SCREEN[0], Z_SCREEN[1], Z_SCREEN[2]);

		this->rot.x = 0;
		this->rot.y = 0;
		this->rot.z = 0;
	}
}

bool COpenGLObject::drawLabels()
{
	QColor qcolor(Qt::blue);

	for ( int i = 0; i < project.getNumberOfXYZPointArrays(); i++)
	{
		CXYZPointArray* xyz = project.getPointArrayAt(i);

		if ( i == 1 ) qcolor = Qt::yellow;
		if ( i == 2 ) qcolor = Qt::green;

		this->makePointLabels(xyz, qcolor);
	}

	if ( project.getNumberOfMonoplottedPoints() > 0 )
	{
		CXYZPointArray* xyz = project.getMonoplottedPoints();
		qcolor = Qt::magenta;

		this->makePointLabels(xyz, qcolor);
	}

	if ( project.getNumberOfGeoreferencedPoints() > 0 )
	{
		CXYZPointArray* xyz = project.getGeoreferencedPoints();
		qcolor = Qt::cyan;

		this->makePointLabels(xyz, qcolor );
	}

	return true;

}

void COpenGLObject::resetView()
{
	this->zEye	= 1.5f;
    this->zNearClip = this->zEye / 1000.;
    this->zFarClip = this->zEye * 100.;
	this->FOV = 45.0;

	this->X_SCREEN[0] = 1.0f;
	this->X_SCREEN[1] = 0.0f;
	this->X_SCREEN[2] = 0.0f;

	this->Y_SCREEN[0] = 0.0f;
	this->Y_SCREEN[1] = 1.0f;
	this->Y_SCREEN[2] = 0.0f;

	this->Z_SCREEN[0] = 0.0f;
	this->Z_SCREEN[1] = 0.0f;
	this->Z_SCREEN[2] = 1.0f;

	this->RotMat[0] = 1;
	this->RotMat[1] = 0;
	this->RotMat[2] = 0;
	this->RotMat[3] = 0;

	this->RotMat[4] = 0;
	this->RotMat[5] = 1;
	this->RotMat[6] = 0;
	this->RotMat[7] = 0;

	this->RotMat[8] = 0;
	this->RotMat[9] = 0;
	this->RotMat[10] = 1;
	this->RotMat[11] = 0;

	this->RotMat[12] = 0;
	this->RotMat[13] = 0;
	this->RotMat[14] = 0;
	this->RotMat[15] = 1;

	this->bRotationChanged = true;

	this->pos.x = 0;
	this->pos.y = 0;
	this->pos.z = 0;

	this->rot.x = 0;
	this->rot.y = 0;
	this->rot.z = 0;

	this->pointsize = 2.0;
}

void COpenGLObject::makeObjects()
{
	if (!this->getExtends()) return;

	QColor qcolor(Qt::red);

	for ( int i = 0; i < project.getNumberOfXYZPointArrays(); i++)
	{
		CXYZPointArray* xyz = project.getPointArrayAt(i);
		this->xyzLists.push_back(glGenLists(this->xyzListcount+1));
		glNewList(this->xyzLists.at(this->xyzListcount), GL_COMPILE);

		if ( i == 1 ) qcolor = Qt::yellow;
		if ( i == 2 ) qcolor = Qt::green;
		this->makePoints(xyz, qcolor);
		
		glEndList();
		this->xyzListcount++;
	}
	
	if ( project.getNumberOfMonoplottedPoints() > 0 )
	{
		CXYZPointArray* xyz = project.getMonoplottedPoints();
		this->monoList = glGenLists(1);
		glNewList(this->monoList, GL_COMPILE);

		qcolor = Qt::magenta;

		this->makePoints(xyz, qcolor);
		glEndList();
	}

	if ( project.getNumberOfGeoreferencedPoints() > 0 )
	{
		CXYZPointArray* xyz = project.getGeoreferencedPoints();
		this->geoList = glGenLists(1);
		glNewList(this->geoList, GL_COMPILE);

		qcolor = Qt::cyan;
		this->makePoints(xyz, qcolor );
		glEndList();
	}


	for ( int i = 0; i < project.getNumberOfDEMs(); i++)
	{
		CVDEM* vdem = project.getDEMAt(i);

		if ( i == 0 ) qcolor = Qt::lightGray;
		if ( i == 1 ) qcolor = Qt::darkRed;
		if ( i == 2 ) qcolor = Qt::darkGray;

		this->makeDEM(vdem, qcolor);
	}

	int nEchos = -1;

	for ( int i = 0; i < project.getNumberOfAlsDataSets(); i++)
	{
		CVALS* vals = project.getALSAt(i);

		if ( i == 0 ) qcolor = Qt::lightGray;
		if ( i == 1 ) qcolor = Qt::darkRed;
		if ( i == 2 ) qcolor = Qt::darkGray;

		if (vals->getCALSData()->getMaxEchos() > nEchos)
			nEchos = vals->getCALSData()->getMaxEchos();

		this->makeALS(vals, qcolor);
	}

	if (nEchos > 0 )
	{
		this->alspoints = new CALSPoint *[512*512];
		this->capacity = 512*512;

		for (int i = 0; i < capacity; ++i)
		{
			this->alspoints[i] = new CALSPoint(nEchos);
		}
	}

	if ( project.getNumberOfMonoplottedLines() > 0 )
	{
		CXYZPolyLineArray* lines = project.getXYZPolyLines();
		this->lineList = glGenLists(1);
		glNewList(this->lineList, GL_COMPILE);

		qcolor = QColor(project.getMonoplottedLineColor().x,project.getMonoplottedLineColor().y,project.getMonoplottedLineColor().z); 
		this->makeLines(lines, qcolor );
		glEndList();
	}


#ifdef WIN32
	if ( project.getNumberOfMonoplottedBuildings() > 0 )
	{
		CBuildingArray* buildings = project.getBuildings();
		this->buildingList = glGenLists(1);
		glNewList(this->buildingList, GL_COMPILE);

		qcolor = QColor(project.getBuildingLineColor().x,project.getBuildingLineColor().y,project.getBuildingLineColor().z);
		this->makeBuildings(buildings, qcolor );
		glEndList();
	}
#endif

	this->axesList = glGenLists(1);
	glNewList(this->axesList, GL_COMPILE);

	this->makeAxes();

	glEndList();
}

void COpenGLObject::makeAxes()
{
	// draw coordinates axes
	this->qglColor(QColor(255.0, 0.0, 0.0));
	glBegin(GL_LINES);
	glVertex3d(0, 0, 0);
	glVertex3d(0.2, 0, 0);
	glEnd();

	this->qglColor(QColor(0.0, 255.0, 0.0));
	glBegin(GL_LINES);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 0.2, 0);
	glEnd();

	this->qglColor(QColor(0.0, 0.0, 255.0));
	glBegin(GL_LINES);
	glVertex3d(0, 0, 0);
	glVertex3d(0, 0, 0.2);
	glEnd();
}

bool COpenGLObject::getExtends()
{
	for ( int i = 0; i < project.getNumberOfXYZPointArrays(); i++)
	{
		CXYZPointArray* xyz = project.getPointArrayAt(i);

		xyz->getExtends();

		if ( xyz->getMaxX() > this->max.x ) this->max.x = xyz->getMaxX();
		if ( xyz->getMaxY() > this->max.y ) this->max.y = xyz->getMaxY();
		if ( xyz->getMaxZ() > this->max.z ) this->max.z = xyz->getMaxZ();

		if ( xyz->getMinX() < this->min.x ) this->min.x = xyz->getMinX();
		if ( xyz->getMinY() < this->min.y ) this->min.y = xyz->getMinY();
		if ( xyz->getMinZ() < this->min.z ) this->min.z = xyz->getMinZ();
	}


	if ( project.getNumberOfMonoplottedPoints() > 0)
	{
		CXYZPointArray* xyz = project.getMonoplottedPoints();

		xyz->getExtends();

		if ( xyz->getMaxX() > this->max.x ) this->max.x = xyz->getMaxX();
		if ( xyz->getMaxY() > this->max.y ) this->max.y = xyz->getMaxY();
		if ( xyz->getMaxZ() > this->max.z ) this->max.z = xyz->getMaxZ();

		if ( xyz->getMinX() < this->min.x ) this->min.x = xyz->getMinX();
		if ( xyz->getMinY() < this->min.y ) this->min.y = xyz->getMinY();
		if ( xyz->getMinZ() < this->min.z ) this->min.z = xyz->getMinZ();

	}

	if ( project.getNumberOfGeoreferencedPoints() > 0)
	{
		CXYZPointArray* xyz = project.getGeoreferencedPoints();

		xyz->getExtends();

		if ( xyz->getMaxX() > this->max.x ) this->max.x = xyz->getMaxX();
		if ( xyz->getMaxY() > this->max.y ) this->max.y = xyz->getMaxY();
		if ( xyz->getMaxZ() > this->max.z ) this->max.z = xyz->getMaxZ();

		if ( xyz->getMinX() < this->min.x ) this->min.x = xyz->getMinX();
		if ( xyz->getMinY() < this->min.y ) this->min.y = xyz->getMinY();
		if ( xyz->getMinZ() < this->min.z ) this->min.z = xyz->getMinZ();

	}

	for ( int i = 0; i < project.getNumberOfDEMs(); i++)
	{
		CVDEM* vdem = project.getDEMAt(i);
		CDEM* dem = vdem->getDEM();

		if ( dem->getmaxX() > this->max.x ) this->max.x = dem->getmaxX();
		if ( dem->getmaxY() > this->max.y ) this->max.y = dem->getmaxY();
		if ( dem->getmaxZ() > this->max.z ) this->max.z = dem->getmaxZ();

		if ( dem->getminX() < this->min.x ) this->min.x = dem->getminX();
		if ( dem->getminY() < this->min.y ) this->min.y = dem->getminY();
		if ( dem->getminZ() < this->min.z ) this->min.z = dem->getminZ();
	}

	for ( int i = 0; i < project.getNumberOfAlsDataSets(); i++)
	{
		CVALS* vals = project.getALSAt(i);

		C3DPoint alsmax = vals->getCALSData()->getPMax();
		C3DPoint alsmin = vals->getCALSData()->getPMin();

		if ( alsmax.x > this->max.x ) this->max.x = alsmax.x;
		if ( alsmax.y > this->max.y ) this->max.y = alsmax.y;
		if ( alsmax.z > this->max.z ) this->max.z = alsmax.z;

		if ( alsmin.x < this->min.x ) this->min.x = alsmin.x;
		if ( alsmin.y < this->min.y ) this->min.y = alsmin.y;
		if ( alsmin.z < this->min.z ) this->min.z = alsmin.z;
	}
	
	this->dim.x = this->max.x - this->min.x;
	this->dim.y = this->max.y - this->min.y;
	this->dim.z = this->max.z - this->min.z;

	this->scale = 1.0 / sqrt(this->dim.x*this->dim.x + this->dim.y*this->dim.y + this->dim.z*this->dim.z);

	this->center.x = this->min.x + this->dim.x/2;
	this->center.y = this->min.y + this->dim.y/2;
	this->center.z = this->min.z + this->dim.z/2;

	return true;
}

void COpenGLObject::makeDEM(CVDEM* vdem, QColor qcolor)
{
	CDEM* dem = vdem->getDEM();

	C3DPoint pmin = dem->getPMin();
	C3DPoint pmax = dem->getPMax();

	this->demList.push_back(TiledElement());
	unsigned int dindex = this->demList.size()-1;

	this->demList.at(dindex).tilesacross = dem->tilesAcross;
	this->demList.at(dindex).tilesdown = dem->tilesDown;

	this->demList.at(dindex).pointdist.x = dem->getgridX();
	this->demList.at(dindex).pointdist.y = dem->getgridY();

	unsigned int ncols = this->demList.at(dindex).tilesacross;
	unsigned int nrows = this->demList.at(dindex).tilesdown;

	C3DPoint tul;
	C3DPoint tll;
	C3DPoint tlr;
	C3DPoint tur;
	
	this->demList.at(dindex).tiles.resize(ncols*nrows);
	
	//int nlevels = dem->getNumberOfPyramidLevels();
	int nlevels = 8;

	double metricTileWidth = this->demList.at(dindex).pointdist.x*dem->tileWidth;
	double metricTileHeight = this->demList.at(dindex).pointdist.y*dem->tileHeight;

	double meanZ =  (pmax.z + pmin.z)/2.0;

	for (unsigned int row = 0; row < nrows; row++)
	{
		for (unsigned int col = 0; col < ncols; col++)
		{
			unsigned int tileindex = row*ncols + col;
			
			//this->demList.at(dindex).tiles[tileindex].glList = glGenLists(1);
			//glNewList(this->demList.at(dindex).tiles[tileindex].glList, GL_COMPILE);
			this->demList.at(dindex).tiles[tileindex].hasList = false;
			
			this->demList.at(dindex).tiles[tileindex].lowLevel = nlevels;
			this->demList.at(dindex).tiles[tileindex].level = this->demList.at(dindex).tiles[tileindex].lowLevel;

			tul.x = pmin.x + (col)*metricTileWidth;
			tul.y = pmax.y - (row)*metricTileHeight;
			tul.z = meanZ;

			tll.x = pmin.x + (col)*metricTileWidth;
			tll.y = pmax.y - (row+1)*metricTileHeight;
			tll.z = meanZ;

			tlr.x = pmin.x + (col+1)*metricTileWidth;
			tlr.y = pmax.y - (row+1)*metricTileHeight;
			tlr.z = meanZ;

			tur.x = pmin.x + (col+1)*metricTileWidth;
			tur.y = pmax.y - (row)*metricTileHeight;
			tur.z = meanZ;
			
			if ( col == ncols - 1)
			{
				tlr.x = pmax.x;
				tur.x = pmax.x;
			}
			
			if ( row == nrows - 1)
			{
				tll.y = pmin.y;
				tlr.y = pmin.y;
			}

			this->demList.at(dindex).tiles[tileindex].ul = tul;
			this->demList.at(dindex).tiles[tileindex].ll = tll;
			this->demList.at(dindex).tiles[tileindex].lr = tlr;
			this->demList.at(dindex).tiles[tileindex].ur = tur;

			this->demList.at(dindex).tiles[tileindex].pmid.x = (tul.x + tll.x + tlr.x + tur.x)/4;
			this->demList.at(dindex).tiles[tileindex].pmid.y = (tul.y + tll.y + tlr.y + tur.y)/4;
			this->demList.at(dindex).tiles[tileindex].pmid.z = meanZ;
		}
	}
}

void COpenGLObject::makeALS(CVALS* vals, QColor qcolor, int level)
{
	CALSDataSet* als = vals->getCALSData();

	C3DPoint pmin = als->getPMin();
	C3DPoint pmax = als->getPMax();

	this->alsList.push_back(TiledElement());
	unsigned int aindex = this->alsList.size()-1;

	this->alsList.at(aindex).tilesacross = als->getTilesAcross();
	this->alsList.at(aindex).tilesdown = als->getTilesDown();
	this->alsList.at(aindex).pointdist = als->getPointDist();

	unsigned int ncols = this->alsList.at(aindex).tilesacross;
	unsigned int nrows = this->alsList.at(aindex).tilesdown;

	C3DPoint tul;
	C3DPoint tll;
	C3DPoint tlr;
	C3DPoint tur;
	C3DPoint tmid;
	
	this->alsList.at(aindex).tiles.resize(ncols*nrows);

	for (unsigned int row = 0; row < nrows; row++)
	{
		for (unsigned int col = 0; col < ncols; col++)
		{
			unsigned int tileindex = row*ncols + col;

			this->alsList.at(aindex).tiles[tileindex].lowLevel = als->getMinCommonSublevel();
			this->alsList.at(aindex).tiles[tileindex].level = this->alsList.at(aindex).tiles[tileindex].lowLevel;

			int level = this->alsList.at(aindex).tiles[tileindex].level;
			int lowlevel = this->alsList.at(aindex).tiles[tileindex].lowLevel;

			als->getTileExtension(row, col, tul, tll, tlr, tur);

			this->alsList.at(aindex).tiles[tileindex].ul = tul;
			this->alsList.at(aindex).tiles[tileindex].ll = tll;
			this->alsList.at(aindex).tiles[tileindex].lr = tlr;
			this->alsList.at(aindex).tiles[tileindex].ur = tur;

			tmid.x = (tul.x + tll.x + tlr.x + tur.x)/4;
			tmid.y = (tul.y + tll.y + tlr.y + tur.y)/4;
			tmid.z = (tul.z + tll.z + tlr.z + tur.z)/4;

			this->alsList.at(aindex).tiles[tileindex].pmid = tmid;

			this->alsList.at(aindex).tiles[tileindex].level = lowlevel;
		}
	}
}

void COpenGLObject::makePointLabels(CXYZPointArray* pointArray, QColor qcolor)
{
	/*
	if ( pointArray->getReferenceSystem()->getCoordinateType() == eGEOGRAPHIC )
			zfactor /= 100000;

	GLdouble x1;
	GLdouble y1;
	GLdouble z1;

	this->qglColor(qcolor);

	QFont qfont("Helvetica", 10, 75);

	for (int i = 0; i < pointArray->GetSize(); i++)
	{
		CObsPoint* Point = pointArray->GetAt(i);
		

		QString qlabel(Point->getLabel().GetChar());
		this->renderText(x1, y1, z1, qlabel, qfont, this->objects.at(this->listcount));	

	}
	*/
}

void COpenGLObject::makeLines(CXYZPolyLineArray* lines, QColor qcolor)
{
	GLdouble x1;
	GLdouble y1;
	GLdouble z1;

	this->qglColor(qcolor);
	for (int i = 0; i < lines->GetSize(); i++)
	{
		CXYZPolyLine* line = lines->getAt(i);

		if ( line->isClosed()) glBegin(GL_LINE_LOOP);
		else glBegin(GL_LINE_STRIP);

		for ( int j = 0; j < line->getPointCount(); j++)
		{
			CXYZPoint point = line->getPoint(j);
			this->toModel(point, x1, y1, z1);
			glVertex3d(x1, y1, z1);
			
		}
		glEnd();
	}
}
#ifdef WIN32
void COpenGLObject::makeBuildings(CBuildingArray* buildings, QColor qcolor)
{
	this->qglColor(qcolor);
	for (int i = 0; i < buildings->GetSize(); i++)
	{
		CBuilding* building = buildings->GetAt(i);

		CXYZPolyLineArray loops;
		building->getAllLoops(loops);

		this->makeLines(&loops, qcolor);
	}

}
#endif


void COpenGLObject::refreshView()
{
	this->initializeGL();
}


bool COpenGLObject::toModel(const CPointBase& point, GLdouble &xmodel, GLdouble &ymodel, GLdouble &zmodel)
{

	xmodel = this->scale*(point[0] - this->center.x);
	ymodel = this->scale*(point[1] - this->center.y);
	zmodel = this->scale*(point[2] - this->center.z);

	return true;
}

bool COpenGLObject::toScreen(const CPointBase& point, GLdouble &xscreen, GLdouble &yscreen, GLdouble &zscreen)
{
	GLdouble xmodel;
	GLdouble ymodel;
	GLdouble zmodel;

	this->toModel(point, xmodel, ymodel, zmodel);
	
	GLdouble projMatrix[16];
	GLdouble modelMatrix[16];
	GLint viewport[4];

	::glGetDoublev(GL_PROJECTION_MATRIX, projMatrix);
	::glGetDoublev(GL_MODELVIEW_MATRIX, modelMatrix);
	::glGetIntegerv(GL_VIEWPORT, viewport);

	gluProject(xmodel, ymodel, zmodel, modelMatrix, projMatrix, viewport,&xscreen,&yscreen,&zscreen);

	int w = this->width();
	int h = this->height();

	bool ret = true;
	if ((xscreen < 0 ) || (xscreen > w)  || (yscreen < 0 ) || (yscreen > h )) 
		ret = false;
	return ret;
}

int COpenGLObject::refreshALSTile(unsigned int valsindex, int row, int col, GLuint list, int level)
{
	GLdouble x1;
	GLdouble y1;
	GLdouble z1;

	QColor qfirst(Qt::lightGray);
	QColor qlast(Qt::red);
	
	glPointSize(this->pointsize);

	CVALS* vals = project.getALSAt(valsindex);

	CALSDataSet* als = vals->getCALSData();

	unsigned int tileindex = row*als->getTilesAcross() + col; 		
	this->alsList.at(valsindex).tiles[tileindex].level = level;	

	QTime qt1;
	qt1.start();

	int elementcount = als->getPointCloudTile(row, col, this->alspoints, this->capacity, 512*512, level);
	this->alsList.at(valsindex).tiles[tileindex].elementcount = elementcount;

	this->sum1 += qt1.elapsed();
	qt1.restart();

	/*
	if ( level == 0 ) qfirst = Qt::red;
	if ( level == 1 ) qfirst = Qt::green;
	if ( level == 2 ) qfirst = Qt::blue;
	if ( level == 3 ) qfirst = Qt::yellow;
	if ( level == 4 ) qfirst = Qt::cyan;
	if ( level == 5 ) qfirst = Qt::magenta;
	*/
	/*
	if ( tileindex == 0 ) qfirst = Qt::red;
	else if ( tileindex == 1 ) qfirst = Qt::green;
	else if ( tileindex == 2 ) qfirst = Qt::blue;
	else if ( tileindex == 3 ) qfirst = Qt::yellow;
	else if ( tileindex == 30 ) qfirst = Qt::blue;
	*/

	for (int i = 0; i < elementcount; i++)
	{
		CALSPoint* p = this->alspoints[i];

		this->toModel((*p),x1,y1,z1);
				
		this->qglColor(qfirst);
		glBegin(GL_POINTS);
		glVertex3d(x1, y1, z1);
		glEnd();

		this->qglColor(qlast);
		this->toModel(p->getLastEcho(),x1,y1,z1);	
		glBegin(GL_POINTS);
		glVertex3d(x1, y1, z1);
		glEnd();

	}

	// paint tiles
	GLdouble tx0 = 0.0; 
 	GLdouble ty0 = 0.0; 
 	GLdouble tz0 = 0.0;

 	GLdouble tx1 = 0.0; 
 	GLdouble ty1 = 0.0; 
 	GLdouble tz1 = 0.0;

 	GLdouble tx2 = 0.0; 
 	GLdouble ty2 = 0.0; 
 	GLdouble tz2 = 0.0;

 	GLdouble tx3 = 0.0; 
 	GLdouble ty3 = 0.0; 
 	GLdouble tz3 = 0.0;

 	GLdouble tx4 = 0.0; 
 	GLdouble ty4 = 0.0; 
 	GLdouble tz4 = 0.0;

	this->toModel(this->alsList.at(valsindex).tiles[tileindex].ul,tx0,ty0,tz0);
	this->toModel(this->alsList.at(valsindex).tiles[tileindex].ll,tx1,ty1,tz1);
	this->toModel(this->alsList.at(valsindex).tiles[tileindex].lr,tx2,ty2,tz2);
	this->toModel(this->alsList.at(valsindex).tiles[tileindex].ur,tx3,ty3,tz3);

	tx0 = tx0 +0.0001;
	ty0 = ty0 -0.0001;

	tx1 = tx1 +0.0001;
	ty1 = ty1 +0.0001;

	tx2 = tx2 -0.0001;
	ty2 = ty2 +0.0001;

	tx3 = tx3 -0.0001;
	ty3 = ty3 -0.0001;

	glBegin(GL_LINE_LOOP);
	glVertex3d(tx0, ty0, tz0);
	glVertex3d(tx1, ty1, tz1);
	glVertex3d(tx2, ty2, tz2);
	glVertex3d(tx3, ty3, tz3);
	glEnd();
	

	this->toModel(this->alsList.at(valsindex).tiles[tileindex].pmid,tx4,ty4,tz4);

	this->qglColor(Qt::lightGray);
	glBegin(GL_POINTS);
	glVertex3d(tx0,ty0,tz0);
	glVertex3d(tx1,ty1,tz1);
	glVertex3d(tx2,ty2,tz2);
	glVertex3d(tx3,ty3,tz3);
	glVertex3d(tx4,ty4,tz4);
	glEnd();

	glEndList();

	this->sum2 += qt1.elapsed();
	qt1.restart();

	return elementcount;
}



bool COpenGLObject::areaOnScreen(C3DPoint ul, C3DPoint ll, C3DPoint lr, C3DPoint ur)
{
 	GLdouble wx[4]; 
 	GLdouble wy[4]; 
 	GLdouble wz[4];

	bool blr = this->toScreen(lr,wx[0],wy[0],wz[0]);
	bool bll = this->toScreen(ll,wx[1],wy[1],wz[1]);
	bool bur = this->toScreen(ur,wx[2],wy[2],wz[2]);
	bool bul = this->toScreen(ul,wx[3],wy[3],wz[3]);

	this->testvalue1 = -99999;
	this->testvalue2 = -99999;
	this->testvalue3 = -99999;
	this->testvalue4 = -99999;
	
	int w = this->width();
	int h = this->height();

	double dx = (wx[0] + wx[1] + wx[2] + wx[3])/4 - w/2;
	double dy = (wy[0] + wy[1] + wy[2] + wy[3])/4 - h/2;


	if ( blr || bll || bur || bul ) 
		return true;
	else
	{
		// find min and max
		double dmaxx = wx[0];
		double dmaxy = wy[0];
		double dminx = wx[0];
		double dminy = wy[0];

		int imaxx = 0;
		int iminx = 0;
		int imaxy = 0;
		int iminy = 0;

		for ( int i = 1; i < 4; i++)
		{
			if ( dmaxx < wx[i] ) 
			{
				dmaxx = wx[i];
				imaxx = i;
			}
			if ( dmaxy < wy[i] )
			{
				dmaxy = wy[i];
				imaxy = i;
			}
			if ( dminx > wx[i] )
			{
				dminx = wx[i];
				iminx = i;
			}
			if ( dminy > wy[i] )
			{
				dminy = wy[i];
				iminy = i;
			}
		}

		
		// 3 == UL, 1 == LL
		double leanx =  wx[3] - wx[1];
		double leany =  wy[3] - wy[1];

		bool leaningleft;

		if ( leanx*leany > 0 ) leaningleft = false;
		else leaningleft = true;
		



		this->testvalue1 = -99;

		this->testvalue2 = leanx;
		this->testvalue3 = leany;

		if ( leaningleft ) this->testvalue1 = 1;
		else if ( !leaningleft ) this->testvalue1 = -1;


		if (dmaxx < 0 || dminx > w || dmaxy < 0 || dminy > h )
			return false;
		else
		{
			if ( dminx < 0 && dmaxx > w && dminy < 0 && dmaxy > h ) 
				return true;
			else if ( dminx < 0 && dmaxx > w && dminy < h && dminy > 0 ) 
				return true;
			else if ( dminx < 0 && dmaxx > w && dmaxy < h && dmaxy > 0 ) 
				return true;
			else if ( dminy < 0 && dmaxy > h && dminx < w && dminx > 0 ) 
				return true;
			else if ( dminy < 0 && dmaxy > h && dmaxx < w && dmaxx > 0 ) 
				return true;

			//double dx = (wx[0] + wx[1] + wx[2] + wx[3])/4 - w/2;
			//double dy = (wy[0] + wy[1] + wy[2] + wy[3])/4 - h/2;

			// check which quadrant

			//   -  |	+
			//   +  |   +
			// -----------
			//   -  |   +
			//   -  |   -

			this->testvalue2 = dx;
			this->testvalue3 = dy;

			if ( dx > 0 && dy > 0 )
			{
						
				//C2DPoint P1(wx[iminy], dminy);
				//C2DPoint P2(dminx, wy[iminx]);

				//C2DPoint N(P2.y-P1.y, P1.x - P2.x);

				//double ln = sqrt(N.x*N.x + N.y*N.y);

				//C2DPoint N0(N.x/ln, N.y/ln);

				//C2DPoint R1(w, h);
				//C2DPoint R2(w/2, h/2);

				//this->distance1 = N0.x*(R1.x -P1.x) + N0.y*(R1.y - P1.y);
				//this->distance2 = N0.x*(R2.x -P1.x) + N0.y*(R1.y - P1.y);

				//if ( this->distance1*this->distance2 > 0 ) return false;
				//return true;

				if ( leaningleft )
				{
					if ( (wx[iminy] < w ) && (dminy < 0 && wy[iminx] > h)) 
						return true;

					double Nx = wy[iminx] - dminy;
					double Ny = wx[iminy] - dminx;

					if ( fabs(Nx) < FLT_EPSILON || fabs(Ny) < FLT_EPSILON) 
						return true;

					double ln = 1.0/sqrt(Nx*Nx + Ny*Ny);

					Nx *= ln;
					Ny *= ln;

					double dscreenLL = Nx*(0 - wx[iminy]) + Ny*(0 - dminy);
					double dscreenUR = Nx*(w - wx[iminy]) + Ny*(h - dminy);

					if ( dscreenLL*dscreenUR < 0 )
						return true;
					else
						return false;
				}
				else
				{
					if ( (wx[imaxy] < w ) && (wy[iminx] < 0 && dmaxy > h)) 
						return true;

					double Nx = wy[iminx] - dmaxy;
					double Ny = wx[imaxy] - dminx;

					if ( fabs(Nx) < FLT_EPSILON || fabs(Ny) < FLT_EPSILON) 
						return true;

					double ln = 1.0/sqrt(Nx*Nx + Ny*Ny);

					Nx *= ln;
					Ny *= ln;

					double dscreenUL = Nx*(0 - wx[imaxy]) + Ny*(h - dmaxy);
					double dscreenLR = Nx*(w - wx[imaxy]) + Ny*(0 - dmaxy);

					if ( dscreenUL*dscreenLR < 0 )
						return true;
					else
						return false;
				}
			}
			else if ( dx > 0 && dy < 0 )
			{
				if ( leaningleft )
				{
					if ( (wx[imaxy] < w ) && (wy[iminx] < 0 && dmaxy > h)) 
						return true;

					double Nx = wy[iminx] - dmaxy;
					double Ny = wx[imaxy] - dminx;

					if ( fabs(Nx) < FLT_EPSILON || fabs(Ny) < FLT_EPSILON ) 
						return true;

					double ln = 1.0/sqrt(Nx*Nx + Ny*Ny);

					Nx *= ln;
					Ny *= ln;

					double dscreenUL = Nx*(0 - wx[imaxy]) + Ny*(h - dmaxy);
					double dscreenLR = Nx*(w - wx[imaxy]) + Ny*(0 - dmaxy);

					if ( dscreenUL*dscreenLR < 0 )
						return true;
					else
						return false;
				}
				else
				{
					if ( (wx[imaxy] < w ) && (wy[iminx] < 0 && dmaxy > h)) 
						return true;

					double Nx = wy[iminx] - dmaxy;
					double Ny = wx[imaxy] - dminx;

					if ( fabs(Nx) < FLT_EPSILON || fabs(Ny) < FLT_EPSILON) 
						return true;

					double ln = 1.0/sqrt(Nx*Nx + Ny*Ny);

					Nx *= ln;
					Ny *= ln;

					double dscreenUL = Nx*(0 - wx[imaxy]) + Ny*(h - dmaxy);
					double dscreenLR = Nx*(w - wx[imaxy]) + Ny*(0 - dmaxy);

					if ( dscreenUL*dscreenLR < 0 )
						return true;
					else
						return false;
				}
			}
			else if ( dx < 0 && dy > 0 )
			{
				if ( leaningleft )
				{
					if ( (dmaxx > 0 ) && (wy[imaxx] > h && dminy > 0)) 
						return true;

					double Nx = wy[imaxx] - dminy;
					double Ny = wx[iminy] - dmaxx;

					if ( fabs(Nx) < FLT_EPSILON || fabs(Ny) < FLT_EPSILON) 
						return true;

					double ln = 1.0/sqrt(Nx*Nx + Ny*Ny);

					Nx *= ln;
					Ny *= ln;

					double dscreenUL = Nx*(0 - wx[iminy]) + Ny*(h - dminy);
					double dscreenLR = Nx*(w - wx[iminy]) + Ny*(0 - dminy);

					if ( dscreenUL*dscreenLR < 0 )
						return true;
					else
						return false;
				}
				else
				{
					if ( (dmaxx > 0 ) && (dmaxy > h  && wy[imaxx] < 0)) 
						return true;

					double Nx = wy[imaxx] - dmaxy;
					double Ny = wx[imaxy] - dmaxx;

					if ( fabs(Nx) < FLT_EPSILON || fabs(Ny) < FLT_EPSILON) 
						return true;

					double ln = 1.0/sqrt(Nx*Nx + Ny*Ny);

					Nx *= ln;
					Ny *= ln;

					double dscreenLL = Nx*(0 - wx[imaxy]) + Ny*(0 - dmaxy);
					double dscreenUR = Nx*(w - wx[imaxy]) + Ny*(h - dmaxy);

					if ( dscreenLL*dscreenUR < 0 )
						return true;
					else
						return false;
				}
			}
			else if ( dx < 0 && dy < 0 )
			{
				if ( leaningleft )
				{
					if ( (wx[imaxy] > 0 ) && (wy[imaxx] < 0 && dmaxy > h)) 
						return true;

					double Nx = wy[imaxx] - dmaxy;
					double Ny = wx[imaxy] - dmaxx;

					if ( fabs(Nx) < FLT_EPSILON || fabs(Ny) < FLT_EPSILON) 
						return true;

					double ln = 1.0/sqrt(Nx*Nx + Ny*Ny);

					Nx *= ln;
					Ny *= ln;

					double dscreenLL = Nx*(0 - wx[imaxy]) + Ny*(0 - dmaxy);
					double dscreenUR = Nx*(w - wx[imaxy]) + Ny*(h - dmaxy);

					if ( dscreenUR*dscreenLL < 0 )
						return true;
					else
						return false;
				}
				else
				{
					if ( (wx[imaxy] > 0 ) && (dmaxy > h && wy[imaxx] < 0)) 
						return true;

					double Nx = wy[imaxx] - dmaxy;
					double Ny = wx[imaxy] - dmaxx;

					if ( fabs(Nx) < FLT_EPSILON || fabs(Ny) < FLT_EPSILON) 
						return true;

					double ln = 1.0/sqrt(Nx*Nx + Ny*Ny);

					Nx *= ln;
					Ny *= ln;

					double dscreenLL = Nx*(0 - wy[imaxy]) + Ny*(0 - dmaxy);
					double dscreenUR = Nx*(w - wy[imaxy]) + Ny*(h - dmaxy);

					if ( dscreenLL*dscreenUR < 0 )
						return true;
					else
						return false;
				}
			}
			else if ( fabs(dx) < FLT_EPSILON )
				return true;
			else if ( fabs(dy) < FLT_EPSILON )
				return true;
			else
				return false;
		}
	}
}


int COpenGLObject::refreshDEMTile(unsigned int demindex, int row, int col, GLuint list, int level)
{
	GLdouble x1;
	GLdouble y1;
	GLdouble z1;
	
	glPointSize(this->pointsize);

	CVDEM* vdem = project.getDEMAt(demindex);

	CDEM* dem = vdem->getDEM();

	this->lut = CLookupTableHelper::getDSMLookup();

	unsigned int tileindex = row*dem->tilesAcross + col; 		

	QColor qcolor(Qt::red);

/*
	if ( level == 0 ) qcolor = Qt::red;
	else if ( level == 1 ) qcolor = Qt::green;
	else if ( level == 2 ) qcolor = Qt::blue;
	else if ( level == 3 ) qcolor = Qt::yellow;
	else if ( level == 4 ) qcolor = Qt::cyan;
	else if ( level == 5 ) qcolor = Qt::magenta;

	
	if ( tileindex == 0 ) qcolor = Qt::red;
	else if ( tileindex == 1 ) qcolor = Qt::green;
	else if ( tileindex == 2 ) qcolor = Qt::blue;
	else if ( tileindex == 3 ) qcolor = Qt::yellow;
	else if ( tileindex == 30 ) qcolor = Qt::blue;

	//qcolor = QColor(3*row+100,3*col+100,row*col+100);
	*/

	QTime qt1;
	qt1.start();

	this->demList.at(demindex).tiles[tileindex].elementcount = dem->getTilePointVector(row, col, level, this->points);

	this->sum1 += qt1.elapsed();

	this->qglColor(qcolor);

	qt1.restart();

	unsigned int size = this->demList.at(demindex).tiles[tileindex].elementcount;

	int r;
	int g;
	int b;

	for (unsigned int i = 0; i < size; i++)
	{
		this->toModel(this->points[i],x1,y1,z1);

		lut.getColor(r, g, b, this->points[i].id);	
		qcolor = QColor(r,g,b);
		this->qglColor(qcolor);
				
		glBegin(GL_POINTS);
		glVertex3d(x1, y1, z1);
		glEnd();
	}

	this->sum2 += qt1.elapsed();
	qt1.restart();

	// paint tiles
	
 	GLdouble tx0 = 0.0; 
 	GLdouble ty0 = 0.0; 
 	GLdouble tz0 = 0.0;

 	GLdouble tx1 = 0.0; 
 	GLdouble ty1 = 0.0; 
 	GLdouble tz1 = 0.0;

 	GLdouble tx2 = 0.0; 
 	GLdouble ty2 = 0.0; 
 	GLdouble tz2 = 0.0;

 	GLdouble tx3 = 0.0; 
 	GLdouble ty3 = 0.0; 
 	GLdouble tz3 = 0.0;

	this->toModel(this->demList.at(demindex).tiles[tileindex].ul,tx0,ty0,tz0);
	this->toModel(this->demList.at(demindex).tiles[tileindex].ll,tx1,ty1,tz1);
	this->toModel(this->demList.at(demindex).tiles[tileindex].lr,tx2,ty2,tz2);
	this->toModel(this->demList.at(demindex).tiles[tileindex].ur,tx3,ty3,tz3);

	glBegin(GL_LINE_LOOP);
	glVertex3d(tx0,ty0,tz0);
	glVertex3d(tx1,ty1,tz1);
	glVertex3d(tx2,ty2,tz2);
	glVertex3d(tx3,ty3,tz3);
	glEnd();
	
	glEndList();


	this->sum3 +=  qt1.elapsed();

	return size;
}


bool COpenGLObject::checkTileLevel(C3DPoint ul, C3DPoint ll, C3DPoint lr, C3DPoint ur, C3DPoint tmiddle, C2DPoint dxy, int &level, int lowlevel, double min)
{
	if ( !this->areaOnScreen(ul, ll, lr, ur))
	{
		level = lowlevel;
		return false;
	}
	
	int oldlevel = level;

	GLdouble xscreen0;
	GLdouble yscreen0;
	GLdouble zscreen0;
	GLdouble xscreen1;
	GLdouble yscreen1;
	GLdouble zscreen1;
	GLdouble xscreen2;
	GLdouble yscreen2;
	GLdouble zscreen2;
	GLdouble xscreen3;
	GLdouble yscreen3;
	GLdouble zscreen3;

	C3DPoint t1 = tmiddle;
	double delta = (dxy.x > dxy.y)? dxy.x :dxy.y;
	t1.x += delta;
	C3DPoint t2 = tmiddle;
	t2.y += delta;
	C3DPoint t3 = tmiddle;
	t3.z += delta;

	bool mid = this->toScreen(tmiddle, xscreen0, yscreen0, zscreen0);
	this->toScreen(t1, xscreen1, yscreen1, zscreen1);
	this->toScreen(t2, xscreen2, yscreen2, zscreen2);
	this->toScreen(t3, xscreen3, yscreen3, zscreen3);

	xscreen1 -= xscreen0;
	yscreen1 -= yscreen0;
	xscreen2 -= xscreen0;
	yscreen2 -= yscreen0;
	xscreen3 -= xscreen0;
	yscreen3 -= yscreen0;

	double ds1 = sqrt(xscreen1 * xscreen1 + yscreen1 * yscreen1);
	double ds2 = sqrt(xscreen2 * xscreen2 + yscreen2 * yscreen2);
	double ds3 = sqrt(xscreen3 * xscreen3 + yscreen3 * yscreen3);

	double ds = (ds1 > ds2) ? ds1 : ds2;
	ds = (ds > ds3) ? ds : ds3;

	//level = int(floor(log(2.0) * (log(min) - log(ds*ds*ds)) + 0.5));
	level = int(floor(log(2.0) * (log(min) - log(ds)) + 0.5));
	if (level > lowlevel) level = lowlevel;
	else if (level < 0) level = 0;
	//else if (level < 2) level = 2;
	
	return true;
}

