Bundle Adjustment
=================

Date: 06/10/11
Time: 12:09:56

Stations (1): 


Number of point observations:              57
Number of direct observations:              0
Number of constraints:                      0
Number of unknown station parameters:      12
Number of unknown 3D Points:                0
Redundancy:                                45



Starting iteration process
==========================


Station (  1): Orbit Path Model, 19 / 19 / 19 points in X / Y / Z
   RMS of residuals in X / Y / Z [m]                     9318.905 /   44.870 /  16934.547
   Max residual in X / Y / Z [m]                         16410.959 /  105.397 /  -29950.377
   Max residual in X / Y / Z [units of sigma a priori]:  21881.279 /  140.530 /  39933.836


The  10 worst normalised residuals:
===================================

               Point  Obs.  normRes   Residual  Station      Model
                        Z  39933.836 -29950.377    (  1)     Orbit Path Model
                        Z  35492.071 -26619.053    (  1)     Orbit Path Model
                        Z  35416.866 26562.649    (  1)     Orbit Path Model
                        Z  35416.866 26562.649    (  1)     Orbit Path Model
                        Z  35416.866 26562.649    (  1)     Orbit Path Model
                        Z  35416.866 26562.649    (  1)     Orbit Path Model
                        Z  35416.866 26562.649    (  1)     Orbit Path Model
                        Z  35416.866 26562.649    (  1)     Orbit Path Model
                        Z  35416.866 26562.649    (  1)     Orbit Path Model
                        Z  35416.866 26562.649    (  1)     Orbit Path Model




Iteration finished
==================

   Sigma 0: 0.000

==================



Station (  1): Orbit Path Model, 19 / 19 / 19 points in X / Y / Z
   RMS of residuals in X / Y / Z [m]                       0.000 /    0.000 /    0.000
   Max residual in X / Y / Z [m]                           0.000 /   -0.000 /    0.000
   Max residual in X / Y / Z [units of sigma a priori]:    0.000 /    0.000 /    0.000


The  10 worst normalised residuals:
===================================

               Point  Obs.  normRes   Residual  Station      Model
                        X     0.000     0.000    (  1)     Orbit Path Model
                        X     0.000     0.000    (  1)     Orbit Path Model
                        X     0.000     0.000    (  1)     Orbit Path Model
                        X     0.000     0.000    (  1)     Orbit Path Model
                        X     0.000     0.000    (  1)     Orbit Path Model
                        X     0.000     0.000    (  1)     Orbit Path Model
                        X     0.000     0.000    (  1)     Orbit Path Model
                        X     0.000     0.000    (  1)     Orbit Path Model
                        X     0.000     0.000    (  1)     Orbit Path Model
                        X     0.000     0.000    (  1)     Orbit Path Model




Iteration finished
Time: 12:09:56

Adjustment converged
====================

Bundle Adjustment
=================

Date: 06/10/11
Time: 12:09:56


Statistics if observations / unknowns:
======================================

Number of image / object point observations:      57
Number of direct observations:                     0
Number of unknown station parameters:             12
Number of unknown 3D Points:                       0

Total number of observations:                     57
Total number of unknowns:                         12
Number of constraints:                             0
Redundancy:                                       45
====================================================


R e s u l t s   o f   A d j u s t m e n t
=========================================


List of Stations:
=================

Station  Model                      Points    RMSX     RMSY     RMSZ    Name
(  1)    Orbit Path Model               19    0.000    0.000    0.000   


RMS error of the weight unit: Sigma0 = +-  0.000
================================================

   Station parameters:
   ===================

      Station (  1): Orbit Path Model
      Spline Parameter             Parameter[m]	       Sigma[m]
      Segment   1 (    X) a0:       -16410.96 +-            0.00
      Segment   1 (    X) a1:        30876.64 +-            0.00
      Segment   1 (    X) a2:          230.39 +-            0.00
      Segment   1 (    X) a3:           -0.48 +-            0.00
      Segment   1 (    Y) a0:         -105.40 +-            0.00
      Segment   1 (    Y) a1:          269.93 +-            0.00
      Segment   1 (    Y) a2:         -133.76 +-            0.00
      Segment   1 (    Y) a3:           -0.09 +-            0.00
      Segment   1 (    Z) a0:        29950.38 +-            0.00
      Segment   1 (    Z) a1:       -56639.93 +-            0.00
      Segment   1 (    Z) a2:          126.14 +-            0.00
      Segment   1 (    Z) a3:            0.76 +-            0.00


   Object points:
   ==============

         Point      X [m]   Sigma[m]      Y [m]   Sigma[m]    Z [m] Sigma[m]
      RMS SIGMA:              +- 0.000              +- 0.000          +- 0.000


Statistics of residuals:
========================

Station (  1): Orbit Path Model, 19 / 19 / 19 points in X / Y / Z
   RMS of residuals in X / Y / Z [m]                       0.000 /    0.000 /    0.000
   Max residual in X / Y / Z [m]                           0.000 /   -0.000 /    0.000
   Max residual in X / Y / Z [units of sigma a priori]:    0.000 /    0.000 /    0.000


The  10 worst normalised residuals:
===================================

Point  Obs.  normRes   Residual  Station      Model
    X     0.000     0.000    (  1)     Orbit Path Model
    X     0.000     0.000    (  1)     Orbit Path Model
    X     0.000     0.000    (  1)     Orbit Path Model
    X     0.000     0.000    (  1)     Orbit Path Model
    X     0.000     0.000    (  1)     Orbit Path Model
    X     0.000     0.000    (  1)     Orbit Path Model
    X     0.000     0.000    (  1)     Orbit Path Model
    X     0.000     0.000    (  1)     Orbit Path Model
    X     0.000     0.000    (  1)     Orbit Path Model
    X     0.000     0.000    (  1)     Orbit Path Model




L i s t   o f   a l l   r e s i d u a l s :
===========================================

Station (  1):
=============

Point     res_X      res_Y      res_Z  [m]
     0.000    -0.000     0.000 
    -0.000     0.000    -0.000 
    -0.000     0.000    -0.000 
    -0.000     0.000    -0.000 
    -0.000     0.000    -0.000 
    -0.000     0.000    -0.000 
     0.000    -0.000     0.000 
     0.000    -0.000     0.000 
     0.000    -0.000     0.000 
     0.000    -0.000     0.000 
     0.000    -0.000     0.000 
     0.000    -0.000     0.000 
     0.000    -0.000     0.000 
    -0.000     0.000    -0.000 
    -0.000     0.000    -0.000 
    -0.000     0.000    -0.000 
    -0.000     0.000    -0.000 
    -0.000     0.000    -0.000 
     0.000    -0.000     0.000 
