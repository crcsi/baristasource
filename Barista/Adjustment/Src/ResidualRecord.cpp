#include <strstream>
using namespace std;

#include "ResidualRecord.h"
#include "SensorModel.h"
#include "ObsPoint.h"
	  
void CResidualRecord::set(double Res, double NormRes, CObsPoint *Obs, const CCharString &StationName, 
						  CSensorModel *SensorMod, int Index)
{
	this->obs         = Obs;
	this->sensorMod   = SensorMod;
	this->index       = Index;
	this->res         = Res;
	this->normRes     = NormRes;
	this->stationName = StationName;
}
	 
void CResidualRecord::set(double Res, double NormRes, int Index)
{
	this->index     = Index;
	this->res       = Res;
	this->normRes   = NormRes;
}
	  
CResidualRecord &CResidualRecord::operator = (const CResidualRecord &rec) 
{
	if (this != &rec)
	{
		this->obs         = rec.obs;
		this->sensorMod   = rec.sensorMod;
		this->index       = rec.index;
		this->res         = rec.res;
		this->normRes     = rec.normRes;
		this->stationName = rec.stationName;
	}

	return *this;
};

CCharString CResidualRecord::getString(int pointWidth) const
{
	CCharStringArray obsNames;
	this->sensorMod->getObservationNames(obsNames);

	CCharString name;
	this->sensorMod->getName(name);
				
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision(3);
	oss.width(pointWidth);

	oss << this->obs->getLabel().GetChar() << "    " << obsNames.GetAt(this->index)->GetChar() << "  ";
	oss.width(8);
	oss << this->normRes << " ";
	oss.precision(3);
	oss.width(9);
	oss << this->res << "    " << this->stationName.GetChar()
		<< "     " << name.GetChar() << ends;
	char *z = oss.str();
	CCharString str(z);
	delete [] z;

	return str;
};
