#include "OrbitPathModel.h"
#include "AdjustMatrix.h"
#include "utilities.h"
#include <strstream>


COrbitPathModel::COrbitPathModel(void) : CSplineModel(3,3,0,6), shift(0,0,0),
	shiftObservation(0,0,0),rotParameter(&C3DPoint(0,0,0)),angleObservation(0,0,0),
	satH(0.0),sigmaSatH(0.0),sigmaInnerProduct(0.0)
{
	this->shift.setParent(this);
	this->shiftObservation.setParent(this);
	this->rotParameter.setParent(this);

	// init the direct obs
	// shift
	CXYZPoint directObsShift(0,0,0);
	directObsShift.setCovarElement(0,0,10.0);
	directObsShift.setCovarElement(1,1,10.0);
	directObsShift.setCovarElement(2,2,10.0);
	this->setDirectObservationForShift(directObsShift);


	// rotation
	CXYZPoint directObsRotation(0,0,0);
	directObsRotation.setCovarElement(0,0,1.4e-10);
	directObsRotation.setCovarElement(1,1,1.4e-5);
	directObsRotation.setCovarElement(2,2,1.4e-5);
	this->setDirectObservationForAngles(directObsRotation);

}

COrbitPathModel::COrbitPathModel(const COrbitPathModel& model)
	:CSplineModel(model),shift(model.shift),shiftObservation(model.shiftObservation),
	rotParameter(model.rotParameter),angleObservation(model.angleObservation),
	satH(model.satH),sigmaSatH(model.sigmaSatH),sigmaInnerProduct(model.sigmaInnerProduct),
	times(model.times)
{
	this->shift.setParent(this);
	this->shiftObservation.setParent(this);
	this->rotParameter.setParent(this);
}

COrbitPathModel::~COrbitPathModel(void)
{
}


COrbitPathModel &COrbitPathModel::operator = (const COrbitPathModel & orbitPath)
{
	CSplineModel::operator =(orbitPath);
	this->shift = orbitPath.shift;
	this->shiftObservation = orbitPath.shiftObservation;
	this->angleObservation = orbitPath.angleObservation;
	this->rotParameter = orbitPath.rotParameter;
	this->satH = orbitPath.satH;
	this->sigmaSatH = orbitPath.sigmaSatH;
	this->times = orbitPath.times;
	this->sigmaInnerProduct = orbitPath.sigmaInnerProduct;

	this->informListeners_elementsChanged(this->getClassName().c_str());

	return *this;
}



void COrbitPathModel::getName(CCharString& names) const
{
    names = "Orbit Path Model";
}


int COrbitPathModel::getDirectObsCount() const
{
	int n = 0;
	n += this->nActualParameter[rotPar] ? 3 : 0;
	n += this->nActualParameter[shiftPar] ? 3 : 0;
	n += this->getNDirectObservationsForSpline();
	n += this->useDirectObservationsForSpline ? (2 + this->times.size()) : 0;

	return n;
}


void COrbitPathModel::extend_N_and_t_Matrix(Matrix& N,Matrix& t,
											const CAdjustMatrix& AxTP,
											const CAdjustMatrix& AyTP, 
											const CAdjustMatrix& AzTP,
											const CAdjustMatrix& P,
											const CAdjustMatrix& dl,
											const int indexNx,
											const int indexNy,
											const int indexNz,
											const CObsPoint &obs)const
{
	int indexShift = this->parameterIndex[shiftPar];
	int indexRot = this->parameterIndex[rotPar];

	if (this->nActualParameter[shiftPar])
	{
		// Ashift = I;

		// add to N Matrix (AshiftT*P*Ashift) and t vector (Ashift*P*dl)
		for (int i = 0; i < 3; ++i)
		{
		    int row = indexShift + i;
			for (int j = 0; j < 3; ++j)
			{
				t.element(row, 0) += P(i,j) * dl(j,0);
			
				N.element(row, j + indexShift) += P(i,j);
			}
		}	

		// add Aspline*P*Ashift to N
		for (int i = 0; i < AxTP.getRows(); ++i)
		{
			int idxRX = indexNx + i;
			int idxRY = indexNy + i;
			int idxRZ = indexNz + i;

			for (int j = 0; j < 3; ++j)
			{
				int idxC = indexShift + j;
				N.element(idxRX, idxC) += AxTP(i,j);
				N.element(idxC, idxRX) += AxTP(i,j);
				N.element(idxRY, idxC) += AyTP(i,j);
				N.element(idxC, idxRY) += AyTP(i,j);
				N.element(idxRZ, idxC) += AzTP(i,j);
				N.element(idxC, idxRZ) += AzTP(i,j);
			}
		}
	}
	
	if (this->nActualParameter[rotPar])
	{
		CXYZOrbitPoint* test = (CXYZOrbitPoint*)&(obs);
		int rowSize;		
		if (test->getHasVelocity())
			rowSize = 6;	// both path and velocity of observation
		else
			rowSize = 3;	// just the observation

		// dA/dRotationMatrixElements
		CAdjustMatrix dAdF(rowSize,9,0.0);
		for (int i=0; i < 3; i++)
		{
			double value = -obs[i] + this->offsetSpline[i]; 
			dAdF(0,i) = value;
			dAdF(1,i + 3) = value;
			dAdF(2,i + 6) = value;
			if (rowSize == 6)
			{
				dAdF(3,i) = -obs[i + 3];
				dAdF(4,i + 3) = -obs[i + 3];
				dAdF(5,i + 6) = -obs[i + 3];
			}
		}

		// dF/dRotationAngles
		CAdjustMatrix dFdAngles(9,3);
		this->rotParameter.computeDerivation(dFdAngles);

		// dA/dRotationAngles
		CAdjustMatrix dAdAngles = dAdF*dFdAngles;

		CAdjustMatrix ArotationT = dAdAngles;
		ArotationT.transpose();
		CAdjustMatrix ArotationTP = ArotationT * P;

		// add to N Matrix (Arotation*P*Arotation) and t vector
		CAdjustMatrix ATPA = ArotationTP*dAdAngles; // ATPA
		ATPA.addToMatrix(indexRot,indexRot,N);
		ATPA = ArotationTP * dl;	// ATPdl
		ATPA.addToMatrix(indexRot,0,t);

		// add mixed part with spline
		ATPA = AxTP * dAdAngles;
		ATPA.addToMatrix(indexNx,indexRot,N,true);
		ATPA = AyTP * dAdAngles;
		ATPA.addToMatrix(indexNy,indexRot,N,true);
		ATPA = AzTP * dAdAngles;
		ATPA.addToMatrix(indexNz,indexRot,N,true);

		// the mixed part between shift and rotation
		if (this->nActualParameter[shiftPar])
		{
			for (int i=0; i < 3; i++)
				for (int k=0; k < 3; k++)
				{
					N.element(indexShift+i,indexRot+k) = ArotationTP(k,i);
					N.element(indexRot+i,indexShift+k) = ArotationTP(i,k);
				}
		}

	}
	

}

void COrbitPathModel::extend_Ai(Matrix &AiErp,Matrix &AiRot,Matrix &AiSca, 
								Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
								Matrix &AiAdd, Matrix &AiPoint,const Matrix &obs, const C3DPoint &approx)const
{
	int dimension = obs.Ncols()-1;

	if (this->nActualParameter[shiftPar])
	{
		AiErp.ReSize(dimension,this->nActualParameter[shiftPar]);
		AiErp = 0;
		for (int i=0; i < this->nActualParameter[shiftPar]; i++)
			AiErp.element(i,i) = 1.0;
	}

	if (this->nActualParameter[rotPar])
	{

		// dA/dRotationMatrixElements
		CAdjustMatrix dAdF(dimension,9,0.0);
		for (int i=0; i < 3; i++)
		{

			double value = -obs.element(0,i) + this->offsetSpline[i]; 
			dAdF(0,i) = value;
			dAdF(1,i + 3) = value;
			dAdF(2,i + 6) = value;
			if (dimension == 6)
			{
				dAdF(3,i) = -obs.element(0,i + 3);
				dAdF(4,i + 3) = -obs.element(0,i + 3);
				dAdF(5,i + 6) = -obs.element(0,i + 3);
			}
		}

		// dF/dRotationAngles
		CAdjustMatrix dFdAngles(9,3);
		this->rotParameter.computeDerivation(dFdAngles);

		// dA/dRotationAngles
		AiRot.ReSize(dAdF.getRows(),dFdAngles.getCols());
		for (int i = 0; i < dAdF.getRows(); i++)
		{
			for (int j = 0; j < dFdAngles.getCols(); j++)
			{
				AiRot.element(i,j) = 0;
				for (int k = 0; k < dAdF.getCols(); k++) 
				{
					AiRot.element(i,j) += dAdF(i,k) * dFdAngles(k,j);
				}
			}
		}		
	}

}

void COrbitPathModel::extend_Bi(Matrix& Bi,const Matrix &obs, const C3DPoint &approx)const
{
	int dimension =  obs.Ncols()-1;
	
	if (this->nActualParameter[rotPar])
	{
		const CRotationMatrix& rotMat = this->rotParameter.getRotMat();

		// fill BT Matrix
		for (int i=0; i < 3; i++)
		{
			for (int k=0; k < 3; k++)
			{
				Bi.element(i,k) = -rotMat(i,k);
				if (dimension == 6)
				{
					Bi.element(i+3,k+3) = -rotMat(i,k);
				}
			}
		}
		
	}	
}


void COrbitPathModel::computePMatrix(const Matrix& Qobs, CAdjustMatrix& P)const
{
	int dimension = Qobs.Nrows();
	
	if (this->nActualParameter[rotPar])
	{

		Matrix BT(dimension,dimension);
		Matrix B(dimension,dimension);
		BT=0.0;
		B=0.0;
		const CRotationMatrix& rotMat = this->rotParameter.getRotMat();

		// fill BT and B Matrix
		for (int i=0; i < 3; i++)
		{
			for (int k=0; k < 3; k++)
			{
				BT.element(i,k) = -rotMat(i,k);
				B.element(i,k) = -rotMat(k,i);
				if (dimension == 6)
				{
					BT.element(i+3,k+3) = -rotMat(i,k);
					B.element(i+3,k+3) = -rotMat(k,i);
				}
			}
		}

		Matrix Ptmp = (BT*Qobs*B).i();
		for (int i=0; i < Ptmp.Nrows(); i++)
			for (int k=0; k < Ptmp.Ncols(); k++)
				P(i,k) = Ptmp.element(i,k);

	}
	else
	{
		CSplineModel::computePMatrix(Qobs,P);
	}

}


bool COrbitPathModel::Fi(Matrix& Fi,const Matrix &obs, const C3DPoint &approx)const
{
	if (this->nActualParameter[rotPar])
	{
		int dimension = obs.Ncols()-1;
		double time = obs.element(0,dimension);
		Fi.ReSize(dimension, 1);

		// first evaluate the spline with the current parameter
		C3DPoint pos;
		this->spline.evaluateLocation(pos,time);	// evaluate the spline at t

		CObsPoint tmp(3);	// correct the spline offset
		for (int i=0; i < 3; i++)
			tmp[i] = obs.element(0,i) - offsetSpline[i];

		//correct the rotation
		C3DPoint erg;
		this->rotParameter.R_times_x(erg,tmp);

		// compute the residuals
		Fi.element(0,0) =   pos[0] - erg[0];
		Fi.element(1,0) =	pos[1] - erg[1];
		Fi.element(2,0) =   pos[2] - erg[2];

		if (dimension ==6)
		{
			C3DPoint velocity;
			this->spline.evaluateVelocity(velocity,time);	// evaluate the spline at t

			tmp[0] = obs.element(0,3);
			tmp[1] = obs.element(0,4);
			tmp[2] = obs.element(0,5);
			
			this->rotParameter.R_times_x(erg,tmp);

			Fi.element(3,0) = velocity[0] - erg[0];
			Fi.element(4,0) = velocity[1] - erg[1];
			Fi.element(5,0) = velocity[2] - erg[2];	

		}
	}
	else
	{
		CSplineModel::Fi(Fi,obs,approx);
	}


	if (this->nActualParameter[shiftPar])
	{
		Fi.element(0,0) += this->shift.x;
		Fi.element(1,0) += this->shift.y;
		Fi.element(2,0) += this->shift.z;	
	}

	return true;
}



bool COrbitPathModel::Fi(CObsPoint& fi, const CObsPoint &obs) const
{
	bool ret = false;
	if (this->nActualParameter[rotPar])
	{
		CXYZOrbitPoint* test = (CXYZOrbitPoint*)&(obs);
		int dimension;		
		if (test->getHasVelocity())
			dimension = 6;	// both path and velocity of observation
		else
			dimension = 3;	// just the observation


		// first evaluate the spline with the current parameter
		C3DPoint pos;
		ret = this->spline.evaluateLocation(pos,obs.dot);	// evaluate the spline at t

		if (!ret)
			return false;

		// correct the spline offset
		C3DPoint observation(obs[0]-offsetSpline[0],obs[1]-offsetSpline[1],obs[2]-offsetSpline[2]);

		C3DPoint erg;
		this->rotParameter.R_times_x(erg,observation);

		fi[0] = pos[0] - erg.x;
		fi[1] = pos[1] - erg.y;
		fi[2] = pos[2] - erg.z;

		if (dimension == 6)
		{
			C3DPoint velocity;
			this->spline.evaluateVelocity(velocity,obs.dot);	// evaluate the spline at t			
			
			observation.x = obs[3];
			observation.y = obs[4];
			observation.z = obs[5];

			this->rotParameter.R_times_x(erg,observation);
			
			fi[3] = velocity[0] - erg.x;
			fi[4] = velocity[1] - erg.y;
			fi[5] = velocity[2] - erg.z;		
		}
	}
	else
	{
		ret = CSplineModel::Fi(fi,obs);
	}


	if (this->nActualParameter[shiftPar])
	{
		fi[0] += this->shift.x;
		fi[1] += this->shift.y;
		fi[2] += this->shift.z;
	}

	return ret;
}

bool COrbitPathModel::backProjection(double &time,const C3DPoint& objPoint,const C3DPoint &n)
{
	C3DPoint correctedPoint = objPoint - this->offsetSpline;
	return this->spline.backProjection(time,correctedPoint,n);
}

void COrbitPathModel::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);
	
	CToken* token = pStructuredFileSection->addToken();
	token->setValue("actualShiftPar", this->nActualParameter[shiftPar]);	

	token = pStructuredFileSection->addToken();
	token->setValue("actualRotationPar", this->nActualParameter[rotPar]);

	token = pStructuredFileSection->addToken();
	token->setValue("fileName", this->FileName);

	token = pStructuredFileSection->addToken();
	token->setValue("useDirectObservationsForSpline", this->useDirectObservationsForSpline);

	token = pStructuredFileSection->addToken();
	token->setValue("scale", this->scale);

	token = pStructuredFileSection->addToken();
	token->setValue("satH", this->satH);

	token = pStructuredFileSection->addToken();
	token->setValue("sigmaSatH", this->sigmaSatH);

	token = pStructuredFileSection->addToken();
	token->setValue("sigmaInnerProduct", this->sigmaInnerProduct);

	token = pStructuredFileSection->addToken();
    token->setValue("nTimeObservations", (int)this->times.size());

	CCharString valueStr;
	CCharString elementStr;

	for (unsigned int i = 0; i < this->times.size(); i++)
		{
			double element = this->times[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	token = pStructuredFileSection->addToken();
	token->setValue("times", valueStr.GetChar());



	CStructuredFileSection* pNewSection = pStructuredFileSection->addChild();;
	offsetSpline.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild(); 
	shift.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild(); 
	this->shiftObservation.serializeStore(pNewSection);
	
	pNewSection = pStructuredFileSection->addChild(); 
	this->rotParameter.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild(); 
	this->angleObservation.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild(); 
	spline.serializeStore(pNewSection);

	if (this->useDirectObservationsForSpline)
	{	
		for (unsigned int i=0; i < this->directObservations.size(); i++)
		{
			pNewSection = pStructuredFileSection->addChild();
			this->directObservations[i].serializeStore(pNewSection);
		}
	}
}

void COrbitPathModel::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	bool readData = false;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("actualShiftPar"))
		{
			int par = token->getIntValue();
			if (par) 
				this->activateParameterGroup(shiftPar);
			else
				this->deactivateParameterGroup(shiftPar);
		}
		else if(key.CompareNoCase("actualRotationPar"))
		{
			int par = token->getIntValue();
			if (par) 
				this->activateParameterGroup(rotPar);
			else
				this->deactivateParameterGroup(rotPar);
		}
		else if(key.CompareNoCase("fileName"))
		{
			this->setFileName(token->getValue());
		}
		else if(key.CompareNoCase("useDirectObservationsForSpline"))
		{
			this->useDirectObservationsForSpline = token->getBoolValue();
		}
		else if(key.CompareNoCase("scale"))
		{
			this->setScale(token->getDoubleValue());
		}
		else if(key.CompareNoCase("satH"))
		{
			this->satH = token->getDoubleValue();
		}
		else if(key.CompareNoCase("sigmaSatH"))
		{
			this->sigmaSatH = token->getDoubleValue();
		}
		else if(key.CompareNoCase("sigmaInnerProduct"))
		{
			this->sigmaInnerProduct = token->getDoubleValue();
		}
		else if(key.CompareNoCase("nTimeObservations"))
		{
			
			int size = token->getIntValue();
			
			if (size >0)
			{
				this->times.resize(size);
				readData = true;
			}
			else
			{
				this->times.clear();
				readData = false;
			}

			
		}
		else if(readData && key.CompareNoCase("times"))
		{
			token->getDoublesClass(this->times);
		}

	}

	bool readShift = false;
	bool readShiftObservation = false;
	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("CObsPoint"))
			offsetSpline.serializeRestore(pChild);
		else if (pChild->getName().CompareNoCase("CSpline"))
			spline.serializeRestore(pChild);
		else if (pChild->getName().CompareNoCase("CXYZPoint") && !readShift && !readShiftObservation)
		{
			readShift = true;		
			shift.serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase("CXYZPoint") && readShift && !readShiftObservation)
		{
			readShiftObservation = true;
			this->shiftObservation.serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase("CXYZPoint") && readShift && readShiftObservation)
		{
			this->angleObservation.serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase("CRollPitchYawRotation"))
		{
			this->rotParameter.serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase("CDirectSplineObservation"))
		{
			CDirectSplineObservation obs;
			obs.serializeRestore(pChild);
			this->directObservations.push_back(obs);
		}
	}

	this->computeParameterCount();
}

void COrbitPathModel::reconnectPointers()
{
	CSerializable::reconnectPointers();
	offsetSpline.reconnectPointers();
	spline.reconnectPointers();
	shift.reconnectPointers();
	this->shiftObservation.reconnectPointers();
	this->rotParameter.reconnectPointers();
	this->angleObservation.reconnectPointers();

}

void COrbitPathModel::resetPointers()
{
	CSerializable::resetPointers();
	offsetSpline.resetPointers();
	spline.resetPointers();
	shift.resetPointers();
	this->shiftObservation.resetPointers();
	this->rotParameter.resetPointers();
	this->angleObservation.resetPointers();
}


bool COrbitPathModel::getAllParameters(Matrix &parms) const
{
	int size = CSplineModel::getParameterCount(addPar) + this->nActualParameter[shiftPar] + this->nActualParameter[rotPar];
	parms.ReSize(size,1);
	
	int index = 0;

	if (this->nActualParameter[shiftPar])
	{
		parms.element(index     ,0) = this->shift.x;
		parms.element(index + 1 ,0) = this->shift.y;
		parms.element(index + 2 ,0) = this->shift.z;

		index += 3;
	}

	if (this->nActualParameter[rotPar])
	{
		parms.element(index     ,0) = this->rotParameter[0];
		parms.element(index + 1 ,0) = this->rotParameter[1];
		parms.element(index + 2 ,0) = this->rotParameter[2];

		index += 3;
	}

	return CSplineModel::getAllParameters(parms);
}

bool COrbitPathModel::getAdjustableParameters(Matrix &parms) const
{
	int size = CSplineModel::getParameterCount(addPar) + this->nActualParameter[shiftPar] + this->nActualParameter[rotPar];
	parms.ReSize(size,1);
	
	int index = 0;

	if (this->nActualParameter[shiftPar])
	{
		parms.element(index     ,0) = this->shift.x;
		parms.element(index + 1 ,0) = this->shift.y;
		parms.element(index + 2 ,0) = this->shift.z;

		index += 3;

	}

	if (this->nActualParameter[rotPar])
	{
		parms.element(index     ,0) = this->rotParameter[0]*180/M_PI;
		parms.element(index + 1 ,0) = this->rotParameter[1]*180/M_PI;
		parms.element(index + 2 ,0) = this->rotParameter[2]*180/M_PI;

		index += 3;
	}

	return CSplineModel::getAdjustableParameters(parms);
}

bool COrbitPathModel::insertParametersForAdjustment(Matrix &parms)
{
	int addIndex = this->parameterIndex[addPar];
	this->spline.insertParametersForAdjustment(parms,addIndex);

	if (this->nActualParameter[shiftPar])
	{
		int shiftIndex = this->parameterIndex[shiftPar];
		parms.element(shiftIndex,0) = this->shift.x;
		parms.element(shiftIndex + 1,0) = this->shift.y;
		parms.element(shiftIndex + 2,0) = this->shift.z;
	}

	if (this->nActualParameter[rotPar])
	{
		int rotIndex = this->parameterIndex[rotPar];
		parms.element(rotIndex,0) = this->rotParameter[0];
		parms.element(rotIndex + 1,0) = this->rotParameter[1];
		parms.element(rotIndex + 2,0) = this->rotParameter[2];
	}

	return true;
}

bool COrbitPathModel::setParametersFromAdjustment(const Matrix &parms)
{
	int addIndex = this->parameterIndex[addPar];
	this->spline.setParametersFromAdjustment(parms,addIndex);

	if (this->nActualParameter[shiftPar])
	{
		int shiftIndex = this->parameterIndex[shiftPar];
		this->shift.x = parms.element(shiftIndex,0);
		this->shift.y = parms.element(shiftIndex + 1,0);
		this->shift.z = parms.element(shiftIndex + 2,0);
	}

	if (this->nActualParameter[rotPar])
	{
		int rotIndex = this->parameterIndex[rotPar];
		this->rotParameter[0] = parms.element(rotIndex,0);
		this->rotParameter[1] = parms.element(rotIndex + 1,0);
		this->rotParameter[2] = parms.element(rotIndex + 2,0);
		this->rotParameter.computeRotMatFromParameter();
	}

	return true;
}

void COrbitPathModel::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
	if (this->nActualParameter[shiftPar])
	{
		StationNames.Add("Orbit Path Shift             Parameter[m]	       Sigma[m]\n      "
						 "                    X ");
		StationNames.Add("                    Y ");
		StationNames.Add("                    Z ");
	}

	if (this->nActualParameter[rotPar])
	{
		StationNames.Add("Orbit Path Rotation          Parameter[�]	       Sigma[�]\n      "
						 "                 Roll ");
		StationNames.Add("                Pitch ");
		StationNames.Add("                  Yaw ");
	}

	CSplineModel::getParameterNames(StationNames,PointNames);
}



void COrbitPathModel::addDirectObservations(Matrix &N, Matrix &t)
{

	if (this->nActualParameter[shiftPar])
	{
		int indexShift = this->parameterIndex[shiftPar];
		Matrix Q = *this->shiftObservation.getCovariance();
		Matrix P = Q.i();

		Matrix g(3,1);
		g.element(0,0) = this->shift[0] - this->shiftObservation[0];
		g.element(1,0) = this->shift[1] - this->shiftObservation[1];
		g.element(2,0) = this->shift[2] - this->shiftObservation[2];

		Matrix ATPl = P * g;

		this->addMatrix(indexShift,indexShift,N,P);
		this->addMatrix(indexShift,0,t,ATPl);
	}

	if (this->nActualParameter[rotPar])
	{
		int indexRot = this->parameterIndex[rotPar];
		Matrix Q = *this->angleObservation.getCovariance();
		Matrix P = Q.i();

		Matrix g(3,1);
		g.element(0,0) = this->rotParameter[0] - this->angleObservation[0];
		g.element(1,0) = this->rotParameter[1] - this->angleObservation[1];
		g.element(2,0) = this->rotParameter[2] - this->angleObservation[2];

		Matrix ATPl = P * g;

		this->addMatrix(indexRot,indexRot,N,P);
		this->addMatrix(indexRot,0,t,ATPl);
	}



	if (this->useDirectObservationsForSpline && this->spline.getNumberOfSegments() == 1)
	{
		// this assumes that the path has been initialized with one segment and
		// that the scene is recorded between 0.0 - 1.0 s
		// the degree of the spline has to be 3!

		//#############################################################################
		// two observations for orbit path height
		//#############################################################################

		//f(t) = ||path(t)||^2 - (satH + v)^2 = 0.0 
		//f(t) = ||path(t)||^2 - satH^2 - 2*satH*v = 0.0 // we ignore v^2!
		//f(t) = (||path(t)||^2)/2*satH - satH/2 - v = 0.0
		
		int nCoefficients = this->spline.getDegree() + 1;
		int indexX = 0;
		int indexY = indexX + nCoefficients;
		int indexZ = indexY + nCoefficients;

		Matrix P(2,2);
		P.element(0,0) = 1.0 / (this->sigmaSatH * this->sigmaSatH);
		P.element(1,1) = P.element(0,0);
		P.element(0,1) = 0.0;
		P.element(1,0) = 0.0;

		Matrix dl(2,1);
		Matrix A(2,nCoefficients * 3);
		A = 0.0;

		// first line: f(t) = (||path(0.0)||^2)/2*satH - satH/2 - v = 0.0  with t = 0.0
		C3DPoint location0;
		this->evaluateLocation(location0,0.0);

		A.element(0,indexX) = location0.x / this->satH; // df/da0
		A.element(0,indexY) = location0.y / this->satH; // df/db0
		A.element(0,indexZ) = location0.z / this->satH; // df/dc0

		double length = location0.getNorm();
		dl.element(0,0) = length*length / (this->satH*2.0) - this->satH/2.0;

		// second line: f(t) = (||path(1.0)||^2)/2*satH - satH/2 - v = 0.0  with t = 1.0
		C3DPoint location1;
		this->evaluateLocation(location1,1.0);
		location1 /= this->satH; 

		A.element(1,indexX) = location1.x; // df/da0
		A.element(1,indexX + 1) = location1.x; // df/da1
		A.element(1,indexX + 2) = location1.x; // df/da2
		A.element(1,indexY) = location1.y; // df/db0
		A.element(1,indexY + 1) = location1.y; // df/db1
		A.element(1,indexY + 2) = location1.y; // df/db2
		A.element(1,indexZ) = location1.z; // df/dc0	
		A.element(1,indexZ + 1) = location1.z; // df/dc1	
		A.element(1,indexZ + 2) = location1.z; // df/dc2	

		location1 *= this->satH; // the location in m
		length = location1.getNorm();
		dl.element(1,0) = length*length /(this->satH*2.0) - this->satH/2.0;

		Matrix ATP = A.t() * P; 
		Matrix ATPA =  ATP * A;
		Matrix ATPdl = ATP * dl;

		this->addMatrix(this->parameterIndex[addPar],this->parameterIndex[addPar],N,ATPA);
		this->addMatrix(this->parameterIndex[addPar],0,t,ATPdl);
	
		//#############################################################################
		// observations to force orbit path to be in a planar
		//#############################################################################
		
		// a = vector to first point on spline (=location0)
		// b = vector to last point on spline (=location1)
		// n = normal vector for orbit path planar = a x b
		// t = tangential vector on spline at time t 
		// inner product s = t . n = 0
		C3DPoint n = location0.crossProduct(location1);
		n /= n.getNorm();

		double a0,b0,c0,a1,b1,c1,a2,b2,c2; // the spline parameter
		this->spline.getSplineParameter(0,0,a0,b0,c0);
		this->spline.getSplineParameter(0,1,a1,b1,c1);
		this->spline.getSplineParameter(0,2,a2,b2,c2);

		double x0 = this->offsetSpline[0];
		double y0 = this->offsetSpline[1];
		double z0 = this->offsetSpline[2];

		
		P.ReSize(this->times.size(),this->times.size());
		P = 0.0;

		dl.ReSize(this->times.size(),1);
		A.ReSize(this->times.size(),nCoefficients * 3);

		// compute the partials 
		double a0x0 = a0+x0;
		double b0y0 = b0+y0;
		double c0z0 = c0+z0;
		double a1a2 = a1+a2;
		double b1b2 = b1+b2;
		double c1c2 = c1+c2;

		double aa = b1b2*c0z0 - c1c2*b0y0;
		double bb = c1c2*a0x0 - a1a2*c0z0;
		double cc = a1a2*b0y0 - b1b2*a0x0;
	
		double denominator = pow(aa*aa + bb*bb + cc*cc,3.0/2.0);

		double degree0 = a2*a0x0*(b1*b1b2 + c1*c1c2) +
						 aa*(b1*c2 - b2*c1)-
						 a2*a2*(b1*b0y0 + c1*c0z0) + a1*a1*(b2*b0y0 + c2*c0z0) -
						 a1 *((b2*b1b2 + c2*c1c2)*a0x0 + a2*(b0y0*(b1-b2) + c0z0*(c1-c2)));

		degree0 /= denominator;

		double degree1 =   a0*a0*b1*b2 +   a0*a0*b2*b2 +   b1*b2*c0*c0 +   b2*b2*c0*c0 -   b0*b2*c0*c1 -   b0*b1*c0*c2 -
						 2*b0*b2*c0*c2 +   a0*a0*c1*c2 +   b0*b0*c1*c2 +   a0*a0*c2*c2 +   b0*b0*c2*c2 + 2*a0*b1*b2*x0 +
						 2*a0*b2*b2*x0 + 2*a0*c1*c2*x0 + 2*a0*c2*c2*x0 +   b1*b2*x0*x0 +   b2*b2*x0*x0 +   c1*c2*x0*x0 +
						   c2*c2*x0*x0 -   b2*c0*c1*y0 -   b1*c0*c2*y0 - 2*b2*c0*c2*y0 + 2*b0*c1*c2*y0 + 2*b0*c2*c2*y0 +
						   c1*c2*y0*y0 +   c2*c2*y0*y0 +
						   (b1*(2*b2*c0 - c2*b0y0) + b2*(2*b2*c0-(c1+2*c2)*b0y0))*z0 +
						   b2*(b1 + b2)*z0*z0 - 
						   a2*a0x0*((b1+2*b2)*b0y0 + (c1 + 2*c2)*c0z0) +
						   a2*a2*(b0y0*b0y0 + c0z0*c0z0) +
						   a1*(a2*(b0y0*b0y0 + c0z0*c0z0) - a0x0*(b2*b0y0 + c2*c0z0));

		degree1 /= denominator;

		double degree2 =  b1*b1*c0*c0 + b1*b1*c0*c0 - 2*b0*b1*c0*c1 - b0*b2*c0*c1 + b0*b0*c1*c1 - b0*b1*c0*c2 +
						  b0*b0*c1*c2 + a0*a0*(b1*(b1 + b2) + c1*(c1+c2)) - a2*b0*b1*x0 - a2*c0*c1*x0 +
						  b1*b1*x0*x0 + b1*b2*x0*x0 + c1*c1*x0*x0 + c1*c2*x0*x0 - 2*b1*c0*c1*y0 - b2*c0*c1*y0 +
						  2*b0*c1*c1*y0 - b1*c0*c2*y0 + 2*b0*c1*c2*y0 - a2*b1*x0*y0 + c1*c1*y0*y0 + c1*c2*y0*y0 +
						  (2*b1*b1*c0 - c1*(a2*x0 + b2*b0y0) +b1*(2*b2*c0 - (2*c1+c2)*b0y0))*z0 +
						  b1*z0*z0*(b1+b2) + a1*a1*(b0y0*b0y0 + c0z0*c0z0) -
						  a0*(-2*(b1*(b1+b2) + c1*(c1+c2))*x0 + a2*(b1*b0y0 + c1*c0z0)) +
						  a1*(-a0x0*((2*b1+b2)*b0y0 + (2*c1+c2)*c0z0) + a2*(b0y0*b0y0 + c0z0*c0z0));

		degree2 /= denominator;

		for (unsigned int i=0; i < this->times.size(); i++)
		{
			C3DPoint t(a1 + 2.0 * a2*this->times[i],b1 +  2.0 * b2*this->times[i],c1 +  2.0 * c2*this->times[i]);
			double lengthT = t.getNorm();
			dl.element(i,0) = t.innerProduct(n); //dl = f(x0) - l, with l =0.0

			// computes P = (BT*Q*B)^-1
			P.element(i,i) = 1.0 / (lengthT * lengthT * this->sigmaInnerProduct * this->sigmaInnerProduct);

			A.element(i,indexX) = (2*this->times[i]-1)* aa * degree0; // ds/da0
			A.element(i,indexY) = (2*this->times[i]-1)* bb * degree0; // ds/db0
			A.element(i,indexZ) = (2*this->times[i]-1)* cc * degree0; // ds/dc0
			
			A.element(i,indexX + 1) = (2*this->times[i]-1)* aa * degree1; // ds/da1
			A.element(i,indexY + 1) = (2*this->times[i]-1)* bb * degree1; // ds/db1
			A.element(i,indexZ + 1) = (2*this->times[i]-1)* cc * degree1; // ds/dc1

			A.element(i,indexX + 2) = (2*this->times[i]-1)* -aa * degree2; // ds/da2
			A.element(i,indexY + 2) = (2*this->times[i]-1)* -bb * degree2; // ds/db2
			A.element(i,indexZ + 2) = (2*this->times[i]-1)* -cc * degree2; // ds/dc2
		}

		// add to N and t matrix
		ATP = A.t() * P; 
		ATPA =  ATP * A;
		ATPdl = ATP * dl;

		this->addMatrix(this->parameterIndex[addPar],this->parameterIndex[addPar],N,ATPA);
		this->addMatrix(this->parameterIndex[addPar],0,t,ATPdl);

		//#############################################################################
		// observations to force orbit path tangent to be perpendicular to path
		//#############################################################################
		// location = position vector at time t
		// t = tangential vector on spline at time t 
		// inner product s = t . location = 0



		// just to make sure we have the correct size
		P.ReSize(this->times.size(),this->times.size());
		P = 0.0;

		dl.ReSize(this->times.size(),1);
		A.ReSize(this->times.size(),nCoefficients * 3);


		C3DPoint location;
		for (unsigned int i=0; i < this->times.size(); i++)
		{
			C3DPoint t(a1 + 2.0 * a2*this->times[i],b1 +  2.0 * b2*this->times[i],c1 +  2.0 * c2*this->times[i]);
			
			if (!this->evaluateLocation(location,this->times[i]))
				continue;



			dl.element(i,0) = t.innerProduct(location); //dl = f(x0) - l, with l =0.0
	

			double scale = t.getNorm() * location.getNorm() ;
			scale *= scale;

			// computes P = (BT*Q*B)^-1
			double sigma =   0.035;
			P.element(i,i) = 1.0 / (scale * sigma * sigma);

			A.element(i,indexX) = a1 + 2* a2* this->times[i]; // ds/da0
			A.element(i,indexY) = b1 + 2* b2* this->times[i]; // ds/db0
			A.element(i,indexZ) = c1 + 2* c2* this->times[i]; // ds/dc0
			
			A.element(i,indexX + 1) = a0 + 2*a1*this->times[i] + 3*a2*this->times[i]*this->times[i] + x0; // ds/da1
			A.element(i,indexY + 1) = b0 + 2*b1*this->times[i] + 3*b2*this->times[i]*this->times[i] + y0; // ds/db1
			A.element(i,indexZ + 1) = c0 + 2*c1*this->times[i] + 3*c2*this->times[i]*this->times[i] + z0; // ds/dc1

			A.element(i,indexX + 2) = this->times[i] *(2*a0 + 3*a1*this->times[i] + 4*a2*this->times[i]*this->times[i] + 2*x0); // ds/da2
			A.element(i,indexY + 2) = this->times[i] *(2*b0 + 3*b1*this->times[i] + 4*b2*this->times[i]*this->times[i] + 2*y0); // ds/db2
			A.element(i,indexZ + 2) = this->times[i] *(2*c0 + 3*c1*this->times[i] + 4*c2*this->times[i]*this->times[i] + 2*z0); // ds/dc2
		}


		// add to N and t matrix
		ATP = A.t() * P; 
		ATPA =  ATP * A;
		ATPdl = ATP * dl;

		this->addMatrix(this->parameterIndex[addPar],this->parameterIndex[addPar],N,ATPA);
		this->addMatrix(this->parameterIndex[addPar],0,t,ATPdl);

	}


	CSplineModel::addDirectObservations(N,t);
}



double COrbitPathModel::getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res)
{
	double value = 0.0;

	int index = 0;


	if (this->nActualParameter[shiftPar])
	{
		res.ReSize(3,1);
		res.element(0,0) = - deltaPar.element(index + 0,0) + this->shift[0];
		res.element(1,0) = - deltaPar.element(index + 1,0) + this->shift[1];
		res.element(2,0) = - deltaPar.element(index + 2,0) + this->shift[2];
		
		Matrix Q = *this->shiftObservation.getCovariance();
		Matrix P = Q.i();

		Matrix vtv = res.t() * P * res;
		value += vtv.element(0,0);

		index +=3;
	}

	if (this->nActualParameter[rotPar])
	{
		res.ReSize(3,1);
		res.element(0,0) = - deltaPar.element(index + 0,0) + this->rotParameter[0];
		res.element(1,0) = - deltaPar.element(index + 1,0) + this->rotParameter[1];
		res.element(2,0) = - deltaPar.element(index + 2,0) + this->rotParameter[2];
		
		Matrix Q = *this->angleObservation.getCovariance();
		Matrix P = Q.i();

		Matrix vtv = res.t() * P * res;
		value += vtv.element(0,0);

		index +=3;
	}


	if (this->useDirectObservationsForSpline&& this->spline.getNumberOfSegments() == 1)
	{
		// this assumes that the path has been initialized with one segment and
		// that the scene is recorded between 0.0 - 1.0 s

		//#############################################################################
		// two observations for orbit path height
		//#############################################################################

		//f(t) = ||path(t)||^2 - (satH + v)^2 = 0.0 
		//f(t) = ||path(t)||^2 - satH^2 - 2*satH*v = 0.0 // we ignore v^2!
		//f(t) = (||path(t)||^2)/2*satH - satH/2 - v = 0.0

		res.ReSize(2,1);
		Matrix P(2,2);
		P.element(0,0) = 1.0 / (this->sigmaSatH * this->sigmaSatH);

		P.element(1,1) = P.element(0,0);
		P.element(0,1) = 0.0;
		P.element(1,0) = 0.0;


		// first line: f(t)= (||path(0.0)||^2)/2*satH - satH/2 - v = 0.0  with t = 0.0
		C3DPoint location0;
		this->evaluateLocation(location0,0.0);
		double length = location0.getNorm();
		res.element(0,0) = -length*length / (this->satH * 2.0)  + this->satH / 2.0;

		// second line: f(t) = (||path(1.0)||^2)/2*satH - satH/2 - v = 0.0  with t = 1.0
		C3DPoint location1;
		this->evaluateLocation(location1,1.0);
		length = location1.getNorm();
		res.element(1,0) = -length*length / (this->satH * 2.0)  + this->satH / 2.0;

		Matrix vtv = res.t() * P * res;
		value += vtv.element(0,0);
	

		//#############################################################################
		// observations to force orbit path to be in a planar
		//#############################################################################
		
		// a = vector to first point on spline (=location0)
		// b = vector to last point on spline (=location1)
		// n = normal vector for orbit path planar = a x b
		// t = tangential vector on spline at time t 
		// inner product s = t . n = 0


		C3DPoint n = location0.crossProduct(location1);
		n /= n.getNorm();


		double a1,b1,c1,a2,b2,c2; // the spline parameter
		this->spline.getSplineParameter(0,1,a1,b1,c1);
		this->spline.getSplineParameter(0,2,a2,b2,c2);

		res.ReSize(this->times.size(),1);
		P.ReSize(this->times.size(),this->times.size());
		P = 0.0;
		for (unsigned int i=0; i < this->times.size(); i++)
		{
			C3DPoint t(a1 + 2.0 * a2*this->times[i],b1 + 2.0 * b2*this->times[i],c1 + 2.0 * c2*this->times[i]);
			double lengthT = t.getNorm();
			res.element(i,0) = t.innerProduct(n); //v = f(x0) + dx - l, with l =0.0
			
			// computes P = (BT*Q*B)^-1
			P.element(i,i) = 1.0 / (lengthT * lengthT * this->sigmaInnerProduct * this->sigmaInnerProduct);
		}

		vtv = res.t() * P * res;
		value += vtv.element(0,0);


		//#############################################################################
		// observations to force orbit path tangent to be perpendicular to path
		//#############################################################################
		// location = position vector at time t
		// t = tangential vector on spline at time t 
		// inner product s = t . location = 0


		res.ReSize(this->times.size(),1);
		P.ReSize(this->times.size(),this->times.size());
		P = 0.0;

		C3DPoint location;
		for (unsigned int i=0; i < this->times.size(); i++)
		{
			C3DPoint t(a1 + 2.0 * a2*this->times[i],b1 + 2.0 * b2*this->times[i],c1 + 2.0 * c2*this->times[i]);
			
			if (!this->evaluateLocation(location,this->times[i]))
				continue;
	

			
			double scale = t.getNorm() * location.getNorm() ;
			scale *= scale;

			res.element(i,0) = t.innerProduct(location); //v = f(x0) + dx - l, with l =0.0
			
			// computes P = (BT*Q*B)^-1
			double sigma = 0.035;
			P.element(i,i) = 1.0 / (scale * sigma * sigma);
		}

		vtv = res.t() * P * res;
		value += vtv.element(0,0);
	}


	value += CSplineModel::getResidualsDirectObservations(deltaPar,res);

	return value;
};

void COrbitPathModel::setSatelliteHeight(double satH,double sigmaSatH)
{
	this->satH = satH;
	this->sigmaSatH = sigmaSatH;
}


void COrbitPathModel::setTimesForDirectObservations(doubles times,double sigmaInnerProduct)
{
	this->times = times;
	this->sigmaInnerProduct = sigmaInnerProduct;
}



void COrbitPathModel::getObservationNames(CCharStringArray &names) const
{
	names.RemoveAll();
    
    CCharString* name = names.Add();
    *name = "X";
	name = names.Add();
	*name = "Y";
	name = names.Add();
	*name = "Z";
	name = names.Add();
    *name = "VX";
	name = names.Add();
	*name = "VY";
	name = names.Add();
	*name = "VZ";
}

int COrbitPathModel::coordPrecision() const
{
	return 3;
};

void COrbitPathModel::setCovariance(const Matrix &covar)
{
	int index = 0;

	if (this->nActualParameter[shiftPar])
	{
		Matrix* shiftCovar = this->shift.getCovariance();
		*shiftCovar = covar.SubMatrix(index+1,index + 3,index+1,index+3);

		index += 3;
	}

	if (this->nActualParameter[rotPar])
	{
		Matrix* rotCovar = this->rotParameter.getCovariance();
		*rotCovar = covar.SubMatrix(index+1,index + 3,index+1,index+3);	

		index += 3;
	}

	this->covariance = covar;

	this->informListeners_elementsChanged(this->getClassName().c_str());


}

void COrbitPathModel::setDirectObservationForShift(const CXYZPoint& obs)
{
	this->shiftObservation = obs;
	this->informListeners_elementsChanged(this->getClassName().c_str());
}

void COrbitPathModel::setShift(CXYZPoint& shift)
{
	this->shift = shift; 
	this->informListeners_elementsChanged(this->getClassName().c_str());
}

void COrbitPathModel::setRotation(const CXYZPoint& rot)
{
	this->rotParameter.updateAngles_Rho(&rot);
	this->informListeners_elementsChanged(this->getClassName().c_str());
}

void COrbitPathModel::setDirectObservationForAngles(CXYZPoint& obs)
{
	this->angleObservation = obs;
	this->informListeners_elementsChanged(this->getClassName().c_str());


}

void COrbitPathModel::printStationParameter(CCharString& output,CRotationMatrix* rotMat)const
{
	ostrstream protocol;

	int widthParameterName = 12;
	int widthParameter = 15;
	int widthSigma = 15;

	int precisionParameter = 2;
	int precisionSigma = 2;

	protocol.setf(ios::fixed, ios::floatfield);
	int parameterOffset = 0;
	
	if (this->nActualParameter[shiftPar])
	{
		parameterOffset +=3;
		

		if (rotMat)
		{
			CXYZPoint tmpShiftOrbit;
			rotMat->RT_times_x(tmpShiftOrbit,this->shift);

			Matrix rot(3,3);
			for (int i=0; i<3; i++)
				for (int k=0; k< 3; k++)
					rot.element(i,k) = (*rotMat)(i,k);
			
			Matrix Qxx = rot.t() * this->shift.getCovar() * rot;
			CXYZPoint tmpShiftSigmaOrbit(sqrt(Qxx.element(0,0)),
									sqrt(Qxx.element(1,1)),
									sqrt(Qxx.element(2,2)));


			protocol << endl << "      Orbit Path Shift        Parameter[m]	       Sigma[m] (in orbital system)\n";
			protocol.width(widthParameterName); 
			protocol << "                    X :";
			protocol.width(widthParameter);
			protocol.precision(precisionParameter);
			protocol << tmpShiftOrbit.x;
			protocol << " +- ";
			protocol.width(widthSigma);
			protocol.precision(precisionSigma);
			protocol << tmpShiftSigmaOrbit.x << endl;
		
			protocol.width(widthParameterName); 
			protocol << "                    Y :";
			protocol.width(widthParameter);
			protocol.precision(precisionParameter);
			protocol << tmpShiftOrbit.y;
			protocol << " +- ";
			protocol.width(widthSigma);
			protocol.precision(precisionSigma);
			protocol << tmpShiftSigmaOrbit.y << endl;

			protocol.width(widthParameterName); 
			protocol << "                    Z :";
			protocol.width(widthParameter);
			protocol.precision(precisionParameter);
			protocol << tmpShiftOrbit.z;
			protocol << " +- ";
			protocol.width(widthSigma);
			protocol.precision(precisionSigma);
			protocol << tmpShiftSigmaOrbit.z << endl;
		
		}

		protocol << endl << "      Orbit Path Shift        Parameter[m]	       Sigma[m] (in global reference system)\n";
		protocol.width(widthParameterName); 
		protocol << "                    X :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->shift.x;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << this->shift.getSigma(0) << endl;
	
		protocol.width(widthParameterName); 
		protocol << "                    Y :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->shift.y;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << this->shift.getSigma(2) << endl;

		protocol.width(widthParameterName); 
		protocol << "                    Z :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->shift.z;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << this->shift.getSigma(2) << endl;

	
	}

	if (this->nActualParameter[rotPar])
	{
		parameterOffset +=3;


		if (rotMat)
		{
			
			CXYZPoint tmpRotOrbit;
			rotMat->RT_times_x(tmpRotOrbit,this->rotParameter);
			
			Matrix rot(3,3);
			for (int i=0; i<3; i++)
				for (int k=0; k< 3; k++)
					rot.element(i,k) = (*rotMat)(i,k);
			
			Matrix Qxx = rot.t() * this->rotParameter.getCovar() * rot;
			CXYZPoint tmpRotSigmaOrbit(sqrt(Qxx.element(0,0)),
									sqrt(Qxx.element(1,1)),
									sqrt(Qxx.element(2,2)));

			protocol << endl <<  "      Orbit Path Rotation      Parameter[sec]	       Sigma[sec] (in orbital system)\n";
			protocol.width(widthParameterName); 
			protocol << "                 Roll :";
			protocol.width(widthParameter);
			protocol.precision(precisionParameter);
			protocol << tmpRotOrbit.x * 3600 *  180.0 / M_PI;
			protocol << " +- ";
			protocol.width(widthSigma);
			protocol.precision(precisionSigma);
			protocol << tmpRotSigmaOrbit.x * 3600 * 180.0 / M_PI << endl;


			protocol.width(widthParameterName); 
			protocol << "                Pitch :";
			protocol.width(widthParameter);
			protocol.precision(precisionParameter);
			protocol << tmpRotOrbit.y * 3600 * 180.0 / M_PI;
			protocol << " +- ";
			protocol.width(widthSigma);
			protocol.precision(precisionSigma);
			protocol << tmpRotSigmaOrbit.z * 3600 * 180.0 / M_PI << endl;

			protocol.width(widthParameterName); 
			protocol << "                  Yaw :";
			protocol.width(widthParameter);
			protocol.precision(precisionParameter);
			protocol << tmpRotOrbit.z * 3600 * 180.0 / M_PI;
			protocol << " +- ";
			protocol.width(widthSigma);
			protocol.precision(precisionSigma);
			protocol << tmpRotSigmaOrbit.z * 3600 * 180.0 / M_PI << endl;


		}

		protocol << endl <<  "      Orbit Path Rotation      Parameter[sec]	       Sigma[sec] (in global reference system)\n";
		protocol.width(widthParameterName); 
		protocol << "                 Roll :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->rotParameter[0] * 3600 *  180.0 / M_PI;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << this->rotParameter.getSigma(0)* 3600 * 180.0 / M_PI << endl;


		protocol.width(widthParameterName); 
		protocol << "                Pitch :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->rotParameter[1]  * 3600 * 180.0 / M_PI;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << this->rotParameter.getSigma(1) * 3600 * 180.0 / M_PI << endl;

		protocol.width(widthParameterName); 
		protocol << "                  Yaw :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->rotParameter[2]  * 3600 * 180.0 / M_PI;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << this->rotParameter.getSigma(2) * 3600 * 180.0 / M_PI << endl;

	}

	
	CCharStringArray StationNames, PointNames;
	this->getParameterNames(StationNames, PointNames);
	Matrix ModelPars;
	this->getAdjustableParameters(ModelPars);
	protocol.precision(precisionParameter);
	for (int j = parameterOffset; j < ModelPars.Nrows(); ++j)
	{
		CCharString *parName = StationNames.GetAt(j);
		protocol << "\n      ";
		protocol.width(5); 
		protocol << parName->GetChar() << ": ";
		protocol.width(widthParameter); 
		CObsPoint oldUnit(2);
		CObsPoint newUnit(2);
		oldUnit[0] = ModelPars.element(j,0);
		oldUnit[1] = sqrt(this->covariance.element(j,j));
		this->convertToDifferentialUnits(newUnit,oldUnit);
		protocol << newUnit[0] << " +- ";
		protocol.width(widthSigma); 
		protocol << newUnit[1] ;
	}


	

	protocol << ends;
	char* s = protocol.str();
	output = s;
	delete [] s;
	

}


void COrbitPathModel::initDirectObservations()
{
	// this initializes the vector
	CSplineModel::initDirectObservations();
	

	// now we set the right values for this sensor model
	double sigmaLinear = 1000;
	double sigmaQuadratic = 5;

	int segments = this->spline.getNumberOfSegments();
	
	doubles observationsX(segments);
	doubles observationsY(segments);
	doubles observationsZ(segments);
	doubles sigmasX(segments);
	doubles sigmasY(segments);
	doubles sigmasZ(segments);

	// activate the linear coefficients
	for (int i=0; i < segments; i++)
	{
		this->spline.getSplineParameter(i,1,observationsX[i],
										    observationsY[i],
										    observationsZ[i]);

		sigmasX[i] = sigmaLinear;
		sigmasY[i] = sigmaLinear;
		sigmasZ[i] = sigmaLinear;
	}

	this->setDirectObservationsForSpline(1,0,observationsX,sigmasX);
	this->setDirectObservationsForSpline(1,1,observationsY,sigmasY);
	this->setDirectObservationsForSpline(1,2,observationsZ,sigmasZ);
	

	//activate the quadratic coefficients
	for (int i=0; i < segments; i++)
	{
		this->spline.getSplineParameter(i,2,observationsX[i],
										    observationsY[i],
										    observationsZ[i]);

		sigmasX[i] = sigmaQuadratic;
		sigmasY[i] = sigmaQuadratic;
		sigmasZ[i] = sigmaQuadratic;
	}

	this->setDirectObservationsForSpline(2,0,observationsX,sigmasX);
	this->setDirectObservationsForSpline(2,1,observationsY,sigmasY);
	this->setDirectObservationsForSpline(2,2,observationsZ,sigmasZ);

	// direct observations for inner product in order to force the orbit path into a planar
	int nObs = 5;
	doubles times(nObs);
	
	for (int i=0; i<nObs; i++)
		times[i] = (float(i)+ 1.0)/float(nObs+1);
	
	this->setTimesForDirectObservations(times,0.00001);

	this->informListeners_elementsChanged(this->getClassName().c_str());
}
 