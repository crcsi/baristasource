/**
 * @class CSensorModel
 * virtual base class for semsor models
 * concerning import/export, sensor modeling
 */
#include <float.h>
#include "newmat.h"
#include "SensorModel.h"
#include "2DPoint.h"
#include "3DPoint.h"
#include "XYPoint.h"
#include "XYZPoint.h"
#include "XYPointArray.h"
#include "XYZPointArray.h"
#include "StructuredFile.h"
#include "BundleManager.h"
#include "robustWeightFunction.h"
#include "AdjustMatrix.h"
#include <strstream>


CSensorModel::CSensorModel(bool I_hasValues,          const CFileName &I_FileName, 				
						   int nShiftParameters,      int nRotationParameters,
						   int nScaleParameters,      int nMountShiftParameters,
						   int nMountRotParameters,   int nInteriorParameters, 
						   int nAdditionalParameters, int nPointParameters,
						   int nPointObservations) :
		hasValues(I_hasValues), FileName(I_FileName), bIsCurrentSensorModel(false),
		nParsPerPoint(nPointParameters),nObsPerPoint(nPointObservations),reduction(0.0, 0.0, 0.0) 
{

	nPotentialParameter[shiftPar] = nShiftParameters;
	nPotentialParameter[rotPar]   = nRotationParameters;
	nPotentialParameter[scalePar] = nScaleParameters;
	nPotentialParameter[mountShiftPar] = nMountShiftParameters;
	nPotentialParameter[mountRotPar] = nMountRotParameters;
	nPotentialParameter[interiorPar] = nInteriorParameters;
	nPotentialParameter[addPar] = nAdditionalParameters;
	nPotentialParameter[constraints] = 0;

	nActualParameter[shiftPar] = nShiftParameters;
	nActualParameter[rotPar]   = nRotationParameters;
	nActualParameter[scalePar] = nScaleParameters;
	nActualParameter[mountShiftPar] = nMountShiftParameters;
	nActualParameter[mountRotPar] = nMountRotParameters;
	nActualParameter[interiorPar] = nInteriorParameters;
	nActualParameter[addPar] = nAdditionalParameters;
	nActualParameter[constraints] = 0;

	this->activeParameter[shiftPar] = (1 << nShiftParameters) -1;
	this->activeParameter[rotPar] = (1 << nRotationParameters) -1;
	this->activeParameter[scalePar] = (1 << nScaleParameters) -1;
	this->activeParameter[mountShiftPar] = (1 << nMountShiftParameters) -1;
	this->activeParameter[mountRotPar] = (1 << nMountRotParameters) -1;
	this->activeParameter[interiorPar] = (1 << nInteriorParameters) -1;
	this->activeParameter[addPar] = (1 << nAdditionalParameters) -1;
	this->activeParameter[constraints] = 0;



	for (int i=0; i < 8; i++)
		parameterIndex[i] = -1;

	this->calculateNumberOfParameter();

	this->covariance.ReSize(this->nPotentialStationPars, this->nPotentialStationPars);
	this->covariance = 0.0f;
}

CSensorModel::CSensorModel(const CSensorModel &sensor) :
		hasValues(sensor.hasValues), FileName(sensor.FileName),    
		covariance (sensor.covariance), refSystem(sensor.refSystem),bIsCurrentSensorModel(sensor.bIsCurrentSensorModel),
		nPotentialStationPars (sensor.nPotentialStationPars),nActualStationPars(sensor.nActualStationPars),
		reduction(sensor.reduction),nParsPerPoint(sensor.nParsPerPoint),nObsPerPoint(sensor.nObsPerPoint)
{
	for (int i=0; i < 8; i++)
	{
		nPotentialParameter[i] = sensor.nPotentialParameter[i];
		nActualParameter[i] = sensor.nActualParameter[i];
		parameterIndex [i] = sensor.parameterIndex[i];
		this->activeParameter [i] = sensor.activeParameter[i];
	}

};

CSensorModel &CSensorModel::operator = (const CSensorModel & sensor)
{
	for (int i=0; i< 8; i++)
	{
		this->nPotentialParameter[i] = sensor.nPotentialParameter[i];
		this->nActualParameter[i] = sensor.nActualParameter[i];
		this->activeParameter[i] = sensor.activeParameter[i];
		this->parameterIndex[i] = sensor.parameterIndex[i];
	}
	
	this->reduction = sensor.reduction;	
    this->FileName = sensor.FileName;
	this->nPotentialStationPars = sensor.nPotentialStationPars;
	this->nActualStationPars = sensor.nActualStationPars;        
	this->nParsPerPoint = sensor.nParsPerPoint;              
	this->nObsPerPoint = sensor.nObsPerPoint;             
	this->covariance = sensor.covariance;              
	this->refSystem = sensor.refSystem;		
	this->hasValues = sensor.hasValues;
	this->bIsCurrentSensorModel = sensor.bIsCurrentSensorModel;

	return *this;
}


CSensorModel::~CSensorModel(void)
{
}



CCharString CSensorModel::getGUIName() const
{
	CCharString name = this->getClassName();
	return name.Right(name.GetLength() - 1);	
}

void CSensorModel::calculateNumberOfParameter()
{
	this->nPotentialStationPars = 0;
	this->nActualStationPars = 0;

	for (int i=0; i < 7; i++) 
	{
		this->nPotentialStationPars += this->nPotentialParameter[i];
		this->nActualStationPars    += this->nActualParameter[i];

	}
}

void CSensorModel::resetParameterIndices() 
{
	for (int i=0; i < 8; i++)
		parameterIndex[i] = -1;
}

void CSensorModel::initFromModel(const CSensorModel &sensorModel)
{
	this->FileName                  = sensorModel.FileName;
	this->hasValues                 = sensorModel.hasValues;
	this->covariance                = sensorModel.covariance;
	this->refSystem                 = sensorModel.refSystem;
	for (int i=0; i < 7; i++) 
	{
		this->nPotentialParameter[i] = sensorModel.nPotentialParameter[i];
		this->nActualParameter[i]    = sensorModel.nActualParameter[i];
	}
};

bool CSensorModel::setRefSys(const CReferenceSystem &I_refSystem)
{
	refSystem = I_refSystem;

	this->informListeners_elementsChanged(this->getClassName().c_str());
	return true;
};

/**
* Transforms an xyz point array to its cooresponding xy point array
* in image space
* The xyz point array is assumed to be Lat Long Height expressed in the
* appropriate zone for the RPC.
*
* @param dest  The xy point array, and XYZPointArray, the XYZ Points
*/
void CSensorModel::transformArrayObj2Obs(CObsPointArray &obsArray, const CXYZPointArray &objArray) const
{
    obsArray.RemoveAll();

    for(int i = 0; i < objArray.GetSize(); i++)
    {
        CXYZPoint* XYZPoint = objArray.getAt(i);
            
        CObsPoint* XYPoint = obsArray.Add();

        this->transformObj2Obs(*XYPoint, *XYZPoint);
    }

    for (int i = 0; i < obsArray.getListenerCount(); i++)
    {
        CUpdateListener* l = obsArray.getListener(i);

		l->elementsChanged();
    }
}
/*
void CSensorModel::transformArrayObj2Obs(CXYZPointArray &obsArray, const CXYZPointArray &objArray) const
{
    obsArray.RemoveAll();

    for(int i = 0; i < objArray.GetSize(); i++)
    {
        CXYZPoint* XYZPoint = objArray.GetAt(i);
            
        CXYZPoint* destPoint = obsArray.Add();

        this->transformObj2Obs(*destPoint, *XYZPoint);
    }

    for (int i = 0; i < obsArray.listeners.GetSize(); i++)
    {
        CXYZPointArrayListener* l = obsArray.listeners.GetAt(i);

        l->changedValues();
    }
}
*/
/**
 * gets filename of rpc parameters
 * @returns filename
 */
CFileName* CSensorModel::getFileName()
{
    return &this->FileName;
}

/**
 * sets filename of rpc parameters
 * @param  filename
 */
void CSensorModel::setFileName(const char* filename)
{
    this->FileName = filename;

	this->informListeners_elementsChanged(this->getClassName().c_str());
}

/**
 * reconnects pointers
 */
void CSensorModel::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CSensorModel::resetPointers()
{
    CSerializable::resetPointers();
}


/**
 * Flag if CSensorModel is modified
 * returns bool
 */
bool CSensorModel::isModified()
{
    return this->bModified;
}


Matrix* CSensorModel::getCovariance()
{
    return &this->covariance;
}

CFileName CSensorModel::getItemText() const
{
    CFileName itemtext = this->FileName.GetFullFileName();
    return itemtext.GetChar();
}


void CSensorModel::setCovariance(const Matrix &c)
{
    this->covariance = c;

    this->informListeners_elementsChanged(this->getClassName().c_str());
}


void CSensorModel::getObservationNames(CCharStringArray &names) const
{
	names.RemoveAll();
    
    CCharString* name = names.Add();
    *name = "x";

	if (this->nObsPerPoint > 1)
	{
		name = names.Add();
		*name = "y";
		if (this->nObsPerPoint > 2)
		{
			name = names.Add();
			*name = "z";
			if (this->nObsPerPoint > 3)
			{
				name = names.Add();
				*name = "t";
			}
		}
	}
}

double CSensorModel::getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res)
{
	res = deltaPar;
	Matrix vtv = res.t() * covariance.i() * res;
	return vtv.element(0,0);
};

bool CSensorModel::getVerticalPole(CXYZPoint      &objGround, CXYZPoint      &objTop, 
								   const CXYPoint &obsGround, const CXYPoint &obsTop, 
								   double heightGround, float  sigmaZ) const
{
	Try
	{
		double pZ = 1.0 / (double) sigmaZ;
		pZ *= pZ;
		CObsPoint *p = &objGround;
		this->transformObs2Obj(*p, obsGround, heightGround, sigmaZ);
		objTop.set(objGround.getX(), objGround.getY(), heightGround + 10.0f);

		Matrix obsMatGround(1,2);
		obsMatGround.element(0,0) = obsGround.getX();
		obsMatGround.element(0,1) = obsGround.getY();
		Matrix BtQllBinvGround;
		this->Bi(BtQllBinvGround, obsMatGround, objGround);
		BtQllBinvGround = BtQllBinvGround.t() * obsGround.getCovar() * BtQllBinvGround;
		BtQllBinvGround = BtQllBinvGround.i();

		Matrix obsMatTop(1,2);
		obsMatTop.element(0,0) = obsTop.getX();
		obsMatTop.element(0,1) = obsTop.getY();
		Matrix BtQllBinvTop;
		this->Bi(BtQllBinvTop, obsMatTop, objTop);
		BtQllBinvTop = BtQllBinvTop.t() * obsTop.getCovar() * BtQllBinvTop;
		BtQllBinvTop = BtQllBinvTop.i();

		int iter = 0;
		Matrix Qxx;
		double rms = FLT_MAX;
		double rmsOld = FLT_MAX;
		double deltaRMS = 1.0;

		CXYPoint backproject;

		do
		{
			Matrix lAi, lFi;
			this->AiPoint(lAi, obsMatTop, objTop);
			this->Fi(lFi, obsMatTop, objTop);
			rms = lFi.element(0,0) * lFi.element(0,0) + lFi.element(1,0) *  lFi.element(1,0);
			Matrix Npt = lAi.t() * BtQllBinvTop;
			Matrix N(4,4); N = 0.0;
			Matrix t(4,1); t = 0.0;
			Matrix N1 = Npt * lAi;
			Matrix t1 = Npt * lFi;
			this->addMatrix(0, 0, N, N1);
			this->addMatrix(0, 0, t, t1);
			
		    this->AiPoint(lAi, obsMatGround, objGround);
			this->Fi(lFi, obsMatGround, objGround);
			rms += lFi.element(0,0) * lFi.element(0,0) + lFi.element(1,0) *  lFi.element(1,0);
			
			Npt = lAi.t() * BtQllBinvGround;
			N1 = Npt * lAi;
			t1 = Npt * lFi;

			N.element(0,0) += N1.element(0,0); N.element(0,1) += N1.element(0,1); N.element(0,3) += N1.element(0,2);
			N.element(1,0) += N1.element(1,0); N.element(1,1) += N1.element(1,1); N.element(1,3) += N1.element(1,2);
			N.element(3,0) += N1.element(2,0); N.element(3,1) += N1.element(2,1); N.element(3,3) += N1.element(2,2);
			t.element(0,0) += t1.element(0,0);
			t.element(1,0) += t1.element(1,0); 
			t.element(3,0) += t1.element(2,0); 
			N.element(3,3) += pZ;
			double dZ = objGround.z - heightGround;
			t.element(3,0) += dZ * pZ;

			Qxx = N.i();
			Matrix delta = Qxx * t;
			objGround.set(objGround.x - delta.element(0,0), objGround.y  - delta.element(1,0), objGround.z - delta.element(3,0));
			objTop.set(objGround.x, objGround.y, objTop.z - delta.element(2,0));

			deltaRMS = fabs(rmsOld - rms);
			rmsOld = rms;
			++iter;
		} while ((iter < 20) && (rms > 1e-20) && (deltaRMS > 1e-10));


		if (rms < 1.0f) rms = 1.0f;
		Qxx *= rms;
		objGround.setSX(sqrt(Qxx.element(0,0))); objGround.setSY(sqrt(Qxx.element(1,1))); objGround.setSZ(sqrt(Qxx.element(3,3)));
		objGround.setSXY(Qxx.element(1,0));      objGround.setSXZ(Qxx.element(3,0));      objGround.setSYZ(Qxx.element(3,1));
		objTop.setSX(sqrt(Qxx.element(0,0)));    objTop.setSY(sqrt(Qxx.element(1,1)));    objTop.setSZ(sqrt(Qxx.element(2,2)));
		objTop.setSXY(Qxx.element(1,0));         objTop.setSXZ(Qxx.element(2,0));         objTop.setSYZ(Qxx.element(2,1));
		
	}
	CatchAll
	{
		return false;
	}

	return true;
};

void CSensorModel::setReduction(const C3DPoint &red)
{
	if (this->getRefSys().getCoordinateType() != eGEOGRAPHIC)
		this->reduction = red;
};

void CSensorModel::applyReduction()
{
	this->reduction.x = this->reduction.y = this->reduction.z = 0.0;
};
 
void CSensorModel::transformObj2Obs(CObsPoint  &obs, const CXYZPoint &obj) const
{
	obs.setLabel(obj.getLabel());
	
	if (obs.getDimension() == 2)
	{
		C2DPoint obsCopy;
		transformObj2Obs(obsCopy, obj);
		obs[0] = obsCopy.x;
		obs[1] = obsCopy.y;
	}
	else if (obs.getDimension() == 3)
	{
		C3DPoint obsCopy;
		transformObj2Obs(obsCopy, obj);
		obs[0] = obsCopy.x;
		obs[1] = obsCopy.y;
		obs[2] = obsCopy.z;
	}
	else
		assert(false);
	
};

bool CSensorModel::transformObs2Obj(CObsPoint &obj, const CObsPoint &obs) const
{
	double z = 0.0;
	float sz = 1.0;

	if (obs.getDimension() > 2) 
	{
		sz = (float) sqrt(obs.getCovarElement(2,2));
		z = obs[2];
	}

	return transformObs2Obj(obj, obs, z, sz);
};

void CSensorModel::addMatrix(int startRow, int startCol, Matrix &dest, Matrix &src)
{
	for (int r = 0, R = startRow; r < src.Nrows(); r++, R++)
    {
		for (int c = 0, C = startCol; c < src.Ncols(); c++, C++)
        {
			dest.element(R, C) += src.element(r, c);
        }
    }
}

void CSensorModel::accumulateShifts(Matrix &N, Matrix &t,
									const Matrix &Fi,         const Matrix &Bi,   
									const Matrix &AiErp,      const Matrix &AiRot,      
									const Matrix &AiSca,      const Matrix &AiMountShift, 
									const Matrix &AiMountRot, const Matrix &AiIrp, 
									const Matrix &AiPar,      const Matrix &AiPoint,
									const Matrix &We,         int pointOffset)
{
	if (this->getParameterCount(shiftPar))
	{
		Matrix erptW = AiErp.t() * We;
		Matrix ATWeA = erptW * AiErp;
		this->addMatrix(this->parameterIndex[shiftPar], this->parameterIndex[shiftPar], N, ATWeA);
		ATWeA = erptW * AiPoint;	
		this->addMatrix(this->parameterIndex[shiftPar], pointOffset, N, ATWeA);
		ATWeA = ATWeA.t();
		this->addMatrix(pointOffset, this->parameterIndex[shiftPar], N, ATWeA);
		Matrix ATWef = erptW * Fi;
		this->addMatrix(this->parameterIndex[shiftPar], 0, t, ATWef);

		if (this->getParameterCount(rotPar))
		{
			ATWeA = erptW * AiRot;
			this->addMatrix(this->parameterIndex[shiftPar], this->parameterIndex[rotPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[rotPar], this->parameterIndex[shiftPar], N, ATWeA);		
		}
		
		if (this->getParameterCount(scalePar))
		{
			ATWeA = erptW * AiSca;
			this->addMatrix(this->parameterIndex[shiftPar], this->parameterIndex[scalePar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[scalePar], this->parameterIndex[shiftPar], N, ATWeA);
		}

		if (this->getParameterCount(mountShiftPar))
		{
			ATWeA = erptW * AiMountShift;
			this->addMatrix(this->parameterIndex[shiftPar], this->parameterIndex[mountShiftPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[mountShiftPar], this->parameterIndex[shiftPar], N, ATWeA);
		}

		if (this->getParameterCount(mountRotPar))
		{
			ATWeA = erptW * AiMountRot;
			this->addMatrix(this->parameterIndex[shiftPar], this->parameterIndex[mountRotPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[mountRotPar], this->parameterIndex[shiftPar], N, ATWeA);
		}

		if (this->getParameterCount(interiorPar))
		{
			ATWeA = erptW * AiIrp;
			this->addMatrix(this->parameterIndex[shiftPar], this->parameterIndex[interiorPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[interiorPar], this->parameterIndex[shiftPar], N, ATWeA);
		}
			
		if (this->getParameterCount(addPar))
		{
			ATWeA = erptW * AiPar;
			this->addMatrix(this->parameterIndex[shiftPar], this->parameterIndex[addPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[addPar], this->parameterIndex[shiftPar], N, ATWeA);
		}
	}
};
	  
void CSensorModel::accumulateRots(Matrix &N, Matrix &t,					
								  const Matrix &Fi,           const Matrix &Bi,   
								  const Matrix &AiRot,        const Matrix &AiSca,      
								  const Matrix &AiMountShift, const Matrix &AiMountRot, 
								  const Matrix &AiIrp,        const Matrix &AiPar,      
								  const Matrix &AiPoint,      const Matrix &We,
								  int pointOffset)
{
	if (this->getParameterCount(rotPar))
	{
		Matrix rottW = AiRot.t() * We;
		Matrix ATWeA = rottW * AiRot;
		this->addMatrix(this->parameterIndex[rotPar], this->parameterIndex[rotPar], N, ATWeA);
		ATWeA = rottW * AiPoint;
		this->addMatrix(this->parameterIndex[rotPar], pointOffset, N, ATWeA);
		ATWeA = ATWeA.t();
		this->addMatrix(pointOffset, this->parameterIndex[rotPar], N, ATWeA);
		Matrix ATWef = rottW * Fi;
		this->addMatrix(this->parameterIndex[rotPar], 0, t, ATWef);
	
		if (this->getParameterCount(scalePar))
		{
			ATWeA = rottW * AiSca;
			this->addMatrix(this->parameterIndex[rotPar], this->parameterIndex[scalePar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[scalePar], this->parameterIndex[rotPar], N, ATWeA);
		}

		if (this->getParameterCount(mountShiftPar))
		{
			ATWeA = rottW * AiMountShift;
			this->addMatrix(this->parameterIndex[rotPar], this->parameterIndex[mountShiftPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[mountShiftPar], this->parameterIndex[rotPar], N, ATWeA);
		}

		if (this->getParameterCount(mountRotPar))
		{
			ATWeA = rottW * AiMountRot;
			this->addMatrix(this->parameterIndex[rotPar], this->parameterIndex[mountRotPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[mountRotPar], this->parameterIndex[rotPar], N, ATWeA);
		}

		if (this->getParameterCount(interiorPar))
		{
			ATWeA = rottW * AiIrp;
			this->addMatrix(this->parameterIndex[rotPar], this->parameterIndex[interiorPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[interiorPar], this->parameterIndex[rotPar], N, ATWeA);
		}
		
		if (this->getParameterCount(addPar))
		{
			ATWeA = rottW * AiPar;
			this->addMatrix(this->parameterIndex[rotPar], this->parameterIndex[addPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[addPar], this->parameterIndex[rotPar], N, ATWeA);
		}	
	}		
};

void CSensorModel::accumulateScale(Matrix &N, Matrix &t,
								   const Matrix &Fi,         const Matrix &Bi,   
								   const Matrix &AiSca,      const Matrix &AiMountShift, 
								   const Matrix &AiMountRot, const Matrix &AiIrp, 
								   const Matrix &AiPar,      const Matrix &AiPoint, 
								   const Matrix &We,         int pointOffset)
{
	if (this->getParameterCount(scalePar))
	{
		Matrix scatW = AiSca.t() * We;
		Matrix ATWeA = scatW * AiSca;
		this->addMatrix(this->parameterIndex[scalePar], this->parameterIndex[scalePar], N, ATWeA);
		ATWeA = scatW * AiPoint;
		this->addMatrix(this->parameterIndex[scalePar], pointOffset, N, ATWeA);
		ATWeA = ATWeA.t();
		this->addMatrix(pointOffset, this->parameterIndex[scalePar], N, ATWeA);
		Matrix ATWef = scatW * Fi;
		this->addMatrix(this->parameterIndex[scalePar], 0, t, ATWef);

		if (this->getParameterCount(mountShiftPar))
		{
			ATWeA = scatW * AiMountShift;
			this->addMatrix(this->parameterIndex[scalePar], this->parameterIndex[mountShiftPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[mountShiftPar], this->parameterIndex[scalePar], N, ATWeA);
		}

		if (this->getParameterCount(mountRotPar))
		{
			ATWeA = scatW * AiMountRot;
			this->addMatrix(this->parameterIndex[scalePar], this->parameterIndex[mountRotPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[mountRotPar], this->parameterIndex[scalePar], N, ATWeA);
		}

		if (this->getParameterCount(interiorPar))
		{
			ATWeA = scatW * AiIrp;
			this->addMatrix(this->parameterIndex[scalePar], this->parameterIndex[interiorPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[interiorPar], this->parameterIndex[scalePar], N, ATWeA);
		}
			
		if (this->getParameterCount(addPar))
		{
			ATWeA = scatW * AiPar;
			this->addMatrix(this->parameterIndex[scalePar], this->parameterIndex[addPar], N, ATWeA);				
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[addPar], this->parameterIndex[scalePar], N, ATWeA);
		}

	}
};

void CSensorModel::accumulateMtShft(Matrix &N, Matrix &t,
									const Matrix &Fi,         const Matrix &Bi,   
									const Matrix &AiMountShift, 
 									const Matrix &AiMountRot, const Matrix &AiIrp, 
 									const Matrix &AiPar,      const Matrix &AiPoint, 
									const Matrix &We,         int pointOffset)
{
	if (this->getParameterCount(mountShiftPar))
	{
		Matrix msshtW = AiMountShift.t() * We;
		Matrix ATWeA = msshtW * AiMountShift;
		this->addMatrix(this->parameterIndex[mountShiftPar], this->parameterIndex[mountShiftPar], N, ATWeA);
		ATWeA = msshtW * AiPoint;
		this->addMatrix(this->parameterIndex[mountShiftPar], pointOffset, N, ATWeA);
		ATWeA = ATWeA.t();
		this->addMatrix(pointOffset, this->parameterIndex[mountShiftPar], N, ATWeA);
		Matrix ATWef = msshtW * Fi;
		this->addMatrix(this->parameterIndex[mountShiftPar], 0, t, ATWef);

		if (this->getParameterCount(mountRotPar))
		{
			ATWeA = msshtW * AiMountRot;
			this->addMatrix(this->parameterIndex[mountShiftPar], this->parameterIndex[mountRotPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[mountRotPar], this->parameterIndex[mountShiftPar], N, ATWeA);
		}

		if (this->getParameterCount(interiorPar))
		{
			ATWeA = msshtW * AiIrp;
			this->addMatrix(this->parameterIndex[mountShiftPar], this->parameterIndex[interiorPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[interiorPar], this->parameterIndex[mountShiftPar], N, ATWeA);
		}
			
		if (this->getParameterCount(addPar))
		{
			ATWeA = msshtW * AiPar;
			this->addMatrix(this->parameterIndex[mountShiftPar], this->parameterIndex[addPar], N, ATWeA);				
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[addPar], this->parameterIndex[mountShiftPar], N, ATWeA);
		}

	}
};

void CSensorModel::accumulateMtRot(Matrix &N, Matrix &t,
								   const Matrix &Fi,         const Matrix &Bi,   
								   const Matrix &AiMountRot, const Matrix &AiIrp, 
 								   const Matrix &AiPar,      const Matrix &AiPoint, 
								   const Matrix &We,         int pointOffset)
{
	if (this->getParameterCount(mountRotPar))
	{
		Matrix mrottW = AiMountRot.t() * We;
		Matrix ATWeA = mrottW * AiMountRot;
		this->addMatrix(this->parameterIndex[mountRotPar], this->parameterIndex[mountRotPar], N, ATWeA);
		ATWeA = mrottW * AiPoint;
		this->addMatrix(this->parameterIndex[mountRotPar], pointOffset, N, ATWeA);
		ATWeA = ATWeA.t();
		this->addMatrix(pointOffset, this->parameterIndex[mountRotPar], N, ATWeA);
		Matrix ATWef = mrottW * Fi;
		this->addMatrix(this->parameterIndex[mountRotPar], 0, t, ATWef);

		if (this->getParameterCount(interiorPar))
		{
			ATWeA = mrottW * AiIrp;
			this->addMatrix(this->parameterIndex[mountRotPar], this->parameterIndex[interiorPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[interiorPar], this->parameterIndex[mountRotPar], N, ATWeA);
		}
			
		if (this->getParameterCount(addPar))
		{
			ATWeA = mrottW * AiPar;
			this->addMatrix(this->parameterIndex[mountRotPar], this->parameterIndex[addPar], N, ATWeA);				
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[addPar], this->parameterIndex[mountRotPar], N, ATWeA);
		}
	}
};

void CSensorModel::accumulateIntPar(Matrix &N, Matrix &t,
								    const Matrix &Fi,         const Matrix &Bi,   
								    const Matrix &AiIrp, 
 								    const Matrix &AiPar,      const Matrix &AiPoint, 
								    const Matrix &We,         int pointOffset)
{
	if (this->getParameterCount(interiorPar))
	{
		Matrix irptW = AiIrp.t() * We;
		Matrix ATWeA = irptW * AiIrp;
		this->addMatrix(this->parameterIndex[interiorPar], this->parameterIndex[interiorPar], N, ATWeA);
		ATWeA = irptW * AiPoint;
		this->addMatrix(this->parameterIndex[interiorPar], pointOffset, N, ATWeA);
		ATWeA = ATWeA.t();
		this->addMatrix(pointOffset, this->parameterIndex[interiorPar], N, ATWeA);
		Matrix ATWef = irptW * Fi;
		this->addMatrix(this->parameterIndex[interiorPar], 0, t, ATWef);

		if (this->getParameterCount(addPar))
		{
			ATWeA = irptW * AiPar;
			this->addMatrix(this->parameterIndex[interiorPar], this->parameterIndex[addPar], N, ATWeA);
			ATWeA = ATWeA.t();
			this->addMatrix(this->parameterIndex[addPar], this->parameterIndex[interiorPar], N, ATWeA);
		}

	}
}

void CSensorModel::accumulateAddPar (Matrix &N, Matrix &t,
									 const Matrix &Fi,         const Matrix &Bi,   
									 const Matrix &AiPar,      const Matrix &AiPoint, 
									 const Matrix &We,         int pointOffset)
{
	if (this->getParameterCount(addPar))
	{
		Matrix adptW = AiPar.t() * We;
		Matrix ATWeA = adptW * AiPar;
		this->addMatrix(this->parameterIndex[addPar], this->parameterIndex[addPar], N, ATWeA);
		ATWeA = adptW * AiPoint;
		this->addMatrix(this->parameterIndex[addPar], pointOffset, N, ATWeA);
		ATWeA = ATWeA.t();
		this->addMatrix(pointOffset, this->parameterIndex[addPar], N, ATWeA);
		Matrix ATWef = adptW * Fi;
		this->addMatrix(this->parameterIndex[addPar], 0, t, ATWef);
	}
}

CObsPoint CSensorModel::calcRes(const Matrix &Delta, const CObsPoint &obs, const CObsPoint &obj)
{
	int dimension = obs.getDimension();
	CObsPoint residual(dimension);
	if (obs.id >= 0 || obj.id < 0) 
	{
		Matrix AiErp,  AiRot,  AiSca,  AiMtShft,  AiMtRot,  AiIrp,  AiPar,  AiPoint, Fi, Bi, 
	           parErp, parRot, parSca, parMtShft, parMtRot, parIrp, parPar, parPnt;

		Matrix obsMat(1, dimension+1);
		for (int i=0; i < dimension; i++)
			obsMat.element(0,i) = obs[i];

		obsMat.element(0,dimension) = obs.dot;

		Matrix *Qi = obs.getCovariance();
		if (!this->AiBiFi(Fi, Bi, AiErp, AiRot, AiSca, AiMtShft, AiMtRot, AiIrp, AiPar, AiPoint, obsMat, C3DPoint(obj[0],obj[1],obj[2])))
		{
			for (int i=0; i < dimension; i++)
				residual[i] = 0.0;
			return residual;
		}

		if (this->getParameterCount(shiftPar))
		{
			parErp.ReSize(this->getParameterCount(shiftPar), 1);
			for (int j = 0; j < this->getParameterCount(shiftPar); ++j)
			{
				parErp.element(j, 0) =Delta.element(this->parameterIndex[shiftPar] + j, 0);
			};

			Fi = Fi - AiErp * parErp;
		}
	
		if (this->getParameterCount(rotPar))
		{
			parRot.ReSize(this->getParameterCount(rotPar), 1);
			for (int j = 0; j < this->getParameterCount(rotPar); ++j)
			{
				parRot.element(j, 0) = Delta.element(this->parameterIndex[rotPar] + j, 0);
			};
			Fi = Fi - AiRot * parRot;
		}

		if (this->getParameterCount(scalePar))
		{
			parSca.ReSize(this->getParameterCount(scalePar), 1);
			for (int j = 0; j < this->getParameterCount(scalePar); ++j)
			{
				parSca.element(j, 0) = Delta.element(this->parameterIndex[scalePar] + j, 0);
			};
			Fi = Fi - AiSca * parSca;
		}

		if (this->getParameterCount(mountShiftPar))
		{
			parMtShft.ReSize(this->getParameterCount(mountShiftPar), 1);
			for (int j = 0; j < this->getParameterCount(mountShiftPar); ++j)
			{
				parMtShft.element(j, 0) = Delta.element(this->parameterIndex[mountShiftPar] + j, 0);
			};
			Fi = Fi - AiMtShft * parMtShft;
		}
			
		if (this->getParameterCount(mountRotPar))
		{
			parMtRot.ReSize(this->getParameterCount(mountRotPar), 1);
			for (int j = 0; j < this->getParameterCount(mountRotPar); ++j)
			{
				parMtRot.element(j, 0) = Delta.element(this->parameterIndex[mountRotPar] + j, 0);
			};
			Fi = Fi - AiMtRot * parMtRot;
		}

		if (this->getParameterCount(interiorPar))
		{
			parIrp.ReSize(this->getParameterCount(interiorPar), 1);
			for (int j = 0; j < this->getParameterCount(interiorPar); ++j)
			{
				parIrp.element(j, 0) = Delta.element(this->parameterIndex[interiorPar] + j, 0);
			};
			Fi = Fi - AiIrp * parIrp;
		}
					
		if (this->getParameterCount(addPar))
		{
			parPar.ReSize(this->getParameterCount(addPar), 1);
			for (int j = 0; j < this->getParameterCount(addPar); ++j)
			{
				parPar.element(j, 0) = Delta.element(this->parameterIndex[addPar] + j, 0);
			};
			Fi = Fi - AiPar * parPar;
		}

		if (this->getNParsPerPoint())
		{
			parPnt.ReSize(this->getNParsPerPoint(), 1);

			for (int j = 0; j < this->getNParsPerPoint(); ++j)
			{
				parPnt.element(j, 0) = Delta.element(obj.id + j, 0);
			};

			Fi = Fi - AiPoint * parPnt;
		}

    	Matrix res = -*Qi* Bi.t() * (Bi * *Qi * Bi.t()).i() * Fi;

		for (int i=0; i < dimension; i++)
			residual[i] = res.element(i,0);
	}

	return residual;
};


void CSensorModel::convertToDifferentialUnits(CObsPoint &out, const CObsPoint &in) const
{ 
	out = in; 
};
	  
bool CSensorModel::checkRobust(CObsPoint &obs, const CObsPoint &normalisedDiscrepancy, 
		                       const CRobustWeightFunction *robust,
							   int coordIndex, bool omit, double &w,
							   CAdjustMatrix &Pmat)
{
	bool ret = true;

	if (omit && obs.getStatusBit(ROBUSTBITBASE + coordIndex))
	{
		w = 0.0;
		ret = false;

		for (int i = 0; i < Pmat.getRows(); ++i)
		{
			Pmat(i, coordIndex) = 0;
			Pmat(coordIndex, i) = 0;
		}
	}
	else if (robust)
	{
		w = robust->getWeight(normalisedDiscrepancy[coordIndex]);
			
		if (w < FLT_EPSILON)
		{
			obs.activateStatusBit(ROBUSTBITBASE + coordIndex); 
			ret = false;
		}
		else 
		{
			obs.deactivateStatusBit(ROBUSTBITBASE + coordIndex); 
		}

		for (int i = 0; i < Pmat.getRows(); ++i)
		{
			Pmat(i, coordIndex) *= w;
			Pmat(coordIndex, i) *= w;
		}
	}
	else w = 1.0;

	return ret; 
};

void CSensorModel::printStationParameter(CCharString& output,CRotationMatrix* rotMat) const
{
	ostrstream stream;

	CCharStringArray StationNames, PointNames;
	this->getParameterNames(StationNames, PointNames);
	Matrix ModelPars;
	this->getAdjustableParameters(ModelPars);
	stream.setf(ios::fixed,ios::floatfield); 
	stream.precision(this->coordPrecision()); 
	int width = 20;
	int sigmawidth = 13;

	for (int j = 0; j < ModelPars.Nrows(); ++j)
	{
		CCharString *parName = StationNames.GetAt(j);
		stream << "\n      ";
		stream.width(5); 
		stream << parName->GetChar() << ": ";
		stream.width(width); 
		CObsPoint oldUnit(2);
		CObsPoint newUnit(2);
		oldUnit[0] = ModelPars.element(j,0);
		oldUnit[1] = sqrt(this->covariance.element(j,j));
		this->convertToDifferentialUnits(newUnit,oldUnit);
		stream << newUnit[0] << " +- ";
		stream.width(sigmawidth); 
		stream << newUnit[1] ;
	}

	stream << ends;
	char* s = stream.str();
	output = s;
	delete [] s;
}