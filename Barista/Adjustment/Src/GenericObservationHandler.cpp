/**
 * @class CGenericObservationHandler
 * CGenericObservationHandler handles observations
 */

#include "GenericObservationHandler.h"

CGenericObservationHandler::CGenericObservationHandler(CObsPointArray* ObsPoints, CSensorModel *SensorModel, 
													   const CCharString & Name, bool isControl):
													   IsControl (isControl), name(Name)
{
	this->obsPoints = ObsPoints;
	this->currentSensorModel = SensorModel;
}

CGenericObservationHandler::~CGenericObservationHandler()
{
	this->obsPoints = 0;
	this->currentSensorModel = 0;
}
