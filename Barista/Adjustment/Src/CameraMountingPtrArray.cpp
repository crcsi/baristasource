#include "CameraMountingPtrArray.h"

CCameraMountingPtrArray::CCameraMountingPtrArray(int nGrowBy):
		CMUObjectPtrArray<CCameraMounting>(nGrowBy)
{
}

CCameraMountingPtrArray::CCameraMountingPtrArray(const CCameraMountingPtrArray& src) :
	CMUObjectPtrArray<CCameraMounting>(src.GetGrowBy())
{
}

CCameraMountingPtrArray::~CCameraMountingPtrArray(void)
{
}
