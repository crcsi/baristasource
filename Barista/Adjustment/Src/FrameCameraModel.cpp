#include <vector>

#include "FrameCameraModel.h"
#include "RotationBaseFactory.h"
#include "Trans2DAffine.h"
#include "Trans2DConformal.h"
#include "AdjustMatrix.h"
#include "3Dplane.h"
#include "3DadjPlane.h"
#include "XYZLinesAdj.h"


CFrameCameraModel::CFrameCameraModel(void) : CSensorModel(false, "Frame Camera", 3, 3, 0, 0, 0, 0, 0,  0, 2),
	camera(0),cameraMounting(0),cameraRotation(0),cameraPos(0,0,0),cameraToFrame(0),frameToCamera(0)
{
}


CFrameCameraModel::CFrameCameraModel(const CFrameCameraModel& sensor): CSensorModel(sensor),
	camera(sensor.camera),cameraMounting(sensor.cameraMounting),cameraRotation(sensor.cameraRotation),
	cameraPos(sensor.cameraPos),cameraToFrame(sensor.cameraToFrame),frameToCamera(sensor.frameToCamera)
{
}

CFrameCameraModel &CFrameCameraModel::operator = (const CFrameCameraModel & framecamera)
{
	CSensorModel::operator =(framecamera);
	
	this->cameraPos = framecamera.cameraPos;
	if (this->cameraRotation)
	{
		delete this->cameraRotation;
		this->cameraRotation = NULL;
	}
		
	if (framecamera.cameraRotation)
		this->cameraRotation = CRotationBaseFactory::initRotation(framecamera.cameraRotation);

	if (this->cameraToFrame)
	{
		delete this->cameraToFrame;
		this->cameraToFrame = NULL;
	}

	if (framecamera.cameraToFrame)
		this->cameraToFrame = framecamera.cameraToFrame->clone();
	

	if (this->frameToCamera)
	{
		delete this->frameToCamera;
		this->frameToCamera = NULL;
	}

	if (framecamera.frameToCamera)
		this->frameToCamera = framecamera.frameToCamera->clone();

	return *this;
}


CFrameCameraModel::~CFrameCameraModel(void) 
{
	if (this->cameraRotation)
		delete this->cameraRotation;

	this->cameraRotation = NULL;

	if (this->cameraToFrame)
		delete this->cameraToFrame;

	this->cameraToFrame = NULL;

	if (this->frameToCamera)
		delete this->frameToCamera;

	this->frameToCamera = NULL;

}

//================================================================================

void CFrameCameraModel::setCameraPosition(const CXYZPoint &p0)
{
	this->cameraPos = p0;

	this->informListeners_elementsChanged(this->getClassName().c_str());
};

//================================================================================
	
void CFrameCameraModel::setCameraRotation(CRotationBase *rot)
{
	if ((this->cameraRotation) &&  (this->cameraRotation != rot))
		delete this->cameraRotation;
		
	this->cameraRotation = rot;

	this->informListeners_elementsChanged(this->getClassName().c_str());
};

//================================================================================

bool CFrameCameraModel::init(const CFileName& filename,
			  const CXYZPoint &newCameraPos, 
			  CRotationBase* newCameraRotation,
			  CCameraMounting* newCameraMounting,
			  CFrameCamera* newCamera)
{
	this->camera = newCamera;
	this->cameraMounting = newCameraMounting;
	this->cameraPos = newCameraPos;

	if ((this->cameraRotation) &&  (this->cameraRotation != newCameraRotation))
		delete this->cameraRotation;
		
	this->cameraRotation = newCameraRotation;

	this->FileName = filename;
	this->sethasValues(true);
	return true;
}



void CFrameCameraModel::setFiducialMarkTransformation(CTrans2D* cameraToFrame,CTrans2D* frameToCamera)
{
	if (this->cameraToFrame)
		delete this->cameraToFrame;

	this->cameraToFrame = cameraToFrame;

	if (this->frameToCamera)
		delete this->frameToCamera;

	this->frameToCamera = frameToCamera;

}



void CFrameCameraModel::crop(int offsetX, int offsetY, int newWidth, int newHeight)
{

}

void CFrameCameraModel::setAdjustableValues(const Matrix &parms)
{

}


void CFrameCameraModel::setAll(const Matrix &parms)
{

}


bool CFrameCameraModel::read(const char *filename)
{
	return true;
}


bool CFrameCameraModel::writeTextFile(const char* filename) const
{
	return true;
}


// CSerializable functions
void CFrameCameraModel::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("name", this->getFileNameChar());

    token = pStructuredFileSection->addToken();
	token->setValue("bIsCurrentSensorModel", this->bIsCurrentSensorModel);

    token = pStructuredFileSection->addToken();
    token->setValue("hasValues", this->gethasValues());

	token = pStructuredFileSection->addToken();
	token->setPointerValue("camera",this->camera);

	token = pStructuredFileSection->addToken();
	token->setPointerValue("cameraMounting",this->cameraMounting);



	if (this->cameraToFrame)
	{
		token = pStructuredFileSection->addToken();
		token->setValue("transType",this->cameraToFrame->getClassName().data());


		vector<double> parameter;
		this->cameraToFrame->getParameter(parameter);

		token = pStructuredFileSection->addToken();
		token->setValue("number of parameter", (int)parameter.size());


		token = pStructuredFileSection->addToken();
		CCharString valueStr;
		CCharString elementStr;
		
		valueStr = "";

		for (int i = 0; i < (int)parameter.size(); i++)
		{
			double element = parameter[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

		token->setValue("transParameter", valueStr.GetChar());


	}

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->refSystem.serializeStore(child);

	child = pStructuredFileSection->addChild();
    this->cameraPos.serializeStore(child);

	if (this->cameraRotation)
	{
		child = pStructuredFileSection->addChild();
		this->cameraRotation->serializeStore(child);
	}

	if (this->cameraToFrame)
	{
		child = pStructuredFileSection->addChild();
		this->cameraToFrame->getP0().serializeStore(child);
	}
	
}

void CFrameCameraModel::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	if (this->cameraToFrame)
		delete this->cameraToFrame;

	this->cameraToFrame = NULL;
	doubles parameterTrafo;
	C2DPoint parameterX0;
	int nParameter=0;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("name"))
            this->setFileName( token->getValue());
		else if(key.CompareNoCase("bIsCurrentSensorModel"))
			this->bIsCurrentSensorModel = token->getBoolValue();
		else if(key.CompareNoCase("hasValues"))
            this->hasValues = token->getBoolValue();
		else if(key.CompareNoCase("camera"))
			this->camera = (CFrameCamera*)token->getPointerValue();
		else if(key.CompareNoCase("cameraMounting"))
			this->cameraMounting = (CCameraMounting*)token->getPointerValue();
		else if(key.CompareNoCase("number of parameter"))
			nParameter = token->getIntValue();
		else if(key.CompareNoCase("transType"))
		{
			CCharString type(token->getValue());
			if (type.CompareNoCase("CTrans2DAffine"))
				this->cameraToFrame = new CTrans2DAffine();
			else if (type.CompareNoCase("CTrans2DConformal"))
				this->cameraToFrame = new CTrans2DConformal();
		}
		else if (token->isKey("transParameter") && this->cameraToFrame)
		{
			parameterTrafo.resize(nParameter);	
			token->getDoublesClass(parameterTrafo);
		}
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
           this->refSystem.serializeRestore(child);
		else if (child->getName().CompareNoCase("CXYZPoint"))
           this->cameraPos.serializeRestore(child);
		else if (child->getName().CompareNoCase("C2DPoint") && this->cameraToFrame)
			parameterX0.serializeRestore(child);
		else if (child->getName().CompareNoCase("CRollPitchYawRotation"))
		{
			if (this->cameraRotation)
				delete this->cameraRotation;

			this->cameraRotation = CRotationBaseFactory::initRotation(&C3DPoint(),RPY);
			this->cameraRotation->serializeRestore(child);
		}
		else if (child->getName().CompareNoCase("COmegaPhiKappaRotation"))
		{
			if (this->cameraRotation)
				delete this->cameraRotation;

			this->cameraRotation = CRotationBaseFactory::initRotation(&C3DPoint(),OPK);
			this->cameraRotation->serializeRestore(child);
		}
    }

	if (this->cameraToFrame && parameterTrafo.size())
	{
		Matrix parameter(parameterTrafo.size(),1);
		for (unsigned int i=0; i< parameterTrafo.size(); i++)
			parameter.element(i,0) = parameterTrafo[i];
		this->cameraToFrame->set(parameter,parameterX0);

		if (this->frameToCamera)
			delete this->frameToCamera;

		this->frameToCamera = this->cameraToFrame->clone();
		this->frameToCamera->invert();

	}


}


void CFrameCameraModel::reconnectPointers()
{
	this->camera = (CFrameCamera*) this->handleManager.getNewPointer(this->camera);
	this->cameraMounting = (CCameraMounting*) this->handleManager.getNewPointer(this->cameraMounting);
}

void CFrameCameraModel::resetPointers()
{

}


bool CFrameCameraModel::isModified()
{
	return true;
}

//================================================================================

CCharString CFrameCameraModel::getDescriptorString() const
{
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);

	if (this->frameToCamera)
	{
		oss << this->frameToCamera->getDescriptor("", 3, 7,"_cam", "_pix").GetChar() << "\n";
	}

	oss << "\nPerspective transformation model: p_cam - p_0 = s R (P_obj - P_0)";

	C3DPoint xp;
	this->cameraRotation->R_times_x(xp, this->cameraMounting->getShift());
	xp += this->cameraPos;

	CRotationMatrix totalRot;
	this->cameraRotation->R_times_R(totalRot, this->cameraMounting->getRotation().getRotMat());
	totalRot.transpose();

	C3DPoint x0 = this->camera->getIRP();
	x0.z = -x0.z;

	oss << "\np_0:\n" << x0.getString(" ", 10, 3).GetChar() << "\nP0:\n" << xp.getString(" ", 10, 3).GetChar()
		<< "\n R:\n" << totalRot.getString("", 9) << ends;
	

	char *z = oss.str();
	CCharString str(z);
	delete[]z;

	return str;
}

//================================================================================

Matrix CFrameCameraModel::getProjectionMatrix(const C2DPoint &imageOffset) const
{
	
	Matrix K = this->camera->getCalibrationMatrix();;

	if (this->cameraToFrame)
	{
		K = this->cameraToFrame->getProjectionMatrix() * K;
	}

	Matrix Hmount(4,4);
	CRotationMatrix R = this->cameraMounting->getRotation().getRotMat();
	C3DPoint shift, cms, camp;
	cms = this->cameraMounting->getShift();

	R.RT_times_x(shift, -cms);

	Hmount.element(0,0) = R(0);   Hmount.element(0,1) = R(3);   Hmount.element(0,2) = R(6);   Hmount.element(0,3) = shift.x;
	Hmount.element(1,0) = R(1);   Hmount.element(1,1) = R(4);   Hmount.element(1,2) = R(7);   Hmount.element(1,3) = shift.y;
	Hmount.element(2,0) = R(2);   Hmount.element(2,1) = R(5);   Hmount.element(2,2) = R(8);   Hmount.element(2,3) = shift.z;
	Hmount.element(3,0) =  0.0;   Hmount.element(3,1) =  0.0;   Hmount.element(3,2) =  0.0;   Hmount.element(3,3) = 1.0;
	
	Matrix H(4,4);
	R = this->cameraRotation->getRotMat();
	camp = this->cameraPos;
	R.RT_times_x(shift, -camp);

	H.element(0,0) = R(0);   H.element(0,1) = R(3);   H.element(0,2) = R(6);   H.element(0,3) = shift.x;
	H.element(1,0) = R(1);   H.element(1,1) = R(4);   H.element(1,2) = R(7);   H.element(1,3) = shift.y;
	H.element(2,0) = R(2);   H.element(2,1) = R(5);   H.element(2,2) = R(8);   H.element(2,3) = shift.z;
	H.element(3,0) =  0.0;   H.element(3,1) =  0.0;   H.element(3,2) =  0.0;   H.element(3,3) = 1.0;

	Matrix h(3,3);
	h.element(0,0) = 1.0;   h.element(0,1) = 0.0;  h.element(0,2) = -imageOffset.x;
	h.element(1,0) = 0.0;   h.element(1,1) = 1.0;  h.element(1,2) = -imageOffset.y;
	h.element(2,0) = 0.0;   h.element(2,1) = 0.0;  h.element(2,2) =  1.0;
	Matrix P1 = Hmount * H;
	Matrix P(3,4);
	for (int r = 0; r < P.Nrows(); ++r)
	{
		for (int c = 0; c < P.Ncols(); ++c)
		{
			double e = P1.element(r,c);
			P.element(r,c) = P1.element(r,c);
		}
	};

	P = h * K * P;
	
	double fac = 1.0 / P.MaximumAbsoluteValue();
	P = P * fac;

/*	C3DPoint Pobj(309934.2035, 6252870.48, 43,3924);
	ColumnVector vecObj(4);
	vecObj.element(0) = Pobj.x; vecObj.element(1) = Pobj.y; vecObj.element(2) = Pobj.z; vecObj.element(3) = 1.0;
	
	C2DPoint pimg;
	this->transformObj2Obs(pimg,Pobj);

	ColumnVector vecImg = P * vecObj;
	vecImg /= vecImg.element(2);
	C2DPoint pimg2(vecImg.element(0), vecImg.element(1));
*/
	return P;
};

void CFrameCameraModel::transformObj2Obs(C2DPoint  &obs, const C3DPoint &obj) const
{

	C3DPoint xp,xm;
	this->cameraRotation->RT_times_x(xp,obj - this->cameraPos);	// computes xp = RT * (X-X0)
	xp -=this->cameraMounting->getShift();
	this->cameraMounting->getRotation().RT_times_x(xm,xp);

	this->camera->transformObj2Obs(obs,xm,this->cameraPos.z, obj.z, this->cameraToFrame);

}


void CFrameCameraModel::transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const
{
	C2DPoint obs2D;
	transformObj2Obs(obs2D, obj);
	obs.x = obs2D.x;
	obs.y = obs2D.y;	
	obs.z = 0.0f;
}

bool CFrameCameraModel::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, double Height) const
{
	C3DPoint xc,xm,xp;

	if (!this->camera->transformObs2Obj(xc,obs,this->cameraPos.z, Height, this->frameToCamera))
		return false;

	this->cameraMounting->getRotation().R_times_x(xm,xc);
	xm += this->cameraMounting->getShift();
	
	this->cameraRotation->R_times_x(xp,xm);

	double lambda = (Height - this->cameraPos.z) / xp.z;
	obj[0] = lambda * xp.x + this->cameraPos.x;
	obj[1] = lambda * xp.y + this->cameraPos.y;
	obj[2] = Height;
	return true;
}


bool CFrameCameraModel::transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const
{
	
	C3DPoint xc,xm,xp;

	if (!this->camera->transformObs2Obj(xc,obs,this->cameraPos.z, 0.0, this->frameToCamera))
		return false;

	this->cameraMounting->getRotation().R_times_x(xm,xc);
	xm += this->cameraMounting->getShift();
	
	this->cameraRotation->R_times_x(xp,xm);

	obj = xp + this->cameraPos;
	return true;
}


bool CFrameCameraModel::transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const
{
	C3DPoint xc,xm,xp;

	if (!this->camera->transformObs2Obj(xc,obs,this->cameraPos.z, Height, this->frameToCamera))
		return false;

	this->cameraMounting->getRotation().R_times_x(xm,xc);
	xm += this->cameraMounting->getShift();
	
	this->cameraRotation->R_times_x(xp,xm);

	double lambda = (Height - this->cameraPos.z) / xp.z;
	obj[0] = lambda * xp.x + this->cameraPos.x;
	obj[1] = lambda * xp.y + this->cameraPos.y;
	obj[2] = Height;
	return true;
}


bool CFrameCameraModel::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const
{
	C3DPoint P1, P2;
	this->transformObs2Obj(P1, obs, 0.0);
	this->transformObs2Obj(P2, obs, 1.0);

    CXYZLine line(P1, P2);
		 
	eIntersect res = plane.getIntersection(line, obj);

	return res == eIntersection;
};

bool CFrameCameraModel::transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const
{
	CXYZPoint P1, P2;
	this->transformObs2Obj(P1, obs, 0.0, 0.0);
	this->transformObs2Obj(P2, obs, 1.0, 0.0);

    CXYZHomoSegment seg(P1, P2);
		 
	eIntersect res = seg.getIntersection(plane, obj);

	return res == eIntersection;
};


int CFrameCameraModel::accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
			   CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
			   CRobustWeightFunction *robust, bool omitMarked)
{
// this function computes the components of the N-Matrix and the t-vector for the given observation

// we first compute: xc - xo = F, where F = RmT*(ReT(X-X0) - Xm) 
// and the partials dF/dX0, dF/dAngles, dF/d3DPoints, dF/dMountRot, dF/dMountShift

// as a second step we compute xc = xo + c*Fx/Fz and yc = yo + c*Fy/Fz
// and the according partials	AxyX0	    =	(dxc/dX0), 
//												(dyc/dX0)
//								AxyRe       =	(dxc/dRe), 
//							                    (dyc/dRe),              
//								Axy3DPoints =	(dxc/3DPoints)
//												(dyc/3DPoints)
// then N = AT*P*A and t = AT*P*dl

	int nObs = 2;

	// compute first the 3D image vector F = RmT*(ReT(X-X0) - Xm)
	C3DPoint xp,F;
	C2DPoint xc;
	this->cameraRotation->RT_times_x(xp,approx - this->cameraPos);	// computes xp = RT * (X-X0)
	xp -=this->cameraMounting->getShift();
	this->cameraMounting->getRotation().RT_times_x(F,xp);

	this->camera->transformObj2Obs(xc,F, this->cameraPos.z, approx[2], this->cameraToFrame);

	// compute discrepancy
	discrepancy[0] =  xc.x - obs[0] ;
	discrepancy[1] =  xc.y - obs[1];

		// compute normalized residuals
	for (int i=0; i < 2; i++) 
		normalisedDiscrepancy[i] = fabs(discrepancy[i]) / sqrt(obs.getCovariance()->element(i,i));


	CAdjustMatrix derivRotMat(9,3);
	this->cameraRotation->computeDerivation(derivRotMat); // this gives as the derivations rotMat elements r00 - r22 after the angles


	const CRotationMatrix &mountRotMat = this->cameraMounting->getRotation().getRotMat();
	
	// for dF/dRotMatElements we need v = X - X0
	xp = approx - this->cameraPos;

	// compute matrix dF/dRotMatElements
	CAdjustMatrix dFdRotMatElements(3,9);
	for (int i=0; i < 3; i++)
	{
		for (int k=0; k <3; k++)
		{
			for (int j=0; j < 3; j++)
				dFdRotMatElements(i,k + j*3) = mountRotMat(k,i) * xp[j];
		}
	}

	// and now dF/dAngles
	CAdjustMatrix dFdAngles = dFdRotMatElements * derivRotMat;

	CRotationMatrix tmpRotMat;

	mountRotMat.RT_times_RT(tmpRotMat,this->cameraRotation->getRotMat());

	CAdjustMatrix dFd3DPoints(3,3);	// the partials for the 3D Points
	for (int i=0; i<3;i++)
	{
		dFd3DPoints(0,i) = tmpRotMat(0,i);
		dFd3DPoints(1,i) = tmpRotMat(1,i);
		dFd3DPoints(2,i) = tmpRotMat(2,i);
	}

	CAdjustMatrix dFdX0 = dFd3DPoints * -1;

	// determine the number of Parameter for every group
	int nX0Parameter = 3;
	int n3DParameter = 3;
	int nRotationParameter = 3;

	CAdjustMatrix Axy3DPoints(2,n3DParameter); 
	CAdjustMatrix AxyX0 (2,nX0Parameter);
	CAdjustMatrix AxyRotation(2,nRotationParameter);

	double fac1 = -this->camera->getPrincipalDistance()/(F.z);
//	if (this->frameToCamera)
//	{	
		fac1 = -fac1;
//	}
	double fac2 = -fac1*F.x/F.z;
	double fac3 = -fac1*F.y/F.z;

	// compute the final partial for the camera position
	for (int i=0; i < nX0Parameter; i++)
	{
		AxyX0(0,i) = fac1 * dFdX0(0,i) + fac2 * dFdX0(2,i);
		AxyX0(1,i) = fac1 * dFdX0(1,i) + fac3 * dFdX0(2,i);
	}

	// compute the final partials for the 3D points
	for (int i=0; i< n3DParameter; i++)
	{
		Axy3DPoints(0,i) = fac1 * dFd3DPoints(0,i) + fac2 * dFd3DPoints(2,i);
		Axy3DPoints(1,i) = fac1 * dFd3DPoints(1,i) + fac3 * dFd3DPoints(2,i);
	}

	// compute the final partials for the camera rotation
	for (int i=0; i < nRotationParameter; i++)
	{
		AxyRotation(0,i) = fac1 *dFdAngles(0,i) + fac2 * dFdAngles(2,i);
		AxyRotation(1,i) = fac1 *dFdAngles(1,i) + fac3 * dFdAngles(2,i);
	}	

	// fill N and t Matrix
	CAdjustMatrix Fi(2,1);
	
	if (this->frameToCamera)
	{
		C2DPoint xcMM;
		CXYPoint obsMM;
		this->frameToCamera->transform(xcMM,xc);
		this->frameToCamera->transform(obsMM,obs);
		Fi(0,0) = xcMM[0] -obsMM[0];
		Fi(1,0) = xcMM[1] -obsMM[1];
	}
	else
	{
		Fi(0,0) = discrepancy[0];
		Fi(1,0) = discrepancy[1];
	}

	CAdjustMatrix ATX0 = AxyX0;
	CAdjustMatrix AT3DPoints = Axy3DPoints;
	CAdjustMatrix ATRotation = AxyRotation;
	ATX0.transpose();
	AT3DPoints.transpose();
	ATRotation.transpose();

	CAdjustMatrix P(2,2,0.0);

	if (this->frameToCamera)
	{
		Matrix B;
		this->frameToCamera->getDerivativesCoords(obs[0],obs[1],B);
		Matrix Ptmp = (B*obs.getCovar()*B.t()).i();
		P(0,0) = Ptmp.element(0,0);
		P(0,1) = Ptmp.element(0,1);
		P(1,0) = Ptmp.element(1,0);
		P(1,1) = Ptmp.element(1,1);	
	}
	else
	{	
		Matrix pbb = obs.getCovar().i();
	
		P(0,0) = pbb.element(0,0);
		P(0,1) = pbb.element(0,1);
		P(1,0) = pbb.element(1,0);
		P(1,1) = pbb.element(1,1);
	}

	if (robust || omitMarked)
	{
		double w0 , w1;
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 0, omitMarked, w0, P)) --nObs; 
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 1, omitMarked, w1, P)) --nObs; 
	}

	// compute the ATP blocks
	CAdjustMatrix ATX0_P =  ATX0 * P;
	CAdjustMatrix AT3DPoints_P = AT3DPoints * P;
	CAdjustMatrix ATRotation_P = ATRotation * P;

	// compute the separat blocks
	CAdjustMatrix AT_P_AX0 = ATX0_P * AxyX0;
	CAdjustMatrix AT_P_A3DPoints = AT3DPoints_P * Axy3DPoints;
	CAdjustMatrix AT_P_ARotation = ATRotation_P * AxyRotation;

	// the mixed blocks
	CAdjustMatrix ATX0_P_A3DPoints = ATX0_P * Axy3DPoints;
	CAdjustMatrix ATX0_P_ARotation = ATX0_P * AxyRotation;
	CAdjustMatrix ATRotation_P_A3DPoints = ATRotation_P * Axy3DPoints;




	int indexX0 = this->getParameterIndex(shiftPar);
	int indexRotation = this->getParameterIndex(rotPar);

	// fill the separat blocks
	AT_P_AX0.addToMatrix(indexX0,indexX0,N);
	AT_P_A3DPoints.addToMatrix(approx.id,approx.id,N);	
	AT_P_ARotation.addToMatrix(indexRotation,indexRotation,N);

	// the mixed blocks
	// X0 and 3dpoints
	ATX0_P_A3DPoints.addToMatrix(indexX0,approx.id,N,true);

	// X0 and rotation
	ATX0_P_ARotation.addToMatrix(indexX0,indexRotation,N,true);

	// rotation and 3dpoints
	ATRotation_P_A3DPoints.addToMatrix(indexRotation,approx.id,N,true);



	// fill t-vector
	AT_P_AX0 = ATX0_P * Fi;
	AT_P_AX0.addToMatrix(indexX0,0,t);

	AT_P_A3DPoints = AT3DPoints_P * Fi;
	AT_P_A3DPoints.addToMatrix(approx.id,0,t);

	ATRotation_P_A3DPoints = ATRotation_P * Fi;
	ATRotation_P_A3DPoints.addToMatrix(indexRotation,0,t);
	
	return nObs;
}


bool CFrameCameraModel::setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const
{
	return true;
}


bool CFrameCameraModel::setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const
{
	return true;
}


bool CFrameCameraModel::getAllParameters(Matrix &parms) const
{
	parms.ReSize(6,1);

	parms.element(0,0) = this->cameraPos.x;
	parms.element(1,0) = this->cameraPos.y;
	parms.element(2,0) = this->cameraPos.z;

	parms.element(3,0) = (*this->cameraRotation)[0];
	parms.element(4,0) = (*this->cameraRotation)[1];
	parms.element(5,0) = (*this->cameraRotation)[2];


	return true;
}

int CFrameCameraModel::coordPrecision() const
{
	return 2;
}


void CFrameCameraModel::getObservationNames(CCharStringArray &names) const
{
	names.RemoveAll();
    
    CCharString* name = names.Add();
    *name = "x";
	name = names.Add();
	*name = "y";

}


bool CFrameCameraModel::getAdjustableParameters(Matrix &parms) const
{
	parms.ReSize(6,1);

	parms.element(0,0) = this->cameraPos.x;
	parms.element(1,0) = this->cameraPos.y;
	parms.element(2,0) = this->cameraPos.z;

	parms.element(3,0) = (*this->cameraRotation)[0];
	parms.element(4,0) = (*this->cameraRotation)[1];
	parms.element(5,0) = (*this->cameraRotation)[2];

	return true;
}


bool CFrameCameraModel::insertParametersForAdjustment(Matrix &parms)
{
	int indexX0 = this->getParameterIndex(shiftPar);
	int indexRotation = this->getParameterIndex(rotPar);

	parms.element(indexX0    , 0) = this->cameraPos.x;
	parms.element(indexX0 + 1, 0) = this->cameraPos.y;
	parms.element(indexX0 + 2, 0) = this->cameraPos.z;

	parms.element(indexRotation    , 0) = (*this->cameraRotation)[0];
	parms.element(indexRotation + 1, 0) = (*this->cameraRotation)[1];
	parms.element(indexRotation + 2, 0) = (*this->cameraRotation)[2];




	return true;
}


bool CFrameCameraModel::setParametersFromAdjustment(const Matrix &parms)
{
	int indexX0 = this->getParameterIndex(shiftPar);
	int indexRotation = this->getParameterIndex(rotPar);

	this->cameraPos.x = parms.element(indexX0    , 0);
	this->cameraPos.y = parms.element(indexX0 + 1, 0);
	this->cameraPos.z = parms.element(indexX0 + 2, 0);

	(*this->cameraRotation)[0] = parms.element(indexRotation    , 0);
	(*this->cameraRotation)[1] = parms.element(indexRotation + 1, 0);
	(*this->cameraRotation)[2] = parms.element(indexRotation + 2, 0);

	this->cameraRotation->computeRotMatFromParameter();

	return true;
}


void CFrameCameraModel::resetSensor()
{
	this->cameraPos = CXYZPoint();
	if (this->cameraRotation)
		delete this->cameraRotation;

	this->cameraRotation = NULL;
	this->camera = NULL;
	this->cameraMounting = NULL;
	
	if (this->cameraToFrame)
		delete this->cameraToFrame;

	this->cameraToFrame = NULL;

	if (this->frameToCamera)
		delete this->frameToCamera;

	this->frameToCamera = NULL;


	this->hasValues = false;
}


Matrix CFrameCameraModel::getConstants() const
{
	Matrix parms(0,0);
	return parms;
}




CObsPoint CFrameCameraModel::calcRes(const Matrix &Delta, const CObsPoint &obs, const CObsPoint &obj)
{
	int dimension = obs.getDimension();
	CObsPoint residual(dimension);

	if (obs.id >= 0 || obj.id < 0) 
	{
		Matrix FiMat(dimension, 1);;
		Matrix *Qi = obs.getCovariance();

	
		C3DPoint xp,F;
		C2DPoint xc;
		this->cameraRotation->RT_times_x(xp,obj - this->cameraPos);	// computes xp = RT * (X-X0)
		xp -=this->cameraMounting->getShift();
		this->cameraMounting->getRotation().RT_times_x(F,xp);

		this->camera->transformObj2Obs(xc, F, this->cameraPos.z, obj[2], this->cameraToFrame);


		if (this->frameToCamera)
		{
			C2DPoint xcMM;
			CXYPoint obsMM;
			this->frameToCamera->transform(xcMM,xc);
			this->frameToCamera->transform(obsMM,obs);
			FiMat.element(0,0) = xcMM[0] -obsMM[0];
			FiMat.element(1,0) = xcMM[1] -obsMM[1];;
		}
		else
		{
			FiMat.element(0,0) = xc.x - obs[0];
			FiMat.element(1,0) = xc.y - obs[1];
		}





		// get the partials for the rotation angles
		CAdjustMatrix derivRotMat(9,3);
		this->cameraRotation->computeDerivation(derivRotMat); // this gives as the derivations rotMat elements r00 - r22 after the angles


		const CRotationMatrix &mountRotMat = this->cameraMounting->getRotation().getRotMat();



		
	
		// compute matrix dF/dRotMatElements
		xp = obj - this->cameraPos;
		CAdjustMatrix dFdRotMatElements(3,9);
		for (int i=0; i < 3; i++)
		{
			for (int k=0; k <3; k++)
			{
				for (int j=0; j < 3; j++)
					dFdRotMatElements(i,k + j*3) = mountRotMat(k,i) * xp[j];
			}
		}

		// and now dF/dAngles
		CAdjustMatrix dFdAngles = dFdRotMatElements * derivRotMat;

		CRotationMatrix tmpRotMat;

		mountRotMat.RT_times_RT(tmpRotMat,this->cameraRotation->getRotMat());
		//this->cameraRotation->getRotMat().RT_times_R(tmpRotMat,mountRotMat);


		CAdjustMatrix dFd3DPoints(3,3);	// the partials for the 3D Points
		for (int i=0; i<3;i++)
		{
			dFd3DPoints(0,i) = tmpRotMat(0,i);
			dFd3DPoints(1,i) = tmpRotMat(1,i);
			dFd3DPoints(2,i) = tmpRotMat(2,i);
		}

		CAdjustMatrix dFdX0 = dFd3DPoints * -1;

		// determine the number of Parameter for every group
		int nX0Parameter = 3;
		int n3DParameter = 3;
		int nRotationParameter = 3;

		// count the parameter
		int nParameter = nX0Parameter + n3DParameter + nRotationParameter;

		// get Index of every used parameter group
		int indexX0 = this->getParameterIndex(shiftPar);
		int indexRotation = this->getParameterIndex(rotPar);
		int index3DPoint = obj.id;
		


		Matrix AiMat(2,nParameter);	
		Matrix parameterAiMat(nParameter,1);
			
		double fac1 = -this->camera->getPrincipalDistance()/(F.z);
//		if (this->frameToCamera)
//		{	
			fac1 = -fac1;
//		}
		double fac2 = -fac1*F.x/F.z;
		double fac3 = -fac1*F.y/F.z;

		// build AiMat and parameterAiMat
		for (int i=0; i < nX0Parameter; i++)
		{
			AiMat.element(0,i) = fac1 * dFdX0(0,i) + fac2 * dFdX0(2,i);
			AiMat.element(1,i) = fac1 * dFdX0(1,i) + fac3 * dFdX0(2,i);
			parameterAiMat.element(i, 0) = Delta.element( indexX0 + i, 0);
		}

		int offset = nX0Parameter;
		for (int i=0; i< n3DParameter; i++)
		{
			AiMat.element(0,i + offset) = fac1 * dFd3DPoints(0,i) + fac2 * dFd3DPoints(2,i);
			AiMat.element(1,i + offset) = fac1 * dFd3DPoints(1,i) + fac3 * dFd3DPoints(2,i);
			parameterAiMat.element(offset + i, 0) = Delta.element( index3DPoint + i, 0);
		}  

		offset += n3DParameter;
		// compute the final partials for the attitudes
		for (int i=0; i < nRotationParameter; i++)
		{
			AiMat.element(0,i + offset) = fac1 *dFdAngles(0,i) + fac2 * dFdAngles(2,i);
			AiMat.element(1,i + offset) = fac1 *dFdAngles(1,i) + fac3 * dFdAngles(2,i);
			parameterAiMat.element(offset + i, 0) = Delta.element( indexRotation + i, 0);
		}

		// compute v
		FiMat = FiMat - AiMat * parameterAiMat;

		if (this->frameToCamera)
		{
			Matrix B;
			this->frameToCamera->getDerivativesCoords(obs[0],obs[1],B);
			
			Matrix P = (B*obs.getCovar()*B.t()).i();
				
			FiMat = -obs.getCovar()*B.t()*P*FiMat;

		}

  		for (int i=0; i < dimension; i++)
			residual[i] = FiMat.element(i,0);
	
	}

	
	return residual;
}




bool CFrameCameraModel::Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const
{
	Fi.ReSize(2, 1);
	// compute first the 3D image vector F = RmT*(ReT(X-X0) - Xm)
	C3DPoint xp,F;
	C2DPoint xc;
	this->cameraRotation->RT_times_x(xp,approx - this->cameraPos);	// computes xp = RT * (X-X0)
	xp -=this->cameraMounting->getShift();
	this->cameraMounting->getRotation().RT_times_x(F,xp);

	this->camera->transformObj2Obs(xc,F, this->cameraPos.z, approx[2], this->cameraToFrame);

	if (this->frameToCamera)
	{
		C2DPoint ObsPt(obs.element(0,0), obs.element(0,1));
		C2DPoint xcMM;
		C2DPoint obsMM;
		this->frameToCamera->transform(xcMM,xc);
		this->frameToCamera->transform(obsMM,ObsPt);
		Fi.element(0,0) = xcMM[0] -obsMM[0];
		Fi.element(1,0) = xcMM[1] -obsMM[1];;
	}
	else
	{
		Fi.element(0,0) = xc.x - obs.element(0,0);
		Fi.element(1,0) = xc.y - obs.element(0,1);
	}

	return true;
}



bool CFrameCameraModel::Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
						   Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						   Matrix &AiPar,        Matrix &AiPoint, 
						   const Matrix &obs, const C3DPoint &approx) const
{
	int nObs = 2;

	// compute first the 3D image vector F = RmT*(ReT(X-X0) - Xm)
	C3DPoint xp,F;
	C2DPoint xc;
	this->cameraRotation->RT_times_x(xp,approx - this->cameraPos);	// computes xp = RT * (X-X0)
	xp -=this->cameraMounting->getShift();
	this->cameraMounting->getRotation().RT_times_x(F,xp);

	this->camera->transformObj2Obs(xc,F, this->cameraPos.z, approx[2], this->cameraToFrame);

	CAdjustMatrix derivRotMat(9,3);
	this->cameraRotation->computeDerivation(derivRotMat); // this gives as the derivations rotMat elements r00 - r22 after the angles


	const CRotationMatrix &mountRotMat = this->cameraMounting->getRotation().getRotMat();
	


	// compute matrix dF/dRotMatElements
	xp = approx - this->cameraPos;
	CAdjustMatrix dFdRotMatElements(3,9);
	for (int i=0; i < 3; i++)
	{
		for (int k=0; k <3; k++)
		{
			for (int j=0; j < 3; j++)
				dFdRotMatElements(i,k + j*3) = mountRotMat(k,i) * xp[j];
		}
	}

	// and now dF/dAngles
	CAdjustMatrix dFdAngles = dFdRotMatElements * derivRotMat;

	CRotationMatrix tmpRotMat;

	mountRotMat.RT_times_RT(tmpRotMat,this->cameraRotation->getRotMat());

	CAdjustMatrix dFd3DPoints(3,3);	// the partials for the 3D Points
	for (int i=0; i<3;i++)
	{
		dFd3DPoints(0,i) = tmpRotMat(0,i);
		dFd3DPoints(1,i) = tmpRotMat(1,i);
		dFd3DPoints(2,i) = tmpRotMat(2,i);
	}

	CAdjustMatrix dFdX0 = dFd3DPoints * -1;

	// determine the number of Parameter for every group
	int nX0Parameter = 3;
	int n3DParameter = 3;
	int nRotationParameter = 3;

	AiPoint.ReSize(2,n3DParameter); 
	AiErp.ReSize(2,nX0Parameter);
	AiRot.ReSize(2,nRotationParameter);

	double fac1 = -this->camera->getPrincipalDistance()/(F.z);
//	if (this->frameToCamera)
//	{	
		fac1 = -fac1;
//	}

	double fac2 = -fac1*F.x/F.z;
	double fac3 = -fac1*F.y/F.z;

	// compute the final partial for the camera position
	for (int i=0; i < nX0Parameter; i++)
	{
		AiErp.element(0,i) = fac1 * dFdX0(0,i) + fac2 * dFdX0(2,i);
		AiErp.element(1,i) = fac1 * dFdX0(1,i) + fac3 * dFdX0(2,i);
	}

	// compute the final partials for the 3D points
	for (int i=0; i< n3DParameter; i++)
	{
		AiPoint.element(0,i) = fac1 * dFd3DPoints(0,i) + fac2 * dFd3DPoints(2,i);
		AiPoint.element(1,i) = fac1 * dFd3DPoints(1,i) + fac3 * dFd3DPoints(2,i);
	}

	// compute the final partials for the camera rotation
	for (int i=0; i < nRotationParameter; i++)
	{
		AiRot.element(0,i) = fac1 *dFdAngles(0,i) + fac2 * dFdAngles(2,i);
		AiRot.element(1,i) = fac1 *dFdAngles(1,i) + fac3 * dFdAngles(2,i);
	}	

	
	return true;
}


bool CFrameCameraModel::AiPars (Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
             Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
			 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const
{
	return true;
}


bool CFrameCameraModel::AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const
{
	int nObs = 2;

	// compute first the 3D image vector F = RmT*(ReT(X-X0) - Xm)
	C3DPoint xp,F;
	C2DPoint xc;
	this->cameraRotation->RT_times_x(xp,approx - this->cameraPos);	// computes xp = RT * (X-X0)
	xp -=this->cameraMounting->getShift();
	this->cameraMounting->getRotation().RT_times_x(F,xp);

	CRotationMatrix tmpRotMat;
	const CRotationMatrix &mountRotMat = this->cameraMounting->getRotation().getRotMat();
	mountRotMat.RT_times_RT(tmpRotMat,this->cameraRotation->getRotMat());

	CAdjustMatrix dFd3DPoints(3,3);	// the partials for the 3D Points
	for (int i=0; i<3;i++)
	{
		dFd3DPoints(0,i) = tmpRotMat(0,i);
		dFd3DPoints(1,i) = tmpRotMat(1,i);
		dFd3DPoints(2,i) = tmpRotMat(2,i);
	}

	CAdjustMatrix dFdX0 = dFd3DPoints * -1;

	// determine the number of Parameter for every group
	int n3DParameter = 3;

	Ai.ReSize(2,n3DParameter); 

	double fac1 = -this->camera->getPrincipalDistance()/(F.z);
//	if (this->frameToCamera)
//	{	
		fac1 = -fac1;
//	}

	double fac2 = -fac1*F.x/F.z;
	double fac3 = -fac1*F.y/F.z;


	// compute the final partials for the 3D points
	for (int i=0; i< n3DParameter; i++)
	{
		Ai.element(0,i) = fac1 * dFd3DPoints(0,i) + fac2 * dFd3DPoints(2,i);
		Ai.element(1,i) = fac1 * dFd3DPoints(1,i) + fac3 * dFd3DPoints(2,i);
	}
	
	return true;
}



bool CFrameCameraModel::AiBiFi(Matrix &Fi, Matrix &Bi, 
			Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
			Matrix &AiPar,        Matrix &AiPoint,
			const Matrix &obs, const C3DPoint &approx)
{
	return true;
}


bool CFrameCameraModel::Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const
{
	Bi.ReSize(2, 2);

	if (this->frameToCamera)
	{
		this->frameToCamera->getDerivativesCoords(obs.element(0,0),obs.element(0,1), Bi);
	}
	else
	{	
		Bi.element(0,0) = -1.0; Bi.element(0,1) = -0.0;
		Bi.element(1,0) = -0.0; Bi.element(1,1) = -1.0;
	}

	
	return true;
}


void CFrameCameraModel::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
    StationNames.RemoveAll();
	PointNames.RemoveAll();


	StationNames.Add("Camera Position (Rotation)   Parameter[m]	       Sigma[m]\n      "
					 "                    X ");
	StationNames.Add("                    Y ");
	StationNames.Add("                    Z ");


	if (this->cameraRotation && this->cameraRotation->getType() == RPY)	
	{
		StationNames.Add("Camera Rotation              Parameter[rad]	   Sigma[rad]\n      "
						 "                 Roll ");
		StationNames.Add("                Pitch ");
		StationNames.Add("                  Yaw ");
	}
	else
	{
		StationNames.Add("Camera Rotation              Parameter[rad]	   Sigma[rad]\n      "
						 "                Omega ");
		StationNames.Add("                  Phi ");
		StationNames.Add("                Kappa ");
	}

}



void CFrameCameraModel::addDirectObservations(Matrix &N, Matrix &t)
{

}


void CFrameCameraModel::getName(CCharString& names) const
{
    names = "Frame Camera Bundle";
}
