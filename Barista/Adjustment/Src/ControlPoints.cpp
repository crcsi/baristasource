/**
 * @class CControlPointModel
 * CControlPointModel stores and handles control points w.r.t. sensor modeling
 */
#include "newmat.h"
#include "ControlPoints.h"
#include "2DPoint.h"
#include "3DPoint.h"
#include "XYPoint.h"
#include "XYZPoint.h"
#include "StructuredFile.h"
#include "AdjustMatrix.h"
#include "utilities.h"
#include "3Dplane.h"
#include "3DadjPlane.h"
#include "XYZLinesAdj.h"

CControlPointModel::CControlPointModel() : 
				CSensorModel(false, "Control Points", 0, 0, 0, 0, 0, 0, 0, 3, 3)
{
}

/**
 * contructor from existing rpc
 * @param rpc
 */
CControlPointModel::CControlPointModel(const CControlPointModel &cp) : 
				CSensorModel(cp)
{
}

CControlPointModel::~CControlPointModel(void)
{
}

void CControlPointModel::copy(const CControlPointModel &cp)
{
	initFromModel(cp);
    this->reduction = cp.reduction;

   this->informListeners_elementsChanged();  
}

/**
 * Sets rpc parameter from Matrix
 * @param parms
 */
void CControlPointModel::setAll(const Matrix &parms)
{

}


/**
* Transforms an xyz point to its cooresponding xy point
* in image space
* The xyz point is assumed to be Lat Long Height expressed in the
* appropriate zone for the RPC.
*
* @param xyz The xyz point
* @return the transformed xy point
*/
void CControlPointModel::transformObj2Obs(C2DPoint &obs, const C3DPoint &obj) const
{
    obs.x = obj.x;
	obs.y = obj.y;
}

/**
* Transforms an xyz point to its cooresponding xyz point
* in image space
* The xyz point is assumed to be Lat Long Height expressed in the
* appropriate zone for the RPC.
*
* @param xyz The xyz point
* @return the transformed xyz point
*/
void CControlPointModel::transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const
{
    obs.x = obj.x;
	obs.y = obj.y;
	obs.z = obj.z;
}

/**
 * monoplots a single point
 * @param  xyz xyzpoint xy xypoint, Height Height
 * @returns true/false depending if monoplot was succesful
 */
bool CControlPointModel::transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const
{
    
	obj.x = obs.x;
	obj.y = obs.y;
	obj.z = Height;
   
    return true;
}
    
bool CControlPointModel::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const
{
	obj.x = obs.x;
	obj.y = obs.y;
	obj.z = plane.getHeight(C2DPoint(obj.x, obj.y));

	return true;
};

bool CControlPointModel::transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const
{
	obj.x = obs.x;
	obj.y = obs.y;
	obj.z = plane.getHeight(C2DPoint(obj.x, obj.y));

	CXYZPoint p1(obj); 
	p1.z += 1.0;

    CXYZHomoSegment seg(obj, p1);
		 
	eIntersect res = seg.getIntersection(plane, obj);

	return res == eIntersection;
};

bool CControlPointModel::transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const
{
	obj.setLabel(obs.getLabel());
   
	obj[0] = obs[0];
	obj[1] = obs[1];
	obj[2] = Height;

	Matrix *covObj = obj.getCovariance();
	Matrix *covObs = obs.getCovariance();
	for (int i = 0; i < covObs->Nrows(); ++i)
	{
		covObj->element(i,i) = covObs->element(i,i);
		for (int j = i; j < covObs->Ncols(); ++j)
		{
			covObj->element(i,j) = covObs->element(i,j);
			covObj->element(j,i) = covObs->element(j,i);
		}
	}

	if (covObs->Ncols() < 2) covObj->element(2,2) = sigmaZ * sigmaZ;
	
    return true;
};

/**
 * monoplots a single point
 * @param  xyz xyzpoint xy xypoint, Height Height
 * @returns true/false depending if monoplot was succesful
 */
bool CControlPointModel::transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const
{
	obj.x = obs.x;
	obj.y = obs.y;
	obj.z = obs.z;
    
	return true;
}


/**
* Gets a matrix of the parameters
*
* @return the parameters backed into a matrix
*/
bool CControlPointModel::getAllParameters(Matrix &parms) const
{
    parms.ReSize(3, 1);

    parms.element(0, 0) = reduction.x;
	parms.element(1, 0) = reduction.y;
	parms.element(2, 0) = reduction.z;

    return true;
}

bool CControlPointModel::getAdjustableParameters(Matrix &parms) const
{
    parms.ReSize(0, 0);

    return true;
}

/**
* Gets a matrix of the parameters
*
*/
Matrix CControlPointModel::getConstants() const
{
    Matrix parms(3,1);
    getAllParameters(parms);
    return parms;
}


/**
 * reads PRC parameters from file, ckecks the format and reads Ikonos or QuickBird
 * @param  const char* filename
 * @returns void
 */
bool CControlPointModel::read(const char *filename)
{
	return true;
}

/**
 * writes PRC parameter file as comma separated file 
 * @param  filename
 * @returns true/false
 */
bool CControlPointModel::writeTextFile(const char* filename) const
{
	return true;
}

/**
 * Serialized storage of a CControlPointModel to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CControlPointModel::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("conpoiFilename", this->getFileName()->GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("hasValues", this->gethasValues());

    token = pStructuredFileSection->addToken();
    token->setValue("offset_X", this->reduction.x);

    token = pStructuredFileSection->addToken();
    token->setValue("offset_Y", this->reduction.y);

    token = pStructuredFileSection->addToken();
    token->setValue("offset_Z", this->reduction.z);

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->refSystem.serializeStore(child);
}

/**
 * Restores a CControlPointModel from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CControlPointModel::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);


	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("conpoiFilename"))
            this->setFileName( token->getValue());
		else if(key.CompareNoCase("hasValues"))
            this->hasValues = token->getBoolValue();
		else if(key.CompareNoCase("offset_X"))
            this->reduction.x = token->getDoubleValue();
		else if(key.CompareNoCase("offset_Y"))
            this->reduction.y = token->getDoubleValue();
		else if(key.CompareNoCase("offset_Z"))
            this->reduction.z = token->getDoubleValue();
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refSystem.serializeRestore(child);
        }
    }
}

void CControlPointModel::setAdjustableValues(const Matrix &parms)
{
}

int CControlPointModel::accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
								   CObsPoint &discrepancy,  CObsPoint &normalisedDiscrepancy, 
								   CRobustWeightFunction *robust, bool omitMarked)
{
	int nObs = 3;

	Matrix Pbb = obs.getCovariance()->i();
	CAdjustMatrix PBB(Pbb.Nrows(), Pbb.Ncols());

	for (int i = 0; i < Pbb.Nrows(); i++)
	{
		PBB(i,i) = Pbb.element(i,i);
		for (int j = i + 1; j < Pbb.Ncols(); j++)
		{
			PBB(i,j) = PBB(j,i) = Pbb.element(i,j);
		}
	}

	CAdjustMatrix Fi (3,1);

	discrepancy = approx - obs;

	normalisedDiscrepancy[0] = fabs(discrepancy[0]) * sqrt(PBB(0,0));
	normalisedDiscrepancy[1] = fabs(discrepancy[1]) * sqrt(PBB(1,1));
	normalisedDiscrepancy[2] = fabs(discrepancy[2]) * sqrt(PBB(2,2));

	if (robust || omitMarked)
	{
		double w0 , w1, w2;
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 0, omitMarked, w0, PBB)) --nObs; 
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 1, omitMarked, w1, PBB)) --nObs; 
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 2, omitMarked, w2, PBB)) --nObs; 
	}

    Fi(0,0) = discrepancy[0];
    Fi(1,0) = discrepancy[1];
    Fi(2,0) = discrepancy[2];

	CAdjustMatrix PFi = PBB * Fi;

	for (int r = 0, R = approx.id; r < 3; r++, R++)
    {
		for (int c = 0, C = approx.id; c < 3; c++, C++)
        {
			N.element(R, C) += PBB(r,c);
        }
    }

	PFi.addToMatrix(approx.id, 0, t);

	return nObs;
};

bool CControlPointModel::Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
							Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
							Matrix &AiPar,        Matrix &AiPoint, 
							const Matrix &obs, const C3DPoint &approx) const
{
	AiErp.ReSize(0,0);
	AiRot.ReSize(0,0);
	AiSca.ReSize(0,0);
	AiMountShift.ReSize(0,0);
	AiMountRot.ReSize(0,0);
	AiIrp.ReSize(0,0);
	AiPar.ReSize(0,0);
	
	AiPoint.ReSize(this->nObsPerPoint, this->nActualStationPars + this->nParsPerPoint);

    AiPoint.element(0, 0) = 1.0;
    AiPoint.element(0, 1) = 0.0;
    AiPoint.element(0, 2) = 0.0;

    AiPoint.element(1, 0) = 0.0;
    AiPoint.element(1, 1) = 1.0;
    AiPoint.element(1, 2) = 0.0;

	AiPoint.element(2, 0) = 0.0;
    AiPoint.element(2, 1) = 0.0;
    AiPoint.element(2, 2) = 1.0;

    return true;
}

bool CControlPointModel::AiPars(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
								Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
								Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const
{
	AiErp.ReSize(0,0);
	AiRot.ReSize(0,0);
	AiSca.ReSize(0,0);
	AiMountShift.ReSize(0,0);
	AiMountRot.ReSize(0,0);
	AiIrp.ReSize(0,0);
	AiPar.ReSize(0,0);
    return true;
}


bool CControlPointModel::AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const
{
    Ai.ReSize(this->nObsPerPoint, this->nParsPerPoint);
   
    Ai.element(0, 0) = 1.0;
    Ai.element(0, 1) = 0.0;
    Ai.element(0, 2) = 0.0;

    Ai.element(1, 0) = 0.0;
    Ai.element(1, 1) = 1.0;
    Ai.element(1, 2) = 0.0;

	Ai.element(2, 0) = 0.0;
    Ai.element(2, 1) = 0.0;
    Ai.element(2, 2) = 1.0;

    return true;
}

bool CControlPointModel::Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const
{
    Fi.ReSize(this->nObsPerPoint, 1);

    double x = obs(1, 1);
    double y = obs(1, 2);
    double z = obs(1, 3);

    Fi.element(0, 0) = approx.x - x;
    Fi.element(1, 0) = approx.y - y;
    Fi.element(2, 0) = approx.z - z;

    return true;
}


bool CControlPointModel::AiBiFi(Matrix &Fi, Matrix &Bi, 
								Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
								Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
								Matrix &AiPar,        Matrix &AiPoint,
								const Matrix &obs, const C3DPoint &approx)
{
	AiErp.ReSize(0,0);
	AiRot.ReSize(0,0);
	AiSca.ReSize(0,0);
	AiMountShift.ReSize(0,0);
	AiMountRot.ReSize(0,0);
	AiIrp.ReSize(0,0);
	AiPar.ReSize(0,0);
	
	AiPoint.ReSize(this->nObsPerPoint, this->nActualStationPars + this->nParsPerPoint);
	Bi.ReSize(this->nObsPerPoint, this->nObsPerPoint);
	Fi.ReSize(this->nObsPerPoint, 1);

    AiPoint.element(0, 0) = 1.0;
    AiPoint.element(0, 1) = 0.0;
    AiPoint.element(0, 2) = 0.0;

    AiPoint.element(1, 0) = 0.0;
    AiPoint.element(1, 1) = 1.0;
    AiPoint.element(1, 2) = 0.0;

	AiPoint.element(2, 0) = 0.0;
    AiPoint.element(2, 1) = 0.0;
    AiPoint.element(2, 2) = 1.0;

	Bi.element(0, 0) = -1;
    Bi.element(0, 1) =  0;
    Bi.element(0, 2) =  0;

	Bi.element(1, 0) =  0;
    Bi.element(1, 1) = -1;
    Bi.element(1, 2) =  0;

	Bi.element(2, 0) =  0;
    Bi.element(2, 1) =  0;
    Bi.element(2, 2) = -1;
 
    double x = obs.element(0,0);
    double y = obs.element(0,1);
    double z = obs.element(0,2);

    Fi.element(0, 0) = approx.x - x ;
    Fi.element(1, 0) = approx.y - y ;
    Fi.element(2, 0) = approx.z - z;

    return true;
}
bool CControlPointModel::Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const
{
    Bi.ReSize(this->nObsPerPoint, this->nObsPerPoint);

	Bi.element(0, 0) = -1;
    Bi.element(0, 1) =  0;
    Bi.element(0, 2) =  0;

	Bi.element(1, 0) =  0;
    Bi.element(1, 1) = -1;
    Bi.element(1, 2) =  0;

	Bi.element(2, 0) =  0;
    Bi.element(2, 1) =  0;
    Bi.element(2, 2) = -1;

    return true;
}

void CControlPointModel::getName(CCharString& name) const
{
    name = "Control Points";
}

void CControlPointModel::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
    CCharString* str;
   
	StationNames.RemoveAll();
	PointNames.RemoveAll();

	str=PointNames.Add();
    *str = "X";

    str=PointNames.Add();
    *str = "Y";

    str=PointNames.Add();
    *str = "Z";
}


void CControlPointModel::addDirectObservations(Matrix &N, Matrix &t)
{
};

bool CControlPointModel::setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const
{
	return true;
};
	 
bool CControlPointModel::setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const
{
	return true;
};

bool CControlPointModel::insertParametersForAdjustment(Matrix &parms) 
{
	return true;
};


bool CControlPointModel::setParametersFromAdjustment(const Matrix &parms) 
{
	return true;
};


void CControlPointModel::getObservationNames(CCharStringArray &names) const
{
	names.RemoveAll();

    CCharString* name = names.Add();
    *name = "X";
	name = names.Add();
    *name = "Y";
	name = names.Add();
    *name = "Z";
}

int CControlPointModel::coordPrecision() const
{
	if (this->refSystem.getCoordinateType() == eGEOGRAPHIC) return 9;

	return 3;
};

CCharString CControlPointModel::obsUnitsString() const
{
	if (this->refSystem.getCoordinateType() == eGEOGRAPHIC) return "[deg]";
	return "[m]";
};

CCharString CControlPointModel::diffUnitsString() const
{
	return "[m]";
};
	 
void CControlPointModel::convertToDifferentialUnits(CObsPoint &out, const CObsPoint &in) const
{
	out = in;
	if (this->refSystem.getCoordinateType() == eGEOGRAPHIC) 
	{
		double factR = (this->refSystem.getSpheroid().getSemiMajor() + 
			            this->refSystem.getSpheroid().getSemiMajor()) / 360.0 * M_PI;

		out[0] *= factR;
		out[1] *= factR;
	}
};

void CControlPointModel::crop(int offsetX, int offsetY, int newWidth, int newHeight)
{
};
