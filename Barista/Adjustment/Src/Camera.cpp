#include "Camera.h"
#include "DistortionFactory.h"


CCamera::CCamera(void):scale(1),interiorReferencePoint(0,0,0),nRows(0),nCols(0),distortion(0),type(eNoType),
	callListener(true)

{
	this->interiorReferencePoint.setParent(this);
}

CCamera::CCamera(const CCamera& camera) : scale(camera.scale),interiorReferencePoint(camera.interiorReferencePoint),nRows(camera.nRows),
		nCols(camera.nCols),type(camera.type)
{
	this->interiorReferencePoint.setParent(this);
	this->name = camera.name;
	this->distortion = CDistortionFactory::createDistortion(camera.distortion);

}

CCamera::~CCamera(void)
{
	if (this->distortion)
		delete this->distortion;

	this->distortion = NULL;
}

CCamera &CCamera::operator = (const CCamera & c)
{

	bool changeListenerStatus = false;
	if (this->callListener)
	{
		this->callListener = false;
		changeListenerStatus = true;
	}

	this->interiorReferencePoint = c.interiorReferencePoint;
	this->nRows = c.nRows;
	this->nCols = c.nCols;
	this->name = c.name;
	this->scale = c.scale;

	if (this->distortion)
		delete this->distortion;

	this->distortion = CDistortionFactory::createDistortion(c.distortion);
	this->type = c.type;


	if (changeListenerStatus)
		this->callListener = true;

	if (this->callListener)
		this->informListeners_elementsChanged(this->getClassName().c_str());  



	return *this;
}



bool CCamera::AiIRP(CAdjustMatrix &AiIrp,const C2DPoint &obs, const C2DPoint &approx,int activeIrpPar) const
{

	return true;
}


bool CCamera::AiADP(CAdjustMatrix &AiAdp,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar) const
{
	if (this->distortion)
		return this->distortion->Ai(AiAdp,obs,approx,activeAddPar);
	else
		return false;
}


bool CCamera::Bi(CAdjustMatrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const
{
	if (this->distortion)
		return this->distortion->Bi(Bi,obs,approx,activeAddPar);
	else
		return false;
}

bool CCamera::Bi(Matrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const
{
	if (this->distortion)
		return this->distortion->Bi(Bi,obs,approx,activeAddPar);
	else
		return false;
}


bool CCamera::insertIRPParametersForAdjustment(Matrix &parms,int indexIrp,int activeIrpPar)const
{
	return true;
}

bool CCamera::insertADPParametersForAdjustment(Matrix &parms,int indexAdp,int activeAddPar)const
{
	if (this->distortion)
		return this->distortion->insertParametersForAdjustment(parms,indexAdp,activeAddPar);
	else
	return false;
}
	
bool CCamera::setIRPParametersFromAdjustment(const Matrix &parms,int indexIrp,int activeIrpPar)
{
	return true;
}

bool CCamera::setADPParametersFromAdjustment(const Matrix &parms,int indexAdp,int activeAddPar)
{
	bool ret = false;
	if (this->distortion)
		ret = this->distortion->setParametersFromAdjustment(parms,indexAdp,activeAddPar);
	
	if (ret)
	{
		this->informListeners_elementsChanged(this->getClassName().c_str());  
	}

	return ret;
}




void CCamera::printIRPParameter(ostrstream &protocol,int activeIrpPar) const
{

}

void CCamera::printADPParameter(ostrstream &protocol,int activeAdpPar) const
{
	if (this->distortion)
		return this->distortion->printParameter(protocol,activeAdpPar);
}


void CCamera::setIRPParameterCovariance(const Matrix &covar,int activeIrpPar)
{

}

void CCamera::setADPParameterCovariance(const Matrix &covar,int activeAdpPar)
{
	if (this->distortion)
	{
		this->distortion->setParameterCovariance(covar,activeAdpPar);
		
		this->informListeners_elementsChanged(this->getClassName().c_str());  
	}
}


void CCamera::addDirectObservationsAdp(Matrix &N, Matrix &t,int indexAdp,int activeAddPar)const
{
	if (this->distortion)
		this->distortion->addDirectObservations(N,t,indexAdp,activeAddPar);
}

double CCamera::getResidualsDirectAdpObservations(const Matrix &deltaPar, Matrix &res,int indexAdp,int activeAddPar)const
{
	if (this->distortion)
		return this->distortion->getResidualsDirectObservations(deltaPar,res,indexAdp,activeAddPar);
	return 0.0;
}







void CCamera::serializeStore(CStructuredFileSection* pStructuredFileSection)
{

	CToken* pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("nCols",this->nCols);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("name",this->name.GetChar());

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("nRows",this->nCols);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("scale",this->scale);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("distortionType",this->distortion != NULL ? this->distortion->getDistortionType() : eNotInitialized);

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->interiorReferencePoint.serializeStore(child);

	if (this->distortion)
	{
		CStructuredFileSection* child = pStructuredFileSection->addChild();
		this->distortion->serializeStore(child);
	}
}

void CCamera::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	
	DistortionType type = eNotInitialized;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("nCols"))
			this->nCols = token->getIntValue();
		else if(key.CompareNoCase("nRows"))
            this->nRows = token->getIntValue();
		else if(key.CompareNoCase("scale"))
            this->scale = token->getDoubleValue();
		else if(key.CompareNoCase("name"))
            this->name = token->getValue();
		else if (key.CompareNoCase("distortionType"))
			type = (DistortionType) token->getIntValue();
    }

	this->distortion = CDistortionFactory::createDistortion(type);
	CCharString distortionName("");
	if (this->distortion)
		distortionName = this->distortion->getClassName();


    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CXYZPoint"))
        {
            this->interiorReferencePoint.serializeRestore(child);
        }
		else if (child->getName().CompareNoCase(distortionName))
        {
            this->distortion->serializeRestore(child);
        }
    }
}

 void CCamera::reconnectPointers()
 {
	 CSerializable::reconnectPointers();
	 this->interiorReferencePoint.reconnectPointers();
 }

 void CCamera::resetPointers()
 {
	 CSerializable::resetPointers();

	 this->interiorReferencePoint.resetPointers();
 }


 void CCamera::setDistortion(DistortionType type,const doubles& parameter)
 {
	if (this->distortion)
		delete this->distortion;

	this->distortion = CDistortionFactory::createDistortion(type);
	
	if (this->distortion)
		this->distortion->setDistortion(parameter);
 }


void CCamera::setDistortion(CDistortion* distortion)
{
	if (this->distortion)
		delete this->distortion;

	this->distortion = distortion;
}

 void CCamera::resetADPParameter()
 {
	 if (this->distortion)
	 {
		this->distortion->resetParameter();
		this->informListeners_elementsChanged(this->getClassName().c_str());  
	 }
 }

 void CCamera::resetIRPParameter()
 {


 }


void CCamera::informListener()
{
	this->informListeners_elementsChanged(this->getClassName().c_str());  
}

void CCamera::goingToBeDeleted()
{
	this->informListeners_elementDeleted();
}