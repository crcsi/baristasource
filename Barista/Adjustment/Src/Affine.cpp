/**
 * @class CAffine
 * CAffine stores and handles affine parameters
 * concerning import/export, sensor modeling
 */
#include <float.h>
#include "newmat.h"
#include "Affine.h"

#include "2DPoint.h"
#include "3DPoint.h"
#include "XYPoint.h"
#include "XYZPoint.h"
#include "XYPointArray.h"
#include "XYZPointArray.h"
#include "MUIntegerArray.h"
#include "AdjustMatrix.h"
#include "3Dplane.h"
#include "3DadjPlane.h"
#include "XYZLinesAdj.h"
#include "TextFile.h"



CAffine::CAffine() : CSensorModel(false, "Affine", 0, 0, 0, 0, 0, 0, 8, 3, 2),
				a1(1.0), a2(0.0), a3(0.0), a4(0.0), 
				a5(0.0), a6(1.0), a7(0.0), a8(0.0)
{
}

/**
 * contructor from existing affine
 * @param affine
 */
CAffine::CAffine(const CAffine &affine) : CSensorModel(affine)
{
    this->a1 = affine.a1;
    this->a2 = affine.a2;
    this->a3 = affine.a3;
    this->a4 = affine.a4;
    this->a5 = affine.a5;
    this->a6 = affine.a6;
    this->a7 = affine.a7;
    this->a8 = affine.a8;
}

CAffine::CAffine(const Matrix &parms) : CSensorModel(true, "Affine",  0, 0, 0, 0, 0, 0, 8, 3, 2)
{
    this->a1 = parms.element(0, 0);
    this->a2 = parms.element(1, 0);
    this->a3 = parms.element(2, 0);
    this->a4 = parms.element(3, 0);
    this->a5 = parms.element(4, 0);
    this->a6 = parms.element(5, 0);
    this->a7 = parms.element(6, 0);
    this->a8 = parms.element(7, 0);
}

CAffine::~CAffine()
{
}

CAffine &CAffine::operator =(const CAffine &affine)
{
	if (this != &affine)
	{
		this->initFromModel(affine);

		this->a1 = affine.a1;
		this->a2 = affine.a2;
		this->a3 = affine.a3;
		this->a4 = affine.a4;
		this->a5 = affine.a5;
		this->a6 = affine.a6;
		this->a7 = affine.a7;
		this->a8 = affine.a8;
	
		this->nStationConstants = affine.nStationConstants;
		this->stationConstants  = affine.stationConstants;;
	}

	return *this;
};

/**
 * Sets affine parameter from Matrix
 * @param parms
 */
void CAffine::setAll(const Matrix &parms)
{
    this->hasValues = true;

    this->a1 = parms.element(0, 0);
    this->a2 = parms.element(1, 0);
    this->a3 = parms.element(2, 0);
    this->a4 = parms.element(3, 0);
    this->a5 = parms.element(4, 0);
    this->a6 = parms.element(5, 0);
    this->a7 = parms.element(6, 0);
    this->a8 = parms.element(7, 0);

	this->informListeners_elementsChanged();

}

    /**
     * Transforms an xyz point to its cooresponding xy point
     * in image space
     * The xyz point is assumed to be in the same coordinate system
     * as the used Affine parameters.
     *
     * @param xyz The xyz point
     * @return the transformed xy point
     */
void CAffine::transformObj2Obs(C2DPoint &obs, const C3DPoint &obj) const
{
    obs.x = a1 * obj.x + a2 * obj.y + a3 * obj.z + a4;
    obs.y = a5 * obj.x + a6 * obj.y + a7 * obj.z + a8;
}
void CAffine::transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const
{
    obs.x = a1 * obj.x + a2 * obj.y + a3 * obj.z + a4;
    obs.y = a5 * obj.x + a6 * obj.y + a7 * obj.z + a8;
	obs.z = 0.0f;
}

bool CAffine::transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const
{
    obj.z = Height;
	
	double x = obs.x - this->a3 * Height - this->a4;
	double y = obs.y - this->a7 * Height - this->a8;
	double detDiv = 1.0f / (this->a1 * this->a6 - this->a2 * this->a5);

    obj.x = ( this->a6 * x - this->a2 * y) * detDiv;
    obj.y = (-this->a5 * x + this->a1 * y) * detDiv;

    return true;
}

bool CAffine::transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const
{
    obj.z = obs.z;
	double x = obs.x - this->a3 * obj.z - this->a4;
	double y = obs.y - this->a7 * obj.z - this->a8;
	double detDiv = 1.0f / (this->a1 * this->a6 - this->a2 * this->a5);

    obj.x = ( this->a6 * x - this->a2 * y) * detDiv;
    obj.y = (-this->a5 * x + this->a1 * y) * detDiv;

    return true;
}
   
bool CAffine::transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const
{
	obj.setLabel(obs.getLabel());
    obj[2] = Height;
	double x = obs[0] - this->a3 * Height - this->a4;
	double y = obs[1] - this->a7 * Height - this->a8;
	double det = this->a1 * this->a6 - this->a2 * this->a5;
	bool ret = (fabs(det) > FLT_EPSILON);
	if (ret)
	{
		double detDiv = 1.0f / det;

		obj[0] = ( this->a6 * x - this->a2 * y) * detDiv;
		obj[1] = (-this->a5 * x + this->a1 * y) * detDiv;

		Matrix A(2,3);
		A.element(0,0) = this->a1; A.element(0,1) = this->a2; A.element(0,2) = this->a3; 
		A.element(1,0) = this->a5; A.element(1,1) = this->a6; A.element(1,2) = this->a7; 
		Matrix N = A.t() * obs.getCovar() * A;
		N.element(2,2) += 1.0 / (double) sigmaZ * (double) sigmaZ;
		obj.setCovariance(N.i());
	}

    return ret;
};

bool CAffine::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const
{
	bool ret = true;
	CXYZPoint P1, P2;
	C3DPoint normal(plane.getNormal());
	Matrix A(3,3);
	ColumnVector b(3);
	A.element(0,0) = this->a1;	A.element(0,1) = this->a2;	A.element(0,2) = this->a3; b.element(0) = obs.x - this->a4;
	A.element(1,0) = this->a5;	A.element(1,1) = this->a6;	A.element(1,2) = this->a7; b.element(1) = obs.y - this->a8;
	A.element(2,0) = normal.x;	A.element(2,1) = normal.y;	A.element(2,2) = normal.z; b.element(2) = plane.getPlaneConstant();

	try
	{
		ColumnVector x = A.i() * b;
		obj.x = x.element(0);
		obj.y = x.element(1);
		obj.z = x.element(2);
	}
	catch(Exception& ex)
	{    
		ret = false;
	}

	return ret;
};

bool CAffine::transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const
{
	CXYZPoint P1, P2;
	this->transformObs2Obj(P1, obs, 0.0, 0.0);
	this->transformObs2Obj(P2, obs, 1.0, 0.0);

    CXYZHomoSegment seg(P1, P2);
		 
	eIntersect res = seg.getIntersection(plane, obj);

	return res == eIntersection;
};

void CAffine::setAdjustableValues(const Matrix &parms)
{
    this->a1 = parms.element(0, 0);
    this->a2 = parms.element(1, 0);
    this->a3 = parms.element(2, 0);
    this->a4 = parms.element(3, 0);
    this->a5 = parms.element(4, 0);
    this->a6 = parms.element(5, 0);
    this->a7 = parms.element(6, 0);
    this->a8 = parms.element(7, 0);

    this->informListeners_elementsChanged();

    this->hasValues = true;
}

/**
* Gets a matrix of the parameters
*
* @return the parameters backed into a matrix
*/

bool CAffine::getAllParameters(Matrix &parms) const
{
	parms.ReSize( 8, 1);

    parms.element(0, 0) = this->a1;
    parms.element(1, 0) = this->a2;
    parms.element(2, 0) = this->a3;
    parms.element(3, 0) = this->a4;
    parms.element(4, 0) = this->a5;
    parms.element(5, 0) = this->a6;
    parms.element(6, 0) = this->a7;
    parms.element(7, 0) = this->a8;

    return true;
};

bool CAffine::insertParametersForAdjustment(Matrix &parms) 
{
	if (this->parameterIndex[addPar] >= 0)
	{
		parms.element(this->parameterIndex[addPar]    , 0) = this->a1;
		parms.element(this->parameterIndex[addPar] + 1, 0) = this->a2;
		parms.element(this->parameterIndex[addPar] + 2, 0) = this->a3;
		parms.element(this->parameterIndex[addPar] + 3, 0) = this->a4;
		parms.element(this->parameterIndex[addPar] + 4, 0) = this->a5;
		parms.element(this->parameterIndex[addPar] + 5, 0) = this->a6;
		parms.element(this->parameterIndex[addPar] + 6, 0) = this->a7;
		parms.element(this->parameterIndex[addPar] + 7, 0) = this->a8;
	}
	return true;
};

bool CAffine::setParametersFromAdjustment(const Matrix &parms) 
{
	if (this->parameterIndex[addPar] >= 0)
	{
		this->a1 = parms.element(this->parameterIndex[addPar]    , 0);
		this->a2 = parms.element(this->parameterIndex[addPar] + 1, 0);
		this->a3 = parms.element(this->parameterIndex[addPar] + 2, 0);
		this->a4 = parms.element(this->parameterIndex[addPar] + 3, 0);
		this->a5 = parms.element(this->parameterIndex[addPar] + 4, 0);
		this->a6 = parms.element(this->parameterIndex[addPar] + 5, 0);
		this->a7 = parms.element(this->parameterIndex[addPar] + 6, 0);
		this->a8 = parms.element(this->parameterIndex[addPar] + 7, 0);
	}

	return true;
}

Matrix CAffine::getConstants() const
{
	Matrix parms( 0, 0);
    return parms;
};

/**
 * freeAffine modifies the covariance matrix of the affine parameter
 * setting the weight to "free"
 */
void CAffine::freeAffine() //Covariance = Matrix.identity(8, 8).times(100000000);
{
    this->covariance.ReSize(8,8);
    this->covariance = 0;

    for (int i = 0; i < 8; i++)
        this->covariance.element(i, i) = 100000000;
}


/**
 * resetSensor modifies the covariance matrix of the additional parameter
 * setting the weight to "free", by calling freeAffine and sets affine
 * parameters to 0
 */
void CAffine::resetSensor()
{
    this->freeAffine();

    this->a1 = 0;    
    this->a2 = 0;    
    this->a3 = 0;    
    this->a4 = 0;    
    this->a5 = 0;    
    this->a6 = 0;    
    this->a7 = 0;    
    this->a8 = 0;

	this->hasValues = false;
}


/**
 * reads affine parameter file (text format and sets affine values 
 * @param  char data
 * @returns true/false
 */
bool CAffine::read(const char* filename)
{  
	bool ret = true;
    CCharString str;

    CTextFile file(filename, CTextFile::READ);

    int i = 0;

    Matrix parameters(8, 1);
    for (;;)
    {
        if (!file.ReadLine(str))
		{
			ret = false;
			break;
		}

        const char* string = str.GetChar();
        char buf[1024];
        double value;
        
        ::sscanf(string, "%s%lf", buf, &value);
        parameters.element(i, 0) = value;
        i++;
    }

    this->setAll(parameters);
    this->FileName = filename;
	return ret;
}



/**
 * writes affine parameter file as space separated file 
 * @param  filename
 * @returns true/false
 */
bool CAffine::writeTextFile(const char* filename) const
{
    FILE	*fp;
	fp = fopen(filename,"w");

    ::fprintf(fp,"a1 %.6f\n", a1);
    ::fprintf(fp,"a2 %.6f\n", a2);
    ::fprintf(fp,"a3 %.6f\n", a3);
    ::fprintf(fp,"a4 %.6f\n", a4);
    ::fprintf(fp,"a5 %.6f\n", a5);
    ::fprintf(fp,"a6 %.6f\n", a6);
    ::fprintf(fp,"a7 %.6f\n", a7);
    ::fprintf(fp,"a8 %.6f\n", a8);

	fclose(fp);

	return true;
}

/**
 * Serialized storage of a CRPC to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CAffine::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("affinefilename", this->getFileName()->GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("hasValues", this->gethasValues());

    token = pStructuredFileSection->addToken();
	token->setValue("bIsCurrentSensorModel", this->bIsCurrentSensorModel);

    token = pStructuredFileSection->addToken();
    token->setValue("a1", this->a1);

    token = pStructuredFileSection->addToken();
    token->setValue("a2", this->a2);

    token = pStructuredFileSection->addToken();
    token->setValue("a3", this->a3);

    token = pStructuredFileSection->addToken();
    token->setValue("a4", this->a4);
 
    token = pStructuredFileSection->addToken();
    token->setValue("a5", this->a5);

    token = pStructuredFileSection->addToken();
    token->setValue("a6", this->a6);

    token = pStructuredFileSection->addToken();
    token->setValue("a7", this->a7);

    token = pStructuredFileSection->addToken();
    token->setValue("a8", this->a8);
    
	CCharString valueStr("");
	CCharString elementStr("");

	token = pStructuredFileSection->addToken();
	for (int i=0; i< this->nActualParameter[addPar]; i++)
	{
		for (int k=0; k< this->nActualParameter[addPar]; k++)
		{
			double element = this->covariance.element(i,k);
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}
	}

	token->setValue("Affinecovariance", valueStr.GetChar());	

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->refSystem.serializeStore(child);
}

/**
 * Restores a CAffine from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CAffine::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("affinefilename"))
            this->setFileName( token->getValue());
		else if(key.CompareNoCase("hasValues"))
            this->hasValues = token->getBoolValue();
		else if(key.CompareNoCase("bIsCurrentSensorModel"))
			this->bIsCurrentSensorModel = token->getBoolValue();
		else if(key.CompareNoCase("a1"))
            this->a1 = token->getDoubleValue();
		else if(key.CompareNoCase("a2"))
            this->a2 = token->getDoubleValue();
		else if(key.CompareNoCase("a3"))
            this->a3 = token->getDoubleValue();
		else if(key.CompareNoCase("a4"))
            this->a4 = token->getDoubleValue();
        else if(key.CompareNoCase("a5"))
            this->a5 = token->getDoubleValue();     
		else if(key.CompareNoCase("a6"))
            this->a6 = token->getDoubleValue();
		else if(key.CompareNoCase("a7"))
            this->a7 = token->getDoubleValue();
		else if(key.CompareNoCase("a8"))
            this->a8 = token->getDoubleValue();
		else if (key.CompareNoCase("Affinecovariance"))
		{
			double* tmpArray = new double [this->nActualParameter[addPar]*this->nActualParameter[addPar]];
			token->getDoubleValues(tmpArray,this->nActualParameter[addPar]*this->nActualParameter[addPar]);
			
			for (int i=0; i< this->nActualParameter[addPar]; i++)
			{
				for (int k=0; k< this->nActualParameter[addPar]; k++)
				{
					this->covariance.element(i,k) = tmpArray[i*this->nActualParameter[addPar]+k];
				}	 
			}

			if (tmpArray) delete [] tmpArray;
		}
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refSystem.serializeRestore(child);
        }
    }

}

/**
 * Gets ClassName
 * returns a CCharString
 */
string CAffine::getClassName() const
{
    return string( "CAffine");
}

bool CAffine::isa(string& className) const
{
    if (className == "CAffine")
        return true;

    return false;
}

int CAffine::accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
						CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
						CRobustWeightFunction *robust, bool omitMarked)
{
	int nObs = 2;
    CAdjustMatrix AiPar(2, this->getNumberOfStationPars());
	CAdjustMatrix AiPoint(2, this->nParsPerPoint);

	CAdjustMatrix Fi(2, 1);
	CAdjustMatrix Bi(2, 2);

	double X = approx[0] - reduction.x;
	double Y = approx[1] - reduction.y;
	double Z = approx[2] - reduction.z;

    double p_F1_A1 = X;
    double p_F1_A2 = Y;
    double p_F1_A3 = Z;
    double p_F1_A4 = 1;

    double p_F1_A5 = 0;
    double p_F1_A6 = 0;
    double p_F1_A7 = 0;
    double p_F1_A8 = 0;

    double p_F1_X = this->a1;
    double p_F1_Y = this->a2;
    double p_F1_Z = this->a3;

    double p_F2_A1 = 0;
    double p_F2_A2 = 0;
    double p_F2_A3 = 0;
    double p_F2_A4 = 0;

    double p_F2_A5 = X;
    double p_F2_A6 = Y;
    double p_F2_A7 = Z;
    double p_F2_A8 = 1;

    double p_F2_X = this->a5;
    double p_F2_Y = this->a6;
    double p_F2_Z = this->a7;

    AiPar(0, 0) = p_F1_A1;
    AiPar(0, 1) = p_F1_A2;
    AiPar(0, 2) = p_F1_A3;
    AiPar(0, 3) = p_F1_A4;
    AiPar(0, 4) = p_F1_A5;
    AiPar(0, 5) = p_F1_A6;
    AiPar(0, 6) = p_F1_A7;
    AiPar(0, 7) = p_F1_A8;
   
    AiPar(1, 0) = p_F2_A1;
    AiPar(1, 1) = p_F2_A2;
    AiPar(1, 2) = p_F2_A3;
    AiPar(1, 3) = p_F2_A4;
    AiPar(1, 4) = p_F2_A5;
    AiPar(1, 5) = p_F2_A6;
    AiPar(1, 6) = p_F2_A7;
    AiPar(1, 7) = p_F2_A8;

	AiPoint(0, 0) = p_F1_X;
    AiPoint(0, 1) = p_F1_Y;
    AiPoint(0, 2) = p_F1_Z;

	AiPoint(1, 0) = p_F2_X;
    AiPoint(1, 1) = p_F2_Y;
    AiPoint(1, 2) = p_F2_Z;

    double x = obs[0];
    double y = obs[1];

    double f00 = (this->a1 * X + this->a2 * Y + this->a3 * Z + this->a4) - x;
    double f10 = (this->a5 * X + this->a6 * Y + this->a7 * Z + this->a8) - y;

    Fi(0, 0) = f00;
    Fi(1, 0) = f10;

	CAdjustMatrix PBB(2,2);
	PBB(0,0) = obs.getCovariance()->element(0,0);
	PBB(0,1) = obs.getCovariance()->element(0,1);
	PBB(1,1) = obs.getCovariance()->element(1,1);
	PBB(1,0) = PBB(0,1);
	PBB.invert();

	discrepancy[0] = f00;
	discrepancy[1] = f10;
	
	normalisedDiscrepancy[0] = fabs(discrepancy[0]) / obs.getSigma(0);
	normalisedDiscrepancy[1] = fabs(discrepancy[1]) / obs.getSigma(0);

	if (robust || omitMarked)
	{
		double w0 , w1;
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 0, omitMarked, w0, PBB)) --nObs; 
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 1, omitMarked, w1, PBB)) --nObs; 
	}

    CAdjustMatrix ATP = AiPar;
	ATP.transpose();
	ATP = ATP * PBB;

	CAdjustMatrix ATPA = ATP * AiPar;
	ATPA.addToMatrix(this->parameterIndex[addPar], this->parameterIndex[addPar], N);
	ATPA = ATP * AiPoint;
	ATPA.addToMatrix(this->parameterIndex[addPar], approx.id, N);
	ATPA.transpose();
	ATPA.addToMatrix(approx.id, this->parameterIndex[addPar], N);
	ATPA = ATP * Fi;
	ATPA.addToMatrix(this->parameterIndex[addPar], 0, t);

	ATP = AiPoint;
	ATP.transpose();
	ATP = ATP * PBB;
	ATPA = ATP * AiPoint;
	ATPA.addToMatrix(approx.id, approx.id, N);
	ATPA = ATP * Fi;
	ATPA.addToMatrix(approx.id, 0, t);

	return nObs;
};


bool CAffine::AiBiFi(Matrix &Fi, Matrix &Bi, 
					 Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
					 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					 Matrix &AiPar,        Matrix &AiPoint,
					 const Matrix &obs, const C3DPoint &approx)
{
	AiErp.ReSize(0, 0);
    AiRot.ReSize(0, 0);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);

    AiPar.ReSize(2, this->getNumberOfStationPars());
	AiPoint.ReSize(2, this->nParsPerPoint);

	Fi.ReSize(2, 1);
    Bi.ReSize(2, 2);

	double X = approx.x - reduction.x;
	double Y = approx.y - reduction.y;
	double Z = approx.z - reduction.z;

    double p_F1_A1 = X;
    double p_F1_A2 = Y;
    double p_F1_A3 = Z;
    double p_F1_A4 = 1;

    double p_F1_A5 = 0;
    double p_F1_A6 = 0;
    double p_F1_A7 = 0;
    double p_F1_A8 = 0;

    double p_F1_X = this->a1;
    double p_F1_Y = this->a2;
    double p_F1_Z = this->a3;

    double p_F2_A1 = 0;
    double p_F2_A2 = 0;
    double p_F2_A3 = 0;
    double p_F2_A4 = 0;

    double p_F2_A5 = X;
    double p_F2_A6 = Y;
    double p_F2_A7 = Z;
    double p_F2_A8 = 1;

    double p_F2_X = this->a5;
    double p_F2_Y = this->a6;
    double p_F2_Z = this->a7;

    AiPar.element(0, 0) = p_F1_A1;
    AiPar.element(0, 1) = p_F1_A2;
    AiPar.element(0, 2) = p_F1_A3;
    AiPar.element(0, 3) = p_F1_A4;
    AiPar.element(0, 4) = p_F1_A5;
    AiPar.element(0, 5) = p_F1_A6;
    AiPar.element(0, 6) = p_F1_A7;
    AiPar.element(0, 7) = p_F1_A8;
   
    AiPar.element(1, 0) = p_F2_A1;
    AiPar.element(1, 1) = p_F2_A2;
    AiPar.element(1, 2) = p_F2_A3;
    AiPar.element(1, 3) = p_F2_A4;
    AiPar.element(1, 4) = p_F2_A5;
    AiPar.element(1, 5) = p_F2_A6;
    AiPar.element(1, 6) = p_F2_A7;
    AiPar.element(1, 7) = p_F2_A8;

	AiPoint.element(0, 0) = p_F1_X;
    AiPoint.element(0, 1) = p_F1_Y;
    AiPoint.element(0, 2) = p_F1_Z;

	AiPoint.element(1, 0) = p_F2_X;
    AiPoint.element(1, 1) = p_F2_Y;
    AiPoint.element(1, 2) = p_F2_Z;

    double x = obs.element(0,0);
    double y = obs.element(0,1);

    double f00 = (this->a1 * X + this->a2 * Y + this->a3 * Z + this->a4) - x;
    double f10 = (this->a5 * X + this->a6 * Y + this->a7 * Z + this->a8) - y;

    Fi.element(0, 0) = f00;
    Fi.element(1, 0) = f10;

    Bi.element(0, 0) = -1;
    Bi.element(0, 1) = 0;
    Bi.element(1, 0) = 0;
    Bi.element(1, 1) = -1;

    return true;
}

bool CAffine::Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca,    
				 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
				 Matrix &AiPar,        Matrix &AiPoint, 
				 const Matrix &obs, const C3DPoint &approx) const
{
	AiErp.ReSize(0, 0);
    AiRot.ReSize(0, 0);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);

    AiPar.ReSize(2, this->getNumberOfStationPars());
	AiPoint.ReSize(2, this->nParsPerPoint);

	double X = approx.x - reduction.x;
	double Y = approx.y - reduction.y;
	double Z = approx.z - reduction.z;

    double p_F1_A1 = X;
    double p_F1_A2 = Y;
    double p_F1_A3 = Z;
    double p_F1_A4 = 1;

    double p_F1_A5 = 0;
    double p_F1_A6 = 0;
    double p_F1_A7 = 0;
    double p_F1_A8 = 0;

    double p_F1_X = this->a1;
    double p_F1_Y = this->a2;
    double p_F1_Z = this->a3;

    double p_F2_A1 = 0;
    double p_F2_A2 = 0;
    double p_F2_A3 = 0;
    double p_F2_A4 = 0;

    double p_F2_A5 = X;
    double p_F2_A6 = Y;
    double p_F2_A7 = Z;
    double p_F2_A8 = 1;

    double p_F2_X = this->a5;
    double p_F2_Y = this->a6;
    double p_F2_Z = this->a7;

    AiPar.element(0, 0) = p_F1_A1;
    AiPar.element(0, 1) = p_F1_A2;
    AiPar.element(0, 2) = p_F1_A3;
    AiPar.element(0, 3) = p_F1_A4;
    AiPar.element(0, 4) = p_F1_A5;
    AiPar.element(0, 5) = p_F1_A6;
    AiPar.element(0, 6) = p_F1_A7;
    AiPar.element(0, 7) = p_F1_A8;
   
    AiPar.element(1, 0) = p_F2_A1;
    AiPar.element(1, 1) = p_F2_A2;
    AiPar.element(1, 2) = p_F2_A3;
    AiPar.element(1, 3) = p_F2_A4;
    AiPar.element(1, 4) = p_F2_A5;
    AiPar.element(1, 5) = p_F2_A6;
    AiPar.element(1, 6) = p_F2_A7;
    AiPar.element(1, 7) = p_F2_A8;

	AiPoint.element(0, 0) = p_F1_X;
    AiPoint.element(0, 1) = p_F1_Y;
    AiPoint.element(0, 2) = p_F1_Z;

	AiPoint.element(1, 0) = p_F2_X;
    AiPoint.element(1, 1) = p_F2_Y;
    AiPoint.element(1, 2) = p_F2_Z;

    return true;
}

bool CAffine::AiPars(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
					 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const
{
 	AiErp.ReSize(0, 0);
    AiRot.ReSize(0, 0);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);

    AiPar.ReSize(2, this->getNumberOfStationPars());

	double X = approx.x - reduction.x;
	double Y = approx.y - reduction.y;
	double Z = approx.z - reduction.z;

    double p_F1_A1 = X;
    double p_F1_A2 = Y;
    double p_F1_A3 = Z;
    double p_F1_A4 = 1;

    double p_F1_A5 = 0;
    double p_F1_A6 = 0;
    double p_F1_A7 = 0;
    double p_F1_A8 = 0;

    double p_F2_A1 = 0;
    double p_F2_A2 = 0;
    double p_F2_A3 = 0;
    double p_F2_A4 = 0;

    double p_F2_A5 = X;
    double p_F2_A6 = Y;
    double p_F2_A7 = Z;
    double p_F2_A8 = 1;


    AiPar.element(0, 0) = p_F1_A1;
    AiPar.element(0, 1) = p_F1_A2;
    AiPar.element(0, 2) = p_F1_A3;
    AiPar.element(0, 3) = p_F1_A4;
    AiPar.element(0, 4) = p_F1_A5;
    AiPar.element(0, 5) = p_F1_A6;
    AiPar.element(0, 6) = p_F1_A7;
    AiPar.element(0, 7) = p_F1_A8;
   
    AiPar.element(1, 0) = p_F2_A1;
    AiPar.element(1, 1) = p_F2_A2;
    AiPar.element(1, 2) = p_F2_A3;
    AiPar.element(1, 3) = p_F2_A4;
    AiPar.element(1, 4) = p_F2_A5;
    AiPar.element(1, 5) = p_F2_A6;
    AiPar.element(1, 6) = p_F2_A7;
    AiPar.element(1, 7) = p_F2_A8;

    return true;
}


bool CAffine::AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const
{
    // ob will be a 2 x 1
    // and contains an xy image pair
    Ai.ReSize(2, this->nParsPerPoint);

    double p_F1_X = this->a1;
    double p_F1_Y = this->a2;
    double p_F1_Z = this->a3;

    double p_F2_X = this->a5;
    double p_F2_Y = this->a6;
    double p_F2_Z = this->a7;

    Ai.element(0, 0) = p_F1_X;
    Ai.element(0, 1) = p_F1_Y;
    Ai.element(0, 2) = p_F1_Z;
   
    Ai.element(1, 0) = p_F2_X;
    Ai.element(1, 1) = p_F2_Y;
    Ai.element(1, 2) = p_F2_Z;

    return true;
}


bool CAffine::Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const
{
    Fi.ReSize(2, 1);

    double x = obs(1, 1);
    double y = obs(1, 2);

	double X = approx.x - reduction.x;
	double Y = approx.y - reduction.y;
	double Z = approx.z - reduction.z;

    double f00 = (this->a1 * X + this->a2 * Y + this->a3 * Z + this->a4) - x;
    double f10 = (this->a5 * X + this->a6 * Y + this->a7 * Z + this->a8) - y;

    Fi.element(0, 0) = f00;
    Fi.element(1, 0) = f10;

    return true;
}

bool CAffine::Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const
{
    // This is basically a Gauss-markov problem,
    // so the B matrix is just -Identity since x and y can
    // be solved for explicitly in the model
    Bi.ReSize(2, 2);

    Bi.element(0, 0) = -1;
    Bi.element(0, 1) = 0;
    Bi.element(1, 0) = 0;
    Bi.element(1, 1) = -1;

    return true;
}

void CAffine::getName(CCharString& name) const
{
    name = "Affine Bundle";
}

void CAffine::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
    StationNames.RemoveAll();
    PointNames.RemoveAll();

	CCharString* str;

    str=StationNames.Add();
    *str = "A1";

    str=StationNames.Add();
    *str = "A2";

    str=StationNames.Add();
    *str = "A3";

    str=StationNames.Add();
    *str = "A4";

    str=StationNames.Add();
    *str = "A5";

    str=StationNames.Add();
    *str = "A6";

    str=StationNames.Add();
    *str = "A7";

    str=StationNames.Add();
    *str = "A8";
    
	str=PointNames.Add();
    *str = "X";

    str=PointNames.Add();
    *str = "Y";

	str=PointNames.Add();
    *str = "Z";
}

void CAffine::addDirectObservations(Matrix &N, Matrix &t)
{
};

bool CAffine::resectParameters(const CXYPointArray &obsArray,CXYZPointArray &controlArray)
{
	Matrix Coeff(4, 4);     Coeff = 0.0;
    Matrix RightSideX(4, 1); RightSideX = 0.0;
    Matrix RightSideY(4, 1); RightSideY = 0.0;

    int nObs = 0;
    bool solved = false;

	for (int i = 0; i < obsArray.GetSize(); ++i)
	{
		CObsPoint *obs = obsArray.GetAt(i);
		CLabel label(obs->getLabel());
		CObsPoint *conp = controlArray.getPoint(label.GetChar());
		if (conp)
		{
			double X  = (*conp)[0] - reduction.x;
            double Y  = (*conp)[1] - reduction.y;
            double Z  = (*conp)[2] - reduction.z;
			double XY = X * Y;
			double XZ = X * Z;
			double YZ = Y * Z;

            Coeff.element(0, 0) += X * X;  Coeff.element(0, 1) += XY;    Coeff.element(0, 2) += XZ;    Coeff.element(0, 3) += X;
            Coeff.element(1, 0) += XY;     Coeff.element(1, 1) += Y * Y; Coeff.element(1, 2) += YZ;    Coeff.element(1, 3) += Y;
            Coeff.element(2, 0) += XZ;     Coeff.element(2, 1) += YZ;    Coeff.element(2, 2) += Z * Z; Coeff.element(2, 3) += Z;
            Coeff.element(3, 0) += X;      Coeff.element(3, 1) += Y;     Coeff.element(3, 2) += Z;     Coeff.element(3, 3) += 1;
                               
			RightSideX.element(0, 0) += (*obs)[0] * X;
			RightSideX.element(1, 0) += (*obs)[0] * Y;
			RightSideX.element(2, 0) += (*obs)[0] * Z;
			RightSideX.element(3, 0) += (*obs)[0];
			RightSideY.element(0, 0) += (*obs)[1] * X;
			RightSideY.element(1, 0) += (*obs)[1] * Y;
			RightSideY.element(2, 0) += (*obs)[1] * Z;
			RightSideY.element(3, 0) += (*obs)[1];

			++nObs;
		}
	};

	Matrix SolX, SolY;
	Try
	{
		Coeff = Coeff.i();  
		SolX = Coeff * RightSideX;    
		SolY = Coeff * RightSideY;
        solved = true;
	}
	CatchAll
	{
	}
   
    if (solved)
    {
		this->a1 = SolX.element(0,0);
		this->a2 = SolX.element(1,0);
		this->a3 = SolX.element(2,0);
		this->a4 = SolX.element(3,0);
		this->a5 = SolY.element(0,0);
		this->a6 = SolY.element(1,0);
		this->a7 = SolY.element(2,0);
		this->a8 = SolY.element(3,0);
    }

    return true;
};

bool CAffine::setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const
{
	Matrix lBi;
	Matrix obsMat(1,2);
	obsMat.element(0,0) = obs.getX();
	obsMat.element(0,1) = obs.getY();
	this->Bi(lBi, obsMat, obj);
	Matrix BtQllBinv = lBi.t() * lBi * 0.000001;
	BtQllBinv = BtQllBinv.i();
	int iter = 0;
	Matrix Qxx;
	double rms = FLT_MAX;

    do
	{
		Matrix lAi, lFi;
		this->AiPoint(lAi, obsMat, obj);
		lAi = lAi.SubMatrix(1,2,3,3);
		this->Fi(lFi, obsMat, obj);
		Matrix Npt = lAi.t() * BtQllBinv;
		Matrix N = Npt * lAi;
		Matrix t = Npt * lFi;
		Qxx = N.i();
		Matrix delta = Qxx * t;
		obj.setZ(obj.getZ() - delta.element(0,0));
		rms = lFi.element(0,0) * lFi.element(0,0) + lFi.element(1,0) *  lFi.element(1,0);
		++iter;
	} while ((iter < 20) && (rms > 1e-20));

	return true;
};
	 
bool CAffine::setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const
{
	CXYPoint obs2D(obs.getX(), obs.getY(), obs.getLabel());
	return setHeigthAtXY(obj, obs2D);
};

void CAffine::applyReduction()
{

	this->a4 = this->a4 - this->a1 * reduction.x - this->a2 * reduction.y - this->a3 * reduction.z;
	this->a8 = this->a8 - this->a5 * reduction.x - this->a6 * reduction.y - this->a7 * reduction.z;

	CSensorModel::applyReduction();
};

void CAffine::setReduction(const C3DPoint &red)
{
	applyReduction();
	CSensorModel::setReduction(red);
	this->a4 = this->a4 + this->a1 * reduction.x + this->a2 * reduction.y + this->a3 * reduction.z;
	this->a8 = this->a8 + this->a5 * reduction.x + this->a6 * reduction.y + this->a7 * reduction.z;
};

void CAffine::copy(const CAffine &affine)
{
	initFromModel(affine);
   
	this->a1 = affine.a1;
	this->a2 = affine.a2;
	this->a3 = affine.a3;
	this->a4 = affine.a4;
	this->a5 = affine.a5;
	this->a6 = affine.a6;
	this->a7 = affine.a7;
	this->a8 = affine.a8;

   this->informListeners_elementsChanged();
};


void CAffine::crop(int offsetX, int offsetY, int newWidth, int newHeight)
{
	this->a4 = this->a4 - (double)offsetX;
	this->a8 = this->a8 - (double)offsetY;
	this->setFileName("CropAffine");
};
