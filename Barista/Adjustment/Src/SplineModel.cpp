#include "SplineModel.h"
#include "newmat.h"
#include "AdjustMatrix.h"
#include <strstream>


/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

CSplineModel::CSplineModel(void) : CSensorModel(false, "Spline", 0, 0, 0, 0, 0, 0, 0, 0, 6), 
									offsetSpline(3),scale(1.0),useDirectObservationsForSpline(false)

{
	this->offsetSpline.setParent(this);
	this->spline.setParent(this);
	this->i_scale = 1.0 / this->scale;
}

CSplineModel::CSplineModel(int nShiftParameters, int nRotationParameters,int nScaleParameters,int nPointObservations) : 
	CSensorModel(false, "Spline", nShiftParameters, nRotationParameters, nScaleParameters, 0, 0, 0, 0, 0, nPointObservations), offsetSpline(3),
	scale(1.0),useDirectObservationsForSpline(false)
{
	this->offsetSpline.setParent(this);
	this->spline.setParent(this);
	this->i_scale = 1.0 / this->scale;
}


CSplineModel::CSplineModel(const CSplineModel& model) : CSensorModel(model), 
	spline(model.spline), offsetSpline(model.offsetSpline),scale(model.scale),
	useDirectObservationsForSpline(model.useDirectObservationsForSpline),
	directObservations(model.directObservations)
{
	this->offsetSpline.setParent(this);
	this->spline.setParent(this);
	this->i_scale = 1.0 / this->scale;


}

CSplineModel &CSplineModel::operator = (const CSplineModel & splineModel)
{
	CSensorModel::operator=(splineModel);
	this->offsetSpline = splineModel.offsetSpline;
	this->spline = splineModel.spline;
	this->scale = splineModel.scale;
	this->i_scale = 1.0 / this->scale;
	this->useDirectObservationsForSpline = splineModel.useDirectObservationsForSpline;
	this->directObservations = splineModel.directObservations;
	return *this;
}


CSplineModel::~CSplineModel(void)
{
}


void CSplineModel::goingToBeDeleted()
{
	this->informListeners_elementDeleted();
}

void CSplineModel::initSpline(int splineDegree,const  CFileName& name, doubles& knots)
{
	this->setFileName(name.GetChar());
	this->spline.init(splineDegree,name,knots);
	this->computeParameterCount();
}

void CSplineModel::computeParameterCount()
{
	this->nPotentialParameter[addPar] = (this->spline.getDegree()+1) * this->spline.getNumberOfSegments()*3;
	this->nPotentialParameter[constraints] = (spline.getNumberOfSegments()-1)*this->spline.getDegree()*3;
	this->nActualParameter[addPar] = this->nPotentialParameter[addPar];
	this->nActualParameter[constraints] = this->nPotentialParameter[constraints];
	this->calculateNumberOfParameter();	
}

void CSplineModel::transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const
{
	
}

void CSplineModel::crop(int offsetX, int offsetY, int newWidth, int newHeight)
{
};

void CSplineModel::transformObj2Obs(C3DPoint &obs, const double time) const
{

	
}



void CSplineModel::serializeStore(CStructuredFileSection* pStructuredFileSection)
{

}

void CSplineModel::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{

}

bool CSplineModel::isa(string& className) const
{
	if (className == "CSplineModel")
		return true;
	return false;
}

string CSplineModel::getClassName() const
{
	return string("CSplineModel");
}


void CSplineModel::transformObj2Obs(C2DPoint  &obs, const C3DPoint &obj)const
{

}

bool CSplineModel::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, double Height) const
{
	return true;
}

bool CSplineModel::transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const
{
	return true;
}


bool CSplineModel::transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const
{
	return true;
};

bool CSplineModel::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const
{
	return true;
};

bool CSplineModel::transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const
{
	return true;
};

void CSplineModel::setAdjustableValues(const Matrix &parms)
{

}

void CSplineModel::setAll(const Matrix &parms)
{

}

bool CSplineModel::read(const char *filename)
{
	return true;
}

bool CSplineModel::writeTextFile(const char* filename) const
{
	return true;
}

	
bool CSplineModel::setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const
{
	return true;
}

bool CSplineModel::setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const
{
	return true;
}


bool CSplineModel::getAllParameters(Matrix &parms) const
{
	int addParIndex=0;
	addParIndex += this->nActualParameter[shiftPar];
	addParIndex += this->nActualParameter[rotPar];
	return this->spline.getAdjustableParameters(parms,addParIndex);
}

bool CSplineModel::getAdjustableParameters(Matrix &parms) const
{
	
	int addParIndex = 0;
	addParIndex += this->nActualParameter[shiftPar];
	addParIndex += this->nActualParameter[rotPar];
	return this->spline.getAdjustableParameters(parms,addParIndex);
}

bool CSplineModel::insertParametersForAdjustment(Matrix &parms)
{
	int addIndex = this->parameterIndex[addPar];;
	this->spline.insertParametersForAdjustment(parms,addIndex);

	return true;
}

bool CSplineModel::setParametersFromAdjustment(const Matrix &parms)
{
	int addIndex = this->parameterIndex[addPar];
	this->spline.setParametersFromAdjustment(parms,addIndex);

	return true;
}



Matrix CSplineModel::getConstants() const
{
	Matrix parms( 0, 0);
    return parms;
}

int CSplineModel::accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
							 CObsPoint &discrepancy,  CObsPoint &normalisedDiscrepancy, 
							 CRobustWeightFunction *robust, bool omitMarked)
{
	CXYZOrbitPoint* test = (CXYZOrbitPoint*)&(obs);
	int rowSize;		
	if (test->getHasVelocity())
		rowSize = 6;	// both path and velocity of observation
	else
		rowSize = 3;	// just the observation

	int nObs = rowSize;

	int colSize = (this->spline.getDegree()+1);	// number of polynom parameter
	int segIndex = 0;	// only the spline knows the segment of the point obs


	
	// compute the P = (BT*Q*B)-1 for path and velocity
	// B is -I for the spline, but orbit path or orbit attitudes might add a real B matrix
	Matrix* Qobs = obs.getCovariance();
	CAdjustMatrix P(rowSize,rowSize);
	this->computePMatrix(*Qobs,P);

	// get the partials and the segment index of the point
	CAdjustMatrix AiPar(rowSize/3,colSize);
	if (!this->spline.Ai(AiPar,obs.dot,segIndex))
	{
		nObs = 0;
		return nObs;
	}


	this->Fi(discrepancy,obs);

	CAdjustMatrix dl(rowSize,1);
	for (int i=0; i < rowSize; i++)
	{
		// compute normalized residuals
		normalisedDiscrepancy[i] = fabs(discrepancy[i]) / sqrt(obs.getCovariance()->element(i,i));	
		// fill dl
		dl(i,0) = discrepancy[i];
	}

	if (robust || omitMarked)
	{
		double w;
		for (int i=0; i < rowSize; i++)
		{
			if (!checkRobust(obs, normalisedDiscrepancy, robust, i, omitMarked, w, P)) --nObs; 
		}
	}

	// create Ax, Ay, Az
	CAdjustMatrix Ax (rowSize,colSize,0.0);
	CAdjustMatrix Ay (rowSize,colSize,0.0);
	CAdjustMatrix Az (rowSize,colSize,0.0);

	for (int i=0; i < colSize; i++)
	{
		Ax(0,i) = AiPar(0,i);
		Ay(1,i) = AiPar(0,i);
		Az(2,i) = AiPar(0,i);
		if (rowSize == 6)
		{
			Ax(3,i) = AiPar(1,i);
			Ay(4,i) = AiPar(1,i);
			Az(5,i) = AiPar(1,i);
		}
	}

	// create AxT*P, AyT*P, AzT*P
	CAdjustMatrix AxT = Ax;
	CAdjustMatrix AyT = Ay;
	CAdjustMatrix AzT = Az;
	AxT.transpose();
	AyT.transpose();
	AzT.transpose();

	CAdjustMatrix AxTP = AxT*P;
	CAdjustMatrix AyTP = AyT*P;
	CAdjustMatrix AzTP = AzT*P;

	// fill N Matrix and t vector
	// x
	int indexNx = this->parameterIndex[addPar] + segIndex*colSize;
	CAdjustMatrix ATPA = AxTP*Ax;
	ATPA.addToMatrix(indexNx, indexNx, N);
	ATPA = AxTP * dl;						// ATPdl
	ATPA.addToMatrix(indexNx, 0, t);

	// y
	int indexNy = indexNx +  colSize* this->spline.getNumberOfSegments();
	ATPA = AyTP*Ay;
	ATPA.addToMatrix(indexNy, indexNy, N);
	ATPA = AyTP * dl;						// ATPdl
	ATPA.addToMatrix(indexNy, 0, t);

	// z
	int indexNz = indexNy + colSize* this->spline.getNumberOfSegments();
	ATPA = AzTP*Az;
	ATPA.addToMatrix(indexNz, indexNz, N);
	ATPA = AzTP * dl;						// ATPdl
	ATPA.addToMatrix(indexNz, 0, t);


	// the mixed part
	ATPA = AxTP * Ay;
	ATPA.addToMatrix(indexNx,indexNy,N,true);

	ATPA = AxTP * Az;
	ATPA.addToMatrix(indexNx,indexNz,N,true);

	ATPA = AyTP * Az;
	ATPA.addToMatrix(indexNy,indexNz,N,true);

	// extend if orbit path or attitude model
	this->extend_N_and_t_Matrix(N,t,AxTP,AyTP,AzTP,P,dl,indexNx,indexNy,indexNz,obs);

	return nObs;
}


void CSplineModel::addConstraint(Matrix* N, Matrix* t, Matrix* parms)
{
	int indexRow = this->parameterIndex[constraints];
	int indexCol = this->parameterIndex[addPar];
	this->spline.addConstraint(N,t,parms,indexRow,indexCol);

}



bool CSplineModel::Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const
{
	int dimension = obs.Ncols()-1;
	double time = obs.element(0,dimension);
	Fi.ReSize(dimension, 1);	

	CObsPoint tmp(3);	// correct the spline offset
	for (int i=0; i < 3; i++)
		tmp[i] = obs.element(0,i) - offsetSpline[i];

	if (dimension == 6)	
	{
		CObsPoint p(6);
		this->spline.evaluate(p,time);	// evaluate the spline at t

		// compute residuals
		Fi(1, 1) = p[0] - tmp[0];
		Fi(2, 1) = p[1] - tmp[1];
		Fi(3, 1) = p[2] - tmp[2];
		Fi(4, 1) = p[3] - obs(1,4);
		Fi(5, 1) = p[4] - obs(1,5);
		Fi(6, 1) = p[5] - obs(1,6);

	}
	else if (dimension == 3)
	{
		C3DPoint p;
		this->spline.evaluateLocation(p,time);	// evaluate the spline at t

		// compute residuals
		Fi(1, 1) = p[0] - tmp[0];
		Fi(2, 1) = p[1] - tmp[1];
		Fi(3, 1) = p[2] - tmp[2];
	}

	return true;
}

bool CSplineModel::Fi(CObsPoint& Fi, const CObsPoint &obs) const
{
	CXYZOrbitPoint* test = (CXYZOrbitPoint*)&(obs);
	int dimension;		
	if (test->getHasVelocity())
		dimension = 6;	// both path and velocity of observation
	else
		dimension = 3;	// just the observation
	
	

	CObsPoint tmp(3);	// correct the spline offset
	for (int i=0; i < 3; i++)
		tmp[i] = obs[i] - offsetSpline[i];

	if (dimension == 6)	
	{
		CObsPoint p(6);
		if (!this->spline.evaluate(p,obs.dot))	// evaluate the spline at t
			return false;
		// compute residuals
		Fi[0] = p[0] - tmp[0];
		Fi[1] = p[1] - tmp[1];
		Fi[2] = p[2] - tmp[2];
		Fi[3] = p[3] - obs[3];
		Fi[4] = p[4] - obs[4];
		Fi[5] = p[5] - obs[5];

	}
	else if (dimension == 3)
	{
		C3DPoint p;
		if (!this->spline.evaluateLocation(p,obs.dot))	// evaluate the spline at t
			return false;
		// compute residuals
		Fi[0] = p[0] - tmp[0];
		Fi[1] = p[1] - tmp[1];
		Fi[2] = p[2] - tmp[2];
	}

	return true;
}



bool CSplineModel::Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					Matrix &AiAdd,        Matrix &AiPoint, 
					const Matrix &obs, const C3DPoint &approx) const
{
	// set all the unused matrices to 0
	AiErp.ReSize(0,0);
	AiRot.ReSize(0, 0);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);
	AiPoint.ReSize(0,0);	
	
	int dimension = obs.Ncols()-1;
	int nCoefficients = this->spline.getDegree()+1;
	int nSegments = this->spline.getNumberOfSegments();

    AiAdd.ReSize(dimension, nCoefficients*nSegments*3);
	AiAdd = 0.0;
    
	CAdjustMatrix Ai(dimension/3,nCoefficients);	// we get the partials from the spline


	double time = obs.element(0,dimension);

	int segIndex = 0;	// only the spline knows the segment of the point obs
	this->spline.Ai(Ai,time,segIndex);		// get the partials and the segment index of the point

	// adjust the unit 
	Ai *= this->i_scale;

	int indexX = segIndex * nCoefficients;
	int indexY = indexX + nCoefficients* nSegments;
	int indexZ = indexY + nCoefficients* nSegments;


	if (dimension == 6)	
	{
		for (int i=0; i < nCoefficients; i++)
		{
			// we have the same partials for x, y and z, just different positions in the matrix
			AiAdd.element(0,indexX + i) = Ai(0,i);
			AiAdd.element(3,indexX + i) = Ai(1,i);
			AiAdd.element(1,indexY + i) = Ai(0,i);
			AiAdd.element(4,indexY + i) = Ai(1,i);
			AiAdd.element(2,indexZ + i) = Ai(0,i);
			AiAdd.element(5,indexZ + i) = Ai(1,i);
		}
	}
	else if (dimension == 3)
	{
		for (int i=0; i < nCoefficients; i++)
		{
			// we have the same partials for x, y and z, just different positions in the matrix
			AiAdd.element(0,indexX + i) = Ai(0,i);
			AiAdd.element(1,indexY + i) = Ai(0,i);
			AiAdd.element(2,indexZ + i) = Ai(0,i);
			}
	}
	
	return true;
}


bool CSplineModel::AiPars (Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const
{
	return true;
}


bool CSplineModel::AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const
{
	return true;
}



bool CSplineModel::AiBiFi(Matrix &Fi, Matrix &Bi, 
						Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiAdd,        Matrix &AiPoint,
						const Matrix &obs, const C3DPoint &approx) 
{
	// set all the unused matrices to 0
	AiErp.ReSize(0,0);
	AiRot.ReSize(0, 0);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);
	AiPoint.ReSize(0,0);

	int dimension = obs.Ncols()-1;
	int nCoefficients = this->spline.getDegree()+1;
	int nSegments = this->spline.getNumberOfSegments();

    AiAdd.ReSize(dimension, nCoefficients*nSegments*3);
	AiAdd = 0.0;

	CAdjustMatrix Ai(dimension/3,nCoefficients);	// we get the partials from the spline
	double time = obs.element(0,dimension);
	int segIndex = 0;	// only the spline knows the segment of the point obs
	
	if (!this->spline.Ai(Ai,time,segIndex))
		return false;		// get the partials and the segment index of the point


	int indexX = segIndex * nCoefficients;
	int indexY = indexX + nCoefficients* nSegments;
	int indexZ = indexY + nCoefficients* nSegments;

	if (dimension == 6)	
	{
		for (int i=0; i < nCoefficients; i++)
		{
			// we have the same partials for x, y and z, just different positions in the matrix
			AiAdd.element(0,indexX + i) = Ai(0,i);
			AiAdd.element(3,indexX + i) = Ai(1,i);
			AiAdd.element(1,indexY + i) = Ai(0,i);
			AiAdd.element(4,indexY + i) = Ai(1,i);
			AiAdd.element(2,indexZ + i) = Ai(0,i);
			AiAdd.element(5,indexZ + i) = Ai(1,i);
		}
	}
	else if (dimension == 3)
	{
		for (int i=0; i < nCoefficients; i++)
		{
			// we have the same partials for x, y and z, just different positions in the matrix
			AiAdd.element(0,indexX + i) = Ai(0,i);
			AiAdd.element(1,indexY + i) = Ai(0,i);
			AiAdd.element(2,indexZ + i) = Ai(0,i);
			}
	}

	this->Bi(Bi,obs,approx);
	this->Fi(Fi,obs,approx);
	this->extend_Ai(AiErp,AiRot,AiSca,AiMountShift,AiMountRot,AiIrp,AiAdd,AiPoint,obs,approx);

	return true;
}


bool CSplineModel::Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const
{
	int dimension = obs.Ncols()-1;

	Bi.ReSize(dimension, dimension);
	Bi = 0;
	for (int i=0; i < dimension; i++)
		Bi.element(i,i) = -1.0;

	this->extend_Bi(Bi,obs,approx);

	return true;
}


void CSplineModel::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
	CCharStringArray obsNames;
	this->getObservationNames(obsNames);
	this->spline.getParameterNames(StationNames,PointNames,this->obsUnitsString(),obsNames);
}



void CSplineModel::getName(CCharString& names) const
{
    names = "Spline Model";
}

void CSplineModel::computePMatrix(const Matrix& Qobs, CAdjustMatrix& P) const
{
	Matrix Ptmp = Qobs.i();
	
	for (int i=0; i < Ptmp.Nrows(); i++)
		for (int k=0; k < Ptmp.Ncols(); k++)
			P(i,k) = Ptmp.element(i,k);


}


CObsPoint CSplineModel::calcRes(const Matrix &Delta, const CObsPoint &obs, const CObsPoint &obj)
{
	int dimension = obs.getDimension();
	CObsPoint residual(dimension);

	if (obs.id >= 0 || obj.id < 0) 
	{
		Matrix AiErp,  AiRot,  AiSca,  AiMtShft,  AiMtRot,  AiIrp,  AiPar,  AiPoint, Fi, Bi, 
	           parErp, parRot, parSca, parMtShft, parMtRot, parIrp, parPar, parPnt;

		Matrix obsMat(1, dimension+1);
		for (int i=0; i < dimension; i++)
			obsMat.element(0,i) = obs[i];

		obsMat.element(0,dimension) = obs.dot;

		
		if (!this->AiBiFi(Fi, Bi, AiErp, AiRot, AiSca, AiMtShft, AiMtRot, AiIrp, AiPar, AiPoint, obsMat, C3DPoint(obj[0],obj[1],obj[2])))
		{
			for (int i=0; i < dimension; i++)
				residual[i] = 0.0;
			return residual;
		}

		if (this->getParameterCount(shiftPar))
		{
			parErp.ReSize(this->getParameterCount(shiftPar), 1);
			for (int j = 0; j < this->getParameterCount(shiftPar); ++j)
			{
				parErp.element(j, 0) =Delta.element(this->parameterIndex[shiftPar] + j, 0);
			};

			Fi = Fi - AiErp * parErp;
		}
	
		if (this->getParameterCount(rotPar))
		{
			parRot.ReSize(this->getParameterCount(rotPar), 1);
			for (int j = 0; j < this->getParameterCount(rotPar); ++j)
			{
				parRot.element(j, 0) = Delta.element(this->parameterIndex[rotPar] + j, 0);
			};
			Fi = Fi - AiRot * parRot;
		}

		if (this->getParameterCount(scalePar))
		{
			parSca.ReSize(this->getParameterCount(scalePar), 1);
			for (int j = 0; j < this->getParameterCount(scalePar); ++j)
			{
				parSca.element(j, 0) = Delta.element(this->parameterIndex[scalePar] + j, 0);
			};
			Fi = Fi - AiSca * parSca;
		}

		if (this->getParameterCount(mountShiftPar))
		{
			parMtShft.ReSize(this->getParameterCount(mountShiftPar), 1);
			for (int j = 0; j < this->getParameterCount(mountShiftPar); ++j)
			{
				parMtShft.element(j, 0) = Delta.element(this->parameterIndex[mountShiftPar] + j, 0);
			};
			Fi = Fi - AiMtShft * parMtShft;
		}
			
		if (this->getParameterCount(mountRotPar))
		{
			parMtRot.ReSize(this->getParameterCount(mountRotPar), 1);
			for (int j = 0; j < this->getParameterCount(mountRotPar); ++j)
			{
				parMtRot.element(j, 0) = Delta.element(this->parameterIndex[mountRotPar] + j, 0);
			};
			Fi = Fi - AiMtRot * parMtRot;
		}

		if (this->getParameterCount(interiorPar))
		{
			parIrp.ReSize(this->getParameterCount(interiorPar), 1);
			for (int j = 0; j < this->getParameterCount(interiorPar); ++j)
			{
				parIrp.element(j, 0) = Delta.element(this->parameterIndex[interiorPar] + j, 0);
			};
			Fi = Fi - AiIrp * parIrp;
		}
					
		if (this->getParameterCount(addPar))
		{
			parPar.ReSize(this->getParameterCount(addPar), 1);
			for (int j = 0; j < this->getParameterCount(addPar); ++j)
			{
				parPar.element(j, 0) = Delta.element(this->parameterIndex[addPar] + j, 0);
			};
			Fi = Fi - AiPar * parPar;
		}

		if (this->getNParsPerPoint())
		{
			parPnt.ReSize(this->getNParsPerPoint(), 1);

			for (int j = 0; j < this->getNParsPerPoint(); ++j)
			{
				parPnt.element(j, 0) = Delta.element(obj.id + j, 0);
			};

			Fi = Fi - AiPoint * parPnt;
		}

		Matrix *Qi =  obs.getCovariance();

    	Matrix res = -*Qi * Bi.t() * (Bi * *Qi * Bi.t()).i() * Fi;

		for (int i=0; i < dimension; i++)
			residual[i] = res.element(i,0);
	}


	
	return residual;
}

void CSplineModel::initDirectObservations()
{
	// create the space for every spline coefficient
	this->directObservations.resize(this->spline.getDegree() + 1);

	// init every coefficient with the right number of segments
	int nSegments = this->spline.getNumberOfSegments();
	int nCoefficients = this->spline.getDegree() + 1;
	
	// by default we set the coefficient to the current spline parameter
	// the sigma depends on the kind of model, so just init with 1.0 as default value
	doubles observationsX(nSegments);
	doubles observationsY(nSegments);
	doubles observationsZ(nSegments);
	doubles sigmas(nSegments);

	for (int coefficient = 0; coefficient < nCoefficients; coefficient++)
	{
		for (int segment=0; segment < nSegments; segment++)
		{
			this->spline.getSplineParameter(segment,coefficient,observationsX[segment],
																observationsY[segment],
																observationsZ[segment]);

			sigmas[segment] = 1.0;
		}

		this->setDirectObservationsForSpline(coefficient,0,observationsX,sigmas);
		this->setDirectObservationsForSpline(coefficient,1,observationsY,sigmas);
		this->setDirectObservationsForSpline(coefficient,2,observationsZ,sigmas);

		// turn the observation all off, the path or attitude model has to decide what to use
		this->setUseDirectObservationForSpline(coefficient,0,false);
		this->setUseDirectObservationForSpline(coefficient,1,false);
		this->setUseDirectObservationForSpline(coefficient,2,false);
	}

	this->setUseDirectObservationsForSpline(true);

}

void CSplineModel::setDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,const doubles& observations,const doubles& sigmas)
{
	if (degree < this->directObservations.size() && degree >=0 && coordinate >=0 && coordinate < 3)
	{
		this->directObservations[degree].useObservation[coordinate] = true;
		this->directObservations[degree].observation[coordinate] = observations;
		this->directObservations[degree].sigma[coordinate] = sigmas;
	}

}

void CSplineModel::setUseDirectObservationForSpline(unsigned int degree,unsigned int coordinate, bool b)
{
	if (degree < this->directObservations.size() && degree >=0 && coordinate >=0 && coordinate < 3)
		this->directObservations[degree].useObservation[coordinate] = b;
}


void CSplineModel::setDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,unsigned int segment,double observation)
{
	if (degree < this->directObservations.size() && 
		degree >=0 && coordinate >=0 && coordinate < 3 &&
		segment < this->directObservations[degree].observation[coordinate].size())
	{
		this->directObservations[degree].observation[coordinate][segment] = observation;
	}
}

void CSplineModel::setSigmaDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,unsigned int segment,double sigma)
{
	if (degree < this->directObservations.size() && 
		degree >=0 && coordinate >=0 && coordinate < 3 &&
		segment < this->directObservations[degree].observation[coordinate].size())
	{
		this->directObservations[degree].sigma[coordinate][segment] = sigma;
	}
}

double CSplineModel::getDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,unsigned int segment) const
{
	if (degree < this->directObservations.size() && 
		degree >=0 && coordinate >=0 && coordinate < 3 &&
		segment < this->directObservations[degree].observation[coordinate].size())
	{
		return this->directObservations[degree].observation[coordinate][segment];
	}
	else
		return 0.0;
}

double CSplineModel::getSigmaDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,unsigned int segment) const
{
	if (degree < this->directObservations.size() && 
		degree >=0 && coordinate >=0 && coordinate < 3 &&
		segment < this->directObservations[degree].sigma[coordinate].size())
	{
		return this->directObservations[degree].sigma[coordinate][segment];
	}
	else
		return 0.0;
}


bool CSplineModel::getUseDirectObservationForSpline(unsigned int degree,unsigned int coordinate) const
{
	if (degree < this->directObservations.size() && degree >=0 && coordinate >=0 && coordinate < 3)
	{
		return this->directObservations[degree].useObservation[coordinate];
	}

	return false;
}

int CSplineModel::getNDirectObservationsForSpline() const
{
	if (this->useDirectObservationsForSpline)
	{
		int n = 0;
		for (unsigned int i=0; i < this->directObservations.size(); i++)
		{
			const CDirectSplineObservation& o = this->directObservations[i];
			n += o.useObservation[0] ? 1 : 0;
			n += o.useObservation[1] ? 1 : 0;
			n += o.useObservation[2] ? 1 : 0;
		}
		
		return n;
	}
	else
		return 0;
}





void CSplineModel::addDirectObservations(Matrix &N, Matrix &t)
{
	if (this->useDirectObservationsForSpline)
	{
		unsigned int degree = this->spline.getDegree();
		int size = degree + 1; // number of polynom parameter
		int nSegments = this->spline.getNumberOfSegments();

		for (unsigned int parameter = 0; parameter < this->directObservations.size() && degree >= parameter; parameter++)
		{
		// check if one of the observations is to be used
			if (this->directObservations[parameter].useObservation[0] || 
				this->directObservations[parameter].useObservation[1] ||
				this->directObservations[parameter].useObservation[2])
			{
				for (int segment=0; segment < nSegments; segment++)
				{
					// compute the first index
					int indexSplineX = this->parameterIndex[addPar] + segment*size + parameter;
					int indexSplineY = indexSplineX + size * nSegments;
					int indexSplineZ = indexSplineY + size * nSegments;
					
					// get the parameter from the spline 
					double X,Y,Z;
					this->spline.getSplineParameter(segment,parameter,X,Y,Z);

					// add obs for X
					if (this->directObservations[parameter].useObservation[0])
					{
						Matrix pX(1,1);
						pX.element(0,0) = 1.0 / (this->directObservations[parameter].sigma[0][segment] * this->directObservations[parameter].sigma[0][segment]);
						Matrix ATPlX = pX * (X - this->directObservations[parameter].observation[0][segment]);
						this->addMatrix(indexSplineX,indexSplineX,N,pX);
						this->addMatrix(indexSplineX,0,t,ATPlX);
					}

					// add obs for Y
					if (this->directObservations[parameter].useObservation[1])
					{
						Matrix pY(1,1);
						pY.element(0,0) = 1.0 / (this->directObservations[parameter].sigma[1][segment] * this->directObservations[parameter].sigma[1][segment]);				
						Matrix ATPlY = pY * (Y - this->directObservations[parameter].observation[1][segment]);
						this->addMatrix(indexSplineY,indexSplineY,N,pY);
						this->addMatrix(indexSplineY,0,t,ATPlY);
					}

					// add obs for Z
					if (this->directObservations[parameter].useObservation[2])
					{

						Matrix  pZ(1,1);
						pZ.element(0,0) = 1.0 / (this->directObservations[parameter].sigma[2][segment] * this->directObservations[parameter].sigma[2][segment]);
						Matrix ATPlZ = pZ * (Z - this->directObservations[parameter].observation[2][segment]);
						this->addMatrix(indexSplineZ,indexSplineZ,N,pZ);
						this->addMatrix(indexSplineZ,0,t,ATPlZ);
					}
				}// for segments
			}  // if has obs
		} // for parameters
	} 
}


double CSplineModel::getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res)
{
	double value = 0.0;

	if (this->useDirectObservationsForSpline)
	{
		unsigned int degree = this->spline.getDegree();
		int size = degree + 1; // number of polynom parameter
		int nSegments = this->spline.getNumberOfSegments();

		for (unsigned int parameter = 0; parameter < this->directObservations.size() && degree >= parameter; parameter++)
		{
		// check if one of the observations is to be used
			if (this->directObservations[parameter].useObservation[0] || 
				this->directObservations[parameter].useObservation[1] ||
				this->directObservations[parameter].useObservation[2])
			{

				// compute first the index deltaPar matrix
				int indexSpline = this->nActualParameter[rotPar] + this->nActualParameter[scalePar] + this->nActualParameter[shiftPar];

				for (int segment=0; segment < nSegments; segment++)
				{
					int indexSplineX = indexSpline + segment*size;
					int indexSplineY = indexSplineX + size * nSegments;
					int indexSplineZ = indexSplineY + size * nSegments;
					
					double X,Y,Z;
					this->spline.getSplineParameter(segment,parameter,X,Y,Z);

					res.ReSize(3,1);
					res =0;


					// compute for X
					if (this->directObservations[parameter].useObservation[0])
					{
						double pX = 1.0 / (this->directObservations[parameter].sigma[0][segment] * this->directObservations[parameter].sigma[0][segment]);
						X += deltaPar.element(indexSplineX,0) - this->directObservations[parameter].observation[0][segment];
						res.element(0,0) = X;
						X *= X * pX;
						value += X;
					}
					
					// compute for Y
					if (this->directObservations[parameter].useObservation[1])
					{
						double pY = 1.0 / (this->directObservations[parameter].sigma[1][segment] * this->directObservations[parameter].sigma[1][segment]);				
						Y += deltaPar.element(indexSplineY,0) - this->directObservations[parameter].observation[1][segment];;
						res.element(1,0) = Y;
						Y *= Y * pY;
						value += Y;
					}

					// compute for Z
					if (this->directObservations[parameter].useObservation[2])
					{						
						double pZ = 1.0 / (this->directObservations[parameter].sigma[2][segment] * this->directObservations[parameter].sigma[2][segment]);
						Z += deltaPar.element(indexSplineZ,0) - this->directObservations[parameter].observation[2][segment];;
						res.element(2,0) = Z;
						Z *= Z * pZ;
						value += Z;
					}
				} // segments
			} // if has obs
		} // for parameters
	}

	return value;
}



void CSplineModel::getSplineParameter(int segmentIndex,int degree, double& parameterX,double& parameterY,double& parameterZ)
{
	this->spline.getSplineParameter(segmentIndex,degree,parameterX,parameterY,parameterZ);
}

double CSplineModel::getSigmaSplineParameter(unsigned int index) const
{
	int offset = this->getParameterCount(shiftPar) + this->getParameterCount(rotPar) + this->getParameterCount(scalePar);

	index += offset;

	if (index < (unsigned int)(this->covariance.Ncols()))
	{
		return sqrt(this->covariance.element(index,index));
	}
	else
		return 0.0;


}

void CSplineModel::setDirectObservationsForSpline(const vector<CDirectSplineObservation>& directObservations)
{
	this->directObservations = directObservations;
	this->informListeners_elementsChanged(this->getClassName().c_str());
}




double CSplineModel::getFirstTime()
{
	return this->spline.getFirstTime();
}

double CSplineModel::getLastTime()
{
	return this->spline.getLastTime();
}






CDirectSplineObservation::CDirectSplineObservation()
{
	for (int i=0; i<3; i++)
		this->useObservation[i] = false;

}

CDirectSplineObservation::CDirectSplineObservation(const CDirectSplineObservation& obs)
{
	for (int i=0; i<3; i++)
	{
		this->useObservation[i] = obs.useObservation[i];
		this->observation[i] = obs.observation[i];
		this->sigma[i] = obs.sigma[i];
	}
}


CDirectSplineObservation &CDirectSplineObservation::operator = (const CDirectSplineObservation & obs)
{
	
	for (int i=0; i<3; i++)
	{
		this->useObservation[i] = obs.useObservation[i];
		this->observation[i] = obs.observation[i];
		this->sigma[i] = obs.sigma[i];
	}
	return *this;
}



CDirectSplineObservation::~CDirectSplineObservation()
{
	
}




void CDirectSplineObservation::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("useObservationX", this->useObservation[0]);
	
	token = pStructuredFileSection->addToken();
	token->setValue("useObservationY", this->useObservation[1]);

	token = pStructuredFileSection->addToken();
	token->setValue("useObservationZ", this->useObservation[2]);

	token = pStructuredFileSection->addToken();
    token->setValue("size of vector", (int)this->observation[0].size());

	CCharString valueStrX = "";
	CCharString valueStrY = "";
	CCharString valueStrZ = "";
	
	CCharString valueStrSigmaX = "";
	CCharString valueStrSigmaY = "";
	CCharString valueStrSigmaZ = "";

	CCharString elementStr;

	for (unsigned int i = 0; i < this->observation[0].size(); i++)
		{
			double element = this->observation[0][i];
			elementStr.Format("%.13e ", element);
			valueStrX += elementStr;
			
			element = this->observation[1][i];
			elementStr.Format("%.13e ", element);
			valueStrY += elementStr;

			element = this->observation[2][i];
			elementStr.Format("%.13e ", element);
			valueStrZ += elementStr;

			element = this->sigma[0][i];
			elementStr.Format("%.13e ", element);
			valueStrSigmaX += elementStr;

			element = this->sigma[1][i];
			elementStr.Format("%.13e ", element);
			valueStrSigmaY += elementStr;

			element = this->sigma[2][i];
			elementStr.Format("%.13e ", element);
			valueStrSigmaZ += elementStr;
		}

	token = pStructuredFileSection->addToken();
	token->setValue("observationX", valueStrX.GetChar());
	token = pStructuredFileSection->addToken();
	token->setValue("observationY", valueStrY.GetChar());
	token = pStructuredFileSection->addToken();
	token->setValue("observationZ", valueStrZ.GetChar());
	token = pStructuredFileSection->addToken();
	token->setValue("sigmaX", valueStrSigmaX.GetChar());
	token = pStructuredFileSection->addToken();
	token->setValue("sigmaY", valueStrSigmaY.GetChar());
	token = pStructuredFileSection->addToken();
	token->setValue("sigmaZ", valueStrSigmaZ.GetChar());

}

void CDirectSplineObservation::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{

	CSerializable::serializeRestore(pStructuredFileSection);

	bool readData = false;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("useObservationX"))
		{
			this->useObservation[0] = token->getBoolValue();
		}
		else if(key.CompareNoCase("useObservationY"))
		{
			this->useObservation[1] = token->getBoolValue();
		}
		else if(key.CompareNoCase("useObservationZ"))
		{
			this->useObservation[2] = token->getBoolValue();
		}
		else if(key.CompareNoCase("size of vector"))
		{
			int size = token->getIntValue();
			this->observation[0].resize(size);
			this->observation[1].resize(size);
			this->observation[2].resize(size);

			this->sigma[0].resize(size);			
			this->sigma[1].resize(size);
			this->sigma[2].resize(size);

			readData = size > 0 ? true : false;
		}
		else if(readData && key.CompareNoCase("observationX"))
		{
			token->getDoublesClass(this->observation[0]);
		}
		else if(readData && key.CompareNoCase("observationY"))
		{
			token->getDoublesClass(this->observation[1]);
		}
		else if(readData && key.CompareNoCase("observationZ"))
		{
			token->getDoublesClass(this->observation[2]);
		}
		else if(readData && key.CompareNoCase("sigmaX"))
		{
			token->getDoublesClass(this->sigma[0]);
		}
		else if(readData && key.CompareNoCase("sigmaY"))
		{
			token->getDoublesClass(this->sigma[1]);
		}
		else if(readData && key.CompareNoCase("sigmaZ"))
		{
			token->getDoublesClass(this->sigma[2]);
		}
	}
}

