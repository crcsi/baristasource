#include "newmat.h"
#include "SensorModel.h"
#include "SplineSegment.h"

#include "3DPoint.h"
#include "AdjustMatrix.h"
#include "newmat.h"

CSplineSegment::CSplineSegment(int degree,double* startTime,double* endTime):
	splineX(degree),splineY(degree),splineZ(degree),
	startTime(startTime), endTime(endTime),timeScale(0.0)
{
	if (startTime != NULL && endTime != NULL)
		this->timeScale = 1.0/(*endTime - *startTime);

	this->splineX.setParent(this);
	this->splineY.setParent(this);
	this->splineZ.setParent(this);
}

CSplineSegment::~CSplineSegment(void)
{
	
}


CSplineSegment &CSplineSegment::operator = (const CSplineSegment & segment)
{
	this->splineX = segment.splineX;
	this->splineY = segment.splineY;
	this->splineZ = segment.splineZ;

	this->timeScale = segment.timeScale;

	return *this;
}



void CSplineSegment::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CToken* token = pStructuredFileSection->addToken();
    token->setValue("timeScale", (double)this->timeScale);

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	child->setName("CPolynomial");
	this->splineX.serializeStore(child);

	child = pStructuredFileSection->addChild();
	child->setName("CPolynomial");
	this->splineY.serializeStore(child);

	child = pStructuredFileSection->addChild();
	child->setName("CPolynomial");
	this->splineZ.serializeStore(child);

}

void CSplineSegment::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("timeScale"))
            this->timeScale = token->getDoubleValue();
    }

	bool hasX = false;
	bool hasY = false;
	bool hasZ = false;

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (!hasX && !hasY && !hasZ && child->getName().CompareNoCase("CPolynomial"))
		{
			this->splineX.serializeRestore(child);
			hasX=true;
		}
		else if (hasX && !hasY && !hasZ && child->getName().CompareNoCase("CPolynomial"))
		{
			this->splineY.serializeRestore(child);
			hasY = true;
		}
		else if (hasX && hasY && !hasZ && child->getName().CompareNoCase("CPolynomial"))
		{
			this->splineZ.serializeRestore(child);
			hasZ = true;
		}
    }

}

void CSplineSegment::applyScaleToOldProject(double scale)
{
	this->splineX *= scale;
	this->splineY *= scale;
	this->splineZ *= scale;
}

bool CSplineSegment::isModified()
{
	return this->bModified;
}

void CSplineSegment::reconnectPointers()
{
	CSerializable::reconnectPointers();
}

void CSplineSegment::resetPointers()
{
	CSerializable::resetPointers();
}


void CSplineSegment::evaluateVelocity(C3DPoint& erg,const double time)
{
	double lokalTime = this->getLocalTime(time);
	int size = this->getDegree() +1;
	double value = 1.0/(*this->endTime - *this->startTime);

	erg.x = erg.y = erg.z = 0.0;
	for (int i = 1; i < size; i++)
	{
		erg.x += this->splineX.coefficient(i)*i*value;
		erg.y += this->splineY.coefficient(i)*i*value;
		erg.z += this->splineZ.coefficient(i)*i*value;
		value*= lokalTime;
	}
}


void CSplineSegment::evaluateLocation(C3DPoint& erg,const double time)
{
	double lokalTime = this->getLocalTime(time);

	erg.x = this->splineX.functionValueAt(lokalTime);
	erg.y = this->splineY.functionValueAt(lokalTime);
	erg.z = this->splineZ.functionValueAt(lokalTime);
}


void CSplineSegment::extrapolateLocation(C3DPoint& erg,const double time)
{
	double lokalTime = this->getLocalTime(time);
	double dt = *this->endTime - *this->startTime;
	int size = this->getDegree()+1;

	



}

void CSplineSegment::extrapolateVelocity(C3DPoint& erg,const double time)
{
	double lokalTime = this->getLocalTime(time);
	double dt = *this->endTime - *this->startTime;
	int size = this->getDegree()+1;

		



}



void CSplineSegment::Ai(CAdjustMatrix& AiPar, const double &t)
{
	
	double lokalTime = this->getLocalTime(t);
	double dt = 1.0 / (*this->endTime - *this->startTime);
	int size = this->getDegree()+1;

	double value = 1;
	AiPar(0,0) = value;

	if (AiPar.getRows() == 2)
	{
		AiPar(1,0) = 0;
		for (int i=1; i < size; i++)
		{
			value*= lokalTime;						// = t, t^2 and t^3
			AiPar(0,i) = value;						// dPath/dCoefficients
			AiPar(1,i) = i*AiPar(0,i-1) * dt;		// dVelocity/dCoefficients
		}
	}
	else if (AiPar.getRows() == 1)
	{
		for (int i=1; i < size; i++)
		{
			value*= lokalTime;						// = t, t^2 and t^3
			AiPar(0,i) = value;						// dPath/dCoefficients
		}
	}
}


void CSplineSegment::updateCoefficients(const Matrix& param, const int startIndexX,const  int startIndexY,const int startIndexZ)
{
	int nCoefficients = (this->splineX.getDegree()+1);

	for (int i= 0; i < nCoefficients; i++)
	{
		this->splineX.setCoefficient(i,param(startIndexX+i+1,1));
		this->splineY.setCoefficient(i,param(startIndexY+i+1,1));
		this->splineZ.setCoefficient(i,param(startIndexZ+i+1,1));
	}
}

void CSplineSegment::getCoefficients(Matrix& param, const int startIndexX,const  int startIndexY,const int startIndexZ)
{
	int nCoefficients = (this->splineX.getDegree()+1);

	for (int i= 0; i < nCoefficients; i++)
	{
		param(startIndexX+i+1,1) = this->splineX.coefficient(i);
		param(startIndexY+i+1,1) = this->splineY.coefficient(i);
		param(startIndexZ+i+1,1) = this->splineZ.coefficient(i);
	}
}

bool CSplineSegment::getCoefficients(int degree,double& parameterX,double& parameterY,double& parameterZ)
{
	if (degree > this->splineX.getDegree())
		return false;

	parameterX = this->splineX.coefficient(degree);
	parameterY = this->splineY.coefficient(degree);
	parameterZ = this->splineZ.coefficient(degree);

	return true;
}