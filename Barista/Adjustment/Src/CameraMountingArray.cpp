#include "CameraMountingArray.h"

CCameraMountingArray::CCameraMountingArray(void)
{
}

CCameraMountingArray::CCameraMountingArray(const CCameraMountingArray& src)
{
}

CCameraMountingArray::~CCameraMountingArray(void)
{
}

void CCameraMountingArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CStructuredFileSection* pNewSection;

	for (int i=0; i< this->GetSize(); i++)
	{
		CCameraMounting* cameraMounting = this->GetAt(i);
		pNewSection = pStructuredFileSection->addChild();
		cameraMounting->serializeStore(pNewSection);
	}
}

void CCameraMountingArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("CCameraMounting"))
		{
			CCameraMounting* cameraMounting = this->Add();
			cameraMounting->serializeRestore(pChild);
		}
	}
}

bool CCameraMountingArray::isModified()
{
	return this->bModified;
}

void CCameraMountingArray::reconnectPointers()
{
	CSerializable::reconnectPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		CCameraMounting* cameraMounting = this->GetAt(i);
		cameraMounting->reconnectPointers();
	}
}

void CCameraMountingArray::resetPointers()
{
	CSerializable::resetPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		CCameraMounting* cameraMounting = this->GetAt(i);
		cameraMounting->resetPointers();
	}
}


CCameraMounting* CCameraMountingArray::Add()
{
	CCameraMounting* cameraMounting = CMUObjectArray<CCameraMounting, CCameraMountingPtrArray>::Add();
	cameraMounting->setParent(this);
	return cameraMounting;
}

CCameraMounting* CCameraMountingArray::Add(const CCameraMounting& cameraMounting)
{
	CCameraMounting* newCameraMounting = CMUObjectArray<CCameraMounting, CCameraMountingPtrArray>::Add(cameraMounting);
	newCameraMounting->setParent(this);
	return newCameraMounting;
}


CCameraMounting* CCameraMountingArray::getCameraMountingByName(CCharString* name) const
{
    CCameraMounting* test = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        test = this->GetAt(i);
		if (name->CompareNoCase(test->getName().GetChar()))
            return test;

        continue;
    }

    return NULL;
}