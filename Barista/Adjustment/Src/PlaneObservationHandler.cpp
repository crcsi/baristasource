/**
 * @class CPlaneObservationHandler
 * CObservationHandler handles observations
 */

#include "PlaneObservationHandler.h"
#include "XYPointArray.h"
#include "3DAdjustingPlane.h"

CPlaneObservationHandler::CPlaneObservationHandler(const CCharString &fileName) : name(fileName)
{
	this->obsPoints = new CObsPointArray(1);
	this->plane = new C3DAdjustingPlane;
	this->currentSensorModel = this->plane;
}

CPlaneObservationHandler::~CPlaneObservationHandler()
{
	if (this->obsPoints) delete this->obsPoints;
	this->obsPoints = 0;

	if (this->plane) delete this->plane;
	this->plane = 0;
	this->currentSensorModel = 0;
}
