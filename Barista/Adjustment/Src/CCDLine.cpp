#include "CCDLine.h"
#include "2DPoint.h"

CCCDLine::CCCDLine(void)
{
	this->type = eCCD;
}

CCCDLine::CCCDLine(const CCCDLine& src):CCamera(src)
{
	this->type = eCCD;
}

CCCDLine::~CCCDLine(void)
{
}

CCCDLine &CCCDLine::operator = (const CCCDLine & c)
{
	bool changeListenerStatus = false;
	if (this->callListener)
	{
		this->callListener = false;
		changeListenerStatus = true;
	}

	CCamera::operator=(c);

	if (changeListenerStatus)
		this->callListener = true;

	if (this->callListener)
		this->informListeners_elementsChanged(this->getClassName().c_str());  

	return *this;
}


bool CCCDLine::transformObs2Obj(C3DPoint& xc,const C3DPoint& obs,const CTrans2D* frameToCamera) const
{
	// tranform from pixel in camera coordinate system unit
	xc.x = obs.x;
	xc.y = 0.0;
	xc.z = 0.0;

	// transition to camera system
	xc -= this->interiorReferencePoint;	

	// apply distortion in camera sytem 
	if (this->distortion)
		this->distortion->applyObs2Obj(xc,obs,this->interiorReferencePoint,this->scale);


	return true;
}

bool CCCDLine::transformObj2Obs(C2DPoint& obs,const C3DPoint& obj,const CTrans2D* cameraToFrame) const
{
	C2DPoint localXC;
	localXC.x = obj.x / obj.z;
	localXC.y = obj.y / obj.z;

	localXC = -this->interiorReferencePoint.z * localXC;

	if (this->distortion)
		this->distortion->applyObj2Obs(localXC,localXC,this->interiorReferencePoint,this->scale);

	// transition to framelet cs
	localXC.x += this->interiorReferencePoint.x;
	localXC.y += this->interiorReferencePoint.y;

	obs.x   = localXC.x;
	obs.y  += localXC.y;
	return true;
}




void CCCDLine::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);
	
	// add first all the common things
	CCamera::serializeStore(pStructuredFileSection);

	// and now the private members


}


void CCCDLine::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	// add first all the common things
	CCamera::serializeRestore(pStructuredFileSection);

	// and now the private members
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

    }


}
