#include <strstream>
#include <float.h>
#include "newmat.h"

#include "BundleManager.h"
#include "CharString.h"
#include "XYPointArrayPtrArray.h"
#include "XYZPointArray.h"
#include "LabelMerger.h"
#include "SensorModel.h"
#include "XYZOrbitPointArray.h"
#include "SplineModel.h"
#include "robustWeightFunction.h"
#include "RotationBase.h"
#include "AdjustNormEqu.h"
#include "SystemUtilities.h"

CBundleManager::CBundleManager(float I_convergenceLimit, float MaxRobustPercentage,
							   float MinRobFac, float havg, const CCharString &protfile) : 
			nStationPars(0), minObs(3), averageHeight(havg), ConvergenceLimit(I_convergenceLimit),nConstraints(0),
			protocol(0), worstResiduals(10), maxRobustPercentage(MaxRobustPercentage), 
			minRobFac(MinRobFac), nGrossErrors(0), NormalPointIsBlockDiagonal(true)
{
	reset();
	if (protfile != "") initProtocol(protfile.GetChar());
}

CBundleManager::~CBundleManager()
{
	closeProtocol();
}
	  
int CBundleManager::getIndexOfObsHandler(CObservationHandler *handler) const
{
	for (int i = 0; i < this->ObservationHandlers.GetSize(); ++i)
	{
		if (handler == this->ObservationHandlers.GetAt(i)) return i;
	}

	return -1;
};

CCharString CBundleManager::rmsString(const char *prompt) const
{
	double digits = this->rms;
	if (digits < FLT_EPSILON) digits = fabs(digits + FLT_EPSILON);
	digits = floor(log(digits));
	if (digits < 0) digits = -digits + 2;
	else digits = 2;

	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss.precision((int) digits);
	oss << prompt << this->rms << ends;
	char *z = oss.str();

	CCharString retVal(z);
	delete [] z;

	return retVal;
};
	  
CCharString CBundleManager::getStationName(int index) const
{
	CCharString retVal("");

	if (index < this->ObservationHandlers.GetSize())
	{
		CObservationHandler* oh = ObservationHandlers.GetAt(index);
		retVal = oh->getFileNameChar();
	}

	return retVal;
};

void CBundleManager::reset()
{
 	this->nStationPars = 0;
	this->nPointPars = 0;
	this->nConstraints = 0;
	this->averageHeight = 0.0f;
	this->ObservationHandlers.RemoveAll();
    this->ObjectPoints.RemoveAll();
	this->nGrossErrors = 0;

	this->reduction.x = this->reduction.y = this->reduction.z = 0.0f;
}

void CBundleManager::initProtocol(const char *filename)
{
	if (this->protocol) delete this->protocol;
	this->protocol = new ofstream(filename);
	if (!protocol->good()) this->closeProtocol();
}

void CBundleManager::closeProtocol()
{
	if (this->protocol) 
	{
		this->protocol->close();
		delete this->protocol;
		this->protocol = 0;	
	}

}

bool CBundleManager::addObservations(CObservationHandler *obs)
{
	CSensorModel *sensorModel = obs->getCurrentSensorModel();
    if ((sensorModel) /*&& (obs->nObs() > 0)*/)
	{
		sensorModel->setReduction(this->reduction);
		this->ObservationHandlers.Add(obs);
		return true;
	}
	return false;
};
	  
void CBundleManager::getCameraIndices(vector<int> &cameraIndices)
{
	cameraIndices.resize(this->ObservationHandlers.GetSize(), -1);
   
	for (int i = 0; i < this->ObservationHandlers.GetSize(); i++)
    {
        CObservationHandler* obs = this->ObservationHandlers.GetAt(i);
		CCamera *cam = obs->getCurrentSensorModel()->getGenericCamera();
		if (cam)
		{
			int index = i;

			for (int j = 0; (j < i) && (index == i); j++)
			{
				CCamera *prevcam = this->ObservationHandlers.GetAt(j)->getCurrentSensorModel()->getGenericCamera();
				if (prevcam == cam) index = j;
			} 
			cameraIndices[i] = index;
		}
    }
};

/**
*
*/
void CBundleManager::bundleInit(bool forwardOnly,bool excludeTie)
{
	worstResiduals.reset();
	this->resetQualityAssessmentData();
	vector<int> cameraIndices;
	this->getCameraIndices(cameraIndices);

    for (int i = 0; i < this->ObservationHandlers.GetSize(); i++)
    {
        CObservationHandler* obs = this->ObservationHandlers.GetAt(i);
		obs->getCurrentSensorModel()->setReduction(this->reduction);
		if (!forwardOnly)
		{
			this->nStationPars += obs->getNstationParameters();
			if ((cameraIndices[i] >= 0) && (cameraIndices[i] != i))
			{
				this->nStationPars -= obs->getCurrentSensorModel()->getParameterCount(CSensorModel::interiorPar);
				this->nStationPars -= obs->getCurrentSensorModel()->getParameterCount(CSensorModel::addPar);
			}
		}
    }

    this->initObservations(excludeTie);
	if (forwardOnly) this->nConstraints = 0;
    this->initParameters(forwardOnly);
	if (protocol)
	{
		*protocol << "Bundle Adjustment\n=================\n\nDate: " << CCharString::getDateString()
			<< "\nTime: " << CCharString::getTimeString() << "\n\nStations (" << this->getNStations() << "): ";

		for (int i = 0; i < this->getNStations(); ++i)
		{
			CObservationHandler* oh = ObservationHandlers.GetAt(i);
			*protocol << "\n" << oh->getFileNameChar();
		}
		*protocol << "\n\nNumber of point observations:         ";
		protocol->width(7);
		*protocol << this->nPointObs 
			      << "\nNumber of direct observations:        ";
		protocol->width(7);
		*protocol << this->nDirectObs
				  << "\nNumber of constraints:                ";
		protocol->width(7);
		*protocol << this->getNConstraints()
			<< "\nNumber of unknown station parameters: ";
		protocol->width(7);
		*protocol << this->nStationPars
			      << "\nNumber of unknown 3D Points:          ";
		protocol->width(7);
		*protocol << this->getNObjectPoints() 
			      << "\nRedundancy:                           ";
		protocol->width(7);
		*protocol << (this->nPointObs + this->nDirectObs + this->getNConstraints() - this->nStationPars - this->getNObjectPoints() * 3) << "\n" << endl;
	}
}

/**
* Builds the Observation Matrix and Covariance Matrix "Q", ObsLabels are
* the String labels of the observations. They coorespond by index to the
* observations.
*/
void CBundleManager::initObservations(bool excludeTie)
{
	maxResX.resize(this->getNStations());
	maxResY.resize(this->getNStations());
	maxResZ.resize(this->getNStations());
	maxRobX.resize(this->getNStations());
	maxRobY.resize(this->getNStations());
	maxRobZ.resize(this->getNStations());
	maxResLabelX.resize(this->getNStations());
	maxResLabelY.resize(this->getNStations());
	maxResLabelZ.resize(this->getNStations());
	maxRobLabelX.resize(this->getNStations());
	maxRobLabelY.resize(this->getNStations());
	maxRobLabelZ.resize(this->getNStations());

	RMS.resize(this->getNStations());
	NPointsX.resize(this->getNStations());;
	NPointsY.resize(this->getNStations());;
	NPointsZ.resize(this->getNStations());;

	this->nGrossErrors = 0;

	if (this->getNStations() > 0)
	{
		for (int i = 0; i < this->getNStations(); ++i)
		{
			CObservationHandler* oh = ObservationHandlers.GetAt(i);
			oh->resetIndices();
		}

		CLabelMerger labelMerger;
		CCharStringArray mergedLabels;
	    
		labelMerger.merge(mergedLabels, ObservationHandlers);

		// Form a Vector of coorespondance indexes.
		// The vector will contain "nobs" elements. Each element is an int[2],
		// and as such, each observation has an array of two indexes. The first
		// is the point index cooresponding to the index inside the XYZ points
		// array, the second is the station index cooresponding to the index in
		// the station array.

		// First reduce the sightings to the ones that have enough rays
		CIntegerArrayArray* sightings = labelMerger.getSightings();
		int index = 0;

		CIntegerArrayArray reducedSightings;

		this->ObjectPoints.RemoveAll();

		int nTotalRays = 0;
		this->nPointPars = 0;

		this->nPointObs = 0;

		for (int i = 0; i < sightings->GetSize(); i++)
		{
			CMUIntegerArray* sighting = sightings->GetAt(i);

			// count the number of rays for this point
			int nObs  = 0;
			int nRays = 0;
			int FirstObsIdx = -1;
			bool useSight = excludeTie ? false : true;

			for (int j = 0; j < sighting->GetSize(); j++)
			{
				
				CObservationHandler* oh = ObservationHandlers.GetAt(j);
				int nObsPerRay = oh->getNpointObservations();
				int* s = sighting->GetAt(j);

				if (excludeTie && *s && oh->isControl())
					useSight = true;

				nRays += *s;
				nObs  += *s * nObsPerRay;
				if (*s) FirstObsIdx = j;
			}

			bool enoughObs = ((nObs > this->minObs) && useSight);

			if ((nObs == this->minObs) && (nRays == 1))
			{
				if (!ObservationHandlers.GetAt(FirstObsIdx)->isControl()) enoughObs = true;
			}

			if (enoughObs)
			{
				nTotalRays += nRays;
				reducedSightings.Add(*sighting);
				CCharString* label = mergedLabels.GetAt(i);

				CObsPoint *ObjectPointsPoint = this->ObjectPoints.Add();
				ObjectPointsPoint->setLabel(CLabel(label->GetChar()));
				this->nPointPars += 3;
				this->nPointObs += nObs;
			}
		}

		CMUIntegerArray pointIndex;
		pointIndex.SetSize(this->ObservationHandlers.GetSize());
	    
		for (int i = 0; i < pointIndex.GetSize(); i++)
		{
			int* index = pointIndex.GetAt(i);
			*index = 0;
		}

		for (int i=0; i < this->getNStations(); ++i)
		{
			this->nPointObs += this->ObservationHandlers.GetAt(i)->getNOrbitPoints();
			this->nConstraints += this->ObservationHandlers.GetAt(i)->getNConstraints();
		}

		// Loop over all points (each sighting represents one XYZ point)
		for (int iPoint = 0; iPoint < reducedSightings.GetSize(); iPoint++)
		{
			CObsPoint *ObjectPoint = this->ObjectPoints.GetAt(iPoint);
			CLabel currentPointLabel = ObjectPoint->getLabel();

			CMUIntegerArray* sighting = reducedSightings.GetAt(iPoint);
			CObservationHandler* oh = 0;
			CObsPoint* ObsPoint = 0;

			for (int iStation = 0; iStation < sighting->GetSize(); iStation++)
			{
				// check that point was seen form this station
				int s = *(sighting->GetAt(iStation));
				if (s)
				{
					//CObservationHandler* oh = ObservationHandlers.GetAt(iStation);
					oh = ObservationHandlers.GetAt(iStation);
					
					CCharString thisLabel = "";
					while (!thisLabel.CompareNoCase(currentPointLabel.GetChar()))
					{
						CObsPointArray* points = oh->getObsPoints();
						int* pi = pointIndex.GetAt(iStation);
						ObsPoint = points->GetAt(*pi);
						thisLabel = ObsPoint->getLabel().GetChar();
						(*pi)++;
					}
					
					if (ObsPoint && ObsPoint->getStatusBit(0))
						ObsPoint->id = iPoint;
				}
			}

			if (ObsPoint != NULL && oh->getDimension() == 3) // for XYZPoints 
			{
				(*ObjectPoint)[2] = (*ObsPoint)[2]; // set Z
				oh->getCurrentSensorModel()->transformObs2Obj(*ObjectPoint, *ObsPoint);
			}
			else if (ObsPoint != NULL && oh->getDimension() == 2) oh->getCurrentSensorModel()->transformObs2Obj(*ObjectPoint, *ObsPoint, this->averageHeight, 1.0);
		}
	}
}


void CBundleManager::initParameters(bool forwardOnly)
{
	nDirectObs = 0;
	// to have at least one station
	if (this->getNStations() > 0)
	{
		// Set up parameter approximations
		this->solution.ReSize(this->getNUnknowns() + this->getNConstraints(), 1);
        this->solution = 0.0;
		vector<int> cameraIndices;
		this->getCameraIndices(cameraIndices);

		int offset = 0;
		int offsetConstraint = this->getNUnknowns();

		if (!forwardOnly)
		{
			for (int i = 0; i < this->getNStations(); i++)
			{
				CObservationHandler* oh = ObservationHandlers.GetAt(i);
				CSensorModel *sensorMod = oh->getCurrentSensorModel();

				sensorMod->setParameterIndex(CSensorModel::shiftPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::shiftPar);
				sensorMod->setParameterIndex(CSensorModel::rotPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::rotPar);
				sensorMod->setParameterIndex(CSensorModel::scalePar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::scalePar);
				sensorMod->setParameterIndex(CSensorModel::mountShiftPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::mountShiftPar);
				sensorMod->setParameterIndex(CSensorModel::mountRotPar,offset);

				offset += sensorMod->getParameterCount(CSensorModel::mountRotPar);

				if ((cameraIndices[i] < 0) || (cameraIndices[i] == i))
				{
					sensorMod->setParameterIndex(CSensorModel::interiorPar,offset);
					offset += sensorMod->getParameterCount(CSensorModel::interiorPar);
					sensorMod->setParameterIndex(CSensorModel::addPar,offset);
					offset += sensorMod->getParameterCount(CSensorModel::addPar);
				}
				else
				{
					sensorMod->setParameterIndex(CSensorModel::interiorPar, ObservationHandlers.GetAt(cameraIndices[i])->getCurrentSensorModel()->getParameterIndex(CSensorModel::interiorPar));
					sensorMod->setParameterIndex(CSensorModel::addPar,      ObservationHandlers.GetAt(cameraIndices[i])->getCurrentSensorModel()->getParameterIndex(CSensorModel::addPar));
				}
				
				sensorMod->setParameterIndex(CSensorModel::constraints,offsetConstraint);
				offsetConstraint+=sensorMod->getParameterCount(CSensorModel::constraints);

				if (oh->getNstationParameters() > 0)
					sensorMod->insertParametersForAdjustment(this->solution);
					
				nDirectObs += sensorMod->getDirectObsCount();
			}
		}

		for (int i = 0; i < this->getNObjectPoints(); i++)
		{
			CObsPoint* xyzPoint = this->ObjectPoints.GetAt(i);
			xyzPoint->id = offset;
			this->solution.element(offset, 0) = (*xyzPoint)[0]; ++offset;
			this->solution.element(offset, 0) = (*xyzPoint)[1]; ++offset;
			this->solution.element(offset, 0) = (*xyzPoint)[2]; ++offset;
		}
	}
}
void CBundleManager::printToProtocol(const char* stationName, const char* parameterType, 
									 const char *pointName,  const Matrix &obs, const Matrix &Fi)
{
	if (protocol)
	{
		protocol->setf(ios::fixed, ios::floatfield);
		
		*protocol << "\nPoint: " << pointName << " in "  << stationName << " with " << parameterType  
				  << "\nObservation    /    discrepancy: \n";

		for (int i = 0; i < Fi.Nrows(); ++i)
		{
			protocol->precision(3);
			protocol->width(12);
			*protocol << obs.element(0, i) << "   / ";
			protocol->precision(9);
			protocol->width(15);
			*protocol << Fi.element(i,0) << "\n";
		}

		*protocol << "\n";
	};
}; 
void CBundleManager::printResToProtocol(const char* stationName, const char* parameterType, 
									    const char *pointName,  const Matrix &obs, const Matrix &res)
{
	if (protocol)
	{
		protocol->setf(ios::fixed, ios::floatfield);
		
		*protocol << "\nPoint: " << pointName << " in "  << stationName << " with " << parameterType  
				  << "\nObservation    /    residual: \n";

		for (int i = 0; i < res.Nrows(); ++i)
		{
			protocol->precision(3);
			protocol->width(12);
			*protocol << obs.element(0, i) << "   / ";
			protocol->precision(3);
			protocol->width(12);
			*protocol << res.element(i,0) << "\n";
		}

		*protocol << "\n";	};
}; 

void CBundleManager::updateParameters(bool forwardOnly)
{
	if (!forwardOnly)
	{
		for (int i = 0; i < this->getNStations(); i++)     
		{ 
			CObservationHandler* oh = ObservationHandlers.GetAt(i);
			oh->getCurrentSensorModel()->setParametersFromAdjustment(this->solution);
		};
	}

	for (int i = 0; i < this->getNObjectPoints(); i++)     
	{ 
		CObsPoint *approx = this->ObjectPoints.GetAt(i);
		int offset = approx->id;
		(*approx)[0] = this->solution.element(offset,     0);
		(*approx)[1] = this->solution.element(offset + 1, 0);
		(*approx)[2] = this->solution.element(offset + 2, 0);

	}
};

void CBundleManager::getStationParsVec(int i, const Matrix &parVec, Matrix &stationPars)
{
	CObservationHandler* oh = ObservationHandlers.GetAt(i);
	CSensorModel *sensorMod = oh->getCurrentSensorModel();

	int startE  = sensorMod->getParameterIndex(CSensorModel::shiftPar);
	int startR  = sensorMod->getParameterIndex(CSensorModel::rotPar);
	int startS  = sensorMod->getParameterIndex(CSensorModel::scalePar);
	int startMS = sensorMod->getParameterIndex(CSensorModel::mountShiftPar);
	int startMR = sensorMod->getParameterIndex(CSensorModel::mountRotPar);
	int startI  = sensorMod->getParameterIndex(CSensorModel::interiorPar);
	int startA  = sensorMod->getParameterIndex(CSensorModel::addPar);

	stationPars.ReSize(sensorMod->getNumberOfStationPars(), 1);

	int parIdx = 0;

	for (int j = 0; j < sensorMod->getParameterCount(CSensorModel::shiftPar); ++j, ++parIdx)
	{
		stationPars.element(parIdx, 0) = parVec.element(startE + j, 0);
	};

	for (int j = 0; j < sensorMod->getParameterCount(CSensorModel::rotPar); ++j, ++parIdx)
	{
		stationPars.element(parIdx, 0) = parVec.element(startR + j, 0);
	};

	for (int j = 0; j < sensorMod->getParameterCount(CSensorModel::scalePar); ++j, ++parIdx)
	{
		stationPars.element(parIdx, 0) = parVec.element(startS + j, 0);
	};

	for (int j = 0; j < sensorMod->getParameterCount(CSensorModel::mountShiftPar); ++j, ++parIdx)
	{
		stationPars.element(parIdx, 0) = parVec.element(startMS + j, 0);
	};

	for (int j = 0; j < sensorMod->getParameterCount(CSensorModel::mountRotPar); ++j, ++parIdx)
	{
		stationPars.element(parIdx, 0) = parVec.element(startMR + j, 0);
	};

	for (int j = 0; j < sensorMod->getParameterCount(CSensorModel::interiorPar); ++j, ++parIdx)
	{
		stationPars.element(parIdx, 0) = parVec.element(startI + j, 0);
	};

	for (int j = 0; j < sensorMod->getParameterCount(CSensorModel::addPar); ++j, ++parIdx)
	{
		stationPars.element(parIdx, 0) = parVec.element(startA + j, 0);
	};
};

double CBundleManager::calculateResiduals(bool forwardOnly, const Matrix &Delta,
										  bool omitMarked)
{      	
	double vtpv = 0.0;

	//addition>>
	//bool posDef[2000];
	//<<addition

	for (int iStation = 0; iStation < this->getNStations(); ++iStation)
	{
		CObservationHandler* oh = ObservationHandlers.GetAt(iStation);

		if (!oh->getUseObservationHandler())
			continue;

		CSensorModel *sensorMod = oh->getCurrentSensorModel();
		CObsPointArray  *obsPoints  = oh->getObsPoints();

		int nPoints = obsPoints->GetSize();
		CObsPoint *approx = NULL;
		Matrix *Qi;

		//addition
		//double ck[2000];
		//double mat[2000][9]; 
		//FILE *fp, *fp1;
		//fp = fopen("I:\\QuickBird\\covar.txt","w");
		//fp1 = fopen("I:\\QuickBird\\covar_cng1.txt","r");
		//addition
		for (int i = 0; i < nPoints; ++i)
		{
			CObsPoint* obs = obsPoints->GetAt(i);
			

			int dimension = obs->getDimension();
			Matrix resMat(dimension,1);
			Matrix obsMat(1,dimension);
			CObsPoint resPoint(dimension);

			if (oh->hasUnknownObjPoints())
			{
				if (obs->id >= 0)
				{
					approx = this->ObjectPoints.GetAt(obs->id);
					if (!forwardOnly) resPoint = sensorMod->calcRes(Delta, *obs, *approx);
					else
					{
						sensorMod->transformObj2Obs(resPoint,*approx);
						resPoint -= *obs;
					}

					Qi = obs->getCovariance();
					for (int i=0; i< dimension; i++)
					{
						obsMat.element(0,i) = resPoint[i];
						resMat.element(i,0) = resPoint[i];
					}
				}
				else Qi = 0;
			}
			else
			{
				resPoint = sensorMod->calcRes(Delta, *obs, CObsPoint(3));
				Qi = obs->getCovariance();
				
				//addition>>
				double q00 = Qi->element(0,0);
				double q01 = Qi->element(0,1);
				double q02 = Qi->element(0,2);
				double q10 = Qi->element(1,0);
				double q11 = Qi->element(1,1);
				double q12 = Qi->element(1,2);
				double q20 = Qi->element(2,0);
				double q21 = Qi->element(2,1);
				double q22 = Qi->element(2,2);
				//int pd; 
				//fscanf(fp1,"%d",&pd);
				if (!(q00>0 && q11>0 && q22>0 && q00>(q01+q02) && q11>(q10+q12) && q22>(q20+q21)))
					//posDef[i] = true;
				//else //if (pd == 1)
				{
					//posDef[i] = false;
					
					if (q11<(q10+q12))
					{
						Qi->element(1,1) += fabs(max(fabs(Qi->element(1,0)),fabs(Qi->element(1,2))));
					}
					else if (q00<(q01+q02))
					{
						Qi->element(0,0) += fabs(max(fabs(Qi->element(0,1)),fabs(Qi->element(0,2))));
					}
					else if (q22<(q20+q21))
					{
						Qi->element(2,2) += fabs(max(fabs(Qi->element(2,0)),fabs(Qi->element(2,1))));
					}
					
				}
				//else
				//{
				//	posDef[i] = false;
				//}
				//fprintf(fp,"%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t",q00,q01,q02,q10,q11,q12,q20,q21,q22);
				//<<addition

				for (int i=0; i< dimension; i++)
				{
					obsMat.element(0,i) = resPoint[i];
					resMat.element(i,0) = resPoint[i];
				}
			}	
			if (Qi != 0)
			{
//				printResToProtocol(oh->getFileNameChar(), sensorMod->getFileNameChar(), approx->getLabel().GetChar(), obsMat, res); 

				Matrix P = Qi->i();

				if (omitMarked)
				{
					DiagonalMatrix W(P.Nrows());

					for (int i = 0; i < dimension; i++)
					{
						if (obs->getStatusBit(ROBUSTBITBASE + i))
							W.element(i,i) = 0.0;
						else W.element(i,i) = 1.0;
					}

					P = W * P * W;
				}

			        
				Matrix vTv = resMat.t() * P * resMat;
				
				//addition
				//ck[i] = vTv.element(0,0);

				//if (i == 363)
				//{
				//	int here = 1;
				//}
				//fprintf(fp,"%f\n",ck[i]);
				//addition

				vtpv += vTv.element(0,0);
			}
		}

		//addition>>
		//fclose(fp);
		//fclose(fp1);
		//<<addition

		if (!forwardOnly && sensorMod->getDirectObsCount()) 
		{
			Matrix dpar;
			Matrix resMat;
			getStationParsVec(iStation, Delta, dpar);
			vtpv += sensorMod->getResidualsDirectObservations(dpar, resMat);
		}
	}

	return vtpv;
};
	  	  
void CBundleManager::dumpMatrix(const Matrix &mat, const char *filename, int digits, int precision)
{
	ofstream out(filename);
	out.setf(ios::fixed, ios::floatfield);
	out.precision(precision);

	for (int i = 0; i < mat.Nrows(); i++)
	{
		for (int j=0; j < mat.Ncols(); j++)
		{
			out.width(digits);
			out << mat.element(i,j);
		}

		out << "\n";
	}
};

bool CBundleManager::invertNormalEquationMatrix(const Matrix &N)
{
	bool ret = true;
	
	if (this->NormalPointIsBlockDiagonal && (this->ObjectPoints.GetSize() > 0) && (N.Nrows() > 1000))
	{
		CXYZPoint *p = this->ObjectPoints.getAt(0);
		int indexFirstPoint = p->id;

		SymmetricMatrix Q(N.Nrows() - this->nConstraints);
		SymmetricMatrix N22Inv(Q.Nrows() - indexFirstPoint);
		N22Inv = 0.0;

		for (int iPt = 0; iPt < this->ObjectPoints.GetSize(); ++iPt)
		{
			p = this->ObjectPoints.getAt(iPt);
			SymmetricMatrix NPt(p->getDimension());

			for (int i = 0; i < NPt.Nrows(); ++i)
			{
				for (int j = i; j < NPt.Ncols(); ++j)
				{
					NPt.element(i,j) = N.element(p->id + i, p->id + j);
				}
			}
			NPt = NPt.i();
			for (int i = 0; i < NPt.Nrows(); ++i)
			{
				int iN22 = p->id - indexFirstPoint;
				for (int j = i; j < NPt.Ncols(); ++j)
				{
					N22Inv.element(iN22 + i, iN22 + j) = NPt.element(i,j);
				}
			}
		}

		if (indexFirstPoint == 0) Q = N22Inv;
		else
		{

			Matrix N12(indexFirstPoint, N.Ncols() - this->nConstraints - indexFirstPoint);

			for (int i = 0; i < N12.Nrows(); ++i)
			{
				for (int j = 0; j < N12.Ncols(); ++j)
				{
					N12.element(i,j) = N.element(i, indexFirstPoint + j);
				}
			}

			Matrix N12N22inv = N12 * N22Inv;
			Matrix Hlp = N12N22inv * N12.t();

			SymmetricMatrix Q11(indexFirstPoint);

			for (int i = 0; i < Q11.Nrows(); ++i)
			{
				for (int j = i; j < Q11.Ncols(); ++j)
				{
					Q11.element(i,j) = N.element(i,j) - Hlp.element(i,j);
				}
			}

			Q11 = Q11.i();

			Matrix Q12 = -Q11 * N12N22inv;
			Hlp = -N12.t() * Q12;
			for (int i = 0; i < Hlp.Nrows(); ++i) Hlp.element(i,i) += 1.0;
			Hlp = N22Inv * Hlp;

			for (int i = 0; i < Q11.Nrows(); ++i)
			{
				for (int j = i; j < Q11.Ncols(); ++j)
				{
					Q.element(i,j) = Q11.element(i,j);
				}
			}
			for (int i = 0; i < Q12.Nrows(); ++i)
			{
				for (int j = 0; j < Q12.Ncols(); ++j)
				{
					Q.element(i, indexFirstPoint +j) = Q12.element(i, j);
				}
			}
			for (int i = 0; i < Hlp.Nrows(); ++i)
			{
				for (int j = i; j < Hlp.Ncols(); ++j)
				{
					Q.element(indexFirstPoint + i,indexFirstPoint +j) = Hlp.element(i,j);
				}
			}
		}

		if (this->nConstraints <= 0) this->Qxx = Q;
		else
		{
			SymmetricMatrix NWithoutConstraints(N.Nrows() - this->nConstraints);
		
			for (int i = 0; i < NWithoutConstraints.Nrows(); ++i)
			{
				for (int j = i; j < NWithoutConstraints.Ncols(); ++j)
				{
					NWithoutConstraints.element(i,j) = N.element(i, j);
				}
			}

//			dumpMatrix(Q - NWithoutConstraints.i(), "dqxxOhneConstraints.txt", 30, 10);

			Matrix H(N.Nrows() - this->nConstraints, this->nConstraints);

			for (int i = 0; i < H.Nrows(); ++i)
			{
				for (int j = 0; j < H.Ncols(); ++j)
				{
					H.element(i,j) = N.element(i, j + Q.Ncols());
				}
			}

			Matrix Hlp = H.t() * Q;
			Matrix Q12t = Hlp * H;
			Q12t = Q12t.i();
			Q12t = Q12t * Hlp;
			Hlp = -H * Q12t;


			for (int i = 0; i < Hlp.Nrows(); ++i) Hlp.element(i,i) += 1.0;

			Hlp = Q * Hlp;

			this->Qxx = 0;

			for (int i = 0; i < Hlp.Nrows(); ++i)
			{
				for (int j = 0; j < Hlp.Ncols(); ++j)
				{
					this->Qxx.element(i,j) = Hlp.element(i,j);
				}
			}

			double el; int jQ;
			for (int i = 0; i < Q12t.Ncols(); ++i)
			{
				for (int j = 0; j < Q12t.Nrows(); ++j)
				{
					el = Q12t.element(j,i);
					jQ = Hlp.Ncols() + j;
					this->Qxx.element(i, jQ) = this->Qxx.element(jQ, i) = el;
				}
			}

			Hlp = -Q12t * NWithoutConstraints * Q12t.t();

			for (int i = 0; i < Hlp.Nrows(); ++i)
			{
				for (int j = 0; j < Hlp.Ncols(); ++j)
				{
					this->Qxx.element(i + NWithoutConstraints.Nrows(),j + NWithoutConstraints.Nrows()) = Hlp.element(i,j);
				}
			}


		}

//		Matrix mat = N.i() ;	
//		dumpMatrix(this->Qxx - mat, "dQxx.txt", 30, 10);
	}
	else
	{
		this->Qxx = N.i();	
	}
	return ret;
};

void CBundleManager::solveOnce(CRobustWeightFunction *robust, bool omitMarked)
{
	int nParms = this->getNUnknowns() + this->getNConstraints();
	this->nGrossErrors = 0;

	Matrix N(nParms, nParms); // The normals
	Matrix t(nParms, 1); // the right hand side

	N = 0;
	t = 0;

	int nStations = this->getNStations();

	int nObsTotal =0;
	int progressCount =0;
	for (int k = 0; k < nStations; k++)
	{
		CObservationHandler* oh = ObservationHandlers.GetAt(k);
		nObsTotal += oh->getObsPoints()->GetSize();
	}

	this->worstResiduals.reset();

	this->nPointObs = 0;

	vector<int> nObsPerObj(this->ObjectPoints.GetSize(),0);


	for (int k = 0; k < nStations; k++)
	{
		ostrstream oss;
		oss << "("; oss.width(3); oss << (k+1) << ")" << ends;
		char *z = oss.str();
		CObservationHandler* oh = ObservationHandlers.GetAt(k);
		CSensorModel *sensorMod = oh->getCurrentSensorModel();

		CResidualRecord resRec(sensorMod, z);
		delete [] z;

		int dimension = oh->getDimension();
		CObsPoint resNorm(dimension);

		CObsPoint res(dimension);

		maxResX[k] = FLT_MIN;
		maxResY[k] = FLT_MIN;
		maxResZ[k] = FLT_MIN;
		maxRobX[k] = FLT_MIN;
		maxRobY[k] = FLT_MIN;
		maxRobZ[k] = FLT_MIN;
		RMS[k].x = RMS[k].y = RMS[k].z = 0.0;

		NPointsX[k] = 0;
		NPointsY[k] = 0;
		NPointsZ[k] = 0;

		if (!oh->getUseObservationHandler())
			continue;
		
		if (oh->getObsPoints())  
		{
			CObsPointArray *obsPoints = oh->getObsPoints();
			int nPoints =  obsPoints->GetSize();
			for (int i = 0; i <nPoints; ++i)
			{
				CObsPoint *obs = obsPoints->GetAt(i);
				resRec.setObs(obs);
				
				if (obs->id >= 0 ||!oh->hasUnknownObjPoints() )
				{
					CObsPoint *obj = 0;
					CObsPoint dummy(3); // 3D
					if (obs->id >= 0 && oh->hasUnknownObjPoints())
						obj = this->ObjectPoints.GetAt(obs->id);
					else if (!oh->hasUnknownObjPoints())
						obj = &dummy;

					if (!obj) continue;

					int nlocPtObs = sensorMod->accumulate(N,t,*obs, *obj, res, resNorm, robust, omitMarked);
					this->nPointObs += nlocPtObs;

					if ((obs->id >= 0) && (oh->hasUnknownObjPoints())) nObsPerObj[obs->id] += nlocPtObs;

					this->worstResiduals.insertRecord(resRec, *obs, res, resNorm, omitMarked || robust);

					for (int m = 0; m < dimension; ++m) 
					{
						if (obs->getStatusBit(ROBUSTBITBASE + m)) ++this->nGrossErrors;
					}

					// save the max residuals
					if ((!obs->getStatusBit(ROBUSTBITBASE + 0) || (!omitMarked && !robust)) && nlocPtObs)
					{
						if (fabs(res[0]) > fabs(maxResX[k]))
						{
							maxResX[k] = res[0];
							maxResLabelX[k] = obs->getLabel();
						}

						if (resNorm[0] > maxRobX[k])
						{
							maxRobX[k] = resNorm[0];
							maxRobLabelX[k] = obs->getLabel();
						}

						RMS[k].x += res[0] * res[0];
						++NPointsX[k];
					}

					if ((!obs->getStatusBit(ROBUSTBITBASE + 1) || (!omitMarked && !robust)) && nlocPtObs)
					{
						if (fabs(res[1]) >  fabs(maxResY[k]))
						{
							maxResY[k] = res[1];
							maxResLabelY[k] = obs->getLabel();
						}
						if (resNorm[1] > maxRobY[k])
						{
							maxRobY[k] = resNorm[1];
							maxRobLabelY[k] = obs->getLabel();
						}

						RMS[k].y += res[1] * res[1];
						++NPointsY[k];
					}

					if (dimension >= 3)
					{	
						if ((!obs->getStatusBit(ROBUSTBITBASE + 2) || (!omitMarked && !robust)) && nlocPtObs)
						{
							if (fabs(res[2]) > fabs(maxResZ[k]))
							{
								maxResZ[k] = res[2];
								maxResLabelZ[k] = obs->getLabel();
							}							
							if (resNorm[2] > maxRobZ[k])
							{
								maxRobZ[k] = resNorm[2];
								maxRobLabelZ[k] = obs->getLabel();
							}
							RMS[k].z += res[2] * res[2];
							++NPointsZ[k];
						}
					}

					if (this->listener)
					{
						progressCount++;
						this->listener->setProgressValue((double)progressCount/(double)nObsTotal * 100);
					}
				}
			}

			if (NPointsX[k] > 0) RMS[k].x = sqrt(RMS[k].x / (double) NPointsX[k]);
			if (NPointsY[k] > 0) RMS[k].y = sqrt(RMS[k].y / (double) NPointsY[k]);
			if (NPointsZ[k] > 0) RMS[k].z = sqrt(RMS[k].z / (double) NPointsZ[k]);

			printMaxResRecord(k);
		}


		if (sensorMod->getDirectObsCount())
		{
			sensorMod->addDirectObservations(N, t);	                                                  
		}

		if (sensorMod->getParameterCount(CSensorModel::constraints))
		{
			sensorMod->addConstraint(&N,&t,NULL);
		}
	}

#ifndef FINAL_RELEASE
//	dumpMatrix(N, "N.txt", 30, 10);
//	dumpMatrix(t, "t.txt", 30, 10);
#endif

	if (protocol)
	{
		(*protocol)  << "\n\n" << this->worstResiduals.getString(20) << "\n" << endl;
	}
	
	if (robust)
	{
		// direct observations for avoiding singularities if robust estimation 'kills' too many observations
		double w = 1.0 / 1000000;
		for (unsigned int i=0; i < nObsPerObj.size(); i++)
		{	
			CObsPoint *obj = this->ObjectPoints.GetAt(i);
			int dim = obj->getDimension() > 3? 3 : obj->getDimension();

			if (nObsPerObj[i] < dim)
			{
				for (int j = 0; j < dim; ++j)
				{
					N.element(obj->id + j,obj->id+ j) += w; 
				}
			}
		}
	}
		

	invertNormalEquationMatrix(N);
//	this->Qxx = N.i();	
#ifndef FINAL_RELEASE
//	dumpMatrix(this->Qxx, "Q.txt", 30, 10);
#endif

	Matrix Delta = this->Qxx * t;

	
	//FILE* out = fopen("Qxx.txt","w");

	//for (int i = 0; i < this->Qxx.Nrows(); i++)
	//{
	//	for (int k=0; k < this->Qxx.Ncols(); k++)
	//	{
	//		fprintf(out,"%20.3lf",this->Qxx(i+1,k+1));
	//	}
	//	fprintf(out,"\n");
	//}
	//fclose(out);


	// update parameters         
	this->solution = this->solution - Delta;

	
	//out = fopen("solution.txt","w");

	//for (int i = 0; i < this->solution.Nrows(); i++)
	//{
	//	for (int k=0; k < this->solution.Ncols(); k++)
	//	{
	//		fprintf(out,"%20.3lf",this->solution(i+1,k+1));
	//	}
	//	fprintf(out,"\n");
	//}
	//fclose(out);


	// Calculate residuals
	this->rms = this->calculateResiduals(false, Delta, true);

	//addition
	//if (this->rms < 0)
	//{
		//this->rms *=-1.0;
	//	int here = 1;
	//}
	//addition

	this->redundancy = getNObs() + getNConstraints() - getNUnknowns();

	this->updateParameters(false);	
};

void CBundleManager::solveOnceForward(CRobustWeightFunction *robust, bool omitMarked)
{
	int nParms = this->getNUnknowns() + this->getNConstraints();
	
	Matrix N(nParms, nParms); // The normals
	Matrix t(nParms, 1); // the right hand side

	N = 0;
	t = 0;

	int nStations = this->getNStations();

	int nObsTotal =0;
	int progressCount =0;
	for (int k = 0; k < nStations; k++)
	{
		CObservationHandler* oh = ObservationHandlers.GetAt(k);
		nObsTotal += oh->getObsPoints()->GetSize();
	}

	this->worstResiduals.reset();
	Matrix Ai, l, Fi,Bi;

	for (int k = 0; k < nStations; k++)
	{
		CObservationHandler* oh = ObservationHandlers.GetAt(k);
		CSensorModel *sensorMod = oh->getCurrentSensorModel();

		CResidualRecord resRec(sensorMod, oh->getFileNameChar());

		int dimension = oh->getDimension();
		
		int nObsPoints = 0;

		if (oh->getObsPoints())  
		{
			l.ReSize(1, dimension);
			CObsPointArray *obsPoints = oh->getObsPoints();
			int nPoints =  obsPoints->GetSize();
			for (int i = 0; i <nPoints; ++i)
			{
				CObsPoint *obs = obsPoints->GetAt(i);
				resRec.setObs(obs);
				
				if (obs->id >= 0 ||!oh->hasUnknownObjPoints() )
				{
					CObsPoint *obj = 0;
					CObsPoint dummy(3);
					if (obs->id >= 0 && oh->hasUnknownObjPoints())
						obj = this->ObjectPoints.GetAt(obs->id);
					else if (!oh->hasUnknownObjPoints())
						obj = &dummy;

					if (!obj) continue;

					for (int j = 0; j < dimension; ++j) l.element(0, j) = (*obs)[j];

					sensorMod->Fi(Fi, l, *obj);
					sensorMod->Bi(Bi, l, *obj);
					sensorMod->AiPoint(Ai, l, *obj);
					Matrix P = Bi * obs->getCovar() * Bi.t();
					P = P.i();
					Matrix nPt = Ai.t() * P;
					Matrix AtlPt = nPt * Fi;
					nPt *= Ai;
					for (int r = 0; r < 3; ++r)
					{
						int rInd = r + obj->id;
						t.element(rInd,0) += AtlPt.element(r, 0);

						for (int c = 0; c < 3; ++c)
						{
							N.element(rInd, c + obj->id) += nPt.element(r, c);
						}
					}
				
					++nObsPoints;

					if (this->listener)
					{
						progressCount++;
						this->listener->setProgressValue((double)progressCount/(double)nObsTotal * 100);
					}

				}



			}
		}

	}

	if (protocol)
	{
		(*protocol)  << "\n\n" << this->worstResiduals.getString(20) << "\n" << endl;
	}

	invertNormalEquationMatrix(N);
//	this->Qxx = N.i();	
	Matrix Delta = this->Qxx * t;

	// update parameters         
	this->solution = this->solution - Delta;

	// Calculate residuals
	this->rms = this->calculateResiduals(true, Delta, true);

	this->updateParameters(true);	
};
		
void CBundleManager::updateParameterCovar(bool forwardOnly)
{
	
	if (!forwardOnly)
	{
#ifndef FINAL_RELEASE
		CCharStringArray startParameters;
#endif
		for (int i = 0; i < this->getNStations(); i++)     
		{ 
			CObservationHandler* oh = ObservationHandlers.GetAt(i);
			CSensorModel *sensorMod = oh->getCurrentSensorModel();

			Matrix VarCovar(sensorMod->getNumberOfStationPars(), sensorMod->getNumberOfStationPars());
			VarCovar = 0.0f;
#ifndef FINAL_RELEASE
			CCharString dummy;
			if (sensorMod->getNumberOfStationPars())
			{
				sensorMod->getName(dummy);
				startParameters.Add(dummy);
			}
#endif
			int startE  = sensorMod->getParameterIndex(CSensorModel::shiftPar);
			int startR  = sensorMod->getParameterIndex(CSensorModel::rotPar);
			int startS  = sensorMod->getParameterIndex(CSensorModel::scalePar);
			int startMS = sensorMod->getParameterIndex(CSensorModel::mountShiftPar);
			int startMR = sensorMod->getParameterIndex(CSensorModel::mountRotPar);
			int startI  = sensorMod->getParameterIndex(CSensorModel::interiorPar);
			int startA  = sensorMod->getParameterIndex(CSensorModel::addPar);

			int covarIndex = 0;

			int nlocPar = sensorMod->getParameterCount(CSensorModel::shiftPar);

			if (nlocPar)
			{
				VarCovar.SubMatrix(covarIndex + 1, covarIndex + nlocPar, covarIndex + 1, covarIndex + nlocPar) = 
								   this->Qxx.SubMatrix(startE + 1, startE + nlocPar, startE + 1, startE + nlocPar);

				covarIndex += nlocPar;
#ifndef FINAL_RELEASE
				dummy.Format("Shift Parameter %d",nlocPar);
				startParameters.Add(dummy);
#endif
			};
			
			nlocPar = sensorMod->getParameterCount(CSensorModel::rotPar);

			if (nlocPar)
			{
				VarCovar.SubMatrix(covarIndex + 1, covarIndex + nlocPar, covarIndex + 1, covarIndex + nlocPar) = 
								   this->Qxx.SubMatrix(startR + 1, startR + nlocPar, startR + 1, startR + nlocPar);

				covarIndex += nlocPar;
#ifndef FINAL_RELEASE
				dummy.Format("Rot Parameter %d",nlocPar);
				startParameters.Add(dummy);
#endif
			};

			nlocPar = sensorMod->getParameterCount(CSensorModel::scalePar);

			if (nlocPar)
			{
				VarCovar.SubMatrix(covarIndex + 1, covarIndex + nlocPar, covarIndex + 1, covarIndex + nlocPar) = 
								   this->Qxx.SubMatrix(startS + 1, startS + nlocPar, startS + 1, startS + nlocPar);

				covarIndex += nlocPar;
#ifndef FINAL_RELEASE
				dummy.Format("Scale Parameter %d",nlocPar);
				startParameters.Add(dummy);
#endif
			};
			
			nlocPar = sensorMod->getParameterCount(CSensorModel::mountShiftPar);

			if (nlocPar)
			{
				VarCovar.SubMatrix(covarIndex + 1, covarIndex + nlocPar, covarIndex + 1, covarIndex + nlocPar) = 
								   this->Qxx.SubMatrix(startMS + 1, startMS + nlocPar, startMS + 1, startMS + nlocPar);

				covarIndex += nlocPar;
#ifndef FINAL_RELEASE
				dummy.Format("Mount Shift Parameter %d",nlocPar);
				startParameters.Add(dummy);
#endif
			};
			
			nlocPar = sensorMod->getParameterCount(CSensorModel::mountRotPar);

			if (nlocPar)
			{
				VarCovar.SubMatrix(covarIndex + 1, covarIndex + nlocPar, covarIndex + 1, covarIndex + nlocPar) = 
								   this->Qxx.SubMatrix(startMR + 1, startMR + nlocPar, startMR + 1, startMR + nlocPar);

				covarIndex += nlocPar;
#ifndef FINAL_RELEASE
				dummy.Format("Mount Rot Parameter %d",nlocPar);
				startParameters.Add(dummy);
#endif
			};

			nlocPar = sensorMod->getParameterCount(CSensorModel::interiorPar);

			if (nlocPar)
			{
				VarCovar.SubMatrix(covarIndex + 1, covarIndex + nlocPar, covarIndex + 1, covarIndex + nlocPar) = 
								   this->Qxx.SubMatrix(startI + 1, startI + nlocPar, startI + 1, startI + nlocPar);

				covarIndex += nlocPar;
#ifndef FINAL_RELEASE
				dummy.Format("Interior Parameter %d",nlocPar);
				startParameters.Add(dummy);
#endif
			};


			nlocPar = sensorMod->getParameterCount(CSensorModel::addPar);

			if (nlocPar)
			{
				VarCovar.SubMatrix(covarIndex + 1, covarIndex + nlocPar, covarIndex + 1, covarIndex + nlocPar) = 
								   this->Qxx.SubMatrix(startA + 1, startA + nlocPar, startA + 1, startA + nlocPar);

				covarIndex += nlocPar;
#ifndef FINAL_RELEASE
				dummy.Format("Additional Parameter %d",nlocPar);
				startParameters.Add(dummy);
#endif
			};

#ifndef FINAL_RELEASE
			if (sensorMod->getNumberOfStationPars())
				startParameters.Add("\n");
#endif
			sensorMod->setCovariance(VarCovar);
		};

/*
#ifndef FINAL_RELEASE
		Matrix correlationMat( this->nStationPars, this->nStationPars);
		correlationMat.SubMatrix(1, this->nStationPars, 1, this->nStationPars) =this->Qxx.SubMatrix(1,this->nStationPars,1,this->nStationPars);

		
		FILE*out = fopen("correlation.txt","w");

		for (int i=0; i < startParameters.GetSize(); i++)
		{
			fprintf(out,startParameters.GetAt(i)->GetChar());
			fprintf(out,"\n");
		}

		for (int i = 0; i < correlationMat.Nrows(); i++)
		{
			for (int k=0; k < correlationMat.Ncols(); k++)
			{
				fprintf(out,"%8.3lf",correlationMat(i+1,k+1)/(sqrt(correlationMat(i+1,i+1))*sqrt(correlationMat(k+1,k+1))));
			}
			fprintf(out,"\n");
		}
		fclose(out);
		
#endif
*/
	};

	int PointOffset = this->nStationPars;

	
	
	


	for (int i = 0; i < this->getNObjectPoints(); i++)     
	{ 
		CObsPoint *approx = this->ObjectPoints.GetAt(i);
		approx->setCovariance(this->Qxx.SubMatrix(PointOffset + 1, PointOffset + 3, PointOffset + 1, PointOffset + 3));
		PointOffset += 3;
	}
};
	
bool CBundleManager::endIterateOnce(bool forwardOnly, CRobustWeightFunction *robust, 
									bool omitMarked, int MaxIter)
{	
	bool bConverged = false;
	double lastRMS = FLT_MAX;

    for (int iter = 0; (iter <= MaxIter) && (!bConverged); iter++)
	{
		if (this->listener)
		{
			char buffer[100];
			sprintf(buffer,"Iteration %d ...",iter+1);
			this->listener->setTitleString(buffer);
			this->listener->setProgressValue(0.0);
		}

		if (!forwardOnly) solveOnce(robust, omitMarked);
		else solveOnceForward(robust, omitMarked);
		
		if (this->listener)
		{
			this->listener->setProgressValue(100.0);
		}
		double sum = this->rms + lastRMS;
		if (sum < FLT_EPSILON) bConverged = true;
		else
		{
			double diff = (lastRMS - this->rms) / sum;
	    
			lastRMS = this->rms;

			if ((this->rms > 0) && (this->redundancy > 0)) this->rms = ::sqrt(rms/this->redundancy);

			if (protocol)
			{
				protocol->setf(ios::fixed, ios::floatfield);
				protocol->precision(3);
				if (robust)
				{
					*protocol << "\n\nParameters for robust estimation: W(" << robust->getHalfweight()
						      << "), S(" << robust->getSlant() << "), M(" 
							  << robust->getMarkwidth() <<")\nNumber of eliminated observations: " 
							  << this->nGrossErrors << "\n";
				}

				*protocol << "\n\nIteration finished\n==================\n\n   Sigma 0: " 
					      << this->rms << "\n\n==================\n\n" << endl;
			};

			if ( (iter > 0) && (fabs(diff) < this->ConvergenceLimit))
			{
				bConverged = true;
			}
		}
        
	 } //for (int iter = 0; iter < MaxIters; iter++)

	 return  bConverged;
};


bool CBundleManager::solve(bool forwardOnly, bool robust, bool omitMarked)
{
	this->redundancy = getNObs() + getNConstraints() - getNUnknowns();

	int MaxIter = 50;
	int MaxIterRob = 5;

	if ((this->redundancy < 0) || (nPointObs < 1)) return false;

    Try
    {
		if (protocol)
		{
			*protocol << "\n\nStarting iteration process\n==========================\n\n";
		}

		// first end-iterate without robust estimation
        bool bConverged = endIterateOnce(forwardOnly, 0, omitMarked, MaxIter); 

        if ((robust) && (this->nPointObs > 2))
		{   	
		
			double robFac = (worstResiduals.getNormRes(0) + worstResiduals.getNormRes(1)) * 0.5f;
			if ((robFac < this->minRobFac) && (worstResiduals.getNormRes(0) > this->minRobFac)) 
				robFac = (worstResiduals.getNormRes(0) + this->minRobFac) * 0.5f;

			int MaxDel = (int) (float(this->nPointObs) * this->maxRobustPercentage);

			int maxIt = 100;
			int iter = 0;

			CRobustWeightFunction RobustWeight;

			bool rob = false;
			while ((iter < maxIt) && (robFac > this->minRobFac) && (this->nGrossErrors < MaxDel))
			{
				if (!rob && protocol)
					*protocol << "\n\nStarting robust estimation\n==========================\n\n";

				RobustWeight.setParameters(robFac, robFac);
				endIterateOnce(forwardOnly, &RobustWeight, omitMarked, MaxIterRob); 

				robFac = (this->worstResiduals.getNormRes(0) + this->worstResiduals.getNormRes(1)) * 0.5f;
				++iter;
				rob = true;
			}

			if (rob && protocol)
				*protocol << "\n\nComputing final solution without gross errors\n=============================================\n\n";

			bConverged = endIterateOnce(forwardOnly, 0, true, MaxIter); 
		}
	
        if (bConverged)
        {
			this->Qxx = this->Qxx * (this->rms * this->rms);
			updateParameterCovar(forwardOnly);
			applyReduction();

			if (protocol)
			{
				*protocol << "\n\nIteration finished\nTime: " 
					      << CCharString::getTimeString() 
						  << "\n\nAdjustment converged\n====================\n\n";
			}
        }
		else
		{
			if (protocol)
			{
				*protocol << "\n\nIteration finished\nTime: " 
					      << CCharString::getTimeString() 
						  << "\n\nAdjustment did not converge!\n============================\n\n";
			}
		}

        return  bConverged;
    }
	
    CatchAll //(char* error) 
    {
        return false;
    }
};

void CBundleManager::getObjectPoints(CXYZPointArray &points)
{
    points.RemoveAll();

	int pointOffset = this->nStationPars;
    int iPoint = 0;
    
	for (int iPoint = 0; iPoint < this->getNObjectPoints(); iPoint++, pointOffset +=3)
    {
        CObsPoint* point = points.Add();
		CObsPoint* solutionPt = this->ObjectPoints.GetAt(iPoint);

		point->setLabel(solutionPt->getLabel());
		for (int i=0; i < 3; i++)
			(*point)[i] = (*solutionPt)[i];

		Matrix covmat = *(solutionPt->getCovariance());

        point->setCovariance(covmat); 
    }
}

void CBundleManager::updateStationParametersCovar()
{	
	int offset = 0;
	for (int i = 0; i < this->getNStations(); i++)
	{
		CObservationHandler* oh = ObservationHandlers.GetAt(i);
        int nParameters = oh->getNstationParameters();
		if (nParameters > 0) //does only work for RPC and Affine!!!
		{
			Matrix aps = this->Qxx.SubMatrix(offset, offset + nParameters - 1, 1, 1);
			Matrix* apscovmat = oh->getCurrentSensorModel()->getCovariance();
			*apscovmat = this->Qxx.SubMatrix(offset, offset + nParameters - 1, offset, offset + nParameters - 1);

			offset += nParameters;

			// set name to after Bundle
			CLabel nr = "";
			nr += i+1;
			CCharString rpcname("AfterBundle");
			rpcname += nr.GetChar(); 

			oh->getCurrentSensorModel()->setFileName(rpcname.GetChar());
		}
    }
}

void CBundleManager::updateResiduals(bool forwardOnly)
{
	Matrix Delta(this->getNUnknowns(), 1);
	Delta = 0;

	for (int i = 0; i < this->getNStations(); i++)     
	{ 
		CObservationHandler* oh = ObservationHandlers.GetAt(i);
		CSensorModel  *sensorMod = oh->getCurrentSensorModel();
		CObsPointArray *respoints = oh->getResidualPoints();
		
		CObsPointArray *points = oh->getObsPoints();
		int dimension = points->getDimension();

		if (respoints && points && (oh->getNpointObservations() == 2 || oh->getNpointObservations() == 3))
		{
			respoints->RemoveAll();
			int NP = 0;
			CObsPoint sumSq(dimension);

			for (int j = 0; j < points->GetSize(); ++j)
			{
				CObsPoint *obs = points->GetAt(j);
				if (obs->id >= 0)
				{
					CObsPoint *resultPoint = this->ObjectPoints.GetAt(obs->id);
					CCharString pointlabel  = resultPoint->getLabel().GetChar();

					CObsPoint *respoint = respoints->getPoint(pointlabel);

					if (!respoint) 
					{
						respoint = respoints->Add();
						respoint->setLabel(pointlabel.GetChar());
					}
					CObsPoint p(*obs);
					if (!forwardOnly) p = sensorMod->calcRes(Delta,*obs,*resultPoint);
					else 
					{
						sensorMod->transformObj2Obs(p,*resultPoint);
						p -= *obs;
					}

					for (int k=0; k< dimension; k++)
					{
						sumSq[k] += p[k] * p[k];
						(*respoint)[k] = p[k];
						respoint->getCovariance()->element(k,k) = p[k] * p[k] / obs->getCovariance()->element(k,k);
					}
					
					++NP;
					respoint->setStatus(obs->getStatus());
				}
			}
/*			if (NP)
			{
				CCharString pointlabel  = "RMS";

				CObsPoint *respoint = respoints->getPoint(pointlabel);
				if (!respoint)  respoint = respoints->Add();
				for (int k=0; k< dimension; k++)
					(*respoint)[k] = sqrt(sumSq[k] / NP);
				
				respoint->setLabel("RMS");
			}
*/
		}
	}
}

void CBundleManager::setSensorFilenames()
{
	for (int i = 0; i < this->getNStations(); ++i)
	{
		CSensorModel *sensor = this->ObservationHandlers.GetAt(i)->getCurrentSensorModel();
		CFileName filename = sensor->getClassName().c_str();
		if (filename.Find('C') == 0) filename = filename.Right(filename.GetLength() - 1);
		filename += "Bundle";
		CLabel nr = "";
		nr += i+1;
		filename += nr.GetChar();
		sensor->setFileName(filename.GetChar());
	}
};

void CBundleManager::setMatrix(int startRow, int startCol, Matrix &dest, Matrix &src)
{
    for (int r = 0, R = startRow; r < src.Nrows(); r++, R++)
    {
        for (int c = 0, C = startCol; c < src.Ncols(); c++, C++)
        {
			dest.element(R, C) = src.element(r, c);
        }
    }
}

void CBundleManager::print(const Matrix &mat, ofstream &aFile)
{
	for (int i = 0; i < mat.Nrows(); ++i)
	{

		for (int j = 0; j < mat.Ncols(); ++j)
		{
			aFile << mat.element(i,j) << "  ";
		};
		aFile << "\n";
	}
};

void CBundleManager::setReduction(const C3DPoint &red)
{
	for (int i = 0; i < this->ObservationHandlers.GetSize(); i++)
    {
		this->ObservationHandlers.GetAt(i)->getCurrentSensorModel()->setReduction(red);
    }

	this->reduction = red;
};

void CBundleManager::applyReduction()
{ 
	for (int i = 0; i < this->ObservationHandlers.GetSize(); i++)
    {
		this->ObservationHandlers.GetAt(i)->getCurrentSensorModel()->applyReduction();
    }
	this->reduction.x = this->reduction.y = this->reduction.z = 0.0f;
};

void CBundleManager::printObjectPointRecords(int pointwidth, CSensorModel *controlPointModel)
{
	int precision = 3;
	eCoordinateType coordType = eUndefinedCoordinate;

	if (pointwidth < 10)
		pointwidth = 10;

	CObsPoint sig(3);

	if (controlPointModel) 
	{
		precision = controlPointModel->coordPrecision();
		coordType = controlPointModel->getRefSys().getCoordinateType();
	}

	int XYwidth = precision;
	int Zwidth = 3;
	if (coordType == eGEOGRAPHIC)  
	{
		XYwidth += 5;
	}
	else 
	{
		XYwidth += 9;
	}

	if (coordType == eGEOCENTRIC) Zwidth  += 9;
	else Zwidth  += 5;

	int sigmawidth = 6;
	int sigmaprecision = 3;
	

	*protocol << "\n   Object points:\n   ==============\n\n       ";
	protocol->width(pointwidth);
	*protocol << "Point   "; 
	if (coordType == eGEOGRAPHIC) *protocol << "Phi [deg]    ";
	else *protocol << "   X [m]   ";
	*protocol << "Sigma[m]  "; 
	if (coordType == eGEOGRAPHIC) *protocol << "Lambda [deg]  ";
	else *protocol << "    Y [m]   ";
	*protocol << "Sigma[m]";
	if (coordType == eGEOCENTRIC)*protocol <<  "       Z [m]    Sigma[m]"; 
	else *protocol <<  "    Z [m] Sigma[m]"; 

	double sigX = 0.0;
	double sigY = 0.0;
	double sigZ = 0.0;

	for (int index = 0; index < this->ObjectPoints.GetSize(); index++)
	{
		CObsPoint *obj = this->ObjectPoints.GetAt(index);
		
		sig[0] = sqrt(obj->getCovariance()->element(0,0));
		sig[1] = sqrt(obj->getCovariance()->element(1,1));
		sig[2] = sqrt(obj->getCovariance()->element(2,2));

		if (controlPointModel) controlPointModel->convertToDifferentialUnits(sig, sig);

		sigX += sig[0]*sig[0];
		sigY += sig[1]*sig[1];
		sigZ += sig[2]*sig[2];

		protocol->precision(precision);
		*protocol << "\n      ";
		protocol->width(pointwidth); 
		*protocol << obj->getLabel().GetChar() << " ";
		protocol->width(XYwidth); 
		*protocol << (*obj)[0] << " +-";
		protocol->width(sigmawidth); 
		protocol->precision(sigmaprecision);
		*protocol << sig[0] << " ";
		protocol->width(XYwidth); 
		protocol->precision(precision);
		*protocol << (*obj)[1] << " +-";
		protocol->width(sigmawidth); 
		protocol->precision(sigmaprecision);
		*protocol << sig[1] << " ";
		protocol->width(Zwidth); 
		*protocol << (*obj)[2] << " +-";
		protocol->width(sigmawidth); 
		*protocol << sig[2];	
	}

	if (sigX && this->ObjectPoints.GetSize())
	{
		sigX = sqrt(sigX/(this->ObjectPoints.GetSize()));
	}
	if (sigY && this->ObjectPoints.GetSize() > 1)
	{
		sigY = sqrt(sigY/(this->ObjectPoints.GetSize()));
	}
	if (sigZ && this->ObjectPoints.GetSize() > 1)
	{
		sigZ = sqrt(sigZ/(this->ObjectPoints.GetSize()));
	}


	// print the sigma rms
	protocol->precision(precision);
	*protocol << "\n      ";
	protocol->width(pointwidth); 
	*protocol << "RMS SIGMA:" << " ";
	protocol->width(XYwidth); 
	*protocol << "" << " +-";
	protocol->width(sigmawidth); 
	protocol->precision(sigmaprecision);
	*protocol << sigX << " ";
	protocol->width(XYwidth); 
	protocol->precision(precision);
	*protocol << "" << " +-";
	protocol->width(sigmawidth); 
	protocol->precision(sigmaprecision);
	*protocol << sigY << " ";
	protocol->width(Zwidth); 
	*protocol << "" << " +-";
	protocol->width(sigmawidth); 
	*protocol << sigZ;




	*protocol << "\n";
};

void CBundleManager::printMaxResRecord(int index)
{
	int LabelLength = 0;

	for (int i = 0; i < this->ObjectPoints.GetSize(); ++i)
	{
		CXYZPoint *p = ObjectPoints.getAt(i);
		int len = p->getLabel().GetLength();
		if (len > LabelLength) LabelLength = len;
	}

	if (protocol)
	{
		protocol->setf(ios::fixed, ios::floatfield);

		CObservationHandler* oh = ObservationHandlers.GetAt(index);
		int dimension = oh->getDimension();
		CSensorModel *sensorMod = oh->getCurrentSensorModel();
		CCharString name;
		sensorMod->getName(name);
		CCharStringArray obsNames;
		sensorMod->getObservationNames(obsNames);
		int coordprecision = 3;
		int coordwidth = coordprecision + 4;
		CCharString UnitName = sensorMod->diffUnitsString();
		int unitwidth = 27 - UnitName.GetLength();
		CObsPoint p(3);
		p[0] = this->RMS[index].x;
		p[1] = this->RMS[index].y;
		p[2] = this->RMS[index].z;
		sensorMod->convertToDifferentialUnits(p, p);

		
		*protocol << "\nStation (";
		protocol->width(3); 
		*protocol << (index + 1) << "): " << name << ", " << this->NPointsX[index]
			      << " / " << this->NPointsY[index];
		if (dimension >=3)  *protocol << " / " << this->NPointsZ[index];
		*protocol << " points in X / Y";
		if (dimension >=3)  *protocol << " / Z";

		*protocol << "\n   RMS of residuals in " << *obsNames.GetAt(0) << " / " << *obsNames.GetAt(1);
		if (dimension >= 3) *protocol << " / " << *obsNames.GetAt(2);
		*protocol << " " << UnitName.GetChar();
		protocol->width(unitwidth - 4);
		*protocol << " ";

		if (dimension < 3) *protocol << "    ";

		protocol->precision(coordprecision);
		protocol->width(LabelLength + 1);
		*protocol << " ";
		protocol->width(coordwidth);
		*protocol << p[0] << " / ";
		protocol->width(LabelLength + 1);
		*protocol << " ";
		protocol->width(coordwidth);
		*protocol << p[1];
				
		if (dimension >=3)
		{
			*protocol << " / ";
			protocol->width(LabelLength+ 1);
			*protocol << " ";
			protocol->width(7);
			protocol->precision(3);
			*protocol << p[2];
		}		

		p[0] = maxResX[index];
		p[1] = maxResY[index];
		p[2] = maxResZ[index];
		sensorMod->convertToDifferentialUnits(p, p);

		*protocol << "\n   Max residual in " << *obsNames.GetAt(0) << " / " << *obsNames.GetAt(1);
		if (dimension >= 3) *protocol << " / " << *obsNames.GetAt(2);
		*protocol << " " << UnitName.GetChar();
		protocol->width(unitwidth);
		*protocol << " ";

		if (dimension < 3) *protocol << "    ";

		protocol->precision(coordprecision);
		protocol->width(LabelLength);
		*protocol << maxResLabelX[index].GetChar() << " ";
		protocol->width(coordwidth);
		*protocol << p[0] << " / ";
		protocol->width(LabelLength);
		*protocol << maxResLabelY[index].GetChar() << " ";
		protocol->width(coordwidth);
		*protocol << p[1];
				
		if (dimension >=3)
		{
			*protocol << " / ";
			protocol->width(LabelLength);
			*protocol << maxResLabelZ[index].GetChar() << " ";
			protocol->width(7);
			protocol->precision(3);
			*protocol << p[2];
		}
			
		protocol->precision(3);
		*protocol << "\n   Max residual in " << *obsNames.GetAt(0) << " / " << *obsNames.GetAt(1);
		if (dimension >= 3) *protocol << " / " << *obsNames.GetAt(2);
		*protocol << " [units of sigma a priori]: ";
		if (dimension < 3) *protocol << "    ";

		protocol->width(LabelLength);
		*protocol << maxRobLabelX[index].GetChar() << " ";
		protocol->width(coordwidth);
		*protocol << maxRobX[index] << " / ";
		protocol->width(LabelLength);
		*protocol << maxRobLabelY[index].GetChar() << " ";
		protocol->width(coordwidth);
		*protocol << maxRobY[index];
				
		if (dimension >=3)
		{
			*protocol << " / ";
			protocol->width(LabelLength);
			*protocol << maxRobLabelZ[index].GetChar() << " ";
			protocol->width(7);
			protocol->precision(3);
			*protocol << maxRobZ[index];
		};
	
		*protocol << endl;
	}
};

void CBundleManager::printAllResiduals(int labellength)
{
	if (this->protocol)
	{
		*protocol << "\n\nL i s t   o f   a l l   r e s i d u a l s :\n===========================================";

		for (int i = 0; i < this->getNStations(); i++)     
		{ 
			CObservationHandler* oh = ObservationHandlers.GetAt(i);
			CSensorModel  *sensorMod = oh->getCurrentSensorModel();
			CObsPointArray *obspoints = oh->getObsPoints();

			if (obspoints)
			{			
				CCharStringArray obsNames;
				sensorMod->getObservationNames(obsNames);
				CCharString UnitName = sensorMod->diffUnitsString();
				int unitwidth = 9 - UnitName.GetLength();
			
				*protocol << "\n\nStation (" ;
				protocol->width(3);
				*protocol << (i + 1) << "):\n=============\n\n";
				protocol->width(labellength);
				*protocol << "Point" << "     res_" << obsNames.GetAt(0)->GetChar() 
					      << "      res_" << obsNames.GetAt(1)->GetChar();
				if (oh->getDimension() >= 3) *protocol << "      res_" << obsNames.GetAt(2)->GetChar();
				
				*protocol << "  " << UnitName;

				for (int j = 0; j < obspoints->GetSize(); ++j)
				{
					CObsPoint *obs = obspoints->GetAt(j);
					CObsPoint p(*obs);
					
					if (oh->hasUnknownObjPoints())
					{
						if (obs->id >= 0)
						{
							CObsPoint *resultPoint = this->ObjectPoints.GetAt(obs->id);
							sensorMod->transformObj2Obs(p,*resultPoint);
							p -= *obs;	
						}
					}
					else
					{
						CSplineModel* spline = (CSplineModel*)sensorMod;
						if (!spline->Fi(p,*(CXYZOrbitPoint*)obs))
							continue;
					}

					if (!oh->hasUnknownObjPoints() || obs->id >=0)
					{
						sensorMod->convertToDifferentialUnits(p, p);
						*protocol << "\n";
						protocol->width(labellength);
						protocol->precision(3);
						*protocol << obs->getLabel().GetChar() << " ";
						protocol->width(9);
						*protocol << p[0];
						if (obs->getStatusBit(ROBUSTBITBASE + 0)) *protocol << "*";
						else *protocol << " ";
						protocol->width(9);
						*protocol << p[1];
						if (obs->getStatusBit(ROBUSTBITBASE + 1)) *protocol << "*";
						else *protocol << " ";

						if (oh->getDimension() >= 3)
						{
							protocol->width(9);
							*protocol << p[2];
							if (obs->getStatusBit(ROBUSTBITBASE + 2)) *protocol << "*";
							else *protocol << " ";
						}
					}
				}
			}
		}
	}
};

void CBundleManager::printResults()
{
	CSensorModel *controlPointModel = 0;

	int LabelLength = 0;

	for (int i = 0; i < this->ObjectPoints.GetSize(); ++i)
	{
		CXYZPoint *p = ObjectPoints.getAt(i);
		int len = p->getLabel().GetLength();
		if (len > LabelLength) LabelLength = len;
	}

	if (this->protocol)
	{
		*protocol << "Bundle Adjustment\n=================\n\nDate: " << CCharString::getDateString()
			      << "\nTime: " << CCharString::getTimeString() << "\n\n";
		
	
		*protocol << "\nStatistics if observations / unknowns:\n======================================\n\nNumber of image / object point observations: ";
		protocol->width(7);
		*protocol << this->nPointObs           << "\nNumber of direct observations:               ";
		protocol->width(7);
		*protocol << this->nDirectObs          << "\nNumber of unknown station parameters:        ";
		protocol->width(7);
		*protocol << this->nStationPars        << "\nNumber of unknown 3D Points:                 ";
		protocol->width(7);
		*protocol << this->getNObjectPoints();
		
		if (this->hasQualityData)
		{
			*protocol << "\nNumber of 3D Points used as check points:    ";
			protocol->width(7);
			*protocol<< this->nCheckPointsUsed;
		}
		*protocol  << "\n\nTotal number of observations:                ";
		protocol->width(7);
		*protocol << this->getNObs()           << "\nTotal number of unknowns:                    ";
		protocol->width(7);
		*protocol << this->getNUnknowns()      << "\nNumber of constraints:                       ";
		protocol->width(7);
		*protocol << this->getNConstraints()   << "\nRedundancy:                                  ";
		protocol->width(7);
		*protocol << (getNObs() + this->getNConstraints() - getNUnknowns());
		if (this->nGrossErrors > 0)
		{
			*protocol  << "\nNumber of gross errors:                      ";
			protocol->width(7);
			*protocol << this->nGrossErrors;
		}
		*protocol << "\n====================================================\n\n\nR e s u l t s   o f   A d j u s t m e n t\n=========================================\n\n\nList of Stations:\n=================\n\n"
				  << "Station  Model                      Points    RMSX     RMSY     RMSZ    Name";
		
		protocol->setf(ios::fixed, ios::floatfield);
		protocol->precision(3);
	
		for (int i = 0; i < this->getNStations(); ++i)
		{
			CObservationHandler* oh = ObservationHandlers.GetAt(i);
			CObsPoint p(3);
			p[0] = RMS[i].x;
			p[1] = RMS[i].y;
			p[2] = RMS[i].z;
			oh->getCurrentSensorModel()->convertToDifferentialUnits(p, p);
			if (oh->isControl()) controlPointModel = oh->getCurrentSensorModel();

			*protocol << "\n(";
			protocol->width(3);
			*protocol << (i + 1) << ")    "; 
			CCharString Cnam;
			oh->getCurrentSensorModel()->getName(Cnam);
			int len = 25 - Cnam.GetLength();
			if (len < 1) len = 1;
			*protocol << Cnam;
			protocol->width(len);
			*protocol << " ";
			protocol->width(8);
			*protocol << NPointsX[i] << " ";;
			protocol->width(8);
			*protocol << p[0]  << " ";;
			protocol->width(8);
			*protocol << p[1] << " ";;
			protocol->width(8);
			if (oh->getDimension() >= 3)*protocol << p[2];
			else *protocol << " ";

			*protocol << "   " << oh->getFileNameChar();
		}

		*protocol << "\n\n\nRMS error of the weight unit: Sigma0 = +-";
		protocol->precision(3); 		
		protocol->width(7); 
		*protocol << this->rms << "\n================================================";

		this->printQualityAssessmentData();
		
		*protocol << "\n\n   Station parameters:\n   ===================";
		
		for (int i = 0; i < this->ObservationHandlers.GetSize(); i++)
		{
			CSensorModel *sensorMod = this->ObservationHandlers.GetAt(i)->getCurrentSensorModel();
			if (sensorMod->getNumberOfStationPars())
			{
				CRotationMatrix fixedECRMatrix;
							
				CCharString name;
				sensorMod->getName(name);
				*protocol << "\n\n      Station (";
			    protocol->width(3);
				*protocol << (i + 1) << "): " << name;
				CCharString sensorModParameter;
				if (this->determineFixedRotMatForOrbit(fixedECRMatrix,*sensorMod->getFileName()))
					sensorMod->printStationParameter(sensorModParameter,&fixedECRMatrix);
				else
					sensorMod->printStationParameter(sensorModParameter);
				*protocol << sensorModParameter.GetChar();
			}
		}

		*protocol << "\n\n";
		printObjectPointRecords(LabelLength, controlPointModel);
	  
		*protocol << "\n\nStatistics of residuals:\n========================\n";
		for (int i = 0; i < this->getNStations(); ++i) 
		{
			this->printMaxResRecord(i);
		}

		printEliminatedObservations(LabelLength);

		*protocol  << "\n\n" << this->worstResiduals.getString(LabelLength) << "\n\n";

		printAllResiduals(LabelLength);

		if (controlPointModel)
		{
			if (controlPointModel->getRefSys().getCoordinateType() == eGEOGRAPHIC)
			{
				*protocol << "\n\nEarth radius for computing metric residuals [m]: ";
				protocol->precision(3);
				double R = (controlPointModel->getRefSys().getSpheroid().getSemiMajor() + 
					        controlPointModel->getRefSys().getSpheroid().getSemiMinor()) * 0.5f;
				*protocol << R << "\n";
			}
		}

		*protocol  << endl;
	}
};

	  
void CBundleManager::printEliminatedObservations(int labelLength)
{
	if (protocol && this->nGrossErrors)
	{
		*protocol << "\n\n\nList of gross errors eliminated by robust estimation:"
			      << "\n=====================================================\n";

		for (int iStat = 0; iStat < this->getNStations(); iStat++)
		{
			CObservationHandler* oh = ObservationHandlers.GetAt(iStat);
			CSensorModel *sensorMod = oh->getCurrentSensorModel();

			if (oh->getObsPoints())  
			{
				CCharStringArray obsNames;
				sensorMod->getObservationNames(obsNames);
				
				bool firstError = false;
				CObsPointArray *obsPoints = oh->getObsPoints();

				for (int iPnt = 0; iPnt < obsPoints->GetSize(); ++iPnt)
				{
					CObsPoint *obs = obsPoints->GetAt(iPnt);

					if (obs->id >= 0 ||!oh->hasUnknownObjPoints() )
					{
						bool hasError = false;
						ostrstream oss;
				
						for (int coord = 0; coord < obs->getDimension(); ++coord)
						{						
							if (obs->getStatusBit(ROBUSTBITBASE + coord)) 
							{
								if (hasError) oss << " / ";
								oss << obsNames.GetAt(coord)->GetChar();
								hasError = true;
								if (!firstError)
								{
									*protocol << "\nStation (";
									protocol->width(3);
									*protocol << (iStat + 1) << "): " << oh->getFileNameChar();
								}
								firstError = true;
							}
						}
					
						if (hasError)
						{
							
							*protocol << "\n  ";
							protocol->width(labelLength);
							*protocol << obs->getLabel().GetChar() << " ";
							oss << ends;
							char *z = oss.str();
							*protocol << z;
							delete[] z;
						}
					}
				}
			}
		}

		*protocol << endl;
	}
};


void CBundleManager::setQualityAssessmentData(int nCheckPointsUsed, vector<C3DPoint> maxResCheckPoints , vector<C3DPoint> rmsCheckPoints, vector<int> nStationCheckPoints,CReferenceSystem refSysCheckPoints)
{
	if (nCheckPointsUsed && 
		maxResCheckPoints.size() && 
		rmsCheckPoints.size() == maxResCheckPoints.size() && 
		nStationCheckPoints.size() == maxResCheckPoints.size() &&
		int(maxResCheckPoints.size()) <= this->getNStations())
	{
		this->hasQualityData = true;
		this->nCheckPointsUsed = nCheckPointsUsed;
		this->maxResCheckPoints = maxResCheckPoints;
		this->rmsCheckPoints = rmsCheckPoints;
		this->nStationCheckPoints = nStationCheckPoints;
		this->refSysCheckPoints = refSysCheckPoints;
	}
	else
		this->resetQualityAssessmentData();
}

void CBundleManager::resetQualityAssessmentData()
{
		this->hasQualityData = false;
		this->nCheckPointsUsed = 0;
		this->maxResCheckPoints.clear();
		this->rmsCheckPoints.clear();
		this->nStationCheckPoints.clear();
}

void CBundleManager::printQualityAssessmentData()
{
	if (this->hasQualityData)
	{
		CCharString coordinateType;
		int precision=0;
		int length=0;

		if (this->refSysCheckPoints.getCoordinateType() == eGEOCENTRIC)
		{
			coordinateType = "Geocentric coordinates";
			precision = 3;
			length = 8;
		}
		else if (this->refSysCheckPoints.getCoordinateType() == eGEOGRAPHIC)
		{
			coordinateType = "Geographic coordinates";
			precision = 6;
			length = 11;
		}
		else if (this->refSysCheckPoints.getCoordinateType() == eGRID)
		{
			coordinateType = "Grid coordinates";
			precision = 3;
			length = 8;
		}
		else 
		{
			coordinateType = "Undefined coordinates";
			precision = 3;
			length = 8;
		}

		*protocol << "\n\nQuality Assessment:\n===================\n\n"
			      << "Coordinate type of 3D Check Points: " << coordinateType << "\n";



		if (this->refSysCheckPoints.getCoordinateType() == eGEOGRAPHIC)
			*protocol << "Station  Model                      Check Points    RMSX/MaxResX           RMSY/MaxResY           RMSZ/MaxResZ";
		else
			*protocol << "Station  Model                      Check Points    RMSX/MaxResX     RMSY/MaxResY     RMSZ/MaxResZ";
		
		protocol->setf(ios::fixed, ios::floatfield);
		protocol->precision(3);
	
		for (unsigned int i = 0; i < this->rmsCheckPoints.size() ; ++i)
		{
			CObservationHandler* oh = ObservationHandlers.GetAt(i);
			C3DPoint rmsValues = this->rmsCheckPoints[i];
			C3DPoint maxResValues = this->maxResCheckPoints[i];
			
			if (!this->nStationCheckPoints[i])
				continue;

			protocol->precision(3);
			if (i == this->rmsCheckPoints.size() -1 )
				protocol->precision(precision);

			*protocol << "\n(";
			protocol->width(3);
			*protocol << (i + 1) << ")    "; 
			CCharString Cnam;
			oh->getCurrentSensorModel()->getName(Cnam);
			int len = 25 - Cnam.GetLength();
			if (len < 1) len = 1;
			*protocol << Cnam;
			protocol->width(len);
			*protocol << " ";
			protocol->width(8);
			*protocol << this->nStationCheckPoints[i] << "        ";
			protocol->width(8);
			*protocol << rmsValues.x  << "/" << maxResValues.x << "   ";
			protocol->width(length);
			*protocol << rmsValues.y  << "/" << maxResValues.y << "   ";
			protocol->width(length);
			if (oh->getDimension() >= 3)*protocol << rmsValues.z  << "/" << maxResValues.z;
			else *protocol << " ";
		}


	}
}