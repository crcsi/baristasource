#include "SplineSegmentArray.h"

CSplineSegmentArray::CSplineSegmentArray(int nGrowBy ):
 CMUObjectArray<CSplineSegment, CSplineSegmentPtrArray>(nGrowBy)
{
}


CSplineSegmentArray::~CSplineSegmentArray(void)
{
}



void CSplineSegmentArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);


	CStructuredFileSection* pNewSection; 

	for (int i=0; i< this->GetSize(); i++)
	{
		CSplineSegment* point = this->GetAt(i);
		pNewSection = pStructuredFileSection->addChild();
		point->serializeStore(pNewSection);
	}

}

void CSplineSegmentArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("CSplineSegment"))
		{
			CSplineSegment* point = this->Add();
			point->serializeRestore(pChild);
		}	
	}
}

bool CSplineSegmentArray::isModified()
{
	return this->bModified;
}

void CSplineSegmentArray::reconnectPointers()
{
	CSerializable::reconnectPointers();
	for (int i=0;i< this->GetSize();i++)
	{
		CSplineSegment* pPointArray = this->GetAt(i);
		pPointArray->reconnectPointers();
	}

}


CSplineSegment* CSplineSegmentArray::Add()
{
	CSplineSegment* pSegment = CMUObjectArray<CSplineSegment, CSplineSegmentPtrArray>::Add();
	pSegment->setParent(this);
	return pSegment;
}

CSplineSegment* CSplineSegmentArray::Add(const CSplineSegment& point)
{
	CSplineSegment* pSegment = CMUObjectArray<CSplineSegment, CSplineSegmentPtrArray>::Add(point);
	pSegment->setParent(this);
	return pSegment;
}


void CSplineSegmentArray::resetPointers()
{
	CSerializable::resetPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		CSplineSegment* pPointArray = this->GetAt(i);
		pPointArray->resetPointers();
	}
}
