#include "DistortionFactory.h"
#include "DistortionALOS.h"
#include "DistortionTHEOS.h"

CDistortion* CDistortionFactory::createDistortion(DistortionType type)
{
	switch (type)
	{
	case eDistortionALOS	: return new CDistortionALOS();
	case eDistortionTHEOS	: return new CDistortionTHEOS();
	case eNotInitialized	: return NULL;
	default					: return NULL;
	}
}


CDistortion* CDistortionFactory::createDistortion(CDistortion* d)
{
	if (d)
	{
		switch (d->getDistortionType())
		{
		case eDistortionALOS	: return new CDistortionALOS(*(CDistortionALOS*)d);
		case eDistortionTHEOS	: return new CDistortionTHEOS(*(CDistortionTHEOS*)d);
		case eNotInitialized	: return NULL;
		default					: return NULL;
		}
	}
	return NULL;
}

void CDistortionFactory::copyDistortion(CDistortion* dest,const CDistortion* src)
{
	if (src)
	{
		switch (src->getDistortionType())
		{
		case eDistortionALOS	: (*(CDistortionALOS*)dest) = *(CDistortionALOS*)src;
		case eDistortionTHEOS	: (*(CDistortionTHEOS*)dest) = *(CDistortionTHEOS*)src;

		default					: return ;
		}
	}

}