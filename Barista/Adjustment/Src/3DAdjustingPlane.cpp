/**
 * @class C3DAdjustingPlane
 * C3DAdjustingPlane handles observations for 3D Points situated on a plane  
 * in object space
 */
#include <float.h>
#include "newmat.h"
#include "3DAdjustingPlane.h"

#include "XYZPoint.h"
#include "RotationBase.h"
#include "TextFile.h"
#include "AdjustMatrix.h"
#include "2DPoint.h"
#include "XYPoint.h"
/**
 * contructor from existing C3DAdjustingPlane
 * @param affine
 */

C3DAdjustingPlane::C3DAdjustingPlane() : 
				CSensorModel(false,"3DAdjustingPlane", 3, 3, 0, 0, 0, 0, 3, 3, 1),
				pERP(0), pROT(0), eEquationType(eUPlaneEquation), sigma(1.0), 
				parametrisation(0), denominator(1.0)
{
	for (int i = 0; i < 4; ++i) this->coefficients[i] = 0;
	for (int i = 0; i < 3; ++i) this->mirrorCoeffs[i] = 1;
	this->coefficients[eEquationType] = 1;
	setupPars();
};
C3DAdjustingPlane::C3DAdjustingPlane(const C3DAdjustingPlane &plane) : CSensorModel(plane),
				pERP(plane.pERP), pROT(plane.pROT), eEquationType(plane.eEquationType), 
				sigma(plane.sigma), parametrisation(plane.parametrisation),
				denominator(plane.denominator)

{
	for (int i = 0; i < 4; ++i) this->coefficients[i] = plane.coefficients[i];
	for (int i = 0; i < 3; ++i) this->mirrorCoeffs[i] = plane.mirrorCoeffs[i];

}

C3DAdjustingPlane::C3DAdjustingPlane(CXYZPoint *erp, CRotationBase *rot, ePlaneEquationType type, 
									 int Parameterisation, double Sigma, 
									 int MirrorType) : 
				CSensorModel(false,"3DAdjustingPlane", 3, 3, 0, 0, 0, 0, 3, 3, 1),
				pERP(erp), pROT(rot), eEquationType(type), sigma(Sigma), parametrisation(Parameterisation),
				denominator(1.0)
{
	for (int i = 0; i < 4; ++i) this->coefficients[i] = 0;

	if (MirrorType & UMirror) this->mirrorCoeffs[0] = -1.0;
	else this->mirrorCoeffs[0] = 1.0;
	if (MirrorType & VMirror) this->mirrorCoeffs[1] = -1.0;
	else this->mirrorCoeffs[1] = 1.0;
	if (MirrorType & WMirror) this->mirrorCoeffs[2] = -1.0;
	else this->mirrorCoeffs[2] = 1.0;

	this->coefficients[eEquationType] = 1;
	setupPars();
};

C3DAdjustingPlane::~C3DAdjustingPlane()
{
}

void C3DAdjustingPlane::init(CXYZPoint *erp, CRotationBase *rot, ePlaneEquationType type, 
							 int Parameterisation, double Sigma, int MirrorType)
{

	this->pERP = erp;
	this->pROT = rot;
	this-> eEquationType = type;
	this->sigma = Sigma;
	this->parametrisation = Parameterisation;
	this->denominator = 1.0;

	for (int i = 0; i < 4; ++i) this->coefficients[i] = 0;

	if (MirrorType & UMirror) this->mirrorCoeffs[0] = -1.0;
	else this->mirrorCoeffs[0] = 1.0;
	if (MirrorType & VMirror) this->mirrorCoeffs[1] = -1.0;
	else this->mirrorCoeffs[1] = 1.0;
	if (MirrorType & WMirror) this->mirrorCoeffs[2] = -1.0;
	else this->mirrorCoeffs[2] = 1.0;

	this->coefficients[eEquationType] = 1;
	setupPars();
}
	  
void C3DAdjustingPlane::setupPars()
{	
	this->activeParameter[addPar] = 0;

	if (this->eEquationType == eUPlaneEquation) 
	{
		if (this->parametrisation & this->linUCoeff)  this->parametrisation -= this->linUCoeff;
	}
	else if (this->eEquationType == eVPlaneEquation)
	{
		if (this->parametrisation & this->linVCoeff)  this->parametrisation -= this->linVCoeff;
	}
	else
	{
		if (this->parametrisation & this->linWCoeff)  this->parametrisation -= this->linWCoeff;
	}

	if (this->parametrisation & this->constCoeff) this->activeParameter[addPar]++;
	if (this->parametrisation & this->linUCoeff)  this->activeParameter[addPar]++;
	if (this->parametrisation & this->linVCoeff)  this->activeParameter[addPar]++;
	if (this->parametrisation & this->linWCoeff)  this->activeParameter[addPar]++;
}

C3DAdjustingPlane &C3DAdjustingPlane::operator =(const C3DAdjustingPlane &plane)
{
	if (this != &plane)
	{
		this->initFromModel(plane);

		this->pERP  = plane.pERP;
		this->pROT  = plane.pROT;
		this->sigma = plane.sigma;
		this->eEquationType   = plane.eEquationType;
		this->parametrisation = plane.parametrisation;
		this->denominator     = plane.denominator;
	
		for (int i = 0; i < 3; ++i) this->mirrorCoeffs[i] = plane.mirrorCoeffs[i];
		for (int i = 0; i < 4; ++i) this->coefficients[i] = plane.coefficients[i];
	}

	return *this;
};

/**
 * Sets affine parameter from Matrix
 * @param parms
 */
void C3DAdjustingPlane::setAll(const Matrix &parms)
{
    this->hasValues = true;

	this->denominator = 0.0;
	this->coefficients[0] = parms.element(0, 0);

	for (int index = 1; index < 4; ++index)
	{
		this->coefficients[index] = parms.element(index, 0);
		this->denominator += this->coefficients[index] * this->coefficients[index];
	}

	this->denominator = sqrt(1.0 / this->denominator);

   this->informListeners_elementsChanged();
}

    /**
     * Transforms an xyz point to its cooresponding xy point
     * in image space
     * The xyz point is assumed to be in the same coordinate system
     * as the used Affine parameters.
     *
     * @param xyz The xyz point
     * @return the transformed xy point
     */
void C3DAdjustingPlane::transformObj2Obs(C2DPoint &obs, const C3DPoint &obj) const
{
	C3DPoint P = obj - *this->pERP;
	this->pROT->getRotMat().RT_times_x(P, P);

	double d = 0.0;

	for (int i = 0; i < 3; ++i)
	{
		P[i] *= this->mirrorCoeffs[i];
		d += P[i] * this->coefficients[i + 1];
	}

	d += this->coefficients[0];
	d *= this->denominator;
	
    obs.x = d;
    obs.y = 0;
}
void C3DAdjustingPlane::transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const
{
	C3DPoint P = obj - *this->pERP;
	this->pROT->getRotMat().RT_times_x(P, P);

	double d = 0.0;

	for (int i = 0; i < 3; ++i)
	{
		P[i] *= this->mirrorCoeffs[i];
		d += P[i] * this->coefficients[i + 1];
	}

	d += this->coefficients[0];
	d *= this->denominator;
	
	obs = P;
	obs[eEquationType - 1] = d;
}

bool C3DAdjustingPlane::transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const
{
	C3DPoint P;

	for (int i = 0, index = 0; i < 3; ++i)
	{
		if (i != eEquationType)
		{
			P[i] = obs[index];
			index++;
		}
		else P[i] = Height;

		P[i] *= this->mirrorCoeffs[i];
	}

	this->pROT->getRotMat().R_times_x(P, P);

	P += *this->pERP;

    return true;
}

bool C3DAdjustingPlane::transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const
{
	C3DPoint P(obs);

	for (int i = 0, index = 0; i < 3; ++i)
	{
		P[i] *= this->mirrorCoeffs[i];
	}

	this->pROT->getRotMat().R_times_x(P, P);

	P += *this->pERP;

    return true;
}
   
bool C3DAdjustingPlane::transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const
{
	obj.setLabel(obs.getLabel());

	C3DPoint P(obs[0], obs[1], obs[2]);

	for (int i = 0, index = 0; i < 3; ++i)
	{
		P[i] *= this->mirrorCoeffs[i];
	}

	this->pROT->getRotMat().R_times_x(P, P);

	P += *this->pERP;

    return true;
};

bool C3DAdjustingPlane::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const
{
	// dummy implementation

	return true;
};

bool C3DAdjustingPlane::transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const
{
	// dummy implementation

	return true;
};

void C3DAdjustingPlane::setAdjustableValues(const Matrix &parms)
{
    this->hasValues = true;

	int index = 0;
	
	if (this->parametrisation & this->constCoeff) 
	{
		this->coefficients[0] = parms.element(index, 0);
		++index;
	};

	if (this->parametrisation & this->linUCoeff)  
	{
		this->coefficients[1] = parms.element(index, 0);
		++index;
	};

	if (this->parametrisation & this->linVCoeff)  
	{
		this->coefficients[2] = parms.element(index, 0);
		++index;
	};
	if (this->parametrisation & this->linWCoeff)  
	{
		this->coefficients[3] = parms.element(index, 0);
		++index;
	};

	this->denominator = 0.0;
	for (int i = 1; i < 4; ++i) this->denominator += this->coefficients[i] * this->coefficients[i];
	this->denominator = sqrt(1.0 / this->denominator);

    this->informListeners_elementsChanged();
}

/**
* Gets a matrix of the parameters
*
* @return the parameters backed into a matrix
*/

bool C3DAdjustingPlane::getAllParameters(Matrix &parms) const
{
	parms.ReSize( 4, 1);

    for (int i = 0; i < 4; ++i) parms.element(i, 0) = this->coefficients[i];

    return true;
};

bool C3DAdjustingPlane::getAdjustableParameters(Matrix &parms) const
{
	parms.ReSize(this->activeParameter[addPar], 1);

	int index = 0;
	if (this->parametrisation & this->constCoeff) 
	{
		parms.element(index, 0) = this->coefficients[0];
		++index;
	};

	if (this->parametrisation & this->linUCoeff)  
	{
		parms.element(index, 0) = this->coefficients[1];
		++index;
	};

	if (this->parametrisation & this->linVCoeff)  
	{
		parms.element(index, 0) = this->coefficients[2];
		++index;
	};
	if (this->parametrisation & this->linWCoeff)  
	{
		parms.element(index, 0) = this->coefficients[3];
		++index;
	};

	return true;
};

bool C3DAdjustingPlane::insertParametersForAdjustment(Matrix &parms) 
{
	if (this->parameterIndex[addPar] >= 0)
	{
		int index = this->parameterIndex[addPar];
		if (this->parametrisation & this->constCoeff) 
		{
			parms.element(index, 0) = this->coefficients[0];
			++index;
		};

		if (this->parametrisation & this->linUCoeff)  
		{
			parms.element(index, 0) = this->coefficients[1];
			++index;
		};

		if (this->parametrisation & this->linVCoeff)  
		{
			parms.element(index, 0) = this->coefficients[2];
			++index;
		};
		if (this->parametrisation & this->linWCoeff)  
		{
			parms.element(index, 0) = this->coefficients[3];
			++index;
		};
	}
	return true;
};

bool C3DAdjustingPlane::setParametersFromAdjustment(const Matrix &parms) 
{
	if (this->parameterIndex[addPar] >= 0)
	{
		this->hasValues = true;

		int index = this->parameterIndex[addPar];
		
		if (this->parametrisation & this->constCoeff) 
		{
			this->coefficients[0] = parms.element(index, 0);
			++index;
		};

		if (this->parametrisation & this->linUCoeff)  
		{
			this->coefficients[1] = parms.element(index, 0);
			++index;
		};

		if (this->parametrisation & this->linVCoeff)  
		{
			this->coefficients[2] = parms.element(index, 0);
			++index;
		};
		if (this->parametrisation & this->linWCoeff)  
		{
			this->coefficients[3] = parms.element(index, 0);
			++index;
		};

		this->denominator = 0.0;
		for (int i = 1; i < 4; ++i) this->denominator += this->coefficients[i] * this->coefficients[i];
		this->denominator = sqrt(1.0 / this->denominator);
	}

	return true;
}

Matrix C3DAdjustingPlane::getConstants() const
{
	Matrix parms( 0, 0);
    return parms;
};

/**
 * freeAffine modifies the covariance matrix of the affine parameter
 * setting the weight to "free"
 */
void C3DAdjustingPlane::freeAffine() //Covariance = Matrix.identity(8, 8).times(100000000);
{
    this->covariance.ReSize(8,8);
    this->covariance = 0;

    for (int i = 0; i < 8; i++)
        this->covariance.element(i, i) = 100000000;
}


/**
 * resetSensor modifies the covariance matrix of the additional parameter
 * setting the weight to "free", by calling freeAffine and sets affine
 * parameters to 0
 */
void C3DAdjustingPlane::resetSensor()
{
	for (int i = 0; i < 4; ++i) this->coefficients[i] = 0;
	this->coefficients[eEquationType] = 1.0;
	this->denominator = 1.0;
	this->hasValues = false;
}


/**
 * reads affine parameter file (text format and sets affine values 
 * @param  char data
 * @returns true/false
 */
bool C3DAdjustingPlane::read(const char* filename)
{  
	bool ret = true;
    CCharString str;

    CTextFile file(filename, CTextFile::READ);

    int i = 0;

    Matrix parameters(4, 1);
    for (;;)
    {
        if (!file.ReadLine(str))
		{
			ret = false;
			break;
		}

        const char* string = str.GetChar();
        char buf[1024];
        double value;
        
        ::sscanf(string, "%s%lf", buf, &value);
        parameters.element(i, 0) = value;
        i++;
    }

    this->setAll(parameters);
    this->FileName = filename;
	return ret;
}



/**
 * writes affine parameter file as space separated file 
 * @param  filename
 * @returns true/false
 */
bool C3DAdjustingPlane::writeTextFile(const char* filename) const
{
    FILE	*fp;
	fp = fopen(filename,"w");

	for (int i = 0; i < 4; ++i) ::fprintf(fp,"a1 %.9f\n", this->coefficients[i]);

	fclose(fp);

	return true;
}

/**
 * Serialized storage of a CRPC to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void C3DAdjustingPlane::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("PlaneFilename", this->getFileName()->GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("hasValues", this->gethasValues());

    token = pStructuredFileSection->addToken();
	token->setValue("bIsCurrentSensorModel", this->bIsCurrentSensorModel);

    token = pStructuredFileSection->addToken();
    token->setValue("a0", this->coefficients[0]);

    token = pStructuredFileSection->addToken();
    token->setValue("a1", this->coefficients[1]);

    token = pStructuredFileSection->addToken();
    token->setValue("a2", this->coefficients[2]);

    token = pStructuredFileSection->addToken();
    token->setValue("a3", this->coefficients[3]);
 
 	CCharString valueStr("");
	CCharString elementStr("");

	token = pStructuredFileSection->addToken();
	for (int i=0; i< this->nActualParameter[addPar]; i++)
	{
		for (int k=0; k< this->nActualParameter[addPar]; k++)
		{
			double element = this->covariance.element(i,k);
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}
	}

	token->setValue("Planecovariance", valueStr.GetChar());	

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->refSystem.serializeStore(child);
}

/**
 * Restores a C3DAdjustingPlane from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void C3DAdjustingPlane::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("PlaneFilename"))
            this->setFileName( token->getValue());
		else if(key.CompareNoCase("hasValues"))
            this->hasValues = token->getBoolValue();
		else if(key.CompareNoCase("bIsCurrentSensorModel"))
			this->bIsCurrentSensorModel = token->getBoolValue();
		else if(key.CompareNoCase("a0"))
            this->coefficients[0] = token->getDoubleValue();
		else if(key.CompareNoCase("a1"))
            this->coefficients[1] = token->getDoubleValue();
		else if(key.CompareNoCase("a2"))
            this->coefficients[2] = token->getDoubleValue();
		else if(key.CompareNoCase("a3"))
            this->coefficients[3] = token->getDoubleValue();
		else if (key.CompareNoCase("Planecovariance"))
		{
			double* tmpArray = new double [this->nActualParameter[addPar]*this->nActualParameter[addPar]];
			token->getDoubleValues(tmpArray,this->nActualParameter[addPar]*this->nActualParameter[addPar]);
			
			for (int i=0; i< this->nActualParameter[addPar]; i++)
			{
				for (int k=0; k< this->nActualParameter[addPar]; k++)
				{
					this->covariance.element(i,k) = tmpArray[i*this->nActualParameter[addPar]+k];
				}	 
			}

			if (tmpArray) delete [] tmpArray;
		}
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refSystem.serializeRestore(child);
        }
    }

}

/**
 * Gets ClassName
 * returns a CCharString
 */
string C3DAdjustingPlane::getClassName() const
{
    return string( "C3DAdjustingPlane");
}

bool C3DAdjustingPlane::isa(string& className) const
{
    if (className == "C3DAdjustingPlane")
        return true;

    return false;
}

int C3DAdjustingPlane::accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
						CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
						CRobustWeightFunction *robust, bool omitMarked)
{
	int nObs = 1;

	C3DPoint Pred = approx - *this->pERP;
	C3DPoint P;

	this->pROT->getRotMat().RT_times_x(P, Pred);

	double f00 = 0.0;

	for (int i = 0; i < 3; ++i)
	{
		P[i] *= this->mirrorCoeffs[i];
		f00 += P[i] * this->coefficients[i + 1];
	}

	f00 += this->coefficients[0];
	f00 *= this->denominator;

    CAdjustMatrix AiPar(1, this->activeParameter[addPar]);
    CAdjustMatrix AiROT(1, this->activeParameter[rotPar], 0.0);
    CAdjustMatrix AiERP(1, this->activeParameter[shiftPar]);
	CAdjustMatrix AiPoint(1, this->nParsPerPoint, 0.0);

	if (this->activeParameter[addPar])
	{
		int index = 0;
		double denomDer = this->denominator * this->denominator * this->denominator;
	
		if (this->parametrisation & this->constCoeff) 
		{
			AiPar(0, index) =  this->denominator;
			++index;
		};

		if (this->parametrisation & this->linUCoeff)  
		{
			AiPar(0, index) = ((this->coefficients[2] * this->coefficients[2] + this->coefficients[3] * this->coefficients[3]) * P.x
				                - this->coefficients[1] * this->coefficients[2] * P.y
							    - this->coefficients[1] * this->coefficients[3] * P.z
							    - this->coefficients[1] * this->coefficients[0]) * denomDer;
			++index;
		};

		if (this->parametrisation & this->linVCoeff)  
		{
			AiPar(0, index) = ((this->coefficients[1] * this->coefficients[1] + this->coefficients[3] * this->coefficients[3]) * P.x
				                - this->coefficients[2] * this->coefficients[1] * P.y
							    - this->coefficients[2] * this->coefficients[3] * P.z
							    - this->coefficients[2] * this->coefficients[0]) * denomDer;
			++index;
		};

		if (this->parametrisation & this->linWCoeff)  
		{
			AiPar(0, index) = ((this->coefficients[1] * this->coefficients[1] + this->coefficients[2] * this->coefficients[2]) * P.x
				                - this->coefficients[3] * this->coefficients[1] * P.y
							    - this->coefficients[3] * this->coefficients[2] * P.z
							    - this->coefficients[3] * this->coefficients[0]) * denomDer;
			++index;
		};
	}

	C3DPoint dFdP;
	for (int i = 0; i < 3; ++i)
	{
		dFdP[i] = this->coefficients[i + 1] * this->mirrorCoeffs[i] * this->denominator; 
	}
	
	if (this->activeParameter[rotPar])
	{
		CAdjustMatrix derivRotMat(9,3);
		this->pROT->computeDerivation(derivRotMat);
		C3DPoint dPd0, dPd1, dPd2;
		for (int i = 0; i < 3; ++i)
		{
			dPd0[i] = 0;
			dPd1[i] = 0;
			dPd2[i] = 0;
			

			for (int idxDer = i, j = 0; j < 3; j++, idxDer += 3)
			{
				dPd0[i] += Pred[j] * derivRotMat(idxDer, 0);
				dPd1[i] += Pred[j] * derivRotMat(idxDer, 1);
				dPd2[i] += Pred[j] * derivRotMat(idxDer, 2);
			}
		}

		for (int i = 0; i < 3; ++i)
		{
			AiROT(0,0) += dFdP[i] * dPd0[i]; 
			AiROT(0,1) += dFdP[i] * dPd1[i]; 
			AiROT(0,2) += dFdP[i] * dPd2[i]; 
		}	
	}

	if (this->activeParameter[shiftPar])
	{
		for (int i = 0; i < 3; ++i)
		{
			AiERP(0, i) = 0;
			for (int j = 0; j < 3; ++j)
			{
				AiERP(0, i) -= dFdP[i] * this->pROT->getRotMat()(j, i);
			}
		}
	}

	for (int i = 0; i < 3; ++i)
	{
		AiPoint(0, i) = 0;
		for (int j = 0; j < 3; ++j)
		{
			AiPoint(0, i) += dFdP[i] * this->pROT->getRotMat()(j, i);
		}
	}

	CAdjustMatrix Fi(1, 1);

	double sigInv = 1.0 / this->sigma;
	CAdjustMatrix PBB(1,1, sigInv * sigInv);
	
	discrepancy[0] = f00;
	
	normalisedDiscrepancy[0] = fabs(discrepancy[0]) * sigInv;

	if (robust || omitMarked)
	{
		double w0;
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 0, omitMarked, w0, PBB)) --nObs; 
	}

	PBB(0,0) = sqrt(PBB(0,0));

	AiPar   *= PBB(0,0);
    AiROT   *= PBB(0,0);
    AiERP   *= PBB(0,0);
	AiPoint *= PBB(0,0);
	Fi(0,0) = f00 * PBB(0,0);

	if (this->activeParameter[addPar])
	{
		CAdjustMatrix ATP = AiPar;
		ATP.transpose();
		CAdjustMatrix ATPA = ATP * AiPar;
		ATPA.addToMatrix(this->parameterIndex[addPar], this->parameterIndex[addPar], N);

		if (this->activeParameter[rotPar])
		{
			ATPA = ATP * AiROT;
			ATPA.addToMatrix(this->parameterIndex[addPar], this->parameterIndex[rotPar], N);
			ATPA.transpose();
			ATPA.addToMatrix(this->parameterIndex[rotPar], this->parameterIndex[addPar], N);
		}
		
		if (this->activeParameter[shiftPar])
		{
			ATPA = ATP * AiERP;
			ATPA.addToMatrix(this->parameterIndex[addPar], this->parameterIndex[shiftPar], N);
			ATPA.transpose();
			ATPA.addToMatrix(this->parameterIndex[shiftPar], this->parameterIndex[addPar], N);
		}

		ATPA = ATP * AiPoint;
		ATPA.addToMatrix(this->parameterIndex[addPar], approx.id, N);
		ATPA.transpose();
		ATPA.addToMatrix(approx.id, this->parameterIndex[addPar], N);
		ATPA = ATP * Fi;
		ATPA.addToMatrix(this->parameterIndex[addPar], 0, t);
	}

	if (this->activeParameter[rotPar])
	{
		CAdjustMatrix ATP = AiROT;
		ATP.transpose();
		CAdjustMatrix ATPA = ATP * AiROT;
		ATPA.addToMatrix(this->parameterIndex[rotPar], this->parameterIndex[rotPar], N);

		if (this->activeParameter[shiftPar])
		{
			ATPA = ATP * AiERP;
			ATPA.addToMatrix(this->parameterIndex[rotPar], this->parameterIndex[shiftPar], N);
			ATPA.transpose();
			ATPA.addToMatrix(this->parameterIndex[shiftPar], this->parameterIndex[rotPar], N);
		}

		ATPA = ATP * AiPoint;
		ATPA.addToMatrix(this->parameterIndex[rotPar], approx.id, N);
		ATPA.transpose();
		ATPA.addToMatrix(approx.id, this->parameterIndex[rotPar], N);
		ATPA = ATP * Fi;
		ATPA.addToMatrix(this->parameterIndex[rotPar], 0, t);
	}

	if (this->activeParameter[shiftPar])
	{
		CAdjustMatrix ATP = AiERP;
		ATP.transpose();
		CAdjustMatrix ATPA = ATP * AiERP;
		ATPA.addToMatrix(this->parameterIndex[shiftPar], this->parameterIndex[shiftPar], N);

		ATPA = ATP * AiPoint;
		ATPA.addToMatrix(this->parameterIndex[shiftPar], approx.id, N);
		ATPA.transpose();
		ATPA.addToMatrix(approx.id, this->parameterIndex[shiftPar], N);
		ATPA = ATP * Fi;
		ATPA.addToMatrix(this->parameterIndex[rotPar], 0, t);
	}
	
	CAdjustMatrix ATP = AiPoint;
	ATP.transpose();
	CAdjustMatrix ATPA = ATP * AiPoint;

	ATPA.addToMatrix(approx.id, approx.id, N);
	ATPA = ATP * Fi;
	ATPA.addToMatrix(approx.id, 0, t);

	return nObs;
};


bool C3DAdjustingPlane::AiBiFi(Matrix &Fi, Matrix &Bi, 
					 Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
					 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					 Matrix &AiPar,        Matrix &AiPoint,
					 const Matrix &obs, const C3DPoint &approx)
{
	AiErp.ReSize(1, this->activeParameter[shiftPar]);
    AiRot.ReSize(1, this->activeParameter[rotPar]);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);

    AiPar.ReSize(1, this->activeParameter[addPar]);
	AiPoint.ReSize(1, this->nParsPerPoint);

	Fi.ReSize(1,1);
    Bi.ReSize(1,1);

	int nObs = 1;

	C3DPoint Pred = approx - *this->pERP;
	C3DPoint P;

	this->pROT->getRotMat().RT_times_x(P, Pred);

	double f00 = 0.0;

	for (int i = 0; i < 3; ++i)
	{
		P[i] *= this->mirrorCoeffs[i];
		f00 += P[i] * this->coefficients[i + 1];
	}

	f00 += this->coefficients[0];
	f00 *= this->denominator;

    if (this->activeParameter[addPar])
	{
		int index = 0;
		double denomDer = this->denominator * this->denominator * this->denominator;
	
		if (this->parametrisation & this->constCoeff) 
		{
			AiPar.element(0, index) =  this->denominator;
			++index;
		};

		if (this->parametrisation & this->linUCoeff)  
		{
			AiPar.element(0, index) = ((this->coefficients[2] * this->coefficients[2] + this->coefficients[3] * this->coefficients[3]) * P.x
				                      - this->coefficients[1] * this->coefficients[2] * P.y
							          - this->coefficients[1] * this->coefficients[3] * P.z
							          - this->coefficients[1] * this->coefficients[0]) * denomDer;
			++index;
		};

		if (this->parametrisation & this->linVCoeff)  
		{
			AiPar.element(0, index) = ((this->coefficients[1] * this->coefficients[1] + this->coefficients[3] * this->coefficients[3]) * P.x
				                      - this->coefficients[2] * this->coefficients[1] * P.y
							          - this->coefficients[2] * this->coefficients[3] * P.z
							          - this->coefficients[2] * this->coefficients[0]) * denomDer;
			++index;
		};

		if (this->parametrisation & this->linWCoeff)  
		{
			AiPar.element(0, index) = ((this->coefficients[1] * this->coefficients[1] + this->coefficients[2] * this->coefficients[2]) * P.x
				                      - this->coefficients[3] * this->coefficients[1] * P.y
							          - this->coefficients[3] * this->coefficients[2] * P.z
							          - this->coefficients[3] * this->coefficients[0]) * denomDer;
			++index;
		};
	}

	C3DPoint dFdP;
	for (int i = 0; i < 3; ++i)
	{
		dFdP[i] = this->coefficients[i + 1] * this->mirrorCoeffs[i] * this->denominator; 
	}
	
	if (this->activeParameter[rotPar])
	{
		CAdjustMatrix derivRotMat(9,3);
		this->pROT->computeDerivation(derivRotMat);
		C3DPoint dPd0, dPd1, dPd2;
		for (int i = 0; i < 3; ++i)
		{
			dPd0[i] = 0;
			dPd1[i] = 0;
			dPd2[i] = 0;
			

			for (int idxDer = i, j = 0; j < 3; j++, idxDer += 3)
			{
				dPd0[i] += Pred[j] * derivRotMat(idxDer, 0);
				dPd1[i] += Pred[j] * derivRotMat(idxDer, 1);
				dPd2[i] += Pred[j] * derivRotMat(idxDer, 2);
			}
		}

		for (int i = 0; i < 3; ++i)
		{
			AiRot.element(0,0) += dFdP[i] * dPd0[i]; 
			AiRot.element(0,1) += dFdP[i] * dPd1[i]; 
			AiRot.element(0,2) += dFdP[i] * dPd2[i]; 
		}	
	}

	if (this->activeParameter[shiftPar])
	{
		for (int i = 0; i < 3; ++i)
		{
			AiErp.element(0, i) = 0;
			for (int j = 0; j < 3; ++j)
			{
				AiErp.element(0, i) -= dFdP[i] * this->pROT->getRotMat()(j, i);
			}
		}
	}

	for (int i = 0; i < 3; ++i)
	{
		AiPoint(0, i) = 0;
		for (int j = 0; j < 3; ++j)
		{
			AiPoint(0, i) += dFdP[i] * this->pROT->getRotMat()(j, i);
		}
	}

    Fi.element(0, 0) = f00;

    Bi.element(0, 0) = -1;
    
    return true;
}

bool C3DAdjustingPlane::Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca,    
				 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
				 Matrix &AiPar,        Matrix &AiPoint, 
				 const Matrix &obs, const C3DPoint &approx) const
{
	AiErp.ReSize(1, this->activeParameter[shiftPar]);
    AiRot.ReSize(1, this->activeParameter[rotPar]);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);

    AiPar.ReSize(1, this->activeParameter[addPar]);
	AiPoint.ReSize(1, this->nParsPerPoint);

	
	int nObs = 1;

	C3DPoint Pred = approx - *this->pERP;
	C3DPoint P;

	this->pROT->getRotMat().RT_times_x(P, Pred);

	double f00 = 0.0;

	for (int i = 0; i < 3; ++i)
	{
		P[i] *= this->mirrorCoeffs[i];
		f00 += P[i] * this->coefficients[i + 1];
	}

	f00 += this->coefficients[0];
	f00 *= this->denominator;

    if (this->activeParameter[addPar])
	{
		int index = 0;
		double denomDer = this->denominator * this->denominator * this->denominator;
	
		if (this->parametrisation & this->constCoeff) 
		{
			AiPar.element(0, index) =  this->denominator;
			++index;
		};

		if (this->parametrisation & this->linUCoeff)  
		{
			AiPar.element(0, index) = ((this->coefficients[2] * this->coefficients[2] + this->coefficients[3] * this->coefficients[3]) * P.x
				                      - this->coefficients[1] * this->coefficients[2] * P.y
							          - this->coefficients[1] * this->coefficients[3] * P.z
							          - this->coefficients[1] * this->coefficients[0]) * denomDer;
			++index;
		};

		if (this->parametrisation & this->linVCoeff)  
		{
			AiPar.element(0, index) = ((this->coefficients[1] * this->coefficients[1] + this->coefficients[3] * this->coefficients[3]) * P.x
				                      - this->coefficients[2] * this->coefficients[1] * P.y
							          - this->coefficients[2] * this->coefficients[3] * P.z
							          - this->coefficients[2] * this->coefficients[0]) * denomDer;
			++index;
		};

		if (this->parametrisation & this->linWCoeff)  
		{
			AiPar.element(0, index) = ((this->coefficients[1] * this->coefficients[1] + this->coefficients[2] * this->coefficients[2]) * P.x
				                      - this->coefficients[3] * this->coefficients[1] * P.y
							          - this->coefficients[3] * this->coefficients[2] * P.z
							          - this->coefficients[3] * this->coefficients[0]) * denomDer;
			++index;
		};
	}

	C3DPoint dFdP;
	for (int i = 0; i < 3; ++i)
	{
		dFdP[i] = this->coefficients[i + 1] * this->mirrorCoeffs[i] * this->denominator; 
	}
	
	if (this->activeParameter[rotPar])
	{
		CAdjustMatrix derivRotMat(9,3);
		this->pROT->computeDerivation(derivRotMat);
		C3DPoint dPd0, dPd1, dPd2;
		for (int i = 0; i < 3; ++i)
		{
			dPd0[i] = 0;
			dPd1[i] = 0;
			dPd2[i] = 0;
			

			for (int idxDer = i, j = 0; j < 3; j++, idxDer += 3)
			{
				dPd0[i] += Pred[j] * derivRotMat(idxDer, 0);
				dPd1[i] += Pred[j] * derivRotMat(idxDer, 1);
				dPd2[i] += Pred[j] * derivRotMat(idxDer, 2);
			}
		}

		for (int i = 0; i < 3; ++i)
		{
			AiRot.element(0,0) += dFdP[i] * dPd0[i]; 
			AiRot.element(0,1) += dFdP[i] * dPd1[i]; 
			AiRot.element(0,2) += dFdP[i] * dPd2[i]; 
		}	
	}

	if (this->activeParameter[shiftPar])
	{
		for (int i = 0; i < 3; ++i)
		{
			AiErp.element(0, i) = 0;
			for (int j = 0; j < 3; ++j)
			{
				AiErp.element(0, i) -= dFdP[i] * this->pROT->getRotMat()(j, i);
			}
		}
	}

	for (int i = 0; i < 3; ++i)
	{
		AiPoint.element(0, i) = 0;
		for (int j = 0; j < 3; ++j)
		{
			AiPoint.element(0, i) += dFdP[i] * this->pROT->getRotMat()(j, i);
		}
	}

	return true;
}

bool C3DAdjustingPlane::AiPars(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
					 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const
{
	AiErp.ReSize(1, this->activeParameter[shiftPar]);
    AiRot.ReSize(1, this->activeParameter[rotPar]);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);

    AiPar.ReSize(1, this->activeParameter[addPar]);
	
	int nObs = 1;

	C3DPoint Pred = approx - *this->pERP;
	C3DPoint P;

	this->pROT->getRotMat().RT_times_x(P, Pred);

	double f00 = 0.0;

	for (int i = 0; i < 3; ++i)
	{
		P[i] *= this->mirrorCoeffs[i];
		f00 += P[i] * this->coefficients[i + 1];
	}

	f00 += this->coefficients[0];
	f00 *= this->denominator;

    if (this->activeParameter[addPar])
	{
		int index = 0;
		double denomDer = this->denominator * this->denominator * this->denominator;
	
		if (this->parametrisation & this->constCoeff) 
		{
			AiPar.element(0, index) =  this->denominator;
			++index;
		};

		if (this->parametrisation & this->linUCoeff)  
		{
			AiPar.element(0, index) = ((this->coefficients[2] * this->coefficients[2] + this->coefficients[3] * this->coefficients[3]) * P.x
				                      - this->coefficients[1] * this->coefficients[2] * P.y
							          - this->coefficients[1] * this->coefficients[3] * P.z
							          - this->coefficients[1] * this->coefficients[0]) * denomDer;
			++index;
		};

		if (this->parametrisation & this->linVCoeff)  
		{
			AiPar.element(0, index) = ((this->coefficients[1] * this->coefficients[1] + this->coefficients[3] * this->coefficients[3]) * P.x
				                      - this->coefficients[2] * this->coefficients[1] * P.y
							          - this->coefficients[2] * this->coefficients[3] * P.z
							          - this->coefficients[2] * this->coefficients[0]) * denomDer;
			++index;
		};

		if (this->parametrisation & this->linWCoeff)  
		{
			AiPar.element(0, index) = ((this->coefficients[1] * this->coefficients[1] + this->coefficients[2] * this->coefficients[2]) * P.x
				                      - this->coefficients[3] * this->coefficients[1] * P.y
							          - this->coefficients[3] * this->coefficients[2] * P.z
							          - this->coefficients[3] * this->coefficients[0]) * denomDer;
			++index;
		};
	}

	C3DPoint dFdP;
	for (int i = 0; i < 3; ++i)
	{
		dFdP[i] = this->coefficients[i + 1] * this->mirrorCoeffs[i] * this->denominator; 
	}
	
	if (this->activeParameter[rotPar])
	{
		CAdjustMatrix derivRotMat(9,3);
		this->pROT->computeDerivation(derivRotMat);
		C3DPoint dPd0, dPd1, dPd2;
		for (int i = 0; i < 3; ++i)
		{
			dPd0[i] = 0;
			dPd1[i] = 0;
			dPd2[i] = 0;
			

			for (int idxDer = i, j = 0; j < 3; j++, idxDer += 3)
			{
				dPd0[i] += Pred[j] * derivRotMat(idxDer, 0);
				dPd1[i] += Pred[j] * derivRotMat(idxDer, 1);
				dPd2[i] += Pred[j] * derivRotMat(idxDer, 2);
			}
		}

		for (int i = 0; i < 3; ++i)
		{
			AiRot.element(0,0) += dFdP[i] * dPd0[i]; 
			AiRot.element(0,1) += dFdP[i] * dPd1[i]; 
			AiRot.element(0,2) += dFdP[i] * dPd2[i]; 
		}	
	}

	if (this->activeParameter[shiftPar])
	{
		for (int i = 0; i < 3; ++i)
		{
			AiErp.element(0, i) = 0;
			for (int j = 0; j < 3; ++j)
			{
				AiErp.element(0, i) -= dFdP[i] * this->pROT->getRotMat()(j, i);
			}
		}
	}



    return true;
}


bool C3DAdjustingPlane::AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const
{
    // ob will be a 2 x 1
    // and contains an xy image pair
    Ai.ReSize(1, this->nParsPerPoint);

	C3DPoint dFdP;
	for (int i = 0; i < 3; ++i)
	{
		dFdP[i] = this->coefficients[i + 1] * this->mirrorCoeffs[i] * this->denominator; 
	}
	
 	for (int i = 0; i < 3; ++i)
	{
		Ai.element(0, i) = 0;
		for (int j = 0; j < 3; ++j)
		{
			Ai(0, i) += dFdP[i] * this->pROT->getRotMat()(j, i);
		}
	}

    return true;
}


bool C3DAdjustingPlane::Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const
{
    Fi.ReSize(1, 1);

   	double f00 = 0.0;

	C3DPoint Pred = approx - *this->pERP;
	C3DPoint P;

	this->pROT->getRotMat().RT_times_x(P, Pred);

	for (int i = 0; i < 3; ++i)
	{
		P[i] *= this->mirrorCoeffs[i];
		f00 += P[i] * this->coefficients[i + 1];
	}

	f00 += this->coefficients[0];
	f00 *= this->denominator;

    Fi.element(0, 0) = f00;

    return true;
}

bool C3DAdjustingPlane::Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const
{
    // This is basically a Gauss-markov problem,
    // so the B matrix is just -Identity since x and y can
    // be solved for explicitly in the model
    Bi.ReSize(1, 1);

    Bi.element(0, 0) = -1;

    return true;
}

void C3DAdjustingPlane::getName(CCharString& name) const
{
    name = "Adjusting Plane";
}

void C3DAdjustingPlane::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
    StationNames.RemoveAll();
    PointNames.RemoveAll();

	CCharString* str;

    str=StationNames.Add();
    *str = "a0";

    str=StationNames.Add();
    *str = "a1";

    str=StationNames.Add();
    *str = "a2";

    str=StationNames.Add();
    *str = "a3";
    
	str=PointNames.Add();
    *str = "X";

    str=PointNames.Add();
    *str = "Y";

	str=PointNames.Add();
    *str = "Z";
}

void C3DAdjustingPlane::addDirectObservations(Matrix &N, Matrix &t)
{
};

bool C3DAdjustingPlane::setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const
{
	return true;
};
	 
bool C3DAdjustingPlane::setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const
{
	CXYPoint obs2D(obs.getX(), obs.getY(), obs.getLabel());
	return setHeigthAtXY(obj, obs2D);
};

void C3DAdjustingPlane::copy(const C3DAdjustingPlane &plane)
{
	*this = plane;
   
	this->informListeners_elementsChanged();
};


void C3DAdjustingPlane::crop(int offsetX, int offsetY, int newWidth, int newHeight)
{
};
