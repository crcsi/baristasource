#include "DistortionTHEOS.h"

#include "XYZPoint.h"
#include "doubles.h"
#include "SensorModel.h"
#include "AdjustMatrix.h"
#include "2DPoint.h"
CDistortionTHEOS::CDistortionTHEOS(void) : 
CDistortion(eDistortionTHEOS),xDistortion(3),yDistortion(3),xDistortionObs(3),yDistortionObs(3)
{
	this->xDistortion.setCoefficient(0,0.0);
	this->xDistortion.setCoefficient(1,0.0);
	this->xDistortion.setCoefficient(2,0.0);
	this->xDistortion.setCoefficient(3,0.0);

	this->yDistortion.setCoefficient(0,0.0);
	this->yDistortion.setCoefficient(1,0.0);
	this->yDistortion.setCoefficient(2,0.0);
	this->yDistortion.setCoefficient(3,0.0);

	this->xDistortionObs.setCoefficient(0,0.0);
	this->xDistortionObs.setCoefficient(1,0.0);
	this->xDistortionObs.setCoefficient(2,0.0);
	this->xDistortionObs.setCoefficient(3,0.0);

	this->yDistortionObs.setCoefficient(0,0.0);
	this->yDistortionObs.setCoefficient(1,0.0);
	this->yDistortionObs.setCoefficient(2,0.0);
	this->yDistortionObs.setCoefficient(3,0.0);

	Matrix covar(4,4);
	covar =0;

	covar.element(0,0) = 2.0;
	covar.element(1,1) = 9.0e-8;
	covar.element(2,2) = 9.0e-8;
	covar.element(3,3) = 9.0e-8;

	this->xDistortionObs.setCovariance(covar);
	this->yDistortionObs.setCovariance(covar);

	this->resetParameter();




}

CDistortionTHEOS::CDistortionTHEOS(const CDistortionTHEOS& d) : 
CDistortion(d),xDistortion(d.xDistortion),yDistortion(d.yDistortion),
xDistortionObs(d.xDistortionObs),yDistortionObs(d.yDistortionObs)
{

}

CDistortionTHEOS::~CDistortionTHEOS(void)
{

}


CDistortionTHEOS &CDistortionTHEOS::operator = (const CDistortionTHEOS & d)
{
	CDistortion::operator=(d);

	this->xDistortion = d.xDistortion;
	this->yDistortion = d.yDistortion;
	this->xDistortionObs = d.xDistortionObs;
	this->yDistortionObs = d.yDistortionObs;
	return *this;
}


void CDistortionTHEOS::setDistortion(const doubles& parameter) 
{
	if (parameter.size() != 8)
		return;

	// expected order a0,a1,a2,a3,b0,b1,b2,b3
	this->xDistortion.setCoefficient(0,parameter[0]);
	this->xDistortion.setCoefficient(1,parameter[1]);
	this->xDistortion.setCoefficient(2,parameter[2]);
	this->xDistortion.setCoefficient(3,parameter[3]);

	this->yDistortion.setCoefficient(0,parameter[4]);
	this->yDistortion.setCoefficient(1,parameter[5]);
	this->yDistortion.setCoefficient(2,parameter[6]);
	this->yDistortion.setCoefficient(3,parameter[7]);

	this->xDistortionObs.setCoefficient(0,parameter[0]);
	this->xDistortionObs.setCoefficient(1,parameter[1]);
	this->xDistortionObs.setCoefficient(2,parameter[2]);
	this->xDistortionObs.setCoefficient(3,parameter[3]);

	this->yDistortionObs.setCoefficient(0,parameter[4]);
	this->yDistortionObs.setCoefficient(1,parameter[5]);
	this->yDistortionObs.setCoefficient(2,parameter[6]);
	this->yDistortionObs.setCoefficient(3,parameter[7]);

}


bool CDistortionTHEOS::applyObs2Obj(C3DPoint& obj,const C3DPoint& obs, const CXYZPoint& irp,const double& scale) const
{
	double x = obs[0]*scale;

	obj.x += this->xDistortion.coefficient(0) + x*this->xDistortion.coefficient(1) + pow(x,2)*this->xDistortion.coefficient(2) + pow(x,3)*this->xDistortion.coefficient(3);
	obj.y += this->yDistortion.coefficient(0) + x*this->yDistortion.coefficient(1) + pow(x,2)*this->yDistortion.coefficient(2) + pow(x,3)*this->yDistortion.coefficient(3);

	return true;
}

bool CDistortionTHEOS::applyObj2Obs(C2DPoint& obs,const C2DPoint& obj, const CXYZPoint& irp,const double& scale) const
{

	C3DPoint correction(0,0,0);
	C3DPoint obj3D(obj.x,obj.y,0);

	// compute correction and apply in opposit direction
	this->applyObs2Obj(correction, obj3D + irp,irp,scale);

	// new obs in camera system
	C3DPoint localObs = obj3D - correction;

	int maxIter = 50;
	double diff;
	double eps = 1e-8;

	int i=0;
	for (i = 0; i < maxIter; i++)
	{
		correction.x = 0;
		correction.y = 0;
		// compute correction for new observation
		this->applyObs2Obj(correction,localObs + irp ,irp,scale);

		// compute difference between original object coordinates and the new ones plus their corrections
		double dx = obj.x - localObs.x - correction.x;
		double dy = obj.y - localObs.y - correction.y;

		diff = dx*dx + dy*dy;

		// update the object coordinates
		localObs.x += dx;
		localObs.y += dy;

		if (diff < eps)
			break;

	}

	if (i != maxIter)	
	{
		obs.x = localObs.x;
		obs.y = localObs.y;
		return true;
	}

	return false;
}


bool CDistortionTHEOS::Ai(CAdjustMatrix& AiAdp,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar) const
{	/*
	int nPar = 0;

	// count the number of parameter
	if (activeAddPar & PARAMETER_1) nPar++;
	if (activeAddPar & PARAMETER_2) nPar++;
	if (activeAddPar & PARAMETER_3) nPar++;
	if (activeAddPar & PARAMETER_4) nPar++;
	if (activeAddPar & PARAMETER_5) nPar++;
	if (activeAddPar & PARAMETER_6) nPar++;

	AiAdp.reSize(2,nPar,0.0);

	// fill the matrix
	int col=0;

	if (activeAddPar & PARAMETER_1) AiAdp(0,col++) = -1;
	if (activeAddPar & PARAMETER_2) AiAdp(0,col++) = -obs.x;
	if (activeAddPar & PARAMETER_3) AiAdp(0,col++) = -obs.x*obs.x;
	if (activeAddPar & PARAMETER_4) AiAdp(1,col++) = -1;
	if (activeAddPar & PARAMETER_5) AiAdp(1,col++) = -obs.x;
	if (activeAddPar & PARAMETER_6) AiAdp(1,col++) = -obs.x*obs.x;

	return true;//*/
	return false;
}

bool CDistortionTHEOS::Bi(CAdjustMatrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const
{

	/*
	// -a0 -a1*xf - a2*xf^2
	Bi(0,0) += -this->xDistortion.coefficient(1) - this->xDistortion.coefficient(2) * 2 * obs.x;

	// -b0 -b1*xf - b2*xf^2
	Bi(0,0) += -this->yDistortion.coefficient(1) - this->yDistortion.coefficient(2) * 2 * obs.x;

	Bi(1,1) += -1.0;

	return true;//*/

	return false;
}

bool CDistortionTHEOS::Bi(Matrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const
{

	/*
	// -a0 -a1*xf - a2*xf^2
	Bi.element(0,0) += - this->xDistortion.coefficient(1) - this->xDistortion.coefficient(2) * 2 * obs.x;

	// -b0 -b1*xf - b2*xf^2
	Bi.element(0,0) += - this->yDistortion.coefficient(1) -  this->yDistortion.coefficient(2) * 2 * obs.x;

	Bi.element(1,1) += -1.0;

	return true;//*/
	return false;
}


bool CDistortionTHEOS::insertParametersForAdjustment(Matrix &parms,int indexAdp,int activeAddPar)const
{
	/*
	int index = indexAdp;

	if (activeAddPar & PARAMETER_1) parms.element(index++,0) = this->xDistortion.coefficient(0);
	if (activeAddPar & PARAMETER_2) parms.element(index++,0) = this->xDistortion.coefficient(1);
	if (activeAddPar & PARAMETER_3) parms.element(index++,0) = this->xDistortion.coefficient(2);
	if (activeAddPar & PARAMETER_4) parms.element(index++,0) = this->yDistortion.coefficient(0);
	if (activeAddPar & PARAMETER_5) parms.element(index++,0) = this->yDistortion.coefficient(1);
	if (activeAddPar & PARAMETER_6) parms.element(index++,0) = this->yDistortion.coefficient(2);

	return true;//*/

	return false;
}


bool CDistortionTHEOS::setParametersFromAdjustment(const Matrix &parms,int indexAdp,int activeAddPar)
{	/*
	int index = indexAdp;

	if (activeAddPar & PARAMETER_1) this->xDistortion.setCoefficient(0,parms.element(index++,0));
	if (activeAddPar & PARAMETER_2) this->xDistortion.setCoefficient(1,parms.element(index++,0));
	if (activeAddPar & PARAMETER_3) this->xDistortion.setCoefficient(2,parms.element(index++,0));
	if (activeAddPar & PARAMETER_4) this->yDistortion.setCoefficient(0,parms.element(index++,0));
	if (activeAddPar & PARAMETER_5) this->yDistortion.setCoefficient(1,parms.element(index++,0));
	if (activeAddPar & PARAMETER_6) this->yDistortion.setCoefficient(2,parms.element(index++,0));

	return true;//*/

	return false;
}

void  CDistortionTHEOS::printParameter(ostrstream &protocol,int activeAdpPar ) const
{
	int widthParameterName = 12;
	int widthParameter = 15;
	int widthSigma = 15;

	int precisionParameter = 2;
	int precisionSigma = 2;

	protocol.setf(ios::fixed, ios::floatfield);
	protocol << "ALOS additional parameter";
	protocol << "\n      Name                Parameter              Sigma       ";

	if (activeAdpPar & PARAMETER_1)
	{
		protocol << "\n      ";
		protocol.width(widthParameterName); 
		protocol << "a0 [pixel]  : ";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->xDistortion.coefficient(0);
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << sqrt(this->xDistortion.getCovariance().element(0,0));
	}

	if (activeAdpPar & PARAMETER_2)
	{
		protocol << "\n      ";
		protocol.width(widthParameterName); 
		protocol << "a1 [-]      : ";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->xDistortion.coefficient(1);
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << sqrt(this->xDistortion.getCovariance().element(1,1));
	}

	if (activeAdpPar & PARAMETER_3)
	{
		protocol << "\n      ";
		protocol.width(widthParameterName); 
		protocol << "a3 [1/pixel]: ";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->xDistortion.coefficient(2);
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << sqrt(this->xDistortion.getCovariance().element(2,2));
	}

	if (activeAdpPar & PARAMETER_4)
	{
		protocol << "\n      ";
		protocol.width(widthParameterName); 
		protocol << "b0 [pixel]  : ";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->yDistortion.coefficient(0);
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << sqrt(this->xDistortion.getCovariance().element(0,0));
	}

	if (activeAdpPar & PARAMETER_5)
	{
		protocol << "\n      ";
		protocol.width(widthParameterName); 
		protocol << "b1 [-]      : ";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->yDistortion.coefficient(1);
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << sqrt(this->xDistortion.getCovariance().element(1,1));
	}

	if (activeAdpPar & PARAMETER_6)
	{
		protocol << "\n      ";
		protocol.width(widthParameterName); 
		protocol << "b2 [1/pixel]: ";
		protocol.width(widthParameter);
		protocol.precision(precisionParameter);
		protocol << this->yDistortion.coefficient(2);
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigma);
		protocol << sqrt(this->xDistortion.getCovariance().element(2,2));
	}




}


void CDistortionTHEOS::setParameterCovariance(const Matrix &covar,int activeAdpPar)
{
	int nParameter=0;

	nParameter += this->xDistortion.getDegree() ? this->xDistortion.getDegree() + 1 : 0;
	nParameter += this->yDistortion.getDegree() ? this->yDistortion.getDegree() + 1 : 0;
	//nParameter += this->radialDistortion.getDegree() ? this->radialDistortion.getDegree() + 1 : 0;
	Matrix allParameter(nParameter,nParameter);
	allParameter =0;

	vector<int> index;
	if (activeAdpPar & PARAMETER_1) index.push_back(0);
	if (activeAdpPar & PARAMETER_2) index.push_back(1);
	if (activeAdpPar & PARAMETER_3) index.push_back(2);
	if (activeAdpPar & PARAMETER_4) index.push_back(3);
	if (activeAdpPar & PARAMETER_5) index.push_back(4);
	if (activeAdpPar & PARAMETER_6) index.push_back(5);

	for (int i=0; i<covar.Nrows(); i++)
		for (int k=0; k < covar.Ncols(); k++)
			allParameter.element(index[i],index[k]) = covar.element(i,k);

	this->xDistortion.setCovariance(allParameter.SubMatrix(1,3,1,3));
	this->yDistortion.setCovariance(allParameter.SubMatrix(4,6,4,6));
}

void CDistortionTHEOS::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->radialDistortion.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->xDistortion.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->yDistortion.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->xDistortionObs.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->yDistortionObs.serializeStore(child);

}

void CDistortionTHEOS::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	// and now the private members
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		CCharString key = token->getKey();
	}


	bool hasRadialDistortion = false;
	bool hasXDistortion = false;
	bool hasYDistortion = false;
	bool hasXDistortionObs = false;


	for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
	{
		CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
		if (child->getName().CompareNoCase("CPolynomial") && 
			!hasRadialDistortion && 
			!hasXDistortion && 
			!hasYDistortion &&
			!hasXDistortionObs)
		{
			this->radialDistortion.serializeRestore(child);
			hasRadialDistortion = true;
		}
		else if (child->getName().CompareNoCase("CPolynomial") && 
			hasRadialDistortion && 
			!hasXDistortion && 
			!hasYDistortion &&
			!hasXDistortionObs)
		{
			this->xDistortion.serializeRestore(child);
			hasXDistortion = true;
		}
		else if (child->getName().CompareNoCase("CPolynomial") && 
			hasRadialDistortion && 
			hasXDistortion && 
			!hasYDistortion &&
			!hasXDistortionObs)
		{
			this->yDistortion.serializeRestore(child);
			hasYDistortion = true;
		}
		else if (child->getName().CompareNoCase("CPolynomial") && 
			hasRadialDistortion && 
			hasXDistortion && 
			hasYDistortion &&
			!hasXDistortionObs)
		{
			this->xDistortionObs.serializeRestore(child);
			hasXDistortionObs = true;
		}
		else if (child->getName().CompareNoCase("CPolynomial") && 
			hasRadialDistortion && 
			hasXDistortion && 
			hasYDistortion &&
			hasXDistortionObs)
		{
			this->yDistortionObs.serializeRestore(child);

		}

	}

}



void CDistortionTHEOS::addDirectObservations(Matrix &N, Matrix &t,int indexAdp,int activeAddPar) 
{
	Matrix Qx = this->xDistortionObs.getCovariance();
	Matrix Qy = this->yDistortionObs.getCovariance();

	int index = indexAdp -1;

	if (activeAddPar & PARAMETER_1) 
	{
		double p = 1.0/Qx.element(0,0);
		index++;
		N.element(index,index) += p;
		t.element(index,0) += p * (this->xDistortion.coefficient(0) - this->xDistortionObs.coefficient(0));
	}

	if (activeAddPar & PARAMETER_2)
	{
		double p = 1.0/Qx.element(1,1);
		index++;
		N.element(index,index) += p;
		t.element(index,0) += p * (this->xDistortion.coefficient(1) - this->xDistortionObs.coefficient(1));
	}

	if (activeAddPar & PARAMETER_3) 
	{
		double p = 1.0/Qx.element(2,2);
		index++;
		N.element(index,index) += p;
		t.element(index,0) += p * (this->xDistortion.coefficient(2) - this->xDistortionObs.coefficient(2));
	}


	if (activeAddPar & PARAMETER_4) 
	{
		double p = 1.0/Qy.element(0,0);
		index++;
		N.element(index,index) += p;
		t.element(index,0) += p * (this->yDistortion.coefficient(0) - this->yDistortionObs.coefficient(0));
	}

	if (activeAddPar & PARAMETER_5)
	{
		double p = 1.0/Qy.element(1,1);
		index++;
		N.element(index,index) += p;
		t.element(index,0) += p * (this->yDistortion.coefficient(1) - this->yDistortionObs.coefficient(1));
	}

	if (activeAddPar & PARAMETER_6) 
	{
		double p = 1.0/Qy.element(2,2);
		index++;
		N.element(index,index) += p;
		t.element(index,0) += p * (this->yDistortion.coefficient(2) - this->yDistortionObs.coefficient(2));
	}

}

double CDistortionTHEOS::getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res,int indexAdp,int activeAddPar) 
{
	double value = 0.0;

	vector<double> resTmp;

	Matrix Qx = this->xDistortionObs.getCovariance();
	Matrix Qy = this->yDistortionObs.getCovariance();

	if (activeAddPar & PARAMETER_1)
	{
		double r = - deltaPar.element(indexAdp++,0) + this->xDistortion.coefficient(0) - this->xDistortionObs.coefficient(0);
		resTmp.push_back(r);
		value += r*r / Qx.element(0,0);
	}

	if (activeAddPar & PARAMETER_2)
	{
		double r = - deltaPar.element(indexAdp++,0) + this->xDistortion.coefficient(1) - this->xDistortionObs.coefficient(1);
		resTmp.push_back(r);
		value += r*r / Qx.element(1,1);
	}

	if (activeAddPar & PARAMETER_3)
	{
		double r = - deltaPar.element(indexAdp++,0) + this->xDistortion.coefficient(2) - this->xDistortionObs.coefficient(2);
		resTmp.push_back(r);
		value += r*r / Qx.element(2,2);
	}

	if (activeAddPar & PARAMETER_4)
	{
		double r = - deltaPar.element(indexAdp++,0) + this->yDistortion.coefficient(0) - this->yDistortionObs.coefficient(0);
		resTmp.push_back(r);
		value += r*r / Qy.element(0,0);
	}

	if (activeAddPar & PARAMETER_5)
	{
		double r = - deltaPar.element(indexAdp++,0) + this->yDistortion.coefficient(1) - this->yDistortionObs.coefficient(1);
		resTmp.push_back(r);
		value += r*r / Qy.element(1,1);
	}

	if (activeAddPar & PARAMETER_6)
	{
		double r = - deltaPar.element(indexAdp++,0) + this->yDistortion.coefficient(2) - this->yDistortionObs.coefficient(2);
		resTmp.push_back(r);
		value += r*r / Qy.element(2,2);
	}

	res.ReSize(resTmp.size(),1);

	for (int i=0; i< res.Nrows(); i++)
		res.element(i,0) = resTmp[i];

	return value;
}

void CDistortionTHEOS::setDirectObservation(double observation,int activeAdpPar)
{
	if (activeAdpPar & PARAMETER_1) this->xDistortionObs.setCoefficient(0,observation);
	if (activeAdpPar & PARAMETER_2) this->xDistortionObs.setCoefficient(1,observation);
	if (activeAdpPar & PARAMETER_3) this->xDistortionObs.setCoefficient(2,observation);
	if (activeAdpPar & PARAMETER_4) this->xDistortionObs.setCoefficient(3,observation);

	if (activeAdpPar & PARAMETER_5) this->yDistortionObs.setCoefficient(0,observation);
	if (activeAdpPar & PARAMETER_6) this->yDistortionObs.setCoefficient(1,observation);
	if (activeAdpPar & PARAMETER_7) this->yDistortionObs.setCoefficient(2,observation);
	if (activeAdpPar & PARAMETER_8) this->yDistortionObs.setCoefficient(3,observation);
}

double CDistortionTHEOS::getDirectObservation(int activeAdpPar)
{
	if (activeAdpPar & PARAMETER_1) return this->xDistortionObs.coefficient(0);
	if (activeAdpPar & PARAMETER_2) return this->xDistortionObs.coefficient(1);
	if (activeAdpPar & PARAMETER_3) return this->xDistortionObs.coefficient(2);
	if (activeAdpPar & PARAMETER_4) return this->xDistortionObs.coefficient(3);

	if (activeAdpPar & PARAMETER_5) return this->yDistortionObs.coefficient(0);
	if (activeAdpPar & PARAMETER_6) return this->yDistortionObs.coefficient(1);
	if (activeAdpPar & PARAMETER_7) return this->yDistortionObs.coefficient(2);
	if (activeAdpPar & PARAMETER_8) return this->yDistortionObs.coefficient(3);

	return 0.0;
}

double CDistortionTHEOS::getSigmaDirectObservation(int activeAdpPar)
{
	if (activeAdpPar & PARAMETER_1) return sqrt(this->xDistortionObs.getCovariance().element(0,0));
	if (activeAdpPar & PARAMETER_2) return sqrt(this->xDistortionObs.getCovariance().element(1,1));
	if (activeAdpPar & PARAMETER_3) return sqrt(this->xDistortionObs.getCovariance().element(2,2));
	if (activeAdpPar & PARAMETER_4) return sqrt(this->xDistortionObs.getCovariance().element(3,3));

	if (activeAdpPar & PARAMETER_5) return sqrt(this->yDistortionObs.getCovariance().element(0,0));
	if (activeAdpPar & PARAMETER_6) return sqrt(this->yDistortionObs.getCovariance().element(1,1));
	if (activeAdpPar & PARAMETER_7) return sqrt(this->yDistortionObs.getCovariance().element(2,2));
	if (activeAdpPar & PARAMETER_8) return sqrt(this->yDistortionObs.getCovariance().element(3,3));

	return 0.0;
}

void CDistortionTHEOS::setSigmaDirectObservation(double sigma,int activeAdpPar)
{
	if (activeAdpPar & PARAMETER_1) this->xDistortionObs.setCovarElement(0,0,sigma*sigma);
	if (activeAdpPar & PARAMETER_2) this->xDistortionObs.setCovarElement(1,1,sigma*sigma);
	if (activeAdpPar & PARAMETER_3) this->xDistortionObs.setCovarElement(2,2,sigma*sigma);
	if (activeAdpPar & PARAMETER_4) this->xDistortionObs.setCovarElement(3,3,sigma*sigma);

	if (activeAdpPar & PARAMETER_5) this->yDistortionObs.setCovarElement(0,0,sigma*sigma);
	if (activeAdpPar & PARAMETER_6) this->yDistortionObs.setCovarElement(1,1,sigma*sigma);
	if (activeAdpPar & PARAMETER_7) this->yDistortionObs.setCovarElement(2,2,sigma*sigma);
	if (activeAdpPar & PARAMETER_8) this->yDistortionObs.setCovarElement(3,3,sigma*sigma);

};

double CDistortionTHEOS::getParameter(int activeAdpPar)
{
	if (activeAdpPar & PARAMETER_1) return this->xDistortion.coefficient(0);
	if (activeAdpPar & PARAMETER_2) return this->xDistortion.coefficient(1);
	if (activeAdpPar & PARAMETER_3) return this->xDistortion.coefficient(2);
	if (activeAdpPar & PARAMETER_4) return this->xDistortion.coefficient(3);

	if (activeAdpPar & PARAMETER_5) return this->yDistortion.coefficient(0);
	if (activeAdpPar & PARAMETER_6) return this->yDistortion.coefficient(1);
	if (activeAdpPar & PARAMETER_7) return this->yDistortion.coefficient(2);
	if (activeAdpPar & PARAMETER_8) return this->yDistortion.coefficient(3);

	return 0.0;
}

double CDistortionTHEOS::getSigmaParameter(int activeAdpPar)
{
	if (activeAdpPar & PARAMETER_1) return sqrt(this->xDistortion.getCovariance().element(0,0));
	if (activeAdpPar & PARAMETER_2) return sqrt(this->xDistortion.getCovariance().element(1,1));
	if (activeAdpPar & PARAMETER_3) return sqrt(this->xDistortion.getCovariance().element(2,2));
	if (activeAdpPar & PARAMETER_4) return sqrt(this->xDistortion.getCovariance().element(3,3));

	if (activeAdpPar & PARAMETER_5) return sqrt(this->yDistortion.getCovariance().element(0,0));
	if (activeAdpPar & PARAMETER_6) return sqrt(this->yDistortion.getCovariance().element(1,1));
	if (activeAdpPar & PARAMETER_7) return sqrt(this->yDistortion.getCovariance().element(2,2));
	if (activeAdpPar & PARAMETER_8) return sqrt(this->yDistortion.getCovariance().element(3,3));

	return 0.0;
}

void CDistortionTHEOS::setParameter(double parameter,int activeAdpPar)
{
	if (activeAdpPar & PARAMETER_1) this->xDistortion.setCoefficient(0,parameter);
	if (activeAdpPar & PARAMETER_2) this->xDistortion.setCoefficient(1,parameter);
	if (activeAdpPar & PARAMETER_3) this->xDistortion.setCoefficient(2,parameter);
	if (activeAdpPar & PARAMETER_4) this->xDistortion.setCoefficient(3,parameter);

	if (activeAdpPar & PARAMETER_5) this->yDistortion.setCoefficient(0,parameter);
	if (activeAdpPar & PARAMETER_6) this->yDistortion.setCoefficient(1,parameter);
	if (activeAdpPar & PARAMETER_7) this->yDistortion.setCoefficient(2,parameter);
	if (activeAdpPar & PARAMETER_8) this->yDistortion.setCoefficient(3,parameter);
}

void CDistortionTHEOS::getParameterName(CCharString& name,int activeParameter)
{
	if (activeParameter & PARAMETER_1) name = "shift x [pixel]";
	if (activeParameter & PARAMETER_2) name = "scale x [1pl/mm]";
	if (activeParameter & PARAMETER_3) name = "quadratic term x [1pl/mm2]";
	if (activeParameter & PARAMETER_4) name = "cubic term x [1pl/mm3]";

	if (activeParameter & PARAMETER_5) name = "shift y  [pixel]";
	if (activeParameter & PARAMETER_6) name = "scale y [1pl/mm]";
	if (activeParameter & PARAMETER_7) name = "quadratic term y [1pl/mm2]";
	if (activeParameter & PARAMETER_8) name = "cubic term y [1pl/mm3]";
}

void CDistortionTHEOS::resetParameter()
{

	this->xDistortion = this->xDistortionObs;
	this->yDistortion = this->yDistortionObs;


	Matrix covar(4,4);
	covar =0;
	covar.element(0,0) = 2.0;
	covar.element(1,1) = 9.0e-8;
	covar.element(2,2) = 9.0e-8;
	covar.element(3,3) = 9.0e-8;

	this->xDistortion.setCovariance(covar);
	this->yDistortion.setCovariance(covar);
}
