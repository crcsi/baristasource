#include "SplineModelFactory.h"

#include "OrbitPathModel.h"
#include "OrbitAttitudeModel.h"

CSplineModel* CSplineModelFactory::initSplineModel(SplineModelType type)
{
	if (type == Spline)
	{
		return new CSplineModel();
	}
	else if (type == Path)
	{
		return new COrbitPathModel();

	}
	else if (type == Attitude)
	{
		return new COrbitAttitudeModel();
	}
	else
		return 0;
}


CSplineModel* CSplineModelFactory::initSplineModel(CSplineModel* splineModel,SplineModelType type)
{
	if (type == Spline)
	{
		return new CSplineModel(*splineModel);
	}
	else if (type == Path)
	{
		return new COrbitPathModel(*(COrbitPathModel*)splineModel);

	}
	else if (type == Attitude)
	{
		return new COrbitAttitudeModel(*(COrbitAttitudeModel*)splineModel);
	}
	else
		return 0;
}