/**
 * @class CRPC
 * CRPC stores and handles rpc parameters
 * concerning import/export, sensor modeling
 */
#include <float.h>
#include "newmat.h"

#include "RPC.h"
#include "2DPoint.h"
#include "3DPoint.h"
#include "XYPoint.h"
#include "XYZPoint.h"
#include "2DPointArray.h"
#include "3DPointArray.h"
#include "XYPointArray.h"
#include "XYZPointArray.h"
#include "StructuredFile.h"
#include "utilities.h"
#include "doubles.h"

#include "MUIntegerArray.h"
#include "BundleManager.h"
#include "AdjustMatrix.h"

CRPC::CRPC() : CSensorModel(false, "RPC", 0, 0, 0, 0, 0, 0, 7, 3, 2),
		line_off (0), samp_off(0), lat_off(0), long_off(0), height_off(0), line_scale(1),
		samp_scale(1), lat_scale(1), long_scale(1), height_scale(1)
{	
	for (int i = 0; i < 20; ++i)
	{
		line_num_coeff[i] = 0.0;
		line_den_coeff[i] = 0.0;
		samp_num_coeff[i] = 0.0;
		samp_den_coeff[i] = 0.0;
	}
    

    CRPC::resetAffine();
	this->refSystem.setToWGS1984();
	this->refSystem.setCoordinateType(eGEOGRAPHIC);	
}


/**
 * contructor from existing rpc
 * @param rpc
 */
CRPC::CRPC(const CRPC &rpc) : CSensorModel(rpc),config(rpc.config)
{
    this->line_num_coeff[0]  = rpc.line_num_coeff[0]; //
    this->line_num_coeff[1]  = rpc.line_num_coeff[1]; 
    this->line_num_coeff[2]  = rpc.line_num_coeff[2]; 
    this->line_num_coeff[3]  = rpc.line_num_coeff[3]; 
    this->line_num_coeff[4]  = rpc.line_num_coeff[4]; 
    this->line_num_coeff[5]  = rpc.line_num_coeff[5]; 
    this->line_num_coeff[6]  = rpc.line_num_coeff[6]; 
    this->line_num_coeff[7]  = rpc.line_num_coeff[7]; 
    this->line_num_coeff[8]  = rpc.line_num_coeff[8]; 
    this->line_num_coeff[9]  = rpc.line_num_coeff[9]; 
    this->line_num_coeff[10] = rpc.line_num_coeff[10];
    this->line_num_coeff[11] = rpc.line_num_coeff[11];
    this->line_num_coeff[12] = rpc.line_num_coeff[12];
    this->line_num_coeff[13] = rpc.line_num_coeff[13];
    this->line_num_coeff[14] = rpc.line_num_coeff[14];
    this->line_num_coeff[15] = rpc.line_num_coeff[15];
    this->line_num_coeff[16] = rpc.line_num_coeff[16];
    this->line_num_coeff[17] = rpc.line_num_coeff[17];
    this->line_num_coeff[18] = rpc.line_num_coeff[18];
    this->line_num_coeff[19] = rpc.line_num_coeff[19];

    this->line_den_coeff[0]  = rpc.line_den_coeff[0];
    this->line_den_coeff[1]  = rpc.line_den_coeff[1];
    this->line_den_coeff[2]  = rpc.line_den_coeff[2];
    this->line_den_coeff[3]  = rpc.line_den_coeff[3];
    this->line_den_coeff[4]  = rpc.line_den_coeff[4];
    this->line_den_coeff[5]  = rpc.line_den_coeff[5];
    this->line_den_coeff[6]  = rpc.line_den_coeff[6];
    this->line_den_coeff[7]  = rpc.line_den_coeff[7];
    this->line_den_coeff[8]  = rpc.line_den_coeff[8];
    this->line_den_coeff[9]  = rpc.line_den_coeff[9];
    this->line_den_coeff[10] = rpc.line_den_coeff[10];
    this->line_den_coeff[11] = rpc.line_den_coeff[11];
    this->line_den_coeff[12] = rpc.line_den_coeff[12];
    this->line_den_coeff[13] = rpc.line_den_coeff[13];
    this->line_den_coeff[14] = rpc.line_den_coeff[14];
    this->line_den_coeff[15] = rpc.line_den_coeff[15];
    this->line_den_coeff[16] = rpc.line_den_coeff[16];
    this->line_den_coeff[17] = rpc.line_den_coeff[17];
    this->line_den_coeff[18] = rpc.line_den_coeff[18];
    this->line_den_coeff[19] = rpc.line_den_coeff[19];

    this->samp_num_coeff[0]  = rpc.samp_num_coeff[0];
    this->samp_num_coeff[1]  = rpc.samp_num_coeff[1];
    this->samp_num_coeff[2]  = rpc.samp_num_coeff[2];
    this->samp_num_coeff[3]  = rpc.samp_num_coeff[3];
    this->samp_num_coeff[4]  = rpc.samp_num_coeff[4];
    this->samp_num_coeff[5]  = rpc.samp_num_coeff[5];
    this->samp_num_coeff[6]  = rpc.samp_num_coeff[6];
    this->samp_num_coeff[7]  = rpc.samp_num_coeff[7];
    this->samp_num_coeff[8]  = rpc.samp_num_coeff[8];
    this->samp_num_coeff[9]  = rpc.samp_num_coeff[9];
    this->samp_num_coeff[10] = rpc.samp_num_coeff[10];
    this->samp_num_coeff[11] = rpc.samp_num_coeff[11];
    this->samp_num_coeff[12] = rpc.samp_num_coeff[12];
    this->samp_num_coeff[13] = rpc.samp_num_coeff[13];
    this->samp_num_coeff[14] = rpc.samp_num_coeff[14];
    this->samp_num_coeff[15] = rpc.samp_num_coeff[15];
    this->samp_num_coeff[16] = rpc.samp_num_coeff[16];
    this->samp_num_coeff[17] = rpc.samp_num_coeff[17];
    this->samp_num_coeff[18] = rpc.samp_num_coeff[18];
    this->samp_num_coeff[19] = rpc.samp_num_coeff[19];

    this->samp_den_coeff[0]  = rpc.samp_den_coeff[0];
    this->samp_den_coeff[1]  = rpc.samp_den_coeff[1];
    this->samp_den_coeff[2]  = rpc.samp_den_coeff[2];
    this->samp_den_coeff[3]  = rpc.samp_den_coeff[3];
    this->samp_den_coeff[4]  = rpc.samp_den_coeff[4];
    this->samp_den_coeff[5]  = rpc.samp_den_coeff[5];
    this->samp_den_coeff[6]  = rpc.samp_den_coeff[6];
    this->samp_den_coeff[7]  = rpc.samp_den_coeff[7];
    this->samp_den_coeff[8]  = rpc.samp_den_coeff[8];
    this->samp_den_coeff[9]  = rpc.samp_den_coeff[9];
    this->samp_den_coeff[10] = rpc.samp_den_coeff[10];
    this->samp_den_coeff[11] = rpc.samp_den_coeff[11];
    this->samp_den_coeff[12] = rpc.samp_den_coeff[12];
    this->samp_den_coeff[13] = rpc.samp_den_coeff[13];
    this->samp_den_coeff[14] = rpc.samp_den_coeff[14];
    this->samp_den_coeff[15] = rpc.samp_den_coeff[15];
    this->samp_den_coeff[16] = rpc.samp_den_coeff[16];
    this->samp_den_coeff[17] = rpc.samp_den_coeff[17];
    this->samp_den_coeff[18] = rpc.samp_den_coeff[18];
    this->samp_den_coeff[19] = rpc.samp_den_coeff[19];

    this->line_off     = rpc.line_off;
    this->samp_off     = rpc.samp_off;
    this->lat_off      = rpc.lat_off;
    this->long_off     = rpc.long_off;
    this->height_off   = rpc.height_off;
    this->line_scale   = rpc.line_scale;
    this->samp_scale   = rpc.samp_scale;
    this->lat_scale    = rpc.lat_scale;
    this->long_scale   = rpc.long_scale;
    this->height_scale = rpc.height_scale;
    this->width        = rpc.width;
    this->height       = rpc.height;

	for (int i = 0; i < this->getParameterCount(addPar); ++i)
	{
		this->ap[i] = rpc.ap[i];
	}
}

CRPC::CRPC(const Matrix &parms, 
		   double line_off,   double samp_off, 
		   double lat_off,    double long_off,   double height_off,
           double line_scale, double samp_scale, 
		   double lat_scale,  double long_scale, double height_scale,
           int width, int height) : CSensorModel(true, "RPC",0, 0, 0, 0, 0, 0, 7, 3, 2) 
{
    this->line_num_coeff[0]  = parms.element(0, 0);
    this->line_num_coeff[1]  = parms.element(1, 0);
    this->line_num_coeff[2]  = parms.element(2, 0);
    this->line_num_coeff[3]  = parms.element(3, 0);
    this->line_num_coeff[4]  = parms.element(4, 0);
    this->line_num_coeff[5]  = parms.element(5, 0);
    this->line_num_coeff[6]  = parms.element(6, 0);
    this->line_num_coeff[7]  = parms.element(7, 0);
    this->line_num_coeff[8]  = parms.element(8, 0);
    this->line_num_coeff[9]  = parms.element(9, 0);
    this->line_num_coeff[10] = parms.element(10, 0);
    this->line_num_coeff[11] = parms.element(11, 0);
    this->line_num_coeff[12] = parms.element(12, 0);
    this->line_num_coeff[13] = parms.element(13, 0);
    this->line_num_coeff[14] = parms.element(14, 0);
    this->line_num_coeff[15] = parms.element(15, 0);
    this->line_num_coeff[16] = parms.element(16, 0);
    this->line_num_coeff[17] = parms.element(17, 0);
    this->line_num_coeff[18] = parms.element(18, 0);
    this->line_num_coeff[19] = parms.element(19, 0);

    this->line_den_coeff[0]  = parms.element(20, 0);
    this->line_den_coeff[1]  = parms.element(21, 0);
    this->line_den_coeff[2]  = parms.element(22, 0);
    this->line_den_coeff[3]  = parms.element(23, 0);
    this->line_den_coeff[4]  = parms.element(24, 0);
    this->line_den_coeff[5]  = parms.element(25, 0);
    this->line_den_coeff[6]  = parms.element(26, 0);
    this->line_den_coeff[7]  = parms.element(27, 0);
    this->line_den_coeff[8]  = parms.element(28, 0);
    this->line_den_coeff[9]  = parms.element(29, 0);
    this->line_den_coeff[10] = parms.element(30, 0);
    this->line_den_coeff[11] = parms.element(31, 0);
    this->line_den_coeff[12] = parms.element(32, 0);
    this->line_den_coeff[13] = parms.element(33, 0);
    this->line_den_coeff[14] = parms.element(34, 0);
    this->line_den_coeff[15] = parms.element(35, 0);
    this->line_den_coeff[16] = parms.element(36, 0);
    this->line_den_coeff[17] = parms.element(37, 0);
    this->line_den_coeff[18] = parms.element(38, 0);
    this->line_den_coeff[19] = parms.element(39, 0);

    this->samp_num_coeff[0]  = parms.element(40, 0);
    this->samp_num_coeff[1]  = parms.element(41, 0);
    this->samp_num_coeff[2]  = parms.element(42, 0);
    this->samp_num_coeff[3]  = parms.element(43, 0);
    this->samp_num_coeff[4]  = parms.element(44, 0);
    this->samp_num_coeff[5]  = parms.element(45, 0);
    this->samp_num_coeff[6]  = parms.element(46, 0);
    this->samp_num_coeff[7]  = parms.element(47, 0);
    this->samp_num_coeff[8]  = parms.element(48, 0);
    this->samp_num_coeff[9]  = parms.element(49, 0);
    this->samp_num_coeff[10] = parms.element(50, 0);
    this->samp_num_coeff[11] = parms.element(51, 0);
    this->samp_num_coeff[12] = parms.element(52, 0);
    this->samp_num_coeff[13] = parms.element(53, 0);
    this->samp_num_coeff[14] = parms.element(54, 0);
    this->samp_num_coeff[15] = parms.element(55, 0);
    this->samp_num_coeff[16] = parms.element(56, 0);
    this->samp_num_coeff[17] = parms.element(57, 0);
    this->samp_num_coeff[18] = parms.element(58, 0);
    this->samp_num_coeff[19] = parms.element(59, 0);

    this->samp_den_coeff[0]  = parms.element(60, 0);
    this->samp_den_coeff[1]  = parms.element(61, 0);
    this->samp_den_coeff[2]  = parms.element(62, 0);
    this->samp_den_coeff[3]  = parms.element(63, 0);
    this->samp_den_coeff[4]  = parms.element(64, 0);
    this->samp_den_coeff[5]  = parms.element(65, 0);
    this->samp_den_coeff[6]  = parms.element(66, 0);
    this->samp_den_coeff[7]  = parms.element(67, 0);
    this->samp_den_coeff[8]  = parms.element(68, 0);
    this->samp_den_coeff[9]  = parms.element(69, 0);
    this->samp_den_coeff[10] = parms.element(70, 0);
    this->samp_den_coeff[11] = parms.element(71, 0);
    this->samp_den_coeff[12] = parms.element(72, 0);
    this->samp_den_coeff[13] = parms.element(73, 0);
    this->samp_den_coeff[14] = parms.element(74, 0);
    this->samp_den_coeff[15] = parms.element(75, 0);
    this->samp_den_coeff[16] = parms.element(76, 0);
    this->samp_den_coeff[17] = parms.element(77, 0);
    this->samp_den_coeff[18] = parms.element(78, 0);
    this->samp_den_coeff[19] = parms.element(79, 0);

    this->line_off = line_off;
    this->samp_off = samp_off;
    this->lat_off = lat_off;
    this->long_off = long_off;
    this->height_off = height_off;
    this->line_scale = line_scale;
    this->samp_scale = samp_scale;
    this->lat_scale = lat_scale;
    this->long_scale = long_scale;
    this->height_scale = height_scale;
    this->width = width;
    this->height = height;

    this->resetAffine();

	this->refSystem.setToWGS1984();
	this->refSystem.setCoordinateType(eGEOGRAPHIC);	
	this->refSystem.setUTMZone(this->lat_off, this->long_off);
	
    this->informListeners_elementsChanged();
}

CRPC::~CRPC()
{
	int here = 1;
}

void CRPC::copy(const CRPC &rpc)
{
	initFromModel(rpc);
    this->line_num_coeff[0]  = rpc.line_num_coeff[0];
    this->line_num_coeff[1]  = rpc.line_num_coeff[1]; 
    this->line_num_coeff[2]  = rpc.line_num_coeff[2]; 
    this->line_num_coeff[3]  = rpc.line_num_coeff[3]; 
    this->line_num_coeff[4]  = rpc.line_num_coeff[4]; 
    this->line_num_coeff[5]  = rpc.line_num_coeff[5]; 
    this->line_num_coeff[6]  = rpc.line_num_coeff[6]; 
    this->line_num_coeff[7]  = rpc.line_num_coeff[7]; 
    this->line_num_coeff[8]  = rpc.line_num_coeff[8]; 
    this->line_num_coeff[9]  = rpc.line_num_coeff[9]; 
    this->line_num_coeff[10] = rpc.line_num_coeff[10];
    this->line_num_coeff[11] = rpc.line_num_coeff[11];
    this->line_num_coeff[12] = rpc.line_num_coeff[12];
    this->line_num_coeff[13] = rpc.line_num_coeff[13];
    this->line_num_coeff[14] = rpc.line_num_coeff[14];
    this->line_num_coeff[15] = rpc.line_num_coeff[15];
    this->line_num_coeff[16] = rpc.line_num_coeff[16];
    this->line_num_coeff[17] = rpc.line_num_coeff[17];
    this->line_num_coeff[18] = rpc.line_num_coeff[18];
    this->line_num_coeff[19] = rpc.line_num_coeff[19];

    this->line_den_coeff[0]  = rpc.line_den_coeff[0];
    this->line_den_coeff[1]  = rpc.line_den_coeff[1];
    this->line_den_coeff[2]  = rpc.line_den_coeff[2];
    this->line_den_coeff[3]  = rpc.line_den_coeff[3];
    this->line_den_coeff[4]  = rpc.line_den_coeff[4];
    this->line_den_coeff[5]  = rpc.line_den_coeff[5];
    this->line_den_coeff[6]  = rpc.line_den_coeff[6];
    this->line_den_coeff[7]  = rpc.line_den_coeff[7];
    this->line_den_coeff[8]  = rpc.line_den_coeff[8];
    this->line_den_coeff[9]  = rpc.line_den_coeff[9];
    this->line_den_coeff[10] = rpc.line_den_coeff[10];
    this->line_den_coeff[11] = rpc.line_den_coeff[11];
    this->line_den_coeff[12] = rpc.line_den_coeff[12];
    this->line_den_coeff[13] = rpc.line_den_coeff[13];
    this->line_den_coeff[14] = rpc.line_den_coeff[14];
    this->line_den_coeff[15] = rpc.line_den_coeff[15];
    this->line_den_coeff[16] = rpc.line_den_coeff[16];
    this->line_den_coeff[17] = rpc.line_den_coeff[17];
    this->line_den_coeff[18] = rpc.line_den_coeff[18];
    this->line_den_coeff[19] = rpc.line_den_coeff[19];

    this->samp_num_coeff[0]  = rpc.samp_num_coeff[0];
    this->samp_num_coeff[1]  = rpc.samp_num_coeff[1];
    this->samp_num_coeff[2]  = rpc.samp_num_coeff[2];
    this->samp_num_coeff[3]  = rpc.samp_num_coeff[3];
    this->samp_num_coeff[4]  = rpc.samp_num_coeff[4];
    this->samp_num_coeff[5]  = rpc.samp_num_coeff[5];
    this->samp_num_coeff[6]  = rpc.samp_num_coeff[6];
    this->samp_num_coeff[7]  = rpc.samp_num_coeff[7];
    this->samp_num_coeff[8]  = rpc.samp_num_coeff[8];
    this->samp_num_coeff[9]  = rpc.samp_num_coeff[9];
    this->samp_num_coeff[10] = rpc.samp_num_coeff[10];
    this->samp_num_coeff[11] = rpc.samp_num_coeff[11];
    this->samp_num_coeff[12] = rpc.samp_num_coeff[12];
    this->samp_num_coeff[13] = rpc.samp_num_coeff[13];
    this->samp_num_coeff[14] = rpc.samp_num_coeff[14];
    this->samp_num_coeff[15] = rpc.samp_num_coeff[15];
    this->samp_num_coeff[16] = rpc.samp_num_coeff[16];
    this->samp_num_coeff[17] = rpc.samp_num_coeff[17];
    this->samp_num_coeff[18] = rpc.samp_num_coeff[18];
    this->samp_num_coeff[19] = rpc.samp_num_coeff[19];

    this->samp_den_coeff[0]  = rpc.samp_den_coeff[0];
    this->samp_den_coeff[1]  = rpc.samp_den_coeff[1];
    this->samp_den_coeff[2]  = rpc.samp_den_coeff[2];
    this->samp_den_coeff[3]  = rpc.samp_den_coeff[3];
    this->samp_den_coeff[4]  = rpc.samp_den_coeff[4];
    this->samp_den_coeff[5]  = rpc.samp_den_coeff[5];
    this->samp_den_coeff[6]  = rpc.samp_den_coeff[6];
    this->samp_den_coeff[7]  = rpc.samp_den_coeff[7];
    this->samp_den_coeff[8]  = rpc.samp_den_coeff[8];
    this->samp_den_coeff[9]  = rpc.samp_den_coeff[9];
    this->samp_den_coeff[10] = rpc.samp_den_coeff[10];
    this->samp_den_coeff[11] = rpc.samp_den_coeff[11];
    this->samp_den_coeff[12] = rpc.samp_den_coeff[12];
    this->samp_den_coeff[13] = rpc.samp_den_coeff[13];
    this->samp_den_coeff[14] = rpc.samp_den_coeff[14];
    this->samp_den_coeff[15] = rpc.samp_den_coeff[15];
    this->samp_den_coeff[16] = rpc.samp_den_coeff[16];
    this->samp_den_coeff[17] = rpc.samp_den_coeff[17];
    this->samp_den_coeff[18] = rpc.samp_den_coeff[18];
    this->samp_den_coeff[19] = rpc.samp_den_coeff[19];

    this->line_off     = rpc.line_off;
    this->samp_off     = rpc.samp_off;
    this->lat_off      = rpc.lat_off;
    this->long_off     = rpc.long_off;
    this->height_off   = rpc.height_off;
    this->line_scale   = rpc.line_scale;
    this->samp_scale   = rpc.samp_scale;
    this->lat_scale    = rpc.lat_scale;
    this->long_scale   = rpc.long_scale;
    this->height_scale = rpc.height_scale;
    this->width        = rpc.width;
    this->height       = rpc.height;

	this->config = rpc.config;

    for (int i = 0; i < this->getParameterCount(addPar); ++i)
	{
		this->ap[i] = rpc.ap[i];
	}

	this->refSystem.setToWGS1984();
	this->refSystem.setCoordinateType(eGEOGRAPHIC);	
	this->refSystem.setUTMZone(this->lat_off, this->long_off);
	
	this->reduction = rpc.reduction;

    this->informListeners_elementsChanged();
}

CXYZPoint CRPC::getOffset() const
{
	CXYZPoint offset(lat_off, long_off, height_off);
	return offset;
};


/**
 * Sets rpc parameter from Matrix
 * @param parms
 */
void CRPC::setAll(const Matrix &parms)
{
    this->hasValues = true;

    this->line_off     = parms.element(0, 0);
    this->samp_off     = parms.element(1, 0);
    this->lat_off      = parms.element(2, 0);
    this->long_off     = parms.element(3, 0);;
    this->height_off   = parms.element(4, 0);
    this->line_scale   = parms.element(5, 0);
    this->samp_scale   = parms.element(6, 0);
    this->lat_scale    = parms.element(7, 0);
    this->long_scale   = parms.element(8, 0);
    this->height_scale = parms.element(9, 0);

    this->line_num_coeff[0]  = parms.element(10, 0);
    this->line_num_coeff[1]  = parms.element(11, 0);
    this->line_num_coeff[2]  = parms.element(12, 0);
    this->line_num_coeff[3]  = parms.element(13, 0);
    this->line_num_coeff[4]  = parms.element(14, 0);
    this->line_num_coeff[5]  = parms.element(15, 0);
    this->line_num_coeff[6]  = parms.element(16, 0);
    this->line_num_coeff[7]  = parms.element(17, 0);
    this->line_num_coeff[8]  = parms.element(18, 0);
    this->line_num_coeff[9]  = parms.element(19, 0);
    this->line_num_coeff[10] = parms.element(20, 0);
    this->line_num_coeff[11] = parms.element(21, 0);
    this->line_num_coeff[12] = parms.element(22, 0);
    this->line_num_coeff[13] = parms.element(23, 0);
    this->line_num_coeff[14] = parms.element(24, 0);
    this->line_num_coeff[15] = parms.element(25, 0);
    this->line_num_coeff[16] = parms.element(26, 0);
    this->line_num_coeff[17] = parms.element(27, 0);
    this->line_num_coeff[18] = parms.element(28, 0);
    this->line_num_coeff[19] = parms.element(29, 0);
    this->line_den_coeff[0]  = parms.element(30, 0);
    this->line_den_coeff[1]  = parms.element(31, 0);
    this->line_den_coeff[2]  = parms.element(32, 0);
    this->line_den_coeff[3]  = parms.element(33, 0);
    this->line_den_coeff[4]  = parms.element(34, 0);
    this->line_den_coeff[5]  = parms.element(35, 0);
    this->line_den_coeff[6]  = parms.element(36, 0);
    this->line_den_coeff[7]  = parms.element(37, 0);
    this->line_den_coeff[8]  = parms.element(38, 0);
    this->line_den_coeff[9]  = parms.element(39, 0);
    this->line_den_coeff[10] = parms.element(40, 0);
    this->line_den_coeff[11] = parms.element(41, 0);
    this->line_den_coeff[12] = parms.element(42, 0);
    this->line_den_coeff[13] = parms.element(43, 0);
    this->line_den_coeff[14] = parms.element(44, 0);
    this->line_den_coeff[15] = parms.element(45, 0);
    this->line_den_coeff[16] = parms.element(46, 0);
    this->line_den_coeff[17] = parms.element(47, 0);
    this->line_den_coeff[18] = parms.element(48, 0);
    this->line_den_coeff[19] = parms.element(49, 0);

    this->samp_num_coeff[0]  = parms.element(50, 0);
    this->samp_num_coeff[1]  = parms.element(51, 0);
    this->samp_num_coeff[2]  = parms.element(52, 0);
    this->samp_num_coeff[3]  = parms.element(53, 0);
    this->samp_num_coeff[4]  = parms.element(54, 0);
    this->samp_num_coeff[5]  = parms.element(55, 0);
    this->samp_num_coeff[6]  = parms.element(56, 0);
    this->samp_num_coeff[7]  = parms.element(57, 0);
    this->samp_num_coeff[8]  = parms.element(58, 0);
    this->samp_num_coeff[9]  = parms.element(59, 0);
    this->samp_num_coeff[10] = parms.element(60, 0);
    this->samp_num_coeff[11] = parms.element(61, 0);
    this->samp_num_coeff[12] = parms.element(62, 0);
    this->samp_num_coeff[13] = parms.element(63, 0);
    this->samp_num_coeff[14] = parms.element(64, 0);
    this->samp_num_coeff[15] = parms.element(65, 0);
    this->samp_num_coeff[16] = parms.element(66, 0);
    this->samp_num_coeff[17] = parms.element(67, 0);
    this->samp_num_coeff[18] = parms.element(68, 0);
    this->samp_num_coeff[19] = parms.element(69, 0);

    this->samp_den_coeff[0]  = parms.element(70, 0);
    this->samp_den_coeff[1]  = parms.element(71, 0);
    this->samp_den_coeff[2]  = parms.element(72, 0);
    this->samp_den_coeff[3]  = parms.element(73, 0);
    this->samp_den_coeff[4]  = parms.element(74, 0);
    this->samp_den_coeff[5]  = parms.element(75, 0);
    this->samp_den_coeff[6]  = parms.element(76, 0);
    this->samp_den_coeff[7]  = parms.element(77, 0);
    this->samp_den_coeff[8]  = parms.element(78, 0);
    this->samp_den_coeff[9]  = parms.element(79, 0);
    this->samp_den_coeff[10] = parms.element(80, 0);
    this->samp_den_coeff[11] = parms.element(81, 0);
    this->samp_den_coeff[12] = parms.element(82, 0);
    this->samp_den_coeff[13] = parms.element(83, 0);
    this->samp_den_coeff[14] = parms.element(84, 0);
    this->samp_den_coeff[15] = parms.element(85, 0);
    this->samp_den_coeff[16] = parms.element(86, 0);
    this->samp_den_coeff[17] = parms.element(87, 0);
    this->samp_den_coeff[18] = parms.element(88, 0);
    this->samp_den_coeff[19] = parms.element(89, 0);

	this->refSystem.setUTMZone(this->lat_off, this->long_off);

    this->informListeners_elementsChanged();
    
}

void CRPC::findMatrices(const C2DPointArray &imgPtsRed, const C3DPointArray &objPtsRed, Matrix &M, Matrix &N, Matrix &L, Matrix &S)
{
	unsigned int nUnknNum = 20;
	for (int i = 0; i < imgPtsRed.GetSize(); i++)
	{
		C2DPoint  *pImg = imgPtsRed.GetAt(i);
		C3DPoint *pObj = objPtsRed.GetAt(i);

		double U = pObj->x,   V = pObj->y,    W = pObj->z;
        double VU  = V * U,   VV = V * V,   UU = U * U,   WW = W * W;
		
		// addition
		M.element(i, 0) = N.element(i, 0) = 1.0;
		M.element(i, 1) = N.element(i, 1) = V;
		M.element(i, 2) = N.element(i, 2) = U;
		M.element(i, 3) = N.element(i, 3) = W;
		M.element(i, 4) = N.element(i, 4) = VU;
		M.element(i, 5) = N.element(i, 5) = V * W;
		M.element(i, 6) = N.element(i, 6) = U * W;
		M.element(i, 7) = N.element(i, 7) = VV;
		M.element(i, 8) = N.element(i, 8) = UU;
		M.element(i, 9) = N.element(i, 9) = WW;
		M.element(i,10) = N.element(i,10) = W * VU;
		M.element(i,11) = N.element(i,11) = V * VV;
		M.element(i,12) = N.element(i,12) = V * UU;	
		M.element(i,13) = N.element(i,13) = V * WW;
		M.element(i,14) = N.element(i,14) = U * VV;
		M.element(i,15) = N.element(i,15) = U * UU;
		M.element(i,16) = N.element(i,16) = U * WW;
		M.element(i,17) = N.element(i,17) = W * VV;
		M.element(i,18) = N.element(i,18) = W * UU;
		M.element(i,19) = N.element(i,19) = W * WW;
		
		for (unsigned int k1 = 1, k2 = nUnknNum; k1 < nUnknNum; ++k1, ++k2)
		{
			double d1 = M.element(i,k1); // delete
			M.element(i,k2) = -pImg->y*M.element(i,k1);
			double d2 = M.element(i,k2);  // delete
			N.element(i,k2) = -pImg->x*N.element(i,k1);
		}
		
		double d3 = L.element(i,0); // delete
		L.element(i,0) = pImg->y;
		double d4 = L.element(i,0); // delete
		S.element(i,0) = pImg->x;
		// addition		
	};
}
bool CRPC::iterateRPCadjustment(const Matrix &M, 
								const Matrix &N,
								const Matrix &L,
								const Matrix &S,
								const unsigned int &nData,
								doubles &directWeightsL,
								doubles &directWeightsS)
{
	// Implementation from the following papers
	//[1] Tao, V., Hu, Y., 2001a. A comprehensive study on the rational function model for photogrammetric processing, PE&RS, 67(12), pp. 1347-1357.
    //[2] Tao, C.V., and Y. Hu, 2000a. Investigation on the Rational Function Model, Proceedings of 2000 ASPRS Annual Convention (CD ROM), 24-26 May, Washington, D.C., USA.
    //[3] Tao, C.V., and Y. Hu, 2001b. 3-D reconstruction algorithms with the rational function model and their applications for IKONOS stereo imagery, Proceedings of Joint ISPRS Workshop on "High Resolution Mapping from Space 2001" (CD ROM), 19-21 September, Hannover, Germany.
	bool ret = true;
	unsigned int nUnkn = 39;
	unsigned int nUnknNum = 20;
	
	// M: design matrix for Line data
	// N: design matrix for sample data	
	// L: line data matrix 
	// S: sample data matrix
	// nData: number of grid-points
	// directWeightsL: weights of line data
	// directWeightsS: weights of sample data

	Matrix Mtw(nUnkn,nData); // M': transpose of M multiplied by weight matrix for line data
	Matrix Ntw(nUnkn,nData); // N': transpose of N multiplied by weight matrix for sample data	
	Matrix Lval(nData,1); // column matrix: M.[a0 a1 ... a19 b1 b2 ... b19]^t
	Matrix Sval(nData,1); // column matrix: N.[c0 c1 ... c19 d1 d2 ... d19]^t
	SymmetricMatrix Sm(nUnkn); // for M'WM + h^2E, E is an identity matrix
	SymmetricMatrix Sn(nUnkn); // for N'WN + h^2E, E is an identity matrix
	Matrix Rm(nUnkn,1); // M'WR(L-Lval), W = directWeightsL 
	Matrix Rn(nUnkn,1); // N'WC(S-Sval), W = directWeightsS
	Mtw = 0.0; Ntw = 0.0;
	Lval = 0.0; Sval = 0.0;
	Sm = 0.0; Sn = 0.0;
	Rm = 0.0; Rn = 0.0;	


	// Tikhonov regularization
	double h = 0.001;
	doubles h2E(nUnkn,h*h);	

	for (unsigned int i = 0; i < nData; i++)
	{	
		for (unsigned int j = 0; j < 20; ++j)
		{
			// calculate M^t.W
			Mtw.element( j,i) = M.element(i, j) * directWeightsL[i];
		
			// calculate N^t.W
			Ntw.element( j,i) = N.element(i, j) * directWeightsS[i];

			// Lval = M.[a0 a1 ... a19 b1 b2 ... b19]^t
			Lval.element(i, 0) += M.element(i, j) * (this->line_num_coeff[ j]);

			// Sval = N.[c0 c1 ... c19 d1 d2 ... d19]^t
			Sval.element(i, 0) += N.element(i, j) * (this->samp_num_coeff[ j]);
		}

		for (unsigned int k1 = 1, k2 = nUnknNum; k1 < nUnknNum; ++k1, ++k2)
		{	
			// calculate M^t.W
			Mtw.element(k2,i) = M.element(i,k2) * directWeightsL[i];
			// calculate N^t.W
			Ntw.element(k2,i) = N.element(i,k2) * directWeightsS[i];
			// Lval = M.[a0 a1 ... a19 b1 b2 ... b19]^t
			Lval.element(i,0) += M.element(i,k2) * (this->line_den_coeff[ k1]);
			// Sval = N.[c0 c1 ... c19 d1 d2 ... d19]^t
			Sval.element(i,0) += N.element(i,k2) * (this->samp_den_coeff[ k1]);
		}		
	};

	// calculate error in line and sample data (actual - measured)
	Matrix Lr = L-Lval; // column matrix of line residuals
	Matrix Sr = S-Sval; // column matrix of sample residuals
	for (int r = 0; r < Sm.Nrows(); ++r)
	{
		for (int c = r; c < Sm.Ncols(); ++c)
		{
			for (int e = 0; e < M.Nrows(); ++e)
			{
				Sm.element(r,c) += Mtw.element(r,e) * M.element(e,c); // finding symmetric matrix Mtw*M in the normal equation
				Sn.element(r,c) += Ntw.element(r,e) * N.element(e,c); // finding symmetric matrix Ntw*N in the normal equation
			}
			if (r == c)
			{
				Sm.element(r,c) += h2E[r]; // apply Tikhonov regularization
				Sn.element(r,c) += h2E[r]; // apply Tikhonov regularization
			}
		}
		for (int e = 0; e < M.Nrows(); ++e)
		{
			Rm.element(r,0) += Mtw.element(r,e)*Lr.element(e,0); // right part of the LSM for line
			Rn.element(r,0) += Ntw.element(r,e)*Sr.element(e,0); // right part of the LSM for sample
		}
	}	

	Try
	{
		Matrix Qm = Sm.i(); // inversion of Sm
		Matrix Qn = Sn.i();
		Matrix Xl = Qm * Rm;
        Matrix Xs = Qn * Rn;

		this->line_num_coeff[0] += Xl.element(0, 0);
		this->samp_num_coeff[0] += Xs.element(0, 0);

		for (unsigned int j1 = 1, j2 = nUnknNum; j1 < nUnknNum; ++j1, ++j2)
		{
			assert(j1 < 20);
			assert(j2 < (unsigned int)(Xl.Nrows()));
			assert(j2 < (unsigned int)(Xs.Nrows()));
	
			this->line_num_coeff[j1] += Xl.element(j1, 0);
			this->line_den_coeff[j1] += Xl.element(j2, 0);
			this->samp_num_coeff[j1] += Xs.element(j1, 0);
			this->samp_den_coeff[j1] += Xs.element(j2, 0);
		}
	} 
	CatchAll //(char* error) 
	{
		ret = false;
	}

	// update weights
	for (unsigned int i = 0; i < nData; i++)
	{
		double sumL = 1.0;
		double sumS = 1.0;
		for (int j = 1; j<20; j++)
		{
			sumL += M.element(i,j)*line_den_coeff[j];
			sumS += N.element(i,j)*samp_den_coeff[j];
		}
		directWeightsL[i] = 1/(sumL*sumL);
		directWeightsS[i] = 1/(sumS*sumS);
	}

	return ret;
};

double CRPC::init(const C2DPointArray &imgPts, const C3DPointArray &objPts, int Width, int Height)
{
	bool OK = true; // flag indicating an error; 
	double rms = -1;

	// first check whether the point counts are identical and whether there are at least 40 image points
	if ((imgPts.GetSize() != objPts.GetSize()) && (imgPts.GetSize() < 40)) OK = false;
	else
	{
		// Just to make sure: if anything goes wrong, the old version of the RPCs is maintained
		CRPC copyRPC(*this); 

		// Initialise the bias correction parameters by 0
		for (int i = 0; i < 7; ++i)  ap[i] = 0.0;


		// Initialise some RPC parameters
		this->hasValues  = true;
		this->width      = Width;
		this->height     = Height;

		// Initialise offset parameters in image space
		this->samp_off   = 0.0;
		this->line_off   = 0.0;		

		// Initialise offset parameters in object space
		this->lat_off    = 0.0f;
		this->long_off   = 0.0f;
		this->height_off = 0.0f;

		double sampMin	=	FLT_MAX;
		double lineMin	=	FLT_MAX;
		double sampMax	=	-FLT_MAX;
		double lineMax	=	-FLT_MAX;
		
		double latMin    =  FLT_MAX;
		double longMin   =  FLT_MAX;
		double heightMin =  FLT_MAX;
		double latMax    = -FLT_MAX;
		double longMax   = -FLT_MAX;
		double heightMax = -FLT_MAX;
		
		for (int i = 0; i < imgPts.GetSize(); ++i)
		{
			C2DPoint  *pImg = imgPts.GetAt(i);
			if (pImg->x < sampMin)   sampMin = pImg->x;
			if (pImg->x > sampMax)   sampMax = pImg->x;
			if (pImg->y < lineMin)   lineMin = pImg->y;
			if (pImg->y > lineMax)   lineMax = pImg->y;
			this->samp_off	+= pImg->x;
			this->line_off  += pImg->y;

			C3DPoint *pObj = objPts.GetAt(i);
			if (pObj->x < latMin)   latMin = pObj->x;
			if (pObj->x > latMax)   latMax = pObj->x;
			if (pObj->y < longMin)   longMin = pObj->y;
			if (pObj->y > longMax)   longMax = pObj->y;
			if (pObj->z < heightMin) heightMin = pObj->z;
			if (pObj->z > heightMax) heightMax = pObj->z;
 			this->lat_off    += pObj->x;
			this->long_off   += pObj->y;
			this->height_off += pObj->z;
		}
		
		// find offset parameters in image space
		this->samp_off	/= (double) imgPts.GetSize();
		this->line_off  /= (double) imgPts.GetSize();

		// find offset parameters in object space
		this->lat_off    /= (double) imgPts.GetSize();
		this->long_off   /= (double) imgPts.GetSize();
		this->height_off /= (double) imgPts.GetSize();
		
		// find scale parameters in image space
		if (fabs(sampMin - this->samp_off) > fabs(sampMax - this->samp_off))
			this->samp_scale = fabs(sampMin - this->samp_off);
		else
			this->samp_scale = fabs(sampMax - this->samp_off);
		
		if (fabs(lineMin - this->line_off) > fabs(lineMax - this->line_off))
			this->line_scale = fabs(lineMin - this->line_off);
		else
			this->line_scale = fabs(lineMax - this->line_off);

		// find scale parameters in object space
		if (fabs(latMin - this->lat_off) > fabs(latMax - this->lat_off))
			this->lat_scale = fabs(latMin - this->lat_off);
		else
			this->lat_scale = fabs(latMax - this->lat_off);
		
		if (fabs(longMin - this->long_off) > fabs(longMax - this->long_off))
			this->long_scale = fabs(longMin - this->long_off);
		else
			this->long_scale = fabs(longMax - this->long_off);

		if (fabs(heightMin - this->height_off) > fabs(heightMax - this->height_off))
			this->height_scale = fabs(heightMin - this->height_off);
		else
			this->height_scale = fabs(heightMax - this->height_off);


		// Now all offset and scale parameters are initialised;
		// The image and object points are normalized using the
		// offset and scale parameters; the normalized image coordinates
		// are used to determine the RPC parameters

		C2DPointArray imgPtsRed; // array of normalized image points; all coordinates are between -1.0 and 1.0
		C3DPointArray objPtsRed; // array of normalized object points; all coordinates are between -1.0 and 1.0

		for (int i = 0; i < imgPts.GetSize(); ++i) // image and object points are normalized
		{
			C2DPoint *pImgOrig = imgPts.GetAt(i);
			C3DPoint *pObjOrig = objPts.GetAt(i);
			C2DPoint *pImgRed  = imgPtsRed.Add();
			C3DPoint *pObjRed  = objPtsRed.Add();
			this->normalizeXYZ(*pObjRed, *pObjOrig);
			this->normalizeXY(*pImgRed, *pImgOrig);			
		}

		// Initialise all RPC coefficients by 0.0
		for (int i = 0; i < 20; ++i)
		{
			this->line_num_coeff[i] = 0.0;
			this->line_den_coeff[i] = 0.0;
			this->samp_num_coeff[i] = 0.0;
			this->samp_den_coeff[i] = 0.0;
		}

		// However, the constant terms in both denominators have to be initialised by 1.0
		this->line_den_coeff[0] = 1.0;		
		this->samp_den_coeff[0] = 1.0;
	
		// == addition == define the intitial weight matrix as an identity matrix
		unsigned int nData = imgPts.GetSize();
		unsigned int nUnkn = 39;
		unsigned int nUnknNum = 20;
		doubles weightsL(nData, 1.0);  // Weights for the line coordinate
		doubles weightsS(nData, 1.0);  // Weights for the sample coordinate
		
		Matrix M(nData,nUnkn); // design matrix for Line data
		Matrix N(nData,nUnkn); // design matrix for sample data	
		Matrix L(nData,1); // column matrix of line data
		Matrix S(nData,1); // column matrix of sample data	
		M = 0.0; N = 0.0;	
		L = 0.0; S = 0.0;
		
		// calculate design and data matrices
		findMatrices(imgPtsRed, objPtsRed, M, N, L, S);
		
		// calculate RPC coefficients and update the weight matrix based on the calculated RPCs
		OK = iterateRPCadjustment(M, N, L, S, nData, weightsL, weightsS); // find RPC coefficient values
		
		// addition
		rms = calculateRMSE(imgPts,objPts);
		int it = 0; int maxIt = 10;
		double rmse_diff = rms;
		while (OK && rmse_diff > 0.00001 && it < maxIt) // we exit if the rmse difference between successive iterations is too small
		{
			OK = iterateRPCadjustment(M, N, L, S, nData, weightsL, weightsS); // find coefficient values iteratively
			double rms1 = calculateRMSE(imgPts,objPts);
			rmse_diff = fabs(rms1-rms);
			rms = rms1;
			it++;			
		}

		if (!OK) 
		{ // if an error was encountered, the copy of the RPCs will be restored
			*this = copyRPC;
			rms = -1.0;
		}
		else
		{
			// initialise stochastic model for bias correction: shifts can be determined
			this->freeShift();
		}
		// addition

	}
	return rms;
}; 

double CRPC::calculateRMSE(const C2DPointArray &imgPts, const C3DPointArray &objPts)
{
	double sse = 0.0; // sum of square error
	CXYPoint transformedPt;

	for (int i = 0; i < imgPts.GetSize(); ++i)
	{
		C2DPoint  *pImg = imgPts.GetAt(i);
		C3DPoint *pObj = objPts.GetAt(i);
		this->transformObj2Obs(transformedPt, *pObj);
		double vx = transformedPt.x - pImg->x;
		double vy = transformedPt.y - pImg->y;
		sse += vx * vx + vy * vy;
	}
	double mse = sse/(double)imgPts.GetSize();
	double rmse = sqrt(mse);
	return rmse;
}

double CRPC::getDenominatorL(double U, double V, double W) const
{
	double U2 = U *  U;
	double V2 = V *  V;
	double W2 = W *  W;

    double Denl =   this->line_den_coeff[0] +
                    this->line_den_coeff[1] * V +
                    this->line_den_coeff[2] * U +
                    this->line_den_coeff[3] * W +
                    this->line_den_coeff[4] * V*U +
                    this->line_den_coeff[5] * V*W +
                    this->line_den_coeff[6] * U*W +
                    this->line_den_coeff[7] * V2 +
                    this->line_den_coeff[8] * U2 +
                    this->line_den_coeff[9] * W2 +
                    this->line_den_coeff[10] * U*V*W +
                    this->line_den_coeff[11] * V*V2 +
                    this->line_den_coeff[12] * V*U2 +
                    this->line_den_coeff[13] * V*W2 +
                    this->line_den_coeff[14] * V2*U +
                    this->line_den_coeff[15] * U*U2 +
                    this->line_den_coeff[16] * U*W2 +
                    this->line_den_coeff[17] * V2*W +
                    this->line_den_coeff[18] * U2*W +
                    this->line_den_coeff[19] * W*W2;
	return Denl;
};

double CRPC::getDenominatorS(double U, double V, double W) const
{
	double U2 = U *  U;
	double V2 = V *  V;
	double W2 = W *  W;

	double Dens =   this->samp_den_coeff[0] +
                    this->samp_den_coeff[1] * V +
                    this->samp_den_coeff[2] * U +
                    this->samp_den_coeff[3] * W +
                    this->samp_den_coeff[4] * V*U +
                    this->samp_den_coeff[5] * V*W +
                    this->samp_den_coeff[6] * U*W +
                    this->samp_den_coeff[7] * V2 +
                    this->samp_den_coeff[8] * U2 +
                    this->samp_den_coeff[9] * W2 +
                    this->samp_den_coeff[10] * U*V*W +
                    this->samp_den_coeff[11] * V*V2 +
                    this->samp_den_coeff[12] * V*U2 +
                    this->samp_den_coeff[13] * V*W2 +
                    this->samp_den_coeff[14] * V2*U +
                    this->samp_den_coeff[15] * U*U2 +
                    this->samp_den_coeff[16] * U*W2 +
                    this->samp_den_coeff[17] * V2*W +
                    this->samp_den_coeff[18] * U2*W +
                    this->samp_den_coeff[19] * W*W2;

	return Dens;
};

double CRPC::getNumeratorL(double U, double V, double W) const
{
	double U2 = U *  U;
	double V2 = V *  V;
	double W2 = W *  W;
	double Numl =   this->line_num_coeff[0] +
                    this->line_num_coeff[1]  * V +
                    this->line_num_coeff[2]  * U +
                    this->line_num_coeff[3]  * W +
                    this->line_num_coeff[4]  * V*U +
                    this->line_num_coeff[5]  * V*W +
                    this->line_num_coeff[6]  * U*W +
                    this->line_num_coeff[7]  * V2 +
                    this->line_num_coeff[8]  * U2 +
                    this->line_num_coeff[9]  * W2 +
                    this->line_num_coeff[10] * U*V*W +
                    this->line_num_coeff[11] * V*V2 +
                    this->line_num_coeff[12] * V*U2 +
                    this->line_num_coeff[13] * V*W2 +
                    this->line_num_coeff[14] * V2*U +
                    this->line_num_coeff[15] * U*U2 +
                    this->line_num_coeff[16] * U*W2 +
                    this->line_num_coeff[17] * V2*W +
                    this->line_num_coeff[18] * U2*W +
                    this->line_num_coeff[19] * W*W2;
	return Numl;
};

double CRPC::getNumeratorS(double U, double V, double W) const
{
	double U2 = U *  U;
	double V2 = V *  V;
	double W2 = W *  W;

	double Nums =   this->samp_num_coeff[0] +
                    this->samp_num_coeff[1] * V +
                    this->samp_num_coeff[2] * U +
                    this->samp_num_coeff[3] * W +
                    this->samp_num_coeff[4] * V*U +
                    this->samp_num_coeff[5] * V*W +
                    this->samp_num_coeff[6] * U*W +
                    this->samp_num_coeff[7] * V2 +
                    this->samp_num_coeff[8] * U2 +
                    this->samp_num_coeff[9] * W2 +
                    this->samp_num_coeff[10] * U*V*W +
                    this->samp_num_coeff[11] * V*V2 +
                    this->samp_num_coeff[12] * V*U2 +
                    this->samp_num_coeff[13] * V*W2 +
                    this->samp_num_coeff[14] * V2*U +
                    this->samp_num_coeff[15] * U*U2 +
                    this->samp_num_coeff[16] * U*W2 +
                    this->samp_num_coeff[17] * V2*W +
                    this->samp_num_coeff[18] * U2*W +
                    this->samp_num_coeff[19] * W*W2;
	return Nums;
};

/**
* Transforms an xyz point to its cooresponding xy point
* in image space
* The xyz point is assumed to be Lat Long Height expressed in the
* appropriate zone for the RPC.
*
* @param xyz The xyz point
* @return the transformed xy point
*/
void CRPC::transformObj2Obs(C2DPoint &obs, const C3DPoint &obj) const
{
    C3DPoint norm;
    
    this->normalizeXYZ(norm, obj);

    double U = norm.x;
    double V = norm.y;
    double W = norm.z;

    double Numl = getNumeratorL(U, V, W);
    double Denl = getDenominatorL(U, V, W);
    double Nums = getNumeratorS(U, V, W);
    double Dens = getDenominatorS(U, V, W);

    double l = Numl/Denl;
    double s = Nums/Dens;

    double L = l*this->line_scale + this->line_off - this->ap[3];
    double S = s*this->samp_scale + this->samp_off - this->ap[0];

	double dL = 0.0;
	double dS = 0.0;
	double delta = 0.0;
	int it = 0;
	do
	{
		double dL1 = - this->ap[4] * (S + dS) - this->ap[5] * (L + dL) - this->ap[6] * sin(getPhi(l));
		double dS1 = - this->ap[1] * (S + dS) - this->ap[2] * (L + dL);
		double ddL = dL1 - dL;
		double ddS = dS1 - dS;
		delta = ddL * ddL + ddS * ddS;
		dL = dL1;
		dS = dS1;
		++it;
	} while ((it < 2) && (delta > 0.00001));

	L += dL;
	S += dS;

    obs.x = S;
	obs.y = L;
}


/**
* Transforms an xyz point to its cooresponding xyz point
* in image space
* The xyz point is assumed to be Lat Long Height expressed in the
* appropriate zone for the RPC.
*
* @param xyz The xyz point
* @return the transformed xyz point
*/
void CRPC::transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const
{
    C3DPoint norm;
    
    this->normalizeXYZ(norm, obj);

    double U = norm.x;
    double V = norm.y;
    double W = norm.z;

	double Numl = getNumeratorL(U, V, W);
    double Denl = getDenominatorL(U, V, W);
    double Nums = getNumeratorS(U, V, W);
    double Dens = getDenominatorS(U, V, W);


    double l = Numl/Denl;
    double s = Nums/Dens;

    double L = l*this->line_scale + this->line_off - this->ap[3];
    double S = s*this->samp_scale + this->samp_off - this->ap[0];

    obs.x = S;
	obs.y = L;
	obs.z = 0.0f;
}

/**
 * monoplots a single point
 * @param  xyz xyzpoint xy xypoint, Height Height
 * @returns true/false depending if monoplot was succesful
 */
bool CRPC::transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const
{
    obj.z = Height;
	obj.x = this->lat_off;
	obj.y = this->long_off;
	
	Matrix lBi;
	Matrix obsMat(1,2);
	obsMat.element(0,0) = obs.x;
	obsMat.element(0,1) = obs.y;
	this->Bi(lBi, obsMat, obj);
	Matrix BtQllBinv = lBi.t() * lBi * 0.000001;
	BtQllBinv = BtQllBinv.i();
	int iter = 0;
	Matrix Qxx;
	double rms = FLT_MAX;
    do
	{
		Matrix lAi, lFi;
		this->AiPoint(lAi, obsMat, obj);
		lAi = lAi.SubMatrix(1,2,1,2);
		this->Fi(lFi, obsMat, obj);
		Matrix Npt = lAi.t() * BtQllBinv;
		Matrix N = Npt * lAi;
		Matrix t = Npt * lFi;
		Qxx = N.i();
		Matrix delta = Qxx * t;
		obj.x -= delta.element(0,0);
		obj.y -= delta.element(1,0);
		rms = lFi.element(0,0) * lFi.element(0,0) + lFi.element(1,0) *  lFi.element(1,0);
		++iter;
	} while ((iter < 20) && (rms > 1e-15));

    return true;
}

/**
 * monoplots a single point
 * @param  xyz xyzpoint xy xypoint, Height Height
 * @returns true/false depending if monoplot was succesful
 */
bool CRPC::transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const
{
	obj.setLabel(obs.getLabel());

	double pZ = 1.0 / (double) sigmaZ;
	pZ *= pZ;

	obj[0] = this->lat_off;
	obj[1] = this->long_off;
    obj[2] = Height;
	
	Matrix lBi;
	Matrix obsMat(1,2);
	obsMat.element(0,0) = obs[0];
	obsMat.element(0,1) = obs[1];
	this->Bi(lBi, obsMat, obj);
	Matrix BtQllBinv = lBi.t() * lBi * 0.000001;
	BtQllBinv = BtQllBinv.i();
	int iter = 0;
	Matrix Qxx;
	double rms = FLT_MAX;
    do
	{
		Matrix lAi, lFi;
		this->AiPoint(lAi, obsMat, obj);
		this->Fi(lFi, obsMat, obj);
		Matrix Npt = lAi.t() * BtQllBinv;
		Matrix N = Npt * lAi;
		Matrix t = Npt * lFi;
		N.element(2,2) += pZ;
		Qxx = N.i();
		Matrix delta = Qxx * t;
		obj[0] -= delta.element(0,0);
		obj[1] -= delta.element(1,0);
		rms = lFi.element(0,0) * lFi.element(0,0) + lFi.element(1,0) *  lFi.element(1,0);
		++iter;
	} while ((iter < 20) && (rms > 1e-15));

	obj.setCovariance(Qxx);

    return true;
}

/**
 * monoplots a single point
 * @param  xyz xyzpoint xy xypoint, Height Height
 * @returns true/false depending if monoplot was succesful
 */
bool CRPC::transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const
{
	C2DPoint obs2D;
	obs2D.x = obs.x;
	obs2D.y = obs.y;
	return transformObs2Obj(obj, obs2D, obj.z);
}


bool CRPC::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const
{
	// dummy implementation!

	return true;
};

bool CRPC::transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const
{
	// dummy implementation!

	return true;
};

/**
* Gets a matrix of the parameters
*
* @return the parameters backed into a matrix
*/
bool CRPC::getAllParameters(Matrix &parms) const
{
    parms.ReSize(99, 1);

    for (int i = 0; i < 20; i++)
        parms.element(i, 0) = line_num_coeff[i];

    for (int i = 0; i < 20; i++)
        parms.element(i+20, 0) = line_den_coeff[i];

    for (int i = 0; i < 20; i++)
        parms.element(i+40, 0) = samp_num_coeff[i];

    for (int i = 0; i < 20; i++)
        parms.element(i+60, 0) = samp_den_coeff[i];

    parms.element(80, 0) = this->line_off;
    parms.element(81, 0) = this->samp_off;
    parms.element(82, 0) = this->lat_off;
    parms.element(83, 0) = this->long_off;
    parms.element(84, 0) = this->height_off;

    parms.element(85, 0) = this->line_scale;
    parms.element(86, 0) = this->samp_scale;
    parms.element(87, 0) = this->lat_scale;
    parms.element(88, 0) = this->long_scale;
    parms.element(89, 0) = this->height_scale;
    parms.element(90, 0) = this->width;
    parms.element(91, 0) = this->height;

    parms.element(92, 0) = this->ap[0];
    parms.element(93, 0) = this->ap[1];
    parms.element(94, 0) = this->ap[2];
    parms.element(95, 0) = this->ap[3];
    parms.element(96, 0) = this->ap[4];
    parms.element(97, 0) = this->ap[5];
    parms.element(98, 0) = this->ap[6];

    return true;
}

bool CRPC::getAdjustableParameters(Matrix &parms) const
{
    parms.ReSize(7, 1);

    parms.element(0, 0) = this->ap[0];
    parms.element(1, 0) = this->ap[1];
    parms.element(2, 0) = this->ap[2];
    parms.element(3, 0) = this->ap[3];
    parms.element(4, 0) = this->ap[4];
    parms.element(5, 0) = this->ap[5];
    parms.element(6, 0) = this->ap[6];

    return true;
}

bool CRPC::insertParametersForAdjustment(Matrix &parms)
{
	if (this->parameterIndex[addPar] >= 0)
	{
		parms.element(this->parameterIndex[addPar]    , 0) = this->ap[0];
		parms.element(this->parameterIndex[addPar] + 1, 0) = this->ap[1];
		parms.element(this->parameterIndex[addPar] + 2, 0) = this->ap[2];
		parms.element(this->parameterIndex[addPar] + 3, 0) = this->ap[3];
		parms.element(this->parameterIndex[addPar] + 4, 0) = this->ap[4];
		parms.element(this->parameterIndex[addPar] + 5, 0) = this->ap[5];
		parms.element(this->parameterIndex[addPar] + 6, 0) = this->ap[6];
	}
	return true;
};

bool CRPC::setParametersFromAdjustment(const Matrix &parms) 
{
	if (this->parameterIndex[addPar] >= 0)
	{
		this->ap[0] = parms.element(this->parameterIndex[addPar]    , 0);
		this->ap[1] = parms.element(this->parameterIndex[addPar] + 1, 0);
		this->ap[2] = parms.element(this->parameterIndex[addPar] + 2, 0);
		this->ap[3] = parms.element(this->parameterIndex[addPar] + 3, 0);
		this->ap[4] = parms.element(this->parameterIndex[addPar] + 4, 0);
		this->ap[5] = parms.element(this->parameterIndex[addPar] + 5, 0);
		this->ap[6] = parms.element(this->parameterIndex[addPar] + 6, 0);
	}

	
    this->informListeners_elementsChanged();

	return true;
}
/**
* Gets a matrix of the parameters
*
*/
Matrix CRPC::getConstants() const
{
    Matrix parms(92, 1);

    for (int i = 0; i < 20; i++)
        parms.element(i, 0) = line_num_coeff[i];

    for (int i = 0; i < 20; i++)
        parms.element(i+20, 0) = line_den_coeff[i];

    for (int i = 0; i < 20; i++)
        parms.element(i+40, 0) = samp_num_coeff[i];

    for (int i = 0; i < 20; i++)
        parms.element(i+60, 0) = samp_den_coeff[i];

    parms.element(80, 0) = this->line_off;
    parms.element(81, 0) = this->samp_off;
    parms.element(82, 0) = this->lat_off;
    parms.element(83, 0) = this->long_off;
    parms.element(84, 0) = this->height_off;

    parms.element(85, 0) = this->line_scale;
    parms.element(86, 0) = this->samp_scale;
    parms.element(87, 0) = this->lat_scale;
    parms.element(88, 0) = this->long_scale;
    parms.element(89, 0) = this->height_scale;
    parms.element(90, 0) = this->width;
    parms.element(91, 0) = this->height;

    return parms;
}

    /**
     *
     */
void CRPC::normalizeXY(C2DPoint &dest, const C2DPoint &src) const
{
    dest.x = (src.x - this->samp_off) / this->samp_scale;
    dest.y = (src.y - this->line_off) / this->line_scale;
}

/**
* "Normalizes" the given xyz point expressed in WGS84 Lat Long Height
* @return The normalized point
*/
void CRPC::normalizeXYZ(C3DPoint &UVW, const C3DPoint &xyz) const
{
    UVW.x = (xyz.x - this->lat_off)    / this->lat_scale;
    UVW.y = (xyz.y - this->long_off)   / this->long_scale;
    UVW.z = (xyz.z - this->height_off) / this->height_scale;
}

/**
    * "Expands" the given Normalized xyz point to WGS84 Lat Long Height
    * @return The expanded point
    */
void CRPC::expandXYZ(C3DPoint &xyz, const C3DPoint &UVW) const
{
    xyz.x = UVW.x * this->lat_scale + this->lat_off;
    xyz.y = UVW.y * this->long_scale + this->long_off;
    xyz.z = UVW.z * this->height_scale + this->height_off;
}

    /**
     * This will reformulate the RPC's to incorporate the AP terms into the standard
     * RPC form.
     */
void CRPC::reformulate()
{
    double sl = this->ap[3]/this->line_scale;

    for (int i = 0; i < 20; i ++)
    {
        this->line_num_coeff[i] -=  this->line_den_coeff[i]*sl;
    }

    double ss = this->ap[0]/this->samp_scale;

    for (int i = 0; i < 20; i ++)
    {
        this->samp_num_coeff[i] -=  this->samp_den_coeff[i]*ss;
    }

    this->ap[0] = 0.;
    this->ap[3] = 0.;

	

    this->informListeners_elementsChanged();
}


/**
 * freeAffine modifies the covariance matrix of the additional parameter
 * setting the weight to "free", except b3, which is fixed. This setting
 * is used when "Affine"-option is choosen
 */
void CRPC::freeAffine()
{
    
	this->config = eRPCAffine;

	this->covariance.ReSize(7,7);
    this->covariance = 0;
    this->covariance.element(0, 0) = RPC_FREE;
    this->covariance.element(1, 1) = RPC_FREE;
    this->covariance.element(2, 2) = RPC_FREE;
    this->covariance.element(3, 3) = RPC_FREE;
    this->covariance.element(4, 4) = RPC_FREE;
    this->covariance.element(5, 5) = RPC_FREE;
    this->covariance.element(6, 6) = RPC_FIXED;

    this->informListeners_elementsChanged();
}


/**
 * Fixes all ap paramters' covariance
 */
void CRPC::fixAll()
{
    this->config = eRPCNone;
	
	this->covariance.ReSize(7,7);
    this->covariance = 0;
    this->covariance.element(0, 0) = RPC_FIXED;
    this->covariance.element(1, 1) = RPC_FIXED;
    this->covariance.element(2, 2) = RPC_FIXED;
    this->covariance.element(3, 3) = RPC_FIXED;
    this->covariance.element(4, 4) = RPC_FIXED;
    this->covariance.element(5, 5) = RPC_FIXED;
    this->covariance.element(6, 6) = RPC_FIXED;

    this->informListeners_elementsChanged();
}

/**
 * Free shift paramters' covariance
 */
void CRPC::freeShift()
{
    this->config = eRPCShift;
	
	this->covariance.ReSize(7,7);
    this->covariance = 0;
    this->covariance.element(0, 0) = RPC_FREE;
    this->covariance.element(1, 1) = RPC_FIXED;
    this->covariance.element(2, 2) = RPC_FIXED;
    this->covariance.element(3, 3) = RPC_FREE;
    this->covariance.element(4, 4) = RPC_FIXED;
    this->covariance.element(5, 5) = RPC_FIXED;
    this->covariance.element(6, 6) = RPC_FIXED;

    this->informListeners_elementsChanged();
}

/**
 * Free shift+drift paramters' covariance
 */
void CRPC::freeShiftDrift()
{
   
	this->config = eRPCShiftDrift;

	this->covariance.ReSize(7,7);
    this->covariance = 0;
    this->covariance.element(0, 0) = RPC_FREE;
    this->covariance.element(1, 1) = RPC_FIXED;
    this->covariance.element(2, 2) = RPC_FREE;
    this->covariance.element(3, 3) = RPC_FREE;
    this->covariance.element(4, 4) = RPC_FIXED;
    this->covariance.element(5, 5) = RPC_FREE;
    this->covariance.element(6, 6) = RPC_FIXED;

    this->informListeners_elementsChanged();
}

/**
 * resetAps modifies the covariance matrix of the additional parameter
 * setting the weight to "freeShift", by calling freeAps and sets additional
 * parameters to 0
 */
void CRPC::resetAffine()
{
	this->freeShift();

    this->ap[0] = 0;
    this->ap[1] = 0;
    this->ap[2] = 0;
    this->ap[3] = 0;
    this->ap[4] = 0;
    this->ap[5] = 0;
    this->ap[6] = 0;

    this->informListeners_elementsChanged();
}

void CRPC::resetSensor()
{
    for (int i = 0; i < 20; i++)
    {
		this->line_num_coeff[i]  = 0.0;
		this->line_den_coeff[i]  = 0.0;
		this->samp_num_coeff[i]  = 0.0;
		this->samp_den_coeff[i]  = 0.0;
	}

    this->line_off     = 0.0;
    this->samp_off     = 0.0;
    this->lat_off      = 0.0;
    this->long_off     = 0.0;
    this->height_off   = 0.0;
    this->line_scale   = 0.0;
    this->samp_scale   = 0.0;
    this->lat_scale    = 0.0;
    this->long_scale   = 0.0;
    this->height_scale = 0.0;
    this->width        = 0.0;
    this->height       = 0.0;

	this->resetAffine();
	this->hasValues = false;

}
/**
 * reads PRC parameters from file, ckecks the format and reads Ikonos or QuickBird
 * @param  const char* filename
 * @returns void
 */
bool CRPC::read(const char *filename)
{
	bool ret = false;
	ifstream fs(filename, ios::in | ios::binary);

	if (!fs.good())
	{	
		fs.close();
		return false;
	}

	char data[2];
	fs.read(data,1);
	fs.close();

	//switch between Ikonos and Quickbird readFormat
	// Ikonos starts with LINE_OFF: ....
	// Quickbird starts with satId ...
	// Socetset starts with 0...
	if (data[0] == 'L')
	{
		ret = this->readIkonosFormat(filename);
	}
	else if (data[0] == 's')
	{
		ret = this->readQuickBirdFormat(filename);		
	}
	else if (data[0] == '0')
	{
		ret = this->readSocetFormat(filename);		
	}
	else if (data[0] == 'P')
	{
		ret = this->readTextFormat(filename);		
	}
	else
		return false;
	

	if (ret) 
	{
		this->hasValues = true;
		this->refSystem.setUTMZone(this->lat_off, this->long_off);

		this->informListeners_elementsChanged();
	}

	this->setFileName(filename);

	return ret;
}


/**
 * reads PRC parameter file in Ikonos format and sets rpc values 
 * @param  char data
 * @returns true/false
 */
bool CRPC::readIkonosFormat(const char* filename)
{

	FILE* in = fopen(filename,"r");
	if (!in) 
	{
		fclose(in);
		return false;
	}
	
	char buffer[1000];

	int found=0;
	bool readLineNumCoef = false;
	bool readLineDenCoef =false;
	bool readSampNumCoef = false;
	bool readSampDenCoef = false;

	int count=0;

	while (fgets(buffer,1999,in))
	{
		CCharString b(buffer);
		b.trim();
		if (b.Find("LINE_OFF:") == 0)
		{
			sscanf(b.GetChar(),"LINE_OFF: %lf",&this->line_off);
			found++;
		}
		else if (b.Find("SAMP_OFF:") == 0)
		{
			sscanf(b.GetChar(),"SAMP_OFF: %lf",&this->samp_off);
			found++;
		}
		else if (b.Find("LAT_OFF:") == 0)
		{
			sscanf(b.GetChar(),"LAT_OFF: %lf",&this->lat_off);
			found++;
		}
		else if (b.Find("LONG_OFF:") == 0)
		{
			sscanf(b.GetChar(),"LONG_OFF: %lf",&this->long_off);
			found++;
		}
		else if (b.Find("HEIGHT_OFF:") == 0)
		{
			sscanf(b.GetChar(),"HEIGHT_OFF: %lf",&this->height_off);
			found++;
		}
		else if (b.Find("LINE_SCALE:") == 0)
		{
			sscanf(b.GetChar(),"LINE_SCALE: %lf",&this->line_scale);
			found++;
		}
		else if (b.Find("SAMP_SCALE:") == 0)
		{
			sscanf(b.GetChar(),"SAMP_SCALE: %lf",&this->samp_scale);
			found++;
		}
		else if (b.Find("LAT_SCALE:") == 0)
		{
			sscanf(b.GetChar(),"LAT_SCALE: %lf",&this->lat_scale);
			found++;
		}
		else if (b.Find("LONG_SCALE:") == 0)
		{
			sscanf(b.GetChar(),"LONG_SCALE: %lf",&this->long_scale);
			found++;
		}
		else if (b.Find("HEIGHT_SCALE:") == 0)
		{
			sscanf(b.GetChar(),"HEIGHT_SCALE: %lf",&this->height_scale);
			found++;
			readLineNumCoef= true;
			count =0;
		}
		else if (readLineNumCoef && count < 20)
		{
			char dummy[20];
			sscanf(b.GetChar(),"%s %lf",dummy,&this->line_num_coeff[count]);
			found++;
			count++;
			if (count == 20)
			{
				readLineNumCoef= false;
				readLineDenCoef= true;
				count =0;
			}
		}
		else if (readLineDenCoef && count < 20)
		{
			char dummy[20];
			sscanf(b.GetChar(),"%s %lf",dummy,&this->line_den_coeff[count]);
			found++;
			count++;
			if (count == 20)
			{
				readLineDenCoef= false;
				readSampNumCoef= true;
				count =0;
			}
		}
		else if (readSampNumCoef && count < 20)
		{
			char dummy[20];
			sscanf(b.GetChar(),"%s %lf",dummy,&this->samp_num_coeff[count]);
			found++;
			count++;
			if (count == 20)
			{
				readSampNumCoef= false;
				readSampDenCoef= true;
				count =0;
			}
		}
		else if (readSampDenCoef && count < 20)
		{
			char dummy[20];
			sscanf(b.GetChar(),"%s %lf",dummy,&this->samp_den_coeff[count]);
			found++;
			count++;
			if (count == 20)
			{
				readSampDenCoef= false;
				count =0;
			}
		}
	}

	if (found != 90)
		return false;
	
    return true;
}


/**
 * reads PRC parameter file in QuickBird format and sets rpc values 
 * @param  char data
 * @returns true/false
 */
bool CRPC::readQuickBirdFormat(const char* filename)
{
	FILE* in = fopen(filename,"r");
	if (!in) 
	{
		fclose(in);
		return false;
	}

	char buffer[1000];

	int found=0;
	bool readLineNumCoef = false;
	bool readLineDenCoef =false;
	bool readSampNumCoef = false;
	bool readSampDenCoef = false;

	int count=0;

	while (fgets(buffer,1999,in))
	{
		CCharString b(buffer);
		b.trim();
		if (b.Find("lineOffset") == 0)
		{
			sscanf(b.GetChar(),"lineOffset = %lf",&this->line_off);
			found++;
		}
		else if (b.Find("sampOffset") == 0)
		{
			sscanf(b.GetChar(),"sampOffset = %lf",&this->samp_off);
			found++;
		}
		else if (b.Find("latOffset") == 0)
		{
			sscanf(b.GetChar(),"latOffset = %lf",&this->lat_off);
			found++;
		}
		else if (b.Find("longOffset") == 0)
		{
			sscanf(b.GetChar(),"longOffset = %lf",&this->long_off);
			found++;
		}
		else if (b.Find("heightOffset") == 0)
		{
			sscanf(b.GetChar(),"heightOffset = %lf",&this->height_off);
			found++;
		}
		else if (b.Find("lineScale") == 0)
		{
			sscanf(b.GetChar(),"lineScale = %lf",&this->line_scale);
			found++;
		}
		else if (b.Find("sampScale") == 0)
		{
			sscanf(b.GetChar(),"sampScale = %lf",&this->samp_scale);
			found++;
		}
		else if (b.Find("latScale") == 0)
		{
			sscanf(b.GetChar(),"latScale = %lf",&this->lat_scale);
			found++;
		}
		else if (b.Find("longScale") == 0)
		{
			sscanf(b.GetChar(),"longScale = %lf",&this->long_scale);
			found++;
		}
		else if (b.Find("heightScale") == 0)
		{
			sscanf(b.GetChar(),"heightScale = %lf",&this->height_scale);
			found++;
		}
		else if (b.Find("lineNumCoef") == 0)
		{
			found++;
			readLineNumCoef= true;
		}
		else if (readLineNumCoef && count < 20)
		{
			sscanf(b.GetChar(),"%lf",&this->line_num_coeff[count]);
			found++;
			count++;
			if (count == 20)
			{
				readLineNumCoef= false;
				count =0;
			}
		}
		else if (b.Find("lineDenCoef") == 0)
		{
			found++;
			readLineDenCoef= true;
		}
		else if (readLineDenCoef && count < 20)
		{
			sscanf(b.GetChar(),"%lf",&this->line_den_coeff[count]);
			found++;
			count++;
			if (count == 20)
			{
				readLineDenCoef= false;
				count =0;
			}
		}
		else if (b.Find("sampNumCoef") == 0)
		{
			found++;
			readSampNumCoef= true;
		}
		else if (readSampNumCoef && count < 20)
		{
			sscanf(b.GetChar(),"%lf",&this->samp_num_coeff[count]);
			found++;
			count++;
			if (count == 20)
			{
				readSampNumCoef= false;
				count =0;
			}
		}
		else if (b.Find("sampDenCoef") == 0)
		{
			found++;
			readSampDenCoef= true;
		}
		else if (readSampDenCoef && count < 20)
		{
			sscanf(b.GetChar(),"%lf",&this->samp_den_coeff[count]);
			found++;
			count++;
			if (count == 20)
			{
				readSampDenCoef= false;
				count =0;
			}
		}
	}

	if (found != 94)
		return false;

    return true;
}


/**
 * reads PRC parameter file in QuickBird format and sets rpc values 
 * @param  char data
 * @returns true/false
 */
bool CRPC::readSocetFormat(const char* filename)
{
	FILE* fp = fopen(filename,"r");
	if (!fp) 
	{
		fclose(fp);
		return false;
	}

	char buffer[13];
	
	fgets(buffer,7,fp);
	this->line_off=atof(buffer);

	fgets(buffer,6,fp);
	this->samp_off=atof(buffer);
    
	fgets(buffer,9,fp);
	this->lat_off=atof(buffer);

	fgets(buffer,10,fp);
	this->long_off=atof(buffer);
	
	fgets(buffer,7,fp);
	this->height_off=atof(buffer);

	fgets(buffer,6,fp);
	this->line_scale=atof(buffer);

	fgets(buffer,6,fp);
	this->samp_scale=atof(buffer);

	fgets(buffer,9,fp);
	this->lat_scale=atof(buffer);

	fgets(buffer,10,fp);
	this->long_scale=atof(buffer);

	fgets(buffer,6,fp);
	this->height_scale=atof(buffer);

	for(int i = 0; i < 20; i++) 
    {
	    fgets(buffer,13,fp);
		this->line_num_coeff[i]=atof(buffer);
    }
	for(int i = 0; i < 20; i++) 
    {
	    fgets(buffer,13,fp);
		this->line_den_coeff[i]=atof(buffer);
    }
	for(int i = 0; i < 20; i++) 
    {
	    fgets(buffer,13,fp);
		this->samp_num_coeff[i]=atof(buffer);
    }
	for(int i = 0; i < 20; i++) 
    {
	    fgets(buffer,13,fp);
		this->samp_den_coeff[i]=atof(buffer);
    }

    return true;
}

bool CRPC::readTextFormat(const char* filename)
{
	FILE* fp = fopen(filename,"r");
	if (!fp) 
	{
		fclose(fp);
		return false;
	}

   char buffer[100];
   int count=0;
	
   fgets(buffer,30,fp);

	for (count=0;count<20;count++)
	{
		fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->line_num_coeff[count]);
	}

    for (count=0;count<20;count++)
	{
		fgets(buffer,30,fp);
        sscanf(buffer,"%*d, %lf",&this->line_den_coeff[count]);
	}
    
	for (count=0;count<20;count++)
	{
		fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->samp_num_coeff[count]);
	}

    for (count=0;count<20;count++)
	{
		fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->samp_den_coeff[count]);
	}
        
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->line_off);
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->samp_off);
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->lat_off);
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->long_off);
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->height_off);
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->line_scale);
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->samp_scale);
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->lat_scale);
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->long_scale);
	    fgets(buffer,30,fp);
		sscanf(buffer,"%*d, %lf",&this->height_scale);

    return true;
}

/**
 * writes PRC parameter file as comma separated file 
 * @param  filename
 * @returns true/false
 */
bool CRPC::writeTextFile(const char* filename) const
{
	FILE	*fp;
	fp = fopen(filename,"w");

    Matrix matrix;
   
    this->getAllParameters(matrix);

    fprintf(fp,"Parameter, Value\n");

    for(int i = 0; i < matrix.Nrows(); i++) 
    {     
        fprintf(fp,"%d, %+.15E,\n", i, matrix.element(i,0));
    }
	fclose(fp);

	return true;
}

/**
 * writes PRC parameter file in QuickBird format
 * @param  filename
 * @returns true/false
 */
bool CRPC::writeQuickBirdFormat(const char* filename)
{
	FILE *fp;
	fp = fopen( filename,"w");

	fprintf(fp,"satId = \"QB02\";\n");
	fprintf(fp,"bandId = \"P\";\n");
	fprintf(fp,"SpecId = \"RPC00B\";\n");
	fprintf(fp,"BEGIN_GROUP = IMAGE\n");
	fprintf(fp,"       errBias = 0.0;\n");
	fprintf(fp,"       errRand = 0.0;\n");
	fprintf(fp,"       lineOffset = %5.0lf;\n", this->line_off);
	fprintf(fp,"       sampOffset = %5.0lf;\n", this->samp_off);
	fprintf(fp,"       latOffset = %11.8lf;\n",this->lat_off);
	fprintf(fp,"       longOffset = %12.8lf;\n",this->long_off);
    fprintf(fp,"       heightOffset = %9.3lf;\n", this->height_off);
	fprintf(fp,"       lineScale = %10.2lf;\n", this->line_scale);
    fprintf(fp,"       sampScale = %10.2lf;\n", this->samp_scale);
    fprintf(fp,"       latScale = %12.8lf;\n",this->lat_scale);
    fprintf(fp,"       longScale = %13.8lf;\n",this->long_scale);
    fprintf(fp,"       heightScale = %9.3lf;\n", this->height_scale);
	
    fprintf(fp,"       lineNumCoef = (\n");

	for(int i = 0; i < 19; i++) 
    {
      
        fprintf(fp,"                       %+.15E,\n", this->line_num_coeff[i]);
    }

    fprintf(fp,"                       %+.15E);\n", this->line_num_coeff[19]);
    fprintf(fp,"       lineDenCoef = (\n");

	for(int i = 0; i < 19; i++) 
    {
        fprintf(fp,"                       %+.15E,\n", this->line_den_coeff[i]);
    }

    fprintf(fp,"                       %+.15E);\n", this->line_den_coeff[19]);
    fprintf(fp,"       sampNumCoef = (\n");

	for(int i = 0; i < 19; i++) 
    {
        fprintf(fp,"                       %+.15E,\n", this->samp_num_coeff[i]);
    }

    fprintf(fp,"                       %+.15E);\n", this->samp_num_coeff[19]);
    fprintf(fp,"       sampDenCoef = (\n");

    for(int i = 0; i < 19; i++) 
    {
        fprintf(fp,"                       %+.15E,\n", this->samp_den_coeff[i]);
    }

    fprintf(fp,"                       %+.15E);\n", this->samp_den_coeff[19]);
    fprintf(fp,"END_GROUP = IMAGE\n");
    fprintf(fp,"END;\n");

	fclose(fp);

	return true;
}

/**
 * writes PRC parameter file in Ikonos format
 * @param  filename
 * @returns true/false
 */
bool CRPC::writeIKONOSFormat(const char* filename)
{
	FILE	*fp;
	fp = fopen( filename,"w");

	fprintf(fp,"LINE_OFF: %+#010.2lf pixels\n", this->line_off);
	fprintf(fp,"SAMP_OFF: %+#010.2lf pixels\n", this->samp_off);
	fprintf(fp,"LAT_OFF: %+#011.8lf degrees\n",this->lat_off);
	fprintf(fp,"LONG_OFF: %+#012.8lf degrees\n",this->long_off);
    fprintf(fp,"HEIGHT_OFF: %+#09.3lf meters\n", this->height_off);
	fprintf(fp,"LINE_SCALE: %+#010.2lf pixels\n", this->line_scale);
    fprintf(fp,"SAMP_SCALE: %+#010.2lf pixels\n", this->samp_scale);
    fprintf(fp,"LAT_SCALE: %+#012.8lf degrees\n",this->lat_scale);
    fprintf(fp,"LONG_SCALE: %+#013.8lf degrees\n",this->long_scale);
    fprintf(fp,"HEIGHT_SCALE: %+#09.3lf meters\n", this->height_scale);
	

	for(int i = 0; i < 20; i++) 
    {
        fprintf(fp,"LINE_NUM_COEFF_%d: %+.15E\n",i+1,this->line_num_coeff[i]);
    }

	for(int i = 0; i < 20; i++) 
    {
        fprintf(fp,"LINE_DEN_COEFF_%d: %+.15E\n",i+1,this->line_den_coeff[i]);
    }

	for(int i = 0; i < 20; i++) 
    {
        fprintf(fp,"SAMP_NUM_COEFF_%d: %+.15E\n",i+1,this->samp_num_coeff[i]);
    }

	for(int i = 0; i < 20; i++) 
    {
        fprintf(fp,"SAMP_DEN_COEFF_%d: %+.15E\n",i+1,this->samp_den_coeff[i]);
    }

	fclose(fp);

	return true;
}


/**
 * writes PRC parameter file in socet format
 * @param  filename
 * @returns true/false
 */
bool CRPC::writeSocetFormat(const char* filename)
{
	FILE *fp;
	fp = fopen( filename,"w");
	fprintf(fp,"%06.0lf", this->line_off);
	fprintf(fp,"%05.0lf", this->samp_off);
	fprintf(fp,"%+08.4lf",this->lat_off);
	fprintf(fp,"%+09.4lf",this->long_off);
    fprintf(fp,"%+05.0lf", this->height_off);
	fprintf(fp,"%06.0lf", this->line_scale);
    fprintf(fp,"%05.0lf", this->samp_scale);
    fprintf(fp,"%+08.4lf",this->lat_scale);
    fprintf(fp,"%+09.4lf",this->long_scale);
    fprintf(fp,"%+05.0lf", this->height_scale);

	double m;
	int e;

	for(int i = 0; i < 20; i++) 
    {
		this->separateFloatValue(this->line_num_coeff[i],m,e);	
        fprintf(fp,"%+9.6lfE%+1d",m,e);
    }


	for(int i = 0; i < 20; i++) 
    {
		this->separateFloatValue(this->line_den_coeff[i],m,e);	
        fprintf(fp,"%+9.6lfE%+1d",m,e);
    }

	for(int i = 0; i <20; i++) 
    {
		this->separateFloatValue(this->samp_num_coeff[i],m,e);	
        fprintf(fp,"%+9.6lfE%+1d",m,e);
    }

    for(int i = 0; i < 20; i++) 
    {
		this->separateFloatValue(this->samp_den_coeff[i],m,e);	
        fprintf(fp,"%+9.6lfE%+1d",m,e);
    }
 
	fclose(fp);

	return true;
}

void CRPC::separateFloatValue(double value,double& m, int& e)
{

	if (fabs(value) < FLT_EPSILON)
	{
		m = 0.0;
		e = 0;
	}
	else
	{
	
		int signM = value < 0.0 ?  -1 : 1;
		int signE = value < 1.0 ? -1 : 1;

		double tmp = log10(value*signM);
		e = int(ceil(fabs(tmp)));

		m = value;

		if (e > 1 && e < 10)  // shift comma 
			m *=  pow(10.0,e);
		else if (e > 9 && signE < 0.0) // smaller then 10-9
		{
			m = 0.0;
			e = 0;
			signE = 1;
			signM = 1;
		}
		else if (e == 1)
			e = 0;

		e *= signE;
	}

}


/**
 * sets image height in pixels from according image
 * @param  height
 */
void CRPC::setHeight(int height)
{
    this->height = height;
}

/**
 * gets height of according image in pixels
 * @returns height as double
 */
double CRPC::getHeight()
{
    return this->height;
}

/**
 * sets image width in pixels from according image
 * @param  width
 */
void CRPC::setWidth(int width)
{
    this->width = width;
}

/**
 * gets width of according image in pixels
 * @returns width as double
 */
double CRPC::getWidth()
{
    return this->width;
}

/**
 * Serialized storage of a CRPC to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CRPC::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("rpcfilename", this->getFileName()->GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("hasValues", this->gethasValues());

    token = pStructuredFileSection->addToken();
    token->setValue("config", (int)this->config);

    token = pStructuredFileSection->addToken();
	token->setValue("bIsCurrentSensorModel", this->bIsCurrentSensorModel);

	char littlestr[100];

    for (int i = 0; i < 20; i++)
    {
		sprintf(littlestr,"LINE_NUM[%d]",i);

        token = pStructuredFileSection->addToken();
        token->setValue(littlestr, this->line_num_coeff[i]);
    }

    for (int i = 0; i < 20; i++)
    {
		sprintf(littlestr,"LINE_DEN[%d]",i);

        token = pStructuredFileSection->addToken();
        token->setValue(littlestr, this->line_den_coeff[i]);
    }

    for (int i = 0; i < 20; i++)
    {
		sprintf(littlestr,"SAMP_NUM[%d]",i);

        token = pStructuredFileSection->addToken();
        token->setValue(littlestr, this->samp_num_coeff[i]);
    }

    for (int i = 0; i < 20; i++)
    {
		sprintf(littlestr,"SAMP_DEN[%d]",i);

        token = pStructuredFileSection->addToken();
        token->setValue(littlestr, this->samp_den_coeff[i]);
    }

    token = pStructuredFileSection->addToken();
    token->setValue("line_off", this->line_off);

    token = pStructuredFileSection->addToken();
    token->setValue("samp_off", this->samp_off);

    token = pStructuredFileSection->addToken();
    token->setValue("lat_off", this->lat_off);

    token = pStructuredFileSection->addToken();
    token->setValue("long_off", this->long_off);

    token = pStructuredFileSection->addToken();
    token->setValue("height_off", this->height_off);

    token = pStructuredFileSection->addToken();
    token->setValue("line_scale", this->line_scale);

    token = pStructuredFileSection->addToken();
    token->setValue("samp_scale", this->samp_scale);

    token = pStructuredFileSection->addToken();
    token->setValue("lat_scale", this->lat_scale);

    token = pStructuredFileSection->addToken();
    token->setValue("long_scale", this->long_scale);

    token = pStructuredFileSection->addToken();
    token->setValue("height_scale", this->height_scale);

    token = pStructuredFileSection->addToken();
    token->setValue("width", this->width);

    token = pStructuredFileSection->addToken();
    token->setValue("height", this->height);


    for (int i = 0; i < 7; i++)
    {
		sprintf(littlestr,"AP[%d]",i);

        token = pStructuredFileSection->addToken();
        token->setValue(littlestr, this->ap[i]);
    }

	CCharString valueStr("");
	CCharString elementStr("");

	token = pStructuredFileSection->addToken();
	for (int i=0; i< this->nActualParameter[addPar]; i++)
	{
		for (int k=0; k< this->nActualParameter[addPar]; k++)
		{
			double element = this->covariance.element(i,k);
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}
	}

	token->setValue("APcovariance", valueStr.GetChar());

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->refSystem.serializeStore(child);
}

/**
 * Restores a CRPC from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CRPC::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);


	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("rpcfilename"))
            this->setFileName( token->getValue());
		else if(key.CompareNoCase("hasValues"))
            this->hasValues = token->getBoolValue();
		else if(key.CompareNoCase("config"))
            this->config = (RPCConfigType) token->getIntValue();
		else if(key.CompareNoCase("bIsCurrentSensorModel"))
			this->bIsCurrentSensorModel = token->getBoolValue();
		else if(key.CompareNoCase("Line_Num[0]"))
            this->line_num_coeff[0] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[1]"))
            this->line_num_coeff[1] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[2]"))
            this->line_num_coeff[2] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[3]"))
            this->line_num_coeff[3] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[4]"))
            this->line_num_coeff[4] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[5]"))
            this->line_num_coeff[5] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[6]"))
            this->line_num_coeff[6] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[7]"))
            this->line_num_coeff[7] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[8]"))
            this->line_num_coeff[8] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[9]"))
            this->line_num_coeff[9] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[10]"))
            this->line_num_coeff[10] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[11]"))
            this->line_num_coeff[11] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[12]"))
            this->line_num_coeff[12] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[13]"))
            this->line_num_coeff[13] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[14]"))
            this->line_num_coeff[14] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[15]"))
            this->line_num_coeff[15] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[16]"))
            this->line_num_coeff[16] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[17]"))
            this->line_num_coeff[17] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[18]"))
            this->line_num_coeff[18] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Num[19]"))
            this->line_num_coeff[19] = token->getDoubleValue();

		else if(key.CompareNoCase("Line_Den[0]"))
            this->line_den_coeff[0] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[1]"))
            this->line_den_coeff[1] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[2]"))
            this->line_den_coeff[2] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[3]"))
            this->line_den_coeff[3] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[4]"))
            this->line_den_coeff[4] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[5]"))
            this->line_den_coeff[5] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[6]"))
            this->line_den_coeff[6] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[7]"))
            this->line_den_coeff[7] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[8]"))
            this->line_den_coeff[8] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[9]"))
            this->line_den_coeff[9] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[10]"))
            this->line_den_coeff[10] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[11]"))
            this->line_den_coeff[11] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[12]"))
            this->line_den_coeff[12] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[13]"))
            this->line_den_coeff[13] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[14]"))
            this->line_den_coeff[14] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[15]"))
            this->line_den_coeff[15] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[16]"))
            this->line_den_coeff[16] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[17]"))
            this->line_den_coeff[17] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[18]"))
            this->line_den_coeff[18] = token->getDoubleValue();
        else if (key.CompareNoCase("Line_Den[19]"))
            this->line_den_coeff[19] = token->getDoubleValue();

		else if(key.CompareNoCase("Samp_Num[0]"))
            this->samp_num_coeff[0] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[1]"))
            this->samp_num_coeff[1] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[2]"))
            this->samp_num_coeff[2] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[3]"))
            this->samp_num_coeff[3] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[4]"))
            this->samp_num_coeff[4] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[5]"))
            this->samp_num_coeff[5] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[6]"))
            this->samp_num_coeff[6] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[7]"))
            this->samp_num_coeff[7] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[8]"))
            this->samp_num_coeff[8] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[9]"))
            this->samp_num_coeff[9] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[10]"))
            this->samp_num_coeff[10] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[11]"))
            this->samp_num_coeff[11] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[12]"))
            this->samp_num_coeff[12] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[13]"))
            this->samp_num_coeff[13] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[14]"))
            this->samp_num_coeff[14] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[15]"))
            this->samp_num_coeff[15] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[16]"))
            this->samp_num_coeff[16] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[17]"))
            this->samp_num_coeff[17] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[18]"))
            this->samp_num_coeff[18] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Num[19]"))
            this->samp_num_coeff[19] = token->getDoubleValue(); 

		else if(key.CompareNoCase("Samp_Den[0]"))
            this->samp_den_coeff[0] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[1]"))
            this->samp_den_coeff[1] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[2]"))
            this->samp_den_coeff[2] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[3]"))
            this->samp_den_coeff[3] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[4]"))
            this->samp_den_coeff[4] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[5]"))
            this->samp_den_coeff[5] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[6]"))
            this->samp_den_coeff[6] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[7]"))
            this->samp_den_coeff[7] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[8]"))
            this->samp_den_coeff[8] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[9]"))
            this->samp_den_coeff[9] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[10]"))
            this->samp_den_coeff[10] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[11]"))
            this->samp_den_coeff[11] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[12]"))
            this->samp_den_coeff[12] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[13]"))
            this->samp_den_coeff[13] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[14]"))
            this->samp_den_coeff[14] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[15]"))
            this->samp_den_coeff[15] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[16]"))
            this->samp_den_coeff[16] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[17]"))
            this->samp_den_coeff[17] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[18]"))
            this->samp_den_coeff[18] = token->getDoubleValue();
        else if (key.CompareNoCase("Samp_Den[19]"))
            this->samp_den_coeff[19] = token->getDoubleValue();

        else if (key.CompareNoCase("line_off"))
            this->line_off = token->getDoubleValue();
        else if (key.CompareNoCase("samp_off"))
            this->samp_off = token->getDoubleValue();
        else if (key.CompareNoCase("lat_off"))
            this->lat_off = token->getDoubleValue();
        else if (key.CompareNoCase("long_off"))
            this->long_off = token->getDoubleValue();
        else if (key.CompareNoCase("height_off"))
            this->height_off = token->getDoubleValue();
        else if (key.CompareNoCase("line_scale"))
            this->line_scale = token->getDoubleValue();
        else if (key.CompareNoCase("samp_scale"))
            this->samp_scale = token->getDoubleValue();
        else if (key.CompareNoCase("lat_scale"))
            this->lat_scale = token->getDoubleValue();
        else if (key.CompareNoCase("long_scale"))
            this->long_scale = token->getDoubleValue();
        else if (key.CompareNoCase("height_scale"))
            this->height_scale = token->getDoubleValue();
        else if (key.CompareNoCase("width"))
            this->width = token->getDoubleValue();
        else if (key.CompareNoCase("height"))
            this->height = token->getDoubleValue();

        else if (key.CompareNoCase("AP[0]"))
            this->ap[0] = token->getDoubleValue();
        else if (key.CompareNoCase("AP[1]"))
            this->ap[1] = token->getDoubleValue();
        else if (key.CompareNoCase("AP[2]"))
            this->ap[2] = token->getDoubleValue();
        else if (key.CompareNoCase("AP[3]"))
            this->ap[3] = token->getDoubleValue();
        else if (key.CompareNoCase("AP[4]"))
            this->ap[4] = token->getDoubleValue();
        else if (key.CompareNoCase("AP[5]"))
            this->ap[5] = token->getDoubleValue();
        else if (key.CompareNoCase("AP[6]"))
            this->ap[6] = token->getDoubleValue();

		else if (key.CompareNoCase("APcovariance"))
		{
			double* tmpArray = new double [this->nActualParameter[addPar]*this->nActualParameter[addPar]];
			token->getDoubleValues(tmpArray,this->nActualParameter[addPar]*this->nActualParameter[addPar]);
			
			for (int i=0; i< this->nActualParameter[addPar]; i++)
			{
				for (int k=0; k< this->nActualParameter[addPar]; k++)
				{
					this->covariance.element(i,k) = tmpArray[i*this->nActualParameter[addPar]+k];
				}	 
			}

			if (tmpArray) delete [] tmpArray;
		}

    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refSystem.serializeRestore(child);
			this->refSystem.setUTMZone(this->lat_off, this->long_off);
        }
    }
}

void CRPC::setAdjustableValues(const Matrix &parms)
{
    this->ap[0] = parms.element(0, 0);
    this->ap[1] = parms.element(1, 0);
    this->ap[2] = parms.element(2, 0);
    this->ap[3] = parms.element(3, 0);
    this->ap[4] = parms.element(4, 0);
    this->ap[5] = parms.element(5, 0);
    this->ap[6] = parms.element(6, 0);

    this->informListeners_elementsChanged();
}

bool CRPC::setRefSys(const CReferenceSystem &I_refSystem)
{
	bool ret = true;

	if (I_refSystem.getCoordinateType() !=  eGEOGRAPHIC) ret = false;
	else this->refSystem = I_refSystem;

	return ret;
};

int CRPC::accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
					 CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
					 CRobustWeightFunction *robust, bool omitMarked)
{
	int nObs = 2;
    double s = obs[0];
    double l = obs[1];

	
	C3DPoint norm; // 3D normailized point of the object point approx

    this->normalizeXYZ(norm, approx); // approx is object point

    double U = norm.x;
    double V = norm.y;
    double W = norm.z;
    double Numl = getNumeratorL(U, V, W);
	double Denl = getDenominatorL(U, V, W);
    double Nums = getNumeratorS(U, V, W);
    double Dens = getDenominatorS(U, V, W);

	double oneByDenl  = 1.0/Denl;
	double oneByDens  = 1.0/Dens;
	double oneByDenl2 = oneByDenl * oneByDenl;
	double oneByDens2 = oneByDens * oneByDens;
 
	double U2 = U * U;
	double V2 = V * V;
	double W2 = W * W;
	double UV = U * V;
	double UW = U * W;
	double VW = V * W;

    double p_f1_U = -Numl * (line_den_coeff[2]       +     line_den_coeff[4]  * V + 
		                     line_den_coeff[6]  * W  + 2 * line_den_coeff[8]  * U + 
							 line_den_coeff[10] * VW + 2 * line_den_coeff[12] * UV +
							 line_den_coeff[14] * V2 + 3 * line_den_coeff[15] * U2 + 
							 line_den_coeff[16] * W2 + 2 * line_den_coeff[18] * UW) * oneByDenl2 +
							(line_num_coeff[2]       +     line_num_coeff[4]  * V + 
							 line_num_coeff[6]  * W  + 2 * line_num_coeff[8]  * U + 
							 line_num_coeff[10] * VW + 2 * line_num_coeff[12] * UV +
							 line_num_coeff[14] * V2 + 3 * line_num_coeff[15] * U2 + 
							 line_num_coeff[16] * W2 + 2 * line_num_coeff[18] * UW) * oneByDenl;

    double p_f1_V = -Numl * (line_den_coeff[1]       +     line_den_coeff[4 ] * U + 
							 line_den_coeff[5]  * W  + 2 * line_den_coeff[7]  * V + 
							 line_den_coeff[10] * UW + 3 * line_den_coeff[11] * V2 +
							 line_den_coeff[12] * U2 + 2 * line_den_coeff[14] * UV +
							 line_den_coeff[13] * W2 + 2 * line_den_coeff[17] * VW) * oneByDenl2 +
                            (line_num_coeff[1]       +     line_num_coeff[4]  * U + 
							 line_num_coeff[5]  * W  + 2 * line_num_coeff[7]  * V + 
							 line_num_coeff[10] * UW + 3 * line_num_coeff[11] * V2 +
							 line_num_coeff[12] * U2 + 2 * line_num_coeff[14] * UV +  
							 line_num_coeff[13] * W2 + 2 * line_num_coeff[17] * VW) * oneByDenl;

    double p_f1_W = -Numl * (line_den_coeff[3]       +     line_den_coeff[5]  * V + 
							 line_den_coeff[6]  * U  + 2 * line_den_coeff[9]  * W + 
							 line_den_coeff[10] * UV + 2 * line_den_coeff[13] * VW +
							 line_den_coeff[17] * V2 + 2 * line_den_coeff[16] * UW + 
							 line_den_coeff[18] * U2 + 3 * line_den_coeff[19] * W2)  * oneByDenl2 +
                            (line_num_coeff[3]       +     line_num_coeff[5]  * V + 
							 line_num_coeff[6]  * U  + 2 * line_num_coeff[9]  * W + 
							 line_num_coeff[10] * UV + 2 * line_num_coeff[13] * VW +
							 line_num_coeff[17] * V2 + 2 * line_num_coeff[16] * UW + 
							 line_num_coeff[18] * U2 + 3 * line_num_coeff[19] * W2) * oneByDenl;

    double p_f2_U = -Nums * (samp_den_coeff[2]       +     samp_den_coeff[4]  * V + 
							 samp_den_coeff[6]  * W  + 2 * samp_den_coeff[8]  * U + 
							 samp_den_coeff[10] * VW + 2 * samp_den_coeff[12] * UV +
							 samp_den_coeff[14] * V2 + 3 * samp_den_coeff[15] * U2 + 
							 samp_den_coeff[16] * W2 + 2 * samp_den_coeff[18] * UW) * oneByDens2 +
                            (samp_num_coeff[2]       +     samp_num_coeff[4]  * V + 
							 samp_num_coeff[6]  * W  + 2 * samp_num_coeff[8]  * U + 
							 samp_num_coeff[10] * VW + 2 * samp_num_coeff[12] * UV +
                             samp_num_coeff[14] * V2 + 3 * samp_num_coeff[15] * U2 + 
							 samp_num_coeff[16] * W2 + 2 * samp_num_coeff[18] * UW) * oneByDens;

    double p_f2_V = -Nums * (samp_den_coeff[1]       +     samp_den_coeff[4]  * U + 
							 samp_den_coeff[5]  * W  + 2 * samp_den_coeff[7]  * V + 
							 samp_den_coeff[10] * UW + 3 * samp_den_coeff[11] * V2 +
							 samp_den_coeff[12] * U2 + 2 * samp_den_coeff[14] * UV + 
							 samp_den_coeff[13] * W2 + 2 * samp_den_coeff[17] * VW) * oneByDens2 +
                            (samp_num_coeff[1]       +     samp_num_coeff[4]  * U + 
							 samp_num_coeff[5]  * W  + 2 * samp_num_coeff[7]  * V + 
							 samp_num_coeff[10] * UW + 3 * samp_num_coeff[11] * V2 +
							 samp_num_coeff[12] * U2 + 2 * samp_num_coeff[14] * UV + 
							 samp_num_coeff[13] * W2 + 2 * samp_num_coeff[17] * VW) * oneByDens;

    double p_f2_W = -Nums * (samp_den_coeff[3]       +     samp_den_coeff[5]  * V + 
							 samp_den_coeff[6]  * U  + 2 * samp_den_coeff[9]  * W + 
							 samp_den_coeff[10] * UV + 2 * samp_den_coeff[13] * VW +
							 samp_den_coeff[17] * V2 + 2 * samp_den_coeff[16] * UW + 
							 samp_den_coeff[18] * U2 + 3 * samp_den_coeff[19] * W2) * oneByDens2 +
                            (samp_num_coeff[3]       +     samp_num_coeff[5]  * V + 
							 samp_num_coeff[6]  * U  + 2 * samp_num_coeff[9]  * W + 
							 samp_num_coeff[10] * UV + 2 * samp_num_coeff[13] * VW +
							 samp_num_coeff[17] * V2 + 2 * samp_num_coeff[16] * UW + 
							 samp_num_coeff[18] * U2 + 3 * samp_num_coeff[19] * W2) * oneByDens;


    double phi = getPhi(l);

	CAdjustMatrix AiPar(2, this->nActualStationPars);
      
	AiPar(0,0) =         0;  // df1 / dA0
    AiPar(0,1) =         0;  // df1 / dA1
    AiPar(0,2) =         0;  // df1 / dA2
    AiPar(0,3) =        -1;  // df1 / dB0
    AiPar(0,4) =        -s;  // df1 / dB1
    AiPar(0,5) =        -l;  // df1 / dB2
    AiPar(0,6) = -sin(phi);  // df1 / dB3

    AiPar(1,0) =        -1;  // df2 / dA0
    AiPar(1,1) =        -s;  // df2 / dA1
    AiPar(1,2) =        -l;  // df2 / dA2
    AiPar(1,3) =         0;  // df2 / dB0
    AiPar(1,4) =         0;  // df2 / dB1
    AiPar(1,5) =         0;  // df2 / dB2
    AiPar(1,6) =         0;  // df2 / dB3

	CAdjustMatrix AiPoint(2, 3);

    double p_f1_X = p_f1_U / lat_scale * line_scale;
    double p_f1_Y = p_f1_V / long_scale * line_scale;
    double p_f1_Z = p_f1_W / height_scale * line_scale;

    double p_f2_X = p_f2_U / lat_scale * samp_scale;
    double p_f2_Y = p_f2_V / long_scale * samp_scale;
    double p_f2_Z = p_f2_W / height_scale * samp_scale;

	AiPoint(0,0) = p_f1_X;
    AiPoint(0,1) = p_f1_Y;
    AiPoint(0,2) = p_f1_Z;
    AiPoint(1,0) = p_f2_X;
    AiPoint(1,1) = p_f2_Y;
    AiPoint(1,2) = p_f2_Z;

	CAdjustMatrix Bi(2,2);

    double p_f1_l = -this->ap[5] - this->ap[6]*cos(phi) * 2 * M_PI / height - 1;// step
    double p_f1_s = -this->ap[4];

    double p_f2_l = -this->ap[2];
    double p_f2_s = -this->ap[1] - 1;

    Bi(0,0) = p_f1_s; // df1 / ds
    Bi(0,1) = p_f1_l; // df1 / dl
    Bi(1,0) = p_f2_s; // df2 / ds
    Bi(1,1) = p_f2_l; // df2 / dl

	CAdjustMatrix q(2,2);
	q(0,0) = obs.getCovariance()->element(0,0);
	q(0,1) = obs.getCovariance()->element(0,1);
	q(1,1) = obs.getCovariance()->element(1,1);
	q(1,0) = q(0,1);

	
	CAdjustMatrix QB = Bi * q;
	QB.transpose();
	CAdjustMatrix PBB = Bi * QB;
	PBB.invert();

    CAdjustMatrix Fi(2,1);

    Fi(0,0) = (Numl / Denl) * line_scale + line_off - this->ap[3] - this->ap[4] * s - this->ap[5] * l - this->ap[6] * sin(phi) - l;//step
    Fi(1,0) = (Nums / Dens) * samp_scale + samp_off - this->ap[0] - this->ap[1] * s - this->ap[2] * l - s;

	CAdjustMatrix discr = QB * PBB;
	discr = discr * Fi;
	discrepancy[0] = -discr(0,0);
	discrepancy[1] = -discr(1,0);
	normalisedDiscrepancy[0] = fabs(discrepancy[0]) / sqrt(q(0,0));
	normalisedDiscrepancy[1] = fabs(discrepancy[1]) / sqrt(q(1,1));

	if (robust || omitMarked)
	{
		double w0 , w1;
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 0, omitMarked, w0, PBB)) --nObs; 
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 1, omitMarked, w1, PBB)) --nObs; 
	}

    CAdjustMatrix ATP = AiPar;
	ATP.transpose();
	ATP = ATP * PBB;

	CAdjustMatrix ATPA = ATP * AiPar;
	ATPA.addToMatrix(this->parameterIndex[addPar], this->parameterIndex[addPar], N);
	ATPA = ATP * AiPoint;
	ATPA.addToMatrix(this->parameterIndex[addPar], approx.id, N);
	ATPA.transpose();
	ATPA.addToMatrix(approx.id, this->parameterIndex[addPar], N);
	ATPA = ATP * Fi;
	ATPA.addToMatrix(this->parameterIndex[addPar], 0, t);

	ATP = AiPoint;
	ATP.transpose();
	ATP = ATP * PBB;
	ATPA = ATP * AiPoint;
	ATPA.addToMatrix(approx.id, approx.id, N);
	ATPA = ATP * Fi;
	ATPA.addToMatrix(approx.id, 0, t);
	
	return nObs;
};


bool CRPC::AiBiFi(Matrix &Fi, Matrix &Bi, 
				  Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca,       
				  Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
				  Matrix &AiPar,        Matrix &AiPoint,
				  const Matrix &obs, const C3DPoint &approx)
{
	AiErp.ReSize(0, 0);
    AiRot.ReSize(0, 0);
	AiSca.ReSize(0, 0);
    AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);

    AiPar.ReSize(2, this->nActualStationPars);
    AiPoint.ReSize(2, this->nParsPerPoint);
	Bi.ReSize(2, 2);
    Fi.ReSize(2, 1);
 
    double s = obs.element(0,0);
    double l = obs.element(0,1);

	C3DPoint norm;

    this->normalizeXYZ(norm, approx);

    double U = norm.x;
    double V = norm.y;
    double W = norm.z;
    double Numl = getNumeratorL(U, V, W);
	double Denl = getDenominatorL(U, V, W);
    double Nums = getNumeratorS(U, V, W);
    double Dens = getDenominatorS(U, V, W);

	double oneByDenl  = 1.0/Denl;
	double oneByDens  = 1.0/Dens;
	double oneByDenl2 = oneByDenl * oneByDenl;
	double oneByDens2 = oneByDens * oneByDens;
 
	double U2 = U * U;
	double V2 = V * V;
	double W2 = W * W;
	double UV = U * V;
	double UW = U * W;
	double VW = V * W;

    double p_f1_U = -Numl * (line_den_coeff[2]       +     line_den_coeff[4]  * V + 
		                     line_den_coeff[6]  * W  + 2 * line_den_coeff[8]  * U + 
							 line_den_coeff[10] * VW + 2 * line_den_coeff[12] * UV +
							 line_den_coeff[14] * V2 + 3 * line_den_coeff[15] * U2 + 
							 line_den_coeff[16] * W2 + 2 * line_den_coeff[18] * UW) * oneByDenl2 +
							(line_num_coeff[2]       +     line_num_coeff[4]  * V + 
							 line_num_coeff[6]  * W  + 2 * line_num_coeff[8]  * U + 
							 line_num_coeff[10] * VW + 2 * line_num_coeff[12] * UV +
							 line_num_coeff[14] * V2 + 3 * line_num_coeff[15] * U2 + 
							 line_num_coeff[16] * W2 + 2 * line_num_coeff[18] * UW) * oneByDenl;

    double p_f1_V = -Numl * (line_den_coeff[1]       +     line_den_coeff[4 ] * U + 
							 line_den_coeff[5]  * W  + 2 * line_den_coeff[7]  * V + 
							 line_den_coeff[10] * UW + 3 * line_den_coeff[11] * V2 +
							 line_den_coeff[12] * U2 + 2 * line_den_coeff[14] * UV +
							 line_den_coeff[13] * W2 + 2 * line_den_coeff[17] * VW) * oneByDenl2 +
                            (line_num_coeff[1]       +     line_num_coeff[4]  * U + 
							 line_num_coeff[5]  * W  + 2 * line_num_coeff[7]  * V + 
							 line_num_coeff[10] * UW + 3 * line_num_coeff[11] * V2 +
							 line_num_coeff[12] * U2 + 2 * line_num_coeff[14] * UV +  
							 line_num_coeff[13] * W2 + 2 * line_num_coeff[17] * VW) * oneByDenl;

    double p_f1_W = -Numl * (line_den_coeff[3]       +     line_den_coeff[5]  * V + 
							 line_den_coeff[6]  * U  + 2 * line_den_coeff[9]  * W + 
							 line_den_coeff[10] * UV + 2 * line_den_coeff[13] * VW +
							 line_den_coeff[17] * V2 + 2 * line_den_coeff[16] * UW + 
							 line_den_coeff[18] * U2 + 3 * line_den_coeff[19] * W2)  * oneByDenl2 +
                            (line_num_coeff[3]       +     line_num_coeff[5]  * V + 
							 line_num_coeff[6]  * U  + 2 * line_num_coeff[9]  * W + 
							 line_num_coeff[10] * UV + 2 * line_num_coeff[13] * VW +
							 line_num_coeff[17] * V2 + 2 * line_num_coeff[16] * UW + 
							 line_num_coeff[18] * U2 + 3 * line_num_coeff[19] * W2) * oneByDenl;

    double p_f2_U = -Nums * (samp_den_coeff[2]       +     samp_den_coeff[4]  * V + 
							 samp_den_coeff[6]  * W  + 2 * samp_den_coeff[8]  * U + 
							 samp_den_coeff[10] * VW + 2 * samp_den_coeff[12] * UV +
							 samp_den_coeff[14] * V2 + 3 * samp_den_coeff[15] * U2 + 
							 samp_den_coeff[16] * W2 + 2 * samp_den_coeff[18] * UW) * oneByDens2 +
                            (samp_num_coeff[2]       +     samp_num_coeff[4]  * V + 
							 samp_num_coeff[6]  * W  + 2 * samp_num_coeff[8]  * U + 
							 samp_num_coeff[10] * VW + 2 * samp_num_coeff[12] * UV +
                             samp_num_coeff[14] * V2 + 3 * samp_num_coeff[15] * U2 + 
							 samp_num_coeff[16] * W2 + 2 * samp_num_coeff[18] * UW) * oneByDens;

    double p_f2_V = -Nums * (samp_den_coeff[1]       +     samp_den_coeff[4]  * U + 
							 samp_den_coeff[5]  * W  + 2 * samp_den_coeff[7]  * V + 
							 samp_den_coeff[10] * UW + 3 * samp_den_coeff[11] * V2 +
							 samp_den_coeff[12] * U2 + 2 * samp_den_coeff[14] * UV + 
							 samp_den_coeff[13] * W2 + 2 * samp_den_coeff[17] * VW) * oneByDens2 +
                            (samp_num_coeff[1]       +     samp_num_coeff[4]  * U + 
							 samp_num_coeff[5]  * W  + 2 * samp_num_coeff[7]  * V + 
							 samp_num_coeff[10] * UW + 3 * samp_num_coeff[11] * V2 +
							 samp_num_coeff[12] * U2 + 2 * samp_num_coeff[14] * UV + 
							 samp_num_coeff[13] * W2 + 2 * samp_num_coeff[17] * VW) * oneByDens;

    double p_f2_W = -Nums * (samp_den_coeff[3]       +     samp_den_coeff[5]  * V + 
							 samp_den_coeff[6]  * U  + 2 * samp_den_coeff[9]  * W + 
							 samp_den_coeff[10] * UV + 2 * samp_den_coeff[13] * VW +
							 samp_den_coeff[17] * V2 + 2 * samp_den_coeff[16] * UW + 
							 samp_den_coeff[18] * U2 + 3 * samp_den_coeff[19] * W2) * oneByDens2 +
                            (samp_num_coeff[3]       +     samp_num_coeff[5]  * V + 
							 samp_num_coeff[6]  * U  + 2 * samp_num_coeff[9]  * W + 
							 samp_num_coeff[10] * UV + 2 * samp_num_coeff[13] * VW +
							 samp_num_coeff[17] * V2 + 2 * samp_num_coeff[16] * UW + 
							 samp_num_coeff[18] * U2 + 3 * samp_num_coeff[19] * W2) * oneByDens;

    double p_f1_X = p_f1_U / lat_scale * line_scale;
    double p_f1_Y = p_f1_V / long_scale * line_scale;
    double p_f1_Z = p_f1_W / height_scale * line_scale;

    double p_f2_X = p_f2_U / lat_scale * samp_scale;
    double p_f2_Y = p_f2_V / long_scale * samp_scale;
    double p_f2_Z = p_f2_W / height_scale * samp_scale;

    double phi = getPhi(l);

    double p_f1_A0 = 0;
    double p_f1_A1 = 0;
    double p_f1_A2 = 0;

    double p_f1_B0 = -1;
    double p_f1_B1 = -s;
    double p_f1_B2 = -l;
    double p_f1_B3 = -sin(phi);

    double p_f2_A0 = -1;
    double p_f2_A1 = -s;
    double p_f2_A2 = -l;

    double p_f2_B0 = 0;
    double p_f2_B1 = 0;
    double p_f2_B2 = 0;
    double p_f2_B3 = 0;

    AiPar.element(0, 0) = p_f1_A0;
    AiPar.element(0, 1) = p_f1_A1;
    AiPar.element(0, 2) = p_f1_A2;
    AiPar.element(0, 3) = p_f1_B0;
    AiPar.element(0, 4) = p_f1_B1;
    AiPar.element(0, 5) = p_f1_B2;
    AiPar.element(0, 6) = p_f1_B3;

    AiPar.element(1, 0) = p_f2_A0;
    AiPar.element(1, 1) = p_f2_A1;
    AiPar.element(1, 2) = p_f2_A2;
    AiPar.element(1, 3) = p_f2_B0;
    AiPar.element(1, 4) = p_f2_B1;
    AiPar.element(1, 5) = p_f2_B2;
    AiPar.element(1, 6) = p_f2_B3;

	AiPoint.element(0, 0) = p_f1_X;
    AiPoint.element(0, 1) = p_f1_Y;
    AiPoint.element(0, 2) = p_f1_Z;
    AiPoint.element(1, 0) = p_f2_X;
    AiPoint.element(1, 1) = p_f2_Y;
    AiPoint.element(1, 2) = p_f2_Z;

   
    double p_f1_l = -this->ap[5] - this->ap[6]*cos(phi) * 2* M_PI / height - 1;// step
    double p_f1_s = -this->ap[4];

    double p_f2_l = -this->ap[2];
    double p_f2_s = -this->ap[1] - 1;

    Bi.element(0, 0) = p_f1_s;
    Bi.element(0, 1) = p_f1_l;
    Bi.element(1, 0) = p_f2_s;
    Bi.element(1, 1) = p_f2_l;
   
   
    double F1 = 0;
    double F2 = 0;

    F1 = (Numl / Denl) * line_scale + line_off - this->ap[3] - this->ap[4] * s - this->ap[5] * l - this->ap[6] * sin(phi) - l;//step
    F2 = (Nums / Dens) * samp_scale + samp_off - this->ap[0] - this->ap[1] * s - this->ap[2] * l - s;

    Fi.element(0, 0) = F1;
    Fi.element(1, 0) = F2;

    return true;
}

bool CRPC::Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
			  Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
			  Matrix &AiPar,        Matrix &AiPoint, 
			  const Matrix &obs, const C3DPoint &approx) const
{
	AiErp.ReSize(0, 0);
    AiRot.ReSize(0, 0);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
	AiIrp.ReSize(0, 0);

    AiPar.ReSize(2, this->nActualStationPars);
    AiPoint.ReSize(2, this->nParsPerPoint);

    double s = obs(1, 1);
    double l = obs(1, 2);

	C3DPoint norm;

    this->normalizeXYZ(norm, approx);

    double U = norm.x;
    double V = norm.y;
    double W = norm.z;
    double Numl = getNumeratorL(U, V, W);
	double Denl = getDenominatorL(U, V, W);
    double Nums = getNumeratorS(U, V, W);
    double Dens = getDenominatorS(U, V, W);

	double oneByDenl  = 1.0/Denl;
	double oneByDens  = 1.0/Dens;
	double oneByDenl2 = oneByDenl * oneByDenl;
	double oneByDens2 = oneByDens * oneByDens;
 
	double U2 = U * U;
	double V2 = V * V;
	double W2 = W * W;
	double UV = U * V;
	double UW = U * W;
	double VW = V * W;

    double p_f1_U = -Numl * (line_den_coeff[2]       +     line_den_coeff[4]  * V + 
		                     line_den_coeff[6]  * W  + 2 * line_den_coeff[8]  * U + 
							 line_den_coeff[10] * VW + 2 * line_den_coeff[12] * UV +
							 line_den_coeff[14] * V2 + 3 * line_den_coeff[15] * U2 + 
							 line_den_coeff[16] * W2 + 2 * line_den_coeff[18] * UW) * oneByDenl2 +
							(line_num_coeff[2]       +     line_num_coeff[4]  * V + 
							 line_num_coeff[6]  * W  + 2 * line_num_coeff[8]  * U + 
							 line_num_coeff[10] * VW + 2 * line_num_coeff[12] * UV +
							 line_num_coeff[14] * V2 + 3 * line_num_coeff[15] * U2 + 
							 line_num_coeff[16] * W2 + 2 * line_num_coeff[18] * UW) * oneByDenl;

    double p_f1_V = -Numl * (line_den_coeff[1]       +     line_den_coeff[4 ] * U + 
							 line_den_coeff[5]  * W  + 2 * line_den_coeff[7]  * V + 
							 line_den_coeff[10] * UW + 3 * line_den_coeff[11] * V2 +
							 line_den_coeff[12] * U2 + 2 * line_den_coeff[14] * UV +
							 line_den_coeff[13] * W2 + 2 * line_den_coeff[17] * VW) * oneByDenl2 +
                            (line_num_coeff[1]       +     line_num_coeff[4]  * U + 
							 line_num_coeff[5]  * W  + 2 * line_num_coeff[7]  * V + 
							 line_num_coeff[10] * UW + 3 * line_num_coeff[11] * V2 +
							 line_num_coeff[12] * U2 + 2 * line_num_coeff[14] * UV +  
							 line_num_coeff[13] * W2 + 2 * line_num_coeff[17] * VW) * oneByDenl;

    double p_f1_W = -Numl * (line_den_coeff[3]       +     line_den_coeff[5]  * V + 
							 line_den_coeff[6]  * U  + 2 * line_den_coeff[9]  * W + 
							 line_den_coeff[10] * UV + 2 * line_den_coeff[13] * VW +
							 line_den_coeff[17] * V2 + 2 * line_den_coeff[16] * UW + 
							 line_den_coeff[18] * U2 + 3 * line_den_coeff[19] * W2)  * oneByDenl2 +
                            (line_num_coeff[3]       +     line_num_coeff[5]  * V + 
							 line_num_coeff[6]  * U  + 2 * line_num_coeff[9]  * W + 
							 line_num_coeff[10] * UV + 2 * line_num_coeff[13] * VW +
							 line_num_coeff[17] * V2 + 2 * line_num_coeff[16] * UW + 
							 line_num_coeff[18] * U2 + 3 * line_num_coeff[19] * W2) * oneByDenl;

    double p_f2_U = -Nums * (samp_den_coeff[2]       +     samp_den_coeff[4]  * V + 
							 samp_den_coeff[6]  * W  + 2 * samp_den_coeff[8]  * U + 
							 samp_den_coeff[10] * VW + 2 * samp_den_coeff[12] * UV +
							 samp_den_coeff[14] * V2 + 3 * samp_den_coeff[15] * U2 + 
							 samp_den_coeff[16] * W2 + 2 * samp_den_coeff[18] * UW) * oneByDens2 +
                            (samp_num_coeff[2]       +     samp_num_coeff[4]  * V + 
							 samp_num_coeff[6]  * W  + 2 * samp_num_coeff[8]  * U + 
							 samp_num_coeff[10] * VW + 2 * samp_num_coeff[12] * UV +
                             samp_num_coeff[14] * V2 + 3 * samp_num_coeff[15] * U2 + 
							 samp_num_coeff[16] * W2 + 2 * samp_num_coeff[18] * UW) * oneByDens;

    double p_f2_V = -Nums * (samp_den_coeff[1]       +     samp_den_coeff[4]  * U + 
							 samp_den_coeff[5]  * W  + 2 * samp_den_coeff[7]  * V + 
							 samp_den_coeff[10] * UW + 3 * samp_den_coeff[11] * V2 +
							 samp_den_coeff[12] * U2 + 2 * samp_den_coeff[14] * UV + 
							 samp_den_coeff[13] * W2 + 2 * samp_den_coeff[17] * VW) * oneByDens2 +
                            (samp_num_coeff[1]       +     samp_num_coeff[4]  * U + 
							 samp_num_coeff[5]  * W  + 2 * samp_num_coeff[7]  * V + 
							 samp_num_coeff[10] * UW + 3 * samp_num_coeff[11] * V2 +
							 samp_num_coeff[12] * U2 + 2 * samp_num_coeff[14] * UV + 
							 samp_num_coeff[13] * W2 + 2 * samp_num_coeff[17] * VW) * oneByDens;

    double p_f2_W = -Nums * (samp_den_coeff[3]       +     samp_den_coeff[5]  * V + 
							 samp_den_coeff[6]  * U  + 2 * samp_den_coeff[9]  * W + 
							 samp_den_coeff[10] * UV + 2 * samp_den_coeff[13] * VW +
							 samp_den_coeff[17] * V2 + 2 * samp_den_coeff[16] * UW + 
							 samp_den_coeff[18] * U2 + 3 * samp_den_coeff[19] * W2) * oneByDens2 +
                            (samp_num_coeff[3]       +     samp_num_coeff[5]  * V + 
							 samp_num_coeff[6]  * U  + 2 * samp_num_coeff[9]  * W + 
							 samp_num_coeff[10] * UV + 2 * samp_num_coeff[13] * VW +
							 samp_num_coeff[17] * V2 + 2 * samp_num_coeff[16] * UW + 
							 samp_num_coeff[18] * U2 + 3 * samp_num_coeff[19] * W2) * oneByDens;

    double p_f1_X = p_f1_U / lat_scale * line_scale;
    double p_f1_Y = p_f1_V / long_scale * line_scale;
    double p_f1_Z = p_f1_W / height_scale * line_scale;

    double p_f2_X = p_f2_U / lat_scale * samp_scale;
    double p_f2_Y = p_f2_V / long_scale * samp_scale;
    double p_f2_Z = p_f2_W / height_scale * samp_scale;

    double phi = getPhi(l);

    double p_f1_A0 = 0;
    double p_f1_A1 = 0;
    double p_f1_A2 = 0;

    double p_f1_B0 = -1;
    double p_f1_B1 = -s;
    double p_f1_B2 = -l;
    double p_f1_B3 = -sin(phi);

    double p_f2_A0 = -1;
    double p_f2_A1 = -s;
    double p_f2_A2 = -l;

    double p_f2_B0 = 0;
    double p_f2_B1 = 0;
    double p_f2_B2 = 0;
    double p_f2_B3 = 0;

    AiPar.element(0, 0) = p_f1_A0;
    AiPar.element(0, 1) = p_f1_A1;
    AiPar.element(0, 2) = p_f1_A2;
    AiPar.element(0, 3) = p_f1_B0;
    AiPar.element(0, 4) = p_f1_B1;
    AiPar.element(0, 5) = p_f1_B2;
    AiPar.element(0, 6) = p_f1_B3;

    AiPar.element(1, 0) = p_f2_A0;
    AiPar.element(1, 1) = p_f2_A1;
    AiPar.element(1, 2) = p_f2_A2;
    AiPar.element(1, 3) = p_f2_B0;
    AiPar.element(1, 4) = p_f2_B1;
    AiPar.element(1, 5) = p_f2_B2;
    AiPar.element(1, 6) = p_f2_B3;

	AiPoint.element(0, 0) = p_f1_X;
    AiPoint.element(0, 1) = p_f1_Y;
    AiPoint.element(0, 2) = p_f1_Z;
    AiPoint.element(1, 0) = p_f2_X;
    AiPoint.element(1, 1) = p_f2_Y;
    AiPoint.element(1, 2) = p_f2_Z;

    return true;
}

bool CRPC::AiPars(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
				  Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
				  Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const
{
	AiErp.ReSize(0, 0);
    AiRot.ReSize(0, 0);
    AiSca.ReSize(0, 0);
	AiMountShift.ReSize(0, 0);
	AiMountRot.ReSize(0, 0);
    AiIrp.ReSize(0, 0);

    AiPar.ReSize(2, this->nActualStationPars + this->nParsPerPoint);
   
    double s = obs(1, 1);
    double l = obs(1, 2);

	C3DPoint norm;

    this->normalizeXYZ(norm, approx);

    double U = norm.x;
    double V = norm.y;
    double W = norm.z;
    double Numl = getNumeratorL(U, V, W);
	double Denl = getDenominatorL(U, V, W);
    double Nums = getNumeratorS(U, V, W);
    double Dens = getDenominatorS(U, V, W);

	double oneByDenl  = 1.0/Denl;
	double oneByDens  = 1.0/Dens;
	double oneByDenl2 = oneByDenl * oneByDenl;
	double oneByDens2 = oneByDens * oneByDens;
 
	double U2 = U * U;
	double V2 = V * V;
	double W2 = W * W;
	double UV = U * V;
	double UW = U * W;
	double VW = V * W;

    double p_f1_U = -Numl * (line_den_coeff[2]       +     line_den_coeff[4]  * V + 
		                     line_den_coeff[6]  * W  + 2 * line_den_coeff[8]  * U + 
							 line_den_coeff[10] * VW + 2 * line_den_coeff[12] * UV +
							 line_den_coeff[14] * V2 + 3 * line_den_coeff[15] * U2 + 
							 line_den_coeff[16] * W2 + 2 * line_den_coeff[18] * UW) * oneByDenl2 +
							(line_num_coeff[2]       +     line_num_coeff[4]  * V + 
							 line_num_coeff[6]  * W  + 2 * line_num_coeff[8]  * U + 
							 line_num_coeff[10] * VW + 2 * line_num_coeff[12] * UV +
							 line_num_coeff[14] * V2 + 3 * line_num_coeff[15] * U2 + 
							 line_num_coeff[16] * W2 + 2 * line_num_coeff[18] * UW) * oneByDenl;

    double p_f1_V = -Numl * (line_den_coeff[1]       +     line_den_coeff[4 ] * U + 
							 line_den_coeff[5]  * W  + 2 * line_den_coeff[7]  * V + 
							 line_den_coeff[10] * UW + 3 * line_den_coeff[11] * V2 +
							 line_den_coeff[12] * U2 + 2 * line_den_coeff[14] * UV +
							 line_den_coeff[13] * W2 + 2 * line_den_coeff[17] * VW) * oneByDenl2 +
                            (line_num_coeff[1]       +     line_num_coeff[4]  * U + 
							 line_num_coeff[5]  * W  + 2 * line_num_coeff[7]  * V + 
							 line_num_coeff[10] * UW + 3 * line_num_coeff[11] * V2 +
							 line_num_coeff[12] * U2 + 2 * line_num_coeff[14] * UV +  
							 line_num_coeff[13] * W2 + 2 * line_num_coeff[17] * VW) * oneByDenl;

    double p_f1_W = -Numl * (line_den_coeff[3]       +     line_den_coeff[5]  * V + 
							 line_den_coeff[6]  * U  + 2 * line_den_coeff[9]  * W + 
							 line_den_coeff[10] * UV + 2 * line_den_coeff[13] * VW +
							 line_den_coeff[17] * V2 + 2 * line_den_coeff[16] * UW + 
							 line_den_coeff[18] * U2 + 3 * line_den_coeff[19] * W2)  * oneByDenl2 +
                            (line_num_coeff[3]       +     line_num_coeff[5]  * V + 
							 line_num_coeff[6]  * U  + 2 * line_num_coeff[9]  * W + 
							 line_num_coeff[10] * UV + 2 * line_num_coeff[13] * VW +
							 line_num_coeff[17] * V2 + 2 * line_num_coeff[16] * UW + 
							 line_num_coeff[18] * U2 + 3 * line_num_coeff[19] * W2) * oneByDenl;

    double p_f2_U = -Nums * (samp_den_coeff[2]       +     samp_den_coeff[4]  * V + 
							 samp_den_coeff[6]  * W  + 2 * samp_den_coeff[8]  * U + 
							 samp_den_coeff[10] * VW + 2 * samp_den_coeff[12] * UV +
							 samp_den_coeff[14] * V2 + 3 * samp_den_coeff[15] * U2 + 
							 samp_den_coeff[16] * W2 + 2 * samp_den_coeff[18] * UW) * oneByDens2 +
                            (samp_num_coeff[2]       +     samp_num_coeff[4]  * V + 
							 samp_num_coeff[6]  * W  + 2 * samp_num_coeff[8]  * U + 
							 samp_num_coeff[10] * VW + 2 * samp_num_coeff[12] * UV +
                             samp_num_coeff[14] * V2 + 3 * samp_num_coeff[15] * U2 + 
							 samp_num_coeff[16] * W2 + 2 * samp_num_coeff[18] * UW) * oneByDens;

    double p_f2_V = -Nums * (samp_den_coeff[1]       +     samp_den_coeff[4]  * U + 
							 samp_den_coeff[5]  * W  + 2 * samp_den_coeff[7]  * V + 
							 samp_den_coeff[10] * UW + 3 * samp_den_coeff[11] * V2 +
							 samp_den_coeff[12] * U2 + 2 * samp_den_coeff[14] * UV + 
							 samp_den_coeff[13] * W2 + 2 * samp_den_coeff[17] * VW) * oneByDens2 +
                            (samp_num_coeff[1]       +     samp_num_coeff[4]  * U + 
							 samp_num_coeff[5]  * W  + 2 * samp_num_coeff[7]  * V + 
							 samp_num_coeff[10] * UW + 3 * samp_num_coeff[11] * V2 +
							 samp_num_coeff[12] * U2 + 2 * samp_num_coeff[14] * UV + 
							 samp_num_coeff[13] * W2 + 2 * samp_num_coeff[17] * VW) * oneByDens;

    double p_f2_W = -Nums * (samp_den_coeff[3]       +     samp_den_coeff[5]  * V + 
							 samp_den_coeff[6]  * U  + 2 * samp_den_coeff[9]  * W + 
							 samp_den_coeff[10] * UV + 2 * samp_den_coeff[13] * VW +
							 samp_den_coeff[17] * V2 + 2 * samp_den_coeff[16] * UW + 
							 samp_den_coeff[18] * U2 + 3 * samp_den_coeff[19] * W2) * oneByDens2 +
                            (samp_num_coeff[3]       +     samp_num_coeff[5]  * V + 
							 samp_num_coeff[6]  * U  + 2 * samp_num_coeff[9]  * W + 
							 samp_num_coeff[10] * UV + 2 * samp_num_coeff[13] * VW +
							 samp_num_coeff[17] * V2 + 2 * samp_num_coeff[16] * UW + 
							 samp_num_coeff[18] * U2 + 3 * samp_num_coeff[19] * W2) * oneByDens;

    double phi = getPhi(l);

    double p_f1_A0 = 0;
    double p_f1_A1 = 0;
    double p_f1_A2 = 0;

    double p_f1_B0 = -1;
    double p_f1_B1 = -s;
    double p_f1_B2 = -l;
    double p_f1_B3 = -sin(phi);

    double p_f2_A0 = -1;
    double p_f2_A1 = -s;
    double p_f2_A2 = -l;

    double p_f2_B0 = 0;
    double p_f2_B1 = 0;
    double p_f2_B2 = 0;
    double p_f2_B3 = 0;

    AiPar.element(0, 0) = p_f1_A0;
    AiPar.element(0, 1) = p_f1_A1;
    AiPar.element(0, 2) = p_f1_A2;

    AiPar.element(0, 3) = p_f1_B0;
    AiPar.element(0, 4) = p_f1_B1;
    AiPar.element(0, 5) = p_f1_B2;
    AiPar.element(0, 6) = p_f1_B3;

    AiPar.element(1, 0) = p_f2_A0;
    AiPar.element(1, 1) = p_f2_A1;
    AiPar.element(1, 2) = p_f2_A2;
    AiPar.element(1, 3) = p_f2_B0;
    AiPar.element(1, 4) = p_f2_B1;
    AiPar.element(1, 5) = p_f2_B2;
    AiPar.element(1, 6) = p_f2_B3;

    return true;
}


bool CRPC::AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const
{
    Ai.ReSize(2, this->nParsPerPoint);
   
    double s = obs(1, 1);
    double l = obs(1, 2);

	C3DPoint norm;

    this->normalizeXYZ(norm, approx);

    double U = norm.x;
    double V = norm.y;
    double W = norm.z;
    double Numl = getNumeratorL(U, V, W);
	double Denl = getDenominatorL(U, V, W);
    double Nums = getNumeratorS(U, V, W);
    double Dens = getDenominatorS(U, V, W);

	double oneByDenl  = 1.0/Denl;
	double oneByDens  = 1.0/Dens;
	double oneByDenl2 = oneByDenl * oneByDenl;
	double oneByDens2 = oneByDens * oneByDens;
 
	double U2 = U * U;
	double V2 = V * V;
	double W2 = W * W;
	double UV = U * V;
	double UW = U * W;
	double VW = V * W;

    double p_f1_U = -Numl * (line_den_coeff[2]       +     line_den_coeff[4]  * V + 
		                     line_den_coeff[6]  * W  + 2 * line_den_coeff[8]  * U + 
							 line_den_coeff[10] * VW + 2 * line_den_coeff[12] * UV +
							 line_den_coeff[14] * V2 + 3 * line_den_coeff[15] * U2 + 
							 line_den_coeff[16] * W2 + 2 * line_den_coeff[18] * UW) * oneByDenl2 +
							(line_num_coeff[2]       +     line_num_coeff[4]  * V + 
							 line_num_coeff[6]  * W  + 2 * line_num_coeff[8]  * U + 
							 line_num_coeff[10] * VW + 2 * line_num_coeff[12] * UV +
							 line_num_coeff[14] * V2 + 3 * line_num_coeff[15] * U2 + 
							 line_num_coeff[16] * W2 + 2 * line_num_coeff[18] * UW) * oneByDenl;

    double p_f1_V = -Numl * (line_den_coeff[1]       +     line_den_coeff[4 ] * U + 
							 line_den_coeff[5]  * W  + 2 * line_den_coeff[7]  * V + 
							 line_den_coeff[10] * UW + 3 * line_den_coeff[11] * V2 +
							 line_den_coeff[12] * U2 + 2 * line_den_coeff[14] * UV +
							 line_den_coeff[13] * W2 + 2 * line_den_coeff[17] * VW) * oneByDenl2 +
                            (line_num_coeff[1]       +     line_num_coeff[4]  * U + 
							 line_num_coeff[5]  * W  + 2 * line_num_coeff[7]  * V + 
							 line_num_coeff[10] * UW + 3 * line_num_coeff[11] * V2 +
							 line_num_coeff[12] * U2 + 2 * line_num_coeff[14] * UV +  
							 line_num_coeff[13] * W2 + 2 * line_num_coeff[17] * VW) * oneByDenl;

    double p_f1_W = -Numl * (line_den_coeff[3]       +     line_den_coeff[5]  * V + 
							 line_den_coeff[6]  * U  + 2 * line_den_coeff[9]  * W + 
							 line_den_coeff[10] * UV + 2 * line_den_coeff[13] * VW +
							 line_den_coeff[17] * V2 + 2 * line_den_coeff[16] * UW + 
							 line_den_coeff[18] * U2 + 3 * line_den_coeff[19] * W2)  * oneByDenl2 +
                            (line_num_coeff[3]       +     line_num_coeff[5]  * V + 
							 line_num_coeff[6]  * U  + 2 * line_num_coeff[9]  * W + 
							 line_num_coeff[10] * UV + 2 * line_num_coeff[13] * VW +
							 line_num_coeff[17] * V2 + 2 * line_num_coeff[16] * UW + 
							 line_num_coeff[18] * U2 + 3 * line_num_coeff[19] * W2) * oneByDenl;

    double p_f2_U = -Nums * (samp_den_coeff[2]       +     samp_den_coeff[4]  * V + 
							 samp_den_coeff[6]  * W  + 2 * samp_den_coeff[8]  * U + 
							 samp_den_coeff[10] * VW + 2 * samp_den_coeff[12] * UV +
							 samp_den_coeff[14] * V2 + 3 * samp_den_coeff[15] * U2 + 
							 samp_den_coeff[16] * W2 + 2 * samp_den_coeff[18] * UW) * oneByDens2 +
                            (samp_num_coeff[2]       +     samp_num_coeff[4]  * V + 
							 samp_num_coeff[6]  * W  + 2 * samp_num_coeff[8]  * U + 
							 samp_num_coeff[10] * VW + 2 * samp_num_coeff[12] * UV +
                             samp_num_coeff[14] * V2 + 3 * samp_num_coeff[15] * U2 + 
							 samp_num_coeff[16] * W2 + 2 * samp_num_coeff[18] * UW) * oneByDens;

    double p_f2_V = -Nums * (samp_den_coeff[1]       +     samp_den_coeff[4]  * U + 
							 samp_den_coeff[5]  * W  + 2 * samp_den_coeff[7]  * V + 
							 samp_den_coeff[10] * UW + 3 * samp_den_coeff[11] * V2 +
							 samp_den_coeff[12] * U2 + 2 * samp_den_coeff[14] * UV + 
							 samp_den_coeff[13] * W2 + 2 * samp_den_coeff[17] * VW) * oneByDens2 +
                            (samp_num_coeff[1]       +     samp_num_coeff[4]  * U + 
							 samp_num_coeff[5]  * W  + 2 * samp_num_coeff[7]  * V + 
							 samp_num_coeff[10] * UW + 3 * samp_num_coeff[11] * V2 +
							 samp_num_coeff[12] * U2 + 2 * samp_num_coeff[14] * UV + 
							 samp_num_coeff[13] * W2 + 2 * samp_num_coeff[17] * VW) * oneByDens;

    double p_f2_W = -Nums * (samp_den_coeff[3]       +     samp_den_coeff[5]  * V + 
							 samp_den_coeff[6]  * U  + 2 * samp_den_coeff[9]  * W + 
							 samp_den_coeff[10] * UV + 2 * samp_den_coeff[13] * VW +
							 samp_den_coeff[17] * V2 + 2 * samp_den_coeff[16] * UW + 
							 samp_den_coeff[18] * U2 + 3 * samp_den_coeff[19] * W2) * oneByDens2 +
                            (samp_num_coeff[3]       +     samp_num_coeff[5]  * V + 
							 samp_num_coeff[6]  * U  + 2 * samp_num_coeff[9]  * W + 
							 samp_num_coeff[10] * UV + 2 * samp_num_coeff[13] * VW +
							 samp_num_coeff[17] * V2 + 2 * samp_num_coeff[16] * UW + 
							 samp_num_coeff[18] * U2 + 3 * samp_num_coeff[19] * W2) * oneByDens;

    double p_f1_X = p_f1_U / lat_scale * line_scale;
    double p_f1_Y = p_f1_V / long_scale * line_scale;
    double p_f1_Z = p_f1_W / height_scale * line_scale;

    double p_f2_X = p_f2_U / lat_scale * samp_scale;
    double p_f2_Y = p_f2_V / long_scale * samp_scale;
    double p_f2_Z = p_f2_W / height_scale * samp_scale;


    Ai.element(0, 0) = p_f1_X;
    Ai.element(0, 1) = p_f1_Y;
    Ai.element(0, 2) = p_f1_Z;

    
    Ai.element(1, 0) = p_f2_X;
    Ai.element(1, 1) = p_f2_Y;
    Ai.element(1, 2) = p_f2_Z;

    return true;
}

bool CRPC::Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const
{
    Fi.ReSize(2, 1);

    double s = obs(1, 1);
    double l = obs(1, 2);

	C3DPoint norm;
    this->normalizeXYZ(norm, approx);

    double U = norm.x;
    double V = norm.y;
    double W = norm.z;

    double Numl = getNumeratorL(U, V, W);
	double Denl = getDenominatorL(U, V, W);
    double Nums = getNumeratorS(U, V, W);
    double Dens = getDenominatorS(U, V, W);

    double phi = getPhi(l);

    double F1 = (Numl / Denl) * line_scale + line_off - this->ap[3] - this->ap[4] * s - this->ap[5] * l - this->ap[6] * sin(phi) - l;//step
    double F2 = (Nums / Dens) * samp_scale + samp_off - this->ap[0] - this->ap[1] * s - this->ap[2] * l - s;

    Fi.element(0, 0) = F1;
    Fi.element(1, 0) = F2;

    return true;
}

bool CRPC::Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const
{
    Bi.ReSize(2, 2);

    
    double s = obs.element(0, 0);
    double l = obs.element(0, 1);

    double phi = getPhi(l);

    double p_f1_l = -this->ap[5] - this->ap[6]*cos(phi) * 2 * M_PI / height - 1;// step
    double p_f1_s = -this->ap[4];

    double p_f2_l = -this->ap[2];
    double p_f2_s = -this->ap[1] - 1;

    if (true)
    {
        Bi.element(0, 0) = p_f1_s;
        Bi.element(0, 1) = p_f1_l;
        Bi.element(1, 0) = p_f2_s;
        Bi.element(1, 1) = p_f2_l;
    }
    else
    {
        Bi.element(0, 0) =  0;
        Bi.element(0, 1) = -1;
        Bi.element(1, 0) = -1;
        Bi.element(1, 1) =  0;
    }

    return true;
}

void CRPC::getName(CCharString& name) const
{
    name = "RPC Bundle";
}

void CRPC::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
    CCharString* str;
   
	StationNames.RemoveAll();
	PointNames.RemoveAll();

	str=StationNames.Add();
    *str = "A0";

    str=StationNames.Add();
    *str = "A1";
        
    str=StationNames.Add();
    *str = "A2";

    str=StationNames.Add();
    *str = "B0";

    str=StationNames.Add();
    *str = "B1";

    str=StationNames.Add();
    *str = "B2";

    str=StationNames.Add();
    *str = "B3";

	str=PointNames.Add();
    *str = "Latitude";

    str=PointNames.Add();
    *str = "Longitude";

    str=PointNames.Add();
    *str = "Height";
}

double CRPC::getPhi(double l) const
{
	return (l - height * 0.5f) * 2.0f * M_PI / height;
};

void CRPC::addDirectObservations(Matrix &N, Matrix &t)
{
	Matrix Q = this->covariance.i();
	this->addMatrix(this->parameterIndex[addPar], this->parameterIndex[addPar], N, Q);	
};

bool CRPC::setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const
{
	Matrix lBi;
	Matrix obsMat(1,2);
	obsMat.element(0,0) = obs.getX();
	obsMat.element(0,1) = obs.getY();
	this->Bi(lBi, obsMat, obj);
	Matrix BtQllBinv = lBi.t() * lBi * 0.000001;
	BtQllBinv = BtQllBinv.i();
	int iter = 0;
	Matrix Qxx;
	double rms = FLT_MAX;
    do
	{
		Matrix lAi, lFi;
		this->AiPoint(lAi, obsMat, obj);
		lAi = lAi.SubMatrix(1,2,3,3);
		this->Fi(lFi, obsMat, obj);
		Matrix Npt = lAi.t() * BtQllBinv;
		Matrix N = Npt * lAi;
		Matrix t = Npt * lFi;
		Qxx = N.i();
		Matrix delta = Qxx * t;
		obj.setZ(obj.getZ() - delta.element(0,0));
		rms = lFi.element(0,0) * lFi.element(0,0) + lFi.element(1,0) *  lFi.element(1,0);
		++iter;
	} while ((iter < 20) && (rms > 1e-20));

	return true;
};
	 
bool CRPC::setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const
{
	CXYPoint obs2D(obs.getX(), obs.getY(), obs.getLabel());
	return setHeigthAtXY(obj, obs2D);
};

void CRPC::crop(int offsetX, int offsetY, int newWidth, int newHeight)
{
	this->samp_off = this->samp_off - offsetX;
	this->line_off = this->line_off - offsetY;
	this->setFileName("CropRPCs");

};
