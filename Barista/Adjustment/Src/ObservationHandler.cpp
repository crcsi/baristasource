/**
 * @class CObservationHandler
 * CObservationHandler handles observations
 */

#include "ObservationHandler.h"
#include "XYPointArray.h"
#include "XYZPointArray.h"
#include "XYZOrbitPointArray.h"
#include "SensorModel.h"

CObservationHandler::CObservationHandler() : obsPoints (0), currentSensorModel (0), residualPoints(0),
	useObservationHandler(true)
{
}

CObservationHandler::CObservationHandler(const CObservationHandler& src) : obsPoints (0), currentSensorModel (0), residualPoints(0),
	useObservationHandler(src.useObservationHandler)
{
}

CObservationHandler::~CObservationHandler()
{
}

CObsPointArray *CObservationHandler::getObsPoints() const
{
	CObsPointArray *pobs = 0;

	if (currentSensorModel)
	{
			pobs = this->obsPoints;
	}

	return pobs;
};

CObsPointArray *CObservationHandler::getResidualPoints() const
{
	return this->residualPoints;
};


CSensorModel *CObservationHandler::getCurrentSensorModel() const
{
	return currentSensorModel;
};

CLabel CObservationHandler::getLabel(int index) const
{
	if (this->obsPoints)  return this->obsPoints->GetAt(index)->getLabel();
	CLabel label;
	return label;
};

bool CObservationHandler::isActive(int index) const
{
	if (this->obsPoints)
		if (this->obsPoints->GetAt(index)->getStatusBit(0))
			return true;

	return false;
}

int CObservationHandler::nObs() const
{
	if (this->obsPoints)  return this->obsPoints->GetSize();
	return 0;
};

int CObservationHandler::getNpointObservations() const
{
	if (this->currentSensorModel) return currentSensorModel->getNObsPerPoint();

	return 0;
};

int CObservationHandler::getNstationParameters() const
{
	if (this->currentSensorModel) return currentSensorModel->getNumberOfStationPars();

	return 0;
};

int CObservationHandler::getNConstraints() const
{
	if (this->currentSensorModel) return this->currentSensorModel->getParameterCount(CSensorModel::constraints);
		
	return 0;
}

void CObservationHandler::resetIndices()
{
	if (this->obsPoints)  
	{
		for (int i = 0; i < this->obsPoints->GetSize(); ++i)
		{
			this->obsPoints->GetAt(i)->id = -1;

		}
	}
	if (this->currentSensorModel) this->currentSensorModel->resetParameterIndices();
};

bool CObservationHandler::hasUnknownObjPoints()
{
	if (this->currentSensorModel) return this->currentSensorModel->hasUnknownObjPoints();

	return false;
}

int CObservationHandler::getNOrbitPoints() const
{
	return 0;
}

int CObservationHandler::getDimension() const
{ 
	if (this->obsPoints)	
		return this->obsPoints->getDimension();
	else return 0;
}