#include "CameraMounting.h"

CCameraMounting::CCameraMounting(void): shift(0,0,0)
{
}

CCameraMounting::CCameraMounting(const CCameraMounting& cameraMounting) : 
	shift(cameraMounting.shift), rotation(cameraMounting.rotation)
{
	this->name = cameraMounting.name;
}

CCameraMounting::CCameraMounting(CCharString name)
	: shift(0,0,0)
{
	this->name = name;
}

CCameraMounting &CCameraMounting::operator = (const CCameraMounting & cm)
{
	this->shift = cm.shift;
	this->rotation = cm.rotation;
	this->name = cm.name;

	this->informListeners_elementsChanged(this->getClassName().c_str());  

	return *this;
}



CCameraMounting::~CCameraMounting(void)
{
}

void CCameraMounting::goingToBeDeleted()
{
	this->informListeners_elementDeleted();
}


void CCameraMounting::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CToken* pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("name",this->name.GetChar());

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->shift.serializeStore(child);

	child = pStructuredFileSection->addChild();
    this->rotation.serializeStore(child);
}

void CCameraMounting::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("name"))
            this->name = token->getValue();
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CXYZPoint"))
        {
            this->shift.serializeRestore(child);
        }
		else if (child->getName().CompareNoCase("CRollPitchYawRotation"))
        {
            this->rotation.serializeRestore(child);
        }
    }
}

 void CCameraMounting::reconnectPointers()
 {
	 CSerializable::reconnectPointers();

	 this->shift.reconnectPointers();
	 this->rotation.reconnectPointers();
 }

 void CCameraMounting::resetPointers()
 {
	 CSerializable::resetPointers();

	 this->shift.resetPointers();
	 this->rotation.resetPointers(); 

 }

 void CCameraMounting::setRotation(const CRollPitchYawRotation& rot)
 {
	 this->rotation.updateRotMat(rot.getRotMat());

	this->informListeners_elementsChanged(this->getClassName().c_str());  
 }
	
 void CCameraMounting::applyRotation(const CRotationMatrix& rotMat)
 {
	 C3DPoint oldShift(this->shift);
	 rotMat.R_times_x(this->shift, oldShift);
	 CRotationMatrix rot;
	 rotMat.R_times_R(rot, this->rotation.getRotMat());

	 this->rotation.updateRotMat(rot);
	 this->informListeners_elementsChanged(this->getClassName().c_str());  
 };

void CCameraMounting::setShift(const CXYZPoint& shift)
{
	this->shift = shift;

	this->informListeners_elementsChanged(this->getClassName().c_str());  
}

