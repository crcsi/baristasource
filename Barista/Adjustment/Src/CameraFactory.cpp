#include "CameraFactory.h"

#include "CCDLine.h"
#include "FrameCamera.h"
#include "FrameCameraAnalogue.h"

CCamera* CCameraFactory::createCamera(CameraType type)
{

	switch (type)
	{
	case eNoType		: return NULL;
	case eCCD			: return new CCCDLine();
	case eDigitalFrame	: return new CFrameCamera();
	case eAnalogueFrame	: return new CFrameCameraAnalogue();
	default				: return NULL;
	}
}

CCamera* CCameraFactory::createCamera(const CCamera* c)
{
	if (c)
	{
		switch (c->getCameraType())
		{
		case eNoType		: return NULL;
		case eCCD			: return new CCCDLine(*(CCCDLine*)c);
		case eDigitalFrame	: return new CFrameCamera(*(CFrameCamera*)c);
		case eAnalogueFrame	: return new CFrameCameraAnalogue(*(CFrameCameraAnalogue*)c);
		default				: return NULL;
		}
	}
	else
		return NULL;
}

void CCameraFactory::copyCamera(CCamera* dest, const CCamera* src)
{
	if (!dest && !src)
		return;

	switch (src->getCameraType())
	{
	case eCCD			:	{
								if (dest->getCameraType() == eCCD)
								{	(*(CCCDLine*)dest) = *(CCCDLine*)src;
									return;
								}
							}
	case eDigitalFrame	:	{
								if (dest->getCameraType() == eDigitalFrame)
								{	(*(CFrameCamera*)dest) = *(CFrameCamera*)src;
									return;						
								}
							}
	case eAnalogueFrame	:	{
								if (dest->getCameraType() == eAnalogueFrame)
								{	(*(CFrameCameraAnalogue*)dest) = *(CFrameCameraAnalogue*)src;
									return;						
								}
							}
	default				: return ;
	}

	(*dest) = *src;

}