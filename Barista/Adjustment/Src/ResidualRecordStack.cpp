#include <strstream>
using namespace std;

#include "ResidualRecordStack.h"
#include "SensorModel.h"
#include "ObsPoint.h"

bool CResidualRecordStack::insertRecord(const CResidualRecord &rec)
{
	bool ret = false;

	if (this->currRes == 0)
	{
		this->stack[this->currRes] = rec;
		++this->currRes;
		ret = true;
	}
	else if (this->stack[this->currRes - 1] >= rec)
	{
		if (this->currRes < this->stackSize())
		{
			this->stack[this->currRes] = rec;
			++this->currRes;
			ret = true;
		}
	}
	else 
	{
		int index = -1;
		if (this->stack[0] <= rec) 
		{
			index = 0;
			ret = true;
		}
		else
		{
			int imin = 0, imax = this->currRes - 1;

			while (imax - imin > 1)
			{
				int i = (imin + imax) / 2;

				if (this->stack[i] > rec) imin = i;
				else imax = i;
			}

			index = imin + 1;
		}

		if (this->currRes < this->stackSize()) ++this->currRes;

		for (int i = this->currRes - 1; i > index; --i)
		{
			this->stack[i] = this->stack[i - 1];
		}

		this->stack[index] = rec;
		ret = true;
	};

	return ret;
};
	  
int CResidualRecordStack::insertRecord(CResidualRecord &rec, const CObsPoint &obs, 
									   const CObsPoint &res, const CObsPoint &resnorm,
									   bool omitMarked)
{
	int indMax = rec.getSensorMod()->getNObsPerPoint();
	int ret = 0;
	for (int i = 0; i < indMax; ++i)
	{
		if (!obs.getStatusBit(ROBUSTBITBASE + i) || !omitMarked)
		{
			rec.set(res[i], resnorm[i], i);
			ret += this->insertRecord(rec);
		}
	}
	return ret;
};

CCharString CResidualRecordStack::getString(int pointWidth) const
{
	CCharString str;

	if (this->nRes() > 0)
	{
		ostrstream oss;
		oss << "The ";
		oss.width(3);
		oss << this->nRes() << " worst normalised residuals:\n===================================\n\n";
		oss.width(pointWidth);
		oss << "Point" << "  Obs.  normRes   Residual  Station      Model\n";
		oss << ends;
		char *z = oss.str();
		str = z;
		delete [] z;

		for (int i = 0; i < this->nRes(); ++i)
		{
			str += this->stack[i].getString(pointWidth) + "\n";
		}
	}
	return str;
};
