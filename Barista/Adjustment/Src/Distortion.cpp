#include "Distortion.h"


CDistortion::CDistortion(DistortionType type) : radialDistortion(3),type(type)
{
	this->radialDistortion.setParent(this);
}

CDistortion::CDistortion(const CDistortion& d) : radialDistortion(d.radialDistortion),type(d.type)
{
	this->radialDistortion.setParent(this);
}




CDistortion::~CDistortion(void)
{
}


CDistortion &CDistortion::operator = (const CDistortion & d)
{
	this->radialDistortion = d.radialDistortion;
	this->type = d.type;
	return *this;
}


void CDistortion::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
/*
	CToken* pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("nCols",this->nCols);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("name",this->name.GetChar());

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("nRows",this->nCols);
*/
	
}

void CDistortion::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
/*	
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("nCols"))
			this->nCols = token->getIntValue();
		else if(key.CompareNoCase("nRows"))
            this->nRows = token->getIntValue();
		else if(key.CompareNoCase("name"))
            this->name.Set(token->getValue());
    }
*/


}

 void CDistortion::reconnectPointers()
 {
	 CSerializable::reconnectPointers();
 }

 void CDistortion::resetPointers()
 {
	 CSerializable::resetPointers();

}