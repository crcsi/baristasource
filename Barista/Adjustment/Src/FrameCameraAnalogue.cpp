#include "FrameCameraAnalogue.h"
#include "Trans2DAffine.h"


CFrameCameraAnalogue::CFrameCameraAnalogue(void)
{
	this->type = eAnalogueFrame;
	this->fiducialMarks.setParent(this);
}

CFrameCameraAnalogue::CFrameCameraAnalogue(const CFrameCameraAnalogue& src) : CFrameCamera(src),
	fiducialMarks(src.fiducialMarks)
{
	this->type = eAnalogueFrame;
}

CFrameCameraAnalogue::~CFrameCameraAnalogue(void)
{
}


CFrameCameraAnalogue &CFrameCameraAnalogue::operator = (const CFrameCameraAnalogue & c)
{
	bool changeListenerStatus = false;
	if (this->callListener)
	{
		this->callListener = false;
		changeListenerStatus = true;
	}


	CFrameCamera::operator=(c);
	this->type = eAnalogueFrame;
	this->fiducialMarks = c.fiducialMarks;

	if (changeListenerStatus)
		this->callListener = true;

	if (this->callListener)
		this->informListeners_elementsChanged(this->getClassName().c_str());   

	return *this;
}


CFrameCameraAnalogue &CFrameCameraAnalogue::operator = (const CFrameCamera & c)
{
	bool changeListenerStatus = false;
	if (this->callListener)
	{
		this->callListener = false;
		changeListenerStatus = true;
	}

	CFrameCamera::operator=(c);
	this->type = eAnalogueFrame;
	
	if (changeListenerStatus)
		this->callListener = true;

	if (this->callListener)
		this->informListeners_elementsChanged(this->getClassName().c_str());  

	return *this;
}

bool CFrameCameraAnalogue::transformObs2Obj(C3DPoint& xc,  const C3DPoint& obs, double Z0, double Z, 
											const CTrans2D* frameToCamera) const
{

	if (!frameToCamera)
		return false;

	C2DPoint in(obs.x,obs.y),out;
	
	frameToCamera->transform(out,in);

	xc.x  = out.x - this->interiorReferencePoint.x;
	xc.y  = out.y - this->interiorReferencePoint.y;
	xc.z =  this->interiorReferencePoint.z;

	if (this->distortion)
		this->distortion->applyObs2Obj(xc,obs,this->interiorReferencePoint,this->scale);

	double dx, dy;
	computeRefractionAndEarthCurvature(Z0, Z, 6379000.0, xc.x, xc.y, dx, dy);
	xc.x += dx;
	xc.y += dy;

	return true;
}

bool CFrameCameraAnalogue::transformObj2Obs(C2DPoint& obs, const C3DPoint& obj, double Z0, double Z, const CTrans2D* cameraToFrame) const
{
	if (!cameraToFrame)
		return false;

	C2DPoint obsTmp(obj.x / obj.z,obj.y / obj.z);


	obsTmp = this->interiorReferencePoint.z * obsTmp;

	double dx, dy;

	computeRefractionAndEarthCurvature(Z0, Z, 6379000.0, obsTmp.x, obsTmp.y, dx, dy);
	obsTmp.x -= dx;
	obsTmp.y -= dy;

	if (this->distortion)
		this->distortion->applyObj2Obs(obsTmp,obsTmp,this->interiorReferencePoint,this->scale);

	
	obsTmp.x += this->interiorReferencePoint.x;
	obsTmp.y += this->interiorReferencePoint.y;

	// transition to framlet cs
	cameraToFrame->transform(obs,obsTmp);

	return true;
}



void CFrameCameraAnalogue::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);
	
	// add first all the common things
	CCamera::serializeStore(pStructuredFileSection);

	// and now the private members
	CToken* pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("calibrationDay",this->calibrationDay);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("calibrationMonth",this->calibrationMonth);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("calibrationYear",this->calibrationYear);


	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("objectiveName",this->objectiveName.GetChar());

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->irpSymmetry.serializeStore(child);

	child = pStructuredFileSection->addChild();
    this->fiducialMarks.serializeStore(child);

}

void CFrameCameraAnalogue::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	// add first all the common things
	CCamera::serializeRestore(pStructuredFileSection);

	// and now the private members
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("calibrationDay"))
			this->calibrationDay = token->getIntValue();
		else if(key.CompareNoCase("calibrationMonth"))
            this->calibrationMonth = token->getIntValue();
		else if(key.CompareNoCase("calibrationYear"))
            this->calibrationYear = token->getIntValue();
		else if(key.CompareNoCase("objectiveName"))
            this->objectiveName = token->getValue();

    }


	
   for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CXYPoint"))
        {
            this->irpSymmetry.serializeRestore(child);
        }
		else if (child->getName().CompareNoCase("CXYPointArray"))
        {
            this->fiducialMarks.serializeRestore(child);
        }

    }
}

bool CFrameCameraAnalogue::updateFiducialMarkLabel(CLabel label,int index)
{
	if (!this->checkFiducialMarkLabel(label))
		return false;

	if (index >= this->fiducialMarks.GetSize())
		return false;

	this->fiducialMarks.getAt(index)->setLabel(label);
	return true;
}

bool CFrameCameraAnalogue::updateFiducialMarkX(double x,int index)
{
	if (index >= this->fiducialMarks.GetSize())
		return false;

	this->fiducialMarks.getAt(index)->x = x;
	return true;
}

bool CFrameCameraAnalogue::updateFiducialMarkY(double y,int index)
{
	if (index >= this->fiducialMarks.GetSize())
		return false;

	this->fiducialMarks.getAt(index)->y = y;
	return true;
}


bool CFrameCameraAnalogue::addFiducialMark(const CXYPoint& fiducialMark)
{
	if (!this->checkFiducialMarkLabel(fiducialMark.getLabel()))
		return false;

	this->fiducialMarks.Add(fiducialMark);
	return true;
}

bool CFrameCameraAnalogue::checkFiducialMarkLabel(CLabel label) const
{
	if (!label.GetLength())
		return false;

	// check if we go the label already
	for (int i=0; i< this->fiducialMarks.GetSize(); i++)
		if (this->fiducialMarks.GetAt(i)->getLabel() == label)
			return false;

	return true;
}

bool CFrameCameraAnalogue::deleteFiducialMark(int index)
{

	if (index >= this->fiducialMarks.GetSize())
		return false;

	this->fiducialMarks.RemoveAt(index,1);
	return true;
}
