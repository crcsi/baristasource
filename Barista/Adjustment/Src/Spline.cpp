#include "Spline.h"
#include "newmat.h"
#include "XYZPoint.h"
#include "AdjustMatrix.h"

CSpline::CSpline(void)
{
	this->segments.SetSize(0);
	this->knots.resize(0);
	this->degree=0;

	this->segments.setParent(this);
}

CSpline::CSpline(const CSpline& spline):segments(spline.segments),
	knots(spline.knots),name(spline.name),degree(spline.degree)
{
	this->segments.setParent(this);
}

CSpline &CSpline::operator = (const CSpline & spline)
{
	this->segments.Copy(spline.segments);
	this->knots = spline.knots;
	this->name = spline.name;
	this->degree = spline.degree;

	// set the times of the spline segments correct
	for (int i= 0; i < this->segments.GetSize(); i++)
	{
		CSplineSegment *seg= this->segments.GetAt(i);
		seg->setStartTime(&this->knots[i]);
		seg->setEndTime(&this->knots[i+1]);
	}

	return *this;
}

CSpline::~CSpline(void)
{
}

bool CSpline::init(int degree, const CFileName& Name,doubles& knots)
{
	int nKnots = knots.size();
	
	if (nKnots < 2)
		return false;

	int nSegments = nKnots -1;
	this->knots.resize(nKnots);

	for (int i=0; i < nKnots; i++)
		this->knots[i] = knots[i];

	if (degree < 1)
		return false;

	this->degree = degree;
	this->name = Name;

	this->segments.SetSize(0);
	for (int i=0; i < nSegments; i++)
	{	CSplineSegment seg(degree,&this->knots[i],&this->knots[i+1]);
		this->segments.Add(seg);
	}

	return true;
}


int CSpline::findSegment(const double time) const
{
	int low = 0;
	int heigh = this->knots.size() -1;

	if (time < this->knots[low] || time > this->knots[heigh])
	{
		// check if just numeric
		if (time < this->knots[low]) // we are to early
		{	
			if (fabs (time - this->knots[low]) > 0.00001)  // not even close
				return -1;
		}
		else if ( time > this->knots[heigh]) // we are to late
		{
			if (fabs(time - this->knots[heigh]) > 0.00001)  // 0.2 was set to fix GA's xyz-lines problem
				return -2;
		}

	}

	int mid = (heigh + low)/2;

	while ( (heigh-low) > 1)
	{
		if (this->knots[mid] > time)
			heigh = mid;
		else
			low = mid;
		mid = (heigh+low)/2;
	}

	return low;
}

bool CSpline::evaluate(CObsPoint& erg, const double time)const
{
	int index = this->findSegment(time);

	C3DPoint location;
	C3DPoint velocity;
	CSplineSegment* seg = NULL;

	if ( index < 0)
	{
		if (index == -1) // time before spline starts
		{
			seg = this->segments.GetAt(0);
		}
		else if (index == -2) // time after spline ends
		{
			seg = this->segments.GetAt(this->segments.GetSize() - 1);
		}

		seg->extrapolateLocation(location,time);
		seg->extrapolateVelocity(velocity,time);

		return false; // return always false to show that we are outside !!
	}
	else
	{
		seg = this->segments.GetAt(index);
		seg->evaluateLocation(location,time);
		seg->evaluateVelocity(velocity,time);
	}

	erg[0] = location.x;
	erg[1] = location.y;
	erg[2] = location.z;
	erg[3] = velocity.x;
	erg[4] = velocity.y;
	erg[5] = velocity.z;

	return true;

}

bool CSpline::evaluateVelocity(C3DPoint& erg,const double time)const
{
	int index = this->findSegment(time);
	CSplineSegment* seg = NULL;

	if ( index < 0)
	{
		if (index == -1) // time before spline starts
		{
			seg = this->segments.GetAt(0);
		}
		else if (index == -2) // time after spline ends
		{
			seg = this->segments.GetAt(this->segments.GetSize() - 1);
		}

		seg->extrapolateVelocity(erg,time);

		return false; // return always false to show that we are outside !!
	}
	else
	{
		seg = this->segments.GetAt(index);
		seg->evaluateVelocity(erg,time);
	}
	
	return true;
}

bool CSpline::evaluateLocation(C3DPoint& erg,const double time)const
{
	int index = this->findSegment(time);
	CSplineSegment* seg = NULL;

	if ( index < 0)
	{
		if (index == -1) // time before spline starts
		{
			seg = this->segments.GetAt(0);
		}
		else if (index == -2) // time after spline ends
		{
			seg = this->segments.GetAt(this->segments.GetSize() - 1);
		}

		seg->extrapolateLocation(erg,time);

		return false; // return always false to show that we are outside !!
	}
	else
	{
		seg = this->segments.GetAt(index);
		seg->evaluateLocation(erg,time);
	}

	return true;
}





bool  CSpline::Ai(CAdjustMatrix &AiPar,const double &time,int& segIndex) const
{
	segIndex = this->findSegment(time);

	if (segIndex < 0)
	{
		if (segIndex == -1) // time before spline starts
		{

		}
		else if (segIndex == -2) // time after spline ends
		{

		}

		return false;  // always return false, the spline model uses return value!!!
	}
	else
	{
		CSplineSegment* seg = this->segments.GetAt(segIndex);
		seg->Ai(AiPar,time);
	}

	return true;
}

int CSpline::getDegree()const
{
	if (this->getNumberOfSegments() < 1)
		return 0;
	else
		return this->segments.GetAt(0)->getDegree();
}



void CSpline::addConstraint(Matrix* N, Matrix* t, Matrix* parms,int constraintsIndex, const int colIndex)const
{
	int nSegments = this->getNumberOfSegments();
	int nConstraints = nSegments -1;
	int nCoefficients  = this->getDegree()+1;
	
	if (nConstraints < 1 || nCoefficients < 2)
		return;

	int rows = this->getDegree();	
	int cols = nCoefficients*2;
	
	for (int i=0; i < nConstraints; i++)
	{
		double dt = this->knots[i+1] - this->knots[i];
		double scale = 1.0/dt;
		CAdjustMatrix H(rows,cols,0.0);
		H(0,0) = 1;

		// compute constraints for one coordinate
		for (int k=1; k < nCoefficients; k++)
		{
			H(0,k)= 1;					// function value first spline
			if (rows > 1)
			{
				H(1,k)= H(0,k-1)*scale*k;				// first derivation first spline
				if (rows > 2)
					H(2,k)= H(1,k-1)*scale*k;				// secound derivation first spline
			}
		}

		double dt2 = 1.0 / (this->knots[i+2] - this->knots[i+1]);
		H(0,nCoefficients) = -1;		 // function value first spline	
		if (rows > 1)
		{
			H(1,nCoefficients + 1) = -1*dt2;	 // first derivation first spline
			if (rows > 2)
				H(2,nCoefficients + 2) = -2*dt2 * dt2 ;	 // secound derivation first spline
		}
		
		// add for X,Y,Z in N-Matrix
		H.addToMatrix(constraintsIndex +                  i*rows,colIndex + nCoefficients*i                          ,*N);	// X
		H.addToMatrix(constraintsIndex + rows*nConstraints + i*rows,colIndex + nCoefficients*i+nCoefficients*nSegments  ,*N);	// Y
		H.addToMatrix(constraintsIndex + rows*2*nConstraints + i*rows,colIndex + nCoefficients*i+nCoefficients*nSegments*2,*N);	// Z
		
		H.transpose();
		H.addToMatrix(colIndex + nCoefficients*i                          ,constraintsIndex +                  i*rows,*N);	// X
		H.addToMatrix(colIndex + nCoefficients*i+nCoefficients*nSegments  ,constraintsIndex + rows*nConstraints + i*rows,*N);	// Y
		H.addToMatrix(colIndex + nCoefficients*i+nCoefficients*nSegments*2,constraintsIndex + rows*2*nConstraints + i*rows,*N);	// Z

	}

}


bool CSpline::insertParametersForAdjustment(Matrix &parms,const int addParIndex) const
{
	int nSegments = this->getNumberOfSegments();
	int nCoefficients = this->getDegree() + 1;

	int startX = addParIndex;
	int startY = startX + nCoefficients * nSegments;
	int startZ = startY + nCoefficients * nSegments;

	for (int i=0; i < nSegments; i++)
	{
		this->segments.GetAt(i)->getCoefficients(parms,startX,startY,startZ);
		startX += nCoefficients;
		startY += nCoefficients;
		startZ += nCoefficients;
		
	}
	return true;
}

bool CSpline::setParametersFromAdjustment(const Matrix &parms,const int addParIndex)
{
	int nSegments = this->getNumberOfSegments();
	int nCoefficients = this->getDegree() + 1;

	int startX = addParIndex;
	int startY = startX + nCoefficients * nSegments;
	int startZ = startY + nCoefficients * nSegments;

	for (int i=0; i < nSegments; i++)
	{
		this->segments.GetAt(i)->updateCoefficients(parms,startX,startY,startZ);
		startX += nCoefficients;
		startY += nCoefficients;
		startZ += nCoefficients;
		
	}

	return true;
}

bool CSpline::getAdjustableParameters(Matrix &parms,const int addParIndex) const
{
	return this->insertParametersForAdjustment(parms,addParIndex);
}

void CSpline::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames,const CCharString unit,const CCharStringArray &ParameterNames) const
{

	int nSegments = this->getNumberOfSegments();
	int nCoefficients = this->getDegree() + 1;

	char buffer[200];

	if (nSegments && nCoefficients)
	{
		sprintf(buffer,"Spline Parameter             Parameter%s	       Sigma%s\n      Segment %3d (%5s) a%d",unit.GetChar(),unit.GetChar(),1,ParameterNames.GetAt(0)->GetChar(),0) ;
		StationNames.Add(buffer);
		for (int k=1; k < nCoefficients; k++)
		{

			sprintf(buffer,"Segment %3d (%5s) a%d",1,ParameterNames.GetAt(0)->GetChar(),k) ;
			StationNames.Add(buffer);
		}
	}

	for (int i= 1; i < nSegments; i++)
	{
			for (int k=0; k < nCoefficients; k++)
			{

				sprintf(buffer,"Segment %3d (%5s) a%d",i+1,ParameterNames.GetAt(0)->GetChar(),k) ;
				StationNames.Add(buffer);
			}
	}

	for (int i= 0; i < nSegments; i++)
	{
			for (int k=0; k < nCoefficients; k++)
			{
				sprintf(buffer,"Segment %3d (%5s) a%d",i+1,ParameterNames.GetAt(1)->GetChar(),k) ;
				StationNames.Add(buffer);
			}
	}	

	for (int i= 0; i < nSegments; i++)
	{
			for (int k=0; k < nCoefficients; k++)
			{
				sprintf(buffer,"Segment %3d (%5s) a%d",i+1,ParameterNames.GetAt(2)->GetChar(),k) ;
				StationNames.Add(buffer );
			}
	}
}

void CSpline::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CCharString valueStr;
	CCharString elementStr;

	CToken* pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("degree", this->degree);

	pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("name", this->name.GetChar());


	pNewToken = pStructuredFileSection->addToken();
    pNewToken->setValue("size of knots", (int)this->knots.size());

	// knot
	pNewToken = pStructuredFileSection->addToken();
	valueStr = "";

	for (unsigned int i = 0; i < this->knots.size(); i++)
		{
			double element = this->knots[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	pNewToken->setValue("knots", valueStr.GetChar());

	CStructuredFileSection* pNewSection = pStructuredFileSection->addChild();
	this->segments.serializeStore(pNewSection);
}

void CSpline::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* pToken = pStructuredFileSection->getTokens()->GetAt(i);

		if (pToken->isKey("degree"))
			this->degree = pToken->getIntValue();
		else if (pToken->isKey("name"))
			this->name = pToken->getValue();
		else if (pToken->isKey("size of knots"))
			this->knots.resize(pToken->getIntValue());
		else if (pToken->isKey("knots"))
		{
			if (this->knots.size()> 0)
				pToken->getDoublesClass(this->knots);	
		}
	}


    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CSplineSegmentArray"))
        {	
			this->segments.serializeRestore(child);
		}
    }


	// set the times of the spline segments correct

	for (int i= 0; i < this->segments.GetSize(); i++)
	{
		CSplineSegment *seg= this->segments.GetAt(i);
		seg->setStartTime(&this->knots[i]);
		seg->setEndTime(&this->knots[i+1]);
	}

}

void CSpline::applyScaleToOldProject(double scale)
{
	for (int i= 0; i < this->segments.GetSize(); i++)
	{
		CSplineSegment *seg= this->segments.GetAt(i);
		seg->applyScaleToOldProject(scale);
	}
};
	
bool CSpline::isModified()
{
	return this->bModified;
}

void CSpline::reconnectPointers()
{
	CSerializable::reconnectPointers();
	this->segments.reconnectPointers();
}

void CSpline::resetPointers()
{
	CSerializable::resetPointers();
	this->segments.resetPointers();
}



bool CSpline::backProjection(double &time,const C3DPoint& objPoint,const C3DPoint &n)
{

	if (this->knots.size() < 2)
		return false;

	int low = 0;
	int heigh = this->knots.size()-1;
	int mid = (heigh + low) /2;

	if (mid == low) mid++;

	C3DPoint lowPoint;
	C3DPoint midPoint;

	if (!this->evaluateLocation(lowPoint,this->knots[low]) || 
		!this->evaluateLocation(midPoint,this->knots[mid]))
		return false;

	double a = -((lowPoint-objPoint).innerProduct(n));
	a /= (midPoint-lowPoint).innerProduct(n);

	while ( (heigh - low) > 1 )
	{
		if (a < 0.0 )
			return false;
		else if (a > 1.0)
			low = mid;
		else
			heigh = mid;

		mid = (heigh + low) /2;
		if (mid == low) mid++;

		if (!this->evaluateLocation(lowPoint,this->knots[low]) || 
		!this->evaluateLocation(midPoint,this->knots[mid]))
		return false;

		a = -((lowPoint-objPoint).innerProduct(n));
		a /= (midPoint-lowPoint).innerProduct(n);

	}

	time = this->knots[low] + a*(this->knots[mid] - this->knots[low]);
	return true;
}

bool CSpline::getSplineParameter(int segmentIndex,int degree,double& parameterX,double& parameterY,double& parameterZ)
{
	if (segmentIndex >= this->getNumberOfSegments())
		return false;

	
	this->segments.GetAt(segmentIndex)->getCoefficients(degree,parameterX,parameterY,parameterZ);
	return true;
}

double CSpline::getFirstTime()
{
	if (this->knots.size())
		return this->knots[0];
	else
		return 0.0;
}

double CSpline::getLastTime()
{
	if (this->knots.size())
		return this->knots[this->knots.size() - 1];
	else
		return 0.0;
}