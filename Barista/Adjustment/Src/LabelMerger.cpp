/**
 * @class CLabelMerger
 * Title:        Barista
 * Description:
 * Copyright:    Copyright (c) Harry Hanley
 * Company:      University of Melbourne
 * @author Harry Hanley
 * @version 1.0
 *
 * The purpose of this class is to build a superset of Labels from
 * the JXYPoints in an array of JXYScenes
 */

#include "LabelMerger.h"
#include "CharStringArray.h"
#include "XYPointArrayPtrArray.h"
#include "XYZPointArrayPtrArray.h"
#include "XYZOrbitPointArray.h"
#include "ObservationHandlerPtrArray.h"

CLabelMerger::CLabelMerger(void)
{
    this->sightings = NULL;
}

CLabelMerger::~CLabelMerger(void)
{
}

/**
    * Gets a Vector of JStringLabels representing the superset of all labels
    */
void CLabelMerger::merge(CCharStringArray* labels, CXYPointArrayPtrArray* XYPointArrays)
{
    labels->RemoveAll();

    if (XYPointArrays == NULL)
        return;

    // First make sure all the points are sorted by label
    int n = XYPointArrays->GetSize();

    for (int i = 0; i < n; i++)
    {
        CXYPointArray* Points = XYPointArrays->GetAt(i);
        Points->sortByLabel();
    }

    // Seed the process with a beginning label
    CXYPointArray* StartPoints = XYPointArrays->GetAt(0);

    CCharString highLabel = StartPoints->GetAt(0)->getLabel().GetChar();
    CCharString lowLabel = "ZZZZZZZ";

    // These are indexes to the current point in each XYScene
    //  They start at '0'
    int* index = new int[n]; //@@@
    ::memset(index, 0, n*sizeof(int));

    int lowIndex = -1;
    CCharString lastLow = "";

    int totalCount = 0;
    for (;;)
    {
        // Change the target
        lowLabel = highLabel;

        int finishedCount = 0;
        CCharString label = "";
        for (int i = 0; i < n; i++)
        {
            CXYPointArray* XYPointArray = XYPointArrays->GetAt(i);

            // First make sure we are not pointing beyond
            // the end of this XYScene's point array
            if (index[i] >= XYPointArray->GetSize())
            {
                // Keep track of how many Scenes are finished
                finishedCount++;
                continue;
            }

            CObsPoint* point = XYPointArray->GetAt(index[i]);
            label = point->getLabel().GetChar();

			if (point->getStatusBit(0))
			{
				index[i]++;
				continue;
			}

            if ((label.isLessThan(lowLabel) || label.CompareNoCase(lowLabel)))
            {
                lowIndex = i;
                lowLabel = label;
            }

            if (label.isGreaterThan(highLabel))
            {
                highLabel = label;
            }
        }

        // If all Scenes are finished, we are done
        if (finishedCount == n)
            break;

        // Increment the Scene with the lowLabel to its next point
        index[lowIndex]++;

        // Check that we don't doubley add a label
        if (lowLabel.CompareNoCase(lastLow))
        {
            CMUIntegerArray* sighting2 = this->sightings.GetAt(totalCount-1);
            //int* sighting = (int*)this->sightings[totalCount-1];
            //sighting[lowIndex] = 1;
            int* s = sighting2->GetAt(lowIndex);
            *s = 1;
            continue;
        }

        // Add the new label
        CCharString* newLabel = labels->Add();
        *newLabel = lowLabel;

        // Add to the sightings
        //int* sighting = new int[n];
        CMUIntegerArray* sighting = this->sightings.Add();
        sighting->SetSize(n);
        for (int ii = 0; ii < n; ii++)
        {
            int* s = sighting->GetAt(ii);
            *s = 0;
        }

        int* s = sighting->GetAt(lowIndex);
        *s = 1;
        //sighting[lowIndex] = 1;
        //this.sightings.add(sighting);
        totalCount++;

        // Keep track of the last low
        lastLow = lowLabel;
    }

    delete [] index;
}


void CLabelMerger::merge(CCharStringArray &labels, CObservationHandlerPtrArray &ObservationArrays)
{
    labels.RemoveAll();

    // First make sure all the points are sorted by label

    vector<CObservationHandler *> observationArr;
    for (int i = 0; i < ObservationArrays.GetSize(); i++)
    {
		CObservationHandler *h = ObservationArrays.GetAt(i);
        CObsPointArray *obsPoints = h->getObsPoints();
		if (h->hasUnknownObjPoints()) 
		{
			obsPoints->sortByLabel();
			observationArr.push_back(h);
		}
    }

	int n = observationArr.size();
	if (n < 1) return;

    // Seed the process with a beginning label
    CObservationHandler* StartObs = observationArr[0];

    CCharString highLabel = StartObs->getLabel(0).GetChar();

    CCharString lowLabel = "ZZZZZZZ";

    // These are indexes to the current point in each XYScene
    //  They start at '0'
    int* index = new int[n]; //@@@
    ::memset(index, 0, n*sizeof(int));

    int lowIndex = -1;
    CCharString lastLow = "";

    int totalCount = 0;
    for (;;)
    {
        // Change the target
        lowLabel = highLabel;

        int finishedCount = 0;
        CCharString label = "";
        for (int i = 0; i < n; i++)
        {
            CObservationHandler* oh = observationArr[i];

            // First make sure we are not pointing beyond
            // the end of this XYScene's point array
            if (index[i] >= oh->nObs())
            {
                // Keep track of how many Scenes are finished
                finishedCount++;
                continue;
            }

            label = oh->getLabel(index[i]).GetChar();

            if (!oh->isActive(index[i]))
			{
				// search for next valid point
				while (index[i] < oh->nObs() && !oh->isActive(index[i]))
					index[i]++;

				if (index[i] >= oh->nObs())
				{
					// Keep track of how many Scenes are finished
					finishedCount++;
					continue;
				}

				label = oh->getLabel(index[i]).GetChar();
			}


            if ((label.isLessThan(lowLabel) || label.CompareNoCase(lowLabel)))
            {
				lowIndex = i;
				lowLabel = label;
            }

            if (label.isGreaterThan(highLabel))
            {
                highLabel = label;
            }
        }

        // If all Scenes are finished, we are done
        if (finishedCount == n)
            break;

        // Increment the Scene with the lowLabel to its next point
        index[lowIndex]++;

        // Check that we don't doubley add a label
        if (lowLabel.CompareNoCase(lastLow))
        {
            CMUIntegerArray* sighting2 = this->sightings.GetAt(totalCount-1);
            //int* sighting = (int*)this->sightings[totalCount-1];
            //sighting[lowIndex] = 1;
            int* s = sighting2->GetAt(lowIndex);
            *s = 1;
            continue;
        }

        // Add the new label
        CCharString* newLabel = labels.Add();
        *newLabel = lowLabel;

        // Add to the sightings
        //int* sighting = new int[n];
        CMUIntegerArray* sighting = this->sightings.Add();
        sighting->SetSize(n);
        for (int ii = 0; ii < n; ii++)
        {
            int* s = sighting->GetAt(ii);
            *s = 0;
        }

        int* s = sighting->GetAt(lowIndex);
        *s = 1;
        //sighting[lowIndex] = 1;
        //this.sightings.add(sighting);
        totalCount++;

        // Keep track of the last low
        lastLow = lowLabel;
    }

    delete [] index;
}