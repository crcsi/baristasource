/**
 * @class CPushBroomScanner
 * CPushBroomScanner stores and handles pushbroom scanners
 */

#include <math.h>
#include <float.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <strstream>

#include "newmat.h"

#include "PushbroomScanner.h"
#include "utilities.h"

#include "2DPoint.h"
#include "3DPoint.h"

#include "2DPointArray.h"
#include "3DPointArray.h"
#include "3DPoint.h"
#include "ReferenceSystem.h"
#include "XYPoint.h"
#include "XYZPoint.h"
#include "XYPointArray.h"
#include "XYZPointArray.h"
#include "RotationBaseFactory.h"

#include "TextFile.h"
#include "XYZPointArray.h"
#include "AdjustMatrix.h"
#include "CCDLine.h"
#include "RPC.h"
#include "3Dplane.h"
#include "3DadjPlane.h"
#include "XYZLinesAdj.h"

//================================================================================

CPushBroomScanner::CPushBroomScanner() : CSensorModel(false, "Pushbroom Scanner", 0, 0, 0, 3, 3, 0, 6,  0, 2),
	path(0), attitudes(0),camera(0),cameraMounting(0),timeFixedECRMatrix(0.0),
	normalVectorCCDLine(0,0,0),refractionCorrection(0,0,0),aberrationCorrection(0,0,0),satelliteHeight(0.0),
	groundHeight(0.0),groundTemperature(0.0),firstLineTime(0.0),timePerLine(0.0), changeMountingForMerging(true)
{
	this->refSystem.setParent(this);
	this->deactivateParameterGroup(mountShiftPar);
	this->deactivateParameterGroup(mountRotPar);
	this->deactivateParameterGroup(addPar);
//	this->activateParameter(addPar,PARAMETER_1 + PARAMETER_2 + PARAMETER_3 + PARAMETER_4 + PARAMETER_5 + PARAMETER_6);
}

//================================================================================

CPushBroomScanner::CPushBroomScanner(const CPushBroomScanner &scanner) : CSensorModel(scanner),
	path(scanner.path), attitudes(scanner.attitudes),camera(scanner.camera),
	cameraMounting(scanner.cameraMounting),fixedECRMatrix(scanner.fixedECRMatrix),
	normalVectorCCDLine(scanner.normalVectorCCDLine),timeFixedECRMatrix(scanner.timeFixedECRMatrix),
	refractionCorrection(scanner.refractionCorrection),aberrationCorrection(scanner.aberrationCorrection),
	satelliteHeight(scanner.satelliteHeight),groundHeight(scanner.groundHeight),groundTemperature(scanner.groundTemperature),
	firstLineTime(scanner.firstLineTime),timePerLine(scanner.timePerLine), changeMountingForMerging(scanner.changeMountingForMerging)
{ 
	this->refSystem.setParent(this);

}

//================================================================================

CPushBroomScanner & CPushBroomScanner::operator = (const CPushBroomScanner & scanner)
{
	CSensorModel::operator =(scanner); //
	this->fixedECRMatrix = scanner.fixedECRMatrix;
	this->normalVectorCCDLine = scanner.normalVectorCCDLine;
	this->timeFixedECRMatrix = scanner.timeFixedECRMatrix;
	this->refractionCorrection =scanner.refractionCorrection;
	this->aberrationCorrection = scanner.aberrationCorrection;
	this->satelliteHeight = scanner.satelliteHeight;
	this->groundHeight = scanner.groundHeight;
	this->groundTemperature = scanner.groundTemperature;
	this->firstLineTime = scanner.firstLineTime;
	this->timePerLine = scanner.timePerLine;
	this->changeMountingForMerging = scanner.changeMountingForMerging;
	this->informListeners_elementsChanged(this->getClassName().c_str());

	return *this;
}

//================================================================================

CPushBroomScanner::CPushBroomScanner(const Matrix &parms) : 
			CSensorModel(true,"Pushbroom Scanner", 0, 0, 0, 0, 0, 0, 0,  0, 2),
			changeMountingForMerging(true)
{
	this->refSystem.setParent(this);
}

//================================================================================

CPushBroomScanner::~CPushBroomScanner()
{
}

//================================================================================

bool CPushBroomScanner::init(CSensorModel* path,
							 CSensorModel* attitudes,
							 CCameraMounting * cameraMounting,
							 CCCDLine* camera,
							 const CFileName& filename,
							 double satelliteHeight,
							 double groundHeight,
							 double groundTemperature,
							 double firstLineTime,
							 double timePerLine)
{
	this->satelliteHeight = satelliteHeight;
	this->groundHeight = groundHeight;
	this->groundTemperature = groundTemperature;

	this->initOrbitPathModel(path);
	this->initOrbitAttitudeModel(attitudes);
	this->initMounting(cameraMounting);
	this->initCamera(camera);
	this->setFileName(filename);
	this->firstLineTime = firstLineTime;
	this->timePerLine = timePerLine;

	// init reference system

	
	C3DPoint x;
	if (!this->path->evaluateLocation(x,this->timeFixedECRMatrix))
		return false;
	this->refSystem.geocentricToGeographic(x,x);
	this->refSystem.setUTMZone(x.x,x.y);



	this->refSystem.setCoordinateType(eGEOCENTRIC);
	this->refSystem.setToWGS1984();

	this->path->setRefSys(this->refSystem);
	this->attitudes->setRefSys(this->refSystem);


	C3DPoint erg1,erg2;
	C3DPoint firstCCD(0,0,0);
	C3DPoint lastCCD(this->camera->getNCols()-1,0,0);

	if (!this->transformObs2Obj(erg1,firstCCD) || !this->transformObs2Obj(erg2,lastCCD))
		return false;


	// ueberarbeiten !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	C3DPoint x0;
	if (!this->path->evaluateLocation(x0,erg1.dot))
		return false;

	erg1 -= x0;
	erg2 -= x0;
	// ueberarbeiten !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	this->normalVectorCCDLine = erg1.crossProduct(erg2);
	this->normalVectorCCDLine /= this->normalVectorCCDLine.getNorm();

	C3DPoint refr(0,0,0);
/*	
	
	if (!this->computeRefractionCorrection(refr))
		return false;


	refr.x = this->cameraCurrent.getNCols()/2;
	if (!this->computeRefractionCorrection(refr))
		return false;


	refr.x = this->cameraCurrent.getNCols();
	if (!this->computeRefractionCorrection(refr))
		return false;


	refr.x = 0;
	refr.y = this->cameraCurrent.getLine(this->timeFixedECRMatrix);
	if (!this->computeRefractionCorrection(refr))
		return false;

*/
	refr.y = this->getLine(this->timeFixedECRMatrix);
	refr.x = this->camera->getNCols()/2;
	if (!this->computeRefractionCorrection(refr))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",refr.x,refr.y,this->refractionCorrection.x,this->refractionCorrection.y);

/*
	refr.y = 0 ;
	if (!this->computeRefractionCorrection(refr))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",refr.x,refr.y,this->refractionCorrection.x,this->refractionCorrection.y);

	refr.x = 47000;

	if (!this->computeRefractionCorrection(refr))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",refr.x,refr.y,this->refractionCorrection.x,this->refractionCorrection.y);

	refr.x = this->cameraCurrent.getNCols()/2;
	if (!this->computeRefractionCorrection(refr))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",refr.x,refr.y,this->refractionCorrection.x,this->refractionCorrection.y);

	refr.x = this->cameraCurrent.getNCols();
	if (!this->computeRefractionCorrection(refr))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",refr.x,refr.y,this->refractionCorrection.x,this->refractionCorrection.y);

*/


	C3DPoint aber(0,0,0);
/*
//	fprintf(prot,"\n\ncomputation of aberration correction\n");
	if (!this->computeAberrationCorrection(aber))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",aber.x,aber.y,this->aberrationCorrection.x,this->aberrationCorrection.y);

	aber.x = this->cameraCurrent.getNCols()/2;
	if (!this->computeAberrationCorrection(aber))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",aber.x,aber.y,this->aberrationCorrection.x,this->aberrationCorrection.y);

	aber.x = this->cameraCurrent.getNCols();
	if (!this->computeAberrationCorrection(aber))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",aber.x,aber.y,this->aberrationCorrection.x,this->aberrationCorrection.y);

	aber.x = 0;
	aber.y = this->cameraCurrent.getLine(this->timeFixedECRMatrix);
	if (!this->computeAberrationCorrection(aber))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",aber.x,aber.y,this->aberrationCorrection.x,this->aberrationCorrection.y);
*/
	aber.y = this->getLine(this->timeFixedECRMatrix);
	aber.x = this->camera->getNCols()/2;
	if (!this->computeAberrationCorrection(aber))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",aber.x,aber.y,this->aberrationCorrection.x,this->aberrationCorrection.y);
/*
	aber.x = this->cameraCurrent.getNCols();
	if (!this->computeAberrationCorrection(aber))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",aber.x,aber.y,this->aberrationCorrection.x,this->aberrationCorrection.y);

	aber.x = 0;
	aber.y = this->cameraCurrent.getLine(this->timeFixedECRMatrix)*2-2;
	if (!this->computeAberrationCorrection(aber))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",aber.x,aber.y,this->aberrationCorrection.x,this->aberrationCorrection.y);

	aber.x = this->cameraCurrent.getNCols()/2;
	if (!this->computeAberrationCorrection(aber))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",aber.x,aber.y,this->aberrationCorrection.x,this->aberrationCorrection.y);

	aber.x = this->cameraCurrent.getNCols();
	if (!this->computeAberrationCorrection(aber))
		return false;
//	fprintf(prot,"Correction pos(%6.0lf,%6.0lf):  %7.3lf  %7.4lf\n",aber.x,aber.y,this->aberrationCorrection.x,this->aberrationCorrection.y);

*/
	this->sethasValues(true);
//	fclose(prot);
	return true;


}

//================================================================================

bool CPushBroomScanner::init(CSensorModel* path,
							 CSensorModel* attitudes,
							 CCameraMounting * cameraMounting,
							 CCCDLine* camera,
							 const CFileName& filename,
							 double satelliteHeight,
							 double groundHeight,
							 double groundTemperature,
							 double timePerLine, 
							 double inTrackViewAngle,
							 double crossTrackViewAngle,
							 C3DPoint UL,
							 C3DPoint UR,
							 C3DPoint LL,
							 C3DPoint LR)
{

	double firstLineTime = 0.0;
	double lastLineTime = 1.0;
	double minTime,maxTime;

	COrbitPathModel* localPath = (COrbitPathModel*)path;
	COrbitAttitudeModel* localAttitudes = (COrbitAttitudeModel*)attitudes;

	this->initOrbitPathModel(localPath,firstLineTime,lastLineTime,inTrackViewAngle,crossTrackViewAngle,satelliteHeight,UL,UR,LL,LR,minTime,maxTime);
	this->initOrbitAttitudeModel(localAttitudes,minTime,maxTime);
	this->initMounting(cameraMounting,crossTrackViewAngle,inTrackViewAngle);
	

	localPath->initDirectObservations();
	localAttitudes->initDirectObservations();
	

	return this->init(path,attitudes,cameraMounting,camera,filename,satelliteHeight,groundHeight,groundTemperature,firstLineTime,timePerLine);
}

//================================================================================

void CPushBroomScanner::initCamera(CCCDLine* camera)
{
	this->camera = camera;
}

//================================================================================

void CPushBroomScanner::initMounting(CCameraMounting *cameraMounting,double crossTrackViewAngle,double inTrackViewAngle)
{
	C2DPoint angle(crossTrackViewAngle,inTrackViewAngle);
	angle *= M_PI / 180.0;


	C3DPoint I,J,K;
	K.x = tan(angle.y);
	K.y = -tan(angle.x);
	K.z = -1.0;
	K = K / K.getNorm();

	I.x = 0.0;
	I.y = cos(angle.x);
	I.z = -sin(angle.x);

	J = K.crossProduct(I);

	CRotationMatrix r(I,J,K);
	CRollPitchYawRotation rot(r);
	cameraMounting->setRotation(rot);
}

//================================================================================

void CPushBroomScanner::initMounting(CCameraMounting* cameraMounting)
{
	this->cameraMounting = cameraMounting;
}

//================================================================================

void CPushBroomScanner::initOrbitPathModel(CSensorModel* path)
{
	this->path = (COrbitPathModel*) path;
}

//================================================================================

void CPushBroomScanner::initOrbitPathModel(  COrbitPathModel* path,
											 double firstLineTime,
											 double lastLineTime,
											 double inTrackViewAngle,
											 double crossTrackViewAngle,
											 double satH,
											 C3DPoint UL,
											 C3DPoint UR,
											 C3DPoint LL,
											 C3DPoint LR,
											 double &minTime,
											 double &maxTime)
{

	this->refSystem.setToWGS1984();
	double R = this->refSystem.getSpheroid().calcRadius(UL.x);
	double R_sat = satH + R;

	this->refSystem.geographicToGeocentric(LL,LL);
	this->refSystem.geographicToGeocentric(LR,LR);
	this->refSystem.geographicToGeocentric(UL,UL);
	this->refSystem.geographicToGeocentric(UR,UR);

	// middle of first and last line on ground
	C3DPoint P1 = (UL + UR) / 2.0;
	C3DPoint P2 = (LL + LR) / 2.0;

	// at orbit height
	C3DPoint S1 = (R_sat) / R * P1;
	C3DPoint S2 = (R_sat) / R * P2;

	// view direction in sat system
	C3DPoint r1(tan(inTrackViewAngle * M_PI / 180.0),-tan(crossTrackViewAngle * M_PI / 180.0),-1);
	C3DPoint r2(tan(inTrackViewAngle * M_PI / 180.0),-tan(crossTrackViewAngle * M_PI / 180.0),-1);
	r1 /= r1.getNorm();
	r2 /= r2.getNorm();

	C3DPoint S1_old, S2_old;
	int maxIter = 10;
	int count =0;
	C3DPoint r1_g,r2_g;

	// iterate sat position in geocentric system
	do 
	{
		S1_old = S1;
		S2_old = S2;

		// compute rotation matrix from sat system to geocentric
		C3DPoint Z = S1 / S1.getNorm();
		C3DPoint Y = S1.crossProduct(S2);
		Y /= Y.getNorm();
		C3DPoint X = Y.crossProduct(Z);
		CRotationMatrix R1(X,Y,Z);

		Z = S2 / S2.getNorm();
		Y = S1.crossProduct(S2);
		Y /= Y.getNorm();
		X = Y.crossProduct(Z);
		CRotationMatrix R2(X,Y,Z);

		// rotation view direction to geocentric
		R1.R_times_x(r1_g,r1);
		R2.R_times_x(r2_g,r2);

		// intersect ray with sphere
		double p_2 = P1.innerProduct(r1_g);
		double q = P1.getSquareNorm() - R_sat * R_sat;

		double v = p_2*p_2 - q;

		if (v < 0)
			return;

		v = sqrt(v);

		double lamda1 = -p_2 - v;
		double lamda2 = -p_2 + v;

		double lamda =   fabs (lamda1) < fabs(lamda2) ? lamda1 : lamda2;

		// the new sat pos
		S1 = P1 + lamda * r1_g;
		
		// the same for second point

		p_2 = P2.innerProduct(r2_g);
		q = P2.getSquareNorm() - R_sat * R_sat;
		v = p_2*p_2 - q;

		if (v < 0)
			return;

		v = sqrt(v);
		lamda1 = -p_2 - v;
		lamda2 = -p_2 + v;
		lamda =  fabs (lamda1) < fabs(lamda2) ? lamda1 : lamda2;

		S2 = P2 + lamda * r2_g;
		count++;
	}
	while ((S1 - S1_old).getNorm() > 10 && (S2 - S2_old).getNorm() > 10 && count < maxIter);

	// compute point in the middle of scene
	C3DPoint Centre = S1 + 0.5 * (S2-S1);
	double lamda = (R_sat) / Centre.getNorm();
	C3DPoint offset =  lamda * Centre;

	C3DPoint velocity(S2-S1);
	CRotationMatrix fixedMatrix;

	C3DPoint X,Y,Z;
	Z = offset /  offset.getNorm();
	Y = Z.crossProduct(velocity);
	Y = Y / Y.getNorm();
	X = Y.crossProduct(Z);

	fixedMatrix(0,0) = X[0];
	fixedMatrix(1,0) = X[1];
	fixedMatrix(2,0) = X[2];

	fixedMatrix(0,1) = Y[0];
	fixedMatrix(1,1) = Y[1];
	fixedMatrix(2,1) = Y[2];

	fixedMatrix(0,2) = Z[0];
	fixedMatrix(1,2) = Z[1];
	fixedMatrix(2,2) = Z[2];

	this->fixedECRMatrix = fixedMatrix;	// save Rfixed!
	
	// we have now 3 points describing the orbit

	// compute vectors for sphere
	C3DPoint n = S1.crossProduct(S2);
	n /= n.getNorm();

	C3DPoint t = n.crossProduct(S1);
	t /= t.getNorm();
	t*= S1.getNorm();

	// the angle from between first and last line
	double angleScene = acos(S1.innerProduct(S2) / (S1.getNorm() * S2.getNorm()));
	double timeScene = lastLineTime - firstLineTime;
	
	double sceneSize = 0.1; // original size * sceneSize 
	angleScene *= sceneSize;
	timeScene *= sceneSize;
	
	minTime = firstLineTime - timeScene;
	maxTime = lastLineTime + timeScene;
	double scale = 1.0 / (maxTime - minTime);

	int nPoints = 6;
	double gapAngle = angleScene / double(nPoints);
	double gapTime = timeScene / double(nPoints);

	Matrix N ( 3,3);
	Matrix lhsX(3,1);
	Matrix lhsY(3,1);
	Matrix lhsZ(3,1);
	N = 0;
	lhsX =0;
	lhsY =0;
	lhsZ =0;


	// compute points along the path including S1 and S2
	for (int i=0; i <= nPoints; i++)
	{
		double before = -gapAngle * (nPoints - i);
		double after = angleScene / sceneSize + gapAngle * (i);

		C3DPoint pointBefore = cos(before)*S1 + sin(before)*t - offset;
		C3DPoint pointAfter = cos(after)*S1 + sin(after)*t - offset;
		
		double tBefore =(firstLineTime - gapTime * (nPoints -i) - minTime) * scale;
		double tAfter = (lastLineTime  + gapTime * (i)        - minTime) * scale;

		N.element(0,0) += 2;		  
		N.element(0,1) += tBefore + tAfter; 
		N.element(0,2) += tBefore*tBefore + tAfter*tAfter;

		N.element(1,0) = N.element(0,1);
		N.element(1,1) = N.element(0,2); 
		N.element(1,2) += tBefore*tBefore*tBefore + tAfter*tAfter*tAfter;

		N.element(2,0) = N.element(0,2);   
		N.element(2,1) = N.element(1,2); 
		N.element(2,2) += tBefore*tBefore*tBefore*tBefore + tAfter*tAfter*tAfter*tAfter;

		lhsX.element(0,0) += pointBefore.x + pointAfter.x;
		lhsX.element(1,0) += pointBefore.x * tBefore + pointAfter.x * tAfter;
		lhsX.element(2,0) += pointBefore.x * tBefore * tBefore + pointAfter.x * tAfter * tAfter;
		
		lhsY.element(0,0) += pointBefore.y + pointAfter.y;
		lhsY.element(1,0) += pointBefore.y * tBefore + pointAfter.y * tAfter;
		lhsY.element(2,0) += pointBefore.y * tBefore * tBefore + pointAfter.y * tAfter * tAfter;
		
		lhsZ.element(0,0) += pointBefore.z + pointAfter.z;
		lhsZ.element(1,0) += pointBefore.z * tBefore + pointAfter.z * tAfter;
		lhsZ.element(2,0) += pointBefore.z * tBefore * tBefore + pointAfter.z * tAfter * tAfter;
	}


	Matrix Qxx = N.i();
	Matrix polyX = Qxx * lhsX;
	Matrix polyY = Qxx * lhsY;
	Matrix polyZ = Qxx * lhsZ;


	double time = (firstLineTime - minTime) * scale;
	double test = polyX.element(0,0) + polyX.element(1,0)* time + polyX.element(2,0)* time * time +  offset.x;
	test = polyY.element(0,0) + polyY.element(1,0)* time + polyY.element(2,0)* time * time + offset.y;
	test = polyZ.element(0,0) + polyZ.element(1,0)* time + polyZ.element(2,0)* time * time + offset.z;


	Matrix param(9,1);
	param.element(0,0) = polyX.element(0,0);
	param.element(1,0) = polyX.element(1,0);
	param.element(2,0) = polyX.element(2,0);
	param.element(3,0) = polyY.element(0,0);
	param.element(4,0) = polyY.element(1,0);
	param.element(5,0) = polyY.element(2,0);
	param.element(6,0) = polyZ.element(0,0);
	param.element(7,0) = polyZ.element(1,0);
	param.element(8,0) = polyZ.element(2,0);


	doubles knots(2);
	knots[0] = minTime;
	knots[1] = maxTime;

	path->initSpline(2,*path->getFileName(),knots);
	CObsPoint offSet(3);
	offSet[0] = offset.x;
	offSet[1] = offset.y;
	offSet[2] = offset.z;
	path->setOffsetSpline(offSet);
	path->setParameterIndex(addPar,0);
	path->deactivateParameterGroup(shiftPar);
	path->deactivateParameterGroup(rotPar);
	path->setParametersFromAdjustment(param);
	path->setSatelliteHeight(R_sat,10000.0); // both values in m

}

//================================================================================

void CPushBroomScanner::initOrbitAttitudeModel(CSensorModel* attitudes)
{
	this->attitudes = (COrbitAttitudeModel*)attitudes;
	this->fixedECRMatrix = this->attitudes->getFixedECRMatrix();
	this->timeFixedECRMatrix = this->attitudes->getTimeFixedECRMatrix();
}

//================================================================================

void CPushBroomScanner::initOrbitAttitudeModel(COrbitAttitudeModel* attitudes,double firstLineTime,double lastLineTime)
{

	doubles knots(2);
	int degree = 2;
	knots[0] = firstLineTime;
	knots[1] = lastLineTime;

	attitudes->initSpline(degree,*attitudes->getFileName(),knots);
	attitudes->setParameterIndex(addPar,0);
	attitudes->deactivateParameterGroup(rotPar);
	attitudes->deactivateParameterGroup(scalePar);

	Matrix param(3*(degree + 1),1);
	param = 0.0;

	attitudes->setParametersFromAdjustment(param);
	// fixed matrix has been computed in initOrbitPathModel !!
	attitudes->setFixedECRMatrix(this->fixedECRMatrix);
	attitudes->setTimeFixedECRMatrix((firstLineTime + lastLineTime) * 0.5);

}

//================================================================================

const CRotationMatrix& CPushBroomScanner::getFixedMatrix()
{
	return this->fixedECRMatrix;
}

//================================================================================

void CPushBroomScanner::copy(const CPushBroomScanner &scanner)
{
	this->informListeners_elementsChanged();

};

//================================================================================

void CPushBroomScanner::setAll(const Matrix &parms)
{
    this->hasValues = true;


    this->informListeners_elementsChanged();
}

//================================================================================

double CPushBroomScanner::generateRPC(CRPC &rpc, int width, int height, float Zmin, float Zmax) const
{
	C2DPointArray imagePoints;   // array containing the image points
	C3DPointArray objectPoints;  // array containing the object points
	// homologous points in image and object space have the same array index

	double gridWidthImage       = 20.0; // grid width in image space; hard-coded as 20 pixels 
	double numberOfHeightLevels =  6.0; // Number of height levels in object space
	
	double dw = double(width  - 1) / gridWidthImage; 
	double dh = double(height - 1) / gridWidthImage;
	double dz = (Zmax - Zmin) / (numberOfHeightLevels - 1.0);

	// initialise a grid in image space with a grid width of gridWidthImage; there will be
	// numberOfHeightLevels such grids, each grid having the same position in image space,
	// but being projected to different height levels in object space
	for (double w = 0.0; w < width; w += dw)
	{ // loop over image columns
		for (double h = 0.0; h < height; h += dh)
		{ // loop over image rows
			for (double z = Zmin; z <= Zmax; z += dz)
			{ // loop over height levels;
			  // add the image point at (w,h) to the image point list
				C2DPoint *pImg = imagePoints.Add();
				pImg->x = w;
				pImg->y = h;	
			  // add the corresponding object point to the object point list
				C3DPoint *pObj = objectPoints.Add();
			  // project the image point at (w,h)  to height z in object space;
			  // there will be numberOfHeightLevels points having image coordinates (w,h)
				this->transformObs2Obj(*pObj, *pImg, z);
			  // transform the object point to geographic coordinates
				this->refSystem.geocentricToGeographic(*pObj, *pObj);
			}
		}
	}

	// initialise the rpc parameters from the homologous points
	return rpc.init(imagePoints, objectPoints, width, height);
};

//================================================================================

void CPushBroomScanner::transformObj2Obs(C2DPoint  &obs, const C3DPoint &obj) const
{
	double t;
	double t_old=0.0;
	double eps = 0.000001;

	t = this->timeFixedECRMatrix; // this time should be good enough

	
	obs.x = 0.0;
	obs.y = this->getLine(t);

	C3DPoint xc3D,xc2D;	
	CRotationBase* Rrpy = CRotationBaseFactory::initRotation(&C3DPoint(0,0,0),RPY);

	for (int i=0; i < 10 && fabs(t - t_old) > eps; i++)
	{
		t_old = t;

		this->compute3DImageVector(xc3D,t,obj,Rrpy);
		this->camera->transformObj2Obs(obs,xc3D);
		
		t = this->getTime(obs.y);
	}
	
	delete Rrpy;
	this->applyCorrections(obs);

}

//================================================================================

void CPushBroomScanner::applyCorrections(C2DPoint& obs) const
{
	obs.x -= this->refractionCorrection.x;
	obs.y -= this->refractionCorrection.y;

	obs.x += this->aberrationCorrection.x;
	obs.y += this->aberrationCorrection.y;
}

//================================================================================

void CPushBroomScanner::transformObj2Obs(C3DPoint  &obs, const C3DPoint &obj) const
{
	C2DPoint obs2D;
	transformObj2Obs(obs2D, obj);
	obs.x = obs2D.x;
	obs.y = obs2D.y;	
	obs.z = 0.0f;
}

//================================================================================

double CPushBroomScanner::getOffNadirAngle(int row) const
{
	C3DPoint xc,xp,xecr;
	C3DPoint rpy;
	C3DPoint xm;

	C3DPoint tmpObs(this->camera->getNCols(), 0.0, 0.0);

	tmpObs = tmpObs + this->refractionCorrection - this->aberrationCorrection;

	if (!this->camera->transformObs2Obj(xc,tmpObs))
		return false;

	// save the time 
	xc.dot = this->getTime(row);

	if (!this->attitudes->evaluateLocation(rpy,xc.dot)) return false;

	CRotationBase *rotRPY = CRotationBaseFactory::initRotation(&rpy,RPY);

	this->cameraMounting->getRotation().R_times_x(xm,xc);
	xp = this->cameraMounting->getShift() + xm; 

	rotRPY->R_times_x(xm,xp);
	delete rotRPY;
	
	xm /= xm.getNorm();


	
	return acos(fabs(xm.z));
}

//================================================================================

bool CPushBroomScanner::transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const
{
	obj.z = Height;

	C3DPoint xc,xp,xecr;
	C3DPoint rpy;
	C3DPoint x0;
	C3DPoint xm;

	C3DPoint tmpObs = obs + this->refractionCorrection;

	tmpObs -= this->aberrationCorrection; // obs + refCorrection - AbbCorrection

	if (!this->camera->transformObs2Obj(xc,tmpObs))
		return false;

	// save the time 
	xc.dot = this->getTime(tmpObs.y);

	if (!this->path->evaluateLocation(x0,xc.dot) ||	!this->attitudes->evaluateLocation(rpy,xc.dot))
		return false;

	CRotationBase *rotRPY = CRotationBaseFactory::initRotation(&rpy,RPY);

	this->cameraMounting->getRotation().R_times_x(xm,xc);
	xp = this->cameraMounting->getShift() + xm; 

	rotRPY->R_times_x(xm,xp);
	delete rotRPY;

	this->fixedECRMatrix.R_times_x(obj,xm);
	obj.dot = xc.dot;
	xm = obj;

	CSpheroid spheroid = this->refSystem.getSpheroid();
	double a2 = spheroid.getSemiMajor() + Height; a2 = 1.0 / (a2 * a2);
	double b2 = spheroid.getSemiMinor() + Height; b2 = 1.0 / (b2 * b2);

	double fact0 = (xm.x * xm.x + xm.y * xm.y) * a2 + (xm.z * xm.z) * b2;
	fact0 = 1.0 / fact0;
	double fact1 = ((x0.x * xm.x + x0.y * xm.y) * a2 + (x0.z * xm.z) * b2);  // * 2.0;
	double fact2 = (x0.x * x0.x + x0.y * x0.y) * a2 + (x0.z * x0.z) * b2 - 1.0;
	fact1 *= fact0; // * 0.5;
	fact2 *= fact0;

	double sq = fact1 * fact1 - fact2;
	if (sq < 0) return false;

	sq = sqrt(sq);

	double my1 = -fact1 + sq;
	double my2 = -fact1 - sq;

	double my = (fabs(my1) < fabs(my2)) ? my1 : my2;

	obj = x0 + my * xm;

	C3DPoint Geographic;
	
	this->refSystem.geocentricToGeographic(Geographic, obj);
		
	double dH = Height - Geographic.z;

	C3DPoint normal;
	C3DPoint Geocentric;

	for (int i = 0; i < 5 && fabs(dH) > 0.0005; ++i)
	{
		Geographic.x /= R2D;
		Geographic.y /= R2D;
		double cosphi = cos(Geographic.x);
		normal.x = cosphi * cos(Geographic.y);
		normal.y = cosphi * sin(Geographic.y);
		normal.z = sin(Geographic.x);
		Geocentric = obj + dH * normal - x0;

		double lambda = normal.innerProduct(Geocentric) / normal.innerProduct(xm);

		obj = x0 + lambda * xm;	
		this->refSystem.geocentricToGeographic(Geographic, obj);
		dH = Height - Geographic.z;
	}

	
	return (fabs(dH) < 0.0005);
}
	
//================================================================================

bool CPushBroomScanner::transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const
{
	obj.setLabel(obs.getLabel());
	C3DPoint Obj;
	C2DPoint Obs(obs[0], obs[1]);
	
	bool ret = transformObs2Obj(Obj, Obs, Height);

	obj[0] = Obj.x;
	obj[1] = Obj.y;
	obj[2] = Obj.z;
	Try
	{
		Matrix APt;
		Matrix obsMat(1,2);
		obsMat.element(0,0) = obs[0]; obsMat.element(0,1) = obs[1]; 
		this->AiPoint(APt, obsMat, obj);
		Matrix N = APt.t() * obs.getCovar().i() * APt;

		CSpheroid spheroid = this->refSystem.getSpheroid();
		double aNew = spheroid.getSemiMajor() + Height;
		double bNew = spheroid.getSemiMinor() + Height; 
		double a2 = aNew * aNew; a2 = 1.0 / a2;
		double b2 = bNew * bNew; b2 = 1.0 / b2;
		double B = -2.0 * double(sigmaZ) * ((Obj.x * Obj.x + Obj.y * Obj.y) * a2 / aNew + Obj.z * Obj.z * b2 / bNew);
		B = 1.0 / B;

		APt.ReSize(1,3);
		APt.element(0,0) = 2.0 * Obj.x * a2 * B;
		APt.element(0,1) = 2.0 * Obj.y * a2 * B;
		APt.element(0,2) = 2.0 * Obj.z * b2 * B;

		N += APt.t() * APt;
		N = N.i();
		obj.setCovariance(N);
	}
	CatchAll
	{
		ret = false;
	}


	return ret;
};

//================================================================================

bool CPushBroomScanner::transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const
{
	C3DPoint xc,xp,xecr;
	C3DPoint rpy;
	C3DPoint x0;
	C3DPoint xm;

	C3DPoint tmpObs = obs + this->refractionCorrection;
	tmpObs -= this->aberrationCorrection;

	if (!this->camera->transformObs2Obj(xc,tmpObs))
		return false;

	// save the time 
	xc.dot = this->getTime(tmpObs.y);

	if (!this->path->evaluateLocation(x0,xc.dot) ||	!this->attitudes->evaluateLocation(rpy,xc.dot))
		return false;

	CRotationBase *rotRPY = CRotationBaseFactory::initRotation(&rpy,RPY);

	this->cameraMounting->getRotation().R_times_x(xm,xc);
	xp = this->cameraMounting->getShift() + xm; 

	rotRPY->R_times_x(xm,xp);
	this->fixedECRMatrix.R_times_x(obj,xm);
	
	obj += x0;
	obj.dot = xc.dot;

	delete rotRPY;
	return true;
}

//================================================================================

bool CPushBroomScanner::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const
{
	C3DPoint P1, P2;
	this->transformObs2Obj(P1, obs, 0.0);
	this->transformObs2Obj(P2, obs, 1.0);

    CXYZLine line(P1, P2);
		 
	eIntersect res = plane.getIntersection(line, obj);

	return res == eIntersection;
};

//================================================================================

bool CPushBroomScanner::transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const
{
	CXYZPoint P1, P2;
	this->transformObs2Obj(P1, obs, 0.0, 0.0);
	this->transformObs2Obj(P2, obs, 1.0, 0.0);

    CXYZHomoSegment seg(P1, P2);
		 
	eIntersect res = seg.getIntersection(plane, obj);

	return res == eIntersection;
};

//================================================================================

bool CPushBroomScanner::getVerticalPole(CXYZPoint      &objGround, CXYZPoint      &objTop, 
								        const CXYPoint &obsGround, const CXYPoint &obsTop, 
								        double heightGround, float SigmaZ) const
{
	if (!this->transformObs2Obj(objGround, obsGround, heightGround))
		return false;
	else
	{
		CSpheroid spheroid = this->refSystem.getSpheroid();
		C3DPoint normal;
		C3DPoint Geographic;
		this->refSystem.geocentricToGeographic(Geographic, objGround);
	
		Geographic.x /= R2D;
		Geographic.y /= R2D;
		double cosphi = cos(Geographic.x);
		normal.x = cosphi * cos(Geographic.y);
		normal.y = cosphi * sin(Geographic.y);
		normal.z = sin(Geographic.x);

		objTop = objGround + normal * 80.0f;

		Try
		{
			Matrix obsMatGround(1,2);
			obsMatGround.element(0,0) = obsGround.getX();
			obsMatGround.element(0,1) = obsGround.getY();
			Matrix BtQllBinvGround = obsGround.getCovar().i();

			Matrix obsMatTop(1,2);
			obsMatTop.element(0,0) = obsTop.getX();
			obsMatTop.element(0,1) = obsTop.getY();
			Matrix BtQllBinvTop = obsTop.getCovar().i();

			int iter = 0;
			Matrix Qxx;
			double rms = FLT_MAX;
			double rmsOld = FLT_MAX;
			double deltaRMS = 1.0;

			CXYPoint backproject;
			Matrix lFi(2,1);
			double a2 = spheroid.getSemiMajor(); a2 = 1.0 / (a2 * a2);
			double b2 = spheroid.getSemiMinor(); b2 = 1.0 / (b2 * b2);

			do
			{

				Matrix N(9,9), t(9, 1);
				N = 0; t = 0;
				
				this->transformObj2Obs(backproject,objTop);
				lFi.element(0,0) = backproject.x - obsTop.x;
				lFi.element(1,0) = backproject.y - obsTop.y;


				Matrix lAi;
				this->AiPoint(lAi, obsMatTop, objTop);

				rms = lFi.element(0,0) * lFi.element(0,0) + lFi.element(1,0) *  lFi.element(1,0);
				Matrix Npt = lAi.t() * BtQllBinvTop;
				Matrix hlp = Npt * lAi;
				this->addMatrix(0, 0, N, hlp);
				hlp = Npt * lFi;
				this->addMatrix(0, 0, t, hlp);
				
				this->AiPoint(lAi, obsMatGround, objGround);

				this->transformObj2Obs(backproject,objGround);
				lFi.element(0,0) = backproject.x - obsGround.x;
				lFi.element(1,0) = backproject.y - obsGround.y;

				rms += lFi.element(0,0) * lFi.element(0,0) + lFi.element(1,0) *  lFi.element(1,0);
				
				Npt = lAi.t() * BtQllBinvGround;
				hlp = Npt * lAi;
				this->addMatrix(3, 3, N, hlp);
				hlp = Npt * lFi;
				this->addMatrix(3, 0, t, hlp);
				Matrix const1(1,3);

				const1.element(0, 0) = 2 * objGround.x * a2;
				const1.element(0, 1) = 2 * objGround.y * a2;
				const1.element(0, 2) = 2 * objGround.z * b2;

				this->addMatrix(6, 3, N, const1);
				const1 = const1.t();
				this->addMatrix(3, 6, N, const1);
				double XH = objGround.x - normal.x * heightGround;
				double YH = objGround.y - normal.y * heightGround;
				double ZH = objGround.z - normal.z * heightGround;
				t.element(6,0) = (XH * XH + YH * YH) * a2 + ZH * ZH * b2 -1.0;

				Matrix const2(1,6), const3(1,6);
				const2 = 0; const3 = 0;
				double t2 = 0.0, t3 = 0.0;
				
				if (fabs(normal.x) < FLT_EPSILON)
				{
					const2.element(0,0) =  1.0;
					const2.element(0,3) = -1.0;
					t2 = objTop.x - objGround.x;

					if (fabs(normal.y) < FLT_EPSILON)
					{
						const3.element(0,1) =  1.0;
						const3.element(0,4) = -1.0;
						t3 = objTop.y - objGround.y;
					}
					else if (fabs(normal.z) < FLT_EPSILON)
					{
						const3.element(0,2) =  1.0;
						const3.element(0,5) = -1.0;
						t3 = objTop.z - objGround.z;
					}
					else
					{
						double onebyy = 1.0/normal.y;
						double onebyz = 1.0/normal.z;
						const3.element(0,1) =  onebyy;
						const3.element(0,4) = -onebyy;
						const3.element(0,2) = -onebyz;
						const3.element(0,5) =  onebyz;
						t3 = (objTop.y - objGround.y) * onebyy - (objTop.z - objGround.z) * onebyz;
					}
				}
				else if (fabs(normal.y) < FLT_EPSILON)
				{
					const2.element(0,1) =  1.0;
					const2.element(0,4) = -1.0;
					t2 = objTop.y - objGround.y;

					if (fabs(normal.z) < FLT_EPSILON)
					{
						const3.element(0,2) =  1.0;
						const3.element(0,5) = -1.0;
						t3 = objTop.z - objGround.z;
					}
					else
					{
						double onebyx = 1.0/normal.x;
						double onebyz = 1.0/normal.z;

						const3.element(0,0) =  onebyx;
						const3.element(0,3) = -onebyx;
						const3.element(0,2) = -onebyz;
						const3.element(0,5) =  onebyz;
						t3 = (objTop.x - objGround.x) * onebyx - (objTop.z - objGround.z) * onebyz;
					}
				}
				else if (fabs(normal.z) < FLT_EPSILON)
				{
					const2.element(0,2) =  1.0;
					const2.element(0,5) = -1.0;
					t2 = objTop.z - objGround.z;

					double onebyx = 1.0/normal.x;
					double onebyy = 1.0/normal.y;

					const3.element(0,0) =  onebyx;
					const3.element(0,3) = -onebyx;
					const3.element(0,1) = -onebyy;
					const3.element(0,4) =  onebyy;
					t3 = (objTop.x - objGround.x) * onebyx - (objTop.y - objGround.y) * onebyy;
				}
				else 
				{
					double onebyx = 1.0/normal.x;
					double onebyy = 1.0/normal.y;
					double onebyz = 1.0/normal.z;
					double dx = (objTop.x - objGround.x) * onebyx;
					
					const2.element(0,0) =  onebyx;
					const2.element(0,3) = -onebyx;
					const2.element(0,2) = -onebyz;
					const2.element(0,5) =  onebyz;
					t2 = dx - (objTop.z - objGround.z) * onebyz;


					const3.element(0,0) =  onebyx;
					const3.element(0,3) = -onebyx;
					const3.element(0,1) = -onebyy;
					const3.element(0,4) =  onebyy;
					t3 = dx - (objTop.y - objGround.y) * onebyy;
				}

				this->addMatrix(7, 0, N, const2);
				const2 = const2.t();
				this->addMatrix(0, 7, N, const2);
				t.element(7,0) = t2;
				this->addMatrix(8, 0, N, const3);
				const3 = const3.t();
				this->addMatrix(0, 8, N, const3);
				t.element(8,0) = t3;

				Qxx = N.i();
				Matrix delta = Qxx * t;
				objTop.x -= delta.element(0,0);
				objTop.y -= delta.element(1,0);
				objTop.z -= delta.element(2,0);
				objGround.x -= delta.element(3,0);
				objGround.y -= delta.element(4,0);
				objGround.z -= delta.element(5,0);

				deltaRMS = fabs(rmsOld - rms);
				rmsOld = rms;
				++iter;
			} while ((iter < 20) && (rms > 1e-20) && (deltaRMS > 1e-10));


			if (rms < 0.25f) rms = 0.25f;
			Qxx *= rms;
			objGround.setSX(sqrt(Qxx.element(0,0))); objGround.setSY(sqrt(Qxx.element(1,1))); objGround.setSZ(sqrt(Qxx.element(2,2)));
			objGround.setSXY(Qxx.element(1,0));      objGround.setSXZ(Qxx.element(2,0));      objGround.setSYZ(Qxx.element(2,1));
			objTop.setSX(sqrt(Qxx.element(0,0)));    objTop.setSY(sqrt(Qxx.element(1,1)));    objTop.setSZ(sqrt(Qxx.element(2,2)));
			objTop.setSXY(Qxx.element(1,0));         objTop.setSXZ(Qxx.element(2,0));         objTop.setSYZ(Qxx.element(2,1));
			
		}
		CatchAll
		{
			return false;
		}
	}
	return true;
};

//================================================================================

bool CPushBroomScanner::computeRefractionCorrection(const C3DPoint& imagePos)
{
	C3DPoint xc,xp,xm;

	if (!this->camera->transformObs2Obj(xc,imagePos))
		return false;

	this->cameraMounting->getRotation().R_times_x(xp,xc);
	xp += this->cameraMounting->getShift(); 

	const CRotationMatrix& mountRotMatrix = this->cameraMounting->getRotation().getRotMat();
	double theta = acos(-mountRotMatrix(2,2));
	
	CRefraction refr;
	double dtheta = refr.getRefractionAngle(theta,this->satelliteHeight,this->groundHeight,this->groundTemperature);

	double s = sqrt(xp.x*xp.x + xp.y*xp.y);
	double alpha = atan2(xp.y,xp.x);
	
	double sc = (tan(theta)-tan(theta - dtheta))*xp.z;
	xp.x = cos(alpha)*(s-sc);
	xp.y = sin(alpha)*(s-sc);

	// backprojection
	C2DPoint newObs(imagePos.x,imagePos.y);

	xp -= this->cameraMounting->getShift();
	this->cameraMounting->getRotation().RT_times_x(xc,xp);
	

	if (!this->camera->transformObj2Obs(newObs,xc))
		return false;
	
	this->refractionCorrection.x = newObs.x - imagePos.x;
	this->refractionCorrection.y = newObs.y - imagePos.y;

	return true;
}

//================================================================================

bool CPushBroomScanner::computeAberrationCorrection(const C3DPoint& imagePos)
{
	C3DPoint x0,v0,xc;
	C3DPoint rpy;
	C3DPoint xm,xp;

	if (!this->camera->transformObs2Obj(xc,imagePos))
		return false;

	// save the time 
	xc.dot = this->getTime(imagePos.y);

	if (!this->path->evaluate(x0,v0,xc.dot) || !this->attitudes->evaluateLocation(rpy,xc.dot))
		return false;

	// transform the velocity from ECR to tangential system
	C3DPoint newv0;
	this->fixedECRMatrix.RT_times_x(newv0,v0);

	this->cameraMounting->getRotation().R_times_x(xm,xc);
	xp = this->cameraMounting->getShift() + xm; 

	double beta = asin(xp.x/xp.z);
	double betaCorrected = atan(tan(beta) - newv0.x/(SPEED_OF_LIGHT * cos(beta)));

	xp.x = sin(betaCorrected) * xp.z;
	
	// backprojection
	C3DPoint tmp1;
	C2DPoint newObs(imagePos.x,imagePos.y);

	xp -= this->cameraMounting->getShift();
	this->cameraMounting->getRotation().RT_times_x(xc,xp);

	if (!this->camera->transformObj2Obs(newObs,xc))
		return false;
	
	this->aberrationCorrection.x = newObs.x - imagePos.x;
	this->aberrationCorrection.y = newObs.y - imagePos.y;
	
	return true;
}

//================================================================================

void CPushBroomScanner::setAdjustableValues(const Matrix &parms)
{

    this->informListeners_elementsChanged();

    this->hasValues = true;
}

//================================================================================

/**
* Gets a matrix of the parameters
*
* @return the parameters backed into a matrix
*/
bool CPushBroomScanner::getAllParameters(Matrix &parms) const
{
   int size = this->nActualParameter[mountRotPar] + this->nActualParameter[mountShiftPar] + this->nActualParameter[addPar];
   parms.ReSize(size,1);

   int indexMountRot = 0;
   int indexMountShift = 0;
   int indexAddPar = this->nActualParameter[mountRotPar] + this->nActualParameter[mountShiftPar];

   if (size && this->nActualParameter[mountRotPar] && this->nActualParameter[mountShiftPar])
   {
		indexMountRot=3;
		indexMountShift=0;
   }

	if (this->nActualParameter[mountRotPar])
	{
		parms.element(indexMountRot    ,0) = this->cameraMounting->getRotation()[0];
		parms.element(indexMountRot + 1,0) = this->cameraMounting->getRotation()[1];
		parms.element(indexMountRot + 2,0) = this->cameraMounting->getRotation()[2];
	}

	if (this->nActualParameter[mountShiftPar])
	{
		parms.element(indexMountShift    ,0) = this->cameraMounting->getShift()[0];
		parms.element(indexMountShift + 1,0) = this->cameraMounting->getShift()[1];
		parms.element(indexMountShift + 2,0) = this->cameraMounting->getShift()[2];
	}

	if (this->nActualParameter[addPar])
	{	
		
	}

    return true;
}

//================================================================================

Matrix CPushBroomScanner::getConstants() const
{
	Matrix parms(0,0);
	return parms;
};

//================================================================================

bool CPushBroomScanner::read(const char* filename)
{  
    CCharString str;

    CTextFile file(filename, CTextFile::READ);

	file.ReadLine(str);

    this->FileName = filename;

    return true;
}

//================================================================================

bool CPushBroomScanner::writeTextFile(const char* filename) const
{
    FILE	*fp;
	fp = fopen(filename,"w");


	fclose(fp);

	return true;
}

//================================================================================

/**
 * Serialized storage of a CPushBroomScanner to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CPushBroomScanner::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("pushbroomfilename", this->getFileName()->GetChar());

    token = pStructuredFileSection->addToken();
	token->setValue("Parametercount", this->nActualStationPars);

	token = pStructuredFileSection->addToken();
	token->setValue("actualMountRotParameter", this->nActualParameter[mountRotPar]);

	token = pStructuredFileSection->addToken();
	token->setValue("actualMountShiftParameter", this->nActualParameter[mountShiftPar]);

	token = pStructuredFileSection->addToken();
	token->setValue("actualAdditionalParameter", this->nActualParameter[addPar]);

	token = pStructuredFileSection->addToken();
	token->setValue("activeAdditionalParameter", this->activeParameter[addPar]);


    token = pStructuredFileSection->addToken();
	token->setValue("bIsCurrentSensorModel", this->bIsCurrentSensorModel);

    token = pStructuredFileSection->addToken();
    token->setValue("hasValues", this->gethasValues());

    token = pStructuredFileSection->addToken();
    token->setValue("satelliteHeight", this->satelliteHeight);

    token = pStructuredFileSection->addToken();
    token->setValue("groundHeight", this->groundHeight);

    token = pStructuredFileSection->addToken();
    token->setValue("groundTemperature", this->groundTemperature);

	token = pStructuredFileSection->addToken();
	token->setValue("firstLineTime",this->firstLineTime);

	token = pStructuredFileSection->addToken();
	token->setValue("timePerLine",this->timePerLine);

	token = pStructuredFileSection->addToken();
	token->setPointerValue("path",this->path);

	token = pStructuredFileSection->addToken();
	token->setPointerValue("attitudes",this->attitudes);

	token = pStructuredFileSection->addToken();
	token->setPointerValue("camera",this->camera);

	token = pStructuredFileSection->addToken();
	token->setPointerValue("cameraMounting",this->cameraMounting);

	token = pStructuredFileSection->addToken();
	token->setValue("changeMounting",this->changeMountingForMerging);

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->refSystem.serializeStore(child);
	  
}

//================================================================================

/**
 * Restores a CPushBroomScanner from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CPushBroomScanner::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("pushbroomfilename"))
            this->setFileName( token->getValue());
		else if(key.CompareNoCase("Parametercount"))
			this->nActualStationPars = token->getIntValue();
		else if(key.CompareNoCase("bIsCurrentSensorModel"))
			this->bIsCurrentSensorModel = token->getBoolValue();
		else if(key.CompareNoCase("hasValues"))
            this->hasValues = token->getBoolValue();
		else if(key.CompareNoCase("satelliteHeight"))
            this->satelliteHeight= token->getDoubleValue();
		else if(key.CompareNoCase("groundHeight"))
            this->groundHeight = token->getDoubleValue();
		else if(key.CompareNoCase("groundTemperature"))
            this->groundTemperature = token->getDoubleValue();
		else if(key.CompareNoCase("firstLineTime"))
            this->firstLineTime = token->getDoubleValue();
		else if(key.CompareNoCase("timePerLine"))
            this->timePerLine = token->getDoubleValue();
		else if(key.CompareNoCase("path"))
			this->path = (COrbitPathModel*)token->getPointerValue();
		else if(key.CompareNoCase("attitudes"))
			this->attitudes = (COrbitAttitudeModel*)token->getPointerValue();
		else if(key.CompareNoCase("camera"))
			this->camera = (CCCDLine*)token->getPointerValue();
		else if(key.CompareNoCase("cameraMounting"))
			this->cameraMounting = (CCameraMounting*)token->getPointerValue();
		else if (key.CompareNoCase("actualMountRotParameter"))
			this->nActualParameter[mountRotPar] = token->getIntValue();
		else if (key.CompareNoCase("actualMountShiftParameter"))
			this->nActualParameter[mountShiftPar] = token->getIntValue();
		else if (key.CompareNoCase("actualAdditionalParameter"))
			this->nActualParameter[addPar] = token->getIntValue();
		else if (key.CompareNoCase("activeAdditionalParameter"))
			this->activeParameter[addPar] = token->getIntValue();
		else if (key.CompareNoCase("changeMounting")) 
			this->changeMountingForMerging = token->getBoolValue();
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
           this->refSystem.serializeRestore(child);
    }



}

//================================================================================

void CPushBroomScanner::reconnectPointers()
{
	CSerializable::reconnectPointers();

	this->path = (COrbitPathModel*)this->handleManager.getNewPointer(this->path);
	this->attitudes = (COrbitAttitudeModel*)this->handleManager.getNewPointer(this->attitudes);
	this->camera = (CCCDLine*) this->handleManager.getNewPointer(this->camera);
	this->cameraMounting = (CCameraMounting*) this->handleManager.getNewPointer(this->cameraMounting);


	if (this->path && this->attitudes && this->camera && this->cameraMounting )
		this->init(this->path,
					this->attitudes,
					this->cameraMounting,
					this->camera,
					this->FileName,
					this->satelliteHeight,
					this->groundHeight,
					this->groundTemperature,
					this->firstLineTime,
					this->timePerLine);
	else
		this->sethasValues(false);
	
}

//================================================================================

void CPushBroomScanner::resetPointers()
{
	CSerializable::resetPointers();
}

//================================================================================

/**
 * Gets ClassName
 * returns a CCharString
 */
string CPushBroomScanner::getClassName() const
{
    return string( "CPushBroomScanner");
}

//================================================================================

bool CPushBroomScanner::isa(string& className) const
{
    if (className == "CPushBroomScanner")
        return true;

    return false;
}

//================================================================================

bool CPushBroomScanner::AiBiFi(Matrix &Fi, Matrix &Bi, 
							   Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
							   Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
							   Matrix &AiPar,        Matrix &AiPoint, 
							   const Matrix &obs, const C3DPoint &approx)
{   
	
	return true;
}

//================================================================================

bool CPushBroomScanner::Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
						   Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						   Matrix &AiPar,        Matrix &AiPoint, 
						   const Matrix &obs, const C3DPoint &approx) const
{   
	
	return true;
}

//================================================================================

bool CPushBroomScanner::AiPars(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
							   Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
							   Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const
{   
    return true;
}

//================================================================================

bool CPushBroomScanner::AiPoint(Matrix& Ai, const Matrix &obs, const C3DPoint &approx) const
{   
	Ai.ReSize(2,3);

	double time = this->getTime(obs.element(0,1));

	CRotationBase* Rrpy = CRotationBaseFactory::initRotation(&C3DPoint(0,0,0),RPY);

	C3DPoint F;

	// compute the image vector
	this->compute3DImageVector(F,time,approx,Rrpy);

	CRotationMatrix tmpRotMat1,tmpRotMat2;

	Rrpy->getRotMat().RT_times_RT(tmpRotMat1,this->fixedECRMatrix);
	this->cameraMounting->getRotation().RT_times_R(tmpRotMat2,tmpRotMat1);

	Matrix dFdX(3,3);	// the partials for the 3D Points
	for (int i=0; i<3;i++)
	{
		dFdX.element(0,i) = tmpRotMat2(0,i);
		dFdX.element(1,i) = tmpRotMat2(1,i);
		dFdX.element(2,i) = tmpRotMat2(2,i);
	}
	
	double fac1 = -this->camera->getPrincipalDistance()/(F.z);
	double fac2 = -fac1*F.x/F.z;
	double fac3 = -fac1*F.y/F.z;

	// compute the final partials for the 3D points
	for (int i=0; i< 3; i++)
	{
		double x = fac1 * dFdX.element(0,i) + fac2 * dFdX.element(2,i);
		double y = fac1 * dFdX.element(1,i) + fac3 * dFdX.element(2,i);
		Ai.element(0,i) = x;
		Ai.element(1,i) = y;
	}
	

	delete Rrpy;

    return true;
}

//================================================================================

bool CPushBroomScanner::Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const
{  
	Fi.ReSize(2, 1);

	C2DPoint projection;
	
	this->transformObj2Obs(projection,approx);

	Fi.element(0, 0) =  projection.x - obs.element(0,0);
	Fi.element(1, 0) =  projection.y - obs.element(0,1);

    return true;
}

//================================================================================

bool CPushBroomScanner::Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const
{
	Bi.ReSize(2,2);
	Bi.element(0,0) = Bi.element(1,1) = -1.0;
	Bi.element(1,0) = Bi.element(0,1) =  0.0;
    return true;
}

//================================================================================

void CPushBroomScanner::getName(CCharString& name) const
{
    name = "Pushbroom Bundle";
}

//================================================================================

void CPushBroomScanner::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
    StationNames.RemoveAll();
	PointNames.RemoveAll();

	if (this->nActualParameter[mountRotPar])
	{
		StationNames.Add("Camera Mounting (Rotation)   Parameter[�]	       Sigma[�]\n      "
						 "                 Roll ");
		StationNames.Add("                Pitch ");
		StationNames.Add("                  Yaw ");
	}

	if (this->nActualParameter[mountShiftPar])
	{
		StationNames.Add("Camera Mounting (Shift)      Parameter[m]	       Sigma[m]\n      "
						 "                    X ");
		StationNames.Add("                    Y ");
		StationNames.Add("                    Z ");
	}



}

//================================================================================

int CPushBroomScanner::coordPrecision() const
{
	return 2;
}

//================================================================================

void CPushBroomScanner::getObservationNames(CCharStringArray &names) const
{
	names.RemoveAll();
    
    CCharString* name = names.Add();
    *name = "x";
	name = names.Add();
	*name = "y";

}

//================================================================================

void CPushBroomScanner::setCovariance(const Matrix &covar)
{
	if (this->nActualParameter[addPar])
	{
		int iAdp = 0;
		int nAdp = this->nActualParameter[addPar];
		this->camera->setADPParameterCovariance(covar.SubMatrix(iAdp+1,iAdp +nAdp ,iAdp+1,iAdp +nAdp),this->activeParameter[addPar]);
	}


	if (this->nActualParameter[interiorPar])
	{
		int iIrp = 0;
		int nIrp = this->nActualParameter[interiorPar];
		this->camera->setIRPParameterCovariance(covar.SubMatrix(iIrp+1,iIrp +nIrp ,iIrp+1,iIrp +nIrp),this->activeParameter[interiorPar]);
	}


	this->covariance = covar;

}

//================================================================================

bool CPushBroomScanner::setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const
{
	return true;
};

//================================================================================
	 
bool CPushBroomScanner::setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const
{
	return true;
};

//================================================================================

bool CPushBroomScanner::getAdjustableParameters(Matrix &parms) const
{
   int size = this->nActualParameter[mountRotPar] + this->nActualParameter[mountShiftPar];
   parms.ReSize(size,1);

   int indexMountRot = 0;
   int indexMountShift = 0;

   if (size && this->nActualParameter[mountRotPar] && this->nActualParameter[mountShiftPar])
   {
		indexMountRot=3;
		indexMountShift=0;
   }

	if (this->nActualParameter[mountRotPar])
	{
		parms.element(indexMountRot    ,0) = this->cameraMounting->getRotation()[0];
		parms.element(indexMountRot + 1,0) = this->cameraMounting->getRotation()[1];
		parms.element(indexMountRot + 2,0) = this->cameraMounting->getRotation()[2];
	}

	if (this->nActualParameter[mountShiftPar])
	{
		parms.element(indexMountShift    ,0) = this->cameraMounting->getShift()[0];
		parms.element(indexMountShift + 1,0) = this->cameraMounting->getShift()[1];
		parms.element(indexMountShift + 2,0) = this->cameraMounting->getShift()[2];
	}


    return true;
}	

//================================================================================

/**
 * Computes the first step of the image vector determination.
 * The result is the 3D vector x-x0 (used as interstage product)
 * 
 */
bool CPushBroomScanner::compute3DImageVector(C3DPoint& imageVector,const double& time, const C3DPoint& objPoint,CRotationBase* Rrpy) const
{
	C3DPoint location;			// the location of the satellite
	
	// interstage products
	C3DPoint xp, xm;
	CRotationMatrix tmpRotMat1,tmpRotMat2;
	C3DPoint rpyAngles;
	
	// get the angles and location
	if (!this->attitudes->evaluateLocation(rpyAngles,time) || !this->path->evaluateLocation(location,time))
		return false;

	Rrpy->updateAngles_Rho(&rpyAngles);

	this->fixedECRMatrix.RT_times_x(xp,objPoint - location);	// computes xp = RfixedT * (X-X0)

	Rrpy->RT_times_x(xm,xp);		// computes xm = RrpyT * xp
	xm -= this->cameraMounting->getShift();	// reduces the mounting shift
	this->cameraMounting->getRotation().RT_times_x(imageVector,xm);	// computes xc = RmountingT * xm

	return true;
}

//================================================================================

int CPushBroomScanner::accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
								  CObsPoint &discrepancy,  CObsPoint &normalisedDiscrepancy, 	
								  CRobustWeightFunction *robust, bool omitMarked)
{
// this function computes the components of the N-Matrix and the t-vector for the given observation

// we first compute: xf - xo = F, where F = RmT*(RrpyT*ReT(X-X0) - Xm) 
// and the partials dF/dPath, dF/dAttitudes, dF/d3DPoints, dF/dMountRot, dF/dMountShift

// as a second step we compute xf = xo + c*Fx/Fz and yf = yo + c*Fy/Fz
// and the according partials	AxyPath =		(dxf/dPath), 
//												(dyf/dPath)
//								AxyAttitdes =	(dxf/dAttitudes), 
//							                    (dyf/dAttitudes),              
//								Axy3DPoints =	(dxf/3DPoints)
//												(dyf/3DPoints)
//								AxyMountRot =	(dxf/dMountRot)
//												(dyf/dMountRot)	
//								AxyMountShift=	(dxf/dMountShift)
//												(dyf/dMountShift)	
// then N = AT*P*A and t = AT*P*dl


	Matrix AiPath;	
	Matrix AiAttitudes;
	CAdjustMatrix AiAdp(0,0);

	int nObs = 2;

	Matrix AiRot,AiSca,AiIrp,AiMountShift,AiMountRot,AiErp,AiPoint;	// dummys

	int dimension = obs.getDimension();
	double time = this->getTime(obs[1]);

	C3DPoint F;
	C2DPoint xf2D(obs[0],obs[1]);

	CRotationBase* Rrpy = CRotationBaseFactory::initRotation(&C3DPoint(0,0,0),RPY);

	double eps = 0.000001;
	double t_old=0.0; 
	for (int i=0; i < 10 && fabs(time - t_old) > eps; i++)
	{
		t_old = time;

		this->compute3DImageVector(F,time,approx,Rrpy);
		this->camera->transformObj2Obs(xf2D,F);
		
		time = this->getTime(xf2D.y);
	}
	this->applyCorrections(xf2D);


	// compute discrepancy
	discrepancy[0] =  xf2D.x - obs[0];
	discrepancy[1] =  xf2D.y - obs[1];


	// compute the partials for the path, attitudes and the 3D Points
	Matrix objMat(1,approx.getDimension() +1);	// this is just a dummy, but the size is important
	objMat.element(0,approx.getDimension()) = time; //we only use the time in the Ai function

	// get first the partials for the path and the attitudes after the spline parameter
	// only AiPoint and AiAttitudes are computed
	this->path->Ai(AiErp,AiRot,AiSca,AiMountShift,AiMountRot,AiIrp,AiPath,AiPoint,objMat,C3DPoint(0,0,0));
	this->attitudes->Ai(AiErp,AiRot,AiSca,AiMountShift,AiMountRot,AiIrp,AiAttitudes,AiPoint,objMat,C3DPoint(0,0,0));

	// get the partials for the rotation angles of the attitudes
	CAdjustMatrix derivRotMat(9,3);
	Rrpy->computeDerivation(derivRotMat); // this gives as the derivations rotMat elements r00 - r22 after the angles

	// we also need the vector v = ReT(X-X0), we got F= RmT*(RrpyT*ReT(X-X0)-xm)
	C3DPoint vTmp,v,g;
	this->cameraMounting->getRotation().R_times_x(vTmp,F);
	vTmp += this->cameraMounting->getShift();
	Rrpy->R_times_x(v,vTmp);

	const CRotationMatrix &mountRotMat = this->cameraMounting->getRotation().getRotMat();

	// compute matrix dF/dRotMatElementsRPY
	CAdjustMatrix dFdRotMatElements(3,9);
	for (int i=0; i < 3; i++)
	{
		for (int k=0; k <3; k++)
		{
			for (int j=0; j < 3; j++)
				dFdRotMatElements(i,k + j*3) = mountRotMat(k,i) * v[j];
		}
	}

	// and now dF/dAngles
	CAdjustMatrix dFdAnglesTmp = dFdRotMatElements * derivRotMat;
	Matrix dFdAngles(3,3);

	for (int i=0; i < 3; i++)
	{
		for (int k=0; k <3; k++)
		{
			dFdAngles.element(i,k) = dFdAnglesTmp(i,k);
		}

	}



	// finaly dF/dAttitudes (derivations of the vector F after the spline parameters )
	Matrix dFdAttitudes;
	dFdAttitudes = dFdAngles * AiAttitudes;

	CRotationMatrix tmpRotMat1,tmpRotMat2;

	Rrpy->getRotMat().RT_times_RT(tmpRotMat1,this->fixedECRMatrix);
	this->cameraMounting->getRotation().RT_times_R(tmpRotMat2,tmpRotMat1);

	Matrix dFdX(3,3);	// the partials for the 3D Points
	for (int i=0; i<3;i++)
	{
		dFdX.element(0,i) = tmpRotMat2(0,i);
		dFdX.element(1,i) = tmpRotMat2(1,i);
		dFdX.element(2,i) = tmpRotMat2(2,i);
	}

	Matrix dFdPath = -dFdX * AiPath;  // the partials for the orbit path

	// only if we want to determine the mounting parameter
	// mounting rotation
	CAdjustMatrix dFdMountRot(3,3);
	if (this->nActualParameter[mountRotPar])
	{
		// we also need the vector g =RrpyT*ReT(X-X0), we got  v = ReT(X-X0)
		C3DPoint g;
		Rrpy->RT_times_x(g,v);
		
		
		const CRollPitchYawRotation &mountRot = this->cameraMounting->getRotation();
		const CXYZPoint& mountShift = this->cameraMounting->getShift();
		// get the partials for the rotation angles of the camera mounting
		mountRot.computeDerivation(derivRotMat); // this gives as the derivations rotMat elements r00 - r22 after the angles
		CAdjustMatrix dFdRotMat(3,9,0.0);
		for (int i=0; i < 3; i++)
		{
			double value = g[i] - mountShift[i];
			dFdRotMat(i,i*3 + 0) = value;
			dFdRotMat(i,i*3 + 1) = value;
			dFdRotMat(i,i*3 + 2) = value;
		}

		dFdMountRot = dFdRotMat * derivRotMat;
	}


	// determine the number of Parameter for every group
	int nPathParameter = AiPath.Ncols();
	int n3DParameter = 3;
	int nAttitudeParameter = AiAttitudes.Ncols();
	int nMountRotParameter = this->nActualParameter[mountRotPar];
	int nMountShiftParameter = this->nActualParameter[mountShiftPar];
	int nAdditionalParameter = this->nActualParameter[addPar];

	CAdjustMatrix Axy3DPoints(2,n3DParameter); 
	CAdjustMatrix AxyPath (2,nPathParameter);
	CAdjustMatrix AxyAttitudes(2,nAttitudeParameter);
	CAdjustMatrix AxyMountRot(2,nMountRotParameter);
	CAdjustMatrix AxyMountShift(2,nMountShiftParameter);	

	double fac1 = -this->camera->getPrincipalDistance()/(F.z);
	double fac2 = -fac1*F.x/F.z;
	double fac3 = -fac1*F.y/F.z;

	// compute the final partial for the path
	for (int i=0; i < nPathParameter; i++)
	{
		AxyPath(0,i) = fac1 * dFdPath.element(0,i) + fac2 * dFdPath.element(2,i);
		AxyPath(1,i) = fac1 * dFdPath.element(1,i) + fac3 * dFdPath.element(2,i);
	}

	// compute the final partials for the 3D points
	for (int i=0; i< 3; i++)
	{
		Axy3DPoints(0,i) = fac1 * dFdX.element(0,i) + fac2 * dFdX.element(2,i);
		Axy3DPoints(1,i) = fac1 * dFdX.element(1,i) + fac3 * dFdX.element(2,i);
	}

	// compute the final partials for the attitudes
	for (int i=0; i < nAttitudeParameter; i++)
	{
		AxyAttitudes(0,i) = fac1 *dFdAttitudes.element(0,i) + fac2 * dFdAttitudes.element(2,i);
		AxyAttitudes(1,i) = fac1 *dFdAttitudes.element(1,i) + fac3 * dFdAttitudes.element(2,i);
	}


	// compute the final partials for the camera mounting rotation
	for (int i=0; i < nMountRotParameter; i++)
	{
		AxyMountRot(0,i) = fac1 *dFdMountRot(0,i) + fac2 * dFdMountRot(2,i);
		AxyMountRot(1,i) = fac1 *dFdMountRot(1,i) + fac3 * dFdMountRot(2,i);
	}


	// compute the final partials for the camera mounting shift
	const CRotationMatrix &mountRot = this->cameraMounting->getRotation().getRotMat();
	for (int i=0; i < nMountShiftParameter; i++)
	{
		AxyMountShift(0,i) = -fac1 *mountRot(0,i) - fac2 * mountRot(2,i);
		AxyMountShift(1,i) = -fac1 *mountRot(1,i) - fac3 * mountRot(2,i);
	}


	// fill N and t Matrix
	CAdjustMatrix Fi(2,1);
	Fi(0,0) = discrepancy[0];
	Fi(1,0) = discrepancy[1];

	CAdjustMatrix ATPath = AxyPath;
	CAdjustMatrix AT3DPoints = Axy3DPoints;
	CAdjustMatrix ATAttitudes = AxyAttitudes;
	ATPath.transpose();
	AT3DPoints.transpose();
	ATAttitudes.transpose();

	CAdjustMatrix pbb(2,2);
	pbb(0,0) = obs.getCovar().element(0,0);
	pbb(0,1) = obs.getCovar().element(0,1);
	pbb(1,0) = obs.getCovar().element(1,0);
	pbb(1,1) = obs.getCovar().element(1,1);

	CAdjustMatrix P(2,2,0.0);
	if (this->nActualParameter[addPar])
	{
		C2DPoint observation(obs[0],obs[1]);
		this->camera->AiADP(AiAdp,observation,C2DPoint(),this->activeParameter[addPar]);

		CAdjustMatrix BT(2,2,0.0);
		this->camera->Bi(BT,observation,C2DPoint(0,0),this->activeParameter[addPar]);
		
		P = BT*pbb*BT;	// BT*Qll*B
		P.invert();

	}
	else
	{
		pbb.invert();
		P(0,0) = pbb(0,0);
		P(0,1) = pbb(0,1);
		P(1,0) = pbb(1,0);
		P(1,1) = pbb(1,1);
	}




	// compute normalized residuals
	normalisedDiscrepancy[0] = fabs(discrepancy[0]) / obs.getSigma(0);
	normalisedDiscrepancy[1] = fabs(discrepancy[1]) / obs.getSigma(1);

	if (robust || omitMarked)
	{
		double w0 , w1;
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 0, omitMarked, w0, P)) --nObs; 
		if (!checkRobust(obs, normalisedDiscrepancy, robust, 1, omitMarked, w1, P)) --nObs; 
	}

	// compute the ATP blocks
	CAdjustMatrix ATPath_P =  ATPath * P;
	CAdjustMatrix AT3DPoints_P = AT3DPoints * P;
	CAdjustMatrix ATAttitudes_P = ATAttitudes * P;

	// compute the separat blocks
	CAdjustMatrix AT_P_APath = ATPath_P * AxyPath;
	CAdjustMatrix AT_P_A3DPoints = AT3DPoints_P * Axy3DPoints;
	CAdjustMatrix AT_P_AAttitudes = ATAttitudes_P * AxyAttitudes;

	// the mixed blocks
	CAdjustMatrix ATPath_P_A3DPoints = ATPath_P * Axy3DPoints;
	CAdjustMatrix ATPath_P_AAttitudes = ATPath_P * AxyAttitudes;
	CAdjustMatrix ATAttitudes_P_A3DPoints = ATAttitudes_P * Axy3DPoints;




	int indexPath = this->path->getParameterIndex(addPar);
	int indexAttitudes = this->attitudes->getParameterIndex(addPar);
	int indexMountShift = this->getParameterIndex(mountShiftPar);
	int indexMountRot = this->getParameterIndex(mountRotPar);
	int indexAddPar = this->getParameterIndex(addPar);

	// fill the separat blocks
	AT_P_APath.addToMatrix(indexPath,indexPath,N);
	AT_P_A3DPoints.addToMatrix(approx.id,approx.id,N);	
	AT_P_AAttitudes.addToMatrix(indexAttitudes,indexAttitudes,N);

	// the mixed blocks
	// path and 3dpoints
	ATPath_P_A3DPoints.addToMatrix(indexPath,approx.id,N,true);

	// path and attitudes
	ATPath_P_AAttitudes.addToMatrix(indexPath,indexAttitudes,N,true);

	// attitudes and 3dpoints
	ATAttitudes_P_A3DPoints.addToMatrix(indexAttitudes,approx.id,N,true);



	// fill t-vector
	AT_P_APath = ATPath_P * Fi;
	AT_P_APath.addToMatrix(indexPath,0,t);

	AT_P_A3DPoints = AT3DPoints_P * Fi;
	AT_P_A3DPoints.addToMatrix(approx.id,0,t);

	ATAttitudes_P_A3DPoints = ATAttitudes_P * Fi;
	ATAttitudes_P_A3DPoints.addToMatrix(indexAttitudes,0,t);

	if (this->nActualParameter[mountRotPar])
	{
		

		CAdjustMatrix ATMountRot = AxyMountRot;
		ATMountRot.transpose();
		// compute the ATP
		CAdjustMatrix ATMountRot_P = ATMountRot * P;
		
		// compute the separat block
		CAdjustMatrix AT_P_AMountRot = ATMountRot_P * AxyMountRot;

		// the mixed blocks
		CAdjustMatrix ATPath_P_AMountRot = ATPath_P * AxyMountRot;
		CAdjustMatrix ATAttitudes_P_AMountRot = ATAttitudes_P * AxyMountRot;
		CAdjustMatrix AT3DPoints_P_AMountRot = AT3DPoints_P * AxyMountRot;

		// fill the separat block
		AT_P_AMountRot.addToMatrix(indexMountRot,indexMountRot,N);

		// the mixed blocks
		// path and mount rot
		ATPath_P_AMountRot.addToMatrix(indexPath,indexMountRot,N,true);

		// attitudes and mount rot
		ATAttitudes_P_AMountRot.addToMatrix(indexAttitudes,indexMountRot,N,true);

		// 3dpoints and mount rot
		AT3DPoints_P_AMountRot.addToMatrix(approx.id,indexMountRot,N,true);

		// fill t-vector
		AT_P_AMountRot = ATMountRot_P * Fi;
		AT_P_AMountRot.addToMatrix(indexMountRot,0,t);

	}


	if (this->nActualParameter[mountShiftPar])
	{
		

		CAdjustMatrix ATMountShift = AxyMountShift;
		ATMountShift.transpose();
		// compute the ATP
		CAdjustMatrix ATMountShift_P = ATMountShift * P;
		
		// compute the separat block
		CAdjustMatrix AT_P_AMountShift = ATMountShift_P * AxyMountShift;

		// the mixed blocks
		CAdjustMatrix ATPath_P_AMountShift = ATPath_P * AxyMountShift;
		CAdjustMatrix ATAttitudes_P_AMountShift = ATAttitudes_P * AxyMountShift;
		CAdjustMatrix AT3DPoints_P_AMountShift = AT3DPoints_P * AxyMountShift;

		// fill the separat block
		AT_P_AMountShift.addToMatrix(indexMountShift,indexMountShift,N);

		// the mixed blocks
		// path and mount shift
		ATPath_P_AMountShift.addToMatrix(indexPath,indexMountShift,N,true);

		// attitudes and mount shift
		ATAttitudes_P_AMountShift.addToMatrix(indexAttitudes,indexMountShift,N,true);

		// 3dpoints and mount shift
		AT3DPoints_P_AMountShift.addToMatrix(approx.id,indexMountShift,N,true);

		// fill t-vector
		AT_P_AMountShift = ATMountShift_P * Fi;
		AT_P_AMountShift.addToMatrix(indexMountShift,0,t);

		// the mixed block between mountRot and mountShift
		if (this->nActualParameter[mountRotPar])
		{
			CAdjustMatrix ATMountShift_P_AMountRot =  ATMountShift_P * AxyMountRot;
			ATMountShift_P_AMountRot.addToMatrix(indexMountShift,indexMountRot,N,true);
		}

	}

	if (this->nActualParameter[addPar])
	{
		CAdjustMatrix ATAddPar = AiAdp;
		ATAddPar.transpose();
		
		// compute the ATP
		CAdjustMatrix ATAddPar_P = ATAddPar * P;

		// compute the separat block
		CAdjustMatrix AT_P_AAddPar = ATAddPar_P * AiAdp;

		// fill the separat block
		AT_P_AAddPar.addToMatrix(indexAddPar,indexAddPar,N);

		// compute the mixed blocks
		CAdjustMatrix ATPath_P_AAddPar = ATPath_P * AiAdp;
		CAdjustMatrix ATAttitudes_P_AAddPar = ATAttitudes_P * AiAdp;
		CAdjustMatrix AT3DPoints_P_AAddPar = AT3DPoints_P * AiAdp;

		// fill the mixed blocks
		// path and add pars
		ATPath_P_AAddPar.addToMatrix(indexPath,indexAddPar,N,true);

		// attitudes and add pars
		ATAttitudes_P_AAddPar.addToMatrix(indexAttitudes,indexAddPar,N,true);

		// 3dpoints and add pars
		AT3DPoints_P_AAddPar.addToMatrix(approx.id,indexAddPar,N,true);

		// mount shift and add pars
		if (this->nActualParameter[mountShiftPar])
		{
			CAdjustMatrix ATAddPar_P_AMountShift =  ATAddPar_P * AxyMountShift;
			ATAddPar_P_AMountShift.addToMatrix(indexAddPar,indexMountShift,N,true);
		}
		
		// mount rot and add pars
		if (this->nActualParameter[mountRotPar])
		{
			CAdjustMatrix ATAddPar_P_AMountRot =  ATAddPar_P * AxyMountRot;
			ATAddPar_P_AMountRot.addToMatrix(indexAddPar,indexMountRot,N,true);
		}	

		// fill t-vector
		AT_P_AAddPar = ATAddPar_P * Fi;
		AT_P_AAddPar.addToMatrix(indexAddPar,0,t);
		
	}


	delete Rrpy;

	return nObs;
};

//================================================================================

bool CPushBroomScanner::insertParametersForAdjustment(Matrix &parms) 
{
	if (this->nActualParameter[mountRotPar])
	{
		int indexMountRot = this->parameterIndex[mountRotPar];
		parms.element( indexMountRot    ,0) = this->cameraMounting->getRotation()[0];
		parms.element( indexMountRot + 1,0) = this->cameraMounting->getRotation()[1];
		parms.element( indexMountRot + 2,0) = this->cameraMounting->getRotation()[2];

	}

	if (this->nActualParameter[mountShiftPar])
	{
		int indexMountShift = this->parameterIndex[mountShiftPar];
		parms.element( indexMountShift    ,0) = this->cameraMounting->getShift()[0];
		parms.element( indexMountShift + 1,0) = this->cameraMounting->getShift()[1];
		parms.element( indexMountShift + 2,0) = this->cameraMounting->getShift()[2];

	}

	if (this->nActualParameter[addPar])
	{
		int indexAddPar = this->parameterIndex[addPar];
		this->camera->insertADPParametersForAdjustment(parms,indexAddPar,this->activeParameter[addPar]);
	}

	return true;
};

//================================================================================

bool CPushBroomScanner::setParametersFromAdjustment(const Matrix &parms) 
{

	if (this->nActualParameter[mountRotPar])
	{
		int indexMountRot = this->parameterIndex[mountRotPar];
		this->cameraMounting->getRotation()[0] = parms.element( indexMountRot    ,0);
		this->cameraMounting->getRotation()[1] = parms.element( indexMountRot + 1,0);
		this->cameraMounting->getRotation()[2] = parms.element( indexMountRot + 2,0);
		this->cameraMounting->getRotation().computeRotMatFromParameter();
	}

	if (this->nActualParameter[mountShiftPar])
	{
		int indexMountShift = this->parameterIndex[mountShiftPar];
		this->cameraMounting->getShift()[0] = parms.element( indexMountShift    ,0);
		this->cameraMounting->getShift()[1] = parms.element( indexMountShift + 1,0);
		this->cameraMounting->getShift()[2] = parms.element( indexMountShift + 2,0);

	}

	if (this->nActualParameter[addPar])
	{
		int indexAddPar = this->parameterIndex[addPar];
		this->camera->setADPParametersFromAdjustment(parms,indexAddPar,this->activeParameter[addPar]);
	}

	C3DPoint P(this->camera->getNCols()/2, this->getLine(this->timeFixedECRMatrix), 0.0);
	if (!this->computeRefractionCorrection(P)) return false;
	if (!this->computeAberrationCorrection(P)) return false;

	return true;
};

//================================================================================

CObsPoint CPushBroomScanner::calcRes(const Matrix &Delta, const CObsPoint &obs, const CObsPoint &obj)
{
	int dimension = obs.getDimension();
	CObsPoint residual(dimension);

	if (obs.id >= 0 || obj.id < 0) 
	{
		Matrix FiMat(dimension, 1);;

		double time = this->getTime(obs[1]);

	
		CRotationBase* Rrpy = CRotationBaseFactory::initRotation(&C3DPoint(0,0,0),RPY);
		C3DPoint xc3D;
		C2DPoint xc2D(obs[0],obs[1]);
		CRotationMatrix tmpRotMat1,tmpRotMat2;

		double eps = 0.000001;
		double t_old=0.0; 
		for (int i=0; i < 10 && fabs(time - t_old) > eps; i++)
		{
			t_old = time;

			this->compute3DImageVector(xc3D,time,obj,Rrpy);
			this->camera->transformObj2Obs(xc2D,xc3D);
			
			time = this->getTime(xc2D.y);
		}
		this->applyCorrections(xc2D);

		FiMat.element(0,0) = xc2D.x - obs[0];
		FiMat.element(1,0) = xc2D.y - obs[1];

		// compute the partials for the path and the controlpoints
		Matrix objMat(1,obj.getDimension() + 1); // we have to give the path the obs in matrix form
		Matrix AiErp,AiRot,AiSca,AiMountShift,AiMountRot,AiIrp,AiPoint; // dummys
		
		// only these Matrices are filled by the splines
		Matrix AiPath;	
		Matrix AiAttitudes;
		CAdjustMatrix AiAdp(0,0);

		for (int i=0; i < obj.getDimension(); i++)
			objMat.element(0,i) = obj[i];
		objMat.element(0,obj.getDimension()) = time;

		this->path->Ai(AiErp,AiRot,AiSca,AiMountShift,AiMountRot,AiIrp,AiPath,AiPoint,objMat,C3DPoint(0,0,0));
		this->attitudes->Ai(AiErp,AiRot,AiSca,AiMountShift,AiMountRot,AiIrp,AiAttitudes,AiPoint,objMat,C3DPoint(0,0,0));
	
		// get the partials for the rotation angles
		CAdjustMatrix derivRotMat(9,3);
		Rrpy->computeDerivation(derivRotMat); // this gives as the derivations rotMat elements r00 - r22 after the angles


		// we also need the vector v = ReT(X-X0), we got F= RmT*(RrpyT*ReT(X-X0)-xm)
		C3DPoint vTmp,v;
		this->cameraMounting->getRotation().R_times_x(vTmp,xc3D);
		vTmp += this->cameraMounting->getShift();
		Rrpy->R_times_x(v,vTmp);

		const CRotationMatrix &mountRotMat = this->cameraMounting->getRotation().getRotMat();

		// compute matrix dF/dRotMatElements
		CAdjustMatrix dFdRotMatElements(3,9);
		for (int i=0; i < 3; i++)
		{
			for (int k=0; k <3; k++)
			{
				for (int j=0; j < 3; j++)
					dFdRotMatElements(i,k + j*3) = mountRotMat(k,i) * v[j];
			}
		}

		// and now dF/dAngles
		CAdjustMatrix dFdAnglesTmp = dFdRotMatElements * derivRotMat;
		Matrix dFdAngles(3,3);

		for (int i=0; i < 3; i++)
		{
			for (int k=0; k <3; k++)
			{
				dFdAngles.element(i,k) = dFdAnglesTmp(i,k);
			}

		}



		// finaly dF/dAttitudes (derivations of the vector F after the spline parameters )
		Matrix dFdAttitudes;
		dFdAttitudes = dFdAngles * AiAttitudes;




		Rrpy->getRotMat().RT_times_RT(tmpRotMat1,this->fixedECRMatrix);
		this->cameraMounting->getRotation().RT_times_R(tmpRotMat2,tmpRotMat1);

		Matrix dFdX(3,3);	// the partials for the controlpoints
		for (int i=0; i<3;i++)
		{
			dFdX.element(0,i) = tmpRotMat2(0,i);
			dFdX.element(1,i) = tmpRotMat2(1,i);
			dFdX.element(2,i) = tmpRotMat2(2,i);
		}

		Matrix dFdPath = -dFdX * AiPath;  // the partials for the orbit path

		// only if we want to determine the mounting parameter
		// mounting rotation
		CAdjustMatrix dFdMountRot(3,3);
		if (this->nActualParameter[mountRotPar])
		{
			// we also need the vector g =RrpyT*ReT(X-X0), we got  v = ReT(X-X0)
			C3DPoint g;
			Rrpy->RT_times_x(g,v);
			
			
			const CRollPitchYawRotation &mountRot = this->cameraMounting->getRotation();
			const CXYZPoint& mountShift = this->cameraMounting->getShift();
			// get the partials for the rotation angles of the camera mounting
			mountRot.computeDerivation(derivRotMat); // this gives as the derivations rotMat elements r00 - r22 after the angles
			CAdjustMatrix dFdRotMat(3,9,0.0);
			for (int i=0; i < 3; i++)
			{
				double value = g[i] - mountShift[i];
				dFdRotMat(i,i*3 + 0) = value;
				dFdRotMat(i,i*3 + 1) = value;
				dFdRotMat(i,i*3 + 2) = value;
			}

			dFdMountRot = dFdRotMat * derivRotMat;
		}


		if (this->nActualParameter[addPar])
			this->camera->AiADP(AiAdp,C2DPoint(obs[0],obs[1]),C2DPoint(),this->activeParameter[addPar]);

		// determine the number of parameter for every group
		int nPathParameter = AiPath.Ncols();
		int nPointParameter = 3;
		int nAttitudeParameter = AiAttitudes.Ncols();
		int nMountRotParameter = this->nActualParameter[mountRotPar];
		int nMountShiftParameter = this->nActualParameter[mountShiftPar];
		int nAdditionalParameter = AiAdp.getCols();
		

		// count the parameter
		int nParameter = nPathParameter + nPointParameter + nAttitudeParameter + nMountRotParameter + nMountShiftParameter + nAdditionalParameter;

		// get Index of every used parameter group
		int indexPath = this->path->getParameterIndex(addPar);
		int index3DPoint = obj.id;
		int indexAttitudes = this->attitudes->getParameterIndex(rotPar);
		int indexMountRot = this->getParameterIndex(mountRotPar);
		int indexMountShift = this->getParameterIndex(mountShiftPar);
		int indexAddPar = this->getParameterIndex(addPar);


		Matrix AiMat(2,nParameter);	
		Matrix parameterAiMat(nParameter,1);
			
		double fac1 = -this->camera->getPrincipalDistance()/xc3D.z;
		double fac2 = -fac1*xc3D.x/xc3D.z;
		double fac3 = -fac1*xc3D.y/xc3D.z;

		// build AiMat and parameterAiMat
		for (int i=0; i < nPathParameter; i++)
		{
			AiMat.element(0,i) = fac1 * dFdPath.element(0,i) + fac2 * dFdPath.element(2,i);
			AiMat.element(1,i) = fac1 * dFdPath.element(1,i) + fac3 * dFdPath.element(2,i);
			parameterAiMat.element(i, 0) = Delta.element( indexPath + i, 0);
		}

		int offset = nPathParameter;
		for (int i=0; i< nPointParameter; i++)
		{
			AiMat.element(0,i + offset) = fac1 * dFdX.element(0,i) + fac2 * dFdX.element(2,i);
			AiMat.element(1,i + offset) = fac1 * dFdX.element(1,i) + fac3 * dFdX.element(2,i);
			parameterAiMat.element(offset + i, 0) = Delta.element( index3DPoint + i, 0);
		}  

		offset += nPointParameter;
		// compute the final partials for the attitudes
		for (int i=0; i < nAttitudeParameter; i++)
		{
			AiMat.element(0,i + offset) = fac1 *dFdAttitudes.element(0,i) + fac2 * dFdAttitudes.element(2,i);
			AiMat.element(1,i + offset) = fac1 *dFdAttitudes.element(1,i) + fac3 * dFdAttitudes.element(2,i);
			parameterAiMat.element(offset + i, 0) = Delta.element( indexAttitudes + i, 0);
		}


		// compute the final partials for the camera mounting rotation
		offset += nAttitudeParameter;
		for (int i=0; i < nMountRotParameter; i++)
		{
			AiMat.element(0,i + offset) = fac1 *dFdMountRot(0,i) + fac2 * dFdMountRot(2,i);
			AiMat.element(1,i + offset) = fac1 *dFdMountRot(1,i) + fac3 * dFdMountRot(2,i);
			parameterAiMat.element(offset + i, 0) = Delta.element( indexMountRot + i, 0);
		}


		// compute the final partials for the camera mounting shift
		offset += nMountRotParameter;
		const CRotationMatrix &mountRot = this->cameraMounting->getRotation().getRotMat();
		for (int i=0; i < nMountShiftParameter; i++)
		{
			AiMat.element(0,i + offset) = -fac1 *mountRot(0,i) - fac2 * mountRot(2,i);
			AiMat.element(1,i + offset) = -fac1 *mountRot(1,i) - fac3 * mountRot(2,i);
			parameterAiMat.element(offset + i, 0) = Delta.element( indexMountShift + i, 0);
		}

		offset += nMountShiftParameter;

		for (int i=0; i < nAdditionalParameter; i++)
		{
			AiMat.element(0,i + offset) = AiAdp(0,i);
			AiMat.element(1,i + offset) = AiAdp(1,i);
			parameterAiMat.element(offset + i, 0) = Delta.element( indexAddPar + i, 0);
		}


		// compute v
		FiMat = FiMat - AiMat * parameterAiMat;

		
		if (this->nActualParameter[addPar])
		{
			C2DPoint observation(obs[0],obs[1]);

			Matrix BT(2,2);
			BT=0;
			this->camera->Bi(BT,observation,C2DPoint(0,0),this->activeParameter[addPar]);
			
			FiMat = -obs.getCovar()* BT.t() * (BT * obs.getCovar() * BT.t()).i() * FiMat;

		}

  		for (int i=0; i < dimension; i++)
			residual[i] = FiMat.element(i,0);
	
		delete Rrpy;
	}

	
	return residual;
}

//================================================================================

void CPushBroomScanner::resetSensor()
{
	this->path = NULL;
	this->attitudes = NULL;
	this->camera = NULL;
	this->cameraMounting = NULL;
	  
	this->timeFixedECRMatrix = 0.0;

	this->normalVectorCCDLine = C2DPoint(0,0,0);
	this->refractionCorrection= C2DPoint(0,0,0);
	this->aberrationCorrection= C2DPoint(0,0,0);

	this->satelliteHeight = 0.0;
	this->groundHeight = 0.0;
	this->groundTemperature = 0.0;

	this->hasValues = false;


	
}

//================================================================================

void CPushBroomScanner::computeParameterCount()
{
	
}

//================================================================================

void CPushBroomScanner::crop(int offsetX, int offsetY, int newWidth, int newHeight)
{
	int hmm =9;

};

//================================================================================

void CPushBroomScanner::printStationParameter(CCharString& output,CRotationMatrix* rotMat)const
{
	ostrstream protocol;

	if (this->nActualParameter[addPar])
	{
		protocol << "\n      ";
		this->camera->printADPParameter(protocol,this->activeParameter[addPar]);
	}

	protocol << ends;
	char* s = protocol.str();
	output = s;
	delete [] s;
	

}

//================================================================================

int CPushBroomScanner::getDirectObsCount() const
{
	return this->nActualParameter[addPar];
}

//================================================================================

void CPushBroomScanner::addDirectObservations(Matrix &N, Matrix &t)
{
	if (this->nActualParameter[addPar])
	{
		this->camera->addDirectObservationsAdp(N,t,this->parameterIndex[addPar],this->activeParameter[addPar]);
	}
}

//================================================================================

double CPushBroomScanner::getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res)
{
	if (this->nActualParameter[addPar])
	{
		return this->camera->getResidualsDirectAdpObservations(deltaPar,res,0,this->activeParameter[addPar]);
	}
	
	return 0.0;
}

//================================================================================

void CPushBroomScanner::goingToBeDeleted()
{
	this->informListeners_elementsChanged();
}

//================================================================================

void CPushBroomScanner::setChangeMountingForMerging(bool changeForMerging)
{
	this->changeMountingForMerging = changeForMerging;
};

//================================================================================

void CPushBroomScanner::updateFirstLineTime(double newFirstLineTime)
{
	this->firstLineTime = newFirstLineTime;
}

//================================================================================

double CRefraction::getRefractionAngle(const double viewAngle, const double heightOfSattelite,const double heightOfArea,const double tempAtSealevel) const
{
	
	if (fabs(viewAngle) < FLT_EPSILON)
		return 0.0;
	
	// correct input values
	double c_tempAtSealevel = tempAtSealevel + this->temp;

	double RandHs = this->earthRadius + heightOfSattelite;
	double RandHp = this->earthRadius + heightOfArea;
	

	// zenit angle at unrefracted position
	double z0 = asin(sin(viewAngle)*RandHs/RandHp);
	
	// zenit angle at refracted ground position
	double tempFac = 1.0 - this->lapsRate * heightOfArea / c_tempAtSealevel;
	double densFac = pow(tempFac,this->gamma);
	double my = 1.0 + 0.0002905 * densFac;
	double zr = asin(sin(z0)/my);
	
	// angle refraction 
	double refr = ((my - 1.0)/(1.0+this->W/this->earthRadius))*(tan(zr) - 0.00117 * pow(tan(zr),3.0));
	
	// refraction in degree
	double z = refr + zr;
	double dang = z0 - z;

	// computation of dtheta
	double gammaR = z0 - viewAngle - dang;
	double h = sin(viewAngle)/sin(z)*RandHs - RandHp;
	double s = sqrt( pow(RandHp,2) + pow(RandHs,2) - 2*RandHs*RandHp*cos(gammaR));
	double dTheta = asin(sin(z)*h/s);


	
	return dTheta;
}

//================================================================================
