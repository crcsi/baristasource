#include "FrameCamera.h"

#include "newmat.h"

CFrameCamera::CFrameCamera(void):calibrationDay(0),calibrationMonth(0),calibrationYear(0),objectiveName(""),
	irpSymmetry(0,0,0)
{
	this->type = eDigitalFrame;
}

CFrameCamera::CFrameCamera(const CFrameCamera& src) : CCamera(src),
	calibrationDay(src.calibrationDay),calibrationMonth(src.calibrationMonth),
	calibrationYear(src.calibrationYear),objectiveName(src.objectiveName),
	irpSymmetry(src.irpSymmetry)
{
	this->type = eDigitalFrame;
}

CFrameCamera::~CFrameCamera(void)
{
}


CFrameCamera &CFrameCamera::operator = (const CFrameCamera & c)
{
	bool changeListenerStatus = false;
	if (this->callListener)
	{
		this->callListener = false;
		changeListenerStatus = true;
	}

	CCamera::operator=(c);
	this->calibrationDay = c.calibrationDay;
	this->calibrationMonth = c.calibrationMonth;
	this->calibrationYear = c.calibrationYear;
	this->objectiveName = c.objectiveName;
	this->type = eDigitalFrame;
	this->irpSymmetry = c.irpSymmetry;

	if (changeListenerStatus)
		this->callListener = true;

	if (this->callListener)
		this->informListeners_elementsChanged(this->getClassName().c_str());  

	return *this;
}


bool CFrameCamera::transformObs2Obj(C3DPoint& xc,  const C3DPoint& obs, double Z0, double Z, const CTrans2D* frameToCamera) const
{
	xc = obs * this->scale - this->interiorReferencePoint;
	xc.z = - xc.z;

	if (this->distortion)
		this->distortion->applyObs2Obj(xc,obs,this->interiorReferencePoint,this->scale);

	double dx, dy;
	computeRefractionAndEarthCurvature(Z0, Z, 6379000.0, xc.x, xc.y, dx, dy);
	xc.x += dx;
	xc.y += dy;
	
	return true;
}

bool CFrameCamera::transformObj2Obs(C2DPoint& obs, const C3DPoint& obj, double Z0, double Z, const CTrans2D* cameraToFrame) const
{
	obs.x = obj.x / obj.z;
	obs.y = obj.y / obj.z;

	obs = this->interiorReferencePoint.z * obs;

	double dx, dy;

	computeRefractionAndEarthCurvature(Z0, Z, 6379000.0, obs.x, obs.y, dx, dy);
	obs.x -= dx;
	obs.y -= dy;

	if (this->distortion)
		this->distortion->applyObj2Obs(obs,obs,this->interiorReferencePoint,this->scale);

	// transition to framlet cs
	obs.x += this->interiorReferencePoint.x;
	obs.y += this->interiorReferencePoint.y;

	obs /= this->scale;

	return true;
}


void CFrameCamera::computeRefractionAndEarthCurvature(double Z0, double Z, double R, double x, double y, double &dx, double &dy)const
{
	double k = getK(Z0, Z);
	double r2 = x * x + y * y;
	double dr = k + r2 / (this->interiorReferencePoint.z * this->interiorReferencePoint.z) * (k - (Z-Z0) / (2.0 * R));

	dx = x * dr;
	dy = y * dr;
};
	
Matrix CFrameCamera::getCalibrationMatrix() const
{
	Matrix K(3,3);

	K.element(0,0) = -this->interiorReferencePoint.z;  K.element(0,1) =  0.0;                               K.element(0,2) = -this->interiorReferencePoint.x; 
	K.element(1,0) =  0.0;                             K.element(1,1) = -this->interiorReferencePoint.z;    K.element(1,2) = -this->interiorReferencePoint.y; 
	K.element(2,0) =  0.0;                             K.element(2,1) =  0.0;                               K.element(2,2) = -1.0; 
	return K;
};


void CFrameCamera::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);
	
	// add first all the common things
	CCamera::serializeStore(pStructuredFileSection);

	// and now the private members
	CToken* pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("calibrationDay",this->calibrationDay);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("calibrationMonth",this->calibrationMonth);

	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("calibrationYear",this->calibrationYear);


	pNewToken = pStructuredFileSection->addToken();
	pNewToken->setValue("objectiveName",this->objectiveName.GetChar());

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->irpSymmetry.serializeStore(child);

}

void CFrameCamera::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	// add first all the common things
	CCamera::serializeRestore(pStructuredFileSection);

	// and now the private members
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("calibrationDay"))
			this->calibrationDay = token->getIntValue();
		else if(key.CompareNoCase("calibrationMonth"))
            this->calibrationMonth = token->getIntValue();
		else if(key.CompareNoCase("calibrationYear"))
            this->calibrationYear = token->getIntValue();
		else if(key.CompareNoCase("objectiveName"))
            this->objectiveName = token->getValue();

    }

   for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CXYZPoint"))
        {
            this->irpSymmetry.serializeRestore(child);
        }

    }


}
