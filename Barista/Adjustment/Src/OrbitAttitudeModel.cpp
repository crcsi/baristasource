#include "OrbitAttitudeModel.h"
#include "RotationBaseFactory.h"
#include "AdjustMatrix.h"
#include "utilities.h"
#include <strstream>

COrbitAttitudeModel::COrbitAttitudeModel(void) : CSplineModel(0,3,3,3),
	rotParameter(&C3DPoint(0,0,0)),angleObservation(0,0,0),scaleObservation(0,0,0),scaleParameter(0,0,0)
{
	this->fixedECRMatrix.setParent(this);
	this->rotParameter.setParent(this);
	this->angleObservation.setParent(this);
	this->scaleObservation.setParent(this);
	this->scaleParameter.setParent(this);


	CXYZPoint directObs(0,0,0);
	directObs.setCovarElement(0,0,1.4e-2);
	directObs.setCovarElement(1,1,1.4e-2);
	directObs.setCovarElement(2,2,1.4e-2);
	this->setDirectObservationForAngles(directObs);

	directObs.setCovarElement(0,0,1.4e-6);
	directObs.setCovarElement(1,1,1.4e-6);
	directObs.setCovarElement(2,2,1.4e-6);
	this->setDirectObservationForScale(directObs);

	this->scale = 1000.0;
	this->i_scale = 1.0 / this->scale;
}

COrbitAttitudeModel::~COrbitAttitudeModel(void)
{
}

COrbitAttitudeModel::COrbitAttitudeModel(const COrbitAttitudeModel& model)
	: CSplineModel(model),rotParameter(model.rotParameter),
	angleObservation(model.angleObservation),
	scaleObservation(model.scaleObservation),scaleParameter(model.scaleParameter),
	fixedECRMatrix(model.fixedECRMatrix),timeFixedECRMatrix(model.timeFixedECRMatrix)
{
	this->fixedECRMatrix.setParent(this);
	this->rotParameter.setParent(this);
	this->angleObservation.setParent(this);
	this->scaleObservation.setParent(this);
	this->scaleParameter.setParent(this);
}



COrbitAttitudeModel &COrbitAttitudeModel::operator = (const COrbitAttitudeModel & orbitAttitudes)
{
	CSplineModel::operator =(orbitAttitudes);
	
	this->fixedECRMatrix = orbitAttitudes.fixedECRMatrix;
	this->timeFixedECRMatrix = orbitAttitudes.timeFixedECRMatrix;

	this->angleObservation = orbitAttitudes.angleObservation;
	this->scaleObservation = orbitAttitudes.scaleObservation;

	this->scaleParameter = orbitAttitudes.scaleParameter;
	this->rotParameter = orbitAttitudes.rotParameter;

	this->informListeners_elementsChanged("COrbitAttitudeModel");


	return *this;
}



void COrbitAttitudeModel::getName(CCharString& names) const
{
    names = "Orbit Attitude Model";
}

int COrbitAttitudeModel::getDirectObsCount() const
{
	int n = 0;
	n += this->nActualParameter[rotPar] ? 3 : 0;
	n += this->nActualParameter[scalePar] ? 3 : 0;
	n += this->getNDirectObservationsForSpline();

	return n;
}

void COrbitAttitudeModel::extend_N_and_t_Matrix(Matrix& N,Matrix& t,
											const CAdjustMatrix& AxTP,
											const CAdjustMatrix& AyTP, 
											const CAdjustMatrix& AzTP,
											const CAdjustMatrix& P,
											const CAdjustMatrix& dl,
											const int indexNx,
											const int indexNy,
											const int indexNz,
											const CObsPoint &obs)const
{
	if (this->nActualParameter[rotPar])
	{
		int indexRot = this->parameterIndex[rotPar];

		// Arot = I;

		// add to N Matrix (ArotT*P*Arot) and t vector
		P.addToMatrix(indexRot,indexRot,N);
		CAdjustMatrix ArotTPdl = P * dl;
		ArotTPdl.addToMatrix(indexRot,0,t);

		// add mixed part
		AxTP.addToMatrix(indexNx,indexRot,N,true);
		AyTP.addToMatrix(indexNy,indexRot,N,true);
		AzTP.addToMatrix(indexNz,indexRot,N,true);
	}

	if (this->nActualParameter[scalePar])
	{

		int indexScale = this->parameterIndex[scalePar];
		double time = obs.dot;

		// Ascale = time*I;

		// add to N Matrix (time^2*P) and t vector
		CAdjustMatrix ATPA = P * time*time;
		ATPA.addToMatrix(indexScale,indexScale,N);
		CAdjustMatrix ArotTPdl = P * dl * time;
		ArotTPdl.addToMatrix(indexScale,0,t);

		// add mixed part
		ATPA = AxTP * time;
		ATPA.addToMatrix(indexNx,indexScale,N,true);
		ATPA = AyTP * time;
		ATPA.addToMatrix(indexNy,indexScale,N,true);
		ATPA = AzTP * time;
		ATPA.addToMatrix(indexNz,indexScale,N,true);
	}

}

void COrbitAttitudeModel::extend_Ai(Matrix &AiErp,Matrix &AiRot,Matrix &AiSca, 
								Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
								Matrix &AiAdd, Matrix &AiPoint,const Matrix &obs, const C3DPoint &approx)const
{
	int dimension = obs.Ncols()-1;

	if (this->nActualParameter[rotPar])
	{
		AiRot.ReSize(dimension,this->nActualParameter[rotPar]);
		AiRot = 0;
		for (int i=0; i <this->nActualParameter[rotPar]; i++)
			AiRot.element(i,i) = 1.0;
	}

	if (this->nActualParameter[scalePar])
	{
		double time = obs.element(0,dimension);

		AiSca.ReSize(3,this->nActualParameter[scalePar]);
		AiSca = 0;
		for (int i=0; i <this->nActualParameter[scalePar]; i++)
			AiSca.element(i,i) = time;		
	}

}

void COrbitAttitudeModel::extend_Bi(Matrix& Fi,const Matrix &obs, const C3DPoint &approx)const
{
	
}

bool COrbitAttitudeModel::Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const
{
	CSplineModel::Fi(Fi,obs,approx);

	if (this->nActualParameter[rotPar])
	{
		Fi.element(0,0) += this->rotParameter[0];
		Fi.element(1,0) += this->rotParameter[1];
		Fi.element(2,0) += this->rotParameter[2];
	}

	if (this->nActualParameter[scalePar])
	{
		double time = obs.element(0,obs.Ncols()-1);

		Fi.element(0,0) += this->scaleParameter[0] * time;
		Fi.element(1,0) += this->scaleParameter[1] * time;
		Fi.element(2,0) += this->scaleParameter[2] * time;
	}

	return true;

}

bool COrbitAttitudeModel::Fi(CObsPoint& fi, const CObsPoint &obs) const
{
	bool ret = CSplineModel::Fi(fi,obs);

	if (!ret)
		return false;

	if (this->nActualParameter[rotPar])
	{
		fi[0] += this->rotParameter[0];
		fi[1] += this->rotParameter[1];
		fi[2] += this->rotParameter[2];
	}

	if (this->nActualParameter[scalePar])
	{
		double time = obs.dot;

		fi[0] += this->scaleParameter[0] * time;
		fi[1] += this->scaleParameter[1] * time;
		fi[2] += this->scaleParameter[2] * time;
	}

	return true;
}


CXYZPoint COrbitAttitudeModel::getRotParameter() const 
{
	CXYZPoint tmp(this->rotParameter[0],this->rotParameter[1],this->rotParameter[2]);
	tmp.setCovariance(*this->rotParameter.getCovariance());
	return tmp;
}

void COrbitAttitudeModel::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("timeFixedECRMatrix", this->timeFixedECRMatrix);

	token = pStructuredFileSection->addToken();
	token->setValue("actualRotPar", this->nActualParameter[rotPar]);
		
	token = pStructuredFileSection->addToken();
	token->setValue("actualScalePar", this->nActualParameter[scalePar]);

	token = pStructuredFileSection->addToken();
	token->setValue("fileName", this->FileName);

	token = pStructuredFileSection->addToken();
	token->setValue("useDirectObservationsForSpline", this->useDirectObservationsForSpline);

	token = pStructuredFileSection->addToken();
	token->setValue("scale", this->scale);

	CStructuredFileSection* pNewSection = pStructuredFileSection->addChild();;
	offsetSpline.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild(); 
	spline.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild(); 
	fixedECRMatrix.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild();
	this->rotParameter.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild();
	this->angleObservation.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild();
	this->scaleParameter.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild();
	this->scaleObservation.serializeStore(pNewSection);

	if (this->useDirectObservationsForSpline)
	{
		for (unsigned int i=0; i < this->directObservations.size(); i++)
		{
			pNewSection = pStructuredFileSection->addChild();
			this->directObservations[i].serializeStore(pNewSection);
		}
	}

}

void COrbitAttitudeModel::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	// reset the scale to 1 so we can check if and old project has been read
	this->scale = 1.0;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("timeFixedECRMatrix"))
            this->setTimeFixedECRMatrix( token->getDoubleValue());
		else if(key.CompareNoCase("actualRotPar"))
		{
			int par = token->getIntValue();
			if (par) 
				this->activateParameterGroup(rotPar);
			else
				this->deactivateParameterGroup(rotPar);
		}
		else if(key.CompareNoCase("actualScalePar"))
		{
			int par = token->getIntValue();
			if (par) 
				this->activateParameterGroup(scalePar);
			else
				this->deactivateParameterGroup(scalePar);
		}
		else if(key.CompareNoCase("fileName"))
		{
			this->setFileName(token->getValue());
		}
		else if(key.CompareNoCase("useDirectObservationsForSpline"))
		{
			this->useDirectObservationsForSpline = token->getBoolValue();
		}
		else if(key.CompareNoCase("scale"))
		{
			this->setScale(token->getDoubleValue());
		}
    }

	bool readRot = false;
	bool readScale = false;

	this->directObservations.clear();

	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("CObsPoint"))
			offsetSpline.serializeRestore(pChild);
		else if (pChild->getName().CompareNoCase("CSpline"))
			spline.serializeRestore(pChild);
		else if (pChild->getName().CompareNoCase("CRotationMatrix"))
			fixedECRMatrix.serializeRestore(pChild);
		else if (pChild->getName().CompareNoCase("CRollPitchYawRotation"))
			this->rotParameter.serializeRestore(pChild);
		else if (pChild->getName().CompareNoCase("CXYZPoint") && !readRot && !readScale)
		{
			readRot = true;		
			this->angleObservation.serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase("CXYZPoint") && readRot && !readScale)
		{
			readScale = true;
			this->scaleParameter.serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase("CXYZPoint") && readRot && readScale)
		{
			this->scaleObservation.serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase("CDirectSplineObservation"))
		{
			CDirectSplineObservation obs;
			obs.serializeRestore(pChild);
			this->directObservations.push_back(obs);
		}

	}

	// check for old projects, we now use mrad instead of rad
	if (fabs(this->scale - 1.0) < 0.000001)
	{

		// change the spline offset
		this->offsetSpline *= 1000.0;

		// change angle offsets, we actually don't use the rot mat, so no need to update
		this->rotParameter[0] *= 1000.0;
		this->rotParameter[1] *= 1000.0;
		this->rotParameter[2] *= 1000.0;
		(*this->rotParameter.getCovariance()) *= 1000000.0;

		// change the observation for the angle offset
		for (int i=0; i < this->angleObservation.getDimension(); i++)
			this->angleObservation[i] *= 1000.0;
		(*this->angleObservation.getCovariance()) *=1000000.0;

		// change spline parameters
		this->spline.applyScaleToOldProject(1000.0);

		// don't change the scale here!
		// the orbit observation object needs the wrong scale to check for old projects
	}


	this->computeParameterCount();
}

void COrbitAttitudeModel::reconnectPointers()
{
	CSerializable::reconnectPointers();
	offsetSpline.reconnectPointers();
	spline.reconnectPointers();
	fixedECRMatrix.reconnectPointers();
	this->rotParameter.reconnectPointers();
	this->angleObservation.reconnectPointers();
	this->scaleParameter.reconnectPointers();
	this->scaleObservation.reconnectPointers();
}

void COrbitAttitudeModel::resetPointers()
{
	CSerializable::resetPointers();
	offsetSpline.resetPointers();
	spline.resetPointers();
	fixedECRMatrix.resetPointers();
	this->rotParameter.resetPointers();
	this->angleObservation.resetPointers();
	this->scaleParameter.resetPointers();
	this->scaleObservation.resetPointers();
}


bool COrbitAttitudeModel::getRotMat(CRotationMatrix& rotMat, const double& time) const
{
	C3DPoint rpy;
	this->evaluateLocation(rpy,time);
	CRotationBase* rotRPY = CRotationBaseFactory::initRotation(&rpy,RPY);
	rotMat = rotRPY->getRotMat();
	delete rotRPY;
	return true;
}


bool COrbitAttitudeModel::getAllParameters(Matrix &parms) const
{
	int size = CSplineModel::getParameterCount(addPar) + this->nActualParameter[rotPar] + this->nActualParameter[scalePar];
	parms.ReSize(size,1);

	int index = 0;

	if (this->nActualParameter[rotPar])
	{
		parms.element(index     ,0) = this->rotParameter[0];
		parms.element(index + 1 ,0) = this->rotParameter[1];
		parms.element(index + 2 ,0) = this->rotParameter[2];

		index += 3;
	}

	if (this->nActualParameter[scalePar])
	{
		parms.element(index     ,0) = this->scaleParameter[0]*M_PI/180.0;
		parms.element(index + 1 ,0) = this->scaleParameter[1]*M_PI/180.0;
		parms.element(index + 2 ,0) = this->scaleParameter[2]*M_PI/180.0;

		index += 3;
	}

	return CSplineModel::getAllParameters(parms);
}

bool COrbitAttitudeModel::getAdjustableParameters(Matrix &parms) const
{
	int size = CSplineModel::getParameterCount(addPar) + this->nActualParameter[rotPar];
	parms.ReSize(size,1);
	
	int index = 0;

	if (this->nActualParameter[rotPar])
	{
		parms.element(index     ,0) = this->rotParameter[0];
		parms.element(index + 1 ,0) = this->rotParameter[1];
		parms.element(index + 2 ,0) = this->rotParameter[2];

		index += 3;
	}

	if (this->nActualParameter[scalePar])
	{
		parms.element(index     ,0) = this->scaleParameter[0]*M_PI/180.0;
		parms.element(index + 1 ,0) = this->scaleParameter[1]*M_PI/180.0;
		parms.element(index + 2 ,0) = this->scaleParameter[2]*M_PI/180.0;


		index += 3;
	}

	return CSplineModel::getAdjustableParameters(parms);
}

bool COrbitAttitudeModel::insertParametersForAdjustment(Matrix &parms)
{
	int addIndex = this->parameterIndex[addPar];
	this->spline.insertParametersForAdjustment(parms,addIndex);

	if (this->nActualParameter[rotPar])
	{
		int rotIndex = this->parameterIndex[rotPar];
		parms.element(rotIndex,0) = this->rotParameter[0];
		parms.element(rotIndex + 1,0) = this->rotParameter[1];
		parms.element(rotIndex + 2,0) = this->rotParameter[2];
	}

	if (this->nActualParameter[scalePar])
	{
		int scaleIndex = this->parameterIndex[scalePar];
		parms.element(scaleIndex,0) = this->scaleParameter[0];
		parms.element(scaleIndex + 1,0) = this->scaleParameter[1];
		parms.element(scaleIndex + 2,0) = this->scaleParameter[2];
	}

	return true;
}

bool COrbitAttitudeModel::setParametersFromAdjustment(const Matrix &parms)
{
	int addIndex = this->parameterIndex[addPar];
	this->spline.setParametersFromAdjustment(parms,addIndex);

	if (this->nActualParameter[rotPar])
	{
		int rotIndex = this->parameterIndex[rotPar];
		C3DPoint tmp(parms.element(rotIndex,0),parms.element(rotIndex + 1,0),parms.element(rotIndex + 2,0));
		this->rotParameter.updateAngles_Rho(&tmp);
	}

	if (this->nActualParameter[scalePar])
	{
		int scaleIndex = this->parameterIndex[scalePar];
		this->scaleParameter.x = parms.element(scaleIndex  ,0);
		this->scaleParameter.y = parms.element(scaleIndex+1,0);
		this->scaleParameter.z = parms.element(scaleIndex+2,0);

	}

	return true;
}

void COrbitAttitudeModel::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
	StationNames.RemoveAll();
	PointNames.RemoveAll();	

	if (this->nActualParameter[rotPar])
	{
		StationNames.Add("Attitude Rotation            Parameter[m�]	       Sigma[m�]\n      "
						 "                 Roll ");
		StationNames.Add("                Pitch ");
		StationNames.Add("                  Yaw ");
	}

	if (this->nActualParameter[scalePar])
	{
		StationNames.Add("Time dependant Rotation      Parameter[1/sec]	       Sigma[1/sec]\n      "
						 "                 Roll ");
		StationNames.Add("                Pitch ");
		StationNames.Add("                  Yaw ");
	}

	CSplineModel::getParameterNames(StationNames,PointNames);
}

void COrbitAttitudeModel::addDirectObservations(Matrix &N, Matrix &t) 
{
	if (this->nActualParameter[rotPar])
	{
		int indexRot = this->parameterIndex[rotPar];
		Matrix Q = *this->angleObservation.getCovariance();
		Matrix P = Q.i();

		Matrix g(3,1);
		g.element(0,0) = this->rotParameter[0];
		g.element(1,0) = this->rotParameter[1];
		g.element(2,0) = this->rotParameter[2];

		Matrix ATPl = P * g;
		this->addMatrix(indexRot,indexRot,N,P);
		this->addMatrix(indexRot,0,t,ATPl);
	}

	if (this->nActualParameter[scalePar])
	{
		int indexScale = this->parameterIndex[scalePar];
		Matrix Q = *this->scaleObservation.getCovariance();
		Matrix P = Q.i();

		Matrix g(3,1);
		g.element(0,0) = this->scaleParameter[0];
		g.element(1,0) = this->scaleParameter[1];
		g.element(2,0) = this->scaleParameter[2];

		Matrix ATPl = P * g;
		this->addMatrix(indexScale,indexScale,N,P);
		this->addMatrix(indexScale,0,t,ATPl);
	}

	CSplineModel::addDirectObservations(N,t);

}

double COrbitAttitudeModel::getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res)
{
	double value = 0.0;
	int index = 0;

	if (this->nActualParameter[rotPar])
	{	
		res.ReSize(3,1);
		res.element(0,0) = - deltaPar.element(index,0)   + this->rotParameter[0];
		res.element(1,0) = - deltaPar.element(index+1,0) + this->rotParameter[1];
		res.element(2,0) = - deltaPar.element(index+2,0) + this->rotParameter[2];
		
		Matrix Q = *this->angleObservation.getCovariance();
		Matrix P = Q.i();

		Matrix vtv = res.t() * P * res;
		value+= vtv.element(0,0);
		
		index += 3;
	}

	if (this->nActualParameter[scalePar])
	{	
		res.ReSize(3,1);
		res.element(0,0) = - deltaPar.element(index,0) + this->scaleParameter[0];
		res.element(1,0) = - deltaPar.element(index+1,0) + this->scaleParameter[1];
		res.element(2,0) = - deltaPar.element(index+2,0) + this->scaleParameter[2];
		
		Matrix Q = *this->scaleObservation.getCovariance();
		Matrix P = Q.i();

		Matrix vtv = res.t() * P * res;
		value+= vtv.element(0,0);

		index += 3;
	}

	
	value += CSplineModel::getResidualsDirectObservations(deltaPar,res);

	return value;

};

void COrbitAttitudeModel::getObservationNames(CCharStringArray &names) const
{
	names.RemoveAll();
    
    CCharString* name = names.Add();
    *name = "Roll";
	name = names.Add();
	*name = "Pitch";
	name = names.Add();
	*name = "Yaw";


}

int COrbitAttitudeModel::coordPrecision() const
{
	return 4;
};

void COrbitAttitudeModel::convertToDifferentialUnits(CObsPoint &out, const CObsPoint &in) const
{
	out = in;
	out *= 180/M_PI;
};


CXYZPoint COrbitAttitudeModel::getScaleParameter() const
{
	return this->scaleParameter;
}

void COrbitAttitudeModel::setRotParameter(const CXYZPoint& rot)
{
	this->rotParameter.updateAngles_Rho(&rot);
	this->rotParameter.setCovariance(rot.getCovar());

	this->informListeners_elementsChanged(this->getClassName().c_str());  


}


void COrbitAttitudeModel::setScaleParameter(const CXYZPoint& scale)
{
	this->scaleParameter = scale;

	this->informListeners_elementsChanged(this->getClassName().c_str()); 
}

void COrbitAttitudeModel::setDirectObservationForAngles(const CXYZPoint& obs)
{
	this->angleObservation = obs;

	this->informListeners_elementsChanged(this->getClassName().c_str()); 
}

void COrbitAttitudeModel::setDirectObservationForScale(CXYZPoint& obs)
{
	this->scaleObservation = obs;

	this->informListeners_elementsChanged(this->getClassName().c_str()); 

}



void COrbitAttitudeModel::setCovariance(const Matrix &covar)
{
	int index = 0;

	if (this->nActualParameter[rotPar])
	{
		Matrix* rotCovar = this->rotParameter.getCovariance();
		*rotCovar = covar.SubMatrix(index+1,index+3,index+1,index+3);

		index += 3;
	}

	if (this->nActualParameter[scalePar])
	{
		Matrix* scaleCovar = this->scaleParameter.getCovariance();
		*scaleCovar = covar.SubMatrix(index+1,index+3,index+1,index+3);	

		index += 3;
	}

	this->covariance = covar;
	
	this->informListeners_elementsChanged(this->getClassName().c_str()); 

}


void COrbitAttitudeModel::printStationParameter(CCharString& output,CRotationMatrix* rotMatL)const
{
	ostrstream protocol;

	int widthParameterName = 12;
	int widthParameter = 15;
	int widthSigma = 15;

	int precisionParameterSec = 2;
	int precisionSigmaSec = 2;

	int precisionParameterDeg = 4;
	int precisionSigmaDeg = 4;

	int parameterOffset =0;
	protocol.setf(ios::fixed, ios::floatfield);

	if (this->nActualParameter[rotPar])
	{
		parameterOffset +=3;

		protocol << endl << "      Attitude Rotation         Parameter[sec]	       Sigma[sec]\n";
		protocol.width(widthParameterName); 
		protocol << "                 Roll :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameterSec);
		protocol << this->rotParameter[0] * 3.6 * 180.0 / M_PI ;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigmaSec);
		protocol << this->rotParameter.getSigma(0)  * 3.6* 180.0 / M_PI << endl;
	
		protocol.width(widthParameterName); 
		protocol << "                Pitch :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameterSec);
		protocol << this->rotParameter[1] * 3.6 * 180.0 / M_PI ;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigmaSec);
		protocol << this->rotParameter.getSigma(1) * 3.6 * 180.0 / M_PI << endl;

		protocol.width(widthParameterName); 
		protocol << "                  Yaw :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameterSec);
		protocol << this->rotParameter[2] * 3.6 * 180.0 / M_PI ;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigmaSec);
		protocol << this->rotParameter.getSigma(2) * 3.6 * 180.0 / M_PI << endl;

	
	}

	if (this->nActualParameter[scalePar])
	{
		parameterOffset +=3;


		protocol << endl <<  "Time dependant Rotation      Parameter[1/sec]	       Sigma[1/sec]\n";
		protocol.width(widthParameterName); 
		protocol << "                 Roll :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameterSec);
		protocol << this->scaleParameter.x ;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigmaSec);
		protocol <<this->scaleParameter.getSigma(0) << endl;


		protocol.width(widthParameterName); 
		protocol << "                Pitch :";
		protocol.width(widthParameter);
		protocol.precision(precisionParameterSec);
		protocol << this->scaleParameter.y;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigmaSec);
		protocol << this->scaleParameter.getSigma(1) << endl;

		protocol.width(widthParameterName); 
		protocol << "                  Yaw ";
		protocol.width(widthParameter);
		protocol.precision(precisionParameterSec);
		protocol << this->scaleParameter.z;
		protocol << " +- ";
		protocol.width(widthSigma);
		protocol.precision(precisionSigmaSec);
		protocol << this->scaleParameter.getSigma(2) << endl;

	}


	CCharStringArray StationNames, PointNames;
	this->getParameterNames(StationNames, PointNames);
	Matrix ModelPars;
	this->getAdjustableParameters(ModelPars);
	protocol.precision(4);
	for (int j = parameterOffset; j < ModelPars.Nrows(); ++j)
	{
		CCharString *parName = StationNames.GetAt(j);
		protocol << "\n      ";
		protocol.width(5); 
		protocol << parName->GetChar() << ": ";
		protocol.width(widthParameter); 
		CObsPoint oldUnit(2);
		CObsPoint newUnit(2);
		oldUnit[0] = ModelPars.element(j,0);
		oldUnit[1] = sqrt(this->covariance.element(j,j));
		this->convertToDifferentialUnits(newUnit,oldUnit);
		protocol.precision(precisionParameterDeg);
		protocol << newUnit[0] << " +- ";
		protocol.width(widthSigma); 
		protocol.precision(precisionSigmaDeg);
		protocol << newUnit[1] ;
	}

	protocol << ends;
	char* s = protocol.str();
	output = s;
	delete [] s;
	

}

void COrbitAttitudeModel::initDirectObservations()
{
	// this initializes the vector
	CSplineModel::initDirectObservations();

	// now we set the right values for this sensor model
	int segments = this->spline.getNumberOfSegments();
	doubles observationsRoll(segments);
	doubles sigmasRoll(segments);

	doubles observationsPitch(segments);
	doubles sigmasPitch(segments);

	double yaw;
	double sigmaConstantPitch = 0.087; // 5 mdeg
	double sigmaConstantRoll = 17.45; // 1 deg

	for (int i=0; i < segments; i++)
	{
		this->spline.getSplineParameter(i,0,observationsRoll[i],
										    observationsPitch[i],
										    yaw);

		sigmasRoll[i] = sigmaConstantRoll;
		sigmasPitch[i] = sigmaConstantPitch;

	}


	// this will overwrite the default sigma values and activate these two parameters
	this->setDirectObservationsForSpline(0,0,observationsRoll,sigmasRoll);
	this->setDirectObservationsForSpline(0,1,observationsPitch,sigmasPitch);


	this->informListeners_elementsChanged(this->getClassName().c_str());

}
