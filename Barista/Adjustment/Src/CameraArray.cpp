#include "CameraArray.h"
#include "CCDLine.h"
#include "FrameCamera.h"
#include "FrameCameraAnalogue.h"
#include "CameraFactory.h"

CCameraArray::CCameraArray(void) : nextCameraType(eCCD)
{
}

CCameraArray::CCameraArray(const CCameraArray& src) : nextCameraType(src.nextCameraType)
{
	
	for (int i=0; i < src.GetSize();i++)
		this->Add(*src.GetAt(i));


		
}

CCameraArray::~CCameraArray(void)
{
}


void CCameraArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CStructuredFileSection* pNewSection;

	for (int i=0; i< this->GetSize(); i++)
	{
		CCamera* camera = this->GetAt(i);
		pNewSection = pStructuredFileSection->addChild();
		camera->serializeStore(pNewSection);
	}
}


void CCameraArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);


	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("CCCDLine"))
		{
			CCamera* camera = this->Add(eCCD);
			camera->serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase("CFrameCamera"))
		{
			CCamera* camera = this->Add(eDigitalFrame);
			camera->serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase("CFrameCameraAnalogue"))
		{
			CCamera* camera = this->Add(eAnalogueFrame);
			camera->serializeRestore(pChild);
		}
	}
}



bool CCameraArray::isModified()
{
	return this->bModified;
}

void CCameraArray::reconnectPointers()
{
	CSerializable::reconnectPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		CCamera* pCamera= this->GetAt(i);
		pCamera->reconnectPointers();
	}
}

void CCameraArray::resetPointers()
{
	CSerializable::resetPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		CCamera* pCamera= this->GetAt(i);
		pCamera->resetPointers();
	}
}


CCamera* CCameraArray::Add()
{
	CCamera* newCamera = CMUObjectArray<CCamera, CCameraPtrArray>::Add();
	newCamera->setParent(this);
	return newCamera;
}

CCamera* CCameraArray::Add(const CCamera& camera)
{
	this->nextCameraType = camera.getCameraType();

	CCamera* newCamera = CMUObjectArray<CCamera, CCameraPtrArray>::Add(camera);
	newCamera->setParent(this);
	return newCamera;
}

CCamera* CCameraArray::Add(CameraType type)
{
	this->nextCameraType = type;

	CCamera* newCamera = this->Add();
	
	return newCamera;
}



CCamera* CCameraArray::CreateType() const 
{ 
	return CCameraFactory::createCamera(this->nextCameraType);	
}


CCamera* CCameraArray::CreateType(const CCamera& camera) const
{
	return CCameraFactory::createCamera(&camera);	
}

CCamera* CCameraArray::getCamerabyName(CCharString* name) const
{
    CCamera* test = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        test = this->GetAt(i);
		if (name->CompareNoCase(test->getName().GetChar()))
            return test;

        continue;
    }

    return NULL;
}