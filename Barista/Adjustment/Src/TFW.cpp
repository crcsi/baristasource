/**
 * @class CTFW
 * CTFW stores and handles tfw parameters (world file)
 */
#include <float.h>

#include "newmat.h"

#include "TFW.h"

#include "2DPoint.h"
#include "3DPoint.h"
#include "3Dplane.h"
#include "3DadjPlane.h"
#include "XYZLinesAdj.h"

#include "ReferenceSystem.h"
#include "XYPoint.h"
#include "XYZPoint.h"
#include "XYPointArray.h"
#include "XYZPointArray.h"

#include "TextFile.h"

CTFW::CTFW() : CSensorModel(false, "TFW", 0, 0, 0, 0, 0, 0, 6, 2, 2),
	a(1), b (0), c(0), d (0), e(-1), f(0)
{
	this->refSystem.sethasValues(false);
}

/**
 * contructor from existing tfw
 * @param tfw
 */

CTFW::CTFW(const CTFW &tfw) : CSensorModel(tfw),
 a(tfw.a), b (tfw.b), c(tfw.c), d (tfw.d), e(tfw.e), f(tfw.f)
{  
}

CTFW::CTFW(const Matrix &parms) : CSensorModel(true, "TFW", 0, 0, 0, 0, 0, 0, 6, 2, 2),
    a(parms.element(0, 0)), b(parms.element(1, 0)), c(parms.element(2, 0)),
    d(parms.element(3, 0)), e(parms.element(4, 0)), f(parms.element(5, 0))
{
}

CTFW::~CTFW()
{
}
	
CTFW &CTFW::operator = (const CTFW &tfw) 
{
	if (&tfw != this)
	{
		initFromModel(tfw);
		this->hasValues = tfw.hasValues;
   
		a = tfw.a;
		b = tfw.b;
		c = tfw.c;
		d = tfw.d;
		e = tfw.e; 
		f = tfw.f;

		this->informListeners_elementsChanged();
	}

	return *this;
};
	
bool CTFW::operator == (const CTFW &tfw) const
{
	if (this->a != tfw.a) return false;
	if (this->b != tfw.b) return false;
	if (this->c != tfw.c) return false;
	if (this->d != tfw.d) return false;
	if (this->e != tfw.e) return false;
	if (this->f != tfw.f) return false;

	if (this->getRefSys() != tfw.getRefSys() ) return false;

	return true;
};
	
void CTFW::reset()
{	
	this->a =  1.0;
	this->b =  0.0;
	this->c =  0.0;
	this->d =  0.0;
	this->e = -1.0;
	this->f =  0.0;

	this->hasValues = false;
};

//================================================================================

double CTFW::getScaleX() const
{
	return sqrt(this->a * this->a + this->d * this->d);
};

//================================================================================

double CTFW::getScaleY() const
{
	return sqrt(this->b * this->b + this->e * this->e);
};

//================================================================================

void CTFW::setFromShiftAndPixelSize(const C2DPoint &shift, const C2DPoint &pixelsize)
{
	if (this->refSystem.getCoordinateType() != eGEOGRAPHIC)
	{
		this->a = pixelsize.x;
		this->b = 0.0;
		this->c = shift.x;
		this->d = 0.0;
		this->e = -pixelsize.y;
		this->f = shift.y;
	}
	else
	{
		this->a = 0.0;
		this->b = -pixelsize.x;
		this->c = shift.x;
		this->d = pixelsize.y;
		this->e = 0.0;
		this->f = shift.y;
	}
	
	this->sethasValues(true);
};

void CTFW::setShift(const C2DPoint &shift)
{
		this->c = shift.x;
		this->f = shift.y;
}

void CTFW::copy(const CTFW &tfw)
{
	initFromModel(tfw);
    this->hasValues = true;
   
	a = tfw.a;
	b = tfw.b;
	c = tfw.c;
	d = tfw.d;
	e = tfw.e; 
	f = tfw.f;

    this->informListeners_elementsChanged();
};

/**
 * Sets tfw parameter from Matrix
 * @param parms
 */
void CTFW::setAll(const Matrix &parms)
{
    this->hasValues = true;

    this->a = parms.element(0, 0);
    this->b = parms.element(1, 0);
    this->c = parms.element(2, 0);
    this->d = parms.element(3, 0);
    this->e = parms.element(4, 0);
    this->f = parms.element(5, 0);

    this->informListeners_elementsChanged();
}

bool CTFW::invert()
{
	double determinant = this->a * this->e - this->b * this->d;
	if (fabs(determinant) < FLT_EPSILON) return false;

	determinant = 1.0 / determinant;

    this->d *= -determinant;
    this->b *= -determinant;
	double hlp = this->a;
	this->a = this->e * determinant;
	this->e = hlp * determinant;

	double x = this->c;
	double y = this->f;
	this->c = -this->a * x - this->b * y;
	this->f = -this->d * x - this->e * y;

	return true;
};

void CTFW::concatenate(const CTFW &tfw)
{
	double A = this->a, B = this->b, D = this->d, E = this->e;

	this->a = A * tfw.a + B * tfw.d;
	this->b = A * tfw.b + B * tfw.e;
	this->d = D * tfw.a + E * tfw.d;
	this->e = D * tfw.b + E * tfw.e;

	this->c += A * tfw.c + B * tfw.f ;
	this->f += D * tfw.c + E * tfw.f;
};

    /**
     * Transforms an xyzpoint to its cooresponding xy point
     * in image space
     * The xyz point is assumed to be in the same coordinate system
     * as the used tfw parameters.
     *
     * @param xyz The xyzpoint
     * @return the transformed xy point
     */
void CTFW::transformObj2Obs(C2DPoint  &obs, const C3DPoint &obj) const
{
	double x = obj.x;
	double y = obj.y;

	if (this->nActualStationPars == 6 )
	{
		double det = this->a*this->e - this->b*this->d;
		x -= this->c;
		y -= this->f;
		if (fabs(det) > DBL_EPSILON)
		{
			det = 1.0f / det;
			obs.x = ( this->e * x - this->b * y) * det;
			obs.y = (-this->d * x + this->a * y) * det;
		}
	}
	else if (this->nActualStationPars == 4 )
	{

		// @todo this doesn't really  work
		Matrix conformal;

		this->getAllParameters(conformal);

		double a1 = conformal.element(0,0);
		double a2 = conformal.element(1,0);
		double a3 = conformal.element(2,0);
		double a4 = conformal.element(3,0);

		if (this->refSystem.getCoordinateType() == eGRID)
		{
			y = -1.0*y;
		}

		obs.x = (a1*x - a1*a2 + a2*y - a4)/(a1*a1 - a2*a2);
		obs.y = (y - a2* obs.x - a4)/ a1;
	}
}
void CTFW::transformObj2Obs(C3DPoint  &obs, const C3DPoint &obj) const
{
	C2DPoint obs2D;
	transformObj2Obs(obs2D, obj);
	obs.x = obs2D.x;
	obs.y = obs2D.y;
	obs.z = 0.0f;
}
    /**
     * Transforms an xypoint to its cooresponding xyz point
     * in object space
     * The xyz point is assumed to be in the same coordinate system
     * as the used tfw parameters.
     *
     * @param xy The xypoint
     * @return the transformed xyz point
     */
bool CTFW::transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const
{
	obj.z = Height;

	if (this->nActualStationPars == 6 )
	{
		obj.x = this->a * obs.x + this->b * obs.y + this->c;
		obj.y = this->d * obs.x + this->e * obs.y + this->f;
	}
	else if (this->nActualStationPars == 4 )
	{
		obj.x = this->a * obs.x - this->b * obs.y + this->c;
		obj.y = this->b * obs.x + this->a * obs.y + this->d;
	}
	return true;
}

bool CTFW::transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const
{
	if (this->nActualStationPars == 6 )
	{
		obj.x = this->a * obs.x + this->b * obs.y + this->c;
		obj.y = this->d * obs.x + this->e * obs.y + this->f;
		obj.z = 0.0f;
	}
	else if (this->nActualStationPars == 4 )
	{
		obj.x = this->a * obs.x - this->b * obs.y + this->c;
		obj.y = this->b * obs.x + this->a * obs.y + this->d;
		obj.z = 0.0f;
	}
	return true;
}

    
void CTFW::transformObj2Obs(CObsPoint  &obs, const CXYZPoint &obj) const
{	
	obs.setLabel(obj.getLabel());
	
	Matrix A(obs.getDimension(), obj.getDimension());

	double det = this->a*this->e - this->b*this->d;
	if (fabs(det) > DBL_EPSILON)
	{
		double xred = obj.x - this->c;
		double yred = obj.y - this->f;

		det = 1.0 / det;

		double ainv =  this->e * det;
		double binv = -this->b * det;
		double dinv = -this->d * det;
		double einv =  this->a * det;
		A.element(0,0) = ainv; 
		A.element(1,0) = binv;
		A.element(0,1) = dinv;
		A.element(1,1) = einv;
		A.element(0,2) = 0.0;
		A.element(1,2) = 0.0;

		if (obs.getDimension() > 2)
		{
			A.element(2,0) = 0.0;
			A.element(2,1) = 0.0;
			A.element(2,2) = 1.0;
			obs[2] = 0;
		}

		obs[0] = ainv * xred + binv * yred;
		obs[1] = dinv * xred + einv * yred;
		*obs.getCovariance() = A * obj.getCovar() * A.t();
	}
};

bool CTFW::transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const
{
	obj.setLabel(obs.getLabel());
	Matrix A(obs.getDimension(), obs.getDimension());

	A.element(0,0) = a; 
	A.element(0,1) = b;
	A.element(1,0) = d;
	A.element(1,1) = e;

	if (obs.getDimension() > 2)
	{
		A.element(2,0) = 0.0;
		A.element(0,2) = 0.0;
		A.element(2,1) = 0.0;
		A.element(1,2) = 0.0;
		A.element(2,2) = 1.0;
	}

	obj[0] = this->a * obs[0] + this->b * obs[1] + this->c;
	obj[1] = this->d * obs[0] + this->e * obs[1] + this->f;
	obj[2] = Height;

	Matrix covar = A * obs.getCovar() * A.t();

	Matrix *objCov = obj.getCovariance();

	for (int i = 0; i < covar.Nrows(); ++i)
	{
		for (int j = 0; j < covar.Ncols(); ++j)
		{
			objCov->element(i,j) = covar.element(i,j);
		}
	}

	objCov->element(2,2) = sigmaZ * sigmaZ;
	return true;
};

bool CTFW::transformObs2Obj(CObsPoint &obj, const CObsPoint &obs) const
{
	obj.setLabel(obs.getLabel());
	Matrix A(obs.getDimension(), obs.getDimension());

	A.element(0,0) = a; 
	A.element(0,1) = b;
	A.element(1,0) = d;
	A.element(1,1) = e;

	if (obs.getDimension() > 2)
	{
		A.element(2,0) = 0.0;
		A.element(0,2) = 0.0;
		A.element(2,1) = 0.0;
		A.element(1,2) = 0.0;
		A.element(2,2) = 1.0;
		obj[2] = obs[2];
	}
	else obj[2] = 0.0;


	obj[0] = this->a * obs[0] + this->b * obs[1] + this->c;
	obj[1] = this->d * obs[0] + this->e * obs[1] + this->f;
	
	
	Matrix covar = A * obs.getCovar() * A.t();

	Matrix *objCov = obj.getCovariance();

	for (int i = 0; i < covar.Nrows(); ++i)
	{
		for (int j = 0; j < covar.Ncols(); ++j)
		{
			objCov->element(i,j) = covar.element(i,j);
		}
	}
	
	return true;
};

//=============================================================================

bool CTFW::transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const
{
	if (this->nActualStationPars == 6 )
	{
		obj.x = this->a * obs.x + this->b * obs.y + this->c;
		obj.y = this->d * obs.x + this->e * obs.y + this->f;
	}
	else if (this->nActualStationPars == 4 )
	{
		obj.x = this->a * obs.x - this->b * obs.y + this->c;
		obj.y = this->b * obs.x + this->a * obs.y + this->d;
	}

	obj.z = plane.getHeight(C2DPoint(obj.x, obj.y));

	return true;
};

//=============================================================================

bool CTFW::transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const
{
	this->transformObs2Obj((C3DPoint &) obj, (C2DPoint &) obs, (C3DPlane &) plane);

	CXYZPoint p1(obj); 
	p1.z += 1.0;

    CXYZHomoSegment seg(obj, p1);
		 
	eIntersect res = seg.getIntersection(plane, obj);

	return res == eIntersection;
};

//=============================================================================

void CTFW::setAdjustableValues(const Matrix &parms)
{
    this->a = parms.element(0, 0);
    this->b = parms.element(1, 0);
    this->c = parms.element(2, 0);
    this->d = parms.element(3, 0);
    this->e = parms.element(4, 0);
    this->f = parms.element(5, 0);

    this->informListeners_elementsChanged();

    this->hasValues = true;
}

//=============================================================================

/**
* Gets a matrix of the parameters
*
* @return the parameters backed into a matrix
*/
bool CTFW::getAllParameters(Matrix &parms) const
{
    parms.ReSize( 6, 1);

    parms.element(0, 0) = this->a;
    parms.element(1, 0) = this->b;
    parms.element(2, 0) = this->c;
    parms.element(3, 0) = this->d;
    parms.element(4, 0) = this->e;
    parms.element(5, 0) = this->f;

    return true;
}

bool CTFW::insertParametersForAdjustment(Matrix &parms)
{
	if (this->parameterIndex[addPar] >= 0)
	{
		parms.element(this->parameterIndex[addPar]    , 0) = this->a;
		parms.element(this->parameterIndex[addPar] + 1, 0) = this->b;
		parms.element(this->parameterIndex[addPar] + 2, 0) = this->c;
		parms.element(this->parameterIndex[addPar] + 3, 0) = this->d;
		parms.element(this->parameterIndex[addPar] + 4, 0) = this->e;
		parms.element(this->parameterIndex[addPar] + 5, 0) = this->f;
	}
	return true;
};


bool CTFW::setParametersFromAdjustment(const Matrix &parms)
{
	if (this->parameterIndex[addPar] >= 0)
	{
		this->a = parms.element(this->parameterIndex[addPar]    , 0);
		this->b = parms.element(this->parameterIndex[addPar] + 1, 0);
		this->c = parms.element(this->parameterIndex[addPar] + 2, 0);
		this->d = parms.element(this->parameterIndex[addPar] + 3, 0);
		this->e = parms.element(this->parameterIndex[addPar] + 4, 0);
		this->f = parms.element(this->parameterIndex[addPar] + 5, 0);
	}

	return true;
}

Matrix CTFW::getConstants() const
{
	Matrix parms(0,0);
	return parms;
};

/**
 * resetTFW  sets tfw
 * parameters to 0
 */
void CTFW::resetSensor()
{
    this->a = 0;    
    this->b = 0;    
    this->c = 0;    
    this->d = 0;    
    this->e = 0;    
    this->f = 0;

	this->hasValues = false;
}


/**
 * reads tfw parameter file (text format and sets tfw values 
 * @param  char data
 * @returns true/false
 */
bool CTFW::read(const char* filename)
{  
    CCharString str;

    CTextFile file(filename, CTextFile::READ);

	file.ReadLine(str);
	const char* string = str.GetChar();
	sscanf(string, "%lf", &this->a);

	file.ReadLine(str);
	string = str.GetChar();
	sscanf(string, "%lf", &this->d);

	file.ReadLine(str);
	string = str.GetChar();
	sscanf(string, "%lf", &this->b);

	file.ReadLine(str);
	string = str.GetChar();
	sscanf(string, "%lf", &this->e);

	file.ReadLine(str);
	string = str.GetChar();
	sscanf(string, "%lf", &this->c);

	file.ReadLine(str);
	string = str.GetChar();
	sscanf(string, "%lf", &this->f);

	this->sethasValues(true);

	this->nActualStationPars = 6;

    this->FileName = filename;

    return true;
}



/**
 * writes tfw parameter file as space separated file 
 * @param  filename
 * @returns true/false
 */
bool CTFW::writeTextFile(const char* filename) const
{
    FILE	*fp;
	fp = fopen(filename,"w");


	if ( this->getRefSys().getCoordinateType() == eGEOGRAPHIC )
	{
		::fprintf(fp,"%.10f\n", a);
		::fprintf(fp,"%.10f\n", d);
		::fprintf(fp,"%.10f\n", b);
		::fprintf(fp,"%.10f\n", e);
		::fprintf(fp,"%.10f\n", c);
		::fprintf(fp,"%.10f\n", f);
	}
	else
	{
		::fprintf(fp,"%.6f\n", a);
		::fprintf(fp,"%.6f\n", d);
		::fprintf(fp,"%.6f\n", b);
		::fprintf(fp,"%.6f\n", e);
		::fprintf(fp,"%.6f\n", c);
		::fprintf(fp,"%.6f\n", f);
	}


	fclose(fp);

	return true;
}

/**
 * Serialized storage of a CTFW to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CTFW::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("tfwfilename", this->getFileName()->GetChar());

    token = pStructuredFileSection->addToken();
    token->setValue("hasValues", this->gethasValues());

    token = pStructuredFileSection->addToken();
	token->setValue("Parametercount", this->nActualStationPars);

    token = pStructuredFileSection->addToken();
    token->setValue("a", this->a);

    token = pStructuredFileSection->addToken();
    token->setValue("b", this->b);

    token = pStructuredFileSection->addToken();
    token->setValue("c", this->c);

    token = pStructuredFileSection->addToken();
    token->setValue("d", this->d);
 
    token = pStructuredFileSection->addToken();
    token->setValue("e", this->e);

    token = pStructuredFileSection->addToken();
    token->setValue("f", this->f);

    CStructuredFileSection* child = pStructuredFileSection->addChild();
    this->refSystem.serializeStore(child);
}

/**
 * Restores a CTFW from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CTFW::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("tfwfilename"))
            this->setFileName( token->getValue());
		else if(key.CompareNoCase("hasValues"))
            this->hasValues = token->getBoolValue();
		else if(key.CompareNoCase("Parametercount"))
			this->setParameterCount(addPar, token->getIntValue());
		else if(key.CompareNoCase("a"))
            this->a = token->getDoubleValue();
		else if(key.CompareNoCase("b"))
            this->b = token->getDoubleValue();
		else if(key.CompareNoCase("c"))
            this->c = token->getDoubleValue();
		else if(key.CompareNoCase("d"))
            this->d = token->getDoubleValue();
        else if(key.CompareNoCase("e"))
            this->e = token->getDoubleValue();     
		else if(key.CompareNoCase("f"))
            this->f = token->getDoubleValue();
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CReferenceSystem"))
        {
            this->refSystem.serializeRestore(child);
        }
    }

}

/**
 * Gets ClassName
 * returns a CCharString
 */
string CTFW::getClassName() const
{
    return string( "CTFW");
}

bool CTFW::isa(string& className) const
{
    if (className == "CTFW")
        return true;

    return false;
}

bool CTFW::Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		      Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
			  Matrix &AiPar,        Matrix &AiPoint, 
			  const Matrix &obs, const C3DPoint &approx) const
{   
    return true;
}

bool CTFW::AiPars(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
				  Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
				  Matrix &AiPar,        const Matrix &obs, const C3DPoint &approx) const
{   
    return true;
}

bool CTFW::AiPoint(Matrix& Ai, const Matrix &obs, const C3DPoint &approx) const
{   
    return true;
}

bool CTFW::AiBiFi(Matrix &Fi, Matrix &Bi, 
				  Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
				  Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
				  Matrix &AiPar,        Matrix &AiPoint, 
				  const Matrix &obs, const C3DPoint &approx)
{   
    return true;
}

bool CTFW::Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const
{
    return true;
}

bool CTFW::Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const
{
    return true;
}

void CTFW::getName(CCharString& name) const
{
    name = "TFW Bundle";
}


void CTFW::getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const
{
    StationNames.RemoveAll();
	PointNames.RemoveAll();

	CCharString* str;
    
	str=StationNames.Add();
	*str = "A0";

	str=StationNames.Add();
	*str = "A1";
        
	str=StationNames.Add();
	*str = "A2";

	str=StationNames.Add();
	*str = "B0";

	str=StationNames.Add();
	*str = "B1";

	str=PointNames.Add();
	*str = "X";
	str=PointNames.Add();
	*str = "Y";
	str=StationNames.Add();
	*str = "B2";
}

void CTFW::addDirectObservations(Matrix &N, Matrix &t)
{
};


bool CTFW::setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const
{
	return true;
};
	 
bool CTFW::setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const
{
	return true;
};
	
int CTFW::accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
					 CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
					 CRobustWeightFunction *robust, bool omitMarked)
{
	return 0;
};

void CTFW::crop(int offsetX, int offsetY, int newWidth, int newHeight)
{
	CXYPoint UL((int)offsetX, (int)offsetY);
	CXYZPoint metricUL;

	this->transformObs2Obj(metricUL, UL);
	double newC = metricUL.x;
	double newF = metricUL.y;

	// reduce for UL
	this->setC(newC);
	this->setF(newF);
	this->setFileName("CropTFW");
};