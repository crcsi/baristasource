#include "CameraPtrArray.h"

CCameraPtrArray::CCameraPtrArray( int nGrowBy):
	CMUObjectPtrArray<CCamera>(nGrowBy)
{
}

	CCameraPtrArray::CCameraPtrArray(const CCameraPtrArray & src):
	CMUObjectPtrArray<CCamera>(src.GetGrowBy())
{
}


CCameraPtrArray::~CCameraPtrArray(void)
{
}
