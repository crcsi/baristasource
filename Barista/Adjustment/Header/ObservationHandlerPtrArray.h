#ifndef __CObservationHandlerPtrArray__
#define __CObservationHandlerPtrArray__

#include "MUObjectPtrArray.h"
#include "ObservationHandler.h"

class CObservationHandlerPtrArray : public CMUObjectPtrArray<CObservationHandler>
{
  public:
    CObservationHandlerPtrArray(int nGrowBy = 0);
    ~CObservationHandlerPtrArray(void);

};

#endif

