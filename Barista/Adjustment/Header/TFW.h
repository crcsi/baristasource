#ifndef __CTFW__
#define __CTFW__

#include "SensorModel.h"
#include "2DPoint.h"

class CXYPoint;
class CXYZPoint;
class CXYPointArray;
class CXYZPointArray;

class CTFW : public CSensorModel
{
  protected:
	  
	  double a, b, c, d, e, f;
	
  public:
    CTFW(void);
    CTFW(const CTFW &tfw);
    CTFW(const Matrix &parms);
    ~CTFW();

	void copy(const CTFW &tfw);

	double getA() const { return a; };
	double getB() const { return b; };
	double getC() const { return c; };
	double getD() const { return d; };
	double getE() const { return e; };
	double getF() const { return f; };

	void setA(double newa)  { a = newa; };
	void setB(double newb)  { b = newb; };
	void setC(double newc)  { c = newc; };
	void setD(double newd)  { d = newd; };
	void setE(double newe)  { e = newe; };
	void setF(double newf)  { f = newf; };

	C2DPoint getShift() const { return C2DPoint(this->c, this->f); }
	C2DPoint getGridding() const { return C2DPoint(this->a, - this->e); }

	double getScaleX() const;
	double getScaleY() const;

	void setFromShiftAndPixelSize(const C2DPoint &shift, const C2DPoint &pixelsize);
	void setShift(const C2DPoint &shift);

    virtual void setAll(const Matrix &parms);

	// invert tfw transformation: *this = *this^-1
	bool invert();

	// concatenate *this and tfw; order: *this = *this(tfw)
	void concatenate(const CTFW &tfw);

	bool operator == (const CTFW &tfw) const;
	bool operator != (const CTFW &tfw) const { return !(*this == tfw); };

	CTFW &operator = (const CTFW &tfw);

	void reset();

    bool isa(string& className) const;
    string getClassName() const;

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    virtual void transformObj2Obs(C2DPoint &obs, const C3DPoint &obj) const;
	virtual void transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const;
    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const;
    virtual bool transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const;

    virtual void transformObj2Obs(CObsPoint  &obs, const CXYZPoint &obj) const;
    virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const;
    virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint &obs) const;

	virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const;

    virtual bool transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const;

    bool read(const char *filename);
    bool writeTextFile(const char* filename) const;


    /**
     * Gets a matrix of the parameters
     *
     * @return the parameters backed into a matrix
     */
    virtual bool getAllParameters(Matrix &parms) const;

    virtual Matrix getConstants() const;

    virtual void setAdjustableValues(const Matrix &parms);

	virtual void resetSensor();

    virtual bool getAdjustableParameters(Matrix &parms) const
	{
		return getAllParameters(parms);
	};

	virtual void crop(int offsetX, int offsetY, int newWidth, int newHeight);

    virtual bool insertParametersForAdjustment(Matrix &parms);

    virtual bool setParametersFromAdjustment(const Matrix &parms);

	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const;
	
	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const;
 
	virtual bool Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const;

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return A Matrix of partials with respect to paramters evaluated at ob
     */
    virtual bool Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					Matrix &AiPar,        Matrix &AiPoint, 
					const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPars (Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
  						 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiBiFi(Matrix &Fi, Matrix &Bi, 
		                Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
						Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiPar,        Matrix &AiPoint, 
						const Matrix &obs, const C3DPoint &approx);

	virtual int accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
						   CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
						   CRobustWeightFunction *robust, bool omitMarked);

	virtual void addDirectObservations(Matrix &N, Matrix &t);

    /**
     * @return Bi Matrix of patials with respect to observations for the ith observation
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     */
    virtual bool Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const;

    /**
     * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
     */
     virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const;

     /**
      * @return a String cantainint the name of the best fit ex. "Best fit Line"
      */
     virtual void getName(CCharString& names) const;  

};


#endif