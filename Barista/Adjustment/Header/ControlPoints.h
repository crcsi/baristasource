#ifndef __CControlPointModel__
#define __CControlPointModel__

#include "SensorModel.h"
#include "XYZPoint.h"

class CControlPointModel : public CSensorModel
{

  public:

    CControlPointModel();
    virtual ~CControlPointModel();

    CControlPointModel(const CControlPointModel &cp);

	void copy(const CControlPointModel &cp);


	virtual void setAll(const Matrix &parms);

    virtual bool read(const char* filename) ;

    virtual bool writeTextFile(const char* filename) const;

	virtual void setAdjustableValues(const Matrix &parms);

	string getClassName() const
	{
		return string ("CControlPointModel");
	};

	bool isa(string& className) const
	{
		if (className == "CControlPointModel")
			return true;

		return false;
	};

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

  	virtual void crop(int offsetX, int offsetY, int newWidth, int newHeight);

    /**
     * Transforms an xyz point to its cooresponding xy point
     * in image space
     * The xyz point is assumed to be Lat Long Height expressed in the
     * appropriate zone for the RPC.
     *
     * @param xyz The xyz point
     * @return the transformed xy point
     */
    virtual void transformObj2Obs(C2DPoint &obs, const C3DPoint &obj) const;

    virtual void transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const;

    virtual bool transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const;

	virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const;

    virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const;

    /**
     * Gets a matrix of the parameters (all 99)
     *
     * @return the parameters backed into a matrix
     */
    virtual bool getAllParameters(Matrix &parms) const;

    virtual bool getAdjustableParameters(Matrix &parms) const;

    virtual bool insertParametersForAdjustment(Matrix &parms);

    virtual bool setParametersFromAdjustment(const Matrix &parms);

    /**
     * Gets a matrix of the rpc parameters without aps
     *
     */
    virtual Matrix getConstants() const;

	virtual int accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, CObsPoint &discrepancy, 
		                   CObsPoint &normalisedDiscrepancy, CRobustWeightFunction *robust, bool omitMarked);

	virtual bool Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const;
    virtual bool Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const;

	virtual bool Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					Matrix &AiPar,        Matrix &AiPoint, 
					const Matrix &obs, const C3DPoint &approx) const;

	virtual bool AiPars (Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const;
    virtual bool AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiBiFi(Matrix &Fi, Matrix &Bi, 
						Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiPar,        Matrix &AiPoint,
						const Matrix &obs, const C3DPoint &approx);

	virtual int coordPrecision() const;

	virtual CCharString obsUnitsString() const;

	virtual CCharString diffUnitsString() const;
	 
	virtual void convertToDifferentialUnits(CObsPoint &out, const CObsPoint &in) const;

	virtual void getObservationNames(CCharStringArray &names) const;

	virtual void addDirectObservations(Matrix &N, Matrix &t);

    virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const;

    virtual void getName(CCharString& name) const;
 
	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const;
	
	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const;
 
};


#endif

