#ifndef __COrbitPathModel__
#define __COrbitPathModel__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */



#include "SplineModel.h"
#include "XYZPoint.h"
#include "RollPitchYawRotation.h"

class COrbitPathModel : public CSplineModel
{
protected:
	CXYZPoint shift;
	CXYZPoint shiftObservation;
	CXYZPoint angleObservation;
	CRollPitchYawRotation rotParameter;

	double satH; // satellite height from centre of earth in m!!
	double sigmaSatH; // [m]

	doubles times; // the times at which the scalar product will introduced as direct observation
	double sigmaInnerProduct;  // sigma of inner product

public:
	COrbitPathModel(void);
	COrbitPathModel(const COrbitPathModel& model);
	virtual ~COrbitPathModel(void);

	COrbitPathModel &operator = (const COrbitPathModel & orbitPath);

	virtual void getName(CCharString& names) const;

	virtual void extend_N_and_t_Matrix(Matrix& N,Matrix& t,
											const CAdjustMatrix& AxTP,
											const CAdjustMatrix& AyTP, 
											const CAdjustMatrix& AzTP,
											const CAdjustMatrix& P,
											const CAdjustMatrix& dl,
											const int indexNx,
											const int indexNy,
											const int indexNz,
											const CObsPoint &obs)const;

	virtual void extend_Ai(	Matrix &AiErp,Matrix &AiRot,Matrix &AiSca, 
							Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
							Matrix &AiAdd, Matrix &AiPoint,const Matrix &obs, const C3DPoint &approx)const;

	virtual void extend_Bi(	Matrix& Fi,const Matrix &obs, const C3DPoint &approx)const;
	virtual bool Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const;

	virtual bool Fi(CObsPoint& fi, const CObsPoint &obs) const;

	virtual void computePMatrix(const Matrix& Qobs, CAdjustMatrix& P)const;

	virtual int getDirectObsCount() const;
	virtual void addDirectObservations(Matrix &N, Matrix &t);
	virtual double getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res);

	void setDirectObservationForShift(const CXYZPoint& obs); 
	CXYZPoint getDirectObservationForShift() const {return this->shiftObservation;};

	void initDirectObservationForLinearSplineParameter(double sigma);
	void initDirectObservationForQuadraticSplineParameter(double sigma);

	bool backProjection(double &time,const C3DPoint& objPoint,const C3DPoint &n);

    virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
    virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual void reconnectPointers();
	virtual void resetPointers();
	virtual bool isa(string& className) const
	{
		if (className == "COrbitPathModel")
			return true;
		return false;
	};

    virtual string getClassName() const {return string("COrbitPathModel"); };

	virtual bool getAllParameters(Matrix &parms) const;
	virtual bool getAdjustableParameters(Matrix &parms) const;
	virtual bool insertParametersForAdjustment(Matrix &parms);
	virtual bool setParametersFromAdjustment(const Matrix &parms);
	virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const;

	CXYZPoint getShift() const { return this->shift;};
	void setShift(CXYZPoint& shift);

	CXYZPoint getRotation() const 
	{	CXYZPoint tmp(this->rotParameter[0],this->rotParameter[1],this->rotParameter[2]);
		tmp.setCovariance(*this->rotParameter.getCovariance());
		return tmp;}; 

	void setRotation(const CXYZPoint& rot);

	void setDirectObservationForAngles(CXYZPoint& obs);
	CXYZPoint getDirectObservationForAngles() const {return this->angleObservation;};

	virtual CCharString obsUnitsString() const { return "[m]"; };	 
	virtual CCharString diffUnitsString() const { return "[m]"; };
	virtual int coordPrecision() const;
	virtual void getObservationNames(CCharStringArray &names) const;

	virtual void setCovariance(const Matrix &covar);

	virtual void printStationParameter(CCharString& output,CRotationMatrix* rotMat=NULL)const;

	virtual void initDirectObservations();

	void setSatelliteHeight(double satH,double sigmaSatH);

	void setTimesForDirectObservations(doubles times,double sigmaInnerProduct);

};
#endif