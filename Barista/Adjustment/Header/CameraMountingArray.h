#ifndef __CCameraMountingArray__
#define __CCameraMountingArray__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "CameraMounting.h"
#include "CameraMountingPtrArray.h"
#include "MUObjectArray.h"
#include "Serializable.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
#include "Filename.h"

class CCameraMountingArray : public CMUObjectArray<CCameraMounting, CCameraMountingPtrArray>, public CSerializable
{
public:
	CCameraMountingArray(void);
	CCameraMountingArray(const CCameraMountingArray& src);

	~CCameraMountingArray(void);

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();

	virtual CCameraMounting* Add();
	virtual CCameraMounting* Add(const CCameraMounting& camera);

	CCameraMounting* getCameraMountingByName(CCharString* name) const;

	virtual bool isa(string& className) const
	{
		if (className == "CCameraMountingArray")
			return true;

		return false;
	};

	virtual string getClassName() const {return string("CCameraMountingArray");};

};
#endif