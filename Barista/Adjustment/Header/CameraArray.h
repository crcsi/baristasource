#ifndef __CCameraArray__
#define __CCameraArray__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "Camera.h"
#include "CameraPtrArray.h"
#include "MUObjectArray.h"
#include "Serializable.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
#include "Filename.h"


class CCameraArray : public CMUObjectArray<CCamera, CCameraPtrArray>, public CSerializable
{
private: 
	virtual CCamera* Add();
	CameraType nextCameraType;

public:
	CCameraArray(void);
	CCameraArray(const CCameraArray& src);

	~CCameraArray(void);


	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();


	virtual CCamera* Add(CameraType type);
	virtual CCamera* Add(const CCamera& camera);
	virtual CCamera* CreateType() const;
	virtual CCamera* CreateType(const CCamera& camera)const;

	CCamera* getCamerabyName(CCharString* name) const;
	virtual bool isa(string& className) const
	{
		if (className == "CCameraArray")
			return true;

		return false;
	};

	virtual string getClassName() const {return string("CCameraArray");};
};

#endif