#ifndef __CPushBroomScanner__
#define __CPushBroomScanner__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "SensorModel.h"

#include "OrbitPathModel.h"
#include "OrbitAttitudeModel.h"
#include "CCDLine.h"
#include "CameraMounting.h"
#include "XYZPoint.h"
#include "RollPitchYawRotation.h"


#define EARTHRADIUS 6371000 // m
#define LAPSRATE 0.0065
#define POLYTROPIC_INDEX 4.123
#define DENSITY_SCALE_HEIGHT 8591.7
#define ZERO_DEGREE_IN_KELVIN 273.115 //K
#define SPEED_OF_LIGHT 299792458.0 //m/s

class CXYPoint;
class CXYPointArray;
class CXYZPointArray;
class CRPC;


class CRefraction;


/*!
 * \brief
 * Sensor model for pushbroom scanners. 
 * 
 * Write detailed description for CPushBroomScanner here.
 * 
 * \remarks
 * Write remarks for CPushBroomScanner here.
 * 
 * \see
 * COrbitPathModel | COrbitAttitudeModel | CCCDLine | CCameraMounting | CSensorModel
 */
class CPushBroomScanner : public CSensorModel
{
  protected:
	
	COrbitPathModel*	path;
	COrbitAttitudeModel*	attitudes;
	  
	CCCDLine* camera;
	CCameraMounting *cameraMounting;	  

	CRotationMatrix fixedECRMatrix;
	double timeFixedECRMatrix;

	C3DPoint normalVectorCCDLine;
	C3DPoint refractionCorrection;
	C3DPoint aberrationCorrection;

	double satelliteHeight;
	double groundHeight;
	double groundTemperature;

	double firstLineTime;
	double timePerLine;

	//! a flag that indicates whether the mounting calibration has to be changed when this pushbroom scanner is merged with another one
	bool changeMountingForMerging;

protected: 
	void initCamera(CCCDLine* camera);
	void initMounting(CCameraMounting* cameraMounting);
	void initMounting(CCameraMounting *cameraMounting,double crossTrackViewAngle,double inTrackViewAngle);
	void initOrbitPathModel(CSensorModel* path);
	void initOrbitPathModel( COrbitPathModel* path,
							 double firstLineTime,
							 double lastLineTime,
							 double inTrackViewAngle,
							 double crossTrackViewAngle,
							 double satH,
							 C3DPoint UL,
							 C3DPoint UR,
							 C3DPoint LL,
							 C3DPoint LR,
							 double &minTime,
							 double &maxTime);


	void initOrbitAttitudeModel(CSensorModel* attitudes);
	void initOrbitAttitudeModel(COrbitAttitudeModel* attitudes,double firstLineTime,double lastLineTime);
	
	bool computeRefractionCorrection(const C3DPoint& imagePos);
	bool computeAberrationCorrection(const C3DPoint& imagePos);

	bool compute3DImageVector(C3DPoint& imageVector,const double& time, const C3DPoint& objPoint,CRotationBase* Rrpy) const;
	void applyCorrections(C2DPoint& obs) const;

  public:
    CPushBroomScanner(void);
    CPushBroomScanner(const CPushBroomScanner &scanner);
    CPushBroomScanner(const Matrix &parms);
    ~CPushBroomScanner();

	bool init(CSensorModel* path,
			CSensorModel* attitudes,
			CCameraMounting* cameraMounting,
			CCCDLine* camera,
			const CFileName& filename,
			double satelliteHeight,
			double groundHeight,
			double groundTemperature,
			double firstLineTime,
			double timePerLine
			);

	bool init(CSensorModel* path,
			 CSensorModel* attitudes,
			 CCameraMounting * cameraMounting,
			 CCCDLine* camera,
			 const CFileName& filename,
			 double satelliteHeight,
			 double groundHeight,
			 double groundTemperature,
			 double timePerLinedouble,
			 double inTrackViewAngle,
			 double crossTrackViewAngle,
			 C3DPoint UL,
			 C3DPoint UR,
			 C3DPoint LL,
			 C3DPoint LR);

	bool getChangeMountingForMerging() const { return this->changeMountingForMerging; };

	COrbitPathModel* getOrbitPath() {return this->path;};
	COrbitAttitudeModel* getOrbitAttitudes() {return this->attitudes;};
	CCCDLine* getCamera() {return this->camera;};

	virtual CCamera *getGenericCamera() const { return this->camera; };

	CCameraMounting* getCameraMounting() {return this->cameraMounting;};

	CPushBroomScanner &operator = (const CPushBroomScanner & scanner);

	const CRotationMatrix& getFixedMatrix();

	void goingToBeDeleted();

	void setChangeMountingForMerging(bool changeForMerging);

	void updateFirstLineTime(double newFirstLineTime);

	double getTime(const double& row) const {return this->firstLineTime + row * this->timePerLine;};

	double getLine(const double& time) const {return (time - this->firstLineTime)/this->timePerLine;};

	double getFirstLineTime() const {return this->firstLineTime;};

	double getTimePerLine() const {return this->timePerLine;};


	double getSatelliteHeight() const{ return this->satelliteHeight;};
	
	double getGroundHeight() const {return this->groundHeight;};

	double getGroundTemperature() const {return this->groundTemperature;};

	double getOffNadirAngle(int row) const;

	virtual void resetSensor();

	virtual void computeParameterCount();

	void copy(const CPushBroomScanner &scanner);

    virtual void setAll(const Matrix &parms);

    bool isa(string& className) const;

    string getClassName() const;

    // CSerializable functions
    virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual void reconnectPointers();
	virtual void resetPointers();

	//! Generate RPCs from pushbroom sensor model.
	//! width:      width of the image (i.e., number of columns) [pixels]
	//! height:     height of the image (i.e., number of rows) [pixels]
	//! Zmin, Zmax: minimum/maximum elevation in the scene
	//! rpc: the resulting RPC parameters will be stored here.
	//! return value: sigma0 of the adjustment
	double generateRPC(CRPC &rpc, int width, int height, float Zmin, float Zmax) const;

    virtual void transformObj2Obs(C2DPoint &obs, const C3DPoint &obj) const;
	virtual void transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const;
    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const;
    virtual bool transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const;
	virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const;

    virtual bool transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const;

	virtual void crop(int offsetX, int offsetY, int newWidth, int newHeight);

	virtual bool getVerticalPole(CXYZPoint      &objGround, CXYZPoint      &objTop, 
						         const CXYPoint &obsGround, const CXYPoint &obsTop, 
						         double heightGround, float sigmaZ) const;

    bool read(const char *filename);

    bool writeTextFile(const char* filename) const;

	virtual bool getAllParameters(Matrix &parms) const;

    virtual Matrix getConstants() const;

    virtual void setAdjustableValues(const Matrix &parms);

    virtual bool getAdjustableParameters(Matrix &parms) const;
 
	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const;
	
	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const;

 	virtual int accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
						   CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
						   CRobustWeightFunction *robust, bool omitMarked);

	virtual bool Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const;

	CObsPoint calcRes(const Matrix &Delta, const CObsPoint &obs, const CObsPoint &obj);

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return A Matrix of partials with respect to paramters evaluated at ob
     */
    virtual bool Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					Matrix &AiPar,        Matrix &AiPoint, 
					const Matrix &obs, const C3DPoint &approx) const;
    
	virtual bool AiPars (Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const;
    virtual bool AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiBiFi(Matrix &Fi, Matrix &Bi, 
						Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiPar,        Matrix &AiPoint,
						const Matrix &obs, const C3DPoint &approx);


    /**
     * @return Bi Matrix of patials with respect to observations for the ith observation
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     */
    virtual bool Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const;

    /**
     * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
     */
     virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const;

     /**
      * @return a String cantainint the name of the best fit ex. "Best fit Line"
      */
     virtual void getName(CCharString& names) const;  
   
	 virtual bool insertParametersForAdjustment(Matrix &parms);

	 virtual bool setParametersFromAdjustment(const Matrix &parms);

	virtual void printStationParameter(CCharString& output,CRotationMatrix* rotMat=NULL)const;


	virtual CCharString obsUnitsString() const { return "[pixel]"; };	 
	virtual CCharString diffUnitsString() const { return "[pixel]"; };
	virtual int coordPrecision() const;
	virtual void getObservationNames(CCharStringArray &names) const;

	virtual void setCovariance(const Matrix &covar);

	virtual int getDirectObsCount() const;

	virtual void addDirectObservations(Matrix &N, Matrix &t);
	virtual double getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res);


};


class CRefraction
{
public:
	CRefraction() : gamma(POLYTROPIC_INDEX),lapsRate(LAPSRATE),earthRadius(EARTHRADIUS),W(DENSITY_SCALE_HEIGHT),temp(ZERO_DEGREE_IN_KELVIN){};

	double getRefractionAngle(const double viewAngle, const double heightOfSattelite,const double heightOfArea,const double tempAtSealevel) const;

protected:
	const double gamma;
	const double lapsRate;
	const double earthRadius;
	const double W;
	const double temp;
};

#endif