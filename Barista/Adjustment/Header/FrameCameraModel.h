#ifndef __CFrameCameraModel__
#define __CFrameCameraModel__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "SensorModel.h"
#include "FrameCamera.h"
#include "CameraMounting.h"
#include "XYZPoint.h"
#include "RotationBase.h"

class CFrameCameraModel :	public CSensorModel
{
protected:
	CFrameCamera* camera;
	CCameraMounting* cameraMounting;
	CXYZPoint cameraPos;
	CRotationBase* cameraRotation;

	CTrans2D* cameraToFrame;
	CTrans2D* frameToCamera;

public:
	CFrameCameraModel(void);
	CFrameCameraModel(const CFrameCameraModel& sensor);

	virtual ~CFrameCameraModel(void);

	bool init(const CFileName& filename,
			  const CXYZPoint &newCameraPos, 
			  CRotationBase* newCameraRotation,
			  CCameraMounting* newCameraMounting,
			  CFrameCamera* newCamera);

	CFrameCameraModel &operator = (const CFrameCameraModel & framecamera);

	CFrameCamera* getCamera() {return this->camera;};
	virtual CCamera *getGenericCamera() const { return this->camera; };

	CCameraMounting* getCameraMounting() {return this->cameraMounting;};
	CXYZPoint getCameraPosition() const {return this->cameraPos;};
	CRotationBase* getCameraRotation() {return this->cameraRotation;};
	
	void setCameraPosition(const CXYZPoint &p0);
	void setCameraRotation(CRotationBase *rot);
	
	virtual CCharString getDescriptorString() const;

	virtual Matrix getProjectionMatrix(const C2DPoint &imageOffset) const;
	virtual Matrix getProjectionMatrix() const
	{ return getProjectionMatrix(C2DPoint(0.0,0.0));	};

	void setFiducialMarkTransformation(CTrans2D* cameraToFrame,CTrans2D* frameToCamera);

	const CXYPointArray* getFiducialMarks() const {return this->camera ? this->camera->getFiducialMarks() : NULL;};
	
	virtual bool isa(string& className) const
	{
	  if (className == "CFrameCameraModel")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CFrameCameraModel"); };  

	virtual CObsPoint calcRes(const Matrix &Delta, const CObsPoint &obs, const CObsPoint &obj);

	virtual void crop(int offsetX, int offsetY, int newWidth, int newHeight);

    virtual void setAdjustableValues(const Matrix &parms);

	virtual void setAll(const Matrix &parms);

    virtual bool read(const char *filename);

	virtual bool writeTextFile(const char* filename) const;

    // CSerializable functions
    virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
    virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    virtual void reconnectPointers();
    virtual void resetPointers();

    virtual bool isModified();

    virtual void transformObj2Obs(C2DPoint  &obs, const C3DPoint &obj) const;

	virtual void transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, double Height) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const;

    virtual bool transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const;

    virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const;

	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const;

	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const;

    virtual bool getAllParameters(Matrix &parms) const;

    virtual bool getAdjustableParameters(Matrix &parms) const;

    virtual bool insertParametersForAdjustment(Matrix &parms);

    virtual bool setParametersFromAdjustment(const Matrix &parms);
    
	virtual Matrix getConstants() const;

	virtual int accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
						   CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
						   CRobustWeightFunction *robust, bool omitMarked);

    virtual bool Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const;


    virtual bool Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					Matrix &AiPar,        Matrix &AiPoint, 
					const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPars (Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const;


    virtual bool AiBiFi(Matrix &Fi, Matrix &Bi, 
						Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiPar,        Matrix &AiPoint,
						const Matrix &obs, const C3DPoint &approx);

    virtual bool Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const ;

     virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const;

	 
	 virtual void addDirectObservations(Matrix &N, Matrix &t);

     virtual void getName(CCharString& names) const;  

	virtual int coordPrecision() const;

	virtual void getObservationNames(CCharStringArray &names) const;

	virtual void resetSensor();

};

#endif