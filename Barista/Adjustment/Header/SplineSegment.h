#ifndef __CSplineSegment__
#define __CSplineSegment__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Serializable.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"
#include "Polynomial.h"

class C3DPoint;
class CAdjustMatrix;
class Matrix;

class CSplineSegment : public CSerializable
{
public:
	CSplineSegment(int degree= 3,double* startTime = NULL,double* endTime= NULL);
	~CSplineSegment(void);

	int getDegree() {return this->splineX.getDegree();};
	bool getCoefficients(int degree,double& parameterX,double& parameterY,double& parameterZ);

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();
	void applyScaleToOldProject(double scale);

	virtual bool isa(string& className) const
	{
		if (className == "CSplineSegment")
			return true;
		return false;
	};

	virtual string getClassName() const {return string("CSplineSegment"); }; 

	double getEndTime() { return *(this->endTime);};
	double getStartTime() { return *(this->startTime);};

	void setStartTime(double* st) {this->startTime = st;};
	void setEndTime(double* et) {this->endTime = et;};

	void evaluateLocation(C3DPoint& erg,const double time);
	void evaluateVelocity(C3DPoint& erg,const double time);

	void extrapolateLocation(C3DPoint& erg,const double time);
	void extrapolateVelocity(C3DPoint& erg,const double time);

	void Ai(CAdjustMatrix& AiPar, const double &t);
	void AiLocation(CAdjustMatrix& AiPar, const double &t);

	void getCoefficients(Matrix& param, const int startIndexX,const  int startIndexY,const int startIndexZ);
	void updateCoefficients(const Matrix& param, const int startIndexX,const  int startIndexY,const int startIndexZ);

	CSplineSegment &operator = (const CSplineSegment & segment);


private:
	
	double getLocalTime(const double time) const 
		{ return (time - *this->startTime) * this->timeScale;};
	
	CPolynomial splineX;
	CPolynomial splineY;
	CPolynomial splineZ;


	double* startTime;
	double* endTime;
	double timeScale;

	

};


#endif