#ifndef __SensorModelBase__
#define __SensorModelBase__


#include "3DPoint.h"
#include "Filename.h"
#include "UpdateHandler.h"
#include "ReferenceSystem.h"
#include "Serializable.h"
#include "newmat.h"

class C2DPoint;
class C3DPoint;
class CXYPoint;
class CXYZPoint;
class CXYPointArray;
class CXYZPointArray;
class CObsPoint;
class CObsPointArray;
class CRobustWeightFunction;
class CAdjustMatrix;
class CRotationMatrix;
class CCamera;
class C3DPlane;
class C3DAdjustPlane;

#define PARAMETER_1 1
#define PARAMETER_2 2
#define PARAMETER_3 4
#define PARAMETER_4 8
#define PARAMETER_5 16
#define PARAMETER_6 32
#define PARAMETER_7 64
#define PARAMETER_8 128
#define PARAMETER_9 256
#define PARAMETER_10 512





/*!
 * \brief
 * Abstract sensor model class for (hybrid) adjustment.
 * 
  	 CSensorModel is an abstract base class describing a sensor model for
	 (hybrid) adjustment that is used to determine the sensor's orientation
	 parameters.
	
	<h3 style="text-decoration:underline;">Coordinate systems:</h3>
	<OL CLASS="sublist">
		<LI CLASS="sublist">
			the "File Coordinate System": the system in which the actual observations
			were made. In most cases it is identical to the sensor coordinate system.
			Exception: for digitized aerial photos, the "file coordinates" refer to the
			scanned image file, whereas the "sensor coordinates" refer to the camera frame
		</LI>

		<LI CLASS="sublist">
			the "Sensor Coordinate System" (or "Observation Coordinate System") is in most 
			cases identical to the file coordinate system, and it is the coordinate system 
			in which the observations are provided to adjustment (exception: see above)
		</LI>

		<LI CLASS="doc">
			the "Platform Coordinate System" is a system in which the sensor is mounted. 
			The platform coordinate system is related to the sensor coordinate system by
			a shift and a rotation; the parameters of this transformation are sometimes referred
			to as "mounting calibration". Examples: GPS antenna excentricity (shift), 
			IMU boresight alignment (rotation)
		</LI>


		<LI CLASS="doc">
			the "Reference Coordinate System" (or "Object Coordinate System") is the system
			in which the object points are to be determined. It is related to the platform
			coordinate system by a spatial similarity transformation, i.e., by a shift, 
			a rotation, and a scale.
		</LI>

	</OL>

	 A sensor model describes the relation between the Reference Coordinate System and
	 the Sensor Coordinate System. A point's coordinates in the sensor coordinate system
	 depend on its coordinates in the reference coordinate system and on the parameters
	 of the function describing the mapping between the two systems. The sensor model 
	 is described by the mapping function and its parameters.
	
	 The basic model used for CSensorModel and its derived classes is: 
	
    (p_s - p_0 + dADP(p_s, ADP)) = R_m^T * (s * R^T * (P_r - P_0) - p_m) + F(P_r, C)

	
	 with
	<table style="border-width:0px">
	  <tr>
		<td> p_s  </td><td>observed point in the sensor coordinate system (the observation)</td>
      </tr>
	  <tr>
	 	<td> p_0  </td><td> interior reference point in the sensor coordinate system 
	                      (reduction point; interior orientation with images)</td>
	  </tr>
	  <tr>
	 	<td> dADP(p_s, ADP)</td><td>a function for correction of the observations for systematic
	                      errors, depending on the observations p_s and (potentially unknown) additional parameters ADP</td>
	  </tr>
	  <tr>
	 	<td> R_m^T </td><td>a transposed rotation matrix describing a "mounting rotation" inside
	                      the platform </td>
	  </tr>
	  <tr>
	 	<td> p_m  </td><td>a shift describing the "mounting shift" inside the platform</td>
	   </tr>
	  <tr>
	 	<td> s    </td><td>a scale </td>
	  </tr>
	  <tr>
	 	<td> R^T  </td><td>a transposed rotation matrix describing the rotation between the
	                      reference and the platform coordinate systems</td>
      </tr>
	  <tr>
		<td> P_0  </td><td>a shift </td>
	  </tr>
	  <tr>
	 	<td> P_r  </td><td>the point in the reference coordinate system</td>
      </tr>
	  <tr>
	 	<td> F(P_r, C) </td><td>a function F of P_r and some station constants C (e.g. RPC parameters)</td>
	 </tr>
	</table>

	 The derived classes might not want to use all these parameters or declare some
	 of them to be constant. That is why the abstract base class just provides 
	 the numbers of the parameters for each of the above groups. For each individual
	 sensor, only a subset of the potential parameters might actually be considered
	 unknown. Thus, the "actual number" of free parameters (the number of parameters
	 to be determined in adjustment) might differ from the "potential number" of parameters
	 (the overall number of parameters, including both the free parameters and constants).
 * 
 * \see
 * CBundleManager | CObservationHandler
 */
class CSensorModel : public CSerializable, public CUpdateHandler
{
public:

//! possible parameters used by the sensor models
typedef enum {shiftPar = 0 , /*!< shift parameters: P_0 */  
			  rotPar, /*!< rotation parameters: R^T */
			  scalePar, /*!< scale parameters: s */ 
			  mountShiftPar, /*!< mounting shift parameters: p_m */ 
			  mountRotPar, /*!< mounting rotation parameter: R_m^T */ 
			  interiorPar, /*!< interior shift parameters: p_0 */ 
              addPar, /*!< additional parameters: ADP */ 
			  constraints /*!< constraints */ 
			  } ModelParameter; 

protected:

	//! reduction point for object coordinates
	C3DPoint reduction;				

	//! file name / string ID of the sensor model
    CFileName FileName;				

	/// Parameter numbers (to be filled in by derived class):

	//! Potential number of Parameter of the Sensor model
	int nPotentialParameter[8];	

	//! Actual number of used Parameter of the Sensor model
	int nActualParameter[8];

	//! index of the parameter in N-Matrix
	int parameterIndex[8];	

	//! allows individual parameters within one of the 9 parameter groups to be turned on/off
	int activeParameter[8];			
									
	//! Potential number of all station parameters; identical to the sum of all potential pars
    int nPotentialStationPars;      

	//! Actual number of all station parameters; identical to the sum of all actual pars
	int nActualStationPars;         
								    
	//! number of unknowns per object point
	/*! identical to the dimensionality of the reference coordinate system
        (usually 3, but might be 2 for 2D adjustment problems)*/
    int nParsPerPoint;               
					                                              
	//! number of observations per observed point
	/*! identical to the dimensionality of the sensor coordinate system*/
    int nObsPerPoint;                
	                                
	//! covariance matrix of the parameters	
	Matrix covariance;             

	//! description of the reference frame the sensor model refers to
	CReferenceSystem refSystem;		

	//! indicates if the model has bee initialised 
    bool hasValues;

	//! for objects with more then one sensor model
	bool bIsCurrentSensorModel;

  public:

    CSensorModel(bool I_hasValues,          const CFileName &I_FileName, 
				 int nShiftParameters,      int nRotationParameters,
				 int nScaleParameters,      int nMountShiftParameters,
				 int nMountRotParameters,   int nInteriorParameters, 
				 int nAdditionalParameters, int nPointParameters,
				 int nPointObservations);

    CSensorModel(const CSensorModel &sensor);

    virtual ~CSensorModel();

	CSensorModel &operator = (const CSensorModel & sensor);

	void setToCurrentSensorModel(bool b) {this->bIsCurrentSensorModel = b;};

	bool isCurrentSensorModel()const {return this->bIsCurrentSensorModel;};


	void setParameterCount(ModelParameter type,int n) 
	{
		this->nActualParameter[type] = n;
		this->calculateNumberOfParameter();
	
		this->informListeners_elementsChanged(this->getClassName().c_str());

	};

	void deactivateParameterGroup(ModelParameter type) 
	{
		this->nActualParameter[type] = 0;
		this->calculateNumberOfParameter();
	
		this->informListeners_elementsChanged(this->getClassName().c_str());
	};


	void activateParameterGroup(ModelParameter type) 
	{
		this->nActualParameter[type] = this->nPotentialParameter[type];
		this->calculateNumberOfParameter();
	
		this->informListeners_elementsChanged(this->getClassName().c_str());
	};


	void deactivateParameter(ModelParameter type,int parameters) 
	{
		int maxParameter = (1 << this->nPotentialParameter[type]) - 1;
		if (parameters > maxParameter)
			return;

		this->activeParameter[type] &= ~parameters;
		this->nActualParameter[type] = this->getNActiveParameter(type);
		this->calculateNumberOfParameter();
	
		this->informListeners_elementsChanged(this->getClassName().c_str());
	};


	void activateParameter(ModelParameter type,int parameters) 
	{

		int maxParameter = (1 << this->nPotentialParameter[type]) - 1;
		if (parameters > maxParameter)
			return;


		this->activeParameter[type] |=  parameters;
		this->nActualParameter[type] = this->getNActiveParameter(type);
		this->calculateNumberOfParameter();
	
		this->informListeners_elementsChanged(this->getClassName().c_str());
	};


	int getActiveParameterStatus(ModelParameter type,int parameter) const
	{
		return ( this->activeParameter[type] & parameter );
	};

	int getActiveParameter(ModelParameter type) const
	{
		return ( this->activeParameter[type]);
	};

	int getNActiveParameter(ModelParameter type) const
	{
		int n=0;
		int parameter =1;
		for (int i=0; i< this->nPotentialParameter[type]; i++)
		{
			if (this->activeParameter[type] & parameter)
				n++;
			parameter = parameter << 1;
		}

		return n;
	}

	void setActiveParameter(ModelParameter type, int val) 
	{
		this->activeParameter[type] =  val;
		this->nActualParameter[type] = this->getNActiveParameter(type);
		this->calculateNumberOfParameter();
	
		this->informListeners_elementsChanged(this->getClassName().c_str());
	};

	int getParameterCount(ModelParameter type) const { return this->nActualParameter[type];};
	
	int getParameterIndex(ModelParameter type) const { return this->parameterIndex[type];};
	void setParameterIndex(ModelParameter type, int index) { this->parameterIndex[type] = index;};

	virtual int getNumberOfStationPars() const { return nActualStationPars; };

	int getNObsPerPoint() const { return nObsPerPoint; };

	int getNParsPerPoint() const { return nParsPerPoint; };

	virtual CCamera *getGenericCamera() const { return 0; };

	virtual void crop(int offsetX, int offsetY, int newWidth, int newHeight) = 0;

	virtual int getDirectObsCount() const { return 0; };

    CFileName* getFileName();

	virtual CFileName getItemText() const;
   
	const char *getFileNameChar() const { return FileName.GetChar(); };

	const CReferenceSystem &getRefSys() const { return refSystem; };

	C3DPoint getReduction() const { return reduction; };

	virtual Matrix *getCovariance();

	void calculateNumberOfParameter();

	virtual void setReduction(const C3DPoint &red);

	virtual void applyReduction();

	void initFromModel(const CSensorModel &sensorModel);

	virtual bool setRefSys(const CReferenceSystem &I_refSystem);

    virtual void setAdjustableValues(const Matrix &parms) = 0;

	virtual void setAll(const Matrix &parms) = 0;

    virtual bool read(const char *filename) = 0;

	virtual bool writeTextFile(const char* filename) const = 0;

    void setFileName(const char* filename);

	virtual void setCovariance(const Matrix &covar);

	void resetParameterIndices();

    // CSerializable functions
    virtual void serializeStore(CStructuredFileSection* pStructuredFileSection) = 0;
    virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection) = 0;

    virtual void reconnectPointers();
    virtual void resetPointers();

    virtual bool isModified();

	virtual bool hasUnknownObjPoints() {return true;};

	virtual CCharString getGUIName() const;

    /** \brief Transforms an xyz point to its cooresponding xy point
     *    in image space using the current parameters of the sensor model.
     *
     * @param obj The xyz point
     * @return the transformed xy point
     */
    virtual void transformObj2Obs(C2DPoint  &obs, const C3DPoint &obj) const = 0;



    /** \brief Transforms an xyz point to its cooresponding xy point
     *    in image space using the current parameters of the sensor model.
     *
     * @param obj The xyz point
     * @return the transformed xy point
     */
	virtual void transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const = 0;

    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, double Height) const = 0;

    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const = 0;

    virtual bool transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const = 0;

    virtual bool transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const = 0;

    virtual void transformObj2Obs(CObsPoint  &obs, const CXYZPoint &obj) const;
    virtual void transformArrayObj2Obs(CObsPointArray &obsArray, const CXYZPointArray &objArray) const;

    virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const = 0;

    virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint &obs) const;

	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const = 0;

	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const = 0;

	virtual bool getVerticalPole(CXYZPoint      &objGround, CXYZPoint      &objTop, 
								 const CXYPoint &obsGround, const CXYPoint &obsTop, 
								 double heightGround, float sigmaZ) const;


    /**
     * Gets a matrix of the parameters
     *
     * @return the parameters backed into a matrix
     */
    virtual bool getAllParameters(Matrix &parms) const = 0;

    virtual bool getAdjustableParameters(Matrix &parms) const = 0;

    virtual bool insertParametersForAdjustment(Matrix &parms) = 0;

    virtual bool setParametersFromAdjustment(const Matrix &parms) = 0;

    /**
     * Gets a matrix of the parameters without aps
     *
     */
    virtual Matrix getConstants() const = 0;

    /**
     * This will reformulate the parameters to incorporate the AP terms into the standard
     * RPC form.
     */
	virtual void reformulate() { };

	virtual void resetSensor() { };

    bool gethasValues() const
    {
        return this->hasValues;
    }

	virtual void sethasValues(bool hasvalues)
	{
		this->hasValues = hasvalues;

		if (!hasvalues)
			this->resetSensor();
	}


	virtual int accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
						   CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
						   CRobustWeightFunction *robust, bool omitMarked) = 0;

    /**
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Column Matrix of approximations of the parameters of the least squares fit
     */
    virtual bool approximate (Matrix &approx, const Matrix &consts, const Matrix &obs) const
	{
		return getAdjustableParameters(approx);
	};

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return Function evaluated at the given observation unsing the given parameters
     */
    virtual bool Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const = 0;

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return A Matrix of partials with respect to paramters evaluated at ob
     */
    virtual bool Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					Matrix &AiPar,        Matrix &AiPoint, 
					const Matrix &obs, const C3DPoint &approx) const = 0;

    virtual bool AiPars (Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const = 0;

    virtual bool AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const = 0;


    virtual bool AiBiFi(Matrix &Fi, Matrix &Bi, 
						Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiPar,        Matrix &AiPoint,
						const Matrix &obs, const C3DPoint &approx) = 0;

    /**
     * @return Bi Matrix of patials with respect to observations for the ith observation
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     */
    virtual bool Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const = 0;

    /**
     * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
     */
     virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const = 0;

	 
	 virtual void addDirectObservations(Matrix &N, Matrix &t)=0;
	
	 virtual double getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res);

    /**
     * @return a Vector containg the "familiar" String names of each observation; ex. "x", "y", "z"...
     */
     virtual void getObservationNames(CCharStringArray &names) const;

	 virtual int coordPrecision() const { return 3; };

	 virtual CCharString obsUnitsString() const { return "[pixels]"; };	 

	 virtual CCharString diffUnitsString() const { return "[pixels]"; };

	 virtual void convertToDifferentialUnits(CObsPoint &out, const CObsPoint &in) const;

	virtual void printStationParameter(CCharString& output,CRotationMatrix* rotMat=NULL)const;


     /**
      * @return a String cantainint the name of the best fit ex. "Best fit Line"
      */
     virtual void getName(CCharString& names) const = 0;  

	 virtual void addConstraint(Matrix* N, Matrix* t, Matrix* parms){};
   

    /**
     * Generally 0, but can be overridden to reflect the number of "observations"
     * that are not really discreet. Useful in calculating the dof.
     */
	virtual int getNonDistinctObservationCount() const { return 0; };

	static void addMatrix(int startRow, int startCol, Matrix &dest, Matrix &src);

	virtual CObsPoint calcRes(const Matrix &Delta, const CObsPoint &obs, const CObsPoint &obj);

  protected:

	  static bool checkRobust(CObsPoint &obs, const CObsPoint &normalisedDiscrepancy, 
		                      const CRobustWeightFunction *robust,
							  int coordIndex, bool omit, double &w, 
							  CAdjustMatrix &Pmat);

	  void accumulateShifts(Matrix &N, Matrix &t,
						    const Matrix &Fi,         const Matrix &Bi,   
						    const Matrix &AiErp,      const Matrix &AiRot,      
							const Matrix &AiSca,      const Matrix &AiMountShift, 
							const Matrix &AiMountRot, const Matrix &AiIrp, 
							const Matrix &AiPar,      const Matrix &AiPoint, 
							const Matrix &We,         int pointOffset);

	  void accumulateRots  (Matrix &N, Matrix &t,
						    const Matrix &Fi,           const Matrix &Bi,   
						    const Matrix &AiRot,        const Matrix &AiSca,      
							const Matrix &AiMountShift, const Matrix &AiMountRot, 
							const Matrix &AiIrp,        const Matrix &AiPar,      
							const Matrix &AiPoint,      const Matrix &We,
							int pointOffset);

	  void accumulateScale (Matrix &N, Matrix &t,
						    const Matrix &Fi,         const Matrix &Bi,   
							const Matrix &AiSca,      const Matrix &AiMountShift, 
							const Matrix &AiMountRot, const Matrix &AiIrp, 
							const Matrix &AiPar,      const Matrix &AiPoint, 
							const Matrix &We,         int pointOffset);

	  void accumulateMtShft(Matrix &N, Matrix &t,
						    const Matrix &Fi,           const Matrix &Bi,   
						    const Matrix &AiMountShift, const Matrix &AiMountRot, 
							const Matrix &AiIrp,        const Matrix &AiPar,      
							const Matrix &AiPoint,      const Matrix &We,
							int pointOffset);

	  void accumulateMtRot (Matrix &N, Matrix &t,
						    const Matrix &Fi,         const Matrix &Bi,   
							const Matrix &AiMountRot, const Matrix &AiIrp, 
							const Matrix &AiPar,      const Matrix &AiPoint, 
							const Matrix &We,         int pointOffset);

	  void accumulateIntPar(Matrix &N, Matrix &t,
						    const Matrix &Fi,           const Matrix &Bi,   
						    const Matrix &AiIrp,        const Matrix &AiPar,      
							const Matrix &AiPoint,      const Matrix &We,
							int pointOffset);

	  void accumulateAddPar(Matrix &N, Matrix &t,
						    const Matrix &Fi,         const Matrix &Bi,   
							const Matrix &AiPar,      const Matrix &AiPoint, 
							const Matrix &We,         int pointOffset);

};


#endif

