#ifndef __CResidualRecord__
#define __CResidualRecord__

#include "CharString.h"


/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

class CObsPoint;
class CSensorModel;

class CResidualRecord 
{
  protected:

	  double normRes;

	  double res;

	  CObsPoint *obs;

	  CCharString stationName;

	  CSensorModel *sensorMod;
  
	  int index;

  public:
	 
	  CResidualRecord() : obs(0), sensorMod (0), index(0), res(0), normRes(0) { };
	  CResidualRecord(CSensorModel *SensorMod, const CCharString &StationName) : stationName(StationName), obs(0), sensorMod (SensorMod), index(0), res(0), normRes(0) { };
	  CResidualRecord(double Res, double NormRes, CObsPoint *Obs, const CCharString &StationName, CSensorModel *SensorMod, int Index):
		obs(Obs), sensorMod (SensorMod), index(Index), res(Res), normRes(NormRes), stationName(StationName) { };
	  
	  CResidualRecord(const CResidualRecord &rec):
		obs(rec.obs), sensorMod (rec.sensorMod), index(rec.index), res(rec.res), normRes(rec.normRes), stationName(rec.stationName) { };


	  ~CResidualRecord() { obs = 0; sensorMod = 0; };

	  double getRes() const { return res; };

	  double getNormRes() const { return normRes; };

	  CCharString getStationName() const { return stationName; };

	  CObsPoint *getObs() const { return obs; };

	  CSensorModel *getSensorMod() const { return sensorMod;  };

	  bool isValid() const { return obs == 0; };

	  bool operator <  (const CResidualRecord &rec) const { return this->normRes <  rec.normRes; };
	  bool operator <= (const CResidualRecord &rec) const { return this->normRes <= rec.normRes; };
	  bool operator == (const CResidualRecord &rec) const { return this->normRes == rec.normRes; };
	  bool operator >  (const CResidualRecord &rec) const { return this->normRes >  rec.normRes; };
	  bool operator >= (const CResidualRecord &rec) const { return this->normRes >= rec.normRes; };

	  CResidualRecord &operator = (const CResidualRecord &rec);

	  void set(double Res, double NormRes, CObsPoint *Obs, const CCharString &StationName, CSensorModel *SensorMod, int Index);
	  
	  void setObs(CObsPoint *Obs) { this->obs = Obs; };

	  void set(double Res, double NormRes, int Index);

	  CCharString getString(int pointWidth) const;

  protected:


};



#endif

