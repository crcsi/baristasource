#ifndef __CCamera__
#define __CCamera__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "XYZPoint.h"
#include "Serializable.h"
#include "UpdateHandler.h"

#include "Distortion.h"

class CTrans2D;
typedef enum {eNoType,eCCD,eDigitalFrame,eAnalogueFrame} CameraType;

class CCamera : public CSerializable, public CUpdateHandler
{

public:
	CCamera(void);
	CCamera(const CCamera& camera);

	virtual ~CCamera(void);

	CCamera &operator = (const CCamera & c);

	virtual bool transformObs2Obj(C3DPoint& xc,const C3DPoint& obs,const CTrans2D* frameToCamera = NULL) const {return false;};
	virtual bool transformObj2Obs(C2DPoint& obs,const C3DPoint& obj,const CTrans2D* cameraToFrame = NULL) const {return false;};


	virtual bool AiIRP(CAdjustMatrix &AiIrp,const C2DPoint &obs, const C2DPoint &approx,int activeIrpPar) const;
	virtual bool AiADP(CAdjustMatrix &AiAdp,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar) const;

	virtual bool Bi(CAdjustMatrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const;
	virtual bool Bi(Matrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const;

	virtual bool insertIRPParametersForAdjustment(Matrix &parms,int indexIrp,int activeIrpPar)const;
	virtual bool insertADPParametersForAdjustment(Matrix &parms,int indexAdp,int activeAddPar)const;
	
	virtual bool setIRPParametersFromAdjustment(const Matrix &parms,int indexIrp,int activeIrpPar);
	virtual bool setADPParametersFromAdjustment(const Matrix &parms,int indexAdp,int activeAddPar);

	virtual void printIRPParameter(ostrstream &protocol,int activeIrpPar) const;
	virtual void printADPParameter(ostrstream &protocol,int activeAdpPar) const;

	virtual void setIRPParameterCovariance(const Matrix &covar,int activeIrpPar);
	virtual void setADPParameterCovariance(const Matrix &covar,int activeAdpPar);

	virtual void resetADPParameter();
	virtual void resetIRPParameter();

	virtual void addDirectObservationsAdp(Matrix &N, Matrix &t,int indexAdp,int activeAddPar)const;
	virtual double getResidualsDirectAdpObservations(const Matrix &deltaPar, Matrix &res,int indexAdp,int activeAddPar)const;


	void setIRP(const CXYZPoint pp){this->interiorReferencePoint = pp;};
	void setNCols(const int ncols){this->nCols = ncols;};
	void setNRows(const int nRows){this->nRows = nRows;};
	void setScale(const double scale){this->scale = scale;};

	double getPrincipalDistance() const {return this->interiorReferencePoint[2];};
	CXYZPoint getIRP()  const{return this->interiorReferencePoint;}; 
	int getNCols()  const{return this->nCols;}; 
	int getNRows()  const{return this->nRows;}; 
	double getScale()  const{return this->scale;}; 

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified(){return this->bModified;};
	virtual void reconnectPointers();
	virtual void resetPointers();

	virtual bool isa(string& className) const
	{
	  if (className == "CCamera")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CCamera"); };  

	void setDistortion(DistortionType type,const doubles& parameter);

	void setDistortion(CDistortion* distortion);

	CDistortion* getDistortion() { return this->distortion;};

	CameraType getCameraType() const {return this->type;};

	void informListener();

	void goingToBeDeleted();
protected :

	CXYZPoint interiorReferencePoint;	// in camera system unit (m or mm, for spot in pixel)

	int nCols;
	int nRows;
	double scale;	// transforms from pixel to camera system xc = scale * xf[pixel]

	CDistortion* distortion;

	CameraType type;

	bool callListener;

};
#endif