#ifndef __CDistortionFactory__
#define __CDistortionFactory__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Distortion.h"

class CDistortionFactory
{
public:
	static CDistortion* createDistortion(DistortionType type);
	static CDistortion* createDistortion(CDistortion* d);
	static void copyDistortion(CDistortion* dest,const CDistortion* src);
};
#endif
