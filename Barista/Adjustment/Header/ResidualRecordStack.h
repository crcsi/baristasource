#ifndef __CResidualRecordStack__
#define __CResidualRecordStack__

#include <vector>
using namespace std;

#include "ResidualRecord.h"

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University of Melbourne<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */

class CResidualRecordStack
{
  protected:

	  vector<CResidualRecord> stack;

	  int currRes;

	  CResidualRecordStack() { };

  public:
	 
	  CResidualRecordStack(int StackSize): currRes(0), stack(StackSize) { };

	  ~CResidualRecordStack() { };

	  int nRes() const { return currRes; };

	  int stackSize() const { return stack.size(); };

	  void reset() { currRes = 0; };

	  const CResidualRecord &operator[](int index) const { return stack[index]; };

	  double getNormRes(int index) const { return stack[index].getNormRes(); };

	  bool insertRecord(const CResidualRecord &rec);

	  int insertRecord(CResidualRecord &rec, const CObsPoint &obs, 
		               const CObsPoint &res, const CObsPoint &resnorm,
					   bool omitMarked);

	  CCharString getString(int pointWidth) const;

  protected:


};



#endif

