#ifndef __CCameraMountingPtrArray__
#define __CCameraMountingPtrArray__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "MUObjectPtrArray.h"
#include "CameraMounting.h"

class CCameraMountingPtrArray : public CMUObjectPtrArray<CCameraMounting>
{
public:
	CCameraMountingPtrArray(int nGrowBy = 0);
	CCameraMountingPtrArray(const CCameraMountingPtrArray& src);

	~CCameraMountingPtrArray(void);
};

#endif