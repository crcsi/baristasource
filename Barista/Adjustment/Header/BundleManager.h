#ifndef __CBundleManager__
#define __CBundleManager__

/**
 * Title:        Barista
 * Description:
 * Copyright:    Copyright (c) Franz Rottensteiner, Harry Hanley, Paul Dare, Simon Cronk
 * Company:      University of Melbourne
 * @author Franz Rottensteiner, Harry Hanley
 * @version 1.0
 */

#pragma once
#include <iostream>

#include "newmat.h"

#include "IntegerArrayArray.h"
#include "XYPointArrayPtrArray.h"
#include "PointerArray.h"
#include "3DPoint.h"
#include "XYZPointArray.h"
#include "ObservationHandlerPtrArray.h"
#include "ProgressHandler.h"
#include "ResidualRecordStack.h"


class Matrix;
class CRobustWeightFunction;
class CRotationMatrix;

class CBundleManager : public CProgressHandler
{
  protected:

	  /// All observation handlers taking part in adjustment
	  CObservationHandlerPtrArray ObservationHandlers; 

	  /// All unknown object points determined in adjustment
	  CXYZPointArray ObjectPoints;                  

	  /// Coordinate reduction applied to object points for numerical reasons
	  C3DPoint reduction;
	
	  /// Number of station parameters to be determined in adjustment
	  int nStationPars;
	
	  /// Number of unknown point coordinates to be determined in adjustment
	  int nPointPars;

	  /// Number of point observations taking part in the adjustment
	  int nPointObs;

	  /// Number of direct observations for unknown station parameters taking part in the adjustment
	  int nDirectObs;
		
	  /// Number of constrains to be added to the adjustment
	  int nConstraints;

	  /// Redundancy of adjustment. 
	  /// redundancy = nPointObs + nDirectObs - (nStationPars + nPointPars)
	  int redundancy;

	  /// Minimum number of observations for an object point to be determined in adjustment (3)
	  int minObs;

	  /// Average height of object points; used to compute approximations for object point coordinates
	  double averageHeight;

	  /// Limit for convergence: if between two iterations i and j 
	  /// ||(v^TPv_i - v^TPv_j) / (v^TPv_i + v^TPv_j) || < ConvergenceLimit 
	  /// the adjustment is considered to have converged
	  float ConvergenceLimit;

	  /// Vector containing the solution
	  Matrix solution;

	  /// RMS error of the weigth unit after adjustment
	  double rms;

	  /// Covariance matrix of the unknowns after adjustment
	  Matrix Qxx;

	  /// Maximum number of observations to be eliminated by robust estimation
	  float maxRobustPercentage;

	  /// Minimum normalised discrepancy for finishing robust estimation
	  float minRobFac;

	  /// Number of gross errors detected in robust estimation
	  int nGrossErrors;

	  /// protocol file
	  ofstream *protocol;
    
	  CResidualRecordStack worstResiduals;

	  vector<double> maxResX;
	  vector<double> maxResY;
	  vector<double> maxResZ;
	  vector<double> maxRobX;
	  vector<double> maxRobY;
	  vector<double> maxRobZ;

	  vector<CLabel> maxResLabelX;
	  vector<CLabel> maxResLabelY;
	  vector<CLabel> maxResLabelZ;
	  vector<CLabel> maxRobLabelX;
	  vector<CLabel> maxRobLabelY;
	  vector<CLabel> maxRobLabelZ;


	  vector<C3DPoint> RMS;

	  vector<int> NPointsX;
	  vector<int> NPointsY;
	  vector<int> NPointsZ;

	  bool hasQualityData;
	  int nCheckPointsUsed;

	  vector<C3DPoint> maxResCheckPoints;
	  vector<C3DPoint> rmsCheckPoints;
	  vector<int> nStationCheckPoints;

	  CReferenceSystem refSysCheckPoints;


	  bool NormalPointIsBlockDiagonal;

  public:

	  CBundleManager(float I_convergenceLimit = 0.0001, float MaxRobustPercentage = 0.03,
		             float MinRobFac = 3.0, float havg = 0.0, 
					 const CCharString &protfile = "");

	  virtual ~CBundleManager();

	  int getNStations() const
	  {
		  return this->ObservationHandlers.GetSize();
	  };
	  
	  const CObservationHandler *getObservationHandler(int index) const { return this->ObservationHandlers.GetAt(index); };

	  int getIndexOfObsHandler(CObservationHandler *handler) const;

	  int getNObjectPoints() const
	  {
		  return this->ObjectPoints.GetSize();
	  };

	  int getNObs() const { return (nPointObs + nDirectObs); };

	  int getNUnknowns() const { return (nStationPars + nPointPars); };

	  int getNConstraints() const { return this->nConstraints; };

	  double getRMS() const { return rms; };

	  int getNGrossErrors()const {return this->nGrossErrors;};

	  const CResidualRecord &getWorstResidual(int index) const { return worstResiduals[index]; };

	  int getNWorst() const { return worstResiduals.nRes(); };

	  CCharString rmsString(const char *prompt) const;

	  CCharString getStationName(int index) const;

	  void reset();

	  void setAverageHeight(double h) { averageHeight = h; };

	  bool addObservations(CObservationHandler *obs);

	  virtual void bundleInit(bool forwardOnly,bool excludeTie);

	  void updateParameterCovar(bool forwardOnly);

	  virtual void updateResiduals(bool forwardOnly);

	  void updateStationParametersCovar();

	  void getObjectPoints(CXYZPointArray &points);

	  virtual void setSensorFilenames();

	  virtual bool solve(bool forwardOnly, bool robust, bool omitMarked);

	  void initProtocol(const char *filename);
	  void closeProtocol();

	  void printResults();

	  static void setMatrix(int startRow, int startCol, Matrix &dest, Matrix &src);

	  static void print(const Matrix &mat, ofstream &aFile);

	  void setQualityAssessmentData(int nCheckPointsUsed, vector<C3DPoint> maxResCheckPoints , vector<C3DPoint> rmsCheckPoints, vector<int> nStationCheckPoints,CReferenceSystem refSysCheckPoints);
	  void resetQualityAssessmentData();

  protected: 
	  
	  void printObjectPointRecords(int pointwidth, CSensorModel *controlPointModel);

	  void printMaxResRecord(int index);

	  void printQualityAssessmentData();

	  virtual void initObservations(bool excludeTie);

	  virtual void initParameters(bool forwardOnly);

	  void setReduction(const C3DPoint &red);

	  void applyReduction();

	  void printToProtocol(const char *stationName, const char* parameterType, 
		                   const char *pointName,  const Matrix &obs, const Matrix &Fi); 

	  void printResToProtocol(const char *stationName, const char* parameterType, 
		                      const char *pointName,  const Matrix &obs, const Matrix &res); 

	  void printAllResiduals(int labellength);

	  void updateParameters(bool forwardOnly);

	  double calculateResiduals(bool forwardOnly, const Matrix &Delta, 
		                        bool omitMarked);

	  void solveOnce(CRobustWeightFunction *robust, bool omitMarked);

	  bool endIterateOnce(bool forwardOnly, CRobustWeightFunction *robust,
		                  bool omitMarked, int MaxIter);

	  void solveOnceForward(CRobustWeightFunction *robust, bool omitMarked);

	  bool invertNormalEquationMatrix(const Matrix &N);

	  void getStationParsVec(int i, const Matrix &parVec, Matrix &stationPars);

	  void printEliminatedObservations(int labelLength);

	  virtual bool determineFixedRotMatForOrbit(CRotationMatrix& fixedRotMat,const CCharString& orbitName)const {return false;};

	  static void dumpMatrix(const Matrix &mat, const char *filename, int digits, int precision);

	  void getCameraIndices(vector<int> &cameraIndices);
};

#endif