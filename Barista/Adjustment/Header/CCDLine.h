#ifndef __CCCDLine__
#define __CCCDLine__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Camera.h"



class CCCDLine : public CCamera
{
public:
	CCCDLine(void);
	CCCDLine(const CCCDLine& src);
	virtual ~CCCDLine(void);

	CCCDLine &operator = (const CCCDLine & c);

	virtual bool transformObs2Obj(C3DPoint& xc,const C3DPoint& obs,const CTrans2D* frameToCamera = NULL) const;
	virtual bool transformObj2Obs(C2DPoint& obs,const C3DPoint& obj,const CTrans2D* cameraToFrame = NULL) const;

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);


	virtual bool isa(string& className) const
	{
	  if (className == "CCCDLine")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CCCDLine"); };  


};
#endif