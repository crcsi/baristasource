#ifndef __CSPLINE__
#define __CSPLINE__


#include "Serializable.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

#include "SplineSegmentArray.h"
#include "Filename.h"
#include "doubles.h"
#include "ObsPoint.h"

class Matrix;
class CXYZPoint;

class CSpline : public CSerializable
{
public:
	CSpline(void);
	CSpline(const CSpline& spline);
	~CSpline(void);

	CSpline &operator = (const CSpline & spline);


	bool init(int degree,const CFileName& Name,doubles& knots);

	bool evaluateLocation(C3DPoint& erg,const double time)const;
	bool evaluateVelocity(C3DPoint& erg,const double time)const;
	bool evaluate(CObsPoint& erg, const double time)const;

	int getDegree() const;
	int getNumberOfSegments() const { return this->segments.GetSize();};

	bool getSplineParameter(int segmentIndex,int degree, double& parameterX,double& parameterY,double& parameterZ);

	bool backProjection(double &time,const C3DPoint& objPoint,const C3DPoint &n);


	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	void applyScaleToOldProject(double scale); // to be used for attitudes in [mrad] 
	                                           // if an old project is found with attitudes in [rad]
	virtual bool isModified();
	virtual void reconnectPointers();
	virtual void resetPointers();
	

	virtual bool isa(string& className) const
	{
		if (className == "CSpline")
			return true;
		return false;
	};

	virtual string getClassName() const {return string("CSpline"); }; 

	bool Ai(CAdjustMatrix &AiPar,const double &time,int &segIndex) const;
	bool AiLocation(CAdjustMatrix &AiPar,const double &time,int* segIndex)const;
	void addConstraint(Matrix* N, Matrix* t, Matrix* parms,const int constraintsIndex, const int colIndex)const;

	bool insertParametersForAdjustment(Matrix &parms,const int shiftParIndex)const;
	bool setParametersFromAdjustment(const Matrix &parms,const int shiftParIndex);

	bool getAdjustableParameters(Matrix &parms,const int addParIndex) const;
	void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames,const CCharString unit,const CCharStringArray &ParameterNames) const;

	// function returns index of spline segment for time
	// returns -1 when time before spline start
	// returns -2 when time after spline end
	int findSegment(const double time) const;

	double getFirstTime();

	double getLastTime();

protected:
	


	
	
	CSplineSegmentArray segments;
	doubles knots;
	CFileName	name;
	int degree;

};
#endif