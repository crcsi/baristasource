#ifndef __COrbitAttitudeModel__
#define __COrbitAttitudeModel__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "SplineModel.h"
#include "RotationBase.h"
#include "RollPitchYawRotation.h"
#include "XYZPoint.h"

class COrbitAttitudeModel : public CSplineModel
{
protected:
	CRotationMatrix fixedECRMatrix;
	double timeFixedECRMatrix;

	CXYZPoint angleObservation;
	CXYZPoint scaleObservation;

	CXYZPoint scaleParameter;
	CRollPitchYawRotation rotParameter;

public:
	COrbitAttitudeModel(void);
	COrbitAttitudeModel(const COrbitAttitudeModel& model);
	~COrbitAttitudeModel(void);

	COrbitAttitudeModel &operator = (const COrbitAttitudeModel & orbitAttitudes);

	virtual void getName(CCharString& names) const;

	void setFixedECRMatrix(CRotationMatrix& mat) {this->fixedECRMatrix = mat;};
	void setTimeFixedECRMatrix(double t) {this->timeFixedECRMatrix = t;};

	CRotationMatrix getFixedECRMatrix() const{return this->fixedECRMatrix;};
	double getTimeFixedECRMatrix() const {return this->timeFixedECRMatrix;};

	bool getRotMat(CRotationMatrix& rotMat, const double& time) const;

    virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
    virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual void reconnectPointers();
	virtual void resetPointers();
	virtual bool isa(string& className) const
	{
		if (className == "COrbitAttitudeModel")
			return true;
		return false;
	};

    virtual string getClassName() const {return string("COrbitAttitudeModel"); };

	virtual bool getAllParameters(Matrix &parms) const;
	virtual bool getAdjustableParameters(Matrix &parms) const;
	virtual bool insertParametersForAdjustment(Matrix &parms);
	virtual bool setParametersFromAdjustment(const Matrix &parms);
	virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const;


	CXYZPoint getRotParameter() const;
	void setRotParameter(const CXYZPoint& rot);

	CXYZPoint getScaleParameter() const;
	void setScaleParameter(const CXYZPoint& scale);


	virtual void extend_N_and_t_Matrix(Matrix& N,Matrix& t,
											const CAdjustMatrix& AxTP,
											const CAdjustMatrix& AyTP, 
											const CAdjustMatrix& AzTP,
											const CAdjustMatrix& P,
											const CAdjustMatrix& dl,
											const int indexNx,
											const int indexNy,
											const int indexNz,
											const CObsPoint &obs)const;

	virtual void extend_Ai(	Matrix &AiErp,Matrix &AiRot,Matrix &AiSca, 
							Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
							Matrix &AiAdd, Matrix &AiPoint,const Matrix &obs, const C3DPoint &approx)const;

	virtual void extend_Bi(	Matrix& Fi,const Matrix &obs, const C3DPoint &approx)const;
	virtual bool Fi(Matrix& Fi,const Matrix &obs, const C3DPoint &approx)const;
	virtual bool Fi(CObsPoint& fi, const CObsPoint &obs) const;

	virtual int getDirectObsCount() const;
	virtual void addDirectObservations(Matrix &N, Matrix &t);
	virtual double getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res);

	void setDirectObservationForAngles(const CXYZPoint& obs);
	CXYZPoint getDirectObservationForAngles() const {return this->angleObservation;};

	void setDirectObservationForScale(CXYZPoint& obs) ;
	CXYZPoint getDirectObservationForScale() const {return this->scaleObservation;};

	virtual CCharString obsUnitsString() const { return "[m�]"; };	
	virtual CCharString diffUnitsString() const { return "[m�]"; };
	virtual int coordPrecision() const;
	virtual void convertToDifferentialUnits(CObsPoint &out, const CObsPoint &in) const;
	virtual void getObservationNames(CCharStringArray &names) const;

	virtual void setCovariance(const Matrix &covar);
	
	virtual void printStationParameter(CCharString& output,CRotationMatrix* rotMat=NULL)const;

	virtual void initDirectObservations();
};
#endif