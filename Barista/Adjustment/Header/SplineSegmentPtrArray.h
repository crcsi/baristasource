#ifndef __CSplineSegmentPtrArray__
#define __CSplineSegmentPtrArray__

#include "MUObjectPtrArray.h"
#include "SplineSegment.h"

class CSplineSegmentPtrArray : public CMUObjectPtrArray<CSplineSegment>
{
public:
	CSplineSegmentPtrArray(int nGrowBy = 0);
	~CSplineSegmentPtrArray(void);
};

#endif