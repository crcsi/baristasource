#ifndef __CDistortion__
#define __CDistortion__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Serializable.h"
#include "Polynomial.h"
#include "2DPoint.h"
#include <strstream>

class CXYZPoint;
class C3DPoint;

class doubles;
class CAdjustMatrix;
class Matrix;

typedef enum {eNotInitialized, eDistortionALOS, eDistortionTHEOS} DistortionType;

class CDistortion : public CSerializable
{
public:
	CDistortion(DistortionType type);
	CDistortion(const CDistortion& d);
	virtual ~CDistortion(void);

	CDistortion &operator = (const CDistortion & d);

	virtual bool applyObs2Obj(C3DPoint& obj,const C3DPoint& obs, const CXYZPoint& irp,const double& scale) const=0;
	virtual bool applyObj2Obs(C2DPoint& obs,const C2DPoint& obj, const CXYZPoint& irp,const double& scale) const=0;

	virtual bool Ai(CAdjustMatrix& AiAdp,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar) const=0;
	virtual bool Bi(CAdjustMatrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const=0;
	virtual bool Bi(Matrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const=0;

	virtual bool insertParametersForAdjustment(Matrix &parms,int indexAdp,int activeAddPar)const=0;

	virtual bool setParametersFromAdjustment(const Matrix &parms,int indexAdp,int activeAddPar)=0;

	virtual void printParameter(ostrstream &protocol,int activeAdpPar) const=0;

	virtual void setParameterCovariance(const Matrix &covar,int activeAdpPar)=0;

	virtual void setDistortion(const doubles& parameter) =0;

	virtual void addDirectObservations(Matrix &N, Matrix &t,int indexAdp,int activeAddPar) {};

	virtual double getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res,int indexAdp,int activeAddPar) {return 0.0;};

	virtual void setDirectObservation(double observation,int activeAdpPar){};

	virtual double getDirectObservation(int activeAdpPar){ return 0.0;};

	virtual double getSigmaDirectObservation(int activeAdpPar){ return 0.0;};

	virtual void setSigmaDirectObservation(double sigma,int activeAdpPar){};

	virtual double getParameter(int activeAdpPar){ return 0.0;};
		
	virtual double getSigmaParameter(int activeAdpPar){ return 0.0;};

	virtual void setParameter(double parameter,int activeAdpPar){};

	virtual void getParameterName(CCharString& name,int activeParameter){};

	virtual void resetParameter(){};

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified(){return this->bModified;};
	virtual void reconnectPointers();
	virtual void resetPointers();

	virtual bool isa(string& className) const
	{
	  if (className == "CDistortion")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CDistortion"); }; 

	DistortionType getDistortionType() const {return this->type;};
	
protected:
	CPolynomial radialDistortion;
	DistortionType type;

};

#endif
