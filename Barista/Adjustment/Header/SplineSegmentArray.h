#ifndef __CSplineSegmentARRAY__
#define __CSplineSegmentARRAY__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "SplineSegment.h"
#include "SplineSegmentPtrArray.h"

#include "MUObjectArray.h"
#include "Serializable.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

class CSplineSegmentArray : public CMUObjectArray<CSplineSegment, CSplineSegmentPtrArray>, public CSerializable
{
public:
	CSplineSegmentArray(int nGrowBy = 0);
	~CSplineSegmentArray(void);

	// serialize functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	bool isModified();
	void reconnectPointers();
	void resetPointers();

	CSplineSegment* Add();
	CSplineSegment* Add(const CSplineSegment& point);
	
	bool isa(string& className) const
	{
		if (className == "CSplineSegmentArray")
			return true;

		return false;
	};
	string getClassName() const {return string("CSplineSegmentArray");};
};
#endif