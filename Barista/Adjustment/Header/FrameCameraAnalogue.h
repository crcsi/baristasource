#ifndef __CFrameCameraAnalogue__
#define __CFrameCameraAnalogue__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "FrameCamera.h"
#include "XYPointArray.h"

class CFrameCameraAnalogue : public CFrameCamera
{
protected:
	

public:
	CFrameCameraAnalogue(void);
	CFrameCameraAnalogue(const CFrameCameraAnalogue& src);

	~CFrameCameraAnalogue(void);

	CFrameCameraAnalogue &operator = (const CFrameCameraAnalogue & c);
	CFrameCameraAnalogue &operator = (const CFrameCamera & c);

	virtual bool transformObs2Obj(C3DPoint& xc,  const C3DPoint& obs, double Z0, double Z, const CTrans2D* frameToCamera = NULL) const;
	virtual bool transformObj2Obs(C2DPoint& obs, const C3DPoint& obj, double Z0, double Z, const CTrans2D* cameraToFrame = NULL) const;

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	virtual bool isa(string& className) const
	{
	  if (className == "CFrameCameraAnalogue")
		  return true;
	  return false;
	};
	virtual string getClassName() const {return string("CFrameCameraAnalogue"); }; 

	bool addFiducialMark(const CXYPoint& fiducialMark);
	bool deleteFiducialMark(int index);

	bool updateFiducialMarkLabel(CLabel label,int index);
	bool updateFiducialMarkX(double x,int index);
	bool updateFiducialMarkY(double y,int index);
	


 
	
	virtual const CXYPointArray* getFiducialMarks() const {return &this->fiducialMarks;};

private:
	bool checkFiducialMarkLabel(CLabel label) const;
	
	CXYPointArray fiducialMarks;


};
#endif
