#ifndef __CPlaneObservationHandler__
#define __CPlaneObservationHandler__
#include <iostream>
using namespace std;

#include "ObservationHandler.h"
#include "CharString.h"

class C3DAdjustingPlane;

class CPlaneObservationHandler : public CObservationHandler
{
  protected: 

	  CCharString name;

	  C3DAdjustingPlane *plane;

  public:

	  CPlaneObservationHandler(const CCharString &fileName = "Plane");
	  
	  virtual ~CPlaneObservationHandler();

	  virtual const char *getFileNameChar() const { return name.GetChar(); };

	  C3DAdjustingPlane *getPlane() const { return this->plane; };
};


#endif

