#pragma once

class CXYPointArrayPtrArray;
class CObservationHandlerPtrArray;
class CCharStringArray;
#include "IntegerArrayArray.h"

class CLabelMerger
{
public:
    CLabelMerger(void);
    ~CLabelMerger(void);

protected:
    
    CIntegerArrayArray sightings;

public:
    void merge(CCharStringArray* labels, CXYPointArrayPtrArray* XYPointArrays);
    void merge(CCharStringArray &labels, CObservationHandlerPtrArray &ObservationArrays);

    CIntegerArrayArray* getSightings()
    {
        return &this->sightings;
    };
};
