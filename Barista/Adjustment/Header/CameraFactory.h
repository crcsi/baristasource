#ifndef __CCameraFactory__
#define __CCameraFactory__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Camera.h"

class CCameraFactory
{
public:
	static CCamera* createCamera(CameraType type);
	static CCamera* createCamera(const CCamera* c);
	static void copyCamera(CCamera* dest, const CCamera* src);
};
#endif