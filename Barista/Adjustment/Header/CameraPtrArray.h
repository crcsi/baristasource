#ifndef __CCameraPtrArray__
#define __CCameraPtrArray__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */
#include "MUObjectPtrArray.h"
#include "Camera.h"

class CCameraPtrArray : public CMUObjectPtrArray<CCamera>
{
public:
	CCameraPtrArray(int nGrowBy = 0);
	CCameraPtrArray(const CCameraPtrArray & src);
	~CCameraPtrArray(void);

};

#endif