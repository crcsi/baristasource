#ifndef __CSplineModelFactory__
#define __CSplineModelFactory__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "SplineModel.h"


typedef enum {Spline,Path,Attitude} SplineModelType;

class CSplineModelFactory
{
public:
	static CSplineModel* initSplineModel(SplineModelType type);
	static CSplineModel* initSplineModel(CSplineModel* splineModel,SplineModelType type);
};


#endif