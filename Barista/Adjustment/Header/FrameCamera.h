#ifndef __CFrameCamera__
#define __CFrameCamera__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Camera.h"
#include "CharString.h"
#include "XYPoint.h"

class CXYPointArray;

class CFrameCamera : public CCamera
{
protected:
	int calibrationDay;
	int calibrationMonth;
	int calibrationYear;

	CCharString objectiveName;
	CXYPoint irpSymmetry;

public:
	CFrameCamera(void);
	CFrameCamera(const CFrameCamera& src);

	~CFrameCamera(void);

	CFrameCamera &operator = (const CFrameCamera & c);

	virtual bool transformObs2Obj(C3DPoint& xc,  const C3DPoint& obs, double Z0, double Z, const CTrans2D* frameToCamera = NULL) const;
	virtual bool transformObj2Obs(C2DPoint& obs, const C3DPoint& obj, double Z0, double Z, const CTrans2D* cameraToFrame = NULL) const;

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	
	virtual Matrix getCalibrationMatrix() const;

	virtual bool isa(string& className) const
	{
	  if (className == "CFrameCamera")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CFrameCamera"); };  

	int getCalibrationDay()const {return this->calibrationDay;};
	int getCalibrationMonth() const {return this->calibrationMonth;};
	int getCalibrationYear() const {return this->calibrationYear;};
	CCharString getObjectiveName() const {return this->objectiveName;};
	CXYPoint getIRPSymmetry() const {return this->irpSymmetry;};

	void setCalibrationDay(int day) { this->calibrationDay= day;};
	void setCalibrationMonth(int month)  {this->calibrationMonth= month;};
	void setCalibrationYear(int year)  { this->calibrationYear= year;};
	void setObjectiveName(CCharString name) {this->objectiveName = name;};
	void setIRPSymmetry(CXYPoint irp) {this->irpSymmetry = irp;};

	virtual const CXYPointArray* getFiducialMarks() const {return NULL;};

	static double getK(double Z0, double Z) { return 13e-9*(Z0-Z) * (1.0 - 0.00002 * (2.0 * Z0 + Z)); }

	void computeRefractionAndEarthCurvature(double  Z0, double Z, double R, 
		                                    double   x, double y, 
											double &dx, double &dy) const;


};
#endif
