#ifndef __CGenericObservationHandler__
#define __CGenericObservationHandler__

#include "ObservationHandler.h"
#include "CharString.h"

class CGenericObservationHandler: public CObservationHandler 
{	
  protected:

	  CCharString name;

	  bool IsControl;

  public:

	  CGenericObservationHandler(CObsPointArray* ObsPoints, CSensorModel *SensorModel, const CCharString & Name, bool isControl);
	  
	  virtual ~CGenericObservationHandler();

	  virtual const char *getFileNameChar() const { return name; };

	  virtual bool isControl() const { return IsControl; };

};


#endif

