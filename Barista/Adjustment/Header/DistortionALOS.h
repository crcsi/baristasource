#ifndef __CDistortionALOS__
#define __CDistortionALOS__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "Distortion.h"


class CDistortionALOS : public CDistortion
{
public:
	CDistortionALOS(void);
	CDistortionALOS(const CDistortionALOS& d);

	virtual ~CDistortionALOS(void);

	CDistortionALOS &operator = (const CDistortionALOS & d);

	virtual bool applyObs2Obj(C3DPoint& obj,const C3DPoint& obs, const CXYZPoint& irp,const double& scale) const;
	virtual bool applyObj2Obs(C2DPoint& obs,const C2DPoint& obj, const CXYZPoint& irp,const double& scale) const;

	virtual bool Ai(CAdjustMatrix& AiAdp,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar) const;
	virtual bool Bi(CAdjustMatrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const;
	virtual bool Bi(Matrix& Bi,const C2DPoint &obs, const C2DPoint &approx,int activeAddPar)const;

	virtual bool insertParametersForAdjustment(Matrix &parms,int indexAdp,int activeAddPar)const;
	virtual bool setParametersFromAdjustment(const Matrix &parms,int indexAdp,int activeAddPar);

	virtual void printParameter(ostrstream &protocol,int activeAdpPar) const;

	virtual void setParameterCovariance(const Matrix &covar,int activeAdpPar);

	virtual void setDistortion(const doubles& parameter);

	virtual void addDirectObservations(Matrix &N, Matrix &t,int indexAdp,int activeAddPar);

	virtual double getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res,int indexAdp,int activeAddPar);

	virtual void setDirectObservation(double observation,int activeAdpPar);

	virtual double getDirectObservation(int activeAdpPar);

	virtual double getSigmaDirectObservation(int activeAdpPar);

	virtual void setSigmaDirectObservation(double sigma,int activeAdpPar);

	virtual double getParameter(int activeAdpPar);
		
	virtual double getSigmaParameter(int activeAdpPar);

	virtual void setParameter(double parameter,int activeAdpPar);

	virtual void getParameterName(CCharString& name,int activeParameter);

	virtual void resetParameter();


	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);


	virtual bool isa(string& className) const
	{
	  if (className == "CDistortionALOS")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CDistortionALOS"); }; 

protected:
	CPolynomial xDistortion;
	CPolynomial yDistortion;

	CPolynomial xDistortionObs;
	CPolynomial yDistortionObs;

};
#endif
