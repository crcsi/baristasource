#ifndef __CRPC__
#define __CRPC__

#include "SensorModel.h"

class doubles;
class C2DPointArray;
class C3DPointArray;

#define STANDARD_PARAMETER_COUNT 92;
#define RPC_FIXED 1.0e-20
#define RPC_FREE 10000

typedef enum {eRPCNone,eRPCShift,eRPCShiftDrift,eRPCAffine} RPCConfigType;

class CRPC : public CSensorModel
{

  protected:

	double line_num_coeff[20];
    double line_den_coeff[20];
    double samp_num_coeff[20];
    double samp_den_coeff[20];

    double line_off;
    double samp_off;
    double lat_off;
    double long_off;
    double height_off;

    double line_scale;
    double samp_scale;
    double lat_scale;
    double long_scale;
    double height_scale;

    double width;
    double height;

    double ap[7];

	RPCConfigType config;

  public:

    CRPC(void);
    ~CRPC(void);

    CRPC(const CRPC &rpc);
    CRPC(const Matrix &parms,
    double line_off,
    double samp_off,
    double lat_off,
    double long_off,
    double height_off,
    double line_scale,
    double samp_scale,
    double lat_scale,
    double long_scale,
    double height_scale,
    int width,
    int height);

	void copy(const CRPC &rpc);

	//! Initialise RPCs homologous image and object points
	//! imgPts: array of image points
	//! objPts: array of object points
	//!         homologous points must have identical array indices
	//! Width:  width of the image (i.e., number of columns) [pixels]
	//! Height: height of the image (i.e., number of rows) [pixels]
	//! return value: sigma0 of the adjustment
	double init(const C2DPointArray &imgPts, const C3DPointArray &objPts, int Width, int Height); 

	CXYZPoint getOffset() const;

	int nStationConstants() const { return 92; };

	double getAffinePar(int index) const { return ap[index]; };

    virtual void setAll(const Matrix &parms);

	virtual bool setRefSys(const CReferenceSystem &I_refSystem);

    virtual bool read(const char* filename);
    bool readIkonosFormat(const char* filename);
    bool readQuickBirdFormat(const char* filename);
	bool readSocetFormat(const char* filename);
	bool readTextFormat(const char* filename);

    bool writeIKONOSFormat(const char* filename);
    bool writeQuickBirdFormat(const char* filename);
	bool writeSocetFormat(const char* filename);

    virtual bool writeTextFile(const char* filename) const;

    void setHeight(int height);
    void setWidth(int width);
    double  getHeight();
    double  getWidth();

    void freeAffine();
    void fixAll();
    void freeShift();
    void freeShiftDrift();

	RPCConfigType getConfigType() const {return this->config;};

	virtual void resetSensor();

	virtual int getDirectObsCount() const { return covariance.Nrows(); };

    virtual void setAdjustableValues(const Matrix &parms);

	string getClassName() const
	{
		return string ("CRPC");
	};

	bool isa(string& className) const
	{
		if (className == "CRPC")
			return true;

		return false;
	};

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

  
    /**
     * Transforms an xyz point to its cooresponding xy point
     * in image space
     * The xyz point is assumed to be Lat Long Height expressed in the
     * appropriate zone for the RPC.
     *
     * @param xyz The xyz point
     * @return the transformed xy point
     */
    virtual void transformObj2Obs(C2DPoint &obs, const C3DPoint &obj) const;

    virtual void transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const;

	virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const;

	virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const;
	
	virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const;

    virtual bool transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const;

	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const;
	
	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const;

 	virtual void crop(int offsetX, int offsetY, int newWidth, int newHeight);

    /**
     * Gets a matrix of the parameters (all 99)
     *
     * @return the parameters backed into a matrix
     */
    virtual bool getAllParameters(Matrix &parms) const;

    virtual bool getAdjustableParameters(Matrix &parms) const;
    
	virtual bool insertParametersForAdjustment(Matrix &parms);

    virtual bool setParametersFromAdjustment(const Matrix &parms);

    /**
     * Gets a matrix of the rpc parameters without aps
     *
     */
    virtual Matrix getConstants() const;


    /**
     * This will reformulate the RPC's to incorporate the AP terms into the standard
     * RPC form.
     */
    virtual void reformulate();

    virtual void resetAffine();

    virtual bool Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const;
    virtual bool Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const;

	virtual bool Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					Matrix &AiPar,        Matrix &AiPoint, 
					const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPars(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiBiFi(Matrix &Fi, Matrix &Bi, 
						Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiPar,        Matrix &AiPoint,
						const Matrix &obs, const C3DPoint &approx);

	virtual int accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
						   CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
						   CRobustWeightFunction *robust, bool omitMarked);

	virtual void addDirectObservations(Matrix &N, Matrix &t);

    virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const;

    virtual void getName(CCharString& name) const;

  protected:
    /**
     *
     */
    void normalizeXY(C2DPoint &dest, const C2DPoint &src) const;


    /**
     * "Normalizes" the given xyz point expressed in WGS84 Lat Long Height
     * @return The normalized point
     */
    void normalizeXYZ(C3DPoint &dest, const C3DPoint &src) const;

    /**
     * "Expands" the given Normalized xyz point to WGS84 Lat Long Height
     * @return The expanded point
     */
    void expandXYZ(C3DPoint &dest, const C3DPoint &UVW) const;


	double getDenominatorL(double U, double V, double W) const;

	double getDenominatorS(double U, double V, double W) const;

	double getNumeratorL(double U, double V, double W) const;

	double getNumeratorS(double U, double V, double W) const;

	double getPhi(double l) const;

	bool iterateRPCadjustment(const Matrix &M, 
		                      const Matrix &N,
							  const Matrix &L,
							  const Matrix &S,
							  const unsigned int &nData,
							  doubles &directWeightsL,
							  doubles &directWeightsS);

	void separateFloatValue(double value,double& m, int& e);
	// newly added functions
	double calculateRMSE(const C2DPointArray &imgPts, const C3DPointArray &objPts);
	void findMatrices(const C2DPointArray &imgPtsRed, const C3DPointArray &objPtsRed, Matrix &M, Matrix &N, Matrix &L, Matrix &S);
};


#endif

