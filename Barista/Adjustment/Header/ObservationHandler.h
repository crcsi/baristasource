#ifndef __CObservationHandler__
#define __CObservationHandler__
#include <iostream>
using namespace std;

class CXYPointArray;
class CSensorModel;
class CLabel;
class Matrix;
class CXYZOrbitPointArray;
class CObsPointArray;

class CObservationHandler 
{
  protected:

	  CObsPointArray* obsPoints;
	  
	  CObsPointArray  *residualPoints;
	  
	  CSensorModel   *currentSensorModel;

	  bool useObservationHandler;
  public:

	  CObservationHandler();

	  CObservationHandler(const CObservationHandler& src);
	  
	  virtual ~CObservationHandler();

	  CObsPointArray *getObsPoints() const;

	  CObsPointArray  *getResidualPoints() const;

	  CSensorModel *getCurrentSensorModel() const;
 
	  CLabel getLabel(int index) const;

	  bool isActive(int index) const;

	  int nObs() const;

	  int getNpointObservations() const;
	  int getNstationParameters() const;
	  int getNConstraints() const;
	  virtual const char *getFileNameChar() const = 0;

	  void resetIndices();
	  virtual int getNOrbitPoints() const;

	  virtual bool isControl() const { return false; };

	  int getDimension() const;

	  bool hasUnknownObjPoints();

	  bool getUseObservationHandler() const {return this->useObservationHandler;};

	  void setUseObservationHandler(bool use) {this->useObservationHandler = use;};
	

};


#endif

