#ifndef __CAffine__
#define __CAffine__

#include "SensorModel.h"

class CAffine : public CSensorModel
{
  protected:

	double a1, a2, a3, a4, a5, a6, a7, a8;
    int nStationConstants;

    Matrix stationConstants;

  public:
    CAffine(void);
    CAffine(const CAffine &affine);
    CAffine(const Matrix &parms);
    ~CAffine(void);

	void copy(const CAffine &affine);

    virtual void setAll(const Matrix &parms);

	virtual void crop(int offsetX, int offsetY, int newWidth, int newHeight);

	virtual CAffine &operator =(const CAffine &affine);

    bool isa(string& className) const;
    string getClassName() const;

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    virtual void transformObj2Obs(C2DPoint &obs, const C3DPoint &obj) const;
    virtual void transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const;

    virtual bool transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint &obs, double Height) const;

    virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const;

    virtual bool transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const;

    virtual void freeAffine(); //Covariance = Matrix.identity(8, 8).times(100000000);

    virtual void resetSensor();

    virtual bool read(const char* filename);

    virtual bool writeTextFile(const char* filename) const;


    /**
     * Gets a matrix of the parameters
     *
     * @return the parameters backed into a matrix
     */
	virtual bool getAllParameters(Matrix &parms) const;

	virtual bool getAdjustableParameters(Matrix &parms) const
	{
		return getAllParameters(parms);
	}

    virtual bool insertParametersForAdjustment(Matrix &parms);

    virtual bool setParametersFromAdjustment(const Matrix &parms);


    virtual Matrix getConstants() const;
    virtual void setAdjustableValues(const Matrix &parms);

	virtual int accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
		                   CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
						   CRobustWeightFunction *robust, bool omitMarked);

    virtual bool Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const;

    /**
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     * @return A Matrix of partials with respect to paramters evaluated at ob
     */
    virtual bool Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					Matrix &AiPar,        Matrix &AiPoint, 
					const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPars (Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiBiFi(Matrix &Fi, Matrix &Bi, 
						Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiPar,        Matrix &AiPoint,
						const Matrix &obs, const C3DPoint &approx);

    /**
     * @return Bi Matrix of patials with respect to observations for the ith observation
     * @param parms Column Matrix of approximations of the parameters
     * @param obs Row Matrix containg a single "joint" Observation (Each observation can be made of several part like an X, Y, Z)
     */
    virtual bool Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const;
	 
	void addDirectObservations(Matrix &N, Matrix &t);
    /**
     * @return a Vector containg the "familiar" String names of each parameter; ex. "a0", "b0", "theta"...
     */
     virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const;

     /**
      * @return a String cantainint the name of the best fit ex. "Best fit Line"
      */
     virtual void getName(CCharString& names) const;  

	 bool resectParameters(const CXYPointArray &obsArray,CXYZPointArray &controlArray);
	
	 virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const;
	 virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const;
	
	 virtual void applyReduction();

	 virtual void setReduction(const C3DPoint &red);

};


#endif