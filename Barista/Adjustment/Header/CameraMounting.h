#ifndef __CCameraMounting__
#define __CCameraMounting__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "XYZPoint.h"
#include "RollPitchYawRotation.h"
#include "Serializable.h"
#include "UpdateHandler.h"


class CCameraMounting : public CSerializable, public CUpdateHandler
{
public:
	CCameraMounting(void);
	CCameraMounting(CCharString name);
	CCameraMounting(const CCameraMounting& cameraMounting);
	
	~CCameraMounting(void);

	CCameraMounting &operator = (const CCameraMounting & cm);


	void setRotation(const CRollPitchYawRotation& rot);
	void setShift(const CXYZPoint& shift);
	void applyRotation(const CRotationMatrix& rotMat);

	const CRollPitchYawRotation &getRotation()const {return this->rotation;};
	CRollPitchYawRotation &getRotation() {return this->rotation;};

	const CXYZPoint& getShift() const {return this->shift;};
	CXYZPoint& getShift() {return this->shift;};

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified(){return this->bModified;};
	virtual void reconnectPointers();
	virtual void resetPointers();

	virtual bool isa(string& className) const
	{
	  if (className == "CCameraMounting")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CCameraMounting"); };  

	void goingToBeDeleted();

protected:

	CRollPitchYawRotation	rotation;
	CXYZPoint shift;
};
#endif