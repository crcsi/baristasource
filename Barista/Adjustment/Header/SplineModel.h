#ifndef __CSplineModel__
#define __CSplineModel__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "SensorModel.h"
#include "XYZOrbitPointArray.h"
#include "Spline.h"

class CDirectSplineObservation;

class CSplineModel : public CSensorModel
{

public:
	CSplineModel(void);
    CSplineModel(int nShiftParameters, int nRotationParameters,int nScaleParameters,int nPointObservations);	
	
	CSplineModel &operator = (const CSplineModel & splineModel);

	CSplineModel(const CSplineModel& model);
	virtual ~CSplineModel(void);

	void goingToBeDeleted();

	int getDegree() const { return this->spline.getDegree();};

	int getNSegments() const { return this->spline.getNumberOfSegments();};

    // CSerializable functions
    virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
    virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
    virtual bool isa(string& className) const;
    virtual string getClassName() const;

	void initSpline(int splineDegree,const  CFileName& name, doubles& knots);

	virtual bool hasUnknownObjPoints() { return false;};

	virtual void transformObj2Obs(C3DPoint &obs, const C3DPoint &obj) const;
    virtual void transformObj2Obs(C3DPoint &obs, const double time) const;
	virtual void transformObj2Obs(C2DPoint  &obs, const C3DPoint &obj)const;
    virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, double Height) const;
    virtual bool transformObs2Obj(C3DPoint &obj, const C3DPoint &obs) const;
	virtual bool transformObs2Obj(CObsPoint &obj, const CObsPoint  &obs, double Height, float sigmaZ) const;

	virtual bool transformObs2Obj(C3DPoint &obj, const C2DPoint  &obs, const C3DPlane &plane) const;

    virtual bool transformObs2Obj(CXYZPoint &obj, const CXYPoint  &obs, const C3DAdjustPlane &plane) const;

	virtual void setAdjustableValues(const Matrix &parms);
	virtual void setAll(const Matrix &parms);
    virtual bool read(const char *filename);
	virtual bool writeTextFile(const char* filename) const;
	
	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYPoint  &obs) const;
	virtual bool setHeigthAtXY(CXYZPoint &obj, const CXYZPoint &obs) const;

    virtual bool getAllParameters(Matrix &parms) const;
    virtual bool getAdjustableParameters(Matrix &parms) const;

    virtual bool insertParametersForAdjustment(Matrix &parms);
    virtual bool setParametersFromAdjustment(const Matrix &parms);

    virtual Matrix getConstants() const;

	virtual int accumulate(Matrix &N, Matrix &t, CObsPoint &obs, const CObsPoint &approx, 
		                   CObsPoint &discrepancy, CObsPoint &normalisedDiscrepancy, 
						   CRobustWeightFunction *robust, bool omitMarked);

    virtual bool Fi(Matrix &Fi, const Matrix &obs, const C3DPoint &approx) const;

    virtual bool Ai(Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		            Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
					Matrix &AiPar,        Matrix &AiPoint, 
					const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPars (Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                 Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						 Matrix &AiPar,  const Matrix &obs, const C3DPoint &approx) const;

    virtual bool AiPoint(Matrix &Ai, const Matrix &obs, const C3DPoint &approx) const;


    virtual bool AiBiFi(Matrix &Fi, Matrix &Bi, 
						Matrix &AiErp,        Matrix &AiRot,      Matrix &AiSca, 
		                Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
						Matrix &AiPar,        Matrix &AiPoint,
						const Matrix &obs, const C3DPoint &approx);

    virtual bool Bi(Matrix &Bi, const Matrix &obs, const C3DPoint &approx) const ;

	virtual CObsPoint calcRes(const Matrix &Delta, const CObsPoint &obs, const CObsPoint &obj);

	virtual void getParameterNames(CCharStringArray &StationNames, CCharStringArray &PointNames) const ;

	virtual void addDirectObservations(Matrix &N, Matrix &t);

	virtual double getResidualsDirectObservations(const Matrix &deltaPar, Matrix &res);

	virtual void getName(CCharString& names) const ;  

	virtual void addConstraint(Matrix* N, Matrix* t, Matrix* parms);

	void setOffsetSpline(const CObsPoint& offset) { this->offsetSpline = offset;};

	CObsPoint getOffsetSpline() const {return this->offsetSpline;};
		
	virtual void crop(int offsetX, int offsetY, int newWidth, int newHeight);

	bool evaluate(C3DPoint& location, C3DPoint& velocity,const double time) 
	{
		bool retLocation = this->spline.evaluateLocation(location,time);
		bool retVelocity = this->spline.evaluateVelocity(velocity,time);
		location += this->offsetSpline;				
		location *= this->i_scale;
		return retLocation && retVelocity;
	}

	bool evaluateLocation(C3DPoint& location,const double time) const
	{
		bool retLocation = this->spline.evaluateLocation(location,time);
		location += this->offsetSpline;
		location *= this->i_scale;
		return retLocation;
	}

	virtual void extend_N_and_t_Matrix(Matrix& N,Matrix& t,
											const CAdjustMatrix& AxTP,
											const CAdjustMatrix& AyTP, 
											const CAdjustMatrix& AzTP,
											const CAdjustMatrix& P,
											const CAdjustMatrix& dl,
											const int indexNx,
											const int indexNy,
											const int indexNz,
											const CObsPoint &obs)const {};

	virtual void extend_Ai(	Matrix &AiErp,Matrix &AiRot,Matrix &AiSca, 
							Matrix &AiMountShift, Matrix &AiMountRot, Matrix &AiIrp, 
							Matrix &AiAdd, Matrix &AiPoint,const Matrix &obs, const C3DPoint &approx)const{};

	virtual void extend_Bi(	Matrix& Fi,const Matrix &obs, const C3DPoint &approx)const{};

	virtual bool Fi(CObsPoint& Fi, const CObsPoint &obs) const;
	virtual void computePMatrix(const Matrix& Qobs, CAdjustMatrix& P)const;

	virtual void computeParameterCount();

	 virtual CCharString obsUnitsString() const { return "[m]"; };

	 void setScale(double s) {this->scale = s;
							  this->i_scale = 1.0 / this->scale;};
	
	double getScale() const {return this->scale;};

	// function initializes the vector of direct observations
	virtual void initDirectObservations();

	// function to set everything in one step
	void setDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,const doubles& observations,const doubles& sigmas);
	void setDirectObservationsForSpline(const vector<CDirectSplineObservation>& directObservations);

	const vector<CDirectSplineObservation>& getDirectObservationsForSpline() const {return this->directObservations;} ;
	
	void setDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,unsigned int segment,double observation);
	void setSigmaDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,unsigned int segment,double sigma);

	double getDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,unsigned int segment) const;
	double getSigmaDirectObservationsForSpline(unsigned int degree,unsigned int coordinate,unsigned int segment) const;

	void setUseDirectObservationForSpline(unsigned int degree,unsigned int coordinate, bool b);
	void setUseDirectObservationsForSpline(bool b) { this->useDirectObservationsForSpline = b;};

	bool getUseDirectObservationForSpline(unsigned int degree,unsigned int coordinate) const;
	bool getUseDirectObservationsForSpline() const { return this->useDirectObservationsForSpline;};

	int getNDirectObservationsForSpline() const;

	void getSplineParameter(int segmentIndex,int degree, double& parameterX,double& parameterY,double& parameterZ);

	double getSigmaSplineParameter(unsigned int index) const;


	double getFirstTime();

	double getLastTime();
	
protected:

	CObsPoint offsetSpline;
	CSpline spline;
	double scale;
	double i_scale;

	bool useDirectObservationsForSpline; // flag, used to distinguish between 'normal' and generic initialization of the model

	vector<CDirectSplineObservation> directObservations;

};










class CDirectSplineObservation : public CSerializable
{
friend class CSplineModel;

public:

	CDirectSplineObservation();
	~CDirectSplineObservation();

	CDirectSplineObservation(const CDirectSplineObservation& obs);
	
	CDirectSplineObservation &operator = (const CDirectSplineObservation & obs);

	// CSerializable functions
    virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
    virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual void reconnectPointers(){};
	virtual void resetPointers(){};

	virtual bool isModified() {return false;};

	bool isa(string& className) const
	{
		if (className == "CDirectSplineObservation")
			return true;
		return false;
	}

	string getClassName() const
	{
		return string("CDirectSplineObservation");
	}

protected:

	bool useObservation[3];
	doubles observation[3];
	doubles sigma[3];
};


#endif