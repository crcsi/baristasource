#ifndef __CVImage__
#define __CVImage__

#include "VBase.h"
#include "Filename.h"

#include "XYZPointArray.h"
#include "XYContourArray.h"

#include "XYZPolyLine.h"
#include "XYZPolyLineArray.h"
#include "XYPolyLine.h"
#include "XYEllipseArray.h"
#include "Building.h"
#include "BuildingArray.h"
#include "XYPointPtrArray.h"

#include "HugeImage.h"
#include "Affine.h"
#include "RPC.h"
#include "TFW.h"
#include "PushbroomScanner.h"
#include "BundleModel.h"
#include "OrbitObservationsArray.h"
#include "FrameCameraModel.h"


enum eImageSensorModel
{ 
	eUNDEFINEDSENSOR = 0,
	eRPC,
	eTFW,   
    eAFFINE,
	ePUSHBROOM,
	eFRAMECAMERA
};

class CVImage : public CObservationHandler, public CVBase
{
public:
    CVImage(void);
    CVImage(CVImage* src);
    CVImage(const CVImage& src);
    ~CVImage(void);

	void setMeasuredPointsActive(bool b) {this->xypoints.setActive(b);};

	//addition>>
	void setXY3DResidualsActive(bool b) {this->xy3DResiduals.setActive(b);};
	CXYPointArray* getXY3DResiduals(){return &this->xy3DResiduals;};
	//<<addition
	CLabel getCurrentLabel();
	void setCurrentLabel(CLabel newlabel);
	void incrementCurrentLabel();

	void makeXYContoursActive();
	void thinOutXYContours();

	void regionGrowing(const C2DPoint &p);

	float getApproxGSD(float height) const;

	bool initRPCs(const CFileName &filename);
	bool initRPCs(const CRPC &rpc);
	double initRPCs(const CPushBroomScanner &pushbroom, float Zmin, float Zmax);

	bool initAffine(const CFileName &filename, const CReferenceSystem &refsys);
	bool initAffine(const CAffine &affine);

	bool initTFW(const CFileName &filename, const CReferenceSystem &refsys);
	bool initTFW(const CTFW &tfw);


	bool initPushBroomScanner(const CFileName& filename,
							CCCDLine* camera, 
							CCameraMounting* cameraMounting, 
							CSensorModel* path,
							CSensorModel* attitudes,
							double satelliteHeight,
							double groundHeight,
							double groundTemperature,
							double firstLineTime,
							double timePerLine,
							int activeMountingShiftParameter,
							int activeMountingRotationParameter,
							int activeIrpParameter,
							int activeAdditionalParameter);
							//int type); //addition: int type

	bool initGenericPushBroomScanner(const CFileName& filename,
									CCCDLine* camera, 									
									double satelliteHeight,
									double groundHeight,
									double groundTemperature,
									double inTrackViewAngle,
									double crossTrackViewAngle,
									C3DPoint UL,
									C3DPoint UR,
									C3DPoint LL,
									C3DPoint LR);



	bool initFrameCamera(const CFileName& filename,
						CFrameCamera* camera,
						CCameraMounting* cameraMounting,
						CXYZPoint cameraPos,
						CRotationBase* cameraRotation);

    CXYPointArray* getXYPoints(){return &this->xypoints;};
    CXYPointArray* getProjectedXYPoints(){return &this->projectedxypoints;};
    CXYPointArray* getDifferencedXYPoints(){return &this->differencedxypoints;};
    CXYPointArray* getResiduals(){return &this->residuals;};
	CXYContourArray* getXYContours(){return &this->xycontours;};
	CXYPolyLineArray* getXYEdges(){return &this->xyedges;};
	CXYEllipseArray* getEllipses(){return &this->ellipses;};


    virtual CHugeImage* getHugeImage();

	bool hasAffine() const { return this->affine.gethasValues(); };
	bool hasRPC() const { return this->rpc.gethasValues(); };
	virtual bool hasTFW() const { return this->tfw.gethasValues(); };
	bool hasPushbroom() const {return this->pushbroom.gethasValues();};
	bool hasFrameCamera() const { return this->frameCamera.gethasValues();};

	bool hasSensorModel(bool considerTFW) const;

	const char *affineFileName() const { return this->affine.getFileNameChar(); }
	const char *rpcFileName()    const { return this->rpc.getFileNameChar(); }
	const char *oriRPCFileName()    const { return this->originalrpc.getFileNameChar(); }
	const char *tfwFileName()    const { return this->tfw.getFileNameChar(); }
	const char *pushBroomScannerFileName() const {return this->pushbroom.getFileNameChar();};
	const char *frameCameraFileName() const {return this->frameCamera.getFileNameChar();};

    CRPC* getRPC(){return &this->rpc;};
	CRPC* getOriginalRPC(){return &this->originalrpc;};
	CPushBroomScanner *getPushbroom(){return &this->pushbroom;};
	virtual const CTFW* getTFW() const{return &this->tfw;};
	CAffine* getAffine(){return &this->affine;};
	CFrameCameraModel* getFrameCamera() {return &this->frameCamera;};

	void resetSensorModel(CSensorModel *sensorMod);

    bool isa(string& className) const
	{
		if (className == "CVImage")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CVImage");
	};

    // CSerializable
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    void resetRPC();
	void eraseRPC();

	bool setCurrentSensorModel (eImageSensorModel newmodel);
	eImageSensorModel getCurrentSensorModelType() {return this->currentSensorModelType;};

	// cropping function for VImage
	bool cropfromImage(CVImage* oriImage, double ulx, double uly, double width, double height,
		bool cropPoints = 0, bool cropRPC = 0, bool cropTFW = 0, bool cropPushBroom = 0, bool cropAffine = 0);

	bool cropTFW(CVImage* oriImage, double ulx, double uly, double width, double height);
	bool cropRPC(CVImage* oriImage, double ulx, double uly, double width, double height);
	bool cropPoints(CVImage* oriImage, double ulx, double uly, double width, double height);
	bool cropPushBroom(CVImage* oriImage, double ulx, double uly, double width, double height);
	bool cropAffine(CVImage* oriImage, double ulx, double uly, double width, double height);

	virtual const char *getFileNameChar() const {return this->name.GetChar();};

	virtual const CSensorModel* getModel(enum eLSMTRAFOMODE trafomode = eLSMNOTFW);

	int getNFiducialmarkObs() const {return this->fiducialMarkObs.GetSize();};
	CXYPointArray* getFiducialMarkObs() {return &this->fiducialMarkObs;};

	bool computeInnerOrientation();
	
	virtual void setTFW(const CTFW& tfw) { this->initTFW(tfw); };

	bool getOptimalPyrLevel(float Z, double GSD, int &level, int &factor) const;

	void goingToBeDeleted();

	bool getFootPrint(C3DPoint &upperLeftObj,  C3DPoint &upperRightObj,
		              C3DPoint &lowerRightObj, C3DPoint &lowerLeftObj, 
					  double Z, CReferenceSystem *targetRefsys) const;
	bool getFootPrint(C2DPoint &upperLeftObj,  C2DPoint &upperRightObj, 
		              C2DPoint &lowerRightObj, C2DPoint &lowerLeftObj, 
					  double Z, CReferenceSystem *targetRefsys) const;

	bool getFootPrintMinMax(C3DPoint &pMinObj, C3DPoint &pMaxObj,
		                    double Z, CReferenceSystem *targetRefsys) const;
	bool getFootPrintMinMax(C2DPoint &pMinObj, C2DPoint &pMaxObj,
		                    double Z, CReferenceSystem *targetRefsys) const;

	bool getOverlapArea(const C3DPoint &pMinObj, const C3DPoint &pMaxObj,
		                C2DPoint &pMinImg, C2DPoint &pMaxImg) const;

	bool hasValidHugeImage();

protected:
	bool initFiducialMarks(const CXYPointArray* fiducialMarks);  
	// CFileName imageFileName;

    CXYPointArray xypoints;
	//addition>>
	CXYPointArray xy3DResiduals;
	//<<addition
    CXYPointArray projectedxypoints;
    CXYPointArray differencedxypoints;
	CXYPointArray residuals;
	CXYPointArray fiducialMarkObs;


    CXYPointPtrArray labelSorted;

	CXYContourArray xycontours;
	CXYPolyLineArray xyedges;
	CXYEllipseArray ellipses;
    CHugeImage hugeImage;

    CRPC rpc;
    CRPC originalrpc;

	CTFW tfw;
    CAffine affine;
	CPushBroomScanner pushbroom;

	CFrameCameraModel frameCamera;

	CLabel currentLabel;
	eImageSensorModel currentSensorModelType;
};

#endif

