#ifndef CEntityDrawing_H
#define CEntityDrawing_H

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include <vector>
#include "DrawingDataSet.h"

class CObsPoint;
class CXYPolyLine;
class CXYZPolyLine;

extern int BARISTA_RED[3];
extern int BARISTA_GREEN[3];
extern int BARISTA_BLUE[3];

extern int BARISTA_MAGENTA[3];
extern int BARISTA_YELLOW[3];
extern int BARISTA_CYAN[3];

extern int BARISTA_WHITE[3];
extern int BARISTA_BLACK[3];

class CEntityDrawing 
{

protected:

	// search function
	bool findPos(const CLabel& dataSetName,int &pos);


public:	
	CEntityDrawing(void);
	virtual ~CEntityDrawing(void);

	// functions to add new datasets
	bool addDataSet(CDrawingDataSet& dataSet);
//	bool addDataSet(const CLabel& dataSetName,int dim, bool showPoints = true, bool showPointLabels = true, QColor pointColor = Qt::green);
	bool addDataSet(const CLabel& dataSetName,int dim, bool showPoints, bool showPointLabels);
	bool addDataSet(const CLabel& dataSetName,int dim, bool showPoints, bool showPointLabels, int *pointColor);

	// functions to remove single or all datasets
	bool removeDataSet(const CLabel& dataSetName);
	void removeAllDataSets();

	// functions to add or remove a point to a dataset
	bool addPoint(const CLabel& dataSetName,CObsPoint* entity);
	bool removePoint(const CLabel& dataSetName,const CLabel& point);

	bool queryPoint(const CLabel& dataSetName,const CLabel & pointLabel);


	// functions to add or remove a polyline
	// topological connections are added and removed too
	bool addPolyLine(const CLabel& dataSetName,CXYPolyLine* entity,const int *color,unsigned int lineWidth);
	bool removePolyLine(const CLabel& dataSetName,CXYZPolyLine* entity);

	// functions to add and remove connections
	bool addTopology(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2, int *color,unsigned int lineWidth);
	bool removeTopology(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2);


	int getNDataSets() {return this->datasets.size();};
	CDrawingDataSet* getDataSet(int index);


	// querry functions for drawing
	bool getShowPoints(const CLabel& dataSetName);
	void setShowPoints(const CLabel& dataSetName,bool b);

	bool getShowPointLabels(const CLabel& dataSetName);
	void setShowPointLabels(const CLabel& dataSetName,bool b);

	int *getPointColor(const CLabel& dataSetName);
	void setPointColor(const CLabel& dataSetName,int *color);

	// change color
	void changeTopologyColor(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2,int *color);
	void changeAllTopologyColors(const CLabel& dataSetName, int *color);

	// change line width
	void changeTopologyLineWidth(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2,unsigned int lineWidth);
	void changeAllTopologyLineWidth(const CLabel& dataSetName,unsigned int lineWidth);

	// rename dataSet
	bool renameDataSet(const CLabel& oldName,const CLabel& newName);

protected:
	vector<CDrawingDataSet*> datasets;
};

#endif