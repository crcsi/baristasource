#ifndef __COrthoPhotoRecord__
#define __COrthoPhotoRecord__



#include "VImage.h"
#include "2DPoint.h"

#define DEFAULT_GSD 1.0

class CImageExporter;

class COrthoPhotoRecord
{
friend class COrthoPhotoVector;
protected:

	   CVImage ortho;

	   CVImage *img;

	   CImageExporter* imageExporter;

	   C2DPoint LLC;

	   C2DPoint RUC;

	   C2DPoint GSD;

	   bool isSelected;

	   CCharString errorMessage;

	   CCharString errorTitleMessage;

	   //addition >>
		C3DPoint ULin;
		C3DPoint URin;
		C3DPoint LLin;
		C3DPoint LRin;

		C3DPoint ULout;
		C3DPoint URout;
		C3DPoint LLout;
		C3DPoint LRout;

		bool isUTM; //true for UTM & false for Geographic
	   //<< addition
public:

   	   COrthoPhotoRecord();

	   COrthoPhotoRecord(const COrthoPhotoRecord &rec);

	   ~COrthoPhotoRecord();

	   const char *getOrthoFileNameChar() const { return this->ortho.getFileNameChar(); };

	   CFileName getOrthoFileName() const { return CFileName(this->getOrthoFileNameChar());};

	   CFileName getOrthoHugeFileName() { return CFileName(*this->ortho.getHugeImage()->getFileName());};

	   CVImage *getOrtho() { return &(this->ortho); };

	   CVImage *getInputImg() const { return this->img; };

	   CImageExporter& getImageExporter();
	   
	   void setOrthoFileType(const CCharString &extension);

	   bool exportOrtho();

	   void setExtents(C2DPoint newLLC,C2DPoint newRUC);

	   void setLLC(C2DPoint newLLC); 
	   C2DPoint getLLC() const { return this->LLC;};
	   
	   void setRUC(C2DPoint newRUC); 
	   C2DPoint getRUC() const { return this->RUC;};

	   void setGSD(double newX, double newY);
	   C2DPoint getGSD() const { return this->GSD;};

	   void setIsSelected(bool b) { this->isSelected = b;};
	   bool getIsSelected() const { return this->isSelected; };

	  CCharString getErrorMessage() const { return this->errorMessage;};
	  CCharString getErrorTitleMessage() const { return this->errorTitleMessage;};

	  void setOrthoPhotoName(CFileName newOrthoName);

	   void clearExporter();

	   //addition >>
	   bool getCoordinateType() {return this->isUTM;};
	   void setCoordinateType(bool ctype) {this->isUTM = ctype;};
	   //<< addition
protected:

	    void setImg(CVImage *inputImage, bool initOrthoname);

	   void initOrthoName();

	   void initExporter();


	
};


#endif // __COrthoPhotoRecord__
