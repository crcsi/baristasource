#ifndef __CBaristaProject__
#include "Serializable.h"
#include "XYPoint.h"
#include "XYZPoint.h"
#include "VImageArray.h"
#include "XYZPointArrayArray.h"
#include "VDEMArray.h"
#include "VALSArray.h"
#include "VControl.h"
#include "XYZPolyLineArray.h"
#include "BuildingArray.h"
#include "ObservationHandler.h"
#include "ProgressHandler.h"
#include "CameraArray.h"
#include "CameraMountingArray.h"
#include "MatchingParameters.h"
#include <ANN/ANN.h>

//addition on 16 Feb 2010 >>
#include "GCPDataBasePoint.h"
#include "GCPData2DPoint.h"
//<< addition

class CG3DFile;
class CASCIIDEMFile;
class CGDALFile;
class CStatusBarHandler;
class CDrawingSetting;
class CPushBroomScanner;
class COrbitObservations;
class CGCPDataBasePoint;

//addition>>
enum shwLabel
{
	//lNoLabel = 0,
	lOn,
	lOff
};
enum shwPoint
{ 
	pNoPoint = 0,
	pCross,	
	pDot
};
enum shwResidual
{ 
	rNoResidual = 0,
	rImgxy,
	rImgDiff,
	rObXY,
	rObHt,   
};
//<<addition

class CBaristaProject : public CSerializable , public CProgressHandler
{
public:
	CBaristaProject();
	~CBaristaProject();

	// CSerializable functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	void reconnectPointers();
	void resetPointers();

    bool isa(string& className) const
	{
		if (className == "CBaristaProject")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CBaristaProject");
	};

	bool isModified();
	// end of CSerializable functions

    CFileName* getFileName();
    void setFileName(const char* filename);

	CCharString getNewXYZPointArrayName(CCharString name);

// =*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	// project management

	bool initProject();
	bool close();
    void writeProject(const char* pFileName);
    void readProject(const char* pFileName);

	//bool getIsSaved() const {return this->saved;};


	CVImage *getSelectedCVImage() const { return this->selectedCVImage; }; //used for ortho generation
	
	void initExteriorOrientationOfFrameCameras(const char *filename, bool rotate180);

	int findImageGroups(vector<vector<CVImage*> >& newImages,vector<CCharString>& names, int minNumberOfImagesPerGroup);

	void setSelectedCVImage(CVImage *current)  { this->selectedCVImage = current; }; //used for ortho generation
	void resetSelectedCVImage()  { this->selectedCVImage = 0; }; //used for ortho generation



	// get/set meanHeight
	double getMeanHeight() const { return this->meanHeight; }
	void setMeanHeight(double height) { this->meanHeight = height; }

	// get/set MonoplottedLineColor
	C3DPoint getMonoplottedLineColor();
	void setMonoplottedLineColor(C3DPoint color);

	// get/set MonoplottedLineWidth
	int getMonoplottedLineWidth();
	void setMonoplottedLineWidth(int width);

	// get/set BuildingLineColor
	C3DPoint getBuildingLineColor();
	void setBuildingLineColor(C3DPoint color);

	// get/set BuildingLineWidth
	int getBuildingLineWidth();
	void setBuildingLineWidth(int width);

	// get/setMeanHeightMonoplotting
	bool getMeanHeightMonoplotting() const { return this->meanHeightMonoplotting; };
	void setMeanHeightMonoplotting(bool allow) { this->meanHeightMonoplotting = allow; };

	bool getHeightPointMonoplotting() const { return this->heightPoint; };
	void setHeightPointMonoplotting(bool allow) { this->heightPoint = allow; };

	bool getHeightDEMMonoplotting() const { return this->heightDEM; };
	void setHeightDEMMonoplotting(bool allow) { this->heightDEM = allow; };



	//addition>> by Awrangjeb Apr 2009
	shwLabel getLblShowType() {return this->lblType;};
	shwPoint getPntShowType() {return this->pntType;};
	shwResidual getResShowType() {return this->resType;};
	int *getLblColor() {return this->lColor;};
	int *getPntColor() {return this->pColor;};
	int *getResColor() {return this->rColor;};
	int getDotWidth() {return this->dotWidth;};
	int getLnWidth() {return this->lnWidth;};
	bool isShowProjectedPoint() {return this->showProjPoint;};
	double getScaleValue() {return this->scaleValue;};
	bool isShowScale() {return this->showScale;};
	double getGSDValue() {return this->gsdValue;};
	double getProjectGSDValue();

	void setLblShowType(shwLabel lType) {this->lblType = lType;};
	void setPntShowType(shwPoint pType) {this->pntType = pType;};
	void setResShowType(shwResidual rType) {this->resType = rType;};
	void setLblColor(int * color);
	void setPntColor(int * color);
	void setResColor(int * color);
	void setDotWidth(int wd) {this->dotWidth = wd;};
	void setLnWidth(int wd) {this->lnWidth = wd;};
	void setShowProjectedPoint(bool flag) {this->showProjPoint = flag;};
	void setScaleValue(double scale) {this->scaleValue = scale;};
	void setShowScale(bool show) {this->showScale = show;};
	void setGSDValue(double gsd) {this->gsdValue = gsd;};
	//<<addition

//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=* 
// Access to data of project

// Get number of images / DEMs / XYZPointArrays

	int getNumberOfImages() const { return this->images.GetSize(); }

	int getNumberOfImagesWithFrameCam() const;

	int getNumberOfDEMs() const { return this->dems.GetSize(); }

	int getNumberOfXYZPointArrays() const { return this->xyzArrays.GetSize(); }

	int getNumberOfAlsDataSets() const { return this->alsDataSets.GetSize(); }

	int getNumberOfOrbits() const { return this->orbits.GetSize();}

	int getNumberOfMonoplottedPoints() const { return this->xyzMonoplottedPoints.GetSize();}

	int getNumberOfMonoplottedLines() const { return this->xyzpolylines.GetSize();}

	// addition >>
	int getNumberOfMonoplottedHeightLines() const { return this->xyzBheights.GetSize();}
	CXYZPolyLineArray *getXYZHeightLines() { return &this->xyzBheights; }
	//<< addition 

	int getNumberOfMonoplottedBuildings() const 
	{
#ifdef WIN32
		return this->buildings.GetSize();
#else
		return 0;
#endif
		}

	int getNumberOfGeoreferencedPoints() const { return this->xyzGeoreferencedPoints.GetSize();}

	int getNumberOfCameras() const {return this->cameras.GetSize();};

	int getNumberOfCameraMountings() const {return this->cameraMountings.GetSize();};


//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=* 
// Get image / DEM / XYZPointArray / control points / Buildings / XYZPolyLines

	CVControl* getControlPoints() const { return this->controlPoints; };

	CXYZPointArray* getCheckPoints() const;

	CVImage *getImageAt(int index) const { return this->images.GetAt(index); };

	CVImage *getImageByName(CCharString* name) const { return this->images.getImagebyName(name); };

	int getImageIndex(CCharString* name) const { return this->images.getbyName(name); };

	int getImageIndex(CVImage* image) const { return this->images.indexOf(image); };

	CVDEM *getDEMByName(const CCharString &name) const { return this->dems.getDEMbyName(name); };

	CVDEM *getDEMAt(int index) const { return this->dems.GetAt(index); }

	int getDEMIndex(CVDEM* dem) const { return this->dems.indexOf(dem); };

	CVALS *getALSAt(int index) const { return this->alsDataSets.GetAt(index); }

	CXYZPointArray *getPointArrayAt(int index) const { return this->xyzArrays.GetAt(index); }

	CXYZPointArray *getPointArrayByName(const CCharString &name) const { return this->xyzArrays.getPointArray(name); };

	CXYZPointArray* getPointResidualArrayAt(int index) { return this->xyzResidulaArray.GetAt(index);};
	
	CXYZPointArray* getPointResidualArrayOfXYZPointArray(const CXYZPointArray* xyzPointArray) const;

	COrbitObservations* getOrbitObservationAt(int index) const { return this->orbits.GetAt(index);}

	COrbitObservations* getOrbitObservation(const COrbitPathModel* path) const;

	COrbitObservations* getOrbitObservation(const COrbitAttitudeModel* attitude) const;



	void setUseOrbitOvsevationHandler(const CSensorModel* sensorModel, bool use);

	CCamera* getCameraAt(int index) const {return this->cameras.GetAt(index);};
	CCameraMounting* getCameraMountingAt(int index) const {return this->cameraMountings.GetAt(index);};

	CXYZPolyLineArray *getXYZPolyLines() { return &this->xyzpolylines; }
    CXYZPointArray* getMonoplottedPoints() {return &this->xyzMonoplottedPoints;};
    CXYZPointArray* getGeoreferencedPoints() {return &this->xyzGeoreferencedPoints;};

#ifdef WIN32	
	CBuildingArray *getBuildings() { return &this->buildings; }
	CBuilding *setSelectedBuilding(int index);
	CBuilding *selectCurrentBuilding();
#endif
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
// Set / unset Active DEM
	CVDEM* getActiveDEM() const { return this->activeDEM; }
	void setActiveDEM(CVDEM* dem);
	void unsetActiveDEM(); 
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

	// Add image / DEM / XYZPointArray

	CVImage *addImage(const CFileName& filename, bool open);
		
	CVDEM* addDEM(CG3DFile &imageFile, const CReferenceSystem& refSys);
	CVDEM* addDEM(CASCIIDEMFile &imageFile, const CReferenceSystem& refSys);
	CVDEM* addDEM(CGDALFile& imageFile, const CReferenceSystem& refSys);
	CVDEM* addDEM(const CFileName& filename);

	CVALS* addALSStrips(const CFileName& filename, eALSFormatType eFormat, eALSPulseModeType ePulseType,
		                const CReferenceSystem& refSys, const C2DPoint &offset);

	CVALS* addALSTiles(const CFileName& filename, eALSFormatType eFormat, eALSPulseModeType ePulseType,
		               const CReferenceSystem& refSys, const C2DPoint &offset, const C2DPoint &delta);

	CVALS *addALS();

	CXYZPointArray* addXYZPoints(const CFileName &filename,
								 CReferenceSystem refSys, 
								 bool checkUnique, bool read,XyzPointType type);

	COrbitObservations* addOrbitObservation(SplineModelType type);

	bool addMonoplottedPoint(const CXYZPoint& point);
	bool addMonoplottedLine(const CXYZPolyLine& line);
	//addition >>
	bool addBheightLine(const CXYZPolyLine& line);
	//<< addition
#ifdef WIN32
	int addMonoplottedBuilding(const CBuilding& building, CStatusBarHandler *handler);
#endif
	CCamera* addCamera(CameraType type);
	CCamera* addCamera(const CCamera* camera);

	CCameraMounting* addCameraMounting();

	bool removeMonoplottedPoint(const CXYZPoint& point);
	bool removeMonoplottedPoint(int index,int count);

	bool removeMonoplottedLine(const CXYZPolyLine& line);
	bool removeMonoplottedLine(int index,int count);
	//addition >>
	bool removeBheightLine(const CXYZPolyLine& line);
	bool removeBheightLine(int index,int count);
	//<<addition
#ifdef WIN32
	bool removeMonoplottedBuilding(const CBuilding& building);
	bool removeMonoplottedBuilding(int index,int count);
#endif

	void addGeoreferencedPoint(const CXYZPoint& point);
	void removeGeoreferencedPoint(const CXYZPoint& point);
	void removeGeoreferencedPoint(int index,int count);

	void deleteMonoplottedPoints(CXYZPointArray* points);
	void deleteGeoreferencedPoints(CXYZPointArray* points);

	void setGeoreferencedPointReferenceSystem(const CReferenceSystem& ref);

	void setMonoplottedPointReferenceSystem(const CReferenceSystem& ref);
	void setMonoplottedLineReferenceSystem(const CReferenceSystem& ref);
	void setMonoplottedBuildingReferenceSystem(const CReferenceSystem& ref);
	//addition >>
	void setBheightLineReferenceSystem(const CReferenceSystem& ref);
	//<< addition
	void removeRelated2DPoints(const CLabel& label,xypointtype type);

//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

	// Delete image / DEM / XYZPointArray / Buildings / XYZPolyLines

	void deleteImage(CVImage *image);

	void deleteImageAt(int index);

	void deleteDEM(CVDEM *dem);

	void deleteXYZPoints(CXYZPointArray* points);

	void deleteOrbitObservation(CFileName& filename);
	void deleteOrbitObservation(COrbitObservations* orbit);

	void deleteCamera(CFileName& filename);
	void deleteCamera(CCamera* camera);

	void deleteCameraMounting(CFileName& filename);
	void deleteCameraMounting(CCameraMounting* cameraMounting);

	void deleteALS(CVALS *als);
	void deleteALSAt(int index);

	void deleteXYZPolyLines();
	void deleteBuildings();

	//addition >>
	void deleteXYZHeightLines();
	//<< addition
//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	// Rename/change image / DEM / XYZPointArray / Buildings / XYZPolyLines

	bool renameImage(CVImage *image, const char *newName);

	bool renameDEM(CVDEM *dem, const char *newName);

	bool renameALS(CVALS *als, const char *newName);

	bool renameXYZPointArray(CXYZPointArray *points, const char *newName);

	bool renameOrbitObservation(COrbitObservations* orbit, const char *newName);

	bool setCurrentSensorModelofImage(CVImage *image, eImageSensorModel newmodel);

	bool deleteSensorModelofImage(CVImage *image, CSensorModel *deletemodel);
	bool deleteSensorModelofDEM(CVDEM *dem, CSensorModel *deletemodel);

	bool renameBuildings(const char *newName);
	bool renameXYZPolyLines(const char *newName);

	bool renameCamera(CCamera* camera ,const char *newName);

	bool renameCameraMounting(CCameraMounting* cameraMounting ,const char *newName);


//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
// Set / unset control points

	void setControlPoints(CXYZPointArray* points);
	void unsetControlPoints(CXYZPointArray* points);
	

	void setCheckPoints(CXYZPointArray* points);
	void unsetCheckPoints(CXYZPointArray* points);

//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
	// loop over images
 
	void RPCBiasCorrectionNone();

	void RPCBiasCorrectionShift();

	void RPCBiasCorrectionShiftDrift();

	void RPCBiasCorrectionAffine();

	void RPCRegenerateBiasCorrected();

	void resetRPCtoOriginal();



	CLabel getCurrentPointLabel();
	void setCurrentPointLabel(CLabel newPointLabel);
	void setCurrentXYPointLabelForAllImages(CLabel newPointLabel);
	void incrementCurrentPointLabel();

	CLabel getCurrentLineLabel();
	void setCurrentLineLabel(CLabel newLineLabel);
	void incrementCurrentLineLabel();

	//addition >>
	CLabel getCurrentBheightLabel();
	void setCurrentBheightLabel(CLabel newLineLabel);
	void incrementCurrentBheightLabel();
	//<< addition

	CLabel getCurrentBuildingLabel();
	void setCurrentBuildingLabel(CLabel newBuildingLabel);
	void incrementCurrentBuildingLabel();

	void setCurrentBuildingIndex(int index){this->currentBuildingIndex = index;};
	int getCurrentBuildingIndex(){return this->currentBuildingIndex;};

	bool getUniqueImageName(CCharString& uniqueName);
	bool getUniqueCameraName(CCharString& uniqueName);
	bool getUniqueCameraMountingName(CCharString& uniqueName);
	bool getUniqueOrbitPathName(CCharString& uniqueName);
	bool getUniqueOrbitAttitudeName(CCharString& uniqueName);

	bool checkforUniquePointLabel(CLabel newPointLabel);
	bool checkforUniqueBuildingLabel(CLabel newLabel);



	void resetRobustErrors();
	bool getFixedRotationForOrbit(CRotationMatrix& fixedMatrix,const CCharString& orbitModelName);
	CPushBroomScanner* getPushBroomScannerForOrbit(const COrbitObservations* orbit);

	void setMatchingParameter(const CMatchingParameters parameters) 
	{ 
		this->matchingParameters = parameters;
	};
	CMatchingParameters getMatchingParameter() const 
	{
		return this->matchingParameters;
	};

	COrbitObservations* findExistingOrbitPath(CCharString newName);
	COrbitObservations* findExistingOrbitAttitudes(CCharString newName);
	CCameraMounting* findExistingCameraMounting(CCharString newName);
	CCameraMounting* findExistingCameraMounting(CCameraMounting* newCameraMounting);
	CCamera* findExistingCamera(CCharString newName);
	bool mergeOrbits(vector<CPushBroomScanner*> scanner);

	bool differenceByLabelXYZ(CXYZPointArray* resultArray, CXYZPointArray* referenceArray, CXYZPointArray* compareArray,bool metrical) const;
	bool addCheckPointsToControlPoints(CXYZPointArray* checkPointArray);
	bool removeChekPointsFromControlPoints(CXYZPointArray* checkPointArray);

	bool backProjectXYZPoints(vector<CVImage*> images,CXYZPointArray& xyzpoints,CCharString& errorMessage,bool onlyPointsInImage,bool keepExistingPoints);

//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

	void insertSatelliteFootprintsToLines(CReferenceSystem refsys);

	bool readGCPPointDataBase(CFileName dataBaseFileName,vector<CGCPDataBasePoint>& dataBase);

	bool matchGCPDataBasePoints(const vector<CGCPDataBasePoint>& dataBase, const CMatchingParameters &pars,CReferenceSystem refSys);
 
	//addition>>
	// GCP chips
	CFileName* getChipDbFileName();
    void setChipDbFileName(const char* filename);
	void saveGCPDbase();
	bool openGCPDbase(CFileName& filename);
	unsigned long getCurrentChipID();
    void setCurrentChipID(unsigned long cid);
	void addToChipDB(CGCPData2DPoint P2d, CGCPDataBasePoint P3d);
	//<<addition
	void setDefaultGCPvalues();
	CGCPDataBasePoint getGCPpoint();
	void setGCPpoint(CGCPDataBasePoint P);
	vector <CGCPDataBasePoint> *getGCPdb();
	vector <bool> *getChipSelectionArray();
	void setChipSelectionArray(vector <bool> *SelectionArray);
	void addToChipSelectionArray(bool flag);

	//void selectGCPPoints(vector <CGCPDataBasePoint> gcpDB, ANNidxArray &nnIdx);
	int *selectGCPPoints(CXYZPointArray* dBPoints);
	//int compare (const void * a, const void * b);
	bool isInsideQuadrangle(const double P1 [], const double P2 [], const double P3 [], const double P4 [], double P []);

	int getNumberOfGCPchipPoints() const { return this->xyzGCPchipPoints.GetSize();}
	CXYZPointArray* getGCPchipPoints() {return &this->xyzGCPchipPoints;};	

	void addGCPchipPoint(const CXYZPoint& point) {this->xyzGCPchipPoints.Add(point);};
	void removeGCPchipPoint(const CXYZPoint& point);
	void removeGCPchipPoint(int index,int count);	
	void deleteGCPchipPoints(CXYZPointArray* points);
	void setGCPchipPointReferenceSystem(const CReferenceSystem& ref) {this->xyzGCPchipPoints.setReferenceSystem(ref);};
	void setUseWallis4ChipMatching(bool useWallis) {this->useWallis4ChipMatching = useWallis;};
	bool getUseWallis4ChipMatching() {return this->useWallis4ChipMatching;};

protected:

    CFileName projectFileName;

    CVImageArray images;

    CVImage *selectedCVImage; //used for ortho generation

    CXYZPointArrayArray xyzArrays;

	CXYZPointArrayArray xyzResidulaArray;

	CVDEMArray dems;

    CVALSArray alsDataSets;

	COrbitObservationsArray orbits;

	CVControl *controlPoints;

#ifdef WIN32
	CBuildingArray buildings;
#endif

	CXYZPolyLineArray xyzpolylines;

	CVDEM* activeDEM;

    CXYZPointArray xyzMonoplottedPoints;
    CXYZPointArray xyzGeoreferencedPoints;

	CLabel currentPointLabel;
	CLabel currentLineLabel;
	CLabel currentBuildingLabel;

	int currentBuildingIndex;

	CCameraArray cameras;
	CCameraMountingArray cameraMountings;

	// project settings
	double meanHeight;
	bool meanHeightMonoplotting;
	bool heightPoint;
	bool heightDEM;

	int lineWidth;
	C3DPoint lineColor;

	CMatchingParameters matchingParameters;

	vector<CDrawingSetting> drawingSettings;

	//addition>>
	//show label
	shwLabel lblType;
	shwPoint pntType;
	shwResidual resType;
	int lColor [3]; //Label Color
	int pColor [3]; //Point Color
	int rColor[3]; //Residual Color
	int dotWidth, lnWidth;
	bool showProjPoint;
	bool showScale;
	double scaleValue;
	double gsdValue;

	// GCP chips
	CFileName chipDbFileName;
	vector <CGCPDataBasePoint> gcpDB;
	CGCPDataBasePoint gcpPoint;
	unsigned long currentChipID; // current available ID for a new chip
	CXYZPointArray xyzGCPchipPoints;
	vector <bool> chipSelectionArray;
	bool useWallis4ChipMatching;

	// monoplotting heights
	CXYZPolyLineArray xyzBheights;
	CLabel currentBheightLabel;
	//<<addition
};



class CDrawingSetting : public  CSerializable
{
public:
	CDrawingSetting();

	virtual bool isa(string& className) const
	{
		if (className == "CDrawingSetting")
			return true;

		return false;
	};

	virtual string getClassName() const {return string("CDrawingSetting");};

	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified() { return true;};
	virtual void reconnectPointers(){};
	virtual void resetPointers(){};

public:
	int lineWidth;
	C3DPoint lineColor;
};


extern CBaristaProject project;

#define __CBaristaProject__
#endif

