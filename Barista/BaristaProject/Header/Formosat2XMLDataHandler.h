#ifndef FORMOSAT2XMLDATAHANDLER_H
#define FORMOSAT2XMLDATAHANDLER_H

/**
 * Copyright:    Copyright (c) <p>
 * Company:      University of Melbourne<p>
 * @author Jan Tischer
 * @version 1.0
 */

#include "XMLDataHandler.h"

class CXYZOrbitPointArray;
class CFormosatData;

class CFormosat2XMLDataHandler : public CXMLDataHandler
{
public:
	CFormosat2XMLDataHandler(CFormosatData& dataStorage);
	~CFormosat2XMLDataHandler(void);

    virtual void endElement(const XMLCh* const name);
	virtual void startElement(const XMLCh* const name, AttributeList& attributes);

private:
 
	CFormosatData& dataStorage;

	bool readSubElement;
	
	bool readMetadata_Id;
	bool readDataset_Id;
	bool readDataset_Frame;
	bool readRaster_Frame;
	bool	readVertex;
	bool	readScene_Center;
    bool readRaster_CS;
	bool readProduction;
	bool	readProduction_Facility;
	bool readRaster_Dimension;
	bool readCoordinate_Reference_System;
	bool	readHorizontal_CS;
	bool readRaster_Encoding;
	bool readData_Processing;
	bool	readProcessing_Options;
	bool		readCorrection_Algorithm;
	bool readData_Access;
	bool	readData_File_Format;
	bool		readData_File;
	bool readImage_Display;
	bool	readBand_Display_Order;
	bool readDataset_Sources;
	bool	readSource_Information;
	bool		readScene_Source;
	bool readImage_Interpretation;
	bool	readSpectral_Band_Info;
	bool		readGain_List;
	bool			readGain;
	bool readData_Strip;
	bool	readIdentification;
	bool	readEphemeris;
	bool		readRaw_Ephemeris;
	bool			readPoint_List;
	bool				readPoint;
	bool					readLocation;
	bool					readVelocity;
	bool		readCorrected_Ephemeris;
	bool			readCorrected_Point_List;
	bool				readCorrected_Point;
	bool	readAttitudes;
	bool		readRaw_Attitudes;
	bool			readQuaternion_List;
	bool				readQuaternion;
	bool		readCorrected_Attitudes;
	bool			readECF_Attitude;
	bool				readAngle_List;
	bool				readAngle;
	bool	readSensor_Configuration;
	bool		readInstrument_Look_Angles_List;
	bool			readInstrument_Look_Angles;
	bool				readPolynomial_Look_Angles;
	bool		readInstrument_Biases;
	bool	readFrame_Counters;
	bool		readBand_Counters;
	bool	readTime_Stamp;
	bool	readSensor_Calibration;
	bool		readCalibration;
	bool			readBand_Parameters;
	bool				readGain_Section_List;
	bool					readGain_Section;
	bool						readPixelParameters;
	bool							readCells;
	bool								readCell;

	double x;
	double y;
	double z;
	double vx;
	double vy;
	double vz;
	double q0;
	double q1;
	double q2;
	double q3;
	CCharString time;
	bool hasVelocity;
	int bandIndex;
};
#endif //FORMOSAT2XMLDATAHANDLER_H