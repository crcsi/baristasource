#ifndef __CBaristaLSM__
#define __CBaristaLSM__
 
#include "LSM.h"
#include "XYZPoint.h"

class CVImage;
class CXYPoint;
class CXYZPointArray;
class CSensorModel;
class CWallisFilter;

class CBaristaLSM : public CLSM
{
  protected:

	  CVImage *templateImg;

	  vector<CVImage *> searchImgs;

	  vector<C2DPoint> pminVec;
	  vector<C2DPoint> pmaxVec;

	  float rhoMin;

	  float coordFactor;
	  float coordOffset;

	  double sigmaGeometryUsed;

	  double weightFactor;

	  int currentPyrLevel;

	  bool pointObs;


	  C3DPoint Pzmin;

	  C3DPoint Pzmax;

	  float uncertaintyH;

	  float Z0;

	  CImageBuffer *temp0;

	  CXYZPointArray *objectPointArray;

	  CWallisFilter *WallisFilter;

  public:

	  CBaristaLSM();

	  CBaristaLSM(CVImage *img, const CXYPoint &p, double z, float uncertaintyZ,
				  const CMatchingParameters &pars, CWallisFilter *Wallis,
				  const CCharString &obsPtName, C2DPoint *averageShift);

	  virtual ~CBaristaLSM();

	  virtual bool hasPointObs() const { return pointObs; };

	  CVImage *getTemplateImg() const { return this->templateImg; };

	  CVImage *getSearchImg(int i) const { return this->searchImgs[i]; };

	  const CSensorModel *getTemplateSensorModel();

	  const CSensorModel *getSearchSensorModel(int i);

	  bool initialize(CVImage *img, const CXYPoint &p, double z, float uncertaintyZ,
		              const CMatchingParameters &pars, CWallisFilter *WallisFilter,
					  const CCharString &obsPtName, C2DPoint *averageShift);

	  bool MatchWithLSM();

	  bool CrossCorrelationAllLevels();

	  virtual bool LSM();

	  int getCurrentPyrLevel() const { return this->currentPyrLevel; };

	  bool intersect(bool storeQxx);

	  void setObjectPointArray(CXYZPointArray *ptArr);

	  bool initObjectPointArray(const CCharString &name);
	  void resetObjectPointArray();

	  virtual void clear();

	  bool insertObjectPoint() const;


  protected:
  
	  bool setResultsToBaristaProject(double sigmaObs);


	  bool adjustBppOfSearchBuf(CImageBuffer *searchBuf);

	  bool MoveToNextLevel();
	  bool CrossCorrelationCurrentLevel(float maxCorr, vector<bool> &isUsed);

	  bool CrossCorrelationCurrentLevelArea(float maxCorr, vector<bool> &isUsed);


	  bool initSearchBuffers(C2DPoint *averageShift);
	  bool checkSearchImage(CVImage *img);

	  virtual int addPointObservations(SymmetricMatrix &N, Matrix &t, double &pll, int IndexN);

	  bool getSearchWin(const C2DPoint &p1, const C2DPoint &p2, int width, int height, 
		                int &xmin, int &ymin, int &w, int &h, int scaleFactor);

	  bool initSearchWin(CVImage *img, CImageBuffer *&imgBuf, int xmin, int ymin, int w, int h, int searchBuf);

	  bool readTemplate();

	  bool initTemplate();

	  bool rotateCrossCorrTemplate(int SearchIndex);

	  bool CrossCorrelationCurrentLevelOffEpipolar(int nSteps, float maxCorr);

	  void deleteSearchElement(int index);

	  double getTempSearchFactor();
	  double getTempSearchFactor(CVImage *psearch);
	  int getSearchTempFactor(CVImage *psearch);
};

#endif