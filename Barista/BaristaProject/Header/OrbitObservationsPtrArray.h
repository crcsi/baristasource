#ifndef __COrbitObservationsPtrArray__
#define __COrbitObservationsPtrArray__

#pragma once

#include "MUObjectPtrArray.h"
#include "OrbitObservations.h"

class COrbitObservationsPtrArray : public CMUObjectPtrArray<COrbitObservations>
{
public:
	COrbitObservationsPtrArray(int nGrowBy = 0);
	~COrbitObservationsPtrArray(void);
};
#endif