#ifndef __CVALSArray__
#define __CVALSArray__

#include "VALS.h"
#include "VALSPtrArray.h"
#include "MUObjectArray.h"
#include "UpdateHandler.h"

class CVALSArray : public CMUObjectArray<CVALS, CVALSPtrArray>, CSerializable,public CUpdateHandler
{
public:
	CVALSArray(int nGrowBy = 0);
	~CVALSArray(void);

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    bool isModified();

	int getbyName(const CCharString &name) const;

	CVALS* getALSbyName(const CCharString &name) const;

	CVALS* getALSbyName(CCharString* name) const { return getALSbyName(*name); };

    bool isa(string& className) const
	{
		if (className == "CVALSArray")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CVALSArray");
	};

};

#endif