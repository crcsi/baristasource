#pragma once
#include "Serializable.h"
#include "StructuredFile.h"
#include "Filename.h"

#define NUMBER_OF_SATELLITES 6
class CIniFile : public CSerializable
{
  private:
    CFileName currdir;
    CFileName ininame;
	int zone;
	int hemisphere;
	double centralMeridian;
	double latitudeOrigin;
	bool loadRPC;
	bool loadTFW;
	bool rememberloadSettings;
	bool askloadSettings;
	CCharString spheriod;
	double scale;
	double easting;
	double northing;

	double OrthoRes;
	double OrthoMinX;
	double OrthoMinY;
	double OrthoMaxX;
	double OrthoMaxY;
	CCharString OrthoDEM;
	CCharString Ellipsoid;
	bool isUTM;
	bool isWGS84;
	int coordinatetype;


	doubles satelliteHeights;
	doubles groundHeights;
	doubles groundTemperatures;

	doubles rmsPath;
	doubles rmsAttitudes;

	int satellite;

	int dem_nStatisticRows;
	int dem_nStatisticCols;
	bool dem_showAvg;
	bool dem_showDevAvg;
	bool dem_showMin;
	bool dem_showMax;
	bool dem_showDev;
	vector<int> dem_statTextColor;
	vector<int> dem_statLineColor;
	int dem_statFontSize;
	int dem_nLegendeClasses;

	double registrationCrossCorr;

	double registrationParallax;

	double registrationSigma0;

	bool registrationAddPoints;

	int widthObjectSelector;
	int heightObjectSelector;

	int selectedCamera;

	double inTrackViewAngle;
	double crossTrackViewAngle;

	bool useForwardIntersection;
	bool excludeTiePoints;

	double maxRobustPercentage;
	double minRobFac;
	
	bool ALShasStrips;
	int ALSFormat;
	int ALSPulseMode;
	double ALSoffsetX;
	double ALSoffsetY;
	double ALSdeltaX;
	double ALSdeltaY;

public:
    CIniFile(void);
    ~CIniFile(void);

	// CSerializable functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);

	void reconnectPointers();
	void resetPointers();

	//CCharString getClassName();
	bool isModified();
	// end of CSerializable functions

    void writeIniFile();
    void readIniFile();
    void setCurrDir(const char* dir);
    const char* getCurrDir();
    const char* getIniFileName();

	const char *getEllipsoidName() const;
	void setEllipsoidName(const char *ell);

	bool getIsUTM() const;
	void setIsUTM(bool utm);

	bool getIsWGS84() const;
	void setIsWGS84(bool wgs);

	int getCoordinateType() const;
	void setCoordinateType(int type);

	int getZone();
	void setZone(int zone);

	int getHemisphere();
	void setHemisphere(int hemisphere);

	double getCentralMeridian();
	void setCentralMeridian(double centralMeridian);

	double getLatitudeOrigin();
	void setLatitudeOrigin(double latitudeOrigin);

	double getTMScale();
	void setTMScale(double scale);

	double getTMEasting();
	void setTMEasting(double easting);

	double getTMNorthing();
	void setTMNorthing(double northing);

	double getOrthoRes();
	void setOrthoRes(double orthoRes);
	double getOrthoMinX();
	void setOrthoMinX(double orthoMinX);
	double getOrthoMinY();
	void setOrthoMinY(double orthoMinY);
	double getOrthoMaxX();
	void setOrthoMaxX(double orthoMaxX);
	double getOrthoMaxY();
	void setOrthoMaxY(double orthoMaxX);
	CCharString getOrthoDEM();
	void setOrthoDEM(const CCharString &orthoDEM);

	bool getloadRPC();
	bool getloadTFW();
	bool getrememberloadSettings();
	bool getaskloadSettings();
	void setloadRPC(bool loadRPC);
	void setloadTFW(bool loadTFW);
	void setrememberloadSettings(bool rememberloadSettings);
	void setaskloadSettings(bool askloadSettings);


	void setSatelliteHeight(double height,int index) {
		if (index >= NUMBER_OF_SATELLITES)
			return;
		else
			this->satelliteHeights[index] =height;};

	
	void setSceneHeight(double groundHeight,int index) {
		if (index >= NUMBER_OF_SATELLITES)
			return;
		else
			this->groundHeights[index] = groundHeight;};


	void setSceneTemperature(double temp,int index) {
		if (index >= NUMBER_OF_SATELLITES)
			return;
		else
			this->groundTemperatures[index] = temp;};

	void setRMSPath(double rms,int index) {
		if (index >= NUMBER_OF_SATELLITES)
			return;
		else
			this->rmsPath[index] = rms;};

	void setRMSAttitudes(double rms,int index) {
		if (index >= NUMBER_OF_SATELLITES)
			return;
		else
			this->rmsAttitudes[index] = rms;};



	double getSatelliteHeight(int index)const{
		if (index >= NUMBER_OF_SATELLITES)
			return -1.0;
		else
			return this->satelliteHeights[index];};

	double getSceneHeight(int index)const{
		if (index >= NUMBER_OF_SATELLITES)
			return -100.0;
		else
			return this->groundHeights[index];};

	double getSceneTemperature(int index)const{
		if (index >= NUMBER_OF_SATELLITES)
			return -200.0;
		else
			return this->groundTemperatures[index];};

	double getRMSPath(int index)const{
		if (index >= NUMBER_OF_SATELLITES)
			return -1.0;
		else
			return this->rmsPath[index];};

	double getRMSAttitudes(int index)const{
		if (index >= NUMBER_OF_SATELLITES)
			return -1.0;
		else
			return this->rmsAttitudes[index];};
	
	int getSatellite()const {return this->satellite;};
	void setSatellite(int sat){this->satellite = sat;};	

    bool isa(string& className) const
	{
		if (className == "CIniFile")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CIniFile");
	};

	void setDEMStatistics(	int nRows,
							int nCols,
							bool showAvg,
							bool showDevAvg,
							bool showMin,
							bool showMax,
							bool showDev,
							vector<int> statTextColor,
							vector<int> statLineColor,
							int statFontSize,
							int nLegendeClasses);

	int getDEM_NStatisticRows() const { return this->dem_nStatisticRows;};
	int getDEM_NStatisticCols() const { return this->dem_nStatisticCols;};
	bool getDEM_ShowAvg() const {return this->dem_showAvg;};
	bool getDEM_ShowDevAvg() const {return this->dem_showDevAvg;};
	bool getDEM_ShowMin() const {return this->dem_showMin;};
	bool getDEM_ShowMax() const {return this->dem_showMax;};
	bool getDEM_ShowDev() const {return this->dem_showDev;};
	vector<int> getDEM_StatTextColor() const {return this->dem_statTextColor;};
	vector<int> getDEM_StatLineColor() const {return this->dem_statLineColor;};
	int getDEMStatFontSize() const { return this->dem_statFontSize;};
	int getDEMNLegendeClasses() const { return this->dem_nLegendeClasses;};

	double getRegistrationCrossCorr() const { return registrationCrossCorr; }

	double getRegistrationParallax() const { return registrationParallax; }

	double getRegistrationSigma0() const { return registrationSigma0; }

	bool getRegistrationAddPoints() const { return registrationAddPoints; }

	void setRegistrationCrossCorr(double val) { registrationCrossCorr = val; }

	void setRegistrationParallax(double val) { registrationParallax = val; }

	void setRegistrationSigma0(double val) { registrationSigma0 = val; }

	void setRegistrationAddPoints(bool val) { registrationAddPoints = val; }

	
	void getSizeOfObjectSelectorDlg(int& width,int& height) {width = this->widthObjectSelector; height = this->heightObjectSelector;};

	void setSizeOfObjectSelectorDlg(int width,int height) { this->widthObjectSelector = width; this->heightObjectSelector = height;};

	int getSelectedCamera()const {return this->selectedCamera;} ;
	void setSelectedCamera(int selectedCamera) {this->selectedCamera = selectedCamera;};

	void getLastViewAngle(double& inTrackAngle, double& crossTrackAngle) const { crossTrackAngle = this->crossTrackViewAngle;
																				 inTrackAngle = this->inTrackViewAngle;};
	void setLastViewAngle(double inTrackAngle, double crossTrackAngle) { this->crossTrackViewAngle = crossTrackAngle;
																		 this->inTrackViewAngle = inTrackAngle;};

	void setAdjustmentSettings(bool useForwardIntersection,bool excludeTiePoints,double maxRobustPercentage,double minRobFac);
	void getAdjustmentSettings(bool& useForwardIntersection,bool& excludeTiePoints,double& maxRobustPercentage,double& minRobFac);
	
	bool getALShasStrips() const { return this->ALShasStrips; };
	void setALShasStrips(bool hasStrips) { this->ALShasStrips = hasStrips; };

	int  getALSFormat() { return this->ALSFormat; };
	void setALSFormat(int alsformat) { this->ALSFormat = alsformat; };

	int  getALSPulseMode() const { return this->ALSPulseMode; };
	void setALSPulseMode(int pulsmode) { this->ALSPulseMode = pulsmode; };

	double getALSoffsetX() const { return this->ALSoffsetX; };
	void   setALSoffsetX(double offsetx) { this->ALSoffsetX = offsetx; };

	double getALSoffsetY() const { return this->ALSoffsetY; };
	void   setALSoffsetY(double offsety) { this->ALSoffsetY = offsety; };

	double getALSdeltaX() const { return this->ALSdeltaX; };
	void   setALSdeltaX(double deltax) { this->ALSdeltaX = deltax; };

	double getALSdeltaY() const { return this->ALSdeltaY; };
	void   setALSdeltaY(double deltay) { this->ALSdeltaY = deltay; };

};


extern CIniFile inifile;