#ifndef XMLPARSER_H
#define XMLPARSER_H

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "Filename.h"

class CXMLDataHandler;
class CXMLErrorHandler;

class CXMLParser
{
public :

	CXMLParser() {};  
	~CXMLParser() {};
	bool parseFile(CFileName filename,CXMLDataHandler* dataHandler,CXMLErrorHandler* errorHandler);

private:



};

#endif