#pragma once

/**
 * Copyright:    Copyright (c) Franz Rottensteiner<p>
 * Company:      University Hannover<p>
 * @author Franz Rottensteiner
 * @version 1.0
 */


#include "ProgressHandler.h"
#include "GenerateEpipolar.h"

class CGenerateEpipolarProject : public CGenerateEpipolar
{	
  protected:
	  CVImage *leftImage;
	  CVImage *rightImage;

	  // Data
	  CVImage *leftEpipolarImage;
	  CVImage *rightEpipolarImage;

	  CFrameCamera *leftCamera;
	  CFrameCamera *rightCamera;
	  CCameraMounting *mounting;
	  
	  CFrameCameraModel *leftFrameCam;
	  CFrameCameraModel *rightFrameCam;

	  CCharString outputPath;


  public:

	  CGenerateEpipolarProject(CVImage *left, CVImage *right, const CCharString &path);
	  
	  ~CGenerateEpipolarProject();

	
	  bool generateEpipolar(); 


  protected:

	  bool initBaristaProject();

	  virtual bool initNewOrientations();

	  bool generateImage(CVImage *imgOrig, CVImage *imgEpi);

	  void cleanBaristaProject();
};
