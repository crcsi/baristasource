#ifndef __CMonoPlotter__
#define __CMonoPlotter__

/** 
 * @class CMonoplotter
 * CMonoplotter, is abstract base for monoplotting with different
 * sensor models like rpc or affine
 * 
 * 
 * @author Jochen Willneff
 * @version 1.0 
 * @date 28-june-2005
 *
 */

#include "ZObject.h"

class CXYZPoint;
class CXYPoint;
class CVDEM;
class CSensorModel;
class CTrans3D;
class C3DPoint;
class C2DPoint;
class CReferenceSystem;
class CXYZPolyLineArray;

class CMonoPlotter : public ZObject
{
  protected:
  
	  CVDEM* vdem;

	  const CSensorModel *sensorModel;

	  CTrans3D *sensorToDEM;

	  CTrans3D *DEMToSensor;

	  double lastZ;

	  bool iteratePoint(CXYZPoint &point, const CXYPoint &sl, double Z,double limit = 0.0001);

  public:
 
	  CMonoPlotter();
	  ~CMonoPlotter();

	  void setVDEM(CVDEM* vdem,bool initDEMBorder = true);

	  const CVDEM* getVDEM() const {return this->vdem;};

	  double getMinZ() const;
	  double getMaxZ() const;

	  double getInterpolatedHeight (double x, double y, double &sigma);

	  CReferenceSystem getReferenceSystem() const;

	  CXYZPolyLineArray* getDEMBorderLines();

	  double getLastZ() const { return this->lastZ; };
  
	  CTrans3D *getSensorToDEM() const { return sensorToDEM; };

	  CTrans3D *getDEMToSensor() const { return DEMToSensor; };

	  /// gets the sensor model, e.g. rpc or affine

	  const CSensorModel *getModel() const;
	  /// sets the sensor model, e.g. rpc or affine
	  void setModel(const CSensorModel *pSensorModel);

	  /// to monoplot a 3D point from image coordinates
	  bool getPoint(CXYZPoint &point, const CXYPoint &sl,double limit= 0.0001);

	  /// determines XY position from image coordinates and given height
	  bool getXYfromImgPointHeight(CXYZPoint &point, const CXYPoint &sl, double height);

	  /// gets "roofpoint" above monoplotted groundpoint
	  bool getHeightPoint(CXYZPoint &point, CXYPoint &imagepoint, const CXYZPoint &groundpoint);

	  /// gets vertical Pole from from imagecoordinates of both ground and top point
	  bool getVerticalPole(CXYZPoint      &objGround, CXYZPoint      &objTop, 
						   const CXYPoint &obsGround, const CXYPoint &obsTop);

	  bool getProjectionRay(C3DPoint &p0, C3DPoint &dir, const C2DPoint &obs, double Zlow, double Zhigh) const;

	bool isa(string& className) const
	{
		if (className == "CMonoPlotter")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CMonoPlotter");
	};  

	void init();
};

#endif

