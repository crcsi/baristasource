#pragma once

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "ProgressHandler.h"
#include "VImage.h"
#include "Filename.h"

class CImageBuffer;


class CBandMerger : public CProgressHandler
{


public:
	enum InterpolationMethod{eBiLinear};

	CBandMerger();
	~CBandMerger();

	// we expect the order RGBA for the inputImages
	// R channel will be the reference for the corrections
	bool init(const vector<CVImage*>& inputImages,
			  unsigned int startPosRefImage,
			CCharString newCameraName,
			  CBandMerger::InterpolationMethod method,
			  CVImage* newMergedImage);

	void setInitWallisFilter(bool b) { this->initWallisFilter = b;};

	bool mergeBands();

	CCharString getErrorMessage() const {return this->errorMessage;};

	
	static void findImageGroups(vector<vector<CVImage*> >& newImages,vector<CCharString>& names);

	static void sortImages(vector<CVImage*>& newImages);

	
	static int findBoundary(CHugeImage* himage);

protected:

	bool initNewImage();
	bool computeCCDPos();
	bool resampleImages();

	vector<vector<C2DPoint> > ccdPos;

	vector<CVImage*> inputImages;

	CFileName newFileName; 
	CFileName newHugeFileName;
	CCharString newCameraName;
	CVImage* newImage;

	CCharString errorMessage;

	unsigned int newImageWidth;
	unsigned int newImageHeight;

	unsigned int startColOfRefImage;
	int newImageBpp;
	int newImageBands;

	int minYValue;
	int maxYValue;

	int marginInterpolation;
	InterpolationMethod method;

	bool initWallisFilter;
};
