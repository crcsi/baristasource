#pragma once


/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "ProgressHandler.h"
#include "Filename.h"
#include "SatelliteDataReaderFactory.h"
#include "TFW.h"

class CImageImportData;
class CImageFile;
class CVBase;
class CVImage;
class CVDEM;

class CImageImporter : public CProgressHandler
{


public:
	
	CImageImporter();
	~CImageImporter();

	// reads a file for the first time and saves header information in CImageImportData class
	// if the hugeFileNameDir is not empty the output directoy will be set to hugeFileNameDir
	bool initImportData(CImageImportData& importData,bool isDEM,CFileName filename,CFileName hugeFileNameDir);

	// when initImportData has failed the user can still provide enough information to read the file
	// use initImortDataAsBinary or initImportDataAsASCII to init the imagefile, then use importImage
	bool initImportDataAsBinary(CImageImportData& importData,bool isDEM);

	bool initImportDataAsASCII(CImageImportData& importData,bool isDEM);

	// in case of AVNIR-2 images we have to use the AVNIR2File class, but the importer cannot
	// detect that, so we have to init it from the outside!
	// the initialisation can be done after the gdal object has been created
	bool initImportDataForAVNIR2Image(CImageImportData& importData);

	// imports a file using the information in CImageImportData class
	bool importImage(CImageImportData& importData,bool isDEM);
	
	// function to check write permissions 
	bool checkWritePemission(CFileName filename);

	CCharString getErrorMessage() const {return this->errorMessage;};

	void setErrorMessage(CCharString ch) {this->errorMessage = ch;};


protected:

	bool tryReadFile(CImageImportData& importData);


	CCharString errorMessage;
};




class CImageImportData
{
public:
	CImageImportData();
	~CImageImportData();
	
	void deleteImageFile();
	void reset();

	enum eGEOSource {eImageFile,eExternalFile,eParameter};

public:

	CImageFile* imagefile;
	CFileName filename;
	CFileName tfwFilename;
	CFileName rpcFilename;
	CFileName pushBroomFilename;

	CVBase* vbase; // used to indicate whether we need to ask for geo infos later or not
	CTFW tfw;


	CVImage* vimage;  // used to access the created file after import
	CVDEM* vdem; // used to access the created file after import

	

	int bands;
	int channel1;
	int channel2;
	int channel3;
	int channel4;
	bool bDeleteHugeFile;
	
	// rpc import
	bool bImportRPC;
	eGEOSource rpcSource;
	bool bHasRPCInImageFile;
	bool bHasRPCInExternalFile;

	bool bImportPushBroom;

	bool bHugeFile;


	// tfw import
	bool bImportTFW; // import the data
	eGEOSource tfwSource;
	bool bHasTFWInImageFile;
	bool bHasTFWInExternalFile;

	SatelliteType satType; 



	float Z0;
	float scale;
	float NoDataValue;
	float sigmaZ;

	// values only uesed in the raw importer
	int cols;
	int rows;
	int filebpp;
	int destbpp;

	int HeaderSize;

	C2DPoint lowerleft;
	C2DPoint gridding;

	bool interleaved;
	bool BigEndian;
	bool interpretation;
	bool toptobottom;

	bool readAVNIR2Image;

	bool initWallisFilter;
};
