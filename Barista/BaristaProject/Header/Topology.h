#pragma once

class CObsPoint;

class CTopology
{
private:
	CTopology(void);

public:
	CTopology(CObsPoint* from,CObsPoint* to,const int *col,unsigned int lineWidth);
	CTopology(const CTopology& topology);
	~CTopology(void);

	CObsPoint* from;
	CObsPoint* to;

	int color [3];
	unsigned int lineWidth;
};
