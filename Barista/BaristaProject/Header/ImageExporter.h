#pragma once

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */
#include "ProgressHandler.h"
#include "CharString.h"
#include "Filename.h"
#include "GDALFile.h"

class CTFW;
class CLookupTable;
class CHugeImage;
class CVBase;


class CImageExporter : public CProgressHandler
{


public:
	
	CImageExporter(CFileName exportFileName,CVBase* srcImage,bool isDEM);
	~CImageExporter();

	bool writeImage();

	bool writeSettings();

	CFileName getFilename() const { return this->filename; };
	void setFilename(CFileName name);

	CCharString getFileType() const { return this->filetype;};
	void setFileType(CCharString newFileType);

	void setTFW(const CTFW* tfw);
	bool hasGeoInformation() const;

	void setLookupTable(const CLookupTable* lut);

	void setExportGeo(bool b);
	bool getExportGeo() const {return this->exportGeo;};

	void setExportExtern(bool b);
	bool getExportExtern() const {return this->exportExtern;};

	void setGeoFileName();
	void setGeoFileName(CFileName name);
	CFileName getGeoFileName()const  { return this->geoFilename;};

	CCharString getCompression()const { return this->compression;};
	void setCompression(CCharString c);

	int getDepth() const {return this->depth;};
	void setDepth(int d);

	int getDataDepth() const;
	int getDataBands() const;

	void setQuality(int q) { this->quality = q;};
	int getQuality() const { return this->quality;};

	void setBands(int b);
	int getBands() const { return this->nChannels;};

	void setChannelCombination(CCharString c) {this->channelCombination = c;};
	CCharString  getChannelCombination()const { return this->channelCombination;};

	void setTiledTiff(bool b) { this->tiledTiff = b;};
	bool getTiledTiff() const { return this->tiledTiff;};

	void setBandInterlave(bool b) { this->bandInterleave = b;};
	bool getBandInterlave() const { return this->bandInterleave;};

	bool applySettings(const CImageExporter& exporter);

private:

	CFileName filename;
	CGDALFile gdalFile;
	CVBase* vBaseSrc;
	CHugeImage* imageSrc;

	CCharString filetype;
	int nChannels;
	CCharString channelCombination;
	int depth;
	CCharString compression;
	int quality;

	bool tiledTiff;
	
	bool bandInterleave;


	// tfw information
	bool exportGeo;
	bool exportExtern;

	CFileName geoFilename;

};


