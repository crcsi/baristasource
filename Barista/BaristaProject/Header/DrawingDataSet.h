#ifndef CDrawingDataSet_H
#define CDrawingDataSet_H
/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "ObsPointArray.h"
#include "Topology.h"
#include "Label.h"

class CXYPolyLine;


class CDrawingDataSet
{
private:
	CDrawingDataSet(void):points(2),labelSort(2){}; 

	// search functions
	bool findPos_PointLabel(const CLabel& pointName,int &pos);
	bool findPos_Topology_Key1(const CLabel& key1,int &pos,int &length);
	bool findPos_Topology_Key2(const CLabel& key2,int &pos,int low,int length);
	
	bool findTopologyPos(const CLabel& pointName1,const CLabel& pointName2,int& pos);
	
	// internal topology functions
	bool removeTopologiesForPoint(const CLabel& p);

public:
	CDrawingDataSet(int dim);
	CDrawingDataSet(const CDrawingDataSet& dataSet);
	~CDrawingDataSet(void);

	// name of the dataset
	CLabel getName()const {return this->name;};
	void setName(const CLabel& name);


	// functions to add and remove a single point
	// the sorted array is updated
	// remove also removes all topological connections
	CObsPoint* addPoint(CObsPoint* point);
	void removePoint(const CLabel& point);
	void removeAllPoints();
	bool queryPoint(const CLabel & pointLabel);

	// functions to add and remove topology
	bool addTopology(const CLabel& p1,const CLabel& p2,const int *color,unsigned int lineWidth);
	bool removeTopology(const CLabel& p1,const CLabel& p2);

	// get the size of the arrays
	unsigned int getNTopology()const {return topology.size();};
	unsigned int getNPoints() const {return points.GetSize();};

	// get single elements of the arrays
	CTopology* getTopology(const int index);
	CObsPoint* getPoint(const int index);

	// querry functions for drawing
	bool getShowPoints() {return this->showPoints;};
	void setShowPoints(bool b) {this->showPoints = b;};

	bool getShowPointLabels() {return this->showPointLabel;};
	void setShowPointLabels(bool b) {this->showPointLabel = b;};

	int *getPointColor() {return this->pointColor;};
	void setPointColor(const int *color);

	// change color
	void changeTopologyColor(const CLabel& p1,const CLabel& p2,const int *color);
	void changeAllTopologyColors(const int *color);

	// change line width
	void changeTopologyLineWidth(const CLabel& p1,const CLabel& p2,unsigned int lineWidth);
	void changeAllTopologyLineWidth(unsigned int lineWidth);

protected:

	CObsPointArray points;
	vector<CObsPoint*> labelSort;
	vector<CTopology> topology;
	CLabel name;
	bool showPoints;
	bool showPointLabel;
	int pointColor[3];
};
#endif
