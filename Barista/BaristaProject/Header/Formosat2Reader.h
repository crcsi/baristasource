#ifndef __CFormosat2Reader__
#define __CFormosat2Reader__

/**
 * Copyright:    Copyright (c) Jan Tischer<p>
 * Company:      University of Melbourne<p>
 * @author Jan Tischer
 * @version 1.0
 * revised by shijie liu 23/04/2010
 */

#include "SatelliteDataReader.h"
#include "FormosatData.h"


class CFormosat2Reader : public CSatelliteDataReader
{

public:
	CFormosat2Reader(void);
	virtual ~CFormosat2Reader(void);

	virtual bool readData(CFileName &filename);

protected:
	virtual bool prepareEphemeris();
	virtual bool prepareAttitudes();
	virtual bool prepareMounting(){return true;};

	void computeLOS2PIPMatrix(CRotationMatrix& mat);
	
	bool prepareLookAngles();

private:
	CFormosatData dataStorage;

};
#endif