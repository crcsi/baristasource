#ifndef __CVDEM__
#define __CVDEM__

#include "VBase.h"
#include "Serializable.h"
#include "Filename.h"
#include "DEM.h"
#include "UpdateListenerPtrArray.h"
#include "EntityDrawing.h"
#include "ReferenceSystem.h"
#include "histogramFlt.h"
#include "DisplaySettings.h"

class QDialog;

class CVDEM : public CVBase
{
public:
    CVDEM(void);
    CVDEM(CVDEM* src);
    CVDEM(const CVDEM& src);
    ~CVDEM(void);

    CFileName* getFileName();
    void setFileName(const char* filename);
    CFileName  getItemText();


    CDEM* getDEM();

    bool isa(string& className) const
	{
		if (className == "CVDEM")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CVDEM");
	};

    // CSerializable
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);
    void reconnectPointers();
    void resetPointers();

	void setReferenceSystem(const CReferenceSystem &referenceSystem);
	void resetSensorModel(CSensorModel* model){ this->dem.resetSensorModel(model);};

	virtual const CTFW* getTFW()const {return &dem.GetTFW();};
	virtual bool hasTFW() const { return dem.GetTFW().gethasValues(); };


	const CReferenceSystem &getReferenceSystem() const
    {
        return this->dem.GetTFW().getRefSys();
    };

	bool interpolate(CALSDataSet &als, const char *filename, const C2DPoint &pmin, 
					 const C2DPoint &pmax, const C2DPoint &gridS, int N, int echo, int level,
					 eInterpolationMethod method, double maxDist, double minsquareDistForWeight);

	bool interpolate(const CXYZPointArray &points, const char *filename, const C2DPoint &pmin, 
					 const C2DPoint &pmax, const C2DPoint &gridS, int N, 
					 eInterpolationMethod method, double maxDist, double minsquareDistForWeight);


	void setDEMtoActive(bool isActive) { this->isActiveDEM = isActive; };
	bool getIsActiveDEM() { return this->isActiveDEM; };

	bool cropfromDEM(CVDEM* vdem,double ulx,double uly,double width,double height);

	virtual const CSensorModel* getModel(enum eLSMTRAFOMODE trafomode = eLSMNOTFW)  { return this->dem.getTFW();};

	virtual CHugeImage* getHugeImage() { return &this->dem;};
	
	virtual void init() { this->dem.init();};

	virtual void setTFW(const CTFW& tfw) { this->dem.setTFW(tfw);};

	bool computeDEMStatistics(	int nRows,
								int nCols,
								bool showAvg,
								bool showDevAvg,
								bool showMin,
								bool showMax,
								bool showDev,
								int *statTextColor,
								int *statLineColor,
								int statFontSize,
								int nLegendeClasses,
								bool computeHist);

	void deleteDEMStatistics();
	
	int getNStatisticRows() const { return this->nStatisticRows;};
	int getNStatisticCols() const { return this->nStatisticCols;};
	bool getShowAvg() const {return this->showAvg;};
	bool getShowDevAvg() const {return this->showDevAvg;};
	bool getShowMin() const {return this->showMin;};
	bool getShowMax() const {return this->showMax;};
	bool getShowDev() const {return this->showDev;};
	const int *getStatTextColor() const {return this->statTextColor;};
	const int *getStatLineColor() const {return this->statLineColor;};
	int getStatFontSize() const { return this->statFontSize;};
	int getNLegendeClasses() const { return this->nLegendeClasses;};

	const CHistogramFlt& getStatHistogram() const {return this->statHist;}; 
	double getZeroValue() const { return this->zeroValue;};
	void setZeroValue(double zeroValue) { this->zeroValue = zeroValue;};

	const CObsPointArray& getDEMStatisticts() const { return this->demStatistics;};

	void setLegend(QDialog* Legend) { legend = Legend; };
	QDialog *getLegend() const { return this->legend; };

   CDisplaySettings* getDisplaySettings() { return &this->displaySettings;};

protected:
    CFileName demFileName;
    CDEM dem;

    bool isActiveDEM;

	CObsPointArray demStatistics;
	int nStatisticRows;
	int nStatisticCols;
	bool showAvg;
	bool showDevAvg;
	bool showMin;
	bool showMax;
	bool showDev;
	int statTextColor[3];
	int statLineColor[3];
	int statFontSize;
	int nLegendeClasses;
	bool computeHist;
	double zeroValue;
	CHistogramFlt statHist;
	QDialog* legend;

	CDisplaySettings displaySettings;

};

#endif
