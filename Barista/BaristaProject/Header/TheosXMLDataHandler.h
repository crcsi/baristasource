#ifndef THEOSXMLDATAHANDLER_H
#define THEOSXMLDATAHANDLER_H

#include "XMLDataHandler.h"

/*
* Copyright:    Copyright (c) Shijie Liu<p>
* Company:      University of Melbourne<p>
* author:		Shijie Liu
* version 1.0
*/


class CXYZOrbitPointArray;
class CTheosData;


class CTheosXMLDataHandler : 
	public CXMLDataHandler
{
public:
	CTheosXMLDataHandler(CTheosData& dataStorage);	
	~CTheosXMLDataHandler(void);

	virtual void endElement(const XMLCh* const name);
	virtual void startElement(const XMLCh* const name, AttributeList& attributes);

private:

	CTheosData& dataStorage;
/*
	bool readSubElement;

	bool readMetadata_Id;
	bool readDataset_Frame;
	bool	readVertex;
	bool	readScene_Center;
	bool readCoordinate_Reference_System;
	bool	readHorizontal_CS;
	bool readRaster_CS;
	bool readGeoposition;
	bool	readGeoposition_Points;
	bool		readTie_Point;
	bool readRaster_Dimensions;
	bool readRaster_Encoding;
	bool readData_Strip;
	bool	readData_Strip_Identification;
	bool	readSatellite_Time;
	bool	readEphemeris;
	bool		readPoints;
	bool			readPoint;
	bool				readLocation;
	bool				readVelocity;
	bool		readDoris_Points;
	bool			readDoris_Point;
	bool				readDorisLocation;
	bool				readDorisVelocity;
	bool	readSensor_Configuration;
	bool		readTime_Stamp;
	bool		readInstrument_Look_Angles_List;
	bool			readInstrument_Look_Angles;
	bool				readLook_Angles_List;
	bool					readLook_Angles;
	bool	readSatellite_Attitudes;
	bool		readRaw_Attitudes;
	bool			readAocs_Attitudes;
	bool				readAngle_List;
	bool					readAocsAngles;
	bool				readAngular_Speed_List;
	bool					readAngular_Speeds;
	bool			readStar_Tracker_Attitudes;
	bool				readQuaternion_List;
	bool					readQuaternion;
	bool		readCorrected_Attitudes;
	bool			readCorrected_Attitude;
	bool				readCorrectedAngles;//*/

	bool readMetadata_Id;
	bool readDataset_Frame;
	bool	readVertex;
	bool	readScene_Center;
	bool readCoordinate_Reference_System;
	bool	readHorizontal_CS;
	bool readRaster_CS;
	bool readGeoposition;
	bool	readGeoposition_Points;
	bool		readTie_Point;
	bool readRaster_Dimensions;
	bool readRaster_Encoding;

	bool readData_Strip;
	bool	readData_Strip_Identification;
	bool	readTime_Stamp;
	bool	readEphemeris;
	bool		readRaw_Ephemeris;
	bool			readPoint_List;
	bool				readPoint;
	bool					readLocation;
	bool					readVelocity;

	bool	readAttitudes;
	bool		readRaw_Attitudes;
	bool			readQuaternion_List;
	bool				readQuaternion;
	
	bool	readSensor_Configuration;	
	bool		readInstrument_Look_Angles_List;
	bool			readInstrument_Look_Angles;
	bool				readPolynomial_Look_Angles;

	bool		readInstrument_Biases;

	double x;
	double y;
	double z;
	double w;	//appended
	double vx;
	double vy;
	double vz;
	double vw;	//appended
	CCharString time;
	CCharString flag;
	CCharString flag2;
	bool hasVelocity;
	int bandIndex;
	int detectorID;
};
#endif //THEOSXMLDATAHANDLER_H