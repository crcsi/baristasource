#ifndef XMLPARSER_INTERN_H
#define XMLPARSER_INTERN_H

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include <xercesc/parsers/SAXParser.hpp>

XERCES_CPP_NAMESPACE_USE

XERCES_CPP_NAMESPACE_BEGIN
class AttributeList;
XERCES_CPP_NAMESPACE_END


class CXMLErrorHandler;
class CXMLDataHandler;

class CXMLParser_intern : public SAXParser
{
public :

	CXMLParser_intern(CXMLDataHandler* dataHandler,CXMLErrorHandler* errorHandler); 
    ~CXMLParser_intern();

	static bool init();
	static bool terminate();

	bool parseFile(const char* filename);

private:
	CXMLDataHandler* dataHandler;
	CXMLErrorHandler* errorHandler;
};

#endif


