#ifndef GCPMatcher_H
#define GCPMatcher_H

#include "MatchingParameters.h"
#include "2DPoint.h"
#include "ProgressHandler.h"
#include "FeatExtractPars.h"
#include "ReferenceSystem.h"

class CVImage;
class CVDEM;
class CXYZPointArray;
class CReferenceSystem;
class CXYPoint;
class CXYZPoint;
class CWallisFilter;

class CGCPMatcher : public CProgressHandler
{

  protected:

	  CVImage *pOrthoPhoto;

	  CVDEM *pDEM;

	  C2DPoint Pmin, Pmax;

	  CXYZPointArray *pGCPs;

	  CMatchingParameters matchingPars;

	  FeatureExtractionParameters extractionPars;

	  bool success;

	  vector<C2DPoint> shiftVec;

	  C2DPoint averageShift;

	  vector<CVImage *> possibleOrthophotos;

	  vector<CWallisFilter *> WallisFilterVector;

	  vector<CVImage *> imageVector;

	  vector<vector<C2DPoint> > footprintVector;

	  bool isStrip;

  public:

	  CGCPMatcher (CXYZPointArray *GCPs, const CMatchingParameters &pars);

	  CGCPMatcher (CXYZPointArray *GCPs, const CMatchingParameters &pars, CVImage* orthoImage);

	  CGCPMatcher(const vector<CVImage *> &ImageVector, CVDEM *DEM, 
		          int NScenesBridged, float sceneSizePercentage, 
		          const CMatchingParameters &pars, 
		          const FeatureExtractionParameters &ExtractionPars);

	  ~CGCPMatcher();

	  int ExtractAndMatchPoints(bool useWallis, eCoordinateType GCPType, bool makeGCP);

	  int MatchPoints(bool useWallis);

	  bool isInitialised() const { return (this->possibleOrthophotos.size() > 0); }

	  bool getSuccess() const { return this->success; };

	  int getNumberOfPossibleOrthophotos() const { return this->possibleOrthophotos.size(); };
	  int getNumberOfPossibleImages() const { return this->imageVector.size(); };

	  CVImage *getImageAt(int index) const { return this->imageVector[index]; };

	  CVImage *getOrthoImageAt(int index) const { return this->possibleOrthophotos[index]; };

	  CVImage *getOrthoImage() const { return this->pOrthoPhoto; };

	  CXYZPointArray *getGCPs() const { return this->pGCPs; }

	  CMatchingParameters getMatchPars() const { return this->matchingPars; };

	  FeatureExtractionParameters getExtractionPars() const { return this->extractionPars; };

	  void setCurrentOrthoImage(int index);

	  void setImageVector(const vector<CVImage *> &ImageVector,  CVDEM *DEM, 
		                  int NScenesBridged, float sceneSizePercentage);

	  void setDEM(CVDEM *DEM, int NScenesBridged, float sceneSizePercentage);

	  void setPossibleOrthoVector(const vector<CVImage *> &OrthoVector);

	  void setMatchPars(const CMatchingParameters &pars);
	  void setFeatureExtractionPars(const FeatureExtractionParameters &ExtractionPars);

	  void clearOrthos();

	  bool imageVectorIsStrip() const { return this->isStrip; };

	  int getIndexOfOrthophoto(CVImage *ortho) const;


  protected: 

	  void initWallisFilters();
	  void initWallisFilter(int index);

	  void clearWallisFilters();

	  void setGCPs(CXYZPointArray *GCPs,CVImage* orthoImage = NULL);

	  void initOrthos(const C2DPoint &pmin, const C2DPoint &pmax, CReferenceSystem *ref);

	  bool matchPoint(CXYPoint &pImg, CXYZPoint &pObj, bool useDiff, CWallisFilter *pWallis);

	  bool analyseDiffVectors(float DistThreshold);

	  bool checkStrip();

	  int matchAllPoints();

	  void getMinMaxFootprint(int index, C2DPoint &pmin, C2DPoint &pmax);

	  void getOrthosForFootprint(int index, vector<CVImage *> &orthoVec);

	  void initStripFootprints(int NScenesBridged, float sceneSizePercentage);

	  void getFootPrint(CVImage *img, C2DPoint &p0, C2DPoint &p1, C2DPoint &p2, C2DPoint &p3, CReferenceSystem *ref);

	  int getNScenesInStrip();

	  int extractPoints(int footPrintIndex, CVImage *ortho, int minPointNumber, bool useWallis);

};

#endif // GCPMatcher_H