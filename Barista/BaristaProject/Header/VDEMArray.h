#ifndef __CVDEMArray__
#define __CVDEMArray__

#include "VDEM.h"
#include "VDEMPtrArray.h"
#include "MUObjectArray.h"
#include "UpdateHandler.h"

class CVDEMArray : public CMUObjectArray<CVDEM, CVDEMPtrArray>, CSerializable, public CUpdateHandler
{
public:
	CVDEMArray(int nGrowBy = 0);
	~CVDEMArray(void);

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    bool isModified();

	int getbyName(const CCharString &name) const;

	CVDEM* getDEMbyName(const CCharString &name) const;

	CVDEM* getDEMbyName(CCharString* name) const { return getDEMbyName(*name); };

    bool isa(string& className) const
	{
		if (className == "CVDEMArray")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CVDEMArray");
	};

};

#endif