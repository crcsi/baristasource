#ifndef __COrbitObservationsArray__
#define __COrbitObservationsArray__

#include "OrbitObservations.h"
#include "OrbitObservationsPtrArray.h"
#include "MUObjectArray.h"
#include "Serializable.h"
#include "StructuredFileSection.h"
#include "StructuredFileSectionArray.h"

class COrbitObservationsArray : public CMUObjectArray<COrbitObservations, COrbitObservationsPtrArray>, public CSerializable
{
protected:
	SplineModelType type;
public:
	COrbitObservationsArray(int nGrowBy = 0);
	~COrbitObservationsArray(void);

	// serialize functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	bool isModified();
	void reconnectPointers();
	void resetPointers();

	
	virtual COrbitObservations* Add();
	virtual COrbitObservations* Add(const COrbitObservations& obs);
	virtual COrbitObservations* Add(SplineModelType type);
	virtual COrbitObservations* CreateType() const { return new COrbitObservations(type); };


	bool isa(string& className) const 
	{
		if (className == "COrbitObservationsArray")
			return true;

		return false;
	};
	string getClassName() const {return string("COrbitObservationsArray");};

};
#endif