#pragma once

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

typedef enum {Spot5,Quickbird,Alos,Formosat2,Theos,GeoEye1} SatelliteType;

class CSatelliteDataReader;


class CSatelliteDataReaderFactory
{
public:
	static CSatelliteDataReader* initSatelliteReader(SatelliteType type);
	
};
