#ifndef XMLDATAHANDLER_H
#define XMLDATAHANDLER_H


/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include <xercesc/sax/HandlerBase.hpp>

#include "CharString.h"
#include <vector>

XERCES_CPP_NAMESPACE_USE

XERCES_CPP_NAMESPACE_BEGIN
class AttributeList;
XERCES_CPP_NAMESPACE_END

class CXYZOrbitPointArray;


class CXMLDataHandler : public HandlerBase
{
public:
	typedef enum {noRead, readInt,readDouble,readString} ReaderType;
	
	CXMLDataHandler();
	~CXMLDataHandler();

    virtual void endElement(const XMLCh* const name) = 0;
	virtual void startElement(const XMLCh* const name, AttributeList& attributes) = 0;
    
	virtual void characters(const XMLCh* const chars, const XMLSize_t length);
    virtual void ignorableWhitespace(const XMLCh* const chars, const unsigned int length);

protected:
 
	ReaderType read;

	int* intReader;
	double* doubleReader;
	CCharString*	stringReader;

	vector<CCharString> xmlTag;

	int tagDepth;

};


#endif