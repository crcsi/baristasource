#ifndef __CSPOTDATA__
#define __CSPOTDATA__

#include "XYZOrbitPointArray.h"
#include "CharString.h"
#include "2DPointArray.h"

class CSpotData
{

public:

	int ncols;
	int nrows;
	int nbands;
	int bpp;
	
	CSpotData() :	aocsAngles(3), 
					aocsAnglesSpeed(3), 
					correctedAngles (3),
					datasetFramePoints(4),
					datasetSceneCenter(4),
					tiePoints(5),
					starTrackerUsed(false) {};
	~CSpotData(){};



	CXYZOrbitPointArray triodeOrbitPoints ;
	CXYZOrbitPointArray dorisOrbitpoints;
	CObsPointArray aocsAngles ;
	CObsPointArray aocsAnglesSpeed;
	CObsPointArray correctedAngles;
	CObsPointArray datasetFramePoints;
	CObsPointArray tiePoints;
	CObsPoint datasetSceneCenter;

	vector<C2DPointArray> lookAngles;

	double datasetFrameOrientation;

	CCharString metadataVersion;
	CCharString metatdatProfile;

	CCharString horizontalCsCode;
	CCharString horizontalCsType;
	CCharString horizontalCsName;

	CCharString dataStripID;

	double timeOffsetDorisTriode;
	double linePeriode;
	double sceneCenterTime;
	int sceneCenterLine;
	int sceneCenterCol;
	bool starTrackerUsed;
	int pixelOrgin;
};
#endif