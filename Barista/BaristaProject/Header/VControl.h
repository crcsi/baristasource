#ifndef __CVControl__
#define __CVControl__

#include "Serializable.h"
#include "Filename.h"
#include "XYPointArray.h"
#include "XYZPointArray.h"
#include "XYContourArray.h"
#include "ObservationHandler.h"

class CVControl : public CObservationHandler
{
protected:
	CXYZPointArray* pointArray;
  public:

    CVControl(CXYZPointArray *pointArray,CXYZPointArray *residualArray);

    ~CVControl();

	virtual const char *getFileNameChar() const;

	virtual bool isControl() const { return true; };

	CXYZPointArray* getXYZPointArray() const;
	CXYZPointArray* getXYZResidualPointArray() const;
};

#endif

