#ifndef __CVImagePtrArray__
#define __CVImagePtrArray__

#include "MUObjectPtrArray.h"
#include "VImage.h"

class CVImagePtrArray : public CMUObjectPtrArray<CVImage>
{
public:
    CVImagePtrArray(int nGrowBy = 0);
    ~CVImagePtrArray(void);
};

#endif