#ifndef __CVBase__
#define __CVBase__

#include "Serializable.h"
#include "Label.h"
#include "UpdateHandler.h"
#include "XYPointArray.h"
#include "XYPolyLineArray.h"
#include "SensorModel.h"
#include "LSM.h"

class CHugeImage;
class CTFW;

class CVBase : public CSerializable, public CUpdateHandler
{
public:
	CVBase(void);
	CVBase(const CVBase& src);
	~CVBase(void);
	
	void addMonoPlottedPoint(CXYPoint& p,bool sortArray = true);
	void removeMonoPlottedPoint(const CLabel& label);
	void removeMonoPlottedPoint(int index);
	void sortMonoPlottedPoints();
	int getNMonoPlottedPoints()const;
	CXYPointArray* getMonoPlottedPoints(){return &this->monoPlottedPoints;};
	CXYPoint* getMonoPlottedPoint(int index);
	CXYPoint* getMonoPlottedPoint(const CLabel& label);


	// serialize functions
	virtual void serializeStore(CStructuredFileSection* pStructuredFileSection);
	virtual void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	virtual bool isModified(){return this->bModified;};

	void changedData();

	virtual bool isa(string& className) const
	{
	  if (className == "CVBase")
		  return true;
	  return false;
	};

	virtual string getClassName() const {return string("CVBase"); }; 

	void setMonoPlottedPointsActive(bool b) {this->monoPlottedPoints.setActive(b);};

    virtual void setName(const char* filename);
	CFileName getItemText(){return this->name;}
	
	virtual const CSensorModel* getModel(enum eLSMTRAFOMODE trafomode = eLSMNOTFW) = 0;

	virtual CHugeImage* getHugeImage() { return 0;};
	
	virtual void init() {};

	virtual void setTFW(const CTFW& tfw) {};
	virtual const CTFW* getTFW() const{ return NULL;};
	virtual bool hasTFW() const {return false; };

protected:
	CXYPointArray monoPlottedPoints;
	CXYPolyLineArray xyMonoPlottedLines;

};
#endif