#pragma once

#include "ProgressHandler.h"
#include "VImage.h"
#include "Filename.h"

class CImageBuffer;


class CImageMerger : public CProgressHandler
{


public:
	enum InterpolationMethod{eNearestNeighbour,eBiLinear,eBiCubic};

	CImageMerger();
	~CImageMerger();

	bool init(const vector<CVImage*>& inputImages,
			  unsigned int startPos,
			  unsigned int endPos,
		      CCharString newCameraName,
			  CImageMerger::InterpolationMethod method,
			  CVImage* newMergedImage);

	void setInitWallisFilter(bool b) { this->initWallisFilter = b;};

	bool mergeImages();

	CCharString getErrorMessage() const {return this->errorMessage;};

	static int findBoundary(CHugeImage* himage, bool onRightSide);
	
	static bool sortImages(vector<CVImage*>& inputImages);

protected:

	bool computeCCDPositions(vector<double>& xPosCCD,vector<double>& yPosCCD,vector<CHugeImage*>& accordingImage,vector<unsigned int>& nextImage);
	bool initNewImage();
	bool resampleImages(const vector<double>& xPosCCD,const vector<double>& yPosCCD,const vector<CHugeImage*>& accordingImage,const vector<unsigned int>& nextImage);
	void transferPointObservations();

	

	vector<CVImage*> inputImages;
	unsigned int startColFirstImage;
	unsigned int endColLastImage;

	CFileName newFileName; 
	CFileName newHugeFileName;
	CCharString newCameraName;
	CVImage* newImage;

	CCharString errorMessage;

	unsigned int newImageWidth;
	unsigned int newImageHeight;

	int newImageBpp;
	int newImageBands;


	unsigned int firstIndexNewImage;
	unsigned int lastIndexNewImage;

	int minYValue;
	int maxYValue;

	int marginInterpolation;
	InterpolationMethod method;

	bool initWallisFilter;
};
