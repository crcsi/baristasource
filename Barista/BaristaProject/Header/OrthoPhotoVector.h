#ifndef __COrthoPhotoVector__
#define __COrthoPhotoVector__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "2DPoint.h"
#include "OrthoPhotoRecord.h"

#define DEFAULT_USE_BICUBIC false
#define DEFAULT_USE_ANCHOR_POINTS true
#define DEFAULT_ANCHOR_WIDTH 10


class CVDEM;

class COrthoPhotoVector : public  vector<COrthoPhotoRecord>
{
	bool oneImageOnly;
	bool bicubic;
	bool useAnchor;
	int anchorWidth;

	CVDEM *pDEM;

	C2DPoint LrROI, UlROI;

	bool fromROI;

	CCharString errorMessage;
	CCharString errorTitleMessage;

public:
	COrthoPhotoVector();
	~COrthoPhotoVector();

	// this function will initialize the image(s) and select a DEM from the project
	// it will also compute the extents, depending on the available DEM or ROI
	void init(CVImage *image, C2DPoint *lrc, C2DPoint *ulc);

	bool hasOneImageOnly() const { return this->oneImageOnly;};
	bool isInitialised() const;

	void setToBicubicInterpolation(bool b) { this->bicubic = b;};
	bool useBicubicInterpolation() const { return this->bicubic;};

	void setUseAnchorPoints(bool b) { this->useAnchor = b;};
	bool useAnchorPoints() const { return this->useAnchor;};

	void setAnchorWidth(int w) { this->anchorWidth = w;};
	int  getAnchorWidth() const { return this->anchorWidth;};

	// return error messages 
	CCharString getErrorMessage() const { return this->errorMessage;};
	CCharString getErrorTitleMessage() const { return this->errorTitleMessage;};

	void setDEM(CVDEM* newDEM);
	CVDEM* getDEM() { return this->pDEM;};

	bool initDEMBorders(int index);

	bool setMaxExtentsForAll();

	void setGSDForAll(double newGSD);

	//addition >>
	void covertCoordinates(int index);
	//void covertCoordinatesAll();
	//<< addition
protected:

	bool initImg(CVImage *image);
	
	void initImgList();

	bool initAreafromROI(int index);

	

};
#endif // __COrthoPhotoVector__
