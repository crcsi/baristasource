#ifndef __CVALSPtrArray__
#define __CVALSPtrArray__

#include "MUObjectPtrArray.h"
#include "VALS.h"

class CVALSPtrArray : public CMUObjectPtrArray<CVALS>
{
  public:
    CVALSPtrArray(int nGrowBy = 0);
    ~CVALSPtrArray();
};

#endif