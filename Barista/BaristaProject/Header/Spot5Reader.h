#ifndef __CSpot5Reader__
#define __CSpot5Reader__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "SatelliteDataReader.h"
#include "SpotData.h"

class CSpot5Reader : public CSatelliteDataReader
{
public:
	CSpot5Reader(void);
	virtual ~CSpot5Reader(void);

	virtual bool readData(CFileName &filename);

protected:
	virtual bool prepareEphemeris();
	virtual bool prepareAttitudes();
	virtual bool prepareMounting(){return true;};


	bool prepareLookAngles();

	void computeTangentialMatrix(CRotationMatrix& mat,const C3DPoint& location, const C3DPoint& velocity);

	double lagrange(double x[],double y[],double time);
private:
	CSpotData dataStorage;


};
#endif