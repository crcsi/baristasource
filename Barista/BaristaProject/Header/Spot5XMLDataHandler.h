#ifndef SPOT5XMLDATAHANDLER_H
#define SPOT5XMLDATAHANDLER_H

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include "XMLDataHandler.h"

class CXYZOrbitPointArray;
class CSpotData;

class CSpot5XMLDataHandler : public CXMLDataHandler
{
public:
	CSpot5XMLDataHandler(CSpotData& dataStorage);
	~CSpot5XMLDataHandler(void);

    virtual void endElement(const XMLCh* const name);
	virtual void startElement(const XMLCh* const name, AttributeList& attributes);

private:
 
	CSpotData& dataStorage;

	bool readSubElement;
	
	bool readMetadata_Id;
	bool readDataset_Frame;
	bool	readVertex;
	bool	readScene_Center;
	bool readCoordinate_Reference_System;
	bool	readHorizontal_CS;
	bool readRaster_CS;
	bool readGeoposition;
	bool	readGeoposition_Points;
	bool		readTie_Point;
	bool readRaster_Dimensions;
	bool readRaster_Encoding;
	bool readData_Strip;
	bool	readData_Strip_Identification;
	bool	readSatellite_Time;
	bool	readEphemeris;
	bool		readPoints;
	bool			readPoint;
	bool				readLocation;
	bool				readVelocity;
	bool		readDoris_Points;
	bool			readDoris_Point;
	bool				readDorisLocation;
	bool				readDorisVelocity;
	bool	readSensor_Configuration;
	bool		readTime_Stamp;
	bool		readInstrument_Look_Angles_List;
	bool			readInstrument_Look_Angles;
	bool				readLook_Angles_List;
	bool					readLook_Angles;
	bool	readSatellite_Attitudes;
	bool		readRaw_Attitudes;
	bool			readAocs_Attitudes;
	bool				readAngle_List;
	bool					readAocsAngles;
	bool				readAngular_Speed_List;
	bool					readAngular_Speeds;
	bool			readStar_Tracker_Attitudes;
	bool				readQuaternion_List;
	bool					readQuaternion;
	bool		readCorrected_Attitudes;
	bool			readCorrected_Attitude;
	bool				readCorrectedAngles;

	double x;
	double y;
	double z;
	double vx;
	double vy;
	double vz;
	CCharString time;
	CCharString flag;
	CCharString flag2;
	bool hasVelocity;
	int bandIndex;
	int detectorID;
};
#endif //SPOT5XMLDATAHANDLER_H