#ifndef __CQuickbirdReader__
#define __CQuickbirdReader__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */


#include "SatelliteDataReader.h"
#include "ObsPointArray.h"

class Matrix;


class CQuickbirdReader : public CSatelliteDataReader
{
public:
	CQuickbirdReader(void);
	~CQuickbirdReader(void);

	virtual bool readData(CFileName &filename);



protected:
	virtual bool prepareEphemeris() {return true;};
	virtual bool prepareAttitudes();
	virtual bool prepareMounting();

	virtual void computeInterval(double &startTime, double& endTime);
	
	void computeError(Matrix& covar,const CRotationMatrix& rpy, const CObsPoint& quaternion) const;

	bool readEphemeris();
	bool readAttitudes();
	bool readGeoCalib();
	bool readImageMetaFile();
	


	CFileName ephemerisFile;
	CFileName attitudeFile;
	CFileName geoCalFile;
	CFileName imageMetaFile;

	CObsPointArray quaternions;

	double pd;

	double cx;
	double cy;
	double cz;
	double qcs1;
	double qcs2;
	double qcs3;
	double qcs4;

	double detOriginX;
	double detOriginY;
	double detRotAngle;
	double detPitch;

	CRotationMatrix mountingTemp;

	bool isReverse;

};
#endif