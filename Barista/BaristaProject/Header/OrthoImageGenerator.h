#pragma once

#include "Filename.h"
#include "VImage.h"
#include "ProgressHandler.h"

class CVDEM;
class CSensorModel;

class C3DTrans;
class CImageBuffer;

class COrthoImageGenerator : public CProgressHandler
{
  protected:

	  CVImage *inputimage;

	  CSensorModel *sensorModel;

	  CVDEM* vdem;

	  CTrans3D     *trans;

	  int nBands;

	  int bpp;

	  int orthoRows;

	  int orthoCols;

	  int originalRows;

	  int originalCols;

	  int inputPyrLevel;

	  double inputPyrFactor;

	  bool useAnchor;

	  double llx;

	  double lly;

	  double urx;

	  double ury;

	  double gsd;

	  bool bicubic;

	  int AnchorWidth;

	  CCharString errorMessage;

	  //addition >>
	  bool isUTM; //true for UTM & false for Geographic
	  //<< addition
  public:

	  COrthoImageGenerator();


	  ~COrthoImageGenerator();

	  CVImage *orthoimage;

	  void initDEMBorders();

	  bool makeOrthoImage();

	  void setImage(CVImage *img);

	  void setDEM(CVDEM *dem);

	  CVImage* getImage() const { return this->inputimage; };

	  CSensorModel *getSensorModel() const { return this->sensorModel; };

	  const char *getSensorModelFileName() const { return sensorModel->getFileNameChar(); };

	  double getLLX() const { return this->llx; };
	  double getLLY() const { return this->lly; };
	  double getURX() const { return this->urx; };
	  double getURY() const { return this->ury; };
	  double getGsd() const { return this->gsd; };

	  bool getUseAnchor() const { return this->useAnchor; }
	  bool getBicubic() const { return this->bicubic; }

	  void setLLX(double val) { this->llx = val; };
	  void setLLY(double val) { this->lly = val; };
	  void setURX(double val) { this->urx = val; };
	  void setURY(double val) { this->ury = val; };
	  void setGsd(double val) { this->gsd = val; };

	  void setUseAnchor(bool val) { this->useAnchor = val; }
	  void setAnchorWidth(int width) { this->AnchorWidth = width; }
	  void setBicubic(bool val)   { this->bicubic = val; }

	  //addition >>
	  void setCoordinateType(bool ctype) {this->isUTM = ctype;}; //true for UTM & false for Geographic
	  bool getCoordinateType() {return this->isUTM;}; //true for UTM & false for Geographic
	  //<< addition

	  CCharString getErrorMessage() const { return this->errorMessage;};

  protected:

	  bool computeImgCoordsForTile(C2DPoint &firstDEMPoint, double *xArray, double *yArray,
								   double &xmin, double &xmax,double &ymin, double &ymax);

	  bool computeImgCoordsForTile(C2DPoint &firstDEMPoint, double *xArray, double *yArray,
								   double &xmin, double &xmax,double &ymin, double &ymax,
								   double AnchorFactor);


	  bool initDEMbuffer(CImageBuffer &buf, C2DPoint &shift,C2DPoint &pix,
		                 double &xmin, double &xmax, double &ymin, double &ymax,
						 int AnchorFactor);

	  bool CreateOrthoImage();

	  bool LoopTiles();


	  void Create8bitTileBilin(unsigned char *orthoBuf, double *xArray, double *yArray,
							   const CImageBuffer &imgBuf);

	  void Create16bitTileBilin(unsigned short *orthoBuf, double *xArray, double *yArray,
								const CImageBuffer &imgBuf);

	  void Create8bitTileBicub(unsigned char *orthoBuf, double *xArray, double *yArray,
							   const CImageBuffer &imgBuf);

	  void Create16bitTileBicub(unsigned short *orthoBuf, double *xArray, double *yArray,
								const CImageBuffer &imgBuf);
	
	  int setPyrLevel();
};
