#ifndef XMLERRORHANDLER_H
#define XMLERRORHANDLER_H
#include <xercesc/sax/HandlerBase.hpp>

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

XERCES_CPP_NAMESPACE_USE

XERCES_CPP_NAMESPACE_BEGIN
class AttributeList;
XERCES_CPP_NAMESPACE_END

#include "CharString.h"
#include <vector>
using namespace std;

class CXMLErrorHandler : public HandlerBase
{
public:
	CXMLErrorHandler();
	CXMLErrorHandler(vector<CCharString>& warnings,vector<CCharString>& errors,vector<CCharString>& fatalErrors);
	~CXMLErrorHandler(void);

	void warning(const SAXParseException& exc);
    void error(const SAXParseException& exc);
    void fatalError(const SAXParseException& exc);

private :
	
	bool recordData;
	vector<CCharString>& warnings;
	vector<CCharString>& errors;
	vector<CCharString>& fatalErrors;

	vector<CCharString> dummyWarnings;
	vector<CCharString> dummyErrors;
	vector<CCharString> dummyFatalErrors;

};

#endif
