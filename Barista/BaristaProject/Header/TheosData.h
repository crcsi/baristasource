#ifndef __CTHEOSDATA__
#define __CTHEOSDATA__

#include "XYZOrbitPointArray.h"
#include "CharString.h"
#include "2DPointArray.h"
#include "3DPointArray.h"

class CTheosData
{

public:

	int ncols;		//Raster_Dimensions.NCOLS
	int nrows;		//Raster_Dimensions.NROWS
	int nbands;		//Raster_Dimensions.NBANDS
	int bpp;		//Raster_Encoding.NBITS

	CTheosData() :	attitudeQuaternions(4),					
					datasetFramePoints(4),
					datasetSceneCenter(4),
					tiePoints(4),
					polynomialLookAngles(8),					
					instrumentBiases(3){};
	~CTheosData(){};



	CXYZOrbitPointArray triodeOrbitPoints;
	
	double UT1_UTC,UTC_GPST;
	C2DPoint UV;	//added
	C3DPointArray GPSTimes;	//Including year,month and date, while secs is saved in attitudeQuaternions.dot
	CObsPointArray attitudeQuaternions;	//added	

	CObsPointArray datasetFramePoints;
	CObsPointArray tiePoints;
	CObsPoint datasetSceneCenter;
	
	CObsPoint polynomialLookAngles;		//added, Theos provides polynomial other than look angle list
	
	CObsPoint instrumentBiases;	//added, Theos provides yaw,pitch,roll biases between Rlos and Rsat

	double datasetFrameOrientation;

	CCharString metadataVersion;
	CCharString metatdatProfile;

	CCharString horizontalCsCode;
	CCharString horizontalCsType;
	CCharString horizontalCsName;

	CCharString dataStripID;


	double linePeriod;
	double referenceTime;	//added
	int referenceLine;		//added

	int pixelOrgin;
};
#endif