/**
 * Title:        Barista
 * Description:
 * Copyright:    Copyright (c) Franz Rottensteiner, Harry Hanley, Paul Dare, Simon Cronk
 * Company:      University of Melbourne
 * @author Franz Rottensteiner, Harry Hanley
 * @version 1.0
 */

#pragma once

#include "BundleManager.h"


class CBaristaBundleHelper : public CBundleManager
{
  protected:
    
  
  public:
    CBaristaBundleHelper(float I_convergenceLimit = 0.0001, float MaxRobustPercentage = 0.1,
		                 float MinRobFac = 3.0, float havg = 0.0, const CCharString &protfile = "");

    ~CBaristaBundleHelper(void);

    virtual void bundleInit();

    void bundleInit(CObservationHandler *obs, bool useControlPoints = true);

	void bundleInit(CObservationHandlerPtrArray *images,CObservationHandlerPtrArray *orbits,bool forwardOnly,bool excludeTie);

 	virtual void updateResiduals();

	virtual void setSensorFilenames();

	void setMaxRobustPercentage(double maxRobustPercentage) {this->maxRobustPercentage = maxRobustPercentage;};

	void setMinRobFac(double minRobFac) {this->minRobFac = minRobFac;};

	bool isa(string& className) const
	{
		if (className == "CBaristaBundleHelper")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CBaristaBundleHelper");
	};

	bool isModified() { return false; };

	virtual bool determineFixedRotMatForOrbit(CRotationMatrix& fixedRotMat,const CCharString& orbitName)const;

  protected:
	  
	double getAverageHeightFromReduction();

};

