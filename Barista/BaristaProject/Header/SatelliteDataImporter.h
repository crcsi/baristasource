#ifndef __CSatelliteDataImporter__
#define __CSatelliteDataImporter__


/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 */

#include <vector>
#include "ProgressHandler.h"
#include "SatelliteDataReaderFactory.h"
#include "Filename.h"

class COrbitObservations;
class CSensorModel;
class CCameraMounting;
class CCamera;
class CSatelliteDataReader;


class CSatelliteDataImporter : public CProgressHandler
{
public:
	CSatelliteDataImporter(SatelliteType type);
	~CSatelliteDataImporter(void);

	// public function to read and adjust the orbit path and attitudes
	bool importMetadata(CFileName filename);

	bool recomputeOrbit(bool adjustPath,bool adjustAttitudes);

	// if an error occurs, the gui will present this message to the user
	CCharString getErrorMessage() {return this->errorMessage;};


	void cancelImport();


	double getSatelliteHeight() const { return this->satelliteHeight;};
	double getGroundHeight() const { return this->groundHeight;};
	double getGroundTemperature() const { return this->groundTemperature;};
	double getAimedRMSPath() const;
	double getAimedRMSAttitudes() const;
	double getCurrentRMSPath() const;
	double getCurrentRMSAttitudes() const;
	double getFirstLineTime() const;
	double getTimePerLine() const;

	CFileName getFileName() const {return this->filename;};
	CSensorModel* getOrbitPath();
	CSensorModel* getOrbitAttitudes();
	CCameraMounting* getCameraMounting() { return this->knownCameraMountings[this->selectedCameraMounting];};
	CCamera* getCamera() {return this->knownCameras[this->selectedCamera];};
	
	void getKnownOrbitPaths(vector<CCharString>& names,unsigned int& selected) const;
	void getKnownOrbitAttitudes(vector<CCharString>& names,unsigned int& selected) const;
	void getKnownCameraMountings(vector<CCharString>& names,unsigned int& selected) const;
	void getKnownCameras(vector<CCharString>& names,unsigned int& selected) const;

	int getActiveAdditionalParameter()const;
	int getActiveInteriorParameter() const;
	int getActiveMountingRotationParameter()const;
	int getActiveMountingShiftParameter()const;

	//addition>>
	bool getSatelliteType(){return this->satellite;};
	//<<addition

	void setAimedRMSPath(double rms);
	void setAimedRMSAttitudes(double rms);
	void setSatelliteHeight(double value) {this->satelliteHeight = value;};
	void setGroundHeight(double value) { this->groundHeight = value;};
	void setGroundTemperature(double value) { this->groundTemperature = value;};
	
	void selectPath(unsigned int index);
	void selectAttitudes(unsigned int index);
	void selectCameraMounting(unsigned int index);
	void selectCamera(unsigned int index);




private:

	void collectParameter();

	void selectPath(COrbitObservations* existingPath);
	void selectAttitudes(COrbitObservations* existingAttitudes);
	void selectCameraMounting(CCameraMounting* existingMounting);
	void selectCamera(CCamera* existingCamera);


	vector<COrbitObservations*> knownOrbitAttitudes;
	vector<COrbitObservations*> knownOrbitPaths;
	vector<CCameraMounting*> knownCameraMountings;
	vector<CCamera*> knownCameras;

	unsigned int selectedPath;
	unsigned int selectedAttitudes;
	unsigned int selectedCameraMounting;
	unsigned int selectedCamera;

	double satelliteHeight;
	double groundHeight;
	double groundTemperature;

	CSatelliteDataReader* satelliteReader;
	SatelliteType satellite;

	CFileName filename;

	CCharString errorMessage;
};
#endif