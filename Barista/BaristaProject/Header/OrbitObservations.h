#ifndef __CORBITOBSERVATIONS__
#define __CORBITOBSERVATIONS__

#include "Serializable.h"
#include "Filename.h"
#include "ObservationHandler.h"
#include "XYZOrbitPointArray.h"
#include "SplineModelFactory.h"

class COrbitPointArray;

class COrbitObservations : public CObservationHandler, public CSerializable
{

public:
	COrbitObservations(void);
	COrbitObservations(SplineModelType type);
	COrbitObservations(const COrbitObservations& orbitObservation);
	virtual ~COrbitObservations();

	virtual const char *getFileNameChar() const;
	void setFileName(const char* filename);

	virtual	void setName(CCharString name);

	CXYZOrbitPointArray* initObservations();
	
	void setSplineModelParameter(int splineDegree,doubles& knots);


	// serialize functions
	void serializeStore(CStructuredFileSection* pStructuredFileSection);
	void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	bool isModified();
	void reconnectPointers();
	void resetPointers();
	
	bool isa(string& className) const
	{
		if (className == "COrbitObservations")
			return true;

		return false;
	};
	string getClassName() const {return string("COrbitObservations");};

	virtual int getNOrbitPoints() const;
	SplineModelType getSplineType() const { return this->splineType;};

	void setSplineType(SplineModelType type) {this->splineType = splineType;};

	void goingToBeDeleted();

	int getFirstPointIndex() const { return this->indexFirstPoint;};
	int getLastPointIndex() const { return this->indexLastPoint;};

	void setFirstPointIndex(int index) { this->indexFirstPoint = index;};
	void setLastPointIndex(int index) { this->indexLastPoint = index;};

protected:
	// the observations
	CXYZOrbitPointArray orbitPoints;	// position and velocity
	CSplineModel* splineModel;

	SplineModelType splineType;

	int indexFirstPoint;	// first point in interval
	int indexLastPoint;		// last point in interval
};

#endif