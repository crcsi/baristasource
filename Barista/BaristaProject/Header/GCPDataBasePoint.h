#ifndef __CGCPDATABASEPOINT__
#define __CGCPDATABASEPOINT__


//#include "Filename.h"
#include "XYZPoint.h"
//#include "XYPoint.h"
#include "ReferenceSystem.h"
#include "GCPData2DPoint.h"

class CGCPDataBasePoint : public CXYZPoint
{

public:
	CGCPDataBasePoint(void): CPointBase(3),	fromGPS(true),type(eGRID) {};
	CGCPDataBasePoint(const CGCPDataBasePoint& point) : CPointBase(point),CXYZPoint(point), gcpPoint2DArray(point.gcpPoint2DArray),
		                                                 type(point.type), date(point.date), fromGPS(point.fromGPS) {};
	
	virtual CGCPDataBasePoint &operator = (const CGCPDataBasePoint & point);
	virtual ~CGCPDataBasePoint() {};


	// get functions
	eCoordinateType getCoordinateType() const { return this->type;};
	vector<CGCPData2DPoint> getGCPPoint2DArray() {return this->gcpPoint2DArray;}
	int get2DPointArraySize() const {return this->gcpPoint2DArray.size();};
	const CGCPData2DPoint& get2DPointAt(int index) const {return this->gcpPoint2DArray.at(index);};
	//new
	//CGCPData2DPoint get2DPoint() {return this->gcp2dPoint;};

	// set functions
	void setCoordinateType(eCoordinateType ctype);
	void setGCPPoint2DArray(vector<CGCPData2DPoint> &Points);
	void setDate(CCharString dt);
	void setFromGPS(bool gps);
	void set2DPoint(CGCPData2DPoint pt);
	void setGCPPoint2DArrayClear() {this->gcpPoint2DArray.clear();};
	//void set2DPoint(CGCPData2DPoint P2d) {this->gcp2dPoint=P2d;};

protected:
	
	//3D information
	// type of the 3d coordinates
	eCoordinateType type;
	// date at which the point was acquired
	CCharString date;
	// indication if acquired with GPS
	bool fromGPS;
	//2D observation point array
	vector<CGCPData2DPoint> gcpPoint2DArray;
	//CGCPData2DPoint gcp2dPoint;
};

#endif