#ifndef __CTheosReader__
#define __CTheosReader__

/**
 * Copyright:    Copyright (c) Shijie Liu<p>
 * Company:      University of Melbourne<p>
 * author:		 Shijie Liu
 * version 1.0
 */

#include "SatelliteDataReader.h"
#include "TheosData.h"

class CTheosReader :	public CSatelliteDataReader
{
public:
	CTheosReader(void);
	virtual	~CTheosReader(void);

	virtual bool readData(CFileName &filename);

protected:
	virtual bool prepareEphemeris();
	virtual bool prepareAttitudes();
	virtual bool prepareMounting(){return true;};

	bool prepareLookAngles();

	void computeTangentialMatrix(CRotationMatrix& mat,const C3DPoint& location, const C3DPoint& velocity);

	double thetaECI2ECF(int Y,int M,int D,double secs,double UT1_UTC,double UTC_GPST);	

	void calcRotECI2ECF(CRotationMatrix& mat,int Y,int M,int D,double secs,double UT1_UTC,double UTC_GPST);

	double julianDay(int Y,int M,int D);

	double julianDay(int Y,int M,double D);

	void calcPI(double xp,double yp,CRotationMatrix &mat);

	void calcRotGAST(double jd,double UT1,double ep,double delpsi,CRotationMatrix &mat);

	void calcPrecession(double T,CRotationMatrix &rotP);

	void calcNutation(double T,CRotationMatrix &rotN, double &ep,double &delpsi);

	
private:
	CTheosData dataStorage;

	CRotationMatrix mountingTemp;

	//camera mounting shift parameters
	double cx; 
	double cy;
	double cz;
};
#endif