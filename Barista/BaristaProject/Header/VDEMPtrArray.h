#ifndef __CVDEMPtrArray__
#define __CVDEMPtrArray__

#include "MUObjectPtrArray.h"
#include "VDEM.h"

class CVDEMPtrArray : public CMUObjectPtrArray<CVDEM>
{
public:
    CVDEMPtrArray(int nGrowBy = 0);
    ~CVDEMPtrArray(void);
};

#endif