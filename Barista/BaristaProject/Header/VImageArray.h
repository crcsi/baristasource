#ifndef __CVImageArray__
#define __CVImageArray__

#include "VImage.h"
#include "VImagePtrArray.h"
#include "MUObjectArray.h"
#include "UpdateHandler.h"

class CVImageArray : public CMUObjectArray<CVImage, CVImagePtrArray>, public CSerializable, public CUpdateHandler
{
public:
	CVImageArray(int nGrowBy = 0);
	~CVImageArray(void);


	CVImage* Add();
	CVImage* Add(const CVImage& image);

    // CSerializable functions
    void serializeStore(CStructuredFileSection* pStructuredFileSection);
    void serializeRestore(CStructuredFileSection* pStructuredFileSection);

    void reconnectPointers();
    void resetPointers();

    bool isModified();

	int getbyName(CCharString* name) const;
	CVImage* getImagebyName(CCharString* name) const;

    bool isa(string& className) const
	{
		if (className == "CVImageArray")
			return true;

		return false;
	};

	string getClassName() const
	{
		return string("CVImageArray");
	};

};

#endif
