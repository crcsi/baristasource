#ifndef __CVALS__
#define __CVALS__

#include "Serializable.h"
#include "Filename.h"
#include "ALSData.h"

#include "ReferenceSystem.h"
#include "DisplaySettings.h"
#include "GenericPointCloud.h"
class CHugeImage;
class CALSDataSetWithTiles;

class CVALS : public CSerializable
{
  public:
 
	  CVALS();

	  CVALS(const CVALS& src);

	  ~CVALS();

	  CFileName getFileName() const { return this->alsFileName; };

	  const char *getFileNameChar() const { return this->alsFileName.GetChar(); };

	  CFileName   getDiskFileName() const { return this->als->getName(); };

	  const char *getDiskFileNameChar() const { return this->als->getFileNameChar(); };

	  bool initALSStrip(const char *filename, eALSFormatType formatType, 
		                eALSPulseModeType PulseMode, const CReferenceSystem &ref,
					    const C2DPoint &offset);

	  bool initALSTile(const char *filename, eALSFormatType formatType, 
		               eALSPulseModeType PulseMode, const CReferenceSystem &ref,
					   const C2DPoint &offset, const C2DPoint &delta);

	  CALSDataSetWithTiles *initALSTile(const char *filename, vector<CALSDataSet *> &datasetVec, bool mergeDatasets);

	  void setFileName(const char* filename);

	  CFileName  getItemText();
  
	  CALSDataSet* getCALSData() const { return this->als; };
  
	  bool isa(string& className) const
	  {
		  if (className == "CVALS") return true;
		  return false;
	  };

	  string getClassName() const
	  {
		  return string("CVALS");
	  };

	  // CSerializable
	  void serializeStore(CStructuredFileSection* pStructuredFileSection);
	  void serializeRestore(CStructuredFileSection* pStructuredFileSection);
	  void reconnectPointers();
	  void resetPointers();
	  bool isModified();

	  void setReferenceSystem(const CReferenceSystem &referenceSystem);

	  CReferenceSystem getReferenceSystem() const;

	  bool createIntensityImage(CHugeImage *img, const char *filename, const C2DPoint &pmin, 
		                        const C2DPoint &pmax, const C2DPoint &gridS, int N, int echo, 
								int level, eInterpolationMethod method, double maxDist);

	  CDisplaySettings* getDisplaySettings() { return &this->displaySettings;};

	  bool exportASCII(const char *filename, int pulse, bool exportTime, bool exportI);

  protected:
  
	  CFileName alsFileName;

	  CALSDataSet *als;
	  
	  CDisplaySettings displaySettings;

};

#endif
