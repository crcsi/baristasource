#ifndef __CAlosReader__
#define __CAlosReader__

#include "SatelliteDataReader.h"
#include "XYZOrbitPointArray.h"
#include "assert.h"
#include "ObsPoint.h"
#include "TFW.h"
#include "RPC.h"

class CPushBroomScanner;
class CDistortion;

#define READBUFFER_LENGTH 200


class CAlosReader : public CSatelliteDataReader
{
public:

	CAlosReader(void);

	CAlosReader(bool directReadMode);

	virtual ~CAlosReader(void);

	virtual bool readData(CFileName &filename);

	bool readPrecisionData(CFileName imageFile,CFileName dataDirectoryAttitudes,CFileName dataDirectoryPath,CPushBroomScanner* scanner);

	virtual bool hasTFW() const { return tfw.gethasValues(); }
		
	virtual CTFW *getTFW()  { return &tfw; }

	virtual bool hasRPC() const { return this->rpc.gethasValues(); }
		
	virtual CRPC *getRPC()  { return 0; }

	virtual void setDirectObservations(COrbitPathModel* path,COrbitAttitudeModel* attitudes) const;

	const vector<double>& getLineCoefficients() const {return coefficientsLine;};
	const vector<double>& getSampleCoefficients() const {return coefficientsSample;};
	const vector<double>& getLatCoefficients() const {return coefficientsLamda;};
	const vector<double>& getLonCoefficients() const {return coefficientsPhi;};
	
	static bool isALOS_PRISMCamera(CCharString cameraName);
	static bool isALOS_AVNIR2Camera(CCharString cameraName);

protected:

	virtual bool prepareEphemeris();
	virtual bool prepareAttitudes();
	virtual bool prepareMounting(){return true;};

	bool computeInnerOrienation(vector<vector<CObsPoint> >& camera,int ccd);
	doubles computeAVNIRDistortion(float focalLength,float referenceFocalLength,float instOffsetX,float instOffsetY, float instRotation,float detectorPitch);
	

	bool computeRPC(int zone, int hemisphere);
	bool computeTFW(double easting, double northing,int zone, int hemisphere);

	bool readVolumeFile();
	bool readLeaderFile();
	bool readSupplFile();
	bool readImageFile();

	bool readCommonPart(CFileName& filename,int& ccdElement);

	bool readFileDescriptor();

	bool readSupplFileDescripor();
	bool readLeaderFileDescriptor();

	bool hermite(CXYZOrbitPoint &newPoint,
				 double x[],
				 double y[],
				 double z[],
				 double vx[],
				 double vy[],
				 double vz[],
				 double time[]);

	bool readSceneHeader();

	bool readAncillary1();
	bool readAncillary2();
	bool readAncillary3();
	bool readAncillary4();
	bool readAncillary5();
	bool readAncillary6();
	bool readAncillary7();
	bool readAncillary8(bool readSeparateFile = false);
	bool readAncillary9();
	bool readAncillary10();
	bool readAncillary11();
	bool readAncillary12(bool readSeparateFile,bool appendData);
	
	bool readAncillary13_PRISM();
	bool readAncillary13_AVNIR();
	
	bool readOrbitPathSection(CXYZOrbitPointArray* pointArray);

	double transformTime(int julianDate,int hour,int min,double sec);
	int julianDay(int year,int month,int day);
	void getStartAndEndTime(double& startTime,double& endTime,bool forAttitudes=false);

	void initOldALOSCamera();
	void initNewALOSCamera();


	// file reading functions
	bool readInt(int length, int &variable);
	bool readString(int length, CCharString& string);
	bool readDouble(int length,double& variable);
	bool readBinary(int base,unsigned short& variable,bool swapBytes= true);
	bool readBinary(int base,unsigned int& variable,bool swapBytes = true);
	bool readBinary(int base,unsigned char& variable);
	bool readBinary(int base,double& variable,bool swapBytes = false);
	bool readTime(int &hour, int &min, double &sec,bool withDot= false);
	bool readDate(int &year, int &month, int & day);

	void skip(int length)
	{
		this->file.seekg(length,ios::cur);
		this->filePos += length;
	};

	void goToPos(int pos)
	{
		this->file.seekg(0,ios::beg);
		this->skip(pos);
		this->filePos = pos;
	};

	bool closeFile();
	bool openFile(CFileName& filename);

private:
	void allocateMemory(int length)
	{
			if (this->readBuffer) delete [] this->readBuffer;

			this->readBuffer= new char[length + 1];
			this->bufferLength =length;
	};



private:
	CFileName volumeFile;
	CFileName leaderFile;
	CFileName trailerFile;
	CFileName supplFile;
	CFileName imageFile;

	// Ancillary8
	int nOrbitPoints;
	int nOrbitPointLength;
	int nTaiUTCData;
	int timeOffsetTAI_UTC;
	CXYZOrbitPointArray tmpOrbitPoints;

	// Ancillary10
	double gastAngle;
	double gastVelocity;
	double gastTime;
	vector<CRotationMatrix> XYMatrix;
	vector<CRotationMatrix> PNMatrix;
	vector<double> matrixTime;

	// Ancillary12
	int nAttitudePoints;
	int nAttitudePointLength;
	vector<CRotationMatrix> AttitudeMatrix;
	vector<double> attitudeTime;


	// Ancillary13
	vector<vector<CObsPoint> > nadirCamera;
	vector<vector<CObsPoint> > backwardCamera;
	vector<vector<CObsPoint> > forwardCamera;

	// leader File
	int sceneCenterDate;	// julian date
	double sceneCenterTime; // sec per day
	

	// image file
	int firstLineDate;	// julian date

	// rpc parameter
	vector<double> coefficientsPhi;
	vector<double> coefficientsLamda;
	vector<double> coefficientsSample;
	vector<double> coefficientsLine;
	vector<double> coefficientsF4;


	// supplemental File
	int nAncillary4;
	int lengthAncillary4;
	int nAncillary5;
	int lengthAncillary5;
	int nAncillary6;
	int lengthAncillary6;
	int nAncillary7;
	int lengthAncillary7;
	int nAncillary8;
	int lengthAncillary8;
	int nAncillary9;
	int lengthAncillary9;
	int nAncillary10;
	int lengthAncillary10;
	int nAncillary11;
	int lengthAncillary11;
	int nAncillary12;
	int lengthAncillary12;
	int nAncillary13;
	int lengthAncillary13;


	CTFW tfw;
	CRPC rpc;

	bool directReadMode;
	bool readAVNIR;
	bool readNITF;


	// file handling
	ifstream file;
	int filePos;
	char* readBuffer;
	int bufferLength;

};





inline bool CAlosReader::openFile(CFileName& filename)
{
	this->closeFile();

	this->file.open(filename.GetChar(), ios::in | ios::binary);
	if (!this->file.good())
	{
		this->errorMessage.Format("File: %s not found or file not readable!",filename.GetFileName().GetChar());
		return false;
	}

	this->filePos = 0;
	return true;
}


inline bool CAlosReader::closeFile()
{
	if (this->file.is_open())
		this->file.close();

	return true;
}

inline bool CAlosReader::readInt(int length, int &variable)
{

	CCharString tmp;
	if (!this->readString(length,tmp))
		return false;

	tmp.trim();
	variable = atoi(tmp.GetChar());
	return true;

};

inline bool CAlosReader::readString(int length, CCharString& string)
{
	if (this->bufferLength < length)
		this->allocateMemory(length);

	this->file.read(this->readBuffer,length);
	if (this->file.gcount() != length)
	{
		this->errorMessage = "Reading error";
		return false;
	}

	this->readBuffer[length] = '\0';

	string = this->readBuffer;
	this->filePos += length;
	return true;
}


inline bool CAlosReader::readDouble(int length,double& variable)
{
	CCharString tmp;
	if (!this->readString(length,tmp))
		return false;

	tmp.trim();
	variable = atof(tmp.GetChar());
	return true;
}

inline bool CAlosReader::readBinary(int base, unsigned int& variable,bool swapBytes)
{
	char* dummy = (char*)&variable;

	// read 4 bytes == 1 int
	this->file.read(dummy,4);


	// swap the order of the bytes
	if (swapBytes)
	{
		for (int k = 0; k < 2; ++k)
		{
			char a = dummy[k];
			dummy[k] = dummy[3 - k];
			dummy[3 - k] = a;
		}
	}
	this->filePos += 4;
	return true;
}

inline bool CAlosReader::readBinary(int base, double& variable,bool swapBytes)
{
	char* dummy = (char*)&variable;

	// read 4 bytes == 1 int
	this->file.read(dummy,8);

	
	if (swapBytes)// swap the order of the bytes
	{
		for (int k = 0; k < 4; ++k)
		{
			char a = dummy[k];
			dummy[k] = dummy[7 - k];
			dummy[7 - k] = a;
		}
		this->filePos += 8;
	}
	return true;
}


inline bool CAlosReader::readBinary(int base, unsigned short& variable,bool swapBytes)
{
	char* dummy = (char*)&variable;
	this->file.read(dummy,2);
	if (swapBytes)
	{
		char a = dummy[0];
		dummy[0] = dummy[1];
		dummy[1] = a;
	}

	this->filePos += 2;
	return true;
}

inline bool CAlosReader::readBinary(int base, unsigned char& variable)
{
	char* dummy = (char*)&variable;
	this->file.read(dummy,1);
	this->filePos += 1;
	return true;
}


inline bool CAlosReader::readTime(int &hour, int &min, double &sec,bool withDot)
{
	this->readInt(2,hour);
	this->readInt(2,min);
	int intSec;
	this->readInt(2,intSec);
	int mil;
	if (withDot)
		this->skip(1);
	this->readInt(6,mil);
	sec= intSec + (double) mil/1000000.0;
	return true;
}

inline bool CAlosReader::readDate(int &year, int &month, int & day)
{
	this->readInt(4,year);
	this->readInt(2,month);
	this->readInt(2,day);

	return true;
}



#endif
