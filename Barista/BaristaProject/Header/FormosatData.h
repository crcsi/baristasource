#ifndef __CFORMOSATDATA__
#define __CFORMOSATDATA__

#include "XYZOrbitPointArray.h"
#include "CharString.h"
#include "2DPointArray.h"

class CFormosatData
{

public:

	int ncols;
	int nrows;
	int nbands;
	int bpp;
	
	CFormosatData() : correctedAngles (3),
					rasterFramePoints(4),
					quaternions(4),
					biases(3),
					rasterSceneCenter(4) {};
	~CFormosatData(){};


	CXYZOrbitPointArray tmpOrbitPoints ;
	CObsPointArray correctedAngles;
    CObsPointArray rasterFramePoints;
    CObsPointArray quaternions;
	CObsPoint biases;	
	CObsPoint rasterSceneCenter;	

	vector<C2DPointArray> lookAngles;

	double RasterFrameOrientation;

	CCharString metadataVersion;
	CCharString metatdatProfile;

	CCharString horizontalCsCode;
	CCharString horizontalCsType;
	CCharString horizontalCsName;

	CCharString dataStripID;
	CCharString dataSetName;	

	double lookx0,lookx1,lookx2,lookx3;
	double looky0,looky1,looky2,looky3;

	double linePeriode;
	double referenceTime;
	int referenceLine;
	int referenceBand;
	int pixelOrgin;
};
#endif