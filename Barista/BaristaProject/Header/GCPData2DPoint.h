#ifndef __CGCPDATA2DPOINT__
#define __CGCPDATA2DPOINT__

#include "Filename.h"
#include "XYPoint.h"

class CGCPData2DPoint : public CXYPoint
{

public:
	CGCPData2DPoint(void): CPointBase(2), qualityOfDefinition(1.0) {};
	//CGCPData2DPoint(void): CPointBase(2), CXYPoint() {};
	//CGCPData2DPoint(void): CPointBase(2), CXYPoint(2) {};
	CGCPData2DPoint(const CGCPData2DPoint& point): CPointBase(point), CXYPoint(point), qualityOfDefinition(point.qualityOfDefinition), 
		                                           date(point.date), orthoImageFileName(point.orthoImageFileName), Platform(point.Platform),
	                                               Sensor(point.Sensor), DateCreation(point.DateCreation), ViewAngle(point.ViewAngle),
												   Accuracy(point.Accuracy), AccuracyZ(point.AccuracyZ), Unit(point.Unit), sourceOfHeight(point.sourceOfHeight),
												   hDatum(point.hDatum), vDatum(point.vDatum), Projection(point.Projection), Zone(point.Zone),
												   epsgCode(point.epsgCode), chipSize(point.chipSize), dataProvider(point.dataProvider),
												   License(point.License), Comments(point.Comments), nBands(point.nBands), 
												   isMonoplotted(point.isMonoplotted), ChipID(point.ChipID) {};

	virtual CGCPData2DPoint &operator = (const CGCPData2DPoint & point);

	virtual ~CGCPData2DPoint() {};


	//get functions
//	const CXYPoint& get2DPoint() const {return this->gcpPoint2D;};
	CFileName getOrthoImageFileName() const {return this->orthoImageFileName;};
	CCharString getAcquiredDate() const {return this->date;};
	double getQualityOfDefinition() const {return this->qualityOfDefinition;};

	CCharString getPlatform()			{return this->Platform;};
	CCharString getSensor()				{return this->Sensor;};
	CCharString getDateCreation()		{return this->DateCreation;};
	double getViewAngle()				{return this->ViewAngle;};
	double getAccuracy()				{return this->Accuracy;};
	double getAccuracyZ()				{return this->AccuracyZ;};
	CCharString getUnit()				{return this->Unit;};
	CCharString getSourceOfHeight()		{return this->sourceOfHeight;};
	CCharString gethDatum()				{return this->hDatum;};
	CCharString getvDatum()				{return this->vDatum;};
	CCharString getProjection()			{return this->Projection;};
	int getZone()						{return this->Zone;};
	CCharString getepsgCode()			{return this->epsgCode;};
	int getchipSize()					{return this->chipSize;};
	CCharString getdataProvider()		{return this->dataProvider;};
	CCharString getLicense()			{return this->License;};
	CCharString getComments()			{return this->Comments;};
	int getnBands()						{return this->nBands;};
	bool getisMonoplotted()				{return this->isMonoplotted;};
	CCharString getChipID()				{return this->ChipID;};

	// set functions
//	void set2DPoint(CXYPoint p) {this->gcpPoint2D = p;};
	void setOrthoImageFileName(CFileName fname);
	void setDate(CCharString dt);
	void setQualityOfDefinition(double q);
	void setPlatform(CCharString platform)			{this->Platform = platform;};
	void setSensor(CCharString sensor)				{this->Sensor = sensor;};
	void setDateCreation(CCharString dateCreation)	{this->DateCreation = dateCreation;};
	void setViewAngle(double viewAngle)				{this->ViewAngle = viewAngle;};
	void setAccuracy(double accuracy)				{this->Accuracy = accuracy;};
	void setAccuracyZ(double accuracy)				{this->AccuracyZ = accuracy;};
	void setUnit(CCharString unit)					{this->Unit = unit;};
	void setSourceOfHeight(CCharString sHeight)		{this->sourceOfHeight = sHeight;};
	void sethDatum(CCharString HDatum)				{this->hDatum = HDatum;};
	void setvDatum(CCharString VDatum)				{this->vDatum = VDatum;};
	void setProjection(CCharString projection)		{this->Projection = projection;};
	void setZone(int zone)							{this->Zone = zone;};
	void setepsgCode(CCharString epsgCode)			{this->epsgCode = epsgCode;};
	void setchipSize(int chipSize)					{this->chipSize = chipSize;};
	void setdataProvider(CCharString dataProvider)	{this->dataProvider = dataProvider;};
	void setLicense(CCharString license)			{this->License = license;};
	void setComments(CCharString comments)			{this->Comments = comments;};
	void setnBands(int nbands)						{this->nBands = nbands;};
	void setisMonoplotted(bool isMonoplotted)		{this->isMonoplotted = isMonoplotted;};
	void setChipID(CCharString cid)					{this->ChipID = cid;};

protected:
	// 2D information
	// the image coordinates with respect to the upper left corner of the associated ortho image
	//CXYPoint gcpPoint2D;
	// name of the associated ortho image chip
	CFileName orthoImageFileName;
	// date at which the point was acquired
	CCharString date;	
	// when fromGPS at 3D GCP (member of class CGCPDataBasePoint) is not acquired with GPS, 
	// the following variable holds the ortho image resolution in meter where the point has been defined
	double qualityOfDefinition;

	//GA's information
	CCharString Platform; // vehicle type that carries sensors
	CCharString Sensor; // sensor type
	//CCharString DateAcquisition; // date when image was captured (see date defined above)
	CCharString DateCreation; // date when chip was created
	//double Resolution; // same as qualityOfDefinition defined above
	double ViewAngle; // view angle of sensor
	double Accuracy; // accuracy(1STD) of ?
	double AccuracyZ;
	CCharString Unit; //
	CCharString sourceOfHeight;// source of the height data
	CCharString hDatum; //
	CCharString vDatum;
	CCharString Projection;
	int Zone;
	CCharString epsgCode;
	int chipSize;
	CCharString dataProvider;
	CCharString License;
	CCharString Comments;
	int nBands; // number of bands
	bool isMonoplotted; // should always be true
	CCharString ChipID;
};

#endif