#ifndef __CGeoEye1Reader__
#define __CGeoEye1Reader__

/**
* Copyright:    Copyright (c) Thomas Weser<p>
* Company:      University of Melbourne<p>
* @author       Mehdi Ravanbakhsh
* @version      1.0
*/


#include "SatelliteDataReader.h"
#include "ObsPointArray.h"

class Matrix;


class CGeoEye1Reader : public CSatelliteDataReader
{
public:

	int nbands;
	int bpp;
	double linePeriode;

	CGeoEye1Reader(void);
	~CGeoEye1Reader(void);

	virtual bool readData(CFileName &filename);
	void littleToBigEndian(double * n);



protected:
	virtual bool prepareEphemeris() {return true;};
	virtual bool prepareAttitudes();
	virtual bool prepareMounting();

	virtual void computeInterval(double &startTime, double& endTime);

	void computeError(Matrix& covar,const CRotationMatrix& rpy, const CObsPoint& quaternion) const;

	bool readEphemeris();
	bool readAttitudes();
	bool readImgSensorMetaFile();



	CFileName ephemerisFile;
	CFileName attitudeFile;
	CFileName geoCalFile;
	CFileName imageMetaFile;
	CFileName imgSensorMetaFile;

	CObsPointArray quaternions;

	double focalLength;
	double DetectorSize;
	double polyCoffx0;
	double polyCoffx1;
	double polyCoffy0;
	double polyCoffy1;
	double focalLengthStdDev;
	double StdDevPolyCoffx0;
	double StdDevPolyCoffx1;
	double StdDevPolyCoffy0;
	double StdDevPolyCoffy1;

	double ResolutionX;
	double ResolutionY;

	bool isReverse;

};
#endif