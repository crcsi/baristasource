#pragma once

#include "Trans2D.h"
#include "Affine.h"
#include "VImage.h"
#include "XYPoint.h"
#include "m_FeatExtract.h"
#include "MatchRecordVector.h"
#include "ImgBuf.h"
#include "ProgressHandler.h"

class CFileName;
class CTrans2D;

class CRegistrationMatcher : public CProgressHandler
{
  public:
	  CRegistrationMatcher(CVImage *img1, CVImage *img2, float gsd1, float gsd2);
	
	  ~CRegistrationMatcher();
	
	  void setFeatureExtractionPars(const FeatureExtractionParameters &featureParameters);

	  void setMinCrossCorr(float minCC);

	  void setMatchWindowSize(int width, int height);

	  void setSensorModel(eRegistrationSensorModel model);

	  void setMaxParallaxDifference(double diff) { this->maxParallaxDifference = fabs(diff); } ;

	  bool match(float startGSD, bool keepPoints);
	  
	  bool MoveMeasuredPoints();


	  bool initProtocol (const CFileName &filename);

	  void closeProtocol();

	  double getSigma0() const { return this->sigma0; };

	  CTrans2D *getTrafo() const { return trafo; };
	  CTrans2D *getTrafoClone() const { return trafo->clone(); };

  protected:

	  double maxParallaxDifference;

	  void writeTrafoCovarToProt(const char *line, const Matrix &covar) const;

	  CVImage* inputimage1;
	
	  CVImage* inputimage2;
	
	  eRegistrationSensorModel trafoModel;

	  CTrans2D *trafo;

	  Matrix VarCovar;

	  double sigma0;

	  CImageBuffer buf1;       
	
	  CImageBuffer buf2;

	  CMatchRecordVector matches1;
	  
	  CMatchRecordVector matches2;

	  int matchWindowWidth;

	  int matchWindowHeigth;

	  FeatureExtractionParameters FEXpars;

	  float minCrossCorr;

	  float gsdImg1;

	  float gsdImg2;

	  bool prepBuffer1(int level1);

	  bool prepBuffer2(int level2);

	  bool matchCurrent(int level1, int level2, float diffMaxX, float diffMaxY);

	  bool matchCurrentWindow(float diffMaxX, float diffMaxY);

	  bool LSM(const CXYPoint &xyPoint);

	  ofstream *protocol;


};
