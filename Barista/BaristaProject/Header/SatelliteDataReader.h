#ifndef __CSatelliteDataReader__
#define __CSatelliteDataReader__

/**
 * Copyright:    Copyright (c) Thomas Weser<p>
 * Company:      University of Melbourne<p>
 * @author Thomas Weser
 * @version 1.0
 * revised by Shijie Liu 12,May,2010
 * to enable manually setting the splineOrder
 */


#include "Filename.h"
#include "ProgressHandler.h"
#include "Camera.h"
#include "CameraMounting.h"
#include "XYZPoint.h"
#include "RotationBaseFactory.h"
#include "RollPitchYawRotation.h" 

#define MAX_ITER 20
#define PERCENTAGE_OVERLAP 15.0

class CSatelliteInfoData
{
public:
	CSatelliteInfoData() :nRows(-1),nCols(-1),viewDirection(""),processingLevel(""),imageID(""),imageType(""){};
	~CSatelliteInfoData() {};
	int nRows;
	int nCols;
	CCharString viewDirection;	// forward, backward, nadir
	CCharString processingLevel;	// 
	CCharString imageID;	// unique id of the image
	CCharString imageType;	// pan or mult
	CCharString cameraName;

} ;

class COrbitObservations;
class doubles;
class CTFW;
class CRPC;
class COrbitPathModel;
class COrbitAttitudeModel;




class CSatelliteDataReader : public CProgressHandler
{
public:
	CSatelliteDataReader();

	virtual ~CSatelliteDataReader(void){};

	virtual bool hasTFW() const { return false; }
		
	virtual CTFW *getTFW()  { return 0; }

	virtual bool hasRPC() const { return false; }
		
	virtual CRPC *getRPC()  { return 0; }

	virtual void setDirectObservations(COrbitPathModel* path,COrbitAttitudeModel* attitudes) const;

	void initReader(COrbitObservations* path,COrbitObservations* attitudes,CCameraMounting* mounting,CCamera* camera);

	// read the metadata and adjust the path with the current set rms threshold
	virtual bool readData(CFileName &filename) = 0;	
	bool recomputeOrbit(bool adjustPath,bool adjustAttitudes);

	// these functions return the rms after the last adjustment of the orbit
	double getRMSPath()const {return this->currentRMSPath;};
	double getRMSAttitudes() const {return this->currentRMSAttitudes;};

	// used to set the rms threshold for the adjustment of the orbit, 
	// the adjustment will run until the current rms is smaller then the threshold
	void setAimedRMSPath(double rms) {	this->oldAimedRMSPath = this->aimedRMSPath;
										this->aimedRMSPath = rms;};
	void setAimedRMSAttitudes(double rms) { this->oldAimedRMSAttitudes = this->aimedRMSAttitudes;
											this->aimedRMSAttitudes = rms;};

	double getAimedRMSPath() const { return this->aimedRMSPath;};
	double getAimedRMSAttitudes() const { return this->aimedRMSAttitudes;};


	// if an error occurs, the gui will present this message to the user
	CCharString getErrorMessage() {return this->errorMessage;};
	void setErrorMessage(CCharString errorMessage) { this->errorMessage = errorMessage;};

	double getFirstLineTime() const {return this->firstLineTime;};
	double getTimePerLine() const {return this->timePerLine;};

	CCamera* getCamera() {return this->camera;};
	CCameraMounting* getCameraMounting() {return this->cameraMounting;};

	COrbitObservations* getOrbitPathObservationHandler() {return this->orbitPoints;};
	COrbitObservations* getOrbitAttitudeObservationHandler() {return this->attitudes;};


	bool adjustPath(COrbitObservations* newPath);
	bool adjustAttitude(COrbitObservations* newAttitudes);


	CSatelliteInfoData satInfo;

protected:

	// adjust the path with the current set rms threshold (for the updated rms)
	bool adjustOrbit(bool adjustPath,bool adjustAttitudes,double startTime, double endTime); 
	bool adjustOrbit(bool adjustPath,bool adjustAttitudes,double startTime, double endTime, int splineDegreePath,int splineDegreeAtt); 

	bool adjustPath();
	bool adjustAttitude();

	bool adjustPath(int splineDegree);
	bool adjustAttitude(int splineDegree);

	virtual bool prepareEphemeris() =0;
	virtual bool prepareAttitudes() =0;
	virtual bool prepareMounting() =0;

	void setAchievedRMSPath(double rms) {this->currentRMSPath = rms;};
	void setAchievedRMSAttitudes(double rms) {this->currentRMSAttitudes = rms;};

	bool determineOrbitPointsInInterval(COrbitObservations* orbitObs,double startTime,double endTime);

	virtual void computeInterval(double &startTime, double& endTime);

protected:

	COrbitObservations* orbitPoints;
	COrbitObservations* attitudes;
	CCamera* camera;
	CCameraMounting* cameraMounting;


	double firstLineTime;
	double lastLineTime;
	double timePerLine;

	double currentRMSPath;
	double currentRMSAttitudes;

	double oldAimedRMSPath;
	double oldAimedRMSAttitudes;

	double aimedRMSPath;
	double aimedRMSAttitudes;

	CCharString errorMessage;

	int nPathSegments;
	int nAttitudeSegments;


};
#endif