#include "XMLParser_intern.h"

#include "XMLDataHandler.h"
#include "XMLErrorHandler.h"


// we have to do this before calling the constructor !
bool CXMLParser_intern::init()
{
	try
    {
         XMLPlatformUtils::Initialize();
    }
    catch (const XMLException& toCatch)
    {
		toCatch.getMessage();
		return false;
    }

	return true;
}

bool CXMLParser_intern::terminate()
{
	try
    {	
		XMLPlatformUtils::Terminate();
    }
    catch (const XMLException& toCatch)
    {
		toCatch.getMessage();
		return false;
    }

	return true;

}


CXMLParser_intern::CXMLParser_intern(CXMLDataHandler* dataHandler,CXMLErrorHandler* errorHandler)
{
	this->setValidationScheme(SAXParser::Val_Auto);
    this->setDoNamespaces(false);
    this->setDoSchema(false);
	this->setValidationSchemaFullChecking(false);

	this->dataHandler = dataHandler;
	this->errorHandler = errorHandler;
    this->setDocumentHandler(this->dataHandler);
    this->setErrorHandler(this->errorHandler);
}



CXMLParser_intern::~CXMLParser_intern()
{
	if (this->dataHandler)
		delete this->dataHandler;
	this->dataHandler = NULL;

	if (this->errorHandler)
		delete this->errorHandler;
	this->errorHandler = NULL;
}


bool CXMLParser_intern::parseFile(const char* filename)
{
	bool ret = false;
	try
	{
		this->parse(filename);
		ret = true;
	}
	catch (...)
	{
		ret = false;
	}	

	return ret;
}