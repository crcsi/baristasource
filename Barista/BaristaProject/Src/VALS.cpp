/** 
 * @class CVALS
 * CVALS, class for handling of ALS data
 *
 * @author Franz Rottensteiner
 * @version 1.0 
 * @date 03-jan-2007
 *
 */

#include "SystemUtilities.h"

#include "VALS.h"
#include "StructuredFileSectionArray.h"
#include "ALSData.h"
#include "ALSPointCloud.h"
#include "HugeImage.h"
#include "ALSDataWithTiles.h"
#include "ALSDataWithStrips.h"

CVALS::CVALS() : als (0)
{

}

CVALS::CVALS(const CVALS& src) : als (0), alsFileName(src.alsFileName)
{
	if (src.als) 
		this->als = src.als->getClone();
}

CVALS::~CVALS()
{
	if (als) delete als;
	als = 0;
}

CReferenceSystem CVALS::getReferenceSystem() const
{
	CReferenceSystem ref;

	if (this->als) ref = this->als->getReferenceSystem();

	return ref;
};
	  
bool CVALS::exportASCII(const char *filename, int pulse, bool exportTime, bool exportI)
{
	return this->als->exportXYZ(filename, pulse, exportTime, exportI);
};

bool CVALS::initALSStrip(const char *filename, eALSFormatType formatType, 		              
					     eALSPulseModeType PulseMode, const CReferenceSystem &ref,
					     const C2DPoint &offset)
{
	if (this->als) delete als;
	this->alsFileName = filename;
	CFileName binfile(this->alsFileName);
	binfile += ".als";
	this->als = new CALSDataSetWithStrips(binfile.GetChar(), ref, false);

	if (!this->als) return false;



	return true;
};

CALSDataSetWithTiles *CVALS::initALSTile(const char *filename, vector<CALSDataSet *> &datasetVec, bool mergeDatasets)
{
	CALSDataSetWithTiles *alsTiles = 0;
	if (datasetVec.size() > 1)
	{
		if (this->als) delete als;
		this->als = 0;

		this->alsFileName = filename;
		alsTiles = new CALSDataSetWithTiles(this->alsFileName.GetChar(), datasetVec[0]->getReferenceSystem(), false, datasetVec[0]->getPointDist());
		this->als = alsTiles;

		if (this->als) 
		{
			if (mergeDatasets) 
			{
				if (!alsTiles->init(datasetVec)) alsTiles = 0;
			}
		}
	};

	return alsTiles;
};


bool CVALS::initALSTile(const char *filename, eALSFormatType formatType, 		              
				        eALSPulseModeType PulseMode, const CReferenceSystem &ref,
					    const C2DPoint &offset, const C2DPoint &delta)
{
	if (this->als) delete als;
	this->alsFileName = filename;
	CFileName binfile(this->alsFileName);
	binfile += ".als";
	this->als = new CALSDataSetWithTiles(binfile.GetChar(), ref, false, delta);

	if (!this->als) return false;

	return true;
};

void CVALS::setFileName(const char* filename)
{
    this->alsFileName = filename;

}

/**
 * Serialized storage of a CVALS to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVALS::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName relPath;
	CFileName fileSaveName = this->alsFileName;

	if (!Path.IsEmpty())
	{
		relPath = CSystemUtilities::RelPath(this->alsFileName, Path);
		fileSaveName.SetPath(relPath);
	}


    CToken* token = pStructuredFileSection->addToken();
	token->setValue("als filename", fileSaveName);

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->getDisplaySettings()->serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->als->serializeStore(child);	
}

/**
 * Restores a CVALS from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVALS::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);
	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName CWD;
	if (!Path.IsEmpty())
	{
		CWD = CSystemUtilities::getCwd();
		CSystemUtilities::chDir(Path);
	}

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("als filename"))
			this->alsFileName = token->getValue();
    }

	if (!Path.IsEmpty())
	{
		this->alsFileName = CSystemUtilities::AbsPath(this->alsFileName);
		CSystemUtilities::chDir(CWD);
	}


    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if ((child->getName().CompareNoCase("CALSDataSet")) || (child->getName().CompareNoCase("CALSDataSetWithStrips")))
        {
			if (!this->als) 
			{
				const char *fn = "als";
				CReferenceSystem ref;
				this->als = new CALSDataSetWithStrips(fn, ref, false);
			}

			this->als->serializeRestore(child);
        }
		else if (child->getName().CompareNoCase("CALSDataSetWithTiles"))
        {
			if (!this->als) 
			{
				const char *fn = "als";
				CReferenceSystem ref;
				C2DPoint delta(1.0,1.0);
				this->als = new CALSDataSetWithTiles(fn, ref, false, delta);
			}

			this->als->serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CDisplaySettings"))
        {
			this->displaySettings.serializeRestore(child);
        }
    }
}

/**
 * reconnects pointers
 */
void CVALS::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CVALS::resetPointers()
{
    CSerializable::resetPointers();
}


/**
 * Flag if CVALS is modified
 * returns bool
 */
bool CVALS::isModified()
{
    ///@todo when the points are serializable we can loop them... 
    return this->bModified;
}

CFileName CVALS::getItemText()
{
    CFileName itemtext = this->alsFileName.GetFullFileName();
    return itemtext.GetChar();
}

void CVALS::setReferenceSystem(const CReferenceSystem &referenceSystem)
{
	if (this->als) this->als->setReferenceSystem(referenceSystem);
};

bool CVALS::createIntensityImage(CHugeImage *img, const char *filename, const C2DPoint &pmin, 
								 const C2DPoint &pmax, const C2DPoint &gridS, int N, int echo, int level,
								 eInterpolationMethod method, double maxDist)
{
	if ((gridS.x < FLT_EPSILON) || (gridS.y < FLT_EPSILON)) return false;

	int w = int (floor((pmax.x - pmin.x) / gridS.x) + 1.0);
	int h = int (floor((pmax.y - pmin.y) / gridS.y) + 1.0);
	if ((w <= 0) || (h <= 0)) return false;

	if (!img->prepareWriting(filename, w, h, 16, 1)) return false;

	float nTiles = float(img->tilesDown * img->tilesAcross);

	CALSPointCloudBuffer pointcloud(2);

	CImageBuffer buffer (0, 0, img->tileWidth, img->tileHeight, 1, 16, true);

	double yul = pmax.y;

	C2DPoint tileExt(gridS.x * img->tileWidth, gridS.y  * img->tileHeight);

	C3DPoint min, max;

	min.z =      -1.00;
    max.z =    1000.00;
	double overlap = (this->als->getPointDist().x > this->als->getPointDist().y) ? 
		this->als->getPointDist().x : this->als->getPointDist().y;
	overlap *= 10;

	float minz, maxz;
	
	float pfact = (float) 85.0 / float(img->tilesDown * img->tilesAcross);

    for(int TR = 0; TR < img->height; TR += img->tileHeight, yul -= tileExt.y) 
	{
		double xul = pmin.x;

		min.y = yul - tileExt.y - overlap;
		max.y = yul + overlap;

        for (int TC = 0; TC < img->width; TC += img->tileWidth, xul  += tileExt.x)
        {
			min.x = xul - overlap;
			max.x = xul + tileExt.x + overlap;

			if (pointcloud.get(*(this->als), min, max, level) <= 0) buffer.setAll(0);
			else
			{
				if (maxDist <= FLT_EPSILON)
					pointcloud.interpolateIntensity(buffer.getPixUS(), img->tileWidth, img->tileHeight, 
											        xul, yul, gridS.x, gridS.y, N, echo, method);
				else
					pointcloud.interpolateIntensity(buffer.getPixUS(), img->tileWidth, img->tileHeight, 
											        xul, yul, gridS.x, gridS.y, N, echo, method, maxDist);

			}

			img->dumpTile(buffer);
			
			float pvalue = (float)((TR / img->tileHeight) * img->tilesAcross + (TC / img->tileWidth)) * pfact;
			img->setProgressValue(pvalue);
		}
	}

	this->als->clear();

    bool ret = img->finishWriting();

	img->setProgressValue(100.0);

	return ret;
};