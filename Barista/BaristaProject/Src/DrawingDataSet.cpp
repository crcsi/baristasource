#include "DrawingDataSet.h"
#include "XYPolyLine.h"


CDrawingDataSet::CDrawingDataSet(int dim) : 
points(dim),name(""),showPoints(true),showPointLabel(true)
{
	pointColor[0] = 0;
	pointColor[1] = 255;
	pointColor[2] = 0;
}

CDrawingDataSet::CDrawingDataSet(const CDrawingDataSet& dataSet):
	points(dataSet.points),labelSort(dataSet.labelSort),name(dataSet.name),
	topology(dataSet.topology),showPoints(dataSet.showPoints),showPointLabel(dataSet.showPointLabel)
{
	for (int i=0; i < 3; i++) pointColor[i] = dataSet.pointColor[i];
	

	for (int i=0; i < dataSet.labelSort.size(); i++)
	{
		int indexNew = dataSet.points.indexOf(dataSet.labelSort[i]);
		this->labelSort[i] = this->points.GetAt(indexNew);
	}
	
	for (int i=0; i < dataSet.topology.size(); i++)
	{
		int indexFrom = dataSet.points.indexOf(dataSet.topology[i].from);
		int indexTo =   dataSet.points.indexOf(dataSet.topology[i].to);
		this->topology[i].from = this->points.GetAt(indexFrom);
		this->topology[i].to   = this->points.GetAt(indexTo);
	}
}

CDrawingDataSet::~CDrawingDataSet(void)
{
}

CTopology* CDrawingDataSet::getTopology(const int index)
{
	assert(index >= 0 || index < this->topology.size());
	return & this->topology[index];
}

CObsPoint* CDrawingDataSet::getPoint(const int index)
{
	assert(index >= 0 || index < this->points.GetSize());
	return this->points.GetAt(index);
}

void CDrawingDataSet::setName(const CLabel& name)
{
	this->name = name;
}


void CDrawingDataSet::setPointColor(const int *color) 
{
	for (int i=0; i < 3; i++) pointColor[i] = color[i];
};

CObsPoint* CDrawingDataSet::addPoint(CObsPoint* point)
{
	CObsPoint* newPoint = NULL;
	
	if (!point)
		return point;

	int pos;

	// do we have the point already?
	if (!this->findPos_PointLabel(point->getLabel(),pos)) // no
	{
		// store point first
		newPoint = this->points.Add(point);
		
		// add at the right position in the label sorted array
		if (pos <= this->labelSort.size())
		{
			this->labelSort.insert(this->labelSort.begin() + pos,newPoint);
		}		
	}
	else // yes
		newPoint = this->labelSort[pos];


	return newPoint;
}

void CDrawingDataSet::removePoint(const CLabel& point)
{
	int pos;

	// do we have the point?
	if (this->findPos_PointLabel(point,pos)) // yes
	{
		// delete all connections with this point
		this->removeTopologiesForPoint(this->labelSort[pos]->getLabel());
		
		// delete point
		this->points.Remove(this->labelSort[pos]);

		// remove pointer
		this->labelSort.erase(this->labelSort.begin()+ pos);
	}
}

bool CDrawingDataSet::queryPoint(const CLabel & pointLabel)
{
	int pos;
	// do we have the point?
	return this->findPos_PointLabel(pointLabel,pos);

}


bool CDrawingDataSet::findPos_PointLabel(const CLabel& pointName,int &pos)
{
  int low = 0, high = this->labelSort.size() - 1;

  while ( high >= low ) 
  {
    pos = ( low + high ) / 2;
	int test = strcmp(pointName.GetChar(),this->labelSort[pos]->getLabel().GetChar());
    if ( test < 0)
      high = pos - 1;
    else if ( test > 0)
      low = pos + 1;
    else 
      return true;

  }
	pos = low;
	return	false;
}


bool  CDrawingDataSet::findPos_Topology_Key1(const CLabel& key1,int &pos,int &length)
{

	int low = 0, high = this->topology.size() - 1;	
	while ( high >= low ) 
	{
		pos = ( low + high ) / 2;
		int test = strcmp(key1.GetChar(),this->topology[pos].from->getLabel().GetChar());
		if ( test < 0  )
			high = pos - 1;
		else if ( test > 0 )
			low = pos + 1;
		else // we found the first point
		{	// but we don't know yet if it is the first or last occurance

			// go back and find the first occurance
			while ( (pos - 1 >= 0) && // stay in the array
				    (key1 == this->topology[pos - 1].from->getLabel()) // till the label changes
					){pos--;};
			
			length = 1;
			// go forward and find last occurance
			for (int cpos = pos +1; cpos <  this->topology.size(); length++,cpos++)
				if (key1 !=  this->topology[cpos].from->getLabel())
					break;
		
			return true;
				
		}

	}
	length = 0;
	pos = low;
	return	false;

}

bool CDrawingDataSet::findPos_Topology_Key2(const CLabel& key2,int &pos,int low,int length)
{
	int high = low + length - 1;

	while ( high >= low ) 
	{
		pos = ( low + high ) / 2;
		int test = strcmp(key2.GetChar(),this->topology[pos].to->getLabel().GetChar());
		if ( test <  0)
			high = pos - 1;
		else if ( test > 0 )
			low = pos + 1;
		else // we found the first point
			return true;

	}
	pos = low;
	return	false;
}



bool CDrawingDataSet::findTopologyPos(const CLabel& pointName1,const CLabel& pointName2,int& pos)
{
	int length;
	pos = 0;
	if (!this->topology.size())
		return false;

	if (!this->findPos_Topology_Key1(pointName1,pos,length))
		return false;

	return this->findPos_Topology_Key2(pointName2,pos,pos,length);

}


bool CDrawingDataSet::addTopology(const CLabel& p1,const CLabel& p2, const int *color,unsigned int lineWidth)
{	
	int pos1,pos2;

	// find the points
	if (!this->findPos_PointLabel(p1,pos1) || !this->findPos_PointLabel(p2,pos2))
		return false;


	int pos;

	// make sure p1 <= p2
	if (p1 > p2)
	{
		// do we have the topology already?
		if (this->findTopologyPos(p2,p1,pos))
		return false;

		int tmp = pos1;
		pos1 = pos2;
		pos2 = tmp; 
	}
	else
	{
		// do we have the topology already?
		if (this->findTopologyPos(p1,p2,pos))
		return false;
	}



	// add the new topology
	CTopology topo(this->labelSort[pos1],this->labelSort[pos2],color,lineWidth);
	this->topology.insert(this->topology.begin() + pos,topo);

	return true;
}

bool CDrawingDataSet::removeTopology(const CLabel& p1,const CLabel& p2)
{

	int pos1,pos2;

	// find the points
	if (!this->findPos_PointLabel(p1,pos1) || !!this->findPos_PointLabel(p2,pos2))
		return false;

	int pos;

	// make sure p1 <= p2
	if (p1 > p2)
	{	// do we have the topology?
		if (this->findTopologyPos(p2,p1,pos))
			return false;
	}
	else
	{	// do we have the topology?
		if (this->findTopologyPos(p1,p2,pos))
			return false;
	}

	this->topology.erase(this->topology.begin()+ pos);

	return true;
}

bool CDrawingDataSet::removeTopologiesForPoint(const CLabel& p)
{

	int pos1,length;
	
	// find the label as key1 and delete all connections
	if (this->findPos_Topology_Key1(p,pos1,length))
	{
		this->topology.erase(this->topology.begin() + pos1,this->topology.begin() + pos1 + length);
	}

	// now check the second key
	for (int i= this->topology.size() -1; i >=0; i--)
	{
		if (this->topology[i].to->getLabel() == p)
			this->topology.erase(this->topology.begin() + i);
	}

	return true;
}

void CDrawingDataSet::removeAllPoints()
{
	this->points.RemoveAll();
	this->labelSort.clear();
	this->topology.clear();
}

void CDrawingDataSet::changeTopologyColor(const CLabel& p1,const CLabel& p2, const int *color)
{
	int pos;
	
	if(this->findTopologyPos(p1,p2,pos))
	{
		for (int i=0; i < 3; i++) this->topology[pos].color[i] = color[i];
	}
}

void CDrawingDataSet::changeAllTopologyColors(const int *color)
{
	for (int j= 0; j < this->topology.size(); j++)
	{
		for (int i=0; i < 3; i++) this->topology[j].color[i] = color[i];
	}

}


void CDrawingDataSet::changeTopologyLineWidth(const CLabel& p1,const CLabel& p2,unsigned int lineWidth)
{
	int pos;
	
	if(!this->findTopologyPos(p1,p2,pos))
		return;

	this->topology[pos].lineWidth = lineWidth;
}

void CDrawingDataSet::changeAllTopologyLineWidth(unsigned int lineWidth)
{
	for (int i= 0; i < this->topology.size(); i++)
		this->topology[i].lineWidth = lineWidth;

}