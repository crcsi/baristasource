#include <float.h>
#include <strstream>
#include "DEMMatcher.h"
#include "VImage.h"
#include "XYPointArray.h"
#include "BaristaProject.h"
#include "Trans2DAffine.h"
#include "Trans2DProjective.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"

#include "3DPoint.h"
#include "Trans3DFactory.h"
#include "XYZPolyLine.h"
#include "LabelImage.h"

#include "GenericPointCloud.h"
#include "sortVector.h"

#include "DenseMatcher.h"
#include "GenerateEpipolar.h"

CDEMMatcher::CDEMMatcher(const vector<CVImage *> &ImageVector, CVDEM *DEM, 
						 const CSGMParameters &SGMPars, double Zmin, double Zmax, 
						 eInterpolationMethod method, double DistMaxInterpol,
						 int Neighbourhood, CXYZPolyLine *poly, 
						 vector<CXYZPolyLine *> *innerPolys):
	 pDEM(DEM), MatchingParameters(SGMPars), DEMTileBuffer(0),
	 pDEMtoSensorTrafo(0), pSensorToDEMTrafo(0), currentObjPts(0),
	 interpolationMethod(method), maxDistInterpol(DistMaxInterpol), 
	 neighbourhood(Neighbourhood), marginSize(0), 
	 pOuterPolyLine(0), imageVector(ImageVector)
{
	this->listener = NULL;
	
	if (poly)
	{
		this->pOuterPolyLine = new CXYPolyLine;
		this->transformPolyToImage(*poly, *this->pOuterPolyLine);
	}

	if (innerPolys) 
	{
		this->InnerPolylineList.resize(innerPolys->size(), 0);
		for (unsigned int i = 0; i < this->InnerPolylineList.size(); ++i)
		{
			this->InnerPolylineList[i] = new CXYPolyLine;
			this->transformPolyToImage(*(*innerPolys)[i], *this->InnerPolylineList[i]);
		};
	}
	
	this->Pmin.x = this->pDEM->getDEM()->getminX();
	this->Pmin.y = this->pDEM->getDEM()->getminY();
	this->Pmax.x = this->pDEM->getDEM()->getmaxX();
	this->Pmax.y = this->pDEM->getDEM()->getmaxY();
	this->Pmin.z = Zmin;
	this->Pmax.z = Zmax;
}

//================================================================================

CDEMMatcher::~CDEMMatcher()
{
	if (this->pDEMtoSensorTrafo) delete this->pDEMtoSensorTrafo;
	this->pDEMtoSensorTrafo = 0;
	if (this->pSensorToDEMTrafo) delete this->pSensorToDEMTrafo;
	this->pSensorToDEMTrafo = 0;

	if (this->DEMTileBuffer) delete this->DEMTileBuffer;
	this->DEMTileBuffer = 0;

	this->clearPointCloud();

	if (this->pOuterPolyLine) delete this->pOuterPolyLine;
	this->pOuterPolyLine = 0;

	for (unsigned int i = 0; i < this->InnerPolylineList.size(); ++i)
	{
		delete this->InnerPolylineList[i];
		this->InnerPolylineList[i] = 0;
	}
}

//================================================================================
	  
void CDEMMatcher::clearPointCloud()
{
	if (this->currentObjPts)
	{
		delete this->currentObjPts;
		this->currentObjPts = 0;
	};

	for (unsigned int i = 0; i < this->currentPointColours.size(); ++i)
	{
		delete [] this->currentPointColours[i];
		this->currentPointColours[i] = 0;
	}

	this->currentPointColours.resize(0);
	this->currentPointCount.resize(0);
};

//================================================================================
	  
bool CDEMMatcher::isInsideOuterPoly(const C3DPoint &pmin, const C3DPoint &pmax) const
{
	C2DPoint p(pmin.x, pmin.y);
	if (this->pOuterPolyLine->contains(p)) return true;
	p.x = pmax.x;
	if (this->pOuterPolyLine->contains(p)) return true;
	p.y = pmax.y;
	if (this->pOuterPolyLine->contains(p)) return true;
	p.x = pmin.x;
	if (this->pOuterPolyLine->contains(p)) return true;
	
	for (int i = 0; i < this->pOuterPolyLine->getPointCount(); ++i)
	{
		if ((this->pOuterPolyLine->getPoint(i).x >= pmin.x) && 
			(this->pOuterPolyLine->getPoint(i).x <= pmax.x) &&
			(this->pOuterPolyLine->getPoint(i).y >= pmin.y) && 
			(this->pOuterPolyLine->getPoint(i).y <= pmax.y)) return true;
	}

	return false;
};

//================================================================================
	  
void CDEMMatcher::transformPolyToImage(const CXYZPolyLine &ObjPoly, 
									   CXYPolyLine &ImgPoly) const
{
	if (ImgPoly.getPointCount() > 0) ImgPoly.removePoints(0, ImgPoly.getPointCount() -1);
	C2DPoint p;
	CXYZPoint *P;
	for (int i = 0; i < this->pOuterPolyLine->getPointCount(); ++i)
	{
		P = ObjPoly.getPoint(i);
		p.x = P->x;
		p.y = P->y;
		this->pDEM->getTFW()->transformObj2Obs(p,p);
		ImgPoly.addPoint(p);
	}
	ImgPoly.closeLine();
};

//================================================================================

CCharString CDEMMatcher::getSuffix(int r, int c) const
{
	CCharString rS(r,  4, true);
	CCharString cS(c,  4, true);
	CCharString f("_");
	f += rS + "_" + cS;

	return f;
};

//================================================================================

int CDEMMatcher::getPointCountThreshold(int nStereoPairs) const
{
	/*
	int minPtsSure = nStereoPairs / 3;
	if (minPtsSure == 0) minPtsSure = nStereoPairs;
	if (minPtsSure > 3) minPtsSure = 3;

	return minPtsSure;
    */

	if (nStereoPairs > 3) return 2;

	return 1;
}

//================================================================================

void CDEMMatcher::initTileStructure()
{
	ostrstream oss;
	float maxBufSize = 500.0f;

	C2DPoint diff(this->Pmax.x - this->Pmin.x, this->Pmax.y - this->Pmin.y);
	
	long nX = floor(diff.x / this->pDEM->getDEM()->getgridX() + 0.5);
	long nY = floor(diff.y / this->pDEM->getDEM()->getgridY() + 0.5);

	int tileSizeX = this->pDEM->getDEM()->getTileWidth();
	int tileSizeY = this->pDEM->getDEM()->getTileHeight();

	long nTilesX = nX / tileSizeX + 1;
	long nTilesY = nY / tileSizeY + 1;
	oss << "\n   Computing " << nTilesX << " x " << nTilesY << " Tiles (columns/rows), size: " << tileSizeX << " x " << tileSizeY << " Pixels\n";

	int nT = nTilesX * nTilesY;
	this->TilePminVec.resize(nT);
	this->TilePmaxVec.resize(nT);;
	this->TileFileNames.resize(nT);
	this->TileOffset.resize(nT);

	diff.x = double(tileSizeX) * this->pDEM->getDEM()->getgridX();
	diff.y = double(tileSizeX) * this->pDEM->getDEM()->getgridY();
	nT = 0;
	int tr = 0, tc = 0;
	
	for (int rT = 0; rT < nTilesY; ++rT, tr += tileSizeY)
	{		
		this->TileOffset[nT].resize(2);
		this->TileOffset[nT][0] = 0;
		this->TileOffset[nT][1] = tr;
		this->TileFileNames[nT] = protHandler.getDefaultDirectory() + "Tile" + this->getSuffix(rT, 0);	

		this->TilePminVec[nT].x = this->Pmin.x;
		this->TilePmaxVec[nT].y = this->Pmax.y - double(rT) * diff.y;
		
		this->TilePmaxVec[nT].x = this->TilePminVec[nT].x + diff.x;
		this->TilePminVec[nT].z = this->Pmin.z;
		this->TilePmaxVec[nT].z = this->Pmax.z;
		if (rT < (nTilesY - 1)) this->TilePminVec[nT].y = this->TilePmaxVec[nT].y - diff.y;		
		else this->TilePminVec[nT].y = this->Pmin.y;
		++nT;
		tc = tileSizeX;

		for (int cT = 1; cT < nTilesX; ++cT, ++nT, tc += tileSizeX)
		{
			this->TilePminVec[nT].x = this->TilePminVec[nT - 1].x + diff.x;
			this->TilePmaxVec[nT].x = this->TilePmaxVec[nT - 1].x + diff.x;
			this->TilePminVec[nT].y = this->TilePminVec[nT - 1].y ;
			this->TilePmaxVec[nT].y = this->TilePmaxVec[nT - 1].y;
			this->TilePminVec[nT].z = this->Pmin.z;
			this->TilePmaxVec[nT].z = this->Pmax.z;
			this->TileFileNames[nT] = protHandler.getDefaultDirectory() + "Tile" + this->getSuffix(rT, cT);	
			this->TileOffset[nT].resize(2);
			this->TileOffset[nT][0] = tc;
			this->TileOffset[nT][1] = tr;
		}

		this->TilePmaxVec[nT - 1].x = this->Pmax.x;
	}

	protHandler.print(oss, protHandler.prt);
}; 

//================================================================================

C3DPoint CDEMMatcher::getExpectedSigma(int imageIndex1, int imageIndex2)
{
	double s0 = 0.7;

	CXYZPoint PC1 = (this->Pmin + this->Pmax) * 0.5f;

	CXYZPoint PC;

	this->pDEMtoSensorTrafo->transform(PC, PC1);

	C2DPoint p1, p2;
	this->imageVector[imageIndex1]->getCurrentSensorModel()->transformObj2Obs(p1, (const C3DPoint &) PC);
	this->imageVector[imageIndex2]->getCurrentSensorModel()->transformObj2Obs(p2, (const C3DPoint &) PC);

	double delta, dx, dy, dz, el;

	Matrix Ai, lSearch(1,2), Fi,Bi,  nPt, t, Ntemp;
	SymmetricMatrix P(2), N(3);
	N = 0;

	SymmetricMatrix Psearch1(2), Psearch2(2);

	
	lSearch.element(0,0) = p1.x;
	lSearch.element(0,1) = p1.y;
	this->imageVector[imageIndex1]->getCurrentSensorModel()->Bi(Bi, lSearch, PC);
	Bi = Bi * Bi.t();
	for (int r = 0; r < Psearch1.Nrows(); r++)
	{
		for (int c = r; c < Psearch1.Ncols(); ++c)
		{
			Psearch1.element(r,c) = Bi.element(r,c);
		}
	}

	Psearch1 = Psearch1.i();
		
	lSearch.element(0,0) = p2.x;
	lSearch.element(0,1) = p2.y;
	this->imageVector[imageIndex2]->getCurrentSensorModel()->Bi(Bi, lSearch, PC);
	Bi = Bi * Bi.t();
	for (int r = 0; r < Psearch2.Nrows(); r++)
	{
		for (int c = r; c < Psearch2.Ncols(); ++c)
		{
			Psearch2.element(r,c) = Bi.element(r,c);
		}
	}

	Psearch2 = Psearch2.i();
		
	lSearch.element(0,0) = p1.x;
	lSearch.element(0,1) = p1.y;
	this->imageVector[imageIndex1]->getCurrentSensorModel()->Fi(Fi, lSearch, PC);
	this->imageVector[imageIndex1]->getCurrentSensorModel()->Bi(Bi, lSearch, PC);
	this->imageVector[imageIndex1]->getCurrentSensorModel()->AiPoint(Ai, lSearch, PC);
	nPt = Ai.t() * Psearch1;
	t = nPt * Fi;

	for (int r = 0; r < N.Nrows(); r++)
	{
		for (int c = r; c < N.Ncols(); ++c)
		{
			el = 0.0;
			for (int i = 0; i < nPt.Ncols(); ++i)
			{
				el += nPt.element(r , i) * Ai.element(i,c);
			}

			N.element(r,c) += el;
		}
	}

	lSearch.element(0,0) = p2.x;
	lSearch.element(0,1) = p2.y;
	this->imageVector[imageIndex2]->getCurrentSensorModel()->Fi(Fi, lSearch, PC);
	this->imageVector[imageIndex2]->getCurrentSensorModel()->Bi(Bi, lSearch, PC);
	this->imageVector[imageIndex2]->getCurrentSensorModel()->AiPoint(Ai, lSearch, PC);
	nPt = Ai.t() * Psearch1;
	t += nPt * Fi;
	
	for (int r = 0; r < N.Nrows(); r++)
	{
		for (int c = r; c < N.Ncols(); ++c)
		{
			el = 0.0;
			for (int i = 0; i < nPt.Ncols(); ++i)
			{
				el += nPt.element(r , i) * Ai.element(i,c);
			}

			N.element(r,c) += el;
		}
	}

	Try
	{
		N = N.i();	
		t = -N * t;
		dx = t.element(0,0); 
		dy = t.element(1,0);
		dz = t.element(2,0); 
		PC.x += dx;
		PC.y += dy;
		PC.z += dz;
		PC.setCovariance(N * s0 * s0);
		delta = dx * dx + dy * dy + dz * dz;
	}
	CatchAll
	{

	}

	this->pSensorToDEMTrafo->transform(PC1, PC);

	SigmaPoint.x = PC1.getSX();
	SigmaPoint.y = PC1.getSY();
	SigmaPoint.z = PC1.getSZ();

	
	return SigmaPoint;
};

//================================================================================

bool CDEMMatcher::prepHeightBuffer(int tileIndex)
{
	bool success = true;
	
	long w = this->pDEM->getDEM()->tileWidth;
	long h = this->pDEM->getDEM()->tileHeight;

	long offsetX = this->TileOffset[tileIndex][0];
	long offsetY = this->TileOffset[tileIndex][1];

	if (!this->DEMTileBuffer) 
	{
		this->DEMTileBuffer = new CImageBuffer(offsetX, offsetY, w, h,1, 32, false, -9999.0);
		success = this->DEMTileBuffer->getNGV() > 0;
	}
	else 
	{
		if ((this->DEMTileBuffer->getWidth() == w) && (this->DEMTileBuffer->getHeight() == h))
			this->DEMTileBuffer->setOffset(offsetX, offsetY);
		else success = this->DEMTileBuffer->set(offsetX, offsetY,w, h, 1, 32, false, -9999.0);
	}

	return success;
};
 	  
//================================================================================

vector<int> CDEMMatcher::getOverlappingImages(int tileIndex, vector<C2DPoint> &extents) const
{
	vector<int> visibleImgVec(this->imageVector.size());
	extents.resize(this->imageVector.size());
	
	C2DPoint pminImg, pmaxImg, DeltaMax(0.0, 0.0);

	unsigned int nVisible = 0;
	
	for (unsigned int i = 0; i < this->imageVector.size(); ++i)
	{
		CVImage *img = this->imageVector[i];
		if (img->getOverlapArea(this->TilePminVec[tileIndex], this->TilePmaxVec[tileIndex], pminImg, pmaxImg))
		{
			extents[nVisible] = pmaxImg - pminImg;
			if (extents[nVisible].x > DeltaMax.x) DeltaMax.x = extents[nVisible].x;
			if (extents[nVisible].y > DeltaMax.y) DeltaMax.y = extents[nVisible].y;
			visibleImgVec[nVisible] = i;
			++nVisible;
		};
	};
	
	visibleImgVec.resize(nVisible);

	nVisible = 0;

	for (unsigned int i = 0; i < visibleImgVec.size(); ++i)
	{
		double fractX = extents[i].x / DeltaMax.x;
		double fractY = extents[i].y / DeltaMax.y;
		double fractMin = (fractX < fractY) ? fractX : fractY;
		if (fractMin > 0.5) 
		{
			visibleImgVec[nVisible] = visibleImgVec[i];
			extents[nVisible]       = extents[i];
			++nVisible;
		}
	}

	visibleImgVec.resize(nVisible);
	extents.resize(nVisible);
	return visibleImgVec;
};
		  
//================================================================================

bool CDEMMatcher::isGoodModel(int imageIndex1, int imageIndex2, float alphaThreshold) const
{
	CRotationMatrix R1 = this->imageVector[imageIndex1]->getFrameCamera()->getCameraRotation()->getRotMat();
	CRotationMatrix R2 = this->imageVector[imageIndex2]->getFrameCamera()->getCameraRotation()->getRotMat();
	C3DPoint BaseVec = C3DPoint(this->imageVector[imageIndex1]->getFrameCamera()->getCameraPosition()) - 
							   C3DPoint(this->imageVector[imageIndex2]->getFrameCamera()->getCameraPosition());
	BaseVec = BaseVec * (1.0 / BaseVec.getNorm());
	C3DPoint colR;
	float cosAlphaThr = cos(alphaThreshold);
	R1.getFirstCol(colR.x, colR.y, colR.z);
	float cosAlpha = (float) fabs(colR.innerProduct(BaseVec));
	float cosAlphaMax = cosAlpha; 
	R1.getSecondCol(colR.x, colR.y, colR.z);
	cosAlpha = (float) fabs(colR.innerProduct(BaseVec));
	if (cosAlpha > cosAlphaMax) cosAlphaMax = cosAlpha;
	R2.getFirstCol(colR.x, colR.y, colR.z);
	cosAlpha = (float) fabs(colR.innerProduct(BaseVec));
	if (cosAlpha > cosAlphaMax) cosAlphaMax = cosAlpha;
	R2.getSecondCol(colR.x, colR.y, colR.z);
	cosAlpha = (float) fabs(colR.innerProduct(BaseVec));
	if (cosAlpha > cosAlphaMax) cosAlphaMax = cosAlpha;
	
	if (cosAlphaMax > cosAlphaThr)  return true;

	return false;
};  

//================================================================================

vector<vector<int>> CDEMMatcher::getStereoPairsImages(int tileIndex) const
{
	vector<vector<int>> stereoPairs;
	
	vector<C2DPoint> extents;

	vector<int> imageVec = this->getOverlappingImages(tileIndex, extents);
		
	float alphaThreshold = 20.0f * float(M_PI) / 200.0f;

	if ((imageVec.size() >= 2) && (imageVec.size() < 4))
	{
		vector<int> pair(2, 0);
		for (unsigned int i = 0; i < imageVec.size(); ++i)
		{
			for (unsigned int j = i + 1; j < imageVec.size(); ++j)
			{
				pair[0] = imageVec[i];
				pair[1] = imageVec[j];
				if (this->isGoodModel(pair[0], pair[1] , alphaThreshold)) 
					stereoPairs.push_back(pair);
			}
		}

	}
	else if (imageVec.size() >= 2)
	{
		CGenericPointCloud prcCld(imageVec.size(),3,2);

		for (unsigned int i = 0; i < imageVec.size(); ++i)
		{
			prcCld.addPoint(this->imageVector[imageVec[i]]->getFrameCamera()->getCameraPosition());
		}
		prcCld.Triangulate();

		vector<int> pair(2, 0);
		
		for (int i = 0; i < prcCld.getNedges(); ++i)
		{
			prcCld.getEdgeIndices(i, pair[0], pair[1]);
			if (pair[0] > pair[1])
			{
				int dmy = pair[0]; pair[0] = pair[1]; pair[1] = dmy;
			}

			pair[0] = imageVec[pair[0]];
			pair[1] = imageVec[pair[1]];
			
			if (this->isGoodModel(pair[0], pair[1] , alphaThreshold)) 
				stereoPairs.push_back(pair);
		}
	}

	return stereoPairs;
};

//================================================================================

int CDEMMatcher::matchTile(int tileIndex)
{
	int nPointsMatched = 0;
	vector<vector<int>> stereoPairs = this->getStereoPairsImages(tileIndex);
				
	if (stereoPairs.size() < 1) 
	{
		ostrstream oss;
		oss << "\n     Tile " << tileIndex << " without stereo coverage!";
		protHandler.print(oss, protHandler.prt);
	}
	else
	{
		if (this->listener)
		{
			ostrstream oss; oss << "Matching tile " << tileIndex + 1 << " of " << this->TileFileNames.size() << ends;
			char *title = oss.str();
			this->listener->setTitleString(title); 
			delete [] title;
		}

		ostrstream oss;
		oss.setf(ios::fixed, ios::floatfield);
		oss.precision(2);

		oss << "     Tile "; oss.width(3);
		oss << tileIndex << ": Pmin:   " << TilePminVec[tileIndex].getString("  ", 10, 2) 
			<< ", Pmax:   "  << TilePmaxVec[tileIndex].getString("   ", 10, 2);
		oss << "\n               Number of stereo pairs: "; oss.width(3);
		oss << stereoPairs.size() << ";  Time: " << CSystemUtilities::timeString();

		protHandler.print(oss, protHandler.prt);

		C3DPoint minPt = TilePminVec[tileIndex];
		C3DPoint maxPt = TilePmaxVec[tileIndex];
		this->marginSize = this->imageVector[stereoPairs[0][0]]->getApproxGSD((this->Pmax.z + this->Pmin.z) * 0.5) * 75.0;
		minPt.x -= this->marginSize;  maxPt.x += this->marginSize;
		minPt.y -= this->marginSize;  maxPt.y += this->marginSize;

		for (unsigned int stereoIdx = 0; stereoIdx < stereoPairs.size(); ++stereoIdx)
		{
			C3DPoint sigPt = this->getExpectedSigma(stereoPairs[stereoIdx][0], stereoPairs[stereoIdx][1]);

			CDenseMatcher matcher(this->imageVector[stereoPairs[stereoIdx][0]]->getHugeImage(), 
								  this->imageVector[stereoPairs[stereoIdx][1]]->getHugeImage(), 
								  this->imageVector[stereoPairs[stereoIdx][0]]->getFrameCamera(), 
								  this->imageVector[stereoPairs[stereoIdx][1]]->getFrameCamera(),
								  minPt, maxPt, this->MatchingParameters, sigPt, 500);

			if (!matcher.isInitialised())
			{
				ostrstream oss;
				oss << "\n   Impossible to initialise matcher!";
				protHandler.print(oss, protHandler.prt);
			}
			else
			{
				CCharString fn = this->TileFileNames[tileIndex] + "_";
				fn.addInteger(stereoIdx, 0);
					
				if (outputMode > eMEDIUM) matcher.exportEpis(fn);
				
				if (matcher.match())
				{
					if (outputMode > eMEDIUM) matcher.exportParallax(fn);

					
					matcher.getPointCloud(&this->currentObjPts, this->currentPointCount, this->currentPointColours);
					
					ostrstream oss1;
					oss1 << "          ";  oss1.width(5); 
					oss1 << " ";           oss1.width(8);
					oss1 << (this->currentObjPts->getNpoints() - nPointsMatched) << " points matched in stereo pair "
						 << this->imageVector[stereoPairs[stereoIdx][0]]->getFileNameChar() << " / " << this->imageVector[stereoPairs[stereoIdx][1]]->getFileNameChar()
						 << "; Total number of points in Tile: ";   oss1.width(8);
					oss1 << this->currentObjPts->getNpoints() << "; Time: " 
						 << CSystemUtilities::timeString();
					protHandler.print(oss1, protHandler.prt);

					nPointsMatched = this->currentObjPts->getNpoints();
				}
			}

			if (this->listener)
			{
				this->listener->setProgressValue(100.0f * float(stereoIdx + 1) / float(stereoPairs.size()));
			}
		}
	}

	if (this->currentObjPts) 	
	{
		vector<int> hist(stereoPairs.size() + 1, 0);

		for (int i = 0, ind = 0; i < this->currentObjPts->getNpoints(); ++i)
		{
			unsigned char pc = this->currentPointCount[i];
			if (pc > stereoPairs.size()) pc = stereoPairs.size();
			hist[pc]++;
		}
 
		int minPtsSure = this->getPointCountThreshold(stereoPairs.size());

		this->exportPointCloud(this->TileFileNames[tileIndex].GetChar(), minPtsSure, this->TilePminVec[tileIndex], this->TilePmaxVec[tileIndex]);

		ostrstream oss1;
		oss1 << "\n     Tile " << tileIndex << ": " << this->currentObjPts->getNpoints() << " points matched in all stereo pairs\n     Histogram of overlap for points: ";
		for (unsigned int i = 1; i < hist.size(); ++i) oss1 << i << ": " << hist[i] << "; ";

        oss1 << "     Time: " << CSystemUtilities::timeString() << "\n";
		protHandler.print(oss1, protHandler.prt);

		bool OK = this->interpolateDEM(tileIndex, minPtsSure); 
	}

	this->clearPointCloud();

	return nPointsMatched;
};
	
//================================================================================

void CDEMMatcher::deleteOutliersInDEM(int tileIndex)
{
	CSortVector<double> NeighbourHeights(8, 0);

	float threshold = float(this->SigmaPoint.z * 1.96);

	int rMax = this->pDEM->getDEM()->getHeight() - this->DEMTileBuffer->getOffsetY() + 1;
	if (rMax > this->pDEM->getDEM()->getTileHeight()) rMax = this->pDEM->getDEM()->getTileHeight();
	rMax -= 1;

	vector<int> dNeighbourIdx = this->DEMTileBuffer->get8NeigbourIndexDiffs();

	int nValidNeighbours;
	vector<int> validNeighbourIndices(8);

	CImageBuffer buf(*this->DEMTileBuffer, false);
	buf.setAll(DEM_NOHEIGHT);

	for (int r = 1; r < rMax; ++r)
	{
		long idx = r * this->DEMTileBuffer->getDiffY() + 1;
		long idxmax = idx + this->DEMTileBuffer->getDiffY() - 2;

		for (; idx < idxmax; ++idx)
		{
			float Z = this->DEMTileBuffer->pixFlt(idx);
			if (Z > DEM_NOHEIGHT)
			{
				nValidNeighbours = 0;
				for (unsigned int i = 0; i < dNeighbourIdx.size(); ++i)
				{
					NeighbourHeights[i] = this->DEMTileBuffer->pixFlt(idx + dNeighbourIdx[i]);
					if (NeighbourHeights[i] > -9998.0)
					{
						validNeighbourIndices[nValidNeighbours] = i;
						++nValidNeighbours;
					}
				}

				if (nValidNeighbours > 2)
				{
					NeighbourHeights.sortDescending(); 
					float zMed = NeighbourHeights[nValidNeighbours / 2]; 
					if (fabs(zMed - Z) > threshold) buf.pixFlt(idx) = zMed;
				}
				else this->DEMTileBuffer->pixFlt(idx) = DEM_NOHEIGHT;
			}
		}
	};

	for (unsigned int i = 0; i < buf.getNGV(); ++i)
	{
		if (buf.pixFlt(i) > -9998.0)  this->DEMTileBuffer->pixFlt(i) = buf.pixFlt(i);
	}
};

//================================================================================

void CDEMMatcher::fillSmallGapsInDEM(int tileIndex)
{
	int N = 30;

	ANNkd_tree *kdTree =  this->currentObjPts->getKdTree();
	ANNpointArray annQueryPoints = new ANNpoint [N];
	ANNidxArray   nnIdx   = new ANNidx[N];  // near neighbor indices
	ANNdistArray  nnDists = new ANNdist[N]; // near neighbor distances
	ANNpoint queryPt = annAllocPt(3);
	vector<int> indexVec(N);

	CSortVector<double> NeighbourHeights(8, 0);

	float threshold = float(this->SigmaPoint.z * 1.96);

	int rMax = this->pDEM->getDEM()->getHeight() - this->DEMTileBuffer->getOffsetY() + 1;
	if (rMax > this->pDEM->getDEM()->getTileHeight()) rMax = this->pDEM->getDEM()->getTileHeight();
	rMax -= 1;

	vector<int> dNeighbourIdx = this->DEMTileBuffer->get8NeigbourIndexDiffs();

	int nValidNeighbours;
	vector<int> validNeighbourIndices(8);

	CImageBuffer buf(*this->DEMTileBuffer, false);
	buf.setAll(DEM_NOHEIGHT);

	double dx = this->pDEM->getDEM()->getgridX();
	double dy = this->pDEM->getDEM()->getgridY();
	float dxMax = 0.5f * (float) dx;
	float dyMax = 0.5f * (float) dy;

	queryPt[1] = this->TilePmaxVec[tileIndex].y - dy;

	for (int r = 1; r < rMax; ++r, queryPt[1] -= dy)
	{
		long idx = r * this->DEMTileBuffer->getDiffY() + 1;
		long idxmax = idx + this->DEMTileBuffer->getDiffY() - 2;
		queryPt[0] = this->TilePminVec[tileIndex].x + dx;

		for (; idx < idxmax; ++idx, queryPt[0] += dx)
		{
			float Z = this->DEMTileBuffer->pixFlt(idx);
			if (Z < -9998.0)
			{
				kdTree->annkSearch(queryPt, N, nnIdx, nnDists, 0.0); 
			
				nValidNeighbours = 0;
				for (unsigned int i = 0; i < dNeighbourIdx.size(); ++i)
				{
					NeighbourHeights[i] = this->DEMTileBuffer->pixFlt(idx + dNeighbourIdx[i]);
					if (NeighbourHeights[i] > -9998.0)
					{
						validNeighbourIndices[nValidNeighbours] = i;
						++nValidNeighbours;
					}
				}

				int nPtsFound = 0;
				for (int i = 0; i < N; ++i)
				{
					ANNpoint p = this->currentObjPts->getPt(nnIdx[i]);
					if ((fabs(queryPt[0] - p[0]) <=  dx) && (fabs(queryPt[1] - p[1]) <=  dy))
					{
						annQueryPoints[nPtsFound] = p;
						indexVec[nPtsFound] = nnIdx[i];
						++nPtsFound;
					}
				}

				if (nValidNeighbours > 0)
				{
					NeighbourHeights.sortDescending(); 
					float zMed = NeighbourHeights[nValidNeighbours / 2]; 

					double Sum = 0.0;
					int nPtsF = 0;
					for (int i = 0; i < nPtsFound; ++i)
					{
						if (fabs(zMed - annQueryPoints[i][2]) < threshold) 
						{
							Sum += annQueryPoints[i][2];
							++nPtsF;
						}
					}
						
					if (nPtsF > 0)
					{
						Z = float(Sum / double(nPtsF));
					}
					else if (nValidNeighbours > 2) 
					{
						Z  = zMed; 
					}
					else 
					{
						Z = DEM_NOHEIGHT;
					}

					buf.pixFlt(idx) = Z;
				}
			}
		}
	}

	for (unsigned int i = 0; i < buf.getNGV(); ++i)
	{
		if (buf.pixFlt(i) > -9998.0)  this->DEMTileBuffer->pixFlt(i) = buf.pixFlt(i);
	}

	delete [] annQueryPoints;
	delete [] nnIdx;
	delete [] nnDists;
	annDeallocPt(queryPt);
};

//================================================================================

void CDEMMatcher::fillLargeGapsInDEM(int tileIndex)
{
	CDEM *dem = this->pDEM->getDEM();

    int N = 30;
	float dx = dem->getgridX() / 2.0;
	float dy = dem->getgridY() / 2.0;


	ANNkd_tree *kdTree =  this->currentObjPts->getKdTree();
	ANNpointArray annQueryPoints = new ANNpoint [N];
	ANNidxArray   nnIdx   = new ANNidx[N];  // near neighbor indices
	ANNdistArray  nnDists = new ANNdist[N]; // near neighbor distances
	ANNpoint queryPt = annAllocPt(3);
	vector<int> indexVec(N);

	queryPt[1] = this->TilePmaxVec[tileIndex].y;

	int rMax = dem->getHeight() - this->DEMTileBuffer->getOffsetY() + 1;
	if (rMax > dem->getTileHeight()) rMax = dem->getTileHeight();

	for (int r = 0; r < rMax; ++r, queryPt[1] -= dem->getgridY())
	{
		long idx = r * this->DEMTileBuffer->getDiffY();
		long idxmax = idx + this->DEMTileBuffer->getDiffY();
		queryPt[0] = this->TilePminVec[tileIndex].x;

		for (; idx < idxmax; ++idx, queryPt[0] += dem->getgridX())
		{
			if (this->DEMTileBuffer->pixFlt(idx) < -9998.0)
			{
				kdTree->annkSearch(queryPt, N, nnIdx, nnDists, 0.0); 
				double Z = -9999.0;
				
				int nPtsFound = 0;
				for (int i = 0; i < N; ++i)
				{
					ANNpoint p = this->currentObjPts->getPt(nnIdx[i]);
					if ((fabs(queryPt[0] - p[0]) <=  dx) && (fabs(queryPt[1] - p[1]) <=  dy))
					{
						annQueryPoints[nPtsFound] = p;
						indexVec[nPtsFound] = nnIdx[i];
						++nPtsFound;
					}
				}

				if (nPtsFound >= 1)
				{
					if (nPtsFound > 1)
					{
						double MaxZ = annQueryPoints[0][2];
						int maxInt = 0;
						for (int i = 0; i < nPtsFound; ++i)
						{
							MaxZ = annQueryPoints[i][2];
							maxInt = i;
							for (int j = i + 1; j < nPtsFound; ++j)
							{
								if (annQueryPoints[j][2] > MaxZ) 
								{
									MaxZ = annQueryPoints[j][2];
									maxInt = j;
								}
							}
							double dmyZ = annQueryPoints[i][2];
							annQueryPoints[i][2] = MaxZ;
							annQueryPoints[maxInt][2] = dmyZ;
							int dmyI = indexVec[i];
							indexVec[i] = indexVec[maxInt];
							indexVec[maxInt] = dmyI;
						}
					}

					Z = getInterpolatedHeight(annQueryPoints, indexVec, nPtsFound, 1);
				}

				this->DEMTileBuffer->pixFlt(idx) = (float) Z;
			}
		}
	}

};
	 
//================================================================================

void CDEMMatcher::interpolateMissingGapsInDEM()
{
//	CImageBuffer buf(0,0, this->d;
//	CLabelImage l
};

//================================================================================

float CDEMMatcher::getInterpolatedHeight(ANNpointArray &points, vector<int> &indexVec, 
										 int nPoints, int minMatchCount) const
{
	int firstIndex = -1;

	for (int i = 0; i < nPoints; ++i)
	{
		if (this->currentPointCount[indexVec[i]] >= minMatchCount) 
		{
			if (firstIndex < 0) firstIndex = i;
			else return float(points[i][2]);
		}
	}
	
	if (firstIndex >= 0) return float(points[firstIndex][2]);
	return DEM_NOHEIGHT;
}

//================================================================================

bool CDEMMatcher::interpolateDEM(int tileIndex, int minMatchCount)
{
	if (this->listener)
	{
		ostrstream oss; oss << "Interpolating tile " << tileIndex + 1 << " of " << this->TileFileNames.size() << ends;
		char *title = oss.str();
		this->listener->setTitleString(title); 
		delete [] title;
	}

	ostrstream oss;
	oss << "\nStarting DEM interpolation for tile " << tileIndex << " ; Time: " << CSystemUtilities::timeString();
	protHandler.print(oss, protHandler.prt);

	bool success = this->prepHeightBuffer(tileIndex);

	if (success)
	{
		CDEM *dem = this->pDEM->getDEM();

		this->currentObjPts->setAnnDim(2);

		int N = 30;
		float dx = dem->getgridX() / 2.0;
		float dy = dem->getgridY() / 2.0;


		ANNkd_tree *kdTree =  this->currentObjPts->getKdTree();
		ANNpointArray annQueryPoints = new ANNpoint [N];
		ANNidxArray   nnIdx   = new ANNidx[N];  // near neighbor indices
		ANNdistArray  nnDists = new ANNdist[N]; // near neighbor distances
		ANNpoint queryPt = annAllocPt(3);
		vector<int> indexVec(N);

		queryPt[1] = this->TilePmaxVec[tileIndex].y;

		int rMax = dem->getHeight() - this->DEMTileBuffer->getOffsetY() + 1;
		if (rMax > dem->getTileHeight()) rMax = dem->getTileHeight();

		for (int r = 0; r < rMax; ++r, queryPt[1] -= dem->getgridY())
		{
			long idx = r * this->DEMTileBuffer->getDiffY();
			long idxmax = idx + this->DEMTileBuffer->getDiffY();
			queryPt[0] = this->TilePminVec[tileIndex].x;

			for (; idx < idxmax; ++idx, queryPt[0] += dem->getgridX())
			{
				kdTree->annkSearch(queryPt, N, nnIdx, nnDists, 0.0); 
				double Z = -9999.0;
				
				int nPtsFound = 0;
				for (int i = 0; i < N; ++i)
				{
					ANNpoint p = this->currentObjPts->getPt(nnIdx[i]);
					if ((fabs(queryPt[0] - p[0]) <=  dx) && (fabs(queryPt[1] - p[1]) <=  dy))
					{
						annQueryPoints[nPtsFound] = p;
						indexVec[nPtsFound] = nnIdx[i];
						++nPtsFound;
					}
				}

				if (nPtsFound >= 1)
				{
					if (nPtsFound > 1)
					{
						double MaxZ = annQueryPoints[0][2];
						int maxInt = 0;
						for (int i = 0; i < nPtsFound; ++i)
						{
							MaxZ = annQueryPoints[i][2];
							maxInt = i;
							for (int j = i + 1; j < nPtsFound; ++j)
							{
								if (annQueryPoints[j][2] > MaxZ) 
								{
									MaxZ = annQueryPoints[j][2];
									maxInt = j;
								}
							}
							double dmyZ = annQueryPoints[i][2];
							annQueryPoints[i][2] = MaxZ;
							annQueryPoints[maxInt][2] = dmyZ;
							int dmyI = indexVec[i];
							indexVec[i] = indexVec[maxInt];
							indexVec[maxInt] = dmyI;
						}
					}

					Z = getInterpolatedHeight(annQueryPoints, indexVec, nPtsFound, minMatchCount);
				}

				this->DEMTileBuffer->pixFlt(idx) = (float) Z;
			}
		}


		delete [] annQueryPoints;
		delete [] nnIdx;
		delete [] nnDists;
		annDeallocPt(queryPt);

		this->exportTileBuffer(this->TileFileNames[tileIndex] + "_dem_raw.tif");
		this->deleteOutliersInDEM(tileIndex);
		this->exportTileBuffer(this->TileFileNames[tileIndex] + "_dem_filtered.tif");
		this->fillSmallGapsInDEM(tileIndex);
		this->deleteOutliersInDEM(tileIndex);
		this->exportTileBuffer(this->TileFileNames[tileIndex] + "_dem_smallGaps.tif");
		this->fillLargeGapsInDEM(tileIndex);
		this->fillSmallGapsInDEM(tileIndex);
		this->interpolateMissingGapsInDEM();

		this->deleteOutliersInDEM(tileIndex);
		this->deleteOutliersInDEM(tileIndex);
		this->exportTileBuffer(this->TileFileNames[tileIndex] + "_dem_largeGaps.tif");
		
		if (pOuterPolyLine) 
		{
			CLabelImage labelImg(0,0,dem->getTileWidth(), dem->getTileHeight(), 8);
			labelImg.setOffset(this->TileOffset[tileIndex][0], this->TileOffset[tileIndex][1]);
			labelImg.setAll(0);
			labelImg.fill(*pOuterPolyLine, 1);
			for (int labelIdx = 0; labelIdx < labelImg.getNGV(); ++labelIdx)
			{
				if (!labelImg.getLabel(labelIdx)) this->DEMTileBuffer->pixFlt(labelIdx) = DEM_NOHEIGHT;
			}
		}

		success &= dem->dumpTile(*(this->DEMTileBuffer));
	}

	ostrstream oss1;
	oss1 << "\n             Interpolation finished; Time: " << CSystemUtilities::timeString();
	protHandler.print(oss1, protHandler.prt);

	return success;
};
	  
//================================================================================

bool CDEMMatcher::match()
{
	bool success = true;

	int nPointsMatched = 0;

	ostrstream oss; 
	oss.setf(ios::fixed, ios::floatfield);
	oss << "DEM Matching in Barista\n=======================\n\n";
	
	if (this->imageVector.size() < 2) 
	{
		oss << "\n\nError: Not enough images available";
		return false;
	}
	
	oss << "\nSearch images: ";
	
	for (int i = 0; i < this->imageVector.size(); ++i)
	{
		oss << this->imageVector[i]->getFileNameChar() <<"\n               ";
	}

	oss << this->MatchingParameters.getDescriptor().GetChar();
	
	oss.precision(3);
	oss << "\n\nParameters of the DEM:\n======================\n"
		<< "\n   Pmin:  " << this->Pmin.x << " / " << this->Pmin.y 
		<< "\n   Pmax:  " << this->Pmax.x << " / " << this->Pmax.y 
		<< "\n   Delta: " << this->pDEM->getDEM()->getgridX() << " [m]";

	oss << "\n\nMin / max height:\n================\n"
		<< "\n   Minimum Height: " << this->Pmin.z
		<< "\n   Maximum Height: " << this->Pmax.z;		

	
	if (this->pDEMtoSensorTrafo) delete this->pDEMtoSensorTrafo;
	if (this->pSensorToDEMTrafo) delete this->pSensorToDEMTrafo;

	this->pDEMtoSensorTrafo = CTrans3DFactory::initTransformation(this->pDEM->getReferenceSystem(), this->imageVector[0]->getCurrentSensorModel()->getRefSys());
	this->pSensorToDEMTrafo = CTrans3DFactory::initTransformation(this->imageVector[0]->getCurrentSensorModel()->getRefSys(),this->pDEM->getReferenceSystem());

	if (!this->pDEMtoSensorTrafo || !this->pSensorToDEMTrafo)
	{
		success = false;
		oss << "\n\nError: Cannot initialise transformation parameters between sensor and DEM coordinate systems";
		
		protHandler.print(oss, protHandler.prt);
	}
	else
	{
		CDEM *dem = this->pDEM->getDEM();

		dem->tileHeight = 512;
		dem->tileWidth  = 512;

		oss << "\n\nStarting Matching process: Date: " 
			<< CSystemUtilities::dateString().GetChar() << ", Time: " 
			<< CSystemUtilities::timeString().GetChar() << "\n";
		protHandler.print(oss, protHandler.prt);

		this->initTileStructure(); 

		ostrstream oss1;
		oss1.setf(ios::fixed, ios::floatfield);
		oss1.precision(2);
	
		int w = int(floor((this->Pmax.x - this->Pmin.x) / dem->getgridX())) + 1;
		int h = int(floor((this->Pmax.y - this->Pmin.y) / dem->getgridY())) + 1;

		success = dem->prepareWriting(dem->getFileNameChar(), w, h, 32, 1);
		
		for (int i = 0; i < this->nTiles() && success; ++i)
		{
			nPointsMatched += this->matchTile(i);

			
			if (this->listener)
			{
				this->listener->setProgressValue(100.0f * float(i + 1) / float(this->nTiles()));
			}
		}

		ostrstream oss;
		if (nPointsMatched > 0)
			oss << "\n     Altogether " << nPointsMatched << " 3D points determined \n";
		else 
		{
			success = false;
			oss << "\n     No 3D points determined \n";
		}

		protHandler.print(oss, protHandler.prt);


		if (success)
		{
			this->pDEM->getDEM()->setSigmaZ(float(SigmaPoint.z ));			
			success = dem->finishWriting();

			ostrstream oss3;
			oss3 << "\nDEM generation finished; Time: " << CSystemUtilities::timeString() << "\n";
			protHandler.print(oss3, protHandler.prt);
		}
	}

	if (this->pDEMtoSensorTrafo) delete this->pDEMtoSensorTrafo;
	this->pDEMtoSensorTrafo = 0;
	if (this->pSensorToDEMTrafo) delete this->pSensorToDEMTrafo;
	this->pSensorToDEMTrafo = 0;

	return success;
}

//================================================================================
	  	  
void CDEMMatcher::exportPointCloud(const char *fnBase,   int separateThreshold, 
								   const C3DPoint &pmin, const C3DPoint &pmax) const
{
	CCharString name1(fnBase), name2(fnBase);
	name1 += "_pts.xyz";

	ofstream ofile1 (name1.GetChar());
	ofile1.setf(ios::fixed, ios::floatfield);
	ofile1.precision(2);


	if (separateThreshold > 1) 
	{
		name2 += "_pts_mult.xyz";

		ofstream ofile2 (name2.GetChar());
		ofile2.setf(ios::fixed, ios::floatfield);
		ofile2.precision(2);

		for (int i = 0; i < this->currentObjPts->getNpoints(); ++i)
		{
			ANNpoint p = this->currentObjPts->getPt(i);
			if ((p[0] >= pmin.x) && (p[0] < pmax.x) && (p[1] >= pmin.y) && (p[1] < pmax.y))
			{
				if (this->currentPointCount[i] < separateThreshold)
				{
					for (unsigned char b = 0; b < 3; b++) ofile1 << p[b] << " ";
					for (unsigned char b = 0; b < 3; b++) ofile1 << int(this->currentPointColours[b][i]) << " ";
					ofile1 << this->currentPointCount[i] << "\n";
				}
				else
				{
					for (unsigned char b = 0; b < 3; b++) ofile2 << p[b] << " ";
					for (unsigned char b = 0; b < 3; b++) ofile2 << int(this->currentPointColours[b][i]) << " ";
					ofile2 << this->currentPointCount[i] << "\n";
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < this->currentObjPts->getNpoints(); ++i)
		{
			ANNpoint p = this->currentObjPts->getPt(i);
			if ((p[0] >= pmin.x) && (p[0] < pmax.x) && (p[1] >= pmin.y) && (p[1] < pmax.y))
			{
				for (unsigned char b = 0; b < 3; b++) ofile1 << p[b] << " ";
				for (unsigned char b = 0; b < 3; b++) ofile1 << int(this->currentPointColours[b][i]) << " ";
				ofile1 << this->currentPointCount[i] << "\n";
			}
		}
	};	
};

//================================================================================
	  
void CDEMMatcher::exportTileBuffer(const CCharString &filename) const
{
	CImageBuffer buf(0,0,this->DEMTileBuffer->getWidth(), this->DEMTileBuffer->getHeight(), 1,8, true, 0.0f);

	CLookupTable lut(8, 3);
	lut.setColor(0, 0, 255, 0);

	float zmin =  FLT_MAX;
	float zmax = -FLT_MAX;

	for (unsigned int i = 0; i < buf.getNGV(); ++i)
	{
		float Z = this->DEMTileBuffer->pixFlt(i);
		if (Z > -9998.0)
		{
			if (Z < zmin) zmin = Z;
			if (Z > zmax) zmax = Z;
		}
	}

	float fac = 254.0f / (zmax - zmin);

	for (unsigned int i = 0; i < buf.getNGV(); ++i)
	{
		float Z = this->DEMTileBuffer->pixFlt(i);
		if (Z > -9998.0)
		{
			buf.pixUChar(i) = 1 + unsigned char((Z - zmin) * fac);
		}
	}

	
	buf.exportToTiffFile(filename.GetChar(), &lut, false);
};

//================================================================================
	  