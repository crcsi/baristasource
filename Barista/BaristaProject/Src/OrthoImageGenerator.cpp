#include <float.h>
#include "OrthoImageGenerator.h"

#include "VDEM.h"
#include "RPC.h"
#include "VImage.h"
#include "TiffFile.h"
#include "ImgBuf.h"
#include "Trans3DFactory.h"
#include "SensorModel.h"
#include "GDALFile.h"
#include "doubles.h"

COrthoImageGenerator::COrthoImageGenerator() :  sensorModel(0),
				trans(0), bpp(8), nBands(1), inputPyrLevel(0),
				inputPyrFactor(1.0), AnchorWidth(10),
				inputimage(0), vdem(0), useAnchor(true)
{

}

COrthoImageGenerator::~COrthoImageGenerator(void)
{
	if (this->trans) delete this->trans;
	this->trans = 0;
}

void COrthoImageGenerator::setDEM(CVDEM *dem)
{
	this->vdem = dem;
};

void COrthoImageGenerator::setImage(CVImage *img)
{
	this->inputimage = img;

	// make sure the image is open
	this->inputimage->getHugeImage()->open(this->inputimage->getHugeImage()->getFileNameChar(),false);

	this->sensorModel = img->getCurrentSensorModel();
};

int COrthoImageGenerator::setPyrLevel()
{
	this->inputPyrLevel = 0;
	this->inputPyrFactor = 1.0;

	C3DPoint demPt, demPtX, demPtY, geoPt;
	C2DPoint imgPt, imgPtX, imgPtY;

	//addition >>
	if (this->orthoimage->getTFW()->getRefSys().getCoordinateType() == eGEOGRAPHIC)
	{
		demPt.y = this->llx + this->orthoimage->getHugeImage()->getWidth()  * 0.5f * this->gsd; // mid y of orthophoto (within DEM)
		demPt.x = this->ury - this->orthoimage->getHugeImage()->getHeight() * 0.5f * this->gsd; // mid x of orthophoto (within DEM)
	}
	else if (this->orthoimage->getTFW()->getRefSys().getCoordinateType() == eGRID)
	{ //<<addition
		demPt.x = this->llx + this->orthoimage->getHugeImage()->getWidth()  * 0.5f * this->gsd; // mid x of orthophoto (within DEM)
		demPt.y = this->ury - this->orthoimage->getHugeImage()->getHeight() * 0.5f * this->gsd; // mid y of orthophoto (within DEM)
		//addition >>
	}//<<addition
	demPt.z = (this->vdem->getDEM()->getminZ() + this->vdem->getDEM()->getmaxZ()) * 0.5f; // average height = (max-min)/2

	demPtX = demPt;
	demPtY = demPt;
	demPtX.x += this->gsd; // next point just right of the center
	demPtY.y += this->gsd; // next point just above the center

	this->trans->transform(geoPt, demPt);
	this->sensorModel->transformObj2Obs(imgPt, geoPt);
	this->trans->transform(geoPt, demPtX);
	this->sensorModel->transformObj2Obs(imgPtX, geoPt);
	this->trans->transform(geoPt, demPtY);
	this->sensorModel->transformObj2Obs(imgPtY, geoPt);

	double distX = imgPt.getDistance(imgPtX); // distance (in the ortho-image space) in x-direction of a grid in orthophoto
	double distY = imgPt.getDistance(imgPtY); // distance (in the ortho-image space) in y-direction of a grid in orthophoto
	double distMax = (distX > distY) ? distX : distY;

	int maxLevels = this->inputimage->getHugeImage()->getNumberOfPyramidLevels() - 1;

	while ((distMax > 2.0f) && (this->inputPyrLevel < maxLevels))
	{
		this->inputPyrLevel  += 1;
		this->inputPyrFactor *= 2.0;
		distMax *= 0.5;
	}

	return this->inputPyrLevel;
};

bool COrthoImageGenerator::CreateOrthoImage()
{
	bool ret = true;
	this->bpp = this->inputimage->getHugeImage()->getBpp();
	this->nBands = this->inputimage->getHugeImage()->getBands();
	double width = (this->urx - this->llx)/this->gsd;
	double height = (this->ury - this->lly)/this->gsd;
	this->orthoCols = (int)(width + 1);
	this->orthoRows = (int)(height+ 1);

	//addition >>
	if (this->orthoCols < 1 || this->orthoRows < 1)
	{
		ret = false;
		this->errorMessage = "Ortho image initialisation failed!";
	}
	else
	{
	//<< addition

	CCharString hugname(this->orthoimage->getFileNameChar());
	hugname += ".hug";

	this->inputimage->getHugeImage()->resetTiles();

	if (!this->orthoimage->getHugeImage()->prepareWriting(hugname.GetChar(), this->orthoCols, this->orthoRows, this->bpp, this->nBands))
	{
		ret = false;
		this->errorMessage = "Ortho image initialisation failed!";
	}
	else
	{
		this->orthoimage->getHugeImage()->computeTotalTileCount(this->orthoRows, this->orthoCols);

		CReferenceSystem from = this->vdem->getReferenceSystem();
		CReferenceSystem to = this->sensorModel->getRefSys();
		this->trans = CTrans3DFactory::initTransformation(from,to );

		if (trans)
		{
			setPyrLevel();
			LoopTiles();

			this->inputimage->getHugeImage()->resetTiles();

			delete trans;
			trans = 0;
		}
		else
		{
			ret = false;
			this->errorMessage =  "Initialisiation of transformation between DEM and image failed!";
		}
	}
	//addition>>
	}
	//<< addition
	return ret;
}

bool COrthoImageGenerator::initDEMbuffer(CImageBuffer &buf, C2DPoint &shift, C2DPoint &pix,
										 double &xmin, double &xmax,
										 double &ymin, double &ymax,
										 int AnchorFactor)
{
	bool ret = true;

	shift.x = xmin;
	shift.y = ymax;
	pix.x = double(AnchorFactor) * this->gsd / this->vdem->getDEM()->getgridX();
	pix.y = double(AnchorFactor) * this->gsd / this->vdem->getDEM()->getgridY();

	float oneByFac = 1.0f / float(AnchorFactor);
	long offsetX = 0;
	long offsetY = 0;

	//addition >>
	double DEMLLCX;
	double DEMLLCY;
	double DEMRUCX;
	double DEMRUCY;
	long width, height;
	if (this->vdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
	{
		DEMLLCY = this->vdem->getDEM()->getminX();
		DEMLLCX = this->vdem->getDEM()->getminY();
		DEMRUCY = this->vdem->getDEM()->getmaxX();
		DEMRUCX = this->vdem->getDEM()->getmaxY();		

		if (xmin <= DEMLLCX) xmin = DEMLLCX;
		else
		{
			offsetX = int((xmin - DEMLLCX) / this->vdem->getDEM()->getgridX()); //int(floor(xmin - DEMLLCX) / this->vdem->getDEM()->getgridX());
			xmin = DEMLLCX + double(offsetX) * this->vdem->getDEM()->getgridX();
		}

		if (ymax >= DEMRUCY) ymax = DEMRUCY;
		else
		{
			offsetY = int((DEMRUCY - ymax) / this->vdem->getDEM()->getgridY()); //int(floor(DEMRUCY - ymax) / this->vdem->getDEM()->getgridY());
			ymax = DEMRUCY - double(offsetY) * this->vdem->getDEM()->getgridY();
		}

		if (ymin <= DEMLLCY) ymin = DEMLLCY;
		else
		{
			int offsetYMin = int((DEMRUCY - ymin) / this->vdem->getDEM()->getgridY()) + 1;//int(floor(DEMRUCY - ymin) / this->vdem->getDEM()->getgridY()) + 1;
			ymin = DEMRUCY - double(offsetYMin) * this->vdem->getDEM()->getgridY();
		}

		if (xmax >= DEMRUCX) xmax = DEMRUCX;
		else
		{
			int offsetXmax = int((xmax - DEMLLCX) / this->vdem->getDEM()->getgridX()) + 1;//int(floor(xmax - DEMLLCX) / this->vdem->getDEM()->getgridX()) + 1;
			xmax = DEMLLCX + double(offsetXmax) * this->vdem->getDEM()->getgridX();
		}

		shift.x =  double(AnchorFactor) * (shift.x - xmin) / this->vdem->getDEM()->getgridX();
		shift.y = -double(AnchorFactor) * (shift.y - ymax) / this->vdem->getDEM()->getgridY();

		//doubles db;
		//double d = db.round(xmax-xmin);
		width  = floor((xmax - xmin) / this->vdem->getDEM()->getgridX()) + 1;//floor(db.round(xmax - xmin) / this->vdem->getDEM()->getgridX()) + 1;
		height = floor((ymax - ymin) / this->vdem->getDEM()->getgridY()) + 1;//floor(db.round(ymax - ymin) / this->vdem->getDEM()->getgridY()) + 1;
	}
	else //if (this->vdem->getReferenceSystem().getCoordinateType() == eGRID)
	{
		DEMLLCX = this->vdem->getDEM()->getminX();
		DEMLLCY = this->vdem->getDEM()->getminY();
		DEMRUCX = this->vdem->getDEM()->getmaxX();
		DEMRUCY = this->vdem->getDEM()->getmaxY();
	
		if (xmin <= DEMLLCX) xmin = DEMLLCX;
		else
		{
			offsetX = int(floor(xmin - DEMLLCX) / this->vdem->getDEM()->getgridX());
			xmin = DEMLLCX + double(offsetX) * this->vdem->getDEM()->getgridX();
		}

		if (ymax >= DEMRUCY) ymax = DEMRUCY;
		else
		{
			offsetY = int(floor(DEMRUCY - ymax) / this->vdem->getDEM()->getgridY());
			ymax = DEMRUCY - double(offsetY) * this->vdem->getDEM()->getgridY();
		}

		if (ymin <= DEMLLCY) ymin = DEMLLCY;
		else
		{
			int offsetYMin = int(floor(DEMRUCY - ymin) / this->vdem->getDEM()->getgridY()) + 1;
			ymin = DEMRUCY - double(offsetYMin) * this->vdem->getDEM()->getgridY();
		}

		if (xmax >= DEMRUCX) xmax = DEMRUCX;
		else
		{
			int offsetXmax = int(floor(xmax - DEMLLCX) / this->vdem->getDEM()->getgridX()) + 1;
			xmax = DEMLLCX + double(offsetXmax) * this->vdem->getDEM()->getgridX();
		}
		
		shift.x =  double(AnchorFactor) * (shift.x - xmin) / this->vdem->getDEM()->getgridX();
		shift.y = -double(AnchorFactor) * (shift.y - ymax) / this->vdem->getDEM()->getgridY();

		doubles db;
		//double d = db.round(xmax-xmin);
		width  = floor(db.round(xmax - xmin) / this->vdem->getDEM()->getgridX()) + 1;
		height = floor(db.round(ymax - ymin) / this->vdem->getDEM()->getgridY()) + 1;
	}
	//<<addition
	
	/*
	if (xmin <= this->vdem->getDEM()->getminX()) xmin = this->vdem->getDEM()->getminX();
	else
	{
		offsetX = int(floor(xmin - this->vdem->getDEM()->getminX()) / this->vdem->getDEM()->getgridX());
		xmin = this->vdem->getDEM()->getminX() + double(offsetX) * this->vdem->getDEM()->getgridX();
	}

	if (ymax >= this->vdem->getDEM()->getmaxY()) ymax = this->vdem->getDEM()->getmaxY();
	else
	{
		offsetY = int(floor(this->vdem->getDEM()->getmaxY() - ymax) / this->vdem->getDEM()->getgridY());
		ymax = this->vdem->getDEM()->getmaxY() - double(offsetY) * this->vdem->getDEM()->getgridY();
	}

	if (ymin <= this->vdem->getDEM()->getminY()) ymin = this->vdem->getDEM()->getminY();
	else
	{
		int offsetYMin = int(floor(this->vdem->getDEM()->getmaxY() - ymin) / this->vdem->getDEM()->getgridY()) + 1;
		ymin = this->vdem->getDEM()->getmaxY() - double(offsetYMin) * this->vdem->getDEM()->getgridY();
	}

	if (xmax >= this->vdem->getDEM()->getmaxX()) xmax = this->vdem->getDEM()->getmaxX();
	else
	{
		int offsetXmax = int(floor(xmax - this->vdem->getDEM()->getminX()) / this->vdem->getDEM()->getgridX()) + 1;
		xmax = this->vdem->getDEM()->getminX() + double(offsetXmax) * this->vdem->getDEM()->getgridX();
	}
	
	

	shift.x =  double(AnchorFactor) * (shift.x - xmin) / this->vdem->getDEM()->getgridX();
	shift.y = -double(AnchorFactor) * (shift.y - ymax) / this->vdem->getDEM()->getgridY();

	doubles db;
	//double d = db.round(xmax-xmin);
	long width  = floor(db.round(xmax - xmin) / this->vdem->getDEM()->getgridX()) + 1;
	long height = floor(db.round(ymax - ymin) / this->vdem->getDEM()->getgridY()) + 1;
*/
	if ((width < 1) || (height < 1))
	{
		ret = false;
	}
	else
	{
		if (AnchorFactor < 2)
		{
			buf.set(offsetX, offsetY,width, height, 1, 32, false);
			ret = this->vdem->getDEM()->getRect(buf);
		}
		else //the following code was not checked whether it works for ortho & DEM in geographic grids

		{
			CImageBuffer rawBuf(offsetX, offsetY,width, height, 1, 32, false);
			ret = this->vdem->getDEM()->getRect(rawBuf);
			if (ret)
			{
				buf.set(0,0, (width - 1) * AnchorFactor + 1, (height - 1) * AnchorFactor + 1, 1, 32, false);
				for (int r = 0, rNew = 0; r < rawBuf.getHeight(); ++r, rNew += AnchorFactor)
				{
					long iMin = r * rawBuf.getDiffY();
					long iMax = iMin + rawBuf.getDiffY();
					long iMinNew = rNew * buf.getDiffY();
					float prevZ = rawBuf.pixFlt(iMin);

					buf.pixFlt(iMinNew) = prevZ;
					iMin += rawBuf.getDiffX();
					for (;iMin < iMax; ++iMin, iMinNew += AnchorFactor)
					{
						float currZ = rawBuf.pixFlt(iMin);
						if ((prevZ <= -9999.0) || (currZ <= -9999.0))
						{
							for (int i = 1; i < AnchorFactor; ++i) buf.pixFlt(iMinNew + i) = -9999.0f;
							buf.pixFlt(iMinNew + AnchorFactor) = currZ;
						}
						else
						{
							float k = (currZ - prevZ) * oneByFac;
							float Z = prevZ + k;
							for (int i = 1; i < AnchorFactor; ++i, Z += k) buf.pixFlt(iMinNew + i) = Z;

							buf.pixFlt(iMinNew + AnchorFactor) = currZ;
						}
						prevZ = currZ;
						
					}
				}

				long delta = buf.getDiffY() * AnchorFactor;

				for (int r = AnchorFactor; r < buf.getHeight(); r += AnchorFactor)
				{
					long iMin  = r * buf.getDiffY();
					long iMax  = iMin + buf.getDiffY();
					long iPrev = iMin - delta;
					long ip;

					for (;iMin < iMax; ++iMin, ++iPrev)
					{
						float prevZ = buf.pixFlt(iPrev);
						float currZ = buf.pixFlt(iMin);
						ip = iPrev + buf.getDiffY();
						if ((prevZ <= -9999.0) || (currZ <= -9999.0))
						{
							for (int i = 1; i < AnchorFactor; ++i, ip += buf.getDiffY()) buf.pixFlt(ip) = -9999.0;
						}
						else
						{
							float k = (currZ - prevZ) * oneByFac;
							float Z = prevZ + k;

							for (int i = 1; i < AnchorFactor; ++i, Z += k, ip += buf.getDiffY()) buf.pixFlt(ip) = Z;
						}
					}
				}
			}
		}
	}

	return ret;
};


bool COrthoImageGenerator::computeImgCoordsForTile(C2DPoint &firstDEMPoint, double *xArray, double *yArray,
												   double &xmin, double &xmax,double &ymin, double &ymax,
												   double AnchorFactor)
{
	xmin = ymin =  FLT_MAX;
	xmax = ymax = -FLT_MAX;

	int tilewidth  = this->orthoimage->getHugeImage()->getTileWidth();
	int tileheight = this->orthoimage->getHugeImage()->getTileHeight();
	double coordFactor = 1.0 / this->inputPyrFactor;
	double coordOffset = (this->inputPyrFactor - 1.0) * 0.5;
	int imgSubWidth  = this->inputimage->getHugeImage()->getWidth()  / int(this->inputPyrFactor);
	int imgSubHeight = this->inputimage->getHugeImage()->getHeight() / int(this->inputPyrFactor);

// initialise DEM in buffer
	double DEMxmin = firstDEMPoint.x; double DEMxmax = DEMxmin + double(tilewidth  - 1) * this->gsd;
	double DEMymax = firstDEMPoint.y; double DEMymin = DEMymax - double(tileheight - 1) * this->gsd ;
	CImageBuffer DEMbuf;
	C2DPoint orthoToDEMShift, orthoToDEMPix;

	//find corresponding DEM buffer => area, updated positions (DEMxmin, DEMxmax, DEMymin, DEMymax), shift & pixel ratio info (orthoToDEMShift, orthoToDEMPix)
	bool ret = initDEMbuffer(DEMbuf, orthoToDEMShift, orthoToDEMPix, DEMxmin, DEMxmax, DEMymin, DEMymax, AnchorFactor); // orthoToDEMShift, orthoToDEMPix are unknown when called


	if (ret)
	{
		// back-project DEM grid points to the source image 
		int iOK = 0;
		double *imgXBack = new double[DEMbuf.getNGV()];
		double *imgYBack = new double[DEMbuf.getNGV()];
		C2DPoint imgPoint, gridDEM(this->vdem->getDEM()->getgridX() / AnchorFactor, this->vdem->getDEM()->getgridY() / AnchorFactor);
		C3DPoint demPoint, 	geoPoint, orthoPoint;

		demPoint.y = DEMymax;
		

		for (unsigned long uMin = 0; uMin < DEMbuf.getNGV(); uMin += DEMbuf.getDiffY(), demPoint.y -= gridDEM.y)
		{
			demPoint.x  = DEMxmin;
			unsigned long indexRowMax = uMin + DEMbuf.getDiffY();
			for (unsigned long index = uMin; index < indexRowMax; ++index, demPoint.x += gridDEM.x)
			{
				imgXBack[index] = -1.0;
				imgYBack[index] = -1.0;

				float Z = DEMbuf.pixFlt(index);
				
				if (Z > -9999.0f)
				{
					demPoint.z = double(Z);
					trans->transform(geoPoint, demPoint);

					// reproject to sensor frame
					this->sensorModel->transformObj2Obs(imgPoint, geoPoint);

					imgPoint.x = (imgPoint.x - coordOffset) * coordFactor;
					imgPoint.y = (imgPoint.y - coordOffset) * coordFactor;

					if ((imgPoint.y > 0) && (imgPoint.y < imgSubHeight) &&
						(imgPoint.x > 0) && (imgPoint.x < imgSubWidth))
					{  // interpolate pixel value for ortho image
						imgXBack[index] = imgPoint.x;
						imgYBack[index] = imgPoint.y;
						iOK++;
					}				
				}
			}
		};

		ret = (iOK > 0);
		if (ret)
		{
			// interpolate image coordinates from the back-projected points		
			for (long r = 0, index0 = 0; r < tileheight; index0 += tilewidth, ++r)
			{
				orthoPoint.y = r;
				demPoint.y = orthoPoint.y * orthoToDEMPix.y + orthoToDEMShift.y;
				long rDEM = floor(demPoint.y);

				if ((rDEM < 0) || (rDEM >= DEMbuf.getHeight()))
				{
					long indexMax = index0 + tilewidth;

					for (long  i = index0; i < indexMax; ++i)
					{
						xArray[i] = -1;
						yArray[i] = -1;
					}
				}
				else
				{
					long index0DEM = rDEM * DEMbuf.getDiffY();
					long index0DEM1 = index0DEM + DEMbuf.getDiffY(); // neighbor point on the next column
					if (index0DEM1 >= DEMbuf.getNGV()) index0DEM1 = index0DEM;
					double dy = demPoint.y - rDEM;

					for ( long index = index0, c = 0; c < tilewidth; ++index, ++c)
					{
						orthoPoint.x = c;
						demPoint.x = orthoPoint.x * orthoToDEMPix.x + orthoToDEMShift.x;// - 2; // to test 2 subtracted
						long cDEM = floor(demPoint.x);
					
						if ((cDEM < 0) || (cDEM >= DEMbuf.getWidth()))
						{
							xArray[index] = -1;
							yArray[index] = -1;
						}
						else
						{
							double dx = demPoint.x - cDEM;
							double dxy = dx * dy;

							double x00, y00, x10, y10, x01, y01, x11, y11;
							long idxDEM  = index0DEM  + cDEM;
							long idxDEM1 = index0DEM1 + cDEM;
							x00 = imgXBack[idxDEM];
							y00 = imgYBack[idxDEM];
							x01 = imgXBack[idxDEM1];
							y01 = imgYBack[idxDEM1];

							if (cDEM + 1 < DEMbuf.getWidth())
							{
								idxDEM  += DEMbuf.getDiffX();
								idxDEM1 += DEMbuf.getDiffX();
							}

							x10 = imgXBack[idxDEM];
							y10 = imgYBack[idxDEM];
							x11 = imgXBack[idxDEM1];
							y11 = imgYBack[idxDEM1];
							if ((x00 < 0.0) || (x10 < 0.0) || (x01 < 0.0) || (x11 < 0.0))
							{
								int nct = 0;
								if (x00 >= 0.0) ++nct;
								if (x10 >= 0.0) ++nct;
								if (x01 >= 0.0) ++nct;
								if (x11 >= 0.0) ++nct;
								if (nct < 3)
								{
									xArray[index] = -1.0;
									yArray[index] = -1.0;
								}
								else
								{
									if (x00 < 0.0)
									{
										xArray[index] = (x01 + x10 - x11) + (x11 - x01) * dx + (x11 - x10) * dy;
										yArray[index] = (y01 + y10 - y11) + (y11 - y01) * dx + (y11 - y10) * dy;
									}
									else if (x10 < 0.0)
									{
										xArray[index] = x00 + (x11 - x01) * dx + (x01 - x00) * dy;
										yArray[index] = y00 + (y11 - y01) * dx + (y01 - y00) * dy;
									}
									else if (x01 < 0.0)
									{
										xArray[index] = x00 + (x10 - x00) * dx + (x11 - x10) * dy;
										yArray[index] = y00 + (y10 - y00) * dx + (y11 - y10) * dy;
									}
									else //(x11 < 0.0)
									{
										xArray[index] = x00 + (x10 - x00) * dx + (x01 - x00) * dy;
										yArray[index] = y00 + (y10 - y00) * dx + (y01 - y00) * dy;
									}
								}
							}
							else
							{
								xArray[index] = x00 + (x10 - x00) * dx + (x01 - x00) * dy + ((x11 + x00) - (x01 + x10)) * dxy;
								yArray[index] = y00 + (y10 - y00) * dx + (y01 - y00) * dy + ((y11 + y00) - (y01 + y10)) * dxy;
							}


							if (xArray[index] > xmax) xmax = xArray[index];
							if (xArray[index] < xmin) xmin = xArray[index];
							if (yArray[index] > ymax) ymax = yArray[index];
							if (yArray[index] < ymin) ymin = yArray[index];
						}						
					}
				}
			}
		}
		delete[] imgXBack;
		delete[] imgYBack;

		xmin = floor(xmin) - 7.0;
		xmax = floor(xmax) + 7.0;
		ymin = floor(ymin) - 7.0;
		ymax = floor(ymax) + 7.0;
		if (xmin < 0) xmin = 0.0;
		if (ymin < 0) ymin = 0.0;
		if (xmax >= this->inputimage->getHugeImage()->getWidth())
			xmax = this->inputimage->getHugeImage()->getWidth() - 1;
		if (ymax >= this->inputimage->getHugeImage()->getHeight())
			ymax = this->inputimage->getHugeImage()->getHeight() - 1;

		long dx = (long) (xmax - xmin);
		long dy = (long) (ymax - ymin);
		ret = ((iOK > 1) && (dx > 0) && (dy > 0));
	}
	return ret;
};


bool COrthoImageGenerator::computeImgCoordsForTile(C2DPoint &firstDEMPoint, double *xArray, double *yArray,
												   double &xmin, double &xmax,double &ymin, double &ymax)
{
	xmin = ymin =  FLT_MAX;
	xmax = ymax = -FLT_MAX;

	int tilewidth  = this->orthoimage->getHugeImage()->getTileWidth();
	int tileheight = this->orthoimage->getHugeImage()->getTileHeight();
	long indexMax = tilewidth * tileheight;
	double coordFactor = 1.0 / this->inputPyrFactor;
	double coordOffset = (this->inputPyrFactor - 1.0) * 0.5;
	int imgSubWidth  = this->inputimage->getHugeImage()->getWidth()  / int(this->inputPyrFactor);
	int imgSubHeight = this->inputimage->getHugeImage()->getHeight() / int(this->inputPyrFactor);

	double DEMxmin = firstDEMPoint.x; double DEMxmax = DEMxmin + double(tilewidth  - 1) * this->gsd;
	double DEMymax = firstDEMPoint.y; double DEMymin = DEMymax - double(tileheight - 1) * this->gsd ;
	CImageBuffer DEMbuf;
	C2DPoint orthoToDEMShift, orthoToDEMPix;


	C2DPoint imgPoint;
	C3DPoint demPoint, 	geoPoint;
	demPoint.y = firstDEMPoint.y;
    int iOK = 0;

	if (!initDEMbuffer(DEMbuf, orthoToDEMShift, orthoToDEMPix, DEMxmin, DEMxmax, DEMymin, DEMymax, 1))
	{
		for (int i = 0; i < indexMax; ++i)
		{
			xArray[i] = -1.0;
			yArray[i] = -1.0;
		}
	}
	else
	{
		// process the current tile for orthoimage
		for ( long index0 = 0, r = 0; index0 < indexMax; index0 += tilewidth,  r++, demPoint.y -= this->gsd)
		{
			demPoint.x  = firstDEMPoint.x;
			int indexRowMax = index0 + tilewidth;

			double dy = double(r) * orthoToDEMPix.y + orthoToDEMShift.y;
			long rDEM = floor(dy);
			dy -= rDEM;

			if ((rDEM < 0) || (rDEM >= DEMbuf.getHeight()))
			{
				for (long  i = index0; i < indexMax; ++i)
					{
						xArray[i] = -1;
						yArray[i] = -1;
					}
			}
			else 
			{
				long index0DEM = rDEM * DEMbuf.getDiffY();
				long index0DEM1 = index0DEM + DEMbuf.getDiffY();
				if (index0DEM1 >= DEMbuf.getNGV()) index0DEM1 = index0DEM;


				for ( int index = index0, c = 0; index < indexRowMax; ++index, ++c, demPoint.x += this->gsd)
				{
					double dx = double(c) * orthoToDEMPix.x + orthoToDEMShift.x;
					long cDEM = floor(dx);
					dx -= cDEM;
					if ((cDEM < 0) || (cDEM >= DEMbuf.getWidth()))
					{
						xArray[index] = -1;
						yArray[index] = -1;
					}
					else
					{
						double dxy = dx * dy;

						float z00, z10, z01, z11;
						long idxDEM  = index0DEM  + cDEM;
						long idxDEM1 = index0DEM1 + cDEM;
						z00 = DEMbuf.pixFlt(idxDEM);
						z01 = DEMbuf.pixFlt(idxDEM1);

						if (cDEM + 1 < DEMbuf.getWidth())
						{
							idxDEM  += DEMbuf.getDiffX();
							idxDEM1 += DEMbuf.getDiffX();
						}

						z10 = DEMbuf.pixFlt(idxDEM);
						z11 = DEMbuf.pixFlt(idxDEM1);

						if ((z00 <= -9999.0) || (z10 <  -9999.0) || (z01 < -9999.0) || (z11 <  -9999.0))
						{
							demPoint.z = -9999.0;
						}
						else
						{
							demPoint.z = (float) (z00 + (z10 - z00) * dx + (z01 - z00) * dy + ((z11 + z00) - (z01 + z10)) * dxy);
						}

						if ( demPoint.z != -9999.0)
						{
							//addition >>
							C3DPoint demPointg;
							if (this->vdem->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
							{
								demPointg.y = demPoint.x;
								demPointg.x = demPoint.y;
								demPointg.z = demPoint.z;
							}
							else //in UTM grid
								demPointg = demPoint;
							// transform dempoint to sensor reference frame
							trans->transform(geoPoint, demPointg);
							//<< addition

							/*// transform dempoint to sensor reference frame
							trans->transform(geoPoint, demPoint);*/

							// reproject to sensor frame
							this->sensorModel->transformObj2Obs(imgPoint, geoPoint);

							imgPoint.x = (imgPoint.x - coordOffset) * coordFactor ;
							imgPoint.y = (imgPoint.y - coordOffset) * coordFactor;

							if ((imgPoint.y > 0) && (imgPoint.y < imgSubHeight) &&
								(imgPoint.x > 0) && (imgPoint.x < imgSubWidth))
							{  // interpolate pixel value for ortho image
								xArray[index] = imgPoint.x;
								yArray[index] = imgPoint.y;
								if (imgPoint.x > xmax) xmax = imgPoint.x;
								if (imgPoint.x < xmin) xmin = imgPoint.x;
								if (imgPoint.y > ymax) ymax = imgPoint.y;
								if (imgPoint.y < ymin) ymin = imgPoint.y;
								iOK++;
							}
							else
							{  // re-projected point is outside the image
								xArray[index] = -1;
								yArray[index] = -1;
							}

						}
						else
						{ // no DEM heigth available
							xArray[index] = -1;
							yArray[index] = -1;
						}
					}
				}
			}
		}


		xmin = floor(xmin) - 7.0;
		xmax = floor(xmax) + 7.0;
		ymin = floor(ymin) - 7.0;
		ymax = floor(ymax) + 7.0;
		if (xmin < 0) xmin = 0.0;
		if (ymin < 0) ymin = 0.0;
		if (xmax >= this->inputimage->getHugeImage()->getWidth())
			xmax = this->inputimage->getHugeImage()->getWidth() - 1;
		if (ymax >= this->inputimage->getHugeImage()->getHeight())
			ymax = this->inputimage->getHugeImage()->getHeight() - 1;
	}

	long dx = (long) (xmax - xmin);
	long dy = (long) (ymax - ymin);
	bool ret = ((iOK > 1) && (dx > 0) && (dy > 0));
	return ret;
};

void COrthoImageGenerator::Create8bitTileBilin(unsigned char *orthoBuf,
											   double *xArray, double *yArray,
											   const CImageBuffer &imgBuf)
{
	C2DPoint p;

	long coordIdx = 0;
	int diffX = imgBuf.getDiffX();

	int tilewidth  = this->orthoimage->getHugeImage()->getTileWidth();
	int tileheight = this->orthoimage->getHugeImage()->getTileHeight();

	long iMax = tilewidth * tileheight * diffX;

	memset(orthoBuf, 0, iMax * sizeof(unsigned char));

	for (long imgIdx = 0; imgIdx < iMax; imgIdx += diffX, ++coordIdx)
	{
		if (xArray[coordIdx] >= 0)
		{
			p.x = xArray[coordIdx];
			p.y = yArray[coordIdx];
			imgBuf.getInterpolatedColourVecBilin(p, orthoBuf + imgIdx);
		}	
	}
}

void COrthoImageGenerator::Create16bitTileBilin(unsigned short *orthoBuf,
												double *xArray, double *yArray,
												const CImageBuffer &imgBuf)
{
	C2DPoint p;

	long coordIdx = 0;
	int diffX = imgBuf.getDiffX();

	int tilewidth  = this->orthoimage->getHugeImage()->getTileWidth();
	int tileheight = this->orthoimage->getHugeImage()->getTileHeight();

	long iMax = tilewidth * tileheight * diffX;

	memset(orthoBuf, 0, iMax * sizeof(unsigned short));

	for (long imgIdx = 0; imgIdx < iMax; imgIdx += diffX, ++coordIdx)
	{
		if (xArray[coordIdx] >= 0)
		{
			p.x = xArray[coordIdx];
			p.y = yArray[coordIdx];
			imgBuf.getInterpolatedColourVecBilin(p, orthoBuf + imgIdx);
		};
	}
}


void COrthoImageGenerator::Create8bitTileBicub(unsigned char *orthoBuf,
											   double *xArray, double *yArray,
											   const CImageBuffer &imgBuf)
{
	C2DPoint p;

	long coordIdx = 0;
	int diffX = imgBuf.getDiffX();

	int tilewidth  = this->orthoimage->getHugeImage()->getTileWidth();
	int tileheight = this->orthoimage->getHugeImage()->getTileHeight();

	long iMax = tilewidth * tileheight * diffX;

	memset(orthoBuf, 0, iMax * sizeof(unsigned char));

	for (long imgIdx = 0; imgIdx < iMax; imgIdx += diffX, ++coordIdx)
	{
		if (xArray[coordIdx] >= 0)
		{
			p.x = xArray[coordIdx];
			p.y = yArray[coordIdx];
			imgBuf.getInterpolatedColourVecBicub(p, orthoBuf + imgIdx);
		};
	}
}

void COrthoImageGenerator::Create16bitTileBicub(unsigned short *orthoBuf,
												double *xArray, double *yArray,
												const CImageBuffer &imgBuf)
{
	C2DPoint p;

	long coordIdx = 0;
	int diffX = imgBuf.getDiffX();

	int tilewidth  = this->orthoimage->getHugeImage()->getTileWidth();
	int tileheight = this->orthoimage->getHugeImage()->getTileHeight();

	long iMax = tilewidth * tileheight * diffX;

	memset(orthoBuf, 0, iMax * sizeof(unsigned short));

	for (long imgIdx = 0; imgIdx < iMax; imgIdx += diffX, ++coordIdx)
	{
		if (xArray[coordIdx] >= 0)
		{
			p.x = xArray[coordIdx];
			p.y = yArray[coordIdx];
			imgBuf.getInterpolatedColourVecBicub(p, orthoBuf + imgIdx);
		};
	}
}

bool COrthoImageGenerator::LoopTiles()
{
	C2DPoint demPt;

	int tilewidth  = this->orthoimage->getHugeImage()->getTileWidth();
	int tileheight = this->orthoimage->getHugeImage()->getTileHeight();

	int tilesdown   = this->orthoimage->getHugeImage()->tilesDown;
	int tilesacross = this->orthoimage->getHugeImage()->tilesAcross;

	double nTiles = tilesdown*tilesacross;

	CImageBuffer orthoBuf(0,0,tilewidth, tileheight, this->nBands, this->bpp, true);

	unsigned char  *tilebufferUchr = 0;
	unsigned short *tilebufferShrt = 0;

	if (this->bpp == 8)  tilebufferUchr = orthoBuf.getPixUC();
	else tilebufferShrt = orthoBuf.getPixUS(); // (this->bpp == 16)

	double *imgX = new double[tilewidth*tileheight];
	double *imgY = new double[tilewidth*tileheight];

	double anchorFactor = 1.0;

	bool anchor = this->useAnchor;
	if (this->useAnchor)
	{
		double coordFactor = 1.0 / this->inputPyrFactor;
		double maxPix = AnchorWidth;
		double sensorGSD = (double) this->inputimage->getApproxGSD(vdem->getDEM()->getminZ()) * this->inputPyrFactor; // maximum width of a ortho-grid
		if (sensorGSD < FLT_EPSILON) sensorGSD = this->gsd;
		if (this->vdem->getDEM()->getgridX() > sensorGSD)
		{
			double fac = this->vdem->getDEM()->getgridX() / sensorGSD;
			if (fac > maxPix)
			{
				anchorFactor = floor(fac / maxPix) + 1.0;
			}
		}
		else anchor = false;
	}

	for ( int tr = 0; tr < tilesdown; tr++ )
	{
		for ( int tc = 0; tc < tilesacross; tc++ )
		{
			orthoBuf.setOffset(tc * tilewidth, tr * tileheight);

			demPt.x = this->llx + tc * tilewidth * this->gsd; // left-most point of the current tile
			demPt.y = this->ury - tr * tileheight * this->gsd;	

			double xmin, xmax, ymin, ymax;

			bool ok;
			if (anchor)
			{	// for the current tile in orthoimage, find pixel locations (imgX,imgY) & area (xmin,xmax,ymin,ymax) in the source image
				ok = computeImgCoordsForTile(demPt, imgX, imgY, xmin, xmax, ymin, ymax, anchorFactor); // imgX, imgY, xmin, xmax, ymin, ymax not defined when called
				
#ifdef _DEBUG
				/*
				int imx = tilewidth*tileheight;
				double *imgX1 = new double[imx];
				double *imgY1 = new double[imx];
				ok = computeImgCoordsForTile(demPt, imgX1, imgY1, xmin, xmax, ymin, ymax);
				double distMax = -1.0;

				for (int k = 0; k < imx; ++k)
				{
					if (imgX[k] > 0)
					{
						imgX1[k] -= imgX[k];
						imgY1[k] -= imgY[k];
						double d = sqrt(imgX1[k] * imgX1[k] + imgY1[k] * imgY1[k]);
						if (d > distMax) distMax = d;
					}
				}

				delete[] imgX1;
				delete[] imgY1;
				*/
#endif
			}
			else ok = computeImgCoordsForTile(demPt, imgX, imgY, xmin, xmax, ymin, ymax);
			

			if (ok)
			{
				CImageBuffer buf(long(xmin), long(ymin), long(xmax - xmin), long(ymax - ymin), this->nBands, this->bpp, true);
				ok = this->inputimage->getHugeImage()->getRect(buf, this->inputPyrLevel);

				if (ok)
				{
					if (this->bpp == 8)
					{
						if (this->bicubic) Create8bitTileBicub(tilebufferUchr, imgX, imgY, buf);
						else Create8bitTileBilin(tilebufferUchr, imgX, imgY, buf);
					}
					else
					{
						if (this->bicubic) Create16bitTileBicub(tilebufferShrt, imgX, imgY, buf);
						else Create16bitTileBilin(tilebufferShrt, imgX, imgY, buf);
					}
				}
			};

			if (!ok) orthoBuf.setAll(0);

			this->orthoimage->getHugeImage()->dumpTile(orthoBuf);

			// update value for progress bar
			if ( this->listener != NULL )
			{
				double done = tr * tilesacross + tc + 1;
				double pvalue = 100.0 * done/nTiles;
				this->listener->setProgressValue(pvalue);
			}

		}
	}

	delete [] imgX;
	delete [] imgY;

	if ( this->listener != NULL )
	{
		this->listener->setProgressValue(100.0);
	}

	return this->orthoimage->getHugeImage()->finishWriting();
}

bool COrthoImageGenerator::makeOrthoImage()
{
	//addition >>
	if ( !this->isUTM ) // in Geographic
	{
		CTFW tfw;
		CReferenceSystem refsys;

		C2DPoint shift(this->ury, this->llx);
		C2DPoint gridding(this->gsd,this->gsd);

		refsys.setUTMZone(this->ury, this->llx);
		refsys.setCoordinateType(eGEOGRAPHIC);

		tfw.setRefSys(refsys);
		tfw.setFromShiftAndPixelSize(shift, gridding);	
		this->orthoimage->setTFW(tfw);
	}
	else //in UTM
	{
		/*C2DPoint shift(this->llx,  this->ury);
		C2DPoint gridding(this->gsd,this->gsd);
		refsys.setCoordinateType(eGRID);

		refsys.setUTMZone(this->llx, this->ury);

		tfw.setRefSys(refsys);
		tfw.setFromShiftAndPixelSize(shift, gridding);*/
		Matrix parms;
		parms.ReSize(6,1);

		parms.element(0,0) = this->gsd;
		parms.element(1,0) = 0.0;
		parms.element(2,0) = this->llx;
		parms.element(3,0) = 0.0;
		parms.element(4,0) = -1.0*this->gsd;
		parms.element(5,0) = this->ury;


		CTFW tfw (*this->orthoimage->getTFW());
		tfw.setAll(parms);
		tfw.setRefSys(this->vdem->getReferenceSystem());
		this->orthoimage->setTFW(tfw);
	}	
	
	//tfw.setFileName("ortho.tfw");

	//this->orthoimage->setTFW(tfw);
	// <<addition

	if (!this->CreateOrthoImage()) return false;

	/*Matrix parms;
	parms.ReSize(6,1);

	parms.element(0,0) = this->gsd;
	parms.element(1,0) = 0.0;
	parms.element(2,0) = this->llx;
	parms.element(3,0) = 0.0;
	parms.element(4,0) = -1.0*this->gsd;
	parms.element(5,0) = this->ury;

	CTFW tfw (*this->orthoimage->getTFW());
	tfw.setAll(parms);
	tfw.setRefSys(this->vdem->getReferenceSystem());
	this->orthoimage->setTFW(tfw);*/

	this->orthoimage->getHugeImage()->resetTiles();
	this->orthoimage->getHugeImage()->closeHugeFile();

	return true;
}

