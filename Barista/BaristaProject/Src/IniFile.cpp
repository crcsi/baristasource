#include "IniFile.h"
#include "SystemUtilities.h"


CIniFile::CIniFile(): 	Ellipsoid("WGS 1984"), isUTM(true), isWGS84(true), coordinatetype(-1),
					satelliteHeights(NUMBER_OF_SATELLITES),groundHeights(NUMBER_OF_SATELLITES),
					groundTemperatures(NUMBER_OF_SATELLITES),rmsPath(NUMBER_OF_SATELLITES),
					rmsAttitudes(NUMBER_OF_SATELLITES),satellite(0),dem_nStatisticRows(-1),
					dem_nStatisticCols(-1),dem_showAvg(true),dem_showDevAvg(true),
					dem_showMin(true),dem_showMax(true),dem_showDev(false),dem_statTextColor(3),
					dem_statLineColor(3),dem_statFontSize(10),dem_nLegendeClasses(10),widthObjectSelector(-1),
					heightObjectSelector(-1), registrationCrossCorr(0.8f), registrationParallax (100),
					registrationSigma0(1.0), registrationAddPoints(true),selectedCamera(0),
					inTrackViewAngle(0.0),crossTrackViewAngle(0.0),maxRobustPercentage(0.03),minRobFac(4.0),
					useForwardIntersection(false),excludeTiePoints(false), 
					ALShasStrips(true), ALSFormat(4), ALSPulseMode(2),
					ALSoffsetX(0.0), ALSoffsetY(0.0), ALSdeltaX(1.0), ALSdeltaY(1.0)
{
	CCharString dirName = "Barista";
	CCharString dirDelimStr = "\\";
#ifdef WIN32
	this->currdir = CSystemUtilities::getEnvVar( "APPDATA" );
	dirDelimStr = "\\";
#else
	this->currdir = CSystemUtilities::getEnvVar( "HOME" );
	dirName = "." + dirName;	// hide the directory
	dirDelimStr = "/";
#endif
	// dirDelimStr will not be initialized when this code is executed, use local variable instead
	//this->currdir += CSystemUtilities::dirDelimStr + dirName;
	this->currdir += dirDelimStr + dirName;
	this->ininame =   this->currdir;
	//this->ininame +=  CSystemUtilities::dirDelimStr + "Barista.ini";
	this->ininame += dirDelimStr + "Barista.ini";

	CSystemUtilities::mkdir(this->currdir);


	this->zone = -1;
	this->hemisphere = -1;
	this->centralMeridian = -1;
	this->latitudeOrigin = -1;
	this->scale = 1.0;
	this->easting = 0.0;
	this->northing = 0.0;
	this->OrthoRes = 1.0;
	OrthoMinX = 0.0;
	OrthoMinY = 0.0;
	OrthoMaxX = 0.0;
	OrthoMaxY = 0.0;
	OrthoDEM = "DEM";

//revised by shijie liu 07/01/2010
//begin 1
/*
	this->satelliteHeights[0] = -1;
	this->satelliteHeights[1] = -1;
	this->satelliteHeights[2] = -1;
	this->satelliteHeights[3] = -1;

	this->groundHeights[1] = -100;
	this->groundHeights[2] = -100;
	this->groundHeights[3] = -100;

	this->groundTemperatures[0] = -200;
	this->groundTemperatures[1] = -200;
	this->groundTemperatures[2] = -200;
	this->groundTemperatures[3] = -200;

	this->rmsPath[0] = -1.0;
	this->rmsPath[1] = -1.0;
	this->rmsPath[2] = -1.0;
	this->rmsPath[3] = -1.0;

	this->rmsAttitudes[0] = -1.0;
	this->rmsAttitudes[1] = -1.0;
	this->rmsAttitudes[2] = -1.0;
	this->rmsAttitudes[3] = -1.0;
*/

	for(int i=0;i<NUMBER_OF_SATELLITES;i++)
	{
		this->satelliteHeights[i]=-1;
		this->satelliteHeights[i]=-100;
		this->groundTemperatures[i] = -200;
		this->rmsPath[i] = -1.0;
		this->rmsAttitudes[i] = -1.0;
	}

//end 1
//////////////////////////////////////////////////////////////////////////
}

CIniFile::~CIniFile(void)
{
}

void CIniFile::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("currdir", this->currdir);
	token = pStructuredFileSection->addToken();
	token->setValue("zone", this->zone);
	token = pStructuredFileSection->addToken();
	token->setValue("hemisphere", this->hemisphere);
	token = pStructuredFileSection->addToken();
	token->setValue("centralmeridian", this->centralMeridian);
	token = pStructuredFileSection->addToken();
	token->setValue("latitudeorigin", this->latitudeOrigin);
	token = pStructuredFileSection->addToken();
	token->setValue("loadRPC", this->loadRPC);
	token = pStructuredFileSection->addToken();
	token->setValue("loadTFW", this->loadTFW);
	token = pStructuredFileSection->addToken();
	token->setValue("rememberloadSettings", this->rememberloadSettings);
	token = pStructuredFileSection->addToken();
	token->setValue("askloadSettings", this->askloadSettings);
	token = pStructuredFileSection->addToken();
	token->setValue("TMScale", this->scale);
	token = pStructuredFileSection->addToken();
	token->setValue("TMEasting", this->easting);
	token = pStructuredFileSection->addToken();
	token->setValue("TMNorthing", this->northing);
	token = pStructuredFileSection->addToken();
	token->setValue("isUTM", this->isUTM);
	token = pStructuredFileSection->addToken();
	token->setValue("isWGS84", this->isWGS84);
	token = pStructuredFileSection->addToken();
	token->setValue("ellipsoid", this->Ellipsoid);
	token = pStructuredFileSection->addToken();
	token->setValue("coordtyp", this->coordinatetype);
	token = pStructuredFileSection->addToken();
	token->setValue("orthores", this->OrthoRes);
	token = pStructuredFileSection->addToken();
	token->setValue("orthominx", this->OrthoMinX);
	token = pStructuredFileSection->addToken();
	token->setValue("orthominy", this->OrthoMinY);
	token = pStructuredFileSection->addToken();
	token->setValue("orthomaxx", this->OrthoMaxX);
	token = pStructuredFileSection->addToken();
	token->setValue("orthomaxy", this->OrthoMaxY);
	token = pStructuredFileSection->addToken();
	token->setValue("orthodem", this->OrthoDEM);
	token = pStructuredFileSection->addToken();
	token->setValue("satellite", this->satellite);
	token = pStructuredFileSection->addToken();
	token->setValue("widthObjectSelector", this->widthObjectSelector);
	token = pStructuredFileSection->addToken();
	token->setValue("heightObjectSelector", this->heightObjectSelector);
	token = pStructuredFileSection->addToken();
	token->setValue("selectedCamera", this->selectedCamera);
	token = pStructuredFileSection->addToken();
	token->setValue("inTrackViewAngle", this->inTrackViewAngle);
	token = pStructuredFileSection->addToken();
	token->setValue("crossTrackViewAngle", this->crossTrackViewAngle);

	token = pStructuredFileSection->addToken();
	token->setValue("useForwardIntersection", this->useForwardIntersection);
	token = pStructuredFileSection->addToken();
	token->setValue("excludeTiePoints", this->excludeTiePoints);
	token = pStructuredFileSection->addToken();
	token->setValue("maxRobustPercentage", this->maxRobustPercentage);
	token = pStructuredFileSection->addToken();
	token->setValue("minRobFac", this->minRobFac);


	token = pStructuredFileSection->addToken();
    token->setValue("number of satellites", NUMBER_OF_SATELLITES);

	CCharString valueStr;
	CCharString elementStr;

	// satellite heights
	token = pStructuredFileSection->addToken();
	valueStr = "";

	for (int i = 0; i < this->satelliteHeights.size(); i++)
		{
			double element = this->satelliteHeights[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	token->setValue("satellite heights", valueStr.GetChar());

	// ground heights
	token = pStructuredFileSection->addToken();
	valueStr = "";

	for (int i = 0; i < this->satelliteHeights.size(); i++)
		{
			double element = this->groundHeights[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	token->setValue("ground heights", valueStr.GetChar());

	// ground temperatures
	token = pStructuredFileSection->addToken();
	valueStr = "";

	for (int i = 0; i < this->satelliteHeights.size(); i++)
		{
			double element = this->groundTemperatures[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	token->setValue("ground temperatures", valueStr.GetChar());

	// rms path
	token = pStructuredFileSection->addToken();
	valueStr = "";

	for (int i = 0; i < this->rmsPath.size(); i++)
		{
			double element = this->rmsPath[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	token->setValue("rmsPath", valueStr.GetChar());


	// rms Attitudes
	token = pStructuredFileSection->addToken();
	valueStr = "";

	for (int i = 0; i < this->rmsAttitudes.size(); i++)
		{
			double element = this->rmsAttitudes[i];
			elementStr.Format("%.13e ", element);
			valueStr += elementStr;
		}

	token->setValue("rmsAttitudes", valueStr.GetChar());


	token = pStructuredFileSection->addToken();
	token->setValue("ALShasStrips", this->ALShasStrips);
	token = pStructuredFileSection->addToken();
	token->setValue("ALSFormat", this->ALSFormat);
	token = pStructuredFileSection->addToken();
	token->setValue("ALSPulseMode", this->ALSPulseMode);
	token = pStructuredFileSection->addToken();
	token->setValue("ALSoffsetX", this->ALSoffsetX);
	token = pStructuredFileSection->addToken();
	token->setValue("ALSoffsetY", this->ALSoffsetY);
	token = pStructuredFileSection->addToken();
	token->setValue("ALSdeltaX", this->ALSdeltaX);
	token = pStructuredFileSection->addToken();
	token->setValue("ALSdeltaY", this->ALSdeltaY);
}

void CIniFile::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	bool readSatlelliteData = false;

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("currdir"))
            this->currdir = token->getValue();
		else if(key.CompareNoCase("zone"))
			this->zone = token->getIntValue();
		else if(key.CompareNoCase("hemisphere"))
			this->hemisphere = token->getIntValue();
		else if(key.CompareNoCase("centralmeridian"))
			this->centralMeridian = token->getDoubleValue();
		else if(key.CompareNoCase("latitudeorigin"))
			this->latitudeOrigin = token->getDoubleValue();
		else if(key.CompareNoCase("loadRPC"))
			this->loadRPC = token->getBoolValue();
		else if(key.CompareNoCase("loadTFW"))
			this->loadTFW = token->getBoolValue();
		else if(key.CompareNoCase("rememberloadSettings"))
			this->rememberloadSettings = token->getBoolValue();
		else if(key.CompareNoCase("askloadSettings"))
			this->askloadSettings = token->getBoolValue();
		else if(key.CompareNoCase("TMScale"))
			this->scale = token->getDoubleValue();
		else if(key.CompareNoCase("TMEasting"))
			this->easting = token->getDoubleValue();
		else if(key.CompareNoCase("TMNorthing"))
			this->northing = token->getDoubleValue();
		else if(key.CompareNoCase("isUTM"))
			this->isUTM = token->getBoolValue();
		else if(key.CompareNoCase("isWGS84"))
			this->isWGS84 = token->getBoolValue();
		else if(key.CompareNoCase("ellipsoid"))
			this->Ellipsoid = token->getValue();
		else if(key.CompareNoCase("coordtyp"))
			this->coordinatetype = token->getIntValue();
		else if(key.CompareNoCase("orthores"))
			this->OrthoRes = token->getDoubleValue();
		else if(key.CompareNoCase("orthominx"))
			this->OrthoMinX = token->getDoubleValue();
		else if(key.CompareNoCase("orthominy"))
			this->OrthoMinY = token->getDoubleValue();
		else if(key.CompareNoCase("orthomaxx"))
			this->OrthoMaxX = token->getDoubleValue();
		else if(key.CompareNoCase("orthomaxy"))
			this->OrthoMaxY = token->getDoubleValue();
		else if(key.CompareNoCase("orthodem"))
			this->OrthoDEM = token->getValue();
		else if(key.CompareNoCase("orthodem"))
			this->OrthoDEM = token->getValue();
		else if(key.CompareNoCase("orthodem"))
			this->OrthoDEM = token->getValue();
		else if(key.CompareNoCase("satellite"))
			this->satellite = token->getIntValue();
		else if(key.CompareNoCase("widthObjectSelector"))
			this->widthObjectSelector = token->getIntValue();
		else if(key.CompareNoCase("heightObjectSelector"))
			this->heightObjectSelector = token->getIntValue();
		else if(key.CompareNoCase("selectedCamera"))
			this->selectedCamera = token->getIntValue();
		else if(key.CompareNoCase("inTrackViewAngle"))
			this->inTrackViewAngle = token->getDoubleValue();
		else if(key.CompareNoCase("crossTrackViewAngle"))
			this->crossTrackViewAngle = token->getDoubleValue();
		else if(key.CompareNoCase("maxRobustPercentage"))
			this->maxRobustPercentage = token->getDoubleValue();
		else if(key.CompareNoCase("minRobFac"))
			this->minRobFac = token->getDoubleValue();
		else if(key.CompareNoCase("useForwardIntersection"))
			this->useForwardIntersection = token->getBoolValue();
		else if(key.CompareNoCase("excludeTiePoints"))
			this->excludeTiePoints = token->getBoolValue();
		else if (token->isKey("number of satellites"))
		{
			if (token->getIntValue() == this->satelliteHeights.size() &&
				token->getIntValue() == this->groundHeights.size() &&
				token->getIntValue() == this->groundTemperatures.size()&&
				token->getIntValue() == this->rmsPath.size()&&
				token->getIntValue() == this->rmsAttitudes.size())
			readSatlelliteData = true;
		}
		else if (token->isKey("satellite heights") && readSatlelliteData)
		{
				token->getDoublesClass(this->satelliteHeights);	
		}
		else if (token->isKey("ground heights") && readSatlelliteData)
		{
				token->getDoublesClass(this->groundHeights);	
		}
		else if (token->isKey("ground temperatures") && readSatlelliteData)
		{
				token->getDoublesClass(this->groundTemperatures);	
		}
		else if (token->isKey("rmsPath") && readSatlelliteData)
		{
				token->getDoublesClass(this->rmsPath);	
		}
		else if (token->isKey("rmsAttitudes") && readSatlelliteData)
		{
				token->getDoublesClass(this->rmsAttitudes);	
		}
		else if(key.CompareNoCase("ALShasStrips"))
			this->ALShasStrips = token->getBoolValue();
		else if(key.CompareNoCase("ALSFormat"))
			this->ALSFormat = token->getIntValue();
		else if(key.CompareNoCase("ALSPulseMode"))
			this->ALSPulseMode = token->getIntValue();
		else if(key.CompareNoCase("ALSoffsetX"))
			this->ALSoffsetX = token->getDoubleValue();
		else if(key.CompareNoCase("ALSoffsetY"))
			this->ALSoffsetY = token->getDoubleValue();
		else if(key.CompareNoCase("ALSdeltaX"))
			this->ALSdeltaX = token->getDoubleValue();
		else if(key.CompareNoCase("ALSdeltaY"))
			this->ALSdeltaY = token->getDoubleValue();
    }
}

void CIniFile::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

void CIniFile::resetPointers()
{
    CSerializable::resetPointers();
}


bool CIniFile::isModified()
{
    return true;
}

void CIniFile::writeIniFile()
{
	CStructuredFile structuredFile;
	CStructuredFileSection* pMainSection = structuredFile.getMainSection();

	this->serializeStore(pMainSection);

    structuredFile.writeFile(this->ininame);
}

void CIniFile::readIniFile()
{
	CStructuredFile structuredFile;
	CStructuredFileSection* pMainSection = structuredFile.getMainSection();

	structuredFile.readFile(this->ininame);
	this->serializeRestore(pMainSection);

	this->reconnectPointers();
	this->resetPointers();
}

void CIniFile::setCurrDir(const char* dir)
{
    CCharString withslash = dir;
    withslash += CSystemUtilities::dirDelimStr;
    //this->currdir = dir;
    this->currdir = withslash.GetChar();
}
const char *CIniFile::getEllipsoidName() const 
{ 
	return this->Ellipsoid.GetChar(); 
};
	
void CIniFile::setEllipsoidName(const char *ell)
{
	this->Ellipsoid = ell;
	if (!this->Ellipsoid.CompareNoCase("WGS 1984")) this->isWGS84 = false;
};


bool CIniFile::getIsUTM() const
{
	return this->isUTM;
};

void CIniFile::setIsUTM(bool utm)
{
	this->isUTM = utm;
};


bool CIniFile::getIsWGS84() const
{
	return this->isWGS84;
};

void CIniFile::setIsWGS84(bool wgs)
{
	this->isWGS84 = wgs; 
};

int CIniFile::getCoordinateType() const
{
	return this->coordinatetype;
};

void CIniFile::setCoordinateType(int type)
{
	this->coordinatetype = type;
};

const char* CIniFile::getCurrDir()
{
    return this->currdir.GetChar();
}

const char* CIniFile::getIniFileName()
{
    return this->ininame.GetChar();
}

int CIniFile::getZone()
{
    return this->zone;
}

void CIniFile::setZone(int zone)
{
    this->zone = zone;
}

int CIniFile::getHemisphere()
{
   return this->hemisphere;
}

void CIniFile::setHemisphere(int i)
{
    this->hemisphere = i;
}

double CIniFile::getCentralMeridian()
{
	return this->centralMeridian;
}

void CIniFile::setCentralMeridian(double longi)
{
	this->centralMeridian = longi;
}

double CIniFile::getLatitudeOrigin()
{
	return this->latitudeOrigin;
}

void CIniFile::setLatitudeOrigin(double lati)
{
	this->latitudeOrigin = lati;
}


bool CIniFile::getloadRPC()
{
	return this->loadRPC;
}

void CIniFile::setloadRPC(bool loadRPC)
{
	this->loadRPC = loadRPC;
}

bool CIniFile::getloadTFW()
{
	return this->loadTFW;
}

void CIniFile::setloadTFW(bool loadTFW)
{
	this->loadTFW = loadTFW;
}

bool CIniFile::getrememberloadSettings()
{
	return this->rememberloadSettings;
}

void CIniFile::setrememberloadSettings(bool rememberloadSettings)
{
	this->rememberloadSettings = rememberloadSettings;
}

bool CIniFile::getaskloadSettings()
{
	return this->askloadSettings;
}

void CIniFile::setaskloadSettings(bool askloadSettings)
{
	this->askloadSettings = askloadSettings;
}


double CIniFile::getTMScale()
{
	return this->scale;
}

void CIniFile::setTMScale(double scale)
{
	this->scale = scale;
}
double CIniFile::getTMEasting()
{
	return this->easting;
}

void CIniFile::setTMEasting(double easting)
{
	this->easting =easting;
}

double CIniFile::getTMNorthing()
{
	return this->northing;
}

void CIniFile::setTMNorthing(double northing)
{
	this->northing = northing;
}

double CIniFile::getOrthoRes()
{
	return this->OrthoRes;
};

void CIniFile::setOrthoRes(double orthoRes)
{
	this->OrthoRes = orthoRes;
};

double CIniFile::getOrthoMinX()
{
	return this->OrthoMinX;	
};

void CIniFile::setOrthoMinX(double orthoMinX)
{
	this->OrthoMinX = orthoMinX;
};

double CIniFile::getOrthoMinY()
{
	return this->OrthoMinY;
	
};

void CIniFile::setOrthoMinY(double orthoMinY)
{
	this->OrthoMinY = orthoMinY;
};

double CIniFile::getOrthoMaxX()
{
	return this->OrthoMaxX;
};

void CIniFile::setOrthoMaxX(double orthoMaxX)
{
	this->OrthoMaxX = orthoMaxX; 
};

double CIniFile::getOrthoMaxY()
{
	return this->OrthoMaxY;
};

void CIniFile::setOrthoMaxY(double orthoMaxY)
{
	this->OrthoMaxY = orthoMaxY;
};

CCharString CIniFile::getOrthoDEM()
{
	return this->OrthoDEM;
};

void CIniFile::setOrthoDEM(const CCharString &orthoDEM)
{
	this->OrthoDEM = orthoDEM;
};

void CIniFile::setDEMStatistics(int nRows,
								int nCols,
								bool showAvg,
								bool showDevAvg,
								bool showMin,
								bool showMax,
								bool showDev,
								vector<int> statTextColor,
								vector<int> statLineColor,
								int statFontSize,
								int nLegendeClasses)
{

	this->dem_nStatisticRows =nRows;
	this->dem_nStatisticCols =nCols;
	this->dem_showAvg = showAvg;
	this->dem_showDevAvg= showDevAvg;
	this->dem_showMin= showMin;
	this->dem_showMax= showMax;
	this->dem_showDev= showDev;
	this->dem_statTextColor= statTextColor;
	this->dem_statLineColor= statLineColor;
	this->dem_statFontSize = statFontSize;
	this->dem_nLegendeClasses = nLegendeClasses;
}

void CIniFile::setAdjustmentSettings(bool useForwardIntersection,bool excludeTiePoints,double maxRobustPercentage,double minRobFac)
{
	this->useForwardIntersection = useForwardIntersection;
	this->excludeTiePoints = excludeTiePoints;
	this->maxRobustPercentage = maxRobustPercentage;
	this->minRobFac = minRobFac;
}

void CIniFile::getAdjustmentSettings(bool& useForwardIntersection,bool& excludeTiePoints,double& maxRobustPercentage,double& minRobFac)
{
	useForwardIntersection = this->useForwardIntersection;
	excludeTiePoints = this->excludeTiePoints;
	maxRobustPercentage = this->maxRobustPercentage;
	minRobFac = this->minRobFac;
}



CIniFile inifile;