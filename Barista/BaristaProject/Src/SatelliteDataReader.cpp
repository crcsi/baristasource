#include "SatelliteDataReader.h"

#include "BundleHelper.h"
#include "OrbitObservations.h"
#include "ObsPointArray.h"
#include "doubles.h"
#include "BaristaProject.h"
#include "ProtocolHandler.h"


CSatelliteDataReader::CSatelliteDataReader() : orbitPoints(0),attitudes(0), 
	camera(0),cameraMounting(0),
	currentRMSPath(-1.0),currentRMSAttitudes(-1.0),aimedRMSPath(-1.0),
	aimedRMSAttitudes(-1.0),errorMessage(""),nPathSegments(1),
	oldAimedRMSPath(-1.0),oldAimedRMSAttitudes(-1.0),
	nAttitudeSegments(1),firstLineTime(0.0),timePerLine(0.0)
{
}

void CSatelliteDataReader::initReader(COrbitObservations* path,COrbitObservations* attitudes,CCameraMounting* mounting,CCamera* camera)
{
	this->orbitPoints = path;
	this->attitudes = attitudes;
	this->cameraMounting = mounting;
	this->camera = camera;
}

bool CSatelliteDataReader::determineOrbitPointsInInterval(COrbitObservations* orbitObs, double startTime,double endTime)
{
	// get the observations
	CObsPointArray* points = orbitObs->getObsPoints();
	int size = points->GetSize();

	int indexStart;
	int indexEnd;

	// determine index of last point before interval
	for (indexStart=0; indexStart < size; indexStart++)
	{
		if (points->GetAt(indexStart)->dot >= startTime)
			break;
	}
	
	if (indexStart > 0)
		indexStart--; // that's the last point before interval


	if (indexStart < 0 || indexStart > points->GetSize())
	{
		this->errorMessage = "Not enough orbit points found!";
		return false;
	}

	for (indexEnd=indexStart; indexEnd < size; indexEnd++)
	{
		if (points->GetAt(indexEnd)->dot >= endTime)
			break;
	}

	
	if (indexEnd == size) 
		indexEnd--;  // stay in the array!

	indexEnd; // that's the first point after interval, or the last point in the array


	if (indexEnd - indexStart < 2)
	{
		this->errorMessage = "Not enough orbit points found!";
		return false;
	}

	orbitObs->setFirstPointIndex(indexStart);
	orbitObs->setLastPointIndex(indexEnd);

	return true;
}


void CSatelliteDataReader::computeInterval(double &startTime, double& endTime)
{
	double timeOffset = this->satInfo.nRows * PERCENTAGE_OVERLAP / 100.0 * this->timePerLine;
	startTime = this->firstLineTime - timeOffset;
	endTime = this->lastLineTime + timeOffset;
}

bool CSatelliteDataReader::adjustOrbit(bool adjustPath,bool adjustAttitudes,double startTime, double endTime)
{
	int splineDegreePath=3;
	int splineDegreeAtt=3;
	return adjustOrbit(adjustPath,adjustAttitudes,startTime,endTime,splineDegreePath,splineDegreeAtt);
}

bool CSatelliteDataReader::adjustOrbit(bool adjustPath,bool adjustAttitudes,double startTime, double endTime,int splineDegreePath,int splineDegreeAtt)
{
	double listenerStartValue = 0.0;
	double listenerEndValue = this->listenerMaxValue;
	if (this->listener)
	{
		listenerStartValue = this->listener->getProgress();
		double max = listenerStartValue + 0.1*( listenerEndValue- listenerStartValue); 
		this->setMaxListenerValue(max);
	}

	if (adjustPath)
	{
		
		if (!this->prepareEphemeris())
			return false;

		if (!this->determineOrbitPointsInInterval(this->orbitPoints,startTime,endTime))
			return false;

		if (this->listener)
		{
			double max = listenerStartValue + 0.3*(listenerEndValue - listenerStartValue); 
			this->setMaxListenerValue(max);
			this->listener->setTitleString("Adjusting path spline ...");
		}

		if (!this->adjustPath(splineDegreePath))
			return false;

		if (this->listener)
		{
			double max = listenerStartValue + 0.5*(listenerEndValue - listenerStartValue); 
			this->setMaxListenerValue(max);
		}
	}

	if (adjustAttitudes)
	{
		if (!this->prepareAttitudes())
			return false;

		if (!this->determineOrbitPointsInInterval(this->attitudes,startTime,endTime))
			return false;	

		if (this->listener)
		{
			double max = listenerStartValue + 0.9*(listenerEndValue - listenerStartValue); 
			this->setMaxListenerValue(max);
			this->listener->setTitleString("Adjusting Attitude spline ...");
		}

		if (!this->adjustAttitude(splineDegreeAtt))
			return false;

		if (this->listener)
		{
			double max = listenerStartValue + 1.0*(listenerEndValue - listenerStartValue); 
			this->setMaxListenerValue(max);
		}

		if (!this->prepareMounting()) // calculate camera mounting
			return false;
	}

	return true;
}


bool CSatelliteDataReader::recomputeOrbit(bool adjustPath,bool adjustAttitudes)
{

	if (this->listener)
	{
		this->listener->setProgressValue(0.0);
		this->setMaxListenerValue(100.0);
	}

	double startTime,endTime;
	this->computeInterval(startTime,endTime);

	// no need to recompute, the new level is stil worse then the current acieved one
	if (adjustPath && (this->currentRMSPath > 0.0 && this->currentRMSPath < this->aimedRMSPath))
		adjustPath = false;

	if (adjustAttitudes && ( this->currentRMSAttitudes > 0.0 && this->currentRMSAttitudes < this->aimedRMSAttitudes))
		adjustAttitudes =false;

	if (!adjustPath && !adjustAttitudes)
		return true;

	int oldNPathSegments = this->nPathSegments;
	int oldNAttitudeSegments = this->nAttitudeSegments;


	//start already one level higher or lower 
	if (this->oldAimedRMSPath > this->aimedRMSPath) // increase accuracy
		this->nPathSegments *= 2;
	else if (this->oldAimedRMSPath < this->aimedRMSPath) // decrease accuracy
		this->nPathSegments /= 2;

	if (this->oldAimedRMSAttitudes > this->aimedRMSAttitudes) // increase accuracy
		this->nAttitudeSegments *= 2;  
	else if (this->oldAimedRMSAttitudes < this->aimedRMSAttitudes) // decrease accuracy
		this->nAttitudeSegments /= 2;

	bool error = false;

	if (this->nPathSegments == 0 || this->nAttitudeSegments == 0)
		error = true;

	bool driveBack = false;
	// recompute the orbit with the new thresholds
	if (!error)
		driveBack = !this->adjustOrbit(adjustPath,adjustAttitudes,startTime,endTime);
	
	if (error || driveBack)// if something goes wrong, drive back to the old configuration
	{
		// restore old number of segments
		this->nPathSegments = oldNPathSegments;
		this->nAttitudeSegments = oldNAttitudeSegments;

		// restore the old accuracy level
		this->aimedRMSPath = this->oldAimedRMSPath;
		this->aimedRMSAttitudes = this->oldAimedRMSAttitudes;

		// if this also doesn't work, clear the rms values
		if (driveBack && !this->adjustOrbit(adjustPath,adjustAttitudes,startTime,endTime)) 
		{
			this->currentRMSAttitudes = -1.0;
			this->currentRMSPath = -1.0;
		}
		
		// tell the user anyway
		return false;
	}
	return true;



}

bool CSatelliteDataReader::adjustPath()
{
	//if no splineDegree set,then set it to 3
	int splineDegreePath=3;
	return adjustPath(splineDegreePath);
}


bool CSatelliteDataReader::adjustPath(int splineDegree)
{
	int iterCount =0;

	for (int i=0; i < MAX_ITER; i++)
	{

		// get the observations
		CXYZOrbitPointArray* oa = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();
		int size = this->orbitPoints->getLastPointIndex() - this->orbitPoints->getFirstPointIndex();

		// the intervall between two knots
		int length = size / this->nPathSegments;

		// fill the knot vector
		doubles knots;
		knots.resize(this->nPathSegments + 1);

		for (int i=0; i < this->nPathSegments; i++)
			knots[i] = oa->GetAt(this->orbitPoints->getFirstPointIndex() + i*length)->dot;
		knots[this->nPathSegments] = oa->GetAt(this->orbitPoints->getLastPointIndex())->dot;
		

		// start with degree 3
		//double splineDegree = 2;
		

		this->orbitPoints->setSplineModelParameter(splineDegree,knots);

		CBaristaBundleHelper helper;
#ifndef FINAL_RELEASE
		CCharString path = protHandler.getDefaultDirectory();
		helper.initProtocol(path + "protEphemeriesPath.txt");
#endif
		helper.bundleInit((CObservationHandler*)this->orbitPoints,false);
		
		if (!helper.solve(false, false, false))
		{
			this->errorMessage = "Adjustment of the orbit path failed!";
			helper.closeProtocol();
			return false;
		}
		if (helper.getRMS() < this->aimedRMSPath)
		{
			this->setAchievedRMSPath(helper.getRMS());
			helper.printResults();
			helper.closeProtocol();
			return true;
		}
		else
			this->nPathSegments *=2;

		helper.closeProtocol();
	}
	this->errorMessage = "Accuracy level could not be achieved! Decrease Path accuracy level!";	
	return false;
}

bool CSatelliteDataReader::adjustAttitude()
{
	//if no splineDegree set,then set it to 3
	int splineDegreeAtt=3;
	return adjustAttitude(splineDegreeAtt);
}

bool CSatelliteDataReader::adjustAttitude(int splineDegree)
{
	int iterCount =0;

	for (int i=0; i < MAX_ITER; i++)
	{

		// get the observations
		CXYZOrbitPointArray* newRPYAngles = (CXYZOrbitPointArray*)this->attitudes->getObsPoints();
		int size = this->attitudes->getLastPointIndex() - this->attitudes->getFirstPointIndex();
		
		// the intervall between two knots
		int length = size / this->nAttitudeSegments;
		
		// fill the knot vector
		doubles knots;
		knots.resize(this->nAttitudeSegments+1);

		for (int i=0; i < this->nAttitudeSegments; i++)
			knots[i] = newRPYAngles->GetAt(this->attitudes->getFirstPointIndex() + i*length)->dot;
		knots[this->nAttitudeSegments] = newRPYAngles->GetAt(this->attitudes->getLastPointIndex())->dot;

		//double splineDegree = 2;
		this->attitudes->setSplineModelParameter(splineDegree,knots);
		
		//	compute path first
		CBaristaBundleHelper helper;

#ifndef FINAL_RELEASE
		CCharString path = protHandler.getDefaultDirectory();
		helper.initProtocol(path + "protEphemeriesAttitudes.txt");
#endif

		helper.bundleInit(this->attitudes,false);

		if (!helper.solve(false, false,false))
		{
			this->errorMessage = "Adjustment of orbit Attitudes failed!";
			helper.closeProtocol();
			return false;
		}
		if (helper.getRMS() < this->aimedRMSAttitudes)
		{		
			this->setAchievedRMSAttitudes(helper.getRMS());
			helper.printResults();
			helper.closeProtocol();
			return true;
		}
		else
			this->nAttitudeSegments *=2;
		helper.closeProtocol();
	}	

	this->errorMessage = "Accuracy level could not be achieved! Decrease Attitude accuracy level!";
	return false;	
	
}


bool CSatelliteDataReader::adjustPath(COrbitObservations* newPath)
{
	if (newPath)
	{
		this->setAimedRMSPath(0.5);
		this->orbitPoints = newPath;
		return this->adjustPath();
	}
	else
		return false;
}

bool CSatelliteDataReader::adjustAttitude(COrbitObservations* newAttitudes)
{
	if (newAttitudes)
	{
		this->setAimedRMSAttitudes(0.5);
		this->attitudes = newAttitudes;
		return this->adjustAttitude();
	}
	else
		return false;
}

void CSatelliteDataReader::setDirectObservations(COrbitPathModel* path,COrbitAttitudeModel* attitudes) const
{
	// do nothing by default, we leave the parameters as they are initialised by the sensor models
}