#include <float.h> 

#include "GCPMatcher.h"
#include "newmatap.h"

#include "VDEM.h"
#include "VImage.h"
#include "BaristaLSM.h"
#include "BaristaProject.h"
#include "XYZPointArray.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"
#include "Trans3DFactory.h"
#include "PushbroomScanner.h"
#include "m_FeatExtract.h"
#include "WallisFilter.h"

//================================================================================

CGCPMatcher::CGCPMatcher(CXYZPointArray *GCPs, const CMatchingParameters &pars) : 
                         pOrthoPhoto(0), pGCPs(GCPs), success(false),
						 averageShift(0.0,0.0), matchingPars(pars), isStrip(false)
{
	this->setGCPs(GCPs);
}

//================================================================================

CGCPMatcher::CGCPMatcher(const vector<CVImage *> &ImageVector, CVDEM *DEM, 
						 int NScenesBridged, float sceneSizePercentage, 
						 const CMatchingParameters &pars, 
						 const FeatureExtractionParameters &ExtractionPars) : 
                         pOrthoPhoto(0), pGCPs(0), success(false),
						 averageShift(0.0,0.0), matchingPars(pars), pDEM(0),
						 isStrip(false)
{
	this->setFeatureExtractionPars(ExtractionPars);
	this->setImageVector(ImageVector, DEM, NScenesBridged,sceneSizePercentage);
}

//================================================================================

CGCPMatcher::CGCPMatcher(CXYZPointArray *GCPs, const CMatchingParameters &pars, CVImage* orthoImage) : 
                         pOrthoPhoto(0), pGCPs(GCPs), success(false),
						 averageShift(0.0,0.0), matchingPars(pars), isStrip(false)
{
	this->setGCPs(GCPs,orthoImage);
}

//================================================================================
CGCPMatcher::~CGCPMatcher()
{
	this->clearOrthos();
	this->pGCPs       = 0;
}
	  
//================================================================================
	  
void CGCPMatcher::setFeatureExtractionPars(const FeatureExtractionParameters &ExtractionPars)
{
	this->extractionPars = ExtractionPars;

	this->extractionPars.extractPoints  = true;
	this->extractionPars.extractEdges   = false;
	this->extractionPars.extractRegions = false;
};

//================================================================================
	  
void CGCPMatcher::initOrthos(const C2DPoint &pmin, const C2DPoint &pmax, CReferenceSystem *ref)
{
	for (int i = 0; i < project.getNumberOfImages(); ++i)
	{
		C2DPoint objMin, objMax;
		CVImage *img = project.getImageAt(i);
		if (img->hasTFW() && img->getFootPrintMinMax(objMin, objMax, 0, ref))
		{
			if ((pmin.x < objMax.x) && (pmin.y < objMax.y) && (pmax.x > objMin.x) && (pmax.y > objMin.y))
			{
				CVImage *oldimg = 0;
				for (unsigned int j = 0; (j < this->possibleOrthophotos.size()) && (!oldimg); ++ j)
				{
					if (this->possibleOrthophotos[j] == img) oldimg = img;
				}

				if (!oldimg) this->possibleOrthophotos.push_back(img);
			}
		}
	}
};

//================================================================================
	
void CGCPMatcher::setGCPs(CXYZPointArray *GCPs,CVImage* orthoImage)
{
	this->pGCPs = GCPs;
	possibleOrthophotos.clear();
	this->pGCPs->getExtends();

	C2DPoint pmin(this->pGCPs->getMinX(), this->pGCPs->getMinY());
	C2DPoint pmax(this->pGCPs->getMaxX(), this->pGCPs->getMaxY());

	CReferenceSystem *ref = this->pGCPs->getReferenceSystem();

	if (orthoImage)
		this->possibleOrthophotos.push_back(orthoImage);
	else
		this->initOrthos(pmin, pmax, ref);

	if (this->isInitialised()) this->setCurrentOrthoImage(0);
}

//================================================================================

void CGCPMatcher::setImageVector(const vector<CVImage *> &ImageVector, 
								 CVDEM *DEM,
								 int NScenesBridged,
								 float sceneSizePercentage)
{
	this->imageVector = ImageVector;
	this->setDEM(DEM, NScenesBridged, sceneSizePercentage);
};

//================================================================================

void CGCPMatcher::setDEM(CVDEM *DEM, int NScenesBridged, float sceneSizePercentage)
{
	this->clearOrthos();

	this->pDEM = DEM;

	CReferenceSystem ref;
	if (this->pDEM) ref = this->pDEM->getReferenceSystem();

	int nImgs = 0;
	for (unsigned int i = 0; i < this->imageVector.size(); ++i) 
	{
		if (this->imageVector[i]->getCurrentSensorModel())
		{
			this->imageVector[nImgs] = this->imageVector[i];
			++nImgs;
		}
	}
	this->imageVector.resize(nImgs);

	if (this->imageVector.size() < 1)
	{
		this->isStrip = false;
	}
	else
	{
		this->isStrip = checkStrip();
		if (!this->isStrip)
		{
			this->footprintVector.resize(this->imageVector.size());
			for (unsigned int i = 0; i < this->footprintVector.size(); ++i) 
			{
				vector<C2DPoint> Footprint(4);
				this->imageVector[i]->getFootPrint(Footprint[0], Footprint[1], Footprint[2], Footprint[3], 0.0, &ref);
				this->footprintVector[i] = Footprint;
			}
		}
		else
		{
			this->initStripFootprints(NScenesBridged, sceneSizePercentage);
		}

		for (unsigned int i = 0; i < this->footprintVector.size(); ++i)
		{
			vector<C2DPoint> Footprint = this->footprintVector[i];

			C2DPoint pmin = Footprint[0];
			C2DPoint pmax = Footprint[0];
			for (unsigned int j = 1; j < 4; ++j)
			{
				if (Footprint[j].x < pmin.x) pmin.x = Footprint[j].x;
				if (Footprint[j].x > pmax.x) pmax.x = Footprint[j].x;
				if (Footprint[j].y < pmin.y) pmin.y = Footprint[j].y;
				if (Footprint[j].y > pmax.y) pmax.y = Footprint[j].y;
			}

			this->initOrthos(pmin, pmax, &ref);
		}
	}
};
	 	  
//================================================================================
 
void CGCPMatcher::getFootPrint(CVImage *img, C2DPoint &p0, C2DPoint &p1, C2DPoint &p2, C2DPoint &p3, CReferenceSystem *ref)
{
	vector<vector<CVImage*> > ImageGroups;
	vector<CCharString> names;

	int groupIndex = -1;
	if (project.findImageGroups(ImageGroups, names, 2))
	{
		for (unsigned int i = 0; (i < ImageGroups.size()) && (groupIndex < 0); ++i)
		{
			for (unsigned int j = 0; (j < ImageGroups[i].size()) && (groupIndex < 0); ++j)
			{
				if ((ImageGroups[i])[j] == img) groupIndex = i;
			}
		}
	}
	
	if (groupIndex < 0)
	{
		img->getFootPrint(p0, p1, p2, p3, 0.0, ref);
	}
	else
	{
		vector<C2DPoint> p0Vec(ImageGroups[groupIndex].size());
		vector<C2DPoint> p1Vec(ImageGroups[groupIndex].size());
		vector<C2DPoint> p2Vec(ImageGroups[groupIndex].size());
		vector<C2DPoint> p3Vec(ImageGroups[groupIndex].size());
		for (unsigned int j = 0; j < ImageGroups[groupIndex].size(); ++j)
		{
			(ImageGroups[groupIndex])[j]->getFootPrint(p0Vec[j], p1Vec[j], p2Vec[j], p3Vec[j], 0.0, ref);
		}

		p0 = p0Vec[0]; p1 = p1Vec[0]; p2 = p2Vec[0]; p3 = p3Vec[0];
		C2DPoint dir1 = p1 - p0;
		double l1 = dir1.getNorm();
		dir1 /= l1;
		C2DPoint dir2 = p3 - p2;
		double l2 = dir2.getNorm();
		dir2 /= l2;

		for (unsigned int j = 1; j < ImageGroups[groupIndex].size(); ++j)
		{
			if (dir1.innerProduct(p0Vec[j] - p0) < 0.0)
			{
				p0 = p0Vec[j];
				dir1 = p1 - p0;
				l1 = dir1.getNorm();
				dir1 /= l1;
			}

			if (dir1.innerProduct(p1Vec[j] - p0) > l1)
			{
				p1 = p1Vec[j];
				dir1 = p1 - p0;
				l1 = dir1.getNorm();
				dir1 /= l1;
			}
			if (dir2.innerProduct(p2Vec[j] - p2) < 0.0)
			{
				p2 = p2Vec[j];
				dir2 = p3 - p2;
				l2 = dir2.getNorm();
				dir2 /= l2;
			}

			if (dir2.innerProduct(p3Vec[j] - p2) > l2)
			{
				p3 = p3Vec[j];
				dir2 = p3 - p2;
				l2 = dir2.getNorm();
				dir2 /= l2;
			}
		}
	};
};
 
//================================================================================

int CGCPMatcher::getNScenesInStrip()
{
	vector<vector<CVImage*> > ImageGroups;
	vector<CCharString> names;

	int nScenes = 0;

	if (project.findImageGroups(ImageGroups, names, 2))
	{
		vector<CVImage*> copyVec(this->imageVector);
			
		for (unsigned int i = 0; i < ImageGroups.size(); ++i)
		{
			bool hasImage = false;
			for (unsigned int j = 0; j < ImageGroups[i].size(); ++j)
			{
				for (int k = 0; k < (int) copyVec.size(); ++k)
				{
					if ((ImageGroups[i])[j] == copyVec[k])
					{
						hasImage = true;
						CVImage *img = copyVec[k];
						copyVec[k] = copyVec[copyVec.size() - 1];
						copyVec[copyVec.size() - 1] = img;
						copyVec.pop_back();
						--k;
					}
				};
			}

			if (hasImage) ++nScenes;
		}

		nScenes += copyVec.size();
	}
	else nScenes = (int) this->imageVector.size();

	return nScenes;
};

//================================================================================
 
void CGCPMatcher::initStripFootprints(int NScenesBridged, float sceneSizePercentage)
{
	CReferenceSystem ref;
	if (this->pDEM) ref = this->pDEM->getReferenceSystem();

	vector<C2DPoint> firstFootprint(4);
	vector<C2DPoint> lastFootprint(4);

	this->getFootPrint(imageVector[0], firstFootprint[0], firstFootprint[1], firstFootprint[2], firstFootprint[3], &ref);
	this->getFootPrint(imageVector[this->imageVector.size() - 1], lastFootprint[0], lastFootprint[1], lastFootprint[2], lastFootprint[3], &ref);

	C2DPoint cog1(0.0,0.0), cog2(0.0,0.0);
	for (int i = 0; i < 4; ++i)
	{
		cog1 += firstFootprint[i] * 0.25;
		cog2 += lastFootprint[i] * 0.25;
	}

	double stripLength = cog1.getDistance(cog2);
	double stripWidth =  (firstFootprint[0].getDistance(firstFootprint[1]) + firstFootprint[2].getDistance(firstFootprint[1]) +
		                  lastFootprint[0].getDistance(lastFootprint[1])   + lastFootprint[2].getDistance(lastFootprint[1])) * 0.25;
	double sceneLength = (firstFootprint[0].getDistance(firstFootprint[3]) + firstFootprint[2].getDistance(firstFootprint[1]) +
		                  lastFootprint[0].getDistance(lastFootprint[3])   + lastFootprint[2].getDistance(lastFootprint[1])) * 0.25;

	
	int n = this->getNScenesInStrip() / NScenesBridged - 1;
	if ((stripLength  / double(n + 1)) > double(NScenesBridged) * sceneLength) ++n;

	sceneLength *= double(sceneSizePercentage);
	stripLength  /= double(n + 1);

	C2DPoint dir = cog2 - cog1;
	dir.normalise();
	if (dir.innerProduct(firstFootprint[3] - firstFootprint[0]) >= 0.0)
	{
		firstFootprint[3] = firstFootprint[0] + sceneLength * dir;
		firstFootprint[2] = firstFootprint[1] + sceneLength * dir;
	}
	else
	{
		firstFootprint[0] = firstFootprint[3] + sceneLength * dir;
		firstFootprint[1] = firstFootprint[2] + sceneLength * dir;
	}
		
	if (dir.innerProduct(lastFootprint[3] - lastFootprint[0]) >= 0.0)
	{
		lastFootprint[0] = lastFootprint[3] - sceneLength * dir;
		lastFootprint[1] = lastFootprint[2] - sceneLength * dir;
	}
	else
	{
		lastFootprint[3] = lastFootprint[0] - sceneLength * dir;
		lastFootprint[2] = lastFootprint[1] - sceneLength * dir;
	}

	this->footprintVector.resize(0);

	this->footprintVector.push_back(firstFootprint);

	vector<C2DPoint> Footprint(4);
	C2DPoint normal(dir.y, -dir.x);

	for (int i = 0; i < n; ++i)
	{
		C2DPoint cog = cog1 + dir * stripLength * double(i + 1);
		Footprint[0] = cog + dir * sceneLength * 0.5 + normal * stripWidth * 0.5;
		Footprint[1] = cog + dir * sceneLength * 0.5 - normal * stripWidth * 0.5;
		Footprint[2] = cog - dir * sceneLength * 0.5 - normal * stripWidth * 0.5;
		Footprint[3] = cog - dir * sceneLength * 0.5 + normal * stripWidth * 0.5;
		this->footprintVector.push_back(Footprint);
	}

	this->footprintVector.push_back(lastFootprint);
};

//================================================================================
 
void CGCPMatcher::setPossibleOrthoVector(const vector<CVImage *> &OrthoVector)
{
	this->possibleOrthophotos = OrthoVector;
};

//================================================================================

void CGCPMatcher::getMinMaxFootprint(int index, C2DPoint &pmin, C2DPoint &pmax)
{
	vector<C2DPoint> Footprint = this->footprintVector[index];

	pmin = Footprint[0];
	pmax = Footprint[0];
	for (unsigned int j = 1; j < 4; ++j)
	{
		if (Footprint[j].x < pmin.x) pmin.x = Footprint[j].x;
		if (Footprint[j].x > pmax.x) pmax.x = Footprint[j].x;
		if (Footprint[j].y < pmin.y) pmin.y = Footprint[j].y;
		if (Footprint[j].y > pmax.y) pmax.y = Footprint[j].y;
	}
};

//================================================================================
	  
void CGCPMatcher::getOrthosForFootprint(int index, vector<CVImage *> &orthoVec)
{
	orthoVec.clear();

	C2DPoint pmin, pmax;
	C2DPoint orthoMin, orthoMax;
		
	this->getMinMaxFootprint(index, pmin, pmax);

	CReferenceSystem ref;
	if (this->pDEM) ref = this->pDEM->getReferenceSystem();

	for (unsigned int i = 0; i < this->possibleOrthophotos.size(); ++i)
	{
		this->possibleOrthophotos[i]->getFootPrintMinMax(orthoMin, orthoMax, 0, &ref);
	
		if ((pmin.x < orthoMax.x) && (pmin.y < orthoMax.y) && (pmax.x > orthoMin.x) && (pmax.y > orthoMin.y))
		{
			orthoVec.push_back(this->possibleOrthophotos[i]);
		}
	}
};

//================================================================================

bool CGCPMatcher::checkStrip()
{
	this->isStrip = true;
	if (this->imageVector.size() < 2) this->isStrip = false; // a strip has to consist of at least two images

	if (this->isStrip)
	{
		CVImage *img = this->imageVector[0];
		this->isStrip = img->hasPushbroom();  // a strip must exist solely of pushbroom scanner images

		CPushBroomScanner *pushbroomFirst = img->getPushbroom();
		CPushBroomScanner *pushbroomCurr  = 0;
		
		for (unsigned int i = 1; i < this->imageVector.size() && this->isStrip; ++i) 
		{// a strip must exist solely of pushbroom scanner images sharing their exterior orientation parameters

			img = this->imageVector[i];
			if (!img->hasPushbroom()) this->isStrip = false;
			else
			{
				pushbroomCurr = img->getPushbroom();
				if ((pushbroomCurr->getOrbitPath()      != pushbroomFirst->getOrbitPath()) ||
					(pushbroomCurr->getOrbitAttitudes() != pushbroomFirst->getOrbitAttitudes())) this->isStrip = false;
			}
		}

		if (this->isStrip)
		{ // order images by time
			for (unsigned int i = 0; i < this->imageVector.size(); ++i) 
			{
				img = this->imageVector[i];
				double t0 = img->getPushbroom()->getFirstLineTime();

				for (unsigned int j = i+1; j < this->imageVector.size(); ++j) 
				{
					CVImage *img1 = this->imageVector[j];
					double t1 = img1->getPushbroom()->getFirstLineTime();
					if (t1 < t0)
					{
						this->imageVector[j] = img;
						this->imageVector[i] = img1;
						img = img1;
						t0 = t1;
					}
				}
			}
		}
	}

	return this->isStrip;
};

//================================================================================

void CGCPMatcher::setCurrentOrthoImage(int index)
{
	if (index >= (int) this->possibleOrthophotos.size())
	{
		this->pOrthoPhoto = 0;
	}
	else
	{
		CReferenceSystem *ref = 0;
		if (this->pGCPs) ref = this->pGCPs->getReferenceSystem();
		this->pOrthoPhoto = this->possibleOrthophotos[index];
		this->pOrthoPhoto->getFootPrintMinMax(this->Pmin, this->Pmax, 0, ref);
	};
}
	  
//================================================================================

void CGCPMatcher::clearOrthos()
{
	this->possibleOrthophotos.clear();
	this->pOrthoPhoto = 0;

	this->clearWallisFilters();
};
	  
//================================================================================

void CGCPMatcher::initWallisFilters()
{
	this->clearWallisFilters();
	this->WallisFilterVector.resize(this->possibleOrthophotos.size(), 0);
};
    
//================================================================================

void CGCPMatcher::initWallisFilter(int index)
{
	if (!this->WallisFilterVector[index])
	{
		CHugeImage *hug = this->possibleOrthophotos[index]->getHugeImage();

		double mean   = DEFAULT_MEAN_8BIT;
		double stdDev = DEFAULT_STDDEV_8BIT;
		if (hug->getBpp() > 8)
		{
			mean   = DEFAULT_MEAN_16BIT;		
			stdDev = DEFAULT_STDDEV_16BIT;
		}

		hug->setProgressListener(this->listener);

		this->WallisFilterVector[index] = hug->createWallisFilter(DEFAULT_WINDOWSIZE, DEFAULT_WINDOWSIZE,
																  DEFAULT_CONTRAST, DEFAULT_BRIGHTNESS, 
																  mean, stdDev, true);
		hug->removeProgressListener();
	}
};
  
//================================================================================

void CGCPMatcher::clearWallisFilters()
{
	for (unsigned int i = 0; i < this->WallisFilterVector.size(); ++i)
	{
		if (this->WallisFilterVector[i] ) delete this->WallisFilterVector[i];
		this->WallisFilterVector[i] = 0;
	}

	this->WallisFilterVector.clear();
};

//================================================================================

void CGCPMatcher::setMatchPars(const CMatchingParameters &pars)
{
	this->matchingPars = pars;
}

//================================================================================

bool  CGCPMatcher::analyseDiffVectors(float DistThreshold)
{
	bool ret = false;

	DistThreshold *= DistThreshold;

	int nm = this->shiftVec.size() * this->shiftVec.size();
	double *dist = new double[nm];

	for (int k = 0; k < nm; ++k) dist[k] = 0;

	double distMax = -1.0;

	for (unsigned int i = 0; i < this->shiftVec.size(); ++i)
	{
		for (unsigned int j = i+1; j < this->shiftVec.size(); ++j)
		{
			double d = this->shiftVec[i].getSquareDistance(shiftVec[j]);
			dist[i * this->shiftVec.size() + j] = d;
			dist[j * this->shiftVec.size() + i] = d;
			if (d > distMax) distMax = d;
		}
	}

	if (distMax < DistThreshold)
	{
		this->averageShift.x = 0.0;		this->averageShift.y = 0.0;
		for (unsigned int i = 0; i < this->shiftVec.size(); ++i) 
		{
			this->averageShift.x += this->shiftVec[i].x;	
			this->averageShift.y += this->shiftVec[i].y;
		}

		this->averageShift /= double(this->shiftVec.size());
		ret = true;
	}
	else
	{
		vector<int> nSmaller(this->shiftVec.size(), 0);

		for (unsigned int i = 0; i < this->shiftVec.size(); ++i)
		{
			int i0 = i * this->shiftVec.size();
			for (unsigned int j = 0; j < this->shiftVec.size(); ++j)
			{
				if (i != j)
				{
					if (dist[i+j] < DistThreshold) ++nSmaller[i];
				}
			}
		}

		int iMax = 0, indexMax = 0;

		for (unsigned int i = 0; i < this->shiftVec.size(); ++i)
		{
			if (nSmaller[i] > iMax)
			{
				iMax = nSmaller[i];
				indexMax = i;
			}
		}

		if (iMax > this->shiftVec.size() / 2)
		{
			int i0 = indexMax * this->shiftVec.size();

			this->averageShift.x = 0.0;		this->averageShift.y = 0.0;

			for (unsigned int i = 0; i < this->shiftVec.size(); ++i) 
			{
				if (dist[i+i0] < DistThreshold) 
				{
					this->averageShift.x += this->shiftVec[i].x;	
					this->averageShift.y += this->shiftVec[i].y;
				}
			}

			this->averageShift /= double(iMax + 1);
			ret = true;
		}
	}

	delete [] dist;
	return ret;
};

//================================================================================

int CGCPMatcher::MatchPoints(bool useWallis)
{
	this->initWallisFilters();
	
	//CFileName fn = protHandler.getDefaultDirectory() + "MatchGCPs.prt";

	this->shiftVec.clear();
	this->averageShift.x = 0.0; this->averageShift.y = 0.0;

	//protHandler.open(protHandler.prt, fn, false);

	this->pOrthoPhoto->getXYPoints()->forceSortPoints();

	int iTried = 0, iMatched = 0;

	int minWH = this->matchingPars.getPatchSize() / 2 + 1;
	int maxW  = this->pOrthoPhoto->getHugeImage()->getWidth()  - minWH;
	int maxH  = this->pOrthoPhoto->getHugeImage()->getHeight() - minWH;

	for (int i = 0; i < this->pGCPs->GetSize(); ++i)
	{
		CXYPoint pImg;
		CXYZPoint *pObj = this->pGCPs->getAt(i);
		this->pOrthoPhoto->getTFW()->transformObj2Obs(pImg, *pObj);
		if ((pImg.x > minWH) && (pImg.y > minWH) && (pImg.x < maxW) && (pImg.y < maxH))
		{
			++iTried;
		}
	}

	ostrstream oss;
	//oss << "  M A T C H I N G   O F   G R O U N D   C O N T R O L   P O I N T S\n  =================================================================\n\n    Number of 3D points: " << iTried 
	oss << "Number of 3D points: " << iTried 
		<< "\n             Orthophoto: " << this->pOrthoPhoto->getFileNameChar() << "\n\n    Date: "
		<< CSystemUtilities::dateString().GetChar() << "\n    Time: " << CSystemUtilities::timeString().GetChar() << "\n";

	protHandler.print(oss, protHandler.prt);


	if (iTried)
	{
		int orthoIdx = this->getIndexOfOrthophoto(this->pOrthoPhoto);
		CWallisFilter *pWallis = 0;
		if (useWallis)
		{
			this->initWallisFilter(orthoIdx);
			pWallis = this->WallisFilterVector[orthoIdx];
		}

		double useShift = false;
		for (int i = 0; i < this->pGCPs->GetSize(); ++i)
		{
			CXYPoint pImg;
			CXYZPoint *pObj = this->pGCPs->getAt(i);
			this->pOrthoPhoto->getTFW()->transformObj2Obs(pImg, *pObj);
			if ((pImg.x > minWH) && (pImg.y > minWH) && (pImg.x < maxW) && (pImg.y < maxH))
			{
				if (this->matchPoint(pImg, *pObj, useShift, pWallis)) ++iMatched;
				if (this->listener)
				{
					float f = float(iMatched) * 100.0f / float(iTried);
					this->listener->setProgressValue(f);
				}
			}

			if (!useShift && (iMatched > 5) && (iMatched < 15)) useShift = this->analyseDiffVectors(10.0f);
		}
	}
	ostrstream oss1;
	oss1 << "Matching of Ground control points finished;\n    Number of matched points: " << iMatched 
		 << "\n    Time: " << CSystemUtilities::timeString().GetChar() << "\n";

	protHandler.print(oss1, protHandler.prt);
	//protHandler.close(protHandler.prt);

	return iMatched;
};
	 
//================================================================================
  
int CGCPMatcher::getIndexOfOrthophoto(CVImage *ortho) const
{
	for (unsigned int i = 0; i < this->possibleOrthophotos.size(); ++i)
	{
		if (this->possibleOrthophotos[i] == ortho) return i;
	}

	return -1;
};

//=============================================================================

int CGCPMatcher::extractPoints(int footPrintIndex, CVImage *ortho, int minPointNumber, bool useWallis) 
{
	int nPointsExtracted = 0;

	CTrans3D *trafo = CTrans3DFactory::initTransformation(this->pDEM->getReferenceSystem(), ortho->getTFW()->getRefSys());

	if (!trafo) return 0;

	if (!ortho->getXYPoints()->getIsSorted()) ortho->getXYPoints()->forceSortPoints();
	CLabel lab = "SCP_";
	lab += minPointNumber;

	CHugeImage *hug = ortho->getHugeImage();
	
	vector<C2DPoint> footprint = this->footprintVector[footPrintIndex];

	CXYPolyLine footprintPolyPixOrtho;

	C2DPoint pixMin(FLT_MAX, FLT_MAX), pixMax(-FLT_MAX, -FLT_MAX), pix;
	for (int i = 0; i < footprint.size(); ++i)
	{
		C3DPoint footprintPt(footprint[i].x, footprint[i].y, 0.0);
		trafo->transform(footprintPt, footprintPt);
		ortho->getTFW()->transformObj2Obs(pix,footprintPt);
		footprintPolyPixOrtho.addPoint(pix);
		if (pix.x < pixMin.x) pixMin.x = pix.x;
		if (pix.x > pixMax.x) pixMax.x = pix.x;
		if (pix.y < pixMin.y) pixMin.y = pix.y;
		if (pix.y > pixMax.y) pixMax.y = pix.y;
	}

	footprintPolyPixOrtho.closeLine();

	int offsetX = floor(pixMin.x);
	int offsetY = floor(pixMin.y);
	int maxX = floor(pixMax.x);
	int maxY = floor(pixMax.y);
	if (offsetX < 0) offsetX = 0;
	if (offsetY < 0) offsetY = 0;
	if (maxX    < 0) maxX    = 0;
	if (maxY    < 0) maxY    = 0;

	if (offsetX + 1 >= hug->getWidth())  offsetX = hug->getWidth()  - 1;
	if (offsetY + 1 >= hug->getHeight()) offsetY = hug->getHeight() - 1;
	if (maxX + 1    >= hug->getWidth())  maxX    = hug->getWidth()  - 1;
	if (maxY + 1    >= hug->getHeight()) maxY    = hug->getHeight() - 1;
	
	int w = maxX - offsetX;
	int h = maxY - offsetY;

	delete trafo;
	trafo = CTrans3DFactory::initTransformation( ortho->getTFW()->getRefSys(), this->pDEM->getReferenceSystem());

	if ((w > this->matchingPars.getPatchSize()) && (h > this->matchingPars.getPatchSize()) && trafo)
	{
		CImageBuffer orthoBuf(offsetX, offsetY, w, h, hug->getBands(), hug->getBpp(), true);
		if (hug->getRect(orthoBuf))
		{
			int orthoIndex = this->getIndexOfOrthophoto(ortho);

			if (useWallis) 
			{
				this->initWallisFilter(orthoIndex);
				if (this->WallisFilterVector[orthoIndex]) this->WallisFilterVector[orthoIndex]->applyTransferFunction(orthoBuf, orthoBuf, orthoBuf.getHeight(), orthoBuf.getWidth(), 1, true);
			}

			int deltaP = ortho->getXYPoints()->GetSize();

			CFeatureExtractor fex(this->extractionPars);
			fex.setProgressListener(this->listener);

			CXYPolyLine *poly = 0;
			fex.setRoi(&orthoBuf, poly);
			fex.extractFeatures(ortho->getXYPoints(), 0, 0, 0, 0, false);

			for (int j = deltaP; j < ortho->getXYPoints()->GetSize(); ++j)
			{
				CXYPoint *pImg = ortho->getXYPoints()->getAt(j);
				pImg->setLabel(lab);
				CXYZPoint pObj, pObjTrans;
				if (!footprintPolyPixOrtho.contains(*pImg))
				{
					pObj.z = -9999.0;
				}
				else
				{
					if (pImg->getSX() < 0.5) pImg->setSX(0.5);
					if (pImg->getSY() < 0.5) pImg->setSY(0.5);

					ortho->getTFW()->transformObs2Obj(pObjTrans, *pImg, project.getMeanHeight(), this->pDEM->getDEM()->getSigmaZ());
					trafo->transform(pObj, pObjTrans);
					pObj.z = this->pDEM->getDEM()->getBLInterpolatedHeight(pObj.x, pObj.y); 
				}
				if (pObj.z > -9999.0)
				{
					pObj.setLabel(lab);
					pObj.id = orthoIndex;
					this->pGCPs->add(pObj);
					++nPointsExtracted;
					++lab;
				}
				else
				{
					ortho->getXYPoints()->RemoveAt(j);
					--j;
				}
			}
		}
	}

	delete trafo;

	return nPointsExtracted;
};

//================================================================================
  
int CGCPMatcher::ExtractAndMatchPoints(bool useWallis, eCoordinateType GCPType, bool makeGCP)
{
	int iTried = 0, iMatched = 0;

	this->initWallisFilters();

	if (this->isInitialised() && this->pDEM)
	{
		CFileName fn = protHandler.getDefaultDirectory() + "MatchGCPs.prt";

		this->shiftVec.clear();
		this->averageShift.x = 0.0; this->averageShift.y = 0.0;

		protHandler.open(protHandler.prt, fn, false);

		ostrstream oss;
		oss << "  M A T C H I N G   O F   G R O U N D   C O N T R O L   P O I N T S\n  =================================================================\n\n    Date: "
			<< CSystemUtilities::dateString().GetChar() << "\n    Time: " << CSystemUtilities::timeString().GetChar() << "\n";

		protHandler.print(oss, protHandler.prt);

		this->pGCPs = project.getPointArrayByName(this->matchingPars.getXYZFileName());
		if (!this->pGCPs) this->pGCPs = project.addXYZPoints(this->matchingPars.getXYZFileName(), 
			                                                 this->pDEM->getReferenceSystem(), 
															 true, false, eMONOPLOTTED);
		else 
		{
			this->pGCPs->setCoordinateType(this->pDEM->getReferenceSystem().getCoordinateType());
		
			for (int i = 0; i < this->pGCPs->GetSize(); ++i)
			{
				this->pGCPs->GetAt(i)->id = -1;
			}
		}

			
		ostrstream oss2;
		oss2.setf(ios::fixed, ios::floatfield);
		oss2.precision(2);
		int digits = 1;
		if (imageVector.size() > 99) digits = 3;
		else if (imageVector.size() > 9) digits = 2;
		

		oss2 << "        Image list: ";
		for (unsigned int i = 0; i < this->imageVector.size(); ++i)
		{
			oss2 << "\n           (";
			oss2.width(digits);
			oss2 << (i+1) <<"): " << this->imageVector[i]->getFileNameChar();
		}

		oss2 << "\n\n        Orthophoto list: ";
		for (unsigned int i = 0; i < this->possibleOrthophotos.size(); ++i)
		{
			oss2 << "\n           (";
			oss2.width(digits);
			oss2 << (i+1) <<"): " << this->possibleOrthophotos[i]->getFileNameChar();
		}
	
		digits = 1;
		if (footprintVector.size() > 99) digits = 3;
		else if (footprintVector.size() > 9) digits = 2;
		int xyprec = 2;
		if (this->pDEM->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)  xyprec = 6;

		oss2 << "\n\n        List of GCP areas: ";

		for (unsigned int i = 0; i < this->footprintVector.size(); ++i)
		{
			oss2 << "\n           (";
			oss2.width(digits);
			oss2 << (i+1) <<"): ";
			for (int j = 0; j < this->footprintVector[i].size(); ++j)
			{
				oss2.precision(xyprec); 
				oss2 << (this->footprintVector[i])[j].x << " / " << (this->footprintVector[i])[j].y << "; ";
			}
		}

		oss2 << "\n\n        Starting feature extraction; Time: " << CSystemUtilities::timeString().GetChar() << "\n";

		protHandler.print(oss2, protHandler.prt);

	
		for (unsigned int i = 0; i < this->footprintVector.size(); ++i)
		{
			if (this->listener)
			{
				ostrstream ltxt;
				ltxt << "Feature extraction in area " << (i+1) << " of " << this->footprintVector.size() << ends;
				char *z = ltxt.str();
				this->listener->setTitleString(z);
				delete [] z;
			}

			vector<CVImage *> orthoVec;
			this->getOrthosForFootprint(i, orthoVec);
			if (orthoVec.size() > 0)
			{
				for (unsigned int j = 0; j < orthoVec.size(); ++j)
				{
					int nExtracted = extractPoints(i, orthoVec[j], iTried, useWallis);
					ostrstream oss3;
					oss3.setf(ios::fixed, ios::floatfield);
					oss3.precision(2);
					oss3 << "          Feature points extracted from " << orthoVec[j]->getFileNameChar() << ": "
						 << nExtracted << "; Time: " << CSystemUtilities::timeString().GetChar();
					protHandler.print(oss3, protHandler.prt);
					iTried += nExtracted; 
				}
			}
			else
			{
				ostrstream oss3;
				oss3 << "\n        No orthophoto found to overlap with footprint area";
				protHandler.print(oss3, protHandler.prt);
			}

		}

		if (iTried > 0)
		{	
			if (this->listener)
			{
				this->listener->setTitleString("Matching extracted feature points in images");
			}
			ostrstream oss3;
			oss3 << "\n\n        Feature extraction finished; total number of feature points: " <<  iTried << "; Time: " << CSystemUtilities::timeString().GetChar() << "\n";
			oss3 << "\n\n        Commencing matching process\n";
			protHandler.print(oss3, protHandler.prt);
	
			double useShift = false;
			for (int i = 0; i < this->pGCPs->GetSize(); ++i)
			{
				CXYPoint pImg;
				CXYZPoint *pObj = this->pGCPs->getAt(i);
				int id = pObj->id;

				if ((id >= 0) && (id < this->possibleOrthophotos.size()))
				{
					this->pOrthoPhoto = this->possibleOrthophotos[id];
					CWallisFilter *pWallis = this->WallisFilterVector[id];

					int minWH = this->matchingPars.getPatchSize() / 2 + 1;
					int maxW  = this->pOrthoPhoto->getHugeImage()->getWidth()  - minWH;
					int maxH  = this->pOrthoPhoto->getHugeImage()->getHeight() - minWH;

					CTrans3D *trafo = CTrans3DFactory::initTransformation(this->pDEM->getReferenceSystem(), this->pOrthoPhoto->getTFW()->getRefSys());

					if (trafo)
					{
						C3DPoint p;
						trafo->transform(p, *pObj);
						this->pOrthoPhoto->getTFW()->transformObj2Obs(pImg, p);
						pImg.setLabel(pObj->getLabel());

						if ((pImg.x > minWH) && (pImg.y > minWH) && (pImg.x < maxW) && (pImg.y < maxH))
						{
							if (this->matchPoint(pImg, *pObj, useShift, pWallis))
							{
								++iMatched;
								pObj->id = -1;
							}
							if (this->listener)
							{
								float f = float(i) * 100.0f / float(iTried);
								this->listener->setProgressValue(f);
							}
						}

						if (!useShift && (iMatched > 5) && (iMatched < 15)) useShift = this->analyseDiffVectors(10.0f);

						delete trafo;
					}
				}
			}

	
			for (unsigned int i = 0; i < this->possibleOrthophotos.size(); ++i)
			{
				this->possibleOrthophotos[i]->getXYPoints()->forceSortPoints();
			}
	
			for (int i = this->pGCPs->GetSize() - 1; i >= 0; --i)
			{
				int orthoId = this->pGCPs->GetAt(i)->id;

				if (orthoId >= 0) 
				{
					int orthoIdx = this->possibleOrthophotos[orthoId]->getXYPoints()->getPointIndexBinarySearch(this->pGCPs->GetAt(i)->getLabel());
					if (orthoIdx >= 0) this->possibleOrthophotos[orthoId]->getXYPoints()->RemoveAt(orthoIdx);
					this->pGCPs->RemoveAt(i);	
				};
			}

			ostrstream oss1;
			oss1 << "Matching of Ground control points finished;\n    Number of matched points: " << iMatched 
				 << "\n    Time: " << CSystemUtilities::timeString().GetChar() << "\n";

			protHandler.print(oss1, protHandler.prt);
		}
		else
		{
			ostrstream oss1;
			oss1 << "No features could be extracted from the orthophoto; Matching was unsuccessful";
			protHandler.print(oss1, protHandler.prt);
			protHandler.close(protHandler.prt);
		}
	}
		
	if (this->pGCPs) 
	{
		this->pGCPs->setCoordinateType(GCPType);
		if (makeGCP) project.setControlPoints(this->pGCPs);
	}

	protHandler.close(protHandler.prt);

	return iMatched;
};

//================================================================================
  
bool CGCPMatcher::matchPoint(CXYPoint &pImg, CXYZPoint &pObj, bool useDiff, CWallisFilter *pWallis)
{
	bool ret = true;
	// set default sigma SX and SY to 0.5 pixel
	pImg.setSX(0.5);
	pImg.setSY(0.5);
	pImg.setSXY(0.0);

	CBaristaLSM lsm;

	this->matchingPars.setPyrLevel(0);

	double z = pObj.z;
	float uncertaintyZ = pObj.getSigma(2);

	this->matchingPars.setSigmaGeom(-fabs(this->matchingPars.getSigmaGeom()));
	
	C2DPoint *p = 0;

	if (useDiff)
	{
		p = &(this->averageShift);

		if (fabs(this->matchingPars.getSigmaGeom()) > 20.0) this->matchingPars.setSigmaGeom(-20);
	}

	lsm.setProjectDir(project.getFileName()->GetPath());


	ret = lsm.initialize(this->pOrthoPhoto, pImg, z, uncertaintyZ, this->matchingPars, pWallis, "", p);

	if (ret) 
	{
		lsm.setObjectPointArray(this->pGCPs);
		ret = lsm.MatchWithLSM();
		if (ret)
		{
			if (!useDiff)
			{
				for (int i = 0; i < lsm.nSearchImgs(); ++i)
				{
					CSensorModel *s = lsm.getSearchImg(i)->getCurrentSensorModel();
					CTrans3D *trafo = 0;
					if (s) trafo = CTrans3DFactory::initTransformation(*(this->pGCPs->getReferenceSystem()), s->getRefSys());

					if (trafo)
					{
						C3DPoint P(pObj);
						trafo->transform(P, P);
						C2DPoint p = lsm.getSearchPoint(i);
						C2DPoint pBack;
						s->transformObj2Obs(pBack,P);
						p-= pBack;
						this->shiftVec.push_back(p);
						delete trafo;
					}
				}
			}

			ostrstream oss;
			oss.setf(ios::fixed, ios::floatfield);
			oss << "     Point ";
			oss.width(10);
			oss << pObj.getLabel().GetChar() << ": ";
			oss.precision(2);
			oss.width(10);
			oss << pObj.x << " / ";
			oss.width(10);
			oss << pObj.y << " / ";
			oss.width(10);
			oss <<  pObj.z << " was successfully matched";
			protHandler.print(oss, protHandler.prt);
		}
		else
		{
			if (outputMode > eMEDIUM)
			{
				ostrstream oss;
				oss.setf(ios::fixed, ios::floatfield);
				oss << "     Point ";
				oss.width(10);
				oss << pObj.getLabel().GetChar() << ": ";
				oss.precision(2);
				oss.width(10);
				oss << pObj.x << " / ";
				oss.width(10);
				oss << pObj.y << " / ";
				oss.width(10);
				oss <<  pObj.z << " cannot be matched: matching error";
				protHandler.print(oss, protHandler.prt);
			}
		}
	}
	else
	{
		if (outputMode > eMEDIUM)
		{
			ostrstream oss;
			oss.setf(ios::fixed, ios::floatfield);
			oss << "     Point ";
			oss.width(10);
			oss << pObj.getLabel().GetChar() << ": ";
			oss.precision(2);
			oss.width(10);
			oss << pObj.x << " / ";
			oss.width(10);
			oss << pObj.y << " / ";
			oss.width(10);
			oss <<  pObj.z << " cannot be matched because it is outside all search images";
			protHandler.print(oss, protHandler.prt);
		}
	}

	return ret;
};

//================================================================================
