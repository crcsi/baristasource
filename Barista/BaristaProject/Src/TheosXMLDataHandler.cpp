#include "TheosXMLDataHandler.h"

#include <xercesc/sax/AttributeList.hpp>

#include "TheosData.h"
#include "XYZPoint.h"

CTheosXMLDataHandler::CTheosXMLDataHandler(CTheosData& dataStorage) : dataStorage(dataStorage)
{
	this->readMetadata_Id=false;
	this->readDataset_Frame=false;
		this->readVertex=false;
		this->readScene_Center=false;
	this->readCoordinate_Reference_System=false;
		this->readHorizontal_CS=false;
	this->readRaster_CS=false;
	this->readGeoposition=false;
		this->readGeoposition_Points=false;
			this->readTie_Point=false;
	this->readRaster_Dimensions=false;
	this->readRaster_Encoding=false;

	this->readData_Strip=false;
		this->readData_Strip_Identification=false;
		this->readTime_Stamp=false;
		this->readEphemeris=false;
			this->readRaw_Ephemeris=false;
				this->readPoint_List=false;
					this->readPoint=false;
						this->readLocation=false;
						this->readVelocity=false;

		this->readAttitudes=false;
			this->readRaw_Attitudes=false;
				this->readQuaternion_List=false;
					this->readQuaternion=false;

		this->readSensor_Configuration=false;	
			this->readInstrument_Look_Angles_List=false;
				this->readInstrument_Look_Angles=false;
					this->readPolynomial_Look_Angles=false;

			this->readInstrument_Biases=false;

	this->hasVelocity = false;
}

CTheosXMLDataHandler::~CTheosXMLDataHandler(void)
{
}


void CTheosXMLDataHandler::endElement(const XMLCh* const name )
{
	this->tagDepth--;

	switch (this->tagDepth)
	{
	case 1:	{
		this->readMetadata_Id = false;
		this->readDataset_Frame = false;
		this->readCoordinate_Reference_System = false;
		this->readRaster_CS = false;
		this->readGeoposition= false;
		this->readRaster_Dimensions=false;
		this->readRaster_Encoding = false;
		this->readData_Strip = false;		
		
		break;
			}
	case 2: {		
		this->readHorizontal_CS = false;			
		this->readGeoposition_Points= false;
		this->readData_Strip_Identification = false;
		this->readEphemeris = false;	
		this->readAttitudes = false;
		this->readSensor_Configuration = false;

		if (this->readVertex)
		{
			this->readVertex = false;
			CObsPoint* p= this->dataStorage.datasetFramePoints.Add();
			(*p)[0] = this->x;
			(*p)[1] = this->y;
			(*p)[2] = this->vx;
			(*p)[3] = this->vy;
		}
		if (this->readScene_Center)
		{
			this->readScene_Center = false;
			this->dataStorage.datasetSceneCenter[0] = this->x;
			this->dataStorage.datasetSceneCenter[1] = this->y;
			this->dataStorage.datasetSceneCenter[2] = this->vx;
			this->dataStorage.datasetSceneCenter[3] = this->vy;
		}
		if (this->readTime_Stamp)
		{
			this->readTime_Stamp = false;
			int day, month, year,hour, min;
			double sec;				
			sscanf(this->time.GetChar(),"%4d-%2d-%2d %2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
			this->dataStorage.referenceTime = hour*3600 + min*60 + sec;
		}
		
		break;
			}
	case 3: {	
		this->readRaw_Ephemeris=false;
		this->readRaw_Attitudes = false;
		this->readInstrument_Look_Angles_List = false;
		

		if (this->readTie_Point)
		{
			this->readTie_Point = false;
			CObsPoint * p = this->dataStorage.tiePoints.Add();
			(*p)[0]= this->x;
			(*p)[1]= this->y;
			(*p)[2]= this->vx;
			(*p)[3]= this->vy;			
		}	
		else if(this->readInstrument_Biases)
		{
			this->readInstrument_Biases=false;
			CObsPoint* p= &(this->dataStorage.instrumentBiases);
			(*p)[0]=this->x;
			(*p)[1]=this->y;
			(*p)[2]=this->z;
		}
		
		break;
			}
	case 4: {
		this->readPoint_List=false;
		this->readQuaternion_List=false;
		this->readInstrument_Look_Angles = false;	
		

		break;
			}
	case 5: {		

		if (this->readPoint)
		{
			this->readPoint = false;
			CXYZOrbitPoint *point = this->dataStorage.triodeOrbitPoints.add(); 
			int day, month, year,hour, min;
			double sec;	
			CXYZPoint pos(this->x,this->y,this->z);
			CXYZPoint vel(this->vx,this->vy,this->vz);
			sscanf(this->time.GetChar(),"%4d-%2d-%2d %2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
			point->setOrbitPoint(pos,vel,day,month,year,hour,min,sec);
			point->setHasVelocity(this->hasVelocity);
			this->hasVelocity = false;	
		}
		else if (this->readQuaternion)
		{
			this->readQuaternion = false;
			CObsPoint* p= this->dataStorage.attitudeQuaternions.Add();			
			int day, month, year,hour, min;
			double sec;				
			sscanf(this->time.GetChar(),"%4d-%2d-%2d %2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
			(*p)[0] = this->x;
			(*p)[1] = this->y;
			(*p)[2] = this->z;
			(*p)[3] = this->w;
			p->dot = hour*3600 + min*60 + sec;

			C3DPoint* p3=this->dataStorage.GPSTimes.Add();	
			p3->x=year;
			p3->y=month;
			p3->z=day;
			p3->dot=p->dot;		
		}
		else if(this->readPolynomial_Look_Angles)
		{
			this->readPolynomial_Look_Angles=false;
			CObsPoint* p= &(this->dataStorage.polynomialLookAngles);			
			(*p)[0] = this->x;
			(*p)[1] = this->y;
			(*p)[2] = this->z;
			(*p)[3] = this->w;
			(*p)[4] = this->vx;
			(*p)[5] = this->vy;
			(*p)[6] = this->vz;
			(*p)[7] = this->vw;
		}
		
		
		break;
			}
	case 6: {		
		this->readLocation = false;
		this->readVelocity = false;		
		
			}

	}

}

void CTheosXMLDataHandler::startElement(const XMLCh* const name , AttributeList&  attributes)
{
	this->xmlTag[this->tagDepth] = XMLString::transcode(name);
	
	switch (this->tagDepth)
	{
	case 1:		{
		this->read = noRead;
		if (this->xmlTag[1].CompareNoCase("Metadata_Id"))
			this->readMetadata_Id = true;
		else if (this->xmlTag[1].CompareNoCase("Dataset_Frame"))
			this->readDataset_Frame = true;
		else if (this->xmlTag[1].CompareNoCase("Coordinate_Reference_System"))
			this->readCoordinate_Reference_System = true;
		else if (this->xmlTag[1].CompareNoCase("Raster_CS"))
			this->readRaster_CS = true;
		else if (this->xmlTag[1].CompareNoCase("Geoposition"))
			this->readGeoposition = true;
		else if (this->xmlTag[1].CompareNoCase("Raster_Dimensions"))
			this->readRaster_Dimensions = true;
		else if (this->xmlTag[1].CompareNoCase("Raster_Encoding"))
			this->readRaster_Encoding = true;
		else if (this->xmlTag[1].CompareNoCase("Data_Strip"))
			this->readData_Strip = true;		
		
		break;
				}
	case 2:		{
		if (this->readMetadata_Id)
		{
			this->read = readString;
			if (this->xmlTag[2].CompareNoCase("METADATA_FORMAT"))
			{
				this->dataStorage.metadataVersion = XMLString::transcode(attributes.getValue("version"));
				this->read = noRead;
			}
			else if (this->xmlTag[2].CompareNoCase("METADATA_PROFILE"))
				this->stringReader = &this->dataStorage.metatdatProfile;
			else
				this->read = noRead;
		}
		else if (this->readDataset_Frame)
		{
			this->read = noRead;
			if (this->xmlTag[2].CompareNoCase("Vertex"))
				this->readVertex = true;
			else if (this->xmlTag[2].CompareNoCase("Scene_Center"))
				this->readScene_Center = true;
			else if (this->xmlTag[2].CompareNoCase("SCENE_ORIENTATION"))
			{
				this->read = readDouble;
				this->doubleReader = &this->dataStorage.datasetFrameOrientation;
			}
		}
		else if (this->readCoordinate_Reference_System)
		{
			this->read = noRead;
			if (this->xmlTag[2].CompareNoCase("Horizontal_CS"))
				this->readHorizontal_CS = true;
		}
		else if (this->readRaster_CS)
		{
			this->read = readInt;
			if (this->xmlTag[2].CompareNoCase("PIXEL_ORIGIN"))
				this->intReader = &this->dataStorage.pixelOrgin;
			else
				this->read = noRead;
		}
		else if (this->readGeoposition)
		{
			this->read = noRead;
			if (this->xmlTag[2].CompareNoCase("Geoposition_Points"))
				this->readGeoposition_Points = true;
		}
		else if (this->readRaster_Dimensions)
		{
			this->read = readInt;
			if (this->xmlTag[2].CompareNoCase("NCOLS"))
				this->intReader = &this->dataStorage.ncols;
			else if (this->xmlTag[2].CompareNoCase("NROWS"))
				this->intReader = &this->dataStorage.nrows;
			else if (this->xmlTag[2].CompareNoCase("NBANDS"))
				this->intReader = &this->dataStorage.nbands;
			else
				this->read = noRead;
		}
		else if (this->readRaster_Encoding)
		{
			this->read = readInt;
			if (this->xmlTag[2].CompareNoCase("NBITS"))
				this->intReader = & this->dataStorage.bpp;
			else
				this->read = noRead;
		}
		else if (this->readData_Strip)
		{
			this->read = noRead;
			if (this->xmlTag[2].CompareNoCase("Data_Strip_Identification"))
				this->readData_Strip_Identification = true;
			else if (this->xmlTag[2].CompareNoCase("Time_Stamp"))
				this->readTime_Stamp = true;
			else if (this->xmlTag[2].CompareNoCase("Ephemeris"))
				this->readEphemeris = true;
			else if (this->xmlTag[2].CompareNoCase("Attitudes"))
				this->readAttitudes = true;
			else if (this->xmlTag[2].CompareNoCase("Sensor_Configuration"))
				this->readSensor_Configuration = true;
		}	

		break;
				}
	case 3:		{
		if (this->readVertex)
		{
			this->read = readDouble;
			if (this->xmlTag[3].CompareNoCase("FRAME_LON"))
				this->doubleReader = &this->x;
			else if (this->xmlTag[3].CompareNoCase("FRAME_LAT"))
				this->doubleReader = &this->y;
			else if (this->xmlTag[3].CompareNoCase("FRAME_ROW"))
				this->doubleReader = &this->vx;
			else if (this->xmlTag[3].CompareNoCase("FRAME_COL"))
				this->doubleReader = &this->vy;
			else
				this->read=noRead;
		}
		else if (this->readScene_Center)
		{
			this->read = readDouble;
			if (this->xmlTag[3].CompareNoCase("FRAME_LON"))
				this->doubleReader = &this->x;
			else if (this->xmlTag[3].CompareNoCase("FRAME_LAT"))
				this->doubleReader = &this->y;
			else if (this->xmlTag[3].CompareNoCase("FRAME_ROW"))
				this->doubleReader = &this->vx;
			else if (this->xmlTag[3].CompareNoCase("FRAME_COL"))
				this->doubleReader = &this->vy;
			else
				this->read=noRead;
		}
		else if (this->readHorizontal_CS)
		{
			this->read = readString;
			if (this->xmlTag[3].CompareNoCase("HORIZONTAL_CS_TYPE"))
				this->stringReader = &this->dataStorage.horizontalCsType;
			else if (this->xmlTag[3].CompareNoCase("HORIZONTAL_CS_CODE"))
				this->stringReader = &this->dataStorage.horizontalCsCode;			
			else if (this->xmlTag[3].CompareNoCase("HORIZONTAL_CS_NAME"))
				this->stringReader = &this->dataStorage.horizontalCsName;
			else
				this->read=noRead;
		}
		else if (this->readGeoposition_Points)
		{
			this->read = noRead;
			if (this->xmlTag[3].CompareNoCase("Tie_Point"))
				this->readTie_Point = true;
		}
		else if (this->readData_Strip_Identification)
		{
			this->read = readString;
			if (this->xmlTag[3].CompareNoCase("DATA_STRIP_ID"))
				this->stringReader = &this->dataStorage.dataStripID;
			else
				this->read=noRead;
		}
		else if(this->readTime_Stamp)
		{
			this->read = noRead;
			
			if (this->xmlTag[3].CompareNoCase("REFERENCE_TIME"))
			{
				this->read = readString;
				this->stringReader = &this->time;
			}
			else if (this->xmlTag[3].CompareNoCase("REFERENCE_LINE"))
			{
				this->read = readInt;
				this->intReader = &this->dataStorage.referenceLine;
			}
			else if (this->xmlTag[3].CompareNoCase("LINE_PERIOD"))
			{
				this->read = readDouble;
				this->doubleReader = &this->dataStorage.linePeriod;
			}		
			
		}
		else if (this->readEphemeris)
		{
			this->read = noRead;
			if (this->xmlTag[3].CompareNoCase("Raw_Ephemeris"))
				this->readRaw_Ephemeris = true;			
		}		
		else if (this->readAttitudes)
		{
			this->read = noRead;
			if (this->xmlTag[3].CompareNoCase("Raw_Attitudes"))
				this->readRaw_Attitudes = true;			
		}
		else if (this->readSensor_Configuration)
		{
			this->read = noRead;			
			if (this->xmlTag[3].CompareNoCase("Instrument_Look_Angles_List"))
				this->readInstrument_Look_Angles_List = true;
			else if(this->xmlTag[3].CompareNoCase("Instrument_Biases"))
				this->readInstrument_Biases=true;
		}		

		break;
				}
	case 4:		{
		if (this->readTie_Point)
		{
			this->read = readDouble;
			if (this->xmlTag[4].CompareNoCase("TIE_POINT_CRS_X"))
				this->doubleReader = &this->x;
			else if (this->xmlTag[4].CompareNoCase("TIE_POINT_CRS_Y"))
				this->doubleReader = &this->y;			
			else if (this->xmlTag[4].CompareNoCase("TIE_POINT_DATA_X"))
				this->doubleReader = &this->vx;
			else if (this->xmlTag[4].CompareNoCase("TIE_POINT_DATA_Y"))
				this->doubleReader = &this->vy;
			else 
				this->read = noRead;
		}
		else if(this->readRaw_Ephemeris)
		{
			this->read = noRead;
			if (this->xmlTag[4].CompareNoCase("Point_List"))
				this->readPoint_List = true;			
		}
		else if(this->readRaw_Attitudes)
		{
			this->read=noRead;
			if (this->xmlTag[4].CompareNoCase("UT1_UTC"))
			{
				this->read=readDouble;
				this->doubleReader = &(this->dataStorage.UT1_UTC);
			}
			else if (this->xmlTag[4].CompareNoCase("UTC_GPST"))
			{
				this->read=readDouble;
				this->doubleReader = &(this->dataStorage.UTC_GPST);
			}
			else if (this->xmlTag[4].CompareNoCase("U"))
			{
				this->read=readDouble;
				this->doubleReader = &(this->dataStorage.UV.x);
			}
			else if (this->xmlTag[4].CompareNoCase("V"))
			{
				this->read=readDouble;
				this->doubleReader = &(this->dataStorage.UV.y);
			}				
			else if (this->xmlTag[4].CompareNoCase("Quaternion_List"))
			{
				this->readQuaternion_List = true;
			}
		}
		else if (this->readInstrument_Look_Angles_List)
		{
			this->read = noRead;
			if (this->xmlTag[4].CompareNoCase("Instrument_Look_Angles"))
				this->readInstrument_Look_Angles= true;
		}
		else if(this->readInstrument_Biases)
		{
			this->read=readDouble;
			if (this->xmlTag[4].CompareNoCase("YAW"))				
				this->doubleReader = &this->x;						
			else if (this->xmlTag[4].CompareNoCase("PITCH"))
				this->doubleReader = &this->y;
			else if (this->xmlTag[4].CompareNoCase("ROLL"))
				this->doubleReader = &this->z;
			else
				this->read=noRead;
		}

		break;
				}
	case 5:		{
		if(this->readPoint_List)
		{
			this->read=noRead;
			if (this->xmlTag[5].CompareNoCase("Point"))
				this->readPoint = true;
		}
		else if(this->readQuaternion_List)
		{
			this->read=noRead;
			if (this->xmlTag[5].CompareNoCase("Quaternion"))
				this->readQuaternion = true;
		}	
		else if (this->readInstrument_Look_Angles)
		{
			this->read=noRead;
			if(this->xmlTag[5].CompareNoCase("Polynomial_Look_Angles"))			
				this->readPolynomial_Look_Angles=true;			
		}
		break;
				}
	case 6:		{
		if (this->readPoint)
		{
			this->read = noRead;
			if (this->xmlTag[6].CompareNoCase("TIME"))
			{	
				this->read = readString;
				this->stringReader = &this->time;
			}
			else if (this->xmlTag[6].CompareNoCase("Location"))
				this->readLocation = true;
			else if (this->xmlTag[6].CompareNoCase("Velocity"))
				this->readVelocity = true;
			
		}
		else if (this->readQuaternion)
		{
			this->read = readDouble;
			if (this->xmlTag[6].CompareNoCase("TIME"))
			{	
				this->read = readString;
				this->stringReader = &this->time;
			}			
			else if (this->xmlTag[6].CompareNoCase("Q0"))
				this->doubleReader = &this->x;
			else if (this->xmlTag[6].CompareNoCase("Q1"))
				this->doubleReader = &this->y;
			else if (this->xmlTag[6].CompareNoCase("Q2"))
				this->doubleReader = &this->z;
			else if (this->xmlTag[6].CompareNoCase("Q3"))
				this->doubleReader = &this->w;
			else
				this->read = noRead;	
		}	
		else if (this->readPolynomial_Look_Angles)
		{
			this->read = readDouble;			
			if (this->xmlTag[6].CompareNoCase("BAND_INDEX"))
			{
				this->read = readInt;						
				this->intReader = &this->bandIndex;
			}
			else if(this->xmlTag[6].CompareNoCase("XLOS_0"))			
				this->doubleReader=&this->x;
			else if(this->xmlTag[6].CompareNoCase("XLOS_1"))			
				this->doubleReader=&this->y;
			else if(this->xmlTag[6].CompareNoCase("XLOS_2"))			
				this->doubleReader=&this->z;
			else if(this->xmlTag[6].CompareNoCase("XLOS_3"))			
				this->doubleReader=&this->w;
			else if(this->xmlTag[6].CompareNoCase("YLOS_0"))			
				this->doubleReader=&this->vx;
			else if(this->xmlTag[6].CompareNoCase("YLOS_1"))			
				this->doubleReader=&this->vy;
			else if(this->xmlTag[6].CompareNoCase("YLOS_2"))			
				this->doubleReader=&this->vz;
			else if(this->xmlTag[6].CompareNoCase("YLOS_3"))			
				this->doubleReader=&this->vw;	
			else
				this->read=noRead;			
		}
		
		break;
				}
	case 7:		{
		if (this->readLocation)
		{
			this->read = readDouble;
			if (this->xmlTag[7].CompareNoCase("X"))
				this->doubleReader = &this->x;
			else if (this->xmlTag[7].CompareNoCase("Y"))
				this->doubleReader = &this->y;
			else if (this->xmlTag[7].CompareNoCase("Z"))
				this->doubleReader = &this->z;
			else
				this->read=noRead;
		}
		else if (this->readVelocity)
		{
			this->read = readDouble;
			this->hasVelocity = true;
			if (this->xmlTag[7].CompareNoCase("X"))
				this->doubleReader = &this->vx;
			else if (this->xmlTag[7].CompareNoCase("Y"))
				this->doubleReader = &this->vy;
			else if (this->xmlTag[7].CompareNoCase("Z"))
				this->doubleReader = &this->vz;
			else
				this->read=noRead;
		}

				}
	}

	this->tagDepth++;

}