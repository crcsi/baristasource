#include "OrbitObservationsArray.h"

COrbitObservationsArray::COrbitObservationsArray(int nGrowBy) : 
 CMUObjectArray<COrbitObservations, COrbitObservationsPtrArray>(nGrowBy),type(Path)
{
}

COrbitObservationsArray::~COrbitObservationsArray(void)
{
}



void COrbitObservationsArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

	CToken* pNewToken;
	CCharString name("TypeOfOrbit");

	for (int i=0; i< this->GetSize(); i++)
	{
		COrbitObservations* pObs = this->GetAt(i);
		pNewToken = pStructuredFileSection->addToken();
		pNewToken->setValue(name.GetChar(),pObs->getSplineType());
	}
	
	CStructuredFileSection* pNewSection; 
	for (int i=0; i< this->GetSize(); i++)
	{
		COrbitObservations* pObs = this->GetAt(i);
		pNewSection = pStructuredFileSection->addChild();
		pObs->serializeStore(pNewSection);
	}

}

void COrbitObservationsArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	vector<SplineModelType> model;

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("TypeOfOrbit"))
			model.push_back((SplineModelType)token->getIntValue());
	}

	int count=0;
	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("COrbitObservations"))
		{
			COrbitObservations* pObs = this->Add(model[count]);
			pObs->serializeRestore(pChild);
			count++;
		}	
	}
	
}

bool COrbitObservationsArray::isModified()
{
	for (int i = 0; i < this->GetSize(); i++) 
		if (this->GetAt(i)->isModified())
			return true;
	return false;
}

void COrbitObservationsArray::reconnectPointers()
{
	CSerializable::reconnectPointers();

	for (int i=0; i< this->GetSize(); i++)
	{
		COrbitObservations* pObs = this->GetAt(i);
		pObs->reconnectPointers();
	}

}


COrbitObservations* COrbitObservationsArray::Add()
{
	COrbitObservations* pObs = CMUObjectArray<COrbitObservations, COrbitObservationsPtrArray>::Add();
	pObs->setParent(this);
	return pObs;
}

COrbitObservations* COrbitObservationsArray::Add(const COrbitObservations& obs)
{
	this->type = obs.getSplineType();
	COrbitObservations* pNewObs = CMUObjectArray<COrbitObservations, COrbitObservationsPtrArray>::Add(obs);
	pNewObs->setParent(this);
	return pNewObs;
}

COrbitObservations* COrbitObservationsArray::Add(SplineModelType type)
{
	this->type = type;
	COrbitObservations* pObs = CMUObjectArray<COrbitObservations, COrbitObservationsPtrArray>::Add();
	pObs->setParent(this);
	return pObs;
}

void COrbitObservationsArray::resetPointers()
{
	CSerializable::resetPointers();

	for (int i=0;i< this->GetSize();i++)
	{
		COrbitObservations* pObsArray = this->GetAt(i);
		pObsArray->resetPointers();
	}
}

