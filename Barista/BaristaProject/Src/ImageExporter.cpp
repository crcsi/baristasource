#include "ImageExporter.h"

#include "VBase.h"
#include "HugeImage.h"
#include "SystemUtilities.h"

CImageExporter::CImageExporter(CFileName exportFileName,CVBase* srcImage,bool isDEM) :
		filename(exportFileName),filetype("tif"),quality(75),tiledTiff(true),
		exportGeo(isDEM),exportExtern(false),compression("None"),vBaseSrc(srcImage),
		imageSrc(NULL),nChannels(0),depth(8),channelCombination("Intensity"),bandInterleave(false)
{
	this->setFilename(exportFileName);

	if (this->vBaseSrc)
	{
		this->imageSrc = this->vBaseSrc->getHugeImage();
	}

	if (this->imageSrc)
	{
		this->depth = this->imageSrc->getBpp();
		this->setBands(this->imageSrc->getBands());

		if (this->imageSrc->getColorIndexed())
			this->gdalFile.setLookupTable(this->imageSrc->getTransferFunction());
	}

	if (isDEM)
		this->exportGeo = true;

}

CImageExporter::~CImageExporter(void)
{

}

void CImageExporter::setFilename(CFileName name)
{
	this->filename = name;

	if (!this->filename.IsEmpty())
	{

		// use tif as default type if not provided
		if (this->filename.GetFileExt().IsEmpty())
		{
			this->setFileType("tif");
		}
		else
			this->setFileType(this->filename.GetFileExt());
	}
	else
		this->filetype = "";
}


void CImageExporter::setGeoFileName()
{
	if (!this->filename.IsEmpty())
	{
		this->geoFilename = this->filename.GetPath() + CSystemUtilities::dirDelimStr + this->filename.GetFileName();

		if (this->filetype.CompareNoCase("tif"))
			this->geoFilename = this->geoFilename + ".tfw";
		else if (this->filetype.CompareNoCase("jpg"))
			this->geoFilename = this->geoFilename + ".jgw";
		else if (this->filetype.CompareNoCase("jp2"))
			this->geoFilename = this->geoFilename + ".wld";
		else
			this->geoFilename = this->geoFilename + ".tfw";
	}
}

void CImageExporter::setGeoFileName(CFileName name)
{
	this->geoFilename = name;
}

void CImageExporter::setExportExtern(bool b)
{
	if (b && this->hasGeoInformation())
	{
		this->exportExtern = b;
		this->setGeoFileName();
	}
	else if (!b)
		this->exportExtern = b;
}

void CImageExporter::setExportGeo(bool b)
{
	if (b && this->hasGeoInformation())
	{
		this->exportGeo = b;
		if (this->exportGeo && (this->filetype.CompareNoCase("jpg") || this->filetype.CompareNoCase("ecw")))
			this->setExportExtern(true);
	}
	else if (!b)
		this->exportGeo = b;

}

bool CImageExporter::hasGeoInformation() const
{
	return (this->vBaseSrc && this->vBaseSrc->hasTFW());
}

void CImageExporter::setFileType(CCharString newFileType)
{
	this->filetype = newFileType;
	this->filetype.MakeLower();

	// change extension of file name as well
	if (this->filename.GetFileExt() != newFileType)
		this->filename.SetFileExt(newFileType);

	if (newFileType.CompareNoCase("jpg") )
	{
		this->setDepth(8);
		this->setExportGeo(this->exportGeo);

		if (!this->compression.CompareNoCase("Jpeg"))
			this->setCompression("Jpeg");

	}
	else if (newFileType.CompareNoCase("ecw"))
	{
		this->setDepth(8);
		this->setCompression("None");
		this->setExportGeo(this->exportGeo);
	}

	this->setGeoFileName();
}

void CImageExporter::setBands(int b)
{
	if (this->imageSrc && this->imageSrc->getBands() >= b)
	{
		this->nChannels = b;

		if (this->nChannels == 1)
			this->setChannelCombination("Intensity");
		else if (this->nChannels == 3)
			this->setChannelCombination("RGB");
	}
}

void CImageExporter::setDepth(int d)
{
	if (this->imageSrc && this->imageSrc->getBpp() >= d)
	{
		if (this->filetype.CompareNoCase("jpg") && d !=8)
		{
			// we can only write 8 BIT jpeg
			this->depth = 8;
		}
		else if (this->filetype.CompareNoCase("ecw") && d !=8)
		{
			// we can only write 8 BIT ecw
			this->depth = 8;
		}
		else
			this->depth = d;
	}
}


void CImageExporter::setCompression(CCharString c)
{
	this->compression = c;

	this->setDepth(this->depth);

}

int CImageExporter::getDataDepth() const
{
	if (this->imageSrc)
		return this->imageSrc->getBpp();
	else return 0;
}

int CImageExporter::getDataBands() const
{
	if (this->imageSrc)
		return this->imageSrc->getBands();
	else return 0;
}


bool CImageExporter::writeSettings()
{
	bool ret = false;

	if (this->imageSrc)
	{

		// set basic informations
		this->gdalFile.setHeight(imageSrc->getHeight());
		this->gdalFile.setWidth(imageSrc->getWidth());
		this->gdalFile.setBpp(this->depth);
		this->gdalFile.setBands(this->nChannels);
		this->gdalFile.setExportFileOptions(this->channelCombination,this->compression,this->quality,this->exportGeo,this->exportExtern,this->bandInterleave,this->geoFilename);

		if (this->filetype.CompareNoCase("tif"))
			this->gdalFile.setTiled(this->tiledTiff);
		else
			this->gdalFile.setTiled(false);

		CTFW tfw = *this->vBaseSrc->getTFW();

		if (this->exportGeo && tfw.gethasValues())
			this->gdalFile.setTFW(&tfw);
		else
			this->gdalFile.setTFW(NULL);

		ret = true;
	}

	return ret;
}

bool CImageExporter::writeImage()
{
	this->gdalFile.setProgressListener(this->listener);

	bool ret = this->gdalFile.write(this->filename,this->imageSrc);
	this->imageSrc->closeHugeFile();

	this->gdalFile.removeProgressListener();

	return ret;
}

void CImageExporter::setTFW(const CTFW* tfw)
{
	if (tfw)
	{
		this->gdalFile.setTFW(tfw);
	}
}

void CImageExporter::setLookupTable(const CLookupTable* lut)
{
	if (lut)
	{
		this->gdalFile.setLookupTable(lut);
	}
}

bool CImageExporter::applySettings(const CImageExporter& exporter)
{
	bool ret = true;

	// we don't copy all data
	this->setFileType(exporter.getFileType());
	this->setBandInterlave(exporter.getBandInterlave());
	this->setTiledTiff(exporter.getTiledTiff());
	this->setExportGeo(exporter.getExportGeo());
	this->setExportExtern(exporter.getExportExtern());

	return ret;
}
