
#include "ImageImporter.h"

#include "ImageFile.h"
#include "SystemUtilities.h"
#include "GDALFile.h"
#include "RAWFile.h"
#include "ASCIIDEMFile.h"
#include "AVNIR2File.h"
#include "VImage.h"
#include "VDEM.h"
#include "BaristaProject.h"
#include "SatelliteDataImporter.h"
#include "WallisFilter.h"

CImageImporter::CImageImporter() 
{

}

CImageImporter::~CImageImporter(void)
{

}

bool CImageImporter::initImportData(CImageImportData& importData,bool isDEM,CFileName filename,CFileName hugeFileNameDir)
{
	importData.filename = filename;

	bool success = true;

	// first check if this is a huge file
	if (importData.filename.GetFileExt().CompareNoCase("hug"))
	{
		importData.bHugeFile = true;
		importData.imagefile = new CGDALFile(importData.filename);
	}
	else
	{
		importData.bHugeFile = false;

		if (!this->tryReadFile(importData))
		{
			if (this->errorMessage.IsEmpty())
				this->errorMessage = "Reading of file failed!";
			success = false;
		}
		
		// now we can change the hug file directory
		if (success && !hugeFileNameDir.IsEmpty())
		{
			CFileName newName = hugeFileNameDir + importData.filename.GetFullFileName();
			importData.imagefile->setHugeFileName(newName);
		}

		if (success && importData.imagefile)
		{
			// for DEM case only allow file with TFW information
			if (isDEM)
			{
							
				if ( !importData.bHasTFWInImageFile)
				{
					delete importData.imagefile;
					importData.imagefile = NULL;
					this->errorMessage = "No TFW information found!";
					success = false;
				}
				else
				{
					importData.bImportTFW = true;
				}
			}			
		}
	}


	return success;
}



bool CImageImporter::initImportDataAsBinary(CImageImportData& importData,bool isDEM)
{
	if (importData.filename.IsEmpty())
	{
		this->errorMessage = "Empty filename!";
		return false;
	}
	
	
	if (importData.imagefile)
		delete importData.imagefile;
	importData.imagefile = NULL;	

	importData.imagefile = new CRAWFile(importData.filename);

	if (isDEM)
	{
		importData.destbpp = 32;
		importData.bImportTFW = true;
		
		// we expect the tfw information in importData.tfw !!!
		importData.bHasTFWInImageFile = false;
		importData.bHasTFWInExternalFile = false;
		importData.tfwSource = CImageImportData::eParameter;

	}

	return true;

}

bool CImageImporter::initImportDataAsASCII(CImageImportData& importData,bool isDEM)
{
	if (importData.filename.IsEmpty())
	{
		this->errorMessage = "Empty filename!";
		return false;
	}

	if (importData.imagefile)
		delete importData.imagefile;
	importData.imagefile = NULL;

	importData.imagefile = new CASCIIDEMFile(importData.filename);

	if (isDEM)
	{
		importData.destbpp = 32;
		importData.bImportTFW = true;

		// we expect the tfw information in importData.tfw !!!
		importData.bHasTFWInImageFile = false;
		importData.bHasTFWInExternalFile = false;
		importData.tfwSource = CImageImportData::eParameter;
	}

	return true;
}

bool CImageImporter::initImportDataForAVNIR2Image(CImageImportData& importData)
{
	bool ret = false;
	
	if (importData.bands == 1 && importData.filename.GetFileName().Find("ALAV2") >=0)
	{
		CAVNIR2File *avnir = 0;

		if (importData.imagefile && importData.imagefile->getFileType() == CImageFile::eGDALFile)
		{
			CGDALFile* gdal = (CGDALFile*)importData.imagefile;
			avnir = new CAVNIR2File(*gdal);
			importData.imagefile = avnir;
			
			delete gdal;

		}
		else 
		{
			if (importData.imagefile)
				delete importData.imagefile;
		
			avnir = new CAVNIR2File;
			importData.imagefile = avnir;
		}

		importData.pushBroomFilename = importData.filename;

		CSatelliteDataImporter satImporter(Alos);
		ret = satImporter.importMetadata(importData.pushBroomFilename);
			
		if (ret)
		{
			CVImage img;

			ret = img.initPushBroomScanner(importData.pushBroomFilename,
				  						   (CCCDLine*)satImporter.getCamera(),
											satImporter.getCameraMounting(),
											satImporter.getOrbitPath(),
											satImporter.getOrbitAttitudes(),
											satImporter.getSatelliteHeight(),
											satImporter.getGroundHeight(),
											satImporter.getGroundTemperature(),
											satImporter.getFirstLineTime(),
											satImporter.getTimePerLine(),
											satImporter.getActiveMountingShiftParameter(),
											satImporter.getActiveMountingRotationParameter(),
											satImporter.getActiveInteriorParameter(),
											satImporter.getActiveAdditionalParameter());
											//satImporter.getSatelliteType()); // addition: satImporter.getSatelliteType()

			if (ret)
			{
				CPushBroomScanner *push = img.getPushbroom();
				double offNadir = push->getOffNadirAngle(4000);
				avnir->initOffsetFunction(offNadir);
			}

			satImporter.cancelImport();
		}
	}

	return ret;
}

bool CImageImporter::importImage(CImageImportData& importData,bool isDEM)
{
	if (!importData.imagefile)
	{
		this->errorMessage = "Internal Error, no image file created!";
		return false;
	}

	if (!this->checkWritePemission(importData.imagefile->getHugeFileName()))
	{
		this->errorMessage = "No write permissions on disk.";
		return false;
	}

	// set band informations
	importData.imagefile->setColourInformations(importData.channel1,importData.channel2,importData.channel3,importData.channel4);

	// set offset, Z0, sigma, nodata
	importData.imagefile->setZ0(importData.Z0);
	importData.imagefile->setScale(importData.scale);
	importData.imagefile->setSigmaZ(importData.sigmaZ);
	importData.imagefile->setNoDataValue(importData.NoDataValue);


	importData.imagefile->setBands(importData.bands);
	importData.imagefile->setBpp(importData.destbpp);
	importData.imagefile->setWidth(importData.cols);
	importData.imagefile->setHeight(importData.rows);


	importData.imagefile->setBigEndian(importData.BigEndian);
	//importData.imagefile->setToptoBottom(importData.toptobottom);
	importData.imagefile->setInterleaved(importData.interleaved);
	importData.imagefile->setInterpretation(importData.interpretation);


	if (importData.imagefile->getFileType() == CImageFile::eRAWFile)
	{
		CRAWFile* raw = (CRAWFile*)importData.imagefile;
		raw->setHeaderSize(importData.HeaderSize);
		raw->setFileBpp(importData.filebpp);
		raw->setSizeofDestPixel();
		raw->setSizeofFilePixel();
	}


	// delete the huge file
	CFileName hugeFile (importData.filename.GetPath() + CSystemUtilities::dirDelimStr + importData.filename.GetFullFileName() + ".hug");
	if (importData.bDeleteHugeFile && !importData.bHugeFile)
	{
		ifstream file(hugeFile.GetChar(), ios::in | ios::binary);

		if (!file.good())	
			file.close();
		else
		{
			file.close();
			remove(hugeFile.GetChar());
		}
	}

	CDEM* dem = NULL;
	CHugeImage* himage = NULL;
	CVImage* image= NULL;
	CVDEM *vdem = NULL;

	CFileName Path = project.getFileName()->GetPath();
	if (Path.GetLength() && Path[Path.GetLength() - 1] != CSystemUtilities::dirDelim)
	{
		Path += CSystemUtilities::dirDelimStr;
	}

	CFileName relPath;
	CFileName fileSaveName = importData.filename;

	if (!Path.IsEmpty())
	{
		relPath = CSystemUtilities::RelPath(importData.filename, Path);
		fileSaveName.SetPath(relPath);
	}


	if (isDEM)
	{	
		if (!importData.bImportTFW) 
		{
			this->errorMessage.Format("File: %s will not be imported! Provide TFW File first and try import again.",importData.filename.GetFullFileName().GetChar());
			return false;
		}
		
		vdem = project.addDEM(fileSaveName);
		dem = vdem->getDEM();
		himage = (CHugeImage*)dem;
		importData.vbase = vdem; // for DEM we always want the vbase, not for the vimage!!

		//set sigmaZ
		dem->setSigmaZ(importData.sigmaZ);
	}
	else
	{
		image = project.addImage(fileSaveName, false);
		himage = image->getHugeImage();
	}

	if (!himage)
	{
		this->errorMessage = "Error while creating huge image!";
		return false;
	}

	// load the image
	himage->setProgressListener(this->listener);
	
	bool success = false;
	// try the huge file first
    if (!himage->open(importData.imagefile->getHugeFileName(), true))
		success = himage->open(importData.imagefile);
	else
	{
		// set the channels
		himage->setRChannel(0);
		himage->setGChannel(1);
		himage->setBChannel(2);
		success = true;
	}
	
	himage->removeProgressListener();

	// check if we have to flip the image
	if (success && !importData.toptobottom)
	{
		himage->setProgressListener(this->listener);
		success = himage->flip(true,false);
		himage->removeProgressListener();
	}

	// init wallis filter, compute parameters and write to disk, we don't set the transfer function here
	// the user can do that later, but then we got the parameters already!
	if (success && !isDEM && importData.initWallisFilter)
	{
		himage->setProgressListener(this->listener);
		double defaultMean = importData.filebpp == 8 ? DEFAULT_MEAN_8BIT : DEFAULT_MEAN_16BIT;
		double defaultStdDev = importData.filebpp == 8 ? DEFAULT_STDDEV_8BIT : DEFAULT_STDDEV_16BIT;
		CWallisFilter* wf = himage->createWallisFilter(DEFAULT_WINDOWSIZE,DEFAULT_WINDOWSIZE,DEFAULT_CONTRAST,DEFAULT_BRIGHTNESS,defaultMean,defaultStdDev,true);
		if (wf)
		{
			success = true;
			// we have saved the parameters on disk, so it's quick to change the transfer function now
			delete wf; 
		}
		else
		{
			success = false;
			this->errorMessage = "Could not initialise wallis filter!";
		}
		himage->removeProgressListener();
	}	

	// init the dem, only a dem should have a vbase at this time
	if (success && importData.vbase)
		importData.vbase->init();

	if (success)
	{
		// import Geo Data
		if (importData.bImportTFW)
		{
			CFileName tfwFile;

			// copy the information from the image
			if (importData.tfwSource == CImageImportData::eImageFile && 
				importData.bHasTFWInImageFile &&
				importData.imagefile && 
				importData.imagefile->getTFW())
			{
				importData.tfw.copy(*importData.imagefile->getTFW());
				tfwFile = importData.filename.GetFileName() + ".tfw";
				importData.tfw.setFileName(tfwFile);
			}
			else if (importData.tfwSource == CImageImportData::eExternalFile &&
					 importData.bHasTFWInExternalFile &&
					 !importData.tfwFilename.IsEmpty())
			{
				if (image)
				{
					image->initTFW(importData.tfwFilename,importData.tfw.getRefSys());						
					
					importData.tfw.copy(*image->getTFW()); // check if we have a refsys
				}
				else
					importData.tfw.read(importData.tfwFilename.GetChar());
			}
			else  if (importData.tfwSource == CImageImportData::eParameter &&
					  importData.tfw.gethasValues())
			{

			}

			if (importData.tfw.gethasValues() || importData.tfwSource == CImageImportData::eParameter)
			{
				if (dem) 
				{
					importData.vbase = vdem; 			
					dem->setTFW(importData.tfw);
				}
				else if (image) 
				{
					importData.vbase = image; // now also add vbase for vimage
					image->initTFW(importData.tfw);
				}
			}
		} // end import tfw


		// import rpcs
		if (!isDEM && importData.bImportRPC && image)
		{
			if (importData.bHasRPCInExternalFile)
				image->initRPCs(importData.rpcFilename);
			else if (importData.bHasRPCInImageFile && importData.imagefile->getRPC().gethasValues())
			{
				CRPC rpc = importData.imagefile->getRPC();
				rpc.setWidth(image->getHugeImage()->getWidth());
				rpc.setHeight(image->getHugeImage()->getHeight());
				image->initRPCs(rpc);

			}
		}
		

		// import pushbroom data
		if (!isDEM && importData.bImportPushBroom)
		{
		
			CSatelliteDataImporter satImporter(importData.satType);
			satImporter.setProgressListener(this->listener);
			success = satImporter.importMetadata(importData.pushBroomFilename);
			
			if (success)
			{
				//addition>>
				if (importData.satType == Alos) // set the setChangeMountingForMerging flag of the image scanner into false for Alos, by default true
				{
					image->getPushbroom()->setChangeMountingForMerging(false); 
				}
				//<<addition
				if (!image->initPushBroomScanner(importData.pushBroomFilename,
												(CCCDLine*)satImporter.getCamera(),
												satImporter.getCameraMounting(),
												satImporter.getOrbitPath(),
												satImporter.getOrbitAttitudes(),
												satImporter.getSatelliteHeight(),
												satImporter.getGroundHeight(),
												satImporter.getGroundTemperature(),
												satImporter.getFirstLineTime(),
												satImporter.getTimePerLine(),
												satImporter.getActiveMountingShiftParameter(),
												satImporter.getActiveMountingRotationParameter(),
												satImporter.getActiveInteriorParameter(),
												satImporter.getActiveAdditionalParameter()))
												//satImporter.getSatelliteType())) // addition: satImporter.getSatelliteType()
				{//satImporter.s

					satImporter.cancelImport();
					success = false;
					this->errorMessage ="Could not initialize the Pushbroom Scanner with the read data!";
				}
			}
			else
				this->errorMessage = satImporter.getErrorMessage();

			satImporter.removeProgressListener();
		}		
	}
	else
	{
		this->errorMessage = "Image couldn't be imported!";
	}
	
	// force file pointer to close
	importData.imagefile->closeFile();

	// we allow access to the created vbase if the import was ok
	if (success)
	{
		if (isDEM)
			importData.vdem = vdem;
		else
			importData.vimage = image;
	}

	return success;
}


bool CImageImporter::tryReadFile(CImageImportData& importData)
{
	if (importData.filename.IsEmpty())
	{
		this->errorMessage = "Empty filename, no images imported!";	
		return false;
	}

	if (importData.imagefile)
		delete importData.imagefile;
	importData.imagefile = NULL;

	// try as GDALFile
	importData.imagefile = new CGDALFile(importData.filename);
	CGDALFile* gf = (CGDALFile*)importData.imagefile;

	if (gf->readFileInformation())
	{
		// get colour information 
		if (importData.imagefile->bands() == 1)
			importData.channel1 = 0;
		else if (importData.imagefile->bands() == 3)
		{
			importData.channel1 = gf->getColourInterpretation(0);
			importData.channel2 = gf->getColourInterpretation(1);
			importData.channel3 = gf->getColourInterpretation(2);
		}
		else if (importData.imagefile->bands() == 4)
		{
			importData.channel1 = gf->getColourInterpretation(0);
			importData.channel2 = gf->getColourInterpretation(1);
			importData.channel3 = gf->getColourInterpretation(2);
			importData.channel4 = gf->getColourInterpretation(3);
		}

		importData.bands = importData.imagefile->bands();
		

		if ( gf->getTFW() != NULL && gf->getTFW()->gethasValues())
		{
			importData.bHasTFWInImageFile = true;
			importData.bImportTFW = true;
			importData.tfwSource = CImageImportData::eImageFile;
			importData.tfw = *gf->getTFW();
			importData.lowerleft = gf->getTFW()->getShift();
			importData.gridding = gf->getTFW()->getGridding();

		}
		else
			importData.bHasTFWInImageFile = false;


		if (gf->getRPC().gethasValues())
		{
			importData.bHasRPCInImageFile = true;
			//addition >>
			importData.bImportRPC = true;
			importData.rpcSource = CImageImportData::eImageFile;
			importData.imagefile->setRPC(gf->getRPC());
			//<< addition
			//importData.rpcSource = CImageImportData::eExternalFile;
		}
		//addition >>
		else
		{
			importData.bHasRPCInImageFile = false;
			//importData.bHasRPCInExternalFile = false;
			//importData.bImportRPC = true;
			//importData.rpcSource = CImageImportData::eExternalFile;
		}
		//<< addition


		// in case we read a DEM
		importData.NoDataValue = gf->getNoDataValue(1);
		importData.scale = gf->getScale();
		importData.Z0 = gf->getZ0();
		importData.sigmaZ = gf->getSigmaZ();

		// data for raw reader
		importData.cols = gf->getWidth();
		importData.rows = gf->getHeight();
		importData.filebpp = gf->getBpp();
		importData.destbpp = gf->getBpp();
		importData.HeaderSize = 0;


	}
	else
	{
		delete importData.imagefile;
		importData.imagefile = NULL;
	}

	// try as RAWFile
	if ( !importData.imagefile )
	{
		importData.imagefile = new CRAWFile(importData.filename);
		CRAWFile* raw = (CRAWFile*)importData.imagefile;

		if (raw->readHeader())
		{
			// get colour information 
			if (importData.imagefile->bands() == 1)
				importData.channel1 = 0;
			else if (importData.imagefile->bands() == 3)
			{
				importData.channel1 = 0;
				importData.channel2 = 1;
				importData.channel3 = 2;
			}
			else if (importData.imagefile->bands() == 4)
			{
				importData.channel1 = 0;
				importData.channel2 = 1;
				importData.channel3 = 2;
				importData.channel4 = 3;
			}

			importData.bands = importData.imagefile->bands();

			if ( raw->getTFW() != NULL && raw->getTFW()->gethasValues())
			{
				importData.bHasTFWInImageFile = true;
				importData.tfw = *raw->getTFW();
			}
			else
				importData.bHasTFWInImageFile = false;

			importData.gridding.x = raw->getgridX();
			importData.gridding.y = raw->getgridY();
			importData.lowerleft.x = raw->getminX();
			importData.lowerleft.y = raw->getminY();

			importData.HeaderSize = raw->getHeaderSize();
			importData.toptobottom = raw->getToptoBottom();

			importData.filebpp = raw->getBpp();
			importData.destbpp = raw->getBpp();
			
			importData.NoDataValue = raw->getNoDataValue();
			importData.scale = raw->getScale();
			importData.Z0 = raw->getZ0();
			importData.sigmaZ = raw->getSigmaZ();

			importData.cols = raw->getWidth();
			importData.rows = raw->getHeight();

		}
		else
		{
			delete importData.imagefile;
			importData.imagefile = NULL;
		}
	}

	// try as ASCIIDEMFile
	if ( !importData.imagefile )
	{
		importData.imagefile = new CASCIIDEMFile(importData.filename);
		CASCIIDEMFile* ascii = (CASCIIDEMFile*)importData.imagefile;

		if (ascii->readHeader())
		{
			// get colour information 
			if (importData.imagefile->bands() == 1)
				importData.channel1 = 0;
			else if (importData.imagefile->bands() == 3)
			{
				importData.channel1 = 0;
				importData.channel2 = 1;
				importData.channel3 = 2;
			}
			else if (importData.imagefile->bands() == 4)
			{
				importData.channel1 = 0;
				importData.channel2 = 1;
				importData.channel3 = 2;
				importData.channel4 = 3;
			}
			
			importData.bands = importData.imagefile->bands();

			if ( ascii->getTFW() != NULL && ascii->getTFW()->gethasValues())
			{
				importData.bHasTFWInImageFile = true;
				importData.tfw = *ascii->getTFW();
			}
			else
				importData.bHasTFWInImageFile = false;

			importData.gridding.x = ascii->getgridX();
			importData.gridding.y = ascii->getgridY();
			importData.lowerleft.x = ascii->getminX();
			importData.lowerleft.y = ascii->getminY();

			importData.NoDataValue = ascii->getNoDataValue();
			importData.scale = ascii->getScale();
			importData.Z0 = ascii->getZ0();
			importData.sigmaZ = ascii->getSigmaZ();

			importData.filebpp = ascii->getBpp();
			importData.destbpp = ascii->getBpp();
			importData.HeaderSize = 0;

			importData.cols = ascii->getWidth();
			importData.rows = ascii->getHeight();
		}
		else
		{
			// it didn't work.. 
			delete importData.imagefile;
			importData.imagefile = NULL;
			
			// last chance, try XYZ file and convert it into DEM
			if(importData.filename.GetFileExt().CompareNoCase("XYZ"))
			{
				CASCIIDEMFile xyzDEM;
				xyzDEM.setNoDataValue(importData.NoDataValue);
				
				xyzDEM.setProgressListener(this->listener);
				int ret = xyzDEM.convertFromXYZ(importData.filename.GetChar());
				xyzDEM.removeProgressListener();
				/*
				return values:
				0: success in reading the input XYZ file
				1: the input file could not be openned
				2: error in reading the input file
				3: the input file does not represent an appropriate DEM grid
				*/
				if (!ret)
				{
					importData.filename += ".txt";

					importData.imagefile = new CASCIIDEMFile(importData.filename);
					CASCIIDEMFile* converted = (CASCIIDEMFile*)importData.imagefile;

					if ( converted->readHeader())
					{
						importData.NoDataValue = converted->getNoDataValue();
						importData.rows = converted->getHeight();
						importData.cols = converted->getWidth();

						if ( converted->getTFW() != NULL && converted->getTFW()->gethasValues())
						{
							importData.bHasTFWInImageFile = true;
							importData.tfw = *converted->getTFW();
						}
						else
							importData.bHasTFWInImageFile = false;

						importData.gridding.x = converted->getgridX();
						importData.gridding.y = converted->getgridY();
						importData.lowerleft.x = converted->getminX();
						importData.lowerleft.y = converted->getminY();

						importData.filebpp = converted->getBpp();
						importData.destbpp = converted->getBpp();
						importData.HeaderSize = 0;

					}
					else
					{
						delete importData.imagefile;
						importData.imagefile = NULL;
						ret = false;
					}
				}

				if (ret)
				{
					/*
				return values:
				0: success in reading the input XYZ file
				1: the input file could not be openned
				2: error in reading the input file
				3: the input file does not represent an appropriate DEM grid
				*/
					if (ret == 1)
						this->errorMessage = "The input XYZ file could not be openned!";
					else if (ret == 2)
						this->errorMessage = "Error in reading the input XYZ file!";
					else if (ret == 3)
						this->errorMessage = "The input XYZ file does not represent an appropriate DEM grid!";
				}
			}
		}
	}

	return importData.imagefile;
}


bool CImageImporter::checkWritePemission(CFileName filename)
{
	CFileName testFile = filename.GetPath() + "/testFile.txt";
	FILE* check = fopen(testFile.GetChar(),"w");
	
	if (check)
	{
		fclose(check);
		remove(testFile.GetChar());
		return true;
	}
	else
		return false;

}


// #################################################################################################
// #################################################################################################

CImageImportData::CImageImportData() : imagefile(NULL)
{
	this->reset();
}



CImageImportData::~CImageImportData()
{
}

void CImageImportData::deleteImageFile()
{
	if (this->imagefile)
		delete this->imagefile;
	this->imagefile = NULL;
}

void CImageImportData::reset()
{
	this->filename.Empty();
	this->channel1 = 0;
	this->channel2 = 1;
	this->channel3 = 2;
	this->channel4 = 3;
	this->bands = 1;
	this->bDeleteHugeFile = false;
	this->readAVNIR2Image = false;
	
	this->bImportRPC = false;
	this->bHasRPCInExternalFile = false;
	this->bHasRPCInImageFile = false;
	this->rpcSource = eImageFile;


	this->bImportTFW = false;
	this->bImportPushBroom = false;
	this->bHasTFWInImageFile = false;
	this->bHasTFWInExternalFile = false;
	this->tfwSource = eImageFile;

	this->Z0 = 0.0;
	this->scale = 1.0;
	this->NoDataValue = DEM_NOHEIGHT;
	this->sigmaZ = 1.0;
	this->bHugeFile = false;
	this->vbase = NULL;
	this->cols = 0;
	this->rows = 0;
	this->filebpp = 8;
	this->destbpp = 8;
	this->interleaved = true;
	this->BigEndian = true;
	this->interpretation = true;
	this->toptobottom = true;
	this->gridding = C2DPoint(10.0,10.0);
	this->lowerleft = C2DPoint(0.0,0.0);
	this->HeaderSize = 0;

	this->vimage = NULL;
	this->vdem = NULL;

	this->initWallisFilter =  false;
}