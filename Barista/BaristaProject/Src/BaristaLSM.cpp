#include <float.h> 

#include "BaristaLSM.h"
#include "BaristaProject.h"
#include "VImage.h"
#include "XYPoint.h"
#include "ImgBuf.h"
#include "Trans2DAffine.h"
#include "ProtocolHandler.h"
#include "Trans3DFactory.h"
#include "filter.h"
#include "WallisFilter.h"

//================================================================================

CBaristaLSM::CBaristaLSM() : CLSM(project.getMatchingParameter()),
		templateImg(0), rhoMin(0.70f), pointObs(false), currentPyrLevel(0), coordFactor(1.0),
		uncertaintyH(100.0), temp0(0), objectPointArray(0), sigmaGeometryUsed(0.5), Z0(0),
		WallisFilter(0)
{
	ObjPoint.z = 0.0f;
	this->weightFactor = 1.0f / (this->sigmaGreyValUsed * this->sigmaGreyValUsed);
};

//================================================================================

CBaristaLSM::CBaristaLSM(CVImage *img, const CXYPoint &p, double z, float uncertaintyZ, 
						 const CMatchingParameters &pars, CWallisFilter *Wallis,
						 const CCharString &obsPtName, C2DPoint *averageShift) : CLSM(pars),
		templateImg(img), pointObs(false), uncertaintyH(uncertaintyZ), temp0(0), 
		objectPointArray(0), Z0(z), WallisFilter(Wallis)
{
	this->initialize(img,p, z, uncertaintyZ, pars, WallisFilter, obsPtName, averageShift);
}

//================================================================================

CBaristaLSM::~CBaristaLSM()
{
	clear();
};

//================================================================================

void CBaristaLSM::clear()
{
	for (int i = 0; i < this->searchBuffers.size(); ++i)
	{
		delete this->searchBuffers[i];
		this->searchBuffers[i] = 0;
	}

	this->searchBuffers.clear();

	this->searchImgs.clear();

	if (this->templateBuf) delete this->templateBuf;
	this->templateBuf = 0;

	if (this->temp0) delete this->temp0;
	this->temp0 = 0;

	this->resetObjectPointArray();

	CLSM::clear();
}; 
	  

//================================================================================

bool CBaristaLSM::insertObjectPoint() const
{
	bool ret = true;

	if (!this->hasPointObs())
	{
		if (!this->templateImg) ret = false;
		else
		{
			const CSensorModel* smodel = templateImg->getModel(eLSMTFW);
			if (smodel != templateImg->getTFW()) ret = false;
		}
	}

	return ret;
};

//================================================================================

bool CBaristaLSM::initialize(CVImage *img, const CXYPoint &p, double z, float uncertaintyZ, 
							 const CMatchingParameters &pars, CWallisFilter *Wallis,
							 const CCharString &obsPtName, C2DPoint *averageShift)
{
	bool ret = true;

	this->matchingPars = pars;
	this->WallisFilter = Wallis;

	this->Z0 = z;
	this->uncertaintyH = uncertaintyZ;
	this->ObjPoint.setLabel(p.getLabel());

	this->currentPyrLevel = this->matchingPars.getPyrLevel();
	if (this->currentPyrLevel < 0)
	{
		this->currentPyrLevel = img->getHugeImage()->getNumberOfPyramidLevels() - 1;
	}

	this->sigmaGreyValUsed = (img->getHugeImage()->getBpp() <= 8) ? 
		this->matchingPars.getSigmaGrey8Bit() : this->matchingPars.getSigmaGrey16Bit();

	this->sigmaGeometryUsed = fabs(this->matchingPars.getSigmaGeom());

	this->coordFactor = pow(2.0,this->currentPyrLevel);
	this->coordOffset = (this->coordFactor - 1.0) * 0.5;

	this->clear();

	this->templateImg = img;
	if (!img) ret = false;
	else if (!img->getModel( eLSMTFW)) ret = false;
	else
	{
		const CSensorModel* smodel = img->getModel(eLSMTFW);

		if (!smodel ) ret = false;

		this->templateImg = img;
		this->refPoint = p;
		
		smodel->transformObs2Obj(this->ObjPoint, this->refPoint, z, uncertaintyZ);
		smodel->transformObs2Obj(this->Pzmin, this->refPoint, z - uncertaintyH);
		smodel->transformObs2Obj(this->Pzmax, this->refPoint, z + uncertaintyH);
		
		int ps = this->matchingPars.getPatchSize();
		int x = floor(p.x / coordFactor + 0.5) - ps / 2;
		int y = floor(p.y / coordFactor + 0.5) - ps / 2;

		CHugeImage *himg = img->getHugeImage();
		
		if (this->templateBuf) ret = this->templateBuf->set(x, y, ps, ps, himg->getBands(), himg->getBpp(), true);
		else this->templateBuf = new CImageBuffer(x, y, ps, ps, himg->getBands(), himg->getBpp(), true);
		if (this->temp0) ret = this->temp0->set(x- ps / 2, y- ps / 2, 2*ps, 2*ps, himg->getBands(), himg->getBpp(), true);
		else this->temp0 = new CImageBuffer(x- ps / 2, y- ps / 2, 2*ps, 2*ps, himg->getBands(), himg->getBpp(), true);
	
		this->initLSMWeights();

		ret &= himg->getRect(*(this->temp0), this->currentPyrLevel);
		ret &= himg->getRect(*(this->templateBuf), this->currentPyrLevel);

		if (this->WallisFilter)
		{
			this->WallisFilter->applyTransferFunction(*(this->temp0), *(this->temp0), this->temp0->getHeight(), this->temp0->getWidth(), int(this->coordFactor), true);
			this->WallisFilter->applyTransferFunction(*(this->templateBuf), *(this->templateBuf), this->templateBuf->getHeight(), this->templateBuf->getWidth(), int(this->coordFactor), true);
		}

		if (this->matchingPars.getSigmaGeom() > 0)
		{
			this->pointObs = true;
			this->weightFactor = 1.0 / (this->sigmaGreyValUsed * this->sigmaGreyValUsed);
		}
		else
		{
			this->pointObs = false;
			this->weightFactor = 1.0;
		}

		if (ret)
		{
			this->temp0->SetToIntensity();
			this->templateBuf->SetToIntensity();
			initTemplateMeanVariance();
			ret = initSearchBuffers(averageShift);

			int ctPointObs = 0;
			if (this->templateImg->getCurrentSensorModelType() != eTFW) ++ctPointObs;

			for (int i = 0; i < this->searchImgs.size(); ++i)
			{
				if (this->searchImgs[i]->getCurrentSensorModelType() != eTFW) ++ctPointObs;
				this->searchImgs[i]->getXYPoints()->forceSortPoints();
			}
			if (ctPointObs < 2)
			{
				this->pointObs = false;
				this->weightFactor = 1.0;
			}
		}

		if (!ret) clear();
		else if ((!this->objectPointArray) && (!obsPtName.IsEmpty()) && (this->insertObjectPoint()))
		{	
			this->initObjectPointArray(obsPtName);
		}
	}

	return ret;
};

//================================================================================

int CBaristaLSM::addPointObservations(SymmetricMatrix &N, Matrix &t, double &pll, int IndexN)
{
	int nObsPoints = 0;

	Matrix Ai, l(1,2), Fi,Bi;
	Matrix RefCov =  this->refPoint.getCovar() * this->weightFactor;

	if (this->templateImg->getCurrentSensorModel())
	{
		l.element(0,0) = this->refPoint.x;
		l.element(0,1) = this->refPoint.y;

		this->templateImg->getCurrentSensorModel()->Fi(Fi, l, ObjPoint);
		this->templateImg->getCurrentSensorModel()->Bi(Bi, l, ObjPoint);
		this->templateImg->getCurrentSensorModel()->AiPoint(Ai, l, ObjPoint);
		Matrix P = Bi * RefCov * Bi.t();
		P = P.i();

		Matrix nPt = Ai.t() * P;
		Matrix AtPl = nPt * Fi;
		Matrix n = nPt * Ai;

		for (int i = 0; i < 3; ++i)
		{
			t.element(IndexN + i, 0) = -AtPl.element(i,0);
			for (int j = i; j < 3; ++j)
			{
				N.element(IndexN + i, IndexN + j) = n.element(i, j);
			}
		}

		Matrix ltl = Fi.t() * P * Fi;
		pll += ltl.element(0,0);
		nObsPoints = 1;
	}

	for (int idxS = 0, IndexSearchN = 0; idxS < searchImgs.size(); idxS++)
	{
		if (this->searchImgs[idxS]->getCurrentSensorModel())
		{
			this->templateToSearchTrafos[idxS]->transform(searchPoints[idxS], this->refPoint);
			int npars = this->templateToSearchTrafos[idxS]->nParameters();
			Matrix Ax(npars, 1);
			Matrix Ay(npars, 1);
			Matrix BTr(2, 2);

			this->templateToSearchTrafos[idxS]->getDerivatives(this->refPoint.x, this->refPoint.y, Ax, Ay);
			this->templateToSearchTrafos[idxS]->getDerivativesCoords(this->refPoint.x, this->refPoint.y, BTr);

			l.element(0,0) = this->searchPoints[idxS].x * double(this->searchBufferFactors[idxS]);
			l.element(0,1) = this->searchPoints[idxS].y * double(this->searchBufferFactors[idxS]);
			this->searchImgs[idxS]->getCurrentSensorModel()->Fi(Fi, l, ObjPoint);
			this->searchImgs[idxS]->getCurrentSensorModel()->Bi(Bi, l, ObjPoint);
			this->searchImgs[idxS]->getCurrentSensorModel()->AiPoint(Ai, l, ObjPoint);
			Matrix APar(2, npars);
			APar = 0;

			Matrix B = Bi * BTr; // BTr * Bi ?
			
			for (int j = 0; j < npars; ++j)
			{
				APar.element(0,j) += Bi.element(0,0) * Ax.element(j, 0) + Bi.element(0,1) * Ay.element(j, 0);
				APar.element(1,j) += Bi.element(1,0) * Ax.element(j, 0) + Bi.element(1,1) * Ay.element(j, 0);
			}

			Matrix P = B * RefCov * B.t();
			P = P.i();

			Matrix nPt = Ai.t() * P;
			Matrix nPt1 = APar.t() * P;
			Matrix AtPl = nPt  * Fi;
			Matrix AtPl1 = nPt1  * Fi;
			Matrix n = nPt * Ai;	
			Matrix n1 = nPt1 * APar;
			Matrix n12 = nPt1 * Ai;

			for (int i = 0; i < 3; ++i)
			{
				t.element(IndexN + i, 0) += -AtPl.element(i,0);
				for (int j = i; j < 3; ++j)
				{
					N.element(IndexN + i, IndexN + j) += n.element(i, j);
				}
			}

			for (int i = 0; i < npars; ++i)
			{
				t.element(IndexSearchN + i, 0) += -AtPl1.element(i,0);
				for (int j = i; j < npars; ++j)
				{
					N.element(IndexSearchN + i, IndexSearchN + j) += n1.element(i, j);
				}
		
				for (int j = 1; j < 3; ++j)
				{
					N.element(IndexSearchN + i, IndexN + j) += n12.element(i, j);
				}
			}

			++nObsPoints;
			Matrix ltl = Fi.t() * P * Fi;
			pll += ltl.element(0,0);

			IndexSearchN += templateToSearchTrafos[idxS]->nParameters();
		}
	}


	return nObsPoints * 2;
};

//================================================================================

bool CBaristaLSM::getSearchWin(const C2DPoint &p1, const C2DPoint &p2, 
							   int width, int height, 
							   int &xmin, int &ymin, 
							   int &w, int &h, int scaleFactor)
{
	int wT = this->templateBuf->getWidth() * scaleFactor;
	int hT = this->templateBuf->getWidth() * scaleFactor;

	float fact = 1.0 / coordFactor;

	int xmax, ymax;

	if (p1.x < p2.x)
	{
		xmin = floor(fact * p1.x + 0.5) - wT;
		xmax = floor(fact * p2.x + 0.5) + wT;
	}
	else
	{
		xmin = floor(fact * p2.x + 0.5) - wT;
		xmax = floor(fact * p1.x + 0.5) + wT;
	}

	if (p1.y < p2.y)
	{
		ymin = floor(fact * p1.y + 0.5) - hT;
		ymax = floor(fact * p2.y + 0.5) + hT;
	}
	else
	{
		ymin = floor(fact * p2.y + 0.5) - hT;
		ymax = floor(fact * p1.y + 0.5) + hT;
	}

	xmin -= floor(fabs(this->sigmaGeometryUsed * fact / double(scaleFactor)));
	ymin -= floor(fabs(this->sigmaGeometryUsed * fact / double(scaleFactor)));
	xmax += floor(fabs(this->sigmaGeometryUsed * fact / double(scaleFactor)));
	ymax += floor(fabs(this->sigmaGeometryUsed * fact / double(scaleFactor)));

	if (xmin < 0) xmin = 0;
	if (ymin < 0) ymin = 0;
	if (xmax >= width)  xmax = width  - 1;
	if (ymax >= height) ymax = height - 1;

	xmin = (xmin / scaleFactor) * scaleFactor;
	ymin = (ymin / scaleFactor) * scaleFactor;
	xmax = (xmax / scaleFactor) * scaleFactor;
	ymax = (ymax / scaleFactor) * scaleFactor;

	bool ret = true;
	if ((xmax > 0) && (ymax > 0) && (xmin + wT < width) && (ymin + hT < height))
	{
		w = xmax - xmin + 1;
		h = ymax - ymin + 1;
		if ((w <= wT) || (h <= hT))
		{
			w = h = -1;
			ret = false;
		}
	}
	else
	{
		w = h = -1;
		ret = false;
	}

	return ret;
};

//================================================================================

bool CBaristaLSM::adjustBppOfSearchBuf(CImageBuffer *searchBuf)
{
	bool ret = true;

	if (this->templateBuf->getBpp() !=  searchBuf->getBpp())				
	{		
		if (this->templateBuf->getBpp() == 8)
		{
			vector<float> min, max;
			if (!searchBuf->getMinMax(0,min,max))  ret = false;
			else				
			{
				float fact = max[0] - min[0];
				if (fact < FLT_EPSILON)  ret = false;				
				else
				{				
					fact = float(255.0) / fact;
					if (searchBuf->getBpp() == 16)
					{					
						if (searchBuf->getUnsignedFlag())
						{
							for (unsigned long u = 0; u < searchBuf->getNGV(); ++u)
							{
								float g = (float) searchBuf->pixUShort(u);
								g = (g-min[0]) * fact;
								searchBuf->pixUShort(u) = (unsigned short) g;
							}
						}
						else
						{
							for (unsigned long u = 0; u < searchBuf->getNGV(); ++u)
							{
								float g = (float) searchBuf->pixShort(u);
								g = (g-min[0]) * fact;
								searchBuf->pixShort(u) = (short) g;
							}
						}
					}
					else
					{
						for (unsigned long u = 0; u < searchBuf->getNGV(); ++u)
						{
							float g =  searchBuf->pixFlt(u);
							g = (g-min[0]) * fact;
							searchBuf->pixFlt(u) = g;
						}
					}
					searchBuf->convertToUnsignedChar();				
				}	
			}		
		}
		else if (this->templateBuf->getBpp() == 16) 
		{
			if (this->templateBuf->getUnsignedFlag()) searchBuf->convertToUnsignedShort();
			else searchBuf->convertToShort();
		}
		else searchBuf->convertToFloat();				
	}

	return ret;
}
 
//================================================================================

bool CBaristaLSM::checkSearchImage(CVImage *img)
{
	bool ret = true;if (img == this->templateImg) ret = false;
	else if (!img->getModel(this->matchingPars.getTrafoMode()) )
	{
		ret = false;
	}

	if (ret)
	{
		if ((this->matchingPars.getBandMode() == eIDENTICALBANDS) && 
			(img->getHugeImage()->getBands() != this->templateImg->getHugeImage()->getBands())) ret = false;
		if ((this->matchingPars.getBppMode()  == eIDENTICALBPPS)  && 
			(img->getHugeImage()->getBpp() != this->templateImg->getHugeImage()->getBpp())) ret = false;

		if (ret)
		{
			if ((img->getCurrentSensorModel()) && (this->templateImg->getCurrentSensorModel()))
			{
				if (img->getCurrentSensorModel()->getRefSys().getCoordinateType() != 
					this->templateImg->getCurrentSensorModel()->getRefSys().getCoordinateType()) ret = false;
			}
		}
	}

	return ret;
};

//================================================================================
	  
bool CBaristaLSM::initSearchWin(CVImage *img, CImageBuffer *&imgBuf, int xmin, 
								int ymin, int w, int h, int searchFactor)
{
	bool ret = true;

	bool initBuf = (imgBuf == 0);

	if (initBuf) imgBuf = new CImageBuffer;
	CHugeImage *hug = img->getHugeImage();

	if ((!imgBuf) || (hug->getBpp() > 16)) ret = false;
	else
	{
		if (searchFactor > 1)
		{
			CImageBuffer rawBuf(xmin, ymin, w, h, hug->getBands(), hug->getBpp(), true);
			if (!hug->getRect(rawBuf, this->currentPyrLevel)) ret = false;
			else
			{
				rawBuf.SetToIntensity();
				ret = adjustBppOfSearchBuf(&rawBuf);
				if (ret)
				{				
					CImageFilter fil (eGauss, 3, 3, searchFactor / 2);
					CImageBuffer rawBuf1(rawBuf);
					fil.convolve(rawBuf1, rawBuf, false);
					xmin = xmin / searchFactor + 1;
					ymin = ymin / searchFactor + 1;
					w    = w    / searchFactor - 1;
					h    = h    / searchFactor - 1;
					if ((w <= this->templateBuf->getWidth()) || (h <= this->templateBuf->getHeight())) ret = false;
					else
					{
						ret = imgBuf->set(xmin, ymin, w, h, hug->getBands(), hug->getBpp(), true);
						if (ret)
						{			
							double x0 = xmin * searchFactor;
							C2DPoint p(x0, ymin * searchFactor);
							unsigned long uMax = imgBuf->getNGV();
							unsigned long uMaxRow = imgBuf->getDiffY();

							if (hug->getBpp() <= 8)
							{
								unsigned char *buf = imgBuf->getPixUC();
								for (unsigned long uMin = 0; uMin < uMax; uMin += imgBuf->getDiffY(), uMaxRow += imgBuf->getDiffY(), p.y += searchFactor)
								{
									p.x = x0;
									for (unsigned long u = uMin; u < uMaxRow; ++u, p.x += searchFactor)
									{
										rawBuf.getInterpolatedColourVecBicub(p, &(buf[u]));
									}
								}
							}
							else
							{
								unsigned short *buf = imgBuf->getPixUS();
								for (unsigned long uMin = 0; uMin < uMax; uMin += imgBuf->getDiffY(), uMaxRow += imgBuf->getDiffY(), p.y += searchFactor)
								{
									p.x = x0;
									for (unsigned long u = uMin; u < uMaxRow; ++u, p.x += searchFactor)
									{
										rawBuf.getInterpolatedColourVecBicub(p, &(buf[u]));
									}
								}
							}
						

							changeSearchMeanVariance(imgBuf);
						}
					}
				}
			}
		}
		else
		{
			if (!imgBuf->set(xmin, ymin, w, h, hug->getBands(), hug->getBpp(), true)) ret = false;
			else if (!hug->getRect(*imgBuf, this->currentPyrLevel)) ret = false;
			else
			{
				imgBuf->SetToIntensity();
				if (!adjustBppOfSearchBuf(imgBuf)) ret = false;
				else changeSearchMeanVariance(imgBuf);
			}
		}
	}
				
	if (!ret && initBuf)
	{
		if (imgBuf) delete imgBuf;
		imgBuf = 0;
	}

	return ret;
};

//================================================================================

bool CBaristaLSM::initSearchBuffers(C2DPoint *averageShift)
{
	bool ret = true;

	this->pminVec.clear();
	this->pmaxVec.clear();	

	int wT = this->templateBuf->getWidth();
	int hT = this->templateBuf->getWidth();

	C3DPoint P1, P2;
	C2DPoint p1 = this->refPoint; p1.x -= wT * this->coordFactor * 0.5;
	C2DPoint p2 = this->refPoint; p2.y -= hT * this->coordFactor * 0.5;

	const CSensorModel* templatemodel = this->templateImg->getModel(eLSMTFW);

	if ( !templatemodel) return false;

	templatemodel->transformObs2Obj(P1, p1, this->Z0);
	templatemodel->transformObs2Obj(P2, p2, this->Z0);

	for (int i = 0, iSearch = 0; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		if (checkSearchImage(img))
		{
			C2DPoint obsPt, obsPtMin, obsPtMax, pS1, pS2;

			const CSensorModel* imgmodel = img->getModel(this->matchingPars.getTrafoMode());

			if (imgmodel) 
			{
				CTrans3D *trafo = CTrans3DFactory::initTransformation(templatemodel->getRefSys(), imgmodel->getRefSys());
				if (trafo)
				{
					C3DPoint P;
					trafo->transform(P, this->ObjPoint);
					imgmodel->transformObj2Obs(obsPt, P);
					trafo->transform(P, this->Pzmin);
					imgmodel->transformObj2Obs(obsPtMin, P);
					trafo->transform(P, this->Pzmax);
					imgmodel->transformObj2Obs(obsPtMax, P);
					trafo->transform(P, P1);
					imgmodel->transformObj2Obs(pS1, P);
					trafo->transform(P, P2);
					imgmodel->transformObj2Obs(pS2, P);
					delete trafo;
				}
				else
				{
					imgmodel->transformObj2Obs(obsPt, this->ObjPoint);
					imgmodel->transformObj2Obs(obsPtMin, this->Pzmin);
					imgmodel->transformObj2Obs(obsPtMax, this->Pzmax);
					imgmodel->transformObj2Obs(pS1, P1);
					imgmodel->transformObj2Obs(pS2, P2);
				}
				if (averageShift)
				{
					obsPt.x    -= averageShift->x;  obsPt.y    -= averageShift->y;
					obsPtMin.x -= averageShift->x;  obsPtMin.y -= averageShift->y;
					obsPtMax.x -= averageShift->x;  obsPtMax.y -= averageShift->y;
					pS1.x      -= averageShift->x;  pS1.y      -= averageShift->y;
					pS2.x      -= averageShift->x;  pS2.y      -= averageShift->y;
				}

				CHugeImage *himg = img->getHugeImage();

				float fact = 1.0 / coordFactor;

				int xmin, ymin, ymax, w, h;
				int levWidth  = himg->getWidth()  / (int)  this->coordFactor;
				int levHeight = himg->getHeight() / (int)  this->coordFactor;
				
				int searchFactor = getSearchTempFactor(img);
				
				if (getSearchWin(obsPtMin, obsPtMax, levWidth, levHeight, xmin, ymin, w, h, searchFactor))
				{
					CImageBuffer *searchBuf = 0;
					if (initSearchWin(img, searchBuf, xmin, ymin, w, h, searchFactor))
					{
						double redFact = 1.0 / double(searchFactor);
						CTrans2DAffine *trafo = new CTrans2DAffine(this->refPoint, obsPt * redFact, p1, pS1 * redFact, p2, pS2 * redFact);
						this->searchImgs.push_back(img);

						this->pminVec.push_back(obsPtMin * fact * redFact);
						this->pmaxVec.push_back(obsPtMax * fact * redFact);
						this->addSearchImage(searchBuf, trafo, searchFactor);	
						this->searchPoints[this->searchPoints.size() - 1] *= double(searchFactor);
					}
				}
			}
		}
	}

	if (nSearchImgs() < 1) 
	{
		clear();
		ret = false;
	}
	
	return ret;
};


//================================================================================

bool CBaristaLSM::readTemplate()
{
	if (!this->templateImg || !this->templateBuf) return false;

	bool ret = this->templateImg->getHugeImage()->getRect(*(this->templateBuf), this->currentPyrLevel);

	if (this->WallisFilter)
	{
		this->WallisFilter->applyTransferFunction(*(this->templateBuf), *(this->templateBuf), this->templateBuf->getHeight(), this->templateBuf->getWidth(), int(this->coordFactor), true);
	}

	this->templateBuf->SetToIntensity();
	return ret;
}

//================================================================================
	  
void CBaristaLSM::setObjectPointArray(CXYZPointArray *ptArr)
{
	this->objectPointArray = ptArr;
	if (this->objectPointArray) 
	{
		if (!this->objectPointArray->getIsSorted()) this->objectPointArray->forceSortPoints();
	}
};

//================================================================================

bool CBaristaLSM::initObjectPointArray(const CCharString &name)
{		
	this->objectPointArray = project.getPointArrayByName(name);
	if (!this->objectPointArray)
	{
		const CSensorModel *mod = this->templateImg->getCurrentSensorModel();
		if (!mod && (this->templateImg->hasTFW())) mod = this->templateImg->getTFW();
		if (mod) this->objectPointArray = project.addXYZPoints(name, mod->getRefSys(), 
											                   false, false, eBUNDLERESULT);			
	};

	if (this->objectPointArray) this->objectPointArray->forceSortPoints();
	return (this->objectPointArray != 0);
};

//================================================================================

void CBaristaLSM::resetObjectPointArray()
{
	this->objectPointArray = 0;
};
	
//================================================================================
  	  
double CBaristaLSM::getTempSearchFactor()
{
	double factor = 0.0f;

	C2DPoint p1(0,0), p2(1,1), p1T, p2T;
	double d = 1.0 / sqrt(2.0);

	for (unsigned int i = 0; i < this->templateToSearchTrafos.size(); ++i)
	{
		this->templateToSearchTrafos[i]->transform(p1T, p1);
		this->templateToSearchTrafos[i]->transform(p2T, p2);
		double f = p1T.getDistance(p2T) * d;
		factor += f;
	}

	factor /= double(this->templateToSearchTrafos.size());

	return factor;
};

//================================================================================
	    
const CSensorModel *CBaristaLSM::getTemplateSensorModel() 
{
	return this->templateImg->getModel(eLSMTFW);
};

//================================================================================
  	    
const CSensorModel *CBaristaLSM::getSearchSensorModel(int i) 
{
	return searchImgs[i]->getModel(eLSMTFW);
};

//================================================================================
  	    
double CBaristaLSM::getTempSearchFactor(CVImage *psearch)
{
	double factor = -1.0f;

	float height = (float) project.getMeanHeight();

	float gsdTmp = this->templateImg->getApproxGSD(height);
	float gsdImg = psearch->getApproxGSD(height);
	
	if ((gsdTmp > 0.0f) && (gsdImg > 0.0f))
	{
		factor = gsdImg / gsdTmp;
	}


	return factor;
};
	  
//================================================================================
  	   
int CBaristaLSM::getSearchTempFactor(CVImage *psearch)
{
	int searchFactor = 1;
	
	if (this->matchingPars.getScaleMode() == eLSMUSETEMP)
	{
		float scale = this->getTempSearchFactor(psearch);

		if (scale > FLT_EPSILON) searchFactor = int(floor(1.0f / scale));
		
		if (searchFactor < 1)  searchFactor = 1;
	}
	return searchFactor;
};

//================================================================================

bool CBaristaLSM::initTemplate()
{
	int fact = (int) floor(getTempSearchFactor() + 0.5);
	if (fact < 1) fact = 1;
	
	bool ret;

	if (fact == 1)
		ret = this->temp0->extractSubWindow(this->templateBuf->getOffsetX(),
											this->templateBuf->getOffsetY(), 
											this->templateBuf->getWidth(), 
											this->templateBuf->getHeight(), 
											*(this->templateBuf));

	else
	{
		double oneByFac = 1.0 / double(fact);

		Matrix pars(6,1);
		pars.element(0, 0) = oneByFac; // a1;
		pars.element(1, 0) = 0.0 ; // a2;
		pars.element(2, 0) = this->refPoint.x; // a0;
		pars.element(3, 0) = 0.0; // b1;
		pars.element(4, 0) = oneByFac ; // b2;
		pars.element(5, 0) = this->refPoint.y; // b0;

		
		CTrans2DAffine trafo(pars, this->refPoint);

		ret = this->templateBuf->resample(*(this->temp0), trafo, eBicubic);
		for (unsigned int i = 0; i < this->templateToSearchTrafos.size(); ++i)
		{
			this->templateToSearchTrafos[i]->get(pars, this->refPoint);
			pars.element(0, 0) *= oneByFac; // a1;
			pars.element(1, 0) *= oneByFac; // a2;
			pars.element(3, 0) *= oneByFac; // b1;
			pars.element(4, 0) *= oneByFac; // b2;
			this->templateToSearchTrafos[i]->set(pars, this->refPoint); 
		}
	}


	return ret;
};

//================================================================================
	  
bool CBaristaLSM::setResultsToBaristaProject(double sigmaObs)
{
	bool ret = true;

	int nS = 0;
	for (int i = 0; i < this->nSearchImgs(); ++i)
	{
		if (this->rhoVec[i] >= this->matchingPars.getRhoMin())
		{
			if (sigmaObs > FLT_EPSILON)
			{
				this->searchPoints[i].setSX(sigmaObs);
				this->searchPoints[i].setSY(sigmaObs);
			}

			int PtIndex = this->searchImgs[i]->getXYPoints()->getPointIndexBinarySearch(this->ObjPoint.getLabel());
			if (PtIndex < 0) this->searchImgs[i]->getXYPoints()->add(this->searchPoints[i]);
			else
			{
				CXYPoint *p = this->searchImgs[i]->getXYPoints()->getAt(PtIndex);
				*p = this->searchPoints[i];
			}
			++nS;
		}
	}

	if (this->insertObjectPoint())
	{
		if (!nS) ret = false;
		else
		{
			if (sigmaObs > FLT_EPSILON) ret = this->intersect(true);

			if (this->objectPointArray)
			{				
				CTrans3D *trafo = CTrans3DFactory::initTransformation(this->templateImg->getModel(eLSMTFW)->getRefSys(), this->objectPointArray->getRefSys());
				if (trafo)
				{
					CXYZPoint tmp(this->ObjPoint);
					trafo->transform(this->ObjPoint, tmp);

					int PtIndex = this->objectPointArray->getPointIndexBinarySearch(this->ObjPoint.getLabel());
					if (PtIndex < 0) this->objectPointArray->add(this->ObjPoint);
					else 
					{
						CXYZPoint *pObj = this->objectPointArray->getAt(PtIndex);
						*pObj = this->ObjPoint;
					}

					delete trafo;
				}
			}
		}
	}

	return ret;
};

//================================================================================
	
bool CBaristaLSM::LSM()
{
	bool ret = initTemplate();

	if (ret) ret = CLSM::LSM();

	if (ret) ret = this->setResultsToBaristaProject(-1.0);
	
	return ret;
}

//================================================================================

bool CBaristaLSM::CrossCorrelationCurrentLevel(float maxCorr, vector<bool> &isUsed)
{
	bool ret = true;

	float lMax = 0;

	vector<C2DPoint> differences;
	vector<C2DPoint> Pmin;

	int wby2 = this->templateBuf->getWidth()  / 2;
	int hby2 = this->templateBuf->getHeight() / 2;

	isUsed.resize(this->pminVec.size());

	for (int i = 0; i < this->pminVec.size(); ++i)
	{
		differences.push_back(this->pmaxVec[i] - this->pminVec[i]);
		double l = differences[i].getNorm();
		if (l > lMax) lMax = l;
		Pmin.push_back(this->pminVec[i]);
		Pmin[i].x -= wby2;
		Pmin[i].y -= hby2;
	}

	int nSteps = int(floor(lMax + 0.5));

	if (nSteps < 1) nSteps = 1;

	for (int i = 0; i < this->pminVec.size(); ++i)
	{
		differences[i] /= (double) nSteps;
	}

	vector<vector<float> > currentRhos(nSteps);
	vector<float> totalRhos(nSteps);
	vector<int> nUsed(nSteps);

	for (int i = 0; i < nSteps; ++i)
	{
		currentRhos[i].resize(this->pminVec.size());
		totalRhos[i] = 0;
		nUsed[i] = 0;
	}
		
	for (int j = 0; j < this->pminVec.size(); ++j)
	{	  
		rotateCrossCorrTemplate(j);

		for (int i = 0; i < nSteps; ++i)
		{
			int x = (int) floor(Pmin[j].x + (double) i * differences[j].x + 0.5);
			int y = (int) floor(Pmin[j].y + (double) i * differences[j].y + 0.5);
			if ((x <  this->searchBuffers[j]->getOffsetX()) ||
				(y <  this->searchBuffers[j]->getOffsetY()) ||
				(x + this->templateBuf->getWidth()  >= this->searchBuffers[j]->getOffsetX() + this->searchBuffers[j]->getWidth()) ||
				(y + this->templateBuf->getHeight() >= this->searchBuffers[j]->getOffsetY() + this->searchBuffers[j]->getHeight()))
			{
				currentRhos[i][j] = -1.0f;
			}
			else
			{
				currentRhos[i][j] = crossCorr(x, y, j);
				if (currentRhos[i][j] > maxCorr)
				{
					totalRhos[i] += currentRhos[i][j];
					++nUsed[i];
				}
			}
		}
	}


	float maxRho = -2;
	int iMax = -1;

	for (int i = 0; i < nSteps; ++i)
	{
		if (nUsed[i] < 1) totalRhos[i] = 0.0; 
		else 
		{
			totalRhos[i] /= (float) nUsed[i];

			if (totalRhos[i] > maxRho)
			{
				maxRho = totalRhos[i];
				iMax = i;
			}
		}
	}

	if (iMax < 0) ret = false;
	else
	{
		double fact = (double) iMax;
		if ((iMax < nSteps - 1) && iMax > 0)
		{
			double rM1 = totalRhos[iMax - 1];
			double rP1 = totalRhos[iMax + 1];
			double d = 2.0 * (rM1 + rP1 - 2.0 * maxRho);
			if (fabs(d) > FLT_EPSILON) fact += (rM1 - rP1) / d;
		}

		for (int j = 0; j < this->pminVec.size(); ++j)
		{
			isUsed[j] = currentRhos[iMax][j] > maxCorr;
			int x = (int) floor(Pmin[j].x + (double) iMax * differences[j].x + 0.5);
			int y = (int) floor(Pmin[j].y + (double) iMax * differences[j].y + 0.5);
		
			this->searchPoints[j].x = (pminVec[j].x + fact * differences[j].x) * this->coordFactor * double(this->searchBufferFactors[j]);
			this->searchPoints[j].y = (pminVec[j].y + fact * differences[j].y) * this->coordFactor * double(this->searchBufferFactors[j]);
			double sig = (this->matchingPars.getSigmaGeom()> 0)? this->matchingPars.getSigmaGeom() * this->coordFactor : 0.5 * this->coordFactor;
			this->searchPoints[j].setSX(sig);
			this->searchPoints[j].setSY(sig);
			this->searchPoints[j].setSXY(0);

			this->rhoVec = currentRhos[iMax];
		}
	}
	if (outputMode > eMEDIUM)
	{
		ostrstream oss; oss << "lev_" << this->currentPyrLevel << ends;
		char *z = oss.str();
		dump(z, true, true, false, false, this->coordFactor);
		delete [] z;
	}
	
	if (maxRho < maxCorr) ret = false;

	return ret;
};

//================================================================================

bool CBaristaLSM::CrossCorrelationCurrentLevelArea(float maxCorr, vector<bool> &isUsed)
{
	bool ret = true;

	int wby2 = this->templateBuf->getWidth()  / 2;
	int hby2 = this->templateBuf->getHeight() / 2;

	this->rhoVec.resize(this->pminVec.size());
	isUsed.resize(this->pminVec.size());


	int nUsed = 0;

	double dXref = this->refPoint.x - double(this->templateBuf->getOffsetX());
	double dYref = this->refPoint.y - double(this->templateBuf->getOffsetY());

	for (int j = 0; j < this->pminVec.size(); ++j)
	{	  
		float rhoMax = -1.0f;
		unsigned long uMax = 0;
		int yMax = 0, xMax = 0;

		rotateCrossCorrTemplate(j);
		CImageBuffer rho(0, 0, this->searchBuffers[j]->getWidth() - this->templateBuf->getWidth(), 
			             this->searchBuffers[j]->getHeight() - this->templateBuf->getHeight(), 1, 32, false);

		for (int y = 0; y < rho.getHeight(); ++y)
		{
			unsigned long uRho = y * rho.getWidth();
			for (int x = 0; x < rho.getWidth(); ++x, ++uRho)
			{
				float cRho = crossCorr(x + this->searchBuffers[j]->getOffsetX(), y + this->searchBuffers[j]->getOffsetY(), j);
				rho.pixFlt(uRho) = cRho;
				if (cRho > rhoMax)
				{
					rhoMax = cRho;
					uMax = uRho;
					yMax = y;
					xMax = x;
				}
			}
		}

		this->rhoVec[j] = rhoMax;

		if (rhoMax > maxCorr)
		{
			isUsed[j] = true;
			++nUsed;
			double X = double(xMax + this->searchBuffers[j]->getOffsetX() + dXref) * this->coordFactor;
			double Y = double(yMax + this->searchBuffers[j]->getOffsetY() + dYref) * this->coordFactor;
			if ((xMax > 0) && (yMax > 0) && (xMax + 1 < rho.getWidth()) && (yMax + 1 < rho.getHeight()))
			{
				unsigned long u  = uMax;
				float cRho = rho.pixFlt(u);
				double sumRho = cRho, sumRhoX = 0.0, sumRhoY = 0.0, sumRhoXY = 0.0, sumRhoX2 = 0.0, sumRhoY2 = 0.0;
				u += rho.getDiffX(); cRho = rho.pixFlt(u); // (1 / 0)
				sumRho += cRho; sumRhoX += cRho; sumRhoX2 += cRho;
				u += rho.getDiffY(); cRho = rho.pixFlt(u); // (1 / 1)
				sumRho += cRho; sumRhoX += cRho; sumRhoY += cRho;  sumRhoXY += cRho; sumRhoX2 += cRho; sumRhoY2 += cRho;
				u -= rho.getDiffX(); cRho = rho.pixFlt(u); // (0 / 1)
				sumRho += cRho; sumRhoY += cRho;  sumRhoY2 += cRho;
				u -= rho.getDiffX(); cRho = rho.pixFlt(u); // (-1 / 1)
				sumRho += cRho; sumRhoX -= cRho; sumRhoY += cRho;  sumRhoXY -= cRho; sumRhoX2 += cRho; sumRhoY2 += cRho;
				u -= rho.getDiffY(); cRho = rho.pixFlt(u); // (-1 / 0)
				sumRho += cRho; sumRhoX -= cRho; sumRhoX2 += cRho;
				u -= rho.getDiffY(); cRho = rho.pixFlt(u); // (-1 / -1)
				sumRho += cRho; sumRhoX -= cRho; sumRhoY -= cRho;  sumRhoXY += cRho; sumRhoX2 += cRho; sumRhoY2 += cRho;
				u += rho.getDiffX(); cRho = rho.pixFlt(u); // (0 / -1)
				sumRho += cRho; sumRhoY -= cRho;  sumRhoY2 += cRho;
				u += rho.getDiffX(); cRho = rho.pixFlt(u); // (1 / -1)
				sumRho += cRho; sumRhoX += cRho; sumRhoY -= cRho;  sumRhoXY -= cRho; sumRhoX2 += cRho; sumRhoY2 += cRho;

				double oneByThree = 1.0 / 3.0;
				double oneBySix = 1.0 / 6.0;

				double a0 = sumRho / 1.8 - oneByThree * (sumRhoX2 + sumRhoY2);
				double a1 = oneBySix *  sumRhoX;
				double a2 = oneBySix *  sumRhoY;
				double a3 = 0.25 *  sumRhoXY;
				double a4 = -oneByThree * sumRho + 0.5 * sumRhoX2;
				double a5 = -oneByThree * sumRho + 0.5 * sumRhoY2;
   
				double det = 4.0 * a4 * a5 - a3 * a3;
				if (fabs(det) > FLT_EPSILON)
				{
					det = 1.0 / det;
					double dx = (-2.0 * a5 * a1 + a3 * a2)       * det;
					double dy = (  a3 * a1      - 2.0 * a4 * a2) * det;
					if ((fabs(dx) < 1.0) && (fabs(dy) < 1.0))
					{
						dx *= this->coordFactor;
						dy *= this->coordFactor;
						X += dx;
						Y += dy;
					}
				}
			}

			this->searchPoints[j].x = X * double(this->searchBufferFactors[j]);
			this->searchPoints[j].y = Y * double(this->searchBufferFactors[j]);
			double sig = (this->matchingPars.getSigmaGeom()> 0)? this->matchingPars.getSigmaGeom() * this->coordFactor : 0.5 * this->coordFactor;
			this->searchPoints[j].setSX(sig);
			this->searchPoints[j].setSY(sig);
			this->searchPoints[j].setSXY(0);
		}

		if (outputMode > eMEDIUM)
		{
			ostrstream oss; oss << "rho_" << this->currentPyrLevel << "_" << j << ".tif" << ends;
			char *z = oss.str();
			CCharString filename = this->projectDir + z;
			delete [] z;
			rho.exportToTiffFile(filename.GetChar(), 0, true);
		}
	}

	if (outputMode > eMEDIUM)
	{
		ostrstream oss; oss << "lev_" << this->currentPyrLevel << ends;
		char *z = oss.str();
		dump(z, true, true, false, false, this->coordFactor);
		delete [] z;
	}
	
	if (nUsed < 1) ret = false;

	return ret;
};

//================================================================================

bool CBaristaLSM::CrossCorrelationCurrentLevelOffEpipolar(int nSteps, float maxCorr)
{
	bool ret = true;

	int l = 2*nSteps + 1;
	vector<vector<float> > currentRhos(l*l);
	vector<float> totalRhos(currentRhos.size());


	int wby2 = this->templateBuf->getWidth()  / 2;
	int hby2 = this->templateBuf->getHeight() / 2;

	for (int i = 0; i < currentRhos.size(); ++i)
	{
		currentRhos[i].resize(this->searchPoints.size());
		totalRhos[i] = 0;
	}
		
	for (int j = 0; j < this->searchPoints.size(); ++j)
	{	  
		rotateCrossCorrTemplate(j);
		int iRho = 0;

		for (int c = -nSteps; c <= nSteps; ++c)
		{
			int x = (int) floor(searchPoints[j].x / this->coordFactor - wby2 + 0.5) + c;
			for (int r = -nSteps; r <= nSteps; ++r)
			{
				int y = (int) floor(searchPoints[j].y / this->coordFactor - hby2 + 0.5) + r;
				currentRhos[iRho][j] = crossCorr(x, y, j);
				totalRhos[iRho] += currentRhos[iRho][j];
				++iRho;
			}
		}
	}


	float maxRho = -2;
	int iMax = -1;

	for (int i = 0; i < totalRhos.size(); ++i)
	{
		totalRhos[i] /= (float) searchPoints.size();

		if (totalRhos[i] > maxRho)
		{
			maxRho = totalRhos[i];
			iMax = i;
		}
	}

	for (int j = 0; j < this->searchPoints.size(); ++j)
	{
		int cMax = iMax / (2*nSteps + 1) - nSteps;
		int rMax = iMax % (2*nSteps + 1) - nSteps;

		this->searchPoints[j].x += double(cMax) * this->coordFactor;
		this->searchPoints[j].y += double(rMax) * this->coordFactor;
		double sig = (this->matchingPars.getSigmaGeom()> 0)? this->matchingPars.getSigmaGeom() * this->coordFactor : 0.5 * this->coordFactor;
		this->searchPoints[j].setSX(sig);
		this->searchPoints[j].setSY(sig);
		this->searchPoints[j].setSXY(0);

		this->rhoVec = currentRhos[iMax];
	}

	if (outputMode > eMEDIUM)
	{
		ostrstream oss; oss << "lev_" << this->currentPyrLevel << ends;
		char *z = oss.str();
		dump(z, true, true, false, false, this->coordFactor);
		delete [] z;
	}
	
	if (maxRho < maxCorr) ret = false;

	return ret;
};

//================================================================================

void CBaristaLSM::deleteSearchElement(int index)
{
	if (index < this->searchBuffers.size())
	{
		delete this->searchBuffers[index];
		delete this->transformedSearchBuffers[index];
		delete this->templateToSearchTrafos[index];

		for (unsigned int j = index + 1; j < this->searchBuffers.size(); ++j)
		{
			this->transformedSearchBuffers[j - 1] = this->transformedSearchBuffers[j] ;
			this->templateToSearchTrafos[j - 1]   = this->templateToSearchTrafos[j];	
			this->searchBuffers[j - 1] = this->searchBuffers[j];
			this->searchPoints[j - 1]  = this->searchPoints[j];
			this->searchImgs[j - 1]    = this->searchImgs[j];
			this->rhoVec[j - 1]        = this->rhoVec[j];
			this->pminVec[j - 1]       = this->pminVec[j];
			this->pmaxVec[j - 1]       = this->pmaxVec[j];
		}
		this->transformedSearchBuffers.pop_back();
		this->templateToSearchTrafos.pop_back();	
		this->searchBuffers.pop_back();
		this->searchPoints.pop_back();
		this->searchImgs.pop_back();
		this->rhoVec.pop_back();
		this->pminVec.pop_back();
		this->pmaxVec.pop_back();
	}
};
	
//================================================================================
  
bool CBaristaLSM::MatchWithLSM()
{
	bool ret = this->CrossCorrelationAllLevels();
	if (ret) 
	{
		vector<CXYPoint> sPt(this->searchPoints);
		CXYZPoint oPt(this->ObjPoint);
		vector<float> rV(this->rhoVec);

		if (!this->LSM())
		{
			if (this->templateImg->getCurrentSensorModel())
			{
				this->searchPoints = sPt;
				
				this->rhoVec = rV;
				ret = this->setResultsToBaristaProject(0.5);
			}
			else ret = false;
		}
	}

	return ret;
};

//================================================================================

bool CBaristaLSM::CrossCorrelationAllLevels()
{
	if (outputMode > eMEDIUM)
	{
		dump("orig", true, true, false, false, this->coordFactor);
	}

	vector<bool> isUsed;
	float corr = this->matchingPars.getRhoMin();
	if (this->currentPyrLevel > 0) corr = 0.2;
	bool ret;
	if (this->matchingPars.getSigmaGeom() > 0.0) ret = CrossCorrelationCurrentLevel(corr, isUsed);
	else  ret = CrossCorrelationCurrentLevelArea(corr, isUsed);

	while (ret && this->currentPyrLevel > 0)
	{
		ret = MoveToNextLevel();
		if (currentPyrLevel > 0) corr = 0.2;
		else corr = this->matchingPars.getRhoMin();
		if (ret)
		{
			if (this->matchingPars.getSigmaGeom() > 0.0)  ret = CrossCorrelationCurrentLevel(corr, isUsed);
			else  ret = CrossCorrelationCurrentLevelArea(corr, isUsed);
		}
	}



	if (ret) 
	{
		for (int i = 0; i < this->searchBuffers.size(); ++i)
		{
			if (!isUsed[i])
			{
				this->deleteSearchElement(i);
				--i;
			}
			else
			{	
				Matrix pars;
				templateToSearchTrafos[i]->get(pars, this->refPoint);
				pars.element(2,0) = this->searchPoints[i].x / double(this->searchBufferFactors[i]);
				pars.element(5,0) = this->searchPoints[i].y / double(this->searchBufferFactors[i]);
				templateToSearchTrafos[i]->set(pars, this->refPoint);
			}
		}

		if (this->searchBuffers.size() < 1) ret = false;
		else if (this->matchingPars.getSigmaGeom() > 0.0) ret = intersect(false);
	}

	return ret;
};
	  
//================================================================================

bool CBaristaLSM::intersect(bool storeQxx)
{
	Matrix VarCovar;

	bool ret = true;
	double dVtPV = 0;
	double vtpv = 0; 
	double vtpvOld = FLT_MAX; 
	int it = 0;

	do
	{
		++it;
		int nObsPoints = 1;

		Matrix Ai, l(1,2), Fi,Bi;
		l.element(0,0) = this->refPoint.x;
		l.element(0,1) = this->refPoint.y;

		this->templateImg->getCurrentSensorModel()->Fi(Fi, l, ObjPoint);
		this->templateImg->getCurrentSensorModel()->Bi(Bi, l, ObjPoint);
		this->templateImg->getCurrentSensorModel()->AiPoint(Ai, l, ObjPoint);
		Matrix P = Bi * this->refPoint.getCovar() * Bi.t();
		P = P.i();
		Matrix nPt = Ai.t() * P;
		Matrix t = nPt * Fi;
		Matrix N = nPt * Ai;
		vtpv = Fi.element(0,0) * Fi.element(0,0) + Fi.element(1,0)  * Fi.element(1,0);

		for (int i = 0; i < searchImgs.size(); i++)
		{
			if (this->rhoVec[i] > this->rhoMin)
			{
				l.element(0,0) = this->searchPoints[i].x;
				l.element(0,1) = this->searchPoints[i].y;
				this->searchImgs[i]->getCurrentSensorModel()->Fi(Fi, l, ObjPoint);
				this->searchImgs[i]->getCurrentSensorModel()->Bi(Bi, l, ObjPoint);
				this->searchImgs[i]->getCurrentSensorModel()->AiPoint(Ai, l, ObjPoint);
				P = Bi * this->searchPoints[i].getCovar() * Bi.t();
				P = P.i();
				nPt = Ai.t() * P;
				t += nPt * Fi;
				N += nPt * Ai;	
				++nObsPoints;
				vtpv += Fi.element(0,0) * Fi.element(0,0) + Fi.element(1,0)  * Fi.element(1,0);
			}
		}

		if (nObsPoints < 2) ret = false;
		else
		{
			Try
			{
				VarCovar = N.i();	
				t = -VarCovar * t;
				ObjPoint.x += t.element(0,0);
				ObjPoint.y += t.element(1,0);
				ObjPoint.z += t.element(2,0);


				dVtPV = vtpv + vtpvOld;
				if (dVtPV > FLT_EPSILON)
				{
					dVtPV = (vtpv - vtpvOld) / dVtPV;
					vtpvOld = vtpv;
				}

			}
			CatchAll
			{
				ret = false;
			}
		}

	} while ((fabs(dVtPV) > 0.0001) && (fabs(vtpv) > 0.001) && ret && (it < 20));

	if (ret && storeQxx) 
	{
		VarCovar *= 0.25f;
		this->ObjPoint.setCovariance(VarCovar);
	}

	return ret;
};

//================================================================================

bool CBaristaLSM::MoveToNextLevel()
{
	this->currentPyrLevel--;
	
	this->coordFactor /= 2.0;
	this->coordOffset = (this->coordFactor - 1.0) * 0.5;	

	int x = floor(this->refPoint.x / coordFactor + 0.5) - this->templateBuf->getWidth()  / 2;
	int y = floor(this->refPoint.y / coordFactor + 0.5) - this->templateBuf->getHeight() / 2;

	CHugeImage *himg = templateImg->getHugeImage();
		
	this->templateBuf->setOffset(x, y);
	this->temp0->setOffset(x - this->templateBuf->getWidth()  / 2, y - this->templateBuf->getWidth()  / 2);

	bool ret = himg->getRect(*(this->temp0), this->currentPyrLevel) && 
		       himg->getRect(*(this->templateBuf), this->currentPyrLevel);

	if (this->WallisFilter)
	{
		this->WallisFilter->applyTransferFunction(*(this->temp0), *(this->temp0), this->temp0->getHeight(), this->temp0->getWidth(), int(this->coordFactor), true);
		this->WallisFilter->applyTransferFunction(*(this->templateBuf), *(this->templateBuf), this->templateBuf->getHeight(), this->templateBuf->getWidth(), int(this->coordFactor), true);
	}

	float lMax = 0;

	for (int i = 0; i < this->pminVec.size(); ++i)
	{
		double l = this->pmaxVec[i].getDistance(this->pminVec[i]);
		if (l > lMax) lMax = l;
	}

	if (ret)
	{
		this->templateBuf->SetToIntensity();
		this->temp0->SetToIntensity();
		initTemplateMeanVariance();

		int wT = this->templateBuf->getWidth();
		int hT = this->templateBuf->getWidth();

		for (int i = 0; i < this->searchBuffers.size(); ++i)
		{
			this->transformedSearchBuffers[i]->setOffset(x, y);
			Matrix pars;
			templateToSearchTrafos[i]->get(pars, this->refPoint);
			pars.element(2,0) = this->searchPoints[i].x;
			pars.element(5,0) = this->searchPoints[i].y;
			templateToSearchTrafos[i]->set(pars, this->refPoint);

			C2DPoint diff = coordFactor * 10.0 * (this->pmaxVec[i] - this->pminVec[i]) / lMax;
			this->pminVec[i] = this->searchPoints[i] - diff;
			this->pmaxVec[i] = this->searchPoints[i] + diff;

			himg = this->searchImgs[i]->getHugeImage();

			int levWidth  = himg->getWidth()  / (int)  this->coordFactor;
			int levHeight = himg->getHeight() / (int)  this->coordFactor;

			int xmin, ymin, w, h;

			if (getSearchWin(this->pminVec[i], this->pmaxVec[i] , levWidth, levHeight, xmin, ymin, w, h, this->searchBufferFactors[i]))
			{
				ret &= initSearchWin(this->searchImgs[i], this->searchBuffers[i], xmin, ymin, w, h, this->searchBufferFactors[i]);
				this->pminVec[i] /= coordFactor;
				this->pmaxVec[i] /= coordFactor;
			}
			else
			{
				this->deleteSearchElement(i);
				--i;
			}
		}
	}

	return ret;
};
	 
//================================================================================

bool CBaristaLSM::rotateCrossCorrTemplate(int SearchIndex)
{
	CTrans2D *trans = templateToSearchTrafos[SearchIndex]->clone();

	Matrix pars;
	C2DPoint rf;

	trans->get(pars, rf);
	rf /= this->coordFactor;

	pars.element(2,0) = rf.x;// + this->templateBuf->getOffsetX() - this->temp0->getOffsetX();
	pars.element(5,0) = rf.y;// + this->templateBuf->getOffsetY() - this->temp0->getOffsetY();
	trans->set(pars, rf);
	trans->invert();

	this->templateBuf->resample(*(this->temp0), *trans, eBicubic);

	initTemplateMeanVariance();

	delete trans;

	return true;
};

//================================================================================
