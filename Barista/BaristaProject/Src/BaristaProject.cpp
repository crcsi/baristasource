#include "BaristaProject.h"
#include "ProtocolHandler.h"
#include "StructuredFile.h"
#include "OmegaPhiKappaRotation.h"

#include "TiffFile.h"
#include "ASCIIDEMFile.h"
#include "G3DFile.h"
#include "ZImage.h"
#include "GDALFile.h"
#include "PushbroomScanner.h"
#include "OrbitObservations.h"
#include "SatelliteDataImporter.h"
#include "StatusBarHandler.h"
#include "AlosReader.h"
#include "EntityDrawing.h"
#include "Trans3DFactory.h"

#include "SystemUtilities.h"

#include "Icons.h"
#include "LookupTableHelper.h"
#include "histogram.h"

#include "GCPMatcher.h"
#include "GCPDataBasePoint.h"
#include "ImageImporter.h"
#include <algorithm>
#include <time.h>

//addition by M Awrangjeb on 18 May 2009
#include "doubles.h"
//#include "BaristaProjectHelper.h"

#define N_DRAWING_SETTINGS 2
#define INDEX_LINE_SETTINGS 0
#define INDEX_BUILDING_SETTINGS 1
using namespace std;

CBaristaProject::CBaristaProject(void) : controlPoints(0),activeDEM(0),xyzMonoplottedPoints(eMONOPLOTTED),xyzGeoreferencedPoints(eGEOREFERENCED),
	currentBuildingIndex(-1),drawingSettings(N_DRAWING_SETTINGS),
	lblType(lOn), pntType(pCross), resType(rImgxy), //addition
	 dotWidth(1), lnWidth(1), showProjPoint(true), showScale(false), scaleValue(1.0), gsdValue(0.0),//addition
	 xyzGCPchipPoints(eGCPCHIP)
{
	for (int i = 0; i < 3; ++i)
	{
		 lColor[i] = BARISTA_GREEN[i];
		 pColor[i] = BARISTA_GREEN[i];
		 rColor[i] = BARISTA_MAGENTA[i]; 
	}
    selectedCVImage = NULL;
	this->images.setParent(this);
	this->orbits.setParent(this);
	this->cameras.setParent(this);
	this->cameraMountings.setParent(this);
	this->xyzGeoreferencedPoints.setFileName("GeoreferencedPoints");
	this->xyzMonoplottedPoints.setFileName("MonoplottedPoints");
	
	//addition by M. Awrangjeb on 18 May 2009
	/*if (this->getNumberOfImages()>0)
		double num = this->getImageAt(0)->getApproxGSD(100);
		double d = floor(num);
		double diff = num-d;
		if (diff >= 0.5)
			this->gsdValue = d+1;
		else
			this->gsdValue = d;		
	else
		this->gsdValue = 0;
		*/
	
	//GCP database
	this->gcpDB.clear();
	this->chipDbFileName = "";
	this->gcpPoint.setGCPPoint2DArrayClear();
	this->xyzGCPchipPoints.setFileName("GCPchipPoints");
	this->xyzGCPchipPoints.setCoordinateType(eGEOCENTRIC);
}


CBaristaProject::~CBaristaProject(void)
{
	if (controlPoints) delete controlPoints;
}




void CBaristaProject::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
	token->setValue("mean project height", this->meanHeight);

	CStructuredFileSection* newSection; 

    // restore my children objects
    newSection = pStructuredFileSection->addChild();
    this->images.serializeStore(newSection);

    newSection = pStructuredFileSection->addChild();
    this->xyzArrays.serializeStore(newSection);

    newSection = pStructuredFileSection->addChild();
    this->xyzResidulaArray.serializeStore(newSection);

    newSection = pStructuredFileSection->addChild();
	this->dems.serializeStore(newSection);

    newSection = pStructuredFileSection->addChild();
	this->alsDataSets.serializeStore(newSection);

	newSection = pStructuredFileSection->addChild();
	this->orbits.serializeStore(newSection);

	newSection = pStructuredFileSection->addChild();
	this->cameras.serializeStore(newSection);

	newSection = pStructuredFileSection->addChild();
	this->cameraMountings.serializeStore(newSection);

	newSection = pStructuredFileSection->addChild();
	this->xyzpolylines.serializeStore(newSection);
	//addition >>
	//this->xyzBheights.serializeStore(newSection);
	//<< addition
#ifdef WIN32
	newSection = pStructuredFileSection->addChild();
	this->buildings.serializeStore(newSection);
#endif

    newSection = pStructuredFileSection->addChild();
    this->xyzMonoplottedPoints.serializeStore(newSection);

    newSection = pStructuredFileSection->addChild();
    this->xyzGeoreferencedPoints.serializeStore(newSection);

	//addition
	//newSection = pStructuredFileSection->addChild();
    //this->xyzGCPchipPoints.serializeStore(newSection);
	//<<addition

    newSection = pStructuredFileSection->addChild();
    this->matchingParameters.serializeStore(newSection);


	for (int i=0; i < this->drawingSettings.size(); i++)
	{
		newSection = pStructuredFileSection->addChild();
		this->drawingSettings[i].serializeStore(newSection);
	}

}

void CBaristaProject::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
        CCharString key = token->getKey();

		if (key.CompareNoCase("mean project height"))
			this->meanHeight = token->getDoubleValue();
    }

	bool hasXYZPointArrays = false;

	int nDrawingSettings = 0;
    // restore my children objects
	for(int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
	{
		CStructuredFileSection* pSection = pStructuredFileSection->getChildren()->GetAt(i);
        
		if(pSection->getName().CompareNoCase("CVImageArray"))
			this->images.serializeRestore(pSection);
   		else if(pSection->getName().CompareNoCase("CXYZPointArrayArray") && !hasXYZPointArrays)
		{
			this->xyzArrays.serializeRestore(pSection);
			hasXYZPointArrays = true;
		}
   		else if(pSection->getName().CompareNoCase("CXYZPointArrayArray") && hasXYZPointArrays)
		{
			this->xyzResidulaArray.serializeRestore(pSection);
		}
   		else if(pSection->getName().CompareNoCase("CVDEMArray"))
			this->dems.serializeRestore(pSection);
   		else if(pSection->getName().CompareNoCase("CVALSArray"))
			this->alsDataSets.serializeRestore(pSection);
		else if (pSection->getName().CompareNoCase("COrbitObservationsArray"))
			this->orbits.serializeRestore(pSection);
		else if (pSection->getName().CompareNoCase("CCameraArray"))
			this->cameras.serializeRestore(pSection);
		else if (pSection->getName().CompareNoCase("CCameraMountingArray"))
			this->cameraMountings.serializeRestore(pSection);

		else if (pSection->getName().CompareNoCase("CXYZPolyLineArray"))
			this->xyzpolylines.serializeRestore(pSection);
#ifdef WIN32
		else if (pSection->getName().CompareNoCase("CBuildingArray"))
			this->buildings.serializeRestore(pSection);
#endif			
		else if (pSection->getName().CompareNoCase("CXYZPointArray"))
        {
			CXYZPointArray tmpArray;
			tmpArray.serializeRestore(pSection);
			if (tmpArray.getPointType() == eMONOPLOTTED)
				this->xyzMonoplottedPoints = tmpArray;
			else if (tmpArray.getPointType() == eGEOREFERENCED)
				this->xyzGeoreferencedPoints = tmpArray;
			//addition>>
			//else if (tmpArray.getPointType() == eGCPCHIP)
			//	this->xyzGCPchipPoints = tmpArray;
			//<<addition
       }
		else if (pSection->getName().CompareNoCase("CMatchingParameters"))
			this->matchingParameters.serializeRestore(pSection);
		else if (pSection->getName().CompareNoCase("CDrawingSetting") && nDrawingSettings < N_DRAWING_SETTINGS)
        {
			this->drawingSettings[nDrawingSettings].serializeRestore(pSection);
			nDrawingSettings++;
       }

    }

	// that means we have an old project file without residuals for xyz points
	if (this->xyzResidulaArray.GetSize() == 0)
	{
		for (int i=0;  i <this->xyzArrays.GetSize(); i++)
		{
			CXYZPointArray*xyzResPoints =  this->xyzResidulaArray.Add();
			xyzResPoints->setFileName("Residuals");
			xyzResPoints->setPointType(eRESIDUAL);
			xyzResPoints->setReferenceSystem(*this->xyzArrays.GetAt(i)->getReferenceSystem());
		}
	}
	else if (this->xyzResidulaArray.GetSize() != this->xyzArrays.GetSize())
		assert(false); // we need the same size for both arrays


}

void CBaristaProject::reconnectPointers()
{
    CSerializable::reconnectPointers();
	
	this->images.reconnectPointers();
	this->orbits.reconnectPointers();
	this->cameras.reconnectPointers();
	this->cameraMountings.reconnectPointers();
}

void CBaristaProject::resetPointers()
{
    CSerializable::resetPointers();

	this->images.resetPointers();
	this->orbits.resetPointers();
	this->cameras.resetPointers();
	this->cameraMountings.resetPointers();
}


bool CBaristaProject::isModified()
{
	return this->bModified;

}

#ifdef WIN32
CBuilding *CBaristaProject::selectCurrentBuilding()
{
	return this->setSelectedBuilding(this->currentBuildingIndex);
};

CBuilding *CBaristaProject::setSelectedBuilding(int index)
{
	CBuilding *pBuilding = 0;
	this->buildings.unselectAll();

	this->currentBuildingIndex = index;
	if ((this->currentBuildingIndex >= 0) && (this->currentBuildingIndex < this->buildings.GetSize()))
	{
		pBuilding = this->buildings.GetAt(this->currentBuildingIndex);
		this->buildings.selectElement(pBuilding->getLabel(),true);
	}

	return pBuilding;
};
#endif

void CBaristaProject::writeProject(const char* pFileName)
{

	CStructuredFile structuredFile;
	CStructuredFileSection* pMainSection = structuredFile.getMainSection();
	pMainSection->setProjectPath(this->projectFileName.GetPath());

	this->serializeStore(pMainSection);

	structuredFile.writeFile(pFileName);

}

void CBaristaProject::readProject(const char* pFileName)
{
	
	if (this->listener)
		this->listener->setProgressValue(-1.0); // the progress dialog does the rest

    this->setFileName(pFileName);

	CStructuredFile structuredFile;
	CStructuredFileSection* pMainSection = structuredFile.getMainSection();
	pMainSection->setProjectPath(this->projectFileName.GetPath());

	protHandler.setDefaultDirectory(this->projectFileName.GetPath());

	structuredFile.readFile(pFileName);
	this->serializeRestore(pMainSection);

	this->reconnectPointers();
	this->resetPointers();
	this->handleManager.reset();

}




CFileName* CBaristaProject::getFileName()
{
    return &this->projectFileName;
}

void CBaristaProject::setFileName(const char* filename)
{
    this->projectFileName = filename;
}


//addition>>
// GCP Chip
CFileName* CBaristaProject::getChipDbFileName()
{
    return &this->chipDbFileName;
}

void CBaristaProject::setChipDbFileName(const char* filename)
{
    this->chipDbFileName = filename;
}

unsigned long CBaristaProject::getCurrentChipID()
{
	return this->currentChipID;
}

void CBaristaProject::setCurrentChipID(unsigned long cid)
{
	this->currentChipID = cid;
}

void CBaristaProject::setDefaultGCPvalues() // only for a newly created GCP database
{
	//set current chip id to 1
	this->setCurrentChipID(1);

	//2d point info
	CGCPData2DPoint gcp2DPoint;

	SYSTEMTIME st;
    memset(&st, 0, sizeof(SYSTEMTIME));
    GetLocalTime(&st);
	CCharString cdatestr;
	cdatestr.addInteger(st.wDay,2);
	cdatestr += "/";
	cdatestr.addInteger(st.wMonth,2);
	cdatestr += "/";
	cdatestr.addInteger(st.wYear,4);
	
 //dff
	gcp2DPoint.setDate("24/02/2010"); // mm/dd/yy
	gcp2DPoint.setQualityOfDefinition(2.5); // for ALOS PRISM
	gcp2DPoint.setPlatform("ALOS");
	gcp2DPoint.setSensor("PRISM");
	//gcp2DPoint.setDateCreation(CSystemUtilities::dateString().GetChar());
	gcp2DPoint.setDateCreation(cdatestr);
	gcp2DPoint.setViewAngle(-1.2);
	gcp2DPoint.setAccuracy(2.5);
	gcp2DPoint.setAccuracyZ(2.5);
	gcp2DPoint.setUnit("metre");
	gcp2DPoint.setSourceOfHeight("SRTM");
	gcp2DPoint.sethDatum("GDA94");
	gcp2DPoint.setvDatum("AHD");
	gcp2DPoint.setProjection("Geodetic");
	gcp2DPoint.setZone(55);
	gcp2DPoint.setepsgCode("xxxxxxxx");
	gcp2DPoint.setchipSize(256);
	gcp2DPoint.setdataProvider("GA");
	gcp2DPoint.setLicense("yyyyyyyy");
	gcp2DPoint.setComments("zzzzzzzz");
	gcp2DPoint.setnBands(1);
	gcp2DPoint.setisMonoplotted(true);
	gcp2DPoint.setX(128);
	gcp2DPoint.setY(128);
	gcp2DPoint.setSX(0.5);
	gcp2DPoint.setSY(0.5);

	//this->gcpPoint.set2DPoint(gcp2DPoint);
	this->gcpPoint.set2DPoint(gcp2DPoint);
	this->gcpPoint.setCoordinateType(eGEOCENTRIC);
	//this->gcpPoint.setDate(CSystemUtilities::dateString().GetChar());
	this->gcpPoint.setDate(cdatestr);
	this->gcpPoint.setFromGPS(true);

}

CGCPDataBasePoint CBaristaProject::getGCPpoint()
{
	return this->gcpPoint;
}

void CBaristaProject::setGCPpoint(CGCPDataBasePoint P)
{
	//clear 2d vector at gcpPoint
	this->gcpPoint.setGCPPoint2DArrayClear();
	
	CGCPData2DPoint gcp2DPoint = P.get2DPointAt(0);
	this->gcpPoint.set2DPoint(gcp2DPoint);	
}
//<<addition

vector <CGCPDataBasePoint>* CBaristaProject::getGCPdb()
{
	return &this->gcpDB;
}

vector <bool>* CBaristaProject::getChipSelectionArray()
{
	return &this->chipSelectionArray;
}

void CBaristaProject::setChipSelectionArray(vector <bool> *SelectionArray)
{
	this->chipSelectionArray.clear();
	this->chipSelectionArray = *SelectionArray;
}

void CBaristaProject::addToChipSelectionArray(bool flag)
{
	this->chipSelectionArray.push_back(flag);
}

/** 
 * gets a new xyzpointarray name, increments if it exists already
 */
CCharString CBaristaProject::getNewXYZPointArrayName(CCharString name)
{
    int sameNameCount = 0;
    CCharString uniquename = name;

    for ( int i = 0; i < this->getNumberOfXYZPointArrays(); i++)
    {
		if (this->xyzArrays.getPointArray(uniquename))
        {
            sameNameCount++;
            uniquename = name;
            CLabel number = "";
            number += sameNameCount;
            uniquename += number.GetChar();
        }
    }

    return uniquename;
}

/** 
 * sets xyzpoints to control points
 */
void CBaristaProject::setControlPoints(CXYZPointArray* points)
{
	if (controlPoints) delete controlPoints;
	controlPoints = 0;

    for (int i = 0; i < this->xyzArrays.GetSize(); i++)
    {
		CXYZPointArray *pts = this->xyzArrays.GetAt(i);
		if (pts == points)
		{
			pts->setControl(true);
			this->controlPoints = new CVControl(pts,this->xyzResidulaArray.GetAt(i));
		}
		else if (pts->isControl()) pts->setControl(false);
    }
}

/** 
 * unsets xyzpoints to control points
 */
void CBaristaProject::unsetControlPoints(CXYZPointArray* points)
{
	if (controlPoints) delete controlPoints;
	controlPoints = 0;
	points->setControl(false);
}


void CBaristaProject::setCheckPoints(CXYZPointArray* points)
{
    for (int i = 0; i < this->xyzArrays.GetSize(); i++)
    {
		CXYZPointArray *pts = this->xyzArrays.GetAt(i);
		if (pts == points)
		{
			pts->setCheckPoint(true);
		}
		else if (pts->isCheckPoint()) pts->setCheckPoint(false);
    }
}

void CBaristaProject::unsetCheckPoints(CXYZPointArray* points)
{
	points->setCheckPoint(false);
}

CXYZPointArray* CBaristaProject::getCheckPoints() const
{
    for (int i = 0; i < this->xyzArrays.GetSize(); i++)
    {
		CXYZPointArray *pts = this->xyzArrays.GetAt(i);
		if (pts->isCheckPoint()) 
			return pts;
    }

	return NULL;
}


/** 
 * sets DEM to Active DEM
 */
void CBaristaProject::setActiveDEM(CVDEM* dem)
{
	this->unsetActiveDEM();
	dem->setDEMtoActive(true);
	this->activeDEM = dem;
}

/** 
 * unsets Active DEM
 */
void CBaristaProject::unsetActiveDEM()
{
	this->activeDEM = 0;

	for( int i = 0; i < this->getNumberOfDEMs(); i++)
	{
		this->getDEMAt(i)->setDEMtoActive(false);
	}
}


/** 
 * deletes xyzpoints points (including check if it is control points)
 */
void CBaristaProject::deleteXYZPoints(CXYZPointArray* points)
{
	if( points->isControl() ) this->unsetControlPoints(points);

	int todelete = this->xyzArrays.getbyName(points->getFileName()->GetChar());

	if (todelete != -1) 
	{
		this->xyzArrays.RemoveAt(todelete, 1);
		this->xyzResidulaArray.RemoveAt(todelete, 1);
	}
}

CXYZPointArray* CBaristaProject::addXYZPoints(const CFileName &filename,  CReferenceSystem refSys, bool checkUnique, bool read,XyzPointType type)
{
	CXYZPointArray* xyzpoints = this->xyzArrays.Add();
	CXYZPointArray* xyzResPoints = this->xyzResidulaArray.Add();
	xyzResPoints->setFileName("Residuals");
	xyzResPoints->setPointType(eRESIDUAL);

	xyzpoints->setPointType(type);

	if (xyzpoints != NULL)
	{
		if (!checkUnique) xyzpoints->setFileName(filename.GetChar());
		else xyzpoints->setFileName(this->getNewXYZPointArrayName(filename).GetChar());
	
		if (read) xyzpoints->readXYZPointTextFile(filename);
		if (xyzpoints->GetSize() > 0)
		{
			CXYZPoint *p = xyzpoints->getAt(0);
			
			if (refSys.getCoordinateType() == eGEOGRAPHIC)
			{
				double lat = p->x;
				double lon = p->y;
				refSys.setUTMZone(lat, lon);
			}
			else if (refSys.getCoordinateType() == eGEOCENTRIC)
			{
				C3DPoint pt;
				refSys.geocentricToGeographic(pt,*p);
				double lat = pt.x;
				double lon = pt.y;
				refSys.setUTMZone(lat, lon);
			}
		}
		xyzpoints->setReferenceSystem(refSys);
		xyzResPoints->setReferenceSystem(refSys);
	}
	
	return xyzpoints;

}


CVDEM* CBaristaProject::addDEM(CG3DFile& imageFile, const CReferenceSystem &refSys)
{
	CVDEM* vdem = this->addDEM(imageFile.getFilename());

	if (vdem != NULL)
	{
		vdem->setReferenceSystem(refSys);
		C2DPoint gridS(imageFile.getgridX(), imageFile.getgridY());
		C2DPoint shift(imageFile.getminX(),  imageFile.getminY() + (imageFile.getHeight() - 1) * gridS.y);
		vdem->getDEM()->setTFW(shift, gridS);
		vdem->getDEM()->init();
	}

	return vdem;
}


CVALS *CBaristaProject::addALSStrips(const CFileName& filename, eALSFormatType eFormat, eALSPulseModeType ePulseType,
							         const CReferenceSystem& refSys, const C2DPoint &offset)
{
	CVALS *als = this->alsDataSets.Add();

	if (als)
	{
		if (!als->initALSStrip(filename, eFormat, ePulseType, refSys, offset))
		{
			this->deleteALS(als);
			als = 0;
		}
	}

	return als;
};

CVALS *CBaristaProject::addALS()
{
	CVALS *als = this->alsDataSets.Add();
	return als;
}

CVALS *CBaristaProject::addALSTiles(const CFileName& filename, eALSFormatType eFormat, eALSPulseModeType ePulseType,
							         const CReferenceSystem& refSys, const C2DPoint &offset, const C2DPoint &delta)
{
	CVALS *als = this->alsDataSets.Add();

	if (als)
	{
		if (!als->initALSTile(filename, eFormat, ePulseType, refSys, offset, delta))
		{
			this->deleteALS(als);
			als = 0;
		}
	}

	return als;
};

CVDEM* CBaristaProject::addDEM(CASCIIDEMFile& imageFile, const CReferenceSystem& refSys)
{
	CVDEM* vdem = this->addDEM(imageFile.getFilename());

	if (vdem != NULL)
	{
		vdem->setReferenceSystem(refSys);

		if ( this->getNumberOfDEMs() == 1 ) this->setActiveDEM(vdem);
	}
	
	return vdem;
}

CVDEM* CBaristaProject::addDEM(CGDALFile& imageFile, const CReferenceSystem& refSys)
{
	CVDEM* vdem = this->addDEM(imageFile.getFilename());

	if (vdem && imageFile.getTFW())
	{
		const CTFW *tfw = imageFile.getTFW();
		vdem->getDEM()->setTFW(*tfw);
		vdem->setReferenceSystem(refSys);

		if ( this->getNumberOfDEMs() == 1 ) this->setActiveDEM(vdem);
	}

	return vdem;
}


CVDEM* CBaristaProject::addDEM(const CFileName& filename)
{
	CVDEM* vdem = this->dems.Add();

	if (vdem != NULL)
	{
		vdem->setFileName(filename);
		vdem->getDEM()->setFileName(filename.GetChar());
		if ( this->getNumberOfDEMs() == 1 ) this->setActiveDEM(vdem);
	}

	return vdem;
}
	
CVImage *CBaristaProject::addImage(const CFileName& filename, bool open)
{
	CVImage *newImage = this->images.Add();

	newImage->setName(filename);

	if (open) newImage->getHugeImage()->open(filename.GetChar(), true);

	return newImage;
}


void CBaristaProject::deleteImage(CVImage *image)
{
	int index = -1;
	for (int i = 0; (i < this->images.GetSize()) && (index < 0); ++i)
	{
		if (this->images.GetAt(i) == image) index = i;
	}

	if (index >= 0) 
	{
		image->getHugeImage()->closeHugeFile();
		this->deleteSensorModelofImage(image,image->getPushbroom()); // just to delete the orbit observations
		this->deleteSensorModelofImage(image,image->getFrameCamera()); // just to delete the camera/mounting rotation
		image->goingToBeDeleted();
		deleteImageAt(index);
	}
};

void CBaristaProject::deleteImageAt(int index)
{
	if (this->images.GetAt(index) == this->selectedCVImage) this->selectedCVImage = 0;
	this->images.RemoveAt(index, 1);
};
	
void CBaristaProject::deleteALS(CVALS *als)
{
	int tokill = this->alsDataSets.getbyName(als->getFileNameChar());

	if (tokill >= 0) this->alsDataSets.RemoveAt(tokill, 1);
};

void CBaristaProject::deleteALSAt(int index)
{
	this->alsDataSets.RemoveAt(index, 1);
};

bool CBaristaProject::renameImage(CVImage *image, const char *newName)
{
	CCharString cname(newName);
	bool ret = true;
	if ( strlen(newName) == 0) ret = false;
	else if (strcmp(newName, image->getFileNameChar()) == 0) ret = false;
	else if ( this->images.getImagebyName(&cname) != 0) ret = false;
	else
	{
		image->setName(newName);
	}

	return ret;
}

bool CBaristaProject::renameDEM(CVDEM *dem, const char *newName)
{
	bool ret = true;
	if ( strlen(newName) == 0) ret = false;
	else if (strcmp(newName, dem->getFileName()->GetChar()) == 0) ret = false;
	else if ( this->dems.getDEMbyName(newName) != 0) ret = false;
	else
	{
		dem->setFileName(newName);
	}

	return ret;
};
	
bool CBaristaProject::renameALS(CVALS *als, const char *newName)
{
	bool ret = true;
	if ( strlen(newName) == 0) ret = false;
	else if (strcmp(newName, als->getFileNameChar()) == 0) ret = false;
	else if ( this->alsDataSets.getALSbyName(newName) != 0) ret = false;
	else
	{
		als->setFileName(newName);
	}

	return ret;
};

bool CBaristaProject::renameXYZPointArray(CXYZPointArray *points, const char *newName)
{
	bool ret = true;
	if ( strlen(newName) == 0) ret = false;
	else if (strcmp(newName, points->getFileName()->GetChar()) == 0) ret = false;
	else if ( this->xyzArrays.getbyName(newName) != -1) ret = false;
	else
	{
		points->setFileName(newName);
	}

	return ret;
};

void CBaristaProject::deleteDEM(CVDEM *dem)
{
	
	if (dem == this->activeDEM)
		this->activeDEM = NULL;

	int tokill = this->dems.getbyName(dem->getFileName()->GetChar());

	if (tokill >= 0) this->dems.RemoveAt(tokill, 1);
};

void CBaristaProject::RPCBiasCorrectionNone()
{
    for (int i = 0; i < this->images.GetSize(); i++)
    {  
		this->images.GetAt(i)->getRPC()->fixAll();
    }
}

void CBaristaProject::RPCBiasCorrectionShift()
{
    for (int i = 0; i < this->images.GetSize(); i++)
    {    
		this->images.GetAt(i)->getRPC()->freeShift();     
    }

}


void CBaristaProject::RPCBiasCorrectionShiftDrift()
{
	for (int i = 0; i < this->images.GetSize(); i++)
    {      
		this->images.GetAt(i)->getRPC()->freeShiftDrift();   
    }
}


void CBaristaProject::RPCBiasCorrectionAffine()
{
    for (int i = 0; i < this->images.GetSize(); i++)
    {   
		this->images.GetAt(i)->getRPC()->freeAffine();     
    }
}


void CBaristaProject::RPCRegenerateBiasCorrected()
{
	for (int i = 0; i < this->images.GetSize(); i++)
    {
		CVImage* vimage = images.GetAt(i);

		if (vimage->getRPC()->gethasValues())
		{
			vimage->getRPC()->reformulate();

			// get original name, attach "_bc" and set it
			CFileName oldname = vimage->oriRPCFileName();
			CFileName bcname = oldname.GetFileName().GetChar();
			bcname += "_bc";
			vimage->getRPC()->setFileName(bcname.GetChar());
		}
    }
}

void CBaristaProject::resetRPCtoOriginal()
{
    for (int i = 0; i < this->images.GetSize(); i++)
    {
		if ( this->images.GetAt(i)->getRPC()->gethasValues())
			this->images.GetAt(i)->resetRPC();
    }
}


COrbitObservations* CBaristaProject::addOrbitObservation(SplineModelType type)
{
	COrbitObservations* orbit = this->orbits.Add(type);
	return orbit;

}

void CBaristaProject::deleteOrbitObservation(COrbitObservations* orbit)
{
	if (orbit)
	{
		int index = this->orbits.indexOf(orbit);

		if (index >= 0)
		{
			orbit->goingToBeDeleted();	
			this->orbits.RemoveAt(index,1);
		}
	}

}


void CBaristaProject::deleteOrbitObservation(CFileName& filename)
{
	
	for (int i =this->orbits.GetSize() -1; i >= 0 ; i--)
	{
		COrbitObservations* o = this->orbits.GetAt(i);
		if (filename.CompareNoCase(o->getFileNameChar()))
		{
			o->goingToBeDeleted();		
			this->orbits.RemoveAt(i,1);
		}
	}

}

bool CBaristaProject::renameOrbitObservation(COrbitObservations* orbit, const char *newName)
{
	bool ret = true;
	if ( strlen(newName) == 0) ret = false;
	else if (strcmp(newName, orbit->getFileNameChar()) == 0) ret = false;
	else if ( this->orbits.indexOf(orbit) < 0) ret = false;
	else
	{
		orbit->setFileName(newName);
	}

	return ret;
}


bool CBaristaProject::setCurrentSensorModelofImage(CVImage *image, eImageSensorModel newmodel)
{
	image->setCurrentSensorModel(newmodel);
	return true;
}

void CBaristaProject::setLblColor(int * color) 
{
	for (int i = 0; i < 3; ++i) this->lColor[i] = color[i];
};

void CBaristaProject::setPntColor(int * color) 
{
	for (int i = 0; i < 3; ++i) this->pColor[i] = color[i];
};

void CBaristaProject::setResColor(int * color)
{
	for (int i = 0; i < 3; ++i) this->rColor[i] = color[i];
};
	
int CBaristaProject::getNumberOfImagesWithFrameCam() const
{
	int N = 0; 

	for (int i = 0; i < this->images.GetSize(); ++i)
	{
		if (this->images.GetAt(i)->getFrameCamera()->gethasValues()) ++N;
	}

	return N;
};


bool CBaristaProject::deleteSensorModelofImage(CVImage *image, CSensorModel *deletemodel)
{
	// check if we have to delete the orbit as well
	if (deletemodel->isa("CPushBroomScanner"))
	{
		CPushBroomScanner* scanner = (CPushBroomScanner*) deletemodel;
		scanner->goingToBeDeleted();
		int countPath = 0;
		int countAttitudes = 0;
		int countCamera = 0;
		int countCameraMounting = 0;
		for (int i=0; i < this->images.GetSize(); i++)
		{
			CVImage * images = this->images.GetAt(i);
			CPushBroomScanner* imageScanner = images->getPushbroom();
			if (scanner->getOrbitPath() == imageScanner->getOrbitPath())
				countPath++;
			if (scanner->getOrbitAttitudes()== imageScanner->getOrbitAttitudes())
				countAttitudes++;
			if (scanner->getCamera()== imageScanner->getCamera())
				countCamera++;
			if (scanner->getCameraMounting() == imageScanner->getCameraMounting())
				countCameraMounting++;

		}
		if (countPath == 1)
		{
			for (int i=0; i< this->orbits.GetSize(); i++)
				if (this->orbits.GetAt(i)->getCurrentSensorModel() == scanner->getOrbitPath())
					this->deleteOrbitObservation(this->orbits.GetAt(i));
		}
		if (countAttitudes == 1)
		{
			for (int i=0; i< this->orbits.GetSize(); i++)		
				if (this->orbits.GetAt(i)->getCurrentSensorModel() == scanner->getOrbitAttitudes())
					this->deleteOrbitObservation(this->orbits.GetAt(i));
		}
		if (countCamera == 1)
		{
			this->deleteCamera(scanner->getCamera());
		}
		if (countCameraMounting == 1)
		{
			this->deleteCameraMounting(scanner->getCameraMounting());
		}


	}
	else if (deletemodel->isa("CRPC"))
	{
		image->eraseRPC();
	}
	else if (deletemodel->isa("CFrameCameraModel"))
	{
		CFrameCameraModel* model = (CFrameCameraModel*) deletemodel;
		int countCamera = 0;
		int countCameraMounting = 0;
		for (int i=0; i < this->images.GetSize(); i++)
		{
			CVImage * images = this->images.GetAt(i);
			CFrameCameraModel* imageCamera = images->getFrameCamera();
			if (model->getCamera()== imageCamera->getCamera())
				countCamera++;
			if (model->getCameraMounting() == imageCamera->getCameraMounting())
				countCameraMounting++;

		}
		if (countCamera == 1)
		{
			this->deleteCamera(model->getCamera());
		}
		if (countCameraMounting == 1)
		{
			this->deleteCameraMounting(model->getCameraMounting());
		}


	}


	image->resetSensorModel(deletemodel);

	return true;
}



bool CBaristaProject::getFixedRotationForOrbit(CRotationMatrix& fixedMatrix,const CCharString& orbitModelName)
{
	for (int i=0; i<this->images.GetSize(); i++)
	{
		CPushBroomScanner* scanner = this->images.GetAt(i)->getPushbroom();
		
		CCharString pathName;
		if (scanner->getOrbitPath())
			pathName = *scanner->getOrbitPath()->getFileName();

		CCharString attitudeName;
		if (scanner->getOrbitAttitudes())
			attitudeName = *scanner->getOrbitAttitudes()->getFileName();

		if (orbitModelName.CompareNoCase(pathName)||
			 orbitModelName.CompareNoCase(attitudeName))
		{
			fixedMatrix = scanner->getFixedMatrix();
			return true;
		}
	}
	
	return false;
}


bool CBaristaProject::deleteSensorModelofDEM(CVDEM *dem, CSensorModel *deletemodel)
{
	dem->resetSensorModel(deletemodel);
	return true;
}

void CBaristaProject::deleteXYZPolyLines()
{
	this->removeMonoplottedLine(0,this->xyzpolylines.GetSize());
}

//addition >>
void CBaristaProject::deleteXYZHeightLines()
{
	this->removeBheightLine(0,this->xyzBheights.GetSize());
}
//<< addition

#ifdef WIN32
void CBaristaProject::deleteBuildings()
{
	this->removeMonoplottedBuilding(0,this->buildings.GetSize());

}

bool CBaristaProject::renameBuildings(const char *newName)
{
	bool ret = true;
	if ( strlen(newName) == 0) ret = false;
	else if (strcmp(newName, this->buildings.getFileName()->GetChar()) == 0) ret = false;
	else
	{
		this->buildings.setFileName(newName);
	}

	return ret;
}
#endif

bool CBaristaProject::renameXYZPolyLines(const char *newName)
{
	bool ret = true;
	if ( strlen(newName) == 0) ret = false;
	else if (strcmp(newName, this->xyzpolylines.getFileName()->GetChar()) == 0) ret = false;
	else
	{
#ifdef WIN32		
		this->buildings.setFileName(newName);
#endif		
	}

	return ret;
}


bool CBaristaProject::initProject()
{
 
	this->projectFileName.Empty();
	this->images.RemoveAll();
	this->xyzArrays.RemoveAll();
	this->xyzResidulaArray.RemoveAll();
	this->dems.RemoveAll();
	this->alsDataSets.RemoveAll();
	this->orbits.RemoveAll();
	this->cameras.RemoveAll();
	this->cameraMountings.RemoveAll();

	this->controlPoints = NULL;
	
#ifdef WIN32
	this->buildings.RemoveAll();
#endif
	
	this->xyzpolylines.RemoveAll();
	this->xyzMonoplottedPoints.RemoveAll();
	this->xyzGeoreferencedPoints.RemoveAll();
	this->activeDEM = NULL;
	this->currentBuildingIndex = -1;

	this->meanHeight = -9999.0;
	this->meanHeightMonoplotting = false;

	//addition>>
	this->gcpDB.clear();
	this->xyzGCPchipPoints.RemoveAll();
	this->gcpPoint.setGCPPoint2DArrayClear();
	this->xyzBheights.RemoveAll();
	//<<addition

	return true;
}
bool CBaristaProject::close()
{
	
	this->projectFileName.Empty();

	for (int i=0; i < this->images.GetSize(); i++)
		this->images.GetAt(i)->getPushbroom()->goingToBeDeleted();

	this->images.RemoveAll();
	this->xyzArrays.RemoveAll();
	this->xyzResidulaArray.RemoveAll();
	this->dems.RemoveAll();
	this->alsDataSets.RemoveAll();

	for (int i=0; i < this->orbits.GetSize(); i++)
	{
		this->orbits.GetAt(i)->goingToBeDeleted();
	}
	this->orbits.RemoveAll();

	for (int i=0; i < this->cameras.GetSize(); i++)
		this->cameras.GetAt(i)->goingToBeDeleted();

	this->cameras.RemoveAll();

	for (int i=0; i < this->cameraMountings.GetSize(); i++)
		this->cameraMountings.GetAt(i)->goingToBeDeleted();

	this->cameraMountings.RemoveAll();

	this->controlPoints = NULL;

#ifdef WIN32
	this->buildings.RemoveAll();
#endif	
	
	this->xyzpolylines.RemoveAll();
	this->xyzMonoplottedPoints.RemoveAll();
	this->xyzGeoreferencedPoints.RemoveAll();
	this->activeDEM = NULL;
	this->currentBuildingIndex = -1;

	this->meanHeight = -9999.0;
	this->meanHeightMonoplotting = false;

	//addition>>
	this->gcpDB.clear();
	this->xyzGCPchipPoints.RemoveAll();
	this->gcpPoint.setGCPPoint2DArrayClear();
	this->xyzBheights.RemoveAll();
	//<<addition

	return true;
}

void CBaristaProject::addGeoreferencedPoint(const CXYZPoint& point)
{
	this->xyzGeoreferencedPoints.Add(point);
}

bool CBaristaProject::addMonoplottedPoint(const CXYZPoint& point)
{
	this->xyzMonoplottedPoints.Add(point);

	return (this->xyzMonoplottedPoints.GetSize() == 1);		
}

bool CBaristaProject::addMonoplottedLine(const CXYZPolyLine& line)
{
	this->xyzpolylines.add(line);

	return  (this->xyzpolylines.GetSize()== 1);
}

// addition >>
bool CBaristaProject::addBheightLine(const CXYZPolyLine& line)
{
	this->xyzBheights.add(line);

	return  (this->xyzBheights.GetSize()== 1);
}
//<< addition

#ifdef WIN32
int CBaristaProject::addMonoplottedBuilding(const CBuilding& building,  CStatusBarHandler *handler)
{
	if (!(this->buildings.GetSize() % 20))
	{
		CFileName backupName = *this->getFileName();
		backupName = backupName.GetPath() + CSystemUtilities::dirDelimStr + backupName.GetFileName() + "_backup2." +  backupName.GetFileExt();
		if (handler) handler->setText("writing backup ..");
		this->writeProject(backupName.GetChar());
		if (handler) handler->setText("");
	}
	else if (!(this->buildings.GetSize() % 10))
	{
		CFileName backupName = *this->getFileName();
		backupName = backupName.GetPath() + CSystemUtilities::dirDelimStr + backupName.GetFileName() + "_backup1." +  backupName.GetFileExt();
		if (handler) handler->setText("writing backup ..");
		this->writeProject(backupName.GetChar());
		if (handler) handler->setText("");
	}

	
	CBuilding* pointer = this->buildings.Add(building);

	return this->buildings.indexOf(pointer);
}

void CBaristaProject::setMonoplottedBuildingReferenceSystem(const CReferenceSystem& ref)
{
	this->buildings.setReferenceSystem(ref);
}

#endif

void CBaristaProject::setMonoplottedPointReferenceSystem(const CReferenceSystem& ref)
{
	this->xyzMonoplottedPoints.setReferenceSystem(ref);
}

void CBaristaProject::setGeoreferencedPointReferenceSystem(const CReferenceSystem& ref)
{
	this->xyzGeoreferencedPoints.setReferenceSystem(ref);
}

void CBaristaProject::setMonoplottedLineReferenceSystem(const CReferenceSystem& ref)
{
	this->xyzpolylines.setReferenceSystem(ref);
}

//addition >>
void CBaristaProject::setBheightLineReferenceSystem(const CReferenceSystem& ref)
{
	this->xyzBheights.setReferenceSystem(ref);
}
//<< addition

bool CBaristaProject::removeMonoplottedPoint(const CXYZPoint& point)
{
	int index = this->xyzMonoplottedPoints.indexOf((CObsPoint*)&point);
	if (index >= 0)
	{
		this->removeRelated2DPoints(point.getLabel(),MONOPLOTTED);
		this->xyzMonoplottedPoints.RemoveAt(index);
	}

	return (this->xyzMonoplottedPoints.GetSize() < 1);

}

bool CBaristaProject::removeMonoplottedPoint(int index,int count)
{
	if (this->xyzMonoplottedPoints.GetSize() >= (index + count))
	{
		for (int i=0; i< count; i++)
			this->removeRelated2DPoints(this->xyzMonoplottedPoints.GetAt(index + i)->getLabel(),MONOPLOTTED);

		this->xyzMonoplottedPoints.RemoveAt(index,count);
	}
	return  (this->xyzMonoplottedPoints.GetSize() < 1);
}

void CBaristaProject::removeGeoreferencedPoint(const CXYZPoint& point)
{
	int index = this->xyzGeoreferencedPoints.indexOf((CObsPoint*)&point);
	if (index >= 0)
	{
		this->xyzGeoreferencedPoints.RemoveAt(index);
	}
}

//addition>>
void CBaristaProject::removeGCPchipPoint(int index,int count)
{
	this->xyzGCPchipPoints.RemoveAt(index,count);
}

void CBaristaProject::removeGCPchipPoint(const CXYZPoint& point)
{
	int index = this->xyzGCPchipPoints.indexOf((CObsPoint*)&point);
	if (index >= 0)
	{
		this->xyzGCPchipPoints.RemoveAt(index);
	}
}
void CBaristaProject::deleteGCPchipPoints(CXYZPointArray* points)
{
	if (points && points->getPointType() == eGCPCHIP)
		this->removeGCPchipPoint(0,this->xyzGCPchipPoints.GetSize());
}
//<<addition

void CBaristaProject::removeGeoreferencedPoint(int index,int count)
{
	this->xyzGeoreferencedPoints.RemoveAt(index,count);
}

bool CBaristaProject::removeMonoplottedLine(const CXYZPolyLine& line)
{
	int index = this->xyzpolylines.indexOf((CXYZPolyLine*)&line);
	if (index >= 0)
		this->xyzpolylines.RemoveAt(index);

	return  (this->xyzpolylines.GetSize() < 1);
}


bool CBaristaProject::removeMonoplottedLine(int index,int count)
{
	
	for (int i=0; i< count; i++)
	{
		int n = this->xyzpolylines.GetAt(index+i)->getPointCount();	
		for (int k=0; k < n; k++)
			this->removeRelated2DPoints(this->xyzpolylines.getAt(index+i)->getPoint(k)->getLabel(),MONOPLOTTED);
			
	}

	this->xyzpolylines.RemoveAt(index,count);

	return (this->xyzpolylines.GetSize() < 1);
}

// addition >>
bool CBaristaProject::removeBheightLine(const CXYZPolyLine& line)
{
	int index = this->xyzBheights.indexOf((CXYZPolyLine*)&line);
	if (index >= 0)
		this->xyzBheights.RemoveAt(index);

	return  (this->xyzBheights.GetSize() < 1);
}


bool CBaristaProject::removeBheightLine(int index,int count)
{
	//to change in images & dems
	/*for (int i=0; i< count; i++)
	{
		int n = this->xyzBheights.GetAt(index+i)->getPointCount();	
		for (int k=0; k < n; k++)
			this->removeRelated2DPoints(this->xyzBheights.getAt(index+i)->getPoint(k)->getLabel(),MONOPLOTTED);
			
	}*/

	this->xyzBheights.RemoveAt(index,count);

	return (this->xyzBheights.GetSize() < 1);
}
//<< addition

#ifdef WIN32

bool CBaristaProject::removeMonoplottedBuilding(int index,int count)
{
	for (int i=0; i < count; i++)
	{
		
		CXYZPointArray points = this->buildings.GetAt(index + i)->getVertices();
		int n = points.GetSize();	
		for (int k=0; k < n; k++)
			this->removeRelated2DPoints(points.getAt(k)->getLabel(),MONOPLOTTED);
	}

	this->buildings.RemoveAt(index,count);
	this->currentBuildingIndex = -1;

	return (this->buildings.GetSize() < 1);
}


bool CBaristaProject::removeMonoplottedBuilding(const CBuilding& building)
{
	int index = this->buildings.indexOf((CBuilding*)&building);
	if (index >= 0)
		this->buildings.RemoveAt(index);

	this->currentBuildingIndex = -1;

	return (this->buildings.GetSize() < 1);
}

#endif

void CBaristaProject::deleteMonoplottedPoints(CXYZPointArray* points)
{
	if (points && points->getPointType() == eMONOPLOTTED)
		this->removeMonoplottedPoint(0,this->xyzMonoplottedPoints.GetSize());
}

void CBaristaProject::deleteGeoreferencedPoints(CXYZPointArray* points)
{
	if (points && points->getPointType() == eGEOREFERENCED)
		this->removeGeoreferencedPoint(0,this->xyzGeoreferencedPoints.GetSize());
}



CLabel CBaristaProject::getCurrentPointLabel()
{ 
	
	if ( this->currentPointLabel.GetLength() == 0 )
		this->currentPointLabel= "MPOINT";

	do
	{
      if (!project.getHeightPointMonoplotting()) this->currentPointLabel++;

	}while (this->xyzMonoplottedPoints.getPoint(this->currentPointLabel.GetChar()));
	
	return this->currentPointLabel;
}

void CBaristaProject::setCurrentPointLabel(CLabel newPointlabel)
{
	this->currentPointLabel = newPointlabel;
}

void CBaristaProject::incrementCurrentPointLabel()
{
	this->currentPointLabel++;
}

CLabel CBaristaProject::getCurrentLineLabel()
{
	if ( this->currentLineLabel.GetLength() == 0)
		this->currentLineLabel= "MLINE";
	do
	{
		this->currentLineLabel++;
	}while (this->xyzpolylines.getLine(this->currentLineLabel.GetChar()));

	return this->currentLineLabel;
}

void CBaristaProject::setCurrentLineLabel(CLabel newLineLabel)
{
	this->currentLineLabel = newLineLabel;
}

void CBaristaProject::incrementCurrentLineLabel()
{
	this->currentLineLabel++;
}

//addition >>
CLabel CBaristaProject::getCurrentBheightLabel()
{
	if ( this->currentBheightLabel.GetLength() == 0)
		this->currentBheightLabel= "HLINE";
	do
	{
		this->currentBheightLabel++;
	}while (this->xyzBheights.getLine(this->currentBheightLabel.GetChar()));

	return this->currentBheightLabel;
}

void CBaristaProject::setCurrentBheightLabel(CLabel newLineLabel)
{
	this->currentBheightLabel = newLineLabel;
}

void CBaristaProject::incrementCurrentBheightLabel()
{
	this->currentBheightLabel++;
}
//<< addition

CLabel CBaristaProject::getCurrentBuildingLabel()
{
	if ( this->currentBuildingLabel.GetLength() == 0)
	{
		this->currentBuildingLabel= "B";
		this->currentBuildingLabel += this->getNumberOfMonoplottedBuildings();
	}
#ifdef WIN32
	while (this->buildings.getBuilding(this->currentBuildingLabel.GetChar()))
	{
		this->currentBuildingLabel++;
	}
#endif	
	return this->currentBuildingLabel;
}

void CBaristaProject::setCurrentBuildingLabel(CLabel newBuildingLabel)
{
	this->currentBuildingLabel = newBuildingLabel;
}

void CBaristaProject::incrementCurrentBuildingLabel()
{
	this->currentBuildingLabel++;
}








void CBaristaProject::removeRelated2DPoints(const CLabel& label,xypointtype type)
{
	int nImages = this->getNumberOfImages();
	int nDEMs = this->getNumberOfDEMs();

	for (int i=0; i < nImages; i++)
	{
		CVImage* image = this->images.GetAt(i);
		if (type = MONOPLOTTED)
		{
			image->removeMonoPlottedPoint(label);
		}
	}

	for (int i=0; i < nDEMs; i++)
	{
		CVDEM* vdem = this->dems.GetAt(i);
		if (type = MONOPLOTTED)
		{
			vdem->removeMonoPlottedPoint(label);
		}
	}

}

CCamera* CBaristaProject::addCamera(CameraType type)
{
	return this->cameras.Add(type);
}

CCamera* CBaristaProject::addCamera(const CCamera* camera)
{
	return this->cameras.Add(*camera);
}


void CBaristaProject::deleteCamera(CFileName& filename)
{
	for (int i =this->cameras.GetSize() -1; i >= 0 ; i--)
	{
		CCamera* c = this->cameras.GetAt(i);
		if (filename.CompareNoCase(c->getName()))
		{
			c->goingToBeDeleted();		
			this->cameras.RemoveAt(i,1);
		}
	}
}

void CBaristaProject::deleteCamera(CCamera* camera)
{
	int index = this->cameras.indexOf(camera);

	if (index >= 0 ) 
	{
		camera->goingToBeDeleted();		
		this->cameras.RemoveAt(index,1);
	}
}


bool CBaristaProject::renameCamera(CCamera* camera ,const char *newName)
{
	CCharString cname(newName);
	bool ret = true;
	if ( strlen(newName) == 0) ret = false;
	else if (strcmp(newName, camera->getName().GetChar()) == 0) ret = false;
	else if ( this->cameras.getCamerabyName(&cname) != 0) ret = false;
	else
	{
		camera->setName(newName);
	}

	return ret;
}

CCameraMounting* CBaristaProject::addCameraMounting()
{
	return this->cameraMountings.Add();
}

void CBaristaProject::deleteCameraMounting(CFileName& filename)
{
	for (int i =this->cameraMountings.GetSize() -1; i >= 0 ; i--)
	{
		CCameraMounting* cm = this->cameraMountings.GetAt(i);
		if (filename.CompareNoCase(cm->getName()))
		{
			cm->goingToBeDeleted();		
			this->cameraMountings.RemoveAt(i,1);
		}
	}
}

void CBaristaProject::deleteCameraMounting(CCameraMounting* cameraMountint)
{
	int index = this->cameraMountings.indexOf(cameraMountint);

	if (index >= 0 ) 
	{
		cameraMountint->goingToBeDeleted();
		this->cameraMountings.RemoveAt(index,1);
	}
}

bool CBaristaProject::renameCameraMounting(CCameraMounting* cameraMounting ,const char *newName)
{
	CCharString cname(newName);
	bool ret = true;
	if ( strlen(newName) == 0) ret = false;
	else if (strcmp(newName, cameraMounting->getName().GetChar()) == 0) ret = false;
	else if ( this->cameraMountings.getCameraMountingByName(&cname) != 0) ret = false;
	else
	{
		cameraMounting->setName(newName);
	}

	return ret;
}

bool CBaristaProject::getUniqueImageName(CCharString& uniqueName)
{
	int i=0;
	int add = 0;
	CCharString oldName(uniqueName);
	bool unique = true;
	
	while (i < this->images.GetSize())
	{
		CVImage* image = this->images.GetAt(i);
		if ( image->getName().CompareNoCase(uniqueName))
		{
			add++;
			i = 0; // start again with the search
			char* buffer = new char[uniqueName.GetLength() + 10];
			sprintf(buffer,"%s_%d",oldName.GetChar(),add);
			uniqueName = buffer;
			delete []  buffer;
			unique = false;
		}
		else
			i++;
	}

	return unique;
}

bool CBaristaProject::getUniqueCameraName(CCharString& uniqueName)
{
	int i=0;
	int add = 0;
	CCharString oldName(uniqueName);
	bool unique = true;
	
	while (i < this->cameras.GetSize())
	{
		CCamera* camera = this->cameras.GetAt(i);
		if (camera->getName().CompareNoCase(uniqueName))
		{
			add++;
			i = 0; // start again with the search
			char* buffer = new char[uniqueName.GetLength() + 10];
			sprintf(buffer,"%s_%d",oldName.GetChar(),add);
			uniqueName = buffer;
			delete []  buffer;
			unique = false;
		}
		else
			i++;
	}

	return unique;
}

bool CBaristaProject::getUniqueCameraMountingName(CCharString& uniqueName)
{
	int i=0;
	int add = 0;
	CCharString oldName(uniqueName);
	bool unique = true;
	
	while (i < this->cameraMountings.GetSize())
	{
		CCameraMounting* cameraMounting = this->cameraMountings.GetAt(i);
		if (cameraMounting->getName().CompareNoCase(uniqueName))
		{
			add++;
			i = 0; // start again with the search
			char* buffer = new char[uniqueName.GetLength() + 10];
			sprintf(buffer,"%s_%d",oldName.GetChar(),add);
			uniqueName = buffer;
			delete []  buffer;
			unique = false;
		}
		else
			i++;
	}

	return unique;
}

bool CBaristaProject::getUniqueOrbitPathName(CCharString& uniqueName)
{
	int i=0;
	int add = 0;
	CCharString oldName(uniqueName);
	bool unique = true;
	
	while (i < this->orbits.GetSize())
	{
		COrbitObservations* orbit = this->orbits.GetAt(i);
		if (orbit->getSplineType() == Path && orbit->getName().CompareNoCase(uniqueName))
		{
			add++;
			i = 0; // start again with the search
			char* buffer = new char[uniqueName.GetLength() + 10];
			sprintf(buffer,"%s_%d",oldName.GetChar(),add);
			uniqueName = buffer;
			delete []  buffer;
			unique = false;
		}
		else
			i++;
	}

	return unique;
}

bool CBaristaProject::getUniqueOrbitAttitudeName(CCharString& uniqueName)
{
	int i=0;
	int add = 0;
	CCharString oldName(uniqueName);
	bool unique = true;
	
	while (i < this->orbits.GetSize())
	{
		COrbitObservations* orbit = this->orbits.GetAt(i);
		if (orbit->getSplineType() == Attitude && orbit->getName().CompareNoCase(uniqueName))
		{
			add++;
			i = 0; // start again with the search
			char* buffer = new char[uniqueName.GetLength() + 10];
			sprintf(buffer,"%s_%d",oldName.GetChar(),add);
			uniqueName = buffer;
			delete []  buffer;
			unique = false;
		}
		else
			i++;
	}

	return unique;
}

bool CBaristaProject::checkforUniquePointLabel(CLabel newPointLabel)
{
	bool unique = true;

	int i =0;
	
	while (i < this->images.GetSize())
	{
		CVImage* image = this->images.GetAt(i);
		if ( image->getXYPoints()->getPoint(newPointLabel.GetChar()))
		{
			// XYPoint label not new for all images
			return false;
		}
		else
			i++;
	}

	return unique;
}


void CBaristaProject::setCurrentXYPointLabelForAllImages(CLabel newPointLabel)
{
    // setting current XYPoint label for each image
    for (int i = 0; i < this->images.GetSize(); i++)
    {
        CVImage* image = this->images.GetAt(i);
		image->setCurrentLabel(newPointLabel);
	}
}

bool CBaristaProject::checkforUniqueBuildingLabel(CLabel newLabel)
{
#ifdef WIN32
	CBuilding* building = this->getBuildings()->getBuilding(newLabel.GetChar());
	if ( building)
	{
		// building label exists already
		return false;
	}
	else
		return true;
#else
	return false;
#endif		
}

void CBaristaProject::resetRobustErrors()
{
	// reset all observations in the images
	for (int i=0; i < this->images.GetSize(); i++)
	{
		this->images.GetAt(i)->getXYPoints()->setStauts(1);
		this->images.GetAt(i)->getResiduals()->setStauts(1);

	}
	// reset the current control points
	if (this->controlPoints)
	{
		this->controlPoints->getXYZPointArray()->setStauts(1);
	}

	// reset the orbits
	for (int i=0; i < this->orbits.GetSize(); i++)
	{
		this->orbits.GetAt(i)->getObsPoints()->setStauts(1);
	}

}


void CBaristaProject::setUseOrbitOvsevationHandler(const CSensorModel* sensorModel, bool use)
{
	for (int i=0; i < this->orbits.GetSize(); i++)
	{
		COrbitObservations* orbit = this->orbits.GetAt(i);
		if (orbit->getCurrentSensorModel() == sensorModel)
		{
			orbit->setUseObservationHandler(use);
		}
	}
}

CXYZPointArray* CBaristaProject::getPointResidualArrayOfXYZPointArray(const CXYZPointArray* xyzPointArray) const
{

	int index = this->xyzArrays.indexOf(xyzPointArray);
	CXYZPointArray* ret = NULL;
	
	if (index >=0)
	{
		ret = this->xyzResidulaArray.GetAt(index);
	}

	return ret;
}

void CBaristaProject::initExteriorOrientationOfFrameCameras(const char *filename, bool rotate180)
{		
	ifstream oriFile(filename);
	
	if (oriFile.good())
	{
		C3DPoint P0;
		double c;
		int maxLine = 2048;
		char *line1 = new char[maxLine];
		char *line2 = new char[maxLine];
		char *line3 = new char[maxLine];
		
		bool OK = true;

		while (OK)
		{
			oriFile.getline(line1, maxLine);
			OK = oriFile.good();
			if (OK)
			{
				oriFile.getline(line2, maxLine);
				OK = oriFile.good();
				if (OK)
				{
					oriFile.getline(line3, maxLine);
					OK = oriFile.good();
					if (OK)
					{
						CFrameCameraModel *frameCam = 0;
						istrstream if1(line1);
						istrstream if2(line2);
						istrstream if3(line3);
						char *imgNameChar = new char[maxLine];
						if1 >> imgNameChar;
						CCharString imgName(imgNameChar);
						delete [] imgNameChar;

						for (int i = 0; !frameCam && (i < this->images.GetSize()) ; ++i)
						{
							CVImage *img = this->images.GetAt(i);
							if (imgName == img->getFileNameChar())
							{
								if (img->hasFrameCamera())
								{
									frameCam = img->getFrameCamera();
								}
								else i = this->images.GetSize();
							}
						}

						if (frameCam)
						{
							if1 >> c;
							if1 >> P0.x;
							if1 >> P0.y;
							if1 >> P0.z;
							if (rotate180)
							{
								double d;
								if2 >> d;
								frameCam->getCameraRotation()->getRotMat()(0,0) = -d;
								if2 >> d;
								frameCam->getCameraRotation()->getRotMat()(0,1) = -d;
								if2 >> d;
								frameCam->getCameraRotation()->getRotMat()(0,2) =  d;
								if2 >> d;
								frameCam->getCameraRotation()->getRotMat()(1,0) = -d;
								if2 >> d;
								frameCam->getCameraRotation()->getRotMat()(1,1) = -d;
								if3 >> d;
								frameCam->getCameraRotation()->getRotMat()(1,2) =  d;
								if3 >> d;
								frameCam->getCameraRotation()->getRotMat()(2,0) = -d;
								if3 >> d;
								frameCam->getCameraRotation()->getRotMat()(2,1) = -d;
								if3 >> d;
								frameCam->getCameraRotation()->getRotMat()(2,2) =  d;
							}
							else
							{
								if2 >> frameCam->getCameraRotation()->getRotMat()(0,0);
 								if2 >> frameCam->getCameraRotation()->getRotMat()(1,0);
 								if2 >> frameCam->getCameraRotation()->getRotMat()(2,0);
 								if2 >> frameCam->getCameraRotation()->getRotMat()(0,1);
 								if2 >> frameCam->getCameraRotation()->getRotMat()(1,1);
 								if3 >> frameCam->getCameraRotation()->getRotMat()(2,1);
 								if3 >> frameCam->getCameraRotation()->getRotMat()(0,2);
 								if3 >> frameCam->getCameraRotation()->getRotMat()(1,2);
 								if3 >> frameCam->getCameraRotation()->getRotMat()(2,2);
							}

							frameCam->getCameraRotation()->computeParameterFromRotMat();
							frameCam->setCameraPosition(P0);
							frameCam->getCameraMounting()->getRotation().getRotMat()(0,0) =  1.0;
							frameCam->getCameraMounting()->getRotation().getRotMat()(0,1) =  0.0;
							frameCam->getCameraMounting()->getRotation().getRotMat()(0,2) =  0.0;
							frameCam->getCameraMounting()->getRotation().getRotMat()(1,0) =  0.0;
							frameCam->getCameraMounting()->getRotation().getRotMat()(1,1) = -1.0;
							frameCam->getCameraMounting()->getRotation().getRotMat()(1,2) =  0.0;
							frameCam->getCameraMounting()->getRotation().getRotMat()(2,0) =  0.0;
							frameCam->getCameraMounting()->getRotation().getRotMat()(2,1) =  0.0;
							frameCam->getCameraMounting()->getRotation().getRotMat()(2,2) = -1.0;
							frameCam->getCameraMounting()->getRotation().computeParameterFromRotMat();
						}
					}
				}
			}
		}
						
		delete [] line1;
		delete [] line2;
		delete [] line3;
	}
};
	
int CBaristaProject::findImageGroups(vector<vector<CVImage*> >& newImages,vector<CCharString>& names, int minNumberOfImagesPerGroup)
{
	newImages.clear();
	names.clear();
	
	vector<CCamera*> cameras;

	// first we check which of the ALOS cameras we got (Forward, Backward, Nadir)
	for (int i=0; i < this->getNumberOfCameras(); i++)
	{
		CCamera* c = this->getCameraAt(i);

		if (c->getDistortion() && c->getDistortion()->getDistortionType() == eDistortionALOS && CAlosReader::isALOS_PRISMCamera(c->getName()))
		{
			double y = c->getIRP().y;
			bool hasCameraAlready = false;
			for (unsigned int k=0; k < cameras.size(); k++)
			{
				if (fabs(cameras[k]->getIRP().y - y) < 0.000001)
				{
					hasCameraAlready = true;
					break;
				}
			}

			if (!hasCameraAlready)
			{
				cameras.push_back(c);
			}
		}
	}

	// now finde the images using the ALOS cameras
	for (unsigned int i=0; i < cameras.size(); i++)
	{
		CCamera* c = cameras[i];
		double y = c->getIRP().y;
		
		vector<vector<CVImage*> > images;

		for (int k=0; k < this->getNumberOfImages(); k++)
		{
			CVImage* image = this->getImageAt(k);
			CCamera* curentCamera = image->getPushbroom()->getCamera();
			// we only know that all cameras have the same y component of irp
			if (curentCamera && curentCamera->getDistortion() &&
				curentCamera->getDistortion()->getDistortionType() == eDistortionALOS 
				&& fabs(curentCamera->getIRP().y - y) < 0.00001)
			{
				// image found, 
				// first check the according orbit to make sure images in one group belong together 
				bool foundGroup = false;
				unsigned int groupIndex=0;
				for (unsigned int j=0; j < images.size(); j++)
				{					
					if (images[j].size() && fabs(image->getPushbroom()->getFirstLineTime() - images[j][0]->getPushbroom()->getFirstLineTime()) < FLT_EPSILON)
					{
						foundGroup = true;
						groupIndex = j;
						break;
					}
				}

				// if not in one of the existing groups create a new one
				if (!foundGroup)
				{
					vector<CVImage*> group;
					images.push_back(group);
					groupIndex = images.size() - 1;
				}

				//now sort it accordingly to the x component of the irp
				unsigned int index =0;
				while ( index < images[groupIndex].size() && 
						images[groupIndex][index]->getPushbroom()->getCamera()->getDistortion()->getDirectObservation(PARAMETER_1) < 
						image->getPushbroom()->getCamera()->getDistortion()->getDirectObservation(PARAMETER_1))
				{
					index++;
				}

				images[groupIndex].insert(images[groupIndex].begin()+index,image);
			}
		}


		for (unsigned int k=0; k < images.size(); k++)
		{
			if (images[k].size() >= minNumberOfImagesPerGroup && images[k].size() <= 6)
			{
				newImages.push_back(images[k]);
				CCharString cameraName;
				cameraName.Format("Image group %d",newImages.size());
				this->getUniqueCameraName(cameraName);
				names.push_back(cameraName);
			}
		}

	}

	return newImages.size();
};

/////////////
// get/set MonoplottedLineColor
C3DPoint CBaristaProject::getMonoplottedLineColor()
{
	return this->drawingSettings[INDEX_LINE_SETTINGS].lineColor;
}

void CBaristaProject::setMonoplottedLineColor(C3DPoint color)
{
	this->drawingSettings[INDEX_LINE_SETTINGS].lineColor = color;
}

// get/set MonoplottedLineWidth
int CBaristaProject::getMonoplottedLineWidth()
{
	return this->drawingSettings[INDEX_LINE_SETTINGS].lineWidth;
}
void CBaristaProject::setMonoplottedLineWidth(int width)
{
	this->drawingSettings[INDEX_LINE_SETTINGS].lineWidth = width;
}


C3DPoint CBaristaProject::getBuildingLineColor()
{
	return this->drawingSettings[INDEX_BUILDING_SETTINGS].lineColor;
}

void CBaristaProject::setBuildingLineColor(C3DPoint color)
{
	this->drawingSettings[INDEX_BUILDING_SETTINGS].lineColor = color;
}

// get/set BuildingLineWidth
int CBaristaProject::getBuildingLineWidth()
{
	return this->drawingSettings[INDEX_BUILDING_SETTINGS].lineWidth;
}
void CBaristaProject::setBuildingLineWidth(int width)
{
	this->drawingSettings[INDEX_BUILDING_SETTINGS].lineWidth = width;
}



COrbitObservations* CBaristaProject::findExistingOrbitPath(CCharString newName)
{
	// try to find existing orbits with the same id
	CCharString originalOrbitName(newName);
	CCharString newOrbitName;
	int end = originalOrbitName.ReverseFind('_');
	if (end >=0 && end < originalOrbitName.GetLength())
	{
		newOrbitName = originalOrbitName.Left(end);
	}

	COrbitObservations* result = NULL;
	for (int i=0; i < this->orbits.GetSize(); i++)
	{
		COrbitObservations* search = this->orbits.GetAt(i);
		if (search->getSplineType() == Path && 
			newOrbitName.CompareNoCase(search->getName()))
		{
			result = search;
			break;
		}
	}
	
	return result;
}

COrbitObservations* CBaristaProject::findExistingOrbitAttitudes(CCharString newName)
{
	CCharString originalAttitudeName(newName);
	CCharString newAttitudeName;
	int end = originalAttitudeName.ReverseFind('_');
	if (end >=0 && end < originalAttitudeName.GetLength())
	{
		newAttitudeName = originalAttitudeName.Left(end);
	}

	COrbitObservations* result = NULL;
	for ( int i=0; i < this->orbits.GetSize(); i++)
	{
		COrbitObservations* search = this->orbits.GetAt(i);
		if (search->getSplineType() == Attitude &&
			newAttitudeName.CompareNoCase(search->getName()))
		{
			result = search;
			break;
		}
	}

	return result;
}

CCameraMounting* CBaristaProject::findExistingCameraMounting(CCharString newName)
{
	CCharString originalCameraMountingName(newName);
	CCharString newCameraMountingName;
	int end = originalCameraMountingName.ReverseFind('_');
	if (end >=0 && end < originalCameraMountingName.GetLength())
	{
		newCameraMountingName = originalCameraMountingName.Left(end);
	}

	CCameraMounting* result = NULL;
	for (unsigned int i=0; i < this->cameraMountings.GetSize(); i++)
	{
		CCameraMounting* search =  this->cameraMountings.GetAt(i);
		if (newCameraMountingName.CompareNoCase(search->getName()))
		{
			result = search;
			break;
		}
	}

	return result;
}

CCameraMounting* CBaristaProject::findExistingCameraMounting(CCameraMounting* newCameraMounting)
{
	CCameraMounting* result = NULL;
	if (newCameraMounting)
	{
	
		C3DPoint shiftNew(newCameraMounting->getShift());
		C3DPoint rotAngleNew(newCameraMounting->getRotation()[0],
						     newCameraMounting->getRotation()[1],
						     newCameraMounting->getRotation()[2]);

		for (unsigned int i=0; i < this->cameraMountings.GetSize(); i++)
		{
			CCameraMounting* search =  this->cameraMountings.GetAt(i);
			C3DPoint shiftSearch(search->getShift());
			C3DPoint rotAngleSearch(search->getRotation()[0],
						            search->getRotation()[1],
						            search->getRotation()[2]);

			if (fabs(shiftSearch.x -shiftNew.x) < FLT_EPSILON && 
				fabs(shiftSearch.y -shiftNew.y) < FLT_EPSILON &&
				fabs(shiftSearch.z -shiftNew.z) < FLT_EPSILON &&
				fabs(rotAngleSearch.x -rotAngleNew.x) < FLT_EPSILON && 
				fabs(rotAngleSearch.y -rotAngleNew.y) < FLT_EPSILON &&
				fabs(rotAngleSearch.z -rotAngleNew.z) < FLT_EPSILON)
			{
				result = search;
				break;
			}
		}
	}
	return result;
}

CCamera* CBaristaProject::findExistingCamera(CCharString newName)
{
	CCharString originalCameraName(newName);
	CCharString newCameraName;
	int end = originalCameraName.ReverseFind('_');
	if (end >=0  && end < originalCameraName.GetLength())
	{
		newCameraName = originalCameraName.Left(end);
	}

	CCamera* result = NULL;
	for (unsigned int i=0; i < this->cameras.GetSize(); i++)
	{
		
		CCamera* search =  this->cameras.GetAt(i);
		if (newCameraName.CompareNoCase(search->getName()))
		{
			result = search;
			break;
		}
	}

	return result;
}

CPushBroomScanner* CBaristaProject::getPushBroomScannerForOrbit(const COrbitObservations* orbit)
{
	if (orbit)
	{
		for (int i=0; i < this->images.GetSize(); i++)
		{
			CPushBroomScanner* scanner = this->getImageAt(i)->getPushbroom();

			if (scanner->getOrbitAttitudes() == orbit->getCurrentSensorModel())
				return scanner;
		}
	
	}

	return NULL;
}

COrbitObservations* CBaristaProject::getOrbitObservation(const COrbitPathModel* path) const
{
	for (int i=0; i < this->orbits.GetSize(); i++)
	{
		COrbitObservations* orbit = this->orbits.GetAt(i);

		if (orbit->getSplineType() == Path && orbit->getCurrentSensorModel() == path)
			return orbit;
	}

	return NULL;
}

COrbitObservations* CBaristaProject::getOrbitObservation(const COrbitAttitudeModel* attitude) const
{
	for (int i=0; i < this->orbits.GetSize(); i++)
	{
		COrbitObservations* orbit = this->orbits.GetAt(i);

		if (orbit->getSplineType() == Attitude && orbit->getCurrentSensorModel() == attitude)
			return orbit;
	}

	return NULL;
}


bool CBaristaProject::mergeOrbits(vector<CPushBroomScanner*> scanner)
{
	bool error = false;
	CRotationMatrix fixedMatrix;
	double fixedTime;	

	// create new observation handler
	COrbitObservations* path = project.addOrbitObservation(Path);
	path->getCurrentSensorModel()->deactivateParameterGroup(CSensorModel::shiftPar);
	path->getCurrentSensorModel()->deactivateParameterGroup(CSensorModel::rotPar);
	CCharString pathName("MergedOrbitPath");
	project.getUniqueOrbitPathName(pathName);
	path->setFileName(pathName.GetChar());

	COrbitObservations* attitudes = project.addOrbitObservation(Attitude);
	attitudes->getCurrentSensorModel()->deactivateParameterGroup(CSensorModel::rotPar);
	attitudes->getCurrentSensorModel()->deactivateParameterGroup(CSensorModel::scalePar);
	CCharString attitudeName("MergedOrbitAttitude");
	project.getUniqueOrbitAttitudeName(attitudeName);
	attitudes->setFileName(attitudeName.GetChar());

	vector<CCameraMounting *>         orderedMountings;
	vector<CRotationMatrix>  orderedMountMatTransposed;

	//addition>>
	bool changeMounting = false; // for ALOS and other images, no need to change the Camera Mounting Rotation Matrix
	if (scanner[0]->getChangeMountingForMerging())
		changeMounting = true; // for QuickBird images
	int scene_start, scene_end, total_count;
		//addition>>
	//	FILE *fp;
	//	fp = fopen("Angles_merge.txt","w");
		//double Ang[2000][3];
		//<<addition
	//<<addition

	// just use a for loop, most things are similar for path and attitudes
	for (int orbitType=0; orbitType < 2; orbitType++)
	{
		vector<COrbitObservations *> orderedOrbits;
		COrbitAttitudeModel* attitudeModel = NULL;
		CRotationMatrix currentMat(0,0,0,0,0,0,0,0,0);
		CRotationMatrix tmpRot(0,0,0,0,0,0,0,0,0);
		CRotationMatrix currentMountMat(0,0,0,0,0,0,0,0,0);
		CRotationMatrix currentMountMatTransposed(0,0,0,0,0,0,0,0,0);

		//addition>>
		total_count = 0;
		//<<addition

		// put the orbit observations in the right order
		for (unsigned int i=0; !error && i < scanner.size(); i++)
		{
			COrbitObservations* orbit = NULL;
			CCameraMounting *mount = 0;
			
			if (orbitType == 0)
				orbit = project.getOrbitObservation(scanner[i]->getOrbitPath());
			else
			{
				orbit = project.getOrbitObservation(scanner[i]->getOrbitAttitudes());
				mount = scanner[i]->getCameraMounting();
			}

			if (!orbit)
				continue;

			int firstPointIndex = orbit->getFirstPointIndex();
			int lastPointIndex = orbit->getLastPointIndex();

			// check first and last index here, so we don't have to do that later again
			if ( firstPointIndex < 0  || firstPointIndex >= orbit->getObsPoints()->GetSize() ||
				 lastPointIndex < 0   || lastPointIndex  >= orbit->getObsPoints()->GetSize())
				 continue;

			double startTime = orbit->getObsPoints()->GetAt(firstPointIndex)->getDot();

			int indexVector = 0; 
			
			while (indexVector < orderedOrbits.size() &&
				   orderedOrbits[indexVector]->getObsPoints()->GetAt(orderedOrbits[indexVector]->getFirstPointIndex())->getDot() < startTime)
				indexVector ++;
			
			// save the orbits accordingly to their time
			orderedOrbits.insert(orderedOrbits.begin() + indexVector,orbit);
			if (mount) orderedMountings.insert(orderedMountings.begin() + indexVector, mount);
		}


		if (orderedOrbits.size() < 1)
			error = true;

#ifdef _DEBUG
		CCharString fn = protHandler.getDefaultDirectory() + "mergedRotations.dat";
		ofstream out(fn.GetChar());
		out.setf(ios::fixed, ios::floatfield);
		out.precision(7);
#endif
		// fill the new observation handler with the existing orbit points
		for (unsigned int i=0; !error && i < orderedOrbits.size(); i++)
		{
			COrbitObservations* orbit = orderedOrbits[i];

			if (orbitType == 1)
			{   // fixed matrix is recaculated in the first iteration, i.e., while merging path, so comment the following line here
				// fixedMatrix = ((COrbitAttitudeModel*)orderedOrbits[0]->getCurrentSensorModel())->getFixedECRMatrix();
				attitudeModel = (COrbitAttitudeModel*)orbit->getCurrentSensorModel();
				fixedMatrix.RT_times_R(currentMat,attitudeModel->getFixedECRMatrix());

				//addition>>
				if (changeMounting)
				{
				//<<addition
				CRotationMatrix firstOrbitRot = orderedMountings[0]->getRotation().getRotMat();
				orderedMountings[i]->getRotation().getRotMat().R_times_RT(currentMountMat, firstOrbitRot);
				currentMountMatTransposed = currentMountMat;
				currentMountMatTransposed.transpose();
				orderedMountMatTransposed.push_back(currentMountMatTransposed);
				//addition>>
				}
				//<<addition

			}

			int firstIndexCurrent = orbit->getFirstPointIndex();

			//addition>>
			// for both ALOS and QB paths and attitudes
			if (i == 0)
			{
				scene_start = firstIndexCurrent;				
			}
			//<<addition

			double endTime;

			if (i+1 == orderedOrbits.size())
			{
				// last orbit, so we stop at last point index
				COrbitObservations* nextOrbit = orderedOrbits[i];
				int lastIndexNext = nextOrbit->getLastPointIndex();
				//endTime = nextOrbit->getObsPoints()->GetAt(lastIndexNext)->getDot();
				
				//addition>>
				endTime = orbit->getObsPoints()->GetAt(orbit->getObsPoints()->GetSize()-1)->getDot();
				scene_end = total_count + lastIndexNext - firstIndexCurrent;				
				//<<addition
			}
			else
			{
				// more orbits, stop at time of first point of next orbit
				COrbitObservations* nextOrbit = orderedOrbits[i+1];
				int firstIndexNext = nextOrbit->getFirstPointIndex();
				endTime = nextOrbit->getObsPoints()->GetAt(firstIndexNext)->getDot();
			}
		
			//int indexOrbitPoint = (i == 0) ? firstIndexCurrent : 0;

			//addition>>
			//int indexOrbitPoint = firstIndexCurrent;
			int indexOrbitPoint = (i == 0) ? 0 : firstIndexCurrent;
			//<<addition

			while (indexOrbitPoint < orbit->getObsPoints()->GetSize() && 
				//orbit->getObsPoints()->GetAt(indexOrbitPoint)->getDot() < endTime)
				//addition>>
				   (orbit->getObsPoints()->GetAt(indexOrbitPoint)->getDot() < endTime || (i+1 == orderedOrbits.size())))
				//<<addition
			{
				total_count++;
				if (orbitType == 0)
					path->getObsPoints()->Add(*orbit->getObsPoints()->GetAt(indexOrbitPoint));
				else
				{ // re-calculation of time dependant rotations (roll-pitch-yaw) while merging
					CObsPoint* p = orbit->getObsPoints()->GetAt(indexOrbitPoint);
					*p /= 1000.0; // back to radiant
					CRollPitchYawRotation oldRpy(p);
					CRollPitchYawRotation newRpy(p);

					//addition>>
					if (changeMounting) // for QuickBird
					{
					//<<addtion
					currentMat.R_times_R(tmpRot,oldRpy.getRotMat());
					tmpRot.R_times_R(newRpy.getRotMat(),currentMountMat);

					//addition>>
					}
					else // for ALOS and other satllites
					{
						currentMat.R_times_R(newRpy.getRotMat(),oldRpy.getRotMat());
					}
					//<<addition

					newRpy.computeParameterFromRotMat();
					(*p)[0] = newRpy[0] * 1000.0; // back to mradiant
					(*p)[1] = newRpy[1] * 1000.0;
					(*p)[2] = newRpy[2] * 1000.0;


					//addition>>
					//fprintf(fp,"%f\t%f\t%f\n",newRpy[0],newRpy[1],newRpy[2]);
					//<<addition

#ifdef _DEBUG
		
					out.width(15); out << p->dot << " ";
					out.width(15); out << (*p)[0] << " ";
					out.width(15); out << (*p)[1] << " ";
					out.width(15); out << (*p)[2] << " \n";
#endif

					attitudes->getObsPoints()->Add(*p);
				}

				indexOrbitPoint++;
			}

		}
#ifdef _DEBUG
		out.close();
#endif
		if (orbitType == 0 && !error && path->getObsPoints()->GetSize() > 0)
		{
			//path->setFirstPointIndex(0);
			//path->setLastPointIndex(path->getObsPoints()->GetSize()-1);

			//addition>>
			//path->setFirstPointIndex(orderedOrbits[0]->getFirstPointIndex());
			//path->setLastPointIndex(orderedOrbits[orderedOrbits.size()-1]->getLastPointIndex());
			path->setFirstPointIndex(scene_start);
			path->setLastPointIndex(scene_end);
			//<<addition

			// adjust the new orbit path
			// it doesn't matter which sat reader we are using!!
			CAlosReader reader;
			error = !reader.adjustPath(path);
			path->getCurrentSensorModel()->activateParameterGroup(CSensorModel::shiftPar);

			// we need a new fixed matrix 
			if (!error) // calculation of fixed matrix
			{
				COrbitPathModel* modelPath = (COrbitPathModel*) path->getCurrentSensorModel();
				// computation of the fixed Rotation Matrix
				// due to different times in attitudes and orbit points we have
				// to interpolate the orbit points to get it at the same time as the angles

				//fixedTime = path->getObsPoints()->GetAt(0)->getDot();
				//fixedTime += (path->getObsPoints()->GetAt(path->getObsPoints()->GetSize() - 1)->getDot() - path->getObsPoints()->GetAt(0)->getDot()) / 2.0;

				//addition>>
					double time_first, time_last;
					time_first = path->getObsPoints()->GetAt(path->getFirstPointIndex())->getDot();
					time_last = path->getObsPoints()->GetAt(path->getLastPointIndex())->getDot();
					fixedTime = time_first + (time_last - time_first)/2.0;
				//<<addition

				C3DPoint location;
				C3DPoint velocity;
				modelPath->evaluate(location,velocity,fixedTime);

				C3DPoint X,Y,Z;

				Z = location /  location.getNorm();
				Y = Z.crossProduct(velocity);
				Y = Y / Y.getNorm();
				X = Y.crossProduct(Z);

				fixedMatrix(0,0) = X[0];
				fixedMatrix(1,0) = X[1];
				fixedMatrix(2,0) = X[2];

				fixedMatrix(0,1) = Y[0];
				fixedMatrix(1,1) = Y[1];
				fixedMatrix(2,1) = Y[2];

				fixedMatrix(0,2) = Z[0];
				fixedMatrix(1,2) = Z[1];
				fixedMatrix(2,2) = Z[2];

				// keep direct observation
				modelPath->setDirectObservationForShift(((COrbitPathModel*)orderedOrbits[0]->getCurrentSensorModel())->getDirectObservationForShift());

			}

		}
		else if (orbitType == 1 && !error && attitudes->getObsPoints()->GetSize() > 0)
		{
			//attitudes->setFirstPointIndex(0);
			//attitudes->setLastPointIndex(attitudes->getObsPoints()->GetSize()-1);

			//addition>>
			//attitudes->setFirstPointIndex(orderedOrbits[0]->getFirstPointIndex());
			//attitudes->setLastPointIndex(orderedOrbits[orderedOrbits.size()-1]->getLastPointIndex());
			attitudes->setFirstPointIndex(scene_start);
			attitudes->setLastPointIndex(scene_end);
			//<<addition

			// adjust the new orbit path
			// it doesn't matter which sat reader we are using!!
			CAlosReader reader;
			error = !reader.adjustAttitude(attitudes);
			attitudes->getCurrentSensorModel()->activateParameterGroup(CSensorModel::rotPar);
			

			if (!error)
			{
				COrbitAttitudeModel *modelAttitudes = (COrbitAttitudeModel*)attitudes->getCurrentSensorModel();
				modelAttitudes->setFixedECRMatrix(fixedMatrix);	// save Rfixed!
				modelAttitudes->setTimeFixedECRMatrix(fixedTime); // and the time
				
				// keep the direct observation
				modelAttitudes->setDirectObservationForAngles(((COrbitAttitudeModel*)orderedOrbits[0]->getCurrentSensorModel())->getDirectObservationForAngles());
			}
		}
	}


	if (error)
	{
		project.deleteOrbitObservation(path);
		project.deleteOrbitObservation(attitudes);
	}
	else
	{
		//addition>>
		if (changeMounting)
		{
		//<<addition
			vector<bool>  orderedMountingsChanged(orderedMountings.size(), false);
			for (unsigned int i = 0; i < orderedMountings.size(); ++i)
			{
				if (!orderedMountingsChanged[i])
				{
					orderedMountings[i]->applyRotation(orderedMountMatTransposed[i]);
					orderedMountingsChanged[i] = true;
					for (unsigned int j = i + 1; j < orderedMountings.size(); ++j)
					{
						if (orderedMountings[i] == orderedMountings[j]) 
							orderedMountingsChanged[j] = true;
					}
				}
			}
		//addition>>
		}
		//<<addition

		for (unsigned int i=0; !error && i < scanner.size(); i++)
		{
			COrbitPathModel* oldPath = scanner[i]->getOrbitPath();
			COrbitAttitudeModel* oldAttitudes = scanner[i]->getOrbitAttitudes();
			
			for (int j=0; j < this->images.GetSize(); j++)
			{
				
				CVImage* vimage = this->images.GetAt(j);
				CPushBroomScanner* s = vimage->getPushbroom();
				
				// update all images using the old path and attitudes
				if (s->getOrbitPath() &&
					s->getOrbitPath() == oldPath &&
					s->getOrbitAttitudes() &&
					s->getOrbitAttitudes() == oldAttitudes)
				{

					
					// init the scanner;
					error = !s->init(path->getCurrentSensorModel(),
									 attitudes->getCurrentSensorModel(),
									 orderedMountings[i],
									 s->getCamera(),
									 *s->getFileName(),
									 s->getSatelliteHeight(),
									 s->getGroundHeight(),
									 s->getGroundTemperature(),
									 s->getFirstLineTime(),
									 s->getTimePerLine());

					if (error)
					{
						// drive back ...
						s->init(oldPath,
								oldAttitudes,
								 s->getCameraMounting(),
								 s->getCamera(),
								 *s->getFileName(),
								 s->getSatelliteHeight(),
								 s->getGroundHeight(),
								 s->getGroundTemperature(),
								 s->getFirstLineTime(),
								 s->getTimePerLine());

					}

					else
					{
						project.deleteOrbitObservation(project.getOrbitObservation(oldPath));
						project.deleteOrbitObservation(project.getOrbitObservation(oldAttitudes));
					}
				}
			} // loop over images
		} // loop over used scanners
	}



	//addtion>>
	//fclose(fp);
	//<<additio

	return !error;
}


bool CBaristaProject::differenceByLabelXYZ(CXYZPointArray* resultArray,CXYZPointArray* referenceArray,CXYZPointArray* compareArray,bool metrical) const
{

	eCoordinateType referenceType = referenceArray->getRefSys().getCoordinateType();
	eCoordinateType compareType   = compareArray->getRefSys().getCoordinateType();

	if (!referenceArray->GetSize() || !compareArray->GetSize())
		return false;

	if (referenceType == compareType)
	{
		if (referenceType == eGEOGRAPHIC && metrical)
		{
			CXYZPointArray utmReference;
			CXYZPointArray utmCompare;

			CXYZPoint *p = referenceArray->getAt(0);
			utmReference.getReferenceSystem()->setToWGS1984();
			utmReference.getReferenceSystem()->setUTMZone(p->getX(), p->getY());
			utmReference.setCoordinateType(eGRID);

			CTrans3D* trans = CTrans3DFactory::initTransformation(referenceArray->getRefSys(), utmReference.getRefSys());

			if (!trans)
				return false;

			trans->transformArray(&utmReference, referenceArray);
			trans->transformArray(&utmCompare, compareArray);

			delete trans;

			utmReference.differenceByLabel(resultArray, &utmCompare);
			resultArray->getReferenceSystem()->setCoordinateType(eUndefinedCoordinate);
		}
		else 
		{
			referenceArray->differenceByLabel(resultArray, compareArray);
			resultArray->getReferenceSystem()->setCoordinateType(referenceType);
		}
	}
	else
	{
		CTrans3D* trans = CTrans3DFactory::initTransformation(compareArray->getRefSys(), referenceArray->getRefSys());
		
		if (!trans)
			return false;

		CXYZPointArray tmpArray;
		trans->transformArray(&tmpArray, compareArray);
		delete trans;

		referenceArray->differenceByLabel(resultArray, &tmpArray);
		resultArray->getReferenceSystem()->setCoordinateType(referenceType);
	}

	return true;
}


bool CBaristaProject::addCheckPointsToControlPoints(CXYZPointArray* checkPointArray)
{
	if (!checkPointArray ||
		!checkPointArray->GetSize() ||
		!this->getControlPoints() || 
		!this->getControlPoints()->getXYZPointArray())
		return false;

	CXYZPointArray* controlPoints = this->getControlPoints()->getXYZPointArray();

	CTrans3D* trans = CTrans3DFactory::initTransformation(checkPointArray->getRefSys(),controlPoints->getRefSys());

	for (int i=0; i < checkPointArray->GetSize(); i++)
	{
		CXYZPoint* p_check = checkPointArray->getAt(i);

		// only if selected
		if (!p_check->getStatusBit(0))
			continue;

		
		CXYZPoint* p_control = (CXYZPoint*)controlPoints->getPoint(p_check->getLabel().GetChar());

		// add to array 
		if (!p_control)
		{
			
			// add point to control point array and set status bit so that we can delete the point later
			CXYZPoint* newPoint = controlPoints->add(p_check);
			newPoint->activateStatusBit(0);
			newPoint->activateStatusBit(ADDED_CHECKPOINT);
			trans->transform(*newPoint,*newPoint);

			// also set the weight (sigmas) very low, it's just a check point
			newPoint->setSX(1000.0);
			newPoint->setSY(1000.0);
			newPoint->setSZ(1000.0);

		} // point is already in array, just mark it
		else if (!p_control->getStatusBit(0))
		{
			p_control->activateStatusBit(0);
			p_control->activateStatusBit(CHANGED_CONTROLPOINT);

			// make a backup of the sigmas so that we can restore the old sigma after the adjustment
			p_check->setSX(p_control->getSigma(0));
			p_check->setSY(p_control->getSigma(1));
			p_check->setSZ(p_control->getSigma(2));
			p_control->setSX(1000.0);
			p_control->setSY(1000.0);
			p_control->setSZ(1000.0);

		} // it's a real control point, skip it
		else
		{ 
		}


	}


	return true;
}

bool CBaristaProject::removeChekPointsFromControlPoints(CXYZPointArray* checkPointArray)
{
	if (!checkPointArray ||
		!this->getControlPoints() || 
		!this->getControlPoints()->getXYZPointArray())
		return false;

	CXYZPointArray* controlPoints = this->getControlPoints()->getXYZPointArray();

	for (int i=controlPoints->GetSize() - 1; i >=0 ; i--)
	{
		CXYZPoint* p_control = controlPoints->getAt(i);

		// delete only points which have been added 
		if (p_control->getStatusBit(ADDED_CHECKPOINT))
		{
			controlPoints->RemoveAt(i);
		}
		else if (p_control->getStatusBit(CHANGED_CONTROLPOINT))
		{
			// restore old sigmas, we have saved them in the according checkpoint
			CObsPoint* p_check = checkPointArray->getPoint(p_control->getLabel().GetChar());
			p_control->deactivateStatusBit(0);
			if (p_check)
			{
				p_control->setSX(p_check->getSigma(0));
				p_control->setSY(p_check->getSigma(1));
				p_control->setSZ(p_check->getSigma(2));
			}
		}
	}

	return true;
}

bool CBaristaProject::backProjectXYZPoints(vector<CVImage*> images,CXYZPointArray& xyzpoints,CCharString& errorMessage,bool onlyPointsInImage,bool keepExistingPoints)
{
	bool ret= true;

	for (unsigned int i=0; i < images.size(); i++)
	{
		CVImage* image = images[i];
		
		if (image && image->hasPushbroom())
		{
			eCoordinateType xyzsystem = xyzpoints.getReferenceSystem()->getCoordinateType();

			if ( xyzsystem != eGEOCENTRIC || !xyzpoints.getReferenceSystem()->isWGS84() )
			{
				 errorMessage = "Only geocentric coordinates in WGS 84 possible at the moment!";
				 ret = false;
			}
			else
			{
				CXYPointArray* xy = image->getProjectedXYPoints();
				CPushBroomScanner* pb = image->getPushbroom();
				
				if (!keepExistingPoints)
					xy->RemoveAll();
				
				int width = image->getHugeImage()->getWidth();
				int height = image->getHugeImage()->getHeight();

				for (int k=0; k < xyzpoints.GetSize(); k++)
				{
					CXYPoint point;
					CXYZPoint* point3D = xyzpoints.getAt(k);

					if (keepExistingPoints && xy->getPoint(point3D->getLabel().GetChar()))
						continue;

					pb->transformObj2Obs(point,*point3D);

					if (onlyPointsInImage && (point.x < 0 || point.x > width || point.y < 0 || point.y > height))
					{
						// don't add the point
					}
					else
					{
						point.setLabel(point3D->getLabel());
						xy->add(point);		
					}
				}
					
				xy->setFileName("3Dto2DwithPushbroom");
				
			}
		}
		else
		{
			 errorMessage =  "The selected image %s has no Pushbroom Sensor Model.";
			 ret = false;
		}
	}


	return ret;
}

//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

void CBaristaProject::insertSatelliteFootprintsToLines(CReferenceSystem refsys)
{
	if (this->xyzpolylines.GetSize() > 0)
	{
		CTrans3D *trafo = CTrans3DFactory::initTransformation(*this->xyzpolylines.getReferenceSystem(), refsys);
		if (trafo)
		{
			for (int i = 0; i < this->xyzpolylines.GetSize(); ++i)
			{
				CXYZPolyLine *poly = this->xyzpolylines.getAt(i);
				for (int j = 0; j < poly->getPointCount(); ++j)
				{
					CXYZPoint *p = poly->getPoint(j);
					CXYZPoint pOld(*p);
					trafo->transform(*p,pOld);
				}
			}
		}
	}

	
	this->xyzpolylines.setReferenceSystem(refsys);

	CLabel baseLabel("L_0");

	for (int i = 0; i < this->images.GetSize(); ++i)
	{
		CTrans3D *trafo = 0;
		CVImage *img = this->images.GetAt(i);
		CSensorModel *m = img->getCurrentSensorModel();
		if (m) trafo = CTrans3DFactory::initTransformation(m->getRefSys(), refsys);

		if (trafo)
		{
			baseLabel++;
			CXYZPolyLine *poly = this->xyzpolylines.add();
			poly->setLabel(baseLabel);
			CXYZPoint pul, pur, plr, pll;
			CCharString l = baseLabel.GetChar() + CCharString("_ul");
			pul.setLabel(l.GetChar());
			l = baseLabel.GetChar() + CCharString("_ur");
			pur.setLabel(l.GetChar());
			l = baseLabel.GetChar() + CCharString("_lr");
			plr.setLabel(l.GetChar());
			l = baseLabel.GetChar() + CCharString("_ll");
			pll.setLabel(l.GetChar());
			img->getFootPrint(pul, pur, plr, pll, this->meanHeight, &refsys);
			poly->addPoint(pul); poly->addPoint(pur); 
			poly->addPoint(plr); ; poly->addPoint(pll); 
			poly->closeLine();
		}
	}
};



bool CBaristaProject::readGCPPointDataBase(CFileName dataBaseFileName,vector<CGCPDataBasePoint>& dataBase)
{
	bool ret = true;

	ret = CSystemUtilities::fileExists(dataBaseFileName);

	if (ret)
	{

	}


	return ret;
}

bool CBaristaProject::matchGCPDataBasePoints(const vector<CGCPDataBasePoint>& dataBase, const CMatchingParameters &pars,CReferenceSystem refSys)
{
	CXYZPointArray *matchedXYZPoints = project.getPointArrayByName(pars.getXYZFileName());
	
	if (!matchedXYZPoints)
	{
		matchedXYZPoints = project.addXYZPoints(pars.getXYZFileName(), 
			                                                 refSys, 
															 true, false, eMONOPLOTTED);
	}
	else
	{
		matchedXYZPoints->setReferenceSystem(refSys);
	}

	vector <bool> *selection = project.getChipSelectionArray();
	int currentSelection = 0;

	//write matching information into a protocol file
	CFileName fn = protHandler.getDefaultDirectory() + "MatchGCPs.prt";
	protHandler.open(protHandler.prt, fn, false);

	ostrstream oss;
	oss << "  M A T C H I N G   O F   G R O U N D   C O N T R O L   P O I N T S\n  =================================================================\n\n";
	protHandler.print(oss, protHandler.prt);

	for (unsigned int i=0; i < dataBase.size(); i++)
	{
		bool ret = true;
		const CGCPDataBasePoint& point = dataBase[i];

		// create an array with only one point
		CXYZPointArray gcpArray;
		

		CReferenceSystem refSysGCPPoint(refSys);
		refSysGCPPoint.setCoordinateType(point.getCoordinateType());
		CTrans3D* transGCPPointToOrtho = NULL;
		
	
		//int selection = i; // selected 2D point from the array of 2D observed points
		for (unsigned int j = 0; j < point.get2DPointArraySize(); j++)
		{
			if (selection->at(currentSelection)) // match only selected chips
			{
				//CGCPData2DPoint point2D = point.get2DPointAt(selection);
				CGCPData2DPoint point2D = point.get2DPointAt(j);
				CImageImporter imageImporter;
				CImageImportData importData;
				
				ret = imageImporter.initImportData(importData,false,point2D.getOrthoImageFileName().GetChar(),"");

				// read the ortho image chip for the point
				if (ret)
				{
					importData.bImportTFW = true;
					ret = imageImporter.importImage(importData,false);
				}
				CVImage *orthoImage = 0;
				if (ret)
				{
					orthoImage = importData.vimage;
					
					// transform gcp database point into ortho reference system
					transGCPPointToOrtho = CTrans3DFactory::initTransformation(refSysGCPPoint,orthoImage->getTFW()->getRefSys());
					if (transGCPPointToOrtho)
					{
						CXYZPoint transformed;
						transGCPPointToOrtho->transform(transformed,point);

						// add point 
						gcpArray.setReferenceSystem(orthoImage->getTFW()->getRefSys());
						gcpArray.add(transformed);

						delete transGCPPointToOrtho;
						transGCPPointToOrtho = NULL;
					}
					else
						ret = false;
				}
				// access particular 2D point from the array of 2D points ar point
				//ret = orthoImage.getHugeImage()->open(point2D.getOrthoImageFileName().GetChar(),true);
				


				int patchWindowSize = pars.getPatchSize();
				
				// check the image, only continue when image chip is bigger the requested patch size
				if (ret && orthoImage && patchWindowSize < orthoImage->getHugeImage()->getWidth() &&
						   patchWindowSize < orthoImage->getHugeImage()->getHeight())
				{
					// create the matcher
					CGCPMatcher matcher(&gcpArray,pars,orthoImage);

					// match template in all project images
					if (matcher.isInitialised()&& matcher.MatchPoints(project.getUseWallis4ChipMatching()))
					{
						CTrans3D* transOrthoToDest = CTrans3DFactory::initTransformation(orthoImage->getTFW()->getRefSys(),refSys);
						
						if (transOrthoToDest)
						{
							CXYZPoint* from = gcpArray.getAt(0);
							CXYZPoint* to = (CXYZPoint*) matchedXYZPoints->getPoint(from->getLabel().GetChar());
		
							if (!to)
								to = matchedXYZPoints->add();

							transOrthoToDest->transform(*to,*from);

							delete transOrthoToDest;
							transOrthoToDest = NULL;
						}
						
					}

				}

				//this->deleteImage(orthoImage); // image is not shown on the screen (window-tree) but is deleted from the array of imported images
				// delete the huge file from disk
				
				if (CSystemUtilities::fileExists(*orthoImage->getHugeImage()->getFileName()))
				{
					orthoImage->getHugeImage()->closeHugeFile();	
					CSystemUtilities::del(*orthoImage->getHugeImage()->getFileName());
				}
				this->deleteImage(orthoImage); // image is not shown on the screen (window-tree) but is deleted from the array of imported images
			} //if (selection->at(currentSelection)) 
			currentSelection++;
		}
		if (this->listener)
		{
			double value  = double(i) / double (dataBase.size());
			this->listener->setProgressValue(value*100);
		}

	}
	
	protHandler.close(protHandler.prt);
	return true;

}




//=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

CBaristaProject project;

// #################################################################################################
// #################################################################################################

CDrawingSetting::CDrawingSetting():lineWidth(1), lineColor(-1, 0, 255, 255) { }

void CDrawingSetting::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);
	
	CToken* token = pStructuredFileSection->addToken();
	token->setValue("Line Width", this->lineWidth);

	CStructuredFileSection* pNewSection = pStructuredFileSection->addChild();
	this->lineColor.serializeStore(pNewSection);

}

// #################################################################################################
void CDrawingSetting::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);
	
	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("Line Width"))
			this->lineWidth = token->getIntValue();
	}

	for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(i);

		if (pChild->getName().CompareNoCase("C3DPoint"))
		{
			this->lineColor.serializeRestore(pChild);
		}	
	}


}


// #################################################################################################
//void bui_MainWindow::selectGCPPoints(vector <CGCPDataBasePoint> gcpDB, ANNidxArray &nnIdx)
/*
bool CBaristaProject::selectGCPPoints(CXYZPointArray* dBPoints)
{
	CReferenceSystem refSys(55,SOUTHERN);
	refSys.setCoordinateType(eGRID);
	CTrans3D* transPointsToGrid = NULL;
	ANNkd_tree *kdTree = NULL;
	bool ret = true;
	ANNpointArray	dataPts = NULL; //annAllocPts(dBPoints->GetSize(),2);
	// transfer GCPs to eGRID
	CXYZPointArray transformedGCPs;
//	ANNidxArray nnIdx; // array of indices of selected GCP Points from gcpDB;
	vector <ANNidxArray> nnIdxArray; // array of index lists of selected GCP Points from gcpDB, one list for each rectangular strip;
	

	if (dBPoints)
	{
		transPointsToGrid = CTrans3DFactory::initTransformation(dBPoints->getRefSys(),refSys);
		if (transPointsToGrid)
		{
			transPointsToGrid->transformArray(&transformedGCPs, dBPoints);
			dataPts = annAllocPts(transformedGCPs.GetSize(),2);
			for (int i = 0; i < transformedGCPs.GetSize(); i++)
			{
				CXYZPoint* point = transformedGCPs.getAt(i);
				//dataPts[i] = &((*point)[0]);
				dataPts[i][0] = point->x;
				dataPts[i][1] = point->y;
			}	
			kdTree = new ANNkd_tree(dataPts, transformedGCPs.GetSize(), 2, 5); // 5 for the best splitting suggested by the author of ANN library

		}
	}
	else
	{
		ret = false;
	}

	CXYZPolyLineArray *polyLineArray = project.getXYZPolyLines();

	ANNpoint queryPt;
	queryPt = annAllocPt(2);
	ANNidxArray nnIdx = NULL; // array of indices of selected GCP Points from gcpDB;

	int *lenArrays = new int[polyLineArray->GetSize()];
	for (int i = 0; i<polyLineArray->GetSize(); i++)
	{
		CXYZPolyLine *polyLine = polyLineArray->getAt(i);		

		// assuming each polyline is a rectangle, we get its center as the center of the search space
		queryPt[0] = (polyLine->getPoint(0)->x + polyLine->getPoint(2)->x)/2;
		queryPt[1] = (polyLine->getPoint(0)->y + polyLine->getPoint(2)->y)/2;
		
		double sqRad = (queryPt[0] - polyLine->getPoint(0)->x) * (queryPt[0] - polyLine->getPoint(0)->x) + (queryPt[1] - polyLine->getPoint(0)->y) * (queryPt[1] - polyLine->getPoint(0)->y);
		int k = kdTree->annkFRSearch(queryPt,sqRad,0);
		nnIdx = new ANNidx[k];
		k = kdTree->annkFRSearch(queryPt,sqRad,k,nnIdx);

		// should delete all lists after making the final list
		//delete [] nnIdx;
		//nnIdx = NULL;
		sort(nnIdx, nnIdx+k);
		nnIdxArray.push_back(nnIdx);
		lenArrays[i] = k;
		//ANNidxArray id = nnIdxArray[i];		
	}
	int n = nnIdxArray.size();
	while (n>1)
	{
		vector <ANNidxArray> tempArray;
		div_t divRes = div(n,2);
		//int m = divRes.quot + divRes.rem;
		//while m>1
		//{

		//}
		for (int i = 0; i<divRes.quot; i++)
		{			
			vector<int> merged;
			vector<int>::iterator it;
			//vector<int> myvector;
			//vector<int>::iterator itc;
			int ft = 2*i;
			int lt = 2*i+1;
			merged.resize(lenArrays[ft]+lenArrays[lt]);
			it = set_union(nnIdxArray[ft],nnIdxArray[ft]+lenArrays[ft],nnIdxArray[lt],nnIdxArray[lt]+lenArrays[lt],merged.begin());
			int nElem = distance(merged.begin(),it);
			nnIdx = new ANNidx[nElem];
			std::copy(merged.begin(), it, nnIdx) ;
			tempArray.push_back(nnIdx);
			lenArrays[i] = nElem;
		}
		if (divRes.rem) // if odd number of lists then copy the final one
		{
			tempArray.push_back(nnIdxArray[n-1]);
			lenArrays[divRes.quot] = lenArrays[n-1];
		}
		n = divRes.quot + divRes.rem;
		nnIdxArray.assign(tempArray.begin(),tempArray.end());
		tempArray.clear();
	}


	//for (int i = 0; i<polyLineArray->GetSize(); i++)
	//{

	//}



	if (nnIdx)
	{
		delete [] nnIdx;
		nnIdx = NULL;
	}
	if (lenArrays)
	{
		delete [] lenArrays;
		lenArrays = NULL;
	}
	if (kdTree)
	{
		delete kdTree;
		kdTree = 0;
	}
	if (transPointsToGrid)
	{
		delete transPointsToGrid;
		transPointsToGrid = 0;
	}
	annDeallocPts(dataPts);
	annDeallocPt(queryPt);
	//annClose();									// done with ANN

	
	return ret;
}
*/

// #################################################################################################


int *CBaristaProject::selectGCPPoints(CXYZPointArray* dBPoints)
{
	CReferenceSystem refSys(55,SOUTHERN);
	refSys.setCoordinateType(eGRID);
	CTrans3D* transPointsToGrid = NULL;
	ANNkd_tree *kdTree = NULL;
	bool ret = true;
	ANNpointArray	dataPts = NULL; //annAllocPts(dBPoints->GetSize(),2);
	int *index = NULL;
	CXYZPointArray transformedGCPs;
	//vector <ANNidxArray> nnIdxArray; // array of index lists of selected GCP Points from gcpDB, one list for each rectangular strip;
	

	if (dBPoints)
	{
		transPointsToGrid = CTrans3DFactory::initTransformation(dBPoints->getRefSys(),refSys);
		if (transPointsToGrid)
		{
			transPointsToGrid->transformArray(&transformedGCPs, dBPoints);
			dataPts = annAllocPts(transformedGCPs.GetSize(),2);
			for (int i = 0; i < transformedGCPs.GetSize(); i++)
			{
				CXYZPoint* point = transformedGCPs.getAt(i);
				dataPts[i][0] = point->x;
				dataPts[i][1] = point->y;
			}	
			kdTree = new ANNkd_tree(dataPts, transformedGCPs.GetSize(), 2, 5); // 5 for the best splitting suggested by the author of ANN library

		}
	}
	else
	{
		return index;
	}

	CXYZPolyLineArray *polyLineArray = project.getXYZPolyLines();

	ANNpoint queryPt;
	queryPt = annAllocPt(2);
	ANNidxArray nnIdx = NULL; // array of indices of selected GCP Points from gcpDB;

	index = new int[transformedGCPs.GetSize()];
	for (int i = 0; i<transformedGCPs.GetSize(); i++) // all points are initially de-selected
		index[i] = 0;

	for (int i = 0; i<polyLineArray->GetSize(); i++)
	{
		CXYZPolyLine *polyLine = polyLineArray->getAt(i);		

		// assuming each polyline is a rectangle, we get its center as the center of the circular search space
		queryPt[0] = (polyLine->getPoint(0)->x + polyLine->getPoint(2)->x)/2;
		queryPt[1] = (polyLine->getPoint(0)->y + polyLine->getPoint(2)->y)/2;
		double sqRad = (queryPt[0] - polyLine->getPoint(0)->x) * (queryPt[0] - polyLine->getPoint(0)->x) + (queryPt[1] - polyLine->getPoint(0)->y) * (queryPt[1] - polyLine->getPoint(0)->y);
		int k = kdTree->annkFRSearch(queryPt,sqRad,0);
		nnIdx = new ANNidx[k];
		k = kdTree->annkFRSearch(queryPt,sqRad,k,nnIdx);
		
		for (int j = 0; j<k; j++) 
			index[nnIdx[j]] = 1; // point is selected
	}
	
	// four points of the large strip
	// now corners are manually selected 
	// 4 points in any strip is defined as a sequnce: UL->UR->LR->LL
	double Pul [2] = {polyLineArray->getAt(0)->getPoint(0)->x, polyLineArray->getAt(0)->getPoint(0)->y}; // upper-left corner
	double Pur [2] = {polyLineArray->getAt(3)->getPoint(1)->x, polyLineArray->getAt(3)->getPoint(1)->y}; // upper-right corner
	double Plr [2] = {polyLineArray->getAt(39)->getPoint(2)->x, polyLineArray->getAt(39)->getPoint(2)->y}; // lower-right corner
	double Pll [2] = {polyLineArray->getAt(36)->getPoint(3)->x, polyLineArray->getAt(36)->getPoint(3)->y}; // lower-left corner
	
	for (int i = 0; i<transformedGCPs.GetSize(); i++)
	{
		if (index[i]) // check if a selected point is within the large strip-area
		{
			double P [2] = {transformedGCPs.getAt(i)->x,transformedGCPs.getAt(i)->y}; // transferred point wrt Pul (origin, P1)
			bool flag = isInsideQuadrangle(Pul,Pur,Plr,Pll,P);
			if (!flag)
				index[i] = 0;			
		}
	}

	
	if (nnIdx)
	{
		delete [] nnIdx;
		nnIdx = NULL;
	}
	if (kdTree)
	{
		delete kdTree;
		kdTree = 0;
	}
	if (transPointsToGrid)
	{
		delete transPointsToGrid;
		transPointsToGrid = 0;
	}
	annDeallocPts(dataPts);
	annDeallocPt(queryPt);
	//annClose();									// done with ANN

	
	return index;
}

// #################################################################################################

bool CBaristaProject::isInsideQuadrangle(const double P1 [], const double P2 [], const double P3 [], const double P4 [], double P [])
{
	// calculate (twice, multiplied by 2) areas of clockwise triangles with P
	// for clockwise areas (of all 4) triangles (P1P2P, P2P3P, P3P4P, P4P1P) should be -ve, if P is inside the quadrangle
	// for anti-clockwise all areas should be +ve, if P is inside the quadrangle
	double a1 = P2[0]*P[1] - P2[1]*P[0] - P1[0]*P[1] + P1[1]*P[0] + P1[0]*P2[1] - P1[1]*P2[0];
	double a2 = P3[0]*P[1] - P3[1]*P[0] - P2[0]*P[1] + P2[1]*P[0] + P2[0]*P3[1] - P2[1]*P3[0];
	double a3 = P4[0]*P[1] - P4[1]*P[0] - P3[0]*P[1] + P3[1]*P[0] + P3[0]*P4[1] - P3[1]*P4[0];
	double a4 = P1[0]*P[1] - P1[1]*P[0] - P4[0]*P[1] + P4[1]*P[0] + P4[0]*P1[1] - P4[1]*P1[0];
	
	if (a1<0 && a2<0 && a3<0 && a4<0)
		return true;
	else
		return false;
}

// #################################################################################################
double CBaristaProject::getProjectGSDValue()
{	
	double gsd=0.0;
	if (this->getNumberOfImages()>0)
	{
		gsd = this->getImageAt(0)->getApproxGSD(100);
		project.setGSDValue(gsd);
	}
	return gsd;
};

// #################################################################################################
void CBaristaProject::saveGCPDbase()
{
	FILE *fp = fopen(project.getChipDbFileName()->GetChar(),"w");

	vector<CGCPDataBasePoint>::iterator it3d;
	for (it3d = gcpDB.begin() ; it3d < gcpDB.end(); it3d++ )
	{
		//access 2d point array from 3d vector, that means from gcpDB
		vector<CGCPData2DPoint> P2ds = it3d->getGCPPoint2DArray();

		//now go through all 2d elements
		vector<CGCPData2DPoint>::iterator it;
		for (it = P2ds.begin() ; it < P2ds.end(); it++ )
		{
			fprintf(fp,"%s,%f,%f,%f,%f,%f,%f,%s,%s,%s,%s,%f,%f,%f,%f,%s,%s,%s,%s,%s,%d,%s,%d,%s,%s,%s,%d,%s\n",
				it->getChipID().GetChar(), //0
				it3d->getX(),//1
				it3d->getY(),//2
				it3d->getZ(),//3
				it3d->getSX(),//4
				it3d->getSY(),//5
				it3d->getSZ(),//6
				it->getPlatform().GetChar(),//7
				it->getSensor().GetChar(),//8
				it->getAcquiredDate().GetChar(),//9
				it->getDateCreation().GetChar(),//10
				it->getQualityOfDefinition(),//11
				it->getViewAngle(),//12
				it->getAccuracy(),//13
				it->getAccuracyZ(),//14
				it->getUnit().GetChar(),//15
				it->getSourceOfHeight().GetChar(),//16
				it->gethDatum().GetChar(),//17
				it->getvDatum().GetChar(),//18
				it->getProjection().GetChar(),//19
				it->getZone(),//20
				it->getepsgCode().GetChar(),//21
				it->getchipSize(),//22
				it->getdataProvider().GetChar(),//23
				it->getLicense().GetChar(),//24
				(const char*)it->getOrthoImageFileName(),//25
				it->getnBands(),//26
				it->getComments().GetChar());//27
		}

	}//for it3 ends
	fclose(fp);
}

// #################################################################################################
bool CBaristaProject::openGCPDbase(CFileName& filename)

{
	FILE *fp = fopen(filename,"r");

	char string [1000];
	char str1[28][100];	//number of fields in each record is 27

	if (!fp)
		return false;

	//clear all previous database points
	this->gcpDB.clear();
	//remove all previous xyzGCPchipPoints
	this->xyzGCPchipPoints.RemoveAll();
	this->xyzGCPchipPoints.setCoordinateType(eGEOCENTRIC);
	//clear any chip selection
	this->chipSelectionArray.clear();

	for (;;)
    {
        if (!fgets(string,999,fp))//!file.ReadLine(str))
        break;
		
		int foundAt, prevFound;
		char * pch;
		pch=strchr(string,',');
		int ind = 0;
		int im = 0; //total items initially 0
		while (pch!=NULL && im < 28)
		{
			foundAt = pch-string; //number of characters count by sutracting address/pointers			
			strncpy(str1[ind],string,foundAt);
			str1[ind][foundAt]  = '\0';
			strcpy(string, pch+1);
			pch=strchr(string,',');
			ind++;
			im++;
		}
		strcpy(str1[ind],string);

		//clear 2d vector at gcpPoint
		this->gcpPoint.setGCPPoint2DArrayClear();

		//2d point info
		CGCPData2DPoint gcp2DPoint;
		gcp2DPoint.setChipID(str1[0]);
		this->gcpPoint.setX(atof(str1[1]));
		this->gcpPoint.setY(atof(str1[2]));
		this->gcpPoint.setZ(atof(str1[3]));
		this->gcpPoint.setSX(atof(str1[4]));
		this->gcpPoint.setSY(atof(str1[5]));
		this->gcpPoint.setSZ(atof(str1[6]));
		gcp2DPoint.setPlatform(str1[7]);
		gcp2DPoint.setSensor(str1[8]);
		gcp2DPoint.setDate(str1[9]);
		gcp2DPoint.setDateCreation(str1[10]);
		gcp2DPoint.setQualityOfDefinition(atof(str1[11]));
		gcp2DPoint.setViewAngle(atof(str1[12]));
		gcp2DPoint.setAccuracy(atof(str1[13]));
		gcp2DPoint.setAccuracyZ(atof(str1[14]));
		gcp2DPoint.setUnit(str1[15]);
		gcp2DPoint.setSourceOfHeight(str1[16]);
		gcp2DPoint.sethDatum(str1[17]);
		gcp2DPoint.setvDatum(str1[18]);
		gcp2DPoint.setProjection(str1[19]);		
		gcp2DPoint.setZone(atoi(str1[20]));
		gcp2DPoint.setepsgCode(str1[21]);	
		gcp2DPoint.setchipSize(atoi(str1[22]));
		gcp2DPoint.setdataProvider(str1[23]);		
		gcp2DPoint.setLicense(str1[24]);	
		gcp2DPoint.setOrthoImageFileName((CFileName)str1[25]);	
		gcp2DPoint.setnBands(atoi(str1[26]));
		gcp2DPoint.setComments(str1[24]);			
		
		//generate label				
		char buf1[17], buf2[12];
		strcpy(buf1,"CHIP_");
		this->setCurrentChipID(atol(str1[0]));
		sprintf(buf2,"%u",this->getCurrentChipID());
		CLabel label(strcat(buf1,buf2));
		
		gcp2DPoint.setLabel(label);

		//assign this 2d point to 3d point
		this->gcpPoint.set2DPoint(gcp2DPoint);
		this->gcpPoint.setLabel(label);
		this->gcpPoint.setCoordinateType(eGEOCENTRIC);
		this->gcpPoint.setDate(str1[9]);
		this->gcpPoint.setFromGPS(true);
		
		
		//store in GCP DB
		this->getGCPdb()->push_back(this->gcpPoint);
		//save selection flag to true by default
		this->chipSelectionArray.push_back(true);

		//add to project's xyzGCPchipPoints list
		CXYZPoint objPoint;
		objPoint.setX(atof(str1[1]));
		objPoint.setY(atof(str1[2]));
		objPoint.setZ(atof(str1[3]));
		objPoint.setSX(atof(str1[4]));
		objPoint.setSY(atof(str1[5]));
		objPoint.setSZ(atof(str1[6]));
		objPoint.setLabel(label);
		this->addGCPchipPoint(objPoint);		
		//if ( project.getNumberOfGCPchipPoints() == 1 ) baristaProjectHelper.refreshTree();	
	}	
	fclose(fp);

	this->setCurrentChipID(atol(str1[0])+1);
	
	return true;
}

void CBaristaProject::addToChipDB(CGCPData2DPoint P2d, CGCPDataBasePoint P3d)
{
	//clear 2d vector at gcpPoint
	//this->gcpPoint.setGCPPoint2DArrayClear();
	P3d.setGCPPoint2DArrayClear();
	P3d.set2DPoint(P2d);
	//store in GCP DB
	this->getGCPdb()->push_back(P3d);
}