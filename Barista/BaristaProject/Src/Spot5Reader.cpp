#include "Spot5Reader.h"
#include "utilities.h"

#include "XMLParser.h"
#include "Spot5XMLDataHandler.h"
#include "XMLErrorHandler.h"
#include "BundleHelper.h"
#include "OrbitObservations.h"
#include "RotationBaseFactory.h"
#include "OrbitAttitudeModel.h"
#include "AdjustMatrix.h"
#include "OrbitPathModel.h"
#include "CCDLine.h"

CSpot5Reader::CSpot5Reader(void) : CSatelliteDataReader()
{
}

CSpot5Reader::~CSpot5Reader(void)
{
}


bool CSpot5Reader::readData(CFileName &filename)
{
	if (this->aimedRMSAttitudes < 0.0 || this->aimedRMSPath < 0.0)
	{
		this->errorMessage = "No thresholds for RMS set!";
		return false;
	}


	if (this->listener)
	{
		this->listener->setProgressValue(10);
		this->listener->setTitleString("Parsing Metadata File ...");
	}




	// parse the file and get all the needed information
	CXMLParser parser;
	
	// the parser will delete the memory of the dataHandler!!
	parser.parseFile(filename,new CSpot5XMLDataHandler(this->dataStorage), new CXMLErrorHandler());


	this->satInfo.nCols = this->dataStorage.ncols;
	this->satInfo.nRows = this->dataStorage.nrows;
	this->satInfo.imageID = this->dataStorage.dataStripID;
	this->satInfo.imageType = this->dataStorage.nbands == 1 ? "P" : "Multi";
	this->satInfo.cameraName = "Spot5-" + this->satInfo.imageType;

	// compute time of first line (in UTC!)
	this->firstLineTime = this->dataStorage.sceneCenterTime 
						- this->dataStorage.linePeriode* this->dataStorage.sceneCenterLine;
	this->lastLineTime = this->dataStorage.sceneCenterTime 
						+ this->dataStorage.linePeriode* this->dataStorage.sceneCenterLine;

	this->timePerLine = this->dataStorage.linePeriode;



	if (this->listener)
	{
		this->listener->setProgressValue(20.0);
		this->listener->setTitleString("Processing Look Angles ...");
		this->setMaxListenerValue(80);
	}	

	if (!this->prepareLookAngles())
		return false;


	this->camera->setNCols(this->dataStorage.ncols);
	this->camera->setNRows(this->dataStorage.nrows);



	this->orbitPoints->initObservations();
	this->orbitPoints->setFileName(filename);

	this->attitudes->initObservations();
	this->attitudes->setFileName(filename);


	this->setMaxListenerValue(99);
	if (this->listener)
		this->listener->setTitleString("Computing Orbit ...");


	double startTime,endTime;
	this->computeInterval(startTime,endTime);

	if(!this->adjustOrbit(true,true,startTime,endTime))
		return false;

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
		this->listener->setTitleString("Finish Reading");
	}

	return true;

}

bool CSpot5Reader::prepareEphemeris()
{
	int sizeTriode = this->dataStorage.triodeOrbitPoints.GetSize();
	int sizeDoris = this->dataStorage.dorisOrbitpoints.GetSize();

	CXYZOrbitPointArray tmpPoints;
	int indexDoris=0;
	CXYZOrbitPoint* triode;
	CXYZOrbitPoint *doris;

	// correct the time offset --> doris is now in UTC;
	// transform to meter
	for (int i=0; i < sizeDoris; i++)
	{
		CXYZOrbitPoint *p = this->dataStorage.dorisOrbitpoints.getAt(i);
		p->dot -=this->dataStorage.timeOffsetDorisTriode;
		(*p)[0] /= 100.0;
		(*p)[1] /= 100.0;
		(*p)[2] /= 100.0;
		(*p)[3]	/= 1000.0;
		(*p)[4] /= 1000.0;
		(*p)[5] /= 1000.0;
	}

	// merge triode and doris points
	for (int i=0; i < sizeTriode && sizeDoris > 0 ; i++)
	{	
		triode= this->dataStorage.triodeOrbitPoints.getAt(i);
		for (int k=indexDoris; k < sizeDoris; k++)
		{
			doris =  this->dataStorage.dorisOrbitpoints.getAt(k);
			if ( doris->dot >= triode->dot)
				break;
			else
			{
				indexDoris++;			
				tmpPoints.add(*doris);

			}
		}
		
		if (doris->dot == triode->dot)
		{}
		else
			tmpPoints.add(*triode);
	}


	int size = tmpPoints.GetSize();
	if (size < 3)
	{
		this->errorMessage = "Not enough orbit points found!";
		return false;
	}

	// determine index of last point before the scene actually starts
	int indexFirstPoint;
	for (indexFirstPoint=0; indexFirstPoint < size; indexFirstPoint++)
	{
		if (tmpPoints.GetAt(indexFirstPoint)->dot >= this->firstLineTime)
			break;
	}

	indexFirstPoint--; // that's the last point before the scene

	if (indexFirstPoint < 3 || (size -indexFirstPoint) < 4)
	{
		this->errorMessage = "Not enough orbit points before scene starts found!";	
		return false;	// we need 4 points at both ends to use lagrange
	}

	// it's better to use one more point before the scene, if possible
	if ((indexFirstPoint -1) >= 3)
		indexFirstPoint--;


	// determine index of first point after the scene ends
	int indexLastPoint;
	for (indexLastPoint = size-1; indexLastPoint >= 0; indexLastPoint--)
	{
		if (tmpPoints.getAt(indexLastPoint)->dot < this->lastLineTime)
			break;
	}
	
	indexLastPoint++;

	if (indexLastPoint < 3 || (size -indexLastPoint) < 4)
	{
		this->errorMessage = "Not enough orbit points after scene ends found!";	
		return false;	// we need 4 points at both ends to use lagrange
	}

	// try one point more, if possible
	if ((size - (indexLastPoint + 1) ) >= 4 )
		indexLastPoint++;

	if (indexLastPoint <= indexFirstPoint)
	{
		this->errorMessage = "Wrong order of orbit points!";	
		return false;
	}



	// interpolate a point every sec and fill orbitPointArray

	// now interpolate between first and last point
	double t[8];
	double x[8];
	double y[8];
	double z[8];
	double vx[8];
	double vy[8];
	double vz[8];


	for (int offset = indexFirstPoint; offset < indexLastPoint ; offset++)
	{
		// fill vectors for lagrange
		for (int i=0;i<8;i++)
		{
			CXYZOrbitPoint* p =  tmpPoints.getAt(offset-3+i);
			x[i]= (*p)[0];
			y[i]= (*p)[1];
			z[i]= (*p)[2];
			vx[i]= (*p)[3];
			vy[i]= (*p)[4];
			vz[i]= (*p)[5];
			t[i]= p->dot;
		}

		CXYZOrbitPoint* before = tmpPoints.getAt(offset);
		CXYZOrbitPoint* after = tmpPoints.getAt(offset+1);
		double timeBefore = before->dot;
		double timeAfter =  after->dot;
		double step = 1.0;

		CXYZOrbitPointArray* newOrbitPoints = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();
		// interpolate between 2 known points every sec
		for (double time = timeBefore; time <= timeAfter; time+=step)
		{
			CXYZOrbitPoint* p = newOrbitPoints->add();	
			p->dot = time;
			(*p)[0] = this->lagrange(t,x,time);
			(*p)[1] = this->lagrange(t,y,time);
			(*p)[2] = this->lagrange(t,z,time);
			p->setCovarElement(0,0,0.75);
			p->setCovarElement(1,1,0.75);
			p->setCovarElement(2,2,0.75);

			if (before->getHasVelocity() && after->getHasVelocity())
			{
				(*p)[3] = this->lagrange(t,vx,time);
				(*p)[4] = this->lagrange(t,vy,time);
				(*p)[5] = this->lagrange(t,vz,time);
				p->setHasVelocity(true);
			}
			else
				p->setHasVelocity(false);
		}
		
		if (this->listener)
		{
			double per = (double) offset / (double) (indexLastPoint- indexFirstPoint);
			this->listener->setProgressValue(per*this->getMaxListenerValue());
		}
	}


	CXYZOrbitPointArray* newOrbitPoints = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();
	FILE* out= fopen("spot_path.txt","w");
	for (int i=0; i < newOrbitPoints->GetSize(); i++)
	{
		CXYZOrbitPoint* p = newOrbitPoints->getAt(i);
		fprintf(out,"%20.15lf %20.15lf %20.15lf %20.7lf\n",(*p)[0],(*p)[1],(*p)[2],p->dot);
	}
	fclose(out);



	return true;
}

double CSpot5Reader::lagrange(double x[],double y[],double time)
{

	double erg=0.0;
	int i,j;
	for (j=0; j < 8; j++)
	{
		double tmp=1.0;
		for (i= 0; i < 8; i++)
		{
			if (i==j)
				continue;

			tmp*= (time - x[i])/(x[j]-x[i]);
		}
		erg+=tmp*y[j];
	}
	return erg;
}


bool CSpot5Reader::prepareAttitudes()
{
	COrbitAttitudeModel* modelAttitudes = (COrbitAttitudeModel*) this->attitudes->getCurrentSensorModel();
	COrbitPathModel* modelPath = (COrbitPathModel*) this->orbitPoints->getCurrentSensorModel();


	// get the attitudes, give in YPR notation!
	// we have to transform the given spot angles to the barista notation -> RPY
	// therefore we have to change to angle order
	// spot  barista
	//  R --> P  
	//  P --> R
	//  Y --> -Y
	// this gives us the matrix Rspot(YPR) = RTbarista(RPY)
	// -> to use the same matrix transpose R(RPY) !!
		
	// order from the spot file
		// x -> Y
		// y -> P
		// z -> R

		// barista order
		// x -> R
		// y -> P
		// z -> Y
	CObsPointArray* originalYPRAngles = &this->dataStorage.correctedAngles;
	

	// this is the time where we compute the fixed Matrix!
	double halfTime = this->dataStorage.sceneCenterTime;

	
	
	// the number of old YPR Angles
	int nOriginalYPR = originalYPRAngles->GetSize();

	CObsPointArray originalRPYAnglesCorrected(3);
	CObsPoint * tmpPoint = NULL;
	bool foundTime = false;
	double fixedTime;
	CObsPoint newRPYPoint(3);

	FILE* out1= fopen("spot_oldYPR.txt","w");
		
	// in this loop we correct the time offset and swap the angles!
	// time of the corrected angles is in UTC !!
	for (int i=0; i< nOriginalYPR; i++)
	{
		tmpPoint = originalYPRAngles->GetAt(i);
		
		// correct the time
		newRPYPoint.dot = tmpPoint->dot ;
 

		// swap angles
		newRPYPoint[0] = (*tmpPoint)[1];
		newRPYPoint[1] = (*tmpPoint)[2];
		newRPYPoint[2] = -(*tmpPoint)[0];

		fprintf(out1,"%20.15lf %20.15lf %20.15lf %20.7lf\n",newRPYPoint[0],newRPYPoint[1],newRPYPoint[2],newRPYPoint.dot);

		// save the closest point to the half time
		if (!foundTime && newRPYPoint.dot > halfTime)
		{
			fixedTime = newRPYPoint.dot;
			foundTime = true;
		}
		
		// store the corrected angles in the new array
		originalRPYAnglesCorrected.Add(newRPYPoint);

	}

	fclose(out1);

	// if we have found a point close to the half time, save the time and continue
	if (!foundTime)
	{
		this->errorMessage = "Not enough orbit attitudes found!";	
		return false;
	}

	// remember!! at this point the corrected angles defining the transposed Matrix Rspot(YPR),
	// so we have to use the transposed one later!!!


	// computation of the fixed Rotation Matrix
	// due to different times in attitudes and orbit points we have
	// to interpolate the orbit point to get it at the same time as the angles
	C3DPoint location;
	C3DPoint velocity;
	modelPath->evaluate(location,velocity,fixedTime);
	
	CRotationMatrix fixedMatrix;
	// get the tangeltial matrix at the "fixedTime"


	C3DPoint X,Y,Z;

	Z = location /  location.getNorm();
	Y = Z.crossProduct(velocity);
	Y = Y / Y.getNorm();
	X = Z.crossProduct(Y);

	fixedMatrix(0,0) = X[0];
	fixedMatrix(1,0) = X[1];
	fixedMatrix(2,0) = X[2];

	fixedMatrix(0,1) = Y[0];
	fixedMatrix(1,1) = Y[1];
	fixedMatrix(2,1) = Y[2];

	fixedMatrix(0,2) = Z[0];
	fixedMatrix(1,2) = Z[1];
	fixedMatrix(2,2) = Z[2];





	modelAttitudes->setFixedECRMatrix(fixedMatrix);	// save Rfixed!
	modelAttitudes->setTimeFixedECRMatrix(fixedTime); // and the time




	
	CXYZOrbitPointArray* oa = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();
	double startTime = oa->getAt(0)->dot;
	double endTime = oa->getAt(oa->GetSize()-1)->dot;
	CRotationMatrix tmpMat1,tmpMat2;
	CObsPoint* dummy = NULL;

	CRotationMatrix ephemMatrix;

	// we will save the computed angles directly in the attitude observation handler
	CXYZOrbitPointArray* newRPYAngles = (CXYZOrbitPointArray*)this->attitudes->getObsPoints();
	newRPYAngles->setCreateWithVelocity(false);
	// we use this object to transform the new Matrix in angles (and the old one!)
	CRotationBase* newRPY = CRotationBaseFactory::initRotation(&C3DPoint(),RPY);
	CRotationMatrix &newRPYMatrix = newRPY->getRotMat();

	CRotationMatrix rotXY(0,1,0,-1,0,0,0,0,-1);

	for (int i=0; i <  nOriginalYPR ; i++)
	{
		// transform the adjusted angles into a rotation matrix
		dummy = originalRPYAnglesCorrected.GetAt(i);
		// make sure that we cn interpolate a orbit point
		if (dummy->dot < startTime)
			continue;
		else if (dummy->dot > endTime)
			break;

		// we have to transpose this matrix due to spot -> barista transformation (YPR -> RPY)!!
		newRPY->updateAngles_Rho(dummy);

		// get the location and velocity at the same time
		if (!modelPath->evaluate(location,velocity,dummy->dot))
			continue;
		
		// compute rotation matrix from location and velocity
		this->computeTangentialMatrix(ephemMatrix,location,velocity);
	
		// compute new RPY Matrix
		// newRPYMatrix is a reference to the old angles at this point, so we use the transposed version!
		newRPYMatrix.RT_times_R(tmpMat1,rotXY);
			
		ephemMatrix.R_times_R(tmpMat2,tmpMat1); 
		fixedMatrix.RT_times_R(newRPYMatrix,tmpMat2); // now newRPYMatrix contains the transformed Matrix

		// and update the angles
		newRPY->computeParameterFromRotMat();

		CXYZOrbitPoint* angles = newRPYAngles->add();
		for (int k=0; k < 3; k++)
			(*angles)[k] = (*newRPY)[k];
		angles->dot = dummy->dot;	

		(*angles) *= 1000.0;

		angles->setCovarElement(0,0,0.00001);
		angles->setCovarElement(1,1,0.00001);
		angles->setCovarElement(2,2,0.00001);

		(*angles->getCovariance()) *= 1000000.0;

		if (this->listener)
		{
			double per = (double) i / (double)nOriginalYPR;
			this->listener->setProgressValue(per*this->getMaxListenerValue());
		}

	}

	delete newRPY;
	

	FILE* out= fopen("spot_newRPY.txt","w");
	for (int i=0; i < newRPYAngles->GetSize(); i++)
	{
		CXYZOrbitPoint* p = newRPYAngles->getAt(i);
		fprintf(out,"%20.15lf %20.15lf %20.15lf %20.7lf\n",(*p)[0],(*p)[1],(*p)[2],p->dot);
	}
	fclose(out);


	return true;
}

bool CSpot5Reader::prepareLookAngles()
{

	FILE *out = fopen("lookAngles.txt","w");
	for (int i=0; i < this->dataStorage.lookAngles.size();i++)
	{
		fprintf(out,"Band %d\n",i+1);
		for (int k=0; k < this->dataStorage.lookAngles[i].GetSize();k++)
		{
			C2DPoint * p = this->dataStorage.lookAngles[i].GetAt(k);
			fprintf(out,"%6d   %10.6lf    %10.6lf\n",p->id,p->x,p->y);
		}
	}
	fclose(out);



	int nBands = this->dataStorage.lookAngles.size() > 1 ? 3 : 1;
	int nObs = 0;

	// compute the direction vector and save the values
	vector<vector<C3DPoint> > directionVector(3);
	for (int i=0; i < nBands; i++)
	{
		nObs += this->dataStorage.lookAngles[i].GetSize();
		directionVector[i].resize(this->dataStorage.lookAngles[i].GetSize());
		for (int k=0; k < this->dataStorage.lookAngles[i].GetSize(); k++)
		{
			C2DPoint *p = this->dataStorage.lookAngles[i].GetAt(k);
			(directionVector[i])[k].x = tan(p->x);
			(directionVector[i])[k].y = tan(p->y);
			(directionVector[i])[k].z = -1.0;
		}

	}
	

	// ####################################################################
	// get start values for the adjustment

	// the start values for the rotation matrix
	C3DPoint X(0,1,0), Y(1,0,0), Z((directionVector[0])[this->dataStorage.lookAngles[0].GetSize() / 2]);
	Z = Z / Z.getNorm();
	X = Y.crossProduct(Z);
	X = X / X.getNorm();
	Y = Z.crossProduct(X);

	CRotationMatrix initMat(X,Y,Z);
	CRollPitchYawRotation rotRPY(initMat);
	rotRPY[0] = M_PI;
	rotRPY.computeRotMatFromParameter();

	Matrix A_Start(4,3);
	Matrix dl_Start(4,1);
	A_Start=0;
	dl_Start =0;

	// the first CCD element( corrected with rot mat)
	C3DPoint p1;
	rotRPY.RT_times_x(p1,(directionVector[0])[0]);
	p1 /= -p1.z;

	// the first and last look angle of the first band
	// p0
	A_Start.element(0,0) = 1.0;
	A_Start.element(0,2) = p1.x;
	A_Start.element(1,1) = 1.0;
	A_Start.element(1,2) = p1.y;
	dl_Start.element(0,0) = 0;

	// the las CCD element
	rotRPY.RT_times_x(p1,(directionVector[0])[this->dataStorage.lookAngles[0].GetSize()-1]);
	p1 /= -p1.z;

	A_Start.element(2,0) = 1.0;
	A_Start.element(2,2) = p1.x;
	A_Start.element(3,1) = 1.0;
	A_Start.element(3,2) = p1.y;
	dl_Start.element(2,0) = this->dataStorage.lookAngles[0].GetSize()-1;	

	Matrix N_Start = A_Start.t() * A_Start;
	Matrix t_Start = A_Start.t() * dl_Start;
	Matrix dx_Start;
	try 
	{
		Matrix Qxx = N_Start.i();
		dx_Start = Qxx * t_Start;
	}
	catch (...)
	{
		this->errorMessage = "Processing of Look Angles failed";
		return false;
	}



	// ####################################################################
	// compute mounting and xc,yc, c from look angles
	double redfac = 0.01;

	Matrix dx(6,1);
	Matrix Qxx;

	dx.element(0,0) = rotRPY[0];
	dx.element(1,0) = rotRPY[1];
	dx.element(2,0) = rotRPY[2];

	dx.element(3,0) = dx_Start.element(0,0) * redfac;
	dx.element(4,0) = dx_Start.element(1,0) * redfac;
	dx.element(5,0) = dx_Start.element(2,0) * redfac;
	
	double vTvOld = 0.0;
	double vTv = 100.0;

	C2DPoint *p;
	CAdjustMatrix dFdAngles(3,3);
	C3DPoint F;

	double tmp;
	double fac1;
	double fac2;
	double fac3;
	
	double count=0.0;

	CAdjustMatrix A(2,6);
	CAdjustMatrix dl(2,1,0.0);

	double listenerStartValue =0.0;
	if (this->listener)
		listenerStartValue = this->listener->getProgress();

	CRotationBase * rotBase = NULL;
	// iteration loop
	for (int j=0; (j < 20) && (fabs((vTv - vTvOld) / (vTv + vTvOld)) > 0.0001); j++)
	{
		vTvOld = vTv;
		vTv = 0.0;
		Matrix N(6,6);
		N=0;
		Matrix t(6,1);
		t=0;

		C3DPoint rpy(dx.element(0,0),dx.element(1,0),dx.element(2,0));
		rotBase = CRotationBaseFactory::initRotation(&rpy,RPY);
		CAdjustMatrix dRdAngles(9,3);

		rotBase->computeDerivation(dRdAngles);

	
		for (int band=0; band < nBands; band++)
		{
			int nLookAngles = this->dataStorage.lookAngles[band].GetSize();
			count++;
			
			// fill N Matrix
			for (int i=0; i< nLookAngles; i++)
			{				
				// compute derivations dF/dAngles
				dFdAngles(0,0) = (directionVector[band])[i].x * dRdAngles(0,0) + (directionVector[band])[i].y * dRdAngles(3,0) + dRdAngles(6,0);
				dFdAngles(0,1) = (directionVector[band])[i].x * dRdAngles(0,1) + (directionVector[band])[i].y * dRdAngles(3,1) + dRdAngles(6,1);
				dFdAngles(0,2) = (directionVector[band])[i].x * dRdAngles(0,2) + (directionVector[band])[i].y * dRdAngles(3,2) + dRdAngles(6,2);

				dFdAngles(1,0) = (directionVector[band])[i].x * dRdAngles(1,0) + (directionVector[band])[i].y * dRdAngles(4,0) + dRdAngles(7,0);
				dFdAngles(1,1) = (directionVector[band])[i].x * dRdAngles(1,1) + (directionVector[band])[i].y * dRdAngles(4,1) + dRdAngles(7,1);
				dFdAngles(1,2) = (directionVector[band])[i].x * dRdAngles(1,2) + (directionVector[band])[i].y * dRdAngles(4,2) + dRdAngles(7,2);

				dFdAngles(2,0) = (directionVector[band])[i].x * dRdAngles(2,0) + (directionVector[band])[i].y * dRdAngles(5,0) + dRdAngles(8,0);
				dFdAngles(2,1) = (directionVector[band])[i].x * dRdAngles(2,1) + (directionVector[band])[i].y * dRdAngles(5,1) + dRdAngles(8,1);
				dFdAngles(2,2) = (directionVector[band])[i].x * dRdAngles(2,2) + (directionVector[band])[i].y * dRdAngles(5,2) + dRdAngles(8,2);


				rotBase->RT_times_x(F,(directionVector[band])[i]);

				tmp = 1.0/F.z;
				fac1 =  -dx.element(5,0)*tmp;
				fac2 = -fac1*F.x*tmp;
				fac3 = -fac1*F.y*tmp;

				
				A(0,0) = fac1 * dFdAngles(0,0) + fac2 * dFdAngles(2,0);
				A(0,1) = fac1 * dFdAngles(0,1) + fac2 * dFdAngles(2,1);
				A(0,2) = fac1 * dFdAngles(0,2) + fac2 * dFdAngles(2,2);

				A(1,0) = fac1 * dFdAngles(1,0) + fac3 * dFdAngles(2,0);
				A(1,1) = fac1 * dFdAngles(1,1) + fac3 * dFdAngles(2,1);
				A(1,2) = fac1 * dFdAngles(1,2) + fac3 * dFdAngles(2,2);

				A(0,3) = 1.0;
				A(0,4) = 0.0;
				A(0,5) = -F.x*tmp;

				A(1,4) = 1.0;
				A(1,3) = 0.0;
				A(1,5) = -F.y*tmp;


				CAdjustMatrix AT = A;
				AT.transpose();

				CAdjustMatrix ATPA = AT * A;
				ATPA.addToMatrix(0,0,N);

				
				dl(0,0) = i *redfac - (dx.element(3,0) + dx.element(5,0) * A(0,5));
				dl(1,0) = -(dx.element(4,0) + dx.element(5,0) * A(1,5));
				vTv += dl(0,0) * dl(0,0) + dl(1,0) * dl(1,0);
			
				CAdjustMatrix ATdl = AT * dl;
				ATdl.addToMatrix(0,0,t);

			}

		N.element(0,0) += this->dataStorage.ncols;

		}
		Qxx = N.i();
		dx += Qxx * t;

		delete rotBase;
		rotBase = NULL;

		if (this->listener)
		{
			double per = listenerStartValue + ((double) (j+1)/8.0 ) *(this->listenerMaxValue - listenerStartValue );
			this->listener->setProgressValue(per);
		}


	}



	CXYZPoint irp(dx.element(3,0) / redfac,dx.element(4,0) / redfac,dx.element(5,0) / redfac);
	this->camera->setIRP(irp);
	this->camera->setScale(0.001);
	
	C3DPoint angles(dx.element(0,0),dx.element(1,0),dx.element(2,0));

	CRollPitchYawRotation rotMat(&angles);

	this->cameraMounting->setRotation(rotMat.getRotMat());
	
	this->cameraMounting->setShift(CXYZPoint(0.0,0.0,0.0));


	double viewAngle = this->cameraMounting->getRotation()[0];

	if (viewAngle < 0) viewAngle +=2*M_PI;

/*
	if ( (viewAngle + 0.09) < (M_PI) )
		this->satInfo.viewDirection = "Forward";
	else if ( (viewAngle - 0.09) > (M_PI) )
		this->satInfo.viewDirection = "Backward";
	else
		this->satInfo.viewDirection = "Nadir";
*/

	this->satInfo.viewDirection = "Spot5-" + this->satInfo.imageID;






	double s0 = sqrt(vTv / (2*nObs - 5));
	C3DPoint sig(s0 * sqrt(Qxx.element(3,3)) / redfac, s0 * sqrt(Qxx.element(4,4)) / redfac, s0 * sqrt(Qxx.element(5,5)) / redfac);
	C3DPoint sigr(s0 * sqrt(Qxx.element(0,0)), s0 * sqrt(Qxx.element(1,1)),s0 * sqrt(Qxx.element(2,2)));


	FILE* outQxx;
	outQxx = fopen("Qxx.txt","w");

	fprintf(outQxx,"s0 : %20.3lf\n",s0);
	fprintf(outQxx,"x: %20.3lf   sx : %20.3lf\n",irp.x,sig.x);
	fprintf(outQxx,"y: %20.3lf   sy : %20.3lf\n",irp.y,sig.y);
	fprintf(outQxx,"c: %20.3lf   sz : %20.3lf\n",irp.z,sig.z);
	fprintf(outQxx,"roll : %20.15lf   sroll  : %20.15lf\n",angles.x,sigr.x);
	fprintf(outQxx,"pitch: %20.15lf   spitch : %20.15lf\n",angles.y,sigr.y);
	fprintf(outQxx,"yaw  : %20.15lf   syaw   : %20.15lf\n",angles.z,sigr.z);

	for (int i = 0; i < Qxx.Nrows(); i++)
	{
		for (int k=0; k < Qxx.Ncols(); k++)
		{
			fprintf(outQxx,"%20.3lf",Qxx(i+1,k+1));
		}
		fprintf(outQxx,"\n");
	}
	
	fprintf(outQxx,"\ncorrelations\n\n");

	for (int i = 0; i < Qxx.Nrows(); i++)
	{
		for (int k=0; k < Qxx.Ncols(); k++)
		{
			fprintf(outQxx,"%20.3lf",Qxx(i+1,k+1)/(sqrt(Qxx(i+1,i+1))*sqrt(Qxx(k+1,k+1))));
		}
		fprintf(outQxx,"\n");
	}
	fclose(outQxx);





	C3DPoint rpy(dx.element(0,0),dx.element(1,0),dx.element(2,0));
	rotBase = CRotationBaseFactory::initRotation(&rpy,RPY);

	vTv =0.0;
	FILE *out1 = fopen("v.txt","w");
	for (int band=0; band < nBands; band++)
	{
		int nLookAngles = this->dataStorage.lookAngles[band].GetSize();

		for (int i=0; i< nLookAngles; i++)
		{
			
			C2DPoint *p = this->dataStorage.lookAngles[band].GetAt(i);
			
			C3DPoint F;
			rotBase->RT_times_x(F,(directionVector[band])[i]);

			double tmp = 1.0/F.z;
			double fac1 =  -dx.element(5,0)*tmp;
			double fac2 = -fac1*F.x*tmp;
			double fac3 = -fac1*F.y*tmp;

			dl(0,0) = i *redfac - (dx.element(3,0) - dx.element(5,0) *  F.x*tmp);
			dl(1,0) = -(dx.element(4,0) - dx.element(5,0) * F.y*tmp);
			fprintf(out1,"%30.20lf  %30.20lf\n",dl(0,0),dl(1,0));
			vTv += dl(0,0) * dl(0,0) + dl(1,0) * dl(1,0);
		}
	}

	fprintf(out1,"vTPv %30.20lf",vTv);
	fclose(out1);
	delete rotBase;
	rotBase = NULL;

	return true;
}


void CSpot5Reader::computeTangentialMatrix(CRotationMatrix& mat,const C3DPoint& location, const C3DPoint& velocity)
{// rotates from tangential system to ECR 
	C3DPoint X,Y,Z;

	Z = location /  location.getNorm();
	X = velocity.crossProduct(Z);
	X = X / X.getNorm();
	Y = Z.crossProduct(X);

	mat(0,0) = X[0];
	mat(1,0) = X[1];
	mat(2,0) = X[2];

	mat(0,1) = Y[0];
	mat(1,1) = Y[1];
	mat(2,1) = Y[2];

	mat(0,2) = Z[0];
	mat(1,2) = Z[1];
	mat(2,2) = Z[2];

}

