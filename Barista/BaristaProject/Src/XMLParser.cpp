#include "XMLParser.h"

#include "XMLParser_intern.h"


bool CXMLParser::parseFile(CFileName filename,CXMLDataHandler* dataHandler,CXMLErrorHandler* errorHandler)
{
    bool ret = false;

	if (dataHandler && errorHandler)
	{
		CXMLParser_intern::init();	// this has to be done static

		CXMLParser_intern* parser = new CXMLParser_intern(dataHandler,errorHandler);
		ret = parser->parseFile(filename.GetChar());
		delete parser;

		CXMLParser_intern::terminate(); // this has to be done static and after deleting the SAXParser
	}

	return ret;
}