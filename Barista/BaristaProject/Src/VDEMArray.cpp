#include "VDEMArray.h"
#include "VDEMPtrArray.h"
#include "VDEM.h"
#include "Serializable.h"
#include "StructuredFileSectionArray.h"

CVDEMArray::CVDEMArray(int nGrowBy) : 
	CMUObjectArray<CVDEM, CVDEMPtrArray>(nGrowBy)
{
}

CVDEMArray::~CVDEMArray(void)
{
}

/**
 * Serialized storage of a CVDEMArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVDEMArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    for (int i = 0; i < this->GetSize(); i++)
    {
        CVDEM* vdem = this->GetAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        vdem->serializeStore(newSection);
    }
}

/**
 * Restores a CVDEMArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVDEMArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();
    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);
        CVDEM* vdem = this->Add();
        vdem->serializeRestore(child);
    }
}

/**
 * reconnects pointers
 */
void CVDEMArray::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CVDEMArray::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Flag if CVDEMArray is modified
 * returns bool
 */
bool CVDEMArray::isModified()
{
    for (int i = 0; i < this->GetSize(); i++)
    {
        if (this->GetAt(i)->isModified())
            return true;
    }

    return false;
}


/** SLOW FOR LOTS OF DEMs!
 * Gets a dem by label (null if not found)
 *
 * @param CCharString Label of dem
 */
int CVDEMArray::getbyName(const CCharString &name) const
{
    CVDEM* test = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        test = this->GetAt(i);

		if (name.CompareNoCase(test->getFileName()->GetChar()))
            return i;

        continue;
    }

    return -1;
}

/** SLOW FOR LOTS OF DEMs!
 * Gets a dem by label (null if not found)
 *
 * @param CCharString Label of dem
 */
CVDEM* CVDEMArray::getDEMbyName(const CCharString &name) const
{
    CVDEM* test = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        test = this->GetAt(i);
		if (name.CompareNoCase(test->getFileName()->GetChar()))
            return test;

        continue;
    }

    return NULL;
}