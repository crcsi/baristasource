#include "VImageArray.h"
#include "VImagePtrArray.h"
#include "VImage.h"
#include "Serializable.h"
#include "StructuredFileSectionArray.h"

CVImageArray::CVImageArray(int nGrowBy) : 
	CMUObjectArray<CVImage, CVImagePtrArray>(nGrowBy)
{

}

CVImageArray::~CVImageArray(void)
{
}

CVImage* CVImageArray::Add()
{
	CVImage* image = CMUObjectArray<CVImage, CVImagePtrArray>::Add();
	image->setParent(this);
	return image;
}

CVImage* CVImageArray::Add(const CVImage& oldImage)
{
	CVImage* image = CMUObjectArray<CVImage, CVImagePtrArray>::Add(oldImage);
	image->setParent(this);

	return image;
}


/**
 * Serialized storage of a CVImageArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVImageArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    for (int i = 0; i < this->GetSize(); i++)
    {
        CVImage* image = this->GetAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        image->serializeStore(newSection);
    }
}

/**
 * Restores a CVImageArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVImageArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();
    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);
        CVImage* image = this->Add();
        image->serializeRestore(child);
    }
}

/**
 * reconnects pointers
 */
void CVImageArray::reconnectPointers()
{
    CSerializable::reconnectPointers();

	for (int i=0; i< this->GetSize();i++)
	{
		CVImage* image = this->GetAt(i);
		image->reconnectPointers();
	}
}

/**
 * resets pointers
 */
void CVImageArray::resetPointers()
{
    CSerializable::resetPointers();

	for (int i=0; i< this->GetSize();i++)
	{
		CVImage* image = this->GetAt(i);
		image->resetPointers();
	}
}

/**
 * Flag if CVImageArray is modified
 * returns bool
 */
bool CVImageArray::isModified()
{
    for (int i = 0; i < this->GetSize(); i++)
    {
        if (this->GetAt(i)->isModified())
            return true;
    }

    return false;
}


/** SLOW FOR LOTS OF Images!
 * Gets a image by label (null if not found)
 *
 * @param CCharString Label of image
 */
int CVImageArray::getbyName(CCharString* name) const
{
    CVImage* test = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        test = this->GetAt(i);

		if (name->CompareNoCase(test->getFileNameChar()))
            return i;

        continue;
    }

    return -1;
}

/** SLOW FOR LOTS OF Images!
 * Gets a image by label (null if not found)
 *
 * @param CCharString Label of image
 */
CVImage* CVImageArray::getImagebyName(CCharString* name) const
{
    CVImage* test = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        test = this->GetAt(i);
		if (name->CompareNoCase(test->getFileNameChar()))
            return test;

        continue;
    }

    return NULL;
}