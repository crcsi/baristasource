#include "OrbitObservations.h"
#include "XYZOrbitPointArray.h"
#include "Filename.h"


COrbitObservations::COrbitObservations(void): splineModel(0),splineType(Spline),
	indexFirstPoint(-1),indexLastPoint(-1)
{
	this->orbitPoints.SetSize(0);
	this->obsPoints = &this->orbitPoints;
	this->splineModel = CSplineModelFactory::initSplineModel(Spline);
	this->currentSensorModel = this->splineModel;

	this->splineModel->setParent(this);
	this->orbitPoints.setParent(this);
}

COrbitObservations::COrbitObservations(SplineModelType type): splineModel(0),splineType(type),
		indexFirstPoint(-1),indexLastPoint(-1)
{
	
	this->orbitPoints.SetSize(0);
	this->obsPoints = &this->orbitPoints;
	this->splineModel = CSplineModelFactory::initSplineModel(type);
	this->currentSensorModel = this->splineModel;
	
	this->splineModel->setParent(this);
	this->orbitPoints.setParent(this);
}

COrbitObservations::~COrbitObservations()
{
	if (this->currentSensorModel)
		delete this->currentSensorModel;
	this->currentSensorModel = NULL;
}

void COrbitObservations::goingToBeDeleted()
{
	if (this->splineModel)
		this->splineModel->goingToBeDeleted();
}

COrbitObservations::COrbitObservations(const COrbitObservations& orbitObservation) : CObservationHandler(orbitObservation),
	splineType(orbitObservation.splineType),indexFirstPoint(orbitObservation.indexFirstPoint),indexLastPoint(orbitObservation.indexLastPoint)
{
	/*this->currentSensorModel = this->splineModel;
	this->obsPoints = &this->orbitPoints;
	this->orbitPoints.SetSize(0);
	*/

	this->splineModel = CSplineModelFactory::initSplineModel(splineType);
	this->splineModel->setParent(this);
	this->orbitPoints.setParent(this);
}

const char *COrbitObservations::getFileNameChar() const
{
	return this->name.GetChar();
}

void COrbitObservations::setFileName(const char* filename)
{
    this->name = filename;
}

void COrbitObservations::setName(CCharString name)
{
	this->name = name;
	if (this->currentSensorModel)
		this->currentSensorModel->setFileName(name.GetChar());
}


CXYZOrbitPointArray* COrbitObservations::initObservations()
{
	this->orbitPoints.SetSize(0);
	return &this->orbitPoints;
}


void COrbitObservations::setSplineModelParameter(int splineDegree,doubles& knots)
{
	this->splineModel->initSpline(splineDegree,(CFileName)this->name,knots);
	
	int size = this->orbitPoints.GetSize();
	int mid = this->indexLastPoint - this->indexFirstPoint;
	if (size > 0 && mid > 2)
	{
		CObsPoint* tmp = this->orbitPoints.GetAt(this->indexFirstPoint + mid/2);		
		this->splineModel->setOffsetSpline(*tmp);
	}
}





void COrbitObservations::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("Name", this->name.GetChar());

	token = pStructuredFileSection->addToken();
    token->setValue("SplineModelType", this->splineType);

	token = pStructuredFileSection->addToken();
    token->setValue("indexFirstPoint", this->indexFirstPoint);

	token = pStructuredFileSection->addToken();
    token->setValue("indexLastPoint", this->indexLastPoint);

	CStructuredFileSection* pNewSection;

	pNewSection = pStructuredFileSection->addChild();
	this->orbitPoints.serializeStore(pNewSection);

	pNewSection = pStructuredFileSection->addChild();
	this->splineModel->serializeStore(pNewSection);

}

void COrbitObservations::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("Name"))
            this->name = token->getValue();
		else if(key.CompareNoCase("name2"))
            this->name = token->getValue(); // this is actually obsolete, but required for old bar files
		else if (key.CompareNoCase("SplineModelType"))
			this->splineType = (SplineModelType) token->getIntValue();
		else if (key.CompareNoCase("indexFirstPoint"))
			this->indexFirstPoint = token->getIntValue();
		else if (key.CompareNoCase("indexLastPoint"))
			this->indexLastPoint = token->getIntValue();
	}

	CCharString modelName(this->splineType == Path ? "COrbitPathModel" : "COrbitAttitudeModel");
	
	bool idOldProject = false;

	for (int j = 0; j < pStructuredFileSection->getChildren()->GetSize(); j++)
	{
		CStructuredFileSection* pChild = pStructuredFileSection->getChildren()->GetAt(j);

		if (pChild->getName().CompareNoCase("CXYZOrbitPointArray"))
		{
			this->orbitPoints.serializeRestore(pChild);
		}
		else if (pChild->getName().CompareNoCase(modelName))
		{
			if (this->splineType == Attitude)
			{
				// reset the scale
				// if this is a new project then the scale should be different to 1.0 after restoring
				this->splineModel->setScale(1.0); 
				
				this->splineModel->serializeRestore(pChild);

				// still 1.0 means that we have an old project
				if (fabs(this->splineModel->getScale() - 1.0) < 0.0000001)
					idOldProject = true;

			}
			else
				this->splineModel->serializeRestore(pChild);

		}
	}

	if (this->indexFirstPoint == -1)
		this->indexFirstPoint = 0;
	
	if (this->indexLastPoint == -1)
		this->indexLastPoint = this->orbitPoints.GetSize() -1;

	if (idOldProject)
	{
		// old projects still use rad instead of mrad
		// change the observationss
		for (int i=0; i < this->orbitPoints.GetSize(); i++)
		{
			CObsPoint* p = this->orbitPoints.GetAt(i);
			(*p) *= 1000.0;
			(*p->getCovariance()) *= 1000000.0;
		}

		// set the scale now
		this->splineModel->setScale(1000.0);
	}


}

bool COrbitObservations::isModified()
{

	return this->bModified;
}

void COrbitObservations::reconnectPointers()
{
	CSerializable::reconnectPointers();
	this->orbitPoints.reconnectPointers();
	this->splineModel->reconnectPointers();
}

void COrbitObservations::resetPointers()
{
	CSerializable::resetPointers();
	this->orbitPoints.resetPointers();
	this->splineModel->resetPointers();
}

int COrbitObservations::getNOrbitPoints() const 
{ 
	// we have to compute the number of points for every point separat
	int size = this->orbitPoints.GetSize();
	int n=0;

	for (int i=0 ; i < size; i++)
		if (i >= this->indexFirstPoint && i <= this->indexLastPoint)
			n += this->orbitPoints.GetAt(i)->getDimension();
	
	return n;

}