#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>


#include "GeoEye1Reader.h"

#include "OrbitObservations.h"
#include "QuaternionRotation.h"
#include "OrbitAttitudeModel.h"
#include "CCDLine.h"

#include "utilities.h"
#include "SystemUtilities.h"



CGeoEye1Reader::CGeoEye1Reader(void): CSatelliteDataReader(),ephemerisFile(""),attitudeFile(""),geoCalFile(""),imageMetaFile(""),
imgSensorMetaFile(""),quaternions(4),focalLength(0.0),DetectorSize(0.0),polyCoffx0(0),polyCoffx1(0),polyCoffy0(0),
polyCoffy1(0),isReverse(false)
{
}

CGeoEye1Reader::~CGeoEye1Reader(void)
{
}


bool CGeoEye1Reader::readData(CFileName &filename)
{

	if (this->aimedRMSAttitudes < 0.0 || this->aimedRMSPath < 0.0)
	{
		this->errorMessage = "No thresholds for RMS set!";
		return false;
	}

	if (this->listener)
	{
		this->listener->setProgressValue(0.0);
		this->listener->setTitleString("Reading Dataset File ...");
	}

	char buffer[1000];

	FILE* in = fopen(filename.GetChar(),"r");

	if (!in) 
	{
		this->errorMessage.Format("File: %s not found!",filename.GetFullFileName().GetChar());	
		return false;
	}

	CCharString NoExtName=filename.GetFileName();

	while (fgets(buffer,1999,in))
	{
		CCharString b(buffer);
		b.trim();

		if (b.Find("SingleOrganization_license.txt") == 0)
			break;


		if (b.Find(NoExtName+".att") != -1)
		{
			this->attitudeFile = filename.GetPath() + CSystemUtilities::dirDelimStr + NoExtName +
				CSystemUtilities::dirDelimStr + "Volume1" + CSystemUtilities::dirDelimStr + NoExtName+ ".att";
		}

		else if (b.Find(NoExtName+".eph") != -1)
		{
			this->ephemerisFile = filename.GetPath() + CSystemUtilities::dirDelimStr + NoExtName +
				CSystemUtilities::dirDelimStr + "Volume1" + CSystemUtilities::dirDelimStr + NoExtName+ ".eph";

		}

		else if (b.Find(NoExtName+".pvl") != -1)
		{
			this->imgSensorMetaFile = filename.GetPath() + CSystemUtilities::dirDelimStr + NoExtName +
				CSystemUtilities::dirDelimStr + "Volume1" + CSystemUtilities::dirDelimStr + NoExtName+ ".pvl";

		}
	}

	// check if we found all the files
	if (this->ephemerisFile.IsEmpty() || this->attitudeFile.IsEmpty() ||
		this->imgSensorMetaFile.IsEmpty())//|| this->imageMetaFile.IsEmpty())
	{
		this->errorMessage = "Not all needed files found!";	
		return false;
	}

	// init the observation handler
	this->orbitPoints->initObservations();
	this->orbitPoints->setFileName(this->ephemerisFile);


	this->attitudes->initObservations();
	this->attitudes->setFileName(this->attitudeFile);


	// start reading the files
	if (this->listener)
	{
		this->setMaxListenerValue(0);
		this->listener->setTitleString("Reading Ephemeris File ...");
	}


	if (!this->readEphemeris())
		return false;

	if (this->listener)
	{
		this->setMaxListenerValue(10);
		this->listener->setTitleString("Reading Attitude File ...");
	}

		if (!this->readAttitudes())
			return false;

	// added for GeoEye1
	if (!this->readImgSensorMetaFile())
		return false;

	if (this->listener)
	{
		this->setMaxListenerValue(99);
		this->listener->setTitleString("Computing Orbit ...");
	}

	double startTime,endTime;
	this->computeInterval(startTime,endTime);

	if(!this->adjustOrbit(true,true,startTime,endTime))
		return false;


	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
		this->listener->setTitleString("Finish Reading");
	}
	return true;
}



bool CGeoEye1Reader::readEphemeris()
{

	//Reading a binary file 

	fstream in(this->ephemerisFile.GetChar(), ios::in|ios::binary);
	
     
	if (!in.good())
	{
		this->errorMessage.Format("File: %s not found!",this->ephemerisFile.GetFullFileName().GetChar());		
		return false;
	}
	
	CXYZOrbitPointArray* oa = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();

	// clear the array
	oa->SetSize(0);

	char buffer[160];
	double start_time = 0.0;
	double timeInterval = 0.0;
	int nEphemeris=0;		
		
	in.seekg(64 * sizeof(char), ios::beg);
	in.read(buffer, sizeof(double));
	string strInfo(buffer);
	stringstream streamInfo;
	streamInfo.str(strInfo);
	
	double hour, min, sec;
	char place=':';
	streamInfo>>hour>>place>>min>>place>>sec;
	start_time = hour*3600 + min*60 + sec;

	in.read(buffer, sizeof(double));
	strInfo.assign(buffer);
	streamInfo.str(strInfo);

	double duration;
	streamInfo>>duration;

	in.seekg(108, ios::beg);
	in.read(buffer,12);
	strInfo.assign(buffer);
	streamInfo.str(strInfo);
	streamInfo>>nEphemeris;
    
	timeInterval = duration/nEphemeris;
	
	int count=0;
	int numPars= 19;
	
	//ofstream XYZ ("f:\\GeoEye1_HobartTriplet\\Ephemlist.txt");
	//char comma=',';

	// Read the actual data
	while (count < nEphemeris)
	{

		// assigning values to parameters
		vector <double> ephParms(numPars,0);
		
		for (int i=0; i < ephParms.size(); i++)
		{
			in.read(reinterpret_cast<char *>(&ephParms[i]), sizeof(double));
			this->littleToBigEndian(&ephParms[i]);
		}

		CXYZOrbitPoint* p = oa->add(); 
		double qp11,qp12,qp13,qp22,qp23,qp33;	 // co-variance values for position
		double qv11,qv12,qv13,qv22,qv23,qv33;	 // co-variance values for velocity

		timeInterval = ephParms[0]; // read from the meta data file
		(*p)[0] = ephParms[1];
		(*p)[1] = ephParms[2];
		(*p)[2] = ephParms[3];
		(*p)[3] = ephParms[4];
		(*p)[4] = ephParms[5];
		(*p)[5] = ephParms[6];
		qp11 = ephParms[7];
		qp12 = ephParms[8];
		qp13 = ephParms[9];
		qp22 = ephParms[10];
		qp23 = ephParms[11];
		qp33 = ephParms[12];
		qv11 = ephParms[13];
		qv12 = ephParms[14];
		qv13 = ephParms[15];
		qv22 = ephParms[16];
		qv23 = ephParms[17];
		qv33 = ephParms[18];
		
		// writing to a file
		
	//	XYZ<<fixed<<setprecision (12)<<endl;

	//	XYZ<<ephParms[0]<<comma<<" "<<ephParms[1]<<comma<<" "<<ephParms[2]<<comma<<" "<<ephParms[3]<<comma<<" "<<ephParms[4]<<comma<<" "<<ephParms[5]<<comma<<" "<<ephParms[6]<<comma;
	//	XYZ<<fixed<<setprecision (1);
	//	XYZ<<ephParms[7]<<comma<<" "<<ephParms[8]<<comma<<" "<<ephParms[9]<<comma<<" "<<ephParms[10]<<comma<<" "<<ephParms[11]<<comma<<" "<<ephParms[12]<<endl;


		Matrix *cov = p->getCovariance();
		cov->element(0,0) = qp11;
		cov->element(0,1) = cov->element(1,0) = qp12;
		cov->element(0,2) = cov->element(2,0) = qp13;
		cov->element(1,1) = qp22;
		cov->element(1,2) = cov->element(2,1) = qp23;
		cov->element(2,2) = qp33;

		int k=3;
		cov->element(k+0,k+0) = qv11;
		cov->element(k+0,k+1) = cov->element(k+1,k+0) = qv12;
		cov->element(k+0,k+2) = cov->element(k+2,k+0) = qv13;
		cov->element(k+1,k+1) = qv22;
		cov->element(k+1,k+2) = cov->element(k+2,k+1) = qv23;
		cov->element(k+2,k+2) = qv33;

		p->setTime(0,0,start_time + timeInterval);
		p->setHasVelocity(false); // to improve the accuracy spline interpolation


		if (this->listener != NULL)
		{
			double per = (double)count / (double) nEphemeris;
			this->listener->setProgressValue(per * this->listenerMaxValue);
		}

		count++;
	}
//	XYZ.close();
	in.close();
	return true;
}

bool CGeoEye1Reader::readAttitudes()
{

	//Reading a binary file 

	fstream in(this->attitudeFile.GetChar(), ios::in|ios::binary);


	if (!in.good())
	{
		this->errorMessage.Format("File: %s not found!",this->attitudeFile.GetFullFileName().GetChar());		
		return false;
	}

	this->quaternions.SetSize(0);

	char buffer[160];
	double start_time = 0.0;
	double timeInterval = 0.0;
	int nAttitudes=0;		

	in.seekg(64 * sizeof(char), ios::beg);
	in.read(buffer, sizeof(double));
	string strInfo(buffer);
	stringstream streamInfo;
	streamInfo.str(strInfo);

	double hour, min, sec;
	char place=':';
	streamInfo>>hour>>place>>min>>place>>sec;
	start_time = hour*3600 + min*60 + sec;

	in.read(buffer, sizeof(double));
	strInfo.assign(buffer);
	streamInfo.str(strInfo);

	double duration;
	streamInfo>>duration;

	in.seekg(108, ios::beg);
	in.read(buffer,12);
	strInfo.assign(buffer);
	streamInfo.str(strInfo);
	streamInfo>>nAttitudes;

	timeInterval = duration/nAttitudes;	 //calculated but not used 

	int count=0;
	int numPars= 15;

	// Read the actual data
	while (count < nAttitudes)
	{

		// assigning values to parameters
		vector <double> ephParms(numPars,0);

		for (int i=0; i < ephParms.size(); i++)
		{
			in.read(reinterpret_cast<char *>(&ephParms[i]), sizeof(double));
			this->littleToBigEndian(&ephParms[i]);
		}

		double q11,q12,q13,q14,q22,q23,q24 ,q33,q34,q44;	 // co-variance values for quaternion
		CObsPoint* p = this->quaternions.Add();

		timeInterval = ephParms[0]; // read from the meta data file
		(*p)[0] = ephParms[1];
		(*p)[1] = ephParms[2];
		(*p)[2] = ephParms[3];
		(*p)[3] = ephParms[4];
		q11 = ephParms[5];
		q12 = ephParms[6];
		q13 = ephParms[7];
		q14 = ephParms[8];
		q22 = ephParms[9];
		q23 = ephParms[10];
		q24 = ephParms[11];
		q33 = ephParms[12];
		q34 = ephParms[13];
		q44 = ephParms[14];

		
		Matrix *cov = p->getCovariance();
		cov->element(0,0) = q11;
		cov->element(0,1) = cov->element(1,0) = q12;
		cov->element(0,2) = cov->element(2,0) = q13;
		cov->element(0,3) = cov->element(3,0) = q14;
		cov->element(1,1) = q22;
		cov->element(1,2) = cov->element(2,1) = q23;
		cov->element(1,3) = cov->element(3,1) = q24;
		cov->element(2,2) = q33;
		cov->element(2,3) = cov->element(3,2) = q34;
		cov->element(3,3) = q44;
		p->dot = start_time + timeInterval;

		if (this->listener != NULL)
			{
				double per = (double)count / (double) nAttitudes;
				this->listener->setProgressValue(per * this->listenerMaxValue);
			}

		count++;
	}

	in.close();

	return true;
}


// Read image & sensor (camera) model information
bool CGeoEye1Reader::readImgSensorMetaFile()
{

	FILE* in = fopen(this->imgSensorMetaFile.GetChar(),"r");
	if (!in) 
	{
		this->errorMessage.Format("File: %s not found!",this->imgSensorMetaFile.GetFullFileName().GetChar());	
		return false;
	}


	char buffer[1000];
	bool readFocal=true;
	int found=0;
	int cols(-1),rows(-1);
	double firstTime=0.0;
	double linesPerSec=0.0;
	int count=0;


	while (fgets(buffer,1999,in))
	{
		CCharString b(buffer);
		b.trim();

		if (b.Find("productType") == 0)	 
		{
			sscanf(b.GetChar(),"productType = \"%100s",buffer);
			b=buffer;
			this->satInfo.processingLevel = b.Left(b.GetLength()-2);
			found++;
		}
		else if (b.Find("uniqueImageId") == 0)	 
		{
			sscanf(b.GetChar(),"uniqueImageId = \"%100s",buffer);
			b=buffer;
			this->satInfo.imageID = b.Left(b.GetLength()-2);
			found++;
		}
		else if (b.Find("numberOfSpectralBands") == 0)	 
		{

			sscanf(b.GetChar(),"numberOfSpectralBands = %d",&nbands);

			if (nbands == 1) this->satInfo.imageType = "P";
			else if (nbands == 4) this->satInfo.imageType = "Multi";
			else this->errorMessage.Format("Image type not known. Look at the file: %s ",
				this->imgSensorMetaFile.GetFullFileName().GetChar());
			found++;

			this->satInfo.cameraName = "GeoEye1-" + this->satInfo.imageType;
		}
		else if ((b.Find("focalLength") == 0) && readFocal)	
		{
			sscanf(b.GetChar(),"focalLength = %lf",&focalLength);
			readFocal=false;
			found++;
			count++;
		}
		else if (b.Find("idsNumberScanLines") == 0)
		{
			sscanf(b.GetChar(),"idsNumberScanLines = %d",&rows);
			found++;
		}
		else if (b.Find("idsPixelsPerScanLine") == 0)
		{
			sscanf(b.GetChar(),"idsPixelsPerScanLine = %d",&cols);
			found++;
		}
		else if ((b.Find("focalLengthStdDev") == 0) && !readFocal)	
		{
			sscanf(b.GetChar(),"focalLengthStdDev = %lf",&focalLengthStdDev);
			found++;
			count++;
		}
		else if (b.Find("idsLineSpacing") == 0)
		{
			sscanf(b.GetChar(),"idsLineSpacing = %lf",&ResolutionY);
			found++;
		}
		else if (b.Find("idsPixelSpacing") == 0)
		{
			sscanf(b.GetChar(),"idsPixelSpacing = %lf",&ResolutionX);
			found++;
		}
		else if (b.Find("scanDirection") == 0)
		{
			CCharString scanDirection="";

			sscanf(b.GetChar(),"scanDirection = %100s",&buffer);
			scanDirection = buffer;
			if (scanDirection.Find("Reverse") >= 0)
				this->isReverse = true;
			else
				this->isReverse = false; // forward as default
			found++;
		}
		else if (b.Find("lineSampleTime") == 0)
		{
			sscanf(b.GetChar(),"lineSampleTime = %lf",&linePeriode);
			found++;
		}
		else if (b.Find("firstLineAcquisitionDateTime") == 0)
		{
			int day, month, year, hour, min;
			double sec;	
			sscanf(b.GetChar(),"firstLineAcquisitionDateTime = %4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
			firstTime = hour*3600 + min*60 + sec;
			found++;
		}
		else if (b.Find("lineRate") == 0)
		{
			sscanf(b.GetChar(),"lineRate = %lf",&linesPerSec);
			found++;
		}
		else if (b.Find("firstLineRollAngle") == 0)
		{
			double firstLineRollAngle=0.0;

			sscanf(b.GetChar(),"firstLineRollAngle = %lf",&firstLineRollAngle);
			if (firstLineRollAngle > 0) this->satInfo.viewDirection="GeoEye1-Forward";
			else if (firstLineRollAngle < 0) this->satInfo.viewDirection="GeoEye1-Backward";
			else if (firstLineRollAngle == 0) this->satInfo.viewDirection="GeoEye1-Nadir";
			found++;
		}
		else if (b.Find("bitsPerPixel") == 0)
		{
			sscanf(b.GetChar(),"bitsPerPixel = %d",&bpp);
			found++;
		}
		else if (count == 3)
		{
			sscanf(b.GetChar(),"%lf",&polyCoffx0);
			found++;
			count++;
		}
		else if (count == 4)
		{
			sscanf(b.GetChar(),"%lf",&polyCoffx1);
			found++;
			count++;
		}
		else if (count == 8)
		{
			sscanf(b.GetChar(),"%lf",&polyCoffy0);
			found++;
			count++;
		}
		else if (count == 9)
		{
			sscanf(b.GetChar(),"%lf",&polyCoffy1);
			found++;
			count++;
		}
		else if (count == 14)
		{
			sscanf(b.GetChar(),"%lf",&StdDevPolyCoffx0);
			found++;
			count++;
		}
		else if (count == 15)
		{
			sscanf(b.GetChar(),"%lf",&StdDevPolyCoffx1);
			found++;
			count++;
		}
		else if (count == 19)
		{
			sscanf(b.GetChar(),"%lf",&StdDevPolyCoffy0);
			found++;
			count++;
		}
		else if (count == 20)
		{
			sscanf(b.GetChar(),"%lf",&StdDevPolyCoffy1);
			found++;
			count++;
		}
		else if (!readFocal )
		{
			count++;
		}
	}
	fclose(in);

	if (found != 23)
	{
		this->errorMessage.Format("Not all parameter found in the file: %s ",this->imgSensorMetaFile.GetFullFileName().GetChar());
		return false;
	}

	((CCCDLine*)this->camera)->setNCols(cols);
	((CCCDLine*)this->camera)->setNRows(rows);

	this->satInfo.nRows = rows;
	this->satInfo.nCols = cols;

	if (this->isReverse)
		this->timePerLine = -1.0/linesPerSec;
	else // forward case, the usual case...
		this->timePerLine = 1.0/linesPerSec;

	this->firstLineTime = firstTime;
	this->lastLineTime = firstTime + this->timePerLine * rows;

	return true;
}


bool CGeoEye1Reader::prepareAttitudes()
{
	COrbitAttitudeModel *model = (COrbitAttitudeModel*)this->attitudes->getCurrentSensorModel();

	// this array stores the transformed rpy angles
	CXYZOrbitPointArray* newRPYAngles = (CXYZOrbitPointArray*)this->attitudes->getObsPoints();
	newRPYAngles->setCreateWithVelocity(false);

	// clear the array
	newRPYAngles->SetSize(0);


	// compute the fixed Rotation Matrix at R(T/2) and save it in the attitude model
	CXYZOrbitPointArray* orbitPointArray = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();	

	// take the middle as a first guess for the middle time
	int middleIndex = orbitPointArray->GetSize()/2;
	CXYZOrbitPoint* point = orbitPointArray->getAt(middleIndex);
	double middleTime = this->firstLineTime + (this->lastLineTime - this->firstLineTime) / 2.0;

	if (point->dot < middleTime)
		while (middleIndex < orbitPointArray->GetSize() && orbitPointArray->getAt(middleIndex)->dot < middleTime)
			middleIndex++;
	else 
		while (middleIndex >= 0 && orbitPointArray->getAt(middleIndex)->dot > middleTime)
			middleIndex--;


	if (middleIndex < 0 || middleIndex >= orbitPointArray->GetSize())
	{
		this->errorMessage.Format("No Attitude Data Points found!");
		return false;
	}

	point = orbitPointArray->getAt(middleIndex);
	point->setHasVelocity(true);

	C3DPoint location((*point)[0],(*point)[1],(*point)[2]);
	C3DPoint velocity((*point)[3],(*point)[4],(*point)[5]);
	CRotationMatrix fixedMatrix;

	C3DPoint X,Y,Z; // page 3 of Photogrammetric Record Paper

	Z = -location / location.getNorm();
	X = velocity.crossProduct(Z);
	X = X / X.getNorm();
	Y = Z.crossProduct(X);

	/*Z = -location / location.getNorm();
	Y = Z.crossProduct(velocity);
	Y = Y / Y.getNorm();
	X = Y.crossProduct(Z);*/


	fixedMatrix(0,0) = X[0];
	fixedMatrix(1,0) = X[1];
	fixedMatrix(2,0) = X[2];

	fixedMatrix(0,1) = Y[0];
	fixedMatrix(1,1) = Y[1];
	fixedMatrix(2,1) = Y[2];

	fixedMatrix(0,2) = Z[0];
	fixedMatrix(1,2) = Z[1];
	fixedMatrix(2,2) = Z[2];

	model->setFixedECRMatrix(fixedMatrix);	// save Rfixed!
	model->setTimeFixedECRMatrix(point->dot); // and the time


	// create an empty matrix: for {Roll = 0, Pitch = 0, Yaw = 0}
	CRotationBase* newRPY = CRotationBaseFactory::initRotation(&C3DPoint(),RPY);
	// we store the result  in here and compute then the final angles
	CRotationMatrix& newRPYMatrix = newRPY->getRotMat();

	double listenerStartValue= 0.0;
	if (this->listener)
	{
		listenerStartValue = this->listener->getProgress();
	}

	CObsPoint* dummy = NULL;
	int size =this->quaternions.GetSize();
	CObsPoint *p = this->quaternions.GetAt(size/2); // just use one point
	
	CQuaternionRotation* oldQuaternion=new CQuaternionRotation(p,eGeoEye1);
	//CRotationBase* oldQuaternion = CRotationBaseFactory::initRotation(p,Quaternion);


	for (int i=0; i< size; i++)
	{
		dummy = this->quaternions.GetAt(i);
		oldQuaternion->updateAngles_Rho(dummy);

		// the multiplication
		//CRotationMatrix intermMat=oldQuaternion->getRotMat();
		//intermMat.transpose();

		fixedMatrix.RT_times_RT(newRPYMatrix,oldQuaternion->getRotMat());

		// update and save the parameter
		newRPY->computeParameterFromRotMat();

		CXYZOrbitPoint* angles = newRPYAngles->add();

		this->computeError(*angles->getCovariance(),newRPY->getRotMat(),dummy);

		for (int k=0; k < 3; k++)
			(*angles)[k] = ((*newRPY)[k] < 0.0 )&& (fabs((*newRPY)[k] + M_PI) < 1.0) ? (*newRPY)[k]+ 2*M_PI: (*newRPY)[k];
		angles->dot = dummy->dot;

		(*angles) *= 1000.0; // save in m rad
		(*angles->getCovariance()) *= 1000000; // covar as well

		/*double temp = 0;
		temp = (*angles)[0];
		(*angles)[0] = (*angles)[1];
		(*angles)[1] = temp;*/


		if (this->listener != NULL)
		{
			double per = listenerStartValue + ((double)i / (double) size) *(this->listenerMaxValue - listenerStartValue);

			this->listener->setProgressValue(per);
		}
	}

	delete oldQuaternion;
	delete newRPY;
	return true;
}


void CGeoEye1Reader::computeError(Matrix& covar,const CRotationMatrix& rpy, const CObsPoint& quaternion) const
{
	Matrix dRdQ(5,4);   // rotation matrix elements after quaternion
	Matrix dAdR(3,5);   // vector after rotation matrix
	Matrix dAdAngle(3,3);  // angles after vector

	dAdR = 0;
	dAdAngle = 0;

	dRdQ.element(0,0) = 4*quaternion[0];
	dRdQ.element(0,1) = 0;
	dRdQ.element(0,2) = 0;
	dRdQ.element(0,3) = 4*quaternion[3];

	dRdQ.element(1,0) = 2*quaternion[1];
	dRdQ.element(1,1) = 2*quaternion[0];
	dRdQ.element(1,2) = -2*quaternion[3];
	dRdQ.element(1,3) = -2*quaternion[2];

	dRdQ.element(2,0) = 2*quaternion[2];
	dRdQ.element(2,1) = 2*quaternion[3];
	dRdQ.element(2,2) = 2*quaternion[0];
	dRdQ.element(2,3) = 2*quaternion[1];

	dRdQ.element(3,0) = -2*quaternion[3];
	dRdQ.element(3,1) = 2*quaternion[2];
	dRdQ.element(3,2) = 2*quaternion[1];
	dRdQ.element(3,3) = -2*quaternion[0];

	dRdQ.element(4,0) = 0;
	dRdQ.element(4,1) = 0;
	dRdQ.element(4,2) = 4*quaternion[2];
	dRdQ.element(4,3) = 4*quaternion[3];


	dAdR.element(0,3) = 1.0/rpy(2,2);
	dAdR.element(0,4) = -rpy(2,1)/(rpy(2,2)*rpy(2,2));

	dAdR.element(1,2) = -1.0;

	dAdR.element(2,0) = 1.0/rpy(0,0);
	dAdR.element(2,1) = -rpy(1,0)/(rpy(0,0)*rpy(0,0));


	double x = rpy(2,1)/rpy(2,2);
	double z = rpy(1,0)/rpy(0,0);

	dAdAngle.element(0,0) = 1.0/(1.0 + x*x);
	dAdAngle.element(1,1) = 1.0/sqrt(1.0 - rpy(2,0));
	dAdAngle.element(2,2) = 1.0/(1.0 + z*z);

	Matrix F = dAdAngle * dAdR * dRdQ;
	covar = F * *quaternion.getCovariance() * F.t();

}



bool CGeoEye1Reader::prepareMounting()
{
	// unit matrix
	CRotationMatrix mountRotMat(1,0,0,0,1,0,0,0,1); 
	
	this->cameraMounting->setRotation(mountRotMat);

	// save the mounting  shift parameter
	C3DPoint shift (0,0,0);
	this->cameraMounting->setShift(shift);

	// init camera
	this->DetectorSize = this->polyCoffx1*1e6;
	this->camera->setScale(this->DetectorSize / 1000); // detector size in milimeter( 8 micron for geoEye1)

	//calculate projection centre's coordinate in framelet coordinate system

	double detectorIndex=0;
	double xFirstDetectorInCameraSys = (this->polyCoffx0 + this->polyCoffx1 * detectorIndex) *1e6 ; //in micron
	double yFirstDetectorInCameraSys = (this->polyCoffy0 + this->polyCoffy1 * detectorIndex) *1e6; //in micron

	double ppInFrameletX= -xFirstDetectorInCameraSys/ DetectorSize;
    double ppInFrameletY= -yFirstDetectorInCameraSys/ DetectorSize;

	// Another way: a0+a1*s=0 => s=(a0/-a1)
    /*ppInFrameletX= this->polyCoffx0/ -this->polyCoffx1;
	ppInFrameletY = 0.0;
	if (this->polyCoffy1)  ppInFrameletY= this->polyCoffy0/ -this->polyCoffy1;*/
	

	CXYZPoint irp (ppInFrameletX, ppInFrameletY ,-focalLength *1e6 / DetectorSize);
   
	this->camera->setIRP(irp);
	

	return true;
}


void CGeoEye1Reader::computeInterval(double &startTime, double& endTime)
{
	if (this->isReverse)
	{
		// in this case timePerLine is negative!
		double timeOffset = this->satInfo.nRows * PERCENTAGE_OVERLAP / 100.0 * this->timePerLine;
		startTime = this->lastLineTime + timeOffset ;
		endTime = this->firstLineTime - timeOffset ;
	}
	else
	{
		CSatelliteDataReader::computeInterval(startTime,endTime);
	}
}

void CGeoEye1Reader::littleToBigEndian(double * n)
{
	unsigned char *cptr,tmp;
	cptr = (unsigned char *) n;
	tmp = cptr[0];
	cptr[0] = cptr[7];
	cptr[7] = tmp;
	tmp = cptr[1];
	cptr[1] = cptr[6];
	cptr[6] = tmp;
	tmp = cptr[2];
	cptr[2] = cptr[5];
	cptr[5] =tmp;
	tmp = cptr[3];
	cptr[3] = cptr[4];
	cptr[4] = tmp;
}
