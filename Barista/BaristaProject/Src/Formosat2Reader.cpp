#include "Formosat2Reader.h"
#include "utilities.h"

#include "XMLParser.h"
#include "Formosat2XMLDataHandler.h"
#include "XMLErrorHandler.h"
#include "BundleHelper.h"
#include "OrbitObservations.h"
#include "RotationBaseFactory.h"
#include "OrbitAttitudeModel.h"
#include "AdjustMatrix.h"
#include "OrbitPathModel.h"
#include "CCDLine.h"

CFormosat2Reader::CFormosat2Reader(void) : CSatelliteDataReader()
{
}

CFormosat2Reader::~CFormosat2Reader(void)
{
}


bool CFormosat2Reader::readData(CFileName &filename)
{
	if (this->aimedRMSAttitudes < 0.0 || this->aimedRMSPath < 0.0)
	{
		this->errorMessage = "No thresholds for RMS set!";
		return false;
	}


	if (this->listener)
	{
		this->listener->setProgressValue(10);
		this->listener->setTitleString("Parsing Metadata File ...");
	}

	// parse the file and get all the needed information
	CXMLParser parser;

	// the parser will delete the memory of the dataHandler!
	parser.parseFile(filename,new CFormosat2XMLDataHandler(this->dataStorage), new CXMLErrorHandler());


	this->satInfo.nCols = this->dataStorage.ncols;
	this->satInfo.nRows = this->dataStorage.nrows;
	this->satInfo.imageID = this->dataStorage.dataSetName;
	this->satInfo.imageType = this->dataStorage.nbands == 1 ? "P" : "Multi";
	this->satInfo.cameraName = "Formosat2-" + this->satInfo.imageType;

	// compute time of the last line 
	this->firstLineTime = this->dataStorage.referenceTime;
	this->timePerLine = this->dataStorage.linePeriode;
	this->lastLineTime = this->dataStorage.referenceTime + this->satInfo.nRows * this->dataStorage.linePeriode;

	if (this->listener)
	{
		this->listener->setProgressValue(20.0);
		this->listener->setTitleString("Processing Look Angles ...");
		this->setMaxListenerValue(80);
	}

	if (!this->prepareLookAngles())
		return false;


	this->camera->setNCols(this->dataStorage.ncols);
	this->camera->setNRows(this->dataStorage.nrows);

	this->orbitPoints->initObservations();
	this->orbitPoints->setFileName(filename);

	this->attitudes->initObservations();
	this->attitudes->setFileName(filename);


	this->setMaxListenerValue(99);
	if (this->listener)
		this->listener->setTitleString("Computing Orbit ...");


	double startTime,endTime;
	this->computeInterval(startTime,endTime);

	if(!this->adjustOrbit(true,true,startTime,endTime))
		return false;

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
		this->listener->setTitleString("Finish Reading");
	}

	return true;

}

bool CFormosat2Reader::prepareEphemeris()
{
	int size = this->dataStorage.tmpOrbitPoints.GetSize();
	CXYZOrbitPointArray* oa = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();

	// clear the array
	oa->SetSize(0);
	FILE* out1= fopen("fs2_path.txt","w");

	for (int i = 0; i < size; i++)
	{
		CXYZOrbitPoint* p = oa->add();
		*p =  this->dataStorage.tmpOrbitPoints.getAt(i);
		p->setCovarElement(0,0,1);
		p->setCovarElement(1,1,1);
		p->setCovarElement(2,2,1);
	    p->setCovarElement(3,3,50);//Velocities are very unaccurate for corrected Orbit points
		p->setCovarElement(4,4,50);
		p->setCovarElement(5,5,50);

		fprintf(out1,"%20.15lf %20.15lf %20.15lf  %20.15lf %20.15lf %20.15lf %20.7lf\n",(*p)[0],(*p)[1],(*p)[2],(*p)[3], (*p)[4], (*p)[5], p->dot);

		if (this->listener != NULL)
		{
			double per = (double)i / (double)size;
			this->listener->setProgressValue(per * this->listenerMaxValue);
		}
	}
	fclose(out1);

	return true;
}

bool CFormosat2Reader::prepareAttitudes()
{
	COrbitAttitudeModel* modelAttitudes = (COrbitAttitudeModel*) this->attitudes->getCurrentSensorModel();
	COrbitPathModel* modelPath = (COrbitPathModel*) this->orbitPoints->getCurrentSensorModel();

	CObsPointArray* originalRPYAngles = &this->dataStorage.correctedAngles;


	// this is the time where we compute the fixed Matrix!
	double halfTime = this->dataStorage.referenceTime + this->satInfo.nRows/2 * this->dataStorage.linePeriode;

	// the number of old YPR Angles
	int nOriginalRPY = originalRPYAngles->GetSize();

	CObsPoint * tmpPoint = NULL;
	bool foundTime = false;
	double fixedTime;
	CObsPoint newRPYPoint(3);
	CObsPointArray originalRPYAnglesCorrected(3);

	FILE* out1= fopen("fs_oldRPY.txt","w");

	// in this loop we swap the angles!
	for (int i=0; i< nOriginalRPY; i++)
	{
		tmpPoint = originalRPYAngles->GetAt(i);


		// correct the time
		newRPYPoint.dot = tmpPoint->dot ;

		// swap angles
		newRPYPoint[0] = -(*tmpPoint)[0];
		newRPYPoint[1] = -(*tmpPoint)[1];
		newRPYPoint[2] = -(*tmpPoint)[2];

		fprintf(out1,"%20.15lf %20.15lf %20.15lf %20.7lf\n",newRPYPoint[0],newRPYPoint[1],newRPYPoint[2],newRPYPoint.dot);

		// save the closest point to the half time
		if (!foundTime && newRPYPoint.dot > halfTime)
		{
			fixedTime = newRPYPoint.dot;
			foundTime = true;
		}
		// store the corrected angles in the new array
		originalRPYAnglesCorrected.Add(newRPYPoint);
	}
	fclose(out1);

	// if we have found a point close to the half time, save the time and continue
	if (!foundTime)
	{
		this->errorMessage = "Not enough orbit attitudes found!";
		return false;
	}


	// computation of the fixed Rotation Matrix
	// due to different times in attitudes and orbit points we have
	// to interpolate the orbit point to get it at the same time as the angles
	C3DPoint location;
	C3DPoint velocity;
	modelPath->evaluate(location,velocity,fixedTime);

	CRotationMatrix fixedMatrix;
	// get the tangeltial matrix at the "fixedTime"
	//it is dependent on the definition of the platform coordinate system,so that the remaining Rp(t) is with small rotation angles. 
	C3DPoint X,Y,Z;

	Z = -location /  location.getNorm(); //axis Zp points downward 
	Y = Z.crossProduct(velocity);        //Xp along the velocity direction, and Yp complete the crossproduct
	Y = Y / Y.getNorm();
	X = Y.crossProduct(Z); 

	fixedMatrix(0,0) = X[0];
	fixedMatrix(1,0) = X[1];
	fixedMatrix(2,0) = X[2];

	fixedMatrix(0,1) = Y[0];
	fixedMatrix(1,1) = Y[1];
	fixedMatrix(2,1) = Y[2];

	fixedMatrix(0,2) = Z[0];
	fixedMatrix(1,2) = Z[1];
	fixedMatrix(2,2) = Z[2];

	modelAttitudes->setFixedECRMatrix(fixedMatrix);	// save Rfixed!
	modelAttitudes->setTimeFixedECRMatrix(fixedTime); // and the time

	// this array stores the transformed rpy angles
	CXYZOrbitPointArray* newRPYAngles = (CXYZOrbitPointArray*)this->attitudes->getObsPoints();
	newRPYAngles->setCreateWithVelocity(false);

	// clear the array
	newRPYAngles->RemoveAll();

	CRotationBase* newRPY = CRotationBaseFactory::initRotation(&C3DPoint(),RPY);
	CRotationMatrix &newRPYMatrix = newRPY->getRotMat();

	CRotationMatrix tmpMat1,tmpMat2;

	CObsPoint* dummy = NULL;

	CRotationMatrix rotXYZ(1,0,0,0,-1,0,0,0,1);

	for (int i=0; i < nOriginalRPY; i++)
	{
		// transform adjusted angles into a rotation matrix
		dummy = originalRPYAnglesCorrected.GetAt(i);

		newRPY->updateAngles_Rho(dummy);

		newRPYMatrix.transpose(tmpMat1); //newRPYMatrix.RT_times_R(tmpMat1,rotXYZ);   //revised 20100423		

		fixedMatrix.RT_times_R(newRPYMatrix,tmpMat1);

		// and update the angles
		newRPY->computeParameterFromRotMat();
		newRPY->computeRotMatFromParameter();

		CXYZOrbitPoint* angles = newRPYAngles->add();
		for (int k=0; k < 3; k++)
			(*angles)[k] = (*newRPY)[k];


		angles->dot = dummy->dot;

		(*angles) *= 1000.0;// save in m rad

		angles->setCovarElement(0,0,0.0004);
		angles->setCovarElement(1,1,0.0004);
		angles->setCovarElement(2,2,0.0004);

		(*angles->getCovariance()) *= 1000000.0;// save in m rad

		if (this->listener)
		{
			double per = (double) i / (double)nOriginalRPY;
			this->listener->setProgressValue(per*this->getMaxListenerValue());
		}
	}


	FILE* out2= fopen("fs2_att.txt","w");
	for (int i=0; i < newRPYAngles->GetSize(); i++)
	{
		CXYZOrbitPoint* p = newRPYAngles->getAt(i);
				fprintf(out1,"%20.15lf %20.15lf %20.15lf %20.7lf\n",(*p)[0],(*p)[1],(*p)[2],p->dot);
	}
	fclose(out2);

	delete newRPY;
	return true;
}



bool CFormosat2Reader::prepareLookAngles()
{

	int nBands = this->dataStorage.nbands;// 1 for PAN
	int nseg=200;
	int nObs = nseg+1; //this->dataStorage.ncols;// for PAN
	double norm;

	// compute the transformation matrix from LOS(framelet) to PIP(body)
	//CRotationMatrix rotBias;
	//this->computeLOS2PIPMatrix(rotBias);
	C3DPoint angles_RPY(-dataStorage.biases[0],-dataStorage.biases[1],-dataStorage.biases[2]);
	CRollPitchYawRotation rotBias(&angles_RPY);

	// compute the direction vector and save the values
	vector<vector<C3DPoint> > directionVector(3);
	C3DPoint oldDirection;

	C2DPointArray lookAngles;	
	FILE *out1 = fopen("lookAngles.txt","w");
	for (int i=0; i < nBands; i++)
	{
		directionVector[i].resize(nObs);
		for(int seg=0;seg<=nseg;seg++)
		//for (int k = 0; k < nObs; k++)
		{
			int k=dataStorage.ncols*seg/nseg;

			C2DPoint * pa=lookAngles.Add();
			pa->id = k;
	
			// polynomial expressions of pixel k in band i in RLOS reference frame (framelet)
			//pa->y =(-0.343-0.074*(k+1)/12000)/1000;
			//pa->x =(-2.118 -(1.908*pow(10.0,-13)*pow(float(k+1),3))+3.435*pow(10.0,-9)*pow(float(k+1),2)+2.233*pow(10.0,-3)*(k+1)-13.48)/1000;
			pa->x = (dataStorage.lookx0+dataStorage.lookx1*k+dataStorage.lookx2*pow(float(k),2)+dataStorage.lookx3*pow(float(k),3))/1000.0; //fi(x) along the detector array and fi(y) along the flight direction
			pa->y = (dataStorage.looky0+dataStorage.looky1*k+dataStorage.looky2*pow(float(k),2)+dataStorage.looky3*pow(float(k),3))/1000.0; //originally in milli-radian

			fprintf(out1,"%6d   %10.6lf    %10.6lf\n",pa->id,pa->x,pa->y);

			oldDirection.z  = 1;
			oldDirection.x  =  tan(pa->y);
			oldDirection.y  = -tan(pa->x);

			(directionVector[i])[seg]=oldDirection;

			/*
			// transformation from LOS to PIP frame
			rotBias.R_times_x((directionVector[i])[k], oldDirection);

			//// we have to normalize the resulting vector
			norm = sqrt(pow((directionVector[i])[k].x,2)+pow((directionVector[i])[k].y,2)+pow((directionVector[i])[k].z,2));
			(directionVector[i])[k].x /=norm;
			(directionVector[i])[k].y /=norm;//(directionVector[i])[k].y /=-norm;//mirror y axis //revised by liusj 20100423
			(directionVector[i])[k].z /=norm;//*/

			//fprintf(out1,"%6d  %6d %10.6lf   %10.6lf    %10.6lf  %10.6lf   %10.6lf   %10.6lf   %10.6lf\n",i, (k+1), (directionVector[i])[k].x,(directionVector[i])[k].y,(directionVector[i])[k].z, norm, oldDirection.x , oldDirection.y, oldDirection.z );
		}
	}

	fclose(out1);


	// ####################################################################
	// get start values for the adjustment

	// the start values for the rotation matrix
	C3DPoint X(0,-1,0), Y(1,0,0), Z((directionVector[0])[nObs/2]); //differ from spot X(0,1,0)
	Z = Z / Z.getNorm();
	X = Y.crossProduct(Z);
	X = X / X.getNorm();
	Y = Z.crossProduct(X);

	CRotationMatrix initMat(X,Y,Z);
	CRollPitchYawRotation rotRPY(initMat);
	rotRPY[0] = 0; //rotRPY[0] = M_PI; //revised 20100423

	rotRPY.computeRotMatFromParameter();

	Matrix A_Start(4,3);
	Matrix dl_Start(4,1);
	A_Start=0;
	dl_Start =0;

	//// the first CCD element( corrected with rot mat)
	C3DPoint p1;
	rotRPY.RT_times_x(p1,(directionVector[0])[0]);
	p1 /= -p1.z;

	//// the first and last look angle of the first band
	//// p0
	A_Start.element(0,0) = 1.0;
	A_Start.element(0,2) = p1.x;
	A_Start.element(1,1) = 1.0;
	A_Start.element(1,2) = p1.y;
	dl_Start.element(0,0) = 0;

	// the last CCD element
	rotRPY.RT_times_x(p1,(directionVector[0])[nObs-1]);
	p1 /= -p1.z;

	A_Start.element(2,0) = 1.0;
	A_Start.element(2,2) = p1.x;
	A_Start.element(3,1) = 1.0;
	A_Start.element(3,2) = p1.y;
	dl_Start.element(2,0) = nObs-1;

	Matrix N_Start = A_Start.t() * A_Start;
	Matrix t_Start = A_Start.t() * dl_Start;
	Matrix dx_Start;
	try
	{
		Matrix Qxx = N_Start.i();
		dx_Start = Qxx * t_Start;
	}
	catch (...)
	{
		this->errorMessage = "Processing of Look Angles failed";
		return false;
	}



	// ####################################################################
	// compute mounting and xc,yc, c from look angles
	double redfac = 0.01; double ftmp;

	Matrix dx(6,1); Matrix dx5(5,1); Matrix dx4(4,1);
	Matrix Qxx;
	///5 unknowns,roll=0
	dx.element(0,0) = 0; //ftmp= rotRPY[0]; //roll set to 0 const
	dx.element(1,0) = dx5.element(0,0)= ftmp= rotRPY[1];
	dx.element(2,0) = dx5.element(1,0)= ftmp= rotRPY[2]; 

	dx.element(3,0) = dx5.element(2,0)= ftmp= dx_Start.element(0,0) * redfac;
	dx.element(4,0) = dx5.element(3,0)= ftmp= dx_Start.element(1,0) * redfac;
	dx.element(5,0) = dx5.element(4,0)= ftmp= dx_Start.element(2,0) * redfac;//*/

	double vTvOld = 0.0;
	double vTv = 100.0;

	C2DPoint *p;
	CAdjustMatrix dFdAngles(3,3);
	C3DPoint F;

	double tmp;
	double fac1;
	double fac2;
	double fac3;

	double count=0.0;

	CAdjustMatrix A(2,5);//A(2,4);//A(2,6);//
	CAdjustMatrix dl(2,1,0.0);

	double listenerStartValue =0.0;
	if (this->listener)
		listenerStartValue = this->listener->getProgress();

	CRotationBase * rotBase = NULL;
	// iteration loop	
	for (int j=0; (j < 20) && (fabs((vTv - vTvOld) / (vTv + vTvOld)) > 0.0001); j++)	
	{
		vTvOld = vTv;
		vTv = 0.0;
		Matrix N(5,5);//N(4,4);//N(6,6);//
		N=0;
		Matrix t(5,1);//t(4,1);//t(6,1);//
		t=0;

		C3DPoint rpy(dx.element(0,0),dx.element(1,0),dx.element(2,0));		
		rotBase = CRotationBaseFactory::initRotation(&rpy,RPY);
		CAdjustMatrix dRdAngles(9,3);

		rotBase->computeDerivation(dRdAngles);


		for (int band=0; band < nBands; band++)
		{
			int nLookAngles = lookAngles.GetSize();
			count++;

			// fill N Matrix
			for (int i=0; i< nLookAngles; i++)
			{
				// compute derivations dF/dAngles
				//revised,(directionVector[band])[i].z should not be ignored in case it's not 1,below as well
				dFdAngles(0,0) = (directionVector[band])[i].x * dRdAngles(0,0) + (directionVector[band])[i].y * dRdAngles(3,0) + (directionVector[band])[i].z * dRdAngles(6,0);
				dFdAngles(0,1) = (directionVector[band])[i].x * dRdAngles(0,1) + (directionVector[band])[i].y * dRdAngles(3,1) + (directionVector[band])[i].z * dRdAngles(6,1);
				dFdAngles(0,2) = (directionVector[band])[i].x * dRdAngles(0,2) + (directionVector[band])[i].y * dRdAngles(3,2) + (directionVector[band])[i].z * dRdAngles(6,2);

				dFdAngles(1,0) = (directionVector[band])[i].x * dRdAngles(1,0) + (directionVector[band])[i].y * dRdAngles(4,0) + (directionVector[band])[i].z * dRdAngles(7,0);
				dFdAngles(1,1) = (directionVector[band])[i].x * dRdAngles(1,1) + (directionVector[band])[i].y * dRdAngles(4,1) + (directionVector[band])[i].z * dRdAngles(7,1);
				dFdAngles(1,2) = (directionVector[band])[i].x * dRdAngles(1,2) + (directionVector[band])[i].y * dRdAngles(4,2) + (directionVector[band])[i].z * dRdAngles(7,2);

				dFdAngles(2,0) = (directionVector[band])[i].x * dRdAngles(2,0) + (directionVector[band])[i].y * dRdAngles(5,0) + (directionVector[band])[i].z * dRdAngles(8,0);
				dFdAngles(2,1) = (directionVector[band])[i].x * dRdAngles(2,1) + (directionVector[band])[i].y * dRdAngles(5,1) + (directionVector[band])[i].z * dRdAngles(8,1);
				dFdAngles(2,2) = (directionVector[band])[i].x * dRdAngles(2,2) + (directionVector[band])[i].y * dRdAngles(5,2) + (directionVector[band])[i].z * dRdAngles(8,2);


				rotBase->RT_times_x(F,(directionVector[band])[i]);

				tmp = 1.0/F.z;
				fac1 =  -dx.element(5,0)*tmp;
				fac2 = -fac1*F.x*tmp;
				fac3 = -fac1*F.y*tmp;

				/*
				A(0,0) = fac1 * dFdAngles(0,0) + fac2 * dFdAngles(2,0);
				A(0,1) = fac1 * dFdAngles(0,1) + fac2 * dFdAngles(2,1);
				A(0,2) = fac1 * dFdAngles(0,2) + fac2 * dFdAngles(2,2);

				A(1,0) = fac1 * dFdAngles(1,0) + fac3 * dFdAngles(2,0);
				A(1,1) = fac1 * dFdAngles(1,1) + fac3 * dFdAngles(2,1);
				A(1,2) = fac1 * dFdAngles(1,2) + fac3 * dFdAngles(2,2);

				A(0,3) = 1.0;
				A(0,4) = 0.0;
				A(0,5) = -F.x*tmp;

				A(1,3) = 0.0;
				A(1,4) = 1.0;				
				A(1,5) = -F.y*tmp;//*/

				///5 unknowns
				A(0,0) = (fac1 * dFdAngles(0,1) + fac2 * dFdAngles(2,1));
				A(0,1) = (fac1 * dFdAngles(0,2) + fac2 * dFdAngles(2,2));
				A(0,2) = 1.0;
				A(0,3) = 0.0;
				A(0,4) = -F.x*tmp;

				A(1,0) = fac1 * dFdAngles(1,1) + fac3 * dFdAngles(2,1);
				A(1,1) = fac1 * dFdAngles(1,2) + fac3 * dFdAngles(2,2);
				A(1,2) = 0.0;
				A(1,3) = 1.0;				
				A(1,4) = -F.y*tmp;//*/

				CAdjustMatrix AT = A;
				AT.transpose();

				CAdjustMatrix ATPA = AT * A;
				ATPA.addToMatrix(0,0,N);


				dl(0,0) = (lookAngles.GetAt(i)->id) *redfac - dx.element(3,0) + dx.element(5,0) * F.x/F.z;
				dl(1,0) = -dx.element(4,0) + dx.element(5,0) * F.y/F.z;
				vTv += dl(0,0) * dl(0,0) + dl(1,0) * dl(1,0);				

				CAdjustMatrix ATdl = AT * dl;
				ATdl.addToMatrix(0,0,t);
			}
			///for 5 unknowns
			for (int i=0;i<5;i++) 		
				N.element(i,0) += this->dataStorage.ncols; //so that dpitch would be close to 0,i.e. pitch close to 0;				
			//*/
		}
		Qxx = N.i();
		//dx4 += Qxx * t; for(int i=0;i<4;i++)dx.element(i+2,0)=dx4.element(i,0);//4 unknowns
		dx5 += Qxx * t; for(int i=0;i< 5;i++)dx.element(i+1,0)=dx5.element(i,0);//5 unknowns

		delete rotBase;
		rotBase = NULL;

		if (this->listener)
		{
			double per = listenerStartValue + ((double) (j+1)/8.0 ) *(this->listenerMaxValue - listenerStartValue );
			this->listener->setProgressValue(per);
		}
	}

	CXYZPoint irp(dx.element(3,0)/redfac,dx.element(4,0)/redfac, dx.element(5,0)/redfac);

	this->camera->setIRP(irp);
	this->camera->setScale(0.0065);//pixel size

	C3DPoint angles(dx.element(0,0),dx.element(1,0),dx.element(2,0));

	CRollPitchYawRotation rotMat(&angles);//Rc
	CRotationMatrix rotMounting;
	rotBias.R_times_R(rotMounting,rotMat.getRotMat());//Rm=Rbias*Rc
	this->cameraMounting->setRotation(rotMounting);

	this->cameraMounting->setShift(CXYZPoint(0.0,0.0,0.0));// no shift

	double viewAngle = this->cameraMounting->getRotation()[0];

	if (viewAngle < 0) viewAngle +=2*M_PI;


	this->satInfo.viewDirection = "Formosat2-" + this->dataStorage.dataStripID;
	/*
	double s0 = sqrt(vTv / (2*nObs - 5));
	C3DPoint sig(s0 * sqrt(Qxx.element(3,3)) / redfac, s0 * sqrt(Qxx.element(4,4)) / redfac, s0 * sqrt(Qxx.element(5,5)) / redfac);
	C3DPoint sigr(s0 * sqrt(Qxx.element(0,0)), s0 * sqrt(Qxx.element(1,1)),s0 * sqrt(Qxx.element(2,2)));//*/
	///for 5 unknowns
	double s0 = sqrt(vTv / (2*nObs - 5));
	C3DPoint sig(s0 * sqrt(Qxx.element(2,2)) / redfac, s0 * sqrt(Qxx.element(3,3)) / redfac, s0 * sqrt(Qxx.element(4,4)) / redfac);
	C3DPoint sigr(0, s0 * sqrt(Qxx.element(0,0)),s0 * sqrt(Qxx.element(1,1)));//*/

	FILE* outQxx;
	outQxx = fopen("Qxx.txt","w");

	fprintf(outQxx,"s0 : %20.3lf\n",s0);
	fprintf(outQxx,"x: %20.3lf   sx : %20.3lf\n",irp.x,sig.x);
	fprintf(outQxx,"y: %20.3lf   sy : %20.3lf\n",irp.y,sig.y);
	fprintf(outQxx,"c: %20.3lf   sz : %20.3lf\n",irp.z,sig.z);
	fprintf(outQxx,"roll : %20.15lf   sroll  : %20.15lf\n",angles.x,sigr.x);
	fprintf(outQxx,"pitch: %20.15lf   spitch : %20.15lf\n",angles.y,sigr.y);
	fprintf(outQxx,"yaw  : %20.15lf   syaw   : %20.15lf\n",angles.z,sigr.z);

	for (int i = 0; i < Qxx.Nrows(); i++)
	{
		for (int k=0; k < Qxx.Ncols(); k++)
		{
			fprintf(outQxx,"%20.3lf",Qxx(i+1,k+1));
		}
		fprintf(outQxx,"\n");
	}

	fprintf(outQxx,"\ncorrelations\n\n");

	for (int i = 0; i < Qxx.Nrows(); i++)
	{
		for (int k=0; k < Qxx.Ncols(); k++)
		{
			fprintf(outQxx,"%20.3lf",Qxx(i+1,k+1)/(sqrt(Qxx(i+1,i+1))*sqrt(Qxx(k+1,k+1))));
		}
		fprintf(outQxx,"\n");
	}
	fclose(outQxx);
	//////////////////////////////////////////////////////////////////////////
	//IO residuals compensation
	// x_bias_pl = a0 + a1*xf + a2*xf^2 + a3*xf^3  //x_pl is in pixel,and xf is in pixel*scale
	// y_bias_pl = b0 + b1*xf + b2*xf^2 + b3*xf^3
	CAdjustMatrix Ax(1,4),Ay(1,4);	
	CAdjustMatrix dlx(1,1),dly(1,1);
	Matrix Nx(4,4),Ny(4,4);Nx=0;Ny=0;
	Matrix tx(4,1),ty(4,1);tx=0;ty=0;
	C3DPoint rpy(dx.element(0,0),dx.element(1,0),dx.element(2,0));
	rotBase = CRotationBaseFactory::initRotation(&rpy,RPY);
	vTv =0.0;
	FILE* outv = fopen("v.txt","w");
	for (int band=0; band < nBands; band++)
	{
		int nLookAngles = nObs;

		for (int i=0; i< nLookAngles; i++)
		{
			C3DPoint F;
			rotBase->RT_times_x(F,(directionVector[band])[i]);

			double tmp = 1.0/F.z;
			double fac1 =  -dx.element(5,0)*tmp;
			double fac2 = -fac1*F.x*tmp;
			double fac3 = -fac1*F.y*tmp;

			dlx(0,0) = -lookAngles.GetAt(i)->id *redfac + (dx.element(3,0) - dx.element(5,0) *  F.x*tmp);
			dly(0,0) = -0 + (dx.element(4,0) - dx.element(5,0) * F.y*tmp);
			fprintf(outv,"%30.20lf  %30.20lf\n",dlx(0,0),dly(0,0));
			vTv += dlx(0,0) * dlx(0,0) + dly(0,0) * dly(0,0);


			Ax(0,0)=Ay(0,0)=1;
			Ax(0,1)=Ay(0,1)=lookAngles.GetAt(i)->id*this->camera->getScale();
			Ax(0,2)=Ay(0,2)=pow(Ax(0,1),2);
			Ax(0,3)=Ay(0,3)=pow(Ax(0,1),3);

			CAdjustMatrix AxT = Ax;	AxT.transpose();
			CAdjustMatrix ATPAx = AxT * Ax;
			ATPAx.addToMatrix(0,0,Nx);

			CAdjustMatrix AyT = Ay;	AyT.transpose();
			CAdjustMatrix ATPAy = AyT * Ay;
			ATPAy.addToMatrix(0,0,Ny);

			dlx(0,0)=dlx(0,0)/redfac; //in pixel
			CAdjustMatrix ATdx = AxT * dlx;
			ATdx.addToMatrix(0,0,tx);

			dly(0,0)=dly(0,0)/redfac;
			CAdjustMatrix ATdy = AyT * dly;
			ATdy.addToMatrix(0,0,ty);//*/			
		}
	}
	fprintf(outv,"vTPv %30.20lf",vTv);
	fclose(outv);
	delete rotBase;
	rotBase = NULL;

	Try{
		Matrix px = Nx.i() * tx;
		Matrix py = Ny.i() * ty;

		doubles parameter;
		parameter.push_back(px.element(0,0));
		parameter.push_back(px.element(1,0));
		parameter.push_back(px.element(2,0));
		parameter.push_back(px.element(3,0));
		parameter.push_back(py.element(0,0));
		parameter.push_back(py.element(1,0));
		parameter.push_back(py.element(2,0));
		parameter.push_back(py.element(3,0));

		this->camera->setDistortion(eDistortionTHEOS,parameter);

	}CatchAll{
		this->errorMessage = "Failed computing the image distortion";
		return false;
	}//*/	
	//////////////////////////////////////////////////////////////////////////
	return true;
}

void CFormosat2Reader::computeLOS2PIPMatrix(CRotationMatrix& mat)
{
	double biasRoll = this->dataStorage.biases[0];
	double biasPitch = this->dataStorage.biases[1];
	double biasYaw = this->dataStorage.biases[2];

	mat(0,0) = cos(biasPitch)*cos(biasYaw);
	mat(0,1) = cos(biasYaw)*sin(biasPitch)*sin(biasRoll)+sin(biasYaw)*cos(biasRoll);
	mat(0,2) = -sin(biasPitch)*cos(biasRoll)*cos(biasYaw)+sin(biasRoll)*sin(biasYaw);
	mat(1,0) = -sin(biasYaw)*cos(biasPitch);
	mat(1,1) = -sin(biasYaw)*sin(biasPitch)*sin(biasRoll)+cos(biasYaw)*cos(biasRoll);
	mat(1,2) = sin(biasRoll)*sin(biasPitch)*cos(biasRoll)+cos(biasYaw)*sin(biasRoll);
	mat(2,0) = sin(biasPitch);
	mat(2,1) = -cos(biasPitch)*sin(biasRoll);
	mat(2,2) = cos(biasPitch)*cos(biasRoll);
}


