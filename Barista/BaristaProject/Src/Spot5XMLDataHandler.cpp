#include "Spot5XMLDataHandler.h"

#include <xercesc/sax/AttributeList.hpp>

#include "SpotData.h"
#include "XYZPoint.h"

CSpot5XMLDataHandler::CSpot5XMLDataHandler(CSpotData& dataStorage) : dataStorage(dataStorage)
{
	this->readMetadata_Id = false;
	this->readDataset_Frame = false;
		this->readVertex = false;
		this->readScene_Center = false;
	this->readCoordinate_Reference_System = false;
		this->readHorizontal_CS = false;
	this->readRaster_CS = false;
	this->readGeoposition= false;
		this->readGeoposition_Points= false;
			this->readTie_Point= false;
	this->readRaster_Dimensions = false;
	this->readRaster_Encoding = false;
	this->readData_Strip = false;
		this->readData_Strip_Identification = false;
		this->readSatellite_Time = false;
		this->readEphemeris = false;
			this->readPoints = false;
				this->readPoint = false;
					this->readLocation = false;
					this->readVelocity = false;
			this->readDoris_Points = false;
				this->readDoris_Point = false;
					this->readDorisLocation = false;
					this->readDorisVelocity = false;
		this->readSensor_Configuration = false;
			this->readTime_Stamp = false;
			this->readInstrument_Look_Angles_List = false;
				this->readInstrument_Look_Angles = false;
					this->readLook_Angles_List = false;
						this->readLook_Angles = false;
		this->readSatellite_Attitudes = false;
			this->readRaw_Attitudes = false;
				this->readAocs_Attitudes = false;
					this->readAngle_List = false;
						this->readAocsAngles = false;
					this->readAngular_Speed_List = false;
						this->readAngular_Speeds = false;
				this->readStar_Tracker_Attitudes = false;
					this->readQuaternion_List = false;
						this->readQuaternion = false;
			this->readCorrected_Attitudes = false;
				this->readCorrected_Attitude = false;
					this->readCorrectedAngles = false;

	this->hasVelocity = false;
}

CSpot5XMLDataHandler::~CSpot5XMLDataHandler(void)
{
}


void CSpot5XMLDataHandler::endElement(const XMLCh* const name )
{
	this->tagDepth--;

	switch (this->tagDepth)
	{
	case 1:	{
				this->readMetadata_Id = false;
				this->readDataset_Frame = false;
				if (this->readRaster_Dimensions)
				{
					this->readRaster_Dimensions = false;
					this->dataStorage.lookAngles.resize(this->dataStorage.nbands);	
				}
				this->readCoordinate_Reference_System = false;
				this->readRaster_Encoding = false;
				this->readData_Strip = false;
				this->readRaster_CS = false;
				this->readGeoposition= false;
				break;
			}
	case 2: {
				this->readEphemeris = false;
				this->readSatellite_Time = false;
				this->readHorizontal_CS = false;
				this->readGeoposition_Points= false;
				this->readData_Strip_Identification = false;

				if (this->readVertex)
				{
					this->readVertex = false;
					CObsPoint* p= this->dataStorage.datasetFramePoints.Add();
					(*p)[0] = this->x;
					(*p)[1] = this->y;
					(*p)[2] = this->z;
					(*p)[3] = this->vx;
				}
				if (this->readScene_Center)
				{
					this->readScene_Center = false;
					this->dataStorage.datasetSceneCenter[0] = this->x;
					this->dataStorage.datasetSceneCenter[1] = this->y;
					this->dataStorage.datasetSceneCenter[2] = this->z;
					this->dataStorage.datasetSceneCenter[3] = this->vx;
				}
				if (this->readSensor_Configuration)
				{
					this->readSensor_Configuration = false;
					int day, month, year,hour, min;
					double sec;				
					sscanf(this->time.GetChar(),"%4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					this->dataStorage.sceneCenterTime = hour*3600 + min*60 + sec;
				}
				this->readSatellite_Attitudes = false;
				break;
			}
	case 3: {
				this->readPoints = false;
				this->readDoris_Points = false;
				this->readRaw_Attitudes = false;
				this->readInstrument_Look_Angles_List = false;
				this->readTime_Stamp = false;
				if (this->readCorrected_Attitudes)
				{
					this->readCorrected_Attitudes = false;
					if (this->flag2.CompareNoCase("Y"))
						this->dataStorage.starTrackerUsed = true;
					else
						this->dataStorage.starTrackerUsed = false;

				}

				break;
			}
	case 4: {
				if (this->readPoint)
				{
					this->readPoint = false;
					CXYZOrbitPoint *point = this->dataStorage.triodeOrbitPoints.add(); 
					int day, month, year,hour, min;
					double sec;	
					CXYZPoint pos(this->x,this->y,this->z);
					CXYZPoint vel(this->vx,this->vy,this->vz);
					sscanf(this->time.GetChar(),"%4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					point->setOrbitPoint(pos,vel,day,month,year,hour,min,sec);
					point->setHasVelocity(this->hasVelocity);
					this->hasVelocity = false;
					
				}
				else if (this->readDoris_Point)
				{
					this->readDoris_Point = false;
					CXYZOrbitPoint* point = this->dataStorage.dorisOrbitpoints.add(); 
					int day, month, year,hour, min;
					double sec;					
					sscanf(this->time.GetChar(),"%4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					point->setOrbitPoint(CXYZPoint(this->x,this->y,this->z),CXYZPoint(this->vx,this->vy,this->vz),day,month,year,hour,min,sec);
					point->setHasVelocity(this->hasVelocity);
					this->hasVelocity = false;
					
				}
				else if (this->readTie_Point)
				{
					this->readTie_Point = false;
					CObsPoint * p = this->dataStorage.tiePoints.Add();
					(*p)[0]= this->x;
					(*p)[1]= this->y;
					(*p)[2]= this->z;
					(*p)[3]= this->vx;
					(*p)[4]= this->vy;
				}
				
				
				this->readAocs_Attitudes = false;
				this->readStar_Tracker_Attitudes = false;
				this->readCorrected_Attitude = false;
				this->readInstrument_Look_Angles = false;

				break;
			}
	case 5: {
				this->readLocation = false;
				this->readVelocity = false;
				this->readDorisLocation = false;
				this->readDorisVelocity = false;
				this->readAngle_List = false;
				this->readAngular_Speed_List = false;
				this->readLook_Angles_List = false;
				if (this->readCorrectedAngles && this->flag.CompareNoCase("N"))
				{
					this->readCorrectedAngles = false;
					CObsPoint* p  = this->dataStorage.correctedAngles.Add();
					int day, month, year,hour, min;
					double sec;	
					sscanf(this->time.GetChar(),"%4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					(*p)[0] = this->x;
					(*p)[1] = this->y;
					(*p)[2] = this->z;
					p->dot = hour*3600 + min*60 + sec;

				}
				break;
			}
	case 6: {
				if (this->readAocsAngles && this->flag.CompareNoCase("N"))
				{
					
					this->readAocsAngles = false;
					CObsPoint* p  = this->dataStorage.aocsAngles.Add();
					int day, month, year,hour, min;
					double sec;	
					sscanf(this->time.GetChar(),"%4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					(*p)[0] = this->x;	
					(*p)[1] = this->y;
					(*p)[2] = this->z; 
					p->dot = hour*3600 + min*60 + sec;
					
				}
				else if (this->readAngular_Speeds && this->flag.CompareNoCase("N"))
				{
					this->readAngular_Speeds =false;
					CObsPoint* p  = this->dataStorage.aocsAnglesSpeed.Add();
					int day, month, year,hour, min;
					double sec;	
					sscanf(this->time.GetChar(),"%4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					(*p)[0] = this->x;
					(*p)[1] = this->y;
					(*p)[2] = this->z;
					p->dot = hour*3600 + min*60 + sec;
				}
				else if (this->readLook_Angles)
				{
					this->readLook_Angles = false;
					C2DPoint *p = this->dataStorage.lookAngles[bandIndex-1].Add();
					p->x = x;
					p->y = y;
					p->id = detectorID;
				}
			}

	}

}

void CSpot5XMLDataHandler::startElement(const XMLCh* const name , AttributeList&  attributes)
{
	this->xmlTag[this->tagDepth] = XMLString::transcode(name);

	switch (this->tagDepth)
	{
	case 1:		{this->read = noRead;
				if (this->xmlTag[1].CompareNoCase("Raster_Dimensions"))
					this->readRaster_Dimensions = true;
				else if (this->xmlTag[1].CompareNoCase("Raster_Encoding"))
					this->readRaster_Encoding = true;
				else if (this->xmlTag[1].CompareNoCase("Data_Strip"))
					this->readData_Strip = true;
				else if (this->xmlTag[1].CompareNoCase("Metadata_Id"))
					this->readMetadata_Id = true;
				else if (this->xmlTag[1].CompareNoCase("Dataset_Frame"))
					this->readDataset_Frame = true;
				else if (this->xmlTag[1].CompareNoCase("Coordinate_Reference_System"))
					this->readCoordinate_Reference_System = true;
				else if (this->xmlTag[1].CompareNoCase("Raster_CS"))
					this->readRaster_CS = true;
				else if (this->xmlTag[1].CompareNoCase("Geoposition"))
					this->readGeoposition = true;

				break;
				}
	case 2:		{
					if (this->readRaster_Dimensions)
					{
						this->read = readInt;
						if (this->xmlTag[2].CompareNoCase("NCOLS"))
							this->intReader = &this->dataStorage.ncols;
						else if (this->xmlTag[2].CompareNoCase("NROWS"))
							this->intReader = &this->dataStorage.nrows;
						else if (this->xmlTag[2].CompareNoCase("NBANDS"))
							this->intReader = &this->dataStorage.nbands;

						else
							this->read = noRead;
					}
					else if (this->readRaster_Encoding)
					{
						this->read = readInt;
						if (this->xmlTag[2].CompareNoCase("NBITS"))
							this->intReader = & this->dataStorage.bpp;
						else
							this->read = noRead;
					}
					else if (this->readData_Strip)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Ephemeris"))
							this->readEphemeris = true;
						else if (this->xmlTag[2].CompareNoCase("Satellite_Time"))
							this->readSatellite_Time = true;
						else if (this->xmlTag[2].CompareNoCase("Sensor_Configuration"))
							this->readSensor_Configuration = true;
						else if (this->xmlTag[2].CompareNoCase("Satellite_Attitudes"))
							this->readSatellite_Attitudes = true;
						else if (this->xmlTag[2].CompareNoCase("Data_Strip_Identification"))
							this->readData_Strip_Identification = true;

					}
					else if (this->readMetadata_Id)
					{
						this->read = readString;
						if (this->xmlTag[2].CompareNoCase("METADATA_FORMAT"))
						{
							this->dataStorage.metadataVersion = XMLString::transcode(attributes.getValue("version"));
							this->read = noRead;
						}
						else if (this->xmlTag[2].CompareNoCase("METADATA_PROFILE"))
							this->stringReader = &this->dataStorage.metatdatProfile;
						else
							this->read = noRead;
					}
					else if (this->readDataset_Frame)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Vertex"))
							this->readVertex = true;
						else if (this->xmlTag[2].CompareNoCase("Scene_Center"))
							this->readScene_Center = true;
						else if (this->xmlTag[2].CompareNoCase("SCENE_ORIENTATION"))
						{
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.datasetFrameOrientation;
						}
					}
					else if (this->readCoordinate_Reference_System)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Horizontal_CS"))
							this->readHorizontal_CS = true;
					}
					else if (this->readRaster_CS)
					{
						this->read = readInt;
						if (this->xmlTag[2].CompareNoCase("PIXEL_ORIGIN"))
							this->intReader = &this->dataStorage.pixelOrgin;
						else
							this->read = noRead;
					}
					else if (this->readGeoposition)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Geoposition_Points"))
							this->readGeoposition_Points = true;

					}
	
					break;
				}
	case 3:		{
					if (this->readEphemeris)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Points"))
							this->readPoints = true;
						if (this->xmlTag[3].CompareNoCase("Doris_Points"))
							this->readDoris_Points = true;
					}
					else if (this->readSatellite_Time)
					{
						this->read = readDouble;
						if (this->xmlTag[3].CompareNoCase("TAI_TUC"))
							this->doubleReader = &this->dataStorage.timeOffsetDorisTriode;
						else
							this->read = noRead;
						
					}
					else if (this->readSensor_Configuration)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Time_Stamp"))
							this->readTime_Stamp = true;
						else if (this->xmlTag[3].CompareNoCase("Instrument_Look_Angles_List"))
							this->readInstrument_Look_Angles_List = true;

					}
					else if (this->readSatellite_Attitudes)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Raw_Attitudes"))
							this->readRaw_Attitudes = true;
						else if (this->xmlTag[3].CompareNoCase("Corrected_Attitudes"))
							this->readCorrected_Attitudes = true;
					}
					else if (this->readVertex)
					{
						this->read = readDouble;
						if (this->xmlTag[3].CompareNoCase("FRAME_LON"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[3].CompareNoCase("FRAME_LAT"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[3].CompareNoCase("FRAME_ROW"))
							this->doubleReader = &this->z;
						else if (this->xmlTag[3].CompareNoCase("FRAME_COL"))
							this->doubleReader = &this->vx;
					}
					else if (this->readScene_Center)
					{
						this->read = readDouble;
						if (this->xmlTag[3].CompareNoCase("FRAME_LON"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[3].CompareNoCase("FRAME_LAT"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[3].CompareNoCase("FRAME_ROW"))
							this->doubleReader = &this->z;
						else if (this->xmlTag[3].CompareNoCase("FRAME_COL"))
							this->doubleReader = &this->vx;
					}
					else if (this->readHorizontal_CS)
					{
						this->read = readString;
						if (this->xmlTag[3].CompareNoCase("HORIZONTAL_CS_CODE"))
							this->stringReader = &this->dataStorage.horizontalCsCode;
						else if (this->xmlTag[3].CompareNoCase("HORIZONTAL_CS_TYPE"))
							this->stringReader = &this->dataStorage.horizontalCsType;
						else if (this->xmlTag[3].CompareNoCase("HORIZONTAL_CS_NAME"))
							this->stringReader = &this->dataStorage.horizontalCsName;
					}
					else if (this->readGeoposition_Points)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Tie_Point"))
							this->readTie_Point = true;

					}
					else if (this->readData_Strip_Identification)
					{
						this->read = readString;
						if (this->xmlTag[3].CompareNoCase("DATA_STRIP_ID"))
							this->stringReader = &this->dataStorage.dataStripID;
					}
					
					break;
				}
	case 4:		{
					if (this->readPoints)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Point"))
							this->readPoint = true;
					}
					else if(this->readDoris_Points)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Doris_Point"))
							this->readDoris_Point = true;
					}
					else if(this->readTime_Stamp)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("LINE_PERIOD"))
						{
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.linePeriode;
						}
						else if (this->xmlTag[4].CompareNoCase("SCENE_CENTER_TIME"))
						{
							this->read = readString;
							this->stringReader = &this->time;
						}
						else if (this->xmlTag[4].CompareNoCase("SCENE_CENTER_LINE"))
						{
							this->read = readInt;
							this->intReader = &this->dataStorage.sceneCenterLine;
						}
						else if (this->xmlTag[4].CompareNoCase("SCENE_CENTER_COL"))
						{
							this->read = readInt;
							this->intReader = &this->dataStorage.sceneCenterCol;
						}
						else
							this->read = noRead;
					}
					else if(this->readRaw_Attitudes)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Aocs_Attitude"))
							this->readAocs_Attitudes = true;
						else if (this->xmlTag[4].CompareNoCase("Star_Tracker_Attitude"))
							this->readStar_Tracker_Attitudes = true;
					}
					else if(this->readCorrected_Attitudes)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Corrected_Attitude"))
							this->readCorrected_Attitude = true;
						else if (this->xmlTag[4].CompareNoCase("STAR_TRACKER_USED"))
						{
							this->read = readString;
							this->stringReader = &this->flag2;
						}
					}
					else if (this->readTie_Point)
					{
						this->read = readDouble;
						if (this->xmlTag[4].CompareNoCase("TIE_POINT_CRS_X"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[4].CompareNoCase("TIE_POINT_CRS_Y"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[4].CompareNoCase("TIE_POINT_CRS_Z"))
							this->doubleReader = &this->z;
						else if (this->xmlTag[4].CompareNoCase("TIE_POINT_DATA_X"))
							this->doubleReader = &this->vx;
						else if (this->xmlTag[4].CompareNoCase("TIE_POINT_DATA_Y"))
							this->doubleReader = &this->vy;
						else 
							this->read = noRead;
					}
					else if (this->readInstrument_Look_Angles_List)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Instrument_Look_Angles"))
							this->readInstrument_Look_Angles= true;

					}
						
					break;
				}
	case 5:		{
					if (this->readPoint)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Location"))
							this->readLocation = true;
						else if (this->xmlTag[5].CompareNoCase("Velocity"))
							this->readVelocity = true;
						else if (this->xmlTag[5].CompareNoCase("TIME"))
						{	
							this->read = readString;
							this->stringReader = &this->time;
						}
					}
					else if (this->readDoris_Point)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Location"))
							this->readDorisLocation = true;
						else if (this->xmlTag[5].CompareNoCase("Velocity"))
							this->readDorisVelocity = true;
						else if (this->xmlTag[5].CompareNoCase("TIME"))
						{	
							this->read = readString;
							this->stringReader = &this->time;
						}
					}
					else if (this->readAocs_Attitudes)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Angles_List"))
							this->readAngle_List = true;
						else if (this->xmlTag[5].CompareNoCase("Angular_Speeds_List"))
							this->readAngular_Speed_List = true;
					}
					else if (this->readCorrected_Attitude)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Angles"))
							this->readCorrectedAngles = true;
					}
					else if (this->readInstrument_Look_Angles)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Look_Angles_List"))
							this->readLook_Angles_List = true;
						else if (this->xmlTag[5].CompareNoCase("BAND_INDEX"))
						{
							this->read = readInt;						
							this->intReader = &this->bandIndex;
						}
					}
					break;
				}
	case 6:		{
					if (this->readLocation)
					{
						this->read = readDouble;
						if (this->xmlTag[6].CompareNoCase("X"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[6].CompareNoCase("Y"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[6].CompareNoCase("Z"))
							this->doubleReader = &this->z;
					}
					else if (this->readVelocity)
					{
						this->read = readDouble;
						this->hasVelocity = true;
						if (this->xmlTag[6].CompareNoCase("X"))
							this->doubleReader = &this->vx;
						else if (this->xmlTag[6].CompareNoCase("Y"))
							this->doubleReader = &this->vy;
						else if (this->xmlTag[6].CompareNoCase("Z"))
							this->doubleReader = &this->vz;
					}
					else if (this->readDorisLocation)
					{
						this->read = readDouble;
						if (this->xmlTag[6].CompareNoCase("X"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[6].CompareNoCase("Y"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[6].CompareNoCase("Z"))
							this->doubleReader = &this->z;
					}
					else if (this->readDorisVelocity)
					{
						this->read = readDouble;
						this->hasVelocity = true;
						if (this->xmlTag[6].CompareNoCase("X"))
							this->doubleReader = &this->vx;
						else if (this->xmlTag[6].CompareNoCase("Y"))
							this->doubleReader = &this->vy;
						else if (this->xmlTag[6].CompareNoCase("Z"))
							this->doubleReader = &this->vz;
					}
					else if (this->readAngle_List)
					{
						this->read = noRead;
						if (this->xmlTag[6].CompareNoCase("Angles"))
							this->readAocsAngles = true;
					}
					else if (this->readAngular_Speed_List)
					{
						this->read = noRead;
						if (this->xmlTag[6].CompareNoCase("Angular_Speeds"))
							this->readAngular_Speeds = true;
					}
					else if (this->readCorrectedAngles)
					{
						this->read = readDouble;
						if (this->xmlTag[6].CompareNoCase("TIME"))
						{	
							this->read = readString;
							this->stringReader = &this->time;
						}
						else if (this->xmlTag[6].CompareNoCase("OUT_OF_RANGE"))
						{	
							this->read = readString;
							this->stringReader = &this->flag;
						}
						else if (this->xmlTag[6].CompareNoCase("YAW"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[6].CompareNoCase("PITCH"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[6].CompareNoCase("ROLL"))
							this->doubleReader = &this->z;
						else
							this->read = noRead;	
					}
					else if (this->readLook_Angles_List)
					{
						this->read = noRead;
						if (this->xmlTag[6].CompareNoCase("Look_Angles"))
							this->readLook_Angles = true;
					}
					break;
				}
	case 7:		{
					if (this->readAocsAngles)
					{
						this->read = readDouble;
						if (this->xmlTag[7].CompareNoCase("TIME"))
						{	
							this->read = readString;
							this->stringReader = &this->time;
						}
						else if (this->xmlTag[7].CompareNoCase("OUT_OF_RANGE"))
						{	
							this->read = readString;
							this->stringReader = &this->flag;
						}
						else if (this->xmlTag[7].CompareNoCase("YAW"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[7].CompareNoCase("PITCH"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[7].CompareNoCase("ROLL"))
							this->doubleReader = &this->z;
						else
							this->read = noRead;

					}
					else if (this->readAngular_Speeds)
					{
						this->read = readDouble;
						if (this->xmlTag[7].CompareNoCase("TIME"))
						{	
							this->read = readString;
							this->stringReader = &this->time;
						}
						else if (this->xmlTag[7].CompareNoCase("OUT_OF_RANGE"))
						{	
							this->read = readString;
							this->stringReader = &this->flag;
						}
						else if (this->xmlTag[7].CompareNoCase("YAW"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[7].CompareNoCase("PITCH"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[7].CompareNoCase("ROLL"))
							this->doubleReader = &this->z;
						else
							this->read = noRead;
					}
					else if (this->readLook_Angles)
					{
						if (this->xmlTag[7].CompareNoCase("DETECTOR_ID"))
						{	
							this->read = readInt;
							this->intReader = &this->detectorID;
						}
						else if (this->xmlTag[7].CompareNoCase("PSI_X"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->x;
						}
						else if (this->xmlTag[7].CompareNoCase("PSI_Y"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->y;
						}
					}
				}
	}

	this->tagDepth++;

}


