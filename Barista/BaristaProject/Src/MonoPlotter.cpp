/**
 * @class CMonoPlotter
 * abstract for monoplotting with different sensor models
 */
#include <float.h>
#include "MonoPlotter.h"
#include "VDEM.h"
#include "SensorModel.h"
#include "BaristaProject.h"
#include "Trans3DFactory.h"

CMonoPlotter::CMonoPlotter() : sensorModel(0), vdem (0), sensorToDEM(0), DEMToSensor(0), lastZ(-9999.0)
{
}

CMonoPlotter::~CMonoPlotter()
{
	this->init();
}

double CMonoPlotter::getMinZ() const
{
	if (this->vdem && project.getHeightDEMMonoplotting()) return this->vdem->getDEM()->getminZ();
	
	return project.getMeanHeight();
};
	  
double CMonoPlotter::getMaxZ() const
{
	if (this->vdem && project.getHeightDEMMonoplotting()) return this->vdem->getDEM()->getmaxZ();

	return project.getMeanHeight();
};

CReferenceSystem CMonoPlotter::getReferenceSystem() const
{
	CReferenceSystem ref;

	if (this->vdem) ref = this->vdem->getReferenceSystem();
	else if (this->sensorModel) ref = this->sensorModel->getRefSys();
	else
	{
	}

	return ref;
};
CXYZPolyLineArray* CMonoPlotter::getDEMBorderLines()
{
	if (this->vdem) return this->vdem->getDEM()->getBorder();

	return 0;
}

double CMonoPlotter::getInterpolatedHeight (double x, double y, double &sigma)
{
	float SigF = 1.0;
	double Z;
	if (this->vdem) Z = this->vdem->getDEM()->getBLInterpolatedHeight(x, y, SigF);
	else Z = project.getMeanHeight();

	return Z;
};

/**
 * Sets VDEM used for monoplotting
 */
void CMonoPlotter::setVDEM(CVDEM* vdem,bool initDEMBorder)
{
    this->vdem = vdem;

	if (sensorToDEM) delete sensorToDEM;
	sensorToDEM = 0;
	if (DEMToSensor) delete DEMToSensor;
	DEMToSensor = 0;
	if (this->sensorModel)
	{
		sensorToDEM = CTrans3DFactory::initTransformation(this->sensorModel->getRefSys(), this->getReferenceSystem());
		DEMToSensor = CTrans3DFactory::initTransformation(this->getReferenceSystem(), this->sensorModel->getRefSys());
	}
	
	if ( this->vdem) this->vdem->getDEM()->load(initDEMBorder);
}

const CSensorModel *CMonoPlotter::getModel() const
{
	return sensorModel;
};
	  /// sets the sensor model, e.g. rpc or affine

void CMonoPlotter::setModel(const CSensorModel *pSensorModel)
{
	sensorModel = pSensorModel;
	
	if (sensorToDEM) delete sensorToDEM;
	sensorToDEM = 0;
	if (DEMToSensor) delete DEMToSensor;
	DEMToSensor = 0;

	if (this->sensorModel)
	{
		sensorToDEM = CTrans3DFactory::initTransformation(this->sensorModel->getRefSys(), this->getReferenceSystem());
		DEMToSensor = CTrans3DFactory::initTransformation(this->getReferenceSystem(), this->sensorModel->getRefSys());
	}
};
	  
bool CMonoPlotter::getProjectionRay(C3DPoint &p0, C3DPoint &dir, const C2DPoint &obs, double Zlow, double Zhigh) const
{
	bool ret = true;
	if (!this->sensorToDEM || !this->sensorModel) ret = false;
	else
	{
		if ((!this->sensorModel->transformObs2Obj(dir, obs, Zlow)) ||
			(!this->sensorModel->transformObs2Obj(p0, obs, Zhigh))) ret = false;
		else
		{	
			C3DPoint hlp = p0;
			this->sensorToDEM->transform(p0, hlp);
			hlp = dir;
			this->sensorToDEM->transform(dir, hlp);

			dir -= p0;
			double d = dir.getNorm();
			if (fabs(d) < FLT_EPSILON) ret = false;
			else dir /= d;
		}
	}

	return ret;
};

bool CMonoPlotter::getXYfromImgPointHeight(CXYZPoint &point, const CXYPoint &sl, double height)
{
	if (!this->sensorModel) return false;
    if (!this->sensorModel->gethasValues()) return false;
    
    CXYZPoint tmp, tmp1;
	bool ret = this->sensorModel->transformObs2Obj(tmp, sl, height);
	if (ret) this->sensorToDEM->transform(tmp1, tmp);

	point = tmp1;
	return ret;

}

bool CMonoPlotter::getPoint(CXYZPoint &point, const CXYPoint &sl,double limit)
{
    if (this->sensorModel == NULL) return false;
    if (!this->sensorModel->gethasValues()) return false;
	if (!this->sensorToDEM) return false;
     

	double minDEMZ = this->getMinZ();
	double maxDEMZ = this->getMaxZ();
	double meanDEMZ = (minDEMZ + maxDEMZ)/2.0;


    // calculate starting height
	if ( this->lastZ == -9999.0)
		this->lastZ = meanDEMZ;
    
	// Assign height of individual points
     if (project.getHeightPointMonoplotting())  this->lastZ=project.getMeanHeight();

	// use last monoplotted Z as start value
	if (this->iteratePoint(point, sl, this->lastZ,limit))
	{
		this->lastZ = point.z;
		return true;
	}
	// use mean dem height as start value
	else if (this->iteratePoint(point, sl, meanDEMZ,limit))
	{
		this->lastZ = point.z;
		return true;
	}
	// use minimum dem height as start value
	else if (this->iteratePoint(point, sl, minDEMZ,limit))
	{
		this->lastZ = point.z;
		return true;
	}
	// use maximum dem height as start value
	else if (this->iteratePoint(point, sl, maxDEMZ,limit))
	{
		this->lastZ = point.z;
		return true;
	}
	else
	{
		return false;
	}
}


bool CMonoPlotter::getHeightPoint(CXYZPoint &point, CXYPoint &imagepoint, const CXYZPoint &groundpoint)
{
    if (this->sensorModel == NULL) return false;
    if (!this->sensorModel->gethasValues()) return false;

	point.set(groundpoint.getX(), groundpoint.getY(), 0.0);
	this->sensorModel->setHeigthAtXY(point, imagepoint);

    return true;
}

bool CMonoPlotter::getVerticalPole(CXYZPoint      &objGround, CXYZPoint      &objTop, 
						           const CXYPoint &obsGround, const CXYPoint &obsTop)
{
    if (this->sensorModel == NULL) return false;
    if (!this->sensorModel->gethasValues()) return false;

	CXYZPoint tmpTop, tmpGround;
	tmpGround.setLabel(objGround.getLabel());

	if (!this->getPoint(tmpGround, obsGround)) return false;

	this->DEMToSensor->transform(objGround, tmpGround);

    // calculate starting height
    double delta = 9999;
    double limit = 0.0001;
    int iter = 0;

	double sigmaZ = 1.0;
	if (this->vdem) sigmaZ = this->vdem->getDEM()->getSigmaZ();
		
    for (int iter = 0; (iter < 1000) && (delta > limit); iter++)
    {
		
		if (!this->sensorModel->getVerticalPole(objGround, objTop, obsGround, obsTop, tmpGround.z, sigmaZ))
            return false;

 		this->sensorToDEM->transform(tmpGround, objGround);
		this->sensorToDEM->transform(tmpTop, objTop);

		double oldZ = tmpGround.getZ();

        /// interpo latte' new Z
		double newZ = this->getInterpolatedHeight(tmpGround.x, tmpGround.y, sigmaZ);

        // was point off dem?
        if (newZ == -9999) return false;

        tmpGround.z = newZ;

        // checking height difference
        double dz = newZ - oldZ;

        delta = fabs(dz);
    }

	objGround = tmpGround;
	objTop    = tmpTop;
   	
    return true;
}

void CMonoPlotter::init()
{
	sensorModel = 0;
	vdem        = 0;
	if (sensorToDEM) delete sensorToDEM;
	sensorToDEM = 0;
	if (DEMToSensor) delete DEMToSensor;
	DEMToSensor = 0;
}

bool CMonoPlotter::iteratePoint(CXYZPoint &point, const CXYPoint &sl, double Z,double limit)
{
    double delta = 9999;
    int iter = 0;

    CXYZPoint temppoint;

	double sigmaZ = 1.0;
	if (this->vdem) sigmaZ = this->vdem->getDEM()->getSigmaZ();

    for (int iter = 0; (iter < 1000) && (delta > limit); iter++)
    {
        C3DPoint tmp;
		C2DPoint obs(sl.x, sl.y);
		if (!this->sensorModel->transformObs2Obj(tmp, obs, Z))
            return false;

		point = tmp;

        temppoint.setX(point.getX()); 
        temppoint.setY(point.getY()); 

		this->sensorToDEM->transform(temppoint, point);

		double oldZ = temppoint.getZ();

        /// interpo latte' new Z
        double newZ = -9999;

		
		newZ = this->getInterpolatedHeight(temppoint.getX(), temppoint.getY(), sigmaZ);
        
		// check for the case of individual point
		if (project.getHeightPointMonoplotting()) 
		{
			newZ=project.getMeanHeight();
			oldZ=project.getMeanHeight();
		}
 
        // was point off dem?
        if (newZ == -9999) return false;

        temppoint.setZ(newZ);

		// checking height difference
        double dz = newZ - oldZ;

        delta = fabs(dz);

        point.set(temppoint.getX(), temppoint.getY(), temppoint.getZ());
    }

	this->sensorModel->transformObs2Obj(point, sl, point.z, sigmaZ);
	this->sensorToDEM->transform(point, point);

	return true;
};