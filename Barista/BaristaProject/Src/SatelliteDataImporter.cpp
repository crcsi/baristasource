#include "SatelliteDataImporter.h"
#include "IniFile.h"
#include "BaristaProject.h"
#include "SatelliteDataReader.h"

#define SPOT5_HEIGHT 832000
#define QUICKBIRD_HEIGHT 460000
#define ALOS_HEIGHT 700000
#define FORMOSAT2_HEIGHT 891000
#define THEOS_HEIGHT 822000
#define GEOEYE1_HEIGHT 681000
#define MEAN_GROUND_HEIGHT 0.0
#define MEAN_GROUND_TEMPERATURE 15.0

#define RMS_ATTITUDES 0.5 // unit of sigma
#define RMS_PATH 0.5 // unit of sigma


CSatelliteDataImporter::CSatelliteDataImporter(SatelliteType type) : 
	satellite(type),groundTemperature(MEAN_GROUND_TEMPERATURE),
	groundHeight(project.getMeanHeight()),
	satelliteReader(0),filename("")
{
	
	// init with plausible values
	if (inifile.getSatelliteHeight(this->satellite) > 1000.0)
		this->satelliteHeight = inifile.getSatelliteHeight(this->satellite);
	else // init with default values 
	{
		if (this->satellite == Spot5)
			this->satelliteHeight = SPOT5_HEIGHT;
		else if (this->satellite == Quickbird)
			this->satelliteHeight = QUICKBIRD_HEIGHT;
		else if (this->satellite == Alos)
			this->satelliteHeight = ALOS_HEIGHT;
		else if (this->satellite == Formosat2)
			this->satelliteHeight = FORMOSAT2_HEIGHT;
		else if (this->satellite == Theos)
			this->satelliteHeight = THEOS_HEIGHT;
		else if (this->satellite == GeoEye1)
			this->satelliteHeight = GEOEYE1_HEIGHT;
	}


	// create the reader
	this->satelliteReader = CSatelliteDataReaderFactory::initSatelliteReader(this->satellite);

	if (inifile.getSceneTemperature(this->satellite) > -200.0)
		this->groundTemperature = inifile.getSceneTemperature(this->satellite);

	if (inifile.getSceneHeight(this->satellite) > -100.0)
		this->groundHeight = inifile.getSceneHeight(this->satellite);
/*
	if (inifile.getRMSPath(this->satellite) > -1.0)
		this->satelliteReader->setAimedRMSPath(inifile.getRMSPath(this->satellite));
	else
*/

	this->satelliteReader->setAimedRMSPath(RMS_PATH);
/*
	if (inifile.getRMSAttitudes(this->satellite) > -1.0)
		this->satelliteReader->setAimedRMSAttitudes(inifile.getRMSAttitudes(this->satellite));
	else
*/
	this->satelliteReader->setAimedRMSAttitudes(RMS_ATTITUDES);

	// create space in the arrays
	// the first entry holds always the newly read data
	this->knownOrbitPaths.push_back(0);
	this->knownOrbitAttitudes.push_back(0);
	this->knownCameraMountings.push_back(0);
	this->knownCameras.push_back(0);

	this->selectedPath = 0;
	this->selectedAttitudes = 0;
	this->selectedCameraMounting = 0;
	this->selectedCamera = 0;

	// add all known parameters from the project so we can choose later 
	this->collectParameter();
}


CSatelliteDataImporter::~CSatelliteDataImporter(void)
{
	if (this->satelliteReader != NULL)
		delete this->satelliteReader;
	this->satelliteReader = NULL;


	if (this->knownOrbitPaths[0] && this->selectedPath > 0)
		project.deleteOrbitObservation(this->knownOrbitPaths[0]);
	this->knownOrbitPaths[0] = NULL;

	if (this->knownOrbitAttitudes[0] && this->selectedAttitudes > 0)
		project.deleteOrbitObservation(this->knownOrbitAttitudes[0]);
	this->knownOrbitAttitudes[0] = NULL;

	if (this->knownCameraMountings[0] && this->selectedCameraMounting > 0)
		project.deleteCameraMounting(this->knownCameraMountings[0]);
	this->knownCameraMountings[0] = NULL;

	if (this->knownCameras[0] && this->selectedCamera > 0)
		project.deleteCamera(this->knownCameras[0]);
	this->knownCameras[0] = NULL;


}


bool CSatelliteDataImporter::importMetadata(CFileName filename)
{
	// delete first old data if any
	this->cancelImport();

	this->filename = filename;

	// create the ObservationHandler and their SensorModels
	this->knownOrbitPaths[0] = project.addOrbitObservation(Path);
	this->knownOrbitPaths[0]->getCurrentSensorModel()->deactivateParameterGroup(CSensorModel::shiftPar);
	this->knownOrbitPaths[0]->getCurrentSensorModel()->deactivateParameterGroup(CSensorModel::rotPar);

	this->knownOrbitAttitudes[0] = project.addOrbitObservation(Attitude);
	this->knownOrbitAttitudes[0]->getCurrentSensorModel()->deactivateParameterGroup(CSensorModel::rotPar);
	this->knownOrbitAttitudes[0]->getCurrentSensorModel()->deactivateParameterGroup(CSensorModel::scalePar);

	// create the camera
	this->knownCameras[0] = project.addCamera(eCCD);

	// create the cameraMounting
	this->knownCameraMountings[0] = project.addCameraMounting();

	// init the reader
	this->satelliteReader->initReader(this->knownOrbitPaths[0],this->knownOrbitAttitudes[0],this->knownCameraMountings[0],this->knownCameras[0]);

	this->satelliteReader->setProgressListener(this->listener);
	bool success = this->satelliteReader->readData(filename);
	this->satelliteReader->removeProgressListener();

	if (!success || this->knownOrbitPaths[0]->getObsPoints()->GetSize() < 3 ||
		 this->knownOrbitAttitudes[0]->getObsPoints()->GetSize() < 3)
	{
		
		if (success)
		{
			if (this->satelliteReader->satInfo.processingLevel.CompareNoCase("1B2"))
		 		this->errorMessage = "Use the TFW import for ALOS PRISM 1B2 products!";
			else
				this->errorMessage =  "Not enough Orbit observations found!";
		}
		else
			this->errorMessage = this->satelliteReader->getErrorMessage();

		this->cancelImport();
	}
	else
	{
		
		this->satelliteReader->setDirectObservations((COrbitPathModel*)this->knownOrbitPaths[0]->getCurrentSensorModel(),(COrbitAttitudeModel*)this->knownOrbitAttitudes[0]->getCurrentSensorModel());
		
		// get unique names
		CCharString name = this->satelliteReader->satInfo.imageID;
		project.getUniqueOrbitPathName(name);
		this->knownOrbitPaths[0]->setName(name);
		this->knownOrbitPaths[0]->getCurrentSensorModel()->activateParameterGroup(CSensorModel::shiftPar);
		

		name =  this->satelliteReader->satInfo.imageID;
		project.getUniqueOrbitAttitudeName(name);
		this->knownOrbitAttitudes[0]->setName(name);
		this->knownOrbitAttitudes[0]->getCurrentSensorModel()->activateParameterGroup(CSensorModel::rotPar);

		CCharString cameraName(this->satelliteReader->satInfo.cameraName);
		project.getUniqueCameraName(cameraName);
		this->knownCameras[0]->setName(cameraName);

		CCharString cameraMountingName(this->satelliteReader->satInfo.viewDirection);
		project.getUniqueCameraMountingName(cameraMountingName);
		this->knownCameraMountings[0]->setName(cameraMountingName);

		// check for existing parameters
		this->selectPath(project.findExistingOrbitPath(this->knownOrbitPaths[0]->getName()));
		this->selectAttitudes(project.findExistingOrbitAttitudes(this->knownOrbitAttitudes[0]->getName()));
		this->selectCameraMounting(project.findExistingCameraMounting(this->knownCameraMountings[0]));
		this->selectCamera(project.findExistingCamera(this->knownCameras[0]->getName()));

	}

	return success;
}


bool CSatelliteDataImporter::recomputeOrbit(bool adjustPath,bool adjustAttitudes)
{
	bool ret;
	this->satelliteReader->setProgressListener(this->listener);
	ret = this->satelliteReader->recomputeOrbit(adjustPath,adjustAttitudes);
	this->satelliteReader->removeProgressListener();

	return ret;
}

double CSatelliteDataImporter::getAimedRMSPath() const
{
	return this->satelliteReader->getAimedRMSPath();
}

double CSatelliteDataImporter::getAimedRMSAttitudes() const
{
	return this->satelliteReader->getAimedRMSAttitudes();
}

double CSatelliteDataImporter::getCurrentRMSPath() const
{
	return this->satelliteReader->getRMSPath();
}

double CSatelliteDataImporter::getCurrentRMSAttitudes() const
{
	return this->satelliteReader->getRMSAttitudes();
}

void CSatelliteDataImporter::setAimedRMSPath(double rms)
{
	this->satelliteReader->setAimedRMSPath(rms);
}

void CSatelliteDataImporter::setAimedRMSAttitudes(double rms)
{
	this->satelliteReader->setAimedRMSAttitudes(rms);
}

double CSatelliteDataImporter::getFirstLineTime() const
{
	return this->satelliteReader->getFirstLineTime();
}

double CSatelliteDataImporter::getTimePerLine() const
{
	return this->satelliteReader->getTimePerLine();
}


void CSatelliteDataImporter::getKnownOrbitPaths(vector<CCharString>& names,unsigned int& selected) const
{
	names.clear();
	names.push_back("Use from file");

	for (unsigned int i=1; i < this->knownOrbitPaths.size(); i++)
	{
		names.push_back(this->knownOrbitPaths[i]->getName());
	}

	selected = this->selectedPath;
}

void CSatelliteDataImporter::getKnownOrbitAttitudes(vector<CCharString>& names,unsigned int& selected) const
{
	names.clear();
	names.push_back("Use from file");

	for (unsigned int i=1; i < this->knownOrbitAttitudes.size(); i++)
	{
		names.push_back(this->knownOrbitAttitudes[i]->getName());
	}

	selected = this->selectedAttitudes;
}

void CSatelliteDataImporter::getKnownCameraMountings(vector<CCharString>& names,unsigned int& selected) const
{
	names.clear();
	names.push_back("Use from file");

	for (unsigned int i=1; i < this->knownCameraMountings.size(); i++)
	{
		names.push_back(this->knownCameraMountings[i]->getName());
	}

	selected = this->selectedCameraMounting;
}

void CSatelliteDataImporter::getKnownCameras(vector<CCharString>& names,unsigned int& selected) const
{
	names.clear();
	names.push_back("Use from file");

	for (unsigned int i=1; i < this->knownCameras.size(); i++)
	{
		names.push_back(this->knownCameras[i]->getName());
	}

	selected = this->selectedCamera;
}


void CSatelliteDataImporter::selectPath(unsigned int index)
{
	if (index < this->knownOrbitPaths.size())
		this->selectedPath = index;
}

void CSatelliteDataImporter::selectAttitudes(unsigned int index)
{
	if (index < this->knownOrbitAttitudes.size())
		this->selectedAttitudes = index;
}

void CSatelliteDataImporter::selectCameraMounting(unsigned int index)
{
	if (index < this->knownCameraMountings.size())
		this->selectedCameraMounting = index;
}

void CSatelliteDataImporter::selectCamera(unsigned int index)
{
	if (index < this->knownCameras.size())
		this->selectedCamera = index;
}


void CSatelliteDataImporter::selectPath(COrbitObservations* existingPath)
{
	this->selectedPath = 0;
	
	if (existingPath)
	{
		for (unsigned int i=1; i < this->knownOrbitPaths.size();i++)
		{
			if (existingPath == this->knownOrbitPaths[i])
			{
				this->selectedPath = i;
				break;
			}
		}
	}
}

void CSatelliteDataImporter::selectAttitudes(COrbitObservations* existingAttitudes)
{
	this->selectedAttitudes = 0;

	if (existingAttitudes)
	{
		for (unsigned int i=1; i < this->knownOrbitAttitudes.size();i++)
		{
			if (existingAttitudes == this->knownOrbitAttitudes[i])
			{
				this->selectedAttitudes = i;
				break;
			}
		}
	}
}

void CSatelliteDataImporter::selectCameraMounting(CCameraMounting* existingMounting)
{
	this->selectedCameraMounting = 0;
	
	if (existingMounting)
	{
		for (unsigned int i=1; i < this->knownCameraMountings.size();i++)
		{
			if (existingMounting == this->knownCameraMountings[i])
			{
				this->selectedCameraMounting = i;
				break;
			}
		}
	}
}

void CSatelliteDataImporter::selectCamera(CCamera* existingCamera)
{
	this->selectedCamera = 0;
	
	if (existingCamera)
	{
		for (unsigned int i=1; i < this->knownCameras.size();i++)
		{
			if (existingCamera == this->knownCameras[i])
			{
				this->selectedCamera = i;
				break;
			}
		}
	}
}


void CSatelliteDataImporter::cancelImport()
{
	this->filename.Empty();

	if (this->knownOrbitPaths[0])
		project.deleteOrbitObservation(this->knownOrbitPaths[0]);
	this->knownOrbitPaths[0]=NULL;

	if (this->knownOrbitAttitudes[0])
		project.deleteOrbitObservation(this->knownOrbitAttitudes[0]);
	this->knownOrbitAttitudes[0] = NULL;

	if (this->knownCameraMountings[0])
		project.deleteCameraMounting(this->knownCameraMountings[0]);
	this->knownCameraMountings[0] = NULL;

	if (this->knownCameras[0])
		project.deleteCamera(this->knownCameras[0]);
	this->knownCameras[0] = NULL;
}


void CSatelliteDataImporter::collectParameter()
{
	for (int i=0; i < project.getNumberOfOrbits(); i++)
	{
		COrbitObservations* orbit = project.getOrbitObservationAt(i);
		if (orbit->getSplineType() == Path)
		{
			this->knownOrbitPaths.push_back(orbit);
		}
		else if (orbit->getSplineType() == Attitude)
		{
			this->knownOrbitAttitudes.push_back(orbit);
		}

	}

	for (int i=0; i < project.getNumberOfCameraMountings(); i++)
	{
		this->knownCameraMountings.push_back(project.getCameraMountingAt(i));
	}

	for (int i=0; i < project.getNumberOfCameras(); i++)
	{
		CCamera* camera =project.getCameraAt(i);
		if (camera->getCameraType() == eCCD)
			this->knownCameras.push_back(camera);
	}

}


int CSatelliteDataImporter::getActiveAdditionalParameter()const
{
//	if (this->satellite == Alos)
//		return PARAMETER_1 + PARAMETER_2 + PARAMETER_3 + PARAMETER_4 + PARAMETER_5 + PARAMETER_6;
//	else 
		return 0;
}

int CSatelliteDataImporter::getActiveInteriorParameter() const
{
	return 0;
}

int CSatelliteDataImporter::getActiveMountingRotationParameter()const
{
	return 0;
}

int CSatelliteDataImporter::getActiveMountingShiftParameter()const
{
	return 0;
}

CSensorModel* CSatelliteDataImporter::getOrbitPath() 
{
	return this->knownOrbitPaths[this->selectedPath]->getCurrentSensorModel();
}

CSensorModel* CSatelliteDataImporter::getOrbitAttitudes() 
{
	return this->knownOrbitAttitudes[this->selectedAttitudes]->getCurrentSensorModel();
}
	