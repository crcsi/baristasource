#include "VALSArray.h"
#include "VALSPtrArray.h"
#include "VALS.h"
#include "Serializable.h"
#include "StructuredFileSectionArray.h"

CVALSArray::CVALSArray(int nGrowBy) : 
	CMUObjectArray<CVALS, CVALSPtrArray>(nGrowBy)
{
}

CVALSArray::~CVALSArray(void)
{
}


/**
 * Serialized storage of a CVALSArray to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVALSArray::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

    for (int i = 0; i < this->GetSize(); i++)
    {
        CVALS* vALS = this->GetAt(i);
        CStructuredFileSection* newSection = pStructuredFileSection->addChild();
        vALS->serializeStore(newSection);
    }
}

/**
 * Restores a CVALSArray from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVALSArray::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

    CStructuredFileSectionArray* children = pStructuredFileSection->getChildren();
    for (int i = 0; i < children->GetSize(); i++)
    {
        CStructuredFileSection* child = children->GetAt(i);
        CVALS* vALS = this->Add();
        vALS->serializeRestore(child);
    }
}

/**
 * reconnects pointers
 */
void CVALSArray::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CVALSArray::resetPointers()
{
    CSerializable::resetPointers();
}

/**
 * Flag if CVALSArray is modified
 * returns bool
 */
bool CVALSArray::isModified()
{
    for (int i = 0; i < this->GetSize(); i++)
    {
        if (this->GetAt(i)->isModified())
            return true;
    }

    return false;
}


/** SLOW FOR LOTS OF ALSs!
 * Gets a ALS by label (null if not found)
 *
 * @param CCharString Label of ALS
 */
int CVALSArray::getbyName(const CCharString &name) const
{
    CVALS* test = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        test = this->GetAt(i);

		if (name.CompareNoCase(test->getFileNameChar()))
            return i;

        continue;
    }

    return -1;
}

/** SLOW FOR LOTS OF ALSs!
 * Gets a ALS by label (null if not found)
 *
 * @param CCharString Label of ALS
 */
CVALS* CVALSArray::getALSbyName(const CCharString &name) const
{
    CVALS* test = NULL;

    for (int i = 0; i < this->GetSize(); i++)
    {
        test = this->GetAt(i);
		if (name.CompareNoCase(test->getFileNameChar()))
            return test;

        continue;
    }

    return NULL;
}
