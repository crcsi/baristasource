#include "TheosReader.h"
#include "utilities.h"
#include "XMLParser.h"
#include "TheosXMLDataHandler.h"
#include "XMLErrorHandler.h"
#include "BundleHelper.h"
#include "OrbitObservations.h"
#include "RotationBaseFactory.h"
#include "OrbitAttitudeModel.h"
#include "AdjustMatrix.h"
#include "OrbitPathModel.h"
#include "CCDLine.h"
#include "QuaternionRotation.h"

CTheosReader::CTheosReader(void) : CSatelliteDataReader()
{
	cx=cy=cz=0.0;
}

CTheosReader::~CTheosReader(void)
{
}

bool CTheosReader::readData(CFileName &filename)
{
	if (this->aimedRMSAttitudes < 0.0 || this->aimedRMSPath < 0.0)
	{
		this->errorMessage = "No thresholds for RMS set!";
		return false;
	}


	if (this->listener)
	{
		this->listener->setProgressValue(10);
		this->listener->setTitleString("Parsing Metadata File ...");
	}


	// parse the file and get all the needed information
	CXMLParser parser;

	// the parser will delete the memory of the dataHandler!!
	parser.parseFile(filename,new CTheosXMLDataHandler(this->dataStorage), new CXMLErrorHandler());

	this->satInfo.nCols = this->dataStorage.ncols;
	this->satInfo.nRows = this->dataStorage.nrows;
	this->satInfo.imageID = this->dataStorage.dataStripID;
	this->satInfo.imageType = this->dataStorage.nbands == 1 ? "P" : "Multi";
	this->satInfo.cameraName = "THEOS-" + this->satInfo.imageType;
	
	// compute the time of the first line 
	this->firstLineTime = this->dataStorage.referenceTime - this->dataStorage.linePeriod * (this->dataStorage.referenceLine-1);
	this->lastLineTime = this->firstLineTime + this->dataStorage.linePeriod * this->dataStorage.nrows;

	this->timePerLine = this->dataStorage.linePeriod;


	if (this->listener)
	{
		this->listener->setProgressValue(20.0);
		this->listener->setTitleString("Processing Look Angles ...");
		this->setMaxListenerValue(80);
	}	

	if (!this->prepareLookAngles())
		return false;


	this->camera->setNCols(this->dataStorage.ncols);
	this->camera->setNRows(this->dataStorage.nrows);


	this->orbitPoints->initObservations();
	this->orbitPoints->setFileName(filename);

	this->attitudes->initObservations();
	this->attitudes->setFileName(filename);


	this->setMaxListenerValue(99);
	if (this->listener)
		this->listener->setTitleString("Computing Orbit ...");

	double startTime,endTime;
	this->computeInterval(startTime,endTime);

	if(!this->adjustOrbit(true,true,startTime,endTime,3,3))
		return false;


	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
		this->listener->setTitleString("Finish Reading");
	}

	return true;
}

bool CTheosReader::prepareEphemeris()	//virtual
{
	int sizeTriode = this->dataStorage.triodeOrbitPoints.GetSize();	

	CXYZOrbitPointArray tmpPoints;	
	CXYZOrbitPoint* triode;
	
	for (int i=0; i < sizeTriode; i++)	//added
	{	
		triode= this->dataStorage.triodeOrbitPoints.getAt(i);
		tmpPoints.add(*triode);
	}

	int size = tmpPoints.GetSize();
	if (size < 3)
	{
		this->errorMessage = "Not enough orbit points found!";
		return false;
	}

	// determine index of last point before the scene actually starts
	int indexFirstPoint;
	int indexLastPoint;

	for (indexFirstPoint=0; indexFirstPoint < size; indexFirstPoint++)
	{
		if (tmpPoints.GetAt(indexFirstPoint)->dot >= this->firstLineTime)
			break;
	}	
	if(indexFirstPoint>0)
		indexFirstPoint--;
	else
	{
		this->errorMessage = "Lack orbit points before the scene!";		
		//return false;
	}
		
	for(int i = 0;i< 4 && indexFirstPoint>0; i++) //use at most 4 more points before the scene if possible
		indexFirstPoint--;

	// determine index of first point after the scene ends	
	for (indexLastPoint = size-1; indexLastPoint >= 0; indexLastPoint--)
	{
		if (tmpPoints.GetAt(indexLastPoint)->dot <= this->lastLineTime)
			break;
	}	
	if(indexLastPoint<size-1)
		indexLastPoint++;
	else
	{
		this->errorMessage ="Lack orbit points after the scene!";
		//return false;
	}
		
	for(int i = 0;i< 4 && size-1-indexLastPoint>0; i++)	//use at most 4 more points after the scene if possible
		indexLastPoint++;

	if (indexLastPoint <= indexFirstPoint)
	{
		this->errorMessage = "Wrong order of orbit points!";	
		return false;
	}

	indexFirstPoint=0;
	indexLastPoint=size-1;

	//copy the observation points to orbitPoints
	for (int offset = indexFirstPoint; offset <= indexLastPoint ; offset++)
	{
		CXYZOrbitPoint* psrc = tmpPoints.getAt(offset);
		CXYZOrbitPoint* pdes = ((CXYZOrbitPointArray*)this->orbitPoints->getObsPoints())->add();
		
		pdes->dot = psrc->dot;
		pdes->setHasVelocity(psrc->getHasVelocity());//(false);//
		for(int i=0;i<6;i++)(*pdes)[i]=(*psrc)[i];
		
		pdes->setCovarElement(0,0,2);//the accuracy of the ephemeris observation,it significantly affects the spline fitting procedure
		pdes->setCovarElement(1,1,2);	
		pdes->setCovarElement(2,2,2);
		pdes->setCovarElement(3,3,2);	
		pdes->setCovarElement(4,4,2);	
		pdes->setCovarElement(5,5,2);
	}

	//no need to interpolate points as the observation in THEOS is at the interval of 0.25sec


	CXYZOrbitPointArray* newOrbitPoints = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();
	FILE* out= fopen("Theos_path.txt","w");
	for (int i=0; i < newOrbitPoints->GetSize(); i++)
	{
		CXYZOrbitPoint* p = newOrbitPoints->getAt(i);
		fprintf(out,"%20.15lf %20.15lf %20.15lf %20.15lf %20.15lf %20.15lf %20.7lf\n",(*p)[0],(*p)[1],(*p)[2],(*p)[3],(*p)[4],(*p)[5],p->dot);
	}
	fclose(out);


	return true;
}

bool CTheosReader::prepareAttitudes()
{
	COrbitAttitudeModel* modelAttitudes = (COrbitAttitudeModel*) this->attitudes->getCurrentSensorModel();
	COrbitPathModel* modelPath = (COrbitPathModel*) this->orbitPoints->getCurrentSensorModel();

	CXYZOrbitPointArray* attRPYAngles = (CXYZOrbitPointArray*)this->attitudes->getObsPoints();
	attRPYAngles->setCreateWithVelocity(false);
	// clear the array
	attRPYAngles->SetSize(0);

	CXYZOrbitPointArray* orbitPointArray = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();	
	CObsPointArray* originalAttQuats=&this->dataStorage.attitudeQuaternions;
	//Quaternion Attitudes are referential to ECI, need to be transformed to ECF
	
	int size = originalAttQuats->GetSize();
	// determine index of last point before the scene actually starts
	int indexFirstPoint;
	int indexLastPoint;

	for (indexFirstPoint=0; indexFirstPoint < size; indexFirstPoint++)
	{
		if (originalAttQuats->GetAt(indexFirstPoint)->dot >= this->firstLineTime)
			break;
	}	
	if(indexFirstPoint>0)
		indexFirstPoint--;
	else
	{
		this->errorMessage ="Lack attitude points before the scene!";
		//return false;
	}
	for(int i = 0;i< 4 && indexFirstPoint>0; i++) //use at most 4 more points before the scene if possible
		indexFirstPoint--;

	// determine index of first point after the scene ends
	
	for (indexLastPoint = size-1; indexLastPoint >= 0; indexLastPoint--)
	{
		if (originalAttQuats->GetAt(indexLastPoint)->dot <= this->lastLineTime)
			break;
	}	
	if(indexLastPoint< size-1)
		indexLastPoint++;
	else
	{
		this->errorMessage ="Lack attitude points after the scene!";
		//return false;
	}

	for(int i = 0;i< 4 && size-1-indexLastPoint> 0; i++)	//use at most 4 more points after the scene if possible
		indexLastPoint++;
	
	if (indexLastPoint <= indexFirstPoint)
	{
		this->errorMessage = "Wrong order of attitude points!";
		return false;
	}

	indexFirstPoint=0;
	indexLastPoint=size-1;

	// the time to compute the fixed matrix	
	double middleTime = (this->lastLineTime + this->firstLineTime) / 2.0;	
	int middleIndex=(indexFirstPoint+indexLastPoint)/2;

	if(originalAttQuats->GetAt(middleIndex)->dot < middleTime)
	{
		while (middleIndex <=indexLastPoint && originalAttQuats->GetAt(middleIndex)->dot < middleTime)
			middleIndex++;
	}else{		
		while (middleIndex >=indexFirstPoint && originalAttQuats->GetAt(middleIndex)->dot > middleTime)
			middleIndex--;
	}

	if (middleIndex < indexFirstPoint || middleIndex > indexLastPoint)
	{
		this->errorMessage.Format("No Attitude Data Points found!");
		return false;
	}

	double fixedTime=originalAttQuats->GetAt(middleIndex)->dot;
		
	C3DPoint location, velocity;
	bool bExist=false;
	modelPath->evaluate(location,velocity,fixedTime);
	
	//get the tangential matrix at the "fixedTime"
	//it is dependent on the definition of the platform coordinate system,so that the remaining Rp(t) is with small rotation angles. 
	CRotationMatrix fixedMatrix;

	C3DPoint X,Y,Z;
	Z = -location /  location.getNorm();//for THEOS, axis Zp points downward 
	Y = Z.crossProduct(velocity);		//Xp along the velocity direction, and Yp complete the crossproduct
	Y = Y / Y.getNorm();
	X = Y.crossProduct(Z);

	fixedMatrix(0,0) = X[0];
	fixedMatrix(1,0) = X[1];
	fixedMatrix(2,0) = X[2];

	fixedMatrix(0,1) = Y[0];
	fixedMatrix(1,1) = Y[1];
	fixedMatrix(2,1) = Y[2];

	fixedMatrix(0,2) = Z[0];
	fixedMatrix(1,2) = Z[1];
	fixedMatrix(2,2) = Z[2];

	modelAttitudes->setFixedECRMatrix(fixedMatrix);	// save Rfixed!
	modelAttitudes->setTimeFixedECRMatrix(fixedTime); // and the time		

	CObsPoint *p = originalAttQuats->GetAt(middleIndex); // use the point at the fixed time
	CQuaternionRotation* oldQuaternion=new CQuaternionRotation(p,eQQuickBird);
	//CRotationBase* oldQuaternion = CRotationBaseFactory::initRotation(p,Quaternion);

	// create an empty matrix: for {Roll = 0, Pitch = 0, Yaw = 0}
	CRotationBase* newRPY = CRotationBaseFactory::initRotation(&C3DPoint(),RPY);
	// we store the result  in here and compute then the final angles
	CRotationMatrix& newRPYMatrix = newRPY->getRotMat();

	CRotationMatrix rotECI2ECF(1,0,0,0,1,0,0,0,1),rotTmp1,rotTmp2;	
	
	//U,V are involved in the transformation from ECI to ECF
	CRotationMatrix rotUV;
	double cosu=cos(this->dataStorage.UV.x);
	double sinu=sin(this->dataStorage.UV.x);
	double cosv=cos(this->dataStorage.UV.y);
	double sinv=sin(this->dataStorage.UV.y);
	CRotationMatrix rotU(cosu,0,sinu,0,1,0,-sinu,0,cosu);
	CRotationMatrix rotV(1,0,0,0,cosv,-sinv,0,sinv,cosv);
	rotU.R_times_R(rotUV,rotV);	

	CObsPoint* dummy = NULL;	
	FILE* out1= fopen("Theos_AttQuats.txt","w");
	for (int i=indexFirstPoint; i<= indexLastPoint; i++)
	{
		dummy = originalAttQuats->GetAt(i);
		//change the order, in THEOS, the first one is the scaler element while in QB the last one is the scaler element 
		double scaler=(*dummy)[0];
		for (int j=0;j<3;j++)(*dummy)[j]=(*dummy)[j+1];
		(*dummy)[3]=scaler;

		oldQuaternion->updateAngles_Rho(dummy);
		fprintf(out1,"%20.15lf %20.15lf %20.15lf %20.15lf %20.7lf\n",(*dummy)[0],(*dummy)[1],(*dummy)[2],(*dummy)[3],dummy->dot);

		C3DPoint *pT=this->dataStorage.GPSTimes.GetAt(i);
		int Year=pT->x+0.1;
		int Month=pT->y+0.1;
		int Day=pT->z+0.1;
		double secs=pT->dot;
		calcRotECI2ECF(rotECI2ECF,Year,Month,Day,secs,this->dataStorage.UT1_UTC,this->dataStorage.UTC_GPST);
				
		//rotUV.R_times_R(rotTmp1,rotECI2ECF);	

		rotECI2ECF.R_times_RT(rotTmp2,oldQuaternion->getRotMat());		

		fixedMatrix.RT_times_R(newRPYMatrix,rotTmp2);//Rp=Ro'Rq'


		// update and save the parameter
		newRPY->computeParameterFromRotMat();

		CXYZOrbitPoint* angles = attRPYAngles->add();
		for (int k=0; k < 3; k++)
			(*angles)[k] = ((*newRPY)[k] < 0.0 )&& (fabs((*newRPY)[k] + M_PI) < 1.0) ? (*newRPY)[k]+ 2*M_PI: (*newRPY)[k];
		angles->dot = dummy->dot;

		(*angles) *= 1000.0; // save in m rad
		angles->setCovarElement(0,0,0.001);//0.00001rad = 2 sec
		angles->setCovarElement(1,1,0.001);
		angles->setCovarElement(2,2,0.001);

		(*angles->getCovariance()) *= 1000000.0;	//covar as well	


		if (this->listener)
		{
			double per = (double) i / (double)(indexLastPoint-indexFirstPoint+1);
			this->listener->setProgressValue(per*this->getMaxListenerValue());
		}
	}
	fprintf(out1,"roll,pitch,yaw angles\n");
	for (int i=indexFirstPoint; i<= indexLastPoint; i++)
	{
		dummy = originalAttQuats->GetAt(i);
		//change the order, in THEOS, the first one is the scaler element while in QB the last one is the scaler element 
		double scaler=(*dummy)[0];
		for (int j=0;j<3;j++)(*dummy)[j]=(*dummy)[j+1];
		(*dummy)[3]=scaler;
		
		oldQuaternion->updateAngles_Rho(dummy);
		newRPYMatrix=oldQuaternion->getRotMat();
		newRPY->computeParameterFromRotMat();
		
		fprintf(out1,"%20.15lf %20.15lf %20.15lf %20.15lf\n",(*newRPY)[0],(*newRPY)[1],(*newRPY)[2],dummy->dot);		
	}

	fclose(out1);

	
	delete oldQuaternion;
	delete newRPY;


	FILE* out= fopen("Theos_newRPY.txt","w");
	for (int i=0; i < attRPYAngles->GetSize(); i++)
	{
		CXYZOrbitPoint* p = attRPYAngles->getAt(i);
		fprintf(out,"%20.15lf %20.15lf %20.15lf %20.7lf\n",(*p)[0],(*p)[1],(*p)[2],p->dot);
	}
	fclose(out);

	return true;
}


bool CTheosReader::prepareLookAngles()
{
	//calculate camera and cameraMounting parameters

	C2DPointArray lookAngles;
	CObsPoint * pp=&(this->dataStorage.polynomialLookAngles);
	
	FILE *out = fopen("Theos_lookAngles.txt","w");
	int nseg=200;
	//for(int seg=0;seg<dataStorage.ncols;seg++)
	for(int seg=0;seg<=nseg;seg++)
	{		
		int i=dataStorage.ncols*seg/nseg;
		if(i==0)i=1;
		//i=seg;

		C2DPoint * pa=lookAngles.Add();
		pa->id = i;
		pa->x = ((*pp)[0]+(*pp)[1]*i+(*pp)[2]*pow(float(i),2)+(*pp)[3]*pow(float(i),3))/1000.0; //fi(x) along the detector array and fi(y) along the flight direction
		pa->y = ((*pp)[4]+(*pp)[5]*i+(*pp)[6]*pow(float(i),2)+(*pp)[7]*pow(float(i),3))/1000.0; //originally in milli-radian  

		fprintf(out,"%6d   %10.6lf    %10.6lf\n",pa->id,pa->x,pa->y);
	}
	fclose(out);
	
	C3DPoint angles_YPR(dataStorage.instrumentBiases[0],dataStorage.instrumentBiases[1],dataStorage.instrumentBiases[2]);
	C3DPoint angles_RPY(angles_YPR.z,angles_YPR.y,angles_YPR.x);
	CRollPitchYawRotation rotBias(&angles_RPY);
	
	/*/////////////////////////////////////////////////////////////////////////
	//set the irp and Rm manually
	double x0=this->dataStorage.ncols/2;
	double f=fabs((x0-lookAngles.GetAt(0)->id+1)/tan(lookAngles.GetAt(0)->x));
	double y0=-f*tan((lookAngles.GetAt(0)->y+lookAngles.GetAt(lookAngles.GetSize()/2)->y+lookAngles.GetAt(lookAngles.GetSize()-1)->y)/3);

	CXYZPoint irp(x0,y0,-f);
	this->camera->setIRP(irp);
	this->camera->setScale(0.0065); //pixel size in millimeter,it does not affect the pushbroom imaging model

	CRotationMatrix rotXY(0,-1,0,1,0,0,0,0,1);
	CRotationMatrix mountRotMat;
	rotBias.R_times_RT(mountRotMat,rotXY);//Rm=Rbias*Rxy'
	this->cameraMounting->setRotation(mountRotMat);

	this->satInfo.viewDirection = "THEOS-" + this->satInfo.imageID;

	this->cameraMounting->setShift(C3DPoint(0,0,0)); //Cm=Cmq

	return true;//*/
	//////////////////////////////////////////////////////////////////////////
	// compute the direction vector and save the values
	int nBands=this->dataStorage.nbands;
	int nObs=lookAngles.GetSize();

	vector<vector<C3DPoint> > directionVector(3);
	C3DPoint oldDirection;

	for (int i=0; i < nBands; i++)
	{
		directionVector[i].resize(nObs);
		for (int k = 0; k < nObs; k++)
		{
			//vector of pixel k in band i in RLOS reference frame	

			oldDirection.z  = 1;
			oldDirection.x  =  tan(lookAngles.GetAt(k)->y);
			oldDirection.y  = -tan(lookAngles.GetAt(k)->x);

			(directionVector[i])[k]=oldDirection;

		}
	}


	// ####################################################################
	// get start values for the adjustment

	// the start values for the rotation matrix Rc
	C3DPoint X(0,-1,0), Y(1,0,0), Z((directionVector[0])[nObs/2]); //differ from spot X(0,1,0)
	Z = Z / Z.getNorm();
	X = Y.crossProduct(Z);
	X = X / X.getNorm();
	Y = Z.crossProduct(X);

	CRotationMatrix initMat(X,Y,Z);
	CRollPitchYawRotation rotRPY(initMat);
	rotRPY[0] = 0; //set roll to 0 const
	rotRPY[1] = 0; //set pitch to 0 const as well

	rotRPY.computeRotMatFromParameter();

	Matrix A_Start(4,3);
	Matrix dl_Start(4,1);
	A_Start=0;
	dl_Start =0;

	//// the first CCD element( corrected with rot mat)
	C3DPoint p1;
	rotRPY.RT_times_x(p1,(directionVector[0])[0]);
	p1 /= -p1.z;  

	//// the first and last look angle of the first band
	//// p0
	A_Start.element(0,0) = 1.0;
	A_Start.element(0,2) = p1.x;
	A_Start.element(1,1) = 1.0;
	A_Start.element(1,2) = p1.y;
	dl_Start.element(0,0) = lookAngles.GetAt(0)->id;

	// the last CCD element
	rotRPY.RT_times_x(p1,(directionVector[0])[nObs-1]);
	p1 /= -p1.z; 

	A_Start.element(2,0) = 1.0;
	A_Start.element(2,2) = p1.x;
	A_Start.element(3,1) = 1.0;
	A_Start.element(3,2) = p1.y;
	dl_Start.element(2,0) = lookAngles.GetAt(nObs-1)->id;

	Matrix N_Start = A_Start.t() * A_Start;
	Matrix t_Start = A_Start.t() * dl_Start;
	Matrix dx_Start;
	try
	{
		Matrix Qxx = N_Start.i();
		dx_Start = Qxx * t_Start;
	}
	catch (...)
	{
		this->errorMessage = "Processing of Look Angles failed";
		return false;
	}



	// ####################################################################
	// compute mounting and xc,yc, c from look angles
	double redfac = 0.01; double ftmp;

	Matrix dx(6,1); Matrix dx5(5,1); Matrix dx4(4,1);
	Matrix Qxx;
	///5 unknowns
	dx.element(0,0) = 0; //ftmp= rotRPY[0]; //roll set to 0 const
	dx.element(1,0) = dx5.element(0,0)= ftmp= rotRPY[1];
	dx.element(2,0) = dx5.element(1,0)= ftmp= rotRPY[2];

	dx.element(3,0) = dx5.element(2,0)= ftmp= dx_Start.element(0,0) * redfac;
	dx.element(4,0) = dx5.element(3,0)= ftmp= dx_Start.element(1,0) * redfac;
	dx.element(5,0) = dx5.element(4,0)= ftmp= dx_Start.element(2,0) * redfac;//*/	

	double vTvOld = 0.0;
	double vTv = 100.0;

	C2DPoint *p;
	CAdjustMatrix dFdAngles(3,3);
	C3DPoint F;

	double tmp;
	double fac1;
	double fac2;
	double fac3;

	double count=0.0;

	CAdjustMatrix A(2,5);//A(2,6);//
	CAdjustMatrix dl(2,1,0.0);

	double listenerStartValue =0.0;
	if (this->listener)
		listenerStartValue = this->listener->getProgress();

	CRotationBase * rotBase = NULL;
	// iteration loop
	for (int j=0; (j < 20) && (fabs((vTv - vTvOld) / (vTv + vTvOld)) > 0.0001); j++)
	{
		vTvOld = vTv;
		vTv = 0.0;
		Matrix N(5,5);//N(6,6);//
		N=0;
		Matrix t(5,1);//t(6,1);//
		t=0;

		C3DPoint rpy(dx.element(0,0),dx.element(1,0),dx.element(2,0));
		rotBase = CRotationBaseFactory::initRotation(&rpy,RPY);
		CAdjustMatrix dRdAngles(9,3);

		rotBase->computeDerivation(dRdAngles);


		for (int band=0; band < nBands; band++)
		{
			int nLookAngles = lookAngles.GetSize();
			count++;

			// fill N Matrix
			for (int i=0; i< nLookAngles; i++)
			{
				// compute derivations dF/dAngles
				//revised,(directionVector[band])[i].z should not be ignored in case it's not 1,below as well
				dFdAngles(0,0) = (directionVector[band])[i].x * dRdAngles(0,0) + (directionVector[band])[i].y * dRdAngles(3,0) + (directionVector[band])[i].z * dRdAngles(6,0);
				dFdAngles(0,1) = (directionVector[band])[i].x * dRdAngles(0,1) + (directionVector[band])[i].y * dRdAngles(3,1) + (directionVector[band])[i].z * dRdAngles(6,1);
				dFdAngles(0,2) = (directionVector[band])[i].x * dRdAngles(0,2) + (directionVector[band])[i].y * dRdAngles(3,2) + (directionVector[band])[i].z * dRdAngles(6,2);

				dFdAngles(1,0) = (directionVector[band])[i].x * dRdAngles(1,0) + (directionVector[band])[i].y * dRdAngles(4,0) + (directionVector[band])[i].z * dRdAngles(7,0);
				dFdAngles(1,1) = (directionVector[band])[i].x * dRdAngles(1,1) + (directionVector[band])[i].y * dRdAngles(4,1) + (directionVector[band])[i].z * dRdAngles(7,1);
				dFdAngles(1,2) = (directionVector[band])[i].x * dRdAngles(1,2) + (directionVector[band])[i].y * dRdAngles(4,2) + (directionVector[band])[i].z * dRdAngles(7,2);

				dFdAngles(2,0) = (directionVector[band])[i].x * dRdAngles(2,0) + (directionVector[band])[i].y * dRdAngles(5,0) + (directionVector[band])[i].z * dRdAngles(8,0);
				dFdAngles(2,1) = (directionVector[band])[i].x * dRdAngles(2,1) + (directionVector[band])[i].y * dRdAngles(5,1) + (directionVector[band])[i].z * dRdAngles(8,1);
				dFdAngles(2,2) = (directionVector[band])[i].x * dRdAngles(2,2) + (directionVector[band])[i].y * dRdAngles(5,2) + (directionVector[band])[i].z * dRdAngles(8,2);


				rotBase->RT_times_x(F,(directionVector[band])[i]);

				tmp = 1.0/F.z;
				fac1 =  -dx.element(5,0)*tmp;
				fac2 = -fac1*F.x*tmp;
				fac3 = -fac1*F.y*tmp;

				/*
				A(0,0) = fac1 * dFdAngles(0,0) + fac2 * dFdAngles(2,0);
				A(0,1) = fac1 * dFdAngles(0,1) + fac2 * dFdAngles(2,1);
				A(0,2) = fac1 * dFdAngles(0,2) + fac2 * dFdAngles(2,2);

				A(1,0) = fac1 * dFdAngles(1,0) + fac3 * dFdAngles(2,0);
				A(1,1) = fac1 * dFdAngles(1,1) + fac3 * dFdAngles(2,1);
				A(1,2) = fac1 * dFdAngles(1,2) + fac3 * dFdAngles(2,2);

				A(0,3) = 1.0;
				A(0,4) = 0.0;
				A(0,5) = -F.x*tmp;

				A(1,3) = 0.0;
				A(1,4) = 1.0;				
				A(1,5) = -F.y*tmp;//*/

				///5 unknowns
				A(0,0) = fac1 * dFdAngles(0,1) + fac2 * dFdAngles(2,1);
				A(0,1) = fac1 * dFdAngles(0,2) + fac2 * dFdAngles(2,2);
				A(0,2) = 1.0;
				A(0,3) = 0.0;
				A(0,4) = -F.x*tmp;
				
				A(1,0) = fac1 * dFdAngles(1,1) + fac3 * dFdAngles(2,1);
				A(1,1) = fac1 * dFdAngles(1,2) + fac3 * dFdAngles(2,2);
				A(1,2) = 0.0;
				A(1,3) = 1.0;				
				A(1,4) = -F.y*tmp;//*/

				CAdjustMatrix AT = A;
				AT.transpose();

				CAdjustMatrix ATPA = AT * A;
				ATPA.addToMatrix(0,0,N);


				dl(0,0) = (lookAngles.GetAt(i)->id) *redfac - dx.element(3,0) + dx.element(5,0) * F.x/F.z;
				dl(1,0) = -dx.element(4,0) + dx.element(5,0) * F.y/F.z;
				vTv += dl(0,0) * dl(0,0) + dl(1,0) * dl(1,0);

				CAdjustMatrix ATdl = AT * dl;
				ATdl.addToMatrix(0,0,t);
			}
			///
			for (int i=0;i<5;i++)//i=5 for 5 unknowns,6 for 6 unknowns
				N.element(i,0) += this->dataStorage.ncols; //so that droll/dpitch would be close to 0,i.e. roll/pitch close to 0;				
			//*/
		}
		Qxx = N.i();
		dx5 += Qxx * t; for(int i=0;i< 5;i++)dx.element(i+1,0)=dx5.element(i,0);//5 unknowns

		delete rotBase;
		rotBase = NULL;

		if (this->listener)
		{
			double per = listenerStartValue + ((double) (j+1)/8.0 ) *(this->listenerMaxValue - listenerStartValue );
			this->listener->setProgressValue(per);
		}
	}	

	CXYZPoint irp(dx.element(3,0)/redfac,dx.element(4,0)/redfac, dx.element(5,0)/redfac);

	this->camera->setIRP(irp);
	this->camera->setScale(0.0065);//pixel size

	C3DPoint angles(dx.element(0,0),dx.element(1,0),dx.element(2,0));

	CRollPitchYawRotation rotMat(&angles); //Rc
	CRotationMatrix rotMounting;
	rotBias.R_times_R(rotMounting,rotMat.getRotMat());//Rm=Rbias*Rc
	this->cameraMounting->setRotation(rotMounting);

	this->cameraMounting->setShift(CXYZPoint(0.0,0.0,0.0));// no shift

	double viewAngle = this->cameraMounting->getRotation()[0];

	if (viewAngle < 0) viewAngle +=2*M_PI;


	this->satInfo.viewDirection = "Theos-" + this->dataStorage.dataStripID;
	/*
	double s0 = sqrt(vTv / (2*nObs - 5));
	C3DPoint sig(s0 * sqrt(Qxx.element(3,3)) / redfac, s0 * sqrt(Qxx.element(4,4)) / redfac, s0 * sqrt(Qxx.element(5,5)) / redfac);
	C3DPoint sigr(s0 * sqrt(Qxx.element(0,0)), s0 * sqrt(Qxx.element(1,1)),s0 * sqrt(Qxx.element(2,2)));//*/
	///for 5 unknowns
	double s0 = sqrt(vTv / (2*nObs - 5));
	C3DPoint sig(s0 * sqrt(Qxx.element(2,2)) / redfac, s0 * sqrt(Qxx.element(3,3)) / redfac, s0 * sqrt(Qxx.element(4,4)) / redfac);
	C3DPoint sigr(0, s0 * sqrt(Qxx.element(0,0)),s0 * sqrt(Qxx.element(1,1)));//*/

	FILE* outQxx;
	outQxx = fopen("Theos_Qxx.txt","w");

	fprintf(outQxx,"s0 : %20.3lf\n",s0);
	fprintf(outQxx,"x: %20.3lf   sx : %20.3lf\n",irp.x,sig.x);
	fprintf(outQxx,"y: %20.3lf   sy : %20.3lf\n",irp.y,sig.y);
	fprintf(outQxx,"c: %20.3lf   sz : %20.3lf\n",irp.z,sig.z);
	fprintf(outQxx,"roll : %20.15lf   sroll  : %20.15lf\n",angles.x,sigr.x);
	fprintf(outQxx,"pitch: %20.15lf   spitch : %20.15lf\n",angles.y,sigr.y);
	fprintf(outQxx,"yaw  : %20.15lf   syaw   : %20.15lf\n",angles.z,sigr.z);

	fprintf(outQxx,"\nQxx\n");
	for (int i = 0; i < Qxx.Nrows(); i++)
	{
		for (int k=0; k < Qxx.Ncols(); k++)
		{
			fprintf(outQxx,"%20.3lf",Qxx(i+1,k+1));
		}
		fprintf(outQxx,"\n");
	}

	fprintf(outQxx,"\ncorrelations\n\n");
	for (int i = 0; i < Qxx.Nrows(); i++)
	{
		for (int k=0; k < Qxx.Ncols(); k++)
		{
			fprintf(outQxx,"%20.3lf",Qxx(i+1,k+1)/(sqrt(Qxx(i+1,i+1))*sqrt(Qxx(k+1,k+1))));
		}
		fprintf(outQxx,"\n");
	}
	fclose(outQxx);
	//////////////////////////////////////////////////////////////////////////
	//IO residuals compensation
	// x_bias_pl = a0 + a1*xf + a2*xf^2 + a3*xf^3  //x_pl is in pixel,and xf is in pixel*scale
	// y_bias_pl = b0 + b1*xf + b2*xf^2 + b3*xf^3
	CAdjustMatrix Ax(1,4),Ay(1,4);	
	CAdjustMatrix dlx(1,1),dly(1,1);
	Matrix Nx(4,4),Ny(4,4);Nx=0;Ny=0;
	Matrix tx(4,1),ty(4,1);tx=0;ty=0;
	C3DPoint rpy(dx.element(0,0),dx.element(1,0),dx.element(2,0));
	rotBase = CRotationBaseFactory::initRotation(&rpy,RPY);
	vTv =0.0;
	FILE* outv = fopen("Theos_v.txt","w");
	for (int band=0; band < nBands; band++)
	{
		int nLookAngles = nObs;

		for (int i=0; i< nLookAngles; i++)
		{
			C3DPoint F;
			rotBase->RT_times_x(F,(directionVector[band])[i]);

			double tmp = 1.0/F.z;
			double fac1 =  -dx.element(5,0)*tmp;
			double fac2 = -fac1*F.x*tmp;
			double fac3 = -fac1*F.y*tmp;

			dlx(0,0) = -lookAngles.GetAt(i)->id *redfac + (dx.element(3,0) - dx.element(5,0) *  F.x*tmp);
			dly(0,0) = -0 + (dx.element(4,0) - dx.element(5,0) * F.y*tmp);
			fprintf(outv,"%30.20lf  %30.20lf\n",dlx(0,0),dly(0,0));
			vTv += dlx(0,0) * dlx(0,0) + dly(0,0) * dly(0,0);


			Ax(0,0)=Ay(0,0)=1;
			Ax(0,1)=Ay(0,1)=lookAngles.GetAt(i)->id*this->camera->getScale();
			Ax(0,2)=Ay(0,2)=pow(Ax(0,1),2);
			Ax(0,3)=Ay(0,3)=pow(Ax(0,1),3);

			CAdjustMatrix AxT = Ax;	AxT.transpose();
			CAdjustMatrix ATPAx = AxT * Ax;
			ATPAx.addToMatrix(0,0,Nx);

			CAdjustMatrix AyT = Ay;	AyT.transpose();
			CAdjustMatrix ATPAy = AyT * Ay;
			ATPAy.addToMatrix(0,0,Ny);

			dlx(0,0)=dlx(0,0)/redfac; //in pixel
			CAdjustMatrix ATdx = AxT * dlx;
			ATdx.addToMatrix(0,0,tx);

			dly(0,0)=dly(0,0)/redfac;
			CAdjustMatrix ATdy = AyT * dly;
			ATdy.addToMatrix(0,0,ty);//*/			
		}
	}
	fprintf(outv,"vTPv %30.20lf",vTv);
	fclose(outv);
	delete rotBase;
	rotBase = NULL;

	Try{
		Matrix px = Nx.i() * tx;
		Matrix py = Ny.i() * ty;

		doubles parameter;
		parameter.push_back(px.element(0,0));
		parameter.push_back(px.element(1,0));
		parameter.push_back(px.element(2,0));
		parameter.push_back(px.element(3,0));
		parameter.push_back(py.element(0,0));
		parameter.push_back(py.element(1,0));
		parameter.push_back(py.element(2,0));
		parameter.push_back(py.element(3,0));

		this->camera->setDistortion(eDistortionTHEOS,parameter);

	}CatchAll{
		this->errorMessage = "Failed computing the image distortion";
		return false;
	}//*/
	//////////////////////////////////////////////////////////////////////////

	return true;	//*/
}


void CTheosReader::computeTangentialMatrix(CRotationMatrix& mat,const C3DPoint& location, const C3DPoint& velocity)
{	//rotates from orbit tangential system to ECR	
	//here the matrix is the rotation between the ECF and the satellite-defined orbit/navigation system
	//for Theos, x is along velocity,z points the earth center, y completes the cross-production

	C3DPoint X,Y,Z;

	Z = -location /  location.getNorm();
	Y = Z.crossProduct(velocity);
	Y = Y/Y.getNorm();
	X = Y.crossProduct(Z);

	mat(0,0) = X[0];
	mat(1,0) = X[1];
	mat(2,0) = X[2];

	mat(0,1) = Y[0];
	mat(1,1) = Y[1];
	mat(2,1) = Y[2];

	mat(0,2) = Z[0];
	mat(1,2) = Z[1];
	mat(2,2) = Z[2];
}

double CTheosReader::thetaECI2ECF(int Y,int M,int D,double GPST,double UT1_UTC,double UTC_GPST)
{
	//secs=hour*3600+min*60+sec
	//Y,M,D,secs are in GPSTime	
	if(M<=2)
	{
		Y=Y-1;
		M=M+12;
	}
	int A = Y/100;
	int B = A/4;
	int C = 2-A+B;
	int E = 365.25 * (Y+4716);
	int F = 30.6001 * (M+1);
	double JD= C+D+E+F-1524.5;  //UTC 0h Julian Day, in half day

	//method 1
	double Tu=(JD-2451545.0)/36525.0;
	double thetas = 24110.54841 + 8640184.812866*Tu + 0.093104*Tu*Tu - 0.0000062*Tu*Tu*Tu; //in time sec, refer to poli's thesis section 3.2.2	
	thetas=thetas-int(thetas/86400)*86400;		//within (0,86400)
	double angle0=thetas*2*M_PI/86400.0; //convert to radian

	double anglespeed=7.29212e-5;		//earth rotation angular speed, in radian per sec
	angle0+=anglespeed*(GPST+UTC_GPST+UT1_UTC); //UT1=UTC+UT1_UTC=GPST+UTC_GPST+UT1_UTC

	if(angle0>2*M_PI)angle0-=2*M_PI;	//within (0,2PI)

	
	//method 2
	//��o = 280.46061837 + 360.98564736629*(JD-2451545.0) + 0.000387933*T^2 - T^3/38710000  //in degree
	double JDf=JD+(GPST+(UTC_GPST+UT1_UTC))/86400.0; //in Julian Day with decimal
	double T=(JDf-2451545.0)/36525.0;
	angle0=	double(280.46061837)+360.98564736629*(JDf-2451545.0)+0.000387933*pow(T,2)+pow(T,3)/38710000.0;
	angle0=angle0-int(angle0/360)*360;		//within (0,360)
	angle0=angle0*M_PI/180.0;	//convert to radian//*/

	return angle0;
}

void CTheosReader::calcRotECI2ECF(CRotationMatrix& mat,int Y,int M,int D,double GPST,double UT1_UTC,double UTC_GPST)
{
	double TT_TAI=32.184;//const //http://edu-observatory.org/gps/time.html
	double GPST_TAI=-19; //const
	double TT_GPST=TT_TAI-GPST_TAI;

	double JD=julianDay(Y,M,D);
	double UT1=GPST+UTC_GPST+UT1_UTC;

	//precession
	double JDf=JD+(GPST+TT_GPST)/86400.0;//JD+(GPST-GPST_TAI)/86400.0;//
	double T=(JDf-2451545.0)/36525.0;
	CRotationMatrix rotPrecession;
	calcPrecession(T,rotPrecession);
	
	//nutation
	CRotationMatrix rotNutation;
	double ep,delpsi;
	calcNutation(T,rotNutation,ep,delpsi);

	//rotGAST
	CRotationMatrix rotTheta;
	calcRotGAST(JD,UT1,ep,delpsi,rotTheta);

	//rotPI
	//xp=0.19792",yp=0.51365" on 22/07/2009, Bulletin B from www.iers.org
	CRotationMatrix rotPI;
	double xp=0.19792;
	double yp=0.51365;
	calcPI(xp,yp,rotPI);
	
	//R_IF=PI * THETA * N * P;
	//because rotPI,nutation,and precession are small and almost constant during the image recording,
	//they can be compensated in the bundle adjustment even if they were not taken into account
	//so is the instrument mounting rotbias
	CRotationMatrix rotTmp;
	rotTheta.R_times_R(rotTmp,rotNutation);
	rotTmp.R_times_R(mat,rotPrecession);

	/*
	rotTmp=mat;
	rotPI.R_times_R(mat,rotTmp);//*/
}

double CTheosReader::julianDay(int Y,int M,int D)
{
	double Df=D;		
	return julianDay(Y,M,Df);
}

double CTheosReader::julianDay(int Y,int M,double D)
{
	if(M<=2)
	{
		Y=Y-1;
		M=M+12;
	}
	int A = Y/100;
	int B = A/4;
	int C = 2-A+B;
	int E = 365.25 * (Y+4716);
	int F = 30.6001 * (M+1);
	double JD= C+D+E+F-1524.5;

	return JD;
}

void CTheosReader::calcPI(double xp,double yp,CRotationMatrix &mat)
{
	//xp" and yp",Broadcast in Bulletin B from www.iers.org
	double secrad=180*3600/M_PI;
	double xpR=xp/secrad;
	double ypR=yp/secrad;
	//PI=m2(-xpR)*m1(-ypR);
	CRotationMatrix rot1(cos(-xpR),0,-sin(-xpR),0,1,0,sin(-xpR),0,cos(-xpR));
	CRotationMatrix rot2(1,0,0,0,cos(-ypR),sin(-ypR),0,-sin(-ypR),cos(-ypR));
	rot1.R_times_R(mat,rot2);
}

void CTheosReader::calcRotGAST(double jd,double UT1,double ep,double delpsi,CRotationMatrix &mat)
{
	/*
	*compute rotation of GAST Greenwich apparent sidereal time
	*careful about units
	*UT1 in time seconds	
	*ep and delpsi in radians
	//*/
	double tsecrad=(24*60*60)/(2*M_PI);
	double c0=24110.54841;
	double c1=8640184.812866;
	double c2=0.093104;
	double c3=-0.0000062;
	double Tu=(jd-2451545.0)/36525.0;
	double GMST_0h_UT1=c0 + c1*Tu + c2*pow(Tu,2) + c3*pow(Tu,3);
	double r=1.002737909350795 + 5.9006e-11*Tu - 5.9e-15*pow(Tu,2);//ratio of universal time to sidereal time
	double GMST=GMST_0h_UT1 + r*UT1; // GMST time seconds	
	double GAST=GMST/tsecrad + delpsi*cos(ep); //convert GMST to radians
	//result=m3(GAST)
	mat(0,0)=cos(GAST); mat(0,1)=sin(GAST);	mat(0,2)=0;
	mat(1,0)=-mat(0,1);	mat(1,1)=mat(0,0);	mat(1,2)=0;
	mat(2,0)=0;			mat(2,1)=0;			mat(2,2)=1;
}

void CTheosReader::calcPrecession(double T,CRotationMatrix &rotP)
{	
	//T is TT time
	double secrad=180*3600/M_PI;
	double zeta=(2306.2181*T + 0.30188*pow(T,2) + 0.017998*pow(T,3))/secrad;
	double z=(2306.2181*T + 1.09468*pow(T,2) + 0.018203*pow(T,3))/secrad;
	double theta=(2004.3109*T - 0.42665*pow(T,2) - 0.041833*pow(T,3))/secrad;

	//result=m3(-z)*m2(theta)*m3(-zeta);
	CRotationMatrix rotzeta(cos(-zeta),sin(-zeta),0,-sin(-zeta),cos(-zeta),0,0,0,1);
	CRotationMatrix rotz(cos(-z),sin(-z),0,-sin(-z),cos(-z),0,0,0,1);
	CRotationMatrix rottheta(cos(theta),0,-sin(theta),0,1,0,sin(theta),0,cos(theta));		
	CRotationMatrix rotTmp;
	rotz.R_times_R(rotTmp,rottheta);
	rotTmp.R_times_R(rotP,rotzeta);
}

void CTheosReader::calcNutation(double T,CRotationMatrix &rotN, double &ep,double &delpsi)
{
	//T is TT time
	CCharString strNutation="0,  0,  0,  0,  1, -171996, -1742, 92025,  89,   1,\
							0,  0,  0,  0,  2,    2062,     2,  -895,   5,   2,\
							-2,  0,  2,  0,  1,      46,     0,   -24,   0,   3,\
							2,  0, -2,  0,  0,      11,     0,     0,   0,   4,\
							-2,  0,  2,  0,  2,      -3,     0,     1,   0,   5,\
							1, -1,  0, -1,  0,      -3,     0,     0,   0,   6,\
							0, -2,  2, -2,  1,      -2,     0,     1,   0,   7,\
							2,  0, -2,  0,  1,       1,     0,     0,   0,   8,\
							0,  0,  2, -2,  2,  -13187,   -16,  5736, -31,   9,\
							0,  1,  0,  0,  0,    1426,   -34,    54,  -1,  10,\
							0,  1,  2, -2,  2,    -517,    12,   224,  -6,  11,\
							0, -1,  2, -2,  2,     217,    -5,   -95,   3,  12,\
							0,  0,  2, -2,  1,     129,     1,   -70,   0,  13,\
							2,  0,  0, -2,  0,      48,     0,     1,   0,  14,\
							0,  0,  2, -2,  0,     -22,     0,     0,   0,  15,\
							0,  2,  0,  0,  0,      17,    -1,     0,   0,  16,\
							0,  1,  0,  0,  1,     -15,     0,     9,   0,  17,\
							0,  2,  2, -2,  2,     -16,     1,     7,   0,  18,\
							0, -1,  0,  0,  1,     -12,     0,     6,   0,  19,\
							-2,  0,  0,  2,  1,      -6,     0,     3,   0,  20,\
							0, -1,  2, -2,  1,      -5,     0,     3,   0,  21,\
							2,  0,  0,  2,  1,       4,     0,    -2,   0,  22,\
							0,  1,  2, -2,  1,       4,     0,    -2,   0,  23,\
							1,  0,  0, -1,  0,      -4,     0,     0,   0,  24,\
							2,  1,  0, -2,  0,       1,     0,     0,   0,  25,\
							0,  0, -2,  2,  1,       1,     0,     0,   0,  26,\
							0,  1, -2,  2,  0,      -1,     0,     0,   0,  27,\
							0,  1,  0,  0,  2,       1,     0,     0,   0,  28,\
							-1,  0,  0,  1,  1,       1,     0,     0,   0,  29,\
							0,  1,  2, -2,  0,      -1,     0,     0,   0,  30,\
							0,  0,  2,  0,  2,   -2274,    -2,   977,  -5,  31,\
							1,  0,  0,  0,  0,     712,     1,    -7,   0,  32,\
							0,  0,  2,  0,  1,    -386,    -4,   200,   0,  33,\
							1,  0,  2,  0,  2,    -301,     0,   129,  -1,  34,\
							1,  0,  0, -2,  0,    -158,     0,    -1,   0,  35,\
							-1,  0,  2,  0,  2,     123,     0,   -53,   0,  36,\
							0,  0,  0,  2,  0,      63,     0,    -2,   0,  37,\
							1,  0,  0,  0,  1,      63,     1,   -33,   0,  38,\
							-1,  0,  0,  0,  1,     -58,    -1,    32,   0,  39,\
							-1,  0,  2,  2,  2,     -59,     0,    26,   0,  40,\
							1,  0,  2,  0,  1,     -51,     0,    27,   0,  41,\
							0,  0,  2,  2,  2,     -38,     0,    16,   0,  42,\
							2,  0,  0,  0,  0,      29,     0,    -1,   0,  43,\
							1,  0,  2, -2,  2,      29,     0,   -12,   0,  44,\
							2,  0,  2,  0,  2,     -31,     0,    13,   0,  45,\
							0,  0,  2,  0,  0,      26,     0,    -1,   0,  46,\
							-1,  0,  2,  0,  1,      21,     0,   -10,   0,  47,\
							-1,  0,  0,  2,  1,      16,     0,    -8,   0,  48,\
							1,  0,  0, -2,  1,     -13,     0,     7,   0,  49,\
							-1,  0,  2,  2,  1,     -10,     0,     5,   0,  50,\
							1,  1,  0, -2,  0,      -7,     0,     0,   0,  51,\
							0,  1,  2,  0,  2,       7,     0,    -3,   0,  52,\
							0, -1,  2,  0,  2,      -7,     0,     3,   0,  53,\
							1,  0,  2,  2,  2,      -8,     0,     3,   0,  54,\
							1,  0,  0,  2,  0,       6,     0,     0,   0,  55,\
							2,  0,  2, -2,  2,       6,     0,    -3,   0,  56,\
							0,  0,  0,  2,  1,      -6,     0,     3,   0,  57,\
							0,  0,  2,  2,  1,      -7,     0,     3,   0,  58,\
							1,  0,  2, -2,  1,       6,     0,    -3,   0,  59,\
							0,  0,  0, -2,  1,      -5,     0,     3,   0,  60,\
							1, -1,  0,  0,  0,       5,     0,     0,   0,  61,\
							2,  0,  2,  0,  1,      -5,     0,     3,   0,  62,\
							0,  1,  0, -2,  0,      -4,     0,     0,   0,  63,\
							1,  0, -2,  0,  0,       4,     0,     0,   0,  64,\
							0,  0,  0,  1,  0,      -4,     0,     0,   0,  65,\
							1,  1,  0,  0,  0,      -3,     0,     0,   0,  66,\
							1,  0,  2,  0,  0,       3,     0,     0,   0,  67,\
							1, -1,  2,  0,  2,      -3,     0,     1,   0,  68,\
							-1, -1,  2,  2,  2,      -3,     0,     1,   0,  69,\
							-2,  0,  0,  0,  1,      -2,     0,     1,   0,  70,\
							3,  0,  2,  0,  2,      -3,     0,     1,   0,  71,\
							0, -1,  2,  2,  2,      -3,     0,     1,   0,  72,\
							1,  1,  2,  0,  2,       2,     0,    -1,   0,  73,\
							-1,  0,  2, -2,  1,      -2,     0,     1,   0,  74,\
							2,  0,  0,  0,  1,       2,     0,    -1,   0,  75,\
							1,  0,  0,  0,  2,      -2,     0,     1,   0,  76,\
							3,  0,  0,  0,  0,       2,     0,     0,   0,  77,\
							0,  0,  2,  1,  2,       2,     0,    -1,   0,  78,\
							-1,  0,  0,  0,  2,       1,     0,    -1,   0,  79,\
							1,  0,  0, -4,  0,      -1,     0,     0,   0,  80,\
							-2,  0,  2,  2,  2,       1,     0,    -1,   0,  81,\
							-1,  0,  2,  4,  2,      -2,     0,     1,   0,  82,\
							2,  0,  0, -4,  0,      -1,     0,     0,   0,  83,\
							1,  1,  2,  2,  2,       1,     0,    -1,   0,  84,\
							1,  0,  2,  2,  1,      -1,     0,     1,   0,  85,\
							-2,  0,  2,  4,  2,      -1,     0,     1,   0,  86,\
							-1,  0,  4,  0,  2,       1,     0,     0,   0,  87,\
							1, -1,  0, -2,  0,       1,     0,     0,   0,  88,\
							2,  0,  2, -2,  1,       1,     0,    -1,   0,  89,\
							2,  0,  2,  2,  2,      -1,     0,     0,   0,  90,\
							1,  0,  0,  2,  1,      -1,     0,     0,   0,  91,\
							0,  0,  4, -2,  2,       1,     0,     0,   0,  92,\
							3,  0,  2, -2,  2,       1,     0,     0,   0,  93,\
							1,  0,  2, -2,  0,      -1,     0,     0,   0,  94,\
							0,  1,  2,  0,  1,       1,     0,     0,   0,  95,\
							-1, -1,  0,  2,  1,       1,     0,     0,   0,  96,\
							0,  0, -2,  0,  1,      -1,     0,     0,   0,  97,\
							0,  0,  2, -1,  2,      -1,     0,     0,   0,  98,\
							0,  1,  0,  2,  0,      -1,     0,     0,   0,  99,\
							1,  0, -2, -2,  0,      -1,     0,     0,   0, 100,\
							0, -1,  2,  0,  1,      -1,     0,     0,   0, 101,\
							1,  1,  0, -2,  1,      -1,     0,     0,   0, 102,\
							1,  0, -2,  2,  0,      -1,     0,     0,   0, 103,\
							2,  0,  0,  2,  0,       1,     0,     0,   0, 104,\
							0,  0,  2,  4,  2,      -1,     0,     0,   0, 105,\
							0,  1,  0,  1,  0,       1,     0,     0,   0, 106,,";
//////////////////////////////////////////////////////////////////////////
	int n=106,idx,count=0;
	double pl[106],plp[106],pF[106],pD[106],pOm[106],dP0[106],dP1[106],de0[106],de1[106];
	for(int i=0;i<n;i++)
	{
		int len=strNutation.GetLength();
		idx=strNutation.Find(",");
		pl[count]=atof(strNutation.Left(idx));
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);


		idx=strNutation.Find(",");
		plp[count]=atof(strNutation.Left(idx));		
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);

		idx=strNutation.Find(",");
		pF[count]=atof(strNutation.Left(idx));
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);

		idx=strNutation.Find(",");
		pD[count]=atof(strNutation.Left(idx));
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);

		idx=strNutation.Find(",");
		pOm[count]=atof(strNutation.Left(idx));
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);

		idx=strNutation.Find(",");
		dP0[count]=atof(strNutation.Left(idx));
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);

		idx=strNutation.Find(",");
		dP1[count]=atof(strNutation.Left(idx));
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);

		idx=strNutation.Find(",");
		de0[count]=atof(strNutation.Left(idx));
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);

		idx=strNutation.Find(",");
		de1[count]=atof(strNutation.Left(idx));
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);

		idx=strNutation.Find(",");
		strNutation=strNutation.Mid(idx+1,strNutation.GetLength()-1);

		count++;
	}
	for(int i=0;i<n;i++)
	{
		dP1[i]=dP1[i]/10;
		de1[i]=de1[i]/10;
	}		
	double degrad=180/M_PI;
	double secrad=degrad*3600;
	//l,lp,F,D,Om all in radians
	double c0=(134+57.0/60+46.733/3600)/degrad;
	double c1=(477198+52.0/60+2.633/3600)/degrad;
	double c2=(31.310/3600)/degrad;
	double c3=(0.064/3600)/degrad;
	double l=c0 + c1*T + c2*pow(T,2) + c3*pow(T,3);
	c0=(357+31.0/60+39.804/3600)/degrad;
	c1=(35999+3.0/60+1.224/3600)/degrad;
	c2=(-0.577/3600)/degrad;
	c3=(-0.012/3600)/degrad;
	double lp=c0 + c1*T + c2*pow(T,2) + c3*pow(T,3);
	c0=(93+16.0/60+18.877/3600)/degrad;
	c1=(483202+1.0/60+3.137/3600)/degrad;
	c2=(-13.257/3600)/degrad;
	c3=(0.011/3600)/degrad;
	double F=c0 + c1*T + c2*pow(T,2) + c3*pow(T,3);
	c0=(297+51.0/60+1.307/3600)/degrad;
	c1=(445267+6.0/60+41.328/3600)/degrad;
	c2=(-6.891/3600)/degrad;
	c3=(0.019/3600)/degrad;
	double D=c0 + c1*T + c2*pow(T,2) + c3*pow(T,3);
	c0=(125+2.0/60+40.280/3600)/degrad;
	c1=(-(1934+8.0/60+10.539/3600))/degrad;
	c2=(7.455/3600)/degrad;
	c3=(0.008/3600)/degrad;
	double Om=c0 + c1*T + c2*pow(T,2) + c3*pow(T,3);
	delpsi=0;
	double deleps=0,phi,dP,de;
	for(int i=0;i<n;i++)
	{
		phi=pl[i]*l + plp[i]*lp + pF[i]*F + pD[i]*D + pOm[i]*Om;// phi in radians	
		dP=dP0[i] + dP1[i]*T;
		delpsi=delpsi + dP*sin(phi);
		de=de0[i] + de1[i]*T;
		deleps=deleps + de*cos(phi);
	}
	// put delpsi and deleps into radians
	delpsi=delpsi/10000;
	delpsi=delpsi/secrad;
	deleps=deleps/10000;
	deleps=deleps/secrad;
	c0=23.43929111/degrad;
	c1=(-46.8150/3600)/degrad;
	c2=(-0.00059/3600)/degrad;
	c3=(0.001813/3600)/degrad;
	ep=c0 + c1*T + c2*pow(T,2) + c3*pow(T,3); //ep in radians

	//N=m1(-ep-deleps)*m3(-delpsi)*m1(ep);
	CRotationMatrix rot1(1,0,0,0,cos(-ep-deleps),sin(-ep-deleps),0,-sin(-ep-deleps),cos(-ep-deleps));
	CRotationMatrix rot2(cos(-delpsi),sin(-delpsi),0,-sin(-delpsi),cos(-delpsi),0,0,0,1);
	CRotationMatrix rot3(1,0,0,0,cos(ep),sin(ep),0,-sin(ep),cos(ep));	
	CRotationMatrix rotTmp;
	rot1.R_times_R(rotTmp,rot2);
	rotTmp.R_times_R(rotN,rot3);	
}
