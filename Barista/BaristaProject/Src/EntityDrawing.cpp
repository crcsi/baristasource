#include "EntityDrawing.h"
#include "XYPolyLine.h"
#include "XYZPolyLine.h"

int BARISTA_RED[3]   = {255,0,0};
int BARISTA_GREEN[3] = {0,255,0};
int BARISTA_BLUE[3]  = {0,0,255};

int BARISTA_MAGENTA[3] = {255,0,255};
int BARISTA_YELLOW[3]  = {255,255, 0};
int BARISTA_CYAN[3]    = {0,255,255};

int BARISTA_WHITE[3]    = {255,255,255};
int BARISTA_BLACK[3]    = {0,0,0};

CEntityDrawing::CEntityDrawing(void) 
{
}

CEntityDrawing::~CEntityDrawing(void)
{
	this->removeAllDataSets();
}

bool CEntityDrawing::addDataSet(const CLabel& dataSetName,int dim,bool showPoints,bool showPointLabels)
{
	int pos;
	
	// do we have a set with the same name already?
	if (this->findPos(dataSetName,pos))
		return false;

	// add at the right pos, so the array is sorted by label
	if (pos <= this->datasets.size())
	{
		CDrawingDataSet* newDataSet = new CDrawingDataSet(dim);
		newDataSet->setName(dataSetName);
		newDataSet->setShowPoints(showPoints);
		newDataSet->setShowPointLabels(showPointLabels);
		int pointColor[3] = {0,255,0};
		newDataSet->setPointColor(pointColor);

		this->datasets.insert(this->datasets.begin() + pos,newDataSet);
		return true;
	}
	else
		return false;

}
bool CEntityDrawing::addDataSet(const CLabel& dataSetName,int dim,bool showPoints,bool showPointLabels, int *pointColor)
{
	int pos;
	
	// do we have a set with the same name already?
	if (this->findPos(dataSetName,pos))
		return false;

	// add at the right pos, so the array is sorted by label
	if (pos <= this->datasets.size())
	{
		CDrawingDataSet* newDataSet = new CDrawingDataSet(dim);
		newDataSet->setName(dataSetName);
		newDataSet->setShowPoints(showPoints);
		newDataSet->setShowPointLabels(showPointLabels);
		newDataSet->setPointColor(pointColor);

		this->datasets.insert(this->datasets.begin() + pos,newDataSet);
		return true;
	}
	else
		return false;

}

bool CEntityDrawing::removeDataSet(const CLabel& dataSetName)
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return false;  // not found

	delete this->datasets[pos];
	this->datasets.erase(this->datasets.begin()+ pos);
	
	return true;
}


bool CEntityDrawing::addDataSet(CDrawingDataSet& dataSet)
{
	int pos;

	// do we have a set with the same name already?
	if (this->findPos(dataSet.getName(),pos))
		return false;

	// add at the right pos, so the array is sorted by label
	if (pos <= this->datasets.size())
	{
		CDrawingDataSet* newDataSet = new CDrawingDataSet(dataSet);
		this->datasets.insert(this->datasets.begin() + pos,newDataSet);
		return true;
	}
	else
		return false;
}


void CEntityDrawing::removeAllDataSets()
{
	for (int i=0; i< this->datasets.size(); i++)
	{
		delete this->datasets[i];
	}

	this->datasets.clear();
}


bool CEntityDrawing::findPos(const CLabel& dataSetName,int &pos)
{

  int low = 0, high = this->datasets.size() - 1;

  while ( high >= low ) 
  {
    pos = ( low + high ) / 2;
	int test = strcmp(dataSetName.GetChar(),this->datasets[pos]->getName().GetChar());
    if ( test < 0)
      high = pos - 1;
    else if ( test > 0)
      low = pos + 1;
    else 
      return true;

  }
	pos = low;
	return	false;
}


bool CEntityDrawing::addPoint(const CLabel& dataSetName,CObsPoint* point)
{
	if (point !=NULL)
	{
		int pos;
		
		// do we have a dataset with this name
		if (!this->findPos(dataSetName,pos)) // no, then create it
		{
			this->addDataSet(dataSetName,point->getDimension(), true, true);
				// we should find it now
			if (!this->findPos(dataSetName,pos))
				return false;
		}

		// add the point
		this->datasets[pos]->addPoint(point);
		return true;
	}

	return false;
}

bool CEntityDrawing::removePoint(const CLabel& dataSetName,const CLabel& point)
{
	if (point !=NULL)
	{
		int pos;
		// do we have a dataset with this name
		if (!this->findPos(dataSetName,pos))
			return false; // no

		this->datasets[pos]->removePoint(point);

		return true;
	}	
	return false;
}


bool CEntityDrawing::queryPoint(const CLabel& dataSetName,const CLabel & pointLabel)
{
	int pos;
	// do we have a dataset with this name
	if (!this->findPos(dataSetName,pos))
		return false; // no

	return this->datasets[pos]->queryPoint(pointLabel);
}

CDrawingDataSet* CEntityDrawing::getDataSet(int index)
{
	if (index < 0 || index >= this->datasets.size())
		return NULL;

	return this->datasets[index];

}

bool CEntityDrawing::addPolyLine(const CLabel& dataSetName,CXYPolyLine* polyline,const int *color,unsigned int lineWidth)
{

	if (!polyline || polyline->getPointCount() < 2)
		return false;
	
	int pos;
	
	// do we have a dataSet with the same name ?
	if (!this->findPos(dataSetName,pos))// no, then create it
	{
		this->addDataSet(dataSetName,2, true, true);
		// we should find it now
		if (!this->findPos(dataSetName,pos))
			return false;
	}


	// add the points step by step and create topology
	CObsPoint* p1 = this->datasets[pos]->addPoint(&polyline->getPoint(0));
	for (int i=1; i < polyline->getPointCount(); i++)
	{
		CObsPoint* p2 = this->datasets[pos]->addPoint(&polyline->getPoint(i));
		if (p1 && p2)
			{
				CLabel p1Label(p1->getLabel());
				CLabel p2Label(p2->getLabel());
				this->datasets[pos]->addTopology(p1Label,p2Label,color,lineWidth);
			}
		p1 = p2;
	}

	return true;

}


bool CEntityDrawing::removePolyLine(const CLabel& dataSetName,CXYZPolyLine* polyline)
{
	if (!polyline)
		return false;

	int pos;
	
	// do we have a dataSet with the same name ?
	if (!this->findPos(dataSetName,pos))
		return false;

	for (int i=0; i < polyline->getPointCount(); i++)
		{
			CLabel label (polyline->getPoint(i)->getLabel());
			this->datasets[pos]->removePoint(label);
		}

	return true;
}


bool CEntityDrawing::addTopology(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2, int *color,unsigned int lineWidth)
{
	int pos;
	
	// do we have a dataSet with the same name ?
	if (!this->findPos(dataSetName,pos))
		return false;
	
	return this->datasets[pos]->addTopology(p1,p2,color,lineWidth);

}

bool CEntityDrawing::removeTopology(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2)
{
	int pos;
	
	// do we have a dataSet with the same name ?
	if (!this->findPos(dataSetName,pos))
		return false;

	return this->datasets[pos]->removeTopology(p1,p2);
}



bool CEntityDrawing::getShowPoints(const CLabel& dataSetName) 
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return false;

	return this->datasets[pos]->getShowPoints();
};

void CEntityDrawing::setShowPoints(const CLabel& dataSetName,bool b) 
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return;

	this->datasets[pos]->setShowPoints(b);
};

bool CEntityDrawing::getShowPointLabels(const CLabel& dataSetName) 
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return false;

	return this->datasets[pos]->getShowPointLabels();
};

void CEntityDrawing::setShowPointLabels(const CLabel& dataSetName,bool b) 
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return;

	this->datasets[pos]->setShowPointLabels(b);
};

int *CEntityDrawing::getPointColor(const CLabel& dataSetName)
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return BARISTA_GREEN;

	return this->datasets[pos]->getPointColor();
};

void CEntityDrawing::setPointColor(const CLabel& dataSetName, int *color)
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return;

	this->datasets[pos]->setPointColor(color);
};

	// change color
void CEntityDrawing::changeTopologyColor(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2, int *color)
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return;

	this->datasets[pos]->changeTopologyColor(p1,p2,color);
}

void CEntityDrawing::changeAllTopologyColors(const CLabel& dataSetName, int *color)
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return;

	this->datasets[pos]->changeAllTopologyColors(color);
}


	// changeline width
void CEntityDrawing::changeTopologyLineWidth(const CLabel& dataSetName,const CLabel& p1,const CLabel& p2,unsigned int lineWidth)
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return;

	this->datasets[pos]->changeTopologyLineWidth(p1,p2,lineWidth);
}

void CEntityDrawing::changeAllTopologyLineWidth(const CLabel& dataSetName,unsigned int lineWidth)
{
	int pos;
	if (!this->findPos(dataSetName,pos))
		return;

	this->datasets[pos]->changeAllTopologyLineWidth(lineWidth);
}


bool CEntityDrawing::renameDataSet(const CLabel& oldName,const CLabel& newName)
{
	// find the dataSet with the old name
	int pos;
	if (!this->findPos(oldName,pos))
		return false;

	// copy the old one
	CDrawingDataSet tmp(*this->datasets[pos]);

	// rename the copy
	tmp.setName(newName);

	this->removeDataSet(this->datasets[pos]->getName());
	
	// add the copy with new name so we keep the array sorted
	return this->addDataSet(tmp);


}
