
#include "BandMerger.h"
#include "ImgBuf.h"
#include "PushbroomScanner.h"
#include "CCDLine.h"
#include "BaristaProject.h"
#include "AlosReader.h"
#include "WallisFilter.h"
#include "float.h"


CBandMerger::CBandMerger() : newImage(0),newImageWidth(0),
	newImageHeight(0),minYValue(0),maxYValue(0),newImageBpp(0),
	newImageBands(0),marginInterpolation(0),startColOfRefImage(0),
	method(eBiLinear),initWallisFilter(false)
{

}

CBandMerger::~CBandMerger(void)
{

}



bool CBandMerger::init(const vector<CVImage*>& inputImages,
					   unsigned int startPosRefImage,
					    CCharString newCameraName,
						CBandMerger::InterpolationMethod method,
						CVImage* newMergedImage)
{
	this->inputImages.clear();

	this->inputImages = inputImages;
	
	project.getUniqueCameraName(newCameraName);
	this->newCameraName = newCameraName;
	this->method = method;
	this->newImage = newMergedImage;

	// we expect that the first valid col has been computed for the reference channel (red)
	// we know that the other channels are shifted to the right so we would get the first col only red
	// --> just move the start of the image by one more pixel
	this->startColOfRefImage = startPosRefImage + 1;

	if (this->inputImages.size() < 3)
	{
		this->errorMessage = "Less then 3 images! No merging possible!";
		return false;
	}



	if (!this->newImage)
	{
		this->errorMessage = "No image selected.";
		return false;
	}

	this->newFileName = this->newImage->getFileNameChar();

	if (this->newFileName.IsEmpty())
	{
		this->errorMessage = "Filename for merged image is empty.";
		return false;
	}


	this->newHugeFileName = this->newFileName + ".hug";

	if (this->method == eBiLinear)
		this->marginInterpolation = 2;

	return true;
}

bool CBandMerger::mergeBands()
{

	// first create a new image in the project
	bool ret = this->initNewImage();

	if (ret)
		ret = this->computeCCDPos();

	if (ret)
		ret = this->resampleImages();

	if (ret && this->initWallisFilter)
	{
		CHugeImage* himage = this->newImage->getHugeImage();
		himage->setProgressListener(this->listener);
		double defaultMean = himage->getBpp() == 8 ? DEFAULT_MEAN_8BIT : DEFAULT_MEAN_16BIT;
		double defaultStdDev = himage->getBpp() == 8 ? DEFAULT_STDDEV_8BIT : DEFAULT_STDDEV_16BIT;
		CWallisFilter* wf = himage->createWallisFilter(DEFAULT_WINDOWSIZE,DEFAULT_WINDOWSIZE,DEFAULT_CONTRAST,DEFAULT_BRIGHTNESS,defaultMean,defaultStdDev,true);
		if (wf)
		{
			ret = true;
			// we have saved the parameters on disk, so it's quick to change the transfer function now
			delete wf; 
		}
		else
		{
			ret = false;		
			this->errorMessage = "Could not initialise wallis filter!";
		}
		himage->removeProgressListener();
	}


	return ret;
}



bool CBandMerger::initNewImage()
{
	
	CPushBroomScanner* currentScanner = this->inputImages[0]->getPushbroom();

	this->newImageBands = int(this->inputImages.size());
	this->newImageBpp = this->inputImages[0]->getHugeImage()->getBpp();


	C3DPoint newIRP = this->inputImages[0]->getPushbroom()->getCamera()->getIRP();
	newIRP.x -= this->startColOfRefImage;

	CDistortion* d = this->inputImages[0]->getPushbroom()->getCamera()->getDistortion();
	double p1_new(-1000),p2_new(-1000),p3_new(-1000),p4_new(-1000),p5_new(-1000),p6_new(-1000);
	if (d)
	{
		p1_new = d->getParameter(PARAMETER_1);
		p2_new = d->getParameter(PARAMETER_2);
		p3_new = d->getParameter(PARAMETER_3);
		p4_new = d->getParameter(PARAMETER_4);
		p5_new = d->getParameter(PARAMETER_5);
		p6_new = d->getParameter(PARAMETER_6);
	}


	// check first if we got a camera with the same irp, if not create a new one 
	CCCDLine* newCamera = NULL;
	for (int i=0; i < project.getNumberOfCameras(); i++)
	{
		CCamera* c = project.getCameraAt(i);

		// we know the all cameras of merged images have no distortion
		if (c &&  c->getCameraType() == eCCD  && c->getDistortion() && c->getDistortion()->getDistortionType() == eDistortionALOS)
		{
			C3DPoint tmpIRP(c->getIRP());

			double p1_tmp = c->getDistortion()->getParameter(PARAMETER_1);
			double p2_tmp = c->getDistortion()->getParameter(PARAMETER_2);
			double p3_tmp = c->getDistortion()->getParameter(PARAMETER_3);
			double p4_tmp = c->getDistortion()->getParameter(PARAMETER_4);
			double p5_tmp = c->getDistortion()->getParameter(PARAMETER_5);
			double p6_tmp = c->getDistortion()->getParameter(PARAMETER_6);

			if (fabs(tmpIRP.x -newIRP.x) < FLT_EPSILON && 
				fabs(tmpIRP.y -newIRP.y) < FLT_EPSILON &&
				fabs(tmpIRP.z -newIRP.z) < FLT_EPSILON &&
				fabs(p1_tmp-p1_new )     < FLT_EPSILON &&
				fabs(p2_tmp-p2_new )     < FLT_EPSILON &&
				fabs(p3_tmp-p3_new )     < FLT_EPSILON &&
				fabs(p4_tmp-p4_new )     < FLT_EPSILON &&
				fabs(p5_tmp-p5_new )     < FLT_EPSILON &&
				fabs(p6_tmp-p6_new )     < FLT_EPSILON)
			{
				newCamera = (CCCDLine*) c;
				break;
			}
		}
	}

	if (!newCamera)
	{	// create a new camera
		newCamera = (CCCDLine*)project.addCamera(this->inputImages[0]->getPushbroom()->getCamera());
		newCamera->setIRP(newIRP);
		newCamera->setName(this->newCameraName);
	}

	// init the pushbroom scanner with the new camera
	CPushBroomScanner* newScanner = this->newImage->getPushbroom();
	bool ret = newScanner->init(currentScanner->getOrbitPath(),
						 currentScanner->getOrbitAttitudes(),
						 currentScanner->getCameraMounting(),
						 newCamera,
						 "Merged PushBroomscanner",
						 currentScanner->getSatelliteHeight(),
						 currentScanner->getGroundHeight(),
						 currentScanner->getGroundTemperature(),
						 currentScanner->getFirstLineTime(),
						 currentScanner->getTimePerLine());

	
	if (!ret)
	{
		this->errorMessage = "Failed to initialize pushbroom scanner.";
		return false;
	}
	else this->newImage->setCurrentSensorModel(ePUSHBROOM);

	this->newImageWidth = this->inputImages[0]->getHugeImage()->getWidth() - this->startColOfRefImage;
	this->newImageHeight = this->inputImages[0]->getHugeImage()->getHeight() - 1;

	return true;
}


bool CBandMerger::computeCCDPos()
{
	
	this->ccdPos.clear();

	CCamera* referenceCamera = this->inputImages[0]->getPushbroom()->getCamera();
	C3DPoint xf(0,0,0),xc(0,0,0);
	for (unsigned int band = 0; band < this->inputImages.size(); band++)
	{
		vector<C2DPoint> v;
		this->ccdPos.push_back(v);
		unsigned int originalWidth = this->newImageWidth + this->startColOfRefImage;
		this->ccdPos[band].resize(originalWidth);
		
		CCamera* c = this->inputImages[band]->getPushbroom()->getCamera();

		for (unsigned int pos=0; pos < originalWidth ; pos++)
		{
			xf.x = pos;
			referenceCamera->transformObs2Obj(xc,xf);
			c->transformObj2Obs(this->ccdPos[band][pos],xc);
			this->ccdPos[band][pos].x -= pos;

		}


	}
	
	return true;
}




bool CBandMerger::resampleImages()
{

	CHugeImage* hImage = this->newImage->getHugeImage();

	bool ret = hImage->open(this->newHugeFileName.GetChar(),true);

	if (!ret)
	{
		ret = hImage->prepareWriting(this->newHugeFileName.GetChar(),this->newImageWidth,this->newImageHeight,this->newImageBpp,this->newImageBands);

		if (ret)
		{
			int tileWidth = hImage->getTileWidth();
			int tileHeight = hImage->getTileHeight();
			int nTiles = hImage->tilesAcross * hImage->tilesDown;
			unsigned long indexCol;
			int indexFirstCol,indexLastCol;
			int indexFirstRow,indexLastRow;
			int indexSrc;
			unsigned int indexDest;
			unsigned long rowOffset;
			unsigned int originalWidth = this->newImageWidth + this->startColOfRefImage;
			int rowImage,colImage;
			C2DPoint srcImagePos;

			CImageBuffer destBuf(0,0,tileWidth,tileHeight,hImage->getBands(),hImage->getBpp(),true);
			vector<CImageBuffer*> srcBuf(this->newImageBands);

			for (rowImage = 0;  rowImage < this->newImageHeight; rowImage+=tileHeight)
			{
				unsigned long maxHeight = (unsigned long)destBuf.getHeight();
				if (rowImage + maxHeight > this->newImageHeight) 
					maxHeight = this->newImageHeight - rowImage;

				unsigned int colOffset=0;

				for (colImage = this->startColOfRefImage; colImage < originalWidth; colImage+= tileWidth)
				{
					
					unsigned long maxWidth = (unsigned long)destBuf.getWidth();
					if (colImage + maxWidth > originalWidth) 
						maxWidth = originalWidth - colImage;	
					
					indexFirstRow = rowImage  - this->marginInterpolation;
					indexFirstCol = colImage  - this->marginInterpolation;

					indexLastRow = indexFirstRow + maxHeight + 2*this->marginInterpolation;
					indexLastCol = indexFirstCol + maxWidth + 2*this->marginInterpolation;

					CImageBuffer srcBufR(indexFirstCol,indexFirstRow,indexLastCol-indexFirstCol,indexLastRow -indexFirstRow,1,this->newImageBpp,true);
					this->inputImages[0]->getHugeImage()->getRect(srcBufR);
					srcBuf[0]=&srcBufR;


					CImageBuffer srcBufG(indexFirstCol,indexFirstRow,indexLastCol-indexFirstCol,indexLastRow -indexFirstRow,1,this->newImageBpp,true);
					this->inputImages[1]->getHugeImage()->getRect(srcBufG);
					srcBuf[1]=&srcBufG;


					CImageBuffer srcBufB(indexFirstCol,indexFirstRow,indexLastCol-indexFirstCol,indexLastRow -indexFirstRow,1,this->newImageBpp,true);
					this->inputImages[2]->getHugeImage()->getRect(srcBufB);
					srcBuf[2]=&srcBufB;


					CImageBuffer srcBufA(indexFirstCol,indexFirstRow,indexLastCol-indexFirstCol,indexLastRow -indexFirstRow,1,this->newImageBpp,true);
					if (this->inputImages.size() == 4)
					{
						this->inputImages[3]->getHugeImage()->getRect(srcBufA);
						srcBuf[3]=&srcBufA;
					}


					if (this->method == CBandMerger::eBiLinear)
					{
						for (int rowTile = 0; rowTile < maxHeight; rowTile++)
						{
							unsigned long rowOffsetDest = rowTile * tileWidth * this->newImageBands;
							unsigned long rowOffsetSrc = rowTile * maxWidth;

							for (int colTile = 0; colTile < maxWidth; colTile++)
							{
								indexDest = rowOffsetDest + colTile*this->newImageBands;
								indexCol = colImage + colTile;

								for (unsigned int band = 0; band < this->ccdPos.size(); band++)
								{
										srcImagePos.y = this->ccdPos[band][indexCol].y + rowImage + rowTile;
										srcImagePos.x = this->ccdPos[band][indexCol].x + colImage + colTile;

										srcBuf[band]->getInterpolatedColourVecBilin(srcImagePos,destBuf.getPixUC() +indexDest + band);					
								}

							}
						}

						// dump the full tile
						hImage->dumpTile(destBuf);
					}
				}
			
				if (this->listener)
				{
					this->listener->setProgressValue(100.0 * float(rowImage) / float(this->newImageHeight));
				}
			}

			ret = hImage->finishWriting();
		}
	}

	return ret;
}



void CBandMerger::findImageGroups(vector<vector<CVImage*> >& newImages,vector<CCharString>& names)
{
	newImages.clear();
	names.clear();
	
	// first we check how many AVNIR scenes we got, we use the shared cameraMounting name as indicator
	for (int i=0; i < project.getNumberOfImages(); i++)
	{
		CVImage* vImage = project.getImageAt(i);
		
		if (vImage && vImage->hasPushbroom() && vImage->getHugeImage() && vImage->getHugeImage()->getBands() == 1)
		{
			CCameraMounting* cm = vImage->getPushbroom()->getCameraMounting();
			CCamera* c = vImage->getPushbroom()->getCamera();
			
			// check if this image could be from ALOS AVNIR
			if (cm &&c && c->getDistortion() && c->getDistortion()->getDistortionType() == eDistortionALOS && CAlosReader::isALOS_AVNIR2Camera(c->getName()))
			{
				
				bool hasSceneAlready = false;
				unsigned int indexImageGroup;
				for ( indexImageGroup=0; indexImageGroup < names.size(); indexImageGroup++)
				{
					if (names[indexImageGroup].CompareNoCase(cm->getName()))
					{
						hasSceneAlready = true;
						break;
					}
				}

				// create a new group and fill with first image
				if (!hasSceneAlready)
				{
					vector<CVImage*> v(1,vImage);
					newImages.push_back(v);
					names.push_back(cm->getName());

				}
				else
					newImages[indexImageGroup].push_back(vImage);

			}
		}
	}


	// JAXA provides images in BGRA order, we want RGBA so we have to take care of the order
	for (unsigned int group=0; group < newImages.size();group++)
	{
		CBandMerger::sortImages(newImages[group]);
	}


}


void CBandMerger::sortImages(vector<CVImage*>& inputImages)
{

	if (inputImages.size() >= 3)
	{	
		CVImage* R=NULL;
		CVImage* G=NULL;
		CVImage* B=NULL;
		CVImage* A=NULL;

		for (unsigned int image=0; image < inputImages.size(); image++)
		{
			if (inputImages[image]->getHugeImage()->getFileName()->Find("IMG-01-ALAV2") >= 0)
				B = inputImages[image];
			else if (inputImages[image]->getHugeImage()->getFileName()->Find("IMG-02-ALAV2") >= 0)
				G = inputImages[image];
			else if (inputImages[image]->getHugeImage()->getFileName()->Find("IMG-03-ALAV2") >= 0)
				R = inputImages[image];
			else if (inputImages[image]->getHugeImage()->getFileName()->Find("IMG-04-ALAV2") >= 0)
				A = inputImages[image];
		}

		if (R && G && B)
		{
			inputImages[0] = R;
			inputImages[1] = G;
			inputImages[2] = B;

			if (inputImages.size() == 4 && A)
				inputImages[3] = A;
		}
		else
		{
			// we can't detect the order, so we leave the images as they are, the user has to take care now
		}
	}

}

int CBandMerger::findBoundary(CHugeImage* himage)
{
	int pos = -1;
	int height = himage->getHeight();
	int nTests = 4;
	if (!himage || height < 10)
	{
		return pos;
	}
	
	int offset = float(height) / float(nTests+1);
	vector<int> rowsToCheck;
	
	for (int i=0; i < nTests; i++)
		rowsToCheck.push_back(offset + offset* i);

	vector<int> foundCols(rowsToCheck.size(),0);

	for (int row =0; row < rowsToCheck.size(); row++)
	{
		unsigned char pixel;
		for (pos = 15; pos < himage->getWidth(); pos++)
		{
			himage->getRect(&pixel,rowsToCheck[row],1,pos,1);
			if (pixel > 0)
			{
				foundCols[row] = pos;
				break;
			}
		}

	}

	return foundCols[rowsToCheck.size()/2];
}