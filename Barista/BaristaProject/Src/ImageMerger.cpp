
#include "ImageMerger.h"
#include "ImgBuf.h"
#include "PushbroomScanner.h"
#include "CCDLine.h"
#include "BaristaProject.h"
#include "AlosReader.h"
#include "WallisFilter.h"
#include "float.h"

CImageMerger::CImageMerger() : newImage(0),newImageWidth(0),
	newImageHeight(0),firstIndexNewImage(0),lastIndexNewImage(0),
	minYValue(0),maxYValue(0),newImageBpp(0),newImageBands(0),marginInterpolation(0),
	method(eNearestNeighbour),initWallisFilter(false)
{

}

CImageMerger::~CImageMerger(void)
{

}

bool CImageMerger::init(const vector<CVImage*>& inputImages,
						unsigned int startPos,
						unsigned int endPos,
						CCharString newCameraName,
						CImageMerger::InterpolationMethod method,
						CVImage* newMergedImage)
{
	this->inputImages.clear();

	this->inputImages = inputImages;
	
	project.getUniqueCameraName(newCameraName);
	this->newCameraName = newCameraName;
	this->startColFirstImage = startPos;
	this->endColLastImage = endPos;
	this->method = method;
	this->newImage = newMergedImage;

	if (this->inputImages.size() < 2)
	{
		this->errorMessage = "Less then 2 images! No merging possible!";
		return false;
	}



	if (!this->newImage)
	{
		this->errorMessage = "No image selected.";
		return false;
	}

	this->newFileName = this->newImage->getFileNameChar();

	if (this->newFileName.IsEmpty())
	{
		this->errorMessage = "Filename for merged image is empty.";
		return false;
	}


	this->newHugeFileName = this->newFileName + ".hug";



	if (this->method == eNearestNeighbour)
		this->marginInterpolation = 0;
	else if (this->method == eBiLinear)
		this->marginInterpolation = 2;
	else if (this->method == eBiCubic)
		this->marginInterpolation = 8;

	return true;
}

bool CImageMerger::mergeImages()
{

	// first create a new image in the project
	bool ret = this->initNewImage();

	vector<double> xPosCCD;
	vector<double> yPosCCD;
	vector<CHugeImage*> images;
	vector<unsigned int> nextImage;

	// now the positions of every CCD element of the new image 
	if (ret)
		ret = this->computeCCDPositions(xPosCCD,yPosCCD,images,nextImage);

	if (ret)
		ret = this->resampleImages(xPosCCD,yPosCCD,images,nextImage);

	if (ret)
		transferPointObservations();

	if (ret && this->initWallisFilter)
	{
		CHugeImage* himage = this->newImage->getHugeImage();
		himage->setProgressListener(this->listener);
		double defaultMean = himage->getBpp() == 8 ? DEFAULT_MEAN_8BIT : DEFAULT_MEAN_16BIT;
		double defaultStdDev = himage->getBpp() == 8 ? DEFAULT_STDDEV_8BIT : DEFAULT_STDDEV_16BIT;
		CWallisFilter* wf = himage->createWallisFilter(DEFAULT_WINDOWSIZE,DEFAULT_WINDOWSIZE,DEFAULT_CONTRAST,DEFAULT_BRIGHTNESS,defaultMean,defaultStdDev,true);
		if (wf)
		{
			ret = true;
			// we have saved the parameters on disk, so it's quick to change the transfer function now
			delete wf; 
		}
		else
		{
			ret = false;		
			this->errorMessage = "Could not initialise wallis filter!";
		}
		himage->removeProgressListener();
	}

	return ret;
}


bool CImageMerger::computeCCDPositions(vector<double>& xPosCCD,vector<double>& yPosCCD,vector<CHugeImage*>& accordingImage,vector<unsigned int>& nextImage)
{

	if (!this->newImage || this->inputImages.size() < 2)
		return false;

	CCCDLine* newCamera = this->newImage->getPushbroom()->getCamera();
	
	// init with first camera
	unsigned int currentIndex = 0;
	int currentWidth = this->inputImages[currentIndex]->getHugeImage()->getWidth();
	CCCDLine* currentCamera = this->inputImages[currentIndex]->getPushbroom()->getCamera();
	CDistortion* currentDistortion = currentCamera->getDistortion();
	
	if (!currentDistortion)
		return false;

	C2DPoint obs,obj;
	unsigned int lastIndex = 0;
	CXYZPoint newIRP = newCamera->getIRP();

	xPosCCD.resize(this->newImageWidth);
	yPosCCD.resize(this->newImageWidth);
	accordingImage.resize(this->newImageWidth);
	nextImage.clear();
	// this vector describes the transitions between the input images 
	nextImage.push_back(0);
	
	this->minYValue = 1000;
	this->maxYValue = -1000;

	float roundCoord = 0.0f;
	if (this->method == eNearestNeighbour)
		roundCoord = 0.5f;

	vector<int> endOfLine(this->inputImages.size());
	
	for (int i=0; i< endOfLine.size(); i++)
	{
		endOfLine[i] = this->inputImages[i]->getHugeImage()->getWidth();
	}
	endOfLine[this->inputImages.size() - 1] = this->endColLastImage; 

	// loop over every column of the new image 
	// we determine the position in the old sub images and save the according image
	for (int i=0; i < this->newImageWidth; i++)
	{
		// from framelet of new image into camera system
		obj.x = i  - newIRP.x;
		obj.y = - newIRP.y;

		// apply distortion in camera system with old camera
		currentDistortion->applyObj2Obs(obs,obj,currentCamera->getIRP(),currentCamera->getScale());

		// transform into framelet coordinates of old camera
		xPosCCD[i] = obs.x + currentCamera->getIRP().x + roundCoord;
		yPosCCD[i] = obs.y + currentCamera->getIRP().y + roundCoord;
		accordingImage[i] = this->inputImages[currentIndex]->getHugeImage();
		
		// save the min and max shift in y direction
		if (yPosCCD[i] < this->minYValue)
			this->minYValue = yPosCCD[i];
		else if (yPosCCD[i] > this->maxYValue)
			this->maxYValue = yPosCCD[i];


		if ((endOfLine[currentIndex] - xPosCCD[i]) <= this->marginInterpolation + 1)
		{
			if ( (currentIndex + 1) < this->inputImages.size() ) // change to next image in vector
			{
				currentIndex++;
				currentCamera = this->inputImages[currentIndex]->getPushbroom()->getCamera();
				currentDistortion = currentCamera->getDistortion();
				nextImage.push_back(i); // last index in left image
			}
		}
	}

	nextImage.push_back(this->newImageWidth - 1); // last index in left image, here the last image
	
	// data for the last rows in the new image would come from invalid positions in the old images
	if ((this->maxYValue + this->marginInterpolation) > 0)
	{
		this->newImageHeight -= this->maxYValue + this->marginInterpolation;
	}



	return true;
}

bool CImageMerger::initNewImage()
{
	
	CPushBroomScanner* currentScanner = this->inputImages[0]->getPushbroom();
	CCCDLine* currentCamera = currentScanner->getCamera();

	this->newImageBands = this->inputImages[0]->getHugeImage()->getBands();
	this->newImageBpp = this->inputImages[0]->getHugeImage()->getBpp();


	CXYZPoint newIRP(0,0,0);

	// determine the new irp of the merged image so that startPos of first image is i the new image
	// also consider offset caused by the interpolation
	C3DPoint tmpPoint(this->startColFirstImage + this->marginInterpolation,0,0);
	
	// when outside of the image because of the interpolation, correct the startPosition
	if (tmpPoint.x < 0)
	{
		this->startColFirstImage -= tmpPoint.x;
		tmpPoint.x = 0;
	}
	currentCamera->transformObs2Obj(newIRP,tmpPoint);
	newIRP.x = - newIRP.x;
	newIRP.y = currentCamera->getIRP().y;
	newIRP.z = currentCamera->getIRP().z;

	// check first if we got a camera with the same irp, if not create a new one 
	CCCDLine* newCamera = NULL;
	for (int i=0; i < project.getNumberOfCameras(); i++)
	{
		CCamera* c = project.getCameraAt(i);

		if (c &&  c->getCameraType() == eCCD  && c->getDistortion())
		{
			C3DPoint tmpIRP(c->getIRP());


			if (fabs(tmpIRP.x -newIRP.x) < FLT_EPSILON && 
				fabs(tmpIRP.y -newIRP.y) < FLT_EPSILON &&
				fabs(tmpIRP.z -newIRP.z) < FLT_EPSILON)
			{
				newCamera = (CCCDLine*) c;
				break;
			}
		}
	}

	if (!newCamera)
	{// create a new camera
		newCamera= (CCCDLine*)project.addCamera(eCCD);
		newCamera->setName(this->newCameraName);
		newCamera->setIRP(newIRP);
		doubles parameters(6,0.0);
		parameters[1] = 1.0;
		newCamera->setDistortion(eDistortionALOS,parameters);
	}

	// init the pushbroom scanner with the new camera
	CPushBroomScanner* newScanner = this->newImage->getPushbroom();
	bool ret = newScanner->init(currentScanner->getOrbitPath(),
						 currentScanner->getOrbitAttitudes(),
						 currentScanner->getCameraMounting(),
						 newCamera,
						 "Merged PushBroomscanner",
						 currentScanner->getSatelliteHeight(),
						 currentScanner->getGroundHeight(),
						 currentScanner->getGroundTemperature(),
						 currentScanner->getFirstLineTime(),
						 currentScanner->getTimePerLine());

	
	if (!ret)
	{
		this->errorMessage = "Failed to initialize pushbroom scanner.";
		return false;
	}
	else
	{
		this->newImage->setCurrentSensorModel(ePUSHBROOM);
		//addition>> 02-Mar-2009 by M. Awrangjeb
		this->newImage->getPushbroom()->setChangeMountingForMerging(currentScanner->getChangeMountingForMerging());
		//addition
	}

	// we also determine the new image width so that endPos of last image is just in the new image
	unsigned int indexLastImage = this->inputImages.size() -1;
	currentScanner = this->inputImages[indexLastImage]->getPushbroom();
	currentCamera = currentScanner->getCamera();

	tmpPoint.x = this->endColLastImage - this->marginInterpolation;

	if (tmpPoint.x >= this->inputImages[indexLastImage]->getHugeImage()->getWidth())
	{
		this->endColLastImage =this->inputImages[indexLastImage]->getHugeImage()->getWidth() - this->marginInterpolation ;
		tmpPoint.x = this->endColLastImage;
	}


	C3DPoint lastPoint(0,0,0);	
	currentCamera->transformObs2Obj(lastPoint,tmpPoint);
	this->newImageWidth = newIRP.x + lastPoint.x;
	this->newImageHeight = this->inputImages[0]->getHugeImage()->getHeight();

	return true;
}


bool CImageMerger::resampleImages(const vector<double>& xPosCCD,const vector<double>& yPosCCD,const vector<CHugeImage*>& accordingImage,const vector<unsigned int>& nextImage)
{
	CHugeImage* hImage = this->newImage->getHugeImage();

	// check first if we have to hug file already
	bool ret = hImage->open(this->newHugeFileName.GetChar(),true);

	if (!ret)
	{
		 ret = hImage->prepareWriting(this->newHugeFileName.GetChar(),this->newImageWidth,this->newImageHeight,this->newImageBpp,this->newImageBands);

		if (ret)
		{
			int tileWidth = hImage->getTileWidth();
			int tileHeight = hImage->getTileHeight();
			int nTiles = hImage->tilesAcross * hImage->tilesDown;
			unsigned long indexCol;
			int indexFirstCol,indexLastCol;
			int indexFirstRow,indexLastRow;
			int indexSrc;
			unsigned long rowOffset;
			
			int rowImage,colImage;
			C2DPoint srcImagePos;

			CImageBuffer destBuf(0,0,tileWidth,tileHeight,hImage->getBands(),hImage->getBpp(),true);


			for (rowImage = 0;  rowImage < this->newImageHeight; rowImage+=tileHeight)
			{
				unsigned long maxHeight = (unsigned long)destBuf.getHeight();
				if (rowImage + maxHeight > this->newImageHeight) 
					maxHeight = this->newImageHeight - rowImage;

				unsigned int colOffset=0;

				for (int indexImage = 0; indexImage < this->inputImages.size(); indexImage++)
				{
					// first process all full tiles in the current image
					for (colImage = nextImage[indexImage] + colOffset; (colImage + tileWidth) < nextImage[indexImage + 1] + 1; colImage+= tileWidth)
					{
						// we have checked in createNewImage that indexFirstCol and indexLastCol are inside the old image 
						indexFirstCol = xPosCCD[colImage] - this->marginInterpolation;
						indexLastCol = xPosCCD[colImage + tileWidth] + this->marginInterpolation;

						indexFirstRow = rowImage + this->minYValue - this->marginInterpolation;
						indexLastRow =  rowImage + tileHeight + this->maxYValue + this->marginInterpolation;
						
						CImageBuffer srcBuf(indexFirstCol,indexFirstRow,indexLastCol-indexFirstCol,indexLastRow -indexFirstRow,this->newImageBands,this->newImageBpp,true);
						accordingImage[colImage]->getRect(srcBuf);
						
						if (this->method == eNearestNeighbour)
						{
							for (int rowTile = 0; rowTile < maxHeight; rowTile++)
							{
								unsigned long rowOffset = rowTile * tileWidth;

								for (int colTile = 0; colTile < tileWidth; colTile++)
								{
									indexCol = colImage + colTile;
									indexSrc = (rowTile + int(yPosCCD[indexCol]) - this->minYValue)*srcBuf.getWidth();
									indexSrc += xPosCCD[indexCol] - indexFirstCol;
									destBuf.pixUChar(rowOffset + colTile ) = srcBuf.pixUChar(indexSrc);
								}
							}	
						}
						else if (this->method == eBiLinear)
						{
							for (int rowTile = 0; rowTile < maxHeight; rowTile++)
							{
								unsigned long rowOffset = rowTile * tileWidth;

								for (int colTile = 0; colTile < tileWidth; colTile++)
								{
									indexCol = colImage + colTile;
									srcImagePos.y = yPosCCD[indexCol] + rowTile + rowImage ;
									srcImagePos.x = xPosCCD[indexCol];
									srcBuf.getInterpolatedColourVecBilin(srcImagePos,destBuf.getPixUC() + rowOffset + colTile);
								}
							}	
						}
						else if (this->method == eBiCubic)
						{
							for (int rowTile = 0; rowTile < maxHeight; rowTile++)
							{
								unsigned long rowOffset = rowTile * tileWidth;

								for (int colTile = 0; colTile < tileWidth; colTile++)
								{
									indexCol = colImage + colTile;
									srcImagePos.y = yPosCCD[indexCol] + rowTile + rowImage ;
									srcImagePos.x = xPosCCD[indexCol];
									srcBuf.getInterpolatedColourVecBicub(srcImagePos,destBuf.getPixUC() + rowOffset + colTile);
								}
							}	
						}

						// dump the full tile
						hImage->dumpTile(destBuf);
					}


					// the next tile of the new image has data also from the next image
					colOffset = colImage + tileWidth - nextImage[indexImage + 1];
					int restLastImage = nextImage[indexImage + 1] - colImage;

					// first process from the old image
					indexFirstCol = xPosCCD[colImage] - this->marginInterpolation;
					indexLastCol = xPosCCD[colImage + restLastImage] + this->marginInterpolation; 
					indexFirstRow = rowImage + this->minYValue - this->marginInterpolation;
					indexLastRow =  rowImage + tileHeight + this->maxYValue + this->marginInterpolation;
					
					// the buffer needs to be one pixel wider in x direction because restLastImage is also considered
					CImageBuffer srcBuf(indexFirstCol,indexFirstRow,indexLastCol-indexFirstCol + 1,indexLastRow -indexFirstRow,this->newImageBands,this->newImageBpp,true);
					accordingImage[colImage]->getRect(srcBuf);
					
					if (this->method == eNearestNeighbour)
					{
						for (int rowTile = 0; rowTile < maxHeight; rowTile++)
						{
							unsigned long rowOffset = rowTile * tileWidth;
							
							for (int colTile = 0; colTile <= restLastImage; colTile++)
							{
								
								indexCol = colImage + colTile;
								indexSrc = (rowTile + int(yPosCCD[indexCol]) - this->minYValue) *srcBuf.getWidth();
								indexSrc += xPosCCD[indexCol] - indexFirstCol;
								destBuf.pixUChar(rowOffset + colTile) = srcBuf.pixUChar(indexSrc);
							}
						}
					}
					else if (this->method == eBiLinear)
					{
						for (int rowTile = 0; rowTile < maxHeight; rowTile++)
						{
							unsigned long rowOffset = rowTile * tileWidth;
							
							for (int colTile = 0; colTile <= restLastImage; colTile++)
							{
								indexCol = colImage + colTile;
								srcImagePos.y = yPosCCD[indexCol] + rowTile + rowImage ;
								srcImagePos.x = xPosCCD[indexCol];
								srcBuf.getInterpolatedColourVecBilin(srcImagePos,destBuf.getPixUC() + rowOffset + colTile);
							}
						}
					}
					else if (this->method == eBiCubic)
					{
						for (int rowTile = 0; rowTile < maxHeight; rowTile++)
						{
							unsigned long rowOffset = rowTile * tileWidth;
							
							for (int colTile = 0; colTile <= restLastImage; colTile++)
							{
								indexCol = colImage + colTile;
								srcImagePos.y = yPosCCD[indexCol] + rowTile + rowImage ;
								srcImagePos.x = xPosCCD[indexCol];
								srcBuf.getInterpolatedColourVecBicub(srcImagePos,destBuf.getPixUC() + rowOffset + colTile);
							}
						}		
					}
					
					// and then process form the next one, but only when not last image
					if ((indexImage + 1) < this->inputImages.size())
					{
						
						indexFirstCol = xPosCCD[nextImage[indexImage + 1]+1] - this->marginInterpolation;
						indexLastCol = xPosCCD[colImage + tileWidth] + this->marginInterpolation;
						indexFirstRow = rowImage + this->minYValue - this->marginInterpolation;
						indexLastRow =  rowImage + tileHeight + this->maxYValue + this->marginInterpolation;
						
						CImageBuffer srcBuf(indexFirstCol,indexFirstRow,indexLastCol-indexFirstCol,indexLastRow -indexFirstRow,this->newImageBands,this->newImageBpp,true);
						accordingImage[colImage + tileWidth]->getRect(srcBuf);
						
						if (this->method == eNearestNeighbour)
						{
							for (int rowTile = 0; rowTile < maxHeight; rowTile++)
							{
								unsigned long rowOffset = rowTile * tileWidth;
								
								for (int colTile = restLastImage + 1; colTile < tileWidth; colTile++)
								{
									indexCol = colImage + colTile;
									indexSrc = (rowTile + int(yPosCCD[indexCol]) - this->minYValue) *srcBuf.getWidth();
									indexSrc += xPosCCD[indexCol] - indexFirstCol;
									destBuf.pixUChar(rowOffset + colTile ) = srcBuf.pixUChar(indexSrc);
								}
							}
						}
						else if (this->method == eBiLinear)
						{
							for (int rowTile = 0; rowTile < maxHeight; rowTile++)
							{
								unsigned long rowOffset = rowTile * tileWidth;
								
								for (int colTile = restLastImage + 1; colTile < tileWidth; colTile++)
								{
									indexCol = colImage + colTile;
									srcImagePos.y = yPosCCD[indexCol] + rowTile + rowImage ;
									srcImagePos.x = xPosCCD[indexCol];
									srcBuf.getInterpolatedColourVecBilin(srcImagePos,destBuf.getPixUC() + rowOffset + colTile);							
								}
							}
						}
						else if (this->method == eBiCubic)
						{
							for (int rowTile = 0; rowTile < maxHeight; rowTile++)
							{
								unsigned long rowOffset = rowTile * tileWidth;
								
								for (int colTile = restLastImage + 1; colTile < tileWidth; colTile++)
								{
									indexCol = colImage + colTile;
									srcImagePos.y = yPosCCD[indexCol] + rowTile + rowImage ;
									srcImagePos.x = xPosCCD[indexCol];
									srcBuf.getInterpolatedColourVecBicub(srcImagePos,destBuf.getPixUC() + rowOffset + colTile);
								}
							}
						}
					}

					hImage->dumpTile(destBuf);

				}// for indexImage

				if (this->listener)
				{
					double percent = 100.0 *  double(rowImage) / double(this->newImageHeight);
					this->listener->setProgressValue(percent);
				}

			} 

			ret = hImage->finishWriting();
		}
	}

	return ret;
}

int CImageMerger::findBoundary(CHugeImage* himage, bool onRightSide)
{
	int pos = -1;
	int height = himage->getHeight();
	int nTests = 4;
	if (!himage || height < 10)
	{
		return pos;
	}
	
	int offset = float(height) / float(nTests+1);
	vector<int> rowsToCheck;
	
	for (int i=0; i < nTests; i++)
		rowsToCheck.push_back(offset + offset* i);

	vector<int> foundCols(rowsToCheck.size());

	for (int row =0; row < rowsToCheck.size(); row++)
	{
		unsigned char pixel;
		if (onRightSide)
		{
			for (pos = himage->getWidth() - 1; pos >= 32; pos--)
			{
				himage->getRect(&pixel,rowsToCheck[row],1,pos,1);
				if (pixel > 0)
				{
					foundCols[row] = pos;
					break;
				}
			}

		}
		else
		{
			for (pos = 32; pos < himage->getWidth(); pos++)
			{
				himage->getRect(&pixel,rowsToCheck[row],1,pos,1);
				if (pixel > 0)
				{
					foundCols[row] = pos;
					break;
				}
			}
		}
	}


	return foundCols[rowsToCheck.size()/2];
}

void CImageMerger::transferPointObservations()
{
	CXYPointArray* newPointArray = this->newImage->getXYPoints();
	CXYPoint newIRP(this->newImage->getPushbroom()->getCamera()->getIRP().x,
					this->newImage->getPushbroom()->getCamera()->getIRP().y);

	C3DPoint old3D,new3D;
	for (unsigned int i = 0; i < this->inputImages.size(); i++)
	{
		CXYPointArray* oldPointArray = this->inputImages[i]->getXYPoints();
		CCamera* camera = this->inputImages[i]->getPushbroom()->getCamera();
		for (int k=0; k < oldPointArray->GetSize(); k++)
		{
			CXYPoint* oldPoint = oldPointArray->getAt(k);
			CXYPoint* newPoint = newPointArray->add(oldPoint);
			old3D.x = oldPoint->x;
			old3D.y = oldPoint->y;
			camera->transformObs2Obj(new3D,old3D);
			newPoint->x = new3D.x + newIRP.x ;
			newPoint->y = oldPoint->y + new3D.y + newIRP.y;
		}

	}
}


bool CImageMerger::sortImages(vector<CVImage*>& inputImages)
{
	vector<CVImage*> localImages(inputImages);

	inputImages.clear();
	
	for (unsigned int image=0; image < localImages.size(); image++)
	{
		CVImage* current = localImages[image];

		//now sort it accordingly to the x component of the irp
		unsigned int index =0;
		while ( index < inputImages.size() && 
				inputImages[index]->getPushbroom()->getCamera()->getDistortion()->getDirectObservation(PARAMETER_1) < 
				current->getPushbroom()->getCamera()->getDistortion()->getDirectObservation(PARAMETER_1))
		{
			index++;
		}

		inputImages.insert(inputImages.begin()+index,current);
	}
	return true;
}