#include "OrthoPhotoRecord.h"
#include "SystemUtilities.h"
#include "ImageExporter.h"
#include <float.h>

//================================================================================

COrthoPhotoRecord::COrthoPhotoRecord(): img(0), isSelected(false), imageExporter(0),GSD(DEFAULT_GSD,DEFAULT_GSD)
{
}

//================================================================================


COrthoPhotoRecord::COrthoPhotoRecord(const COrthoPhotoRecord &rec):
	img(rec.img), ortho(rec.ortho), imageExporter(0),  LLC(rec.LLC),
	RUC(rec.RUC), GSD(rec.GSD), isSelected(rec.isSelected)
{
	CTFW tfw = *rec.ortho.getTFW();
	tfw.sethasValues(rec.ortho.hasTFW());
	this->ortho.setTFW(tfw);

	if (this->img)
	{
		this->ortho.getHugeImage()->bpp   = this->img->getHugeImage()->getBpp();
		this->ortho.getHugeImage()->bands = this->img->getHugeImage()->getBands();
	}
	this->ortho.setName(rec.ortho.getFileNameChar());
}

//================================================================================

COrthoPhotoRecord::~COrthoPhotoRecord()
{
	this->clearExporter();
}

//================================================================================

void COrthoPhotoRecord::setOrthoFileType(const CCharString &extension)
{
	this->clearExporter();
	CFileName OrthoFileName(this->ortho.getName());
	OrthoFileName.SetFileExt(extension);
	this->ortho.setName(OrthoFileName);
}

//================================================================================

void COrthoPhotoRecord::initOrthoName()
{
	CFileName fn = CSystemUtilities::AbsPath(this->img->getHugeImage()->getFileNameChar());
	if (fn.Find(".hug") > 0) fn = fn.Left(fn.GetLength() - 4);
	fn = fn.GetPath() + CSystemUtilities::dirDelimStr + fn.GetFileName() + "_ortho.tif";
	this->ortho.setName(fn.GetChar());
}

//================================================================================

void COrthoPhotoRecord::setImg(CVImage *inputImage, bool initOrthoname)
{
	this->img = inputImage;
	this->ortho.getHugeImage()->bpp = this->img->getHugeImage()->getBpp();
	this->ortho.getHugeImage()->bands = this->img->getHugeImage()->getBands();
	CTFW tfw = *this->ortho.getTFW();
	tfw.sethasValues(true);
	this->ortho.setTFW(tfw);
	if (initOrthoname) this->initOrthoName();
}

//================================================================================

void COrthoPhotoRecord::initExporter()
{
	this->clearExporter();
	this->imageExporter = new CImageExporter(this->getOrthoFileName(),&this->ortho, false);
	this->imageExporter->setExportGeo(true);
	this->imageExporter->setBandInterlave(true);
}


CImageExporter& COrthoPhotoRecord::getImageExporter()
{
	if (!this->imageExporter) this->initExporter();

	return *this->imageExporter;
}

//================================================================================

bool COrthoPhotoRecord::exportOrtho()
{
	if (!this->imageExporter) this->initExporter();

	this->imageExporter->writeSettings();
	this->imageExporter->writeImage();

	return CSystemUtilities::fileExists(this->getOrthoFileName());
}

//================================================================================

void COrthoPhotoRecord::clearExporter()
{
	if (this->imageExporter) delete this->imageExporter;
	this->imageExporter = 0;
}


void COrthoPhotoRecord::setExtents(C2DPoint newLLC,C2DPoint newRUC)
{
	this->LLC = newLLC;
	this->RUC = newRUC;

	double d = (this->RUC.x - this->LLC.x) / this->GSD.x;
	double npix = floor(d);
	if (fabs(npix - d) > FLT_EPSILON) npix += 1.0;
	this->RUC.x = this->LLC.x +  GSD.x * npix;

	d = (this->RUC.y - this->LLC.y) / this->GSD.y;
	npix = floor(d);
	if (fabs(npix - d) > FLT_EPSILON) npix += 1.0;
	this->RUC.y = this->LLC.y + this->GSD.y * npix;
}


void COrthoPhotoRecord::setLLC(C2DPoint newLLC)
{
	 this->LLC.x = newLLC.x;
	 if (this->RUC.x < newLLC.x)
		 this->RUC.x = newLLC.x + this->GSD.x;
	 else
	 {
		 double d = (this->RUC.x - this->LLC.x) / this->GSD.x;
		 double npix = floor(d);
		 if (fabs(npix - d) > FLT_EPSILON) npix += 1.0;
		 this->RUC.x = this->LLC.x + this->GSD.x * npix;
	 }

	 this->LLC.y = newLLC.y;
	 if (this->RUC.y < newLLC.y)
		 this->RUC.y = newLLC.y + this->GSD.y;
	 else
	 {
		 double d = (this->RUC.y - this->LLC.y) / this->GSD.y;
		 double npix = floor(d);
		 if (fabs(npix - d) > FLT_EPSILON) npix += 1.0;

		 this->RUC.y = this->LLC.y + this->GSD.y * npix;
	 }

}

void COrthoPhotoRecord::setRUC(C2DPoint newRUC)
{
	 this->RUC.x = newRUC.x;
	 if (this->LLC.x > newRUC.x)
		 this->LLC.x = newRUC.x - GSD.x;
	 else
	 {
		 double d = (this->RUC.x - this->LLC.x) / this->GSD.x;

		 double npix = floor(d);
		 if (fabs(npix - d) > FLT_EPSILON) npix += 1.0;

		 this->LLC.x = this->RUC.x - this->GSD.x * npix;
	 }


	 this->RUC.y = newRUC.y;
	 if (this->LLC.y > newRUC.y)
		 this->LLC.y = newRUC.y - this->GSD.y;
	 else
	 {
		 double d = (this->RUC.y - this->LLC.y) / this->GSD.y;
		 double npix = floor(d);
		 if (fabs(npix - d) > FLT_EPSILON) npix += 1.0;
		 this->LLC.y = this->RUC.y - this->GSD.y * npix;
	 }

}

void COrthoPhotoRecord::setGSD(double newX, double newY)
{
	this->GSD.x  = newX;
	this->GSD.y =  newY;

	double d = (this->RUC.x - this->LLC.x) / newX;
	double npix = floor(d);
	if (fabs(npix - d) > FLT_EPSILON) npix += 1.0;
	this->RUC.x = this->LLC.x + newX * npix;

	d = (this->RUC.y - this->LLC.y) / newX;
	npix = floor(d);
	if (fabs(npix - d) > FLT_EPSILON) npix += 1.0;
	this->RUC.y = this->LLC.y + newX * npix;

}

void COrthoPhotoRecord::setOrthoPhotoName(CFileName newOrthoName)
{
	this->ortho.setName(newOrthoName.GetChar());
}
