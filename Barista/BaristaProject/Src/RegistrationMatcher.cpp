#include <float.h>
#include <strstream>
#include "RegistrationMatcher.h"
#include "VImage.h"
#include "m_FeatExtract.h"
#include "XYPointArray.h"
#include "BaristaProject.h"
#include "Trans2DAffine.h"
#include "Trans2DProjective.h"
#include "LSM.h"


CRegistrationMatcher::CRegistrationMatcher(CVImage *img1, CVImage *img2, float gsd1, float gsd2):
		inputimage1 (img1), inputimage2(img2), gsdImg1(gsd1), gsdImg2(gsd2), trafo(0), minCrossCorr(0.8f),
		matchWindowWidth(9), matchWindowHeigth(9), trafoModel(eAFFINEREGISTRATION), protocol(0), sigma0(0),
		maxParallaxDifference(2000)
{
	setSensorModel(eAFFINEREGISTRATION);

	this->listener = NULL;
}

CRegistrationMatcher::~CRegistrationMatcher(void)
{
	if (this->trafo) delete this->trafo;
	this->trafo = 0;
	this->closeProtocol();
}

void CRegistrationMatcher::setMatchWindowSize(int width, int height)
{
	matchWindowWidth = width;
	matchWindowHeigth = height;
};

void CRegistrationMatcher::setSensorModel(eRegistrationSensorModel model)
{
	if (this->trafo) delete this->trafo;
	this->trafoModel = model;
	this->trafo = 0;

	if (this->trafoModel == eAFFINEREGISTRATION)
	{
		this->trafo = new CTrans2DAffine;
		VarCovar.ReSize(6,6);
	}
	else if (this->trafoModel == ePROJECTIVEREGISTRATION)
	{
		this->trafo = new CTrans2DProjective;
		VarCovar.ReSize(9,9);
	}

	VarCovar = 0;
};

void CRegistrationMatcher::setFeatureExtractionPars(const FeatureExtractionParameters &featureParameters)
{
	this->FEXpars = featureParameters;
};

void CRegistrationMatcher::setMinCrossCorr(float minCC)
{
	this->minCrossCorr = minCC;
};

bool CRegistrationMatcher::initProtocol (const CFileName &filename)
{
	closeProtocol();
	this->protocol = new ofstream(filename.GetChar());
	if (!this->protocol->good()) closeProtocol();

	if (!this->protocol) return false;
	
	return true;
};

void CRegistrationMatcher::closeProtocol()
{
	if (this->protocol) delete this->protocol;
	this->protocol = 0;
};

bool CRegistrationMatcher::prepBuffer1(int level1)
{
	long boffsetx = 0;
	long boffsety = 0;
	CHugeImage *hug = this->inputimage1->getHugeImage();
	long bwidth   = hug->getWidth();
	long bheight  = hug->getHeight();
	int clev = 0;

	while (clev < level1)
	{
		clev++;
		bwidth /= 2;
		bheight /= 2;
	}

	if (bwidth * bheight > 2048 * 2048)
	{
		if (bwidth > 2048)
		{
			int n = (hug->getWidth() / 2048) + 1;
			bwidth = hug->getWidth() / n + n;
		};
		if (bheight > 2048)
		{
			int n = (hug->getHeight() / 2048) + 1;
			bheight = hug->getHeight()  / n + n;
		};
	};

	return this->buf1.set(boffsetx, boffsety, bwidth, bheight, hug->getBands(), hug->getBpp(),true);
};

bool CRegistrationMatcher::prepBuffer2(int level2)
{
	CHugeImage *hug = this->inputimage2->getHugeImage();
	
	C2DPoint P1(buf1.getOffsetX(), buf1.getOffsetY());
	C2DPoint P2(buf1.getOffsetX() + buf1.getWidth(), buf1.getOffsetY() + buf1.getHeight());
	C2DPoint p1, p2;

	this->trafo->transform(p1, P1);
	this->trafo->transform(p2, P2);
	
    if (p2.x < p1.x) 
	{
		double dmy = p1.x;
		p1.x = p2.x;
		p2.x = p1.x;
	}

    if (p2.y < p1.y) 
	{
		double dmy = p1.y;
		p1.y = p2.y;
		p2.y = p1.y;
	}
	if (p1.x < 0) p1.x = 0;
	if (p1.y < 0) p1.y = 0;
	if (p2.x >= hug->getWidth())  p2.x = hug->getWidth() - 1;
	if (p2.y >= hug->getHeight()) p2.y = hug->getHeight() - 1;
		
	p2 -= p1;

	return this->buf2.set(p1.x, p1.y, p2.x, p2.y, hug->getBands(), hug->getBpp(),true);
}

bool CRegistrationMatcher::matchCurrentWindow(float diffMaxX, float diffMaxY)
{
	this->buf1.RGBToIntensity();
	this->buf2.RGBToIntensity();

	if (this->protocol) 
	{
		this->protocol->precision(0);
		*(this->protocol) << "\n     Extracting features for image 1 at " << CCharString::getTimeString() << "; offset: ";
		this->protocol->width(6);
		*(this->protocol)<< this->buf1.getOffsetX() << " / ";
		this->protocol->width(6);
		*(this->protocol)<< this->buf1.getOffsetY() << " , dimensions: ";
		this->protocol->width(6);
		*(this->protocol)<< this->buf1.getWidth() << " x ";
		this->protocol->width(6);
		*(this->protocol)<< this->buf1.getHeight() << " [pixels]" << endl;
	}

	CXYPointArray points1, points2;
	CFeatureExtractor extractor (this->FEXpars);
	CXYPolyLine *poly = 0;
	extractor.setRoi(&(this->buf1), poly);
	extractor.extractFeatures(&points1, 0, 0, 0, 0, false);
	extractor.resetRoi();

	if (this->protocol) 
	{
		*(this->protocol) << "     Number of points extracted from image 1: ";
		this->protocol->width(7);
		*(this->protocol) << points1.GetSize() 
						  << "\n\n     Extracting features for image 2 at " << CCharString::getTimeString() <<"; offset: ";
		this->protocol->width(6);
		*(this->protocol)<< this->buf2.getOffsetX() << " / ";
		this->protocol->width(6);
		*(this->protocol)<< this->buf2.getOffsetY() << " , dimensions: ";
		this->protocol->width(6);
		*(this->protocol)<< this->buf2.getWidth() << " x ";
		this->protocol->width(6);
		*(this->protocol)<< this->buf2.getHeight() << " [pixels]" << endl;
	}	

	CXYPolyLine *polyBoundary = 0;
	extractor.setRoi(&(this->buf2), polyBoundary);
	extractor.extractFeatures(&points2, 0, 0, 0, 0, false);
	extractor.resetRoi();

	if (this->protocol) 
	{
		*(this->protocol) << "     Number of points extracted from image 2: ";
		this->protocol->width(7);
		*(this->protocol) << points2.GetSize()
			              << "\n\n     Correspondence analysis started at " <<  CCharString::getTimeString() << endl;
	}

	CMatchRecordVector recVec1(points1, buf1, this->matchWindowWidth, this->matchWindowHeigth);
	CMatchRecordVector recVec2(points2, buf2, this->matchWindowWidth, this->matchWindowHeigth);

	C2DPoint trafPos;

	for (int i = 0; i < recVec1.nRecords(); ++i)
	{
		CMatchRecord *rec1 = recVec1[i];
		this->trafo->transform(trafPos, rec1->getPos());	

		int jMin = recVec2.getIndexByY(trafPos.y - diffMaxY);
		int jMax = recVec2.getIndexByY(trafPos.y + diffMaxY);

		for (long j = jMin; j < jMax; ++j)
		{
			CMatchRecord *rec2 = recVec2[j];
			C2DPoint diff = trafPos - rec2->getPos();

			if ((fabs(diff.x) < diffMaxX) && (fabs(diff.y) < diffMaxY))
			{
				rec1->checkHypothesis(rec2, this->minCrossCorr);
			}
		}
	}

	recVec1.deleteNoMatches();
	recVec2.deleteNoMatches();

	if (this->protocol) 
	{
		*(this->protocol) << "     Correspondence analysis finished at " << CCharString::getTimeString()
			              << "\n     Number of correspondence hypotheses generated: ";
		this->protocol->width(7);
		*(this->protocol) << recVec1.getNHypotheses();
		if (recVec1.getNHypotheses() < 4)
			*(this->protocol) << "\n\n     Not enough hypotheses generated " << endl;
		else
			*(this->protocol) << "\n\n     Starting evaluation of correspondence hypotheses at " << CCharString::getTimeString() << endl;
	}

	bool success = true;

	if (recVec1.getNHypotheses() >= 4)
	{
		CTrans2D *localtrafo = this->trafo->clone();

		Matrix localCovar;
		success = recVec1.computeRobustTrafo(localtrafo, localCovar, recVec2, 3.0, this->sigma0);
		
		if (this->protocol) 
		{
			*(this->protocol) << "     Evaluation of correspondence analysis finished at " << CCharString::getTimeString()
							  << "\n     Number of correct correspondence hypotheses: ";
			this->protocol->width(7);
			*(this->protocol) << recVec1.getNHypotheses()<< "\n\n     Results:\n" << localtrafo->getDescriptor("       ", 2, 7);
			  this->writeTrafoCovarToProt("       ", localCovar);

			*(this->protocol) << "\n     Time: "  << CCharString::getTimeString() << "\n" << endl;
		}

		if (success)
		{
			this->matches1.appendVectors(recVec1, this->matches2, recVec2);
		}


		delete localtrafo;
	}
	else success = false;

	return success;
};

bool CRegistrationMatcher::matchCurrent(int level1, int level2, float diffMaxX, float diffMaxY)
{
	matches1.deleteAll();
	matches2.deleteAll();

	if (this->protocol) 
	{
		*(this->protocol) << "\n\n  Matching at image pyramid levels " << level1 << " (image 1) and " 
			              << level2 << " (image 2)\n  ============================================================";
		this->protocol->precision(1);
		*(this->protocol) << "\n\n     Maximum coordinate differences (X / Y): " << diffMaxX << " / " << diffMaxY
						  << " [pixels]\n     Starting time: " << CCharString::getTimeString() << "\n" << endl;
	}

	bool ret = prepBuffer1(level1);

	if (ret)
	{
		ret = false;

		CHugeImage *hug1 = this->inputimage1->getHugeImage();
		for (int i = 0; i < level1; ++i) hug1 = (CHugeImage *) hug1->halfRes;

        C2DPoint offset0 = this->buf1.getOffset();
		
		int nAllTiles = ((hug1->getWidth()  - (offset0.x + 10)) / buf1.getWidth()  + 1) *
			            ((hug1->getHeight() - (offset0.y + 10)) / buf1.getHeight() + 1);
		int iTile = 0;

		for ( ; offset0.y + 10 < hug1->getHeight(); offset0.y += buf1.getHeight())
		{
			for (C2DPoint offset = offset0 ; offset.x + 10 < hug1->getWidth(); offset.x += buf1.getWidth(), ++iTile)
			{
				buf1.setOffset(offset.x, offset.y);
				if (prepBuffer2(level2))
				{
					if ((this->inputimage1->getHugeImage()->getRect(buf1, level1)) && (this->inputimage2->getHugeImage()->getRect(buf2, level2)))
					{
						ret |= matchCurrentWindow(diffMaxX, diffMaxY);			
					}

					if (this->listener)
					{
						float percent = float(iTile) * float(100.0) / float(nAllTiles);
						if (percent > 100.0) percent = float(99.0);
						this->listener->setProgressValue(percent);
					}
				}
			}
		}

		if (this->protocol)
		{
			*(this->protocol) << "\n  Matching at image pyramid levels " << level1 << " (image 1) and " 
			                      << level2 << " (image 2) generated "  
								  << this->matches1.nRecords() 
								  << " homologous points\n  Starting overall evaluation at " 
								  << CCharString::getTimeString() << endl;
		}

		if (ret) ret = this->matches1.computeRobustTrafo(this->trafo, this->VarCovar, this->matches2, 3.0, this->sigma0);

		if (!ret) 
		{
			if (this->protocol)
			{
				*(this->protocol) << "\n  Matching at image pyramid levels " << level1 << " (image 1) and " 
			                      << level2 << " (image 2) was not successful\n  Time: " 
								  << CCharString::getTimeString() << endl;
			}
		}
		else
		{
			if (this->protocol)
			{
				*(this->protocol) << "\n  Matching at image pyramid levels " << level1 << " (image 1) and " 
			                      << level2 << " (image 2) was successful\n\n  Number of homologous points: "
								  << this->matches1.nRecords() << "\n\nResults:\n" << trafo->getDescriptor("    ", 2, 7);	
				writeTrafoCovarToProt("    ", this->VarCovar);
				*(this->protocol) << "  Time: " << CCharString::getTimeString() << endl;
			}

		}
	}

	return ret;
}
	  
bool CRegistrationMatcher::MoveMeasuredPoints()
{
	bool ret = true;

	CXYPointArray *pointsImg1 = this->inputimage1->getXYPoints();
	CXYPointArray *pointsImg2 = this->inputimage2->getXYPoints();
				
	if ((!pointsImg1) || (!pointsImg2)) ret = false;
	else
	{
		int nPts = 0;

		pointsImg2->sortByLabel();
		for (int i = 0; i < pointsImg1->GetSize(); ++i)
		{
			CXYPoint *pRef = pointsImg1->getAt(i);
			CLabel lab = pRef->getLabel();
			CObsPoint *pSearch = pointsImg2->getPoint_binarySearch(lab);
			if (!pSearch)
			{
				CXYPoint *pSearchNew = pointsImg2->add();
				this->trafo->transform(*pSearchNew,*pRef);
				++nPts;
			}
		}

		CTrans2D *trafInv = this->getTrafoClone();
		if (trafInv->invert())
		{
			pointsImg1->sortByLabel();
			for (int i = 0; i < pointsImg2->GetSize(); ++i)
			{
				CXYPoint *pRef = pointsImg2->getAt(i);
				CLabel lab = pRef->getLabel();
				CObsPoint *pSearch = pointsImg1->getPoint_binarySearch(lab);
				if (!pSearch)
				{
					CXYPoint *pSearchNew = pointsImg1->add();
					trafInv->transform(*pSearchNew,*pRef);
					++nPts;
				}
			}
		}

		ret = nPts > 0;
		delete trafInv;
	}

	return ret;
};

bool CRegistrationMatcher::match(float startGSD, bool keepPoints)
{
	float log2 = 1.0 / log(2.0);
	int level1 = (int) floor(log(startGSD / this->gsdImg1) * log2 + 0.5);
	int level2 = (int) floor(log(startGSD / this->gsdImg2) * log2 + 0.5);

	double factor1 = pow(2.0f, level1);
	double factor2 = pow(2.0f, level2);

    float diffXMax = this->maxParallaxDifference / factor1;
    float diffYMax = this->maxParallaxDifference / factor1;
	
	int l1 = level1, l2 = level2;
	int maxIt = 1;
	while ((l1 > 0) && (l2 > 0)) 
	{
		int minLev = (l1 < l2) ? l1 : l2;
		if (minLev > 3) minLev = 3;
		l1 -= minLev;
		l2 -= minLev;
		++maxIt;
	}

	if (this->protocol)
	{
		this->protocol->setf(ios::fixed, ios::floatfield);
		this->protocol->precision(2);

		*(this->protocol) << "Image Registration by Feature Based Matching\n============================================\n\nDate: " 
 						  << CCharString::getDateString() << "\nTime: " << CCharString::getTimeString() << "\n\nImage 1: " 
						  << this->inputimage1->getFileNameChar() << "\n    GSD: ";
		this->protocol->width(5); 
		*(this->protocol) << this->gsdImg1 <<" m\nImage 2: " << this->inputimage2->getFileNameChar() << "\n    GSD: ";
		this->protocol->width(5); 
		*(this->protocol) << this->gsdImg2 << " m\n\nTransformation model: ";
		if (this->trafoModel == eAFFINEREGISTRATION) *(this->protocol) << "Affine";
		else *(this->protocol) << "Projective";
		*(this->protocol) << "\nSize of match window: " << matchWindowWidth << " / " << matchWindowHeigth << " pixels\nMinimum correlation coefficient for matching candidates: ";
		this->protocol->precision(3);
		this->protocol->width(5);
		*(this->protocol) << minCrossCorr << "\n\nParameters for Foerstner point extraction: \n           Noise Model:                      ";
		if (this->FEXpars.NoiseModel == ePoisson) *(this->protocol) << "Poisson noise";
		else if (this->FEXpars.NoiseModel == eGaussian)  *(this->protocol) << "Gaussian noise";
		else  *(this->protocol) << "Percentile of texture roughness";

		*(this->protocol) << " \n           Derivative Filter:                ";
		if (this->FEXpars.getDerivativeFilterX().getType() == eDiffX) 
			*(this->protocol) << "Coordinate difference (3 x 1 or 1 x 3)";
		else if (this->FEXpars.getDerivativeFilterX().getType() == eSobelX) 
			*(this->protocol) << "Sobel filter (3 x 3)";
		else 
		{
			*(this->protocol) << "Derivative of Gaussian filter; sigma :";
			this->protocol->precision(3);
			this->protocol->width(7); 
			*(this->protocol) << this->FEXpars.getDerivativeFilterX().getParameter() << " ("
							  << this->FEXpars.getDerivativeFilterX().getSizeX() << " x " 
							  << this->FEXpars.getDerivativeFilterX().getSizeY()<< ")";
		}

		*(this->protocol) << " \n           Foerstner Integration Filter:     ";
		if (this->FEXpars.getIntegrationFilterPoints().getType() == eBinom) 
			*(this->protocol) << "Binomial filter (";
		else if (this->FEXpars.getIntegrationFilterPoints().getType() == eAverage) 
			*(this->protocol) << "Moving average filter (";
		else 
		{
			*(this->protocol) << "Gaussian filter; sigma :";
			this->protocol->precision(3);
			this->protocol->width(7); 
			*(this->protocol) << this->FEXpars.getIntegrationFilterPoints().getParameter() << " (";
		}
		*(this->protocol) << this->FEXpars.getIntegrationFilterPoints().getSizeX() << " x " 
			              << this->FEXpars.getIntegrationFilterPoints().getSizeY()
						  << ")\n           Alpha for Texture Classification: ";
		this->protocol->precision(2);
		*(this->protocol) << this->FEXpars.getAlpha() * 100 <<" %\n           Non-maxima Suppression:           " 
			              << this->FEXpars.distNonmaxPts 
						  << " Pixels\n" << endl;

	};

	int it = 1;
	
	if (this->listener)
	{
		ostrstream oss;
		oss << "Image co-registration: Iteration 1 of " << maxIt << ends;
		char *z = oss.str();
		this->listener->setTitleString(z);
		delete [] z;
		this->listener->setProgressValue(0);
	};

	bool success = matchCurrent(level1, level2, diffXMax, diffYMax);

	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
	};

	diffXMax = diffYMax = 3.0;
	
	while ((level1 > 0) && (level2 > 0) && success) 
	{
		++it;
		int minLev = (level1 < level2) ? level1 : level2;
		if (minLev > 3) minLev = 3;
		double fact = pow(2.0, (double) minLev);
		level1 -= minLev;
		level2 -= minLev;
		
		Matrix pars;
		this->trafo->applyFactorToBoth(fact, this->VarCovar);
		if (this->listener)
		{
			ostrstream oss;
			oss << "Image co-registration: Iteration " << it << " of " << maxIt << ends;
			char *z = oss.str();
			this->listener->setTitleString(z);
			delete [] z;
			this->listener->setProgressValue(0);
		};

		success = matchCurrent(level1, level2, fact * diffXMax, fact * diffYMax);

		if (this->listener)
		{
			this->listener->setProgressValue(100.0);
		};

		factor1 = pow(2.0f, level1);
		factor2 = pow(2.0f, level2);
	}

	if (success)
	{		
		double o = (factor1-1) * 0.5;
		C2DPoint offset1(o, o);
		o = (factor2-1) * 0.5;
		C2DPoint offset2(o, o);

		if (keepPoints)
		{
			CLabel matchLabel("ZZMATCH_1");

			CXYPointArray *pts1 = this->inputimage1->getXYPoints();
			CXYPointArray *pts2 = this->inputimage2->getXYPoints();


			for (int i = 0; i < this->matches1.nRecords(); ++i, matchLabel++)
			{	
				CXYPoint pi = this->matches1[i]->getP();
				CXYPoint pj = this->matches1[i]->getHypothesis(0)->getP();						
				pi = pi * factor1;// + offset1;
				pj = pj * factor2 + offset2;
				Matrix *var = pi.getCovariance();
				(*var) *= factor1 * factor1;
				var = pj.getCovariance();
				(*var) *= factor2 * factor2;
				pi.setLabel(matchLabel);
				pj.setLabel(matchLabel);
				pts1->Add(pi);
				pts2->Add(pj);	
			}
						
			pts1->setActive(true);
			pts2->setActive(true);
		}	

		this->trafo->applyLinearTransformation(factor2, factor1, offset2, offset1, this->VarCovar);


		if (this->protocol)
		{
			CTrans2D *tred = this->trafo->clone();
			tred->applyP0();

			*(this->protocol) << "\n\nHierarchical matching was finished successfully\n\nResults:\n"
				              << this->trafo->getDescriptor("  ", 2, 7);	
			writeTrafoCovarToProt("  ", this->VarCovar);
			*(this->protocol) << "\nNumber of homologous points: " 
				<< this->matches1.nRecords() << "\n\nTransformation without reduction point: \n\n"
				<< tred->getDescriptor("  ", 2, 7) << "\n\nTime: " 
							  << CCharString::getTimeString() << endl;

			delete tred;
		}
	}
	else if (this->protocol)
	{
		*(this->protocol) << "\n\nHierarchical matching was not successful\nTime: " 
							  << CCharString::getTimeString() << endl;

	}

	return success;
}


void CRegistrationMatcher::writeTrafoCovarToProt(const char *line, const Matrix &covar) const
{
	if (this->protocol)
	{
		*(this->protocol) << "\n\n" << line << "     sigma_0: ";
		this->protocol->precision(3);

		*(this->protocol) << this->sigma0 << "\n\n" << line << "     Covariance matrix: ";
		
		this->protocol->precision(8);

		for (int i = 0; i < covar.Nrows(); ++i)
		{
			for (int j = 0; j < covar.Ncols(); ++j)
			{
				this->protocol->width(15);
				*(this->protocol) << covar.element(i,j) << " ";	
			}
			*(this->protocol) << "\n                        " << line ;
		}

		*(this->protocol) << "\n\n" << line << "     Sigmas: ";
		for (int i = 0; i < covar.Nrows(); ++i)
		{
			this->protocol->width(9);
			*(this->protocol) << sqrt(covar.element(i,i)) << " ";	
		}

		*(this->protocol) << "\n" << endl;
	}
};

bool CRegistrationMatcher::LSM(const CXYPoint &xyPoint)
{
	bool ret = true;
	int width  = 42;
	int height = 42;
	int offsetX = (int) floor(xyPoint.x) - width / 2;
	int offsetY = (int) floor(xyPoint.y) - height / 2;
	if (offsetX < 0) offsetX = 0;
	if (offsetY < 0) offsetY = 0;
	if (offsetX + width >= this->inputimage2->getHugeImage()->getWidth())
		offsetX = this->inputimage2->getHugeImage()->getWidth() - width - 1;
	if (offsetY + height >= this->inputimage2->getHugeImage()->getHeight())
		offsetY = this->inputimage2->getHugeImage()->getHeight() - height - 1;

	this->buf1.set(offsetX, offsetY, width, height, 
		           this->inputimage1->getHugeImage()->getBands(),
				   this->inputimage1->getHugeImage()->getBpp(), true);


	this->inputimage1->getHugeImage()->getRect(buf1);
	buf1.SetToIntensity();
	int searchoffsetx, searchoffsety, searchHeight, searchWidth;
	this->trafo->transformWindow(offsetX, offsetY,  width, height,
								 searchoffsetx, searchoffsety, searchWidth, searchHeight,
								 this->inputimage2->getHugeImage()->getWidth(),  
								 this->inputimage2->getHugeImage()->getHeight(), 
								 width, height);
	if ((searchWidth < width) || (searchHeight < height)) ret = false;
	else
	{
		CMatchingParameters par = project.getMatchingParameter();
		par.setRhoMin(this->minCrossCorr);
		par.setIterationSteps(5);
		par.setPatchSize(width);

		this->buf2.set(searchoffsetx, searchoffsety, searchWidth, searchHeight, 
					   this->inputimage2->getHugeImage()->getBands(),
					   this->inputimage2->getHugeImage()->getBpp(), true);


		this->inputimage2->getHugeImage()->getRect(buf2);
		buf2.SetToIntensity();
	
		CLSM lsm(par, &buf1, xyPoint);
		lsm.addSearchImage(&buf2, this->trafo, 1);
		ret = lsm.LSM();
	}

	return ret;
};
