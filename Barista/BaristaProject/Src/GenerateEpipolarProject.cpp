#include "float.h"

#include "GenerateEpipolarProject.h"
#include "ImgBuf.h"
#include "FrameCamera.h"
#include "BaristaProject.h"


CGenerateEpipolarProject::CGenerateEpipolarProject(CVImage *left, CVImage *right, const CCharString &path) : 
    CGenerateEpipolar(left->getHugeImage(),   right->getHugeImage(), 
		              left->getFrameCamera(), right->getFrameCamera()), 
	leftEpipolarImage(0), rightEpipolarImage(0), leftCamera (0), rightCamera(0), 
	mounting(0), leftFrameCam(0), rightFrameCam(0), outputPath(path)
{

}
//================================================================================

CGenerateEpipolarProject::~CGenerateEpipolarProject()
{
	this->leftEpipolarImage = 0;
	this->rightEpipolarImage = 0;
	this->leftCamera = 0;
	this->rightCamera = 0;
	this->mounting = 0;
	this->leftFrameCam = 0;
	this->rightFrameCam = 0;
}

//================================================================================

bool CGenerateEpipolarProject::generateEpipolar()
{
	bool ret = this->initBaristaProject();

	if (ret) ret = this->initNewOrientations();
	if (ret) ret = this->generateImage(leftImage, leftEpipolarImage);
	if (ret) ret = this->generateImage(rightImage, rightEpipolarImage);
	
	if (!ret) this->cleanBaristaProject(); 

	return ret;
}; 

//================================================================================
	
bool CGenerateEpipolarProject::initBaristaProject()
{	
	bool ret = true;

	CFileName fnLeft  = this->leftImage->getFileNameChar();
	CFileName fnRight = this->rightImage->getFileNameChar();
	CCharString modelName = fnLeft.GetFileName() + "_" + fnRight.GetFileName();
	fnLeft  = modelName + "_left";
	fnRight = modelName + "_right";

	this->leftEpipolarImage  = project.addImage(fnLeft, false); 
	if (!leftEpipolarImage) ret = false;
	else
	{
		this->rightEpipolarImage = project.addImage(fnRight, false); 
		if (!rightEpipolarImage) ret = false;
		else
		{
			this->leftFrameCam  = this->leftEpipolarImage->getFrameCamera();
			this->rightFrameCam = this->rightEpipolarImage->getFrameCamera();
			this->leftCamera    = (CFrameCamera *) project.addCamera(eDigitalFrame);
			this->rightCamera   = (CFrameCamera *) project.addCamera(eDigitalFrame);
			this->mounting      = project.addCameraMounting();

			if ((!this->leftCamera)   || (!this->rightCamera) || (!this->mounting)) ret = false;
			else
			{
				CRollPitchYawRotation *prpyL = new CRollPitchYawRotation;
				ret = this->leftFrameCam->init(fnLeft + "_frameCam", 
					                           this->leftImage->getFrameCamera()->getCameraPosition(),
					                           prpyL, this->mounting, this->leftCamera);
				if (ret)
				{
					CRollPitchYawRotation *prpyR = new CRollPitchYawRotation;
					ret = this->rightFrameCam->init(fnRight + "_frameCam", 
						                            this->rightImage->getFrameCamera()->getCameraPosition(),
						                            prpyR, this->mounting, this->rightCamera);

					this->leftCamera->setName(fnLeft + "_cam");
					this->rightCamera->setName(fnRight + "_cam");
					this->mounting->setName(modelName + "_mount");		
					this->mounting->setRotation(this->leftImage->getFrameCamera()->getCameraMounting()->getRotation());
					CXYZPoint p(0.0,0.0,0.0);
					this->mounting->setShift(p);
					this->leftCamera->setIRP(this->leftImage->getFrameCamera()->getCamera()->getIRP());
					this->rightCamera->setIRP(this->rightImage->getFrameCamera()->getCamera()->getIRP());
					this->leftCamera->setIRPSymmetry(this->leftImage->getFrameCamera()->getCamera()->getIRPSymmetry());
					this->rightCamera->setIRPSymmetry(this->rightImage->getFrameCamera()->getCamera()->getIRPSymmetry());

					this->leftEpipolarImage->setCurrentSensorModel(eFRAMECAMERA);
					this->rightEpipolarImage->setCurrentSensorModel(eFRAMECAMERA);
					this->leftFrameCam->setRefSys(this->leftImage->getFrameCamera()->getRefSys());
					this->rightFrameCam->setRefSys(this->rightImage->getFrameCamera()->getRefSys());
				}
			}
		}
	}
	return ret;
};

//================================================================================

void CGenerateEpipolarProject::cleanBaristaProject()
{
	if (this->leftCamera)         project.deleteCamera(this->leftCamera);
	if (this->rightCamera)        project.deleteCamera(this->leftCamera);
	if (this->mounting)           project.deleteCameraMounting(this->mounting);
	if (this->leftEpipolarImage)  project.deleteImage(this->leftEpipolarImage);
	if (this->rightEpipolarImage) project.deleteImage(this->rightEpipolarImage);
};

//================================================================================

bool CGenerateEpipolarProject::initNewOrientations()
{
	bool ret = CGenerateEpipolar::initNewOrientations(*this->leftFrameCam, *this->rightFrameCam);

	CFrameCameraModel *pFRML = this->leftImage->getFrameCamera();
	CFrameCameraModel *pFRMR = this->rightImage->getFrameCamera();

	int npts = 4;
	vector<C2DPoint> pL(npts);
	vector<C2DPoint> pR(npts);
	vector<C2DPoint> pEpiL(npts);
	vector<C2DPoint> pEpiR(npts);
	vector<C3DPoint> P(npts);
	pL[0].x = 0.0;
	pL[0].y = 0.0;
	pL[1].x = double(this->leftImage->getHugeImage()->getWidth());
	pL[1].y = 0.0;
	pL[2].x = 0.0; 
	pL[2].y = double(this->leftImage->getHugeImage()->getHeight());
	pL[3].x = pL[1].x;
	pL[3].y = pL[2].y;
//	pL[4].x = 5191.0;
//	pL[4].y = 1685.0;

	C2DPoint pMinL(FLT_MAX, FLT_MAX), pMaxL(-FLT_MAX, -FLT_MAX);
	C2DPoint pMinR(FLT_MAX, FLT_MAX), pMaxR(-FLT_MAX, -FLT_MAX);

	for (unsigned int ind = 0; ind < P.size(); ++ind)
	{
		pFRML->transformObs2Obj(P[ind], pL[ind], project.getMeanHeight());
		pFRMR->transformObj2Obs(pR[ind], P[ind]);
		if (pR[ind].x < 0) pR[ind].x = 0.0;
		if (pR[ind].y < 0) pR[ind].y = 0.0;
		if (pR[ind].x >= this->rightImage->getHugeImage()->getWidth())  pR[ind].x = double(this->rightImage->getHugeImage()->getWidth())   - 1.0;
		if (pR[ind].y >= this->rightImage->getHugeImage()->getHeight()) pR[ind].y = double(this->rightImage->getHugeImage()->getHeight()) - 1.0;

		pFRMR->transformObs2Obj(P[ind], pR[ind], project.getMeanHeight());
		pFRML->transformObj2Obs(pL[ind], P[ind]);

		this->leftFrameCam->transformObj2Obs(pEpiL[ind], P[ind]);
		this->rightFrameCam->transformObj2Obs(pEpiR[ind], P[ind]);
		if (pEpiL[ind].x < pMinL.x) pMinL.x = pEpiL[ind].x;
		if (pEpiL[ind].y < pMinL.y) pMinL.y = pEpiL[ind].y;
		if (pEpiL[ind].x > pMaxL.x) pMaxL.x = pEpiL[ind].x;
		if (pEpiL[ind].y > pMaxL.y) pMaxL.y = pEpiL[ind].y;

		if (pEpiR[ind].x < pMinR.x) pMinR.x = pEpiR[ind].x;
		if (pEpiR[ind].y < pMinR.y) pMinR.y = pEpiR[ind].y;
		if (pEpiR[ind].x > pMaxR.x) pMaxR.x = pEpiR[ind].x;
		if (pEpiR[ind].y > pMaxR.y) pMaxR.y = pEpiR[ind].y;
	}


	pMinL.x = floor(pMinL.x + 0.5);
	pMinL.y = floor((pMinL.y + pMinR.y) * 0.5 + 0.5);
	pMaxL.x = floor(pMaxL.x + 0.5);
	pMaxL.y = floor((pMaxL.y + pMaxR.y) * 0.5 + 0.5);

	pMinR.x = floor(pMinR.x + 0.5);
	pMinR.y = pMinL.y;
	pMaxR.x = floor(pMaxR.x + 0.5);
	pMaxR.y = pMaxL.y;
	CXYZPoint IRP = this->leftCamera->getIRP();
	IRP.x -= pMinL.x;
	IRP.y -= pMinL.y;
	pMaxL -= pMinL;
	this->leftCamera->setIRP(IRP);
	this->leftCamera->setIRPSymmetry(IRP);
	IRP = this->rightCamera->getIRP();
	IRP.x -= pMinR.x;
	IRP.y -= pMinR.y;
	pMaxR -= pMinR;
	this->rightCamera->setIRP(IRP);
	this->rightCamera->setIRPSymmetry(IRP);
	CHugeImage *pHugLeftEpi = this->leftEpipolarImage->getHugeImage();
	CHugeImage *pHugLeft = this->leftImage->getHugeImage();
	CHugeImage *pHugRightEpi = this->rightEpipolarImage->getHugeImage();
	CHugeImage *pHugRight = this->rightImage->getHugeImage();
					
	CCharString cL = this->outputPath + this->leftEpipolarImage->getFileNameChar() + CCharString(".hug");
	CCharString cR = this->outputPath + this->rightEpipolarImage->getFileNameChar() + CCharString(".hug");

	pHugLeftEpi->setFileName(cL.GetChar());
	pHugLeftEpi->bpp    = pHugLeft->bpp;
	pHugLeftEpi->bands  = pHugLeft->bands;
	pHugLeftEpi->width  = long(pMaxL.x);
	pHugLeftEpi->height = long(pMaxL.y);
	this->leftCamera->setNCols(pHugLeftEpi->width);
	this->leftCamera->setNRows(pHugRightEpi->height);

	pHugRightEpi->setFileName(cR.GetChar());
	pHugRightEpi->bpp    = pHugRight->bpp;
	pHugRightEpi->bands  = pHugRight->bands;
	pHugRightEpi->width  = long(pMaxR.x);
	pHugRightEpi->height = long(pMaxR.y);
	this->rightCamera->setNCols(pHugRightEpi->width);
	this->rightCamera->setNRows(pHugRightEpi->height);

	return ret;
};

//================================================================================
	 
bool CGenerateEpipolarProject::generateImage(CVImage *imgOrig, CVImage *imgEpi)
{
	bool ret = true;

	CHugeImage *pHugOri = imgOrig->getHugeImage();
	CHugeImage *pHugEpi = imgEpi->getHugeImage();
	pHugOri->resetTiles();

	if (!pHugEpi->prepareWriting(pHugEpi->getFileNameChar(), pHugEpi->width, pHugEpi->height, pHugEpi->bpp, pHugEpi->bands))
	{
		ret = false;
	}
	else
	{
		CRotationMatrix rEpiToOri;
		C3DPoint irpEpi;
		C3DPoint irpOri;

		initEpiTrafPar(*imgOrig->getFrameCamera(), *imgEpi->getFrameCamera(), rEpiToOri, irpEpi, irpOri);
		pHugEpi->computeTotalTileCount(pHugEpi->height, pHugEpi->width);

		double nTiles = pHugEpi->tilesDown * pHugEpi->tilesAcross;

		CImageBuffer epiBuf(0,0, pHugEpi->tileWidth, pHugEpi->tileHeight, pHugEpi->bands, pHugEpi->bpp, true);

		for ( int tr = 0; tr < pHugEpi->tilesDown; tr++ )
		{
			for ( int tc = 0; tc < pHugEpi->tilesAcross; tc++ )
			{
				epiBuf.setOffset(tc * pHugEpi->tileWidth, tr * pHugEpi->tileHeight);
				
				bool ok = transformEpi(pHugOri, rEpiToOri, irpEpi, irpOri, epiBuf, false);

				if (!ok) epiBuf.setAll(0);

				pHugEpi->dumpTile(epiBuf);

				// update value for progress bar
				if ( this->listener != NULL )
				{
					double done = tr * pHugEpi->tilesAcross + tc + 1;
					double pvalue = 100.0 * done/nTiles;
					this->listener->setProgressValue(pvalue);
				}

			}
		}

	
		if ( this->listener != NULL )
		{
			this->listener->setProgressValue(100.0);
		}

		ret = pHugEpi->finishWriting();

		pHugOri->resetTiles();
	}
	return ret;
};

//================================================================================
