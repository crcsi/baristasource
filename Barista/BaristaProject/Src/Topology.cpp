#include "Topology.h"

#include "ObsPoint.h"

CTopology::CTopology(void): 
	from(0),to(0), lineWidth(1)
{
	color[0] = 0;
	color[1] = 255;
	color[2] = 0;
}

CTopology::CTopology(CObsPoint* from,CObsPoint* to, const int *col,unsigned int lineWidth):
	from(from), to(to), lineWidth(lineWidth)
{
	for (int i=0; i < 3; i++) color[i] = col[i];
}

CTopology::CTopology(const CTopology& topology): 
	from(topology.from),to(topology.to), lineWidth(topology.lineWidth)
{
		for (int i=0; i < 3; i++) color[i] = topology.color[i];
}


CTopology::~CTopology(void)
{
	this->from = 0;
	this->to = 0;
}
