/** 
 * @class CVImage
 * CVImage, class for image handling, with sensor models, points, lines
 *
 * @author Harry Hanley, Jochen Willneff
 * @version 1.0 
 * @date 28-aug-2005
 *
 */
#include <float.h>

#include "VImage.h"
#include "StructuredFileSectionArray.h"
#include "HugeImage.h"
#include "Affine.h"
#include "RPC.h"
#include "BundleModel.h"
#include "statistics.h"
#include "BaristaProject.h"
#include "Trans2DAffine.h"
#include "Trans2DConformal.h"
#include "ProtocolHandler.h"
#include "SystemUtilities.h"
#include "AdjustNormEqu.h"
#include "AlosReader.h"
#include "Trans3DFactory.h"

CVImage::CVImage(void): currentSensorModelType(eUNDEFINEDSENSOR),
	xypoints(MEASURED),projectedxypoints(PROJECTED), xy3DResiduals(OBJRESIDUALS),
	differencedxypoints(DIFFERENCED),residuals(RESIDUALS),fiducialMarkObs(FIDUCIALMARKS)
{
	obsPoints  = &this->xypoints;
	residualPoints = &this->residuals;
	this->fiducialMarkObs.setFileName("Fiducial marks");
	this->pushbroom.setParent(this);
	this->frameCamera.setParent(this);
}

CVImage::CVImage(CVImage* src): CObservationHandler(*src), currentSensorModelType(src->currentSensorModelType)
{
	this->name = src->name;
    this->xypoints.Copy(src->xypoints);
//maria
	this->xycontours.Copy(src->xycontours);
//end maria

 	obsPoints  = &this->xypoints;
	residualPoints = &this->residuals;
     
	this->pushbroom.setParent(this);
	this->frameCamera.setParent(this);
}

CVImage::CVImage(const CVImage& src) : CObservationHandler(src)
{
	this->name = src.name;
    this->xypoints.Copy(src.xypoints);
	this->xycontours.Copy(src.xycontours);

	obsPoints  = &this->xypoints;
	residualPoints = &this->residuals;

	this->pushbroom.setParent(this);
	this->frameCamera.setParent(this);
}

CVImage::~CVImage(void)
{
	
}

CHugeImage* CVImage::getHugeImage()
{
    return &this->hugeImage;
}
	
bool CVImage::getOptimalPyrLevel(float Z, double GSD, int &level, int &factor) const
{
	level = 0;
	factor = 1;

	if (!this->getCurrentSensorModel()) return false;

	float approxGSD = this->getApproxGSD(Z);
	if (approxGSD < FLT_EPSILON) return false;

	if (approxGSD < GSD)
	{
		float fact = GSD / approxGSD;
		int maxLevels = this->hugeImage.getNumberOfPyramidLevels() - 1;
		
		while ((fact > 2.0f) && (level < maxLevels))
		{
			level  += 1;
			factor *= 2;
			fact *= 0.5;
		}
	}
	return true;
};

/*
const CSensorModel* CVImage::getMonoplottingModel() 
{
	const CSensorModel* smodel = this->getCurrentSensorModel();
	if (! smodel )
		smodel = this->getTFW();
	
	if (!smodel->gethasValues())
		return 0;

	else
		return smodel;
}
*/

const CSensorModel* CVImage::getModel(enum eLSMTRAFOMODE trafomode) 
{
	const CSensorModel* smodel = this->getCurrentSensorModel();
	if (!smodel && trafomode == eLSMTFW)
	{
		smodel = this->getTFW();
	
		if (!smodel->gethasValues()) smodel = 0;
	}
	
	return smodel;
}

CLabel CVImage::getCurrentLabel()
{ 
	if ( this->currentLabel.GetLength() == 0)
	{
		this->currentLabel= "POINT";
		this->currentLabel += this->getXYPoints()->GetSize();
	}
	
	//while ( this->getXYPoints()->getPoint(this->currentLabel.GetChar()))
	//	this->incrementCurrentLabel();

	return this->currentLabel;
}

void CVImage::setCurrentLabel(CLabel newlabel)
{
	this->currentLabel = newlabel;
}

void CVImage::incrementCurrentLabel()
{
	this->currentLabel++;

	while ( this->getXYPoints()->getPoint(this->currentLabel.GetChar()))
		this->currentLabel++;
}
	
bool CVImage::hasSensorModel(bool considerTFW) const
{
	if (this->hasAffine())      return true;
	if (this->hasRPC())         return true;
	if (this->hasPushbroom())   return true;
	if (this->hasFrameCamera()) return true;
	if (considerTFW && this->hasTFW()) return true;
	
	return false;
};

void CVImage::resetSensorModel(CSensorModel *sensorMod)
{
	sensorMod->sethasValues(false);
	if (sensorMod == this->currentSensorModel)
	{
		if (this->rpc.gethasValues())
		{
			this->currentSensorModel = &this->rpc;
			this->currentSensorModelType = eRPC;
		}
		else if (this->affine.gethasValues())
		{
			this->currentSensorModel = &this->affine;
			this->currentSensorModelType = eAFFINE;
		}
		else if (this->pushbroom.gethasValues())
		{
			this->currentSensorModel = &this->pushbroom;
			this->currentSensorModelType = ePUSHBROOM;
		}
		else if (this->frameCamera.gethasValues())
		{
			this->currentSensorModel = &this->frameCamera;
			this->currentSensorModelType = eFRAMECAMERA;
		}
		else
		{
			this->currentSensorModelType = eUNDEFINEDSENSOR;
			this->currentSensorModel = 0;
		}	
	}
	this->changedData();
};

bool CVImage::setCurrentSensorModel (eImageSensorModel newmodel)
{
	this->rpc.setToCurrentSensorModel(false);
	this->affine.setToCurrentSensorModel(false);
	this->pushbroom.setToCurrentSensorModel(false);

	if ((newmodel == eRPC) && (this->rpc.gethasValues()))
	{
		this->currentSensorModel = &this->rpc;
		this->currentSensorModelType = newmodel;
		this->rpc.setToCurrentSensorModel(true);
	}
	else if ((newmodel == eAFFINE) && (this->affine.gethasValues()))
	{
		this->currentSensorModel = &this->affine;
		this->currentSensorModelType = newmodel;
		this->affine.setToCurrentSensorModel(true);
	}
	else if ((newmodel == ePUSHBROOM) && (this->pushbroom.gethasValues()))
	{
		this->currentSensorModel = &this->pushbroom;
		this->currentSensorModelType = newmodel;
		this->pushbroom.setToCurrentSensorModel(true);
	}
	else if ((newmodel == eFRAMECAMERA) && (this->frameCamera.gethasValues()))
	{
		this->currentSensorModel = &this->frameCamera;
		this->currentSensorModelType = newmodel;
		this->frameCamera.setToCurrentSensorModel(true);
	}
	else if ((newmodel == eTFW) && (this->tfw.gethasValues()))
	{
		this->currentSensorModel = &this->tfw;
		this->currentSensorModelType = newmodel;
	}
	else
	{
		this->currentSensorModelType = eUNDEFINEDSENSOR;
		this->currentSensorModel = 0;
	}

	this->changedData();

	return true;
};


/**
 * Serialized storage of a CVImage to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVImage::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
    CSerializable::serializeStore(pStructuredFileSection);
	
	CVBase::serializeStore(pStructuredFileSection);

    CToken* token = pStructuredFileSection->addToken();
    token->setValue("project image name", this->name.GetChar());

	token = pStructuredFileSection->addToken();
    token->setValue("current sensor", currentSensorModelType);

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->hugeImage.serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->xypoints.serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->projectedxypoints.serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->residuals.serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->differencedxypoints.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->xycontours.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->xyedges.serializeStore(child);

    child = pStructuredFileSection->addChild();
    this->rpc.serializeStore(child);

    child = pStructuredFileSection->addChild();
    this->originalrpc.serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->affine.serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->tfw.serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->ellipses.serializeStore(child);

	child = pStructuredFileSection->addChild();
	this->pushbroom.serializeStore(child);

#ifndef FINAL_RELEASE
	child = pStructuredFileSection->addChild();
	this->frameCamera.serializeStore(child);
	
	child = pStructuredFileSection->addChild();
	this->fiducialMarkObs.serializeStore(child);

#endif

}

/**
 * Restores a CVImage from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVImage::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
    bool firstRPC = true;
	CSerializable::serializeRestore(pStructuredFileSection);

	CVBase::serializeRestore(pStructuredFileSection);

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
        CCharString key = token->getKey();

		if (key.CompareNoCase("project image name"))
            this->name = token->getValue();
		else if (key.CompareNoCase("current sensor"))
		{
			int i = token->getIntValue();
		    currentSensorModelType = (eImageSensorModel) i;
		}
    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);

        if (child->getName().CompareNoCase("CHugeImage"))
        {
			this->hugeImage.serializeRestore(child);
        }
		else if (child->getName().CompareNoCase("CXYPointArray"))
        {
			CXYPointArray tmpArray;
			tmpArray.serializeRestore(child);
			if (tmpArray.getPointType() == MEASURED)
				this->xypoints = tmpArray;
			else if (tmpArray.getPointType() == PROJECTED)
				this->projectedxypoints = tmpArray;
			else if (tmpArray.getPointType() == DIFFERENCED)
				this->differencedxypoints = tmpArray;
			else if (tmpArray.getPointType() == RESIDUALS)
				this->residuals = tmpArray;
			else if (tmpArray.getPointType() == FIDUCIALMARKS)
				this->fiducialMarkObs = tmpArray;

       }
		else if (child->getName().CompareNoCase("CXYPolyLineArray"))
        {
			this->xyedges.serializeRestore(child);
        }
		else if (child->getName().CompareNoCase("CXYContourArray"))
        {
			this->xycontours.serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CRPC") && firstRPC)
        {
            this->rpc.serializeRestore(child);
            firstRPC = false;
        }
        else if (child->getName().CompareNoCase("CRPC"))
        {
            this->originalrpc.serializeRestore(child);

        }
        else if (child->getName().CompareNoCase("CAffine"))
        {
            this->affine.serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CTFW"))
        {
            this->tfw.serializeRestore(child);
        }
        else if (child->getName().CompareNoCase("CXYEllipseArray"))
        {
			this->ellipses.serializeRestore(child);
        }
		else if (child->getName().CompareNoCase("CPushbroomScanner"))
		{
			this->pushbroom.serializeRestore(child);
		}
#ifndef FINAL_RELEASE
		else if (child->getName().CompareNoCase("CFrameCameraModel"))
		{
			this->frameCamera.serializeRestore(child);
		}
#endif
        else
        {
			// just ignore the child
           // assert(false);
        }
    }

}

/**
 * reconnects pointers
 */
void CVImage::reconnectPointers()
{
    CSerializable::reconnectPointers();
	this->pushbroom.reconnectPointers();
	this->frameCamera.reconnectPointers();

	setCurrentSensorModel (currentSensorModelType);
}

/**
 * resets pointers
 */
void CVImage::resetPointers()
{
    CSerializable::resetPointers();
	this->pushbroom.resetPointers();
	this->frameCamera.resetPointers();
}


/**
 * resets the rpc's to the original values
 */
void CVImage::resetRPC()
{
    this->rpc.copy(this->originalrpc);
}

void CVImage::eraseRPC()
{
    this->rpc.resetSensor();
	this->originalrpc.resetSensor();
}

bool CVImage::initRPCs(const CFileName &filename)
{
	bool ret = this->rpc.read(filename.GetChar());
	this->rpc.setHeight(this->hugeImage.getHeight());
	this->rpc.setWidth(this->hugeImage.getWidth());
	this->originalrpc.copy(this->rpc);
	if (!this->affine.gethasValues()) this->setCurrentSensorModel(eRPC);

	this->changedData();
	return ret;
};

bool CVImage::initRPCs(const CRPC &rpc)
{
	this->rpc.copy(rpc);
	this->rpc.setHeight(this->hugeImage.getHeight());
	this->rpc.setWidth(this->hugeImage.getWidth());
	this->originalrpc.copy(this->rpc);
	if (!this->affine.gethasValues()) this->setCurrentSensorModel(eRPC);

	this->changedData();
	return true;
};

double CVImage::initRPCs(const CPushBroomScanner &pushbroom, float Zmin, float Zmax)
{
	bool hadRPCs = this->rpc.gethasValues();
	double rms = pushbroom.generateRPC(this->rpc, this->hugeImage.getWidth(), 
		                               this->hugeImage.getHeight(), Zmin, Zmax);

	if (rms >= 0.0)
	{
		if (!hadRPCs) this->originalrpc.copy(this->rpc);
		if (!this->currentSensorModel) this->setCurrentSensorModel(eRPC);
	}

	return rms;
}

bool CVImage::initAffine(const CFileName &filename, const CReferenceSystem &refsys)
{
    bool ret = this->affine.read(filename.GetChar());
	this->affine.setRefSys(refsys);
	if (!this->rpc.gethasValues()) this->setCurrentSensorModel(eAFFINE);
	return ret;
};

bool CVImage::initAffine(const CAffine &affine)
{
	this->affine.copy(affine);
	return true;
}

bool CVImage::initTFW(const CFileName &filename, const CReferenceSystem &refsys)
{
	CAlosReader reader;
	CFileName f(filename);
	bool ret = false;

	if (reader.readData(f))
	{
		CTFW *t = reader.getTFW();
		if (t) 
			ret = this->initTFW(*t);
		else
			ret = false;
	}
	else
	{
		ret = this->tfw.read(filename.GetChar());
		this->tfw.setRefSys(refsys);
	}

	CReferenceSystem refTFW = this->tfw.getRefSys();

	if ((refTFW.getCoordinateType() == eGEOGRAPHIC) || (refTFW.getCoordinateType() == eGEOCENTRIC))
	{
		C2DPoint obs(this->hugeImage.getWidth() / 2, this->hugeImage.getHeight() / 2);
		C3DPoint obj;
		this->tfw.transformObs2Obj(obj, obs, 0.0);

		if (refTFW.getCoordinateType() == eGEOCENTRIC) refTFW.geocentricToGeographic(obj, obj);
			
		int cM = int(refTFW.getCentralMeridian());
		while (cM < 0) cM += 360;
		int M = int(obj.y);
		while (M < 0) M += 360;
		int dM = abs(M - cM) % 360;

		if (dM > 6)
		{
			refTFW.setUTMZone(obj.x, obj.y);
			this->tfw.setRefSys(refTFW);
		}
	}

	return ret;
}

bool CVImage::initTFW(const CTFW &tfw)
{
	this->tfw.copy(tfw);
	return true;
}


bool  CVImage::initPushBroomScanner(const CFileName& filename,
									CCCDLine* camera, 
									CCameraMounting* cameraMounting, 
									CSensorModel* path,
									CSensorModel* attitudes,
									double satelliteHeight,
									double groundHeight,
									double groundTemperature,
									double firstLineTime,
									double timePerLine,
									int activeMountingShiftParameter,
									int activeMountingRotationParameter,
									int activeIrpParameter,
									int activeAdditionalParameter)
									//int type) // addition: int type
{
	if (!this->pushbroom.init(path,attitudes,cameraMounting,camera,filename,satelliteHeight,groundHeight,groundTemperature,firstLineTime,timePerLine))
		return false;

	if (!this->rpc.gethasValues() && !this->affine.gethasValues() && !this->frameCamera.gethasValues())
		this->setCurrentSensorModel(ePUSHBROOM);

	if (activeMountingRotationParameter == 0)
		this->pushbroom.deactivateParameterGroup(CSensorModel::mountRotPar);
	else
		this->pushbroom.activateParameter(CSensorModel::mountRotPar,activeMountingRotationParameter);

	if (activeMountingShiftParameter == 0)
		this->pushbroom.deactivateParameterGroup(CSensorModel::mountShiftPar);
	else
		this->pushbroom.activateParameter(CSensorModel::mountShiftPar,activeMountingShiftParameter);

	if (activeIrpParameter == 0)
		this->pushbroom.deactivateParameterGroup(CSensorModel::interiorPar);
	else
		this->pushbroom.activateParameter(CSensorModel::interiorPar,activeIrpParameter);

	if (activeAdditionalParameter == 0)
		this->pushbroom.deactivateParameterGroup(CSensorModel::addPar);
	else
		this->pushbroom.activateParameter(CSensorModel::addPar,activeAdditionalParameter);

	//addition>>
	//if (type = 2) // 2 = Alos, set the setChangeMountingForMerging flag of the image scanner into false for Alos, by default true
	//{
	//	this->getPushbroom()->setChangeMountingForMerging(false); 
	//}
	//else
	//{ //else true though by default true
	//	this->getPushbroom()->setChangeMountingForMerging(true);
	//}
	//<<addition
	return true;
}


bool CVImage::initGenericPushBroomScanner(const CFileName& filename,
									CCCDLine* camera, 
									double satelliteHeight,
									double groundHeight,
									double groundTemperature,
									double inTrackViewAngle,
									double crossTrackViewAngle,
									C3DPoint UL,
									C3DPoint UR,
									C3DPoint LL,
									C3DPoint LR)
{
	COrbitObservations* path = project.addOrbitObservation(Path);
	CCharString name = "Generic Orbit Path";
	project.getUniqueOrbitPathName(name);
	path->setName(name);
	
	COrbitObservations* attitudes = project.addOrbitObservation(Attitude);
	name = "Generic Orbit Attitudes";
	project.getUniqueOrbitPathName(name);
	attitudes->setName(name);

	CCameraMounting* cameraMounting = project.addCameraMounting();
	name = "Generic CameraMounting";
	project.getUniqueCameraMountingName(name);
	cameraMounting->setName(name);


	if (this->getHugeImage())
	{
		camera->setNRows(this->getHugeImage()->getHeight());
		camera->setNCols(this->getHugeImage()->getWidth());
	}

	if (!this->pushbroom.init(	path->getCurrentSensorModel(),
								attitudes->getCurrentSensorModel(),
								cameraMounting,
								camera,
								filename,
								satelliteHeight,
								groundHeight,
								groundTemperature,
								1.0/this->getHugeImage()->getHeight(),
								inTrackViewAngle,
								crossTrackViewAngle,
								UL,
								UR,
								LL,
								LR))
	{
		project.deleteOrbitObservation(path);
		project.deleteOrbitObservation(attitudes);
		project.deleteCameraMounting(cameraMounting);
		project.deleteCamera(camera);
		
		return false;
	}

	if (!this->rpc.gethasValues() && !this->affine.gethasValues() && !this->frameCamera.gethasValues())
		this->setCurrentSensorModel(ePUSHBROOM);

	return true;
}


bool CVImage::initFrameCamera(const CFileName& filename,
						CFrameCamera* camera,
						CCameraMounting* cameraMounting,
						CXYZPoint cameraPos,
						CRotationBase* cameraRotation)
{
	if (!this->frameCamera.init(filename,cameraPos,cameraRotation,cameraMounting,camera))
		return false;

	if (!this->rpc.gethasValues() && !this->affine.gethasValues() && !this->pushbroom.gethasValues())
		this->setCurrentSensorModel(eFRAMECAMERA);
	return true;
}



void CVImage::makeXYContoursActive()
{
	this->xycontours.setActive(true);

}
void CVImage::thinOutXYContours()
{
	if (this->xycontours.GetSize())
	{
		this->xyedges.RemoveAll();

		CLabel EdgeLabel("Line");
		EdgeLabel += 0; 
		double sigma0 = 0.5f;
		double maxDist = 1.0f;
		float probability = 0.95f;

		statistics::setNormalQuantil (probability);
		statistics::setChiSquareQuantils(probability);

		CXYContourArray localContours(xycontours);

		localContours.connectContours();

		for (long i = 0; i < localContours.GetSize(); ++i)
		{
			CXYContour *contour = localContours.GetAt(i);
			CXYPolyLine poly;
			poly.setLabel(EdgeLabel);
			++EdgeLabel;
			poly.initFromContour(contour, maxDist, sigma0);
			this->xyedges.add(poly);
		};
	};
};

void CVImage::regionGrowing(const C2DPoint &p)
{
	this->hugeImage.regionGrowing(p, &(this->xyedges));
};

float CVImage::getApproxGSD(float height) const
{
	float GSD = -1;

	if (this->rpc.gethasValues())
	{
		height = this->rpc.getOffset().getZ();
	}

	if (this->currentSensorModel || this->rpc.gethasValues() || this->affine.gethasValues() || this->tfw.gethasValues()
		|| (this->pushbroom.gethasValues()) || (this->frameCamera.gethasValues()))
	{
		CReferenceSystem sys;
		C2DPoint imgOffset = this->hugeImage.getCentre();
		C2DPoint imgOffsetX(imgOffset.x + 1, imgOffset.y);
		C2DPoint imgOffsetY(imgOffset.x    , imgOffset.y + 1);;
	
		C3DPoint offset, offsetX, offsetY;
		if (this->currentSensorModel)
		{
			sys = this->currentSensorModel->getRefSys();
			this->currentSensorModel->transformObs2Obj(offset,  imgOffset,  height);
			this->currentSensorModel->transformObs2Obj(offsetX, imgOffsetX, height);
			this->currentSensorModel->transformObs2Obj(offsetY, imgOffsetY, height);
		}
		else if (this->pushbroom.gethasValues())
		{
			sys = this->pushbroom.getRefSys();
			this->pushbroom.transformObs2Obj(offset,  imgOffset,  height);
			this->pushbroom.transformObs2Obj(offsetX, imgOffsetX, height);
			this->pushbroom.transformObs2Obj(offsetY, imgOffsetY, height);
		}
		else if (this->frameCamera.gethasValues())
		{
			sys = this->frameCamera.getRefSys();
			this->frameCamera.transformObs2Obj(offset,  imgOffset,  height);
			this->frameCamera.transformObs2Obj(offsetX, imgOffsetX, height);
			this->frameCamera.transformObs2Obj(offsetY, imgOffsetY, height);
		}
		else if (this->rpc.gethasValues())
		{
			sys = this->rpc.getRefSys();
			this->rpc.transformObs2Obj(offset,  imgOffset,  height);
			this->rpc.transformObs2Obj(offsetX, imgOffsetX, height);
			this->rpc.transformObs2Obj(offsetY, imgOffsetY, height);
		}
		else if (this->affine.gethasValues())
		{
			sys = this->affine.getRefSys();
			this->affine.transformObs2Obj(offset,  imgOffset,  height);
			this->affine.transformObs2Obj(offsetX, imgOffsetX, height);
			this->affine.transformObs2Obj(offsetY, imgOffsetY, height);
		}
		else
		{
			sys = this->tfw.getRefSys();
			this->tfw.transformObs2Obj(offset,  imgOffset,  height);
			this->tfw.transformObs2Obj(offsetX, imgOffsetX, height);
			this->tfw.transformObs2Obj(offsetY, imgOffsetY, height);
		}

		if (sys.getCoordinateType() == eGEOGRAPHIC)
		{
			sys.geographicToGrid(offset, offset);
			sys.geographicToGrid(offsetX, offsetX);
			sys.geographicToGrid(offsetY, offsetY);
		}
		else if (sys.getCoordinateType() == eGEOCENTRIC)
		{
			sys.geocentricToGrid(offset, offset);
			sys.geocentricToGrid(offsetX, offsetX);
			sys.geocentricToGrid(offsetY, offsetY);
		}

		offsetX.x -= offset.x;
		offsetX.y -= offset.y;
		offsetY.x -= offset.x;
		offsetY.y -= offset.y;
		float gsd1 = (float) sqrt(offsetX.x * offsetX.x + offsetX.y * offsetX.y);
		float gsd2 = (float) sqrt(offsetY.x * offsetY.x + offsetY.y * offsetY.y);
		if (gsd1 > gsd2) GSD = gsd1;
		else GSD = gsd2;
		GSD = 0.01f * floor(100.0 * GSD + 0.5);
	}

	return GSD;
};

bool CVImage::cropfromImage(CVImage* oriImage, double ulx, double uly, double width, double height,
		bool cropPoints, bool cropRPC, bool cropTFW, bool cropPushBroom, bool cropAffine)
{
	CHugeImage *newHug = this->getHugeImage();
	CHugeImage *oldHug = oriImage->getHugeImage();

	bool ret = newHug->cropImage(this->name.GetChar(), oldHug, ulx, uly, width, height);

	// crop image points
	if ( cropPoints ) this->cropPoints(oriImage, ulx, uly, width, height);

	// crop attached sensor models if corresponding bool = true;
	if ( cropRPC ) this->cropRPC(oriImage, ulx, uly, width, height);
	if ( cropTFW ) this->cropTFW(oriImage, ulx, uly, width, height);
	if ( cropPushBroom ) this->cropPushBroom(oriImage, ulx, uly, width, height);
	if ( cropAffine ) this->cropAffine(oriImage, ulx, uly, width, height);

	return true;
}


bool CVImage::cropTFW(CVImage* oriImage, double ulx, double uly, double width, double height)
{
	if ( oriImage->hasTFW() )
	{
		this->initTFW(*oriImage->getTFW());
		this->tfw.crop(ulx, uly, 0, 0);
		return true;
	}

	return false;
}

bool CVImage::cropRPC(CVImage* oriImage, double ulx, double uly, double width, double height)
{
	if ( oriImage->hasRPC() )
	{
		this->initRPCs(*oriImage->getRPC());
		this->getRPC()->crop(ulx, uly, width, height);
		
		this->originalrpc.copy(this->rpc);
		return true;
	}

	return false;
}

bool CVImage::cropPoints(CVImage* oriImage, double ulx, double uly, double width, double height)
{
	CXYPointArray points;

	oriImage->getXYPoints()->getbyArea(ulx, ulx + width, uly, uly + height, &points);

	if ( points.GetSize() > 0 )
	{
		int size = points.GetSize();

		for ( int i = 0; i < size; i++ )
		{
			CXYPoint croppoint( points.getAt(i));
			// int UL as cropping takes full pixel
			croppoint.x -= (int)ulx; 
			croppoint.y -= (int)uly;

			this->getXYPoints()->add(croppoint);
		}

		oriImage->getXYPoints()->setName("crop2DPoints");
		return true;
	}

	return false;
}

bool CVImage::cropPushBroom(CVImage* oriImage, double ulx, double uly, double width, double height)
{
	if ( oriImage->hasPushbroom() )
	{
		
		CPushBroomScanner* pbs = oriImage->getPushbroom();
		CCCDLine* oldCamera = pbs->getCamera();
		CCCDLine* newCamera= (CCCDLine*)project.addCamera(eCCD);
	
		*newCamera = *oldCamera;
		CCharString croppedCameraname(oldCamera->getName() +"_Cropped"); 
		project.getUniqueCameraName(croppedCameraname);
		newCamera->setName(croppedCameraname);

		CXYZPoint irp = oldCamera->getIRP();
		irp.x -= ulx;
		newCamera->setIRP(irp);

		double firstLineTime = pbs->getFirstLineTime();
		firstLineTime +=  uly * pbs->getTimePerLine();

		//addition>>
		//int satType;
		//if (pbs->getChangeMountingForMerging() == false)
		//	satType = 2; // 2 means Alos
		//else
		//	satType = 0; // 0 or 1 means not Alos
		//<<addition
		this->initPushBroomScanner("CroppedPushBroom",newCamera,
									pbs->getCameraMounting(),
									pbs->getOrbitPath(),
									pbs->getOrbitAttitudes(),
									pbs->getSatelliteHeight(),
									pbs->getGroundHeight(),
									pbs->getGroundTemperature(),
									firstLineTime,
									pbs->getTimePerLine(),
									pbs->getActiveParameter(CSensorModel::mountShiftPar),
									pbs->getActiveParameter(CSensorModel::mountRotPar),
									pbs->getActiveParameter(CSensorModel::interiorPar),
									pbs->getActiveParameter(CSensorModel::addPar));
									//satType); // addition: satType

		this->getPushbroom()->crop(ulx, uly, width, height);

		return true;
	}

	return false;
}

bool CVImage::cropAffine(CVImage* oriImage, double ulx, double uly, double width, double height)
{
	if ( oriImage->hasAffine() )
	{
		this->initAffine(*oriImage->getAffine());
		this->getAffine()->crop(ulx, uly, width, height);
		return true;
	}

	return false;
}


bool CVImage::computeInnerOrientation()
{
	if (!this->initFiducialMarks(this->frameCamera.getFiducialMarks()))
		return false;

	CTrans2D * trafo = NULL;
	CTrans2D * trafoInverse = NULL;

	if (this->fiducialMarkObs.GetSize() < 3)
		return false;
	else 
	{
		trafo = new CTrans2DAffine();
	}


	vector<CXYPoint *> frmVec, camVec;

	CXYPoint P0;

	for (int i=0; i< this->fiducialMarkObs.GetSize();i++)
	{
		CXYPoint* pointFrame = this->fiducialMarkObs.getAt(i);
		for (int k=0; k < this->frameCamera.getFiducialMarks()->GetSize(); k++)
		{
			CXYPoint* pointCamera = this->frameCamera.getFiducialMarks()->getAt(k);
			if (pointFrame->getLabel() == pointCamera->getLabel())
			{
				frmVec.push_back(pointFrame);
				camVec.push_back(pointCamera);
				P0.x += pointFrame->x;
				P0.y += pointFrame->y;

				break;
			}
		}
	}

	if (frmVec.size() < 3) return false;

	double sigma0 = 0.0;


	P0 *= 1.0 / (double) frmVec.size();

	CNormalEquationMatrix N(trafo->nParameters());
	CtVector m(trafo->nParameters());
	
	double vtv;
	unsigned long nobs;
	trafo->prepareAdjustment(N,m, vtv, nobs);
	trafo->setP0(P0);


	for (int i=0; i< frmVec.size();i++)
	{
		trafo->accumulate(*frmVec[i],*camVec[i],N,m,  vtv, nobs);
	}

	if (!trafo->solve(N, m, vtv, nobs, sigma0))
		return false;

	Matrix covar = *(trafo->getCovar());

	trafoInverse = trafo->clone();;
	if (!trafoInverse->invert())
		return false;

	this->frameCamera.setFiducialMarkTransformation(trafoInverse,trafo);

	CFileName fN(this->name);
	fN = fN.GetFileName() + "_IOR.PRT";
	fN = project.getFileName()->GetPath() + CSystemUtilities::dirDelimStr + fN;
	protHandler.open(CProtocolHandler::prt, fN, false);
	ostrstream oss;
	oss.setf(ios::fixed, ios::floatfield);
	oss << "Fiducial mark transformation\n============================\n\nImage: " << this->name 
		<< "\n\n  Statistics of residuals:\n  ========================\n\n   Fid        x        y      resx   resy  [pixels]\n";


	sigma0 = 0.0;
	CXYPoint camPoint;

	for (int i=0; i< frmVec.size();i++)
	{
		trafoInverse->transform (camPoint, *camVec[i]);
		double vx = camPoint.x - frmVec[i]->x;
		double vy = camPoint.y - frmVec[i]->y;

		sigma0 += vx * vx + vy * vy;

		oss.width(9); oss.precision(1);
		oss << frmVec[i]->getLabel().GetChar() << " ";
		oss.width(8);
		oss << frmVec[i]->x << " ";
		oss.width(8);
		oss << frmVec[i]->y << " ";
		oss.precision(2);
		oss.width(6);
		oss << vx<< " ";
		oss.width(6);
		oss << vy << "\n";
	}

	oss.precision(2);

	if (frmVec.size() > 3) sigma0 = sqrt(sigma0 / (2.0 * double(frmVec.size()) - 6));

	oss << "\n   Sigma0: +-" << sigma0 <<"  [pixels]\n\n Transformation Parameters\n =========================\n\n" << trafo->getDescriptor("", 3, 9) << "\n\n";

	protHandler.print(oss, CProtocolHandler::prt);
	protHandler.close(CProtocolHandler::prt);
	

	return true;

}

bool CVImage::initFiducialMarks(const CXYPointArray* fiducialMarks)
{
	if (!fiducialMarks)
		return false;

	
	int nFiducialMarks = fiducialMarks->GetSize();
	int nMeasuredPoints = this->xypoints.GetSize();
	int minFiducialMarks = 4;

	if (nFiducialMarks < minFiducialMarks || nMeasuredPoints < minFiducialMarks)
		return false;

	// check all measured points if they have the same label as one of the fiducial marks
	for (int i=nMeasuredPoints-1; i >= 0; i--) 
	{
		for (int k=0; k < nFiducialMarks; k++)
		{
			// copy the measured point into the fiducial mark array
			if (this->xypoints.getAt(i)->getLabel() == fiducialMarks->getAt(k)->getLabel())
			{
				
				// if we have the label already, replace the old measurement
				int m=0;
				for (m; m < this->fiducialMarkObs.GetSize(); m++)
					if (this->xypoints.getAt(i)->getLabel() == this->fiducialMarkObs.getAt(m)->getLabel())
						break;
				
				if (m == this->fiducialMarkObs.GetSize())
					this->fiducialMarkObs.add(this->xypoints.getAt(i)); // add to the array
				else
					*this->fiducialMarkObs.getAt(m) = *this->xypoints.getAt(i);  // replace the existing point
					
				// finaly, delete frome the measured points
				this->xypoints.RemoveAt(i,1);
				break;
			}
		}
	}

	if (this->fiducialMarkObs.GetSize() >= minFiducialMarks)
		return true;
	else
		return false;

}

//================================================================================

void CVImage::goingToBeDeleted()
{
	this->informListeners_elementDeleted();
}

//================================================================================
	
bool CVImage::getFootPrint(C3DPoint &upperLeftObj,  C3DPoint &upperRightObj, 
						   C3DPoint &lowerRightObj, C3DPoint &lowerLeftObj, 
						   double Z, CReferenceSystem *targetRefsys) const
{
	bool ret = true;
	CSensorModel *mod = this->currentSensorModel;
	CReferenceSystem ref;
	if (!mod && this->tfw.gethasValues())
	{
		C2DPoint pImg(0.0, 0.0);
		this->tfw.transformObs2Obj(upperLeftObj, pImg, Z);

		pImg.x = this->hugeImage.getWidth() - 1;
		this->tfw.transformObs2Obj(upperRightObj, pImg, Z);

		pImg.y = this->hugeImage.getHeight() - 1;
		this->tfw.transformObs2Obj(lowerRightObj, pImg, Z);

		pImg.x = 0;
		this->tfw.transformObs2Obj(lowerLeftObj, pImg, Z);

		ref = this->tfw.getRefSys();
	}
	else if (!mod) ret = false;
	else
	{
		C2DPoint pImg(0.0, 0.0);
		mod->transformObs2Obj(upperLeftObj, pImg, Z);

		pImg.x = this->hugeImage.getWidth() - 1;
		mod->transformObs2Obj(upperRightObj, pImg, Z);

		pImg.y = this->hugeImage.getHeight() - 1;
		mod->transformObs2Obj(lowerRightObj, pImg, Z);

		pImg.x = 0;
		mod->transformObs2Obj(lowerLeftObj, pImg, Z);
		ref = mod->getRefSys();
	}

	if (ret && targetRefsys) 
	{
		CTrans3D *trafo = CTrans3DFactory::initTransformation(ref, *targetRefsys);
		if (!trafo) ret = false;
		else
		{
			trafo->transform(upperLeftObj,  upperLeftObj);
			trafo->transform(upperRightObj, upperRightObj);
			trafo->transform(lowerRightObj, lowerRightObj);
			trafo->transform(lowerLeftObj,  lowerLeftObj);
		}
	}

	return ret;
};

//================================================================================
	
bool CVImage::getFootPrint(C2DPoint &upperLeftObj,  C2DPoint &upperRightObj, 
						   C2DPoint &lowerRightObj, C2DPoint &lowerLeftObj,
						   double Z, CReferenceSystem *targetRefsys) const
{
	C3DPoint upperLeftObj3D, upperRightObj3D, lowerRightObj3D, lowerLeftObj3D;

	bool ret = this->getFootPrint(upperLeftObj3D, upperRightObj3D, lowerRightObj3D, lowerLeftObj3D, Z, targetRefsys);
	if (ret)
	{
		upperLeftObj.x  = upperLeftObj3D.x;
		upperLeftObj.y  = upperLeftObj3D.y;  
		upperRightObj.x = upperRightObj3D.x;
		upperRightObj.y = upperRightObj3D.y;
		lowerRightObj.x = lowerRightObj3D.x;
		lowerRightObj.y = lowerRightObj3D.y;
		lowerLeftObj.x  = lowerLeftObj3D.x;
		lowerLeftObj.y  = lowerLeftObj3D.y;
	}

	return ret;
};

//================================================================================
	
bool CVImage::getFootPrintMinMax(C3DPoint &pMinObj, C3DPoint &pMaxObj, 
								 double Z, CReferenceSystem *targetRefsys) const
{
	vector<C3DPoint> cornerPoints(4);

	bool ret = this->getFootPrint(cornerPoints[0], cornerPoints[1], cornerPoints[2], cornerPoints[3], Z, targetRefsys);
	if (ret)
	{
		pMinObj = cornerPoints[0];
		pMaxObj = cornerPoints[0];

		for (unsigned int i = 1; i < cornerPoints.size(); ++i)
		{
			double x = cornerPoints[i].x;
			double y = cornerPoints[i].y;
			double z = cornerPoints[i].z;
			if (x < pMinObj.x) pMinObj.x = x;
			if (y < pMinObj.y) pMinObj.y = y;
			if (z < pMinObj.z) pMinObj.z = z;
			if (x > pMaxObj.x) pMaxObj.x = x;
			if (y > pMaxObj.y) pMaxObj.y = y;
			if (z > pMaxObj.z) pMaxObj.z = z;
		}

	}

	return ret;
};

//================================================================================
	
bool CVImage::getFootPrintMinMax(C2DPoint &pMinObj, C2DPoint &pMaxObj,
								 double Z, CReferenceSystem *targetRefsys) const
{
	vector<C2DPoint> cornerPoints(4);

	bool ret = this->getFootPrint(cornerPoints[0], cornerPoints[1], cornerPoints[2], cornerPoints[3], Z, targetRefsys);
	if (ret)
	{
		pMinObj = cornerPoints[0];
		pMaxObj = cornerPoints[0];

		for (unsigned int i = 1; i < cornerPoints.size(); ++i)
		{
			double x = cornerPoints[i].x;
			double y = cornerPoints[i].y;
			if (x < pMinObj.x) pMinObj.x = x;
			if (y < pMinObj.y) pMinObj.y = y;
			if (x > pMaxObj.x) pMaxObj.x = x;
			if (y > pMaxObj.y) pMaxObj.y = y;
		}

	}

	return ret;
};
	
//================================================================================
	
bool CVImage::getOverlapArea(const C3DPoint &pMinObj, const C3DPoint &pMaxObj,
							 C2DPoint &pMinImg, C2DPoint &pMaxImg) const
{
	if (this->hugeImage.getWidth()  < 1)  return false;
	if (this->hugeImage.getHeight() < 1)  return false;
	if (!this->currentSensorModel)        return false;

	vector<C3DPoint> corners(8);
	vector<C2DPoint> cornersImage(8);
	pMinImg.x = pMinImg.y =  FLT_MAX;
	pMaxImg.x = pMaxImg.y = -FLT_MAX;

	corners[0] = corners[4] = pMinObj; corners[4].z = pMaxObj.z;
	corners[2] = corners[6] = pMaxObj; corners[2].z = pMinObj.z;

	corners[1].x = corners[5].x = pMinObj.x; 
	corners[1].y = corners[5].y = pMaxObj.y; 
	corners[1].z = pMinObj.z;
	corners[5].z = pMaxObj.z;
		
	corners[3].x = corners[7].x = pMaxObj.x; 
	corners[3].y = corners[7].y = pMinObj.y; 
	corners[3].z = pMinObj.z;
	corners[7].z = pMaxObj.z;

	for (unsigned int ind = 0; ind < corners.size(); ++ind)
	{
		this->currentSensorModel->transformObj2Obs(cornersImage[ind], corners[ind]);
		if (cornersImage[ind].x < pMinImg.x) pMinImg.x = cornersImage[ind].x;
		if (cornersImage[ind].y < pMinImg.y) pMinImg.y = cornersImage[ind].y;
		if (cornersImage[ind].x > pMaxImg.x) pMaxImg.x = cornersImage[ind].x;
		if (cornersImage[ind].y > pMaxImg.y) pMaxImg.y = cornersImage[ind].y;
	}

	if (pMinImg.x < 0) pMinImg.x = 0;
	if (pMinImg.y < 0) pMinImg.y = 0;
	if (pMaxImg.x < 0) pMaxImg.x = 0;
	if (pMaxImg.y < 0) pMaxImg.y = 0;

	if (pMinImg.x >= this->hugeImage.getWidth())  pMinImg.x = this->hugeImage.getWidth()  - 1;
	if (pMinImg.y >= this->hugeImage.getHeight()) pMinImg.y = this->hugeImage.getHeight() - 1;
	if (pMaxImg.x >= this->hugeImage.getWidth())  pMaxImg.x = this->hugeImage.getWidth()  - 1;
	if (pMaxImg.y >= this->hugeImage.getHeight()) pMaxImg.y = this->hugeImage.getHeight() - 1;
	

	if (pMinImg.x >= pMaxImg.x) return false;
	if (pMinImg.y >= pMaxImg.y) return false;

	return true;
};

//================================================================================

bool CVImage::hasValidHugeImage()
{
	return this->hugeImage.open(this->hugeImage.getFileName()->GetChar(), false);
}

//================================================================================
