#include "GCPDataBasePoint.h"



CGCPDataBasePoint& CGCPDataBasePoint::operator = (const CGCPDataBasePoint & point)
{

	CXYZPoint::operator =(point);
	this->gcpPoint2DArray = point.gcpPoint2DArray;
	this->type = point.type;
	this->date = point.date;	
	this->fromGPS = point.fromGPS;	

	return *this;
}

void CGCPDataBasePoint::setCoordinateType(eCoordinateType ctype)  
{//comm
	this->type = ctype;
}

void CGCPDataBasePoint::setGCPPoint2DArray(vector<CGCPData2DPoint> &Points)  
{
	//following 
	//this->gcpPoint2DArray = Points;

	// delete all previous points
	this->gcpPoint2DArray.clear();

	//now copy all elements
	vector<CGCPData2DPoint>::iterator it;
	for (it = Points.begin() ; it < Points.end(); it++ )
	{
		this->gcpPoint2DArray.push_back(*it);
	}
}

 

void CGCPDataBasePoint::setDate(CCharString dt)  
{
	this->date = dt;
}

void CGCPDataBasePoint::setFromGPS(bool gps) 
{
	this->fromGPS = gps;
}

void CGCPDataBasePoint::set2DPoint(CGCPData2DPoint pt)
{
	this->gcpPoint2DArray.push_back(pt);
	//this->gcp2dPoint=pt;

/*
	this->gcp2dPoint.setDate(pt.getAcquiredDate());
	this->gcp2dPoint.setQualityOfDefinition(pt.getQualityOfDefinition()); // for ALOS PRISM
	this->gcp2dPoint.setPlatform(pt.getPlatform());
	this->gcp2dPoint.setSensor(pt.getSensor());
	this->gcp2dPoint.setDateCreation(pt.getDateCreation());
	this->gcp2dPoint.setViewAngle(pt.getViewAngle());
	this->gcp2dPoint.setAccuracy(pt.getAccuracy());
	this->gcp2dPoint.setAccuracyZ(pt.getAccuracyZ());
	this->gcp2dPoint.setUnit(pt.getUnit());
	this->gcp2dPoint.setSourceOfHeight(pt.getSourceOfHeight());
	this->gcp2dPoint.sethDatum(pt.gethDatum());
	this->gcp2dPoint.setvDatum(pt.getvDatum());
	this->gcp2dPoint.setProjection(pt.getProjection());
	this->gcp2dPoint.setZone(pt.getZone());
	this->gcp2dPoint.setepsgCode(pt.getepsgCode());
	this->gcp2dPoint.setchipSize(pt.getchipSize());
	this->gcp2dPoint.setdataProvider(pt.getdataProvider());
	this->gcp2dPoint.setLicense(pt.getLicense());
	this->gcp2dPoint.setComments(pt.getComments());
	this->gcp2dPoint.setnBands(pt.getnBands());
	this->gcp2dPoint.setisMonoplotted(pt.getisMonoplotted());
*/
}