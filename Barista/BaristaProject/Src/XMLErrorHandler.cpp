#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/sax/SAXException.hpp>

#include "XMLErrorHandler.h"

CXMLErrorHandler::CXMLErrorHandler() : recordData(false),
	warnings(dummyWarnings),errors(dummyErrors),fatalErrors(dummyFatalErrors)
{
	
}


CXMLErrorHandler::CXMLErrorHandler(vector<CCharString>& warnings,vector<CCharString>& errors,vector<CCharString>& fatalErrors) :
	warnings(warnings),errors(errors),fatalErrors(fatalErrors),recordData(true)
{
	this->warnings.clear();
	this->errors.clear();
	this->fatalErrors.clear();
}

CXMLErrorHandler::~CXMLErrorHandler(void)
{
}



void CXMLErrorHandler::error(const SAXParseException& e)
{
	if (this->recordData)
	{
		CCharString message = XMLString::transcode(e.getMessage());
		CCharString line;
		line.Format("%ld",e.getLineNumber());
		
		CCharString col;
		col.Format("%ld",e.getColumnNumber());

		CCharString fullMessage;

		fullMessage.Format("error       (line: %4s, column: %4s): %s",line.GetChar(),col.GetChar(),message.GetChar());

		this->errors.push_back(fullMessage);
	}
}

void CXMLErrorHandler::fatalError(const SAXParseException& e)
{
	if (this->recordData)
	{
		CCharString message = XMLString::transcode(e.getMessage());
		CCharString line;
		line.Format("%ld",e.getLineNumber());
		
		CCharString col;
		col.Format("%ld",e.getColumnNumber());

		CCharString fullMessage;

		fullMessage.Format("fatal error (line: %4s, column: %4s): %s",line.GetChar(),col.GetChar(),message.GetChar());

		this->fatalErrors.push_back(fullMessage);
	}
}

void CXMLErrorHandler::warning(const SAXParseException& e)
{
	if (this->recordData)
	{
		CCharString message = XMLString::transcode(e.getMessage());
		CCharString line;
		line.Format("%ld",e.getLineNumber());
		
		CCharString col;
		col.Format("%ld",e.getColumnNumber());

		CCharString fullMessage;

		fullMessage.Format("warning     (line: %4s, column: %4s): %s",line.GetChar(),col.GetChar(),message.GetChar());

		this->warnings.push_back(fullMessage);
	}
}
