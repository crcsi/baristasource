#include "XMLDataHandler.h"

CXMLDataHandler::CXMLDataHandler() : intReader(NULL),doubleReader(NULL),stringReader(NULL),
	read(CXMLDataHandler::noRead),tagDepth(0),xmlTag(15)
{
}

CXMLDataHandler::~CXMLDataHandler(void)
{
}

void CXMLDataHandler::characters(const   XMLCh* const chars , const XMLSize_t length)
{
	if (this->read == noRead)
		return;
	else if( this->read == readInt)
		*this->intReader = atoi(XMLString::transcode(chars));
	else if( this->read == readDouble)
		*this->doubleReader = atof(XMLString::transcode(chars));
	else if (this->read == readString)
		(*this->stringReader) = XMLString::transcode(chars);
	this->read = noRead;
}

void CXMLDataHandler::ignorableWhitespace(const  XMLCh* const chars, const unsigned int length)
{
}

