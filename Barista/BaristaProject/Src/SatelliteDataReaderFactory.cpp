#include "SatelliteDataReaderFactory.h"
#include "QuickbirdReader.h"
#include "Spot5Reader.h"
#include "AlosReader.h"
#include "Formosat2Reader.h"
#include "TheosReader.h"
#include "GeoEye1Reader.h"

CSatelliteDataReader* CSatelliteDataReaderFactory::initSatelliteReader(SatelliteType type)
{
	if (type == Spot5)
		return new CSpot5Reader();
	else if (type == Quickbird)
		return new CQuickbirdReader();
	else if (type == Alos)
		return new CAlosReader();
	else if (type == Formosat2)
		return new CFormosat2Reader();
	else if (type == Theos)
		return new CTheosReader();
	else if (type == GeoEye1)
		return new CGeoEye1Reader();
	else
		return 0;
}