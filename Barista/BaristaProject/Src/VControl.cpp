/** 
 * @class CVControl
 * CVControl, class for handling of control points
 *
 * @author Franz Rottensteiner
 * @version 1.0 
 * @date 05-oct-2006
 *
 */

#include "VControl.h"
#include "ControlPoints.h"


CVControl::CVControl(CXYZPointArray *pointArray,CXYZPointArray *residualArray) : 
	pointArray(pointArray)
{
	this->obsPoints = pointArray;
	this->residualPoints = residualArray;
	this->currentSensorModel = new CControlPointModel();
	this->currentSensorModel->setRefSys(*(pointArray->getReferenceSystem()));
}

CVControl::~CVControl(void)
{
	obsPoints = 0;
	if (currentSensorModel) delete currentSensorModel;
	currentSensorModel = 0;
}

const char *CVControl::getFileNameChar() const
{
	if (obsPoints) return obsPoints->getFileNameChar();

	return "";
};

CXYZPointArray* CVControl::getXYZPointArray() const
{
	return this->pointArray;
}

CXYZPointArray* CVControl::getXYZResidualPointArray() const
{
	return (CXYZPointArray*)this->getResidualPoints();
}