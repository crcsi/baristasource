#include "Formosat2XMLDataHandler.h"

#include <xercesc/sax/AttributeList.hpp>

#include "FormosatData.h"
#include "XYZPoint.h"

CFormosat2XMLDataHandler::CFormosat2XMLDataHandler(CFormosatData& dataStorage) : dataStorage(dataStorage)
{
	this->readMetadata_Id = false;
	this->readDataset_Id = false;
	this->readDataset_Frame = false;
		this->readVertex = false;
		this->readScene_Center = false;
	this->readRaster_Frame = false;
    this->readRaster_CS = false;
	this->readProduction = false;
	this->readProduction_Facility = false;
	this->readRaster_Dimension = false;
	this->readCoordinate_Reference_System = false;
		this->readHorizontal_CS = false;
	this->readRaster_Encoding = false;
	this->readData_Processing = false;
		this->readProcessing_Options = false;
			this->readCorrection_Algorithm = false;
	this->readData_Access = false;
		this->readData_File_Format = false;
			this->readData_File = false;
	this->readImage_Display = false;
		this->readBand_Display_Order = false;
	this->readDataset_Sources = false;
		this->readSource_Information = false;
			this->readScene_Source = false;
	this->readImage_Interpretation = false;
		this->readSpectral_Band_Info = false;
			this->readGain_List = false;
				this->readGain = false;
	this->readData_Strip = false;
		this->readIdentification = false;
		this->readEphemeris = false;
			this->readRaw_Ephemeris = false;
				this->readPoint_List = false;
					this->readPoint = false;
						this->readLocation = false;
						this->readVelocity = false;
			this->readCorrected_Ephemeris = false;
				this->readCorrected_Point_List = false;
					this->readCorrected_Point = false;
		this->readAttitudes = false;
			this->readRaw_Attitudes = false;
				this->readQuaternion_List = false;
					this->readQuaternion = false;
			this->readCorrected_Attitudes = false;
				this->readECF_Attitude = false;
					this->readAngle_List = false;
					this->readAngle = false;
		this->readSensor_Configuration = false;
			this->readInstrument_Look_Angles_List = false;
				this->readInstrument_Look_Angles = false;
						this->readPolynomial_Look_Angles = false;
			this->readInstrument_Biases = false;
		this->readFrame_Counters = false;
			this->readBand_Counters = false;
		this->readTime_Stamp = false;
		this->readSensor_Calibration = false;
			this->readCalibration = false;
				this->readBand_Parameters = false;
					this->readGain_Section_List = false;
						this->readGain_Section = false;
							this->readPixelParameters = false;
								this->readCells = false;
									this->readCell = false;

	this->hasVelocity = false;
}

CFormosat2XMLDataHandler::~CFormosat2XMLDataHandler(void)
{
}


void CFormosat2XMLDataHandler::endElement(const XMLCh* const name )
{
	this->tagDepth--;

	switch (this->tagDepth)
	{
	case 1:	{
				this->readMetadata_Id = false;
				this->readDataset_Id = false;
				this->readDataset_Frame = false;
				this->readRaster_Frame = false;
				this->readRaster_CS = false;
				this->readProduction = false;
				if (this->readRaster_Dimension)
				{
					this->readRaster_Dimension = false;
					this->dataStorage.lookAngles.resize(this->dataStorage.nbands);	
				}
				this->readCoordinate_Reference_System = false;
				this->readRaster_Encoding = false;
				this->readData_Processing = false;
				this->readData_Access = false;
				this->readImage_Display = false;
				this->readDataset_Sources = false;
				this->readImage_Interpretation = false;
				this->readData_Strip = false;
				break;
			}
	case 2: {
				this->readProduction_Facility = false;
				this->readHorizontal_CS = false;
				this->readProcessing_Options = false;
				this->readData_File_Format = false;
				this->readBand_Display_Order = false;
				this->readSource_Information = false;
				this->readSpectral_Band_Info = false;
				this->readIdentification = false;
				this->readEphemeris = false;
				this->readFrame_Counters = false;
				this->readProduction_Facility = false;
				this->readSensor_Calibration = false;
				this->readAttitudes = false;

				if (this->readVertex)
				{
					this->readVertex = false;
					CObsPoint* p= this->dataStorage.rasterFramePoints.Add();
					(*p)[0] = this->x;
					(*p)[1] = this->y;
					(*p)[2] = this->z;
					(*p)[3] = this->vx;
				}
				if (this->readScene_Center)
				{
					this->readScene_Center = false;
					this->dataStorage.rasterSceneCenter[0] = this->x;
					this->dataStorage.rasterSceneCenter[1] = this->y;
					this->dataStorage.rasterSceneCenter[2] = this->z;
					this->dataStorage.rasterSceneCenter[3] = this->vx;
				}
				if (this->readTime_Stamp)
				{
					this->readTime_Stamp = false;
					int day, month, year,hour, min;
					double sec;				
					sscanf(this->time.GetChar(),"%4d-%2d-%2d %2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					this->dataStorage.referenceTime = hour*3600 + min*60 + sec;
				}

				break;
			}
	case 3: {
				this->readCorrection_Algorithm = false;
				this->readData_File = false;
				this->readScene_Source = false;
				this->readGain_List = false;
				this->readRaw_Ephemeris = false;
				this->readCorrected_Ephemeris = false;
				this->readRaw_Attitudes = false;
				this->readInstrument_Look_Angles_List = false;
				this->readBand_Counters = false;
				this->readCalibration = false;

				if (this->readInstrument_Biases)
				{
					this->readInstrument_Biases = false;
					this->dataStorage.biases[0] = this->x;
					this->dataStorage.biases[1] = this->y;
					this->dataStorage.biases[2] = this->z;
				}

				if (this->readCorrected_Attitudes)
				{
					this->readCorrected_Attitudes = false;
				}
				break;
			}
	case 4: {
				this->readGain = false;
				this->readPoint_List = false;
				this->readCorrected_Point_List = false;
				this->readQuaternion_List = false;				
				this->readECF_Attitude = false;
				this->readInstrument_Look_Angles = false;
				this->readBand_Parameters = false;
				break;
			}
	case 5: {
				this->readPoint = false;

				if (this->readCorrected_Point)
				{
					this->readCorrected_Point = false;
					CXYZOrbitPoint *point = this->dataStorage.tmpOrbitPoints.add(); 
					int day, month, year,hour, min;
					double sec;	
					CXYZPoint pos(this->x,this->y,this->z);
					CXYZPoint vel(this->vx,this->vy,this->vz);
					sscanf(this->time.GetChar(),"%4d-%2d-%2d %2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					point->setOrbitPoint(pos,vel,day,month,year,hour,min,sec);
					point->setHasVelocity(this->hasVelocity);

					this->hasVelocity = false;								
				}

				if (this->readQuaternion)
				{
					this->readQuaternion = false;
					CObsPoint* p = this->dataStorage.quaternions.Add();  
					int day, month, year,hour, min;
					double sec;	
					sscanf(this->time.GetChar(),"%4d-%2d-%2d %2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					double q_time = hour*3600 + min*60 + sec;
					(*p)[0] = this->q0;	
					(*p)[1] = this->q1;
					(*p)[2] = this->q2; 
					(*p)[3] = this->q3; 
					p->setCovarElement(0,0,0.0001);
					p->setCovarElement(1,1,0.0001);
					p->setCovarElement(2,2,0.0001);
					p->setCovarElement(3,3,0.0001);

					p->dot = q_time; 
				}

	
				this->readGain_Section_List = false;
				break;
			}
	case 6: {
				if (this->readAngle)
				{
					this->readAngle = false;
					CObsPoint* p  = this->dataStorage.correctedAngles.Add();
					int day, month, year,hour, min;
					double sec;	
					sscanf(this->time.GetChar(),"%4d-%2d-%2d %2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
					(*p)[0] = this->x;
					(*p)[1] = this->y;
					(*p)[2] = this->z;
					p->dot = hour*3600 + min*60 + sec;

				}
				this->readLocation = false;
				this->readVelocity = false;
				this->readGain_Section = false;
			}

	}

}

void CFormosat2XMLDataHandler::startElement(const XMLCh* const name , AttributeList&  attributes)
{
	this->xmlTag[this->tagDepth] = XMLString::transcode(name);

	switch (this->tagDepth)
	{
	case 1:		{this->read = noRead;
				if (this->xmlTag[1].CompareNoCase("Metadata_Id"))
					this->readMetadata_Id = true;
				else if (this->xmlTag[1].CompareNoCase("Dataset_Id"))
					this->readDataset_Id = true;
				/*else if (this->xmlTag[1].CompareNoCase("Dataset_Frame"))
					this->readDataset_Frame = true;*/
				else if (this->xmlTag[1].CompareNoCase("Raster_Frame"))
					this->readRaster_Frame = true;
				else if (this->xmlTag[1].CompareNoCase("Raster_CS"))
					this->readRaster_CS = true;
				else if (this->xmlTag[1].CompareNoCase("Production"))
					this->readProduction = true;
				else if (this->xmlTag[1].CompareNoCase("Raster_Dimensions"))
					this->readRaster_Dimension = true;
				else if (this->xmlTag[1].CompareNoCase("Coordinate_Reference_System"))
					this->readCoordinate_Reference_System = true;
				else if (this->xmlTag[1].CompareNoCase("Raster_Encoding"))
					this->readRaster_Encoding = true;
				else if (this->xmlTag[1].CompareNoCase("Data_Processing"))
					this->readData_Processing = true;
				else if (this->xmlTag[1].CompareNoCase("Data_Access"))
					this->readData_Access = true;
				else if (this->xmlTag[1].CompareNoCase("Image_Display"))
					this->readImage_Display = true;
				else if (this->xmlTag[1].CompareNoCase("Dataset_Sourse"))
					this->readDataset_Sources = true;
				else if (this->xmlTag[1].CompareNoCase("Image_Interpretation"))
					this->readImage_Interpretation = true;
				else if (this->xmlTag[1].CompareNoCase("Data_Strip"))
					this->readData_Strip = true;
				break;
				}
	case 2:		{
					if (this->readMetadata_Id)
					{
						this->read = readString;
						if (this->xmlTag[2].CompareNoCase("METADATA_FORMAT"))
						{
							this->dataStorage.metadataVersion = XMLString::transcode(attributes.getValue("version"));
							this->read = noRead;
						}
						else if (this->xmlTag[2].CompareNoCase("METADATA_PROFILE"))
							this->stringReader = &this->dataStorage.metatdatProfile;
						else
							this->read = noRead;

					}
					else if (this->readDataset_Id)
					{
						this->read = readString;
						if (this->xmlTag[2].CompareNoCase("DATASET_NAME"))
							this->stringReader = &this->dataStorage.dataSetName;
						else 
							this->read = noRead;
					}
					else if (this->readRaster_Frame)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("VERTEX"))
							this->readVertex = true;
						else if (this->xmlTag[2].CompareNoCase("Scene_Center"))
							this->readScene_Center = true;
						else if (this->xmlTag[2].CompareNoCase("SCENE_ORIENTATION"))
						{
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.RasterFrameOrientation;
						}
					}
					else if (this->readRaster_CS)
					{
						this->read = readInt;
						if (this->xmlTag[2].CompareNoCase("PIXEL_ORIGIN"))
							this->intReader = &this->dataStorage.pixelOrgin;
						else
							this->read = noRead;
					}
					else if (this->readProduction)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Production_Facility"))
							this->readProduction_Facility = true;

					}
					else if (this->readRaster_Dimension)
					{
						this->read = readInt;
						if (this->xmlTag[2].CompareNoCase("NCOLS"))
							this->intReader = &this->dataStorage.ncols;
						else if (this->xmlTag[2].CompareNoCase("NROWS"))
							this->intReader = &this->dataStorage.nrows;
						else if (this->xmlTag[2].CompareNoCase("NBANDS"))
							this->intReader = &this->dataStorage.nbands;

						else
							this->read = noRead;
					}
					else if (this->readCoordinate_Reference_System)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Horizontal_CS"))
							this->readHorizontal_CS = true;
					}
					else if (this->readRaster_Encoding)
					{
						this->read = readInt;
						if (this->xmlTag[2].CompareNoCase("NBITS"))
							this->intReader = & this->dataStorage.bpp;
						else
							this->read = noRead;
					}
					else if (this->readData_Processing)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Processing_Options"))
							this->readProcessing_Options = true;
					}
					else if (this->readData_Access)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Data_File"))
							this->readData_File = true;

					}
					else if (this->readImage_Display)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Band_Display_Order"))
							this->readBand_Display_Order = true;

					}
					else if (this->readDataset_Sources)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Source_Information"))
							this->readSource_Information = true;
					}
					else if (this->readImage_Interpretation)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Spectral_Band_Info"))
							this->readSpectral_Band_Info = true;
					}
					else if (this->readData_Strip)
					{
						this->read = noRead;
						if (this->xmlTag[2].CompareNoCase("Ephemeris"))
							this->readEphemeris = true;
						else if (this->xmlTag[2].CompareNoCase("Time_Stamp"))
							this->readTime_Stamp = true;
						else if (this->xmlTag[2].CompareNoCase("Sensor_Configuration"))
							this->readSensor_Configuration = true;
						else if (this->xmlTag[2].CompareNoCase("Attitudes"))
							this->readAttitudes = true;
						else if (this->xmlTag[2].CompareNoCase("Identification"))
							this->readIdentification = true;
						else if (this->xmlTag[2].CompareNoCase("Frame_Counters"))
							this->readFrame_Counters = true;
						else if (this->xmlTag[2].CompareNoCase("Sensor_Calibration"))
							this->readSensor_Calibration = true;
					}
					
					break;
				}
	case 3:		{
					if (this->readEphemeris)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Raw_Ephemeris"))
							this->readRaw_Ephemeris = true;
						if (this->xmlTag[3].CompareNoCase("Corrected_Ephemeris"))
							this->readCorrected_Ephemeris = true;
					}
					else if (this->readTime_Stamp)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("LINE_PERIOD"))
						{
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.linePeriode;
						}
						else if (this->xmlTag[3].CompareNoCase("REFERENCE_TIME"))
						{
							this->read = readString;
							this->stringReader = &this->time;
						}
						else if (this->xmlTag[3].CompareNoCase("REFERENCE_LINE"))
						{
							this->read = readInt;
							this->intReader = &this->dataStorage.referenceLine;
						}
						else if (this->xmlTag[3].CompareNoCase("REFERENCE_BAND"))
						{
							this->read = readInt;
							this->intReader = &this->dataStorage.referenceBand;
						}
						
					}
					else if (this->readSensor_Configuration)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Instrument_Biases"))
							this->readInstrument_Biases = true;
						else if (this->xmlTag[3].CompareNoCase("Instrument_Look_Angles_List"))
							this->readInstrument_Look_Angles_List = true;

					}
					else if (this->readAttitudes)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Raw_Attitudes"))
							this->readRaw_Attitudes = true;
						else if (this->xmlTag[3].CompareNoCase("Corrected_Attitudes"))
							this->readCorrected_Attitudes = true;
					}
					else if (this->readProduction_Facility)
					{
						this->read = noRead;
					}
					else if (this->readProcessing_Options)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Correction_Algorithm"))
							this->readCorrection_Algorithm = true;
					}
					else if (this->readData_File_Format)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Data_File"))
							this->readData_File = true;
					}
					else if (this->readBand_Display_Order)
					{
						this->read = noRead;
					}
					else if (this->readSource_Information)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Scene_Source"))
							this->readScene_Source = true;
					}
					else if (this->readSpectral_Band_Info)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Gain_List"))
							this->readGain_List = true;
					}
					else if (this->readVertex)
					{
						this->read = readDouble;
						if (this->xmlTag[3].CompareNoCase("FRAME_LON"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[3].CompareNoCase("FRAME_LAT"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[3].CompareNoCase("FRAME_ROW"))
							this->doubleReader = &this->z;
						else if (this->xmlTag[3].CompareNoCase("FRAME_COL"))
							this->doubleReader = &this->vx;
						else 
							this->read = noRead;
					}
					else if (this->readScene_Center)
					{
						this->read = readDouble;
						if (this->xmlTag[3].CompareNoCase("FRAME_LON"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[3].CompareNoCase("FRAME_LAT"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[3].CompareNoCase("FRAME_ROW"))
							this->doubleReader = &this->z;
						else if (this->xmlTag[3].CompareNoCase("FRAME_COL"))
							this->doubleReader = &this->vx;
						else 
							this->read = noRead;

					}
					else if (this->readHorizontal_CS)
					{
						this->read = readString;
						if (this->xmlTag[3].CompareNoCase("HORIZONTAL_CS_CODE"))
							this->stringReader = &this->dataStorage.horizontalCsCode;
						else if (this->xmlTag[3].CompareNoCase("HORIZONTAL_CS_TYPE"))
							this->stringReader = &this->dataStorage.horizontalCsType;
						else if (this->xmlTag[3].CompareNoCase("HORIZONTAL_CS_NAME"))
							this->stringReader = &this->dataStorage.horizontalCsName;
					}
					else if (this->readFrame_Counters)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Band_Counters"))
							this->readBand_Counters = true;

					}
					else if (this->readSensor_Calibration)
					{
						this->read = noRead;
						if (this->xmlTag[3].CompareNoCase("Calibration"))
							this->readCalibration = true;

					}
					else if (this->readIdentification)
					{
						this->read = readString;
						if (this->xmlTag[3].CompareNoCase("ID"))
							this->stringReader = &this->dataStorage.dataStripID;
						else 
							this->read = noRead;
					}
					
					break;
				}
	case 4:		{
					if (this->readCorrection_Algorithm)
					{
						this->read = noRead;
					}
					else if (this->readData_File)
					{
						this->read = noRead;
					}
					else if (this->readScene_Source)
					{
						this->read = noRead;
					}
					else if (this->readGain_List)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Gain"))
							this->readGain = true;
					}
					else if(this->readRaw_Ephemeris)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Point_List"))
							this->readPoint_List = true;
					}
					else if(this->readCorrected_Ephemeris)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Point_List"))
							this->readCorrected_Point_List = true;
					}
					else if(this->readRaw_Attitudes)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Quaternion_List"))
							this->readQuaternion_List = true;
					}
					else if(this->readCorrected_Attitudes)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("ECF_Attitude"))
							this->readECF_Attitude = true;
					}
					else if (this->readInstrument_Look_Angles_List)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Instrument_Look_Angles"))
							this->readInstrument_Look_Angles= true;

					}
					else if (this->readInstrument_Biases)
					{
						this->read = readDouble;
						if (this->xmlTag[4].CompareNoCase("ROLL"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[4].CompareNoCase("PITCH"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[4].CompareNoCase("YAW"))
							this->doubleReader = &this->z;
					}
					else if (this->readBand_Counters)
					{
						this->read = noRead;
					}
					else if (this->readCalibration)
					{
						this->read = noRead;
						if (this->xmlTag[4].CompareNoCase("Band_Parameters"))
							this->readBand_Parameters= true;
					}
						
					break;
				}
	case 5:		{
					if (this->readGain)
					{
						this->read = noRead;
					}
					else if (this->readCorrected_Point_List)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Point"))
							this->readCorrected_Point = true;
					}
					else if (this->readPoint_List)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Point"))
							this->readPoint = true;
					}
					else if (this->readQuaternion_List)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Quaternion"))
							this->readQuaternion = true;

					}
					else if (this->readInstrument_Look_Angles)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Polynomial_Look_Angles"))
							this->readPolynomial_Look_Angles = true;
					}
					else if (this->readBand_Parameters)
					{
						this->read = noRead;
						if (this->xmlTag[5].CompareNoCase("Gain_Section_List"))
							this->readGain_Section_List = true;
					}

					break;
				}
	case 6:		{
					if (this->readPoint)
					{
						this->read = noRead;
						if (this->xmlTag[6].CompareNoCase("Location"))
							this->readLocation = true;
						else if (this->xmlTag[6].CompareNoCase("Velocity"))
							this->readVelocity = true;
						else if (this->xmlTag[6].CompareNoCase("TIME"))
						{	
							this->read = readString;
							this->stringReader = &this->time;
						}
					}
					if (this->readCorrected_Point)
					{
						this->read = noRead;
						if (this->xmlTag[6].CompareNoCase("Location"))
							this->readLocation = true;
						else if (this->xmlTag[6].CompareNoCase("Velocity"))
							this->readVelocity = true;
						else if (this->xmlTag[6].CompareNoCase("TIME"))
						{	
							this->read = readString;
							this->stringReader = &this->time;
						}
					}
					else if (this->readQuaternion)
					{
						this->read = noRead;
						if (this->xmlTag[6].CompareNoCase("TIME"))
						{
							this->read = readString;
							this->stringReader = &this->time;
						}
						else if (this->xmlTag[6].CompareNoCase("Q0"))
						{
							this->read = readDouble;
							this->doubleReader = &this->q0;
						}
						else if (this->xmlTag[6].CompareNoCase("Q1"))
						{
							this->read = readDouble;
							this->doubleReader = &this->q1;
						}
						else if (this->xmlTag[6].CompareNoCase("Q2"))
						{
							this->read = readDouble;
							this->doubleReader = &this->q2;
						}
						else if (this->xmlTag[6].CompareNoCase("Q3"))
						{
							this->read = readDouble;
							this->doubleReader = &this->q3;
						}

					}
					else if (this->readECF_Attitude)
					{
						this->read = noRead;
						if (this->xmlTag[6].CompareNoCase("Angle"))
							this->readAngle = true;
					}
					else if (this->readPolynomial_Look_Angles)
					{
						if (this->xmlTag[6].CompareNoCase("PSI_X_0"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.lookx0;
						}
						else if (this->xmlTag[6].CompareNoCase("PSI_X_1"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.lookx1;
						}
						else if (this->xmlTag[6].CompareNoCase("PSI_X_2"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.lookx2;
						}
						else if (this->xmlTag[6].CompareNoCase("PSI_X_3"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.lookx3;
						}
						else if (this->xmlTag[6].CompareNoCase("PSI_Y_0"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.looky0;
						}
						else if (this->xmlTag[6].CompareNoCase("PSI_Y_1"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.looky1;
						}
						else if (this->xmlTag[6].CompareNoCase("PSI_Y_2"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.looky2;
						}
						else if (this->xmlTag[6].CompareNoCase("PSI_Y_3"))
						{	
							this->read = readDouble;
							this->doubleReader = &this->dataStorage.looky3;
						}
					}
					else if (this->readGain_Section_List)
					{
						this->read = noRead;
						if (this->xmlTag[6].CompareNoCase("Gain_Section"))
							this->readGain_Section = true;
					}
					
					break;
				}
	case 7:		{
					if (this->readLocation)
					{
						this->read = readDouble;
						if (this->xmlTag[7].CompareNoCase("X"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[7].CompareNoCase("Y"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[7].CompareNoCase("Z"))
							this->doubleReader = &this->z;
					}
					else if (this->readVelocity)
					{
						this->read = readDouble;
						this->hasVelocity = true;
						if (this->xmlTag[7].CompareNoCase("X"))
							this->doubleReader = &this->vx;
						else if (this->xmlTag[7].CompareNoCase("Y"))
							this->doubleReader = &this->vy;
						else if (this->xmlTag[7].CompareNoCase("Z"))
							this->doubleReader = &this->vz;
					}
					else if (this->readAngle)
					{
						this->read = readDouble;
						if (this->xmlTag[7].CompareNoCase("TIME"))
						{	
							this->read = readString;
							this->stringReader = &this->time;
						}
						else if (this->xmlTag[7].CompareNoCase("ROLL"))
							this->doubleReader = &this->x;
						else if (this->xmlTag[7].CompareNoCase("PITCH"))
							this->doubleReader = &this->y;
						else if (this->xmlTag[7].CompareNoCase("YAW"))
							this->doubleReader = &this->z;
					}
					else if (this->readGain_Section)
					{
						this->read = noRead;
						if (this->xmlTag[7].CompareNoCase("Pixel_Parameters"))
							this->readPixelParameters = true;
					}
				}
	}

	this->tagDepth++;

}


