#include <float.h> 
#include "OrthoPhotoVector.h"
#include "BaristaProject.h"
#include "Trans3DFactory.h"
#include "IniFile.h"
#include "MonoPlotter.h"

COrthoPhotoVector::COrthoPhotoVector() :
	oneImageOnly(false),bicubic(DEFAULT_USE_BICUBIC), useAnchor(DEFAULT_USE_ANCHOR_POINTS),
	anchorWidth(DEFAULT_ANCHOR_WIDTH),pDEM(NULL),fromROI(false)
{

}

COrthoPhotoVector::~COrthoPhotoVector()
{

}

bool COrthoPhotoVector::initImg(CVImage *image)
{
	this->oneImageOnly = true;
	this->clear();

	bool ret = false;

	if (image->getCurrentSensorModel())
	{
		COrthoPhotoRecord pars;

		this->push_back(pars);
		(*this)[0].setImg(image, true);
		ret = true;
	}	
	return ret;
}


void COrthoPhotoVector::initImgList()
{
	this->oneImageOnly = false;
	this->clear();

	COrthoPhotoRecord pars;
	int count =0;
	for (int i = 0; i < project.getNumberOfImages(); ++i)
	{
		CVImage *img = project.getImageAt(i);
		if (img->getCurrentSensorModel())
		{
			this->push_back(pars);
			(*this)[count].setImg(img, true);
			count++;
		}
	}
}

bool COrthoPhotoVector::isInitialised() const
{
	return (this->size() > 0);
}


void COrthoPhotoVector::setDEM(CVDEM* newDEM)
{
	this->pDEM = newDEM;

	if (this->pDEM)
	{
		if (this->fromROI) this->setMaxExtentsForAll();
		else if (inifile.getOrthoDEM() == *(this->pDEM->getFileName()))
		{
			double LLX = inifile.getOrthoMinX();
			double LLY = inifile.getOrthoMinY();
			double URX = inifile.getOrthoMaxX();
			double URY = inifile.getOrthoMaxY();
			if ((LLX <= 0) || (LLY <= 0) || (URX <= 0) || (URY <= 0) || (URX <= LLX) || (URY <= LLY))
				this->setMaxExtentsForAll();
			else
			{
				for (unsigned int i = 0; i < this->size(); ++i)
				{
					this->at(i).LLC.x = LLX;
					this->at(i).LLC.y = LLY;
					this->at(i).RUC.x = URX;
					this->at(i).RUC.y = URY;
				}
			}

		}
		else this->setMaxExtentsForAll();
	}

}


void COrthoPhotoVector::init(CVImage *img, C2DPoint *lrc, C2DPoint *ulc)
{
	this->fromROI = false;
	
	if (lrc && ulc)
	{
		this->LrROI = *lrc;
		this->UlROI = *ulc;
		this->fromROI = true;
	}

	// int images 
	if (img) this->initImg(img); 
	else this->initImgList();

	/* resolution is initialized in initDEMBorders()
	// set resolution
	double orthoRes = inifile.getOrthoRes();
	if (orthoRes <= 0) orthoRes = 1.0f;
	for (unsigned int i = 0; i < this->size(); ++i)
	{
		(*this)[i].GSD.x = orthoRes;
		(*this)[i].GSD.y = orthoRes;
	}
	*/

	// check for DEM

	// first check if we know the DEM from the inifile
	int selected = -1;
	CCharString oldDEM = inifile.getOrthoDEM();
	for (int i = 0; i < project.getNumberOfDEMs(); ++i)
	{
		CVDEM *dem = project.getDEMAt(i);
		if (oldDEM == *(dem->getFileName())) 
			selected = i;		
	}
	
	// then use the active DEM
	CVDEM* activeDEM = project.getActiveDEM();
	if (activeDEM)
		selected = project.getDEMIndex(activeDEM);


	if (project.getNumberOfDEMs() <= 0)  // no DEM in project
		this->pDEM = 0;
	else
	{
		if (selected < 0) // DEMs in project have not been selected automatically, just use the first in project
		{
			this->pDEM = project.getDEMAt(0);
			//this->setMaxExtentsForAll();
		}
		else // use the selected one
		{
			this->pDEM = project.getDEMAt(selected);
			//if (this->fromROI) this->setMaxExtentsForAll();
			//else
			//{
				//double LLX = inifile.getOrthoMinX();
				//double LLY = inifile.getOrthoMinY();
				//double URX = inifile.getOrthoMaxX();
				//double URY = inifile.getOrthoMaxY();
				//if ((LLX >= this->pDEM->getDEM()->getmaxX()) || (LLY >= this->pDEM->getDEM()->getmaxY()) || 
				//	(URX <= this->pDEM->getDEM()->getminX()) || (URY <= this->pDEM->getDEM()->getminY()) || 
				//	(URX <= LLX) || (URY <= LLY))
				//	this->setMaxExtentsForAll();
				//else
				//{
				//	for (unsigned int i = 0; i < this->size(); ++i)
				//	{
				//		this->at(i).LLC.x = LLX;
				//		this->at(i).LLC.y = LLY;
				//		this->at(i).RUC.x = URX;
				//		this->at(i).RUC.y = URY;
				//	}
				//}
			//}
		}
		this->setMaxExtentsForAll(); // set the extents for all images
	}
}

//addition >>
void COrthoPhotoVector::covertCoordinates(int index)
{
	if ((*this)[index].isUTM)//if previously in UTM, change it to geographic
	{
		(*this)[index].isUTM = false;

		this->pDEM->getReferenceSystem().gridToGeographic((*this)[index].ULout, (*this)[index].ULin);
		this->pDEM->getReferenceSystem().gridToGeographic((*this)[index].URout, (*this)[index].URin);
		this->pDEM->getReferenceSystem().gridToGeographic((*this)[index].LLout, (*this)[index].LLin);
		this->pDEM->getReferenceSystem().gridToGeographic((*this)[index].LRout, (*this)[index].LRin);
	}
	else //if previously in geographic, change it to utm
	{
		(*this)[index].isUTM = true;

		this->pDEM->getReferenceSystem().geographicToGrid((*this)[index].ULout, (*this)[index].ULin);
		this->pDEM->getReferenceSystem().geographicToGrid((*this)[index].URout, (*this)[index].URin);
		this->pDEM->getReferenceSystem().geographicToGrid((*this)[index].LLout, (*this)[index].LLin);
		this->pDEM->getReferenceSystem().geographicToGrid((*this)[index].LRout, (*this)[index].LRin);		
	}

	C3DPoint P, O;
	P.x = (*this)[index].ULin.x;
	P.y = (*this)[index].ULin.y;
	P.z = 1.0;
	this->pDEM->getReferenceSystem().gridToGeographic(O,P);
	this->pDEM->getReferenceSystem().geographicToGrid(P,O);

	double newxmin = (*this)[index].ULout.x;
	double newymin = (*this)[index].ULout.y;

	double newxmax = (*this)[index].ULout.x;
	double newymax = (*this)[index].ULout.y;


	// determine xmin
	if ( (*this)[index].URout.x < newxmin ) newxmin = (*this)[index].URout.x;
	if ( (*this)[index].LLout.x < newxmin ) newxmin = (*this)[index].LLout.x;
	if ( (*this)[index].LRout.x < newxmin ) newxmin = (*this)[index].LRout.x;

	// determine xmax
	if ( (*this)[index].URout.x > newxmax ) newxmax = (*this)[index].URout.x;
	if ( (*this)[index].LLout.x > newxmax ) newxmax = (*this)[index].LLout.x;
	if ( (*this)[index].LRout.x > newxmax ) newxmax = (*this)[index].LRout.x;

	// determine ymin
	if ( (*this)[index].URout.y < newymin ) newymin = (*this)[index].URout.y;
	if ( (*this)[index].LLout.y < newymin ) newymin = (*this)[index].LLout.y;
	if ( (*this)[index].LRout.y < newymin ) newymin = (*this)[index].LRout.y;

	// determine ymax
	if ( (*this)[index].URout.y > newymax ) newymax = (*this)[index].URout.y;
	if ( (*this)[index].LLout.y > newymax ) newymax = (*this)[index].LLout.y;
	if ( (*this)[index].LRout.y > newymax ) newymax = (*this)[index].LRout.y;


	C3DPoint llObj, urObj;
	if ((*this)[index].isUTM) // if changed to UTM
	{
		llObj.x = newxmin;
		llObj.y = newymin;
		urObj.x = newxmax;
		urObj.y = newymax;
		//convert grid values to metres
		(*this)[index].GSD.x = (*this)[index].GSD.x * 111194.83; // Earth radius R= 6371000m; so 1 degree = 111194.83m
		(*this)[index].GSD.y = (*this)[index].GSD.x;

		/*this->ymin = newxmin;
		this->xmin = newymin;

		this->ymax = newxmax;
		this->xmax = newymax;

		this->gridx = (newxmax - newxmin)/this->rows;
		this->gridy = (newymax - newymin)/this->cols;*/
	}
	else //if changed to geographic
	{
		llObj.x = newymin;
		llObj.y = newxmin;
		urObj.x = newymax;
		urObj.y = newxmax;
		//convert grid values to degrees
		(*this)[index].GSD.x = (*this)[index].GSD.x / 111194.83; // Earth radius R= 6371000m; so 1 degree = 111194.83m
		(*this)[index].GSD.y = (*this)[index].GSD.x;

	/*	this->xmin = newxmin;
		this->ymin = newymin;

		this->xmax = newxmax;
		this->ymax = newymax;

		this->gridx = (newxmax - newxmin)/this->cols;
		this->gridy = (newymax - newymin)/this->rows;*/
	}
	//else
	//{

	//}

			(*this)[index].LLC.x = floor(llObj.x / (*this)[index].GSD.x) * (*this)[index].GSD.x;
			(*this)[index].LLC.y = floor(llObj.y / (*this)[index].GSD.y) * (*this)[index].GSD.y;
			(*this)[index].RUC.x = floor(urObj.x / (*this)[index].GSD.x + 1) * (*this)[index].GSD.x;
			(*this)[index].RUC.y = floor(urObj.y / (*this)[index].GSD.y + 1) * (*this)[index].GSD.y;

			(*this)[index].LLin = (*this)[index].LLout;
			(*this)[index].URin = (*this)[index].URout;
			(*this)[index].ULin = (*this)[index].ULout;
			(*this)[index].LRin = (*this)[index].LRout;

			/*double zmin = this->pDEM->getDEM()->getminZ();
			if ((*this)[index].isUTM) // if changed to UTM
			{
				(*this)[index].LLin.x = llObj.x;
				(*this)[index].LLin.y = llObj.y;

				(*this)[index].URin.x = urObj.x;
				(*this)[index].URin.y = urObj.y;
				
				(*this)[index].ULin.x = llObj.x;
				(*this)[index].ULin.y = urObj.y;
				
				(*this)[index].LRin.x = urObj.x;
				(*this)[index].LRin.y = llObj.y;
			}
			else // if changed to geographic
			{
				(*this)[index].LLin.x = llObj.y;
				(*this)[index].LLin.y = llObj.x;

				(*this)[index].URin.x = urObj.y;
				(*this)[index].URin.y = urObj.x;
				
				(*this)[index].ULin.x = llObj.y;
				(*this)[index].ULin.y = urObj.x;
				
				(*this)[index].LRin.x = urObj.y;
				(*this)[index].LRin.y = llObj.x;
			}
			(*this)[index].LLin.z = zmin;
			(*this)[index].ULin.z = zmin;
			(*this)[index].URin.z = zmin;
			(*this)[index].LRin.z = zmin;*/
}

//void COrthoPhotoVector::covertCoordinatesAll()
//{

//}
//<< addition

bool COrthoPhotoVector::initDEMBorders(int index)
{
	bool ret= true;

	if (this->isInitialised() && this->pDEM)
	{
		//addition >>
		// set resolution
		double orthoRes = inifile.getOrthoRes(); // resolution in metres
		if (orthoRes <= 0) orthoRes = 1.0f; // Earth radius R= 6371000m; so 1 degree = 111194.83m
		if (this->pDEM->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
			orthoRes = orthoRes/111194.83;
		//else
		// keep it in metres for eGRID system (UTM)
		(*this)[index].GSD.x = orthoRes;
		(*this)[index].GSD.y = orthoRes;
		//for (unsigned int i = 0; i < this->size(); ++i)
		//{

		//	(*this)[i].GSD.x = orthoRes;
		//	(*this)[i].GSD.y = orthoRes;
		//}
		//<< addition

		// calculate area from ROI on image
		if (this->fromROI)
		{
			ret = this->initAreafromROI(0);
			this->fromROI = false;
		}
		else if ( (index >=0) && (index < (int) this->size()) && this->pDEM)
		{	
			double DEMLLCX;// = this->pDEM->getDEM()->getminX();
			double DEMLLCY;// = this->pDEM->getDEM()->getminY();
			double DEMRUCX;// = this->pDEM->getDEM()->getmaxX();
			double DEMRUCY;// = this->pDEM->getDEM()->getmaxY();
			double zmin = this->pDEM->getDEM()->getminZ();

			if (this->pDEM->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
			{
				DEMLLCY = this->pDEM->getDEM()->getminX();
				DEMLLCX = this->pDEM->getDEM()->getminY();
				DEMRUCY = this->pDEM->getDEM()->getmaxX();
				DEMRUCX = this->pDEM->getDEM()->getmaxY();
				//DEMRUCY = DEMLLCY + (this->pDEM->getDEM()->getHeight()-1)*(*this)[index].GSD.y;
				//DEMRUCX = DEMLLCX + (this->pDEM->getDEM()->getWidth()-1)*(*this)[index].GSD.x;
			}
			else //if (this->pDEM->getReferenceSystem().getCoordinateType() == eGRID)
			{
				DEMLLCX = this->pDEM->getDEM()->getminX();
				DEMLLCY = this->pDEM->getDEM()->getminY();
				DEMRUCX = this->pDEM->getDEM()->getmaxX();
				DEMRUCY = this->pDEM->getDEM()->getmaxY();
			}

			C2DPoint luImg(0, 0);
			C2DPoint lrImg((*this)[index].getInputImg()->getHugeImage()->getWidth(), (*this)[index].getInputImg()->getHugeImage()->getHeight());
			C2DPoint llImg(luImg.x, lrImg.y);
			C2DPoint urImg(lrImg.x, luImg.y);
			C3DPoint luObj, lrObj, llObj, urObj;
			CSensorModel *model = (*this)[index].getInputImg()->getCurrentSensorModel();

			model->transformObs2Obj(luObj, luImg, zmin);
			model->transformObs2Obj(lrObj, lrImg, zmin);
			model->transformObs2Obj(urObj, urImg, zmin);
			model->transformObs2Obj(llObj, llImg, zmin);

			CTrans3D *trans = CTrans3DFactory::initTransformation(model->getRefSys(), this->pDEM->getReferenceSystem());
			if (trans)
			{
				trans->transform(luObj, luObj);
				trans->transform(lrObj, lrObj);
				trans->transform(llObj, llObj);
				trans->transform(urObj, urObj);

				// test
				/*C3DPoint luObjOut, lrObjOut, llObjOut, urObjOut;
				if (this->pDEM->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
				{
					this->pDEM->getReferenceSystem().geographicToGrid(luObjOut, luObj);
					this->pDEM->getReferenceSystem().geographicToGrid(lrObjOut, lrObj);
					this->pDEM->getReferenceSystem().geographicToGrid(llObjOut, llObj);
					this->pDEM->getReferenceSystem().geographicToGrid(urObjOut, urObj);
				}
				else
				{
					this->pDEM->getReferenceSystem().gridToGeographic(luObjOut, luObj);
					this->pDEM->getReferenceSystem().gridToGeographic(lrObjOut, lrObj);
					this->pDEM->getReferenceSystem().gridToGeographic(llObjOut, llObj);
					this->pDEM->getReferenceSystem().gridToGeographic(urObjOut, urObj);
				}*/
				//

				//addition >>
				(*this)[index].LLin = llObj;
				(*this)[index].URin = urObj;
				(*this)[index].ULin = luObj;
				(*this)[index].LRin = lrObj;
				//<< addition

				double xmin = luObj.x; 
				if (lrObj.x < xmin) xmin = lrObj.x;
				if (urObj.x < xmin) xmin = urObj.x;
				if (llObj.x < xmin) xmin = llObj.x;
				double xmax = luObj.x; 
				if (lrObj.x > xmax) xmax = lrObj.x;
				if (urObj.x > xmax) xmax = urObj.x;
				if (llObj.x > xmax) xmax = llObj.x;
				double ymin = luObj.y; 
				if (lrObj.y < ymin) ymin = lrObj.y;
				if (urObj.y < ymin) ymin = urObj.y;
				if (llObj.y < ymin) ymin = llObj.y;
				double ymax = luObj.y; 
				if (lrObj.y > ymax) ymax = lrObj.y;
				if (urObj.y > ymax) ymax = urObj.y;
				if (llObj.y > ymax) ymax = llObj.y;
				/*llObj.x = xmin;
				llObj.y = ymin;
				urObj.x = xmax;
				urObj.y = ymax;*/
				// addition >>
				if (this->pDEM->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
				{
					llObj.x = ymin;
					llObj.y = xmin;
					urObj.x = ymax;
					urObj.y = xmax;
					(*this)[index].isUTM = false;					
				}
				else if (this->pDEM->getReferenceSystem().getCoordinateType() == eGRID)
				{
					llObj.x = xmin;
					llObj.y = ymin;
					urObj.x = xmax;
					urObj.y = ymax;
					(*this)[index].isUTM = true;
				}
				//<< addition

				if (DEMLLCX > llObj.x) llObj.x = DEMLLCX;
				if (DEMLLCY > llObj.y) llObj.y = DEMLLCY;
				if (DEMRUCX < urObj.x) urObj.x = DEMRUCX;
				if (DEMRUCY < urObj.y) urObj.y = DEMRUCY;
				delete trans;
			}
			else
			{
				llObj.x = DEMLLCX;
				llObj.y = DEMLLCY;
				urObj.x = DEMRUCX;
				urObj.y = DEMRUCY;
			}

			//addition>>
			/*if (this->pDEM->getReferenceSystem().getCoordinateType() == eGEOGRAPHIC)
			{
				(*this)[index].LLC.x = (llObj.x / (*this)[index].GSD.x) * (*this)[index].GSD.x;
				(*this)[index].LLC.y = (llObj.y / (*this)[index].GSD.y) * (*this)[index].GSD.y;
				(*this)[index].RUC.x = (urObj.x / (*this)[index].GSD.x + 1) * (*this)[index].GSD.x;
				(*this)[index].RUC.y = (urObj.y / (*this)[index].GSD.y + 1) * (*this)[index].GSD.y;					
			}
			else if (this->pDEM->getReferenceSystem().getCoordinateType() == eGRID)
			{
				(*this)[index].LLC.x = floor(llObj.x / (*this)[index].GSD.x) * (*this)[index].GSD.x;
				(*this)[index].LLC.y = floor(llObj.y / (*this)[index].GSD.y) * (*this)[index].GSD.y;
				(*this)[index].RUC.x = floor(urObj.x / (*this)[index].GSD.x + 1) * (*this)[index].GSD.x;
				(*this)[index].RUC.y = floor(urObj.y / (*this)[index].GSD.y + 1) * (*this)[index].GSD.y;
			}*/
			//<< addition

			(*this)[index].LLC.x = floor(llObj.x / (*this)[index].GSD.x) * (*this)[index].GSD.x;
			(*this)[index].LLC.y = floor(llObj.y / (*this)[index].GSD.y) * (*this)[index].GSD.y;
			(*this)[index].RUC.x = floor(urObj.x / (*this)[index].GSD.x + 1) * (*this)[index].GSD.x;
			(*this)[index].RUC.y = floor(urObj.y / (*this)[index].GSD.y + 1) * (*this)[index].GSD.y;

			//addition >>
			/*if ((*this)[index].isUTM) // if in UTM
			{
				(*this)[index].LLin.x = llObj.x;
				(*this)[index].LLin.y = llObj.y;

				(*this)[index].URin.x = urObj.x;
				(*this)[index].URin.y = urObj.y;
				
				(*this)[index].ULin.x = llObj.x;
				(*this)[index].ULin.y = urObj.y;
				
				(*this)[index].LRin.x = urObj.x;
				(*this)[index].LRin.y = llObj.y;
			}
			else // if in geographic
			{
				(*this)[index].LLin.x = llObj.y;
				(*this)[index].LLin.y = llObj.x;

				(*this)[index].URin.x = urObj.y;
				(*this)[index].URin.y = urObj.x;
				
				(*this)[index].ULin.x = llObj.y;
				(*this)[index].ULin.y = urObj.x;
				
				(*this)[index].LRin.x = urObj.y;
				(*this)[index].LRin.y = llObj.x;
			}
			(*this)[index].LLin.z = zmin;
			(*this)[index].ULin.z = zmin;
			(*this)[index].URin.z = zmin;
			(*this)[index].LRin.z = zmin;*/
			//<< addition

		}
	}

	return ret;
}



bool COrthoPhotoVector::initAreafromROI(int index)
{
	bool ret = true;

	if (this->isInitialised() && this->pDEM && (index < this->size()) && (index >=0))
	{
		CXYZPoint lowerright;
		CXYZPoint upperleft;
		CXYZPoint lowerleft;
		CXYZPoint upperright;

		CMonoPlotter plotter;

		plotter.setModel(this->at(index).img->getCurrentSensorModel());
		plotter.setVDEM(this->pDEM, false);
		if ((!plotter.getDEMToSensor()) || (!plotter.getSensorToDEM()))
		{		
			this->errorMessage = " Coordinate systems of sensor model and DEM are incompatible!";
			this->errorTitleMessage = "Orthorectification not possible!";
			ret = false;
		}
		else
		{
			bool lrokay = plotter.getPoint(lowerright, this->LrROI);
			bool ulokay = plotter.getPoint(upperleft,  this->UlROI);
			bool llokay = plotter.getPoint(lowerleft,  this->LrROI);
			bool urokay = plotter.getPoint(upperright, this->UlROI);

			double minx =  FLT_MAX;
			double miny =  FLT_MAX;
			double maxx = -FLT_MAX;
			double maxy = -FLT_MAX;

			if (!lrokay || !ulokay || !llokay || !urokay)
			{
				this->errorMessage = " The corners outside of DEM will be set to DEM extends!";
				this->errorTitleMessage = "ROI exceeds current DEM extensions!";
				ret = false;		
			}

			if (lrokay)
			{
				if (lowerright.x < minx) minx = lowerright.x;
				if (lowerright.x > maxx) maxx = lowerright.x;
				if (lowerright.y < miny) miny = lowerright.y;
				if (lowerright.y > maxy) maxy = lowerright.y;
			}
			
			if (ulokay)
			{
				if (upperleft.x < minx) minx = upperleft.x;
				if (upperleft.x > maxx) maxx = upperleft.x;
				if (upperleft.y < miny) miny = upperleft.y;
				if (upperleft.y > maxy) maxy = upperleft.y;
			}
				
			if (llokay)
			{
				if (lowerleft.x < minx) minx = lowerleft.x;
				if (lowerleft.x > maxx) maxx = lowerleft.x;
				if (lowerleft.y < miny) miny = lowerleft.y;
				if (lowerleft.y > maxy) maxy = lowerleft.y;
			}
		
			if (urokay)
			{
				if (upperright.x < minx) minx = upperright.x;
				if (upperright.x > maxx) maxx = upperright.x;
				if (upperright.y < miny) miny = upperright.y;
				if (upperright.y > maxy) maxy = upperright.y;
			}
		

			if ((fabs(minx - FLT_MAX) < 1.0f) || (minx < this->pDEM->getDEM()->getminX()))  
				minx = this->pDEM->getDEM()->getminX();

			if ((fabs(miny - FLT_MAX) < 1.0f) || (miny < this->pDEM->getDEM()->getminY()))  
				miny = this->pDEM->getDEM()->getminY();

			if ((fabs(maxx + FLT_MAX) < 1.0f) || (maxx > this->pDEM->getDEM()->getmaxX()))  
				maxx = this->pDEM->getDEM()->getmaxX();

			if ((fabs(maxy - FLT_MAX) < 1.0f) || (maxy > this->pDEM->getDEM()->getmaxY()))  
				maxy = this->pDEM->getDEM()->getmaxY();

			
			this->at(index).LLC.x = minx;
			this->at(index).LLC.y = miny;
			this->at(index).RUC.x = maxx;
			this->at(index).RUC.y = maxy;
				

			double gsdx = this->at(index).GSD.x;
			double gsdy = this->at(index).GSD.y;

			this->at(index).LLC.x = floor(this->at(index).LLC.x / gsdx) * gsdx;
			this->at(index).LLC.y = floor(this->at(index).LLC.y / gsdy) * gsdy;
			if (this->at(index).RUC.x <= this->at(index).LLC.x)
				this->at(index).RUC.x = this->at(index).LLC.x + gsdx;
			else 
			{
				double npix = floor((this->at(index).RUC.x - this->at(index).LLC.x) / gsdx + 1.0);
				this->at(index).RUC.x = this->at(index).LLC.x + npix * gsdx;
			}
			
			if (this->at(index).RUC.y <= this->at(index).LLC.y)
				this->at(index).RUC.y = this->at(index).LLC.y + gsdy;
			else 
			{
				double npix = floor((this->at(index).RUC.y - this->at(index).LLC.y) / gsdy + 1.0);
				this->at(index).RUC.y = this->at(index).LLC.y + npix * gsdy;
			}

		}
	}

	return ret;
}



bool COrthoPhotoVector::setMaxExtentsForAll()
{    
	bool ret = true;
	if (this->isInitialised() && this->pDEM)
	{
		// calculate area from ROI on image
		if (this->fromROI)
		{
			ret = this->initAreafromROI(0);
			this->fromROI = false;
		}
		else
		{	
			for (unsigned int i = 0; i < this->size(); ++i)
			{
				ret = this->initDEMBorders(i);
			}
		}
	}

	return ret;
}

void COrthoPhotoVector::setGSDForAll(double newGSD)
{
	for (unsigned int i = 0; i < this->size(); ++i) 
		this->at(i).setGSD(newGSD,newGSD);
}
