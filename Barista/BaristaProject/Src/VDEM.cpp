/** 
 * @class CVDEM
 * CVDEM, class for dem handling
 *
 * @author Jochen Willneff
 * @version 1.0 
 * @date 21-feb-2006
 *
 */

#include "SystemUtilities.h"

#include "VDEM.h"
#include "StructuredFileSectionArray.h"
#include "ALSData.h"
#include "histogramFlt.h"
#include "histogram.h"
#include "LookupTableHelper.h"
#include "Icons.h"
/*
#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPalette>
#include <QDialog>
#include <QIcon>
#include <QWidget>
*/

CVDEM::CVDEM(void):isActiveDEM(false),demStatistics(4), nStatisticRows(-1),
	nStatisticCols(-1),statFontSize(10),nLegendeClasses(10),zeroValue(0.0),
	legend(NULL)
{

}

CVDEM::CVDEM(CVDEM* src):isActiveDEM(false),demStatistics(src->demStatistics),
	nStatisticRows(src->nStatisticRows),nStatisticCols(src->nStatisticCols)
	,statFontSize(src->statFontSize),nLegendeClasses(src->nLegendeClasses),
	legend(NULL)
	
{
    this->demFileName = src->demFileName;

}

CVDEM::CVDEM(const CVDEM& src):isActiveDEM(false),demStatistics(src.demStatistics),
	nStatisticRows(src.nStatisticRows),nStatisticCols(src.nStatisticCols),
	statFontSize(src.statFontSize),nLegendeClasses(src.nLegendeClasses),
	legend(NULL)
{
    this->demFileName = src.demFileName;


}

CVDEM::~CVDEM(void)
{
	if (this->legend)
	{
		delete this->legend;
		this->legend = NULL;
	}
		
}

bool CVDEM::interpolate(CALSDataSet &als, const char *filename, const C2DPoint &pmin, 
						const C2DPoint &pmax, const C2DPoint &gridS, int N, int echo, int level,
						eInterpolationMethod method, double maxDist, double minsquareDistForWeight)
{
	this->setReferenceSystem(als.getReferenceSystem());

	return this->dem.interpolate(als, filename, pmin, pmax, gridS, N, echo, level, method, maxDist, minsquareDistForWeight);
};



bool CVDEM::interpolate(const CXYZPointArray &points, const char *filename, const C2DPoint &pmin, 
					    const C2DPoint &pmax, const C2DPoint &gridS, int N, 
					    eInterpolationMethod method, double maxDist, double minsquareDistForWeight)
{
	this->setReferenceSystem(points.getRefSys());

	return this->dem.interpolate(points, filename, pmin, pmax, gridS, N, method, maxDist, minsquareDistForWeight);
};


CDEM* CVDEM::getDEM()
{
    return &this->dem;
}

CFileName* CVDEM::getFileName()
{
    return &this->demFileName;
}

void CVDEM::setFileName(const char* filename)
{
    this->demFileName = filename;

    this->informListeners_elementsChanged();

}

/**
 * Serialized storage of a CVDEM to a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVDEM::serializeStore(CStructuredFileSection* pStructuredFileSection)
{
   	CSerializable::serializeStore(pStructuredFileSection);

	CVBase::serializeStore(pStructuredFileSection);

	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName relPath;
	CFileName fileSaveName = this->demFileName;

	if (!Path.IsEmpty())
	{
		relPath = CSystemUtilities::RelPath(this->demFileName, Path);
		fileSaveName.SetPath(relPath);
	}

	CToken* token = pStructuredFileSection->addToken();
	token->setValue("dem filename", fileSaveName);

    token = pStructuredFileSection->addToken();
	token->setValue("isActiveDEM", this->getIsActiveDEM());

    token = pStructuredFileSection->addToken();
	token->setValue("zeroValue", this->zeroValue);

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->getDEM()->serializeStore(child);

    child = pStructuredFileSection->addChild();
	this->getDisplaySettings()->serializeStore(child);

}

/**
 * Restores a CVDEM from a CStructuredFileSection
 * @param pStructuredFileSection
 */
void CVDEM::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{
	CSerializable::serializeRestore(pStructuredFileSection);

	CVBase::serializeRestore(pStructuredFileSection);
	
	CFileName Path = pStructuredFileSection->getProjectPath();
	CFileName CWD;
	if (!Path.IsEmpty())
	{
		CWD = CSystemUtilities::getCwd();
		CSystemUtilities::chDir(Path);
	}

    for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
		CCharString key = token->getKey();

		if(key.CompareNoCase("dem filename"))
			this->demFileName = token->getValue();
		else if(key.CompareNoCase("isActiveDEM"))
			this->setDEMtoActive(token->getBoolValue());
		else if(key.CompareNoCase("zeroValue"))
			this->zeroValue = token->getDoubleValue();

    }

	bool refRead = false;
	CReferenceSystem ref;
    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);
        if (child->getName().CompareNoCase("CDEM"))
        {
			this->dem.serializeRestore(child);
        }
		else if (child->getName().CompareNoCase("CReferenceSystem"))
		{
			ref.serializeRestore(child);
			refRead = true;
		}
        else if (child->getName().CompareNoCase("CDisplaySettings"))
        {
			this->displaySettings.serializeRestore(child);
        }
    }

	if (!Path.IsEmpty())
	{
		this->demFileName = CSystemUtilities::AbsPath(this->demFileName);
		CSystemUtilities::chDir(CWD);
	}


	if (refRead) 
	{
		double gridX = (this->dem.getmaxX() - this->dem.getminX()) / (double (this->dem.getWidth() - 1));
		double gridY = (this->dem.getmaxY() - this->dem.getminY()) / (double (this->dem.getHeight() - 1));

		C2DPoint shift(this->dem.getminX(),this->dem.getmaxY()); 
		this->dem.setTFW(shift,C2DPoint(gridX,gridY));
		this->dem.getTFW()->setRefSys(ref);
	}
}

/**
 * reconnects pointers
 */
void CVDEM::reconnectPointers()
{
    CSerializable::reconnectPointers();
}

/**
 * resets pointers
 */
void CVDEM::resetPointers()
{
    CSerializable::resetPointers();
}


CFileName CVDEM::getItemText()
{
    CFileName itemtext = this->demFileName.GetFullFileName();
    return itemtext.GetChar();
}


void CVDEM::setReferenceSystem(const CReferenceSystem &referenceSystem)
{
	this->dem.getTFW()->setRefSys(referenceSystem);
	
	this->informListeners_elementsChanged();
};


bool CVDEM::cropfromDEM(CVDEM* vdem,double ulx,double uly,double width,double height)
{
	// crop image contents
	CCharString hugname(this->demFileName);
	bool ret = this->dem.cropImage(hugname.GetChar(), vdem->getDEM(), ulx, uly, width, height);

	// crop attached TFW parameters
	this->dem.cropTFW(*vdem->dem.getTFW(), ulx, uly, width, height);
	
	return true;
}


bool CVDEM::computeDEMStatistics(	int nRows,
									int nCols,
									bool showAvg,
									bool showDevAvg,
									bool showMin,
									bool showMax,
									bool showDev,
									int *stattextColor,
									int *statlineColor,
									int statFontSize,
									int nLegendeClasses,
									bool computeHist)
{
	
	this->showAvg = showAvg;
	this->showDevAvg = showDevAvg;
	this->showMin = showMin;
	this->showMax = showMax;
	this->showDev = showDev;
	for (int i = 0; i < 3; ++i) this->statTextColor[i] = stattextColor[i];
	for (int i = 0; i < 3; ++i) this->statLineColor[i] = statlineColor[i];
	this->statFontSize = statFontSize;
	this->nLegendeClasses = nLegendeClasses;
	this->computeHist = computeHist;

	float maxHistValue = this->dem.getHistogram()->getMax();
	float minHistValue = this->dem.getHistogram()->getMin();



	vector<float> histClasses;
	if (this->computeHist)
	{
		CLookupTableHelper helper;
		helper.computeFloatClasses(histClasses,this->nLegendeClasses,minHistValue,this->zeroValue,maxHistValue);
	}

	this->statHist.init(histClasses);


	if (this->nStatisticRows != nRows || this->nStatisticCols != nCols ||this->computeHist)
	{
		this->demStatistics.RemoveAll();
		this->nStatisticRows = nRows;
		this->nStatisticCols = nCols;

		int width = this->dem.getWidth() / this->nStatisticCols;
		int height = this->dem.getHeight() / this->nStatisticRows;


		vector<double> avg,rms,min,max,dev;
		for (int row=0; row < nRows; row++)
		{
			int h = height;
			if ((row + 1) == nRows)
				h += this->dem.getHeight() - (row+1)*height; // adjust last row height

			for (int col=0;col < nCols; col++)
			{
				int w = width;
				if ((col+1) == nCols)
					w +=  this->dem.getWidth() -(col+1)*width; // adjust last col widht

				this->dem.computeStatistics(avg,rms,min,max,dev,this->statHist,row*height,col*width,h,w);
				CObsPoint stat(5);
				stat[0] = avg[0];
				stat[1] = rms[0];
				stat[2] = min[0];
				stat[3] = max[0];
				stat[4] = dev[0];

				this->demStatistics.Add(stat);
			}
		}
	}

	this->informListeners_elementDeleted("DEMStatistics");
	this->informListeners_elementsChanged("DEMStatistics");

	return false;
}

void CVDEM::deleteDEMStatistics()
{
	this->nStatisticRows = -1;
	this->nStatisticCols = -1;
	this->demStatistics.RemoveAll();

	this->informListeners_elementDeleted("DEMStatistics");

	if (this->legend) 
	{
		delete this->legend;
		this->legend = 0;
	}
}

