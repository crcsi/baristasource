#include <float.h>
#include "AlosReader.h"

#include "QuaternionRotation.h"
#include "OmegaPhiKappaRotation.h"
#include "DistortionALOS.h"
#include "SystemUtilities.h"
#include "PushbroomScanner.h"
#include "BaristaProject.h"


#define TIME_PER_LINE_PRISM  0.00037025  // from JAXA
#define TIME_PER_LINE_AVNIR2 0.001479842 // computed from firstLineTime and sceneCenterTime!!!
#define FOCAL_LENGTH_PRISM 2000.0

#define BACKWARD "ALOS-Backward"
#define NADIR "ALOS-Nadir"
#define WIDE "ALOS-Wide"
#define FORWARD "ALOS-Forward"
#define AVNIR2 "AVNIR-2"

#define STDDEV_SHIFT_PRISM 10
#define STDDEV_SHIFT_AVNIR2 40

#define STDDEV_ROT_PRISM 1.4e-2
#define STDDEV_ROT_AVNIR2 5.6e-2

CAlosReader::CAlosReader(void): CSatelliteDataReader(),
		nTaiUTCData(0),directReadMode(false),readAVNIR(false),
		readNITF(false),readBuffer(0),bufferLength(0)
{

	//this->initOldALOSCamera();
	this->initNewALOSCamera();
	this->allocateMemory(READBUFFER_LENGTH);

}

CAlosReader::CAlosReader(bool directReadMode): CSatelliteDataReader(),
		nTaiUTCData(0),directReadMode(directReadMode),readBuffer(0),
		bufferLength(0),readAVNIR(false),readNITF(false)
{

	//this->initOldALOSCamera();
	this->initNewALOSCamera();
	this->allocateMemory(READBUFFER_LENGTH);


}
CAlosReader::~CAlosReader(void)
{
	this->closeFile();
}


bool CAlosReader::readData(CFileName &filename)
{
	if (this->listener)
	{
		this->listener->setProgressValue(20);
		this->listener->setTitleString("Reading Dataset ...");
	}

	int ccdElement = -1;

	// for 1A, 1B1 and 1B2 levels
	if (!this->readCommonPart(filename,ccdElement))
		return false;

	// we are done with level 1B2 here, continue only for 1A and 1B1
	if (this->satInfo.processingLevel.CompareNoCase("1A") || 
		this->satInfo.processingLevel.CompareNoCase("1B1"))
	{
		
		// read the image next
		if (!this->readImageFile())
			return false;

		// compute according camera
		if (ccdElement != -1)
		{
			if (this->satInfo.viewDirection.CompareNoCase(BACKWARD))
			{
				this->computeInnerOrienation(this->backwardCamera,ccdElement);
				CCharString format = CCharString(BACKWARD) + "-%d";
				this->satInfo.cameraName.Format(format,ccdElement);
			}
			else if (this->satInfo.viewDirection.CompareNoCase(NADIR))
			{
				this->computeInnerOrienation(this->nadirCamera,ccdElement);
				CCharString format = CCharString(NADIR) + "-%d";
				this->satInfo.cameraName.Format(format,ccdElement);
			}
			else if (this->satInfo.viewDirection.CompareNoCase(FORWARD))
			{
				this->computeInnerOrienation(this->forwardCamera,ccdElement);
				CCharString format = CCharString(FORWARD) + "-%d";
				this->satInfo.cameraName.Format(format,ccdElement);
			}
			else if (this->satInfo.viewDirection.CompareNoCase(WIDE))
			{
				this->computeInnerOrienation(this->nadirCamera,ccdElement);
				CCharString format = CCharString(WIDE) + "-%d";
				this->satInfo.cameraName.Format(format,ccdElement);
			}
			else if (this->satInfo.viewDirection.CompareNoCase(AVNIR2))
			{
				CCharString format = CCharString(AVNIR2) + "-%d";
				this->satInfo.cameraName.Format(format,ccdElement);
				this->satInfo.viewDirection = this->satInfo.viewDirection + "_" + this->satInfo.imageID;
			}
			else
			{
				this->errorMessage = "Wrong DataSet, unkonw camera!";
				return false;
			}
		}
		else
		{
			this->errorMessage = "Wrong DataSet!";
			return false;
		}

		this->camera->setNCols(this->satInfo.nCols);
		this->camera->setNRows(this->satInfo.nRows);

		// compute or adjust the time
		if (this->readNITF)
		{
			// we don't have the firstLineTime so we just compute the time from scene centre time
			double halfTime = this->camera->getNRows()/2 * this->timePerLine;
			this->firstLineTime = this->sceneCenterTime - halfTime;
			this->firstLineDate = this->sceneCenterDate;

			if (this->firstLineTime < 0) // scene starts previous day
			{
				this->firstLineDate -= 1;
				this->firstLineTime += 24*60*60;	
			}
		}
		else
		{
			if (this->firstLineTime < this->sceneCenterTime) // on the same day
				this->firstLineDate = this->sceneCenterDate;
			else
			{
				this->firstLineDate = this->sceneCenterDate - 1;
				this->sceneCenterTime += 24*60*60;
			}
		}

		// time of last time in image, used to determine orbit elements
		this->lastLineTime = this->firstLineTime + this->camera->getNRows() * this->timePerLine;

		// read orbit elements
		if (!this->readSupplFile())
			return false;


		// compute spline for orbit path and attitudes
		if (this->listener)
		{
			this->setMaxListenerValue(99);
			this->listener->setTitleString("Computing Orbit ...");
		}

		double startTime,endTime;
		this->computeInterval(startTime,endTime);

		if(!this->adjustOrbit(true,true,startTime,endTime))
			return false;

		if (this->listener)
		{
			this->listener->setProgressValue(100.0);
			this->listener->setTitleString("Finish Reading");
		}
	}

	return true;
}



bool CAlosReader::readVolumeFile()
{
	// no volume file when NITF files are used
	if (!this->readNITF)
	{
		if (!this->openFile(this->volumeFile))
			return false;

		CCharString erg;
		this->skip(16);
		this->readString(12,erg);

		// check out the camera used, we can only read PRISM and AVNIR-2
		if (erg.CompareNoCase("CEOS-PSM-CCT"))
		{
			this->readAVNIR = false;
			this->satInfo.imageType = "P";
			this->timePerLine = TIME_PER_LINE_PRISM;
		}
		else if (erg.CompareNoCase("CEOS-AV2-CCT"))
		{
			this->readAVNIR = true;
			this->satInfo.imageType = "Multi";
			this->timePerLine = TIME_PER_LINE_AVNIR2;
		}
		else
		{
			this->closeFile();
			this->errorMessage = "Unsupported sensor found!";
			return false;
		}


		this->goToPos(160);
		int fileRecords =0;
		this->readInt(4,fileRecords);
		this->skip(196); // end of volume descriptor

		CCharString testString;


		// read the first fileRecord, tells us which view direction and processing level is beeing used
		this->skip(20);
		this->readString(2,testString);
		if (!testString.CompareNoCase("AL"))
		{
			this->closeFile();
			this->errorMessage = "Wrong DataSet! ALOS Satellite expected";
			return false;
		}

		this->skip(1);
		this->readString(3,testString);
		
		if (!(!this->readAVNIR && testString.CompareNoCase("PSM")) &&
			!( this->readAVNIR && testString.CompareNoCase("AV2")))
		{
			this->closeFile();
			this->errorMessage = "Wrong DataSet! Only PRISM and AVNIR-2 sensor are supported";
			return false;
		}


		this->readString(1,testString);
		if (testString.CompareNoCase("N"))
			this->satInfo.viewDirection = NADIR;
		else if (testString.CompareNoCase("F"))
			this->satInfo.viewDirection = FORWARD;
		else if (testString.CompareNoCase("B"))
			this->satInfo.viewDirection = BACKWARD;
		else if (testString.CompareNoCase("W"))
			this->satInfo.viewDirection = WIDE;
		else if (testString.CompareNoCase("A"))
			this->satInfo.viewDirection = AVNIR2;
		else
		{
			this->errorMessage = "Unsupported camera found!";
			this->closeFile();
			return false;
		}

		this->readString(1,testString);
		if (testString.CompareNoCase("0"))
			this->satInfo.processingLevel = "1A";
		else if (testString.CompareNoCase("1"))
			this->satInfo.processingLevel = "1B1";
		else if (testString.CompareNoCase("2"))
			this->satInfo.processingLevel = "1B2";
		else
		{
			this->errorMessage = "Only Level 1A, 1B1 and 1B2 are supported!";
			this->closeFile();
			return false;
		}
	}
	else
	{
		// for NITF files we extract the needed information from the filename
		// check out the camera used, we can only read PRISM and AVNIR-2
		if (this->imageFile.Find("ALPSM") > 0)
		{
			this->readAVNIR = false;
			this->satInfo.imageType = "P";
			this->timePerLine = TIME_PER_LINE_PRISM;
		}
		else if (this->imageFile.Find("ALAV2") > 0)
		{
			this->readAVNIR = true;
			this->satInfo.imageType = "Multi";
			this->timePerLine = TIME_PER_LINE_AVNIR2;
		}
		else
		{
			this->closeFile();
			this->errorMessage = "Unsupported sensor found!";
			return false;
		}	


		if (this->imageFile.Find("__N") > 0)
			this->satInfo.viewDirection = NADIR;
		else if (this->imageFile.Find("__F") > 0)
			this->satInfo.viewDirection = FORWARD;
		else if (this->imageFile.Find("__B") > 0)
			this->satInfo.viewDirection = BACKWARD;
		else if (this->imageFile.Find("__W") > 0)
			this->satInfo.viewDirection = WIDE;
		else if (this->imageFile.Find("__A") > 0)
			this->satInfo.viewDirection = AVNIR2;
		else
		{
			this->errorMessage = "Unsupported camera found!";
			return false;
		}

		if (this->imageFile.Find("O1B2R") > 0 || this->imageFile.Find("O1B2G") > 0)
			this->satInfo.processingLevel = "1B2";
		else if (this->imageFile.Find("O1A1") > 0)
			this->satInfo.processingLevel = "1A";
		else if (this->imageFile.Find("O1B1") > 0 )
			this->satInfo.processingLevel = "1B1";
		else
		{
			this->errorMessage = "Only Level 1A, 1B1 and 1B2 are supported!";
			this->closeFile();
			return false;
		}
	}

	this->closeFile();
	return true;
}


bool CAlosReader::readImageFile()
{
	if (!this->readNITF)
	{
		if (!this->openFile(this->imageFile))
			return false;

		if (!this->readFileDescriptor())
		{
			this->closeFile();
			return false;
		}

		int recordLength=0;
		this->skip(6);
		this->readInt(6,recordLength);
		this->goToPos(recordLength);
		this->skip(16);
		unsigned int CCD;
		this->readBinary(10,CCD);
		unsigned int startTimeMilSec;
		unsigned short startTimeMicSec;
		this->readBinary(10,startTimeMilSec);
		this->readBinary(10,startTimeMicSec);

		this->firstLineTime = startTimeMilSec / 1000.0 + startTimeMicSec / 1000000.0;
		this->closeFile();
	}

	return true;
}

bool CAlosReader::readLeaderFile()
{
	if (!this->openFile(this->leaderFile))
		return false;

	// the specific descriptor
	if (!this->readLeaderFileDescriptor())
	{
		this->closeFile();
		return false;
	}

	// scene header
	if (!this->readSceneHeader())
	{
		this->closeFile();
		return false;
	}




	return this->closeFile();

}


bool CAlosReader::readSupplFile()
{
	if (!this->openFile(this->supplFile))
		return false;

	// the specific descriptor
	this->readSupplFileDescripor();

	if (!this->readAncillary4())
	{
		this->closeFile();
		return false;
	}

	if (!this->readAncillary5())
	{
		this->closeFile();
		return false;
	}

	if (!this->readAncillary6())
	{
		this->closeFile();
		return false;
	}

	if (!this->readAncillary7())
	{
		this->closeFile();
		return false;
	}

	if (!this->readAncillary8())
	{
		this->closeFile();
		return false;
	}

	if (!this->readAncillary9())
	{
		this->closeFile();
		return false;
	}

	if (!this->readAncillary10())
	{
		this->closeFile();
		return false;
	}

	if (!this->readAncillary11())
	{
		this->closeFile();
		return false;
	}

	if (!this->readAncillary12(false,false))
	{
		this->closeFile();
		return false;
	}

	if (!this->directReadMode)
	{
		bool ret = false;
		if (!this->readAVNIR)
			ret = this->readAncillary13_PRISM();
		else if (this->readAVNIR)
			ret = this->readAncillary13_AVNIR();
		
		if (!ret)
		{
			this->closeFile(); //
			return false;
		}
	}

	return this->closeFile();
}


bool CAlosReader::readFileDescriptor()
{
	this->skip(8);
	unsigned int length;

	this->readBinary(10,length,false);

	this->skip(32);
	int test;
	this->readInt(4,test);

//	if (test!= expectedFile)
//		return false;

	this->skip(132);
	return true;
}


bool CAlosReader::readSupplFileDescripor()
{
	if (!this->readFileDescriptor())
	{
		return false;
	}

	// Ancillary 4
	this->readInt(6,this->nAncillary4);
	this->readInt(8,this->lengthAncillary4);

	// Ancillary 5
	this->readInt(6,this->nAncillary5);
	this->readInt(8,this->lengthAncillary5);

	// Ancillary 6
	this->readInt(6,this->nAncillary6);
	this->readInt(8,this->lengthAncillary6);

	// Ancillary 7
	this->readInt(6,this->nAncillary7);
	this->readInt(8,this->lengthAncillary7);

	// Ancillary 8
	this->readInt(6,this->nAncillary8);
	this->readInt(8,this->lengthAncillary8);

	// Ancillary 9
	this->readInt(6,this->nAncillary9);
	this->readInt(8,this->lengthAncillary9);

	// Ancillary 10
	this->readInt(6,this->nAncillary10);
	this->readInt(8,this->lengthAncillary10);

	// Ancillary 11
	this->readInt(6,this->nAncillary11);
	this->readInt(8,this->lengthAncillary11);

	// Ancillary 12
	this->readInt(6,this->nAncillary12);
	this->readInt(8,this->lengthAncillary12);

	// Ancillary 13
	this->readInt(6,this->nAncillary13);
	this->readInt(8,this->lengthAncillary13);

	this->skip(4360); // end of file descriptor record

	return true;
}


bool CAlosReader::readLeaderFileDescriptor()
{
	if (!this->readFileDescriptor())
	{
		return false;
	}

	this->skip(4500);
	return true;
}


unsigned int julianDayFromGregorianDate(int year, int month, int day)
{
    // Gregorian calendar starting from October 15, 1582
    // Algorithm from Henry F. Fliegel and Thomas C. Van Flandern
    return (1461 * (year + 4800 + (month - 14) / 12)) / 4
           + (367 * (month - 2 - 12 * ((month - 14) / 12))) / 12
           - (3 * ((year + 4900 + (month - 14) / 12) / 100)) / 4
           + day - 32075;
}
unsigned int julianDayFromDate(int year, int month, int day)
{
    if (year < 0)
        ++year;

    if (year > 1582 || (year == 1582 && (month > 10 || (month == 10 && day >= 15)))) {
        return julianDayFromGregorianDate(year, month, day);
    } else if (year < 1582 || (year == 1582 && (month < 10 || (month == 10 && day <= 4)))) {
        // Julian calendar until October 4, 1582
        // Algorithm from Frequently Asked Questions about Calendars by Claus Toendering
        int a = (14 - month) / 12;
        return (153 * (month + (12 * a) - 3) + 2) / 5
               + (1461 * (year + 4800 - a)) / 4
               + day - 32083;
    } else {
        // the day following October 4, 1582 is October 15, 1582
        return 0;
    }
}

bool CAlosReader::readSceneHeader()
{
	this->skip(41);

	this->skip(75);
/*	this->skip(11);
	double lat,lon;
	this->readDouble(16,lat);
	this->readDouble(16,lon);
	this->skip(32);
*/
	int year=0,month=0,day=0;
	this->readDate(year,month,day);
	this->sceneCenterDate = julianDayFromDate(year,month,day);

	int hour=0,min=0;
	double sec = 0.0;
	this->readTime(hour,min,sec);
	this->skip(12);
	this->sceneCenterTime = hour*3600 + min*60 + sec;
	int timeOffset;
	this->readInt(16,timeOffset);
//	this->skip(1264);

	this ->skip(48);
	double latB2,lonB2;
	double nRowsB2,nColsB2;
	this->readDouble(16,latB2);
	this->readDouble(16,lonB2);
	this->readDouble(16,nColsB2);
	this->readDouble(16,nRowsB2);

	this->skip(1264 -48 - 4*16 );



	this->readInt(16,this->satInfo.nCols);
	this->readInt(16,this->satInfo.nRows);

	this->skip(64);

	CCharString level1B2Option;
	CCharString mapProjection;
	this->readString(16,level1B2Option);
	level1B2Option.trim();

	if (level1B2Option.CompareNoCase("R"))
	{
	}
	else if (level1B2Option.CompareNoCase("G"))
	{

	}


	this->skip(16);
	this->readString(16,mapProjection);

	mapProjection.trim();

	if (this->satInfo.processingLevel.CompareNoCase("1B2"))
	{
		if (!mapProjection.CompareNoCase("YNNNN"))
		{
			this->errorMessage = "Only UTM map projection is supported!";
			return false;
		}
	}

	int correctionLevel;
	this->readInt(16,correctionLevel);
	this->skip(3092); // end of scene header record

	if (this->satInfo.processingLevel.CompareNoCase("1B2"))
		if (!this->readAncillary1())
			return false;


	return true;
}



bool CAlosReader::readAncillary1()
{
	this->skip(92);

	int hemisphere;
	int zone;
	this->readInt(4,hemisphere);
	this->readInt(12,zone);
	this->skip(32);

	double northing,easting;
	this->readDouble(16,northing);
	this->readDouble(16,easting);

	this->skip(32);

	double trueNorthAngle;
	this->readDouble(16,trueNorthAngle);

	this->skip(736);
	// coefficients

	this->coefficientsPhi.resize(10);
	this->coefficientsLamda.resize(10);
	this->coefficientsSample.resize(10);
	this->coefficientsLine.resize(10);

	this->coefficientsF4.resize(6);

	for (int i=0; i < 10 ; i++)
	{
		this->readDouble(24,this->coefficientsPhi[i]);
	}

	for (int i=0; i < 10 ; i++)
	{
		this->readDouble(24,this->coefficientsLamda[i]);
	}

	for (int i=0; i < 10 ; i++)
	{
		this->readDouble(24,this->coefficientsSample[i]);
	}

	for (int i=0; i < 10 ; i++)
	{
		this->readDouble(24,this->coefficientsLine[i]);
	}


	for (int i=0; i < 6 ; i++)
	{
		this->readBinary(10,this->coefficientsF4[i],true);
	}

	this->computeRPC(zone,hemisphere);
	this->computeTFW(easting,northing,zone,hemisphere);
	return true;
}

bool CAlosReader::computeRPC(int zone, int hemisphere)
{

	Matrix  parms(99, 1);

	parms.element(0, 0) = 0.0;
	parms.element(1, 0) = 0.0;
	parms.element(2, 0) = this->coefficientsLamda[0];
	parms.element(3, 0) = this->coefficientsPhi[0];
	parms.element(4, 0) = 0.0;
    parms.element(5, 0) = 1.0;
    parms.element(6, 0) = 1.0;
    parms.element(7, 0) = 1.0;
    parms.element(8, 0) = 1.0;
    parms.element(9, 0) = 1.0;

    parms.element(10, 0) = this->coefficientsLine[0];
    parms.element(11, 0) = this->coefficientsLine[2];
    parms.element(12, 0) = this->coefficientsLine[1];
    parms.element(13, 0) = 0.0;
    parms.element(14, 0) = this->coefficientsLine[3];
    parms.element(15, 0) = 0.0;
    parms.element(16, 0) = 0.0;
    parms.element(17, 0) = this->coefficientsLine[5];
    parms.element(18, 0) = this->coefficientsLine[4];
    parms.element(19, 0) = 0.0;
    parms.element(20, 0) = 0.0;
    parms.element(21, 0) = this->coefficientsLine[9];
    parms.element(22, 0) = coefficientsLine[6];
    parms.element(23, 0) = 0.0;
    parms.element(24, 0) = this->coefficientsLine[7];
    parms.element(25, 0) = this->coefficientsLine[8];
    parms.element(26, 0) = 0.0;
    parms.element(27, 0) = 0.0;
    parms.element(28, 0) = 0.0;
    parms.element(29, 0) = 0.0;


    parms.element(30, 0) = 1.0;
    parms.element(31, 0) = 0.0;
    parms.element(32, 0) = 0.0;
    parms.element(33, 0) = 0.0;
    parms.element(34, 0) = 0.0;
    parms.element(35, 0) = 0.0;
    parms.element(36, 0) = 0.0;
    parms.element(37, 0) = 0.0;
    parms.element(38, 0) = 0.0;
    parms.element(39, 0) = 0.0;
    parms.element(40, 0) = 0.0;
    parms.element(41, 0) = 0.0;
    parms.element(42, 0) = 0.0;
    parms.element(43, 0) = 0.0;
    parms.element(44, 0) = 0.0;
    parms.element(45, 0) = 0.0;
    parms.element(46, 0) = 0.0;
    parms.element(47, 0) = 0.0;
    parms.element(48, 0) = 0.0;
    parms.element(49, 0) = 0.0;

    parms.element(50, 0) = this->coefficientsSample[0];
    parms.element(51, 0) = this->coefficientsSample[2];
    parms.element(52, 0) = this->coefficientsSample[1];
    parms.element(53, 0) = 0.0;
    parms.element(54, 0) = this->coefficientsSample[3];
    parms.element(55, 0) = 0.0;
    parms.element(56, 0) = 0.0;
    parms.element(57, 0) = this->coefficientsSample[5];
    parms.element(58, 0) = this->coefficientsSample[4];
    parms.element(59, 0) = 0.0;
    parms.element(60, 0) = 0.0;
    parms.element(61, 0) = this->coefficientsSample[9];
    parms.element(62, 0) = this->coefficientsSample[6];
    parms.element(63, 0) = 0.0;
    parms.element(64, 0) = this->coefficientsSample[7];
    parms.element(65, 0) = this->coefficientsSample[8];
    parms.element(66, 0) = 0.0;
    parms.element(67, 0) = 0.0;
    parms.element(68, 0) = 0.0;
    parms.element(69, 0) = 0.0;

    parms.element(70, 0) = 1.0;
    parms.element(71, 0) = 0.0;
    parms.element(72, 0) = 0.0;
    parms.element(73, 0) = 0.0;
    parms.element(74, 0) = 0.0;
    parms.element(75, 0) = 0.0;
    parms.element(76, 0) = 0.0;
    parms.element(77, 0) = 0.0;
    parms.element(78, 0) = 0.0;
    parms.element(79, 0) = 0.0;
    parms.element(80, 0) = 0.0;
    parms.element(81, 0) = 0.0;
    parms.element(82, 0) = 0.0;
    parms.element(83, 0) = 0.0;
    parms.element(84, 0) = 0.0;
    parms.element(85, 0) = 0.0;
    parms.element(86, 0) = 0.0;
    parms.element(87, 0) = 0.0;
    parms.element(88, 0) = 0.0;
    parms.element(89, 0) = 0.0;

	this->rpc.setAll(parms);

	return true;
}

bool CAlosReader::computeTFW(double easting, double northing,int zone, int hemisphere)
{
	double xc = double (this->satInfo.nCols) * 0.5;
	double yc = double (this->satInfo.nRows) * 0.5;
	Matrix m(2,2);
	m.element(0,0) = this->coefficientsF4[0] * 0.001;  m.element(0,1) = this->coefficientsF4[1] * 0.001;
	m.element(1,0) = this->coefficientsF4[2] * 0.001;  m.element(1,1) = this->coefficientsF4[3] * 0.001;
	double det = m.Determinant();
	if (fabs(det) > FLT_EPSILON)
	{
		m = m.i();
		Matrix parms(6,1);

		parms.element(0, 0) = m.element(0,0);
		parms.element(1, 0) = m.element(0,1);
		parms.element(3, 0) = m.element(1,0);
		parms.element(4, 0) = m.element(1,1);
		parms.element(2, 0) = easting  * 1000.0 -(m.element(0,0) * xc + m.element(0,1) * yc);
		parms.element(5, 0) = northing * 1000.0 -(m.element(1,0) * xc + m.element(1,1) * yc);

		tfw.setAll(parms);
		CReferenceSystem refSys(WGS84Spheroid,zone,hemisphere);
		refSys.setCoordinateType(eGRID);
		tfw.setRefSys(refSys);
	}
	else
		return false;

	return true;
}


bool CAlosReader::readAncillary2()
{
	return true;
}

bool CAlosReader::readAncillary3()
{
	return true;
}

bool CAlosReader::readAncillary4()
{
	this->skip(this->lengthAncillary4);
	return true;
}

bool CAlosReader::readAncillary5()
{
	this->skip(this->lengthAncillary5);
	return true;
}

bool CAlosReader::readAncillary6()
{
	this->skip(this->lengthAncillary6);
	return true;
}

bool CAlosReader::readAncillary7()
{
	this->skip(this->lengthAncillary7);
	return true;
}

bool CAlosReader::readAncillary8(bool readSeparateFile)
{
	// read ECI data
	//this->skip(24); // now we are at field 8, the header (ECI)
	//this->readOrbitPathSection((CXYZOrbitPointArray*)this->orbitPoints->getObsPoints()); // read ECI orbit
	//this->skip(264500 - 832 - 170*this->nTaiUTCData - (this->nOrbitPoints)*this->nOrbitPointLength);

	// skip ECI data
	if (!readSeparateFile)
		this->skip(264500); 

	this->orbitPoints->getObsPoints()->SetSize(0);
	this->tmpOrbitPoints.SetSize(0);
	this->readOrbitPathSection(&this->tmpOrbitPoints); // read ECR orbit
	
	// go to the end of that Ancillary
	if (!readSeparateFile)
		this->skip(264500 - 808 - 170*this->nTaiUTCData - (this->nOrbitPoints)*this->nOrbitPointLength);


	return true;
}

bool CAlosReader::readOrbitPathSection(CXYZOrbitPointArray* pointArray)
{
	this->skip(46);
	this->readInt(4,this->nOrbitPointLength);
	this->skip(38);
	CCharString cs;
	this->readString(3,cs); // either ECI or ECR

	this->skip(37 + 170*2 + 30);


	this->readInt(3,this->nTaiUTCData);
	this->skip(7);
	this->readInt(8,this->nOrbitPoints);
	this->skip(122);

	// field 12
	this->skip(60);
	CCharString accuracy;
	this->readString(1,accuracy);
	this->skip(109);

	// field 13 - 14
	int year=0,month=0,day=0;
	for (int i =0; i < nTaiUTCData; i++)
	{
		this->readDate(year,month,day);
		this->skip(2);
		this->readInt(3,this->timeOffsetTAI_UTC);
		this->skip(157);
	}



	// orbit data
//	FILE* out = fopen("ALOSpath.txt","w");
//	fprintf(out,"first Line date: %10d\n",this->firstLineDate);
//	fprintf(out,"first Line time: %10.3lf\n",this->firstLineTime);

	int hour = 0,min =0;
	double sec = 0.0;
	double time = 0.0;

	double startTime,endTime;
	this->getStartAndEndTime(startTime,endTime);

	for (int i=0; i < this->nOrbitPoints; i++)
	{
		// read the date and time
		this->readDate(year,month,day);
//		fprintf(out,"%4d %2d %2d",year,month,day);

		int dateJulian = this->julianDay(year,month,day);
		this->skip(2);
		this->readTime(hour,min,sec,true);
		this->skip(2);

		// transform the date and time
		time = this->transformTime(dateJulian,hour,min,sec);

//		fprintf(out,"  %10d %10.2lf",dateJulian,time);
		if (time < startTime || time > endTime)
		{
//			fprintf(out,"\n");
			this->skip(6*24 + 1);
			continue;
		}
		CXYZOrbitPoint* p = pointArray->add();
		p->dot = time;

		this->readDouble(24,(*p)[0]);
		(*p)[0] *=1000.0;
		this->readDouble(24,(*p)[1]);
		(*p)[1] *=1000.0;
		this->readDouble(24,(*p)[2]);
		(*p)[2] *=1000.0;
		this->readDouble(24,(*p)[3]);
		(*p)[3] *=1000.0;
		this->readDouble(24,(*p)[4]);
		(*p)[4] *=1000.0;
		this->readDouble(24,(*p)[5]);
		(*p)[5] *=1000.0;
		this->skip(1);

//	fprintf(out," %20.10lf %20.10lf %20.10lf %20.10lf %20.10lf %20.10lf\n",(*p)[0],(*p)[1],(*p)[2],(*p)[3],(*p)[4],(*p)[5]);
	}
//	fclose(out);

	return true;
}


bool CAlosReader::readAncillary9()
{
	this->skip(this->lengthAncillary9);
	return true;
}


bool CAlosReader::readAncillary10()
{

	this->skip(24 + 128 + 63 + 61 + 30);

	int nTaiUtcData=0;
	int nTransformationData=0;
	this->readInt(10,nTaiUtcData);
	this->readInt(10,nTransformationData);
	this->skip(1);
	

	// read the GAST date
	int gastYear,gastMonth,gastDay,gastHour,gastMin;
	double gastSec;
	this->readDate(gastYear,gastMonth,gastDay);
	int gastDate = this->julianDay(gastYear,gastMonth,gastDay);
	this->skip(2);

	// read GAST time
	this->readTime(gastHour,gastMin,gastSec,true);
	this->gastTime = this->transformTime(gastDate,gastHour,gastMin,gastSec);
	this->skip(2);

	// read GAST angle and velocity
	this->readDouble(24,this->gastAngle);
	this->readDouble(24,this->gastVelocity);
	this->skip(1);

	double startTime,endTime;
	this->getStartAndEndTime(startTime,endTime);


	for (int i=0; i < nTaiUtcData; i++)
	{
		this->skip(21);
	}

	this->PNMatrix.clear();
	this->XYMatrix.clear();
	this->matrixTime.clear();

	CRotationMatrix rotMatPN;
	CRotationMatrix rotMatXY;

	int year,month,day,hour,min,date;
	double sec,time;

	for (int i=0; i < nTransformationData; i++)
	{

		this->readDate(year,month,day);
		date = this->julianDay(year,month,day);
		this->skip(2);
		this->readTime(hour,min,sec,true);
		this->skip(3);
		time = this->transformTime(date,hour,min,sec);

		if (time < startTime || time > endTime)
		{
			this->skip(18*24 + 6);
			continue;
		}

		// matrix xy
		for (int k=0; k < 3; k++)
		{
			this->readDouble(24,rotMatXY(0,k));
			this->readDouble(24,rotMatXY(1,k));
			this->readDouble(24,rotMatXY(2,k));
			this->skip(1);
		}

		// matrix pn
		for (int k=0; k < 3; k++)
		{
			this->readDouble(24,rotMatPN(0,k));
			this->readDouble(24,rotMatPN(1,k));
			this->readDouble(24,rotMatPN(2,k));
			this->skip(1);
		}

		this->PNMatrix.push_back(rotMatPN);
		this->XYMatrix.push_back(rotMatXY);
		this->matrixTime.push_back(time);

	}


	this->skip(this->lengthAncillary10 - 24 - 128 - 63 - 61 - 30 - 20 - 26 - 24 -24 - 1 - 21*nTaiUtcData - (73*6+26) *  nTransformationData);
	return true;
}


bool CAlosReader::readAncillary11()
{

	this->skip(24);
	CCharString test;
	this->readString(128,test);

	this->skip(this->lengthAncillary11 - 24 - 128);
	return true;
}


bool CAlosReader::readAncillary12(bool readSeparateFile,bool appendData)
{
	if (!readSeparateFile)
		this->skip(24);

	CCharString test;
	this->readString(128,test); // the header

	this->skip(2);

	this->readInt(5,this->nAttitudePoints);
	
	int skipValue;
	if (readSeparateFile)
	{
		// ALOSPPP file
		// these files do not have the drift rate values, so 12 byte less
		this->skip(68);
		skipValue = 3; 
		
		//ALOSPAD file
		//this->skip(67);
		//skipValue = 15; 
	}
	else
	{
		this->skip(67);
		//this->readString(67,test);
		skipValue = 15;
	}

	

	unsigned char month,day,hour,min;
	unsigned short year;
	double length;
	int date;
	double time,sec;

	double startTime,endTime;
	this->getStartAndEndTime(startTime,endTime,true);


	// determine PN and XY matrix, we use the first one after startTime
	bool found = false;
	int index=0;
	for (index =0; index<this->XYMatrix.size(); index++)
	{
		if (this->matrixTime[index] > startTime)
		{
			found = true;
			break;
		}
	}

	if (!found)
	{
		this->errorMessage = "DataSet wrong!";
		return false;
	}

	CObsPoint q(4);
	CQuaternionRotation M(&q,eQALOS);
	CRotationMatrix rotMatU,tmpMat1,tmpMat2,erg;
	
	if (!appendData)
	{
		this->AttitudeMatrix.clear();
		this->attitudeTime.clear();
	}

	CRotationMatrix rotX(1,0,0,0,-1,0,0,0,-1);
//	FILE* out = fopen("ALOSattitudes.txt","w");
//	fprintf(out,"first Line date: %10d\n",this->firstLineDate);
//	fprintf(out,"first Line time: %10.3lf\n",this->firstLineTime);
	for (int i=0; i<this->nAttitudePoints; i++)
	{
		this->readBinary(10,year,false);	//2
		this->readBinary(10,month); //1
		this->readBinary(10,day); //1
		date = this->julianDay(year,month,day);

		this->readBinary(10,hour); //1
		this->readBinary(10,min);  //1
		this->readBinary(10,sec);  //8
		time = this->transformTime(date,hour,min,sec);
		this->skip(11);

		if (time < startTime || time > endTime)
		{
			this->skip(4*8 + skipValue);
			continue;
		}

		this->readBinary(10,q[0]);//8
		this->readBinary(10,q[1]);//8
		this->readBinary(10,q[2]);//8
		this->readBinary(10,q[3]);//8
		
		this->skip(skipValue);

		M.updateAngles_Rho(&q);


		double theta = this->gastAngle + this->gastVelocity*(time - this->gastTime);
		theta *=M_PI/180.0;
		rotMatU(0,0) = cos(-theta);	rotMatU(0,1) = -sin(-theta); rotMatU(0,2) = 0;
		rotMatU(1,0) = sin(-theta); rotMatU(1,1) =  cos(-theta); rotMatU(1,2) = 0;
		rotMatU(2,0) = 	   0;		rotMatU(2,1) =      0;		 rotMatU(2,2) = 1;

		// A = XY(index(tstart)) * U(t) * PN(index(tstart)) * M(q(t))
		this->PNMatrix[index].R_times_R(tmpMat1,M.getRotMat());
		rotMatU.R_times_R(tmpMat2,tmpMat1);
		this->XYMatrix[index].R_times_R(tmpMat1,tmpMat2);
		tmpMat1.R_times_R(tmpMat2,rotX);
		this->AttitudeMatrix.push_back(tmpMat2);
		this->attitudeTime.push_back(time);

//		CRollPitchYawRotation r(tmpMat1);
//		length  = q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3];
//		fprintf(out,"%6d %3d %3d %10d %15.7lf %10.7lf %10.7lf %10.7lf %10.7lf\n",year,month,day,date,time,r[0],r[1],r[2],length);
	}
//	fclose(out);
	this->skip(this->lengthAncillary12 - 24 -128- 74 - 72 * this->nAttitudePoints);
	return true;
}


bool CAlosReader::readAncillary13_PRISM()
{
	unsigned int recordNumber;
	C3DPoint omega,phi,DeltaThetaA,DeltaTheta;
	C3DPoint satCoordOffset,Do_PRI_NS,ds_N;

	this->readBinary(10,recordNumber);
	this->skip(20);
	this->skip(15);	//skip thetaXp
	char buffer[200];


	bool an11,an12,an13,an21,an22,an23,an31,an32,an33;
	bool SatCoordOffset,ds,Do_PRI;
	bool nadir=false;

	an11=an12=an13=an21=an22=an23=an31=an32=an33 = false;
	SatCoordOffset=ds=Do_PRI = false;
	int id=0;
	int index =0;
	CCharString labelAN11("&&&&");
	CCharString labelAN12("&&&&");
	CCharString labelAN13("&&&&");
	CCharString labelAN21("&&&&");
	CCharString labelAN22("&&&&");
	CCharString labelAN23("&&&&");
	CCharString labelAN31("&&&&");
	CCharString labelAN32("&&&&");
	CCharString labelAN33("&&&&");
	CCharString labelDS,labelDo_PRI;


	if (this->satInfo.viewDirection.CompareNoCase(BACKWARD))
	{
		labelDS.Format("ds_%c",'B');
		labelDo_PRI.Format("Do_PRI_%cS",'B');
		index = 2;
	}
	if (this->satInfo.viewDirection.CompareNoCase(FORWARD))
	{
		labelDS.Format("ds_%c",'F');
		labelDo_PRI.Format("Do_PRI_%cS",'F');
		index =0;
	}
	if (this->satInfo.viewDirection.CompareNoCase(NADIR))
	{
		labelDS.Format("ds_%c",'N');
		labelDo_PRI.Format("Do_PRI_%cS",'N');
		index =1;
		nadir = true;
	}
	if (this->satInfo.viewDirection.CompareNoCase(WIDE))
	{
		labelDS.Format("ds_%c",'N'); //ds_Nc
		labelDo_PRI.Format("Do_PRI_%cS",'N');
		index =1;
		nadir = true;
	}


	CCharString formatString;

	CRotationMatrix rotMat(0,0,0,0,0,0,0,0,0);
	//int iiii = this->file.gcount();
	while ((!an11 || !an12 || !an13 ||
			!an21 || !an22 || !an23 ||
			!an31 || !an32 || !an33 ||
			!SatCoordOffset || !ds || !Do_PRI) &&
			this->file.gcount())
	{
		this->file.getline(buffer,199,'\n');
		CCharString b(buffer);
		b.trim();
		
		if (b.Find("Set_ID") == 0)
		{

			sscanf(b.GetChar(),"Set_ID=\"%d",&id);
			labelAN11.Format("ID%d_an11",id);
			labelAN12.Format("ID%d_an12",id);
			labelAN13.Format("ID%d_an13",id);
			labelAN21.Format("ID%d_an21",id);
			labelAN22.Format("ID%d_an22",id);
			labelAN23.Format("ID%d_an23",id);
			labelAN31.Format("ID%d_an31",id);
			labelAN32.Format("ID%d_an32",id);
			labelAN33.Format("ID%d_an33",id);
		}
		else if (b.Find(labelAN11) == 0)
		{
			formatString = labelAN11 + "=\"%lf";
			sscanf(b.GetChar(),formatString.GetChar(),&rotMat(0,0));
			an11 = true;
		}
		else if (b.Find(labelAN12) == 0)
		{
			formatString = labelAN12 + "=\"%lf";
			sscanf(b.GetChar(),formatString.GetChar(),&rotMat(0,1));
			an12 = true;
		}
		else if (b.Find(labelAN13) == 0)
		{
			formatString = labelAN13 + "=\"%lf";
			sscanf(b.GetChar(),formatString.GetChar(),&rotMat(0,2));
			an13 = true;
		}
		else if (b.Find(labelAN21) == 0)
		{
			formatString = labelAN21 + "=\"%lf";
			sscanf(b.GetChar(),formatString.GetChar(),&rotMat(1,0));
			an21 = true;
		}
		else if (b.Find(labelAN22) == 0)
		{
			formatString = labelAN22 + "=\"%lf";
			sscanf(b.GetChar(),formatString.GetChar(),&rotMat(1,1));
			an22 = true;
		}
		else if (b.Find(labelAN23) == 0)
		{
			formatString = labelAN23 + "=\"%lf";
			sscanf(b.GetChar(),formatString.GetChar(),&rotMat(1,2));
			an23 = true;
		}
		else if (b.Find(labelAN31) == 0)
		{
			formatString = labelAN31 + "=\"%lf";
			sscanf(b.GetChar(),formatString.GetChar(),&rotMat(2,0));
			an31 = true;
		}
		else if (b.Find(labelAN32) == 0)
		{
			formatString = labelAN32 + "=\"%lf";
			sscanf(b.GetChar(),formatString.GetChar(),&rotMat(2,1));
			an32 = true;
		}
		else if (b.Find(labelAN33) == 0)
		{
			formatString = labelAN33 + "=\"%lf";
			sscanf(b.GetChar(),formatString.GetChar(),&rotMat(2,2));
			an33 = true;
		}
		else if (b.Find("SatCoordOffset") == 0)
		{
			sscanf(buffer,"SatCoordOffset=\"%lf %lf %lf)",&satCoordOffset[0],&satCoordOffset[1],&satCoordOffset[2]);
			SatCoordOffset = true;
		}
		else if (b.Find(labelDS) == 0)
		{
			formatString = labelDS + "=\"%lf %lf %lf";
			sscanf(buffer,formatString.GetChar(),&ds_N[0],&ds_N[1],&ds_N[2]);
			ds = true;
		}
		else if (b.Find(labelDo_PRI) == 0)
		{
			formatString = labelDo_PRI + "=\"%lf %lf %lf";
			sscanf(buffer,formatString.GetChar(),&Do_PRI_NS[0],&Do_PRI_NS[1],&Do_PRI_NS[2]);
			Do_PRI = true;
		}
	}

	//int iiii = this->file.gcount();
	//if (this->file.gcount() == 0)
	if (this->file.gcount() == 0 && ( !an11 || !an12 || !an13 || !an21 ||
!an22 || !an23 || !an31 || !an32 || !an33 || !SatCoordOffset || !ds ||
!Do_PRI) )

	{
		this->errorMessage = "Not all parameter in Ancillary 13 section found";
		return false;
	}


	CRotationMatrix rotX(1,0,0,0,-1,0,0,0,-1);
	CRotationMatrix rotZ(0,1,0,-1,0,0,0,0,1);
	CRotationMatrix tmpMat1,tmpMat2;

	double angle_Nadir = -0.65 * M_PI/180.0;
	CRotationMatrix rotX1(1,0,0,0,cos(angle_Nadir),-sin(angle_Nadir),0,sin(angle_Nadir),cos(angle_Nadir));

	rotMat.RT_times_R(tmpMat1,rotZ);

	rotX.R_times_R(tmpMat2,tmpMat1);

	if (nadir)
	{
		rotX1.R_times_R(tmpMat1,tmpMat2);
		
		CRollPitchYawRotation rpy(tmpMat1);
		this->cameraMounting->setRotation(rpy);
	}
	else
	{
		CRollPitchYawRotation rpy(tmpMat2);
		this->cameraMounting->setRotation(rpy);
	}

	// save camera shift in [m]
	C3DPoint xo = (ds_N + Do_PRI_NS - satCoordOffset)/1000.0;
	xo.y *= -1.0;
	xo.z *= -1.0;
	this->cameraMounting->setShift(xo);


//	this->skip(this->lengthAncillary13 - 24 - 128 );
	return true;
}

bool CAlosReader::readAncillary13_AVNIR()
{
	unsigned int recordNumber;
	this->readBinary(10,recordNumber);
	this->skip(20); // start of ascii section

	char buffer[200];

	bool bChannelOffset1,bChannelOffset2,bChannelOffset3,bChannelOffset4;
	bool bDetectorPitch1,bDetectorPitch2,bDetectorPitch3,bDetectorPitch4;
	bool bInstOffset1,bInstOffset2,bInstOffset3,bInstOffset4;
	bool bInstRotation1,bInstRotation2,bInstRotation3,bInstRotation4;
	bool bFocalLength1,bFocalLength2,bFocalLength3,bFocalLength4;
	bool bAvnir2Alignment, bScanAngle;

	float fChannelOffset1,fChannelOffset2,fChannelOffset3,fChannelOffset4;
	float fDetectorPitch1,fDetectorPitch2,fDetectorPitch3,fDetectorPitch4;
	float fInstOffset1[2],fInstOffset2[2],fInstOffset3[2],fInstOffset4[2];
	float fInstRotation1,fInstRotation2,fInstRotation3,fInstRotation4;
	float fFocalLength1,fFocalLength2,fFocalLength3,fFocalLength4;
	float fPointingAngle;
	C3DPoint avnir2Alignment;
	
	bChannelOffset1 = bChannelOffset2 = bChannelOffset3 = bChannelOffset4 = false;
	bDetectorPitch1 = bDetectorPitch2 = bDetectorPitch3 = bDetectorPitch4 = false;
	bInstOffset1 = bInstOffset2 = bInstOffset3 = bInstOffset4 = false;
	bInstRotation1 = bInstRotation2 = bInstRotation3 = bInstRotation4 = false;
	bFocalLength1 = bFocalLength2 = bFocalLength3 = bFocalLength4 = false;
	bAvnir2Alignment = false;
	bScanAngle = false;

	while ((!bChannelOffset1 || !bChannelOffset2 || !bChannelOffset3 || !bChannelOffset4 ||
			!bDetectorPitch1 || !bDetectorPitch2 || !bDetectorPitch3 || !bDetectorPitch4 ||
			!bInstOffset1 || !bInstOffset2 || !bInstOffset3 || !bInstOffset4 ||
			!bInstRotation1 || !bInstRotation2 || !bInstRotation3 || !bInstRotation4 ||
			!bFocalLength1 || !bFocalLength2 || !bFocalLength3 || !bFocalLength4 ||
			!bAvnir2Alignment) &&
			this->file.gcount())
	{
		this->file.getline(buffer,199,'\n');
		CCharString b(buffer);
		b.trim();

		if (b.Find("ChannelOffset1") == 0)
		{
			sscanf(b.GetChar(),"ChannelOffset1=\"%f",&fChannelOffset1);
			bChannelOffset1 = true;
		}
		else if (b.Find("ChannelOffset2") == 0)
		{
			sscanf(b.GetChar(),"ChannelOffset2=\"%f",&fChannelOffset2);
			bChannelOffset2 = true;
		}
		else if (b.Find("ChannelOffset3") == 0)
		{
			sscanf(b.GetChar(),"ChannelOffset3=\"%f",&fChannelOffset3);
			bChannelOffset3 = true;
		}
		else if (b.Find("ChannelOffset4") == 0)
		{
			sscanf(b.GetChar(),"ChannelOffset4=\"%f",&fChannelOffset4);
			bChannelOffset4 = true;
		}
		else if (b.Find("DetectorPitch1") == 0)
		{
			sscanf(b.GetChar(),"DetectorPitch1=\"%f",&fDetectorPitch1);
			bDetectorPitch1 = true;
		}
		else if (b.Find("DetectorPitch2") == 0)
		{
			sscanf(b.GetChar(),"DetectorPitch2=\"%f",&fDetectorPitch2);
			bDetectorPitch2 = true;
		}
		else if (b.Find("DetectorPitch3") == 0)
		{
			sscanf(b.GetChar(),"DetectorPitch3=\"%f",&fDetectorPitch3);
			bDetectorPitch3 = true;
		}
		else if (b.Find("DetectorPitch4") == 0)
		{
			sscanf(b.GetChar(),"DetectorPitch4=\"%f",&fDetectorPitch4);
			bDetectorPitch4 = true;
		}
		else if (b.Find("InstOffset1") == 0)
		{
			sscanf(b.GetChar(),"InstOffset1=\"%f %f",&fInstOffset1[0],&fInstOffset1[1]);
			bInstOffset1 = true;
		}
		else if (b.Find("InstOffset2") == 0)
		{
			sscanf(b.GetChar(),"InstOffset2=\"%f %f",&fInstOffset2[0],&fInstOffset2[1]);
			bInstOffset2 = true;
		}
		else if (b.Find("InstOffset3") == 0)
		{
			sscanf(b.GetChar(),"InstOffset3=\"%f %f",&fInstOffset3[0],&fInstOffset3[1]);
			bInstOffset3 = true;
		}
		else if (b.Find("InstOffset4") == 0)
		{
			sscanf(b.GetChar(),"InstOffset4=\"%f %f",&fInstOffset4[0],&fInstOffset4[1]);
			bInstOffset4 = true;
		}
		else if (b.Find("InstRotation1") == 0)
		{
			sscanf(b.GetChar(),"InstRotation1=\"%f",&fInstRotation1);
			bInstRotation1 = true;
		}
		else if (b.Find("InstRotation2") == 0)
		{
			sscanf(b.GetChar(),"InstRotation2=\"%f",&fInstRotation2);
			bInstRotation2 = true;
		}
		else if (b.Find("InstRotation3") == 0)
		{
			sscanf(b.GetChar(),"InstRotation3=\"%f",&fInstRotation3);
			bInstRotation3 = true;
		}
		else if (b.Find("InstRotation4") == 0)
		{
			sscanf(b.GetChar(),"InstRotation4=\"%f",&fInstRotation4);
			bInstRotation4 = true;
		}
		else if (b.Find("FocalLength1") == 0)
		{
			sscanf(b.GetChar(),"FocalLength1=\"%f",&fFocalLength1);
			bFocalLength1 = true;
		}
		else if (b.Find("FocalLength2") == 0)
		{
			sscanf(b.GetChar(),"FocalLength2=\"%f",&fFocalLength2);
			bFocalLength2 = true;
		}
		else if (b.Find("FocalLength3") == 0)
		{
			sscanf(b.GetChar(),"FocalLength3=\"%f",&fFocalLength3);
			bFocalLength3 = true;
		}
		else if (b.Find("FocalLength4") == 0)
		{
			sscanf(b.GetChar(),"FocalLength4=\"%f",&fFocalLength4);
			bFocalLength4 = true;
		}
		else if (b.Find("Avnir2Alignment") == 0)
		{
			sscanf(b.GetChar(),"Avnir2Alignment=\"%lf %lf %lf",&avnir2Alignment[0],&avnir2Alignment[1],&avnir2Alignment[2]);
			bAvnir2Alignment = true;
		}
	}

	if (this->file.gcount() == 0)
	{
		this->errorMessage = "Not all parameter in Ancillary 13 section found";
		return false;
	}

	this->file.seekg(-64250,ios::end);
	this->filePos = this->file.tellg();

	while ((!bScanAngle) && this->file.gcount())
	{
		this->file.getline(buffer,199,'\n');
		CCharString b(buffer);
		b.trim();
		if (b.Find("Sci_PointingAngle") == 0)
		{
			sscanf(b.GetChar(),"Sci_PointingAngle=\"%f",&fPointingAngle);
			bScanAngle = true;
		}
	}
	
	if (this->file.gcount() == 0)
	{
		this->errorMessage = "Not all parameter in Ancillary 13 section found";
		return false;
	}

	unsigned int length = this->satInfo.cameraName.GetLength();
	if (length)
	{
		float focalLength;
		float detectorPitch;
		float instOffsetX,instOffsetY;
		float instRotation;

		CDistortion* distortion = NULL;
		if (this->satInfo.cameraName.GetChar()[length-1] == '1')
		{
			focalLength = fFocalLength1; 
			detectorPitch = fDetectorPitch1;
			instOffsetX = fInstOffset1[0];
			instOffsetY = fInstOffset1[1];
			instRotation = fInstRotation1;
		}
		else if (this->satInfo.cameraName.GetChar()[length-1] == '2')
		{
			focalLength = fFocalLength2; 
			detectorPitch = fDetectorPitch2;
			instOffsetX = fInstOffset2[0];
			instOffsetY = fInstOffset2[1];
			instRotation = fInstRotation2;
		}
		else if (this->satInfo.cameraName.GetChar()[length-1] == '3')
		{
			focalLength = fFocalLength3; 
			detectorPitch = fDetectorPitch3;
			instOffsetX = fInstOffset3[0];
			instOffsetY = fInstOffset3[1];
			instRotation = fInstRotation3;
		}
		else if (this->satInfo.cameraName.GetChar()[length-1] == '4')
		{
			focalLength = fFocalLength4; 
			detectorPitch = fDetectorPitch4;
			instOffsetX = fInstOffset4[0];
			instOffsetY = fInstOffset4[1];
			instRotation = fInstRotation4;
		}
		else
			return false;

		

		avnir2Alignment *=M_PI/180.0;
		C3DPoint pointingAngles(fPointingAngle * M_PI/180.0, 0.0,0.0);

		COmegaPhiKappaRotation opointing(&pointingAngles);

		COmegaPhiKappaRotation opk(&avnir2Alignment);

		CRotationMatrix rotX(1,0,0,0,-1,0,0,0,-1);
		CRotationMatrix rotZ(0,1,0,-1,0,0,0,0,1);
		CRotationMatrix tmpMat1,tmpMat2,tmpMat3;

		opointing.getRotMat().RT_times_R(tmpMat3, opk.getRotMat());
		tmpMat3.RT_times_R(tmpMat1,rotZ);
		rotX.R_times_R(tmpMat2,tmpMat1);

		CRollPitchYawRotation rpy(tmpMat2);
		this->cameraMounting->setRotation(rpy);

		CXYZPoint irp(this->satInfo.nCols/2,0,-focalLength/detectorPitch);
		this->camera->setIRP(irp);
		this->camera->setScale(detectorPitch);
		this->camera->setDistortion(eDistortionALOS,this->computeAVNIRDistortion(focalLength,fFocalLength3,instOffsetX,instOffsetY,instRotation,detectorPitch));
	
	}
	else
		return false;

	return true;
}

doubles CAlosReader::computeAVNIRDistortion(float focalLength,float referenceFocalLength,float instOffsetX,float instOffsetY, float instRotation,float detectorPitch)
{
	doubles parameter(6);


	parameter[0] = instOffsetY/detectorPitch;
	parameter[1] = focalLength/referenceFocalLength;

	parameter[3] = -instOffsetX/detectorPitch;
	parameter[4] = focalLength/referenceFocalLength - 1.0;


	return parameter;

}




double CAlosReader::transformTime(int julianDate,int hour,int min,double sec)
{
	double time;

	if (julianDate > this->firstLineDate)
		time = 24*3600 + hour*3600 + min*60 + sec;
	else if (julianDate < this->firstLineDate)
		time = -24*3600 + hour*3600 + min*60 + sec;
	else
		time = hour*3600 + min*60 + sec;

	return time;
}

int CAlosReader::julianDay(int year,int month,int day)
{
	return julianDayFromDate(year,month,day);
}

void CAlosReader::getStartAndEndTime(double& startTime,double& endTime,bool forAttitudes)
{
	double timeOffset=0.0;
	//double sceneLength = this->camera->getNRows() * this->timePerLine;
	if (forAttitudes)
	{
		timeOffset = 10.0;
	}
	else
	{
		int nPointsAroundScene = 2;
		double timeBetweenTwoPoints = 60.0;
		timeOffset = double(nPointsAroundScene * timeBetweenTwoPoints);
	}

	startTime = this->firstLineTime - timeOffset;
	endTime   = this->lastLineTime + timeOffset;
}

bool CAlosReader::prepareEphemeris()
{

	int size = this->tmpOrbitPoints.GetSize();
	if ( size < 4) // we need 4 points for interpolation
	{
		this->errorMessage = "Not enough orbit points before found!";
		return false;
	}

	double t[4];
	double x[4];
	double y[4];
	double z[4];
	double vx[4];
	double vy[4];
	double vz[4];


	this->orbitPoints->getObsPoints()->SetSize(0);

	// interplate points between second and second last orbit point
	for (int offset = 1; offset < size-2 ; offset++)
	{
		// fill vectors for hermite
		for (int i=0;i<4;i++)
		{
			CXYZOrbitPoint* p =  this->tmpOrbitPoints.getAt(offset-1+i);
			x[i]= (*p)[0];
			y[i]= (*p)[1];
			z[i]= (*p)[2];
			vx[i]= (*p)[3];
			vy[i]= (*p)[4];
			vz[i]= (*p)[5];
			t[i]= p->dot;
		}

		CXYZOrbitPoint* before = this->tmpOrbitPoints.getAt(offset);
		CXYZOrbitPoint* after = this->tmpOrbitPoints.getAt(offset+1);
		double timeBefore = before->dot;
		double timeAfter =  after->dot;
		double step = 0.5;

		CXYZOrbitPointArray* newOrbitPoints = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();
		// interpolate between 2 known points every sec
		for (double time = timeBefore; time <= timeAfter; time+=step)
		{
			CXYZOrbitPoint* p = newOrbitPoints->add();
			p->dot = time;

			this->hermite(*p,x,y,z,vx,vy,vz,t);

			p->setCovarElement(0,0,0.75);
			p->setCovarElement(1,1,0.75);
			p->setCovarElement(2,2,0.75);

			p->setHasVelocity(false);
		}

		if (this->listener)
		{
			double per = (double) offset / (double) (size - 2);
			this->listener->setProgressValue(per*this->getMaxListenerValue());
		}
	}


	return true;
}


bool CAlosReader::prepareAttitudes()
{
	COrbitAttitudeModel *modelAttitudes = (COrbitAttitudeModel*)this->attitudes->getCurrentSensorModel();
	
	double fixedTime;
	CRotationMatrix fixedMatrix; // get the tangeltial matrix at the "fixedTime"

	COrbitPathModel* modelPath = (COrbitPathModel*) this->orbitPoints->getCurrentSensorModel();
	// computation of the fixed Rotation Matrix
	// due to different times in attitudes and orbit points we have
	// to interpolate the orbit points to get it at the same time as the angles
	fixedTime = this->sceneCenterTime;
	C3DPoint location;
	C3DPoint velocity;
	modelPath->evaluate(location,velocity,fixedTime);

	C3DPoint X,Y,Z;

	Z = location /  location.getNorm();
	Y = Z.crossProduct(velocity);
	Y = Y / Y.getNorm();
	X = Y.crossProduct(Z);

	fixedMatrix(0,0) = X[0];
	fixedMatrix(1,0) = X[1];
	fixedMatrix(2,0) = X[2];

	fixedMatrix(0,1) = Y[0];
	fixedMatrix(1,1) = Y[1];
	fixedMatrix(2,1) = Y[2];

	fixedMatrix(0,2) = Z[0];
	fixedMatrix(1,2) = Z[1];
	fixedMatrix(2,2) = Z[2];

	modelAttitudes->setFixedECRMatrix(fixedMatrix);	// save Rfixed!
	modelAttitudes->setTimeFixedECRMatrix(fixedTime); // and the time
	


	// this array stores the transformed rpy angles
	CXYZOrbitPointArray* newRPYAngles = (CXYZOrbitPointArray*)this->attitudes->getObsPoints();
	newRPYAngles->setCreateWithVelocity(false);

	// clear the array
	newRPYAngles->RemoveAll();

	CRotationBase* newRPY = CRotationBaseFactory::initRotation(&C3DPoint(),RPY);
	CRotationMatrix &newRPYMatrix = newRPY->getRotMat();

	for (int i=0; i < this->AttitudeMatrix.size(); i++)
	{
		fixedMatrix.RT_times_R(newRPYMatrix,this->AttitudeMatrix[i]);

		// and update the angles
		newRPY->computeParameterFromRotMat();
		CXYZOrbitPoint* angles = newRPYAngles->add();
		for (int k=0; k < 3; k++)
			(*angles)[k] = (*newRPY)[k];

		angles->dot = this->attitudeTime[i];

		(*angles) *= 1000.0;

		angles->setCovarElement(0,0,0.00001);
		angles->setCovarElement(1,1,0.00001);
		angles->setCovarElement(2,2,0.00001);

		(*angles->getCovariance()) *= 1000000.0;
	}

/*
	FILE* out1= fopen("alos_newRPY.txt","w");
	for (int i=0; i < newRPYAngles->GetSize(); i++)
	{
		CXYZOrbitPoint* p = newRPYAngles->getAt(i);
				fprintf(out1,"%20.15lf %20.15lf %20.15lf %20.7lf\n",(*p)[0],(*p)[1],(*p)[2],p->dot);
	}
	fclose(out1);
*/

	delete newRPY;
	return true;
}

bool CAlosReader::computeInnerOrienation(vector<vector<CObsPoint> >& camera,int ccd)
{
	int indexCCD = -1;
	for (int i=0; i< camera.size(); i++)
	{
		if (camera[i][0].id == ccd)
		{
			indexCCD = i;
			break;
		}
	}

	if (indexCCD == -1)
		return false;

	vector<CObsPoint> seg = camera[indexCCD];


	int nPoints = seg.size();

	if (nPoints != 3)
		return false;

	double averageScale = 0.0;
	double averageY = 0.0;

	// determine the average scale
	for (int i=0; i <camera.size(); i++)
	{
		vector<CObsPoint> seg = camera[i];
		double dxf = seg[2][0] - seg[0][0];
		double dxc = seg[2][2] - seg[0][2];
		double dyc = seg[2][3] - seg[0][3];
		averageScale += sqrt(dxc*dxc + dyc*dyc) / dxf;
		averageY += (seg[2][3] + seg[1][3] + seg[0][3]);
	}

	averageScale /= camera.size();
	averageY /= averageScale;
	averageY /= camera.size()*3;  // in pixel now


	Matrix N(3,3),t1(3,1),t2(3,1);
	// xf_r = a0 + a1*xf + a2*xf^2
	// yf_r = b0 + b1*xf + b2*xf^2
	N.element(0,0) = 1; N.element(0,1) = seg[0][0]; N.element(0,2) = seg[0][0] * seg[0][0];
	N.element(1,0) = 1; N.element(1,1) = seg[1][0]; N.element(1,2) = seg[1][0] * seg[1][0];
	N.element(2,0) = 1; N.element(2,1) = seg[2][0]; N.element(2,2) = seg[2][0] * seg[2][0];

	// all in pixel
	// xf_r = xc/scale - 0.0
	t1.element(0,0) = seg[0][2] /averageScale;
	t1.element(1,0) = seg[1][2] /averageScale;
	t1.element(2,0) = seg[2][2] /averageScale;

	// yf_r = yc/scale - y_average
	t2.element(0,0) = seg[0][3]/averageScale - averageY;
	t2.element(1,0) = seg[1][3]/averageScale - averageY;
	t2.element(2,0) = seg[2][3]/averageScale - averageY;

	Try
	{
		Matrix polyX = N.i() * t1;
		Matrix polyY = N.i() * t2;

		C3DPoint irp(0.0,-averageY,-FOCAL_LENGTH_PRISM/averageScale);
		this->camera->setIRP(irp);
		this->camera->setScale(averageScale);
		doubles parameter;
		parameter.push_back(polyX.element(0,0));
		parameter.push_back(polyX.element(1,0));
		parameter.push_back(polyX.element(2,0));
		parameter.push_back(polyY.element(0,0));
		parameter.push_back(polyY.element(1,0));
		parameter.push_back(polyY.element(2,0));
		this->camera->setDistortion(eDistortionALOS,parameter);

	}
    CatchAll //(char* error)
    {
        this->errorMessage = "Failed computing the interior orientation";
		return false;
    }

	return true;
}


bool CAlosReader::hermite(CXYZOrbitPoint &newPoint,
				 double x[],
				 double y[],
				 double z[],
				 double vx[],
				 double vy[],
				 double vz[],
				 double time[])
{

	double sum[4];
	double F0[4];
	double F1[4] = {newPoint.dot - time[0],
					newPoint.dot - time[1],
					newPoint.dot - time[2],
					newPoint.dot - time[3]};
	double H2[4];

	for (int i=0; i< 4; i++)
	{
		sum[i] = 0.0;
		H2[i] = 1.0;
		for (int k=0; k< 4; k++)
		{
			if (k == i)
				continue;
			else
			{
				double value = 1.0/(time[i]-time[k]);
				sum[i] += value;
				H2[i] *= F1[k] * value;

			}
		}
		F0[i] = 1.0 - 2.0 * F1[i] * sum[i];
		H2[i] *=H2[i];
	}

	newPoint[0] = 0.0;
	newPoint[1] = 0.0;
	newPoint[2] = 0.0;

	for (int i=0; i < 4; i++)
	{
		newPoint[0] += (x[i]*F0[i] + vx[i]*F1[i])*H2[i];
		newPoint[1] += (y[i]*F0[i] + vy[i]*F1[i])*H2[i];
		newPoint[2] += (z[i]*F0[i] + vz[i]*F1[i])*H2[i];
	}


	return true;
}

void CAlosReader::initOldALOSCamera()
{
	vector<CObsPoint> ccdSegment;
	CObsPoint ccdElement(4);

	// nadir camera ####################################################################################
	ccdElement.id = 1;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] = -0.5026203;	  ccdElement[2] = -84.9954059;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] = -0.4804528;	  ccdElement[2] = -67.0525073;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] = -0.4642901;   ccdElement[2] = -49.1203839 ;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 2;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] = -0.4642901;   ccdElement[2] = -49.1194137;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] = -0.4490066;	  ccdElement[2] = -31.1852728;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] = -0.4397304;   ccdElement[2] = -13.2561444;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 3;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] = -0.4387609;   ccdElement[2] = -13.2571141;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] = -0.4272627;	  ccdElement[2] = 4.6617150;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] = -0.4217739;   ccdElement[2] = 22.5812925;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);


	ccdSegment.clear();
	ccdElement.id = 4;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] = -0.4208042;   ccdElement[2] = 22.5812925;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] = -0.3969758;	  ccdElement[2] = 40.5091519;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] = -0.3791556;   ccdElement[2] = 58.4435209;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 5;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] = -0.3781857;   ccdElement[2] = 58.4444914;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] = -0.3689513;	  ccdElement[2] = 76.3814304;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] = -0.3657292;   ccdElement[2] = 94.3306472;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 6;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] = -0.3648053;   ccdElement[2] = 94.3306472;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] = -0.3772962;	  ccdElement[2] = 112.2861093;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] = -0.3938716;   ccdElement[2] = 130.2596331;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);


	// forward camera ####################################################################################
	ccdSegment.clear();
	ccdElement.id = 1;
	ccdElement[0] = 1271;	ccdElement[1] = 0;	ccdElement[3] = 0.0019310;	  ccdElement[2] = -132.6450738;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 3099;	ccdElement[1] = 0;	ccdElement[3] = 0.0051844;	  ccdElement[2] = -119.3996249;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.0106576;   ccdElement[2] = -106.1573668 ;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 2;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.0106274;   ccdElement[2] = -106.1602839;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = -0.0116016;	  ccdElement[2] = -88.4487527;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = -0.0279734;   ccdElement[2] = -70.7510567;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 3;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = -0.0270038;   ccdElement[2] = -70.7510567;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = -0.0157952;	  ccdElement[2] = -53.0546925;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.0012705;   ccdElement[2] = -35.3666259;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);


	ccdSegment.clear();
	ccdElement.id = 4;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.0012705;   ccdElement[2] = -35.3675959;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = -0.0018665;	  ccdElement[2] = -17.6813538;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.0008536;   ccdElement[2] = 0.0021236;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 5;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.0018232;   ccdElement[2] = 0.0001843;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = -0.0032579;	  ccdElement[2] = 17.6757975;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = -0.0024819;   ccdElement[2] = 35.3541720;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 6;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = -0.0005427;   ccdElement[2] = 35.3532021;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = -0.0026180;	  ccdElement[2] = 53.0451204;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.0011639;   ccdElement[2] = 70.7453385;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);
	ccdSegment.clear();
	ccdElement.id = 7;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.0011639;   ccdElement[2] = 70.7453385;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = -0.0119749;	  ccdElement[2] = 88.4492093;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = -0.0192565;   ccdElement[2] = 106.1669251;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 8;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = -0.0201953;   ccdElement[2] = 106.1669251;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 1861;	ccdElement[1] = 0;	ccdElement[3] = -0.0010297;	  ccdElement[2] = 119.4037054;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 3690;	ccdElement[1] = 0;	ccdElement[3] = 0.0203545;   ccdElement[2] = 132.6509172;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);


	// backward camera ####################################################################################
	ccdSegment.clear();
	ccdElement.id = 1;
	ccdElement[0] = 1271;	ccdElement[1] = 0;	ccdElement[3] = 0.1570713;	  ccdElement[2] = -132.9479574;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 3099;	ccdElement[1] = 0;	ccdElement[3] = 0.1459637;	  ccdElement[2] = -119.6990704;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.1370679;   ccdElement[2] = -106.4534039 ;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 2;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.1370377;   ccdElement[2] = -106.4534039;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = 0.1394712;	  ccdElement[2] = -88.7297288;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.1477618;   ccdElement[2] = -71.0199518;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 3;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.1467922;   ccdElement[2] = -71.0199518;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = 0.1458853;	  ccdElement[2] = -53.3120244;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.1508355;   ccdElement[2] = -35.6124457;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);


	ccdSegment.clear();
	ccdElement.id = 4;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.1518052;   ccdElement[2] = -35.6124457;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = 0.1450369;	  ccdElement[2] = -17.9186792;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.1441257;   ccdElement[2] = -0.2277169;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 5;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.1441257;   ccdElement[2] = -0.2267473;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = 0.1333383;	  ccdElement[2] = 17.4692703;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.1284081;   ccdElement[2] = 35.1680233;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 6;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.1293777;   ccdElement[2] = 35.1689932;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = 0.1261728;	  ccdElement[2] = 52.8673796;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.1288250;   ccdElement[2] = 70.5740439;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);
	ccdSegment.clear();
	ccdElement.id = 7;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.1278554;   ccdElement[2] = 70.5730730;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] = 0.1264297;	  ccdElement[2] = 88.2800337;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] = 0.1308612;   ccdElement[2] = 106.0008177;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 8;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] = 0.1308920;   ccdElement[2] = 106.0027624;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 1861;	ccdElement[1] = 0;	ccdElement[3] = 0.1278434;	  ccdElement[2] = 119.2442032;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 3690;	ccdElement[1] = 0;	ccdElement[3] = 0.1270135;   ccdElement[2] = 132.4960690;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);
}













void CAlosReader::initNewALOSCamera()
{
	vector<CObsPoint> ccdSegment;
	CObsPoint ccdElement(4);

	// nadir camera ####################################################################################
	ccdElement.id = 1;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] =-0.5026203;	ccdElement[2] =	  -85.0069943;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] =-0.4798761;	ccdElement[2] =	  -67.0627434;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] =-0.4631366;   ccdElement[2] =  -49.1292709;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 2;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] =-0.4629331;   ccdElement[2] =  -49.1296105;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] =-0.4459336;	ccdElement[2] =	  -31.1962952;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] =-0.4349411;   ccdElement[2] =  -13.2679937;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 3;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] =-0.4349411;   ccdElement[2] =  -13.2693221;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] =-0.4254059;	ccdElement[2] =	  4.6556063 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] =-0.4218805;   ccdElement[2] =  22.5812827 ;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 4;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] =-0.4209884;   ccdElement[2] =  22.5811857 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] =-0.3974656;	ccdElement[2] =	  40.5092051 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] =-0.3799509;   ccdElement[2] =  58.4437343 ;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 5;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] =-0.3791847;   ccdElement[2] =  58.4441807 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] =-0.3694363;	ccdElement[2] =	  76.3817459 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] =-0.3657001;   ccdElement[2] =  94.3315897 ;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 6;
	ccdElement[0] = 16;	    ccdElement[1] = 0;	ccdElement[3] =-0.3644755;   ccdElement[2] =  94.3314731 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2496;	ccdElement[1] = 0;	ccdElement[3] =-0.3735416;	ccdElement[2] =	  112.2868582;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4976;	ccdElement[1] = 0;	ccdElement[3] =-0.3866915;   ccdElement[2] =  130.2603049;
	ccdSegment.push_back(ccdElement);
	this->nadirCamera.push_back(ccdSegment);


	// forward camera ##############################################################
	ccdSegment.clear();
	ccdElement.id = 1;
	ccdElement[0] = 1271;	ccdElement[1] = 0;	ccdElement[3] =0.0013492 ;	ccdElement[2] =	 -132.6566338;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 3099;	ccdElement[1] = 0;	ccdElement[3] =0.0047092 ;	ccdElement[2] =	 -119.4110926;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.0102891 ;   ccdElement[2] = -106.1687433;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 2;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.0098323 ;   ccdElement[2] = -106.1719910;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =-0.0124209;	ccdElement[2] =	 -88.4603720 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =-0.0288170;   ccdElement[2] = -70.7625902 ;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 3;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =-0.0273238;   ccdElement[2] = -70.7625708 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =-0.0159552;	ccdElement[2] =	 -53.0671512 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.0012705 ;   ccdElement[2] = -35.3800303 ;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 4;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.0009215 ;   ccdElement[2] = -35.3816404 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =-0.0021235;	ccdElement[2] =	 -17.6942266 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.0006887 ;   ccdElement[2] = -0.0095799 ;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 5;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.0022208 ;   ccdElement[2] = -0.0117713 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =-0.0025647;	ccdElement[2] =	 17.6701198 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =-0.0014929;   ccdElement[2] = 35.3547733 ;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 6;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.0005627 ;   ccdElement[2] = 35.3537646 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =-0.0020653;	ccdElement[2] =	 53.0452513 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.0011639 ;   ccdElement[2] = 70.7450374 ;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 7;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.0011639 ;   ccdElement[2] = 70.7449112 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =-0.0119264;	ccdElement[2] =	 88.4494472 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =-0.0191595;   ccdElement[2] = 106.1678293 ;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 8;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =-0.0202923;   ccdElement[2] = 106.1670320 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 1861;	ccdElement[1] = 0;	ccdElement[3] =-0.0010782;	ccdElement[2] =	 119.4037588 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 3690;	ccdElement[1] = 0;	ccdElement[3] =0.0203545 ;   ccdElement[2] = 132.6509170 ;
	ccdSegment.push_back(ccdElement);
	this->forwardCamera.push_back(ccdSegment);


	// backward camera #############################################################
	ccdSegment.clear();
	ccdElement.id = 1;
	ccdElement[0] = 1271;	ccdElement[1] = 0;	ccdElement[3] =0.1591366 ;	ccdElement[2] =	 -132.9562745;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 3099;	ccdElement[1] = 0;	ccdElement[3] =0.1503360 ;	ccdElement[2] =	 -119.7048853;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.1437487 ;   ccdElement[2] = -106.4567196;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 2;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.1432434 ;   ccdElement[2] = -106.4574391;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =0.1423607 ;	ccdElement[2] =	 -88.7328328 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.1473352 ;   ccdElement[2] = -71.0221264 ;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 3;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.1470928 ;   ccdElement[2] = -71.0222040 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =0.1451920 ;	ccdElement[2] =	 -53.3135719 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.1491484 ;   ccdElement[2] = -35.6132894 ;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 4;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.1505350 ;   ccdElement[2] = -35.6138908 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =0.1440284 ;	ccdElement[2] =	 -17.9179083 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.1433791 ;   ccdElement[2] = -0.2247305 ;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 5;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.1433500 ;   ccdElement[2] = -0.2242748 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =0.1341722 ;	ccdElement[2] =	 17.4716654 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.1308515 ;   ccdElement[2] = 35.1703413 ;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 6;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.1312394 ;   ccdElement[2] = 35.1710300 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =0.1273460 ;	ccdElement[2] =	 52.8687524 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.1293098 ;   ccdElement[2] = 70.5747525 ;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);
	ccdSegment.clear();

	ccdElement.id = 7;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.1286020 ;   ccdElement[2] = 70.5736749 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 2480;	ccdElement[1] = 0;	ccdElement[3] =0.1268709 ;	ccdElement[2] =	 88.2807428 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 4928;	ccdElement[1] = 0;	ccdElement[3] =0.1309970 ;   ccdElement[2] = 106.0016343 ;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);

	ccdSegment.clear();
	ccdElement.id = 8;
	ccdElement[0] = 32;	    ccdElement[1] = 0;	ccdElement[3] =0.1307563 ;   ccdElement[2] = 106.0019455 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 1861;	ccdElement[1] = 0;	ccdElement[3] =0.1277756 ;	ccdElement[2] =	 119.2437943 ;
	ccdSegment.push_back(ccdElement);
	ccdElement[0] = 3690;	ccdElement[1] = 0;	ccdElement[3] =0.1270135 ;   ccdElement[2] = 132.4960689 ;
	ccdSegment.push_back(ccdElement);
	this->backwardCamera.push_back(ccdSegment);
}


bool CAlosReader::readPrecisionData(CFileName imageFile,CFileName dataDirectoryAttitudes,CFileName dataDirectoryPath,CPushBroomScanner* scanner)
{
	this->orbitPoints = project.getOrbitObservation(scanner->getOrbitPath());
	this->attitudes = project.getOrbitObservation(scanner->getOrbitAttitudes());

	// remember if parameters were active
	int hasRotPar = this->attitudes->getCurrentSensorModel()->getActiveParameter(CSensorModel::rotPar);
	this->attitudes->getCurrentSensorModel()->deactivateParameterGroup(CSensorModel::rotPar);

	this->camera = scanner->getCamera();
	this->cameraMounting = scanner->getCameraMounting();

	if (!this->orbitPoints || !this->attitudes)
	{
		this->errorMessage = "Internal Error";
		return false;
	}

	int ccdElement;

	// read common part 
	if (!this->readCommonPart(imageFile,ccdElement))
		return false;

	if (this->satInfo.processingLevel.CompareNoCase("1B2"))
	{
		this->errorMessage = "No precise orbit information for Level 1B2 available!";
		return false;
	}

	CCharString viewDirection;
	if (this->satInfo.viewDirection.CompareNoCase(NADIR))
		viewDirection = "_N";
	else if (this->satInfo.viewDirection.CompareNoCase(FORWARD))
		viewDirection = "_F";
	else if (this->satInfo.viewDirection.CompareNoCase(BACKWARD))
		viewDirection = "_B";
	else if (this->satInfo.viewDirection.CompareNoCase(WIDE))
		viewDirection = "_W";


	this->firstLineTime = scanner->getFirstLineTime();
	this->lastLineTime = this->firstLineTime + this->satInfo.nRows * this->timePerLine;

	if (this->firstLineTime < this->sceneCenterTime) // on the same day
		this->firstLineDate = this->sceneCenterDate;
	else
		this->firstLineDate = this->sceneCenterDate - 1;

	if (!this->readSupplFile())
		return false;
	
	double startTimeScene = -1.0;
	double endTimeScene = -1.0;

	int firstPointIndex = this->attitudes->getFirstPointIndex();
	int lastPointIndex = this->attitudes->getLastPointIndex();

	if ( this->attitudes->getObsPoints() && 
		firstPointIndex >=0 && firstPointIndex < this->attitudes->getObsPoints()->GetSize() &&
		lastPointIndex >=0 &&  lastPointIndex  < this->attitudes->getObsPoints()->GetSize())
	{
		startTimeScene = this->attitudes->getObsPoints()->GetAt(firstPointIndex)->getDot();
		endTimeScene = this->attitudes->getObsPoints()->GetAt(lastPointIndex)->getDot();
	}
	else
	{
		this->errorMessage = "Internal Error!";
		return false;
	}


	// read updated orbit attitude data
	CCharStringArray fileList;
	fileList.RemoveAll();
	CCharString filter = "ALOSPPP*" + viewDirection;
	CSystemUtilities::dir(fileList,dataDirectoryAttitudes,filter.GetChar());
	
	vector<CFileName> neededFiles;

	for (int i=0; i < fileList.GetSize(); i++)
	{
		CFileName name(*fileList.GetAt(i));
		CCharString shortName =  name.GetFileName();
		int start = shortName.Find("ALOSPPP") + 7;
		
		if (start < 0 || start + 20 >= shortName.GetLength())
			continue;

		int year = atoi(shortName.Mid(start,start+3).GetChar());
		int month = atoi(shortName.Mid(start+4,start+5).GetChar());
		int day = atoi(shortName.Mid(start+6,start+7).GetChar());
		int hour_start = atoi(shortName.Mid(start+8,start+9).GetChar());
		int min_start = atoi(shortName.Mid(start+10,start+11).GetChar());
		double sec_start = atoi(shortName.Mid(start+12,start+13).GetChar());
		int hour_end = atoi(shortName.Mid(start+15,start+16).GetChar());
		int min_end = atoi(shortName.Mid(start+17,start+18).GetChar());
		double sec_end = atoi(shortName.Mid(start+19,start+20).GetChar());
		
		int jDay = this->julianDay(year,month,day);
		double startTimeFile = this->transformTime(jDay,hour_start,min_start,sec_start);

		if (hour_end < hour_start)
			jDay++;
		double endTimeFile = this->transformTime(jDay,hour_end,min_end,sec_end);

		if (startTimeFile < startTimeScene && endTimeFile > endTimeScene) 
			neededFiles.push_back(name); // scene  starts and ends in this period
		else if (startTimeFile < startTimeScene && fabs(endTimeFile) < endTimeScene)
			neededFiles.push_back(name); // scene starts in this period but end time is outside -> we need a second file
		else if (startTimeFile > startTimeScene && startTimeFile < endTimeScene && fabs(endTimeFile) > endTimeScene)
			neededFiles.push_back(name); // scene starts before periode but ends after file periode -> should be the second file

	}
	
	if (neededFiles.size() > 0)
	{
		for (unsigned int i=0; i < neededFiles.size(); i++)
		{
			if (!this->openFile(neededFiles[0]) || !this->readAncillary12(true,i > 0))
			{
				this->closeFile();
				this->errorMessage = "Error while importing precision data!";
				return false;
			}
			this->closeFile();
		}
		
		this->setAimedRMSPath(0.5);
		this->setAimedRMSAttitudes(0.5);

		if(!this->adjustOrbit(true,true,startTimeScene,endTimeScene))
			return false;
	}
	else
	{
		if (fileList.GetSize() == 0)
			this->errorMessage = "No files with precision data found (ALOSPPP..)!";
		else
			this->errorMessage = "No files with precision data in according period of time found!";
		return false;
	}

	if (hasRotPar)
		this->attitudes->getCurrentSensorModel()->activateParameterGroup(CSensorModel::rotPar);

	// the precise orbit data incorperate the camera mounting, so we reset the camera mounting
	// only the axis transformation remains
	CRotationMatrix rot(0,1,0,1,0,0,0,0,-1);

	// for nadir camera we have to consider the additional angle
	if (this->satInfo.viewDirection.CompareNoCase(NADIR))
	{
		double angle_Nadir = -0.65;
		CRotationMatrix rotX(1,0,0,0,cos(angle_Nadir*M_PI/180.0),-sin(angle_Nadir*M_PI/180.0),0,sin(angle_Nadir*M_PI/180.0),cos(angle_Nadir*M_PI/180.0));
		CRotationMatrix tmpMat;
		rotX.R_times_R(tmpMat,rot);
		rot = tmpMat;
	}

	this->cameraMounting->getRotation().updateRotMat(rot);

	return true;
}

bool CAlosReader::readCommonPart(CFileName& filename,int& ccdElement)
{
	CCharString id;
	CCharString filetype;
	
	// check if huge file was selected
	if (filename.Find(".hug") > 0 )
	{
		this->errorMessage = "The original image file is needed!";
		return false;
	}

	// check if the image file is a NITF file
	if (filename.Find(".ntf") > 0)
		this->readNITF = true;
	else
		this->readNITF = false;


	// analyse the filename
	CCharString content(filename.GetFileName());

	int start = content.Find("-");
	int end = content.GetLength();

	if (start >=0 && ((start+1)  < (content.GetLength()-1)))
	{
		filetype = content.Left(start);
		id = content.Mid(start+1,end-1);
	}

	// check if one of the ALOS files was selected
	if (!this->readNITF && 
		 !filetype.Find("VOL") == 0 && 
		 !filetype.Find("LED") == 0 &&	
		 !filetype.Find("IMG") == 0 &&
		 !filetype.Find("TRL") == 0 &&
		 !filetype.Find("SUP") == 0)    
	{
		this->errorMessage = "The original filenames are needed. \nRename the files before importing ALOS metadata!";
		return false;
	}

	// user has to select the IMG- file in order to continue
	if (!this->readNITF && 
		(filetype.Find("VOL") == 0 ||
		 filetype.Find("LED") == 0 ||
		 filetype.Find("TRL") == 0 ||
		 filetype.Find("SUP") == 0))
	{
		this->errorMessage = "Select the image file (IMG-..) to import the metadata!";
		return false;
	}
	else if (this->readNITF && filename.Find(".ntf") < 0)
	{
		this->errorMessage = "Select the image file (*.ntf ..) to import the metadata!";
		return false;
	}	
	
	if (filename.Find("-O1B2") > 0)
	{

	}
	else if (filename.Find("-O1A") || filename.Find("-O1B1"))
	{
		if (filetype.Find("IMG") == 0)
		{
			int start = id.Find("-");
			int end = id.GetLength();

			if (start > 0 && start < end)
			{
				// extract CCD element from filename
				ccdElement = atoi(id.Left(start).GetChar());
			
				// and the scene id
				id = id.Mid(start+1,end-1);
			}
		}
		else if (this->readNITF)
		{
			// no prefix IMG
			ccdElement = atoi(filetype.GetChar());
		}
		else
		{
			this->errorMessage = "Error while analysing image file name!";
			return false;
		}

		// clean id 
		int pos = id.Find("_rp");
		if  (pos > 0 && pos < id.GetLength())
		{
			id = id.Left(pos);
		}

		if (ccdElement < 1 || ccdElement > 8)
		{
			this->errorMessage = "Error while analysing image file name!";
			return false;
		}
	}
	else
	{
		this->errorMessage = "Wrong processing level!\nOnly 1A, 1B1, 1B2R and 1B2G are supported!";
		return false;

	}

	if (id.GetLength() == 0 ) 
	{
		this->errorMessage = "Error while analysing image file name!";
		return false;
	}
	
	this->satInfo.imageID = id.Left(id.Find("-"));
	this->volumeFile  = filename.GetPath() + CSystemUtilities::dirDelimStr + "VOL-" + id;
	this->leaderFile  = filename.GetPath() + CSystemUtilities::dirDelimStr + "LED-" + id;
	this->supplFile   = filename.GetPath() + CSystemUtilities::dirDelimStr + "SUP-" + id;
	this->imageFile = filename;

	// first read the volume file
	if (!this->readVolumeFile())
		return false;

	// next the leader file
	if (!this->readLeaderFile())
		return false;

	return true;
}


bool CAlosReader::isALOS_PRISMCamera(CCharString cameraName)
{
	bool ret = false;

	if (cameraName.Find(NADIR) >=0)
		ret = true;
	else if (cameraName.Find(BACKWARD) >=0)
		ret = true;
	else if (cameraName.Find(FORWARD) >=0)
		ret = true;
	else if (cameraName.Find(WIDE) >=0)
		ret = true;

	return ret;
}

bool CAlosReader::isALOS_AVNIR2Camera(CCharString cameraName)
{
	bool ret = false;

	if (cameraName.Find(AVNIR2) >=0)
		ret = true;

	return ret;
}

void CAlosReader::setDirectObservations(COrbitPathModel* path,COrbitAttitudeModel* attitudes) const
{
	// do nothing by default, we leave the parameters as they are initialised by the sensor models

	if (path)
	{
		CXYZPoint directObs = path->getDirectObservationForShift();
		
		if (this->readAVNIR)
		{
			directObs.setCovarElement(0,0,STDDEV_SHIFT_AVNIR2);
			directObs.setCovarElement(1,1,STDDEV_SHIFT_AVNIR2);
			directObs.setCovarElement(2,2,STDDEV_SHIFT_AVNIR2);
		}
		else
		{
			directObs.setCovarElement(0,0,STDDEV_SHIFT_PRISM);
			directObs.setCovarElement(1,1,STDDEV_SHIFT_PRISM);
			directObs.setCovarElement(2,2,STDDEV_SHIFT_PRISM);
		}

		path->setDirectObservationForShift(directObs);
	}

	if (attitudes)
	{
		CXYZPoint directObs = attitudes->getDirectObservationForAngles();
		
		if (this->readAVNIR)
		{
			directObs.setCovarElement(0,0,STDDEV_ROT_AVNIR2);
			directObs.setCovarElement(1,1,STDDEV_ROT_AVNIR2);
			directObs.setCovarElement(2,2,STDDEV_ROT_AVNIR2);
		}
		else
		{
			directObs.setCovarElement(0,0,STDDEV_ROT_PRISM);
			directObs.setCovarElement(1,1,STDDEV_ROT_PRISM);
			directObs.setCovarElement(2,2,STDDEV_ROT_PRISM);
		}

		attitudes->setDirectObservationForAngles(directObs);
	}
	


}