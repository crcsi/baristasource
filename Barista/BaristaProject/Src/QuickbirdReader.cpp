#include "QuickbirdReader.h"


#include "OrbitObservations.h"
#include "OrbitAttitudeModel.h"
#include "CCDLine.h"

#include "utilities.h"
#include "SystemUtilities.h"


CQuickbirdReader::CQuickbirdReader(void): CSatelliteDataReader(),ephemerisFile(""),attitudeFile(""),geoCalFile(""),imageMetaFile(""),
			quaternions(4),	pd(0.0),cx(0.0),cy(0.0),cz(0.0),qcs1(0.0),qcs2(0.0),qcs3(0.0),qcs4(0.0),
			detOriginX(0.0),detOriginY(0.0),detRotAngle(0.0),detPitch(0.0),isReverse(false)
{
}

CQuickbirdReader::~CQuickbirdReader(void)
{
}


bool CQuickbirdReader::readData(CFileName &filename)
{

	if (this->aimedRMSAttitudes < 0.0 || this->aimedRMSPath < 0.0)
	{
		this->errorMessage = "No thresholds for RMS set!";
		return false;
	}

	if (this->listener)
	{
		this->listener->setProgressValue(0.0);
		this->listener->setTitleString("Reading Dataset File ...");
	}

	char buffer[1000];

	FILE* in = fopen(filename.GetChar(),"r");
	
	if (!in) 
	{
		this->errorMessage.Format("File: %s not found!",filename.GetFullFileName().GetChar());	
		return false;
	}

	while (fgets(buffer,1999,in))
	{
		CCharString b(buffer);
		b.trim();

		if (b.Find("END;") == 0)
			break;


		if (b.Find("ephemFilename") == 0)
		{
			int start = b.Find("=")+3;
			int end = b.Find(";")-2;
			this->ephemerisFile = filename.GetPath() + CSystemUtilities::dirDelimStr + b.Mid(start,end);

		}
		else if (b.Find("attFilename") == 0)
		{
			int start = b.Find("=")+3;
			int end = b.Find(";")-2;
			this->attitudeFile = filename.GetPath() + CSystemUtilities::dirDelimStr + b.Mid(start,end);
		}
		else if (b.Find("geoCalFilename") == 0)
		{
			int start = b.Find("=")+3;
			int end = b.Find(";")-2;
			this->geoCalFile = filename.GetPath() + CSystemUtilities::dirDelimStr + b.Mid(start,end);
		}
		else if (b.Find("IMFFilename") == 0)
		{
			int start = b.Find("=")+3;
			int end = b.Find(";")-2;
			this->imageMetaFile = filename.GetPath() + CSystemUtilities::dirDelimStr + b.Mid(start,end);
		}
	}

	// check if we found all the files
	if (this->ephemerisFile.IsEmpty() || this->attitudeFile.IsEmpty() ||
		this->geoCalFile.IsEmpty() || this->imageMetaFile.IsEmpty())
	{
		this->errorMessage = "Not all needed files found!";	
		return false;
	}

	// init the observation handler
	this->orbitPoints->initObservations();
	this->orbitPoints->setFileName(this->ephemerisFile);


	this->attitudes->initObservations();
	this->attitudes->setFileName(this->attitudeFile);


	// start reading the files
	if (this->listener)
	{
		this->setMaxListenerValue(0);
		this->listener->setTitleString("Reading Ephemeris File ...");
	}


	if (!this->readEphemeris())
		return false;

	if (this->listener)
	{
		this->setMaxListenerValue(10);
		this->listener->setTitleString("Reading Attitude File ...");
	}

	if (!this->readAttitudes())
		return false;


	if (!this->readGeoCalib())
		return false;
	
	if (!this->readImageMetaFile())
		return false;

	if (this->listener)
	{
		this->setMaxListenerValue(99);
		this->listener->setTitleString("Computing Orbit ...");
	}

	double startTime,endTime;
	this->computeInterval(startTime,endTime);

	if(!this->adjustOrbit(true,true,startTime,endTime))
		return false;


	if (this->listener)
	{
		this->listener->setProgressValue(100.0);
		this->listener->setTitleString("Finish Reading");
	}
	return true;
}



bool CQuickbirdReader::readEphemeris()
{

	FILE* in = fopen(this->ephemerisFile.GetChar(),"r");
	
	if (!in) 
	{
		this->errorMessage.Format("File: %s not found!",this->ephemerisFile.GetFullFileName().GetChar());		
		return false;
	}

	CXYZOrbitPointArray* oa = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();
	
	// clear the array
	oa->SetSize(0);


	char buffer[1000];
	int count=0;
	double start_time = 0.0;
	double timeInterval = 0.0;
	int found=0;
	int nEphemeris=0;

	// search for starttime and timeInterval
	while (fgets(buffer,1999,in))
	{
		CCharString b(buffer);
		if (b.Find("startTime") == 0)
		{
			int day, month, year, hour, min;
			double sec;
			sscanf(buffer,"startTime = %4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
			start_time = hour*3600 + min*60 + sec;
			found++;
		}
		else if (b.Find("timeInterval") == 0)
		{
			sscanf(buffer,"timeInterval = %lf",&timeInterval);
			found++;
		}
		else if (b.Find("numPoints") == 0)
		{
			sscanf(buffer,"numPoints = %d", &nEphemeris);
			found++;
		}
		else if (b.Find("ephemList") == 0)
		{
			found++;
			break;
		}
	}

	if (found != 4)
	{
		fclose(in);
		this->errorMessage.Format("Not all parameter found in file: %s !",this->ephemerisFile.GetFullFileName().GetChar());
		return false;
	}

	// now read the actual ephemeris
	for (;;count++)
	{
		if (fgets(buffer,1999,in) == NULL)
			break;
		if (strcmp(buffer,"END;") == 0)
			break;

		CXYZOrbitPoint* p = oa->add(); 
		double q11,q12,q13,q22,q23,q33;
		sscanf(buffer,"( %*d, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf),",&(*p)[0],&(*p)[1],&(*p)[2],&(*p)[3],&(*p)[4],&(*p)[5],&q11,&q12,&q13,&q22,&q23,&q33);

		Matrix *cov = p->getCovariance();
		cov->element(0,0) = q11;
		cov->element(0,1) = cov->element(1,0) = q12;
		cov->element(0,2) = cov->element(2,0) = q13;
		cov->element(1,1) = q22;
		cov->element(1,2) = cov->element(2,1) = q23;
		cov->element(2,2) = q33;
		p->setTime(0,0,start_time + count*timeInterval);
		p->setHasVelocity(true);

		if (this->listener != NULL)
		{
			double per = (double)count / (double) nEphemeris;
			this->listener->setProgressValue(per * this->listenerMaxValue);
		}
		
	}

	fclose(in);
/*
	this->orbitPoints.setCreateWithVelocity(false);
	// read the LIDAR data
	for (;count < 15500;count++)
	{
		if (fgets(buffer,1999,in) == NULL)
			break;
		CXYZOrbitPoint* p = this->orbitPoints.add(); 
		double q11,q12,q13,q22,q23,q33;
		sscanf(buffer," %lf %lf %lf %lf",&p->dot, &(*p)[0],&(*p)[1],&(*p)[2]);

	}
*/

	return true;
}

bool CQuickbirdReader::readAttitudes()
{


	FILE* in = fopen(this->attitudeFile.GetChar(),"r");
	
	if (!in) 
	{
		this->errorMessage.Format("File: %s not found!",this->attitudeFile.GetFullFileName().GetChar());		
		return false;
	}

	// clear the array
	this->quaternions.SetSize(0);

	char buffer[1000];
	int count=0;
	double start_time = 0.0;
	double timeInterval = 0.0;
	int found=0;
	int nAttitudes=0;
	// search for starttime and timeInterval
	while (fgets(buffer,1999,in))
	{
		CCharString b(buffer);
		if (b.Find("startTime") == 0)
		{
			int day, month, year, hour, min;
			double sec;
			sscanf(buffer,"startTime = %4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
			start_time = hour*3600 + min*60 + sec;
			found++;
		}
		else if (b.Find("timeInterval") == 0)
		{
			sscanf(buffer,"timeInterval = %lf",&timeInterval);
			found++;
		}
		else if (b.Find("numPoints") == 0)
		{
			sscanf(buffer,"numPoints = %d", &nAttitudes);
			found++;
		}
		else if (b.Find("attList") == 0)
		{
			found++;
			break;
		}
	}

	if (found != 4)
	{
		fclose(in);
		this->errorMessage.Format("Not all parameter found in file: %s !",this->attitudeFile.GetFullFileName().GetChar());
		return false;
	}

	double listenerStartValue= 0.0;
	if (this->listener)
	{
		listenerStartValue = this->listener->getProgress();
	}


	//addition
	//bool posDefinite[2000];
	//bool posDefinite1[2000];
	//addition

	// now read the actual ephemeris
	for (;;count++)
	{
		if (fgets(buffer,1999,in) == NULL)
			break;
		if (strcmp(buffer,"END;") == 0)
			break;

		CObsPoint* p = this->quaternions.Add(); 
		double q11,q12,q13,q14,q22,q23,q24 ,q33,q34,q44;
		sscanf(buffer,"( %*d, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf),",&(*p)[0],&(*p)[1],&(*p)[2],&(*p)[3],&q11,&q12,&q13,&q14,&q22,&q23,&q24,&q33,&q34,&q44);

		Matrix *cov = p->getCovariance();
		cov->element(0,0) = q11;
		cov->element(0,1) = cov->element(1,0) = q12;
		cov->element(0,2) = cov->element(2,0) = q13;
		cov->element(0,3) = cov->element(3,0) = q14;
		cov->element(1,1) = q22;
		cov->element(1,2) = cov->element(2,1) = q23;
		cov->element(1,3) = cov->element(3,1) = q24;
		cov->element(2,2) = q33;
		cov->element(2,3) = cov->element(3,2) = q34;
		cov->element(3,3) = q44;
		p->dot = start_time + count*timeInterval;
		
		//addition>>
		//if (q11 > 0 && q22>0 && q33>0 && q44>0 && (q11>fabs(q12+q13+q14)) && (q22>fabs(q12+q23+q24)) && (q33>fabs(q13+q23+q34)) && (q44>fabs(q14+q24+q34)))
		//{
		//	posDefinite[count] = true;
		//}
		//else
		//{
		//	posDefinite[count] = false;
		//}

		//double m1 = q11
		//<<addition

		if (this->listener != NULL)
		{
			double per = listenerStartValue + ((double)count / (double) nAttitudes) *(this->listenerMaxValue - listenerStartValue);
			this->listener->setProgressValue(per);
		}
		
	}

	fclose(in);

	return true;
}




bool CQuickbirdReader::readGeoCalib()
{
	FILE* in = fopen(this->geoCalFile.GetChar(),"r");
	if (!in) 
	{
		this->errorMessage.Format("File: %s not found!",this->geoCalFile.GetFullFileName().GetChar());	
		return false;
	}


	char buffer[1000];
	int count=0;

	int found=0;

	bool readDetX = true;
	bool readDetY = true;
	bool readDetRot = true;
	bool readDetPitch = true;



	while (fgets(buffer,1999,in))
	{
		CCharString b(buffer);
		b.trim();

		if (b.Find("geoModelLevel") == 0)
		{
			sscanf(b.GetChar(),"geoModelLevel = \"%100s",buffer);
			b = buffer;
			if (b.GetLength() > 2)
				this->satInfo.processingLevel = b.Left(b.GetLength()-2);
			else
				this->satInfo.processingLevel = b;
			found++;
		}
		else if (b.Find("PD") == 0)
		{
			sscanf(b.GetChar(),"PD = %lf",&pd);
			found++;
		}
		else if (b.Find("CX") == 0)
		{
			sscanf(b.GetChar(),"CX = %lf",&cx);
			found++;
		}
		else if (b.Find("CY") == 0)
		{
			sscanf(b.GetChar(),"CY = %lf",&cy);
			found++;
		}
		else if (b.Find("CZ") == 0)
		{
			sscanf(b.GetChar(),"CZ = %lf",&cz);
			found++;
		}
		else if (b.Find("qcs1") == 0)
		{
			sscanf(b.GetChar(),"qcs1 = %lf",&qcs1);
			found++;
		}
		else if (b.Find("qcs2") == 0)
		{
			sscanf(b.GetChar(),"qcs2 = %lf",&qcs2);
			found++;
		}
		else if (b.Find("qcs3") == 0)
		{
			sscanf(b.GetChar(),"qcs3 = %lf",&qcs3);
			found++;
		}
		else if (b.Find("qcs4") == 0)
		{
			sscanf(b.GetChar(),"qcs4 = %lf",&qcs4);
			found++;
		}
		else if (b.Find("detOriginX") == 0 && readDetX)
		{
			sscanf(b.GetChar(),"detOriginX = %lf",&detOriginX);
			found++;
			readDetX = false;
		}
		else if (b.Find("detOriginY") == 0 && readDetY)
		{
			sscanf(b.GetChar(),"detOriginY = %lf",&detOriginY);
			found++;
			readDetY = false;
		}
		else if (b.Find("detRotAngle") == 0 && readDetRot)
		{
			sscanf(b.GetChar(),"detRotAngle = %lf",&detRotAngle);
			found++;
			readDetRot = false;
		}
		else if (b.Find("detPitch") == 0 && readDetPitch)
		{
			sscanf(b.GetChar(),"detPitch = %lf",&detPitch);
			found++;
			readDetPitch = false;
		}
	}
	fclose(in);

	if (found != 13)
	{
		this->errorMessage.Format("Not all parameter found in file: %s !",this->geoCalFile.GetFullFileName().GetChar());
		return false;
	}

	return true;

}



bool CQuickbirdReader::readImageMetaFile()
{
	FILE* in = fopen(this->imageMetaFile.GetChar(),"r");
	if (!in) 
	{
		this->errorMessage.Format("File: %s not found!",this->imageMetaFile.GetFullFileName().GetChar());	
		return false;
	}


	char buffer[1000];

	int found=0;
	int cols,rows;
	double firstTime=0.0;
	double linesPerSec=0.0;
	CCharString scanDirection;
	
	this->satInfo.imageID = this->imageMetaFile.GetFileName();

	while (fgets(buffer,1999,in))
	{
		CCharString b(buffer);
		b.trim();

		 if (b.Find("bandId") == 0)
		{
			sscanf(b.GetChar(),"bandId = \"%100s;",buffer);
			b = buffer;
			if (b.GetLength() > 2)
				this->satInfo.imageType = b.Left(b.GetLength()-2);
			else
				this->satInfo.imageType = b;
			found++;

			this->satInfo.cameraName = "QuickBird-" + this->satInfo.imageType;
		}
		else if (b.Find("numColumns") == 0)
		{
			sscanf(b.GetChar(),"numColumns = %d",&cols);
			found++;
		}
		else if (b.Find("numRows") == 0)
		{
			sscanf(b.GetChar(),"numRows = %d",&rows);
			found++;
		}
		else if (b.Find("firstLineTime") == 0)
		{
			int day, month, year, hour, min;
			double sec;	
			sscanf(b.GetChar(),"firstLineTime = %4d-%2d-%2dT%2d:%2d:%lf",&year,&month,&day,&hour,&min,&sec);
			firstTime = hour*3600 + min*60 + sec;
			found++;
		}
		else if (b.Find("avgLineRate") == 0)
		{
			sscanf(b.GetChar(),"avgLineRate = %lf",&linesPerSec);
			found++;
		}
		else if (b.Find("scanDirection") == 0)
		{
			sscanf(b.GetChar(),"scanDirection = %100s",&buffer);
			scanDirection = buffer;
			if (scanDirection.Find("Reverse") >= 0)
				this->isReverse = true;
			else
				this->isReverse = false; // forward as default
			
		}

	}

	fclose(in);

	if (found < 5)
	{
		this->errorMessage.Format("Not all parameter found in file: %s !",this->imageMetaFile.GetFullFileName().GetChar());
		return false;
	}

	((CCCDLine*)this->camera)->setNCols(cols);
	((CCCDLine*)this->camera)->setNRows(rows);

	this->satInfo.nRows = rows;
	this->satInfo.nCols = cols;

	if (this->isReverse)
		this->timePerLine = -1.0/linesPerSec;
	else // forward case, the usual case...
		this->timePerLine = 1.0/linesPerSec;

	this->firstLineTime = firstTime;
	this->lastLineTime = firstTime + this->timePerLine * rows;

	return true;
}



bool CQuickbirdReader::prepareAttitudes()
{
	COrbitAttitudeModel *model = (COrbitAttitudeModel*)this->attitudes->getCurrentSensorModel();

	// this array stores the transformed rpy angles
	CXYZOrbitPointArray* newRPYAngles = (CXYZOrbitPointArray*)this->attitudes->getObsPoints();
	newRPYAngles->setCreateWithVelocity(false);
	
	// clear the array
	newRPYAngles->SetSize(0);


	// compute the fixed Rotation Matrix at R(T/2) and save it in the attitude model
	CXYZOrbitPointArray* orbitPointArray = (CXYZOrbitPointArray*)this->orbitPoints->getObsPoints();	
	
	// take the middle as a first guess for the middle time
	int middleIndex = orbitPointArray->GetSize()/2;
	CXYZOrbitPoint* point = orbitPointArray->getAt(middleIndex);
	double middleTime = this->firstLineTime + (this->lastLineTime - this->firstLineTime) / 2.0;

	if (point->dot < middleTime)
		while (middleIndex < orbitPointArray->GetSize() && orbitPointArray->getAt(middleIndex)->dot < middleTime)
			middleIndex++;
	else 
		while (middleIndex >= 0 && orbitPointArray->getAt(middleIndex)->dot > middleTime)
			middleIndex--;


	if (middleIndex < 0 || middleIndex >= orbitPointArray->GetSize())
	{
		this->errorMessage.Format("No Attitude Data Points found!");
		return false;
	}

	point = orbitPointArray->getAt(middleIndex);

	C3DPoint location((*point)[0],(*point)[1],(*point)[2]);
	C3DPoint velocity((*point)[3],(*point)[4],(*point)[5]);
	CRotationMatrix fixedMatrix;

	C3DPoint X,Y,Z; // page 3 of Photogrammetric Record Paper
	Z = location /  location.getNorm();
	Y = Z.crossProduct(velocity);
	Y = Y / Y.getNorm();
	X = Y.crossProduct(Z);

	fixedMatrix(0,0) = X[0];
	fixedMatrix(1,0) = X[1];
	fixedMatrix(2,0) = X[2];

	fixedMatrix(0,1) = Y[0];
	fixedMatrix(1,1) = Y[1];
	fixedMatrix(2,1) = Y[2];

	fixedMatrix(0,2) = Z[0];
	fixedMatrix(1,2) = Z[1];
	fixedMatrix(2,2) = Z[2];

	model->setFixedECRMatrix(fixedMatrix);	// save Rfixed!
	model->setTimeFixedECRMatrix(point->dot); // and the time


	// compute the mounting matrix between tangential System and sensor 
	int size =this->quaternions.GetSize();
	CObsPoint *p = this->quaternions.GetAt(size/2); // just use one point
	CRotationBase* oldQuaternion = CRotationBaseFactory::initRotation(p,Quaternion);
	oldQuaternion->getRotMat().R_times_R(this->mountingTemp,fixedMatrix); // this->mountingTemp is the mounting matrix


	// create an empty matrix: for {Roll = 0, Pitch = 0, Yaw = 0}
	CRotationBase* newRPY = CRotationBaseFactory::initRotation(&C3DPoint(),RPY);
	// we store the result  in here and compute then the final angles
	CRotationMatrix& newRPYMatrix = newRPY->getRotMat();


	CObsPoint* dummy = NULL;
	CRotationMatrix tmpMat;

	double listenerStartValue= 0.0;
	if (this->listener)
	{
		listenerStartValue = this->listener->getProgress();
	}

	//addition>>
	//double Ang1[2000], Ang2[2000], Ang3[2000];
	//FILE *fp;
	//fp = fopen("Angles.txt","w");
	//<<addition

	for (int i=0; i< size; i++)
	{
		dummy = this->quaternions.GetAt(i);
		oldQuaternion->updateAngles_Rho(dummy);
		
		// the multiplication
		oldQuaternion->getRotMat().RT_times_R(tmpMat,this->mountingTemp);
		fixedMatrix.RT_times_R(newRPYMatrix,tmpMat);

		// update and save the parameter
		newRPY->computeParameterFromRotMat();
	
		CXYZOrbitPoint* angles = newRPYAngles->add();
		
		this->computeError(*angles->getCovariance(),newRPY->getRotMat(),dummy);

		for (int k=0; k < 3; k++)
			(*angles)[k] = ((*newRPY)[k] < 0.0 )&& (fabs((*newRPY)[k] + M_PI) < 1.0) ? (*newRPY)[k]+ 2*M_PI: (*newRPY)[k];
		angles->dot = dummy->dot;

		//addition>>
		//Ang1[i] = (*angles)[0];
		//Ang2[i] = (*angles)[1];
		//Ang3[i] = (*angles)[2];
		//fprintf(fp,"%f\t%f\t%f\n",Ang1[i],Ang2[i],Ang2[i]);
		//<<addition

		(*angles) *= 1000.0; // save in m rad
		(*angles->getCovariance()) *= 1000000; // covar as well


		if (this->listener != NULL)
		{
			double per = listenerStartValue + ((double)i / (double) size) *(this->listenerMaxValue - listenerStartValue);

			this->listener->setProgressValue(per);
		}
	}

/*
	FILE* out1= fopen("quickbird_newRPY.txt","w");
	for (int i=0; i < newRPYAngles->GetSize(); i++)
	{
		CXYZOrbitPoint* p = newRPYAngles->getAt(i);
		fprintf(out1,"%20.15lf %20.15lf %20.15lf %20.7lf %20.15lf %20.15lf %20.15lf\n",(*p)[0],(*p)[1],(*p)[2],p->dot,p->getSigma(0),p->getSigma(1),p->getSigma(2));
	}
	fclose(out1);
*/


	//addition>>
	//fclose(fp);
	//<<addition

	delete oldQuaternion;
	delete newRPY;
	return true;
}


void CQuickbirdReader::computeError(Matrix& covar,const CRotationMatrix& rpy, const CObsPoint& quaternion) const
{
	Matrix dRdQ(5,4);   // rotation matrix elements after quaternion
	Matrix dAdR(3,5);   // vector after rotation matrix
	Matrix dAdAngle(3,3);  // angles after vector

	dAdR = 0;
	dAdAngle = 0;

	dRdQ.element(0,0) = 4*quaternion[0];
	dRdQ.element(0,1) = 0;
	dRdQ.element(0,2) = 0;
	dRdQ.element(0,3) = 4*quaternion[3];

	dRdQ.element(1,0) = 2*quaternion[1];
	dRdQ.element(1,1) = 2*quaternion[0];
	dRdQ.element(1,2) = -2*quaternion[3];
	dRdQ.element(1,3) = -2*quaternion[2];

	dRdQ.element(2,0) = 2*quaternion[2];
	dRdQ.element(2,1) = 2*quaternion[3];
	dRdQ.element(2,2) = 2*quaternion[0];
	dRdQ.element(2,3) = 2*quaternion[1];

	dRdQ.element(3,0) = -2*quaternion[3];
	dRdQ.element(3,1) = 2*quaternion[2];
	dRdQ.element(3,2) = 2*quaternion[1];
	dRdQ.element(3,3) = -2*quaternion[0];

	dRdQ.element(4,0) = 0;
	dRdQ.element(4,1) = 0;
	dRdQ.element(4,2) = 4*quaternion[2];
	dRdQ.element(4,3) = 4*quaternion[3];


	dAdR.element(0,3) = 1.0/rpy(2,2);
	dAdR.element(0,4) = -rpy(2,1)/(rpy(2,2)*rpy(2,2));

	dAdR.element(1,2) = -1.0;

	dAdR.element(2,0) = 1.0/rpy(0,0);
	dAdR.element(2,1) = -rpy(1,0)/(rpy(0,0)*rpy(0,0));

	
	double x = rpy(2,1)/rpy(2,2);
	double z = rpy(1,0)/rpy(0,0);

	dAdAngle.element(0,0) = 1.0/(1.0 + x*x);
	dAdAngle.element(1,1) = 1.0/sqrt(1.0 - rpy(2,0));
	dAdAngle.element(2,2) = 1.0/(1.0 + z*z);

	Matrix F = dAdAngle * dAdR * dRdQ;
	covar = F * *quaternion.getCovariance() * F.t();

}



bool CQuickbirdReader::prepareMounting()
{
	// swap x and y axis
	CRotationMatrix rotXY(0,-1,0,1,0,0,0,0,1);


	// mounting (camera -> spacecraft)
	// rotation 
	CObsPoint p(4);
	p[0] = qcs1;
	p[1] = qcs2;
	p[2] = qcs3;
	p[3] = qcs4;

	CRotationBase *rb = CRotationBaseFactory::initRotation(&p,Quaternion);
	CRotationMatrix &mountingQ = rb->getRotMat();
	CRotationMatrix tmpMat;
	CRotationMatrix mountRotMat;

	// compute new mounting rotmat in barista format
	mountingQ.R_times_R(tmpMat,this->mountingTemp);
	tmpMat.RT_times_RT(mountRotMat,rotXY);

	this->cameraMounting->setRotation(mountRotMat);

	double viewAngle = this->cameraMounting->getRotation()[0];

	if (viewAngle < 0) viewAngle +=2*M_PI;

	this->satInfo.viewDirection = "QuickBird-" + this->satInfo.imageID;
	


	// save the mounting  shift parameter, just in case its not equal to zero at some stage
	C3DPoint tmpPoint (cx,cy,cz);
	CXYZPoint newShift;
	this->mountingTemp.RT_times_x(newShift,tmpPoint);
	this->cameraMounting->setShift(newShift);


	detRotAngle *= M_PI/180.0;

	// init camera
	this->camera->setScale(detPitch);
	CXYZPoint irp(detOriginY / detPitch,-detOriginX / detPitch,-pd / detPitch);
	this->camera->setIRP(irp);

	delete rb;
	return true;
}


void CQuickbirdReader::computeInterval(double &startTime, double& endTime)
{
	if (this->isReverse)
	{
		// in this case timePerLine is negative!
		double timeOffset = this->satInfo.nRows * PERCENTAGE_OVERLAP / 100.0 * this->timePerLine;
		startTime = this->lastLineTime + timeOffset ;
		endTime = this->firstLineTime - timeOffset ;
	}
	else
	{
		CSatelliteDataReader::computeInterval(startTime,endTime);
	}
}