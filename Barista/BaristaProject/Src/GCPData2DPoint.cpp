#include "GCPData2DPoint.h"

CGCPData2DPoint& CGCPData2DPoint::operator = (const CGCPData2DPoint & point)
{

	CXYPoint::operator =(point);
	this->orthoImageFileName = point.orthoImageFileName;
	this->date = point.date;
	this->qualityOfDefinition = point.qualityOfDefinition;

	return *this;
}

void CGCPData2DPoint::setOrthoImageFileName(CFileName fname) 
{
	this->orthoImageFileName = fname;	
}

void CGCPData2DPoint::setDate(CCharString dt) 
{
	this->date = dt;
}

void CGCPData2DPoint::setQualityOfDefinition(double q) 
{
	this->qualityOfDefinition = q;
}

