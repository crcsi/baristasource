#include "newmat.h"
#include "BundleHelper.h"
#include "CharString.h"
#include "XYPointArrayPtrArray.h"
#include "XYZPointArray.h"
#include "BaristaProject.h"
#include "RotationBase.h"

CBaristaBundleHelper::CBaristaBundleHelper(float I_convergenceLimit, float MaxRobustPercentage,
		                                   float MinRobFac, float havg, const CCharString &protfile) : 
	CBundleManager(I_convergenceLimit, MaxRobustPercentage, MinRobFac, havg, protfile)
{
}

CBaristaBundleHelper::~CBaristaBundleHelper()
{
}

/**
*
*/
void CBaristaBundleHelper::bundleInit()
{
	this->reduction.x = this->reduction.y = this->reduction.z = 0.0;
  
	for (int i = 0; i < project.getNumberOfImages(); i++)
    {
		this->addObservations(project.getImageAt(i));
    }

	CVControl *control = project.getControlPoints();

	if (control && (control->nObs() > 0)) 
	{
		this->addObservations(control);

		CObsPointArray *points = control->getObsPoints();

		for (int i = 0; i < points->GetSize(); ++i)
		{
			CObsPoint *p = points->GetAt(i);
			this->reduction.x += (*p)[0];
			this->reduction.y += (*p)[1];
			this->reduction.z += (*p)[2];
		}

		double divisor = 1.0 / (double) points->GetSize();
		this->reduction.x *= divisor;
		this->reduction.y *= divisor;
		this->reduction.z *= divisor;
	}

	this->averageHeight = getAverageHeightFromReduction();

	for (int i=0; i < project.getNumberOfOrbits(); i++)
	{
		this->addObservations(project.getOrbitObservationAt(i));	
	}

  	CBundleManager::bundleInit(false,false);
}

 void CBaristaBundleHelper::bundleInit(CObservationHandler *obs, bool useControlPoints)
{
	this->reduction.x = this->reduction.y = this->reduction.z = 0.0;

	this->addObservations(obs);
    
	CVControl *control = project.getControlPoints();
	if (useControlPoints && control && (control->nObs() > 0)) 
	{
		this->addObservations(control);
		CObsPointArray *points = control->getObsPoints();

		for (int i = 0; i < points->GetSize(); ++i)
		{
			CObsPoint *p = points->GetAt(i);
			this->reduction.x += (*p)[0];
			this->reduction.y += (*p)[1];
			this->reduction.z += (*p)[2];
		}

		double divisor = 1.0 / (double) points->GetSize();
		this->reduction.x *= divisor;
		this->reduction.y *= divisor;
		this->reduction.z *= divisor;
	}

	this->averageHeight = getAverageHeightFromReduction();
	CBundleManager::bundleInit(false,false);
}

void CBaristaBundleHelper::bundleInit(CObservationHandlerPtrArray *images,CObservationHandlerPtrArray *orbits,bool forwardOnly,bool excludeTie)
{
	this->reduction.x = this->reduction.y = this->reduction.z = 0.0;

	for (int i = 0; i < images->GetSize(); i++) // add observation points
	{
		if (images->GetAt(i)->getObsPoints()->GetSize())
			this->addObservations(images->GetAt(i));
	}
    
	CVControl *control = project.getControlPoints();
	if (control && (control->nObs() > 0)) 
	{
		this->addObservations(control);
		CObsPointArray *points = control->getObsPoints();

		for (int i = 0; i < points->GetSize(); ++i)
		{
			CObsPoint *p = points->GetAt(i);
			this->reduction.x += (*p)[0];
			this->reduction.y += (*p)[1];
			this->reduction.z += (*p)[2];
		}

		double divisor = 1.0 / (double) points->GetSize(); // make average
		this->reduction.x *= divisor;
		this->reduction.y *= divisor;
		this->reduction.z *= divisor;
	}

	if (orbits)
	{
		for (int i = 0; i < orbits->GetSize(); i++)
		{
			this->addObservations(orbits->GetAt(i));
		}
	}

	this->averageHeight = getAverageHeightFromReduction();
	CBundleManager::bundleInit(forwardOnly,excludeTie);
}

double CBaristaBundleHelper::getAverageHeightFromReduction()
{
	this->averageHeight = 0.0;
  
	CVControl *control = project.getControlPoints();

	if (control && (control->nObs() > 0) && (control->getCurrentSensorModel()))
	{
		CReferenceSystem ref = control->getCurrentSensorModel()->getRefSys(); // reference system for the given images
		if (ref.getCoordinateType() != eGEOCENTRIC) this->averageHeight = this->reduction.z;
		else
		{
			C3DPoint p;
			ref.geocentricToGeographic(p,this->reduction);
			this->averageHeight = p.z;
		}
	}

	return this->averageHeight;
};

void CBaristaBundleHelper::updateResiduals()
{
	CBundleManager::updateResiduals(false);

	for ( int i = 0; i < this->getNStations(); i++)
	{
		CCharString station = this->getStationName(i);
		CObservationHandler* currentstation = project.getImageByName(&station);
		if ( !currentstation) // xyzpoints next
			currentstation = project.getControlPoints();

		if (currentstation)
		{
			if ( currentstation->getResidualPoints()->GetSize() > 0 )
			{
			//	CLabel nr = "";
			//	nr += i+1;
				CCharString resiname("Residuals");
			//	resiname += nr.GetChar(); 
				currentstation->getResidualPoints()->setFileName(resiname.GetChar());
			}
		}
	}
};

void CBaristaBundleHelper::setSensorFilenames()
{
	CBundleManager::setSensorFilenames();
	for ( int i = 0; i < this->getNStations(); i++)
	{
		
		CSensorModel *sensor = this->ObservationHandlers.GetAt(i)->getCurrentSensorModel();

		for (int j = 0; j < sensor->getListenerCount(); j++)
		{
			CUpdateListener* l = sensor->getListener(j);
			l->elementsChanged();
		}
	}
}


bool CBaristaBundleHelper::determineFixedRotMatForOrbit(CRotationMatrix& fixedRotMat,const CCharString& orbitName)const
{
	return project.getFixedRotationForOrbit(fixedRotMat,orbitName);	
}