#include "VBase.h"

CVBase::CVBase(void) : monoPlottedPoints(MONOPLOTTED)
{
}

CVBase::CVBase(const CVBase& src) : monoPlottedPoints(src.monoPlottedPoints),
	xyMonoPlottedLines(src.xyMonoPlottedLines),CUpdateHandler(src)
{

}
CVBase::~CVBase(void)
{
}



void CVBase::setName(const char* filename)
{
    this->name = filename;

	this->informListeners_elementsChanged(this->getClassName().c_str());
}


void CVBase::changedData()
{
    this->informListeners_elementsChanged(this->getClassName().c_str());
}


void CVBase::addMonoPlottedPoint(CXYPoint& p,bool sortArray)
{
	this->monoPlottedPoints.Add(p);

	if (sortArray)
		this->monoPlottedPoints.sortByLabel();
}

void CVBase::removeMonoPlottedPoint(int index)
{
	if (index >= 0 && index < this->monoPlottedPoints.GetSize())
		this->monoPlottedPoints.RemoveAt(index,1);
}

void CVBase::removeMonoPlottedPoint(const CLabel& label)
{
	CObsPoint * point = this->monoPlottedPoints.getPoint_binarySearch(label);
	if (point)
	{
		this->monoPlottedPoints.Remove(point);
		this->informListeners_elementDeleted();
	}
}


int CVBase::getNMonoPlottedPoints()const
{
	return this->monoPlottedPoints.GetSize();
}
	
CXYPoint* CVBase::getMonoPlottedPoint(int index)
{
	if (index >= 0 && index < this->monoPlottedPoints.GetSize())
		return this->monoPlottedPoints.getAt(index);
	else
		return NULL;
}

CXYPoint* CVBase::getMonoPlottedPoint(const CLabel& label)
{
	return (CXYPoint*) this->monoPlottedPoints.getPoint_binarySearch(label);
}

void CVBase::sortMonoPlottedPoints()
{
	this->monoPlottedPoints.sortByLabel();
}


void CVBase::serializeStore(CStructuredFileSection* pStructuredFileSection)
{

    CStructuredFileSection* child = pStructuredFileSection->addChild();
	this->monoPlottedPoints.serializeStore(child);

}


void CVBase::serializeRestore(CStructuredFileSection* pStructuredFileSection)
{

	for(int i = 0; i < pStructuredFileSection->getTokens()->GetSize(); i++)
	{
		CToken* token = pStructuredFileSection->getTokens()->GetAt(i);
		
        CCharString key = token->getKey();

    }

    for (int i = 0; i < pStructuredFileSection->getChildren()->GetSize(); i++)
    {
        CStructuredFileSection* child = pStructuredFileSection->getChildren()->GetAt(i);

		if (child->getName().CompareNoCase("CXYPointArray"))
        {
			CXYPointArray tmpArray;
			tmpArray.serializeRestore(child);
			 if (tmpArray.getPointType() == MONOPLOTTED)
				this->monoPlottedPoints = tmpArray;

       }

    }


}


