@echo off
echo Generating Documentation...


cd ..\Utilities\doc
@echo off
call generate.bat

cd ..\..\Maths\doc
@echo off
call generate.bat

cd ..\..\Geometry\doc
@echo off
call generate.bat

cd ..\..\ImageProc\doc
@echo off
call generate.bat

cd ..\..\Adjustment\doc
@echo off
call generate.bat

cd ..\..\ALS\doc
@echo off
call generate.bat

cd ..\..\Imaging\doc
@echo off
call generate.bat

cd ..\..\FeatureExtraction\doc
@echo off
call generate.bat

cd ..\..\BaristaProject\doc
@echo off
call generate.bat

cd ..\..\BaristaBatch\doc
@echo off
call generate.bat

cd ..\..\Bhl\doc
@echo off
call generate.bat

cd ..\..\info
@echo off
call generate.bat
