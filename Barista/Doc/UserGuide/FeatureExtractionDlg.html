<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<HTML>
<HEAD>
	<TITLE>The Extraction Dialog</TITLE>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=Windows-1252">
	<META HTTP-EQUIV="MSThemeCompatible" CONTENT="Yes">

	<!-- stylesheets for viewing the documentation in the HTMLHelp browser -->
	<LINK REL="stylesheet" MEDIA="screen" TYPE="text/css" HREF="MS-ITS:Barista.chm::/Barista.css">

	<!-- stylesheets for printing the documentation -->
	<LINK REL="stylesheet" MEDIA="print" TYPE="text/css" HREF="MS-ITS:Barista.chm::/Barista_print.css">

	<!-- just to view the html file direct in a browser -->
	<LINK REL="stylesheet" MEDIA="screen" TYPE="text/css" HREF="../Barista.css">

</HEAD>
<BODY>
	<DIV CLASS="doc">

	<OBJECT TYPE="application/x-oleobject" CLASSID="clsid:1e2a7bd0-dab9-11d0-b93a-00c04fc99f9e">
	<PARAM NAME="Keyword" VALUE="feature extraction">
	<PARAM NAME="Keyword" VALUE="edge extraction">
	<PARAM NAME="Keyword" VALUE="point extraction">
	<PARAM NAME="Keyword" VALUE="Canny operator">
	<PARAM NAME="Keyword" VALUE="Foerstner operator">
	</OBJECT>

	<H2 CLASS="doc">The Edge and Interest-Point Extraction Dialog</H2>
	<P CLASS="doc">
	This dialog provides a user interface for selecting methods for image feature extraction
	and the parameters for these feature extraction methods.
	In the group <B CLASS="doc">Feature Extraction Mode</B>, two methods for feature extraction can be selected:
	</P>
      <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc"><A CLASS="doc" HREF="#CannyExtraction">Canny</A></B>: Select this option if you want to
		extract image edges using the Canny operator</P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#FoerstnerExtraction">Foerstner Operator</A></B>: Select this option if you want to
		extract image points and/or edges using the framework for polymorphic feature extraction that is often
		referred to as the Foerstner Operator.</P></LI>
	</OL>


	<P CLASS="doc">
	Depending on which feature extraction mode has been selected, different parameters will be accessible.
	</P>
	<DIV CLASS="img">
			<IMG CLASS="img" SRC="FeatueExtractCanny.jpg" ALT="Grafik">&nbsp;&nbsp;
			<IMG CLASS="img" SRC="FeatureExtractionDlg.jpg" ALT="Grafik">
			<!-- caption of the image -->
			<P CLASS="img">Left: The Feature Extraction Dialog for the Canny operator;
			right: the Feature Extraction Dialog for the Foerstner operator</P>
	</DIV>

	<A NAME="CannyExtraction"><H2 CLASS="doc">Image Edge Extraction using the Canny Operator</H2></A>
	<P CLASS="doc">
	Image edge extraction using the Canny Operator consists of six phases:
	</P>
     <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc"><A CLASS="doc" HREF="#CannyEdgeGrad">Computation of gradients</A></B>:
		The image gradients are computed using a user-defined filter.</P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#CannyEdgeMark">Edge pixel classification</A></B>:
		Pixels having a significant gradient strength are marked as edge pixels.</P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#CannyEdgeNonMax">Non-maxima suppression</A></B>:
		Edge pixels not being local maxima are suppressed, which leaves the most significant edge pixels.</P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#CannyEdgeSubpix">Subpixel estimation</A></B>:
		For the edge pixels, a subpixel estimation is carried out.</P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#CannyEdgeTrack">Edge tracking</A></B>:
		Neighbouring edge pixels are connected to edge pixel chains.</P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#CannyEdgeThinne">Edge thinning</A></B>:
		The edge pixel chains are thinned out and approximated by polygons.</P></LI>
	</OL>

    <P CLASS="doc">
	Barista will store both the edge pixel chains and the final image edges:
	</P>

    <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Edge pixel chains</B>:
		The edge pixel chains will be added as a <B CLASS="doc">2D Contour</B> node to the
		<A CLASS="doc" HREF="ThumbNodeImage.html">image node</A> in the <A CLASS="doc" HREF="ThumbnailView.html">thumbnail view</A>.
		By default, the edge pixel chains will not be displayed, but they can be made active by selecting
		the <B CLASS="doc">2D Contour</B> node with a mouse button and selecting <B CLASS="doc">Display On/Off</B>
		in the appearing context menu.</P></LI>
		<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Image edges</B>:
		The thinned-out image edges chains will be added as a <B CLASS="doc">2D Line</B> node to the
		<A CLASS="doc" HREF="ThumbNodeImage.html">image node</A> in the <A CLASS="doc" HREF="ThumbnailView.html">thumbnail view</A>.
		They will also be displayed in the images.</P></LI>
	</OL>
		<DIV CLASS="img">
				<IMG CLASS="img" SRC="FeaturesContoursIcon.jpg" ALT="Grafik">
				<!-- caption of the image -->
				<P CLASS="img">The Icons for the extracted 2D Contours and the extracted 2D Lines</P>
		</DIV>

	<A NAME="CannyEdgeGrad"><H3 CLASS="doc">Computation of gradients for the Canny operator</H2></A>
	<P CLASS="doc">
	This is the first phase of feature extraction. The original image is convolved using a
	<B CLASS="doc">derivative filter</B> for both row and column direction. This convolution
	results in a <B CLASS="doc">gradient vector</B>, the components of which are the first derivatives
	of the grey levels in row and column directions. For images having more than one image band,
	the gradients are computed independently for each band, and an overall gradient is computed weighting the
	individual bands by their noise level. Finally, the <B CLASS="doc">length of the gradient vector</B> is
	computed as an indicator for the occurrence of an edge. The following parameters can be selected:
	</P>
      <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Parameters for computing the first derivative</B>:
		The user can select between three different filter <B CLASS="doc">kernels</B> to compute the first
		derivatives:</P>
		<OL CLASS="sublist">
			<LI CLASS="sublist"><P CLASS="doc"><B CLASS="doc">Simple Derivative:</B>
			The derivatives will be computed using a simple derivative kernel:</P>
			<P CLASS="doc">dg / dx (x,y) = 0.5 * (g[x + 1, y] - g[x - 1, y])</P>
			<P CLASS="doc">dg / dy (x,y) = 0.5 * (g[x, y + 1] - g[x, y - 1])</P>
			<P CLASS="doc">This is the fastest option, but the one with the lowest degree of smooting.</P></LI>
			<LI CLASS="sublist"><P CLASS="doc"><B CLASS="doc">Sobel Operator</B>:
			A 3 x 3 Sobel operator kernel will be used for computing the derivatives. This option does
			offer some degree of smoothing.</P></LI>
			<LI CLASS="sublist"><P CLASS="doc"><B CLASS="doc">Derivative of Gaussian</B>: the first
			derivative of a 2D Gaussian function in row and column direction, respectively, will be used
			for computing the image derivatives. This filter has a parameter <B CLASS="doc">Sigma</B>,
			i.e. the width sigma of the Gaussian function in [pixels], which can be selected in the respective
			numeric field. The size of the filter kernel will be 2 x (3 x Sigma) + 1 pixels. The larger
			the parameter Sigma, the more noise will be suppressed, but the more details will be suppressed, too.
			Thus, selecting a large value for Sigma will result in only the most relevant edges of the image to
			be extracted, whereas a small value for Sigma will result in more edges, but also in more noise.
			This is the theoretically optimal filter, but it is also the slowest one.</P></LI>
		</OL></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Noise Model for Feature Extraction</B>: The gradients are
		computed independently for each band of an image; the gradients for the individual bands are combined
		by a weighting average, the weight being dependent on the selected noise model.
		For more detail on the selection of that noise model,
		see chapter <A CLASS="doc" HREF="#CannyEdgeMark">edge pixel classification</A>.</P></LI>
	</OL>

	<DIV CLASS="img">
			<IMG CLASS="img" SRC="FeatueExtractDerivativeKernel.jpg" ALT="Grafik">
			<!-- caption of the image -->
			<P CLASS="img">Selection of the derivative kernel</P>
	</DIV>

	<A NAME="CannyEdgeMark"><H3 CLASS="doc">Edge pixel classification</H2></A>
	<P CLASS="doc">
	The <B CLASS="doc">lengths of the gradient vector</B> computed previously are compared to a threshold.
	Pixels with a gradient vector length smaller than that threshold are classified as
	<B CLASS="doc">homogeneous pixels</B>, whereas pixels with a gradient vector length larger
	than that threshold are classified as <B CLASS="doc">edge pixels</B>. The selection of the threshold
	depends on the <B CLASS="doc">noise model for feature extraction</B> and the selected
	<B CLASS="doc">significance level alpha</B>. The user can select between three different models for characterising image noise and selecting
		the threshold for edge pixel classification:
	</P>
     <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Percentile</B>: The threshold will be computed as
		the alpha percentile of the distribution of the gradient vector lengths. The value for alpha can
		be entered in the field <B CLASS="doc">Alpha / Percentile</B>. It has to be a value between 0 and 1.
		Using this model means that the 100 x alpha percent of the pixels having the smallest gradient lenghts
		will be classified as homogeneous, whereas the 100 x (1 - alpha) percent of the pixels with the largest
		gradient lengths will be flagged as edges.
		</P>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Gaussian</B>: If this model is selected, the noise
		variance of the image will be computed under the assumption of a Gaussian noise distribution, and
		the threshold will be computed as the alpha percentile of a Gaussian distribution having that noise
		variance. In this case, alpha is to be interpreted as a significance level, i.e. it is
		equal to the probability of erroneously classifying a homogeneous pixel as an edge pixel.
		Alpha has to be between 0 and 1.
		The larger alpha, the more pixels will be classified as edge pixels, which will result in more image
		details, but also in more noise. The smaller alpha, the fewer pixels will be classified as edge pixels,
		which will result in less noise and in more significant edges being extracted, but also
		in fewer details. </P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Poisson</B>: If this model is selected, the noise
		variance of the image will be computed under the assumption of a Poisson distribution for the noise noise,
		which means that the image noise depends on the grey levels. The poisson distribution is approximated
		by a Gaussian distribution using signal-dependent noise:</P>
		<P CLASS="doc">sigma_n = a + b x g</P>
		<P CLASS="doc">
		where g is the grey level and the parameters a and b are estimated from the images. Choosing this option
		works very similar to choosing <B CLASS="doc">Gaussian</B>. It is theoretically sounder and
		might yield slightly better results in very dark or very bright areas.</P></LI>
	</OL>

	<DIV CLASS="img">
			<IMG CLASS="img" SRC="FeatueExtractNoiseModel.jpg" ALT="Grafik">
			<!-- caption of the image -->
			<P CLASS="img">Selection of the noise model</P>
	</DIV>

	<A NAME="CannyEdgeNonMax"><H3 CLASS="doc">Non-maxima suppression</H2></A>
	<P CLASS="doc">
	For all pixels classified as <B CLASS="doc">edge pixels</B>, their gradient vector lenghts are
	compared to the lenghts of the two neighbouring edge pixels <B CLASS="doc">in gradient direction</B>.
	If any of these two neighbours has a larger gradient length, the current pixel is not a local maximum
	of gradient vector length, and thus the pixel is declared not to be an edge pixel.
	Thus, only the most significant edge pixels remain. No parameters can be selected.
	</P>

	<A NAME="CannyEdgeSubpix"><H3 CLASS="doc">Subpixel estimation</H2></A>
	<P CLASS="doc">
	For the remaining edge pixels, a subpixel position of the actual edge point is carried out.
	The gradients in gradient direction are approximated by a second order polynomial, and the
	position of the edge point is estimated as the position of the maximum of the polynomial.
	No parameters can be selected.
	</P>

	<A NAME="CannyEdgeTrack"><H3 CLASS="doc">Edge tracking</H2></A>
	<P CLASS="doc">
	Neighbouring edge pixels are connected by an edge tracking algorithm, which results in edge pixel chains.
	These edge pixel chains will be split at junctions (i.e. where edges intersect) and at points of high
	curvature. After that, short edge pixel chains will be discarded.
	The <B CLASS="doc">Minimum Line Length</B> in [pixels] can be selected in the according numerical field.
	</P>
	<DIV CLASS="img">
			<IMG CLASS="img" SRC="FeaturesContours.jpg" ALT="Grafik">
			<!-- caption of the image -->
			<P CLASS="img">The extracted edge pixel chains.</P>
	</DIV>


	<A NAME="CannyEdgeThinne"><H3 CLASS="doc">Edge thinning</H2></A>
	<P CLASS="doc">
	Each edge pixel chain is approximated by a polygon in an iterative procedure:
	</P>
	     <OL CLASS="doc">
			<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Splitting</B>:
			The edge pixel chain is split into straight line segments by a recursive algorithm.
			Splitting is finished when the maximum distance between any edge pixel and its nearest
			straight line segment is smaller than a threshold. This threshold (in [pixels]) can be selected in the
			numerical field <B CLASS="doc">Edge Thinning Threshold</B>.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Approximation</B>:
			The parameters of the individual straight line segments are computed from the edge pixels assigned
			to them, and the intersection points between these line segments, i.e. the vertices of the thinned-out
			edge polygons, are computed.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Merging</B>:
			A hypothesis test for identity is carried out for each pair of neighbouring straight line segments.
			If the straight line segments are found to be identical, they are merged.</P></LI>
			<LI CLASS="doc"><P CLASS="doc">Steps 2 and 3 are repeated until no further changes occur.</P></LI>
	</OL>

	<P CLASS="doc">
	No parameters can be selected.
	</P>
	<DIV CLASS="img">
			<IMG CLASS="img" SRC="FeaturesEdges.jpg" ALT="Grafik">
			<!-- caption of the image -->
			<P CLASS="img">The extracted image edges</P>
	</DIV>


	<A NAME="FoerstnerExtraction"><H2 CLASS="doc">Image Point and/or Edge Extraction using the Foerstner Operator</H2></A>
	<P CLASS="doc">
	Image feature extraction using the Foerstner Operator consists of six phases:
	</P>
	     <OL CLASS="doc">
			<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc"><A CLASS="doc" HREF="#FoerstnerGrad">Computation of gradients</A></B>:
			The image gradients are computed using a user-defined filter.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc"><A CLASS="doc" HREF="#FoerstnerVal">Computation of texture strength and directedness</A></B>:
			From the gradients, <B CLASS="doc">texture strength</B> and <B CLASS="doc">texture directedness</B>.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#FoerstnerEdgeMark">Texture classification</A></B>:
			Based on texture strength and texture directedness, a three-way classification is carried out for each pixel. Each pixel
			is classified as being either a <B CLASS="doc">point</B>, an <B CLASS="doc">edge</B>, or a <B CLASS="doc">homogeneous pixel</B>.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#FoerstnerNonMax">Non-maxima suppression</A></B>:
			Edge and/or point pixels not being local maxima are suppressed, which leaves the most significant edge/point pixels.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#FoerstnerPoints">Subpixel estimation</A></B>:
			A subpixel estimation is carried out for both edge and point pixels.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#FoerstnerTrack">Edge tracking</A></B>:
			Neighbouring edge pixels are connected to edge pixel chains.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc"><A CLASS="doc" HREF="#FoerstnerEdgeThinne">Edge thinning</A></B>:
			The edge pixel chains are thinned out and approximated by polygons.</P></LI>
	</OL>
	<P CLASS="doc">
	Note that here the user can select which features are to be extracted: points, edges, or both. If no edges are
	to be extracted, only the stages relevant for point extraction will be carried out;
	if no points are to be extracted, only the stages relevant for edge extraction will be carried out.
	</P>

    <P CLASS="doc">
	Barista will store the extracted points, the edge pixel chains and the final image edges:
	</P>

    <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Points</B>:
		The extracted image points will be added to the <B CLASS="doc">2D Points</B> node of
		<A CLASS="doc" HREF="ThumbNodeImage.html">image node</A> in the <A CLASS="doc" HREF="ThumbnailView.html">thumbnail view</A>.
		If the 2D Points are active, the extracted image points will be superimposed to the image.
		Note that the point label is initialised by an empty string.</P></LI>
		<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Edge pixel chains</B>:
		The edge pixel chains will be added as a <B CLASS="doc">2D Contour</B> node to the
		<A CLASS="doc" HREF="ThumbNodeImage.html">image node</A> in the <A CLASS="doc" HREF="ThumbnailView.html">thumbnail view</A>.
		By default, the edge pixel chains will not be displayed, but they can be made active by selecting
		the <B CLASS="doc">2D Contour</B> node with a mouse button and selecting <B CLASS="doc">Display On/Off</B>
		in the appearing context menu.</P></LI>
		<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Image edges</B>:
		The thinned-out image edges chains will be added as a <B CLASS="doc">2D Line</B> node to the
		<A CLASS="doc" HREF="ThumbNodeImage.html">image node</A> in the <A CLASS="doc" HREF="ThumbnailView.html">thumbnail view</A>.
		They will also be displayed in the images.</P></LI>
	</OL>
		<DIV CLASS="img">
				<IMG CLASS="img" SRC="FeaturesContoursIcon.jpg" ALT="Grafik">
				<!-- caption of the image -->
				<P CLASS="img">The Icons for the extracted 2D Contours and the extracted 2D Lines</P>
		</DIV>

	<A NAME="FoerstnerGrad"><H3 CLASS="doc">Computation of gradients for the Foerstner operator</H2></A>
	<P CLASS="doc">
	This is the first phase of feature extraction. The original image is convolved using a
	<B CLASS="doc">derivative filter</B> for both row and column direction. This convolution
	results in a <B CLASS="doc">gradient vector</B>, the components of which are the first derivatives
	of the grey levels in row and column directions. For images having more than one image band,
	the gradients are computed independently for each band. The filter kernel can be selected:
	</P>
      <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Simple Derivative:</B>
			The derivatives will be computed using a simple derivative kernel:</P>
			<P CLASS="doc">dg / dx (x,y) = 0.5 * (g[x + 1, y] - g[x - 1, y])</P>
			<P CLASS="doc">dg / dy (x,y) = 0.5 * (g[x, y + 1] - g[x, y - 1])</P>
			<P CLASS="doc">This is the fastest option, but the one with the lowest degree of smooting.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Sobel Operator</B>:
			A 3 x 3 Sobel operator kernel will be used for computing the derivatives. This option does
			offer some degree of smoothing.</P></LI>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Derivative of Gaussian</B>: the first
			derivative of a 2D Gaussian function in row and column direction, respectively, will be used
			for computing the image derivatives. This filter has a parameter <B CLASS="doc">Sigma</B>,
			i.e. the width sigma of the Gaussian function in [pixels], which can be selected in the respective
			numeric field. The size of the filter kernel will be 2 x (3 x Sigma) + 1 pixels. The larger
			the parameter Sigma, the more noise will be suppressed, but the more details will be suppressed, too.
			Thus, selecting a large value for Sigma will result in only the most relevant edges of the image to
			be extracted, whereas a small value for Sigma will result in more edges, but also in more noise.
			This is the theoretically optimal filter, but it is also the slowest one.</P></LI>
	</OL>

	<DIV CLASS="img">
			<IMG CLASS="img" SRC="FeatueExtractDerivativeKernel.jpg" ALT="Grafik">
			<!-- caption of the image -->
			<P CLASS="img">Selection of the derivative kernel</P>
	</DIV>

	<A NAME="FoerstnerVal"><H3 CLASS="doc">Computation of texture strength and directedness</H2></A>
	<P CLASS="doc">
	From the gradients, two measures describing the local variations of the grey levels are computed:
	</P>

    <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Texture Strength W</B>: This is the smoothed average
		squared gradient in a certain window. The user can select which window and which smoothing kernels are
		to be used for computing <B CLASS="doc">W</B>. For colour images, the gradients for the individual
		bands are combined, using the noise variance of the respective bands as weights.
		</P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Texture Directedness Q</B>: This parameter is also
		computed from the smoothed squared gradients (and mixed products). <B CLASS="doc">Q</B> is a measure
		for the local variation of the directional angles of the gradient vectors in a local neighbourhood,
		with <B CLASS="doc">Q</B> being in the interval [0, 1]. <B CLASS="doc">Q</B> is large for image edges
		and small for image points.
			</P></LI>
	</OL>

	<P CLASS="doc">
	The following parameters can be selected:
	</P>
    <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Kernel</B>:
		The user can select one of three smoothing kernels for computing texture strenght and directedness:
		</P>
			<OL CLASS="sublist">
			<LI CLASS="sublist"><P CLASS="doc"><B CLASS="doc">Gaussian Filter:</B>
			A 2D Gaussian function will be used for smoothing.
			This filter has a parameter <B CLASS="doc">Sigma</B>,
			i.e. the width sigma of the Gaussian function in [pixels].
			The size of the filter kernel will be 2 x (3 x Sigma) + 1 pixels.
			The larger the parameter Sigma, the more noise will be suppressed,
			but the more details will be suppressed, too.
			Thus, selecting a large value for Sigma will result in only the most relevant features of the image to
			be extracted, whereas a small value for Sigma will result in more features, but also in more noise.
			This is the theoretically optimal filter, but it is also the slowest one.</P></LI>
			<LI CLASS="sublist"><P CLASS="doc"><B CLASS="doc">Binomial Filter</B>:
			A n x n binomial filter kernel will be used for smoothing, where the parameter
			<B CLASS="doc">n</B> is the extent of the kernel. This can be seen as an approximation
			for a Gaussian filter.  </P></LI>
			<LI CLASS="sublist"><P CLASS="doc"><B CLASS="doc">Moving Average</B>:
			A n x n moving average filter kernel will be used for smoothing, where the parameter
			<B CLASS="doc">n</B> is the extent of the kernel.</P></LI>
			</P></LI></P></OL></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Integration Scale Edges</B>:
		    Here, the parameter of the filter kernel for edge extraction can be selected.
		    If the kernel is a <B CLASS="doc">Gaussian Filter</B>, it is
		    the filter parameter <B CLASS="doc">Sigma</B>; in the two other cases, it is the extent
		    <B CLASS="doc">n</B> in [pixels] of the filter kernel. If edges are to be extracted,
		    this parameter will be the one used for  <A CLASS="doc" HREF="#FoerstnerEdgeMark">texture classification</A>,
		    independently from whether points are to be extracted or not. If no edges are to be extracted,
		    this parameter will be inaccessible.
			</P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Integration Scale Points</B>:
		    Here, the parameter of the filter kernel for edge extraction can be selected.
		    If the kernel is a <B CLASS="doc">Gaussian Filter</B>, it is
		    the filter parameter <B CLASS="doc">Sigma</B>; in the two other cases, it is the extent
		    <B CLASS="doc">n</B> in [pixels] of the filter kernel.
		    If edges are to be extracted, this parameter will not be the one used for <A CLASS="doc" HREF="#FoerstnerEdgeMark">texture classification</A>,
		    but it will be used for <A CLASS="doc" HREF="#FoerstnerNonMax">non-maxima suppression</A> of points.
		    If no edges are to be extracted, this will be the parameter used for <A CLASS="doc" HREF="#FoerstnerEdgeMark">texture classification</A>.
		    If no points are to be extracted, this parameter will be inaccessible.
			</P></LI>
		<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Noise Model for Feature Extraction</B>: The gradients are
		computed independently for each band of an image; the gradients for the individual bands are combined
		by a weighting average, the weight being dependent on the selected noise model.
		For more detail on the selection of that noise model,
		see chapter <A CLASS="doc" HREF="#FoerstnerEdgeMark">texture classification</A>.</P></LI>
	</OL>

	<DIV CLASS="img">
			<IMG CLASS="img" SRC="FeatueExtractFoerstnerKernel.jpg" ALT="Grafik">
			<!-- caption of the image -->
			<P CLASS="img">Selection of the smoothing kernel for the Foerstner operator</P>
	</DIV>

	<A NAME="FoerstnerEdgeMark"><H3 CLASS="doc">Texture classification</H2></A>
	<P CLASS="doc">
	A three-way classification is carried out for each pixel of the image,
	using the <B CLASS="doc">texture strength W</B> and
	<B CLASS="doc">texture directedness Q</B>, using two thresholds <B CLASS="doc">Wmin</B> and <B CLASS="doc">Qmin</B>:
	</P>
    <OL CLASS="doc">
		<LI CLASS="doc"><P CLASS="doc">If <B CLASS="doc">W is smaller than Wmin</B>, the pixel
		is classified as a <B CLASS="doc">homogeneous pixel</B>, since the grey level gradients
		in its neighbourhood are not sigificant given the level of noise in the image described
		by the noise variance.
		</P></LI>
		<LI CLASS="doc"><P CLASS="doc">If <B CLASS="doc">W is smaller than Wmin</B>, the grey level
		gradients in the pixel's neighbourhood are sigificant, i.e., there is some significant
		texture. The pixel is further classified according to <B CLASS="doc">texture directedness</B>
		using a threshold q<B CLASS="doc">Qmin</B>:</P>
				<OL CLASS="sublist">
					<LI CLASS="sublist"><P CLASS="doc">If <B CLASS="doc">Q is smaller than Qmin</B>,
					the grey level changes are anisotropic, and the pixel is classified as an
					<B CLASS="doc">edge pixel</B>.</P></LI>
					<LI CLASS="sublist"><P CLASS="doc">If <B CLASS="doc">Q is larger than Qmin</B>,
					the grey level changes are isotropic, and the pixel is classified as
					<B CLASS="doc">point pixel</B>.
					</P></LI></P>
				</OL>
		</LI>
	</OL>

    <P CLASS="doc">
	The selection of the threshold  <B CLASS="doc">Wmin</B>
		depends on the <B CLASS="doc">noise model for feature extraction</B> and the selected
		<B CLASS="doc">significance level alpha</B>. The user can select between three different models for characterising image noise and selecting
			the threshold for pixel classification:
		</P>
	     <OL CLASS="doc">
			<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Percentile</B>: The threshold will be computed as
			the alpha percentile of the distribution of the gradient vector lengths. The value for alpha can
			be entered in the field <B CLASS="doc">Alpha / Percentile</B>. It has to be a value between 0 and 1.
			Using this model means that the 100 x alpha percent of the pixels having the smallest gradient lenghts
			will be classified as homogeneous, whereas the 100 x (1 - alpha) percent of the pixels with the largest
			gradient lengths will be flagged as non-homogeneous.
			</P>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Gaussian</B>: If this model is selected, the noise
			variance of the image will be computed under the assumption of a Gaussian noise distribution, and
			the threshold will be computed as the alpha percentile of a Gaussian distribution having that noise
			variance. In this case, alpha is to be interpreted as a significance level, i.e. it is
			equal to the probability of erroneously classifying a homogeneous pixel as a non-homogeneous pixel.
			Alpha has to be between 0 and 1.
			The larger alpha, the more pixels will be classified as edge pixels, which will result in more image
			details, but also in more noise. The smaller alpha, the fewer pixels will be classified as point or edge
			pixels,
			which will result in less noise and in more significant features being extracted, but also
			in fewer details. </P></LI>
			<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Poisson</B>: If this model is selected, the noise
			variance of the image will be computed under the assumption of a Poisson distribution for the noise noise,
			which means that the image noise depends on the grey levels. The poisson distribution is approximated
			by a Gaussian distribution using signal-dependent noise:</P>
			<P CLASS="doc">sigma_n = a + b x g</P>
			<P CLASS="doc">
			where g is the grey level and the parameters a and b are estimated from the images. Choosing this option
			works very similar to choosing <B CLASS="doc">Gaussian</B>. It is theoretically sounder and
			might yield slightly better results in very dark or very bright areas.</P></LI>
	</OL>


    <P CLASS="doc">
	The threshold  <B CLASS="doc">Qmin</B> can be selected in the according numeric field.
	<B CLASS="doc">Qmin</B> has to be between 0 and 1. The smaller <B CLASS="doc">Qmin</B>, the
	more non-homogeneous pixels will be classified as points, but there will be fewer edge pixels;
	the larger <B CLASS="doc">Qmin</B>, the more non-homogeneous pixels will be classified as
	edges, but there will be fewer point pixels.
	</P>

	<A NAME="FoerstnerNonMax"><H3 CLASS="doc">Non-maxima suppression</H2></A>
		     <OL CLASS="doc">
				<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Edges</B>:
				For all pixels classified as <B CLASS="doc">edge pixels</B>, their texture strengths are
				compared to the lenghts of the two neighbouring edge pixels <B CLASS="doc">in gradient direction</B>.
				If any of these two neighbours has a larger texture strength, the current pixel is not a local maximum
				of texture strength, and thus the pixel is declared not to be an edge pixel.
				Thus, only the most significant edge pixels remain. No parameters can be selected.
				</P>
				<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Points</B>: Local maxima for the texture strength
				are searched. The size of <B CLASS="doc">n</B> of the search area can be selected in the numerical field
				<B CLASS="doc">Non-Max Suppression</B> in [pixels]. Thus, if there is any point pixel within
				a window of size <B CLASS="doc">n</B> x <B CLASS="doc">n</B> centered at a certain pixel that
				has a greater texture strength than the current pixel, the current pixel is not a local
				maximum and will be declared not to be a point pixel. Thus, only the most significant point
				pixels will survive. </P></LI>
		</OL>
	</P>

	<A NAME="FoerstnerPoints"><H3 CLASS="doc">Subpixel estimation</H2></A>
	<P CLASS="doc">

		     <OL CLASS="doc">
				<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Edges</B>:
				For the remaining edge pixels, a subpixel position of the actual edge point is carried out.
				The gradients in gradient direction are approximated by a second order polynomial, and the
				position of the edge point is estimated as the position of the maximum of the polynomial.
	            No parameters can be selected.
				</P>
				<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Points</B>: Sub-pixel coordinates for
				points will be computed in a local window by using two models, i.e. that the point is
				a corner point or that it is the centre of a circular symmetric structure. The model
				achieving a significantly better fit is accepted; if the difference between the
				accuracies achieved is not significant, the point is assumed to be a corner.</P></LI>
		</OL>

	</P>

	<A NAME="FoerstnerTrack"><H3 CLASS="doc">Edge tracking</H2></A>
	<P CLASS="doc">
	Neighbouring edge pixels are connected by an edge tracking algorithm, which results in edge pixel chains.
	These edge pixel chains will be split at junctions (i.e. where edges intersect) and at points of high
	curvature. After that, short edge pixel chains will be discarded.
	The <B CLASS="doc">Minimum Line Length</B> in [pixels] can be selected in the according numerical field.
	</P>
	<DIV CLASS="img">
			<IMG CLASS="img" SRC="FeaturesContours.jpg" ALT="Grafik">
			<!-- caption of the image -->
			<P CLASS="img">The extracted edge pixel chains.</P>
	</DIV>

	<A NAME="FoerstnerEdgeThinne"><H3 CLASS="doc">Edge thinning</H2></A>
	<P CLASS="doc">
		Each edge pixel chain is approximated by a polygon in an iterative procedure:
		</P>
		     <OL CLASS="doc">
				<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Splitting</B>:
				The edge pixel chain is split into straight line segments by a recursive algorithm.
				Splitting is finished when the maximum distance between any edge pixel and its nearest
				straight line segment is smaller than a threshold. This threshold (in [pixels]) can be selected in the
				numerical field <B CLASS="doc">Edge Thinning Threshold</B>.</P></LI>
				<LI CLASS="doc"><P CLASS="doc"> <B CLASS="doc">Approximation</B>:
				The parameters of the individual straight line segments are computed from the edge pixels assigned
				to them, and the intersection points between these line segments, i.e. the vertices of the thinned-out
				edge polygons, are computed.</P></LI>
				<LI CLASS="doc"><P CLASS="doc"><B CLASS="doc">Merging</B>:
				A hypothesis test for identity is carried out for each pair of neighbouring straight line segments.
				If the straight line segments are found to be identical, they are merged.</P></LI>
				<LI CLASS="doc"><P CLASS="doc">Steps 2 and 3 are repeated until no further changes occur.</P></LI>
		</OL>
	<P CLASS="doc">
	No parameters can be selected.
	</P>
	<DIV CLASS="img">
			<IMG CLASS="img" SRC="FeaturesEdges.jpg" ALT="Grafik">
			<!-- caption of the image -->
			<P CLASS="img">The extracted image edges</P>
	</DIV>


      <P CLASS="doc">
       <A CLASS="noPrint" HREF="ImageView.html">Back to Image View Handling</A>
     </P>
     <P CLASS="pagebreak"></P>
	</DIV>
</BODY>
</HTML>

