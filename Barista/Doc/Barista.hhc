<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
<meta name="GENERATOR" content="Microsoft&reg; HTML Help Workshop 4.1">
<!-- Sitemap 1.0 -->
</HEAD><BODY>
<OBJECT type="text/site properties">
	<param name="Window Styles" value="0x800624">
</OBJECT>
<UL>
	<LI> <OBJECT type="text/sitemap">
		<param name="Name" value="Barista User Manual">
		<param name="Local" value="MS-ITS:Barista.chm::/Home.html">
		</OBJECT>
	<UL>
		<LI> <OBJECT type="text/sitemap">
			<param name="Name" value="Introduction">
			<param name="ImageNumber" value="2">
			</OBJECT>
		<UL>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Main Features of Barista">
				<param name="Local" value="Introduction.html#MainFeatures">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Release Notes">
				<param name="Local" value="ReleaseNotes.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Technical Notes">
				<param name="Local" value="UserGuide\TechnicalNotes.html#TechnicalNotes">
				<param name="ImageNumber" value="11">
				</OBJECT>
		</UL>
		<LI> <OBJECT type="text/sitemap">
			<param name="Name" value="Installation Guide">
			<param name="ImageNumber" value="2">
			</OBJECT>
		<UL>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Installation of Barista">
				<param name="Local" value="InstallGuide\InstallInstructions.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Uninstallation of Barista">
				<param name="Local" value="InstallGuide\UninstallInstructions.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
		</UL>
		<LI> <OBJECT type="text/sitemap">
			<param name="Name" value="User Guide">
			</OBJECT>
		<UL>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Main Window">
				<param name="Local" value="Barista.chm::/UserGuide/MainWindow.html">
				<param name="ImageNumber" value="21">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Toolbar">
				<param name="Local" value="UserGuide/Toolbar.html">
				<param name="ImageNumber" value="21">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Image View">
				<param name="Local" value="UserGuide/ImageView.html">
				<param name="ImageNumber" value="21">
				</OBJECT>
			<UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="ImageView Display Settings">
					<param name="Local" value="UserGuide/ImageViewDisplay.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Image Feature Extraction">
					<param name="Local" value="UserGuide/FeatureExtractionDlg.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
			</UL>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="DEM View">
				<param name="Local" value="UserGuide/DEMView.html">
				<param name="ImageNumber" value="21">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Menu">
				<param name="Local" value="UserGuide/MenuOverview.html">
				<param name="ImageNumber" value="21">
				</OBJECT>
			<UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="File">
					<param name="Local" value="UserGuide/MenuFile.html">
					<param name="ImageNumber" value="21">
					</OBJECT>
				<UL>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Import Image(s)">
						<param name="Local" value="UserGuide/ImportImages.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Import 3D Points">
						<param name="Local" value="UserGuide/Import3DPoints.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Import DEM">
						<param name="Local" value="UserGuide/ImportDEM.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Import ALS Data">
						<param name="Local" value="UserGuide/ImportALS.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Chip Database">
						<param name="Local" value="UserGuide\GCPchipDBase.html">
						</OBJECT>
				</UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Adjustments">
					<param name="Local" value="UserGuide/MenuAdjustments.html">
					<param name="ImageNumber" value="21">
					</OBJECT>
				<UL>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="RPC Generation">
						<param name="Local" value="UserGuide\RPCs_Generation.html">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Image/GCP Selection">
						<param name="Local" value="UserGuide/AdjustmentDlgSection1.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Observation/Parameter Selection">
						<param name="Local" value="UserGuide/AdjustmentDlgSection2.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Adjustment Controlling">
						<param name="Local" value="UserGuide/AdjustmentDlgSection3.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Adjustment Protocol">
						<param name="Local" value="UserGuide/AdjustmentProtocol.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
				</UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="ImageProcessing">
					<param name="Local" value="UserGuide/MenuImageProcessing.html">
					<param name="ImageNumber" value="21">
					</OBJECT>
				<UL>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Orthoimage">
						<param name="Local" value="UserGuide/Orthoimage.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="PanSharpen">
						<param name="Local" value="UserGuide/PanSharpen.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Register Images using Image Chips">
						<param name="Local" value="UserGuide\RegisterUsingChips.html">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Merge Image Bands">
						<param name="Local" value="UserGuide/BandMergeDlg.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Merge ALOS PRISM Sub Images">
						<param name="Local" value="UserGuide/AlosMergeDlg.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Matching of Ground Control Points">
						<param name="Local" value="UserGuide/GCPMatching.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
				</UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="DEMProcessing">
					<param name="Local" value="UserGuide/MenuDEMProcessing.html">
					<param name="ImageNumber" value="21">
					</OBJECT>
				<UL>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Merge DEMs">
						<param name="Local" value="UserGuide/MergeDEMs.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Difference DEMs">
						<param name="Local" value="UserGuide/DifferenceDEMs.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
				</UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Monoplotting">
					<param name="Local" value="UserGuide/MenuMonoplotting.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Feature Extraction">
					</OBJECT>
				<UL>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Edge &amp; Interest-Points Extraction">
						<param name="Local" value="UserGuide\MenuFeatureExtraction.html">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Building Detection">
						<param name="Local" value="UserGuide\BuildingsDetector.html">
						</OBJECT>
				</UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Window">
					<param name="Local" value="UserGuide/MenuWindow.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Settings">
					<param name="Local" value="UserGuide/MenuSettings.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Help">
					<param name="Local" value="UserGuide/MenuHelp.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
			</UL>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Thumbnail View">
				<param name="Local" value="UserGuide/ThumbnailView.html">
				<param name="ImageNumber" value="21">
				</OBJECT>
			<UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Image node">
					<param name="Local" value="UserGuide/ThumbNodeImage.html">
					<param name="ImageNumber" value="21">
					</OBJECT>
				<UL>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Export Image">
						<param name="Local" value="UserGuide/ExportImage.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Import PushBroom Metadata">
						<param name="Local" value="UserGuide/ImportMetaData.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Import Generic PushBroom Sensor">
						<param name="Local" value="UserGuide/ImportGenericPushBroom.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Import Frame Camera">
						<param name="Local" value="UserGuide/ImportFrameCameraModel.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Apply Transfer Function permanently">
						<param name="Local" value="UserGuide/ApplyTransferFunction.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
				</UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="3D Point node">
					<param name="Local" value="UserGuide/ThumbNode3DPoint.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="DEM node">
					<param name="Local" value="UserGuide/ThumbNodeDEM.html">
					<param name="ImageNumber" value="21">
					</OBJECT>
				<UL>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Transform a DEM">
						<param name="Local" value="UserGuide/TransformDEM.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
				</UL>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="2D Point node">
					<param name="Local" value="UserGuide/ThumbNode2DPoint.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="RPC node">
					<param name="Local" value="UserGuide/ThumbNodeRPC.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="PushBroomScanner node">
					<param name="Local" value="UserGuide/ThumbNodePushBroomScanner.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="CCD Camera node">
					<param name="Local" value="UserGuide/ThumbNodeCCDCamera.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Frame Camera node">
					<param name="Local" value="UserGuide/ThumbNodeFrameCamera.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Camera Mounting node">
					<param name="Local" value="UserGuide/ThumbNodeCameraMounting.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Orbit Path node">
					<param name="Local" value="UserGuide/ThumbNodeOrbitPath.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Orbit Attitudes node">
					<param name="Local" value="UserGuide/ThumbNodeOrbitAttitudes.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Frame Camera Model node">
					<param name="Local" value="UserGuide/ThumbNodeFrameCameraModel.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Affine node">
					<param name="Local" value="UserGuide/ThumbNodeAffine.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="XYZ Polyline node">
					<param name="Local" value="UserGuide/ThumbNodeXYZPolyline.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="Building node">
					<param name="Local" value="UserGuide/ThumbNodeBuilding.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="TFW node">
					<param name="Local" value="UserGuide/ThumbNodeTFW.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="XY Contour node">
					<param name="Local" value="UserGuide/ThumbNodeXYContour.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="XY Polyline node">
					<param name="Local" value="UserGuide/ThumbNodeXYPolylines.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="XY Ellipse node">
					<param name="Local" value="UserGuide/ThumbNodeXYEllipses.html">
					<param name="ImageNumber" value="11">
					</OBJECT>
				<LI> <OBJECT type="text/sitemap">
					<param name="Name" value="ALS node">
					<param name="Local" value="UserGuide/ThumbNodeALS.html">
					<param name="ImageNumber" value="21">
					</OBJECT>
				<UL>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Interpolation of a DEM">
						<param name="Local" value="UserGuide/ALSDEMInterpolate.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
					<LI> <OBJECT type="text/sitemap">
						<param name="Name" value="Interpolation of an Intensity image">
						<param name="Local" value="UserGuide/ALSIntensityInterpolate.html">
						<param name="ImageNumber" value="11">
						</OBJECT>
				</UL>
			</UL>
		</UL>
		<LI> <OBJECT type="text/sitemap">
			<param name="Name" value="Getting Started">
			<param name="Local" value="GettingStarted.html">
			<param name="ImageNumber" value="2">
			</OBJECT>
		<UL>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Sample Project">
				<param name="Local" value="GettingStarted/TestData.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Project Setup">
				<param name="Local" value="GettingStarted/TestData_Project.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Image Measurement">
				<param name="Local" value="GettingStarted/TestData_ImgMeasu.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Orientation">
				<param name="Local" value="GettingStarted/Orientation.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Monoplotting">
				<param name="Local" value="GettingStarted/TestData_Monoplotting.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Ortho Image Generation">
				<param name="Local" value="GettingStarted/TestData_Ortho.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Pansharpening">
				<param name="Local" value="GettingStarted/TestData_Pansharpening.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
		</UL>
		<LI> <OBJECT type="text/sitemap">
			<param name="Name" value="License terms">
			<param name="Local" value="UserGuide/LicenseOverview.html">
			<param name="ImageNumber" value="2">
			</OBJECT>
		<UL>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="ANN license terms">
				<param name="Local" value="UserGuide/LicenseANN.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="Xerces license terms">
				<param name="Local" value="UserGuide/LicenseXerces.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="GDAL license terms">
				<param name="Local" value="UserGuide/LicenseGDAL.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
			<LI> <OBJECT type="text/sitemap">
				<param name="Name" value="LibTIFF license terms">
				<param name="Local" value="UserGuide/LicenseLibTIFF.html">
				<param name="ImageNumber" value="11">
				</OBJECT>
		</UL>
	</UL>
</UL>
</BODY></HTML>
